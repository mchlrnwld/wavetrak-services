# Wavetrak Services

A monorepo to store services, functions, scripts, documentation, architecture diagrams and common packages.

## Table of contents <!-- omit in toc -->

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [System architecture](#system-architecture)
- [Main structure](#main-structure)
- [Services](#services)
  - [Service Usage](#service-usage)
  - [Authentication](#authentication)
  - [Infrastructure](#infrastructure)
  - [Deployment](#deployment)
    - [Service Deployment](#service-deployment)
    - [Function Deployment](#function-deployment)
    - [Static Webapp Deployment](#static-webapp-deployment)
    - [npm Package Deployment](#npm-package-deployment)
    - [pip Package Deployment](#pip-package-deployment)
    - [Airflow Job Deployment](#airflow-job-deployment)
- [Create a New Service](#create-a-new-service)
  - [Create Service Directory](#create-service-directory)
  - [Create ECR (Elastic Container Registry) Repository](#create-ecr-elastic-container-registry-repository)
  - [Create Initial Task Definitions](#create-initial-task-definitions)
  - [Copy Infrastructure Skeleton](#copy-infrastructure-skeleton)
  - [Customize Infrastructure](#customize-infrastructure)
    - [`services/test-service-for-readme/infra/environments/sandbox/main.tf`](#servicestest-service-for-readmeinfraenvironmentssandboxmaintf)
    - [`services/test-service-for-readme/infra/main.tf`](#servicestest-service-for-readmeinframaintf)
    - [`services/test-service-for-readme/infra/variables.tf`](#servicestest-service-for-readmeinfravariablestf)
    - [`services/test-service-for-readme/infra/README.md`](#servicestest-service-for-readmeinfrareadmemd)
  - [Apply Infrastructure to AWS in Sandbox Environment](#apply-infrastructure-to-aws-in-sandbox-environment)
    - [Run Terraform plan](#run-terraform-plan)
    - [Run Terraform apply](#run-terraform-apply)
  - [Send a Test HTTP Request](#send-a-test-http-request)
  - [Create Service Application Code](#create-service-application-code)
  - [Add `ci.json` File](#add-cijson-file)
  - [Add Container Definition File](#add-container-definition-file)
  - [Add Files and Push Branch to Github](#add-files-and-push-branch-to-github)
  - [Add Service to Jenkins Build](#add-service-to-jenkins-build)
  - [Build and Deploy Service](#build-and-deploy-service)
  - [Test HTTP Request](#test-http-request)
  - [Add `staging` and `prod` Infrastructure](#add-staging-and-prod-infrastructure)
- [Development](#development)
  - [Cloning the repo](#cloning-the-repo)
  - [Conventions / Standards](#conventions--standards)
    - [Starting points](#starting-points)
    - [File naming](#file-naming)
    - [Errors](#errors)
    - [API contracts and documentation](#api-contracts-and-documentation)
  - [Building](#building)
    - [Yarn Workspaces](#yarn-workspaces)
    - [NPM Packages](#npm-packages)
- [Deployment](#deployment-1)
  - [Jenkins jobs](#jenkins-jobs)
  - [Secrets management](#secrets-management)
    - [Secrets management workflow](#secrets-management-workflow)
    - [TODO](#todo)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## System architecture

The below architecture diagram describes the **Wavetrak** systems at the context level using the [C4-model architecture](https://c4model.com/) model.

![Wavetrak System Context](architecture/images/overall/overall-system-context.png?raw=true "Wavetrak System Context")

## Main structure

The current monorepo folders have the next structure:

- `architecture` - Architecture diagrams and proposals.
- `common` - Common packages used throughout services.
- `docker` - Mainly docker-compose files and mapping for several of the services.
- `docs` - Documents that might be shared with external people. Services documentation are within services folders.
- `jobs` - Airflow jobs.
- `notebooks` - Several python notebooks.
- `scripts` - Small script utilities and Jenkins jobs bash scripts.
- `functions` - Lambda functions. Some of the services within the `services` folder also contain lambda functions.
- `services` - A collection of micro services.
- `surfline-build-tools` - Build tools for local development environments.
- `tools` - Other utility scripts.

## Services

### Service Usage

Services may be accessed directly by other services or they may be accessed from outside of our VPC through the `services-proxy`. Services accessed through the `services-proxy` are authenticated through OpenResty and accessed at the following URLs:

- **Production**: [https://services.surfline.com](https://services.surfline.coms)
- **Staging**: [https://services.staging.surfline.com](https://services.staging.surfline.com)
- **Sandbox/Development**: [https://services.sandbox.surfline.com](https://services.sandbox.surfline.com)

### Authentication

Any endpoint that accesses user-dependent data is authenticated with OAuth 2.

To obtain a token, POST to /auth/token/. Include x-www-form-urlencoded grant_type ("password"), username and password:

```console
POST /auth/token/ HTTP/1.1
Host: services-staging.surfline.com
Content-Type: application/x-www-form-urlencoded
grant_type=password&username=johndoe&password=A3ddj3w
```

And the response body will be:

```json
{
  "access_token": "2YotnFZFEjr1zCsicMWpAA",
  "token_type": "bearer",
  "expires_in": 3600,
  "refresh_token": "tGzv3JOkF0XG5Qx2TlKWIA"
}
```

As an API user, either add a query param `?AccessToken=abcdefg123` or header `X-Auth-AccessToken` to requests when using the public services. Our public service proxy will authenticate the request with auth-service and pass the authenticated user id as header `X-Auth-UserId` for these services.

Services use the header `X-Auth-UserId` (injected by the public service proxy) to ensure API actions are authorized.

### Infrastructure

Terraform is used to provision the service infrastructure in AWS. By convention, the infrastructure scripts are stored in an `infra` folder within the service root.

In order to run these terraform scripts, your terminal needs to be configured with [AWS Credentials](https://github.com/Surfline/styleguide/blob/master/terraform/README.md#aws-credentials), and [New Relic Credentials](https://github.com/Surfline/styleguide/blob/master/terraform/README.md#new-relic-credentials). (For more information, see the [Wavetrak Terraform Styleguide](https://github.com/Surfline/styleguide/tree/master/terraform).)

### Deployment

In 2021, we migrated to use GitHub Actions for our CI/CD workflows. This replaces Jenkins for all major application deployments, including services, static webapps, Lambda functions, npm packages, pip packages, and Airflow jobs.

The underlying philosophy of the GitHub Actions CI/CD system is documented in the [CI/CD Architecture proposal](./architecture/proposals/ci-cd/ci-cd.md).

Contracts that must be met by applications in order to use the CI/CD system is documented in the [CI/CD Contracts proposal (WIP)](https://github.com/Surfline/wavetrak-services/blob/INF-672_Add-test-lint-cicd-contracts/architecture/proposals/ci-cd/contracts.md).

Deployment of all applications may be automatic or manual. The [#environments-and-release-strategies](./architecture/proposals/ci-cd/ci-cd.md#environments-and-release-strategies) portion of the CI/CD architecture proposal gives more details on this. This is the basic environment release strategy:

| Environment | Description             | Release Strategy                                      |
| :---------- | :----------             | :---------------                                      |
| `prod`      | Production              | Manual release from merge-commit on master            |
| `staging`   | Pre-production          | Automated release on merge to master                  |
| `preview`   | Per-application preview | Automated release from commit SHA on PR create/update |
| `sandbox`   | Free-for-all            | Manual release from branch, commit SHA, or master     |

There's a deployment dashboard in New Relic at <https://onenr.io/0oqQaeO6PQ1>

![service-deployment-deployment-dashboard](./docs/images/deployment/service-deployment-deployment-dashboard.png)

The next sections of this document describes how to deploy each application type:

#### Service Deployment

1. Navigate to the [wavetrak-services](./) repo in GitHub.
1. Select the "Actions" link in the top nav
    ![service-deployment-select-actions-button](./docs/images/deployment/service-deployment-select-actions-button.png)
1. Select "Services Pipeline" in the left side nav
    ![service-deployment-select-services-pipeline](./docs/images/deployment/service-deployment-select-services-pipeline.png)
1. Select "Run workflow" dropdown at the top of the list of workflow runs
    ![service-deployment-select-run-workflow-dropdown](./docs/images/deployment/service-deployment-select-run-workflow-dropdown.png)
1. Enter details about your deployment and select "Run workflow"
    1. Use workflow from: Branch: master. Leave this as-is.
    1. The name of the service to be deployed: The service name as listed under `./services/{service-name}` (ex. `cameras-service`)
    1. The environment where to deploy: `sandbox`, `staging`, or `prod`
    1. The branch name, tag, or commit SHA to deploy: Usually the commit SHA that you wish to commit
        1. Note: Due to an [issue](https://github.com/actions/checkout/issues/265) with GitHub Actions, you must use the full SHA and not the seven-character short SHA.
    ![service-deployment-enter-deployment-details](./docs/images/deployment/service-deployment-enter-deployment-details.png)
1. After several seconds, the deployment should appear at the top of the workflow runs list. You may need to refresh the page.
    ![service-deployment-view-workflow-in-list](./docs/images/deployment/service-deployment-view-workflow-in-list.png)
1. Select the workflow run that you just initiated to watch it as it runs. You may select the Jobs in the left-nav or the boxes in the graph to drill-down for more information
    ![service-deployment-view-workflow-in-progress](./docs/images/deployment/service-deployment-view-workflow-in-progress.png)
1. When your deployment is complete, it should appear in the [New Relic Deployment Dashboard](https://onenr.io/0oqQaeO6PQ1)

#### Function Deployment

1. Navigate to the [wavetrak-services](./) repo in GitHub.
1. Select the "Actions" link in the top nav
1. Select "Functions Pipeline" in the left side nav
1. Select "Run workflow" dropdown at the top of the list of workflow runs
1. Enter details about your deployment and select "Run workflow"
    1. Use workflow from: Branch: master. Leave this as-is.
    1. The name of the function to be deployed: The name as listed under `./functions/{function-name}` (ex. `airflow-scheduler-monitor`)
    1. The environment where to deploy: `sandbox`, `staging`, or `prod`
    1. The branch name, tag, or commit SHA to deploy: Usually the commit SHA that you wish to commit 
        1. Note: Due to an [issue](https://github.com/actions/checkout/issues/265) with GitHub Actions, you must use the full SHA and not the seven-character short SHA.
1. After several seconds, the deployment should appear at the top of the workflow runs list. You may need to refresh the page.
1. Select the workflow run that you just initiated to watch it as it runs. You may select the Jobs in the left-nav or the boxes in the graph to drill-down for more information
1. When your deployment is complete, it should appear in the [New Relic Deployment Dashboard](https://onenr.io/0oqQaeO6PQ1)

#### Static Webapp Deployment

Static webapp deployments are currently under development and should be available by 2021-10-08.

1. Navigate to the [wavetrak-services](./) repo in GitHub.
1. Select the "Actions" link in the top nav
1. Select "Web Static Pipeline" in the left side nav
1. Select "Run workflow" dropdown at the top of the list of workflow runs
1. Enter details about your deployment and select "Run workflow"
    1. Use workflow from: Branch: master. Leave this as-is.
    1. The name of the function to be deployed: The name as listed under `./web-static/{webapp-name}` (ex. `cancel`)
    1. The environment where to deploy: `sandbox`, `staging`, or `prod`
    1. The branch name, tag, or commit SHA to deploy: Usually the commit SHA that you wish to commit 
        1. Note: Due to an [issue](https://github.com/actions/checkout/issues/265) with GitHub Actions, you must use the full SHA and not the seven-character short SHA.
1. After several seconds, the deployment should appear at the top of the workflow runs list. You may need to refresh the page.
1. Select the workflow run that you just initiated to watch it as it runs. You may select the Jobs in the left-nav or the boxes in the graph to drill-down for more information
1. When your deployment is complete, it should appear in the [New Relic Deployment Dashboard](https://onenr.io/0oqQaeO6PQ1)

#### npm Package Deployment

npm package deployments are currently under development and should be available by 2021-10-13.

npm package deployments use Lerna. They are automatically deployed to Artifactory on PR merge to `master`.

Lower tier (`sandbox`/`staging`) deploys are not currently supported under Lerna.

#### pip Package Deployment

pip package deployments are currently under development and should be available by 2021-10-22.

1. Navigate to the [wavetrak-services](./) repo in GitHub.
1. Select the "Actions" link in the top nav
1. Select "pip Package Pipeline" in the left side nav
1. Select "Run workflow" dropdown at the top of the list of workflow runs
1. Enter details about your deployment and select "Run workflow"
    1. Use workflow from: Branch: master. Leave this as-is.
    1. The name of the function to be deployed: The name as listed under `./packages/{package-name}` (ex. `job-helpers`)
    1. The environment where to deploy: `dev` or `prod`
    1. The branch name, tag, or commit SHA to deploy: Usually the commit SHA that you wish to commit 
        1. Note: Due to an [issue](https://github.com/actions/checkout/issues/265) with GitHub Actions, you must use the full SHA and not the seven-character short SHA.
1. After several seconds, the deployment should appear at the top of the workflow runs list. You may need to refresh the page.
1. Select the workflow run that you just initiated to watch it as it runs. You may select the Jobs in the left-nav or the boxes in the graph to drill-down for more information
1. When your deployment is complete, it should appear in the [New Relic Deployment Dashboard](https://onenr.io/0oqQaeO6PQ1)

#### Airflow Job Deployment

Airflow job deployments are currently under development and should be available by 2021-11-10.

1. Navigate to the [wavetrak-services](./) repo in GitHub.
1. Select the "Actions" link in the top nav
1. Select "Airflow Job Pipeline" in the left side nav
1. Select "Run workflow" dropdown at the top of the list of workflow runs
1. Enter details about your deployment and select "Run workflow"
    1. Use workflow from: Branch: master. Leave this as-is.
    1. The name of the function to be deployed: The name as listed under `./jobs/{job-name}` (ex. `process-forecast-platform-noaa-gefs-wave/`)
    1. The environment where to deploy: `dev` or `prod`
    1. The branch name, tag, or commit SHA to deploy: Usually the commit SHA that you wish to commit 
        1. Note: Due to an [issue](https://github.com/actions/checkout/issues/265) with GitHub Actions, you must use the full SHA and not the seven-character short SHA.
1. After several seconds, the deployment should appear at the top of the workflow runs list. You may need to refresh the page.
1. Select the workflow run that you just initiated to watch it as it runs. You may select the Jobs in the left-nav or the boxes in the graph to drill-down for more information
1. When your deployment is complete, it should appear in the [New Relic Deployment Dashboard](https://onenr.io/0oqQaeO6PQ1)

## Create a New Service

Service bootstrapping is a little bumpy. We hope to automate much of this in the future. For now, these steps should get you started.

If you'd like to watch a video of the following steps being applied, you can view the ECS Service Bootstrapping tech talk from March 2021 (requires `surfline-dev` access):

```console
s3://sl-internal-tech-talks/video/2021-03-19-matt-walker-ecs-service-bootstrapping.mp4
```

### Create Service Directory

All services live under the `services` directory in `wavetrak-services`. The service directory should be named the same as the service itself. For our examples, we'll use the name `test-service-for-readme`.

```bash
# Run these at the root of `wavetrak-services`
cd services
mkdir test-service-for-readme
cd test-service-for-readme
```

### Create ECR (Elastic Container Registry) Repository

An ECR (Elastic Container Registry) repository provides a home for your container images. The following service in Jenkins will create your ECR repo in the `surfline-prod` AWS account and set permissions to allow access from both `surfline-prod` and `surfline-dev` AWS accounts.

[Jenkins > Misc > docker > create-ecr-repository](https://surfline-jenkins-master-prod.surflineadmin.com/job/misc/job/docker/job/create-ecr-repository/)

Be sure to prefix your repository name with `services/` when you create it. For our example repo, the ECS repo name would be `services/test-service-for-readme`.

### Create Initial Task Definitions

With our current ECS service infrastructure and deployment process, we can't create service infrastructure without an existing task definition. We currently create the initial versions of our task definitions manually.

Note: This is probably the ugliest and most manual part of the service bootstrapping process. This will change sometime in the future.

```bash
export SERVICE_NAME=test-service-for-readme

# Store container definition json in a variable
read -r -d '' CONTAINER_DEFINITION <<EOF
{
  "cpu": 0,
  "disableNetworking": true,
  "essential": true,
  "image": "surfline/simple-service",
  "memory": 128,
  "memoryReservation": 128,
  "name": "${SERVICE_NAME}",
  "portMappings": [
    {
      "containerPort": 8080,
      "hostPort": 8080,
      "protocol": "tcp"
    }
  ],
  "privileged": false,
  "readonlyRootFilesystem": true
}
EOF

# Run in `surfline-dev` AWS account
aws ecs register-task-definition \
  --family "wt-core-svc-sandbox-${SERVICE_NAME}" \
  --container-definitions "${CONTAINER_DEFINITION}"

# Run in `surfline-dev` AWS account
aws ecs register-task-definition \
  --family "wt-core-svc-staging-${SERVICE_NAME}" \
  --container-definitions "${CONTAINER_DEFINITION}"

# Run in `surfline-prod` AWS account
aws ecs register-task-definition \
  --family "wt-core-svc-prod-${SERVICE_NAME}" \
  --container-definitions "${CONTAINER_DEFINITION}"
```

### Copy Infrastructure Skeleton

All infrastructure code lives in `infra/` in the service directory. The infrastructure skeleton in [Surfline/styleguide/terraform/examples/service](https://github.com/Surfline/styleguide/tree/master/terraform/examples/service) serves as a good base.

While you're there, give the [Terraform Style Guide](https://github.com/Surfline/styleguide/blob/master/terraform/README.md) a read to get your Terraform environment set up and make sure you're up-to-date on best practices for our organization.

```bash
# This assumes you have `styleguide` repo in same dir as `wavetrak-services`
cp -a ../../styleguide/terraform/examples/service ./infra
```

### Customize Infrastructure

The [Terraform Style Guide](https://github.com/Surfline/styleguide/blob/master/terraform/README.md) has more information on Terraform module structure. This document will stick to the bare minimum to get your service up and running.

Additionally, this guide will focus on configuring a service in `sandbox` environment. To configure a service in `staging` or `production`, you can follow similar steps in those respective environments, taking care to change environment-specific values as necessary.

#### `services/test-service-for-readme/infra/environments/sandbox/main.tf`

Set the Terraform backend to prefix its key with your service name

```hcl
terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "test-service-for-readme/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}
```

Set the module name to your service name

```hcl
module "test-service-for-readme" {
```

#### `services/test-service-for-readme/infra/main.tf`

Set the module name to your service name

```hcl
module "test-service-for-readme" {
```

#### `services/test-service-for-readme/infra/variables.tf`

Set `application` variable default to your service name

```hcl
variable "application" {
  type        = string
  description = "Name of the service"
  default     = "test-service-for-readme"
}
```

Set `service_alb_priority` variable default.

This is another tricky one. We don't really use ALB priorities but it's a required variable and it must be unique across services. The best solution for that right now is to grep in code for `service_alb_priority` and pick one that isn't taken.

```bash
# run in root of wavetrak-services
ag --nogroup service_alb_priority \
  | grep ' = ' \
  | sed 's/.* = //' \
  | grep -v var \
  | sort -nu

# =>

100
150
152
233
240
245
255
[...]
```

```hcl
variable "service_alb_priority" {
  type        = string
  description = "The priority that the service has on the ALB in case of matching service_lb_rules"
  default     = "475"
}
```

#### `services/test-service-for-readme/infra/README.md`

Update the name of the service and add a description

```markdown
# test-service-for-readme Service Infrastructure

This is a test service.

It's used as an example in [wavetrak-services/README.md](../../../README.md#creating-a-new-service).
```

### Apply Infrastructure to AWS in Sandbox Environment

#### Run Terraform plan

Run Terraform plan to preview which infrastructure will be created.

Make sure that you have AWS credentials configured for the `surfline-dev` AWS account and New Relic credentials configured for the New Relic Lite account. For now, New Relic credentials are required whether or not the service uses New Relic. More information on credentials is available at [Surfline/styleguide/terraform#credentials](https://github.com/Surfline/styleguide/tree/master/terraform#credentials).

All commands should be run within `services/test-service-for-readme/infra/`.

Review output to make sure everything looks reasonable.

```bash
ENV=sandbox make plan

# =>

[...]

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

[...]

Plan: 13 to add, 0 to change, 0 to destroy.

[...]
```

#### Run Terraform apply

Run Terraform apply to preview which infrastructure will be created.

```bash
ENV=sandbox make apply

# =>

[...]

Plan: 13 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

[...]

Apply complete! Resources: 13 added, 0 changed, 0 destroyed.
```

If this step fails, it should give you debugging info to help understand why it failed. A common reason for failure here is using a conflicting ALB Priority (which we set above). If this happens, set another ALB priority and retry the `plan` and `apply`.

### Send a Test HTTP Request

At this point your service should be running the `surfline/simple-service` container in `sandbox`.

You should be able to issue an HTTP request against your service and get a response.

Note: You must be connected to the `surfline-dev` VPN to connect directly to your service in `sandbox` or `staging` and the `surfline-prod` VPN to connect directly to your service in `prod`.

```bash
curl -i http://test-service-for-readme.sandbox.surfline.com/

# =>

HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 49
Content-Type: application/json; charset=utf-8
Date: Fri, 19 Mar 2021 20:07:07 GMT
ETag: W/"31-AjT0YDufQnGXqrWbdytUeHgVmCg"
X-Powered-By: Express

{"status":200,"message":"OK","version":"unknown"}
```

### Create Service Application Code

Back in the root of our service directory, `services/test-service-for-readme`, we should create our application code. This guide will contain a very simple express app.

`services/test-service-for-readme/src/index.js`

```js
const express = require("express");
const app = express();
const port = 8080;

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/health", (req, res) => {
  res.send({
    status: 200,
    message: "OK",
    version: process.env.APP_VERSION || "unknown",
  });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
```

`services/test-service-for-readme/package.json`

```json
{
  "name": "test-service-for-readme",
  "version": "1.0.0",
  "description": "",
  "main": "src/index.js",
  "scripts": {
    "start": "node src/index.js",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "express": "^4.17.1"
  }
}
```

`services/test-service-for-readme/.dockerignore`

```console
infra/
node_modules/
```

`services/test-service-for-readme/Dockerfile`

```Dockerfile
FROM node:14

WORKDIR /opt/app
COPY . /opt/app

ARG APP_ENV=sandbox
ARG APP_VERSION=master
ENV NODE_ENV=$APP_ENV
ENV APP_VERSION=$APP_VERSION
RUN npm install

CMD ["npm", "start"]
```

### Add `ci.json` File

The `ci.json` file contains metadata about the service that is used in deployment. If your service uses environment variables, they should be specified here under `secrets`.

```json
{
  "imageName": "services/test-service-for-readme",
  "compose": "test-service-for-readme",
  "serviceName": "wt-test-service-for-readme",
  "cluster": "sl-core-svc",
  "secrets": {
    "common": {
      "prefix": "/{env}/common/",
      "secrets": []
    },
    "service": {
      "prefix": "/{env}/services/test-service-for-readme/",
      "secrets": []
    }
  },
  "taskRoleArn": {
    "sandbox": "arn:aws:iam::665294954271:role/test_service_for_readme_task_role_sandbox",
    "staging": "arn:aws:iam::665294954271:role/test_service_for_readme_task_role_sandbox",
    "prod": "arn:aws:iam::833713747344:role/test_service_for_readme_task_role_prod"
  },
  "newRelic": {
    "applicationId": {
      "sandbox": 0,
      "prod": 0
    }
  }
}
```

### Add Container Definition File

Add a container definition file to specify things like memory reservation and memory limit.

```yaml
version: "2"
services:
  test-service-for-readme:
    mem_reservation: 128m
    mem_limit: 128m
    image: ${REPOSITORY_HOST}/services/test-service-for-readme:${IMAGE_TAG}
    ports:
      - "8080"
    logging:
      driver: "awslogs"
      options:
        awslogs-group: "sl-logs-core-svc-${ENVIRONMENT}"
        awslogs-region: "us-west-1"
        awslogs-stream-prefix: "services"
```

### Add Files and Push Branch to Github

:octocat:

### Add Service to Jenkins Build

Browse to [Jenkins > Surfline > Web > deploy-service-to-ecs](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-service-to-ecs/), click "Configure", add your service as a choice for `SERVICE`, and click "Save".

### Build and Deploy Service

Again, browse to [Jenkins > Surfline > Web > deploy-service-to-ecs](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-service-to-ecs/) and click "Build with Parameters". Select your service from the `SERVICE` dropdown. Leave `ENVIRONMENT` as `sandbox`. Set your working branch name as `VERSION`. You did push it to Github, right? Click "Build". Select your build and click "Console Output" to watch the build in progress.

### Test HTTP Request

If everything went as expected, you should be able to issue an HTTP request against your service and get a response. The response will be similar to the response in your earlier request but this time it should contain the current commit SHA as `version`.

Note: You must be connected to the `surfline-dev` VPN to connect directly to your service in `sandbox` or `staging` and the `surfline-prod` VPN to connect directly to your service in `prod`.

```bash
curl -i http://test-service-for-readme.sandbox.surfline.com/

# =>

HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 49
Content-Type: application/json; charset=utf-8
Date: Fri, 19 Mar 2021 20:07:07 GMT
ETag: W/"31-AjT0YDufQnGXqrWbdytUeHgVmCg"
X-Powered-By: Express

{"status":200,"message":"OK","version":"ba464862141afdee95e47ff2b2220548624811f1"}
```

### Add `staging` and `prod` Infrastructure

This is up to you to do. :smile:

## Development

### Cloning the repo

When cloning the repository for the first time, ensure [Git Large File Storage](https://git-lfs.github.com/) is installed. Without this installed you will see an error message referencing `git-lfs` when executing `git clone`.

### Conventions / Standards

Development standardization is an active pursuit. When you perceive divergences in these guidelines, be a good code janitor.

- Ping a relevant Slack channel
- Comment on relevant pull-requests
- Investigate usages of divergent contracts
- Submit a pull request with code, affected usages, and documentation :star:
- When working with common packages (`./common/**`) abide by [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) structure for commit messages and commit individually for each package

#### Starting points

- [AirBNB's JS styleguide](https://github.com/airbnb/javascript) is our baseline styleguide
- [Wikipedia's List of HTTP status codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)
- [Nine REST principles](https://ninenines.eu/docs/en/cowboy/2.0/guide/rest_principles/)

#### File naming

- 22.6 If your file exports a single class, your filename should be exactly the name of the class.

```js
// file contents
class CheckBox {
  // ...
}
export default CheckBox;

// in some other file
// bad
import CheckBox from "./checkBox";

// bad
import CheckBox from "./check_box";

// good
import CheckBox from "./CheckBox";
```

- 22.7 Use camelCase when you export-default a function. Your filename should be identical to your function's name.
- Test files should be next to original file, but with .spec. `user.js` and `user.spec.js`
- File/directory structure should generally follow this:

```console
.                                  -- (*) This is where you are
├── README.md                      -- (*) This is what you are looking at
└── services                       -- Microservices
   └── user-service                -- Microservice
       ├── package.json            -- NPM project file for user-service
       └── src
           ├── common              -- Common libraries you use across modules
           │   └── logger.js       -- Logging
           ├── index.js            -- Entry point for your service
           ├── model               -- Model (data access)
           │   ├── User.spec.js    -- Test for your model
           │   ├── dbContext.js    -- Database connection initialization
           │   └── user.js         -- Model
           └── server              -- Express
               ├── index.js        -- Express initialization
               ├── user.js         -- Express endpoint (each resource standalone)
               └── user.spec.js    -- Test for express endpoint
```

#### Errors

Our base error format is simply an appropriate HTTP status code with `application/json` and `{ "message": "..." }`.

#### API contracts and documentation

Each service contains a README.md with request/response examples. This is the bare minimum mechanism to communicate your API's usage.

##### HTTP status code rules of thumb

- Successfully processed requests should respond back with a 200.
- Clients should expect a 400 for a bad request, like validation. The client can try the request with better parameters.
- Clients should expect a 500 when the server fails, no fault of the client.

### Building

#### Yarn Workspaces

This repo uses yarn workspaces for more efficient (and much faster!) dependency management. This announcement describes yarn workspaces in detail:

<https://yarnpkg.com/blog/2017/08/02/introducing-workspaces/>

To get started with yarn workspaces, make sure you're running version 1.0+ of yarn and set your local version of yarn to enable workspaces:

```bash
yarn config set workspaces-experimental true
```

After doing this, running `yarn install` from the root of this repo will install dependencies for all services.

Note: One thing to remember when updating with yarn is that you should be sure to commit the `yarn.lock` file in the root directory.

#### NPM Packages

We are now using [lerna](https://github.com/lerna/lerna) to automate our versioning and publishing of `@surfline` and `@wavetrak` NPM packages (the list of those is under the `packages` key in the `lerna.json` file).

Lerna allows us to automate the creation of changelogs using [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/). This makes it super easy to ensure we have up
to date changelogs to communicate changes easily and effectively across the team. It also makes it really easy to abide by semver.

We are using Lerna in tandem with Github Actions in order to make releasing and tagging of versions completely automatic upon the merging of a PR. Below are some instructions
on how to properly set up your local environment in order to make this flow work as efficiently as possible.

```sh
# Clone the repo
git clone https://github.com/Surfline/wavetrak-services.git
cd wavetrak-services
git pull --tags

# Install Lerna and other related packages
npm install
```

After installing the packages at the root of the repo, you can now begin developing a package that you want to work on.

```bash
git checkout -b SOME_BRANCH_NAME

# Go to the package
cd packages/eslint-web/

# Make some changes
vim index.js
```

After you've made you're changes and are ready to commit the changes, you can add your changes to git and then leverage the power of Lerna. You'll want to ensure that you follow
[conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) rules when making your commit messages.

```bash
git add .
git commit -m "fix: some descriptive message about a bug you fixed"

# At this point a commit hook will lint your commit message to ensure that it is properly formatted

cd ../../
# Current directory should now be: ~/wavetrak-services

# Run the `lerna version` command from the package.json
# This will create/update CHANGELOG and package files to properly match the version changes
npm run version-packages

git add .
git commit -m "chore: lerna version"
git push
```

At this point your branch should contain the following:

- The changes that you made to the package
- An updated `CHANGELOG.md` file
- Update `package.json` and `package-lock.json` file

Once the PR you have created is merged, the Github Actions on the repo will automatically run and release any version changes. See [the action here](https://github.com/Surfline/wavetrak-services/actions/workflows/packages-pipeline.yml)

## Deployment

### Jenkins jobs

According to the kind of service, several **Jenkins jobs** were prepared to deploy each of the services, functions and tasks.

The next table will list all of the currently active jobs to deploy in our infrastructure:

| Job                                                                                   | Details                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |         Execution         |
| ------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | :-----------------------: |
| Web / ECS<br/>[deploy-admin-service-to-ecs]                                           | Deploy Admin Services into ECS:<br/><ul><li>cameras</li><li>forecasts</li><li>login</li><li>reports</li><li>search</li><li>spots</li><li>users</li></ul>Repository: [surfline-admin]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | On demand<br/>[Dashboard] |
| Web / ECS<br/>[deploy-buoyweather-webapp-to-ecs]                                      | Deploy Buoyweather `webapp` into ECS:<br/>Repository: [buoyweather]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | On demand<br/>[Dashboard] |
| Web / ECS<br/>[deploy-frontend-to-ecs]                                                | Deploy frontend applications into ECS:<br/><ul><li>account</li><li>kbyg</li><li>homepage</li><li>onboarding</li><li>charts</li><li>travel</li><li>backplane</li></ul>Repository: [surfline-web]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | On demand<br/>[Dashboard] |
| Web / ECS<br/>[deploy-editorial-to-ecs]                                               | Deploy editorial applications into ECS:<br/><ul><li>editorial-wp</li><li>editorial-wp-admin</li><li>editorial-varnish</li></ul>Repository: [surfline-web-editorial]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | On demand<br/>[Dashboard] |
| Web / ECS<br/>[deploy-service-to-ecs]                                                 | Main deploy job for Wavetrak Services into ECS:<br/><ul><li>auth-service</li><li>breaking-wave-height-api</li><li>buoyweather-api</li><li>cameras-service</li><li>charts-service</li><li>entitlements-service</li><li>favorites-service</li><li>feed-service</li><li>forecasts-api</li><li>graphql-gateway</li><li>geo-target-service</li><li>kbyg-product-api</li><li>location-view-service</li><li>metering-service</li><li>msw-tide-service</li><li>notifications-service</li><li>onboarding-service</li><li>password-reset-service</li><li>promotions-service</li><li>reports-api</li><li>science-data-service</li><li>science-models-db</li><li>search-service</li><li>services-proxy</li><li>session-analyzer</li><li>sessions-api</li><li>spots-api</li><li>subscription-service</li><li>tech-talk-service</li><li>test-service-for-readme</li><li>user-service</li><li>wave-api</li><li>weather-api</li></ul>Repository: [wavetrak-services]                                                                                                                                                                                                                                                                                       | On demand<br/>[Dashboard] |
| Web / ECS<br/>[deploy-web-proxy-to-ecs]                                               | Deploy Web Proxy into ECS.<br/>Repository: [surfline-web]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | On demand<br/>[Dashboard] |
| Web / S3<br/>[build-and-deploy-account-to-s3]                                         | Deploy Surfline Account modules to S3:<br/><ul><li>account/cancel-next</li><li>account/upgrade-next</li><li>account/promotions-next</li><li>account/registration-next</li><li>account/gifts-next</li><li>account/offers-next</li><li>perks</li></ul>Repository: [surfline-web]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             | On demand<br/>[Dashboard] |
| Web / S3<br/>[build-and-deploy-sl-admin-to-s3]                                        | Deploy Surfline Admin modules to S3:<br/><ul><li>modules/gifts</li><li>modules/poi</li><li>modules/promotions</li><li>modules/sessions</li></ul>Repository: [surfline-admin]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | On demand<br/>[Dashboard] |
| Web / S3<br/>[build-and-deploy-sl-admin-to-s3]                                        | Deploy Surfline Admin modules to S3:<br/><ul><li>modules/gifts</li><li>modules/poi</li><li>modules/promotions</li><li>modules/sessions</li></ul>Repository: [surfline-admin]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | On demand<br/>[Dashboard] |
| Admin tools<br/>[deploy-spot-to-elasticsearch-function]                               | Updates `spot-etl-admin-to-es` lambda function.<br/>Repository: [wavetrak-services]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |         On demand         |
| Buoyweather<br/>[build-bw-web]                                                        | Creates Docker image for BW web.<br/>Repository: [buoyweather]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |         On demand         |
| Buoyweather<br/>[deploy-bw-web]                                                       | Deploys the Docker image previously created with [build-bw-web] into ECS.<br/>_Does not make use of any repository._                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |         On demand         |
| Microservices<br/>[deploy-lambda-functions-admin]                                     | Updates admin lambda functions:<br/><ul><li>spotreportview-builder</li><li>sort-subregion-spots</li><li>clear-cf-redis</li></ul>Repository: [surfline-admin]<br/>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | On demand<br/>[Dashboard] |
| Microservices<br/>[deploy-lambda-functions-cameras-service-embed-generator]           | Updates Cameras Service lambda function:<br/><ul><li>embed-generator</li></ul>Repository: [wavetrak-services]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |         On demand         |
| Microservices<br/>[deploy-lambda-functions-cameras-service-rewind-clip]               | Updates Cameras Service lambda function:<br/><ul><li>rewind-clip-generator</li></ul>Repository: [wavetrak-services]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |         On demand         |
| Microservices<br/>[deploy-lambda-functions-editorial-cms]                             | Updates Editorial CMS lambda functions:<br/><ul><li>feed-etl</li><li>travel-page</li><li>update-varnish-cache</li></ul>Repository: [surfline-web-editorial]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |         On demand         |
| Microservices<br/>[deploy-lambda-functions-notifications-service]                     | Updates Notifications Service lambda function:<br/><ul><li>notification-processor</li></ul>Repository: [wavetrak-services]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |         On demand         |
| Microservices<br/>[deploy-lambda-functions-sessions-api]                              | Updates Sessions API lambda functions:<br/><ul><li>garmin-parser</li><li>session-enricher</li></ul>Repository: [wavetrak-services]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |         On demand         |
| Misc<br/>[create-ecr-repository]                                                      | Easily create an image repository in ECR with cross-account permissions.<br/>_Does not make use of any repository._                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |         On demand         |
| Misc<br/>[deploy-chat-ops]                                                            | Deploys ChatOps application into ElasticBeanstalk.<br/>Repository: [chatops]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |         On demand         |
| Science / Science Scripts<br/>[build-and-deploy-science-scripts]                      | Build and deploy `science-scripts`.<br/>Repository: [science-scripts]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |         On demand         |
| Science / Science Scripts<br/>[build-and-deploy-surfline-weather-stations]            | Build and deploy `surfline-weather-stations`.<br/>Repository: [surfline-weather-stations]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |         On demand         |
| Science / Science Scripts<br/>[daily-master-build-and-deploy-DEV]                     | Build and deploy daily `science-scripts` into `dev`.<br/>Repository: [science-scripts]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |        Once a day         |
| Science / Science Scripts<br/>[daily-master-build-and-deploy-science-wrf-DEV]         | Build and deploy daily `science-scripts-wrf` into `dev`.<br/>Repository: [science-wrf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |        Once a day         |
| Science / Science Scripts<br/>[daily-master-build-and-deploy-sl-science-DEV]          | Build and deploy daily `science-scripts-sl` into `dev`.<br/>Repository: [surfline-science]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |        Once a day         |
| Science / Science Scripts<br/>[daily-master-build-and-deploy-sl-weather-stations-DEV] | Build and deploy daily `science-weather-stations` into `dev`.<br/>Repository: [surfline-weather-stations]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |        Once a day         |
| Science / Science Scripts<br/>[deploy-science-scripts]                                | Downstream job for building and deploying `science-scripts`.<br/>_Does not make use of any repository._                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |        Downstream         |
| Science / Science Scripts<br/>[deploy-science-wrf]                                    | Downstream job for building and deploying `science-scripts-wrf`.<br/>_Does not make use of any repository._                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |        Downstream         |
| Science / Science Scripts<br/>[deploy-surfline-science]                               | Downstream job for building and deploying `science-scripts-sl`.<br/>_Does not make use of any repository._                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |        Downstream         |
| Science / Science Scripts<br/>[deploy-surfline-weather-stations]                      | Downstream job for building and deploying `surfline-weather-stations`.<br/>_Does not make use of any repository._                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |        Downstream         |
| Science / Surflow<br/>[celery-science-job-deploy]                                     | Build and deploy multiple jobs from files in `science-job`.<br/>Repository: [science-job]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |         On demand         |
| Science / Surflow<br/>[restart-airflow-scheduler]                                     | Restart Airflow scheduler periodically, per suggestion from airflow team.<br/>_Does not make use of any repository._                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |        Once a day         |
| Surfline / Coldfusion<br/>[surfline-coldfusion-deploy-AWS]                            | Builds CF code to AWS.<br/>Repository: [surfline-coldfusion]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |         On demand         |
| Surfline / Coldfusion<br/>[surfline-coldfusion-deploy-qa]                             | Builds CF code to an allegedly QA environment.<br/>Repository: [surfline-coldfusion]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |         On demand         |
| Surfline / Coldfusion<br/>[surfline-coldfusion-deploy-staging]                        | Builds CF code in `master` to `staging`.<br/>Repository: [surfline-coldfusion]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |         On demand         |
| Surfline<br/>[delete_old_redis_caches]                                                | Deletes old Redis cache.<br/>Repository: [surfline-legacy-data-utils]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |        Every 5min         |
| Surfline<br/>[poll-maxmind-database]                                                  | Populates data into database.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |         Every 6hs         |
| Shared<br/>[deploy-chef-bundle]                                                       | Deploy Chef tarball bundles.<br/>Repository: [wavetrak-infrastructure]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | On demand<br/>[Dashboard] |
| Shared / Airflow<br/>[deploy-airflow-job]                                             | Deploy multiple Airflow jobs:<br/><ul><li>common</li><li>analyze-cam-stills</li><li>apple-app-store-connect-etl-pipeline</li><li>archive-spotreportviews</li><li>check-and-update-camera-time-settings</li><li>delete-outdated-model-run-forecasts</li><li>download-nasa-mur-sst</li><li>download-noaa-gfs</li><li>download-noaa-gefs-wave</li><li>download-noaa-nam</li><li>download-noaa-ww3</li><li>expire-gift-subscriptions</li><li>google-play-console-etl-pipeline</li><li>process-forecast-platform-nasa-mur-sst</li><li>process-forecast-platform-noaa-gefs-wave</li><li>process-forecast-platform-noaa-gfs</li><li>process-forecast-platform-noaa-nam</li><li>process-forecast-platform-noaa-ww3</li><li>process-forecast-platform-wavetrak-lotus-ww3</li><li>process-full-grid-noaa-gfs</li><li>process-full-grid-wavetrak-lotus-ww3</li><li>process-prerecorded-cam-clips</li><li>process-wave-buoys-and-wind-stations</li><li>query-wowza-error-logs</li><li>reset-metering-limits</li><li>retry-failed-sessions</li><li>sync-mongodb-to-lower-tiers</li><li>sync-tide-data-to-fauna</li><li>sync-forecast-data-to-fauna</li><li>test-pagerduty-flag</li><li>verify-in-app-purchases</li></ul>Repository: [wavetrak-services] |         On demand         |
| Shared / Airflow<br/>[deploy-airflow-modules]                                         | Deploy plugins and DAG helpers to Airflow.<br/>Repository: [wavetrak-services]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |         On demand         |
| Shared / Airflow<br/>[deploy-wavetrak-common]                                         | Deploy Airflow common modules:<br/><ul><li>airflow-helpers</li><li>grib-helpers</li><li>job-helpers</li><li>science-algorithms</li><li>tap-apple-app-store-connect</li><li>tap-google-play-console</li><li>video-helpers</li></ul>Repository: [wavetrak-services]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |         On demand         |
| Shared / Quiver<br/>[build-quiver]                                                    | Build Docker image for Quiver project.<br/>Repository: [quiver]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |         On demand         |
| Shared / Quiver<br/>[release-quiver]                                                  | Create a release for each of the projects according to their type (patch, minor, major version):<br/><ul><li>react</li><li>themes</li></ul>Repository: [quiver]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |         On demand         |
| Shared / Quiver<br/>[release-quiver-to-frontends]                                     | Create a release for a frontend package:<br/><ul><li>quiver-react</li><li>quiver-assets</li><li>quiver-themes</li><li>web-common</li></ul>Repository: [surfline-web]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |           _WIP_           |

### Secrets management

Variables, secrets and configuration values are all treated the same, and are referenced in the `ci.json` file within the root of each service directory. Shared variables are scoped to the `common` namespace, while service-specific secrets are scoped to `service`. For more info, see [Application Secret Management with Parameter Store
](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/1066338448/Application+Secret+Management+with+Parameter+Store#ApplicationSecretManagementwithParameterStore-Secretnaming).

At a high level, the secrets management workflow is as follows:

1. Secret names are stored in `ci.json` file.
2. The `deploy-service-to-ecs.sh` script, which invokes and orchestrates the deployment steps, calls `create_ecs_params.py`, which creates an `ecs-params.yml` file containing references to secrets in Parameter Store.
3. The `ecs-params.yml` file is passed to the `ecs-cli` command which creates a new task definition.
4. `aws update-service` redeploys the ECS service using the new task definition.
5. ECS finishes redeploying service.
6. ECS signals back to Jenkins that service deployment is complete.

#### Secrets management workflow

![secrets deployment workflow](docs/images/wavetrak-sevice-deployment.jpg "Secrets Management Deployment Workflow")

#### TODO

- Update reference to Jenkins job after secrets management transition has been completed.

If you are trying to deploy a service, get a good understanding of the what-why for the build steps. For the deployment side of services in conjunction with Terraform, AWS and Jenkins, please consult the README under the the **wavetrak-infrastructure** repository at **wavetrak-infrastructure/terraform/surfline-product-services/README.md**

[build-and-deploy-account-to-s3]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/build-and-deploy-account-to-s3/
[build-and-deploy-science-scripts]: https://surfline-jenkins-master-prod.surflineadmin.com/job/science/job/Science-scripts/job/build-and-deploy-science-scripts/
[build-and-deploy-sl-admin-to-s3]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/build-and-deploy-sl-admin-to-s3/
[build-and-deploy-surfline-weather-stations]: https://surfline-jenkins-master-prod.surflineadmin.com/job/science/job/surfline-weather-stations/job/build-and-deploy-surfline-weather-stations/
[build-bw-web]: https://surfline-jenkins-master-prod.surflineadmin.com/job/buoyweather/job/build-bw-web/
[build-quiver]: https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/build-quiver/
[buoyweather]: https://github.com/Surfline/bw
[celery-science-job-deploy]: https://surfline-jenkins-master-prod.surflineadmin.com/job/science/job/Surflow/job/celery-science-job-deploy/
[chatops]: https://github.com/Surfline/chatops
[create-ecr-repository]: https://surfline-jenkins-master-prod.surflineadmin.com/job/misc/job/docker/job/create-ecr-repository/
[daily-master-build-and-deploy-dev]: https://surfline-jenkins-master-prod.surflineadmin.com/job/science/job/Science-scripts/job/daily-master-build-and-deploy-DEV/
[daily-master-build-and-deploy-science-wrf-dev]: https://surfline-jenkins-master-prod.surflineadmin.com/job/science/job/science-wrf/job/daily-master-build-and-deploy-science-wrf-DEV/
[daily-master-build-and-deploy-sl-science-dev]: https://surfline-jenkins-master-prod.surflineadmin.com/job/science/job/surfline-science/job/daily-master-build-and-deploy-sl-science-DEV/
[daily-master-build-and-deploy-sl-weather-stations-dev]: https://surfline-jenkins-master-prod.surflineadmin.com/job/science/job/surfline-weather-stations/job/daily-master-build-and-deploy-sl-weather-stations-DEV/
[delete_old_redis_caches]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/delete_old_redis_caches/
[deploy-admin-service-to-ecs]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-admin-service-to-ecs/
[deploy-airflow-job]: https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/deploy-airflow-job/
[deploy-airflow-modules]: https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/deploy-airflow-modules/
[deploy-buoyweather-webapp-to-ecs]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-buoyweather-webapp-to-ecs/
[deploy-bw-web]: https://surfline-jenkins-master-prod.surflineadmin.com/job/buoyweather/job/deploy-bw-web/
[deploy-chat-ops]: https://surfline-jenkins-master-prod.surflineadmin.com/job/misc/job/deploy-chatops/
[deploy-chef-bundle]: https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/deploy-chef-bundle/
[deploy-editorial-to-ecs]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-editorial-to-ecs/
[deploy-frontend-to-ecs]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-frontend-to-ecs/
[deploy-lambda-functions-admin]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-admin/
[deploy-lambda-functions-cameras-service-embed-generator]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-cameras-service-embed-generator/
[deploy-lambda-functions-cameras-service-rewind-clip]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-cameras-service-rewind-clip/
[deploy-lambda-functions-editorial-cms]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-editorial-cms/
[deploy-lambda-functions-notifications-service]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-notifications-service/
[deploy-lambda-functions-sessions-api]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-sessions-api/
[deploy-science-scripts]: https://surfline-jenkins-master-prod.surflineadmin.com/job/science/job/Science-scripts/job/deploy-science-scripts/
[deploy-science-wrf]: https://surfline-jenkins-master-prod.surflineadmin.com/job/science/job/science-wrf/job/deploy-science-wrf/
[deploy-service-to-ecs]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-service-to-ecs/
[deploy-spot-to-elasticsearch-function]: https://surfline-jenkins-master-prod.surflineadmin.com/job/admin/job/deploy-spot-to-elasticsearch-function/
[deploy-surfline-science]: https://surfline-jenkins-master-prod.surflineadmin.com/job/science/job/surfline-science/job/deploy-surfline-science/
[deploy-surfline-weather-stations]: https://surfline-jenkins-master-prod.surflineadmin.com/job/science/job/surfline-weather-stations/job/deploy-surfline-weather-stations/
[deploy-wavetrak-common]: https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/deploy-wavetrak-common/
[deploy-web-proxy-to-ecs]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-web-proxy-to-ecs/
[poll-maxmind-database]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/poll-maxmind-database/
[quiver]: https://github.com/Surfline/quiver
[release-quiver]: https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/release-quiver/
[release-quiver-to-frontends]: https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/release-quiver-to-frontends/
[restart-airflow-scheduler]: https://surfline-jenkins-master-prod.surflineadmin.com/job/science/job/Surflow/job/restart-airflow-scheduler/
[science-job]: https://github.com/Surfline/science-job
[science-scripts]: https://github.com/Surfline/science-scripts
[science-wrf]: https://github.com/Surfline/science-wrf
[surfline-admin]: https://github.com/Surfline/surfline-admin
[surfline-coldfusion-deploy-aws]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/surfline-coldfusion-deploy-AWS/
[surfline-coldfusion-deploy-qa]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/surfline-coldfusion-deploy-qa/
[surfline-coldfusion-deploy-staging]: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/surfline-coldfusion-deploy-staging/
[surfline-coldfusion]: https://github.com/Surfline/surfline-coldfusion
[surfline-legacy-data-utils]: https://github.com/Surfline/surfline-legacy-data-utils
[surfline-science]: https://github.com/Surfline/surfline-science
[surfline-weather-stations]: https://github.com/Surfline/surfline-weather-stations
[surfline-web-editorial]: https://github.com/Surfline/surfline-web-editorial
[surfline-web]: https://github.com/Surfline/surfline-web
[wavetrak-infrastructure]: https://github.com/Surfline/wavetrak-infrastructure
[wavetrak-services]: https://github.com/Surfline/wavetrak-services
[dashboard]: https://surfline-jenkins-master-prod.surflineadmin.com/view/version%20dashboard/
