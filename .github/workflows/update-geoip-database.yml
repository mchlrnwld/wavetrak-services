name: GeoIP Database Updates

on:
  schedule:
    - cron: "12 * * * *"
  workflow_dispatch:

jobs:
  check:
    name: Check
    runs-on: ubuntu-20.04

    steps:
      - id: checkout
        name: Checkout
        uses: actions/checkout@v2

      - id: aws-prod
        name: Configure AWS credentials (PROD)
        uses: aws-actions/configure-aws-credentials@v1
        with:
          aws-access-key-id: ${{ secrets.PROD_AWS_ACCESS_KEY_ID }}
          aws-secret-access-key: ${{ secrets.PROD_AWS_SECRET_ACCESS_KEY }}
          aws-region: ${{ secrets.AWS_DEFAULT_REGION }}

      - id: verify-changes
        name: Verify changes
        shell: bash
        env:
          GEOIP_LICENSE_KEY: ${{ secrets.GEOIP_LICENSE_KEY }}
        run: |
          echo "> Pulling down the MaxMind DB's md5 from the source..."
          curl -o \
            "./GeoIP2-City.tar.gz.md5" \
            "https://download.maxmind.com/app/geoip_download?edition_id=GeoIP2-City&suffix=tar.gz.md5&license_key=${GEOIP_LICENSE_KEY}"

          echo "> Downloading the MaxMind DB's md5 from 'prod' S3 bucket..."
          aws s3 cp \
            "s3://sl-artifacts-prod/maxmind/GeoIP2-City.tar.gz.md5" \
            "./GeoIP2-CityS3.tar.gz.md5"

          echo "> Comparing files..."
          MD5_FROM_SOURCE="$(cat ./GeoIP2-City.tar.gz.md5)"
          MD5_FROM_S3="$(cat ./GeoIP2-CityS3.tar.gz.md5)"
          echo "  Source DB md5: ${MD5_FROM_SOURCE}"
          echo "  S3 bucket md5: ${MD5_FROM_S3}"

          if [[ "${MD5_FROM_SOURCE}" != "${MD5_FROM_S3}" ]]; then
            echo "> Source database has changed!"

            echo "> Downloading latest maxmind-db's source..."
            curl -o \
              "./GeoIP2-City.tar.gz" \
              "https://download.maxmind.com/app/geoip_download?edition_id=GeoIP2-City&suffix=tar.gz&license_key=${GEOIP_LICENSE_KEY}"

            echo "> Preparing database file..."
            mkdir "./GeoIP2-City"
            tar -xvzf \
              "./GeoIP2-City.tar.gz" \
              --strip-components=1 -C \
              "./GeoIP2-City"
            mv -f \
              "./GeoIP2-City/GeoIP2-City.mmdb" \
              "./GeoIP2-City.mmdb"

            echo "Uploading new MaxMind's DB to S3..."
            aws s3 cp \
              "./GeoIP2-City.mmdb" \
              "s3://sl-artifacts-prod/maxmind/GeoIP2-City.mmdb" --acl public-read

            echo "Uploading new MaxMind DB's md5 to S3..."
            aws s3 cp \
              "./GeoIP2-City.tar.gz.md5" \
              "s3://sl-artifacts-prod/maxmind/GeoIP2-City.tar.gz.md5"

            echo "::set-output name=updated::true"

          else
            echo "> The MaxMind's DB from Source and S3 are the same!"
            echo "::set-output name=updated::false"
          fi

      - id: trigger-workflow
        if: steps.verify-changes.outputs.updated == 'true'
        name: Deploy geo-target-api
        uses: benc-uk/workflow-dispatch@4c044c1613fabbe5250deadc65452d54c4ad4fc7  # v1.1.0
        with:
          workflow: Services Pipeline
          inputs: '{"service": "geo-target-api", "environment": "prod", "ref": "master"}'
          token: ${{ secrets.PERSONAL_ACCESS_TOKEN }}  # PAT from service account, stored in 1Password's Infrastructure Vault
