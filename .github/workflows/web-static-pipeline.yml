name: Web Static Pipeline

on:
  pull_request:
    paths:
      - 'web-static/**'
      - '!**/infra/**'

  push:
    branches:
      - 'master'
    paths:
      - 'web-static/**'
      - '!**/infra/**'

  workflow_dispatch:
    inputs:
      service:
        description:
          The name of the web-static service to be deployed
        required: true

      environment:
        description:
          The environment to which to deploy
        required: true
        default: sandbox

      ref:
        description:
          The branch name, tag or commit sha to deploy
        required: true
        default: master

      brand:
        description:
          Brand to deploy
        required: false

      force-rebuild:
        description:
          Disable the use of cached resources
        required: false
        default: "false"

jobs:
  init:
    name: Init
    runs-on: ubuntu-20.04

    outputs:
      app: ${{ steps.validate.outputs.app }}
      appType: ${{ steps.cicd-config.outputs.appType }}
      branch: ${{ steps.cicd-config.outputs.branch }}
      bucket: ${{ steps.cicd-config.outputs.bucket }}
      cloudfrontDistributionId: ${{ steps.cicd-config.outputs.cloudfrontDistributionId }}
      commit: ${{ steps.cicd-config.outputs.commit }}
      deploy: ${{ steps.cicd-config.outputs.deploy }}
      destinationPath: ${{ steps.render-variables-in-cicd-strings.outputs.destinationPath }}
      image-envs: ${{ steps.cicd-config.outputs.image-envs }}
      image-name: ${{ steps.cicd-config.outputs.image-name }}
      image-tag-env: ${{ steps.cicd-config.outputs.image-tag-env }}
      language: ${{ steps.detect.outputs.language }}
      module: ${{ steps.cicd-config.outputs.module }}
      noCacheFiles: ${{ steps.cicd-config.outputs.noCacheFiles }}
      path: ${{ steps.validate.outputs.path }}
      post-deploy-tasks: ${{ steps.post-deploy-tasks.outputs.targets }}
      pre-build-tasks: ${{ steps.pre-build-tasks.outputs.targets }}
      tasks: ${{ steps.configure.outputs.targets }}
      version: ${{ steps.detect.outputs.version }}

    steps:

      - id: checkout
        name: Checkout
        uses: actions/checkout@v2
        with:
          fetch-depth: 0
          ref: ${{ github.event.inputs.ref }}

      - id: dispatch
        if: github.event_name == 'workflow_dispatch'
        name: Dispatch parameters
        run: |
          echo "Workflow dispatch parameters"
          echo "Service:     ${{ github.event.inputs.service }}"
          echo "Environment: ${{ github.event.inputs.environment }}"
          echo "Brand:       ${{ github.event.inputs.brand }}"
          echo "Github ref:  ${{ github.event.inputs.ref }}"

      - id: changes
        if: github.event_name != 'workflow_dispatch'
        name: Check for new or updated files
        uses: trilom/file-changes-action@a6ca26c14274c33b15e6499323aac178af06ad4b  # v1.2.4

      - id: validate
        name: Validate only one service is updated at the time
        uses: ./.github/actions/validate-changes
        with:
          path: web-static/${{ github.event.inputs.service }}

      - id: detect
        name: Detect application's language and version
        uses: ./.github/actions/detect-language
        with:
          path: ${{ steps.validate.outputs.path }}

      - id: cicd-config
        name: CI/CD configurations
        uses: ./.github/actions/cicd-configurations
        with:
          path: ${{ steps.validate.outputs.path }}
          event: ${{ github.event_name }}
          environment: ${{ github.event.inputs.environment }}
          ref: ${{ github.event.inputs.ref }}

      - id: render-variables-in-cicd-strings
        name: Render variables in CI/CD strings
        run: |
          set -x

          DESTINATION_PATH="$(echo "${{ steps.cicd-config.outputs.destinationPath }}" | sed "s/{brand}/${{ github.event.inputs.brand }}/g")"
          echo "::set-output name=destinationPath::${DESTINATION_PATH}"

      - id: pre-build-tasks
        name: Configure install and pre build targets
        uses: ./.github/actions/configure-targets
        with:
          path: ${{ steps.validate.outputs.path }}
          language: ${{ steps.detect.outputs.language }}
          install: true
          targets:
            lint
            type-check
            test-contract
            test-coverage
            test-e2e
            test-integration
            test-smoke
            test-unit

      - id: post-deploy-tasks
        name: Configure post deploy targets
        uses: ./.github/actions/configure-targets
        with:
          path: ${{ steps.validate.outputs.path }}
          language: ${{ steps.detect.outputs.language }}
          targets:
            test-smoke

      - id: upload-configuration
        name: Upload configuration files
        uses: actions/upload-artifact@v2
        with:
          name: ${{ steps.validate.outputs.app }}-config
          path: |
            ${{ steps.validate.outputs.path }}/Makefile

  pre-build:
    if: needs.init.outputs.pre-build-tasks != '[]'
    name: Pre build
    needs: [
      init,
    ]
    runs-on: ${{ matrix.target.runner }}

    strategy:
      matrix:
        target: ${{ fromJson(needs.init.outputs.pre-build-tasks) }}

    steps:
      - id: checkout
        name: Checkout
        uses: actions/checkout@v2
        with:
          ref: ${{ github.event.inputs.ref }}

      - id: retrieve-configuration
        name: Download configuration files
        uses: actions/download-artifact@v2
        with:
          name: ${{ needs.init.outputs.app }}-config
          path: ${{ needs.init.outputs.path }}

      - id: retrieve-secrets
        name: Retrieve '.npmrc', '.pypirc' and 'pip.conf' files
        working-directory: ${{ needs.init.outputs.path }}
        run: |
          echo "${{ secrets.NPMRC }}" > .npmrc

      - id: setup-nodejs
        if: needs.init.outputs.language == 'nodejs'
        name: Setup NodeJS environment
        uses: actions/setup-node@v2
        with:
          node-version: ${{ needs.init.outputs.version }}

      - id: install-and-execute
        name: Install and execute task
        uses: ./.github/actions/install-and-execute
        with:
          path: ${{ needs.init.outputs.path }}
          target: ${{ matrix.target.name }}
          language: ${{ needs.init.outputs.language }}

      - id: codecov
        if: steps.install-and-execute.outputs.coverage-dir
        name: Send code coverage report
        uses: codecov/codecov-action@f32b3a3741e1053eb607407145bc9619351dc93b  # v2.1.0
        with:
          directory: ${{ steps.install-and-execute.outputs.coverage-dir }}
          token: ${{ secrets.CODECOV_TOKEN }}
          flags: webapp,${{ needs.init.outputs.app }},${{ matrix.target.name }}

  build:
    if: (success() || needs.pre-build.result == 'skipped') && needs.init.outputs.image-envs != '[]'
    name: Build
    needs: [
      init,
      pre-build,
    ]
    runs-on: ubuntu-20.04

    strategy:
      matrix:
        environment: ${{ fromJson(needs.init.outputs.image-envs) }}

    steps:
      - id: checkout
        name: Checkout
        uses: actions/checkout@v2
        with:
          ref: ${{ github.event.inputs.ref }}

      - id: retrieve-configuration
        name: Download configuration files
        uses: actions/download-artifact@v2
        with:
          name: ${{ needs.init.outputs.app }}-config
          path: ${{ needs.init.outputs.path }}

      - id: retrieve-secrets
        name: Retrieve '.npmrc', '.pypirc' and 'pip.conf' files
        working-directory: ${{ needs.init.outputs.path }}
        run: |
          echo "${{ secrets.NPMRC }}" > .npmrc

      - id: aws-prod
        name: Configure AWS credentials (PROD)
        uses: aws-actions/configure-aws-credentials@v1
        with:
          aws-access-key-id: ${{ secrets.PROD_AWS_ACCESS_KEY_ID }}
          aws-secret-access-key: ${{ secrets.PROD_AWS_SECRET_ACCESS_KEY }}
          aws-region: ${{ secrets.AWS_DEFAULT_REGION }}

      - id: ecr-login
        name: Login to ECR
        uses: aws-actions/amazon-ecr-login@v1

      - id: build-and-push
        name: Build and push container image to ECR
        uses: ./.github/actions/build-and-push-image
        with:
          path: ${{ needs.init.outputs.path }}
          environment: ${{ matrix.environment }}
          image-host: ${{ steps.ecr-login.outputs.registry }}
          image-name: web-static-artifacts
          image-tag: ${{ needs.init.outputs.commit }}
          image-tag-env: ${{ needs.init.outputs.image-tag-env }}
          image-tag-app: ${{ needs.init.outputs.app }}-${{ github.event.inputs.brand != '' && github.event.inputs.brand || 'nobrand' }}
          brand: ${{ github.event.inputs.brand }}

  deploy:
    if: needs.init.outputs.deploy != '[]'
    name: Deploy
    needs: [
      init,
      build,
    ]
    runs-on: ubuntu-20.04

    strategy:
      matrix:
        environment: ${{ fromJson(needs.init.outputs.image-envs) }}

    steps:
      - id: checkout
        name: Checkout
        uses: actions/checkout@v2
        with:
          ref: ${{ github.event.inputs.ref }}

      - id: slack-deployment-init
        name: Notify on Slack of a new deployment in progress
        uses: ./.github/actions/slack-notifications
        with:
          app: ${{ needs.init.outputs.app }}
          appType: ${{ needs.init.outputs.appType }}
          brand: ${{ github.event.inputs.brand }}
          branch: ${{ needs.init.outputs.branch }}
          commit: ${{ needs.init.outputs.commit }}
          environment: ${{ matrix.environment }}
          event: ${{ github.event_name }}
          pull-id: ${{ needs.init.outputs.pull-id }}
          type: static webapp
          user: ${{ github.actor }}
          webhook-url: ${{ secrets.SLACK_WEBHOOK_URL_DEPLOYS_CHANNEL }}
          status: init

      - id: aws-prod
        name: Configure AWS credentials (PROD)
        uses: aws-actions/configure-aws-credentials@v1
        with:
          aws-access-key-id: ${{ secrets.PROD_AWS_ACCESS_KEY_ID }}
          aws-secret-access-key: ${{ secrets.PROD_AWS_SECRET_ACCESS_KEY }}
          aws-region: ${{ secrets.AWS_DEFAULT_REGION }}

      - id: ecr-login
        name: Login to ECR
        uses: aws-actions/amazon-ecr-login@v1

      - id: extract-web-static
        name: Extract account assets from container image
        uses: ./.github/actions/extract-container-assets
        with:
          path: ${{ needs.init.outputs.path }}
          artifacts: /opt/dist
          image-host: ${{ steps.ecr-login.outputs.registry }}
          image-name: web-static-artifacts
          image-tag: ${{ needs.init.outputs.commit }}
          image-tag-env: ${{ needs.init.outputs.image-tag-env }}
          image-tag-app: ${{ needs.init.outputs.app }}-${{ github.event.inputs.brand != '' && github.event.inputs.brand || 'nobrand' }}
          environment: ${{ matrix.environment }}

      - id: aws-dev-artifacts-artifacts
        if: matrix.environment != 'prod'
        name: Configure AWS credentials (DEV)
        uses: aws-actions/configure-aws-credentials@v1
        with:
          aws-access-key-id: ${{ secrets.DEV_AWS_ACCESS_KEY_ID }}
          aws-secret-access-key: ${{ secrets.DEV_AWS_SECRET_ACCESS_KEY }}
          aws-region: ${{ secrets.AWS_DEFAULT_REGION }}

      - id: backup-previous-app-files
        name: Backup previous application files
        run: |
          set -x

          ENVIRONMENT="$(echo "${{ matrix.environment }}" | sed "s/sandbox/sbox/")"
          BUCKET="$(echo "${{ needs.init.outputs.bucket }}" | sed "s/{environment}/${ENVIRONMENT}/g")"

          # Append /current to source if app type is account
          aws s3 sync \
            "${BUCKET}/${{ needs.init.outputs.destinationPath }}${{ needs.init.outputs.appType == 'account' && '/current' || '' }}" \
            "${BUCKET}/builds/${{ needs.init.outputs.destinationPath }}/previous-version" \
            --metadata-directive COPY \
            --delete

      - id: copy-app-files-to-s3
        name: Copy app files to S3
        uses: ./.github/actions/copy-web-static-files-to-s3
        with:
          path: ${{ needs.init.outputs.path }}/tmp/opt/dist
          environment: ${{ matrix.environment }}
          bucket: ${{ needs.init.outputs.bucket }}
          destination-path: ${{ needs.init.outputs.destinationPath }}
          commit: ${{ needs.init.outputs.commit }}
          app-type: ${{ needs.init.outputs.appType }}
          no-cache-file-patterns: ${{ needs.init.outputs.noCacheFiles }}

      - id: sync-files-to-live-location
        name: Sync files into live application location
        run: |
          set -x

          ENVIRONMENT="$(echo "${{ matrix.environment }}" | sed "s/sandbox/sbox/")"
          BUCKET="$(echo "${{ needs.init.outputs.bucket }}" | sed "s/{environment}/${ENVIRONMENT}/g")"

          aws s3 sync \
            "${BUCKET}/builds/${{ needs.init.outputs.destinationPath }}/${{ needs.init.outputs.commit }}" \
            "${BUCKET}/${{ needs.init.outputs.destinationPath }}${{ needs.init.outputs.appType == 'account' && '/current' || '' }}" \
            --delete \
            --acl public-read \
            --metadata-directive COPY

      - id: create-cloudfront-invalidation-web-static
        name: Create Cloudfront invalidation
        if: needs.init.outputs.cloudfrontDistributionId
        run: |
          set -x

          aws cloudfront create-invalidation \
            --distribution-id "${{ needs.init.outputs.cloudfrontDistributionId }}" \
            --paths "/${{ needs.init.outputs.destinationPath }}${{ needs.init.outputs.appType == 'account' && '/current' || '' }}"

      - id: newrelic-deployment-event
        name: Push deployment event to NewRelic
        uses: ./.github/actions/push-metric-to-newrelic
        with:
          accountId: ${{ secrets.NEW_RELIC_ACCOUNT_ID }}
          appName: ${{ needs.init.outputs.app }}
          appType: static webapp
          commitSha: ${{ needs.init.outputs.commit }}
          deployType: normal
          environment: ${{ matrix.environment }}
          insightsApiKey: ${{ secrets.NEW_RELIC_INSIGHTS_API_KEY }}
          user: ${{ github.actor }}
        continue-on-error: true

      - id: slack-deployment-success
        if: success()
        name: Notify the deployment was successful
        uses: ./.github/actions/slack-notifications
        with:
          app: ${{ needs.init.outputs.app }}
          appType: ${{ needs.init.outputs.appType }}
          brand: ${{ github.event.inputs.brand }}
          environment: ${{ matrix.environment }}
          webhook-url: ${{ secrets.SLACK_WEBHOOK_URL_DEPLOYS_CHANNEL }}
          status: success

      - id: slack-deployment-error
        if: always() && failure() && needs.deploy.outputs.rollback-revision == ''
        name: Notify the deployment failed
        uses: ./.github/actions/slack-notifications
        with:
          app: ${{ needs.init.outputs.app }}
          appType: ${{ needs.init.outputs.appType }}
          brand: ${{ github.event.inputs.brand }}
          environment: ${{ matrix.environment }}
          webhook-url: ${{ secrets.SLACK_WEBHOOK_URL_DEPLOYS_CHANNEL }}
          status: error

  post-deploy:
    if: needs.init.outputs.post-deploy-tasks != '[]'
    name: Post deploy
    needs: [
      init,
      deploy,
    ]

    runs-on: ubuntu-20.04
    strategy:
      matrix:
        environment: ${{ fromJson(needs.init.outputs.environments) }}
        target: ${{ fromJson(needs.init.outputs.post-deploy-tasks) }}

    steps:
      - id: checkout
        name: Checkout
        uses: actions/checkout@v2
        with:
          ref: ${{ github.event.inputs.ref }}

      - id: retrieve-configuration
        name: Download configuration files
        uses: actions/download-artifact@v2
        with:
          name: ${{ needs.init.outputs.app }}-config
          path: ${{ needs.init.outputs.path }}

      - id: retrieve-secrets
        name: Retrieve '.npmrc', '.pypirc' and 'pip.conf' files
        working-directory: ${{ needs.init.outputs.path }}
        run: |
          echo "${{ secrets.NPMRC }}" > .npmrc

      - id: setup-nodejs
        if: needs.init.outputs.language == 'nodejs'
        name: Setup NodeJS environment
        uses: actions/setup-node@v2
        with:
          node-version: ${{ needs.init.outputs.version }}

      - id: install-and-execute
        name: Install and execute task
        uses: ./.github/actions/install-and-execute
        with:
          path: ${{ needs.init.outputs.path }}
          target: ${{ matrix.target.name }}
          language: ${{ needs.init.outputs.language }}

      - id: codecov
        if: steps.install-and-execute.outputs.coverage-dir
        name: Send code coverage report
        uses: codecov/codecov-action@f32b3a3741e1053eb607407145bc9619351dc93b  # v2.1.0
        with:
          directory: ${{ steps.install-and-execute.outputs.coverage-dir }}
          token: ${{ secrets.CODECOV_TOKEN }}
          flags: webapp,${{ needs.init.outputs.app }},${{ matrix.target.name }}

  rollback:
    if: failure()
    name: Rollback
    needs: [
      init,
      deploy,
      post-deploy,
    ]
    runs-on: ubuntu-20.04

    strategy:
      matrix:
        environment: ${{ fromJson(needs.init.outputs.environments) }}

    steps:
      - id: checkout
        name: Checkout
        uses: actions/checkout@v2
        with:
          ref: ${{ github.event.inputs.ref }}

      - id: aws-prod-rollback
        if: matrix.environment == 'prod'
        name: Configure AWS credentials (PROD)
        uses: aws-actions/configure-aws-credentials@v1
        with:
          aws-access-key-id: ${{ secrets.PROD_AWS_ACCESS_KEY_ID }}
          aws-secret-access-key: ${{ secrets.PROD_AWS_SECRET_ACCESS_KEY }}
          aws-region: ${{ secrets.AWS_DEFAULT_REGION }}

      - id: aws-dev-rollback
        if: matrix.environment != 'prod'
        name: Configure AWS credentials (DEV)
        uses: aws-actions/configure-aws-credentials@v1
        with:
          aws-access-key-id: ${{ secrets.DEV_AWS_ACCESS_KEY_ID }}
          aws-secret-access-key: ${{ secrets.DEV_AWS_SECRET_ACCESS_KEY }}
          aws-region: ${{ secrets.AWS_DEFAULT_REGION }}

      - id: restore-previous-app-files
        name: Restore previous application files
        run: |
          set -x

          ENVIRONMENT="$(echo "${{ matrix.environment }}" | sed "s/sandbox/sbox/")"
          BUCKET="$(echo "${{ needs.init.outputs.bucket }}" | sed "s/{environment}/${ENVIRONMENT}/g")"

          # Append /current to source if app type is account
          aws s3 sync \
            "${BUCKET}/builds/${{ needs.init.outputs.destinationPath }}/previous-version" \
            "${BUCKET}/${{ needs.init.outputs.destinationPath }}${{ needs.init.outputs.appType == 'account' && '/current' || '' }}" \
            --metadata-directive COPY \
            --delete

      - id: newrelic-deployment-event
        name: Push deployment event to NewRelic
        uses: ./.github/actions/push-metric-to-newrelic
        with:
          accountId: ${{ secrets.NEW_RELIC_ACCOUNT_ID }}
          appName: ${{ needs.init.outputs.app }}
          appType: static webapp
          commitSha: ${{ needs.init.outputs.commit }}
          deployType: rollback
          environment: ${{ matrix.environment }}
          insightsApiKey: ${{ secrets.NEW_RELIC_INSIGHTS_API_KEY }}
          user: ${{ github.actor }}
        continue-on-error: true
