import argparse
import json
import os
import re
import sys

sys.path.append(os.path.dirname(__file__) + "/../helpers")
from helpers import (  # noqa E402
    get_app_name,
    get_commit_sha,
    get_commit_sha_from_ref,
    get_file_contents,
    get_git_ref,
    get_git_ref_from_sha,
    get_pull_id,
    get_recent_commit_messages,
    set_output,
)

ALLOWED_ENVIRONMENTS = ["prod", "staging", "sandbox", "dev"]
DEFAULT_STRATEGY = {"prod": "master", "staging": "master", "sandbox": "*"}


def execute():
    """
    Parses the script arguments and executes the main action function.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--path", default=".")
    parser.add_argument("--event", default="pull_request")
    parser.add_argument("--environment", default="")
    parser.add_argument("--ref", default="")

    action(**vars(parser.parse_args()))


def action(environment="", ref="", event="pull_request", path="."):
    """
    Retrieves multiple parameters from 'ci.json' file and Github environment.
    """
    if event == "workflow_dispatch":
        if re.match("[0-9a-f]{40}", ref):
            set_output("commit", ref)
            set_output("branch", get_git_ref_from_sha(ref))
        else:
            set_output("branch", ref)
            set_output("commit", get_commit_sha_from_ref(ref))
    else:
        set_output("branch", get_git_ref())
        set_output("commit", get_commit_sha())
        set_output("pull-id", get_pull_id())
    set_output("changelog", get_recent_commit_messages())

    app_path = path.rstrip("/")
    app_name = get_app_name(app_path)

    ci = get_file_contents(
        f"{app_path}/ci.json", type="json", error=False
    )

    set_output("app", app_name)
    set_output("service", ci.get("serviceName"))
    set_output("compose", ci.get("compose", ci.get("serviceName")))
    set_output("cluster", ci.get("cluster"))
    set_output("module", ci.get("module"))
    set_output("bucket", ci.get("bucket"))
    set_output("destinationPath", ci.get("destinationPath"))
    set_output("appType", ci.get("appType"))
    set_output("noCacheFiles", json.dumps(ci.get("noCacheFiles")))
    set_output("cloudfrontDistributionId", ci.get("cloudfrontDistributionId"))
    set_output("image", ci.get("imageName"))

    set_output("service-urls", json.dumps(ci.get("serviceUrls", {})))

    try:
        if environment:
            # Deploy to the environment sent in a workflow dispatch
            if environment not in ALLOWED_ENVIRONMENTS:
                raise ValueError

            environments = [environment]

        else:
            # Set deploy environments according to release strategy and branch
            environments = []
            strategy = DEFAULT_STRATEGY
            release = ci.get("releaseStrategy", {})
            refs = [get_git_ref(), "*"]
            for env in strategy:
                if release.get(env) == "automatic" and strategy[env] in refs:
                    environments.append(env)

        set_output("environments", json.dumps(environments))

    except ValueError:
        sys.exit("# Invalid environment name.")

    except AttributeError:
        sys.exit("# Invalid release strategy configuration.")

    image_envs = ["prod"]
    if ci.get("requireEnvironmentContainers", False):
        image_envs = environments
        set_output("image-tag-env", "true")
    set_output("image-envs", json.dumps(image_envs))

    set_output(
        "artifacts", ci.get("cdnArtifactPath") or ci.get("buildArtifactPath")
    )

    set_output(
        "new-relic",
        json.dumps(ci.get("newRelic", {}).get("applicationId", {})),
    )


if __name__ == "__main__":
    execute()
