# Github Actions: CI/CD Configurations

As part of the Surfline's CI/CD Pipeline, the purpose of this custom Github Action is to read the `ci.json` file from an application and fetch its information and decorate it in a way a Github Action's workflow understands and can be outputted to use in other actions.

It also retrieves information from Github environment variables and logs.

## Table of Contents <!-- omit in toc -->

- [How it works](#how-it-works)
- [Execution](#execution)
  - [From a workflow dispatch](#from-a-workflow-dispatch)
  - [From a workflow inside a pipeline](#from-a-workflow-inside-a-pipeline)
- [Development](#development)
  - [Installation](#installation)
  - [Type check](#type-check)
  - [Format](#format)
  - [Linting](#linting)
  - [Testing](#testing)

## How it works

The `action.py` script receives a path where to look for the the `ci.json` file will look for ECS configurations, release strategies and more.

It depends on the parameters sent from the `action.yml` file when running from a Github Actions pipeline, but it can be also executed directly for tests purposes.

## Execution

The action is being called from one or more or the workflow files stored in the `.github/workflows/` folder at the root of this repository, setting the current path of the application to test, build and deploy.

In the case a workflow is being dispatched from the Github Actions UI, the action receives the `environment` parameter that will override any setting coming from the `releaseStrategy`.

```yaml
      - id: cicd-config
        name: CI/CD configurations
        uses: ./.github/actions/cicd-configurations
        with:
          path: ${{ steps.validate.outputs.path }}
          environment: ${{ github.event.inputs.environment }}
```

If you want to execute the script locally, you can simulate two different cases:

### From a workflow dispatch

```bash
python action.py \
  --environment sandbox \
  --path services/simple-service
```

### From a workflow inside a pipeline

```bash
python action.py \
  --path services/simple-service
```

## Development

A `Makefile` file contains several targets to ease and structure the development of this action.

### Installation

To install the action locally we make use of Conda DevEnv. The installation target is present in `Makefile` but you'll need to activate the Conda Environment after creation:

```bash
make install
conda activate cicd-pipeline-configurations
```

### Type check

The `mypy` application makes an exhaust audit of the code, so if something fails at this stage, it probably might fail when calling the linting and formatting targets too:

```bash
make type-check
```

### Format

The formatting target uses `autoflake` and `isort` to check the code, but be careful and take into account that it removes unused variables and imports, and the format settings may not correspond with your code styling:

```bash
make format
```

### Linting

Make sure your code is correctly linted when making changes:

```bash
make lint
```

### Testing

We created a basic set of tests for this action, that you can find in `test.py` and can be executed running:

```bash
make test-unit
```

If you need to add more test files, you may want to update the `test-unit` target in `Makefile`.
