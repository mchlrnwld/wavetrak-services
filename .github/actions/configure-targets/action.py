import argparse
import json
import os
import sys

import yaml

sys.path.append(os.path.dirname(__file__) + "/../helpers")
from helpers import (  # noqa E402
    get_app_name,
    get_file_contents,
    put_file_contents,
    set_output,
)

DEFAULT_RUNNER = "ubuntu-20.04"
SELF_HOSTED_MARKER = "RUNNER=self-hosted"


def execute():
    """
    Parses the script arguments and executes the main action function.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--language")
    parser.add_argument("--path", default=".")
    parser.add_argument("--install", default=True)
    parser.add_argument("--package", default=False)
    parser.add_argument("--targets", default="")

    action(**vars(parser.parse_args()))


def action(
    language,
    install=True,
    package=False,
    path=".",
    targets="",
):
    """
    Configure install and testing targets in a Makefile file.
    """
    action_path = os.path.abspath(os.path.dirname(__file__))
    action_output = f"{action_path}/outputs.json"

    app_path = path.rstrip("/").strip('"')
    app_name = get_app_name(app_path)

    try:
        # Retrieve 'ci.json' and 'package.json' files content
        file = f"{app_path}/ci.json"
        ci = get_file_contents(file, type="json", error=False)

        file = f"{app_path}/package.json"
        pk = get_file_contents(file, type="json", error=False)

    except json.decoder.JSONDecodeError as err:
        sys.exit(f"# Error parsing JSON file '{file}':\n  {err}")

    targets = targets.split(",") if "," in targets else targets.split()

    # No targets to configure
    if not install and not len(targets):
        set_output("targets", "[]")
        put_file_contents(action_output, content=json.dumps({"targets": "[]"}))
        return

    # Store the Makefile content in a string
    makefile = get_file_contents(f"{app_path}/Makefile", error=False)

    # Configure the install target
    if install:

        # Override any previous install target for Python and NodeJS apps
        # TO-DO: Revisit this code block in case we find applications with
        # custom installation commands
        if language in ["python", "nodejs"]:
            makefile = makefile.replace("install:", "install-overridden:")

        # Generic target template
        target_file = "Makefile.target.j2"

        if language == "python":
            devenv_file = f"{app_path}/environment.devenv.yml"
            if os.path.exists(devenv_file):
                # Make sure the environment's name is the application's name
                devenv = get_file_contents(devenv_file, type="yml")
                devenv["name"] = app_name
                put_file_contents(devenv_file, content=yaml.dump(devenv))
                target_file = "Makefile.install.python-devenv.j2"
            else:
                target_file = "Makefile.install.python.j2"

            env_file = f"{app_path}/environment.yml"
            if not os.path.exists(env_file):
                # Create a basic 'environment.yml' file to use Conda DevEnv
                tpl_file = f"{action_path}/environment.yml.j2"
                template = get_file_contents(tpl_file)
                template = template.replace("{{ service-name }}", app_name)
                put_file_contents(env_file, content=template)

        if language == "nodejs":
            target_file = (
                "Makefile.install.nodejs.j2"
                if package != "true"
                else "Makefile.install.nodejs-package.j2"
            )

        if language == "other":
            target_file = "Makefile.install.docker.j2"

        makefile += add_makefile_target(
            template=get_file_contents(f"{action_path}/{target_file}"),
            target="install",
            command="@echo 'Not configured'",
            appName=app_name,
        )

    # Configure linting and testing targets
    available = []

    # Template for targets
    suffix = ".docker" if language == "other" else ""
    target_tpl = get_file_contents(f"{action_path}/Makefile.target{suffix}.j2")

    for target in targets:

        runner = ""
        target = target.strip()

        # Use a custom template for the target, if exists
        custom_tpl = get_file_contents(
            f"{action_path}/Makefile.{target}.j2",
            error=False,
            content=target_tpl
        )

        pos = makefile.find(f"{target}:")
        if pos >= 0:
            # Target already exists in Makefile
            start = pos + len(target)
            finish = pos + len(target) + len(SELF_HOSTED_MARKER) + 5
            runner = (
                "self-hosted"
                if SELF_HOSTED_MARKER in makefile[start:finish]
                else DEFAULT_RUNNER
            )
            if language == "other":
                makefile += add_makefile_target(
                    template=custom_tpl,
                    target=target,
                    appName=app_name,
                )

        else:
            # Search in 'ci.json' and 'package.json' files for the command
            command = search_command(target, ci, pk)
            if command:
                runner = (
                    "self-hosted"
                    if SELF_HOSTED_MARKER in command["exec"]
                    else DEFAULT_RUNNER
                )
                self_hosted_marker = (
                    f"{SELF_HOSTED_MARKER} "
                    if runner == "self-hosted"
                    and SELF_HOSTED_MARKER not in command["call"]
                    else ""
                )
                makefile += add_makefile_target(
                    template=custom_tpl,
                    target=target,
                    command=self_hosted_marker + command["call"],
                    appName=app_name,
                )

        if runner:
            available.append({"name": target, "runner": runner})

    set_output("targets", json.dumps(available))
    put_file_contents(action_output, content=json.dumps(available))

    print(f"> makefile: \n{makefile}")
    put_file_contents(f"{app_path}/Makefile", content=makefile)


def search_command(name, ci, package):
    """
    Searches in 'ci.json' and 'package.json' files for the existence of scripts
    with the name passed by arguments.
    The functions returns an object of two elements:
    - call: The command to call from the Makefile target
    - exec: The actual command to be executed, that can be the same as the call
    """
    commands = [
        {
            "call": ci.get("tests", {}).get(name.replace("test-", "")),
            "exec": ci.get("tests", {}).get(name.replace("test-", "")),
        },
        {
            "call": f"npm run {name}",
            "exec": package.get("scripts", {}).get(name),
        },
        {
            "call": f"npm run {name.replace('-', ':')}",
            "exec": package.get("scripts", {}).get(name.replace('-', ':')),
        },
    ]
    for command in commands:
        if command["exec"]:
            return command


def add_makefile_target(template, target, command="", appName=""):
    """
    Replace Jinja placeholders in provided template
    """
    template = template.replace("{{ target }}", target)
    template = template.replace("{{ command }}", command)
    template = template.replace("{{ app-name }}", appName)
    return f"\n{template}"


if __name__ == "__main__":
    execute()
