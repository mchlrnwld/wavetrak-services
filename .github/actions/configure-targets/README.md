# Github Actions: Configure Targets

As part of the Surfline's CI/CD Pipeline, the purpose of this custom Github Action is to parse over multiple files into the application's folder parse the installation, testing, linting and build scripts in order to recreate a `Makefile` file with the configured targets.

## Table of Contents <!-- omit in toc -->

- [How it works](#how-it-works)
- [Execution](#execution)
- [Development](#development)
  - [Installation](#installation)
  - [Type check](#type-check)
  - [Format](#format)
  - [Linting](#linting)
  - [Testing](#testing)

## How it works

The `action.py` script receives a path where to look for `ci.json`, `package.json` and any existing `Makefile` files to read and create new targets from several templates, according to the application's language and settings.

It depends on the parameters sent from the `action.yml` file when running from a Github Actions pipeline, but it can be also executed directly for tests purposes.

## Execution

The action is being called from one or more or the workflow files stored in the `.github/workflows/` folder at the root of this repository.

It has the ability to receive a list of targets to run the tasks based on "pre-build" and "post-deploy" configured as targets, so you can call this action several times with different targets as arguments, but the final result is one unique `Makefile` file.

For Javascript applications, we have a specific NPM configuration to retrieve dependency packages. This parameter is already stored in Github Secrets but you may want to retrieve that content from AWS Parameter Store or AWS Secrets Manager in order to build NodeJS projects.

```yaml
      - id: pre-build-targets
        name: Configure install and pre build targets
        uses: ./.github/actions/configure-targets
        with:
          path: ${{ steps.validate.outputs.path }}
          language: ${{ steps.detect.outputs.language }}
          install: true
          targets:
            lint
            type-check
            test-contract
            test-coverage
            test-e2e
            test-integration
            test-unit

      - id: post-deploy-targets
        name: Configure post deploy targets
        uses: ./.github/actions/configure-targets
        with:
          path: ${{ steps.validate.outputs.path }}
          language: ${{ steps.detect.outputs.language }}
          targets:
            test-smoke
```

If you want to execute the script locally, you can run it like this:

```bash
python action.py \
  --path services/simple-service \
  --language sandbox \
  --install true \
  --targets lint,type-check,test-integration,test-unit
```

## Development

A `Makefile` file contains several targets to ease and structure the development of this action.

### Installation

To install the action locally we make use of Conda DevEnv. The installation target is present in `Makefile` but you'll need to activate the Conda Environment after creation:

```bash
make install
conda activate cicd-pipeline-configure-targets
```

### Type check

The `mypy` application makes an exhaust audit of the code, so if something fails at this stage, it probably might fail when calling the linting and formatting targets too:

```bash
make type-check
```

### Format

The formatting target uses `autoflake` and `isort` to check the code, but be careful and take into account that it removes unused variables and imports, and the format settings may not correspond with your code styling:

```bash
make format
```

### Linting

Make sure your code is correctly linted when making changes:

```bash
make lint
```

### Testing

We created a basic set of tests for this action, that you can find in `test.py` and can be executed running:

```bash
make test-unit
```

If you need to add more test files, you may want to update the `test-unit` target in `Makefile`.
