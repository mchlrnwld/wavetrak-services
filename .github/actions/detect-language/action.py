import argparse
import glob
import json
import os
import sys

sys.path.append(os.path.dirname(__file__) + "/../helpers")
from helpers import (  # noqa E402
    get_app_name,
    get_file_contents,
    put_file_contents,
    set_output
)


def execute():
    """
    Parses the script arguments and executes the main action function.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--path", default=".")

    action(**vars(parser.parse_args()))


def action(path="."):
    """
    The main function of this Github Action
    For lambda functions, check if a serverless.yml file exists
    """
    action_path = os.path.abspath(os.path.dirname(__file__))
    action_output = f"{action_path}/outputs.json"

    app_path = path.rstrip("/").strip('"')
    app_name = get_app_name(app_path)

    set_output("app", app_name)

    serverless = get_file_contents(
        f"{app_path}/serverless.yml",
        error=False,
        type="yaml"
    )
    if serverless:
        set_output("serverless", "true")
        runtime = serverless.get("provider", {}).get("runtime")
        if (
            runtime and not
            runtime.startswith("python") and not
            runtime.startswith("node")
        ):
            set_output("language", "other")
            return

    try:
        detect = get_file_contents(f"{action_path}/detect.json", type="json")

        for lang in detect:
            find = glob.glob(f"{app_path}/{lang['find']}")

            if len(find):
                for step in lang["steps"]:
                    stream = os.popen(step.replace("${APP_PATH}", app_path))
                    version = stream.read()
                    stream.close()
                    if version:
                        package_type = ("pip", "npm")[lang["name"] == "nodejs"]
                        set_output("language", lang["name"])
                        set_output("version", version.strip())
                        set_output("package-type", package_type)

                        put_file_contents(action_output, content=json.dumps({
                            "app": app_name,
                            "language": lang["name"],
                            "version": version.strip(),
                            "package-type": package_type,
                            "serverless": "true" if serverless else "false",
                        }))
                        return

    except Exception as err:
        sys.exit(err)

    put_file_contents(action_output, content=json.dumps({
        "app": app_name,
        "serverless": "true" if serverless else "false",
    }))


if __name__ == "__main__":
    execute()
