# Github Actions: Detect Language

As part of the Surfline's CI/CD Pipeline, the purpose of this custom Github Action is to parse over multiple files into the application's folder and try to figure out which language and version it was built with.

## Table of Contents <!-- omit in toc -->

- [How it works](#how-it-works)
- [Execution](#execution)
- [Development](#development)
  - [Installation](#installation)
  - [Type check](#type-check)
  - [Format](#format)
  - [Linting](#linting)
  - [Testing](#testing)

## How it works

The `action.py` script receives a path where to look for `ci.json`, `package.json` and `environment.devenv.yml` files and detect the application's language explicitly set in their configuration.

It depends on the parameters sent from the `action.yml` file when running from a Github Actions pipeline, but it can be also executed directly for tests purposes.

## Execution

The action is being called from one or more or the workflow files stored in the `.github/workflows/` folder at the root of this repository, setting the current path of the application to test, build and deploy.

```yaml
      - id: detect
        name: Detect application language and version
        uses: ./.github/actions/detect-language
        with:
          path: ${{ steps.validate.outputs.path }}

```

If you want to execute the script locally, you can run it like this:

```bash
python action.py \
  --path services/simple-service
```

## Development

A `Makefile` file contains several targets to ease and structure the development of this action.

### Installation

To install the action locally we make use of Conda DevEnv. The installation target is present in `Makefile` but you'll need to activate the Conda Environment after creation:

```bash
make install
conda activate cicd-pipeline-detect-language
```

### Type check

The `mypy` application makes an exhaust audit of the code, so if something fails at this stage, it probably might fail when calling the linting and formatting targets too:

```bash
make type-check
```

### Format

The formatting target uses `autoflake` and `isort` to check the code, but be careful and take into account that it removes unused variables and imports, and the format settings may not correspond with your code styling:

```bash
make format
```

### Linting

Make sure your code is correctly linted when making changes:

```bash
make lint
```

### Testing

We created a basic set of tests for this action, that you can find in `test.py` and can be executed running:

```bash
make test-unit
```

If you need to add more test files, you may want to update the `test-unit` target in `Makefile`.
