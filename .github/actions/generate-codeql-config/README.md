# Github Actions: Generate CodeQL Config

Dynamically generates a config file for Githubs CodeQL action based on what artifacts within the monorepo actually changed.

## How it works

The `action.py` script receives a list of files created or updated on the current commit, parsing the application folders and generating a CodeQL config file containing the list of folders to scan.

It depends on the parameters sent from the `action.yml` file when running from a Github Actions pipeline, but it can be also executed directly for tests purposes.

## Execution

The action is being called from `.github/workflows/github-advanced-security.yml` folder at the root of this repository. Usually, we make use of `trilom/file-changes-action`, a 3rd party action that creates this file with the modifications and pass it to out action into the `changed-files` parameter.


## Development

`Dockerfile` is used to encapsulate runtime env for development, making setup easier, the action is run natively on Github and not within this specific container.

```bash
# From repo root.
make -C .github/actions/generate-codeql-config/ build
make -C .github/actions/generate-codeql-config/ enter

python action.py \
  --changed-files files.example.json \
  --output-path codeql-config.yml
```
