#!/usr/bin/env python3

import argparse
import json
import logging
import os
import sys

import yaml

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger()

# We will only scan artifacts in these dirs.
ARTIFACT_SEARCH_DIRS = [
    'functions',
    'services',
    'jobs',
    'packages',
    'web-static',
    'common',
]


# Used to determine which languages should be enabled
# for this CodeQL scan.
CODEQL_LANGUAGE_PATTERNS = {'python': ['.py'], 'javascript': ['.js', '.ts']}


def file_get_contents(filename):
    if os.path.isfile(filename):
        with open(filename) as f:
            return f.read()

    return False


def set_output(name, value=""):
    """
    A simple wrapper that outputs a variable a key/name value to a format that
    Github Actions understands. It also works as a debugging tool when testing
    the gathered information from a shared/custom action.
    """
    if os.environ.get("GITHUB_ACTIONS"):
        print(f"::set-output name={name}::{value if value else ''}")
    print(f"> {name}: {value if value else ''}")


def main(argv):
    parser = argparse.ArgumentParser(description='Generate CodeQL config.')

    parser.add_argument(
        '--changed-files',
        type=str,
        help='Path to json file containing array of \
            files that have changed in this diff.',
    )

    parser.add_argument(
        '--removed-files',
        type=str,
        help='Path to json file containing array of \
            files that have been removed in this diff.',
    )

    parser.add_argument(
        '--output-path',
        type=str,
        help='Where to save CodeQL config file',
    )

    parser = parser.parse_args()

    logger.info(f'Load changed files from: {parser.changed_files}')

    try:
        changed_files = json.loads(file_get_contents(parser.changed_files))
        removed_files = json.loads(file_get_contents(parser.removed_files))
    except ValueError:
        logger.error(
            f'Unable to load and parse json from: {parser.changed_files}'
        )
        return

    logger.info('Loaded changed and rmeoved file lists.')

    files = [file for file in changed_files if file not in removed_files]

    # We should only allow 1 artifact to change at once but the code
    # handles multiple incase we change that rule in the future.
    artifacts_changed = []
    languages = []
    for file in files:
        # Ensure first dir matches ARTIFACT_SEARCH_DIRS.
        parts = file.split(os.sep)

        # Not enough parts then it's not an artifact we care about.
        # Has change been made to an artifact type we want to scan?
        if len(parts) >= 2 and parts[0] in ARTIFACT_SEARCH_DIRS:
            artifact_path = os.path.join(parts[0], parts[1])

            if artifact_path not in artifacts_changed:
                logger.info(f'Identified changes in: {artifact_path}')

                artifacts_changed.append(artifact_path)

            # Determine language.
            ext = os.path.splitext(file)[1]

            for lang, extensions in CODEQL_LANGUAGE_PATTERNS.items():
                if ext in extensions and lang not in languages:
                    logger.info(f'CodeQL enabled for: {lang}')
                    languages.append(lang)

    # Generate config yaml and add artifacts_changed as include paths.
    config = dict(
        name='CodeQL config', paths=artifacts_changed, languages=languages
    )

    paths = ', '.join(artifacts_changed)
    logger.info(f'CodeQL will scan: {paths}')
    logger.info(f'Writing config to: {parser.output_path}')

    with open(parser.output_path, 'w') as outfile:
        yaml.dump(config, outfile, default_flow_style=False)

    shouldRun = False
    if len(artifacts_changed) > 0 and len(languages) > 0:
        shouldRun = True
    else:
        logger.info(
            'Won\'t run, either no artifacts changed or no changes to \
            relevant source code. '
        )

    # Print/set output values.
    set_output('config-path', parser.output_path)
    set_output('should-run', 'true' if shouldRun else 'false')
    set_output('languages', ', '.join(languages))


main(sys.argv)
