name: Generate CodeQL Config
description: |
  Dynamically generates a config file for Githubs CodeQL action
  based on what artifacts within the monorepo actually changed.
author: Gavin Cooper
branding:
  icon: droplet
  color: blue

inputs:
  changed-files:
    description: |
      A json formatted file with a list of the changed files
      Typically, the file generated from 'trilom/file-changes-action'
    default: ${HOME}/files.json
    required: true
  removed-files:
    description: |
      A json formatted file with a list of the changed files
      Typically, the file generated from 'trilom/file-changes-action'
    default: ${HOME}/files_removed.json
    required: true

outputs:
  config-path:
    description: |
      Path to CodeQL config yaml file.
    value: ${{ steps.generate-codeql.outputs.config-path }}
  should-run:
    description: |
      Has detected changes in artifacts and thus should run CodeQL
    value: ${{ steps.generate-codeql.outputs.should-run }}
  languages:
    description: |
      Languages to run CodeQL on
    value: ${{ steps.generate-codeql.outputs.languages }}

runs:
  using: composite
  steps:
    - id: setup-python
      name: Setup Python environment
      uses: actions/setup-python@v2
      with:
        python-version: '3.7'
    - id: install-pip-dependencies
      name: Install dependencies
      shell: bash
      run: |
        python -m pip install --upgrade pip
        pip install -r ${GITHUB_ACTION_PATH}/requirements.txt
    - id: generate-codeql
      name: Generate CodeQL Config
      shell: bash
      run: |
        python ${GITHUB_ACTION_PATH}/action.py \
          --changed-files "${{ inputs.changed-files }}" \
          --removed-files "${{ inputs.removed-files }}" \
          --output-path ${GITHUB_WORKSPACE}/codeql-config.yml
