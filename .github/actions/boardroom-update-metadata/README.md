# Boardroom Update Metadata

This action scans the monorepo for artifacts and metadata. It aggregates the metadata into a json file and ships it to S3 (Bucket: wt-boardroom-metadata). A site rebuild is then triggered for Boardroom in Cloudflare pages. This is a static site in the repo: [https://github.com/Surfline/boardroom](https://github.com/Surfline/boardroom)(Cloudflare page doesn't support monorepos yet). When the project is built in Cloudflare Pages it will get the metadata from S3 and generate the HTML. The site is available at: [https://boardroom.pages.dev/](https://boardroom.pages.dev/).

## Development

Developing Github actions is a bit of a pain. The action is made of 2 parts; 1) action yaml and 2) python script. The python script is executed by the action and that can be developed locally using this process:

```bash
# From repo root.
make -C .github/actions/boardroom-update-metadata/ build
make -C .github/actions/boardroom-update-metadata/ enter

python main.py --force-refresh --repo-path /github/workspace --dry-run
```

Dependencies from `requirements.txt` are installed by the GH action. Dependencies within the development environment are installed from `requirements.txt` and `requirements.dev.txt` this keeps the execution time of the GH action shorter, whilst allow using to specifiy requirements like `flake8` for development.

To test and develop the action a change should be made to the action YAML file and commited and then pushed to Github. The best workflow for this is to:

```bash
# Create a feature branch
git checkout -b some-branch

# Make changes to boardroom-update-workflow.yml.

git commit -a -m "Some changes."
git push

# View workflow execution: https://github.com/Surfline/wavetrak-services/actions
```

## PIP vs Conda

I've chosen not to use Conda and the python environment setup we [generally use](https://github.com/Surfline/styleguide/tree/master/python) because Conda env setup is about 10x slower in Github Actions than using the installed python version and pip within the default Github actions image.
