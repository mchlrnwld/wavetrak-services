#!/usr/bin/env python3

import argparse
import glob
import json
import logging
import os
import sys

import boto3
from codeowners import CodeOwners

from lib.Artifact import Artifact
from lib.utils import file_get_contents

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger()

ARTIFACT_SEARCH_DIRS = [
    'functions',
    'services',
    'jobs',
    'packages',
    'web-static',
    'common',
]


def get_all_artifacts(search_dir):
    """
    Iterates subfolders of monorepo to find all artifacts root dirs
    and returns them.

    Args:
        search_dir: root dir of monorepo

    Returns:
        List of service root dirs
        i.e. [./wavetrak-services/services/cameras-service/]
    """
    output_dirs = []
    for dir in ARTIFACT_SEARCH_DIRS:
        search_path = os.path.join(search_dir, dir, '*/')
        logger.debug(f'Searching: {search_path} for artifacts')
        output_dirs = output_dirs + glob.glob(search_path)

    return output_dirs


def get_metadata_for_artifact(owners, repo_path, artifact_path):
    """
    Builds metadata dict for artifact at the path passed.

    Args:
        artifact_path: path to artifact dir.

    Returns:
        Dict of artifact metadata. Docs in Boardroom
        README at: ./services/boardroom/README.
    """
    # Get type of artifact
    # Get name of artifact
    # Get owning team of artifact.
    logger.info(f'Get metadata for: {artifact_path}')

    art = Artifact(repo_path, artifact_path)

    metadata = {
        'serviceName': art.get_service_name(),
        'type': art.get_artifact_type(),
        'newRelic': {'applicationId': art.get_newrelic_application_id()},
        'pagerDutyServiceId': art.get_pagerduty_service_id(),
    }

    relative_artifact_path = art.get_relative_artifact_path()
    owner = owners.of(relative_artifact_path)

    metadata['relativeArtifactPath'] = remove_lead_and_trail_slash(
        relative_artifact_path
    )

    if owner:
        o = owner[0]

        metadata['owner'] = {'type': o[0], 'value': o[1]}
    else:
        logger.info('No Github owner established for this artifact.')

    return metadata


def remove_lead_and_trail_slash(s):
    if s.startswith('/'):
        s = s[1:]
    if s.endswith('/'):
        s = s[:-1]
    return s


def main(argv):
    parser = argparse.ArgumentParser(description='Process monorepo metadata.')

    parser.add_argument(
        '--force-refresh',
        action='store_true',
        help='Refresh all metadata.',
    )

    parser.add_argument(
        '--repo-path',
        default=os.environ['GITHUB_WORKSPACE'],
        type=str,
        help='Path to monorepo root.',
    )

    parser.add_argument(
        '--dry-run',
        action='store_true',
        help='Enable dry run to prevent metadata being uploaded to S3.',
    )

    parser = parser.parse_args()

    force_refresh = (
        True
        if 'FORCE_REFRESH' in os.environ
        and os.environ['FORCE_REFRESH'] == 'true'
        else parser.force_refresh
    )
    dry_run = (
        True
        if 'DRY_RUN' in os.environ and os.environ['DRY_RUN'] == 'true'
        else parser.dry_run
    )

    repo_path = parser.repo_path

    # If any relevant files are changed will refresh all for now.
    # Will implement diff updates later.
    # Add check for watched files that trigger build.
    file_trigger_build = False

    if file_trigger_build or force_refresh:
        codeowners_path = os.path.join(repo_path, '.github/CODEOWNERS')
        owners = CodeOwners(file_get_contents(codeowners_path))

        logger.info('Refreshing all metadata.')
        artifacts = get_all_artifacts(repo_path)

        all_metadata = []

        for a in artifacts:
            metadata = get_metadata_for_artifact(owners, repo_path, a)

            if metadata:
                all_metadata.append(metadata)

        if not dry_run:
            logger.info('Shipping updated metadata.')
            with open('/tmp/metadata.json', 'w', encoding='utf-8') as f:
                json.dump(all_metadata, f, ensure_ascii=False, indent=4)

            s3 = boto3.resource('s3')
            # Exception will be raised if upload fails.
            s3.meta.client.upload_file(
                '/tmp/metadata.json', 'wt-boardroom-metadata', 'metadata.json'
            )
        else:
            logger.info('Dry run mode. Printing metadata...')
            print(all_metadata)
    else:
        logger.info('Nothing to do, pass --force-refresh to force it.')


main(sys.argv)
