import json
import logging
import os
import sys

from lib.utils import file_get_contents

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger()


# Class encapsulates a bunch of methods to make it
# easier to get info about an artifact in the monorepo.
class Artifact:
    def __init__(self, repo_root_path, path):
        """
        Inits class.

        Args:
            path: absolute file path to artifact folder.

        Returns:
            Instance of Artifact
        """
        self.absolute_path = path
        self.repo_root_path = repo_root_path
        self.relative_path = path.replace(os.path.join(repo_root_path, ''), '')

        self.ci_data = False
        self.__load_ci_data()

    def get_relative_artifact_path(self):
        """
        Returns:
            Returns relative path to artifact using repo
            as the working directory.
        """
        return self.relative_path

    def get_artifact_type(self):
        """
        Returns:
            Type of artifact this is based on it's parent folder
            i.e function, service, job etc.
        """
        return os.path.basename(
            os.path.split(os.path.split(self.absolute_path)[0])[0]
        ).upper()

    def get_service_name(self):
        """
        Returns:
            Name of service by using folder name or serviceName in ci.json
        """
        if self.ci_data and 'serviceName' in self.ci_data:
            return self.ci_data['serviceName']

        # Get service name from folder name.
        return self.relative_path.replace(
            self.get_artifact_type().lower(), ''
        ).replace('/', '')

    def get_newrelic_application_id(self):
        """
        Returns:
            Newrelic Application ID from ci.json:newRelic:applicationId:prod
        """
        if (
            self.ci_data
            and 'newRelic' in self.ci_data
            and 'applicationId' in self.ci_data['newRelic']
            and 'prod' in self.ci_data['newRelic']['applicationId']
        ):
            return self.ci_data['newRelic']['applicationId']['prod']

        return False

    def get_pagerduty_service_id(self):
        """
        Returns:
            PagerDuty service ID using ci.json:pagerDutyServiceId
        """
        if self.ci_data and 'pagerDutyServiceId' in self.ci_data:
            return self.ci_data['pagerDutyServiceId']

        return False

    def __load_ci_data(self):
        ci_path = os.path.join(self.absolute_path, 'ci.json')
        ci_file = file_get_contents(ci_path)

        if ci_file:
            logger.info(f'Loaded config: {ci_path}')

            ci_data = json.loads(ci_file)

            if ci_data:
                self.ci_data = ci_data
            else:
                logger.info(f'Invalid json at: {ci_file}')
        else:
            logger.info(f'No ci.json file for artifact at: {ci_path}')
