import os


def file_get_contents(filename):
    if os.path.isfile(filename):
        with open(filename) as f:
            return f.read()

    return False
