import argparse
import os
import sys

sys.path.append(os.path.dirname(__file__) + "/../helpers")
from helpers import get_app_name, get_file_contents, set_output  # noqa E402


def execute():
    """
    Parses the script arguments and executes the main action function.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--changed-files")
    parser.add_argument("--removed-files")
    parser.add_argument("--path", default="services/")

    action(**vars(parser.parse_args()))


def action(changed_files="", removed_files="", path="services/"):
    """
    Parses the JSON string or file with the modified objects and validates only
    one application is being updated at the time.
    """
    app_path = path.rstrip("/")

    try:
        if os.path.dirname(app_path) != "":
            if not os.path.exists(app_path):
                raise FileNotFoundError

            set_output("app", get_app_name(app_path))
            set_output("path", app_path)
            return

    except FileNotFoundError:
        sys.exit("# Application's folder not found.")

    changed = get_file_contents(
        changed_files,
        type="json"
    )
    removed = get_file_contents(
        removed_files,
        type="json",
        error=False,
        content='[]'
    )
    files = [file for file in changed if file not in removed]

    try:
        folders = []
        excluded = (".github", "architecture", "docker", "scripts")
        for file in files:
            depth = os.path.dirname(file.rstrip("/")).split("/")
            folder = (
                "/".join(depth[:4])
                if depth[0] == "jobs" and depth[2] and depth[2] == "docker"
                else "/".join(depth[:2])
            )
            if (
                folder != ""
                and not folder.startswith(excluded)
                and folder not in folders
            ):
                folders.append(folder)

        if not len(folders):
            raise FileNotFoundError

        if len(folders) > 1:
            raise ValueError

        set_output("app", os.path.basename(folders[0]))
        set_output("path", folders[0])

    except FileNotFoundError:
        sys.exit("# No application files were found being updated/created.")

    except ValueError:
        print("> Two or more applications were updated/created at the time:")
        [print(f"  - {folder}") for folder in folders]
        sys.exit("# Validation failed!")


if __name__ == "__main__":
    execute()
