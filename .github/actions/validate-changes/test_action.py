import json
import os

import pytest  # type: ignore

from action import action


class TestValidateChanges:

    # Workflow dispatch: Executing the pipeline from Github Actions' UI
    def test_workflow_dispatch(self, capsys):
        # Invalid path
        with pytest.raises(SystemExit) as info:
            action(path="services/inexistent")
        assert str(info.value) == "# Application's folder not found."

        # Path found
        # Not using a 'tmp_path' fixture because of issues deleting folders
        app = "simple-service"
        path = "services/simple-service"
        try:
            os.makedirs(path)
        except FileExistsError:
            pass
        action(path=path)
        os.rmdir("services/simple-service")
        os.rmdir("services")
        out, err = capsys.readouterr()
        assert out.strip() == f"> app: {app}\n> path: {path}"

    # Normal 'push' or 'pull_request' workflow event
    @pytest.fixture
    def valid_changes(self):
        return [
            [
                ".gitignore",
                "services/README.md",
                "services/simple-service/ci.json",
                "services/simple-service/package.json",
                "services/simple-service/src/index.js",
            ],
        ]

    @pytest.fixture
    def invalid_changes(self):
        return [
            [
                "services/simple-service/ci.json",
                "functions/simple-function/package.json",
            ],
        ]

    def test_changed_files(self, valid_changes, invalid_changes, capsys):
        changes_file = "changed.json"

        for data in valid_changes:
            with open(changes_file, "w+") as file:
                file.write(json.dumps(data))
            action(changes_file)
            os.remove(changes_file)
            out, err = capsys.readouterr()
            assert "> app: " in out.strip()

        for data in invalid_changes:
            with open(changes_file, "w+") as file:
                file.write(json.dumps(data))
            with pytest.raises(SystemExit) as info:
                action(changes_file)
            os.remove(changes_file)
            out, err = capsys.readouterr()
            assert str(info.value) == "# Validation failed!"
