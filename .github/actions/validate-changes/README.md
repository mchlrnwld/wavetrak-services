# Github Actions: Validate Changes

As part of the Surfline's CI/CD Pipeline, this custom Github Action intends to restrict that only one service, function, job, or web-static application is being created or updated at the time, in order to correctly build, test and deploy it with its particular pipeline.

## Table of Contents <!-- omit in toc -->

- [How it works](#how-it-works)
- [Execution](#execution)
  - [From a workflow dispatch](#from-a-workflow-dispatch)
  - [From a workflow inside a pipeline](#from-a-workflow-inside-a-pipeline)
- [Development](#development)
  - [Installation](#installation)
  - [Type check](#type-check)
  - [Format](#format)
  - [Linting](#linting)
  - [Testing](#testing)

## How it works

The `action.py` script receives a list of files created or updated on the current commit, parsing the application folders and comparing them to be sure only one application has modifications.

It depends on the parameters sent from the `action.yml` file when running from a Github Actions pipeline, but it can be also executed directly for tests purposes.

## Execution

The action is being called from one or more or the workflow files stored in the `.github/workflows/` folder at the root of this repository. Usually, we make use of `trilom/file-changes-action`, a 3rd party action that creates this file with the modifications and pass it to out action into the `changed-files` parameter.

In the case a workflow is being dispatched from the Github Actions UI, the action receives the `path` parameter that will override any change coming from the `changed-files` parameter.

```yaml
      - id: changes
        if: github.event.inputs.service == ''
        name: Check for new or updated files
        uses: trilom/file-changes-action@a6ca26c14274c33b15e6499323aac178af06ad4b # v1.2.4

      - id: validate
        name: Validate only one service is updated at the time
        uses: ./.github/actions/validate-changes
        with:
          changed-files: ${HOME}/files.json
          path: services/${{ github.event.inputs.service }}
```

Both parameters may not exists at the same time, but as a declarative language, they must be present in the YAML file.

So, if you want to execute the script locally, you can simulate two different cases:

### From a workflow dispatch

```bash
python action.py \
  --path services/simple-service
```

### From a workflow inside a pipeline

```bash
python action.py \
  --changed-files changes.json
```

## Development

A `Makefile` file contains several targets to ease and structure the development of this action.

### Installation

To install the action locally we make use of Conda DevEnv. The installation target is present in `Makefile` but you'll need to activate the Conda Environment after creation:

```bash
make install
conda activate cicd-pipeline-validate-changes
```

### Type check

The `mypy` application makes an exhaust audit of the code, so if something fails at this stage, it probably might fail when calling the linting and formatting targets too:

```bash
make type-check
```

### Format

The formatting target uses `autoflake` and `isort` to check the code, but be careful and take into account that it removes unused variables and imports, and the format settings may not correspond with your code styling:

```bash
make format
```

### Linting

Make sure your code is correctly linted when making changes:

```bash
make lint
```

### Testing

We created a basic set of tests for this action, that you can find in `test.py` and can be executed running:

```bash
make test-unit
```

If you need to add more test files, you may want to update the `test-unit` target in `Makefile`.
