# Native dependencies
import argparse
import os
import sys

sys.path.append(os.path.dirname(__file__) + "/../helpers")
from helpers import (  # noqa E402
    get_app_name,
    get_file_contents,
    put_file_contents,
)

DEFAULT_PORT = 8080
DEFAULT_REGION = "us-west-1"


def execute():
    """
    Parses the script arguments and executes the main action function.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--compose", required=True)
    parser.add_argument("--environment", required=True)
    parser.add_argument("--image-host", required=True)
    parser.add_argument("--image-name", required=True)
    parser.add_argument("--image-tag", required=True)
    parser.add_argument("--path", default=".")
    parser.add_argument("--port", default=DEFAULT_PORT)
    parser.add_argument("--region", default=DEFAULT_REGION)

    main(**vars(parser.parse_args()))


def main(
    compose,
    environment,
    image_host,
    image_name,
    image_tag,
    path=".",
    port=DEFAULT_PORT,
    region=DEFAULT_REGION,
):
    """
    Creates a docker-compose-{env}.yml file with the corresponding service
    values from a Docker Compose file, if present.
    """
    action_path = os.path.dirname(__file__)
    app_path = path.rstrip("/")

    # Call the 'ci.json' file to confirm the application path is correct
    get_file_contents(f"{app_path}/ci.json")

    # Get the Docker Compose file or a template file.
    content = get_file_contents(
        f"{app_path}/docker-compose.yml",
        alternative=f"{action_path}/docker-compose.yml.j2",
    )

    replacements = {
        "SERVICE_NAME": compose,
        "ENVIRONMENT": environment,
        "REPOSITORY_HOST": image_host,
        "IMAGE_NAME": image_name,
        "IMAGE_TAG": image_tag,
        "PORT": port,
        "AWS_REGION": region,
    }
    for key, val in replacements.items():
        content = content.replace("${" + key + "}", str(val))

    filename = f"{app_path}/docker-compose-{environment}.yml"
    put_file_contents(filename, content=content)

    print(f"> File '{filename}' generated!")
    print(content)


if __name__ == "__main__":
    execute()
