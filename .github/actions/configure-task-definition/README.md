# Github Actions: Configure Task Definition

As part of the Surfline's CI/CD Pipeline, the purpose of this custom Github Action is to parse `docker-compose.yml` and `ecs-params.yml` configuration files from the application's folder in order to later create the service's task definition.

## Table of Contents <!-- omit in toc -->

- [How it works](#how-it-works)
- [Execution](#execution)
- [Development](#development)
  - [Installation](#installation)
  - [Type check](#type-check)
  - [Format](#format)
  - [Linting](#linting)
  - [Testing](#testing)

## How it works

The `action.yml` file is called from a Github Actions pipeline, and sends the corresponding parameters to execute both of the scripts in this action, `create-docker-compose.py` and `create-ecs-params.py`.

## Execution

The action is being called from one or more or the workflow files stored in the `.github/workflows/` folder at the root of this repository.

```yaml
      - id: configure
        name: Configure task definition
        uses: ./.github/actions/configure-task-definition
        with:
          path: ${{ needs.init.outputs.path }}
          service: ${{ needs.init.outputs.app }}
          environment: ${{ matrix.environment }}
          image-host: ${{ steps.ecr-login.outputs.registry }}
          image-name: ${{ needs.init.outputs.image-name }}
          image-tag: ${{ needs.init.outputs.commit }}
```

If you want to execute the script locally, you can run it like this:

```bash
python create-ecs-params.py \
  --path services/simple-service
  --environment sandbox

python create-docker-compose.py \
  --path services/simple-service
  --environment sandbox \
  --image-host ***.dkr.ecr.***.amazonaws.com \
  --image-name services/simple-service \
  --image-tag b2e4d37
```

## Development

A `Makefile` file contains several targets to ease and structure the development of this action.

### Installation

To install the action locally we make use of Conda DevEnv. The installation target is present in `Makefile` but you'll need to activate the Conda Environment after creation:

```bash
make install
conda activate cicd-pipeline-configure-task-definition
```

### Type check

The `mypy` application makes an exhaust audit of the code, so if something fails at this stage, it probably might fail when calling the linting and formatting targets too:

```bash
make type-check
```

### Format

The formatting target uses `autoflake` and `isort` to check the code, but be careful and take into account that it removes unused variables and imports, and the format settings may not correspond with your code styling:

```bash
make format
```

### Linting

Make sure your code is correctly linted when making changes:

```bash
make lint
```

### Testing

We created a basic set of tests for this action, that you can find in `test.py` and can be executed running:

```bash
make test-unit
```

If you need to add more test files, you may want to update the `test-unit` target in `Makefile`.
