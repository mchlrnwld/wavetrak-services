# Native dependencies
import argparse
import os
import sys

# Third party dependencies
import boto3  # type: ignore
import botocore  # type: ignore
import yaml

sys.path.append(os.path.dirname(__file__) + "/../helpers")
from helpers import get_file_contents, put_file_contents  # noqa E402


def execute():
    """
    Parses the script arguments and executes the main action function.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--environment", required=True)
    parser.add_argument("--path", default=".")

    main(**vars(parser.parse_args()))


def main(environment, path="."):
    """
    Creates an ECS Parameters file that embeds the references to AWS Parameter
    Store values from "ci.json" and "ecs-params.yml" configuration files.
    """
    app_path = path.rstrip("/")
    action_path = os.path.dirname(__file__)

    ci = get_file_contents(f"{app_path}/ci.json", type="json")

    ep = get_file_contents(
        f"{app_path}/ecs-params.yml",
        alternative=f"{action_path}/ecs-params.yml.j2",
        error=False,
        type="yaml",
    )

    try:
        service_name = str(ci["compose"])
        task_role_arn = str(ci["taskRoleArn"][environment])
    except KeyError as key:
        sys.exit(f"# Key '{key}' not found in 'ci.json' file.")

    if "services" not in ep["task_definition"]:
        ep["task_definition"]["services"] = {}
        ep["task_definition"]["services"][service_name] = {}

    secrets = [
        {
            "name": str(key),
            "value_from": str(ci["secrets"][scope]["prefix"]).format(
                env=environment
            )
            + str(key),
        }
        for scope in ["common", "service"]
        for key in ci["secrets"][scope]["secrets"]
    ]

    validate_secrets([i["value_from"] for i in secrets])

    ep["task_definition"]["services"][service_name]["secrets"] = secrets

    ep["task_definition"]["task_execution_role"] = task_role_arn
    ep["task_definition"]["task_role_arn"] = task_role_arn

    filename = f"{app_path}/ecs-params-{environment}.yml"
    content = yaml.dump(ep)
    put_file_contents(filename, content=content)

    print(f"> File '{filename}' generated!")
    print(content)


def validate_secrets(secrets):
    """
    Verifies the existence of the secrets object passed by argument
    in AWS Parameter Store.

    If all variables are found, returns success message.
    Else, returns a message with a list of all the variables that had errors.
    """
    print("> Verifying if all secrets exist in AWS Parameter Store...")
    ssm_client = boto3.client("ssm", region_name="us-west-1")

    missing = []
    access_denied = []
    other_error = []

    for parameter in secrets:
        try:
            ssm_client.get_parameter(Name=parameter)

        except botocore.exceptions.NoCredentialsError:
            sys.exit("# Unable to locate AWS credentials.")

        except botocore.exceptions.ClientError as error:
            if error.response["Error"]["Code"] == "ParameterNotFound":
                missing.append(parameter)

            elif error.response["Error"]["Code"] == "AccessDeniedException":
                access_denied.append(parameter)

            else:
                other_error.append(error)

    if not missing and not access_denied and not other_error:
        print("  All parameters found!")

    else:
        if missing:
            for param in missing:
                print(f"- Can't find parameter '{param}'")

        if access_denied:
            for param in access_denied:
                print(f"- Access denied for parameter '{param}'")

        if other_error:
            for param in other_error:
                print(f"- {other_error}")

        sys.exit("# Unable to successfully look up all the parameters!")


if __name__ == "__main__":
    execute()
