import json
import os
import subprocess
import sys

import yaml


def get_app_name(path):
    """
    Retrieves the application's name from a relative path or the current folder
    where the script is being executed. It assumes that the application's path
    consists in a namespace and the name of the application and doesn't really
    validates if the application exists.
    E.g: {namespace}/{application-name}
    """
    return os.path.basename(os.path.normpath(f"{os.getcwd()}/{path}"))


def get_content_object(content, type):
    """
    Returns a text content with the object's type passed by the type argument.
    """
    type = "plain" if not type else type
    if type.lower() not in ["plain", "json", "yaml", "yml"]:
        sys.exit(f"# Invalid output type '{type}'.")

    try:
        if type.lower() == "json":
            return json.loads(content if content else "{}")

        if type.lower() in ["yaml", "yml"]:
            return yaml.safe_load(content)

        return str(content if content else "")

    except FileNotFoundError:
        sys.exit("# Invalid file type.")


def get_file_contents(
    filename,
    alternative="",
    content="",
    create=False,
    error=True,
    type="plain",
):
    """
    Retrieves the JSON or plain content of a file or creates a new one if it
    doesn't exist with an initial content passed by arguments.
    """
    try:
        if filename:
            if not os.path.exists(filename) and create:
                put_file_contents(filename, content=content)

            with open(filename) as file:
                content = file.read()

    except FileNotFoundError:
        if alternative:
            return get_file_contents(
                alternative,
                content=content,
                create=create,
                error=error,
                type=type,
            )

        if error:
            sys.exit(f"# File '{filename}' not found.")

    except PermissionError:
        sys.exit(f"# Insufficient permissions to create file '{filename}'")

    return get_content_object(content, type)


def put_file_contents(
    filename,
    append=False,
    content="",
    type="plain",
):
    """
    Writes the content passed by arguments into a file.
    """
    try:
        mode = "a" if append else "w"
        with open(filename, mode) as file:
            content = get_content_object(content, type)
            file.write(str(content))

    except PermissionError:
        sys.exit(f"# Insufficient permissions to write to file '{filename}'")


def get_commit_sha():
    """
    Retrieves the git commit sha from environment variables.
    """
    try:
        sha = ["PULL_SHA", "COMMIT_SHA"]
        return next(os.environ.get(x) for x in sha if os.environ.get(x))

    except StopIteration:
        sys.exit("# Commit SHA not found.")


def get_commit_sha_from_ref(ref):
    """
    Retrieves the commit sha from a branch or tag name.
    """
    try:
        proc = subprocess.check_output(f"git rev-parse {ref}".split())
        return str(proc, "utf-8")

    except Exception as err:
        print(err)


def get_git_ref():
    """
    Retrieves the current git ref or branch name from environment variables.
    """
    try:
        sha = ["GITHUB_HEAD_REF", "GITHUB_BASE_REF", "GITHUB_REF"]
        return next(
            os.environ.get(x).replace("refs/heads/", "")
            for x in sha if os.environ.get(x)
        )

    except StopIteration:
        sys.exit("# Can't retrieve git ref or branch name.")


def get_git_ref_from_sha(sha):
    """
    Retrieves the branch or tag name from a commit sha.
    """
    try:
        proc = subprocess.check_output(
            f"git name-rev {sha} --name-only".split()
        )
        return str(proc, "utf-8").replace("remotes/origin/", "")

    except Exception as err:
        print(err)


def get_pull_id():
    """
    Retrieves the pull request id from environment variable.
    """
    try:
        return os.environ.get("GITHUB_REF") \
            .replace("refs/pull/", "") \
            .replace("/merge", "")

    except StopIteration:
        sys.exit("# Pull request id not found.")


def get_recent_commit_messages(qty=10):
    """
    Retrieves the latest commit messages from current branch.
    It doesn't make the pipeline fail if command error.
    """
    try:
        proc = subprocess.check_output(f"git log --oneline -n {qty}".split())
        return str(proc, "utf-8")

    except Exception as err:
        print(err)


def set_output(name, value=""):
    """
    A simple wrapper that outputs a variable a key/name value to a format that
    Github Actions understands. It also works as a debugging tool when testing
    the gathered information from a shared/custom action.
    """
    if os.environ.get("GITHUB_ACTIONS"):
        print(f"::set-output name={name}::{value if value else ''}")
    print(f"> {name}: {value if value else ''}")
