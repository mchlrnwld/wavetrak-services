import os

import mock
import pytest

from .helpers import (  # noqa
    get_app_name,
    get_commit_sha,
    get_content_object,
    get_file_contents,
    get_git_ref,
    get_recent_commit_messages,
    put_file_contents,
    set_output,
)


class TestHelpers:

    # get_app_name
    @pytest.fixture
    def application_paths(self):
        return [
            {"path": "../../services/simple-service", "app": "simple-service"},
            {"path": "./", "app": "helpers"},
            {"path": "services/simple-service", "app": "simple-service"},
            {"path": "functions/simple-function", "app": "simple-function"},
        ]

    def test_get_app_name(self, application_paths):
        for data in application_paths:
            assert get_app_name(data["path"]) == data["app"]

    # get_content_object
    @pytest.fixture
    def valid_contents(self):
        return [
            {"content": "", "type": "plain", "res": ""},
            {"content": "", "type": "yaml", "res": None},
            {"content": "", "type": "json", "res": {}},
            {"content": {}, "type": "json", "res": {}},
            {"content": [], "type": "json", "res": {}},
            {"content": "[]", "type": "json", "res": []},
            {"content": "Hello", "res": "Hello"},
            {"content": "Hello", "type": "yml", "res": "Hello"},
            {"content": "Hello", "type": "yaml", "res": "Hello"},
        ]

    def test_get_content_object(self, valid_contents):
        for data in valid_contents:
            res = get_content_object(data.get("content"), data.get("type"))
            assert res == data["res"]

    # get_file_contents
    def test_get_file_contents(self):
        # File doesn't exist, raise error
        with pytest.raises(SystemExit) as info:
            get_file_contents("hello.txt")
        assert str(info.value) == "# File 'hello.txt' not found."

        # File doesn't exist and can't be created, raise error
        with pytest.raises(SystemExit) as info:
            get_file_contents("/etc/hello.txt", create=True)
        assert str(info.value).startswith("# Insufficient permissions")

        # File doesn't exists but don't raise error
        content = get_file_contents("hello.txt", error=False)
        assert content == ""

        # File doesn't exists but don't raise error and get default content
        content = get_file_contents("hello.txt", error=False, content="Hello")
        assert content == "Hello"

        # File doesn't exists, create and get content sent
        content = get_file_contents("hello.txt", create=True, content="Hello")
        with open("hello.txt") as file:
            stored = file.read()
        os.remove("hello.txt")
        assert content == stored

        # File exists and get its content
        with open("hello-world.txt", "w") as file:
            file.write("Hello World")
        content = get_file_contents("hello-world.txt")
        os.remove("hello-world.txt")
        assert content == "Hello World"

        # File exists and get its content in json format
        with open("hello-world.json", "w") as file:
            file.write('{"message": "Hello World", "version": "0.1.0"}')
        content = get_file_contents("hello-world.json", type="json")
        os.remove("hello-world.json")
        assert str(type(content)) == "<class 'dict'>"
        assert content["message"] == "Hello World"
        assert content["version"] == "0.1.0"

        # File exists and get its content in yaml format
        with open("hello-world.yaml", "w") as file:
            file.write("message: 'Hello World'\nversion: 0.1.0")
        content = get_file_contents("hello-world.yaml", type="yaml")
        os.remove("hello-world.yaml")
        assert str(type(content)) == "<class 'dict'>"
        assert content["message"] == "Hello World"
        assert content["version"] == "0.1.0"

        # First file doesn't exist but takes content from the alternative
        with open("2.txt", "w") as file:
            file.write("- Hi, how are you?")
        content = get_file_contents("1.txt", alternative="2.txt", error=False)
        os.remove("2.txt")
        assert content == "- Hi, how are you?"

        # Both files exist but takes content from the first one
        with open("1.txt", "w") as file:
            file.write("- Hi, how are you?")
        with open("2.txt", "w") as file:
            file.write("- Fine, thanks. And you?")
        content = get_file_contents("1.txt", alternative="2.txt")
        os.remove("1.txt")
        os.remove("2.txt")
        assert content == "- Hi, how are you?"

    # put_file_contents
    def test_put_file_contents(self):
        # File doesn't exist, create content
        put_file_contents("hello-world.txt", content="Hello!")
        content = get_file_contents("hello-world.txt")
        os.remove("hello-world.txt")
        assert content == "Hello!"

        # File exists, overwrite content
        with open("greet.txt", "w") as file:
            file.write("Hi, how are you?")
        put_file_contents("greet.txt", content="Hello!")
        content = get_file_contents("greet.txt")
        os.remove("greet.txt")
        assert content == "Hello!"

        # File exists, append content
        with open("greet.txt", "w") as file:
            file.write("- Hi, how are you?\n")
        put_file_contents("greet.txt", content="- Fine! And you?", append=True)
        content = get_file_contents("greet.txt")
        os.remove("greet.txt")
        assert content == "- Hi, how are you?\n- Fine! And you?"

    # get_git_ref
    @pytest.fixture
    def git_references(self):
        return [
            {
                "GITHUB_HEAD_REF": "19ak238",
                "res": "19ak238",
            },
            {
                "GITHUB_REF": "3os88da",
                "res": "3os88da",
            },
            {
                "GITHUB_HEAD_REF": "l0sk3ks",
                "GITHUB_REF": "02kd74l",
                "res": "l0sk3ks",
            },
        ]

    def test_get_git_ref(self, git_references):
        # No git references found
        mocker = mock.patch.dict(
            os.environ, {"GITHUB_HEAD_REF": "", "GITHUB_REF": ""}, clear=True
        )
        mocker.start()
        with pytest.raises(SystemExit) as info:
            get_git_ref()
        assert str(info.value) == "# Can't retrieve git ref or branch name."
        mocker.stop()

        # Non-empty references.
        for data in git_references:
            mocker = mock.patch.dict(
                os.environ,
                {
                    "GITHUB_HEAD_REF": data.get("GITHUB_HEAD_REF", ""),
                    "GITHUB_REF": data.get("GITHUB_REF", ""),
                },
                clear=True,
            )
            mocker.start()
            assert get_git_ref() == data.get("res")
            mocker.stop()

    # get_recent_commit_messages
    def test_get_recent_commit_messages(self):
        # Default quantity (10).
        messages = get_recent_commit_messages()
        assert len(messages.split("\n")[:-1]) <= 10

        # Try a different value.
        messages = get_recent_commit_messages(5)
        assert len(messages.split("\n")[:-1]) <= 5

    # get_commit_sha
    @pytest.fixture
    def commit_sha(self):
        return [
            {
                "DISP_SHA": "19ak238",
                "res": "19ak238",
            },
            {
                "PULL_SHA": "3os88da",
                "res": "3os88da",
            },
            {
                "PUSH_SHA": "9dj37v1",
                "res": "9dj37v1",
            },
            {
                "COMM_SHA": "n29js04",
                "res": "n29js04",
            },
            {
                "DISP_SHA": "19ak238e1c72d7419d02aa8e861b65196504ca87",
                "res": "19ak238e1c72d7419d02aa8e861b65196504ca87",
            },
            {
                "PULL_SHA": "3os88dae1c72d7419d02aa8e861b65196504ca87",
                "res": "3os88dae1c72d7419d02aa8e861b65196504ca87",
            },
            {
                "PUSH_SHA": "9dj37v1e1c72d7419d02aa8e861b65196504ca87",
                "res": "9dj37v1e1c72d7419d02aa8e861b65196504ca87",
            },
            {
                "COMM_SHA": "n29js04e1c72d7419d02aa8e861b65196504ca87",
                "res": "n29js04e1c72d7419d02aa8e861b65196504ca87",
            },
            {
                "DISP_SHA": "19ak238",
                "PULL_SHA": "3os88da",
                "res": "19ak238",
            },
            {
                "PULL_SHA": "3os88da",
                "PUSH_SHA": "9dj37v1",
                "res": "3os88da",
            },
            {
                "PUSH_SHA": "9dj37v1",
                "COMM_SHA": "n29js04",
                "res": "9dj37v1",
            },
            {
                "DISP_SHA": "19ak238",
                "PULL_SHA": "3os88da",
                "PUSH_SHA": "9dj37v1",
                "COMM_SHA": "n29js04",
                "res": "19ak238",
            },
        ]

    def test_get_commit_sha(self, commit_sha):
        mocker = mock.patch.dict(
            os.environ,
            {
                "DISP_SHA": "",
                "PULL_SHA": "",
                "PULL_SHA": "",
                "COMM_SHA": "",
            },
            clear=True,
        )
        mocker.start()
        with pytest.raises(SystemExit) as info:
            get_git_ref()
        assert str(info.value) == "# Can't retrieve git ref or branch name."
        mocker.stop()

        # Non-empty references.
        for data in commit_sha:
            mocker = mock.patch.dict(
                os.environ,
                {
                    "DISP_SHA": data.get("DISP_SHA", ""),
                    "PULL_SHA": data.get("PULL_SHA", ""),
                    "PUSH_SHA": data.get("PUSH_SHA", ""),
                    "COMM_SHA": data.get("COMM_SHA", ""),
                },
                clear=True,
            )
            mocker.start()
            assert get_commit_sha() == data.get("res")
            mocker.stop()

    # set_output
    @pytest.fixture
    def valid_outputs(self):
        return [
            {
                "key": "name",
                "val": "value",
                "res": "> name: value",
            },
            {
                "key": "name",
                "val": "",
                "res": "> name:",
            },
            {
                "key": "name",
                "val": None,
                "res": "> name:",
            },
            {
                "key": "",
                "val": "",
                "res": "> :",
            },
            {
                "key": "name",
                "val": "value",
                "env": "GITHUB_ACTIONS=True",
                "res": "::set-output name=name::value\n> name: value",
            },
            {
                "key": "name",
                "val": None,
                "env": "GITHUB_ACTIONS=True",
                "res": "::set-output name=name::\n> name:",
            },
        ]

    def test_set_output(self, valid_outputs, capsys):
        for data in valid_outputs:
            if "env" in data:
                var, val = data["env"].split("=")
                mocker = mock.patch.dict(os.environ, {var: val}, clear=True)
                mocker.start()
                set_output(data["key"], data["val"])
                mocker.stop()
            else:
                set_output(data["key"], data["val"])

            out, err = capsys.readouterr()
            assert err.strip() == ""
            assert out.strip() == data["res"]
