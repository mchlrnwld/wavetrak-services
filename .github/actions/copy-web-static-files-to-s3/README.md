# Github Actions: Copy Web Static Files To S3

As part of the Surfline's CI/CD Pipeline, this GitHub Action copies web static files from a local directory to a S3 bucket. It varies file locations by app type and includes cache/no-cache metadata based on no-cache-files globs.

## Table of Contents <!-- omit in toc -->

- [Execution](#execution)
  - [From a workflow dispatch](#from-a-workflow-dispatch)
  - [From a workflow inside a pipeline](#from-a-workflow-inside-a-pipeline)
- [Development](#development)
  - [Installation](#installation)
  - [Type check](#type-check)
  - [Format](#format)
  - [Linting](#linting)
  - [Testing](#testing)

## Execution

The action is being called from one or more or the workflow files stored in the `.github/workflows/` folder at the root of this repository, setting the current path of the application to test, build and deploy.

```yaml
- id: copy-app-files-to-s3
  name: Copy app files to S3
  uses: ./.github/actions/copy-web-static-files-to-s3
  with:
    path: ${{ needs.init.outputs.path }}/tmp/opt/dist
    environment: ${{ matrix.environment }}
    bucket: ${{ needs.init.outputs.bucket }}
    destination-path: ${{ needs.init.outputs.destinationPath }}
    commit: ${{ needs.init.outputs.commit }}
    app-type: ${{ needs.init.outputs.appType }}
    no-cache-file-patterns: ${{ needs.init.outputs.noCacheFiles }}
```

You may execute the script locally with the following command:

```bash
pushd fixtures && \
LOGLEVEL=DEBUG python ../action.py \
  --bucket "s3://sl-admin-tools-sbox" \
  --destination-path "poi" \
  --commit "2801ce99c4c35ca1ad7eb6ca23ae0f7423fb38fc" \
  --app-type "" \
  --no-cache-file-patterns "[\"**/foo.txt\"]" && \
popd
```

## Development

A `Makefile` file contains several targets to ease and structure the development of this action.

### Installation

To install the action locally we make use of Conda DevEnv. The installation target is present in `Makefile` but you'll need to activate the Conda Environment after creation:

```bash
make install
conda activate copy-web-static-files-to-s3
```

### Type check

The `mypy` application makes an exhaust audit of the code, so if something fails at this stage, it probably might fail when calling the linting and formatting targets too:

```bash
make type-check
```

### Format

The formatting target uses `autoflake` and `isort` to check the code, but be careful and take into account that it removes unused variables and imports, and the format settings may not correspond with your code styling:

```bash
make format
```

### Linting

Make sure your code is correctly linted when making changes:

```bash
make lint
```

### Testing

We created a basic set of tests for this action, that you can find in `test.py` and can be executed running:

```bash
make test-unit
```

If you need to add more test files, you may want to update the `test-unit` target in `Makefile`.
