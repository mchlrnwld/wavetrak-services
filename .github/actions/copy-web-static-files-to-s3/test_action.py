from action import get_cache_metadata, get_content_type


class TestCopyWebStaticFilesToS3:
    def test_get_content_type(self):
        assert get_content_type('filename.css') == 'text/css'
        assert get_content_type('filename.gif') == 'image/gif'
        assert get_content_type('filename.html') == 'text/html'
        assert get_content_type('filename.ico') == 'image/x-icon'
        assert get_content_type('filename.jpg') == 'image/jpeg'
        assert get_content_type('filename.js') == 'application/javascript'
        assert get_content_type('filename.json') == 'application/json'
        assert get_content_type('filename.png') == 'image/png'
        assert get_content_type('filename.svg') == 'image/svg+xml'
        assert get_content_type('filename.txt') == 'text/plain'
        assert get_content_type('filename.webp') == 'image/webp'

    def test_get_content_type_with_directory(self):
        assert get_content_type('directory/filename.ico') == 'image/x-icon'
        assert get_content_type('directory/filename.css') == 'text/css'
        assert get_content_type('directory/filename.gif') == 'image/gif'
        assert get_content_type('directory/filename.html') == 'text/html'
        assert get_content_type('directory/filename.jpg') == 'image/jpeg'
        assert (
            get_content_type('directory/filename.js')
            == 'application/javascript'
        )
        assert (
            get_content_type('directory/filename.json') == 'application/json'
        )
        assert get_content_type('directory/filename.png') == 'image/png'
        assert get_content_type('directory/filename.svg') == 'image/svg+xml'
        assert get_content_type('directory/filename.txt') == 'text/plain'
        assert get_content_type('directory/filename.webp') == 'image/webp'

    def test_get_content_type_with_default(self):
        assert get_content_type('filename.abc') == 'text/html'
        assert get_content_type('filename') == 'text/html'

    def test_get_cache_metadata(self):
        no_cache_files = {
            'directory/nocache_file_1.html',
            'directory/nocache_file_2.html',
        }
        assert (
            get_cache_metadata('directory/nocache_file_1.html', no_cache_files)
            == 'max-age=0,no-cache,no-store,must-revalidate'
        )
        assert (
            get_cache_metadata('directory/hashed_file_1.html', no_cache_files)
            == 'immutable,max-age=100000000,public'
        )
