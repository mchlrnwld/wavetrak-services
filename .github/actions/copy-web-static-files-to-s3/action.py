import argparse
import glob
import json
import logging
import mimetypes
import os
from typing import List, Set

import boto3

LOGLEVEL = os.environ.get('LOGLEVEL', 'INFO').upper()
logging.basicConfig(level=LOGLEVEL)
logging.getLogger('botocore').setLevel(logging.ERROR)
logging.getLogger('boto3').setLevel(logging.ERROR)
logger = logging.getLogger(__name__)


def execute() -> None:
    """
    Parses the script arguments and executes the main action function
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--bucket')
    parser.add_argument('--destination-path')
    parser.add_argument('--commit')
    parser.add_argument('--app-type')
    parser.add_argument('--no-cache-file-patterns')

    logger.debug(parser.parse_args())

    action(**vars(parser.parse_args()))


def action(
    bucket: str,
    destination_path: str,
    commit: str,
    app_type: str,
    no_cache_file_patterns: str,
) -> None:
    """
    Looks up cache and content-type metadata and pushes files to S3
    """
    s3 = boto3.client('s3')

    try:
        no_cache_file_pattern_list = json.loads(no_cache_file_patterns)
    except ValueError:
        no_cache_file_pattern_list = []

    # Get no-cache files from glob patterns
    no_cache_files = get_no_cache_files(no_cache_file_pattern_list)

    logger.info(f'no_cache_file_patterns: {no_cache_file_patterns}')
    logger.info(f'no_cache_file_pattern_list: {no_cache_file_pattern_list}')
    logger.info(f'no_cache_files: {no_cache_files}')

    for file in glob.glob('**', recursive=True):
        # We only care about files, not directories, symlinks, etc.
        if not os.path.isfile(file):
            continue

        cache_metadata = get_cache_metadata(file, no_cache_files)
        content_type = get_content_type(file)
        key = get_s3_key(file, app_type, destination_path, commit)

        logger.debug(f'{file}|{key}|{cache_metadata}|{content_type}')

        # For account apps, we should duplicate html files w/o filename
        keys = (
            [key, os.path.dirname(key)]
            if app_type == 'account' and key.endswith('.html')
            else [key]
        )

        for key in keys:
            with open(file, 'rb') as file_object:
                upload_file_to_s3(
                    s3,
                    file,
                    bucket,
                    key,
                    file_object,
                    cache_metadata,
                    content_type,
                    'public-read',
                )


def upload_file_to_s3(
    s3_client, file, bucket, key, body, cache_control, content_type, acl
):
    put_response = s3_client.put_object(
        Bucket=bucket.replace('s3://', ''),
        Key=key,
        Body=body,
        CacheControl=cache_control,
        ContentType=content_type,
        ACL=acl,
    )
    logger.debug(f'S3 put response: {put_response}')

    log_info = {
        'file': file,
        'bucket': bucket,
        'key': key,
        'cache-control': cache_control,
        'content-type': content_type,
        'acl': acl,
    }
    logger.info(f'Uploaded file: {log_info}')


def get_no_cache_files(no_cache_file_pattern_list: List[str]) -> Set[str]:
    """
    Returns a set of all files matching a list of glob patterns
    """
    return set(
        [
            file
            for pattern in no_cache_file_pattern_list
            for file in glob.glob(pattern, recursive=True)
            if os.path.isfile(file)
        ]
    )


def get_cache_metadata(file: str, no_cache_files: Set[str]) -> str:
    """
    Returns cache metadata based on whether a file is in a no-cache list
    """
    return (
        'max-age=0,no-cache,no-store,must-revalidate'
        if file in no_cache_files
        else 'immutable,max-age=100000000,public'
    )


def get_content_type(file: str) -> str:
    """
    Returns content-type based on file extension
    """
    mimetype, _ = mimetypes.guess_type(file)

    return mimetype if mimetype else 'text/html'


def get_s3_key(
    file: str, app_type: str, destination_path: str, commit: str
) -> str:
    """
    Builds S3 key based on app type and file location
    """
    # Account apps have some, uh, 'unique' rules
    if app_type == 'account':
        # out/_next/* => {destinationPath}/_next/*
        if file[: len('out/_next')] == 'out/_next':
            file_key = file[len('out/') :]  # noqa: E203
            return f'{destination_path}/{file_key}'

        # static/* => {destinationPath}/static/*
        if file[: len('static')] == 'static':
            return f'{destination_path}/{file}'

        # out/builds/* => builds/{destinationPath}/{git-commit-sha}/*
        if file[: len('out/builds')] == 'out/builds':
            file_key = file[len('out/builds/') :]  # noqa: E203
            return f'builds/{destination_path}/{commit}/{file_key}'

        # html-no-extension/* => builds/{destinationPath}/{git-commit-sha}/*
        if file[: len('html-no-extension')] == 'html-no-extension':
            file_key = file[len('html-no-extension') :]  # noqa: E203
            return f'builds/{destination_path}/{commit}/{file_key}'

    # default: / => builds/{destinationPath}/{git-commit-sha}
    return f'builds/{destination_path}/{commit}/{file}'


if __name__ == '__main__':
    execute()
