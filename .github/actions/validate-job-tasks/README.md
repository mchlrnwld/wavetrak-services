# Github Actions: Validate Job Tasks

As part of the Surfline's CI/CD Pipeline, this custom Github Action builds a list of the tasks that belong to one or more jobs.

## Table of Contents <!-- omit in toc -->

- [How it works](#how-it-works)
- [Execution](#execution)
- [Development](#development)
  - [Installation](#installation)
  - [Type check](#type-check)
  - [Format](#format)
  - [Linting](#linting)
  - [Testing](#testing)

## How it works

The `action.py` script receives a job path to search for the available tasks in it.

It depends on the parameters sent from the `action.yml` file when running from a Github Actions pipeline, but it can be also executed directly for tests purposes.

## Execution

The action is being called from one or more or the workflow files stored in the `.github/workflows/` folder at the root of this repository.

```yaml
      - id: validate
        name: Validates a list of task jobs to be deployed
        uses: ./.github/actions/validate-job-tasks
        with:
          path: jobs/${{ github.event.inputs.job }}
```

If you want to execute the script locally, you can send a job's folder as the `path` value:

```bash
python action.py \
  --path jobs/download-nasa-mur-sst
```

Or using wildcards to run the validation through multiple folders.

```bash
python action.py \
  --path jobs/download-*
```

## Development

A `Makefile` file contains several targets to ease and structure the development of this action.

### Installation

To install the action locally we make use of Conda DevEnv. The installation target is present in `Makefile` but you'll need to activate the Conda Environment after creation:

```bash
make install
conda activate cicd-pipeline-validate-job-tasks
```

### Type check

The `mypy` application makes an exhaust audit of the code, so if something fails at this stage, it probably might fail when calling the linting and formatting targets too:

```bash
make type-check
```

### Format

The formatting target uses `autoflake` and `isort` to check the code, but be careful and take into account that it removes unused variables and imports, and the format settings may not correspond with your code styling:

```bash
make format
```

### Linting

Make sure your code is correctly linted when making changes:

```bash
make lint
```

### Testing

We created a basic set of tests for this action, that you can find in `test.py` and can be executed running:

```bash
make test-unit
```

If you need to add more test files, you may want to update the `test-unit` target in `Makefile`.
