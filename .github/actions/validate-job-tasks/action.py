import argparse
import glob
import json
import os
import sys

sys.path.append(os.path.dirname(__file__) + "/../helpers")
from helpers import set_output  # noqa E402


def execute():
    """
    Parses the script arguments and executes the main action function.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--path", default="services/")

    action(**vars(parser.parse_args()))


def action(path="jobs/"):
    """
    Looks for the inner tasks inside multiple job folders.
    """
    app_path = path.rstrip("/")
    task_paths = []

    try:
        if app_path.startswith("jobs/") and app_path not in ["jobs"]:
            folders = glob.glob(f"{app_path}/docker/*")
            for folder in folders:
                task_paths.append(folder)

    except Exception as e:
        sys.exit(f"# {e}")

    set_output("paths", json.dumps(task_paths))


if __name__ == "__main__":
    execute()
