# push-metric-to-newrelic

Github Action to send deployment metric events to newrelic

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Inputs](#inputs)
- [Example Usage](#example-usage)
  - [Post Deployment Metrics to New Relic](#post-deployment-metrics-to-new-relic)
  - [Querying the Data](#querying-the-data)
  - [Building New Relic Dashboards](#building-new-relic-dashboards)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Inputs

| Key              | Required | Default | Description                                                                                |
| :----            | :----    | :----   | :----                                                                                      |
| `accountId`      | **yes**  | -       | New Relic account to post metrics results to                                               |
| `appName`        | **yes**  | -       | Application name                                                                           |
| `appType`        | **yes**  | -       | Application type (service, function, static webapp, pip package, npm package, airflow job) |
| `commitSha`      | **yes**  | -       | Git commit SHA                                                                             |
| `environment`    | **yes**  | -       | Environment that was deployed to                                                           |
| `insightsApiKey` | **yes**  | -       | New Relic Insights API key                                                                 |
| `deployType`     | no       | normal  | Deploy type. Possible Options: `normal`, `rollback`                                        |
| `region`         | no       | US      | New Relic account region. Possible Options: `US`, `EU`                                     |
| `user`           | no       | ""      | The user that initiated the workflow run                                                   |

## Example Usage

### Post Deployment Metrics to New Relic

The following example will post deployment custom events of type `Deployment` to New Relic.

Github secrets assumed to be set:

- `NEW_RELIC_ACCOUNT_ID` - New Relic Account ID to post the event data to
- `NEW_RELIC_INSERT_API_KEY` - Insert API key

```yaml
name: New Relic Test Workflow

on: [push]

jobs:
  test-new-relic-metric:
    runs-on: ubuntu-20.04
    steps:
      - name: Check out repository code
        uses: actions/checkout@v2
      - id: newrelic-deployment-event
        name: NewRelic deployment event
        uses: ./.github/actions/push-metric-to-newrelic
        with:
          accountId: ${{ secrets.NEW_RELIC_ACCOUNT_ID }}
          appName: wt-test-service
          appType: service
          commitSha: 954201d
          deployType: normal
          environment: sandbox
          insightsApiKey: ${{ secrets.NEW_RELIC_INSIGHTS_API_KEY }}
          user: ${{ github.actor }}
```

### Querying the Data

Data can be queried in New Relic One [Chart Builder](https://docs.newrelic.com/docs/chart-builder/use-chart-builder/get-started/introduction-chart-builder) or with the [New Relic CLI](https://github.com/newrelic/newrelic-cli) via the `newrelic nrql query` command:

```
newrelic nrql query --accountId 12345 --query 'SELECT * from Deployment'
```

### Building New Relic Dashboards

Example deployment dashboard: <https://onenr.io/0xVwgex7djJ>
