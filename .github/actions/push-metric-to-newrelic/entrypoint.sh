#!/bin/sh

if [ "${NEW_RELIC_REGION}" = "US" ]; then
    API_HOST=https://insights-collector.newrelic.com
elif [ "${NEW_RELIC_REGION}" = "EU" ]; then
    API_HOST=https://insights-collector.eu01.nr-data.net
else
    echo "::error:: Region only might be one of the following options: EU, US"
    exit 1
fi

API_ENDPOINT="${API_HOST}/v1/accounts/${NEW_RELIC_ACCOUNT_ID}/events"

WORKFLOW_RUN_URL="${GITHUB_SERVER_URL}/${GITHUB_REPOSITORY}/actions/runs/${GITHUB_RUN_ID}"

timestamp="$(date +%s)"

JSON_STRING=$(jq -n \
    --arg app_name "${EVENT_APP_NAME}" \
    --arg app_type "${EVENT_APP_TYPE}" \
    --arg deploy_type "${EVENT_DEPLOY_TYPE}" \
    --arg commit_sha "${EVENT_COMMIT_SHA}" \
    --arg environment "${EVENT_ENVIRONMENT}" \
    --arg timestamp "${timestamp}" \
    --arg user "${EVENT_USER}" \
    --arg workflow_run_url "${WORKFLOW_RUN_URL}" \
'{
  eventType: "Deployment",
  app_type: $app_type,
  commit_sha: $commit_sha,
  deploy_type: $deploy_type,
  environment: $environment,
  name: $app_name,
  timestamp: $timestamp,
  user: $user,
  workflow_run_url: $workflow_run_url
}')

echo "Sending Events..."
echo "${JSON_STRING}"

result=$(echo "${JSON_STRING}"  | gzip -c | \
    curl -vvv --data-binary @- -X POST \
    -H "Content-Type: application/json"\
    -H "Api-Key: ${NEW_RELIC_INSIGHTS_API_KEY}" \
    -H "Content-Encoding: gzip" \
    "${API_ENDPOINT}"
)

exitStatus=$?

if [ ${exitStatus} -ne 0 ]; then
    echo "::error:: ${result}"
fi

exit ${exitStatus}
