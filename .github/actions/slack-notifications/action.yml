name: Slack notifications
description: |
  Retrieves a payload from a template file, fills it with the information sent
  and calls the Slack webhook to send a message
author: Infrastructure Squad
branding:
  icon: droplet
  color: blue

inputs:
  app:
    description: |
      The application's name
    required: true

  appType:
    description: |
      The application type. May be `account` or empty.
    required: false

  branch:
    description: |
      The branch's name
    required: false

  brand:
    description: |
      The brand's name
    required: false

  commit:
    description: |
      The Github's commit sha
    required: false

  environment:
    description: |
      The environment where the application is being deployed
    required: false

  event:
    description: |
      The event that triggered the workflow, automatically via pull request or
      manually dispatched by the user
    required: false

  pull-id:
    description: |
      The pull request's id
    required: false

  status:
    description: |
      The deployment status, needed to retrieve the notification template
    required: true

  type:
    description: |
      The application's type (Service, Function, Web-Static, etc)
    required: false

  user:
    description: |
      The Github's user who committed code or dispatched the workflow
    required: false

  webhook-url:
    description: |
      The Slack's webhook where to send the notification
    required: true

runs:
  using: composite
  steps:
    - id: notify
      name: Send notification
      shell: bash
      env:
        APP_TYPE: ${{ inputs.appType }}
        BRAND: ${{ inputs.brand }}
        PULL_URL: ${{ github.server_url }}/${{ github.repository }}/pull/${{ inputs.pull-id }}
        WORKFLOW_URL: ${{ github.server_url }}/${{ github.repository }}/actions/runs/${{ github.run_id }}
      run: |
        if [[ "${APP_TYPE}" == "account" && "${BRAND}" != "" ]]; then
          APP="${{ inputs.app }} (${BRAND})"
        else
          APP="${{ inputs.app }}"
        fi

        PAYLOAD=$(
          cat ${GITHUB_ACTION_PATH}/templates/${{ inputs.status }}.json.j2 | \
          sed -e "s|{{ app }}|${APP}|" \
              -e "s|{{ branch }}|${{ inputs.branch }}|" \
              -e "s|{{ commit }}|${{ inputs.commit }}|" \
              -e "s|{{ pull }}|${PULL_URL}|" \
              -e "s|{{ environment }}|${{ inputs.environment }}|" \
              -e "s|{{ event }}|${{ inputs.event }}|" \
              -e "s|{{ type }}|${{ inputs.type }}|" \
              -e "s|{{ user }}|${{ inputs.user }}|" \
              -e "s|{{ workflow-url }}|${WORKFLOW_URL}|"
        )

        curl -X POST \
          --header "Content-Type: application/json" \
          --data "$(echo ${PAYLOAD})" \
          ${{ inputs.webhook-url }}
