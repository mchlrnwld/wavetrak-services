#!/bin/bash -ex
# Script to sync one MongoDB Database with another

set -ex

usage=$(cat <<SETUSAGE
Sync surfline databases
Usage: SOURCE_DB=mongoDBConnectionString DEST_DB=mongoDBConnectionString bash ./${0##*/}
where:
    SOURCE_DB            The mongo connection string of the DB you would like to copy from
    DEST_DB              The mongo connection string of the DB you would like to copy the source DB to
SETUSAGE
)

# If If database URIs aren't provided, exit
if [ -z "${SOURCE_DB}" ] || [ -z "${DEST_DB}" ]; then
  exit 1
fi

# Make sure a TMPDIR exists to save and access dump
TMPDIR=$(mktemp -d) || exit 1;
cd ${TMPDIR}

# Dump Source db
mongodump --uri ${SOURCE_DB}

# Restore dump to Destination
mongorestore --uri ${DEST_DB} --drop

# Clean up
rm -rf ${TMPDIR}
