import { sleep, check } from 'k6';
import http from 'k6/http';
import { Rate, Trend } from 'k6/metrics';
import requests from './requests.js';

export const options = {
  stages: [
    { duration: '5m', target: 100 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
    { duration: '10m', target: 100 }, // stay at 100 users for 10 minutes
    { duration: '5m', target: 0 }, // ramp-down to 0 users
  ],
};

let currentMax = 0;
const weightedBuckets = requests(__ENV.SERVICES_URL, __ENV.WEB_URL, __ENV.BUOYWEATHER_URL, __ENV.API_KEY, __ENV.ACCESS_TOKEN).map(([path, headers, weight]) => {
  const nextMax = currentMax + weight;
  const result = [path, headers, currentMax, nextMax];
  currentMax = nextMax;
  return result;
});

const randomRequest = () => {
  const random = Math.random() * (weightedBuckets[weightedBuckets.length - 1][2] - 1);
  const bucket = weightedBuckets.find(([url, headers, min, max]) => min <= random && random < max);
  return [bucket[0], bucket[1]];
};

export default function() {
  const [url, headers] = randomRequest();
  const response = http.get(url, { headers });

  const validResponse = check(response, {
    'status is 200': r => r.status === 200,
  });

  if (!validResponse) {
    console.log(`Error: ${url}`);
  }

  sleep(0.1);
}
