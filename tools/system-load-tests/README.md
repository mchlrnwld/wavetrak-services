# Load Tests

Load tests using [k6](https://k6.io/).

## Quickstart

```sh
$ cp .env.sample .env
$ env $(xargs < .env) k6 run script.js
```

## Environment Variables

| Name | Description |
|:-----|:------------|
| `SERVICES_URL` | Services proxy URL, leave as-is in production |
| `WEB_URL` | Surfline web URL, leave as-is in production |
| `BUOYWEATHER_URL` | Buoyweather web URL, leave as-is in production |
| `ACCESS_TOKEN` | Access token for a logged-in user, described below |
| `API_KEY` | User ID for a logged-in user, described below |

### `ACCESS_TOKEN`

There may be easier ways to get an `ACCESS_TOKEN` but this will definitely work: Open the Surfline website in Chrome, log in, open Chrome Dev Tools, click the "Application" drop-down, filter by `access_token`, and use the value shown as your environment variable.

### `API_KEY`

There may also be easier ways to get an `API_KEY` but this will definitely work: Open MongoDB `UserDB` in the environment you'll be load testing. View the `UserDB.ClientIds` collection and use the `_id` value for one of the clients.

## Tips / Troubleshooting
If you are using mac os, and are load testing with increasingly higher RPS and VUs, you may hit encounter the following error, or something very similar:
```
socket: too many open files
````
This error is due to default file descriptor limit on mac os, which can be changed. This can be fixed by following these steps:

https://gist.github.com/tombigel/d503800a282fcadbee14b537735d202c
