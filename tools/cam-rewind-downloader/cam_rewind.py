#!/usr/bin/python

import getpass
import os.path
import platform
import shutil
import warnings

import arrow
import requests
from arrow.factory import ArrowParseWarning

warnings.simplefilter("ignore", ArrowParseWarning)


home_dir = getpass.getuser()
downloads = f"/Users/{home_dir}/Downloads/"
domain = "https://services.surfline.com"
dir_prefix = downloads if platform.system() == "Darwin" else ""


print(
    "\nThis script will download all rewinds from a cam between the start and "
    "end time entered. See Matt Chivers with questions or problems. Note that "
    "cam rewinds are only available for 6 days after the recording.\n"
)

cam_alias = input(
    "Enter the cam alias for your rewind. If you do not know the 'ALIAS' it "
    "can be found in the 'CAMERAS' section of Admin Tools.\neg: wc-lowers;"
    " uk-croyde; hi-pipeline"
    "\n"
)

start_date = input(
    "\nEnter the start time for your clips. "
    "Use the local time for the cam that you entered. "
    "Format like yyyy-mm-ddThh:mm (eg: 2019-06-20T08:00 ) \n"
)

end_date = input(
    "\nEnter the end time for your clips. "
    "Use the local time for the cam that you entered. "
    "Format like yyyy-mm-ddThh:mm (eg: 2019-06-20T13:00 ) \n"
)

print("\n\n")

payload = {
    "grant_type": "password",
    "username": "rewind_account@surfline.com",
    "password": "KeEuy2miE7kh4uYMv}8r",
}

req = requests.post(f"{domain}/auth/token", data=payload)

token = req.json()["access_token"]

# Retrieve the cam ID from the cam alias
cam_params = {"alias": cam_alias}
req = requests.get(f"{domain}/cameras/ValidateAlias", params=cam_params)
req.raise_for_status()
cam_id = req.json().get("_id", None)

# Retrieve the local time zone of the cam

timezones = []
if cam_id:
    params = {"select": "timezone,name,cams", "cams": cam_id}
    # Encode params into string explicitly - when requests does this,
    # it percent-encodes special chars (like `,`) in a way that's
    # incompatible with services proxy.
    params_str = "&".join((k + "=" + v) for k, v in params.items())

    req = requests.get(f"{domain}/admin/spots/", params=params_str)
    req.raise_for_status()
    spot_list = req.json()
    timezones = set([spot["timezone"] for spot in spot_list]).pop()

# Convert the time input into a unix timestamp for the local time of cam
start_in_ms = arrow.get(start_date).replace(tzinfo=timezones).timestamp * 1000
end_in_ms = arrow.get(end_date).replace(tzinfo=timezones).timestamp * 1000

time_params = {
    "startDate": start_in_ms,
    "endDate": end_in_ms,
    "allowPartialMatch": "true",
}

headers = {"X-Auth-AccessToken": token}

req = requests.get(
    f"{domain}/cameras/recording/{cam_alias}",
    headers=headers,
    params=time_params,
)

cam_rewinds = req.json()

# Display each file_name to be downloaded and whether it was already downloaded
total_files = 0
for cam_rewind in cam_rewinds:
    url = cam_rewind["recordingUrl"]
    file_name = url.rsplit("/", 1)[1]
    full_filename = dir_prefix + str(file_name)
    if os.path.isfile(full_filename) is True:
        print(f"{file_name} already downloaded.")
        continue
    else:
        print(file_name)
        total_files = total_files + 1

# Free disk space check for Mac only
if platform.system() == "Darwin":
    total, used, free = shutil.disk_usage("/")
    space_needed = total_files * 2.1e8
    if free > space_needed:
        print("\n")
    else:
        print(
            "Not enough free space to download all the requested files. "
            "Exiting script."
        )
        exit()

# Confirmation to proceed with downlaoding
print(f"Download {total_files} files?")
while True:
    confirm = input("Type 'y' to continue or 'n' to stop the script. \n")
    if confirm == "n":
        exit()
    elif confirm == "y":
        break
    else:
        continue

# Download all requested files
# If OS is Mac, it moves them to the ~/Downloads/ dir
for cam_rewind in cam_rewinds:
    url = cam_rewind["recordingUrl"]
    file_name = url.rsplit("/", 1)[1]
    full_filename = dir_prefix + str(file_name)
    if os.path.isfile(full_filename) is True:
        print(f"{file_name} already exists. Skipping to next file.")
        continue
    else:
        print("Downloading {}...".format(url))
        cam_rewind_file = requests.get(url)
        with open(file_name, "wb") as f:
            f.write(cam_rewind_file.content)
            print("Wrote {}".format(file_name))
            if platform.system() == "Darwin":
                shutil.move(
                    str(os.getcwd()) + "/" + str(file_name),
                    downloads + str(file_name),
                )
            else:
                continue

print("Downloading finished!")
if platform.system() == "Darwin":
    print("Rewinds are in your ~/Downloads/ directory.")
else:
    print("Rewinds are in the folder that this script is located in.")
