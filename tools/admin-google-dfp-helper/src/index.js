import React from 'react';
import ReactDOM from 'react-dom';
import DFPContainer from './DFPContainer';


document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.dfp-ad-container')
    .forEach(domContainer => {
        const { adidentifier:adIdentifier, adattributes, enablesinglerequest = 'true' } = domContainer.dataset;
        const enableSingleRequest = (enablesinglerequest === 'true');
        const adAttributesParsed = adattributes ? JSON.parse(adattributes) : null
        ReactDOM.render(
            <DFPContainer  adIdentifier={adIdentifier} adAttributes={adAttributesParsed} enableSingleRequest={enableSingleRequest}/>,
            domContainer
        );
    });
});

