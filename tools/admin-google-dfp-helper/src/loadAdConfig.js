import adConfigMap from './adConfigMap';

const loadAdConfig = (
  adIdentifier,
  adTargets = [],
  adViewType = null,
) => {
  const adConfigResult = {
    ...adConfigMap[adIdentifier],
    adTargets: adTargets,
    adViewType: adViewType || adConfigMap[adIdentifier].adViewType,
  };
  return adConfigResult;
};

export default loadAdConfig;
