import React, { Component } from 'react';
import { GoogleDFP } from '@surfline/quiver-react';
import loadAdConfig from './loadAdConfig.js';

class DFPContainer extends Component {
  constructor(props){
    super(props);
    this.state = {
      isEmpty: false,
      fallbackAdConfig: null
    }
  };
  dfpEventListener = ({ isEmpty, size, slot }) => {
    const { adAttributes } = this.props;
    const fallbackAdUnit = adAttributes ? adAttributes.fallbackAdUnit : null;
    if(isEmpty && fallbackAdUnit){
      const adTargets = adAttributes ? adAttributes.adTargets : [];
      const fallbackAdConfig = loadAdConfig(fallbackAdUnit, adTargets);
      this.setState({
        isEmpty,
        fallbackAdConfig
      });
      console.log('fallbackAdConfig', fallbackAdConfig);
    }
  };
  render() {
    const { adIdentifier, adAttributes, enableSingleRequest = true } = this.props;
    console.log('adIdentifier', adIdentifier);
    console.log('adAttributes', adAttributes);
    const adTargets = adAttributes ? adAttributes.adTargets : [];
    const adConfig = loadAdConfig(adIdentifier, adTargets);
    const { isEmpty, fallbackAdConfig } = this.state;
    return (
        <div>
        {
          isEmpty ?
            <GoogleDFP  adConfig={fallbackAdConfig} enableSingleRequest={enableSingleRequest}/>
            :
            <GoogleDFP  adConfig={adConfig}  eventListeners={[{ type: 'slotRenderEnded', listener: this.dfpEventListener }]} enableSingleRequest={enableSingleRequest}/>
        }
        </div>
    );
  }
}

export default DFPContainer;
