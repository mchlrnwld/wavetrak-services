export default  {
    BW_Top: {
    adUnit: '/1024858/BW_Top',
    adSizes: [[728,90], [320,250], [970,250], [990,90], [1170,90]],
    adSizeDesktop: [[728,90], [320,250], [970,250], [990,90], [1170,90]],
    adSizeMobile: [320,50],
    adViewType: 'BW_Top',
  },
  BW_News_Top: {
    adUnit: '/1024858/BW_News_Top',
    adSizes: [[990,90], [728,90]],
    adSizeDesktop: [[990,90], [728,90]],
    adSizeMobile: [320,50],
    adViewType: 'BW_News_Top',
  },
  BW_ConditionalWeatherFlag: {
    adUnit: '/1024858/BW_ConditionalWeatherFlag',
    adSizes: [[572,84], [400,59], [300,44]],
    adSizeDesktop: [[572,84], [400,59], [300,44]],
    adSizeMobile: [300,44],
    adViewType: 'BW_ConditionalWeatherFlag',
  },
  BW_Top_RightRail: {
    adUnit: '/1024858/BW_Top_RightRail',
    adSizes: [[300,600], [300,250]],
    adSizeDesktop: [[300,600], [300,250]],
    adSizeMobile: [320,50],
    adViewType: 'BW_Top_RightRail',
  },
  BW_Bottom_RightRail: {
    adUnit: '/1024858/BW_Bottom_RightRail',
    adSizes: [[300,250]],
    adSizeDesktop: [[300,250]],
    adSizeMobile: [320,50],
    adViewType: 'BW_Bottom_RightRail',
  },
  BW_Bottom: {
    adUnit: '/1024858/BW_Bottom',
    adSizes: [[990,90], [728,90]],
    adSizeDesktop: [[990,90], [728,90]],
    adSizeMobile: [320,50],
    adViewType: 'BW_Bottom',
  },
};
