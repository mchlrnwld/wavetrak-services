import adConfigbw from './adConfigMap-bw.js';
import adConfigfs from './adConfigMap-fs.js';
import adConfigsl from './adConfigMap-sl.js';

export default {
    ...adConfigbw,
    ...adConfigfs,
    ...adConfigsl,
};
