export default  {
  SL_Legacy_CamRewind: {
    adUnit: '/1024858/SL_Legacy_CamRewind',
    adSizes: [[300,250]],
    adSizeDesktop: [[300,250]],
    adSizeMobile: [300,250],
    adViewType: 'SL_Legacy_CamRewind',
  },
};
