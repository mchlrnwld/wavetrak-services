# GoogleDFP Helper

This is a light weight React helper that would display/load DFP ads when provided with the appropriate Ad configuration.

It uses the `GoogleDFP` component internally from the `@surfline/quiver-react` npm package. More info about this package can be found here - https://github.com/Surfline/quiver/tree/master/react

## Developing
Uses node:v10

```
npm start 
```
This commands starts the webpack dev server in port 8080.

## Build
```
npm run build
```
This command bundles the app into a minified js file - `dist/googledfp_helper.js`.


## Usage


Include the `googledfp_helper.js` file in a script tag as below:

```
<script src="http://cdn-staging.buoyweather.com/static/googledfp_helper.js">
</script>
```
In the html where you want to show the dfp ad, place a `div` as below:

```
<div class="dfp-ad-container" data-adidentifier="BW_Top"></div>
```
This would render a DFP ad if there is a valid `adConfig` defined for the `adidentifier` that is passed in.

Ad Configs are defined in 2 brand specific files - `adConfigMap-bw.js` and `adConfigMap-fs.js`.

NOTE: When the DOM Content has finished loading, we iterate all elements that have a class `.dfp-ad-container` and mounts a React Component for each element.

## TODO

In Staging, `googledfp_helper.js` file is hosted in an S3 bucket - `bw-public-static-staging`.

Object URL: `https://bw-public-static-staging.s3-us-west-1.amazonaws.com/static/googledfp_helper.js`.

Currently we have to manually upload the file to this location. Instead of this we should create a Jenkins job that:

    1. Builds the app (npm run build)
    2. Copy the `googledfp_helper.js` file to an S3 location
    3. Invalidate the Cloudfront cache for this path `static/googledfp_helper.js`

There is a CDN (`cdn-staging.buoyweather.com`) that sits in front of this S3 bucket. 

Since Fishtrack doesn't have a dedicated S3 bucket and Cloudfront distribution, let's keep this file hosted in BW S3 bucket itself.

For Prod we use S3 bucket - `bw-public-static-prod` and CDN - `cdn-prod.buoyweather.com`.
