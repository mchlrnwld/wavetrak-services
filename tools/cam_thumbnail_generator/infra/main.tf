resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda_${var.environment}"
  assume_role_policy = file("${path.module}/resources/lambda-policy.json")
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.func.arn
  principal     = "s3.amazonaws.com"
  source_arn    = var.cam_thumb_bucket_arn
}

resource "aws_lambda_function" "func" {
  s3_bucket     = var.cam_thumb_lamba_bucket_name
  s3_key        = var.cam_thumb_lamba_artifact_name
  function_name = "cam_thumb_gen_s3_${var.environment}"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "cam_recording_s3_trigger.lambda_handler"
  runtime       = "python2.7"
  memory_size   = 512
  timeout       = 30

  vpc_config {
    subnet_ids = var.instance_subnets

    security_group_ids = [
      var.sg_all_servers,
      var.internal_sg_group,
    ]
  }

  environment {
    variables = {
      CAM_THUMBNAIL_BUCKET = var.cam_thumb_bucket_name
      camThumbnailBucket   = var.cam_thumb_bucket_name
      cameraServiceUrl     = var.camera_service_url
      thumbnailUrl         = var.thumbnail_url
      recordingUrl         = var.recording_url
    }
  }
}

# Create SNS topic.
module "sns_topic" {
  source      = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sns"

  topic_name  = "sl-live-cam-archive-s3-sns"
  environment = var.environment
}

# Allow bucket to publish to SNS.
resource "aws_sns_topic_policy" "topic_policy" {
  arn = module.sns_topic.topic
  policy = templatefile("${path.module}/resources/sns-topic-policy.json", {
    cam_rewind_bucket_name = var.cam_rewind_bucket_name
    sns_topic = module.sns_topic.topic
  })
}

# Subscribe bucket to SNS topic.
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = var.cam_rewind_bucket_name

  topic {
    topic_arn     = module.sns_topic.topic
    events        = ["s3:ObjectCreated:*"]
    filter_prefix = "live/"
    filter_suffix = ".mp4"
  }
}

# Allow Lambda to be invoked by an SNS topic
resource "aws_lambda_permission" "lambda_permission" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  principal     = "sns.amazonaws.com"
  function_name = aws_lambda_function.func.function_name
  source_arn    = module.sns_topic.topic
}

# Subscribe lambda to SNS topic.
resource "aws_sns_topic_subscription" "topic_subscription" {
  topic_arn = module.sns_topic.topic
  endpoint  = aws_lambda_function.func.arn
  protocol  = "lambda"
}

# Allow lambda to write to S3.
resource "aws_iam_policy" "s3_policy" {
  name        = "s3_policy_cam-thumb-gen_${var.environment}"
  description = "policy to let lambda publish to s3"
  policy = templatefile("${path.module}/resources/s3-policy.json", {
    cam_thumb_bucket_arn = var.cam_thumb_bucket_arn
  })
}

resource "aws_iam_role_policy_attachment" "lambda_s3_role_policy" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.s3_policy.arn
}

# Allow lambda to access VPC.
resource "aws_iam_role_policy_attachment" "lambda_execution_role_policy" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_cloudwatch_log_group" "main" {
  name              = "/aws/lambda/${aws_lambda_function.func.function_name}"
  retention_in_days = 3
}
