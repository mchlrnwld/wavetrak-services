variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "cam_thumb_lamba_bucket_name" {
}

variable "cam_thumb_bucket_name" {
}

variable "cam_thumb_bucket_arn" {
}

# variable "cam_thumb_url" {
# }

variable "sg_all_servers" {
}

variable "cam_rewind_bucket_name" {
}

variable "instance_subnets" {
}

variable "internal_sg_group" {
}

variable "camera_service_url" {
}

variable "cam_thumb_lamba_artifact_name" {
  default = "CamThumbGenLambda.zip"
}

variable "thumbnail_url" {
  default = "https://camstills.cdn-surfline.com"
}

variable "recording_url" {
  default = "https://camrewinds.cdn-surfline.com"
}
