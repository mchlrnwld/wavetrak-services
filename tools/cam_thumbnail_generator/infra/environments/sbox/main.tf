provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/cam-thumb-gen/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "cam-thumb-gen" {
  source = "../../"

  company     = "sl"
  application = "cam-thumb-gen"
  environment = "sandbox"

  cam_rewind_bucket_name      = "cam-thumb-gen-dev"
  cam_thumb_lamba_bucket_name = "cam-thumb-gen-dev"
  cam_thumb_bucket_name       = "cam-thumb-gen-dev"
  cam_thumb_bucket_arn        = "arn:aws:s3:::cam-thumb-gen-dev"

  thumbnail_url      = "https://s3-us-west-1.amazonaws.com/cam-thumb-gen-dev"
  camera_service_url = "http://cameras-service.sandbox.surfline.com"

  internal_sg_group = "sg-90aeaef5"
  sg_all_servers    = "sg-91aeaef4"
  instance_subnets  = ["subnet-0909466c", "subnet-f2d458ab"]
}
