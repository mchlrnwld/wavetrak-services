provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/cam-thumb-gen/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "cam-thumb-gen" {
  source = "../../"

  company     = "sl"
  application = "cam-thumb-gen"
  environment = "prod"

  cam_rewind_bucket_name        = "sl-live-cam-archive-prod"
  cam_thumb_lamba_bucket_name   = "sl-artifacts-prod"
  cam_thumb_lamba_artifact_name = "lambda/CamThumbGenLambda.zip"
  cam_thumb_bucket_name         = "sl-cam-thumbnails-prod"
  cam_thumb_bucket_arn          = "arn:aws:s3:::sl-cam-thumbnails-prod"

  thumbnail_url      = "https://camstills.cdn-surfline.com"
  camera_service_url = "http://cameras-service.prod.surfline.com"

  internal_sg_group = "sg-a5a0e5c0"
  sg_all_servers    = "sg-a4a0e5c1"
  instance_subnets  = ["subnet-d1bb6988", "subnet-85ab36e0"]
}
