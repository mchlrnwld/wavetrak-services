# -*- coding: utf-8 -*-
import calendar
import datetime
import json
import logging
import os
import re
import subprocess
import tempfile
import time

import boto3
import requests


# Global variables.
S3 = boto3.resource("s3")
S3_URL = "https://s3-us-west-1.amazonaws.com"
RECORDING_URL = ("http://cameras-service.prod.surfline.com/"
                 "cameras/recording/id/")
VALIDATE_ALIAS_URL = ("http://cameras-service.prod.surfline.com"
                      "/cameras/ValidateAlias?alias=")
s3_bucket = os.environ["CAM_THUMBNAIL_BUCKET"]
FULL_IMAGE_FILENAME = "latest_full.jpg"
SMALL_IMAGE_FILENAME = "latest_small.jpg"

# These are populated by temp.mkdtemp() in lambda_handler()
FULL_IMAGE_PATH = ""
SMALL_IMAGE_PATH = ""


# Define location of included ffmpeg binaries
LAMBDA_TASK_ROOT = os.environ.get('LAMBDA_TASK_ROOT', '')
if LAMBDA_TASK_ROOT:
    FFMPEG = os.path.join(LAMBDA_TASK_ROOT, 'ffmpeg')
    FFPROBE = os.path.join(LAMBDA_TASK_ROOT, 'ffprobe')
else:
    FFMPEG = 'ffmpeg'
    FFPROBE = 'ffprobe'

# Remove handler so that logging works correctly within Lambda
root = logging.getLogger()
if root.handlers:
    for handler in root.handlers:
        root.removeHandler(handler)

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


def add_cam_still(local_file_name, image_size, camera_id, date, time):
    """
        Upload  thumbnails to the S3 bucket.

        Thumbnails are added as archives, using date / time from original
        file: e.g. ${bucket}/acam/2018/02/13/acam_20180213T1930_full.jpg
    """
    # Grab current time and set image name and archive name.
    now = datetime.datetime.utcnow()
    expires = now + datetime.timedelta(minutes=15)
    year = date[:4]
    month = date[4:6]
    day = date[6:8]

    primary_thumbnail = "{0}/latest_{1}.jpg".format(camera_id, image_size)
    archive_name = "{0}/{1}/{2}/{3}/{4}.{5}T{6}_{7}.jpg".format(
        camera_id, year, month, day, camera_id, date, time, image_size)

    # Add primary thumbnail to S3.
    S3.Object(s3_bucket, primary_thumbnail) \
        .put(Body=open(local_file_name, "rb"),
             Expires=expires,
             ACL="public-read",
             ContentType="image")
    logger.info("Uploaded {}".format(primary_thumbnail))

    # Add archive thumbnail to S3.
    S3.Object(s3_bucket, archive_name) \
        .put(Body=open(local_file_name, "rb"),
             ACL="public-read",
             ContentType="image")
    logger.info("Uploaded {}".format(archive_name))

    return archive_name


def get_camera_id_from_alias(alias):
    """
    Using alias get cameraId from cameras-service API.
    """
    response = requests.get(VALIDATE_ALIAS_URL + alias)
    logger.info(response.text)
    response.raise_for_status()

    camera_id = response.json()['_id']
    logger.info("Successfully requested cameraId: {}".format(camera_id))
    return camera_id


def extract_video_spec(recording_url):
    """
        Use ffprobe to extract information about a recording,
        given its URL.
    """
    # First verify whether ffprobe binary is set OK.
    try:
        run_subprocess([FFPROBE, "-version"])

    except (subprocess.CalledProcessError, OSError) as e:
        run_subprocess(["cp", "ffprobe", "/tmp/ffprobe"])
        run_subprocess(["chmod", "755", "/tmp/ffprobe"])

    # Then, extract videos specs.
    cmd = [FFPROBE, "-v", "error",
           "-select_streams", "v:0",
           "-show_entries",
           "stream=height,width,duration",
           "-of", "flat=s=_",
           recording_url]
    video_specs = subprocess.check_output(cmd)
    _, width, _, height, _, _, duration_seconds, _, _ = \
        re.split('=|\'|\"|\\n', video_specs.decode("utf-8"))

    return width, height, duration_seconds


def save_recording(rec_file_name, rec_path, camera_alias, camera_id, large_tb, small_tb, rec_url):
    """
        Post the thumbnail data to the recording URL.
    """
    # Create data dictionary.
    width, height, duration_seconds = extract_video_spec(rec_url)

    # rec_file_name new format:
    #     hbpierns.20181031T025200051.mp4
    #     hbpiernscam.stream.20181031T025200051.mp4
    # rec_file_name old format:
    #     hbpiernscam.20180815T140019.mp4
    recording_start_timestamp = rec_file_name.split('.')[-2]

    start_date = timestamp_to_epoch(recording_start_timestamp)
    recordingUrl = "{0}/{1}".format(
        os.environ["recordingUrl"], rec_path)

    thumbnail_url = os.environ["thumbnailUrl"]
    data = {
        "alias": camera_alias,
        "thumbLargeUrl": "{0}/{1}".format(thumbnail_url, large_tb),
        "thumbSmallUrl": "{0}/{1}".format(thumbnail_url, small_tb),
        "recordingUrl": recordingUrl,
        "startDate": start_date,
        "endDate": start_date + int(float(duration_seconds)) * 1000,
        "resolution": "{0}x{1}".format(width, height),
    }
    logger.info(data)

    # POST recording info.
    req_url = requests.compat.urljoin(RECORDING_URL, camera_id)
    r = requests.post(req_url, data=data)
    logger.info(r.text)

    if r.status_code == 200:
        logger.info("{} saved.".format(camera_alias))
        return True
    else:
        raise Exception(r.text)


def thumbnail_resize(filename, size=360):
    """
        Resize an image to a given size.
    """
    cmd = [FFMPEG,
           "-i", filename,
           "-vf", "scale={}:-1".format(size),
           "-y", SMALL_IMAGE_PATH]

    run_subprocess(cmd)


def timestamp_to_epoch(timestamp, timestamp_format="%Y%m%dT%H%M%S%f"):
    """
        Return the linux epoch time to the given
        timestamp format (e.g. 20180227T201716).
    """
    dt = time.strptime(timestamp, timestamp_format)
    return calendar.timegm(dt) * 1000


def generate_thumbnails(rec_filename, rec_url):
    """
        Create a thumbnail from the video.
    """
    logger.info("Generating thumbnails for {}".format(rec_filename))

    thumb_create = [FFMPEG,
                    "-v", "info",
                    "-ss", "00:00:00",
                    "-i", rec_url,
                    "-vframes", '1',
                    "-y", FULL_IMAGE_PATH]

    thumb_chmod = ["chmod", "777", FULL_IMAGE_PATH]

    run_subprocess(thumb_create)
    run_subprocess(thumb_chmod)


def generate_resizes(rec_filename, camera_id):
    """
        Resize thumbnail to latest_full and latest_small.
    """
    if os.path.exists(FULL_IMAGE_PATH):
        # rec_file_name new format:
        #     hbpierns.20181031T025200051.mp4
        #     hbpiernscam.stream.20181031T025200051.mp4
        # rec_file_name old format:
        #     hbpiernscam.20180815T140019.mp4
        recording_start_timestamp = rec_filename.split('.')[-2]
        (date, time) = recording_start_timestamp.split('T')

        thumbnail_resize(FULL_IMAGE_PATH)
        large_thumb = add_cam_still(local_file_name=FULL_IMAGE_PATH,
                                    image_size="full",
                                    camera_id=camera_id,
                                    date=date,
                                    time=time)

        small_thumb = add_cam_still(local_file_name=SMALL_IMAGE_PATH,
                                    image_size="small",
                                    camera_id=camera_id,
                                    date=date,
                                    time=time)
        return large_thumb, small_thumb

    else:
        logger.error("Failed to take frame from: {}".format(
            rec_filename))
        raise Exception("Path ({}) not found".format(FULL_IMAGE_PATH))


def run_subprocess(cmd):
    """
        Given a list of subprocess commands and flags,
        run a subprocess thread for that cmd.
    """
    try:
        logger.info("Running subprocess: {0}".format(" ".join(cmd)))
        with open(os.devnull) as devnull:
            result = subprocess.check_output(
                cmd, stdin=devnull, stderr=subprocess.STDOUT)
            print(result)
        return True

    except (subprocess.CalledProcessError, OSError) as e:
        # When the lambda function is deployed at AWS,
        # every file needs explicit permissions to be modified
        try:
            logger.debug("Changing {} permission.".format(cmd[0]))

            # Moving binary to the work destination
            if os.path.isfile(cmd[0]):
                subprocess.check_output(
                    ["cp", cmd[0], "/tmp/{}".format(cmd[0])])
                subprocess.check_output(
                    ["chmod", "755", "/tmp/{}".format(cmd[0])])

            else:
                bin_dest = cmd[0]

            # Running again the command with the new binary
            cmd[0] = bin_dest
            with open(os.devnull) as devnull:
                subprocess.check_output(
                    cmd, stdin=devnull, stderr=subprocess.STDOUT)

        # We add a catch all here because this block will
        # only be reached if the permission change don't work.
        except Exception as e:
            logger.error(e)


def extract_event_data(event):
    """
        Given an event that triggers the lambda function,
        extract the necessary data from its contract.
    """
    try:
        message = json.loads(event["Records"][0]["Sns"]['Message'])
        recording_full_path = message["Records"][0]["s3"]["object"]["key"]
        bucket_name = message["Records"][0]["s3"]["bucket"]["name"]
        return recording_full_path, bucket_name

    except (KeyError, IndexError) as e:
        logger.error(e)
        raise Exception(e)


def lambda_handler(event, context=None):
    """
        The entry point for this lambda function.
        This will be triggered from a AWS s3 ObjectCreate.
    """
    global FULL_IMAGE_PATH, SMALL_IMAGE_PATH

    logger.info('Event: {0}'.format(
        json.dumps(event, indent=4, sort_keys=True)))

    # Set the Lambda function environment.
    run_subprocess(["rm", "-rf", '/tmp/*'])
    run_subprocess(["ls", "-lah", '/tmp/'])

    # Work in unique temporary directory
    working_dir = tempfile.mkdtemp(dir='/tmp')
    run_subprocess(["chmod", "777", working_dir])
    FULL_IMAGE_PATH = "{}/latest_full.jpg".format(working_dir)
    SMALL_IMAGE_PATH = "{}/latest_small.jpg".format(working_dir)

    # Extract data.
    recording_full_path, bucket_name = extract_event_data(event)
    rec_file_name = recording_full_path.split('/')[-1]
    recording_url = "{0}/{1}/{2}".format(S3_URL,
                                         bucket_name, recording_full_path)

    # Extract camera_alias and get camera_id from cameras-service
    camera_alias = rec_file_name.split('.')[0]
    camera_id = get_camera_id_from_alias(camera_alias)

    # Extract thumbnail and resizes.
    generate_thumbnails(rec_file_name, recording_url)
    large_thumb, small_thumb = generate_resizes(rec_file_name, camera_id)

    # Update data to s3.
    save_recording(rec_file_name, recording_full_path, camera_alias, camera_id,
                   large_thumb, small_thumb, recording_url)

    # Clean up.
    run_subprocess(["rm", "-rfv", '/tmp/*'])
