node {
    checkout scm
    GString workingDirectory = "${pwd()}/tools/cam_thumbnail_generator"
    String dependenciesZipName = 'CamThumbGenDependencies.zip'
    String producerLambdaZipName = 'cam-thumbnail-producer-lambda.zip'
    String consumerLambdaZipName = 'cam-thumbnail-consumer-lambda.zip'
    String producerLambdaName = "sl-cam-thumbnail-producer-lambda-${env.BUILD_ENVIRONMENT}"
    String consumerLambdaName = "sl-cam-thumbnail-consumer-lambda-${env.BUILD_ENVIRONMENT}"

    stage('Build') {
        sh "ls -lah"
        dir(workingDirectory) {
            sh "ls -lah"
            sh "zip -r9 ${dependenciesZipName} cam_stream *.py"
            sh "cp ${dependenciesZipName} ${producerLambdaZipName} & cp ./${dependenciesZipName} ${consumerLambdaZipName}"
            sh 'mkdir target'
            sh 'mv *.zip target/'
            stash name: 'deployArtifacts'
        }
    }
    stage('Test') {
        unstash 'deployArtifacts'
        dir(workingDirectory) {
            sh(
                    script: "zip -T target/${producerLambdaZipName}",
                    returnsStatus: true
            )
            sh(
                    script: "zip -T target/${consumerLambdaZipName}",
                    returnsStatus: true
            )
        }
    }
    stage('Deploy') {
        unstash 'deployArtifacts'
        dir(workingDirectory) {
            profileArgument = " --profile ${env.BUILD_ENVIRONMENT} "

            // Lambda update-function-configuration overwrites all variables
            lambdaFunctions = sh(
                    script: "aws ${profileArgument} lambda list-functions",
                    returnStdout: true
            )

            String camThumbnailSnsArn = ""
            String camThumbnailBucket = ""
            String[] outputLines = lambdaFunctions.split("\n");
            for (String line : outputLines) {
                // pulling out env variables instead of json parsing
                if (line =~ /camThumbnailSnsArn/) {
                    camThumbnailSnsArn = line.split('"')[3].trim()
                } else if (line =~ /camThumbnailBucket/) {
                    camThumbnailBucket = line.split('"')[3].trim()
                }
            }

            CAMERA_API = 'http://cameras-service.staging.surfline.com';
            if (env.BUILD_ENVIRONMENT == 'prod') {
                CAMERA_API = 'http://cameras-service.prod.surfline.com';
            }

            println(camThumbnailSnsArn)
            println(camThumbnailBucket)

            sh 'ls -lah'

            producerLambdaArgument = " --function-name ${producerLambdaName} "
            consumerLambdaArgument = " --function-name ${consumerLambdaName} "
            producerLocalZipArgument = " --zip-file fileb://target/${producerLambdaZipName} "
            consumerLocalZipArgument = " --zip-file fileb://target/${consumerLambdaZipName} "
            camApiKeyArgument = "CAMERA_API=${CAMERA_API}"
            producerEnvironmentVariables = " --environment Variables=\\{${camApiKeyArgument},camThumbnailSnsArn=${camThumbnailSnsArn}\\} "
            consumerEnvironmentVariables = " --environment Variables=\\{camThumbnailBucket=${camThumbnailBucket}\\} "
            updateProducerFunctionConfigCommand = "aws ${profileArgument} lambda update-function-configuration ${producerLambdaArgument} ${producerEnvironmentVariables}"
            updateProducerFunctionCodeCommand = "aws ${profileArgument} lambda update-function-code ${producerLambdaArgument} ${producerLocalZipArgument}"
            updateConsumerFunctionConfigCommand = "aws ${profileArgument} lambda update-function-configuration ${consumerLambdaArgument} ${consumerEnvironmentVariables}"
            updateConsumerFunctionCodeCommand = "aws ${profileArgument} lambda update-function-code ${consumerLambdaArgument} ${consumerLocalZipArgument}"

            sh updateProducerFunctionConfigCommand
            sh updateProducerFunctionCodeCommand
            sh updateConsumerFunctionConfigCommand
            sh updateConsumerFunctionCodeCommand
        }
    }
}
