# Cam Thumbnail Generation

## Table of Contents
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Summary](#summary)
- [Developing](#developing)
- [Deploying](#deploying)
  - [Jenkins](#jenkins)
- [Rebuilding Dependencies](#rebuilding-dependencies)
- [Notes on FFMpeg / FFProbe](#notes-on-ffmpeg--ffprobe)
- [Monitoring](#monitoring)
  - [Configuration](#configuration)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Summary

This Lambda function `(cam_thumb_gen_s3_{env})` generates thumbnails for recordings, saving them in a given S3 bucket. The function is triggered by a S3 `ObjectCreate`.

The resulting thumbnail URLs can be seen in the given cam's recording URL.

Example:

<http://cameras-service.prod.surfline.com/cameras/recording/hbpiersscam>

## Developing

Create a virtual environment and install Python's dependencies:

```bash
# set up and activate virtual env
virtualenv --python=<path_to_python2.7> venv
source venv/bin/activate

# install dependencies
pip install -r requirements.txt
```

Lint your code:

```bash
make fixlint
make lint
```

Invoke the lambda function locally:

```bash
make invoke
```

Note that this will invoke the lambda function with the s3 bucket `cam-thumb-gen-dev` (hardcoded in `Makefile`).

## Deploying

This Lambda doesn't have a working Jenkins deploy. To deploy manually, run the following:

```bash
# Set env vars to access appropriate AWS account

# Build the Lambda zipfile
make build

# Update the Lambda function (ENV = sandbox|staging|prod)
ENV=sandbox make deploy
```

### Jenkins

NOTE: The following Jenkins build information is no longer relevant. It will soon be removed from this repo.

`Jenkinsfile.groovy` formerly defined build and deployment steps in this Jenkins build: [surfline/cam-thumbnail-lambdas](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/cam-thumbnail-lambdas/)

## Rebuilding Dependencies

This Lambda function depends on a prebuilt `ffmpeg` binary and Python packages with natively-compiled dependencies. These dependencies are stored in the file `CamThumbGenDependencies.zip`.

Usually it's sufficient to use the version of this file included with this repo. If this file needs to be rebuilt, it can be done with the following:

```bash
make clean
make docker
```

## Notes on FFMpeg / FFProbe

- FFMpeg / FFProbe need to be statically compiled for AWS Lambda.
- FFMpeg reports duration / frame size when generating the thumbnail.
- FFProbe readily outputs duration / frame size with minimal fuss.

## Monitoring

This Lambda has a [New Relic alert policy](https://one.newrelic.com/launcher/nrai.launcher?pane=eyJuZXJkbGV0SWQiOiJhbGVydGluZy11aS1jbGFzc2ljLnBvbGljaWVzIiwibmF2IjoiUG9saWNpZXMiLCJzZWxlY3RlZEZpZWxkIjoidGhyZXNob2xkcyIsInBvbGljeUlkIjoiMjQ4MzMzIiwiY29uZGl0aW9uSWQiOiIxNjU3MjM0MiJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucmFpLm5hdmlnYXRpb24tYmFyIiwibmF2IjoiUG9saWNpZXMifQ==&platform[accountId]=356245) that monitors the invocation error count.

### Configuration

The alert should be configured at a threshold that indicates a diminished functionality state. Developers should proactively update this threshold as error patterns change over the life of the Lambda function.

Because of reporting delays from AWS to New Relic, the `aggregation window` value should be set to 20 minutes.
 
