#! /usr/bin/env python

import optparse
import urllib2
import urllib
import json

default_username = 'drew@surfline.com'
default_password = '123456'

desc = """Get an auth token for a given environment"""
usage = "usage: %prog [options]"

parser = optparse.OptionParser(description=desc, usage=usage)

parser.add_option('--env',
                  dest='env',
                  help='Environment to authenticate in.',
                  default='sandbox')

parser.add_option('--username',
                  dest='username',
                  help='Username to login as.',
                  default=default_username)

parser.add_option('--password',
                  dest='password',
                  help='Password to authenticate with.',
                  default=default_password)

parser.add_option('--get',
                  dest='get',
                  help='Attribute to return from auth endpoint. Options are ' +
                  'token_type,  access_token, expires_in, refresh_token',
                  default='access_token')


(opts, args) = parser.parse_args()

hosts = {
    'sandbox': 'https://public-services-sandbox.surfline.com',
    'qa': 'https://public-servics-qa.surfline.com',
    'staging': 'https://services-staging.surfline.com',
    'production': 'https://public-services.surfline.com'
}

endpoint = '/auth/token/'

def gen_token(env):
    url = hosts[env] + endpoint
    data = {
        'grant_type': 'password',
        'password': opts.password,
        'username': opts.username
    }

    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }

    data = urllib.urlencode(data)
    req = urllib2.Request(url, data, headers)
    response = urllib2.urlopen(req)

    return response.read()

if __name__ == '__main__':
    response = json.loads(gen_token(opts.env))
    print response[opts.get]
