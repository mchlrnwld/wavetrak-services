# ElasticSearch Migration Utilities

## Purpose

Create a central repository for ElasticSearch related changes.
Currently this is setup to be the source of truth for the index/type mappings and allows us to make changes via Github and to enable a consistent review process for changes to ES Mappings.

Near term the idea is to create tooling scripts and a Jenkins job that will allow merged mappings (master only!) to be applied in each of our environments, making it faster to get these changes to the environments that will require the changes.

## Conventions

In the mappings sub-folder save .json files with {index}\_{type}.json that have the associated ElasticSearch mappings. When adding new mapping file, please pull from the production versions to ensure we are using the most up-to-date mapping values.

Using prod aws credentials, go here to pull down any existing mappings (where {index} and {type} are the actual values you are after)

```
https://search-sl-es-prod-xsw3q37vso7bm7wjooudtsrw7y.us-west-1.es.amazonaws.com/{index}/_mapping/{type}
```

(The simplest way to do this is using Postman)
https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/129390666/How+to+sign+Elasticsearch+requests+using+Postman

## Applying a new mapping

To apply a mapping the [misc/run-elasticsearch-migration](https://surfline-jenkins-master-prod.surflineadmin.com/job/misc/job/run-elasticsearch-migration/) Jenkins job can be used.

**Note: for production changes new mappings must be merged into master.**

## New indexes (with mappings)

If a mapping needs to be created, but does not already have an index, the index must be created first. This should be a consideration when creating tooling scripts.

To test if an index exists, using a HEAD request, hit the URL for the target environment with only the index

Example:

```
https://search-sl-es-prod-xsw3q37vso7bm7wjooudtsrw7y.us-west-1.es.amazonaws.com/{index}
```

If the http status is 404, it does not exist, otherwise if it returns a 200 status, it does.
