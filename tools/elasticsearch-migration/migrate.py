"""Provides ElasticSearch utility functions"""
import json
import argparse
import boto3
from elasticsearch import Elasticsearch, RequestsHttpConnection, client
from requests_aws4auth import AWS4Auth

ENDPOINTS = {
    "sandbox": "search-sl-es-sbox-3i7itfnhgfzidlqxbx2ggpi3rm",
    "staging": "search-sl-es-staging-pdnwispkwdzmj5fuvyp74zyjdy",
    "prod": "search-sl-es-prod-xsw3q37vso7bm7wjooudtsrw7y"
}


def connect(environment):
    """Returns an Elasticsearch connection"""

    endpoint = ('https://%s.us-west-1.es.amazonaws.com' %
                ENDPOINTS.get(environment))

    session = boto3.session.Session()
    credentials = session.get_credentials()

    awsauth = AWS4Auth(credentials.access_key,
                       credentials.secret_key,
                       'us-west-1', 'es',
                       session_token=credentials.token)

    es_conn = Elasticsearch(endpoint,
                            http_auth=awsauth,
                            verify_certs=True,
                            connection_class=RequestsHttpConnection)
    return es_conn


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Run migrations against ElasticSearch indices')
    parser.add_argument("--env", help="elastic search environment")
    parser.add_argument("--index", help="index for the migration")
    parser.add_argument("--doc_type", help="document type")

    args = parser.parse_args()
    es_client = connect(args.env)
    indices = client.IndicesClient(es_client)
    mapping_body = open('mappings/%s_%s.json' %
                        (args.index, args.doc_type), mode='r')

    body = json.loads(mapping_body.read())

    print indices.put_mapping(doc_type=args.doc_type,
                              body=body, index=args.index)
