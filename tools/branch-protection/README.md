# Description
The branch_prot.py script will query all Surfline repos and add branch protection settings as defined.

The branch_prot_test.py script will push defined branch protection settings to a single repo.  This can be used to test your token and the settings you want to push.

Be sure to check the requirements file for needed modules.
