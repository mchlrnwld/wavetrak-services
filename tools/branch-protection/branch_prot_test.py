#!/usr/bin/env python

# Pulled from https://github.com/BrinnerTechie/github-branch-protection-rules
# Run pip install PyGithub to install module

import os

from github import Github

# Set your token as an environment variable before running
# From terminal:"GITHUB_TOKEN='<token>' python3 branch_prot_test.py"

token = os.environ['GITHUB_TOKEN']
gh = Github(token)


# This is a test run on a single repo if you want to check token permissions
# or setting changes.  Use 'Surfline/<repo name>'


def change_protected_branch_settings_test():
    try:
        repo = gh.get_repo('Surfline/test-branch-prot')
        branch = repo.get_branch('master')
        branch.edit_protection(
            required_approving_review_count=2,
            dismiss_stale_reviews=True,
            enforce_admins=False,
        )
        print(f"Edited the branch protection rules for: {repo.name}")
    except Exception as err:
        print(f"Error: {err}")
        print("Defined repo doesn't exist or has no master branch")


if __name__ == '__main__':
    change_protected_branch_settings_test()
