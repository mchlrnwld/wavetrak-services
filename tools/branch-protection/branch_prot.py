#!/usr/bin/env python

# Pulled from https://github.com/BrinnerTechie/github-branch-protection-rules
# Run pip install PyGithub to install module

import os

import yaml
from github import Github
from github.GithubException import GithubException

# Set your token as an environment variable before running
# From terminal: "GITHUB_TOKEN='<token>' python3 branch_prot.py"

token = os.environ['GITHUB_TOKEN']
ghub = Github(token)

# Whitelisted repos can be added or removed from this yaml file
# and will not be edited by this script

with open('whitelist.yaml') as wl:
    whitelist = yaml.safe_load(wl)

# Careful with this and ensure your token has minimum permissions required
# The only scope of the token needed is the "repo" setting
# Loops through all the Surfline repos to PUT /settings/branch_protection_rules
# Skips repos with no master branch


def change_protected_branch_settings():
    for repo in ghub.get_organization('Surfline').get_repos():
        if repo.name not in whitelist:
            try:
                branch = repo.get_branch('master')
                branch.edit_protection(
                    required_approving_review_count=1,
                    dismiss_stale_reviews=True,
                    enforce_admins=True,
                )
                print(
                    f"Edited the master branch protection "
                    f"rules for: {repo.name}"
                )
            except GithubException as err:
                print(f"Error: {err}")
                print(f"{repo.name} has no master branch")
        else:
            print(f"{repo.name} is whitelisted and unchanged.")


if __name__ == '__main__':
    change_protected_branch_settings()
