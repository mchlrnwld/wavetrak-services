# SQL Server Manager

When administering SQL Server, it's sometimes necessary to run SQL Server Management Studio. This directory includes a Vagrantfile to provision a Windows 7 VM. I've also included very brief instructions to get up and running with SQL Server Management Studio.

## Quick Start

### Install Vagrant

```bash
brew cask install vagrant
```

### Install Virtualbox

```bash
brew cask install virtualbox
```

Note: Because of macOS system security, you may need to install this manually.

### Provision a Windows 7 VM

```bash
vagrant up
```

### View status of VM

```bash
vagrant status
```

### Connect to VM using RDP

SCREENSHOT HERE

### Install SQL Server Management Studio

On the VM, download [SQL Server Management Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017) and install it.

### Connect to SQL Server

On the host (your laptop), connect to the AWS surfline-prod VPN.

On the VM, open SQL Server Management Studio, create a new server connection, and enter the SQL Server RDS hostname and your credentials.

### Suspend the VM

```bash
vagrant suspend
```
