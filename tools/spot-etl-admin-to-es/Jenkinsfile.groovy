node {
    checkout scm
    GString workingDirectory = "${pwd()}/tools/spot-etl-admin-to-es"
    String lambdaZipName = 'sl-spot-to-elasticsearch.zip'
    String lambdaName = "sl-spot-to-elasticsearch-lambda-${env.BUILD_ENVIRONMENT}"

    stage('Build') {
        sh "ls -lah"
        dir(workingDirectory) {
            sh "rm -rf ${workingDirectory}/ENV"
            sh "pip install virtualenv"
            sh "virtualenv ${workingDirectory}/ENV && " +
                    "source ${workingDirectory}/ENV/bin/activate && " +
                    "ENV/bin/pip install -r requirements.txt && " +
                    "make build"
            stash name: 'deployArtifacts'
        }
    }
    stage('Test') {
        unstash 'deployArtifacts'
        dir(workingDirectory) {
            sh(
                    script: "zip -T dist/${lambdaZipName}",
                    returnsStatus: true
            )
        }
    }
    stage('Deploy') {
        unstash 'deployArtifacts'
        dir(workingDirectory) {
            profileArgument = " --profile ${env.BUILD_ENVIRONMENT} "

            lambdaName = 'sl-spot-to-elasticsearch-sandbox'
            SPOT_API = 'http://spots-api.sandbox.surfline.com'
            ELASTIC_SEARCH_CONNECTION_STRING = 'https://search-sl-es-sbox-3i7itfnhgfzidlqxbx2ggpi3rm.us-west-1.es.amazonaws.com'
            if (env.BUILD_ENVIRONMENT == "staging") {
                ELASTIC_SEARCH_CONNECTION_STRING = 'https://search-sl-es-staging-pdnwispkwdzmj5fuvyp74zyjdy.us-west-1.es.amazonaws.com';
                SPOT_API = 'http://spots-api.staging.surfline.com';
                lambdaName = 'sl-spot-to-elasticsearch-staging';
            } else if (env.BUILD_ENVIRONMENT == "prod") {
                ELASTIC_SEARCH_CONNECTION_STRING = 'https://search-sl-es-prod-xsw3q37vso7bm7wjooudtsrw7y.us-west-1.es.amazonaws.com';
                SPOT_API = 'http://spots-api.surfline.com';
                lambdaName = 'sl-spot-to-elasticsearch-prod'
            }

            sh 'ls -lah'

            lambdaArgument = " --function-name ${lambdaName} "
            localZipArgument = " --zip-file fileb://dist/${lambdaZipName} "
            environmentVariables = " --environment Variables=\\{SPOT_API=${SPOT_API},ELASTIC_SEARCH_CONNECTION_STRING=${ELASTIC_SEARCH_CONNECTION_STRING},IS_ES_ON_AWS=true,ES_INDEX=spots,ES_TYPE=spot,LOG_LEVEL=DEBUG,ES_HTTP_AUTH=false\\} "
            updateFunctionConfigCommand = "aws ${profileArgument} lambda update-function-configuration ${lambdaArgument} ${environmentVariables}"
            updateFunctionCodeCommand = "aws ${profileArgument} lambda update-function-code ${lambdaArgument} ${localZipArgument}"

            sh updateFunctionConfigCommand
            sh updateFunctionCodeCommand
        }
    }
}