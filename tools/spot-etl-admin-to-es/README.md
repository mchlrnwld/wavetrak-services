# Spot ETL to Elasticsearch

Lambda function for updating Elasticsearch spots index with spot-api content. 

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Getting Started](#getting-started)
- [Local Development](#local-development)
    - [Build containers, start full WP/ES/Kibana dev stack](#build-containers-start-full-wpeskibana-dev-stack)
    - [Start/stop/restart full WP/ES/Kibana dev stack](#startstoprestart-full-wpeskibana-dev-stack)
    - [Run Python bash shell in the running dev stack](#run-python-bash-shell-in-the-running-dev-stack)
    - [Invoke Lambda function in running dev stack](#invoke-lambda-function-in-running-dev-stack)
    - [Invoke Lambda function against AWS](#invoke-lambda-function-against-aws)
- [Deployment](#deployment)
- [Monitoring](#monitoring)
  - [Configuration](#configuration)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Getting Started

TODO

## Local Development

The following convenience commands are available. View the contents of `Makefile`
to see how these commands translate to docker commands.

#### Build containers, start full WP/ES/Kibana dev stack
```bash
make dev-full
```

#### Start/stop/restart full WP/ES/Kibana dev stack
```bash
make dev-full-start

make dev-full-stop

make dev-full-restart
```

#### Run Python bash shell in the running dev stack
```bash
make pyshell
```

#### Invoke Lambda function in running dev stack
```bash
make pyshell

pip install -r requirements.txt

cp .env.sample .env

make invoke
```

#### Invoke Lambda function against AWS
```bash
make pyshell-standalone

pip install -r requirements.txt

cp .env.sample .env

vim .env     # uncomment remote section, comment local section

export AWS_ACCESS_KEY_ID={your key}
export AWS_DEFAULT_REGION={your region}
export AWS_SECRET_ACCESS_KEY={your secret key}

make invoke
```

## Deployment

This Lambda function (sl-spot-to-elasticsearch-{env}) is deployed using a [Jenkins job](https://surfline-jenkins-master-prod.surflineadmin.com/job/admin/job/deploy-spot-to-elasticsearch-function/).

## Monitoring

This Lambda has a [New Relic alert policy](https://one.newrelic.com/launcher/nrai.launcher?platform%5BaccountId%5D=356245&pane=eyJuZXJkbGV0SWQiOiJhbGVydGluZy11aS1jbGFzc2ljLnBvbGljaWVzIiwibmF2IjoiUG9saWNpZXMiLCJzZWxlY3RlZEZpZWxkIjoidGhyZXNob2xkcyIsInBvbGljeUlkIjoiMjQ4MzMyIiwiY29uZGl0aW9uSWQiOiIxNjU3ODQ3MiJ9&sidebars%5B0%5D=eyJuZXJkbGV0SWQiOiJucmFpLm5hdmlnYXRpb24tYmFyIiwibmF2IjoiUG9saWNpZXMifQ) that monitors the invocation error count.

### Configuration

The alert should be configured at a threshold that indicates a diminished functionality state. Developers should proactively update this threshold as error patterns change over the life of the Lambda function.

Because of reporting delays from AWS to New Relic, the `aggregation window` value should be set to 20 minutes.
