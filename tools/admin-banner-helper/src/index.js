import React from 'react';
import ReactDOM from 'react-dom';
import { Notification, NotificationBar } from '@surfline/quiver-react';
import './styles';

const renderNotification = (warningType) => {
  let notification = 'We are unable to renew your membership due to a payment problem. ';
    const linkText = 'Please update your payment details.';
    if (warningType === 'invalidzip') {
      notification = 'Make sure you keep Premium Subscription. ';
    }
  return(
    <div>
      <Notification closable type="warning-notification" level="error">
        <span>{notification}</span>
        <a href="/account/subscription#payment-details">{linkText}</a>
      </Notification>
    </div>
    )
}
document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('.paymentwarning-banner')
    .forEach(domContainer => {
      const warningType = domContainer.dataset.warningtype;
        ReactDOM.render(
          warningType ? <NotificationBar largeNotification={renderNotification(warningType)} /> : null,
          domContainer
        );
    });
});

