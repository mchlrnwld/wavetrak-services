# Banner Helper

This is a light weight React helper that would load quiver `NotificationBar` bar component.

## Developing
Uses node:v10

```
npm start
```
This commands starts the webpack dev server in port 8080.

## Build
```
npm run build
```
This command bundles the app into a minified js file - `dist/banner_helper.js`.
