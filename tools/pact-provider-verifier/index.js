const Verifier = require('pact').Verifier;

const {
  PROVIDER,
  PACT_URLS,
  PROVIDER_BASE_URL,
  PROVIDER_STATES_SETUP,
  PACT_BROKER_URL,
} = process.env;

const verify = () => {
  const config = {
    provider: PROVIDER,
    pactUrls: PACT_URLS ? PACT_URLS.split(',') : [],
    providerBaseUrl: PROVIDER_BASE_URL || 'http://application',
    providerStatesSetupUrl: PROVIDER_STATES_SETUP || 'http://pact-provider-states/setup',
    pactBrokerUrl: PACT_BROKER_URL || 'https://pact-broker-staging.staging.surflineadmin.com/',
    publishVerificationResults: true,
    providerVersion: 'latest'
  };

  console.log('Running pact verifier with the following config')
  console.log('-------------------------');
  console.log(JSON.stringify(config, null, 2));

  return Verifier.verifyProvider(config);
};

verify()
  .then(result => {
    console.log(result);
    process.exit(0);
  })
  .catch(err => {
    console.log(err.stack);
    process.exit(1);
  });
