# Pact Provider Verifier

This project builds a pact-provider-verifier as a Docker container. To publish the docker container run

```
make publish
```

## Running the Verifier

The verifier is meant to be run in coordination with service integration and contract tests. For example, from `services/reports-api`

```
docker-compose \
    -f docker-compose.test.yml \
    -f ../../tools/pact-provider-verifier/docker-compose.yml \
    up \
    --build \
    --force-recreate \
    --no-color \
    --exit-code-from pact-broker-verifier
```

See `docker-compose.yml` for more details.

### Configuration

The verififier can be configured via environment variables.

* `PROVIDER` - the name of the provider to verify
* `PACT_URLS` - a comma seperated list of pact urls to verify the provider against
