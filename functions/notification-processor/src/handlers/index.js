import newrelic from 'newrelic'; // eslint-disable-line import/no-extraneous-dependencies
import * as analytics from '../common/analytics';
import { publishMessage, getS3Object, deleteS3Object } from '../common/aws';
import { createNotificationPayload } from '../utils/createNotificationMessage';
import conditionEnumToText from '../utils/spotConditionToText';

const SESSION = 'SESSION';
const SPOT = 'SPOT';

/**
 * Conditionally outputs errors to our server logs
 * Omitting `EndpointDisabled` from the logs to prevent alerting when this is expected
 * https://docs.aws.amazon.com/sns/latest/api/API_Publish.html#API_Publish_Errors
 * @param {Error} error Error object to be logged
 */
const logError = (error) => {
  console.log(error);

  // prevent alerting around users with disabled notifications or devices
  const errorsCodesToSuppress = ['EndpointDisabled'];
  if (newrelic && errorsCodesToSuppress.includes(error?.code)) {
    newrelic.recordCustomEvent(error.code, { error });
  } else if (newrelic) {
    newrelic.noticeError(error);
  }
};

/**
 * Lambda handler for processing a session notification.
 * @param {Object} batch
 * @param {Object} batch.outlet
 * @param {string} batch.outlet.endpointArn
 * @param {string} batch.outlet.channel
 * @param {Object} batch.session
 * @param {number} batch.session.waveCount
 * @param {string} batch.session.sessionId
 * @param {Object} batch.session.spot
 * @param {String} batch.session.spot.name
 */
export const sessionNotificationHandler = async (batch) => {
  const {
    outlets,
    session: { user, sessionId, waveCount, spot },
  } = batch;

  const message = `Your session at ${spot.name} with ${waveCount} waves is ready!`;

  await Promise.all(
    outlets.map(async (outlet) => {
      const { channel, appName, endpointArn } = outlet;
      console.log(`Processing sessionID ${sessionId} notification for ${user}: ${message}`);
      const payload = createNotificationPayload(
        message,
        sessionId,
        channel,
        appName,
        null,
        `sessions/detail?id=${sessionId}`,
        SESSION,
      );
      let notificationSent = true;

      try {
        await publishMessage(endpointArn, payload, sessionId);
      } catch (error) {
        notificationSent = false;
        logError(error);
      }

      if (notificationSent) {
        analytics.track({
          userId: user,
          event: 'Push Notification Sent to Gateway',
          notificationType: appName,
          notificationPayload: message,
        });
      }
    }),
  );

  return { success: true };
};

/**
 * Lambda handler for processing a spot notification.
 * @param {Object} batch
 * @param {Object} batch.outlet
 * @param {string} batch.outlet.endpointArn
 * @param {string} batch.outlet.channel
 * @param {Object} batch.spot
 * @param {number} batch.spot.spotId
 * @param {string} batch.spot.spotName
 * @param {number} batch.spot.waveHeightMin
 * @param {number} batch.spot.waveHeightMax
 * @param {string} batch.spot.condition
 */
export const spotNotificationHandler = async (batch) => {
  const { outlets, spot } = batch;

  const { spotId, spotName, waveHeightMax, waveHeightMin, condition } = spot;

  const message = `${spotName} is ${waveHeightMin}-${waveHeightMax} and ${conditionEnumToText(
    condition,
  )}`;

  await Promise.all(
    outlets.map(async (outlet) => {
      const { channel, appName, user, endpointArn } = outlet;
      console.log(`Processing spot notification for ${user}: ${message}`);
      const ttl = 43200; // 12 hours in seconds
      const payload = createNotificationPayload(
        message,
        spotId,
        channel,
        appName,
        ttl,
        `spot?id=${spotId}&name=${spotName.replace(/\s+/g, '-')}&segment=report`,
        SPOT,
      );
      let notificationSent = true;

      try {
        await publishMessage(endpointArn, payload, spotId, ttl);
      } catch (error) {
        notificationSent = false;
        logError(error);
      }

      if (notificationSent) {
        analytics.track({
          userId: user,
          event: 'Push Notification Sent to Gateway',
          notificationType: appName,
          notificationPayload: payload,
        });
      }
    }),
  );

  return { success: true };
};

/**
 * Lambda handler for processing an s3 object creation.
 * @param {Object} event
 */
export const s3ObjectCreationHandler = async (event) => {
  const params = {
    Bucket: event.Records[0].s3.bucket.name,
    Key: event.Records[0].s3.object.key,
  };

  let s3Object;
  try {
    // There seem to be instances with race conditions, where a batch file
    // may trigger the lambda multiple times and then the lambda can't find
    // the deleted file
    s3Object = await getS3Object(params);
  } catch (error) {
    console.log('S3 Object Not Found');
  }
  if (s3Object) {
    const batch = JSON.parse(s3Object.Body);
    let success = false;

    try {
      if (batch.type === SESSION) {
        await sessionNotificationHandler(batch);
      }

      if (batch.type === SPOT) {
        await spotNotificationHandler(batch);
      }

      success = true;
    } catch (error) {
      console.log('-- Error Processing Notification --');
      console.log(error);
      newrelic.noticeError(error);
      throw error;
    }

    try {
      // We only want to delete the object if the notification was sent
      if (success) {
        await deleteS3Object(params);
      }
    } catch (error) {
      // We don't want the lambda to fail, because then it will re-process the notification
      // Instead just notice error and succeed. Deletions can be done manually if needed
      newrelic.noticeError(error);
    }

    return { success: true };
  }
  return { success: false };
};
