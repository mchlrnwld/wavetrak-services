import * as handlers from '.';

/**
 * Responsible for parsing the event object passed into the lambda function, and
 * returning the appropriate handler for the given parameters.
 *
 * @param {Object} event event object passed into the lambda handler
 */
const matchHandler = (event) => {
  if (event?.Records?.[0]?.eventName === 'ObjectCreated:Put') {
    return handlers.s3ObjectCreationHandler(event);
  }

  throw new Error('No matching handler to run for functions inputs');
};

export default matchHandler;
