import chai, { expect } from 'chai';
import sinon from 'sinon';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import * as aws from '../common/aws';
import * as analytics from '../common/analytics';
import matchHandler from './matchHandler';
import sessionBatchFixture from '../fixtures/sessionBatchFixture.json';

chai.should();
chai.use(sinonChai);
chai.use(dirtyChai);

describe('handler', () => {
  beforeEach(() => {
    sinon.stub(aws, 'getS3Object');
    sinon.stub(aws, 'deleteS3Object');
    sinon.stub(aws, 'publishMessage');
    sinon.stub(analytics, 'track');
  });

  it('should trigger the s3ObjectCreationHandler', async () => {
    const event = {
      Records: [
        {
          eventName: 'ObjectCreated:Put',
          s3: { bucket: { name: 'test' }, object: { key: 'test' } },
        },
      ],
    };

    aws.getS3Object.resolves({
      Body: JSON.stringify(sessionBatchFixture),
    });
    aws.deleteS3Object.resolves();
    aws.publishMessage.resolves();

    const expectedMessage = 'Your session at HB Pier Southside with 3 waves is ready!';

    const response = await matchHandler(event);
    expect(response.success).to.be.true();
    expect(aws.getS3Object).to.have.been.calledOnce();
    expect(aws.publishMessage).to.have.been.calledOnceWithExactly(
      'test-arn',
      {
        default: expectedMessage,
        APNS: JSON.stringify({
          aps: {
            alert: expectedMessage,
          },
          type: 'SESSION',
          url: `surfline://sessions/detail?id=${sessionBatchFixture.session.sessionId}`,
        }),
      },
      sessionBatchFixture.session.sessionId,
    );
  });
});
