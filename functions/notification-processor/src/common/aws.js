/* eslint-disable import/prefer-default-export */

import AWS from 'aws-sdk';
import config from '../config';

const { SNS_API_VERSION } = config;

/**
 * @description This function is used to publish a message to
 * an SNS topic.
 * Before being sent, the `message` is passed into `JSON.stringify()`.
 * @param {string} topicArn
 * @param {Object} message
 */
export const publishMessage = (targetARN, message, collapseKey, ttl) => {
  const SNS = new AWS.SNS({
    apiVersion: SNS_API_VERSION,
    region: 'us-west-1',
  });

  console.log('-- PUBLISHING MESSAGE TO SNS --');
  console.log('Message: ', JSON.stringify(message));
  console.log('TargetARN: ', targetARN);
  return SNS.publish({
    TargetArn: targetARN,
    Message: JSON.stringify(message),
    MessageStructure: 'json',
    MessageAttributes: {
      ...(collapseKey && {
        'AWS.SNS.MOBILE.APNS.COLLAPSE_ID': {
          DataType: 'String',
          StringValue: collapseKey,
        },
      }),
      ...(ttl && {
        'AWS.SNS.MOBILE.APNS.TTL': {
          DataType: 'Number',
          StringValue: ttl.toString(),
        },
        'WS.SNS.MOBILE.FCM.TTL': {
          DataType: 'Number',
          StringValue: ttl.toString(),
        },
      }),
    },
  }).promise();
};

export const getS3Object = (params) => {
  const s3 = new AWS.S3({});

  return s3.getObject(params).promise();
};

export const deleteS3Object = (params) => {
  const s3 = new AWS.S3({});

  return s3.deleteObject(params).promise();
};
