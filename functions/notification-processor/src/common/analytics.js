import Analytics from 'analytics-node';

const slIOSAnalytics = new Analytics(process.env.IOS_SEGMENT_WRITE_KEY || 'NoWrite', {
  flushAt: 1,
});

export const track = (params, callback = null) => {
  slIOSAnalytics.track(params, callback);
};

export default track;
