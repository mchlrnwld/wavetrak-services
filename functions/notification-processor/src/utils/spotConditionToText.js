const conditionEnumToText = (condition, newSubtr) =>
  condition &&
  condition.charAt(0).toUpperCase() +
    condition
      .replace(/_/g, newSubtr || ' ')
      .slice(1)
      .toLowerCase();

export default conditionEnumToText;
