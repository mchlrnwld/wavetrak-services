const appUrls = {
  SURFLINE: 'surfline',
  BUOYWEATHER: 'buoyweather',
  FISHTRACK: 'fishtrack',
  MAGICSEAWEED: 'magicseaweed',
};

/**
 * @description Create's the GCM notification payload
 *
 * @param {String} message
 * @param {String} url
 * @param {string} collapseKey
 * @param {number} ttl
 * @param {string} [channel='GCM']
 */
export const createGCMNotification = (message, url, collapseKey, ttl, type, channel = 'GCM') => {
  const payload = {
    default: message,
    [channel]: JSON.stringify({
      data: {
        message,
      },
      type,
      url,
    }),
  };

  if (collapseKey) {
    payload.collapse_key = collapseKey;
  }
  if (ttl) {
    payload.time_to_live = ttl;
  }

  return payload;
};
/**
 * @description Create's the APNS notification payload
 *
 * @param {string} message
 * @param {string} url
 * @param {string} collapseKey
 * @param {string} [channel='APNS']
 * @returns
 */
export const createAPNSNotification = (message, url, type, channel = 'APNS') => {
  const payload = {
    default: message,
    [channel]: JSON.stringify({
      aps: {
        alert: message,
      },
      type,
      url,
    }),
  };

  return payload;
};

/**
 * @description Create's the Default notification payload
 *
 * @param {string} message
 */
export const createDefaultNotification = (message) => {
  const payload = {
    default: message,
  };

  return payload;
};

/**
 * @description Create the notification payload
 *
 * @param {string} message
 * @param {string} collapseKey
 * @param {string} channel
 * @returns {Object}
 */
export const createNotificationPayload = (
  message,
  collapseKey,
  channel,
  appName = appUrls.SURFLINE,
  ttl,
  urlPath,
  type,
) => {
  let payload;
  switch (channel) {
    case 'APNS_SANDBOX':
    case 'APNS':
      payload = createAPNSNotification(
        message,
        `${appUrls[appName] || 'surfline'}://${urlPath || ''}`,
        type,
        channel,
      );
      break;
    case 'GCM':
      payload = createGCMNotification(
        message,
        `surfline://${urlPath || ''}`,
        collapseKey,
        ttl,
        type,
        channel,
      );
      break;
    default:
      payload = createDefaultNotification(message);
      break;
  }

  return payload;
};
