import { expect } from 'chai';
import spotConditionToText from './spotConditionToText';

describe('utils / spotConditionToText', () => {
  it('defaults to space if not passed a new substring', () => {
    expect(spotConditionToText('POOR_TO_FAIR')).to.equal('Poor to fair');
  });

  it('returns the correct string when given a new substring', () => {
    expect(spotConditionToText('POOR_TO_FAIR', '-')).to.equal('Poor-to-fair');
  });
});
