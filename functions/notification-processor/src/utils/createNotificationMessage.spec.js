import { expect } from 'chai';
import { createNotificationPayload } from './createNotificationMessage';
import sessionBatchFixture from '../fixtures/sessionBatchFixture.json';

describe('utils / createNotificationMessage', () => {
  it('correctly generates a notification payload for a given session', () => {
    const desiredSessionNotification = {
      default: 'Your session at HB Pier Southside with 3 waves is ready!',
      APNS: '{"aps":{"alert":"Your session at HB Pier Southside with 3 waves is ready!"},"type":"SESSION","url":"surfline://sessions/detail?id=5cccc31c1bd8ef2c478bc55e"}',
    };
    const batch = sessionBatchFixture;
    const {
      outlets,
      session: { sessionId, waveCount, spot },
    } = batch;
    const message = `Your session at ${spot.name} with ${waveCount} waves is ready!`;
    const outlet = outlets[0];
    const { channel, appName } = outlet;
    expect(
      createNotificationPayload(
        message,
        sessionId,
        channel,
        appName,
        null,
        `sessions/detail?id=${sessionId}`,
        'SESSION',
      ),
    ).to.deep.equal(desiredSessionNotification);
  });

  it('correctly generates a notification for a spot', () => {
    const desiredSpotNotification = {
      default: 'HB Pier, Northside is 5-6 and poor to fair',
      APNS: '{"aps":{"alert":"HB Pier, Northside is 5-6 and poor to fair"},"type":"SPOT","url":"surfline://spot?id=5842041f4e65fad6a7708827&name=HB-Pier,-Northside&segment=report"}',
    };
    const spotName = 'HB Pier, Northside';
    const spotId = '5842041f4e65fad6a7708827';
    const message = `${spotName} is 5-6 and poor to fair`;
    expect(
      createNotificationPayload(
        message,
        spotId,
        'APNS',
        'SURFLINE',
        43200,
        `spot?id=${spotId}&name=${spotName.replace(/\s+/g, '-')}&segment=report`,
        'SPOT',
      ),
    ).to.deep.equal(desiredSpotNotification);
  });
});
