export default {
  notificationsService: process.env.NOTIFICATIONS_SERVICE,
  SNS_API_VERSION: '2010-03-31',
};
