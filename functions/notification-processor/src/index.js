import matchHandler from './handlers/matchHandler';

export default async (event) => {
  try {
    const result = await matchHandler(event);
    return result;
  } catch (err) {
    console.error(err);
    throw err;
  }
};
