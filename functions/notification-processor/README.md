# Notification Processor Lambda Function

## Table of contents <!-- omit in toc -->

- [Summary](#summary)
- [Development](#development)
  - [Installation](#installation)
  - [Linting](#linting)
  - [Integration test](#integration-test)
    - [Request](#request)
    - [Response](#response)
- [Monitoring](#monitoring)
  - [Configuration](#configuration)
  - [Triggering the alert](#triggering-the-alert)
- [Jenkins deployment](#jenkins-deployment)

## Summary

A function to process the batch notification files, triggered when they're are saved to S3.

See [Confluence documentation](https://wavetrak.atlassian.net/wiki/spaces/MS/pages/499712417/Session+KBYG+Push+Notification+Architecture) for more information.

## Development

### Installation

```bash
cp .env.sample .env
npm install
```

### Linting

```bash
npm run lint
```

### Integration test

The next call to the lambda function should be enough to test if we receive a successful response.

#### Request

```bash
export AWS_PROFILE=surfline-dev
aws lambda invoke \
    --function-name sl-notifications-service-notification-processor-sandbox \
    --payload "{}" \
    ../output.json
```

#### Response

```json
{
  "StatusCode": 200,
  "ExecutedVersion": "$LATEST"
}
```

The output file generated should contain a `null` string, even if the execution is successful or not. So a good approach would be to browse the [CloudWatch logs for the lambda function](https://us-west-1.console.aws.amazon.com/cloudwatch/home?region=us-west-1#logStream:group=%252Faws%252Flambda%252Fsl-notifications-service-notification-processor-sandbox) (sandbox) and read the log responses.

## Monitoring

The notification processor lambda has a [New Relic alert policy](https://one.newrelic.com/launcher/nrai.launcher?platform[accountId]=356245&pane=eyJuZXJkbGV0SWQiOiJhbGVydGluZy11aS1jbGFzc2ljLnBvbGljaWVzIiwibmF2IjoiUG9saWNpZXMiLCJzZWxlY3RlZEZpZWxkIjoidGhyZXNob2xkcyIsInBvbGljeUlkIjoiMjQ4MzMyIiwiY29uZGl0aW9uSWQiOiIxMDQzMzA5NSJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucmFpLm5hdmlnYXRpb24tYmFyIiwibmF2IjoiUG9saWNpZXMifQ) that
monitors the Lambda invocation error count.

### Configuration

Use your judgment when adjusting the thresholds.

**Note:** Because of reporting delays from AWS to New Relic, the `aggregation window` value should be set to 20 minutes.

### Triggering the alert

If you need to trigger the alert for testing purposes, you can use the AWS console to configure a Lambda test event that contains a null event object (`{}`). The Lambda will throw the following error in response:

```json
{
  "errorType": "TypeError",
  "errorMessage": "Cannot read property '0' of undefined",
  "trace": [
    "TypeError: Cannot read property '0' of undefined",
    "    at matchHandler (/var/task/dist/handlers/matchHandler.js:10:67)",
    "    at Runtime.lambdaHandler [as handler] (/var/task/dist/index.js:26:52)",
    "    at Runtime.handleOnce (/var/runtime/Runtime.js:66:25)"
  ]
}
```

Invoke that event repeatedly using the "Test" button until you reach the error threshold configured in the policy. After the offset window has elapsed, New Relic will trigger an alert.

## Jenkins deployment

To deploy this application as a lambda function, launch a [Jenkins job](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-notifications-service/), in the desired environment.

Once the job is completed, you can check the `lambda-functions/notifications-service-notification-processor/sl-notifications-service-notification-processor.zip` file under the following S3 buckets:

| staging              | sandbox          | prod              |
| -------------------- | ---------------- | ----------------- |
| sl-artifacts-staging | sl-artifacts-dev | sl-artifacts-prod |

## Environment Variables

Environment variables are configured both in the `ci.json` and in the main terraform configuration `infra/main.tf`.

Variables declared in `ci.json` are looked up in the AWS Parameter Store and injected to the Lambda by prepending them to the `index.js`. These are not available to the New Relic Lambda wrapper.

To expose environment variables to the [New Relic Lambda wrapper](https://docs.newrelic.com/docs/serverless-function-monitoring/aws-lambda-monitoring/enable-lambda-monitoring/instrument-your-own/), we configure the `infra/main.tf`. The `aws_ssm_parameter` is used to pull environment variable values from the AWS Parameter Store. Variables exposed by the terraform config are available at runtime before the `index.js` is initialized allowing the New Relic Lambda wrapper to read these values.
