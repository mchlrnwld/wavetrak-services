service: notification-processor

frameworkVersion: "2"
variablesResolutionMode: 20210326
useDotenv: true

plugins:
  - serverless-plugin-typescript
  - serverless-newrelic-lambda-layers
  - serverless-plugin-resource-tagging

package:
  patterns:
    - "!infra/**"

custom:
  application: ${self:service}-${self:custom.environment}
  company: sl
  environment: ${opt:stage, "dev"} # Passed in via the -s flag (ex. -s prod).
  newRelic:
    accountId: ${ssm:/${self:custom.environment}/common/NEW_RELIC_ACCOUNT_ID_FOR_SERVERLESS}
    apiKey: ${ssm:/${self:custom.environment}/common/NEW_RELIC_API_KEY_FOR_SERVERLESS}
    enableExtension: true
    enableIntegration: true
    logEnabled: true
  vpcSettings:
    securityGroupIds:
      sandbox:
        - sg-90aeaef5
        - sg-91aeaef4
      staging:
        - sg-90aeaef5
        - sg-91aeaef4
      prod:
        - sg-a5a0e5c0
        - sg-a4a0e5c1
    subnetIds:
      sandbox:
        - subnet-0909466c
        - subnet-f2d458ab
      staging:
        - subnet-0909466c
        - subnet-f2d458ab
      prod:
        - subnet-d1bb6988
        - subnet-85ab36e0
  envVars:
    sandbox:
      NOTIFICATIONS_SERVICE: http://notifications-service.sandbox.surfline.com
      SNS_ENDPOINT_ARN: arn:aws:sns:us-west-1:665294954271:app
    staging:
      NOTIFICATIONS_SERVICE: http://notifications-service.staging.surfline.com
      SNS_ENDPOINT_ARN: arn:aws:sns:us-west-1:665294954271:app
    prod:
      NOTIFICATIONS_SERVICE: http://notifications-service.surfline.com
      SNS_ENDPOINT_ARN: arn:aws:sns:us-west-1:833713747344:app
  webpack:
    includeModules: true

provider:
  name: aws
  runtime: nodejs14.x
  memorySize: 128
  lambdaHashingVersion: 20201221
  stackTags:
    Company: ${self:custom.company}
    Service: lambda
    Application: notification-processor
    Environment: ${self:custom.environment}
    Provisioned-By: Serverless
    Source: Surfline/wavetrak-services/functions/notification-processor
  region: us-west-1
  iam:
    role:
      statements:
        - Resource: ${self:custom.envVars.${self:custom.environment}.SNS_ENDPOINT_ARN}/*
          Effect: Allow
          Action: sns:Publish
        - Resource:
            - arn:aws:s3:::${self:custom.company}-notification-batches-${self:custom.environment}
            - arn:aws:s3:::${self:custom.company}-notification-batches-${self:custom.environment}/*
          Effect: Allow
          Action:
            - s3:GetObject
            - s3:GetObjectAcl
            - s3:HeadObject
            - s3:ListBucket
            - s3:ListMultipartUploadParts
            - s3:PutObject
            - s3:PutObjectAcl
            - s3:DeleteObject
  s3:
    notificationBatches:
      name: ${self:custom.company}-notification-batches-${self:custom.environment}
      accessControl: Private
      lifecycleConfiguration:
        Rules:
          - Status: Enabled
            ExpirationInDays: 90
            Transitions:
              - StorageClass: STANDARD_IA
                TransitionInDays: 30
              - StorageClass: GLACIER
                TransitionInDays: 60

functions:
  default:
    handler: src/index.default
    name: ${self:custom.company}-${self:custom.application}
    timeout: 30
    events:
      - s3:
          bucket: notificationBatches
          event: s3:ObjectCreated:*
          rules:
            - suffix: .json
    vpc:
      securityGroupIds: ${self:custom.vpcSettings.securityGroupIds.${self:custom.environment}}
      subnetIds: ${self:custom.vpcSettings.subnetIds.${self:custom.environment}}
    environment:
      account_id: ${aws:accountId}
      region: ${aws:region}
      NOTIFICATIONS_SERVICE: ${self:custom.envVars.${self:custom.environment}.NOTIFICATIONS_SERVICE}
