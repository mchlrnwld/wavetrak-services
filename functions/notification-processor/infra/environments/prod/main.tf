provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "notification-processor/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "notification-processor" {
  source = "../../"

  environment = "prod"
}
