package main

import (
	"errors"
	"fmt"
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/config"
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/generators"
	"github.com/newrelic/go-agent/v3/integrations/nrlambda"
	"github.com/newrelic/go-agent/v3/newrelic"
	"log"
	"os"
	"time"
)

// SitemapGenerator ...
type SitemapGenerator struct {
	Name      string
	Generator (func(string, string) error)
}

var sitemapGenerators = [7]SitemapGenerator{
	SitemapGenerator{"Index", generators.Index},
	SitemapGenerator{"Beaches", generators.Beaches},
	SitemapGenerator{"Charts", generators.Charts},
	SitemapGenerator{"Home", generators.Home},
	SitemapGenerator{"Locations", generators.Locations},
	SitemapGenerator{"Spots", generators.Spots},
	SitemapGenerator{"Subregions", generators.Subregions},
}

// Event ...
type Event struct {
	QueryStringParameters map[string]string
}

// Handle ...
func Handle(evt Event) (string, error) {
	start := time.Now()
	lastMod := start.Format("2006-01-02")

	ch := make(chan error)
	for _, sitemapGenerator := range sitemapGenerators {
		go func(sitemapGenerator SitemapGenerator) {
			err := sitemapGenerator.Generator(sitemapGenerator.Name, lastMod)
			ch <- err
		}(sitemapGenerator)
	}
	errorOccurred := false
	for _, siteMapGenerator := range sitemapGenerators {
		err := <-ch
		if err != nil {
			errorOccurred = true
			log.Printf("%s: Error: %s", siteMapGenerator.Name, err)
		}
	}
	if errorOccurred {
		return "", errors.New("failed to generate sitemaps")
	}

	log.Printf("Successfully generating sitemaps in %v!", time.Now().Sub(start))
	return "", nil
}

func main() {
	app, err := newrelic.NewApplication(
		nrlambda.ConfigOption(),
		newrelic.ConfigAppName(config.NewRelicAppName),
		newrelic.ConfigDebugLogger(os.Stdout),
	)

	if nil != err {
		fmt.Println("error creating app (invalid config):", err)
	}

	nrlambda.Start(Handle, app)
}
