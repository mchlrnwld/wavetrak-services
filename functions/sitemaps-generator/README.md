# Sitemaps Generator

This Lambda function (sl-sitemaps-generator-{env}) tool creates the sitemaps for Surfline.com.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents <!-- omit in toc -->

- [Components overview](#components-overview)
- [Serverless Framework](#serverless-framework)
  - [Installing](#installing)
  - [`serverless.yml`](#serverlessyml)
- [Deploying to AWS](#deploying-to-aws)
- [`Makefile`](#makefile)
- [Go Requirements](#go-requirements)
- [`infra` Directory](#infra-directory)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Components overview

- Most build/dev tools are wrangled by `make`: see [`Makefile`](#makefile)
- We are using [go modules](https://go.dev/blog/using-go-modules) to manage packages.
- Lambda function: configured in [`serverless.yml`](#serverlessyml).
- [New Relic] alerts: configured in [`infra`](#infra-directory) directory.

## Serverless Framework

This Lambda function is managed and provisioned by the Serverless Framework.

### Installing

To install the Serverless Framework, you must have Node version >=10 and npm installed. Once both are installed, simply run the following command:

```sh
npm install
```

This will install Serverless and all required plugins.

### `serverless.yml`

Serverless Framework uses this file to create and deploy a lambda functions to any given AWS account. The top-level sections of the serverless.yml are:

- `service`

  This describes the service associated with the Lambda function, for example: tag-ec2-instances. The service name will be used to create the Cloudformation template that then creates the Lambda function.

- `frameworkVersion`

  Version of Serverless Framework being used.

- `provider`

  This section is where all AWS-related information is declared. For example: the AWS region, the AWS account associated with the Lambda, and any IAM role permissions. It also contains information about the Python version.

- `plugins`

  This section is where all 3rd-party Serverless plugins are declared. For example, the serverless-python-requirements allows you to easily package and deploy a Python Lambda with 3rd-party dependencies.

- `package`

  What patterns to include and what not to include in the runtime package that gets uploaded to AWS.

- `functions`

  This section is where all Lambda functions are declared. Do note that many Lambda functions can be declared here, not just one. You can specify the name of the function, the handler to use, any associated infrastructure (Cloudwatch, SNS, etc) and more.

## Invoke Function Locally

We use `serverless` along with the `Makefile` to invoke the lambda from our local machine.

```
$ ENV=sandbox make invoke-local
```

## Deploying to AWS

Included in the `Makefile` is `ENV=<env-name> make deploy`, which allows you to easily deploy the lambda to the AWS account and environment of choice. The desired environment must be passed in. For example:
```sh
AWS_PROFILE=surfline-dev ENV=sandbox make deploy
```

NOTE: There are 3 relevant environments for this lambda function: `sandbox`, `staging` and `prod`. Additionally, `dev` is used as a placeholder for an environment when using `serverless` to run code locally.

## `Makefile`

The `Makefile` supports these targets:

- `make format` to format the source files in the project.
- `make lint` to run the go linter
- `make test-unit` to run `go test`.
- `ENV=<env-name> make invoke-local` to invoke the go function on your local machine with a sample event (`test.json`).
- `ENV=<env-name> make deploy` to deploy the code from your machine to AWS. Also see [Deploying to AWS](#deploying-to-aws).
- `ENV=<env-name> make remove` to uninstall the function from AWS.

## `infra` Directory

This directory contains the terraform module to deploy additonal infrastructure: in this case [New Relic] alert definitions.

### Testing Deployment

The event can be triggered via the aws cli.

```sh
aws lambda invoke \
  --function-name sl-sitemaps-generator-sandbox \
    --payload '{}' output.json
```

The logs from the lambda invocation can be found in CloudWatch.

## Monitoring

This Lambda has a [New Relic alert policy](https://one.newrelic.com/launcher/nrai.launcher?platform[accountId]=356245&pane=eyJuZXJkbGV0SWQiOiJhbGVydGluZy11aS1jbGFzc2ljLnBvbGljaWVzIiwibmF2IjoiUG9saWNpZXMiLCJwb2xpY3lJZCI6IjEyNTQ5NTAifQ&sidebars[0]=eyJuZXJkbGV0SWQiOiJucmFpLm5hdmlnYXRpb24tYmFyIiwibmF2IjoiUG9saWNpZXMifQ) that monitors the invocation error count.

### Configuration

The alert should be configured at a threshold that indicates a diminished functionality state. Developers should proactively update this threshold as error patterns change over the life of the Lambda function.

Because of reporting delays from AWS to New Relic, the `aggregation window` value should be set to 20 minutes.

