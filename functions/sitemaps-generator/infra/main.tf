locals {
  application_title = "Sitemaps Generator"
  function_name     = "sl-sitemaps-generator-${var.environment}"
}

resource "newrelic_alert_policy" "function_alert_policy" {
  name                = local.application_title
  incident_preference = "PER_CONDITION_AND_TARGET"
}

resource "newrelic_nrql_alert_condition" "function_alert_condition" {
  name                         = "Lambda Function Error: ${local.function_name}"
  policy_id                    = newrelic_alert_policy.function_alert_policy.id
  type                         = "static"
  enabled                      = "true"
  value_function               = "single_value"
  violation_time_limit_seconds = "86400"

  nrql {
    query             = <<EOT
      SELECT sum(`provider.errors.Sum`)
      FROM   ServerlessSample
      WHERE  provider LIKE 'LambdaFunction%'
      AND    providerAccountId = '1500'
      FACET  `provider.functionName`
      WHERE  `provider.functionName` = '${local.function_name}'
    EOT
    evaluation_offset = "20"
  }

  # alert on any error (above 0) in the past 5 minutes
  critical {
    operator              = "above"
    threshold             = 0
    threshold_duration    = 5 * 60
    threshold_occurrences = "all"
  }
}
