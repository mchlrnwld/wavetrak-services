provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "sitemaps-generator/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "sitemaps" {
  source = "../../"

  environment = "prod"
}
