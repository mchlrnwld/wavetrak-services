package config

import "os"

// SurflineHost ...
var SurflineHost = os.Getenv("SURFLINE_HOST")

// SpotsAPI ...
var SpotsAPI = os.Getenv("SPOTS_API")

// AWSS3Bucket ...
var AWSS3Bucket = os.Getenv("AWS_S3_BUCKET")

// NewRelicAccountID used when configuring New Relic
var NewRelicAccountID = os.Getenv("NEW_RELIC_ACCOUNT_ID")

// NewRelicAPIKey used when configuring New Relic
var NewRelicAPIKey = os.Getenv("NEW_RELIC_API_KEY")

// NewRelicAppName used when configuring New Relic
var NewRelicAppName = os.Getenv("NEW_RELIC_APP_NAME")
