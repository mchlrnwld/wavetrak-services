package generators

import (
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/common"
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/config"
	"log"
	"net/http"
	"time"
)

// Beaches ...
func Beaches(logPrefix string, lastMod string) error {
	start := time.Now()

	log.Printf("%s: Loading beaches...", logPrefix)
	httpClient := &http.Client{}
	beaches, err := common.GetBeaches(httpClient, "geoname", "6295630")
	if err != nil {
		return err
	}

	log.Printf("%s: Creating beaches url set...", logPrefix)
	urlSet, err := common.CreateBeachesURLSet(beaches, lastMod)
	if err != nil {
		return err
	}

	log.Printf("%s: Saving beaches url set...", logPrefix)
	saveOptions := common.SaveOptions{Bucket: config.AWSS3Bucket, Key: "beaches.xml"}
	err = common.Save(urlSet, saveOptions)
	if err != nil {
		return err
	}

	log.Printf("%s: Finished generating beaches url set in %v.", logPrefix, time.Now().Sub(start))
	return nil
}
