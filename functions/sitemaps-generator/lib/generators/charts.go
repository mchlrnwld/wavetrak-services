package generators

import (
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/common"
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/config"
	"log"
	"time"
)

// Charts ...
func Charts(logPrefix string, lastMod string) error {
	start := time.Now()

	log.Printf("%s: Loading charts...", logPrefix)
	charts, err := common.GetCharts()
	if err != nil {
		return err
	}

	log.Printf("%s: Creating charts url set...", logPrefix)
	urlSet, err := common.CreateChartsURLSet(charts, lastMod)
	if err != nil {
		return err
	}

	log.Printf("%s: Saving charts url set...", logPrefix)
	saveOptions := common.SaveOptions{Bucket: config.AWSS3Bucket, Key: "charts.xml"}
	err = common.Save(urlSet, saveOptions)
	if err != nil {
		return err
	}

	log.Printf("%s: Finished generating charts url set in %v.", logPrefix, time.Now().Sub(start))
	return nil
}
