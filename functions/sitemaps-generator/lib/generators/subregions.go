package generators

import (
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/common"
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/config"
	"log"
	"net/http"
	"time"
)

// Subregions ...
func Subregions(logPrefix string, lastMod string) error {
	start := time.Now()

	log.Printf("%s: Loading subregions...", logPrefix)
	httpClient := &http.Client{}
	subregions, err := common.GetSubregions(httpClient)
	if err != nil {
		return err
	}

	log.Printf("%s: Creating subregions url set...", logPrefix)
	urlSet, err := common.CreateSubregionsURLSet(subregions, lastMod)
	if err != nil {
		return err
	}

	log.Printf("%s: Saving subregions url set...", logPrefix)
	saveOptions := common.SaveOptions{Bucket: config.AWSS3Bucket, Key: "subregions.xml"}
	err = common.Save(urlSet, saveOptions)
	if err != nil {
		return err
	}

	log.Printf("%s: Finished generating subregions url set in %v.", logPrefix, time.Now().Sub(start))
	return nil
}
