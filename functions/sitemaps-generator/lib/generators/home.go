package generators

import (
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/common"
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/config"
	"log"
	"time"
)

// Home ...
func Home(logPrefix string, lastMod string) error {
	start := time.Now()

	urlObjects := []common.URLObject{
		common.Homepage{},
	}

	log.Printf("%s: Creating home url set...", logPrefix)
	urlSet, err := common.CreateURLSet(urlObjects, common.URLSetOptions{lastMod, "daily", "1.0"})
	if err != nil {
		return err
	}

	log.Printf("%s: Saving home url set...", logPrefix)
	saveOptions := common.SaveOptions{Bucket: config.AWSS3Bucket, Key: "home.xml"}
	err = common.Save(urlSet, saveOptions)
	if err != nil {
		return err
	}

	log.Printf("%s: Finished generating home url set in %v.", logPrefix, time.Now().Sub(start))
	return nil
}
