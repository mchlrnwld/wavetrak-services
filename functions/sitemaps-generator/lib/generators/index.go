package generators

import (
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/common"
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/config"
	"log"
	"time"
)

// Index ...
func Index(logPrefix string, lastMod string) error {
	start := time.Now()

	log.Printf("%s: Creating site map index...", logPrefix)
	siteMapIndex, err := common.CreateSiteMapIndex(lastMod)
	if err != nil {
		return err
	}

	log.Printf("%s: Saving site map index...", logPrefix)
	saveOptions := common.SaveOptions{Bucket: config.AWSS3Bucket, Key: "index.xml"}
	err = common.Save(siteMapIndex, saveOptions)
	if err != nil {
		return err
	}

	log.Printf("%s: Finished generating sitemap index in %v.", logPrefix, time.Now().Sub(start))
	return nil
}
