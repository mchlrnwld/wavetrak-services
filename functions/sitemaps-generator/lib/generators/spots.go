package generators

import (
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/common"
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/config"
	"log"
	"net/http"
	"time"
)

// Spots ...
func Spots(logPrefix string, lastMod string) error {
	start := time.Now()

	log.Printf("%s: Loading spots...", logPrefix)
	httpClient := &http.Client{}
	spots, err := common.GetSpots(httpClient)
	if err != nil {
		return err
	}

	log.Printf("%s: Creating spots url set...", logPrefix)
	urlSet, err := common.CreateSpotsURLSet(spots, lastMod)
	if err != nil {
		return err
	}

	log.Printf("%s: Saving spots url set...", logPrefix)
	saveOptions := common.SaveOptions{Bucket: config.AWSS3Bucket, Key: "spots.xml"}
	err = common.Save(urlSet, saveOptions)
	if err != nil {
		return err
	}

	log.Printf("%s: Finished generating spots url set in %v.", logPrefix, time.Now().Sub(start))
	return nil
}
