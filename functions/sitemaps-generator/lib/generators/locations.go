package generators

import (
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/common"
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/config"
	"log"
	"net/http"
	"time"
)

// Locations ...
func Locations(logPrefix string, lastMod string) error {
	start := time.Now()

	log.Printf("%s: Loading locations...", logPrefix)
	httpClient := &http.Client{}
	locations, err := common.GetLocations(httpClient, "geoname", "6295630")
	if err != nil {
		return err
	}

	log.Printf("%s: Creating locations url set...", logPrefix)
	urlSet, err := common.CreateLocationsURLSet(locations, lastMod)
	if err != nil {
		return err
	}

	log.Printf("%s: Saving locations url set...", logPrefix)
	saveOptions := common.SaveOptions{Bucket: config.AWSS3Bucket, Key: "locations.xml"}
	err = common.Save(urlSet, saveOptions)
	if err != nil {
		return err
	}

	log.Printf("%s: Finished generating locations url set in %v.", logPrefix, time.Now().Sub(start))
	return nil
}
