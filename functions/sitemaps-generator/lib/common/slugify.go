package common

import (
	"regexp"
	"strings"
)

func slugify(string string) string {
	slug := regexp.MustCompile("[^a-zA-Z0-9]").ReplaceAllString(string, "-")
	slug = strings.Replace(slug, "--", "-", -1)
	slug = strings.ToLower(slug)
	return slug
}
