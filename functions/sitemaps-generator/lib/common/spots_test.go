package common

import (
	"bytes"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"net/http"
	"testing"
)

// GetSpots
type MockHTTPClientSpots struct {
	mock.Mock
}

func (mockHTTPClient *MockHTTPClientSpots) Get(url string) (*http.Response, error) {
	response := &http.Response{
		Body: ioutil.NopCloser(bytes.NewBuffer([]byte("[{\"_id\":\"5842041f4e65fad6a7708a75\",\"name\":\"10th Street\"},{\"_id\":\"5842041f4e65fad6a7708a0d\",\"name\":\"10th Street North to 14th Street North\"},{\"_id\":\"584204204e65fad6a7709570\",\"name\":\"121's/Magney State Park\"}]"))),
	}
	return response, nil
}

func TestGetSpots(t *testing.T) {
	httpClient := &MockHTTPClientSpots{}
	spots, err := GetSpots(httpClient)
	require.Nil(t, err)
	require.Len(t, spots, 3)
	require.Equal(t, Spot{ID: "5842041f4e65fad6a7708a75", Name: "10th Street"}, spots[0])
	require.Equal(t, Spot{ID: "5842041f4e65fad6a7708a0d", Name: "10th Street North to 14th Street North"}, spots[1])
	require.Equal(t, Spot{ID: "584204204e65fad6a7709570", Name: "121's/Magney State Park"}, spots[2])
}

func TestGetSpotsCannotDecode(t *testing.T) {
	httpClient := &MockHTTPClientCannotDecode{}
	spots, err := GetSpots(httpClient)
	require.NotEmpty(t, err)
	require.Nil(t, spots)
}

func TestGetSpotsError(t *testing.T) {
	httpClient := &MockHTTPClientError{}
	spots, err := GetSpots(httpClient)
	require.NotEmpty(t, err)
	require.Nil(t, spots)
}

// GetSubregions
type MockHTTPClientSubregions struct {
	mock.Mock
}

func (mockHTTPClient *MockHTTPClientSubregions) Get(url string) (*http.Response, error) {
	response := &http.Response{
		Body: ioutil.NopCloser(bytes.NewBuffer([]byte("[{\"_id\":\"58581a836630e24c44878fd6\",\"name\":\"North Orange County\"},{\"_id\":\"58581a836630e24c4487900a\",\"name\":\"South Orange County\"},{\"_id\":\"58581a836630e24c44878fd7\",\"name\":\"North San Diego\"}]"))),
	}
	return response, nil
}

func TestGetSubregions(t *testing.T) {
	httpClient := &MockHTTPClientSubregions{}
	subregions, err := GetSubregions(httpClient)
	require.Nil(t, err)
	require.Len(t, subregions, 3)
	require.Equal(t, Subregion{ID: "58581a836630e24c44878fd6", Name: "North Orange County"}, subregions[0])
	require.Equal(t, Subregion{ID: "58581a836630e24c4487900a", Name: "South Orange County"}, subregions[1])
	require.Equal(t, Subregion{ID: "58581a836630e24c44878fd7", Name: "North San Diego"}, subregions[2])
}

func TestGetSubregionsCannotDecode(t *testing.T) {
	httpClient := &MockHTTPClientCannotDecode{}
	subregions, err := GetSubregions(httpClient)
	require.NotEmpty(t, err)
	require.Nil(t, subregions)
}

func TestGetSubregionsError(t *testing.T) {
	httpClient := &MockHTTPClientError{}
	subregions, err := GetSubregions(httpClient)
	require.NotEmpty(t, err)
	require.Nil(t, subregions)
}
