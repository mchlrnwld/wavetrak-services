package common

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestSlugify(t *testing.T) {
	require.Equal(t, "hb-pier-southside", slugify("HB Pier, Southside"))
	require.Equal(t, "8th-street-ocean-city-md", slugify("8th Street, Ocean City, MD"))
}
