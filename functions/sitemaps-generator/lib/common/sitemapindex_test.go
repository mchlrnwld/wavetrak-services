package common

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestCreateSiteMapIndex(t *testing.T) {
	siteMapIndex, err := CreateSiteMapIndex("2017-07-31")
	require.Nil(t, err)
	require.Equal(t, "<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n\t<sitemap>\n\t\t<loc>/sitemaps/home.xml</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t</sitemap>\n\t<sitemap>\n\t\t<loc>/sitemaps/spots.xml</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t</sitemap>\n\t<sitemap>\n\t\t<loc>/sitemaps/locations.xml</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t</sitemap>\n\t<sitemap>\n\t\t<loc>/sitemaps/subregions.xml</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t</sitemap>\n\t<sitemap>\n\t\t<loc>/sitemaps/beaches.xml</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t</sitemap>\n\t<sitemap>\n\t\t<loc>/sitemaps/charts.xml</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t</sitemap>\n\t<sitemap>\n\t\t<loc>/sitemaps/news.xml</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t</sitemap>\n</sitemapindex>", string(siteMapIndex))
}
