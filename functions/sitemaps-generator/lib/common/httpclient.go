package common

import "net/http"

// HTTPClient ...
type HTTPClient interface {
	Get(string) (*http.Response, error)
}
