package common

import (
	"bytes"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"net/http"
	"testing"
)

// GetBeaches
type MockHTTPClientTaxonomy struct {
	mock.Mock
}

func (mockHTTPClient *MockHTTPClientTaxonomy) Get(url string) (*http.Response, error) {
	response := &http.Response{
		Body: ioutil.NopCloser(bytes.NewBuffer([]byte("{\"contains\":[{\"associated\":{\"links\":[{\"key\":\"travel\",\"href\":\"https://sandbox.surfline.com/travel/united-states/california/orange-county/laguna-niguel-surfing-and-beaches/5364329\"}]}},{\"associated\":{\"links\":[{\"key\":\"travel\",\"href\":\"https://sandbox.surfline.com/travel/united-states/california/orange-county/newport-beach-surfing-and-beaches/5376890\"}]}}]}"))),
	}
	return response, nil
}

func TestGetBeaches(t *testing.T) {
	httpClient := &MockHTTPClientTaxonomy{}
	beaches, err := GetBeaches(httpClient, "geoname", "6295630")
	require.Nil(t, err)
	require.Len(t, beaches, 2)
	require.Equal(t, Beach{
		Associated: Associated{
			Links: []Link{
				Link{Key: "travel", Href: "https://sandbox.surfline.com/travel/united-states/california/orange-county/laguna-niguel-surfing-and-beaches/5364329"},
			},
		},
	}, beaches[0])
	require.Equal(t, Beach{
		Associated: Associated{
			Links: []Link{
				Link{Key: "travel", Href: "https://sandbox.surfline.com/travel/united-states/california/orange-county/newport-beach-surfing-and-beaches/5376890"},
			},
		},
	}, beaches[1])
}

func TestGetBeachesCannotDecode(t *testing.T) {
	httpClient := &MockHTTPClientCannotDecode{}
	beaches, err := GetBeaches(httpClient, "geoname", "6295630")
	require.NotEmpty(t, err)
	require.Nil(t, beaches)
}

func TestGetBeachesError(t *testing.T) {
	httpClient := &MockHTTPClientError{}
	beaches, err := GetBeaches(httpClient, "geoname", "6295630")
	require.NotEmpty(t, err)
	require.Nil(t, beaches)
}

// GetLocations
func TestGetLocations(t *testing.T) {
	httpClient := &MockHTTPClientTaxonomy{}
	locations, err := GetLocations(httpClient, "geoname", "6295630")
	require.Nil(t, err)
	require.Len(t, locations, 2)
	require.Equal(t, Location{
		Associated: Associated{
			Links: []Link{
				Link{Key: "travel", Href: "https://sandbox.surfline.com/travel/united-states/california/orange-county/laguna-niguel-surfing-and-beaches/5364329"},
			},
		},
	}, locations[0])
	require.Equal(t, Location{
		Associated: Associated{
			Links: []Link{
				Link{Key: "travel", Href: "https://sandbox.surfline.com/travel/united-states/california/orange-county/newport-beach-surfing-and-beaches/5376890"},
			},
		},
	}, locations[1])
}

func TestGetLocationsCannotDecode(t *testing.T) {
	httpClient := &MockHTTPClientCannotDecode{}
	locations, err := GetLocations(httpClient, "geoname", "6295630")
	require.NotEmpty(t, err)
	require.Nil(t, locations)
}

func TestGetLocationsError(t *testing.T) {
	httpClient := &MockHTTPClientError{}
	locations, err := GetLocations(httpClient, "geoname", "6295630")
	require.NotEmpty(t, err)
	require.Nil(t, locations)
}
