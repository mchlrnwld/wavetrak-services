package common

import (
	"encoding/json"
	"io/ioutil"
)

// SuperregionChart ...
type SuperregionChart struct {
	URL     string
	Regions []RegionChart
}

// RegionChart ...
type RegionChart struct {
	URL        string
	Subregions []SubregionChart
}

// SubregionChart ...
type SubregionChart struct {
	URL string
}

// Chart ...
type Chart struct {
	URL string
}

func createChartsFromSuperregions(superregions []SuperregionChart) []Chart {
	var charts []Chart
	for _, superregion := range superregions {
		charts = append(charts, Chart{URL: superregion.URL})

		for _, region := range superregion.Regions {
			charts = append(charts, Chart{URL: region.URL})

			for _, subregion := range region.Subregions {
				charts = append(charts, Chart{URL: subregion.URL})
			}
		}
	}
	return charts
}

// GetCharts ...
func GetCharts() ([]Chart, error) {
	file, err := ioutil.ReadFile("chart-data.json")
	if err != nil {
		return nil, err
	}

	var superregions []SuperregionChart
	errJSON := json.Unmarshal([]byte(file), &superregions)

	if errJSON != nil {
		return nil, err
	}

	charts := createChartsFromSuperregions(superregions)

	return charts, nil
}
