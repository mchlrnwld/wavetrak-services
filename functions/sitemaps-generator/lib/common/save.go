package common

import (
	"bytes"
	"encoding/xml"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"io/ioutil"
)

// SaveOptions ...
type SaveOptions struct {
	Bucket string
	Key    string
}

// Save ...
func Save(data []byte, options SaveOptions) error {
	xmlData := []byte(xml.Header + string(data))

	if options.Bucket == "" {
		return ioutil.WriteFile(options.Key, xmlData, 0644)
	}

	sess, err := session.NewSession()

	if err != nil {
		return err
	}

	input := &s3.PutObjectInput{
		Body:        aws.ReadSeekCloser(bytes.NewReader(xmlData)),
		Bucket:      aws.String(options.Bucket),
		Key:         aws.String(options.Key),
		ContentType: aws.String("application/xml"),
	}

	config := &aws.Config{Region: aws.String("us-west-1")}
	service := s3.New(sess, config)
	_, err = service.PutObject(input)

	return err
}
