package common

import (
	"encoding/xml"
	"fmt"
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/config"
	"strings"
)

// URLSet ...
type URLSet struct {
	XMLName xml.Name `xml:"urlset"`
	XMLNS   string   `xml:"xmlns,attr"`
	URLs    []URL    `xml:",innerxml"`
}

// URL ...
type URL struct {
	XMLName    xml.Name `xml:"url"`
	Loc        string   `xml:"loc"`
	LastMod    string   `xml:"lastmod"`
	ChangeFreq string   `xml:"changefreq"`
	Priority   string   `xml:"priority"`
}

// URLObject ...
type URLObject interface {
	urls() []string
}

// URLSetOptions ...
type URLSetOptions struct {
	LastMod    string
	ChangeFreq string
	Priority   string
}

func (homepage Homepage) urls() []string {
	return []string{
		config.SurflineHost,
	}
}

func (spot Spot) urls() []string {
	return []string{
		fmt.Sprintf("%s/surf-report/%s/%s", config.SurflineHost, slugify(spot.Name), spot.ID),
		fmt.Sprintf("%s/surf-report/%s/%s/forecast", config.SurflineHost, slugify(spot.Name), spot.ID),
	}
}

func (subregion Subregion) urls() []string {
	return []string{
		fmt.Sprintf("%s/surf-forecasts/%s/%s", config.SurflineHost, slugify(subregion.Name), subregion.ID),
		fmt.Sprintf("%s/surf-forecasts/%s/%s/premium-analysis", config.SurflineHost, slugify(subregion.Name), subregion.ID),
		fmt.Sprintf("%s/surf-forecasts/%s/%s/premium-analysis/real-time-forecast", config.SurflineHost, slugify(subregion.Name), subregion.ID),
		fmt.Sprintf("%s/surf-forecasts/%s/%s/premium-analysis/short-term-forecast", config.SurflineHost, slugify(subregion.Name), subregion.ID),
		fmt.Sprintf("%s/surf-forecasts/%s/%s/premium-analysis/long-term-forecast", config.SurflineHost, slugify(subregion.Name), subregion.ID),
		fmt.Sprintf("%s/surf-forecasts/%s/%s/premium-analysis/seasonal-forecast", config.SurflineHost, slugify(subregion.Name), subregion.ID),
	}
}

func (beach Beach) urls() []string {
	var urls []string
	for _, link := range beach.Associated.Links {
		if link.Key == "travel" {
			urls = append(urls, link.Href)
			break
		}
	}
	return urls
}

func (location Location) urls() []string {
	var urls []string
	for _, link := range location.Associated.Links {
		if link.Key == "www" && strings.Contains(link.Href, "surf-reports-forecasts-cams") {
			urls = append(urls, link.Href)
			break
		}
	}
	return urls
}

func (chart Chart) urls() []string {
	return []string{
		fmt.Sprintf("%s%s", config.SurflineHost, chart.URL),
	}
}

// CreateSpotsURLSet ...
func CreateSpotsURLSet(spots []Spot, lastMod string) ([]byte, error) {
	var urlObjects []URLObject = make([]URLObject, len(spots))
	for i, spot := range spots {
		urlObjects[i] = spot
	}
	return CreateURLSet(urlObjects, URLSetOptions{lastMod, "daily", "0.9"})
}

// CreateSubregionsURLSet ...
func CreateSubregionsURLSet(subregions []Subregion, lastMod string) ([]byte, error) {
	var urlObjects []URLObject = make([]URLObject, len(subregions))
	for i, subregion := range subregions {
		urlObjects[i] = subregion
	}
	return CreateURLSet(urlObjects, URLSetOptions{lastMod, "daily", "0.7"})
}

// CreateBeachesURLSet ...
func CreateBeachesURLSet(beaches []Beach, lastMod string) ([]byte, error) {
	var urlObjects []URLObject = make([]URLObject, len(beaches))
	for i, beach := range beaches {
		urlObjects[i] = beach
	}
	return CreateURLSet(urlObjects, URLSetOptions{lastMod, "daily", "0.6"})
}

// CreateLocationsURLSet ...
func CreateLocationsURLSet(locations []Location, lastMod string) ([]byte, error) {
	var urlObjects []URLObject = make([]URLObject, len(locations))
	for i, location := range locations {
		urlObjects[i] = location
	}
	return CreateURLSet(urlObjects, URLSetOptions{lastMod, "daily", "0.8"})
}

// CreateChartsURLSet ...
func CreateChartsURLSet(charts []Chart, lastMod string) ([]byte, error) {
	var urlObjects []URLObject = make([]URLObject, len(charts))
	for i, chart := range charts {
		urlObjects[i] = chart
	}
	return CreateURLSet(urlObjects, URLSetOptions{lastMod, "daily", "0.5"})
}

// CreateURLSet ...
func CreateURLSet(urlObjects []URLObject, options URLSetOptions) ([]byte, error) {
	urlSet := URLSet{XMLNS: "http://www.sitemaps.org/schemas/sitemap/0.9"}
	for _, urlObject := range urlObjects {
		urls := urlObject.urls()
		for _, urlString := range urls {
			url := URL{
				Loc:        urlString,
				LastMod:    options.LastMod,
				ChangeFreq: options.ChangeFreq,
				Priority:   options.Priority,
			}
			urlSet.URLs = append(urlSet.URLs, url)
		}
	}
	return xml.MarshalIndent(urlSet, "", "	")
}
