package common

import (
	"bytes"
	"errors"
	"github.com/stretchr/testify/mock"
	"io/ioutil"
	"net/http"
)

type MockHTTPClientCannotDecode struct {
	mock.Mock
}

func (mockHTTPClient *MockHTTPClientCannotDecode) Get(url string) (*http.Response, error) {
	response := &http.Response{
		Body: ioutil.NopCloser(bytes.NewBuffer([]byte("Invalid JSON Response"))),
	}
	return response, nil
}

type MockHTTPClientError struct {
	mock.Mock
}

func (mockHTTPClient *MockHTTPClientError) Get(url string) (*http.Response, error) {
	return nil, errors.New("An error occurred")
}
