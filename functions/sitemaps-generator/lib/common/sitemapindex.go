package common

import (
	"encoding/xml"
	"fmt"
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/config"
)

// SiteMapIndex ...
type SiteMapIndex struct {
	XMLName  xml.Name  `xml:"sitemapindex"`
	XMLNS    string    `xml:"xmlns,attr"`
	SiteMaps []SiteMap `xml:",innerxml"`
}

// SiteMap ...
type SiteMap struct {
	XMLName xml.Name `xml:"sitemap"`
	Loc     string   `xml:"loc"`
	LastMod string   `xml:"lastmod"`
}

// CreateSiteMapIndex ...
func CreateSiteMapIndex(lastMod string) ([]byte, error) {
	siteMapIndex := SiteMapIndex{
		XMLNS: "http://www.sitemaps.org/schemas/sitemap/0.9",
		SiteMaps: []SiteMap{
			SiteMap{Loc: fmt.Sprintf("%s/sitemaps/home.xml", config.SurflineHost), LastMod: lastMod},
			SiteMap{Loc: fmt.Sprintf("%s/sitemaps/spots.xml", config.SurflineHost), LastMod: lastMod},
			SiteMap{Loc: fmt.Sprintf("%s/sitemaps/locations.xml", config.SurflineHost), LastMod: lastMod},
			SiteMap{Loc: fmt.Sprintf("%s/sitemaps/subregions.xml", config.SurflineHost), LastMod: lastMod},
			SiteMap{Loc: fmt.Sprintf("%s/sitemaps/beaches.xml", config.SurflineHost), LastMod: lastMod},
			SiteMap{Loc: fmt.Sprintf("%s/sitemaps/charts.xml", config.SurflineHost), LastMod: lastMod},
			SiteMap{Loc: fmt.Sprintf("%s/sitemaps/news.xml", config.SurflineHost), LastMod: lastMod},
		},
	}
	return xml.MarshalIndent(siteMapIndex, "", "	")
}
