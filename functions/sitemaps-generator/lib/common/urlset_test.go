package common

import (
	"github.com/stretchr/testify/require"
	"testing"
)

const lastMod = "2017-07-31"

func TestCreateSpotsURLSet(t *testing.T) {
	spots := []Spot{
		Spot{ID: "5842041f4e65fad6a7708827", Name: "HB Pier, Northside"},
		Spot{ID: "5842041f4e65fad6a77088ed", Name: "HB Pier, Southside"},
		Spot{ID: "584204204e65fad6a770998c", Name: "Huntington State Beach"},
	}
	urlSet, err := CreateSpotsURLSet(spots, lastMod)
	require.Nil(t, err)
	require.Equal(t, "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n\t<url>\n\t\t<loc>/surf-report/hb-pier-northside/5842041f4e65fad6a7708827</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.9</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-report/hb-pier-northside/5842041f4e65fad6a7708827/forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.9</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-report/hb-pier-southside/5842041f4e65fad6a77088ed</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.9</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-report/hb-pier-southside/5842041f4e65fad6a77088ed/forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.9</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-report/huntington-state-beach/584204204e65fad6a770998c</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.9</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-report/huntington-state-beach/584204204e65fad6a770998c/forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.9</priority>\n\t</url>\n</urlset>", string(urlSet))
}

func TestCreateSubregionsURLSet(t *testing.T) {
	subregions := []Subregion{
		Subregion{ID: "58581a836630e24c44878fd6", Name: "North Orange County"},
		Subregion{ID: "58581a836630e24c4487900a", Name: "South Orange County"},
		Subregion{ID: "58581a836630e24c44878fd7", Name: "North San Diego"},
	}
	urlSet, err := CreateSubregionsURLSet(subregions, lastMod)
	require.Nil(t, err)
	require.Equal(t, "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n\t<url>\n\t\t<loc>/surf-forecasts/north-orange-county/58581a836630e24c44878fd6</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/north-orange-county/58581a836630e24c44878fd6/premium-analysis</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/north-orange-county/58581a836630e24c44878fd6/premium-analysis/real-time-forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/north-orange-county/58581a836630e24c44878fd6/premium-analysis/short-term-forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/north-orange-county/58581a836630e24c44878fd6/premium-analysis/long-term-forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/north-orange-county/58581a836630e24c44878fd6/premium-analysis/seasonal-forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/south-orange-county/58581a836630e24c4487900a</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/south-orange-county/58581a836630e24c4487900a/premium-analysis</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/south-orange-county/58581a836630e24c4487900a/premium-analysis/real-time-forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/south-orange-county/58581a836630e24c4487900a/premium-analysis/short-term-forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/south-orange-county/58581a836630e24c4487900a/premium-analysis/long-term-forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/south-orange-county/58581a836630e24c4487900a/premium-analysis/seasonal-forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/north-san-diego/58581a836630e24c44878fd7</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/north-san-diego/58581a836630e24c44878fd7/premium-analysis</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/north-san-diego/58581a836630e24c44878fd7/premium-analysis/real-time-forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/north-san-diego/58581a836630e24c44878fd7/premium-analysis/short-term-forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/north-san-diego/58581a836630e24c44878fd7/premium-analysis/long-term-forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-forecasts/north-san-diego/58581a836630e24c44878fd7/premium-analysis/seasonal-forecast</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.7</priority>\n\t</url>\n</urlset>", string(urlSet))
}

func TestCreateBeachesURLSet(t *testing.T) {
	beaches := []Beach{
		Beach{
			Associated: Associated{
				Links: []Link{
					Link{Key: "taxonomy", Href: "http://spots-api.surfline.com/taxonomy?id=5364329&type=geoname"},
					Link{Key: "www", Href: "https://www.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/laguna-niguel/5364329"},
					Link{Key: "travel", Href: "https://www.surfline.com/travel/united-states/california/orange-county/laguna-niguel-surfing-and-beaches/5364329"},
				},
			},
		},
		Beach{
			Associated: Associated{
				Links: []Link{
					Link{Key: "taxonomy", Href: "http://spots-api.surfline.com/taxonomy?id=5376890&type=geoname"},
					Link{Key: "www", Href: "https://www.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/newport-beach/5376890"},
					Link{Key: "travel", Href: "https://www.surfline.com/travel/united-states/california/orange-county/newport-beach-surfing-and-beaches/5376890"},
				},
			},
		},
		Beach{
			Associated: Associated{
				Links: []Link{
					Link{Key: "taxonomy", Href: "http://spots-api.surfline.com/taxonomy?id=5341483&type=geoname"},
					Link{Key: "www", Href: "https://www.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/dana-point/5341483"},
					Link{Key: "travel", Href: "https://www.surfline.com/travel/united-states/california/orange-county/dana-point-surfing-and-beaches/5341483"},
				},
			},
		},
	}
	urlSet, err := CreateBeachesURLSet(beaches, lastMod)
	require.Nil(t, err)
	require.Equal(t, "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n\t<url>\n\t\t<loc>https://www.surfline.com/travel/united-states/california/orange-county/laguna-niguel-surfing-and-beaches/5364329</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.6</priority>\n\t</url>\n\t<url>\n\t\t<loc>https://www.surfline.com/travel/united-states/california/orange-county/newport-beach-surfing-and-beaches/5376890</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.6</priority>\n\t</url>\n\t<url>\n\t\t<loc>https://www.surfline.com/travel/united-states/california/orange-county/dana-point-surfing-and-beaches/5341483</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.6</priority>\n\t</url>\n</urlset>", string(urlSet))
}

func TestCreateLocationsURLSet(t *testing.T) {
	locations := []Location{
		Location{
			Associated: Associated{
				Links: []Link{
					Link{Key: "taxonomy", Href: "http://spots-api.surfline.com/taxonomy?id=5364329&type=geoname"},
					Link{Key: "www", Href: "https://www.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/laguna-niguel/5364329"},
					Link{Key: "travel", Href: "https://www.surfline.com/travel/united-states/california/orange-county/laguna-niguel-surfing-and-beaches/5364329"},
				},
			},
		},
		Location{
			Associated: Associated{
				Links: []Link{
					Link{Key: "taxonomy", Href: "http://spots-api.surfline.com/taxonomy?id=5376890&type=geoname"},
					Link{Key: "www", Href: "https://www.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/newport-beach/5376890"},
					Link{Key: "travel", Href: "https://www.surfline.com/travel/united-states/california/orange-county/newport-beach-surfing-and-beaches/5376890"},
				},
			},
		},
		Location{
			Associated: Associated{
				Links: []Link{
					Link{Key: "taxonomy", Href: "http://spots-api.surfline.com/taxonomy?id=5341483&type=geoname"},
					Link{Key: "www", Href: "https://www.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/dana-point/5341483"},
					Link{Key: "travel", Href: "https://www.surfline.com/travel/united-states/california/orange-county/dana-point-surfing-and-beaches/5341483"},
				},
			},
		},
		Location{
			Associated: Associated{
				Links: []Link{
					Link{Key: "taxonomy", Href: "http://spots-api.sandbox.surfline.com/taxonomy?id=584204204e65fad6a770998c&type=spot"},
					Link{Key: "api", Href: "http://spots-api.sandbox.surfline.com/admin/spots/584204204e65fad6a770998c"},
					Link{Key: "www", Href: "https://sandbox.surfline.com/surf-report/huntington-state-beach/584204204e65fad6a770998c/forecast"},
				},
			},
		},
	}
	urlSet, err := CreateLocationsURLSet(locations, lastMod)
	require.Nil(t, err)
	require.Equal(t, "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n\t<url>\n\t\t<loc>https://www.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/laguna-niguel/5364329</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.8</priority>\n\t</url>\n\t<url>\n\t\t<loc>https://www.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/newport-beach/5376890</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.8</priority>\n\t</url>\n\t<url>\n\t\t<loc>https://www.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/dana-point/5341483</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.8</priority>\n\t</url>\n</urlset>", string(urlSet))
}

func TestCreateChartsURLSet(t *testing.T) {
	charts := []Chart{
		Chart{URL: "/surf-charts/north-america/4716"},
		Chart{URL: "/surf-charts/north-america/british-columbia/7997"},
		Chart{URL: "/surf-charts/north-america/british-columbia/vancouver-island/8001"},
		Chart{URL: "/surf-charts/north-america/pacific-northwest/2082"},
	}
	urlSet, err := CreateChartsURLSet(charts, lastMod)
	require.Nil(t, err)
	require.Equal(t, "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n\t<url>\n\t\t<loc>/surf-charts/north-america/4716</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.5</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-charts/north-america/british-columbia/7997</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.5</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-charts/north-america/british-columbia/vancouver-island/8001</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.5</priority>\n\t</url>\n\t<url>\n\t\t<loc>/surf-charts/north-america/pacific-northwest/2082</loc>\n\t\t<lastmod>2017-07-31</lastmod>\n\t\t<changefreq>daily</changefreq>\n\t\t<priority>0.5</priority>\n\t</url>\n</urlset>", string(urlSet))
}
