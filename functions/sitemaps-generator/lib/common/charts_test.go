package common

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestGetCharts(t *testing.T) {
	charts, err := GetCharts()
	require.Nil(t, err)
	require.Len(t, charts, 533)
	require.Equal(t, Chart{URL: "/surf-charts/north-america/4716"}, charts[0])
	require.Equal(t, Chart{URL: "/surf-charts/north-america/british-columbia/7997"}, charts[1])
	require.Equal(t, Chart{URL: "/surf-charts/north-america/british-columbia/vancouver-island/8001"}, charts[2])
}
