package common

import (
	"encoding/json"
	"fmt"
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/config"
)

// Spot ...
type Spot struct {
	ID   string `json:"_id"`
	Name string
}

// Subregion ...
type Subregion struct {
	ID   string `json:"_id"`
	Name string
}

// GetSpots ...
func GetSpots(httpClient HTTPClient) ([]Spot, error) {
	url := fmt.Sprintf("%s/admin/spots/?status=published", config.SpotsAPI)
	res, err := httpClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var spots []Spot
	err = json.NewDecoder(res.Body).Decode(&spots)

	return spots, err
}

// GetSubregions ...
func GetSubregions(httpClient HTTPClient) ([]Subregion, error) {
	url := fmt.Sprintf("%s/admin/subregions/?status=published", config.SpotsAPI)
	res, err := httpClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var subregions []Subregion
	err = json.NewDecoder(res.Body).Decode(&subregions)

	return subregions, err
}
