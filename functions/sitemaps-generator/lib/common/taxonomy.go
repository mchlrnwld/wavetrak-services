package common

import (
	"encoding/json"
	"fmt"
	"github.com/Surfline/wavetrak-services/functions/sitemaps-generator/lib/config"
)

// TopLevelBeaches ...
type TopLevelBeaches struct {
	Contains []Beach
}

// TopLevelLocations ...
type TopLevelLocations struct {
	Contains []Location
}

// Beach ...
type Beach struct {
	Associated Associated
}

// Location ...
type Location struct {
	Associated Associated
}

// Associated ...
type Associated struct {
	Links []Link
}

// Link ...
type Link struct {
	Key  string
	Href string
}

// GetBeaches ...
func GetBeaches(httpClient HTTPClient, queryType string, id string) ([]Beach, error) {
	url := fmt.Sprintf("%s/taxonomy?type=%s&id=%s", config.SpotsAPI, queryType, id)
	res, err := httpClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var taxonomy TopLevelBeaches
	err = json.NewDecoder(res.Body).Decode(&taxonomy)

	return taxonomy.Contains, err
}

// GetLocations ...
func GetLocations(httpClient HTTPClient, queryType string, id string) ([]Location, error) {
	url := fmt.Sprintf("%s/taxonomy?type=%s&id=%s", config.SpotsAPI, queryType, id)
	res, err := httpClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var taxonomy TopLevelLocations
	err = json.NewDecoder(res.Body).Decode(&taxonomy)

	return taxonomy.Contains, err
}
