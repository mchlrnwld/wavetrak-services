module github.com/Surfline/wavetrak-services/functions/sitemaps-generator

go 1.17

require (
	github.com/aws/aws-lambda-go v1.27.0
	github.com/aws/aws-sdk-go v1.10.18-0.20170727041920-b76250596b58
	github.com/fnproject/fdk-go v0.0.12
	github.com/newrelic/go-agent/v3 v3.15.1
	github.com/newrelic/go-agent/v3/integrations/nrlambda v1.2.1
	github.com/stretchr/testify v1.7.0
	github.com/tencentyun/scf-go-lib v0.0.0-20200624065115-ba679e2ec9c9
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-ini/ini v1.28.1 // indirect
	github.com/golang/protobuf v1.3.3 // indirect
	github.com/jmespath/go-jmespath v0.0.0-20160803190731-bd40a432e4c7 // indirect
	github.com/newrelic/go-agent v3.15.1+incompatible // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/net v0.0.0-20211007125505-59d4e928ea9d // indirect
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
	golang.org/x/text v0.3.6 // indirect
	golang.org/x/tools v0.1.7 // indirect
	google.golang.org/genproto v0.0.0-20190819201941-24fa4b261c55 // indirect
	google.golang.org/grpc v1.27.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
