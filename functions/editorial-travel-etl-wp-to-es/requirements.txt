# runtime python requirments
boto3==1.19.4
botocore==1.22.5
elasticsearch==5.5.3
python-dotenv==0.19.1
requests==2.26.0
requests-aws4auth==1.1.1
unidecode==1.3.2
urllib3==1.26.7
