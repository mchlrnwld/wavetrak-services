# -*- coding: utf-8 -*-

import json
from os.path import dirname, join

import mock  # type: ignore
from lib import wordpress_client
from pytest import fixture

fixtures_path = join(dirname(__file__), 'fixtures')


def post_1054():
    post_1054_path = join(fixtures_path, 'post_1054.json')
    with open(post_1054_path, 'r') as post_1054_json:
        return json.load(post_1054_json)


def media_275():
    media_275_path = join(fixtures_path, 'media_275.json')
    with open(media_275_path, 'r') as media_275_json:
        return json.load(media_275_json)


def users_1():
    users_1_path = join(fixtures_path, 'users_1.json')
    with open(users_1_path, 'r') as users_1_json:
        return json.load(users_1_json)


@fixture
def post_to_index_1054():
    post_to_index_1054_path = join(fixtures_path, 'post_to_index_1054.json')
    with open(post_to_index_1054_path, 'r') as post_to_index_1054_json:
        return json.load(post_to_index_1054_json)


def get_wp_data(post_id, type):
    if post_id == 1054 and type == 'travel_page':
        return post_1054()
    elif post_id == 275 and type == 'media':
        return media_275()
    elif post_id == 1072 and type == 'media':
        return media_275()
    elif post_id == 1 and type == 'users':
        return users_1()
    return {}


def get_wp_data_with_filter(post_id, filter):
    return []


class TestHandler:
    @mock.patch('lib.wordpress_client.get_wp_data', get_wp_data)
    @mock.patch(
        'lib.wordpress_client.get_wp_data_with_filter', get_wp_data_with_filter
    )
    def test_post_1054(self, post_to_index_1054):
        result = wordpress_client.build_post(1054, post_to_index_1054['media'])
        assert result['media'] == post_to_index_1054['media']
        assert result['content']['body'] == (
            post_to_index_1054['content']['body']
        )
        assert result['content']['title'] == (
            post_to_index_1054['content']['title']
        )
        assert result['content']['subtitle'] == (
            post_to_index_1054['content']['subtitle']
        )
        assert result['targetTaxonomy'] == post_to_index_1054['targetTaxonomy']
        assert result['relatedPosts'][0] == 303
