# Travel ETL Wordpress to Elasticsearch

This Lambda function (sl-editorial-travel-etl-wp-to-es-{env}) updates the Elasticsearch travel page index with Wordpress content.


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents <!-- omit in toc -->

- [Components overview](#components-overview)
- [Serverless Framework](#serverless-framework)
  - [Installing](#installing)
  - [`serverless.yml`](#serverlessyml)
- [Deploying to AWS](#deploying-to-aws)
- [`Makefile`](#makefile)
- [Python Requirements](#python-requirements)
- [`infra` Directory](#infra-directory)
- [Summary](#summary)
- [Development](#development)
  - [Installation](#installation)
  - [Editorial environment (optional)](#editorial-environment-optional)
    - [Build containers, start full WP/ES/Kibana dev stack](#build-containers-start-full-wpeskibana-dev-stack)
    - [Start/stop/restart full WP/ES/Kibana dev stack](#startstoprestart-full-wpeskibana-dev-stack)
    - [Run Python bash shell in the running dev stack](#run-python-bash-shell-in-the-running-dev-stack)
- [Tests and invokes](#tests-and-invokes)
  - [Invoke Lambda function in running dev stack](#invoke-lambda-function-in-running-dev-stack)
  - [Invoke Lambda function against AWS](#invoke-lambda-function-against-aws)
    - [Request](#request)
    - [Response](#response)
- [Jenkins deployment](#jenkins-deployment)
- [Monitoring](#monitoring)
  - [Configuration](#configuration)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Components overview

- Most build/dev tools are wrangled by `make`: see [`Makefile`](#makefile)
- Python dependencies: handled by [`environment.devenv.yml` and `requirements.txt`](#python-requirements)
- Lambda function: configured in [`serverless.yml`](#serverlessyml).
- [New Relic] alerts: configured in [`infra`](#infra-directory) directory.

## Serverless Framework

This Lambda function is managed and provisioned by the Serverless Framework.

### Installing

To install the Serverless Framework, you must have Node version >=10 and npm installed. Once both are installed, simply run the following command:

```
npm install
```

This will install Serverless and all required plugins.

### `serverless.yml`

Serverless Framework uses this file to create and deploy a lambda functions to any given AWS account. The top-level sections of the serverless.yml are:

- `service`

  This describes the service associated with the Lambda function, for example: tag-ec2-instances. The service name will be used to create the Cloudformation template that then creates the Lambda function.

- `frameworkVersion`

  Version of Serverless Framework being used.

- `provider`

  This section is where all AWS-related information is declared. For example: the AWS region, the AWS account associated with the Lambda, and any IAM role permissions. It also contains information about the Python version.

- `plugins`

  This section is where all 3rd-party Serverless plugins are declared. For example, the serverless-python-requirements allows you to easily package and deploy a Python Lambda with 3rd-party dependencies.

- `package`

  What patterns to include and what not to include in the runtime package that gets uploaded to AWS.

- `functions`

  This section is where all Lambda functions are declared. Do note that many Lambda functions can be declared here, not just one. You can specify the name of the function, the handler to use, any associated infrastructure (Cloudwatch, SNS, etc) and more.

## Deploying to AWS

Included in the `Makefile` is `ENV=<env-name> make deploy`, which allows you to easily deploy the lambda to the AWS account and environment of choice. The desired environment must be passed in. For example:
```sh
AWS_PROFILE=surfline-dev ENV=sandbox make deploy
```

NOTE: There are 3 relevant environments for this lambda function: `sandbox`, `staging` and `prod`. Additionally, `dev` is used as a placeholder for an environment when using `serverless` to run code locally.

## `Makefile`

This project uses the following python development tools.
- `autoflake`
- `black`
- `flake8`
- `isort`
- `mypy`
- `pytest`

Although these tools can be configured using separate configuration files, we rather keep all the required flags in one place in the `Makefile`.

The `Makefile` supports these targets:

- `make format` to format the source files in the project.
- `make lint` to run the python linter (`flake8` and `black`)
- `make type-check` to run the python static type checker `mypy`.
- `make test-unit` to run `pytest`. Unit tests are defined in `test.json`.
- `ENV=<env-name> make invoke-local` to invoke the python function on your local machine with a sample event.
- `ENV=<env-name> make deploy` to deploy the code from your machine to AWS. Also see [Deploying to AWS](#deploying-to-aws).
- `ENV=<env-name> make remove` to uninstall the function from AWS.

## Python Requirements
This project uses `conda` to manage python dependencies.

All dependencies required for runtime are installed via pip in `environment.yml`. Dev-only dependencies are defined in `environment.devenv.yml`. (`boto3-stubs` is put in `requirements.txt` because it's important that it has the same version as `boto3`).

## `infra` Directory

This directory contains the terraform module to deploy additonal infrastructure: in this case [New Relic] alert definitions.

## Summary

This Lambda function (sl-editorial-travel-etl-wp-to-es-{env}) updates the Elasticsearch travel page index with Wordpress content.

## Development

The following convenience commands are available. View the contents of `Makefile` to see how these commands translate to docker commands.

### Installation

Copy the `.env.sample` from into `.env` and configure your python environment.

```bash
conda install conda-devenv -c conda-forge -y
conda devenv
conda activate editorial-travel-etl-wp-to-es-function
```

## Tests and invokes

The `tests.json` file is a sample event from SNS and we can test the event by invoking the function via `make` commands or directly using the AWS cli:

### Invoke Lambda function in running dev stack

Test lambda invocation:

```bash
make type-check
```

### Invoke Lambda function against AWS

Configure environment:

```bash
vim .env # uncomment remote section, comment local section

# If using .env, use the Remote section
export AWS_ACCESS_KEY_ID={your key}
export AWS_DEFAULT_REGION={your region}
export AWS_SECRET_ACCESS_KEY={your secret key}
export IS_ES_ON_AWS=true
```

Test lambda invocation:

```bash
make invoke
```

Another way to accomplish the same is by calling the AWS cli for lambda invokes:

### Invoke Lambda function against AWS

Configure environment:

```bash
make pyshell-standalone
vim .env # uncomment remote section, comment local section

export AWS_ACCESS_KEY_ID={your key}
export AWS_DEFAULT_REGION={your region}
export AWS_SECRET_ACCESS_KEY={your secret key}
```

Test lambda invocation:

```bash
make invoke
```

Another way to accomplish the same is by calling the AWS cli for lambda invokes:

#### Request

```bash
ENV=staging make invoke-local
```

#### Response

```json
{
    "StatusCode": 200,
    "ExecutedVersion": "$LATEST"
}
```

The output file generated should contain a `null` string, even if the execution is successful or not. So a good approach would be to browse the [CloudWatch logs for the lambda function](https://us-west-1.console.aws.amazon.com/cloudwatch/home?region=us-west-1#logsV2:log-groups/log-group/$252Faws$252Flambda$252Fsl-editorial-travel-etl-wp-to-es-sandbox) (sandbox) and read the log responses.

## Monitoring

This Lambda has a [New Relic alert policy](https://one.newrelic.com/launcher/dashboards.launcher?platform[filters]=Iihkb21haW4gPSAnQVBNJyBBTkQgdHlwZSA9ICdBUFBMSUNBVElPTicpIg==&platform[accountId]=356245&platform[$isFallbackTimeRange]=false&pane=eyJuZXJkbGV0SWQiOiJkYXNoYm9hcmRzLmRldGFpbCIsInVzZURlZmF1bHRUaW1lUmFuZ2UiOmZhbHNlLCJzZWxlY3RlZFBhZ2UiOiJNelUyTWpRMWZGWkpXbnhFUVZOSVFrOUJVa1I4TWpVMU1UUXlPUSIsImlzVGVtcGxhdGVFbXB0eSI6ZmFsc2UsImVudGl0eUd1aWQiOiJNelUyTWpRMWZGWkpXbnhFUVZOSVFrOUJVa1I4WkdFNk16WTVNamN3In0=&state=03e3f219-b20c-4646-3424-8ec92e9a56c4) that monitors the invocation error count.

### Configuration

The alert should be configured at a threshold that indicates a diminished functionality state. Developers should proactively update this threshold as error patterns change over the life of the Lambda function.

Because of reporting delays from AWS to New Relic, the `aggregation window` value should be set to 20 minutes.
