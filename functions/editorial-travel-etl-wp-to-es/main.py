# -*- coding: utf-8 -*-
""" Root service handler module for AWS Lambda function """

import json
import logging

from lib import config, travel_page_etl

logging.basicConfig(level=config.LOG_LEVEL)
logger = logging.getLogger()
logger.setLevel(config.LOG_LEVEL)


def main(event, context):

    logger.info(f'Event: {json.dumps(event, indent=4, sort_keys=True)}')

    return travel_page_etl.handler(event, context)
