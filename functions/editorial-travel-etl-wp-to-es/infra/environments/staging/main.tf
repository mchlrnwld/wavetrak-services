provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "editorial-travel-etl-wp-to-es-function/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

