provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "editorial-travel-etl-wp-to-es-function/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "editorial-travel-etl-wp-to-es-function" {
  source = "../../"

  environment = "prod"
}
