"""WordPress client module"""

import copy
import html
import json
import logging
from typing import Any, Dict

import requests  # type: ignore
from lib.config import WORDPRESS_API

logger = logging.getLogger(__name__)

POST_TPL: Dict[str, Any] = {
    'id': '',
    'articleId': '',
    'createdAt': '',
    'updatedAt': '',
    'sortDate': '',
    'content': {'title': '', 'subtitle': '', 'summary': '', 'body': ''},
    'media': {
        'type': '',
        'thumbnail': '',
        'medium': '',
        'large': '',
        'full': '',
    },
    'relatedPosts': [],
}


def _build_related_posts(wp_post):
    ids = []
    if 'acf' in wp_post and 'select_posts' in wp_post['acf']:
        ids = [post["ID"] for post in wp_post['acf']['select_posts']]
    return ids


def _build_post_id(wp_post):
    _id = None
    dynLocationtravel = wp_post['acf']['dynlocationtravel']
    if dynLocationtravel:
        _id = wp_post['acf']['dynlocationtravel']
    return _id


def _build_post_content(wp_post):
    return {
        'title': html.unescape(wp_post['title']['rendered']),
        'subtitle': html.unescape(wp_post['acf']['subtitle']),
        'summary': html.unescape(wp_post['acf']['summary']),
        'body': html.unescape(wp_post['content']['rendered']),
    }


def build_post(post_id, media):
    """Get post data from WP and assemble into ES doc format"""
    wp_post = get_wp_data(post_id, 'travel_page')

    post = copy.deepcopy(POST_TPL)

    post['articleId'] = post_id
    post['targetTaxonomy'] = _build_post_id(wp_post)
    post['createdAt'] = wp_post['date_gmt']
    post['updatedAt'] = wp_post['modified_gmt']
    post['sortDate'] = wp_post['date_gmt']
    post['content'] = _build_post_content(wp_post)
    post['media'] = media
    post['relatedPosts'] = _build_related_posts(wp_post)

    logger.debug('ES post: %s' % json.dumps(post, sort_keys=True, indent=2))

    return post


def get_wp_data(id, type):
    """Get data from WordPress matching type and id"""
    url = "%s/%s/%d" % (WORDPRESS_API, type, id)
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()
    return data


def get_wp_data_with_filter(id, type):
    """Get filtered data from WordPress matching type and id"""
    url = "%s/%s?post=%d" % (WORDPRESS_API, type, id)
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()
    return data
