"""Travel Page ETL handler module."""

import json
import logging

from lib.elasticsearch_client import delete_post, update_post
from lib.wordpress_client import build_post

logger = logging.getLogger(__name__)


def handler(event, context):
    """travel_page_etl handler updates or deletes posts in Elasticsearch"""
    logger.info(event)

    try:
        message = json.loads(event['Records'][0]['Sns']['Message'])
        taxId = "taxId" in message

        if taxId:
            if message['action'] == 'DELETE_POST':
                taxId = message['taxId']
                if taxId is not None:
                    delete_post(taxId)
            elif message['action'] in ('CREATE_POST', 'UPDATE_POST'):
                post = build_post(message['id'], message['media'])
                update_post(post)
        else:
            return None

    except Exception as e:  # type: ignore # noqa F841
        logger.error('Invalid query parameters', exc_info=True)
        raise
