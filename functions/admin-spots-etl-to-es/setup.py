from __future__ import absolute_import

from distutils.core import setup

setup(
    name='lib',
    version='1.0',
    packages=['lib', 'lib.routes', 'lib.handlers'],
)
