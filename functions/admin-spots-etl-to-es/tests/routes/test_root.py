from __future__ import absolute_import

import json
from os.path import dirname, join

from lib.routes import root
from pytest import fail, fixture

fixtures_path = join(dirname(__file__), '..', 'fixtures')


@fixture
def sns_event_record():
    sns_event_record_path = join(fixtures_path, 'sns_event_record.json')
    with open(sns_event_record_path, 'r') as sns_event_record_json:
        return json.load(sns_event_record_json)


@fixture
def context():
    return {}


# class TestHandler:
#
#     def test_returns_nothing(self, sns_event_record, context):
#         result = root.handler(sns_event_record, context)
#         assert result is None


class TestHandler:
    def test_type_error_for_bad_params(self, context):
        try:
            root.handler('', context)
        except TypeError:
            pass
        else:
            # TODO: confim this update is correct
            # original: self.fail('ExpectedException not raised')
            fail('ExpectedException not raised')
