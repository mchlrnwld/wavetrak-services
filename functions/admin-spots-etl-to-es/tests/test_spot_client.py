import json
import logging
from html.parser import HTMLParser
from os.path import dirname, join

import mock
from lib import spot_client
from pytest import fixture

html_parser = HTMLParser()

fixtures_path = join(dirname(__file__), 'fixtures')
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def get_spot_data(spot_id):
    spot_path = join(fixtures_path, 'spot.json')
    with open(spot_path, 'r') as spot_json:
        return json.load(spot_json)


@fixture
def get_spot_to_index():
    spot_to_index_path = join(fixtures_path, 'spot_to_index.json')
    with open(spot_to_index_path, 'r') as spot_to_index_json:
        return json.load(spot_to_index_json)


@fixture
def users_1():
    users_1_path = join(fixtures_path, 'users_1.json')
    with open(users_1_path, 'r') as users_1_json:
        return json.load(users_1_json)


@fixture
def build_spot_taxonomy():
    spot_taxonomy_path = join(fixtures_path, 'spot_taxonomy.json')
    with open(spot_taxonomy_path, 'r') as spot_taxonomy_json:
        return json.load(spot_taxonomy_json)


def get_delete_docs_without_spots():
    return True


def get_insert_spot_taxonomy_docs():
    return True


class TestHandler:
    @mock.patch('lib.spot_client.get_spot_data', get_spot_data)
    @mock.patch(
        'lib.taxonomy_client.delete_docs_without_spots',
        get_delete_docs_without_spots,
    )
    @mock.patch(
        'lib.taxonomy_client.insert_spot_taxonomy_docs',
        get_insert_spot_taxonomy_docs,
    )
    def test_spot_update(self, build_spot_taxonomy, get_spot_to_index):
        spot_taxonomy_data = build_spot_taxonomy
        spot_to_index = get_spot_to_index
        result = spot_client.build_spot(
            '58f7ed51dadb30820bb387d8', spot_taxonomy_data
        )
        assert result['doc']['name'] == spot_to_index['name']
        assert result['doc']['name_standard'] == spot_to_index['name']
        assert result['doc']['href'] == spot_to_index['href']
        assert (
            result['doc']['location']['lat']
            == spot_to_index['location']['lat']
        )
        assert (
            result['doc']['location']['lon']
            == spot_to_index['location']['lon']
        )
        assert result['doc']['href'] == spot_to_index['href']
        assert result['doc']['cams'] == spot_to_index['cams']
        assert result['doc']['thumbnail'] == spot_to_index['thumbnail']
        assert result['doc']['thumbnail_300'] == spot_to_index['thumbnail_300']
        assert result['doc']['thumbnail_640'] == spot_to_index['thumbnail_640']
        assert (
            result['doc']['thumbnail_1500'] == spot_to_index['thumbnail_1500']
        )
        assert (
            result['doc']['thumbnail_3000'] == spot_to_index['thumbnail_3000']
        )
        assert result['doc']['breadCrumbs'] == spot_to_index['breadCrumbs']
