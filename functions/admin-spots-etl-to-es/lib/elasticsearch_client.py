"""Elasticsearch client module"""
# -*- coding: utf-8 -*-

from __future__ import absolute_import

import json
import logging

import boto3
from elasticsearch import (  # type: ignore
    Elasticsearch,
    RequestsHttpConnection,
    TransportError,
    compat,
    exceptions,
    serializer,
)
from lib.config import (
    ELASTIC_SEARCH_CONNECTION_STRING,
    ES_HTTP_AUTH,
    ES_INDEX,
    ES_TYPE,
    IS_ES_ON_AWS,
)
from requests_aws4auth import AWS4Auth  # type: ignore

logger = logging.getLogger(__name__)


def init_elasticsearch():
    """Initialize Elasticsearch client"""
    if IS_ES_ON_AWS:
        session = boto3.session.Session()
        credentials = session.get_credentials()

        awsauth = AWS4Auth(
            credentials.access_key,
            credentials.secret_key,
            session.region_name,
            'es',
            session_token=credentials.token,
        )

        es = Elasticsearch(
            [ELASTIC_SEARCH_CONNECTION_STRING],
            http_auth=awsauth,
            verify_certs=True,
            connection_class=RequestsHttpConnection,
            serializer=JSONSerializerPython2(),
        )
    else:
        es = Elasticsearch(
            [ELASTIC_SEARCH_CONNECTION_STRING],
            http_auth=ES_HTTP_AUTH,
            connection_class=RequestsHttpConnection,
            serializer=JSONSerializerPython2(),
        )

    return es


def get_doc(doc_id, es_index=ES_INDEX, es_type=ES_INDEX):
    """Get doc in Elasticsearch"""

    try:
        es = init_elasticsearch()
        return es.get(index=es_index, doc_type=es_type, id=doc_id)
    except TransportError as te:
        if te.status_code == 404:
            return te.info
        else:
            raise


def update_doc(doc, es_index=ES_INDEX, es_type=ES_TYPE):
    """Create/update doc in Elasticsearch"""

    # Allow ES exceptions to be raised uncaught
    es = init_elasticsearch()
    update_body = {'doc': doc['doc'], 'upsert': doc['doc']}
    return es.update(
        index=es_index, doc_type=es_type, id=doc['_id'], body=update_body
    )


def delete_doc(doc_id, es_index=ES_INDEX, es_type=ES_TYPE):
    """Delete doc in Elasticsearch"""

    try:
        es = init_elasticsearch()
        return es.delete(index=es_index, doc_type=es_type, id=doc_id)
    except TransportError as te:
        if te.status_code == 404:
            return
        else:
            raise


def delete_multi_by_ids(query, es_index=ES_INDEX, es_type=ES_TYPE):
    """Delete doc in Elasticsearch"""

    try:
        es = init_elasticsearch()
        return es.delete_by_query(index=es_index, doc_type=es_type, body=query)
    except TransportError as te:
        if te.status_code == 404:
            return
        else:
            raise


# Include this class to override the default ES JSON serializer
# From https://github.com/elastic/elasticsearch-py/issues/374
class JSONSerializerPython2(serializer.JSONSerializer):
    """Override elasticsearch library serializer to ensure it encodes utf
    characters during json dump. See original at:
    https://github.com/elastic/elasticsearch-py/blob/master/elasticsearch/seri\
    alizer.py#L42
    A description of how ensure_ascii encodes unicode characters to ensure
    they can be sent across the wire as ascii can be found here:
    https://docs.python.org/2/library/json.html#basic-usage
    """

    def dumps(self, data):
        # don't serialize strings
        if isinstance(data, compat.string_types):
            return data
        try:
            return json.dumps(data, default=self.default, ensure_ascii=True)
        except (ValueError, TypeError) as e:
            raise exceptions.SerializationError(data, e)
