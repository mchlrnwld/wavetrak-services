"""
Root service handler module for AWS Lambda function. 'METHOD_HANDLERS'
constant dict is used to map route requests to correct handler.
"""

from __future__ import absolute_import

import json
import logging

from lib.handlers import spot_etl

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def handler(event, context):
    """Root path handler."""

    logger.info('Event: %s' % json.dumps(event, indent=4, sort_keys=True))

    return spot_etl.handler(event, context)
