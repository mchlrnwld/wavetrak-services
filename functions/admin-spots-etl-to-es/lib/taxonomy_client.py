"""Spot client module"""
from __future__ import absolute_import

import copy
import json
import logging
from typing import Any, Dict

import requests
from lib.config import SPOT_API
from lib.elasticsearch_client import delete_multi_by_ids, update_doc
from lib.spot_client import SpotMissingFromSpotApiException

logger = logging.getLogger(__name__)

taxonomy_TPL: Dict[str, Any] = {
    '_id': '',
    'doc': {
        'name': '',
        'name_standard': '',
        'location': {'lat': 0, 'lon': 0},
        'href': '',
    },
}

subregion_TPL: Dict[str, Any] = {
    '_id': '',
    'doc': {'name': '', 'name_standard': '', 'location': None, 'href': ''},
}


def build_taxonomy(tax_data, tax_type):
    """Assemble taxonomy data into ES doc format"""

    if tax_type == 'subregion':
        taxonomy = copy.deepcopy(subregion_TPL)
    else:
        taxonomy = copy.deepcopy(taxonomy_TPL)

    taxonomy['_id'] = tax_data['_id']
    taxonomy['doc']['name'] = tax_data['name']
    taxonomy['doc']['name_standard'] = tax_data['name']
    if 'links' in tax_data['associated']:
        for link in tax_data['associated']['links']:
            if link is not None and 'href' in link and 'key' in link:
                if link['key'] == 'www' and tax_type != 'travel':
                    taxonomy['doc']['href'] = link['href']
                if link['key'] == 'travel' and tax_type == 'travel':
                    taxonomy['doc']['href'] = link['href']

    if (
        'location' in tax_data
        and 'coordinates' in tax_data['location']
        and tax_data['type'] == 'geoname'
    ):
        taxonomy['doc']['location']['lat'] = tax_data['location'][
            'coordinates'
        ][1]
        taxonomy['doc']['location']['lon'] = tax_data['location'][
            'coordinates'
        ][0]

    if 'enumeratedPath' in tax_data:
        taxonomy['doc']['breadCrumbs'] = tax_data['enumeratedPath'].split(',')[
            3:-1
        ]

    logger.debug(
        'ES taxonomy: %s' % json.dumps(taxonomy, sort_keys=True, indent=2)
    )

    return taxonomy


def delete_docs_without_spots():
    docs_to_delete = get_taxonomy_no_spots()

    docs_subregion = []
    docs_geoname = []
    if 'data' in docs_to_delete and len(docs_to_delete['data']):
        for doc in docs_to_delete['data']:
            if doc['type'] == 'subregion':
                docs_subregion.append(str(doc['_id']))
            if doc['type'] == 'geoname':
                docs_geoname.append(str(doc['_id']))

    if docs_subregion:
        query_subregion = json.JSONEncoder().encode(
            {"query": {"ids": {"values": docs_subregion}}}
        )
        delete_multi_by_ids(query_subregion, 'taxonomy', 'subregion')
        logger.debug('subregion delete_by_query is: %s ' % query_subregion)

    if docs_geoname:
        query_geoname = json.JSONEncoder().encode(
            {"query": {"ids": {"values": docs_geoname}}}
        )
        delete_multi_by_ids(query_geoname, 'taxonomy', 'geoname')
        logger.debug('geoname delete_by_query is: %s ' % query_geoname)


def insert_spot_taxonomy_docs(spot_taxonomy):
    for tax in spot_taxonomy['in']:
        if tax['depth'] <= 4 and 'links' in tax['associated']:
            taxonomy = build_taxonomy(tax, tax['type'])
            if taxonomy['doc']['href']:
                update_doc(taxonomy, 'taxonomy', tax['type'])
                if tax['type'] == 'geoname':
                    taxonomy_travel = build_taxonomy(tax, 'travel')
                    update_doc(taxonomy_travel, 'taxonomy', 'travel')


def get_spot_taxonomy(id):
    """Get taxonomy data from spot-api matching id"""
    try:
        url = "%s/taxonomy?id=%s&type=spot" % (SPOT_API, id)
        response = requests.get(url)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.HTTPError as he:
        if he.response.status_code == 404:
            raise SpotMissingFromSpotApiException(id)
        elif he.response.status_code == 400:
            raise SpotMissingFromSpotApiException(id)
        else:
            raise


def get_taxonomy_no_spots():
    """Get taxonomy data from spot-api for documents with no spots"""
    try:
        url = "%s/taxonomy/nospots" % (SPOT_API)
        response = requests.get(url)
        response.raise_for_status()
        return response.json()
    except Exception:
        raise
