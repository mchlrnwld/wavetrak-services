"""Spot client module"""
from __future__ import absolute_import

import copy
import json
import logging
from typing import Any, Dict

import requests
from lib.config import SPOT_API

logger = logging.getLogger(__name__)

spot_TPL: Dict[str, Any] = {
    '_id': '',
    'doc': {
        'name': '',
        'name_standard': '',
        'location': {'lat': 0, 'lon': 0},
        'href': '',
        'cams': [],
        'thumbnail': '',
        'thumbnail_300': '',
        'thumbnail_640': '',
        'thumbnail_1500': '',
        'thumbnail_3000': '',
        'breadCrumbs': [],
    },
}


class SpotInDraftModeException(Exception):
    def __init___(self, error_args):
        Exception.__init__(self, "Spot in draft mode {0}".format(error_args))
        self.error_args = error_args


class SpotInDeletedStatusException(Exception):
    def __init___(self, error_args):
        Exception.__init__(
            self, "Spot in deleted status {0}".format(error_args)
        )
        self.error_args = error_args


class SpotMissingFromSpotApiException(Exception):
    def __init___(self, error_args):
        Exception.__init__(
            self, "Spot Missing From Spot Api {0}".format(error_args)
        )
        self.error_args = error_args


# PARTIAL spot, must use /spots/spot/${id}/_update to avoid overwriting spot
def build_spot(spot_id, spot_taxonomy):
    """Get spot data from WP and assemble into ES doc format"""
    spot_data = get_spot_data(spot_id)
    logger.debug(
        'Spot API spot: %s' % json.dumps(spot_data, sort_keys=True, indent=2)
    )
    if spot_data['status'] == 'DRAFT':
        raise SpotInDraftModeException(spot_id)
    if spot_data['status'] == 'DELETED':
        raise SpotInDeletedStatusException(spot_id)
    spot = copy.deepcopy(spot_TPL)
    spot['_id'] = spot_data['_id']
    spot['doc']['cams'] = spot_data['cams']
    spot['doc']['name'] = spot_data['name']
    spot['doc']['name_standard'] = spot_data['name']
    spot['doc']['cams'] = spot_data['cams']
    if 'location' in spot_data and 'coordinates' in spot_data['location']:
        spot['doc']['location']['lat'] = spot_data['location']['coordinates'][
            1
        ]
        spot['doc']['location']['lon'] = spot_data['location']['coordinates'][
            0
        ]
    if 'thumbnail' in spot_data:
        if 'original' in spot_data['thumbnail']:
            spot['doc']['thumbnail'] = spot_data['thumbnail']['original']
        if '300' in spot_data['thumbnail']:
            spot['doc']['thumbnail_300'] = spot_data['thumbnail']['300']
        if '640' in spot_data['thumbnail']:
            spot['doc']['thumbnail_640'] = spot_data['thumbnail']['640']
        if '1500' in spot_data['thumbnail']:
            spot['doc']['thumbnail_1500'] = spot_data['thumbnail']['1500']
        if '3000' in spot_data['thumbnail']:
            spot['doc']['thumbnail_3000'] = spot_data['thumbnail']['3000']

    for www_link in [
        x for x in spot_taxonomy['associated']['links'] if x['key'] == 'www'
    ]:
        spot['doc']['href'] = www_link['href']

    for r in [
        x
        for x in spot_taxonomy['in']
        if x['category'] == 'geonames' and x['depth'] == 1
    ]:
        spot['doc']['breadCrumbs'] = r['enumeratedPath'].split(',')[3:-1]

    logger.debug('ES spot: %s' % json.dumps(spot, sort_keys=True, indent=2))

    return spot


def get_spot_data(id):
    """Get spot data from spot-api matching id"""
    try:
        url = "%s/admin/spots/%s" % (SPOT_API, id)
        response = requests.get(url)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.HTTPError as he:
        if he.response.status_code == 404:
            raise SpotMissingFromSpotApiException(id)
        else:
            raise
