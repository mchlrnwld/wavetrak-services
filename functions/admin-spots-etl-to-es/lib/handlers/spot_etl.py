"""Spot ETL handler module."""

from __future__ import absolute_import, print_function

import json
import logging
import sys

from lib.elasticsearch_client import delete_doc, update_doc
from lib.spot_client import (
    SpotInDeletedStatusException,
    SpotInDraftModeException,
    SpotMissingFromSpotApiException,
    build_spot,
)
from lib.taxonomy_client import (
    delete_docs_without_spots,
    get_spot_taxonomy,
    insert_spot_taxonomy_docs,
)

logger = logging.getLogger(__name__)


def handler(event, context):
    """feed_etl handler updates or deletes posts in Elasticsearch"""
    logger.info(event)
    message = json.loads(event['Records'][0]['Sns']['Message'])
    print(message)
    try:
        delete_docs_without_spots()
        spot_taxonomy = get_spot_taxonomy(message['spotId'])
        spot = build_spot(message['spotId'], spot_taxonomy)
        logger.debug(
            'Spot API taxonomy: %s'
            % json.dumps(spot_taxonomy, sort_keys=True, indent=2)
        )
        insert_spot_taxonomy_docs(spot_taxonomy)
        return update_doc(spot)
    except SpotMissingFromSpotApiException:
        return delete_doc(message['spotId'])
    except SpotInDeletedStatusException:
        return delete_doc(message['spotId'])
    except SpotInDraftModeException:
        return delete_doc(message['spotId'])
    except Exception:
        print(("Unexpected error:", sys.exc_info()[0]))
        raise
