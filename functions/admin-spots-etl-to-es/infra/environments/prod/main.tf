provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "admin-spots-etl-to-es/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "admin-spots-etl-to-es" {
  source = "../../"

  environment = "prod"
}
