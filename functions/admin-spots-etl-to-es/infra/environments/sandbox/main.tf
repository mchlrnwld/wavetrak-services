provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "admin-spots-etl-to-es/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}
