provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "admin-spots-etl-to-es/staging/terraform.tfstate"
    region = "us-west-1"
  }
}
