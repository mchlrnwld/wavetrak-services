import logging
from contextlib import ExitStack

import lib.config as config
from lib.mysql import MySQLClient
from lib.ssh import SSH

logger = logging.getLogger('airflow-scheduler-monitor')


def main(event, context):
    logger.setLevel(logging.INFO)
    logger.info(f'Logging event: {event}')
    logger.info(f'Logging context: {context}')

    with ExitStack() as stack:
        mysql_client = stack.enter_context(
            MySQLClient(
                config.MYSQL_HOST,
                config.MYSQL_USER,
                config.MYSQL_PASSWORD,
                config.MYSQL_DATABASE,
                config.MYSQL_PORT,
            )
        )
        delayed_dags = mysql_client.check_for_delayed_dags()
        if delayed_dags:
            ssh = stack.enter_context(
                SSH(
                    config.AIRFLOW_HOSTNAME,
                    config.AIRFLOW_USERNAME,
                    config.AIRFLOW_SSH_KEY,
                )
            )
            delayed_dags_with_tasks = [
                (dag, execution_date, ssh.get_first_task_in_dag(dag))
                for dag, execution_date in delayed_dags
            ]
            for dag, execution_date, task in delayed_dags_with_tasks:
                ssh.force_task_to_run(dag, execution_date, task)
    return
