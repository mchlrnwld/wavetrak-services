# Airflow Scheduler Monitor Lambda

This Lambda function queries the Airflow RDS cluster to see if there are any DAGs that are delayed. This is to monitor a known issue where DAGs in the 'running' state will get delayed and no task instances will be placed. If there are any DAGs that are delayed, a PagerDuty alert will be sent with the name of which DAGs are delayed. The Lambda is invoked every 5 minutes via a Cloudwatch event.

## Serverless Framework

This Lambda function introduces the Serverless Framework to package, deploy, and run the lambda function.

### Installing

To install Serverless Framework, you must have Node version `>=10` (the framework is written in Node) and `npm` installed. Once both are installed, simply run the following command:
```sh
npm install
```

This will install `Serverless` and all required plugins.

### serverless.yml

This is the file that Serverless Framework uses to create and deploy a lambda function to any given AWS account. Each main section of the `serverless.yml` will be explained:

#### service

This describes the service associated with the Lambda function, for example: `airflow`. The service name will be used to create the Cloudformation template that then creates the Lambda function.

#### plugins

The section is where all 3rd party Serverless plugins are declared. For example, the `serverless-python-requirements` allows you to easily package and deploy a Python Lambda with 3rd party dependencies. 

#### custom

This section is where custom variables are declared. For example, the VPC configuration must be different depending on the environment the Lambda function is deployed in. The allow that, the `custom` section allows to declare a variable such as `vpcSettings` to ensure that the correct VPC configuration is used in each environment.

#### provider

This section is where all AWS related information is declared. For example the AWS region, the AWS account associated with the Lambda, and any IAM role permissions.

##### stage
This is the value `Serverless` uses to describe to environment the Lambda is deployed in. This is **not** the same as the `profile` which is explained in the next section. With Serverless, there can be multiple stages per AWS Profile.

Since we don't deploy to multiple environments in a single AWS profile, the `serverless.yml` is configured so that depending on the `stage` passed in via `-s`, it deploys to the appropriate AWS profile.

##### profile
This is the value `Serverless` uses to deploy to AWS. It does so by looking up AWS credentials in `~/.aws/credentials`. For example if you set the `profile` to `sandbox`, it will look for `sandbox` credentials in `~/.aws/credentials`

#### functions

This section is where all Lambda functions are declared. Do note that many Lambda functions can be declared here, not just one. You can specify the name of the function, the handler to use, any associated infrastructure (Cloudwatch, SNS, etc) and more.

### Deploying

Included in the `Makefile` is `make ENV=env-name deploy`, which allows you to easily deploy the Airflow Monitor Scheduler Lambda to the AWS account and environment of choice. The desired environment must be passed in. For example:
```sh
make ENV=sandbox deploy
```
When deploying, Serverless will look for the AWS credentials associated with the environment. For example, if you want to deploy to `sandbox`, using `make ENV=sandbox deploy`, it will look for the AWS credentials associated with `sandbox` in `~/.aws/credentials`.

**NOTE:** Deploying the Lambda locally is just temporary due the fact that our CI/CD practices are currently in flux. This is **not** going to be a standard practice moving forward.

#### Python Requirements

Before deploying, all 3rd party Python dependencies required for runtime must be manually added to `requirements.txt`. The `serverless-python-requirements` plugin references `requirements.txt` to install and package Python dependencies. It is **not** possible to use Conda with this plugin. However, since Conda is still used for local development, an `environment.yml` is provided.

All dependencies required for runtime are installed via pip in `environment.yml`. The purpose of installing via `pip` for runtime dependencies is to ensure that the `serverless-python-requirements` can find the same version that is used for local development.

**NOTE:** Manually adding dependencies to `requirements.txt` is temporary practice while a more automated solution can be found.

### Running Locally

The run the Lambda locally and simulate an invocation, run the following command:
```sh
make ENV=env-name invoke-local
```
This calls the `serverless invoke-local` command which simulates a Lambda invocation.

Before running this command, it is reccomended that you create and activate the Conda environment listed in `environment.yml`. The Conda environment can be created by executing the following command:
```sh
conda create -f environment.yml
```
Then the Conda environment can be activated with the command:
```sh
conda activate airflow-scheduler-monitor
```

## Monitoring

This Lambda has a [New Relic alert policy](https://one.newrelic.com/launcher/nrai.launcher?platform[accountId]=356245&pane=eyJuZXJkbGV0SWQiOiJhbGVydGluZy11aS1jbGFzc2ljLnBvbGljaWVzIiwibmF2IjoiUG9saWNpZXMiLCJzZWxlY3RlZEZpZWxkIjoidGhyZXNob2xkcyIsInBvbGljeUlkIjoiMzI4NTQzIiwiY29uZGl0aW9uSWQiOiIxNTY0NzUyMiJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucmFpLm5hdmlnYXRpb24tYmFyIiwibmF2IjoiUG9saWNpZXMifQ) that
monitors the invocation error count.

### Configuration

Because of the impactful downstream effects of missed airflow runs, the thresholds in this alert policy should be set as conservatively as possible. 

**Note:** Because of reporting delays from AWS to New Relic, the `aggregation window` value should be set to 20 minutes.

### Triggering the Alert

If you need to trigger the alert for testing purposes, the fastest way is to temporarily delete one of the mission-critical Lambda environment variables (i.e. `MYSQL_DATABASE`). **Please do this with caution, and communicate your actions to the code owners.** This can be accomplished in the AWS Lambda console where the `Environment variables` are configured.

Then you can use the AWS console to configure a Lambda test event that contains any valid json object. Invoke that event repeatedly using the "Test" button until you reach the error threshold configured in the policy. After the offset window has elapsed, New Relic will trigger an alert.

Be sure to re-add the environment variable(s) after you've completed testing.
