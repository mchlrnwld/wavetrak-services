import logging
from typing import List, Tuple

import mysql.connector  # type: ignore

logger = logging.getLogger('airflow-scheduler-monitor')


class MySQLClient:
    """
    MYSQL client to access the Airflow RDS cluster.
    Methods are provided to safely create and close
    a DB connection.

    Arguments:
        host: The hostname of the database.
        user: The user of the database.
        password: The password associated with the user.
        database: The name of the database to connect to.
        port: The port to use to connect to the database.

    Attributes:
        db: MySQL database instance/
        cursor: Interface to execute queries.
    """

    def __init__(
        self, host: str, user: str, password: str, database: str, port: int
    ):
        self.db = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=database,
            port=port,
            ssl_disabled=True,
        )
        self.cursor = self.db.cursor()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.db.close()

    def check_for_delayed_dags(self) -> List[Tuple[str, str]]:
        """
        Checks DAGs that are delayed. This occurs when
        DAG runs in the RUNNING state do not have an associated
        entry in the task_instance table for more than 10 minutes.

        Returns:
            A Set of DAGs that are delayed, if any.
        """
        # Because there is a delay of up to 20 minutes between when
        # DAG runs are created and when task instances are placed,
        # check for DAG runs that don't have a task instance for more than
        # 20 minutes.
        query = '''
        SELECT dag_id, MIN(execution_date) FROM dag_run
        WHERE state = %s
        AND dag_id IN (
            SELECT dag_id FROM dag
            WHERE is_active = %s
            AND is_paused = %s)
        AND NOT EXISTS (
            SELECT null FROM task_instance
            WHERE task_instance.dag_id = dag_run.dag_id
            AND task_instance.execution_date = dag_run.execution_date)
        AND TIMESTAMPDIFF(MINUTE, start_date, UTC_TIMESTAMP) > %s
        GROUP BY dag_id
        '''

        self.cursor.execute(query, ('running', 1, 0, 20))
        delayed_dags = [
            (dag_id, execution_date.isoformat())
            for dag_id, execution_date in self.cursor.fetchall()
        ]

        logger.info(f'The following DAGs are delayed: {delayed_dags}')

        return delayed_dags
