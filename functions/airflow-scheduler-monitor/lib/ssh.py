import logging
from io import StringIO

from paramiko import AutoAddPolicy, RSAKey, SSHClient  # type: ignore

logger = logging.getLogger('airflow-scheduler-monitor')


class AirflowException(Exception):
    pass


class SSH:
    """
    SSH client to access the Airflow EC2 Instance.
    Methods are provided to safely create and close
    an SSH connection.

    Arguments:
        hostname: The hostname of the Airflow instance.
        username: Username used to connect to the Airflow instance.
        ssh_key: The SSH key to use to connect to the Airflow instance.
                 Provided as string.

    Attributes:
        ssh: SSH interface to execute commands on the Airflow instance.
    """

    def __init__(self, hostname: str, username: str, ssh_key: str):
        self.ssh = SSHClient()
        self.ssh.set_missing_host_key_policy(AutoAddPolicy)
        self.ssh.connect(
            hostname,
            username=username,
            pkey=RSAKey.from_private_key(StringIO(ssh_key)),
        )

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.ssh.close()

    def get_first_task_in_dag(self, dag: str):
        """
        Given a DAG name, execute the 'airflow list_tasks'
        command to find the first task in the DAG.

        Returns:
            The first task in the given DAG.
        """
        _, stdout, stderr = self.ssh.exec_command(
            f"airflow list_tasks -t {dag} |"
            f"tail -n1 | awk '{{print $NF}}' | sed 's/>$//'"
        )
        exit_status = (
            stdout.channel.recv_exit_status()
        )  # Wait for command to finish.
        if exit_status:
            error = stderr.read().decode("utf-8")
            raise AirflowException(error)

        first_task = stdout.read().decode("utf-8").strip()
        logger.info(f'Got first task: {first_task} for DAG: {dag}')

        return first_task

    def force_task_to_run(self, dag: str, execution_date: str, task: str):
        """
        Forces an Airflow task to run using the 'airflow run' command.
        """
        _, stdout, stderr = self.ssh.exec_command(
            f'airflow run {dag} {task} {execution_date} -i -f'
        )
        exit_status = (
            stdout.channel.recv_exit_status()
        )  # Wait for command to finish.
        if exit_status:
            error = stderr.read().decode("utf-8")
            raise AirflowException(error)
        logger.info(
            f'Forced task: {task} to run for DAG: {dag} and execution_date: {execution_date}'
        )
