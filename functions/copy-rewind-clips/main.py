import json
import logging
import os
import re

import boto3
from typing import Dict

_logger = logging.getLogger()
_logger.setLevel(logging.INFO)

_dest_buckets = os.environ['DESTINATION_BUCKETS'].split(',')
_valid_aliases = os.environ['VALID_ALIASES'].split(',')

STREAM_REGEX = re.compile(r'/([A-Za-z0-9-]+)')

def should_copy(key: str) -> bool:
    """
    Take the alias from the object-key of the rewind to
    check if this rewind is coming from a camera we want
    to copy rewinds for.

    Args:
        key: A string representing the object-key of the rewind clip.

    Returns:
        True if we should copy this rewind, False otherwise.
    """
    stream_name_search = STREAM_REGEX.search(key)

    if stream_name_search:
        alias = stream_name_search.group(1)

        return alias in _valid_aliases

    _logger.info('Could not parse alias from object-key')

    return False


def clone_object(bucket: str, object_key: str, dest_bucket: str) -> None:
    """
    Copy a rewind from bucket/object-key to the destination buckets listed in
    the environment variable DESTINATION_BUCKETS.

    Args:
        bucket: A string representing the source bucket of the rewind.
        object_key: A string representing the object-key of the rewind.
        dest_bucket: A string representing the destination bucket of the rewind.

    Returns:
        None
    """
    _logger.info(f'Copy: {bucket}/{object_key} to {dest_bucket}/{object_key}')

    client = boto3.client('s3')

    extra_args = {
        'ACL': 'public-read'
    }

    client.copy({
        'Bucket': bucket,
        'Key': object_key,
    }, dest_bucket, object_key, extra_args)


def handler(event: Dict, context: Dict) -> bool:
    """
    Get the bucket and object-key for the incoming rewind.
    Copy it over to the destination bucket if it is approved.

    Args:
        event: Dictionary representing the payload from a SNS message.
        context: Dictionary representing the context from a lambda trigger.

    Returns:
        True if the lambda succeeded.
    """
    # Try/except clause is here to restrict lambda from retrying on errors
    try:
        message = json.loads(event['Records'][0]['Sns']['Message'])
        bucket = message['Records'][0]['s3']['bucket']['name']
        object_key = message['Records'][0]['s3']['object']['key']

        _logger.info(f'Received: {object_key}')

        approved_copy = should_copy(object_key)

        if not approved_copy:
            _logger.info(f'Skipping object, not a camera we care about.')

            return True

        _logger.info(f'Approved rewind to copy.')

        for dest_bucket in _dest_buckets:
            # Move rewind file to new bucket.
            clone_object(bucket, object_key, dest_bucket)

        return True

    except Exception as e:
        _logger.info(f'There was an error: {e}')

        return False
