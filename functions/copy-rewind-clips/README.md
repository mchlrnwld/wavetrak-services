# Copy Rewind Clips Trigger

This lambda function is triggered when a new clip gets placed in the
`sl-live-cam-archive-sandbox|staging|prod` S3 bucket. The lambda will:
1. Check if the rewind is from a camera we want to copy rewinds for, specified as `SUPPORTED_CAM` in `environment_vars.yml`
2. If the rewind is coming from a camera we want to copy rewinds for, then it will copy the rewind to the bucket/s specified as `DESTINATION_BUCKETS` in `environment_vars.yml` 

## Context

The smart-highlights/crowd-counting pipeline is triggered when a rewind gets placed in the S3 bucket `sl-live-cam-archive-prod`. To mimic the pipeline in a development
environment, we must also have rewinds landing in the `sl-live-cam-archive-sandbox|staging` buckets. The lambda will do this by copying a select amount of rewinds from
the production environment into the development environment.

## Installation

To install all the necessary dependencies for deployment and testing run:

`npm install`

where your `npm` version is `>=10`

## Deploying
Simply run:

`make ENV=sandbox|staging|prod deploy`

Make sure to choose one of **sandbox**, **staging**, or **prod**.
Depending on the environment you choose, the deployment will update
the following:

| Command                   | Account       | Lambda name                            |
|---------------------------|---------------|----------------------------------------|
| `make ENV=prod deploy`    | surfline-prod | wt-copy-rewind-clips-**prod**          |
| `make ENV=staging deploy` | surfline-dev  | wt-copy-rewind-clips-**staging**       |
| `make ENV=sandbox deploy` | surfline-dev  | wt-copy-rewind-clips-**sandbox**       |

Make sure to have the docker daemon running when deploying. Also, make sure to set `DRY_RUN` to `'false'`.

## Testing

This lambda function can be tested locally with one simple command. This command will invoke the Lambda locally,
and will return `True` upon success. Simply do the following:
  - Ensure you have [conda devenv](https://conda-devenv.readthedocs.io/en/latest/installation.html) installed. Then
    create a conda environment:
    - `conda devenv`
  - Activate it:
    - `source activate copy-rewind-clips`
  - Test the lambda:
    - `make ENV=sandbox|staging invoke-local`

Make sure to have the docker daemon running when testing. Also note the `DRY_RUN` variable defined in `environment_vars.yml`,
which you can change to trigger S3 events.

## Environment Variables:

Defined in `environment_vars.yml`, customize the function's actions with the following:

| Name                       | Type                             | Context                                                                  |
|----------------------------|----------------------------------|--------------------------------------------------------------------------|
| `SUPPORTED_CAM`            | **String**                       | Camera-Id of the camera we want to copy rewinds for                      |
| `DESTINATION_BUCKETS`      | **CSV String** Or **String**     | Bucket, or list of buckets, that will be the destination of the rewind   |
| `VALIDATE_ALIAS_URL`       | **String**                       | API url to validate camera alias's                                       |

## Notes

- Add all necessary Python packages to `requirements.txt`
- This repository uses `Serverless` to package and deploy the lambda function.
  The configuration for the deployment can be found in `serverless.yml`. Refer to the
  [Serverless Documentation](https://www.serverless.com/framework/docs/providers/aws/guide/serverless.yml/)
  for information regarding configuration setup.
