# Snapshot Cleanup Infrastructure

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents <!-- omit in toc -->

- [Naming Conventions](#naming-conventions)
- [Terraform](#terraform)
  - [NewRelic information](#newrelic-information)
  - [Using Terraform](#using-terraform)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Terraform

_Uses terraform version `1.0.7`._

### NewRelic information

Before you can run terraform you need the values for [NewRelic] `account_id` and `api_key` configured in your local environment. For example:

```bash
export AWS_PROFILE=surfline-dev
export NEW_RELIC_ACCOUNT_ID=$(
  aws ssm get-parameters \
    --names "/sandbox/common/NEW_RELIC_ACCOUNT_ID" \
    --region "us-west-1" \
    --with-decrypt | \
  jq -r '.Parameters[].Value' \
)
export NEW_RELIC_API_KEY=$(
  aws ssm get-parameters \
    --names "/sandbox/common/NEW_RELIC_API_KEY" \
    --region "us-west-1" \
    --with-decrypt | \
  jq -r '.Parameters[].Value' \
)
```

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
AWS_PROFILE=surfline-dev ENV=sandbox make plan
AWS_PROFILE=surfline-dev ENV=sandbox make apply
```

Valid environments are `sandbox`, `prod`.
