terraform {
  required_version = ">= 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
    newrelic = {
      source  = "newrelic/newrelic"
      version = "~> 2.25.0"
    }
  }
}
