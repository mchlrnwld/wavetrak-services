# EBS Snapshot Cleanup

This Lambda function deletes old snapshots of EBS volumes.


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents <!-- omit in toc -->

- [Components overview:](#components-overview)
- [Serverless Framework](#serverless-framework)
  - [Installing](#installing)
  - [`serverless.yml`](#serverlessyml)
    - [`service`](#service)
    - [`frameworkVersion`](#frameworkversion)
    - [`provider`](#provider)
    - [`plugins`](#plugins)
    - [`package`](#package)
    - [`functions`](#functions)
- [Deploying to AWS](#deploying-to-aws)
  - [Python Requirements](#python-requirements)
- [`infra` Directory](#infra-directory)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Components overview:

- Lambda function: configured in [`serverless.yml`](#serverlessyml).
- [New Relic] alerts: configured in [`infra` directory](#infra-directory).

## Serverless Framework

This Lambda function is managed and provisioned by the Serverless Framework.

### Installing

To install the Serverless Framework, you must have Node version >=10 and npm installed. Once both are installed, simply run the following command:

```
npm install
```

This will install Serverless and all required plugins.

### `serverless.yml`

Serverless Framework uses this file to create and deploy a lambda functions to any given AWS account. The top-level sections of the serverless.yml are:

#### `service`
This describes the service associated with the Lambda function, for example: tag-ec2-instances. The service name will be used to create the Cloudformation template that then creates the Lambda function.

#### `frameworkVersion`
Version of Serverless Framework being used.

#### `provider`
This section is where all AWS-related information is declared. For example: the AWS region, the AWS account associated with the Lambda, and any IAM role permissions. It also contains information about the Python version.

#### `plugins`
This section is where all 3rd-party Serverless plugins are declared. For example, the serverless-python-requirements allows you to easily package and deploy a Python Lambda with 3rd-party dependencies.

#### `package`
What patterns to include and what not to include in the runtime package that gets uploaded to AWS.

#### `functions`
This section is where all Lambda functions are declared. Do note that many Lambda functions can be declared here, not just one. You can specify the name of the function, the handler to use, any associated infrastructure (Cloudwatch, SNS, etc) and more.

## Deploying to AWS

Included in the `Makefile` is `ENV=env-name make deploy`, which allows you to easily deploy the lambda to the AWS account and environment of choice. The desired environment must be passed in. For example:
```sh
AWS_PROFILE=surfline-dev ENV=sandbox make deploy
```

NOTE: There are 2 relevant environments for this lambda function: `sandbox` and `prod`.

### Python Requirements

All dependencies required for runtime are installed via pip in `environment.yml`. Dev-only dependencies are defined in `environment.devenv.yml`.

## `infra` Directory

This directory contains the terraform module to deploy additonal infrastructure: in this case [New Relic] alert definitions.
