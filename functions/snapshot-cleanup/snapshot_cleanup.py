"""
This function looks at *all* snapshots that have a "DeleteOn" tag containing
the current day formatted as YYYY-MM-DD. This function should be run at least
daily.
"""
import datetime
import logging
import os

import boto3

ACCOUNT_IDS = [os.environ["AccountId"]]

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


def lambda_handler(event, context):  # nopep8 pylint: disable=W0613
    """lambda entry point"""
    current_time = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M")
    filters = [
        {"Name": "tag-key", "Values": ["DeleteOn"]},
    ]

    ec2_client = boto3.client("ec2")

    snapshots = []
    des_snaps_paginator = ec2_client.get_paginator("describe_snapshots")
    des_snaps_iterator = des_snaps_paginator.paginate(
        OwnerIds=ACCOUNT_IDS, Filters=filters
    )
    for page in des_snaps_iterator:
        if "Snapshots" in page:
            snapshots.extend(page["Snapshots"])

    for snap in snapshots:
        for value in snap["Tags"]:
            if value["Key"] == "DeleteOn":
                LOGGER.debug("DeleteOn Tag Value: %s", value["Value"])
                if current_time > value["Value"]:
                    LOGGER.info("Deleting snapshot %s", snap["SnapshotId"])
                    ec2_client.delete_snapshot(SnapshotId=snap["SnapshotId"])
