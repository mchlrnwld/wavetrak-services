/**
 * Lambda handler for the default function
 */
const mainHandler = async (event) => {
  // get object from the S3 bucket based on event triggered
  console.log('In surfparks lambda handler. Event: ', event);
  // get sessions based on RDIF and date
  return true;
};

export default mainHandler;
