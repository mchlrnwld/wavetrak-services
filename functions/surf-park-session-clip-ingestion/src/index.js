import dotenv from 'dotenv';

import mainHandler from './handlers';

dotenv.config({ silent: true });

/**
 * Responsible for parsing the event object passed into the lambda function, and
 * returning the appropriate handler for the given parameters.
 *
 * @param {Object} event event object passed into the lambda handler
 */
const handler = (event) => {
  try {
    mainHandler(event);
  } catch (err) {
    console.error(err);
  }
};

export default handler;
