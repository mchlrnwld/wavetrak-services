# Surfparks session clip ingestion Function

This Lambda function runs a sample Node.js function as a scheduled cron.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents <!-- omit in toc -->

- [Components overview](#components-overview)
- [Serverless Framework](#serverless-framework)
  - [Installing](#installing)
  - [`serverless.yml`](#serverlessyml)
- [Deploying to AWS](#deploying-to-aws)
- [`infra` Directory](#infra-directory)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Components overview

- Lambda function: configured in [`serverless.yml`](#serverlessyml).
- [New Relic] alerts: configured in [`infra`](#infra-directory) directory.

## Serverless Framework

This Lambda function is managed and provisioned by the Serverless Framework.

### Installing

To install the Serverless Framework, you must have Node version >=10 and npm installed. Once both are installed, simply run the following command:

```
npm install
```

This will install Serverless and all required plugins. You can use the the Serverless CLI by typing `npx serverless`.

### `serverless.yml`

Serverless Framework uses this file to create and deploy a lambda functions to any given AWS account. The top-level sections of the serverless.yml are:

- `service`

  This describes the service associated with the Lambda function, for example: tag-ec2-instances. The service name will be used to create the Cloudformation template that then creates the Lambda function.

- `frameworkVersion`

  Version of Serverless Framework being used.

- `provider`

  This section is where all AWS-related information is declared. For example: the AWS region, the AWS account associated with the Lambda, and any IAM role permissions. It also contains information about the Node.js version.

- `plugins`

  This section is where all 3rd-party Serverless plugins are declared. For example, the serverless-newrelic-lambda-layers plugin allows easy integration with New Relic.

- `package`

  What patterns to include and what not to include in the runtime package that gets uploaded to AWS.

- `functions`

  This section is where all Lambda functions are declared. Do note that many Lambda functions can be declared here, not just one. You can specify the name of the function, the handler to use, any associated infrastructure (Cloudwatch, SNS, etc) and more.

## Running the Function Locally

To run function locally run:
`npm i`,
`npm run build`,
`npm run start`,

## Deploying to AWS 

Deploy function by lambda functions pipeline - GitHub actions. 
## Deploying to AWS Manually
Make sure you have the right [AWS credentials set up just like for terraform](https://github.com/Surfline/styleguide/tree/master/terraform#aws-credentials) and then run:

```sh
npx serverless --stage <env-name> deploy
```

Or, use the `deploy` script from `package.json` (note the extra `--`):
```sh
npm run deploy -- --stage <env-name>
```

NOTE: There are 3 relevant environments for this lambda function: `sandbox`, `staging` and `prod`. Additionally, `dev` is used as a placeholder for an environment when using `serverless` to run code locally.


## `infra` Directory

This directory contains the terraform module to deploy additonal infrastructure: in this case [New Relic] alert definitions.
