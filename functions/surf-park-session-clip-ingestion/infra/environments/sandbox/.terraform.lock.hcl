# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "2.70.1"
  constraints = "~> 2.70"
  hashes = [
    "h1:B+Cs8HwWnLn9ymYI/r3B5S10w1+hOD5WmfFxiLHTUIA=",
    "zh:04137cdf128cf21dcd190bbba4d4bba43c7868c52ad646b0eaa54a8b8b8160a7",
    "zh:30c9f956133a102b4a426d76dd3ef1a42332d9875261a06aa877409aa6b2b556",
    "zh:3107a43647454a3d6d847fba6aa593650af0f6a353272c04450408af5f4d353a",
    "zh:3f17285478313af822447b453fa4e37f30ef221f0b0e8f2e4655f1ac9f9de1a2",
    "zh:5a626f7a3c4a9fea3bdfde63aedbf6eea73760f3b228f776f1132b61d00c7ff2",
    "zh:6aafc9dd79b511b9e3d0ec49f7df1d1fd697c3c873d1d70a2be1a12475b50206",
    "zh:6fb29b48ccc85f7e9dfde3867ce99d6d65fb76bea68c97d404fae431758a8f03",
    "zh:c47be92e1edf2e8675c932030863536c1a79decf85b2baa4232e5936c5f7088f",
    "zh:cd0a4b28c5e4b5092043803d17fd1d495ecb926c2688603c4cdab4c20f3a91f4",
    "zh:fb0ff763cb5d7a696989e58e0e4b88b1faed2a62b9fb83f4f7c2400ad6fabb84",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version     = "2.2.0"
  constraints = "~> 2.1"
  hashes = [
    "h1:0wlehNaxBX7GJQnPfQwTNvvAf38Jm0Nv7ssKGMaG6Og=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}

provider "registry.terraform.io/newrelic/newrelic" {
  version     = "2.25.0"
  constraints = "~> 2.25.0"
  hashes = [
    "h1:14QQUjGBGPdz+ZSVVjpLFL29/XpYN6VtJ+tXN3NMu8Y=",
    "zh:02071554fe8815c0a8b6bd0bf6f339f3fcf3f09a3976fbe115c3691605d6b954",
    "zh:2f052b0af6d4cc94db05ae9ce90133b94560d89a4bd2d42b81f8e93f343e4f07",
    "zh:34f5196f3a31c7c0d540154bf16067cf0a45febcb60086dae4c5f08ac25a6a5f",
    "zh:3b974daba50669e1330fee20ef5d93ff3c484513a90fb65f4a42444ebec8d89a",
    "zh:3d8b1d82e99c04f32d61355786a365ff914bd05512dbe96543b694acb8c30ea2",
    "zh:4025ea419f48dd36ef6c478fad1a851c6d391f81686ff583f88206fba44b3396",
    "zh:47d1b3af395bd76d9720491d89bb3476b7d994e026fedb6b94d110c41748bdf8",
    "zh:49744a29eed36fe6230d4e3cfd16c4efd26385fa512140c2470d5daf31e63270",
    "zh:69f3733b4a2f09dc80501630fa0529b498f974d8442546466d4166d6bd57d1ac",
    "zh:7eca255d19becd697440ce4889853ff34bba434febd6486b97b6657983a851ad",
    "zh:85e1656b70426a5b7256cb184e95453e71c5ae52db9b3538f0e1c37df807bdf6",
    "zh:8c8a7a356a14acd524d9731b83c68570efb126c4f539b779cf2689a9e58221ed",
    "zh:c4f23826bac33bcebb83e77b00b04862524534012f68ea4a194b4fa47a6d2eba",
    "zh:e021c163c4131c2acfde03c01061c895514b6ca9917b911a153af7392460e500",
    "zh:f46b9e55028fe66268b4e9c9849a83c0de109827f98400296a97bf8f0e35c9dc",
    "zh:fbecb4f74937739cc2f6cd29dc82e22a6dfb1f8b8e2a5fa240799b78e3df2ca5",
  ]
}
