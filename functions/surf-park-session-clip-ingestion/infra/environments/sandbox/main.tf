provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/sandbox/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/sandbox/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surf-park-session-clip-ingestion/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "surf-park-session-clip-ingestion" {
  source = "../../"

  environment = "sandbox"

  cdn_domains = ["sandbox.surfline.com"]
}
