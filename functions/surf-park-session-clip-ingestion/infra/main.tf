locals {
  application_title                       = "Surf Park Session Clip Ingestion"
  function_name                           = "${var.application}-clip-ingestion-${var.environment}"
  surf_park_session_cdn_whitelist_headers = ["Origin"]
  surf_park_session_clips_cdn_fqdn = {
    "${element(var.cdn_domains, 0)}" = ["${var.application}-clips.${element(var.cdn_domains, 0)}"]
  }
}

# SNS
data "aws_sns_topic" "upload_topic" {
  name = "sl-surf-park-sessions-clips-holding-pen-${var.environment}-surf-park-clip-upload-event"
}

# S3

resource "aws_iam_role" "fleye_S3__clips_holding_pen_bucket_role" {
  name = "fleye-clips-holding-pen-iam-role-${var.environment}"
  assume_role_policy = file("${path.module}/resources/fleye-s3-iam-role-policy.json")
}

resource "aws_iam_policy" "fleye_access_policy" {
  name        = "fleye_access_policy_${var.environment}"
  description = "policy for fleye to put clips into s3"
  policy = templatefile("${path.module}/resources/fleye-s3-policy.json", {
    s3_bucket = aws_s3_bucket.surf_park_ingestion_holding_pen.arn
  })
}

resource "aws_iam_role_policy_attachment" "fleye_access_policy_attachment" {
  role       = aws_iam_role.fleye_S3__clips_holding_pen_bucket_role.name
  policy_arn = aws_iam_policy.fleye_access_policy.arn
}

resource "newrelic_alert_policy" "function_alert_policy" {
  name                = local.application_title
  incident_preference = "PER_CONDITION_AND_TARGET"
}

resource "newrelic_nrql_alert_condition" "function_alert_condition" {
  name                         = "Lambda Function Error: ${local.function_name}"
  policy_id                    = newrelic_alert_policy.function_alert_policy.id
  type                         = "static"
  enabled                      = "true"
  value_function               = "single_value"
  violation_time_limit_seconds = "86400"

  nrql {
    query             = <<EOT
      SELECT sum(`provider.errors.Sum`)
      FROM   ServerlessSample
      WHERE  provider LIKE 'LambdaFunction%'
      AND    providerAccountId = '1500'
      FACET  `provider.functionName`
      WHERE  `provider.functionName` = '${local.function_name}'
    EOT
    evaluation_offset = "20"
  }

  # alert on any error (above 0) in the past 5 minutes
  critical {
    operator              = "above"
    threshold             = 0
    threshold_duration    = 5 * 60
    threshold_occurrences = "all"
  }
}


# Add clips holding pen S3 bucket
resource "aws_s3_bucket" "surf_park_ingestion_holding_pen" {
  bucket = "${var.company}-${var.application}-clips-holding-pen-${var.environment}"
  acl    = "private"
}

# Add clips S3 bucket and cloudfront distribution
# TODO - need to get S3 bucket arn and add it to the ingestion access policy
module "surf_park_session_clips" {
  source = "../../../infrastructure/terraform/modules/aws/cloudfront-with-s3"

  company     = var.company
  environment = var.environment
  application = var.application
  service     = "clips"

  cdn_acm_count      = length(var.cdn_domains)
  cdn_acm_domains    = var.cdn_domains
  comment            = "${var.environment} Surf park session clips CDN wth S3"
  cdn_fqdn           = local.surf_park_session_clips_cdn_fqdn
  versioning_enabled = false
  allowed_origins    = "*"
  default_ttl        = var.surf_park_session_cdn_default_ttl
  max_ttl            = var.surf_park_session_cdn_max_ttl
  whitelist_headers  = local.surf_park_session_cdn_whitelist_headers
}

# Add pretty Route53 records for the CDN
data "aws_route53_zone" "cdn" {
  count = length(var.cdn_domains)
  name  =  var.cdn_domains[count.index]
}

resource "aws_route53_record" "cdn" {
  count   = length(var.cdn_domains)
  zone_id = data.aws_route53_zone.cdn.*.zone_id[count.index]
  name = element(
    local.surf_park_session_clips_cdn_fqdn[element(var.cdn_domains, count.index)],
    0,
  )
  type    = "CNAME"
  ttl     = 300
  records = module.surf_park_session_clips.domain_name
}

