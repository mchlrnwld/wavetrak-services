variable "company" {
  default = "sl"
}

variable "application" {
  default = "surf-park-sessions"
}

variable "environment" {
}

/*
TODO - update this we set up the lambda
variable "ingestion_lambda_iam_role" {
  description = "IAM role arn for lambda"
}
*/

variable "cdn_domains" {
  description = "List of cloudfront distribution names"
   type        = list(string)
}

variable "surf_park_session_cdn_default_ttl" {
  description = "Surfpark sessions clips CDN default TTL"
  default     = 60
}

variable "surf_park_session_cdn_max_ttl" {
  description = "Surfpark sessions clips CDN max TTL"
  default     = 60
}
