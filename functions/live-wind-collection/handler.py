import logging

from lib.clients.weather_stations_api_client import fetch_weather_stations
from lib.fetch_station_data import fetch_current_conditions
from lib.store_sensors_to_s3 import store_data_in_s3
from lib.WeatherStation import WeatherStation

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def run(event, context=None):
    stations = [
        WeatherStation.from_dict(station)
        for station in fetch_weather_stations()
    ]
    logger.info(f"Stations => {len(stations)} stations")

    successful_stations = fetch_current_conditions(stations)
    logger.info(f"{len(successful_stations)} successful stations")

    if len(successful_stations) > 0:
        store_data_in_s3(successful_stations)

        for station in successful_stations:
            station.post_missing_sensors()
