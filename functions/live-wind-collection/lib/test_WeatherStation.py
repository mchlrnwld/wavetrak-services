import pytest
import responses  # type: ignore
from lib.config import WEATHER_STATIONS_API
from lib.SurflineSensor import SurflineSensor
from lib.WeatherStation import WeatherStation
from test_conts import MOCK_SENSOR_ID_01, MOCK_SENSOR_ID_02, MOCK_STATION_ID_01

MOCK_NEW_SENSORS = [
    SurflineSensor(
        weather_link_id=85588,
        type=22,
    ),
    SurflineSensor(
        weather_link_id=66666,
        type=21,
    ),
]

MOCK_POST_SENSOR = [
    b'{"weatherLinkId": 85588, "type": 22}',
    b'{"weatherLinkId": 66666, "type": 21}',
]


@pytest.fixture
def intercept_sensor_requests():
    with responses.RequestsMock() as rsps:
        for sensor in MOCK_POST_SENSOR:
            rsps.add(
                responses.POST,
                f'{WEATHER_STATIONS_API}/{MOCK_STATION_ID_01}/sensors',
                body=sensor,
            )
        yield rsps


def test_post_missing_sensors_new_sensors(intercept_sensor_requests):
    station = WeatherStation.from_dict(
        {
            '_id': MOCK_STATION_ID_01,
            'weatherLinkId': 11111,
            'station_name': 'test_station',
            'sensors': [],
        }
    )

    for sensor in MOCK_NEW_SENSORS:
        station.new_sensors.append(sensor)

    station.post_missing_sensors()

    for index, call in enumerate(intercept_sensor_requests.calls):
        assert call.request.body == MOCK_POST_SENSOR[index]


def test_post_missing_sensors_no_new_sensors():
    with responses.RequestsMock() as rsps:
        station = WeatherStation.from_dict(
            {
                '_id': MOCK_STATION_ID_01,
                'weatherLinkId': 11111,
                'station_name': 'test_station',
                'sensors': [],
            }
        )

        station.post_missing_sensors()

        assert len(rsps.calls) == 0


def test_set_sensor_data():
    station = WeatherStation(
        _id=MOCK_STATION_ID_01,
        weather_link_id=12345,
        sensors={
            1234: SurflineSensor(weather_link_id=1234, type=12),
            2345: SurflineSensor(weather_link_id=2345, type=20),
            4567: SurflineSensor(weather_link_id=4567, type=12),
        },
    )

    station.set_sensor_data(
        {
            "station_id": 12345,
            "sensors": [
                {
                    "lsid": 1234,
                    "sensor_type": 20,
                    "data": [
                        {
                            "invalid_key": 'testing sensors',
                        }
                    ],
                },
                {
                    "lsid": 2345,
                    "sensor_type": 20,
                    "data": [
                        {
                            "wind_speed_avg_last_2_min": 70,
                            "wind_dir_scalar_avg_last_2_min": -70,
                            "wind_speed_hi_last_2_min": 2,
                            "wind_dir_at_hi_speed_last_2_min": 1,
                            "ts": 1645228800,
                        }
                    ],
                },
                {
                    "lsid": 3456,
                    "sensor_type": 20,
                    "data": [],
                },
            ],
        }
    )

    assert station.sensors[1234].data is None
    assert station.sensors[4567].data is None
    assert station.sensors[2345].data == {
        "wind_speed_avg_last_2_min": 70,
        "wind_dir_scalar_avg_last_2_min": -70,
        "wind_speed_hi_last_2_min": 2,
        "wind_dir_at_hi_speed_last_2_min": 1,
        "ts": 1645228800,
    }
    assert station.new_sensors == [
        SurflineSensor(weather_link_id=3456, type=20),
    ]


def test_get_sensor_dicts():
    station = WeatherStation(
        _id=MOCK_STATION_ID_01,
        weather_link_id=12345,
        sensors={
            1234: SurflineSensor(
                _id=MOCK_SENSOR_ID_01, weather_link_id=1234, type=33
            ),
            2345: SurflineSensor(
                _id=MOCK_SENSOR_ID_02,
                weather_link_id=2345,
                type=12,
                data={
                    "wind_speed_avg_last_2_min": 70,
                    "wind_dir_scalar_avg_last_2_min": -70,
                    "wind_speed_hi_last_2_min": 2,
                    "wind_dir_at_hi_speed_last_2_min": 1,
                    "ts": 1645228800,
                },
            ),
        },
    )

    assert station.get_sensor_dicts() == [
        {
            "station_id": MOCK_STATION_ID_01,
            "sensor_id": MOCK_SENSOR_ID_02,
            "sensor_type": 12,
            "wind_speed_avg_last_2_min": 70,
            "wind_dir_scalar_avg_last_2_min": -70,
            "wind_speed_hi_last_2_min": 2,
            "wind_dir_at_hi_speed_last_2_min": 1,
            "ts": 1645228800,
        }
    ]
