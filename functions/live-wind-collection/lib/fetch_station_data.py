import asyncio
import logging
from typing import List

import aiohttp
from lib import config
from lib.WeatherStation import WeatherStation

logger = logging.getLogger()


async def throttle_fetch(stations: List[WeatherStation]):
    results = []
    async with aiohttp.ClientSession(raise_for_status=True) as session:
        # iterate over `stations_ids` in chunks of RL_BATCH_SIZE
        for i in range(0, len(stations), config.RL_BATCH_SIZE):
            # create tasks for the batch
            results += [
                asyncio.create_task(
                    station.fetch_current_weather_conditions(session)
                )
                for station in stations[i : i + config.RL_BATCH_SIZE]  # noqa
            ]
            # sleep until next batch
            await asyncio.sleep(config.RL_INTERVAL)
        return await asyncio.gather(*results)


def fetch_current_conditions(stations: List[WeatherStation]):
    successful_stations = []

    # retry fetching current station data if unsuccessful on first attempt
    for _ in range(config.MAX_RETRIES):
        result = asyncio.run(throttle_fetch(stations))

        # iterate through fetched data pulling out successful attempts
        successful_stations += [
            station for station in result if station.success
        ]

        if len(successful_stations) == len(stations):
            break

        # get the ids for the next attempt
        stations = [station for station in result if not station.success]

    return successful_stations
