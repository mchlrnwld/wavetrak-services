import logging
from dataclasses import dataclass, field
from typing import Dict, List

import aiohttp
from lib.clients.weather_link_client import create_request_parameters
from lib.clients.weather_stations_api_client import post_stations
from lib.config import WEATHER_LINK_API_KEY, WEATHER_LINK_URL
from lib.SurflineSensor import SurflineSensor

logger = logging.getLogger()


@dataclass
class WeatherStation:

    _id: str
    weather_link_id: int
    sensors: Dict
    new_sensors: List[SurflineSensor] = field(default_factory=lambda: [])
    success: bool = False

    @classmethod
    def from_dict(cls, station_dict):
        sensors = {}

        for sensor in station_dict['sensors']:
            sensorObj = SurflineSensor.from_dict(sensor)
            sensors[sensorObj.weather_link_id] = sensorObj

        return cls(
            _id=station_dict['_id'],
            weather_link_id=station_dict['weatherLinkId'],
            sensors=sensors,
        )

    async def fetch_current_weather_conditions(self, session):
        request_parameters = create_request_parameters(self.weather_link_id)
        request_path = f'{WEATHER_LINK_URL}/v2/current/{self.weather_link_id}'
        request_url = (
            f'{request_path}?api-key={WEATHER_LINK_API_KEY}&'
            f'api-signature={request_parameters[0]}&t={request_parameters[1]}'
        )
        try:
            async with session.get(request_url) as response:
                data = await response.json()
                self.set_sensor_data(data)

        except aiohttp.ClientResponseError as e:
            logger.error(
                f'Error occurred fetching current conditions'
                f'for weather link station {self._id}.'
            )
            logger.error(f'ERROR: {e}')

        return self

    def set_sensor_data(self, data):
        for wl_sensor in data['sensors']:
            sl_sensor = self.sensors.get(wl_sensor["lsid"])

            if sl_sensor and len(wl_sensor.get("data", [])) > 0:
                sl_sensor.set_data(wl_sensor["data"][0])
            else:
                self.new_sensors.append(
                    SurflineSensor.from_wl_data(data=wl_sensor)
                )
        self.success = True

    def post_missing_sensors(self):
        if len(self.new_sensors) > 0:
            for sensor in self.new_sensors:
                try:
                    post_stations(self._id, sensor.to_dict())
                except Exception as e:
                    logger.error(f'exception: {e}')
            logger.info(
                f'Updated {len(self.new_sensors)} sensor(s) '
                f'for station: {self._id}.'
            )
        else:
            logger.info(f'Nothing to update for station: {self._id}.')

    def get_sensor_dicts(self):
        return [
            {
                "station_id": self._id,
                "sensor_id": sensor._id,
                "sensor_type": sensor.type,
                **sensor.data,
            }
            for sensor in self.sensors.values()
            if sensor.data is not None
        ]
