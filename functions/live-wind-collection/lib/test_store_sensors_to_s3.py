import time
from calendar import timegm

import boto3  # type: ignore
import pandas  # type: ignore
import pytest  # type: ignore
from lib.config import S3_LIVE_WEATHER_BUCKET, TIMESTAMP_FORMAT
from lib.fixtures.store_sensors_s3_fixtures import (
    MOCK_SENSOR_DICT_LIST,
    SUCCESSFUL_STATIONS,
)
from lib.store_sensors_to_s3 import store_data_in_s3
from moto import mock_s3  # type: ignore

AWS_REGION = "us-west-1"


@pytest.fixture()
def live_weather_bucket():
    with mock_s3():
        s3 = boto3.resource('s3')
        # create the s3 bucket
        bucket = s3.create_bucket(
            Bucket=S3_LIVE_WEATHER_BUCKET,
            CreateBucketConfiguration={'LocationConstraint': AWS_REGION},
        )

        yield bucket


# integration test for units tested above
# tests there is a file created in s3
def test_store_data_in_s3(live_weather_bucket):

    store_data_in_s3(SUCCESSFUL_STATIONS)

    bucket_objects = list(live_weather_bucket.objects.all())

    assert len(bucket_objects) == 1

    pq_key = f'weatherlink/data/collection_dt'

    stored_key = bucket_objects[0].key
    key_parts = stored_key.split('=')

    assert key_parts[0] == pq_key

    key_time = timegm(time.strptime(key_parts[1], TIMESTAMP_FORMAT))

    # 3s tolerance
    assert time.time() - key_time < 3

    df = pandas.read_parquet(f's3://{S3_LIVE_WEATHER_BUCKET}/{stored_key}')

    assert df.equals(pandas.DataFrame(MOCK_SENSOR_DICT_LIST))
