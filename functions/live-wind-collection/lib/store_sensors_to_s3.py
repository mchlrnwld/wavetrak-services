import logging
from datetime import datetime
from typing import List

import pandas as pd  # type: ignore
from lib.config import (
    S3_LIVE_WEATHER_BUCKET,
    S3_LIVE_WEATHER_PATH,
    TIMESTAMP_FORMAT,
)
from lib.WeatherStation import WeatherStation

logger = logging.getLogger()


def convert_and_store_in_s3(sensor_dict_list):
    # convert dict list into dataframes
    sensors_df = pd.DataFrame(sensor_dict_list)

    now = datetime.now().strftime(TIMESTAMP_FORMAT)
    s3_file_name = (
        f's3://{S3_LIVE_WEATHER_BUCKET}/'
        f'{S3_LIVE_WEATHER_PATH}collection_dt={now}'
    )

    try:
        sensors_df.to_parquet(s3_file_name)
    except PermissionError as e:
        logger.exception(f'ERROR:  {e}')
        logger.critical(f'Unable to write parquet to {s3_file_name}')
        raise e


def store_data_in_s3(stations: List[WeatherStation]):
    sensor_dicts = []
    for station in stations:
        try:
            sensor_dicts += station.get_sensor_dicts()
        except Exception as e:
            logger.exception(e)
            logger.error(f'station data extraction failed for: {station._id}')
    convert_and_store_in_s3(sensor_dicts)
