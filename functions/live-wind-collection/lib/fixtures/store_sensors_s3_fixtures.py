from lib.SurflineSensor import SurflineSensor
from lib.WeatherStation import WeatherStation
from test_conts import (
    MOCK_SENSOR_ID_01,
    MOCK_SENSOR_ID_02,
    MOCK_SENSOR_ID_03,
    MOCK_STATION_ID_01,
    MOCK_STATION_ID_02,
    MOCK_STATION_ID_03,
)

SUCCESSFUL_STATIONS = [
    WeatherStation(
        _id=MOCK_STATION_ID_01,
        weather_link_id=35552,
        sensors={},
        new_sensors=[
            SurflineSensor(
                weather_link_id=12355,
                type=23,
            ),
            SurflineSensor(
                weather_link_id=6989,
                type=24,
            ),
        ],
    ),
    WeatherStation(
        _id=MOCK_STATION_ID_02,
        weather_link_id=26664,
        sensors={
            12345: SurflineSensor(
                _id=MOCK_SENSOR_ID_01,
                weather_link_id=12345,
                type=23,
                data={
                    "wind_speed_avg_last_2_min": 80,
                    "wind_dir_scalar_avg_last_2_min": -80,
                    "wind_speed_hi_last_2_min": 88,
                    "wind_dir_at_hi_speed_last_2_min": 88,
                    "ts": 1645228800,
                },
            ),
            22345: SurflineSensor(
                _id=MOCK_SENSOR_ID_02,
                weather_link_id=22345,
                type=22,
                data={
                    "wind_speed_avg_last_2_min": 70,
                    "wind_dir_scalar_avg_last_2_min": -70,
                    "wind_speed_hi_last_2_min": 2,
                    "wind_dir_at_hi_speed_last_2_min": 1,
                    "ts": 1645228800,
                },
            ),
        },
    ),
    WeatherStation(
        _id=MOCK_STATION_ID_03,
        weather_link_id=6789,
        sensors={
            6789: SurflineSensor(
                _id=MOCK_SENSOR_ID_03,
                weather_link_id=6789,
                type=24,
                data=None,
            )
        },
        new_sensors=[
            SurflineSensor(
                weather_link_id=6889,
                type=24,
            )
        ],
    ),
]


MOCK_SENSOR_DICT_LIST = [
    {
        "station_id": MOCK_STATION_ID_02,
        "sensor_id": MOCK_SENSOR_ID_01,
        "sensor_type": 23,
        "wind_speed_avg_last_2_min": 80,
        "wind_dir_scalar_avg_last_2_min": -80,
        "wind_speed_hi_last_2_min": 88,
        "wind_dir_at_hi_speed_last_2_min": 88,
        "ts": 1645228800,
    },
    {
        "station_id": MOCK_STATION_ID_02,
        "sensor_id": MOCK_SENSOR_ID_02,
        "sensor_type": 22,
        "wind_speed_avg_last_2_min": 70,
        "wind_dir_scalar_avg_last_2_min": -70,
        "wind_speed_hi_last_2_min": 2,
        "wind_dir_at_hi_speed_last_2_min": 1,
        "ts": 1645228800,
    },
]
