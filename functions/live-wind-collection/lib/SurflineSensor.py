from dataclasses import dataclass
from typing import Dict, Optional

REQUIRED_FIELDS = [
    "wind_speed_avg_last_2_min",
    "wind_dir_scalar_avg_last_2_min",
    "wind_speed_hi_last_2_min",
    "wind_dir_at_hi_speed_last_2_min",
    "ts",
]


@dataclass()
class SurflineSensor:
    weather_link_id: int
    type: int
    _id: Optional[str] = None
    data: Optional[Dict] = None

    @classmethod
    def from_dict(cls, dict):
        return cls(
            _id=dict['_id'],
            weather_link_id=dict['weatherLinkId'],
            type=dict['type'],
        )

    @classmethod
    def from_wl_data(cls, data):
        return cls(weather_link_id=data['lsid'], type=data['sensor_type'])

    def set_data(self, sensor_data):
        if sensor_data and not any(
            sensor_data.get(field) is None for field in REQUIRED_FIELDS
        ):
            self.data = sensor_data

    def to_dict(self):
        return {
            'weatherLinkId': self.weather_link_id,
            'type': self.type,
        }
