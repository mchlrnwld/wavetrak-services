from lib.clients.weather_link_client import create_request_parameters


def test_create_request_parameters():
    station_id = 2
    response = create_request_parameters(station_id)
    assert len(response[0]) == 64
    assert len(response) == 2
