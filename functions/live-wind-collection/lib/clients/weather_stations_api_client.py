import logging

import requests
from lib.config import WEATHER_STATIONS_API

logger = logging.getLogger()


def fetch_weather_stations():
    r = requests.get(WEATHER_STATIONS_API)
    r.raise_for_status()
    return r.json()


def post_stations(station_id, sensor_data):
    """
    Given an id and a dict of sensor data,
    send a HTTP POST request, returning a JSON response.

    Args:
        station_id: parameter
        sensor_data: Payload to POST

    Returns:
        Response payload
    """
    stations_endpoint = f'{WEATHER_STATIONS_API}/{station_id}/sensors'
    r = requests.post(stations_endpoint, json=sensor_data)
    r.raise_for_status()
    return r.json()
