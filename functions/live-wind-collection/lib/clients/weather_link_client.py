import collections
import hashlib
import hmac
import logging
import time

from lib.config import WEATHER_LINK_API_KEY, WEATHER_LINK_API_SECRET

logger = logging.getLogger()


def create_request_parameters(station_id):
    parameters = {
        "api-key": WEATHER_LINK_API_KEY,
        "station-id": station_id,
        "t": int(time.time()),
    }
    parameters = collections.OrderedDict(parameters.items())

    api_secret = WEATHER_LINK_API_SECRET

    data = ""
    for key in parameters:
        data = data + key + str(parameters[key])
    api_signature = hmac.new(
        api_secret.encode('utf-8'), data.encode('utf-8'), hashlib.sha256
    ).hexdigest()
    res = [api_signature, parameters["t"]]
    return res
