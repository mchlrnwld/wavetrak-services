import { config } from 'dotenv';
import '@babel/register';
import '@babel/preset-env';

config({ silent: true, path: '.env.test' });
jest.setTimeout(60000);
