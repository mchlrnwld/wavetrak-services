import fetch from 'node-fetch';

import generateSns from './snsEvent';
import logger from '../common/logger';
import handler from '../handlers/htmlGenerator';
import dbContext from '../model/dbContext';

const generateEmbeds = async () => {
  try {
    await dbContext.initMongoDB();
    const cameraUrl = `${process.env.CAMERAS_API}/cameras`;
    logger.log('info', `Getting camera details from: ${cameraUrl}.`);

    const response = await fetch(cameraUrl);
    const cameras = await response.json();

    const snsArray = [];
    // Accounting for down cameras (this is optional)
    cameras.forEach((cam) => {
      const id = cam._id;
      const sns = generateSns(id);
      snsArray.push(sns);
    });

    // Throttling loop through camArray here.
    // Without throttle the handler was called too often
    let i = 0;
    const loopCount = snsArray.length;
    // eslint-disable-next-line no-inner-declarations
    async function f() {
      const snsMessage = snsArray[i];
      await handler(snsMessage);

      i += 1;
      if (i < loopCount) {
        setTimeout(f, 50);
      } else {
        return 'Done';
      }
      return null;
    }
    return f();
  } catch (error) {
    logger.log('info', error);
    return error;
  }
};

generateEmbeds();
