import aws from 'aws-sdk';
import fs from 'fs';

import logger from '../common/logger';

const deployDist = () => {
  // Deploying JS and CSS files once into same bucket as generated HTML pages
  if (process.env.ENV === 'LOCAL') {
    // JS
    fs.copyFileSync('src/static/assets/script.js', 'src/static/dist/script.js');
    logger.log('info', 'Copying JS Script to dist/ folder');
    // CSS
    fs.copyFileSync('src/static/assets/styles/styles.css', 'src/static/dist/styles.css');
    logger.log('info', 'Copying CSS styles to dist/ folder');
  } else {
    // Locking API Version for consistency
    const s3 = new aws.S3({ apiVersion: '2006-03-01' });

    // Uploading script.js ONCE
    fs.readFile('src/static/assets/script.js', (err, data) => {
      if (err) {
        throw err;
      } else {
        const params = {
          Body: data,
          Bucket: process.env.EMBED_BUCKET,
          Key: 'cam/script.js',
          ContentType: 'application/javascript',
          ContentEncoding: 'utf8',
        };
        try {
          s3.putObject(params, (error, putData) => {
            if (error) {
              logger.error({
                message: 'S3PutError',
                stack: error,
              });
            } else {
              logger.log('info', putData);
            }
          });
        } catch (error) {
          logger.error({
            message: 'S3 Error during JS upload, check params',
            stack: error,
          });
        }
      }
    });

    // Uploading styles.css ONCE
    fs.readFile('src/static/assets/styles/styles.css', (err, data) => {
      if (err) {
        throw err;
      } else {
        const params = {
          Body: data,
          Bucket: process.env.EMBED_BUCKET,
          Key: 'cam/styles.css',
          ContentType: 'text/css',
          ContentEncoding: 'utf8',
        };
        try {
          s3.putObject(params, (error, putData) => {
            if (error) {
              logger.error({
                message: 'S3PutError',
                stack: error,
              });
            } else {
              logger.log('info', putData);
            }
          });
        } catch (error) {
          logger.error({
            message: 'S3 Error during CSS upload, check params',
            stack: error,
          });
        }
      }
    });
  }
};

deployDist();
