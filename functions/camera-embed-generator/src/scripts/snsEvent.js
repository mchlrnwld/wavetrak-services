/* eslint-disable */
const generateSns = (cameraId) => (
    {
        "Records": [
            {
                "EventSource": "aws:sns",
                "EventVersion": "1.0",
                "EventSubscriptionArn": "arn:aws:sns:us-west-1:665294954271:camera_details_updated_sandbox:e3bab144-d81e-4abc-9a0e-d0a30b3b6d2e",
                "Sns": {
                    "Type": "Notification",
                    "MessageId": "e7d6b64f-eead-5a76-9e04-e28ba8d51f0c",
                    "TopicArn": "arn:aws:sns:us-west-1:665294954271:camera_details_updated_sandbox",
                    "Subject": "Cameras Service: Camera Details Updated",
                    "Message": `{\"cameraId\":\"${cameraId}\"}`,
                    "Timestamp": "2019-01-24T23:03:20.723Z",
                    "SignatureVersion": "1",
                    "Signature": "qeYqbMw2ddp2lua9+Ga3BG7KL6eC5ZxrHl1GS/yQqHRS1We1M02+NvifX1zBADhnHkEDYor8OsHpw3e+Lcf+64tvKvRWcGUfkmpaSfciAPnSYk6yZbOIhhwDHFQAfIeDt79vkVE4ow4P9vYNZ/H2Znu+0H+25sydTJppWwrRWmrsHkWRaTqlu0QwPPYsFoynALTDKgrLjva10WX4Vy9jeR5EdRd1DIwaNLMaUdYUalL2rIctJwZOSyjNnZ6uPio9ABwaFVmdOTKQelQsq8xIYbEXHCpHHAaOlcspNVKA4xtFjwpyPaAcD3bLxucpSN+PIbh4upsKLhBkHbe8gy0njg==",
                    "SigningCertUrl": "https://sns.us-west-1.amazonaws.com/SimpleNotificationService-ac565b8b1a6c5d002d285f9598aa1d9b.pem",
                    "UnsubscribeUrl": "https://sns.us-west-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-1:665294954271:camera_details_updated_sandbox:e3bab144-d81e-4abc-9a0e-d0a30b3b6d2e",
                    "MessageAttributes": {}
                }
            }
        ]
    }  
);

export default generateSns;