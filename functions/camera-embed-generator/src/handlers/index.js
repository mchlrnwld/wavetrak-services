import { eventToMessage } from '../utils/sns';
import htmlGenerator from './htmlGenerator';
import embedConfig from './embedConfig';
import cameraDetails from './cameraDetails';

const matchHandler = async (event) => {
  const message = eventToMessage(event);

  if (message) {
    if (message.cameraId && message.topic === 'AWS_CAMERA_EMBED_CONFIG_CREATED_TOPIC') {
      return embedConfig(message);
    }
    if (message.cameraId) {
      await Promise.all([await htmlGenerator(event), await cameraDetails(message)]);
    }
  }

  throw new Error('No matching handler to run for functions inputs');
};

export default matchHandler;
