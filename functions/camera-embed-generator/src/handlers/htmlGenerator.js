import fetch from 'node-fetch';
import aws from 'aws-sdk';
import fs from 'fs';
import mongoose from 'mongoose';

import dbContext from '../model/dbContext';
import logger from '../common/logger';

const generateHtml = async (spot, cam, url) => {
  const { name } = spot;
  const { stillUrl, isPremium, isDown } = cam;
  const { status, message } = isDown;

  const cdnUrl = cam.streamUrl || cam.playlistUrl;
  const passedUrl = isPremium ? '' : cdnUrl;

  const PremiumCamCTA = {
    indicatorText: 'premium cam',
    headerText: 'This is a High Resolution, Premium Only Cam',
    subheaderText: 'Watch live conditions of this spot in full HD, ad-free',
    ctaText: 'Go Premium',
    learnMoreText: 'Learn more about Surfline Premium',
  };

  const html = `
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta charSet='utf-8' />
        <meta httpEquiv="Content-Language" content="en" />
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <title>${name}</title>

        <link rel="stylesheet" href="styles.css" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
        <script src="https://cdn.jsdelivr.net/npm/hls.js@latest"></script>
      </head>

      <body>
        <div id="wrapper">
          <div id="sl-video-wrapper">
            <video class="sl-embed-video" id="sl-video" poster="${stillUrl}" muted controls autoplay playsinline></video>

            <div id="sl-premium-overlay-container" class="sl-premium-overlay-cta" style="background-image: url(${stillUrl}); background-size: cover;">
              <div class="sl-premium-cam-cta__content__container">
                <div class="sl-premium-cam-cta__content">
                  <div class="sl-premium-cam-cta__icon">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="24"
                      height="27"
                      viewBox="0 0 24 27"
                    >
                      <g fill="none" fillRule="evenodd" strokeWidth="1.152">
                        <path fill="#FFF" stroke="#FFF" d="M.576.576v25.541L12 21.088l11.424 5.03V.575H.576z" />
                        <path
                          stroke="#22D737"
                          fill="#22D737"
                          d="M18.286 8.951v5.42l-2.794-1.458v1.638c0 .392-.315.71-.704.71h-8.37a.707.707 0 0 1-.704-.71V8.928c0-.393.316-.71.704-.71h8.37c.389 0 .704.317.704.71v1.48l2.794-1.457z"
                        />
                      </g>
                    </svg>
                  </div>
                  <div class="sl-premium-cam-cta__heading">
                    ${PremiumCamCTA.headerText}
                  </div>
                  <div class="sl-premium-cam-cta__subheading">
                    <span>
                      ${PremiumCamCTA.subheaderText}
                    </span>
                  </div>
                  <div class="sl-premium-cam-cta__links">
                    <div class="sl-premium-cam-cta__links__upgrade">
                      <a target="_blank" href="https://www.surfline.com/upgrade">
                        <button>
                          ${PremiumCamCTA.ctaText}
                        </button>
                      </a>
                    </div>
                    <div class="sl-premium-cam-cta__links__about">
                      <a class="sl-bold-text" target="_blank" href="https://www.surfline.com/premium-benefits">${PremiumCamCTA.learnMoreText}</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div id="sl-paused-overlay-container" class="sl-premium-overlay-cta" style="background-image: url(${stillUrl}); background-size: cover;">
              <div class="sl-premium-cam-cta__content__container">
                <div class="sl-paused-cam-cta__content">
                  <div class="sl-premium-cam-cta__heading">
                    Are you still watching?
                  </div>
                  <div class="sl-premium-cam-cta__links">
                    <div class="sl-paused-cam-cta__links__upgrade">
                      <a id="continue-button">
                        <button>
                          Continue Watching
                        </button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div id="sl-down-overlay-container" class="sl-premium-overlay-cta" style="background-image: url(${stillUrl}); background-size: cover;">
              <div class="sl-premium-cam-cta__content__container">
                <div class="sl-premium-cam-cta__content">
                  <div class="sl-premium-cam-cta__heading">
                    ${message}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="sl-overlay">
            <a id="sl-logo" target="_blank" href="https://www.surfline.com"><img src="./surfline_logo_no_tag_white_300x50.png" /></a>
          </div>
          <div id="sl-embed-bottom">
          <p>Surfline HD Cam. Click here to see <a class="sl-bold-text" href=${url} target="_blank">${name}</a> and all nearby cams live.</p>
          </div>
        </div>

        <script type="text/javascript" src="script.js"></script>
        <script>
        (function() {
          const player = new Player('${passedUrl}', ${isPremium}, ${status});
          player.init();
        })();
        </script>
      </body>
    </html>
        `;

  return html;
};

const saveHtml = (cameraId, html) => new Promise((resolve, reject) => {
  if (process.env.ENV !== 'LOCAL') {
    // Save to S3
    const key = `cam/${cameraId}.html`;
    logger.log('info', `Upload HTML to S3 for cam ${key}`);

    const s3 = new aws.S3();

    const params = {
      Body: html,
      Bucket: process.env.EMBED_BUCKET,
      Key: key,
      ContentType: 'text/html',
      ContentEncoding: 'utf8',
    };
    s3.putObject(params, (err, data) => {
      if (err) {
        logger.log('error', 'Error uploading HTML to S3');
        reject(err);
      } else {
        logger.log('info', data);
        resolve(data);
      }
    });
  } else {
    // Save locally
    logger.log('info', 'Save HTML to disk: src/static/dist/index.html.');

    fs.writeFile('src/static/dist/index.html', html, 'utf8', (err, data) => {
      if (err) {
        logger.log('error', err);
        reject(err);
      }
      logger.log('info', 'Output saved to: src/static/dist/index.html. Run `npm run server`.');
      resolve(data);
    });
  }
});

// eslint-disable-next-line no-unused-vars
const htmlGenerator = async (event) => {
  try {
    await dbContext.initMongoDB();
    const { cameraId } = JSON.parse(event.Records[0].Sns.Message);
    const cameraUrl = `${process.env.CAMERAS_API}/cameras/${cameraId}`;

    logger.log('info', `Get camera stream details from: ${cameraUrl}.`);
    const response = await fetch(cameraUrl);
    const data = await response.json();
    const spotsFetch = await fetch(`${process.env.SPOTS_API}/admin/spots/?select=name,status&camsOnly=true&cams=${cameraId}`);
    const results = await spotsFetch.json();
    // if there still isn't any result, name the spot and just link back to surfline.com
    if (!results[0] || results[0].status === 'DELETED') {
      const spotName = {
        name: 'Surfline',
      };
      const url = 'https://www.surfline.com';
      const html = await generateHtml(spotName, data, url);
      await saveHtml(cameraId, html);

      return;
    }

    const spotsUrl = `${process.env.SPOTS_API}/taxonomy?id=${results[0]._id}&type=spot`;
    logger.log('info', `Getting spot data from ${spotsUrl}`);
    const spotsResponse = await fetch(spotsUrl);
    const spotData = await spotsResponse.json();

    const { links } = spotData?.associated;
    const url = links?.find((obj) => obj.key === 'www').href;
    const html = await generateHtml(results[0], data, url);
    await saveHtml(cameraId, html);

    mongoose.disconnect();
    return;
  } catch (error) {
    logger.log('error', error.stack);
  }
};

export default htmlGenerator;
