import uploadJSONtoS3Bucket from '../utils/uploadJSONtoS3Bucket';
import appConfig from '../config';

const embedConfig = async (message) => {
  const { settings: { config, filename } } = message;
  await uploadJSONtoS3Bucket(
    { ...config },
    `cam-config/${filename}`,
    appConfig.CAM_EMBED_CONFIG_S3_BUCKET,
  );
};

export default embedConfig;
