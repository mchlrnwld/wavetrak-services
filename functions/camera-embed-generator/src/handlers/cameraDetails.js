import uploadJSONtoS3Bucket from '../utils/uploadJSONtoS3Bucket';
import { getCamera } from '../externals/cameras';
import { getSpotsByCams } from '../externals/spots';
import invalidateCloudFrontCache from '../utils/invalidateCloudFrontCache';
import config from '../config';
import logger from '../common/logger';

const cameraDetails = async ({ cameraId, settings }) => {
  logger.log('info', `cameraDetails.js - Get camera: ${cameraId}.`);
  const [camera, spots] = await Promise.all([
    getCamera(cameraId),
    getSpotsByCams(cameraId),
  ]);
  const {
    streamUrl,
    stillUrl,
    isDown,
    isPremium,
    isPrerecorded,
  } = camera;
  let spot = {};
  if (spots.length > 0) {
    spot = {
      id: spots[0]._id,
      name: spots[0].name,
    };
  }

  const camDetailsJSON = {
    cam: {
      streamUrl: settings && settings.noStreamUrl ? null : streamUrl,
      stillUrl,
      isDown,
      isPremium,
      isPrerecorded,
    },
    spot,
  };

  const camDetailFile = `cam-details/${cameraId}.json`;
  logger.log('info', `cameraDetails.js - invalidateCloudFrontCache - Dist: ${config.CAM_EMBED_DISTRIBUTION_ID}, Item: /${camDetailFile}`);
  logger.log('info', 'cameraDetails.js - uploadJSONtoS3Bucket');
  await Promise.all([
    invalidateCloudFrontCache({
      DistributionId: config.CAM_EMBED_DISTRIBUTION_ID,
      InvalidationBatch: {
        CallerReference: `${Date.now()}`,
        Paths: {
          Quantity: 1,
          Items: [
            `/${camDetailFile}`,
          ],
        },
      },
    }),
    uploadJSONtoS3Bucket(
      camDetailsJSON,
      camDetailFile,
      config.CAM_DETAILS_S3_BUCKET,
    ),
  ]);
};

export default cameraDetails;
