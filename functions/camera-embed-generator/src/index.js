import matchHandler from './handlers';

require('dotenv').config({ silent: true });

const handler = async (event) => {
  try {
    await matchHandler(event);
  } catch (err) {
    console.error(err);
  }
};

export default handler;
