require('dotenv').config({ silent: true });

export default {
  SPOTS_API: process.env.SPOTS_API,
  CAMERAS_API: process.env.CAMERAS_API,
  CAM_DETAILS_S3_BUCKET: process.env.CAM_DETAILS_S3_BUCKET,
  CAM_EMBED_CONFIG_S3_BUCKET: process.env.CAM_EMBED_CONFIG_S3_BUCKET,
  CAM_EMBED_DISTRIBUTION_ID: process.env.CAM_EMBED_DISTRIBUTION_ID,
};
