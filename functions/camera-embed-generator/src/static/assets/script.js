// If this script is updated, run 'npm run deploy' to deploy an updated version to S3

/* eslint-disable */
class Player {
  constructor(streamUrl, isPremium, isDown) {
    this.streamUrl = streamUrl;
    this.isPremium = isPremium;
    this.isDown = isDown;
  }

  init() {
    this.paused = false;
    this.readyToPlay = false;
    this.video = document.getElementById('sl-video');
    this.initHLS();
    if (this.isPremium && this.isDown) {
      document.getElementById('sl-down-overlay-container').style.display = 'block';
    } else if (this.isPremium && !this.isDown) {
      document.getElementById('sl-premium-overlay-container').style.display = 'block';
    } else if (this.isDown) {
      document.getElementById('sl-down-overlay-container').style.display = 'block';
    }
  }

  initHLS() {
    if (Hls.isSupported() && this.isPremium === false) {
      const config = {
        maxBufferLength: 3,
        maxBufferSize: 60 * 1000 * 1000,
        maxMaxBufferLength: 60,
        maxLoadingDelay: 8,
        liveSyncDurationCount: 3,
        liveMaxLatencyDurationCount: 4,
      };
      const hls = new Hls(config);
      this.initEventListeners(hls);

      hls.loadSource(this.streamUrl);
      hls.attachMedia(this.video);
      hls.on(Hls.Events.MANIFEST_PARSED, () => {
        this.readyToPlay = true;
      });
      this.play(hls);
    } else if (!Hls.isSupported() && this.isPremium === false) {
      // Any iOS device browser doesn't support HLS, have to attach src attribute directly
      this.video.src = this.streamUrl;
    }
  }

  // Play function to reload on play click
  // Forces video to catch up to stream head
  play(hls) {
    hls.loadSource(this.streamUrl);
    hls.attachMedia(this.video);
    this.video.play();

    document.getElementById('sl-paused-overlay-container').style.visibility = 'hidden';
    document.getElementById('sl-paused-overlay-container').style.display = 'none';
  }

  // Video Event Listeners
  initEventListeners(hls) {
    this.video.addEventListener('timeupdate', () => {
      // Pause after watching for 2 minutes
      if (this.video.currentTime >= 120) {
        this.video.pause();
        document.getElementById('sl-paused-overlay-container').style.visibility = 'visible';
        document.getElementById('sl-paused-overlay-container').style.display = 'block';
      }
    });
    this.video.addEventListener('pause', () => {
      // Stop XHR requests in the background when stream is paused
      hls.stopLoad();
      this.paused = true;
    });
    this.video.addEventListener('play', () => {
      if (this.paused) {
        this.video.currentTime = 0;
        this.play(hls);
        this.paused = false;
      }
    });
    this.video.addEventListener('click', () => {
      this.paused ? this.play(hls) : this.video.pause();
    });

    // Event listener for paused at 2 minute button
    this.continueButton = document.getElementById('continue-button');
    this.continueButton.addEventListener('click', () => {
      this.play(hls);
    });
  }
}
/* eslint-enable */
