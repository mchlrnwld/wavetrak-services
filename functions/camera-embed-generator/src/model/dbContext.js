import mongoose from 'mongoose';
import logger from '../common/logger';

mongoose.Promise = global.Promise;

function initMongoDB() {
  const connectionString = process.env.MONGO_CONNECTION_STRING_KBYG;
  const mongoDbConfig = { maxPoolSize: 50, useNewUrlParser: true };
  return new Promise((resolve, reject) => {
    try {
      mongoose.connect(connectionString, mongoDbConfig);
      mongoose.connection.once('open', () => {
        logger.info('MongoDB connected');
        resolve();
      });
      mongoose.connection.on('error', (error) => {
        logger.error({
          action: 'MongoDB:ConnectionError',
          error,
        });
        reject(error);
      });
    } catch (error) {
      logger.error({
        message: 'MongoDB:ConnectionError',
        stack: error,
      });
      reject(error);
    }
  });
}

module.exports.initMongoDB = initMongoDB;
