import mongoose from 'mongoose';

const spotSchema = new mongoose.Schema({
  name: String,
});

module.exports = mongoose.model('Spot', spotSchema, 'Spots');
