import mongoose from 'mongoose';

const camSchema = new mongoose.Schema({
  name: String,
});

module.exports = mongoose.model('Camera', camSchema, 'Cameras');
