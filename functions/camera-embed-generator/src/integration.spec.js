import {
  jest, it, describe, expect,
} from '@jest/globals';

import nock from 'nock';
import AWS from 'aws-sdk';
import handler from './index';
import mockEvent from './mocks/mockEvent.json';
import mockEventWithTopic from './mocks/mockEventWithTopic.json';
import logger from './common/logger';
import invalidateCloudFrontCache from './utils/invalidateCloudFrontCache';

import htmlGenerator from './handlers/htmlGenerator';

jest.mock('aws-sdk');

// Mocked as we are going to remove this
jest.mock('./handlers/htmlGenerator');

jest.mock('./common/logger');
jest.mock('./utils/invalidateCloudFrontCache');

const MOCK_S3_PUT = jest.fn(() => ({
  promise: () => Promise.resolve('SUCCESS'),
}));

const MOCK_DATE_TIME = 1631895032161;

describe('Integration tests', () => {
  Date.now = () => MOCK_DATE_TIME;
  console.error = jest.fn();

  jest.spyOn(logger, 'log').mockReturnValue();
  jest.spyOn(AWS, 'S3').mockReturnValue({ putObject: MOCK_S3_PUT });

  htmlGenerator.mockResolvedValue('DONE');
  invalidateCloudFrontCache.mockReturnValue(Promise.resolve('SUCCESS'));

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should successfully add cameraDetailsJson file to S3 and invalidate cache correctly when no spots found', async () => {
    const MOCK_CAMERA_RESPONSE = {
      streamUrl: 'test_streamUrl',
      stillUrl: 'test_stillUrl',
      isDown: false,
      isPremium: false,
      isPrerecorded: false,
    };
    const MOCK_CAMERA_ID = '5a21a1ba096c27001ac4f190';

    const MOCK_SPOTS_RESPONSE = [];

    nock('http://cameras-service.sandbox.surfline.com')
      .get(`/cameras/${MOCK_CAMERA_ID}`)
      .reply(200, MOCK_CAMERA_RESPONSE);

    nock('http://spots-api.sandbox.surfline.com')
      .get(`/admin/spots/?select=name&camsOnly=true&cams=${MOCK_CAMERA_ID}`)
      .reply(200, MOCK_SPOTS_RESPONSE);

    await handler(mockEvent);

    const EXPECTED_CAM_DETAILS_FILE = `cam-details/${MOCK_CAMERA_ID}.json`;
    const EXPECTED_CAM_EMBED_DISTRIBUTION_ID = 'XXXXXXXXXX';
    const EXPECTED_CLOUDFRONT_OBJECT = {
      DistributionId: EXPECTED_CAM_EMBED_DISTRIBUTION_ID,
      InvalidationBatch: {
        CallerReference: `${MOCK_DATE_TIME}`,
        Paths: {
          Quantity: 1,
          Items: [
            `/${EXPECTED_CAM_DETAILS_FILE}`,
          ],
        },
      },
    };

    const EXPECTED_BODY = {
      cam: {
        streamUrl: MOCK_CAMERA_RESPONSE.streamUrl,
        stillUrl: MOCK_CAMERA_RESPONSE.stillUrl,
        isDown: MOCK_CAMERA_RESPONSE.isDown,
        isPremium: MOCK_CAMERA_RESPONSE.isPremium,
        isPrerecorded: MOCK_CAMERA_RESPONSE.isPrerecorded,
      },
      spot: {},
    };

    const EXPECTED_CAM_DETAILS_DATA = {
      Key: EXPECTED_CAM_DETAILS_FILE,
      Body: JSON.stringify(EXPECTED_BODY),
      Bucket: 'sl-cameras-service-cam-details-bucket-test',
      CacheControl: 'no-cache',
      ContentType: 'application/json',
    };

    expect(logger.log.mock.calls[0]).toEqual(['info', `cameraDetails.js - Get camera: ${MOCK_CAMERA_ID}.`]);
    expect(logger.log.mock.calls[1]).toEqual(['info', `cameraDetails.js - invalidateCloudFrontCache - Dist: ${EXPECTED_CAM_EMBED_DISTRIBUTION_ID}, Item: /${EXPECTED_CAM_DETAILS_FILE}`]);
    expect(logger.log.mock.calls[2]).toEqual(['info', 'cameraDetails.js - uploadJSONtoS3Bucket']);

    expect(invalidateCloudFrontCache).toHaveBeenCalledWith(EXPECTED_CLOUDFRONT_OBJECT);
    expect(MOCK_S3_PUT).toHaveBeenCalledWith(EXPECTED_CAM_DETAILS_DATA);
    expect(console.error).toHaveBeenCalledWith(new Error('No matching handler to run for functions inputs'));
  });

  it('Should successfully add cameraDetailsJson file to S3 and invalidate cache correctly', async () => {
    const MOCK_CAMERA_RESPONSE = {
      streamUrl: 'test_streamUrl',
      stillUrl: 'test_stillUrl',
      isDown: false,
      isPremium: false,
      isPrerecorded: false,
    };
    const MOCK_CAMERA_ID = '5a21a1ba096c27001ac4f190';

    const MOCK_SPOT = { _id: 'test1_id', name: 'test1_name' };

    // Added two spots as it should pick out the first one
    const MOCK_SPOTS_RESPONSE = [MOCK_SPOT, { _id: 'test2_id', name: 'test2_name' }];

    nock('http://cameras-service.sandbox.surfline.com')
      .get(`/cameras/${MOCK_CAMERA_ID}`)
      .reply(200, MOCK_CAMERA_RESPONSE);

    nock('http://spots-api.sandbox.surfline.com')
      .get(`/admin/spots/?select=name&camsOnly=true&cams=${MOCK_CAMERA_ID}`)
      .reply(200, MOCK_SPOTS_RESPONSE);

    await handler(mockEvent);

    const EXPECTED_CAM_DETAILS_FILE = `cam-details/${MOCK_CAMERA_ID}.json`;
    const EXPECTED_CAM_EMBED_DISTRIBUTION_ID = 'XXXXXXXXXX';
    const EXPECTED_CLOUDFRONT_OBJECT = {
      DistributionId: EXPECTED_CAM_EMBED_DISTRIBUTION_ID,
      InvalidationBatch: {
        CallerReference: `${MOCK_DATE_TIME}`,
        Paths: {
          Quantity: 1,
          Items: [
            `/${EXPECTED_CAM_DETAILS_FILE}`,
          ],
        },
      },
    };

    const EXPECTED_BODY = {
      cam: {
        streamUrl: MOCK_CAMERA_RESPONSE.streamUrl,
        stillUrl: MOCK_CAMERA_RESPONSE.stillUrl,
        isDown: MOCK_CAMERA_RESPONSE.isDown,
        isPremium: MOCK_CAMERA_RESPONSE.isPremium,
        isPrerecorded: MOCK_CAMERA_RESPONSE.isPrerecorded,
      },
      spot: {
        id: MOCK_SPOT._id,
        name: MOCK_SPOT.name,
      },
    };

    const EXPECTED_CAM_DETAILS_DATA = {
      Key: EXPECTED_CAM_DETAILS_FILE,
      Body: JSON.stringify(EXPECTED_BODY),
      Bucket: 'sl-cameras-service-cam-details-bucket-test',
      CacheControl: 'no-cache',
      ContentType: 'application/json',
    };

    expect(logger.log.mock.calls[0]).toEqual(['info', `cameraDetails.js - Get camera: ${MOCK_CAMERA_ID}.`]);
    expect(logger.log.mock.calls[1]).toEqual(['info', `cameraDetails.js - invalidateCloudFrontCache - Dist: ${EXPECTED_CAM_EMBED_DISTRIBUTION_ID}, Item: /${EXPECTED_CAM_DETAILS_FILE}`]);
    expect(logger.log.mock.calls[2]).toEqual(['info', 'cameraDetails.js - uploadJSONtoS3Bucket']);

    expect(invalidateCloudFrontCache).toHaveBeenCalledWith(EXPECTED_CLOUDFRONT_OBJECT);
    expect(MOCK_S3_PUT).toHaveBeenCalledWith(EXPECTED_CAM_DETAILS_DATA);
    expect(console.error).toHaveBeenCalledWith(new Error('No matching handler to run for functions inputs'));
  });

  it('Should successfully add htmlFile & cameraDetailsJson file to S3 when only one spot is returned with noStreamUrl', async () => {
    const MOCK_CAMERA_RESPONSE = {
      stillUrl: 'test_stillUrl',
      isDown: false,
      isPremium: false,
      isPrerecorded: false,
    };
    const MOCK_CAMERA_ID = '5a21a1ba096c27001ac4f190';

    const MOCK_SPOT = { _id: 'test1_id', name: 'test1_name' };

    // Added two spots as it should pick out the first one
    const MOCK_SPOTS_RESPONSE = [MOCK_SPOT, { ...MOCK_SPOT, _id: 'test2' }];

    nock('http://cameras-service.sandbox.surfline.com')
      .get(`/cameras/${MOCK_CAMERA_ID}`)
      .reply(200, MOCK_CAMERA_RESPONSE);

    nock('http://spots-api.sandbox.surfline.com')
      .get(`/admin/spots/?select=name&camsOnly=true&cams=${MOCK_CAMERA_ID}`)
      .reply(200, MOCK_SPOTS_RESPONSE);

    await handler(mockEvent);

    const EXPECTED_CAM_DETAILS_FILE = `cam-details/${MOCK_CAMERA_ID}.json`;
    const EXPECTED_CAM_EMBED_DISTRIBUTION_ID = 'XXXXXXXXXX';
    const EXPECTED_CLOUDFRONT_OBJECT = {
      DistributionId: EXPECTED_CAM_EMBED_DISTRIBUTION_ID,
      InvalidationBatch: {
        CallerReference: `${MOCK_DATE_TIME}`,
        Paths: {
          Quantity: 1,
          Items: [
            `/${EXPECTED_CAM_DETAILS_FILE}`,
          ],
        },
      },
    };

    const EXPECTED_BODY = {
      cam: {
        stillUrl: MOCK_CAMERA_RESPONSE.stillUrl,
        isDown: MOCK_CAMERA_RESPONSE.isDown,
        isPremium: MOCK_CAMERA_RESPONSE.isPremium,
        isPrerecorded: MOCK_CAMERA_RESPONSE.isPrerecorded,
      },
      spot: {
        id: MOCK_SPOT._id,
        name: MOCK_SPOT.name,
      },
    };
    const EXPECTED_CAM_DETAILS_DATA = {
      Key: EXPECTED_CAM_DETAILS_FILE,
      Body: JSON.stringify(EXPECTED_BODY),
      Bucket: 'sl-cameras-service-cam-details-bucket-test',
      CacheControl: 'no-cache',
      ContentType: 'application/json',
    };

    expect(logger.log.mock.calls[0]).toEqual(['info', `cameraDetails.js - Get camera: ${MOCK_CAMERA_ID}.`]);
    expect(logger.log.mock.calls[1]).toEqual(['info', `cameraDetails.js - invalidateCloudFrontCache - Dist: ${EXPECTED_CAM_EMBED_DISTRIBUTION_ID}, Item: /${EXPECTED_CAM_DETAILS_FILE}`]);
    expect(logger.log.mock.calls[2]).toEqual(['info', 'cameraDetails.js - uploadJSONtoS3Bucket']);

    expect(invalidateCloudFrontCache).toHaveBeenCalledWith(EXPECTED_CLOUDFRONT_OBJECT);
    expect(MOCK_S3_PUT).toHaveBeenCalledWith(EXPECTED_CAM_DETAILS_DATA);
    expect(console.error).toHaveBeenCalledWith(new Error('No matching handler to run for functions inputs'));
  });

  it('Should successfully add config file to s3 when topic name is AWS_CAMERA_EMBED_CONFIG_CREATED_TOPIC', async () => {
    await handler(mockEventWithTopic);

    const EXPECTED_CONFIG = { test: 'test_config' };
    const EXPECTED_DATA = {
      Key: 'cam-config/test_filename',
      Body: JSON.stringify(EXPECTED_CONFIG),
      Bucket: 'sl-cameras-service-embed-config-bucket-test',
      CacheControl: 'no-cache',
      ContentType: 'application/json',
    };

    expect(MOCK_S3_PUT).toHaveBeenCalledWith(EXPECTED_DATA);
  });
});
