import AWS from 'aws-sdk';

const cloudfront = new AWS.CloudFront();

const invalidateCloudFrontCache = (params) => cloudfront.createInvalidation(params).promise();

export default invalidateCloudFrontCache;
