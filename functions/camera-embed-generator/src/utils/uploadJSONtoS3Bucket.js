import AWS from 'aws-sdk';

const uploadJSONtoS3Bucket = async (
  json,
  fileName,
  bucket,
) => {
  const s3 = new AWS.S3();
  const data = {
    Key: fileName,
    Body: JSON.stringify(json),
    Bucket: bucket,
    CacheControl: 'no-cache',
    ContentType: 'application/json',
  };

  await s3.putObject(data).promise();
};

export default uploadJSONtoS3Bucket;
