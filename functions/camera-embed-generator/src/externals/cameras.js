import fetch from 'node-fetch';
import config from '../config';

/* eslint-disable import/prefer-default-export */
export const getCamera = async (id) => {
  const url = `${config.CAMERAS_API}/cameras/${id}`;
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};
