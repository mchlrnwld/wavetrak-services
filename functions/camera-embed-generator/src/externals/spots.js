import fetch from 'node-fetch';
import config from '../config';

/* eslint-disable import/prefer-default-export */
export const getSpotsByCams = async (cams) => {
  const url = `${config.SPOTS_API}/admin/spots/?select=name&camsOnly=true&cams=${cams}`;
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};
