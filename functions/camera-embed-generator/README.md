# Cam embed generator

** Camera embed generator** is a lambda function which is triggered by an SNS topic. Once triggered the lambda function will create a static html page for that cam which can be embedded on a site via an iframe. That static html page is uploaded to S3 and served through a cloudfront distribution.

The embeds are designed for extreme performance and HA by not making any on request calls to Surfline services and relying 100% on AWS's ability to scale CloudFront. All on request logic is handled using vanilla JS code on the static html page, which makes the embed performant for client websites embedding the cam.

The sns topic is triggered when the configuration of a camera is updated and the lambda contains the following logic

1. if the event has a topic ID of `AWS_CAMERA_EMBED_CONFIG_CREATED_TOPIC` it will generate the an embed config file and save it into the `sl-cameras-service-embed-config-bucket-*` S3 bucket.

2. if the event does not have the above topic name it will:
    * It invalidates the cloudfront cache for the camera using the `CAM_EMBED_DISTRIBUTION_ID` env var and the filename `cam-details/${cameraId}.json`.
    * It generate a camera details file which uses data from the cameras api and spots api to generate a json object before saving it into `sl-cameras-service-cam-details-bucket-*` S3 bucket.
    * *Deprecated* - It also generates a html file for the embed itself using the config which is then saved into `sl-cameras-service-embed-cdn-*` S3 bucket. We now use a react component to do this and this part of the logic is being deprecated.

## Installation

Ensure you are using node 14.x and run

`npm install`

## Manually Deploying

Production deploys are handled via github actions but to deploy manually, for example to lower tiers you can follow these instructions.

Set your aws profile then simply run:

`ENV=sandbox|staging|prod npm run deploy`

Make sure to choose one of **sandbox**, **staging**, or **prod**.
Depending on the environment you choose, the deployment will update
the following:

| Command                   | Account       | Lambda name                            |
|---------------------------|---------------|----------------------------------------|
| `ENV=prod npm run deploy`    | surfline-prod | sl-camera-embed-generatior-**prod**       |
| `ENV=staging npm run deploy` | surfline-dev  | sl-camera-embed-generatior-**staging**    |
| `ENV=sandbox npm run deploy` | surfline-dev  | sl-camera-embed-generatior-**sandbox**    |

## Testing

This lambda can be tested locally by running

`npm run dev`

This will build the lambda and use the mock event.json file in the root of this dir to run the handler. If you want to run the integration tests you can run

`npm run test`

Which will run integrations tests which will mock out S3 and any third party dependencies.


## Monitoring

the infrastrcuture for this is managed via serverless and terraform. The terraform config for creating the buckets and sns topics are found in

https://github.com/Surfline/wavetrak-services/tree/master/services/cameras-service/infra/modules/embed-generator

Alert thresholds are configured in the [environment-specific configuration file](https://github.com/Surfline/wavetrak-services/blob/master/services/cameras-service/infra/environments/prod/main.tf).

New Relic developer credentials must be configured to successfully provision the alerts via terraform. For credential setup information, see the [New Relic Credentials ](https://github.com/Surfline/styleguide/tree/master/terraform#new-relic-credentials) section of the Wavetrak Styleguide.

For general information about configuring the New Relic resources, see [Configuring New Relic Alerts](https://github.com/Surfline/styleguide/tree/master/terraform#configuring-new-relic-alerts).