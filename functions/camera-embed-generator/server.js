import connect from 'connect';
import serveStatic from 'serve-static';
const server = connect();

server.use(serveStatic(__dirname + '/src/static/dist'));

server.listen(3000);

console.log("Open localhost:3000");
