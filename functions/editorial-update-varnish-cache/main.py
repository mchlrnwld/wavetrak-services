"""
Root service handler module for AWS Lambda function. 'METHOD_HANDLERS'
constant dict is used to map route requests to correct handler.
"""

import json
import logging

from lib import cache_clearing, config

logging.basicConfig(level=config.LOG_LEVEL)
logger = logging.getLogger()
logger.setLevel(config.LOG_LEVEL)


def handler(event, context):
    """Root path handler."""

    logger.info('Event: %s' % json.dumps(event, indent=4, sort_keys=True))

    return cache_clearing.handler(event, context)
