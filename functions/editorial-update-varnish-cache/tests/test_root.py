# -*- coding: utf-8 -*-

import json
from os.path import dirname, join

# from lib.cache_clearing import handler
from main import handler
from pytest import fixture

fixtures_path = join(dirname(__file__), 'fixtures')


@fixture
def sns_event_record():
    sns_event_record_path = join(fixtures_path, 'sns_event_record.json')
    with open(sns_event_record_path, 'r') as sns_event_record_json:
        return json.load(sns_event_record_json)


@fixture
def context():
    return {}


class TestHandler:
    def test_type_error_for_bad_params(self, context):
        try:
            handler('', context)
        except TypeError:
            pass
        else:
            pass
