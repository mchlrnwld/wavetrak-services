"""Wordpress client module"""

import json
import logging
from urllib.parse import urlparse

import requests  # type: ignore
from lib.config import LOG_LEVEL, WORDPRESS_API

# For logging at AWS.
root = logging.getLogger()
if root.handlers:
    for handler in root.handlers:
        root.removeHandler(handler)

logging.basicConfig(level=LOG_LEVEL)
logger = logging.getLogger(__name__)


def get_routes_for_post(post_id, msg):
    """Get all routes associated with a post

    Keyword arguments:
    post_id -- the id of the post being updated
    msg -- the SNS notification message

    SNS notification message format:
        {
          "id": 303,
          "action": "UPDATE_POST",
          "promotions": [
            "carousel",
            "curated"
          ],
          "categories": [
            "features",
            "surf-news"
          ],
          "series": [
            "ask-surfline",
            "good-to-epic"
          ],
          "tags": [
            "kelly-slater"
          ]
        }
    """

    wp_post = get_wp_data(post_id)

    logger.info(json.dumps({'post': wp_post, 'message': msg}))

    routes = []

    # FIXME: Include tests for this code

    # Post link
    url = urlparse(wp_post['permalink'])
    if url.path:
        routes.append(url.path)

    # Custom post API routes
    routes.append(r'/wp-json/sl/v1/posts\?id=%d' % post_id)
    routes.append(r'/wp-json/sl/v1/post/sponsor\?id=%d' % post_id)

    # News index + infinite scroll
    if 'promotions' in msg:
        for item in msg['promotions']:
            if item in ('curated', 'carousel'):
                routes.append('/surf-news')
                routes.append('/surf-news/all')
                routes.append(
                    r'/wp-admin/admin-ajax\.php.*be_ajax_load_more.*'
                )
                break

    # Category index + infinite scroll
    if 'categories' in msg:
        for item in msg['categories']:
            routes.append('/wp-json/sl/v1/taxonomy/posts/category')
            routes.append('/surf-news')
            routes.append('/surf-news/all')
            routes.append('/category/%s' % item)
            routes.append(
                r'/wp-admin/admin-ajax\.php.*query\[category_name\]='
                '%s.*' % item
            )

    # Tag index + infinite scroll
    if 'tags' in msg:
        for item in msg['tags']:
            routes.append('/tag/%s' % item)
            routes.append('/wp-json/sl/v1/taxonomy/posts/tags')
            routes.append(
                r'/wp-admin/admin-ajax\.php.*query\[tag\]=' '%s.*' % item
            )

    # Series index + infinite scroll
    if 'series' in msg:
        for item in msg['series']:
            routes.append('/series/%s' % item)
            routes.append('/wp-json/sl/v1/taxonomy/posts/series')
            routes.append(
                r'/wp-admin/admin-ajax\.php.*query\[series\]=' '%s.*' % item
            )

    # Contest API
    if 'contest' in msg:
        routes.append('wp-json/sl-contests/v1/contest')

    if 'contest_details' in msg:
        routes.append('wp-json/sl-contests/v1/details')

    if 'contest_entry' in msg:
        routes.append('wp-json/sl-contests/v1/entry')
        routes.append('wp-json/sl-contests/v1/entries')

    if 'contest_news' in msg:
        routes.append('wp-json/sl-contests/v1/news')

    # Storm Feed API
    if 'event' in msg:
        routes.append('wp-json/sl/v1/events')
        routes.append('wp-json/sl/v1/events/%d' % post_id)

    return routes


def get_wp_data(id):
    """Get data from WordPress matching type and id"""
    url = "%s%d" % (WORDPRESS_API, id)
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()
    return data
