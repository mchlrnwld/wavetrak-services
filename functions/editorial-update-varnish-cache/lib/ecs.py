"""Elasticsearch client module"""
# -*- coding: utf-8 -*-

import logging

import boto3  # type: ignore

# import json
import requests  # type: ignore
from lib.config import (
    ECS_CLUSTER_NAME,
    ECS_TARGET_CONTAINER,
    LOG_LEVEL,
    REGION,
)

# For logging at AWS.
root = logging.getLogger()
if root.handlers:
    for handler in root.handlers:
        root.removeHandler(handler)

logging.basicConfig(level=LOG_LEVEL)
logger = logging.getLogger(__name__)

# boto3 is very noisy at the log levels we'd like to support
# for the rest of our application
logging.getLogger('botocore').setLevel(logging.ERROR)
logging.getLogger('boto3').setLevel(logging.ERROR)

ECS = boto3.client('ecs', region_name=REGION)
EC2 = boto3.resource('ec2', region_name=REGION)


def ban_routes(routes):
    """Ban a list of routes in all varnish hosts"""
    hosts = get_varnish_hosts(ECS_CLUSTER_NAME, ECS_TARGET_CONTAINER)

    for route in routes:
        for host in hosts:
            # curl -i -X BAN --header 'X-Ban-Host: .*' \
            #     --header 'X-Ban-Url: 123456789' http://localhost:8082/
            url = 'http://%s/' % host
            headers = {}
            headers['X-Ban-Host'] = '.*'
            headers['X-Ban-Url'] = route

            res = requests.request('BAN', url, headers=headers)
            if res.status_code == 200:
                logger.info('Banned %s at %s' % (route, host))
            else:
                logger.warn(
                    'Error banning %s at %s, status code: %s'
                    % (route, host, res.status_code)
                )


def get_varnish_hosts(cluster, service):
    tasks = ECS.list_tasks(cluster=cluster, serviceName=service)
    task_descriptions = ECS.describe_tasks(
        cluster=cluster, tasks=tasks['taskArns']
    )

    hosts = []
    for task in task_descriptions['tasks']:
        logger.info(task)

        container_instances = ECS.describe_container_instances(
            cluster=cluster, containerInstances=[task['containerInstanceArn']]
        )

        for container in container_instances['containerInstances']:
            ip = get_ec2_priv_ip(container['ec2InstanceId'])
            for c in task['containers']:
                for nb in c['networkBindings']:
                    for c in nb:
                        if c == "hostPort":
                            port = str(nb[c])
                            host = '%s:%s' % (ip, port)
                            logger.info('Found varnish host %s' % host)
                            hosts.append(host)

    return hosts


def get_ec2_priv_ip(ec2_id):
    """Return ec2 private ip address"""
    ec2_instance = EC2.Instance(ec2_id)
    ip_addr = ec2_instance.private_ip_address

    logger.info('Found Ec2 Instance %s with ip %s' % (ec2_id, ip_addr))
    return ip_addr
