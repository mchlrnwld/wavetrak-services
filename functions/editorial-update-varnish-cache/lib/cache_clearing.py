"""Cache clearing handler module."""

import json
import logging

from lib.config import LOG_LEVEL
from lib.ecs import ban_routes
from lib.wordpress import get_routes_for_post

# For logging at AWS.
root = logging.getLogger()
if root.handlers:
    for root_handler in root.handlers:
        root.removeHandler(root_handler)

logging.basicConfig(level=LOG_LEVEL)
logger = logging.getLogger(__name__)


def handler(event, context):
    """cache_clearing handler clears varnish caches for the post specified"""
    logger.info(event)

    message = json.loads(event['Records'][0]['Sns']['Message'])

    if isinstance(message['id'], str) or isinstance(message['id'], int):
        post_routes = get_routes_for_post(int(message['id']), message)
    else:
        post_routes = 0

    if post_routes:
        ban_routes(post_routes)
