"""Config value constants"""

from os import environ
from os.path import dirname, join

from dotenv import load_dotenv

load_dotenv(join(dirname(__file__), '..', '.env'))

LOG_LEVEL = environ['LOG_LEVEL']

WORDPRESS_API = environ['WORDPRESS_API']
ECS_CLUSTER_NAME = environ['ECS_CLUSTER_NAME']
ECS_TARGET_CONTAINER = environ['ECS_TARGET_CONTAINER']
REGION = environ['REGION']
