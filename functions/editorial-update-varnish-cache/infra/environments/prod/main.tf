provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "editorial-update-varnish-cache/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "editorial-update-varnish-cache" {
  source = "../../"

  environment = "prod"
}
