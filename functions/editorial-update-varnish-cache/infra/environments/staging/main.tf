provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "editorial-update-varnish-cache/staging/terraform.tfstate"
    region = "us-west-1"
  }
}
