# -*- coding: utf-8 -*-

import json
from os.path import dirname, join

import mock  # type: ignore
from lib import wordpress_client
from pytest import fixture

fixtures_path = join(dirname(__file__), 'fixtures')


def post_303():
    post_303_path = join(fixtures_path, 'post_303.json')
    with open(post_303_path, 'r') as post_303_json:
        return json.load(post_303_json)


def post_304():
    post_304_path = join(fixtures_path, 'post_304.json')
    with open(post_304_path, 'r') as post_304_json:
        return json.load(post_304_json)


def media_275():
    media_275_path = join(fixtures_path, 'media_275.json')
    with open(media_275_path, 'r') as media_275_json:
        return json.load(media_275_json)


def users_1():
    users_1_path = join(fixtures_path, 'users_1.json')
    with open(users_1_path, 'r') as users_1_json:
        return json.load(users_1_json)


@fixture
def post_to_index_303():
    post_to_index_303_path = join(fixtures_path, 'post_to_index_303.json')
    with open(post_to_index_303_path, 'r') as post_to_index_303_json:
        return json.load(post_to_index_303_json)


@fixture
def post_to_index_304():
    post_to_index_304_path = join(fixtures_path, 'post_to_index_304.json')
    with open(post_to_index_304_path, 'r') as post_to_index_304_json:
        return json.load(post_to_index_304_json)


def get_wp_data(post_id, type):
    if post_id == 303 and type == 'posts':
        return post_303()
    elif post_id == 304 and type == 'posts':
        return post_304()
    elif post_id == 275 and type == 'media':
        return media_275()
    elif post_id == 1072 and type == 'media':
        return media_275()
    elif post_id == 1 and type == 'users':
        return users_1()
    return {}


def get_wp_data_with_filter(post_id, filter):
    return []


class TestHandler:
    @mock.patch('lib.wordpress_client.get_wp_data', get_wp_data)
    @mock.patch(
        'lib.wordpress_client.get_wp_data_with_filter', get_wp_data_with_filter
    )
    def test_post_303(self, post_to_index_303):
        result = wordpress_client.build_post(
            303, post_to_index_303['media'], '', ''
        )
        assert result['media'] == post_to_index_303['media']
        assert result['content']['body'] == (
            post_to_index_303['content']['body']
        )
        assert result['content']['title'] == (
            post_to_index_303['content']['title']
        )
        assert (
            result['content']['subtitle']
            == post_to_index_303['content']['subtitle']
        )
        assert result['tags'] == post_to_index_303['tags']
        assert result['author'] == post_to_index_303['author']
        assert result['promoted'] == post_to_index_303['promoted']
        assert result['premium'] == post_to_index_303['premium']
        assert result['targetTaxonomy'] == post_to_index_303['targetTaxonomy']
        assert result['permalink'] == post_to_index_303['permalink']
        assert result['externalLink'] == post_to_index_303['externalLink']
        assert result['externalSource'] == post_to_index_303['externalSource']
        assert result['newWindow'] == post_to_index_303['newWindow']

    @mock.patch('lib.wordpress_client.get_wp_data', get_wp_data)
    @mock.patch(
        'lib.wordpress_client.get_wp_data_with_filter', get_wp_data_with_filter
    )
    def test_post_304(self, post_to_index_304):
        result = wordpress_client.build_post(
            304, post_to_index_304['media'], '', ''
        )
        assert result['media'] == post_to_index_304['media']
        assert result['content']['body'] == (
            post_to_index_304['content']['body']
        )
        assert result['content']['title'] == (
            post_to_index_304['content']['title']
        )
        assert (
            result['content']['subtitle']
            == post_to_index_304['content']['subtitle']
        )
        assert result['author'] == post_to_index_304['author']
        assert result['promoted'] == post_to_index_304['promoted']
        assert result['premium'] == post_to_index_304['premium']
        assert result['targetTaxonomy'] == post_to_index_304['targetTaxonomy']
        assert result['permalink'] == post_to_index_304['permalink']
        assert result['externalLink'] == post_to_index_304['externalLink']
        assert result['externalSource'] == post_to_index_304['externalSource']
        assert result['newWindow'] == post_to_index_304['newWindow']
