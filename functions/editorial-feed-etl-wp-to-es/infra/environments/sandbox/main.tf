provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "editorial-feed-etl-wp-to-es-function/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

