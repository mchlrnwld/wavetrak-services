terraform {
  # we should keep required_version >= 1.0 unless we need to interface
  # with terraform code from libraries that only work with older versions
  required_version = ">= 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 2.70"
    }
    newrelic = {
      source  = "newrelic/newrelic"
      version = "~> 2.25.0"
    }
  }
}
