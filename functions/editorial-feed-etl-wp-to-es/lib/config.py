"""Config value constants"""

from os import environ
from os.path import dirname, join

from dotenv import load_dotenv

load_dotenv(join(dirname(__file__), '..', '.env'))

LOG_LEVEL = environ['LOG_LEVEL']

ELASTIC_SEARCH_CONNECTION_STRING = environ['ELASTIC_SEARCH_CONNECTION_STRING']
ES_HTTP_AUTH = environ['ES_HTTP_AUTH']
IS_ES_ON_AWS = environ['IS_ES_ON_AWS']
ES_INDEX = environ['ES_INDEX']
ES_TYPE = environ['ES_TYPE']

WORDPRESS_API = environ['WORDPRESS_API']
