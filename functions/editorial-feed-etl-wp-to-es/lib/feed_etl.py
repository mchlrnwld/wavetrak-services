"""Feed ETL handler module."""

import json
import logging

from lib.elasticsearch_client import (
    delete_post,
    post_exists,
    reset_order,
    update_post,
)
from lib.wordpress_client import build_forecast, build_post

logger = logging.getLogger(__name__)


def handler(event, context):
    """feed_etl handler updates or deletes posts in Elasticsearch"""
    logger.info(event)

    try:
        message = json.loads(event['Records'][0]['Sns']['Message'])

        if message['action'] == 'DELETE_POST':
            if 'forecast' in message:
                return delete_post(message['id'], 'forecast')
            else:
                return delete_post(message['id'], 'editorial')
        elif message['action'] in ('CREATE_POST', 'UPDATE_POST'):
            if 'forecast' in message:
                forecast = build_forecast(
                    message['id'],
                    message['forecastType'],
                    message['author'],
                    message['media'],
                )
                return update_post(forecast, 'forecast')
            elif 'post' in message:
                reset_order()
                if 'primary_category' not in message:
                    message['primary_category'] = None
                if 'review' in message['categories']:
                    if post_exists(message['id']):
                        return delete_post(message['id'], 'editorial')
                    else:
                        return None
                post = build_post(
                    message['id'],
                    message['media'],
                    message['primary_category'],
                    message['author'],
                )
                return update_post(post, 'editorial')
            else:
                return None

    except Exception as e:  # type: ignore # noqa F841
        logger.error('Invalid query parameters', exc_info=True)
        raise
