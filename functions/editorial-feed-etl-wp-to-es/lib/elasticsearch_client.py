"""Elasticsearch client module"""
# -*- coding: utf-8 -*-

import json
import logging

import boto3  # type: ignore
from elasticsearch import (  # type: ignore
    Elasticsearch,
    RequestsHttpConnection,
    compat,
    exceptions,
    serializer,
)
from lib.config import (
    ELASTIC_SEARCH_CONNECTION_STRING,
    ES_HTTP_AUTH,
    ES_INDEX,
    ES_TYPE,
    IS_ES_ON_AWS,
)
from requests_aws4auth import AWS4Auth  # type: ignore

logger = logging.getLogger(__name__)


def init_elasticsearch():
    """Initialize Elasticsearch client"""
    if IS_ES_ON_AWS:
        session = boto3.session.Session()
        credentials = session.get_credentials()

        awsauth = AWS4Auth(
            credentials.access_key,
            credentials.secret_key,
            session.region_name,
            'es',
            session_token=credentials.token,
        )

        es = Elasticsearch(
            [ELASTIC_SEARCH_CONNECTION_STRING],
            http_auth=awsauth,
            verify_certs=True,
            connection_class=RequestsHttpConnection,
            serializer=JSONSerializerPython2(),
        )
    else:
        es = Elasticsearch(
            [ELASTIC_SEARCH_CONNECTION_STRING],
            http_auth=ES_HTTP_AUTH,
            connection_class=RequestsHttpConnection,
            serializer=JSONSerializerPython2(),
        )

    return es


def delete_post(post_id, docType):
    """Delete post from Elasticsearch matching post_id"""

    # Allow ES exceptions to be raised uncaught
    es = init_elasticsearch()
    return es.delete(index=ES_INDEX, doc_type=docType, id=post_id)


def update_post(post, docType):
    """Create/update post in Elasticsearch"""

    # Allow ES exceptions to be raised uncaught
    es = init_elasticsearch()
    return es.index(
        index=ES_INDEX, doc_type=docType, id=post['articleId'], body=post
    )


def post_exists(post_id):
    """Returns boolean of whether or not the post exists"""

    # Allow ES exceptions to be raised uncaught
    es = init_elasticsearch()
    return es.exists(index=ES_INDEX, doc_type=ES_TYPE, id=post_id)


def reset_order():
    """Reset menuOrder for posts older than 20 days in Elasticsearch"""

    query = {
        'script': {'inline': 'ctx._source.menuOrder=null', 'lang': 'painless'},
        'query': {
            'constant_score': {
                'filter': {
                    'bool': {
                        'must': {'exists': {'field': 'menuOrder'}},
                        'filter': {
                            'bool': {
                                'must': {
                                    'range': {
                                        'updatedAt': {'lte': 'now-20d/d'}
                                    }
                                }
                            }
                        },
                    }
                }
            }
        },
    }

    # Allow ES exceptions to be raised uncaught
    es = init_elasticsearch()
    return es.update_by_query(
        index=ES_INDEX, doc_type=ES_TYPE, body=query, conflicts='proceed'
    )


# Include this class to override the default ES JSON serializer
# From https://github.com/elastic/elasticsearch-py/issues/374
class JSONSerializerPython2(serializer.JSONSerializer):
    """Override elasticsearch library serializer to ensure it encodes utf
    characters during json dump. See original at:
    https://github.com/elastic/elasticsearch-py/blob/master/elasticsearch/seri\
    alizer.py#L42
    A description of how ensure_ascii encodes unicode characters to ensure
    they can be sent across the wire as ascii can be found here:
    https://docs.python.org/2/library/json.html#basic-usage
    """

    def dumps(self, data):
        # don't serialize strings
        if isinstance(data, compat.string_types):
            return data
        try:
            return json.dumps(data, default=self.default, ensure_ascii=True)
        except (ValueError, TypeError) as e:
            raise exceptions.SerializationError(data, e)
