"""WordPress client module"""

import copy
import html
import json
import logging
import re
from operator import itemgetter
from typing import Any, Dict

import requests  # type: ignore
import unidecode
from lib.config import WORDPRESS_API

logger = logging.getLogger(__name__)

POST_TPL: Dict[str, Any] = {
    'articleId': '',
    'createdAt': '',
    'updatedAt': '',
    'sortDate': '',
    'premium': False,
    'promoted': [],
    'targetTaxonomy': [],
    'content': {'title': '', 'displayTitle': '', 'subtitle': '', 'body': ''},
    'auContent': {'title': '', 'displayTitle': '', 'subtitle': ''},
    'permalink': '',
    'media': {
        'type': '',
        'feed1x': '',
        'feed2x': '',
        'promobox1x': '',
        'promobox2x': '',
    },
    'author': {'iconUrl': '', 'name': ''},
    'sponsoredArticle': {
        'attributionText': '',
        'dfpCode': '',
        'showAttributionInFeed': False,
        'sponsorName': '',
    },
    'feedSuperheader': '',
    'tags': [],
    'suggestTags': [],
    'menuOrder': None,
    'auMenuOrder': None,
    'nzMenuOrder': None,
    'externalLink': '',
    'externalSource': None,
    'newWindow': False,
}

FORECAST_TPL: Dict[str, Any] = {
    'articleId': '',
    'createdAt': '',
    'updatedAt': '',
    'sortDate': '',
    'promoted': [],
    'premium': False,
    'targetTaxonomy': [],
    'keywords': [],
    'content': {'title': '', 'teaser': '', 'body': ''},
    'media': {
        'type': '',
        'feed1x': '',
        'feed2x': '',
        'promobox1x': '',
        'promobox2x': '',
    },
    'author': {'iconUrl': '', 'name': ''},
    'permalink': '',
}


def _build_post_target_taxonomy(wp_post):
    target_taxonomy = []
    for loc in wp_post['acf'].get('dynlocation', []):
        target_taxonomy.append(loc['value'])
    return target_taxonomy


def _build_post_content(wp_post):
    displayTitle = ''
    if 'display_title' in wp_post['acf']:
        displayTitle = html.unescape(wp_post['acf']['display_title'])
    return {
        'title': html.unescape(wp_post['title']['rendered']),
        'displayTitle': displayTitle,
        'subtitle': html.unescape(wp_post['acf']['subtitle']),
        'body': html.unescape(wp_post['excerpt']['rendered']),
    }


def _build_post_au_content(wp_post):
    title = ''
    subtitle = ''
    displayTitle = ''

    if 'au_title' in wp_post['acf']:
        title = html.unescape(wp_post['acf']['au_title'])
    if 'au_subtitle' in wp_post['acf']:
        subtitle = html.unescape(wp_post['acf']['au_subtitle'])
    if 'au_display_title' in wp_post['acf']:
        displayTitle = html.unescape(wp_post['acf']['au_display_title'])

    return {
        'title': title,
        'subtitle': subtitle,
        'displayTitle': displayTitle,
    }


def _build_post_permalink(wp_post):
    if 'externalLink' in wp_post['acf'] and 'externalUrl' in wp_post['acf']:
        externalLink = wp_post['acf']['externalLink']
        if externalLink == 'External URL':
            return wp_post['acf']['externalUrl']
    return wp_post['link']


def _build_post_tags(post_id, primary_category):
    categories = get_wp_data_with_filter(post_id, 'categories')
    series = get_wp_data_with_filter(post_id, 'series')

    tags = []

    # If no primary category is specified,
    # then empty string is passed in
    if primary_category == '':
        primary_category = None

    for item in categories + series:
        tags.append({'name': item['name'], 'url': item['link']})

    final_tags = sorted(tags, key=itemgetter('name'))

    if primary_category:
        for item in categories:
            # Title because API returns capitalized cats
            if item['name'] == primary_category.title():
                primary_cat = item
                final_tags = [
                    tag
                    for tag in final_tags
                    if tag['name'] != primary_category.title()
                ]
                # Add in primary_category at head of array
                final_tags.insert(
                    0,
                    {'name': primary_cat['name'], 'url': primary_cat['link']},
                )
    return final_tags


def _build_post_suggestions(post_id):
    categories = get_wp_data_with_filter(post_id, 'categories')
    series = get_wp_data_with_filter(post_id, 'series')
    post_tags = get_wp_data_with_filter(post_id, 'tags')

    tags = categories + series
    suggestTags = []

    for tag in tags:
        suggestTags.append(tag['name'])
    for tag in post_tags:
        suggestTags.append(tag['name'])

    return suggestTags


def build_post(post_id, media, primary_category, author):
    """Get post data from WP and assemble into ES doc format"""
    wp_post = get_wp_data(post_id, 'posts')
    promotions = get_wp_data_with_filter(post_id, 'promotion')

    post = copy.deepcopy(POST_TPL)

    post['articleId'] = post_id
    post['createdAt'] = wp_post['date_gmt']
    post['updatedAt'] = wp_post['modified_gmt']
    post['sortDate'] = wp_post['date_gmt']
    if 'premium' in wp_post['acf']:
        post['premium'] = wp_post['acf']['premium']
    else:
        post['premium'] = False

    if 'us_homepage_order' in wp_post['metadata']:
        post['menuOrder'] = wp_post['metadata']['us_homepage_order']

    if 'au_homepage_order' in wp_post['metadata']:
        post['auMenuOrder'] = wp_post['metadata']['au_homepage_order']

    if 'nz_homepage_order' in wp_post['metadata']:
        post['nzMenuOrder'] = wp_post['metadata']['nz_homepage_order']

    # Map promotions to promoted vars in ES mapping
    post['promoted'] = []
    for promotion in promotions:
        if promotion['name'] == 'Homepage Promo':
            post['promoted'].append('PROMO_BOX')
        if promotion['name'] == 'Editors Pick':
            post['promoted'].append('EDITORS_PICK')
        if promotion['name'] == 'Regional':
            post['promoted'].append('REGIONAL')
        if promotion['name'] == 'Trending':
            post['promoted'].append('TRENDING')
        if promotion['name'] == 'Curated':
            post['promoted'].append('CURATED')
        if promotion['name'] == 'Feed Large':
            post['promoted'].append('FEED_LARGE')
        if promotion['name'] == 'Feed Small':
            post['promoted'].append('FEED_SMALL')
        if promotion['name'] == 'AU Curated':
            post['promoted'].append('AU_CURATED')
        if promotion['name'] == 'AU Feed Large':
            post['promoted'].append('AU_FEED_LARGE')
        if promotion['name'] == 'AU Feed Small':
            post['promoted'].append('AU_FEED_SMALL')
        if promotion['name'] == 'AU Homepage Promo':
            post['promoted'].append('AU_PROMO_BOX')
        if promotion['name'] == 'NZ Curated':
            post['promoted'].append('NZ_CURATED')
        if promotion['name'] == 'NZ Feed Large':
            post['promoted'].append('NZ_FEED_LARGE')
        if promotion['name'] == 'NZ Feed Small':
            post['promoted'].append('NZ_FEED_SMALL')
        if promotion['name'] == 'NZ Homepage Promo':
            post['promoted'].append('NZ_PROMO_BOX')

    post['targetTaxonomy'] = _build_post_target_taxonomy(wp_post)
    post['content'] = _build_post_content(wp_post)
    post['auContent'] = _build_post_au_content(wp_post)
    post['permalink'] = _build_post_permalink(wp_post)
    post['media'] = media
    post['author'] = author
    post['tags'] = _build_post_tags(post_id, primary_category)
    post['suggestTags'] = _build_post_suggestions(post_id)

    if 'externalLink' in wp_post['acf']:
        post['externalLink'] = wp_post['acf']['externalLink']
    if 'externalSource' in wp_post['acf']:
        post['externalSource'] = wp_post['acf']['externalSource']
    if 'newWindow' in wp_post['acf']:
        post['newWindow'] = wp_post['acf']['newWindow']
    if 'feed_superheader' in wp_post['acf']:
        post['feedSuperheader'] = wp_post['acf']['feed_superheader']
    if 'sponsored_article' in wp_post['acf']:
        if 'attribution_text' in wp_post['acf']:
            post['sponsoredArticle']['attributionText'] = wp_post['acf'][
                'attribution_text'
            ]
        if 'sponsor_name' in wp_post['acf']:
            post['sponsoredArticle']['sponsorName'] = wp_post['acf'][
                'sponsor_name'
            ]
        if 'dfp_code' in wp_post['acf']:
            post['sponsoredArticle']['dfpCode'] = wp_post['acf']['dfp_code']
        if 'show_attribution_in_feed' in wp_post['acf']:
            post['sponsoredArticle']['showAttributionInFeed'] = wp_post['acf'][
                'show_attribution_in_feed'
            ]

    logger.debug('ES post: %s' % json.dumps(post, sort_keys=True, indent=2))

    return post


def slugify(text):
    text = unidecode.unidecode(text).lower()
    return re.sub(r'[\W_]+', '-', text)


def build_forecast(forecast_id, forecastType, author, media):
    forecast = copy.deepcopy(FORECAST_TPL)
    wpForecast = get_wp_data(forecast_id, forecastType)

    # Build forecast object
    forecast['articleId'] = forecast_id
    forecast['author'] = author
    forecast['createdAt'] = wpForecast['date_gmt']
    forecast['updatedAt'] = wpForecast['modified_gmt']
    forecast['sortDate'] = wpForecast['modified_gmt']
    forecast['promoted'] = (
        ['REGIONAL']
        if forecastType in ('sl_premium_analysis', 'sl_seasonal_forecast')
        else []
    )
    forecast['media'] = media
    # Deprecated
    # forecast['showInFeed'] = wpForecast['acf']['show_in_feed']
    forecast['premium'] = wpForecast['acf']['premium']
    # Build content
    forecast['content']['body'] = wpForecast['content']['rendered']
    forecast['content']['title'] = html.unescape(
        wpForecast['title']['rendered']
    )
    forecast['content']['teaser'] = wpForecast['content']['rendered'].split(
        '<div id="premium-divider"'
    )[0]

    forecast['targetTaxonomy'] = []
    locations = wpForecast['acf']['dynlocation']
    for location in locations:
        forecast['targetTaxonomy'].append(location['value'])

    forecast['keywords'] = []
    if forecastType == 'sl_premium_analysis':
        forecast['keywords'].append('shorttermforecast')
    if forecastType == 'sl_seasonal_forecast':
        forecast['keywords'].append('seasonalforecast')
    if forecastType == 'sl_realtime_forecast':
        forecast['keywords'].append('realtimeforecast')
    if forecastType == 'sl_local_news':
        forecast['keywords'].append('localnews')

    # Build permalink
    titleSlug = slugify(forecast['content']['title'])
    shards = wpForecast['link'].split('/')
    forecast['permalink'] = (
        shards[0]
        + '//'
        + shards[2]
        + '/'
        + shards[3]
        + '/'
        + 'local/'
        + titleSlug
        + '/'
        + str(forecast_id)
    )

    logger.debug(
        'ES forecast: %s' % json.dumps(forecast, sort_keys=True, indent=2)
    )

    return forecast


def get_wp_data(id, type):
    """Get data from WordPress matching type and id"""
    url = "%s/%s/%d" % (WORDPRESS_API, type, id)
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()
    return data


def get_wp_data_with_filter(id, type):
    """Get filtered data from WordPress matching type and id"""
    url = "%s/%s?post=%d" % (WORDPRESS_API, type, id)
    response = requests.get(url)
    response.raise_for_status()
    data = response.json()
    return data
