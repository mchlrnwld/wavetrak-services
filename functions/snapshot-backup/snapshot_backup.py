""" Automatic EBS snapshots function """
import collections
import datetime
import logging
import os
from typing import Any, Dict, List

import boto3

TAG_KEY = os.environ.get('TagKey', 'Backup')
TAG_VALUE = os.environ.get('TagValue', 'Hourly')
BACKUP_RETENTION = os.environ.get('BackupRetention', 7)

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


def lambda_handler(event, context):  # nopep8 pylint: disable=W0613,R0914
    """lambda entry point"""

    ec2_client = boto3.client('ec2')

    reservations = []
    des_inst_paginator = ec2_client.get_paginator('describe_instances')
    des_inst_iterator = des_inst_paginator.paginate(
        Filters=[
            {'Name': 'tag:%s' % TAG_KEY, 'Values': [TAG_VALUE]},
        ]
    )
    for page in des_inst_iterator:
        if 'Reservations' in page:
            reservations.extend(page['Reservations'])

    instances: List[Dict[str, Any]]
    instances = sum([[i for i in r['Instances']] for r in reservations], [])
    LOGGER.info("Found %d instances that need backing up", len(instances))
    to_tag = collections.defaultdict(list)
    for instance in instances:
        try:
            retention_days = [
                int(t.get('Value'))
                for t in instance['Tags']
                if t['Key'] == 'Retention'
            ][0]
        except IndexError:
            retention_days = int(BACKUP_RETENTION)
        LOGGER.debug(
            'Instance %s has %i days retention',
            instance['InstanceId'],
            retention_days,
        )
        for dev in instance['BlockDeviceMappings']:
            if dev.get('Ebs', None) is None:
                continue
            vol_id = dev['Ebs']['VolumeId']
            LOGGER.info(
                "Found EBS volume %s on instance %s",
                vol_id,
                instance['InstanceId'],
            )
            snap = ec2_client.create_snapshot(
                VolumeId=vol_id,
            )
            LOGGER.debug(
                'Created %s snapshot for instance %s',
                snap['SnapshotId'],
                instance['InstanceId'],
            )
            # setting instance ID in array under the snapshot
            to_tag[snap['SnapshotId']].append(instance['InstanceId'])
            # setting the retention time period in array under the snapshot
            to_tag[snap['SnapshotId']].append(retention_days)
            # setting the instance name in the array under the snapshot
            instance_name = [
                str(t.get('Value'))
                for t in instance['Tags']
                if t['Key'] == 'Name'
            ][0]
            to_tag[snap['SnapshotId']].append(instance_name)
            # setting the volume ID in array under the snapshot
            to_tag[snap['SnapshotId']].append(vol_id)
            LOGGER.debug(
                "Retaining snapshot %s of volume %s "
                "from instance %s for %d days",
                snap['SnapshotId'],
                vol_id,
                instance['InstanceId'],
                retention_days,
            )
    for snapshot in to_tag.keys():
        instanceid = to_tag[snapshot][0]
        retentiondaysforsnapshot = to_tag[snapshot][1]
        instance_name_tag = to_tag[snapshot][2]
        volume_id = to_tag[snapshot][3]
        todays_date = datetime.datetime.now()
        delete_date = todays_date + datetime.timedelta(
            days=retentiondaysforsnapshot
        )  # noqa
        todays_fmt = todays_date.strftime('%Y-%m-%d %I:%M:%S')
        delete_fmt = delete_date.strftime('%Y-%m-%d-%H-%M')
        LOGGER.debug(
            "Will delete snapshot for instance %s and volume %s on %s",
            instanceid,
            volume_id,
            delete_fmt,
        )

        name_tag = "%s - %s - %s" % (
            str(instance_name_tag),
            volume_id,
            todays_fmt,
        )
        ec2_client.create_tags(
            Resources=[snapshot],
            Tags=[
                {'Key': 'DeleteOn', 'Value': delete_fmt},
                {'Key': 'Name', 'Value': name_tag},
                {'Key': 'InstanceID', 'Value': instanceid},
            ],
        )
