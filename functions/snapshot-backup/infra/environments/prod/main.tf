provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "snapshot-backup/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "snapshot-backup" {
  source = "../../"

  environment = "prod"
}
