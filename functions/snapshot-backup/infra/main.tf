locals {
  application_title = "Snapshot Backup"
  function_name     = "snapshot-backup-${var.environment}-main"
}

resource "newrelic_alert_policy" "snapshot_backup_policy" {
  name                = local.application_title
  incident_preference = "PER_CONDITION_AND_TARGET"
}

resource "newrelic_nrql_alert_condition" "snapshot_backup_errors" {
  name                         = "Lambda Function Error: ${local.function_name}"
  policy_id                    = newrelic_alert_policy.snapshot_backup_policy.id
  type                         = "static"
  enabled                      = "true"
  value_function               = "single_value"
  violation_time_limit_seconds = "86400"

  nrql {
    query             = <<EOT
      SELECT sum(`provider.errors.Sum`)
      FROM   ServerlessSample
      WHERE  provider LIKE 'LambdaFunction%'
      AND    providerAccountId = '1500'
      FACET  `provider.functionName`
      WHERE  `provider.functionName` = '${local.function_name}'
    EOT
    evaluation_offset = "20"
  }

  critical {
    operator              = "above"
    # the function runs once a day, so even a single error is critical 
    threshold             = 0
    threshold_duration    = 300
    threshold_occurrences = "all"
  }
}
