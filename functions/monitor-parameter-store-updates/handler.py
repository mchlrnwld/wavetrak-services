import json
import logging
import os

import boto3  # type: ignore
import requests

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def get_last_modified_user(param_name):
    """
    Gets the ARN of the last user to modify a record.
    Arguments:
        param_name: name of the modified parameter.
    """

    ssm_client = boto3.client('ssm')
    record = ssm_client.describe_parameters(
        ParameterFilters=[{'Values': [f'{param_name}'], 'Key': 'Name'}]
    )
    last_modified_user = record['Parameters'][0]['LastModifiedUser']

    return last_modified_user


def lambda_handler(event, context):
    """
    Accepts event from Cloudwatch Events Bridge.
    Reformats and sends event to New Relic using the Events API.
    Arguments:
        event: Events Bridge event object.
        context: Lambda context.
    """
    EVENTS_ENDPOINT = (
        'https://insights-collector.newrelic.com/v1/accounts/356245/events'
    )

    try:
        last_modified_user = (
            get_last_modified_user(event['detail']['name'])
            if event['detail']['operation'] != 'delete'
            else 'unknown: parameter deleted'
        )

        payload = {
            'eventType': 'InfrastructureEvent',
            'eventDetail': 'ssmParameterUpdate',
            'account': event['account'],
            'time': event['time'],
            'parameter': event['detail']['name'],
            'operation': event['detail']['operation'],
            'last_modified_by': last_modified_user,
        }
        headers = {
            'Content-Type': 'application/json',
            'X-Insert-Key': f'{os.environ["NEWRELIC_EVENTS_KEY"]}',
        }

    except AttributeError as error:
        logger.warn('Can\'t assign event attribute to payload.')
        logger.warn(error)

    try:
        resp = requests.post(
            EVENTS_ENDPOINT, data=json.dumps(payload), headers=headers
        )
        logger.info(f'Sent the following event to New Relic: {resp.json()}')

    except IOError as error:
        logger.warn('Request to New Relic endpoint failed:')
        logger.warn(error)
