all: format lint type-check invoke-local deploy

format:
	@autoflake --remove-all-unused-imports --remove-unused-variables --ignore-init-module-imports --recursive --in-place .
	@isort -rc .
	@black .

.PHONY: format

lint:
	@flake8 .
	@black --check .

.PHONY: lint

type-check:
	@mypy .

.PHONY: type-check

invoke-local:
	@serverless invoke local -s ${ENV} -f log-update -d ./test.json

.PHONY: invoke-local

deploy:
	@serverless deploy -v -s ${ENV}

.PHONY: deploy

remove:
	@serverless remove -v -s ${ENV}

.PHONY: remove

doctoc:
	@docker run --entrypoint doctoc --rm -it -v "$(shell pwd)":/usr/src jorgeandrada/doctoc README.md --maxlevel 5 --title "## Table of Contents <!-- omit in toc -->"

.PHONY: doctoc
