provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "functions/process-forecast-platform-live-weather-weatherlink/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "process_forecast_platform_live_weather_weatherlink" {
  source = "../../"

  providers = {
    aws = aws
  }

  environment = "prod"
}
