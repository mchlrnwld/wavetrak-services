provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "functions/process-forecast-platform-live-weather-weatherlink/sandbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "process_forecast_platform_live_weather_weatherlink" {
  source = "../../"

  providers = {
    aws = aws
  }

  environment = "sandbox"
}
