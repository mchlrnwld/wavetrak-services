data "aws_sns_topic" "upload_topic" {
  name = "wt-live-weather-${var.environment}-weatherlink-upload-event"
}

module "sqs_with_sns_subscription" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-services.git//infrastructure/terraform/modules/aws/sqs-with-sns-subscription"

  providers = {
    aws.topic_provider = aws
  }

  application = var.application
  environment = var.environment
  queue_name  = var.queue_name
  topic_arns  = [data.aws_sns_topic.upload_topic.arn]
  company     = var.company
}

module "sns_topic" {
  source      = "git::ssh://git@github.com/Surfline/wavetrak-services.git//infrastructure/terraform/modules/aws/sns"
  environment = var.environment
  topic_name  = "${var.company}-${var.application}"
}
