resource "aws_kms_key" "wt_function_kms_key" {
  description             = "Function specific KMS key."
  key_usage               = "ENCRYPT_DECRYPT"
  is_enabled              = true
  enable_key_rotation     = false
  deletion_window_in_days = 30

  tags = {
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Function    = var.function_name
    Service     = "lambda"
    Terraform   = true
    Source      = "wavetrak-services/functions/process-forecast-platform-live-weather-weatherlink"
  }
}

resource "aws_kms_alias" "wt_function_kms_key_alias" {
  name          = "alias/${var.environment}/functions/${var.application}-${var.function_name}"
  target_key_id = aws_kms_key.wt_function_kms_key.key_id
}
