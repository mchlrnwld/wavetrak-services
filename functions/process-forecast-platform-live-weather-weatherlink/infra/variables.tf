variable "environment" {
  type = string
}

variable "queue_name" {
  default = "s3-events"
}

variable "company" {
  default = "wt"
}

variable "application" {
  default = "process-forecast-platform-live-weather-weatherlink"
}

variable "function_name" {
  description = "The function name"
  default     = "main"
}
