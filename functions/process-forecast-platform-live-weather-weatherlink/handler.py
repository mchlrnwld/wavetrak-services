import datetime
import logging
import os

from lib import config
from lib.sns_client import SNSClient

logger = logging.getLogger(
    'process-forecast-platform-live-weather-weatherlink'
)
logger.setLevel(logging.INFO)


def run(event, context):
    current_time = datetime.datetime.now().time()
    name = context.function_name
    region = os.environ["Region"]
    logger.info(
        f"Your function {name} in AWS region "
        f"{region} ran at {str(current_time)}"
    )

    updated_sensors = {'sensor_ids': ['12345', '67890']}

    sns_client = SNSClient()
    sns_client.send_sns_message(updated_sensors, config.AWS_SNS_TOPIC)
