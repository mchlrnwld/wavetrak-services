import json
import logging
from typing import Dict, List

import boto3  # type: ignore

logger = logging.getLogger(
    'process-forecast-platform-live-weather-weatherlink'
)
logger.setLevel(logging.INFO)


class SNSClient:
    def __init__(self):
        self.client = boto3.client('sns')

    def send_sns_message(self, message: Dict[str, List[str]], sns_topic: str):
        """
        Given a JSON SNS message, send it upstream to the SNS topic at AWS.
        """
        self.client.publish(
            TopicArn=sns_topic,
            Message=json.dumps({'default': json.dumps(message)}),
            MessageStructure='json',
        )
        logger.info(f'SNS notification sent to {sns_topic}')
