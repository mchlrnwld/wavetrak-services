import os

DRY_RUN = bool(os.environ["DRY_RUN"]) if os.environ.get("DRY_RUN") else False
EVENTS_ENDPOINT = (
    "https://insights-collector.newrelic.com/v1/accounts/356245/events"
)

AMAZON1_INSTANCE_PREFIXES = [
    "amzn-ami-",
    "aws-elasticbeanstalk-amzn-",
    "BuoyweatherLegacyDev",
    "bw-legacy-sandbox",
    "bw-sandbox",
    "bw-staging-snowflake",
    "prod-bw-wordpress-snowflake",
    "surfline-science-1.21.4-1572896267",
    "surfline-science-1.21.4-1572896267",
    "surfline-science-1.21.6-1588955260",
    "surfline-wowza",
    "WRF_cluster--2017-08-28",
]

AMAZON2_INSTANCE_PREFIXES = [
    "amzn2-ami-",
    "dev-science-dev-2",
    "prototype-inference-cluster-ami-v2",
    "surfline-science-models-db-1544723624",
    "surfline-science-models-db-1544723624",
    "surfline-science-weather-station-v1.0.0",
]

UBUNTU_INSTANCE_PREFIXES = ["Ubuntu", "ubuntu"]
