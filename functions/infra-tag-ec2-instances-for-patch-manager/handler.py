import json
import logging
import os
import re
from typing import Dict, List

import boto3  # type: ignore
import requests
from botocore.exceptions import ClientError  # type:ignore

from config import (
    AMAZON1_INSTANCE_PREFIXES,
    AMAZON2_INSTANCE_PREFIXES,
    DRY_RUN,
    EVENTS_ENDPOINT,
    UBUNTU_INSTANCE_PREFIXES,
)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def main(event: str = None, context: str = None) -> None:
    instance_info = get_instance_info()
    tag_results = tag_instances(instance_info)
    report_job_results(tag_results)


def get_instance_info() -> List[dict]:
    """
    Look up all the EC2 instances in the default region.
    Arguments:
        None
    Returns:
        instance_info: list of dicts with the keys: instance_id, image_id and
            image_name.
    """
    ec2_client = boto3.client('ec2')

    instance_info = []
    image_name_cache = {}

    try:
        paginator = ec2_client.get_paginator('describe_instances')
        response_iterator = paginator.paginate(
            Filters=[{'Name': 'instance-state-name', 'Values': ['running']}],
            PaginationConfig={'MaxItems': 1000, 'PageSize': 100},
        )
        for response in response_iterator:
            for record in response['Reservations']:
                instance_id = record.get('Instances', '')[0]['InstanceId']
                image_id = record.get('Instances')[0]['ImageId']

                # If the image_id is not in the cache, look it up using boto3
                if not record.get('image_id', '') in image_name_cache:
                    response = ec2_client.describe_images(ImageIds=[image_id])[
                        'Images'
                    ]

                    image_name_cache[image_id] = (
                        response[0]['Name'] if response else 'no_image_found'
                    )

                    if image_name_cache[image_id] == 'no_image_found':
                        logger.warn(f'Image {image_id} could not be found')

                new_dict_entry = dict(
                    instance_id=instance_id,
                    image_id=image_id,
                    image_name=image_name_cache[image_id],
                )

                instance_info.append(new_dict_entry)

    except ClientError as error:
        logger.error(error)

    return instance_info


def tag_instances(instance_info: List[dict]) -> Dict[str, str]:
    """
    Tags all the instances that it receives as input.
    Arguments:
        instance_info: dict with keys: instance_id, image_id and image_name.
    Returns:
        untagged_instances: dict of untagged instances: {instance_id: image_id}
    """
    ec2_client = boto3.client('ec2')

    untagged_instances = {}

    for instance in instance_info:
        try:
            tag_value = ''
            if re.search(
                '|'.join(AMAZON1_INSTANCE_PREFIXES),
                instance.get('image_name', ''),
            ):
                tag_value = f'{os.environ["ENV"]}-patch-group-amnz-1'
            elif re.search(
                '|'.join(AMAZON2_INSTANCE_PREFIXES),
                instance.get('image_name', ''),
            ):
                tag_value = f'{os.environ["ENV"]}-patch-group-amnz-2'
            elif re.search(
                '|'.join(UBUNTU_INSTANCE_PREFIXES),
                instance.get('image_name', ''),
            ):
                tag_value = f'{os.environ["ENV"]}-patch-group-ubuntu'
            elif instance.get('image_name', '') == 'no_image_found':
                untagged_instances[instance['instance_id']] = instance[
                    'image_id'
                ]
            else:
                untagged_instances[instance['instance_id']] = instance.get(
                    'image_id', ''
                )
            if tag_value:
                ec2_client.create_tags(
                    DryRun=DRY_RUN,
                    Resources=[
                        instance.get('instance_id'),
                    ],
                    Tags=[
                        {'Key': 'Patch Group', 'Value': tag_value},
                    ],
                )

        except ClientError as error:
            logger.warn(
                'If DRY_FLAG is set, the following exception is expected:'
            )
            logger.error(error)

    return untagged_instances


def report_job_results(untagged_instances: Dict[str, str]) -> List[dict]:
    """
    Sends information about the patch store job to the New Relic Events API.
    Arguments:
        untagged_instances: dict of untagged instances.
    Returns: list of sent payload dict objects.
    """
    sent_payloads = []
    if not DRY_RUN:
        for instance_id, image_id in untagged_instances.items():
            try:
                payload = {
                    'eventType': 'InfrastructureEvent',
                    'eventDetail': 'ssmPatchStoreJobDetails',
                    'environment': f'{os.environ["ENV"]}',
                    'untaggedInstanceId': instance_id,
                    'correspondingImageId': image_id,
                }

                headers = {
                    'Content-Type': 'application/json',
                    'X-Insert-Key': f'{os.environ["NEWRELIC_EVENTS_KEY"]}',
                }
                resp = requests.post(
                    EVENTS_ENDPOINT, data=json.dumps(payload), headers=headers
                )
                logger.info(
                    f'Sent the following event to New Relic: {resp.json()}'
                )
                sent_payloads.append(payload)

            except IOError as error:
                logger.warn('Request to New Relic endpoint failed:')
                logger.warn(error)

            logger.info('=' * 80)
            logger.info('Job summary:')
            logger.info(f'untagged_instances: {untagged_instances}')

    return sent_payloads


if __name__ == '__main__':
    main()
