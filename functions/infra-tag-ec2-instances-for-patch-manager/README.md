# Tag EC2 Instances For Patch Manager

This Lambda function looks up AMI names for all the EC2 instances in a given environment, and tags them with a tag key ("Patch Group") and the corresponding image name value.


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents <!-- omit in toc -->

- [Components overview:](#components-overview)
- [Serverless Framework](#serverless-framework)
  - [Installing](#installing)
  - [serverless.yml](#serverlessyml)
    - [service](#service)
    - [frameworkVersion](#frameworkversion)
    - [provider](#provider)
    - [plugins](#plugins)
    - [functions](#functions)
- [Deploying to AWS](#deploying-to-aws)
  - [Python Requirements](#python-requirements)
- [Running Locally](#running-locally)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Components overview:

- Cloudwatch event rules: configured in [serverless.yml](#serverlessyml).
- Lambda function: configured in [serverless.yml](#serverlessyml).

## Serverless Framework

This Lambda function is managed and provisioned by the Serverless Framework.

### Installing

To install the Serverless Framework, you must have Node version >=10 and npm installed. Once both are installed, simply run the following command:

```
npm install
```

This will install Serverless and all required plugins.

### serverless.yml

Serverless Framework uses this file to create and deploy a lambda functions to any given AWS account. The top-level sections of the serverless.yml are:

#### service

This describes the service associated with the Lambda function, for example: tag-ec2-instances. The service name will be used to create the Cloudformation template that then creates the Lambda function.

#### frameworkVersion

Version of Serverless Framework being used.

#### provider

This section is where all AWS-related information is declared. For example: the AWS region, the AWS account associated with the Lambda, and any IAM role permissions. It also contains information about the Python version.

#### plugins
This section is where all 3rd-party Serverless plugins are declared. For example, the serverless-python-requirements allows you to easily package and deploy a Python Lambda with 3rd-party dependencies.

#### functions
This section is where all Lambda functions are declared. Do note that many Lambda functions can be declared here, not just one. You can specify the name of the function, the handler to use, any associated infrastructure (Cloudwatch, SNS, etc) and more.

## Deploying to AWS

Included in the `Makefile` is `make ENV=env-name deploy`, which allows you to easily deploy the lambda to the AWS account and environment of choice. The desired environment must be passed in. For example:
```sh
make ENV=dev deploy
```
When deploying, Serverless will look for the AWS credentials associated with the environment. For example, if you want to deploy to `dev`, using `make ENV=dev deploy`, it will look for the AWS credentials associated with `dev` in `~/.aws/credentials`.

NOTE: There are 3 relevant environments for this lambda function: `dev`, `legacy` and `prod`. So it's worth noting that `sandbox` and `staging` are both contained in the same environment: `dev`.

### Python Requirements

Before deploying, all 3rd-party Python dependencies required for runtime must be manually added to `requirements.txt`. The `serverless-python-requirements` plugin references `requirements.txt` to install and package Python dependencies.

All dependencies required for runtime are installed via pip in `environment.yml`. The purpose of installing via `pip` for runtime dependencies is to ensure that the `serverless-python-requirements` can find the same version that is used for local development.

**NOTE:** Manually adding dependencies to `requirements.txt` is temporary practice while a more automated solution can be found.

## Running Locally

To run the Lambda locally and simulate an invocation, run the following command:
```
make ENV=env-name invoke-local
```
Before running this command, it is recommended that you create and activate the Conda environment listed in `environment.devenv.yml`. The Conda environment can be created by executing the following command:

```
conda devenv
```
Conda devenv will generate an `environment.yml` file, which can then be used to create a virtual conda environment using `tag-ec2-instances`.
