INSTANCE_INFO = [
    {
        "instance_id": "i-830080d8",
        "image_id": "ami-687b4f2d",
        "image_name": "amzn-ami-pv-2013.09.0.x86_64-ebs",
    },
    {
        "instance_id": "i-9c32ffc6",
        "image_id": "ami-687b4f2d",
        "image_name": "amzn-ami-pv-2013.09.0.x86_64-ebs",
    },
    {
        "instance_id": "i-328b2cf9",
        "image_id": "ami-d114f295",
        "image_name": "amzn-ami-hvm-2015.03.0.x86_64-gp2",
    },
    {
        "instance_id": "i-062b11c6",
        "image_id": "ami-f8523d98",
        "image_name": "bw-legacy-sandbox",
    },
]

INSTANCE_INFO_WITH_MISSING_AMI_NAMES = [
    {
        "instance_id": "i-4213bf81",
        "image_id": "ami-7295e512",
        "image_name": "no_image_found",
    },
    {
        "instance_id": "i-98f28ddd",
        "image_id": "ami-33b5f453",
        "image_name": "no_image_found",
    },
    {
        "instance_id": "i-bda99f0f",
        "image_id": "ami-cdeb81ad",
        "image_name": "bw-sandbox",
    },
    {
        "instance_id": "i-34646281",
        "image_id": "ami-df6a8b9b",
        "image_name": "ubuntu/images/hvm-ssd/ubuntu"
        "-trusty-14.04-amd64-server-20150325",
    },
]

UNTAGGED_INSTANCES = {
    "i-4213bf81": "ami-7295e512",
    "i-98f28ddd": "ami-33b5f453",
    "i-affe2f1a": "ami-2f116c4f",
}

NEW_RELIC_EVENTS_URL = (
    "https://insights-collector.newrelic.com/v1/accounts/356245/events"
)

NEW_RELIC_RESPONSE = {
    "success": True,
    "uuid": "253461fb-0001-b000-0000-017b1546dffd",
}
