import os
from typing import Callable, Generator

import boto3  # type: ignore
import pytest  # type: ignore
import requests_mock  # type: ignore
from moto import mock_ec2  # type: ignore

from fixtures.ec2_instance_responses import (  # type: ignore
    INSTANCE_INFO_WITH_MISSING_AMI_NAMES,
    NEW_RELIC_EVENTS_URL,
    NEW_RELIC_RESPONSE,
    UNTAGGED_INSTANCES,
)
from handler import get_instance_info, report_job_results, tag_instances


@pytest.fixture(autouse=True)
def aws_variables(monkeypatch) -> None:
    monkeypatch.setenv('AWS_DEFAULT_REGION', 'us-west-1')
    monkeypatch.setenv('AWS_ACCESS_KEY_ID', 'testing')
    monkeypatch.setenv('AWS_SECRET_ACCESS_KEY', 'testing')
    monkeypatch.setenv('AWS_SECURITY_TOKEN', 'testing')
    monkeypatch.setenv('AWS_SESSION_TOKEN', 'testing')
    monkeypatch.setenv('ENV', 'testing')
    monkeypatch.setenv('NEWRELIC_EVENTS_KEY', 'testing')


@pytest.fixture
def create_ec2_images_and_instances() -> Generator:
    with mock_ec2():
        client = boto3.client('ec2')
        ami_names = [
            'amzn-ami-hvm-2016.03.0.x86_64-gp2',
            'amzn-ami-hvm-2017.09.1.20180307-x86_64-gp2',
            'aws-elasticbeanstalk-amzn-2018.03.20.x86_64'
            '-nodejs-hvm-202006290733',
            'surfline-science-1.21.6-1588955260',
            'amzn2-ami-ecs-hvm-2.0.20200805-x86_64-ebs',
            'ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-20180109',
        ]

        for ami_name in ami_names:
            image_info = client.register_image(Name=ami_name)
            client.run_instances(
                ImageId=image_info['ImageId'], MinCount=1, MaxCount=1
            )

        yield client


def test_get_instance_info(create_ec2_images_and_instances: Callable) -> None:
    instances = get_instance_info()

    assert len(instances) == 6

    for instance in instances:
        assert 'instance_id' in instance.keys()
        assert 'image_id' in instance.keys()
        assert 'image_name' in instance.keys()


def test_tag_instances(create_ec2_images_and_instances: Callable) -> None:
    # Ideally we'd build `instance_info` ourselves here but since we're using
    # moto, it's more complicated than just using a dict of values
    instance_info = get_instance_info()
    tag_instances(instance_info)
    client = boto3.client('ec2')
    paginator = client.get_paginator('describe_instances')
    response_iterator = paginator.paginate(
        Filters=[{'Name': 'instance-state-name', 'Values': ['running']}],
        PaginationConfig={'MaxItems': 1000, 'PageSize': 100},
    )

    tagged_instance_count = sum(
        [
            1
            if tag['Value']
            in [
                'testing-patch-group-amnz-1',
                'testing-patch-group-amnz-2',
                'testing-patch-group-ubuntu',
            ]
            else 0
            for response in response_iterator
            for record in response['Reservations']
            for instance in record['Instances']
            for tag in instance.get('Tags', '')
            if tag['Key'] == 'Patch Group'
        ]
    )

    assert tagged_instance_count == 6


def test_tag_instances_with_missing_ami_names() -> None:
    """
    Test image tagging WITH untagged instances.
    """
    untagged_instances = tag_instances(INSTANCE_INFO_WITH_MISSING_AMI_NAMES)

    assert 'i-4213bf81' in untagged_instances.keys()
    assert untagged_instances['i-4213bf81'] == 'ami-7295e512'
    assert len(untagged_instances) == 2
    assert 'i-bda99f0f' not in untagged_instances.keys()


def test_report_job_results() -> None:
    """
    Test sending results to New Relic
    """
    with requests_mock.Mocker() as m:
        m.post(NEW_RELIC_EVENTS_URL, json=NEW_RELIC_RESPONSE)
        response = report_job_results(UNTAGGED_INSTANCES)

        assert m.call_count == 3
        assert response[0]['eventDetail'] == 'ssmPatchStoreJobDetails'
        assert response[0]['environment'] == os.environ['ENV']
        assert response[0]['untaggedInstanceId'] == 'i-4213bf81'
        assert response[0]['correspondingImageId'] == 'ami-7295e512'
        assert len(response[0]) == 5
        assert len(response) == 3
