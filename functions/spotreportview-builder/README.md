# Admin Tools KBYG Cache Service

## Table of contents <!-- omit in toc -->

- [Summary](#summary)
- [Jenkins deployment](#jenkins-deployment)
- [Development](#development)
  - [Installation](#installation)
  - [Unit tests and linting](#unit-tests-and-linting)
  - [Integration tests](#integration-tests)
    - [Publish a single view](#publish-a-single-view)
    - [Create a batch](#create-a-batch)
    - [Update from a camera](#update-from-a-camera)

## Summary

A **SpotReportView** aggregates multiple data sources into a view that can be efficiently queried.

This view is stored in the KBYG Mongo Database and consumed by the KBYG Product API.

This lambda function is named sl-kbyg-spotreportview-builder-{env}.

## Jenkins deployment

To deploy this application as a lambda function, run the Lambda Functions Pipeline in Github in the desired environment (e.g. prod, sandbox or staging).

Use the following input parameters:

- Use workflow from: `master`
- The name of the function to be deployed: `sportreportview-builder`
- The environment where to deploy: `sandbox` | `staging` | `prod`
- The branch name, tag, or commit sha to deploy

Once the job is completed, the lambda and all associated events, topic subscriptions, etc will be updated in the environment you ran the job for.

## Development

### Installation

This lambda function is meant to be run using NodeJS `v14.x.x`.

```bash
cp .env.sample .env
npm install
```

### Unit tests and linting

Execute the next commands to run tests and the code linter.

```bash
npm run test
npm run lint
npm run type-check
```

### Integration tests

#### Publish a single view

Invoke the function locally to build a spotReportView and save the updated data to the database:

```bash
cp event.publish.sample.json event.json
npm run start
```

You can execute the AWS command directly with this parameters:

```bash
aws lambda invoke \
    --function-name sl-kbyg-spotreportview-builder-sandbox \
    --payload $(echo $(cat ./event.publish.sample.json) | base64) \
    ../output.json
```

Or invoke with Serverless:

```bash
ENV=sandbox serverless invoke --function main --stage sandbox --data "$(cat ./event.publish.sample.json)"
```

#### Create a batch

Invoke the function locally to build a batch of spotReportView on the environment configured in the .env file:

```bash
cp event.check.sample.json event.json
npm run start
```

To do the same with the AWS command:

```bash
aws lambda invoke \
    --function-name sl-spotreportview-builder-sandbox \
    --payload $(cat ./event.check.sample.json | base64) \
    ../output.json
```

Or invoke with Serverless:

```bash
ENV=sandbox serverless invoke --function main --stage sandbox --data "$(cat ./event.check.sample.json)"
```

#### Update from a camera

Invoke the function locally to update a spotReportView from a camera id:

```bash
cp event.cam.sample.json event.json
npm run start
```

The AWS command version:

```bash
aws lambda invoke \
    --function-name sl-spotreportview-builder-sandbox \
    --payload $(cat ./event.cam.sample.json | base64) \
    ../output.json
```

Or invoke with Serverless:

```bash
ENV=sandbox serverless invoke --function main --stage sandbox --data "$(cat ./event.cam.sample.json)"
```

In all cases that you invoke using `aws lambda`, the response should look like this:

```json
{
  "StatusCode": 200,
  "ExecutedVersion": "$LATEST"
}
```

## Monitoring

This Lambda has a [New Relic alert policy](https://one.newrelic.com/launcher/nrai.launcher?platform%5BaccountId%5D=356245&pane=eyJuZXJkbGV0SWQiOiJhbGVydGluZy11aS1jbGFzc2ljLnBvbGljaWVzIiwibmF2IjoiUG9saWNpZXMiLCJzZWxlY3RlZEZpZWxkIjoidGhyZXNob2xkcyIsInBvbGljeUlkIjoiMjQ4MzMyIiwiY29uZGl0aW9uSWQiOiIxNjU3ODQ4NyJ9&sidebars%5B0%5D=eyJuZXJkbGV0SWQiOiJucmFpLm5hdmlnYXRpb24tYmFyIiwibmF2IjoiUG9saWNpZXMifQ) that monitors the invocation error count.

### Configuration

The alert is managed in the /infra directory with Terraform and should be configured at a threshold that indicates a diminished functionality state. Developers should proactively update this threshold as error patterns change over the life of the Lambda function.

Because of reporting delays from AWS to New Relic, the `aggregation window` value should be set to 20 minutes.
