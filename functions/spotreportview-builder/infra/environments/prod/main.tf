provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/prod/common/NEW_RELIC_ACCOUNT_ID_FOR_SERVERLESS"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/prod/common/NEW_RELIC_API_KEY_FOR_SERVERLESS"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "kbyg-spotreportview-builder/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "spotreportview-lambda" {
  source = "../../"

  environment = "prod"
}
