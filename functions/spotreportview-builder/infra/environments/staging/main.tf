provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "kbyg-spotreportview-builder/staging/terraform.tfstate"
    region = "us-west-1"
  }
}
