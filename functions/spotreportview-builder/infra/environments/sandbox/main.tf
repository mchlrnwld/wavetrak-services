
provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "kbyg-spotreportview-builder/sandbox/terraform.tfstate"
    region = "us-west-1"
  }
}
