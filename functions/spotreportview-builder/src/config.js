const config = {
  scienceDataService: process.env.SCIENCE_DATA_SERVICE,
  spotsAPI: process.env.SPOTS_API,
  reportsAPI: process.env.REPORTS_API,
  camerasAPI: process.env.CAMERAS_API,
  conditionRatingService: process.env.CONDITION_RATING_SERVICE,
  tideAPI: process.env.TIDE_API,
  spotReportViewBuilderFunction: process.env.SPOTREPORTVIEW_BUILDER_FUNCTION,
  maxSaveFailures: process.env.MAX_SAVE_FAILURES,
};

export default config;
