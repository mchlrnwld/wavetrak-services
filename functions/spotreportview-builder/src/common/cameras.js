import fetch from 'node-fetch';
import config from '../config';

// istanbul ignore next
// eslint-disable-next-line import/prefer-default-export
export const getCameras = (cameraIds) =>
  Promise.all(
    cameraIds.map(async (cameraId) => {
      const url = `${config.camerasAPI}/cameras/${cameraId}`;
      const response = await fetch(url);
      const result = await response.json();

      if (response.status === 200) return result;
      throw result;
    }),
  );
