import { request, gql } from 'graphql-request';
import currentHour from './currentHour';
import config from '../config';

const spotReportViewForecastsQuery = gql`
  query SpotReportViewForecasts($pointOfInterestId: String!, $timestamp: Int!) {
    surfSpotForecasts(pointOfInterestId: $pointOfInterestId) {
      # Need Celsius for consistent min/max values
      sst(temperature: "C") {
        data {
          timestamp
          sst {
            temperature
          }
        }
      }
      surf(start: $timestamp, end: $timestamp, waveHeight: "ft", corrected: true) {
        data {
          timestamp
          surf {
            breakingWaveHeightMin
            breakingWaveHeightMax
          }
        }
      }
      swells(start: $timestamp, end: $timestamp, waveHeight: "ft") {
        data {
          timestamp
          swells {
            components {
              event
              height
              period
              direction
              spread
            }
          }
        }
      }
      weather(start: $timestamp, end: $timestamp, temperature: "F") {
        data {
          timestamp
          weather {
            conditions
            temperature
          }
        }
      }
      wind(start: $timestamp, end: $timestamp, windSpeed: "knot") {
        data {
          timestamp
          wind {
            speed
            direction
            directionType
          }
        }
      }
    }
  }
`;

// istanbul ignore next
// eslint-disable-next-line import/prefer-default-export
export const getForecasts = async (spot, now, utcOffset) => {
  const { pointOfInterestId } = spot;
  const timestamp = currentHour(now, utcOffset);
  const variables = { pointOfInterestId, timestamp };
  console.log({ forecastsGraphQLVariables: variables });

  const {
    surfSpotForecasts: { sst, surf, swells, weather, wind },
  } = await request(
    `${config.scienceDataService}/graphql`,
    spotReportViewForecastsQuery,
    variables,
  );

  return {
    // SST sometimes returns more than 1 value. Always use latest.
    sst: sst && sst.data[sst.data.length - 1].sst,
    surf: surf && surf.data[0].surf,
    swells: swells && swells.data[0].swells,
    weather: weather && weather.data[0].weather,
    wind: wind && wind.data[0].wind,
  };
};
