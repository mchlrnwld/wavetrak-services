import fetch from 'node-fetch';
import { round } from 'lodash';
import config from '../config';
import createParamString from './createParamString';

/*
NOTE: The convertMetersToFeet, convertTideHeightUnits, and convertTideToKBYG functions
are copied from `wavetrak-services/services/kbyg-product-api`.
TODO: Move these function to a common package.
*/
const convertMetersToFeet = (length) => length / 0.3048;

export const convertTideHeightUnits = (tideHeightUnits, tideHeightInMeters, precision = 1) => {
  if (!tideHeightInMeters) return tideHeightInMeters;
  if (tideHeightUnits === 'FT') return round(convertMetersToFeet(tideHeightInMeters), precision);
  return round(tideHeightInMeters, precision);
};

export const convertTideToKBYG = (body, tideHeightUnits) => {
  const levels = body.levels.map((entry) => ({
    timestamp: entry.time,
    type: 'NORMAL',
    height: entry.shift,
  }));

  const tides = body.tide.map((tide) => ({
    timestamp: tide.time,
    type: tide.state.toUpperCase(),
    height: tide.shift,
  }));

  const combinedTides = tides.concat(levels);

  return {
    tides: combinedTides
      .sort((currentTide, nextTide) => currentTide.timestamp - nextTide.timestamp)
      .map((tide) => ({
        ...tide,
        height: convertTideHeightUnits(tideHeightUnits, tide.height, 2),
      })),
    associated: {
      units: {
        tideHeight: tideHeightUnits,
      },
      tideLocation: {
        name: body.port.name,
        min: convertTideHeightUnits(tideHeightUnits, body.port.minHeight, 2),
        max: convertTideHeightUnits(tideHeightUnits, body.port.maxHeight, 2),
        lon: body.port.lon,
        lat: body.port.lat,
        mean: 0.0,
      },
    },
  };
};

// istanbul ignore next
// eslint-disable-next-line import/prefer-default-export
export const getTides = async (tideStation, now) => {
  const tideHeightUnits = 'FT';
  const params = {
    port: tideStation,
    start: now - 172800,
    end: now + 172800,
  };

  const url = `${config.tideAPI}/data?${createParamString(params)}`;

  const response = await fetch(url);
  const body = await response.json();

  console.log({ tideStation, url, statusCode: response.status });

  if (response.status === 200) return convertTideToKBYG(body, tideHeightUnits);
  throw body;
};
