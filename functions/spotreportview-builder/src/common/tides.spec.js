import { expect } from 'chai';
import { convertTideToKBYG, convertTideHeightUnits } from './tides';

describe('Tide Builder', () => {
  describe('convertTideToKBYG', () => {
    it('convert tide response to KBYG format', async () => {
      const body = {
        port: {
          name: 'San Diego, California',
          lat: 32.7,
          lon: -117.2,
          minHeight: -1.16,
          maxHeight: 2.95,
        },
        tide: [
          { time: 1620309227, shift: 1.4, state: 'High' },
          { time: 1620332253, shift: 0, state: 'Low' },
          { time: 1620355846, shift: 1.6, state: 'High' },
        ],
        levels: [
          { time: 1620306000, shift: 1.3 },
          { time: 1620309600, shift: 1.4 },
          { time: 1620313200, shift: 1.3 },
        ],
      };
      const tideHeightUnits = 'FT';
      expect(convertTideToKBYG(body, tideHeightUnits)).to.deep.equal({
        tides: [
          {
            timestamp: 1620306000,
            type: 'NORMAL',
            height: 4.27,
          },
          {
            timestamp: 1620309227,
            type: 'HIGH',
            height: 4.59,
          },
          {
            timestamp: 1620309600,
            type: 'NORMAL',
            height: 4.59,
          },
          {
            timestamp: 1620313200,
            type: 'NORMAL',
            height: 4.27,
          },
          {
            timestamp: 1620332253,
            type: 'LOW',
            height: 0,
          },
          {
            timestamp: 1620355846,
            type: 'HIGH',
            height: 5.25,
          },
        ],
        associated: {
          units: {
            tideHeight: 'FT',
          },
          tideLocation: {
            name: 'San Diego, California',
            min: -3.81,
            max: 9.68,
            lon: -117.2,
            lat: 32.7,
            mean: 0,
          },
        },
      });
    });
  });
  describe('convertTideHeightUnits', () => {
    it('converts wave height from meters', () => {
      expect(convertTideHeightUnits('FT', 1)).to.equal(3.3);
      expect(convertTideHeightUnits('FT', 2)).to.equal(6.6);
      expect(convertTideHeightUnits('FT', 3)).to.equal(9.8);
      expect(convertTideHeightUnits('M', 2.3)).to.equal(2.3);
      expect(convertTideHeightUnits('M', 3.55)).to.equal(3.6);
      expect(convertTideHeightUnits('M', 5.25)).to.equal(5.3);
    });

    it('converts wave height from meters with given precision', () => {
      expect(convertTideHeightUnits('FT', 1, 1)).to.equal(3.3);
      expect(convertTideHeightUnits('FT', 1, 2)).to.equal(3.28);
      expect(convertTideHeightUnits('M', 2.3, 1)).to.equal(2.3);
      expect(convertTideHeightUnits('M', 3.55, 2)).to.equal(3.55);
    });

    it('returns tide height if falsey', () => {
      expect(convertTideHeightUnits('FT', 0)).to.equal(0);
      expect(convertTideHeightUnits('FT', null)).to.be.null();
      expect(convertTideHeightUnits('FT', undefined)).to.be.undefined();
    });
  });
});
