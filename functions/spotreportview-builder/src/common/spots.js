import fetch from 'node-fetch';
import config from '../config';

// istanbul ignore next
// eslint-disable-next-line import/prefer-default-export
export const getSpot = async (spotId) => {
  const url = `${config.spotsAPI}/admin/spots/${spotId}`;
  const response = await fetch(url);
  const body = await response.json();
  if (response.status === 200) return body;
  if (response.status === 404) return null;
  throw body;
};
