import fetch from 'node-fetch';
import config from '../config';

// istanbul ignore next
// eslint-disable-next-line import/prefer-default-export
export const getConditionRating = async (
  spotId,
  { swellPeriod, swellDirection, windSpeed, windDirection, surfMax },
) => {
  const conditions = {
    swellPeriod,
    swellDirection,
    windSpeed,
    windDirection,
    surfMax,
  };
  const query = Object.keys(conditions)
    .map((key) => `${key}=${conditions[key]}`)
    .join('&');
  const url = `${config.conditionRatingService}?spotId=${spotId}&${query}`;
  console.log({ conditions, url });
  const response = await fetch(url);
  const body = await response.json();
  if (response.status === 200) return body;
  throw body;
};
