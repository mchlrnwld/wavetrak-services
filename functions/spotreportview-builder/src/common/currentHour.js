const secondsPerHour = 60 * 60;

const currentHour = (now, utcOffset) => {
  const utcOffsetInSeconds = utcOffset * secondsPerHour;
  const secondsLocal = now + utcOffsetInSeconds;
  return secondsLocal - (secondsLocal % secondsPerHour) - utcOffsetInSeconds;
};

export default currentHour;
