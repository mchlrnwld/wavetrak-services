import fetch from 'node-fetch';
import config from '../config';

// istanbul ignore next
// eslint-disable-next-line import/prefer-default-export
export const getTaxonomy = async (spotId) => {
  const url = `${config.spotsAPI}/taxonomy?type=spot&id=${spotId}`;
  const response = await fetch(url);
  const body = await response.json();
  if (response.status === 200) return body;
  throw body;
};
