import fetch from 'node-fetch';
import config from '../config';

// istanbul ignore next
// eslint-disable-next-line import/prefer-default-export
export const getLatestSpotReport = async (spotId) => {
  const url = `${config.reportsAPI}/admin/reports/spot?spotId=${spotId}&limit=1`;
  const response = await fetch(url);
  const body = await response.json();
  if (response.status === 200) return body.spotReports[0];
  throw body;
};
