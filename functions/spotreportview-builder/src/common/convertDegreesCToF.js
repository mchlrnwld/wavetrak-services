const convertDegreesCToF = (temp) => temp * (9 / 5) + 32;

export default convertDegreesCToF;
