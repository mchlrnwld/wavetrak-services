import { expect } from 'chai';
import currentHour from './currentHour';

describe('common', () => {
  describe('currentHour', () => {
    it('returns current hour of timestamp local to UTC offset', () => {
      const now = 1528754279; // 06/11/2018@21:57 UTC and 06/11/2018@16:27 IST
      const utcOffset = 5.5;

      // 06/11/2018@21:30 UTC and 06/11/2018@16:00 IST
      expect(currentHour(now, utcOffset)).to.equal(1528752600);
    });
  });
});
