import chai from 'chai';
import chaiHttp from 'chai-http';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import mongoose from 'mongoose';

chai.use(chaiHttp);
chai.use(dirtyChai);
chai.use(sinonChai);

mongoose.Promise = global.Promise;
