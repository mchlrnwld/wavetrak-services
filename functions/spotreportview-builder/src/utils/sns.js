/* eslint-disable import/prefer-default-export */
import { get as _get } from 'lodash';

const eventToMessage = (event) => {
  const source = _get(event, 'Records[0].EventSource');
  const record = JSON.parse(_get(event, 'Records[0].Sns.Message', null));
  if (source === 'aws:sns' && record) return record;

  return null;
};

export { eventToMessage };
