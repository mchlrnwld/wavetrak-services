import mongoose from 'mongoose';
import 'mongoose-geojson-schema';

const cameraSchema = new mongoose.Schema({
  title: { type: String, required: true },
  isPremium: { type: Boolean, required: true },
  isPrerecorded: { type: Boolean },
  lastPrerecordedClipStartTimeUTC: { type: Date },
  lastPrerecordedClipEndTimeUTC: { type: Date },
  status: {
    isDown: { type: Boolean, required: true },
    message: { type: String },
    subMessage: { type: String },
    altMessage: { type: String },
  },
  supportsHighlights: { type: Boolean, required: true },
  supportsCrowds: { type: Boolean, required: true },
  streamUrl: { type: String, required: true },
  stillUrl: { type: String, required: true },
  pixelatedStillUrl: { type: String, required: true },
  rewindBaseUrl: { type: String },
  alias: { type: String },
});

const tideStepSchema = new mongoose.Schema(
  {
    type: { type: String, required: true },
    height: { type: Number, required: true },
    timestamp: { type: Number, required: true },
    utcOffset: { type: Number, required: true },
  },
  {
    _id: false,
  },
);

const tideSchema = new mongoose.Schema(
  {
    previous: tideStepSchema,
    current: tideStepSchema,
    next: tideStepSchema,
  },
  {
    _id: false,
    required: false,
  },
);

const swellSchema = new mongoose.Schema(
  {
    height: { type: Number, required: true },
    direction: { type: Number, required: true },
    directionMin: { type: Number, required: true },
    period: { type: Number, required: true },
  },
  {
    _id: false,
  },
);

const spotReportViewSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    lat: { type: Number, required: true },
    lon: { type: Number, required: true },
    location: {
      index: '2dsphere',
      type: mongoose.Schema.Types.Point,
      required: true,
    },
    thumbnail: { type: String, required: true },
    timezone: { type: String, required: true },
    offshoreDirection: { type: Number, required: false, default: null },
    legacyRegionId: { type: Number, required: false, default: null },
    subregionId: { type: mongoose.Schema.Types.ObjectId },
    rank: { type: Number },
    legacyId: { type: Number, required: false, default: null },
    waveHeight: {
      human: { type: Boolean, required: true },
      min: { type: Number, required: true },
      max: { type: Number },
      occasional: { type: Number },
      humanRelation: { type: String },
      plus: { type: Boolean, required: true },
    },
    swells: [swellSchema],
    weather: {
      temperature: { type: Number, required: true },
      condition: { type: String, required: true },
    },
    wind: {
      speed: { type: Number, required: true },
      direction: { type: Number, required: true },
      directionType: { type: String, required: true },
    },
    tide: tideSchema,
    note: { type: String, required: false, default: null },
    conditions: {
      human: { type: Boolean, required: true },
      value: { type: String },
      sortableCondition: { type: Number },
      expired: { type: Boolean, required: true },
    },
    cameras: [cameraSchema],
    waterTemp: {
      min: { type: Number, required: true },
      max: { type: Number, required: true },
    },
    parentTaxonomy: [{ type: mongoose.Schema.Types.ObjectId, required: true, index: true }],
    abilityLevels: [{ type: String }],
    boardTypes: [{ type: String }],
    relivableRating: { type: Number, required: false, default: 0 },
    saveFailures: { type: Number },
  },
  {
    collection: 'SpotReportViews',
    timestamps: true,
  },
);

const SpotReportViewModel = mongoose.model('SpotReportViews', spotReportViewSchema);

export const createSpotReportView = (model) => new SpotReportViewModel(model);

export default SpotReportViewModel;
