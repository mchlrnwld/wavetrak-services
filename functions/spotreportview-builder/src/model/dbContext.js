import mongoose from 'mongoose';

mongoose.Promise = global.Promise;

const initMongoDB = async () => {
  const connectionString = process.env.MONGO_CONNECTION_STRING_KBYG;
  return new Promise((resolve, reject) => {
    try {
      mongoose.connect(connectionString, { useNewUrlParser: true });
      mongoose.connection.once('open', () => {
        console.log('MongoDB connected');
        resolve();
      });
      mongoose.connection.on('error', (error) => {
        console.error({
          message: 'Function initialization failed. Error connecting to database.',
        });
        reject(error);
      });
    } catch (error) {
      console.error({
        message: "Function initialization failed. Can't connect to database.",
      });
      reject(error);
    }
  });
};

const closeMongoDB = async () => {
  await mongoose.connection.close();
  console.log('MongoDB Disconnected');
};

export default { initMongoDB, closeMongoDB };
