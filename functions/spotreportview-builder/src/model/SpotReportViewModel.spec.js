import { expect } from 'chai';
import SpotReportViewModel, { createSpotReportView } from './SpotReportViewModel';

describe('SpotReportView Model', () => {
  describe('createSpotReportView', () => {
    it('creates a new SpotReportView from the model', () => {
      const model = { _id: '583f491b4e65fad6a7701941', name: 'HB Pier, Southside' };
      const spotReportView = createSpotReportView(model);
      expect(spotReportView).to.be.an.instanceof(SpotReportViewModel);
      expect(spotReportView._id.toString()).to.equal('583f491b4e65fad6a7701941');
      expect(spotReportView.name).to.equal('HB Pier, Southside');
      expect(spotReportView.legacyRegionId).to.be.null();
    });
  });
});
