import dbContext from './model/dbContext';
import matchHandler from './handlers';

export default async (event, context) => {
  let result;
  let error;
  try {
    await dbContext.initMongoDB();
    result = await matchHandler(event, context);
  } catch (err) {
    console.error(err);
    error = err;
  } finally {
    await dbContext.closeMongoDB();
  }

  if (error) {
    throw error;
  }
  return result;
};
