import chai, { expect } from 'chai';
import sinon from 'sinon';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import handler from './index';
import * as buildSpotReportView from './buildSpotReportView';
import * as removeSpotReportView from './removeSpotReportView';
import * as invokeSRV from './invokeSpotReportViewBuilderLambda';
import * as getSpots from './getSpots';
import * as spots from '../common/spots';
import config from '../config';

chai.use(dirtyChai);
chai.use(sinonChai);

describe('handler', () => {
  const { cfCacheBustURL } = config;

  beforeEach(() => {
    sinon.stub(buildSpotReportView, 'default');
    sinon.stub(removeSpotReportView, 'default');
    sinon.stub(invokeSRV, 'default');
    sinon.stub(spots, 'getSpot');
    sinon.stub(getSpots, 'getSpotsByCamera');
    config.cfCacheBustURL = 'www.surfline.com';
    // sinon.spy(handlers, 'checkHandler');
  });

  afterEach(() => {
    buildSpotReportView.default.restore();
    removeSpotReportView.default.restore();
    invokeSRV.default.restore();
    spots.getSpot.restore();
    getSpots.getSpotsByCamera.restore();
    config.cfCacheBustURL = cfCacheBustURL;
    // handlers.checkHandler.restore();
  });

  it('should build a spot report view with an event given a published spotID', async () => {
    spots.getSpot.resolves({ status: 'PUBLISHED', timezone: 'America/Los_Angeles' });
    buildSpotReportView.default.resolves(true);

    const event = {
      spotId: '5842041f4e65fad6a77088ed',
    };

    await handler(event);
    expect(buildSpotReportView.default).to.have.been.calledOnce();
    expect(removeSpotReportView.default).not.to.have.been.called();
  });

  it('should build a spot report view from a sns trigger', async () => {
    spots.getSpot.resolves({ status: 'PUBLISHED', timezone: 'America/Los_Angeles' });
    buildSpotReportView.default.resolves(true);

    const event = {
      Records: [
        {
          EventSource: 'aws:sns',
          Sns: {
            Message: '{"spotId":"5842041f4e65fad6a7708847"}',
          },
        },
      ],
    };

    await handler(event);
    expect(buildSpotReportView.default).to.have.been.calledOnce();
    expect(removeSpotReportView.default).not.to.have.been.called();
  });

  it('should build a spot report view from a cam detail sns trigger', async () => {
    getSpots.getSpotsByCamera.resolves([{ _id: 'associatedSpotId', status: 'PUBLISHED' }]);
    invokeSRV.default.resolves(true);

    const event = {
      Records: [
        {
          EventSource: 'aws:sns',
          Sns: {
            Message: '{"cameraId":"5834a0c53421b20545c4b587"}',
          },
        },
      ],
    };

    await handler(event);
    expect(invokeSRV.default).to.have.been.calledOnce();
    expect(removeSpotReportView.default).not.to.have.been.called();
  });

  it('should clear cold fusion spot cache', async () => {
    const event = {
      spotId: '5842041f4e65fad6a77088ed',
    };
    buildSpotReportView.default.resolves(true);
    spots.getSpot.resolves({
      legacyId: '1234',
      status: 'PUBLISHED',
      timezone: 'America/Los_Angeles',
    });

    await handler(event);
    expect(buildSpotReportView.default).to.have.been.calledOnce();
    expect(removeSpotReportView.default).not.to.have.been.called();
  });

  it('should remove a spot report view with an event', async () => {
    spots.getSpot.resolves({ status: 'DRAFT' });
    removeSpotReportView.default.resolves(true);
    const event = {
      spotId: '5842041f4e65fad6a77088ed',
    };

    await handler(event);
    expect(removeSpotReportView.default).to.have.been.calledOnce();
    expect(buildSpotReportView.default).not.to.have.been.called();
  });

  // it('should run spot checker with op check passed', async () => {
  //   const event = { op: 'check' };

  //   await handler(event);
  //   expect(checkHandler).to.have.been.calledOnce();
  // });
});
