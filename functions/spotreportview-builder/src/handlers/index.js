import moment from 'moment-timezone';
import buildSpotReportView from './buildSpotReportView';
import removeSpotReportView from './removeSpotReportView';
import getSpots, { getSpotsByCamera } from './getSpots';
import invokeSpotReportViewBuilderLambda from './invokeSpotReportViewBuilderLambda';
import { getSpot } from '../common/spots';
import { eventToMessage } from '../utils/sns';

/**
 * Handler Responsible for
 *  - removing spots when spot status has transitioned to `DELETED` or `DRAFT`
 *  - building spots for a given `spotId`
 *
 * @param {Object} event params to build spot for. Has `spotId`.
 */
const spotHandler = async (event) => {
  const { spotId } = event;
  console.log(`spotHandler ${spotId} start.`);
  const spot = await getSpot(spotId);
  if (!spot) {
    return removeSpotReportView({ _id: spotId });
  }
  const { status } = spot;

  let opResult = null;
  switch (status) {
    case 'DELETED':
    case 'DRAFT':
      console.log(`Removing spotReportView for spotId=${spotId} status=${status}`);
      return removeSpotReportView(spot);

    case 'PUBLISHED':
    default:
      opResult = await buildSpotReportView(
        spot,
        Math.floor(new Date() / 1000),
        moment().tz(spot.timezone).utcOffset() / 60,
      );
      break;
  }

  if (!opResult) throw new Error('The build operation failed to run');
  return opResult;
};

/**
 * Responsible for triggering spot rebuilds when a SpotReportView's nextUpdated
 * property is in the time window.
 */
const checkHandler = async (event) => {
  console.log(`checkHandler start.`);
  const batchSize = parseInt(event.batchSize, 10);
  const spots = await getSpots(batchSize);
  await Promise.all(spots.map((spot) => invokeSpotReportViewBuilderLambda(spot._id.toString())));
  return `invoked spotreportview-builder for spots:${spots.map((spot) => ` ${spot._id}`)}`;
};

const cameraHandler = async ({ cameraId }) => {
  console.log(`cameraHandler ${cameraId} start`);
  const spots = await getSpotsByCamera(cameraId);
  await Promise.all(spots.map((spot) => invokeSpotReportViewBuilderLambda(spot._id.toString())));
  return `invoked spotreportview-builder for spots:${spots.map((spot) => ` ${spot._id}`)}`;
};

/**
 * Responsible for parsing the event object passed into the lambda function, and
 * returning the appropriate handler for the given parameters.
 *
 * @param {Object} event event object passed into the lambda handler
 */
const matchHandler = (event) => {
  if (event.op === 'check') return checkHandler(event);
  if (event.spotId) return spotHandler(event);
  if (event.cameraId) return cameraHandler(event);

  const message = eventToMessage(event);
  if (message) {
    if (message.spotId) return spotHandler(message);
    if (message.cameraId) return cameraHandler(message);
  }

  throw new Error('No matching handler to run for functions inputs');
};

export { checkHandler, spotHandler };
export default matchHandler;
