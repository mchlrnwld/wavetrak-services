import SpotReportViewModel from '../model/SpotReportViewModel';

const removeSpotReportView = ({ _id }) => SpotReportViewModel.findByIdAndRemove(_id);

export default removeSpotReportView;
