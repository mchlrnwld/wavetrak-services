import AWS from 'aws-sdk';
import config from '../config';

const invokeSpotReportViewBuilderLambda = async (spotId) => {
  const lambda = new AWS.Lambda({
    apiVersion: '2015-03-31',
    region: 'us-west-1',
  });

  await lambda
    .invoke({
      FunctionName: config.spotReportViewBuilderFunction,
      InvocationType: 'Event',
      LogType: 'None',
      Payload: JSON.stringify({
        spotId,
      }),
    })
    .promise();
};

export default invokeSpotReportViewBuilderLambda;
