import SpotReportViewModel from '../model/SpotReportViewModel';
import config from '../config';

const getSpots = async (batchSize) => {
  const spots = await SpotReportViewModel.find({
    $or: [{ saveFailures: { $lt: config.maxSaveFailures } }, { saveFailures: null }],
  })
    .sort({ updatedAt: 1 })
    .limit(batchSize);
  return spots;
};

export default getSpots;

const getSpotsByCamera = (cameraId) => SpotReportViewModel.find({ 'cameras._id': cameraId });

export { getSpotsByCamera };
