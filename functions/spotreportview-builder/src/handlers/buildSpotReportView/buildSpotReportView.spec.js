import { expect } from 'chai';
import buildSpotReportView from './buildSpotReportView';

describe('buildSpotReportView', () => {
  const spotToBuild = {
    _id: '583f491b4e65fad6a7701941',
    name: 'HB Pier, Southside',
    lat: 33.654213041213,
    lon: -118.0032588192,
  };
  const now = Math.floor(new Date() / 1000);
  const utcOffset = -8;

  it('should create and return a model using a pipeline of async builders', async () => {
    const firstStep = async (spot, model) => ({
      ...model,
      name: spot.name,
    });
    firstStep.description = 'First Step';
    const secondStep = async (spot, model) => ({
      ...model,
      lat: spot.lat,
    });
    secondStep.description = 'Second Step';
    const thirdStep = async (spot, model) => ({
      ...model,
      name: `${model.name} UPDATED`,
      lon: spot.lon,
    });
    thirdStep.description = 'Third Step';
    const builders = [firstStep, secondStep, thirdStep];

    const result = await buildSpotReportView(spotToBuild, now, utcOffset, builders);

    expect(result).to.deep.equal({
      name: 'HB Pier, Southside UPDATED',
      lat: 33.654213041213,
      lon: -118.0032588192,
      conditions: {
        human: false,
        expired: false,
        value: null,
      },
      waveHeight: {
        human: false,
        min: -1,
        max: -1,
        occasional: null,
        humanRelation: null,
        plus: false,
      },
      swells: [],
      wind: {
        speed: -1,
        direction: -1,
        directionType: null,
      },
      tide: null,
      cameras: [],
    });
  });

  it('should handle each step of the build pipeline', async () => {
    const firstStep = async (spot, model) => ({
      ...model,
      name: spot.name,
    });
    firstStep.description = 'First Step';
    const secondStep = async (model) => {
      throw new Error(JSON.stringify(model));
    };
    secondStep.description = 'Second Step';
    const thirdStep = async (model) => {
      throw new Error(JSON.stringify(model));
    };
    thirdStep.description = 'Third Step';
    const builders = [firstStep, secondStep, thirdStep];

    const result = await buildSpotReportView(spotToBuild, now, utcOffset, builders);

    expect(result).to.deep.equal({
      name: 'HB Pier, Southside',
      conditions: {
        human: false,
        expired: false,
        value: null,
      },
      waveHeight: {
        human: false,
        min: -1,
        max: -1,
        occasional: null,
        humanRelation: null,
        plus: false,
      },
      swells: [],
      wind: {
        speed: -1,
        direction: -1,
        directionType: null,
      },
      tide: null,
      cameras: [],
    });
  });
});
