const baseModel = {
  conditions: {
    human: false,
    value: null,
    expired: false,
  },
  waveHeight: {
    human: false,
    min: -1,
    max: -1,
    occasional: null,
    humanRelation: null,
    plus: false,
  },
  swells: [],
  wind: {
    speed: -1,
    direction: -1,
    directionType: null,
  },
  tide: null,
  cameras: [],
};

const buildSpotReportView = async (spot, now, utcOffset, buildSteps) => {
  let model = baseModel;
  // eslint-disable-next-line no-restricted-syntax
  for (const buildStep of buildSteps) {
    const buildLog = { spot: spot._id, buildStep: buildStep.description };
    try {
      // eslint-disable-next-line no-await-in-loop
      const build = await buildStep(spot, model, now, utcOffset);
      model = { ...model, ...build };
      console.log({
        ...buildLog,
        message: `Successfully built ${buildStep.description} for Spot ${spot._id}`,
      });
    } catch (err) {
      console.error({
        ...buildLog,
        stack: err.stack,
      });
    }
  }
  return model;
};

export default buildSpotReportView;
