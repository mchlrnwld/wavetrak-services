import SpotReportViewModel, { createSpotReportView } from '../../model/SpotReportViewModel';

const saveSpotReportView = async (spotId, builtSpotReportView) => {
  const spotReportViewDocument =
    (await SpotReportViewModel.findById(spotId)) || createSpotReportView({ _id: spotId });

  Object.assign(spotReportViewDocument, builtSpotReportView);

  spotReportViewDocument.markModified('cameras');

  const { saveFailures } = spotReportViewDocument;
  spotReportViewDocument.saveFailures = 0;

  try {
    await spotReportViewDocument.save();
    return spotReportViewDocument;
  } catch (err) {
    console.error({
      stack: err.stack,
      spotReportViewDocument,
    });
    return SpotReportViewModel.findOneAndUpdate(
      { _id: spotId },
      { $set: { saveFailures: saveFailures + 1 || 1 } },
    );
  }
};

export default saveSpotReportView;
