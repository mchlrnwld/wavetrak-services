import {
  spotBuilder,
  forecastsBuilder,
  reportBuilder,
  cameraBuilder,
  tideBuilder,
  parentTaxonomyBuilder,
} from './builders';
import buildSpotReportView from './buildSpotReportView';
import saveSpotReportView from './saveSpotReportView';

const buildCacheHandler = async (spot, now, utcOffset) => {
  if (!spot || !spot._id) throw new Error('Spot not found');

  const buildSteps = [
    spotBuilder,
    forecastsBuilder,
    reportBuilder,
    tideBuilder,
    cameraBuilder,
    parentTaxonomyBuilder,
  ]; // Build steps to be executed in order
  const builtSpotReportView = await buildSpotReportView(spot, now, utcOffset, buildSteps);
  console.log(`SpotReportView build complete for ${spot._id}. Saving now...`);
  const result = await saveSpotReportView(spot._id, builtSpotReportView);
  console.log(`SpotReportView saved for ${spot._id}`);
  return result;
};

export default buildCacheHandler;
