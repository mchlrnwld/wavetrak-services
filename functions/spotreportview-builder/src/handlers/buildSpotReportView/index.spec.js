import { expect } from 'chai';
import sinon from 'sinon';
import * as saveSpotReportView from './saveSpotReportView';
import * as spotsAPI from '../../common/spots';
import * as reportsAPI from '../../common/reports';
import * as camerasAPI from '../../common/cameras';
import * as conditionRatingAPI from '../../common/conditionRating';
import * as taxonomyAPI from '../../common/taxonomy';

import buildSpotReportView from '.';

describe('Cache Queue Poller', () => {
  let log;

  before(() => {
    log = {
      trace: sinon.stub(),
      info: sinon.stub(),
      error: sinon.stub(),
    };
  });

  beforeEach(() => {
    sinon.stub(spotsAPI, 'getSpot');
    sinon.stub(reportsAPI, 'getLatestSpotReport');
    sinon.stub(camerasAPI, 'getCameras');
    sinon.stub(conditionRatingAPI, 'getConditionRating');
    sinon.stub(taxonomyAPI, 'getTaxonomy');
    sinon.stub(saveSpotReportView, 'default');
  });

  afterEach(() => {
    log.trace.reset();
    log.info.reset();
    log.error.reset();
    spotsAPI.getSpot.restore();
    reportsAPI.getLatestSpotReport.restore();
    camerasAPI.getCameras.restore();
    conditionRatingAPI.getConditionRating.restore();
    taxonomyAPI.getTaxonomy.restore();
    saveSpotReportView.default.restore();
  });

  it('should build and save a SpotReportView for the spot ID', async () => {
    const spot = {
      _id: '583499cb3421b20545c4b53f',
      name: 'HB Pier, Southside',
      cams: ['583499cb3421b20545c4bkd2j'],
      location: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
      forecastLocation: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
      legacyRegionId: 2081,
      subregionId: '58581a836630e24c44878fd6',
      rank: 10,
      legacyId: 2091,
      thumbnail: {
        3000: '',
        1500: 'https://spot-thumbnails.surfline.com/spots/5842041f4e65fad6a77088ed/5842041f4e65fad6a77088ed_1500.jpg',
        640: '',
        300: '',
      },
      timezone: 'America/Los_Angeles',
    };
    const cameras = [
      {
        _id: '58349c5f3421b20545c4b559',
        title: '1st St. Fixed Cam',
        streamUrl:
          'http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-vbfirstfixedcam.smil/playlist.m3u8',
        stillUrl: 'http://camstills.cdn-surfline.com/vbfirstfixedcam/latest_small.jpg',
        pixelatedStillUrl:
          'http://camstills.cdn-surfline.com/vbfirstfixedcam/latest_small_pixalated.png',
        rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/vbfirstfixedcam/vbfirstfixedcam',
        isDown: {
          status: false,
          message: '',
        },
      },
    ];
    const conditions = {
      score: 1.6861718750000003,
      condition: 'POOR_TO_FAIR',
    };
    const parentTaxonomy = [
      '58f7ed51dadb30820bb38782',
      '58f7ed51dadb30820bb38791',
      '58f7ed51dadb30820bb3879c',
      '5908c46bdadb30820b23c1f3',
      '58f7ed5ddadb30820bb39651',
      '58f7ed51dadb30820bb387a6',
      '58f7ed5ddadb30820bb39689',
      '58f7ed5ddadb30820bb3965c',
    ];
    const spotReport = {
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString(),
      minHeight: 2,
      maxHeight: 3,
      plusHeight: true,
      occasionalHeight: 4,
      condition: 3,
      rating: 'FAIR_TO_GOOD',
      humanRelation: 'knee to waist high',
      waterTemp: {
        range: '50-55',
        min: 60,
        max: 65,
      },
    };
    const expectedSpotReportView = {
      _id: '583499cb3421b20545c4b53f',
      name: 'HB Pier, Southside',
      lat: 33.654213041213,
      lon: -118.0032588192,
      legacyId: 2091,
      thumbnail:
        'https://spot-thumbnails.surfline.com/spots/5842041f4e65fad6a77088ed/5842041f4e65fad6a77088ed_1500.jpg',
      timezone: 'America/Los_Angeles',
      location: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
      legacyRegionId: 2081,
      subregionId: '58581a836630e24c44878fd6',
      rank: 10,
      waveHeight: {
        human: true,
        max: 3,
        min: 2,
        occasional: 4,
        humanRelation: 'knee to waist high',
        plus: true,
      },
      weather: {
        temperature: 70.9,
        condition: 'MOSTLY_CLOUDY_NO_RAIN',
      },
      tide: null,
      wind: {
        speed: 16.12,
        direction: 236.81,
      },
      conditions: {
        human: true,
        sortableCondition: 3,
        value: 'FAIR_TO_GOOD',
      },
      cameras: [
        {
          _id: '58349c5f3421b20545c4b559',
          title: '1st St. Fixed Cam',
          streamUrl:
            'http://livestream.cdn-surfline.com/cdn-live/_definst_/surfline/secure/live/wc-vbfirstfixedcam.smil/playlist.m3u8',
          stillUrl: 'http://camstills.cdn-surfline.com/vbfirstfixedcam/latest_small.jpg',
          pixelatedStillUrl:
            'http://camstills.cdn-surfline.com/vbfirstfixedcam/latest_small_pixalated.png',
          rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/vbfirstfixedcam/vbfirstfixedcam',
          status: {
            message: '',
            isDown: false,
          },
        },
      ],
      waterTemp: {
        min: 60,
        max: 65,
      },
      parentTaxonomy: [
        '58f7ed51dadb30820bb38782',
        '58f7ed51dadb30820bb38791',
        '58f7ed51dadb30820bb3879c',
        '5908c46bdadb30820b23c1f3',
        '58f7ed5ddadb30820bb39651',
        '58f7ed51dadb30820bb387a6',
        '58f7ed5ddadb30820bb39689',
        '58f7ed5ddadb30820bb3965c',
      ],
    };
    spotsAPI.getSpot.resolves(spot);
    reportsAPI.getLatestSpotReport.resolves(spotReport);
    camerasAPI.getCameras.resolves(cameras);
    conditionRatingAPI.getConditionRating.resolves(conditions);
    taxonomyAPI.getTaxonomy.resolves(parentTaxonomy);
    saveSpotReportView.default.resolves(expectedSpotReportView);

    await buildSpotReportView({ _id: spot._id });

    // expect(spotsAPI.getSpot).to.have.been.calledOnce();
    // expect(spotsAPI.getSpot).to.have.been.calledWithExactly('583499cb3421b20545c4b53f');
    // expect(saveSpotReportView.default).to.have.been.calledOnce();
    // expect(saveSpotReportView.default.firstCall.args).to.deep.equal([
    //   '583499cb3421b20545c4b53f',
    //   expectedSpotReportView,
    // ]);
  });

  it('should handle errors saving SpotReportView', async () => {
    spotsAPI.getSpot.resolves({
      _id: '583499cb3421b20545c4b53f',
      name: 'HB Pier, Southside',
      location: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
      forecastLocation: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
    });
    saveSpotReportView.default.rejects(new Error('Save spot failed'));

    try {
      await buildSpotReportView({ _id: '583499cb3421b20545c4b53f' });
      throw new Error('expected error message: Save spot failed');
    } catch (err) {
      expect(err.message).to.equal('Save spot failed');
    }
  });
});
