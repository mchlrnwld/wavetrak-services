import { expect } from 'chai';
import sinon from 'sinon';
import SpotReportView, * as spotReportViewModel from '../../model/SpotReportViewModel';
import saveSpotReportView from './saveSpotReportView';

describe('SpotReportView Model', () => {
  describe('saveSpotReportView', () => {
    beforeEach(() => {
      sinon.stub(SpotReportView, 'findById');
      sinon.stub(SpotReportView, 'findOneAndUpdate');
      sinon.stub(spotReportViewModel, 'createSpotReportView');
    });

    afterEach(() => {
      SpotReportView.findById.restore();
      SpotReportView.findOneAndUpdate.restore();
      spotReportViewModel.createSpotReportView.restore();
    });

    it('should update existing SpotReportView with fields provided', async () => {
      const spotReportView = {
        name: 'HB Pier, Southside',
        lat: 33.654213041213,
        lon: -118.0032588192,
        waveHeight: {
          human: true,
          min: 2,
          max: 3,
          occasional: 4,
        },
        cameras: {
          _id: '583499cb3421b20545c4b53f',
          title: 'HB Pier, Southside',
          status: {
            isDown: false,
          },
        },
        weather: {
          temperature: 57.07399,
          condition: 'OVERCAST',
        },
        wind: {
          speed: 0.68659,
          direction: 343.62322,
          directionType: 'Offshore',
        },
        conditions: {
          human: false,
          value: null,
          expired: false,
        },
        waterTemp: {
          min: 60.8,
          max: 62.6,
        },
        save: sinon.stub().resolves(),
        markModified: sinon.stub(),
      };
      const updatedSpotReportView = {
        name: 'HB Pier, Southside',
        lat: 33.654213041213,
        lon: -118.0032588192,
        cameras: {
          _id: '583499cb3421b20545c4b53f',
          title: 'HB Pier, Southside',
          status: {
            isDown: false,
            message:
              'We apologize for the camera downtime due to a local Internet Service Provider issue. Their technicians are working to restore service. Our Camera will return as soon as their issue is resolved. Thank you for your patience.',
          },
        },
      };
      SpotReportView.findById.resolves(spotReportView);
      const result = await saveSpotReportView('583499cb3421b20545c4b53f', updatedSpotReportView);
      expect(result).to.equal(spotReportView);
      expect(SpotReportView.findById).to.have.been.calledOnce();
      expect(SpotReportView.findById).to.have.been.calledWithExactly('583499cb3421b20545c4b53f');
      expect(spotReportViewModel.createSpotReportView).not.to.have.been.called();
      expect(spotReportView.save).to.have.been.calledOnce();
      expect(SpotReportView.findOneAndUpdate).not.to.have.been.called();
      expect(spotReportView.markModified).to.have.been.calledOnce();
      expect(spotReportView).to.deep.equal({
        name: 'HB Pier, Southside',
        lat: 33.654213041213,
        lon: -118.0032588192,
        waveHeight: {
          human: true,
          min: 2,
          max: 3,
          occasional: 4,
        },
        weather: {
          temperature: 57.07399,
          condition: 'OVERCAST',
        },
        wind: {
          speed: 0.68659,
          direction: 343.62322,
          directionType: 'Offshore',
        },
        conditions: {
          human: false,
          value: null,
          expired: false,
        },
        waterTemp: {
          min: 60.8,
          max: 62.6,
        },
        cameras: {
          _id: '583499cb3421b20545c4b53f',
          title: 'HB Pier, Southside',
          status: {
            isDown: false,
            message:
              'We apologize for the camera downtime due to a local Internet Service Provider issue. Their technicians are working to restore service. Our Camera will return as soon as their issue is resolved. Thank you for your patience.',
          },
        },
        save: spotReportView.save,
        markModified: spotReportView.markModified,
        saveFailures: 0,
      });
    });

    it('should create new SpotReportView if one does not exist', async () => {
      const saveStub = sinon.stub().resolves();
      SpotReportView.findById.resolves(null);
      const spotReportView = {
        _id: '583499cb3421b20545c4b53f',
        save: saveStub,
        markModified: sinon.stub(),
      };
      const newSpotReportView = {
        name: 'HB Pier, Southside',
        lat: 33.654213041213,
        lon: -118.0032588192,
        cameras: {
          _id: '583499cb3421b20545c4b53f',
          title: 'HB Pier, Southside',
          status: {
            isDown: false,
            message:
              'We apologize for the camera downtime due to a local Internet Service Provider issue. Their technicians are working to restore service. Our Camera will return as soon as their issue is resolved. Thank you for your patience.',
          },
        },
      };
      spotReportViewModel.createSpotReportView.returns(spotReportView);
      const result = await saveSpotReportView('583499cb3421b20545c4b53f', newSpotReportView);
      expect(result).to.equal(spotReportView);
      expect(SpotReportView.findById).to.have.been.calledOnce();
      expect(SpotReportView.findById).to.have.been.calledWithExactly('583499cb3421b20545c4b53f');
      expect(spotReportViewModel.createSpotReportView).to.have.been.calledOnce();
      expect(spotReportViewModel.createSpotReportView.firstCall.args).to.deep.equal([
        { _id: '583499cb3421b20545c4b53f' },
      ]);
      expect(spotReportView.save).to.have.been.calledOnce();
      expect(spotReportView.markModified).to.have.been.calledOnce();
      expect(spotReportView).to.deep.equal({
        _id: '583499cb3421b20545c4b53f',
        name: 'HB Pier, Southside',
        lat: 33.654213041213,
        lon: -118.0032588192,
        cameras: {
          _id: '583499cb3421b20545c4b53f',
          title: 'HB Pier, Southside',
          status: {
            isDown: false,
            message:
              'We apologize for the camera downtime due to a local Internet Service Provider issue. Their technicians are working to restore service. Our Camera will return as soon as their issue is resolved. Thank you for your patience.',
          },
        },
        save: saveStub,
        markModified: spotReportView.markModified,
        saveFailures: 0,
      });
    });
  });
});
