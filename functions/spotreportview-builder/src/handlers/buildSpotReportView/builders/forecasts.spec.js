import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import forecastsBuilder from './forecasts';
import * as forecasts from '../../../common/forecasts';

describe('Forecasts Builder', () => {
  const spot = deepFreeze({
    _id: '583f491b4e65fad6a7701941',
    name: 'HB Pier, Southside',
    lat: 33.654213041213,
    lon: -118.0032588192,
    location: {
      type: 'Point',
      coordinates: [-118.0032588192, 33.654213041213],
    },
    legacyRegionId: 2081,
    subregionId: '58581a836630e24c44878fd6',
    rank: 10,
    legacyId: 2091,
    thumbnail:
      'https://spot-thumbnails.surfline.com/spots/5842041f4e65fad6a77088ed/5842041f4e65fad6a77088ed_1500.jpg',
  });
  const now = 1493708400;
  const utcOffset = -5;

  let result;

  before(() => {
    sinon.stub(forecasts, 'getForecasts');
  });

  after(() => {
    forecasts.getForecasts.restore();
  });

  describe('with resolved forecasts', () => {
    before(async () => {
      forecasts.getForecasts.resolves({
        sst: {
          temperature: 20.5,
        },
        surf: {
          breakingWaveHeightMin: 1.8,
          breakingWaveHeightMax: 2.2,
        },
        swells: {
          components: [...Array(6)].map((_, event) => ({
            event,
            height: 0.8,
            period: 12,
            direction: 338.59,
            spread: 55.82,
          })),
        },
        weather: {
          temperature: 70.9,
          conditions: 'CLEAR_SKIES',
        },
        wind: {
          speed: 16.12,
          direction: 236.81,
          directionType: 'Offshore',
        },
      });
      result = await forecastsBuilder(spot, spot, now, utcOffset);
    });

    after(() => {
      forecasts.getForecasts.reset();
    });

    it('should add the SST min and max temperatures to the SpotReportView', () => {
      expect(result.waterTemp.min).to.be.closeTo(68, 0.00001);
      expect(result.waterTemp.max).to.be.closeTo(69.8, 0.00001);
    });

    it('should add min and max surf heights to the SpotReportView', () => {
      expect(result.waveHeight).to.deep.equal({
        human: false,
        min: 1.8,
        max: 2.2,
        occasional: null,
        humanRelation: null,
        plus: false,
      });
    });

    it('should add an array of swell components to the SpotReportView', () => {
      expect(result.swells).to.have.length(6);
      result.swells.forEach((swell) => {
        expect(swell.height).to.equal(0.8);
        expect(swell.period).to.equal(12);
        expect(swell.direction).to.equal(338.59);
        expect(swell.directionMin).to.be.closeTo(310.68, 0.00001);
      });
    });

    it('should add weather temperature and conditions to the SpotReportView', () => {
      expect(result.weather).to.deep.equal({
        temperature: 70.9,
        condition: 'CLEAR_SKIES',
      });
    });

    it('should add wind speed, direction and directionType to the SpotReportView', () => {
      expect(result.wind).to.deep.equal({
        speed: 16.12,
        direction: 236.81,
        directionType: 'Offshore',
      });
    });
  });

  describe('with no forecast data found', () => {
    before(() => {
      forecasts.getForecasts.resolves({
        sst: null,
        surf: null,
        swells: null,
        weather: null,
        wind: null,
      });
    });

    after(() => {
      forecasts.getForecasts.reset();
    });

    it('fails gracefully and preserves existing data', async () => {
      const model = {
        waveHeight: {
          human: true,
          min: 0,
          max: 1,
          occasional: 1.5,
          humanRelation: null,
          plus: true,
        },
        swells: [
          {
            height: 0.7,
            period: 11,
            direction: 300,
            directionMin: 290,
          },
        ],
        weather: {
          temperature: 60,
          condition: 'CLOUDY',
        },
        wind: {
          speed: 15.1,
          direction: 225.8,
        },
      };
      result = await forecastsBuilder(spot, model, now, utcOffset);
      expect(result).to.deep.equal(model);
    });
  });
});
