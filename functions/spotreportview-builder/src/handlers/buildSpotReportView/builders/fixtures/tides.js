export default {
  tides: [
    {
      timestamp: 1484276400,
      type: 'NORMAL',
      height: 1.84,
    },
    {
      timestamp: 1484280000,
      type: 'NORMAL',
      height: 3.18,
    },
    {
      timestamp: 1484283600,
      type: 'NORMAL',
      height: 4.1,
    },
    {
      timestamp: 1484287200,
      type: 'NORMAL',
      height: 4.43,
    },
    {
      timestamp: 1484287336,
      type: 'HIGH',
      height: 4.43,
    },
    {
      timestamp: 1484290800,
      type: 'NORMAL',
      height: 4.17,
    },
    {
      timestamp: 1484294400,
      type: 'NORMAL',
      height: 3.48,
    },
    {
      timestamp: 1484298000,
      type: 'NORMAL',
      height: 2.62,
    },
    {
      timestamp: 1484301600,
      type: 'NORMAL',
      height: 1.87,
    },
    {
      timestamp: 1484305200,
      type: 'NORMAL',
      height: 1.61,
    },
    {
      timestamp: 1484305436,
      type: 'LOW',
      height: 1.61,
    },
    {
      timestamp: 1484308800,
      type: 'NORMAL',
      height: 1.84,
    },
    {
      timestamp: 1484312400,
      type: 'NORMAL',
      height: 2.66,
    },
    {
      timestamp: 1484316000,
      type: 'NORMAL',
      height: 3.84,
    },
    {
      timestamp: 1484319600,
      type: 'NORMAL',
      height: 5.12,
    },
    {
      timestamp: 1484323200,
      type: 'NORMAL',
      height: 6.17,
    },
    {
      timestamp: 1484326800,
      type: 'NORMAL',
      height: 6.63,
    },
    {
      timestamp: Math.floor(new Date() / 1000) - 172800,
      type: 'HIGH',
      height: 6.69,
    },
    {
      timestamp: 1484330400,
      type: 'NORMAL',
      height: 6.46,
    },
    {
      timestamp: 1484334000,
      type: 'NORMAL',
      height: 5.51,
    },
    {
      timestamp: 1484337600,
      type: 'NORMAL',
      height: 4.0,
    },
    {
      timestamp: 1484341200,
      type: 'NORMAL',
      height: 2.23,
    },
    {
      timestamp: 1484344800,
      type: 'NORMAL',
      height: 0.52,
    },
    {
      timestamp: 1484348400,
      type: 'NORMAL',
      height: -0.72,
    },
    {
      timestamp: 1484352000,
      type: 'NORMAL',
      height: -1.31,
    },
    {
      timestamp: 1484353002,
      type: 'LOW',
      height: -1.35,
    },
    {
      timestamp: 1484355600,
      type: 'NORMAL',
      height: -1.15,
    },
    {
      timestamp: 1484359200,
      type: 'NORMAL',
      height: -0.33,
    },
    {
      timestamp: 1484362800,
      type: 'NORMAL',
      height: 0.98,
    },
    {
      timestamp: 1484366400,
      type: 'NORMAL',
      height: 2.36,
    },
    {
      timestamp: 1484370000,
      type: 'NORMAL',
      height: 3.58,
    },
    {
      timestamp: 1484373600,
      type: 'NORMAL',
      height: 4.3,
    },
    {
      timestamp: 1484376311,
      type: 'HIGH',
      height: 4.46,
    },
    {
      timestamp: 1484377200,
      type: 'NORMAL',
      height: 4.46,
    },
    {
      timestamp: 1484380800,
      type: 'NORMAL',
      height: 4.07,
    },
    {
      timestamp: 1484384400,
      type: 'NORMAL',
      height: 3.31,
    },
    {
      timestamp: 1484388000,
      type: 'NORMAL',
      height: 2.49,
    },
    {
      timestamp: 1484391600,
      type: 'NORMAL',
      height: 1.84,
    },
    {
      timestamp: Math.floor(new Date() / 1000) + 172800,
      type: 'LOW',
      height: 1.67,
    },
    {
      timestamp: 1484395200,
      type: 'NORMAL',
      height: 1.71,
    },
    {
      timestamp: 1484398800,
      type: 'NORMAL',
      height: 2.03,
    },
    {
      timestamp: 1484402400,
      type: 'NORMAL',
      height: 2.89,
    },
    {
      timestamp: 1484406000,
      type: 'NORMAL',
      height: 4.0,
    },
    {
      timestamp: 1484409600,
      type: 'NORMAL',
      height: 5.15,
    },
    {
      timestamp: 1484413200,
      type: 'NORMAL',
      height: 5.97,
    },
    {
      timestamp: 1484416585,
      type: 'HIGH',
      height: 6.27,
    },
    {
      timestamp: 1484416800,
      type: 'NORMAL',
      height: 6.27,
    },
    {
      timestamp: 1484420400,
      type: 'NORMAL',
      height: 5.87,
    },
    {
      timestamp: 1484424000,
      type: 'NORMAL',
      height: 4.86,
    },
    {
      timestamp: 1484427600,
      type: 'NORMAL',
      height: 3.38,
    },
    {
      timestamp: 1484431200,
      type: 'NORMAL',
      height: 1.77,
    },
    {
      timestamp: 1484434800,
      type: 'NORMAL',
      height: 0.33,
    },
    {
      timestamp: 1484438400,
      type: 'NORMAL',
      height: -0.66,
    },
    {
      timestamp: 1484441842,
      type: 'LOW',
      height: -0.98,
    },
    {
      timestamp: 1484442000,
      type: 'NORMAL',
      height: -0.98,
    },
    {
      timestamp: 1484445600,
      type: 'NORMAL',
      height: -0.62,
    },
  ],
  associated: {
    units: {
      tideHeight: 'ft',
    },
    tideLocation: {
      name: 'Venice Beach cam near Los Angeles',
      min: -3.44,
      max: 9.12,
      lon: -118.464,
      lat: 33.9706,
      mean: 2.85,
    },
  },
};
