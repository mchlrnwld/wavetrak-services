import { getCameras } from '../../../common/cameras';

const cameraBuilder = async (spot) => {
  const spotCams = await getCameras(spot.cams.filter((cam) => cam !== null));
  if (spotCams) {
    const cameras = spotCams.map((camera) => ({
      _id: camera._id,
      title: camera.title,
      streamUrl: camera.streamUrl,
      stillUrl: camera.stillUrl,
      pixelatedStillUrl: camera.pixelatedStillUrl,
      rewindBaseUrl: camera.rewindBaseUrl,
      isPremium: camera.isPremium,
      isPrerecorded: camera.isPrerecorded,
      lastPrerecordedClipStartTimeUTC: camera.lastPrerecordedClipStartTimeUTC,
      lastPrerecordedClipEndTimeUTC: camera.lastPrerecordedClipEndTimeUTC,
      alias: camera.alias,
      supportsHighlights: !!camera.supportsHighlights,
      supportsCrowds: !!camera.supportsCrowds,
      status: {
        isDown: camera.isDown ? camera.isDown.status : false,
        message: camera.isDown ? camera.isDown.message : '',
        subMessage: camera.isDown ? camera.isDown.subMessage : '',
        altMessage: camera.isDown ? camera.isDown.altMessage : '',
      },
    }));
    return { cameras };
  }

  return null;
};

cameraBuilder.description = 'Camera';

export default cameraBuilder;
