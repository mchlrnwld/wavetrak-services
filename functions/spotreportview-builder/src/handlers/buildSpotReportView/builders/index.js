/*
 * Builders have function signature
 *
 * `async (spot, model) => newModel`
 *
 * and have a `description` property for logging purposes
 */

import cameraBuilder from './camera';
import forecastsBuilder from './forecasts';
import reportBuilder from './report';
import parentTaxonomyBuilder from './parentTaxonomy';
import spotBuilder from './spot';
import tideBuilder from './tide';

export {
  cameraBuilder,
  forecastsBuilder,
  reportBuilder,
  parentTaxonomyBuilder,
  spotBuilder,
  tideBuilder,
};
