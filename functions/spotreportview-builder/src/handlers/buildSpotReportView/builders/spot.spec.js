import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import spotBuilder from './spot';

describe('Spot Builder', () => {
  it('should build the spot section of the SpotReportView', async () => {
    const spot = deepFreeze({
      _id: '583f491b4e65fad6a7701941',
      name: 'HB Pier, Southside',
      location: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
      legacyRegionId: 2081,
      subregionId: '58581a836630e24c44878fd6',
      rank: 10,
      legacyId: 2091,
      thumbnail: {
        3000: '',
        1500: 'https://spot-thumbnails.surfline.com/spots/5842041f4e65fad6a77088ed/5842041f4e65fad6a77088ed_1500.jpg',
        640: '',
        300: '',
      },
      timezone: 'America/Los_Angeles',
      offshoreDirection: 211,
      abilityLevels: ['INTERMEDIATE'],
      boardTypes: ['SHORTBOARD', 'FISH', 'FUNBOARD', 'LONGBOARD'],
      relivableRating: 4,
    });
    const model = deepFreeze({
      somePropertyToPreserve: 'some value to preserve',
    });
    const result = await spotBuilder(spot, model);
    expect(result).to.deep.equal({
      somePropertyToPreserve: 'some value to preserve',
      _id: '583f491b4e65fad6a7701941',
      name: 'HB Pier, Southside',
      lat: 33.654213041213,
      lon: -118.0032588192,
      location: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
      legacyRegionId: 2081,
      subregionId: '58581a836630e24c44878fd6',
      rank: 10,
      legacyId: 2091,
      thumbnail:
        'https://spot-thumbnails.surfline.com/spots/5842041f4e65fad6a77088ed/5842041f4e65fad6a77088ed_1500.jpg',
      timezone: 'America/Los_Angeles',
      offshoreDirection: 211,
      abilityLevels: ['INTERMEDIATE'],
      boardTypes: ['SHORTBOARD', 'FISH', 'FUNBOARD', 'LONGBOARD'],
      relivableRating: 4,
    });
  });
});
