import convertDegreesCToF from '../../../common/convertDegreesCToF';
import { getForecasts } from '../../../common/forecasts';

const forecastsBuilder = async (spot, model, now, utcOffset) => {
  const result = { ...model };
  const { sst, surf, swells, weather, wind } = await getForecasts(spot, now, utcOffset);

  if (sst) {
    result.waterTemp = {
      min: convertDegreesCToF(Math.floor(sst.temperature)),
      max: convertDegreesCToF(Math.ceil(sst.temperature)),
    };
  }

  if (swells) {
    result.swells = [...Array(6)].map((_, i) => {
      const componentResult = {
        height: 0,
        period: 0,
        direction: 0,
        directionMin: 0,
      };

      const component = swells.components.find(({ event }) => event === i);
      if (component) {
        componentResult.height = component.height;
        componentResult.period = component.period;
        componentResult.direction = component.direction;
        componentResult.directionMin = component.direction - component.spread / 2;
      }

      return componentResult;
    });
  }

  if (surf) {
    result.waveHeight = {
      human: false,
      min: surf.breakingWaveHeightMin,
      max: surf.breakingWaveHeightMax,
      occasional: null,
      humanRelation: null,
      plus: false,
    };
  }

  if (weather) {
    result.weather = {
      temperature: weather.temperature,
      condition: weather.conditions,
    };
  }

  if (wind) {
    result.wind = {
      speed: wind.speed,
      direction: wind.direction,
      directionType: wind.directionType,
    };
  }

  return result;
};

forecastsBuilder.description = 'Forecasts';

export default forecastsBuilder;
