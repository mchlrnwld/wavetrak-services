import { getTaxonomy } from '../../../common/taxonomy';

const parentTaxonomyBuilder = async (spot, model) => {
  const taxonomy = await getTaxonomy(spot._id);
  const parentTaxonomy = taxonomy.in.map((tax) => tax._id);
  return {
    ...model,
    parentTaxonomy,
  };
};

parentTaxonomyBuilder.description = 'Parent Taxonomy';

export default parentTaxonomyBuilder;
