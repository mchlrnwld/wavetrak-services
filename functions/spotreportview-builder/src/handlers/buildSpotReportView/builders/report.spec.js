import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import reportBuilder from './report';
import * as reportsAPI from '../../../common/reports';

describe('Report Builder', () => {
  beforeEach(() => {
    sinon.stub(reportsAPI, 'getLatestSpotReport');
  });

  afterEach(() => {
    reportsAPI.getLatestSpotReport.restore();
  });

  it('overrides the wave height and conditions sections of SpotReportView with report', async () => {
    const spot = deepFreeze({
      _id: '583f491b4e65fad6a7701941',
      name: 'HB Pier, Southside',
      timezone: 'America/Los_Angeles',
      location: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
      legacyRegionId: 2081,
    });
    const model = deepFreeze({
      name: 'HB Pier, Southside',
      lat: 33.654213041213,
      lon: -118.0032588192,
      location: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
      legacyRegionId: 2081,
      waveHeight: {
        min: 3,
        max: 4,
        occasional: null,
      },
      conditions: {
        value: 'POOR_TO_FAIR',
      },
      waterTemp: {
        min: 20,
        max: 21,
      },
    });
    reportsAPI.getLatestSpotReport.resolves({
      createdAt: '2019-08-05T04:55:54.500Z',
      updatedAt: '2019-08-05T04:55:54.500Z',
      minHeight: 2,
      maxHeight: 3,
      plusHeight: true,
      occasionalHeight: 4,
      condition: 3,
      rating: 'FAIR_TO_GOOD',
      humanRelation: 'knee to waist high',
      waterTemp: {
        range: '30-35',
        min: 30,
        max: 35,
      },
    });
    const result = await reportBuilder(spot, model);
    expect(result).to.deep.equal({
      name: 'HB Pier, Southside',
      lat: 33.654213041213,
      lon: -118.0032588192,
      location: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
      legacyRegionId: 2081,
      waveHeight: {
        human: true,
        min: 2,
        max: 3,
        occasional: 4,
        plus: true,
        humanRelation: 'knee to waist high',
      },
      note: null,
      conditions: {
        human: true,
        sortableCondition: 3,
        value: 'FAIR_TO_GOOD',
        expired: true,
      },
      waterTemp: {
        min: 30,
        max: 35,
      },
    });
  });

  it('leaves the wave height and conditions sections of SpotReportView if no up-to-date report found', async () => {
    const spot = deepFreeze({
      _id: '583f491b4e65fad6a7701941',
      name: 'HB Pier, Southside',
      timezone: 'America/Los_Angeles',
      location: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
      legacyRegionId: 2081,
    });
    const model = deepFreeze({
      name: 'HB Pier, Southside',
      lat: 33.654213041213,
      lon: -118.0032588192,
      location: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
      legacyRegionId: 2081,
      waveHeight: {
        human: false,
        min: 3,
        max: 4,
        occasional: null,
      },
      conditions: {
        human: true,
        sortableCondition: 3,
        value: 'POOR_TO_FAIR',
        expired: true,
      },
    });
    reportsAPI.getLatestSpotReport.resolves({
      createdAt: '2019-08-05T04:55:54.500Z',
      updatedAt: '2019-08-05T04:55:54.500Z',
      minHeight: 2,
      maxHeight: 3,
      occasionalHeight: 4,
      rating: 'FAIR_TO_GOOD',
      stale: true,
    });
    const result = await reportBuilder(spot, model);
    expect(result).to.deep.equal({
      name: 'HB Pier, Southside',
      lat: 33.654213041213,
      lon: -118.0032588192,
      location: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
      legacyRegionId: 2081,
      waveHeight: {
        human: false,
        min: 3,
        max: 4,
        occasional: null,
      },
      conditions: {
        human: true,
        sortableCondition: 3,
        value: 'POOR_TO_FAIR',
        expired: true,
      },
    });
  });
});
