import { expect } from 'chai';
import * as builders from '.';

describe('Builders', () => {
  it('should have description properties', () => {
    Object.keys(builders).forEach((builder) => expect(builders[builder].description).to.be.ok());
  });
});
