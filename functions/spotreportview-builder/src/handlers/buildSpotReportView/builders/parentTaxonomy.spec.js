import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import parentTaxonomyBuilder from './parentTaxonomy';
import taxonomyFixture from './fixtures/taxonomy.json';
import * as taxonomyAPI from '../../../common/taxonomy';

describe('Parent Taxonomy Builder', () => {
  beforeEach(() => {
    sinon.stub(taxonomyAPI, 'getTaxonomy');
  });

  afterEach(() => {
    taxonomyAPI.getTaxonomy.restore();
  });

  const spot = deepFreeze({
    _id: '583f491b4e65fad6a7701941',
    name: 'HB Pier, Southside',
    location: {
      type: 'Point',
      coordinates: [-118.0032588192, 33.654213041213],
    },
    legacyRegionId: 2081,
  });

  const model = deepFreeze({
    name: 'HB Pier, Southside',
    lat: 33.654213041213,
    lon: -118.0032588192,
    location: {
      type: 'Point',
      coordinates: [-118.0032588192, 33.654213041213],
    },
    legacyRegionId: 2081,
    waveHeight: {
      human: false,
      min: 3,
      max: 4,
      occasional: null,
    },
    conditions: {
      human: true,
      sortableCondition: 3,
      value: 'POOR_TO_FAIR',
    },
  });

  it('adds parent taxonomy', async () => {
    taxonomyAPI.getTaxonomy.resolves(taxonomyFixture);
    const result = await parentTaxonomyBuilder(spot, model);
    expect(result).to.deep.equal({
      ...model,
      parentTaxonomy: [
        '58f7ed51dadb30820bb38782',
        '58f7ed51dadb30820bb38791',
        '58f7ed51dadb30820bb3879c',
        '5908c46bdadb30820b23c1f3',
        '58f7ed5ddadb30820bb39651',
        '58f7ed51dadb30820bb387a6',
        '58f7ed5ddadb30820bb39689',
        '58f7ed5ddadb30820bb3965c',
      ],
    });
  });
});
