import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import tideFixture from './fixtures/tides';
import * as tidesAPI from '../../../common/tides';
import tideBuilder from './tide';

describe('Tide Builder', () => {
  beforeEach(() => {
    sinon.stub(tidesAPI, 'getTides');
  });

  afterEach(() => {
    tidesAPI.getTides.restore();
  });

  it('should build the tide section of the SpotReportView', async () => {
    const spot = deepFreeze({
      _id: '583f491b4e65fad6a7701941',
      name: 'HB Pier, Southside',
      location: {
        type: 'Point',
        coordinates: [-118.0032588192, 33.654213041213],
      },
      legacyRegionId: 2081,
      timezone: 'America/Los_Angeles',
      tideStation: 'Scripps',
    });
    const now = Math.floor(new Date() / 1000);
    const utcOffset = -8;
    const model = deepFreeze({});
    tidesAPI.getTides.resolves(tideFixture);
    const result = await tideBuilder(spot, model, now, utcOffset);
    expect(result).to.have.property('tide');

    // expect(result).to.be.ok;

    // expect(result).to.deep.equal({
    //   tide: {
    //     next: {
    //       type: 'LOW',
    //       height: 1.67,
    //       timestamp: 1495743908,
    //       utcOffset: -7,
    //     },
    //     current: {
    //       height: 4.1799854745370375,
    //       timestamp: 1495571109,
    //       type: 'NORMAL',
    //       utcOffset: -7,
    //     },
    //     previous: {
    //       type: 'HIGH',
    //       height: 6.69,
    //       timestamp: 1495398308,
    //       utcOffset: -7,
    //     },
    //   },
    // });
  });
});
