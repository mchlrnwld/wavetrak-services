import moment from 'moment-timezone';
import { getLatestSpotReport } from '../../../common/reports';

const reportBuilder = async (spot, model) => {
  const spotReport = await getLatestSpotReport(spot._id);
  if (!spotReport || spotReport.stale) return model;
  if (spot.humanReport && spot.humanReport.status === 'OFF') return model;
  const utcMoment = moment.utc();
  const now = new Date(utcMoment.format());
  const localMidnight = moment(now).tz(spot.timezone).startOf('day');
  const localReportTime = moment(spotReport.createdAt).tz(spot.timezone).valueOf();
  const expired = localMidnight > localReportTime;
  return {
    ...model,
    note: spotReport.note && spotReport.note.length ? spotReport.note : null,
    waveHeight:
      spotReport.minHeight === -1 || spotReport.maxHeight === -1
        ? model.waveHeight
        : {
            ...model.waveHeight,
            human: true,
            min: spotReport.minHeight,
            max: spotReport.maxHeight,
            occasional: spotReport.occasionalHeight,
            humanRelation: spotReport.humanRelation,
            plus: spotReport.plusHeight || false,
          },
    conditions: {
      ...model.conditions,
      human: true,
      value: spotReport.rating,
      sortableCondition: spotReport.condition,
      expired,
    },
    waterTemp: {
      min: spotReport.waterTemp.min,
      max: spotReport.waterTemp.max,
    },
  };
};

reportBuilder.description = 'Report';

export default reportBuilder;
