import chai, { expect } from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import deepFreeze from 'deep-freeze';
import * as cameras from '../../../common/cameras';
import cameraBuilder from './camera';

chai.use(sinonChai);

describe('Cameras Builder', () => {
  const MOCK_CAMERA = deepFreeze({
    _id: '123f837f003dd0edd3',
    title: 'Test Cam',
    alias: 'test-cam',
    streamUrl: 'https://surfline.com/cam-stream',
    stillUrl: 'https://surfline.com/cam-still',
    pixelatedStillUrl: 'https://surfline.com/pixelated-still',
    rewindBaseUrl: 'https://surfline.com/rewind-base',
    isPremium: true,
    isPrerecorded: false,
    lastPrerecordedClipStartTimeUTC: 124758875,
    lastPrerecordedClipEndTimeUTC: 3298765456,
    supportsHighlights: true,
    supportsCrowds: true,
  });

  const spot = deepFreeze({
    _id: '583f491b4e65fad6a7701941',
    name: 'HB Pier, Southside',
    cams: ['123f837f003dd0edd3', '0987f789f098ff678'],
  });

  before(() => {
    sinon.stub(cameras, 'getCameras');
  });

  after(() => {
    cameras.getCameras.restore();
  });

  describe('with resolved cameras', () => {
    after(() => {
      cameras.getCameras.reset();
    });

    it('should create the correct object if a spot has cameras', async () => {
      cameras.getCameras.resolves([MOCK_CAMERA, { ...MOCK_CAMERA, _id: '0987f789f098ff678' }]);

      const result = await cameraBuilder(spot);

      expect(cameras.getCameras).to.have.been.calledWith([
        '123f837f003dd0edd3',
        '0987f789f098ff678',
      ]);

      const EXPECT_CAMERA = {
        _id: MOCK_CAMERA._id,
        title: MOCK_CAMERA.title,
        streamUrl: MOCK_CAMERA.streamUrl,
        stillUrl: MOCK_CAMERA.stillUrl,
        pixelatedStillUrl: MOCK_CAMERA.pixelatedStillUrl,
        rewindBaseUrl: MOCK_CAMERA.rewindBaseUrl,
        isPremium: MOCK_CAMERA.isPremium,
        isPrerecorded: MOCK_CAMERA.isPrerecorded,
        lastPrerecordedClipStartTimeUTC: MOCK_CAMERA.lastPrerecordedClipStartTimeUTC,
        lastPrerecordedClipEndTimeUTC: MOCK_CAMERA.lastPrerecordedClipEndTimeUTC,
        alias: MOCK_CAMERA.alias,
        supportsHighlights: true,
        supportsCrowds: true,
        status: {
          isDown: false,
          message: '',
          subMessage: '',
          altMessage: '',
        },
      };

      expect(result).to.deep.equal({
        cameras: [EXPECT_CAMERA, { ...EXPECT_CAMERA, _id: '0987f789f098ff678' }],
      });
    });

    it('should create the correct object if a spot has a camera which is down', async () => {
      const MOCK_STATUS_MESSAGE = 'This cam is down';
      const MOCK_STATUS_SUB_MESSAGE = 'This cam is down sub';
      const MOCK_STATUS_ALT_MESSAGE = 'This cam is down alt';
      const TEST_MOCK_CAMERA = {
        ...MOCK_CAMERA,
        isDown: {
          status: true,
          message: MOCK_STATUS_MESSAGE,
          subMessage: MOCK_STATUS_SUB_MESSAGE,
          altMessage: MOCK_STATUS_ALT_MESSAGE,
        },
      };
      const testSpot = deepFreeze({
        _id: '583f491b4e65fad6a7701941',
        name: 'HB Pier, Southside',
        lat: 33.654213041213,
        lon: -118.0032588192,
        location: {
          type: 'Point',
          coordinates: [-118.0032588192, 33.654213041213],
        },
        legacyRegionId: 2081,
        subregionId: '58581a836630e24c44878fd6',
        rank: 10,
        legacyId: 2091,
        thumbnail:
          'https://spot-thumbnails.surfline.com/spots/5842041f4e65fad6a77088ed/5842041f4e65fad6a77088ed_1500.jpg',
        cams: ['123f837f003dd0edd3'],
      });

      cameras.getCameras.resolves([TEST_MOCK_CAMERA]);

      const result = await cameraBuilder(testSpot);

      expect(result).to.deep.equal({
        cameras: [
          {
            _id: MOCK_CAMERA._id,
            title: MOCK_CAMERA.title,
            streamUrl: MOCK_CAMERA.streamUrl,
            stillUrl: MOCK_CAMERA.stillUrl,
            pixelatedStillUrl: MOCK_CAMERA.pixelatedStillUrl,
            rewindBaseUrl: MOCK_CAMERA.rewindBaseUrl,
            isPremium: MOCK_CAMERA.isPremium,
            isPrerecorded: MOCK_CAMERA.isPrerecorded,
            lastPrerecordedClipStartTimeUTC: MOCK_CAMERA.lastPrerecordedClipStartTimeUTC,
            lastPrerecordedClipEndTimeUTC: MOCK_CAMERA.lastPrerecordedClipEndTimeUTC,
            alias: MOCK_CAMERA.alias,
            supportsHighlights: true,
            supportsCrowds: true,
            status: {
              isDown: true,
              message: MOCK_STATUS_MESSAGE,
              subMessage: MOCK_STATUS_SUB_MESSAGE,
              altMessage: MOCK_STATUS_ALT_MESSAGE,
            },
          },
        ],
      });
    });

    it('should create the correct object if a spot.cam has no supports highlights or crowds flag set', async () => {
      const TEST_MOCK_CAMERA = { ...MOCK_CAMERA };

      delete TEST_MOCK_CAMERA.supportsCrowds;
      delete TEST_MOCK_CAMERA.supportsHighlights;

      const testSpot = deepFreeze({
        _id: '583f491b4e65fad6a7701941',
        name: 'HB Pier, Southside',
        lat: 33.654213041213,
        lon: -118.0032588192,
        location: {
          type: 'Point',
          coordinates: [-118.0032588192, 33.654213041213],
        },
        legacyRegionId: 2081,
        subregionId: '58581a836630e24c44878fd6',
        rank: 10,
        legacyId: 2091,
        thumbnail:
          'https://spot-thumbnails.surfline.com/spots/5842041f4e65fad6a77088ed/5842041f4e65fad6a77088ed_1500.jpg',
        cams: ['123f837f003dd0edd3'],
      });

      cameras.getCameras.resolves([TEST_MOCK_CAMERA]);

      const result = await cameraBuilder(testSpot);

      expect(result).to.deep.equal({
        cameras: [
          {
            _id: MOCK_CAMERA._id,
            title: MOCK_CAMERA.title,
            streamUrl: MOCK_CAMERA.streamUrl,
            stillUrl: MOCK_CAMERA.stillUrl,
            pixelatedStillUrl: MOCK_CAMERA.pixelatedStillUrl,
            rewindBaseUrl: MOCK_CAMERA.rewindBaseUrl,
            isPremium: MOCK_CAMERA.isPremium,
            isPrerecorded: MOCK_CAMERA.isPrerecorded,
            lastPrerecordedClipStartTimeUTC: MOCK_CAMERA.lastPrerecordedClipStartTimeUTC,
            lastPrerecordedClipEndTimeUTC: MOCK_CAMERA.lastPrerecordedClipEndTimeUTC,
            alias: MOCK_CAMERA.alias,
            supportsHighlights: false,
            supportsCrowds: false,
            status: {
              isDown: false,
              message: '',
              subMessage: '',
              altMessage: '',
            },
          },
        ],
      });
    });

    it('should correctly filter out spots with cameras that are null', async () => {
      const testSpot = deepFreeze({
        _id: '583f491b4e65fad6a7701941',
        name: 'HB Pier, Southside',
        lat: 33.654213041213,
        lon: -118.0032588192,
        location: {
          type: 'Point',
          coordinates: [-118.0032588192, 33.654213041213],
        },
        legacyRegionId: 2081,
        subregionId: '58581a836630e24c44878fd6',
        rank: 10,
        legacyId: 2091,
        thumbnail:
          'https://spot-thumbnails.surfline.com/spots/5842041f4e65fad6a77088ed/5842041f4e65fad6a77088ed_1500.jpg',
        cams: ['123f837f003dd0edd3', null, undefined],
      });

      cameras.getCameras.resolves([MOCK_CAMERA]);

      const result = await cameraBuilder(testSpot);

      expect(cameras.getCameras).to.have.been.calledWith(['123f837f003dd0edd3']);

      expect(result).to.deep.equal({
        cameras: [
          {
            _id: MOCK_CAMERA._id,
            title: MOCK_CAMERA.title,
            streamUrl: MOCK_CAMERA.streamUrl,
            stillUrl: MOCK_CAMERA.stillUrl,
            pixelatedStillUrl: MOCK_CAMERA.pixelatedStillUrl,
            rewindBaseUrl: MOCK_CAMERA.rewindBaseUrl,
            isPremium: MOCK_CAMERA.isPremium,
            isPrerecorded: MOCK_CAMERA.isPrerecorded,
            lastPrerecordedClipStartTimeUTC: MOCK_CAMERA.lastPrerecordedClipStartTimeUTC,
            lastPrerecordedClipEndTimeUTC: MOCK_CAMERA.lastPrerecordedClipEndTimeUTC,
            alias: MOCK_CAMERA.alias,
            supportsHighlights: true,
            supportsCrowds: true,
            status: {
              isDown: false,
              message: '',
              subMessage: '',
              altMessage: '',
            },
          },
        ],
      });
    });
  });

  describe('without resolved cameras', () => {
    after(() => {
      cameras.getCameras.reset();
    });

    it('should call get camera correctly if they are empty', async () => {
      const testSpot = deepFreeze({
        _id: '583f491b4e65fad6a7701941',
        name: 'HB Pier, Southside',
        lat: 33.654213041213,
        lon: -118.0032588192,
        location: {
          type: 'Point',
          coordinates: [-118.0032588192, 33.654213041213],
        },
        legacyRegionId: 2081,
        subregionId: '58581a836630e24c44878fd6',
        rank: 10,
        legacyId: 2091,
        thumbnail:
          'https://spot-thumbnails.surfline.com/spots/5842041f4e65fad6a77088ed/5842041f4e65fad6a77088ed_1500.jpg',
        cams: [],
      });

      cameras.getCameras.resolves([]);

      const result = await cameraBuilder(testSpot);

      expect(cameras.getCameras).to.have.been.calledWith([]);

      expect(result).to.deep.equal({ cameras: [] });
    });
  });
});
