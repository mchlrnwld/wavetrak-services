const spotBuilder = async (spot, model) => ({
  ...model,
  _id: spot._id,
  name: spot.name,
  lon: spot.location.coordinates[0],
  lat: spot.location.coordinates[1],
  location: spot.location,
  legacyRegionId: spot.legacyRegionId,
  subregionId: spot.subregionId,
  rank: spot.rank,
  legacyId: spot.legacyId,
  thumbnail: spot.thumbnail['1500'],
  timezone: spot.timezone,
  offshoreDirection: spot.offshoreDirection,
  abilityLevels: spot.abilityLevels,
  boardTypes: spot.boardTypes,
  relivableRating: spot.relivableRating,
});

spotBuilder.description = 'Spot';

export default spotBuilder;
