import { getTides } from '../../../common/tides';

const tideBuilder = async (spot, model, now, utcOffset) => {
  if (spot.tideStation) {
    const tides = await getTides(spot.tideStation, now);

    if (tides && tides.tides) {
      const sortedTides = tides.tides.sort((prev, next) => prev.timestamp - next.timestamp);
      const notNormal = sortedTides.filter((tide) => tide.type !== 'NORMAL');

      let previousTide = null;
      let nextTide = null;
      let currentTide = null;

      const tidesBeforeNow = sortedTides.filter((tide) => tide.timestamp <= now);
      const tidesAfterNow = sortedTides.filter((tide) => tide.timestamp >= now);

      if (tidesBeforeNow.length > 0 && tidesAfterNow.length > 0) {
        const prev = tidesBeforeNow[tidesBeforeNow.length - 1];
        const next = tidesAfterNow[0];

        switch (now) {
          case prev.timestamp:
            currentTide = prev.timestamp;
            break;
          case next.timestamp:
            currentTide = next.timestamp;
            break;
          default: {
            // interpolation based on https://github.com/Surfline/surfline-web/blob/master/kbyg/src/components/TideChart/D3TideChart.js#L66
            const timespan = now - prev.timestamp;
            const heightIncrement = next.height - prev.height;
            const timeIncrement = next.timestamp - prev.timestamp;
            const interpolatedHeight = prev.height + (timespan * heightIncrement) / timeIncrement;
            currentTide = {
              type: 'NORMAL',
              height: interpolatedHeight,
              timestamp: now,
              utcOffset: prev.utcOffset,
            };
          }
        }
      }

      for (let k = 0; k < notNormal.length && nextTide === null; k += 1) {
        const comparator = notNormal[k];
        if (comparator.timestamp < now) previousTide = comparator;
        if (comparator.timestamp > now) {
          nextTide = comparator;
        }
      }

      if (!previousTide || !currentTide || !nextTide)
        throw new Error("previous, current, or next tides don't exist");

      return {
        tide: {
          previous: {
            type: previousTide.type,
            height: previousTide.height,
            timestamp: previousTide.timestamp,
            utcOffset,
          },
          current: {
            type: currentTide.type,
            height: currentTide.height,
            timestamp: currentTide.timestamp,
            utcOffset,
          },
          next: {
            type: nextTide.type,
            height: nextTide.height,
            timestamp: nextTide.timestamp,
            utcOffset,
          },
        },
      };
    }

    throw new Error('There was a problem processing the tide station');
  }

  return null;
};

tideBuilder.description = 'Tide';

export default tideBuilder;
