import os

CLUSTER = os.environ['CLUSTER']
LOG_LEVEL = os.environ['LOG_LEVEL']
REGION = os.environ['REGION']
