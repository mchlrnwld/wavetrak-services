provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "ecs-autoscaling-task-draining/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "ecs-autoscaling-task-draining" {
  source = "../../"

  environment = "prod"
}
