provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "ecs-autoscaling-task-draining/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "ecs-autoscaling-task-draining" {
  source = "../../"

  environment = "sandbox"
}
