provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "ecs-autoscaling-task-draining/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "ecs-autoscaling-task-draining" {
  source = "../../"

  environment = "staging"
}
