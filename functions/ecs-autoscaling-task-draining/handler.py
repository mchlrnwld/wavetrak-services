import json
import logging
import time
from typing import Tuple, Union

import boto3
from lib import config

logging.basicConfig(level=config.LOG_LEVEL)
logger = logging.getLogger()
logger.setLevel(config.LOG_LEVEL)

# boto3 is very noisy at the log levels we'd like to support
# for the rest of our application
logging.getLogger('boto3').setLevel(logging.ERROR)

ECS = boto3.client('ecs', region_name=config.REGION)
ASG = boto3.client('autoscaling', region_name=config.REGION)
SNS = boto3.client('sns', region_name=config.REGION)


def find_ecs_instance_info(
    instance_id: str,
) -> Tuple[Union[str, None], Union[str, None], int]:
    paginator = ECS.get_paginator('list_container_instances')
    for list_resp in paginator.paginate(cluster=config.CLUSTER):
        arns = list_resp['containerInstanceArns']
        desc_resp = ECS.describe_container_instances(
            cluster=config.CLUSTER, containerInstances=arns
        )

        for container_instance in desc_resp['containerInstances']:
            if container_instance['ec2InstanceId'] != instance_id:
                continue

            logger.info(
                f'Found instance: id={instance_id}, '
                f'arn={container_instance["containerInstanceArn"]}, '
                f'status={container_instance["status"]}, '
                f'runningTasksCount={container_instance["runningTasksCount"]}'
            )

            return (
                container_instance['containerInstanceArn'],
                container_instance['status'],
                container_instance['runningTasksCount'],
            )
    return None, None, 0


def instance_has_running_tasks(instance_id: str) -> bool:
    (instance_arn, container_status, running_tasks) = find_ecs_instance_info(
        instance_id
    )

    if instance_arn is None:
        logger.info(
            f'Could not find instance ID {instance_id}. '
            'Letting autoscaling kill the instance.'
        )
        return False

    if container_status != 'DRAINING':
        logger.info(
            f'Setting container instance {instance_id} '
            f'({instance_arn}) to DRAINING'
        )
        ECS.update_container_instances_state(
            cluster=config.CLUSTER,
            containerInstances=[instance_arn],
            status='DRAINING',
        )

    return running_tasks > 0


def run(event, context) -> None:
    logger.info(f'Lambda triggered with the following event: {event}')

    msg = json.loads(event['Records'][0]['Sns']['Message'])

    if (
        'LifecycleTransition' not in msg.keys()
        or msg['LifecycleTransition'].find(
            'autoscaling:EC2_INSTANCE_TERMINATING'
        )
        == -1
    ):
        logger.info(
            'Exiting since the lifecycle transition is not '
            'EC2_INSTANCE_TERMINATING.'
        )
        return

    if instance_has_running_tasks(msg['EC2InstanceId']):
        logger.info(
            f'Tasks are still running on instance {msg["EC2InstanceId"]}; '
            'posting msg to SNS topic '
            f'{event["Records"][0]["Sns"]["TopicArn"]}'
        )
        time.sleep(5)
        sns_resp = SNS.publish(
            TopicArn=event['Records'][0]['Sns']['TopicArn'],
            Message=json.dumps(msg),
            Subject='Publishing SNS message to invoke Lambda again.',
        )
        logger.info(f'Posted message {sns_resp["MessageId"]} to SNS topic.')

    else:
        logger.info(
            f'No tasks are running on instance {msg["EC2InstanceId"]}; '
            'setting lifecycle to complete'
        )
        ASG.complete_lifecycle_action(
            LifecycleHookName=msg['LifecycleHookName'],
            AutoScalingGroupName=msg['AutoScalingGroupName'],
            LifecycleActionResult='CONTINUE',
            InstanceId=msg['EC2InstanceId'],
        )
