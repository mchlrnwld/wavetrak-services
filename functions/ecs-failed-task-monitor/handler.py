import json
import logging
import os

import requests


def lambda_handler(event, context):
    """
    Accepts event from cloudwatch events bridge.
    Reformats and sends event to New Relic using the Events API.
    Arguments:
        event: Events Bridge event object.
        context: Lambda context.
    """

    logger = logging.getLogger('log_cloudwatch_event')
    logger.setLevel(logging.INFO)

    EVENTS_ENDPOINT = (
        f'https://insights-collector.newrelic.com/v1/accounts/{os.environ["NEW_RELIC_ACCOUNT_ID"]}/events'
    )

    try:
        payload = {
            'eventType': 'InfrastructureEvent',
            'eventDetail': 'ecsTaskMonitor',
            'time': event['time'],
            'cluster': event['detail']['clusterArn'],
            'task': event['detail']['taskArn'],
        }
        headers = {
            'Content-Type': 'application/json',
            'X-Insert-Key': f'{os.environ["NEWRELIC_EVENTS_KEY"]}',
        }

    except AttributeError as error:
        logger.warn('Can\'t assign event attributes to payload.')
        logger.warn(error)

    try:
        resp = requests.post(
            EVENTS_ENDPOINT, data=json.dumps(payload), headers=headers
        )
        logger.info(f'Sent the following event to New Relic: {resp.json()}')

    except IOError as error:
        logger.warn('Request to New Relic endpoint failed:')
        logger.warn(error)
