ifndef ENV
	ENV := sandbox
endif

all: format lint type-check invoke-local deploy
.PHONY: all

build:
	docker build -t frame-extractor-ml-trigger-lambda-test ./src
.PHONY: build

run:
	docker run -p 9000:8080 -d --name=frame-extractor-ml-trigger-lambda-test --env-file src/testEnv.env frame-extractor-ml-trigger-lambda-test
.PHONY: run

invoke-local:
	make build  && make run
	- rm result.json
	docker cp ./fixtures frame-extractor-ml-trigger-lambda-test:var/task/fixtures
	curl -XPOST "http://localhost:9000/2015-03-31/functions/function/invocations" -d @fixtures/testEvent.json >> result.json
	- docker logs --tail 1000 frame-extractor-ml-trigger-lambda-test
	docker stop frame-extractor-ml-trigger-lambda-test
	docker rm frame-extractor-ml-trigger-lambda-test
.PHONY: invoke-local

test:
	make build && make run
	docker cp ./fixtures frame-extractor-ml-trigger-lambda-test:var/task/fixtures
	docker cp ./src/test_main.py frame-extractor-ml-trigger-lambda-test:var/task
	- docker exec -it frame-extractor-ml-trigger-lambda-test pytest . -s -vv
	docker stop frame-extractor-ml-trigger-lambda-test
	docker rm frame-extractor-ml-trigger-lambda-test
.PHONY: test

setup:
	conda devenv
	conda activate frame-extractor-ml-trigger
.PHONY: setup

format:
	@autoflake --remove-all-unused-imports --remove-unused-variables --ignore-init-module-imports --recursive --in-place .
	@isort -rc .
	@black .
.PHONY: format

lint:
	@flake8 .
	@black --check .
.PHONY: lint

type-check:
	@mypy ./src
.PHONY: type-check

deploy:
# uses env var AWS_PROFILE too
	cd src && serverless deploy -v -s ${ENV}
.PHONY: deploy

snake:
	cd src && camel-snake-pep8 . *.py */*.py
.PHONY: snake
