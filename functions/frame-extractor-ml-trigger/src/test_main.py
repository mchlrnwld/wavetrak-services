import json
import re
import shutil

import boto3  # type: ignore
import pytest  # type: ignore
import responses  # type: ignore
from moto import mock_batch, mock_iam, mock_s3, mock_sqs  # type: ignore

from lib import config
from main import main

RECORDING_BASE_URL = 'http://test-cameras-service.com/cameras/recording/id'
ESCAPED_RECORDING_BASE_URL = RECORDING_BASE_URL.replace('.', r'\.')
RECORDING_BASE_REGEX = re.compile(f'{ESCAPED_RECORDING_BASE_URL}.*')
CDN_URL = 'https://camstills.cdn-surfline.com'
RECORDING_ID = '58349c1fe411dc743a5d52ad'


def get_all_keys_from_bucket(bucket: str):
    s3 = boto3.resource('s3')
    bucket_client = s3.Bucket(bucket)
    return list(map(lambda obj: obj.key, bucket_client.objects.all()))


def get_requests(path, calls):
    return [
        call.request for call in calls if call.request.url.startswith(path)
    ]


@pytest.fixture(scope='function')
def sqs_client():
    with mock_sqs():
        client = boto3.client('sqs', region_name='us-west-1')
        queue = client.create_queue(QueueName='sl-crowds-queue-test')
        config.CROWDS_QUEUE_URL = queue['QueueUrl']
        yield client


@pytest.fixture(scope='function')
def batch():
    with mock_iam():
        iam = boto3.client('iam')
        role = iam.create_role(
            RoleName='test_iam', AssumeRolePolicyDocument='string',
        )
        iam_arn = role.get('Role').get('Arn')

        with mock_batch():
            client = boto3.client("batch", region_name='us-west-1')

            batch = client.create_compute_environment(
                computeEnvironmentName='test-compute-env',
                type='UNMANAGED',
                serviceRole=iam_arn,
            )
            compute_environment_arn = batch.get('computeEnvironmentArn')
            client.create_job_queue(
                jobQueueName=config.HIGHLIGHTS_QUEUE,
                state='ENABLED',
                priority=1,
                computeEnvironmentOrder=[
                    {
                        'order': 1,
                        'computeEnvironment': compute_environment_arn,
                    },
                ],
            )
            client.register_job_definition(
                jobDefinitionName=config.HIGHLIGHTS_JOB_DEF,
                type='container',
                containerProperties={
                    'image': 'string',
                    'vcpus': 123,
                    'memory': 123,
                },
            )
            yield client


def get_crowds_messages(sqs_client):
    event = sqs_client.receive_message(
        QueueUrl=config.CROWDS_QUEUE_URL, MaxNumberOfMessages=10
    )
    return event.get('Messages', [])


@pytest.fixture
def create_s3_buckets():
    with mock_s3():
        s3_client = boto3.resource('s3')
        s3_client.create_bucket(Bucket=config.CAM_THUMBNAIL_BUCKET,)
        s3_client.create_bucket(Bucket=config.CLIP_BUCKET_ORIGIN,)
        yield s3_client


def upload_clip(filename):
    if filename != 'live/wc-hbpierns.stream.20210208T212408646.mp4':
        shutil.copy2(
            'fixtures/live/wc-hbpierns.stream.20210208T212408646.mp4',
            f'fixtures/{filename}',
        )


@pytest.fixture
def intercept_requests():
    with responses.RequestsMock(assert_all_requests_are_fired=True) as rsps:
        responses.add_passthru(re.compile('.*localhost.*'))
        rsps.add(
            responses.POST,
            re.compile(RECORDING_BASE_REGEX),
            body=f'{{"_id": "{RECORDING_ID}"}}',
        )
        yield rsps


def intercept_validate_alias_request(
    rsps, alias, id, supportsHighlights=False, supportsCrowds=False
):
    return_body = {"alias": alias, "_id": id}

    if supportsHighlights or supportsCrowds:
        return_body.update(
            {
                "supportsHighlights": supportsHighlights,
                "supportsCrowds": supportsCrowds,
                "timezone": "America/Los_Angeles",
                "location": {
                    "type": "Point",
                    "coordinates": [-117.68393581858636, 33.460893819308694],
                },
            }
        )
    rsps.add(
        rsps.GET,
        f'{config.CAMERAS_API}/cameras/validatealias?alias={alias}',
        headers={'Content-Type': 'application/json'},
        body=json.dumps(return_body),
    )


def test_highlights_cam(
    create_s3_buckets, sqs_client, intercept_requests, batch
):
    alias = 'wc-hbpierns'
    cam_id = '583499c4e411dc743a5d5296'
    clip_name = f'live/{alias}.stream.20210208T212408646.mp4'
    upload_clip(clip_name)
    intercept_validate_alias_request(
        intercept_requests, alias, cam_id, True, True
    )
    with open('fixtures/highlightsEvent.json') as event:
        main(json.loads(event.read()), None)

    rewind_thumbs = get_all_keys_from_bucket(config.CAM_THUMBNAIL_BUCKET)
    expected_keys_rewind = [
        f'{cam_id}/2021/02/08/{cam_id}.20210208T212408_full.jpg',
        f'{cam_id}/2021/02/08/{cam_id}.20210208T212408_small.jpg',
        f'{cam_id}/latest_full.jpg',
        f'{cam_id}/latest_small.jpg',
    ]
    assert all(
        expected_key in rewind_thumbs for expected_key in expected_keys_rewind
    )

    requests = intercept_requests.calls
    print(requests[1].request.url)
    recording_requests = get_requests(
        f'{RECORDING_BASE_URL}/{cam_id}', requests
    )
    assert len(recording_requests) == 1
    cam_thumb_prefix = f'{CDN_URL}/{cam_id}/2021/02/08/{cam_id}'
    assert json.loads(recording_requests[0].body) == {
        'alias': alias,
        'thumbLargeUrl': f'{cam_thumb_prefix}.20210208T212408_full.jpg',
        'thumbSmallUrl': f'{cam_thumb_prefix}.20210208T212408_small.jpg',
        'recordingUrl': f'https://camrewinds.cdn-surfline.com/{clip_name}',
        'startDate': 1612819448000.0,
        'endDate': 1612819560000.0,
        'resolution': '1280x720',
    }

    crowds_messages = get_crowds_messages(sqs_client)
    assert len(crowds_messages) == 1
    print(crowds_messages)
    assert (
        json.loads(crowds_messages[0]['Body'])['recordingId'] == RECORDING_ID
    )

    jobs = batch.list_jobs(jobQueue=config.HIGHLIGHTS_QUEUE)

    print(jobs)
    assert len(jobs['jobSummaryList']) == 1


def test_highlights_only_cam(
    create_s3_buckets, sqs_client, intercept_requests, batch
):
    alias = 'wc-hbpierns'
    cam_id = '583499c4e411dc743a5d5296'
    clip_name = f'live/{alias}.stream.20210208T212408646.mp4'
    upload_clip(clip_name)
    intercept_validate_alias_request(
        intercept_requests, alias, cam_id, True, False
    )
    with open('fixtures/highlightsEvent.json') as event:
        main(json.loads(event.read()), None)

    rewind_thumbs = get_all_keys_from_bucket(config.CAM_THUMBNAIL_BUCKET)
    expected_keys_rewind = [
        f'{cam_id}/2021/02/08/{cam_id}.20210208T212408_full.jpg',
        f'{cam_id}/2021/02/08/{cam_id}.20210208T212408_small.jpg',
        f'{cam_id}/latest_full.jpg',
        f'{cam_id}/latest_small.jpg',
    ]
    assert all(
        expected_key in rewind_thumbs for expected_key in expected_keys_rewind
    )

    requests = intercept_requests.calls
    recording_requests = get_requests(
        f'{RECORDING_BASE_URL}/{cam_id}', requests
    )
    assert len(recording_requests) == 1
    cam_thumb_prefix = f'{CDN_URL}/{cam_id}/2021/02/08/{cam_id}'
    assert json.loads(recording_requests[0].body) == {
        'alias': alias,
        'thumbLargeUrl': f'{cam_thumb_prefix}.20210208T212408_full.jpg',
        'thumbSmallUrl': f'{cam_thumb_prefix}.20210208T212408_small.jpg',
        'recordingUrl': f'https://camrewinds.cdn-surfline.com/{clip_name}',
        'startDate': 1612819448000.0,
        'endDate': 1612819560000.0,
        'resolution': '1280x720',
    }

    crowds_messages = get_crowds_messages(sqs_client)
    assert len(crowds_messages) == 0

    jobs = batch.list_jobs(jobQueue=config.HIGHLIGHTS_QUEUE)

    assert len(jobs['jobSummaryList']) == 1


def test_crowd_only_cam(
    create_s3_buckets, sqs_client, intercept_requests, batch
):
    alias = 'wc-upperstrestles'
    cam_id = '5834a1ab3421b20545c4b598'
    clip_name = f'live/{alias}.stream.20210208T212408646.mp4'
    upload_clip(clip_name)
    intercept_validate_alias_request(
        intercept_requests, alias, cam_id, False, True
    )
    with open('fixtures/crowdOnlyEvent.json') as event:
        main(json.loads(event.read()), None)

    rewind_thumbs = get_all_keys_from_bucket(config.CAM_THUMBNAIL_BUCKET)
    expected_keys_rewind = [
        f'{cam_id}/2021/02/08/{cam_id}.20210208T212408_full.jpg',
        f'{cam_id}/2021/02/08/{cam_id}.20210208T212408_small.jpg',
        f'{cam_id}/latest_full.jpg',
        f'{cam_id}/latest_small.jpg',
    ]
    assert all(
        expected_key in rewind_thumbs for expected_key in expected_keys_rewind
    )

    requests = intercept_requests.calls

    recording_requests = get_requests(
        f'{RECORDING_BASE_URL}/{cam_id}', requests
    )
    assert len(recording_requests) == 1
    cam_thumb_prefix = f'{CDN_URL}/{cam_id}/2021/02/08/{cam_id}'
    assert json.loads(recording_requests[0].body) == {
        'alias': alias,
        'thumbLargeUrl': f'{cam_thumb_prefix}.20210208T212408_full.jpg',
        'thumbSmallUrl': f'{cam_thumb_prefix}.20210208T212408_small.jpg',
        'recordingUrl': f'https://camrewinds.cdn-surfline.com/{clip_name}',
        'startDate': 1612819448000.0,
        'endDate': 1612819560000.0,
        'resolution': '1280x720',
    }

    crowds_messages = get_crowds_messages(sqs_client)

    assert len(crowds_messages) == 1

    assert (
        json.loads(crowds_messages[0]['Body'])['recordingId'] == RECORDING_ID
    )

    jobs = batch.list_jobs(jobQueue=config.HIGHLIGHTS_QUEUE)

    assert len(jobs['jobSummaryList']) == 0


def test_non_ml_cam(create_s3_buckets, sqs_client, intercept_requests, batch):
    alias = 'uk-whitby'
    cam_id = '5a21a8e5f20eb500195b40c7'
    intercept_validate_alias_request(
        intercept_requests, alias, cam_id, False, False
    )
    clip_name = f'live/{alias}.stream.20210208T232408646.mp4'
    # also night time to be sure we're still producing rewinds at night
    upload_clip(clip_name)
    with open('fixtures/nonMLEvent.json') as event:
        main(json.loads(event.read()), None)

    rewind_thumbs = get_all_keys_from_bucket(config.CAM_THUMBNAIL_BUCKET)
    expected_keys_rewind = [
        f'{cam_id}/2021/02/08/{cam_id}.20210208T232408_full.jpg',
        f'{cam_id}/2021/02/08/{cam_id}.20210208T232408_small.jpg',
        f'{cam_id}/latest_full.jpg',
        f'{cam_id}/latest_small.jpg',
    ]
    assert all(
        expected_key in rewind_thumbs for expected_key in expected_keys_rewind
    )

    requests = intercept_requests.calls

    recording_requests = get_requests(
        f'{RECORDING_BASE_URL}/{cam_id}', requests
    )
    assert len(recording_requests) == 1
    cam_thumb_prefix = f'{CDN_URL}/{cam_id}/2021/02/08/{cam_id}'
    assert json.loads(recording_requests[0].body) == {
        'alias': alias,
        'thumbLargeUrl': f'{cam_thumb_prefix}.20210208T232408_full.jpg',
        'thumbSmallUrl': f'{cam_thumb_prefix}.20210208T232408_small.jpg',
        'recordingUrl': f'https://camrewinds.cdn-surfline.com/{clip_name}',
        'startDate': 1612826648000.0,
        'endDate': 1612826760000.0,
        'resolution': '1280x720',
    }

    crowds_messages = get_crowds_messages(sqs_client)
    assert len(crowds_messages) == 0

    jobs = batch.list_jobs(jobQueue=config.HIGHLIGHTS_QUEUE)

    assert len(jobs['jobSummaryList']) == 0


def test__highlight_cam_night_time(
    create_s3_buckets, sqs_client, intercept_requests, batch
):
    alias = 'wc-hbpierns'
    cam_id = '583499c4e411dc743a5d5296'
    intercept_validate_alias_request(
        intercept_requests, alias, cam_id, False, False
    )

    clip_name = f'live/{alias}.stream.20210208T082408646.mp4'
    upload_clip(clip_name)
    with open('fixtures/nightHighlightEvent.json') as event:
        main(json.loads(event.read()), None)

    rewind_thumbs = get_all_keys_from_bucket(config.CAM_THUMBNAIL_BUCKET)
    expected_keys_rewind = [
        f'{cam_id}/2021/02/08/{cam_id}.20210208T082408_full.jpg',
        f'{cam_id}/2021/02/08/{cam_id}.20210208T082408_small.jpg',
        f'{cam_id}/latest_full.jpg',
        f'{cam_id}/latest_small.jpg',
    ]

    assert all(
        expected_key in rewind_thumbs for expected_key in expected_keys_rewind
    )

    requests = intercept_requests.calls

    recording_requests = get_requests(
        f'{RECORDING_BASE_URL}/{cam_id}', requests
    )
    assert len(recording_requests) == 1
    cam_thumb_prefix = f'{CDN_URL}/{cam_id}/2021/02/08/{cam_id}'
    assert json.loads(recording_requests[0].body) == {
        'alias': alias,
        'thumbLargeUrl': f'{cam_thumb_prefix}.20210208T082408_full.jpg',
        'thumbSmallUrl': f'{cam_thumb_prefix}.20210208T082408_small.jpg',
        'recordingUrl': f'https://camrewinds.cdn-surfline.com/{clip_name}',
        'startDate': 1612772648000.0,
        'endDate': 1612772760000.0,
        'resolution': '1280x720',
    }
    crowds_messages = get_crowds_messages(sqs_client)
    assert len(crowds_messages) == 0

    jobs = batch.list_jobs(jobQueue=config.HIGHLIGHTS_QUEUE)

    assert len(jobs['jobSummaryList']) == 0
