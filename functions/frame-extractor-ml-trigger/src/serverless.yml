service: frame-extractor-ml-trigger

custom:
  env: ${opt:stage}
  oldAwsProfile:
    # Needed to handle sandbox/dev discrepancies in the lower tier.
    # This is so that all new resources created by Serverless in AWS contain the `sandbox` convention.
    # However, some existing AWS resources (such as Parameter store) still use the `dev` convention.
    sandbox: dev
    staging: dev
    prod: prod
  awsRegion: us-west-1
  vpcSettings:
    securityGroupIds:
      sandbox:
        - sg-90aeaef5 # Surfline Core Internal security group.
      staging:
        - sg-90aeaef5 # Surfline Core Internal security group.
      prod:
        - sg-a5a0e5c0 # Surfline Core Internal security group.
    subnetIds:
      sandbox:
        - subnet-0909466c
        - subnet-f2d458ab
      staging:
        - subnet-0909466c
        - subnet-f2d458ab
      prod:
        - subnet-d1bb6988
        - subnet-85ab36e0
  application: frame-extractor-ml-trigger
  company: Wavetrak
  provisionedBy: Serverless
  logLevel:
    sandbox: INFO
    staging: INFO
    prod: WARN
  cdnBaseDomain:
    sandbox: sandbox.surfline.com
    staging: staging.surfline.com
    prod: cdn-surfline.com
  awsAccountNumbers:
    sandbox: "665294954271"
    staging: "665294954271"
    prod: "833713747344"
  awsAccountNumber: ${self:custom.awsAccountNumbers.${self:custom.env}}
  snsTopic: arn:aws:sns:us-west-1:${self:custom.awsAccountNumber}:sl-live-cam-archive-s3-sns_${self:custom.env}
  sqsQueue: arn:aws:sqs:us-west-1:${self:custom.awsAccountNumber}:wt-crowd-counting-service-sqs-${self:custom.env}

provider:
  name: aws
  ecr:
    images:
      frame-extractor-ml-trigger:
        path: ./
  iamRoleStatements:
    - Effect: 'Allow'
      Action:
        - sqs:SendMessage
      Resource:
        - ${self:custom.sqsQueue}
    - Effect: 'Allow'
      Action:
        - 'batch:SubmitJob'
      Resource:
        - 'arn:aws:batch:*:*:job-definition/*'
        - 'arn:aws:batch:*:*:job-queue/*'
    - Effect: Allow
      Action:
        - s3:GetObject
        - s3:GetObjectAcl
        - s3:HeadObject
        - s3:ListBucket
        - s3:ListMultipartUploadParts
        - s3:PutObject
        - s3:PutObjectAcl
      Resource:
        - 'arn:aws:s3:::sl-live-cam-archive-${self:custom.env}'
        - 'arn:aws:s3:::sl-cam-thumbnails-high-freq-${self:custom.env}'
        - 'arn:aws:s3:::sl-cam-thumbnails-${self:custom.env}'
        - 'arn:aws:s3:::sl-live-cam-archive-${self:custom.env}/*'
        - 'arn:aws:s3:::sl-cam-thumbnails-high-freq-${self:custom.env}/*'
        - 'arn:aws:s3:::sl-cam-thumbnails-${self:custom.env}/*'

  region: ${self:custom.awsRegion}

functions:
  frame-extractor-ml-trigger:
    image: frame-extractor-ml-trigger
    runtime: python3.8
    maximumRetryAttempts: 0
    timeout: 90 #Long timeout so we can see what the error was
    memorySize: 450
    name: ${self:service}-${self:custom.application}-${self:custom.env}
    events:
      - sns:
          arn: ${self:custom.snsTopic}
    vpc:
      securityGroupIds: ${self:custom.vpcSettings.securityGroupIds.${self:custom.env}}
      subnetIds: ${self:custom.vpcSettings.subnetIds.${self:custom.env}}
    tags:
      Company: ${self:custom.company}
      Application: ${self:custom.application}
      Environment: ${self:custom.env}
      Service: ${self:service}
      Provisioned-By: ${self:custom.provisionedBy}
    environment:
      NEW_RELIC_LICENCE_KEY: ${ssm:/${self:custom.oldAwsProfile.${self:custom.env}}/common/NEWRELIC_API_KEY~true}
      NEW_RELIC_ACCOUNT_ID: ${ssm:/${self:custom.oldAwsProfile.${self:custom.env}}/common/NEW_RELIC_ACCOUNT_ID~true}
      LOG_LEVEL: ${self:custom.logLevel.${self:custom.env}}
      CAMERAS_API: http://cameras-service.${self:custom.env}.surfline.com
      THUMBNAIL_URL: https://camstills.${self:custom.cdnBaseDomain.${self:custom.env}}
      SOURCE_REWIND_URL: https://camrewinds.${self:custom.cdnBaseDomain.${self:custom.env}}
      CROWDS_QUEUE_URL: ${ssm:/${self:custom.env}/services/crowd-counting-service/CROWD_COUNTING_QUEUE_URL~true}
      CLIP_BUCKET_ORIGIN: sl-live-cam-archive-${self:custom.env}
      CAM_THUMBNAIL_BUCKET: sl-cam-thumbnails-${self:custom.env}
      HIGHLIGHTS_QUEUE: wt-jobs-common-general-purpose-gpu-job-queue-${self:custom.env}
      HIGHLIGHTS_JOB_DEF: wt-cam-highlights-job-${self:custom.env}
      HIGHLIGHTS_JOB_PREFIX: wt-cam-highlights-${self:custom.env}-
      CLIP_URL_PREFIX: https://sl-live-cam-archive-${self:custom.env}.s3.us-west-1.amazonaws.com
      ENV: ${self:custom.env}
