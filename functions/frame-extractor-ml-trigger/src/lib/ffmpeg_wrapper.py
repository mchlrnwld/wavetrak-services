# -*- coding: utf-8 -*-
""" Ffmpeg API wrapper methods module """

import logging
import os
import re
import tempfile
from typing import Tuple

import newrelic.agent  # type: ignore

from lib import config, utils
from lib.custom_errors import NewRelicError

application = newrelic.agent.application()

# Define location of included ffmpeg binaries
FFMPEG = os.path.join(config.LAMBDA_TASK_ROOT, 'ffmpeg')
FFPROBE = os.path.join(config.LAMBDA_TASK_ROOT, 'ffprobe')


class FfmpegWrapper:
    def __init__(self, logger: logging.Logger):
        # ref so folder is only deleted when class obj is destroyed
        self.temp_dir_obj = tempfile.TemporaryDirectory()
        self.temp_dir = self.temp_dir_obj.name
        self.logger = logger

    def resize_thumb(
        self, thumbnail: str, size: int = config.IMG_SIZE_SMALL
    ) -> str:
        """
        Resize an image to a given size.
        """
        img_out_path = thumbnail.replace('.jpg', f'_{size}.jpg')
        cmd = [
            FFMPEG,
            '-loglevel',
            'error',
            '-nostats',
            '-i',
            thumbnail,
            '-vf',
            f'scale={size}:-1',
            '-y',
            img_out_path,
        ]

        utils.run_subprocess(cmd)
        return img_out_path

    def create_thumbnail(self, recording_file: str, clip_time: int = 0) -> str:
        """
        Given a clip filename and path creates a thumbnail
        image in several resolutions, adding their paths
        to a list.
        """
        img_out_path = f'{self.temp_dir}/hi-resThumbs'
        if not os.path.exists(img_out_path):
            os.mkdir(img_out_path)
        img_out_path = f'{img_out_path}/{clip_time}.jpg'
        self.logger.info(f'Creating thumbnail for {img_out_path}')
        minutes = str(clip_time).zfill(2)

        cmd = [
            FFMPEG,
            '-loglevel',
            'error',
            '-nostats',
        ]

        if clip_time > 0:
            # fast seeking fails when clip is less than 1 min long
            cmd += [
                '-ss',
                f'00:{minutes}:00',
            ]
        cmd += [
            '-i',
            recording_file,
            '-vframes',
            '1',
            '-y',
            img_out_path,
        ]

        utils.run_subprocess(cmd)

        return img_out_path

    def get_video_spec(
        self, recording_file: str, alias: str
    ) -> Tuple[str, str, str]:
        """
        Use ffprobe to extract information about a recording,
        given its URL.
        returns width, height and duration in seconds
        """
        cmd = [
            FFPROBE,
            "-v",
            "error",
            "-select_streams",
            "v:0",
            "-show_entries",
            "stream=height,width,duration",
            "-of",
            "flat=s=_",
            recording_file,
        ]
        video_specs = utils.run_subprocess(cmd)

        lines = re.split('\\n', video_specs.decode("utf-8"))
        spec_lines = [
            line.replace('streams_stream_0_', '').split('=')
            for line in lines
            if line.startswith('streams_stream_0_')
        ]

        if len([line for line in lines if line != '']) > len(spec_lines):
            # if there are more lines than just the ones with the specs then
            # there is an error output,if we have the spec then we can proceed
            # but this is an indication of an issue with the cam so we need to
            # report
            self.logger.error(video_specs)
            newrelic.agent.record_custom_event(
                'video_spec_error',
                {
                    "recording_file": recording_file,
                    "output": video_specs,
                    "alias": alias,
                },
                application,
            )
        specs = {}

        for line in spec_lines:
            if len(line) == 2:
                specs[line[0]] = line[1].replace('"', '')

        width = specs.get('width', '0')
        height = specs.get('height', '0')
        duration_seconds = specs.get('duration', '0')

        if not all([width, height, duration_seconds]):
            # when ffmpeg returns nothing it means the clip
            # was either empty or completely broken
            raise NewRelicError(
                'video_spec_error',
                {
                    "recording_file": recording_file,
                    "output": video_specs,
                    "alias": alias,
                },
            )

        return (width, height, duration_seconds)
