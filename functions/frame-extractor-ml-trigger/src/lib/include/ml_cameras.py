# temp object till this information is in the cameras api
ml_cameras = [
    {
        "_id": "583499cb3421b20545c4b53f",
        "alias": "wc-hbss",
        "supportsHighlights": True,
        "timezone": "America/Los_Angeles",
        "location": {
            "type": "Point",
            "coordinates": [-118.00291423423009, 33.65475101379567],
        },
    },
    {
        "_id": "58a37530c9d273fd4f581beb",
        "alias": "wc-lowers",
        "supportsHighlights": True,
        "timezone": "America/Los_Angeles",
        "location": {
            "type": "Point",
            "coordinates": [-117.5884311499704, 33.38143999849023],
        },
    },
    {
        "_id": "5834981be411dc743a5d5286",
        "alias": "wc-dohenysb",
        "supportsHighlights": True,
        "timezone": "America/Los_Angeles",
        "location": {
            "type": "Point",
            "coordinates": [-117.68534915898404, 33.4609858001748],
        },
    },
    {
        "_id": "583499c4e411dc743a5d5296",
        "alias": "wc-hbpierns",
        "timezone": "America/Los_Angeles",
        "supportsHighlights": True,
        "location": {
            "type": "Point",
            "coordinates": [-118.00397, 33.65574085629463],
        },
    },
    {
        "_id": "58349fe73421b20545c4b57e",
        "alias": "wc-saltcreek",
        "timezone": "America/Los_Angeles",
        "supportsHighlights": True,
        "location": {
            "type": "Point",
            "coordinates": [-117.72290996901745, 33.475793399792984],
        },
    },
    {
        "_id": "5834945f3421b20545c4b519",
        "alias": "wc-thirtysixnewport",
        "timezone": "America/Los_Angeles",
        "supportsHighlights": False,
        "location": {
            "coordinates": [-117.93676257133484, 33.61555743705257],
            "type": "Point",
        },
    },
    {
        "_id": "5834949d3421b20545c4b51c",
        "alias": "wc-fiftyfournewport",
        "timezone": "America/Los_Angeles",
        "supportsHighlights": False,
        "location": {
            "coordinates": [-117.94555485248566, 33.62226710774577],
            "type": "Point",
        },
    },
    {
        "_id": "583494b1e411dc743a5d5268",
        "alias": "wc-fiftysixnewport",
        "timezone": "America/Los_Angeles",
        "supportsHighlights": False,
        "location": {
            "coordinates": [-117.94685304164886, 33.6227227388759],
            "type": "Point",
        },
    },
    {
        "_id": "583496a0e411dc743a5d5276",
        "alias": "wc-cstreet",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "type": "Point",
            "coordinates": [-119.2997646331, 34.272041739333],
        },
    },
    {
        "_id": "58349a5c3421b20545c4b542",
        "alias": "wc-hbpiernsov",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "type": "Point",
            "coordinates": [-118.00397, 33.65574085629463],
        },
    },
    {
        "_id": "58349bffe411dc743a5d52ab",
        "alias": "wc-lowerjetties",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "type": "Point",
            "coordinates": [-117.93456174239209, 33.61223684311246],
        },
    },
    {
        "_id": "58349df43421b20545c4b561",
        "alias": "wc-newportjetties",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "coordinates": [-117.94419765472412, 33.62125756352127],
            "type": "Point",
        },
    },
    {
        "_id": "58349dfee411dc743a5d52be",
        "alias": "wc-newportpierss",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "type": "Point",
            "coordinates": [-117.9315376281, 33.60577342582],
        },
    },
    {
        "_id": "5834a005e411dc743a5d52de",
        "alias": "wc-thepointsanonofre",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "type": "Point",
            "coordinates": [-117.56579160690308, 33.3726134926491],
        },
    },
    {
        "_id": "5834a1613421b20545c4b591",
        "alias": "wc-wedge",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "type": "Point",
            "coordinates": [-117.8819918632, 33.5930302087],
        },
    },
    {
        "_id": "5834a1ab3421b20545c4b598",
        "alias": "wc-upperstrestles",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "type": "Point",
            "coordinates": [-117.5944462901005, 33.38445596302903],
        },
    },
    {
        "_id": "58bde3d6f5eced643004de93",
        "alias": "wc-dohenyrivermouth",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "type": "Point",
            "coordinates": [-117.68393581858636, 33.460893819308694],
        },
    },
    {
        "_id": "58bde3f1f5eced643004de94",
        "alias": "wc-boneyard",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "coordinates": [-117.68919467926025, 33.459981047849084],
            "type": "Point",
        },
    },
    {
        "_id": "5978cb2518fd6daf0da062ae",
        "alias": "wc-hbpiersclose",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "coordinates": [-118.00316870212555, 33.65544147271585],
            "type": "Point",
        },
    },
    {
        "_id": "599b3a435a46a5d8045a0cf6",
        "alias": "wc-church",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "type": "Point",
            "coordinates": [-117.5789400389241, 33.379622288757],
        },
    },
    {
        "_id": "5a2036a6096c27001ac4f18b",
        "alias": "wc-riverjettiesnorth",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "type": "Point",
            "coordinates": [-117.9593911234855, 33.62778647706099],
        },
    },
    {
        "_id": "5a203785096c27001ac4f18c",
        "alias": "wc-blackiesclose",
        "supportsHighlights": False,
        "timezone": "America/Los_Angeles",
        "location": {
            "coordinates": [-117.9311192035675, 33.60891872680435],
            "type": "Point",
        },
    },
]
