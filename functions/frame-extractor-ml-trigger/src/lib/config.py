# -*- coding: utf-8 -*-
""" Config value constants """

from os import environ

AWS_REGION = environ['AWS_REGION']  # This is supplied in the lambda env.

# Logging
LOG_LEVEL = environ.get('LOG_LEVEL', default='INFO')
ENV = environ['ENV']

# Local config
TIMESTAMP_FORMAT = '%Y%m%dT%H%M%S%f'
IMG_SIZE_SMALL = 360

# Cam API
CAMERAS_API = environ['CAMERAS_API']
THUMBNAIL_URL = environ['THUMBNAIL_URL']
SOURCE_REWIND_URL = environ['SOURCE_REWIND_URL']

# AWS
CLIP_BUCKET_ORIGIN = environ['CLIP_BUCKET_ORIGIN']
CAM_THUMBNAIL_BUCKET = environ['CAM_THUMBNAIL_BUCKET']
CLIP_URL_PREFIX = environ['CLIP_URL_PREFIX']

# Highlights
HIGHLIGHTS_QUEUE = environ['HIGHLIGHTS_QUEUE']
HIGHLIGHTS_JOB_DEF = environ['HIGHLIGHTS_JOB_DEF']
HIGHLIGHTS_JOB_PREFIX = environ['HIGHLIGHTS_JOB_PREFIX']

# Crowds
CROWDS_QUEUE_URL = environ['CROWDS_QUEUE_URL']

# LAMBDA_TASK_ROOT is part of the lambda execution environment
# https://docs.aws.amazon.com/lambda/latest/dg/current-supported-versions.html
LAMBDA_TASK_ROOT = environ['LAMBDA_TASK_ROOT']
