import logging

import newrelic.agent  # type: ignore

from lib import config, utils
from lib.aws_wrapper import S3Bucket, send_sqs_msg, submit_job
from lib.camera import Camera
from lib.custom_errors import NewRelicError
from lib.ffmpeg_wrapper import FfmpegWrapper

application = newrelic.agent.application()


class Clip:
    """
    Class that represents everything regarding a clip
    Arguments:
        event: an sns event that is used to initialise the Clip
    Attributes:
        bucket: The name of the S3 bucket where the clip is held
        object_key: The object key of the clip in S3
        camera: The camera that captured the clip
        start_timestamp: The start timestamp of the clip
        width: The width of the clip
        height: The height of the clip
        duration_seconds: The duration of the clip in seconds
    """

    def __init__(
        self, bucket: str, object_key: str, alias: str, logger: logging.Logger
    ):
        self.bucket = bucket
        self.object_key = object_key
        self.logger = logger
        logger.info(f'Received: {self.object_key}')
        self.camera = Camera(alias, self.logger)

        if self.camera:
            # init ffmpeg
            self.ffmpeg = FfmpegWrapper(logger)
            self.recording_file = f'{config.CLIP_URL_PREFIX}/{self.object_key}'
            (
                self.width,
                self.height,
                self.duration_seconds,
            ) = self.ffmpeg.get_video_spec(
                self.recording_file, self.camera.alias
            )
            duration = float(self.duration_seconds)
            if duration < 10:
                raise NewRelicError(
                    'short_rewind',
                    {
                        "duration": self.duration_seconds,
                        "recording_file": self.recording_file,
                        "alias": self.camera.alias,
                    },
                )
            if duration > 900:
                raise NewRelicError(
                    'long_rewind',
                    {
                        "duration": self.duration_seconds,
                        "recording_file": self.recording_file,
                        "alias": self.camera.alias,
                    },
                )
            recording_start_timestamp = self.object_key.split('.')[-2]
            self.start_timestamp = utils.timestamp_to_epoch(
                recording_start_timestamp
            )
            if self.camera.is_ml():
                self.logger.info(f'{self.camera.alias} is an ML cam')
                self.is_daylight = self.camera.is_daylight(
                    self.start_timestamp
                )

    def store_rewind_still(self, image_size: str, thumb: str) -> str:
        """
        Upload thumbnails to the S3 bucket.

        Thumbnails are added as archives, using date / time from original
        file: e.g. ${bucket}/acam/2018/02/13/acam_20180213T1930_full.jpg
        """

        s3 = S3Bucket(config.CAM_THUMBNAIL_BUCKET)

        date = utils.epoch_to_timestamp(
            self.start_timestamp, timestamp_format='%Y/%m/%d'
        )
        start_timestamp = utils.epoch_to_timestamp(self.start_timestamp)
        file_name = f"{self.camera.id}.{start_timestamp}_{image_size}.jpg"
        archive_name = f"{self.camera.id}/{date}/{file_name}"

        s3.upload_image(thumb, archive_name)

        # Update Primary Thumbnail for camera.
        primary_thumbnail = f"{self.camera.id}/latest_{image_size}.jpg"

        s3.upload_image(thumb, primary_thumbnail)

        return archive_name

    def generate_rewind_thumbs(self):
        full_size_thumb = self.ffmpeg.create_thumbnail(self.recording_file)
        self.full_image_path = self.store_rewind_still('full', full_size_thumb)
        small_thumb = self.ffmpeg.resize_thumb(full_size_thumb)
        self.small_image_path = self.store_rewind_still('small', small_thumb)

    def submit_highlights_batch(self):
        # Get the time of the clip using the object key
        time = utils.epoch_to_timestamp(self.start_timestamp)
        camera_id = self.camera.id
        key = f'{camera_id}-{time}'
        job_name = config.HIGHLIGHTS_JOB_PREFIX + key
        return submit_job(
            job_name,
            self.object_key,
            config.HIGHLIGHTS_QUEUE,
            config.HIGHLIGHTS_JOB_DEF,
            self.recording_id,
        )

    def submit_crowds_sqs(self):
        send_sqs_msg(
            config.CROWDS_QUEUE_URL, {"recordingId": self.recording_id}
        )

    def trigger_ml(self):
        if self.is_daylight:
            if self.camera.supports_crowds:
                self.submit_crowds_sqs()
                self.logger.info(
                    f'Successfully triggered crowds for: {self.object_key}'
                )
            if self.camera.supports_highlights:
                self.submit_highlights_batch()
                self.logger.info(
                    f'Successfully triggered batch job for: {self.object_key}'
                )

    def save(self):
        """
        Post the thumbnail data to the recording URL.
        """
        try:
            self.generate_rewind_thumbs()
        except Exception as e:
            output = e.output if hasattr(e, 'output') else e  # type: ignore
            newrelic.agent.record_custom_event(
                'thumbnail_generation_error',
                {
                    "recording_file": self.recording_file,
                    "output": output,
                    "alias": self.camera.alias,
                },
                application,
            )
            self.logger.error(output)
            # thumbnails shouldn't be blocking
            self.full_image_path = ''
            self.small_image_path = ''

        data = {
            'alias': self.camera.alias,
            'thumbLargeUrl': f'{config.THUMBNAIL_URL}/{self.full_image_path}',
            'thumbSmallUrl': f'{config.THUMBNAIL_URL}/{self.small_image_path}',
            'recordingUrl': f'{config.SOURCE_REWIND_URL}/{self.object_key}',
            'startDate': self.start_timestamp,
            'endDate': self.start_timestamp
            + int(float(self.duration_seconds)) * 1000,
            'resolution': f'{self.width}x{self.height}',
        }

        # POST recording info.
        recording = utils.post_request(
            config.CAMERAS_API, f'cameras/recording/id/{self.camera.id}', data
        )

        self.recording_id = recording['_id']

        self.logger.info(
            f'Successfully saved recording, recording_id: {self.recording_id}'
        )

        # done at the end so that the rewind stuff can't be affected
        if self.camera.is_ml():
            self.trigger_ml()

        self.logger.info(f'Successfully saved: {self.object_key}')
