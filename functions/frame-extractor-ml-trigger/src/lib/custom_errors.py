import newrelic.agent  # type: ignore

application = newrelic.agent.application()


class NewRelicError(Exception):
    def __init__(self, name, params):
        super().__init__(name)
        self.params = params
        newrelic.agent.record_custom_event(name, params, application)
