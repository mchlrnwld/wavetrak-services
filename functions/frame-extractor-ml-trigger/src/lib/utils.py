# -*- coding: utf-8 -*-
""" Utils methods module """
import calendar
import json
import logging
import os
import subprocess
import time
from datetime import datetime
from typing import Dict, List, Tuple, Union
from urllib.parse import urljoin

import requests

from lib import config

logger = logging.getLogger()


def get_s3_details_from_sns_message(event: dict) -> Tuple[str, str]:
    """
    Given an sns event, return the bucket name and object key as a tuple
    """
    s3_event = json.loads(event['Records'][0]['Sns']['Message'])['Records'][0][
        's3'
    ]
    return (s3_event['bucket']['name'], s3_event['object']['key'])


def get_request(domain: str, endpoint: str) -> Dict:
    """
    Given an URL, send a HTTP GET request, returning
    a JSON object.
    """
    url = urljoin(domain, endpoint)
    r = requests.get(url)
    r.raise_for_status()
    logger.debug(f'GET request to {url} was successful.')

    return r.json()


def post_request(
    domain: str, endpoint: str, data: Union[Dict, List, str]
) -> Dict:
    """
    Given a domain and endpoint strings, and a dict of data,
    send a HTTP POST request, returning a JSON response.
    """
    url = urljoin(domain, endpoint)
    r = requests.post(url, json=data)
    logger.info(f'POST {url} with {data}: {r.status_code}')
    r.raise_for_status()

    return r.json()


def timestamp_to_epoch(
    timestamp: str, timestamp_format: str = config.TIMESTAMP_FORMAT
) -> float:
    """
    Given a timestamp and (optionally) a timestamp format,
    return the unix time in milliseconds.
    """
    if len(timestamp) < 16:
        # The file is in the old format.
        timestamp = f'{timestamp}000'
    date = time.strptime(timestamp, timestamp_format)
    unix_millisec = calendar.timegm(date) * 1000.0

    logger.debug(
        f'Timestamp {timestamp} converted to epoch in ms: {unix_millisec}.'
    )

    return unix_millisec


def epoch_to_timestamp(
    epoch_in_ms: Union[int, float], timestamp_format: str = '%Y%m%dT%H%M%S'
) -> str:
    """
    Given a unix epoch time in milliseconds and (optionally)
    a timestamp format, return a UTC timestamp.
    """
    epoch = epoch_in_ms / 1000.0
    timestamp = datetime.utcfromtimestamp(epoch).strftime(timestamp_format)
    logger.debug(
        'Epoch {0} converted to timestamp {1}.'.format(epoch, timestamp)
    )

    return timestamp


def run_subprocess(cmd: List[str]) -> bytes:
    """
    Given a list of subprocess commands and flags,
    a success message and an error message, run a
    subprocess thread for that cmd.
    """
    joined_command = ' '.join(cmd)

    logger.info(f'Running subprocess: {joined_command}')
    with open(os.devnull) as devnull:
        try:
            output = subprocess.check_output(
                cmd, stdin=devnull, stderr=subprocess.STDOUT
            )
        except subprocess.CalledProcessError as e:
            logger.error(e.output)
            raise e
    logger.debug(f'Command {joined_command}: success.')
    return output
