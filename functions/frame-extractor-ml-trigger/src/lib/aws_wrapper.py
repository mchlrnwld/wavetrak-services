# -*- coding: utf-8 -*-
""" AWS API wrapper methods module """

from lib import config
import json
import logging
from typing import Dict

import boto3  # type: ignore

# boto3 is very noisy at the log levels we'd like to support
# for the rest of our application
logging.getLogger('botocore').setLevel(logging.ERROR)
logging.getLogger('boto3').setLevel(logging.ERROR)

logger = logging.getLogger()


def send_sqs_msg(url: str, body: Dict):
    """
        Given a JSON SQS body message
        compose an SQS message, sending it back to the queue.
    """
    sqs = boto3.client(service_name='sqs', region_name=config.AWS_REGION)

    response = sqs.send_message(QueueUrl=url, MessageBody=json.dumps(body),)

    logger.info(
        f'SQS message ID {response.get("MessageId")} '
        'sent back to the queue.'
    )


def submit_job(
    job_name: str, input_file: str, queue: str, job_def: str, recording_id: str
):
    """
    Submit a batch job using the boto3 client.

    Returns:
        None, but logs on submission.
    """
    logger.info(f'Received for batch job trigger: {input_file}')

    # Submit the batch job
    batch = boto3.client(service_name='batch', region_name=config.AWS_REGION)
    response = batch.submit_job(
        jobName=job_name,
        jobQueue=queue,
        jobDefinition=job_def,
        parameters={'recording_id': recording_id},
    )
    logger.info(response)

    job_id = response['jobId']
    logger.info(f'Submitted job: {job_id}')


class S3Bucket:
    """
    Class to wrap S3 methods for a specific bucket
    """

    def __init__(self, bucket):
        self.bucket = bucket
        self.s3 = boto3.client('s3')

    def download_file(self, key: str, filename: str):
        """
        downloads file from s3 bucket where key is the s3 object key
        and filename is the desired destination
        """
        self.s3.download_file(self.bucket, key, filename)

    def upload_image(self, filename: str, destination: str):
        """
        Given an asset name and a folder structure,
        uploads it to a S3 bucket.
        """
        extra_args = {'ContentType': 'image', 'ACL': 'public-read'}

        self.s3.upload_file(
            filename, self.bucket, destination, ExtraArgs=extra_args,
        )
        logger.info(
            '{0} saved at {1} with {2}.'.format(
                filename, destination, extra_args
            )
        )
