import logging
from datetime import datetime
from typing import Dict

import astral  # type: ignore
import pytz
from astral.sun import sun  # type: ignore
from requests.exceptions import HTTPError

from lib import config, utils
from lib.custom_errors import NewRelicError


class Camera:
    """
    Class that represents everything regarding a camera
    Arguments:
            alias: alias for the camera
    Attributes:
            id: The id of the camera
            alias: The alias of the camera
            lat: The latitude of the camera
            lon: The longitude of the camera
            supports_crowds: Does the cam support crowd counting
            supports_highlights: Does the cam support smart highlights
            timezone: The timezone of the cam
    """

    def __init__(self, alias: str, logger: logging.Logger):
        self.logger = logger
        try:
            camera = utils.get_request(
                config.CAMERAS_API, f'cameras/validatealias?alias={alias}'
            )
            self._from_dict(camera)
            self.logger.info(f"Camera ID: {self.id}")
        except HTTPError as e:
            if e.response.status_code == 404:
                raise NewRelicError('missing_alias', {"alias": alias})
            else:
                raise e

    def _from_dict(self, camera: Dict):
        """
        Initializes class from dict
        """
        self.id = camera['_id']
        self.alias = camera['alias']
        self.supports_crowds = camera.get('supportsCrowds', False)
        self.supports_highlights = camera.get('supportsHighlights', False)
        location = camera.get('location')
        self.timezone = str(camera.get('timezone'))
        if location:
            self.lat = location['coordinates'][1]
            self.lon = location['coordinates'][0]
        else:
            self.lat = None
            self.lon = None

    def is_ml(self) -> bool:
        """
        Is the camera used for ml
        """
        return self.supports_crowds or self.supports_highlights

    def is_daylight(self, timestamp: float) -> bool:
        """
        Check if clip is during daylight hours
        at a specific camera spot.

        sets the property on the class
        """
        # Construct custom location
        location = astral.Observer(self.lat, self.lon, elevation=10,)

        # Use Civil Twilight
        location.solar_depression = 'civil'

        dt = datetime.fromtimestamp(
            timestamp / 1000, pytz.timezone(self.timezone)
        )

        sunlight = sun(location, date=dt)
        now = dt.timestamp()

        self.logger.info(
            f'Time of clip is {now} , '
            f'Dawn is: {sunlight["dawn"].timestamp()} , '
            f'Dusk is {sunlight["dusk"].timestamp()}'
        )

        daylight = (
            sunlight['dawn'].timestamp() < now < sunlight['dusk'].timestamp()
        )

        self.logger.info(
            f"{'Daytime' if daylight else 'Nighttime'} at: {self.alias}"
        )

        return daylight
