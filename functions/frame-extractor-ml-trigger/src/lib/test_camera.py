import json
import logging

import pytest  # type: ignore
import responses  # type: ignore

from lib import config
from lib.camera import Camera
from lib.utils import timestamp_to_epoch


@pytest.fixture
def rsps():
    with responses.RequestsMock() as rsps:
        yield rsps


def intercept_validate_alias_request(
    rsps, alias, id, supportsHighlights=False, supportsCrowds=False
):
    return_body = {"alias": alias, "_id": id}

    if supportsHighlights or supportsCrowds:
        return_body.update(
            {
                "supportsHighlights": supportsHighlights,
                "supportsCrowds": supportsCrowds,
                "timezone": "America/Los_Angeles",
                "location": {
                    "type": "Point",
                    "coordinates": [-117.68393581858636, 33.460893819308694],
                },
            }
        )
    rsps.add(
        rsps.GET,
        f'{config.CAMERAS_API}/cameras/validatealias?alias={alias}',
        adding_headers={'Content-Type': 'application/json'},
        body=json.dumps(return_body),
    )


def test_is_daylight(rsps):
    alias = 'wc-hbpierns'
    cam_id = '583499c4e411dc743a5d5296'
    intercept_validate_alias_request(rsps, alias, cam_id, True, True)
    test_cam = Camera(alias, logging.getLogger())
    # just before dusk
    daylight = test_cam.is_daylight(timestamp_to_epoch('20210629T033016421'))
    assert daylight is True
    # just after dawn
    daylight = test_cam.is_daylight(timestamp_to_epoch('20210629T122016421'))
    assert daylight is True
