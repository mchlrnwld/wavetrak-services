import logging  # noqa E402
import os
import re

import newrelic.agent  # type: ignore # noqa E402
from newrelic_lambda import lambda_handler  # type: ignore # noqa E402

from lib import config  # noqa E402
from lib.clip import Clip  # noqa E402
from lib.custom_errors import NewRelicError  # noqa E402
from lib.utils import get_s3_details_from_sns_message  # noqa E402

os.environ.setdefault(
    "NEW_RELIC_APP_NAME", os.getenv("AWS_LAMBDA_FUNCTION_NAME", "")
)
os.environ.setdefault("NEW_RELIC_NO_CONFIG_FILE", "true")
os.environ.setdefault("NEW_RELIC_DISTRIBUTED_TRACING_ENABLED", "true")
os.environ.setdefault("NEW_RELIC_SERVERLESS_MODE_ENABLED", "true")
os.environ.setdefault(
    "NEW_RELIC_TRUSTED_ACCOUNT_KEY", os.getenv("NEW_RELIC_ACCOUNT_ID", "")
)


newrelic.agent.initialize()

logger = logging.getLogger()
logger.setLevel(config.LOG_LEVEL)
logging.basicConfig(level=config.LOG_LEVEL)

ALIAS_FROM_OBJECT_KEY_REGEX = r'\/([A-Za-z0-9-]+)'


@lambda_handler.lambda_handler()
def main(event, context=None):
    object_key = None
    try:
        (bucket, object_key) = get_s3_details_from_sns_message(event)
        alias = re.findall(ALIAS_FROM_OBJECT_KEY_REGEX, object_key)[0]

        if not alias:
            raise AttributeError(
                'Unable to parse alias from stream name '
                f'for object key {object_key}.'
            )

        newrelic.agent.add_custom_parameter('Alias', alias)

        # Initiate helper functions
        clip = Clip(bucket, object_key, alias, logger)

        # Update data to s3 and cameras-service and trigger batch.
        clip.save()

    except NewRelicError as e:
        logger.error(
            f'Known Exception logged to new relic as custom event '
            f'{str(e)} with {str(e.params)}'
        )
    except Exception as e:
        logger.critical(f'ERROR processing: {object_key}')
        raise e
    return True


# no need to catch all errors here as the serverless config has retries
# set to 0
