# Frame Extractor Highlights Trigger Lambda

AWS lambda function deployed with serverless in a docker container

## Setting up for development

Install Conda (https://docs.conda.io/projects/conda/en/latest/user-guide/install/macos.html)

Install Conda dovenv (https://conda-devenv.readthedocs.io/en/latest/installation.html)

```
npm install -g serverless@2.39.2
```
serverless must be installed globally, we don't need any other specific packages for serverless as we're building the docker container with all the python dependencies so serverless doesn't need to handle that

to bring in the additional resources (ffmpeg and the test clip) run `make setup`. This will also create and activate the conda env that's used for linting and formatting.

## Running the Lambda locally

You just run `make test` which builds the docker container, (this installs all the python dependencies) runs the container and executes the lambda with 4 events. These cover:
- a cam that is used for both highlights and crowds, this should:
  - trigger a batch job for highlights
  - create 1 per min thumbnails for crowds
  - post to the crowds endpoint for each thumb
  - create thumbs for rewinds (1st one from crowds and same image again but compressed)
  - post to the recording endpoint with the thumbs, clip and clip details
- a cam that's just for crowds
  - create 1 per min thumbnails for crowds
  - post to the crowds endpoint for each thumb
  - create thumbs for rewinds (1st one from crowds and same image again but compressed)
  - post to the recording endpoint with the thumbs, clip and clip details
- a cam that's for for highlights and crowds but where it's night time
  - create thumbs for rewinds (1st one from crowds and same image again but compressed)
  - post to the recording endpoint with the thumbs, clip and clip details
- a cam that's not used for ml
  - create thumbs for rewinds (1st one from crowds and same image again but compressed)
  - post to the recording endpoint with the thumbs, clip and clip details

because this lambda has so many external dependencies we can't really run this any other way because we need the test runner to intercept requests and create test s3 buckets. we also can't use serverless's `invoke local` as that doesn't spin up the docker container so the lambda doesn't end up having ffmpeg or the right python environment.

This test runner also copies the thumbnails it's created to the local folder `thumbs` from the container before decommissioning it. You can add a profiler to the test runner by adding `pytest-profiling` to the `requirements.txt` and adding `--profile` to the line in the make file that runs pytest. The results from this get copied off the docker container to a folder called `prof` which you can run with `snakeviz prof` locally after installing `pip install snakeviz` to visualize them.

## Writing some code

Code should comply with the styleguide https://github.com/Surfline/styleguide/blob/master/python/README.md

We have some tools that will help you do that, you can run `make format` to format whitespace issues etc and if you've not made everything apart from Classes snake case you can run `make snake` however !!CAUTION!! this can cause some issues as it renames all your variables so make sure you commit all your work and push it up before running this so you can easily roll back. Also make sure you run the tests before and after each of those commands to be sure they haven't broken the code

## Deploying
 If not using your default aws profile set the aws profile name e.g. `export AWS_PROFILE=surfline-dev` and then append the environment name to the end of the make command e.g. `make deploy ENV=sandbox`. Be careful that these match up as there's no check to prevent you from deploying this to prod with sandbox naming!