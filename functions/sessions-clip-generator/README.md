<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents <!-- omit in toc -->

- [Surfline Rewind Clip Generator](#surfline-rewind-clip-generator)
  - [Table of contents <!-- omit in toc -->](#table-of-contents----omit-in-toc---)
  - [Summary](#summary)
  - [Introduction](#introduction)
  - [Development](#development)
    - [Create a virtual environment](#create-a-virtual-environment)
    - [environment](#environment)
    - [Running the App locally](#running-the-app-locally)
  - [AWS deployment](#aws-deployment)
    - [Deploying App as a Lambda Function](#deploying-app-as-a-lambda-function)
    - [Testing the flow in AWS](#testing-the-flow-in-aws)
    - [Debugging Errors](#debugging-errors)
  - [Monitoring](#monitoring)
    - [Configuration](#configuration)
    - [Triggering the Alert](#triggering-the-alert)
  - [Contributing](#contributing)
    - [Committing new code](#committing-new-code)
  - [More documentation](#more-documentation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Surfline Rewind Clip Generator

This package provides the camera **rewind clip generator application**, which is currently being deployed as an AWS lambda function described under  `services/cameras-service/infra`)

## Summary

This application's goal is to retrieve camera rewind videos within two NTP UTC timestamps (as specified [here](https://wavetrak.atlassian.net/wiki/spaces/TAG/pages/442400941/Add+ability+to+get+cam+rewinds+by+two+NTP+UTC+timestamps)), cropping them with FFMPEG, and delivering a final clip to the viewer.

## Introduction

![diagram](docs/lambda_diagram.png)

As we see in this diagram, this application performs the following steps:

1. (Diagram Step 4) Receive an SQS event requesting a clip for a given time interval. An example of SQS event is the following:

    ```json
    {
      "Records": [
        {
          "body": "{'clipId': '507f191e810c19729de860ea', 'retryTimestamps': [], 'cameraId': '583499c4e411dc743a5d5296', 'startTimestampInMs': 1537119363000, 'endTimestampInMs': 1537119423000}",
          "receiptHandle": "MessageReceiptHandle",
          "md5OfBody": "7b270e59b47ff90a553787216d55d91d",
          "eventSourceARN": "arn:aws:sqs:us-west-1:123456789012:MyQueue",
          "eventSource": "aws:sqs",
          "awsRegion": "us-west-1",
          "messageId": "19dd0b57-b21e-4ac1-bd88-01bbb068cb78",
          "attributes": {
            "ApproximateFirstReceiveTimestamp": "1523232000001",
            "SenderId": "123456789012",
            "ApproximateReceiveCount": "1",
            "SentTimestamp": "1523232000000"
          },
          "messageAttributes": {
            "SentTimestamp": "1523232000000"
          }
        }
      ]
    }
    ```

2. (Diagram Step 5) Call the camera API with the endpoint `/cameras/{id}` to retrieve a camera alias for the given camera id (e.g. `hbpiernscam`).

3. (Diagram Step 5) Call the camera API with the endpoint `/cameras/recording/` to retrieve a list of cam rewind source files within the given time range. An example of request URL:

    ```bash
    http://cameras-service.prod.surfline.com/cameras/recording/hbpiernscam?startDate=1537113617000&endDate=1537114457000
    ```

    Which would generate this response:

    ```json
    [{
        "startDate":"2018-09-16T16:00:17.000Z",
        "endDate":"2018-09-16T16:10:17.000Z",
        "thumbLargeUrl":"https://camstills.cdn-surfline.com/hbpiernscam/2018/09/16/hbpiernscam.20180916T160017_full.jpg",
        "recordingUrl":"https://camrewinds.cdn-surfline.com/live/hbpiernscam.20180916T160017.mp4",
        "thumbSmallUrl":"https://camstills.cdn-surfline.com/hbpiernscam/2018/09/16/hbpiernscam.20180916T160017_small.jpg",
        "alias":"hbpiernscam"
      }]
    ```

4. (Diagram Step 6) Retrieve the cam rewind source files from the origin S3 bucket (downloading them on disk).

5. (Diagram Step 7) Use ffmpeg to trim and merge clips into a single clip and to create several thumbnails.

6. (Diagram Step 8) If the clips are available, store them in the destination S3 bucket.

7. If the clips are not available, send a SQS message back to the queue, similar to the initial SQS, with a visibility timeout.

8. (Diagram Step 9/10) Call the camera API with endpoint `/cameras/clips` to update the information about the new clip and send a SNS message with the resulting metadata. An example of SNS message:

    ```json
    {
      "clipId": "507f191e810c19729de860ea",
      "cameraId": "583499c4e411dc743a5d5296",
      "startTimestampInMs": 1534305591000,
      "endTimestampInMs": 1534305611000,
      "status": "CLIP_AVAILABLE",
      "bucket": "sl-cam-clip-archive-prod",
      "clip": {
        "url": "https://clips.cdn-surfline.com/583499c4e411dc743a5d5296/507f191e810c19729de860ea.mp4",
        "key": "/583499c4e411dc743a5d5296/507f191e810c19729de860ea.mp4"
      },
      "thumbnail": {
        "url": "https://clips.cdn-surfline.com/583499c4e411dc743a5d5296/507f191e810c19729de860ea_{size}.png",
        "key": "/583499c4e411dc743a5d5296/507f191e810c19729de860ea_{size}.png",
        "sizes": [300, 640, 1500, 3000]
      }
    }
    ```

## Development

To add new features to this application, follow these steps:

### Create a virtual environment

(for linting purposes only, the actual runtime is configured in docker and uses pip)

```bash
conda devenv
conda activate sessions-clip-generator-function
```

### environment


| Constant              | Definition                                                                              |
| :-------------------- | :-------------------------------------------------------------------------------------- |
| TIMESTAMP_FORMAT      | The timestamp we will be parsing from the clip name strings                             |
| SQS_RETRY_LIMIT       | The limit, in seconds, of retries for CLIP PENDING (default: 15 minutes)                |
| OUT_OF_RANGE_LIMIT    | The limit, in seconds, of how back in the past clips can be retrieved (default: 3 days) |
| CAMERAS_API      | The url where the camera services is available                                          |
| CLIP_URL              | The url where the clips are posted to, accordingly to the environment                   |
| THUMBNAIL_SIZES       | List of values for which clip thumbnails need to be created                             |
| VIDEO_MAX_LEN         | Maximum length allowed for a clip                                                       |
| S3_BUCKET_DESTINATION | AWS S3 bucket where the clips will be upload to.                                        |
| AWS_SNS_TOPIC         | AWS SNS topic arn                                                                       |
| AWS_SQS_QUEUE_URL     | AWS SQS queue url                                                                       |
| SQS_TIMEOUT           | AWS SQS invisibility timeout in seconds                                                 |


### Running the App locally

```bash
make test
```

Once finished, the results will be in the `results` folder

## AWS deployment

### Deploying App as a Lambda Function

This uses serverless to deploy the lambda function, set your `AWS_PROFILE` to the right one for the env your deploying to as well as the `ENV` parameter for `make deploy` e.g.

```bash
export AWS_PROFILE=surfline-dev
make deploy ENV=sandbox
```

### Testing the flow in AWS

You can test this application flow in sandbox and/or staging environment following theses steps:

1. In the [SQS dashboard](https://console.aws.amazon.com/sqs/home?region=us-west-1), select SQS queue (e.g. `sl-cameras-service-generate-clip-sqs-staging`) and click `Queue action -> Send a Message`.

1. Type the value for `body`, similarly as the a message created in `event.json`. For instance:

    ```json
    {
      clipId: "5bbba560b52a3b7b61500426",
      retryTimestamps: [],
      cameraId: "583499c4e411dc743a5d5296",
      startTimestampInMs: 1632398935000,
      endTimestampInMs: 1632398938000
    }
    ```

1. This should trigger the lambda function and you should see the clips and thumbnails in the environment's S3 bucket (e.g [sl-cameras-service-clips-cdn-staging](https://s3.console.aws.amazon.com/s3/buckets/sl-cameras-service-clips-cdn-staging/?region=us-west-1&tab=overview)) in around 20-40 seconds.

### Debugging Errors

Errors will be logged in [CloudWatch](https://us-west-1.console.aws.amazon.com/cloudwatch/home?region=us-west-1#logs:). To make sense of logs in the CLI, you should install [saw](https://github.com/TylerBrock/saw).

For instance, to check error logs for staging in the last hour:

```bash
saw get /aws/lambda/sl-cameras-service-generate-clip-staging --start -1h --filter error
```

## Monitoring

This Lambda has a [New Relic alert policy](https://one.newrelic.com/launcher/nrai.launcher?pane=eyJuZXJkbGV0SWQiOiJhbGVydGluZy11aS1jbGFzc2ljLnBvbGljaWVzIiwibmF2IjoiUG9saWNpZXMiLCJzZWxlY3RlZEZpZWxkIjoidGhyZXNob2xkcyIsInBvbGljeUlkIjoiMjQ4MzMzIiwiY29uZGl0aW9uSWQiOiIxMDAxNzY2OSJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucmFpLm5hdmlnYXRpb24tYmFyIiwibmF2IjoiUG9saWNpZXMifQ==&platform[accountId]=356245) that
monitors the invocation error count.

[this will change once we have deployed the serverless version]

### Configuration

Use your judgment when adjusting the thresholds.

**Note:** Because of reporting delays from AWS to New Relic, the `aggregation window` value should be set to 20 minutes.

### Triggering the Alert

If you need to trigger the alert for testing purposes, you can use the AWS console to configure a Lambda test event that contains a null event object (`{}`). The Lambda will throw the following error in response:

```json
{
  "stackTrace": [
    [
      "/var/task/lib/routes/root.py",
      30,
      "handler",
      "response = clip_generator_trigger.handler(event, context)"
    ],
    [
      "/var/task/lib/handlers/clip_generator_trigger.py",
      241,
      "handler",
      "event_body, sent_ts = extract_event_data(event)"
    ],
    [
      "/var/task/lib/handlers/clip_generator_trigger.py",
      37,
      "extract_event_data",
      "raise Exception(error)"
    ]
  ],
  "errorType": "Exception",
  "errorMessage": "Ill-formatted event message: 'Records'"
}
```

Invoke that event repeatedly using the "Test" button until you reach the error threshold configured in the policy. After the offset window has elapsed, New Relic will trigger an alert.

## Contributing

### Committing new code

Run unit tests with:

```bash
make test
```

then manually compare the outputs (in the `results` folder) with those in `tests/fixtures/expected_clips`)

When deploying scripts (or to report back to Github on PRs), we ensure that code follows style guidelines with:

```bash
make lint
```

To fix lint errors, use:

```bash
make format
```

typcheck with :

```bash
make type-check
```

ensure snake case with:

```bash
make snake
```


## More documentation

- [Project Confluence Page](https://wavetrak.atlassian.net/wiki/spaces/TAG/pages/442400941/Add+ability+to+get+cam+rewinds+by+two+NTP+UTC+timestamps).
