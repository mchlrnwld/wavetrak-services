# -*- coding: utf-8 -*-
""" Root service handler module for AWS Lambda function """
import json
import logging

import newrelic.agent  # type: ignore # noqa E402
from newrelic_lambda import lambda_handler  # type: ignore # noqa E402

from lib import config
from lib.clip_wrapper import ClipWrapper
from lib.config import LOG_LEVEL

newrelic.agent.initialize()

logging.basicConfig(level=LOG_LEVEL)
logger = logging.getLogger()
logger.setLevel(config.LOG_LEVEL)


@lambda_handler.lambda_handler()
def main(event, context=None):

    logger.info(f'Event: {json.dumps(event, indent=4, sort_keys=True)}')

    newrelic.agent.add_custom_parameter('Event Length', len(event["Records"]))

    # batch size should be 1 but just incase
    for record in event["Records"]:
        clip = ClipWrapper(record, logger)
        # Obviously this will update for each record
        # but if it errors we'll have the right id for the error
        newrelic.agent.add_custom_parameter('camId', clip.camera_id)
        newrelic.agent.add_custom_parameter('clipId', clip.clip_id)
        newrelic.agent.add_custom_parameter('clipDuration', clip.duration)
        clip.generate_clip()
    return
