# -*- coding: utf-8 -*-
""" AWS Wrapper Test Module """

import logging

import mock

import lib.aws_wrapper

filename = 'filename_test'
destination = 'destination_test'
clip_metadata = {'test': 'test'}


class TestAwsWrapper:
    @mock.patch('lib.aws_wrapper.boto3')
    def test_upload_clip_boto(self, boto3):
        aw = lib.aws_wrapper.AwsWrapper(logging.getLogger())
        aw.upload_asset(filename, destination)
        boto3.client.assert_called_with('s3', region_name='us-west-1')

    @mock.patch('lib.aws_wrapper.boto3')
    def test_send_sns_msg_boto(self, boto3):
        aw = lib.aws_wrapper.AwsWrapper(logging.getLogger())
        aw.send_sns_msg(clip_metadata)
        boto3.client.assert_called_with('sns', region_name='us-west-1')
