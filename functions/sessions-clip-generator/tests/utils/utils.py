import json
import os

import boto3  # type: ignore
import httpretty  # type: ignore
import pytest  # type: ignore
from moto import mock_s3, mock_sns, mock_sqs  # type: ignore
from moto.core import ACCOUNT_ID  # type: ignore

from lib import config
from tests.fixtures.generate_fixtures import generate_response
from tests.utils.test_consts import (
    CAM_ID,
    CLIP_ID,
    CLIP_URL,
    RECORDING_BASE_URL,
    TEST_SNS_QUEUE,
)


def get_all_keys_from_bucket(bucket: str):
    s3 = boto3.resource('s3')
    bucket_client = s3.Bucket(bucket)
    return list(map(lambda obj: obj.key, bucket_client.objects.all()))


def get_url(req):
    return req.headers.get('Host', '') + req.path


def get_requests(url, requests):
    '''
        Gets all requests that were intercepted for a given url
    '''
    return [req for req in requests if get_url(req) == url]


def get_messages(sqs_client, queue=config.AWS_SQS_QUEUE_URL):
    event = sqs_client.receive_message(QueueUrl=queue, MaxNumberOfMessages=10)
    return event.get('Messages', [])


def check_all_files_in_s3(expected_body, test_name):
    files = get_all_keys_from_bucket(config.s3_bucket_destination)
    expected_keys = [
        *[
            expected_body['thumbnail']['key'].format(size=size)
            for size in expected_body['thumbnail']['sizes']
        ],
        expected_body['clip']['key'],
    ]
    for expected_key in expected_keys:
        assert expected_key in files

    os.makedirs(f'results/{test_name}', exist_ok=True)
    s3_client = boto3.resource('s3')
    for key in expected_keys:
        download_file = f'results/{test_name}/{key.split("/")[-1]}'
        s3_client.Bucket(config.s3_bucket_destination).download_file(
            key, download_file
        )


def generate_successful_expected(start, end):
    return {
        'cameraId': CAM_ID,
        'clipId': CLIP_ID,
        'endTimestampInMs': end,
        'retryTimestamps': [],
        'startTimestampInMs': start,
        'status': 'CLIP_AVAILABLE',
        'bucket': 'sl-cameras-service-clips-cdn-test',
        'clip': {
            'key': f'{CAM_ID}/{CLIP_ID}.mp4',
            'url': f'{config.CLIP_URL}/{CAM_ID}/{CLIP_ID}.mp4',
        },
        'thumbnail': {
            'key': f'{CAM_ID}/{CLIP_ID}_{{size}}.jpg',
            'sizes': [300, 640, 1500, 3000],
            'url': f'{config.CLIP_URL}/{CAM_ID}/{CLIP_ID}_{{size}}.jpg',
        },
    }


@pytest.fixture
def create_s3_buckets():
    with mock_s3():
        s3_client = boto3.resource('s3')
        s3_client.create_bucket(Bucket=config.s3_bucket_destination)
        yield s3_client


@pytest.fixture(scope='function')
def sqs_client():
    with mock_sqs():
        client = boto3.client('sqs', region_name='us-west-1')
        queue = client.create_queue(
            QueueName='sl-cameras-service-generate-clip-sqs-test'
        )
        config.AWS_SQS_QUEUE_URL = queue['QueueUrl']

        client.create_queue(QueueName=TEST_SNS_QUEUE)
        yield client


@pytest.fixture(scope='function')
def sns_client():
    with mock_sns():
        client = boto3.client('sns', region_name='us-west-1')
        topic = client.create_topic(
            Name='sl-cameras-service-generate-clip-sns-test'
        )
        config.AWS_SNS_TOPIC = topic['TopicArn']

        client.subscribe(
            TopicArn=topic['TopicArn'],
            Protocol="sqs",
            Endpoint=(f"arn:aws:sqs:us-west-1:{ACCOUNT_ID}:test-sns-receiver"),
        )
        yield client


@pytest.fixture
def intercept_requests():
    httpretty.reset()
    httpretty.enable()
    httpretty.register_uri(
        httpretty.PUT, CLIP_URL, body='{}',
    )


def intercept_recording_request(start, end):
    url = (
        f'{RECORDING_BASE_URL}?startDate={int(start)}'
        f'&endDate={int(end)}&allowPartialMatch=true'
    )
    httpretty.register_uri(
        httpretty.GET,
        url,
        body=generate_response(start, end),
        match_querystring=True,
    )


def assert_on_sns_message(sqs_client, expected_body, expected_length=1):
    # test queue is subscribed to the sns topic so that
    # we can assert on the messages
    messages = get_messages(sqs_client, TEST_SNS_QUEUE)
    assert len(messages) == expected_length
    for i in range(expected_length):
        message = json.loads(json.loads(messages[i]['Body'])['Message'])
        assert json.loads(message['default']) == expected_body


@pytest.fixture()
def force_timeout():
    config.TIMEOUT_MINS = 0.1
    yield
    config.TIMEOUT_MINS = float(os.environ['TIMEOUT_MINS'])


@pytest.fixture()
def force_long_clip():
    config.MAX_REWIND_LENGTH = 1
    yield
    config.MAX_REWIND_LENGTH = float(os.environ['MAX_REWIND_LENGTH'])
