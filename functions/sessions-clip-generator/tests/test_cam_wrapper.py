# -*- coding: utf-8 -*-
""" Cam Wrapper Test Module """

import logging

import mock

import lib.cam_wrapper
import lib.utils

session_start_ms = '1535223360000'
session_end_ms = '1535224400000'
camera_id = '583499c4e411dc743a5d5296'
clip_id = '507f191e810c19729de860ea'

metadata_test_clip_key = f'{camera_id}/{clip_id}.mp4'
metadata_test_tb_key = f'{camera_id}/{clip_id}_{{size}}.jpg'
cw = lib.cam_wrapper.CamWrapper(
    int(session_start_ms),
    int(session_end_ms),
    camera_id,
    clip_id,
    [],
    logging.getLogger(),
)


class TestCamWrapper:
    def test_metadata(self):
        cw.status = 'CLIP_AVAILABLE'
        assert cw.metadata['clip']['key'] == metadata_test_clip_key
        assert cw.metadata['thumbnail']['key'] == metadata_test_tb_key
        # check that the thumbnail clip and bucket aren't in the metadata
        # when clip is unavailable
        cw.status = 'FATAL_ERROR'
        assert 'thumbnail' not in cw.metadata
        assert 'clip' not in cw.metadata
        assert 'bucket' not in cw.metadata

    @mock.patch('lib.utils.get_request')
    def test_get_clip_names(self, mocked_method):
        cw.get_clip_names()
        assert mocked_method.called

    @mock.patch('lib.utils.put_request')
    def test_put_clip_metadata(self, mocked_method):
        cw.put_clip_metadata()
        assert mocked_method.called

    def test_update_clip_status(self):
        test_status = 'test'
        cw.status = test_status
        assert cw.metadata['status'] == test_status
