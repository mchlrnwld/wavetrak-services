# -*- coding: utf-8 -*-
""" Utils Test Module """

import json
import os

import mock
import pytest

import lib.utils

fixtures_path = os.path.join(os.path.dirname(__file__), 'fixtures')


@pytest.fixture
def get_fixture(fixture_json):
    get_sqs_event = os.path.join(fixtures_path, fixture_json)
    with open(get_sqs_event, 'r') as f:
        return json.load(f)


domain = 'http://test.com'
endpoint = 'filetest.mp4'
file_url = 'http://test.com/filetest.mp4'
clipname = 'hbpiernscam.20180815T140019.mp4'
working_dir = '/tmp'
epoch_in_ms = 1535224400000
timestamp = '20180825T191320'
timestamp_format = '%Y%m%dT%H%M%S'
msecs = 1807
resp = {'test1': 'test2'}


class TestClipGeneratorTrigger:
    def test_get_timestamp_str(self):
        assert '20180815T140019000' == lib.utils.get_timestamp_str(clipname)

    def test_timestamp_to_epoch(self):
        assert epoch_in_ms == lib.utils.timestamp_to_epoch(
            timestamp, timestamp_format
        )

    def test_humanize_delta_time(self):
        assert '00:01.807' == lib.utils.humanize_delta_time(msecs)

    @mock.patch('lib.utils.os.remove')
    def test_remove_file(self, mocked_remove):
        lib.utils.remove_file(clipname)
        assert mocked_remove.called
