# -*- coding: utf-8 -*-
""" Ffmpeg Wrapper Test Module """

import logging

import lib.ffmpeg_wrapper

expected_crop_start = None
expected_crop_end = '13:01.000'

epoch_video = 1535884819000
session_start_ms = 1535884600000
session_end_ms = 1535885600000

fw = lib.ffmpeg_wrapper.FfmpegWrapper(
    session_start_ms, session_end_ms, logging.getLogger()
)


class TestFfmpegWrapper:
    def test_calculate_crop_time(self):
        crop_start, crop_end = fw.calculate_trim_time(epoch_video)
        assert crop_start == expected_crop_start
        assert crop_end == expected_crop_end
