import json
import shutil

from lib.utils import timestamp_to_epoch
from tests.utils.test_consts import CAM_ID, CLIP_ID, DB_TIMESTAMP_FORMAT

filename_base = 'tests/fixtures/hbpiernscam.stream'

mock_recording_list = [
    {
        "cameraId": CAM_ID,
        "alias": "a-test",
        "startDate": "2020-02-28T23:26:40.000Z",
        "endDate": "2020-02-28T23:27:00.000Z",
        "recordingUrl": f"{filename_base}.20200228T232640000.mp4",
        "resolution": "1280x720",
    },
    {
        "cameraId": CAM_ID,
        "alias": "a-test",
        "startDate": "2020-02-28T23:26:20.000Z",
        "endDate": "2020-02-28T23:26:40.000Z",
        "recordingUrl": f"{filename_base}.20200228T232620000.mp4",
        "resolution": "1280x720",
    },
    {
        "cameraId": CAM_ID,
        "alias": "a-test",
        "startDate": "2020-02-28T23:26:00.000Z",
        "endDate": "2020-02-28T23:26:20.000Z",
        "recordingUrl": f"{filename_base}.20200228T232600000.mp4",
        "resolution": "1280x720",
    },
]

for index, recording in enumerate(mock_recording_list):
    shutil.copy(f'tests/fixtures/{index+1}.mp4', recording['recordingUrl'])


def generate_response(start, end):
    return json.dumps(
        [
            item
            for item in mock_recording_list
            if timestamp_to_epoch(item['endDate'], DB_TIMESTAMP_FORMAT)
            >= start
            and timestamp_to_epoch(item['startDate'], DB_TIMESTAMP_FORMAT)
            <= end
        ]
    )


def generate_event(sqs_timestamp, start, end, retryTimestamps, length=1):
    records = [
        {
            "attributes": {"SentTimestamp": str(sqs_timestamp)},
            "awsRegion": "us-west-1",
            "body": json.dumps(
                {
                    'clipId': CLIP_ID,
                    'retryTimestamps': retryTimestamps,
                    'cameraId': CAM_ID,
                    'startTimestampInMs': int(start),
                    'endTimestampInMs': int(end),
                }
            ),
        }
    ] * length
    return {"Records": records}
