import json
from time import sleep

import httpretty  # type: ignore

from lib import config
from lib.utils import timestamp_to_epoch
from main import main
from tests.fixtures.generate_fixtures import (
    generate_event,
    mock_recording_list,
)
from tests.utils.test_consts import (
    CAM_ID,
    CLIP_ID,
    CLIP_URL,
    DB_TIMESTAMP_FORMAT,
    END,
    INTERVAL_SECONDS,
    START,
)
from tests.utils.utils import (  # noqa
    assert_on_sns_message,
    check_all_files_in_s3,
    create_s3_buckets,
    force_long_clip,
    force_timeout,
    generate_successful_expected,
    get_messages,
    get_requests,
    intercept_recording_request,
    intercept_requests,
    sns_client,
    sqs_client,
)


def test_no_recording_lt_15_mins_from_start(
    sns_client, sqs_client, intercept_requests, create_s3_buckets  # noqa
):
    start = timestamp_to_epoch(END, DB_TIMESTAMP_FORMAT) + 1000
    end = start + 2000
    intercept_recording_request(start, end)
    event = generate_event(start, start, end, [])

    expected_body = {
        'cameraId': CAM_ID,
        'clipId': CLIP_ID,
        'endTimestampInMs': end,
        'retryTimestamps': [start],
        'startTimestampInMs': start,
    }

    main(event)

    messages = get_messages(sqs_client)
    assert len(messages) == 0
    sleep(config.SQS_TIMEOUT + 1)

    messages = get_messages(sqs_client)
    assert len(messages) == 1
    assert json.loads(messages[0]['Body']) == expected_body

    expected_body['retryTimestamps'] = []
    expected_body['status'] = 'CLIP_PENDING'

    assert_on_sns_message(sqs_client, expected_body)

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 1
    assert json.loads(clip_requests[0].body) == expected_body

    main(generate_event(start, start, end, [start]))

    del expected_body['status']
    expected_body['retryTimestamps'] = [start] * 2

    messages = get_messages(sqs_client)
    assert len(messages) == 0
    sleep(config.SQS_TIMEOUT)

    messages = get_messages(sqs_client)
    assert len(messages) == 1
    assert json.loads(messages[0]['Body']) == expected_body

    expected_body['retryTimestamps'] = [start]
    expected_body['status'] = 'CLIP_PENDING'

    assert_on_sns_message(sqs_client, expected_body)

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 2
    assert json.loads(clip_requests[1].body) == expected_body


def test_no_recording_retry_timeout(
    sns_client, sqs_client, intercept_requests, create_s3_buckets  # noqa
):
    start = timestamp_to_epoch(END, DB_TIMESTAMP_FORMAT) + 1000
    end = start + 2000
    intercept_recording_request(start, end)

    max_retry_num = int(config.SQS_RETRY_LIMIT) // int(config.SQS_TIMEOUT)
    retry_timestamps = [start] * (max_retry_num + 1)
    event = generate_event(start, start, end, retry_timestamps)

    expected_body = {
        'cameraId': CAM_ID,
        'clipId': CLIP_ID,
        'endTimestampInMs': end,
        'retryTimestamps': retry_timestamps,
        'startTimestampInMs': start,
        'status': 'CLIP_NOT_AVAILABLE',
    }

    main(event)

    sleep(config.SQS_TIMEOUT)
    messages = get_messages(sqs_client)
    assert len(messages) == 0

    assert_on_sns_message(sqs_client, expected_body)

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 1
    assert json.loads(clip_requests[0].body) == expected_body


def test_no_recording_clip_unavailable(
    sns_client, sqs_client, intercept_requests, create_s3_buckets  # noqa
):
    sqs_timestamp = timestamp_to_epoch(END, DB_TIMESTAMP_FORMAT)
    start = sqs_timestamp + (config.SQS_RETRY_LIMIT + 1) * 60 * 1000
    end = start + 2000
    intercept_recording_request(start, end)
    event = generate_event(sqs_timestamp, start, end, [])

    expected_body = {
        'cameraId': CAM_ID,
        'clipId': CLIP_ID,
        'endTimestampInMs': end,
        'retryTimestamps': [],
        'startTimestampInMs': start,
        'status': 'CLIP_NOT_AVAILABLE',
    }

    main(event)

    sleep(config.SQS_TIMEOUT)
    messages = get_messages(sqs_client)
    assert len(messages) == 0

    assert_on_sns_message(sqs_client, expected_body)

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 1
    assert json.loads(clip_requests[0].body) == expected_body


def test_no_recording_clip_out_of_range(
    sns_client, sqs_client, intercept_requests, create_s3_buckets  # noqa
):
    sqs_timestamp = timestamp_to_epoch(END, DB_TIMESTAMP_FORMAT)
    start = sqs_timestamp + (config.OUT_OF_RANGE_LIMIT + 1) * 60 * 1000
    end = start + 2000
    intercept_recording_request(start, end)
    event = generate_event(sqs_timestamp, start, end, [])

    expected_body = {
        'cameraId': CAM_ID,
        'clipId': CLIP_ID,
        'endTimestampInMs': end,
        'retryTimestamps': [],
        'startTimestampInMs': start,
        'status': 'CLIP_OUT_OF_RANGE',
    }

    main(event)

    sleep(config.SQS_TIMEOUT)
    messages = get_messages(sqs_client)
    assert len(messages) == 0

    assert_on_sns_message(sqs_client, expected_body)

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 1
    assert json.loads(clip_requests[0].body) == expected_body


def test_1_recording_clip_available(
    sns_client, sqs_client, intercept_requests, create_s3_buckets  # noqa
):
    sqs_timestamp = timestamp_to_epoch(START, DB_TIMESTAMP_FORMAT)
    start = sqs_timestamp
    end = start + 2000
    intercept_recording_request(start, end)
    event = generate_event(sqs_timestamp, start, end, [])

    expected_body = generate_successful_expected(start, end)

    main(event)

    sleep(config.SQS_TIMEOUT)
    messages = get_messages(sqs_client)
    assert len(messages) == 0

    assert_on_sns_message(sqs_client, expected_body)

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 1
    assert json.loads(clip_requests[0].body) == expected_body

    check_all_files_in_s3(expected_body, 'test_1_recording_clip_available')


def test_1_recording_clip_available_multiple_events_in_list(
    sns_client, sqs_client, intercept_requests, create_s3_buckets  # noqa
):
    sqs_timestamp = timestamp_to_epoch(START, DB_TIMESTAMP_FORMAT)
    start = sqs_timestamp
    end = start + 2000
    intercept_recording_request(start, end)
    event = generate_event(sqs_timestamp, start, end, [], 2)

    expected_body = generate_successful_expected(start, end)

    main(event)

    sleep(config.SQS_TIMEOUT)
    messages = get_messages(sqs_client)
    assert len(messages) == 0

    assert_on_sns_message(sqs_client, expected_body, 2)

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 2
    assert json.loads(clip_requests[0].body) == expected_body
    assert json.loads(clip_requests[1].body) == expected_body

    check_all_files_in_s3(
        expected_body,
        'test_1_recording_clip_available_multiple_events_in_list',
    )


def test_2_recording_clip_available(
    sns_client, sqs_client, intercept_requests, create_s3_buckets  # noqa
):
    sqs_timestamp = timestamp_to_epoch(START, DB_TIMESTAMP_FORMAT)
    start = sqs_timestamp
    end = start + (INTERVAL_SECONDS + 1) * 1000
    intercept_recording_request(start, end)
    event = generate_event(sqs_timestamp, start, end, [])

    expected_body = generate_successful_expected(start, end)

    main(event)

    sleep(config.SQS_TIMEOUT)
    messages = get_messages(sqs_client)
    assert len(messages) == 0

    assert_on_sns_message(sqs_client, expected_body)

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 1
    assert json.loads(clip_requests[0].body) == expected_body

    check_all_files_in_s3(expected_body, 'test_2_recording_clip_available')


def test_2_recording_start_at_end_of_first_clip(
    sns_client, sqs_client, intercept_requests, create_s3_buckets  # noqa
):
    start = mock_recording_list[1]['endDate']
    sqs_timestamp = timestamp_to_epoch(start, DB_TIMESTAMP_FORMAT)
    start = sqs_timestamp
    end = start + (INTERVAL_SECONDS - 1) * 1000
    intercept_recording_request(start, end)
    event = generate_event(sqs_timestamp, start, end, [])

    expected_body = generate_successful_expected(start, end)

    main(event)

    sleep(config.SQS_TIMEOUT)
    messages = get_messages(sqs_client)
    assert len(messages) == 0

    assert_on_sns_message(sqs_client, expected_body)

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 1
    assert json.loads(clip_requests[0].body) == expected_body

    check_all_files_in_s3(
        expected_body, 'test_2_recording_start_at_end_of_first_clip'
    )


def test_2_recording_second_clip_unavailable(
    sns_client, sqs_client, intercept_requests, create_s3_buckets  # noqa
):
    start = mock_recording_list[0]['startDate']
    start = (
        timestamp_to_epoch(start, DB_TIMESTAMP_FORMAT)
        + (INTERVAL_SECONDS * 1000) / 2
    )
    end = start + INTERVAL_SECONDS * 1000
    intercept_recording_request(start, end)
    event = generate_event(start, start, end, [])

    expected_body = generate_successful_expected(start, end)

    main(event)

    sleep(config.SQS_TIMEOUT)
    messages = get_messages(sqs_client)
    assert len(messages) == 0

    assert_on_sns_message(sqs_client, expected_body)

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 1
    assert json.loads(clip_requests[0].body) == expected_body

    check_all_files_in_s3(
        expected_body, 'test_2_recording_second_clip_unavailable'
    )


def test_1_recording_start_before_start_of_clip(
    sns_client, sqs_client, intercept_requests, create_s3_buckets  # noqa
):
    # this is quite a specific one: Previously, when the rewind started after
    # the wave and there was more of the wave in the part with no rewind the
    # start time was greater than the end time and ffmpeg complained.
    start = mock_recording_list[-1]['startDate']
    start = (
        timestamp_to_epoch(start, DB_TIMESTAMP_FORMAT)
        - (INTERVAL_SECONDS * 1000) / 2
    )
    end = start + (INTERVAL_SECONDS - 1) * 1000
    intercept_recording_request(start, end)
    event = generate_event(start, start, end, [])

    expected_body = generate_successful_expected(start, end)

    main(event)

    sleep(config.SQS_TIMEOUT)
    messages = get_messages(sqs_client)
    assert len(messages) == 0

    assert_on_sns_message(sqs_client, expected_body)

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 1
    assert json.loads(clip_requests[0].body) == expected_body

    check_all_files_in_s3(
        expected_body, 'test_1_recording_start_before_start_of_clip'
    )


def test_3_recording_clip_available(
    sns_client, sqs_client, intercept_requests, create_s3_buckets  # noqa
):
    sqs_timestamp = timestamp_to_epoch(START, DB_TIMESTAMP_FORMAT)
    start = sqs_timestamp + 1000
    end = start + (INTERVAL_SECONDS * 2 + 1) * 1000
    intercept_recording_request(start, end)
    event = generate_event(sqs_timestamp, start, end, [])

    expected_body = generate_successful_expected(start, end)

    main(event)

    sleep(config.SQS_TIMEOUT)
    messages = get_messages(sqs_client)
    assert len(messages) == 0

    assert_on_sns_message(sqs_client, expected_body)

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 1
    assert json.loads(clip_requests[0].body) == expected_body

    check_all_files_in_s3(expected_body, 'test_3_recording_clip_available')


def test_timeout(
    sns_client,  # noqa
    sqs_client,  # noqa
    intercept_requests,  # noqa
    create_s3_buckets,  # noqa
    force_timeout,  # noqa
):
    sqs_timestamp = timestamp_to_epoch(START, DB_TIMESTAMP_FORMAT)
    start = sqs_timestamp + 1000
    end = start + (INTERVAL_SECONDS * 2 + 1) * 1000
    intercept_recording_request(start, end)
    event = generate_event(sqs_timestamp, start, end, [])

    expected_body = {
        'cameraId': CAM_ID,
        'clipId': CLIP_ID,
        'endTimestampInMs': end,
        'retryTimestamps': [],
        'startTimestampInMs': start,
        'status': 'FATAL_ERROR',
    }

    try:
        main(event)
    except Exception as e:
        print(e)

    sleep(config.SQS_TIMEOUT)
    messages = get_messages(sqs_client)
    assert len(messages) == 0

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 1
    assert json.loads(clip_requests[0].body) == expected_body


def test_timeout_long_rewind(
    sns_client,  # noqa
    sqs_client,  # noqa
    intercept_requests,  # noqa
    create_s3_buckets,  # noqa
    force_timeout,  # noqa
    force_long_clip,  # noqa
):
    sqs_timestamp = timestamp_to_epoch(START, DB_TIMESTAMP_FORMAT)
    start = sqs_timestamp + 2000
    end = start + (INTERVAL_SECONDS * 2 + 1) * 1000
    intercept_recording_request(start, end)
    event = generate_event(sqs_timestamp, start, end, [])

    expected_body = {
        'cameraId': CAM_ID,
        'clipId': CLIP_ID,
        'endTimestampInMs': end,
        'retryTimestamps': [],
        'startTimestampInMs': start,
        'status': 'CLIP_NOT_AVAILABLE',
    }

    try:
        main(event)
    except Exception as e:
        print(e)

    messages = get_messages(sqs_client)
    assert len(messages) == 0

    sleep(config.SQS_TIMEOUT + 1)

    assert_on_sns_message(sqs_client, expected_body)

    requests = httpretty.latest_requests()

    clip_requests = get_requests(CLIP_URL.replace('http://', ''), requests)
    assert len(clip_requests) == 1
    assert json.loads(clip_requests[0].body) == expected_body
