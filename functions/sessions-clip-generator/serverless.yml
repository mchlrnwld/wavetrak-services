service: sl-sessions
variablesResolutionMode: 20210326

custom:
  name: ${self:service}-${self:custom.application}-${self:custom.env}
  env: ${opt:stage}
  oldAwsProfile:
    # Needed to handle sandbox/dev discrepancies in the lower tier.
    # This is so that all new resources created by Serverless in AWS contain the `sandbox` convention.
    # However, some existing AWS resources (such as Parameter store) still use the `dev` convention.
    sandbox: dev
    staging: dev
    prod: prod
  awsRegion: us-west-1
  vpcSettings:
    securityGroupIds:
      sandbox:
        - sg-90aeaef5 # Surfline Core Internal security group.
      staging:
        - sg-90aeaef5 # Surfline Core Internal security group.
      prod:
        - sg-a5a0e5c0 # Surfline Core Internal security group.
    subnetIds:
      sandbox:
        - subnet-0909466c
        - subnet-f2d458ab
      staging:
        - subnet-0909466c
        - subnet-f2d458ab
      prod:
        - subnet-d1bb6988
        - subnet-85ab36e0
  application: clip-generator
  company: Wavetrak
  provisionedBy: Serverless
  logLevel:
    sandbox: INFO
    staging: INFO
    prod: INFO
  awsAccountNumbers:
    sandbox: "665294954271"
    staging: "665294954271"
    prod: "833713747344"
  cdnBaseDomain:
    sandbox: sandbox.surfline.com
    staging: staging.surfline.com
    prod: cdn-surfline.com
  awsAccountNumber: ${self:custom.awsAccountNumbers.${self:custom.env}}
  sqsQueue: sl-cameras-service-generate-clip-sqs-${self:custom.env}
  newRelic:
    accountId: ${ssm:/${self:custom.env}/common/NEW_RELIC_ACCOUNT_ID}

provider:
  name: aws
  ecr:
    images:
      sl-sessions-clip-generator:
        path: ./
  iamRoleStatements:
    - Effect: Allow
      Action:
        - s3:PutObject
        - s3:PutObjectAcl
        - s3:Get*
      Resource:
        - "arn:aws:s3:::sl-cameras-service-clips-cdn-${self:custom.env}"
        - "arn:aws:s3:::sl-cameras-service-clips-cdn-${self:custom.env}/*"
    - Effect: Allow
      Action:
        - sns:Publish
      Resource:
        - "arn:aws:sns:us-west-1:${self:custom.awsAccountNumber}:sl-cameras-service-generate-clip-sns_${self:custom.env}"
    - Effect: Allow
      Action:
        - sqs:SendMessage
      Resource:
        - arn:aws:sqs:us-west-1:${self:custom.awsAccountNumber}:${self:custom.sqsQueue}
  region: ${self:custom.awsRegion}

functions:
  wt-sessions-clip-generator:
    image: sl-sessions-clip-generator
    runtime: python3.8
    maximumRetryAttempts: 0
    reservedConcurrency: 300 #long process so uses too much memory if concurrency gets too high
    timeout: 900 #Long timeout as it often takes a while
    memorySize: 3008
    name: ${self:custom.name}
    events:
      - sqs:
          arn: arn:aws:sqs:us-west-1:${self:custom.awsAccountNumber}:${self:custom.sqsQueue}
          batchSize: 1
    vpc:
      securityGroupIds: ${self:custom.vpcSettings.securityGroupIds.${self:custom.env}}
      subnetIds: ${self:custom.vpcSettings.subnetIds.${self:custom.env}}
    tags:
      Company: ${self:custom.company}
      Application: ${self:custom.application}
      Environment: ${self:custom.env}
      Service: ${self:service}
      Source: Surfline/wavetrak-services/functions/sessions-clip-generator
      Provisioned-By: ${self:custom.provisionedBy}
    environment:
      AWS_SNS_TOPIC: arn:aws:sns:us-west-1:${self:custom.awsAccountNumber}:sl-cameras-service-generate-clip-sns_${self:custom.env}
      AWS_SQS_QUEUE_URL: https://sqs.us-west-1.amazonaws.com/${self:custom.awsAccountNumber}/${self:custom.sqsQueue}
      CAMERAS_API: http://cameras-service.${self:custom.env}.surfline.com
      CLIP_URL: https://camclips.${self:custom.cdnBaseDomain.${self:custom.env}}
      ENV: ${self:custom.env}
      LOG_LEVEL: ${self:custom.logLevel.${self:custom.env}}
      NEW_RELIC_ACCOUNT_ID: ${self:custom.newRelic.accountId}
      NEW_RELIC_APP_NAME: ${self:custom.name}
      NEW_RELIC_DISTRIBUTED_TRACING_ENABLED: "true"
      NEW_RELIC_NO_CONFIG_FILE: "true"
      NEW_RELIC_SERVERLESS_MODE_ENABLED: "true"
      NEW_RELIC_TRUSTED_ACCOUNT_KEY: ${self:custom.newRelic.accountId}
      OUT_OF_RANGE_LIMIT: 518400
      S3_BUCKET_DESTINATION: sl-cameras-service-clips-cdn-${self:custom.env}
      SQS_RETRY_LIMIT: 900
      SQS_TIMEOUT: 60
      THUMBNAIL_SIZES: "300 640 1500 3000"
      VIDEO_MAX_LEN: 45
      VIDEO_MIN_LEN: 1
      TIMEOUT_MINS: 14
      MAX_REWIND_LENGTH: 600
