# -*- coding: utf-8 -*-
""" Utils methods module """

import calendar
import json
import logging
import os
import shutil
import subprocess
import time
from typing import Dict, List, Tuple, Union
from urllib.parse import urljoin

import newrelic.agent  # type: ignore
import requests

from lib import config

logging.basicConfig(level=config.LOG_LEVEL)
logger = logging.getLogger()


def get_request(domain: str, endpoint: str) -> Dict:
    """
    Given an URL, send a HTTP GET request, returning
    a JSON object.
    """
    url = urljoin(domain, endpoint)
    r = requests.get(url)
    r.raise_for_status()
    logger.debug(f'GET request to {endpoint} was successful.')

    return r.json()


def put_request(
    domain: str, endpoint: str, data: Union[Dict, List, str]
) -> Dict:
    """
    Given a domain and endpoint strings, and a dict of data,
    send a HTTP POST request, returning a JSON response.
    """
    url = urljoin(domain, endpoint)
    r = requests.put(url, json=data)
    logger.info(f'PUT {endpoint} with {data}: {r.status_code}')
    r.raise_for_status()

    return r.json()


def get_timestamp_str(clipname: str) -> str:
    """
        Given a video name, return the timestamp str.

        Files in the new format have the following format:
        Timestamp yyyymmddThhmmssmmm
        e.g. hbpiernscam.stream.20181031T025200051.mp4

        Files in the old format have the following format:
        e.g. hbpiernscam.20180815T140019.mp4 and need to
        have milliseconds added.
    """
    timestamp = clipname.split('.')[-2]

    if len(timestamp) < 16:
        # The file is in the old format.
        timestamp = f'{timestamp}000'

    logger.debug(f"Timestamp from filename is {timestamp}.")

    return timestamp


def timestamp_to_epoch(
    timestamp: str, timestamp_format: str = config.TIMESTAMP_FORMAT
) -> float:
    """
        Given a timestamp and (optionally) a timestamp format,
        return the unix time in milliseconds.
    """
    date = time.strptime(timestamp, timestamp_format)
    unix_millisec = calendar.timegm(date) * 1000.0

    logger.debug(
        f'Timestamp {timestamp} converted to epoch in ms: {unix_millisec}.'
    )

    return unix_millisec


def humanize_delta_time(timestamp: Union[float, int]) -> str:
    """
        Given a time delta in milliseconds, return a human (ffmpeg)
        readable string.
    """
    mins, secs = divmod(timestamp / 1000, 60)
    secs, msecs = divmod(secs * 1000, 1000)
    min_str = str(int(mins)).zfill(2)
    sec_str = str(int(secs)).zfill(2)
    msec_str = str(int(msecs)).zfill(3)
    return f'{min_str}:{sec_str}.{msec_str}'


def human_time_to_float(timestring: str) -> float:
    mins, secs = timestring.split(':')
    return 60 * float(mins) + float(secs)


def remove_file(filepath: str):
    """
        Delete a given file from disk.
    """
    try:
        os.remove(filepath)

    except OSError as e:
        logger.warning(f'Could not delete {filepath}: {e}')


def run_subprocess(cmd: List[str]) -> str:
    """
    Given a list of subprocess commands and flags,
    a success message and an error message, run a
    subprocess thread for that cmd.
    """
    joined_command = ' '.join(cmd)

    logger.info(f'Running subprocess: {joined_command}')
    with open(os.devnull) as devnull:
        timeout = config.TIMEOUT_MINS * 60 - time.process_time()
        try:
            output = subprocess.check_output(
                cmd, stdin=devnull, stderr=subprocess.STDOUT, timeout=timeout,
            ).decode("utf-8")
            logger.info(output)
        except subprocess.CalledProcessError as e:
            logger.error(e.output.decode("utf-8"))
            available = shutil.disk_usage('/tmp/').free
            # if less than ~20mb (1mb not precicely 1 million bytes)
            # we might struggle to save clips etc
            if available < 20000000:
                newrelic.agent.record_custom_event(
                    'Sessions low storage', {'available': available}
                )
            raise e
        except subprocess.TimeoutExpired as e:
            logger.error(
                f'Command {joined_command} took more than '
                f'the available remaining time: {timeout}'
            )
            logger.error(e.output.decode("utf-8"))
            raise e
    logger.info(f'Command {joined_command}: success.')
    return output


def extract_event_data(record: Dict) -> Tuple:
    """
        Given an event that triggers the lambda function, extract the
        necessary data from its contract.
    """
    try:
        body = record["body"].replace("'", "\"")
        send_ts = record["attributes"]["SentTimestamp"]
        return json.loads(body), int(float(send_ts))

    except (KeyError, IndexError) as e:
        raise Exception(f'Ill-formatted event message: {e}')


def get_alias_from_clip(clip: str) -> str:
    """
        Given a clip string it returns the alias for the cam
    """
    file = clip.split('/')[-1]
    return file.split('.')[0]


def get_delta_time(start_event: int, end_request: int) -> int:
    """
        Given two event timestamps (in millseconds, e.g.1538601628000),
        return their absolute time difference, in seconds.
    """
    return int(abs(start_event - end_request) / 1000)
