# -*- coding: utf-8 -*-
""" Ffmpeg API wrapper methods module """

import logging
import os
import subprocess
import tempfile
from typing import List

import newrelic.agent  # type: ignore

from lib import config, utils
from lib.errors import LongRewindError

# Define location of included ffmpeg binaries
FFMPEG = os.path.join(config.LAMBDA_TASK_ROOT, 'ffmpeg')


class FfmpegWrapper(object):
    def __init__(
        self, epoch_start: int, epoch_end: int, logger: logging.Logger
    ):
        self.epoch_start = epoch_start
        self.epoch_end = epoch_end
        # Create a time around 25% for best match for thumbnail.
        self.trim_thumbnail = utils.humanize_delta_time(
            (self.epoch_end - self.epoch_start) / 4
        )
        self.logger = logger
        self.thumbnails: List = []
        self.output_clip_path = ''
        self.temp_dir_obj = tempfile.TemporaryDirectory()

    def calculate_trim_time(self, epoch_video: int):
        """
            Given a video str epoch time, and two epoch time
            intervals, returns a human (ffmpeg) readable start
            and end trim time.
        """
        # Find where to trim within the video.
        # if the rewind starts after the desired clip
        # we need to just start from the start
        clip_start_trim = max(0, self.epoch_start - epoch_video)
        clip_end_trim = self.epoch_end - epoch_video

        # Create the strings for trimming time for ffmpeg.
        if clip_start_trim == 0:
            trim_start = None
        else:
            trim_start = utils.humanize_delta_time(clip_start_trim)

        if clip_end_trim == 0:
            trim_end = None
        else:
            trim_end = utils.humanize_delta_time(clip_end_trim)

        self.logger.debug("Clip's trim time is {trim_start} to {trim_end}")
        return trim_start, trim_end

    def download_and_trim_video(self, video: str, start: str, end: str):
        """
            Download and trim an MP4 video given an output path, a start time,
            and an optional end time.
        """
        # invalid input as mins can't be over 59
        if start and int(start.split(':')[0]) > 59:
            raise LongRewindError(start, video)
        output = os.path.join(
            self.temp_dir_obj.name, f'trimmed_{video.split("/")[-1]}'
        )
        video = video.replace('sandbox.surfline', 'cdn-surfline')
        cmd = [FFMPEG, '-i', video]

        if start:
            cmd += ['-ss', str(start)]

        if end:
            cmd += ['-to', str(end)]

        cmd += [
            '-vf',
            'format=yuv420p',
            '-preset',
            'fast',
            '-movflags',
            '+faststart',
            '-abort_on',
            'empty_output',
            '-y',
            output,
        ]

        try:
            utils.run_subprocess(cmd)
            return output
        except subprocess.TimeoutExpired as e:
            newrelic.agent.record_custom_event(
                'Sessions Clip Download Timeout',
                {'clip_filename': video, 'start': start, 'end': end},
            )

            if (
                start
                and utils.human_time_to_float(start) > config.MAX_REWIND_LENGTH
            ):
                raise LongRewindError(start, video)
            raise e
        except subprocess.CalledProcessError as e:
            if start and utils.human_time_to_float(start) > (
                config.MAX_REWIND_LENGTH - 1
            ):
                self.logger.info(
                    f'requested start time {start} after/at rewind end time'
                )
            elif 'Conversion failed!' in e.output.decode("utf-8"):
                alias = utils.get_alias_from_clip(video)
                newrelic.agent.record_custom_event(
                    'Sessions Clip Corrupt',
                    {
                        'clip_filename': video,
                        'start': start,
                        'end': end,
                        'alias': alias,
                    },
                )
            else:
                raise e

            return None

    def concatenate_videos(self, videos: List):
        """
            Given a list of MP4 video files, concatenate
            them to an output file.
        """
        concat_list_file = f'{self.temp_dir_obj.name}/concat_list.txt'
        with open(concat_list_file, 'w') as f:
            for video in videos:
                f.write(f'file {video}\n')

        full_clip_file = f'{self.temp_dir_obj.name}/full_clip.mp4'

        cmd = [
            FFMPEG,
            '-f',
            'concat',
            '-safe',
            '0',
            '-i',
            concat_list_file,
            '-movflags',
            '+faststart',
            '-c',
            'copy',
            full_clip_file,
        ]

        utils.run_subprocess(cmd)

        return full_clip_file

    def create_session_clip(self, clips: List):
        """
            Given instances of ffmpeg wrapper and aws wrapper,
            download, trim and concanate videos to create
            the session clip, returning True if successful.

            If more than 2 videos need to be downloaded to
            create the clip, it will download and decode/trim each
            video at the time (this is due AWS Lambda function's
            restrictions, i.e. 500 MB disk limit).
        """
        videos_path = []

        clips = [
            {
                "clip": clip,
                "epoch_video": utils.timestamp_to_epoch(
                    utils.get_timestamp_str(clip)
                ),
            }
            for clip in clips
        ]

        clips.sort(key=lambda x: (x['epoch_video']))
        # Video list starts with the last video (end), so we reverse it.
        for i, clip in enumerate(clips):
            trim_start, trim_end = self.calculate_trim_time(
                clip["epoch_video"]
            )

            if not (trim_start or trim_end):
                continue

            # First video.
            if i == 0 and len(clips) > 1:
                trim_end = None

            # Last video.
            elif i > 0 and i == len(clips) - 1:
                trim_start = None

            # Any video in the middle.
            elif i > 0:
                trim_start = None
                trim_end = None

            output_path = self.download_and_trim_video(
                clip["clip"], trim_start, trim_end
            )

            if output_path:
                videos_path.append(output_path)

        self.logger.info(videos_path)

        if len(videos_path) == 0:
            return None

        clip = self.concatenate_videos(videos_path)

        for video in videos_path:
            utils.remove_file(video)

        return clip

    def create_thumbnails(self, clip_id: str, clip: str):
        """
            Given a clip filename and path creates a thumbnail
            image in several resolutions, adding their paths
            to a list.
        """
        img_output_path = os.path.join(self.temp_dir_obj.name, '_original.jpg')
        self.logger.info(f'Creating thumbnail for {img_output_path}')

        cmd = [
            FFMPEG,
            '-loglevel',
            'error',
            '-nostats',
            '-ss',
            self.trim_thumbnail,
            '-i',
            clip,
            '-vframes',
            '1',
            '-abort_on',
            'empty_output',
            '-y',
            img_output_path,
        ]

        try:
            utils.run_subprocess(cmd)
            self.thumbnails.append(img_output_path)
            return self._thumbnail_resize(clip_id)
        except subprocess.CalledProcessError:
            # it's possible that the clip wasn't as long as we expected
            if self.trim_thumbnail != '0:0':
                self.trim_thumbnail = '0:0'
                return self.create_thumbnails(clip_id, clip)

    def _thumbnail_resize(self, clip_id: str):
        """
            Resize an original thumbnail JPEG file to a list of
            resizes.
        """
        img_output_path = self.thumbnails[0]

        for size in config.THUMBNAIL_SIZES.split():

            img_output_resized_path = os.path.join(
                self.temp_dir_obj.name, f'_{size}.jpg',
            )
            self.thumbnails.append(img_output_resized_path)

            cmd = [
                FFMPEG,
                '-i',
                img_output_path,
                '-vf',
                f'scale={size}:-1',
                '-abort_on',
                'empty_output',
                '-y',
                img_output_resized_path,
            ]

            utils.run_subprocess(cmd)

        return self.thumbnails
