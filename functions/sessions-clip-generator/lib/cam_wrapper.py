# -*- coding: utf-8 -*-
""" Cam API wrapper methods module """

import logging
from typing import List
from urllib.parse import urljoin

from lib import config, utils

sizes = list(map(int, config.THUMBNAIL_SIZES.split()))


class CamWrapper(object):
    def __init__(
        self,
        session_start_ms: int,
        session_end_ms: int,
        camera_id: str,
        clip_id: str,
        retry_timestamps: List[str],
        logger: logging.Logger,
    ):
        self.logger = logger
        self.clip_id = clip_id
        self.camera_id = camera_id
        self.session_start_ms = session_start_ms
        self.session_end_ms = session_end_ms
        self.retry_timestamps = retry_timestamps

        self.clips: List = []

        self.status = 'CLIP_PENDING'
        self.file_prefix = f'{self.camera_id}/{self.clip_id}'
        self.clip_key = f'{self.file_prefix}.mp4'
        self.thumbnail_key = f'{self.file_prefix}_{"{size}"}.jpg'

    def get_clip_names(self):
        """
            Given a camera's id, a unix start timestamp in
            milliseconds and a unix end timestamp in milliseconds,
            return a list with clip names within that period.
        """

        endpoint = (
            f'/cameras/recording/id/{self.camera_id}?'
            f'startDate={self.session_start_ms}'
            f'&endDate={self.session_end_ms}&allowPartialMatch=true'
        )

        self.logger.info(f'Getting available clips from {endpoint}')

        response = utils.get_request(config.CAMERAS_API, endpoint)

        if response and len(response) > 0:
            for clip in response:
                try:
                    self.clips.append(clip['recordingUrl'])
                except KeyError:
                    self.logger.error(f'{clip} missing recording information')

            self.logger.info(f'Retrieved clip(s): {self.clips}')
            return True

        else:
            self.logger.info(f'No clips were found for {endpoint}')
            return False

    @property
    def metadata(self):
        metadata = {
            "clipId": self.clip_id,
            "cameraId": self.camera_id,
            "startTimestampInMs": self.session_start_ms,
            "endTimestampInMs": self.session_end_ms,
            "retryTimestamps": self.retry_timestamps,
            "status": self.status,
        }

        if self.status == 'CLIP_AVAILABLE':
            clip_url = urljoin(config.CLIP_URL, self.clip_key)
            thumbnail_url = urljoin(config.CLIP_URL, self.thumbnail_key)
            metadata = {
                **metadata,
                "bucket": config.s3_bucket_destination,
                "clip": {"key": self.clip_key, "url": clip_url},
                "thumbnail": {
                    "key": self.thumbnail_key,
                    "url": thumbnail_url,
                    "sizes": sizes,
                },
            }
        return metadata

    def put_clip_metadata(self):
        """
            Given the clip metadata, generate a HTTP PUT request to the
            Cameras Service.
        """
        endpoint = f'/clips/{self.clip_id}'
        utils.put_request(config.CAMERAS_API, endpoint, self.metadata)
