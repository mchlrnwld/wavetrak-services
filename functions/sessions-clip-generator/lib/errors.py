import newrelic.agent  # type: ignore


class LongRewindError(Exception):
    def __init__(self, start, video):
        super().__init__('Sessions Long Rewind')
        newrelic.agent.record_custom_event(
            'Sessions Long Rewind',
            {'clip': video, 'minutes_requested': start},
        )
