# -*- coding: utf-8 -*-
""" Clip generator entry module """

import logging
from typing import Dict

import newrelic.agent  # type: ignore

from lib import config, ffmpeg_wrapper, utils
from lib.aws_wrapper import AwsWrapper
from lib.cam_wrapper import CamWrapper
from lib.errors import LongRewindError

MAX_RETRY_NUM = int(config.SQS_RETRY_LIMIT) // int(config.SQS_TIMEOUT)


class ClipWrapper:
    def __init__(self, record: Dict, logger: logging.Logger) -> None:
        self.event_body, self.sent_ts = utils.extract_event_data(record)
        self.logger = logger

        self.start_ts = int(self.event_body['startTimestampInMs'])
        self.end_ts = min(
            int(self.event_body['endTimestampInMs']),
            self.start_ts + config.VIDEO_MAX_LEN * 1000,
        )
        self.duration = utils.get_delta_time(self.start_ts, self.end_ts)
        self.camera_id = self.event_body['cameraId']
        self.clip_id = self.event_body['clipId']
        self.retry_ts = self.event_body.get('retryTimestamps', [])

        self.cw = CamWrapper(
            self.start_ts,
            self.end_ts,
            self.camera_id,
            self.clip_id,
            self.retry_ts,
            self.logger,
        )

        self.aw = AwsWrapper(self.logger)

    def is_clip_shorter_than_limit(self):
        """
            Check whether the requested clip has a length shorter
            than the limit, and return True if this is the case.
        """
        return self.duration < int(config.VIDEO_MIN_LEN)

    def upload_assets(self, output_clip_path, thumbnail_list):
        """
            Call the AWS wrapper upload to S3 method.
        """
        self.logger.info('Uploading clip and thumbnail to S3...')

        self.aw.upload_asset(output_clip_path, self.cw.clip_key)

        for thumbnail in thumbnail_list:
            self.aw.upload_asset(
                thumbnail, f'{self.cw.file_prefix}{thumbnail.split("/")[-1]}'
            )

            utils.remove_file(thumbnail)

    def update_clip_status(self, message, notify=True):
        """
            Update the camera wrapper clip availability.
        """
        self.logger.info(f'Status for ClipId {self.cw.clip_id}: {message}')
        self.cw.status = message
        self.cw.put_clip_metadata()

        if notify and self.aw:
            self.aw.send_sns_msg(self.cw.metadata)

    def generate_clip(self):
        """
            Generate a clip
        """
        try:
            if self.is_clip_shorter_than_limit():
                self.logger.info('Clip length is too short. Exiting.')
                newrelic.agent.record_custom_event(
                    'Sessions Clip Too Short',
                    {'duration': self.duration, 'clip_id': self.clip_id},
                )
                self.update_clip_status('CLIP_DOES_NOT_EXIST')
                return

            if self.cw.get_clip_names():
                fw = ffmpeg_wrapper.FfmpegWrapper(
                    self.start_ts, self.end_ts, self.logger
                )
                try:
                    clip = fw.create_session_clip(self.cw.clips)
                except LongRewindError:
                    status = self.update_clip_status('CLIP_NOT_AVAILABLE')
                    return

                if clip:
                    thumbnails = fw.create_thumbnails(self.cw.clip_id, clip)

                    self.upload_assets(clip, thumbnails)
                    self.update_clip_status('CLIP_AVAILABLE')
                    return

            # If no clips found:
            # Verify whether this message is a retry and check that it
            # hasn't exceeded the max retries, if not: calculate the
            # time difference between the timestamp of the SQS event
            # and the timestamp of the beginning of the clip to work out if
            # we've waited long enough for the clip to become available
            # or whether it will already be in glacier storage
            status = 'CLIP_PENDING'

            if self.retry_ts:
                if len(self.retry_ts) > MAX_RETRY_NUM:
                    self.logger.info('Max number of retries were reached')
                    status = 'CLIP_NOT_AVAILABLE'
                else:
                    self.event_body['retryTimestamps'] = [
                        *self.event_body['retryTimestamps'],
                        self.sent_ts,
                    ]

            else:
                self.event_body['retryTimestamps'] = [self.sent_ts]
                delta_time = utils.get_delta_time(self.sent_ts, self.start_ts)

                # If time difference is smaller than 3 days and
                # larger than 15 minutes (in secs).
                if delta_time > int(
                    config.SQS_RETRY_LIMIT
                ) and delta_time < int(config.OUT_OF_RANGE_LIMIT):
                    status = 'CLIP_NOT_AVAILABLE'

                # If time difference between the current time and the
                # requested time range is larger than 3 days (in secs).
                elif delta_time > int(config.OUT_OF_RANGE_LIMIT):
                    status = 'CLIP_OUT_OF_RANGE'

            # If time difference is smaller than 15 minutes
            # (which it will be if it's a retry
            # since that logic is run on the first attempt).

            self.update_clip_status(status)

            if status == 'CLIP_PENDING':
                self.aw.send_sqs_msg(self.event_body)

            if status in ['CLIP_OUT_OF_RANGE', 'CLIP_NOT_AVAILABLE']:
                newrelic.agent.record_custom_event(
                    'Sessions Clip Unavailable',
                    {
                        'camId': self.camera_id,
                        'start': self.start_ts,
                        'end': self.end_ts,
                        'status': status,
                    },
                )

        except Exception as e:
            # Hard-failure, which will make lambda function retry.
            # no sns event here since we're about to retry
            self.update_clip_status('FATAL_ERROR', notify=False)
            raise e
