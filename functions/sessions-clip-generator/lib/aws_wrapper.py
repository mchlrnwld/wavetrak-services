# -*- coding: utf-8 -*-
""" AWS API wrapper methods module """

import json
import logging
from typing import Dict

import boto3  # type: ignore

from lib import config

# boto3 is very noisy at the log levels we'd like to support
# for the rest of our application
logging.getLogger('boto3').setLevel(logging.ERROR)


class AwsWrapper(object):
    def __init__(self, logger: logging.Logger):
        self.logger = logger
        self.sns_topic = config.AWS_SNS_TOPIC
        self.sqs_queue_url = config.AWS_SQS_QUEUE_URL

    def _create_aws_client(self, client_name: str):
        """
            Return a client object for a given AWS resource.
        """
        return boto3.client(client_name, region_name='us-west-1')

    def upload_asset(self, filename: str, destination: str):
        """
            Given an asset name and a folder structure,
            uploads it to a S3 bucket.
        """

        s3 = self._create_aws_client('s3')

        content_type = (
            'video/mp4'
            if filename[-3:] == 'mp4'
            else 'image/jpeg'
            if filename[-3:] == 'jpg'
            else 'binary/octet-stream'
        )
        extra_args = {'ContentType': content_type}
        s3.upload_file(
            filename,
            config.s3_bucket_destination,
            destination,
            ExtraArgs=extra_args,
        )
        self.logger.info(
            f'{filename} saved at {destination} with {extra_args}.'
        )

    def send_sns_msg(self, clip_metadata: Dict):
        """
            Given a JSON SNS message, send it upstream to the SNS topic at AWS.
        """
        sns = self._create_aws_client('sns')

        sns.publish(
            TopicArn=self.sns_topic,
            Message=json.dumps({'default': json.dumps(clip_metadata)}),
            MessageStructure='json',
        )

        self.logger.info(f'SNS notification sent to {self.sns_topic}')

    def send_sqs_msg(self, body: Dict):
        """
            Given a JSON SQS body message
            compose an SQS message, sending it back to the queue.
        """
        sqs = self._create_aws_client('sqs')

        response = sqs.send_message(
            QueueUrl=self.sqs_queue_url,
            MessageBody=json.dumps(body),
            DelaySeconds=int(config.SQS_TIMEOUT),
        )

        self.logger.info(
            f'SQS message ID {response.get("MessageId")} '
            'sent back to the queue.'
        )
