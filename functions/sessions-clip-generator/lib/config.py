# -*- coding: utf-8 -*-
""" Config value constants """

from os import environ

# Logging
LOG_LEVEL = environ['LOG_LEVEL']

# Local config
TIMESTAMP_FORMAT = '%Y%m%dT%H%M%S%f'
SQS_TIMEOUT = int(environ['SQS_TIMEOUT'])
SQS_RETRY_LIMIT = int(environ['SQS_RETRY_LIMIT'])
OUT_OF_RANGE_LIMIT = int(environ['OUT_OF_RANGE_LIMIT'])

# Cam API
CAMERAS_API = environ['CAMERAS_API']
CLIP_URL = environ['CLIP_URL']
THUMBNAIL_SIZES = environ['THUMBNAIL_SIZES']
VIDEO_MAX_LEN = int(environ['VIDEO_MAX_LEN'])
VIDEO_MIN_LEN = int(environ['VIDEO_MIN_LEN'])
TIMEOUT_MINS = float(environ['TIMEOUT_MINS'])
MAX_REWIND_LENGTH = float(environ['MAX_REWIND_LENGTH'])

# AWS
s3_bucket_destination = environ['S3_BUCKET_DESTINATION']
AWS_SNS_TOPIC = environ['AWS_SNS_TOPIC']
AWS_SQS_QUEUE_URL = environ['AWS_SQS_QUEUE_URL']

# LAMBDA_TASK_ROOT is part of the lambda execution environment
# https://docs.aws.amazon.com/lambda/latest/dg/current-supported-versions.html
LAMBDA_TASK_ROOT = environ['LAMBDA_TASK_ROOT']
