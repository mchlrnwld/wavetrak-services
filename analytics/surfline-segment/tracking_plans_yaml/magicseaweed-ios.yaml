tracking_plan_id: rs_1RIdheSPCbIMIT233hYm6qcsg5g
tracking_plan_name: msw-ios

global:
  context:
    type: object
  traits:
    type: object
  properties:
    type: object

events:
  - name: Application Installed
    description:
      This event fires when a user first opens your mobile application.
      Note, if the user never opens your app after installing, we will not be able
      to collect this event. This event does not wait for attribution or campaign
      information to be received, and is collected automatically by our SDKs. Advertising
      providers like Facebook and Google require discrete install events to correctly
      attribute installs to ads served through their platform. (Segment)
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      build:
        description: The build number of the installed app.
        type: number
      version:
        description: The version installed.
        type: string
    required: []

  - name: Application Opened
    description:
      This event fires when a user launches or foregrounds your mobile
      application after the first open. It will fire after the Application Installed
      event and again after the app is re-opened after being closed. This event does
      not wait for attribution information to be received but may include information
      about referring applications or deep link URLs if available to the application
      upon open. (Segment)
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      build:
        description: The build number of the installed app.
        type: integer
      from_background:
        description: >
          If application transitioned from Background
          to Inactive state prior to foregrounding
          (as opposed to from Not Running state).
          Collected on iOS only.
        type: boolean
      referring_application:
        description: >
          The value of UIApplicationLaunchOptionsSourceApplicationKey
          from launchOptions. Automatically collected on iOS only.
        type: string
      url:
        description: >
          The value of UIApplicationLaunchOptionsURLKey
          from launchOptions.Collected on iOS only.
        type: string
      version:
        description: The version installed.
        type: string
    required: []

  - name: Application Updated
    description:
      This event fires when a user updates the application. Our SDK will
      automatically collect this event in lieu of an Application Opened event when
      we determine that the Open is first since an update.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      build:
        description: The new build.
        type: number
      previous_build:
        description: The previously recorded build.
        type: number
      previous_version:
        description: The previously recorded version.
        type: string
      version:
        description: The new version.
        type: string
    required: []

  - name: Clicked Button
    description: Event description missing. Consult event owner.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      articleId:
        description: The article ID.
        type: integer
      articleName:
        description: The name of the article.
        type: string
      buttonName:
        description: The button name.
        type: string
      category:
        description: The event category (used to group related events).
        type: string
      location:
        description: Where the event fired on our application.
        type: string
      platform:
        description: The platform the event is attributed to.
        type: string
      screenName:
        description: The screen name.
        type: string
      spotId:
        description: The spot ID.
        type: integer
      spotName:
        description: The spot name.
        type: string
      text:
        description: The button text.
        type: string
      type:
        description: The button type.
        type: string
      version:
        description: The application version.
        type: string
    required: []

  - name: Clicked CTA
    description: When a user clicks a CTA (call-to-action).
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      category:
        description: The event category (used to group related events).
        type: string
      CTAText:
        description: The text on the CTA.
        type: string
      funnel:
        description: The name or type of funnel the CTA belongs to.
        type: string
      hasCam:
        description: If the screen has a cam or not.
        type: boolean
      location:
        description: Where the event fired on our application.
        type: string
      orientation:
        description: Screen orientation.
        type: string
      platform:
        description: The platform the CTA was clicked on (iOS in this case).
        type: string
        enum:
          - ios
      screenName:
        description: The name of the screen the CTA was clicked on.
        type: string
      spotId:
        description: The spot ID the CTA was clicked on.
        type: integer
      spotName:
        description: The name of the spot the CTA was clicked on.
        type: string
      tags:
        description: The tags associated with the CTA.
        type: string
      text:
        description: The CTA text.
        type: string
    required: []

  - name: Clicked Play Camera
    description: Event description missing. Consult event owner.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      camId:
        description: The camera ID.
        type: integer
      camName:
        description: The camera name.
        type: string
      camView:
        description: The camera view.
        type: string
      category:
        description: The event category (used to group related events).
        type: string
      location:
        description: Where the event fired on our application.
        type: string
      platform:
        description: The platform the event is attributed to.
        type: string
      screenName:
        description: The screen name.
        type: string
      spotId:
        description: The spot ID.
        type: integer
      spotName:
        description: The spot name.
        type: string
      version:
        description: The application version.
        type: string
    required: []

  - name: Clicked Popup
    description: Client side event fired when a user clicks on any pop-up in
      the app.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      buttonText:
        description: The text on the button.
        type: string
      email:
        description: The email address of the user.
        type: string
      popupName:
        description: The name of the popup.
        type: string
      screenName:
        description: The name of the screen the popup was clicked on.
        type: string
      text:
        description: The text on the popup.
        type: string
      category:
        description: The event category (used to group related events).
        type: string
    required: []

  - name: Completed Onboarding
    description: Track when users Complete Onboarding.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      screenName:
        description: The screen name.
        type: string
    required:
      - screenName

  - name: Expired Data
    description: Event description missing. Consult event owner.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      build:
        description: The application build.
        type: string
      connectionType:
        description: The connection type.
        type: string
      forecastDate:
        description: The forecast date.
        type: string
        format: date-time
      hasInternet:
        description: If the user has internet or not.
        enum:
          - 'no'
          - 'yes'
        type: string
      hasMobileData:
        description: Whether the device has mobile data.
        type: string
        enum:
          - 'no'
          - 'yes'
      platform:
        description: The platform the event is attributed to.
        type: string
      spotId:
        description: The spot ID.
        type: integer
      spotName:
        description: The spot name.
        type: string
      userId:
        description: The user ID.
        type: integer
      version:
        description: The application version.
        type: string
    required: []

  - name: Failed Loading
    description: Event description missing. Consult event owner.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      build:
        description: The application build.
        type: string
      chartId:
        description: The chart ID.
        type: integer
      chartName:
        description: The chart name.
        type: string
      connectionType:
        description: The connection type.
        type: string
      forecastDate:
        description: The forecast date.
        type: string
        format: date-time
      hasInternet:
        description: If the user has internet or not.
        type: string
        enum:
          - 'no'
          - 'yes'
      hasMobileData:
        description: Whether the device has mobile data.
        type: string
        enum:
          - 'no'
          - 'yes'
      platform:
        description: The platform the event is attributed to.
        type: string
      spotId:
        description: The spot ID.
        type: integer
      spotName:
        description: The spot name.
        type: string
      type:
        description: The type of error.
        type: string
      userId:
        description: The user ID.
        type: integer
      version:
        description: The application version.
        type: string
    required: []

  - name: Order Completed
    description: Event description missing. Consult event owner.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties: {}
    required: []

  - name: Refreshed Data
    description: Event description missing. Consult event owner.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      build:
        description: The application build.
        type: string
      connectionType:
        description: The connection type.
        type: string
      forecastDate:
        description: The forecast date.
        type: string
        format: date-time
      hasInternet:
        description: Does the device have internet?
        type: boolean
      hasMobileData:
        description: Does the device have data?
        type: boolean
      platform:
        description: The platform the event is attributed to.
        type: string
      spotId:
        description: The spot ID.
        type: integer
      spotName:
        description: The spot name.
        type: string
      type:
        description: The type of data.
        type: string
      userId:
        description: The user ID.
        type: integer
      version:
        description: The application version.
        type: string
    required: []

  - name: Screen Viewed
    description: Event description missing. Consult event owner.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      areaId:
        description: The area ID.
        type: integer
      camId:
        description: The cam ID.
        type: integer
      camName:
        description: The camera name.
        type: string
      category:
        description: The event category (used to group related events).
        type: string
      channelId:
        description: The channel ID.
        type: integer
      channelName:
        description: The channel name.
        type: string
      chartsId:
        description: The charts ID.
        type: integer
      chartsName:
        description: The charts name.
        type: string
      directoryId:
        description: The directory ID.
        type: integer
      directoryName:
        description: The directory name.
        type: string
      hasCam:
        description: If the screen has a camera or not.
        type: boolean
      lat:
        description: The latitude.
        type: string
      loginStatus:
        description: The user's login status.
        type: string
      lon:
        description: The longitude.
        type: string
      photoId:
        description: The photo ID.
        type: integer
      proFunnel:
        description: Whether the screen has a Pro Funnel or not.
        type: boolean
      regionId:
        description: The region ID.
        type: integer
      regionName:
        description: The region name.
        type: string
      spotId:
        description: The spot ID.
        type: integer
      spotName:
        description: The spot name.
        type: string
      stationId:
        description: The station ID.
        type: integer
      stationName:
        description: The station name.
        type: string
      subregionName:
        description: The region name.
        type: string
      tideReport:
        description: The tide report.
        type: string
      userType:
        description: The user type.
        type: string
    required: []

  - name: Served Popup
    description: Client side event fired when a user is served a popup on a screen.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      screenName:
        description: The screen name.
        type: string
      popupName:
        description: The popup name.
        type: string
      category:
        description: The event category (used to group related events).
        type: string
      wasNotSeen:
        description: Was the popup not seen?
        type: boolean
    required:
      - screenName
      - popupName
      - category

  - name: Started Onboarding
    description: When a user starts onboarding.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      screenName:
        description: The screen name.
        type: string
    required:
      - screenName

  - name: Survey Response
    description: Event description missing. Consult event owner.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties: {}
    required: []

  - name: Swiped Forecast
    description: Event description missing. Consult event owner.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      regionId:
        description: The region ID of the forecast.
        type: string
      regionName:
        description: The region name of the forecast.
        type: string
      spotId:
        description: The spot ID of the forecast.
        type: string
      spotName:
        description: The spot name of the forecast.
        type: string
      surfAreaId:
        description: The surf area ID of the forecast.
        type: string
      surfAreaName:
        description: The surf area name of the forecast.
        type: string
      surveyName:
        description: The survey name on the forecast.
        type: string
    required:
      - surveyName

  - name: Updated User Setting
    description: This event fires when a user changes a personalisation setting
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      location:
        description: Where the event fired on our application.
        type: string
      orientation:
        description: The orientation of the device.
        type: string
      platform:
        description: The platform the event is attributed to.
        type: string
      regionId:
        description: The region ID.
        type: integer
      regionName:
        description: The region name.
        type: string
      screenName:
        description: The screen name.
        type: string
      settingName:
        description: The setting name.
        type: string
      settingValue:
        description: The setting value.
        type: string
      spotId:
        description: The spot ID.
        type: integer
      spotName:
        description: The spot name.
        type: string
      version:
        description: The application version.
        type: string
    required: []

  - name: Used Chart Controls
    description: Event description missing. Consult event owner.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      chartId:
        description: The chart ID.
        type: integer
      chartName:
        description: The chart name.
        type: string
      controlUsed:
        description: The control used.
        type: string
    required: []

  - name: Used Map
    description: Event description missing. Consult event owner.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      clickedMyLocation:
        description: Was 'My Location' clicked?
        type: boolean
      clickedPin:
        description: Was a pin clicked?
        type: boolean
      clickedUpdate:
        description: Was 'Update' clicked?
        type: boolean
      lat:
        description: The latitude.
        type: string
      location:
        description: Where the event fired on our application.
        type: string
      lon:
        description: The longitude.
        type: string
      platform:
        description: The platform the event is attributed to.
        type: string
      screenName:
        description: The screen name.
        type: string
      version:
        description: The application version.
        type: string
      zoomLevel:
        description: The zoom level.
        type: number
    required: []

  - name: Used Search
    description: Event description missing. Consult event owner.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      category:
        description: The event category (used to group related events).
        type: string
      platform:
        description: The platform the event is attributed to.
        type: string
      resultsCount:
        description: The number of results.
        type: integer
      screenName:
        description: The screen name.
        type: string
      searchTerm:
        description: The search term.
        type: string
      version:
        description: The application version.
        type: string
    required: []

  - name: Viewed Notification
    description: Event description missing. Consult event owner.
    version: 1
    labels:
      owner: '@Surfline/squad-msw-buoyweather'
      priority: 2
      fires_on: 'client'
    properties:
      location:
        description: Where the event fired on our application.
        type: string
      name:
        description: The notification name.
        type: string
      orientation:
        description: The orientation of the device.
        type: string
      screenName:
        description: The screen name.
        type: string
      spotId:
        description: The spot ID.
        type: integer
      spotName:
        description: The spot name.
        type: string
      text:
        description: The notification text.
        type: string
      type:
        description: The notification type.
        type: string
      userId:
        description: The user ID.
        type: integer
      version:
        description: The application version.
        type: string
    required: []

identify:
  traits:
    properties:
      subscriptionExpiration:
        description: The expiration of the subscription (or renewal date).
        type: string
      favoritesCount:
        description: Number of favorites user currently has saved.
        type: integer
      gdprConsent:
        description: TBD
        type: boolean
      locale:
        description: Locale of user detected user IP address.
        type: string
      locationCountry:
        description: Country of user detected using IP address.
        type: string
      email:
        description: The user's email.
        type: string
      subscriptionEntitlement:
        description: The subscription entitlement. Either "pro" or "free".
        pattern: Pro|Free
        type: string
      locationContinent:
        description: Continent of user detected using IP address.
        type: string
      subscriptionActive:
        description: Is the subscription active? If so, the TRUE, else FALSE.
        type: boolean
      subscriptionPlanId:
        description:
          The subscription planID. This is used to identify multiple plans
          for given user, adoption of new subscription plans, and more.
        type: string
      subscriptionInterval:
        description: The subscription interval. Either "annual" or "monthly"
        pattern: Monthly|Annual
        type: string
    required: []

group:
  properties: {}
  context: {}
  traits: {}
