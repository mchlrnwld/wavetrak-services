# flake8: noqa
"""Dictionaries of std columns used to template dbt files."""

identifies_std_columns = [
    {
        "source_name": "id",
        "schema_name": "identify_id",
        "description": "Primary key.",
        "tests": ["not_null", "unique"],
    },
    {
        "source_name": None,
        "schema_name": "source_schema",
        "description": "The schema of the source table.",
    },
    {
        "source_name": None,
        "schema_name": "source_table",
        "description": "The source table.",
    },
    {
        "source_name": "receieved_at",
        "schema_name": "received_at_tstamp",
        "description": "The timestamp when the identify call hit Segment's API.",
    },
    {
        "source_name": "sent_at",
        "schema_name": "sent_at_tstamp",
        "description": "The timestamp when the client’s device made the network request to the Segment API.",
    },
    {
        "source_name": "timestamp",
        "schema_name": "tstamp",
        "description": (
            "When the identify call occurred. Calculated by Segment "
            "to correct client-device clock skew using the following "
            "formula; timestamp = received_at - (sent_at - originalTimestamp). "
            "Use timestamp for analysis when chronology DOES matter."
        ),
    },
    {
        "source_name": "anonymous_id",
        "schema_name": "anonymous_id",
        "description": (
            "A pseudo-unique substitute for a User ID, for cases when "
            "we don’t have an absolutely unique identifier."
        ),
    },
    {
        "source_name": "user_id",
        "schema_name": "user_id",
        "description": "Unique identifier for the user in our database.",
    },
]

users_std_columns = [
    {
        "source_name": "id",
        "schema_name": "user_id",
        "description": "Primary key.",
        "tests": ["not_null", "unique"],
    },
    {
        "source_name": None,
        "schema_name": "source_schema",
        "description": "The schema of the source table.",
    },
    {
        "source_name": None,
        "schema_name": "source_table",
        "description": "The source table.",
    },
    {
        "source_name": "received_at",
        "schema_name": "received_at_tstamp",
        "description": (
            "When the *lastest* traits for this user were last updated."
        ),
    },
    {
        "source_name": "anonymous_id",
        "schema_name": "anonymous_id",
        "description": (
            "A pseudo-unique substitute for a User ID, for cases when "
            "we don’t have an absolutely unique identifier."
        ),
    },
    {
        "source_name": "user_id",
        "schema_name": "user_id",
        "description": "Unique identifier for the user in our database.",
    },
]

# Columns only included in page calls for use in Amplitude.
pages_amplitude_reserved_columns = [
    "name",
    "path",
    "referrer",
    "search",
    "title",
    "url",
]

pages_std_columns = [
    {
        "source_name": "id",
        "schema_name": "page_id",
        "description": "Primary key.",
        "tests": ["not_null", "unique"],
    },
    {
        "source_name": None,
        "schema_name": "source_schema",
        "description": "The schema of the source table.",
    },
    {
        "source_name": None,
        "schema_name": "source_table",
        "description": "The source table.",
    },
    {
        "source_name": None,
        "schema_name": "call_type",
        "description": "The type of Segment call.",
    },
    {
        "source_name": "name",
        "schema_name": "page_name",
        "description": "The page name (used to group similar pages, e.g., `Spot Report`).",
    },
    {
        "source_name": "context_library_name",
        "schema_name": "library_name",
        "description": "The name of the library that generated the event.",
    },
    {
        "source_name": "receieved_at",
        "schema_name": "received_at_tstamp",
        "description": "The timestamp when the Page call hit Segment's API.",
    },
    {
        "source_name": "sent_at",
        "schema_name": "sent_at_tstamp",
        "description": "The timestamp when the client’s device made the network request to the Segment API.",
    },
    {
        "source_name": "timestamp",
        "schema_name": "tstamp",
        "description": "The timestamp when the Page call occurred. This is the timestamp that is passed to downstream Segment destinations and used for historical replays. Use this timestamp to define when a user saw Page X.",
    },
    {
        "source_name": "anonymous_id",
        "schema_name": "anonymous_id",
        "description": "A pseudo-unique substitute for a User ID, for cases when we don’t have an absolutely unique identifier.",
    },
    {
        "source_name": "user_id",
        "schema_name": "user_id",
        "description": "Unique identifier for the user in our database.",
    },
    {
        "source_name": "context_page_url",
        "schema_name": "page_url",
        "description": "The full URL of the page.",
    },
    {
        "source_name": None,
        "schema_name": "page_url_host",
        "description": "The page URL host.",
    },
    {
        "source_name": "context_page_path",
        "schema_name": "page_url_path",
        "description": "The path portion of the page URL.",
    },
    {
        "source_name": "context_page_title",
        "schema_name": "page_title",
        "description": "The title of the page.",
    },
    {
        "source_name": "context_page_search",
        "schema_name": "page_url_query",
        "description": "Query string portion of the page's URL.",
    },
    {
        "source_name": "context_campaign_source",
        "schema_name": "utm_source",
        "description": "UTM parameter describing the type or high-level channel of traffic (e.g., email, social - paid, search - organic, etc.).",
    },
    {
        "source_name": "context_campaign_medium",
        "schema_name": "utm_medium",
        "description": "UTM parameter describing the platform or tool that’s used to create the marketing medium (e.g., Facebook, Google, etc.).",
    },
    {
        "source_name": "context_campaign_name",
        "schema_name": "utm_campaign",
        "description": "UTM parameter describing the name of the marketing campaign (e.g., Fall Sale, etc.).",
    },
    {
        "source_name": "context_campaign_term",
        "schema_name": "utm_term",
        "description": "UTM parameter uniquely identifying (at times using an ID) a specific email, an ad in an ad group, a post on social media, the title of a blog post, a search keyword, the title of a video.",
    },
    {
        "source_name": "context_campaign_content",
        "schema_name": "utm_content",
        "description": "UTM parameter used to differentiate similar content, or links within the same ad. For example, if you have two call-to-action links within the same email message, you can use utm_content and set different values for each so you can tell which version is more effective.",
    },
    {
        "source_name": None,
        "schema_name": "gclid",
        "description": "Google AdWords click ID.",
    },
    {
        "source_name": None,
        "schema_name": "fbaid",
        "description": "Facebook Ad ID.",
    },
    {
        "source_name": None,
        "schema_name": "cid",
        "description": "Customer.io email camapign ID.",
    },
    {
        "source_name": "context_page_referrer",
        "schema_name": "referrer",
        "description": "The previous page's URL- aka the page that referred the user to the current page.",
    },
    {
        "source_name": None,
        "schema_name": "referrer_host",
        "description": "The hostname of the URL that referred the user to the current page.",
    },
    {
        "source_name": "context_ip",
        "schema_name": "ip",
        "description": "The IP address of the user.",
    },
    {
        "source_name": "context_user_agent",
        "schema_name": "user_agent",
        "description": "The user agent of the device making the request.",
    },
    {
        "source_name": None,
        "schema_name": "device",
        "description": "The device making the request.",
    },
    {
        "source_name": None,
        "schema_name": "is_native_web_view",
        "description": "Whether or not the page is a native web view.",
    },
    {
        "source_name": None,
        "schema_name": "device_category",
        "description": "The device category.",
    },
]

screens_std_columns = [
    {
        "source_name": "id",
        "schema_name": "sreen_id",
        "description": "Primary key.",
        "tests": ["not_null", "unique"],
    },
    {
        "source_name": None,
        "schema_name": "source_schema",
        "description": "The schema of the source table.",
    },
    {
        "source_name": None,
        "schema_name": "source_table",
        "description": "The source table.",
    },
    {
        "source_name": None,
        "schema_name": "call_type",
        "description": "The type of Segment call.",
    },
    {
        "source_name": "name",
        "schema_name": "screen_name",
        "description": "The screen name (used to group similar pages, e.g., `Spot`).",
    },
    {
        "source_name": "context_library_name",
        "schema_name": "library_name",
        "description": "The name of the library that generated the event.",
    },
    {
        "source_name": "receieved_at",
        "schema_name": "received_at_tstamp",
        "description": "The timestamp when the Page call hit Segment's API.",
    },
    {
        "source_name": "sent_at",
        "schema_name": "sent_at_tstamp",
        "description": "The timestamp when the client’s device made the network request to the Segment API.",
    },
    {
        "source_name": "timestamp",
        "schema_name": "tstamp",
        "description": "The timestamp when the Page call occurred. This is the timestamp that is passed to downstream Segment destinations and used for historical replays. Use this timestamp to define when a user saw Page X.",
    },
    {
        "source_name": "anonymous_id",
        "schema_name": "anonymous_id",
        "description": "A pseudo-unique substitute for a User ID, for cases when we don’t have an absolutely unique identifier.",
    },
    {
        "source_name": "user_id",
        "schema_name": "user_id",
        "description": "Unique identifier for the user in our database.",
    },
    {
        "source_name": "context_device_id",
        "schema_name": "device_id",
        "description": "The device ID.",
    },
    {
        "source_name": "context_device_model",
        "schema_name": "device",
        "description": "The device.",
    },
    {
        "source_name": None,
        "schema_name": "device_version",
        "description": "The device version.",
    },
    {
        "source_name": "context_os_name",
        "schema_name": "device_os_name",
        "description": "The device operating system.",
    },
    {
        "source_name": "context_os_version",
        "schema_name": "device_os_version",
        "description": "The device operating system version.",
    },
    {
        "source_name": "context_network_wifi",
        "schema_name": "device_on_wifi",
        "description": "Whether or not the device is connected to Wi-Fi.",
    },
    {
        "source_name": "context_ip",
        "schema_name": "ip",
        "description": "The IP address of the user.",
    },
    {
        "source_name": "context_app_name",
        "schema_name": "app_name",
        "description": "The name of the application.",
    },
    {
        "source_name": "context_app_version",
        "schema_name": "app_version",
        "description": "The version of the application.",
    },
    {
        "source_name": "context_app_build",
        "schema_name": "app_build",
        "description": "The build of the application.",
    },
    {
        "source_name": "context_device_ad_tracking_enabled",
        "schema_name": "ad_tracking_enabled",
        "description": "Whether or not the device is ad tracking enabled.",
    },
    {
        "source_name": "context_device_advertising_id",
        "schema_name": "device_ad_id",
        "description": "The device advertising ID.",
    },
    {
        "source_name": None,
        "schema_name": "device_category",
        "description": "The device category.",
    },
]

tracks_std_columns_web = [
    {
        "source_name": "id",
        "schema_name": "event_id",
        "description": "Primary key.",
        "tests": ["not_null", "unique"],
    },
    {
        "source_name": None,
        "schema_name": "source_schema",
        "description": "The schema of the source table.",
    },
    {
        "source_name": None,
        "schema_name": "source_table",
        "description": "The source table.",
    },
    {
        "source_name": None,
        "schema_name": "call_type",
        "description": "The type of Segment call.",
    },
    {
        "source_name": "event_text",
        "schema_name": "event_name",
        "description": "The event name.",
    },
    {
        "source_name": "context_library_name",
        "schema_name": "library_name",
        "description": "The name of the library that generated the event.",
    },
    {
        "source_name": "receieved_at",
        "schema_name": "received_at_tstamp",
        "description": "The timestamp when the Page call hit Segment's API.",
    },
    {
        "source_name": "sent_at",
        "schema_name": "sent_at_tstamp",
        "description": "The timestamp when the client’s device made the network request to the Segment API.",
    },
    {
        "source_name": "timestamp",
        "schema_name": "tstamp",
        "description": "The timestamp when the Page call occurred. This is the timestamp that is passed to downstream Segment destinations and used for historical replays. Use this timestamp to define when a user saw Page X.",
    },
    {
        "source_name": "anonymous_id",
        "schema_name": "anonymous_id",
        "description": "A pseudo-unique substitute for a User ID, for cases when we don’t have an absolutely unique identifier.",
    },
    {
        "source_name": "user_id",
        "schema_name": "user_id",
        "description": "Unique identifier for the user in our database.",
    },
    {
        "source_name": "url",
        "schema_name": "page_url",
        "description": "The page URL.",
    },
    {
        "source_name": None,
        "schema_name": "page_url_host",
        "description": "The page URL host.",
    },
    {
        "source_name": "context_page_path",
        "schema_name": "page_url_path",
        "description": "The page URL path.",
    },
    {
        "source_name": "context_page_title",
        "schema_name": "page_title",
        "description": "The page title.",
    },
    {
        "source_name": "context_page_search",
        "schema_name": "page_url_query",
        "description": "The search query terms from the URL.",
    },
    {
        "source_name": "context_campaign_source",
        "schema_name": "utm_source",
        "description": "UTM parameter describing the type or high-level channel of traffic (e.g., email, social - paid, search - organic, etc.).",
    },
    {
        "source_name": "context_campaign_medium",
        "schema_name": "utm_medium",
        "description": "UTM parameter describing the platform or tool that’s used to create the marketing medium (e.g., Facebook, Google, etc.).",
    },
    {
        "source_name": "context_campaign_name",
        "schema_name": "utm_campaign",
        "description": "UTM parameter describing the name of the marketing campaign (e.g., Fall Sale, etc.).",
    },
    {
        "source_name": "context_campaign_term",
        "schema_name": "utm_term",
        "description": "UTM parameter uniquely identifying (at times using an ID) a specific email, an ad in an ad group, a post on social media, the title of a blog post, a search keyword, the title of a video.",
    },
    {
        "source_name": "context_campaign_content",
        "schema_name": "utm_content",
        "description": "UTM parameter used to differentiate similar content, or links within the same ad. For example, if you have two call-to-action links within the same email message, you can use utm_content and set different values for each so you can tell which version is more effective.",
    },
    {
        "source_name": None,
        "schema_name": "gclid",
        "description": "Google AdWords click ID.",
    },
    {
        "source_name": None,
        "schema_name": "fbaid",
        "description": "Facebook Ad ID.",
    },
    {
        "source_name": None,
        "schema_name": "cid",
        "description": "Customer.io email camapign ID.",
    },
    {
        "source_name": "context_page_referrer",
        "schema_name": "referrer",
        "description": "The URL of the page that referred the user to the current page.",
    },
    {
        "source_name": None,
        "schema_name": "referrer_host",
        "description": "The hostname of the URL that referred the user to the current page.",
    },
    {
        "source_name": "context_ip",
        "schema_name": "ip",
        "description": "The IP address of the user.",
    },
    {
        "source_name": "context_user_agent",
        "schema_name": "user_agent",
        "description": "The user agent of the device making the request.",
    },
    {
        "source_name": None,
        "schema_name": "device",
        "description": "The device making the request.",
    },
    {
        "source_name": None,
        "schema_name": "is_native_web_view",
        "description": "Whether or not the page is a native web view.",
    },
    {
        "source_name": None,
        "schema_name": "device_category",
        "description": "The device category.",
    },
]

tracks_std_columns_mobile = [
    {
        "source_name": "id",
        "schema_name": "event_id",
        "description": "Primary key.",
        "tests": ["not_null", "unique"],
    },
    {
        "source_name": None,
        "schema_name": "source_schema",
        "description": "The schema of the source table.",
    },
    {
        "source_name": None,
        "schema_name": "source_table",
        "description": "The source table.",
    },
    {
        "source_name": None,
        "schema_name": "call_type",
        "description": "The type of Segment call.",
    },
    {
        "source_name": "event_text",
        "schema_name": "event_name",
        "description": "The event name.",
    },
    {
        "source_name": "context_library_name",
        "schema_name": "library_name",
        "description": "The name of the library that generated the event.",
    },
    {
        "source_name": "receieved_at",
        "schema_name": "received_at_tstamp",
        "description": "The timestamp when the Page call hit Segment's API.",
    },
    {
        "source_name": "sent_at",
        "schema_name": "sent_at_tstamp",
        "description": "The timestamp when the client’s device made the network request to the Segment API.",
    },
    {
        "source_name": "timestamp",
        "schema_name": "tstamp",
        "description": "The timestamp when the Page call occurred. This is the timestamp that is passed to downstream Segment destinations and used for historical replays. Use this timestamp to define when a user saw Page X.",
    },
    {
        "source_name": "anonymous_id",
        "schema_name": "anonymous_id",
        "description": "A pseudo-unique substitute for a User ID, for cases when we don’t have an absolutely unique identifier.",
    },
    {
        "source_name": "user_id",
        "schema_name": "user_id",
        "description": "Unique identifier for the user in our database.",
    },
    {
        "source_name": "context_device_id",
        "schema_name": "device_id",
        "description": "The device ID.",
    },
    {
        "source_name": "context_device_model",
        "schema_name": "device",
        "description": "The device.",
    },
    {
        "source_name": None,
        "schema_name": "device_version",
        "description": "The device version.",
    },
    {
        "source_name": "context_os_name",
        "schema_name": "device_os_name",
        "description": "The device operating system.",
    },
    {
        "source_name": "context_os_version",
        "schema_name": "device_os_version",
        "description": "The device operating system version.",
    },
    {
        "source_name": "context_network_wifi",
        "schema_name": "device_on_wifi",
        "description": "Whether or not the device is connected to Wi-Fi.",
    },
    {
        "source_name": "context_ip",
        "schema_name": "ip",
        "description": "The IP address of the user.",
    },
    {
        "source_name": "context_app_name",
        "schema_name": "app_name",
        "description": "The name of the application.",
    },
    {
        "source_name": "context_app_version",
        "schema_name": "app_version",
        "description": "The version of the application.",
    },
    {
        "source_name": "context_app_build",
        "schema_name": "app_build",
        "description": "The build of the application.",
    },
    {
        "source_name": "context_device_ad_tracking_enabled",
        "schema_name": "ad_tracking_enabled",
        "description": "Whether or not the device is ad tracking enabled.",
    },
    {
        "source_name": "context_device_advertising_id",
        "schema_name": "device_ad_id",
        "description": "The device advertising ID.",
    },
    {
        "source_name": None,
        "schema_name": "device_category",
        "description": "The device category.",
    },
]


event_std_columns = {
    "id": [
        {
            "source_name": "id",
            "schema_name": "event_id",
            "description": "Primary key.",
        }
    ],
    "event": [
        {
            "source_name": "event",
            "schema_name": "source_table",
            "description": "The name of source table.",
        }
    ],
    "event_text": [
        {
            "source_name": "event_text",
            "schema_name": "event_name",
            "description": "The event name.",
        }
    ],
    "original_timestamp": [
        {
            "source_name": "original_timestamp",
            "schema_name": None,
            "description": "Time on the client device when call was invoked, OR the timestamp value manually passed in through server-side libraries.",
        }
    ],
    "sent_at": [
        {
            "source_name": "sent_at",
            "schema_name": "sent_at_tstamp",
            "description": "Time on client device when call was sent, OR sent_at value manually passed in. NOTE - sent_at is NOT USEFUL for analysis since it’s not always trustworthy as it can be easily adjusted and affected by clock skew.",
        }
    ],
    "received_at": [
        {
            "source_name": "received_at",
            "schema_name": "received_at_tstamp",
            "description": "Time on Segment server clock when call was received. received_at is used as sort key in Warehouses. For max query speed, received_at is the recommended timestamp for analysis when chronology DOES NOT matter as chronology is NOT ENSURED.",
        }
    ],
    "timestamp": [
        {
            "source_name": "timestamp",
            "schema_name": "tstamp",
            "description": "When the event occurred. Calculated by Segment to correct client-device clock skew using the following formula; timestamp = received_at - (sent_at - originalTimestamp). Use timestamp for analysis when chronology DOES matter.",
        }
    ],
    "anonymous_id": [
        {
            "source_name": "anonymous_id",
            "schema_name": "anonymous_id",
            "description": "A pseudo-unique substitute for a User ID, for cases when we don’t have an absolutely unique identifier. A user_id or an anonymous_id is required for all events.",
        }
    ],
    "user_id": [
        {
            "source_name": "user_id",
            "schema_name": "user_id",
            "description": "Unique identifier for the user in our database. A user_id or an anonymous_id is required for all events.",
        }
    ],
    "context_campaign_name": [
        {
            "source_name": "context_campaign_name",
            "schema_name": "utm_campaign",
            "description": "The name of the campaign that the user was in when the event occurred. Maps directly to utm_campaign parameter.",
        }
    ],
    "context_campaign_source": [
        {
            "source_name": "context_campaign_source",
            "schema_name": "utm_source",
            "description": "The source of the campaign that the user was in when the event occurred. Maps directly to utm_source parameter.",
        }
    ],
    "context_campaign_medium": [
        {
            "source_name": "context_campaign_medium",
            "schema_name": "utm_medium",
            "description": "The medium of the campaign that the user was in when the event occurred. Maps directly to utm_medium parameter.",
        }
    ],
    "context_campaign_term": [
        {
            "source_name": "context_campaign_term",
            "schema_name": "utm_term",
            "description": "The term of the campaign that the user was in when the event occurred. Maps directly to utm_term parameter.",
        }
    ],
    "context_campaign_content": [
        {
            "source_name": "context_campaign_content",
            "schema_name": "utm_content",
            "description": "The content of the campaign that the user was in when the event occurred. Maps directly to utm_content parameter.",
        }
    ],
    "context_library_name": [
        {
            "source_name": "context_library_name",
            "schema_name": "library_name",
            "description": "The name of the library that generated the event.",
        }
    ],
    "context_library_version": [
        {
            "source_name": "context_library_version",
            "schema_name": "library_version",
            "description": "The version of the library that generated the event.",
        }
    ],
    "context_app_name": [
        {
            "source_name": "context_app_name",
            "schema_name": "app_name",
            "description": "The name of the app that generated the event.",
        }
    ],
    "context_app_namespace": [
        {
            "source_name": "context_app_namespace",
            "schema_name": None,  # Not used in schema.yml files
            "description": "The namespace of the app that generated the event.",
        }
    ],
    "context_app_version": [
        {
            "source_name": "context_app_version",
            "schema_name": "app_version",
            "description": "The version of the app that generated the event.",
        }
    ],
    "context_app_build": [
        {
            "source_name": "context_app_build",
            "schema_name": "app_build",
            "description": "The build of the app that generated the event.",
        }
    ],
    "context_device_id": [
        {
            "source_name": "context_device_id",
            "schema_name": "device_id",
            "description": "The ID of the device that generated the event.",
        }
    ],
    "context_device_type": [
        {
            "source_name": "context_device_type",
            "schema_name": "device_type",
            "description": "The type of device that generated the event.",
        }
    ],
    "context_device_model": [
        {
            "source_name": "context_device_model",
            "schema_name": "device_model",
            "description": "The model of device that generated the event.",
        },
        {
            "source_name": None,
            "schema_name": "device_version",
            "description": "The version of the device that generated the event.",
        },
    ],
    "context_device_manufacturer": [
        {
            "source_name": "context_device_manufacturer",
            "schema_name": "device_manufacturer",
            "description": "The manufacturer of device that generated the event.",
        }
    ],
    "context_device_ad_tracking_enabled": [
        {
            "source_name": "context_device_ad_tracking_enabled",
            "schema_name": "ad_tracking_enabled",
            "description": "Whether or not the device has ad tracking enabled for advertising purposes.",
        }
    ],
    "context_device_advertising_id": [
        {
            "source_name": "context_device_advertising_id",
            "schema_name": "advertising_id",
            "description": "The advertising ID of the device that generated the event.",
        }
    ],
    "context_timezone": [
        {
            "source_name": "context_timezone",
            "schema_name": "device_timezone",
            "description": "The timezone of the device that generated the event.",
        }
    ],
    "context_screen_height": [
        {
            "source_name": "context_screen_height",
            "schema_name": "screen_height",
            "description": "The height of the device screen in pixels.",
        }
    ],
    "context_screen_width": [
        {
            "source_name": "context_screen_width",
            "schema_name": "screen_width",
            "description": "The width of the device screen in pixels.",
        }
    ],
    "context_screen_density": [
        {
            "source_name": "context_screen_density",
            "schema_name": "screen_density",
            "description": "The density of the device screen in pixels per inch.",
        }
    ],
    "context_os_name": [
        {
            "source_name": "context_os_name",
            "schema_name": "device_os_name",
            "description": "The operating system on the device.",
        }
    ],
    "context_os_version": [
        {
            "source_name": "context_os_version",
            "schema_name": "device_os_version",
            "description": "The version of the operating system on the device.",
        }
    ],
    "context_ip": [
        {
            "source_name": "context_ip",
            "schema_name": "ip",
            "description": "The IP address the event was generated from.",
        }
    ],
    "context_locale": [
        {
            "source_name": "context_locale",
            "schema_name": "locale",
            "description": "Locale string for the device generating the event, for example 'en-US'.",
        }
    ],
    "context_user_agent": [
        {
            "source_name": "context_user_agent",
            "schema_name": "user_agent",
            "description": "The user agent string of the device generating the event.",
        }
    ],
    "context_network_carrier": [
        {
            "source_name": "context_network_carrier",
            "schema_name": "network_carrier",
            "description": "The carrier of the device's network connection.",
        }
    ],
    "context_network_cellular": [
        {
            "source_name": "context_network_cellular",
            "schema_name": "network_cellular",
            "description": "Whether or not the device is connected to a cellular network.",
        }
    ],
    "context_network_wifi": [
        {
            "source_name": "context_network_wifi",
            "schema_name": "network_wifi",
            "description": "Whether or not the device is connected to a wifi network.",
        }
    ],
    "context_network_bluetooth": [
        {
            "source_name": "context_network_bluetooth",
            "schema_name": "network_bluetooth",
            "description": "Whether or not the device is connected to a bluetooth network.",
        }
    ],
    "context_page_url": [
        {
            "source_name": "context_page_url",
            "schema_name": "page_url",
            "description": "The URL of the page where the event occurred.",
        },
        {
            "source_name": None,
            "schema_name": "page_url_host",
            "description": "The hostname of the page where the event occurred.",
        },
        {
            "source_name": None,
            "schema_name": "is_native_web_view",
            "description": "Whether or not the event was generated from a native web view.",
        },
    ],
    "context_page_path": [
        {
            "source_name": "context_page_path",
            "schema_name": "page_url_path",
            "description": "The path of the page where the event occurred.",
        }
    ],
    "context_page_search": [
        {
            "source_name": "context_page_search",
            "schema_name": "page_url_query",
            "description": "The the URL search query parameters of the page where the event occurred.",
        }
    ],
    "context_page_title": [
        {
            "source_name": "context_page_title",
            "schema_name": "page_title",
            "description": "The title of the page where the event occurred.",
        }
    ],
    "context_page_referrer": [
        {
            "source_name": "context_page_referrer",
            "schema_name": "referrer",
            "description": "The URL of the page that referred the user to the page where the event occurred.",
        },
        {
            "source_name": None,
            "schema_name": "referrer_host",
            "description": "The hostname of the page that referred the user to the page where the event occurred.",
        },
    ],
    "context_integrations_name": [
        {
            "source_name": "context_integrations_name",
            "schema_name": "integration_name",
            "description": "The name of the integration that generated the event.",
        }
    ],
    "context_integrations_version": [
        {
            "source_name": "context_integrations_version",
            "schema_name": "integration_version",
            "description": "The version of the integration that generated the event.",
        }
    ],
    "context_integration_name": [
        {
            "source_name": "context_integration_name",
            "schema_name": "integration_name",
            "description": "The name of the integration that generated the event.",
        }
    ],
    "context_integration_version": [
        {
            "source_name": "context_integration_version",
            "schema_name": "integration_version",
            "description": "The version of the integration that generated the event.",
        }
    ],
    "context_integrations_appboy": [
        {
            "source_name": "context_integrations_appboy",
            "schema_name": None,
            "description": "A flag indicating the payload of this event should not be sent back to Braze (aka Appboy).",
        }
    ],
    "context_traits_email": [
        {
            "source_name": "context_traits_email",
            "schema_name": "email",
            "description": "The email address of the user.",
        }
    ],
    "context_message_id": [
        {
            "source_name": "context_message_id",
            "schema_name": None,
            "description": "The message ID of the event. Used by Segment for de-duplication checks.",
        }
    ],
    "context_event_triggered_at": [
        {
            "source_name": "context_event_triggered_at",
            "schema_name": None,
            "description": "The time the event was triggered in Vero.",
        }
    ],
    "context_event_name": [
        {
            "source_name": "context_event_name",
            "schema_name": None,
            "description": "The name of the event in Vero.",
        }
    ],
    "context_event_data_update_payment_details_url": [
        {
            "source_name": "context_event_data_update_payment_details_url",
            "schema_name": None,
            "description": "The URL of the payment details update page.",
        }
    ],
    "context_event_data_timestamp": [
        {
            "source_name": "context_event_data_timestamp",
            "schema_name": None,
            "description": "The timestamp of the event in Vero.",
        }
    ],
    "context_event_data_reset_url": [
        {
            "source_name": "context_event_data_reset_url",
            "schema_name": None,
            "description": "The URL of the password reset page.",
        }
    ],
    "context_event_data_payment_attempt": [
        {
            "source_name": "context_event_data_payment_attempt",
            "schema_name": None,
            "description": "The payment attempt number.",
        }
    ],
    "context_event_data_expiry_hours": [
        {
            "source_name": "context_event_data_expiry_hours",
            "schema_name": None,
            "description": "The number of hours until the update payments URL expires.",
        }
    ],
    "context_protocols_source_id": [
        {
            "source_name": "context_protocols_source_id",
            "schema_name": "segment_protocols_source_id",
            "description": "If a protocol violation is detected for this instance of the event firing, the ID of the Segment source.",
        }
    ],
    "context_protocols_source_name": [
        {
            "source_name": "context_protocols_source_name",
            "schema_name": "segment_protocols_source_name",
            "description": "If a protocol violation is detected for this instance of the event firing, the name of the Segment source.",
        }
    ],
    "context_protocols_violations": [
        {
            "source_name": "context_protocols_violations",
            "schema_name": "segment_protocols_violations",
            "description": "If a protocol violation is detected for this instance of the event firing, a list of the protocol violations.",
        }
    ],
    "context_protocols_omitted": [
        {
            "source_name": "context_protocols_omitted",
            "schema_name": "segment_protocols_omitted_properties",
            "description": "If this instance of the event fired with unplanned properties (not in tracking plan), a list of the unplanned properties, which are ommitted from the event payload and do not reach thea data warehouse.",
        }
    ],
}

# When we find specific "parent" columns in an event's table in Redshift, we
# also need to include certain "children" column(s) to the schema.yml and dbt
# SQL model for the event.
event_parent_child_columns = {
    "context_device_model": [
        {"name": "device", "description": "The device generating the event"},
        {
            "name": "device_version",
            "description": "The version of the device generating the event.",
        },
    ],
    "context_page_url": [
        {
            "name": "page_url_host",
            "description": "The host of the page where the event occurred.",
        },
        {
            "name": "is_native_web_view",
            "description": "Whether or not the event was generated from a native web view.",
        },
    ],
    "context_page_referrer": [
        {
            "name": "referrer_host",
            "description": "The hostname of the URL that referred the user to the current page where the event ocurred.",
        }
    ],
}
