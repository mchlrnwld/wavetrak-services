"""Parses Segment tracking plans and modifies/writes the following dbt files:
    - src_brand_platform.yml
        - Adds `pages` table for the brand_platform Segment source
    - schema.yml
        - Adds a *staged* `stg_brand_platform__pages` model to the
          brand_platform's staging folder schema.yml (for docs and testing).
    - stg_brand_platform__pages.sql
        - The dbt SQL that defines the `stg_brand_platform__pages` model
"""

import json
import logging
import os
from logging.config import fileConfig
from os import listdir
from os.path import isfile, join

import yaml
from dotenv import load_dotenv

from dbt_default_columns import (
    pages_amplitude_reserved_columns,
    pages_std_columns,
)
from dbt_helpers import (
    MyDumper,
    connect_to_redshift,
    format_schema,
    format_source,
    get_columns_in_table,
    redshift_case,
)
from dbt_secrets import DB_HOST_URL, DB_NAME, DB_PASSWORD, DB_PORT, DB_USER
from dbt_source_mapping import (
    source_brand_platform_dict,
    source_redshift_schema_dict,
)


def main():
    """Entry point into script."""
    # Load .env file if it exists
    load_dotenv()

    # Determine if dev or prod, find tracking plans accordingly
    if os.getenv("ENVIRONMENT") == "dev":
        SURFLINE_SEGMENT_DIR = os.getenv("SURFLINE_SEGMENT_DIR")
        SURFLINE_SEGMENT_LOGGING_DIR = SURFLINE_SEGMENT_DIR
        tracking_plan_folder = os.path.join(
            SURFLINE_SEGMENT_DIR, "tracking_plans_yaml"
        )

        # Get list of tracking plan paths
        tracking_plans = [
            os.path.join(tracking_plan_folder, f)
            for f in listdir(tracking_plan_folder)
            if isfile(join(tracking_plan_folder, f)) and f.endswith(".yaml")
        ]

    else:  # Running in Github Actions
        HOME = os.getenv("HOME")
        SURFLINE_SEGMENT_DIR = os.getenv("SURFLINE_SEGMENT_DIR")
        SURFLINE_SEGMENT_LOGGING_DIR = os.getenv(
            "SURFLINE_SEGMENT_LOGGING_DIR"
        )

        # Get modified/new files from 'trilom/file-changes-action'
        modified_files_json = os.path.join(HOME, "files_modified.json")
        added_files_json = os.path.join(HOME, "files_added.json")

        with open(modified_files_json) as f:
            modified_files = json.load(f)

        with open(added_files_json) as f:
            added_files = json.load(f)

        # Generate a list of the modified tracking plans
        modified_plans = [
            os.path.join(SURFLINE_SEGMENT_DIR, f)
            for f in modified_files
            if "/tracking_plans_yaml/" in f and f.endswith(".yaml")
        ]
        # Generate a list of the added tracking plans
        added_plans = [
            os.path.join(SURFLINE_SEGMENT_DIR, f)
            for f in added_files
            if "/tracking_plans_yaml/" in f and f.endswith(".yaml")
        ]

        # Combine the new and modified tracking plan lists
        tracking_plans = modified_plans + added_plans

    # Create pages table/model for Wavetrak web applications only
    platforms = ["web"]
    tracking_plans = [
        tracking_plan
        for tracking_plan in tracking_plans
        if any(platform in tracking_plan for platform in platforms)
    ]

    # Setup logging
    log_file_path = os.path.join(
        SURFLINE_SEGMENT_LOGGING_DIR, "logging-dbt-template.ini"
    )
    fileConfig(log_file_path)
    logger = logging.getLogger(__name__)

    # Set directory where dbt files will be generated
    dbt_codegen_folder = os.path.join(SURFLINE_SEGMENT_DIR, "dbt/codegen")

    # Iterate through each tracking plan
    for tracking_plan in tracking_plans:
        tracking_plan_name = tracking_plan.split("/")[-1].replace(".yaml", "")

        # Set top-level template folder, read folder, write folder
        template_folder = os.path.join(SURFLINE_SEGMENT_DIR, "dbt/templates")
        sub_template_folder = os.path.join(template_folder, "pages")
        write_folder = os.path.join(
            dbt_codegen_folder, tracking_plan_name.replace("-", "_")
        )

        # If the write folder does not exist (new tracking plan), create it
        if not os.path.exists(write_folder):
            os.makedirs(write_folder)

        logger.info(
            f"Templating dbt files for:\n"
            f"    Tracking plan: {tracking_plan_name}\n"
            f"    Source table: pages\n"
            f"    Writing to folder: {write_folder}\n"
        )

        # Set the template files to use
        template_source = os.path.join(template_folder, "template_source.yml")
        template_schema = os.path.join(template_folder, "template_schema.yml")
        template_table = os.path.join(
            sub_template_folder, "template_pages_table.yml"
        )
        template_model = os.path.join(
            sub_template_folder, "template_pages_model.yml"
        )
        template_column = os.path.join(
            sub_template_folder, "template_pages_column.yml"
        )
        template_staging = os.path.join(
            sub_template_folder, "template_pages_staging.sql"
        )

        # Set special variables.
        # We parse templates for strings like "__SCHEMA_NAME__" and
        # replace with corresponding special variable. Special variables are
        # determined from the tracking plan and event. This pattern/approach is
        # used throughout this script to inject values into the templates.
        __SCHEMA_NAME__ = source_redshift_schema_dict[tracking_plan_name]
        __BRAND_PLATFORM__ = source_brand_platform_dict[tracking_plan_name]
        __BRAND_HIPHEN_PLATFORM__ = tracking_plan_name
        __TABLE_NAME__ = "pages"
        __MODEL_NAME__ = "stg___BRAND_PLATFORM____pages".replace(
            "__BRAND_PLATFORM__", __BRAND_PLATFORM__
        )
        source_yml_filename = "src_" + __BRAND_PLATFORM__ + ".yml"
        schema_yml_filename = "schema.yml"
        source_yml_path = os.path.join(write_folder, source_yml_filename)
        schema_yml_path = os.path.join(write_folder, schema_yml_filename)

        # If the source.yml file does not exist, create it
        if not os.path.exists(source_yml_path):
            # Initialize a source object using template
            with open(template_source, "r") as f:
                source = yaml.safe_load(f)

            # Inject __SCHEMA_NAME__, etc. into the source object
            source["sources"][0]["name"] = __SCHEMA_NAME__
            source["sources"][0]["description"] = (
                source["sources"][0]["description"]
                .replace("__BRAND-PLATFORM__", __BRAND_HIPHEN_PLATFORM__)
                .replace("__SCHEMA_NAME__", __SCHEMA_NAME__)
            )

        else:
            # src_brand_platform.yml file exists, so read it
            with open(source_yml_path, "r") as f:
                source = yaml.safe_load(f)

        # If the schema.yml file does not exist, create it
        if not os.path.exists(schema_yml_path):
            # Initialize a schema object using template
            with open(template_schema, "r") as f:
                schema = yaml.safe_load(f)

        else:
            # src_brand_platform.yml file exists, so read it
            with open(schema_yml_path, "r") as f:
                schema = yaml.safe_load(f)

        # Initalize a table and model objects using templates
        with open(template_table, "r") as f:
            table = yaml.safe_load(f)

        with open(template_model, "r") as f:
            model = yaml.safe_load(f)

        # Inject __TABLE_NAME__, __MODEL_NAME__, etc. into table and model
        table["name"] = table["name"].replace("__TABLE_NAME__", __TABLE_NAME__)
        table["description"] = table["description"].replace(
            "__BRAND-PLATFORM__", __BRAND_HIPHEN_PLATFORM__
        )
        model["name"] = model["name"].replace("__MODEL_NAME__", __MODEL_NAME__)
        model["description"] = model["description"].replace(
            "__BRAND-PLATFORM__", __BRAND_HIPHEN_PLATFORM__
        )

        # Connect to redshift and get column names from source table
        con = connect_to_redshift(
            DB_NAME, DB_PORT, DB_USER, DB_PASSWORD, DB_HOST_URL
        )
        redshift_columns = get_columns_in_table(
            __SCHEMA_NAME__, __TABLE_NAME__, con
        )
        redshift_columns.sort()

        # Load the YAML tracking plan
        with open(tracking_plan, "r") as f:
            yaml_plan = yaml.safe_load(f)

        # Determine standard columns for the source table and schema model
        for std_column_dict in pages_std_columns:
            if std_column_dict["source_name"] is not None:
                # Initialize a table column object using template
                with open(template_column, "r") as f:
                    table_column = yaml.safe_load(f)

                table_column["name"] = std_column_dict["source_name"]
                table_column["description"] = std_column_dict["description"]

                table["columns"].append(table_column)
            else:
                pass

            if std_column_dict["schema_name"] is not None:
                # Initialize a table column object using template
                with open(template_column, "r") as f:
                    model_column = yaml.safe_load(f)

                model_column["name"] = std_column_dict["schema_name"]
                model_column["description"] = std_column_dict["description"]

                model["columns"].append(model_column)
            else:
                pass

        # Get all events from tracking plan
        events = yaml_plan["events"]

        # Determine if `Page Viewed` is in the event list
        page_viewed = False

        for idx, event in enumerate(events):
            if event["name"] == "Page Viewed":
                page_viewed = True
                page_viewed_idx = idx
                break

            else:
                pass

        sql_page_properties = []

        if page_viewed:
            page_properties = events[page_viewed_idx]["properties"]

            for property_name, property_dict in sorted(
                page_properties.items()
            ):
                # Page property must be in the source table in Redshift, not
                # just the tracking plan. It may NOT be in the list of reserved
                # column names that.
                if (
                    redshift_case(property_name) in redshift_columns
                    and redshift_case(property_name)
                    not in pages_amplitude_reserved_columns
                ):
                    # Add to page properties
                    sql_page_properties.append(redshift_case(property_name))

                    with open(template_column) as f:
                        table_column = yaml.safe_load(f)

                    table_column["name"] = redshift_case(property_name)
                    table_column["description"] = property_dict["description"]

                    with open(template_column) as f:
                        model_column = yaml.safe_load(f)

                    model_column["name"] = redshift_case(property_name)
                    model_column["description"] = property_dict["description"]

                    table["columns"].append(table_column)
                    model["columns"].append(model_column)

        else:
            pass

        # Append table to source, model to schema
        source["sources"][0]["tables"].append(table)
        schema["models"].append(model)

        if sql_page_properties != []:
            screen_properties_list_str = ""

            for column in sql_page_properties:
                screen_properties_list_str += "'" + column + "', "

            __PAGE_PROPERTIES__ = "[ " + screen_properties_list_str + " ]"

        else:
            __PAGE_PROPERTIES__ = "[]"

        # Initialize a staging object using the template
        with open(template_staging, "r") as f:
            staging = f.read()

        # Replace "__SCHEMA_NAME__", "__TABLE_NAME__", and
        # "__PAGE_PROPERTIES__" strings in SQL template with variables
        staging = (
            staging.replace("__SCHEMA_NAME__", __SCHEMA_NAME__)
            .replace("__TABLE_NAME__", __TABLE_NAME__)
            .replace("__PAGE_PROPERTIES__", __PAGE_PROPERTIES__)
        )

        # Write staging object to file
        with open(
            os.path.join(write_folder, __MODEL_NAME__ + ".sql"), "w"
        ) as f:
            f.write(staging)

        # Dump schema object to schema.yml
        with open(os.path.join(write_folder, "schema.yml"), "w") as f:
            yaml.dump(
                schema,
                f,
                indent=2,
                width=70,
                Dumper=MyDumper,
                sort_keys=False,
                default_flow_style=False,
                allow_unicode=True,
                encoding=("utf-8"),
            )

        # Format schema.yml for readability
        with open(os.path.join(write_folder, "schema.yml"), "r") as f:
            schema_string = f.read()

        # Write formatted schema.yml back to the file
        with open(os.path.join(write_folder, "schema.yml"), "w") as f:
            f.write(format_schema(schema_string))

        # Dump source object to src_brand_platform.yml
        with open(os.path.join(write_folder, source_yml_filename), "w") as f:
            yaml.dump(
                source,
                f,
                indent=2,
                width=70,
                Dumper=MyDumper,
                sort_keys=False,
                default_flow_style=False,
                allow_unicode=True,
                encoding=("utf-8"),
            )

        # Format src_brand_platform.yml for readability
        with open(os.path.join(write_folder, source_yml_filename), "r") as f:
            source_string = f.read()

        # Write formatted src_brand_platform.yml back to the file
        with open(os.path.join(write_folder, source_yml_filename), "w") as f:
            f.write(format_source(source_string))


if __name__ == "__main__":
    main()
