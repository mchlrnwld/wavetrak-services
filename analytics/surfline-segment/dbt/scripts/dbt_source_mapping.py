# flake8: noqa

# Tracking plan names mapped to corresponding Redshift schema
source_redshift_schema_dict = {
    "surfline-web": "surfline",
    "surfline-ios": "surfline_ios",
    "surfline-android": "surfline_android",
    "magicseaweed-web": "msw_web",
    "magicseaweed-ios": "msw_ios",
    "magicseaweed-android": "msw_android",
    "buoyweather-web": "buoyweather",
    "buoyweather-ios": "buoyweather_ios",
    "buoyweather-android": "buoyweatherandroid",
    "fishtrack-web": "fishtrack",
    "fishtrack-ios": "fishtrack_ios",
    "fishtrack-android": "fishtrackandroid",
    "braze": "braze",
    "vero": "vero",
    "customer-io-surfline": "customer_io_surfline",
    "customer-io-buoyweather": "customer_io_buoyweather",
    "customer-io-fishtrack": "customer_io_fishtrack",
    "split-production": "split_production",
}

# Tracking plan names mapped to corresponding brand_platform string
source_brand_platform_dict = {
    "surfline-web": "surfline_web",
    "surfline-ios": "surfline_ios",
    "surfline-android": "surfline_android",
    "magicseaweed-web": "magicseaweed_web",
    "magicseaweed-ios": "magicseaweed_ios",
    "magicseaweed-android": "magicseaweed_android",
    "buoyweather-web": "buoyweather_web",
    "buoyweather-ios": "buoyweather_ios",
    "buoyweather-android": "buoyweather_android",
    "fishtrack-web": "fishtrack_web",
    "fishtrack-ios": "fishtrack_ios",
    "fishtrack-android": "fishtrack_android",
    "braze": "braze",
    "vero": "vero",
    "customer-io-surfline": "customer_io_surfline",
    "customer-io-buoyweather": "customer_io_buoyweather",
    "customer-io-fishtrack": "customer_io_fishtrack",
    "split-production": "split_production",
}
