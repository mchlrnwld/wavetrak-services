"""Helper classes and functions to template dbt files"""

import re
import sys

import psycopg2
import yaml


class MyDumper(yaml.Dumper):
    """Custom dumper class. Applies basic formatting to YAML tracking plans."""

    def increase_indent(self, flow=False, indentless=False):
        return super(MyDumper, self).increase_indent(flow, False)

    # Don't use YAML anchors/aliases
    # https://ttl255.com/yaml-anchors-and-aliases-and-how-to-disable-them/
    def ignore_aliases(self, data):
        return True


def connect_to_redshift(db_name, port, user, password, host_url):
    """Establishes a connection with a Redhsift database.

    Args:
        db_name (string): The database name.
        port (string): The port.
        user (string): The user name credential.
        password (string): The user's password.
        host_url (string): The host URL of the database.

    Returns:
        object: A connection to the database.
    """
    conn_string = (
        f"dbname='{db_name}' "
        f"port='{port}' "
        f"user='{user}' "
        f"password='{password}' "
        f"host='{host_url}'"
    )
    try:
        con = psycopg2.connect(conn_string)
        return con

    except psycopg2.DatabaseError as e:
        print(e)
        sys.exit(1)


def get_columns_in_table(schema_name, table_name, con):
    """Gets the columns for a specified table in Redshift.

    Args:
        schema_name (string): The schema the table belongs to.
        table_name (string): The name of the table.
        con (object): The connection to the Redshift database.

    Returns:
        list: A list of the columns in the table.
    """
    try:
        cur = con.cursor()
        cur.execute(
            f"SELECT column_name FROM information_schema.columns WHERE "
            f"table_schema = '{schema_name}' AND table_name = '{table_name}'"
        )
        columns = cur.fetchall()
        con.close()
        columns_list = [item for t in columns for item in t]

        return columns_list

    except psycopg2.DatabaseError as e:
        print(e)
        sys.exit(1)


def snake_case(in_str):
    """Converts a camelCase string to snake_case.

    Args:
        in_str (string): The input string in camelCase format.

    Returns:
        string: A string in snake_case format.
    """
    temp = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", in_str)
    snake = re.sub("([a-z0-9])([A-Z])", r"\1_\2", temp).lower()
    return snake


def segment_case(in_str):
    """Converts a camelCase string to segment_case.

    Args:
        in_str (string): A string in camelCase format.

    Returns:
        string: A string in segment_case format.

    segment_case logic:
        - Insert underscore only where casing switches from lower to UPPER
        - Then, lower() entire string

    Example:
        NSLocalizedFailureReason --> nslocalized_failure_reason
    """
    temp = re.sub("([a-z])([A-Z]+)", r"\1_\2", in_str)
    segment = temp.lower()
    return segment


def camel_case(in_str):
    """Converts a snake_case string to camelCase.

    Args:
        in_str (string): A string in snake_case format.

    Returns:
        string: A string in camelCase format.
    """
    temp = in_str.split("_")
    camel = temp[0] + "".join(ele.title() for ele in temp[1:])
    return camel


def redshift_case(in_str):
    """Converts a Segment property, trait, etc. to the
    equivalent casing that is used as the column name in Redshift.

    Args:
        in_str ([string]): The Segment property, trait, etc.

    Returns:
        string: The string formatted as a Redshift column name.
    """
    redshift_case = segment_case(in_str).replace(".", "_")
    return redshift_case


def format_source(in_str):
    """Lazy formats a dbt source.yml file using string replacement.

    Args:
        in_str (string): A string with the source.yml file contents.

    Returns:
        string: A string with the *formatted* source.yml file contents.
    """
    out_str = (
        in_str.replace(
            "\n      - name:",
            "\n\n      - name:",  # Separate each table with a blank line
        )
        .replace(
            "tables:\n\n      - name:",
            "tables:\n      - name:",  # First table directly under 'tables:'  # noqa E501
        )
        .replace(
            "columns:\n\n          - name:",
            "columns:\n          - name:",  # First column directly under 'columns:'  # noqa E501
        )
        .replace(
            "\n    tables:",
            "\n\n    tables:",  # Add a blank line before 'tables:'
        )
    )

    return out_str


def format_schema(in_str):
    """Lazy formats a dbt schema.yml file using string replacement.

    Args:
        in_str (string): A string with the schema.yml file contents.

    Returns:
        string: A string with the *formatted* schema.yml file contents.
    """
    out_str = (
        in_str.replace(
            "\n  - name:",
            "\n\n  - name:",  # Separate each model with a blank line
        )
        .replace(
            "models:\n\n  - name:",
            "models:\n  - name:",  # First model directly under 'models:'
        )
        .replace(
            "columns:\n\n      - name:",
            "columns:\n      - name:",  # First column directly under 'columns:'  # noqa E501
        )
        .replace(
            "stg___BRAND_PLATFORM____push_notification_tapped",  # fix this edge case  # noqa E501
            "stg_surfline_ios__push_notification_tapped",
        )
    )

    return out_str
