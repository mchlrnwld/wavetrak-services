"""Parses Segment tracking plans and modifies/writes the following dbt files:
    - src_brand_platform.yml
        - Adds <event_name> table for the brand_platform Segment source for
          each tracked event.
    - schema.yml
        - Adds a *staged* `stg_brand_platform__event_name` model to the
          brand_platform's staging folder schema.yml (for docs and testing).
    - stg_brand_platform__event_name.sql
        - The dbt SQL that defines the `stg_brand_platform__event_name` model
"""

import json
import logging
import os
from logging.config import fileConfig
from os import listdir
from os.path import isfile, join

import yaml
from dotenv import load_dotenv

from dbt_default_columns import event_std_columns
from dbt_helpers import (
    MyDumper,
    connect_to_redshift,
    format_schema,
    format_source,
    get_columns_in_table,
    redshift_case,
)
from dbt_secrets import DB_HOST_URL, DB_NAME, DB_PASSWORD, DB_PORT, DB_USER
from dbt_source_mapping import (
    source_brand_platform_dict,
    source_redshift_schema_dict,
)


def main():
    """Entry point into script"""
    # Load .env file if it exists
    load_dotenv()

    # Determine if dev or prod, find tracking plans accordingly
    if os.getenv("ENVIRONMENT") == "dev":
        SURFLINE_SEGMENT_DIR = os.getenv("SURFLINE_SEGMENT_DIR")
        SURFLINE_SEGMENT_LOGGING_DIR = SURFLINE_SEGMENT_DIR
        tracking_plan_folder = os.path.join(
            SURFLINE_SEGMENT_DIR, "tracking_plans_yaml"
        )

        # Get list of tracking plan paths
        tracking_plans = [
            os.path.join(tracking_plan_folder, f)
            for f in listdir(tracking_plan_folder)
            if isfile(join(tracking_plan_folder, f)) and f.endswith(".yaml")
        ]

    else:
        HOME = os.getenv("HOME")
        SURFLINE_SEGMENT_DIR = os.getenv("SURFLINE_SEGMENT_DIR")
        SURFLINE_SEGMENT_LOGGING_DIR = os.getenv(
            "SURFLINE_SEGMENT_LOGGING_DIR"
        )

        # Get modified & new files from 'trilom/file-changes-action' GitHub Action  # noqa: E501
        modified_files_json = os.path.join(HOME, "files_modified.json")
        added_files_json = os.path.join(HOME, "files_added.json")

        with open(modified_files_json) as f:
            modified_files = json.load(f)

        with open(added_files_json) as f:
            added_files = json.load(f)

        # Generate a list of the modified tracking plans
        modified_plans = [
            os.path.join(SURFLINE_SEGMENT_DIR, f)
            for f in modified_files
            if "/tracking_plans_yaml/" in f and f.endswith(".yaml")
        ]
        # Generate a list of the added tracking plans
        added_plans = [
            os.path.join(SURFLINE_SEGMENT_DIR, f)
            for f in added_files
            if "/tracking_plans_yaml/" in f and f.endswith(".yaml")
        ]

        # Combine the new and modified tracking plan lists
        tracking_plans = modified_plans + added_plans

    # Exclude the example tracking plan
    tracking_plans = [
        tracking_plan
        for tracking_plan in tracking_plans
        if "example" not in tracking_plan and "embed" not in tracking_plan
    ]

    # Setup logging
    log_file_path = os.path.join(
        SURFLINE_SEGMENT_LOGGING_DIR, "logging-dbt-template.ini"
    )
    fileConfig(log_file_path)
    logger = logging.getLogger(__name__)

    # Set directory where dbt files will be generated
    dbt_codegen_folder = os.path.join(SURFLINE_SEGMENT_DIR, "dbt/codegen")

    # Iterate through each tracking plan
    for tracking_plan in tracking_plans:
        tracking_plan_name = tracking_plan.split("/")[-1].replace(".yaml", "")
        logger.info(
            f"Templating dbt files for:\n"
            f"    Tracking plan: {tracking_plan_name}"
        )

        # Set top-level template folder, read folder, write folder
        template_folder = os.path.join(SURFLINE_SEGMENT_DIR, "dbt/templates")
        sub_template_folder = os.path.join(template_folder, "event")
        write_folder = os.path.join(
            dbt_codegen_folder, tracking_plan_name.replace("-", "_")
        )

        # If the write folder does not exist (new tracking plan), create it
        if not os.path.exists(write_folder):
            os.makedirs(write_folder)

        # Set the template files to use
        template_source = os.path.join(template_folder, "template_source.yml")
        template_schema = os.path.join(template_folder, "template_schema.yml")
        template_table = os.path.join(
            sub_template_folder, "template_event_table.yml"
        )
        template_model = os.path.join(
            sub_template_folder, "template_event_model.yml"
        )
        template_column = os.path.join(
            sub_template_folder, "template_event_column.yml"
        )
        template_staging = os.path.join(
            sub_template_folder, "template_event_staging.sql"
        )

        # Set special variables.
        # We parse templates for strings like "__SCHEMA_NAME__" and
        # replace with corresponding special variable. Special variables are
        # determined from the tracking plan and event. This pattern/approach is
        # used throughout this script to inject values into the templates.
        __SCHEMA_NAME__ = source_redshift_schema_dict[tracking_plan_name]
        __BRAND_PLATFORM__ = source_brand_platform_dict[tracking_plan_name]
        __BRAND_HIPHEN_PLATFORM__ = tracking_plan_name
        source_yml_filename = "src_" + __BRAND_PLATFORM__ + ".yml"
        schema_yml_filename = "schema.yml"
        source_yml_path = os.path.join(write_folder, source_yml_filename)
        schema_yml_path = os.path.join(write_folder, schema_yml_filename)

        # If the source.yml file does not exist, create it
        if not os.path.exists(source_yml_path):
            # Initialize a source object using template
            with open(template_source, "r") as f:
                source = yaml.safe_load(f)

            # Inject __SCHEMA_NAME__, etc. into the source object
            source["sources"][0]["name"] = __SCHEMA_NAME__
            source["sources"][0]["description"] = (
                source["sources"][0]["description"]
                .replace("__BRAND-PLATFORM__", __BRAND_HIPHEN_PLATFORM__)
                .replace("__SCHEMA_NAME__", __SCHEMA_NAME__)
            )

        else:
            # src_brand_platform.yml file exists, so read it
            with open(source_yml_path, "r") as f:
                source = yaml.safe_load(f)

        # If the schema.yml file does not exist, create it
        if not os.path.exists(schema_yml_path):
            # Initialize a schema object using template
            with open(template_schema, "r") as f:
                schema = yaml.safe_load(f)

        else:
            # src_brand_platform.yml file exists, so read it
            with open(schema_yml_path, "r") as f:
                schema = yaml.safe_load(f)

        # Load the YAML tracking plan
        with open(tracking_plan, "r") as f:
            yaml_plan = yaml.safe_load(f)

        # Get list of events in tracking plan, excluding Page/Screen viewed
        events = []
        events_raw = yaml_plan["events"]
        for event in events_raw:
            if event["name"] not in ["Page Viewed", "Screen Viewed"]:
                events.append(event)
            else:
                pass

        # Iterate through events in tracking plan
        for event in events:
            # Get event atributes
            event_name = event["name"]
            event_description = event["description"]
            event_version = event["version"]
            properties = event["properties"]
            event_fires_on = event["labels"]["fires_on"]

            # For the event_name, get all versions of the event
            versions_event_name = [
                event for event in events if event["name"] == event_name
            ]

            # Get a list of the event version numbers
            versions = [
                event["version"]
                for event in versions_event_name
                if event["name"] == event_name
            ]

            # Only write dbt files for the latest version of the event
            if event_version == max(versions):
                logging.info(
                    f"Templating event...\n"
                    f"    Tracking plan: {tracking_plan_name}\n"
                    f"    Event name: {event_name}\n"
                    f"    Event version: {event_version}\n"
                    f"    Event fires on: {event_fires_on}\n"
                )

                # Define special variables (e.g., __EVENT_NAME__) for table
                # and model objects
                __EVENT_NAME__ = (
                    event_name.lower()
                    .replace(" ", "_")
                    .replace("-", "")
                    .replace("__", "_")
                    .replace("/", "_")
                    .replace("itunes", "i_tunes")  # Handle bad event names
                    .replace("subscriptionaction", "subscription_action")
                    .replace("inapp", "in_app")
                    .replace("latlon", "lat_lon")
                )
                __TABLE_NAME__ = __EVENT_NAME__
                __TABLE_DESCRIPTION__ = event_description
                __MODEL_NAME__ = (
                    "stg_" + __BRAND_PLATFORM__ + "__" + __EVENT_NAME__
                )
                __MODEL_DESCRIPTION__ = event_description
                __FIRES_ON__ = event_fires_on

                # Initalize a table and model objects using templates
                with open(template_table, "r") as f:
                    table = yaml.safe_load(f)

                with open(template_model, "r") as f:
                    model = yaml.safe_load(f)

                # Inject __SCHEMA_NAME__, etc. into table and model
                table["name"] = table["name"].replace(
                    "__TABLE_NAME__", __TABLE_NAME__
                )
                table["description"] = table["description"].replace(
                    "__TABLE_DESCRIPTION__", __TABLE_DESCRIPTION__
                )
                model["name"] = model["name"].replace(
                    "__MODEL_NAME__", __MODEL_NAME__
                )
                model["description"] = model["description"].replace(
                    "__MODEL_DESCRIPTION__", __MODEL_DESCRIPTION__
                )

                # Connect to redshift and get column names from source table
                con = connect_to_redshift(
                    DB_NAME, DB_PORT, DB_USER, DB_PASSWORD, DB_HOST_URL
                )
                redshift_columns = get_columns_in_table(
                    __SCHEMA_NAME__, __TABLE_NAME__, con
                )
                redshift_columns.sort()

                # Get list of standard columns used for templating events
                std_columns = list(event_std_columns.keys())
                std_columns_in_redshift = [
                    column
                    for column in std_columns
                    if column in redshift_columns
                ]

                # Get list of columns unique to the event being templated
                event_columns = list(properties.keys())
                event_columns_in_redshift = [
                    column
                    for column in event_columns
                    if redshift_case(column) in redshift_columns
                ]

                combined_columns = std_columns_in_redshift + [
                    col
                    for col in event_columns_in_redshift
                    if col not in std_columns_in_redshift
                ]

                sql_columns = []

                # EVENT TEMPLATING LOGIC
                for column in combined_columns:
                    # If a standard column (not event column)
                    if column in std_columns:
                        column_list_of_dicts = event_std_columns[column]
                        for column_dict in column_list_of_dicts:
                            if column_dict["source_name"] is not None:
                                # Initialize a table column object
                                with open(template_column, "r") as f:
                                    table_column = yaml.safe_load(f)

                                table_column["name"] = column_dict[
                                    "source_name"
                                ]
                                table_column["description"] = (
                                    column_dict["description"]
                                    .replace(
                                        "__SCHEMA_NAME__", __SCHEMA_NAME__
                                    )
                                    .replace("__TABLE_NAME__", __TABLE_NAME__)
                                )

                                table["columns"].append(table_column)

                            if column_dict["schema_name"] is not None:
                                # Initialize a model column object
                                with open(template_column, "r") as f:
                                    model_column = yaml.safe_load(f)

                                model_column["name"] = column_dict[
                                    "schema_name"
                                ]
                                model_column["description"] = (
                                    column_dict["description"]
                                    .replace(
                                        "__SCHEMA_NAME__", __SCHEMA_NAME__
                                    )
                                    .replace("__MODEL_NAME__", __MODEL_NAME__)
                                )

                                model["columns"].append(model_column)

                    elif column in event_columns:
                        # Column must be in the source table in Redshift, not
                        # just the tracking plan
                        if redshift_case(column) in redshift_columns:
                            if column == "id":
                                column = (
                                    "_id"
                                )  # id is reserved column name from Segment

                            # Append column to SQL columns for use in dbt SQL
                            sql_columns.append(redshift_case(column))

                            # Initialize table_column object
                            with open(template_column, "r") as f:
                                table_column = yaml.safe_load(f)

                            table_column["name"] = redshift_case(column)
                            table_column["description"] = properties[column][
                                "description"
                            ]

                            table["columns"].append(table_column)

                            # Initialize model_column object
                            with open(template_column, "r") as f:
                                model_column = yaml.safe_load(f)

                            model_column["name"] = redshift_case(column)
                            model_column["description"] = properties[column][
                                "description"
                            ]

                            model["columns"].append(model_column)

                    else:
                        pass

                if (
                    any(["context_device_model", "context_user_agent"])
                    in combined_columns
                ):
                    # Initialize a model_column object
                    with open(template_column, "r") as f:
                        model_column = yaml.safe_load(f)

                    # Add name and description to model_column
                    model_column["name"] = "device_category"
                    model_column["description"] = "The device category."

                    # Append model_column to model object
                    model["columns"].append(model_column)

                # Append table to source object
                source["sources"][0]["tables"].append(table)

                # Append model to schema object
                schema["models"].append(model)

                event_properties_str = ""

                for column in sql_columns:
                    event_properties_str += "'" + column + "', "

                __EVENT_PROPERTIES__ = "[ " + event_properties_str + " ]"

                # Initialize a staging object using the template
                with open(template_staging, "r") as f:
                    staging = f.read()

                # Replace "__SCHEMA_NAME__", "__TABLE_NAME__", and
                # "__IGNORE_COLUMNS__" strings in SQL template with variables
                staging = (
                    staging.replace("__SCHEMA_NAME__", __SCHEMA_NAME__)
                    .replace("__TABLE_NAME__", __TABLE_NAME__)
                    .replace("__EVENT_PROPERTIES__", __EVENT_PROPERTIES__)
                    .replace("__FIRES_ON__", __FIRES_ON__)
                )

                # Write staging object to file
                with open(
                    os.path.join(write_folder, __MODEL_NAME__ + ".sql"), "w"
                ) as f:
                    f.write(staging)

        # Dump schema object to schema.yml
        with open(os.path.join(write_folder, "schema.yml"), "w") as f:
            yaml.dump(
                schema,
                f,
                indent=2,
                width=70,
                Dumper=MyDumper,
                sort_keys=False,
                default_flow_style=False,
                allow_unicode=True,
                encoding=("utf-8"),
            )

        # Format schema.yml for readability
        with open(os.path.join(write_folder, "schema.yml"), "r") as f:
            schema_string = f.read()

        # Write formatted schema.yml back to the file
        with open(os.path.join(write_folder, "schema.yml"), "w") as f:
            f.write(format_schema(schema_string))

        # Dump source object to src_brand_platform.yml
        src_file = "src_" + __BRAND_PLATFORM__ + ".yml"
        with open(os.path.join(write_folder, src_file), "w") as f:
            yaml.dump(
                source,
                f,
                indent=2,
                width=70,
                Dumper=MyDumper,
                sort_keys=False,
                default_flow_style=False,
                allow_unicode=True,
                encoding=("utf-8"),
            )

        # Format src_brand_platform.yml for readability
        with open(os.path.join(write_folder, src_file), "r") as f:
            source_string = f.read()

        # Write formatted src_brand_platform.yml back to the file
        with open(os.path.join(write_folder, src_file), "w") as f:
            f.write(format_source(source_string))

    else:
        pass


if __name__ == "__main__":
    main()
