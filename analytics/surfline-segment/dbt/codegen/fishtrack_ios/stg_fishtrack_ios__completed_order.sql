-- dbt SQL below was generated using the `dbt_codegen_events.py` script
{{
  config(
    materialized = 'incremental',
    unique_key = 'event_id',
    sort = 'tstamp',
    dist = 'event_id'
    )
}}

{#- Get source table object #}
{%- set source_relation = adapter.get_relation(
      database = var('segment_database'),
      schema = 'fishtrack_ios',
      identifier = 'completed_order'
    )
-%}
{%- set schema_name = source_relation.schema -%}
{#- Get all columns from source table #}
{%- set source_columns_raw = adapter.get_columns_in_relation(source('fishtrack_ios', 'completed_order')) -%}

{#- Build a list of strings containing the source column names #}
{%- set source_column_names = [] -%}
{%- for column in source_columns_raw -%}
{{ source_column_names.append(column.name) or '' }}
{%- endfor -%}

{#- Set list of columns that we hard code. As a result, we do not include these
columns in the '-- Segment event properties' section in the 2nd CTE. #}
{%- set event_properties = [ 'quantity', 'interval_count', 'product_name', 'currency', 'price', 'interval', 'order_id', 'sku', 'payment_platform', 'category',  ] %}

{# Reserved Segment event properties.
If any of these are found in `event_properties` above, omit them from being
templated dynamically. Hard code them instead.
#}
{%- set reserved_segment_event_cols = ["user_id"] %}

{# Set of reserved SQL keywords that need to be double-quoted if used as column #}
{%- set double_quoted_cols = ["name", "interval", "location", "time", "group"] %}

{#- Where does event fire? Determines what columns are in the compiled SQL #}
{%- set fires_on = 'client' %}

with source as (

    select *

    from {{ source('fishtrack_ios', 'completed_order') }}
    {%- if is_incremental() %}
    where received_at >= (
            select
                dateadd(
                    hours,
                    -3,
                    max(received_at_tstamp)
                )

            from {{ this }}
        )
        and received_at < current_date
    {%- else %}
    {%- if target.name == 'dev' %}
    where received_at > {{ var('dev_start_date') }}
        and received_at < current_date
    {%- else %}
    where received_at >= {{ var('prod_start_date') }}
        and received_at < current_date
    {%- endif %}
    {%- endif %}

),

renamed as (

    select
        id as event_id
        , 'fishtrack_ios'::varchar as source_schema
        , "event" as source_table
        , event_text as event_name
        , context_library_name as library_name
        , context_library_version as library_version

        -- Timestamp properties
        , "timestamp" as tstamp
        , received_at as received_at_tstamp
        , sent_at as sent_at_tstamp

        -- User properties
        {%- if 'anonymous_id' in source_column_names and 'user_id' in source_column_names %} {#- Client-side events #}
        , anonymous_id
        , user_id
        {%- elif 'user_id' in source_column_names and 'anonymous_id' not in source_column_names %} {#- Server-side events #}
        , user_id
        {%- else %} {#- All track events (except server-side) should at least have an anonymous_id #}
        , anonymous_id
        {%- endif %}

    {#- ------ INTEGRATIONS (e.g., Braze, Vero, Customer.io, etc.) ------ #}
    {%- if fires_on in ['braze', 'customer-io', 'vero'] %}
        -- Integration properties
        {%- for column in [
            'context_integrations_name',
            'context_integrations_version',
            'context_device_model',
            'context_device_type',
            ]
        %}
        {%- if column in source_column_names %}
        , {{ column }} as {{ column|replace('context_', '')|replace('integrations_', 'integration_')|replace('device_model', 'device') }}
        {%- endif %}
        {%- endfor %}

    {%- endif %}

    {#- ------ WEB ------ #}
    {%- if fires_on == 'client' and 'ios' not in schema_name and 'android' not in schema_name %}

        -- Device properties
        {%- for column in [
                'context_ip',
                'context_locale',
                'context_user_agent'
            ]
        %}
        {%- if column in source_column_names %}
        {%- if column == 'context_user_agent' and 'context_device_model' not in source_column_names %}
        -- `context_device_model` filter avoids android events from being modeled like web events (android events have a `context_user_agent` too)
        , context_user_agent as user_agent
        , cast(
            case
                when lower(context_user_agent) like '%android%'
                    then 'Android'
                else replace(
                    {{ dbt_utils.split_part(dbt_utils.split_part('context_user_agent', "'('", 2), "' '", 1) }},
                    ';',
                    ''
                    )
            end
            as varchar
        ) as device
        {%- else %}
        , {{ column }} as {{ column|replace('context_', '') }}
        {%- endif %}
        {%- endif %}
        {%- endfor %}

        -- Page properties
        {%- if 'context_page_search' in source_column_names %}
        , case  -- CASE statement solves issue where the url column doesn't include query string parameters.
            when context_page_url ilike '%?%'
                then context_page_url
            else concat(context_page_url, coalesce(context_page_search, ''))
        end as page_url
        , {{ dbt_utils.get_url_host('context_page_url') }} as page_url_host
        , context_page_path as page_url_path
        , context_page_title as page_title
        , context_page_search as page_url_query
        {%- else %}
        {%- if 'context_page_url' in source_column_names %}
        , context_page_url as page_url
        , {{ dbt_utils.get_url_host('context_page_url') }} as page_url_host
        , context_page_path as page_url_path
        , context_page_title as page_title
        , null as page_url_query
        {%- endif %}
        {%- endif%}
        {%- if 'context_page_referrer' in source_column_names %}
        , context_page_referrer as referrer
        , cast(
            replace( {{ dbt_utils.get_url_host('context_page_referrer') }}, 'www.', '')
            as varchar
        ) as referrer_host
        {%- endif %}

        -- UTM properties
        {%- for column_name in source_column_names %}
        {%- if column_name.startswith('context_campaign_') %}
        , {{ column_name }} as {{ column_name|replace('context_campaign_', 'utm_') }}
        {%- endif %}
        {%- endfor %}

    {%- endif %}

    {#- ------ MOBILE (iOS & Android) ------ #}
    {% if fires_on == 'client' and ('ios' in schema_name or 'android' in schema_name) %}

        -- App properties
        {%- for column in [
                'context_app_name',
                'context_app_version',
                'context_app_build'
            ]
        %}
        {%- if column in source_column_names %}
        , {{ column }} as {{ column|replace('context_', '') }}
        {%- endif %}
        {%- endfor %}

        -- Operating system (OS) properties
        {%- for column in ['context_os_name', 'context_os_version'] %}
        {%- if column in source_column_names %}
        , {{ column }} as {{ column|replace('context_', 'device_') }}
        {%- endif %}
        {%- endfor %}

        -- Device properties
        {%- for column in [
                'context_device_id',
                'context_device_manufacturer',
                'context_device_type',
                'context_device_model',
                'context_timezone',
                'context_screen_height',
                'context_screen_width',
                'context_screen_density'
            ]
        %}
        {%- if column in source_column_names %}
        {%- if column == 'context_device_model' %}
        , regexp_substr(context_device_model, '[a-zA-Z]+') as device
        , REGEXP_replace(
            REGEXP_replace(context_device_model, '[a-zA-Z]', ''),
            ',',
            '.'
        ) as device_version
        {%- elif column == 'context_timezone' %}
        , context_timezone as device_timezone
        {%- else %}
        , {{ column }} as {{ column|replace('context_', '') }}
        {%- endif %}
        {%- endif %}
        {%- endfor %}

        -- Network properties
        {%- set network_columns = [
            'context_network_carrier',
            'context_network_cellular',
            'context_network_wifi',
            'context_network_bluetooth',
            'context_ip',
            'context_locale'
            ]
        %}
        {%- for column in network_columns|sort %}
        {%- if column in source_column_names %}
        , {{ column }} as {{ column|replace('context_', '') }}
        {%- endif %}
        {%- endfor %}

        -- Ad tracking properties
        {%- for protocols_column in [
                'context_device_ad_tracking_enabled',
                'context_device_advertising_id'
            ]
        %}
        {%- if protocols_column in source_column_names %}
        , {{ protocols_column }} as {{ protocols_column|replace('context_device_', '') }}
        {%- endif %}
        {%- endfor %}

    {%- endif %}

    {#- ------ SEGMENT COLUMNS ------  #}
        -- Event Properties from Segment tracking plan
        {%- for event_property in event_properties|sort %}
        {%- if event_property not in reserved_segment_event_cols %}
        {%- if event_property in double_quoted_cols %}
        {%- autoescape false %}
        , "{{ event_property }}"
        {%- endautoescape %}
        {%- else %}
        , {{ event_property }}
        {%- endif %}
        {%- endif %}
        {%- endfor %}

        {#- ------ Protocols ------  #}
        {%- for protocols_column in [
                'context_protocols_source_id',
                'context_protocols_violations',
                'context_protocols_omitted'
            ]
        %}
        {%- if protocols_column in source_column_names %}
        -- Segment protocols properties
        , {{ protocols_column }} as {{ protocols_column|replace('context_protocols_', 'segment_protocols_') }}
        {%- endif %}
        {%- endfor %}

    from source

),

staged as (

    select
        *

        {%- if fires_on == 'client' and 'context_page_url' in source_column_names %}
        -- Is the event occuring in a native web view? See this Slack thread:
        -- https://surfline.slack.com/archives/CSGTPLHN0/p1612561651045000
        , case
            when {{ dbt_utils.get_url_parameter('page_url', 'native') }} = 'true'
                then true
            when {{ dbt_utils.get_url_parameter('page_url', 'ios') }} = 'true'
                then true
            when {{ dbt_utils.get_url_parameter('page_url', 'native') }} = 'ios'
                then true
            else false
        end as is_native_web_view
        {%- endif %}

        {%- if fires_on == 'client' and 'context_user_agent' in source_column_names or 'context_device_model' in source_column_names %}
        , cast(
            case
                when device = 'iPhone' then 'iPhone'
                when device = 'Android' then 'Android'
                when device in ('iPad', 'iPod') then 'Tablet'
                when device in ('Windows', 'Macintosh', 'X11') then 'Desktop'
                else 'Uncategorized'
            end
            as varchar
        ) as device_category
        {%- else %}

        {%- endif %}

    from renamed

)

select * from staged
