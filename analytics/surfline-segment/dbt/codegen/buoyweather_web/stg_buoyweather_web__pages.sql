-- dbt SQL below was generated using the `dbt_codegen_pages.py` script
{{
  config(
    materialized = 'incremental',
    unique_key = 'page_id',
    sort = 'tstamp',
    dist = 'page_id'
    )
}}

{#- Get source table object #}
{%- set source_relation = adapter.get_relation(
      database = var('segment_database'),
      schema = 'buoyweather',
      identifier = 'pages'
    )
-%}

{# Set a list of page properties (from tracking plan) to append to the model #}
{%- set page_properties = [] %}

{# Reserved Segment page properties.
If any of these are found in `page_properties` above, omit them from being
templated dynamically. Hard code them instead.
#}
{%- set reserved_segment_page_cols = ["name", "path", "referrer", "search", "title", "url"] %}

{# Columns the must be double-quoted keywords that need to be double-quoted if used as column #}
{%- set double_quoted_cols = ["interval", "location", "time", "group"] %}

with source as (

    select *

    from {{ source('buoyweather', 'pages') }}
    {%- if is_incremental() %}
    where received_at >= (
            select
                dateadd(
                    hours,
                    -3,
                    max(received_at_tstamp)
                )

            from {{ this }}
        )
        and received_at < current_date
    {%- else %}
    {%- if target.name == 'dev' %}
    where received_at > {{ var('dev_start_date') }}
        and received_at < current_date
    {%- else %}
    where received_at >= {{ var('prod_start_date') }}
        and received_at < current_date
    {%- endif %}
    {%- endif %}

),

source_xf as (

    select
        {{
            dbt_utils.star(
                source('buoyweather', 'pages'),
                except = ['context_page_url']
            )
        }},

        {#
        This CASE statement solves an issue where the `url` column does not
        include the query string parameters. When the query string parameters
        are not part of the URL, the dbt_utils.get_url_parameter() macro will
        not find the specified parameter (e.g., gclid, fbaid, cid, etc.).
        #}
        case
            when context_page_url ilike '%?%'
                then context_page_url
            else concat(context_page_url, coalesce(context_page_search, ''))
        end as context_page_url

    from source

),

renamed as (

    select
        id as page_id,
        'buoyweather'::varchar as source_schema,
        'pages'::varchar as source_table,
        'page'::varchar as call_type,
        "name" as page_name,
        context_library_name as library_name,

        -- Timestamp properties
        "timestamp" as tstamp,
        received_at as received_at_tstamp,
        sent_at as sent_at_tstamp,

        -- User properties
        anonymous_id,
        user_id,

        -- URL properties
        context_page_url as page_url,
        {{ dbt_utils.get_url_host('context_page_url') }} as page_url_host,
        context_page_path as page_url_path,
        context_page_title as page_title,
        context_page_search as page_url_query,

        -- UTM properties
        context_campaign_source::varchar as utm_source,
        context_campaign_medium::varchar as utm_medium,
        context_campaign_name::varchar as utm_campaign,
        context_campaign_term::varchar as utm_term,
        context_campaign_content::varchar as utm_content,

        -- Off-site campaign tracking properties
        cast( {{ dbt_utils.get_url_parameter('context_page_url', 'gclid') }} as varchar) as gclid,
        cast( {{ dbt_utils.get_url_parameter('context_page_url', 'fbaid') }} as varchar) as fbaid,
        cast(
            case
                when context_campaign_source ilike 'customer.io'
                    then {{ dbt_utils.get_url_parameter('context_page_url', 'cid') }}
            end
            as varchar
         ) as cid,

        -- Referral tracking properties
        context_page_referrer as referrer,
        cast(
            replace( {{ dbt_utils.get_url_host('context_page_referrer') }}, 'www.', '')
            as varchar
        ) as referrer_host,

        -- Device properties
        context_ip as ip,
        context_user_agent as user_agent,
        cast(
            case
                when lower(context_user_agent) like '%android%'
                    then 'Android'
                else replace(
                    {{ dbt_utils.split_part(dbt_utils.split_part('context_user_agent', "'('", 2), "' '", 1) }},
                    ';',
                    ''
                    )
            end
            as varchar
        ) as device,

        case
            -- See for context: https://surfline.slack.com/archives/CSGTPLHN0/p1612561651045000
            when {{ dbt_utils.get_url_parameter('context_page_url', 'native') }} = 'true'
                then true
            when {{ dbt_utils.get_url_parameter('context_page_url', 'ios') }} = 'true'
                then true
            when {{ dbt_utils.get_url_parameter('context_page_url', 'native') }} = 'ios'
                then true
            else false
        end as is_native_web_view

        -- Page properties from Segment tracking plan
        {%- for page_property in page_properties|sort %}
        {%- if page_property not in reserved_segment_page_cols %}
        {%- if page_property in double_quoted_cols %}
        {%- autoescape false %}
        , "{{ page_property }}"
        {%- endautoescape %}
        {%- else %}
        , {{ page_property }}
        {%- endif %}
        {%- endif %}
        {%- endfor %}

    from source_xf

),

categorized as (

    select
        *,

        cast(
            case
                when device = 'iPhone' then 'iPhone'
                when device = 'Android' then 'Android'
                when device in ('iPad', 'iPod') then 'Tablet'
                when device in ('Windows', 'Macintosh', 'X11') then 'Desktop'
                else 'Uncategorized'
            end
            as varchar
        ) as device_category

    from renamed

)

select * from categorized
