version: 2
sources:
  - name: vero
    description: Schema in our data warehouse for the vero Segment source.
    freshness:
      warn_after:
        count: 12
        period: hour
      error_after:
        count: 24
        period: hour
    loaded_at_field: received_at

    tables:
      - name: email_opened
        description: User opened an email from Vero.
        columns:
          - name: id
            description: Primary key.
          - name: event
            description: The name of source table.
          - name: event_text
            description: The event name.
          - name: original_timestamp
            description: Time on the client device when call was invoked,
              OR the timestamp value manually passed in through server-side
              libraries.
          - name: sent_at
            description: Time on client device when call was sent, OR sent_at
              value manually passed in. NOTE - sent_at is NOT USEFUL for
              analysis since it’s not always trustworthy as it can be easily
              adjusted and affected by clock skew.
          - name: received_at
            description: Time on Segment server clock when call was received.
              received_at is used as sort key in Warehouses. For max query
              speed, received_at is the recommended timestamp for analysis
              when chronology DOES NOT matter as chronology is NOT ENSURED.
          - name: timestamp
            description: When the event occurred. Calculated by Segment
              to correct client-device clock skew using the following formula;
              timestamp = received_at - (sent_at - originalTimestamp). Use
              timestamp for analysis when chronology DOES matter.
          - name: user_id
            description: Unique identifier for the user in our database.
              A user_id or an anonymous_id is required for all events.
          - name: context_library_name
            description: The name of the library that generated the event.
          - name: context_library_version
            description: The version of the library that generated the event.
          - name: context_ip
            description: The IP address the event was generated from.
          - name: context_user_agent
            description: The user agent string of the device generating
              the event.
          - name: context_integration_name
            description: The name of the integration that generated the
              event.
          - name: context_integration_version
            description: The version of the integration that generated the
              event.
          - name: context_traits_email
            description: The email address of the user.
          - name: context_message_id
            description: The message ID of the event. Used by Segment for
              de-duplication checks.
          - name: context_event_triggered_at
            description: The time the event was triggered in Vero.
          - name: context_event_name
            description: The name of the event in Vero.
          - name: context_event_data_update_payment_details_url
            description: The URL of the payment details update page.
          - name: context_event_data_payment_attempt
            description: The payment attempt number.
          - name: context_protocols_source_id
            description: If a protocol violation is detected for this instance
              of the event firing, the ID of the Segment source.
          - name: context_protocols_violations
            description: If a protocol violation is detected for this instance
              of the event firing, a list of the protocol violations.
          - name: context_traits_email
            description: The email address of the recipient.
          - name: campaign_group
            description: The campaign group that the email belongs to.
          - name: campaign_id
            description: The campaign ID that the email belongs to.
          - name: campaign_name
            description: The campaign name that the email belongs to.
          - name: campaign_tags
            description: The campaign tags.
          - name: channel
            description: The marketing channel.
          - name: email_id
            description: The email ID.
          - name: email_permalink
            description: The email permalink. Points directly to a log of
              the email that the user received.
          - name: email_subject
            description: The email subject.
          - name: email_type
            description: The email type.
          - name: opened_at
            description: The time (in seconds since the epoch) that the
              email was opened.
          - name: variation_display_name
            description: The display name of the email variation (used when
              A/B testing email variations).
          - name: variation_id
            description: The ID of the email variation (used when A/B testing
              email variations).

      - name: email_sent
        description: Email sent to user by Vero.
        columns:
          - name: id
            description: Primary key.
          - name: event
            description: The name of source table.
          - name: event_text
            description: The event name.
          - name: original_timestamp
            description: Time on the client device when call was invoked,
              OR the timestamp value manually passed in through server-side
              libraries.
          - name: sent_at
            description: Time on client device when call was sent, OR sent_at
              value manually passed in. NOTE - sent_at is NOT USEFUL for
              analysis since it’s not always trustworthy as it can be easily
              adjusted and affected by clock skew.
          - name: received_at
            description: Time on Segment server clock when call was received.
              received_at is used as sort key in Warehouses. For max query
              speed, received_at is the recommended timestamp for analysis
              when chronology DOES NOT matter as chronology is NOT ENSURED.
          - name: timestamp
            description: When the event occurred. Calculated by Segment
              to correct client-device clock skew using the following formula;
              timestamp = received_at - (sent_at - originalTimestamp). Use
              timestamp for analysis when chronology DOES matter.
          - name: user_id
            description: Unique identifier for the user in our database.
              A user_id or an anonymous_id is required for all events.
          - name: context_library_name
            description: The name of the library that generated the event.
          - name: context_library_version
            description: The version of the library that generated the event.
          - name: context_integration_name
            description: The name of the integration that generated the
              event.
          - name: context_integration_version
            description: The version of the integration that generated the
              event.
          - name: context_traits_email
            description: The email address of the user.
          - name: context_message_id
            description: The message ID of the event. Used by Segment for
              de-duplication checks.
          - name: context_event_triggered_at
            description: The time the event was triggered in Vero.
          - name: context_event_name
            description: The name of the event in Vero.
          - name: context_event_data_update_payment_details_url
            description: The URL of the payment details update page.
          - name: context_event_data_timestamp
            description: The timestamp of the event in Vero.
          - name: context_event_data_reset_url
            description: The URL of the password reset page.
          - name: context_event_data_payment_attempt
            description: The payment attempt number.
          - name: context_event_data_expiry_hours
            description: The number of hours until the update payments URL
              expires.
          - name: context_protocols_source_id
            description: If a protocol violation is detected for this instance
              of the event firing, the ID of the Segment source.
          - name: context_protocols_violations
            description: If a protocol violation is detected for this instance
              of the event firing, a list of the protocol violations.
          - name: context_traits_email
            description: The email address of the recipient.
          - name: campaign_group
            description: The campaign group that the email belongs to.
          - name: campaign_id
            description: The campaign ID that the email belongs to.
          - name: campaign_name
            description: The campaign name that the email belongs to.
          - name: campaign_tags
            description: The campaign tags.
          - name: channel
            description: The marketing channel.
          - name: email_id
            description: The email ID.
          - name: email_permalink
            description: The email permalink. Points directly to a log of
              the email that the user received.
          - name: email_subject
            description: The email subject.
          - name: email_type
            description: The email type.
          - name: variation_display_name
            description: The display name of the email variation (used when
              A/B testing email variations).
          - name: variation_id
            description: The ID of the email variation (used when A/B testing
              email variations).

      - name: email_delivered
        description: Email delivered to user by Vero.
        columns:
          - name: id
            description: Primary key.
          - name: event
            description: The name of source table.
          - name: event_text
            description: The event name.
          - name: original_timestamp
            description: Time on the client device when call was invoked,
              OR the timestamp value manually passed in through server-side
              libraries.
          - name: sent_at
            description: Time on client device when call was sent, OR sent_at
              value manually passed in. NOTE - sent_at is NOT USEFUL for
              analysis since it’s not always trustworthy as it can be easily
              adjusted and affected by clock skew.
          - name: received_at
            description: Time on Segment server clock when call was received.
              received_at is used as sort key in Warehouses. For max query
              speed, received_at is the recommended timestamp for analysis
              when chronology DOES NOT matter as chronology is NOT ENSURED.
          - name: timestamp
            description: When the event occurred. Calculated by Segment
              to correct client-device clock skew using the following formula;
              timestamp = received_at - (sent_at - originalTimestamp). Use
              timestamp for analysis when chronology DOES matter.
          - name: user_id
            description: Unique identifier for the user in our database.
              A user_id or an anonymous_id is required for all events.
          - name: context_library_name
            description: The name of the library that generated the event.
          - name: context_library_version
            description: The version of the library that generated the event.
          - name: context_integration_name
            description: The name of the integration that generated the
              event.
          - name: context_integration_version
            description: The version of the integration that generated the
              event.
          - name: context_traits_email
            description: The email address of the user.
          - name: context_message_id
            description: The message ID of the event. Used by Segment for
              de-duplication checks.
          - name: context_event_triggered_at
            description: The time the event was triggered in Vero.
          - name: context_event_name
            description: The name of the event in Vero.
          - name: context_event_data_update_payment_details_url
            description: The URL of the payment details update page.
          - name: context_event_data_timestamp
            description: The timestamp of the event in Vero.
          - name: context_event_data_reset_url
            description: The URL of the password reset page.
          - name: context_event_data_payment_attempt
            description: The payment attempt number.
          - name: context_event_data_expiry_hours
            description: The number of hours until the update payments URL
              expires.
          - name: context_protocols_source_id
            description: If a protocol violation is detected for this instance
              of the event firing, the ID of the Segment source.
          - name: context_protocols_violations
            description: If a protocol violation is detected for this instance
              of the event firing, a list of the protocol violations.
          - name: context_traits_email
            description: The email address of the recipient.
          - name: campaign_group
            description: The campaign group that the email belongs to.
          - name: campaign_id
            description: The campaign ID that the email belongs to.
          - name: campaign_name
            description: The campaign name that the email belongs to.
          - name: campaign_tags
            description: The campaign tags.
          - name: channel
            description: The marketing channel.
          - name: email_id
            description: The email ID.
          - name: email_permalink
            description: The email permalink. Points directly to a log of
              the email that the user received.
          - name: email_subject
            description: The email subject.
          - name: email_type
            description: The email type.
          - name: variation_display_name
            description: The display name of the email variation (used when
              A/B testing email variations).
          - name: variation_id
            description: The ID of the email variation (used when A/B testing
              email variations).

      - name: email_bounced
        description: When an Email sent by Vero has not been delivered to
          the ISP server.
        columns:
          - name: id
            description: Primary key.
          - name: event
            description: The name of source table.
          - name: event_text
            description: The event name.
          - name: original_timestamp
            description: Time on the client device when call was invoked,
              OR the timestamp value manually passed in through server-side
              libraries.
          - name: sent_at
            description: Time on client device when call was sent, OR sent_at
              value manually passed in. NOTE - sent_at is NOT USEFUL for
              analysis since it’s not always trustworthy as it can be easily
              adjusted and affected by clock skew.
          - name: received_at
            description: Time on Segment server clock when call was received.
              received_at is used as sort key in Warehouses. For max query
              speed, received_at is the recommended timestamp for analysis
              when chronology DOES NOT matter as chronology is NOT ENSURED.
          - name: timestamp
            description: When the event occurred. Calculated by Segment
              to correct client-device clock skew using the following formula;
              timestamp = received_at - (sent_at - originalTimestamp). Use
              timestamp for analysis when chronology DOES matter.
          - name: user_id
            description: Unique identifier for the user in our database.
              A user_id or an anonymous_id is required for all events.
          - name: context_library_name
            description: The name of the library that generated the event.
          - name: context_library_version
            description: The version of the library that generated the event.
          - name: context_integration_name
            description: The name of the integration that generated the
              event.
          - name: context_integration_version
            description: The version of the integration that generated the
              event.
          - name: context_traits_email
            description: The email address of the user.
          - name: context_message_id
            description: The message ID of the event. Used by Segment for
              de-duplication checks.
          - name: context_event_triggered_at
            description: The time the event was triggered in Vero.
          - name: context_event_name
            description: The name of the event in Vero.
          - name: context_event_data_update_payment_details_url
            description: The URL of the payment details update page.
          - name: context_event_data_timestamp
            description: The timestamp of the event in Vero.
          - name: context_event_data_reset_url
            description: The URL of the password reset page.
          - name: context_event_data_payment_attempt
            description: The payment attempt number.
          - name: context_event_data_expiry_hours
            description: The number of hours until the update payments URL
              expires.
          - name: context_protocols_source_id
            description: If a protocol violation is detected for this instance
              of the event firing, the ID of the Segment source.
          - name: context_protocols_violations
            description: If a protocol violation is detected for this instance
              of the event firing, a list of the protocol violations.
          - name: context_traits_email
            description: The email address of the recipient.
          - name: campaign_group
            description: The campaign group that the email belongs to.
          - name: campaign_id
            description: The campaign ID that the email belongs to.
          - name: campaign_name
            description: The campaign name that the email belongs to.
          - name: campaign_tags
            description: The campaign tags.
          - name: channel
            description: The marketing channel.
          - name: email_id
            description: The email ID.
          - name: email_permalink
            description: The email permalink. Points directly to a log of
              the email that the user received.
          - name: email_subject
            description: The email subject.
          - name: email_type
            description: The email type.
          - name: variation_display_name
            description: The display name of the email variation (used when
              A/B testing email variations).
          - name: variation_id
            description: The ID of the email variation (used when A/B testing
              email variations).

      - name: email_link_clicked
        description: When a user clicks a link in an email sent by Vero.
        columns:
          - name: id
            description: Primary key.
          - name: event
            description: The name of source table.
          - name: event_text
            description: The event name.
          - name: original_timestamp
            description: Time on the client device when call was invoked,
              OR the timestamp value manually passed in through server-side
              libraries.
          - name: sent_at
            description: Time on client device when call was sent, OR sent_at
              value manually passed in. NOTE - sent_at is NOT USEFUL for
              analysis since it’s not always trustworthy as it can be easily
              adjusted and affected by clock skew.
          - name: received_at
            description: Time on Segment server clock when call was received.
              received_at is used as sort key in Warehouses. For max query
              speed, received_at is the recommended timestamp for analysis
              when chronology DOES NOT matter as chronology is NOT ENSURED.
          - name: timestamp
            description: When the event occurred. Calculated by Segment
              to correct client-device clock skew using the following formula;
              timestamp = received_at - (sent_at - originalTimestamp). Use
              timestamp for analysis when chronology DOES matter.
          - name: user_id
            description: Unique identifier for the user in our database.
              A user_id or an anonymous_id is required for all events.
          - name: context_library_name
            description: The name of the library that generated the event.
          - name: context_library_version
            description: The version of the library that generated the event.
          - name: context_ip
            description: The IP address the event was generated from.
          - name: context_user_agent
            description: The user agent string of the device generating
              the event.
          - name: context_integration_name
            description: The name of the integration that generated the
              event.
          - name: context_integration_version
            description: The version of the integration that generated the
              event.
          - name: context_traits_email
            description: The email address of the user.
          - name: context_message_id
            description: The message ID of the event. Used by Segment for
              de-duplication checks.
          - name: context_event_triggered_at
            description: The time the event was triggered in Vero.
          - name: context_event_name
            description: The name of the event in Vero.
          - name: context_event_data_update_payment_details_url
            description: The URL of the payment details update page.
          - name: context_event_data_payment_attempt
            description: The payment attempt number.
          - name: context_protocols_source_id
            description: If a protocol violation is detected for this instance
              of the event firing, the ID of the Segment source.
          - name: context_protocols_violations
            description: If a protocol violation is detected for this instance
              of the event firing, a list of the protocol violations.
          - name: context_traits_email
            description: The email address of the recipient.
          - name: campaign_group
            description: The campaign group that the email belongs to.
          - name: campaign_id
            description: The campaign ID that the email belongs to.
          - name: campaign_name
            description: The campaign name that the email belongs to.
          - name: campaign_tags
            description: The campaign tags.
          - name: channel
            description: The marketing channel.
          - name: email_id
            description: The email ID.
          - name: email_permalink
            description: The email permalink. Points directly to a log of
              the email that the user received.
          - name: email_subject
            description: The email subject.
          - name: email_type
            description: The email type.
          - name: variation_display_name
            description: The display name of the email variation (used when
              A/B testing email variations).
          - name: variation_id
            description: The ID of the email variation (used when A/B testing
              email variations).
          - name: link_url
            description: The URL of the link that was clicked in the email.
          - name: clicked_at
            description: The time (in seconds since the epoch) that the
              link was clicked.

      - name: unsubscribed
        description: When a user unsubscribes from marketing emails sent
          by Vero.
        columns:
          - name: id
            description: Primary key.
          - name: event
            description: The name of source table.
          - name: event_text
            description: The event name.
          - name: original_timestamp
            description: Time on the client device when call was invoked,
              OR the timestamp value manually passed in through server-side
              libraries.
          - name: sent_at
            description: Time on client device when call was sent, OR sent_at
              value manually passed in. NOTE - sent_at is NOT USEFUL for
              analysis since it’s not always trustworthy as it can be easily
              adjusted and affected by clock skew.
          - name: received_at
            description: Time on Segment server clock when call was received.
              received_at is used as sort key in Warehouses. For max query
              speed, received_at is the recommended timestamp for analysis
              when chronology DOES NOT matter as chronology is NOT ENSURED.
          - name: timestamp
            description: When the event occurred. Calculated by Segment
              to correct client-device clock skew using the following formula;
              timestamp = received_at - (sent_at - originalTimestamp). Use
              timestamp for analysis when chronology DOES matter.
          - name: user_id
            description: Unique identifier for the user in our database.
              A user_id or an anonymous_id is required for all events.
          - name: context_library_name
            description: The name of the library that generated the event.
          - name: context_library_version
            description: The version of the library that generated the event.
          - name: context_integration_name
            description: The name of the integration that generated the
              event.
          - name: context_integration_version
            description: The version of the integration that generated the
              event.
          - name: context_traits_email
            description: The email address of the user.
          - name: context_protocols_source_id
            description: If a protocol violation is detected for this instance
              of the event firing, the ID of the Segment source.
          - name: context_protocols_violations
            description: If a protocol violation is detected for this instance
              of the event firing, a list of the protocol violations.
          - name: context_traits_email
            description: The email address of the recipient.
          - name: campaign_id
            description: The campaign ID that the email belongs to.
          - name: campaign_name
            description: The campaign name that the email belongs to.
          - name: email_id
            description: The email ID.
          - name: email_permalink
            description: The email permalink. Points directly to a log of
              the email that the user received.
          - name: email_subject
            description: The email subject.
          - name: email_type
            description: The email type.
