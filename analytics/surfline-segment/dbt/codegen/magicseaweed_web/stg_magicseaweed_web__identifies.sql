-- dbt SQL below was generated using the `dbt_codegen_identifies.py` script
{{
  config(
    materialized = 'incremental',
    unique_key = 'identify_id',
    sort = 'tstamp',
    dist = 'identify_id'
    )
}}

{#- Get source table object #}
{%- set source_relation = adapter.get_relation(
      database = var('segment_database'),
      schema = 'msw_web',
      identifier = 'identifies'
    )
-%}

{# Set a list of page properties (from tracking plan) to append to the model #}
{%- set identify_traits = [ 'is_pro', 'username', 'gdpr_consent', 'location_country', 'subscription_interval', 'email', 'favorites_count', 'locale', 'subscription_active', 'subscription_entitlement', 'location_continent', 'subscription_expiration',  ] %}

{# Set of reserved SQL keywords that need to be double-quoted if used as column #}
{%- set double_quoted_cols = ["name", "interval", "location", "time", "group"] %}

with source as (

    select *

    from {{ source('msw_web', 'identifies') }}
    {%- if is_incremental() %}
    where received_at >= (
            select
                dateadd(
                    hours,
                    -3,
                    max(received_at_tstamp)
                )

            from {{ this }}
        )
        and received_at < current_date
    {%- else %}
    {%- if target.name == 'dev' %}
    where received_at > {{ var('dev_start_date') }}
        and received_at < current_date
    {%- else %}
    where received_at >= {{ var('prod_start_date') }}
        and received_at < current_date
    {%- endif %}
    {%- endif %}

),

renamed as (

    select
        id as identify_id
        , 'msw_web'::varchar as source_schema
        , 'identifies'::varchar as source_table

        -- Timestamp properties
        , "timestamp" as tstamp
        , received_at as received_at_tstamp
        , sent_at as sent_at_tstamp

        -- User properties
        , anonymous_id
        , user_id

        -- Identify traits from Segment tracking plan
        {%- for trait in identify_traits|sort %}
        {%- if trait in double_quoted_cols %}
        {%- autoescape false %}
        , "{{ trait }}"
        {%- endautoescape %}
        {%- else %}
        , {{ trait }}
        {%- endif %}
        {%- endfor %}

    from source

)

select * from renamed
