-- dbt SQL below was generated using the `dbt_codegen_tracks_web.py` script
{{
  config(
    materialized = 'incremental',
    unique_key = 'event_id',
    sort = 'tstamp',
    dist = 'event_id'
    )
}}

{#- Get source table object #}
{%- set source_relation = adapter.get_relation(
      database = var('segment_database'),
      schema = 'msw_web',
      identifier = 'tracks'
    )
-%}

with source as (

    select *

    from {{ source('msw_web', 'tracks') }}
    {%- if is_incremental() %}
    where received_at >= (
            select
                dateadd(
                    hours,
                    -3,
                    max(received_at_tstamp)
                )

            from {{ this }}
        )
        and received_at < current_date
    {%- else %}
    {%- if target.name == 'dev' %}
    where received_at > {{ var('dev_start_date') }}
        and received_at < current_date
    {%- else %}
    where received_at >= {{ var('prod_start_date') }}
        and received_at < current_date
    {%- endif %}
    {%- endif %}

),

source_xf as (

    select
        *,

        {#
        This CASE statement solves an issue where the `url` column does not
        include the query string parameters. When the query string parameters
        are not part of the URL, the dbt_utils.get_url_parameter() macro will
        not find the specified parameter (e.g., gclid, fbaid, cid, etc.).
        #}
        context_page_url as "url",  -- tracks don't have a `url` column
        case
            when context_page_url ilike '%?%' then context_page_url
            else concat(context_page_url, coalesce(context_page_search, ''))
        end as url_with_query

    from source

),

renamed as (

    select
        id as event_id,
        'msw_web'::varchar as source_schema,
        'tracks'::varchar as source_table,
        'track'::varchar as call_type,
        event_text as event_name,
        context_library_name as library_name,

        -- Timestamp properties
        "timestamp" as tstamp,
        received_at as received_at_tstamp,
        sent_at as sent_at_tstamp,

        -- User properties
        anonymous_id,
        user_id,

        -- URL properties
        "url" as page_url,
        {{ dbt_utils.get_url_host('url') }} as page_url_host,
        context_page_path as page_url_path,
        context_page_title as page_title,
        context_page_search as page_url_query,

        -- UTM properties
        context_campaign_source::varchar as utm_source,
        context_campaign_medium::varchar as utm_medium,
        context_campaign_name::varchar as utm_campaign,
        context_campaign_term::varchar as utm_term,
        context_campaign_content::varchar as utm_content,

        -- Off-site campaign tracking properties
        cast( {{ dbt_utils.get_url_parameter('url_with_query', 'gclid') }} as varchar) as gclid,
        cast( {{ dbt_utils.get_url_parameter('url_with_query', 'fbaid') }} as varchar) as fbaid,
        cast(
            case
                when context_campaign_source ilike 'customer.io'
                    then {{ dbt_utils.get_url_parameter('url_with_query', 'cid') }}
            end
            as varchar
         ) as cid,

        -- Referral tracking properties
        context_page_referrer as referrer,
        cast(
            replace( {{ dbt_utils.get_url_host('context_page_referrer') }}, 'www.', '')
            as varchar
        ) as referrer_host,

        -- Device properties
        context_ip as ip,
        context_user_agent as user_agent,
        cast(
            case
                when lower(context_user_agent) like '%android%'
                    then 'Android'
                else replace(
                    {{ dbt_utils.split_part(dbt_utils.split_part('context_user_agent', "'('", 2), "' '", 1) }},
                    ';',
                    ''
                    )
            end
            as varchar
        ) as device,

        case
            -- See for context: https://surfline.slack.com/archives/CSGTPLHN0/p1612561651045000
            when {{ dbt_utils.get_url_parameter('url_with_query', 'native') }} = 'true'
                then true
            when {{ dbt_utils.get_url_parameter('url_with_query', 'ios') }} = 'true'
                then true
            when {{ dbt_utils.get_url_parameter('url_with_query', 'native') }} = 'ios'
                then true
            else false
        end as is_native_web_view

    from source_xf

),

categorized as (

    select
        *,

        cast(
            case
                when device = 'iPhone' then 'iPhone'
                when device = 'Android' then 'Android'
                when device in ('iPad', 'iPod') then 'Tablet'
                when device in ('Windows', 'Macintosh', 'X11') then 'Desktop'
                else 'Uncategorized'
            end
            as varchar
        ) as device_category

    from renamed

)

select * from categorized
