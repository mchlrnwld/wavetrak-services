-- dbt SQL below was generated using the `dbt_codegen_tracks_mobile.py` script
{{
  config(
    materialized = 'incremental',
    unique_key = 'event_id',
    sort = 'tstamp',
    dist = 'event_id'
    )
}}

{#- Get source table object #}
{%- set source_relation = adapter.get_relation(
      database = var('segment_database'),
      schema = 'surfline_ios',
      identifier = 'tracks'
    )
-%}
{#- Get all columns from source table #}
{%- set source_columns_raw = adapter.get_columns_in_relation(source('surfline_ios', 'tracks')) -%}

{#- Build a list of strings containing the source column names #}
{%- set source_column_names = [] -%}
{%- for column in source_columns_raw -%}
{{ source_column_names.append(column.name) or '' }}
{%- endfor -%}

with source as (

    select *

    from {{ source('surfline_ios', 'tracks') }}
    {%- if is_incremental() %}
    where received_at >= (
            select
                dateadd(
                    hours,
                    -3,
                    max(received_at_tstamp)
                )

            from {{ this }}
        )
        and received_at < current_date
    {%- else %}
    {%- if target.name == 'dev' %}
    where received_at > {{ var('dev_start_date') }}
        and received_at < current_date
    {%- else %}
    where received_at >= {{ var('prod_start_date') }}
        and received_at < current_date
    {%- endif %}
    {%- endif %}

),

renamed as (

    select
        id as event_id,
        'surfline_ios'::varchar as source_schema,
        'tracks'::varchar as source_table,
        'track'::varchar as call_type,
        event_text as event_name,
        context_library_name as library_name,

        -- Timestamp properties
        "timestamp" as tstamp,
        received_at as received_at_tstamp,
        sent_at as sent_at_tstamp,

        -- User properties
        anonymous_id,
        user_id,

        -- Device properties
        context_device_id as device_id,
        regexp_substr(context_device_model, '[a-zA-Z]+') as device,
        REGEXP_replace(
            REGEXP_replace(context_device_model, '[a-zA-Z]', ''),
            ',',
            '.'
        ) as device_version,
        context_os_name as device_os_name,
        context_os_version as device_os_version,
        context_network_wifi as device_on_wifi,
        context_ip as ip,

        -- App properties
        {% if 'context_app_name' in source_column_names %}
        context_app_name as "app_name",
        {%- endif %}
        context_app_version as app_version,
        context_app_build as app_build,

        -- Ad tracking properties
        context_device_ad_tracking_enabled as ad_tracking_enabled,
        context_device_advertising_id as device_ad_id

    from source

),

categorized as (

    select
        *,

        case
            when device = 'iPhone' then 'iPhone'
            when device = 'Android' then 'Android'
            when device in ('iPad', 'iPod') then 'Tablet'
            when device in ('Windows', 'Macintosh', 'X11') then 'Desktop'
            else 'Uncategorized'
        end as device_category

    from renamed

)

select * from categorized
