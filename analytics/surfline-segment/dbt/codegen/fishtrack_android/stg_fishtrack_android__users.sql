-- dbt SQL below was generated using the `dbt_codegen_users.py` script
{{
  config(
    materialized = 'table',
    unique_key = 'user_id',
    )
}}

{#- Get source table object #}
{%- set source_relation = adapter.get_relation(
      database = var('segment_database'),
      schema = 'fishtrackandroid',
      identifier = 'users'
    )
-%}

{# Set a list of page properties (from tracking plan) to append to the model #}
{%- set identify_traits = [ 'subscription_entitlement',  ] %}

{# Set of reserved SQL keywords that need to be double-quoted if used as column #}
{%- set double_quoted_cols = ["name", "interval", "location", "time", "group"] %}

with source as (

    select * from {{ source('fishtrackandroid', 'users') }}

),

renamed as (

    select
        id as user_id
        , 'fishtrackandroid'::varchar as source_schema
        , 'users'::varchar as source_table

        -- Timestamp properties
        , "received_at" as received_at_tstamp

        -- User traits from Segment tracking plan
        {%- for trait in identify_traits|sort %}
        {%- if trait in double_quoted_cols %}
        {%- autoescape false %}
        , "{{ trait }}"
        {%- endautoescape %}
        {%- else %}
        , {{ trait }}
        {%- endif %}
        {%- endfor %}

    from source

)

select * from renamed
