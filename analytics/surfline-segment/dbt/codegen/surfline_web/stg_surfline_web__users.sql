-- dbt SQL below was generated using the `dbt_codegen_users.py` script
{{
  config(
    materialized = 'table',
    unique_key = 'user_id',
    )
}}

{#- Get source table object #}
{%- set source_relation = adapter.get_relation(
      database = var('segment_database'),
      schema = 'surfline',
      identifier = 'users'
    )
-%}

{# Set a list of page properties (from tracking plan) to append to the model #}
{%- set identify_traits = [ 'ability_level', 'brand', 'created_at', 'cross_domain_id', 'cw_user_id', 'email', 'favorites_count', 'first_name', 'last_name', 'gender', 'is_email_verified', 'locale', 'location_continent', 'location_country', 'location_country_iso', 'location_subdivision_one', 'location_subdivision_two', 'location_subdivision_two_iso', 'migrated_brand', 'receive_promotions', 'subscription_currency', 'subscription_active', 'subscription_availablediscount_10off', 'subscription_availablediscount_20off', 'subscription_availablediscount_50off', 'subscription_discount', 'subscription_entitlement', 'subscription_expiration', 'subscription_in_grace_period', 'subscription_in_trial', 'subscription_interval', 'subscription_interval_count', 'subscription_plan_id', 'subscription_promotion_id', 'subscription_renewal_count', 'subscription_trial_eligible', 'zero_checks_left',  ] %}

{# Set of reserved SQL keywords that need to be double-quoted if used as column #}
{%- set double_quoted_cols = ["name", "interval", "location", "time", "group"] %}

with source as (

    select * from {{ source('surfline', 'users') }}

),

renamed as (

    select
        id as user_id
        , 'surfline'::varchar as source_schema
        , 'users'::varchar as source_table

        -- Timestamp properties
        , "received_at" as received_at_tstamp

        -- User traits from Segment tracking plan
        {%- for trait in identify_traits|sort %}
        {%- if trait in double_quoted_cols %}
        {%- autoescape false %}
        , "{{ trait }}"
        {%- endautoescape %}
        {%- else %}
        , {{ trait }}
        {%- endif %}
        {%- endfor %}

    from source

)

select * from renamed
