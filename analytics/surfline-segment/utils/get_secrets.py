import os

from dotenv import load_dotenv

# Set environment variables from .env if working locally
load_dotenv()

# Else, set environment variables from inject secrets from AWS Parameter Store
SURFLINE_SEGMENT_TRACKING_PLAN_AS_CODE_TOKEN = os.getenv(
    "SURFLINE_SEGMENT_TRACKING_PLAN_AS_CODE_TOKEN"
)

SEGMENT_EVENT_METADATA_S3_BUCKET = os.getenv(
    "SEGMENT_EVENT_METADATA_S3_BUCKET"
)

AWS_ACCESS_KEY_ID = os.getenv("AWS_ACCESS_KEY_ID")

AWS_SECRET_ACCESS_KEY = os.getenv("AWS_SECRET_ACCESS_KEY")

DB_NAME = os.getenv("DB_NAME")

DB_PORT = os.getenv("DB_PORT")

DB_USER = os.getenv("DB_USER")

DB_PASSWORD = os.getenv("DB_PASSWORD")

DB_HOST_URL = os.getenv("DB_HOST_URL")

DB_SCHEMA = os.getenv("DB_SCHEMA")

SURFLINE_SEGMENT_DIR = os.getenv("SURFLINE_SEGMENT_DIR")

HOME = os.getenv("HOME")
