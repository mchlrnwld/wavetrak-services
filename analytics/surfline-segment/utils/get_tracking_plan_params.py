import json
from typing import Dict

import requests

from get_secrets import SURFLINE_SEGMENT_TRACKING_PLAN_AS_CODE_TOKEN


def get_tracking_plan_ids(bearer_token):
    url = "https://platform.segmentapis.com/v1beta/workspaces/surfline/tracking-plans/"  # noqa: E501

    payload: Dict = {}
    headers = {
        "Authorization": bearer_token,
        "Content-Type": "application/json",
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    json_dict = json.loads(response.text)

    tracking_plans = json_dict["tracking_plans"]

    tracking_plan_ids = []
    tracking_plan_names = []
    for tracking_plan in tracking_plans:
        tracking_plan_url = tracking_plan["name"]
        tracking_plan_id = tracking_plan_url.split("/")[-1]
        tracking_plan_ids.append(tracking_plan_id)
        tracking_plan_names.append(tracking_plan["display_name"])

    return tracking_plan_ids, tracking_plan_names


# Set bearer token for authentication
bearer_token = "Bearer {0}".format(
    SURFLINE_SEGMENT_TRACKING_PLAN_AS_CODE_TOKEN
)

TRACKING_PLAN_IDS, TRACKING_PLAN_NAMES = get_tracking_plan_ids(
    bearer_token=bearer_token
)
