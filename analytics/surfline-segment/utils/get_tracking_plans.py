"""
Script used to generate the YAML tracking plans that correspond to *existing*
tracking plans in Segment. This script should be used with caution during local
development only. It should never be used as part of a GitHub Workflow.

The script is not perfect. Manual formatting may be required after running.
"""

import json

import requests
import yaml

from get_secrets import SURFLINE_SEGMENT_TRACKING_PLAN_AS_CODE_TOKEN

# The user of this script should manually set the two lists:
#  - tracking_plan_names
#  - tracking_plan_ids

# Provide a list of tracking plan names to fetch
tracking_plan_names = [
    "braze",
    # "buoyweather-web",
    # "buoyweather-ios",
    # "buoyweather-android",
    # "fishtrack-web",
    # "fishtrack-ios",
    # "fishtrack-android",
]

# Provide a list of tracking plan IDs to fetch (from Segment URL)
tracking_plan_ids = [
    "rs_1zsRwBjpFWxfmj7G4PAFiOpP6ul",
    # "rs_1yBlcu1vSV6Mo4MlD8ZykqAH2Jx",
    # "rs_1yBlIrWzivSuQtoSz1JNHyYLcwL",
    # "rs_1yBliLtWyQ6Xitoeadwn0na3mp3",
    # "rs_1yBlpTg5juISUnq3hmeQ1Emb8De",
    # "rs_1yBlyg3ZUx8FWpiSSsdhRQ6CyQa",
    # "rs_1yBm3eTLL5hDODkZD3Wo2B4NSfL",
]


class MyDumper(yaml.Dumper):
    """Custom dumper class to handle formatting YAML tracking plans."""

    def increase_indent(self, flow=False, indentless=False):
        return super(MyDumper, self).increase_indent(flow, False)


def main():
    # Where to write generated tracking plans.
    write_folder = "./dev"

    # For Authentication
    bearer_token = "Bearer {0}".format(
        SURFLINE_SEGMENT_TRACKING_PLAN_AS_CODE_TOKEN
    )

    # Iterate through thr provided tracking plans names and IDs
    for tracking_plan_id, tracking_plan_name in zip(
        tracking_plan_ids, tracking_plan_names
    ):
        print(f"Generating tracking plan: {tracking_plan_name}")
        url_base = "https://platform.segmentapis.com/v1beta/workspaces/surfline/tracking-plans/"  # noqa: E501
        url = url_base + tracking_plan_id
        payload = {}
        headers = {
            "Authorization": bearer_token,
            "Content-Type": "application/json",
        }

        # Get JSON tracking plan from Segment Config API, parse to dictionary
        response = requests.request("GET", url, headers=headers, data=payload)
        json_dict = json.loads(response.text.encode("utf8"))

        # Get Surfline YAML tracking plan schema. We use YAML for readability.
        with open("./schema/tracking_plan_schema__surfline.yaml") as f:
            tracking_plan_schema = yaml.safe_load(f)

        tracking_plan_schema["tracking_plan_id"] = tracking_plan_id
        tracking_plan_schema["tracking_plan_name"] = tracking_plan_name

        # Fill in global spec to YAML
        if "global" in json_dict["rules"]:
            global_ = json_dict["rules"]["global"]
            tracking_plan_schema["global"]["context"] = global_["properties"][
                "context"
            ]
            tracking_plan_schema["global"]["traits"] = global_["properties"][
                "traits"
            ]
            tracking_plan_schema["global"]["properties"] = global_[
                "properties"
            ]["properties"]

        # Fill in identify spec to YAML
        if "identify" in json_dict["rules"]:
            identify = json_dict["rules"]["identify"]
            tracking_plan_schema["identify"]["traits"][
                "properties"
            ] = identify["properties"]["traits"]["properties"]

        events = json_dict["rules"]["events"]

        for event in events:
            # Get Surfline YAML event schema. We use YAML for readability.
            with open("./schema/event_schema__surfline.yaml") as f:
                event_schema = yaml.safe_load(f)

            event_schema["name"] = event["name"]

            if "description" in event:
                event_schema["description"] = event["description"]

            if "version" in event:
                event_schema["version"] = event["version"]

            if "labels" in event["rules"]:
                event_schema["labels"] = event["rules"]["labels"]

            if "properties" in event["rules"]["properties"]["properties"]:
                event_schema["properties"] = event["rules"]["properties"][
                    "properties"
                ]["properties"]

                if "required" in event["rules"]["properties"]["properties"]:
                    event_schema["required"] = event["rules"]["properties"][
                        "properties"
                    ]["required"]

            # Append event YAML to `events` in Surfline YAML tracking plan
            tracking_plan_schema["events"].append(event_schema)

        # Write YAML tracking plan to files
        yaml_file_path = write_folder + "/" + tracking_plan_name + ".yaml"

        with open(yaml_file_path, "w+") as yaml_file:
            yaml.dump(
                tracking_plan_schema,
                yaml_file,
                indent=2,
                Dumper=MyDumper,
                sort_keys=False,
                default_flow_style=False,
                allow_unicode=True,
                encoding=("utf-8"),
            )


if __name__ == "__main__":
    main()
