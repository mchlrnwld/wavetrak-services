"""Tests tracking plans.

Test cases:
    - Every event has description.
    - Every event has owner.
    - Every event has priority. Accepted values = 1 | 2
    - Every event has fires_on.
    - Every property has defined data type specified and is a valid type.
    - Every property has description [disable to start]
    - Every event has a required object (empty acceptable)
    - If version > 1, check that a previous version already exists.
"""

import json
import logging
import os
import sys
from logging.config import fileConfig
from os import listdir
from os.path import isfile, join

import yaml
from cerberus import Validator
from dotenv import load_dotenv

# Set environment variables from .env if working locally
load_dotenv()

# Define the validation schema for tracking plan events
event_schema = {
    "name": {"required": True, "type": "string"},
    "description": {"required": True, "type": "string"},
    "version": {"required": True, "type": "integer", "min": 1},
    "labels": {
        "required": True,
        "type": "dict",
        "schema": {
            "owner": {
                "required": True,
                "type": "string",
                "allowed": [
                    "@Surfline/squad-analytics",
                    "@Surfline/squad-subscription",
                    "@Surfline/squad-infrastructure",
                    "@Surfline/squad-web-platform",
                    "@Surfline/forecast-web",
                    "@Surfline/native-ios",
                    "@Surfline/native-android",
                    "@Surfline/squad-realtime",
                    "@Surfline/squad-content",
                    "@Surfline/squad-msw-buoyweather",
                ],
            },
            "priority": {
                "required": True,
                "type": "integer",
                "allowed": [1, 2],
            },
            "fires_on": {
                "required": True,
                "type": "string",
                "allowed": [
                    "client",
                    "server",
                    "braze",
                    "vero",
                    "customer-io",
                    "split",
                ],
            },
        },
    },
    "properties": {"required": True, "type": "dict"},
    "required": {"required": True, "type": "list"},
}

# Define the validation schema for tracking plan event properties
property_schema_strict = {
    "description": {"required": True, "type": "string"},
    "type": {
        "required": True,
        "allowed": [
            "string",
            "integer",
            "number",
            "boolean",
            "array",
            "object",
            "null",
        ],
    },
    "enum": {"required": False, "type": "list"},
    "pattern": {"required": False, "type": "string"},
    "format": {"required": False, "type": "string", "allowed": ["date-time"]},
    "properties": {"required": False, "type": "dict"},
    "items": {"required": False, "type": "dict"},
    "required": {"required": False, "type": "list"},
}

property_schema_relaxed = {
    "description": {"required": True, "type": "string"},
    "type": {
        "required": False,  # Used to validate properties in old event versions
        "allowed": [
            "string",
            "integer",
            "number",
            "boolean",
            "array",
            "object",
            "null",
        ],
    },
    "enum": {"required": False, "type": "list"},
    "pattern": {"required": False, "type": "string"},
    "format": {"required": False, "type": "string", "allowed": ["date-time"]},
    "properties": {"required": False, "type": "dict"},
    "items": {"required": False, "type": "dict"},
    "required": {"required": False, "type": "list"},
}

reserved_properties = [
    "page_id",
    "screen_id",
    "event_id",
    "pageid",
    "screenid",
    "eventid",
]


def parse_json_string(json_string):
    """Checks that a provided JSON string parses to valid JSON."""
    try:
        return json.loads(json_string)
    except:  # noqa: E722
        raise  # exception is raised if their is an issue parsing string


def is_consecutive(lst):
    """
    Checks numbers in a list can be ordered consecutively.
    True if yes, False if no.
    """
    if len(set(lst)) == len(lst) and max(lst) - min(lst) == len(lst) - 1:
        return True
    else:
        return False


def main():
    """Entry point into script."""
    passed_tests = True  # Assume all tests pass until proven otherwise

    # Determine where to find the tracking plans (dev vs. prod)
    if os.getenv("ENVIRONMENT") == "dev":
        # Get tracking plans
        SURFLINE_SEGMENT_DIR = os.getenv("SURFLINE_SEGMENT_DIR")
        SURFLINE_SEGMENT_LOGGING_DIR = SURFLINE_SEGMENT_DIR
        tracking_plan_folder = os.path.join(
            SURFLINE_SEGMENT_DIR, "tracking_plans_yaml"
        )
        tracking_plans = [
            os.path.join(tracking_plan_folder, f)
            for f in listdir(tracking_plan_folder)
            if isfile(join(tracking_plan_folder, f)) and f.endswith(".yaml")
        ]

    else:
        # Get env vars
        HOME = os.getenv("HOME")
        SURFLINE_SEGMENT_DIR = os.getenv("SURFLINE_SEGMENT_DIR")
        SURFLINE_SEGMENT_LOGGING_DIR = os.getenv(
            "SURFLINE_SEGMENT_LOGGING_DIR"
        )

        # Get modified and added files from PR
        modified_files_path = os.path.join(HOME, "files_modified.json")
        added_files_path = os.path.join(HOME, "files_added.json")

        with open(modified_files_path) as json_file:
            modified_files = json.load(json_file)

        with open(added_files_path) as json_file:
            added_files = json.load(json_file)

        # Get a list of the modified tracking plans
        modified_plans = [
            os.path.join(SURFLINE_SEGMENT_DIR, file)
            for file in modified_files
            if "/tracking_plans_yaml/" in file and ".yaml" in file
        ]

        # Get a list of the added tracking plans
        added_plans = [
            os.path.join(SURFLINE_SEGMENT_DIR, file)
            for file in added_files
            if "/tracking_plans_yaml/" in file and ".yaml" in file
        ]

        # Combine the two tracking lists so we have added and modified
        tracking_plans = modified_plans + added_plans

        # If no tracking plans were added or modified, no tests to run
        if not tracking_plans:
            print("No modified tracking plans. Skipping tests.")
            sys.exit(0)  # Throw zero exit code since no tests were run

    # Setup logging
    log_file_path = os.path.join(
        SURFLINE_SEGMENT_LOGGING_DIR, "logging-test-plans.ini"
    )
    fileConfig(log_file_path)
    logger = logging.getLogger(__name__)

    # Iterate through each tracking plan
    for tracking_plan in tracking_plans:
        tracking_plan_name = tracking_plan.split("/")[-1]
        logger.info(f"Validating {tracking_plan_name}")

        with open(tracking_plan, "r") as yaml_in:
            yaml_plan_dict = yaml.safe_load(yaml_in)
            # Validate that YAML can convert to valid JSON
            logger.info(
                f"Validating {tracking_plan_name} coverts to valid JSON"
            )
            # Dump dictionary (from YAML) to a JSON string, then parse JSON
            # string to ensure it is valid JSON.
            parse_json_string(json.dumps(yaml_plan_dict, indent=4))

        events = yaml_plan_dict["events"]

        # Iterate through each event in the tracking plan
        for event in events:
            event_name = event["name"]  # Get the event name
            event_version = event["version"]  # Get the event version
            properties = event["properties"]  # Get the event properties

            # Validate event against event_schema
            logger.info(f"Validating event: {event_name}, ")
            v_event = Validator(event_schema)
            result_event = v_event.validate(event, event_schema)
            if not result_event:
                logger.error(
                    f"in {tracking_plan_name}, event: {event_name} - {v_event.errors}"  # noqa: E501
                )
                passed_tests = False

            # Now we need to validate the properties of the event
            # - For event versions != max version, relaxed validation
            # - For event versions == max version, strict validation
            event_versions = [  # Get all event versions for event name
                event for event in events if event["name"] == event_name
            ]

            # Get a list of all the event versions
            versions = [
                event["version"]
                for event in event_versions
                if event["name"] == event_name
            ]

            # Check if the current event being validated is the latest version
            if event["version"] == max(versions):
                # Validate properties in the event using property_schema_strict
                property_schema = property_schema_strict

                # If only one version of event, ensure version is 1
                if len(versions) == 1:
                    try:
                        assert max(versions) == 1, (
                            f"'{event_name}' in {tracking_plan_name} only has "
                            f"1 entry in the tracking plan."
                            f"\n        But 'version' property value = "
                            f"{event_version}. Please set the 'version' "
                            f"property for {event_name} to 1."
                        )
                    except AssertionError as e:
                        logger.error(e)
                        passed_tests = False

                # If > 1 version, check for consecutive version numbers
                if max(versions) > 1:
                    try:
                        assert is_consecutive(versions), (
                            f"Non-consecutive event versions found for event: "
                            f"{event_name}.\n    Event versions must be "
                            f"consecutive integers.\n"
                            f"    Event versions found: {versions}."
                        )
                    except AssertionError as e:
                        logger.error(e)
                        passed_tests = False

            else:
                # Validate properties in event using property_schema_relaxed
                property_schema = property_schema_relaxed

            # Iterate through each property nested in the properties dictionary
            for prop_key, v in properties.items():
                if str.lower(prop_key) in reserved_properties:
                    logger.error(
                        f"\n    Tracking plan: {tracking_plan_name},"
                        f"\n    Event: {event_name},"
                        f"\n    Version: {event_version},"
                        f"\n    Property name '{prop_key}' is reserved and cannot be used as an event property.\n"  # noqa: E501
                    )
                    passed_tests = False

                # If nested property dictionary is empty, do nothing
                if v == {}:
                    continue
                # Else, validate nested property dictionary against schema
                else:
                    logger.info(f"    Validating property: {prop_key}")
                    validator = Validator(property_schema)
                    is_valid = validator.validate(v, property_schema)
                    if not is_valid:
                        logger.error(
                            f"in {tracking_plan_name}, "
                            f"event: {event_name}, "
                            f"property: {prop_key} - {validator.errors}"
                        )
                        passed_tests = False

    if not passed_tests:
        print(
            "FAILED - Proposed Tracking Plan changes fail schema validataion tests. See logs above."  # noqa: E501
        )
        sys.exit(1)  # Throw non-zero exit code to indicate failure

    else:
        print(
            "PASSED - Proposed Tracking Plan changes pass schema validataion tests."  # noqa: E501
        )


if __name__ == "__main__":
    main()
