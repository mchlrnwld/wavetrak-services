import json
import os

import requests
import yaml

from get_secrets import SURFLINE_SEGMENT_TRACKING_PLAN_AS_CODE_TOKEN


def main(write_payload=False):
    # Determine where to find the tracking plans (dev vs. prod)
    if os.getenv("ENVIRONMENT") == "dev":
        # Set paths manually for dev
        tracking_plans = [
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/magicseaweed-android.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/magicseaweed-ios.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/magicseaweed-web.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/split-production.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/surfline-android.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/surfline-camera-embed.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/surfline-ios.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/surfline-web.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/buoyweather-android.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/buoyweather-ios.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/buoyweather-web.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/fishtrack-android.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/fishtrack-ios.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/fishtrack-web.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/tracking-plan-example.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/braze.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/vero.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/customer-io-surfline.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/customer-io-buoyweather.yaml",  # noqa: E501
            "/Users/gclunies2-mba/Repos/wavetrak-services/analytics/surfline-segment/tracking_plans_yaml/customer-io-fishtrack.yaml",  # noqa: E501
        ]

    else:
        # Get env vars
        HOME = os.getenv("HOME")
        SURFLINE_SEGMENT_DIR = os.getenv("SURFLINE_SEGMENT_DIR")

        # Get modified and added files from PR
        modified_files_path = os.path.join(HOME, "files_modified.json")
        added_files_path = os.path.join(HOME, "files_added.json")

        with open(modified_files_path) as json_file:
            modified_files = json.load(json_file)

        with open(added_files_path) as json_file:
            added_files = json.load(json_file)

        # Get a list of the modified tracking plans
        modified_plans = [
            os.path.join(SURFLINE_SEGMENT_DIR, file)
            for file in modified_files
            if "/tracking_plans_yaml/" in file and ".yaml" in file
        ]

        # Get a list of the added tracking plans
        added_plans = [
            os.path.join(SURFLINE_SEGMENT_DIR, file)
            for file in added_files
            if "/tracking_plans_yaml/" in file and ".yaml" in file
        ]

        # Combine the two tracking lists so we have added and modified
        tracking_plans = modified_plans + added_plans

        # If no tracking plans were added or modified, no tests to run
        if not tracking_plans:
            print("No modified tracking plans. Skipping sync to Segment.")

    # Iterate through each tracking plan
    for tracking_plan in tracking_plans:
        print("\n")
        print("Syncing tracking plan: {}".format(tracking_plan.split("/")[-1]))

        with open(tracking_plan, "r") as yaml_in:
            yaml_plan_dict = yaml.safe_load(yaml_in)

        # Get tracking plan schema
        with open("./schema/tracking_plan_schema.json") as f:
            tracking_plan_schema = json.load(f)

        tracking_plan_schema["display_name"] = yaml_plan_dict[
            "tracking_plan_name"
        ]

        # Fill tracking plan schema with global spec from YAML tracking plan
        tracking_plan_schema["rules"]["global"]["properties"][
            "properties"
        ] = yaml_plan_dict["global"]["properties"]
        tracking_plan_schema["rules"]["global"]["properties"][
            "context"
        ] = yaml_plan_dict["global"]["context"]
        tracking_plan_schema["rules"]["global"]["properties"][
            "traits"
        ] = yaml_plan_dict["global"]["traits"]

        # Fill tracking plan schema with identify spec from YAML tracking plan
        tracking_plan_schema["rules"]["identify"]["properties"][
            "traits"
        ] = yaml_plan_dict["identify"]["traits"]

        # Fill tracking plan schema with group spec from YAML tracking plan
        tracking_plan_schema["rules"]["group"]["properties"][
            "properties"
        ] = yaml_plan_dict["group"]["properties"]
        tracking_plan_schema["rules"]["group"]["properties"][
            "context"
        ] = yaml_plan_dict["group"]["context"]
        tracking_plan_schema["rules"]["group"]["properties"][
            "traits"
        ] = yaml_plan_dict["group"]["traits"]

        # Get the events from thew YAML tracking plan
        events = yaml_plan_dict["events"]

        # Iterate through each event
        for event in events:
            # Get the event schema
            with open("./schema/event_schema.json") as f:
                event_schema = json.load(f)

            # Fill event schema with event spec from YAML tracking plan
            event_schema["name"] = event["name"]
            event_schema["description"] = event["description"]
            event_schema["version"] = event["version"]
            event_schema["rules"]["labels"] = event["labels"]
            event_schema["rules"]["properties"]["properties"][
                "properties"
            ] = event["properties"]
            event_schema["rules"]["properties"]["properties"][
                "required"
            ] = event["required"]

            # Append the individual event schema to the tracking plan schema
            tracking_plan_schema["rules"]["events"].append(event_schema)

        # Setup for request to Segment Config API
        url_base = "https://platform.segmentapis.com/v1beta/workspaces/surfline/tracking-plans/"  # noqa: E501
        tracking_plan_url_suffix = yaml_plan_dict["tracking_plan_id"]
        url = url_base + tracking_plan_url_suffix
        bearer_token = "Bearer {0}".format(
            SURFLINE_SEGMENT_TRACKING_PLAN_AS_CODE_TOKEN
        )
        headers = {
            "Authorization": bearer_token,
            "Content-Type": "application/json",
        }

        with open("./schema/payload_schema.json") as f:
            payload_schema = json.load(f)

        payload_schema["tracking_plan"] = tracking_plan_schema

        # Set the payload for the request
        payload = json.dumps(payload_schema, indent=2)

        if write_payload:
            debug_json_file_path = tracking_plan.replace(
                ".yaml", "-payload.json"
            )
            with open(debug_json_file_path, "w+") as json_file:
                json.dump(payload_schema, json_file, indent=2)

        # Make a request to Segment's Config API to update the tracking plan
        response = requests.request("PUT", url, headers=headers, data=payload)

        response_dict = json.loads(response.text.encode("utf8"))

        if "error" in response_dict:
            raise Exception(
                "Segment Config API error encountered during sync. "
                "Error type: {0}. Error code: {1}. "
                "See https://segment.com/docs/config-api/api-design/#errors for"  # noqa: E501
                " Config API error descriptions.".format(
                    response_dict["error"], response_dict["code"]
                )
            )

        else:
            print(response.text.encode("utf8"))


if __name__ == "__main__":
    if os.getenv("ENVIRONMENT") == "dev":
        main(write_payload=False)  # True to debug payload sent to Segment
    else:
        main()
