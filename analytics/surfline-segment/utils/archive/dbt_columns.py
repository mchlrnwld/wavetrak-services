# flake8: noqa

# Dictionary of the standard columns we use in SQL template that are NOT from
# the event spec in tracking plan
std_columns_dict = {
    "id": {
        "source_name": "id",
        "schema_stg_name": "id",
        "description": "Primary key of the __TABLE_NAME__ table.",
    },
    "event": {
        "source_name": "event",
        "schema_stg_name": "source_table",
        "description": "The slug of the event name, can be used to join to the __SCHEMA_NAME__.tracks table.",
    },
    "event_text": {
        "source_name": "event_text",
        "schema_stg_name": "event_name",
        "description": "The name of the event, can be used to join to the __SCHEMA_NAME__.tracks table.",
    },
    "original_timestamp": {
        "source_name": "original_timestamp",
        "schema_stg_name": None,
        "description": "Time on the client device when call was invoked, OR the timestamp value manually passed in through server-side libraries.",
    },
    "sent_at": {
        "source_name": "sent_at",
        "schema_stg_name": "sent_at_tstamp",
        "description": "Time on client device when call was sent, OR sent_at value manually passed in. NOTE - sent_at is NOT USEFUL for analysis since it’s not always trustworthy as it can be easily adjusted and affected by clock skew.",
    },
    "received_at": {
        "source_name": "received_at",
        "schema_stg_name": "received_at_tstamp",
        "description": "Time on Segment server clock when call was received. received_at is used as sort key in Warehouses. For max query speed, received_at is the recommended timestamp for analysis when chronology DOES NOT matter as chronology is NOT ENSURED.",
    },
    "timestamp": {
        "source_name": "timestamp",
        "schema_stg_name": "tstamp",
        "description": "When the event occurred. Calculated by Segment to correct client-device clock skew using the following formula; timestamp = received_at - (sent_at - originalTimestamp). Use timestamp for analysis when chronology DOES matter.",
    },
    "anonymous_id": {
        "source_name": "anonymous_id",
        "schema_stg_name": "anonymous_id",
        "description": "A pseudo-unique substitute for a User ID, for cases when we don’t have an absolutely unique identifier. A user_id or an anonymous_id is required for all events.",
    },
    "user_id": {
        "source_name": "user_id",
        "schema_stg_name": "user_id",
        "description": "Unique identifier for the user in our database. A user_id or an anonymous_id is required for all events.",
    },
    "context_campaign_name": {
        "source_name": "context_campaign_name",
        "schema_stg_name": "utm_campaign",
        "description": "The name of the campaign that the user was in when the event occurred. Maps directly to utm_campaign parameter.",
    },
    "context_campaign_source": {
        "source_name": "context_campaign_source",
        "schema_stg_name": "utm_source",
        "description": "The source of the campaign that the user was in when the event occurred. Maps directly to utm_source parameter.",
    },
    "context_campaign_medium": {
        "source_name": "context_campaign_medium",
        "schema_stg_name": "utm_medium",
        "description": "The medium of the campaign that the user was in when the event occurred. Maps directly to utm_medium parameter.",
    },
    "context_campaign_term": {
        "source_name": "context_campaign_term",
        "schema_stg_name": "utm_term",
        "description": "The term of the campaign that the user was in when the event occurred. Maps directly to utm_term parameter.",
    },
    "context_campaign_content": {
        "source_name": "context_campaign_content",
        "schema_stg_name": "utm_content",
        "description": "The content of the campaign that the user was in when the event occurred. Maps directly to utm_content parameter.",
    },
    "context_library_name": {
        "source_name": "context_library_name",
        "schema_stg_name": "library_name",
        "description": "The name of the library that generated the event.",
    },
    "context_library_version": {
        "source_name": "context_library_version",
        "schema_stg_name": "library_version",
        "description": "The version of the library that generated the event.",
    },
    "context_app_name": {
        "source_name": "context_app_name",
        "schema_stg_name": "app_name",
        "description": "The name of the app that generated the event.",
    },
    "context_app_namespace": {
        "source_name": "context_app_namespace",
        "schema_stg_name": None,  # Not used in schema.yml files
        "description": "The namespace of the app that generated the event.",
    },
    "context_app_version": {
        "source_name": "context_app_version",
        "schema_stg_name": "app_version",
        "description": "The version of the app that generated the event.",
    },
    "context_app_build": {
        "source_name": "context_app_build",
        "schema_stg_name": "app_build",
        "description": "The build of the app that generated the event.",
    },
    "context_device_id": {
        "source_name": "context_device_id",
        "schema_stg_name": "device_id",
        "description": "The ID of the device that generated the event.",
    },
    "context_device_type": {
        "source_name": "context_device_type",
        "schema_stg_name": "device_type",
        "description": "The type of device that generated the event.",
    },
    "context_device_model": {
        "source_name": "context_device_model",
        "schema_stg_name": "device_model",
        "description": "The model of device that generated the event.",
    },
    "context_device_manufacturer": {
        "source_name": "context_device_manufacturer",
        "schema_stg_name": "device_manufacturer",
        "description": "The manufacturer of device that generated the event.",
    },
    "context_device_ad_tracking_enabled": {
        "source_name": "context_device_ad_tracking_enabled",
        "schema_stg_name": "ad_tracking_enabled",
        "description": "Whether or not the device has ad tracking enabled for advertising purposes.",
    },
    "context_device_advertising_id": {
        "source_name": "context_device_advertising_id",
        "schema_stg_name": "advertising_id",
        "description": "The advertising ID of the device that generated the event.",
    },
    "context_timezone": {
        "source_name": "context_timezone",
        "schema_stg_name": "device_timezone",
        "description": "The timezone of the device that generated the event.",
    },
    "context_screen_height": {
        "source_name": "context_screen_height",
        "schema_stg_name": "screen_height",
        "description": "The height of the device screen in pixels.",
    },
    "context_screen_width": {
        "source_name": "context_screen_width",
        "schema_stg_name": "screen_width",
        "description": "The width of the device screen in pixels.",
    },
    "context_screen_density": {
        "source_name": "context_screen_density",
        "schema_stg_name": "screen_density",
        "description": "The density of the device screen in pixels per inch.",
    },
    "context_os_name": {
        "source_name": "context_os_name",
        "schema_stg_name": "device_os_name",
        "description": "The operating system on the device.",
    },
    "context_os_version": {
        "source_name": "context_os_version",
        "schema_stg_name": "device_os_version",
        "description": "The version of the operating system on the device.",
    },
    "context_ip": {
        "source_name": "context_ip",
        "schema_stg_name": "ip",
        "description": "The IP address the event was generated from.",
    },
    "context_locale": {
        "source_name": "context_locale",
        "schema_stg_name": "locale",
        "description": "Locale string for the device generating the event, for example 'en-US'.",
    },
    "context_user_agent": {
        "source_name": "context_user_agent",
        "schema_stg_name": "user_agent",
        "description": "The user agent string of the device generating the event.",
    },
    "context_network_carrier": {
        "source_name": "context_network_carrier",
        "schema_stg_name": "network_carrier",
        "description": "The carrier of the device's network connection.",
    },
    "context_network_cellular": {
        "source_name": "context_network_cellular",
        "schema_stg_name": "network_cellular",
        "description": "Whether or not the device is connected to a cellular network.",
    },
    "context_network_wifi": {
        "source_name": "context_network_wifi",
        "schema_stg_name": "network_wifi",
        "description": "Whether or not the device is connected to a wifi network.",
    },
    "context_network_bluetooth": {
        "source_name": "context_network_bluetooth",
        "schema_stg_name": "network_bluetooth",
        "description": "Whether or not the device is connected to a bluetooth network.",
    },
    "context_page_url": {
        "source_name": "context_page_url",
        "schema_stg_name": "page_url",
        "description": "The URL of the page where the event occurred.",
    },
    "context_page_path": {
        "source_name": "context_page_path",
        "schema_stg_name": "page_url_path",
        "description": "The path of the page where the event occurred.",
    },
    "context_page_search": {
        "source_name": "context_page_search",
        "schema_stg_name": "page_url_query",
        "description": "The the URL search query parameters of the page where the event occurred.",
    },
    "context_page_title": {
        "source_name": "context_page_title",
        "schema_stg_name": "page_title",
        "description": "The title of the page where the event occurred.",
    },
    "context_page_referrer": {
        "source_name": "context_page_referrer",
        "schema_stg_name": "referrer",
        "description": "The URL of the page that referred the user to the page where the event occurred.",
    },
    "context_integrations_name": {
        "source_name": "context_integrations_name",
        "schema_stg_name": "integration_name",
        "description": "The name of the integration that generated the event.",
    },
    "context_integrations_version": {
        "source_name": "context_integrations_version",
        "schema_stg_name": "integration_version",
        "description": "The version of the integration that generated the event.",
    },
    "context_integration_name": {
        "source_name": "context_integration_name",
        "schema_stg_name": "integration_name",
        "description": "The name of the integration that generated the event.",
    },
    "context_integration_version": {
        "source_name": "context_integration_version",
        "schema_stg_name": "integration_version",
        "description": "The version of the integration that generated the event.",
    },
    "context_integrations_appboy": {
        "source_name": "context_integrations_appboy",
        "schema_stg_name": None,
        "description": "A flag indicating the payload of this event should not be sent back to Braze (aka Appboy).",
    },
    "context_traits_email": {
        "source_name": "context_traits_email",
        "schema_stg_name": "email",
        "description": "The email address of the user.",
    },
    "context_message_id": {
        "source_name": "context_message_id",
        "schema_stg_name": None,
        "description": "The message ID of the event. Used by Segment for de-duplication checks.",
    },
    "context_event_triggered_at": {
        "source_name": "context_event_triggered_at",
        "schema_stg_name": None,
        "description": "The time the event was triggered in Vero.",
    },
    "context_event_name": {
        "source_name": "context_event_name",
        "schema_stg_name": None,
        "description": "The name of the event in Vero.",
    },
    "context_event_data_update_payment_details_url": {
        "source_name": "context_event_data_update_payment_details_url",
        "schema_stg_name": None,
        "description": "The URL of the payment details update page.",
    },
    "context_event_data_timestamp": {
        "source_name": "context_event_data_timestamp",
        "schema_stg_name": None,
        "description": "The timestamp of the event in Vero.",
    },
    "context_event_data_reset_url": {
        "source_name": "context_event_data_reset_url",
        "schema_stg_name": None,
        "description": "The URL of the password reset page.",
    },
    "context_event_data_payment_attempt": {
        "source_name": "context_event_data_payment_attempt",
        "schema_stg_name": None,
        "description": "The payment attempt number.",
    },
    "context_event_data_expiry_hours": {
        "source_name": "context_event_data_expiry_hours",
        "schema_stg_name": None,
        "description": "The number of hours until the update payments URL expires.",
    },
    "context_protocols_source_id": {
        "source_name": "context_protocols_source_id",
        "schema_stg_name": "segment_protocols_source_id",
        "description": "If a protocol violation is detected for this instance of the event firing, the ID of the Segment source.",
    },
    "context_protocols_source_name": {
        "source_name": "context_protocols_source_name",
        "schema_stg_name": "segment_protocols_source_name",
        "description": "If a protocol violation is detected for this instance of the event firing, the name of the Segment source.",
    },
    "context_protocols_violations": {
        "source_name": "context_protocols_violations",
        "schema_stg_name": "segment_protocols_violations",
        "description": "If a protocol violation is detected for this instance of the event firing, a list of the protocol violations.",
    },
    "context_protocols_omitted": {
        "source_name": "context_protocols_omitted",
        "schema_stg_name": "segment_protocols_omitted_properties",
        "description": "If this instance of the event fired with unplanned properties (not in tracking plan), a list of the unplanned properties, which are ommitted from the event payload and do not reach thea data warehouse.",
    },
}


# When we find "parent" columns in an event's table in Redshift, we also need
# to include the child column(s) in the schema.yml model for the event.
parent_child_columns_dict = {
    "context_device_model": [
        {"name": "device", "description": "The device generating the event"},
        {
            "name": "device_version",
            "description": "The version of the device generating the event.",
        },
    ],
    "context_page_url": [
        {
            "name": "page_url_host",
            "description": "The host of the page where the event occurred.",
        },
        {
            "name": "is_native_web_view",
            "description": "Whether or not the event was generated from a native web view.",
        },
    ],
    "context_page_referrer": [
        {
            "name": "referrer_host",
            "description": "The hostname of the URL that referred the user to the current page where the event ocurred.",
        }
    ],
}
