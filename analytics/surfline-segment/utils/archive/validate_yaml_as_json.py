import json
import os

import yaml


def parse_json_string(json_string):
    """Checks that a provided JSON string parses to valid JSON."""
    try:
        return json.loads(json_string)
    except:  # noqa: E722
        raise  # exception is raised if their is an issue parsing string


# ---------------- Dev code for testing & local development ----------------
# tracking_plan_yaml_files = [
#     "/Users/gregclunies/Repos/surfline-segment/tracking_plans_yaml/surfline-web.yaml"
# ]
# ---------------- Dev code for testing & local development ----------------

# Get all of the tracking plans that have been changed
HOME = os.getenv("HOME")
github_action_files_path = os.path.join(HOME, "files_modified.json")

with open(github_action_files_path) as f:
    modified_files = json.load(f)

    # Make sure we only use YAML files that are in "tracking_plans_yaml" folder
    tracking_plan_yaml_files = [
        f
        for f in modified_files
        if ".yaml" in f and "tracking_plans_yaml/" in f
    ]

if not tracking_plan_yaml_files:
    print("No tracking plans have been modified.")

for tracking_plan in tracking_plan_yaml_files:
    with open(tracking_plan, "r") as yaml_in:
        tracking_dict = yaml.safe_load(yaml_in)  # Load YAML as dict object

        # Dump dictionary (from YAML) to a JSON string, then parse JSON string
        # to ensure it is valid JSON.
        parse_json_string(json.dumps(tracking_dict, indent=4))

    json_dict = tracking_dict
