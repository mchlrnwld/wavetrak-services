"""
TO BE USED FOR LOCAL DEVELOPMENT ONLY

This script is used to map event ownership and priority (determined by the
business) to the events in the tracking plans. This script should ONLY BE USED
DURING DEVELOPMENT. It should NEVER be used as part of the GitHub actions flow.
"""

from os import listdir
from os.path import isfile, join

import pandas as pd
import yaml


class MyDumper(yaml.Dumper):
    """Custom dumper class to handle the formatting of the YAML output."""

    def increase_indent(self, flow=False, indentless=False):
        return super(MyDumper, self).increase_indent(flow, False)


def main():
    dev_folder = "./dev/ownership_priority"
    tracking_plans = [
        f
        for f in listdir(dev_folder)
        if isfile(join(dev_folder, f)) and f != "ref_ownership_priority.csv"
    ]

    df_owner_priority = pd.read_csv(
        join(dev_folder, "ref_ownership_priority.csv")
    )

    for tracking_plan in tracking_plans:

        with open(join(dev_folder, tracking_plan), "r") as f:
            tracking_plan_name = tracking_plan.replace(".yaml", "")
            yaml_plan_dict = yaml.safe_load(f)
            yaml_plan_dict_updated = (
                yaml_plan_dict.copy()
            )  # Copy plan dict to avoid modifying the original

        for idx, (event, event_updated) in enumerate(
            zip(yaml_plan_dict["events"], yaml_plan_dict_updated["events"])
        ):
            event_name = event["name"]
            event_version = event["version"]

            row = df_owner_priority.loc[
                (df_owner_priority["source"] == tracking_plan_name)
                & (df_owner_priority["event_name"] == event_name)
                & (df_owner_priority["version"] == event_version),
                :,
            ]

            # Rarely, row is empty from csv, skip if the case
            if row.empty:
                pass
            else:
                owner = row["codeowner"].values[0]
                event_updated["labels"]["owner"] = owner
                # priority = row["priority"]
                # event_updated["labels"]["priority"] = priority

        yaml_plan_dict_updated["events"][idx] = event_updated

        with open(join(dev_folder, tracking_plan), "w") as yaml_out:
            yaml.dump(
                yaml_plan_dict_updated,
                yaml_out,
                indent=2,
                Dumper=MyDumper,
                sort_keys=False,
                default_flow_style=False,
                allow_unicode=True,
                encoding=("utf-8"),
            )


if __name__ == "__main__":
    main()
