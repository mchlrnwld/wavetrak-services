"""Parses tracking plans and writes the corresponding dbt files:
    - src_brand_platform.yml
        - Source files have a source *table* entry for every Segment event
          fired by the Segment source. Source tables are raw data!
    - schema.yml
        - Schema files have a staging *model* entry for every Segment event
          fired by the Segment. These staging models clean up the source tables
          (raw data) and rename columns so that our models have:
            - consistent naming conventions
            - standard columns for all events (e.g., `tstamp`, etc.)
    - stg_brand_platform__event_name.sql
        - One staging SQL model per Segment event. These models form the
          foundation for all Segment event data modeling at Surfline.

This automates the very tedious tasks of writing source, schema, and staging
SQL files manually. This would require over 600+ files to be written by an
Analytics Engineer.

References: dbt folder structure and naming convetions used to guide this work
- https://discourse.getdbt.com/t/how-we-structure-our-dbt-projects/355
- https://github.com/dbt-labs/corp/blob/master/dbt_style_guide.md#model-naming
"""

import json
import logging
import os
import re
import sys
from logging.config import fileConfig
from os import listdir
from os.path import isfile, join

import psycopg2
import yaml
from dotenv import load_dotenv

from dbt_columns import parent_child_columns_dict, std_columns_dict
from get_secrets import DB_HOST_URL, DB_NAME, DB_PASSWORD, DB_PORT, DB_USER

# Set environment variables from .env if working locally
load_dotenv()

# Tracking plan names mapped to corresponding Redshift schema
source_redshift_schema_dict = {
    "surfline-web": "surfline",
    "surfline-ios": "surfline_ios",
    "surfline-android": "surfline_android",
    "msw-web": "msw_web",
    "msw-ios": "msw_ios",
    "msw-android": "msw_android",
    "buoyweather-web": "buoyweather",
    "buoyweather-ios": "buoyweather_ios",
    "buoyweather-android": "buoyweatherandroid",
    "fishtrack-web": "fishtrack",
    "fishtrack-ios": "fishtrack_ios",
    "fishtrack-android": "fishtrackandroid",
    "braze": "braze",
    "vero": "vero",
    "customer-io-surfline": "customer_io_surfline",
    "customer-io-buoyweather": "customer_io_buoyweather",
    "customer-io-fishtrack": "customer_io_fishtrack",
    "split-production": "split_production",
}

# Tracking plan names mapped to corresponding brand_platform string
source_brand_platform_dict = {
    "surfline-web": "surfline_web",
    "surfline-ios": "surfline_ios",
    "surfline-android": "surfline_android",
    "msw-web": "magicseaweed_web",
    "msw-ios": "magicseaweed_ios",
    "msw-android": "magicseaweed_android",
    "buoyweather-web": "buoyweather_web",
    "buoyweather-ios": "buoyweather_ios",
    "buoyweather-android": "buoyweather_android",
    "fishtrack-web": "fishtrack_web",
    "fishtrack-ios": "fishtrack_ios",
    "fishtrack-android": "fishtrack_android",
    "braze": "braze",
    "vero": "vero",
    "customer-io-surfline": "customer_io_surfline",
    "customer-io-buoyweather": "customer_io_buoyweather",
    "customer-io-fishtrack": "customer_io_fishtrack",
    "split-production": "split_production",
}


class MyDumper(yaml.Dumper):
    """Custom dumper class. Applies basic formatting to YAML tracking plans."""

    def increase_indent(self, flow=False, indentless=False):
        return super(MyDumper, self).increase_indent(flow, False)

    # Don't use YAML anchors/aliases
    # https://ttl255.com/yaml-anchors-and-aliases-and-how-to-disable-them/
    def ignore_aliases(self, data):
        return True


def connect_to_redshift(db_name, port, user, password, host_url):
    """Establishes a connection with a Redhsift database.

    Args:
        db_name (string): The database name.
        port (string): The port.
        user (string): The user name credential.
        password (string): The user's password.
        host_url (string): The host URL of the database.

    Returns:
        object: A connection to the database.
    """
    conn_string = (
        f"dbname='{db_name}' "
        f"port='{port}' "
        f"user='{user}' "
        f"password='{password}' "
        f"host='{host_url}'"
    )
    try:
        con = psycopg2.connect(conn_string)
        return con

    except psycopg2.DatabaseError as e:
        print(e)
        sys.exit(1)


def get_columns_in_table(schema_name, table_name, con):
    """Gets the columns for a specified table in Redshift.

    Args:
        schema_name (string): The schema the table belongs to.
        table_name (string): The name of the table.
        con (object): The connection to the Redshift database.

    Returns:
        list: A list of the columns in the table.
    """
    try:
        cur = con.cursor()
        cur.execute(
            f"SELECT column_name FROM information_schema.columns WHERE "
            f"table_schema = '{schema_name}' AND table_name = '{table_name}'"
        )
        columns = cur.fetchall()
        con.close()
        columns_list = [item for t in columns for item in t]

        return columns_list

    except psycopg2.DatabaseError as e:
        print(e)
        sys.exit(1)


def snake_case(in_str):
    """Converts a camelCase string to snake_case.

    Args:
        in_str (string): The input string in camelCase format.

    Returns:
        string: A string in snake_case format.
    """
    temp = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", in_str)
    snake = re.sub("([a-z0-9])([A-Z])", r"\1_\2", temp).lower()
    return snake


def segment_case(in_str):
    """Converts a camelCase string to segment_case.

    Args:
        in_str (string): A string in camelCase format.

    Returns:
        string: A string in segment_case format.

    segment_case logic:
        - Insert underscore only where casing switches from lower to UPPER
        - Then, lower() entire string

    Example:
        NSLocalizedFailureReason --> nslocalized_failure_reason
    """
    temp = re.sub("([a-z])([A-Z]+)", r"\1_\2", in_str)
    segment = temp.lower()
    return segment


def camel_case(in_str):
    """Converts a snake_case string to camelCase.

    Args:
        in_str (string): A string in snake_case format.

    Returns:
        string: A string in camelCase format.
    """
    temp = in_str.split("_")
    camel = temp[0] + "".join(ele.title() for ele in temp[1:])
    return camel


def format_source(in_str):
    """Lazy formats a dbt source.yml file using string replacement.

    Args:
        in_str (string): A string with the source.yml file contents.

    Returns:
        string: A string with the *formatted* source.yml file contents.
    """
    out_str = (
        in_str.replace(
            "\n      - name:",
            "\n\n      - name:",  # Separate each table with a blank line
        )
        .replace(
            "\n          - name:",
            "\n\n          - name:",  # Separate each column with a blank line  # noqa E501
        )
        .replace(
            "tables:\n\n      - name:",
            "tables:\n      - name:",  # First table directly under 'tables:'  # noqa E501
        )
        .replace(
            "columns:\n\n          - name:",
            "columns:\n          - name:",  # First column directly under 'columns:'  # noqa E501
        )
        .replace(
            "\n    tables:",
            "\n\n    tables:",  # Add a blank line before 'tables:'
        )
    )

    return out_str


def format_schema(in_str):
    """Lazy formats a dbt schema.yml file using string replacement.

    Args:
        in_str (string): A string with the schema.yml file contents.

    Returns:
        string: A string with the *formatted* schema.yml file contents.
    """
    out_str = (
        in_str.replace(
            "\n  - name:",
            "\n\n  - name:",  # Separate each model with a blank line
        )
        .replace(
            "\n      - name:",
            "\n\n      - name:",  # Separate each column with a blank line
        )
        .replace(
            "models:\n\n  - name:",
            "models:\n  - name:",  # First model directly under 'models:'
        )
        .replace(
            "columns:\n\n      - name:",
            "columns:\n      - name:",  # First column directly under 'columns:'  # noqa E501
        )
        .replace(
            "stg___BRAND_PLATFORM____push_notification_tapped",  # fix this edge case  # noqa E501
            "stg_surfline_ios__push_notification_tapped",
        )
    )

    return out_str


def main():
    """Entry point into script. This script templates dbt source.yml,
    schema.yml, and staging models for all Segment events found in our tracking
    plans.

    The templated dbt files are used in our dbt project (currently outside the
    monorepo) to materialize sources, staging models, basic tests, and
    documentation for our Segmenet events. These materialize models form the
    foundation for all dbt data modeling of our Segment events.
    """
    # Determine if dev or prod, find tracking plans accordingly
    if os.getenv("ENVIRONMENT") == "dev":
        SURFLINE_SEGMENT_DIR = os.getenv("SURFLINE_SEGMENT_DIR")
        SURFLINE_SEGMENT_LOGGING_DIR = SURFLINE_SEGMENT_DIR
        tracking_plan_folder = os.path.join(
            SURFLINE_SEGMENT_DIR, "tracking_plans_yaml"
        )

        # Get list of tracking plan paths
        tracking_plans = [
            os.path.join(tracking_plan_folder, f)
            for f in listdir(tracking_plan_folder)
            if isfile(join(tracking_plan_folder, f)) and f.endswith(".yaml")
        ]

    else:
        HOME = os.getenv("HOME")
        SURFLINE_SEGMENT_DIR = os.getenv("SURFLINE_SEGMENT_DIR")
        SURFLINE_SEGMENT_LOGGING_DIR = os.getenv(
            "SURFLINE_SEGMENT_LOGGING_DIR"
        )

        # Get modified & new files from 'trilom/file-changes-action' GitHub Action  # noqa: E501
        modified_files_json = os.path.join(HOME, "files_modified.json")
        added_files_json = os.path.join(HOME, "files_added.json")

        with open(modified_files_json) as f:
            modified_files = json.load(f)

        with open(added_files_json) as f:
            added_files = json.load(f)

        # Generate a list of the modified tracking plans
        modified_plans = [
            os.path.join(SURFLINE_SEGMENT_DIR, f)
            for f in modified_files
            if "/tracking_plans_yaml/" in f and f.endswith(".yaml")
        ]
        # Generate a list of the added tracking plans
        added_plans = [
            os.path.join(SURFLINE_SEGMENT_DIR, f)
            for f in added_files
            if "/tracking_plans_yaml/" in f and f.endswith(".yaml")
        ]

        # Combine the new and modified tracking plan lists
        tracking_plans = modified_plans + added_plans

    # Exclude the example tracking plan
    tracking_plans = [
        tracking_plan
        for tracking_plan in tracking_plans
        if "example" not in tracking_plan and "embed" not in tracking_plan
    ]

    # Setup logging
    log_file_path = os.path.join(
        SURFLINE_SEGMENT_LOGGING_DIR, "logging-dbt-template.ini"
    )
    fileConfig(log_file_path)
    logger = logging.getLogger(__name__)

    # Set directory where dbt files will be generated
    dbt_codegen_folder = os.path.join(
        SURFLINE_SEGMENT_DIR, "dbt_codegen/segment"
    )

    # Iterate through each tracking plan
    for tracking_plan in tracking_plans:
        tracking_plan_name = tracking_plan.split("/")[-1].replace(".yaml", "")
        logger.info(
            f"Templating dbt files (source.yml, schema.yml, and SQL models) "
            f"for: {tracking_plan_name}"
        )

        # Set read folder to get templates from
        read_folder = os.path.join(
            SURFLINE_SEGMENT_DIR, "dbt_templates/segment"
        )
        # Set write folder where dbt files for each tracking plan are written
        write_folder = os.path.join(
            dbt_codegen_folder,
            tracking_plan_name.replace("-", "_").replace(
                "msw", "magicseaweed"
            ),
        )

        # If the write folder does not exist (new tracking plan), create it
        if not os.path.exists(write_folder):
            os.makedirs(write_folder)

        # Set the template files to use
        template_table = os.path.join(read_folder, "tmplt_table.yml")
        template_model = os.path.join(read_folder, "tmplt_model.yml")
        template_column = os.path.join(read_folder, "tmplt_column.yml")
        template_staging = os.path.join(read_folder, "tmplt_staging.sql")

        # For Web, iOS, and Android tracking plans, use a specific template
        # that already includes pages, screens, tracks, and identifies tables.
        # TODO
        #   - We may replace the logic below later if we handle Segment pages,
        #     screens, tracks, and identifies tables in this script too.
        #   - For now, keep simple.
        if "web" in tracking_plan_name:
            template_source = os.path.join(read_folder, "tmplt_source_web.yml")
            template_schema = os.path.join(read_folder, "tmplt_schema_web.yml")

        elif "ios" in tracking_plan:
            template_source = os.path.join(read_folder, "tmplt_source_ios.yml")
            template_schema = os.path.join(read_folder, "tmplt_schema_ios.yml")

        elif "android" in tracking_plan:
            template_source = os.path.join(
                read_folder, "tmplt_source_android.yml"
            )
            template_schema = os.path.join(
                read_folder, "tmplt_schema_android.yml"
            )

        else:
            template_source = os.path.join(
                read_folder, "tmplt_source_generic.yml"
            )
            template_schema = os.path.join(
                read_folder, "tmplt_schema_generic.yml"
            )

        # Set special variables.
        # We parse the dbt templates for strings like "__SCHEMA_NAME__" and
        # replace with corresponding special variable. Special variables are
        # determined from the tracking plan and event. This pattern/approach is
        # used throughout this script to inject values into the templates.
        __SCHEMA_NAME__ = source_redshift_schema_dict[tracking_plan_name]
        __BRAND_PLATFORM__ = source_brand_platform_dict[tracking_plan_name]
        __BRAND_HIPHEN_PLATFORM__ = tracking_plan_name

        # Initialize a source object using the template
        with open(template_source, "r") as f:
            source = yaml.safe_load(f)

        # Inject __SCHEMA_NAME__, etc. into the source object
        source["sources"][0]["name"] = __SCHEMA_NAME__
        source["sources"][0]["description"] = (
            source["sources"][0]["description"]
            .replace("__BRAND-PLATFORM__", __BRAND_HIPHEN_PLATFORM__)
            .replace("__SCHEMA_NAME__", __SCHEMA_NAME__)
        )

        # If tables already exist in source object (Web, iOS,
        # and Android sources only), set __SCHEMA_NAME__,
        # __BRAND_PLATFORM__, etc. accordingly.
        for table in source["sources"][0]["tables"]:
            table["description"] = (
                table["description"]
                .replace("__BRAND-PLATFORM__", __BRAND_HIPHEN_PLATFORM__)
                .replace("__SCHEMA_NAME__", __SCHEMA_NAME__)
            )

        # Initialize a schema object using the template
        with open(template_schema, "r") as f:
            schema = yaml.safe_load(f)

        # If there are already models spec'd in the schema template
        # (pages/screens, tracks, identifies), then inject __BRAND_PLATFORM__
        # as it applies.
        for idx, model in enumerate(schema["models"]):
            schema["models"][idx]["name"] = schema["models"][idx][
                "name"
            ].replace("__BRAND_PLATFORM__", __BRAND_PLATFORM__)
            schema["models"][idx]["columns"][0]["description"] = schema[
                "models"
            ][idx]["columns"][0]["description"].replace(
                "__BRAND_PLATFORM__", __BRAND_PLATFORM__
            )

        # Load the YAML tracking plan
        with open(tracking_plan, "r") as f:
            yaml_plan = yaml.safe_load(f)

        # Get list of events in tracking plan, excluding Page/Screen viewed
        events = []
        events_raw = yaml_plan["events"]
        for event in events_raw:
            if event["name"] not in ["Page Viewed", "Screen Viewed"]:
                events.append(event)
            else:
                pass

        # Iterate through events in tracking plan
        for event in events:
            # Get event atributes
            event_name = event["name"]
            event_description = event["description"]
            event_version = event["version"]
            properties = event["properties"]
            event_fires_on = event["labels"]["fires_on"]

            # For the event_name, get all versions of the event
            versions_event_name = [
                event for event in events if event["name"] == event_name
            ]

            # For event_name, get a list of event version numbers
            versions = [
                event["version"]
                for event in versions_event_name
                if event["name"] == event_name
            ]

            # Only write dbt files for the latest version of the event
            if event_version == max(versions):
                logging.info(
                    f"Templating event...\n"
                    f"    Tracking plan: {tracking_plan_name}\n"
                    f"    Event name: {event_name}\n"
                    f"    Event version: {event_version}\n"
                    f"    Event fires on: {event_fires_on}\n"
                )

                # Define special variables (e.g., __EVENT_NAME__) for table
                # and model objects
                __EVENT_NAME__ = (
                    event_name.lower()
                    .replace(" ", "_")
                    .replace("-", "")
                    .replace("__", "_")
                    .replace("/", "_")
                    .replace("itunes", "i_tunes")  # Handle bad event names
                    .replace("subscriptionaction", "subscription_action")
                    .replace("inapp", "in_app")
                    .replace("latlon", "lat_lon")
                )
                __TABLE_NAME__ = __EVENT_NAME__
                __TABLE_DESCRIPTION__ = event_description
                __MODEL_NAME__ = (
                    "stg_" + __BRAND_PLATFORM__ + "__" + __EVENT_NAME__
                )
                __MODEL_DESCRIPTION__ = event_description
                __FIRES_ON__ = event_fires_on

                # Initialize a table object using the template
                with open(template_table, "r") as f:
                    table = yaml.safe_load(f)

                # Inject __TABLE_NAME__, etc. into table object
                table["name"] = __TABLE_NAME__
                table["description"] = __TABLE_DESCRIPTION__

                # Initialize a model object using the template
                with open(template_model, "r") as f:
                    model = yaml.safe_load(f)

                # Inject __MODEL_NAME__, etc. into model object
                model["name"] = __MODEL_NAME__
                model["description"] = __MODEL_DESCRIPTION__

                # Connect to redshift
                con = connect_to_redshift(
                    DB_NAME, DB_PORT, DB_USER, DB_PASSWORD, DB_HOST_URL
                )
                # Get a list of the columns in source table from Redshift
                redshift_columns = get_columns_in_table(
                    __SCHEMA_NAME__, __TABLE_NAME__, con
                )
                redshift_columns.sort()

                # Get list of standard columns we use for templating
                std_columns = list(std_columns_dict.keys())
                std_columns.sort()

                # Get list of columns unique to the event being templated
                event_columns = list(properties.keys())
                event_columns.sort()

                # Combine the standard and event columns. Remove duplicates
                columns = list(set(redshift_columns + event_columns))
                columns.sort()

                # Get list of "parent" columns that if they are present in the
                # source table, we know we need to add specific "child" columns
                parent_columns = list(parent_child_columns_dict.keys())

                # Iterate through all columns in the source table
                # - If column in standard columns (NOT event columns), look it
                #   up in in std_columns_dict an set column values accordingly
                # - If column in event columns (NOT standard columns), set dbt
                #   values accordingly (taken from tracking plan)
                # - Else, do nothing
                for column in columns:
                    # If a standard column (not event column)
                    if column in std_columns and column not in event_columns:
                        # Initialize table_column object
                        with open(template_column, "r") as f:
                            table_column = yaml.safe_load(f)

                        # Add name and description to table_column
                        table_column["name"] = std_columns_dict[column][
                            "source_name"
                        ]
                        table_column["description"] = (
                            std_columns_dict[column]["description"]
                            .replace("__SCHEMA_NAME__", __SCHEMA_NAME__)
                            .replace("__TABLE_NAME__", __TABLE_NAME__)
                        )

                        # Append table_column to table
                        table["columns"].append(table_column)

                        # If standard column goes into model too, do this
                        if (
                            std_columns_dict[column]["schema_stg_name"]
                            is not None
                        ):
                            # Initialize model_olumn object
                            with open(template_column, "r") as f:
                                model_column = yaml.safe_load(f)

                            # Add name and description to model_column
                            model_column["name"] = std_columns_dict[column][
                                "schema_stg_name"
                            ]
                            model_column["description"] = (
                                std_columns_dict[column]["description"]
                                .replace("__SCHEMA_NAME__", __SCHEMA_NAME__)
                                .replace("__TABLE_NAME__", __TABLE_NAME__)
                            )

                            # `id` columns must have a unique and not_null test
                            if column == "id":
                                model_column["tests"] = ["unique", "not_null"]

                            # Append model_column to model
                            model["columns"].append(model_column)

                        # If we know the column is a "parent" column
                        if column in parent_columns:
                            # Get the dictionary holding the parent's children
                            parent_dict = parent_child_columns_dict[column]
                            # For each child
                            for child in parent_dict:
                                # Initialize a model_column object
                                with open(template_column, "r") as f:
                                    model_column = yaml.safe_load(f)

                                # Add name and description to model_column
                                model_column["name"] = child["name"]
                                model_column["description"] = child[
                                    "description"
                                ]

                                # Append model_column to model object
                                model["columns"].append(model_column)

                        # If the column is named `context_device_model` or
                        # `context_user_agent`, add a `device_category` column
                        # to the model
                        if (
                            column == "context_device_model"
                            or column == "context_user_agent"
                        ):
                            # Initialize a model_column object
                            with open(template_column, "r") as f:
                                model_column = yaml.safe_load(f)

                            # Add name and description to model_column
                            model_column["name"] = "device_category"
                            model_column[
                                "description"
                            ] = "The device category."

                            # Append model_column to model object
                            model["columns"].append(model_column)

                    # If the column is from the event in the trackling plan
                    elif column in event_columns and column not in std_columns:
                        # Initialize table_column object
                        with open(template_column, "r") as f:
                            table_column = yaml.safe_load(f)

                        # Add name and description to table_column
                        table_column["name"] = segment_case(column).replace(
                            ".", "_"
                        )
                        table_column["description"] = properties[column][
                            "description"
                        ]

                        # Append table_column to table
                        table["columns"].append(table_column)

                        # Initialize model_column object
                        with open(template_column, "r") as f:
                            model_column = yaml.safe_load(f)

                        # `id` columns must have unique and not_null test
                        if column == "id":
                            model_column["tests"] = ["unique", "not_null"]

                        # Add name and description to model_column
                        model_column["name"] = segment_case(column).replace(
                            ".", "_"
                        )
                        model_column["description"] = properties[column][
                            "description"
                        ]

                        # Append model_column to model
                        model["columns"].append(model_column)

                    else:
                        pass

                # Add `pseudo_anonymous_id` column to every model in schema.yml
                # Initialize model_column object
                with open(template_column, "r") as f:
                    model_column = yaml.safe_load(f)

                model_column["name"] = "pseudo_anonymous_id"
                model_column["description"] = (
                    "Defined as `coalesce(anonymous_id, user_id). Use this "
                    "column to resolve anonymous users performing this event "
                    "who later sign in (and then have a user_id) by joining "
                    "pseudo_anonymous_id to user-stitching tables like "
                    "`int_surfline_web_user_stitching`. This lets you build a "
                    "`blended_user_id` which will give you the most accurate "
                    "view of unique users."
                )

                # Append table to source object
                source["sources"][0]["tables"].append(table)

                # Append model to schema object
                schema["models"].append(model)

                # Generate a list of columns that will be set as
                # `__IGNORE_COLUMNS__` variable. Refer to tmplt_staging.sql
                # model code to see how this is used.
                # ignore_columns_list = [
                #     column
                #     for column in redshift_columns
                #     if column not in list(properties.keys())
                # ]
                event_columns_snaked = [
                    segment_case(column) for column in event_columns
                ]
                ignore_columns_list = list(
                    set(redshift_columns).difference(event_columns_snaked)
                )

                # Convert ignore_columns_list to string representing the list.
                # We inject this "string list" into the SQL staging model code
                # using string replacement.
                ignore_list_str = ""

                for column in ignore_columns_list:
                    ignore_list_str += "'" + column + "', "

                __IGNORE_COLUMNS__ = "[ " + ignore_list_str + " '' ]"

                # Initialize a staging object using the template
                with open(template_staging, "r") as f:
                    staging = f.read()

                # Replace "__SCHEMA_NAME__", "__TABLE_NAME__", and
                # "__IGNORE_COLUMNS__" strings in SQL template with variables
                staging = (
                    staging.replace("__SCHEMA_NAME__", __SCHEMA_NAME__)
                    .replace("__TABLE_NAME__", __TABLE_NAME__)
                    .replace("__IGNORE_COLUMNS__", __IGNORE_COLUMNS__)
                    .replace("__FIRES_ON__", __FIRES_ON__)
                )

                # Write staging object to file
                with open(
                    os.path.join(write_folder, __MODEL_NAME__ + ".sql"), "w"
                ) as f:
                    f.write(staging)

        # Dump schema object to schema.yml
        with open(os.path.join(write_folder, "schema.yml"), "w") as f:
            yaml.dump(
                schema,
                f,
                indent=2,
                width=70,
                Dumper=MyDumper,
                sort_keys=False,
                default_flow_style=False,
                allow_unicode=True,
                encoding=("utf-8"),
            )

        # Format schema.yml for readability
        with open(os.path.join(write_folder, "schema.yml"), "r") as f:
            schema_string = f.read()

        # Write formatted schema.yml back to the file
        with open(os.path.join(write_folder, "schema.yml"), "w") as f:
            f.write(format_schema(schema_string))

        # Dump source object to src_brand_platform.yml
        src_file = "src_" + __BRAND_PLATFORM__ + ".yml"
        with open(os.path.join(write_folder, src_file), "w") as f:
            yaml.dump(
                source,
                f,
                indent=2,
                width=70,
                Dumper=MyDumper,
                sort_keys=False,
                default_flow_style=False,
                allow_unicode=True,
                encoding=("utf-8"),
            )

        # Format src_brand_platform.yml for readability
        with open(os.path.join(write_folder, src_file), "r") as f:
            source_string = f.read()

        # Write formatted src_brand_platform.yml back to the file
        with open(os.path.join(write_folder, src_file), "w") as f:
            f.write(format_source(source_string))

    else:
        pass


if __name__ == "__main__":
    main()
