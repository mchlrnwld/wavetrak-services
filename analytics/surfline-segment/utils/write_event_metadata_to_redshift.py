"""Loads Segment event metadata saved in .csvs from S3 to Redshift.csv

Loads Segment event metadata stored in .csv files in AWS S3 and materializes
the data as tables in Redshift. One table for each Segment source. The metadata
can then be joined to modeled Segment data in dbt.

NOTE:
If running this script locally, it is assumed you are running from the ROOT of
this repo!

References:
    https://stackoverflow.com/questions/34983043/how-to-load-data-into-amazon-redshift-via-python-boto3
    https://www.mydatahack.com/moving-data-in-s3-and-redshift-with-python/
    https://dataform.co/blog/import-data-s3-to-redshift
"""

import os
import sys

import psycopg2

from get_secrets import (
    AWS_ACCESS_KEY_ID,
    AWS_SECRET_ACCESS_KEY,
    DB_HOST_URL,
    DB_NAME,
    DB_PASSWORD,
    DB_PORT,
    DB_SCHEMA,
    DB_USER,
    SEGMENT_EVENT_METADATA_S3_BUCKET,
)


def generate_metadata_csv_lst():
    """Generate list of .csv file based on the tracking plans found in repo.csv

    This list of .csv files is then used in other function to fetch the .csv
    files from an S3 bucket, which is populated using the
    `etl_segment_event_label_metadata_csv_to_s3` script.

    Returns:
        csv_lst [list]: List of .csv names in S3 storing Segment event metadata
    """
    HOME = os.getenv("HOME")  # Setup directories for [local-dev] vs. [prod]

    if "runner" not in HOME:
        print("Running locally! HOME dir: {0}\n".format(HOME))
        root_dir = os.getcwd()  # Assumes running from repo root!
        tracking_plan_dir = os.path.join(root_dir, "tracking_plans_yaml")
    elif "runner" in HOME:
        print("Running in GitHub! HOME dir:\n{}".format(HOME))
        root_dir = os.getenv("HOME")
        tracking_plan_dir = os.path.join(  # Known pattern for Github Actions
            root_dir, "surfline-segment/tracking_plans_yaml"
        )
    else:
        exit("HOME directory not found!")

    csv_lst = [
        tracking_plan.rsplit("/", 1)[-1].replace("yaml", "csv")
        for tracking_plan in os.listdir(tracking_plan_dir)
    ]

    return csv_lst


def determine_brand_platform(csv):
    """Returns brand-platform string based on .csv name. Used to name table"""

    if "surfline-web" in csv:
        brand_platform = "surfline_web"
    elif "surfline-ios" in csv:
        brand_platform = "surfline_ios"
    elif "surfline-android" in csv:
        brand_platform = "surfline_android"
    elif "magicseaweed-web" in csv:
        brand_platform = "magicseaweed_web"
    elif "magicseaweed-ios" in csv:
        brand_platform = "magicseaweed_ios"
    elif "magicseaweed-android" in csv:
        brand_platform = "magicseaweed_android"
    elif "surfline-camera-embed" in csv:
        brand_platform = "surfline_camera_embed"
    elif "split-production" in csv:
        brand_platform = "split_production"
    elif "tracking-plan-example" in csv:
        brand_platform = "tracking_plan_example"
    elif "buoyweather-web" in csv:
        brand_platform = "buoyweather_web"
    elif "buoyweather-ios" in csv:
        brand_platform = "buoyweather_ios"
    elif "buoyweather-android" in csv:
        brand_platform = "buoyweather_android"
    elif "fishtrack-web" in csv:
        brand_platform = "fishtrack_web"
    elif "fishtrack-ios" in csv:
        brand_platform = "fishtrack_ios"
    elif "fishtrack-android" in csv:
        brand_platform = "fishtrack_android"
    elif "braze" in csv:
        brand_platform = "braze"
    elif "customer-io-surfline" in csv:
        brand_platform = "customer_io_surfline"
    elif "customer-io-buoyweather" in csv:
        brand_platform = "customer_io_buoyweather"
    elif "customer-io-fishtrack" in csv:
        brand_platform = "customer_io_fishtrack"
    elif "vero" in csv:
        brand_platform = "vero"

    else:
        raise Exception(
            "Unspecified tracking plan present in S3 Bucket.\n"
            "Modify determine_brand_platform() in write_event_metadata_to_redshift.py\n"  # noqa: E501
            "to handle new tracking plan!"
        )

    return brand_platform


def connect_to_redshift(db_name, port, user, password, host_url):
    "This function connects to a specified Redshift cluster."
    conn_string = "dbname='{}' port='{}' user='{}' password='{}' host='{}'".format(  # noqa: E501
        db_name, port, user, password, host_url
    )
    print("...Connecting to Redshift cluster")
    try:
        con = psycopg2.connect(conn_string)
        print("...connection successful")
        return con

    except psycopg2.Error as e:
        print(e)
        sys.exit(1)


def materialize_s3_csv_in_redshift(
    con,
    csv_file,
    schema_name,
    table_name,
    bucket,
    aws_access_key_id,
    aws_secret_access_key,
    delimiter=",",
):
    """A function that materializes a .csv file in S3 as a table in Redshift.

    Args:
        con (string): The connection to Redshift [from con=psycopg2.connect()].
        csv_file (string): The .csv file to materialize as a table.
        table (string): The table name to be materialized in Redshift.
        bucket (string): The S3 bucket where the csv file is stored.
        AWS_ACCESS_KEY_ID (string): The AWS access key ID for auth.
        AWS_SECRET_ACCESS_KEY (string): The AWS secret access key for auth.

    Returns:
        None
    """
    s3_path = bucket + csv_file

    drop_tmp_if_exists = (
        f"DROP TABLE IF EXISTS {schema_name}.tmp_{table_name};"
    )
    create_tmp_if_not_exists = f"CREATE TABLE IF NOT EXISTS {schema_name}.tmp_{table_name} (segment_source varchar(256), event_name varchar(256), version int, owner varchar(256), priority varchar(256));"  # noqa: E501

    # noqa: start ignore
    copy_from_s3_to_tmp = f"COPY {schema_name}.tmp_{table_name} FROM '{s3_path}'\
        CREDENTIALS 'aws_access_key_id={aws_access_key_id};aws_secret_access_key={aws_secret_access_key}'\
        DELIMITER '{delimiter}'\
        ACCEPTINVCHARS EMPTYASNULL COMPUPDATE OFF FORMAT CSV\
        IGNOREHEADER 1;"
    # noqa: end ignore

    drop_output_if_exists = f"DROP TABLE IF EXISTS {schema_name}.{table_name};"
    rename_tmp_to_output = (
        f"ALTER TABLE {schema_name}.tmp_{table_name} RENAME TO {table_name};"
    )

    try:
        cur = con.cursor()
        print(
            f"...Dropping table {schema_name}.tmp_{table_name} (if it exists)"
        )
        cur.execute(drop_tmp_if_exists)
        print(
            f"...Creating table {schema_name}.tmp_{table_name} (if it does NOT exist)"  # noqa: E501
        )
        cur.execute(create_tmp_if_not_exists)
        print(f"...Copying S3 csv to table {schema_name}.tmp_{table_name}")
        cur.execute(copy_from_s3_to_tmp)
        print(f"...Dropping table {schema_name}.{table_name} (if it exists)")
        cur.execute(drop_output_if_exists)
        print(
            f"...Renaming {schema_name}.tmp_{table_name} to {schema_name}.{table_name}"  # noqa: E501
        )
        cur.execute(rename_tmp_to_output)
        con.commit()

    except psycopg2.DatabaseError as e:
        print(e)
        print("Rolling back transaction.")
        sys.exit(1)

    con.close()
    print("...Transaction committed.")
    print("Closed connection to Redshift cluster.\n")


def main():
    """Load csv data from S3 to Redshift."""
    csv_list = generate_metadata_csv_lst()

    for csv in csv_list:
        brand_platform = determine_brand_platform(csv)

        print(f"Processing {brand_platform}")
        table_name = "ref_{}_tracking_plan_metadata".format(brand_platform)

        con = connect_to_redshift(
            DB_NAME, DB_PORT, DB_USER, DB_PASSWORD, DB_HOST_URL
        )

        materialize_s3_csv_in_redshift(
            con,
            csv,
            DB_SCHEMA,
            table_name,
            SEGMENT_EVENT_METADATA_S3_BUCKET,
            AWS_ACCESS_KEY_ID,
            AWS_SECRET_ACCESS_KEY,
        )


if __name__ == "__main__":
    main()

# flake8: noqa
