"""Parses Segment tracking plan event labels for metadata and sends to S3.

Metadata is stored in S3 using .csv files.

Event metadata includes:
    - Event name
    - Event version
    - Event labels (one column per label). We use these labels to assign event
      metadata (ownership, firesOn, userTriggered, etc).

NOTE:
If running this script locally, it is assumed you are running from the ROOT of
this repo!
"""

import os

import pandas as pd
import yaml

from get_secrets import (
    AWS_ACCESS_KEY_ID,
    AWS_SECRET_ACCESS_KEY,
    SEGMENT_EVENT_METADATA_S3_BUCKET,
)


def get_yaml_tracking_plans():
    """Returns a list of the paths to tracking plans found in repo.

    Returns:
        tracking_plans (list): List of paths to tracking plans found in repo.
    """
    HOME = os.getenv("HOME")  # Setup directories for [local-dev] versus [prod]

    if "runner" not in HOME:
        print("Running locally! HOME dir:\n{0}".format(HOME))
        root_dir = os.getcwd()  # Assumes running from repo root!
        tracking_plan_dir = os.path.join(root_dir, "tracking_plans_yaml")
    elif "runner" in HOME:
        print("Running in GitHub! HOME dir:\n{}".format(HOME))
        root_dir = os.getenv("HOME")
        tracking_plan_dir = os.path.join(  # Known pattern for Github Actions
            root_dir, "surfline-segment/tracking_plans_yaml"
        )
    else:
        exit("HOME directory not found!")

    files = os.listdir(tracking_plan_dir)

    tracking_plans = [os.path.join(tracking_plan_dir, file) for file in files]

    return tracking_plans


def parse_event_metadata_to_dataframe(tracking_plan):
    """Parses a tracking plan for metadata and returns metadata as a DataFrame.

    Args:
        tracking_plan (string): The tracking plan to be parsed to DataFrame.

    Returns:
        df: Pandas DataFrame with event name, version, and labels (if any).
    """
    # Parse Tracking plan and popultae lists
    with open(tracking_plan, "r") as yaml_in:
        tracking_dict = yaml.safe_load(yaml_in)
        events = tracking_dict["events"]

        segment_source = []
        event_name = []
        version = []
        owner = []
        priority = []

        for event in events:
            segment_source.append(tracking_dict["tracking_plan_name"])
            event_name.append(event["name"])

            if "version" in event:
                version.append(int(event["version"]))
            else:
                version.append(1)

            if "labels" in event:
                if "owner" in event["labels"]:
                    owner.append(event["labels"]["owner"])
                else:
                    owner.append("unknown")

                if "priority" in event["labels"]:
                    priority.append(event["labels"]["priority"])
                else:
                    priority.append("unknown")

        df = pd.DataFrame(
            list(zip(segment_source, event_name, version, owner, priority)),
            columns=[
                "segment_source",
                "event_name",
                "version",
                "owner",
                "priority",
            ],
        )

        # Cleanup '0' column in Dataframe. Occurs when label dict is empty.
        cols = df.columns
        cols_cleaned = [col for col in cols if col != 0]
        df_final = df[cols_cleaned]

    return df_final


def dataframe_to_s3(
    tracking_plan, df, AWS_ACCESS_KEY_ID, aws_secret_access_key
):
    """Sends dataframe to S3 bucket.

    Args:
        tracking_plan (string): The tracking plan associated with the DataFrame
        df (pandas.DataFrame): Dataframe with event metadata
        AWS_ACCESS_KEY_ID (string): The AWS access key ID for auth.
        aws_secret_access_key (string): The AWS secret access key for auth.

    Returns:
        None
    """
    # Set up the path to the S3 bucket where metadata csv will be saved
    csv_filename = tracking_plan.rsplit("/", 1)[-1].replace("yaml", "csv")
    csv_file_path = SEGMENT_EVENT_METADATA_S3_BUCKET + csv_filename

    print(
        "Writing {0} Segment event metadata to S3.".format(
            csv_filename.replace(".csv", "")
        )
    )

    # Write to S3
    df.to_csv(
        csv_file_path,
        index=False,
        header=True,
        storage_options={
            "key": AWS_ACCESS_KEY_ID,
            "secret": aws_secret_access_key,
        },
    )


def main():
    """Parse tracking plans for event metadata and send to S3.

    Metadata is saved in S3 as .csv files.
    """
    tracking_plans = get_yaml_tracking_plans()

    for tracking_plan in tracking_plans:
        df = parse_event_metadata_to_dataframe(tracking_plan)
        dataframe_to_s3(
            tracking_plan, df, AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY
        )


if __name__ == "__main__":
    main()
