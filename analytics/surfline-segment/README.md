# surfline-segment

This directory contains the all Wavetrak event Tracking Plans ([as found in Segment](https://app.segment.com/surfline/protocols/tracking-plans)), defined using `YAML`. These tracking plans are the spec for all pages (screens on mobile), events, properties, and user traits we track with Segment.

This directory is the *only way* to add or update a tracking plan (via Pull Requests). Each application Segment source (e.g., `surfline-web`) has a corresponding tracking plan YAML file in [`/tracking_plans_yaml`](./tracking_plans_yaml).

## Table of Contents
  - [Process](#process)
  - [Branch Naming](#branch-naming)
  - [Tracking Plan Format](#tracking-plan-format)
  - [Segment's Config API](#segments-config-api)
  - [Schemas & Tests](#schemas--tests)
    - [Schemas](#schemas)
    - [Tests](#tests)
  - [When things go wrong 🤦‍♀️ 🤦‍♂️](#when-things-go-wrong-️-️)
  - [Segment Resources](#segment-resources)

## Process
Before you make a Pull Request or write new/refactored tracking code, ***review these [internal docs outlining the process to follow](https://wavetrak.atlassian.net/wiki/spaces/AN/pages/2015363520/Segment+Tracking+Surfline#Adding-%26-Updating-Segment-Events)*** (ideation to implementation) when adding a new or updating an existing tracking event. This process ensures input from Product, Engineering, and Analytics. This process minimizes bug occurence and ensures tracking code and data meets the business needs. Please follow it!

If you need to create an *entirely new tracking plan* (very rare), contact the [Analytics team](https://surfline.slack.com/archives/CSGTPLHN0) or make an [Analytics Task](https://wavetrak.atlassian.net/secure/RapidBoard.jspa?rapidView=77) in JIRA.

## Branch Naming
Please use a format starting with the JIRA ticket # and a description of the update/addition.
```bash
git checkout -b AN-3280/make_surfline-segment_accessible_to_engineering
```
This let's us tie work back to tickets and vice versea. If you don't have a ticket, you need to start one and  follow the process linked above!

## Tracking Plan Format
The tracking plan controls 3 major concepts (Pages/Screens, Events, and User Traits) that are vital to our Segment implementation. A [`tracking-plan-example.yaml`](./tracking_plans_yaml/tracking-plan-example.yaml) is included in this directory to show the structure of how events are spec'd. It is also shown below.

***Concerning Style:*** We use the style found in the [YAML Spec](https://yaml.org/spec/1.2/spec.html). Fix style errors when you see them!

[Click here](https://app.segment.com/surfline/protocols/tracking-plans/rs_1wayvJbPiEZyIKuskd7cKJ3L71D?type=Track) to see the example tracking plan below in the Segment UI.
```yaml
# You can see this tracking plan in the Segment UI here: https://app.segment.com/surfline/protocols/tracking-plans/rs_1wayvJbPiEZyIKuskd7cKJ3L71D?type=Track
tracking_plan_id: rs_1wayvJbPiEZyIKuskd7cKJ3L71D
tracking_plan_name: tracking-plan-example

# Global spec for all Segment call types (e.g., track(), identify(), group(), etc.)
# DO NOT use without consulting the Analytics engineering team first.
global:
  context: {}
  traits: {}
  properties: {}

events:  # Spec for track() events and their properties
  - name: Test Event 1  # Name of the event
    description: This is test Event 1. # Always include event descirption
    version: 1
    labels:
      owner: '@Surfline/squad-analytics' # Always include event owner (your squad's GitHub CODEOWNERS name)
      priority: 2       # Always include event priority (1-3). Consult with Analytics eng team if you don't know.
    properties:
      property_1:
        description: A string property.  # Always include property description
        type: string
      property_2:
        description: An integer property.
        type: integer
      property_3:
        description: A boolean property.
        type: boolean
      property_4:
        description: An array property (no nesting). Data in array must be same type.
        type: array
      property_5:
        description: An array property with nested items.
        type: array
        items:
          type: object # When nesting items, the type must be object.
          properties:
            nested_property_1:
              description: The 1st nested property.
              type: string
            nested_property_2:
              description: The 2nd nested property.
              type: string
          required: # Required properties in the nested object
            - nested_property_2
    required:  # List the properties that are required when the event fires
      - property_1

  - name: Test Event 2
    description: This is test Event 2.
    version: 1
    labels:
      owner: '@Surfline/squad-analytics'
      priority: 2
    properties:
      property_1:
        description: A number property (e.g., 9.99).
        type:
          - number
          - 'null'  # You can specify if null values are allowed
      property_2:
        description: An enum property.
        type: string
        enum:  # Enum properties let you specify a list of valid values.
          - one
          - two
          - three
      property_3:
        description: A regex pattern property.
        type: string
        pattern: '[A-Z]'
      property_4:
        description: An object property.
        type: object  # Object properties let you specify nested properties.
        properties:
          key_1:
            description: The 1st key in the object dictionary.
            type: string
          key_2:
            description: The 2nd key in the object dictionary.
            type: number
        required:  # Required properties in the object dictionary. Must be list (even if empty).
          - key_1
      property_5:
        description: A date-time property.  # All timestamps must be ISO-8601 date strings. Use UTC timezone.
        type: string
        format: date-time
    required:
      - property_1
      - property_3
      - property_5

  - name: Test Event 3
    description: This is test Event 3.
    version: 1  # OLD version of Event 3. Event versions only apply for iOS and Android to handle different app versions when an event spec is updated.
    labels:
      owner: '@Surfline/squad-analytics'
      priority: 2
    properties:
      property_1:  # If no `type:` is specified, the data type is set to 'any' in Segment. Only allowed for OLD event versions!
        description: A string property.
      property_2:
        description: An integer property.
        type: integer
    required: []  # Specifies no required properties

  - name: Test Event 3
    description: This is test Event 3.
    version: 2  # NEW version of Event 3. Event versions only apply for iOS and Android to handle different app versions when an event spec is updated.
    labels:
      owner: '@Surfline/squad-analytics'
      priority: 2
    properties:
      property_1:
        description: An string property.
        type: string
      property_2:
        description: An integer property.
        type: integer
    required:
      - property_1

identify:  # Spec for identify() calls
  traits:  # User traits that can be used when firing identify()
    properties:
      user_trait_1:
        description: A user trait.
        type: string
      user_trait_2:
        description: Another user trait.
        type: boolean
      user_trait_3:
        description: One more user trait.
        type:
          - number
          - 'null'
    required:  # User traits that must be set in an identify() call
      - user_trait_2

group:  # Spec for group() calls (not used at Wavetrak)
  properties: {}
  context: {}
  traits: {}


```

## Segment's Config API

We leverage Segment's [Config API](https://segment.com/docs/config-api/) to sync the tracking plan YAML files in this directory to Segment's servers when updates are made. See Segment's docs for further details on [API use](https://reference.segmentapis.com/#c4647e3c-fe1b-4e2f-88b9-6634841eb4e5) (click `Tracking Plans` in the sidebar).

Although the Segment API expects JSON for requests and responses, we use YAML to make our tracking plans human  readable. The [Github Actions](./.github/workflows) in this directory:
- Check that the YAML tracking plans convert to valid JSON.
- Sync updated tracking plan(s) to Segment servers once a PR is approved and merged to `main` branch.

You just need to make a PR and get approval!

## Schemas & Tests
### Schemas
All events, pages/screens, and identify calls have a standard schema (see API docs for schema structure) to be used when synced to the Segment Config API. These schema's have been codified in the JSON files found in the [`/schema`](./schema) folder in this directory. You will also find the schema for the simplified YAML tracking plans in this folder.

### Tests
Test cases:
- Every event has description.
- Every event has owner.
- Every event has priority. Accepted values = 1 | 2
    - 1 = Fix ASAP.
    - 2 = Document and BACKLOG
- Every property has defined data type specified and is a valid type.
- Every property has description [disable to start]
- Every event has a required object (empty acceptable)
- If version > 1, check that a previous version already exists.

## When things go wrong 🤦‍♀️ 🤦‍♂️
In rare cases, a sync to Segment's Servers will fail. You can tell this occurs when a [workflow status](https://github.com/Surfline/surfline-segment/actions) shows an ❌. Reach out to Analytics Engineering in [#analytics]() slack channel for help to resolve!

## Segment Resources
- [Surfline Segment Tracking](https://wavetrak.atlassian.net/wiki/spaces/AN/pages/2015363520/Surfline+Segment+Tracking) (internal docs)
    - Segment [standardized Event names](https://wavetrak.atlassian.net/wiki/spaces/AN/pages/2015363520/Surfline+Segment+Tracking#Standardized-Events-in-Segment)
    - Surfline [naming conventions](https://wavetrak.atlassian.net/wiki/spaces/AN/pages/2015363520/Surfline+Segment+Tracking#Naming-Conventions-in-Segment).
    - [Common Fields/Properties](https://wavetrak.atlassian.net/wiki/spaces/AN/pages/2015363520/Surfline+Segment+Tracking#Common-Fields%2FProperties) in Segment
- [The Protocols Tracking Plan](https://segment.com/docs/protocols/tracking-plan/create/)
- [Data Collection Best Practices](https://segment.com/docs/protocols/tracking-plan/best-practices/)
