# Getting started with e2e & visual diff testing

The following document serves as a guide for engineering squads to configure end-to-end and visual difference testing in web applications. This is a living document that is subject to change as our CI evolves and we migrate to the monorepo structure. This document is written in the context of the monorepo, project configuration and workflow integrations will differ for projects outside of the monorepo.

## Account access

Someone on the engineering team will need to invite you to both the Cypress and Percy (Browserstack) Surfline/Wavetrak organizations. With these accounts, you will be able to access application dashboards allowing you to view the Cypress and Percy application dashboards. Tests can be authored without accounts, but projects will need to be configured if test runs are to be recorded.

## Create Testing Projects

- Application name
  - [Find your application name](https://github.com/Surfline/wavetrak-services/blob/master/architecture/proposals/monorepo/monorepo.md#updated-service-names)
  - This name will be used for establishing projects in Cypress and Percy
- e2e
  - [Create a project](https://percy.io/organizations/c4f03f3e/projects/new) in Cypress using your app name
  - Select `GitHub Actions` as your CI Provider
  - Store both the `projectId` and `record key` for use later in the configuration
- Visual Diff
  - [Create a project](https://percy.io/organizations/c4f03f3e/projects/new) in Percy using your app name
  - Store the Project token (`PERCY_TOKEN`) for use later in the configuration

## Add project keys to param store

The project-specific keys for both Cypress and Percy need to be added to the `surfline-dev` account AWS Parameter Store with the following naming configuration:

```
/test/<parent-directory>/<working-directory>/CYPRESS_RECORD_KEY
/test/<parent-directory>/<working-directory>/PERCY_TOKEN

Example:
/test/packages/quiver-react/CYPRESS_RECORD_KEY
/test/packages/quiver-react/PERCY_TOKEN
```

## Installing dependencies

In your local web application, you will need to install the following npm packages.

```sh
# Install dotenv
npm i -D dotenv

# Install start-server-and-test for later use in CI
npm i -D start-server-and-test

# Install Storybook
npm i -D @storybook/addon-actions @storybook/addon-essentials @storybook/addon-links @storybook/react

# Install Cypress
npm i -D cypress cypress-dotenv

# Install Percy
npm i -D @percy/cli @percy/cypress @percy/storybook
```

## Configuring scripts

Update the `package.json` for the web app you intend to test. The most critical script to include is `test:e2e`. This is what the CI/CD contract has defined for running e2e tests. All other scripts are to aid in development with Storybook, Cypress and Percy.

Add the following scripts to the scripts property:

```json
{
  "scripts": {
    "storybook": "start-storybook -p 6006",
    "storybook:build": "build-storybook",
    "test:e2e": "RUNNER=self-hosted ../../scripts/tests-e2e.sh --e2e --e2e-storybook --diff-storybook",
    "test:e2e:open": "cypress open --config-file tests/cypress.app.json",
    "test:e2e:run": "cypress run --headless --browser chrome --config-file tests/cypress.app.json",
    "test:storybook:open": "cypress open --config-file tests/cypress.storybook.json",
    "test:storybook:run": "cypress run --headless --browser chrome --config-file tests/cypress.storybook.json"
  }
}
```

NOTE: `test:e2e:run:record` script will fail in the GitHub workflow until we resolve localhost communicating with upstream services. Until then, change this section of the command from `cypress run` to `CYPRESS_BASE_URL=https://staging.surfline.com cypress run`. This will point the tests at Staging. This command can also be commented out in the `tests/run.sh` script.

If self-hosted runners are needed for accessing services within our network, ensure `RUNNER=self-hosted` is added to the start of the `test:e2e` script. If this is not added, the services will not be available when starting up a localhost to test from within the workflow. 

## Shell script for CI/CD

When CI/CD is run it will attempt to execute `test:e2e` in the `package.json` scripts within the targeted app. This script should reference the following shell script [`/scripts/tests-e2e.sh`](../../scripts/tests-e2e.sh).

Example: `"test:e2e": "RUNNER=self-hosted ../../scripts/tests-e2e.sh --e2e --e2e-storybook --diff-storybook"`

The above script will call the `tests-e2e` shell script with Cypress, Storybook Visual Diff testing, and Storybook testing with Cypress all enabled.

## Configuring Storybook

Add the following files to your web application.

- `.storybook/main.js`
  - Main configuration file [More info](https://storybook.js.org/docs/react/configure/overview)
  - Configure webpack loaders needed for Storybook preview page
  - [KBYG example](https://github.com/Surfline/surfline-web/blob/288d9603733605b018720f3a01385cbefdcf0722/kbyg/.storybook/main.js)
- `.storybook/preview.js`
  - Control story rendering [More info](https://storybook.js.org/docs/react/configure/overview#configure-story-rendering)
  - [KBYG example](https://github.com/Surfline/surfline-web/blob/288d9603733605b018720f3a01385cbefdcf0722/kbyg/.storybook/preview.js)

## Configuring Cypress

Add `tests/cypress.app.json` with the following config. This allows for testing the web application locally. Cypress tests for the web application should be written here `tests/integration/app`.

```json
{
  "baseUrl": "http://localhost:8080",
  "chromeWebSecurity": false,
  "numTestsKeptInMemory": 5,
  "projectId": "<cypress-project-id>",
  "retries": 2,
  "video": true,
  "viewportHeight": 768,
  "viewportWidth": 1024,
  "waitForAnimations": true,
  "fixturesFolder": "tests/fixtures",
  "integrationFolder": "tests/integration/app",
  "pluginsFile": "tests/plugins/index.js",
  "screenshotsFolder": "tests/screenshots",
  "supportFile": "tests/support/index.js",
  "videosFolder": "tests/videos",
  "env": {
    "<env-var-exposed-to-cypress>": "<value>"
  }
}
```

Add `tests/cypress.storybook.json` with the following config. This allows for testing the configured storybook bookmarked component states. If you want to write Cypress tests for Storybook stories, write them in `tests/integration/components`. If no tests are written, Percy will still run visual diffs across all of your Storybook bookmarks automatically.

```json
{
  "baseUrl": "http://localhost:6006",
  "chromeWebSecurity": false,
  "numTestsKeptInMemory": 5,
  "projectId": "<cypress-project-id>",
  "retries": 1,
  "video": true,
  "viewportHeight": 768,
  "viewportWidth": 1024,
  "waitForAnimations": true,
  "fixturesFolder": "tests/fixtures",
  "integrationFolder": "tests/integration/components",
  "pluginsFile": "tests/plugins/index.js",
  "screenshotsFolder": "tests/screenshots",
  "supportFile": "tests/support/index.js",
  "videosFolder": "tests/videos",
  "env": {}
}
```

### ESLint config specific to Cypress

Add `tests/.eslintrc` with the following config. This allows the Cypress globals to be used.

```json
{
  "extends": [
    "@surfline/eslint-config-web/base",
    "@surfline/eslint-config-web/react"
  ],
  "globals": { "Cypress": "readonly", "cy": "readonly", "context": "readonly" }
}
```

### Configuring plugins

Add `tests/plugins/index.js` with the following config. This allows Cypress to access environment variables during local development defined in a `.env` file.

```js
/// <reference types="cypress" />
import dotenvPlugin from 'cypress-dotenv';

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (_, config) => {
  config = dotenvPlugin(config); // eslint-disable-line no-param-reassign

  return config;
};
```

### Configuring support

Add `tests/support/index.js` with the following config. This makes visual diff testing with Percy available while testing with Cypress and imports any commands defined within `tests/support/commands.js`.

```js
/// <reference types="cypress" />
import "@percy/cypress";
import "./commands";
```

Add `tests/support/commands.js` with the following config. This configured and abstraction for visual difference testing with Cypress and Percy.

```js
/// <reference types="cypress" />
Cypress.Commands.add("visualDiff", (config) => {
  const { name, ...rest } = config;
  cy.percySnapshot(name, { rest });
});
```

Here is an example showing how the previous `visualDiff` command can be implemented.

```js
cy.visualDiff({
  name: "Cam Rewind: Default state",
});
```

## Configuring Percy

Create `.percy.js` in the root of your project with the following configuration:

```js
require("dotenv").config();

module.exports = {
  version: 2,
  snapshot: {
    widths: [375, 1280],
    minHeight: 1024,
    percyCSS: `.percy-hide { visibility: hidden; }`,
  },
  upload: {
    files: "**/*.{png,jpg,jpeg}",
    ignore: "",
  },
  // Include if you want to limit the amount of stories processed automagically for visual diffs
  // exclude can also be used as a property - https://docs.percy.io/docs/storybook#configuration
  storybook: {
    include: ["MyStory"],
  },
};
```

The `percyCSS` prop is used for setting CSS specific to visual difference testing. This CSS will be added when diffs are run. Check the [Percy Configuration](https://docs.percy.io/docs/cli-configuration) documentation for more details.

## Running tests

The following list outlines the usage of scripts in the `package.json`.

- `npm run storybook` - Start Storybook server in dev mode
- `npm run test:e2e` - Run and record e2e tests with visual diff in CI, requires AWS CLI with access to parameter store. Includes the following options:
  - `--e2e` - Runs and records Cypress tests against local app, including Percy visual diff if called within tests
  - `--e2e-storybook` - Runs and records Cypress tests against Storybook story tests
  - `--diff-storybook` - Runs and records Percy visual difference tests against Storybook stories
- `npm run test:e2e:open` - Open Cypress testing interface for testing the web app locally. Useful when creating or debugging web app tests locally.
- `npm run test:e2e:run` - Run Cypress tests against local web app. This does not record tests or run visual diff.
- `npm run test:storybook:open` - Open Cypress testing interface for testing the Storybook stories locally. Useful when creating or debugging Storybook tests locally.
- `npm run test:storybook:run` - Run Cypress tests against local Storybook stories. This does not record tests or run visual diff.

## Writing tests

After configuring your project, running both `npm run test:e2e:open` and `npm run test:storybook:open` will create the additional files for Cypress to run. It will add sample test files within `tests`. These tests can be removed and replaced with tests specific to your application. Application tests will be exectured from `tests/integration/app`. Storybook tests will be executed from `tests/integration/components`.
