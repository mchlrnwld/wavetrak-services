# Crowd Data and Smart Highlights Pipeline Architecture

## What are we doing?

We are creating a production system which generates crowd counts as well as highlight clips for various live cameras. We are taking pieces of the `prototype-ml-cam` pipeline (which has code to do crowd counting, highlight clipping, and frame extraction) and turning it into a clear system which gives consistent output and has clear testing environments. Once all is complete, you will be able to hit a `cameras-service` endpoint to receive highlight clips as well as crowd counts for the specified camera.

## What problem are we solving?

Currently, we are able to get crowd counts and highlight clips for various live cameras, yet the system that produces these results is disorganized, has no testing environments, is not cost effective, and is scattered across production as well as development environments. Also, there is no clear way to get crowd counts or highlight clips for a given camera; we store output in S3, but one can not reliably hit an endpoint to receive consistent results. Essentialy, the system is a prototype and not production grade.

## What impact does this have on the business?

Among the many new features Surfline will have with Ben’s 2021 product roadmap, giving users an active count of the crowd in the water as well as showing cam highlights is of great importance. As Ben mentioned in his presentation, when someone discusses their surf session, they will mention something about the crowd 90% of the time. Also mentioned by Ben in his presentation, when users watch live cameras, they are only seeing good waves about 10% of the time. Hence, having reliable/quick/easy access to crowd counts and highlight clips for cameras is important for the engineering team to implement Ben’s new features. In part, creating a way for our team to easily access these data will allow the new crowd counting and highlight clip tools to be built with ease. This impacts the business because Surfline will now have 2 new cutting edge products which they can use to drive subscription expansion.

## Architecture Diagram

This architecture diagram visualizes what is discussed in the  sections.

![Alt text](../images/crowd-data-and-highlights/crowd-data-and-highlights-pipeline.png?raw=true "Crowd Data and Smart Highlights Architecture")

## What is the solution?

We are going to want to produce crowd counts and highlight clips in real-time (It is not exactly real-time as we are using rewinds as our input, hence it will be about ten minutes behind real-time). Therefore, there needs to be a pipeline which is triggered consistently upon a live clips generation. To be clear, here is the goal we are looking to achieve:

- **Given a camera's most recent clip as input (10 minute rewind), output a crowd count for every minute of the clip as well as a highlight reel of the clips best waves. One should be able to access this data through the `cameras-service`.**

Below, we will discuss the steps needed to carry out this process:

 1. Clip lands in `sl-live-cam-archive-{env}` S3 bucket. This bucket contains 10 minute rewind clips for all Surfline live cameras.

 2. `wt-frame-extractor-and-highlights-trigger-{env}` lambda function is triggered:

    1. This lambda function will combine the logic of [this](https://github.com/Surfline/wavetrak-services/tree/master/tools/cam_thumbnail_generator) function with [this](https://github.com/Surfline/wavetrak-services/pull/4044) function. Essentially, the first lambda function generates stills from an mp4 video. The second lambda function submits a batch job which makes a highlight clip for the specified rewind. Once both functions are combined, the logic should be as such:

       - In the **ENV** variables of the lambda there will be a list of approved **crowd count** cams. If the rewind is coming from a cam we want crowd counts for it should:

         1. Check if it is daylight at the spot for the given rewind, if so, continue, else move out of this block

         2. Create 1 still per minute of video (10 stills because rewinds are 10 minutes long)

         3. Add the generated stills to the `sl-cam-thumbnails-{env}/high-frequency` bucket

         4. For each still (see if you can do a batch insert, so we only have to make 1 request instead of 10), **POST** the following to the `CameraCrowds` collection in Mongo through the `cameras-service`:

            - ```
              POST /cameras/crowds/:cameraId/:timestamp
              {
                "timestamp": timestamp,
                "stillObjectKey": string
              }
              ```

            - The `CameraCrowds` collection will have the following schema once all data is available for that still:

              ```
              {
                "id": string,
                "timestamp": timestamp,
                "cameraId": string,
                "stillObjectKey": string,
                "annotatedObjectKey": string,
                "crowdCount": int,
                "visibility": {
                  "clear": float,
                  "glare": float,
                  "fog": float,
                  "rainblur": float
                }
              }
              ```

       - In the **ENV** variables of the lambda there will be a list of approved **smart highlights** cameras. If the rewind is coming from a cam we want smart highlights for it should:

         1. Check if it is daylight at the spot for the given rewind, if so, continue, else move out of this block

         2. Submit the `wt-highlights-job-{env}` batch job for the current rewind

       - At the very end, do the following (even if the rewind is coming from a camera we did **crowd counts** and/or **smart highlights** for). Note, the current implementation of this lambda already carries out the below (see [here](https://github.com/Surfline/wavetrak-services/tree/master/tools/cam_thumbnail_generator)):

         1. Generate a still for the rewind

         2. Upload it to the `sl-cam-thumbnails-{env}` bucket

         3. **POST** the following to the `CameraRecording` collection in Mongo through the `cameras-service`:

            - ```
              POST /cameras/recording/id/:cameraId
              {
                "alias": string,
                "startDate": timestamp,
                "endDate": timestamp,
                "recordingUrl": string,
                "thumbLargeUrl": string,
                "thumbSmallUrl": string,
                "resolution": string
              }
              ```

            - The `CameraRecording` collection will have the following schema once all data is available:

              ```
              {
                "cameraId": string,
                "alias": string,
                "startDate": timestamp,
                "endDate": timestamp,
                "recordingUrl": string,
                "thumbLargeUrl": string,
                "thumbSmallUrl": string,
                "resolution": string,
                "highlights": {
                  "url": string,
                  "thumbUrl": string,
                  "gifUrl": string
                }
              }
              ```

    2. Upon adding the stills to the `sl-cam-thumbnails-{env}/high-frequency` bucket, we will use SNS to send a message to an SQS queue (`wt-crowd-count-processing-queue-{env}`). The message will contain the path to the still in `sl-cam-thumbnails-{env}/high-frequency`

       **Things to note:**

       - As stated, we are going to check for **crowd count** and **smart highlight** cams using an **ENV** variable. It's important to note that this is temporary. Later down the line we will add an option to the CMS to do this. Also, we could think about adding this to the `spots-api` as it would make it easier to add/remove new cameras.
       - This lambda function will be monitored by `NewRelic`, and will send alerts if an error is thrown. More specifically, it will alert if the **POST** requests do not return 200, and/or if the `wt-highlights-job-{env}` errors on submit
       - Develop this function [here](https://github.com/Surfline/wavetrak-services/tree/master/tools/cam_thumbnail_generator), as the infrastructure is already set up
       - The `CameraCrowds` collection does not yet exist in Mongo. Hence, we will need to update the `cameras-service` to enable access to it

3. Crowd counting service initiates and/or Highlights batch job submitted

   - Crowd counting service initiates:
     - We will have an ECS cluster (`wt-gpu-cluster-{env}`) with a service (`wt-crowd-count-worker-service-{env}`) listening to our SQS queue (`wt-crowd-count-processing-queue-{env}`)
     - When a new message arrives on the SQS queue, our service (which will look similar to [**prototype-ml-frames-worker**](https://github.com/Surfline/prototype-ml-cam/tree/master/prototype-ml-frames-worker)) will begin to burn down the queue
     - The functionality of the `wt-crowd-count-worker-service-{env}` will be:
       1. Download frame from S3 (SNS message in the SQS queue will contain the path to the image which will lie in `sl-cam-thumbnails-{env}/high-frequency`)

       2. Annotate the frame, get detection counts, get visibility of the frame

       3. Add the annotated frame to S3 at `wt-annotated-frames-{env}`

       4. Through the `cameras-service`, **PUT** the detection count result, visibility ratings, and path to the annotated frame into the `CameraCrowds` collection, using the `timestamp` and `cameraId` to access the specific record:

          - ```
            PUT /cameras/crowds/:cameraId/:timestamp
            {
              "annotatedObjectKey": string,
              "crowdCount": int,
              "visibility": {
                "clear": float,
                "glare": float,
                "fog": float,
                "rainblur": float
              }
            }
            ```

     **Things to note:**

     - The ECS cluster will contain a `g4dn.xlarge` GPU per instance. The cluster should auto-scale when a message is on the queue and no GPU instances are available.
     - The `wt-crowd-count-worker-service-{env}` will perpetually listen to the SQS queue
     - The service will need to be scaled up/down depending on the queue size
     - `NewRelic` will send an alert if there is an error thrown in the `wt-crowd-count-worker-service-{env}` and/or if the **PUT** request does not return 200
     - As mentioned before, there is no service setup yet to access the `CameraCrowds` collection. The **PUT** needs to be implemented within the `cameras-service`, along with **POST** and **GET**

   - Highlights batch job submitted:
     - The highlights batch job is submitted with the path to the original clip inside of `sl-live-cam-archive-{env}`
     - The batch job will essentially be the same as [this](https://github.com/Surfline/surfline-labs/tree/master/camera-sensor/predict)
     - The batch job will consist of a **job definition** (`wt-highlights-job-{env}`) and a **job queue** (`wt-highlights-job-queue-{env}`). The **job queue** is where the batch jobs submitted from the `wt-frame-extractor-and-highlights-trigger-{env}` will wait for processing. AWS will spin up the needed compute environments to process the job. The **job definition** will contain the source code and the command line Python command to initiate the job.
     - The batch job will:
       1. Download the mp4 video

       2. Extract frames

       3. Run inference

       4. Upload the highlight video to S3 (`wt-highlights-{env}/clips`), along with a still of the highlight (`wt-highlights-{env}/stills`) and a gif (`wt-highlights-{env}/gifs`)

       5. Through the `cameras-service`, **PUT** the following into the `CameraRecording` collection using the `cameraId`, `startDate`, and `endDate` to access the specific record:

          - ```
            PUT /cameras/recording/:recordingId
            {
              "highlights": {
                "url": string,
                "thumbUrl": string,
                "gifUrl": string
              }
       	    }
            ```

     **Things to note:**
     
     - `NewRelic` will send an alert if there an error is thrown while running the `wt-highlights-job-{env}`
     - **PUT** functionality for the `CameraRecording` collection is not implemented within the `cameras-service`, hence it must be set up

## Development Environment

There are many different parts to this framework that need to be monitored and tested, consistently and reliably. Assume that everything mentioned above will exist in a production environment. This means all naming conventions will end in `-prod` and everything will exist in the `surfline-prod` AWS account. To create the most efficient testing framework, to the best of our abilities, each service should be mimicked in a **sandbox** as well as **staging** environment. This means all naming conventions will end in `-sandbox|-staging` and all testing components will exist in the `surfline-dev` AWS account. Everything will be replicated in the testing environment except for a newly added `wt-copy-rewind-clips-prod` lambda function. This function will feed data into the testing environment by adding rewinds from `sl-live-cam-archive-prod` into the `sl-live-cam-archive-sandbox|staging` bucket, as it is the entry-point to our pipeline. A quick note, this testing environment will receive far fewer data than the production pipeline, the amount and extent to which the pipeline receives data will be outlined below. Here is what the testing framework should look like:

1. Rewind clip lands in `sl-live-cam-archive-prod` bucket
2. `wt-copy-rewind-clips-prod` lambda function will trigger. This lambda function will copy rewind clips to `sl-live-cam-archive-sandbox|staging` from `sl-live-cam-archive-prod` for only one camera
3. Copy all of the production steps discussed above.

**Things to note:**

- Attach a policy to all **sandbox** and **staging** buckets to remove objects after 30 days
- When developing and deploying to the testing environments, one can verify correctness by assuring `NewRelic` does not send any alerts, or by checking the necessary S3 buckets for annotated frames as well as highlight clips
- Make sure to leave out the "**POST|PUT** to `cameras-service`" step when creating the individual parts of the testing environment

## How does this solve the problem?

This production pipeline solves the problem because now there is a way for all Surfline engineers to hit an endpoint to receive crowd counts as well as smart highlights. Also, this new pipeline cleans up the old one, and takes away certain features that were not optimized or cost effective. Finally, we will be able to be confident about getting these data reliably because we will have development environments in place to verify changes before pushing them to production.

## What are the alternatives?

1. Use the [**prototype-ml-pipeline**](https://github.com/Surfline/prototype-ml-cam)
   - **Pros:**
     - Already set-up and working 
   - **Cons:**
     - No development environment
       - No automated testing or CI/CD system
     - Other prototypes (that are not crowd counts or highlights) are being tested in the pipeline
     - Not cost effective as batch is being used in places that do not need it
     - Uses both development and production accounts
     - No API set-up to access the Aurora database
     - Data is varied as models have been approved/changed during the course of the pipelines life

## What do we know already?

-  We know we can accurately get crowd counts/highlight clips for various southern california cameras, hence implementing the pipeline will definitely bear results
- We know each part of the pipeline works, as we have confirmed this through the prototype
- It is going to be hard for us to do this live. The issue with live is we'll need to pass the camera time from Wowza out of the RTSP stream which is a challenge.
- Development environments are important because the results of this pipeline will be directly reflected on the app
- We have the logic for each part of the pipeline, we just need to put it all together

## Q & A

- Will this pipeline work for all Surfline cameras?
  - No. This pipeline will only work for a certain amount of approved cameras. Approved cameras are defined in the environment variables of the model, hence, it won't break the pipeline if a rewind from an unapproved camera lands in the entrypoint.
- What happens if there is no live camera data coming for a certain camera?
  - The database records for the timestamps during the camera outage will not exist. Hence, services that are expecting the crowd count/highlight clips endpoint to return results will receive a 404.
- Does the `CameraCrowds` collection exist in Mongo?
  - No. This collection needs to be created.
- Do the fields `highlightsUrl`, `highlightsThumbUrl`, `highlightsGifUrl` exist in the `CameraRecording` collection?
  - No. These fields need to be added to the `CameraRecording` collection.
- Why don't we split the `wt-frame-extractor-and-highlights-trigger-{env}` lambda function into two separate lambda functions? One could do frame extraction, and the other could submit highlight batch jobs.
  - Then we would have to copy the "Is it daytime at the spot for the given rewind?" logic in two places. Also, adding more lambdas than you need is an added cost as well as added complexity.













