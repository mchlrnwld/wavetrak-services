#!/bin/bash

# Convert all puml files in input dir to pngs in output dir.
INPUTDIR=$(PWD)/systems
OUTPUTDIR=$(PWD)/images

for input_file in "${INPUTDIR}"/**/*.puml; do
  destpath=${input_file/$INPUTDIR/$OUTPUTDIR}
  destpath=${destpath/puml/png}

  echo "Render: ${input_file} --> ${destpath}"

  FILEDIR=$(dirname "${destpath}")
  mkdir -p "${FILEDIR}"

  docker run --rm -i wavetrak-services-plantuml -tpng > "${destpath}" < "${input_file}"
done

# Converts all puml files in the proposals directory to images.
for input_file in "${PWD}"/proposals/**/*.puml; do
  destpath=${input_file/puml/png}

  echo "Render: ${input_file} --> ${destpath}"
  docker run --rm -i wavetrak-services-plantuml -tpng > "${destpath}" < "${input_file}"
done

echo Done
