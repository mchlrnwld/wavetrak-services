@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

!define AWSPuml https://raw.githubusercontent.com/awslabs/aws-icons-for-plantuml/v10.0/dist
!define ICONURL https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.3.0

!includeurl AWSPuml/AWSCommon.puml
!includeurl AWSPuml/Compute/EC2.puml
!includeurl AWSPuml/Containers/ElasticContainerService.puml
!includeurl AWSPuml/Containers/ElasticContainerServiceContainer1.puml
!includeurl AWSPuml/NetworkingContentDelivery/CloudFront.puml
!includeurl AWSPuml/NetworkingContentDelivery/ElasticLoadBalancingApplicationLoadBalancer.puml
!includeurl ICONURL/common.puml
!includeurl ICONURL/devicons/nginx.puml
!includeurl ICONURL/font-awesome-5/cloudflare.puml
!includeurl ICONURL/material/computer.puml


title Detailed Web Service Request Flow

MATERIAL_COMPUTER(customer, Client: Surfline User, rectangle) #White

System_Boundary(slVpc, "AWS VPC") {

  FA5_CLOUDFLARE(slCloudflare, Cloudflare: www.surfline.com, rectangle, blue) #White
  CloudFront(slCloudfront, "Cloudfront", "d2r4cc3gdr1to1.cloudfront.net", "Cloudfront forwards request")
  ElasticLoadBalancingApplicationLoadBalancer(slProxyAlb, "Web Proxy ALB", "sl-ecs-web-proxy-int-prod")
  DEV_NGINX(proxyService, Web Proxy: sl-web-proxy-prod, rectangle, green) #White

  System_Boundary(slWebServices, "Web Services") {
    ElasticContainerServiceContainer1(adminCamerasService, "admin cameras", "admin-cameras-service.prod.surfline.com")
    ElasticContainerServiceContainer1(adminForecastsService, "admin forecasts", "admin-forecasts-service.prod.surfline.com")
    ElasticContainerServiceContainer1(adminLoginService, "admin login", "admin-login-service.prod.surfline.com")
    ElasticContainerServiceContainer1(adminReportsService, "admin reports", "admin-reports-service.prod.surfline.com")
    ElasticContainerServiceContainer1(adminSearchService, "admin search", "admin-search-service.prod.surfline.com")
    ElasticContainerServiceContainer1(adminSpotsService, "admin spots", "admin-spots-service.prod.surfline.com")
    ElasticContainerServiceContainer1(adminUsersService, "admin users", "admin-users-service.prod.surfline.com")
    ElasticContainerServiceContainer1(accountService, "account", "subscription-service.prod.surfline.com")
    ElasticContainerServiceContainer1(chartsService, "charts", "web-charts.prod.surfline.com")
    ElasticContainerServiceContainer1(homepageService, "homepage", "homepage.prod.surfline.com")
    ElasticContainerServiceContainer1(kbygService, "kbyg", "kbyg.prod.surfline.com")
    ElasticContainerServiceContainer1(onboardingService, "onboarding", "onboarding.prod.surfline.com")
    ElasticContainerServiceContainer1(travelService, "travel", "travel.prod.surfline.com")
  }

  System_Boundary(slBackendServices, "Backend Services") {
    ElasticContainerServiceContainer1(geoTargetService, "geo-target", "geo-target-service.prod.surfline.com")
    ElasticContainerServiceContainer1(promotionsService, "promotions", "promotions-service.prod.surfline.com")
    ElasticContainerServiceContainer1(subscriptionService, "subscription", "subscription-service.prod.surfline.com")
    ElasticContainerServiceContainer1(editorialService, "editorial", "editorial-wp.prod.surfline.com")
  }

  System_Boundary(slAlbBoundary, "Core Services ALBs") {
    ElasticLoadBalancingApplicationLoadBalancer(slCoreAlb1, "ALB1", "sl-int-core-srvs-1-prod")
    ElasticLoadBalancingApplicationLoadBalancer(slCoreAlb2, "ALB2", "sl-int-core-srvs-2-prod")
    ElasticLoadBalancingApplicationLoadBalancer(slCoreAlb3, "ALB3", "sl-int-core-srvs-3-prod")
  }

  ' CDN relationships
  Rel_D(customer, slCloudflare, "requests", "surfline link")
  Rel_D(slCloudflare, slCloudfront, "routed to", "Cloudfront origin")
  Rel_D(slCloudfront, slProxyAlb, "routed to", "sl-ecs-web-proxy-int-prod")
  Rel_D(slProxyAlb, proxyService, "routed to", "proxy service")

  ' Proxy -> ALBs relationships
  Rel_D(proxyService, slCoreAlb1, "path matches", "route")
  Rel_D(proxyService, slCoreAlb2, "path matches", "route")
  Rel_D(proxyService, slCoreAlb3, "path matches", "route")

  ' ALB -> service relationships
  Rel_D(slCoreAlb1, accountService, "routes to", "account service")
  Rel_D(slCoreAlb1, geoTargetService, "routes to", "geo target service")
  Rel_D(slCoreAlb1, onboardingService, "routes to", "onboarding service")
  Rel_D(slCoreAlb2, chartsService, "routes to", "charts service")
  Rel_D(slCoreAlb2, homepageService, "routes to", "homepage service")
  Rel_D(slCoreAlb2, travelService, "routes to", "travel service")
  Rel_D(slCoreAlb3, adminCamerasService, "routes to", "admin cameras service")
  Rel_D(slCoreAlb3, adminForecastsService, "routes to", "admin forecasts service")
  Rel_D(slCoreAlb3, adminLoginService, "routes to", "admin login service")
  Rel_D(slCoreAlb3, adminReportsService, "routes to", "admin reports service")
  Rel_D(slCoreAlb3, adminSearchService, "routes to", "admin search service")
  Rel_D(slCoreAlb3, adminSpotsService, "routes to", "admin spots service")
  Rel_D(slCoreAlb3, adminUsersService, "routes to", "admin users service")
  Rel_D(slCoreAlb3, editorialService, "routes to", "editorial service")
  Rel_D(slCoreAlb3, kbygService, "routes to", "kbyg service")
  Rel_D(slCoreAlb3, promotionsService, "routes to", "promotions service")
  Rel_D(slCoreAlb3, subscriptionService, "routes to", "subscription service")
}
@enduml
