<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Services Request Flow](#services-request-flow)
  - [Notes](#notes)
- [Overview of Service Request Flow:](#overview-of-service-request-flow)
- [Detailed Web Service Request Flow:](#detailed-web-service-request-flow)
- [Detailed Backend Service Request Flow:](#detailed-backend-service-request-flow)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Services Request Flow

## Notes
- The recommened way to view a larger version of images below is to right-click and select "Open Image in New Tab". 
- Services architecture is similar in lower tiers.
- Credentials can be found in 1Password.
- Service endpoints are stored in Parameter Store.

Services request flow architecture is as follows:

# Overview of Service Request Flow:
![Service Request Flow Overview](../../images/service-request-flow/overview-service-request-flow.png)

# Detailed Web Service Request Flow:
![Service Request Flow Overview](../../images/service-request-flow/detailed-web-service-request-flow.png)

# Detailed Backend Service Request Flow:
![Service Request Flow Overview](../../images/service-request-flow/detailed-backend-service-request-flow.png)
