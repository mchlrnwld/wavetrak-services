@startuml Editorial Component
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml

!define ICONURL https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.2.0
!includeurl ICONURL/common.puml
!includeurl ICONURL/devicons/swift.puml
!includeurl ICONURL/devicons/android.puml

LAYOUT_WITH_LEGEND()

title Editorial Component

System_Boundary(slSystem, 'Surfline Systems') {
  Container(slIos, "iOS App", "swift", "iOS app", "swift")
  Container(slAndroid, "iOS Android", "kotlin", "SL Android", "android")

  System_Boundary(slWeb, 'SL Web') {
    Container(webHomepage, "web/homepage", "Populates Homepage with editorial content")
    Container(webKBYG, "web/KBYG", "Populates KBYG with editorial content")
    Container(webTravel, "web/travel", "Populates Travel with editorial content")
    Container(webBackplane, "web/backplane", "Populates Backplane with editorial content")
  }
}

System_Boundary(slAWS, 'SL AWS') {
  Container(snsVarnishTopic, "SNS Topic", "Topic to clean Varnish Cache - sl-editorial-cms-update-varnish-cache")
  Container(snsFeedTopic, "SNS Topic", "Topic to create/update the Feed content - sl-editorial-cms-feed-etl")
  Container(snsTravelTopic, "SNS Topic", "Topic to create/update the Travel content - sl-editorial-cms-travel-page-etl")
  Container(feedLambda, "Feed Lambda", "Python Lambda - feed-etl-wp-to-es")
  Container(travelLambda, "Travel Lambda", "Python Lambda - travel-page-etl-wp-to-es")
  Container(varnishLambda, "Varnish Lambda", "Python Lambda - update-varnish-cache")
  Container(ES, "ElasticSearch", "Feeds Mappings")
  ContainerDb(RDS, "RDS", "Relational Database - MySQL")
}

Component(posts, "posts", "Creates posts")
Component(premiumAnalysis, "premiumAnalysis", "Creates Premium Analysis pages")
Component(realTime, "realTime", "Creates Real Time pages")
Component(localNews, "localNews", "Creates Local News pages")
Component(contests, "contests", "Creates Contests pages")
Component(defaultHP, "defaultHP", "Set the order for Defaul Homepage")
Component(australiaHP, "australiaHP", "Set the order for Australia Homepage")
Component(plugins, "plugins", "They can extend functionality or add new features to your WordPress websites")
Component(travel, "travel", "Creates Travel pages")
Component(wpAPI, "WP API", "Provides Editorial Content")

System_Boundary(platformWt, 'Wavetrak Platform') {
  Container(feedService, "feed-service", "Feed Service")
  Container(searchService, "Search", "Populates posts based on search term")
}

System_Ext(appleNews, "Apple News", "3rd-party editors publishing content to")
System_Ext(amp, "Accelerated Mobile Pages", "3rd-party service")
System_Ext(jwPlayer, "JW Player", "3rd-party service to create videos and playlists")


Rel(plugins, wpAPI, "Reads from and writes to", "JDBC")
Rel(plugins, appleNews, "Reads from and writes to", "JDBC")
Rel(plugins, amp, "Reads from and writes to", "JDBC")
Rel(plugins, jwPlayer, "Reads from and writes to", "JDBC")


Rel(wpAPI, RDS, "Reads from and writes to", "JDBC")
Rel(travel, RDS, "Reads from and writes to", "JDBC")
Rel(posts, RDS, "Reads from and writes to", "JDBC")
Rel(premiumAnalysis, RDS, "Reads from and writes to", "JDBC")
Rel(realTime, RDS, "Reads from and writes to", "JDBC")
Rel(localNews, RDS, "Reads from and writes to", "JDBC")
Rel(contests, RDS, "Reads from and writes to", "JDBC")
Rel(defaultHP, RDS, "Reads from and writes to", "JDBC")
Rel(australiaHP, RDS, "Reads from and writes to", "JDBC")
Rel(plugins, RDS, "Reads from and writes to", "JDBC")


Rel(posts, snsFeedTopic, "Uses")
Rel(premiumAnalysis, snsFeedTopic, "Uses")
Rel(realTime, snsFeedTopic, "Uses")
Rel(localNews, snsFeedTopic, "Uses")
Rel(contests, snsFeedTopic, "Uses")
Rel(defaultHP, snsFeedTopic, "Uses")
Rel(australiaHP, snsFeedTopic, "Uses")


Rel(posts, snsVarnishTopic, "Uses")
Rel(premiumAnalysis, snsVarnishTopic, "Uses")
Rel(realTime, snsVarnishTopic, "Uses")
Rel(localNews, snsVarnishTopic, "Uses")
Rel(contests, snsVarnishTopic, "Uses")
Rel(defaultHP, snsVarnishTopic, "Uses")
Rel(australiaHP, snsVarnishTopic, "Uses")


Rel(travel, snsTravelTopic, "Uses")


Rel(snsFeedTopic, feedLambda, "Uses")
Rel(snsTravelTopic, travelLambda, "Uses")
Rel(snsVarnishTopic, varnishLambda, "Uses")
Rel(feedLambda, ES, "Updates ES data")
Rel(travelLambda, ES, "Updates ES data")
Rel(varnishLambda, wpAPI, "Uses")


Rel(ES, feedService, "Uses")
Rel(searchService, ES, "Uses")
Rel(wpAPI, searchService, "Uses")
Rel(webHomepage, feedService, "Makes calls to", "JSON/HTTPS")
Rel(webKBYG, feedService, "Makes calls to", "JSON/HTTPS")
Rel(webTravel, wpAPI, "Makes calls to", "JSON/HTTPS")
Rel(feedService, wpAPI, "Makes calls to", "JSON/HTTPS")
Rel(webBackplane, searchService, "Makes calls to", "JSON/HTTPS")
Rel(webKBYG, wpAPI, "Makes calls to", "JSON/HTTPS")
Rel(webHomepage, wpAPI, "Makes calls to", "JSON/HTTPS")


Rel(slIos, wpAPI, "Makes API calls to", "JSON/HTTPS")
Rel(slAndroid, wpAPI, "Makes API calls to", "JSON/HTTPS")
Rel(slIos, webTravel, "webView", "JSON/HTTPS")
Rel(slAndroid, webTravel, "webView", "JSON/HTTPS")
Rel(slIos, slWeb, "Makes API calls to", "JSON/HTTPS")
Rel(slAndroid, slWeb, "Makes API calls to", "JSON/HTTPS")
Rel(slIos, feedService, "Makes API calls to", "JSON/HTTPS")
Rel(slAndroid, feedService, "Makes API calls to", "JSON/HTTPS")


@enduml
