# Push Notification Architecture

## Glossary

- APNS: [Apple Push Notification Service](https://developer.apple.com/library/archive/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/APNSOverview.html)
- FCM: Firebase cloud message, Android equivalent of APNS.
- APNS Gateway: service that interfaces with APNS or Android equivalent. We're using this to describe one of Wonderpush, customer.io and SNS since the rest of the system is agnostic of this gateway.

## Notification System

![Notifications System Context](../../images/notifications/notifications-context.png)

### System Flows

1. **Token Management**
   1. iOS App uses iOS API to get APNS token from Apple.
   2. App posts token and device info to notification service ([API contract](../../../services/notifications-service/docs/outlets.md)). Will require the user to be logged in. If tokens change, new tokens should be sent for that device ID ([Client side implementation notes](#client-side-handling-tokens)).
   3. Notification service sends that token to SNS via [CreatePlatformEndpoint](https://docs.aws.amazon.com/sns/latest/api/API_CreatePlatformEndpoint.html) request. SNS returns a `EndpointArn` for that platform and device.
   4. We store that `EndpointArn` in Mongo for that user. We also store the token itself since we may need it in the future for use in another system.
2. **Notification Preference**
   1. Once the token is stored in SNS (1.iii) and saved in Mongo (1.iv), the app can initiate requests to setup a notification preferences for both sessions and spot data ([API contract](../../../services/notifications-service/docs/preferences.md)).
   2. Notification service stores preferences in Mongo.
3. **Sending a Notification**

4. **Sending a Notification**<br />
   The Notification system subscribes to SNS topics owned by other services. New messages on those topics can trigger new notifications.
   1. **Spot Reports ECS Service** (API)<br />
      The `spot_report_updated_{env}` topic is used to trigger new notifications when spot reports are updated.
   1. **Sessions ECS Service** (API)<br />
      The `sl-sessions-api-sessions-updated-sns_{env}` topic is used to trigger new notifications when sessions have completed processing. This event is emitted once the camera clips for a particular session are no longer in a pending state.
      1. The Sessions API responds to clip status SNS topic `sl-cameras-service-generate-clip-sns_{env}` to determine if there are no more pending camera clips for a particular session.
      2. The Sessions API emits an event to `sl-sessions-api-sessions-updated-sns_{env}` SNS topic, indicating that a user's session has completed processing and is ready to view (no more pending rewind clips to generate).
   1. Notification Service endpoints subscribed to these SNS topics will receive messages for matching and processing. (Service specific retry logic can be added to each topic subscription, in the event service is unresponsive or throws 500 errors, mitigating Notification Service outages). Max retries for SNS is 1 hour, but [can be configured in multiple ways](https://docs.aws.amazon.com/sns/latest/dg/sns-message-delivery-retries.html) to fit specific needs.
   1. Using the SNS message data, the Notification Service SNS endpoints match users subscribed to this message within Mongo, based on established preferences for each of the spot and session endpoints listening to events. The Notification Service splits query results into batches (5000 to 10000) including `EndpointArn`s and event and message specific metadata and saves each to the `sl-notification-batches-{env}` S3 bucket for processing.
      1. [Structure for message and metadata below](#message-metadata-for-notifications)
         1. **KBYG use case**<br />
            Lambda function will prevent unnecessary notifications for a spot being sent. This can happen if the forecaster updates the report, say to fix a typo in the report but doesn't update the surf height.
            We will store a key in Redis which is a hash of `notification-{spot}{reportHeightMax}{reportHeightMin}{reportOccasional}{reportCondition}{reportAM/PM}` once a report has been sent, the key will have a 48 hour expiration. Before sending a notification for this spot we'll build a key from the report data, if the key doesn't exist then we'll trigger the notification (since the report is considered new) if the key does exist then whilst the report was updated it didn't include any new information that'll we'll communicate to the user in the notification thus we won't send a notification for this report. Note for reportAM/PM we'll only store the AM/PM string for the reports posted time. This ensures that we won't suppress 2 notifications a day (AM/PM).
         2. **Sessions use case**<br />
            Lambda function ensures the `apns-collapse-id` is sent to SNS (which SNS sends to APNS) since this prevents the same message being sent multiple times to the user's device. Docs [here](https://docs.aws.amazon.com/sns/latest/dg/sns-send-custom-platform-specific-payloads-mobile-devices.html) and [here](https://developer.apple.com/library/archive/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/APNSOverview.html#//apple_ref/doc/uid/TP40008194-CH8-SW1). It seems overkill to use Redis to prevent this for sessions given we don't expect to handle the session created event multiple times for a session like we do with reports.
   1. Notification Processor Lambda processes the batch notification files. Saving the batch files to S3 bucket triggers this Notification Processor Lambda function (`sl-notification-processor-{env}`). This Lambda function takes the metadata and contents of the S3 batch file and begins processing in the next step.
   1. 1. Notification Processor Lambda function makes a [publish request to SNS](https://docs.aws.amazon.com/sns/latest/api/API_Publish.html) with `EndpointArn` and event meta data for set of each message data in the file. These can be found within `services/notifications-service/functions/notification-processor/src/handlers/`
      2. Notification Processor Lambda sends off a "Sent Notification" Segment event.
   1. 1. SNS sends message(s) to APNS with message payload, user device token, etc.
   1. APNS delivers message to user's device. App is configured to respond to notification type/data and open relevant UI. We'll use [custom notification UI](https://developer.apple.com/documentation/usernotificationsui/customizing_the_appearance_of_notifications) to show a live cam or session video in the notification itself.
   1. Opened/Received and Tapped Notification events are sent to Segment at this time from the app.

## Message Metadata for Notifications

The following data is sent as part of the message payload to accommodate the mobile team's requirements.

These configurations can be found in `/services/notifications-service/src/server/webhooks/handlers`.

### Session

- sessionId
- user
- waveCount
- spot

### Spots

- spotId
- spotName
- clipUrl (most recent 10 minute rewind url)
- waveHeightMin
- waveHeightMax
- waveHeightOccasional
- condition
- windSpeed
- windDirection
- currentTide
## Token Management

### Client Side Handling Tokens

Since we're only sending notifications for logged in users the following should be tied to whether the user is logged in or not.

- **Already logged in and enables notifications:** see next one down.
  - **Use case:** user sets up notifications for the first time.
- **Upon log in and already has notification permissions:**
  - **Use case:** users logs into new device and expects to get notifications related to their configuration on this device.
  - get device token
  - call POST on notification service with device token
  - save notification service outlet ID (returned by the service, not the iOS device ID) and store device token locally.
- **Upon app open and if user logged in and already asked for notification permissions:**
  - **Use case:** APNS has updated the users device token (can happen) and we need to update the remote copy.
  - get device token
  - if new device token doesn't match stored device token call PUT on notification service endpoint ID to update device token.
- **Upon log out:**
  - **Use case:** user logs out and thus turns off notifications for this device.
  - Call DELETE on notification service device endpoint path with outlet ID to suppress notifications.
  - Clear the locally stored outlet ID and device token.

### Server Side Handling Tokens

On the server we'll provide a POST/PUT/DELETE endpoint to manage tokens. We'll pretty much just pass this on to SNS and modify the endpoint appropriately. We will store the resulting endpoint ARN from SNS in Mongo for this user's outlet/device (the user can have multiple) and we'll send messages to all endpoint ARNs for that user based on their preferences (preferences are unique to account not device).

## Tracking

Sending a message to a users device is like sending any other SNS message. Thus we can send "Sent Notification" event in Segment on the server side. We should track the response from SNS since it might be [EndpointDisabled](https://docs.aws.amazon.com/sns/latest/api/API_Publish.html) which should mean the user has disabled notifications in their iOS settings (we should still attempt to send notifications to this user since they can turn it back on and we won't be notified).

Client side tracking of the notification, i.e. if the user opened the notification or not, should be handled within the iOS app.
