@startuml Detailed Wavetrak System Context
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

AddTagSupport("vLegacy", $bgColor="#d73027", $borderColor="#d73027")
AddTagSupport("vMajorOutOfDate", $bgColor="#ff6f00", $borderColor="#ff6f00")

title Wavetrak System Context

Person(customerSl, "Surfline Customer", "Surfline customers.")
Person(customerMsw, "MSW Customer", "Magicseaweed customers.")
Person(customerBw, "Buoyweather Customer", "Buoyweather customers.")
Person(customerFt, "Fishtrack Customer", "Fishtrack customers.")
Person(wtEmployee, "Wavetrak Employee", "Internal Wavetrak employee who is publishing data, working with customers etc.")

System_Ext(slApiPartners, "Surfline API Partners", "3rd-party clients using REST APIs.")
System_Ext(mswApiPartners, "MSW API Partners", "3rd-party clients using REST APIs.")
System_Ext(sessionsPartners, "Sessions API Partners", "3rd-party clients pushing sessions data.")

System_Boundary(mswSystem, 'MSW Systems') {
  System(msw, "MSW Monolith", "MSW frontend, API, some data processing crons and ExpressionEngine content CMS.", $tags="vMajorOutOfDate")
  System(mswIos, "MSW iOS", "MSW native apps.")
  System(mswAndroid, "MSW Android", "MSW native apps.", $tags="vLegacy")
  System(tideMsw, "MSW Tides", "xtide system. Different harmonics to SL xtide system.", $tags="vMajorOutOfDate")
  System(forecastMsw, "MSW Forecast System", "MSW forecast system for MSW spots.", $tags="vLegacy")
  System(modelProteus, "WW3 - Proteus", "Proteus model.", $tags="vLegacy")
  System(subscriptionMsw, "MSW Store System", "Handle subscriptions and payment processing for MSW. User authentication and entilements is handled by the MSW monolith.", $tags="vLegacy")
}

System_Boundary(bwSystem, 'Buoyweather Systems') {
  System(bwNew, "BW iOS/Android/New Web", "Non-legacy product UI.", $tags="vMajorOutOfDate")
  System(bwLegacy, "BW Legacy", "BW legacy pages, APIs and charting providers for BW/FT/SL.", $tags="vLegacy")
}

System_Boundary(slSystem, 'Surfline Systems') {
  System(slNew, "SL iOS/Android/New Web", "Non-legacy product UI.", "react")
  System(slCf, "SL Coldfusion", "Surfline , APIs and charting providers for BW/FT/SL.", $tags="vLegacy")
}

System(subscriptionWt, "WT Subscription System", "Handles user authentication, subscriptions and entitlements.")

System_Boundary(ftSystem, 'FishTrack Systems') {
  System(ftWebsite, "FT Web/API", "Coldfusion based FishTrack website and APIs.", $tags="vLegacy")
  System(ftApps, "FT iOS/Android", "FT native apps.", $tags="vMajorOutOfDate")
}

System(ftJobs, "FT Jobs", "FishTrack specific data jobs.", $tags="vMajorOutOfDate")

System_Boundary(forecastSystems, 'Forecast Systems') {
  System(forecastWt, "WT Forecast", "WT forecast system for coordinates and POIs.")
  System(forecastBest, "Best Forecast", "Best forecast system for coordinates and POIs.", $tags="vLegacy")
  System(forecastMarineApi, "CF Based Marine API", "CF based coordinate forecast API.", $tags="vLegacy")
  System(forecastCf, "MSSQL Forecast", "Jobs for CF forecast spot API.", $tags="vLegacy")
}

System_Boundary(forecastSystems, 'Modeling Systems') {
  System(modelLotus, "WW3 - Lotus", "Lotus model.")
  System(modelLola, "WW3 - LOLA", "LOLA model.", $tags="vLegacy")
  System(modelWrf, "WRF", "WRF wind models.", $tags="vLegacy")
}

ContainerDb(ocean, "Ocean NFS", "NFS share used to transfer data between legacy science systems.")

System_Ext(noaaModels, "3rd Party Data", "NOAA data for GFS, NAM, GEFS, sea ice, etc. NASA MURS data. There's more data we depend on.")

System_Boundary(tideSytems, 'Tide Systems') {
  System(tideSl, "SL Tides", "Cached vesion of legacy xtide and virtual tide station data.", $tags="vLegacy")
  System(tidePoints, "Legacy Tides", "xtide and virtual tide stations.", $tags="vLegacy")

}

System_Boundary(cmsSystems, 'CMS Systems') {
  System(cmsNew, "Admin Tools", "Admin tools CMS.")
  System(cmsCf, "SL CF Admin", "Coldfusion admin.", $tags="vLegacy")
  System(cmsSl, "SL Wordpress", "WP for Surfline brand.")
  System(cmsBw, "BW Wordpress", "WP for Buoyweather brand.", $tags="vLegacy")
}

System(cameras, "Camera System", "Camera live streams, rewinds and sessions clips.")
System(notifications, "Notifications System", "Push notification system for surf reports.")

'System_Ext(segment, "Segment", "Segment customer data platform. ETLs data from products to downstream sources like Redshift, Amplitude, GoogleAnalytics, customer.io etc.")
System_Ext(stripe, "Payment Gateways", "Stripe/Google/Apple billing platform.")
System_Ext(sagepay, "Payment Gateways", "SagePay/Google/Apple billing platform.")

Rel_D(wtEmployee, cmsNew, "Uses", "HTTPS")
Rel_D(wtEmployee, cmsCf, "Uses", "HTTPS")
Rel_D(wtEmployee, cmsSl, "Uses", "HTTPS")
Rel_D(wtEmployee, cmsBw, "Uses", "HTTPS")

' MSW
Rel_D(customerMsw, msw, "Uses", "HTTPS")
Rel_D(mswApiPartners, msw, "Uses", "HTTPS")
Rel_D(customerMsw, mswAndroid, "Uses", "App")
Rel_D(customerMsw, mswIos, "Uses", "App")
Rel(msw, forecastWt, "Uses", "HTTPS")
Rel(msw, forecastMsw, "Uses", "MySQL")
Rel(msw, tideMsw, "Uses", "MySQL")
Rel(msw, subscriptionMsw, "Uses", "HTTPS")
Rel_L(mswAndroid, msw, "Uses", "HTTPS")
Rel_L(mswIos, msw, "Uses", "HTTPS")
Rel(subscriptionMsw, sagepay, "Uses", "HTTPS")
Rel(msw, cameras, "Uses", "HTTPS")

' Forecast/Models
Rel(forecastMsw, modelProteus, "Uses", "S3")
Rel(forecastWt, modelLotus, "Uses", "S3")
Rel(forecastWt, noaaModels, "Uses", "NOMADS/FTPPRD")
Rel(forecastBest, modelLola, "Uses", "NFS - Ocean")
Rel(forecastBest, noaaModels, "Uses", "NFS - Ocean")
Rel(forecastMarineApi, modelLola, "Uses", "NFS - Ocean")
Rel(forecastMarineApi, noaaModels, "Uses", "NFS - Ocean")
Rel(modelProteus, noaaModels, "Uses", "NOMADS/FTPPRD")
Rel(modelLotus, noaaModels, "Uses", "NOMADS/FTPPRD")
Rel(modelLola, noaaModels, "Uses", "NOMADS/FTPPRD")
Rel(modelWrf, noaaModels, "Uses", "NOMADS/FTPPRD")

Rel(cmsNew, forecastWt, "Uses", "HTTPS")
Rel(cmsNew, forecastBest, "Uses", "HTTPS")
Rel(cmsCf, forecastCf, "Uses", "HTTP")

Rel(tidePoints, tideSl, "Pushes", "Script to push data to")

' Surfline New/Old
Rel_D(customerSl, slNew, "Uses", "HTTPS")
Rel_D(customerSl, slCf, "Uses", "HTTP")
Rel_D(slApiPartners, slCf, "Uses", "HTTP")

Rel(slCf, forecastCf, "Uses", "MSSQL")
Rel(forecastCf, modelLola, "Uses", "NFS - Ocean")
Rel(forecastCf, modelWrf, "Uses", "NFS - Ocean")
Rel(slCf, tidePoints, "Uses", "MSSQL")
Rel(slCf, bwLegacy, "Uses", "HTTP")
Rel(slCf, cmsCf, "Uses", "MSSQL/HTTP")
Rel_L(slCf, slNew, "Uses", "HTTPS")

Rel(slNew, forecastWt, "Uses", "HTTPS")
Rel(slNew, forecastBest, "Uses", "HTTPS")
Rel(slNew, tideSl, "Uses", "HTTPS")
Rel(slNew, cmsNew, "Uses", "HTTPS")
Rel(slNew, cmsSl, "Uses", "HTTPS")
Rel(slNew, subscriptionWt, "Uses", "HTTPS")
Rel(slNew, notifications, "Uses", "HTTPS")
Rel(subscriptionWt, stripe, "Uses", "HTTPS")

Rel(slNew, cameras, "Uses", "HTTPS")

Rel(sessionsPartners, slNew, "Uses", "HTTPS")

' Buoyweather
Rel_D(customerBw, bwNew, "Uses", "HTTPS")
Rel_D(customerBw, bwLegacy, "Uses", "HTTP")
Rel_L(bwNew, bwLegacy, "Uses", "HTTP")
Rel(bwNew, tidePoints, "Uses", "HTTP")
Rel(bwLegacy, modelLola, "Uses", "HTTP")
Rel(bwLegacy, forecastMarineApi, "Uses", "HTTP")
Rel(bwNew, cmsBw, "Uses", "HTTP")
Rel(bwNew, subscriptionWt, "Uses", "HTTPS")
Rel(bwLegacy, subscriptionWt, "Uses", "HTTPS")

' FishTrack
Rel(customerFt, ftWebsite, "Uses", "HTTP")
Rel(customerFt, ftApps, "Uses", "HTTP")
Rel_L(ftApps, ftWebsite, "Uses", "HTTP")
Rel_U(ftJobs, ocean, "Uses", "NFS - Ocean")
Rel(ftWebsite, bwLegacy, "Uses", "HTTP")
Rel(ftApps, bwLegacy, "Uses", "HTTP")

' Ocean
Rel(bwLegacy, ocean, "Uses", "NFS")
Rel(ftWebsite, ocean, "Uses", "NFS")
Rel(modelLola, ocean, "Uses", "NFS")
Rel(modelWrf, ocean, "Uses", "NFS")
Rel(forecastMarineApi, ocean, "Uses", "NFS")
Rel(forecastBest, ocean, "Uses", "NFS")
Rel(forecastCf, ocean, "Uses", "NFS")

SHOW_DYNAMIC_LEGEND()

@enduml
