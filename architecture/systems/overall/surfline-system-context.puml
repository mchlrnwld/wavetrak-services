@startuml Surfline System Context
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

!define ICONURL https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/v2.2.0
!includeurl ICONURL/common.puml
!includeurl ICONURL/devicons/swift.puml
!includeurl ICONURL/devicons/android.puml

title Surfline System Context

Person(customerSl, "Surfline Customer", "Surfline customers.")
Person(wtEmployee, "Wavetrak Employee", "Internal Wavetrak employee who is publishing data, working with customers etc.")

System_Ext(slApiPartners, "Surfline API Partners", "3rd-party clients using REST APIs.")
System_Ext(sessionsPartners, "Sessions API Partners", "3rd-party clients pushing sessions data.")

System(bw, "BW", "SL CF Advanced charts product.")

System_Boundary(slSystem, 'Surfline Systems') {
  Container(slIos, "iOS App", "swift", "iOS app", "swift")
  Container(slAndroid, "iOS Android", "kotlin", "SL Android", "android")
  System(slNew, "SL New", "React based non-legacy product UI.")
  System(services, "Services", "Mostly Node/Express REST APIs, a couple of GraphQL services and Lambda/SNS/SQS/Gateway.")
  System(slCf, "SL Coldfusion", "Surfline CF monolith .")
}

System_Boundary(wavetrakPlatform, 'Wavetrak Platform') {
  System(subscriptionWt, "WT Subscription System", "Handles user authentication, subscriptions and entitlements.")

  System_Boundary(forecastSystems, 'Forecast Systems') {
    System(forecastWt, "WT Forecast", "WT forecast system for coordinates and POIs.")
    System(forecastBest, "Best Forecast", "Best forecast system for coordinates and POIs.")
    System(forecastCf, "MSSQL Forecast", "Jobs for CF forecast spot API.")
  }

  System_Boundary(modelingSystems, 'Modeling Systems') {
    System(modelLotus, "WW3 - Lotus", "Lotus model.")
    System(modelLola, "WW3 - LOLA", "LOLA model.")
    System(modelWrf, "WRF", "WRF wind models.")
  }

  System_Boundary(tideSytems, 'Tide Systems') {
    System(tideSl, "SL Tides", "Cached vesion of legacy xtide and virtual tide station data.")
    System(tidePoints, "Legacy Tides", "xtide and virtual tide stations.")
  }

  System_Boundary(cmsSystems, 'CMS Systems') {
    System(cmsNew, "Admin Tools", "Admin tools CMS.")
    System(cmsCf, "SL CF Admin", "Coldfusion admin.")
    System(cmsWp, "SL Wordpress", "WP for Surfline brand.")
  }

  System(cameras, "Camera System", "Camera live streams, rewinds and sessions clips.")
  System(notifications, "Notifications System", "Push notification system for surf reports.")
}

System_Ext(noaaModels, "3rd Party Data", "NOAA data for GFS, NAM, GEFS, sea ice, etc. NASA MURS data. There's more data we depend on.")

System_Ext(stripe, "Stripe", "Stripe billing platform.")
System_Ext(google, "Google", "Google IAP APIs")
System_Ext(apple, "Apple", "Apple IAP APIs.")

Rel_D(wtEmployee, cmsNew, "Uses", "HTTPS")
Rel_D(wtEmployee, cmsCf, "Uses", "HTTPS")
Rel_D(wtEmployee, cmsWp, "Uses", "HTTPS")

' Forecast/Models
Rel(forecastWt, modelLotus, "Uses", "S3")
Rel(forecastWt, noaaModels, "Uses", "NOMADS/FTPPRD")
Rel(forecastBest, modelLola, "Uses", "NFS - Ocean")
Rel(forecastBest, noaaModels, "Uses", "NFS - Ocean")
Rel(modelLotus, noaaModels, "Uses", "NOMADS/FTPPRD")
Rel(modelLola, noaaModels, "Uses", "NOMADS/FTPPRD")
Rel(modelWrf, noaaModels, "Uses", "NOMADS/FTPPRD")

Rel(cmsNew, forecastWt, "Uses", "HTTPS")
Rel(cmsNew, forecastBest, "Uses", "HTTPS")
Rel(cmsCf, forecastCf, "Uses", "HTTP")

Rel_U(tidePoints, tideSl, "Pushes", "Script to push data to")

' Surfline New/Old
Rel_D(customerSl, slNew, "Uses", "HTTPS")
Rel_D(customerSl, slCf, "Uses", "HTTP")
Rel_D(customerSl, slIos, "Uses", "App")
Rel_D(customerSl, slAndroid, "Uses", "App")

Rel_D(slApiPartners, slCf, "Uses", "HTTP")

Rel_D(slNew, services, "Uses", "HTTPS")
Rel_D(slIos, services, "Uses", "HTTPS")
Rel_D(slAndroid, services, "Uses", "HTTPS")

Rel_D(slNew, slCf, "Uses", "HTTP")
Rel_D(services, slCf, "Uses", "HTTP")
Rel_D(slIos, slCf, "Uses", "HTTP")
Rel_D(slAndroid, slCf, "Uses", "HTTP")

Rel(slCf, forecastCf, "Uses", "MSSQL")
Rel(forecastCf, modelLola, "Uses", "NFS - Ocean")
Rel(forecastCf, modelWrf, "Uses", "NFS - Ocean")
Rel(slCf, tidePoints, "Uses", "MSSQL")
Rel(slCf, bw, "Uses", "HTTP")
Rel(slCf, cmsCf, "Uses", "MSSQL/HTTP")
Rel_L(slCf, slNew, "Uses", "HTTPS")

Rel(services, forecastWt, "Uses", "HTTPS")
Rel(slNew, forecastBest, "Uses", "HTTPS")
Rel(services, tideSl, "Uses", "HTTPS")
Rel(slNew, cmsWp, "Uses", "HTTPS")
Rel(slNew, subscriptionWt, "Uses", "HTTPS")
Rel(subscriptionWt, stripe, "Uses", "HTTPS")
Rel(subscriptionWt, google, "Uses", "HTTPS")
Rel(subscriptionWt, apple, "Uses", "HTTPS")

Rel(slNew, cameras, "Uses", "HTTPS")
Rel(slNew, notifications, "Uses", "HTTPS")

Rel(sessionsPartners, slNew, "Uses", "HTTPS")

SHOW_DYNAMIC_LEGEND()

@enduml
