# Forecast Engineering PyTorch Machine Learning Model Platform

## What are we doing?

Providing a platform for management and execution of PyTorch machine learning models used in forecast engineering.

## What problem are we solving?

The forecast platform does not currently support surf ratings computed by machine learning models from the data science squad, and there will be an increasing number of ML driven features in the forecast platform in the future. 
The forecast platform should be able to integrate new ML models from the data science squad in a stable way with minimal or no involvement from forecast engineering.

## What impact does this have on the business?

Efficient delivery of ML driven features such as surf ratings which will increase user conversion.

## What is the solution?

[TorchServe](https://pytorch.org/serve/) server to [manage](https://pytorch.org/serve/management_api.html) and [invoke](https://pytorch.org/serve/inference_api.html) PyTorch machine learning models used within the forecast platform.

- ### Development and deployment process
   1. Data Science creates a new PyTorch model (and custom handler if required)
   2. Model archive is created with [torch-model-archiver](https://github.com/pytorch/serve/tree/master/model-archiver) CLI
   3. Model archive is uploaded to S3 bucket
   4. Model configuration is added to [TorchServe configuration](https://pytorch.org/serve/configuration.html#config-model)
   5. TorchServe is deployed with our normal CICD process for ECS services

- ### Model package requirements
   - A name for the model
   - Serialized model file (.pt or .pth)
   - Model handler (can use a pre-built TorchServe handler or custom)
   - Extra files (any additional files required to run handler and model, OpenAPI spec)
   - Requirements file (model specific packages required for handler/model)

## How does this solve the problem?

TorchServe has [purpose-built internal architecture](https://github.com/pytorch/serve/blob/master/README.md#torchserve-architecture)
to manage, run, and monitor PyTorch models without custom code:

![torchserve_internal_arch.png](torchserve_internal_arch.png)

Using an off-the-shelf solution from the ML framework authors will allow us to:
- Support multiple models / versions with no custom code
- Adhere to the ML framework best practices with minimal effort
- Adopt updates to the ML framework with minimal effort

## What do we know already?

- _How are model archives created?_
  - [_A key feature of TorchServe is the ability to package all model artifacts into a single model archive file. It is a separate command line interface (CLI), torch-model-archiver, that can take model checkpoints or model definition file with state_dict, and package them into a .mar file. This file can then be redistributed and served by anyone using TorchServe._](https://github.com/pytorch/serve/tree/master/model-archiver#torch-model-archiver-for-torchserve)

- _Can we customize the pre/post processing and request handling for each model?_
  - Yes. [Initialization, pre/postprocessing, request handling for each model can be customized and included in the packaged model](https://pytorch.org/serve/custom_service.html#advanced-custom-handlers)

- _Can we run models on different hardware?_
  - Yes. Models can be served on CPU or [GPU](https://pytorch.org/serve/use_cases.html#serve-models-on-gpus)
  
- _Can we add custom endpoints to TorchServe?_
  - Yes, they are written in Java. We also plan to experiment with adding a 'model' that only runs python logic in the handler without invoking any ML model.
  
- _Will running the PyTorch models on our ECS cluster cause issues? Should this service be on a separate cluster?_
  - TBD
  
## What are the alternatives?

- [This article](https://neptune.ai/blog/best-8-machine-learning-model-deployment-tools) gives a brief overview of some popular ML model deployment tools

- Create custom API and infrastructure to support each single machine learning model using an existing pattern: 
  
  - examples:
    - [Breaking wave height API: TensorFlow model wrapped in Sanic framework](https://github.com/Surfline/wavetrak-services/tree/master/services/breaking-wave-height-api/lib/machine_learning_model)
    - [Forecast API: PyTorch model wrapped in Flask framework](https://github.com/Surfline/surfline-labs/tree/master/forecast_api)
  
  - pros:
    - more customizable
  
  - cons:
    - requires forecast engineers to learn ML frameworks and maintain custom code around each model
    - does not adhere to ML framework best practices out-of-the-box
    - does not support multiple models easily
    - does not evolve with the ML framework
    
- [AWS SageMaker](https://aws.amazon.com/sagemaker/)

  - pros:
    - stability
    - supports multiple ML frameworks
  
  - cons: 

- [BentoML](https://github.com/bentoml/BentoML)
  
  - pros:
    - supports multiple ML frameworks

  - cons:
  
