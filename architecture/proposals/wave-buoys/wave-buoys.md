# Wave Buoys Architecture

## What are we doing?

We are rebuilding the Wave Buoys system to be a one-platform solution for Surfline and Magicseaweed. The system will be added to the Forecast Platform. Real-time data and historical data will be available. In the future, wind stations will also be added.

## What problem are we solving?

Currently, Surfline and Magicseaweed use 2 different systems to provide wave buoy data. Both systems contain legacy code that is hard to maintain. Outages are also difficult to resolve because there is little shared knowledge about these systems. For example, buoys on the Surfline product are displayed on old Cold Fusion webpages. The new wave buoys system will conslidate the 2 different systems into one.

## What impact does this have on the business?

Wave buoys are part of the premium experience. It is crucial that we provide our customers with reliable, relevant, and up-to-date information.
Adding wave buoys to the Forecast Platform will enable us to serve buoy data with less downtime. It will allow us to teardown several legacy codebases, providing a potential cost savings as well. Serving wave buoy data through a single API will enable us to create a whole new set of UI features in the future. The forecast team also use the buoy data to validate our swell models (i.e. Lotus). The validation further enriches the product experience.

## What is the solution?

Wave buoys will be added to the Forecast Platform. This enables us to have a unified platform for wave buoys and forecast models. Many existing patterns established on the Forecast Platform will be used to iterate on this initiative quicker. There will be no CMS for wave buoys, and the wave buoy table will be updated by the processing jobs. Wave buoy data will come from 5 different sources, all linked below:
- [AUS](https://apps.des.qld.gov.au/data-sets/waves/wave-7dayopdata.csv)
- [Channel UK](https://www.channelcoast.org/realtimedata/)
- [CDIP](http://cdip.ucsd.edu/data_access/sccoos.cdip)
- [NDBC](https://www.ndbc.noaa.gov/to_station.shtml)
- [SoFar](https://docs.sofarocean.com/spotter-sensor)

The `station` naming is used so that data from wave buoys and wind stations can be stored in the same table. The characteristics of wave buoys and wind stations are very similar. Wind stations will be added a later date. The process is described below:

1. `insert-buoy-data-from-source` Airflow task implements the following logic (there will be a separate task for each source):
    ### Downloading:
    - The latest buoy data is downloaded from the source. Buoys update independently of each other and at different times. Processing immediately starts after downloading. Only a single task runs at a given time (no concurrent tasks).
    - The set of data to be downloaded is based on the `latest_timestamp` column in the `stations` table for each buoy. If the `latest_timestamp` is more than 72 hours from the current UTC time, then the last 72 hours of data is downloaded.
    - The following python libaries will be used to download the data:
      - `requests`
      - `bs4`
      - `boto3`
      - `pyKML` (for `Channel UK`)
    ### Processing:
    - Swell partitions are computed from wave spectra (if available, some buoys do **not** have wave spectra). The `spectra_to_swell_partitions` code will be included in the `wavetrak-science-algorithms` pip package. This code will be given by the Science team. The logic is as follows:
      - If the spectra is from the [NDBC](https://www.ndbc.noaa.gov/to_station.shtml) source:
        - The spectra is converted into swell partitions.
      - If the spectra is **not** from the [NDBC](https://www.ndbc.noaa.gov/to_station.shtml) source:
        - Non-NDBC spectra is converted to NDBC format.
        - The spectra is then converted into swell partitions.
    ### Storing:
    - **New** buoys:
      - The buoy is added into the `stations` table with the status set to `ONLINE` and the `latest_timestamp`. The buoy data is also added to the `station_data` table.
    - **Existing** buoys:
      - If there is **new** data:
        - The following columns for the `stations` table are updated: `status`, `latest_timestamp`, `latitude`, and `longitude` values.  The status of the buoy is set to `ONLINE`. The new data is also added to the `station_data` table.
      - If there **no** new data:
        - Neither table is updated.
    - The schema for the `station_source` table (the `station` naming is used for both wave buoys and wind stations) is as follows:

      `station_source`:
      ```
      source: text NOT NULL
      copyright_text: text
      copyright_link: text
      created_at: timestamp without time zone NOT NULL
      updated_at: timestamp without time zone NOT NULL
      ```
    - The schema for the `stations` table (the `station` naming is used for both wave buoys and wind stations) is as follows:

      `stations`:
      ```
      id: UUID NOT NULL
      source: text NOT NULL
      source_id: int NOT NULL
      status: text NOT NULL
      reports_wave: boolean not NULL
      reports_wind: boolean not NULL
      name: text
      latitude: numeric NOT NULL
      longitude: numeric NOT NULL
      latest_timestamp: numeric NOT NULL
      created_at: timestamp without time zone NOT NULL
      updated_at: timestamp without time zone NOT NULL

      Primary Key: id
      Foreign key: source -> source in `station_source` table.
      Unique Constraint: source, source_id
      ```
    - The schema for the `station_data` is as follows:

      `station_data`:
      ```
      station_id: UUID not NULL
      timestamp: timestamp without time zone NOT NULL
      significant_height: numeric
      peak_period: numeric
      water_temperature: numeric
      air_temperature: numeric
      wind_speed: numeric
      wind_direction: numeric
      wind_gust: numeric
      pressure: numeric
      dew_point: numeric
      elevation: numeric
      swell_wave_1_height: numeric
      swell_wave_1_period: numeric
      swell_wave_1_direction: numeric
      swell_wave_1_impact: numeric
      swell_wave_2_height: numeric
      swell_wave_2_period: numeric
      swell_wave_2_direction: numeric
      swell_wave_2_impact: numeric
      swell_wave_3_height: numeric
      swell_wave_3_period: numeric
      swell_wave_3_direction: numeric
      swell_wave_3_impact: numeric
      swell_wave_4_height: numeric
      swell_wave_4_period: numeric
      swell_wave_4_direction: numeric
      swell_wave_4_impact: numeric
      swell_wave_5_height: numeric
      swell_wave_5_period: numeric
      swell_wave_5_direction: numeric
      swell_wave_5_impact: numeric
      swell_wave_6_height: numeric
      swell_wave_6_period: numeric
      swell_wave_6_direction: numeric
      swell_wave_6_impact: numeric
      created_at: timestamp without time zone NOT NULL
      updated_at: timestamp without time zone NOT NULL

      Primary Key: id, timestamp
      Foreign Key: station_id -> id in `stations` table
      Indexes: (id, timestamp DESC)
      ```
  - The following python libaries will be used to parse and insert data:
    - `boto3`
    - `psycopg2`
    - `wavetrak-science-algorithms`
2. `update-station-status-and-type` Airflow task implements the following logic (a single task for **all** station sources):
    - The `status` of buoys that have not been updated in 48 hours or more from the current UTC time are set to `OFFLINE`. This is because buoys may stop reporting data for a variety of reasons. For example, there may be a temporary network outage. Or a sensor might need to be replaced, in which case the buoys may stop reporting for several days. We don't receive information from the source about why a buoy stopped reporting, so after 48 hours we assume the buoys is offline. Using these statuses we can communicate to the user that a buoy they used to see is now offline. This system is designed to handle all cases in which a buoy might stop reporting.
    - The `reports_wind` and `reports_weather` columns in the `stations` table are updated for each buoy. The `reports_wind` column is set to `true` if `wind_speed` is non-null in the last 30 days, otherwise it is set to `false`. The `reports_wave` column is set to `true` if the `significant_height` columns is non-null in the last 30 days, otherwise it is set to `false`.
3. Buoy data can then be retrieved via the Science Data Service, which supports the following [mocks](https://app.abstract.com/projects/0ae2c0d0-4030-11e7-9bc4-3338b84102bb/branches/master/commits/cec3f9510ddfb67d6a74ce7379a04417d407b9e5/files/1BC5F36A-1C4C-4BAE-90BB-558FA6B07B6A/layers/61413A9A-C519-42DD-A145-8C6B2D7FF6A3). The GraphQL Spec is as follows:
    ```
    type Query {
      station(
        id: String
      ): HistoricalStationData
      stations(
        lat: Float!,
        lon: Float!,
        radius: Float!,
        status: StationStatus = ONLINE,
        reportsWind: Boolean = True,
        reportsWave: Boolean = True,
      ): [LatestStationData]
    }
    type Station {
      id: String!
      type: StationType!
      reportsWind: Boolean!
      reportsWave: Boolean!
      source: String!
      sourceID: Int!
      name: String
      status: StationStatus!
      latitude: Float!
      longitude: Float!
      latestTimestamp: String!
    }
    type LatestStationData {
      station: Station
      latestData(
        waveHeight: String = "m",
        windSpeed: String = "km/h",
        windGust: String = "km/h",
        seaTemperature: String = "C",
        airTemperature: String = "C",
        dewPoint: String = "C",
        elevation: String = "m",
        pressure: String = "mb"
      ): StationData
    }
    type HistoricalStationData {
      station: Station,
      data(
        start: Int,
        end: Int,
        waveHeight: String = "m",
        windSpeed: String = "km/h",
        windGust: String = "km/h",
        seaTemperature: String = "C",
        airTemperature: String = "C",
        dewPoint: String = "C",
        elevation: String = "m",
        pressure: String = "mb"
      ): [StationData]
    }
    type StationData {
      timestamp: String!
      height: Float
      period: Float
      sst: Float
      airTemperature: Float
      dewPoint: Float
      pressure: Float
      wind: Wind
      elevation: Float
      swells: [SwellComponent]
    }
    enum StationStatus {
      ONLINE
      OFFLINE
    }
    ```
    1. Query using `id` for a single station that can retrieve all current and historical data. This defaults to the latest data for a station, but a specific time range can also be given. (This has the potential to become slow, but assume a slow query is not a problem for now). Example:
    ```
    query station(id: "123-454-839-230"){
      source
      id
      status
      data {
        timestamp
        period
        height
      }
    }

    Returns:
    "stations": [
      {
        "source": "AUS",
        "id": "122-2323-242-232",
        "status": "ONLINE",
        "data": [
          {
            "timestamp": "2020-06-20 09:55:00",
            "period": 1.03,
            "height": 3.4
          }
        ]
      }
    ]
    ```
    2. Query using a radius to get the latest data. This will be possible using `PostGIS` under the hood. An index on the `station_data` will enable fast lookups for the latest data. The radius is set to `km`. Example query:
     ```
    query stations(lat=29.5, lon=100, radius=5){
      id
      source
      sourceID
      latestData {
        timestamp
        height
        period
      }
    }

    Returns:
    "stations": [
      {
        "source": "AUS",
        "id": "122-2323-242-232",
        "status": "ONLINE",
        "latestData": {
            "timestamp": "2020-06-19 09:55:00",
            "period": 1.03,
            "height": 3.4
        }
      },
      {
        "source": "CDIP",
        "id": "456-2323-242-232",
        "status": "OFFLINE",
        "latestData": {
            "timestamp": "2020-06-19 10:45:00",
            "period": 2.5,
            "height": 3.0
        }
      }
    ]
    ```

### Diagram:

![Alt text](wave-buoys.png?raw=true "Wave Buoys Architecture")

## How does this solve the problem?

Wave buoy data will be managed and provided by the Forecast Platform. This allows us to rebuild the wave buoy system to our latest standards while minimizing the scope of the project. The data will served agnostically to fit the needs of Surfline and Magicseaweed. We will no longer rely on old ETL pipelines or Cold Fusion APIs.

## What are the alternatives?

1. [Build upon the partially finished download/processing jobs](https://github.com/Surfline/science-scripts/tree/master/scripts/download/buoy):
  - Pros:
    - The overall scope of the project would be smaller:
      - Downloading and processing logic is already written.
      - Airflow DAGS are already setup.
  - Cons:
    - Relies on files stored on `/ocean` which could be less reliable and is a pattern we trying to move away from.
    - Missing the `Channel UK` and `SoFar` data sources.
    - Written mostly in bash which is more difficult to iterate on if needed.
    - Difficult to write tests for.

2. Build an entirely new microservice:
  - Pros:
    - The service can be tailored to fit the needs of live buoy data. The Science Data Service is geared towards serving forecast data.
    - Helps us move to a micro service oriented architecture.
    - Can scale the infrastructure independently of the Science Data Service.
  - Cons:
    - Significantly increases the scope of the project (new database, new backend application, etc).
    - The maintenance of science data would be split between 2 different services (wave buoys and and science data service).

3. Separate downloads and processing (add S3/SQS):
  - Pros:
    - Downloading from source and parsing files can be separated, which could be helpful when there are network outages.
    - Can retry parsing for a buoy if needed.
  - Cons:
    - Slower to serve data. Buoy data is given in real time so it is critical to serve the data as quickly as possible.
    - Increases overall infrastructure for the wave buoys system.
    - Makes application code more complex by having to consume SQS messages.

## What do we know already?

- Wave buoy data is finicky:
    - New buoys can be added at any time.
    - Buoys may stop reporting data at any time.
    - Buoys may resume reporting data at any time.
    - Only some buoys may have certain variables (ex: wave spectra data).
- Buoys IDs are unique to each source.
- Wave buoy data is given in real-time, so we need wave buoy data to be served as quickly as possible.
- We don't have control over the wave buoys available (i.e. via a CMS), so we rely entirely on 3rd party data.
- There is little knowledge of the current wave buoy system. This makes maintenance and development very difficult.

## Q & A
- Will there be a CMS for buoys?
  - There will be no CMS for buoys. We rely entirely on 3rd party data for wave buoys. As such, buoys are added and updated by the processing jobs. This means the buoys are dynamic and may be added at any time.
- Why is the DB and GraphQL schema named `stations` and not `buoys`?
  - The `stations` name is used so that data from wave buoys and wind stations can be stored in the same table. The characteristics of wave buoys and wind stations are very similar. The focus right is now on wave buoys, but wind stations will be added a later date.
- Will all buoys have wave spectra?
  - Not all wave buoys will have spectra. For example, the [Australian](https://apps.des.qld.gov.au/data-sets/waves/wave-7dayopdata.csv) buoy data source does not have spectra. For those buoys that do not have spectra data, there will be no swell partition information.
- Do all wave buoys update at the same time?
  - Wave buoys are independent of one another and may not report at the same time. Unlike the Forecast models (`Lotus`, `GFS`, etc) we do not wait for all buoys to be updated. New buoy data will be processed as soon as its available.
- What happens when a wave buoy stops reporting?
  - Buoys that have stopped reporting (either temporarily or permanently) will be set to `OFFLINE` after 48 hours (this is configurable) from the last reported time. We don't receive information from the source about why a buoy stop reporting, so after 48 hours we assume the buoy is offline. If a buoy starts reporting again, we will set the status back to `ONLINE`.
- How far in the past will historical data be stored?
  - All historical data for each buoy will be stored.
