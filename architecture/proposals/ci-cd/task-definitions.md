# Decision Document: Migrate task definitions configuration

## Table of Contents <!-- omit in toc -->

- [What are we doing?](#what-are-we-doing)
- [What problem are we solving?](#what-problem-are-we-solving)
  - [Current status](#current-status)
  - [Custom structures](#custom-structures)
  - [Desired structure](#desired-structure)
- [What is the solution?](#what-is-the-solution)
  - [Proposed structure](#proposed-structure)
  - [Custom services configuration](#custom-services-configuration)
  - [How we implement it?](#how-we-implement-it)
- [What are the alternatives?](#what-are-the-alternatives)

## What are we doing?

We're moving the container configurations of our services to their corresponding `ci.json` files in order to have a single point of truth to define the task definitions, ECS parameters and secrets retrieved from Parameter Store.

## What problem are we solving?

The services in this repository, and the new ones we're going to add in the near future, have different customizations. Although most of them fall into a group "ci.json / Dockerfile / docker-compose.yml", a few of them also have a "ecs-params.yml" file or a "pre-docker-build-actions.sh" script.

### Current status

The `ci.json` files have all the service information, like the image name, cluster, secrets, task role ARNs, release strategies, etc.

The `Dockerfile` files are used to build and push the images to ECR and they're the actual files making the services work.

The `docker-compose.yml` files have almost the same structure and information for all of the services:

```yaml
version: '2'
services:
  charts-service:
    mem_reservation: 256m
    mem_limit: 768m
    image: ${REPOSITORY_HOST}/services/charts-service:${IMAGE_TAG}
    ports:
      - "8080"
    logging:
      driver: "awslogs"
      options:
        awslogs-group: "sl-logs-core-svc-${ENVIRONMENT}"
        awslogs-region: "us-west-1"
        awslogs-stream-prefix: "services"
```

> Also, we keep these Docker Compose files in a different `docker/` directory, far away from the services' structure we're working on.

### Custom structures

In the case of **Science Models DB**, this file is slightly different:

```yaml
version: '2'
services:
  science-models-db:
    ulimits:
      nofile:
        soft: 1024000
        hard: 1024000
    image: ${REPOSITORY_HOST}/services/science-models-db:${IMAGE_TAG}
    ports:
      - "8080"
    logging:
      driver: "awslogs"
      options:
        awslogs-group: "wt-logs-science-models-db-${ENVIRONMENT}"
        awslogs-region: "us-west-1"
        awslogs-stream-prefix: "services"
    environment:
      - DATA_PATH=/opt/app/data
    volumes:
      - /mnt/storage/data:/opt/app/data
```

An `ecs-params.yml` file could be the one present, also for **Science Models DB**:

```yaml
version: 1
task_definition:
  services:
    science-models-db:
      mem_reservation: 1024m
```

### Desired structure

While working with Github Actions and attempting to work with official actions from Github or AWS, we find that files like `docker-compose.yml` and `ecs-params.yml` are not needed it we define everything in a `task-definition.json` file, as explained in this [AWS documentation](https://docs.aws.amazon.com/AmazonECS/latest/userguide/task_definition_parameters.html).

As we always try to use the best practices and official repositories or applications, AWS suggest to deploy a task definition by sending a `task-definition.json` file:

```yaml
  - name: Deploy to Amazon ECS
    uses: aws-actions/amazon-ecs-deploy-task-definition@v1
    with:
      task-definition: task-definition.json
      service: my-service
      cluster: my-cluster
      wait-for-service-stability: true
```

Also if we need to retrieve information about an existing task definition, to modify its data and deploy it again, AWS has an official action for that:

```yaml
  - name: Render Amazon ECS task definition
    id: render-web-container
    uses: aws-actions/amazon-ecs-render-task-definition@v1
    with:
      task-definition: task-definition.json
      container-name: web
      image: amazon/amazon-ecs-sample:latest
```

In addition, we can remove the `wavetrak-infrastructure/docker/serviceMaps.json` file, with information that could perfectly fit in `ci.json` files.

Our efforts can be focused in the creation of this Task Definition JSON file.

## What is the solution?

On standard ECS services, moving configurations from Docker Compose and ECS Parameter files to the service's `ci.json` file, like `mem_limit`, `mem_reservation` and `port` settings, frees us from the need to keep those files.

The script building the Task Definitions can take care of the settings and to create reliable configurations using best and updated practices. It also will be easier to maintain and we can trust we have of all our services configuration in one place, without being afraid of losing any infrastructure or service data.

### Proposed structure

Add the next structure in the service's `ci.json` files:

```json
  ...
  "container": {
    "memoryLimit": "768m",
    "memoryReservation": "512m",
    "port": 8080
  }
  ...
```

### Custom services configuration

In case a service uses several microservices or a different configuration structure, we should keep having custom `docker-compose.yml` and `ecs-params.yml` files into the service's folder, so the script building the Task Definition overrides the generic configuration structure with them.

### How we implement it?

We can start with a few configurations like the mentioned above, but as CI/CD Pipeline gain more trust, the services can gain more control over their microservice infrastructure. These settings can be replaced in placeholders on a template file, so the more we're mature on our process, we can add more configurations and container definitions.

Example `task-definition.json.j2` template file:

```json
{
  "family": "{{ service-name }}",
  "taskRoleArn": "{{ task-role-arn }}",
  "executionRoleArn": "{{ task-role-arn }}",
  "containerDefinitions": [
    {
      "name": "{{ service-name }}",
      "image": "{{ image-host }}/{{ image-name }}:{{ image-tag }}",
      "cpu": "{{ cpu }}",
      "memory": "{{ memory-limit }}",
      "memoryReservation": "{{ memory-reservation }}",
      "portMappings": [
        {
          "containerPort": "{{ port }}",
          "hostPort": 0,
          "protocol": "tcp"
        }
      ],
      "secrets": [],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "{{ logs-group }}",
          "awslogs-region": "{{ logs-region }}",
          "awslogs-stream-prefix": "{{ logs-prefix }}"
        }
      }
    }
  ],
  "cpu": "{{ cpu }}",
  "memory": "{{ memory-limit }}",
  "tags": []
}
```

The good news is we already count with all these configurations. Migrating the settings from Docker Compose files and ECS parameters to this format will be straight forward.

## What are the alternatives?

Keep working as now, but according to the new Monorepo structure and contracts, we need to move the Docker Compose and ECS Parameters files already into services folders.

Regarding ECS Parameters, the current Jenkins job `deploy-service-to-ecs` makes use of the AWS `ecs-cli`, which grabs the parameters and adapts them to the needed format. The work here is already done, even if the paths change when switching to new monorepo structure.
