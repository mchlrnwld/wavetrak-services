# CI/CD Vendor Selection

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Overview](#overview)
- [Process](#process)
- [Potential Vendors](#potential-vendors)
  - [Github Actions](#github-actions)
  - [Codefresh](#codefresh)
  - [Jenkins](#jenkins)
  - [Jenkins X](#jenkins-x)
  - [CircleCI](#circleci)
  - [Travis CI](#travis-ci)
  - [GitLab](#gitlab)
  - [Bitbucket](#bitbucket)
- [Decision Matrix](#decision-matrix)
  - [Requirement Descriptions](#requirement-descriptions)
  - [Pricing Details](#pricing-details)
    - [Github Actions Pricing Details](#github-actions-pricing-details)
    - [Codefresh Pricing Details](#codefresh-pricing-details)
    - [Jenkins Pricing Details](#jenkins-pricing-details)
    - [Jenkins X Pricing Details](#jenkins-x-pricing-details)
    - [CircleCI Pricing Details](#circleci-pricing-details)
    - [Travis CI Pricing Details](#travis-ci-pricing-details)
    - [GitLab Pricing Details](#gitlab-pricing-details)
    - [Bitbucket Pricing Details](#bitbucket-pricing-details)
- [Recommendation](#recommendation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Overview

To support our initiative to compose CI/CD pipelines for a range of artifacts that allow us to quickly ship changes with high confidence, we need to move to a new CI/CD platform. This document compares leading CI/CD platforms and recommends a platform to use.

## Process

This is our typical vendor selection process:

1. Perform preliminary vendor screening and determine shortlist
2. Assess vendors and develop scorecard
3. Conduct vendor demos, requirement fit/gap analysis, and pricing analysis
4. Summarize results and make recommendation

## Potential Vendors

### Github Actions

<https://github.com/features/actions>

**Pros**

- Modern CI/CD platform
- Built around composable build pipelines
- Tightly integrated with our code repositories in Github
- Fast and stable
- Rich ecosystem of prebuilt pipeline stages
- Strong community support
- Healthy platform with expected long lifespan
- Works well for microservices on ECS
- Supports future potential upgrade paths to Kubernetes and GitOps
- 30 minute SLA, 24/7 support

**Cons**

- Mediocre observability
  - Pipeline execution visibility is good
  - Overall observability can be improved with [Meercode.io](https://meercode.io/monitor)
    - Still not great but Meercode is very open to feature requests
  - No concept of a "release board"
- So-so for data migrations
  - Requires workers running within firewall

### Codefresh

<https://codefresh.io/>

**Pros**

- Modern CI/CD platform 
- Strengths in Kubernetes, GitOps, and observability
- Robust selection of plugins

**Cons**

- Execution speed is concerning
  - Simple "Hello World" build pipelines take around four minutes to run
- For microservices on ECS, it is middle-tier with reduced observability

### Jenkins

<https://www.jenkins.io/>

**Pros**

- Solid, workhorse CI/CD platform
- Highly extensible
- Very stable

**Cons**

- Very high operational overhead
  - Self-hosted
  - There are managed Jenkins services available but there's still a high operational overhead in maintaining build pipelines
- Slow
  - It can be tuned to be fast but it takes a lot of work to dial in
- Good plugin ecosystem but most plugins require significant customization
  - Many plugins are also out-of-date or abandoned
- Most build steps must be manually scripted

### Jenkins X

<https://jenkins-x.io/>

**Pros**

- CI/CD platform built for (and on) Kubernetes
- Pipeline automation
- GitOps out-of-the-box
- Automatic preview environments
- Built-in ChatOps
- Small set of plugins under active development

**Cons**

- Self-hosted (on Kubernetes)
- Purpose-built for Kubernetes

### CircleCI

<https://circleci.com/>

**Pros**

- Solid CI/CD platform
- Built around pipelines
- Provides a rich selection of container images with preinstalled build tools
- Suports "Orbs", which are effectively plugins

**Cons**

- Pipelines require a lot of custom code
- Hasn't kept pace with newer platforms like Github Actions

### Travis CI

<https://travis-ci.com/>

**Pros**

- Solid CI/CD platform
- Built around pipelines

**Cons**

- Hasn't kept pace with newer platforms like Github Actions
- Requires a lot of custom code

### GitLab

<https://about.gitlab.com/>

**Pros**

- Modern CI/CD platform
- Built around composable build pipelines
- Fast and stable
- Healthy platform with expected long lifespan
- Supports future potential upgrade paths to Kubernetes and GitOps

**Cons**

- Requires code to be stored in GitLab
  - Moving from Github to GitLab would significantly expand the scope of this initiative
  - Benefits of GitLab outweighed by the trouble to migrate repos
- Not great for data migrations
- Philosophically opposed to plugins ([link](https://about.gitlab.com/blog/2019/09/27/plugin-instability/))

### Bitbucket

<https://bitbucket.org/product/features/pipelines>

**Pros**

- Great integration with other Atlassian tools like Jira
- Fast and stable
- Healthy platform with expected long lifespan

**Cons**

- Requires code to be stored in Bitbucket
  - Moving from Github to Bitbucket would significantly expand the scope of this initiative
  - Benefits of Bitbucket outweighed by the trouble to migrate repos
- Not great for data migrations

## Decision Matrix

In the table below, all requirements and priorities are ranked from 1 - 3:

- 1 &rarr; good / high-priority
- 2 &rarr; neutral / medium-priority
- 3 &rarr; bad / low-priority

| Requirement                | Priority | Github Actions | Codefresh | Jenkins | Jenkins X | CircleCI | Travis | GitLab     | Bitbucket |
| :----                      | :----:   | :----:         | :----:    | :----:  | :----:    | :----:   | :----: | :----:     | :----:    |
| **Pipelines**              | 1        | 1              | 1         | 2       | 1         | 1        | 1      | 1          | 1         |
| **Plugin Ecosystem**       | 1        | 1              | 2         | 2       | 2         | 2        | 3      | 3          | 3         |
| **Monorepo Compatibility** | 1        | 1              | 1         | 1       | 2         | 2        | 3      | 2          | 2         |
| **Developer Experience**   | 1        | 2              | 2         | 2       | 2         | 2        | 2      | 1          | 2         |
| **Local Pipeline Dev**     | 2        | 2              | 2         | 2       | 2         | 2        | 2      | 1          | 2         |
| **Observability**          | 2        | 2              | 2         | 2       | 1         | 1        | 2      | 2          | 2         |
| **Speed**                  | 1        | 1              | 3         | 2       | 2         | 1        | 3      | 1          | 1         |
| **Operational Cost**       | 1        | 1              | 1         | 3       | 2         | 2        | 2      | 1          | 2         |
| **Security Features**      | 2        | 1              | 1         | 3       | 3         | 1        | 3      | 1          | 1         |
| **Effort to Learn**        | 2        | 2              | 2         | 2       | 2         | 2        | 2      | 2          | 2         |
| **Training Availability**  | 3        | 2              | N/A       | 1       | 1         | 1        | N/A    | 1          | 1         |
| **SLAs**                   | 1        | 1              | 2         | N/A     | N/A       | 1        | N/A    | 1          | 1         |
| **Professional Services**  | 3        | 1              | N/A       | 1       | 1         | 1        | N/A    | 1          | 1         |
| **Community Support**      | 1        | 1              | 2         | 1       | 2         | 2        | 2      | 2          | 2         |
| **ECS Integration**        | 1        | 1              | 1         | 2       | N/A       | 2        | 2      | 1          | 2         |
| **GitOps Support**         | 3        | 2              | 1         | 3       | 1         | 2        | 2      | 1          | 2         |
| **Kubernetes Integration** | 3        | 2              | 1         | 2       | 1         | 2        | 2      | 1          | 2         |
| **BYO Workers**            | 2        | 1              | 1         | 1       | 1         | 2        | 2      | 1          | Planned   |
| **API Provided?**          | 3        | 1              | 1         | 2       | 2         | 1        | 1      | 1          | 2         |
| **Vendor Lock-in**         | 3        | 3              | 3         | 2       | 2         | 2        | 2      | 3          | 2         |
| **Pricing (Monthly)**      | 2        | $156/$630      | $356+     | $350    | $350      | $450+    | $250   | $570/$2970 | $300+     |

### Requirement Descriptions

The following table describes what each requirement actually is. This is included in a separate table to keep the decision matrix table simple.

| Requirement                | Description                                                                                                   |
| :----                      | :----                                                                                                         |
| **Pipelines**              | Does the platform support build pipelines?                                                                    |
| **Plugin Ecosystem**       | How robust is the platform's plugin ecosystem, if it exists at all?                                           |
| **Monorepo Compatibility** | How well does the plaform work with monorepos?                                                                |
| **Developer Experience**   | What's the developer experience like?                                                                         |
| **Local Pipeline Dev**     | Can pipelines be developed and run locally?                                                                   |
| **Observability**          | How observable are the build and release pipelines?                                                           |
| **Speed**                  | How fast do builds run on the platform, both in cold-start time and execution time?                           |
| **Operational Cost**       | How much effort does it take to keep the platform running smoothly?                                           |
| **Security Features**      | How secure is the platform? Does it support 2FA and/or SSO? How are secrets stored? Have there been breaches? |
| **Effort to Learn**        | How quickly can developers come up to speed?                                                                  |
| **Training Availability**  | Is training available, either from the vendor or from a third-party?                                          |
| **SLAs**                   | Does the platform have an SLA? Is it reasonable?                                                              |
| **Professional Services**  | Are professional services available, either from the vendor or from a third-party?                            |
| **Community Support**      | How robust is community support for the platform?                                                             |
| **ECS Integration**        | How well does the platform integrate with ECS?                                                                |
| **GitOps Support**         | Does the platform support GitOps? Out of the box or does it require customization?                            |
| **Kubernetes Integration** | How well does the platform integrate with Kubernetes?                                                         |
| **BYO Workers**            | Can BYO workers be used to run pipeline tasks? How difficult are they to manage?                              |
| **API Provided?**          | Is an API provided for the platform?                                                                          |
| **Vendor Lock-in**         | How tightly will we be tied to the platform?                                                                  |
| **Pricing**                | How expensive is it to run the platform?                                                                      |

### Pricing Details

CI/CD platforms all have different methods of pricing. For purposes of evaluation, we should assume the following pricing requirements:

- 30 developers
- 4 concurrent builds
- 50 builds per day
- 5 minutes per build
- 7500 build-minutes per month

#### Github Actions Pricing Details

Team: 30 users * $4/user + $0.008/min * (7500 min - 3000 min) = $156/month

Enterprise: 30 users * $21/user = $630/month

#### Codefresh Pricing Details

4 concurrent medium builds = $356/month (+ unknown $$$ for 50 additional users)

#### Jenkins Pricing Details

Current average monthly EC2 cost via Cloudhealth: $348.11/month

#### Jenkins X Pricing Details

Current average monthly EC2 cost via Cloudhealth: $348.11/month

#### CircleCI Pricing Details

30 users * $15/user = $450/month + ??? for additional build minutes

#### Travis CI Pricing Details

5 concurrent jobs, priced monthly = $249/month

#### GitLab Pricing Details

Premium: 30 users * $19/user = $570/month

Ultimate: 30 users * $99/user = $2970/month

#### Bitbucket Pricing Details

30 users * $10/user = $300/month + ??? for additional build minutes

## Recommendation

We should use Github Actions as our CI/CD platform. It performs exceptionally well given our set of requirements, it's competitively-priced, and it's well suited for potential future expansion to Kubernetes and GitOps. It has strong community support and is likely to perform strongly for years to come.

GitLab ranks a strong second-place. It's a very compelling CI/CD platform with a great feature set. It's quite a bit more expensive than any other CI/CD platform and it would require significant effort to migrate our repositories from Github to GitLab.
