# CI/CD Contracts

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Overview](#overview)
- [Application Locations](#application-locations)
- [General CI/CD Flow](#general-cicd-flow)
- [What If A Command Doesn't Exist?](#what-if-a-command-doesnt-exist)
- [Determining Application Language](#determining-application-language)
- [Code Coverage](#code-coverage)
  - [Code Coverage Example](#code-coverage-example)
- [Accessing Private AWS Resources](#accessing-private-aws-resources)
  - [Node.js Self-Hosted Runner Example](#nodejs-self-hosted-runner-example)
  - [Python/Other Language Self-Hosted Runner Example](#pythonother-language-self-hosted-runner-example)
- [Contracts By Language](#contracts-by-language)
  - [Node.js Contracts](#nodejs-contracts)
    - [Node.js Quick Reference](#nodejs-quick-reference)
    - [Node.js Version](#nodejs-version)
    - [Node.js Package Manager](#nodejs-package-manager)
    - [Node.js Environments](#nodejs-environments)
    - [Node.js Contract Tests](#nodejs-contract-tests)
    - [Node.js End-to-End Tests](#nodejs-end-to-end-tests)
    - [Node.js Integration Tests](#nodejs-integration-tests)
    - [Node.js Linting](#nodejs-linting)
    - [Node.js Smoke Tests](#nodejs-smoke-tests)
    - [Node.js Code Coverage](#nodejs-code-coverage)
    - [Node.js Type Check](#nodejs-type-check)
    - [Node.js Unit Tests](#nodejs-unit-tests)
  - [Python Contracts](#python-contracts)
    - [Python Quick Reference](#python-quick-reference)
    - [Conda](#conda)
    - [Python Version](#python-version)
    - [Python Environments](#python-environments)
    - [Python Contract Tests](#python-contract-tests)
    - [Python End-to-End Tests](#python-end-to-end-tests)
    - [Python Integration Tests](#python-integration-tests)
    - [Python Linting](#python-linting)
    - [Python Smoke Tests](#python-smoke-tests)
    - [Python Code Coverage](#python-code-coverage)
    - [Python Type Check](#python-type-check)
    - [Python Unit Tests](#python-unit-tests)
  - ["Other Language" Contracts](#other-language-contracts)
    - ["Other Language" Definition](#other-language-definition)
    - [Multistage Builds for Tests](#multistage-builds-for-tests)
    - ["Other Language" Quick Reference](#other-language-quick-reference)
    - ["Other Language" Contract Tests](#other-language-contract-tests)
    - ["Other Language" End-to-End Tests](#other-language-end-to-end-tests)
    - ["Other Language" Integration Tests](#other-language-integration-tests)
    - ["Other Language" Linting](#other-language-linting)
    - ["Other Language" Smoke Tests](#other-language-smoke-tests)
    - ["Other Language" Code Coverage](#other-language-code-coverage)
    - ["Other Language" Type Check](#other-language-type-check)
    - ["Other Language" Unit Tests](#other-language-unit-tests)
- [Contracts By Application](#contracts-by-application)
  - [Services](#services)
    - [Example Service](#example-service)
    - [Files Required For Services](#files-required-for-services)
    - [Service Builds](#service-builds)
    - [Service Deploys](#service-deploys)
    - [Service Preview Environments](#service-preview-environments)
  - [Lambda Functions](#lambda-functions)
    - [Example Function](#example-function)
    - [Files Required For Lambda Functions](#files-required-for-lambda-functions)
    - [Lambda Function Builds](#lambda-function-builds)
    - [Lambda Function Deploys](#lambda-function-deploys)
  - [Static Webapps](#static-webapps)
    - [Example Static Webapp](#example-static-webapp)
    - [Files Required For Static Webapps](#files-required-for-static-webapps)
    - [Static Webapps Configuration](#static-webapps-configuration)
    - [Build/Deploy Details](#builddeploy-details)
    - [Static Webapp Preview Environments](#static-webapp-preview-environments)
  - [npm Packages](#npm-packages)
    - [Example npm Package](#example-npm-package)
    - [Files Required For npm Packages](#files-required-for-npm-packages)
    - [npm Package Builds](#npm-package-builds)
    - [npm Package Deploys](#npm-package-deploys)
  - [pip Packages](#pip-packages)
    - [Example pip Package](#example-pip-package)
    - [Files Required For pip Packages](#files-required-for-pip-packages)
    - [pip Package Builds](#pip-package-builds)
    - [pip Package Deploys](#pip-package-deploys)
  - [Airflow Jobs](#airflow-jobs)
    - [Example Airflow Job](#example-airflow-job)
    - [Files Required For Airflow Jobs](#files-required-for-airflow-jobs)
    - [Airflow Job Builds](#airflow-job-builds)
    - [Airflow Job Deploys](#airflow-job-deploys)
- [Appendix](#appendix)
  - [Annotated `ci.json` Example - All Languages](#annotated-cijson-example---all-languages)
  - [Annotated `package.json` Example - Node.js](#annotated-packagejson-example---nodejs)
  - [`Makefile` Example - Python](#makefile-example---python)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Overview

This document describes contracts that must be met in order for applications to use CI/CD pipelines.

For simplicity, we've chosen to use [convention over configuration](https://en.wikipedia.org/wiki/Convention_over_configuration) for Node.js/React and Python pipelines with configuration overrides available for non-standard Node.js/React and Python applications and for applications in other languages like Go.

## Application Locations

This describes where applications must be stored. For detailed information on this topic, refer to the [Monorepo Architecture Pitch](../monorepo/monorepo.md).

| Location      | Application Type               |
| :---------    | :-----------------             |
| `/functions`  | Lambda functions               |
| `/jobs`       | Recurring jobs                 |
| `/packages`   | Common packages, all languages |
| `/services`   | ECS services                   |
| `/web-static` | Static web apps                |

## General CI/CD Flow

As mentioned above, we use convention over configuration for CI/CD contracts for Node.js/React and Python applications. We will support configuration-based overrides for non-standard Node.js/React and Python applications and for applications in other languages like Go.

This section describes the basic flow of the CI/CD pipelines and details how overrides will work.

1. Check out source code
1. Determine which files have been updated
1. Detect which language and version the application uses
1. Check to see if any pipeline stage overrides are present
1. Install test dependencies
1. If necessary, build application
1. Run tests
1. Generate test coverage report
1. Send test coverage report(s) to Codecov
1. Build application
1. Deploy application
1. Run post-build tests (if defined)
1. If post-build tests fail, revert deployment
1. Send deploy notifications to New Relic and Slack

## What If A Command Doesn't Exist?

If a particular test or command doesn't exist for your application, leave it blank in `ci.json`, `package.json`, and/or `Makefile` and it will be skipped in the CI/CD pipeline.

## Determining Application Language

1. Is the language explicitly listed in `ci.json` under `language.name`?
1. Does the application contain `environment.devenv.yml` or `environment.yml` files? If so, assume the language is "Python".
1. Does the application contain a `package.json` file? If so, assume the language is "Node.js".
1. Otherwise, assume that this is an "Other Language" application.

## Code Coverage

Code coverage is the percentage of code which is covered by automated tests. We're using [Codecov](https://app.codecov.io/gh/Surfline) to track test code coverage.

Codecov can ingest many [coverage formats](https://docs.codecov.com/docs/supported-report-formats) from a wide variety of [languages and test tooling](https://docs.codecov.com/docs/supported-languages).

While code coverage is typically used with unit tests, Codecov supports tracking code coverage for all test types.

In order for CI/CD pipelines to flag code coverage with the proper test type, teams should update their test commands to output coverage reports in directories that correspond to the test type.

| Test Type         | Coverage Output Directory |
| :----             | :----                     |
| Contract Tests    | `coverage-contract`       |
| End-to-End Tests  | `coverage-e2e`            |
| Integration Tests | `coverage-integration`    |
| Unit Tests        | `coverage-unit`           |

When submitting test coverage reports to Codecov, CI/CD pipelines will look for output in these directories.

In addition to creating test coverage reports while running tests, CI/CD pipelines will support commands to explicitly generate test coverage reports. Those commands are detailed below and should also output to the directories listed above.

### Code Coverage Example

```json
{
  "scripts": {
    "test:unit": "nyc --reporter=lcov --report-dir coverage-unit mocha -t 10000 --require @babel/register src/**/*.spec.js"
  }
}
```

## Accessing Private AWS Resources

If you need any of your tests to access private AWS resources, you may specify that the test executes on a _self-hosted runner_ inside the primary AWS VPC in `surfline-dev` in `us-west-1`. This will allow your test to directly access services, Redis clusters, RDS databases, etc. This is most commonly used by integration tests and smoke tests.

To use self-hosted runners, just preface your test command with `RUNNER=self-hosted`.

### Node.js Self-Hosted Runner Example

```js
{
  "scripts": {
     "test:smoke": "RUNNER=self-hosted mocha -t 60000 --require @babel/register tests/smoke-tests/*.spec.js",
  }
}
```

### Python/Other Language Self-Hosted Runner Example

```Makefile
test-unit:
    RUNNER=self-hosted <test-command>
```

## Contracts By Language

### Node.js Contracts

#### Node.js Quick Reference

This quick reference is provided for convenience. Each of these stages is detailed below.

| Stage             | Default Command                        | Override in `ci.json` |
| :----             | :----                                  | :----                 |
| Contract Tests    | `npm run test:contract`                | `tests.contract`      |
| End-to-End Tests  | `npm run test:e2e`                     | `tests.e2e`           |
| Integration Tests | `npm run test:integration`             | `tests.integration`   |
| Linting           | `npm run lint`                         | `tests.lint`          |
| Smoke Tests       | `SERVICE_URL={url} npm run test:smoke` | `tests.smoke`         |
| Code Coverage     | `npm run test:coverage`                | `tests.coverage`      |
| Type Check        | `npm run type-check`                   | `tests.type-check`    |
| Unit Tests        | `npm run test:unit`                    | `tests.unit`          |

#### Node.js Version

**Source of Truth:** `.nvmrc`

```
v10
```

**Override:** `ci.json`

```json
{
  "language": {
    "version": "v10"
  }
}
```

#### Node.js Package Manager

Node.js applications use [npm](https://www.npmjs.com/package/npm).

#### Node.js Environments

For testing and linting, Node.js application environments are built using `npm install --production=false`.

#### Node.js Contract Tests

**Applies to:** Services

**Source of Truth:** `package.json`

```json
{
  "scripts": {
    "test:contract": "<test command>"
  }
}
```

Run as `npm run test:contract`

**Override:** `ci.json`

```json
{
  "tests": {
    "contract": "<test command>"
  }
}
```

We assume that proper contract test configuration is included with the application and that contract testing dependencies are present in `package.json`.

#### Node.js End-to-End Tests

**Applies to:** Services, Static Web Apps

**Source of Truth:** `package.json`

```json
{
  "scripts": {
    "test:e2e": "<test command>"
  }
}
```

Run as `npm run test:e2e`

**Override:** `ci.json`

```json
{
  "tests": {
    "e2e": "<test command>"
  }
}
```

We assume that proper end-to-end test configuration is included with the application and that end-to-end testing dependencies are present in `package.json`.

#### Node.js Integration Tests

**Applies to:** Jobs, Services, Static Web Apps

**Source of Truth:** `package.json`

```json
{
  "scripts": {
    "test:integration": "<test command>"
  }
}
```

Run as `npm run test:integration`

**Override:** `ci.json`

```json
{
  "tests": {
    "integration": "<test command>"
  }
}
```

We assume that proper integration test configuration is included with the application and that integration testing dependencies are present in `package.json`.

#### Node.js Linting

**Applies to:** Functions, Jobs, Packages, Services, Static Web Apps

**Source of Truth:** `package.json`

```json
{
  "scripts": {
    "lint": "<lint command>"
  }
}
```

Run as `npm run lint`

**Override:** `ci.json`

```json
{
  "tests": {
    "lint": "<lint command>"
  }
}
```

We assume that proper linting configuration is included with the application and that linting dependencies are present in `package.json`.

#### Node.js Smoke Tests

**Applies to:** Services, Static Web Apps

**Source of Truth:** `package.json`

```json
{
  "scripts": {
    "test:smoke": "<test command>"
  }
}
```

Run as `SERVICE_URL={url} npm run test:smoke`

**Override:** `ci.json`

```json
{
  "tests": {
    "smoke": "<test command>"
  }
}
```

For either command, we set the `SERVICE_URL` environment variable to the value specified by `serviceUrls.{environment}` in `ci.json`:

```json
{
  "serviceUrls": {
    "prod": "<prod-service-url>",
    "sandbox": "<sandbox-service-url>",
    "staging": "<staging-service-url>"
  }
}
```

We assume that proper smoke test configuration is included with the application and that smoke testing dependencies are present in `package.json`.

Smoke tests are run post-deploy. If a smoke test fails, the deploy will be reverted to the previous version.

#### Node.js Code Coverage

**Applies to:** Functions, Jobs, Packages, Services, Static Web Apps

The best way to generate code coverage reports is when running individual tests. For example, you might prefix your unit test command with `nyc --reporter=lcov --report-dir coverage-unit` to generate a code coverage report while running unit tests.

If that's not possible or preferable, you may generate code coverage reports explicitly with the following commands.

**Source of Truth:** `package.json`

```json
{
  "scripts": {
    "test:coverage": "<coverage command>"
  }
}
```

Run as `npm run test:coverage`

**Override:** `ci.json`

```json
{
  "tests": {
    "coverage": "<coverage command>"
  }
}
```

We assume that proper test coverage configuration is included with the application and that test coverage dependencies are present in `package.json`.

#### Node.js Type Check

**Applies to:** Functions, Jobs, Packages, Services, Static Web Apps

**Source of Truth:** `package.json`

```json
{
  "scripts": {
    "type-check": "<type check command>"
  }
}
```

Run as `npm run type-check`

**Override:** `ci.json`

```json
{
  "tests": {
    "type-check": "<type check command>"
  }
}
```

We assume that proper type check configuration is included with the application and that type check dependencies are present in `package.json`.

#### Node.js Unit Tests

**Applies to:** Functions, Jobs, Packages, Services, Static Web Apps

**Source of Truth:** `package.json`

```json
{
  "scripts": {
    "test:unit": "<test command>"
  }
}
```

Run as `npm run test:unit`

**Override:** `ci.json`

```json
{
  "tests": {
    "unit": "<test command>"
  }
}
```

We assume that proper linting configuration is included with the application and that linting dependencies are present in `package.json`.

### Python Contracts

#### Python Quick Reference

This quick reference is provided for convenience. Each of these stages is detailed below.

| Stage             | Default Command                     | Override in `ci.json` |
| :----             | :----                               | :----                 |
| Contract Tests    | `make test-contract`                | `tests.contract`      |
| End-to-End Tests  | `make test-e2e`                     | `tests.e2e`           |
| Integration Tests | `make test-integration`             | `tests.integration`   |
| Linting           | `make lint`                         | `tests.lint`          |
| Smoke Tests       | `SERVICE_URL={url} make test-smoke` | `tests.smoke`         |
| Code Coverage     | `make test-coverage`                | `tests.coverage`      |
| Type Check        | `make type-check`                   | `tests.type-check`    |
| Unit Tests        | `make test-unit`                    | `tests.unit`          |

#### Conda

We use [Miniconda](https://docs.conda.io/en/latest/miniconda.html) at Wavetrak to manage Python environments. It is used to manage dev/test environments. In many cases, we deploy applications in Miniconda containers. The Python contracts in this document reflect our use of Miniconda.

#### Python Version

**Source of Truth:** `environment.devenv.yml` or `environment.yml`

```yml
dependencies:
  - python=3.7.6
```

**Override:** `ci.json`

```json
{
  "language": {
    "version": "3.7.6"
  }
}
```

#### Python Environments

Most applications use [conda-devenv](https://conda-devenv.readthedocs.io) to address limitations of `environment.yml`. We set up Python environments using the following files (in order):

1. `environment.devenv.yml`
1. `environment.yml`

If `environment.devenv.yml` exists, we run the Github Actions equivalent of the following:

```bash
conda devenv
conda activate {environment-name}
```

Otherwise if `environment.yml` exists, we run the Github Actions equivalent of the following:

```bash
conda env create -f environment.yml
conda activate {environment-name}
```

#### Python Contract Tests

**Applies to:** Services

**Source of Truth:** `Makefile`

```makefile
test-contract:
    <test command>
```

Run as `make test-contract`

**Override:** `ci.json`

```json
{
  "tests": {
    "contract": "<test command>"
  }
}
```

We assume that proper contract test configuration is included with the application and that contract testing dependencies are present in `environment.devenv.yml` or `environment.yml`.

#### Python End-to-End Tests

**Applies to:** Services

**Source of Truth:** `Makefile`

```makefile
test-e2e:
    <test command>
```

Run as `make test-e2e`

**Override:** `ci.json`

```json
{
  "tests": {
    "e2e": "<test command>"
  }
}
```

We assume that proper end-to-end test configuration is included with the application and that end-to-end testing dependencies are present in `environment.devenv.yml` or `environment.yml`.

#### Python Integration Tests

**Applies to:** Jobs, Services

**Source of Truth:** `Makefile`

```makefile
test-integration:
    <test command>
```

Run as `make test-integration`

**Override:** `ci.json`

```json
{
  "tests": {
    "integration": "<test command>"
  }
}
```

We assume that proper integration test configuration is included with the application and that integration testing dependencies are present in `environment.devenv.yml` or `environment.yml`.

#### Python Linting

**Applies to:** Functions, Jobs, Packages, Services

**Source of Truth:** `Makefile`

```makefile
lint:
    <lint command>
```

Run as `make lint`

**Override:** `ci.json`

```json
{
  "tests": {
    "lint": "<lint command>"
  }
}
```

We assume that proper linting configuration is included with the application and that linting dependencies are present in `environment.devenv.yml` or `environment.yml`.

#### Python Smoke Tests

**Applies to:** Services

**Source of Truth:** `Makefile`

```makefile
test-smoke:
    <test command>
```

Run as `SERVICE_URL={url} make test-smoke`

**Override:** `ci.json`

```json
{
  "tests": {
    "smoke": "<test command>"
  }
}
```

For either command, we set the `SERVICE_URL` environment variable to the value specified by `serviceUrls.{environment}` in `ci.json`.

We assume that proper smoke test configuration is included with the application and that smoke testing dependencies are present in `environment.devenv.yml` or `environment.yml`.

Smoke tests are run post-deploy. If a smoke test fails, the deploy will be reverted to the previous version.

#### Python Code Coverage

**Applies to:** Functions, Jobs, Packages, Services

The best way to generate code coverage reports is when running individual tests.

If that's not possible or preferable, you may generate code coverage reports explicitly with the following commands.

**Source of Truth:** `Makefile`

```makefile
coverage:
    <coverage command>
```

Run as `make coverage`

**Override:** `ci.json`

```json
{
  "tests": {
    "coverage": "<coverage command>"
  }
}
```

We assume that proper test coverage configuration is included with the application and that test coverage dependencies are present in `environment.devenv.yml` or `environment.yml`.

#### Python Type Check

**Applies to:** Functions, Jobs, Packages, Services

**Source of Truth:** `Makefile`

```makefile
type-check:
    <type check command>
```

Run as `make type-check`

**Override:** `ci.json`

```json
{
  "tests": {
    "type-check": "<type check command>"
  }
}
```

We assume that proper type check configuration is included with the application and that type check dependencies are present in `environment.devenv.yml` or `environment.yml`.

#### Python Unit Tests

**Applies to:** Functions, Jobs, Packages, Services

**Source of Truth:** `Makefile`

```makefile
test-unit:
    <test command>
```

Run as `make test-unit`

**Override:** `ci.json`

```json
{
  "tests": {
    "unit": "<test command>"
  }
}
```

We assume that proper linting configuration is included with the application and that linting dependencies are present in `environment.devenv.yml` or `environment.yml`.

### "Other Language" Contracts

#### "Other Language" Definition

For purposes of the CI/CD workflows, "Other Language" means a language other than Node.js or Python. While Node.js and Python use native installations of their tooling and benefit from optimizations that come along with native tooling, we need to support applications running in other languages. To do this, we use containers as an abstraction, similar to how they're used in the previous Jenkins CI/CD pipelines. Rather than installing dependencies directly in Github Actions, we build a container and run tests/linting/etc. in that container.

"Other Language" only applies to Services, Functions, and Jobs. Static Webapps will always use Node.js. Packages will always use Node.js and Python. 

#### Multistage Builds for Tests

If the Dockerfile contains a stage named `test`, we build a container up to that stage and use it from running all tests. Otherwise we build the full container and run tests using the full container image.

##### Multistage Build Example

**`Dockerfile`**

```dockerfile
FROM golang:1.12.7 AS test

WORKDIR /go/src/github.com/Surfline/wavetrak-services/services/science-models-db

RUN go get -u golang.org/x/lint/golint
COPY . .

FROM test AS builder
RUN make build-static

FROM alpine
WORKDIR /opt/app

COPY --from=builder /go/src/github.com/Surfline/wavetrak-services/services/science-models-db/science-models-db ./

ENTRYPOINT ["./science-models-db"]
```

**Building a Container For Tests**

```bash
docker build --target test -t science-models-db:test .
```

**Running Tests**

```bash
# Run lint
docker run -it --rm science-models-db:test make lint

# Run unit tests
mkdir -p ./coverage-unit
docker run -it --rm -v "$(pwd)"/coverage-unit:/tmp/coverage-unit science-models-db:test make test-unit
```

#### "Other Language" Quick Reference

This quick reference is provided for convenience. Each of these stages is detailed below.

| Stage             | Default Command                     | Override in `ci.json` |
| :----             | :----                               | :----                 |
| Contract Tests    | `make test-contract`                | `tests.contract`      |
| End-to-End Tests  | `make test-e2e`                     | `tests.e2e`           |
| Integration Tests | `make test-integration`             | `tests.integration`   |
| Linting           | `make lint`                         | `tests.lint`          |
| Smoke Tests       | `SERVICE_URL={url} make test-smoke` | `tests.smoke`         |
| Code Coverage     | `make test-coverage`                | `tests.coverage`      |
| Type Check        | `make type-check`                   | `tests.type-check`    |
| Unit Tests        | `make test-unit`                    | `tests.unit`          |

#### "Other Language" Contract Tests

**Applies to:** Services

**Source of Truth:** `Makefile`

```makefile
test-contract:
    <test command>
```

Run as `docker run -it --rm -v "$(pwd)"/coverage-contract:/tmp/coverage-contract {app-name}:test make test-contract`

**Override:** `ci.json`

```json
{
  "tests": {
    "contract": "<test command>"
  }
}
```

We assume that proper contract test configuration is included with the application and that contract testing dependencies are present in the test container.

#### "Other Language" End-to-End Tests

**Applies to:** Services

**Source of Truth:** `Makefile`

```makefile
test-e2e:
    <test command>
```

Run as `docker run -it --rm -v "$(pwd)"/coverage-e2e:/tmp/coverage-e2e {app-name}:test make test-e2e`

**Override:** `ci.json`

```json
{
  "tests": {
    "e2e": "<test command>"
  }
}
```

We assume that proper end-to-end test configuration is included with the application and that end-to-end testing dependencies are present in the test container.

#### "Other Language" Integration Tests

**Applies to:** Jobs, Services

**Source of Truth:** `Makefile`

```makefile
test-integration:
    <test command>
```

Run as `docker run -it --rm -v "$(pwd)"/coverage-integration:/tmp/coverage-integration {app-name}:test make test-integration`

**Override:** `ci.json`

```json
{
  "tests": {
    "integration": "<test command>"
  }
}
```

We assume that proper integration test configuration is included with the application and that integration testing dependencies are present in the test container.

#### "Other Language" Linting

**Applies to:** Functions, Jobs, Services

**Source of Truth:** `Makefile`

```makefile
lint:
    <lint command>
```

Run as `docker run -it --rm {app-name}:test make lint`

**Override:** `ci.json`

```json
{
  "tests": {
    "lint": "<lint command>"
  }
}
```

We assume that proper linting configuration is included with the application and that linting dependencies are present in the test container.

#### "Other Language" Smoke Tests

**Applies to:** Services

**Source of Truth:** `Makefile`

```makefile
test-smoke:
    <test command>
```

Run as `docker run -it --rm -e SERVICE_URL={url} {app-name}:test make test-smoke`

**Override:** `ci.json`

```json
{
  "tests": {
    "smoke": "<test command>"
  }
}
```

For either command, we set the `SERVICE_URL` environment variable to the value specified by `serviceUrls.{environment}` in `ci.json`.

We assume that proper smoke test configuration is included with the application and that smoke testing dependencies are present in the test container.

Smoke tests are run post-deploy. If a smoke test fails, the deploy will be reverted to the previous version.

#### "Other Language" Code Coverage

**Applies to:** Functions, Jobs, Services

The best way to generate code coverage reports is when running individual tests.

If that's not possible or preferable, you may generate code coverage reports explicitly with the following commands.

**Source of Truth:** `Makefile`

```makefile
coverage:
    <coverage command>
```

Run as `docker run -it --rm -v "$(pwd)"/coverage:/tmp/coverage {app-name}:test make coverage`

**Override:** `ci.json`

```json
{
  "tests": {
    "coverage": "<coverage command>"
  }
}
```

We assume that proper test coverage configuration is included with the application and that test coverage dependencies are present in the test container.

#### "Other Language" Type Check

**Applies to:** Functions, Jobs, Services

**Source of Truth:** `Makefile`

```makefile
type-check:
    <type check command>
```

Run as `docker run -it --rm {app-name}:test make type-check`

**Override:** `ci.json`

```json
{
  "tests": {
    "type-check": "<type check command>"
  }
}
```

We assume that proper type check configuration is included with the application and that type check dependencies are present in the test container.

#### "Other Language" Unit Tests

**Applies to:** Functions, Jobs, Services

**Source of Truth:** `Makefile`

```makefile
test-unit:
    <test command>
```

Run as `docker run -it --rm -v "$(pwd)"/coverage-unit:/tmp/coverage-unit {app-name}:test make test-unit`

**Override:** `ci.json`

```json
{
  "tests": {
    "unit": "<test command>"
  }
}
```

We assume that proper linting configuration is included with the application and that linting dependencies are present in the test container.

## Contracts By Application

This section describes contracts specific to application types. It focuses on configuration files required by each application type. It also details build, deploy, and preview environment contracts.

### Services

Services run in containers and are deployed to ECS. Some containers, particularly those running web frontends, contain static assets that are deployed to S3/Cloudfront.

#### Example Service

This service meets all CI/CD contracts and is a good reference:

- [services/cameras-service](https://github.com/Surfline/wavetrak-services/tree/master/services/cameras-service)

#### Files Required For Services

All files are assumed to be at the root of the service directory unless specified with an absolute path.

Language-specific files like `package.json` or `environment.devenv.yml` are covered in [Contracts By Language](#contracts-by-language) above.

| Filename                           | Description                                                                                                        |
| :-------                           | :----------                                                                                                        |
| `Dockerfile`                       | Service container definition                                                                                       |
| `Makefile`                         | (Required for non-Node.js services) Contains test targets.                                                         |
| `ci.json`                          | CI/CD configuration (see [Annotated `ci.json` Example - All Languages](#annotated-cijson-example---all-languages)) |
| `package.json`                     | (Required for Node.js services) Contains test targets and service dependencies.                                    |
| `pre-docker-build-actions.sh`      | (Optional) Script to run before container build. Usually used to retrieve build dependencies.                      |
| `/docker/{service}.yml`            | Task definition configuration in Docker Compose syntax                                                             |
| `/docker/{service}.ecs-params.yml` | (Optional) Additional task definition configuration that doesn't correspond to Docker Compose parameters           |

#### Service Builds

Service builds follow these steps:

1. If a `pre-docker-build-actions.sh` file exists at the root of the service, run it.
1. Build the container image using `docker build -t {service-name} .`
1. If the container image contains static files, copy them from the container image and push them to S3.
1. Push the container image to ECR.

For services that generate static files as part of their Docker build, `cdnArtifactPath` from `ci.json` is used to determine the path of the files relative to the working directory in the Docker image. Files are copied from that location in the Docker image to the same location in `s3://sl-product-cdn-{ENV}`.

#### Service Deploys

Service deploys follow these steps:

1. Configure task definition locally, referencing the newly-built container image
1. Create task definition revision in AWS
1. Update service to use task definition revision
1. Notify New Relic & Slack
1. If smoke tests exist, run them
1. If smoke tests fail, do the following:
    1. Roll back the deploy to the previous task definition revision
    1. Notify New Relic & Slack

#### Service Preview Environments

Service preview environments don't have a particular contract, but they will have the following characteristics:

- Service preview environments consist of a standalone service task running in the `staging` environment on the `core-services` ECS cluster
- The service will run a container built from the latest commit in the branch that the PR is based on
- When new commits are pushed to the branch, the preview environment will automatically update
- When the PR is merged to master or closed, the preview environment will be destroyed
- Preview environments will only be accessible over a VPN connection or from within our VPC
  - This is similar to direct access to services.
- Preview environments will have unique URLs matching the following pattern: `http://pr-{pr number}.preview.surfline.com/`
- Preview environments will use `staging` environment variable values from Parameter Store
- For web services, artifact bundles will be generated and pushed to `s3://sl-product-cdn-preview/{cdnArtifactPath}/`

### Lambda Functions

Lambda functions must use [Serverless Framework](https://www.serverless.com/).

There's a guide for migrating existing Lambda Functions to Serverless Framework:

<https://github.com/Surfline/wavetrak-services/blob/master/common/create/README.md>

#### Example Function

This function meets all CI/CD contracts and is a good reference:

- [functions/infra-tag-ec2-instances-for-patch-manager](https://github.com/Surfline/wavetrak-services/tree/master/functions/infra-tag-ec2-instances-for-patch-manager)

#### Files Required For Lambda Functions

| Filename                 | Description                                                                                                        |
| :-------                 | :----------                                                                                                        |
| `Makefile`               | (Required for non-Node.js services) Contains test targets.                                                         |
| `environment.devenv.yml` | For Python functions, contains dev/test dependencies.                                                              |
| `package.json`           | Contains Serverless framework dependencies. For Node.js function, contains test targets and function dependencies. |
| `requirements.txt`       | For Python functions, contains production dependencies.                                                            |
| `serverless.yml`         | Lambda function configuration and related infrastructure                                                           |

#### Lambda Function Builds

Lambda Functions are built with the following command:

```sh
serverless package -s {env} -p build-{env}
```

#### Lambda Function Deploys

Lambda Functions are deployed with the following command:

```sh
serverless deploy -s {env} -p build-{env}
```

### Static Webapps

Static Webapps use Next.js and are deployed to S3/Cloudfront.

#### Example Static Webapp

This static webapp meets all CI/CD contracts and is a good reference:

- [web-static/poi-admin](https://github.com/Surfline/wavetrak-services/tree/master/web-static/poi-admin/)

#### Files Required For Static Webapps

| Filename       | Description                                                                      |
| :-------       | :----------                                                                      |
| `Dockerfile`   | Service container definition, used to build application bundles                  |
| `ci.json`      | CI/CD configuration (see [`ci.json` Configuration](#cijson-configuration) below) |
| `package.json` | Contains test targets and service dependencies.                                  |

#### Static Webapps Configuration

##### File Build Path

Files for distribution should exist in the `/opt/dist` directory of the container generated by `docker build`. This gives application developers the flexibility to move and process files as needed within the Dockerfile. Existing static webapp Dockerfiles should be updated to copy/move files to `/opt/dist`.

We will no longer use the `buildArtifactPath` variable in `ci.json` to define this location.

##### `ci.json` Configuration

The following parameters in `ci.json` will be used to define build configuration:

| Parameter                      | Description                                                                                                      | Example                             |
| :----                          | :----                                                                                                            | :----                               |
| `module`                       | The application module name.                                                                                     | `cancel`, `poi`                     |
| `bucket`                       | The bucket to deploy to. Include `{environment}` to specify environment.                                         | `s3://sl-product-cdn-{environment}` |
| `destinationPath`              | The path within the bucket that files will be copied to. Can include `{brand}` variable.                         | `/cancel/{brand}`, `/poi`           |
| `appType`                      | The application type. May be `account` or empty. This affects how files are deployed (detailed below).           | `account`, `""`                     |
| `noCacheFiles`                 | List of [glob](https://docs.python.org/3/library/glob.html) path patterns that should have no-cache metadata, listed relative to their location in `/opt/dist`. | `["**/index.html"]`                 |
| `requireEnvironmentContainers` | Boolean value that designates whether separate containers will be built for each environment.                    | `true`, `false`                     |
| `cloudfrontDistributionId`     | The Cloudfront distribution ID. This is used to clear the Cloudfront cache after deploying.                      | `ENDP1TZMOR7EI`, `E2TIL0PH7AXAOD`   |

##### Static Webapp `ci.json` Examples

###### Cancel Account `ci.json` Example
```json
{
  "imageName": "account/cancel",
  "module": "cancel",
  "bucket": "s3://sl-product-cdn-{environment}",
  "destinationPath": "/cancel/{brand}",
  "appType": "account",
  "requireEnvironmentContainers": true,
  "noCacheFiles": ["**/index.html"],
  "cloudfrontDistributionId": "E2TIL0PH7AXAOD"
}
```

###### POI Admin `ci.json` Example
```json
{
  "imageName": "admin-tools/poi",
  "module": "poi",
  "bucket": "s3://sl-admin-tools-{environment}",
  "destinationPath": "/poi",
  "requireEnvironmentContainers": true,
  "noCacheFiles": ["**/index.html"]
}
```

##### Additional File Locations

The static webapp workflow will store previous versions of applications in the following locations:

| Type                         | Location                                       | Example                                                              |
| :----                        | :----                                          | :----                                                                |
| All versions                 | `/builds/{destinationPath}/{git-commit-sha}` | `/builds/cancel/sl/bf26c48`, `/builds/poi/f50fdc5`                   |
| Most recent previous version | `/builds/{destinationPath}/previous-version`  | `/builds/cancel/sl/previous-version`, `/builds/poi/previous-version` |

#### Build/Deploy Details

##### Static Webapp Build Summary

Static Webapp builds follow these steps:

1. Build the container image using `docker build -t {service-name} .`
1. Copy static files from the container image to the local filesystem.

##### Static Webapp Deploy Summary

Static Webapp deploys follow these steps:

1. Push static files to S3.
1. Invalidate Cloudfront cache.
1. Notify New Relic & Slack
1. If smoke tests exist, run them
1. If smoke tests fail, do the following:
    1. Roll back the deploy to the previous task definition revision
    1. Notify New Relic & Slack

##### Account Webapp Deploy Details

Account static webapps have a unique build and deploy process that we will continue to support. Once files have been generated by `docker build` and exist in `/opt/dist` in the built Docker container, we will deploy them with the following steps:

1. Backup files from `{bucket}/{destinationPath}/current` to `{bucket}/builds/{destinationPath}/previous-version`.
1. Copy files from `/opt/dist/out/_next` to `{bucket}/{destinationPath}/_next`.
1. Copy files from `/opt/dist/static` to `{bucket}/{destinationPath}/static`.
1. Copy files from `/opt/dist/out/builds` to `{bucket}/builds/{destinationPath}/{git-commit-sha}`.
1. Copy files from `/opt/dist/html-no-extension` to `{bucket}/builds/{destinationPath}/{git-commit-sha}`.
1. Sync files from `{bucket}/builds/{destinationPath}/{git-commit-sha}` to `{bucket}/{destinationPath}/current`.
1. Invalidate Cloudfront cache for `{destinationPath}` if `cloudfrontDistributionId` exists.

##### Non-Account Webapp Deploy Details

For non-account static webapps, we follow a much simpler build and deploy process. Once files have been generated by `docker build` and exist in `/opt/dist` in the built Docker container, we will deploy them with the following steps:

1. Backup files from `{bucket}/{destinationPath}` to `{bucket}/builds/{destinationPath}/previous-version`.
1. Copy files from `/opt/dist` to `{bucket}/builds/{destinationPath}/{git-commit-sha}`.
1. Sync files from `{bucket}/builds/{destinationPath}/{git-commit-sha}` to `{bucket}/{destinationPath}`.
1. Invalidate Cloudfront cache for `{destinationPath}` if `cloudfrontDistributionId` exists.

#### Static Webapp Preview Environments

Static Webapp preview environments don't have a particular contract, but they will have the following characteristics:

- Static Webapp preview environments consist of static files deployed to `s3://sl-product-cdn-preview/{module}/{brand}/` in the `surfline-dev` AWS account
- The static files will come from a container built from the latest commit in the branch that the PR is based on
- When new commits are pushed to the branch, the preview environment will automatically update
- When the PR is merged to master or closed, the preview environment will be destroyed
- Preview environments will have unique URLs matching the following pattern: `http://pr-{pr number}.preview.surfline.com/{module}/{brand}/`
- Preview environments will use `staging` environment variable values from Parameter Store

### npm Packages

npm Packages are managed with Lerna and deployed to Artifactory.

#### Example npm Package

This npm package meets all CI/CD contracts and is a good reference:

- [packages/@types](https://github.com/Surfline/wavetrak-services/tree/master/packages/@types/)

#### Files Required For npm Packages

| Filename       | Description                                        |
| :-------       | :----------                                        |
| `package.json` | Contains test targets and service dependencies.    |
| `/lerna.json`  | Contains Lerna configuration for all npm packages. |

#### npm Package Builds

npm Package builds follow these steps:

1. Build using `npm ci --ignore-scripts`

#### npm Package Deploys

npm Package deploys follow these steps:

1. Deploy using `npm run publish-ci`.
    1. This runs `lerna publish from-package --yes`

### pip Packages

pip Packages are managed with Conda and are deployed to Artifactory.

#### Example pip Package

This pip package meets all CI/CD contracts and is a good reference:

- [packages/job-helpers](https://github.com/Surfline/wavetrak-services/tree/master/packages/job-helpers/)

#### Files Required For pip Packages

| Filename                 | Description                            |
| :-------                 | :----------                            |
| `Makefile`               | Contains test and deploy targets.      |
| `environment.devenv.yml` | Contains dev/test dependencies.        |
| `setup.py`               | Contains Python package configuration. |

#### pip Package Builds

pip package builds follow these steps:

1. Build Conda environment with `conda devenv`

#### pip Package Deploys

pip package deploys follow these steps:

1. Activate Conda environment.
1. Deploy package with `python setup.py bdist_wheel upload -r ${ENV}`

### Airflow Jobs

Airflow job task containers are deployed to ECR and Airflow job DAGs are deployed to Airflow.

#### Example Airflow Job

This Airflow job meets all CI/CD contracts and is a good reference:

- [download-noaa-gfs](https://github.com/Surfline/wavetrak-services/tree/master/jobs/download-noaa-gfs)

#### Files Required For Airflow Jobs

| Filename                                    | Description                                                                                   |
| :-------                                    | :----------                                                                                   |
| `dag.py`                                    | Contains DAG definition.                                                                      |
| `docker/`                                   | Contains containerized tasks used in DAG.                                                     |
| `docker/{task-name}/Dockerfile`             | Task container definition                                                                     |
| `docker/{task-name}/Makefile`               | (Required for non-Node.js jobs) Contains test targets.                                        |
| `docker/{task-name}/environment.devenv.yml` | (Required for Python jobs) Contains dependencies.                                             |
| `docker/{task-name}/pre-build.sh`           | (Optional) Script to run before container build. Usually used to retrieve build dependencies. |
| `docker/{task-name}/package.json`           | (Required for Node.js jobs) Contains test targets and job dependencies.                       |

#### Airflow Job Builds

Airflow job builds follow these steps:

1. For each task in the job, do the following:
    1. Install task dependencies.
    1. Run all available tests.
    1. If `pre-build.sh` file exists, run it.
    1. Build the container image using `docker build -t {service-name} .`

#### Airflow Job Deploys

Airflow job builds follow these steps:

1. For each task in the job, do the following:
    1. Tag and push the container to ECR.
1. Push `dag.py` to all Airflow nodes using `scp`.
1. Restart Airflow webserver on all nodes.

## Appendix

### Annotated `ci.json` Example - All Languages

```javascript
{
  "imageName": "",       // The container image name
  "compose": "",         // The docker-compose file, in `/docker`
  "serviceName": "",     // The service name in AWS
  "cluster": "",         // Which cluster to deploy this service to
  "language": {          // Infomation about the application's language
    "name": "",          // Application language name (Python, Node.js, etc.)
    "version": ""        // Application language version
  },
  "secrets": {           // Information about env vars, stored in Parameter Store
    "common": {          // Secret values shared across applications
      "prefix": "",      // Common secret prefix, usually `/{env}/common/`
      "secrets": []      // List of env vars names
    },
    "service": {         // Secret values specific to application
      "prefix": "",      // App secret prefix, usually `/{env}/{app-type}/{app-name}/`
      "secrets": []      // List of env vars names
    }
  },
  "taskRoleArn": {       // ECS task roles used by service, managed in Terraform
    "sandbox": "",       // Sandbox ECS task role
    "staging": "",       // Staging ECS task role
    "prod": "",          // Production ECS task role
  },
  "tests": {
     "contract": "",     // Override command to run contract tests
     "coverage": "",     // Override command to generate test coverage report
     "integration": "",  // Override command to run integration tests
     "lint": "",         // Override command to run linting
     "smoke": "",        // Override command to run smoke tests
     "type-check": "",   // Override command to run type check
     "unit": ""          // Override command to run unit tests
  },
  "newRelic": {          // New Relic configuration information
    "applicationId": {   // New Relic application IDs
      "sandbox": 0,      // Sandbox application ID
      "staging": 0,      // Staging application ID
      "prod": 0          // Production application ID
    }
  },
  "serviceUrls": {       // Direct service URLs, usually `http://{service-name}.{env}.surfline.com/`
    "prod": "",          // Service URL in production
    "sandbox": "",       // Service URL in sandbox
    "staging": ""        // Service URL in staging
  }
}
```

### Annotated `package.json` Example - Node.js

```javascript
{
  "name": "",
  "version": "",
  "description": "",
  "homepage": "",
  "author": {
    "name": "",
    "url": "",
  },
  "main": "",
  "scripts": {
    "lint": "",              // Command to run linting
    "test:contract": "",     // Command to run contract tests
    "test:coverage": "",     // Command to generate test coverage report
    "test:integration": "",  // Command to run integration tests
    "test:smoke": "",        // Command to run smoke tests
    "test:unit": ""          // Command to run unit tests
    "type-check": "",        // Command to run type check
  },
  "license": "ISC",
  "devDependencies": {},
  "dependencies": {}
}

```

### `Makefile` Example - Python

```Makefile
all: lint type-check test

format:
	@isort -rc .
	@autoflake --remove-all-unused-imports --remove-unused-variables --recursive --in-place .
	@black .

.PHONY: format

# Run linting
lint:
	@flake8 .
	@black --check .

.PHONY: lint

# Run unit tests
test-unit:
	@pytest .

.PHONY: test-unit

# Generate test coverage report
test-coverage:
	@pytest --cov=./

.PHONY: test-coverage

# Run type check
type-check:
	@mypy .

.PHONY: type-check
```
