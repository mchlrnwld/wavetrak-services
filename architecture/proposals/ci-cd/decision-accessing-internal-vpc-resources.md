# Decision Document: Accessing Internal VPC Resources From GitHub Actions Workflows

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [What are we doing?](#what-are-we-doing)
- [What problem are we solving?](#what-problem-are-we-solving)
- [What is the solution?](#what-is-the-solution)
  - [GitHub Actions Self-Hosted Runners](#github-actions-self-hosted-runners)
    - [Pros: Self-Hosted Runners](#pros-self-hosted-runners)
    - [Cons: Self-Hosted Runners](#cons-self-hosted-runners)
    - [Self-Hosted Runners Risk Mitigation](#self-hosted-runners-risk-mitigation)
- [What are the alternatives?](#what-are-the-alternatives)
  - [Alternative: Open direct access to upstream services from GitHub Actions](#alternative-open-direct-access-to-upstream-services-from-github-actions)
    - [Pros: Direct Access](#pros-direct-access)
    - [Cons: Direct Access](#cons-direct-access)
  - [Alternative: Add routes used by tests to services-proxy](#alternative-add-routes-used-by-tests-to-services-proxy)
    - [Pros: services-proxy](#pros-services-proxy)
    - [Cons: services-proxy](#cons-services-proxy)
  - [Alternative: VPN into our VPC from within GitHub Actions workflows](#alternative-vpn-into-our-vpc-from-within-github-actions-workflows)
    - [Pros: VPN](#pros-vpn)
    - [Cons: VPN](#cons-vpn)
  - [Alternative: SSH/Wireguard tunnel into our VPC from within GitHub Actions workflows](#alternative-sshwireguard-tunnel-into-our-vpc-from-within-github-actions-workflows)
    - [Pros: SSH/Wireguard Tunnel](#pros-sshwireguard-tunnel)
    - [Cons: SSH/Wireguard Tunnel](#cons-sshwireguard-tunnel)
- [Other](#other)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## What are we doing?

Determining how to allow GitHub Actions workflows to access internal VPC resources.

## What problem are we solving?

Some integration and end-to-end tests must access upstream services and MongoDB that are only accessible from within our VPC. This was not a problem previously because Jenkins ran in the same VPC as the resources required by these tests. We must solve this problem in order for these tests to run on GitHub Actions.

While this decision document is focused on the CI/CD testing use case, there are other use cases within our organization that would benefit from solving this problem. One example is Analytics' Segment Sync, which loads event metadata to Redshift via GitHub Actions and requires access to Redshift through our VPC.

## What is the solution?

### GitHub Actions Self-Hosted Runners

GitHub Actions self-hosted runners allow portions of workflows to be run on EC2 instances within our AWS account and inside our private VPC. They would have access to any resources that the EC2 instances have access to. Most notably, they would have direct access to services via their internal addresses. This is how services are accessed by most services. Integration and E2E tests on self-hosted runners work with no modifications. Self-hosted runners also allow access to RDS, Elasticache, and MongoDB Atlas.

#### Pros: Self-Hosted Runners

- Completely solves access problem
  - Since jobs execute on our EC2 instances in our VPC, we have total control over what they can access
- Relatively simple to configure and run

#### Cons: Self-Hosted Runners

- Need to maintain all workflow dependencies on self-hosted runner EC2 instances
  - This can get complicated when considering the different Node.js and Python versions run by application
- Need to monitor instances and ensure that they're always available
- We would likely eventually add autoscaling for optimization

#### Self-Hosted Runners Risk Mitigation

While self-hosted runners are the "correct" solution to this problem, there is some risk in going forward with them. Since we have no experience with them outside of running them in proof-of-concepts, we don't know how much long-term effort they will take to maintain. We don't know if they will run hands-off or if they will require significant attention.

Since they're quite easy to get up and running, I propose that we move forward with self-hosted runners in a trial period while paying close attention to the operational overhead that they add. If we find that we're spending non-trivial amounts of time maintaining them, we will back out and seek another solution.

## What are the alternatives?

### Alternative: Open direct access to upstream services from GitHub Actions

To open access to services from GitHub Actions, we would need to do the following:

- Create external load balancer target groups that route requests to services
  - We would likely only need to expose staging services as upstreams to tests
- Add DNS records for each service pointing to ALB
- Add security group limiting access to services to GitHub Actions
  - We would need to grant access to all of Microsoft Azure

#### Pros: Direct Access

- Solves upstream access problem
- Relatively easy to implement and maintain

#### Cons: Direct Access

- Insecure
  - We would need to open access to all of Azure
  - Direct access to some services expose "trusted" functionality that should be behind auth
- Some use cases need access to databases and other non-service resources
  - This wouldn't solve that problem

### Alternative: Add routes used by tests to services-proxy

Similar to opening direct access to upstream services, this solution would add routes used by tests to `services-proxy`. To do this, we would need to do the following:

- Determine all upstream services/routes used by tests
- Compare this list to the services/routes exposed by `services-proxy`
- Add any services/routes that don't exist
- Restrict accesss to any new services/routes to GitHub Actions (all of Azure)

#### Pros: services-proxy

- Solves upstream access problem

#### Cons: services-proxy

- Insecure
  - We would need to open access to all of Azure
  - Direct access to some services expose "trusted" functionality that should be behind auth
- Some use cases need access to databases and other non-service resources
  - This wouldn't solve that problem
- It would be a lot of work to track and maintain services/routes used by tests
- Since tests would run with `services-proxy` upstream rather than direct service upstreams, they wouldn't truly test integration

### Alternative: VPN into our VPC from within GitHub Actions workflows

When Greg Clunies was setting up analytics workflows, he was temporarily able to work around this problem by setting up an OpenVPN connection into the `surfline-dev` AWS account.

#### Pros: VPN

- Solves upstream access problem
- Secure
- Solves DB access issue for RDS/Redshift/Elasticache but not MongoDB

#### Cons: VPN

- Some use cases need access to MongoDB, this wouldn't solve that problem
- The analytics implementation of this solution was unstable because our VPN server doesn't allow multiple simultaneous connections from the same user
  - There are likely ways to solve this but they would require some effort
- More research needed if we were to seriously consider this solution

### Alternative: SSH/Wireguard tunnel into our VPC from within GitHub Actions workflows

#### Pros: SSH/Wireguard Tunnel

- Solves upstream access problem
- Secure
- Solves DB access issue for RDS/Redshift/Elasticache but not MongoDB

#### Cons: SSH/Wireguard Tunnel

- Some use cases need access to MongoDB, this wouldn't solve that problem
- Service upstreams would need to be updated to reference tunnels
- Resources that connect with SSL would by difficult to tunnel
- More research needed if we were to seriously consider this solution

## Other

- [GitHub Docs: About Self-Hosted Runners](https://docs.GitHub.com/en/actions/hosting-your-own-runners/about-self-hosted-runners)
