# CI/CD Architecture

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [What Are We Doing?](#what-are-we-doing)
  - [What Are We Not Doing?](#what-are-we-not-doing)
- [What Problem Are We Solving?](#what-problem-are-we-solving)
  - [Fundamentals of CI/CD](#fundamentals-of-cicd)
    - [Definitions](#definitions)
- [What Impact Does This Have on the Business?](#what-impact-does-this-have-on-the-business)
- [What Is the Solution?](#what-is-the-solution)
  - [Continuous Integration + Continuous Delivery + Preview Environments](#continuous-integration--continuous-delivery--preview-environments)
  - [Build Pipelines](#build-pipelines)
    - [Basic Service Build Pipeline](#basic-service-build-pipeline)
    - [Basic Service Deploy Pipeline](#basic-service-deploy-pipeline)
    - [Parallel Test Pipeline](#parallel-test-pipeline)
  - [Supported Artifacts](#supported-artifacts)
  - [Implementation Philosophy](#implementation-philosophy)
  - [Environments and Release Strategies](#environments-and-release-strategies)
    - [Release Strategy Overrides](#release-strategy-overrides)
    - [Manual Release Triggers](#manual-release-triggers)
    - [Release Observability](#release-observability)
  - [Implementation Phases](#implementation-phases)
    - [1. Establish Platform](#1-establish-platform)
    - [2. Create Individual Pipeline Skeletons](#2-create-individual-pipeline-skeletons)
    - [3. Define Application Contracts](#3-define-application-contracts)
    - [4. Implement CI/CD Testing & Linting](#4-implement-cicd-testing--linting)
    - [5. Measure Code Coverage](#5-measure-code-coverage)
    - [6. Implement CI/CD Artifact Builds](#6-implement-cicd-artifact-builds)
    - [7. Implement CI/CD Deployments](#7-implement-cicd-deployments)
    - [8. Implement Preview Environments](#8-implement-preview-environments)
  - [Existing Builds That Will Be Replaced](#existing-builds-that-will-be-replaced)
    - [ECS Services](#ecs-services)
    - [Lambda Functions](#lambda-functions)
    - [Static Site Bundles](#static-site-bundles)
    - [Airflow Jobs](#airflow-jobs)
    - [npm Packages](#npm-packages)
    - [pip Packages](#pip-packages)
- [What Are the Alternatives?](#what-are-the-alternatives)
  - [GitLab / Bitbucket Pipelines / CircleCI / Jenkins / etc.](#gitlab--bitbucket-pipelines--circleci--jenkins--etc)
  - [GitOps](#gitops)
- [Other](#other)
  - [Jira](#jira)
  - [Resources](#resources)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## What Are We Doing?

Composing CI/CD pipelines for a range of artifacts that allow us to quickly ship changes with high confidence.

### What Are We Not Doing?

- Android/iOS apps
  - They have their own monorepos and CI/CD pipelines
- Infrastructure
  - Terraform CI/CD is planned for FY21Q4 as a separate initiative
- Legacy applications like ColdFusion
  - They will be removed soon
- Replace all builds in Jenkins
  - This initiative covers builds for all applications in the monorepo. There are several old builds that won't be carried forward.

## What Problem Are We Solving?

Our CI/CD process is incomplete and fragmented. We're missing an established deployment pattern for Lambda functions. Tests are not always run on PR. Code quality isn't thoroughly checked on PR. Developers need a map and compass to figure out how to deploy services because there's a lack of consistency across them. This slows velocity and creates a frustrating development experience. 

This initiative defines an architecture and direction for our CI/CD system. We'll define a CI/CD contract that will support a range of CI/CD best practices. Teams will then move their services to this system in order to adopt that functionality.

### Fundamentals of CI/CD

- Traceable, visible deployments
- Easy to adopt
- Easy to rollback
- Scalable
- Fast
- Preview environments
- Automated dependency update PRs
- Documentation builds
- Secrets scanning, code security static analysis
- Bundle size analysis
- Effortless Code Quality

To achieve this, we'll do the following:

- Establish templates for common applications like web services, backend services, Lambda functions, Airflow jobs, Next.js apps, etc.
- Optimize CI/CD delivery for those templates
- Add hooks that enable consuming teams to adopt a given PR check or validation on their schedule
- Deeply integrate with Github
- Automatic sandbox deployment: automatic notification to slack with diff. Prevents multiple coupled services being deployed.

#### Definitions

##### Continuous Integration ("CI")

Continuous Integration means that each code push to a Git repository is built and tested automatically. If a pull request is associated with the pushed code, test and linting results are reported to it and if the tests fail, the PR is blocked from merging.

##### Continuous Delivery ("CD")

Continuous Delivery builds upon Continuous Integration. Artifacts from the builds, like container images, are pushed to repositories but are not deployed. Applications are in an "always-deployable" state. Deployments are triggered manually.

##### Continuous Deployment (also "CD")

Continuous Deployment builds upon Continuous Delivery. Instead of deploying applications manually, they are deployed automatically with no human intervention.

## What Impact Does This Have on the Business?

CI/CD pipelines are absolutely critical to delivering high-quality software. It should be effortless to run tests and view results. Quality check will automatically run and report back to PRs. Engineers will have confidence merging code and quality will be higher. Development velocity will increase. Bugs and incidents will be reduced. Engineering capacity for product development will increase.

More reliable &rarr; less user/subscriber churn due to flaky product/service.

More capacity for product development &rarr; more time spent on initiatives driving subscriber growth.

## What Is the Solution?

### Continuous Integration + Continuous Delivery + Preview Environments

As described above, continuous integration and continuous delivery will run tests and build artifacts for all code pushed to Github but it will not automatically deploy that code to any application environments. Continuous Deployment will happen for some but not all services and environments. This is described in detail below under [Environments and Release Strategies](#environments-and-release-strategies).

Preview environments will be created for most applications types as part of the build pipeline. For ECS services, a standalone task will be created on ECS with its environment variables pointing at `staging` resources. For Lambda functions, the function will be deployed as a preview version. Static webapps will be bundled and pushed to S3. For all applications, the pull request will be updated to include the URL to access the preview environment. Preview environments will be destroyed and static bundles will be removed when PRs are merged or closed.

### Build Pipelines

Modern build pipelines exist as a series of build stages. Each stage is responsible for a single action. Stages may be executed sequentially or in parallel.

Examples of a few common basic build pipelines are below.

#### Basic Service Build Pipeline

Service builds will run when branches are pushed to Github.

If a Github PR exists, the pipeline will report test status to the Github PR, blocking merging when tests fail.

![Basic Service Build Pipeline](diagrams/basic-service-build-pipeline.png)

#### Basic Service Deploy Pipeline

This shows how we would deploy a service to ECS using a build pipeline:

![Basic Service Deploy Pipeline](diagrams/basic-service-deploy-pipeline.png)

#### Parallel Test Pipeline

Modern build pipelines support parallel stages. Tests are good candidates for parallel execution:

![Parallel Test Pipeline](diagrams/parallel-test-pipeline.png)

### Supported Artifacts

- ECS services
- Lambda functions
- Static site bundles
- Airflow jobs
- npm packages
- pip packages

### Implementation Philosophy

- We should minimize the CI/CD code that we write and maintain
- Build pipelines should be observable
- We should use prebuilt platform plugins/actions as much as possible
  - To limit security risk, we should only use prebuilt plugins/actions that have been verified by the platform
  - If we must use a third-party plugin/action, we must...
    - Audit the source code for security issues
    - Pin the version to an entire commit SHA to ensure that the release we're using is immutable

### Environments and Release Strategies

| Environment | Description             | Release Strategy                                      |
| :---------- | :----------             | :---------------                                      |
| `prod`      | Production              | Manual release from merge-commit on master            |
| `staging`   | Pre-production          | Automated release on merge to master                  |
| `preview`   | Per-application preview | Automated release from commit SHA on PR create/update |
| `sandbox`   | Free-for-all            | Manual release from branch, commit SHA, or master     |

#### Release Strategy Overrides

You can override the default release strategy on a per-application basis. The most common use case for this will be to allow services to be deployed to production automatically when pull requests are merged to master.

To override a release strategy for an application, set the following in a `ci.json` file in the root of the application:

```json
{
  "releaseStrategy": {
    "prod": "automatic"
  }
}
```

These are the possible values:

| Value       | Description                                                   |
| :----       | :-----                                                        |
| `automatic` | Releases happen automatically. This is continuous deployment. |
| `manual`    | Releases must be triggered manually.                          |

#### Manual Release Triggers

For services and environments that require manual releases, we will use the "Run workflow" dropdown in GitHub Actions to manually trigger a workflow and deploy. This will prompt the user to enter the service name, environment, and Git reference (commmit SHA, branch, tag, etc.) before running tests, building the application, and deploying it to the specified environment.

#### Release Observability

Releases should be observable. Github Actions observability is a bit lacking out-of-the-box. To solve this problem, all releases will update New Relic with release information and dashboards will be created in New Relic showing release information.

### Implementation Phases

![Implementation Phases Schedule](diagrams/implementation-phases-schedule.png)

#### 1. Establish Platform

Expected: FY21Q2 Sprint 3

- Create vendor analysis document skeleton
- Vendor analysis: Github Actions
- Vendor analysis: Codefresh
- Decide on a platform
- Create CI/CD platform skeleton in monorepo

The vendor selection process for our CI/CD platform will be brief. Github Actions seems to fulfill all of our requirements but [Codefresh's](https://codefresh.io/) observability and plugin library make it worth reviewing. The vendor analysis document is located at [vendor-selection.md](./vendor-selection.md) and contains the following criteria:

- Pipelines
- Plugins
  - How much customization will need to be written?
- Ease of use with monorepo
- Developer experience
- Observability
- Security requirements
  - TFA, SSO, etc. ?
- Training and onboarding
  - How much effort for the team to learn the new system?
  - Is training available or provided and at what cost?
- Support
  - Are there any SLAs to consider?
  - Do we need and are there professional services available?
  - What level of community support exists?
- Integrations
  - ECS
  - Are there any other required or nice to have integrations?
    - GitOps
    - Kubernetes
    - Bring your own worker, for accessing resources within VPC
  - Any API requirements?
- Vendor lock-in
  - How much optionality do we have when going with a particular solution?
- Pricing
  - What is the model and how does it scale with more people/code?

#### 2. Create Individual Pipeline Skeletons

Expected: FY21Q2 Sprint 4

Supported artifacts like ECS services, Lambda functions, etc will have their own build pipelines. This phase of the implementation sets up pipeline skeletons for each artifact type.

Each pipeline will include path globs or regexes that will trigger the appropriate build based on the location of the changes files. For example, changes under `services/*/*` will trigger the ECS service build pipeline, changes under `functions/*/*` will trigger the Lambda function build pipeline, etc.

Pipelines will only support changes in one application per PR/branch. This is by design to avoid situations with brittle and risky deployment orchestration requirements. Code pushes that contain changes in multiple applications will trigger an error in the pipelines.

#### 3. Define Application Contracts

Expected: Details below

Application contracts will define how applications should be structured to be able to use CI/CD stages out of the box. For example, any Node.js ECS service will be assumed to have scripts defined in `package.json` supporting `npm run test`, `npm run lint`, `npm run type-check`, etc. When defining application contracts, we will explicitly document these assumptions.

Applications will be able to override these contracts. This will allow for custom build tasks for specific applications. The mechanism to override contracts will be documented alongside the application contracts.

Rather than define all contracts up front, we plan to define artifact CI/CD contracts for each stage closer to the time that we implement each pipeline stage. Basic contracts such as where each artifact should live in the Monorepo will be defined early. This will allow us to release testing & linting faster and will give us the benefit of more platform experience when defining contracts.

Expected: FY21Q2 Sprint 4

- Test/Lint contracts
- Code Coverage contracts

Expected: FY21Q3 Sprint 1

- Artifact build contracts
- Deployment contracts

Expected: FY21Q4 Sprint 1

- Preview environments contracts

#### 4. Implement CI/CD Testing & Linting

Expected: FY21Q2 Sprints 5, 6

For all application pipelines, add testing and linting stages. These stages should run in parallel where possible for faster pipeline runs. 

More specifically, these test stages will be implemented. Not all stages will run for all applications.

- Unit tests
- Integration tests
- Smoke tests
- Contract tests
- Secrets scanning
  - [Github Advanced Security - Secret Scanning](https://docs.github.com/en/code-security/secret-security/about-secret-scanning)
- Security static analysis
  - [Github Advanced Security - Code Scanning](https://docs.github.com/en/code-security/secure-coding/about-code-scanning)
  - [SonarQube](https://www.sonarqube.org/)
- Calibreapp performance budget analysis
- Bundle size analysis

#### 5. Measure Code Coverage

Expected: FY21Q2 Sprint 6, FY21Q3 Sprint 1

We will use [Codecov](https://about.codecov.io/) to track code test coverage. We will set up a Codecov account and add pipeline stages to generate test coverage reports and upload those reports to Codecov.

#### 6. Implement CI/CD Artifact Builds

Expected: FY21Q3 Sprints 1, 2, 3, 4

In this implementation phase, we will build deployable artifacts for each of our application types and push the artifacts to the appropriate repositories. For ECS services, this deployable artifact will be a Docker container image pushed to ECR. For static webapps, this will be a code bundle pushed to S3. For Lambda functions, this will be a bundled code archive pushed to S3. For npm and pip packages, this will be a package pushed to Artifactory (but not marked as the latest release).

| Application Type    | Artifact Destination                                          |
| :------------------ | :------------------------------------------------------------ |
| ECS services        | Docker container image in ECR, tagged with commit SHA         |
| Lambda functions    | Lambda function packaged for deployment                       |
| Static site bundles | Code bundle in S3 in location not referenced by CDN           |
| Airflow jobs        | Docker container image in ECR, tagged with commit SHA         |
| Packages            | npm/pip package in Artifactory, versioned as pre-release      |

#### 7. Implement CI/CD Deployments

Expected: FY21Q3 Sprints 4, 5, 6

In this implementation phase, we will deploy the already-built artifacts to `sandbox`, `staging`, and `prod` for each of our application types.

| Application Type    | Deployment Destination                                                                                       |
| :------------------ | :------------------------------------------------------------                                                |
| ECS services        | ECS service updated to use new image container                                                               |
| Lambda functions    | Lambda function updated to use new code archive                                                              |
| Static site bundles | Code bundle in S3, CDN pointing at bundle, CDN cache cleared                                                 |
| Airflow jobs        | Docker container image in ECR tagged as `latest`, Airflow jobs deployed to servers, Airflow daemon restarted |
| Packages            | npm/pip package in Artifactory, versioned as latest release                                                  |

#### 8. Implement Preview Environments

Expected: FY21Q4 Sprints 1, 2, 3

Preview environments should be automatically created when PRs are created, updated when branches with open PRs are pushed to, and destroyed when PRs are merged and/or closed. When preview environments are created, their corresponding PRs should be automatically updated to include the location of the preview environment.

Preview environments should use staging environment upstreams.

| Application Type     | Preview Environment                                                            |
| :------------------  | :------------------------------------------------------------                  |
| ECS services         | Standalone task on `staging` `core-services` ECS cluster                       |
| Lambda functions     | Lambda function deployed to `staging` using unique alias                       |
| Static site bundles  | Code bundle in S3, referenced by CDN using preview path                        |
| Airflow jobs         | No preview environment                                                         |
| npm packages (UI)    | Preview environment using something like <https://bit.dev/>                    |
| npm packages (Other) | No preview environment, pre-released versioned package available in Artifactory |
| pip packages         | No preview environment, pre-released versioned package available in Artifactory |

### Existing Builds That Will Be Replaced

#### ECS Services

- [surfline/Web/deploy-admin-service-to-ecs](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-admin-service-to-ecs/)
- [surfline/Web/deploy-buoyweather-webapp-to-ecs](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-buoyweather-webapp-to-ecs/)
- [surfline/Web/deploy-editorial-to-ecs](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-editorial-to-ecs/)
- [surfline/Web/deploy-frontend-to-ecs](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-frontend-to-ecs/)
- [surfline/Web/deploy-service-to-ecs](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-service-to-ecs/)
- [surfline/Web/deploy-web-proxy-to-ecs](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-web-proxy-to-ecs/)
- [surfline/poll-maxmind-database](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/poll-maxmind-database/)

#### Lambda Functions

- [admin/deploy-spot-to-elasticsearch-function](https://surfline-jenkins-master-prod.surflineadmin.com/job/admin/job/deploy-spot-to-elasticsearch-function/)
- [surfline/microservices/deploy-lambda-functions-admin](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-admin/)
- [surfline/microservices/deploy-lambda-functions-cameras-service-rewind-clip](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-cameras-service-rewind-clip/)
- [surfline/microservices/deploy-lambda-functions-editorial-cms](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-editorial-cms/)
- [surfline/microservices/deploy-lambda-functions-notifications-service](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-notifications-service/)
- [surfline/microservices/deploy-lambda-functions-sessions-api](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/microservices/job/deploy-lambda-functions-sessions-api/)

#### Static Site Bundles

- [surfline/Web/build-and-deploy-account-to-s3](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/build-and-deploy-account-to-s3/)
- [surfline/Web/build-and-deploy-sl-admin-to-s3](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/build-and-deploy-sl-admin-to-s3/)
- [surfline/Web/deploy-crossbrand-frontend-to-ecs-master](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/deploy-crossbrand-frontend-to-ecs-master/)

#### Airflow Jobs

- [shared/deploy-airflow-job](https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/deploy-airflow-job/)

#### npm Packages

- [/shared/deploy-wavetrak-common](https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/deploy-wavetrak-common/)
- [shared/deploy-quiver](https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/build-quiver/)
- [shared/deploy-quiver](https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/deploy-quiver/)
- [shared/release-quiver-to-frontends](https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/release-quiver-to-frontends/)
- [shared/release-quiver](https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/release-quiver/)

#### pip Packages

- [/shared/deploy-wavetrak-common](https://surfline-jenkins-master-prod.surflineadmin.com/job/shared/job/deploy-wavetrak-common/)

## What Are the Alternatives?

### GitLab / Bitbucket Pipelines / CircleCI / Jenkins / etc.

This is covered in the vendor analysis document at [vendor-selection.md](./vendor-selection.md).

GitLab and Bitbucket have compelling CI/CD tools but they don't represent enough of an improvement over Github Actions that would justify moving our code repos away from Github.

Circle CI is the solution that we were leaning strongly towards when we initially discussed this initiative a couple years ago but they haven't kept pace with Github Actions.

Jenkins is a solid CI/CD platform but it has high operational overhead.

### GitOps

GitOps is an excellent operating model where every bit of data about the configuration and state of the system exists in Git. It typically uses two repositories: An application repository for application code and an environment repository for config and desired system state. When the desired system state differs from actual system state, deployment pipelines are triggered to resolve that difference. It's a good operating model but it represents too big of a leap right now. Also, most of the tooling for GitOps works best with Kubernetes.

We'll keep an eye on GitOps as a future migration path beyond the scope of this initiative.

- <https://www.gitops.tech/>
- <https://www.weave.works/blog/gitops-high-velocity-cicd-for-kubernetes>

## Other

### Jira

The best place to view this initiative in Jira is in the [Infrastructure Roadmap](https://wavetrak.atlassian.net/secure/PortfolioReportView.jspa?r=ANJRT) in Jira Plans.

This work is tracked in Jira under the following initiative:

- [TP-83 Improve CI/CD](https://wavetrak.atlassian.net/browse/TP-83)

This initiative contains the following epics:

- [INF-624 CI/CD Planning](https://wavetrak.atlassian.net/browse/INF-624)
- [INF-623 Establish CI/CD Platform](https://wavetrak.atlassian.net/browse/INF-623)
- [INF-625 Create CI/CD Pipeline Skeletons ](https://wavetrak.atlassian.net/browse/INF-625)
- [INF-620 Define Artifact CI/CD Contracts](https://wavetrak.atlassian.net/browse/INF-620)
- [INF-622 Implement CI/CD Testing & Linting](https://wavetrak.atlassian.net/browse/INF-622)
- [INF-621 Measure Code Coverage](https://wavetrak.atlassian.net/browse/INF-621)
- [INF-619 Implement CI/CD Artifact Builds](https://wavetrak.atlassian.net/browse/INF-619)
- [INF-618 Implement CI/CD Deployments](https://wavetrak.atlassian.net/browse/INF-618)
- [INF-617 Implement Preview Environments](https://wavetrak.atlassian.net/browse/INF-617)

### Resources

- [Awesome Github Actions](https://github.com/sdras/awesome-actions#official-actions)

