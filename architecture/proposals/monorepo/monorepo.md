# Monorepo Architecture

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [What Are We Doing?](#what-are-we-doing)
  - [What Are We Not Doing?](#what-are-we-not-doing)
- [What Problem Are We Solving?](#what-problem-are-we-solving)
- [What Impact Does This Have on the Business?](#what-impact-does-this-have-on-the-business)
- [What Is the Solution?](#what-is-the-solution)
  - [Monorepo Structure](#monorepo-structure)
    - [Infrastructure Subdirectory Details](#infrastructure-subdirectory-details)
    - [.github Subdirectory Details](#github-subdirectory-details)
      - [Github Actions Workflow Naming](#github-actions-workflow-naming)
    - [Application Naming](#application-naming)
      - [Service Naming](#service-naming)
        - [`name`](#name)
        - [`scope`](#scope)
        - [`type`](#type)
        - [Updated Service Names](#updated-service-names)
    - [Application Structure + Contracts](#application-structure--contracts)
      - [CI/CD Contracts](#cicd-contracts)
      - [Structures By Language and Application Type](#structures-by-language-and-application-type)
      - [General Service Structure](#general-service-structure)
    - [Monorepo Structure Questions](#monorepo-structure-questions)
  - [How and When Will the Applications Be Moved?](#how-and-when-will-the-applications-be-moved)
  - [Will Repo History Persist for Moved Applications?](#will-repo-history-persist-for-moved-applications)
  - [List of Applications To Move](#list-of-applications-to-move)
    - [`bw`](#bw)
    - [`cam-dashboard`](#cam-dashboard)
    - [`magicseaweed`](#magicseaweed)
    - [`quiver`](#quiver)
    - [`styleguide`](#styleguide)
    - [`science-job`](#science-job)
    - [`surfline-admin`](#surfline-admin)
    - [`surfline-segment`](#surfline-segment)
    - [`surfline-sisense-models`](#surfline-sisense-models)
    - [`surfline-web`](#surfline-web)
    - [`surfline-web-editorial`](#surfline-web-editorial)
    - [`wavetrak-infrastructure`](#wavetrak-infrastructure)
    - [`wavetrak-services`](#wavetrak-services)
- [How Does This Solve the Problem?](#how-does-this-solve-the-problem)
- [What Are the Alternatives?](#what-are-the-alternatives)
  - [Leave Things As-Is](#leave-things-as-is)
  - [Break Applications Into Single-Application Repos](#break-applications-into-single-application-repos)
- [Other](#other)
  - [Resources](#resources)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## What Are We Doing?

Defining a Wavetrak monorepo structure to improve the way we structure our code and increase benefits from shared tooling.

### What Are We Not Doing?

- Merging every repo in Github into a single monorepo
    - There's a long tail of repos in Github that are deprecated (`surfline-coldfusion`), hyper-specialized (`lotus-dev`, `science-camera-ml`, etc.), or just aren't worth the effort to merge into the monorepo
- Including mobile app repos in the monorepo
    - iOS and Android have their own monorepos
- Moving Science Airflow jobs to the monorepo
    - These will likely be refactored and/or moved in the [Upgrade or Replace Airflow](https://wavetrak.atlassian.net/browse/TP-17) initiative
    - The exception to this is Fishtrack jobs, which will be migrated to the monorepo

## What Problem Are We Solving?

> _“Delivering robust experiences to customers quickly and painlessly“_

It’s painful right now to be a web/service engineer at Surfline.

- Code isn’t stored in a consistent place

- There are few standards and they aren't well-documented or understood

- We have several distinct build pipelines that do the same thing

In order to more clearly understand our codebase and increase benefits of shared tooling like CI/CD pipelines, we should store our code in a more consistent and well-defined manner.

## What Impact Does This Have on the Business?

It’s hard for engineers to onboard. It's a headache for us find where things are in repositories. We constantly have to learn new unintuitive patterns. We aren’t living up to our mission of _“delivering robust experiences to customers quickly and painlessly.“_

This causes slower subscriber growth as a result of lost velocity (painful development, frustrated engineers) and unreliable features. It also increases time to resolve incidents because on-call engineers don't always know where to look for services.

## What Is the Solution?

`wavetrak-services` is already effectively a monorepo. In many cases, applications already exist in their desired locations. By building the monorepo on top of `wavetrak-services`, we get a big head start and minimize what we'll have to move.

- Use `wavetrak-services` as the monorepo base
- Define specific locations for applications
- Set up CI/CD build/deploy pipelines for applications
    - Detailed in [CI/CD Architecture](../ci-cd/ci-cd.md)
- Move applications to monorepo structure
    - For applications outside of the monorepo, move into the monorepo in the correct locations
    - For applications inside the monorepo in the wrong location (ex. Lambda functions in service directories), move into the correct locations
    - All applications should meet CI/CD contracts in order to take advantage of build/deploy pipelines

### Monorepo Structure

| Location          | Application Type               | Notes                                 |
| :---------        | :-----------------             | :------                               |
| `/.github`        | Github repo configuration      | More details below                    |
| `/analytics`      | Analytics applications         | Analytics jobs live in `/jobs`        |
| `/architecture`   | Architecture documents         |                                       |
| `/docs`           | System/department docs         |                                       |
| `/functions`      | Lambda functions               | Standardize on Serverless framework   |
| `/infrastructure` | Standalone infrastructure      | More details below                    |
| `/jobs`           | Recurring jobs                 | Airflow, Batch, Lambda, etc.          |
| `/packages`       | Common packages, all languages | npm/pip packages both live here       |
| `/services`       | ECS services                   | All languages, web + backend services |
| `/tests/e2e`      | End-to-end tests               |                                       |
| `/tests/load`     | System load tests              |                                       |
| `/tools`          | Miscellaneous tooling          |                                       |
| `/web-static`     | Static web apps                |                                       |

#### Infrastructure Subdirectory Details

| Location                            | Application Type                   |
| :---------                          | :-----------------                 |
| `/infrastructure/ami`               | AWS AMI definitions                |
| `/infrastructure/chef`              | Chef cookbooks                     |
| `/infrastructure/cloudformation`    | Cloudformation stacks              |
| `/infrastructure/docker`            | Standalone Docker container images |
| `/infrastructure/scripts`           | Misc. infrastructure scripts       |
| `/infrastructure/terraform`         | Standalone Terraform applications  |
| `/infrastructure/terraform/modules` | Shared Terraform modules           |

#### .github Subdirectory Details

| Location                                     | Notes                                                                                                                                                                                                  |
| :---------                                   | :-----------------                                                                                                                                                                                     |
| `/.github/CODEOWNERS`                        | [Which teams are responsible for code in the repository](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-on-github/about-code-owners)              |
| `/.github/CONTRIBUTING.md`                   | [How to contribute to the repo](https://docs.github.com/en/communities/setting-up-your-project-for-healthy-contributions/setting-guidelines-for-repository-contributors)                               |
| `/.github/PULL_REQUEST_TEMPLATE.md`          | [Default template to show in PR description](https://docs.github.com/en/communities/using-templates-to-encourage-useful-issues-and-pull-requests/creating-a-pull-request-template-for-your-repository) |
| `/.github/workflows/functions-pipeline.yml`  | Functions build/deploy pipeline                                                                                                                                                                        |
| `/.github/workflows/jobs-pipeline.yml`       | Jobs build/deploy pipeline                                                                                                                                                                             |
| `/.github/workflows/packages-pipeline.yml`   | Packages build/deploy pipeline                                                                                                                                                                         |
| `/.github/workflows/services-pipeline.yml`   | Services build/deploy pipeline                                                                                                                                                                         |
| `/.github/workflows/web-static-pipeline.yml` | Web static build/deploy pipeline                                                                                                                                                                       |
| `/.github/workflows`                         | Github Actions workflows                                                                                                                                                                               |

##### Github Actions Workflow Naming

Base directory: `/.github/workflows`

Workflow filenames should use [kebab-case](https://betterprogramming.pub/string-case-styles-camel-pascal-snake-and-kebab-case-981407998841) to follow the naming pattern created by Github.

Filenames and workflow names within the files should express the command that's taking place within the workflow, using [imperative mood](https://www.thoughtco.com/imperative-mood-grammar-1691151).

The `Name` field within the workflow should match the filename, with kebab-case replaced by [Title Case](https://apastyle.apa.org/style-grammar-guidelines/capitalization/title-case).

Examples:

| Filename                                       | Name                                     |
| :----                                          | :----                                    |
| `sync-platform-openapi-spec-to-readme-com.yml` | Sync Platform OpenAPI Spec to Readme.com |
| `build-test-services.yml`                      | Build/Test Services                      |
| `build-test-deploy-functions.yml`              | Build/Test/Deploy Functions              |

#### Application Naming

We should standardize on how we name applications using guidelines described below.

There are tradeoffs on renaming existing services. It would be easy to simply rename the directories that services live in to use a common pattern. But it would be a significant amount of high-risk work to rename services in Terraform to match this common pattern.

We should rename application directories already within the monorepo and when moving applications into the monorepo but leave service names in Terraform as-is. If/when we move to a service mesh (and possibly Kubernetes) in 2022, we will clean up service names in Terraform.

##### Service Naming

Base directory: `/services`

Name: `{name}-{scope}-{type}`

###### `name`

A simple descriptor of what the service does.

Ex. `metering`, `cameras`, `reports`

###### `scope`

Optional. What type of traffic does the service handle?

- `admin` - The service handles only admin traffic

###### `type`

The type of the service.

- `web` - The service primarily returns rendered web responses
- `api` - The service returns API responses, likely in JSON

###### Updated Service Names

| Repo                | Old Name                       | New Name                        |
| :----               | :----                          | :----                           |
| `bw`                | `buoyweather-web`              | `buoyweather-web`               |
| `surfline-web`      | `account`                      | `account-web`                   |
| `surfline-web`      | `backplane`                    | `backplane-web`                 |
| `surfline-web`      | `charts`                       | `charts-web`                    |
| `surfline-web`      | `homepage`                     | `homepage-web`                  |
| `surfline-web`      | `kbyg`                         | `kbyg-web`                      |
| `surfline-web`      | `onboarding`                   | `onboarding-web`                |
| `surfline-web`      | `perks`                        | `perks-web`                     |
| `surfline-web`      | `sitemaps`                     | `sitemaps-web`                  |
| `surfline-web`      | `travel`                       | `travel-web`                    |
| `surfline-web`      | `web-proxy`                    | `proxy-web`                     |
| `wavetrak-services` | `admin-cameras-service`        | `cameras-admin-web`             |
| `wavetrak-services` | `admin-forecasts-service`      | `forecasts-admin-web`           |
| `wavetrak-services` | `admin-login-service`          | `login-admin-web`               |
| `wavetrak-services` | `admin-reports-service`        | `reports-admin-web`             |
| `wavetrak-services` | `admin-search-service`         | `search-admin-web`              |
| `wavetrak-services` | `admin-spots-service`          | `spots-admin-web`               |
| `wavetrak-services` | `admin-users-service`          | `users-admin-web`               |
| `wavetrak-services` | `auth-service`                 | `auth-api`                      |
| `wavetrak-services` | `breaking-wave-height-api`     | `breaking-wave-height-api`      |
| `wavetrak-services` | `buoyweather-api`              | `buoyweather-api`               |
| `wavetrak-services` | `cameras-service`              | `cameras-api`                   |
| `wavetrak-services` | `charts-service`               | `charts-api`                    |
| `wavetrak-services` | `crowd-counting-service`       | `crowd-counting-api`            |
| `wavetrak-services` | `editorial-varnish`            | `editorial-varnish-web`         |
| `wavetrak-services` | `editorial-wp`                 | `editorial-wordpress-web`       |
| `wavetrak-services` | `editorial-wp-admin`           | `editorial-wordpress-admin-web` |
| `wavetrak-services` | `entitlements-service`         | `entitlements-api`              |
| `wavetrak-services` | `favorites-service`            | `favorites-api`                 |
| `wavetrak-services` | `feed-service`                 | `feed-api`                      |
| `wavetrak-services` | `forecasts-api`                | `forecasts-api`                 |
| `wavetrak-services` | `geo-target-service`           | `geo-target-api`                |
| `wavetrak-services` | `graphql-gateway`              | `graphql-gateway-api`           |
| `wavetrak-services` | `image-resizing-service`       | `image-resizing-api`            |
| `wavetrak-services` | `kbyg-product-api`             | `kbyg-product-api`              |
| `wavetrak-services` | `location-view-service`        | `location-view-api`             |
| `wavetrak-services` | `metering-service`             | `metering-api`                  |
| `wavetrak-services` | `msw-tide-service`             | `tide-api`                      |
| `wavetrak-services` | `notifications-service`        | `notifications-api`             |
| `wavetrak-services` | `onboarding-service`           | `onboarding-api`                |
| `wavetrak-services` | `password-reset-service`       | `password-reset-api`            |
| `wavetrak-services` | `promotions-service`           | `promotions-api`                |
| `wavetrak-services` | `reports-api`                  | `reports-api`                   |
| `wavetrak-services` | `science-data-service`         | `science-data-api`              |
| `wavetrak-services` | `science-models-db`            | `science-models-db-api`         |
| `wavetrak-services` | `search-service`               | `search-api`                    |
| `wavetrak-services` | `services-proxy`               | `proxy-api`                     |
| `wavetrak-services` | `session-analyzer`             | `session-analyzer-api`          |
| `wavetrak-services` | `sessions-api`                 | `sessions-api`                  |
| `wavetrak-services` | `spot-recommender-svc`         | `spot-recommender-api`          |
| `wavetrak-services` | `spots-api`                    | `spots-api`                     |
| `wavetrak-services` | `subscription-service`         | `subscription-api`              |
| `wavetrak-services` | `subscription-service-backend` | `subscription-backend-api`      |
| `wavetrak-services` | `user-service`                 | `user-api`                      |
| `wavetrak-services` | `wave-api`                     | `wave-api`                      |
| `wavetrak-services` | `weather-api`                  | `weather-api`                   |

#### Application Structure + Contracts

Application contracts can mean several things. For our purposes, let's define it as follows:

##### CI/CD Contracts

We will define contracts that applications must meet to support build, testing, code coverage, deployment, etc. These will be defined in [../ci-cd/contracts.md](../ci-cd/contracts.md).

##### Structures By Language and Application Type

These structures should be defined by technology. For example, Express services should have a specific structure, React services should have a specific structure, Golang services should have a specific structure, etc. These will be defined by guilds and/or the Web Platform squad. This will likely be documented in the styleguide.

##### General Service Structure

Regardless of language, framework, or service type, all services should have the following:

| Location                                      | Notes                                             |
| :---------                                    | :-----------------                                |
| `/services/{service-name}/`                   | Application root directory                        |
| `/services/{service-name}/ci.json`            | Application-specific CI/CD settings               |
| `/services/{service-name}/infra/`             | Application-specific infrastructure, in Terraform |
| `/services/{service-name}/tests/integration/` | Application-specific integration tests            |
| `/services/{service-name}/tests/load/`        | Application-specific load tests                   |
| `/services/{service-name}/tests/smoke/`       | Application-specific smoke tests                  |
| `/services/{service-name}/scripts/`           | Application-specific helper scripts               |

#### Monorepo Structure Questions

| Q: Should backend and web services both live in `/services`? |
| :---- |
| A: Yes. Their current build pipelines are very similar. The main exception is that some web services builds generate static assets that are pushed to S3. It should be easy to branch a build pipeline to do this for web services and not for other services. |

| Q: Should `npm` and `pip` packages both live in `/packages`? |
| :---- |
| A: Yes. From a CI/CD perspective, it's fine to include both in the same location. This is what we've been doing up to now. We would differentiate between `npm`, `pip`, and other packages through paths in the CI/CD config. |

| Q: Is `/web-static` a reasonable location for Next.js and other static apps? Do we have any hybrid apps that generate static bundles alongside server-side rendering? |
| :---- |
| A: Yes, all static applications with no server component should live in `/web-static`. |

| Q: Will this monorepo structure work with any opinionated tools that we care about? |
| :---- |
| A: Yes, as far as we can tell. |

| Q: Should `@surfline/quiver` live in `/packages/quiver` or elsewhere? |
| :---- |
| A: Yes, but broken into components, like `/packages/quiver-assets` or `/packages/quiver-themes`. |


### How and When Will the Applications Be Moved?

Teams will be responsible for moving the applications that they own. The Infrastructure team will work with tech leads and product owners to create Jira tasks to track service migration and schedule those tasks for completion.

For applications with corresponding CI/CD pipelines, we should wait to move them until after the deploy stages of their CI/CD pipelines are complete. This is currently scheduled for FY21Q3 Sprints 4, 5, 6. It should be safe to plan to move these applications in FY2021 Q4.

Applications without CI/CD pipelines (or with Github Actions pipelines in their repos) can be moved at any time.

### Will Repo History Persist for Moved Applications?

No. It complicates migrations significantly to merge repo history into the monorepo when moving applications. We can archive repos with valuable history in Github. We'll rename them to `archived-{repo-name}` and set them to read-only.

### List of Applications To Move

#### `bw`

| Previous Location (`bw`) | New Location (`wavetrak-services`) | Owner  |
| :--------                | :--------                          | :----  |
| `/web`                   | TO BE REMOVED                      | MSW/BW |
| `/webapp`                | `/services/buoyweather-web`        | MSW/BW |
| `/wordpress`             | TO BE REMOVED                      | MSW/BW |

#### `cam-dashboard`

| Previous Location (`cam-dashboard`) | New Location (`wavetrak-services`)      | Owner    |
| :--------                           | :--------                               | :----    |
| `/`                                 | `/services/cameras-dashboard-admin-web` | Realtime |

#### `magicseaweed`

The document [Moving MSW to AWS - Phase 1](https://github.com/Surfline/wavetrak-services/blob/PS-1673-msw-aws-arch/architecture/document/move-msw-to-aws.md#phase-1) is the source-of-truth for how MSW will be migrated to the monorepo.

#### `quiver`

| Previous Location (`quiver`) | New Location (`wavetrak-services`)   | Owner        |
| :--------                    | :--------                            | :----        |
| `/assets`                    | `/packages/quiver-assets`            | Web Platform |
| `/page-container`            | `/packages/quiver-page-container`    | Web Platform |
| `/react`                     | `/packages/quiver-react`             | Web Platform |
| `/themes`                    | `/packages/quiver-themes`            | Web Platform |

#### `styleguide`

| Previous Location (`styleguide`) | New Location (`wavetrak-services`) | Owner           |
| :--------                        | :--------                          | :----           |
| `/bash`                          | `/docs/styleguide/bash`            | Infra           |
| `/javascript`                    | `/docs/styleguide/javascript`      | Web Platform    |
| `/python`                        | `/docs/styleguide/python`          | Forecast        |
| `/swift`                         | `/docs/styleguide/swift`           | Native Platform |
| `/terraform`                     | `/docs/styleguide/terraform`       | Infra           |

#### `science-job`

This covers only jobs owned by the Fishtrack team. Other science jobs will remain in `science-job` until they are deprecated and removed.

The migration of these jobs should be coordinated with Infrastructure and likely scheduled after [TP-17: Upgrade or replace Airflow](https://wavetrak.atlassian.net/browse/TP-17).

| Previous Location (`science-job`)                | New Location (`wavetrak-services`)              | Owner  |
| :--------                                        | :--------                                       | :----  |
| `/prod/avhrr-australia_ft-data-download.job`     | `/jobs/download-avhrr-australia`                | MSW/BW |
| `/prod/avhrr-north-america_ft-data-download.job` | `/jobs/download-avhrr-north-america`            | MSW/BW |
| `/prod/modis_ft-data-download-v2.job`            | `/jobs/download-modis`                          | MSW/BW |
| `/prod/modis_ft-image-gen.job`                   | `/jobs/process-fishtrack-modis-generate-images` | MSW/BW |
| `/prod/rtofs-diagnostic.job`                     | `/jobs/download-rtofs-diagnostic`               | MSW/BW |
| `/prod/rtofs-prognostic.job`                     | `/jobs/download-rtofs-prognostic`               | MSW/BW |
| `/prod/truecolor_process.job`                    | `/jobs/process-fishtrack-truecolor`             | MSW/BW |

#### `surfline-admin`

| Previous Location (`surfline-admin`)       | New Location (`wavetrak-services`)  | Owner        |
| :--------                                  | :--------                           | :----        |
| `/functions/sort-subregion-spots`          | `/functions/sort-subregion-spots`   | Forecast     |
| `/functions/spotreportview-builder`        | `/functions/spotreportview-builder` | Forecast     |
| `/modules/cameras`                         | `/services/cameras-admin-web`       | Realtime     |
| `/modules/common`                          | `/packages/common-admin`            | Web Platform |
| `/modules/forecasts`                       | `/services/forecasts-admin-web`     | Forecast     |
| `/modules/gifts`                           | `/web-static/gifts-admin`           | Subs         |
| `/modules/internal-tools/banner_helper`    | `/tools/admin-banner-helper`        | Web Platform |
| `/modules/internal-tools/googledfp_helper` | `/tools/admin-google-dfp-helper`    | Web Platform |
| `/modules/login`                           | `/services/login-admin-web`         | Web Platform |
| `/modules/poi`                             | `/web-static/poi-admin`             | Forecast     |
| `/modules/promotions`                      | `/web-static/promotions-admin`      | Subs         |
| `/modules/reports`                         | `/services/reports-admin-web`       | Forecast     |
| `/modules/search`                          | `/services/search-admin-web`        | Web Platform |
| `/modules/sessions`                        | `/services/sessions-admin-web`      | Realtime     |
| `/modules/spots`                           | `/services/spots-admin-web`         | Forecast     |
| `/modules/users`                           | `/services/users-admin-web`         | Subs         |

#### `surfline-segment`

| Previous Location (`surfline-segment`) | New Location (`wavetrak-services`) | Owner     |
| :--------                              | :--------                          | :----     |
| `/`                                    | `/analytics/surfline-segment`      | Analytics |

#### `surfline-sisense-models`

| Previous Location (`surfline-sisense-models`) | New Location (`wavetrak-services`) | Owner     |
| :--------                                     | :--------                          | :----     |
| `/`                                           | `/analytics/sisense-models`        | Analytics |

#### `surfline-web`

| Previous Location (`surfline-web`) | New Location (`wavetrak-services`) | Owner        |
| :--------                          | :--------                          | :----        |
| `/account`                         | `/services/account-web`            | Subs         |
| `/account/cancel-next`             | `/web-static/cancel`               | Subs         |
| `/account/gifts-next`              | `/web-static/gifts`                | Subs         |
| `/account/offers-next`             | `/web-static/offers`               | Subs         |
| `/account/promotions-next`         | `/web-static/promotions`           | Subs         |
| `/account/registration-next`       | `/web-static/registration`         | Subs         |
| `/account/upgrade-next`            | `/web-static/upgrade`              | Subs         |
| `/backplane`                       | `/services/backplane-web`          | Web Platform |
| `/charts`                          | `/services/charts-web`             | Forecast     |
| `/common`                          | `/packages/common-web`             | Web Platform |
| `/docs-compatible`                 | `/web-static/api-docs-compatible`  | Web Platform |
| `/embed/camera`                    | `/packages/camera-embed-admin`     | Realtime     |
| `/homepage`                        | `/services/homepage-web`           | Content      |
| `/kbyg`                            | `/services/kbyg-web`               | Forecast     |
| `/load-test-frontends`             | `/tests/frontend-load`             | Web Platform |
| `/onboarding`                      | `/services/onboarding-web`         | Subs         |
| `/packages`                        | `/packages/scripts-web`            | Web Platform |
| `/perks`                           | `/web-static/perks`                | Subs         |
| `/sitemaps`                        | `/functions/sitemaps-generator`    | Web Platform |
| `/travel`                          | `/services/travel-web`             | Content      |
| `/web-proxy`                       | `/services/proxy-web`              | Infra        |

#### `surfline-web-editorial`

| Previous Location (`surfline-web-editorial`) | New Location (`wavetrak-services`)              | Owner   |
| :--------                                    | :--------                                       | :----   |
| `/functions/feed-etl-wp-to-es`               | `/functions/editorial-feed-etl-wp-to-es`        | Content |
| `/functions/travel-page-etl-wp-to-es`        | `/functions/editorial-travel-etl-wp-to-es`      | Content |
| `/functions/update-varnish-cache`            | `/functions/editorial-update-varnish-cache`     | Content |
| `/varnish`                                   | `/services/editorial-varnish-web`               | Content |
| `/wordpress`                                 | `/services/editorial-wordpress-web`             | Content |

#### `wavetrak-infrastructure`

| Previous Location (`wavetrak-infrastructure`) | New Location (`wavetrak-services`) | Owner |
| :--------                                     | :--------                          | :---- |
| `/chef`                                       | `/infrastructure/chef`             | Infra |
| `/cloudformation`                             | `/infrastructure/cloudformation`   | Infra |
| `/docker`                                     | `/infrastructure/docker`           | Infra |
| `/packer`                                     | `/infrastructure/ami`              | Infra |
| `/scripts`                                    | `/infrastructure/scripts`          | Infra |
| `/stacker`                                    | `/infrastructure/ami`              | Infra |
| `/terraform`                                  | `/infrastructure/terraform`        | Infra |

#### `wavetrak-services`

| Previous Location (`wavetrak-services`)                   | New Location (`wavetrak-services`)         | Owner        |
| :--------                                                 | :--------                                  | :----        |
| `/cameras-service/functions/embed-generator`              | `/functions/cameras-embed-generator`       | Realtime     |
| `/cameras-service/functions/rewind-clip-generator`        | `/functions/cameras-rewind-clip-generator` | Realtime     |
| `/common/@types`                                          | `/packages/@types`                         | Web Platform |
| `/common/airflow-helpers`                                 | `/packages/airflow-helpers`                | Forecast     |
| `/common/babel-services`                                  | `/packages/babel-services`                 | Web Platform |
| `/common/babel-web`                                       | `/packages/babel-web`                      | Web Platform |
| `/common/cam-stills-helper`                               | `/packages/cam-stills-helper`              | Realtime     |
| `/common/eslint-services`                                 | `/packages/eslint-services`                | Web Platform |
| `/common/eslint-web`                                      | `/packages/eslint-web`                     | Web Platform |
| `/common/fit-file-parser`                                 | `/packages/fit-file-parser`                | Forecast     |
| `/common/grib-helpers`                                    | `/packages/grib-helpers`                   | Forecast     |
| `/common/job-helpers`                                     | `/packages/job-helpers`                    | Forecast     |
| `/common/prettier-config`                                 | `/packages/prettier-config`                | Web Platform |
| `/common/science-algorithms`                              | `/packages/science-algorithms`             | Forecast     |
| `/common/services-common`                                 | `/packages/services-common`                | Web Platform |
| `/common/smdb`                                            | `/packages/smdb`                           | Forecast     |
| `/common/tap-apple-app-store-connect`                     | `/packages/tap-apple-app-store-connect`    | Analytics    |
| `/common/tap-google-play-console`                         | `/packages/tap-google-play-console`        | Analytics    |
| `/common/tsconfig`                                        | `/packages/tsconfig`                       | Web Platform |
| `/common/video-helpers`                                   | `/packages/video-helpers`                  | Realtime     |
| `/notifications-service/functions/notification-processor` | `/functions/notification-processor`        | Web Platform |
| `/services/admin-cameras-service/infra`                   | `/services/cameras-admin-web/infra`        | Realtime     |
| `/services/admin-forecasts-service/infra`                 | `/services/forecasts-admin-web/infra`      | Forecast     |
| `/services/admin-login-service/infra`                     | `/services/login-admin-web/infra`          | Subs         |
| `/services/admin-reports-service/infra`                   | `/services/reports-admin-web/infra`        | Forecast     |
| `/services/admin-search-service/infra`                    | `/services/search-admin-web/infra`         | Web Platform |
| `/services/admin-spots-service/infra`                     | `/services/spots-admin-web/infra`          | Forecast     |
| `/services/admin-users-service/infra`                     | `/services/users-admin-web/infra`          | Subs         |
| `/services/auth-service`                                  | `/services/auth-api`                       | Subs         |
| `/services/breaking-wave-height-api`                      | `/services/breaking-wave-height-api`       | Forecast     |
| `/services/buoyweather-api`                               | `/services/buoyweather-api`                | MSW/BW       |
| `/services/cameras-service`                               | `/services/cameras-api`                    | Realtime     |
| `/services/charts-service`                                | `/services/charts-api`                     | Forecast     |
| `/services/crowd-counting-service`                        | `/services/crowd-counting-api`             | Realtime     |
| `/services/editorial-varnish`                             | `/services/editorial-varnish-web`          | Content      |
| `/services/editorial-wp-admin`                            | `/services/editorial-wordpress-admin-web`  | Content      |
| `/services/editorial-wp`                                  | `/services/editorial-wordpress-web`        | Content      |
| `/services/entitlements-service`                          | `/services/entitlements-api`               | Subs         |
| `/services/favorites-service`                             | `/services/favorites-api`                  | Forecast     |
| `/services/feed-service`                                  | `/services/feed-api`                       | Content      |
| `/services/forecasts-api`                                 | `/services/forecasts-api`                  | Forecast     |
| `/services/geo-target-service`                            | `/services/geo-target-api`                 | Web Platform |
| `/services/graphql-gateway`                               | `/services/graphql-gateway-api`            | Web Platform |
| `/services/image-resizing-service`                        | `/services/image-resizing-api`             | MSW/BW       |
| `/services/kbyg-product-api`                              | `/services/kbyg-product-api`               | Forecast     |
| `/services/location-view-service`                         | `/services/location-view-api`              | Forecast     |
| `/services/metering-service`                              | `/services/metering-api`                   | Subs         |
| `/services/msw-tide-service`                              | `/services/tide-api`                       | MSW/BW       |
| `/services/notifications-service`                         | `/services/notifications-api`              | Web Platform |
| `/services/onboarding-service`                            | `/services/onboarding-api`                 | Subs         |
| `/services/password-reset-service`                        | `/services/password-reset-api`             | Subs         |
| `/services/promotions-service`                            | `/services/promotions-api`                 | Subs         |
| `/services/reports-api`                                   | `/services/reports-api`                    | Forecast     |
| `/services/science-data-service`                          | `/services/science-data-api`               | Forecast     |
| `/services/science-models-db`                             | `/services/science-models-db-api`          | Forecast     |
| `/services/search-service`                                | `/services/search-api`                     | Web Platform |
| `/services/services-proxy`                                | `/services/proxy-api`                      | Infra        |
| `/services/session-analyzer`                              | `/services/session-analyzer-api`           | Realtime     |
| `/services/sessions-api`                                  | `/services/sessions-api`                   | Realtime     |
| `/services/spot-recommender-svc`                          | `/services/spot-recommender-api`           | Forecast     |
| `/services/spots-api`                                     | `/services/spots-api`                      | Forecast     |
| `/services/subscription-service-backend`                  | `/services/subscription-backend-api`       | Subs         |
| `/services/subscription-service`                          | `/services/subscription-api`               | Subs         |
| `/services/user-service`                                  | `/services/user-api`                       | Subs         |
| `/services/wave-api`                                      | `/services/wave-api`                       | Forecast     |
| `/services/weather-api`                                   | `/services/weather-api`                    | Forecast     |
| `/sessions-api/functions/garmin-parser`                   | `/functions/sessions-garmin-parser`        | Realtime     |
| `/sessions-api/functions/session-enricher`                | `/functions/sessions-session-enricher`     | Realtime     |
| `/tools/cam_thumbnail_generator`                          | `/functions/camera-thumbnail-generator`    | Realtime     |
| `/tools/spot-etl-admin-to-es`                             | `/functions/admin-spots-etl-to-es`         | Web Platform |
| `/tools/system-load-tests`                                | `/tests/load/system-soak`                  | Forecast     |

## How Does This Solve the Problem?

Applications will live in intuitive locations in a single repository. It will be easier for engineers to onboard. Applications will use shared CI/CD pipelines for automatic and observable builds. Incident resolution will have less friction. Team velocity and engineer happiness will increase.

## What Are the Alternatives?

### Leave Things As-Is

If we leave things as-is, we'll continue to exist in the friction-filled, painful state that we're in now. CI/CD automation efforts will be hampered. Team velocity will continue to be decreased.

### Break Applications Into Single-Application Repos

While this is a viable strategy when initially designing a complex system, there would be little benefit to migrate our existing system to single-application repos. CI/CD pipelines would see little reuse and it would still be difficult to find application code.

## Other

### Resources

- [Awesome Monorepo](https://awesomeopensource.com/project/korfuri/awesome-monorepo)
