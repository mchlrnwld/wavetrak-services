# End-to-end and Visual Difference Testing

## What are we doing?

We are adding end-to-end and visual difference testing to our CI/CD pipeline.

## What problem are we solving?

We need tools to be able to deploy code with higher confidence. End-to-end and visual difference tests will help increase confidence in releasing code and should lower our overall bugs that make it into production.

With visual difference testing in place, we can confidently make larger changes to our design system and CSS.

## What impact does this have on the business?

- Creating a more reliable product is crucial to maintaining our users. Creating and executing a good test plan before and after deployments will create a more stable product. This will create less churn for active users and free users will be more likely to subscribe if the product works as intended.
- End-to-end and visual difference testing allows us to test the customer experience pre and post-deployments, ensuring the product is reliable and functioning as intended.
- Visual diff testing is a cost-effective method of testing. We are able to snapshot a page or component grouping and have a service compare that against prior snapshots without having to write significant amounts of assertions based on brittle HTML elements.

## What is the solution?

### e2e Testing Framework

For our e2e testing framework, we have used [Selenium](https://www.selenium.dev) in the past. This has a lot of overhead and is too far removed from our application code. Moving forward I recommend Cypress. The POC demo provided below uses [Cypress](https://www.cypress.io) with [Applitools](https://applitools.com) for visual diff testing.

Cypress on their website promotes “Fast, easy and reliable testing for anything that runs in a browser.”. Cypress allows us to easily write tests alongside our application code with the same language and tools our engineers are using to author our applications. If you’re still asking yourself, “why Cypress”, give this a read [Why Cypress?](https://docs.cypress.io/guides/overview/why-cypress).

One thing to note, unit testing is also possible in Cypress. They are not actively trying to solve unit testing, but it is available. The benefit being the unit tests do run in an actual browser [General Questions](https://docs.cypress.io/faq/questions/general-questions-faq#So-what-benefits-would-one-get-for-converting-one-s-unit-tests-from-Karma-or-Jest-to-Cypress).

If we are looking for a way to generate tests based on recorded user interactions, [Cypress Studio](https://docs.cypress.io/guides/core-concepts/cypress-studio) was recently released for this purpose.

### Testing Structure

- `/tests/e2e/` - Integration tests across multiple services
  - Executes on commit to any branch with a pull request
  - Blocks merging to master on failure
  - Runs local instance of service in the workflow container and connects to Staging as an upstream (Sandbox is too unstable)
- `/services/{service-name}/tests/integration/` - Integration tests for UI artifacts specific to the service
  - Executes on commit to any branch with a pull request
  - Blocks merging to master on failure
  - Testing UI components in isolation, ranging from simple components to complex, full pages with mock data and stores
  - Tests will run within Stories configured with [Storybook](https://storybook.js.org/docs/react/workflows/interaction-testing) and mock data, ensuring visual diff testing remains stable
- `/services/{service-name}/tests/integration/{tags:@smoke}` - Smoke tests specific to the service
  - Executes post-deployment to any given environment
  - Test failures should trigger a rollback of the service
  - Tests capture basic functionality of the whole-page
  - Basic assertions (sanity checks) to ensure deployment was successful

### Smoke Tests

Smoke testing is done to reveal simple failures severe enough to block a release to our web applications. Smoke testing will be done in our default browser. The default browser should be selected based on our most used browser by our end users.

We can use [cypress-grep](https://github.com/cypress-io/cypress-grep), an official plugin from Cypress to tag our tests with `@smoke` and target them later in our Workflow configuration based on the environment being deployed.

Example of test tagged as a `@smoke` test:

```js
it("Should render breadcrumbs", { tags: "@smoke" }, () => {
  cy.get(".sl-breadcrumbs li").should("have.length", 4);
});
```

Smoke tests like the one above can be targeted during Cypress runs with the following flag:

```zsh
cypress run --env grepTags=@smoke
```

The `tags` prop in the config object passed to the test can also be an array of other [custom tags](https://github.com/cypress-io/cypress-grep#filter-with-tags) and targeted during Cypress runs.

Using tagging over directory structure to differentiate tests is more flexible when configuring what tests to execute.

### Visual Difference Testing

I recommend we decide on a visual diff testing framework before teams really start to integrated e2e testing because this allows us to avoid writing a bunch of assertions in Cypress to determine if elements exist on a page. We can rely on our visual diff testing framework for these assertions.

Comparison table of [Applitools](https://applitools.com) and [Percy](https://percy.io) (Browserstack)

**_provided by Applitools per my request_**

|                             | Applitools                                                                                               | Percy                                                                                         |
| --------------------------- | -------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- |
| Visual Testing              | Yes                                                                                                      | No                                                                                            |
| Visual Testing              | Visual AI w/ multiple match levels & match levels by regions, full-page match capabilities, DOM analysis | Pixel-to-Pixel w/ basic capabilities to ignore elements                                       |
| AI-powered auto-maintenance | Yes                                                                                                      | No                                                                                            |
| Root Cause Analysis         | Yes                                                                                                      | No                                                                                            |
| Mobile Testing              | Yes                                                                                                      | No                                                                                            |
| Network Requirements        | No Inbound connections to network required                                                               | Requires inbound connection to network via reverse proxy or VPN                               |
| ISO 27001 Certified         | Yes https://applitools.com/gdpr/                                                                         | No indication of certification                                                                |
| Deployment Options          | On-Premise, Dedicated Cloud or Public Cloud                                                              | Public Cloud, Multi-Tenant _multi-tenancy does not meet most government security requirement_ |
| Integrations                | 60+                                                                                                      | 25                                                                                            |
| Cross Browser Support       | Safari, Chrome, FF, Edge, IE 10/11, any combination of viewports.                                        | Chrome, FF                                                                                    |
| Accessibility               | Visual AI-enabled Contrast Testing                                                                       | No                                                                                            |

Applitools-Percy-Screener-QuickCompare-Feb2021.pdf

Applitools features that we do not get with Percy but would highly benefit from

- Ability to [easily mark regions](https://applitools.com/blog/visual-validation-dynamic-data/) as dynamic vs using [CSS in Percy](https://docs.percy.io/docs/percy-specific-css) to hide elements
- Visual Testing is AI-based, not pixel-like other, and is 99.99% accurate (as told by their account manager)
- Root cause analysis can actually try to reference in code where the issue is
- Has mobile testing for actual mobile browsers (likely coming to Percy)
  - Renders with Safari on iOS native
  - emulates Android within chrome
- No inbound network connections required (takes stamp of dom and styles and uses that across devices)
- Much larger integration support as we look at iOS and Android app testing
- AI-based accessibility testing

#### Moving forward with Percy over Applitools

In the end, we have decided to start with Percy as our tool of choice for Visual Difference testing due to the large difference in pricing. Percy offers enough features for us to utilize the benefits of Visual Difference testing with a much lower financial cost.

We can utilize [Percy specific CSS](https://docs.percy.io/docs/percy-specific-css) to address dynamic content and other areas of struggle for Percy when performing diffs. Percy also offers comparable testing for Storybook.

#### When to run visual difference to testing

I feel there are 2 important instances when we want to run visual tests.

1. When we are working on a feature and want to be confident before merging to master and deploying to production
2. When code is merged to production we want to be confident that deployment did not break any of the web applications

Currently, we do not have preview environments before merging to master. We use sandbox and staging to test deployments. I recommend we stand up an instance of the web app within the container in the GitHub workflow triggered by commits to a PR (feature branch). This local instance of the web app in the GitHub workflow container will use Sandbox upstreams for the service. To do this, we need to expose the Backplane service to the GitHub workflow container [IP Addresses](https://api.github.com/meta). To run these tests before going to master, we can trigger this [GitHub workflow on push](https://docs.github.com/en/actions/reference/events-that-trigger-workflows#push).

Triggering tests on post-deployment before migration to the monorepo and action workflows will have to be done with the `workflow_dispatch` documented below in [GitHub Workflow](#github-workflow).

### Costs

#### Cypress

Cypress lists their [pricing online](https://www.cypress.io/pricing/) for up to their business level users. Beyond that, we have to contact them for enterprise pricing.

**Free pricing - FREE!**

- 3 users
- 500 Test Results
- Parallelization
- Load Balancing
- Debugging
- Analytics
- Integrations
- Community Support

**Team pricing - $75 per month**

- 10 users
- 10,000 Test Results
- Everything in Free plus the following
  - Flake detection
  - Jira integration
  - Email support

**Business Pricing - $300 per month**

- 40 users
- 10,000 Test Results
- Everything in Team plus
  - Smart Orchestration (run failed tests first)
  - Flake Management
  - Github Enterprise
  - SSO

**Enterprise - price unknown**

#### Applitools

Applitools is a much more comprehensive tool with more robust features but costs roughly 10 times as much as Percy. Applitools is a far superior tool when you compare features. Applitools does not list [pricing online](https://applitools.com/pricing/), but I did speak with an account manager there.

At roughly the following estimate, I was given the below estimates:

- 100 captures across our various apps being tested
- 10 different device configurations (meaning each screen will be captured with each of these configurations)
- 5 deployments a day x 5 days a week (roughly 100 deployments a month)

##### Pricing options

- 100 Pages (Roughly 100,000 captures a month) @ $62,500/year
- 175 Pages (Roughly 250,000 captures a month) @ $131,250/year

#### Percy

Percy is now owned by Browserstack. The pricing model is on [Browserstack’s site](https://www.browserstack.com/pricing?product=percy&ref=percy).

##### Pricing options

- 100,000 captures a month @ $399
- 200,000 captures a month @ $799
- 500,000 captures a month @ $1999

### Demo

Open PR implementing e2e and visual diff testing in KBYG:

[surfline-web: WP-85: Setup e2e and visual diff testing](https://github.com/Surfline/surfline-web/pull/5858)

Completed action with successful smoke tests:
https://github.com/Surfline/surfline-web/runs/3074447564

Check out the latest from the pr above, install and run

```zsh
cd ~/Projects/surfline-web/kbyg
git checkout WP-85_e2e_testing_poc
npm i
##### configure cypress env vars
##### example scripts to run in project
##### launch cypress in dev mode with hot reloading of tests
npm run cy:open:sandbox
##### run all cypress tests in cli against staging
npm run cy:run:staging
##### run cypress smoke tests in cli against prod
npm run cy:run:prod:smoke
```

### GitHub Workflow

There is a [GitHub workflow configured](https://github.com/Surfline/surfline-web/pull/5858/files#diff-930a5bd3c4c8d4e0d20f94969de276564f2b705f25ba83a6a143ceeff4aed8d9) for running the Cypress tests. This workflow can be triggered on a [variety of events](https://docs.github.com/en/actions/reference/events-that-trigger-workflows#webhook-events). In regards to our current integration with Jenkins, we can call the [Workflow API](https://docs.github.com/en/rest/reference/actions#create-a-workflow-dispatch-event) from GitHub to trigger the Cypress workflow.

Example:

```zsh
curl \
    -X POST \
    -H "Authorization: token <GITHUB_ACCESS_TOKEN>" \
    -H "Accept: application/vnd.github.v3+json" \
    https://api.github.com/repos/Surfline/surfline-web/actions/workflows/testing-e2e.yml/dispatches \
    -d '{"ref":"WP-85_e2e_testing_poc", "inputs":{"environment":"sandbox","path":"kbyg","commit":"xxxx"}}'
```

### Failed Builds

If e2e or visual diff tests fail when committing to a feature branch, that branch should not be allowed to be merged until tests are fixed or any visual differences are approved. Once tests pass, the branch is then merged to master and deployed. Smoke tests should now be run. If these smoke tests fail, the build should be reverted.

In our current configuration, we will need to use `workflow_dispatch` with Jenkins to trigger testing after deployments (the easy part). We would also need to integrate a [job check](https://docs.github.com/en/actions/reference/context-and-expression-syntax-for-github-actions#job-status-check-functions) into the workflow running the tests that report back to Jenkins on failure, triggering a rollback of the build (the harder part). When we are using workflows in the monorepo, this can be integrated into the deployment workflow code itself (much easier). I feel it is best to focus our efforts on auto rollback in the context of GitHub workflows (monorepo). We could integrate the `workflow_dispatch` with Jenkins on deploy and let the tests run. If there is a failure we could manually rollback for the time being.

### Testing in the monorepo

When we are in the context of the mono repo we could pursue one of the following 2 options for testing branches before they merge to master:

1. Test the web app within the GitHub workflow container
   1. build and start a local instance of Backplane from `master`, this would eventually be a container app with module federation in Next.js/Webpack 5
   2. build and start web app locally with local reference to Backplane service
   3. trigger tests against local container instance of application against staging data/services
1. Test the web app in a hosted preview environment, I think infrastructure is working on this.

I recommend against mocking out an instance of Backplane if we’re not using hosted preview environments. If we stand up Backplane from master locally in the container, we avoid access issues (that I know of), we are testing against the latest version of Backplane and the web app can be successfully started and tested locally in the container.

### Resources

- [Visual Testing | Cypress Documentation](https://docs.cypress.io/guides/tooling/visual-testing)
- [Applitools w/ Cypress Tutorial](https://applitools.com/tutorials/cypress.html)
- [Percy w/ Cypress](https://docs.percy.io/docs/cypress)
- [Unit testing example w/ Cypress](https://github.com/cypress-io/cypress-example-recipes/tree/master/examples/unit-testing__application-code)
- [Visual Testing for Mobile Apps](https://applitools.com/blog/visual-testing-for-mobile-apps/)
- [Tutorials - Native SDKs listed as well](https://applitools.com/tutorials/)
- [Visual Validation with Dynamic Data | Applitools](https://applitools.com/blog/visual-validation-dynamic-data/)
- [API Testing with Cypress](https://www.mariedrake.com/post/api-testing-with-cypress)

## How does this solve the problem?

These tests can be run automatically, whether it be during local development, on a deployment to ECS or we want to stand up a server in a container within the GitHub workflow.

## What are the alternatives?

### e2e Testing Frameworks

- [Cross Browser Testing Tool - Sauce Labs](https://saucelabs.com/platform/cross-browser-testing)
- [SeleniumHQ Browser Automation](https://www.selenium.dev)
- [Nightwatch.js](https://nightwatchjs.org)
- [WebdriverIO](https://webdriver.io)
- [Puppeteer | Automation within Google Chrome](https://developers.google.com/web/tools/puppeteer)
- [TestСafe](https://testcafe.io)
- [Walrus.ai](https://walrus.ai)
- [Virtuoso QA](https://www.virtuoso.qa)
- [Endtest](https://endtest.io)
- [CodeceptJS](https://codecept.io)

### Visual Difference Testing Frameworks

- [Happo w/ Cypress](https://docs.happo.io/docs/cypress)
- [meinaart/cypress-plugin-snapshots](https://github.com/meinaart/cypress-plugin-snapshots)
- [Functionize](https://www.functionize.com/visual-testing)
- [Diffy](https://diffy.website)

## What do we know already?

Selenium is not working for us and we need e2e testing to deploy with confidence.

## Other

- Use non-personal token values for `GITHUB_ACCESS_TOKEN` and `NODE_AUTH_TOKEN`
- Possible future implementation of [code coverage w/ Cypress](https://docs.cypress.io/guides/tooling/code-coverage#Introduction)
- We should consider starting out by writing all of our [tests in TypeScript](https://docs.cypress.io/guides/tooling/typescript-support) to start. Cypress ships with official types.
