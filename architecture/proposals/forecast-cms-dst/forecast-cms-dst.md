# Forecast CMS DST Refactor

- [Forecast CMS DST Refactor](#forecast-cms-dst-refactor)
  - [What are we doing?](#what-are-we-doing)
  - [What problem are we solving?](#what-problem-are-we-solving)
    - [Current Forecast Date Handling](#current-forecast-date-handling)
      - [Overview](#overview)
      - [Detailed Flow From Forecasts CMS to MongoDB](#detailed-flow-from-forecasts-cms-to-mongodb)
      - [Detailed Product View Flow](#detailed-product-view-flow)
  - [What impact does this have on the business?](#what-impact-does-this-have-on-the-business)
  - [What is the solution?](#what-is-the-solution)
  - [Solution Overview - ISO Date Strings](#solution-overview---iso-date-strings)
    - [TLDR](#tldr)
    - [Solution Overview](#solution-overview)
      - [Detailed Flow From Forecasts CMS to MongoDB](#detailed-flow-from-forecasts-cms-to-mongodb-1)
      - [Detailed Product View Flow (KBYG Web)](#detailed-product-view-flow-kbyg-web)
    - [Pros](#pros)
    - [Cons](#cons)
  - [How does this solve the problem?](#how-does-this-solve-the-problem)
  - [What do we know already?](#what-do-we-know-already)
  - [Open Questions](#open-questions)
  - [Alternate Solution - UTC Forecast Days with Timestep Offsets](#alternate-solution---utc-forecast-days-with-timestep-offsets)
    - [TLDR](#tldr-1)
    - [Alternate Solution Overview](#alternate-solution-overview)
      - [Detailed Flow From Forecasts CMS to MongoDB](#detailed-flow-from-forecasts-cms-to-mongodb-2)
      - [Detailed Product View Flow](#detailed-product-view-flow-1)
    - [Pros](#pros-1)
    - [Cons](#cons-1)
    - [How does this solve the problem?](#how-does-this-solve-the-problem-1)
    - [What do we know already?](#what-do-we-know-already-1)
  - [Resources](#resources)
  - [Date Formatting in JavaScript Code Examples](#date-formatting-in-javascript-code-examples)
    - [ISO Date to Unix Timestamp](#iso-date-to-unix-timestamp)
      - [Convert JSON Date String to Timestamp in Seconds using `date-fns`](#convert-json-date-string-to-timestamp-in-seconds-using-date-fns)
      - [Convert ISO Date String to Timestamp in Seconds using `date-fns`](#convert-iso-date-string-to-timestamp-in-seconds-using-date-fns)
    - [Unix Timestamp in Seconds to ISO Date](#unix-timestamp-in-seconds-to-iso-date)
      - [Convert Timestamp in Seconds to Date](#convert-timestamp-in-seconds-to-date)
      - [Convert Timestamp in Seconds to ISO Date String](#convert-timestamp-in-seconds-to-iso-date-string)
    - [Parsing ISO Calendar Date String vs Date String with Time](#parsing-iso-calendar-date-string-vs-date-string-with-time)
      - [Parsing ISO Calendar Date](#parsing-iso-calendar-date)
      - [Parsing ISO Date with Time](#parsing-iso-date-with-time)
    - [Parsing Calendar Date String vs Date String with `date-fns-tz/utcToZonedTime`](#parsing-calendar-date-string-vs-date-string-with-date-fns-tzutctozonedtime)
      - [Parsing ISO Calendar Date](#parsing-iso-calendar-date-1)
      - [Parsing ISO Date with Time](#parsing-iso-date-with-time-1)
    - [Formatting Calendar Date String vs Date String with `date-fns-tz/utcToZonedTime` and `date-fns-tz/format`](#formatting-calendar-date-string-vs-date-string-with-date-fns-tzutctozonedtime-and-date-fns-tzformat)
      - [Formatting ISO Calendar Date](#formatting-iso-calendar-date)
      - [Formatting ISO Date with Time](#formatting-iso-date-with-time)
    - [Adding Days to Calendar Dates vs Date String with `date-fns/addDays` and `date-fns-tz`](#adding-days-to-calendar-dates-vs-date-string-with-date-fnsadddays-and-date-fns-tz)
      - [Adding Days and Parsing ISO Calendar Date](#adding-days-and-parsing-iso-calendar-date)
      - [Adding Days and Parsing ISO Date with Time](#adding-days-and-parsing-iso-date-with-time)
    - [Midnight of Date](#midnight-of-date)
      - [Using Moment](#using-moment)
      - [Timestamp in Seconds with Subregion and User Offsets](#timestamp-in-seconds-with-subregion-and-user-offsets)

## What are we doing?

Ensuring accurate forecast dates in the admin tools and products across DST changes and cleaning up related tech debt.

## What problem are we solving?

Preventing incorrect forecast dates in both the Forecast CMS and user-facing product pages related to complex date formatting and DST changes.

Related recurring bugs include:

- Forecast dates are incorrectly displayed when the forecasted range includes a DST change. For example, if a DST change is set to occur on 11/3 in the subregion, a forecast range beginning on 11/2 will display the incorrect forecast for all days after the DST change.
- "Next forecast" dates are incorrectly displayed when the next forecast day/time has a different UTC offset than the day in which the "next forecast" is set in the CMS. This is also caused by a DST change occuring between the CMS action and the future date.
- "Next forecast" dates are displayed one day ahead of the selected value when the admin's location is in Australia. For example, if the forecaster's local location is in Australia, setting the next forecast in the CMS to Thurs 6/24 will display Fri 6/25 on the product (and in the CMS after page refresh). This is not related to DST, but to date formatting in the CMS when to admin's location is in Australia.

In addition to the above mentioned bugs, related tech debt within the forecast flow includes:

- Removing Forecast MongoDB models from the KBYG Product API. The Product API should make external calls to the Forecasts API to obtain forecast data.
  - Removing these models from the Product API will prevent the need to address the bugs and code updates in multiple places. With the bug fixes applied to the Forecasts API, we can proxy data from the Product API to the Forecasts API and allow the correct data to naturally flow downstream
- Updating date formatting utils in the Forecasts CMS and Forecasts API to match those used in the Product API and KBYG Web front-ends
  - Both the CMS and product display the forecast data as local to the subregion location (not local to the user/forecaster), and should be using the same logic, package versions, and utils to arrive at those dates

### Current Forecast Date Handling

#### Overview

- Both the Forecasts CMS and Forecast product pages display forecast times local to the subregion, not the user
- Dates for both Forecast Days and the Next Forecast date are stored in MongoDB as UTC Dates (Forecast Days as midnight UTC; Next Forecast includes the time/hour)
- The Forecasts API and KBYG Product API each go direct to MongoDB to obtain subregion forecast UTC Dates, and convert these dates to timestamps in their responses
- Future dates are determined by converting the date to a timestamp, adding the number of seconds in one day (or the number of days to increment), plus the subregion offset in seconds (as well as the user's local offset in seconds if on front-end)
- The services and apps in the Forecast data flow use different utility functions to format and transform dates
- The services and apps in the Forecast data flow do not consider the UTC offset of future forecast dates, but instead use the current date's UTC offset

#### Detailed Flow From Forecasts CMS to MongoDB

1. Forecaster visits Forecasts CMS
   1. Subregion forecast form contains fields for summary info (i.e. "next forecast" day and time) and `8` forecast days blocks (i.e. conditions for each forecast day)
2. The Forecasts CMS makes a `GET` request to the Forecasts API `/admin/forecasts` to obtain any existing forecast data in MongoDB
   1. Up to `8` forecast days returned, each with a `forecastDate` unix timestamp in seconds
      1. Subregion's primary spot timezone string is used to get current local date for subregion
      2. A date range `start` and `end` is calculated from this current local date, and the MongoDB `SubregionForecastDays` collection is queried for all documents with a `forecastDate` within the specified range.
         1. `forecastDate` is stored in Mongo as UTC Date, and is converted to unix timestamp in seconds in API handler
   2. `nextForecastTimestamp` returned as unix timestamp in seconds
      1. API Handler goes direct to MongoDB `SubregionForecastSummaries`
         where `nextForecastTimestamp` is stored as UTC date
      2. Handler converts to unix timestamp
   3. Only a single `utcOffset` is returned from the API, relative to the current date
3. The Forecasts CMS stores `days[].forecastDate` and `nextForecastTimestamp` in application state
   1. Days with default values, including `forecastDate`, are created for any days that have not yet been forecasted
      1. If less than `8` forecast days are returned from API, the handler creates default values for the additional days
      2. Default dates are determined by converting the current day's `forecastDate` to a unix timestamp and adding a day in seconds to each additional day
4. The Forecasts CMS populates initial form values with data in application state
   1. `forecastDate` timestamp is displayed above each day in the form
      1. Dates are displayed local to the subregion (not forecaster)
      2. Dates are displayed in format `DAY 1 THU 06.24`
         1. Timestamps are manually formatted with native Date utils (no `moment` or `date-fns` used)
         2. Uses `new Date(forecastDate)` to convert timestamp to date
         3. Month and day are set with `getUTCMonth`, `getUTCDate`, and a manual list of day options
         4. No `utcOffset` specific to the future date is considered
   2. Next Forecast block displays two form fields to control the `nextForecastTimestamp`
      1. `DAY` form field to select the date (i.e. `Friday 06/25`)
         1. Initial value for this field uses `nextForecastTimestamp` returned from Forecasts API
            1. Timestamp is converted to midnight local to the subregion, using the current day's `utcOffset` (not the next forecast day)
         2. Other options in select form converted using `getLocalDate` util, using the current day's `utcOffset` (not the next forecast day)
         3. `date-fns/format` used to format date into readable format
      2. `TIME` form field to select the time (i.e. `6am`)
         1. This is a hardcoded list of hours, no date formatting used
5. On publish action, the Forecasts CMS formats dates
   1. `forecastDate` into string date `MM/DD/YYYY`
      1. Timestamp is manually formatted (no `moment` or `date-fns` library)
      2. Code ex: `${forecastDate.getUTCMonth() + 1}/${forecastDate.getUTCDate()}/${forecastDate.getUTCFullYear()}`
   2. `nextForecastTimestamp` into UTC Date
      1. Complex logic convert day and hour from form to UTC
         1. Converts to midnight of date UTC
         2. Adjusts to apply hour of next forecast
         3. Converts timestamp to date object
      2. Uses current date's `utcOffset`, not that of the next forecast date
6. Forecasts CMS `POST` request to Forecasts API `/admin/forecasts`
   1. `forecastDate` saved in MongoDB `SubregionForecastDays` as ISO Date in UTC midnight of the forecast day (i.e. `2021-06-24T00:00:00.000Z`)
      1. Posted `forecastDate` used for form validation (to ensure no days skipped) but not as value inserted into MongoDB
      2. `createForecastHandler` creates `forecastDate` to be inserted into MongoDB `SubregionForecastDays` collection by finding midnight UTC of the current date and adding days i.e. `todayUtcTime.setDate(todayUtcTime.getDate() + index)`
      3. No `utcOffset` specific to the future date is considered
   2. `nextForecastTimeStamp` saved in MongoDB `SubregionForecastSummaries` as ISO Date (i.e. `2021-06-25T06:00:00.000Z`)
      1. Uses value from `POST` body, no date conversion in API

#### Detailed Product View Flow

1. User visits Subregion Forecast page
2. KBYG Web makes `GET` request to KBYG Product API `/regions/forecasts/conditions`
   1. `getConditionsHandler` uses subregion's primary spot timezone string to get current local date for subregion
   2. A date range `start` and `end` is calculated from this current date, and the MongoDB `SubregionForecastDays` collection is queried for all documents with a UTC `forecastDate` within the specified range
   3. All matching forecast days are returned, with the `forecastDate` for each day converted to a unix timestamp in the response
   4. Only a single `associated.utcOffset` is returned from the API, relative to the current date
3. KBYG Web makes `GET` request to `/regions/overview`
   1. `nextForecastTimestamp` returned as unix timestamp in seconds
      1. API Handler goes direc to MongoDB `SubregionForecastSummaries`
      2. `nextForecastTimestamp` stored as UTC date
      3. Handler converts to unix timestamp
4. KBYG Web displays the array forecast day conditions (but not the forecast date itself) as `ForecastGraphsDaySummaries` in the order returned from the Product API. No date from the API response is used on the front-end for forecast conditions in the subregion forecast graphs
5. KBYG Web generates `ForecastGraphsDays` on the client to display the forecast date above each forecast graph day in time local to subregion (not user). This is done without data from the API other than the current subregion `utcOffset`
   1. `getLocalDateForFutureDay` util generates a formatted date `iiii M/d` i.e. `Thurs 06/24` to display
      1. `getLocalDateForFutureDay` uses current date (not forecasted day date) and index of forecasted day generate day by adding days in seconds
      2. `getLocalDateForFutureDay` returns midnight of the date local to the subregion by adding the user's local offset and the subregion's `utcOffset`
      3. The subregion `utcOffset` used is always that of the current date, not the forecasted day
6. KBYG Web displays `nextForecastTimestamp` in time local to subregion
   1. Formatted as `Fri by 6am AEST`
   2. Uses `date-fns/format` to generate human readable date format
   3. The subregion `utcOffset` used is that of the current date, not the next forecast date

## What impact does this have on the business?

Ensuring accurate forecast dates in the CMS and on the product before daylight savings time changes will prevent time-consuming workarounds for the Forecast team and improve product reliability.

The [incident retrospective for March 14, 2021](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/1777664248/2021-03-14%2BDaylight%2BSavings%2BTime%2BDST%2BOutage%2BRetrospective) captures the various engineering hotfixes and manual forecast team updates required before and after the DST change to display the correct dates on product.

---

## What is the solution?

The key distinction between proposed vs alternative solutions is in regards to how Forecast Day dates are returned from the API:

- ISO date strings `YYYY-MM-DD` (proposed)
  - Calendar dates without time
  - No UTC offset provided, each client formats date without concern for timezone adjustments
- Unix timestamp (alternative)
  - Seconds from epoch (same as current contract)
  - Include UTC offset for each date

## Solution Overview - ISO Date Strings

### TLDR

- Forecasts API and KBYG Product API represent "Forecast Day" dates as date string without time
  - Date string is in ISO 8601 calendar date string format `YYYY-MM-DD`
    - Clients format date string without subregion or user UTC offset calculations
    - Date string returned from API as property `forecastDay`
    - Forecast `timestamp` and `utcOffset` (unix timestamp) temporarily maintained for backwards compatibility
  - Endpoints updated to use `forecastDay`:
    - Forecasts API
      - `GET` `/admin/forecasts`
      - `POST` `/admin/forecasts`
    - KBYG Product API
      - `GET` `/kbyg/regions/overview`
      - `GET` `/kbyg/regions/forecasts`
      - `GET` `/kbyg/regions/forecasts/conditions`
- "Next Forecast" still represented as unix timestamp in API response
  - `nextForecastUtcOffset` added to API to ensure correct offset for future date

### Solution Overview

- Continue storing `forecastDate` as UTC dates in MongoDB
  - `SubregionForecastDays` `forecastDate` as midnight of date UTC
  - `SubregionForecastSummaries` `nextForecastTimestamp` as UTC date with time
- Forecasts API updates:
  - `forecastDay` added to Forecasts API `GET` `/admin/forecasts` response as date string value of `forecastDate`
    - ISO 8601 format `YYYY-MM-DD` removes any reference to timezone or time of day
    - `forecastDay` date string not stored in MongoDB because existing `forecastDate` date format allows for easier MongoDB queries
      - `forecastDate` is returned in API as well (as unix timestamp) for backwards compatibility
      - MongoDB forces a timezone for date objects, so we continue to store as midnight of date UTC, but do not expose this timezone at the API level
  - Calculate and return `nextForecastTimezoneOffset` UTC offset for the "Next Forecast"
    - `nextForecastTimezoneOffset` as float in hours for the Next Forecast time
    - Use existing `createUTCOffsetCalculator` util to generate offsets, matching pattern implemented for science forecast data in the KBYG Product API
  - Add the full subregion `timezone` in the API response to enable client-side date calculation across DST changes
    - `timezone` has string value i.e. `"America/Los Angeles"`
      - This value is available on the subregion's primary spot
      - Used with `moment/tz` in the CMS when calculating future offset for selected "next forecast"
- Update Forecasts CMS date formatting:
  - Forecast Days:
    - CMS converts `forecastDay` `YYYY-MM-DD` date string to UTC date string `2021-07-10T00:00:00:000Z`
      - This appears to indicate UTC time, but actually represents midnight of the forecast day in the subregion
      - Default days created on the client for any dates within the range that have not yet been forecasted
        - Get current date local to the subregion
        - Create ISO date string at midnight UTC for the current day in the subregion
        - Use `date-fns-tz/utcToZonedTime`, `date-fns-tz/format` and `date-fns/addDays` to generate and format days
    - Date strings formatted in UTC using `date-fns-tz/utzZoneOffset` and `date-fns-tz/format` to ensure date string does not adjust for user or subregion timezone offsets, and is always matching the value in MongoDB
      - This format, including time, is required for accurate date parsing/formating on the client using `date-fns-tz` without adjusting user and subregion offsets
    - On submit, the date strings are easily converted to UTC dates without timezone conversion
      - Only forecast days with condition values are saved to MongoDB
  - Next Forecast:
    - Initial value of the form will use the `nextForecastUtcOffset` to ensure the correct offset is used, even if the next forecast day is across a DST change
    - Next Forecast day options in the select menu sets day options with simplified logic
      - Get current date local to the subregion
      - Create ISO date string at midnight UTC for the current day in the subregion
      - Use `date-fns-tz/utcToZonedTime`, `date-fns-tz/format` and `date-fns/addDays` to generate and format days
    - On submit, the `nextForecastTimestamp` is generated from the selected day/hour values, adding the subregion offset to the UTC dates from the form
      - Use newly added subregion `timezone` i.e. `"America/Los Angeles"` to calculate correct future offset with `date-fns-tz/getTimezoneOffset`
- Refactor KBYG Product API to remove Subregion Forecast MongoDB models
  - Proxy requests for forecast data to the Forecasts API for endpoints:
    - `GET` `/kbyg/regions/overview`
    - `GET` `/kbyg/regions/forecasts`
    - `GET` `/kbyg/regions/forecasts/conditions`
  - New fields from Forecasts API (described above) include:
    - `forecastDay` date strings
    - `nextForecastUtcOffset` float offset in hours
    - `timestamp` returned for backwards compatibility
  - Add `forecastDay` date string to all model days
    - KBYG Product API returns up to `16` days
    - All forecast days that do not yet have a human forecast must have `forecastDay` date string generated from timestamp
- Update KBYG web date formatting:
  - The `NextForecast` component will use the `nextForecastUtcOffset` when formatting the date, instead of the current date's subregion offset
  - The `ForecastGraphDays` component, which displays dates above both the spot and subregion forecast graphs, will continue to generate dates on the client, but with updated logic:
    - Get current date local to the subregion
    - Create ISO date string at midnight UTC for the current day in the subregion
    - Use `date-fns-tz/utcToZonedTime`, `date-fns-tz/format` and `date-fns/addDays` to generate and format days
- Native app updates:
  - Use the `nextForecastUtcOffset` when formatting the Next Forecast date, instead of the current date's subregion offset
  - Update forecast graph dates to format date from `forecastDay` date string instead of `timestamp` and `utcOffset`

#### Detailed Flow From Forecasts CMS to MongoDB

1. Forecast admin visits Forecasts CMS
2. Forecasts CMS fetches Forecast Days and Summary from the Forecasts API
3. The Forecasts API queries MongoDB:
   1. Range of `SubregionForecastDays` is queried in MongoDB by `forecastDate`:
      1. The `start` date for the queried range is today's date at in the subregion without time i.e. `2021-07-10 00:00:00Z`
      2. The `end` date for the queried range is the `start` plus `8` days
      3. `SubregionForecastDays` `forecastDate` field is stored as an ISO Date, and the existing MongoDB range query will not have to be changed as long as the correct range is queried
   2. `SubregionForecastSummary` is unchanged, querying by `subregionId` and finding the most recent document
4. The Forecasts API decoreates the response with additional properties:
   1. `forecastDay` ISO date string `YYYY-MM-DD`
   2. `timezone` string in format `"America/Los Angeles"`
      1. Provided by primary spot
      2. Used to determine correct offset for `nextForecastTimestamp` on the client
   3. `nextForecastUtcOffset` float UTC offset in hours
      1. Use `createUTCOffsetCalculator` util to generate on API
5. Forecasts CMS stores forecast data in application state
   1. `forecastDay` calendar date converted to UTC date string `2021-07-10T00:00:00:000Z`
      1. `date-fns-tz/utzZoneOffset` and `date-fns-tz/format` utils require UTC date string to to ensure parsed/formatted date remains in UTC (see code examples below)
      2. Default days created on the client for any dates within the range that have not yet been forecasted
         1. Get current date local to the subregion
         2. Create ISO date string at midnight UTC for the current day in the subregion
         3. Use `date-fns-tz/utcToZonedTime`, `date-fns-tz/format` and `date-fns/addDays` to generate and format days
6. Forecast CMS sets "next forecast" options
   1. `nextForecastUtcOffset` used to set default value
   2. Set select menu `day` options
      1. Get current date local to the subregion
      2. Create ISO date string at midnight UTC for the current day in the subregion
      3. Use `date-fns-tz/utcToZonedTime`, `date-fns-tz/format` and `date-fns/addDays` to generate and format days
   3. No changes to `hour` field, which uses hardcoded list of options
7. Forecasts CMS submits forecast dates in format expected by MongoDB
   1. `forecastDate` as UTC date
   2. `nextForecastTimestamp` as unix timestamp with time local to the subregion
      1. `convertNextForecastToTimestamp` util updated to use client-side version of `createUTCOffsetCalculator` to use correct utc offset
8. Forecasts API saves date in MongoDB
   1. `forecastDate` as ISO Date at midnight UTC for the forecast day, local to the subregion
      1. Form validation updated to compare dates instead of date strings
      2. `forecastDate` derived from posted date value instead of calculating on the API
   2. `nextForecastTimestamp` as timestamp
      1. `forecastDate` converted from timestamp to UTC date

#### Detailed Product View Flow (KBYG Web)

1. User visits KBYG Web Subregion Forecast
2. KBYG Web makes `GET` request to KBYG Product API `/regions/forecasts/conditions`
   1. KBYG Product API proxies to Forecasts API for data and returns array of forecast days data
   2. Product API adds `forecastDay` date string to all model days without human forecast
3. KBYG Web makes `GET` request to `/regions/overview`
   1. KBYG Product API proxies to Forecasts API for data
4. KBYG Web stores `nextForecastUtcOffset` and `nextForecastTimestamp` in state
   1. Next Forecast date formatting util on the client is updated to reference correct utc offset for the future date using `nextForecastUtcOffset`
5. KBYG Web stores Forecast Days data as array, in order returned from API
   1. No front-end changes to display forecasts conditions in the correct order is needed, as no date is displayed in KBYG web conditions component
   2. The KBYG web front-end `ForecastGraphDays` component is updated to create dates on the client taking into account future date offsets
      1. Get current date local to the subregion
      2. Create ISO date string at midnight UTC for the current day in the subregion
      3. Use `date-fns-tz/utcToZonedTime`, `date-fns-tz/format` and `date-fns/addDays` to generate and format days

### Pros

- Simplifies logic to display and add days in the CMS by using ISO date strings
- Ensures correct UTC offset is used when calculating Forecast Graph dates on the client in KBYG web app
- Maintains backwards compatibility and MongoDB performance by keeping `forecastDate` as date and not a string
- Ensures correct formatting of next forecast date, regardless of DST changes

### Cons

- `forecastDay` date string is only current instance this pattern in our tech stack
  - On web, new library `date-fns-tz` will be required to format dates
  - Next forecast requires a time/hour, so will not follow the date string pattern and will continue to use UTC Date with offset
- Date strings do not remove all complexity from date utils
  - `date-fns-tz/utcToZonedTime` and `date-fns-tz/format` require some trickery to keep dates in UTC (see code examples below)
  - Dates are always in UTC, and not in the timezone of the subregion
- Refactor of Next Forecast date conversions in CMS still required to fix Australia bug
  - Simplifying CMS logic with `date-fns-tz` utils and ensuring correct offset is used should fix, however this bug will require additional testing as the root cause is not yet clear
- TBD on ability for native apps to use `forecastDay` date string for forecast date display
  - Native apps currently uses the `timestamp` and `utcOffset` from the API response to generate the Forecast Day dates

## How does this solve the problem?

- Maintaining ISO Dates in MongoDB keeps queries simple and performant, and provides flexibility to converting to timestamps and date strings on the API
- ISO 8601 date string increases readability of forecast date, without potentially confusing references to timezones
- Date strings without time simplify complex timestamp conversions on the client
- `date-fns-tz` utils simplify the creation of new, future dates on the client
- Date-specific UTC offsets and standardized date formatting will ensure that "next forecast" dates are consistenly converted accurately across services, regardless of daylight savings changes

## What do we know already?

A similar pattern that of the "next forecast" (adding date-specific UTC offset) was successfully implemented for KBYG Product API forecast endpoints and Surfline Web Graphs in the [Support Daylight Savings Time](https://wavetrak.atlassian.net/browse/FE-214) epic.

## Open Questions

- [ ] Determine best practices for automated testing dates across timezone changes

---

## Alternate Solution - UTC Forecast Days with Timestep Offsets

### TLDR

- Return Forecast Day `timestamp` as unix timestamp for all forecast endpoints
  - Seconds from epoch (same as current contract)
  - Include UTC offset for each date
- "Next Forecast" still represented as unix timestamp in API response
  - Include `nextForecastUtcOffset` for "next forecast" date

### Alternate Solution Overview

- Continue storing `forecastDate` as UTC dates in MongoDB
  - `SubregionForecastDays` `forecastDate` as midnight of date UTC
  - `SubregionForecastSummaries` `nextForecastTimestamp` as UTC date with time
- Forecasts API response overview:
  - Continue returning `forecastDate` and `nextForecastTimestamp` values as unix timestamps
  - Calculate and return timestep UTC offsets to all future forecast dates
    - `utcOffset` for each Forecast Day
    - `nextForecastUtcOffset` for Next Forecast date
    - Use existing `createUTCOffsetCalculator` util to generate offsets, matching pattern implemented for science forecast data in the KBYG Product API
  - Return the full subregion `timezone` in the API response to enable client-side date calculation across DST changes
    - `timezone` has string value i.e. `"America/Los Angeles"`
      - This is available on the subregion's primary spot
      - This is returned in addition to the existing `abbrTimezone`, as the full `timezone` enables the use of `date-fns-tz` on the client when calculating future days with the correct offset
- Refactor KBYG Product API to remove Subregion Forecast MongoDB models
  - Proxy requests for forecast data to the Forecasts API
- Update Forecasts CMS date formatting:
  - Forecast Days:
    - Existing Forecast Days returned from the API will be formatted using the `utcOffset` returned from the API
    - Dates for unforecasted days in the form will be calculated by using an updated version of the `getLocalDateForFutureDay` util, incorporating a new client version of `createUTCOffsetCalculator`
      - `moment` is already included in the CMS, as it is an internal app, but we will instead use `date-fns-tz/getTimezoneOffset` to match KBYG web pattern
    - On submit, the forecast dates will be converted to midnight UTC dates
  - Next Forecast:
    - Initial value of the form will use the `nextForecastUtcOffset` to ensure the correct offset is used, even if the next forecast day is across a DST change
    - Next Forecast day options in the select menu will use `date-fns-tz` and `date-fns/addDays` to generate day options
    - On submit, the `nextForecastTimestamp` will be generated from the selected values
- Update KBYG web date formatting:
  - The `NextForecast` component will use the `nextForecastUtcOffset` when formatting the date, instead of the current date's subregion offset
  - The `ForecastGraphDays` component, which displays dates above both the spot and subregion forecast graphs, will continue to generate dates on the client, but with updated logic:
    - `date-fns-tz/utzZoneOffset` and `date-fns-tz/format` will set the correct start date
    - `date-fns/addDays` will increment days before formatting
- Native app updates:
  - Use the `nextForecastUtcOffset` when formatting the `nextForecastTimestamp`
  - Forecast graph `timestamps` formatted with timestep `utcOffset`

#### Detailed Flow From Forecasts CMS to MongoDB

1. Forecaster visits Forecasts CMS
   1. Subregion forecast form contains fields for summary info (i.e. "next forecast" day and time) and `8` forecast days blocks (i.e. conditions for each forecast day)
2. The Forecasts CMS makes a `GET` request to the Forecasts API `/admin/forecasts` to obtain any existing forecast data in MongoDB
   1. Up to `8` forecast days returned, each with a `forecastDate` unix timestamp in seconds
      1. Subregion's primary spot timezone string is used to get current local date for subregion
      2. A date range `start` and `end` is calculated from this current local date, and the MongoDB `SubregionForecastDays` collection is queried for all documents with a `forecastDate` within the specified range.
         1. `forecastDate` is stored in Mongo as UTC Date, and is converted to unix timestamp in seconds in API handler
   2. `nextForecastTimestamp` returned as unix timestamp in seconds
      1. API Handler goes direct to MongoDB `SubregionForecastSummaries`
         where `nextForecastTimestamp` is stored as UTC date
      2. Handler converts to unix timestamp
   3. `nextForecastUtcOffset` calculated in API and returned in response
      1. Value is a `float` UTC offset in hours
   4. Each Forecast Day now contains a reference to its `utcOffset`, specific to that date in the subregion
      1. Value is a `float` UTC offset in hours
3. The Forecasts CMS stores `days[].forecastDate`, `days[].utcOffset`, `nextForecastTimestamp`, `nextForecastUtcOffset`, and `timezone` in application state
   1. Days with default values, including `forecastDate`, are created for any days that have not yet been forecasted
      1. If less than `8` forecast days are returned from API, the handler creates default values for the additional days
      2. Default dates are determined by converting the current day's `forecastDate` to a unix timestamp and adding a day in seconds to each additional day, taking into account the future offset for each day
         1. Dates for unforecasted days in the form will be calculated by using an updated version of the `getLocalDateForFutureDay` util, incorporating a new client version of `createUTCOffsetCalculator`
            1. `moment` is already included in the CMS, as it is an internal app, but we will instead use `date-fns-tz/getTimezoneOffset` to match KBYG web pattern
4. The Forecasts CMS populates initial form values with data in application state
   1. `forecastDate` timestamp is displayed above each day in the form
      1. Dates are displayed local to the subregion (not forecaster)
      2. Dates are displayed in format `DAY 1 THU 06.24`
         1. Timestamps are formatted using `date-fns/format`
            1. `date-fns/format` converts date to string format for display, which converts date to local user time
         2. Alternative is to use `date-fns-tz` to simplify date formatting without applying offset, but this does not match pattern on the web or native apps
   2. Next Forecast block displays two form fields to control the `nextForecastTimestamp`
      1. `DAY` form field to select the date (i.e. `Friday 06/25`)
         1. Initial value for this field uses `nextForecastTimestamp` and `nextForecastUtcOffset` returned from Forecasts API
            1. Timestamp is converted to midnight local to the subregion, using `nextForecastUtcOffset`
         2. Next Forecast day options in the select menu will use `midnightOfDate` and `date-fns/addDays` to generate day options
            1. Refactored util uses `date-fns/getTimezoneOffset` to reference the forecast date's correct `utcOffset`
            2. `date-fns/format` used to format date into readable format
      2. `TIME` form field to select the time (i.e. `6am`)
         1. This is a hardcoded list of hours, no date formatting used
5. On publish action, the Forecasts CMS formats dates
   1. `forecastDate` converted to midnight UTC for the forecast date using `date-fns` and `date-fns-tz` utils
   2. `nextForecastTimestamp` converted to UTC Date with time
      1. Uses correct UTC offset for Next Forecast `utcOffset`, specific to the future date and subregion timezone
6. Forecasts CMS `POST` request to Forecasts API `/admin/forecasts`
   1. `forecastDate` saved in MongoDB `SubregionForecastDays` as ISO Date in UTC midnight of the forecast day (i.e. `2021-06-24T00:00:00.000Z`)
      1. Form validation updated to compare UTC dates instead of date strings
      2. `createForecastHandler` saves date to MongoDB
         1. Uses value from `POST` body, no date conversion in API
   2. `nextForecastTimestamp` saved in MongoDB `SubregionForecastSummaries` as ISO Date (i.e. `2021-06-25T06:00:00.000Z`)
      1. Uses value from `POST` body, no date conversion in API

#### Detailed Product View Flow

1. User visits Subregion Forecast page
2. KBYG Web makes `GET` request to KBYG Product API `/regions/forecasts/conditions`
   1. KBYG Product API proxies to Forecasts API for data
      1. API response includes `utcOffset` for each Forecast Day
3. KBYG Web makes `GET` request to `/regions/overview`
   1. KBYG Product API proxies to Forecasts API for data
   2. `timestamp` returns Forecast Day date as unix timestamp in seconds
   3. `utcOffset` returns timestep offset for each Forecast Day
   4. `nextForecastTimestamp` returned as unix timestamp in seconds
   5. `nextForecastUtcOffset` returned as float hours
4. KBYG Web displays the array forecast day conditions in the order returned from the API
   1. The `forecastDate` is not used in the forecast conditions displayed in the KBYG app
5. The KBYG web front-end `ForecastGraphDays` component is updated to create dates on the client taking into account future date offsets
   1. Get current date local to the subregion
   2. Create ISO date string at midnight UTC for the current day in the subregion
   3. Use `date-fns-tz/utcToZonedTime`, `date-fns-tz/format` and `date-fns/addDays` to generate and format days
6. KBYG Web displays `nextForecastTimestamp` in time local to subregion
   1. Formatted as `Fri by 6am AEST`
      1. Uses `date-fns/format` to generate human readable date format
      2. `nextForecastUtcOffset` used to provide correct offset value for the future date

### Pros

- Ensures correct offset for all forecast dates, regardless of DST changes
- Follows pattern used by model forecast endpoints in the Product API (decorating API response with calculated `utcOffset` for each data point in the timeseries)

### Cons

- Does not significantly reduce complexity of date formatting
- Does not reduce confusion on date format and timezone coming from the API
- Refactor of next forecast date conversions in CMS still required to fix Australia bug
- Forecast day `utcOffset` should ensure correct days are returned, but is not currently used to generate forecast graph day on web
  - The forecast day UI is calculated client-side before the conditions response returns, and is shared with spot page which does not have forecasts/conditions returned

### How does this solve the problem?

Date-specific UTC offsets and standardized date formatting will ensure that dates are consistenly converted accurately across services, regardless of daylight savings changes.

### What do we know already?

A similar pattern of adding timestep UTC offsets was successfully implemented for KBYG Product API forecast endpoints and Surfline Web Graphs in the [Support Daylight Savings Time](https://wavetrak.atlassian.net/browse/FE-214) epic.

---

## Resources

- [ISO 8601](https://www.iso.org/iso-8601-date-and-time-format.html): Basic overview of calendar date format
- [ISO 8601 Wikipedia](https://en.wikipedia.org/wiki/ISO_8601#Calendar_dates): Wiki article on calendar date format
- [momentjs string parsing](https://momentjs.com/docs/#/parsing/string/): Utils to format date strings
- [moment-timezone](https://momentjs.com/timezone/): Utils to format convert dates across timezones
- [date-fns/parseISO](https://date-fns.org/v2.3.0/docs/parseISO): Util to convert `YYYY-MM-DD` date string into local midnight on front-end
- [date-fns-tz/format](https://www.npmjs.com/package/date-fns-tz#format): Format dates on the browser with specified timezone string. Can be used in conjucntion with `date-fns-tz/utcToZonedTime`
- [date-fns-tz/utcToZonedTime](https://www.npmjs.com/package/date-fns-tz#utctozonedtime): Convert date to specified timezone time. Can be used in conjucntion with `date-fns-tz/format`
- [date-fns-tz/getTimezoneOffset](https://www.npmjs.com/package/date-fns-tz#gettimezoneoffset): Get timezone offset in ms for a given timezone string
- [$dateFromString](https://docs.mongodb.com/manual/reference/operator/aggregation/dateFromString/): Mongo aggregation util to convert date strings into ISO dates for query
- [Stack Overflow discussion on storing calendar dates in Mongo](https://stackoverflow.com/questions/31272471/mongodb-comparing-dates-only-without-times)

## Date Formatting in JavaScript Code Examples

### ISO Date to Unix Timestamp

#### Convert JSON Date String to Timestamp in Seconds using `date-fns`

```JS
// UTC time due to the `Z` ending the date string

import { parseJSON } from 'date-fns';

const ISODate = '2021-07-10T00:00:00:000Z';
const timestamp = Math.floor(parseJSON(ISODate) / 1000); // 1625875200
```

#### Convert ISO Date String to Timestamp in Seconds using `date-fns`

```JS
// Adjusts to local time, similar to date-fns/format

import { parseISO } from 'date-fns';

const ISODate = '2021-07-10';
const timestamp = Math.floor(parseJSON(ISODate) / 1000); // 1625900400
```

### Unix Timestamp in Seconds to ISO Date

#### Convert Timestamp in Seconds to Date

```JS
// Adjusts to local UTC offset

const timestamp = 1625875200;
const newDate = new Date(timestamp * 1000); // Fri Jul 09 2021 17:00:00 GMT-0700 (Pacific Daylight Time)
```

#### Convert Timestamp in Seconds to ISO Date String

```JS
// UTC date

const timestamp = 1625875200;
const newISODate = new Date(timestamp * 1000).toISOString()); // '2021-07-10T00:00:00:000Z';
```

### Parsing ISO Calendar Date String vs Date String with Time

#### Parsing ISO Calendar Date

```JS
// Maintains local UTC offset, which results is problematic when formatting date

import { parseISO } from 'date-fns';

const dateString = '2021-07-10';
const parsedDateString = parseISO(dateString); // Sat Jul 10 2021 00:00:00 GMT-0700 (Pacific Daylight Time)
```

#### Parsing ISO Date with Time

```JS
// Not an acceptable value for parseISO

import { parseISO } from 'date-fns';

const dateString = '2021-07-10T00:00:00:000Z';
const parsedDateString = parseISO(dateString); // Invalid date
```

### Parsing Calendar Date String vs Date String with `date-fns-tz/utcToZonedTime`

#### Parsing ISO Calendar Date

```JS
// Maintains local offset which causes issues when formatting

const dateString = '2021-07-10';
const format = 'yyyy-MM-dd kk:mm:ss xxx';

const formattedDateString = utcToZonedTime(date, 'America/Los Angeles'); // Sat Jul 10 2021 07:00:00 GMT-0700 (Pacific Daylight Time)

const formattedDateString = utcToZonedTime(date, 'UTC'); // Sat Jul 10 2021 07:00:00 GMT-0700 (Pacific Daylight Time)
```

#### Parsing ISO Date with Time

```JS
// Converts to UTC

const formattedDateString = utcToZonedTime(date, 'America/Los Angeles'); // Sat Jul 10 2021 00:00:00 GMT-0700 (Pacific Daylight Time)

const formattedDateString = utcToZonedTime(date, 'UTC'); // Sat Jul 10 2021 00:00:00 GMT-0700 (Pacific Daylight Time)
```

### Formatting Calendar Date String vs Date String with `date-fns-tz/utcToZonedTime` and `date-fns-tz/format`

```JS
// Shared function for below examples

import { format, utcToZonedTime } from 'date-fns-tz';

const formatInTimeZone = (date, fmt, tz) =>
  format(utcToZonedTime(date, tz), fmt, { timeZone: tz });
```

#### Formatting ISO Calendar Date

```JS
// Maintains local offset which results in incorrect date adjustments across timezones

const dateString = '2021-07-10';
const format = 'yyyy-MM-dd kk:mm:ss xxx';

const auFormattedDateString = formatInTimeZone(date, format, 'Australia/Sydney'); // 2021-07-10 17:00:00 +10:00

const usFormattedDateString = formatInTimeZone(date, format, 'America/Los Angeles'); // 2021-07-10 24:00:00 -07:00

const utcFormattedDateString = formatInTimeZone(date, format, 'UTC'); // 2021-07-10 07:00:00 +00:00
```

#### Formatting ISO Date with Time

```JS
// Forcing UTC timezone ensures reliable date format no matter the user location

const format = 'yyyy-MM-dd kk:mm:ss xxx';

const auFormattedDateString = formatInTimeZone(date, format, 'Australia/Sydney'); // 2021-07-10 10:00:00 +10:00

const usFormattedDateString = formatInTimeZone(date, format, 'America/Los Angeles'); // 2021-07-09 17:00:00 -07:00

const utcFormattedDateString = formatInTimeZone(date, format, 'UTC'); // 2021-07-10 24:00:00 +00:00
```

### Adding Days to Calendar Dates vs Date String with `date-fns/addDays` and `date-fns-tz`

```JS
// Shared function for below examples

import { addDays } from 'date-fns';
import { format, utcToZonedTime } from 'date-fns-tz';

const addDaysAndFormatInTimeZone = (date, fmt, tz, daysToAdd) =>
  format(addDays(utcToZonedTime(date, tz), fmt, { timeZone: tz }, daysToAdd));
```

#### Adding Days and Parsing ISO Calendar Date

```JS
// As with formatting example above, date adjustments are unreliable to due UTC offset

const dateString = '2021-07-10';
const format = 'yyyy-MM-dd kk:mm:ss xxx';
const daysToAdd = 1;

const auFormattedDateString = addDaysAndFormatInTimeZone(dateString, format, 'Australia/Sydney', daysToAdd); // 2021-07-11 17:00:00 -07:00

const usFormattedDateString = addDaysAndFormatInTimeZone(dateString, format, 'America/Los Angeles', daysToAdd); // 2021-07-11 24:00:00 -07:00

const utcFormattedDateString = addDaysAndFormatInTimeZone(dateString, format, 'UTC', daysToAdd); // 2021-07-10 07:00:00 +00:00
```

#### Adding Days and Parsing ISO Date with Time

```JS
// As with formatting example above, forcing UTC allows adding dates to maintain midnight UTC value even when adding days

const dateString = '2021-07-10T00:00:00:000Z';
const format = 'yyyy-MM-dd kk:mm:ss xxx';

const auFormattedDateString = addDaysAndFormatInTimeZone(dateString, format, 'Australia/Sydney', daysToAdd); // 2021-07-10 17:00:00 -07:00

const usFormattedDateString = addDaysAndFormatInTimeZone(dateString, format, 'America/Los Angeles', daysToAdd); // 2021-07-10 17:00:00 -07:00

const utcFormattedDateString = addDaysAndFormatInTimeZone(dateString, format, 'UTC', daysToAdd); // 2021-07-11 24:00:00 -07:00
```

### Midnight of Date

#### Using Moment

```JS
const midnightOfDate = (timezone, date) => moment(date).tz(timezone).startOf('day').toDate();
```

#### Timestamp in Seconds with Subregion and User Offsets

```JS
// Sets midnight + local offset so that date-fns/format displays correct time (format converts to local time)
const midnightOfDate = (utcOffset, timestamp = Math.floor(new Date() / 1000)) => {
  const localDate = timestamp + utcOffset * SECONDS_PER_HOUR;
  const localMidnight = localDate - (localDate % SECONDS_PER_DAY);
  const midnight = localMidnight - utcOffset * SECONDS_PER_HOUR;
  return midnight;
};
```
