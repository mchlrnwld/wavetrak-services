# Buoyweather LOTUS Integration

## What are we doing?

Buoyweather currently has a 16-day LOLA forecast and a 7-day NCEP forecast. We are going to replace both forecasts with a 16-day LOTUS forecast. For the first iteration we will only add the 16-day LOTUS forecast tab while leaving the legacy forecasts.

## What problem are we solving?

We want to provide Buoyweather with LOTUS data that comes from the Forecast Platform while also deprecating our dependency on legacy systems (Best Forecast System and Marine API).

## What impact does this have on the business?

Providing more reliable LOTUS data from the Forecast Platform makes the product more valuable for all users leading to greater engagement and conversion. Removing our dependency on legacy systems gets us closer to removing these systems from our technical footprint. This will save us operating costs and engineering resources required to maintain these systems so that we can focus on developing better products and features.

## What is the solution?

The buoyweather forecast requires 6-hourly data for 16 days starting from midnight of the current day to 6 PM of day 16. We will use the page http://www.buoyweather.com/forecast/marine-weather/@33.4955977448657,-119.00390625 as a sample of a Buoyweather forecast page.

6-hourly forecast properties from the Best System or Marine API will be replaced by LOTUS data from the new Forecast Platform. *Tide data will continue to be generated using point based tide scripts executed on Surfline legacy tide servers.* See container level diagram (focusing on Forecast Platform):

![BW System Container](../images/buoyweather/bw-system-container.png?raw=true "BW System Container")

### 6-Hourly Forecast Properties

Forecast winds, weather, seas text are generated in [Buoyweather API code](/services/buoyweather-api/src/utils/writtenForecast.js).

The following properties are computed within the Buoyweather API using [suncalc](https://www.npmjs.com/package/suncalc).

- Sunrise
- Sunset
- Moonrise
- Moonset
- Moonphase

At least for the first iteration we should leave written forecast and suncalc code in Buoyweather API until we see a need to centralize any of this logic.

The rest of the forecast data for this page will come from two endpoints:
- http://science-data-service.prod.surfline.com/forecasts/swells?lat=33.4955977448657&lon=-119.00390625&agency=Wavetrak&model=Lotus-WW3&grid=GLOB_30m
- http://science-data-service.prod.surfline.com/forecasts/weather?lat=33.4955977448657&lon=-119.00390625&agency=NOAA&model=GFS&grid=0p25

`start` and `end` query parameters for midnight of the current date in the time zone of the location should be passed to both endpoints. These query parameters are already being computed in Buoyweather API.

The following is a table of all properties found for each timestamp on the forecast page along with the endpoint the data will come from and the path to the property in the response.

| Property | Endpoint | Response Property |
| -------- | -------- | ----------------- |
| Wind Average (Speed) | /weather | `data[].weather.wind.speed` |
| Wind Gust (Speed) | /weather | `data[].weather.wind.gust` |
| Wind Direction | /weather | `data[].weather.wind.direction` |
| Wave Average (Height) | /swells | `data[].swells.combined.height` |
| Wave Peak (Height) | /swells | `data[].swells.combined.height * 1.3` |
| Wave Direction | /swells | `data[].swells.combined.direction` |
| Wave Period | /swells | `data[].swells.combined.period` |
| Weather | /weather | `data[].weather.conditions` |
| Precipitation | /weather | `data[].weather.precipitation.volume` |
| Temperature | /weather | `data[].weather.temperature` |
| Pressure | /weather | `data[].weather.pressure` |

### Forecast Platform Enhancements/Fixes

- Fix full grid jobs to succeed even if previous run missing.

### Buoyweather Enhancements/Fixes

- Support expanded weather condition icons.

## Future Iterations

Long term we will want to move Buoyweather into MSW as a custom surf spots tool. To support this we will need to add surf height calculations to the endpoints using the Breaking Wave Height API (to be implemented).

We may also want to replace the tide data source at some time.

### GraphQL Full Grid Requests

For this iteration we will continue to use the existing REST endpoints for point based forecasts and not attempt to move these requests onto GraphQL. This is to limit the scope of the work required to validate this solution on Buoyweather and to protect us from some of the complexities of GraphQL.

Particularly, I'm concerned about the performance impact of these types of requests on GraphQL. To enforce the GraphQL standard, Apollo Server validates all nodes of the graph in each response. For large responses this can be computationally expensive. For point of interest forecasts we were able to solve this by caching responses, but for point forecasts caching will not likely be very effective. This will also open up our Science Data Service to be vulnerability to DoS.

## What do we know already?

### Land Forecasts

For points on land, 16-day LOLA forecasts look like they're snapping to the nearest sea grid point and returning all data except for tides. 7-day NCEP forecasts are erroring completely. The LOTUS GLOB_30m grid spans the entire globe including land and returns 0 swells on land.

## Other

- [Forecast Platform Endpoint Documentation (OUTDATED)](http://sl-docs.s3-us-west-1.amazonaws.com/wavetrak/index.html#introduction)
