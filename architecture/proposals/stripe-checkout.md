# Implement Stripe Checkout in Web Funnels

## What are we doing?

Implementing Stripe Checkout to Web Funnels. Stripe Checkout is a prebuilt, hosted payment page optimized for conversion. This lets us easily and securely accept payments online via Stripe. We will implement this first in the Upgrade Funnel. Promotion Funnels would remain as it is.

[Stripe Checkout Demo](https://checkout.stripe.dev/)

[Stripe Checkout Docs](https://stripe.com/docs/billing/subscriptions/checkout)

## What problem are we solving?

- Mitigate Fraud and Credit Card Skimming issues within the Web Funnel
- More secure than our current setup
- Reduce the overhead of designing and maintaining our on Checkout page. It’s like an entire component that we can mentally hand off to Stripe
- Real-time card validation
- Faster loading times and provides an optimal experience across any devices
- Out of the box support for Apple Pay and Google Pay
- Checkout supports 25+ languages, 135+ currencies, and dynamically shows the payment methods most likely to improve conversion
- To be compliant with future payments regulations
- Be tax compliant

## What impact does this have on the business?

New regulations in online commerce are making the payment flows more complicated. The cost of maintaining a high-conversion, compliant payment form quickly becomes significant and growing when we have to support these regulations. With Stripe Checkout we don’t need to maintain this ourselves anymore.

## What is the solution?

Use Stripe's [Checkout Session API](https://stripe.com/docs/api/checkout/sessions/create) to create a session that is customized for the user and let Stripe Checkout payment form take care of the rest. The workflow would look like as below:

![Alt text](../images/subscriptions/stripe-checkout.png?raw=true "Stripe Checkout")

### Upgrade Funnel changes

- Remove all Payment Form related logic from the funnel
- Show only the Plans UI
- Show texbox to enter Discount Code

Upon clicking the checkout button we will call `subscription-service` to create a Checkout Session.

The Sign/Create Account pages won't be modified.

### Subscription Service changes

#### Add a new endpoint `POST /subscriptions/billing/checkout`

To create a new Stripe Checkout Session.

Pseudocode:

```javascript
const session = await stripe.checkout.sessions.create({
  mode: "subscription",
  payment_method_types: ["card"],
  line_items: [
    {
      price: priceId, // Selected PlanId
      quantity: 1,
    },
  ],
  client_reference_id: userInfoMongoId, // A unique string to reference the Checkout Session to a user if exists
  customer: "cus_JqY1EnntBherPP", // Stripe CustomerId if exists
  // ?session_id={CHECKOUT_SESSION_ID} means the redirect will have the session ID set as a query param
  success_url: `${domainURL}/success?session_id={CHECKOUT_SESSION_ID}&success=true`,
  cancel_url: `${domainURL}/failure/?canceled=true`,
  subscription_data: {
    trial_period_days: 15, // Set 15 days Free Trial
    default_tax_rates,
    metadata, // A Key Value pair for storing additional data
  },
  discounts: [
    {
      coupon: "applies_to_test_all",
    },
  ],
});
```

The endpoint accepts the below properties in the body:

- plan
  - This would be an enum and not the actual planId in Stripe. This is done for security purposes so as to not expose the Id and if needed we can change this to any other price we want in the future.
  - This enum mapping would be managed in the service itself.
  - The enum would be sent to the frontend app.
- success_url
  - Success page on the Funnel that Checkout returns the customer to after they complete the payment
- cancel_url
  - Failure page on the Funnel that Checkout returns the customer to if they cancel the payment process
- userId
  - userInfo Mongo Id
- discount
  - Discount Code the user entered

Pseudo logic:

```javascript
try {
  // Lookup req.body.plan from the Plan Dictionary and set the planId accordingly
  const planId = getPlan(req.body.plan);
  // Load  user if exists
  const user = await getUserDetails(userId);
  // has this user subscribed previously?
  const subscriptions = await getAllSubscriptionsByUserId(userId);
  // Is the user an active Premium user ?
  if (isPremium) {
    // Don't proceed
  }
  // Compute default tax rates
  const defaultTaxRates = await listTaxRates();
  // Check if user is eligible for Free Trial or not
  const shouldAllowFreeTrial =
    trialEligible && !wasPremium && !planId.includes("month") && !coupon;

  // If everything looks good and we have the required values then proceed with creation of the checkout session
  const session = await stripe.checkout.sessions.create({
    // required properties
  });
  // Redirect to the URL returned on the Checkout Session.
  res.redirect(303, session.url);
} catch (err) {
  res.status(500).json({ statusCode: 500, message: err.message });
}
```

Also refer to this [create function](https://github.com/Surfline/wavetrak-services/blob/036e37ab849cd56eef1d289ea9e7f0d0a64fb434/services/subscription-service/src/server/subscriptions/stripe/index.js#L66) for more details.

NOTE: In `subscription` mode, the customer’s default payment method will be used if it’s a card, and otherwise the most recent card will be used.

#### Add a new Stripe webhook handler `checkout.session.completed`

After the subscription signup succeeds, the customer returns to the `success_url`, which initiates a `checkout.session.completed` event. Once we receive a `checkout.session.completed` event, we can provision the subscription.

- Check if the subscription is created and is active in Stripe
- Check if our Subscription Mongo collections are created/updated with the correct values
- Check if there are any payment related errors for the user and take reconcilation steps accordingly

Based on my testing so far I've seen that the `checkout.session.completed` Stripe event is triggered last for a succesful payment.

### Notes

- We are moving away from a synchronous pattern to an asynchronous pattern using web-hooks for Stripe checkout. I think this is one of the downside of using Stripe checkout where we depend on receiving the web-hooks from Stripe end to update our internal systems (Mongo) and entitle the user. If there is a delay in sending the web-hooks or processing the web-hooks on our end then the user won't be entitled until the web-hook succeeds. We could try updating our internal systems when the user reaches back to the success page but I feel it is not an elegant solution and can be complex and possibly mess up with the ongoing web-hook calls (such as `customer.subscription.*`, `invoice.*`). Typically Stripe web-hooks are fast and provides a seemless experience in the Checkout process.
- The Stripe checkout page cannot be used more than once per session. i.e the link would only work once, as soon as the customer clicks on 'start trial'. This should help in restricting the offer to that specific customer and prevent gaming the system since the page gets automatically expired after the payment is successful.
- We can customize the Product information displayed in the Checkout page such as Image and description. Also we can add any metadata to the Product. These can be managed in the [Stripe Product](https://dashboard.stripe.com/test/products?active=true) page.
- Below Segment events should be modified/removed from the Upgrade Funnel as part of this change:
  - `Viewed Checkout Step` - Triggered for Step 2
  - `Completed Checkout Step` - Triggered for Step 2
  - `Completed Order` - Triggered for Step 2. Probably best to move this to server side.
