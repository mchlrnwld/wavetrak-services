# Subscription Segment Reconciliation Proposal

## What are we doing?

**Adding a job that reconciles key Segment subscription identity properties on a regular interval.**
The job should call `subscription-service` which triggers a Segment `identify` call with the 
latest document for each user. The job may optionally have additional duties if we want to add
further consolidation.

## What problem are we solving?

The current status quo for updating Segment user identities is that in certain functions (mostly 
in `subscriptions-service`, `user-service`, `metering-service`, and `favorites-service`), we 
manually invoke a segment `identify` call with the associated relevant properties for the identity.
Usually these `identify` calls are associated with a `trackEvent` or MongoDB write.

Our current process for ingesting segment identity updates has a few problems. 
- There is little visibility if there was a problem during the process. An error is
usually logged, but these are mostly unchecked unless an alert occurs.
- Even if we view the error, there is no established or easy way to retrigger the segment
`identify` call. The call is usually closely coupled with the aforementioned code, making it 
difficult to re-trigger in their current form.
- In urgent matters or incidents we may need to modify Mongo documents manually. Usually the
changes made will not make it to Segment in this case.

The above issues results in low confidence of the data in Segment.

## What impact does this have on the business?

The goal is to have higher confidence in Segment identities due to improved accuracy. The business 
has shifted to more heavily rely on this data for decision-making and report generation. As 
engineers our focus should shift accordingly to support the business.

These identities also get sent to other integrations besides Analytics, such as Customer.io and 
Braze. In order for other departments to use these tools heavily and accurately, we should ensure
accuracy in the identities.

For engineering, this should lead to less manual work and audit tasks.

This should have positive impacts for engineering, marketing, product, analytics, and executive 
teams.

## What is the solution?

We currently have an existing reconciliation process for subscriptions, the job 
`verify-in-app-purchases`. This job calls a `subscription-service` endpoint to trigger a
verification of `google` and `itunes` subscriptions with their response.

In addition we also have a job `expire-gift-subscriptions` which also calls `subscription-service`
and validates `gift` subscriptions.

We can leverage the same behavior as these services into a new job called `identify-subscriptions`.
The behavior for `identify-subscriptions` should do as follows:
- The job pulls in relevant subscriptions via query.
- For each subscription, call the following new subscription endpoint:
```/admin/subscriptions/identify POST```
- The `subscription-service` identifies the subscription.
- The job loop continues to the next subscription.
- On a failed call, the `subscription-service` should notify New Relic.

The job can be re-triggered when needed on-demand, and the interval frequency can be configured to
satisfy business needs and to prevent excessive MTU usage.

An alert condition can occur either from `subscription-service` threshold for this API call or a
fatal job error.

### Updated Backend Architecture
![Alt text](../images/subscriptions/subscriptions-backend-container.png?raw=true "Subscriptions Backend Container")

## How does this solve the problem?

In our system, a reconciliation process provides confidence that the end result is correct.
Synchronous systems can be designed correctly, but we need processes in places for when things go
wrong. This solution takes an existing technique/job we are using (`verify-in-app-purchases`) and
expands on the idea to improve identity accuracy.

## What are the alternatives?

### Event-driven

A system that is event-driven would also give us confidence that identify calls eventually succeed.
Instead of invoking the identify call with the associated change, we could change the `identify`
call to be associted with reading from an event message queue. With event-driven systems, we 
can inherently gain persistence and retry mechanisms.

There are a few reasons why this isn't being proposed:
- Event-driven systems can be very different. The scope of such a change is a lot bigger than
the proposed solution. We wouldn't want to proceed naively with it.
- It could be over-engineering for the specific problem at-hand.
- A reconciliation process would be useful in conjunction with whatever future improvements we
make.

### Improved testing

Auditing all functions, and adding or updating unit and integration tests should be done to find
code issues with tests. However this doesn't provide the confidence in the case of an issue occurring.
It can be a separate initiative.

### Status-quo

The number of segment-related issues have indicated that maintaining the status quo is most likely 
a poor decision.

## Other

### Questions

**Do we have adequate test coverage for Segment workflows?**

We do have unit tests for most `track` events. In integration tests, most of the analytics calls 
are stubbed out and not checked. It could be improved. With out new tracking plan proposal, we 
should begin to make sure `identifySubscription` is part of the unit testing criteria.

**What circumstances could lead to decreases accuracy in the Segment Identity values?**

The known reasons are outages, bugs, inadequate code, manual DB updates, or logic changes that
invalidate the current identity.

**What steps can we put in place to recover/resend Segment Identity when critical outages occur within the Subscription APIs.**

The proposed solution should cover this concern. The solution is a separate process that can be 
rerun at any time post-outage.
