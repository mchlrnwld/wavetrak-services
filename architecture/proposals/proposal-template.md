# Title of Architecture Proposal

*Architecture proposals should match the below format. All sections not listed as optional are required, please reach out to your manager or Gavin/Brenden if you need help with any section. Some examples of good documents: [Push Notifications Architecture](https://wavetrak.atlassian.net/wiki/spaces/MS/pages/499712417/Session+KBYG+Push+Notification+Architecture), [Garmin Integration Architecture](https://wavetrak.atlassian.net/wiki/spaces/KBYG/pages/696975369/Sessions+Garmin+Intergration+Architecture) and [Sessions Lambda Queue](https://wavetrak.atlassian.net/wiki/spaces/KBYG/pages/620920900/Sessions+Architecture+-+Queue+Lambda+Invocations).

## What are we doing?

*2-3 sentences describing what we're trying to achieve. There's sections below to describe the problem and solution in detail.*

## What problem are we solving?

*Detailed explanation of what problem we're trying to solve. This could be fullfilling product requirements that the existing architecture isn't fit to support (in which case detail why). It could be improving some aspect of our architecture to avoid outages.*

## What impact does this have on the business?

*This is the hardest section for most to write but probably the most important. We should describe why this positively impact the business and why we need to do this now. Be as objective as possible the most impactful things will naturally take root. If you need help framing this up talk to Gavin or Brenden.*

## What is the solution?

*Description of the proposed solution and architecture diagram. Diagrams can be linked to like this:*

`![Alt text](../dist/systems/buoyweather/overall-system-context.png?raw=true "BW System Overview")`

## How does this solve the problem?

*Optional section but certainly helps makes the case for this change, this section can also be rolled into the one above. Should articulate the benefits of this solution and how components of it solve the problem.*

## What are the alternatives?

*Optional section: list other ways of archiving this and why they aren't as suitable as the proposed.*

## What do we know already?

*Optional section: list data points we might need to know when reviewing this architecture. Feel free to put critical data points in the solution section.*

## Other

*Optional section: misc stuff that might be useful to the reader. For example links to related Confluence docs, PRDs, other architectures, blog posts etc.*
