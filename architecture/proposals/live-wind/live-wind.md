# Live wind

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [What are we doing?](#what-are-we-doing)
- [What problem are we solving?](#what-problem-are-we-solving)
- [What impact does this have on the business?](#what-impact-does-this-have-on-the-business)
- [What is the solution?](#what-is-the-solution)
  - [Weather station / sensor management](#weather-station--sensor-management)
  - [Ingesting and Storing live-wind data](#ingesting-and-storing-live-wind-data)
    - [Data-structure](#data-structure)
    - [Wind-data ingest](#wind-data-ingest)
  - [Science Data Service Changes](#science-data-service-changes)
    - [New HTTP endpoint](#new-http-endpoint)
    - [GraphQL changes](#graphql-changes)
  - [KBYG Product API changes](#kbyg-product-api-changes)
    - [Forecast routes](#forecast-routes)
    - [Spot report](#spot-report)
    - [New live-wind route](#new-live-wind-route)
  - [Spot Report View Builder Changes](#spot-report-view-builder-changes)
  - [Validation](#validation)
  - [System diagram](#system-diagram)
    - [Sequence Diagrams](#sequence-diagrams)
  - [Monitoring and alerting](#monitoring-and-alerting)
    - [Weather Stations API](#weather-stations-api)
    - [WeatherLink data collection Lambda function](#weatherlink-data-collection-lambda-function)
    - [Science Data Service import Lambda function](#science-data-service-import-lambda-function)
    - [Validation Airflow job](#validation-airflow-job)
- [Costs estimates](#costs-estimates)
- [What do we know already?](#what-do-we-know-already)
  - [Understanding the live-wind data](#understanding-the-live-wind-data)
- [live-wind ingestion job diagram](#live-wind-ingestion-job-diagram)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## What are we doing?

We are building a system to gather, validate, and surface live-wind data, consisting of average wind-speed taken at 1-minute intervals, and to associate wind stations to Surfline Spots in order to integrate live winds into the surf spot KBYG product.

We will build:

1. a CMS to manage wind stations and associate spots to wind-stations; and
2. a data-pipeline that gathers, validates, and stores live-wind data, by wind-station and sensor.

Changes in the Science Data Service will deliver:

1. live-winds assimilated with wind forecasts for a Forecast Platform Point-of-interest, i.e. a forecast using live-data up to the current time, and then future forecast data with correction factors in the near term to account for the current wind readings; and
2. use of the assimilated forecast/live-wind data in the automated surf rating algorithm.

## What problem are we solving?

We have an existing proof-of-concept system producing live-wind data called Timberbridge. This is powering a live-wind feature in the production iOS app, but it has the following problems:

- it is built outside of our usual infrastructure patterns;
- it cannot scale past the 30 wind stations it's currently providing; and
- management of the wind-stations and wind-station/spot relations is via CSV.

We will replace this with a productionised system that can easily handle the currently planned 600 stations, with capability to scale further, and with data that can be managed by other teams (Hardware ops, Forecast/Data-Science, and Spots Team) using Surfline-Admin tools.

## What impact does this have on the business?

This will allow the existing live-wind feature in iOS to scale out from ~30 spots to around ~600 (currently planned) spots, improving the accuracy of the product for users and increasing user-engagement.

The work facilitates the complete Surfline iOS redesign (with live wind information assimilated into the forecast and a _consistent_ automated surf rating), which is not possible in the current Timberbridge system, but will be possible in this new architecture.

Replacing proof-of-concept code improves the code-maintainability and observability, and will make new features possible, and allow faster future iteration of this product. Replacing the CSV-based configuration of weather-stations will allow the Hardware Ops Team, Spots Team, and Forecast/Data-science team the ability to manage weather station configurations and spot associations.

## What is the solution?

TL;DR:

> New CMS and API-service to manage live-wind stations list, new live-wind data collection / ingestion jobs to gather and store live-wind data every 1 minute and surface this data through the Science Data Service.

We will use the [WeatherLink API](https://weatherlink.github.io/v2-api/) as our source of live-wind data. The Hardware Ops Team will configure our own wind-stations within WeatherLink where we can also access third-party weather-stations. Our data will thus come from a combination of stations owned and operated by Wavetrak and from third-party stations; but in all cases the data will be gathered via the WeatherLink API.

Below the components of the system are described. For completeness we've included existing services that are used, describing any modifications required. The systems are broken out into a few (somewhat) discrete sub-areas.

### Weather station / sensor management

Component | Status | Owner | Code-location | Description
----------|--------|-------|---------------|------------
Weather Stations API | Built; ongoing development | RT | `wavetrak-services/services/weather-stations-api` | <ul><li>CRUD API service (Node + Express) and Mongo collection of weather stations.</li><li>The key meta-data on stations held by this API are: Name, latitude, longitude, WeatherLink ID, and a stations status (`UP`, `DOWN`, `DELETED`). Stations also contain a list of sub-objects: the sensors attached to the station, including each sensor's latest validation F1 score (see [Validation](#validation)) and status (`UP`, `DOWN`, `INVALID`, `NOT_YET_VALIDATED`).</li></ul>
Weather station updates SNS topic | To do | RT | `wavetrak-services/services/weather-stations-api/infra` | On any update to a weather station the Weather stations API will publish a message on this topic
Weather Stations CMS | Built; ongoing development | RT | `surfline-admin/modules/weatherStations` | CMS to add and manage the list of available wind stations; contains stations meta-data, and WeatherLink station-ID.
Spots API | Pre-existing | FE | `wavetrak-services/services/spots-api` | <ul><li>Add new live-winds weather stations list, and sensor object (with ID and status) to Spot objects, and the spot REST and GraphQL API.</li><li>This will be a many-to-many relationship between Surfline Spots and weather stations. I.e. one station may be used at multiple spots, and a spot may have more than one station associated.</li><li>New endpoint `/sns/weather-station-updated` subscribed to station topic above.</li><li>When there is any update the to list of stations associated with a spot, or a notifaction of station changes on the SNS topic, the spot-admin API should reqeust the stations/sensor status from all associated stations for the spot and choose a `sensor_id` to use, and persisting that against the spot object. Where a spot has multiple sensors, the closest `UP` sensor is chosen. If there are multiple sensors on the same station (so there is not one nearest sensor), the sensor with the best F1 score is chosen.</li><li>If the spot previously had an `UP` sensor, but now none is avaialble the sensor status on the spot object should be updated to `DOWN` or `INVALID` as appropriate.</li></ul>
POI/Spots CMS | Pre-existing | FE | `surfline-admin/tree/master/modules/poi` | Update Surfline spot section to manage / map a weather stations to a spot. (Code similar to existing cams list management.)

### Ingesting and Storing live-wind data

Live-wind data will be stored in the Science Data Service's PostgreSQL database, and surfaced through that service's API.

#### Data-structure

```sql
CREATE TABLE live_weather (
  sensor_id      TEXT        NOT NULL,
  reading_time   TIMESTAMP   NOT NULL,
  wind_speed     NUMERIC     NOT NULL,
  wind_direction NUMERIC     NOT NULL,
  gust_speed     NUMERIC     NOT NULL,
  created_at     TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at     TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (sensor_id, reading_time)
);

CREATE TRIGGER live_weather_updated_at
BEFORE UPDATE ON live_weather
FOR EACH ROW
EXECUTE PROCEDURE set_updated_at();
```

#### Wind-data ingest

The overall pattern here is similar to (some) other forecast platform jobs: We have a downloader fetching data from WeatherLink and writing to S3, and a second job processing files in S3 and inserting data into the PostgreSQL table.

The key difference is that we'll use Lambda functions rather than the Airflow/AWS-Batch approach of other data pipelines. The use-pattern here is different: while model data is processed in large batches at (usually) 6 hour intervals, here we have a continual flow of relatively small amounts of data. The priority here is to fetch data at a relatively high frequency and minimise the time until it's available on our product APIs. This makes AWS Batch unsuitable due to the long and variable time it can take to begin new jobs submitted to the job queue.

Component | Status | Owner | Code-location | Description
----------|--------|-------|---------------|------------
WeatherLink data collection job | To do | RT | `wavetrak-services/functions/live-weather-download-weatherlink`  | EventBridge (new name for CloudWatch events, spun out into separate AWS service) will trigger a lambda every 1 minute which will: </br></br><ul><li>Get the list of stations from our weather-stations-api</li><li>Use this list to make requests to WeatherLink's `/current/{station-id}` endpoint to retrieve current conditions for each of the stations</li><li>Create a single Parquet file in S3 in a new bucket (`s3://wt-live-weather-{env}/weatherlink/data/collection_dt={YYYY}-{mm}-{dd}-{hh}:{mm}:{ss}.parquet`) which contains the conditions from all the sensors</li><li>The Parquet file will include _all_ data we get from the sensor, which we'll keep for archive purposes and potential future product use-cases.</li><li>Any new sensors identified will be `POST`'d to our weather-stations-api</li></ul> Throttling logic within the Lambda function will keep us within WeatherLink's rate limits. This will be written in Python 3.<br>Given the importance of timing to this function, run-time and regular scheduling will be monitored (see [Monitoring and alerting](#monitoring-and-alerting)).
Science Data Service import | To do | FE | `wavetrak-services/functions/process-forecast-platform-live-weather` | <ul><li>New S3 live-weather files will trigger an SNS message going to an SQS queue.</li><li>Messages in SQS will trigger a Lambda function to process the new files and insert data into PostgreSQL.</li><li>After `insert` the Lambda will post messages to a new SNS topic (`wt-forecast-platform-updated-live-weather-{env}`) to indicate which sensors IDs have new live-weather data.</li><li>The will be written in Python 3.</li></ul>

### Science Data Service Changes

#### New HTTP endpoint

In order to (a) avoid having to reduce the caching time on GraphQL queries, and (b) avoid creating large GraphQL responses (which can have performance problems due to run-time type-checking), high resolution live-wind and assimilated forecast data will be surfaced on a new standard HTTP JSON endpoint on the Science Data Service.

For example:

```
GET /live?start=1600000000&end=1600003600&interval=600&pointOfInterestId=aaaabbbbccccdddd&sensorId=xxxxyyyyzzz&units=
```

Query parameter | Description
---------------|------------
`start` | Unix timestamp for start. Inclusive.
`end` | Unix timestamp for end. Exclusive.
`interval` | Time resolution for response data, in minutes.<br>Requests where `(end - start)/(60 * interval)` is more than 294 (i.e. 49 hours of 10 minute resolution) will result in a `400` response.
`pointOfInterestId` | The point of interest to use for future timestamps.
`sensorId` | The wind sensor to use for wind readings.

All query parameters are _required_.

The response structure is

```js
{
  "associated": {
    "windModel": {
      "agency": "NOAA",
      "model": "GFS",
      "grid": {
        "key": "0p25",
        "lat": 0.0,
        "lon": 0.0
      }
    },
    "waveModel": {
      "agency": "Wavetrak",
      "model": "Lotus-WW3",
      "grid": {
        "key": "UK_3m",
        "lat": 0.0,
        "lon": 0.0
      }
    },
    "units": {
      "speed": "mph"
    }
  },
  "data": [
    {
      "timestamp": 1600000000,
      "sensorWind": true,
      "wind": {
        "speed": 10,
        "gust": 15,
        "direction": 72
      },
      "rating": 3
    },
    {
      "timestamp": 1600000600,
      "sensorWind": false,
      "wind": {
        "speed": 12,
        "gust": 16,
        "direction": 68
      },
      "rating": 3
    },
    // etc...
  ]
}
```

Key | Description
----|------------
`associated.windModel,*` | Wind model and grid point used for forecast data.
`associated.swellModel,*` | Swell model and grid point used for forecast breaking wave height data in rating calculation.
`associated.units.speed` | Units for wind speed an gusts.
`data.[].timestamp` | Unix timestamp of data.
`data.[].sensorWind` | Boolean, indicates if the wind at this timestamp is directly from a sensor reading (`true`) or is model data (`false`).
`data.[].wind.speed` | Wind speed.
`data.[].wind.gust` | Wind gust speed.
`data.[].wind.direction` | Wind direction in degrees clockwise from North. E.g. 45 is wind out of the north-east.
`data.[].rating` | Surf rating based on wind and breaking wave forecast.


##### Implementation

* `live_weather` data should be used for historical data
* Future data is interpolated to the same time-resolution and error-corrected in the near term.
  - This details of this algorithm may be adjusted based on testing with the forecast and data-science team, but initial forecast error is based on the average delta of live-winds and forecast over the preceding 20 minutes, and trended out over the following 4 hours.
* Use existing methods to fetch forecast wind and breaking wave height data.
* Breaking wave heights should be interpolated to the same resolutions and wind and wave passed to the existing method to calculate rating.

##### Caching

This data is updating at around once per minute, and we would like the data to show on the product soon after it's available. Nevertheless, we will implement a caching layer with a short TTL in order to reduce computation and reduce load on the database.

* Requests to `/live/` will use a look-aside cache with Redis as the storage engine. (Redis is already in use as the caching layer for the GraphQL endpoint.)
* The key will be composed of the query parameters and path, with the prefix `science-data-service:live:`
* Time-to-live: 30s.

#### GraphQL changes

An additional optional argument to the `SurfSpotForecast` query `wind` and `rating` objects to specify a live-wind sensor ID. If provided then historical data is taken from the sensor, and forecast winds have corrections applied in the near term. There is no change to the response structure.

There is no change the cache times in GraphQL. These queries should therefore only be uses for larger intervals (i.e. greater than or equal to one hour).

```diff
 extend type Query {
   surfSpotForecasts(pointOfInterestId: String!): SurfSpotForecasts
 }
 
 type SurfSpotForecasts {
   pointOfInterestId: String!
   wind(
     agency: String
     model: String
     run: Int
     grid: String
     start: Int
     end: Int
     interval: Int = 10800
     windSpeed: String = "m/s"
+    sensorId: String
   ): WindForecast
   rating(
     run: Int
     start: Int
     end: Int
     interval: Int = 10800
+    sensorId: String
   ): RatingForecast
 }
```

Task | Description
-----|------------
Add live winds to `WindForecast` | <ul><li>If a `sensorId` is passed for `live_weather` data should be used for historical data, and future data is error-corrected in the near term.</li><li>Update `getSurfSpotWindData`</li><li>Reuse the methods developed for the `/live` endpoint</li></ul>
Add live winds to `RatingForecast` | <ul><li>If a `sensorID` is passed then live-winds should be used and future forecast data is error-corrected in the near term.</li><li>The resolver [`getRatingForecast`](https://github.com/Surfline/wavetrak-services/blob/4c60f0da5535c29723447cec5534ed368356542d/services/science-data-service/src/server/graphql/forecasts/resolvers.ts#L298) uses `getSurfSpotWindData` and so it is only required to pass the value of `sensorID` through in order to use adjusted wind data. Otherwise `getRatingForecast` can remain unchanged.</li></ul>

### KBYG Product API changes

#### Forecast routes

Routes in the KBYG product API that fetch wind and rating data (`/spots/forecasts/wind` and `/spots/forecasts/ratings`) should be amended to pass the `sensorID` field to the upstream Science Data Service request (this may be behind a split for a period of time).

There should be no contract change on this API.

#### Spot report

The `/spots/reports` route should be amened to include the following sub-object of the `spot` object:

```js
{
  // within the `spot` object
  windSensor: {
    _id: "aaa-bbb-ccc-ddd",
    stationName: "Bantham weathervane",
    status: "UP",
  },
}
```

This data will be available on the spot report view, and will allow the product front-end to distinguish these three case:

- This spot does not have live wind (do not make a client side request for live wind data, do not render an live-wind product);
- this spot has live wind (make client-side data request, render product); or
- the spot has live-wind, but there is an outage (render appropriate "sorry - we're working on it" type message).


#### New live-wind route

A new route on the KBYG product API `/spots/forecast/live` to proxy the Science Data Service `/live/` route.

The response structure would match [Science Data Service - New HTTP endpoint](#new-http-endpoint), but its query parameters would be as described below. The `spotId` is used to look up the `pointOfInterestId` and `sensorId` to pass to the science data service.

Query parameter | Description
---------------|------------
`start` | Unix timestamp for start- inclusive.
`end` | Unix timestamp for end- exclusive.
`interval` | Time resolution for response data, in minutes.<br>Requests where `(end - start)/(60 * interval)` is more than 294 (i.e. 49 hours of 10 minute resolution) will result in a `400` response.
`spotId` | The spot we're interested in. If this spot does not have live wind data then a `400` response is returned

### Spot Report View Builder Changes

The Spot Report View should always have the best current wind data that we have, be that forecast or live. We will make the following changes to the Spot Report View Builder:

* When the spot report builder is triggered from `spot_details_updated_{env}` it should copy the sensor object (id, status, etc) from the spot object onto the spot-report-view.
* Refactor the the `getForecasts` function:
  - The `wind` object should be requested from the GraphQL endpoint if and only if the spot has no active wind sensor; otherwise
  - the new `/live` endpoint described above should be used.
* The Spot Report View Builder lambda function will be subscribed to the new topic `wt-forecast-platform-updated-live-weather-{env}`. For messages on that topic (which will contain sensor ID) the lambda should iterate over all spot-reports actively using that sensor and update the report's `wind` sub-object using `/live` data.

### Validation

Component | Status | Owner | Code-location | Description
----------|--------|-------|---------------|------------
Station Validation Airflow | To do | RT | `wavetrak-services/jobs/live-wind-validation` | <ul><li>Runs every **10 minutes**</li><li>Logic determined by Data-science, but in production is owned by RT</li><li>Requests to the weather stations API to get the list of all wind sensors in the system.</li><li>The job will check that sensors are reporting data. Any sensor with no new data for more than 20 minutes (subject to change) will be marked as `DOWN`. New F1 scores for each sensor will be caclulated **hourly**; the job re-calculate F1-scores of all sensors whose current F1 score is older than an hour.</li><li>Requests data from the SDS for POI wind forecast data and from S3 (via AWS Athena) for live-wind data, and validates that the live-wind data is good. Aim is to detect faults at the wind sensor.</li><li>History of F1 scores will be stored in `s3://wt-live-weather-{env}/weatherlink/validation/` and may be queried via Athena</li><li>A `POST` to the weather stations API will update all the sensors' latest F1 scores, and if an error is detected will mark the sensor status as `INVALID` or `DOWN`. Conversely it will set to `UP` after a sensor has recovered.</li></ul>

### System diagram

![Live Wind Overview](./images/live-wind-overview.png "Live Wind Overview")

#### Sequence Diagrams

##### Adding a station

![Adding a new station](./images/sequence-new-station.png "Adding a station")

##### Data ingest

This is split over two sequence diagrams: One that shows up to the Parquet file in S3, and one from that point onwards

![Eventbridge event](./images/sequence-eventbridge.png "Eventbridge event")

![New S3 file event](./images/sequence-s3file.png "New S3 file event")

##### Validation

![Validation](./images/sequence-validation.png "Validation")

##### Adding/removing stations from a spot

![Updating spots](./images/sequence-spot-association.png "Updating spots")

### Monitoring and alerting

#### Weather Stations API

Integrated with New Relic. Will have APDEX and error-rate alerting configured as standard using the Terraform configurations in `wavetrak-infrastructure/terraform/modules/aws/ecs/service`.

#### WeatherLink data collection Lambda function

Function will be configured with New Relic integration.

##### Failure modes

* Fail the Lambda outright if:
  * the initial request to stations API to get stations list fails; or
  * writing output to S3 fails
* Lambda should not fail but call `newrelic_notic_error` on:
  * Failure to fetch data for a station from WeatherLink; and
  * failure to `POST`/`PATCH` to the stations API to add new sensors.

##### Alerting

Exact values TBC, but broadly:

* PagerDuty on max run time duration >= 3 minutes;
* PagerDuty on average run time duration >= 30s;
* PagerDuty on error rate >= TBC% over TBC minutes;
* PagerDuty on invocations rate deviating from baseline.
* Slack notifications on failure to create new sensor in stations API (`X` failures in `T` minutes).

#### Science Data Service import Lambda function

Function will be configured with New Relic integration.

##### Failure modes

Fail Lambda function if:
* Unable to parse file in S3; or
* unable to write data to PostgreSQL.

##### Alerting

Exact values TBC, but broadly:

* PagerDuty on max run time duration >= 3 minutes;
* PagerDuty on average run time duration >= 30s;
* PagerDuty on error rate >= TBC% over TBC minutes;
* PagerDuty on invocations rate deviating from baseline.

#### Validation Airflow job

New Relic integration, with PagerDuty on any total failure. 'Notice error' on any failure to fetch data from the Science Data Service or failure to update sensor's F1 score, with alerting on high error percentage.

Each run of validations will push custom New Relic metrics on number of stations and sensors in each status category (up, down, invalid) and event when a station goes down. From this:
* A custom dashboard in New Relic to provide an overview; and
* Slack notifications to the Cam Ops team on station outages and daily overview reports.

## Costs estimates

Lambda (collection):  $5.50/month
* Invocations a day: 1440 (= 60 * 24)
* Running time: ~30 seconds per invocation

Lambda (ingestion):  $0.92/month
* Invocations a day: 1440
* Running time: ~5 second per invocation (probably a _large_ overestimate)

Lambda (spot-report-view-builder): $70/month
* Invocations a day: 1440 minutes/day * 1500 (estimate assuming each wind-station is near 2 spots) = 2,160,000 / day = 67,000,000 / month
* Running time: 200ms (guessed, will be updating only the wind information)

SNS/SQS:
* S3 push events per day: 1440, generating roughly 45,000 messages through SNS/SQS per month. Approx price: zero.
* live-wind updated topic: 67,000,000 messages / month: 33.00 USD

S3:
* Number S3 files written per day: 1440
* Size of each file: approx 2.4MB (based of storing pretty-printed JSON files).
* Growth per month: ~100GB / month, i.e. ~$2.00 / month cost growth (assuming indefinite storage)

PostgreSQL:
* Rows written per day: (1440 (minutes a day) * ~800 = 1,152,000
* Reads per day: Unclear, but long term with live-winds on iOS, Android and Web, and the most heavily trafficked spots covered by live-wind stations, we could expect it to be similar to the KBYG `/spots/forecasts/winds` endpoint, of ~5000rpm.

## What do we know already?

### Understanding the live-wind data

* The live wind data is produced by taking average readings from WeatherLink every 1 minute. The sample period for this average will be 2 minutes as this is the most suitable data that WeatherLink provides. To generate this average WeatherLink take a wind reading from a given sensor every 2 seconds for 2 minutes and then create an average for this window.
* To understand how the sample period works here is an example of how the 2 minute sample period may work. When we take a reading at 12:00 it will be using the averages produced by WeatherLink from approximately 11:58 - 12:00. When we make the next call at 12:01 it will now be using the averages from wind collected between 11:59 - 12:01.

## live-wind ingestion job diagram

![Live Wind PostgreSQL ingestion](./images/live-wind-ingestion.png "Live Wind PostgreSQL ingestion")
