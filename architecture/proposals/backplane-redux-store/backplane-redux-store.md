# Backplane Redux Store Merging

## What are we doing?

We are changing backplane to manage the user and meter state in a more global fashion. Previously backplane was responsible for fetching this data and returning it to the consuming application, so that it could be managed by that application. These changes aim to make backplane responsible for orchestration and management of the user and meter data, in order to prevent duplication and divergence of the state. The changes would make Backplane manage redux state that exists under the `backplane` key, which would contain user and meter data. Upon client hydration, the backplane store would be merged with the store of the consuming application, and interacting with the user and meter data would be done via accessing the backplane managed part of the store.

## What problem are we solving?

The broader problem here is that as a result of the rate limiting initiative we have hit a point where there is a lot of state coordination that is being duplicated (and has the possibility of divergence) related to the user and their metering state. The rate limiting logic has a good amount of complexity and affects both the metering state and the user's perceived premium state (from the perspective of the consuming application). 

We are also having to design a way for users to perform inline registration and sign-in, without having their experience significantly impeded by having to navigate away or risk being taken into different product flows. We are trying to eliminate as many impediments to leading the user through the `rate-limiting -> premium` flow.


## What impact does this have on the business?

This change allows us to iterate more easily on rate-limiting, and deliver a better experience for users that do fall within the rate-limited cohort. It enables us to perform actions like inline sign-in/create-account and dynamic updates to the metering and user state without leaving the page. It also allows us to more easily manage the complications to the user state that have been brought about by rate-limiting. Assuming rate-limiting is successful in its goals, these changes would allows us to more easily bring different parts of the site into rate-limiting without having to write a whole bunch of duplicated logic in different applications.

## What is the solution?

Rather than try to manage and reconcile potentially diverging states across front-end applications, leveraging backplane gives us the flexibility to pass the logic off to backplane and allow it to do what it has always done (and more):

- retrieve and hydrate the user's entitlements, settings, details, and favorites (now via redux store hydration/merging)
- manage the metering state (new)
- manage the user's perceived premium state
- Dynamically update as the user's state changes (rather than requiring a full page reload)

The approach here is inspired by the way we hydrate backplane state into the window and the store, but in this case the idea would be to remove the management of user/metering state from front-ends and give that responsibility to backplane. We also need to keep in mind that we should provide the tools via `@surfline/web-common` (if we deem it necessary we can move to a new package in the future) for front-ends to be able to alter the state in ways that are meaningful to the way the product operates.

This does however have implications. Since we're centralizing some state into backplane, this means that changes to the backplane store could have implications on other applications. We would be forced to abide to more backwards compatibility and contract adherence than before. I think in this case the trade-off is warranted. Consuming applications should be changed so that they do not have to be immediately aware of the business logic that drives the centralized meter and user state, but instead can be dumb consumers of the state. A good use of selectors and re-usable functions can enable this. We would essentially be establishing a contract that is driven by backplane for a certain subset of state (user/metering). If applications need to augment this state, they can use their own part of the store to perform operations and then create selectors to meaningfully merge the two parts of state to be consumed. If this approach does become too heavy or complicated, then we could look to design a more flexible way to alter to combined part of the store.

Here's the flow for an app that wants to have access to the backplane state:

1. Request
2. Consuming app receives request
3. Consuming app makes a request to backplane for state/Javascript/HTML
4. app creates a store with dummy backplane reducer added 
5. app performs server side render 
6. app hydrates it's own client side first
7. client side code injects the store into the window 
8. backplane executes its client side code 
9. backplane sees the injected store
10. backplane uses this store and injects its reducer into it
11. merged store

We will also need to add stricter unit tests that ensure that the backplane contract is not broken by changes made in the application.

## How does this solve the problem?

This solves the problem by creating and orchestration layer to bear the brunt of all the complexity that is inherent to rate limiting and the broad sweeping product changes. It allows us to design the front-ends that are impacted by rate-limiting to be rather _dumb_ with regards to how rate-limiting actually works. Instead the front-ends can be built to react to the data they are given, and display the correct UI, without having to actually know how rate-limiting works or coordinating the rate-limiting logic. It will help us keep our front-ends _micro_, and leverage backplane as an orchestration layer for the state.


## What do we know already?

- We need to support inline sign in
- We need to support inline registration
- We will have to support both KBYG and Homepage in the first iteration (surf-cams page exists in homepage)
- Account section (create-account/sign-in) will have to be aware of meter state to be able to display consistent product messaging
- We have to support modals that could technically be rendered anywhere (similar to open house)

## Alternative Solutions

#### State coordination via `@surfline/web-common`

An alternative solution would be to move all of the user and meter state coordinating logic to `@surfline/web-common`, and letting the clients use that logic to manage the state. 

I don't think this solution makes sense because Backplane is already one of the core places for getting user state (we rely on it to fetch user details, favorites, entitlements, settings) and now also meter state. I think there is a layer that really only backplane needs to be aware of (fetching user info, fetching meter, creating meter) and it makes sense to just keep that there. I think moving all related meter and user state logic to common and having the clients implement them individually would create a lot more code duplication than necessary and would be harder to update across applications.

I think it would be simpler to establish a contract related to user/meter data, and then ensure we abide by that contract than try to manage the code in many different places.

## Other
Architecture PR for reference: https://github.com/Surfline/wavetrak-services/pull/3587

Proposed code changes:
https://github.com/Surfline/surfline-web/pull/4194
https://github.com/Surfline/surfline-web/pull/4193
https://github.com/Surfline/surfline-web/pull/4189
