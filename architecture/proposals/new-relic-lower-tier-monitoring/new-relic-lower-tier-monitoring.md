# New Relic Lower Tier Monitoring

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [What Are We Doing?](#what-are-we-doing)
- [What Problem Are We Solving?](#what-problem-are-we-solving)
- [What Impact Does This Have on the Business?](#what-impact-does-this-have-on-the-business)
- [What Is the Solution?](#what-is-the-solution)
  - [tl;dr](#tldr)
  - [Migrate lower tier applications from New Relic Lite to the main account](#migrate-lower-tier-applications-from-new-relic-lite-to-the-main-account)
    - [Naming conventions](#naming-conventions)
      - [prod tier:](#prod-tier)
      - [lower tiers:](#lower-tiers)
    - [Required service updates](#required-service-updates)
    - [Redeploying services](#redeploying-services)
  - [AWS Lambda functions](#aws-lambda-functions)
    - [Terraform-deployed functions](#terraform-deployed-functions)
    - [Serverless-Framework-deployed functions](#serverless-framework-deployed-functions)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## What Are We Doing?

We are moving the lower tier services monitoring from our New Relic Lite account to the primary New Relic account.

## What Problem Are We Solving?

We are reducing the operational friction and the cost associated with having two separate New Relic accounts for different application tiers.

Our New Relic contract now allows us to monitor all application tiers in the main account. As a result, we can deprecate the New Relic Lite account that we currently use to monitor applications in lower tiers. This will confer a number of benefits:

- Centralized monitoring location
- Eliminates the extra set of shell credentials that terraform requires to provision New Relic infra in the lower tiers
- Expands the number of available third-party integrations in lower tiers
- Reduces confusion regarding the differing accounts
- Reduces cost

## What Impact Does This Have on the Business?

When lower-tier monitoring is difficult and confusing, it gets neglected in development iteration cycles. Centralizing the New Relic reporting will make it easier for developers to implement lower-tier testing. This should incentivize more rigorous lower-tier testing, which will improve the operational practices around traceability and monitoring. Resultantly, fewer bugs should make it to production. When bugs *do* get to production, improved alerting will allow them to be detected and resolved faster.

## What Is the Solution?

### tl;dr
For most services, you'll just need to follow these steps for your service to report to New Relic in sandbox or staging:
1. Ensure your application is configured to append the environment to the end of all New Relic app name. For Node/React, this is usually configured in `newrelic.js`.
1. Redeploy your application to `sandbox`/`staging`.
1. Check New Relic and confirm that your service shows up in APM.
1. Add alerting if needed.

### Migrate lower tier applications from New Relic Lite to the main account

The migration process itself is relatively simple: it requires updating the `NEW_RELIC_LICENSE_KEY` with the main account information, and redeploying the service. However, we will need to establish some conventions and expectations to ensure consistency. This will entail:

1. Establishing naming conventions for services in the lower tiers.
1. Defining monitoring expectations in lower tiers.
1. Updating the services code to reflect changes to the reported APM service names and the terraform naming changes.
1. Redeploying the affected services.

#### Naming conventions

##### prod tier: 

In order to avoid disrupting the `prod` New Relic reporting, we should leave all the names of production services as-is. I.e., these names would stay the same.

favorites-api
|                      | Old Name                        | New Name |
| ---                  | ---                             | ---      |
| APM                  | `Surfline Services - Favorites` | (same)   |
| Alert Policy         | `Favorites API`                 | (same)   |
| Alert Channel        | `Favorites API`                 | (same)   |
| Alert Condition Name | `Error percentage (High)`       | (same)   |


##### lower tiers:
Any lower tier entity should end with a dash and the environment name (`sandbox` or `staging`.). I.e:
  - The APM name for the `favorites-service` in `sandbox` would be `Surfline Services - Favorites - (sandbox)` 
  - The name of the `favorites-service` alert policy in `sandbox` would be `Favorites API - (sandbox)`
  - The name of the `favorites-service` alert channel in `sandbox` would be `Favorites API - (sandbox)`
  - The name of the `favorites-service` Apdex alert in `sandbox` would be `Manual - Error percentage (High) - (sandbox)`

favorites-api

|                      | Old Name                        | New Name                                    |
| ---                  | ---                             | ---                                         |
| APM                  | `Surfline Services - Favorites` | `Surfline Services - Favorites - (sandbox)` |
| Alert Policy         | `Favorites API`                 | `Favorites API - (sandbox)`                 |
| Alert Channel        | `Favorites API`                 | `Favorites API - (sandbox)`                 |
| Alert Condition Name | `Error percentage (High)`       | `Error percentage (High) - (sandbox)`       |


#### Required service updates

Each Wavetrak service configures its own APM agent, which runs as a process in the service container. The service code is where the New Relic APM reporting details are configured. We will need to update this configuration so that it appends the environment to the APM name:

```js
let newRelicAppName = 'Surfline Spots API';
if (process.env.NODE_ENV !== 'production') {
  newRelicAppName += ` (${process.env.NODE_ENV})`;
}
```
Most of the services should already be configured like the above example. None of the `prod` services will need to be redeployed. By any lower-tier services that you wish to monitor will need to be redeployed.

#### Redeploying services

1. Wavetrak services report to the New Relic account that corresponds with the API key stored in the `/{sandbox/staging}/common/NEWRELIC_API_KEY` parameter store value. The Infrastructure Squad will update this value to the same one used for the main New Relic account. The value is shared by all the services in the monorepo. So this value updates only needs to be made once for each respective tier's parameter store variable.

1. Once the update is made, the respective teams/developers can elect to re-deploy the service in lower tiers if they wish to enable monitoring.

### AWS Lambda functions

There are two primary ways we provision AWS Lambda functions at Wavetrak: using Terraform, or using the Serverless Framework. The New Relic monitoring configuration varies slightly depending on which deployment method is used.

Going forward, the standard will be to use Serverless framework for Lambda deployments. But since most of the legacy Lambda functions are still deployed via Terraform, this document will outline how to enable lower-tier monitoring for both cases.

#### Terraform-deployed functions

Terraform-deployed Lambda functions use the [layers](https://github.com/Surfline/wavetrak-infrastructure/blob/master/terraform/modules/aws/lambda/main.tf#L37) attribute of the `aws_lambda_function` resource to specify the ARN corresponding to the New Relic Lambda layer version. This is then configured via [environment variables](https://github.com/Surfline/wavetrak-services/blob/master/services/notifications-service/functions/notification-processor/infra/main.tf#L40-L45). The `NEW_RELIC_ACCOUNT_ID` controls which New Relic account the Lambda layer will report to.

To enable lower-tier reporting, you'll need to update this variable value to the main New Relic account id. 

#### Serverless-Framework-deployed functions

The Serverless Framework uses it's `serverless.yml` file to control the New Relic configuration. When creating new Lambda functions, you'll have to do the following to enable monitoring:

1. Include the `serverless-newrelic-lambda-layers` in the [plugins](https://github.com/Surfline/wavetrak-services/blob/master/functions/ecs-failed-task-monitor/serverless.yml#L26-L27) section.
2. Update the `accountId` and the `apiKey` [attributes](https://github.com/Surfline/wavetrak-services/blob/master/functions/ecs-failed-task-monitor/serverless.yml#L36-L37) to point to the main New Relic account.

Existing Lambda functions with the above configuration will just require a redeploy in order to redirect the New Relic metrics to the main account.