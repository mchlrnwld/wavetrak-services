# Airflow Task Integration Tests

## What are we doing?

Adding an integration test framework and pattern suitable for all Airflow tasks.

## What problem are we solving?

Validation for Airflow tasks is generally a long process. Most often this requires a wait-and-see approach by deploying the job to lower tiers. By establishing a framework to write integration tests, we can greatly reduce the development time for Airflow tasks. It allows us to test the code end to end vs rely on each unit test. The number of bugs introduced to production should also decrease.

## What impact does this have on the business?

Integration testing will greatly reduce the number of bugs introduced into production. Allowing us to test the task does what we expect it to do vs our unit tests which just verify a given function works as expected. Increasing the reliability of our Airflow jobs and expanding the test coverage of our Airflow tasks. The customer experience will be improved as the risk of delayed or incorrect forecast data is decreased. This will also reduce development time.

## What is the solution?

We will leverage the power of existing testing frameworks, such as `pytest` or `mocha`, in combination with `docker-compose`. This will allow us to implement tests quickly using existing patterns. At the same time, `docker-compose` gives us the flexibility to emulate services that aren't easy to mock. This pattern will be compatible for _all_ runtimes.

The tests will be run in CI/CD using the same `ci.json` contract that we have used for other services such as the [science-data-service](https://github.com/Surfline/wavetrak-services/blob/master/services/science-data-service/ci.json). The [test](https://github.com/Surfline/wavetrak-services/blob/334e09937e50c0e5ab47c50e3eeb4cabbf01607b/scripts/deploy-service-to-ecs.sh#L119) field of `ci.json` will determine how to execute the integration tests.

A proof of concept implementation is included in the PR for the `insert-buoy-data-into-ndbc` Airflow task.

## How does this solve the problem?

Testing the entire Airflow task will allow us to validate the logic without deploying to lower tiers. Tasks that write to a database, for example, are difficult validate with unit tests that only cover a single function. Using frameworks like `pytest`, tests can be written around the application's entrypoint (i.e. `main`). We can then validate that the correct data was inserted into the database.

`docker-compose.yml` will serve as the starting point to run all integration tests. Test framework plugins can then be used to mock a service. See [testing-resources-for-python](#testing-resources-for-python) for examples. However, a plugin may not always exist. In this case, additional images can be added to `docker-compose.yml` to emulate a service. See example of this on the [science-data-service](https://github.com/Surfline/wavetrak-services/blob/master/services/science-data-service/docker-compose.test.yml).

And an extra advantage to using unit test frameworks is that we can use code coverage tools. For example, [pytest-cov](https://pypi.org/project/pytest-cov/) will be used for Python tasks. This integrates with [codecov](https://github.com/codecov/example-python#pytest) to produce code coverage reports.

In all, this will help us avoid outages that we have had with [Forecast Platform tasks](https://wavetrak.atlassian.net/wiki/spaces/IR/pages/2015428889/2021-06-01+-+Missing+GEFS+Wave+Probability+Data).

## What are the alternatives?

- Use the unit test frameworks without `docker-compose`:

  - Pros:
    - Can use existing docker images to more quickly implement unit tests. Docker multi-stage builds could be used to isolate tests from the rest of the docker image.
    - Less overhead and fewer files to maintain (ex: only a single Dockerfile).
  - Cons:
    - Solid risk that there is not a plugin for an external service the Airflow task uses. Writing custom mocks could be considerably time consuming.
    - Currently unit tests for Airflow tests are executed during the docker build process. This happens in the same docker image that is used during runtime. As we transition to Github Actions for CI/CD this is likely a pattern we will move away from.

- Test the entire Airflow pipeline end-to-end:

  - Pros:
    - The Airflow job can be tested in its entirety from the first task to the last task.
    - Can run the entire Airflow pipeline without deploying to `dev` environment.
  - Cons:

    - End-to-end pipeline testing wouldn't guarantee that each task has worked properly. For example, several of the tasks in the Forecast Platform write to a database. The `write-to-db` task is one part of a multi-stage Airflow pipeline. Validating the data in the database after the entire job has finished would be time consuming and error prone. It is more effective to write integration tests for the task.
    - For certain Airflow jobs this could take a long time to run. This could greatly increase the deployment cycles.
    - Our current Airflow version is far behind the latest, so theres a lot of risk in implementing code for an old version of Airflow.
    - Newer versions of Airflow likely have better DAG validation and testing.

## What do we know already?

- Many of the processing tasks in the Forecast Platform, for example, take an upward of an hour to complete. For those tasks, we will have to modify the logic so the task can process a smaller set of data.
- Tasks that interact with a database (i.e. `Postgres`) will have to manage migrations. This can be done several different ways:

  1.  Volume attachment from the monorepo (similar to how `scripts/wait-for-it.sh` is [used](https://github.com/Surfline/wavetrak-services/blob/d7396db18b2a612ef2f0b8585a9ae3b87ca6a7f5/services/science-data-service/docker-compose.integration.yml#L10)). This pattern is used in the proof of concept for `insert-buoy-from-ndbc` Airflow task.
  2.  A symlink to the migrations in the monorepo.
  3.  Store the migrations in S3 and pull the migrations before running the tests. In this pattern the migrations should be checked into git for each Airflow task.

- Additional functionality will need to be added to the `deploy-airflow-job` script to run `docker-compose`. The integration tests should be run on every deploy and block a deploy if the tests fail.

# Q & A

- Will `docker-compose` be used for each Airflow task?
  - There should be a `docker-compose.yml` for every Airflow task that implements integration tests. Framework plugins are strongly recommended if they exist. If not, additional images can be added to `docker-compose.yml`. In all cases, `docker-compose.yml` should be the starting point for Airflow integration tests. See the proof of concept for the `insert-buoy-data-from-ndbc` task included in this PR.
- How will environment variables be managed?
  - Environment variables should be added to a `.env.test` file. The `.env.test` file should be referenced in the `env_file` section of `docker-compose.yml`.
- How will other runtimes such as Node.js or Go be supported?
  - Testing the entrypoint of an Airflow task using unit test frameworks should be easily adaptable for other languages. For example, using `mocha` with Node.js or `go test` with go. `docker-compose` provides additional flexibility to mock a service if a plugin does not exist. See the [testing resources section for Node.js](#testing-resources-for-node.js) for reference.
- Will the integration tests be added to every Airflow task?
  - The goal is to have as much test coverage for the Airflow tasks as possible. For existing jobs, this up to the discretion of the team/owner of the Airflow job. For new Airflow jobs, integration testing should be added wherever possible.
- Will it be possible to run all integration tests locally?
  - Test framework plugins should be used wherever possible, see [testing-resources-for-python](#testing-resources-for-python) for examples. In this case, tests should be able to run locally and in docker. When there isn't a plugin available, then additional images can be added `docker-compose.yml`. In the latter case, running locally wouldn't be possible.

## Links

### Docker

- [docker-compose](https://docs.docker.com/compose/)

### Testing Resources for Python

#### Framework

- [pytest](https://pytest.org/)

#### Plugins

- [pytest-postgresql](https://github.com/ClearcodeHQ/pytest-postgresql)
- [moto](https://github.com/spulec/moto)
- [pytest-cov](https://github.com/pytest-dev/pytest-cov)

### Testing Resources for Node.js

#### Framework

- [mocha](https://mochajs.org/)
- [chai](https://www.chaijs.com/)

#### Plugins

- [aws-sdk-mock](https://www.npmjs.com/package/aws-sdk-mock)
- [mongo-mock](https://www.npmjs.com/package/mongo-mock)
