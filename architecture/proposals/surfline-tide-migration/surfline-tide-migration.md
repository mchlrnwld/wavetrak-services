# Surfline Tide Service Migration

## What are we doing?

Migrating the Surfline product to use the [shared tide service](https://github.com/Surfline/wavetrak-services/tree/master/services/tide-service).

## What problem are we solving?

The [existing tide service](https://github.com/Surfline/science-forecast-api/tree/master/services/tide-service) is built upon legacy code that is not actively maintained. The data is static and creating new tide data is a manual process. This must be repeated every year. There are also data integrity issues from the legacy API that are happening [_right now_](https://wavetrak.atlassian.net/browse/FE-327). The DST logic in the legacy API assumes U.S timezones and is causing incorrect data to be shown outside the U.S.. This is negatively impacting the customer experience.

In addition, there are 2 separate tide services used for SL and MSW which is wasteful. The harmonics file in the shared tide service are significantly improved, especially in the UK. This means an improved customer experience overall for tides.

## What impact does this have on the business?

The latest tide data will always be available for each POI. The shared tide service will owned by the MSW team, which will greatly increase the reliability of tide data. A single tide service also means less engineering effort and maintenance. New tide stations will be curated by the Locations team and the quality of the data will be improved, which should have positive impacts on user retention and engagement.

## What is the solution?

The Surfline product will switch to using the shared tide service. Tide data is generated at request time with the shared tide service. This means data for any date in the future will always be available and won't be manually generated.

The plan is as follows:

1. Create a script to update the mongodb `Spots` collection for new tide stations:

   - The list of tides will be given in as a CSV from the Locations team. The CSV should contain columns for `spotId` and `tideStationName`. This will also us to map new tide stations to spots. This is for _all_ spots.
   - Add the new tide stations as a `sharedTideStation` field.

2. Add a `sharedTideStation` parameter in the KBYG product API to switch between shared tide service and legacy tide service:

   - The [/getDataJSON.php](https://github.com/Surfline/wavetrak-services/tree/master/services/tide-service#tide-data) is the primary endpoint that will be used to query tide data from the shared tide service:
     - The API contract between the shared tide service and KBYG product API contract is different. Functions will need to be added to reformat the new tide data to match the KBYG API contract.
     - If a spot in the `Spots` collection does not have the `sharedTideStation` field set, then the closest tide station will be chosen. MSW team is adding functionality to the shared tide service to support this.
   - We should use the cache from `services-common` to cache tide data from the shared tide service like we are already doing for the legacy API. Tide data only changes daily which makes it a natural fit for caching.

3. Add a feature flag to the UI to toggle between the legacy tide service and the shared tide service. The Locations team will validate the new tide data. The data will need to be validated in production. `Split.io` allows us to use the toggle in production without exposing the feature to users.
4. Add a new dropdown in the POI CMS called `New Tide Station`. This modifies the `sharedTideStation` field in the `Spots` collection. The stations listed in this dropdown menu will come from the endpoint in the shared tide service that [lists all stations](https://wavetrak.atlassian.net/browse/MBS-428).
5. MSW team will add functionality so that the Locations team can add new tide stations to the shared tide service. These tide stations will show up under the `New Tide Station` dropdown on the POI CMS (this dropdown will be renamed to `Tide Station` in a later step). These stations will show automatically in the dropdown menu as they will come from the aforementioned [list all stations endpoint](https://wavetrak.atlassian.net/browse/MBS-428).
6. The locations team will now be able to adjust the tide stations that are associated with POIs. The tide stations in the `sharedTideStation` field in the `Spots` collection may be changed by the locations team during this process. This will happen independently by the locations team without involvement from the forecast engineering team.
7. After the new tide stations have been validated, temporary logic used for tide service migration will be removed:

   - Remove `sharedTideService` parameter from the KBYG product API and default to new logic. Logic querying legacy tide service removed.
   - Remove feature flag and reference to `sharedTideService` query parameter from the UI.
   - Remove the old tide station dropdown from the CMS. Change the label on the `New Tide Station` dropdown to `Tide Station`.
   - Replace the `tideStation` field with `sharedTideStation` value and remove `sharedTideStation` property.

8. Tear down [legacy infrastructure](https://github.com/Surfline/science-forecast-api/tree/master/services/tide-service) for tide data:

   - Code in github moved to `deprecated-repos`.
   - Lambda function `python_get_tide` and associated API gateway from all environments.
   - DynamoDB tables `Tide_Service` and `Tide_Location_Meta_Data` from all environments.

## How does this solve the problem?

Tide data will no longer be manually generated. Tide information will always be available. The shared tide service will be under the ownership of the MSW team directly, which will greatly increase the reliability of the service. The shared tide service is time zone agnostic and provides more accurate data. The process of adding new tide stations will also be easier.

## What are the alternatives?

- Stabilize the existing tide service:
  - Pros:
    - Tide stations dont have to be regenerated if the tide stations in the shared tide service are not satisfactory.
    - Existing infrastructure can remain in place.
  - Cons:
    - Tide data will have to be generated every year. A manual process that is difficult and time consuming to maintain.
    - Fixing the time zone DST bug in outdated Perl and C code could require just as much engineering effort as doing the migration.
    - Adding new tide stations is a difficult process.
    - Legacy code base will have to be maintained which will become more difficult as time goes on.
    - We continue to maintain 2 completely independent systems solving the same problem. This goes against our engineering technical values.

## What do we know already?

- `min/max` data from the `Tide_Metadata` table will need to be available from the shared tide service. This data is critical for graphing tides on Surfline. This will be added to the `/getDataJSON.php` in the shared tide service.
- Tide data only changes daily which makes it a natural fit for caching. Caching from the `services-common` package is used for the legacy tide service. The same package should be used for the shared tide service on the KBYG product API. The TTL could be set to 24 hours.
- The shared tide service has been load tested for MSW traffic which is similar to Surfline traffic. The MSW team will determine if the service needs to be load tested for Surfline specifically.

# Q & A

Will there be any noticeable downtime for users?

- There should no downtime for users. The transition should be seamless and the data will be validated before it is rolled out to users.

## Links

- [Shared tide service](https://github.com/Surfline/wavetrak-services/tree/master/services/tide-service)
- [Legacy tide service](https://github.com/Surfline/science-forecast-api/tree/master/services/tide-service)
