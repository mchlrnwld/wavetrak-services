# ColdFusion 3rd Party API Replacement

## What are we doing?

To get off of ColdFusion, we want to explore options for replacing the existing 3rd party API that runs on ColdFusion. Existing API documentation can be found here: <http://e.cdn-surfline.com/api/dawnpatrol-api-access-information.html>.

## What problem are we solving?

We have an existing contract with 3rd party clients including Nixon, Dawn Patrol, Rip Curl, and True Surf to provide spot and forecast data for their apps. The existing APIs that we've provided them are powered by ColdFusion with LOLA data. We no longer want to support ColdFusion or LOLA but we may need to continue providing APIs to our partners.

## What impact does this have on the business?

The impact of getting off of ColdFusion and LOLA have been stated numerous times already. Better data leads to higher engagement and conversion. One forecast system leads to less operational overhead and allows our engineering teams to focus on product development.

If we shut down these APIs without providing a replacement for our partners we run the risk of burning bridges that could close doors for future opportunities. Additionally, these partnerships can help us acquire and convert users, and provide additional revenue streams.

## What is the solution?

Long term we will want to properly build a third party API platform that includes:

- Authentication/authorization and management of third party API tokens
- Data contract management and testing
- Rate limiting
- Monitoring
- Documentation

Even though this list is not the current priority, replacing the existing API involves enough technical overhaul that we should build the immediate solution with the longterm solution in mind.

In the short term, we want to replace the existing API contracts with the least amount of development effort and operational overhead. We will try to use existing API endpoints to fulfill client requirements. These endpoints will be proxied through the Platform Proxy and be authenticated using basic authentication. This will depend on our clients moving over to new contracts.

### API Requirements

Here's a quick overview of the API requirements. I will break these down below and describe the relevant endpoints.

- Location hierarchy.
  - Region > Subregion > Spot (ID, name, location)
- 3 day forecasts with units selection:
  - Tides
  - Weather
  - Surf
  - Swells
  - Water Temperature

### Location Hierarchy

The following endpoints will provide the location hierarchy:

- http://spots-api.prod.surfline.com/admin/spots?select=name,location -> https://platform.surfline.com/spots
- http://spots-api.prod.surfline.com/admin/subregions -> https://platform.surfline.com/subregions
- http://spots-api.prod.surfline.com/admin/regions -> https://platform.surfline.com/regions

Clients can use jobs to asynchronously query these endpoints and build up the hierarchies. We will want to rate limit requests to the spots endpoint or cache its response to prevent us from getting DDoSed. We can rate limit by client ID in nginx at the Platform Proxy (https://theawesomegarage.com/blog/limit-bandwidth-and-requests-to-your-nginx-server-with-rate_limit-and-limit_req).

An alternative to this is to build the hierarchy as a flat file (either through a Lambda that subscribes to spot updates or an Airflow job that updates the file regularly) and serve it statically through S3.

### Forecasts

The following endpoints will provide forecast data:

- http://kbyg-api.prod.surfline.com/spots/reports?spotId=[spotId] -> https://platform.surfline.com/spots/reports?spotId=[spotId]
  - Provides current water temperature.
  - We will pass a query parameter to indicate a stripped response contract to prevent 3rd parties from using data we don't want them to use.
- http://kbyg-api.prod.surfline.com/spots/forecasts/tides?spotId=[spotId]&days=3 -> https://platform.surfline.com/spots/forecasts/tides?spotId=[spotId]
- http://kbyg-api.prod.surfline.com/spots/forecasts/wave?spotId=[spotId]&days=3 -> https://platform.surfline.com/spots/forecasts/wave?spotId=[spotId]
- http://kbyg-api.prod.surfline.com/spots/forecasts/weather?spotId=[spotId]&days=3 -> https://platform.surfline.com/spots/forecasts/weather?spotId=[spotId]
- http://kbyg-api.prod.surfline.com/spots/forecasts/wind?spotId=[spotId]&days=3 -> https://platform.surfline.com/spots/forecasts/wind?spotId=[spotId]

The endpoints currently only support unit selection through user settings and geotargeting. To support unit selection for our clients we will need to add query parameters.

### Contract Validation

We have end to end component tests that validate contracts for microservices involved in fulfilling these APIs. But to ensure the contract holds all the way through the Platform Proxy we can add synthetics to validate the contract end to end.

### Estimation

Estimates in story points per task (or grouping of tasks).

- 5 - Add APIs to Platform Proxy with basic auth.
- 5 - Add query parameters for units.
- 2 - Strip response contract for spots/reports endpoint.
- 5 - Synthetics to validate end to end contract.
- 3 - Rate limit spots endpoint.
- 3 - Documentation.

Overall effort 23 story points (depending on location hierarchy security solution), which is ~2 sprints for a single engineer, or a S.

### To Do

- How do we setup documentation?

## How does this solve the problem?

Though we are not fulfilling the existing API contracts and will require our clients to move over to new APIs, this will fulfill the requirements the existing contracts have while removing ColdFusion and LOLA as tech debt, while maintaining our partnerships.

## What are the alternatives?

### Fulfill Existing Contracts

The easiest way of doing this is likely to continue the work on the `/kbyg/classic/forecasts` endpoint that an intern previously built and mounting this to a proxy to replace the existing endpoint. My preference is not to do this for the following reasons:

- The existing contract is pretty gnarly and the work to transform our data to fulfill the contract will be nontrivial. The likelihood of bugs arising from misunderstanding of the contract on our end is high.
- Supporting this endpoint and contract requires more operational overhead than using existing endpoints.

### Location Hierarchy Security

Instead of rate limiting with nginx to prevent DDoS attacks, we can either rate limit through Cloudflare or statically serve a flat file.

#### Cloudflare Rate Limiting

Cloudflare rate limiting is applied to client IPs does not support rate limiting by basic auth credentials.

### Build Longterm Vision for 3rd Party APIs

The effort for doing this is massive and would derail our current roadmap. The risk is also high as we haven't properly invested in understanding the business requirements and product vision.
