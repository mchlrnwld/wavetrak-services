# Implement NeverBounce for Email Verification

## What are we doing?

Adding email verification functionality to the `user-service` using a third party vendor - [NeverBounce](https://neverbounce.com/). NeverBounce is a real-time email verification service that identifies invalid emails and helps cleaning our email lists.

## What problem are we solving?

Currently we are not able to verify the authenticity of our customer emails which cause below problems:

 - A user can create a fake email and can get access to the product. With rate limiting rolled out they can get the Premium experience as well which is not ideal.
 - Negatively impacts our marketing campaigns and won't be able to effectively reach our customers regarding new features or updates. 
 - A high bounce rate negatively affects the trust relationship between email providers and our domain name, and ultimately inhibits our ability to communicate with a user through email.
 - Degraded customer experience with regards to email typos and fraudulent email addresses (login issues) and elevate the number of CS tickets. 

We do have a verify email flow but it doesn't prevent the user from accessing the product.

## What impact does this have on the business?

Adopting a real-time email verification and list cleaning service such as [NeverBounce](https://neverbounce.com/why-neverbounce) ensures us to reach customers by increasing the delivery of the emails to the inbox. 
NeverBounce ensures improved email quality, low bounce rates and improves the [email sender reputation](https://neverbounce.com/blog/email-sender-reputation).


## What is the solution?

NeverBounce provides 3 main features:
 - Real-Time Email verification
 - Automated List Cleaning
 - Bulk Email List Cleaning

**Implemented on**: Web and Native Apps for Surfline, FishTrack and Buoyweather

**Applicable to**: New accounts only

### [Real-Time Email verification](https://neverbounce.com/verify)

This feature would be most suited for our use case. We can verify and validate customer emails directly at the point of entry using this feature. It offers below connection methods:
### Custom API (Server Side implementation) - Recommended

Implement the NeverBounce [NodeJS api wrapper](https://github.com/NeverBounce/NeverBounceApi-NodeJS) in the `user-service` to call their backend api to verify the authenticity of a customer email.

 First create a `Custom Integration App` in the NeverBounce Account. After creation, we can get the app's API key from the overview page. This key will be used to authenticate our requests to NeverBounce API.
 #### User Service changes
 
 Modify the `POST /user` endpoint in the `user-service` to verify the authenticity of the email with NeverBounce.
  - Check if the email exists in `UserInfo` collection 
  - If it does not exist in `UserInfo` collection , check if the email pass the Neverbounce validation

```javascript
    try {
      const {
        body: { email },
      } = req;
      const userDoc = await getUserByEmail(email);
      if (userDoc) {
        // Email exists. Do not proceed.
        return;
      }
      // Initialize NeverBounce client
      const client = new NeverBounce({ apiKey: 'private_API_KEY' });
      // Verify an email
      const result = await client.single.check(
          email, 
          true, // Include additional address info in response (default: false)
          true, // Include account credit info in response (default: false)
          10, // The maximum time in seconds we should try to verify the address
          );
       const emailResult = verificationResult.response.result;
       let okToSend = true;
       if (emailResult === 'invalid' || emailResult === 'disposable') okToSend = false;
       return res.status(200).json({ email, okToSend });
    } catch (error) {
      // If there are any exceptions then catch and log the error but proceed with account creation
    }
    // Rest of the User creation logic
```
NeverBounce Response:

```json
"verificationResult": {
        "response": {
            "status": "success",
            "result": "invalid",
            "flags": [
                "smtp_connectable",
                "has_dns",
                "has_dns_mx"
            ],
            "suggested_correction": "",
            "address_info": {
                "original_email": "ajith-invalid-email@invalid.com",
                "normalized_email": "ajith-invalid-email@invalid.com",
                "addr": "ajith-invalid-email",
                "alias": "",
                "host": "invalid.com",
                "fqdn": "invalid.com",
                "domain": "invalid",
                "subdomain": "",
                "tld": "com"
            },
            "credits_info": {
                "paid_credits_used": 1,
                "free_credits_used": 0,
                "paid_credits_remaining": 963,
                "free_credits_remaining": 0
            },
            "execution_time": 851
        }
```
The `result` parameter in the response indicates the status of the email. 

##### Result Codes

![Alt text](../images/neverbounce/neverbounce-result.png?raw=true "NeverBounce Result Codes")

NeverBounce recommends to allow `valid`, `catchall` (accept all / unverifiable), and `unknown` emails to proceed, while blocking only `disposable` and `invalid` emails at the point of entry in web-forms.

NeverBounce recommends to use a timeout of 10 seconds while verifying the email. See below quote from their website.

>Additionally, we’ve found that with some smaller email hosts it is not uncommon to take several seconds (sometimes tens of seconds) to respond. It’s for this reason that we suggest enforcing a client side timeout of approximately 10 seconds in your HTTP request library (ie: Curl, Guzzle, Httparty, etc…) and treat any timeouts as unknowns.

Read more about the [Point of Entry Use](https://developers.neverbounce.com/docs/verifying-an-email#point-of-entry-use).

##### Error Handling

If there is a failure in NeverBounce side we can catch those errors and log them to NewRelic. But we won't block account creation in such scenarios. 
https://developers.neverbounce.com/v4.0/reference#error-handling

 #### Web/App changes

  - Registration Funnel
  - Upgrade/Promotional/Gift Funnels
  - Inline Registration Form in Rate Limiting paywalls
  - Native Apps

The apps would be modified to recognize the response from `POST /user` (i.e `response.okToSend === false`). If it's an invalid email then show an error message in the UI. 
 - Modify [CreateAccountForm](https://github.com/Surfline/quiver/blob/89d68037b8b91cd87c6099e327a4fe612e85e631/react/src/CreateAccountForm/CreateAccountForm.js) in quiver
 - Modify login/register actions and reducers in the web apps.


Read More:

https://developers.neverbounce.com/v4.0/reference#single-check

https://developers.neverbounce.com/docs/verifying-an-email

https://neverbounce.com/help/understanding-and-downloading-results/result-codes

### Javascript Widget (Client Side implementation)

This solution is quick and easy to setup but is only applicable to web and not to native apps. **Hence not recommended**.

Read more about [JS Widgets](https://developers.neverbounce.com/docs/widget-getting-started).

To implement this: 
 - Embed NeverBounce JavaScript widget to the `body` of the client side web app (For NextJS apps we can use `_document.js`)
 - After page load is complete, register the email field on the mount of the component.
 ```javascript
    // Get the DOM node
    const field = document.querySelector('#email');
    // Register field with the widget and broadcast nb:registration event
    // If ommitted the second argument will assume the value of true
    _nb.fields.registerListener(field, true);
 ```
![Alt text](../images/neverbounce/neverbounce-upgradefunnel.png?raw=true "NeverBounce Upgrade Funnel")

### Other options to verify emails:
 - Use NeverBounce Dashboard to verify one-off emails.
 - Add a [Zapier integration with Stripe](https://neverbounce.com/integrations/zapier) to verify our Stripe customers' email addresses with NeverBounce.

## [Automated List Cleaning](https://neverbounce.com/sync)

We can connect our email provider (MailChimp) for automated cleaning, ensuring accurate and up-to-date data around the clock using the `SYNC` feature in NeverBounce. Read more [here](https://neverbounce.com/integrations/mailchimp).

**Need to discuss if we want this feature or not.**

## [Bulk Email List Cleaning](https://neverbounce.com/clean)

NeverBounce allows us to clean our existing email lists through 2 ways:
 - Use the Dashboard to bulk upload email lists
 - Use a [custom API](https://developers.neverbounce.com/docs/verifying-a-list) to verify bulk email lists

For our existing users we could export the emails from the `UserInfo` collection and run a bulk cleaning through the NeverBounce dashboard. Once the list is processed we can analyze and validate the results and then proceed to export the results back to MailChimp ([via Zapier](https://neverbounce.com/integrations/mailchimp)). With the export process, NeverBounce integration will unsubscribe all emails that were not marked valid.

**Need to discuss if we want this feature or not.**

### [Pricing](https://neverbounce.com/pricing)

NeverBounce offers a pay as you go plan and it's priced per email.

![Alt text](../images/neverbounce/neverbounce-pricing.png?raw=true "NeverBounce Pricing")

For Surfline alone we have around 35k-50k registrations per month. So the cost would be approximately $175 - $250 per month.

#### Notes
 - NeverBounce does not share data to any outside vendors. We have the option to permanently delete the data at any time.
 - NeverBounce API allows us to run a free analysis to determine the overall email list health. [Read more](https://developers.neverbounce.com/docs/running-a-free-analysis).
 - NeverBounce offers a [webhook](https://developers.neverbounce.com/reference#single-webhooks) for verifying an email. This is just an alternative for the custom api implementation.
 - The [single-check](https://developers.neverbounce.com/v4.0/reference#single-check) endpoint also returns a `suggested_correction` property that may correct common typos such as `gmal.com` or `hotmal.com`.


#### Links
https://developers.neverbounce.com/docs

https://developers.neverbounce.com/reference

https://neverbounce.com/help/getting-started/getting-started-guide







