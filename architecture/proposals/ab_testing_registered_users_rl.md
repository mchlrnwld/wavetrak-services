# Implement A/B Tests for Registered Users in Rate Limiting

## What are we doing?

Since we have launched Rate Limiting to 100% of the global audience, we want to A/B Test the experience of a registered user (unverified and verified) using Split.

## What problem are we solving?

Currently we don't have the ability to run multiple A/B tests on registered users using Split in Rate Limiting. Number of Surf Checks, Reset basis, Copy changes etc would be controlled via Split. This helps us in solving few problems such as : 

 - Reduced retention and engagement (Globally or Regionally) if we aren't giving the right surf checks number. This is especially a concern with Australia.
  - Building from the above-->AUS Surfline brand and user base needs more Surf Checks than elsewhere in order to continue to achieve our growth goals there.
  - If we make another large market entry, 3 Surf Checks might not be enough and having this lever would be valuable for us.

## What impact does this have on the business?

  - Continue to test Number of Surf Checks that optimize retention and conversion
  - Giving more checks for a period of time when we launch new features so users can get to use them extensively (Ex: Live Winds or Crowds).
  - Tests on the ability to give more Surf Checks to regions or user segments during swell events or partnership opportunities.

## What is the solution?

To run these A/B tests on registered users we would make use of the `metering-service` endpoints and Split.

### Split changes

Create a new Split treatment with a user specific traffic type. We will create different treatment variations based on the number of tests Product wants to run. Also we can attach custom JSON configurations to each of these variations. I have created a sandbox example - [`sl_rate_limiting_user_v1`](https://app.split.io/org/67a2bcf0-5000-11e7-ab38-12a980fb7f4a/ws/3e83a030-1f27-11e9-9a2e-069aee18f4aa/splits/f2687420-b405-11eb-b11d-0e264c9979ad/env/ac92f130-5001-11e7-ab38-12a980fb7f4a/definition)

Values that can be configured by Split would be :

1. `meterLimit` - Maximum Number of Surf Checks. Corresponds to the `meterLimit` in the `Meter` document.
2. ~~`resetBasis` - Reset frequency for Surf Checks per user.~~ - Update: Per discussion with Product we won't be implementing this since it adds more complexity.


### Metering Service changes

This service is already configured to run Split in server side. We just need to call the treatment and get the values from Split. 

#### Modifications to [`POST /meter`](https://github.com/Surfline/wavetrak-services/blob/master/services/metering-service/src/server/meter/meter.ts#L50)

When a Meter document is created we will call Split and get the treatment variant config for that user and then insert the `Meter` document accordingly.

Split response would look like:
```json
{
  "treatment": "surf_check_weekly_2",
  "config": "{'meterLimit':2, 'copy':'Surf'}"
}
```
In order to be backward compatible with previous app versions we will ensure that the new logic for evaluating the treatment in server would only be executed if the appVersion is latest. To know the app version we will use `X-App-Version` sent from the app.
```
const appVersion = req.get.headers('X-App-Version);
if(appVersion === <latest app version>){
  const result = featureFramework.getTreatmentWithConfig(userId, treatment, identity);
  const { treatment, config } = result;

  if(treatment == 'off' || 'control') {
    return null; // Don't create the Meter document
  }
  Else proceed with meter document creation with `meterLimit` value from Split.
}

```
For app users if the `X-App-Version` is not the latest the `POST /meter` would have the legacy functionality.
 

###  Modifications to Reset Metering Limits Airflow job

Currently the Reset job runs every Monday at 4 AM specific to each timezones. Since the source of truth for the surf check reset count is from Split (`meterLimit`) now onwards we would have to modify the patch admin endpoint to call Split and get that value per user. Note that we are not making any modifications to the actual Airflow job.

To implement this update the [`PATCH admin/refresh/:meterId`](https://github.com/Surfline/wavetrak-services/blob/master/services/metering-service/src/server/meter/admin/admin.ts#L98) endpoint as below.


Get the `meterLimit` value from Split and update the `meterLimit` and `meterRemaining` for the user in `Meter` document.

```
const result = featureFramework.getTreatmentWithConfig(userId, treatment, identity);
const { treatment, config } = result;
// // Update meterLimit and meterRemaining in Meter doc
```


### How do we implement resetBasis ?

**UPDATE - After discussing with Product we decided not to implement this functionality.Keeping it here for future references.**

~~**Feedback required in this section since I am not quite sure how to implement this reliably.**~~

Product wants to control the frequency of the surf checks reset for a user from Split. For this they will add a `resetBasis` in the Split configuration. I am entirely not sure how we can implement this per user since we don't have a reliable way of determining when a user's surf checks period is over. We could try using the `createdAt` date in `Meter` document as the date when the user was first added to the treatment and compute the reset date based on that but I feel it's not a solid way and could result in bugs and downstream analytics issues. Keeping track of the surf check reset frequency per user would be challenging. 

An alternative would be to predefine the reset basis. For example as below:
 - weekly - Resets beginning of every week, Monday. This is the current implementation.
 - biweekly - Resets biweekly, Monday (of the week after that).
 - monthly - Resets on the 1st of the month. This could be any weekday.

 
 These values would be set in Split in the treatment config as below:

 ```json
 config: '{"meterLimit":2, "copy":"Surf"}'
 ```

 - [Modify the Airflow job](https://github.com/Surfline/wavetrak-services/blob/master/jobs/reset-metering-limits/docker/reset-metering-limits/src/handlers/resetMeteringLimits.js#L22) to run on `weekly` , `biweekly` and `monthly` basis. The way the job is currently setup is that we run only on `weekly`. The job needs to be updated to take into account `biweekly` and `monthly` as well.

 - Modify [`PATCH admin/refresh/:meterId`](https://github.com/Surfline/wavetrak-services/blob/master/services/metering-service/src/server/meter/admin/admin.ts#L98) to
   - Accept a new argument `resetBasis` which would be sent from the job.
   - Call Split and get the `resetBasis` value per user. Verify if the `resetBasis` is same and then update the document. Pseudo code:

    ```
    if(arguments.resetBasis === config.resetBasis) {
        // Reset the Meter limits for the user
    }
    ```

### Modifications to Native App

The App would no longer need to compute Split (for registered users) on their side since that would be done by the `metering-service` going forward. 

 - For **registered** users remove the logic for calling `sl_rate_limiting_v3` treatment from App.
 - For **registered** users call `POST /meter` and `GET /meter` accordingly to get the Meter state and `meterRemaining` value for the user. See below pseudo code logic section.
 - For **anonymous** users the Rate Limiting logic would remain as is.
 
 **Question** - Is it possible to update the `sl_rate_limiting_v3` treatment in Split to send back `off` or `control` for `registered` users? This is to support backward compatibilty for app users who would be on the old versions when we roll this change out.

### Australia users

 Currently Rate limiting is not enabled in Australia. So when we roll this change out Australia users should get `off` or `control` from Split. For this whitelist `AUS` in Split for this treatment.

###  Pseudo Code for the Client (Backplane or Native App) for reference:

 ```
if (userId) {
    If (userId is Premium) {
        Don't excute any Rate Limiting logic and exit
    }
    if (entitlements?.includes(SL_METERED)) {
        Call GET /meter
          - Set the meter state based on the response
    else
        Call POST /meter      
          - Set the meter state based on the response
}      
else {
    For Anonymous Users follow the existing Rate limiting logic
}
 ```

 ### Modification to services-common

 Update the [Split](https://github.com/Surfline/wavetrak-services/blob/master/packages/services-common/src/splitio.ts) utility file to add a new function `getTreatmentWithConfig` 
 ```
 export const getTreatmentWithConfig = (
  key: AnonymousId | UserId,
  treatment: string,
  identity: SplitIdentity,
): TreatmentWithConfig => {
  if (!featureFramework) {
    splitLogger.error('Called `getTreatmentWithConfig` without a feature framework.');
    splitLogger.error('Did you forget to set up Split when the server started?');
    throw new Error('Split not set up.');
  }
  return featureFramework.getTreatmentWithConfig(key, treatment, identity);
};
```
This function will be consumed by the `metering-service` endpoints.
 
### Modification to Backplane

 - Modify the backplane server [here](https://github.com/Surfline/surfline-web/blob/master/backplane/src/common/metering.js#L112) to remove the code assosciated with treatment `sl_rate_limiting_v3` for registered users.
 - Modify the [`MeterPremiumPaywall` component](https://github.com/Surfline/surfline-web/blob/master/backplane/src/bundles/margins/metering/MeterPremiumPaywall/MeterPremiumPaywall.js#L59) to remove the hard coded value and use the meter state instead.


### Notes

 - `GET /meter` won't be modified. Any changes to `meterLimit` value in Split would be effective per user from next reset cycle onwards only.
 - DDF would be creating a new Split test manager based on user traffic type.
 - I don't expect any changes to be made to the Upgrade Funnel, entitlements service and the services-proxy
 - The Surf check endpoint ([`PATCH /surf-check`](https://github.com/Surfline/wavetrak-services/blob/master/services/metering-service/src/server/meter/meter.ts#L164)) won't be modified
 - The `sl_rate_limiting_v3` treatment would continue to be evaluated from client side to support Rate limiting for anonymous users
 