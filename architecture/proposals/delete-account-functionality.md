# Delete Account Feature

## What are we doing?

Adding API functionality to `user-service` that queues a user to permanently delete their account.
Adding a `delete-users` job that executes deletion for these users.

## What problem are we solving?

Apple's latest requirements state that applications supporting account creation must allow account
deletion. The Native team weighed several options and has decided on an API-based solution to this
documented [here](https://wavetrak.atlassian.net/wiki/spaces/NTV/pages/2040102943/iOS+App+Store+Review+Guideline+Changes+-+Account+deletion)

Overall, a process for account deletion that does not require human intervention is ideal. In order
to support the Native team's decision, we need to modify the `user-service` to support account 
deletion functionality.

## What impact does this have on the business?

This allows us to remain on the iOS platform without violating their specified terms. The iOS
apps are one of the key ways we interact with our customers, so it's important that we stay
within compliance.

This also provides an alernative for our current GDPR compliance features for deletion. That
process involves the customer requesting a deletion via customer service, who then can call admin
deletion endpoints. This has the potential to cut down on customer service requests because it's an
alternative to that workflow.

## What is the solution?

We need to add a public API to `user-service` that allows a user to delete their account. We already
support limited account deletion for GDPR via the aforementioned admin functionality.

The endpoint should be available as:

```
  {USER_SERVICE}/user DELETE
```

The authenticated `userId` to delete should be provided by the `services-proxy`

Once a request is received, we should store the `userId` to a `DeletedCustomer` collection. The API can
return a 200 after successful upsert of the record.

```
  DeletedCustomers record:
  {
    _id: ObjectId,
    user: ObjectId, (unique constraint)
    deleted: boolean,
    createdAt: Date,
    updatedAt: Date,
  }
```

At a set inteval (daily perhaps?), a new job `delete-users` will query for `{ deleted: false }` users with 
`createdAt + x > new Date()` where `x` is the number of days we wait to delete an account.

The job will issue requests to each of the existing 3 admin delete endpoints:
```
  {USER_SERVICE}/admin/user/{:userId} DELETE
  {USER_SERVICE}/admin/usersettings/{:userId} DELETE
  {USER_SERVICE}/admin/integrations/{:userId} DELETE 
  {METERING_SERVICE}/admin/meter/{:userId} DELETE (to be added in SA-3650)
```

Upon receiving 404 or 200 for all records, the service can mark the `DeletedCustomer` as `{ deleted: true }`

Here's the scope of what account deletion entails (currently). These need to be groomed by the product 
team.
- `UserInfo` document with matching `_id`
- `UserSettings` document with matching `user`
- Customer.io data with matching `userId`
- Segment user with matching `userId`
- Amplitude user with matching `userId`

In addition, we'll also have orphaned records here that optionally can be addressed. These are anonymous
but serve little purpose with a corresponding user.
- `Meter` document with matching `user`
- `Favorites` document with matching `user`

Payment-related:
- `GiftRedemptionCodes` document with matching `user`
- `StripeCustomers` document with matching `user`
- `Subscription` documention with matching `user`
- `SubscriptionPurchases` document with matching `user`
- Corresponding Stripe info
- Corresponding Apple info
- Correspondign Android info

A user should only be able to delete their own user. We need to ensure this endpoint is only
available with proper credentials (ex. logged-in auth token).

## How does this solve the problem?

The solution solves the problem rather explicitly by implementing the missing functionality of programmatic
account deletion. The solution should offer enough flexibility to the user and to us, and provides visibility
as to which `ids` were deleted for our records.

In this solution, we leverage what we have built, but also add additional functionality, error-handling
and record-keeping on top of it.

## What are the alternatives?

Some alternative aproaches were mainly discussed in the iOS
[documentation](https://wavetrak.atlassian.net/wiki/spaces/NTV/pages/2040102943/iOS+App+Store+Review+Guideline+Changes+-+Account+deletion)

### Synchronous Deletion

One option is to forgo saving the record to delete and attempt to delete the customer synchronously
with the request to delete. This is a valid solution to me but has several drawbacks:

- We'd have to bunch up multiple deletes into a single request since there are more than
just a single record to delete.

- ~~There is no transactions in Mongo, so partial failures could exist due to the above. The deletion
could fail but critically destroy the customer's functionality. This would leave us not in compliance 
but in a broken state for that user with no ability of retry. Inevitably this would get bubbled up to
an engineer or remain broken for the customer.~~
There are [transactions in Mongo 4](https://www.mongodb.com/transactions) so this statement may not apply.

- Timeouts would be a concern in a synchronous setting. Both during the deletion and for the customer
awaiting success.

- We should give the customer time to cancel their deletion based on their reasoning. I would imagine
marketing would want to send them through a customer deletion funnel to attempt to stop the deletion.
I know most companies have some required time period for deletion. One example I know of is Facebook 
takes 30 days to delete an account.

## Other

- We should probably add a `DeletedCustomer` record  with `{ deleted: true }` to the existing manual approach.
- I would assume there are Segment or marketing touchpoints that Product team would like to add here.
