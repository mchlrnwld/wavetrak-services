# Productionize LOTUS

## What are we doing?

We are transferring operations of the LOTUS model to the Forecast Engineering squad. Along the way we will be making changes to the way the model is executed to meet our engineering standards for production ready and reducing the time it takes to deliver data to our users.

## What problem are we solving?

### Operations and Production Ready

The job for creating LOTUS model data is providing data to all of our production systems but has not yet been developed to the standard we expect for production systems. Job execution can be brittle, often requiring manual intervention from Francisco Silva who has significant domain knowledge necessary to troubleshoot and keep the lights on day to day. We don't have in place automated solutions for ensuring that this job is executed reliably and can handle common failures (for example, network issues).

Here is a list of specific issues that need to be addressed:

- LOTUS job's monitoring and observability are insufficient, lacking the visibility we need to ensure stable operations and diagnose failures. Currently the only way to debug issues with the job is to SSH onto instances and inspect the file system. If a problem instance has been terminated we lose the ability to perform any forensics to uncover root causes. We need easier access to logs and precise alerting on individual components of the job.
- LOTUS job cannot always recover from a failure on its own. Dependency on restart files from previous runs can often get us into situations where manual intervention is required.
- LOTUS job failure is monolithic. A failure in any part of the job fails the entire job and requires restarting the entire process. Network failures and other failures that are common to cloud computing are not handled gracefully.
- LOTUS model execution is scheduled rather than event driven, meaning it is heavily dependent on upstream data sets (NOAA GFS) being available on time. When this does not happen, the job needs to be manually executed when data is available.
- The job code repository and deployment don't follow existing patterns in our code base (monorepo, CI/CD, no Airflow, hand rolled instances vs Batch), requiring more domain knowledge to maintain.
- We don't have a lower tier environment to use for development purposes and need to make all changes directly to the production system.
- LOTUS job is difficult to test without running the entire job, making it difficult for us to develop on with any level of confidence.

### Data Delivery Delays

Currently LOTUS data is being delivered 6 hours later than our other data sets. Here's a breakdown of the cause of this delay:

- LOTUS model job is scheduled to run ~1 hour after NOAA GFS is expected to be available as a buffer.
- LOTUS model job takes ~4 hours to generate data.
  - ~0.5 hours for preprocessing restart data.
  - ~1 hour for running the model.
  - ~2.5 hours for post processing.
- Forecast platform takes ~1 hour to process and enrich the data for points of interest.

## What impact does this have on the business?

### Operations and Production Ready

The LOTUS model provides swell data that is core to our highest traffic features (spot forecasts, surf heights, conditions, etc.). This data is also used by our forecasters to write forecasts and reports. Reliably delivering this data to our products is fundamental to our business and doing this well should have an obvious impact on all of our core KPIs (conversion, churn, engagement, etc.).

The [bus factor](https://en.wikipedia.org/wiki/Bus_factor) for this core system is currently ~1. The manual effort involved in keeping the lights on for this model is expensive and wasteful. By transferring operations of LOTUS to the Forecast Engineering squad we can leverage our knowledge and existing infrastructure to run the model while freeing up Francisco to do what he does best: innovating on our forecast models.

### Data Delivery Delays

LOTUS being delayed 6 hours holds back new model runs for all of our data sets from being released, resulting users and forecasters seeing stale data for the majority of the day. By the time forecast data is available to our users the next run is already available. Users can have access to public forecast models that are more up-to-date elsewhere, which can contribute to churn and lower conversion. Additionally this delay hampers our forecasters' ability to use the latest data to write reports and forecasts, and schedule surf events. Users will be making decisions based on stale data that could potentially result in loss of life, which is not only terrible for those involved and affected but can lead to major business ramifications (brand/legal).

## What is the solution?

The solution has multiple epics, each of which will contribute to solving multiple issues laid out in the problem statement. New development will attempt to move code into [wavetrak-services/jobs](https://github.com/Surfline/wavetrak-services/tree/master/jobs) where possible to follow monorepo and CI/CD conventions. Each epic is covered at a high level below with their own estimates. Overall effort will be at least an XXL (or 13 sprints for 1 engineer).

1. [Setup LOTUS in Airflow with event triggering](#setup-lotus-in-airflow-with-event-triggering)
1. [Preprocessing and postprocessing on AWS Batch](#preprocessing-and-postprocessing-on-aws-batch)
1. [Run model on AWS HPC with ParallelCluster](#run-model-on-aws-hpc-with-parallelcluster)

The end result of where we want to be will look like the following diagram:

![LOTUS WW3 Model Component Level Diagram](../images/forecast-platform/lotus-ww3-model-component.png)

### Setup LOTUS in Airflow with Event Triggering

We will setup a `lotus-ww3-model` DAG in Airflow to run the model. Lambda function code to setup the compute cluster will be moved into an Airflow task. In the process we will setup `sandbox` and `prod` environments in `surfline-dev` and `surfline-prod` AWS accounts to run in parallel with the existing LOTUS model. Restart files and output files will be moved to s3://surfline-science-s3-[env]/wavetrak/lotus/ to more accurately follow our path conventions in S3 and to ensure we don't interfere with the existing job. Once we're confident the setup is stable in both environments we will cutover and turn down the old LOTUS model job code from the `msw` AWS account. We will also attempt to automate deployment of the Lambda functions as we'll be redeploying as we work through the subsequent epics.

We will subscribe an SQS queue to the NOAA GFS SNS topic and use the `check-model-ready` pattern in the DAG similar to other jobs (for example, `process-forecast-platform-noaa-gfs`) to trigger the model as soon as NOAA GFS data is available. To decouple this work from the rest of the work that will be changing how the job is executed, we'll have the Airflow processing task trigger the Lambda function `launchLotusMaster`.

#### How does this solve the problem?

Setting up `sandbox` and `prod` environments allows us to develop in a lower tier environment without risking disruptions to production data in the subsequent epics and also for future improvements to the model.

Moving the LOTUS model job into Surfline AWS accounts and moving job execution to Airflow allows us to take advantage of existing tooling and infrastructure for deploying and running the job such as CI/CD, logging, and error handling (retries and alerting) and sets us up for subsequent epics of this initiative. It will also be easier to onboard new engineers to the code base and easier to maintain as this job will follow all of our previously established patterns, increasing our bus factor.

Our event triggering patterns in Airflow with SQS are not a true event driven execution of the job, but a scheduled polling mechanism that allows us to trigger jobs within 5 minutes of the event. By following these patterns, we can more reliably execute LOTUS when the data is available, allowing us to account for unpredictability of the availability of upstream data (NOAA GFS), and reducing our data delivery by ~1 hour since we no longer need to buffer the execution time.

#### Estimate

- Story points:
  - 2 - Add S3 event notifications trigger SNS topic for new S3 destinations.
  - 2 - Setup new SQS queue subscribed to SNS topic.
  - 5 - New `lotus-ww3-model` DAG with `check-model-ready` logic polls messages SQS queue.
  - 13 - New `run-ww3-model` task takes the place of `launchLotusMaster` by launching EC2 master and slave instances. Point to new S3 destinations and code is moved into `wavetrak-services/jobs/run-lotus-ww3-model`. Instances need to be modified to access model code in new repository. Infrastructure setup in Terraform.
  - 2 - Setup PagerDuty alerting.
  - 2 - Point `process-forecast-platform-wavetrak-lotus-ww3` and `process-full-grid-wavetrak-lotus-ww3` to new S3 destinations.
  - 3 - Move existing LOTUS data to new S3 destination and teardown LOTUS model in `msw` AWS account.
- T-shirt size: M

### Preprocessing and Postprocessing on AWS Batch

We will extract the preprocessing and postprocessing parts of the model execution into separate Airflow tasks to be run on Batch. S3 will be used as shared storage across tasks within a DAG run. Specifically, output of preprocessing uploaded to S3 for model to download and use, and then output of model uploaded to S3 for postprocessing to download and use. We will also attempt to parallelize postprocessing code (creating grid and POI data outputs from raw model outputs). At this point we will also write integration tests for the tasks we are creating. Execution of the core WW3 model will still happen via the `launchLotusMaster` Lambda.

#### Estimate

- Story points:
  - 13 - Copy preprocessing code into `preprocess-ww3-inputs` Airflow task. Uploads data into S3.
  - 5 - Compute cluster downloads preprocessing code from S3. Uploads data to S3.
  - 13 - Copy postprocessing code into `postprocess-ww3-outputs` Airflow task. Modify to download from and upload to S3.
  - 5 - Parallelize `postprocess-ww3-outputs`.
- T-shirt size: M-L

#### How does this solve the problem?

By modularizing parts of the job into separate tasks we naturally create checkpoints for the job, allowing us to recover from failures faster by only retrying the part that failed. It also makes development easier since we can test individual tasks of the job in isolation. Integration tests will give us more confidence when we need to make changes to the job.

By running these tasks on Batch, we can take advantage of tooling and infrastructure we already have in place for logging, alerting, automatic retries, etc. We will be able to reuse existing resources (bin packing), which could save us cost. We will also be able to take advantage of patterns in place for parallelizing the execution of the postprocessing part of the job. This part of the job currently takes ~2.5 hours and is we should be able to reduce this to no more than 0.5 hours, providing us the biggest opportunity for performance optimization.

### Run Model on AWS HPC with ParallelCluster

We will replace `launchLotusMaster` with [AWS ParallelCluster](https://aws.amazon.com/hpc/parallelcluster/) to orchestrate `mpiexec` on an [AWS HPC](https://aws.amazon.com/hpc/) cluster instead of hand rolled EC2 instances. This cluster will be where we run satellite assimilation and model processing. We will also evaluate [EFA](https://aws.amazon.com/hpc/efa/) for higher performance communication between nodes in the cluster, and [FSx](https://aws.amazon.com/fsx/) for higher performance shared file storage during processing. Adopting this could be a complex effort as it'll be a learning curve for us and we'll need to validate how reliable AWS HPC is as a service.

This epic will be done last because, though necessary to do eventually, I perceive a lower ROI on this work and if we complete the previous epics a failure in the HPC cluster will be easier to spot and recover from. We should also spike and PoC the AWS HPC service before we commit to doing this (see [multi-node parallel jobs](#multi-node-parallel-jobs)).

#### How does this solve the problem?

The main problem we will be solving with the compute cluster is reliability and observability. Specifically a recurring failure in our model is when instances fail to come online or fail during model execution. Though the previous epics will mitigate this risk by helping us to more easily identify this kind of failure and to recover quickly, we still need to solve this problem.

We use `mpiexec` to execute cluster computing with Message Passing Interface (MPI) to run the WW3 model. HPC is tailor made to run these types of compute clusters and supports MPI out of the box. HPC should give us monitoring and observability of the entire cluster, is elastic in that it autoscales and registers compute nodes for us, and provides performance optimizations like EFA and FSx that could improve our data delivery. Ideally an AWS service like this would allow us to offload the knowledge of running a compute cluster to the experts, giving us the benefit of a highly optimized and stable cluster while saving us engineering time. Without benchmarking with a PoC it's difficult to know how much execute time we could save by using HPC, EFA, and FSx.

#### Estimate

We need an extra spike and PoC to plan this out. Details for the spike can be found in https://wavetrak.atlassian.net/browse/FE-777. Ballpark estimate for the work would be L-XL.

## What are the alternatives?

Most epics outlined in the solution don't have practical alternatives worth mentioning. AWS HPC is the one epic where it's reasonable to consider alternative solutions.

### Multi-node Parallel Jobs

[Multi-node parallel jobs](https://docs.aws.amazon.com/batch/latest/userguide/multi-node-parallel-jobs.html) are potentially another way to execute a compute cluster using MPI. It sounds like HPC is compatible with Batch. We should do the spike and PoC to better understand how these two services work together or if they are competing solutions.

### Move Compute Cluster Management from Lambda to Airflow

If HPC is not the right solution we could consider continuing to manage the compute cluster ourselves, but moving the management from Lambda to Airflow. This could allow us to track the success/failure of the WW3 model, but without any visibility or improved reliability of model execution. We should only consider this after exploring HPC. We will not be doing this as a short term solution because it provides a low ROI. The work is potentially complex and would include moving logic from EC2 instance user data into application code, consolidating it with Lambda code, and infrastructure and IAM roles to properly manage the compute clusters. As currently setup, the Lambda is very rarely a point of failure so simply moving this to Airflow doesn't solve a problem we currently have. We would need to put in more work to solve the problem of monitoring the WW3 model from the Airflow task. If HPC turns out to be the right solution this may become a few sprints of wasted work with little added value.

## What do we know already?

### Current Model Job Flow

This is a dump of my notes on the execution of the model in [lotus-dev](https://github.com/Surfline/lotus-dev).

1. `launchLotusMaster` Lambda runs `jobs/launch-master/index.js`.
1. Launches master instance with user data `jobs/launch-master/model-run-user-data.sh`. User data:
  1. Downloads setup files from s3://msw-lotus/lotus-setup.
  1. Downloads Lotus source from `model-source/`.
  1. Launches 4 slaves using `scripts/lotus/launch-slaves.sh`.
    - `launchLotusSlaves` Lambda runs `jobs/launch-slaves/index.js`.
    - Stores slave IPs to be used by model processing.
    - Waits for confirmation of an SSH "handshake" From each slave before continuing.
  1. Runs model processing using `model-source/run_model.py`.
1. Model processing (`model-source/run_model.py`).
  1. Downloads input and restart files.
  1. Preprocesses input and restart files.
    - `process_model_grid` creates input, bathymetry, wind, ice for each grid (binary format).
    - `run` processes for actual grid points and `output` creates output for square grid (not relevant for `GLOB_30m`).
      - `output` for nests (not `GLOB_15m`) contain filled in data from global grids (`GLOB_15m` then `GLOB_30m` if exists).
  1. Hindcast process (satellite assimilation) (~20 minutes).
    - Octave used for interpolation of satellite data.
    - 12 hours of data from restart files compared to satellite data and we apply corrections.
    - Done 3 hours of hindcast at a time.
    - 6 hours of assimilation (corrections by running the model) and then 6 hours of smoothing.
    - If satellite data is unavailable skip this section.
    - Must be done sequentially so unlikely we can improve performance here.
    - Done on GLOB_30m only.
  1. Runs model for forecast hours.
    - Slaves pull grids for the run off of a queue to do work.
      - `GLOB_15m` and `GLOB_30m` can run in parallel (~1 hour).
      - Nests depend on global grids (~10 minutes each).
      - Could improve by running in parallel.
      - Outputs are calculated version of spectra on grid points in a binary file.
        - We use executables to pull data out of these binaries for our own netCDF4 output files.
  1. Create output files (grid and point netCDF4) (~2.5 hours).
    - This could be parallelized.
    - Point output takes much longer than grid output.
    - File merging slows down process as well.
    - Some points are defined in global grid and nest. We should prefer nest and not get global point if we can get away from it.

### Restart Files

Restart files (2.5 days of WW3 outputs) current reside in s3://msw-lotus/restarts/ and are ~16 GiB. If restart files don't exist, we need to generate new restart with calm conditions. This is done with `model-source-spin-up/run_model.py`, takes a few hours, and will converge after 15  days of runs.

### Resources

- https://learn.scientificprogramming.io/introduction-to-high-performance-computing-hpc-clusters-9189e9daba5a
- https://docs.aws.amazon.com/batch/latest/userguide/multi-node-parallel-jobs.html
- https://docs.aws.amazon.com/parallelcluster/latest/ug/tutorials_03_batch_mpi.html
- https://www.youtube.com/watch?v=UnQM7cX2y0E
