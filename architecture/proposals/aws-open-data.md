# AWS Open Data

## What are we doing?

Currently Forecast Platform jobs attempt to download data from servers operated by NOAA. Some of this data is also available through [AWS Open Data](https://aws.amazon.com/opendata/). We are going to transition our Forecast Platform jobs to use data from AWS Open Data.

## What problem are we solving?

We rely on data sourced from NOAA (NOMADS) to be available on time for our business. NOAA's servers are unreliable and have had a growing number of outages this past year.<sup>[1,2](#references)</sup> Outages seem to be correlated to traffic, which can be exacerbated during significant weather events. We have had to engineer solutions for our processing jobs to work around the expected unreliability of these servers and continue to devote engineering resources to harden our systems to account for these outages. Additionally, NOAA recently announced plans to rate limit data requests which could impact our ability to access data.<sup>[3](#references)</sup>

## What impact does this have on the business?

Forecast data is core to our business. Data outages severely degrade the user experience and diminishes users' trust in our product, which has an obvious impact on conversion and churn. Significant weather events should be conversion and engagement opportunities, but our ability to take advantage of these events and engage our users can be at the mercy of NOAA's ability to handle the additional public traffic to their servers. One example of this was the NOMADS data outage that occurred at the same time as a significant swell event at Mavericks.

Additionally, building on top of unreliable servers and responding to outages takes away from engineering resources that can be better spent on product development.

By switching key data sets to AWS Open Data where possible we should be able to more reliably provide data to our users. This should have a positive impact on conversion and churn while reducing engineering and operational costs.

There will also be a small AWS cost saving. See [Cost Impact](#cost-impact).

## What is the solution?

Modify NOAA GFS and NOAA GEFS processing jobs and LOTUS to use AWS Open Data.

GFS and GEFS processing jobs and LOTUS currently download GRIB files from our own S3 buckets. We will modify them to download GRIB files from AWS Open Data S3 buckets. Our download jobs use IDX files to only download a subset of properties we use. Since the GRIB files in AWS Open Data contain all properties, our processing jobs and LOTUS will need to use IDX files to only download a subset of properties at processing time. S3 GetObject requests support byte range requests for a single byte range only. [AWS suggests aggregating the result of multiple single byte range requests](https://docs.aws.amazon.com/AmazonS3/latest/dev/optimizing-performance-guidelines.html#optimizing-performance-guidelines-get-range) as its solution for multiple byte ranges for performance reasons.

Processing jobs currently trigger based on messages in an SQS queue subscribed to an SNS topic published by an S3 event that notifies when files are uploaded. GFS and GEFS Open Data sets provide an SNS topic that we can subscribe our SQS queue to. LOTUS runs on a schedule.

Download jobs for GFS and GEFS will be removed.

See [AWS Open Data GFS and GEFS-Wave Epic in Jira](https://wavetrak.atlassian.net/browse/PS-1847).

![Forecast Platform System](../images/forecast-platform/forecast-platform-system-container.png?raw=true "Forecast Platform System")

## How does this solve the problem?

There are 2 main ways NOAA has data outages:
1. NOAA's NOMADS servers have networking issues which inhibit our ability to download data.
2. NOAA's modeling supercomputers fail to upload data to NOMADS servers.

For server outages, switching to AWS Open Data allows us to take advantage of the "infinite" scalability and reliability of S3.

For upload failures, there's a chance the data will fail to be uploaded to AWS Open Data as well. These types of outages will be out of our control whether we use AWS Open Data or NOMADS. Occasionally when there are upload failures NOAA, decides not to backfill data to NOMADS. When this happens we sometimes fall back to FTPPRD, which is supposed to be the older data dissemination server (which is why we use NOMADS instead of FTPPRD). There's some evidence that data is more reliably uploaded to AWS Open Data than to NOAA's own servers. See [Reliability and SLA](#reliability-and-sla).

## What are the alternatives?

FTPPRD seems to be more reliable than NOMADS, though not immune to network outages. We could have luck switching to FTPPRD servers, but NOMADS seems like NOAA's newer and recommended server and long term AWS Open Data should be the most resilient. This option would involve less upfront development, but we would likely pay for this in the long run in the form of operational costs and future outages.

## What do we know already?

- GFS and GEFS GRIB (and IDX) files in AWS Open Data are identical to the files found in NOMADS.
- I've successfully tested subscribing an SQS queue in us-west-1 to AWS Open Data SNS topics in us-east-1. As far as I can tell the message bodies are identical to the message bodies of our current setup with S3 events.
- There are currently no legacy science jobs that use GFS or GEFS data from our S3 buckets.

### Usable Data Sets

| Data Set | Location | Notification | Archive Time |
| -------- | -------- | ------------ | ------------ |
| [NOAA GFS](https://registry.opendata.aws/noaa-gfs-bdp-pds/) | `s3://noaa-gfs-bdp-pds/` | `arn:aws:sns:us-east-1:123901341784:NewGFSObject` | 1 Month |
| [NOAA GEFS](https://registry.opendata.aws/noaa-gefs/) | `s3://noaa-gefs-pds/` | `arn:aws:sns:us-east-1:123901341784:NewGEFSObject` | Indefinite |

### Other Relevant Data Sets

NASA provides [MUR-SST](https://registry.opendata.aws/mur/) data, but in the [Zarr](https://zarr.readthedocs.io/en/stable/) data format. While the data format looks much friendlier than netCDF4 provided from NASA's servers, there would still be some effort to familiarize ourselves with Zarr. Since MUR-SST isn't as high value of a data set to us and since we haven't experienced any issues with NASA's servers with our current workarounds I think we shouldn't move to this data source for now.

NOAA provides [HRRR](https://registry.opendata.aws/noaa-hrrr-pds/) data. We're not currently using this data set but it's worth noting since we've experimented with it on the best system in the past.

NOAA NAM and WW3 data sets are not currently available on AWS Open Data. I received the following response from NOAA's Big Data Program contact, when I asked if there were any plans to add these data sets to AWS Open Data:

> We are always looking at additional data to move to the BDP, this is true regardless of the cloud provider. We will make note of this to look into.

### Reliability and SLA

NOAA's Big Data Program has been experimenting with hosting data on cloud platforms, including AWS Open Data, since 2015. As of December 2019 NOAA has signed a contract with AWS (as well as other cloud platforms) to operationalize hosting data in the cloud.<sup>[4,5,6](#references)</sup>

Though I wasn't able to find any specific SLA for NOAA's data, it sounds like NOAA is committed to making more of its data available through cloud platforms like AWS Open Data:<sup>[5](#references)</sup>

> "Cloud-based storage and processing is the future. Not only will this improved accessibility enhance NOAA's core mission to protect life and property, but it will also open up new and exciting areas of research at universities and significant market opportunities for the private sector," said Neil Jacobs, acting NOAA administrator in the statement.

To understand the reliability of data availability on AWS Open Data, I went through the most recent NOMADS outages and inspected the timestamps of GEFS data files being made available on AWS Open Data, which is archived from 2017.

| Outage Date | FTPPRD/NOMADS Impact | AWS Open Data Impact |
| ----------- | -------------------- | -------------------- |
| 2021-01-21 | NOMADS server down through 2021-01-23. NOMADS data not backfilled. | None |
| 2021-01-11 | 45-50 minute delays to both. | None |
| 2020-12-11 | 90 minute delays to both. NOMADS data unavailable through 2020-12-14. | 90 minute delay for single run. |
| 2020-11-12 | 24 hour delays to both. NOMADS data not backfilled. | None |
| 2020-11-06 | 2 hour delay to single run to NOMADS. | None |
| 2020-10-22 | Data unavailable on NOMADS through 2020-10-26. Data not backfilled. | None |

I received the following answers to my questions from NOAA's Big Data Program contact, which suggests using AWS Open Data should be reliable while remaining non-commital:

How reliably can we expect NOAA to upload data to AWS on the same schedule as its uploads to NOMADS?
> All data that moves from NOAA internal/on-prem systems to the Cloud Providers under the Big Data Program (BDP) is moved on the same schedule as long as that is feasible and there is a user requirement. As for data related to GEFS and GFS, we initiate transfers as soon as data is available.

During upload delays like the one we saw on 12/11/2020, are uploads to AWS Open Data affected and if so is the data backfilled at the same time that it’s backfilled to NOMADS and FTPPRD?
> Since the data that moves to the BDP is the same data that is coming out of internal systems, there may be outages that happen upstream that will affect the cloud data. NOAA works to ensure reliable data feeds and we continue to research ways to stabilize systems. By moving data to the cloud, we hope to reduce the strain on some of these systems and see a positive effect on reliability. The BDP makes every attempt to backfill data as long as it becomes available after an outage.

Are you recommending users switch to cloud providers for data as a solution to ease traffic to NOMADS?
> We recommend users adopt the method that is best for them. Ideally, if users are currently working in the cloud, then switching over to cloud feeds will increase their efficiency and take the strain off internal NOAA systems. Users and NOAA alike will benefit from reducing some of this traffic.

### Cost Impact

There will be some AWS cost saving by switching to AWS Open Data as well since we will no longer be running download jobs and storing data in S3. Ingress is free since we host our own NAT instances.

| Description | Usage | Rate | Total Cost |
| ----------- | ----- | ---- | ---- |
| Download Jobs (Batch EC2) | 74 hrs/month | $0.112/hr (m5.large) | $8.29/month/environment |
| S3 Storage | 10 day archive * 4 (runs/day) * (34 GB (GEFS-Wave) + 11 GB (GFS))/run 45 = 1800 GB | $0.026/GB-month | $46.80/month/environment |

Just to be clear there's no cost for data ingress from cross region S3 buckets which host AWS Open Data.

This ends up saving us roughly $110.18/month.

## References

1. [2020-12-05 Forecast Platform Download Outage Retrospective](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/1429012854/2020-12-05+Forecast+Platform+Download+Outage+Retrospective)
2. [2020-10-23 Buoyweather NCEP Forecast Outage Retrospective](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/1290338682/2020-10-23+Buoyweather+NCEP+Forecast+Outage+Retrospective)
3. [Weather Service faces Internet bandwidth shortage, proposes limiting key data](https://www.washingtonpost.com/weather/2020/12/09/nws-data-limits-internet-bandwidth/)
4. [NOAA Big Data Program](https://www.noaa.gov/organization/information-technology/big-data-program)
5. [NOAA's Big Data Project is officially operational](https://gcn.com/articles/2019/12/23/noaa-cloud-data.aspx)
6. [NOAA Expands Public Access to Big Data](https://www.ncei.noaa.gov/news/noaa-expands-big-data-access)
