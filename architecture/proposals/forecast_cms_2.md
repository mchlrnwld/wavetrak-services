# Spot Forecasts

## What are we doing?

We are building the ability for forecasters to create forecasts with surf height, condition, and
analysis at the spot level. Along with this addition of spot level forecasts we will be modifying
subregion forecasts as follows:

- Removing the height and condition ratings from subregion forecasts
- Changing to 3 days of required written forecast analysis
- adding the option to write subregion forecasts 16 days out

In order to support this work we need to build the backend that will drive creating, updating, and
displaying spot forecast data that is created in the spot forecast CMS. The backend API will be the
central point for admin management as well as reading and displaying the data for the KBYG product.

We will also need to build a new CMS module in order to drive this new forecasting logic at the
subregion and spot level. We are building a new CMS because we are deprecating the old one and using
this new one in parallel until we are able to fully roll this feature out.

More details on the requirements and background can be found on these confluence documents:
https://wavetrak.atlassian.net/wiki/spaces/KBYG/pages/2192146588/Spot+Forecast+CMS
https://wavetrak.atlassian.net/wiki/spaces/KBYG/pages/2234253333/What+forecast+content+do+we+provide+at+subregional+and+spot+level

## What problem are we solving?

Currently the forecasts at the subregional level can be confusing for users to translate to a local
spot level due to differences in the mechanics of the local spot. As part of the initiative to solve
for this problem product we are creating a spot forecast CMS which will allow forecasters to correct
forecasts at the spot level (which will be assimilated with the automated forecasts). We will slowly
integrate these forecasts into the product.

Additionally having forecast and reports at both the spot and subregion level makes it unclear to
the user which to give more weight to when making the call. This confusion will lead to poorer
perceived accuracy because we are leaving translation and assimilation to a user who may not be
capable of doing that themselves.

## What impact does this have on the business?

Users (especially less experienced users) will have a much easier time understanding the forecast
and will get a more accurate forecast for their local spots (assuming they live/surf in an area
where we have human forecasts). This will also allow forecasters to give a more nuanced forecast and
hopefully reduce the kickback they get from users who have been confused by the subregion forecasts.

This work will simplify the UX and improve perceived accuracy. It'll also reduce the technical
footprint (removing subregion forecast cms, premium analysis, regional forecast page). The more
accurate the forecast the more we'll retain and convert users given the forecast is a key component
of the value of the product to users. A recent survey showed forecast accuracy is the highest thing
users want us to be working on and recent marketing campaigns have shown a strong connection between
forecast accuracy and increased conversion.

## What is the solution?

### High Level Approach

The high level approach for building and migrating to the new Forecast CMS is proposed as follows:

1. We will not be making any breaking changes to any API's that exist currently in order to ensure full
   backwards compatibility for all the native apps. The new CMS and Product UI will be built off of
   brand new endpoints

2. We will serve model data on the API's that rely on subregion level conditions and surf height
   forecasts (this will ensure that we can stay backwards compatible).

3. New functionality on existing endpoints will be served behind a query parameter (based on a feature
   flag) in order to ensure we can confidently merge to master and toggle this on at a later time

4. We'll create all the new UI and API routes needed to serve the new Forecast CMS:

   - Create the Forecast CMS UI
   - Create the Forecast CMS API routes in the Forecasts API
   - Create the Forecast CMS API routes in the Spots API
   - Create the Forecast CMS API routes in the KBYG Product API

5. We'll update the `Spots` and `Subregions` models to have the `forecastStatus`

6. We'll update existing endpoints (behind a query parameter) to make them backwards compatible to the new
   changes
   a. Updating references to `forecastStatus`
   b. backfilling removed forecast conditions/heights with model data

### Spot Forecast CMS

Because we are planning to roll these features out in parallel with the current subregion level
forecasts we will need to deprecate the old forecast CMS and build a new one to support the spot
forecasts.

As we are currently moving towards using NextJS, a material UI design system, Typescript, and Jest
we should build the CMS using those tools. The existing CMS also already has some of the logic that
we will need to build this new CMS. We should copy this code over and adapt it to our new patterns
and add test coverage wherever we do not have it currently.

To stick with the current architecture that we have with CMS modules, this will be a client-side
rendered app that will be stored in S3. Fortunately, NextJS makes this super easy for us by allowing
us to statically render the application.

Lastly, as we are moving towards the monorepo we will want to store this module in the monorepo
and adapt it to the new CI/CD pipeline that we are building.

Once we have fully migrated to the new CMS, we will completely tear down the old one and leave no
trace (:bomb:).

#### Human Forecast Status

With the introduction of spot forecasts, we will now want to be able to enable and disable forecasts
at both the `subregion` and the `spot` level. As part of the requirements the subregion's
`forecastStatus` will have precedence over a spot's `forecastStatus`. For example a spot inside an
inactive subregion cannot be forecasted, but we can explicitly disable a spot within a subregion.
This means that we will want to store the `forecastStatus` at both the `subregion` and `spot` level.
The subregion level `forecastStatus` will have precedence over a spot `forecastStatus`.

Currently on the existing forecast CMS when we toggle the `forecastStatus` we do this by creating a
new `SubregionForecastSummaries` document. The current implementation feels a bit awkward where
turning a `forecastStatus` to `inactive` will create a `SubregionForecastSummaries` document which
has `forecastStatus.status` as `inactive`

Going forward in the Spot Forecast CMS I am proposing that we store this property on the individual
subregions and spots rather than linking it to a forecast document. With the addition of a
`forecastStatus` at the `spot` level I think it makes sense for it to become part of the model for
`subregions` and `spots`. This property for both the `spots` and `subregions` will be able to be
toggled in both the POI CMS and the new spot forecast CMS. We will want to interface with it via an
endpoint on the `spots-api`:

```
PATCH /admin/spots/:spotId { "forecastStatus": "active" | "inactive" }
PATCH /admin/subregions/:subregionId { "forecastStatus": "inactive" | "active" }
```

We'll automatically migrate over to this new `forecastStatus` property once we are cut over to using
the new CMS (since it will be implemented to use that from the start.)

### Subregion Forecasts

Once we have cut over to the new CMS we will be fully removing the `forecast.am` and `forecast.pm`
properties from the `SubregionForecastDays` model (since we will no longer have conditions and wave
height forecasts). No other modifications are necessary.

This model change will be supported in the code and we will use model data to backfill for the now
removed properties. This will result in a partially degraded experience for native app users on
older versions (no more colored condition bars) but this is something product is aware of and okay
with.

### Spot Forecasts

In order to serve spot forecasts we will create a new collection in the database called
`SpotForecasts` (which will be mostly based off the existing `SubregionForecastDays`).

The major change from `SubregionForecastDays` is that instead of storing 2 observervations
per day (AM and PM), we will store the observations as an array with one element per hour observed.
This way we're able to support corrections at any hour of the day.

As with the `SubregionForecastDays` the document will look something like this:

```json
{
  "_id": "6148faafd33e89b39312c548",
  "hours": [
    {
      "timestamp": 1663750800,
      "waveHeight": {
        "max": 4,
        "min": 3,
        "plus": true
      },
      "rating": "FAIR_TO_GOOD"
    },
    {
      "timestamp": 1663772400,
      "waveHeight": {
        "max": 4,
        "min": 3,
        "plus": true
      },
      "rating": "FAIR_TO_GOOD"
    },
    {
      "timestamp": 1663794000,
      "waveHeight": {
        "max": 4,
        "min": 3,
        "plus": true
      },
      "rating": "FAIR_TO_GOOD"
    }
  ],
  "observation": "some human observation",
  "forecastDate": "2021-09-21",
  "spotId": "58581a836630e24c4487900a",
  "updatedAt": "2021-09-20T21:18:39.432+0000",
  "createdAt": "2021-09-20T21:18:39.432+0000"
}
```

This model would be stored in the `forecasts-api` alongside the existing `SubregionForecastDays`
model.

Additionally we'll create endpoints to create & get these forecast documents.

#### Create POST /admin/forecasts/spot Endpoint

We'll create a POST endpoint which will upsert the forecast days sent in the body of the request.
The contract will be as follows:

**Request**

```json
{
  "forecasts": [
    {
      "spotId": "584204a24e65fad6a7709e46",
      "forecastDate": "12/20/2016",
      "forecast": {
        "hours": [
          {
            "timestamp": 1663750800,
            "waveHeight": {
              "max": 4,
              "min": 3,
              "plus": true
            },
            "rating": "FAIR_TO_GOOD"
          },
          {
            "timestamp": 1663772400,
            "waveHeight": {
              "max": 4,
              "min": 3,
              "plus": true,
              "humanRelation": "Waist to shoulder high",
              "occasionalHeight": 5
            },
            "condition": "TBD HERE --- waist to chest high",
            "rating": "FAIR_TO_GOOD"
          },
          {
            "timestamp": 1663794000,
            "waveHeight": {
              "max": 4,
              "min": 3,
              "plus": true
            },
            "rating": "FAIR_TO_GOOD"
          }
        ],
        "observation": "some human observation"
      }
    }
  ]
}
```

**Response Body**

```json
{
  "forecasts": [
    {
      "_id": "584204a24e65fad6a7709e46"
    }
  ]
}
```

#### Create GET /admin/forecasts/spot endpoint

We'll create a GET endpoint which will retrieve spot level forecasts.
The contract will be as follows:

**Request Body**

```
  Query Parameters:
    days: number
    spotId: string
    startDate: unix timestamp
    endDate: unix timestamp
```

**Response Body**

```json
{
  "forecasts": [
    {
      "_id": "585d9f0a6ef28ffa78af345b",
      "forecastDate": "2016-12-25T00:00:00.000Z",
      "spotId": "584204a24e65fad6a7709e46",
      "forecast": {
        "hours": [
          {
            "timestamp": 1663750800,
            "waveHeight": {
              "max": 4,
              "min": 3,
              "plus": true
            },
            "rating": "FAIR_TO_GOOD"
          },
          {
            "timestamp": 1663772400,
            "waveHeight": {
              "max": 4,
              "min": 3,
              "plus": true
            },
            "rating": "FAIR_TO_GOOD"
          },
          {
            "timestamp": 1663794000,
            "waveHeight": {
              "max": 4,
              "min": 3,
              "plus": true
            },
            "rating": "FAIR_TO_GOOD"
          }
        ],
        "observation": "some human observation"
      }
    }
  ]
}
```

#### Create POST /admin/forecasts/subregion endpoint

We'll create a POST endpoint which will upsert the subregion forecast days and summary sent in the body of the request.
The contract will be as follows:

**Request**

```json
{
  "subregionId": "12345",
  "forecasts": [
    {
      "forecastDate": "2021-11-18",
      "observation": "Some forecast observation"
    }
  ],
  "summary": {
    "nextForecast": 1637222400,
    "forecaster": {
      "email": "jmonson@surfline.com",
      "name": "Jeremy Monson",
      "title": "Senior Software Engineer"
    },
    "highlights": [
      "Jeremy is testing",
      "This is a test",
      "Beep beep I'm a jeep"
    ],
    "bestBets": ["go surf", "you could not go surf too"],
    "forecastDate": "2021-11-18",
    "currentDate": "2021-11-18T00:00:00.000Z"
  }
}
```

**Response Body**

```json
{
  "forecasts": [
    {
      "_id": "6196e10fc02d02756291120e"
    }
  ],
  "summary": {
    "_id": "6196e10fc02d02756291120e"
  }
}
```

#### Create GET /admin/forecasts/subregion endpoint

We'll create a GET endpoint which will retrieve subregion level forecasts.
The contract will be as follows:

**Request Body**

```
Query Parameters:
    days: number
    subregionId: string
    startDate?: unix timestamp
    endDate?: unix timestamp
```

**Response Body**

```json
{
  "subregionId": "12345",
  "subregionName": "Santa Cruz",
  "subregionTimezoneOffset": -8,
  "timezone": "America/Los_Angeles",
  "nextForecast": 1637222400,
  "forecaster": {
    "email": "jmonson@surfline.com",
    "name": "Jeremy Monson",
    "title": "Senior Software Engineer"
  },
  "highlights": ["Jeremy is testing", "This is a test", "Beep beep I'm a jeep"],
  "bestBets": ["go surf", "you could not go surf too"],
  "forecastDate": "2021-11-18",
  "currentDate": "2021-11-18T00:00:00.000Z",
  "forecasts": [
    {
      "_id": "585d9f0a6ef28ffa78af345b",
      "forecastDate": "2016-12-25",
      "subregionId": "584204a24e65fad6a7709e46",
      "updatedAt": "2016-12-23T22:03:03.075Z",
      "observation": "This is the observation",
      "createdAt": "2016-12-23T22:02:50.041Z"
    }
  ]
}
```

### Product API Endpoints

In order to support the new forecasts (and maintain backwards compatibility) we'll create a set of
new product API endpoints which will serve the Subregion & Spot level forecasts. They are modeled
off the existing endpoints, but adapated to the new data models.

#### Create GET /subregions/forecasts/forecast endpoint

We'll create an endpoint to serve the subregion forecasts (daily observations and summary). The
route is not the most optimal, but all the other good options were already taken and we cannot
remove those since we need to keep backwards compatibility.

The contract will be as follows:

**Request Body**

```
Query Parameters:
  subregionId: string (mongo ID)
  days?: number (optional defaults to 16 days for premium, 3 days for non-prem)
```

**Response Body**

```json
{
  "associated": {
    "utcOffset": -7
  },
  "data": {
    "forecastStatus": {
      "status": "active",
      "message": ""
    },
    "nextForecast": {
      "timestamp": 1634259600,
      "utcOffset": -7
    },
    "forecaster": {
      "name": "Keaton Browning",
      "title": "Forecaster",
      "iconUrl": "https://www.gravatar.com/avatar/a7628271b83f93b6361514e747566ee4?d=mm"
    },
    "highlights": [
      "Best Surf in the Next 7 Days: Thursday-Friday",
      "Reinforcing NW and SSW swells keep fun surf going into Thu-Fri",
      "Both SPAC and NPAC look promising beyond mid month"
    ],
    "bestBets": [
      "Late morning as tide drops and conditions remain clean",
      "Lunch break/midday window decent as well"
    ],
    "observations": [
      {
        "utcOffset": -7,
        "forecastDate": "2021-10-14",
        "human": true,
        "observation": "Sluggish with the AM high tide but will improve into midday as tide drops. Fresh NW swell on tap along with some reinforcing SSW swell."
      },
      {
        "utcOffset": -7,
        "forecastDay": "2021-10-15",
        "human": false,
        "observation": "Some LOTUS automated observation."
      }
    ]
  }
}
```

#### Create GET /spots/forecasts/forecast endpoint

We'll create an endpoint to serve the spot level forecasts (daily observations and summary).

The contract will be as follows:

**Request Body**

```
Query Parameters:
  spotId: string (mongo ID)
  days?: number (optional defaults to 16 days for premium, 3 days for non-prem)
```

**Response Body**

```json
{
  "associated": {
    "units": {
      "temperature": "F",
      "tideHeight": "FT",
      "swellHeight": "FT",
      "waveHeight": "FT",
      "windSpeed": "MPH"
    },
    "utcOffset": -7
  },
  "data": {
    "forecasts": [
      {
        "timestamp": 1634281200,
        "utcOffset": -7,
        "forecastDate": "2021-10-15",
        "forecaster": {
          "name": "Chris Borg",
          "avatar": "https://www.gravatar.com/avatar/732a7c19e81048b6e1f7d9d654c450c7?d=mm"
        },
        "human": true,
        "hours": [
          {
            "timestamp": 1663750800,
            "waveHeight": {
              "max": 4,
              "min": 3,
              "plus": true
            },
            "rating": "FAIR_TO_GOOD"
          },
          {
            "timestamp": 1663772400,
            "waveHeight": {
              "max": 4,
              "min": 3,
              "plus": true
            },
            "rating": "FAIR_TO_GOOD"
          },
          {
            "timestamp": 1663794000,
            "waveHeight": {
              "max": 4,
              "min": 3,
              "plus": true
            },
            "rating": "FAIR_TO_GOOD"
          }
        ],
        "observation": "some human observation"
      }
    ]
  }
}
```

## How does this solve the problem?

On an engineering level I think this solution is a simple building block for what will likely be
expanded on in the future. I think at this stage the focus is just to build something really simple
that can be extended, and also to solve for previous mistakes or pain points.

## Other

I want to put a quick note here to discuss previous pain points that we have had with the subregion
forecasts. I think it's important to cover at least the following topics:

- Are there any daylight savings time considerations here? (I don't think so since we just have
  generic AM/PM reports that are linked to a specific day not a timestamp)
- Are there any product considerations and pain points that we want to solve for?
- Are there any CMS considerations and pain points that we want to solve for?
  - Forecasters want to be able to have "Recently Viewed Subregions" on the CMS subregion search
    page
  - Forecasters have some issues with the WYSIWG editor that we need to look into
  - I've started a discussion with the forecasters to find out what part of the workflow is painful
    for them and will continue to work with them to ensure that we give them the best experience
    possible
