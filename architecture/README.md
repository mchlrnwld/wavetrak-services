This folder contains architecture diagrams and proposals for Wavetrak systems and services. It's intended that this folder contains diagrams and proposals across all brands/systems even if the codebases for those systems exist in different repositories.

See Diagraming below for an intro in how to diagram.

# Architecture Diagrams

Diagrams of our currently implemented architecture live in the `systems` subfolder. Feel free to contribute more diagrams or corrections as you see fit by opening a PR and assigning the tech lead for that system to review it. For who that should be see the [code ownership page](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/240779901/Code+and+Service+Ownership+Guidelines) and [team + objectives page](https://wavetrak.atlassian.net/wiki/spaces/PROD/pages/1467810794/2021+Product+and+Engineering+Quarterly+Objectives).

# Proposals

Proposals (also called architecture pitches) are used when proposing changes to the architecture or changes to process. They should be used when a team is considering a major change to a systems design (for example adding sessions clip generation) or architecture within a microservice (for example migrating account app frontends to next.js) or when proposing a different pattern (for example mono repo structure).

## Creating a Proposal

Proposals take research and planning to put together, this ensures we can successfully roll out these changes. Whilst we are open to proposals from anywhere in the org we can only investigate so many at once. The lighest way to pitch a concept is to speak to your manager or lead or do a [tech talk](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/127979914/Weekly+Tech+Talks). For a concept to get prioritized it should be a technical initiative in the [Technology Planning board](https://wavetrak.atlassian.net/secure/RapidBoard.jspa?rapidView=180&projectKey=TP) and prioritized onto the roadmap by the tech lead for your team. The description of the issue in the technology planning board can be used as a light weight pitch.

Once a technical initiative is ready to be worked on a template for proposal can be found in `document/proposal-template.md`. The purpose of this page is to provide context on the architecture diagram, help engineers understand why certain design choices are being made and help us make the case for this investment in time vs burning down some other aspect of tech debt.

We're using a template vs a PR description because it allows us to comment inline. Architecture proposals can contain embedded architecture diagrams, an example is in the proposal template.

## Review Workflow

__Open a PR__ with the proposed changes and assign relevant engineers for that system, all tech leads and Staff+ engineers.

Whilst the above explicit team is added to the PR everyone is heavily encouraged to question/comment and suggest improvements to architecture proposals in the PR (or direct to the PR owner if you're not comfortable hitting them up in the PR). The above list ensures at minimum this group are aware of this change.

__Approval:__ we'll aim to have as much conversation on the PR as possible but will still require a short meeting towards the end of the PR discussion to cover any loose ends.

The purpose of "approval" isn't to prevent changes from happening, quite the opposite. We want to help make those changes happen by creating alignment across teams, providing additional resources as necessary and critique such that changes fulfill objectives and can be lasting parts of the system.

__Merging:__ proposals can be merged once approved. Once the system is fully released the system designer should update the architecture diagrams in `systems` with what's changed as a result of this release.

# Folder/Naming Conventions

* `systems/overall` contains system context diagrams for all brands and then each brand. This is the first place to look when trying to understand the architecture at Wavetrak.
* `systems/[key-system-name]/` should contain multiple diagrams for a key system which is described in the `overall` context diagrams.
* Within each system there should be up to 3 levels of detail. Those levels being the levels described in the C4 model (Context, Containers, and Components). Append `-[level].puml` to each diagram so the viewer knows what level of detail they're looking at.
* There may be multiple diagrams within a systems folder for a given level for example `wowza-containers.puml` and `sessions-containers.puml`.
* We should try to stay away from "v2" etc naming and instead use PRs to view revisions to a diagram.

# Diagraming

## Structure

Architecture diagrams follow the excellent [C4 model](https://c4model.com/) which is a standard way to describe software architecture at various levels of detail. Click [the link](https://c4model.com/) to read more or watch [this short video](https://www.youtube.com/watch?v=x2-rSnhpw0g).

The high-level structure of the C4 model is:

![C4 model abstractions](https://c4model.com/img/c4-overview.png)

## How it works

Architecture diagrams are created using code so we get reproducibility, versioning, and code reviews. We're using [PlantUML](https://github.com/RicardoNiepel/C4-PlantUML) which is an extension of [PlantUML](https://plantuml.com/) to provide C4 assets. [PlantUML](https://plantuml.com/) is a tool to turn code into UML diagrams.

A PlantUML diagram requires a rendering server to generate architecture diagrams from the code.

## Live Preview

There's an excellent plugin for VS Code [PlantUML extension](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml) which enables realtime editing and previewing of architecture diagrams. You can also find VS Code snippets for C4-PlantUML at [.vscode/C4.code-snippets](https://github.com/RicardoNiepel/C4-PlantUML/blob/master/.vscode/C4.code-snippets).

To setup:

1. Install the extension above.
2. Graphviz is required on the host machine to use this plugin. To install that on Mac: `brew install graphviz`.
3. Configure plugin to use local PlantUML server, edit plugin settings:

```
    "plantuml.server": "http://localhost:8080",
    "plantuml.render": "PlantUMLServer",
```

4. Run `make dev` to start a local PlantUML server.
5. Open `example.puml` and hit Alt+D to view the diagram. From this point on the preview will auto-update as changes are made to any `.puml` file.

## Generating Images

Rendered versions of architecture diagrams are stored in `images`. To update diagrams after making changes to `*.puml` files:

```bash
# Run generator.
make build #if you've not run it previously
make generateimages
```

## Useful Links

- https://blog.anoff.io/2018-07-31-diagrams-with-plantuml/
- https://stackoverflow.com/questions/32203610/how-to-integrate-uml-diagrams-into-gitlab-or-github
- https://github.com/RicardoNiepel/C4-PlantUML

