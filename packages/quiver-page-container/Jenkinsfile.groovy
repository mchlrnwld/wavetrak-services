node {
  env.NODEJS_HOME = "/var/lib/jenkins/tools/jenkins.plugins.nodejs.tools.NodeJSInstallation/node-6.9.2/bin/"
  env.PATH = "${ env.NODEJS_HOME }:${ env.PATH }"

  def quiverPageContainerVersion = ''
  def quiverDir = 'quiver'
  def editorialDir = 'editorial'
  def webDir = 'web'
  stage('Checkout Quiver') {
    checkout([$class                           : 'GitSCM',
              branches                         : [[name: "${ QUIVER_BRANCH }"]],
              doGenerateSubmoduleConfigurations: false,
              extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                   relativeTargetDir: quiverDir]],
              submoduleCfg                     : [],
              userRemoteConfigs                : [
                [url: 'git@github.com:Surfline/quiver.git'],
                [credentialsId: 'surflinejenkinsbot']
              ]
    ])
  }
  stage('Bump Quiver Page Container') {
    dir("${ quiverDir }/page-container/bundle") {
      sh """
      aws --profile prod s3 cp s3://sl-artifactory-prod/config/.npmrc .npmrc
      npm version ${ VERSION_BUMP }
      git config user.email "pagecontainer@jenkins"
      git config user.name "Page Container Bump"
      git commit -am "Auto-version bump for ${ VERSION_BUMP }"
      git push origin HEAD:${ QUIVER_BRANCH }
      """
      quiverPageContainerVersion = sh(
        script: 'jq -r .version package.json',
        returnStdout: true
      ).trim()
    }
  }
  stage('Deploy Page Container to CDN') {
    dir("${ quiverDir }/page-container/bundle") {
      QUIVER_ASSET_PATH = "s3://sl-product-cdn-prod/page-container/${ quiverPageContainerVersion }"
      sh """
      echo RELEASING TO CDN
      aws --profile prod s3 cp --recursive dist/ ${ QUIVER_ASSET_PATH }
      """
    }
  }
  stage('Release to Editorial') {
    checkout([$class                           : 'GitSCM',
              branches                         : [[name: EDITORIAL_BRANCH]],
              doGenerateSubmoduleConfigurations: false,
              extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                   relativeTargetDir: editorialDir]],
              submoduleCfg                     : [],
              userRemoteConfigs                : [
                [url: 'git@github.com:Surfline/surfline-web-editorial.git'],
                [credentialsId: 'surflinejenkinsbot']
              ]
    ])
    dir(editorialDir) {
      headerPath = "wordpress/content/themes/surfline/header.php"
      staticHeaderSource = "../${ quiverDir }/page-container/bundle/node_modules/@surfline/quiver-react/dist/NewNavigationBar/static-header.html"
      staticHeaderTarget = "wordpress/content/themes/surfline/static-header.php"
      sh """
      sed -i "s/\\(\'PAGE_CONTAINER_VERSION\', \'\\)\\(.*\\)/\\1${
        quiverPageContainerVersion
      }\')\\;/" ${ headerPath }
      grep PAGE_CONTAINER ${ headerPath }
      cp ${ staticHeaderSource } ${ staticHeaderTarget }
      git config user.email "pagecontainer@jenkins"
      git config user.name "Page Container Bump"
      git commit -am "Auto-version bump for page container: ${ quiverPageContainerVersion }"
      git push origin HEAD:${ EDITORIAL_BRANCH }
      """
    }
  }
  stage('Release to Web Charts') {
    checkout([$class                           : 'GitSCM',
              branches                         : [[name: WEB_BRANCH]],
              doGenerateSubmoduleConfigurations: false,
              extensions                       : [[$class           : 'RelativeTargetDirectory',
                                                   relativeTargetDir: webDir]],
              submoduleCfg                     : [],
              userRemoteConfigs                : [
                [url: 'git@github.com:Surfline/surfline-web.git'],
                [credentialsId: 'surflinejenkinsbot']
              ]
    ])
    dir("${ webDir }/charts/src") {
      indexPath = "index.php"
      distIndexPath = "../dist/index.php"
      sh """
      sed -i "s/\\(\'PAGE_CONTAINER_VERSION\', \'\\)\\(.*\\)/\\1${
        quiverPageContainerVersion
      }\')\\;/" ${ indexPath }
      sed -i "s/\\(\'PAGE_CONTAINER_VERSION\', \'\\)\\(.*\\)/\\1${
        quiverPageContainerVersion
      }\')\\;/" ${ distIndexPath }
      grep PAGE_CONTAINER ${ indexPath }
      grep PAGE_CONTAINER ${ distIndexPath }
      git config user.email "pagecontainer@jenkins"
      git config user.name "Page Container Bump"
      git commit -am "Auto-version bump for page container: ${
        quiverPageContainerVersion
      }"
      git push origin HEAD:${ WEB_BRANCH }
      """
    }
  }
  stage('Deploy editorial and web charts') {
    parallel(
      "Deploy Editorial": {
        build job: 'misc/Convenience Builds/deploy-editorial-to-ecs-all-containers', parameters: [[$class: 'StringParameterValue', name: 'ENVIRONMENT', value: TARGET_ENVIRONMENT],
                                                                                                  [$class: 'StringParameterValue', name: 'VERSION', value: EDITORIAL_BRANCH]]
      },
      "Deploy Web Charts": {
        build job: 'surfline/Web/deploy-frontend-to-ecs', parameters: [[$class: 'StringParameterValue', name: 'ENVIRONMENT', value: TARGET_ENVIRONMENT],
                                                                       [$class: 'StringParameterValue', name: 'VERSION', value: WEB_BRANCH],
                                                                       [$class: 'StringParameterValue', name: 'CONTAINER_PATH', value: 'charts']]
      }
    )
  }
}
