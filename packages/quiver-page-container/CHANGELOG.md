# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### 0.6.5 (2021-11-01)

**Note:** Version bump only for package @surfline/surfline-header-bundle
