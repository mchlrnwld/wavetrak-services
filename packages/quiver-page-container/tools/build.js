import sh from 'shelljs';
import webpackCompiler from './webpackCompiler';
import config from '../config/webpack';

const clean = () => {
  sh.rm('-rf', './build/*');
  sh.rm('-rf', './dist/*');
};

export default () => {
  const conf = config({}, 'prod');

  const compileApp = () => webpackCompiler(conf.clientConfig, (clientStats) => {
    if (clientStats.hasErrors()) return;
  });

  clean();
  compileApp().run(() => undefined);
};
