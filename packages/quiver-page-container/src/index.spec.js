import { expect } from 'chai';
import surflinePageContainer from './index';

describe('index', () => {
  it('has footer and header create functions', () => {
    expect(surflinePageContainer).to.have.property('createHeader');
    expect(surflinePageContainer).to.have.property('createFooter');
  });
});
