import 'babel-polyfill';
import ReactDOM from 'react-dom';
import React from 'react';
import cookie from 'react-cookie';
import { SurflinePageContainer, SurflineFooter } from '@surfline/quiver-react';
import identitySelector from '@surfline/web-common/dist/features/identity';
import {
  setupFeatureFramework,
  // getTreatment,
} from '@surfline/web-common/dist/features/splitio';
import './styles/base.scss';
import getUser from './api/getUser';
import getUserSettings from './api/getUserSettings';
import getFavorites from './api/getFavorites';
import getEntitlements from './api/getEntitlements';
// import { SHOW_EXPANDED_SEARCH_UI } from './features/treatments';

const surflinePageContainer = {

  /**
   *
   * @param element HTML element to add header as child to
   * @param environmentConfig  {{serviceUrl : string, environment: string, splitioAuthKey: string }}
   * @param isNative Boolean is context native / embedded web view
   * @param dfpCallback function to push into googletag cmd stack after targeting param
   * @returns {Promise.<void>} returned promise can be ignored
   */
  createHeader: async (element, environmentConfig = {}, isNative, dfpCallback) => {
    let user;
    let favorites;
    let entitlements;
    let settings = {};
    const newAccessToken = cookie.load('new_access_token');
    const accessToken = cookie.load('access_token') || newAccessToken;

    try {
      if (accessToken) {
        const environmentConfigWithAccessToken = { ...environmentConfig, accessToken };
        user = await getUser(environmentConfigWithAccessToken);
        entitlements = await getEntitlements(environmentConfigWithAccessToken);
        favorites = await getFavorites(environmentConfigWithAccessToken);
        settings = await getUserSettings(environmentConfigWithAccessToken);
        if (newAccessToken && user) {
          cookie.save('access_token',
            accessToken,
            { maxAge: 2628000, path: '/' });
          cookie.save('refresh_token',
            cookie.load('new_refresh_token'),
            { maxAge: 31536000, path: '/' });
          cookie.remove('new_access_token');
          cookie.remove('new_refresh_token');
        }
      } else {
        favorites = await getFavorites(environmentConfig);
        settings = await getUserSettings(environmentConfig);
      }
    } catch (ex) {
      // continue regardless of error
    }
    const googletagUserType = entitlements && entitlements.includes('sl_premium') ? 'PREMIUM' : 'ANONYMOUS';
    if (window.googletag && window.googletag.cmd) {
      window.googletag.cmd.push(() => window.googletag.pubads().setTargeting('usertype', googletagUserType));
      window.googletag.cmd.push(dfpCallback);
    }

    const identity = identitySelector({
      user: user ? {
        details: user,
        entitlements,
      } : {},
    });

    await setupFeatureFramework(
      environmentConfig.splitioAuthKey,
      identity,
    );

    const splitTreatments = {
      // [SHOW_EXPANDED_SEARCH_UI]: getTreatment(SHOW_EXPANDED_SEARCH_UI, identity),
    };

    if (!isNative && element) {
      ReactDOM.render(<SurflinePageContainer
        showFooter={false}
        serviceConfig={environmentConfig}
        user={user}
        favorites={favorites}
        entitlements={entitlements}
        settings={settings}
        splitTreatments={splitTreatments}
      />,
      element);
    }
  },

  /**
   *
   * @param element  HTML element to add header as child to
   */
  createFooter: (element) => {
    if (element) {
      ReactDOM.render(<SurflineFooter />, element);
    }
  },

  /* eslint-disable no-undef */
  dfpCallback: () => {
    googletag.enableServices();
    const unitId = '/1024858/Horizontal_Variable';
    const mapping = googletag.sizeMapping()
        .addSize([1200, 0], ['fluid', [1170, 250], [1170, 160], [990, 90], [970, 250], [970, 160], [970, 90], [728, 90]])
        .addSize([992, 0], ['fluid', [990, 90], [970, 250], [970, 160], [970, 90], [728, 90]])
        .addSize([768, 0], ['fluid', [750, 250], [750, 160], [728, 90]])
        .addSize([320, 0], ['fluid', [320, 80], [320, 50]])
        .addSize([0, 0], ['fluid', [320, 50]])
        .build();
    googletag.defineSlot(unitId, ['fluid', [1170, 250], [1170, 160], [990, 90], [970, 250], [320, 50], [970, 90], [970, 160], [320, 80], [300, 250], [750, 250], [750, 160], [728, 90]], 'div-gpt-ad-1493340496386-0')
        .defineSizeMapping(mapping)
        .setTargeting('position', 'SPONSORSHIP')
        .addService(googletag.pubads());
    googletag.pubads().addEventListener('slotRenderEnded', (event) => {
      if (event.size !== null && event.size[0] === 0 && event.size[1] === 0) {
          // We've got a fluid ad.
        $('.hero-ad').addClass('hero-ad-fluid');
      }
    });
    googletag.display('div-gpt-ad-1493340496386-0');
  },
};


/**
 * Create the surfline header. Don't display the header and top hero ad on native.
 * On native, we only need the user entitlements when initializing googletag. Future refactor?
 */
surflinePageContainer.createHeader(
  document.getElementById('surfline-static-header'),
  window.environmentConfiguration,
  window.nativeApp,
  surflinePageContainer.dfpCallback,
);
surflinePageContainer.createFooter(document.getElementById('surfline-footer'));

module.exports = surflinePageContainer;
