import 'whatwg-fetch';

/**
 * @param environmentConfig {{serviceUrl : string, environment: string, accessToken: string}}
 * @returns {Promise.<Array>} Favorites
 */
const getFavorites = async (environmentConfig) => {
  const { serviceUrl, accessToken } = environmentConfig;
  let url = `${serviceUrl}/kbyg/favorites`;
  if (accessToken) url += `?accessToken=${accessToken}`;
  const response = await fetch(url);
  if (response && response.status < 400) {
    return (await response.json()).data.favorites || [];
  }
  return [];
};

export default getFavorites;
