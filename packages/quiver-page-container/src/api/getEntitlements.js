import 'whatwg-fetch';

/**
 * @param environmentConfig {{serviceUrl : string, environment: string, accessToken: string}}
 * @returns {Promise.<Array>} Entitlement strings
 */
const getEntitlements = async (environmentConfig) => {
  const { serviceUrl, accessToken } = environmentConfig;
  const response = await fetch(`${serviceUrl}/entitlements?accessToken=${accessToken}`);
  if (response && response.status < 400) {
    return (await response.json()).entitlements || [];
  }
  return [];
};

export default getEntitlements;
