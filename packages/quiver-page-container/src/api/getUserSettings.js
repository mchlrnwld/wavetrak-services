import 'whatwg-fetch';

/**
 * @param environmentConfig {{serviceUrl : string, environment: string, accessToken: string}}
 * @returns {Promise.<Object>} UserSettings object
 */
const getUserSettings = async (environmentConfig) => {
  const { serviceUrl, accessToken } = environmentConfig;
  let url = `${serviceUrl}/user/settings`;
  if (accessToken) url += `?accessToken=${accessToken}`;
  const response = await fetch(url);
  if (response && response.status < 400) {
    return response.json();
  }
  return {};
};

export default getUserSettings;
