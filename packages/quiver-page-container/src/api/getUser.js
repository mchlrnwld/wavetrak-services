import 'whatwg-fetch';

/**
 * @param environmentConfig {{serviceUrl : string, environment: string, accessToken: string}}
 * @returns {Promise.<Object>} User object
 */
const getUser = async (environmentConfig) => {
  const { serviceUrl, accessToken } = environmentConfig;
  const response = await fetch(`${serviceUrl}/user?accessToken=${accessToken}`);
  if (response && response.status < 400) {
    return response.json();
  }
  return null;
};

export default getUser;
