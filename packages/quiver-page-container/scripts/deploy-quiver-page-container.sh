#!/usr/bin/env bash

PACKAGE_VERSION=$(< package.json grep \"version\" \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[:space:]')

S3_PATH="s3://sl-product-cdn-prod/page-container/${PACKAGE_VERSION}"

aws s3 cp \
  --recursive \
  ./dist/ \
  "${S3_PATH}"
