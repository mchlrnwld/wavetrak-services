# Page Container and Static Error Pages

Page container is a header/footer bundle for non-react projects like Charts or Editorial

Error pages are static pages for Nginx to default to for (4|5)xx errors

## Development
 * `npm run server` in `bundle` to build and host the dist directory for charts/editorial development

## Publishing NPM package

Within the `/packages/` directory we publish NPM packages using Lerna.

A step by step guide for publishing NPM packages in the monorepo can be found [here](../../README.md#npm-packages).
## Static error pages

These static error pages include the 500 error and 404 error pages in english. These will be pushed up to S3 upon publishing via the `postPublish` command which will run a script for pushing that up to our CDN.

### Sandbox
sl-subscription-public-static-sbox.s3.amazonaws.com

http://d3kyisuydi6c5p.cloudfront.net/error-pages/404.en.html
### Staging
sl-subscription-static-staging.s3.amazonaws.com

http://dojtv91ijeswu.cloudfront.net/error-pages/404.en.html
### Production
sl-subscription-static-prod.s3.amazonaws.com

http://prod-subs.cdn-surfline.com/error-pages/404.en.html
## Prepare environment

`npm run install`

## Build and generate dist

`npm run build`

## Develop

`npm run server`

This hosts `dist/` locally so you can point charts / editorial / error pages to your dev version.
