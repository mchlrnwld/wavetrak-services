const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = () => {
  const config = {
    devtool: 'inline-source-map',
    module: {
      loaders: [
        {
          test: /\.js$/,
          use: {
            loader: 'babel-loader',
            query: {
              presets: ['react', 'es2015', 'stage-0'],
            },
          },
          exclude: /node_modules/,
        },
        {
          test: /\.json$/,
          loader: 'json-loader',
        },
        {
          test: /sinon\.js$/,
          loader: 'imports?define=>false,require=>false',
        },
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: 'css-loader!sass-loader',
            publicPath: 'dist',
          }),
        },
      ],
    },

    resolve: {
      extensions: ['.js', '.json'],
      modules: [path.resolve(__dirname, '../../node_modules')],
    },

    resolveLoader: {
      modules: [path.resolve(__dirname, '../../node_modules')],
    },
    externals: [
      'react/addons',
      'react/lib/ExecutionEnvironment',
      'react/lib/ReactContext',
    ],
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('development'),
      }),
      new ExtractTextPlugin({
        filename: 'surfline-page-container.css',
        disable: false,
        allChunks: true,
      }),
    ],
  };

  return config;
};
