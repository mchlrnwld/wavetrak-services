import merge from 'webpack-merge';
import baseConfig from './base.config';
import surflinePageContainerConfig from './surfline-page-container.config';

export default (config, env = 'dev') => {
  const clientConfig = merge.smart(baseConfig(), surflinePageContainerConfig());

  return {
    clientConfig,
  };
};
