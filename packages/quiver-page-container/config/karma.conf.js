const testWebpackConfig = require('./webpack/tests.config.js')({});

module.exports = (config) => {
  config.set({
    basePath: 'config',
    frameworks: ['mocha'],
    files: [
      '../node_modules/phantomjs-polyfill-find/find-polyfill.js',
      './bundleTests.js',
    ],
    exclude: [
    ],
    preprocessors: {
      './bundleTests.js': ['coverage', 'webpack', 'sourcemap'],
    },
    webpack: testWebpackConfig,
    coverageReporter: {
      type: 'in-memory',
    },
    remapCoverageReporter: {
      'text-summary': null,
      json: './coverage/coverage.json',
      html: './coverage/html',
      lcovonly: './coverage/lcov.info',
    },
    webpackMiddleware: {
      stats: 'errors-only',
    },
    reporters: ['mocha', 'coverage', 'remap-coverage'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['PhantomJS'],
    singleRun: true,
    concurrency: Infinity,
  });
};
