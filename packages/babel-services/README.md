# `@surfline/babel-preset-services`

This package is a babel preset for the babel config for Surfline Services.

## Usage

This config should be used in the `presets` property which babel exposes for .babelrc files.

The configuration can be consumed as follows:

#### Babel base configuration

```javascript
// .eslintrc
{
  "presets": [
    [
      "@surfline/babel-preset-services",
      {
        // options go here
      }
    ]
  ],
}
```

## Options

Our config exposes a few different options:

### Base config options

#### Debug

Key: `debug`

Default: `false`

Valid values: `true, false`

Description: Whether or not to enable verbose debugging logs

#### Modules

Key: `modules`

Default: `"auto"`

Valid values: `true, false, "auto", "cjs"`

Description: Whether or not to transform modules (defaults to `auto`)

*Note:* When using `sinon` for tests, it is likely you will have to pass `modules: "cjs"` for this option, since
sinon breaks for non commonjs modules.

https://babeljs.io/docs/en/babel-preset-env#modules

#### Targets

Key: `targets`

Default: `{ node: "current" }`

Description: A list of environment targets for the project. This value is passed directly to `@babel/preset-env` if set.

https://babeljs.io/docs/en/babel-preset-env#targets

#### Loose Classes

Key: `looseClasses`

Default: `true`

Description: [See babel documentation on loose classes](https://babeljs.io/docs/en/babel-plugin-transform-classes#loose)

#### Loose

Key: `loose`

Default: `false`

Description: Passes the loose option to `@babel/preset-env`. [See babel documentation on loose option for preset-env](https://babeljs.io/docs/en/babel-preset-env#loose)

#### Transform Runtime

Key: `transformRuntime`

Default: `true`

Description: [See babel documentation](https://babeljs.io/docs/en/babel-plugin-transform-runtime)

#### Runtime Version

Key: `runtimeVersion`

Default: `null`

Description: [see babel documentation](https://babeljs.io/docs/en/babel-plugin-transform-runtime#version)

#### Runtime Helper Use ES Modules

Key: `runtimeHelpersUseESModules`

Default: `!modules`

Description: [see babel documentation](https://babeljs.io/docs/en/babel-plugin-transform-runtime#useesmodules)

## Publishing NPM package

Within the `/packages/` directory we publish NPM packages using Lerna.

A step by step guide for publishing NPM packages in the monorepo can be found [here](../../README.md#npm-packages).
