# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## <small>0.1.7 (2021-10-13)</small>

* chore(babel-services): migrate to packages directory ([e245dd8](https://github.com/Surfline/wavetrak-services/commit/e245dd8))





## <small>0.1.6 (2020-11-04)</small>

* PS-000 - Replace artifactoryonline.com references with jfrog.io. (#3664) ([21430cd](https://github.com/Surfline/wavetrak-services/commit/21430cd)), closes [#3664](https://github.com/Surfline/wavetrak-services/issues/3664)





## [0.1.5](https://github.com/Surfline/wavetrak-services/compare/@surfline/babel-preset-services@0.1.4...@surfline/babel-preset-services@0.1.5) (2020-03-18)


### Bug Fixes

* Create a tagging script for Lerna ([9812c76](https://github.com/Surfline/wavetrak-services/commit/9812c76d618b9f54ba34403cf2d026fb983ea856))





## 0.1.4 (2020-03-11)

**Note:** Version bump only for package @surfline/babel-preset-services





## 0.1.3 (2020-03-09)

**Note:** Version bump only for package @surfline/babel-preset-services





## 0.1.2 (2020-03-04)

**Note:** Version bump only for package @surfline/babel-preset-services





## 0.1.1 (2020-03-04)

**Note:** Version bump only for package @surfline/babel-preset-services





# 0.1.0 (2020-02-25)


### Bug Fixes

* fix package versions ([b52885d](https://github.com/Surfline/wavetrak-services/commit/b52885dc221b7c16fc541da550b58457e687eed2))
* README fixes, fix wrong repo link ([49a4329](https://github.com/Surfline/wavetrak-services/commit/49a4329c15885385fc7c7ca5bdb0ace50f4ff0b7))


### Features

* Create Services Babel Config ([9b98b18](https://github.com/Surfline/wavetrak-services/commit/9b98b182bb1d5fb1b1b8e74c9373cb751af7b8f6))
