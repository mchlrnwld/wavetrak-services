/* eslint-disable strict */
/* eslint-disable global-require */

('use strict');

const { declare } = require('@babel/helper-plugin-utils');

const defaultTargets = { node: 'current' };

function buildTargets({ customTargets }) {
  if (customTargets) {
    return customTargets;
  }
  return defaultTargets;
}

module.exports = declare((api, options) => {
  // see docs about api at https://babeljs.io/docs/en/config-files#apicache
  api.assertVersion('^7.0.0');

  const {
    modules = 'auto',
    targets = buildTargets(options),
    looseClasses = true,
    loose = false,
    runtimeVersion,
    runtimeHelpersUseESModules = !modules,
    useBuiltIns = 'usage',
    transformRuntime = true,
  } = options;

  if (typeof modules !== 'boolean' && modules !== 'auto' && modules !== 'cjs') {
    throw new TypeError(
      '@surfline/babel-preset-services only accepts `true`, `false`, `"auto"`,' +
        'or `"cjs"` as the value of the "modules" option'
    );
  }

  const debug = typeof options.debug === 'boolean' ? options.debug : false;

  return {
    sourceMaps: 'inline',
    retainLines: true,
    presets: [
      [
        require('@babel/preset-env'),
        {
          debug,
          exclude: ['transform-async-to-generator', 'transform-regenerator'],
          loose,
          corejs: 3,
          useBuiltIns,
          modules,
          targets,
        },
      ],
    ],
    plugins: [
      require('@babel/plugin-syntax-dynamic-import'),
      [require('@babel/plugin-proposal-class-properties'), { loose: looseClasses }],
      looseClasses
        ? [
            require('@babel/plugin-transform-classes'),
            {
              loose: true,
            },
          ]
        : null,
      require('@babel/plugin-proposal-export-namespace-from'),
      require('@babel/plugin-proposal-export-default-from'),
      require('@babel/plugin-transform-property-literals'),
      require('@babel/plugin-proposal-nullish-coalescing-operator'),
      require('@babel/plugin-proposal-logical-assignment-operators'),
      require('@babel/plugin-proposal-optional-chaining'),
      transformRuntime
        ? [
            require('@babel/plugin-transform-runtime'),
            {
              absoluteRuntime: false,
              corejs: false,
              helpers: true,
              regenerator: false,
              useESModules: runtimeHelpersUseESModules,
              version: runtimeVersion,
            },
          ]
        : null,
    ].filter(Boolean),
  };
});
