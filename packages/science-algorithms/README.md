# Science Algorithms

A library of useful science algorithms.

## Setup

See https://wavetrak.atlassian.net/wiki/spaces/WT/pages/73334794/PyPI+Package+Server for instructions on getting setup to download/upload packages to Artifactory.

Sample `$HOME/.pypirc` file:

```
[distutils]
index-servers =
  dev
  prod

[dev]
repository: https://surfline.jfrog.io/surfline/api/pypi/pypi-local-dev
username:
password:

[prod]
repository: https://surfline.jfrog.io/surfline/api/pypi/pypi-local
username:
password:
```

## Use

Install with `pip`, but before doing so,
run the following command:
`aws s3 cp s3://sl-artifactory-prod/config/.pip/pip.conf ~/.pip/pip.conf`
```

Then install with `pip`.

```sh
$ pip install wavetrak-science-algorithms
```

## Development

### Setup Environment

Use [conda](https://docs.conda.io/en/latest/) to setup a development environment.

```sh
$ conda env create -f environment.yml
$ conda activate wavetrak-science-algorithms
```

### Install Local Package

To test changes in a local application install as an "editable" package.

```sh
$ pip install -e /path/to/job-helpers/
```

### Deploy

Deploy to `dev` repository to test for local development.

```sh
$ ENV=dev make deploy
```
