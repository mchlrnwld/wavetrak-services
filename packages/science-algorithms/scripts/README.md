# Spectral Refraction Scripts

A set of scripts to generate tests, validate results, and plot the results of the Python version of the spectral refraction algorithm.


# Generate Test Cases For Spectral Refraction Algorithm

Generate a random sample test cases for the Spectral Refraction algorithm based on the output of the Fortran code.

## Setup

```sh
$ conda devenv
$ conda activate spectral-refraction-scripts
```

## Running

### Generate Test Cases

1. Run command:
    ```sh
    $ cp .env.generate.sample .env.generate
    ```

2. Edit your `.env.generate` file with appropriate values for each environment variables.

3. Execute the following command:
    ```sh
    $ env $(xargs < .env.generate) python main.py
    ```
4. A fixture: `spectral_refraction_pois.py` will be created once the script exits. This fixture can be used to run unit tests for the `SpectralRefraction.calculate()` method. By default the script will generate `2000` test cases chosen randomly from the set of `FORECAST_HOURS` and `GRIDS` given, but this amount can be changed with the `TEST_CASE_SIZE` variable.

# Calculate and Plot Spectral Refraction Surf Heights

Calculates and plots surf heights for the Spectral Refraction algorithm. The results are compared to the Fortran version.

## Running

### Validate Surf Heights

1. Run command:
    ```sh
    $ cp .env.validate.sample .env.validate
    ```

2. Edit your `.env.validate` file with desired `FORECAST_HOURS` and `GRIDS` to calculate the surf heights for.

2. Execute the following command:
    ```sh
    $ env $(xargs < .env.validate) python validate_surf_heights.py
    ```

3. 2 CSVs: `fortran-surf-heights.csv` and `python-surf-heights.csv` are created when the script exits. The 2 CSVs can be used to plot error rate of the Python results vs the Fortran results with the `plot_surf_height.py` script. The total number of differences between these CSVs can also be seen by using the command: 
    ```sh
    diff <(sort fortran-surf-heights.csv) <(sort python-surf-heights.csv) | grep '^>' | wc -l
    ```

### Plot Surf Heights

1. Run command:
    ```sh
    $ cp .env.plot.sample .env.plot
    ```

2. Edit your `.env.plot` file with the path to the CSVs.

2. Execute the following command:
    ```sh
    $ env $(xargs < .env.plot) python plot_surf_height.py
    ```

3. A histogram showing the error rate of the Python surf heights against the Fortran surf heights will automatically launch.
