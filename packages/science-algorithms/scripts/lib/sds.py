from typing import List

import requests
from lib.models import PointOfInterestGridPoint


class SDS:
    """
    Client for querying the Science Data Service.

    Args:
        url: URL for Science Data Service host.
        agency: The model agency to query.
        model: The model name to query.
        grid: The model grid to query.
    """

    def __init__(self, url: str, agency: str, model: str):
        self.url = url
        self.agency = agency
        self.model = model

    def query_points_of_interest(self) -> List[PointOfInterestGridPoint]:
        """
        Queries the GraphQL endpoint for PointOfInterest data
        from the surf_spot_configurations table in Postgres.

        Returns:
            A list of PointOfInterest objects for
            the given agency, model, and grid.
        """
        query = f'''
        query {{
          models(agency: "{self.agency}", model: "{self.model}") {{
            grids {{
              grid
              pointsOfInterest {{
                id
                lon
                lat
                optimalSwellDirection
                breakingWaveHeightCoefficient
                breakingWaveHeightAlgorithm
                breakingWaveHeightIntercept
                spectralRefractionMatrix
              }}
            }}
          }}
        }}'''
        headers = {'Content-Type': 'application/json'}
        response = requests.post(
            f'{self.url}/graphql', json=dict(query=query), headers=headers
        )
        response.raise_for_status()
        return [
            PointOfInterestGridPoint(
                point_of_interest['id'],
                float(point_of_interest['lon']),
                float(point_of_interest['lat']),
                grid_points_of_interest['grid'],
                point_of_interest['optimalSwellDirection'],
                point_of_interest['breakingWaveHeightCoefficient'],
                point_of_interest['breakingWaveHeightIntercept'],
                point_of_interest['breakingWaveHeightAlgorithm'],
                point_of_interest['spectralRefractionMatrix'],
            )
            for grid_points_of_interest in response.json()['data']['models'][
                0
            ]['grids']
            for point_of_interest in grid_points_of_interest[
                'pointsOfInterest'
            ]
            if point_of_interest['breakingWaveHeightAlgorithm']
            == 'SPECTRAL_REFRACTION'
        ]
