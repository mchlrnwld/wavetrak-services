from lib.models import Surf


def get_all_surf_heights(conn, run):
    with conn.cursor() as cur:
        cur.execute(
            (
                f"""
SELECT point_of_interest_id, breaking_wave_height_min, breaking_wave_height_max, forecast_time, grid
FROM surf
WHERE model = 'Lotus-WW3' and run = %s and breaking_wave_height_algorithm = 'SPECTRAL_REFRACTION';
        """
            ),
            [run],
        )
        points = cur.fetchall()
        return [
            Surf(
                point_of_interest_id=point[0],
                min=int(point[1]) if point[1] == 0.0 else float(point[1]),
                max=int(point[2]) if point[2] == 0.0 else float(point[2]),
                forecast_time=point[3],
                grid=point[4],
            )
            for point in points
        ]
