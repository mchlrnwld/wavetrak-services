from datetime import datetime
from typing import List, Optional

swell_partition_key_groups = [
    (
        'swell_wave_1_height',
        'swell_wave_1_period',
        'swell_wave_1_direction',
        'swell_wave_1_spread',
        'swell_wave_1_impact',
    ),
    (
        'swell_wave_2_height',
        'swell_wave_2_period',
        'swell_wave_2_direction',
        'swell_wave_2_spread',
        'swell_wave_2_impact',
    ),
    (
        'swell_wave_3_height',
        'swell_wave_3_period',
        'swell_wave_3_direction',
        'swell_wave_3_spread',
        'swell_wave_3_impact',
    ),
    (
        'swell_wave_4_height',
        'swell_wave_4_period',
        'swell_wave_4_direction',
        'swell_wave_4_spread',
        'swell_wave_4_impact',
    ),
    (
        'swell_wave_5_height',
        'swell_wave_5_period',
        'swell_wave_5_direction',
        'swell_wave_5_spread',
        'swell_wave_5_impact',
    ),
    (
        'wind_wave_height',
        'wind_wave_period',
        'wind_wave_direction',
        'wind_wave_spread',
        'wind_wave_impact',
    ),
]


class PointOfInterestGridPoint:
    """
    Point of interest and its grid point.

    Args:
        point_of_interest_id: Point of interest ID.
        lat: Grid point latitude as a float.
        lon: Grid point longitude as a float.

    Attributes:
        point_of_interest_id: Point of interest ID.
        lat: Grid point latitude as a float.
        lon: Grid point longitude as a float.
        optimal_swell_direction: Optimal swell direction as a float.
        breaking_wave_height_coefficient: Breaking wave multiplier as a float.
        breaking_wave_height_intercept: Breaking wave intercept as float.
        breaking_wave_height_algorithm: Breaking wave algorithms string.
        spectral_refraction_matrix: Spectral refraction matrix
                                    as list of float.
    """

    def __init__(
        self,
        point_of_interest_id: str,
        lon: float,
        lat: float,
        grid: str,
        optimal_swell_direction: Optional[float],
        breaking_wave_height_coefficient: Optional[float],
        breaking_wave_height_intercept: Optional[float],
        breaking_wave_height_algorithm: str,
        spectral_refraction_matrix: Optional[List[List[float]]],
    ):
        self.point_of_interest_id = point_of_interest_id
        self.lon = lon
        self.lat = lat
        self.grid = grid
        self.optimal_swell_direction = optimal_swell_direction
        self.breaking_wave_height_coefficient = (
            breaking_wave_height_coefficient
        )
        self.breaking_wave_height_intercept = breaking_wave_height_intercept
        self.breaking_wave_height_algorithm = breaking_wave_height_algorithm
        self.spectral_refraction_matrix = spectral_refraction_matrix


class Surf:
    """
    Point of interest and its grid point.

    Args:
        point_of_interest_id: Point of interest ID.
        lat: Grid point latitude as a float.
        lon: Grid point longitude as a float.

    Attributes:
        point_of_interest_id: Point of interest ID.
        lat: Grid point latitude as a float.
        lon: Grid point longitude as a float.
        optimal_swell_direction: Optimal swell direction as a float.
        breaking_wave_height_coefficient: Breaking wave multiplier as a float.
        breaking_wave_height_intercept: Breaking wave intercept as float.
        breaking_wave_height_algorithm: Breaking wave algorithms string.
        spectral_refraction_matrix: Spectral refraction matrix
                                    as list of float.
    """

    def __init__(
        self,
        point_of_interest_id: str,
        min: float,
        max: float,
        forecast_time: datetime,
        grid: str,
    ):
        self.id = point_of_interest_id
        self.min = min
        self.max = max
        self.forecast_time = forecast_time
        self.grid = grid

    def __str__(self):
        return f'point_of_interest_id: {self.id}, min: {self.min}, max: {self.max} forecast time: {self.forecast_time}'
