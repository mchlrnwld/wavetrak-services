import os

DATA_DIR = os.getenv('DATA_DIR', 'data')
AGENCY = os.environ['AGENCY']
MODEL = os.environ['MODEL']
GRIDS = os.environ['GRIDS'].split(',')
RUN = int(os.environ['RUN'])
FORECAST_HOURS = [
    int(forecast_hour)
    for forecast_hour in os.environ['FORECAST_HOURS'].split(',')
]
FULL_GRID_PREFIX = os.environ['FULL_GRID_PREFIX']
POINT_OF_INTEREST_PREFIX = os.environ['POINT_OF_INTEREST_PREFIX']
POSTGRES_CONNECTION_STRING = os.environ['POSTGRES_CONNECTION_STRING']
