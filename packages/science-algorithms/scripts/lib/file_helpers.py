import logging
import re

HOUR_REGEX = re.compile(rf'ww3_lotus_.+\.\d{{10}}\.(\d+)\.nc$')

logger = logging.getLogger('process-point-of-interest-data')


def get_hour_from_file(file: str) -> int:
    result = HOUR_REGEX.search(file)
    if not result:
        logger.error(f'Invalid file format: {file}')
        raise AttributeError(f'Invalid file format: {file}')
    return int(result.group(1))


def lon_lat_to_name(lon: float, lat: float) -> str:
    lon = lon % 360
    return f'{lon:07.3f}_{lat:07.3f}'


def create_spot_file_path(
    lon: float, lat: float, grid: str, run: int, prefix: str
) -> str:
    return (
        f'{prefix}/{grid}/{run}/'
        f'ww3_lotus_{grid}.spot_{lon_lat_to_name(lon, lat)}.nc'
    )


def create_full_grid_file_path(
    run: int, grid: str, prefix: str, forecast_hour: int
) -> str:
    return (
        f'{prefix}/{grid}/{run}/'
        f'ww3_lotus_{grid}.{run}.{str(forecast_hour).zfill(3)}.nc'
    )
