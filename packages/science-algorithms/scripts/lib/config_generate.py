import os
import random

DATA_DIR = os.getenv('DATA_DIR', 'data')
AGENCY = os.environ['AGENCY']
MODEL = os.environ['MODEL']
GRIDS = os.environ['GRIDS'].split(',')
RUN = int(os.environ['RUN'])
try:
    FORECAST_HOURS = [
        int(forecast_hour)
        for forecast_hour in os.environ['FORECAST_HOURS'].split(',')
    ]
except KeyError:
    FORECAST_HOURS = random.sample(range(384), 10)

POSTGRES_CONNECTION_STRING = os.environ['POSTGRES_CONNECTION_STRING']
SCIENCE_DATA_SERVICE = os.environ['SCIENCE_DATA_SERVICE']
FULL_GRID_PREFIX = os.environ['FULL_GRID_PREFIX']
POINT_OF_INTEREST_PREFIX = os.environ['POINT_OF_INTEREST_PREFIX']
SCIENCE_BUCKET = os.environ['SCIENCE_BUCKET']
TEST_CASE = os.getenv('SCIENCE_BUCKET', 2000)
