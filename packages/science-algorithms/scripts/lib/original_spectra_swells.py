import copy

import numpy as np
from scipy import interpolate
from scipy.spatial.distance import pdist

############################
#   Buoy to analyse        #
############################


############################
#   system definitions     #
############################

space_dir = 5  # in degrees
noise_constante_A = 0.00002
noise_constante_B = 0.5
spread_factor = 0.2
minimum_low_peak_factor = 0.65
overall_spectral_distance_merger_factor = 0.35
direction_space_limit = 10  # in degrees
frequency_space_limit = 0.02  # in Hz
dir_list = range(0, 360, space_dir)


def original_convert_spectra_to_swell_partitions(
    freq_list, bandwidth, spectral_wave_density, swr1, swr2, swdir1, swdir2
):

    step = 0

    DirWaveSpec = np.zeros((len(freq_list), len(dir_list)))
    freq_counter = 0
    for freq in freq_list:
        dir_counter = 0
        for direction in dir_list:
            dfa = (1 / np.pi) * (
                (
                    0.5
                    + (2 / 3)
                    * swr1[step, freq_counter]
                    * np.cos(
                        np.radians(direction)
                        - np.radians(swdir1[step, freq_counter])
                    )
                )
                + (1 / 6)
                * swr2[step, freq_counter]
                * np.cos(
                    2
                    * (
                        np.radians(direction)
                        - np.radians(swdir2[step, freq_counter])
                    )
                )
            )
            DirWaveSpec[freq_counter, dir_counter] = (
                spectral_wave_density[step, freq_counter] * dfa
            )
            dir_counter += 1
        freq_counter += 1

    # Find local maximums

    part_map_raw = np.zeros((len(freq_list), len(dir_list)))
    max_map_raw = np.zeros((len(freq_list), len(dir_list)))

    part_map = np.zeros((len(freq_list), len(dir_list)))
    max_map = np.zeros((len(freq_list), len(dir_list)))

    dirs_matrix, freqs_matrix = np.meshgrid(dir_list, freq_list)
    lixo, freqs_matrix_interval = np.meshgrid(dir_list, bandwidth)

    DirWaveSpec_temp = copy.deepcopy(DirWaveSpec)
    max_specs = []
    part_counter = 0

    for k in range(len(freq_list) * len(dir_list)):

        # sort in descendent order

        max_indices = np.where(DirWaveSpec_temp == DirWaveSpec_temp.max())
        max_index_freq = max_indices[0][0]
        max_index_dir = max_indices[1][0]

        if DirWaveSpec_temp[max_index_freq, max_index_dir] > 0:

            # find neighbors and wrap around only in XX axis
            down = max_index_freq - 1
            if down < 0:
                down = 0
            up = max_index_freq + 1
            if up == len(freq_list):
                up = len(freq_list) - 1
            left = max_index_dir - 1
            if left < 0:
                left = len(dir_list) - 1
            right = max_index_dir + 1
            if right == len(dir_list):
                right = 0

            xx = [
                left,
                left,
                left,
                max_index_dir,
                right,
                right,
                right,
                max_index_dir,
            ]
            yy = [up, max_index_freq, down, down, down, max_index_freq, up, up]

            neighbors = []
            for m in range(len(xx)):
                neighbors.append(DirWaveSpec[yy[m], xx[m]])

            alpha = (
                neighbors > DirWaveSpec[max_index_freq, max_index_dir]
            ).sum()
            if alpha == 0:
                part_counter += 1
                part_map_raw[max_index_freq, max_index_dir] = part_counter
                max_map_raw[max_index_freq, max_index_dir] = part_counter
                max_specs.append(DirWaveSpec[max_index_freq, max_index_dir])

            DirWaveSpec_temp[max_index_freq, max_index_dir] = 0

    # Fill the neighbors of the local maxs

    DirWaveSpec_temp = copy.deepcopy(DirWaveSpec)

    for k in range(len(freq_list) * len(dir_list)):

        # sort in descendent order

        max_indices = np.where(DirWaveSpec_temp == DirWaveSpec_temp.max())
        max_index_freq = max_indices[0][0]
        max_index_dir = max_indices[1][0]

        if DirWaveSpec_temp[max_index_freq, max_index_dir] > 0:

            # find neighbors and wrap around only in XX axis
            down = max_index_freq - 1
            if down < 0:
                down = 0
            up = max_index_freq + 1
            if up == len(freq_list):
                up = len(freq_list) - 1
            left = max_index_dir - 1
            if left < 0:
                left = len(dir_list) - 1
            right = max_index_dir + 1
            if right == len(dir_list):
                right = 0

            xx = [
                left,
                left,
                left,
                max_index_dir,
                right,
                right,
                right,
                max_index_dir,
            ]
            yy = [up, max_index_freq, down, down, down, max_index_freq, up, up]

            neighbors = []
            for m in range(len(xx)):
                if part_map_raw[yy[m], xx[m]] > 0:
                    neighbors.append(part_map_raw[yy[m], xx[m]])

            if neighbors:
                part_map_raw[max_index_freq, max_index_dir] = min(neighbors)

        DirWaveSpec_temp[max_index_freq, max_index_dir] = 0

    # Remove noise level partitions

    valid_partitions = np.unique(part_map_raw)
    valid_partitions = np.delete(valid_partitions, 0)

    for k in valid_partitions:
        alpha = np.nonzero(part_map_raw == k)
        pos_peak = np.nonzero(max_map_raw == k)
        energy_part_sum = sum(
            DirWaveSpec[alpha]
            * freqs_matrix_interval[alpha]
            * np.radians(space_dir)
        )
        noise_treshold = noise_constante_A / (
            freqs_matrix[pos_peak] ** 4 + noise_constante_B
        )
        if noise_treshold >= energy_part_sum:
            part_map_raw[alpha] = 0
            max_map_raw[pos_peak] = 0
            # print('Removed noise partition ' + str(k))

    # Renumber and reorder the partitions to fill the gaps in sequence from merging

    valid_partitions = np.unique(part_map_raw)
    valid_partitions = np.delete(valid_partitions, 0)

    energy_partitions = []
    energy_partitions_id = []

    for k in valid_partitions:
        alpha = np.nonzero(part_map_raw == k)
        energy_partitions.append(
            sum(
                DirWaveSpec[alpha]
                * freqs_matrix_interval[alpha]
                * np.radians(space_dir)
            )
        )
        energy_partitions_id.append(k)

    part_index = np.argsort(energy_partitions)

    part_map_raw = part_map_raw * 100
    max_map_raw = max_map_raw * 100

    counter = 1
    for k in part_index:

        alpha = np.nonzero(part_map_raw == energy_partitions_id[k] * 100)
        part_map_raw[alpha] = counter
        alpha = np.nonzero(max_map_raw == energy_partitions_id[k] * 100)
        max_map_raw[alpha] = counter
        counter += 1

    # Check if adjacent swells are in fact the same system and merge them
    # For that to happen they have to meet one of two criteria, the peak separation or minimum between peaks.

    # Calculate middle point and end point in freq_list

    middle = float(len(dir_list)) / 2
    if middle % 2 != 0:
        middle_point = int(middle - 0.5)
    else:
        middle_point = int(middle)
    end_point = int(len(dir_list))

    valid_partitions = np.unique(part_map_raw)
    valid_partitions = np.delete(valid_partitions, 0)

    xx_vector = np.linspace(0, len(dir_list) - 1, len(dir_list))
    yy_vector = np.linspace(0, len(freq_list) - 1, len(freq_list))
    xx_matrix, yy_matrix = np.meshgrid(xx_vector, yy_vector)

    part_flag_counter = 1

    for repeat_cycle in valid_partitions:

        valid_partitions = np.unique(part_map_raw)
        valid_partitions = np.delete(valid_partitions, 0)

        # print(' ')
        # print('Valid partitions for analysis')
        for k in valid_partitions:
            alpha = np.nonzero(part_map_raw == k)
            energy_integral = sum(
                DirWaveSpec[alpha]
                * freqs_matrix_interval[alpha]
                * np.radians(space_dir)
            )
            hs = 4 * np.sqrt(energy_integral)
            pos_peak = np.nonzero(max_map_raw == k)
            tp = 1 / freqs_matrix[pos_peak]
            pdir = dirs_matrix[pos_peak]
            # print(int(k), hs, tp[0], pdir[0])

        if len(valid_partitions) > 0:
            inverted_max_map_raw = np.hstack(
                [
                    max_map_raw[:, middle_point:end_point],
                    max_map_raw[:, 0:middle_point],
                ]
            )
            inverted_DirWaveSpec = np.hstack(
                [
                    DirWaveSpec[:, middle_point:end_point],
                    DirWaveSpec[:, 0:middle_point],
                ]
            )
            alpha_max_eval_std = np.nonzero(max_map_raw == valid_partitions[0])
            alpha_max_eval_inv = np.nonzero(
                inverted_max_map_raw == valid_partitions[0]
            )

            fpx = []
            fpy = []
            partTotalEnergy = []
            fx_mean = []
            fy_mean = []
            fx_mean_squared = []
            fy_mean_squared = []
            spread_fsquare = []
            delta_fsquare = []
            delta_fsquare_freq = []
            delta_fsquare_dir = []

            for k in range(len(valid_partitions)):

                pos_peak_swell = np.nonzero(max_map_raw == valid_partitions[k])
                pos_swell = np.nonzero(part_map_raw == valid_partitions[k])

                fpx.append(
                    freqs_matrix[pos_peak_swell]
                    * np.cos(np.radians(dirs_matrix[pos_peak_swell]))
                )
                fpy.append(
                    freqs_matrix[pos_peak_swell]
                    * np.sin(np.radians(dirs_matrix[pos_peak_swell]))
                )

                partTotalEnergy.append(
                    sum(
                        DirWaveSpec[pos_swell]
                        * freqs_matrix_interval[pos_swell]
                        * np.radians(space_dir)
                    )
                )

                fx_mean.append(
                    (1 / partTotalEnergy[k])
                    * sum(
                        DirWaveSpec[pos_swell]
                        * freqs_matrix[pos_swell]
                        * np.cos(np.radians(dirs_matrix[pos_swell]))
                        * freqs_matrix_interval[pos_swell]
                        * np.radians(space_dir)
                    )
                )
                fy_mean.append(
                    (1 / partTotalEnergy[k])
                    * sum(
                        DirWaveSpec[pos_swell]
                        * freqs_matrix[pos_swell]
                        * np.sin(np.radians(dirs_matrix[pos_swell]))
                        * freqs_matrix_interval[pos_swell]
                        * np.radians(space_dir)
                    )
                )

                fx_mean_squared.append(
                    (1 / partTotalEnergy[k])
                    * sum(
                        DirWaveSpec[pos_swell]
                        * (freqs_matrix[pos_swell] ** 2)
                        * (np.cos(np.radians(dirs_matrix[pos_swell])) ** 2)
                        * freqs_matrix_interval[pos_swell]
                        * np.radians(space_dir)
                    )
                )
                fy_mean_squared.append(
                    (1 / partTotalEnergy[k])
                    * sum(
                        DirWaveSpec[pos_swell]
                        * (freqs_matrix[pos_swell] ** 2)
                        * (np.sin(np.radians(dirs_matrix[pos_swell])) ** 2)
                        * freqs_matrix_interval[pos_swell]
                        * np.radians(space_dir)
                    )
                )

                spread_fsquare.append(
                    (
                        (fx_mean_squared[k] - fx_mean[k] ** 2)
                        + (fy_mean_squared[k] - fy_mean[k] ** 2)
                    )
                    * spread_factor
                )

            for k in range(len(valid_partitions)):
                delta_fsquare.append(
                    (fpx[0] - fpx[k]) ** 2 + (fpy[0] - fpy[k]) ** 2
                )
                delta_fsquare_freq.append((fpy[0] - fpy[k]) ** 2)
                delta_fsquare_dir.append((fpx[0] - fpx[k]) ** 2)

            flag_merge = 0

            for k in range(1, len(valid_partitions)):  # skip first partitions

                if delta_fsquare[k] <= overall_spectral_distance_merger_factor:
                    # Check for peak separation
                    if flag_merge == 0:
                        if delta_fsquare[k] <= spread_fsquare[k]:
                            flag_merge = 1
                            alpha = np.nonzero(
                                part_map_raw == valid_partitions[0]
                            )
                            # print('** Merged partition ' + str(valid_partitions[0]) + ' into partition ' + str(
                            #     valid_partitions[k]) + ' due to peak spread')
                            part_map_raw[alpha] = valid_partitions[k]
                            alpha = np.nonzero(
                                max_map_raw == valid_partitions[0]
                            )
                            max_map_raw[alpha] = 0
                    # Check minimum low between peaks
                    if flag_merge == 0:

                        alpha_max_floating_std = np.nonzero(
                            max_map_raw == valid_partitions[k]
                        )
                        alpha_max_floating_inv = np.nonzero(
                            inverted_max_map_raw == valid_partitions[k]
                        )

                        pos_dist_1_a = []
                        pos_dist_1_b = []
                        pos_dist_1_a.append(xx_matrix[alpha_max_eval_std][0])
                        pos_dist_1_a.append(yy_matrix[alpha_max_eval_std][0])
                        pos_dist_1_b.append(
                            xx_matrix[alpha_max_floating_std][0]
                        )
                        pos_dist_1_b.append(
                            yy_matrix[alpha_max_floating_std][0]
                        )

                        dist_1 = pdist(
                            [pos_dist_1_a, pos_dist_1_b], 'euclidean'
                        )

                        pos_dist_2_a = []
                        pos_dist_2_b = []
                        pos_dist_2_a.append(xx_matrix[alpha_max_eval_inv][0])
                        pos_dist_2_a.append(yy_matrix[alpha_max_eval_inv][0])
                        pos_dist_2_b.append(
                            xx_matrix[alpha_max_floating_inv][0]
                        )
                        pos_dist_2_b.append(
                            yy_matrix[alpha_max_floating_inv][0]
                        )

                        dist_2 = pdist(
                            [pos_dist_2_a, pos_dist_2_b], 'euclidean'
                        )

                        if dist_2 < dist_1:
                            xx_vector_bet_peaks = np.linspace(
                                xx_matrix[alpha_max_eval_inv],
                                xx_matrix[alpha_max_floating_inv],
                                100,
                            )
                            yy_vector_bet_peaks = np.linspace(
                                yy_matrix[alpha_max_eval_inv],
                                yy_matrix[alpha_max_floating_inv],
                                100,
                            )
                            frequency_space_bet_peaks = abs(
                                freqs_matrix[alpha_max_eval_inv]
                                - freqs_matrix[alpha_max_floating_inv]
                            )
                            direction_space_bet_peaks = abs(
                                dirs_matrix[alpha_max_eval_inv]
                                - dirs_matrix[alpha_max_floating_inv]
                            )
                        else:
                            xx_vector_bet_peaks = np.linspace(
                                xx_matrix[alpha_max_eval_std],
                                xx_matrix[alpha_max_floating_std],
                                100,
                            )
                            yy_vector_bet_peaks = np.linspace(
                                yy_matrix[alpha_max_eval_std],
                                yy_matrix[alpha_max_floating_std],
                                100,
                            )
                            frequency_space_bet_peaks = abs(
                                freqs_matrix[alpha_max_eval_std]
                                - freqs_matrix[alpha_max_floating_std]
                            )
                            direction_space_bet_peaks = abs(
                                dirs_matrix[alpha_max_eval_std]
                                - dirs_matrix[alpha_max_floating_std]
                            )

                        f = interpolate.interp2d(
                            xx_vector, yy_vector, DirWaveSpec, kind='linear'
                        )
                        connection_vector = f(
                            xx_vector_bet_peaks.flatten(),
                            yy_vector_bet_peaks.flatten(),
                        )

                        if (
                            np.ndarray.min(connection_vector)
                            > DirWaveSpec[alpha_max_eval_std]
                            * minimum_low_peak_factor
                            and direction_space_bet_peaks
                            < direction_space_limit
                            and frequency_space_bet_peaks
                            < frequency_space_limit
                        ):
                            flag_merge = 1
                            alpha = np.nonzero(
                                part_map_raw == valid_partitions[0]
                            )
                            # print('** Merged partition ' + str(valid_partitions[0]) + ' into partition ' + str(
                            #     valid_partitions[k]) + ' due to minimum between peaks')
                            part_map_raw[alpha] = valid_partitions[k]
                            alpha = np.nonzero(
                                max_map_raw == valid_partitions[0]
                            )
                            max_map_raw[alpha] = 0

            # Transfer partition to final mask if it has not been merged

            if flag_merge == 0:
                alpha = np.nonzero(part_map_raw == valid_partitions[0])
                part_map[alpha] = part_flag_counter
                part_map_raw[alpha] = 0
                alpha = np.nonzero(max_map_raw == valid_partitions[0])
                max_map[alpha] = part_flag_counter
                max_map_raw[alpha] = 0
                part_flag_counter += 1

            # Re-number and reorder the partitions to fill the gaps in sequence from merging

            valid_partitions = np.unique(part_map_raw)
            valid_partitions = np.delete(valid_partitions, 0)

            energy_partitions = []
            energy_partitions_id = []

            for k in valid_partitions:
                alpha = np.nonzero(part_map_raw == k)
                energy_partitions.append(
                    sum(
                        DirWaveSpec[alpha]
                        * freqs_matrix_interval[alpha]
                        * np.radians(space_dir)
                    )
                )
                energy_partitions_id.append(k)

            part_index = np.argsort(energy_partitions)

            part_map_raw = part_map_raw * 100
            max_map_raw = max_map_raw * 100

            counter = 1
            for k in part_index:

                alpha = np.nonzero(
                    part_map_raw == energy_partitions_id[k] * 100
                )
                part_map_raw[alpha] = counter
                alpha = np.nonzero(
                    max_map_raw == energy_partitions_id[k] * 100
                )
                max_map_raw[alpha] = counter
                counter += 1

    # Renumber and reorder the partitions to fill the gaps in sequence from merging

    valid_partitions = np.unique(part_map)
    valid_partitions = np.delete(valid_partitions, 0)

    energy_partitions = []
    energy_partitions_id = []

    for k in valid_partitions:
        alpha = np.nonzero(part_map == k)
        energy_partitions.append(
            sum(
                DirWaveSpec[alpha]
                * freqs_matrix_interval[alpha]
                * np.radians(space_dir)
            )
        )
        energy_partitions_id.append(k)

    energy_partitions = energy_partitions[::-1]
    # energy_partitions_id = energy_partitions_id[::-1]

    part_index = np.argsort(energy_partitions)

    part_map = part_map * 100
    max_map = max_map * 100

    counter = 1
    for k in part_index:
        alpha = np.nonzero(part_map == energy_partitions_id[k] * 100)
        part_map[alpha] = counter
        alpha = np.nonzero(max_map == energy_partitions_id[k] * 100)
        max_map[alpha] = counter
        counter += 1

    # print('  ')
    # print('##############################################################')
    # print('  ')
    # print('Overall Conditions:')

    alpha = np.nonzero(DirWaveSpec > 0)
    energy_integral = sum(
        DirWaveSpec[alpha]
        * freqs_matrix_interval[alpha]
        * np.radians(space_dir)
    )
    hs = 4 * np.sqrt(energy_integral)
    pos_peak = np.nonzero(max_map == 1)
    tp = 1 / freqs_matrix[pos_peak]
    pdir = dirs_matrix[pos_peak]
    # print(hs, tp[0], pdir[0])
    # print('  ')
    # print('Final Partitions:')

    valid_partitions = np.unique(part_map)
    valid_partitions = np.delete(valid_partitions, 0)

    swell_partitions = []

    for k in valid_partitions:
        alpha = np.nonzero(part_map == k)
        energy_integral = sum(
            DirWaveSpec[alpha]
            * freqs_matrix_interval[alpha]
            * np.radians(space_dir)
        )
        hs = 4 * np.sqrt(energy_integral)
        pos_peak = np.nonzero(max_map == k)
        tp = 1 / freqs_matrix[pos_peak]
        pdir = dirs_matrix[pos_peak]
        # print(hs, tp[0], pdir[0])
        # print(type(hs))
        # print(type(tp[0]))
        # print(type(pdir[0]))
        swell_partitions.append([hs, tp[0], int(pdir[0])])

    return swell_partitions
