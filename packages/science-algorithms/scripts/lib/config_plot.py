import os

FORTRAN_CSV_FILE_PATH = os.environ['FORTRAN_CSV_FILE_PATH']
PYTHON_CSV_FILE_PATH = os.environ['PYTHON_CSV_FILE_PATH']
RUN = int(os.environ['RUN'])
