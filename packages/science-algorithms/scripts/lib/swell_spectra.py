from typing import List, Tuple

import numpy as np  # type: ignore
from scipy import interpolate  # type: ignore


def interpolate_swell_spectra(
    frequency_hz: List[float],
    target_s: List[float],
    energy: List[float],
    direction: List[float],
    spread: List[float],
) -> Tuple[List[float], List[float], List[float]]:
    """
    Re-interpolates swell spectra data from Frequecy points (in Hz) to the
    passed in period points (in s).

    Args:
        frequency_hz: List of frequencies at which the energy/direction/spread
            values are specified.
        target_s: List of target values to interpolate to, in seconds.
        energy: List of energy values at each frequency, in any unit.
        direction: List of peak direction at each frequency, in degrees.
        spread: List of directional spread at each frequency, in degrees.

    Returns:
        Tuple of three lists floats, representing new interpolate values for
        energy, direction and spread.
    """
    target_s_np = np.array(target_s)
    input_s_np = 1.0 / np.array(frequency_hz)

    # Energy interpolation
    energy_interp_function = interpolate.interp1d(
        input_s_np, energy, kind='linear', bounds_error=False, fill_value=None
    )
    energy_interp = energy_interp_function(target_s_np)

    # Spread interpolation
    spread_interp_function = interpolate.interp1d(
        input_s_np, spread, kind='linear', bounds_error=False, fill_value=None
    )
    spread_interp = spread_interp_function(target_s_np)

    # Directional interpolation
    # Remove NaNs and replace by last value
    indices = np.where(np.isnan(direction))
    indices = indices[0][:]
    for index in indices:
        if index == 0:
            direction[index] = 0
        else:
            direction[index] = direction[index - 1]

    unwrapped_direction = np.rad2deg(np.unwrap(np.deg2rad(direction)))
    direction_interp_function = interpolate.interp1d(
        input_s_np,
        unwrapped_direction,
        kind='linear',
        bounds_error=False,
        fill_value=None,
    )
    direction_interp = direction_interp_function(target_s_np)
    direction_interp = direction_interp % 360

    # Remove the values where energy is NaN
    indices = np.where(np.isnan(energy_interp))
    indices = indices[0][:]
    for index in indices:
        direction_interp[index] = np.nan
        spread_interp[index] = np.nan

    return (
        np.nan_to_num(energy_interp).tolist(),
        np.nan_to_num(direction_interp).tolist(),
        np.nan_to_num(spread_interp).tolist(),
    )
