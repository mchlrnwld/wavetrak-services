from typing import Dict, List, Tuple

import numpy as np

from lib.constants import DEFAULT_SPECTRA_FREQUENCIES
from lib.swell_spectra import interpolate_swell_spectra
from netCDF4 import Dataset

FULL_GRID_VARIABLES = {
    'height': 'hs',
    'frequency': 'fp',
    'direction': 'dp',
    'swell_wave_1_height': 'phs0',
    'swell_wave_1_period': 'ptp0',
    'swell_wave_1_direction': 'pdir0',
    'swell_wave_1_spread': 'pspr0',
    'swell_wave_2_height': 'phs1',
    'swell_wave_2_period': 'ptp1',
    'swell_wave_2_direction': 'pdir1',
    'swell_wave_2_spread': 'pspr1',
    'swell_wave_3_height': 'phs2',
    'swell_wave_3_period': 'ptp2',
    'swell_wave_3_direction': 'pdir2',
    'swell_wave_3_spread': 'pspr2',
    'swell_wave_4_height': 'phs3',
    'swell_wave_4_period': 'ptp3',
    'swell_wave_4_direction': 'pdir3',
    'swell_wave_4_spread': 'pspr3',
    'swell_wave_5_height': 'phs4',
    'swell_wave_5_period': 'ptp4',
    'swell_wave_5_direction': 'pdir4',
    'swell_wave_5_spread': 'pspr4',
    'wind_wave_height': 'phs5',
    'wind_wave_period': 'ptp5',
    'wind_wave_direction': 'pdir5',
    'wind_wave_spread': 'pspr5',
}

SPOT_FILE_VARIABLES = {
    'height': 'sigH',
    'period': 'tp',
    'direction': 'pdir',
    'spectra_1d_energy': 'spectral_wave_density_1D',
    'spectra_1d_direction': 'mean_wave_direction_1D',
    'spectra_1d_directional_spread': 'directional_spread_1D',
    'spectra_2d': 'spectral_wave_density_2D',
}

SPOT_FILE_SWELL_VARIABLES = {
    'height': 'sigH_partition',
    'period': 'tp_partition',
    'direction': 'pdir_partition',
    'spread': 'wave_spread_partition',
}


def _parse_spot_variables(
    spot_file: Dataset, forecast_hours: List[int]
) -> Dict[str, List]:
    variables_with_nan = {
        column: np.transpose(
            spot_file.variables[spot_variable_name][:], axes=[2, 0, 1]
        )[forecast_hours]
        if column == 'spectra_2d'
        else spot_file.variables[spot_variable_name][forecast_hours]
        for (column, spot_variable_name) in SPOT_FILE_VARIABLES.items()
    }
    variables = {
        column: np.where(np.isnan(value), 0.0, value).tolist()
        for column, value in variables_with_nan.items()
    }
    target_s = np.linspace(2, 28, 27)
    interpolated_values = [
        interpolate_swell_spectra(
            DEFAULT_SPECTRA_FREQUENCIES,
            target_s,
            spectra_1d_energy,
            spectra_1d_direction,
            spectra_1d_directional_spread,
        )
        for (
            spectra_1d_energy,
            spectra_1d_direction,
            spectra_1d_directional_spread,
        ) in zip(
            variables['spectra_1d_energy'],
            variables['spectra_1d_direction'],
            variables['spectra_1d_directional_spread'],
        )
    ]

    variables['spectra_1d_energy'] = [
        spectra_1d_energy for spectra_1d_energy, _, _ in interpolated_values
    ]
    variables['spectra_1d_direction'] = [
        spectra_1d_direction
        for _, spectra_1d_direction, _ in interpolated_values
    ]
    variables['spectra_1d_directional_spread'] = [
        spectra_1d_directional_spread
        for _, _, spectra_1d_directional_spread in interpolated_values
    ]

    return variables


def _parse_swells_variables(
    spot_file: Dataset, forecast_hours: List[int]
) -> Dict[str, List]:
    unindexed_variables_with_nan = {
        column: np.transpose(
            spot_file.variables[spot_variable_name][forecast_hours]
        )
        for (column, spot_variable_name) in SPOT_FILE_SWELL_VARIABLES.items()
    }
    variables_with_nan = {
        (
            f'swell_wave_{i+1}_{column}' if i < 5 else f'wind_wave_{column}'
        ): values
        for column in unindexed_variables_with_nan.keys()
        for i, values in enumerate(unindexed_variables_with_nan[column])
    }
    variables = {
        column: np.where(np.isnan(value), 0.0, value).tolist()
        for column, value in variables_with_nan.items()
    }
    return variables


def parse_lotus_spot_file(
    local_spot_file_path: str, forecast_hours: List[int]
) -> Dict[str, List]:
    with Dataset(local_spot_file_path) as spot_file:
        spot_variables = _parse_spot_variables(spot_file, forecast_hours)
        swell_variables = _parse_swells_variables(spot_file, forecast_hours)
        return {**spot_variables, **swell_variables}


def parse_lotus_full_grid_file(
    local_file_path: str,
) -> Tuple[List[float], List[float], Dict[str, List[List[float]]]]:
    with Dataset(local_file_path) as lotus_ww3_file:
        lats = lotus_ww3_file.variables['latitude'][:].tolist()
        lons = lotus_ww3_file.variables['longitude'][:].tolist()
        data = {
            key: lotus_ww3_file.variables[value][0][:].filled(0.0).tolist()
            for key, value in FULL_GRID_VARIABLES.items()
        }
        return lats, lons, data
