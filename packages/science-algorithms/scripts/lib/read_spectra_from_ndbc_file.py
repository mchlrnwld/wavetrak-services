import numpy as np

from lib.original_spectra_swells import (
    original_convert_spectra_to_swell_partitions,
)


def read_nbdc_spec(inputfile):
    freq_list_temp = []
    bandwidth_temp = []
    spectral_wave_density_temp = []
    swr1_temp = []
    swr2_temp = []
    swdir1_temp = []
    swdir2_temp = []

    steps = 0
    all_data = []

    lines = 0

    with open(inputfile) as file:
        for file_line in file.readlines():
            if lines < 2:  # skip header
                lines += 1
                continue
            if file_line.strip():
                if file_line[0:2] == '20':
                    if steps == 0:
                        steps += 1
                        continue
                    spectral_wave_density_temp = np.array(
                        spectral_wave_density_temp
                    )
                    swr1_temp = np.array(swr1_temp)
                    swr2_temp = np.array(swr2_temp)
                    swdir1_temp = np.array(swdir1_temp)
                    swdir2_temp = np.array(swdir2_temp)

                    swr1_temp_copy = np.copy(swr1_temp)
                    swr2_temp_copy = np.copy(swr2_temp)
                    swdir1_temp_copy = np.copy(swdir1_temp)
                    swdir2_temp_copy = np.copy(swdir2_temp)

                    zero_energy = np.nonzero(spectral_wave_density_temp == 0)
                    swr1_temp[zero_energy] = 0
                    swr2_temp[zero_energy] = 0
                    swdir1_temp[zero_energy] = 0
                    swdir2_temp[zero_energy] = 0

                    size_spec = int(len(freq_list_temp))

                    spectral_wave_density = np.reshape(
                        spectral_wave_density_temp, (1, size_spec)
                    )
                    swr1 = np.reshape(swr1_temp, (1, size_spec))
                    swr2 = np.reshape(swr2_temp, (1, size_spec))
                    swdir1 = np.reshape(swdir1_temp, (1, size_spec))
                    swdir2 = np.reshape(swdir2_temp, (1, size_spec))
                    freq_list = freq_list_temp[0:size_spec]
                    bandwidth = bandwidth_temp[0:size_spec]

                    swell_partitions = original_convert_spectra_to_swell_partitions(
                        freq_list,
                        bandwidth,
                        spectral_wave_density,
                        swr1,
                        swr2,
                        swdir1,
                        swdir2,
                    )

                    all_data.append(
                        {
                            'freq_list': freq_list,
                            'bandwidth': bandwidth,
                            'spectral_wave_density': spectral_wave_density_temp.tolist(),
                            'swr1': swr1_temp_copy.tolist(),
                            'swr2': swr2_temp_copy.tolist(),
                            'swdir1': swdir1_temp_copy.tolist(),
                            'swdir2': swdir2_temp_copy.tolist(),
                            'swell_partitions': swell_partitions,
                        }
                    )

                    if steps == 5:
                        break

                    steps += 1

                    freq_list_temp = []
                    bandwidth_temp = []
                    spectral_wave_density_temp = []
                    swr1_temp = []
                    swr2_temp = []
                    swdir1_temp = []
                    swdir2_temp = []

                else:
                    numbers_float = file_line.split()
                    freq_list_temp.append(float(numbers_float[0]))
                    bandwidth_temp.append(float(numbers_float[1]))
                    spectral_wave_density_temp.append(float(numbers_float[2]))
                    swr1_temp.append(float(numbers_float[3]))
                    swr2_temp.append(float(numbers_float[4]))
                    swdir1_temp.append(float(numbers_float[5]))
                    swdir2_temp.append(float(numbers_float[6]))

    return all_data
