import asyncio
import os
from typing import Dict, List


async def _async_append_csv(csvpath: str, rows: List[Dict[str, object]]):
    loop = asyncio.get_event_loop()
    await loop.run_in_executor(None, partial(_append_csv, csvpath, rows))


def _append_csv(csvpath: str, rows: List[Dict[str, object]]):
    write_header = not os.path.exists(csvpath)

    with open(csvpath, 'a') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=list(rows[0].keys()))

        if write_header:
            writer.writeheader()

        writer.writerows(rows)


async def append_surf_heights_csv(
    csvpath: str, agency: str, model: str, all_surf_heights
):
    await _async_append_csv(
        csvpath,
        [
            {
                'point_of_interest_id': surf.id,
                'agency': agency,
                'model': model,
                'min': round(surf.min, 2),
                'max': round(surf.max, 2),
                'forecast_time': surf.forecast_time,
                'grid': surf.grid,
            }
            for surf in all_surf_heights
        ],
    )
