import asyncio
import os
from datetime import datetime, timedelta
from tempfile import TemporaryDirectory
from typing import List, Tuple

import numpy as np

import lib.config_validate as config
import psycopg2
from grib_helpers.process import get_lat_lon_index
from lib.constants import (
    DEFAULT_SPECTRA_DIRECTIONS,
    DEFAULT_SPECTRA_FREQUENCIES,
)
from lib.csv import append_surf_heights_csv
from lib.file_helpers import (
    create_full_grid_file_path,
    create_spot_file_path,
    get_hour_from_file,
)
from lib.models import Surf
from lib.parse_lotus_ww3_files import (
    parse_lotus_full_grid_file,
    parse_lotus_spot_file,
)
from lib.psql import get_all_surf_heights
from science_algorithms import (
    SurfHeight,
    SurfHeightInput,
    SwellPartition,
    approximate_spectra_from_swell_partitions,
    calculate_impacts,
)


class SurfAndSwellsBuilder:
    def __init__(self):
        self._surf_height = SurfHeight()

    def build_from_spot_data(
        self,
        points_of_interest_and_spot_files: List[
            Tuple[PointOfInterestGridPoint, str]
        ],
        forecast_hours: List[int],
        run_dt: datetime,
    ) -> List[Surf]:
        poi_data = [
            (poi, parse_lotus_spot_file(spot_file, forecast_hours))
            for poi, spot_file in points_of_interest_and_spot_files
        ]

        poi_surf_height_inputs: List[SurfHeightInput] = []
        all_pois = []
        all_forecast_hours = []
        for i, hour in enumerate(forecast_hours):
            for poi, spot_data in poi_data:
                swell_partitions = [
                    SwellPartition(
                        spot_data[h][i],
                        spot_data[p][i],
                        spot_data[d][i],
                        spot_data[s][i],
                    )
                    for h, p, d, s, _ in swell_partition_key_groups
                ]

                poi_surf_height_inputs.append(
                    SurfHeightInput(
                        poi.breaking_wave_height_algorithm,
                        swell_partitions,
                        poi.optimal_swell_direction,
                        poi.breaking_wave_height_coefficient,
                        poi.breaking_wave_height_intercept,
                        DEFAULT_SPECTRA_FREQUENCIES,
                        DEFAULT_SPECTRA_DIRECTIONS,
                        np.array(spot_data['spectra_2d'][i]),
                        np.array(poi.spectral_refraction_matrix or []),
                    )
                )
                all_forecast_hours.append(run_dt + timedelta(hours=hour))
                all_pois.append(poi)

        return [
            Surf(
                poi.point_of_interest_id,
                surf_height[0][0],
                surf_height[0][1],
                hour,
                poi.grid,
            )
            for poi, surf_height, hour in zip(
                all_pois,
                self._surf_height.calculate(poi_surf_height_inputs),
                all_forecast_hours,
            )
        ]

    def build_from_full_grid_data(
        self,
        indexed_points_of_interest: List[
            Tuple[PointOfInterestGridPoint, Tuple[int, int]]
        ],
        forecast_hour: int,
        full_grid_data: np.array,
    ) -> List[Surf]:
        poi_surf_height_inputs = []
        poi_swell_partitions = []
        for poi, (lat_index, lon_index) in indexed_points_of_interest:
            swell_partitions = [
                SwellPartition(
                    full_grid_data[h][lat_index][lon_index],
                    full_grid_data[p][lat_index][lon_index],
                    full_grid_data[d][lat_index][lon_index],
                    full_grid_data[s][lat_index][lon_index],
                )
                for h, p, d, s, _ in swell_partition_key_groups
            ]

            spectra_wave_energy = (
                np.array(
                    approximate_spectra_from_swell_partitions(swell_partitions)
                )
                if poi.breaking_wave_height_algorithm == 'SPECTRAL_REFRACTION'
                else None
            )

            poi_surf_height_inputs.append(
                SurfHeightInput(
                    poi.breaking_wave_height_algorithm,
                    swell_partitions,
                    poi.optimal_swell_direction,
                    poi.breaking_wave_height_coefficient,
                    poi.breaking_wave_height_intercept,
                    DEFAULT_SPECTRA_FREQUENCIES,
                    DEFAULT_SPECTRA_DIRECTIONS,
                    np.array(spectra_wave_energy),
                    np.array(poi.spectral_refraction_matrix or []),
                )
            )
        poi_swell_partitions.append(swell_partitions)

        return [
            Surf(
                poi[0].point_of_interest_id,
                surf_height[0][0],
                surf_height[0][1],
                forecast_hour,
                poi[0].grid,
            )
            for poi, surf_height in zip(
                indexed_points_of_interest,
                self._surf_height.calculate(poi_surf_height_inputs),
            )
        ]


async def main():
    if not os.path.exists(config.DATA_DIR):
        os.makedirs(config.DATA_DIR)

    with TemporaryDirectory(dir=config.DATA_DIR) as temp_dir, psycopg2.connect(
        POSTGRES_CONNECTION_STRING
    ) as conn:
        print('Started querying Postgres')
        fortran_surf_heights = get_all_surf_heights(
            conn, datetime.strptime(str(RUN), '%Y%m%d%H')
        )
        print('Finished querying Postgres')
        FORTRAN_SURF_HEIGHTS_CSV_FILE_PATH = os.path.join(
            config.DATA_DIR, f'fortran-{config.RUN}-surf-heights.csv'
        )

        await asyncio.gather(
            append_surf_heights_csv(
                FORTRAN_SURF_HEIGHTS_CSV_FILE_PATH,
                config.AGENCY,
                config.MODEL,
                fortran_surf_heights,
            )
        )

        surf_and_swells_builder = SurfAndSwellsBuilder(temp_dir)
        sds_client = SDS(
            config.SCIENCE_DATA_SERVICE, config.AGENCY, config.MODEL
        )
        points_of_interest_grid_points = sds_client.query_points_of_interest()
        unique_grid_points = list(
            {
                (point.lon, point.lat, point.grid)
                for point in points_of_interest_grid_points
            }
        )
        local_spot_files = [
            create_spot_file_path(lon, lat, grid, config.RUN, config.DATA_DIR)
            for lon, lat, grid in unique_grid_points
        ]
        s3_spot_files = [
            create_spot_file_path(
                lon, lat, grid, config.RUN, config.POINT_OF_INTEREST_PREFIX
            )
            for lon, lat, grid in unique_grid_points
        ]

        s3_client = S3Client(config.SCIENCE_BUCKET)

        print('downloading spot files')

        await asyncio.gather(
            *[
                s3_client.download_file(s3_file_path, local_file_path)
                for s3_file_path, local_file_path in zip(
                    s3_spot_files, local_spot_files
                )
            ],
            return_exceptions=True,
        )

        PYTHON_SURF_HEIGHTS_CSV_FILE_PATH = os.path.join(
            config.DATA_DIR, f'python-{config.RUN}-surf-heights.csv'
        )

        run_dt = datetime.strptime(str(config.RUN), '%Y%m%d%H')

        poi_with_spot_files: List[Tuple[PointOfInterestGridPoint, str]] = []
        poi_without_spot_files: List[PointOfInterestGridPoint] = []
        for poi in points_of_interest_grid_points:
            spot_file_local_path = create_spot_file_path(
                poi.lon, poi.lat, poi.grid, config.RUN, config.DATA_DIR
            )
            if os.path.exists(spot_file_local_path):
                poi_with_spot_files.append((poi, spot_file_local_path))
            else:
                poi_without_spot_files.append(poi)

        spot_surf_heights = surf_and_swells_builder.build_from_spot_data(
            poi_with_spot_files, config.FORECAST_HOURS, run_dt
        )

        if spot_surf_heights:
            await asyncio.gather(
                append_surf_heights_csv(
                    PYTHON_SURF_HEIGHTS_CSV_FILE_PATH,
                    config.AGENCY,
                    config.MODEL,
                    spot_surf_heights,
                )
            )

        if poi_without_spot_files:
            local_full_grid_files = [
                (
                    grid,
                    create_full_grid_file_path(
                        config.RUN, grid, config.DATA_DIR, forecast_hour
                    ),
                )
                for forecast_hour in config.FORECAST_HOURS
                for grid in config.GRIDS
            ]
            s3_full_grid_files = [
                (
                    grid,
                    create_full_grid_file_path(
                        config.RUN,
                        grid,
                        config.FULL_GRID_PREFIX,
                        forecast_hour,
                    ),
                )
                for forecast_hour in config.FORECAST_HOURS
                for grid in config.GRIDS
            ]

            print(f'downloading full grid files')

            await asyncio.gather(
                *[
                    s3_client.download_file(
                        s3_file_path[1], local_file_path[1]
                    )
                    for s3_file_path, local_file_path in zip(
                        s3_full_grid_files, local_full_grid_files
                    )
                ]
            )

        indexed_points_of_interest_by_grid = {}

        for grid, file in local_full_grid_files:
            forecast_hour = get_hour_from_file(file)
            lats, lons, data = parse_lotus_full_grid_file(file)
            if grid not in indexed_points_of_interest_by_grid:
                indexed_points_of_interest_by_grid[grid] = []
                for poi in [
                    poi for poi in poi_without_spot_files if poi.grid == grid
                ]:
                    try:

                        indexed_points_of_interest_by_grid[grid].append(
                            (
                                poi,
                                get_lat_lon_index(
                                    (poi.lat, poi.lon), lats, lons
                                ),
                            )
                        )
                    except ValueError:
                        pass

            if indexed_points_of_interest_by_grid[grid]:
                file_surf_heights = surf_and_swells_builder.build_from_full_grid_data(
                    indexed_points_of_interest_by_grid[grid],
                    run_dt + timedelta(hours=forecast_hour),
                    data,
                )
                total_surf_heights.extend(file_surf_heights)
                await asyncio.gather(
                    append_surf_heights_csv(
                        PYTHON_SURF_HEIGHTS_CSV_FILE_PATH,
                        config.AGENCY,
                        config.MODEL,
                        file_surf_heights,
                    )
                )


if __name__ == '__main__':
    asyncio.run(main())
