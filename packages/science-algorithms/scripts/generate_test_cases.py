import asyncio
import math
import os
import random
from datetime import datetime, timedelta
from tempfile import TemporaryDirectory
from typing import List, Tuple

import numpy as np

import lib.config_generate as config
import psycopg2
from grib_helpers.process import get_lat_lon_index
from lib.constants import (
    DEFAULT_SPECTRA_DIRECTIONS,
    DEFAULT_SPECTRA_FREQUENCIES,
)
from lib.file_helpers import (
    create_full_grid_file_path,
    create_spot_file_path,
    get_hour_from_file,
)
from lib.models import (
    PointOfInterestGridPoint,
    Surf,
    swell_partition_key_groups,
)
from lib.parse_lotus_ww3_files import (
    parse_lotus_full_grid_file,
    parse_lotus_spot_file,
)
from lib.psql import get_all_surf_heights
from lib.s3_client import S3Client
from lib.sds import SDS
from science_algorithms import (
    SurfHeight,
    SurfHeightInput,
    SwellPartition,
    approximate_spectra_from_swell_partitions,
    calculate_impacts,
)


class SurfAndSwellsBuilder:
    def __init__(self, data_dir: str):
        self._surf_height = SurfHeight()

    def _add_surf_height(
        self,
        all_data,
        fortran_surf_heights,
        poi,
        swell_partitions,
        spectra_data,
        hour,
    ):
        matrix = poi.spectral_refraction_matrix or []
        fortran_surf_height = None
        for surf_height in fortran_surf_heights:
            if (
                surf_height.id == poi.point_of_interest_id
                and surf_height.forecast_time == hour
                and surf_height.grid == poi.grid
            ):
                fortran_surf_height = surf_height
                break
        if fortran_surf_height:
            python_surf_heights = self._surf_height.calculate(
                [
                    SurfHeightInput(
                        poi.breaking_wave_height_algorithm,
                        swell_partitions,
                        poi.optimal_swell_direction,
                        poi.breaking_wave_height_coefficient,
                        poi.breaking_wave_height_intercept,
                        DEFAULT_SPECTRA_FREQUENCIES,
                        DEFAULT_SPECTRA_DIRECTIONS,
                        np.array(spectra_data),
                        np.array(matrix),
                    )
                ]
            )[0]

            if compare_surf_heights(
                (
                    round(python_surf_heights[0][0], 2),
                    round(python_surf_heights[0][1], 2),
                ),
                (
                    round(fortran_surf_height.min, 2),
                    round(fortran_surf_height.max, 2),
                ),
            ):
                surf_min = (
                    (
                        fortran_surf_height.min
                        - poi.breaking_wave_height_intercept
                    )
                    / poi.breaking_wave_height_coefficient
                    if fortran_surf_height.min > 0
                    else fortran_surf_height.min
                )
                surf_max = (
                    (
                        fortran_surf_height.max
                        - poi.breaking_wave_height_intercept
                    )
                    / poi.breaking_wave_height_coefficient
                    if fortran_surf_height.max > 0
                    else fortran_surf_height.max
                )
                all_data.append([spectra_data, matrix, [surf_min, surf_max]])

    def build_from_spot_data(
        self,
        fortran_surf_heights,
        points_of_interest_and_spot_files: List[
            Tuple[PointOfInterestGridPoint, str]
        ],
        forecast_hours: List[int],
        run_dt: datetime,
    ) -> List[Tuple[List[float], List[float], Tuple[float, float]]]:
        poi_data = [
            (poi, parse_lotus_spot_file(spot_file, forecast_hours))
            for poi, spot_file in points_of_interest_and_spot_files
        ]

        all_data = []
        for i, hour in enumerate(forecast_hours):
            for poi, spot_data in poi_data:
                swell_partitions = [
                    SwellPartition(
                        spot_data[h][i],
                        spot_data[p][i],
                        spot_data[d][i],
                        spot_data[s][i],
                    )
                    for h, p, d, s, _ in swell_partition_key_groups
                ]
                spectra_data = spot_data['spectra_2d'][i]
                self._add_surf_height(
                    all_data,
                    fortran_surf_heights,
                    poi,
                    swell_partitions,
                    spectra_data,
                    run_dt + timedelta(hours=hour),
                )

        return all_data

    def build_from_full_grid_data(
        self,
        fortran_surf_heights,
        indexed_points_of_interest: List[
            Tuple[PointOfInterestGridPoint, Tuple[int, int]]
        ],
        hour: datetime,
        full_grid_data: np.array,
        run: datetime,
    ) -> List[Tuple[List[float], List[float], Tuple[float, float]]]:
        all_data = []
        for poi, (lat_index, lon_index) in indexed_points_of_interest:
            swell_partitions = [
                SwellPartition(
                    full_grid_data[h][lat_index][lon_index],
                    full_grid_data[p][lat_index][lon_index],
                    full_grid_data[d][lat_index][lon_index],
                    full_grid_data[s][lat_index][lon_index],
                )
                for h, p, d, s, _ in swell_partition_key_groups
            ]

            spectra_data = (
                approximate_spectra_from_swell_partitions(swell_partitions)
            ).tolist()
            self._add_surf_height(
                all_data,
                fortran_surf_heights,
                poi,
                swell_partitions,
                spectra_data,
                hour,
            )

        return all_data


def compare_surf_heights(python_surf_heights, fortran_surf_heights) -> bool:
    return math.isclose(
        fortran_surf_heights[0],
        python_surf_heights[0],
        abs_tol=0.02,
        rel_tol=0.02,
    ) and math.isclose(
        fortran_surf_heights[1],
        python_surf_heights[1],
        abs_tol=0.02,
        rel_tol=0.02,
    )


async def main():
    if not os.path.exists(config.DATA_DIR):
        os.makedirs(config.DATA_DIR)

    with TemporaryDirectory(dir=config.DATA_DIR) as temp_dir, psycopg2.connect(
        config.POSTGRES_CONNECTION_STRING
    ) as conn:
        run_dt = datetime.strptime(str(config.RUN), '%Y%m%d%H')
        print('Started querying Postgres')
        fortran_surf_heights = get_all_surf_heights(
            conn, datetime.strptime(str(config.RUN), '%Y%m%d%H')
        )
        fortran_surf_heights = [
            surf
            for surf in fortran_surf_heights
            if surf.forecast_time
            in [
                run_dt + timedelta(hours=hour)
                for hour in config.FORECAST_HOURS
            ]
        ]
        print('Finished querying Postgres')
        surf_and_swells_builder = SurfAndSwellsBuilder(temp_dir)
        sds_client = SDS(
            config.SCIENCE_DATA_SERVICE, config.AGENCY, config.MODEL
        )
        points_of_interest_grid_points = sds_client.query_points_of_interest()
        unique_grid_points = list(
            {
                (point.lon, point.lat, point.grid)
                for point in points_of_interest_grid_points
            }
        )
        local_spot_files = [
            create_spot_file_path(lon, lat, grid, config.RUN, temp_dir)
            for lon, lat, grid in unique_grid_points
        ]
        s3_spot_files = [
            create_spot_file_path(
                lon, lat, grid, config.RUN, config.POINT_OF_INTEREST_PREFIX
            )
            for lon, lat, grid in unique_grid_points
        ]

        s3_client = S3Client(config.SCIENCE_BUCKET)

        print('downloading spot files')

        await asyncio.gather(
            *[
                s3_client.download_file(s3_file_path, local_file_path)
                for s3_file_path, local_file_path in zip(
                    s3_spot_files, local_spot_files
                )
            ],
            return_exceptions=True,
        )

        run_dt = datetime.strptime(str(config.RUN), '%Y%m%d%H')

        poi_with_spot_files: List[Tuple[PointOfInterestGridPoint, str]] = []
        poi_without_spot_files: List[PointOfInterestGridPoint] = []
        for poi in points_of_interest_grid_points:
            spot_file_local_path = create_spot_file_path(
                poi.lon, poi.lat, poi.grid, config.RUN, temp_dir
            )
            if os.path.exists(spot_file_local_path):
                poi_with_spot_files.append((poi, spot_file_local_path))
            else:
                poi_without_spot_files.append(poi)

        all_surf_heights = []

        spot_surf_heights = surf_and_swells_builder.build_from_spot_data(
            fortran_surf_heights,
            poi_with_spot_files,
            config.FORECAST_HOURS,
            run_dt,
        )

        all_surf_heights.extend(spot_surf_heights)

        if poi_without_spot_files:
            local_full_grid_files = [
                (
                    grid,
                    create_full_grid_file_path(
                        config.RUN, grid, temp_dir, forecast_hour
                    ),
                )
                for forecast_hour in config.FORECAST_HOURS
                for grid in config.GRIDS
            ]
            s3_full_grid_files = [
                (
                    grid,
                    create_full_grid_file_path(
                        config.RUN,
                        grid,
                        config.FULL_GRID_PREFIX,
                        forecast_hour,
                    ),
                )
                for forecast_hour in config.FORECAST_HOURS
                for grid in config.GRIDS
            ]

            print(f'downloading full grid files')

            await asyncio.gather(
                *[
                    s3_client.download_file(
                        s3_file_path[1], local_file_path[1]
                    )
                    for s3_file_path, local_file_path in zip(
                        s3_full_grid_files, local_full_grid_files
                    )
                ]
            )

        indexed_points_of_interest_by_grid = {}

        for grid, file in local_full_grid_files:
            forecast_hour = get_hour_from_file(file)
            lats, lons, data = parse_lotus_full_grid_file(file)
            if grid not in indexed_points_of_interest_by_grid:
                indexed_points_of_interest_by_grid[grid] = []
                for poi in [
                    poi for poi in poi_without_spot_files if poi.grid == grid
                ]:
                    try:

                        indexed_points_of_interest_by_grid[grid].append(
                            (
                                poi,
                                get_lat_lon_index(
                                    (poi.lat, poi.lon), lats, lons
                                ),
                            )
                        )
                    except ValueError:
                        pass

            if indexed_points_of_interest_by_grid[grid]:
                file_surf_heights = surf_and_swells_builder.build_from_full_grid_data(
                    [
                        surf
                        for surf in fortran_surf_heights
                        if surf.grid == grid
                    ],
                    indexed_points_of_interest_by_grid[grid],
                    run_dt + timedelta(hours=forecast_hour),
                    data,
                    run_dt,
                )
                all_surf_heights.extend(file_surf_heights)

        indexes = random.sample(range(len(all_surf_heights) - 1), 2000)

        if not os.path.exists(config.DATA_DIR):
            os.makedirs(config.DATA_DIR)

        with open('data/spectral_refraction_pois.json', 'a') as file:
            file.write(f'{[all_surf_heights[index] for index in indexes]}')


if __name__ == '__main__':
    asyncio.run(main())
