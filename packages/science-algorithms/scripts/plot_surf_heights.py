import csv
import math

import lib.config_plot as config
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter


def read_csv(filepath):
    with open(filepath, mode='r') as csv_file:
        csv_reader = csv.reader(csv_file)
        return set(tuple(row) for row in csv_reader)


def get_change(current, previous):
    try:
        return (abs(current - previous) / previous) * 100.0
    except ZeroDivisionError:
        return 0


python_surf_heights = read_csv(config.PYTHON_CSV_FILE_PATH)
fortran_surf_heights = read_csv(config.FORTRAN_CSV_FILE_PATH)

python_only_surf_heights = python_surf_heights - fortran_surf_heights
fortran_only_surf_heights = fortran_surf_heights - python_surf_heights

differences = python_surf_heights - fortran_surf_heights

actual_diffs = []

# Compare both
for python_surf_height in python_only_surf_heights:
    for fortran_surf_height in fortran_only_surf_heights:
        if (
            fortran_surf_height[0] == python_surf_height[0]
            and fortran_surf_height[5] == python_surf_height[5]
            and fortran_surf_height[6] == python_surf_height[6]
        ):
            if math.isclose(
                float(fortran_surf_height[3]),
                float(python_surf_height[3]),
                abs_tol=0.4,
                rel_tol=0.4,
            ) and math.isclose(
                float(fortran_surf_height[4]),
                float(python_surf_height[4]),
                abs_tol=0.4,
                rel_tol=0.4,
            ):
                min_diff = get_change(
                    float(fortran_surf_height[3]), float(python_surf_height[3])
                )
                max_diff = get_change(
                    float(fortran_surf_height[4]), float(python_surf_height[4])
                )
                if max_diff < min_diff:
                    actual_diffs.append(min_diff)
                else:
                    actual_diffs.append(max_diff)

plt.hist(x=actual_diffs)
plt.xlabel('Percent Difference Between Surf Heights')
plt.ylabel('Count')
plt.title(f'{config.RUN} Total Errors')
plt.gca().xaxis.set_major_formatter(PercentFormatter(100))
plt.show()
