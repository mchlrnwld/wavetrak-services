import json
import random
import urllib.request
from typing import List

import requests
from bs4 import BeautifulSoup  # type: ignore
from lib.read_spectra_from_ndbc_file import read_nbdc_spec


def parse_ndbc_ids(ndbc_page: str) -> List[str]:
    """
    Parses all NDBC IDs from the 5day2 data page on NDBC.

    Args:
        ndbc_page: NDBC HTML page.

    Returns:
        List of NDBC IDs.
    """
    response = requests.get(ndbc_page)
    ndbc_soup = BeautifulSoup(response.text, 'lxml')
    a_elements = ndbc_soup.find_all('a')

    return [
        anchor.text.split('_')[0]
        for anchor in a_elements
        if '_5day.spectral' in anchor.text
    ]


buoy_numbers = parse_ndbc_ids('https://www.ndbc.noaa.gov/data/5day2/')
buoy_spectra_files = []
for buoy_number in buoy_numbers:
    urllib.request.urlretrieve(
        'http://www.ndbc.noaa.gov/data/5day2/'
        + buoy_number
        + '_5day.spectral',
        f'data/{buoy_number}_spec_file.txt',
    )
    buoy_spectra_files.append(f'data/{buoy_number}_spec_file.txt')

all_data = []

for buoy_spectra_file in buoy_spectra_files:
    file_data = read_nbdc_spec(buoy_spectra_file)
    all_data.extend(file_data)

indexes = random.sample(range(len(all_data) - 1), 500)

with open('data/buoys_spectra.json', 'a') as file:
    file.write(json.dumps([all_data[index] for index in indexes]))
