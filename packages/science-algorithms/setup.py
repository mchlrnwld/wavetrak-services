import setuptools  # type: ignore

with open('README.md', 'r') as f:
    long_description = f.read()

setuptools.setup(
    name='wavetrak-science-algorithms',
    version='4.1.4',
    author='Wavetrak',
    author_email='nerdery@surfline.com',
    description='A library of useful science algorithms.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url=(
        'https://github.com/Surfline/wavetrak-services/common/'
        'wavetrak-science-algorithms'
    ),
    packages=setuptools.find_packages(),
    install_requires=['numpy>=1.17.3', 'scipy>=1.4.1'],
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
    ],
    package_data={'': ['py.typed']},
)
