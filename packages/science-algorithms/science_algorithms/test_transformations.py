import math

import numpy as np  # type: ignore

import science_algorithms.transformations as t
from science_algorithms.enums import (
    FogType,
    PrecipitationType,
    SkyConditions,
    ThunderType,
)


def test_kelvin_to_celsius():
    temperature = np.array([315, 273.15, 215])

    assert [315 - 273.15, 0, 215 - 273.15] == t.kelvin_to_celsius(
        temperature
    ).tolist()


def test_pascal_to_millibar():
    pressure = np.array([1, 0, -1])

    assert [0.01, 0, -0.01] == t.pascal_to_millibar(pressure).tolist()


def test_get_precipitation_type():
    precipitation_rate = np.array([1, 1, 1, 1, 0])
    snow = np.array([1, 0, 0, 0, 0]) > 0
    ice_pellets = np.array([0, 1, 0, 0, 0]) > 0
    freezing_rain = np.array([0, 0, 1, 0, 0]) > 0

    assert (
        [
            PrecipitationType.SNOW,
            PrecipitationType.FREEZING_RAIN,
            PrecipitationType.ICE_PELLETS,
            PrecipitationType.RAIN,
            PrecipitationType.NONE,
        ]
        == t.get_precipitation_type(
            precipitation_rate, snow, ice_pellets, freezing_rain
        ).tolist()
    )


def test_get_hourly_precipitation():
    current_precipitation = np.array([[2, 6], [10, 14]])
    current_details = (
        '80:Total Precipitation:kg m**-2 (accum)'
        ':regular_ll:surface:level 0:fcst time 0-'
        '2 hrs (accum):from 201903190600'
    )

    assert [[1, 3], [5, 7]] == t.get_hourly_precipitation(
        current_precipitation, current_details, None, None
    ).tolist()

    previous_precipitation = np.array([[2, 1], [1, 10]])
    previous_details = (
        '80:Total Precipitation:kg m**-2 (accum)'
        ':regular_ll:surface:level 0:fcst time 0-'
        '1 hrs (accum):from 201903190600'
    )

    assert [[0, 5], [9, 4]] == t.get_hourly_precipitation(
        current_precipitation,
        current_details,
        previous_precipitation,
        previous_details,
    ).tolist()


def test_get_forecast_interval():
    details = (
        '80:Total Precipitation:kg m**-2 (accum):regular_ll:surface'
        ':level 0:fcst time 0-3 hrs (accum):from 201903190600'
    )

    assert (0, 3) == t.get_forecast_interval(details)


def test_get_marginal_hourly_precipitation():
    current_precipitation = np.array([[11, 7], [6, 4]])
    current_interval = (3, 9)
    previous_precipitation = np.array([[2, 1], [3, 4]])
    previous_interval = (3, 6)

    assert [[3, 2], [1, 0]] == t.get_marginal_hourly_precipitation(
        current_precipitation,
        current_interval,
        previous_precipitation,
        previous_interval,
    ).tolist()


def test_get_wind_magnitude():
    wind_u = np.array([-2, 0, 2, 0, 2])
    wind_v = np.array([2, 0, -2, 2, 0])

    assert [math.sqrt(8), 0, math.sqrt(8), 2, 2] == t.get_wind_magnitude(
        wind_u, wind_v
    ).tolist()


def test_get_storm_wind_magnitude():
    storm_wind_u = np.array([-2, 0, 2, 0, 2])
    storm_wind_v = np.array([2, 0, -2, 2, 0])

    assert [4, 0, 4, 0, 0] == t.get_storm_wind_magnitude(
        storm_wind_u, storm_wind_v
    ).tolist()


def test_get_thunder_type():
    lifted_index = np.array(
        [
            [-1, -2, -2],
            [-2, -2, -2],
            [-2, -2, -2],
            [-2, -2, -2],
            [-2, -2, -2],
            [-2, -2, -2],
            [-2, -2, -2],
            [-2, -2, -2],
        ]
    )
    cape_surface = np.array(
        [
            [201, 201, 201],
            [200, 0, 201],
            [201, 201, 201],
            [201, 201, 201],
            [201, 201, 201],
            [2499, 2500, 201],
            [1999, 2000, 201],
            [499, 1499, 1500],
        ]
    )
    cape_ml = np.array(
        [
            [201, 201, 201],
            [0, 200, 201],
            [201, 201, 201],
            [201, 201, 201],
            [201, 201, 201],
            [201, 201, 201],
            [201, 201, 201],
            [201, 201, 201],
        ]
    )
    vertical_velocity_750mb = np.array(
        [
            [-0.26, -0.26, -0.26],
            [-0.26, -0.26, -0.26],
            [-0.25, 0, -0.26],
            [-0.26, -0.26, -0.26],
            [-0.26, -0.26, -0.26],
            [-0.26, -0.26, -0.26],
            [-0.26, -0.26, -0.26],
            [-0.26, -0.26, -0.26],
        ]
    )
    vertical_velocity_800mb = np.array(
        [
            [-0.26, -0.26, -0.26],
            [-0.26, -0.26, -0.26],
            [0, -0.25, -0.26],
            [-0.26, -0.26, -0.26],
            [-0.26, -0.26, -0.26],
            [-0.26, -0.26, -0.26],
            [-0.26, -0.26, -0.26],
            [-0.26, -0.26, -0.26],
        ]
    )
    dewpoint = np.array(
        [
            [12, 12, 12],
            [12, 12, 12],
            [12, 12, 12],
            [11, 12, 12],
            [12, 12, 12],
            [12, 12, 12],
            [12, 12, 12],
            [12, 12, 12],
        ]
    )
    convective_inhibition = np.array(
        [
            [-199, -199, -199],
            [-199, -199, -199],
            [-199, -199, -199],
            [-199, -199, -199],
            [-200, -199, -199],
            [-199, -199, -199],
            [-199, -199, -199],
            [-199, -199, -199],
        ]
    )
    storm_wind_magnitude = np.array(
        [
            [20, 20, 20],
            [20, 20, 20],
            [20, 20, 20],
            [20, 20, 20],
            [20, 20, 20],
            [20, 20, 20],
            [49, 49, 49],
            [50, 50, 50],
        ]
    )

    results = t.get_thunder_type(
        lifted_index,
        cape_surface,
        cape_ml,
        vertical_velocity_750mb,
        vertical_velocity_800mb,
        dewpoint,
        convective_inhibition,
        storm_wind_magnitude,
    )

    # No thunderstorms if lifted index > -2.
    assert [ThunderType.NONE, ThunderType.SHOWERS] == results[0][:2].tolist()

    # No thunderstorms if maximum of CAPE <= 200.
    assert [
        ThunderType.NONE,
        ThunderType.NONE,
        ThunderType.SHOWERS,
    ] == results[1][:3].tolist()

    # No thunderstorms if minimum of vertical velocity >= -2.5.
    assert [
        ThunderType.NONE,
        ThunderType.NONE,
        ThunderType.SHOWERS,
    ] == results[2][:3].tolist()

    # No thunderstorms if dewpoint <= 11.
    assert [ThunderType.NONE, ThunderType.SHOWERS] == results[3][:2].tolist()

    # No thunderstorms if convective inhibition <= -200.
    assert [ThunderType.NONE, ThunderType.SHOWERS] == results[4][:2].tolist()

    # Weak wind
    # Thunder showers if storm wind magnitude < 21 and CAPE < 2500.
    assert ThunderType.SHOWERS == results[5][0]

    # Heavy thunder showers if storm wind magnitude < 21 and CAPE >= 2500.
    assert ThunderType.HEAVY_SHOWERS == results[5][1]

    # Moderate wind
    # Thunder storms if 21 <= storm wind magnitude < 50 and CAPE < 2000.
    assert ThunderType.STORMS == results[6][0]

    # Thunder storms if 21 <= storm wind magnitude < 50 and CAPE >= 2000.
    assert ThunderType.HEAVY_STORMS == results[6][1]

    # Strong wind
    # Thunder showers if storm wind magnitude >= 50 and CAPE < 500.
    assert ThunderType.SHOWERS == results[7][0]

    # Heavy thunder storms if storm wind magnitude >= 50 and
    # 500 <= CAPE < 1500.
    assert ThunderType.HEAVY_STORMS == results[7][1]

    # Severe thunder storms if storm wind magnitude >= 50 and CAPE >= 1500.
    assert ThunderType.SEVERE_STORMS == results[7][2]


def test_get_net_radiation():
    down_short_rad_flux = np.array([2, 3, 4, 5])
    down_long_rad_flux = np.array([1, 2, 3, 4])
    up_short_rad_flux = np.array([3, 2, 1, 0])
    up_long_rad_flux = np.array([4, 3, 2, 1])

    # Returns downward radiation flux less upward radiation flux.
    assert [-4, 0, 4, 8] == t.get_net_radiation(
        down_short_rad_flux,
        down_long_rad_flux,
        up_short_rad_flux,
        up_long_rad_flux,
    ).tolist()


def test_get_average_low_layer_relative_humidity():
    relative_humidity_925mb = np.array([0, 1])
    relative_humidity_950mb = np.array([1, 2])
    relative_humidity_975mb = np.array([2, 3])
    relative_humidity_1000mb = np.array([3, 4])

    assert [1.5, 2.5] == t.get_average_low_layer_relative_humidity(
        relative_humidity_925mb,
        relative_humidity_950mb,
        relative_humidity_975mb,
        relative_humidity_1000mb,
    ).tolist()


def test_get_fog_type():
    wind_magnitude = np.array(
        [[3.8, 3.8, 3.8], [0, 1, 1], [0, 0, 0], [0, 0, 0], [0, 0, 0]]
    )
    relative_humidity_surface = np.array(
        [
            [97.4, 97.5, 97.5],
            [97.5, 97.4, 97.5],
            [97.4, 97.5, 97.4],
            [95.1, 96.4, 97.5],
            [0, 0, 0],
        ]
    )
    net_radiation = np.array(
        [[1, 0, 1], [-1, -1, -1], [1, 1, 1], [0, 0, 0], [0, -1, 0]]
    )
    average_low_layer_relative_humidity = np.array(
        [
            [0, 0, 0],
            [95.1, 95.1, 95.1],
            [97.4, 97.4, 97.4],
            [97.4, 97.4, 97.4],
            [97.5, 97.5, 97.5],
        ]
    )

    results = t.get_fog_type(
        net_radiation,
        average_low_layer_relative_humidity,
        relative_humidity_surface,
        wind_magnitude,
    )

    # Strong Wind
    # Mist only if relative humidity at surface is wet and net radiation not
    # positive.
    assert [FogType.NONE, FogType.NONE, FogType.MIST] == results[0].tolist()

    # Dry Lower Layer
    # Mist only if wind magnitude >= 1 and relative humidity at surface is wet.
    assert [FogType.NONE, FogType.NONE, FogType.MIST] == results[1].tolist()

    # Middle Wet Lower Layer
    # For positive net radiation: mist when relative humidity at surface is wet
    # , otherwise none.
    assert [FogType.NONE, FogType.MIST] == results[2][:2].tolist()

    # None if relative humidity at surface is dry. Mist if not wet.
    # Otherwise fog.
    assert [FogType.NONE, FogType.MIST, FogType.FOG] == results[3].tolist()

    # Wet Lower Layer
    # Fog if negative net radiation. Mist otherwise.
    assert [FogType.MIST, FogType.FOG] == results[4][:2].tolist()


def test_get_optical_depth():
    temperature = np.array(
        [[223.14, 233.14, 243.14, 258.14], [258.15, 258.15, 258.15, 258.15]]
    )
    is_land = np.array(
        [[False, False, False, False], [True, False, False, False]]
    )
    cloud_mixing_ratio = np.array([[0.1, 0.2, 0.3, 0.4], [0.5, 0.6, 0.7, 0.8]])
    thickness = np.array([[1, 2, 3, 4], [5, 6, 7, 8]])

    results = t.get_optical_depth(
        temperature, is_land, cloud_mixing_ratio, thickness
    )

    # Ice clouds
    a_3 = 3.33 * 10 ** (-4)
    a_4 = 2.52

    def iwc(temperature):
        return 1.371 * 10 ** ((0.04962 * temperature) - 14)

    def iwp(cloud_mixing_ratio, thickness):
        return cloud_mixing_ratio * 917 * thickness

    # Ice clouds result depends on temperature.
    assert [
        (
            iwp(0.1, 1)
            * (a_3 + (a_4 / ((1250 / 9.917) * iwc(223.14) ** 0.109)))
        ),
        (iwp(0.2, 2) * (a_3 + (a_4 / ((1250 / 9.337) * iwc(233.14) ** 0.08)))),
        (
            iwp(0.3, 3)
            * (a_3 + (a_4 / ((1250 / 9.208) * iwc(243.14) ** 0.055)))
        ),
        (
            iwp(0.4, 4)
            * (a_3 + (a_4 / ((1250 / 9.387) * iwc(258.14) ** 0.031)))
        ),
    ] == results[0].tolist()

    # Water clouds
    a_1 = -6.59 * 10 ** (-3)
    a_2 = 1.65

    def lwp(cloud_mixing_ratio, thickness):
        return cloud_mixing_ratio * 1000 * thickness

    # Water clouds result depends on temperature only if over land.
    assert [
        lwp(0.5, 5) * (a_1 + (a_2 / ((-0.25 * 258.15) + 73.29))),
        lwp(0.6, 6) * (a_1 + (a_2 / 10)),
    ] == results[1][:2].tolist()


def test_get_total_optical_density():
    is_land = np.array([False])
    layer_temperatures = {
        '100 mb': np.array([0]),
        '150 mb': np.array([1]),
        '200 mb': np.array([2]),
        '250 mb': np.array([3]),
        '300 mb': np.array([4]),
        '350 mb': np.array([5]),
        '400 mb': np.array([6]),
        '450 mb': np.array([7]),
        '500 mb': np.array([8]),
        '550 mb': np.array([9]),
        '600 mb': np.array([10]),
        '650 mb': np.array([11]),
        '700 mb': np.array([12]),
        '750 mb': np.array([13]),
        '800 mb': np.array([14]),
        '850 mb': np.array([15]),
        '900 mb': np.array([16]),
        '925 mb': np.array([17]),
        '950 mb': np.array([18]),
        '975 mb': np.array([19]),
        '1000 mb': np.array([20]),
    }
    layer_cloud_mixing_ratios = {
        '100 mb': np.array([0]),
        '150 mb': np.array([1]),
        '200 mb': np.array([2]),
        '250 mb': np.array([3]),
        '300 mb': np.array([4]),
        '350 mb': np.array([5]),
        '400 mb': np.array([6]),
        '450 mb': np.array([7]),
        '500 mb': np.array([8]),
        '550 mb': np.array([9]),
        '600 mb': np.array([10]),
        '650 mb': np.array([11]),
        '700 mb': np.array([12]),
        '750 mb': np.array([13]),
        '800 mb': np.array([14]),
        '850 mb': np.array([15]),
        '900 mb': np.array([16]),
        '925 mb': np.array([17]),
        '950 mb': np.array([18]),
        '975 mb': np.array([19]),
        '1000 mb': np.array([20]),
    }
    layer_heights = {
        '100 mb': np.array([0]),
        '150 mb': np.array([1]),
        '200 mb': np.array([2]),
        '250 mb': np.array([3]),
        '300 mb': np.array([4]),
        '350 mb': np.array([5]),
        '400 mb': np.array([6]),
        '450 mb': np.array([7]),
        '500 mb': np.array([8]),
        '550 mb': np.array([9]),
        '600 mb': np.array([10]),
        '650 mb': np.array([11]),
        '700 mb': np.array([12]),
        '750 mb': np.array([13]),
        '800 mb': np.array([14]),
        '850 mb': np.array([15]),
        '900 mb': np.array([16]),
        '925 mb': np.array([17]),
        '950 mb': np.array([18]),
        '975 mb': np.array([19]),
        '1000 mb': np.array([20]),
    }

    # TODO: Actual value when removing rounding: [-105598.98666417829]
    assert [-105664.722185924] == t.get_total_optical_density(
        is_land, layer_temperatures, layer_cloud_mixing_ratios, layer_heights
    ).tolist()


def test_get_sky_conditions():
    total_optical_density = np.array([2, 3, 3, 3, 3])
    total_cloud_cover = np.array([11, 10, 50, 90, 91])

    assert (
        [
            SkyConditions.CLEAR,
            SkyConditions.CLEAR,
            SkyConditions.PARTLY_CLOUDY,
            SkyConditions.MOSTLY_CLOUDY,
            SkyConditions.OVERCAST,
        ]
        == t.get_sky_conditions(
            total_optical_density, total_cloud_cover
        ).tolist()
    )
