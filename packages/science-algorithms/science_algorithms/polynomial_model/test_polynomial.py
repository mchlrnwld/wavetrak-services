from science_algorithms.polynomial_model.polynomial import (
    partition_wave_height_poly,
)
from science_algorithms.types import SwellPartition


def test_partition_wave_height_poly():
    def poly4dp(s, a):
        return round(partition_wave_height_poly(s, a), 4)

    # Assert range of swell height/period angle combinations, all with optimal
    # beach angle = 0
    assert 0.7268 == poly4dp(SwellPartition(0.5, 6, 0), 0)
    assert 3.9358 == poly4dp(SwellPartition(5, 6, 0), 0)

    assert 1.03 == poly4dp(SwellPartition(0.5, 12, 0), 0)
    assert 5.756 == poly4dp(SwellPartition(5, 12, 0), 0)

    assert 1.3492 == poly4dp(SwellPartition(0.5, 20, 0), 0)
    assert 7.6152 == poly4dp(SwellPartition(5, 20, 0), 0)

    assert 0.6775 == poly4dp(SwellPartition(0.5, 6, 30), 0)
    assert 3.6692 == poly4dp(SwellPartition(5, 6, 30), 0)

    assert 0.9599 == poly4dp(SwellPartition(0.5, 12, 30), 0)
    assert 5.3655 == poly4dp(SwellPartition(5, 12, 30), 0)

    assert 1.2535 == poly4dp(SwellPartition(0.5, 20, 30), 0)
    assert 7.0924 == poly4dp(SwellPartition(5, 20, 30), 0)

    # ... and some positive and negative beach angle adjustments

    assert 1.2535 == poly4dp(SwellPartition(0.5, 20, 40), 10)
    assert 7.0924 == poly4dp(SwellPartition(5, 20, 40), 10)

    assert 1.2535 == poly4dp(SwellPartition(0.5, 20, 20), 350)
    assert 7.0924 == poly4dp(SwellPartition(5, 20, 20), 350)

    # Check for 0 when swell partition passed in with None values
    assert 0 == partition_wave_height_poly(
        SwellPartition(None, None, None), 350
    )
