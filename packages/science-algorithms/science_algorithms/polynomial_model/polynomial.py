from science_algorithms.polynomial_model._polynomial_matrix import _MATRIX
from science_algorithms.types import SwellPartition


def partition_wave_height_poly(
    swell_partition: SwellPartition, optimal_swell_direction: float
) -> float:
    """
    Calculate breaking wave height for a single swell partition using a
    polynomial algorithm, and optimal swell direction.

    Args:
        swell_partition: An instance of SwellPartition.
        optimal_swell_direction: Angle, in degrees, that is optimal for a swell
            to be coming from. E.g. 270 means swell coming from the west.

    Returns:
        Peak breaking wave height, in meters.
    """
    d = int(round((swell_partition.direction - optimal_swell_direction) / 5))
    d = d % 72

    coeff = _MATRIX[d]
    h = swell_partition.height
    p = swell_partition.period

    breaking_wave = (
        coeff[0]
        + coeff[1] * h
        + coeff[2] * p
        + coeff[3] * h * h
        + coeff[4] * h * p
        + coeff[5] * p * p
        + coeff[6] * h * h * h
        + coeff[7] * h * h * p
        + coeff[8] * h * p * p
        + coeff[9] * p * p * p
        + coeff[10] * h * h * h * h
        + coeff[11] * h * h * h * p
        + coeff[12] * h * h * p * p
        + coeff[13] * h * p * p * p
        + coeff[14] * p * p * p * p
        + coeff[15] * h * h * h * h * h
        + coeff[16] * h * h * h * h * p
        + coeff[17] * h * h * h * p * p
        + coeff[18] * h * h * p * p * p
        + coeff[19] * h * p * p * p * p
        + coeff[20] * p * p * p * p * p
    )

    return max(0, breaking_wave)
