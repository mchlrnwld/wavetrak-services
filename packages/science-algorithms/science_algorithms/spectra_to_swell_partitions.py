from typing import Any, Callable, List, Tuple

import numpy as np  # type: ignore
from scipy import interpolate  # type: ignore
from scipy.spatial.distance import pdist  # type: ignore

from science_algorithms.types import SwellPartition

# NOTE: This algorithm is based on Automated Analysis of Ocean Surface
# Directional Wave Spectra by Jeffrey L. Hanson and Owen M. Phillips.
# DOI: https://doi.org/10.1175/1520-0426(2001)018<0277:AAOOSD>2.0.CO;2

# Values updated per https://wavetrak.atlassian.net/browse/FE-752
SPACE_DIRECTION = 5  # in degrees
NOISE_A = 0.00002
NOISE_B = 0.5
SPREAD_FACTOR = 0.4
MINIMUM_LOW_PEAK_FACTOR = 0.65
DIRECTION_SPACE_LIMIT = 25  # in degrees
PERIOD_SPACE_LIMIT = 2  # in seconds
DIRECTIONS = range(0, 360, SPACE_DIRECTION)


def _calculate_energy_partition_sum(
    directional_wave_spectra: Any,
    frequency_matrix_interval: Any,
    partition_index: float,
) -> float:
    """
    Calculate energy partitions sum.

    Args:
        directional_wave_spectra: 2D varying length numpy array of the
                                  directional wave spectra.
        frequency_matrix_interval: 2D varying length numpy array of the
                                   frequency matrix interval.
        partition_index: Index of the partition to calculate the energy sum.

    Returns:
        Energy partition summation.
    """
    return sum(
        directional_wave_spectra[partition_index]
        * frequency_matrix_interval[partition_index]
        * np.radians(SPACE_DIRECTION)
    )


def _reorder_partitions(
    partitions: Any,
    peak_of_partitions: Any,
    directional_wave_spectra: Any,
    frequency_matrix_interval: Any,
    reverse: bool,
) -> Tuple[Any, Any]:
    """
    Re-number and reorder the partitions to fill the gaps in sequence from
    merging.

    Args:
        partitions: 2D varying length numpy array of partition data.
        peak_of_partitions: 2D varying length numpy array of peak partition
                            data.
        directional_wave_spectra: 2D varying length numpy array of the
                                  directional wave spectra.
        frequency_matrix_interval: 2D varying length numpy array of the
                                   frequency matrix interval.
        reverse: Bool to determine if to reverse energy partitions.

    Returns:
        A tuple of partitions and peak partitions reordered.
    """
    valid_partitions = _get_unique_partitions(partitions)
    energy_partitions = [
        _calculate_energy_partition_sum(
            directional_wave_spectra,
            frequency_matrix_interval,
            np.nonzero(partitions == partition),
        )
        for partition in valid_partitions
    ]

    if reverse:
        energy_partitions = energy_partitions[::-1]

    sorted_energy_partitions_indexes = np.argsort(energy_partitions)

    partitions_100 = partitions * 100
    peak_of_partitions_100 = peak_of_partitions * 100
    # Sort the partitions and peak of partitons according to the order
    # given by the sorted energy partitions.
    for counter, partition in enumerate(
        sorted_energy_partitions_indexes, start=1
    ):
        alpha = np.nonzero(partitions_100 == valid_partitions[partition] * 100)
        partitions_100[alpha] = counter
        alpha = np.nonzero(
            peak_of_partitions_100 == valid_partitions[partition] * 100
        )
        peak_of_partitions_100[alpha] = counter

    return partitions_100, peak_of_partitions_100


def _reorder_partitions_peak_energy(
    partitions: Any,
    peak_of_partitions: Any,
    directional_wave_spectra: Any,
    frequency_matrix_interval: Any,
    reverse: bool,
) -> Tuple[Any, Any]:
    """
    Re-number and reorder the partitions to fill the gaps in sequence from
    merging, but using the peak energy value as the sorting element and not
    the overall energy of the partition.

    Args:
        partitions: 2D varying length numpy array of partition data.
        peak_of_partitions: 2D varying length numpy array of peak partition
                            data.
        directional_wave_spectra: 2D varying length numpy array of the
                                  directional wave spectra.
        frequency_matrix_interval: 2D varying length numpy array of the
                                   frequency matrix interval.
        reverse: Bool to determine if to reverse energy partitions.

    Returns:
        A tuple of partitions and peak partitions reordered.
    """
    valid_partitions = _get_unique_partitions(partitions)
    max_energy_partitions = [
        max(directional_wave_spectra[np.nonzero(partitions == partition)])
        for partition in valid_partitions
    ]

    if reverse:
        max_energy_partitions.reverse()

    sorted_max_energy_partitions_indexes = np.argsort(max_energy_partitions)

    partitions_100 = partitions * 100
    peak_of_partitions_100 = peak_of_partitions * 100
    # Sort the partitions and peak of partitons according to the order
    # given by the sorted energy partitions.
    for counter, partition in enumerate(
        sorted_max_energy_partitions_indexes, start=1
    ):
        alpha = np.nonzero(partitions_100 == valid_partitions[partition] * 100)
        partitions_100[alpha] = counter
        alpha = np.nonzero(
            peak_of_partitions_100 == valid_partitions[partition] * 100
        )
        peak_of_partitions_100[alpha] = counter

    return partitions_100, peak_of_partitions_100


def _find_neighbor_indices(
    frequencies: Any, directions: Any, directional_wave_spectra: Any
) -> Tuple[int, int, List[int], List[int]]:
    """
    Find neighbor indices for frequencies and directions based on the max
    indices.

    Args:
        frequencies: 1D varying length numpy array of frequencies.
        directions: 1D varying length numpy array of directions.
        directional_wave_spectra: 2D varying length numpy array of directional
                                  wave spectra.

    Returns:
        Tuple of lists with the neighbor indices for frequencies and
        directions and the max indexes for both.
    """
    max_indices = np.where(
        directional_wave_spectra == directional_wave_spectra.max()
    )
    max_frequency_index = max_indices[0][0]
    max_direction_index = max_indices[1][0]

    down = max_frequency_index - 1 if max_frequency_index > 0 else 0
    up = (
        max_frequency_index + 1
        if max_frequency_index + 1 < len(frequencies)
        else len(frequencies) - 1
    )
    left = (
        max_direction_index - 1
        if max_direction_index > 0
        else len(directions) - 1
    )
    right = (
        max_direction_index + 1
        if max_direction_index + 1 < len(directions)
        else 0
    )

    return (
        max_frequency_index,
        max_direction_index,
        [
            left,
            left,
            left,
            max_direction_index,
            right,
            right,
            right,
            max_direction_index,
        ],
        [
            up,
            max_frequency_index,
            down,
            down,
            down,
            max_frequency_index,
            up,
            up,
        ],
    )


def _fill_partitions_with_directional_wave_spectra_max(
    frequencies: Any,
    directions: Any,
    directional_wave_spectra: Any,
    partitions: Any,
    peak_of_partitions: Any,
) -> Tuple[Any, Any]:
    """
    Fill partitions and peak partitions with maxes from directional wave
    spectra.

    Args:
        frequencies: 1D varying length numpy array of frequencies.
        directions: 1D varying length numpy array of directions.
        directional_wave_spectra: 2D varying length numpy array of directional
                                  wave spectra.
        partitions: 2D varying length numpy array of partitions.
        peak_of_partitions: 2D varying length numpy array of peak partitions.

    Returns:
        Tuple of filled partitions and peak partitions.
    """
    partition_counter = 0
    directional_wave_spectra_copy = np.copy(directional_wave_spectra)
    for _ in range(len(frequencies) * len(directions)):
        (
            frequency_max_index,
            direction_max_index,
            neighbor_x,
            neighbor_y,
        ) = _find_neighbor_indices(
            frequencies, directions, directional_wave_spectra_copy
        )
        if (
            directional_wave_spectra_copy[
                frequency_max_index, direction_max_index
            ]
            > 0
        ):
            neighbors = [
                directional_wave_spectra[y, x]
                for y, x in zip(neighbor_y, neighbor_x)
            ]

            alpha = (
                neighbors
                > directional_wave_spectra[
                    frequency_max_index, direction_max_index
                ]
            ).sum()
            if alpha == 0:
                partition_counter += 1
                partitions[
                    frequency_max_index, direction_max_index
                ] = partition_counter
                peak_of_partitions[
                    frequency_max_index, direction_max_index
                ] = partition_counter

            directional_wave_spectra_copy[
                frequency_max_index, direction_max_index
            ] = 0
    return partitions, peak_of_partitions


def _fill_partitions_with_neighbors(
    frequencies: Any,
    directions: Any,
    directional_wave_spectra: Any,
    partitions: Any,
) -> Any:
    """
    Fill partitions with neighbors based on frequencies, directions and
    directional_wave_spectra.

    Args:
        frequencies: 1D varying length numpy array of frequencies.
        directions: 1D varying length numpy array of directions.
        directional_wave_spectra: 2D varying length numpy array of directional
                                  wave spectra.
        partitions: 2D varying length numpy array of partitions.
        peak_of_partitions: 2D varying length numpy array of peak partitions.

    Returns:
        2D numpy array of filled partitions.
    """
    partitions_copy = np.copy(partitions)
    directional_wave_spectra_copy = np.copy(directional_wave_spectra)
    for _ in range(len(frequencies) * len(directions)):
        (
            frequency_max_index,
            direction_max_index,
            neighbor_x,
            neighbor_y,
        ) = _find_neighbor_indices(
            frequencies, directions, directional_wave_spectra_copy
        )

        if (
            directional_wave_spectra_copy[
                frequency_max_index, direction_max_index
            ]
            > 0
        ):
            neighbors = [
                partitions_copy[y, x]
                for y, x in zip(neighbor_y, neighbor_x)
                if partitions_copy[y, x] > 0
            ]

            if neighbors:
                partitions_copy[
                    frequency_max_index, direction_max_index
                ] = min(neighbors)

            directional_wave_spectra_copy[
                frequency_max_index, direction_max_index
            ] = 0
    return partitions_copy


def _calculate_directional_wave_spectra(
    frequencies: Any,
    directions: Any,
    r1: Any,
    r2: Any,
    spectral_wave_density: Any,
    alpha1: Any,
    alpha2: Any,
) -> Any:
    """
    Calculate directional wave spectra given frequency, directions, spectra
    wave density and coefficients.

    Args:
        frequencies: 1D varying length numpy array of frequencies.
        directions: 1D varying length numpy array of directions.
        r1: 1D varying length numpy array of r1 coefficient used to calculate
             partitions.
        r2: 1D varying length numpy array of r2 coefficient used to calculate
            partitions.
        alpha1: 1D varying length numpy array of alpha1 coefficient used to
                calculate partitions.
        alpha2: 1D varying length numpy array of alpha2 coefficient used to
                calculate partitions.

    Returns:
        2D numpy array of the directional wave spectra.
    """
    zero_energy = np.nonzero(spectral_wave_density == 0)
    r1[zero_energy] = 0
    r2[zero_energy] = 0
    alpha1[zero_energy] = 0
    alpha2[zero_energy] = 0
    directional_wave_spectra = np.zeros((len(frequencies), len(directions)))
    for frequency_index, _ in enumerate(frequencies):
        for direction_index, direction in enumerate(directions):
            dfa = (1 / np.pi) * (
                (
                    0.5
                    + (2 / 3)
                    * r1[frequency_index]
                    * np.cos(
                        np.radians(direction)
                        - np.radians(alpha1[frequency_index])
                    )
                )
                + (1 / 6)
                * r2[frequency_index]
                * np.cos(
                    2
                    * (
                        np.radians(direction)
                        - np.radians(alpha2[frequency_index])
                    )
                )
            )
            directional_wave_spectra[frequency_index, direction_index] = (
                spectral_wave_density[frequency_index] * dfa
            )
    return directional_wave_spectra


def _calculate_mean(
    directional_wave_spectra: Any,
    positional_swell: Any,
    frequency_matrix: Any,
    directional_matrix: Any,
    factor: float,
    frequency_matrix_interval: Any,
    trig_function: Callable,
) -> float:
    """
    Calculate the mean for the given directional_wave_spectra, frequency_matrix
    and directional_matrix.

    Args:
        directional_wave_spectra: 2D varying length numpy array of the
                                  directional wave spectra.
        positional_swell: 2D varying length numpy array of the positional
                          swell.
        frequency_matrix: 2D varying length numpy array of the frequency
                          matrix.
        directional_matrix: 2D varying length numpy array of directional
                            matrix.
        factor: Multiplication factor used in calculation.
        frequency_matrix_interval: 2D varying length numpy array of the
                                   frequency matrix interval.
        trig_function: Numpy trig calculation function (ex: np.sin or np.cos).

    Returns:
       The mean for the inputs given.
    """
    return (
        1
        / (
            sum(
                directional_wave_spectra[positional_swell]
                * frequency_matrix_interval[positional_swell]
                * np.radians(SPACE_DIRECTION)
            )
        )
        * sum(
            directional_wave_spectra[positional_swell]
            * (frequency_matrix[positional_swell] ** factor)
            * (
                trig_function(np.radians(directional_matrix[positional_swell]))
                ** factor
            )
            * frequency_matrix_interval[positional_swell]
            * np.radians(SPACE_DIRECTION)
        )
    )


def _calculate_swell_partitions(
    partitions: Any,
    directional_wave_spectra: Any,
    frequency_matrix_interval: Any,
    peak_of_partitions: Any,
    directional_matrix: Any,
    frequency_matrix: Any,
) -> List[SwellPartition]:
    """
    Calculate swell partitions height, period, and direction from the given
    partitions and directional_wave_spectra.

    Args:
        partitions: 2D varying length numpy array of partitions.
        directional_wave_spectra: 2D varying length numpy array of directional
                                  wave spectra.
        frequency_matrix_interval: 2D varying length numpy array of the
                                   frequency matrix interval.
        peak_of_partitions: 2D varying length numpy array of peak partitions.
        directional_matrix: 2D varying length numpy array of the directional
                            matrix.
        frequency_matrix: 2D varying length numpy array of the frequency
                          matrix.

    Returns:
        A list of SwellPartitions.
    """
    swell_partitions = []

    for partition in _get_unique_partitions(partitions):
        alpha = np.nonzero(partitions == partition)
        energy_integral = _calculate_energy_partition_sum(
            directional_wave_spectra, frequency_matrix_interval, alpha
        )
        height = 4 * np.sqrt(energy_integral)
        positional_peak = np.nonzero(peak_of_partitions == partition)
        period = 1 / frequency_matrix[positional_peak]
        direction = directional_matrix[positional_peak]
        swell_partitions.append(
            SwellPartition(height, period[0], direction[0])
        )
    return swell_partitions


def _remove_noisy_partitions(
    partitions: Any,
    directional_wave_spectra: Any,
    frequency_matrix_interval: Any,
    noise_constants: Tuple[float, float],
    frequency_matrix: Any,
    peak_of_partitions: Any,
) -> Tuple[Any, Any]:
    """
    Remove partitions above the given noise threshold.

    Args:
        partitions: 2D varying length numpy array of partitions.
        directional_wave_spectra: 2D varying length numpy array of directional
                                  wave spectra.
        frequency_matrix_interval: 2D varying length numpy array of frequency
                                   matrix interval.
        frequency_matrix: 2D varying length numpy array of frequency matrix.
        peak_of_partitions: 2D varying length numpy array of peak partitions.

    Returns:
        Partitions and peak partitions with noisy partitions removed.
    """
    partitions_copy = np.copy(partitions)
    peak_of_partitions_copy = np.copy(peak_of_partitions)
    for partition in _get_unique_partitions(partitions):
        alpha = np.nonzero(partitions == partition)
        positional_peak = np.nonzero(peak_of_partitions == partition)
        energy_partition_sum = _calculate_energy_partition_sum(
            directional_wave_spectra, frequency_matrix_interval, alpha
        )
        noise_threshold = noise_constants[0] / (
            frequency_matrix[positional_peak] ** 4 + noise_constants[1]
        )
        if noise_threshold >= energy_partition_sum:
            partitions_copy[alpha] = 0
            peak_of_partitions_copy[positional_peak] = 0

    return partitions_copy, peak_of_partitions_copy


def _get_unique_partitions(partitions: Any) -> Any:
    """
    Get unique partitions after the first element.

    Args:
        partitions: 2D numpy array of partitions.

    Returns:
        Numpy array of unique partitions.
    """
    return np.unique(partitions)[1:]


def _calculate_frequency_delta_and_spread(
    frequency_matrix: Any,
    directional_matrix: Any,
    directional_wave_spectra: Any,
    frequency_matrix_interval: Any,
    peak_of_partitions: Any,
    partitions: Any,
) -> Tuple[Any, Any]:
    """
    Calculate the frequency delta and spread from the partitions,
    peak_of_partitions, frequency_matrix and directional_matrix.

    Args:
        frequency_matrix: 2D numpy array of the frequency matrix.
        directional_matrix: 2D numpy array of the directional matrix.
        directional_wave_spectra: 2D numpy array of the directional wave
                                  spectra.
        frequency_matrix_interval: 2D numpy array of the frequency_matrix
                                   interval.
        peak_of_partitions: 2D numpy array of peak partitions.
        partitions: 2D numpy array of partitions.

    Returns:
        Tuple of frequency_delta and frequency_spread.
    """
    (frequency_spread, peak_frequency_x, peak_frequency_y) = ([], [], [])

    valid_partitions = _get_unique_partitions(partitions)

    for partition_index, partition in enumerate(valid_partitions):
        positional_peak_swell = np.nonzero(peak_of_partitions == partition)
        positional_swell = np.nonzero(partitions == partition)

        peak_frequency_x.append(
            frequency_matrix[positional_peak_swell]
            * np.cos(np.radians(directional_matrix[positional_peak_swell]))
        )
        peak_frequency_y.append(
            frequency_matrix[positional_peak_swell]
            * np.sin(np.radians(directional_matrix[positional_peak_swell]))
        )

        frequency_spread.append(
            (
                (
                    _calculate_mean(
                        directional_wave_spectra,
                        positional_swell,
                        frequency_matrix,
                        directional_matrix,
                        2,
                        frequency_matrix_interval,
                        np.cos,
                    )
                    - _calculate_mean(
                        directional_wave_spectra,
                        positional_swell,
                        frequency_matrix,
                        directional_matrix,
                        1,
                        frequency_matrix_interval,
                        np.cos,
                    )
                    ** 2
                )
                + (
                    _calculate_mean(
                        directional_wave_spectra,
                        positional_swell,
                        frequency_matrix,
                        directional_matrix,
                        2,
                        frequency_matrix_interval,
                        np.sin,
                    )
                    - _calculate_mean(
                        directional_wave_spectra,
                        positional_swell,
                        frequency_matrix,
                        directional_matrix,
                        1,
                        frequency_matrix_interval,
                        np.sin,
                    )
                    ** 2
                )
            )
            * SPREAD_FACTOR
        )

    return (
        [
            (peak_frequency_x[0] - peak_frequency_x[partition_index]) ** 2
            + (peak_frequency_y[0] - peak_frequency_y[partition_index]) ** 2
            for partition_index, _ in enumerate(valid_partitions)
        ],
        frequency_spread,
    )


def _get_vectors_between_peaks(
    alpha_max_eval: Tuple[Any, Any],
    alpha_max_floating: Tuple[Any, Any],
    xx_matrix: Any,
    yy_matrix: Any,
    frequency_matrix: Any,
    directional_matrix: Any,
) -> Tuple[Any, Any, Any, Any]:
    """
    Get vectors between peak partitions for x, y, frequency. and directional
    matrices.

    Args:
        alpha_max_eval: Tuple of 1 x 1 numpy arrays of the eval max alpha.
        alpha_max_floating: Tuple of 1 x 1 numpt arrays of the floating max
                            alpha.
        xx_matrix: 2D varying length numpy array of the xx_matrix.
        yy_matrix: 2D varying length numpy array of the yy_matrix.
        frequency_matrix: 2D varying length numpy array of the frequency
                          matrix.
        directional_matrix: 2D varying length numpy array of the directional
                            matrix.

    Returns:
        Tuple of numpy arrays for the peaks.
    """
    xx_vector_between_peaks = np.linspace(
        xx_matrix[alpha_max_eval], xx_matrix[alpha_max_floating], 100
    )
    yy_vector_between_peaks = np.linspace(
        yy_matrix[alpha_max_eval], yy_matrix[alpha_max_floating], 100
    )
    period_space_between_peaks = abs(
        1 / frequency_matrix[alpha_max_eval]
        - 1 / frequency_matrix[alpha_max_floating]
    )
    directional_space_between_peaks = abs(
        directional_matrix[alpha_max_eval]
        - directional_matrix[alpha_max_floating]
    )
    return (
        xx_vector_between_peaks,
        yy_vector_between_peaks,
        period_space_between_peaks,
        directional_space_between_peaks,
    )


def _get_euclidean_distance(
    xx_matrix: Any,
    yy_matrix: Any,
    alpha_max_eval: Tuple[int, int],
    alpha_max_floating: Tuple[int, int],
) -> Any:
    """
    Calculates the euclidean distance for xx_matrix and yy_matrix.

    Args:
        xx_matrix: 2D varying length numpy array of the xx_matrix.
        yy_matrix: 2D varying length numpy array of the yy_matrix.
        alpha_max_eval: Tuple of 1 x 1 numpy array of the eval max
                        alpha.
        alpha_max_floating: Tuple of 1 x 1 numpy array of the floating max
                            alpha.

    Returns:
        A numpy array of distance values.
    """
    return pdist(
        [
            [xx_matrix[alpha_max_eval][0], yy_matrix[alpha_max_eval][0]],
            [
                xx_matrix[alpha_max_floating][0],
                yy_matrix[alpha_max_floating][0],
            ],
        ],
        'euclidean',
    )


def spectra_to_swell_partitions(
    frequencies: Any,
    bandwidth: Any,
    spectral_wave_density: Any,
    r1: Any,
    r2: Any,
    alpha1: Any,
    alpha2: Any,
) -> List[SwellPartition]:
    """
    Converts spectra data into swell partitions. Up to 6 partitions may be
    given depending on the spectra input.
    NOTE: The inputs for the spectra_to_swell_partitions function are named
    after the NDBC formatted spectra for wave buoys. See example:
    https://www.ndbc.noaa.gov/data/5day2/51213_5day.spectral

    Args:
        frequencies: 1D varying length numpy array of frequencies.
        bandwidth: 1D varying length numpy array of bandwith.
        spectral_wave_density: 1D varying length numpy array of spectral wave
                               density.
        r1: 1D varying length numpy array of r1 coefficient used to calculate
            partitions.
        r2: 1D varying length numpy array of r2 coefficient used to calculate
            partitions.
        alpha1: 1D varying length numpy array of alpha1 coefficient used to
                calculate partitions.
        alpha2: 1D varying length numpy array of alpha2 coefficient used to
                calculate partitions.

    Returns:
        A list of SwellPartitions.
    """
    if any(
        len(input) != len(spectral_wave_density)
        for input in [
            frequencies,
            bandwidth,
            spectral_wave_density,
            r1,
            r2,
            alpha1,
            alpha1,
        ]
    ):
        raise ValueError(
            'All inputs must have the same length as the spectra.'
        )

    (
        partitions,
        peak_of_partitions,
        final_partitions,
        final_peak_of_partitions,
    ) = [np.zeros((len(frequencies), len(DIRECTIONS))) for _ in range(4)]

    directional_matrix, frequency_matrix = np.meshgrid(DIRECTIONS, frequencies)
    _, frequency_matrix_interval = np.meshgrid(DIRECTIONS, bandwidth)

    directional_wave_spectra = _calculate_directional_wave_spectra(
        frequencies, DIRECTIONS, r1, r2, spectral_wave_density, alpha1, alpha2
    )

    (
        partitions,
        peak_of_partitions,
    ) = _fill_partitions_with_directional_wave_spectra_max(
        frequencies,
        DIRECTIONS,
        directional_wave_spectra,
        partitions,
        peak_of_partitions,
    )

    partitions = _fill_partitions_with_neighbors(
        frequencies, DIRECTIONS, directional_wave_spectra, partitions
    )

    partitions, peak_of_partitions = _remove_noisy_partitions(
        partitions,
        directional_wave_spectra,
        frequency_matrix_interval,
        (NOISE_A, NOISE_B),
        frequency_matrix,
        peak_of_partitions,
    )

    partitions, peak_of_partitions = _reorder_partitions_peak_energy(
        partitions,
        peak_of_partitions,
        directional_wave_spectra,
        frequency_matrix_interval,
        False,
    )

    # Check if adjacent swells are in fact the same system and merge them
    # For that to happen they have to meet one of two criteria, the peak
    # separation or minimum between peaks.

    middle, end = int(len(DIRECTIONS) / 2), int(len(DIRECTIONS))

    xx_vector = np.linspace(0, len(DIRECTIONS) - 1, len(DIRECTIONS))
    yy_vector = np.linspace(0, len(frequencies) - 1, len(frequencies))
    xx_matrix, yy_matrix = np.meshgrid(xx_vector, yy_vector)

    partition_flag_counter = 1

    for _ in range(len(_get_unique_partitions(partitions))):

        valid_partitions = _get_unique_partitions(partitions)

        if len(valid_partitions) > 0:
            inverted_peak_of_partitions = np.hstack(
                [
                    peak_of_partitions[:, middle:end],
                    peak_of_partitions[:, 0:middle],
                ]
            )
            alpha_max_eval_std = np.nonzero(
                peak_of_partitions == valid_partitions[0]
            )

            alpha_max_eval_inverse = np.nonzero(
                inverted_peak_of_partitions == valid_partitions[0]
            )

            (
                frequency_delta,
                frequency_spread,
            ) = _calculate_frequency_delta_and_spread(
                frequency_matrix,
                directional_matrix,
                directional_wave_spectra,
                frequency_matrix_interval,
                peak_of_partitions,
                partitions,
            )

            merge = True

            for partition_index in range(
                1, len(valid_partitions)
            ):  # skip first partition

                if merge:

                    alpha_max_floating_std = np.nonzero(
                        peak_of_partitions == valid_partitions[partition_index]
                    )
                    alpha_max_floating_inverse = np.nonzero(
                        inverted_peak_of_partitions
                        == valid_partitions[partition_index]
                    )

                    distance_std = _get_euclidean_distance(
                        xx_matrix,
                        yy_matrix,
                        alpha_max_eval_std,
                        alpha_max_floating_std,
                    )

                    distance_inverse = _get_euclidean_distance(
                        xx_matrix,
                        yy_matrix,
                        alpha_max_eval_inverse,
                        alpha_max_floating_inverse,
                    )

                    if distance_inverse < distance_std:
                        (
                            xx_vector_between_peaks,
                            yy_vector_between_peaks,
                            period_space_between_peaks,
                            directional_space_between_peaks,
                        ) = _get_vectors_between_peaks(
                            alpha_max_eval_inverse,
                            alpha_max_floating_inverse,
                            xx_matrix,
                            yy_matrix,
                            frequency_matrix,
                            directional_matrix,
                        )
                    else:
                        (
                            xx_vector_between_peaks,
                            yy_vector_between_peaks,
                            period_space_between_peaks,
                            directional_space_between_peaks,
                        ) = _get_vectors_between_peaks(
                            alpha_max_eval_std,
                            alpha_max_floating_std,
                            xx_matrix,
                            yy_matrix,
                            frequency_matrix,
                            directional_matrix,
                        )

                    f = interpolate.interp2d(
                        xx_vector,
                        yy_vector,
                        directional_wave_spectra,
                        kind='linear',
                    )
                    connection_vector = f(
                        xx_vector_between_peaks.flatten(),
                        yy_vector_between_peaks.flatten(),
                    )

                    if (
                        directional_space_between_peaks < DIRECTION_SPACE_LIMIT
                        and period_space_between_peaks < PERIOD_SPACE_LIMIT
                    ):

                        if (
                            frequency_delta[partition_index]
                            <= frequency_spread[partition_index]
                        ):
                            merge = False
                            alpha = np.nonzero(
                                partitions == valid_partitions[0]
                            )
                            partitions[alpha] = valid_partitions[
                                partition_index
                            ]
                            alpha = np.nonzero(
                                peak_of_partitions == valid_partitions[0]
                            )
                            peak_of_partitions[alpha] = 0

                        if (
                            connection_vector.min()
                            > directional_wave_spectra[alpha_max_eval_std]
                            * MINIMUM_LOW_PEAK_FACTOR
                        ):
                            merge = False
                            alpha = np.nonzero(
                                partitions == valid_partitions[0]
                            )
                            partitions[alpha] = valid_partitions[
                                partition_index
                            ]
                            alpha = np.nonzero(
                                peak_of_partitions == valid_partitions[0]
                            )
                            peak_of_partitions[alpha] = 0

            # Transfer partition to final mask if it has not been merged

            if merge:
                alpha = np.nonzero(partitions == valid_partitions[0])
                final_partitions[alpha] = partition_flag_counter
                partitions[alpha] = 0
                alpha = np.nonzero(peak_of_partitions == valid_partitions[0])
                final_peak_of_partitions[alpha] = partition_flag_counter
                peak_of_partitions[alpha] = 0
                partition_flag_counter += 1

            partitions, peak_of_partitions = _reorder_partitions(
                partitions,
                peak_of_partitions,
                directional_wave_spectra,
                frequency_matrix_interval,
                False,
            )

    final_partitions, final_peak_of_partitions = _reorder_partitions(
        final_partitions,
        final_peak_of_partitions,
        directional_wave_spectra,
        frequency_matrix_interval,
        True,
    )

    return _calculate_swell_partitions(
        final_partitions,
        directional_wave_spectra,
        frequency_matrix_interval,
        final_peak_of_partitions,
        directional_matrix,
        frequency_matrix,
    )
