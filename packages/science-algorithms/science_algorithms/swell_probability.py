import logging
from typing import List, Tuple

from science_algorithms.types import SwellPartition

logger = logging.getLogger('swell-probability')


def _percentage_of_values_within_variance(
    values: List[float], control: float, variance: float = 0.0
) -> float:
    """
    Calculates the percentage of values from a list
    that fit within a percentage from the control value.

    Args:
        values: The list of values to check.
        control: The value to check the other members against.
        variance: Variance to check.

    Returns:
        The percentage of values that fit within
        the control as a whole number.
    """
    count_in_range = len(
        [member for member in values if abs(control - member) <= variance]
    )
    return 100.0 * (count_in_range / len(values)) if len(values) > 0.0 else 0.0


def _get_swell_percentage_range(height: float) -> float:
    """
    Takes a swell height and returns a swell
    percentage range based on the swell height.

    Args:
        height: swell height to calculate the
                swell percentage range from.

    Returns:
        Swell percentage range as a whole number.
    """
    if height < 1.0:
        return 40.0

    if height < 2.0:
        return 30.0

    return 20.0


def _find_best_swell_height_period(
    members: List[SwellPartition]
) -> Tuple[float, float]:
    """
    Finds average height and period for the ensemble members
    that are most representative, as determined by period and
    direction.

    Each member is compared to the others in the list, and a
    match between 2 members occurs when the difference
    between the period and direction fall within a range.
    The average of heights and periods for the largest class
    found are returned

    Args:
        members: List of SwellPartitions representing
        WW3-Ensemble members.

    Returns:
        Pair of numbers, expected height and period.
    """
    best_match_count = 0
    expected_height, expected_period = (0.0, 0.0)

    for row_index, swell_partition in enumerate(members):
        group = []
        for comparison_index, comparison_swell_partition in enumerate(members):
            if (
                abs(swell_partition.period - comparison_swell_partition.period)
                < 1.5
                and swell_partition.angle_difference(
                    comparison_swell_partition.direction
                )
                < 35
            ):
                group.append(comparison_index)

        if len(group) > best_match_count:
            logger.debug(
                f'Match count: {len(group)} '
                f'for index: {row_index} is greater '
                f'than: {best_match_count}'
            )
            best_match_count = len(group)
            expected_height = sum([members[i].height for i in group]) / len(
                group
            )
            expected_period = sum([members[i].period for i in group]) / len(
                group
            )

    return (expected_height, expected_period)


def get_swell_probability(members: List[SwellPartition]) -> float:
    """
    Calculates the probability of a swell occurring
    from the given WW3-Ensemble members. Each member is
    compared to the others in the list, and a match
    between 2 members occurs when the difference between
    the period and direction fall within a range. The
    probability is then taken from the members with the
    most matches.

    Args:
        members: List of SwellPartitions representing
        WW3-Ensemble members.

    Returns:
        Swell probability as a whole number.
    """
    if len(members) < 2:
        return 100
    expected_height, expected_period = _find_best_swell_height_period(members)

    swell_heights = [swell.height for swell in members]
    swell_periods = [swell.period for swell in members]

    swell_percentage_range = _get_swell_percentage_range(expected_height)

    logger.debug(
        f'Calculating swell probability with '
        f'percentage range: {swell_percentage_range}'
    )

    swell_height_probability = _percentage_of_values_within_variance(
        swell_heights,
        expected_height,
        max(0.0, swell_percentage_range * (expected_height / 100.0)),
    )

    swell_period_probability = _percentage_of_values_within_variance(
        swell_periods,
        expected_period,
        max(2.0, 0.0 * (expected_period / 100.0)),
    )

    swell_probability = (
        swell_height_probability * swell_period_probability
    ) / 100.0

    logger.debug(
        f'Swell probability from '
        f'percentage_of_values_within_variance is {swell_probability}'
    )

    return max(swell_probability, 5.0)
