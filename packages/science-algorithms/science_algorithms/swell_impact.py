from typing import Iterable, List, Optional

from science_algorithms.polynomial_model import partition_wave_height_poly
from science_algorithms.types import SwellPartition


def calculate_impacts(
    swell_partitions: Iterable[SwellPartition],
    optimal_swell_direction: Optional[float] = None,
) -> List[float]:
    """
    Calculate the relative impacts of a group of swell partitions.

    The returned values are normalized for the group; the values are not
    comparable between results for calls to this function for several
    different groups of swell partitions.

    Args:
        swell_partitions: An iterable of instances of SwellPartition.
        optimal_swell_direction: Angle, in degrees, that is optimal for a
                                 swell to be coming from. E.g. 270 means
                                 swell coming from the west. Defaults to
                                 None.

    Returns:
        list of swell impacts, corresponding to the the swell_partitions
    """
    impacts = [
        partition_wave_height_poly(
            partition,
            # No optimal direction, treat each partition as
            # if it's heading straight to shore.
            optimal_swell_direction
            if optimal_swell_direction
            else partition.direction,
        )
        for partition in swell_partitions
    ]
    total = sum(impacts)

    # Normalize
    if total > 0:
        impacts = [round(val / total, 4) for val in impacts]

    return impacts
