from math import isclose

from science_algorithms.swell_probability import (
    _find_best_swell_height_period,
    _get_swell_percentage_range,
    _percentage_of_values_within_variance,
    get_swell_probability,
)
from science_algorithms.types import SwellPartition


def test_percentage_of_values_within_variance_similar_values():
    swell_heights = [4.76, 4.79, 4.72, 4.79, 4.82, 4.82, 4.72]
    best_swell_height = 4.76
    # Find values within 20% of 4.76
    variance_percent = 20.0
    variance_absolute = 0.0
    assert (
        _percentage_of_values_within_variance(
            swell_heights,
            best_swell_height,
            max(
                variance_absolute, variance_percent * (best_swell_height / 100)
            ),
        )
        == 100
    )


def test_percentage_of_values_within_variance_varied_values():
    swell_heights = [
        7.0,
        12.0,
        11.5,
        6.5,
        10.0,
        11.5,
        10.5,
        10.0,
        7.0,
        8.0,
        6.0,
        11.0,
        13.5,
        8.5,
        8.5,
        6.0,
        9.5,
        10.5,
        7.5,
        14.0,
    ]
    best_swell_height = 12.0
    # Find values within 20% of 12
    variance_percent = 20.0
    variance_absolute = 0.0
    assert (
        _percentage_of_values_within_variance(
            swell_heights,
            best_swell_height,
            max(
                variance_absolute, variance_percent * (best_swell_height / 100)
            ),
        )
        == 50
    )
    # Find values within 30% of 12
    variance_percent = 30
    variance_absolute = 0.0
    assert (
        _percentage_of_values_within_variance(
            swell_heights,
            best_swell_height,
            max(
                variance_absolute, variance_percent * (best_swell_height / 100)
            ),
        )
        == 65
    )
    # Find values between +2 or -2 from 12
    variance_percent = 0.0
    variance_absolute = 2.0
    assert (
        _percentage_of_values_within_variance(
            swell_heights,
            best_swell_height,
            max(
                variance_absolute, variance_percent * (best_swell_height / 100)
            ),
        )
        == 50
    )
    # Find values between +4 or -4 from 12
    variance_percent = 0.0
    variance_absolute = 4.0
    assert (
        _percentage_of_values_within_variance(
            swell_heights,
            best_swell_height,
            max(
                variance_absolute, variance_percent * (best_swell_height / 100)
            ),
        )
        == 70
    )


def test_percentage_of_values_within_variance_no_match():
    swell_periods = [12.0, 11.0, 10.0, 9.0]
    best_swell_period = 7.0
    variance_percent = 1.0
    variance_absolute = 0.0
    # Try to find values between +1 or -1 from 7,
    # but no match found
    assert (
        _percentage_of_values_within_variance(
            swell_periods,
            best_swell_period,
            max(
                variance_absolute, variance_percent * (best_swell_period / 100)
            ),
        )
        == 0
    )
    swell_periods = list()
    # Expect 0 when empty list is passed in
    assert (
        _percentage_of_values_within_variance(swell_periods, best_swell_period)
    ) == 0


def test_get_swell_percentage_range():
    height = 9.32
    # Expect swell percentage range of 20
    # when height > 2
    assert _get_swell_percentage_range(height) == 20
    height = 1.5
    # Expect swell percentage range of 30
    # when height between 1-2
    assert _get_swell_percentage_range(height) == 30
    height = 0.5
    # Expect swell percentage range of 40
    # when height < 1
    assert _get_swell_percentage_range(height) == 40


def test_find_best_swell_height_period():
    # All partitions similar period/direction. Expected height and
    # period are average of all.
    members = [
        SwellPartition(9.32, 13.06, 261.1),
        SwellPartition(6.49, 13.25, 261.64),
        SwellPartition(7.68, 13.01, 260.15),
        SwellPartition(7.84, 13.13, 260.52),
        SwellPartition(9.22, 13.14, 262.27),
    ]
    expected_height, expected_period = _find_best_swell_height_period(members)
    assert isclose(
        expected_height, (9.32 + 6.49 + 7.68 + 7.84 + 9.22) / 5, abs_tol=0.0001
    )
    assert isclose(
        expected_period,
        (13.06 + 13.25 + 13.01 + 13.13 + 13.14) / 5,
        abs_tol=0.0001,
    )

    # Last three members for the largest class. Expected height and
    # period should be average of those three.
    members = [
        SwellPartition(9.32, 3.57, 261.1),
        SwellPartition(6.49, 6.78, 261.64),
        SwellPartition(7.68, 13.01, 260.15),
        SwellPartition(7.84, 13.13, 260.52),
        SwellPartition(9.22, 13.14, 262.27),
    ]
    expected_height, expected_period = _find_best_swell_height_period(members)
    assert isclose(expected_height, (7.68 + 7.84 + 9.22) / 3, abs_tol=0.0001)
    assert isclose(
        expected_period, (13.01 + 13.13 + 13.14) / 3, abs_tol=0.0001
    )


def test_get_swell_probability_one_member():
    # Expect 100% probability when there is only
    # 1 member
    members = [SwellPartition(9.32, 13.06, 261.1)]
    assert get_swell_probability(members) == 100


def test_get_swell_probability():
    # Test a variety of different
    # members inputs, which all have
    # a size of 20
    members = [
        SwellPartition(9.32, 13.06, 261.1),
        SwellPartition(6.49, 13.25, 261.64),
        SwellPartition(7.68, 13.01, 260.15),
        SwellPartition(7.84, 13.13, 260.52),
        SwellPartition(9.22, 13.14, 262.27),
        SwellPartition(9.28, 13.18, 262.24),
        SwellPartition(8.4, 13.06, 261.17),
        SwellPartition(6.86, 13.24, 262.36),
        SwellPartition(7.48, 12.95, 260.9),
        SwellPartition(7.58, 12.97, 259.48),
        SwellPartition(8.5, 13.12, 260.56),
        SwellPartition(8.43, 13.09, 260.56),
        SwellPartition(7.02, 13.02, 260.47),
        SwellPartition(8.3, 13.17, 261.61),
        SwellPartition(9.64, 13.27, 262),
        SwellPartition(9.12, 13.14, 260.3),
        SwellPartition(9.64, 13.1, 260.18),
        SwellPartition(6.43, 12.85, 259.19),
        SwellPartition(8.89, 13.17, 260.58),
        SwellPartition(8.04, 13.21, 261.63),
    ]
    assert get_swell_probability(members) == 90.0

    members = [
        SwellPartition(11.25, 16.95, 232.06),
        SwellPartition(13.68, 15.12, 249.44),
        SwellPartition(10.86, 16.94, 232.05),
        SwellPartition(11.48, 16.14, 238.82),
        SwellPartition(10.66, 16.94, 232.11),
        SwellPartition(16.3, 17, 232.06),
        SwellPartition(12.43, 16.9, 232.29),
        SwellPartition(10.82, 16.96, 231.77),
        SwellPartition(11.84, 17.13, 231.61),
        SwellPartition(11.09, 16.42, 239.72),
        SwellPartition(11.91, 16.34, 237.49),
        SwellPartition(17.88, 10.64, 261.84),
        SwellPartition(18.2, 10.14, 263.16),
        SwellPartition(13.22, 17.08, 232.04),
        SwellPartition(11.78, 16.13, 238.53),
        SwellPartition(11.12, 16.45, 238.39),
        SwellPartition(11.78, 16.92, 231.83),
        SwellPartition(19.55, 11.65, 260.72),
        SwellPartition(11.38, 16.5, 237.66),
        SwellPartition(13.09, 15.8, 238.86),
    ]
    assert get_swell_probability(members) == 68.0

    members = [
        SwellPartition(4.76, 9.74, 257.28),
        SwellPartition(4.79, 9.73, 255.42),
        SwellPartition(4.72, 9.71, 255.53),
        SwellPartition(4.79, 9.78, 255.16),
        SwellPartition(4.82, 9.8, 256.72),
        SwellPartition(4.82, 9.86, 255.31),
        SwellPartition(4.72, 9.86, 255.72),
        SwellPartition(4.89, 9.8, 255.47),
        SwellPartition(4.76, 9.71, 256.81),
        SwellPartition(4.79, 9.78, 256.41),
        SwellPartition(4.82, 9.78, 256.62),
        SwellPartition(4.79, 9.83, 255.07),
        SwellPartition(4.69, 9.76, 255.15),
        SwellPartition(4.66, 9.72, 256.7),
        SwellPartition(4.79, 9.75, 255.9),
        SwellPartition(4.85, 9.79, 257.3),
        SwellPartition(4.79, 9.78, 256.27),
        SwellPartition(4.76, 9.83, 255.27),
        SwellPartition(4.89, 9.84, 257.49),
        SwellPartition(4.89, 9.87, 254.81),
    ]
    assert get_swell_probability(members) == 100.0

    members = [
        SwellPartition(7, 13, 87.04),
        SwellPartition(12, 16, 77.68),
        SwellPartition(11.5, 17, 74.37),
        SwellPartition(6.5, 14, 89.97),
        SwellPartition(10, 13, 82.09),
        SwellPartition(11.5, 16, 77.47),
        SwellPartition(10.5, 13, 87.02),
        SwellPartition(10, 16, 81.7),
        SwellPartition(7, 12, 87.98),
        SwellPartition(8, 15, 88.85),
        SwellPartition(6, 12, 84.36),
        SwellPartition(11, 13, 84.47),
        SwellPartition(13.5, 15, 83.37),
        SwellPartition(8.5, 15, 87.08),
        SwellPartition(8.5, 16, 78.19),
        SwellPartition(6, 12, 82.33),
        SwellPartition(9.5, 15, 83.29),
        SwellPartition(10.5, 8, 89.39),
        SwellPartition(7.5, 15, 82.93),
        SwellPartition(14, 10, 87.78),
    ]

    assert isclose(get_swell_probability(members), 30.25, abs_tol=0.01)

    members = [
        SwellPartition(5, 9, 40),
        SwellPartition(5, 9, 40),
        SwellPartition(5, 9, 40),
        SwellPartition(5, 9, 40),
        SwellPartition(5, 9, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
        SwellPartition(10, 18, 40),
    ]
    assert isclose(get_swell_probability(members), 56.25, abs_tol=0.01)


def test_get_swell_probability_independent_of_order():
    # Test with 30 members. New GEFS data has 30 members.
    members = [
        SwellPartition(2.04, 12.1, 261.86),
        SwellPartition(1.37, 11.77, 261.11),
        SwellPartition(1.5, 11.99, 261.95),
        SwellPartition(1.39, 11.93, 261.63),
        SwellPartition(1.39, 12.0, 261.6),
        SwellPartition(1.29, 12.0, 262.03),
        SwellPartition(1.41, 11.9, 261.06),
        SwellPartition(1.36, 11.84, 261.25),
        SwellPartition(1.66, 11.95, 261.26),
        SwellPartition(1.38, 11.91, 261.59),
        SwellPartition(1.35, 11.89, 261.56),
        SwellPartition(1.34, 11.78, 261.07),
        SwellPartition(1.48, 12.0, 261.37),
        SwellPartition(1.44, 11.84, 261.23),
        SwellPartition(1.58, 12.28, 262.23),
        SwellPartition(1.55, 12.03, 261.78),
        SwellPartition(1.36, 11.91, 261.49),
        SwellPartition(1.35, 11.85, 261.41),
        SwellPartition(1.36, 11.87, 261.73),
        SwellPartition(1.35, 11.84, 261.38),
        SwellPartition(2.64, 7.41, 250.18),
        SwellPartition(1.34, 11.79, 261.49),
        SwellPartition(1.79, 12.08, 261.36),
        SwellPartition(1.44, 11.87, 261.63),
        SwellPartition(1.36, 11.75, 261.45),
        SwellPartition(1.39, 11.82, 261.71),
        SwellPartition(1.40, 12.01, 261.64),
        SwellPartition(1.59, 11.99, 261.41),
        SwellPartition(1.52, 11.81, 261.13),
        SwellPartition(1.34, 11.91, 261.57),
    ]
    assert get_swell_probability(members[::-1]) == get_swell_probability(
        members
    )
    assert isclose(get_swell_probability(members), 90.22, abs_tol=0.01)
