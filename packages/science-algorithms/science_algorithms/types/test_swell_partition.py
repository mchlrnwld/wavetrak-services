from science_algorithms.types import SwellPartition


def test_swell_partition_angle_diff():
    swell = SwellPartition(1.0, 1.0, direction=0)
    assert 10 == swell.angle_difference(10)
    assert -10 == swell.angle_difference(350)

    swell = SwellPartition(1.0, 1.0, direction=25)
    assert 10 == swell.angle_difference(35)
    assert -10 == swell.angle_difference(15)
    assert -35 == swell.angle_difference(350)


def test_swell_partition_truth_value():
    # Expect SwellPartition to be true and
    # expect the attribute values to be the same
    # as the values that were passed in
    swell = SwellPartition(0.1, 0.56, 2.0)
    assert swell
    assert (
        swell.height == 0.1 and swell.period == 0.56 and swell.direction == 2.0
    )

    # Expect SwellPartition to not be true and
    # expect the attributes values return to be 0
    swell = SwellPartition(None, None, None)
    assert not swell
    assert swell.height == 0 and swell.period == 0 and swell.direction == 0
