"Types used in science_algorithms module"

from science_algorithms.types.swell_partition import SwellPartition

__all__ = ['SwellPartition']
