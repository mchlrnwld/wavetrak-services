from science_algorithms.swell_impact import calculate_impacts
from science_algorithms.types import SwellPartition


def test_calculate_impacts():
    partitions = [
        SwellPartition(0.11, 10.75, 232.39),
        SwellPartition(0.27, 4.51, 149.29),
        SwellPartition(0.28, 7.34, 130.87),
        SwellPartition(0.32, 2.44, 72.33),
        SwellPartition(0.0, 8.0, 0.0),
    ]
    impacts = calculate_impacts(partitions, 230)
    assert impacts == [0.5563, 0.3184, 0.1253, 0, 0]
