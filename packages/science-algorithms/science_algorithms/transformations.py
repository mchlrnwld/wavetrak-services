import re

import numpy as np  # type: ignore

from science_algorithms.enums import (
    FogType,
    PrecipitationType,
    SkyConditions,
    ThunderType,
    WeatherConditions,
)


def kelvin_to_celsius(temperature):
    """
    Converts temperature from Kelvin to Celsius.

    Args:
        temperature: numpy array of temperatures (K).

    Returns:
        numpy array of temperatures (C).
    """
    return temperature - 273.15


def pascal_to_millibar(pressure):
    """
    Converts pressure from Pascal to millibar.

    Args:
        pressure: numpy array of pressures (Pa).

    Returns:
        numpy array of pressures (mb).
    """
    return pressure * 0.01


def get_hourly_precipitation(
    current_precipitation,
    current_details,
    previous_precipitation,
    previous_details,
):
    """
    Converts accumulated total precipitation to hourly total precipitation

    Args:
        current_precipitation: numpy array of accumulated precipitation
        current_details: numpy array of details for each weather key
        previous_precipitation: numpy array of accumulated precipitation
        previous_details: numpy array of details for each weather key

    Returns:
        numpy array of hourly precipitation
    """
    current_interval = get_forecast_interval(current_details)
    previous_interval = (
        get_forecast_interval(previous_details)
        if previous_details is not None
        else None
    )

    # if the two intervals are mutually exclusive OR identical
    # then return the hourly precipitation
    # else if the previous interval is a subset of the current interval
    # then return the marginal hourly precipitation
    if (
        previous_interval is None
        or current_interval[0] == previous_interval[1]
        or current_interval == previous_interval
    ):
        start, end = current_interval
        return current_precipitation / (end - start)
    elif current_interval[0] == previous_interval[0]:
        return get_marginal_hourly_precipitation(
            current_precipitation,
            current_interval,
            previous_precipitation,
            previous_interval,
        )
    else:
        raise Exception(
            'Hourly marginal precipitation not calculable '
            'based on forecast intervals'
        )


def get_forecast_interval(details):
    """
    Get start and end hours of a forecast time

    Args:
        details: a string that includes forecast time information in the
        7th section when delimited by :s

    Returns:
        a tuple (start, end)
    """
    forecast_time = details.split(':')[6]
    start, end = re.search(  # type: ignore
        r'(\d+)\-(\d+)', forecast_time
    ).group(1, 2)
    return int(start), int(end)


def get_marginal_hourly_precipitation(
    current_precipitation,
    current_interval,
    previous_precipitation,
    previous_interval,
):
    """
    Get marginal hourly precipitation

    Args:
        current_precipitation: numpy array of accumulated precipitation
        current_details: numpy array of details for each weather key
        previous_precipitation: numpy array of accumulated precipitation
        previous_details: numpy array of details for each weather key

    Returns:
        numpy array of hourly precipitation
    """
    marginal_precipitation = previous_precipitation - current_precipitation
    marginal_interval = previous_interval[1] - current_interval[1]
    return marginal_precipitation / marginal_interval


def get_precipitation_type(
    precipitation_rate, is_snow, is_freezing_rain, is_ice_pellets
):
    conditions = [
        is_snow,
        is_freezing_rain,
        is_ice_pellets,
        precipitation_rate > 0,
    ]
    choices = [
        PrecipitationType.SNOW,
        PrecipitationType.FREEZING_RAIN,
        PrecipitationType.ICE_PELLETS,
        PrecipitationType.RAIN,
    ]
    return np.select(conditions, choices, PrecipitationType.NONE)


def get_wind_magnitude(wind_u, wind_v):
    """
    Calculate wind magnitude from U/V components.

    Args:
        wind_u: numpy array of wind U component (m/s).
        wind_v: numpy array of wind V component (m/s).

    Returns:
        numpy array of wind magnitudes (m/s).
    """
    return np.sqrt((wind_u ** 2) + (wind_v ** 2))


# TODO: Replace this with get_wind_magnitude. Should this be
# sqrt(swu^2 + swv^2) instead?
def get_storm_wind_magnitude(storm_wind_u, storm_wind_v):
    """
    Calculate storm wind magnitude from U/V components.

    Args:
        storm_wind_u: numpy array of storm wind U component (m/s) at 6000-0 m
                      above ground.
        storm_wind_v: numpy array of storm wind V component (m/s) at 6000-0 m
                      above ground.

    Returns:
        numpy array of storm wind magnitudes (m/s).
    """
    return np.sqrt((storm_wind_u ** 2) * (storm_wind_v ** 2))


# TODO: Should we replace this with dewpoint from GFS?
def get_dewpoint_for_weather_conditions(temperature_c, relative_humidity):
    """
    Get dewpoint for weather conditions. This function is used specifically
    for weather conditions algorithm so that we can match the output of the
    existing algorithm. We should see if replacing this with GFS dewpoint
    variable can simplify and possibly improve weather conditions output.


    Args:
        temperature_c: numpy array of temperatures (C).
        relative_humidity: numpy array of relative humidities (%).

    Returns:
        numpy array of dewpoint temperatures (C).
    """
    dp_const_a = 17.67
    dp_const_b = 243.5
    rh_log = np.log(relative_humidity / 100)
    gamma_trh = (
        (dp_const_a * temperature_c) / (dp_const_b + temperature_c)
    ) + rh_log
    return (dp_const_b * gamma_trh) / (dp_const_a - gamma_trh)


def get_thunder_type(
    lifted_index,
    cape_surface,
    cape_ml,
    vertical_velocity_750mb,
    vertical_velocity_800mb,
    dewpoint_c,
    convective_inhibition,
    storm_wind_magnitude,
):
    """
    Get thunder type.

    Args:
        lifted_index: numpy array of lifted index (K) at surface.
        cape_surface: numpy array of CAPE (J/kg) at surface.
        cape_ml: numpy array of CAPE (J/kg) at 255-0 mb above ground.
        vertical_velocity_750mb: numpy array of vertical velocity (Pa/s) at
                                 750 mb.
        vertical_velocity_800mb: numpy array of vertical velocity (Pa/s) at
                                 800 mb.
        dewpoint_c: numpy array of dewpoint (C) at 2 m above ground.
        convective_inhibition: numpy array of convective inhibition (J/kg) at
                               surface.
        storm_wind_magnitude: numpy array of storm wind magnitudes (m/s) at
                              6000-0 m above ground.

    Returns:
        numpy array of ThunderTypes.
    """
    cape = np.maximum(cape_ml, cape_surface)
    vertical_velocity = np.minimum(
        vertical_velocity_750mb, vertical_velocity_800mb
    )

    weak_wind = np.where(
        cape < 2500, ThunderType.SHOWERS, ThunderType.HEAVY_SHOWERS
    )
    moderate_wind = np.where(
        cape < 2000, ThunderType.STORMS, ThunderType.HEAVY_STORMS
    )
    strong_wind = np.select(
        [cape < 500, cape < 1500],
        [ThunderType.SHOWERS, ThunderType.HEAVY_STORMS],
        ThunderType.SEVERE_STORMS,
    )

    thunder = np.select(
        [storm_wind_magnitude < 21, storm_wind_magnitude < 50],
        [weak_wind, moderate_wind],
        strong_wind,
    )

    # TODO: Can we remove the "or equal to"s?
    return np.where(
        (
            (lifted_index > -2)
            | (cape <= 200)
            | (vertical_velocity >= -0.25)
            | (dewpoint_c <= 11)
            | (convective_inhibition <= -200)
        ),
        ThunderType.NONE,
        thunder,
    )


def get_net_radiation(
    down_short_rad_flux,
    down_long_rad_flux,
    up_short_rad_flux,
    up_long_rad_flux,
):
    """
    Gets net radiation flux.

    Args:
        down_short_rad_flux: numpy array of downward short-wave radiation flux
                            (W/m^2) at surface.
        down_long_rad_flux: numpy array of downward long-wave radiation flux
                            (W/m^2) at surface.
        up_short_rad_flux: numpy array of upward short-wave radiation flux
                            (W/m^2) at surface.
        up_long_rad_flux: numpy array of upward long-wave radiation flux
                            (W/m^2) at surface.

    Returns:
        numpy array of downward radiation flux less upward radiation flux.
    """
    down = down_long_rad_flux + down_short_rad_flux
    up = up_long_rad_flux + up_short_rad_flux
    return down - up


def get_average_low_layer_relative_humidity(
    relative_humidity_925mb,
    relative_humidity_950mb,
    relative_humidity_975mb,
    relative_humidity_1000mb,
):
    """
    Gets average of relative humidities at low layers.

    Args:
        relative_humidity_925mb: numpy array of relative humidity (%) at 925
                                 mb.
        relative_humidity_950mb: numpy array of relative humidity (%) at 950
                                 mb.
        relative_humidity_975mb: numpy array of relative humidity (%) at 975
                                 mb.
        relative_humidity_1000mb: numpy array of relative humidity (%) at 1000
                                 mb.

    Returns:
        numpy array of average of relative humidities at 925 mb, 950 mb,
        975 mb, 1000 mb.
    """
    return (
        relative_humidity_925mb
        + relative_humidity_950mb
        + relative_humidity_975mb
        + relative_humidity_1000mb
    ) / 4


def get_fog_type(
    net_radiation,
    average_low_layer_relative_humidity,
    relative_humidity_surface,
    wind_magnitude,
):
    """
    Get fog type.

    Args:
        net_radiation: numpy array of net radiation (W/m^2) at surface.
        average_low_layer_relative_humidity: numpy array of average of
                                             relative humidity (%) at
                                             925 mb, 950 mb, 975 mb, and
                                             1000 mb.
        relative_humidity_surface: numpy array of relative humidity (%) at
                                   2 m above ground.
        wind_magnitude: numpy array of wind magnitude (m/s) at 10 m above
                        ground.

    Returns:
        numpy array of FogTypes. None if no fog.
    """
    dry_threshold = 95.2
    wet_threshold = 97.5

    surface_is_dry = relative_humidity_surface < dry_threshold
    surface_is_wet = relative_humidity_surface >= wet_threshold

    # Can this be combined into a single boolean?
    # What happens when net_radiation == 0?
    positive_net_radiation = net_radiation > 0
    negative_net_radiation = net_radiation < 0

    strong_wind_fog_type = np.where(
        surface_is_wet & positive_net_radiation, FogType.MIST, FogType.NONE
    )

    dry_lower_layer = np.where(
        (wind_magnitude >= 1) & surface_is_wet, FogType.MIST, FogType.NONE
    )

    middle_wet_lower_layer = np.where(
        positive_net_radiation,
        np.where(surface_is_wet, FogType.MIST, FogType.NONE),
        np.select(
            [surface_is_dry, surface_is_wet],
            [FogType.NONE, FogType.FOG],
            FogType.MIST,
        ),
    )

    wet_lower_layer = np.where(
        negative_net_radiation, FogType.FOG, FogType.MIST
    )

    conditions = [
        wind_magnitude >= 3.8,
        average_low_layer_relative_humidity < dry_threshold,
        average_low_layer_relative_humidity < wet_threshold,
    ]
    choices = [strong_wind_fog_type, dry_lower_layer, middle_wet_lower_layer]
    return np.select(conditions, choices, wet_lower_layer)


# Implementation of "Parameterizations for Cloud Overlapping and Shortwave
# Single-Scattering Properties for Use in General Circulation and Cloud
# Ensemble Models"
# https://www.researchgate.net/publication/249610109_Parameterizations_for
# _Cloud_Overlapping_and_Shortwave_Single-Scattering_Properties_for_Use_in
# _General_Circulation_and_Cloud_Ensemble_Models
def get_optical_depth(temperature, is_land, cloud_mixing_ratio, thickness):
    """
    Args:
        temperature: numpy array of temperature (K).
        is_land: numpy array of 1 for land or 0 for sea.
        cloud_mixing_ratio: numpy array of cloud mixing ratio.
        thickness: numpy array of layer thickness (m).

    Returns:
        numpy array of optical depths (m?).
    """
    a_1 = -6.59 * 10 ** (-3)
    a_2 = 1.65
    a_3 = 3.33 * 10 ** (-4)
    a_4 = 2.52

    iwc = 1.371 * 10 ** ((0.04962 * temperature) - 14)
    iwp = cloud_mixing_ratio * 917 * thickness
    lwp = cloud_mixing_ratio * 1000 * thickness

    rei = np.select(
        [temperature < 223.15, temperature < 233.15, temperature < 243.15],
        [
            (1250 / 9.917) * (iwc ** 0.109),
            (1250 / 9.337) * (iwc ** 0.08),
            (1250 / 9.208) * (iwc ** 0.055),
        ],
        (1250 / 9.387) * (iwc ** 0.031),
    )

    rew = np.where(is_land == 1, (-0.25 * temperature) + 73.29, 10)

    ice_cloud = iwp * (a_3 + (a_4 / rei))
    water_cloud = lwp * (a_1 + (a_2 / rew))

    return np.where(temperature < 258.15, ice_cloud, water_cloud)


total_optical_density_layers = [
    '100 mb',
    '150 mb',
    '200 mb',
    '250 mb',
    '300 mb',
    '350 mb',
    '400 mb',
    '450 mb',
    '500 mb',
    '550 mb',
    '600 mb',
    '650 mb',
    '700 mb',
    '750 mb',
    '800 mb',
    '850 mb',
    '900 mb',
    '925 mb',
    '950 mb',
    '975 mb',
    '1000 mb',
]


def get_total_optical_density(
    is_land, layer_temperatures, layer_cloud_mixing_ratios, layer_heights
):
    """
    Gets total optical density.

    Args:
        is_land: numpy array of 1 for land or 0 for sea.
        layer_temperatures: numpy arrays of temperatures (K) at each layer.
        layer_cloud_mixing_ratios: numpy arrays of cloud mixing ratio at each
                                   layer.
        layer_heights: numpy arrays of geopotential heights (m) at each layer.

    Returns:
        numpy array of total optical densitys (m?).
    """
    old_height = 19000
    total_optical_density = 0

    for layer in total_optical_density_layers:
        total_optical_density += get_optical_depth(
            # TODO: Pass in Kelvin without rounding. This little trickery of
            # converting twice and rounding is to account for floating point
            # rounding errors when compared to the original algorithm.
            # layer_temperatures[layer],
            np.round(layer_temperatures[layer] - 273.15, 1) + 273.15,
            is_land,
            layer_cloud_mixing_ratios[layer],
            old_height - layer_heights[layer],
        )
        old_height = layer_heights[layer]

    return total_optical_density


def get_sky_conditions(total_optical_density, total_cloud_cover):
    """
    Get sky conditions.

    Args:
        total_optical_density: numpy array of total optical density (m?).
        total_cloud_cover: numpy array of total cloud cover (%) for entire
                           atmosphere.

    Returns:
        numpy array of SkyConditions.
    """
    clouds_conditions = [
        (total_optical_density <= 2) | (total_cloud_cover <= 10),
        total_cloud_cover <= 50,
        total_cloud_cover <= 90,
    ]
    clouds_choices = [
        SkyConditions.CLEAR,
        SkyConditions.PARTLY_CLOUDY,
        SkyConditions.MOSTLY_CLOUDY,
    ]

    return np.select(clouds_conditions, clouds_choices, SkyConditions.OVERCAST)


def get_clear_conditions(
    precipitation_below_clear,
    precipitation_below_drizzle,
    precipitation_below_rain,
    precipitation_below_light_snow,
    precipitation_below_snow,
    precipitation_type,
):
    snow = np.select(
        [precipitation_below_light_snow, precipitation_below_snow],
        [
            WeatherConditions.BRIEF_SNOW_FLURRIES,
            WeatherConditions.BRIEF_SNOW_SHOWERS,
        ],
        WeatherConditions.BRIEF_HEAVY_SNOW_SHOWERS,
    )
    return np.select(
        [
            precipitation_below_clear,
            precipitation_type == PrecipitationType.ICE_PELLETS,
            precipitation_type == PrecipitationType.FREEZING_RAIN,
            precipitation_type == PrecipitationType.SNOW,
            precipitation_below_drizzle,
            precipitation_below_rain,
        ],
        [
            WeatherConditions.CLEAR,
            WeatherConditions.BRIEF_ICE_PELLETS_SHOWERS,
            WeatherConditions.BRIEF_FREEZING_RAIN_SHOWERS,
            snow,
            WeatherConditions.BRIEF_SHOWERS_POSSIBLE,
            WeatherConditions.LIGHT_SHOWERS_POSSIBLE,
        ],
        WeatherConditions.HEAVY_SHOWERS_POSSIBLE,
    )


def get_partly_cloudy_conditions(
    precipitation_below_clear,
    precipitation_below_drizzle,
    precipitation_below_rain,
    precipitation_below_light_snow,
    precipitation_below_snow,
    precipitation_type,
):
    snow = np.select(
        [precipitation_below_light_snow, precipitation_below_snow],
        [WeatherConditions.LIGHT_SNOW_SHOWERS, WeatherConditions.SNOW_SHOWERS],
        WeatherConditions.HEAVY_SNOW_SHOWERS,
    )
    return np.select(
        [
            precipitation_below_clear,
            precipitation_type == PrecipitationType.ICE_PELLETS,
            precipitation_type == PrecipitationType.FREEZING_RAIN,
            precipitation_type == PrecipitationType.SNOW,
            precipitation_below_drizzle,
            precipitation_below_rain,
        ],
        [
            WeatherConditions.MOSTLY_CLEAR,
            WeatherConditions.ICE_PELLETS_SHOWERS,
            WeatherConditions.FREEZING_RAIN_SHOWERS,
            snow,
            WeatherConditions.BRIEF_SHOWERS,
            WeatherConditions.SHOWERS,
        ],
        WeatherConditions.HEAVY_SHOWERS,
    )


def get_mostly_cloudy_conditions(
    precipitation_below_clear,
    precipitation_below_light_rain,
    precipitation_below_rain,
    precipitation_below_light_snow,
    precipitation_below_snow,
    wind_below_blizzard,
    wind_below_severe_blizzard,
    precipitation_type,
):
    no_blizzard_snow = np.where(
        precipitation_below_snow,
        WeatherConditions.SNOW_SHOWERS,
        WeatherConditions.HEAVY_SNOW_SHOWERS,
    )
    snow = np.select(
        [
            precipitation_below_light_snow,
            wind_below_blizzard,
            wind_below_severe_blizzard,
        ],
        [
            WeatherConditions.LIGHT_SNOW_SHOWERS,
            no_blizzard_snow,
            WeatherConditions.SNOW_SQUALLS,
        ],
        WeatherConditions.SEVERE_SNOW_SQUALLS,
    )
    # TODO: Remove this. Pretty sure it's a bug and this can be consolidated
    # with above. In the original code everything including comments is
    # exactly the same except for the threshold for brief showers.
    showers = np.select(  # noqa: ignore=F841
        [precipitation_below_light_rain, precipitation_below_rain],
        [WeatherConditions.BRIEF_SHOWERS, WeatherConditions.SHOWERS],
        WeatherConditions.HEAVY_SHOWERS,
    )
    return np.select(
        [
            precipitation_below_clear,
            precipitation_type == PrecipitationType.ICE_PELLETS,
            precipitation_type == PrecipitationType.FREEZING_RAIN,
            precipitation_type == PrecipitationType.SNOW,
            precipitation_below_light_rain,
            precipitation_below_rain,
        ],
        [
            WeatherConditions.MOSTLY_CLOUDY,
            WeatherConditions.ICE_PELLETS_SHOWERS,
            WeatherConditions.FREEZING_RAIN_SHOWERS,
            snow,
            WeatherConditions.BRIEF_SHOWERS,
            WeatherConditions.SHOWERS,
        ],
        WeatherConditions.HEAVY_SHOWERS,
    )


def get_overcast_conditions(
    total_optical_density,
    convective_precipitation_rate_mm_hr,
    precipitation_below_clear,
    precipitation_below_drizzle,
    precipitation_below_light_rain,
    precipitation_below_rain,
    precipitation_below_heavy_rain,
    precipitation_below_light_snow,
    precipitation_below_snow,
    wind_below_blizzard,
    wind_below_severe_blizzard,
    is_fog,
    precipitation_type,
):
    no_precipitation = np.where(
        total_optical_density < 23,
        WeatherConditions.CLOUDY,
        WeatherConditions.OVERCAST,
    )
    no_blizzard_snow = np.where(
        precipitation_below_snow,
        WeatherConditions.SNOW,
        WeatherConditions.HEAVY_SNOW,
    )
    snow = np.select(
        [
            precipitation_below_light_snow,
            wind_below_blizzard,
            wind_below_severe_blizzard,
        ],
        [
            WeatherConditions.LIGHT_SNOW,
            no_blizzard_snow,
            WeatherConditions.BLIZZARD,
        ],
        WeatherConditions.SEVERE_BLIZZARD,
    )
    drizzle = np.where(
        # TODO: Fix this. Original had $totalPrecipitationRate, which is
        # probably supposed to be $precipitationRate. This gets coerced to 0.
        # precipitation_rate_mm_hr < (2 * convective_precipitation_rate_mm_hr),
        convective_precipitation_rate_mm_hr > 0,
        WeatherConditions.LIGHT_SHOWERS,
        WeatherConditions.DRIZZLE,
    )
    light_rain = np.where(
        is_fog,
        WeatherConditions.LIGHT_RAIN_AND_FOG,
        WeatherConditions.LIGHT_RAIN,
    )
    rain = np.where(
        is_fog, WeatherConditions.RAIN_AND_FOG, WeatherConditions.RAIN
    )
    return np.select(
        [
            precipitation_below_clear,
            precipitation_type == PrecipitationType.SNOW,
            precipitation_type == PrecipitationType.ICE_PELLETS,
            precipitation_type == PrecipitationType.FREEZING_RAIN,
            precipitation_below_drizzle,
            precipitation_below_light_rain,
            precipitation_below_rain,
            precipitation_below_heavy_rain,
        ],
        [
            no_precipitation,
            snow,
            WeatherConditions.ICE_PELLETS,
            WeatherConditions.FREEZING_RAIN,
            drizzle,
            light_rain,
            rain,
            WeatherConditions.HEAVY_RAIN,
        ],
        WeatherConditions.TORRENTIAL_RAIN,
    )


def get_weather_conditions(
    precipitation_type,
    thunder_type,
    fog_type,
    sky_conditions,
    wind_magnitude,
    precipitation_rate,
    convective_precipitation_rate,
    total_optical_density,
):
    precipitation_rate_mm_hr = precipitation_rate * 3600
    convective_precipitation_rate_mm_hr = convective_precipitation_rate * 3600

    # Shower Thresholds
    precipitation_below_clear = precipitation_rate_mm_hr <= 0.05
    precipitation_below_drizzle = precipitation_rate_mm_hr <= 0.5
    precipitation_below_light_rain = precipitation_rate_mm_hr <= 2.5
    precipitation_below_rain = precipitation_rate_mm_hr <= 7
    precipitation_below_heavy_rain = precipitation_rate_mm_hr <= 40

    # Snow Thresholds
    precipitation_below_light_snow = precipitation_rate_mm_hr <= 0.5
    precipitation_below_snow = precipitation_rate_mm_hr <= 2.5

    wind_below_blizzard = wind_magnitude <= 15
    wind_below_severe_blizzard = wind_magnitude <= 20

    is_fog = fog_type == FogType.FOG
    is_mist = fog_type == FogType.MIST

    clear_conditions = get_clear_conditions(
        precipitation_below_clear,
        precipitation_below_drizzle,
        precipitation_below_rain,
        precipitation_below_light_snow,
        precipitation_below_snow,
        precipitation_type,
    )
    partly_cloudy_conditions = get_partly_cloudy_conditions(
        precipitation_below_clear,
        precipitation_below_drizzle,
        precipitation_below_rain,
        precipitation_below_light_snow,
        precipitation_below_snow,
        precipitation_type,
    )
    mostly_cloudy_conditions = get_mostly_cloudy_conditions(
        precipitation_below_clear,
        precipitation_below_light_rain,
        precipitation_below_rain,
        precipitation_below_light_snow,
        precipitation_below_snow,
        wind_below_blizzard,
        wind_below_severe_blizzard,
        precipitation_type,
    )
    overcast_conditions = get_overcast_conditions(
        total_optical_density,
        convective_precipitation_rate_mm_hr,
        precipitation_below_clear,
        precipitation_below_drizzle,
        precipitation_below_light_rain,
        precipitation_below_rain,
        precipitation_below_heavy_rain,
        precipitation_below_light_snow,
        precipitation_below_snow,
        wind_below_blizzard,
        wind_below_severe_blizzard,
        is_fog,
        precipitation_type,
    )

    conditions = [
        thunder_type == ThunderType.SHOWERS,
        thunder_type == ThunderType.HEAVY_SHOWERS,
        thunder_type == ThunderType.STORMS,
        thunder_type == ThunderType.HEAVY_STORMS,
        thunder_type == ThunderType.SEVERE_STORMS,
        is_fog & precipitation_below_drizzle,
        is_mist & precipitation_below_clear,
        sky_conditions == SkyConditions.CLEAR,
        sky_conditions == SkyConditions.PARTLY_CLOUDY,
        sky_conditions == SkyConditions.MOSTLY_CLOUDY,
        sky_conditions == SkyConditions.OVERCAST,
    ]
    choices = [
        WeatherConditions.THUNDER_SHOWERS,
        WeatherConditions.HEAVY_THUNDER_SHOWERS,
        WeatherConditions.THUNDER_STORMS,
        WeatherConditions.HEAVY_THUNDER_STORMS,
        WeatherConditions.SEVERE_THUNDER_STORMS,
        WeatherConditions.FOG,
        WeatherConditions.MIST,
        clear_conditions,
        partly_cloudy_conditions,
        mostly_cloudy_conditions,
        overcast_conditions,
    ]
    weather_conditions = np.select(conditions, choices, None)
    if np.any(weather_conditions == None):  # noqa
        raise Exception('Weather conditions not found for some points.')

    return weather_conditions
