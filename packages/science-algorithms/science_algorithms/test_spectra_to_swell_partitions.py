import json
import os

import numpy as np  # type: ignore
import pytest  # type: ignore

from science_algorithms import SwellPartition
from science_algorithms.spectra_to_swell_partitions import (
    _calculate_mean,
    _find_neighbor_indices,
    _get_unique_partitions,
    _remove_noisy_partitions,
    _reorder_partitions,
    spectra_to_swell_partitions,
)


def test_find_neighbor_indices():
    directional_wave_spectra = np.array([[1.2, 2.4, 3.7], [4.5, 8.9, 10.12]])
    frequencies = np.array([1.2, 3.4, 2.2])
    directions = np.array([100, 130, 210])
    (
        max_frequency_index,
        max_direction_index,
        neighbors_x,
        neighbors_y,
    ) = _find_neighbor_indices(
        frequencies, directions, directional_wave_spectra
    )
    assert neighbors_x, neighbors_y == (
        [1, 1, 1, 2, 0, 0, 0, 2],
        [2, 1, 0, 0, 0, 1, 2, 2],
    )
    assert max_direction_index == 2
    assert max_frequency_index == 1

    directional_wave_spectra = np.array([[1.2, 2.4, 3.7], [10.12, 8.9, 4.12]])
    (
        max_frequency_index,
        max_direction_index,
        neighbors_x,
        neighbors_y,
    ) = _find_neighbor_indices(
        frequencies, directions, directional_wave_spectra
    )
    assert max_direction_index == 0
    assert max_frequency_index == 1
    assert neighbors_x, neighbors_y == (
        [2, 2, 2, 0, 1, 1, 1, 0],
        [2, 1, 0, 0, 0, 1, 2, 2],
    )

    directional_wave_spectra = np.array([[10.2, 2.4, 3.7], [1.12, 8.9, 4.12]])
    (
        max_frequency_index,
        max_direction_index,
        neighbors_x,
        neighbors_y,
    ) = _find_neighbor_indices(
        frequencies, directions, directional_wave_spectra
    )
    assert max_direction_index == 0
    assert max_frequency_index == 0
    assert neighbors_x, neighbors_y == (
        [2, 2, 2, 0, 1, 1, 1, 0],
        [1, 0, 0, 0, 0, 0, 1, 1],
    )

    directional_wave_spectra = np.array([[1.2, 2.4, 30.7], [1.12, 8.9, 4.12]])
    (
        max_frequency_index,
        max_direction_index,
        neighbors_x,
        neighbors_y,
    ) = _find_neighbor_indices(
        frequencies, directions, directional_wave_spectra
    )
    assert max_direction_index == 2
    assert max_frequency_index == 0
    assert neighbors_x, neighbors_y == (
        [2, 2, 2, 0, 1, 1, 1, 0],
        [1, 0, 0, 0, 0, 0, 1, 1],
    )


def test_calculate_mean():
    with open(
        os.path.join('fixtures', 'mean_sample.json')
    ) as mean_sample_file:
        values = json.load(mean_sample_file)
        # Use np.cos as angle function.
        assert (
            _calculate_mean(
                np.array(values['directional_wave_spectra']),
                (
                    np.array(values['positional_swell_1']),
                    np.array(values['positional_swell_2']),
                ),
                np.array(values['frequency_matrix']),
                np.array(values['directional_matrix']),
                2,
                np.array(values['frequency_matrix_interval']),
                np.cos,
            )
            == 0.050644660370296246
        )
        # Use np.sin as angle function.
        assert (
            _calculate_mean(
                np.array(values['directional_wave_spectra']),
                (
                    np.array(values['positional_swell_1']),
                    np.array(values['positional_swell_2']),
                ),
                np.array(values['frequency_matrix']),
                np.array(values['directional_matrix']),
                1,
                np.array(values['frequency_matrix_interval']),
                np.sin,
            )
            == -0.14984886365673486
        )


def test_reorder_partitions():
    with open(
        os.path.join('fixtures', 'reorder_partitions.json')
    ) as reorder_partitons_file:
        values = json.load(reorder_partitons_file)
        partitions_reordered, peak_partitions_reordered = _reorder_partitions(
            np.array(values['partitions']),
            np.array(values['peak_partitions']),
            np.array(values['directional_wave_spectra']),
            np.array(values['frequency_matrix_interval']),
            False,
        )
        partitions_reordered.tolist(), peak_partitions_reordered.tolist() == (
            values['partitions_reordered'],
            ['peak_partitions_reordered'],
        )

    # Reverse energy partitions.
    with open(
        os.path.join('fixtures', 'reorder_partitions_reversed.json')
    ) as reorder_partitons_file:
        values = json.load(reorder_partitons_file)
        partitions_reordered, peak_partitions_reordered = _reorder_partitions(
            np.array(values['partitions']),
            np.array(values['peak_partitions']),
            np.array(values['directional_wave_spectra']),
            np.array(values['frequency_matrix_interval']),
            True,
        )
        partitions_reordered.tolist(), peak_partitions_reordered.tolist() == (
            values['partitions_reordered'],
            ['peak_partitions_reordered'],
        )


def test_remove_noisy_partitions():
    with open(
        os.path.join('fixtures', 'remove_noisy_partitions.json')
    ) as remove_noisy_partitions_file:
        values = json.load(remove_noisy_partitions_file)
        (
            partitions_reordered,
            peak_partitions_reordered,
        ) = _remove_noisy_partitions(
            np.array(values['partitions']),
            np.array(values['directional_wave_spectra']),
            np.array(values['frequency_matrix_interval']),
            (values['noise_constant_a'], values['noise_constant_b']),
            np.array(values['frequency_matrix']),
            np.array(values['peak_partitions']),
        )
        partitions_reordered.tolist(), peak_partitions_reordered.tolist() == (
            values['partitions_removed'],
            values['peak_partitions_removed'],
        )

    with open(
        os.path.join('fixtures', 'remove_noisy_partitions.json')
    ) as remove_noisy_partitions_file:
        values = json.load(remove_noisy_partitions_file)
        (
            partitions_reordered,
            peak_partitions_reordered,
        ) = _remove_noisy_partitions(
            np.array(values['partitions']),
            np.array(values['directional_wave_spectra']),
            np.array(values['frequency_matrix_interval']),
            (0, values['noise_constant_b']),
            np.array(values['frequency_matrix']),
            np.array(values['peak_partitions']),
        )
        # Partitions should be unchanged due to the noise_constant being 0.
        partitions_reordered.tolist(), peak_partitions_reordered.tolist() == (
            values['partitions'],
            values['peak_partitions'],
        )


def test_get_unique_partitions():
    partitions = np.array([0.5, 0.5, 0.0, 1.23])
    assert _get_unique_partitions(partitions).tolist() == [0.5, 1.23]

    partitions = np.array([45.6, 10.5, 23.9, 1.23])
    assert _get_unique_partitions(partitions).tolist() == [10.5, 23.9, 45.6]


def test_spectra_to_swell_partitions():
    with open(
        os.path.join('fixtures', 'buoys_spectra.json')
    ) as buoys_spectra_file:
        values = json.load(buoys_spectra_file)
        for value in values:
            freq_list = value['freq_list']
            bandwidth = value['bandwidth']
            spectral_wave_density = np.array(value['spectral_wave_density'])
            swr1 = np.array(value['swr1'])
            swr2 = np.array(value['swr2'])
            swdir1 = np.array(value['swdir1'])
            swdir2 = np.array(value['swdir2'])
            original_swell_partitions = [
                SwellPartition(value[0], value[1], value[2])
                for value in value['swell_partitions']
            ]

            swell_partitions = spectra_to_swell_partitions(
                freq_list,
                bandwidth,
                spectral_wave_density,
                swr1,
                swr2,
                swdir1,
                swdir2,
            )

            for swell_partition, original_swell_partition in zip(
                swell_partitions, original_swell_partitions
            ):
                assert swell_partition.height == pytest.approx(
                    original_swell_partition.height, abs=10e-6, rel=10e-6
                )
                assert swell_partition.period == pytest.approx(
                    original_swell_partition.period, abs=10e-6, rel=10e-6
                )
                assert swell_partition.direction == pytest.approx(
                    original_swell_partition.direction, abs=10e-6, rel=10e-6
                )


def test_invalid_spectra_to_swell_partitions():
    with open(
        os.path.join('fixtures', 'buoys_spectra.json')
    ) as buoys_spectra_file:
        value = json.load(buoys_spectra_file)[0]
        freq_list = value['freq_list'][5:]
        bandwidth = value['bandwidth']
        spectral_wave_density = np.array(value['spectral_wave_density'])
        swr1 = np.array(value['swr1'])
        swr2 = np.array(value['swr2'])
        swdir1 = np.array(value['swdir1'][:10])
        swdir2 = np.array(value['swdir2'])

        with pytest.raises(ValueError) as error:
            spectra_to_swell_partitions(
                freq_list,
                bandwidth,
                spectral_wave_density,
                swr1,
                swr2,
                swdir1,
                swdir2,
            )
        assert 'All inputs must have the same length as the spectra.' in str(
            error.value
        )
