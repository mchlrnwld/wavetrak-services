# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [4.1.4] - 2021-11-19
### Fixed
- Remove feet conversion from energy height in `spectra_to_swell_partitions.py`.

## [4.1.3] - 2021-11-18
### Changed
- Improved spectra to swell partitioning algorithm for buoys.

## [4.1.2] - 2021-09-03
### Changed
- Updates swell partition parameter values per FE-752.

## [4.1.1] - 2021-05-19
### Changed
- Fixes a regression introduced in `4.0.0` which caused `py.typed` to be excluded from the package.

## [4.1.0] - 2021-05-04
### Changed
- `spectra_to_swell_partitions` module was added:
  - `spectra_to_swell_partitions` function converts spectra data into swell partitions. Up to 6 partitions may be given depending on the spectra input.

## [4.0.0] - 2021-03-24
### Changed
- `SurfHeight` class and the following modules were removed:
  - `types/surf_height_input.py`.
  - `machine_learning/machine_learning.py`.
  - `spectral_refraction/spectral_refraction.py`.
  - `approximate_swell_partition_from_spectra.py`.
  - `frequency_calculations.py`.
  - `surf_height_poly` function from `polynomial/polynomial.py` was removed. The `partition_wave_height_poly` remains which is used by the `swell_impact.py` module.

## [3.0.6] - 2021-01-13
### Changed
- `science_algorithms.get_swell_probability` improved. Is now independent of the order of ensemble members passed in.

## [3.0.5] - 2020-12-01
### Changed
- Modified to be alpine compatible.

### Fixed
- Updated `install_requires` in `setup.py` to require `h5py<3.0.0`.

## [3.0.4] - 2020-11-16
### Fixed
- Pins `h5py` to `2.10.0` in the conda environment to prevent runtime issues.

## [3.0.3] - 2020-11-16
### Fixed
- Fixed a bug where the `science_algorithms.spectral_refraction_model.calculate` function would try to calculate surf heights for inputs with an empty spectral refraction matrix.

## [3.0.2] - 2020-10-16
### Fixed
- Fixed a bug where batch calculating spectral refraction surf heights happens out of order for more than 10 inputs.

## [3.0.1] - 2020-09-23
### Fixed
- `science_algorithms.spectral_refraction_model.SpectralRefraction` defaults to using Docker on all non-Linux OS.

## [3.0.0] - 2020-08-19
### Added
- `breaking_wave_height_intercept` added to `SurfHeightInput`. Defaults to `None`.

### Changed
- `SurfHeightInput.spot_multiplier` renamed to `breaking_wave_height_coefficient`
- `breaking_wave_height_intercept` added to surf heights in `SurfHeight.calculate`.

## [2.0.0] - 2020-08-07
### Added
- `SurfHeightInput` to encapsulate input values for calculating surf heights.

### Changed
- `science_algorithms.surf_height.SurfHeight`:
  - Requires a `data_dir` argument, a path to a directory for writing temporary files to.
  - `calculate` method:
    - Calculates surf heights on a batch of inputs.
    - Returns list of surf min/max and validation errors (instead of raising `ValueError`).
- `science_algorithms.spectral_refraction_model.SpectralRefraction`:
  - Requires a `data_dir` argument, a path to a directory for writing spectra and matrix files to.
  - Pulls Docker image for macOS once instead of per execution.
  - `calculate` method:
    - Shares function signature with `science_algorithms.surf_height.SurfHeight.calculate`.
    - Writes matrix and spectra inputs to file system and calls a subprocess to calculate surf heights on all inputs.

## [1.1.0] - 2020-06-30
### Added
- `SwellPartition.spread` field defaults to `None` if no value is passed in.
- `approximate_spectra_from_swell_partitions` function in `science_algorithms/approximate_spectra_from_swell_partitions` to approximate 2D spectra from a list of swell partitions.
- `science_algorithms/frequency_calculations` to share common frequency calculations amongst other modules.

## [1.0.1] - 2020-04-22
### Added
- `scipy` dependency.

## [1.0.0] - 2020-04-08
### Fixed
- Fixes the `docker run` subprocess call in `SpectralRefraction.surf_height`. The full ECR repository name is now used.

### Changed
- Changed `SpectralRefraction.surf_height` to set the default for the `docker` parameter to `None`. The default was previously set to `False`. This is a breaking change that modifies the behavior of the function when the `docker` parameter is omitted. If the `docker` parameter is passed in as `None`, the function now does the following:
  - If the operating system is `macOS`, the `docker` parameter is set to `True`.
  - For all other operating systems, the `docker` parameter is set to `True`.
- Changed `SurfHeight.calculate` to set the default for the `docker` parameter to `None`. This is a breaking change as the default was previously `None`.

## [0.5.0] - 2020-04-03
### Added
- Added the `SpectralRefraction` class in `science_algorithms/spectral_refraction_model`:
  - This class implements the `surf_height` algorithm which calculates surf min/max using 2D spectra
  data and a spectral refraction matrix.
- Added `SPECTRAL_REFRACTION_MODEL` to `SurfHeight.calculate()` function, which in turn calls the
`SpectralRefraction.surf_height()` function.
- Added `make build` to Makefile to build the surf height binaries for linux and subsequently export them.
- Added `make push-to-ecr` to Makefile to build the `calculate-surf-heights` image and push it to ECR.

## [0.4.1] - 2020-03-30
### Changed
- SwellPartition type checks for NaN in addition to None. (Simplifies working with pandas.)

## [0.4.0] - 2020-03-23
### Added
- Add swell impact scoring function `calculate_impacts`.

## [0.3.0] - 2020-01-29
### Added
- Support for type-checking package usage in consuming code. See [PEP-561](https://www.python.org/dev/peps/pep-0561/).

## [0.2.1] - 2020-01-24
### Added
- `__eq__` method on `SwellPartition` compares `height`, `period`, and `direction`.

### [0.2.0] - 2020-01-20
### Changed
- Changed SwellPartition class to support `None` as a valid attribute type.

### Added
- Added the `__bool__()` method which defines the truth value for a SwellPartition object.
- Added logic in the `surf_height` algorithm to filter out SwellPartition's with `None` values. Also added a check if an empty list passed in, in which case the function returns `(0,0)`.

## [0.1.4] - 2019-12-16
- science_algorithms/surf_height.py allows `None` algorithm, which will return `(None, None)` surf min/max.

## [0.1.3] - 2019-12-03
### Added
- science_algorithms/transformations.py with functions for transforming and computing weather values.
- science_algorithms/enums.py that includes `FogType`, `PrecipitationType`, `SkyConditions`, `ThunderType`, `WeatherConditions`.

### Changed
- Replaced `tf-nightly-2.0-preview==2.0.0.dev20191002` with `tensorflow==2.1.0rc0` which removes HDFS as a required dependency.

## [0.1.2] - 2019-11-19
### Fixed dependencies to include package_data h5 files.

## [0.1.1] - 2019-11-18
### Added
- install_requires numpy>=1.17.3 and tf-nightly-2.0-preview==2.0.0.dev20191001.

### Changed
- Updated `MACHINE_LEARNING` algorithm model to new version that outputs min/max so we no longer have to manually bucket the output.

## [0.1.0] - 2019-11-14
### Added
- `MACHINE_LEARNING` algorithm for `surf_height`.

### Changed
- Modified `surf_height` to export a `SurfHeight` class instead of a `surf_height` function. The class loads the machine learning model in `__init__`. `calculate` method returns min and max surf heights in meters.
- Raise exception if `optimal_swell_direction` not provided for `POLYNOMIAL` `surf_height`.
- Removed SurfHeightAlgorithm enum in favor of using a string.

## [0.0.2] - 2019-10-07
### Added
- This CHANGELOG.md file, based on conventions established in [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
- Added the `get_swell_probability` algorithm, based on the existing MSW implementation. This algorithm accepts
WW3-Ensemble members and then computes the probability of the swell occurring as forecasted.
- Added python black to Makefile and formatted all existing files.

## [0.0.1] - 2019-09-25
- Added the MSW surf height algorithm to science algorithms packages, with the brand agnostic name polynomial.
