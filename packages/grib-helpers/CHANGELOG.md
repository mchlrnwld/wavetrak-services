# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2021-02-09
### Changed
- `parse_byte_ranges_from_grib_inventory` renamed `parse_byte_ranges_from_idx_url`.

### Added
- `parse_byte_ranges_from_inventory` function which can be used to parse byte ranges from in memory string.

## [0.2.3] - 2021-02-01
### Fixed
- Typings for numpy arrays are now `Any` type since `np.array` is invalid.

## [0.2.2] - 2020-07-28
### Changed
- `GFS_INDICES` include ice coverage variable.

## [0.2.1] - 2020-03-09
### Changed
- Updated the docstring for the `get_lat_lon_index` function in the `process` module. The docstring now gives
a better explanation for the more complex logic used in the function. More details on the required format
for the grid associated with the lats and lons was also added.

## [0.2.0] - 2020-03-06
### Added
- `get_lat_lon_index`  function added to `process` module. This function, given a target lat and lon,
returns the index for the lat and lon based on the definition of the grid.
- Added `NAM_INDICES` to `__init__.py`.

## [0.1.2] - 2020-02-28
### Added
- `NAM_INDICES` added to `indices` module to specify which variables are to be downloaded from NAM grib files.

## [0.1.1] - 2020-02-07
### Added
- `isort` and `autoflake` for automatic code formatting
### Changed
- Update `parse_byte_ranges_from_grib_inventory` to use `aiohttp`
### Removed
- Unused `boto` and `moto` dependencies

## [0.1.0] - 2020-01-29
### Added
  - Support for type-checking package usage in consuming code. See [PEP-561](https://www.python.org/dev/peps/pep-0561/).

## [0.0.1] - 2020-01-07
### Added
  - The `inventory` module contains the `parse_byte_ranges_from_grib_inventory` function, which gets a list of tuples representing the byte ranges from the given inventory file url.
  - The `indices` module is provided and exported for the client to be able to specify which variables are to be downloaded from the grib file. Currently this contains the `GFS_INDICES` constant for GFS variable data to be downloaded.
  - `process` module containts the following functions:
    - The `parse_grib` function which parses a grib file for the desired variables passed in. The variables represent a dictionary mapping between the desired variable name and the default keywords.
    - The `process_grib_data` function, given a grid, latitude values, longitude values, and any additional values desired, formats and outputs the data to stdout.
  - `variables` module contains constants which represent variable mappings for both `GFS` and `NAM`, `GFS_GRIB_VARIABLES` and `NAM_GRIB_VARIABLES` respectively. `NAM_GRIB_VARIABLES` are based upon my analysis of the variables after parsing the grib file for `nam`, and comparing them to `GFS_GRIB_VARIABLES`. These are exported for the client to then pass into the `parse_grib_data` function.
