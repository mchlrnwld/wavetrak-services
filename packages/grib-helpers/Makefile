ENV ?= dev

all: lint type-check test

format:
	@isort -rc .
	@autoflake --remove-all-unused-imports --remove-unused-variables --ignore-init-module-imports --recursive --in-place .
	@black .

.PHONY: format

lint:
	@flake8 .
	@black --check .

.PHONY: lint

test:
	@pytest .

.PHONY: test

type-check:
	@mypy .

.PHONY: type-check

deploy:
	@docker build -t wavetrak-grib-helpers .
	@docker run --rm -t -v ~/.pypirc:/root/.pypirc wavetrak-grib-helpers \
		"conda activate wavetrak-grib-helpers && python setup.py bdist_wheel upload -r ${ENV}"

.PHONY: deploy
