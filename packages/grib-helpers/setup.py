import setuptools  # type: ignore

with open('README.md', 'r') as f:
    long_description = f.read()

setuptools.setup(
    name='wavetrak-grib-helpers',
    version='1.0.0',
    author='Wavetrak',
    author_email='nerdery@surfline.com',
    description=(
        'A library of common functions used to process, parse, and download '
        'grib files.'
    ),
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://github.com/Surfline/wavetrak-services/common/grib-helpers',
    packages=setuptools.find_packages(),
    package_data={'': ['py.typed']},
    install_requires=['aiohttp==3.6.2'],
    python_requires='>=3.7',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
    ],
)
