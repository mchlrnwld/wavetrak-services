import os

from grib_helpers.inventory import parse_byte_ranges_from_inventory


def test_parse_byte_ranges_from_inventory():
    with open(os.path.join('fixtures', 'inventory.txt')) as file:
        inventory = file.read()

        # Find byte ranges for indices at beginning and end of inventory.
        indices = set([("CLWMR", "1 hybrid level"), ("VIS", "surface")])
        assert parse_byte_ranges_from_inventory(inventory, indices) == [
            (0, 39902),
            (1017409, None),
        ]

        # Find byte ranges for indices in the middle of inventory, and ignore
        # index that doesn't exist.
        indices = set([("RWMR", "1 hybrid level"), ("SNSR", "2 hybrid level")])
        assert parse_byte_ranges_from_inventory(inventory, indices) == [
            (134250, 187867)
        ]

        # Return empty list for index that doesn't exist.
        indices = set([("SNSR", "2 hybrid level")])
        assert parse_byte_ranges_from_inventory(inventory, indices) == list()
