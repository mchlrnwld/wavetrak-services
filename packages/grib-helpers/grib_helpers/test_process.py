import pytest  # type: ignore

from grib_helpers.process import get_lat_lon_index


def test_get_lat_lon_index():
    lats = [5.0, 4.0, 3.0, 2.0, 1.0, 0.0, -1.0, -2.0, -3.0, -4.0, -5.0]
    lons = [0.0, 0.5]
    # Test for index in top-left of the grid
    target_lat_lon = (5.0, 0.0)
    assert get_lat_lon_index(target_lat_lon, lats, lons) == (0, 0)
    # Test for index in top-right of the grid
    target_lat_lon = (5.0, 0.5)
    assert get_lat_lon_index(target_lat_lon, lats, lons) == (0, 1)
    # Test for index in middle of the grid
    target_lat_lon = (2.0, 0.5)
    assert get_lat_lon_index(target_lat_lon, lats, lons) == (3, 1)
    # Test for index in bottom-left of the grid
    target_lat_lon = (-5.0, 0.0)
    assert get_lat_lon_index(target_lat_lon, lats, lons) == (10, 0)
    # Test for index in bottom-right of the grid
    target_lat_lon = (-5.0, 0.5)
    assert get_lat_lon_index(target_lat_lon, lats, lons) == (10, 1)
    # Expect ValueError for latitude outside the bounds of the grid
    target_lat_lon = (6.0, 0.0)
    with pytest.raises(ValueError) as value_error:
        get_lat_lon_index(target_lat_lon, lats, lons)
    assert (
        f'Latitude: {target_lat_lon[0]} and longitude: {target_lat_lon[1]} '
        f'combination does not exist in the grid'
    ) in str(value_error.value)
    # Expect ValueError for latitude outside the bounds of the grid
    target_lat_lon = (-6.0, 0.0)
    with pytest.raises(ValueError) as value_error:
        get_lat_lon_index(target_lat_lon, lats, lons)
    assert (
        f'Latitude: {target_lat_lon[0]} and longitude: {target_lat_lon[1]} '
        f'combination does not exist in the grid' in str(value_error.value)
    )
    # Expect ValueError for longitude outside the bounds of the grid
    target_lat_lon = (5.0, 1.0)
    with pytest.raises(ValueError) as value_error:
        get_lat_lon_index(target_lat_lon, lats, lons)
    assert (
        f'Latitude: {target_lat_lon[0]} and longitude: {target_lat_lon[1]} '
        f'combination does not exist in the grid' in str(value_error.value)
    )
    # Expect ValueError for longitude outside the bounds of the grid
    target_lat_lon = (5.0, 1.0)
    with pytest.raises(ValueError) as value_error:
        get_lat_lon_index(target_lat_lon, lats, lons)
    assert (
        f'Latitude: {target_lat_lon[0]} and longitude: {target_lat_lon[1]} '
        f'combination does not exist in the grid' in str(value_error.value)
    )
    # Expect ValueError for latitude and longitude outside the bounds of the
    # grid
    target_lat_lon = (6.0, 1.0)
    with pytest.raises(ValueError) as value_error:
        get_lat_lon_index(target_lat_lon, lats, lons)
    assert (
        f'Latitude: {target_lat_lon[0]} and longitude: {target_lat_lon[1]} '
        f'combination does not exist in the grid' in str(value_error.value)
    )


def test_get_lat_lon_index_small_point_difference():
    # Test with lats and lons whose difference between
    # adjacent points is very small. This is to simulate
    # a common spread between adjacent WW3 points, where
    # numerous grids have high resolution.
    lats = [50.0, 49.933333, 49.866666]
    lons = [243.86666667, 243.93333333, 244.0]
    # Test for index in top-left of the grid
    target_lat_lon = (50.0, 243.86666667)
    assert get_lat_lon_index(target_lat_lon, lats, lons) == (0, 0)
    # Test for index in top-right of the grid
    target_lat_lon = (50.0, 244.0)
    assert get_lat_lon_index(target_lat_lon, lats, lons) == (0, 2)
    # Test for index in middle of the grid
    target_lat_lon = (49.933333, 243.93333333)
    assert get_lat_lon_index(target_lat_lon, lats, lons) == (1, 1)
    # Test for index in bottom-left of the grid
    target_lat_lon = (49.866666, 243.86666667)
    assert get_lat_lon_index(target_lat_lon, lats, lons) == (2, 0)
    # Test for index in bottom-right of the grid
    target_lat_lon = (49.866666, 244.0)
    assert get_lat_lon_index(target_lat_lon, lats, lons) == (2, 2)

    # Expect ValueError for latitude outside the bounds of the grid
    target_lat_lon = (49.799993, 243.86666667)
    with pytest.raises(ValueError) as value_error:
        get_lat_lon_index(target_lat_lon, lats, lons)
    assert (
        f'Latitude: {target_lat_lon[0]} and longitude: {target_lat_lon[1]} '
        f'combination does not exist in the grid' in str(value_error.value)
    )

    # Expect ValueError for latitude outside the bounds of the grid
    target_lat_lon = (50.066667, 243.86666667)
    with pytest.raises(ValueError) as value_error:
        get_lat_lon_index(target_lat_lon, lats, lons)
    assert (
        f'Latitude: {target_lat_lon[0]} and longitude: {target_lat_lon[1]} '
        f'combination does not exist in the grid' in str(value_error.value)
    )

    # Expect ValueError for longitude outside the bounds of the grid
    target_lat_lon = (50.0, 244.066667)
    with pytest.raises(ValueError) as value_error:
        get_lat_lon_index(target_lat_lon, lats, lons)
    assert (
        f'Latitude: {target_lat_lon[0]} and longitude: {target_lat_lon[1]} '
        f'combination does not exist in the grid' in str(value_error.value)
    )
    # Expect ValueError for longitude outside the bounds of the grid
    target_lat_lon = (50.0, 243.799999)
    with pytest.raises(ValueError) as value_error:
        get_lat_lon_index(target_lat_lon, lats, lons)
    assert (
        f'Latitude: {target_lat_lon[0]} and longitude: {target_lat_lon[1]} '
        f'combination does not exist in the grid' in str(value_error.value)
    )
    # Expect ValueError for latitude and longitude outside the bounds of the
    # grid
    target_lat_lon = (50.066667, 243.799999)
    with pytest.raises(ValueError) as value_error:
        get_lat_lon_index(target_lat_lon, lats, lons)
    assert (
        f'Latitude: {target_lat_lon[0]} and longitude: {target_lat_lon[1]} '
        f'combination does not exist in the grid' in str(value_error.value)
    )
