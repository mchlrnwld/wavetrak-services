import asyncio
import logging
from contextlib import AsyncExitStack
from typing import List, Optional, Set, Tuple

import aiohttp  # type: ignore

logger = logging.getLogger(__name__)


async def parse_byte_ranges_from_idx_url(
    idx_url: str,
    indices: Set[Tuple[str, str]],
    semaphore: Optional[asyncio.Semaphore] = None,
) -> List[Tuple[int, Optional[int]]]:
    """
    Gets a list of tuples representing the byte ranges from the given
    inventory file url.

    Args:
        idx_url: The url from which to get contents of the inventory file.
        indices: Set of variables whose byte ranges to search for from the
                 inventory file.

    Returns:
        List of tuples representing the byte range location of each variable
        index.
    """
    byte_ranges: List[Tuple[int, Optional[int]]] = []
    async with AsyncExitStack() as stack:
        if semaphore:
            await stack.enter_async_context(semaphore)

        session = await stack.enter_async_context(
            aiohttp.ClientSession(raise_for_status=True)
        )

        logger.info(f'Opening HTTP connection to {idx_url}')
        response = await stack.enter_async_context(session.get(idx_url))

        raw_inventory = await response.text()
        byte_ranges = parse_byte_ranges_from_inventory(raw_inventory, indices)

    return byte_ranges


def parse_byte_ranges_from_inventory(
    inventory: str, indices: Set[Tuple[str, str]],
) -> List[Tuple[int, Optional[int]]]:
    """
    Gets a list of tuples representing the byte ranges from inventory contents.

    Args:
        inventory: The contents of an inventory file.
        indices: Set of variables whose byte ranges to search for from the
                 inventory file.

    Returns:
        List of tuples representing the byte range location of each variable
        index.
    """
    parsed_inventory = [line.split(':') for line in inventory.split('\n')[:-1]]
    return _build_byte_ranges_from_inventory(parsed_inventory, indices)


def _build_byte_ranges_from_inventory(
    inventory: List[List[str]], indices: Set[Tuple[str, str]]
) -> List[Tuple[int, Optional[int]]]:
    """
    Gets a list of tuples representing the byte range indexes
    for the indices passed in. If the index doesn't exist,
    then it is not added to the final list.

    Args:
        inventory: List of parsed contents from a grib inventory file.
        indices:   Set of variables whose byte ranges
                   to search for from the inventory file.

    Returns:
        List of tuples representing the
        byte range location of each variable index.
    """
    byte_ranges = []
    for i, params in enumerate(inventory):
        if (params[3], params[4]) in indices:
            byte_start = int(params[1])
            byte_end = (
                int(inventory[i + 1][1]) - 1
                if i < len(inventory) - 1
                else None
            )
            byte_ranges.append((byte_start, byte_end))

    return byte_ranges
