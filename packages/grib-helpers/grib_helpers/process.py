import logging
import sys
from datetime import datetime
from typing import Any, Dict, List, Optional, Tuple

import numpy as np  # type: ignore
import pygrib  # type: ignore

logger = logging.getLogger('grib-helpers')


def get_lat_lon_index(
    target_lat_lon: Tuple[float, float], lats: List[float], lons: List[float]
) -> Tuple[int, int]:
    """
    Gets the index for the given latitude and longitude,
    based on the definition for the grid.
    The grid must be rectangular with the two sides
    parallel to the equator. Additionally, the grid
    must be regular with equal distances between
    each grid point.

    If the given latitude and longitude combination
    does not exist in the grid, then an exception is raised.

    NOTE: While there are simpler solutions to obtain the index,
    most notably the list.index() function, this solution
    allows for faster performance. It calculates the distance
    between each adjacent grid point and uses that distance
    to calculate the grid point index. It runs in `O(1)`,
    where simpler solutions like `list.index()` run in `O(n)`.

    Example:
        target_lat: 69.5
        target_lon: 1.0
        lats:
            [  77.5  77.0  69.5  ]
        lons:
            [  0.0   0.5   1.0   ]
        Returns:
            lat_index: 2
            lon_index: 2

    Args:
        target_lat_lon: Pair of lat and lon values to find the index for.
        lats:           List of floats representing latitude values.
        lons:           List of floats representing longitude values.

    Returns:
        A tuple containing the latitude index
        and the longitude index as int.
    """
    target_lat, target_lon = target_lat_lon
    first_lat = lats[0]
    first_lon = lons[0]

    adjacent_lat_diff = first_lat - lats[1]
    adjacent_lon_diff = lons[1] - first_lon

    # The order of subtraction differs because
    # the lat values decrement, while the lon values increment.
    # This can be interpreted as the traversing
    # the grid from top-left to bottom-right.
    target_lat_diff = first_lat - target_lat
    target_lon_diff = target_lon - first_lon

    lat_index = int(round(target_lat_diff / adjacent_lat_diff))
    lon_index = int(round(target_lon_diff / adjacent_lon_diff))

    if (lat_index > len(lats) - 1 or lat_index < 0) or (
        lon_index > len(lons) - 1 or lon_index < 0
    ):
        raise ValueError(
            f'Latitude: {target_lat} and longitude: {target_lon} combination '
            f'does not exist in the grid'
        )

    return lat_index, lon_index


def _find_variable_index(variable_strs: List[str], labels) -> Optional[int]:
    """
    Finds the index of the given variable in the variables list

    Args:
        variable_strs: All of the variables
                       in the run file
                       represented as a string.
        labels:        A list of labels:
                       description and forecast_hour
                       that are used to find the index
                       in the variable_strs list.

    Returns:
        The index where the variable exists, otherwise NoneType.
    """
    matching_indices = (
        i
        for i, variable in enumerate(variable_strs)
        if all(label in variable for label in labels)
    )
    return next(matching_indices, None)


def parse_grib(
    file: str, grib_variables: Dict[str, List[str]]
) -> Tuple[Any, Any, Dict[str, Any], Dict[str, str]]:
    """
    Parses a grib file to extract the variable data.
    Only the relevant variable data is
    added to the total list, which is
    contained in grib_variables.

    Args:
        file: The file of the grib file to process.

    Returns:
        numpy array of lats, numpy array of lons, a dictionary of grib
        variables with the associated numpy array values, and a dictionary of
        grib variables and their descriptions as string.
    """
    logger.info(f'Parsing {file}...')
    with pygrib.open(file) as grib:
        variables = grib.read()
        variable_strs = [f'{variable}' for variable in variables]
        variable_indices = [
            (key, _find_variable_index(variable_strs, labels))
            for key, labels in grib_variables.items()
        ]
        (lat, lon) = variables[0].latlons()
        values = {
            key: variables[i].data()[0]
            for key, i in variable_indices
            if i is not None
        }
        details = {
            key: variable_strs[i]
            for key, i in variable_indices
            if i is not None
        }
        return lat, lon, values, details


def _output_record(record_array: Any):
    """
    Outputs a numpy array as a list to stdout.

    Args:
        record_array: The numpy array to output to stdout.
    """
    record_csv = ','.join([str(value) for value in record_array.tolist()])
    sys.stdout.write(f'{record_csv}\n')


def process_grib_data(
    grid: str,
    forecast_hour_timestamp: datetime,
    lats: Any,
    lons: Any,
    variable_values: List[Any],
):
    """
    Takes a grid, lats and lons common to
    each forecast hour, and additional information data
    as desired in the variable_values list.
    All of the data in the list is then compressed
    into a numpy 3D array, which is then
    outputted to stdout.

    Args:
        grid:                    Grid associated with
                                 the grib data.
        forecast_hour_timestamp: A formatted timestamp of
                                 type string calculated from
                                 the run and forecast_hour.
        lats:                    Latitude coordinates
                                 as a numpy array
                                 for the given grid.
        lons:                    Longitude coordinates
                                 as a numpy array
                                 for the given grid.
        variable_values:         numpy arrays of additional variable values
                                 associated with the forecast_hour.
    """
    values = [
        np.full(lats.shape, grid),
        lats,
        lons,
        np.full(lats.shape, f'{forecast_hour_timestamp.isoformat()}Z'),
    ]
    values.extend(variable_values)
    np.apply_along_axis(_output_record, 2, np.dstack(tuple(values)))
