# GRIB Helpers

A library of common functions used to process, parse, and download grib files.

## Setup

See https://wavetrak.atlassian.net/wiki/spaces/WT/pages/73334794/PyPI+Package+Server for instructions on getting setup to download/upload packages to Artifactory.

Sample `$HOME/.pypirc` file:

```
[distutils]
index-servers =
  dev
  prod

[dev]
repository: https://surfline.jfrog.io/surfline/api/pypi/pypi-local-dev
username:
password:

[prod]
repository: https://surfline.jfrog.io/surfline/api/pypi/pypi-local
username:
password:
```

## Use

Install with `pip`, but before doing so, follow these steps:

1. Install `pygrib` via your application's `conda` environment. This is
necessary for `wavetrak-grib-helpers` to work correctly, as installing
`pygrib` via `pip` has been broken for some time.

2. Then run the following command:

```sh
aws s3 cp s3://sl-artifactory-prod/config/.pip/pip.conf ~/.pip/pip.conf
```

3. Lastly, install with `pip`.

```sh
$ pip install wavetrak-grib-helpers
```

## Development

### Setup Environment

Use [conda](https://docs.conda.io/en/latest/) to setup a development environment.

```sh
$ conda env create -f environment.yml
$ conda activate wavetrak-grib-helpers
```

### Install Local Package

To test changes in a local application install as an "editable" package.

```sh
$ pip install -e /path/to/grib-helpers/
```

### Deploy

Deploy to `dev` repository to test for local development.

```sh
$ ENV=dev make deploy
```
