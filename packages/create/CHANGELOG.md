# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [1.0.3](https://github.com/Surfline/wavetrak-services/compare/@surfline/create@1.0.2...@surfline/create@1.0.3) (2021-11-11)


### Bug Fixes

* resolve issues with python-lambda template ([0eea75f](https://github.com/Surfline/wavetrak-services/commit/0eea75ff952954a4df5052f63b5146daf7623f0d))



### 1.0.2 (2021-10-25)

**Note:** Version bump only for package @surfline/create





### 1.0.1 (2021-10-22)

**Note:** Version bump only for package @surfline/create





### 1.0.0 (2021-10-19)

**Note:** Version bump only for package @surfline/create
