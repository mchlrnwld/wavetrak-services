#!/usr/bin/env node

// roughly based on https://github.com/niiyeboah/create-lit-element/blob/2d599180a2301f69c41f3f8b7fac4a9062ce0f6d/bin/create.js

"use strict";

const os = require('os');
const fs = require('fs-extra');
const path = require('path');
const glob = require('glob');
const replace = require('replace-in-file');

const templates = {

  "python-function": {
    "name": /sample-python-function/g,
    "title": /Sample Python Function/g,
    "identifier": /sample_python_function/g,
    "description": /a sample python function/g
  },

  "nodejs-function": {
    "name": /sample-nodejs-function/g,
    "title": /Sample Node.js Function/g,
    "identifier": /sample_nodejs_function/g,
    "description": /a sample Node.js function/g
  },

  "nodejs-service": {
    "name": /sample-nodejs-service/g,
    "title": /Sample Node.js Service/g,
    "identifier": /sample_nodejs_service/g,
    "description": /a sample Node.js service/g
  }
}

function template(type, appname, description) {
  const info = templates[type]
  if (!info) {
    throw new Error(`Template type '${type}' is not supported`)
  }

  const temp = fs.mkdtempSync(path.join(os.tmpdir(), 'app-template-'));

  const src = path.join(__dirname, 'templates', type);
  fs.copySync(src, temp, { recursive: true })

  const words = appname.split('-');
  const apptitle = words.map(word => word[0].toUpperCase() + word.slice(1)).join(' ');
  const identifier = words.join('_');
  description = description || words.join(' ');

  const files = glob.sync(`${temp}/**/*`, { dot: true }).map(f => f.replace(`${temp}/`, ''));
  const from = [info.name, info.title, info.identifier, info.description];
  const to = [appname, apptitle, identifier, description];
  replace.sync({ files: files.map(f => path.resolve(temp, f)), from, to });
  console.log(from.map((f, i) => `${f} -> ${to[i]}`).join('\n'));

  return temp;
}

function copyOver(temp, dest, { overwrite, errorOnExist }) {
  fs.copySync(temp, dest, { recursive: true, overwrite, errorOnExist })
}

function cleanup(temp) {
  try {
    if (temp) {
      fs.rmdirSync(temp, { recursive: true });
    }
  } catch (error) {
    console.error(`An error occurred deleting the temp folder ${temp}: `, error);
  }
}

function createProject(type, name, dest, ifExists) {
  let temp;
  try {
    temp = template(type, name);
    copyOver(temp, dest, ifExists);
  } catch (error) {
    console.error("An error occurred copying the template: ", error)
  } finally {
    cleanup(temp);
  }
}

function main() {
  const argv = process.argv.slice(2)

  const type = argv[0]
  const dest = argv[1] || process.cwd();
  const name = path.basename(dest)

  // maybe there should be a command line argument for this
  const ifExists = { overwrite: false, errorOnExist: true };

  createProject(type, name, dest, ifExists);
}

main()
