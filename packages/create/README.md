# @surfline/create

This is an [`npm init` initializer](https://docs.npmjs.com/cli/init#description) for creating new projects in the the [wavetrak monorepo](https://github.com/Surfline/wavetrak-services).

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents <!-- omit in toc -->

- [Usage](#usage)
- [Contributing](#contributing)
  - [How it works](#how-it-works)
  - [New templates](#new-templates)
  - [Testing](#testing)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Usage

Enter one of the following commands:

```sh
npm init @surfline <project-type> <directory-name>
```

Or

```sh
yarn create @surfline <project-type> <directory-name>
```

Or

```sh
npx @surfline/create <project-type> <directory-name>
```

If you do not provide a `directory-name` it will default to your current working directory.

Currently these `project-type`s are supported:
- [`python-function`](./templates/python-function)
- [`nodejs-function`](./templates/nodejs-function)
- [`nodejs-service`](./templates/nodejs-service)

## Contributing

### How it works
The simple templating works by first guessing application name, title and description from `directory-name` and then substituting, for example for `python-function`,

- Instances of `"sample-python-function"` with the application name
- Instances of `"Sample Python Function"` with the title
- Instances of `"a sample python function"` with the description

Other `project-type`s may use different replacement tokens.

### New templates
To add support for a new `project-type` simply create a new directory of that name under [`/templates`](./templates). Make sure to use replacement tokens corresponding to the ones listed above. All replacement tokens used by a template should be listed in the `const` named `templates` defined in `index.js`[./index.js].

### Testing
Simply `cd` to a directory and pass the path to `index.js`[./index.js] as the first parameter to `node`, for example:

```console
~/create$ mkdir my-test-app
~/create/my-test-app$ cd ./my-test-app
~/create/my-test-app$ node ../index.js python-function
/sample-python-function/g -> my-test-app
/Sample Python Function/g -> My Test App
/a sample python function/g -> my test app
```
