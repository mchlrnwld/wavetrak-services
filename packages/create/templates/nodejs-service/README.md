# Sample Node.js Service

This project runs a sample Node.js service as an ECS task.


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents <!-- omit in toc -->

- [Development](#development)
- [Testing](#testing)
- [`infra` Directory](#infra-directory)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Development

```bash
nvm use # honors .nvmrc
cp .env.sample .env
# fill out the missing env variables
npm install
npm run startlocal
```

## Testing

This service requires coverage to stay above 95 percent on all metrics. Functions that have code that cannot be stubbed or tested should be ignored using the `/* istanbul ignore next */` or `/* istanbul ignore file */` comments.

Tests can be run using either of the following commands:

```sh
npm run test
```

```sh
npm run coverage
```

## `infra` Directory

This directory contains the terraform module to deploy and manage this service in ECS.
