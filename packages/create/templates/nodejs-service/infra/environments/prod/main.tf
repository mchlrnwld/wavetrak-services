provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/sample-nodejs-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "wt"
  application = "sample-nodejs-service"
  environment = "prod"
  dns_name    = "${local.application}.${local.environment}.surfline.com"
}

module "sample_nodejs_service" {
  source = "../../"

  company          = local.company
  environment      = local.environment
  application      = local.application
  dns_name         = local.dns_name
  ecs_cluster      = "sl-core-svc-${local.environment}"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  default_vpc          = "vpc-116fdb74"
  auto_scaling_enabled = true
  dns_zone_id          = "Z3LLOZIY0ZZQDE"
  load_balancer_arn    = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-4-prod/689f354baf4e976c"
  alb_listener_arn     = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-4-prod/689f354baf4e976c/d6a5a0222c6ea0c3"
  iam_role_arn         = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"

  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 800
  auto_scaling_min_size     = 2
  auto_scaling_max_size     = 5000
}
