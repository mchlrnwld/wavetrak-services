provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/sample-nodejs-service/sandbox/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "wt"
  application = "sample-nodejs-service"
  environment = "sandbox"
  dns_name    = "${local.application}.${local.environment}.surfline.com"
}

module "sample_nodejs_service" {
  source = "../../"

  company          = local.company
  environment      = local.environment
  application      = local.application
  dns_name         = local.dns_name
  ecs_cluster      = "sl-core-svc-${local.environment}"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  default_vpc          = "vpc-981887fd"
  auto_scaling_enabled = false
  dns_zone_id          = "Z3DM6R3JR1RYXV"
  load_balancer_arn    = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac"
  alb_listener_arn     = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac/b3d92f844160d459"
  iam_role_arn         = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
}
