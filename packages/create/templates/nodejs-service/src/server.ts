import { Server } from 'http';
import { Express, Request, Response, NextFunction } from 'express';
import { setupExpress, createLogger, setupLogsene } from '@surfline/services-common';
import config from './config';

setupLogsene(config.LOGSENE_KEY);

export const log = createLogger('sample-nodejs-service');

const trackRequests = (req: Request, _res: Response, next: NextFunction): void => {
  log.trace({
    action: req.path,
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export const setup = (): { app: Express; server: Server } =>
  setupExpress({
    log,
    port: config.EXPRESS_PORT as number,
    name: 'sample-nodejs-service',
    allowedMethods: ['GET', 'OPTIONS', 'POST'],
    handlers: [
      ['*', trackRequests],
      // add more handlres...
    ],
  });
