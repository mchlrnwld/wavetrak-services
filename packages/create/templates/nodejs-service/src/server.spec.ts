import chai from 'chai';
import 'chai-http';
import 'mocha';
import { setup } from './server';

const { expect } = chai;

describe('server', () => {
  let request: ChaiHttp.Agent;

  before(() => {
    const { app } = setup()
    request = chai.request(app).keepOpen();
  });

  after(() => {
    request.close();
  });

  it('should setup the server with a health endpoint', async () => {
    const response = await request.get('/health');

    expect(response).to.have.status(200);
    expect(response.body.status).to.equal(200);
    expect(response.body.message).to.equal('OK');
    expect(response.body.version).to.exist;
  });

  it('should return 404 on non-existing endpoint', async () => {
    const response = await request.get('/this-path-is-most-definitely-not-defined');

    expect(response).to.have.status(404);
    expect(response.body).to.be.empty;
  });
});
