/* istanbul ignore file */
import 'newrelic';
import { setup, log } from './server';

const start = async (): Promise<void> => {
  try {
    setup();
  } catch (err) {
    log.error({
      message: `App Initialization failed: ${err.message}`,
      stack: err,
    });
  }
};

start();
