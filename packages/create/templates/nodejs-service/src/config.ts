/* istanbul ignore file */
export default {
  EXPRESS_PORT: process.env.EXPRESS_PORT || 8081,
  LOGSENE_KEY: process.env.LOGSENE_KEY,
  LOGSENE_LEVEL: process.env.LOGSENE_LEVEL || 'debug',
};
