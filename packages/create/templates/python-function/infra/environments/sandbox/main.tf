provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "sample-python-function/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}
