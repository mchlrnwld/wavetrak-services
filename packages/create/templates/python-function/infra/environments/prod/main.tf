provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "sample-python-function/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "sample-python-function" {
  source = "../../"

  environment = "prod"
}
