provider "aws" {
  region = "us-west-1"
}

provider "newrelic" {
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "sample-python-function/staging/terraform.tfstate"
    region = "us-west-1"
  }
}
