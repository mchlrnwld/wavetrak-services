import datetime
import logging
import os

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def run(event, context):
    current_time = datetime.datetime.now().time()
    name = context.function_name
    region = os.environ["Region"]
    logger.info(f"Your cron function {name} in AWS region {region} ran at {str(current_time)}")
