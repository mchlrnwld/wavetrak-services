import newrelic from 'newrelic';
import '@newrelic/aws-sdk';
import mainHandler from './handlers';

export default newrelic.setLambdaHandler(async (event, context) => {
  try {
    const result = await mainHandler(event, context);
    return result;
  } catch (err) {
    console.error(err);
    throw err;
  }
});
