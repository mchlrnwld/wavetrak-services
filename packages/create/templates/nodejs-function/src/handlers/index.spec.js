import { expect } from 'chai';
import sinon from 'sinon';

import * as mainHandler from '.';

describe('handler', () => {
  let clock;

  beforeEach(() => {
    clock = sinon.useFakeTimers({
      now: new Date(2021, 1, 1),
    });
    sinon.stub(console, 'log');
  });
  afterEach(() => {
    clock.restore();
    console.log.restore();
  });

  it('should log the AWS region and current time', async () => {
    const response = await mainHandler.default({}, { functionName: 'runtimeName' });
    expect(response).to.be.undefined();

    expect(console.log).to.have.been.calledOnceWith(
      sinon.match(
        /Your cron function "runtimeName" in AWS region mars-east-1 ran at Mon Feb 01 2021 00:00:00 .*/,
      ),
    );
  });
});
