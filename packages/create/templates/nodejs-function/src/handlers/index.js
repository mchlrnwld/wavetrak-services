import config from '../config';

/**
 * Lambda handler for the default function
 */
export default async (event, context) => {
  const time = new Date();
  const name = context.functionName;
  const { region } = config;
  console.log(`Your cron function "${name}" in AWS region ${region} ran at ${time}`);
};
