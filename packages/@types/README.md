# @surfline/types

## Usage

First install the package as a development dependency
```sh
npm install --save-dev -E @surfline/types
```

Add the package as a type root so that typescript will properly read and include the types
```jsonc
// tsconfig.json
{
   "compilerOptions": {
       "typeRoots" : ["./node_modules/@surfline/types"]
   }
}
```
