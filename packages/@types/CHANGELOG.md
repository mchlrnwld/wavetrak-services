# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 1.0.0 (2020-12-01)

* chore: update dependencies ([900d49c](https://github.com/Surfline/wavetrak-services/commit/900d49c))


### BREAKING CHANGE

* upgrade typescript to v4




## <small>0.2.1 (2020-11-04)</small>

* PS-000 - Replace artifactoryonline.com references with jfrog.io. (#3664) ([21430cd](https://github.com/Surfline/wavetrak-services/commit/21430cd)), closes [#3664](https://github.com/Surfline/wavetrak-services/issues/3664)





## 0.2.0 (2020-07-16)

* feat: Update Wavetrak Types ([3738c13](https://github.com/Surfline/wavetrak-services/commit/3738c13))





## 0.1.0 (2020-06-11)

* feat: create @surfline/types ([6cadf0c](https://github.com/Surfline/wavetrak-services/commit/6cadf0c))
