import color from '../color';

const conditionColorMapV2 = new Map([
  ['VERY_POOR', {
    default: color('conditions-v2', 'very-poor'),
    dark: color('conditions-v2', 'very-poor')
  }],
  ['POOR', {
    default: color('conditions-v2', 'poor'),
    dark: color('conditions-v2', 'poor')
  }],
  ['POOR_TO_FAIR', {
    default: color('conditions-v2', 'poor-to-fair'),
    dark: color('conditions-v2', 'poor-to-fair')
  }],
  ['FAIR', {
    default: color('conditions-v2', 'fair'),
    dark: color('conditions-v2', 'fair')
  }],
  ['FAIR_TO_GOOD', {
    default: color('conditions-v2', 'fair-to-good'),
    dark: color('conditions-v2', 'fair-to-good')
  }],
  ['GOOD', {
    default: color('conditions-v2', 'good'),
    dark: color('conditions-v2', 'good')
  }],
  ['EPIC', {
    default: color('conditions-v2', 'epic'),
    dark: color('conditions-v2', 'epic')
  }],
  ['NONE', {
    default: color('blue-gray', 70),
    dark: color('blue-gray', 80)
  }]
]);

export default conditionColorMapV2;
