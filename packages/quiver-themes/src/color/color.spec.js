/* eslint-disable no-restricted-syntax */

import fs from 'fs';
import path from 'path';
import { expect } from 'chai';
import { parse } from 'scss-parser';
import createQueryWrapper from 'query-ast';
import color from './color';

const readFile = (file) => new Promise((resolve, reject) => {
  fs.readFile(file, (err, data) => {
    if (err) return reject(err);
    return resolve(data);
  });
});

const loadSCSSColors = async () => {
  const scssPath = path.resolve(__dirname, '..', 'stylesheets', 'common', '_colors.scss');
  const scssAST = parse(String(await readFile(scssPath)));
  const $ = createQueryWrapper(scssAST);

  const colorPaletteDeclaration = $(
    (n) => n.node.value === '__sl-color-palettes' && n.parent.node.type === 'property'
  )
    .first()
    .parent()
    .parent();

  const colorPalettes = colorPaletteDeclaration
    .children((n) => n.node.type === 'value')
    .children((n) => n.node.type === 'parentheses')
    .children((n) => n.node.type === 'declaration')
    .map((n1) => ({
      key: $(n1)
        .children((n2) => n2.node.type === 'property')
        .children((n2) => n2.node.type === 'string_single')
        .first()
        .value(),
      tones: $(n1)
        .children((n2) => n2.node.type === 'value')
        .children((n2) => n2.node.type === 'parentheses')
        .children((n2) => n2.node.type === 'declaration')
        .map((n2) => ({
          key: $(n2)
            .find((n3) => n3.node.type === 'identifier' || n3.node.type === 'number')
            .value(),
          hex: $(n2)
            .find((n3) => n3.node.type === 'color_hex')
            .value()
        }))
    }));

  return colorPalettes;
};

describe('color', () => {
  it('should return the color hex for the palette and tone', () => {
    expect(color('light-blue', 70)).to.equal('#2283DD');
  });

  it('should default tone to core', () => {
    expect(color('shark-blue')).to.equal('#385C6C');
  });

  it('should match one to one with colors in SCSS', async () => {
    const scssColorPalettes = await loadSCSSColors();

    for (const colorPalette of scssColorPalettes) {
      for (const tone of colorPalette.tones) {
        expect(color(colorPalette.key, tone.key)).to.equal(`#${tone.hex}`);
      }
    }
  });
});
