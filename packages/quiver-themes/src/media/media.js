/*
    WaveTrak Supported Media Breakpoints
*/

/* For the extra small devices (phones) */
export const smallMobileWidth = '350px';

/* For the extra small devices (iphone 6/7/8) */
export const mediumMobileWidth = '375px';

/* For the extra small devices (phones) */
export const largeMobileWidth = '512px';

/* For the extra large devices (phones/tablets in-between) */
export const xLargeMobileWidth = '635px';

/* For large desktop left rail */
export const desktopLeftRailWidth = '660px';

/* Small devices (tablets, 768px and up) */
export const tabletWidth = '769px';
export const tabletLargeWidth = '976px';
export const ipadProWidth = '1024px';

/* Small device ad-specific width (tablets, 728px and up) */
export const tabletAdWidth = '728px';

/* Medium devices (desktops, 992px and up) */
export const mediumWidth = '992px';

/* Large devices (large desktops, 1200px and up) */
export const desktopMediumWidth = '1440px';
export const desktopSmallWidth = '1256px';

/* X-Large devices (x-large desktops, TBD and up) */
export const desktopLargeWidth = '1712px';
export const desktopExtraLargeWidth = '2560px';
