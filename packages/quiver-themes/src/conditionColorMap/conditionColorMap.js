import color from '../color';

const conditionColorMap = new Map([
  ['FLAT', {
    default: color('gray', 50),
    dark: color('gray', 60)
  }],
  ['VERY_POOR', {
    default: color('gray', 50),
    dark: color('gray', 60)
  }],
  ['POOR', {
    default: color('light-blue', 50),
    dark: color('light-blue', 60)
  }],
  ['POOR_TO_FAIR', {
    default: color('light-blue', 50),
    dark: color('light-blue', 60)
  }],
  ['FAIR', {
    default: color('green', 50),
    dark: color('green', 60)
  }],
  ['FAIR_TO_GOOD', {
    default: color('green', 50),
    dark: color('green', 60)
  }],
  ['GOOD', {
    default: color('orange', 50),
    dark: color('orange', 60)
  }],
  ['VERY_GOOD', {
    default: color('orange', 50),
    dark: color('orange', 60)
  }],
  ['GOOD_TO_EPIC', {
    default: color('bright-red', 50),
    dark: color('bright-red', 60)
  }],
  ['EPIC', {
    default: color('bright-red', 50),
    dark: color('bright-red', 60)
  }],
  ['NONE', {
    default: color('blue-gray', 70),
    dark: color('blue-gray', 80)
  }]
]);

export default conditionColorMap;
