# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [8.2.1](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-themes@8.2.0...@surfline/quiver-themes@8.2.1) (2022-02-16)


### Bug Fixes

* Additional support for new condition colors in quiver-react ([85d1141](https://github.com/Surfline/wavetrak-services/commit/85d1141d5e35f4b12f5aceb5263d6e28985267d0))



## [8.2.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-themes@8.1.0...@surfline/quiver-themes@8.2.0) (2022-02-15)


### Features

* **SwellEvent:** update theme to support new purple bar theme ([f434da5](https://github.com/Surfline/wavetrak-services/commit/f434da5949e58c60562367a7b53293be79c04488)), closes [CP-138](https://wavetrak.atlassian.net/browse/CP-138)



## [8.1.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-themes@8.0.2...@surfline/quiver-themes@8.1.0) (2022-02-11)


### Features

* [FE-1179](https://wavetrak.atlassian.net/browse/FE-1179) Add support for new condition color scale classes in quiver-react ([d0f37b3](https://github.com/Surfline/wavetrak-services/commit/d0f37b327b02e53708c4e52f5bdfcfb8d24ed9ab))



## <small>8.0.2 (2022-02-11)</small>

* fix: minor ui polishes for freemium ctas ([e70babe](https://github.com/Surfline/wavetrak-services/commit/e70babe))





## <small>8.0.1 (2022-02-08)</small>

* fix: ui polish to freemium ctas ([50247e3](https://github.com/Surfline/wavetrak-services/commit/50247e3))





## [8.0.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-themes@7.4.5...@surfline/quiver-themes@8.0.0) (2022-02-02)


### ⚠ BREAKING CHANGES

* dart-sass compiler may not be 1 to 1, be aware of styling issues

### Chores

* migrate to dart-sass and fix deprecation warnings ([5220c8a](https://github.com/Surfline/wavetrak-services/commit/5220c8a9bffe67c0791f3e63fb4bf4e77f0b11de))
## <small>7.4.6 (2022-02-02)</small>

* fix: freemium component style updates ([1fbefc0](https://github.com/Surfline/wavetrak-services/commit/1fbefc0))





### [7.4.5](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-themes@7.4.4...@surfline/quiver-themes@7.4.5) (2022-01-31)


### Bug Fixes

* add missing cam timeout styles ([bc02e91](https://github.com/Surfline/wavetrak-services/commit/bc02e916d6ebaa9c85f0941f8f056caaaf314332))



## <small>7.4.4 (2022-01-27)</small>

* fix: sa-4039 add forecastgraphsdaysummariescta styles ([8667d5c](https://github.com/Surfline/wavetrak-services/commit/8667d5c))





## <small>7.4.3 (2022-01-13)</small>

* fix: DailyForecastReportCTA ui updates ([a90d442](https://github.com/Surfline/wavetrak-services/commit/a90d442))





## <small>7.4.2 (2022-01-07)</small>

* fix: SA-3917 - Adjust button width for CamCTA ([889d06f](https://github.com/Surfline/wavetrak-services/commit/889d06f))





## <small>7.4.1 (2022-01-06)</small>

* fix: SA-3917 UI updates to CamCTA ([aa2c153](https://github.com/Surfline/wavetrak-services/commit/aa2c153))





## 7.4.0 (2021-12-22)

* feat: SA-4056 Add styles for Daily Forecast Report CTA ([00ee1bc](https://github.com/Surfline/wavetrak-services/commit/00ee1bc))





## 7.3.0 (2021-12-21)

* feat: Add RegionalForecastUpdateCTA styles ([f6e62c5](https://github.com/Surfline/wavetrak-services/commit/f6e62c5))


## 7.2.0 (2021-12-16)

* feat: Add CamCTA styles ([5f6dd76](https://github.com/Surfline/wavetrak-services/commit/5f6dd76))

## [7.1.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-themes@7.0.0...@surfline/quiver-themes@7.1.0) (2021-11-03)


### Features

* add hover to state styles for automated ratings ([7b40dff](https://github.com/Surfline/wavetrak-services/commit/7b40dff19d53e7aa93ab5aa03fa28e0aba0a6fe9))



## [7.0.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-themes@6.15.1...@surfline/quiver-themes@7.0.0) (2021-10-29)


### ⚠ BREAKING CHANGES

* condition variation styles have been removed

### Miscellaneous Chores

* remove condition color variants in favor of V2 ([eb3cbf5](https://github.com/Surfline/wavetrak-services/commit/eb3cbf540740a2ed29fbbd51f9c601f078536f27))



### 6.15.1 (2021-10-21)

**Note:** Version bump only for package @surfline/quiver-themes





## 6.15.0 (2021-10-21)


### Features

* add AutomatedRatings styles ([c6411c1](https://github.com/Surfline/wavetrak-services/commit/c6411c11dc0f039cfec0d5b0e58defe2667d6e90))



### 6.14.7 (2021-10-19)

**Note:** Version bump only for package @surfline/quiver-themes





## <small>6.14.6 (2021-10-19)</small>

* WP 95: Migrate quiver/themes to packages/quiver-themes (#5698) ([22042e3](https://github.com/Surfline/wavetrak-services/commit/22042e3)), closes [#5698](https://github.com/Surfline/wavetrak-services/issues/5698)
* WP-95: resolve quiver-themes package publishing (#5959) ([9d17ec8](https://github.com/Surfline/wavetrak-services/commit/9d17ec8)), closes [#5959](https://github.com/Surfline/wavetrak-services/issues/5959)





## <small>6.14.5 (2021-10-15)</small>

* chore(quiver-themes): migrate quiver-themes to packages/ and update CODEOWNERS ([48d91ab](https://github.com/Surfline/wavetrak-services/commit/48d91ab))





# Quiver Themes Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## 6.14.4
  - FE-781: Adjust the size of the forecast graph bars to match the new desired interval hours
  - chore: bump dependencies

## 6.14.3
  - WP-171: Update padding to bottom of mobile menus from 68px to 180px

## 6.14.2
  - WP-171: Add padding to bottom of mobile menus for improved mobile experience
  - WP-129: Add `.nvmrc` file to ensure correct node version during local development

## 6.14.1
  - WP-65: Add `eslint:fix` to package scripts for autofixing lint errors

## 6.14.0
  - feat: Added bright-red 30 color

## 6.13.0
  - feat: Add `ContentCard` component

## 6.12.11
  - FE-374: Add Condition Ratings Variant 3
  - FE-374: Add Condition Ratings Variant 4

## 6.12.10
  - FE-316: Color for LOTUS condition

## 6.12.9
  - FE-300: Remove DST timestamps split logic
    - Moved `GraphContainerV2` into `GraphContainer`
    - Moved `ContinuousGraphContainerV2` into `ContinuousGraphContainer`

## 6.12.8
  - Remove `/surf-cams` page styles being applied on surf report pages

## 6.12.7
  - Add styles for OrBreak component

## 6.12.6
  - SA-3402: Add styles for CCPA links

## 6.12.5
  - FE-256: Support `GraphContainerV2` component updates

## 6.12.4
  - FE-256: Duplicate `GraphContainer.scss` styles into `GraphContainerV2.scss` to prevent breaking non-split functionality

## 6.12.3

  - Add style variations to  `ConditionDaySummaryV2.scss` file for LOLA conditions

## 6.12.2

  - Duplicated the `ColoredConditionBar.scss` file in order to prevent regression when updating the
    `ColoredConditionBarV2` component and it's styles
  - Duplicated the `ConditionDaySummary.scss` file in order to prevent regression when updating the
    `ConditionDaySummary` component and it's styles
  - Duplicated the `_conditions.scss` map in order to prevent regression when testing condition variations
  - Duplicated the `dualConditions.scss` mixin in order to prevent regression testing condition variations

## 6.12.1

  - Minor modifications to the styles used in `ContinuousGraphContainerV2` based on the changes
    made in quiver/react

## 6.12.0

  - Duplicated the `ContinuousGraphContainer.scss` file in order to prevent regression when updating the
    `ContinuousGraphContainerV2` component and it's styles

## 6.11.1

  - Removed `body`css which was overriding the background color of the entire page
  - Bump dependencies

## 6.10.2

  - Removed Gradient CSS for Reg Wall
  - Updated Box Shadow for Reg Wall
  - Upgrade deps

## 6.9.2

  - Patch: show jw player settings menu when speed controls are enabled

## 6.9.1

  - Patch: SpotDescriptor style updates

## 6.9.0

  - Feat: Add SpotDescriptor component

## 6.7.1

  - Update registry url to use `jfrog.io` instead of `artifactoryonline.com`

## 6.7.0

- Add IBM plex fonts
- Add new grey color

## 6.6.0

- Add Typescript typings to output
- Upgrade dependencies to latest versions
- Upgrade to Babel 7

## 6.0.0

- [BREAKING]: Updated all `sl-` prefixed classnames that used to live in `@surfline/quiver-react` to properly be prefixed with `quiver-`.
  This is a breaking change because the styles that are applied through this package will not apply properly unless the corresponding
  major version changes in `@surfline/quiver-react` have been consumed. Any front-ends still using quiver components with `sl-` prefixed
  classnames will have broken styling.

## 6.1.5

- Add `ProgressBar` theme file.

## 6.5.1

  - Create new `Leaflet` component to be imported in frontend components requiring `leaflet` functionality
