# @surfline/quiver-themes

## Installation

```sh
npm install --save @surfline/quiver-themes
```

## Development

If developing in conjunction with other `quiver` components, use `npm link`

```sh
npm install
```

Standing up `quiver-react` Storybook with `quiver-themes` linked 
```sh
npm i && npm run build && cd ../quiver-react && npm i && npm link ../quiver-themes && npm run storybook
```

## Component Class Naming Conventions

Components should be styled using class names prefixed with `quiver-` and following [BEM](http://getbem.com/) naming conventions.

### BEM TL;DR

Block-level component classes should be kabob cased and prefixed with `quiver-`. i.e. `quiver-text-area` for `TextArea` component.

Within a block-level component, elements should be attached to the block-level component class name by two underscores `__`. i.e. `quiver-text-area__message` for the `TextArea` component's message element.

For any component, modifiers should be attached to the component class name by two hyphens `--`. i.e. `quiver-text-area--success` for the `TextArea` component success state.

## Publishing NPM package

Within the `/packages/` directory we publish NPM packages using Lerna.

A step by step guide for publishing NPM packages in the monorepo can be found [here](../../README.md#npm-packages).
