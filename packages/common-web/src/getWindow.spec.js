/* eslint-disable no-undef */
import { expect } from 'chai';
import { getWindow } from '.';

describe('getWindow', () => {
  beforeEach(() => {
    window.document = { createElement: () => {} };
  });
  it('should return a window interface', () => {
    const wavetrakWindow = getWindow();
    expect(wavetrakWindow).to.be.equal(window);
    expect(getWindow()?.location.hostname ?? null).to.be.equal('surfline.com');
  });
  it('should return null', () => {
    delete window.document;
    const wavetrakWindow = getWindow();
    expect(wavetrakWindow).to.be.equal(null);
    expect(getWindow()?.location.hostname ?? null).to.be.equal(null);
  });
});
