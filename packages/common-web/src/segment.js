/* istanbul ignore file */
import warn from './warn';
import { getWindow } from './getWindow';

/**
 * Create a segment snippet to inject into the HTML template
 * @function snippet
 * @param {string} segmentKey - Segment key
 * @param {boolean} [withScriptTag]
 * @return {string} Injectable HTML snippet
 */
export const snippet = (segmentKey, withScriptTag = true) => `
  ${withScriptTag ? '<script type="text/javascript">' : ''}
    !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on","addSourceMiddleware","addIntegrationMiddleware","setAnonymousId","addDestinationMiddleware"];analytics.factory=function(e){return function(){var t=Array.prototype.slice.call(arguments);t.unshift(e);analytics.push(t);return analytics}};for(var e=0;e<analytics.methods.length;e++){var key=analytics.methods[e];analytics[key]=analytics.factory(key)}analytics.load=function(key,e){var t=document.createElement("script");t.type="text/javascript";t.async=!0;t.src="https://cdn.segment.com/analytics.js/v1/" + key + "/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n);analytics._loadOptions=e};analytics._writeKey="${segmentKey}";analytics.SNIPPET_VERSION="4.13.2";
    analytics.load('${segmentKey}')
  }}();
  ${withScriptTag ? '</script>' : ''}
`;

export const snippetWithoutLoad = (segmentKey, withScriptTag = true) => `
  ${withScriptTag ? '<script type="text/javascript">' : ''}
    !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on","addSourceMiddleware","addIntegrationMiddleware","setAnonymousId","addDestinationMiddleware"];analytics.factory=function(e){return function(){var t=Array.prototype.slice.call(arguments);t.unshift(e);analytics.push(t);return analytics}};for(var e=0;e<analytics.methods.length;e++){var key=analytics.methods[e];analytics[key]=analytics.factory(key)}analytics.load=function(key,e){var t=document.createElement("script");t.type="text/javascript";t.async=!0;t.src="https://cdn.segment.com/analytics.js/v1/" + key + "/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n);analytics._loadOptions=e};analytics._writeKey="${segmentKey}";analytics.SNIPPET_VERSION="4.13.2";
  }}();
  ${withScriptTag ? '</script>' : ''}
`;

/**
 * @param {string} type
 * @param {string} name
 */
const analyticsWarning = (type, name) => {
  warn(`Analytics not loaded, ${type} call for ${name} will be skipped`);
  // Return false to match analytics calls returning false if something goes wrong
  return false;
};

/**
 * @description Creates a listener on the given `element` that will
 * fire a track call with a timeout before the redirect when the link
 * is clicked. This function should be called once for each element
 * that is will be used for (either call in `componentDidMount` or `useEffect`)
 *
 * https://segment.com/docs/connections/sources/catalog/libraries/website/javascript/#track-link
 *
 * @param {HTMLElement} element
 * @param {string | function} event
 * @param {{ [key: string]: object | string } | function} [properties]
 * @returns {boolean}
 */
export const setupLinkTracking = (element, event, properties) => {
  const win = getWindow();
  if (!win?.analytics) {
    return analyticsWarning('trackLink', event);
  }

  return win.analytics.trackLink(element, event, properties);
};

/**
 * @description Performs a `window.analytics.track` call.
 *
 * NOTE: the callback argument will be called even if the
 * track call is not fired. It should be treated as a callback
 * for the wrapper function.
 * @param {string} event
 * @param {{[key: string]: object | string}} [properties]
 * @param {object} [options={}]
 * @param {Function} [callback=null]
 * @returns {boolean}
 */
export const trackEvent = (event, properties, options = {}, callback = null) => {
  const win = getWindow();
  if (!win?.analytics) {
    if (callback) {
      callback();
    }
    return analyticsWarning('track', event);
  }

  return win.analytics.track(event, properties, options, callback);
};

/**
 * @description Performs a Segment trackEvent for a subscription CTA
 * @param {{ [key: string]: object | string }} [properties]
 */
export const trackClickedSubscribeCTA = (properties) =>
  trackEvent('Clicked Subscribe CTA', {
    ...properties,
    // TODO: (SA-3619) ctaLocation is deprecated. Mapped from location property temporarily.
    ctaLocation: properties?.ctaLocation ?? properties?.location,
  });

/**
 * @description Performs a Segment trackEvent for a create account CTA
 * @param {{ [key: string]: object | string }} [properties]
 */
export const trackClickedCreateAccountCTA = (properties) =>
  trackEvent('Clicked Create Account CTA', properties);

/**
 * @description Performs a Semgment TrackEvent for a GA Campaign
 * @param {*} queryString a query string to parse out the details of the GA campaign
 */
export const trackCampaign = (queryString) => {
  if (
    getWindow()?.analytics &&
    queryString &&
    queryString.utm_source &&
    queryString.utm_medium &&
    queryString.utm_campaign
  ) {
    const gaCampaignTrackEvent = {
      campaignContent: queryString.utm_content ?? null,
      campaignId: queryString.utm_id ?? null,
      campaignMedium: queryString.utm_medium,
      campaignName: queryString.utm_campaign,
      campaignSource: queryString.utm_source,
      campaignTerm: queryString.utm_term ?? null,
      legacyMarketingId: queryString.mkt ?? null,
    };
    trackEvent('Clicked Promotion', gaCampaignTrackEvent);
  }
};

/**
 * @description Performs a page call when a page component is mounted
 * @param {string} [name]
 * @param {{ [key: string]: object | string }} [properties]
 * @param {string} [category]
 * @param {object} [options]
 * @returns {boolean}
 */
export const trackNavigatedToPage = (name, properties, category = null, options = {}) => {
  const win = getWindow();

  if (win == null) return null;

  if (!win?.analytics) {
    return analyticsWarning('page', name);
  }

  if (category) {
    return win.analytics.page(category, name, properties, options);
  }
  return win.analytics.page(name, properties, options);
};

/**
 * Create a resetAnonymousIdCookieSnippet to inject into the HTML template
 * @function resetAnonymousIdCookieSnippet
 * @param {boolean} [withScriptTag]
 * @return {string} Injectable HTML snippet
 */
export const resetAnonymousIdCookieSnippet = (withScriptTag = true) => `
  ${withScriptTag ? '<script type="text/javascript">' : ''}
  if(document.cookie.indexOf('sl_reset_anonymous_id_v2') !== -1){
    document.cookie = 'sl_reset_anonymous_id_v2=; Max-Age=0; path=/; domain=.surfline.com';
  }
  if(document.cookie.indexOf('sl_reset_anonymous_id') !== -1){
    document.cookie = 'sl_reset_anonymous_id=; Max-Age=0; path=/; domain=.surfline.com';
  }
  if(document.cookie.indexOf('sl_reset_anonymous_id_v3') == -1){
    document.cookie = 'ajs_anonymous_id=; Max-Age=0; path=/; domain=.surfline.com';
    localStorage.removeItem('ajs_anonymous_id');
    document.cookie = 'sl_reset_anonymous_id_v3=true; Max-Age=31536000; path=/'
  }
  ${withScriptTag ? '</script>' : ''}
`;

/**
 * @description The Segment Consent Manager is an analytics.js add-on with support to consent
 * management. At its core, the Consent Manager empowers your visitors to control and customize
 * their tracking preferences on a website. They can opt out entirely of being tracked, or
 * selectively opt out of tools in which they don’t want their information stored.
 */
export const consentManagerScript = () => `
<script
  src="https://unpkg.com/@segment/consent-manager@5.0.0/standalone/consent-manager.js"
  crossorigin="anonymous"
  defer
  integrity="sha384-Faksh8FKgpBzGdQLDwKe5L9/6YZmofEhEQRwRhM/rUvcJ2fFTEnF6Y3SCjLMTne/"
></script>`;

/**
 * @description An object representing custom consent categories - mapping custom categories
 * to Segment integrations.
 */
export const consentCustomCategories = {
  'Sale of Personal Data': {
    purpose: 'Choose "No" to opt out of the sale of your data',
    integrations: [
      'Facebook App Events',
      'Facebook Pixel',
      'Google Ads',
      'Google Analytics',
      'Omniture',
      'AdRoll',
      'Adjust',
      'TikTok Conversions',
    ],
  },
};

/**
 * @description A script containing a callback function for customizing the contests of the
 * Consent Manager component.
 * @param {string} segmentKey
 * @param {object} customCategories
 * @param {boolean} [withScriptTag=true]
 * @returns
 */
export const consentManagerConfig = (segmentKey, withScriptTag = true) => `
  ${withScriptTag ? '<script>' : ''}
  window.consentManagerConfig = function(exports) {
    var React = exports.React
    var bannerContent = React.createElement(
      'span',
      null,
      'Use this form to opt out of the sale of your data as per Surfline \\\\ Wavetrak Inc.’s privacy policy and applicable laws. Further information about Surfline’s privacy practices is available here:',
      ' ',
      React.createElement(
        'a',
        { href: 'https://www.surfline.com/privacy-policy', target: '_blank' },
        'https://www.surfline.com/privacy-policy'
      ),
      '.'
    )
    var bannerSubContent = 'You can change your preferences at any time.'
    var preferencesDialogTitle = 'Right to Opt-Out Form'
    var preferencesDialogContent = React.createElement(
      'span',
      null,
      React.createElement(
        'p',
        null,
        "Use this form to opt out of the sale of your data as per Surfline \\\\ Wavetrak Inc.’s privacy policy and applicable laws.",
      ),
      React.createElement(
        'p',
        null,
        'Further information about Surfline’s privacy practices is available here:',
      ),
      React.createElement(
        'a',
        { href: 'https://www.surfline.com/privacy-policy', target: '_blank', rel: "noopener noreferrer" },
        'https://www.surfline.com/privacy-policy'
      ),
    )
    var cancelDialogTitle = 'Are you sure you want to cancel?'
    var cancelDialogContent =
      'Your preferences have not been saved. By continuing to use our website, you՚re agreeing to our Website Data Collection Policy.'

    return {
      container: '#consent-manager',
      writeKey: '${segmentKey}',
      shouldRequireConsent: function() { return false },
      bannerContent: bannerContent,
      bannerSubContent: bannerSubContent,
      preferencesDialogTitle: preferencesDialogTitle,
      preferencesDialogContent: preferencesDialogContent,
      cancelDialogTitle: cancelDialogTitle,
      cancelDialogContent: cancelDialogContent,
      implyConsentOnInteraction: true,
      defaultDestinationBehavior: 'imply',
      customCategories: ${JSON.stringify(consentCustomCategories)}
    }
  }
${withScriptTag ? '</script>' : ''}`;
