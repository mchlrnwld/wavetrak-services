import { SECONDS_PER_HOUR, SECONDS_PER_DAY } from '../constants';

/** Creates a timestamp representing the most recent midnight in the desired timezone
 * @param  {Number} utcOffset the UTC offset for the timezone you want to know midnight for
 * @param  {Number} timestamp a timestamp representing now
 *
 * @returns {Number} an timestamp for the time when midnight occurs in the desired timezone
 */
const midnightOfDate = (utcOffset, timestamp = Math.floor(new Date() / 1000)) => {
  const localDate = timestamp + utcOffset * SECONDS_PER_HOUR;
  const localMidnight = localDate - (localDate % SECONDS_PER_DAY);
  const midnight = localMidnight - utcOffset * SECONDS_PER_HOUR;
  return midnight;
};

export default midnightOfDate;
