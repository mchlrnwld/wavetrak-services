const capitalizeFirstLetter = (text) => {
  if (!text) return text;
  const lowerCaseText = text.toLowerCase();
  return lowerCaseText.charAt(0).toUpperCase() + lowerCaseText.substring(1);
};

export default capitalizeFirstLetter;
