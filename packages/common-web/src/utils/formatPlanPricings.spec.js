import { expect } from 'chai';

import {
  convertToMonthlyCost,
  amountToFixed,
  getRenewalPrice,
  getSubscriptionPrice,
  getPlanAnnualAmount,
} from './formatPlanPricings';
import { STRIPE } from '../constants';

describe('formatPlanPricings', () => {
  describe('convertToMonthlyCost', () => {
    it('should properly convert the daily cost to monthly', () => {
      const cost = convertToMonthlyCost(2, 'day');
      expect(cost).to.be.equal(60.875);
    });
    it('should properly convert the weekly cost to monthly', () => {
      const cost = convertToMonthlyCost(10, 'week');
      expect(cost).to.be.equal(43.48214285714286);
    });
    it('should not alter the cost if it is already monthly', () => {
      const cost = convertToMonthlyCost(12, 'month');
      expect(cost).to.be.equal(12);
    });
    it('should properly convert the annual cost to monthly', () => {
      const cost = convertToMonthlyCost(12, 'year');
      expect(cost).to.be.equal(1);
    });
    it('should return 0 if not interval is passed', () => {
      const cost = convertToMonthlyCost(12);
      expect(cost).to.be.equal(0);
    });
    it('should return 0 if not amount is passed', () => {
      const cost = convertToMonthlyCost();
      expect(cost).to.be.equal(0);
    });
  });

  describe('amountToFixed', () => {
    it('should convert the amount to a fixed string', () => {
      const cost = amountToFixed(100);
      expect(cost).to.be.equal('1.00');
    });
  });

  describe('getSubscriptionPrice', () => {
    it('should convert the stripe subscription effective price to a fixed amount', () => {
      const cost = getSubscriptionPrice({ type: STRIPE, effectivePrice: 100 });
      expect(cost).to.be.equal('1.00');
    });
    it('should return null if the subscription is not stripe', () => {
      const cost = getSubscriptionPrice({ type: 'sometype', effectivePrice: 100 });
      expect(cost).to.be.equal(null);
    });
  });

  describe('getRenewalPrice', () => {
    it('should convert the renewalPrice to a fixed string', () => {
      const cost = getRenewalPrice({ renewalPrice: 100 });
      expect(cost).to.be.equal('$1.00');
    });
  });

  describe('getPlanAnnualAmount', () => {
    it('should return an annual amount for a daily cost', () => {
      const cost = getPlanAnnualAmount(1, 'daily');
      expect(cost).to.be.equal('365.00');
    });
    it('should return an annual amount for a monthly cost', () => {
      const cost = getPlanAnnualAmount(8, 'monthly');
      expect(cost).to.be.equal('96.00');
    });
  });
});
