import { format } from 'date-fns';

/**
 * Format a given date to MM-dd-yy format
 * @param {datetime} date
 */

const formatDateToMMDDYY = (date) => {
  const parsedDate = new Date(date);
  return format(parsedDate, 'MM-dd-yy');
};

export default formatDateToMMDDYY;
