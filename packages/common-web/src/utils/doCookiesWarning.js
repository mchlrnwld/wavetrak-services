import canUseDOM from '../canUseDOM';

let DID_COOKIES_WARNING = false;
const ENV = process.env.NODE_ENV;

/* istanbul ignore next */
const doCookiesWarning = (path, cookies) => {
  if (!canUseDOM && !cookies && !DID_COOKIES_WARNING && ENV === 'development') {
    // eslint-disable-next-line no-console
    console.log(
      `Warning: Server-side fetches need to have req.cookies passed in as \`option.cookies\` if you need an access token. If you do not need an access token for the fetch, you can safely ignore this warning
      * Check fetch at path: ${path}`,
    );
    DID_COOKIES_WARNING = true;
  }
};

export default doCookiesWarning;
