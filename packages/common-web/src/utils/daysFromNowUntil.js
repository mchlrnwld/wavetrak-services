import { differenceInDays } from 'date-fns';

/**
 * Get the number of full days between the current date and given date
 * @param {datetime} date
 */
const daysFromNowUntil = (date) => {
  const parsedDate = new Date(date);
  return differenceInDays(parsedDate, new Date());
};

export default daysFromNowUntil;
