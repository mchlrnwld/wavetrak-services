/* istanbul ignore file */
export const getFavoriteSubregions = async (favorites, fetchBatchSubregions) => {
  const subregionCounts = new Map();
  favorites.forEach(({ subregionId }) => {
    if (subregionCounts.has(subregionId)) {
      subregionCounts.set(subregionId, subregionCounts.get(subregionId) + 1);
    } else {
      subregionCounts.set(subregionId, 1);
    }
  });
  const maxCount = Math.max(...subregionCounts.values());
  const { subregions } = await fetchBatchSubregions([...subregionCounts.keys()].join(','));
  return {
    subregions,
    topSubregion: [...subregionCounts.entries()].find(([, count]) => count === maxCount)[0],
  };
};

export const getNearestSubregion = async (fetchNearestSpot) => {
  const {
    spot: { subregionId },
  } = await fetchNearestSpot();
  return subregionId;
};
