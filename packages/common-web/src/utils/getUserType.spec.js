import { expect } from 'chai';

import getUserType from './getUserType';

describe('getUserType', () => {
  it('should return the proper user type', () => {
    const vipAd = getUserType(['sl_vip_advertiser']);
    const vip = getUserType(['sl_vip']);
    const premium = getUserType(['sl_premium']);
    const registered = getUserType([], true);
    const anon = getUserType([], false);

    expect([vipAd, vip, premium, registered, anon]).to.deep.equal([
      'VIP_ADVERTISER',
      'VIP',
      'PREMIUM',
      'REGISTERED',
      'ANONYMOUS',
    ]);
  });
});
