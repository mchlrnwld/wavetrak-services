import { STRIPE } from '../constants';

/**
 * Format the given amount to fixed-point notation (rounding the result to 2 decimal places) and return its value as a string.
 * @param {string|number} amount
 */

export const amountToFixed = (amount) => (amount / 100).toFixed(2);

/**
 * Format the given amount to monthly pricing based on the given interval
 * @param {string|number} amount
 * @param {string|number} interval
 */

export const convertToMonthlyCost = (amount, interval) => {
  if (!amount) return 0;
  switch (interval) {
    case 'day':
      return (amount * 1461) / 48;
    case 'week':
      return ((amount / 7) * 1461) / 48;
    case 'month':
      return amount;
    case 'year':
      return amount / 12;
    default:
      return 0;
  }
};

export const getPlanAnnualAmount = (amount, interval) => {
  let annualAmount = amount;
  if (interval === 'daily') {
    annualAmount = amount * 365;
  }
  if (interval === 'monthly') {
    annualAmount = amount * 12;
  }
  return annualAmount.toFixed(2);
};

/**
 * Get and format the effective price for a given subscription
 * @param {object} subscription
 */

export const getSubscriptionPrice = (subscription) => {
  if (subscription.type !== STRIPE) return null;
  return amountToFixed(subscription.effectivePrice);
};

/**
 * Get and format the next renewal price for a given subscription
 * @param {object} subscription
 */

export const getRenewalPrice = (subscription) => `$${amountToFixed(subscription.renewalPrice)}`;
