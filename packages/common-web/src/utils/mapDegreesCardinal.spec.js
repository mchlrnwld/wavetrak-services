import { expect } from 'chai';

import mapDegreesCardinal from './mapDegreesCardinal';

describe('mapDegreesCardinal', () => {
  it('should map the degrees to the proper cardinal direction', () => {
    expect(mapDegreesCardinal(1)).to.be.equal('N');
    expect(mapDegreesCardinal(12)).to.be.equal('NNE');
    expect(mapDegreesCardinal(34)).to.be.equal('NE');
    expect(mapDegreesCardinal(57)).to.be.equal('ENE');
    expect(mapDegreesCardinal(79)).to.be.equal('E');
    expect(mapDegreesCardinal(102)).to.be.equal('ESE');
    expect(mapDegreesCardinal(124)).to.be.equal('SE');
    expect(mapDegreesCardinal(150)).to.be.equal('SSE');
    expect(mapDegreesCardinal(170)).to.be.equal('S');
    expect(mapDegreesCardinal(200)).to.be.equal('SSW');
    expect(mapDegreesCardinal(220)).to.be.equal('SW');
    expect(mapDegreesCardinal(240)).to.be.equal('WSW');
    expect(mapDegreesCardinal(260)).to.be.equal('W');
    expect(mapDegreesCardinal(290)).to.be.equal('WNW');
    expect(mapDegreesCardinal(310)).to.be.equal('NW');
    expect(mapDegreesCardinal(330)).to.be.equal('NNW');
    expect(mapDegreesCardinal(350)).to.be.equal('N');
  });
});
