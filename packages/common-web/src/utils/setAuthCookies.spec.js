import { expect } from 'chai';
import sinon from 'sinon';
import Cookies from 'universal-cookie';
import setAuthCookies from './setAuthCookies';

// 1/1/2019 00:00:00
const fakeCurrentTime = 1546300800;

describe('setAuthCookies', () => {
  let clock;
  const now = new Date(fakeCurrentTime * 1000);

  /** @type {sinon.SinonStubbedInstance<Cookies>} */
  let cookiesStub;

  beforeEach(() => {
    clock = sinon.useFakeTimers(now);
    cookiesStub = sinon.stub(Cookies.prototype, 'set');
    global.window.document = { createElement: () => {} };
  });

  afterEach(() => {
    clock.restore();
    cookiesStub.restore();
    delete global.window.document;
  });

  it('should set a short term cookie', () => {
    setAuthCookies('accessToken', 'refreshToken', null, false);

    expect(cookiesStub.callCount).to.equal(2);

    const firstCall = cookiesStub.getCall(0);
    const secondCall = cookiesStub.getCall(1);

    expect(firstCall).to.have.been.calledWithExactly('access_token', 'accessToken', {
      path: '/',
      maxAge: 1800,
      expires: new Date('Tue Jan 01 2019 00:30:00 GMT+0000 (Coordinated Universal Time)'),
      domain: 'surfline.com',
      secure: false,
    });
    expect(secondCall).to.have.been.calledWithExactly('refresh_token', 'refreshToken', {
      path: '/',
      maxAge: 1800,
      expires: new Date('Tue Jan 01 2019 00:30:00 GMT+0000 (Coordinated Universal Time)'),
      domain: 'surfline.com',
      secure: false,
    });
  });

  it('should handle a long-term cookie - stay signed in', () => {
    setAuthCookies('accessToken', 'refreshToken', 1, true, 'fishtrack.com');

    expect(cookiesStub.callCount).to.equal(2);

    const firstCall = cookiesStub.getCall(0);
    const secondCall = cookiesStub.getCall(1);

    expect(firstCall).to.have.been.calledWithExactly('access_token', 'accessToken', {
      path: '/',
      maxAge: 1,
      expires: new Date('Tue Jan 01 2019 00:00:01 GMT+0000 (Coordinated Universal Time)'),
      domain: 'fishtrack.com',
      secure: false,
    });
    expect(secondCall).to.have.been.calledWithExactly('refresh_token', 'refreshToken', {
      path: '/',
      maxAge: 31536000,
      expires: new Date('Tue Jan 01 2020 00:00:00 GMT+0000 (Coordinated Universal Time)'),
      domain: 'fishtrack.com',
      secure: false,
    });
  });
});
