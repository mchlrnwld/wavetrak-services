import { expect } from 'chai';

import { postedDate } from './formatReportDate';

const postTimestamp = 1546300800;

describe('postedDate', () => {
  it('should format the report posted date', () => {
    const date = postedDate(postTimestamp, -8, 'PST');
    expect(date).to.equal('Mon at 4:00pm PST');
  });

  it('should format the report posted date with the extended format', () => {
    const date = postedDate(postTimestamp, 0, 'UTC', true);
    expect(date).to.equal('Tue, January 1 at 12:00am UTC');
  });
});
