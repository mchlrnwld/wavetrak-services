import { expect } from 'chai';

import getMaxWindSpeed from './getMaxWindSpeed';

describe('getMaxWindSpeed', () => {
  it('should return binned max wind speeds for KTS', () => {
    const ten = getMaxWindSpeed(9, 'KTS');
    const twenty = getMaxWindSpeed(19, 'KTS');
    const thirty = getMaxWindSpeed(29, 'KTS');

    expect(ten).to.equal(10);
    expect(twenty).to.equal(20);
    expect(thirty).to.equal(30);
  });

  it('should return binned max wind speeds for MPH', () => {
    const eleven = getMaxWindSpeed(9, 'MPH');
    const twentyThree = getMaxWindSpeed(22, 'MPH');
    const thirtyFour = getMaxWindSpeed(33, 'MPH');

    expect(eleven).to.equal(11.51);
    expect(twentyThree).to.equal(23.02);
    expect(thirtyFour).to.equal(34.52);
  });

  it('should return binned max wind speeds for KPH', () => {
    const eighteen = getMaxWindSpeed(10, 'KPH');
    const thirtySeven = getMaxWindSpeed(22, 'KPH');
    const fiftyFive = getMaxWindSpeed(40, 'KPH');

    expect(eighteen).to.equal(18.52);
    expect(thirtySeven).to.equal(37.04);
    expect(fiftyFive).to.equal(55.56);
  });

  it('should return binned max wind speeds for KTS for hourly graphs', () => {
    const ten = getMaxWindSpeed(9, 'KTS', true);
    const twenty = getMaxWindSpeed(19, 'KTS', true);
    const twentyFive = getMaxWindSpeed(22, 'KTS', true);
    const thirty = getMaxWindSpeed(29, 'KTS', true);

    expect(ten).to.equal(15);
    expect(twenty).to.equal(20);
    expect(twentyFive).to.equal(25);
    expect(thirty).to.equal(30);
  });

  it('should return binned max wind speeds for MPH for hourly graphs', () => {
    const eleven = getMaxWindSpeed(9, 'MPH', true);
    const twentyThree = getMaxWindSpeed(22, 'MPH', true);
    const twentyEight = getMaxWindSpeed(25, 'MPH', true);
    const thirtyFour = getMaxWindSpeed(33, 'MPH', true);

    expect(eleven).to.equal(17.265);
    expect(twentyThree).to.equal(23.02);
    expect(twentyEight).to.equal(28);
    expect(thirtyFour).to.equal(34.52);
  });

  it('should return binned max wind speeds for KPH  for hourly graphs', () => {
    const eighteen = getMaxWindSpeed(10, 'KPH', true);
    const thirtySeven = getMaxWindSpeed(22, 'KPH', true);
    const fortyFive = getMaxWindSpeed(40, 'KPH', true);
    const fiftyFive = getMaxWindSpeed(46, 'KPH', true);

    expect(eighteen).to.equal(27.78);
    expect(thirtySeven).to.equal(37.04);
    expect(fortyFive).to.equal(45);
    expect(fiftyFive).to.equal(55.56);
  });
});
