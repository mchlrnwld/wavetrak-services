import { expect } from 'chai';

import getWaveUnits from './getWaveUnits';
import { FEET, HAWAIIAN } from '../constants';

describe('getWaveUnits', () => {
  it('should return the displayed wave units', () => {
    expect(getWaveUnits('FT')).to.equal('FT');
    expect(getWaveUnits(FEET)).to.equal(FEET);
    expect(getWaveUnits('M')).to.equal('M');
    expect(getWaveUnits('FAKE')).to.equal('FAKE');
    expect(getWaveUnits(null)).to.equal(null);
  });

  it('should not display HI wave units', () => {
    expect(getWaveUnits('HI')).to.equal('FT');
    expect(getWaveUnits(HAWAIIAN)).to.equal(FEET);
  });
});
