import { expect } from 'chai';
import { getTop3Swells } from './swells';

describe('utils / getTop3Swells', () => {
  const swells = [
    {
      height: 4.2,
      period: 18,
      direction: 220,
      directionMin: 219,
      index: 0,
    },
    {
      height: 8,
      period: 20,
      direction: 200,
      directionMin: 199,
      index: 1,
    },
    {
      height: 0,
      period: 0,
      direction: 0,
      directionMin: 0,
      index: 2,
    },
    {
      height: 1,
      period: 2,
      direction: 210,
      directionMin: 209,
      index: 3,
    },
    {
      height: 2,
      period: 3,
      direction: 230,
      directionMin: 229,
      index: 4,
    },
  ];

  it('should return 3 largest swells by height in the order of impact', () => {
    const topSwells = getTop3Swells(swells);
    expect(topSwells).to.have.length(3);
    expect(topSwells[0]).to.deep.equal(swells[1]);
    expect(topSwells[1]).to.deep.equal(swells[0]);
    expect(topSwells[2]).to.deep.equal(swells[4]);
  });
});
