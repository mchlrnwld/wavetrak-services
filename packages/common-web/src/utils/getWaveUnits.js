import { FEET, HAWAIIAN } from '../constants';

const getWaveUnits = (unitType) => {
  if (unitType == null) return null;
  if (unitType === HAWAIIAN) return FEET;
  return unitType; // no transformation needed
};

export default getWaveUnits;
