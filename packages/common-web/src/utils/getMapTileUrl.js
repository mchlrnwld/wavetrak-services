/* istanbul ignore file */
const MAP_TILE_KEY = '3tFgnOQBQixe61aigsBT';
const MAP_TILE_URL = 'https://api.maptiler.com/maps/062c0d04-1842-4a45-8181-c5bec3bf2214';

// https://tileserver.readthedocs.io/en/latest/endpoints.html
const getMapTileUrl = (width, height, lat, lon, zoom = 13) =>
  `${MAP_TILE_URL}/static/${lon},${lat},${zoom}/${width}x${height}.png?key=${MAP_TILE_KEY}&attribution=0`;

export default getMapTileUrl;
