/* istanbul ignore file */
import { doExternalFetch } from '../api/baseFetch';

const oEmbedURL = 'https://graph.facebook.com/';
const oembedHosts = {
  instagram: `${oEmbedURL}v11.0/instagram_oembed?url=`,
  facebook: `${oEmbedURL}v11.0/oembed_video?url=`,
  vimeo: 'https://vimeo.com/api/oembed.json?url=',
  youtube: 'https://www.youtube.com/oembed?url=',
  twitter: 'https://publish.twitter.com/oembed?url=',
};

const getOembedObj = async (mediaHost, mediaUrl, fetchUrl) => {
  let oembedObj = await doExternalFetch(`${fetchUrl}?url=${oembedHosts[mediaHost]}${mediaUrl}`);
  if (mediaHost === 'facebook') {
    const [facebookId] = mediaUrl.match(/([0-9]+)/);
    oembedObj = {
      ...oembedObj,
      thumbnail_url: `${oEmbedURL}${facebookId}/picture}`,
    };
  }
  return oembedObj;
};

const getPathPart = (path, index) => {
  const parts = path.split('/');
  return parts.length > index ? parts[index] : null;
};

const fetchOembed = async (mediaUrl, fetchUrl) => {
  let mediaHost = null;
  let oembedObj = {};
  if (mediaUrl.indexOf('youtu') > -1) {
    // use youtu for YouTube short url e.g. youtu.be
    mediaHost = 'youtube';
    oembedObj = await getOembedObj(mediaHost, mediaUrl, fetchUrl);
  }
  if (mediaUrl.indexOf('vimeo') > -1) {
    mediaHost = 'vimeo';
    oembedObj = await getOembedObj(mediaHost, mediaUrl, fetchUrl);
  }
  if (mediaUrl.indexOf('facebook') > -1) {
    mediaHost = 'facebook';
    oembedObj = await getOembedObj(mediaHost, mediaUrl, fetchUrl);
  }
  if (mediaUrl.indexOf('instagr') > -1) {
    mediaHost = 'instagram';
    let shortUrl = mediaUrl;
    if (mediaUrl.indexOf('instagram') > -1) {
      shortUrl = `https://instagr.am/p/${getPathPart(mediaUrl, 4)}`;
    }
    oembedObj = await getOembedObj(mediaHost, shortUrl, fetchUrl);
  }
  if (mediaUrl.indexOf('twitter') > -1) {
    mediaHost = 'twitter';
    oembedObj = await getOembedObj(mediaHost, mediaUrl, fetchUrl);
  }
  return { oembedObj, mediaHost };
};

export default fetchOembed;
