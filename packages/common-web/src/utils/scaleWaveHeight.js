/* istanbul ignore file */
import _round from 'lodash/round';

const binMap = {
  FT: [6, 12, 20],
  M: [1.83, 3.66, 6.1],
};

export const DEFAULT_SWELL_MAX = { FT: 3, M: 0.91 };

export const getMaxWaveHeight = (maxHeight, units) => {
  const bins = binMap[units] || [];
  const digits = units === 'M' ? 1 : 0;
  return bins.find((bin) => maxHeight <= bin) || _round(maxHeight, digits);
};

const scaleLinearWaveHeight = (maxWaveHeight, waveHeight, maxPixels) =>
  (maxPixels * waveHeight) / maxWaveHeight;

export const scalePowerWaveHeight = (maxWaveHeight, waveHeight, maxPixels) =>
  maxPixels * Math.sqrt(_round(waveHeight, 1) / maxWaveHeight);

const scaleWaveHeight = (maxSurfHeight, surfHeight, units, maxPixels = 140) => {
  const maxWaveHeight = getMaxWaveHeight(maxSurfHeight, units);
  const heightInPixels = scaleLinearWaveHeight(maxWaveHeight, surfHeight, maxPixels);
  return heightInPixels;
};

export default scaleWaveHeight;
