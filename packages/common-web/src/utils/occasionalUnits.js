const occasionalUnits = (occasionalHeight, units) =>
  occasionalHeight ? ` occ. ${occasionalHeight} ${units.toLowerCase()}` : null;

export default occasionalUnits;
