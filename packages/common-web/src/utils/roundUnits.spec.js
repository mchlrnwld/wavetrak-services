import { expect } from 'chai';

import { roundSurfHeight } from './roundUnits';

describe('roundUnits', () => {
  it('should round FT units to the nearest whole number', () => {
    const result = roundSurfHeight(1.21, 'FT');

    expect(result).to.equal(1);
  });
  it('should round M units to the nearest tenth', () => {
    const result = roundSurfHeight(1.21, 'M');

    expect(result).to.equal(1.2);
  });
  it('should not round unknown units', () => {
    const result = roundSurfHeight(1.21, 'blah');

    expect(result).to.equal(1.21);
  });
});
