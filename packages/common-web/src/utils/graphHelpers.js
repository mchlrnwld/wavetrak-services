/* istanbul ignore file */

/* eslint-disable import/prefer-default-export */

import { SECONDS_PER_DAY } from '../constants';

/**
 * @description A utility function for the Forecast Graphs which uses the current datetime
 * and the sunlightTimes returned from the Weather API to return the percent of the graph
 * at which we will place the "current time" marker. For example at Noon this function would return
 * .50 which will be halfway through the graph where noon would be placed
 * @param {{ utcOffset: number }} sunlightTimes
 * @param {number} now
 */
export const currentGraphMarkerXPercentageFn = (sunlightTimes, now) => {
  if (!sunlightTimes || (sunlightTimes.utcOffset !== 0 && !sunlightTimes.utcOffset)) {
    return null;
  }
  const localTimestamp = now + sunlightTimes.utcOffset * 3600;
  return (localTimestamp % SECONDS_PER_DAY) / SECONDS_PER_DAY;
};

/**
 * @description Utility function which takes a data set and sections it into an array of `days`
 * by using the `intervalHours` used to request the data. The data comes in as a flat array of
 * data points, so using the interval hours we can separate the data into individual days
 * @param {*} data Data to section into days
 * @param {*} intervalHours Interval hours of the data points
 * @param {*} map Custom map function
 */
export const transformGraphDataToDays = (data, intervalHours, map = (point) => point) => {
  const dataPointsPerDay = 24 / intervalHours;
  const numberOfDays = data.length / dataPointsPerDay;
  return [...Array(Math.round(numberOfDays))].map((_, index) => {
    const start = dataPointsPerDay * index;
    const end = start + dataPointsPerDay;
    return data.slice(start, end).map(map);
  });
};

/**
 * @description Utility function which takes a tide data set and sections it into an array of `days`.
 * The data comes in as a flat array of data points but has HIGH and LOW tide points so this function
 * normalizes the data into a data structure consumable by the graphs
 * @param {*} data Data to section into days
 */
export const transformTideGraphDataToDays = (data) =>
  data.reduce(
    (days, currentTime) => {
      const compar = data[0].timestamp + 86400 * days.length;
      const previousDays = days.slice(0, days.length - 1);
      const currentDay = [...days[days.length - 1], currentTime];
      if (currentTime.timestamp < compar) {
        return [...previousDays, currentDay];
      }
      const nextDay = [currentTime];
      return [...previousDays, currentDay, nextDay];
    },
    [[]],
  );
