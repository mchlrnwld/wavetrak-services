/**
 * @description Geo target service helkper function - Parses the subdivisions array and returns the iso_code if found.
 * @function findSubdivisionCode
 * @param {Array<{ geoname_id: string, iso_code: string, name: string }>} subdivisions
 * @returns {string || null}
 */
const findSubdivisionCode = (subdivisions) => {
  if (!subdivisions) return null;
  const result = subdivisions.find((elem) => !!elem?.iso_code);
  return result?.iso_code;
};

export default findSubdivisionCode;
