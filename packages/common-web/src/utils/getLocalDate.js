import { addDays } from 'date-fns';

/**
 * Returns a Date object that pretends to be local to the timezone provided by the UTC offset.
 * @param  {Number} timestamp Unix timestamp (seconds from epoch)
 * @param  {Number} utcOffset UTC offset in hours
 * @return {Date}           Date object pretending to be local to the timezone provided
 */
const getLocalDate = (timestamp, utcOffset) => {
  const localOffset = new Date(timestamp * 1000).getTimezoneOffset() / 60;
  return new Date(timestamp * 1000 + (utcOffset + localOffset) * 3600 * 1000);
};

/**
 * Returns a Date object that pretends to be a future day local to the timezone provided by the UTC offset.
 * @param  {Number} daysToAdd Int days to add
 * @param  {Number} utcOffset UTC offset in hours
 * @return {Date}             Date object pretending to be local to the timezone provided
 */
export const getLocalDateForFutureDay = (daysToAdd, utcOffset) =>
  addDays(getLocalDate(new Date() / 1000, utcOffset), daysToAdd);

export default getLocalDate;
