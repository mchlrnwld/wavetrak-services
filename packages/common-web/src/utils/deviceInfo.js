/* istanbul ignore file */
import platform from 'platform';

const deviceInfo = () => ({
  deviceId: `${platform.name}-${platform.version}`,
  deviceType: platform.description,
});

export default deviceInfo;
