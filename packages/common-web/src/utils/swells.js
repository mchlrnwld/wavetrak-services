export const swellImpact = (swell) => swell.height * (0.1 * swell.period);

export const getTop3Swells = (swells) =>
  [...swells].sort((a, b) => swellImpact(b) - swellImpact(a)).slice(0, 3);
