import { SL, BW, FS, SURFLINE, BUOYWEATHER, FISHTRACK, WAVETRAK } from '../constants';

/**
 * Format the brand key to full text Brand name
 * @param {"sl" | "bw" | "fs" | void} brand
 */
const brandToString = (brand) => {
  if (brand === SL) return SURFLINE;
  if (brand === BW) return BUOYWEATHER;
  if (brand === FS) return FISHTRACK;
  return WAVETRAK;
};

export default brandToString;
