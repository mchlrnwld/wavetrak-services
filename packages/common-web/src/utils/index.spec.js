import { expect } from 'chai';
import { capitalizeFirstLetter } from '.';

describe('capitalizeFirstLetter', () => {
  it('should return a falsy value if a falsy value is passed', () => {
    const result = capitalizeFirstLetter('');
    expect(result).to.be.equal('');
  });

  it('should capitalize the first letter', () => {
    const result = capitalizeFirstLetter('test');
    expect(result).to.be.equal('Test');
  });
});
