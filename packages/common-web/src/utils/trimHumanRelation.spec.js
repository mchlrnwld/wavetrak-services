import { expect } from 'chai';

import trimHumanRelation from './trimHumanRelation';

describe('trimHumanRelation', () => {
  it('should trim the human relation', () => {
    expect(trimHumanRelation('THIS GETS CUT – head high')).to.be.equal('head high');
    expect(trimHumanRelation('head high')).to.be.equal('head high');
  });
});
