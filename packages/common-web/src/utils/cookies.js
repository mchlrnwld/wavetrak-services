/* istanbul ignore file */
import Cookies from 'universal-cookie';

const cookies = new Cookies();

/**
 * @param {string} cookieName
 */
export const getCookie = (cookieName) => cookies.get(cookieName);

/**
 * @param {string} name
 * @param {any} value
 * @param {import('universal-cookie').CookieSetOptions} options
 */
export const setCookie = (name, value, options) => cookies.set(name, value, options);
