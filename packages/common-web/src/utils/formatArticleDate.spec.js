import sinon from 'sinon';
import { expect } from 'chai';
import formatArticleDate from './formatArticleDate';

// 1/1/2019 00:00:00
const postTimestamp = 1546300800;
const fakeCurrentTime = 1546311600;

describe('formatArticleDate', () => {
  let clock;
  const now = new Date(fakeCurrentTime * 1000);

  beforeEach(() => {
    clock = sinon.useFakeTimers(now);
  });

  afterEach(() => {
    clock.restore();
  });

  it('properly formats the date', () => {
    const date = formatArticleDate(+new Date(postTimestamp));
    expect(date).to.equal('3h');
  });
});
