import { MONTHLY, ANNUAL, GIFT, YEAR, FREE } from '../constants';

/**
 * @param {string} planId
 * @returns {MONTHLY | ANNUAL | GIFT | FREE}
 */
export const getPlanType = (planId) => {
  if (planId.includes(MONTHLY)) return MONTHLY;
  if (planId.includes(ANNUAL) || planId.includes(YEAR)) return ANNUAL;
  if (planId.includes(GIFT)) return GIFT;
  return FREE;
};
