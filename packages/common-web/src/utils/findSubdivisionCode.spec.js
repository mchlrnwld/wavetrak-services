import { expect } from 'chai';

import findSubdivisionCode from './findSubdivisionCode';

describe('findSubdivisionCode', () => {
  it('should return subdivision iso_code from the subdivisions array', () => {
    const subdivisions = [
      { geoname_id: 5855797, iso_code: 'HI', name: 'Hawaii' },
      {
        name: 'Orange County',
        geoname_id: 5379524,
      },
    ];
    const subdivisionCode = findSubdivisionCode(subdivisions);
    expect(subdivisionCode).to.equal('HI');
  });
  it('should return null if subdivisions array is empty', () => {
    const subdivisions = null;
    const subdivisionCode = findSubdivisionCode(subdivisions);
    expect(subdivisionCode).to.equal(null);
  });
});
