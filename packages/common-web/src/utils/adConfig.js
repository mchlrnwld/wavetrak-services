/* istanbul ignore file */
import getUserType from './getUserType';

const adConfigMap = {
  forecastFeedRight: {
    adUnit: '/1024858/Forecast_Feed_Right',
    adSizes: [300, 600],
    adSizeDesktop: [300, 600],
    adSizeMobile: [],
    adViewType: 'SUBREGION',
  },
  forecastFeedMobile: {
    adUnit: '/1024858/Forecast_Feed_Mobile',
    adSizes: [300, 250],
    adSizeDesktop: [],
    adSizeMobile: [300, 250],
    adViewType: 'SUBREGION',
  },
  forecastFeedMobileBottom: {
    adUnit: '/1024858/Forecast_Feed_Mobile_Bottom',
    adSizes: [300, 250],
    adSizeDesktop: [],
    adSizeMobile: [300, 250],
    adViewType: 'SUBREGION',
  },
  spotMapCard: {
    adUnit: '/1024858/Box',
    adSizes: [[300, 250]],
    adSizeDesktop: [300, 250],
    adSizeMobile: [300, 250],
    adViewType: 'MAP',
  },
  camHostAd: {
    adUnit: '/1024858/Text_Link',
    adSizes: ['fluid'],
    adSizeMappings: [],
    adViewType: 'SPOT',
  },
  spotReportPosition2: {
    adUnit: '/1024858/Position2',
    adSizes: [
      [300, 250],
      [320, 50],
    ],
    adSizeDesktop: [300, 250],
    adSizeMobile: [320, 50],
    adViewType: 'SPOT',
  },
  spotReportRight1: {
    adUnit: '/1024858/Right1',
    adSizes: [
      [300, 600],
      [300, 250],
    ],
    adSizeDesktop: [
      [300, 600],
      [300, 250],
    ],
    adSizeMobile: [300, 250],
    adViewType: 'SPOT',
  },
  spotReportBottom: {
    adUnit: '/1024858/Bottom',
    adSizes: [728, 90],
    adSizeDesktop: [728, 90],
    adSizeMobile: [],
    adViewType: 'SPOT',
  },
  spotReportWetsuit: {
    adUnit: '/1024858/Wetsuit_Recommender_Variable',
    adSizes: ['fluid'],
    adSizeMappings: [],
    adViewType: 'SPOT',
  },
  forecastBox: {
    adUnit: '/1024858/Forecast_Box',
    adSizes: [300, 250],
    adSizeDesktop: [300, 250],
    adSizeMobile: [],
    adViewType: 'SPOT',
  },
  forecastHorizontal: {
    adUnit: '/1024858/Forecast_Horizontal',
    adSizes: [
      [976, 90],
      [728, 90],
      [320, 50],
      [320, 80],
    ],
    adSizeMappings: [
      [
        [976, 200],
        [976, 90],
      ],
      [
        [728, 200],
        [728, 90],
      ],
      [
        [0, 0],
        [
          [320, 50],
          [320, 80],
        ],
      ],
    ],
    adViewType: 'SPOT',
  },
  spotNoReportOrCamTopBanner: {
    adUnit: '/1024858/SL_Cams_Top',
    adSizes: [
      [970, 90],
      [970, 250],
      [728, 90],
      [320, 50],
    ],
    adSizeDesktop: [
      [970, 90],
      [970, 250],
      [728, 90],
    ],
    adSizeMobile: [320, 50],
    adViewType: 'SPOT',
  },
  spotMiddle: {
    adUnit: '/1024858/SL_Cams_Middle',
    adSizes: [
      [970, 90],
      [970, 250],
      [728, 90],
      [300, 250],
    ],
    adSizeDesktop: [
      [970, 90],
      [970, 250],
      [728, 90],
    ],
    adSizeMobile: [300, 250],
    adViewType: 'SPOT',
  },
  spotBottom: {
    adUnit: '/1024858/SL_Cams_Bottom',
    adSizes: [
      [970, 90],
      [970, 250],
      [728, 90],
      [300, 250],
    ],
    adSizeDesktop: [
      [970, 90],
      [970, 250],
      [728, 90],
    ],
    adSizeMobile: [300, 250],
    adViewType: 'SPOT',
  },
  forecastBottom: {
    adUnit: '/1024858/Forecast_Bottom',
    adSizes: [
      [728, 90],
      [300, 250],
    ],
    adSizeDesktop: [728, 90],
    adSizeMobile: [300, 250],
    adViewType: 'SPOT',
  },
  evoPromo: {
    adUnit: '/1024858/Store_Affiliate_Variable',
    adSizes: ['fluid'],
    adSizeMappings: [],
    adViewType: 'SPOT',
  },
  sessionClipBottom: {
    adUnit: '/1024858/SL_Sessions_Bottom',
    adSizes: [
      [970, 90],
      [970, 250],
      [728, 90],
      [300, 250],
    ],
    adSizeDesktop: [
      [970, 90],
      [970, 250],
      [728, 90],
    ],
    adSizeMobile: [300, 250],
    adViewType: 'SESSIONS_CLIPS',
  },
};

const loadAdConfig = (
  adIdentifier,
  adTargets = [],
  entitlements = [],
  isUser = false,
  adViewType = null,
) => {
  const userType = getUserType(entitlements, isUser);
  const adConfigResult = {
    ...adConfigMap[adIdentifier],
    adTargets: [...adTargets, ['usertype', userType]],
    adViewType: adViewType || adConfigMap[adIdentifier].adViewType,
  };
  return adConfigResult;
};

export default loadAdConfig;
