import Cookies from 'universal-cookie';
import { getWindow } from '../getWindow';

/**
 * @function setAuthCookies
 * @param {string} accessToken
 * @param {string} refreshToken
 * @param {number} [expiresIn]
 * @param {boolean} [staySignedIn]
 * @param {string} [domainName]
 * @return {void}
 */
const setAuthCookies = (
  accessToken,
  refreshToken,
  expiresIn = null,
  staySignedIn = false,
  domainName = null,
) => {
  const cookies = new Cookies();

  const maxAge = staySignedIn ? expiresIn : 1800;
  const refreshMaxAge = staySignedIn ? 31536000 : 1800;

  const domain = domainName || getWindow()?.location?.hostname;
  cookies.set('access_token', accessToken, {
    path: '/',
    maxAge,
    expires: new Date(new Date().getTime() + maxAge * 1000),
    domain,
    secure: false,
  });

  cookies.set('refresh_token', refreshToken, {
    path: '/',
    maxAge: refreshMaxAge,
    expires: new Date(new Date().getTime() + refreshMaxAge * 1000),
    domain,
    secure: false,
  });
};

export default setAuthCookies;
