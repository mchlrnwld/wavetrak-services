import { expect } from 'chai';

import occasionalUnits from './occasionalUnits';

describe('occassionalUnits', () => {
  it('should properly format the occassionalUnits string', () => {
    const occ = occasionalUnits(5, 'FT');

    expect(occ).to.be.equal(' occ. 5 ft');
  });
  it('should return null if no occasional height', () => {
    const occ = occasionalUnits();

    expect(occ).to.be.equal(null);
  });
});
