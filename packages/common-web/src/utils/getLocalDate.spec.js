import { expect } from 'chai';
import sinon from 'sinon';
import { getLocalDateForFutureDay } from './getLocalDate';

describe('utils / getLocalDate', () => {
  let clock;
  beforeEach(() => {
    const startDay = 1631250000; // 2021-09-09 PDT
    clock = sinon.useFakeTimers(startDay * 1000);
  });

  afterEach(() => {
    clock.restore();
  });

  describe('getLocalDateForFutureDay', () => {
    it('returns future local date', () => {
      expect(getLocalDateForFutureDay(0, -7)).to.deep.equal(new Date('2021-09-09T22:00:00.000Z'));
      expect(getLocalDateForFutureDay(1, -7)).to.deep.equal(new Date('2021-09-10T22:00:00.000Z'));
      expect(getLocalDateForFutureDay(2, -7)).to.deep.equal(new Date('2021-09-11T22:00:00.000Z'));
    });

    it('getLocalDateForFutureDay returns future local date during PDT change', () => {
      const startDay = new Date('2021-11-05T00:00:00.000-07:00') / 1000;
      clock = sinon.useFakeTimers(startDay * 1000);
      expect(getLocalDateForFutureDay(0, -7)).to.deep.equal(new Date('2021-11-05T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(1, -7)).to.deep.equal(new Date('2021-11-06T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(2, -7)).to.deep.equal(new Date('2021-11-07T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(3, -7)).to.deep.equal(new Date('2021-11-08T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(4, -7)).to.deep.equal(new Date('2021-11-09T00:00:00.000Z'));
    });

    it('getLocalDateForFutureDay returns future local date during PST change', () => {
      const startDay = new Date('2021-03-11T00:00:00.000-08:00') / 1000;
      clock = sinon.useFakeTimers(startDay * 1000);
      expect(getLocalDateForFutureDay(0, -8)).to.deep.equal(new Date('2021-03-11T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(1, -8)).to.deep.equal(new Date('2021-03-12T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(2, -8)).to.deep.equal(new Date('2021-03-13T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(3, -8)).to.deep.equal(new Date('2021-03-14T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(4, -8)).to.deep.equal(new Date('2021-03-15T00:00:00.000Z'));
    });

    it('returns future local date during AEDT change', () => {
      const startDay = new Date('2021-04-01T00:00:00.000+11:00') / 1000;
      clock = sinon.useFakeTimers(startDay * 1000);
      expect(getLocalDateForFutureDay(0, 11)).to.deep.equal(new Date('2021-04-01T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(1, 11)).to.deep.equal(new Date('2021-04-02T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(2, 11)).to.deep.equal(new Date('2021-04-03T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(3, 11)).to.deep.equal(new Date('2021-04-04T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(4, 11)).to.deep.equal(new Date('2021-04-05T00:00:00.000Z'));
    });

    it('returns future local date during AEST change', () => {
      const startDay = new Date('2021-09-30T00:00:00.000+10:00') / 1000;
      clock = sinon.useFakeTimers(startDay * 1000);
      expect(getLocalDateForFutureDay(0, 10)).to.deep.equal(new Date('2021-09-30T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(1, 10)).to.deep.equal(new Date('2021-10-01T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(2, 10)).to.deep.equal(new Date('2021-10-02T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(3, 10)).to.deep.equal(new Date('2021-10-03T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(4, 10)).to.deep.equal(new Date('2021-10-04T00:00:00.000Z'));
    });

    it('returns future local date during CLDT change', () => {
      const startDay = new Date('2021-04-01T00:00:00.000-02:00') / 1000;
      clock = sinon.useFakeTimers(startDay * 1000);
      expect(getLocalDateForFutureDay(0, -2)).to.deep.equal(new Date('2021-04-01T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(1, -2)).to.deep.equal(new Date('2021-04-02T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(2, -2)).to.deep.equal(new Date('2021-04-03T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(3, -2)).to.deep.equal(new Date('2021-04-04T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(4, -2)).to.deep.equal(new Date('2021-04-05T00:00:00.000Z'));
    });

    it('returns future local date during CLST change', () => {
      const startDay = new Date('2021-09-01T00:00:00.000-03:00') / 1000;
      clock = sinon.useFakeTimers(startDay * 1000);
      expect(getLocalDateForFutureDay(0, -3)).to.deep.equal(new Date('2021-09-01T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(1, -3)).to.deep.equal(new Date('2021-09-02T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(2, -3)).to.deep.equal(new Date('2021-09-03T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(3, -3)).to.deep.equal(new Date('2021-09-04T00:00:00.000Z'));
      expect(getLocalDateForFutureDay(4, -3)).to.deep.equal(new Date('2021-09-05T00:00:00.000Z'));
    });
  });
});
