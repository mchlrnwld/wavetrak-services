// For each href, strip out protocol
const resolveBreadcrumbs = (breadcrumbs) =>
  breadcrumbs.map((entry) => {
    const url = entry.href;
    let path = url;
    if (entry.href.indexOf('http') > -1) {
      const matches = url.match(/^https?:\/\/([^/?#]+)(?:[/?#]|$)/i);
      path = `/${path.replace(matches[0], '')}`;
    }
    return {
      name: entry.name,
      href: path,
    };
  });

export default resolveBreadcrumbs;
