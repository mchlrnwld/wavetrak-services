import { expect } from 'chai';
import sinon from 'sinon';

import formatDateToMMDDYY from './formatDateToMMDDYY';

describe('formatDateToMMDDYY', () => {
  // Tue Jan 01 2019 03:00:00 UTC
  const fakeCurrentTime = 1546311610000;

  let clock;
  const now = new Date(fakeCurrentTime);

  beforeEach(() => {
    clock = sinon.useFakeTimers(now);
  });

  afterEach(() => {
    clock.restore();
  });

  it('should properly format the date', () => {
    const date = new Date();
    expect(formatDateToMMDDYY(date)).to.be.equal('01-01-19');
  });
});
