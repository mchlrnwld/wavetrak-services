import { expect } from 'chai';

import { conditionEnumToText, conditionClassModifier } from './conditions';

describe('conditions', () => {
  it('Properly replaces the conditionsClassModifier string', () => {
    const modified = conditionClassModifier('FAIR_TO_GOOD');
    expect(modified).to.be.equal('fair-to-good');
  });
  it('should allow lola if missing condition and allowLola = true', () => {
    const replacedString = conditionClassModifier(null, true);
    expect(replacedString).to.be.equal('lola');
  });

  it('Properly replaces the condition enum to text', () => {
    const replacedString = conditionEnumToText('FAIR_TO_GOOD', '-');
    expect(replacedString).to.be.equal('FAIR-TO-GOOD');
  });
  it('should ignore if no condition is passed', () => {
    const replacedString = conditionEnumToText();
    expect(replacedString).to.be.equal(undefined);
  });
});
