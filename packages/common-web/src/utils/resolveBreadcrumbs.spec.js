import { expect } from 'chai';

import resolveBreadcrumbs from './resolveBreadcrumbs';

describe('resolveBreadcrumbs', () => {
  it('should properly strip the protocol from a breadcrumb', () => {
    const breadcrumbs = resolveBreadcrumbs([{ href: 'http://www.surfline.com', name: 'Surfline' }]);

    expect(breadcrumbs).to.deep.equal([
      {
        name: 'Surfline',
        href: '/',
      },
    ]);
  });

  it('should create breadcrumbs', () => {
    const breadcrumbs = resolveBreadcrumbs([{ href: '/home', name: 'Surfline' }]);

    expect(breadcrumbs).to.deep.equal([
      {
        name: 'Surfline',
        href: '/home',
      },
    ]);
  });
});
