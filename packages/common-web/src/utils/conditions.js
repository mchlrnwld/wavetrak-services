export const conditionClassModifier = (condition, allowLola = true) => {
  if (condition) {
    return condition.toLowerCase().replace(/_/g, '-');
  }
  return allowLola ? 'lola' : false;
};

export const conditionEnumToText = (condition, newSubtr) =>
  condition && condition.replace(/_/g, newSubtr || ' ');
