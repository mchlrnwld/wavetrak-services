import { expect } from 'chai';

import brandToString from './brandToString';

describe('brandToString', () => {
  it('should return the surfline brand string', () => {
    expect(brandToString('sl')).to.be.equal('Surfline');
  });
  it('should return the buoyweahter brand string', () => {
    expect(brandToString('bw')).to.be.equal('Buoyweather');
  });
  it('should return the surfline brand string', () => {
    expect(brandToString('fs')).to.be.equal('FishTrack');
  });
  it('should return the surfline brand string', () => {
    expect(brandToString()).to.be.equal('Wavetrak');
  });
});
