import round from 'lodash/round';
import { FEET, METERS } from '../constants';

// eslint-disable-next-line import/prefer-default-export
export const roundSurfHeight = (height, units) => {
  switch (units) {
    case FEET:
      return round(height, 0);
    case METERS:
      return round(height, 1);
    default:
      return height;
  }
};
