import { PROXY_URL } from '../constants';

/* istanbul ignore file */
/**
 * @returns {string}
 */
const getProxyUrl = () => {
  // We want to use `APP_ENV` going forward since NODE_ENV is used by webpack
  // and libraries. Here we'll default to APP_ENV if it exists and fallback
  // to NODE_ENV
  const environment = process.env.APP_ENV || process.env.NODE_ENV;

  return PROXY_URL[environment || 'development'];
};

export default getProxyUrl;
