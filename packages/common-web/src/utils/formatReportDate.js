import { format } from 'date-fns';
import getLocalDate from './getLocalDate';

/**
 * Format the date of a posted report
 * @param {number|string} timestamp
 * @param {number|string} utcOffset
 * @param {string} abbrTimezone
 * @param {boolean} extended
 * @param {string} dateFormat
 */
export const postedDate = (
  timestamp,
  utcOffset,
  abbrTimezone,
  // eslint-disable-next-line default-param-last
  extendedDate = false,
  dateFormat,
) => {
  const localDate = getLocalDate(timestamp, utcOffset);
  const extendedDateFormat =
    dateFormat === 'DMY' ? "iii, d MMMM 'at' h:mmaaaaa'm'" : "iii, MMMM d 'at' h:mmaaaaa'm'";
  const formatString = extendedDate ? extendedDateFormat : "iii 'at' h:mmaaaaa'm'";
  return `${format(localDate, formatString)} ${abbrTimezone}`;
};

/**
 * Format the next date of report update
 * @param {number|string} nextUpdate
 * @param {number|string} utcOffset
 * @param {string} abbrTimezone
 */
export const nextUpdateDate = (nextUpdate, utcOffset, abbrTimezone) =>
  nextUpdate
    ? `Next update ${format(
        getLocalDate(nextUpdate, utcOffset),
        "iii 'by' haaaaa'm'",
      )} ${abbrTimezone}`
    : null;
