import { expect } from 'chai';

import { getPlanType } from './subscriptions';
import { MONTHLY, ANNUAL, GIFT, FREE } from '../constants';

describe('subscriptions', () => {
  describe('getPlanType', () => {
    it('should return the proper plan type', () => {
      const monthly = getPlanType('sl_monthly');
      const annual = getPlanType('sl_annual_v2');
      const annual2 = getPlanType('sl_yearly');
      const gift = getPlanType('sl_gift');
      const free = getPlanType('sl_freeloader_:)');

      expect(monthly).to.be.equal(MONTHLY);
      expect(annual).to.be.equal(ANNUAL);
      expect(annual2).to.be.equal(ANNUAL);
      expect(gift).to.be.equal(GIFT);
      expect(free).to.be.equal(FREE);
    });
  });
});
