import { expect } from 'chai';
import sinon from 'sinon';

import daysFromNowUntil from './daysFromNowUntil';

describe('daysFromNowUntil', () => {
  // Tue Jan 01 2019 03:00:00 UTC
  const fakeCurrentTime = 1546311600000;

  // Wed Jan 02 2019 04:00:00 UTC
  const fakeFutureTime = 1546401600000;

  let clock;
  const now = new Date(fakeCurrentTime);

  beforeEach(() => {
    clock = sinon.useFakeTimers(now);
  });

  afterEach(() => {
    clock.restore();
  });

  it('should return the number of days between now and the given date', () => {
    const futureDate = new Date(fakeFutureTime);
    expect(daysFromNowUntil(futureDate)).to.be.equal(1);
  });
});
