export default (timestamp, utcOffset) => timestamp + utcOffset * 60 * 60 * 1000;
