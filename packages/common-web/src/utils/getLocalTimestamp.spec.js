import { expect } from 'chai';

import getLocalTimestamp from './getLocalTimestamp';

describe('getLocalTimestamp', () => {
  it('should add the utc offset to the timestamp', () => {
    const timestamp = getLocalTimestamp(1546311600000, 8);
    expect(timestamp).to.be.equal(1546340400000);
  });
});
