import { formatDistanceStrict } from 'date-fns';

const formatArticleDate = (date) => {
  // example: converts 1 day => 1d
  let cardDate = '';
  const formattedDate = formatDistanceStrict(date * 1000, new Date()).split(' ');
  if (formattedDate && formattedDate.length && formattedDate.length > 1) {
    cardDate = `${formattedDate[0]}${formattedDate[1][0]}`;
  }
  return cardDate;
};

export default formatArticleDate;
