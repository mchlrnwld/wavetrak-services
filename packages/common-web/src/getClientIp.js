/**
 * @param {import('express').Request} req
 * @returns {string | undefined | null}
 */
const getClientIp = (req) => {
  let clientIp;
  if (req && req.headers) {
    let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    // in dev, loopback is sometimes an ipv6 address... downstream services
    // don't handle that well.
    if (ip === '::1') ip = '8.8.8.8';

    clientIp = ip ? ip.split(',')[0] : null;
  }
  return clientIp;
};

export default getClientIp;
