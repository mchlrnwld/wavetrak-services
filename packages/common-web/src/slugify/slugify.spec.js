import { expect } from 'chai';
import slugify from './slugify';

describe('slugify', () => {
  it('returns slug from string', () => {
    expect(slugify('HB Pier, Southside')).to.equal('hb-pier-southside');
    expect(slugify('8th Street, Ocean City, MD')).to.equal('8th-street-ocean-city-md');
  });
});
