const slugify = (str) =>
  str
    .toLowerCase()
    .replace(/[^a-zA-Z0-9]/g, '-')
    .replace(/(-){2,}/g, '-');

export default slugify;
