/* istanbul ignore file */

/**
 * @typedef {(key: string, reducerToInject: import('redux').Reducer) => void} injectReducer
 */

/**
 * @template S
 * @typedef {import('redux').Store<S & import('./selectors').BackplaneState> & {
 *  injectReducer: injectReducer,
 *  asyncReducers: AsyncReducers
 * }} WavetrakStore
 */

/**
 * @description Create a reducer injecter function. This function allows us to inject reducers into
 * the store after it has already been created (eg: Backplane injects a reducer).
 * @typedef {import('redux').ReducersMapObject} AsyncReducers
 * @param {import('redux').Store} store
 * @param {(asyncReducers: AsyncReducers) => import('redux').Reducer} reducer
 * @returns {injectReducer}
 */
export const createReducerInjecter = (store, reducer) => {
  /** @type {injectReducer} */
  const injectReducer = (key, reducerToInject) => {
    if (!store.asyncReducers) {
      // eslint-disable-next-line no-param-reassign
      store.asyncReducers = {};
    }
    // eslint-disable-next-line no-param-reassign
    store.asyncReducers[key] = reducerToInject;
    store.replaceReducer(reducer(store.asyncReducers));
  };

  // eslint-disable-next-line no-param-reassign
  store.injectReducer = injectReducer;
  return injectReducer;
};
