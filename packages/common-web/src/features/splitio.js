/* eslint-disable no-underscore-dangle */
import { SplitFactory } from '@splitsoftware/splitio';
import Cookies from 'universal-cookie';

import { getCanUseDOM } from '../canUseDOM';
import { getWindow } from '../getWindow';
import { SL_METERED } from '../constants';
import { MISSING_ANONYMOUS_ID, MISSING_AUTH_KEY, MISSING_USER, SERVER_SIDE_ERROR } from './errors';

const READY_TIMEOUT = 1000; // 1 second in milliseconds
const cookies = new Cookies();
const hasDebugCookie = cookies.get('wavetrak-debug') === 'true';
const doDebugLog = process.env.NODE_ENV !== 'production' || hasDebugCookie;

/**
 * @typedef {import('../selectors/user').UserState} User
 */

/**
 * @typedef {SplitIO.Treatment} Treatment
 */

/**
 * @typedef {SplitIO.TreatmentWithConfig} TreatmentWithConfig
 */

/**
 * @typedef {SplitIO.IClient} SplitClient
 */

/**
 * @typedef {Pick<Partial<User>, 'countryCode' | 'details' | 'entitlements'>} UserInfo

/**
 * @typedef {object} WavetrakIdentity
 * @property {string} id
 * @property {string} anonymousId
 * @property {User['details']['_id']?} userId
 * @property {User['details']['email']?} email
 * @property {string?} isoCountryCode
 * @property {'anonymous' | 'logged_in'} type
 * @property {User['entitlements']} entitlements
 */

/**
 * @param {string} message
 */
const logAnalyticsDebug = (message) => {
  if (doDebugLog) {
    // eslint-disable-next-line no-console
    console.log(message);
  }
};

if (hasDebugCookie) {
  const win = getWindow();
  if (win?.analytics.debug) {
    logAnalyticsDebug('Setting analytics debug');
    win.analytics.debug(true);
  }
}

/**
 * @param {string} message
 */
const logSegmentWarning = (message) => {
  /* istanbul ignore next */
  if (doDebugLog) {
    // eslint-disable-next-line no-console
    console.warn(message);
  }
};

/**
 * @param {string} message
 */
const logSegmentError = (message) => {
  /* istanbul ignore next */
  if (doDebugLog) {
    // eslint-disable-next-line no-console
    console.error(message);
  }
};

/**
 * Sets optional callback for analytics.ready
 * @param {Function} cb
 * @returns {(success: boolean) => void | null}
 */
export const setWavetrakAnalyticsReadyCB = (cb) => {
  if (getCanUseDOM()) {
    getWindow().wavetrakAnalyticsReadyCB = cb;
  }
};

/**
 * Gets optional callback for analytics.ready
 * @returns {(success: boolean) => void | null}
 */
export const getWavetrakAnalyticsReadyCB = () => {
  if (getCanUseDOM()) {
    return getWindow().wavetrakAnalyticsReadyCB;
  }
  return null;
};

/**
 * @param {UserInfo} [user]
 */
export const setupIdentity = (user) => {
  const anonymousId = getWindow()?.analytics?.user?.()?.anonymousId();

  // If analytics is blocked, we just short circuit and abandon setting up the identity
  // and the feature framework
  if (!anonymousId) return null;

  const { _id: userId, email } = user?.details || {};
  const entitlements = user?.entitlements || [];
  return {
    id: anonymousId,
    userId,
    anonymousId,
    entitlements,
    rl_forced: entitlements.includes(SL_METERED),
    email,
    type: userId ? 'logged_in' : 'anonymous',
    ...(user?.countryCode && { isoCountryCode: user.countryCode }),
    ...(user?.subdivisionCode && { subdivisionCode: user.subdivisionCode }),
  };
};

/**
 * @description When loading the Split SDK we rely on the `analytics.js`
 * from segment in order to retrieve a UUID (anonymous ID). To ensure that
 * the SDK is not loaded before analytics.js` we can rely on `analytics.ready`
 * and timeout this wait after 2 seconds if analytics has not loaded or is
 * blocked.
 * @returns {Promise<void>}
 */
const waitForAnalytics = () =>
  new Promise((resolve) => {
    logAnalyticsDebug('Waiting for analytics');
    // If this is a new user and analytics is not blocked, we will
    // wait for analytics to be ready so that we can be sure the cookie
    // is set
    const win = getWindow();

    if (win?.analytics?.ready) {
      // Since analytics request could be blocked or fail, `analytics.ready` may not fire
      // so we'll add a timeout here and then resolve
      const analyticsTimeout = setTimeout(() => {
        logAnalyticsDebug('Analytics ready timeout');
        resolve();
      }, READY_TIMEOUT);
      win?.analytics.ready(() => {
        logAnalyticsDebug('Analytics ready callback called');
        clearTimeout(analyticsTimeout);
        getWavetrakAnalyticsReadyCB()?.(true);
        resolve();
      });
    }
    resolve();
  });

/**
 * @param {ReturnType<CreateSplitFilter>} splitFilters
 */
export const setWavetrakSplitFilters = (splitFilters) => {
  if (getCanUseDOM()) {
    getWindow().wavetrakSplitFilters = splitFilters;
  }
};

/**
 * @returns {ReturnType<CreateSplitFilter>}
 */
export const getWavetrakSplitFilters = () => {
  if (getCanUseDOM()) {
    return getWindow().wavetrakSplitFilters || [];
  }
  return [];
};

/**
 * @param {WavetrakIdentity} identity
 */
export const setWavetrakIdentity = (identity) => {
  if (getCanUseDOM()) {
    getWindow().__WAVETRAK_IDENTITY__ = identity;
  }
};

/**
 * @description When a user logs out, we need to clear
 * the existing identity on the client. This function
 * will clear the `__WAVETRAK_IDENTITY__` key on the window
 */
export const clearWavetrakIdentity = () => {
  if (getCanUseDOM()) {
    delete getWindow().__WAVETRAK_IDENTITY__;
  }
};

/**
 * @description A helper function to retrieve the identity
 * object that is attached to the window. If this is called
 * on the server-side we just return `null`.
 * @returns {WavetrakIdentity?}
 */
export const getWavetrakIdentity = () => {
  if (getCanUseDOM()) {
    return getWindow().__WAVETRAK_IDENTITY__;
  }

  return null;
};

/**
 * @returns {(success: boolean) => void | null}
 */
export const getWavetrakFeatureFrameworkCB = () => {
  if (getCanUseDOM()) {
    return getWindow().wavetrakFeatureFrameworkCB;
  }
  return null;
};

/**
 * @param {(success: boolean) => void} cb
 */
export const setWavetrakFeatureFrameworkCB = (cb) => {
  if (getCanUseDOM()) {
    getWindow().wavetrakFeatureFrameworkCB = cb;
  }
};

export const clearWavetrakFeatureFrameworkCB = () => {
  if (getCanUseDOM()) {
    delete getWindow().wavetrakFeatureFrameworkCB;
  }
};

/**
 * @description A helper function to retrieve the feature framework
 * object that is attached to the window.
 * @returns {SplitClient?}
 */
export const getWavetrakFeatureFramework = () => {
  if (getCanUseDOM()) {
    return getWindow().wavetrakFeatureFramework;
  }

  return null;
};

/**
 * @description Set the Split SDK onto the window in case it needs to be accessed by
 * consuming app
 * @param {SplitClient} featureFramework
 */
export const setWavetrakFeatureFramework = (featureFramework) => {
  if (getCanUseDOM()) {
    getWindow().wavetrakFeatureFramework = featureFramework;
  }
};

/**
 * @typedef {object} SetupOptions
 * @property {string} authorizationKey
 * @property {UserInfo} user
 * @property {string[]} [splitFilters]
 * @property {number} [readyTimeout]
 */

/**
 * @param {SetupOptions} options
 * @returns {Promise<SplitClient>}
 */
export const setupFeatureFramework = async ({
  authorizationKey,
  user,
  splitFilters = [],
  readyTimeout = 5,
}) => {
  /* istanbul ignore next */
  if (!getCanUseDOM()) {
    throw new Error(SERVER_SIDE_ERROR);
  }

  if (getWindow()?.wavetrakFeatureFramework) {
    return getWindow().wavetrakFeatureFramework;
  }

  if (!authorizationKey) {
    throw new Error(MISSING_AUTH_KEY);
  }

  if (!user) {
    throw new Error(MISSING_USER);
  }

  await waitForAnalytics();
  return new Promise((resolve, reject) => {
    const identity = setupIdentity(user);
    setWavetrakIdentity(identity);

    if (!identity?.id) {
      getWindow()?.wavetrakFeatureFrameworkCB?.(false);
      logSegmentError(MISSING_ANONYMOUS_ID);
      reject(new Error(MISSING_ANONYMOUS_ID));
    } else {
      if (splitFilters.length) {
        logSegmentWarning(
          'Using `splitFilter` config option. Ensure that all treatments you ' +
            'intend to use are passed into the `splitFilter` array',
        );
      }

      /** @type {SplitIO.ISDK} */
      const sdk = SplitFactory({
        core: { authorizationKey, key: identity.id },
        startup: {
          readyTimeout,
          retriesOnFailureBeforeReady: 2,
          eventFirstPushWindow: 5,
        },
        scheduler: {
          impressionsRefreshRate: 5,
        },
        sync: {
          splitFilters: splitFilters.length ? [{ type: 'byName', values: splitFilters }] : [],
        },
      });

      const featureFramework = sdk.client();

      setWavetrakFeatureFramework(featureFramework);
      const wavetrakFeatureFrameworkCB = getWavetrakFeatureFrameworkCB();

      featureFramework.on(featureFramework.Event.SDK_READY, () => {
        wavetrakFeatureFrameworkCB?.(true);
        resolve(featureFramework);
      });

      featureFramework.on(featureFramework.Event.SDK_READY_TIMED_OUT, () => {
        // We still want to call the ready callback so that the client can proceed
        // Calls to `getTreatment` will work, but will return control until the SDK
        // is ready.
        wavetrakFeatureFrameworkCB?.(false);
        reject(new Error('Feature Framework Setup Timed Out'));
      });
    }
  });
};

const doControlTreatmentError = () => {
  const featureFramework = getWavetrakFeatureFramework();
  logSegmentError(
    `${
      !featureFramework ? 'Feature framework' : 'Identity'
    } is not set up. Returning "control" treatment`,
  );
};

/**
 * @param {string} treatment
 * @returns {Treatment | "control" | null}
 */
export const getTreatment = (treatment) => {
  /* istanbul ignore next */
  if (!getCanUseDOM()) {
    return null;
  }

  const featureFramework = getWavetrakFeatureFramework();
  const identity = getWavetrakIdentity();
  if (!featureFramework || !identity) {
    doControlTreatmentError();
    return 'control';
  }

  return featureFramework.getTreatment(treatment, identity);
};

/**
 * https://help.split.io/hc/en-us/articles/360020037072-Split-evaluator#clientget-treatment-with-config
 * @param {string} treatment
 * @param {import('../redux').Identity} identity
 * @returns {TreatmentWithConfig}
 */
export const getTreatmentWithConfig = (treatment) => {
  /* istanbul ignore next */
  if (!getCanUseDOM()) {
    return null;
  }

  const featureFramework = getWavetrakFeatureFramework();
  const identity = getWavetrakIdentity();
  if (!featureFramework || !identity) {
    doControlTreatmentError();

    // Return the config as an string that can be passed to JSON.parse
    return { treatment: 'control', config: '""' };
  }

  return featureFramework.getTreatmentWithConfig(treatment, identity);
};

/**
 * @param  {...{ [key: string]: string }} treatmentMaps
 * @returns {string[]}
 */
export const createSplitFilter = (...treatmentMaps) => treatmentMaps.map(Object.values).flat();
