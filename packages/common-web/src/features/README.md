# Feature Flags with Split.io

Surfline uses split.io to power feature flags. This folder provides utility functions for integrating
split.io feature flags into Surfline web applications.

These utilities have been written with the following requirements in mind

1. The feature flag cannot be resolved on the server. We have to support loading states to prevent
   content flashes when hydrating and updating the client-side react code.
2. User ids (anonymous or not) need to sync with segment.
3. User attributes must be resolved by Surfline and should be consistent across applications
   implementing feature flags

## Some Best Practices

> Savvy teams view the Feature Toggles in their codebase as inventory which comes with a carrying cost
> and seek to keep that inventory as low as possible. In order to keep the number of feature toggles
> manageable a team must be proactive in removing feature toggles that are no longer needed.

https://martinfowler.com/articles/feature-toggles.html

## Surfline Implementation

- `setupFeatureFramework` uses the identity property defined on the `window` as `__WAVETRAK_IDENTITY__` in
  order to setup the Split Javascript SDK and adds it to the window object (so that it can be shared
  across multiple client entry points or federated modules).
- `@surfline/quiver-react` will export a set of hooks/components/helpers that will get the value of `getTreatment`
  and cache its value into state

## Syncing user data with Split.io and Segment

### User IDs

Data is synced back into segment from split. We no longer allow server side instantiation of Split
in order to prevent issues with duplicate Anonymous IDs and erroneous user fall off rates.
Surfline apps identify anonymous ids on the client and then create a `__WAVETRAK_IDENTITY__` object
on the window that will store this identity for use by Split.

The `web-common/segment#identify` function can be used to generate a snippet that identifies the
generated user id with segment. We must defer the creation of the `ajs_anonymous_id` to Segment or else
we open ourselves up to inconsistencies in the data.

### User Attributes

Split.io does not store user attributes on their servers. As a result, attributes must be fetched by
consuming applications and then passed into feature flag requests. This is why we use the identity
object on the window in order to send it with the `getTreatment` calls.

### Split Filters

In order to ensure that the SDK is able to load as fast as possible we want to use the `splitFilters`
configuration option on the SDK. The `setupFeatureFramework` function takes an optional `splitFilters`
argument. If passed this argument should be an array of treatment names to load into the SDK. The simplest
way to do this is to store all _used_ treatment names in a `/common/treatments.js` file, and then pass them
into `setupFeatureFramework` as detailed in the below example.

## Example setups

```javascript
// /common/treatments.js
export const SL_TEST_TREATMENT = 'sl_test_treatment';
export const SL_TEST_TREATMENT_1 = 'sl_test_treatment_1';
```

```javascript
// /common/treatmentsWithConfig.js
export const SL_TEST_TREATMENT_WITH_CONFIG = 'sl_test_treatment_with_config';
```

```JSX
// backplane/entry.jsx
// Backplane entrypoint file
import { setupFeatureFramework, createSplitFilter, getUser } from '@surfline/web-common';
import * as treatments from '../common/treatments'
import * as treatmentsWithConfig from '../common/treatmentsWithConfig'

window.wavetrakFeatureFrameworkCB = someCallbackYouWantExecuted;
const store = createStore(window.__PRELOADED_REDUX_STATE__)

const hydrate = () => {
  return ReactDOM.hydrate(<HTML/>, "#root");
}

const mount = async () => {
  try {
    const setup = await setupFeatureFramework({
      authorizationKey: config.appKeys.splitio,
      splitFilters: createSplitFilter(treatments, treatmentsWithConfig)
      // Note: this may be different depending on if the app is using the backplane store or not
      user: getUser(store.getState())
    });
  } finally {
    hydrate()
  }
}

mount()
```

```JSX
// kbyg/Component.js
import { useTreatment } from '@surfline/quiver-react';

const Component = () => {
  const { loading, treatment } = useTreatment('sl_web_test');
  const showWebTest = treatment === 'on';

  if (loading) {
    return (
      <Spinner />
    )
  }

  return (
    <h1>{showWebTest ? 'on' : 'off'}</h1>
  )
}

export default Component;
```

```JSX
import { Treatment } from '@surfline/quiver-react';

const A = () => (
  <Treatment treatmentName="sl_test">
    <B />
  </Treatment>
)

const B = ({ treatment }) => (
  <div className="b">
    ({treatment === "on"
        ? <div>on</div>
        : <div>off</div>})
  </div>
)

export { A, B };
```

```JSX
import { getTreatment } from '@surfline/web-common';

class A extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      treatment: null,
    }
  }

  componentDidMount() {
    const treatment = getTreatment('sl_test');
    this.setState({ treatment })
  }

  render() {
    const { treatment } = this.state;
    const isLoading = !treatment;

    if (isLoading) {
      return (<Spinner />)
    }

    return (
      <div>{treatment}</div>
    )
  }
}

export A;
```
