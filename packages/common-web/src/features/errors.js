export const MISSING_ANONYMOUS_ID =
  'Unable to setup feature framework without anonymous ID. Either analytics was blocked or failed to load.';
export const SERVER_SIDE_ERROR = 'Cannot instantiate split on the server-side.';
export const MISSING_AUTH_KEY = 'Cannot instantiate split without an authorization key.';
export const MISSING_USER = 'You must pass a user object so that an identity can be created.';
