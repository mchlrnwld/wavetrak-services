/* eslint-disable no-underscore-dangle */
import { expect } from 'chai';
import sinon from 'sinon';
import * as split from '@splitsoftware/splitio';
import {
  clearWavetrakFeatureFrameworkCB,
  clearWavetrakIdentity,
  createSplitFilter,
  getTreatment,
  getTreatmentWithConfig,
  getWavetrakAnalyticsReadyCB,
  getWavetrakFeatureFramework,
  getWavetrakFeatureFrameworkCB,
  getWavetrakIdentity,
  getWavetrakSplitFilters,
  setupFeatureFramework,
  setupIdentity,
  setWavetrakAnalyticsReadyCB,
  setWavetrakFeatureFramework,
  setWavetrakFeatureFrameworkCB,
  setWavetrakIdentity,
  setWavetrakSplitFilters,
} from './splitio';
import { getWindow } from '../getWindow';
import fixture, { meteredUser } from '../selectors/fixtures/state';
import { MISSING_ANONYMOUS_ID, MISSING_AUTH_KEY, MISSING_USER, SERVER_SIDE_ERROR } from './errors';

describe('Split IO', () => {
  /** @type {sinon.SinonStub} */
  let splitFactoryStub;

  const anonymousIdStub = sinon.stub();
  const analyticsUserStub = sinon.stub();

  beforeEach(() => {
    splitFactoryStub = sinon.stub(split, 'SplitFactory');
    global.window.document = { createElement: () => {} };
    const win = getWindow();

    delete win.__WAVETRAK_IDENTITY__;
    delete win.wavetrakFeatureFramework;
    delete win.wavetrakFeatureFrameworkCB;
    delete win.wavetrakSplitFilters;
    delete win.analytics;
  });

  afterEach(() => {
    delete global.window.document;
    splitFactoryStub.restore();
    anonymousIdStub.reset();
    analyticsUserStub.reset();
  });

  describe('setupIdentity', () => {
    it('should not return an identity if the anonymous ID cookie does not exist', () => {
      expect(setupIdentity({})).to.equal(null);
    });

    it('should return an identity for a user', () => {
      const { user } = fixture.backplane;

      analyticsUserStub.returns({ anonymousId: anonymousIdStub });
      anonymousIdStub.returns('123');
      getWindow().analytics = { user: analyticsUserStub };

      expect(setupIdentity(user)).to.deep.equal({
        id: '123',
        userId: user.details._id,
        anonymousId: '123',
        entitlements: user.entitlements,
        rl_forced: false,
        email: user.details.email,
        type: 'logged_in',
        isoCountryCode: 'US',
        subdivisionCode: 'CA',
      });
    });

    it('should set rl_forced for metered users', () => {
      const { user } = meteredUser.backplane;

      analyticsUserStub.returns({ anonymousId: anonymousIdStub });
      anonymousIdStub.returns('123');
      getWindow().analytics = { user: analyticsUserStub };

      expect(setupIdentity(user)).to.deep.equal({
        id: '123',
        userId: user.details._id,
        anonymousId: '123',
        entitlements: user.entitlements,
        rl_forced: true,
        email: user.details.email,
        type: 'logged_in',
        isoCountryCode: 'US',
        subdivisionCode: 'CA',
      });
    });

    it('should return an identity for a user without a country code', () => {
      const user = { ...fixture.backplane.user };

      delete user.countryCode;

      analyticsUserStub.returns({ anonymousId: anonymousIdStub });
      anonymousIdStub.returns('123');
      getWindow().analytics = { user: analyticsUserStub };

      expect(setupIdentity(user)).to.deep.equal({
        id: '123',
        userId: user.details._id,
        anonymousId: '123',
        entitlements: user.entitlements,
        rl_forced: false,
        email: user.details.email,
        type: 'logged_in',
        subdivisionCode: 'CA',
      });
    });

    it('should return an identity for a user without a subdivision code', () => {
      const user = { ...fixture.backplane.user };

      delete user.subdivisionCode;

      analyticsUserStub.returns({ anonymousId: anonymousIdStub });
      anonymousIdStub.returns('123');
      getWindow().analytics = { user: analyticsUserStub };

      expect(setupIdentity(user)).to.deep.equal({
        id: '123',
        userId: user.details._id,
        anonymousId: '123',
        entitlements: user.entitlements,
        rl_forced: false,
        email: user.details.email,
        type: 'logged_in',
        isoCountryCode: 'US',
      });
    });

    it('should return an identity for an anonymous user', () => {
      analyticsUserStub.returns({ anonymousId: anonymousIdStub });
      anonymousIdStub.returns('123');
      getWindow().analytics = { user: analyticsUserStub };

      expect(setupIdentity({ details: {}, entitlements: [] })).to.deep.equal({
        id: '123',
        userId: undefined,
        anonymousId: '123',
        entitlements: [],
        rl_forced: false,
        email: undefined,
        type: 'anonymous',
      });
    });
  });

  describe('Wavetrak Identity', () => {
    it('should set and clear the wavetrak identity from the window', () => {
      analyticsUserStub.returns({ anonymousId: anonymousIdStub });
      anonymousIdStub.returns('123');
      getWindow().analytics = { user: analyticsUserStub };
      setWavetrakIdentity(setupIdentity());

      expect(getWavetrakIdentity()).to.deep.equal({
        id: '123',
        userId: undefined,
        anonymousId: '123',
        entitlements: [],
        rl_forced: false,
        email: undefined,
        type: 'anonymous',
      });

      clearWavetrakIdentity();

      expect(getWavetrakIdentity()).to.be.equal(undefined);
    });
  });

  describe('Wavetrak Feature Framework', () => {
    it('should setup a feature framework instance when the SDK succeeds', async () => {
      const wavetrakCBStub = sinon.stub();
      const readyStub = sinon.stub().callsArg(0);
      const featureFrameworkStub = {
        on: sinon.stub(),
        Event: { SDK_READY: 'sdk_ready', SDK_READY_TIMED_OUT: 'sdk_timed_out' },
      };

      const clientStub = sinon.stub().returns(featureFrameworkStub);

      getWindow().analytics = {
        ready: readyStub,
      };

      analyticsUserStub.returns({ anonymousId: anonymousIdStub });
      anonymousIdStub.returns('123');
      getWindow().analytics = { user: analyticsUserStub };

      setWavetrakFeatureFrameworkCB(wavetrakCBStub);

      splitFactoryStub.returns({
        client: clientStub,
      });

      setupFeatureFramework({
        authorizationKey: 'authkey',
        splitFilters: ['filter_name'],
        user: { details: null, entitlements: [], countryCode: 'US', subdivisionCode: 'CA' },
        readyTimeout: 10,
      });

      await new Promise((resolve) => {
        setTimeout(resolve, 0);
      });
      featureFrameworkStub.on.getCall(0).callArg(1);

      expect(getWavetrakFeatureFramework()).to.deep.equal(featureFrameworkStub);
      expect(splitFactoryStub).to.have.been.calledOnceWithExactly({
        core: { authorizationKey: 'authkey', key: '123' },
        startup: {
          readyTimeout: 10,
          retriesOnFailureBeforeReady: 2,
          eventFirstPushWindow: 5,
        },
        scheduler: {
          impressionsRefreshRate: 5,
        },
        sync: {
          splitFilters: [{ type: 'byName', values: ['filter_name'] }],
        },
      });

      expect(clientStub).to.have.been.calledOnce();
      expect(featureFrameworkStub.on).to.have.been.calledTwice();
      expect(wavetrakCBStub).to.have.been.calledOnceWithExactly(true);
    });

    it('should setup a feature framework instance when the SDK times out', async () => {
      const wavetrakCBStub = sinon.stub();
      const featureFrameworkStub = {
        on: sinon.stub(),
        Event: { SDK_READY: 'sdk_ready', SDK_READY_TIMED_OUT: 'sdk_timed_out' },
      };

      const clientStub = sinon.stub().returns(featureFrameworkStub);

      analyticsUserStub.returns({ anonymousId: anonymousIdStub });
      anonymousIdStub.returns('123');
      getWindow().analytics = { user: analyticsUserStub };

      setWavetrakFeatureFrameworkCB(wavetrakCBStub);

      splitFactoryStub.returns({
        client: clientStub,
      });

      setupFeatureFramework({
        authorizationKey: 'authkey',
        user: fixture.backplane.user,
      }).catch(() => null);
      await new Promise((resolve) => {
        setTimeout(resolve, 0);
      });
      featureFrameworkStub.on.getCall(1).callArg(1);

      expect(getWavetrakFeatureFramework()).to.deep.equal(featureFrameworkStub);
      expect(splitFactoryStub).to.have.been.calledOnceWithExactly({
        core: { authorizationKey: 'authkey', key: '123' },
        startup: {
          readyTimeout: 5,
          retriesOnFailureBeforeReady: 2,
          eventFirstPushWindow: 5,
        },
        scheduler: {
          impressionsRefreshRate: 5,
        },
        sync: {
          splitFilters: [],
        },
      });

      expect(clientStub).to.have.been.calledOnce();
      expect(featureFrameworkStub.on).to.have.been.calledTwice();
      expect(wavetrakCBStub).to.have.been.calledOnceWithExactly(false);
    });

    it('should throw an error if no authorization key is passed', async () => {
      let err;
      try {
        await setupFeatureFramework({});
      } catch (error) {
        err = error;
      }
      expect(err).to.exist();
      expect(err.message).to.be.equal(MISSING_AUTH_KEY);
    });

    it('should throw an error if no user object is passed', async () => {
      let err;
      try {
        await setupFeatureFramework({ authorizationKey: '1' });
      } catch (error) {
        err = error;
      }
      expect(err).to.exist();
      expect(err.message).to.be.equal(MISSING_USER);
    });

    it('should short circuit and return a feature framework if it already exists', async () => {
      const featureFrameworkStub = {
        on: sinon.stub(),
        Event: { SDK_READY: 'sdk_ready', SDK_READY_TIMED_OUT: 'sdk_timed_out' },
      };

      setWavetrakFeatureFramework(featureFrameworkStub);

      const result = await setupFeatureFramework({ authorizationKey: '123' });

      expect(result).to.be.equal(featureFrameworkStub);
      expect(splitFactoryStub).to.not.have.been.called();
    });

    it('should gracefully fail setup if the identity is not defined', async () => {
      const wavetrakCBStub = sinon.stub();

      // setWavetrakIdentity(setupIdentity);
      setWavetrakFeatureFrameworkCB(wavetrakCBStub);

      let err;
      try {
        await setupFeatureFramework({ authorizationKey: '123', user: fixture.backplane.user });
      } catch (error) {
        err = error;
      }

      expect(err.message).to.be.equal(MISSING_ANONYMOUS_ID);
      expect(splitFactoryStub).to.not.have.been.called();
      expect(wavetrakCBStub).to.have.been.calledOnceWithExactly(false);
    });

    it('should gracefully fail setup if the identity anonymousId is not defined', async () => {
      setWavetrakIdentity(setupIdentity);
      delete getWindow().__WAVETRAK_IDENTITY__.id;

      let err;
      try {
        await setupFeatureFramework({ authorizationKey: '123', user: fixture.backplane.user });
      } catch (error) {
        err = error;
      }

      expect(err.message).to.be.equal(MISSING_ANONYMOUS_ID);
      expect(splitFactoryStub).to.not.have.been.called();
    });

    it('should fail to setup on the server side', async () => {
      delete global.window.document;

      let err;
      try {
        await setupFeatureFramework({ authorizationKey: '123' });
      } catch (error) {
        err = error;
      }

      expect(err.message).to.be.equal(SERVER_SIDE_ERROR);
      expect(splitFactoryStub).to.not.have.been.called();
    });
  });

  describe('getTreatment', () => {
    it('should return control if the feature framework is not defined', async () => {
      const result = getTreatment('some_treatment');

      expect(result).to.be.equal('control');
    });
    it('should return control if the identity is not defined', async () => {
      getWindow().wavetrakFeatureFramework = {};
      const result = getTreatment('some_treatment');

      expect(result).to.be.equal('control');
    });

    it('should call getTreatment on the SDK with the identity', async () => {
      const getTreatmentStub = sinon.stub().returns('on');
      setWavetrakIdentity({ spoofed: true });
      setWavetrakFeatureFramework({ getTreatment: getTreatmentStub });

      const result = getTreatment('some_treatment');

      expect(result).to.be.equal('on');
      expect(getTreatmentStub).to.have.been.calledOnceWithExactly('some_treatment', {
        spoofed: true,
      });
    });
  });

  describe('getTreatmentWithConfig', () => {
    it('should return control if the feature framework is not defined', async () => {
      const result = getTreatmentWithConfig('some_treatment');

      expect(result).to.deep.equal({ treatment: 'control', config: '""' });
    });

    it('should call getTreatment on the SDK with the identity', async () => {
      const getTreatmentWithConfigStub = sinon.stub().returns({ treatment: 'on', config: '""' });
      setWavetrakIdentity({ spoofed: true });
      setWavetrakFeatureFramework({ getTreatmentWithConfig: getTreatmentWithConfigStub });

      const result = getTreatmentWithConfig('some_treatment');

      expect(result).to.deep.equal({ treatment: 'on', config: '""' });
      expect(getTreatmentWithConfigStub).to.have.been.calledOnceWithExactly('some_treatment', {
        spoofed: true,
      });
    });
  });

  describe('createSplitFilter', () => {
    it('should create an array of split names', async () => {
      const result = createSplitFilter(
        { SL_TREATMENT: 'sl_treatment' },
        { SL_TREATMENT_2: 'sl_treatment_2' },
      );

      expect(result).to.deep.equal(['sl_treatment', 'sl_treatment_2']);
    });
  });

  describe('setWavetrakIdentity', () => {
    it('should set the identity on the window', async () => {
      getWindow().document = { createElement: () => {} };

      setWavetrakIdentity({ spoof: true });

      expect(getWavetrakIdentity()).to.deep.equal({ spoof: true });
    });

    it('should not try to set the identity if canUseDOM is false', async () => {
      delete global.window.document;
      setWavetrakIdentity({ spoof: true });

      expect(getWavetrakIdentity()).to.be.equal(null);
    });
  });

  describe('setWavetrakSplitFilters', () => {
    const splits = ['treatment1', 'treatment2'];

    it('should set the split filters on the window', async () => {
      global.window.document = { createElement: () => {} };

      setWavetrakSplitFilters(splits);

      expect(getWavetrakSplitFilters()).to.deep.equal(splits);
    });

    it('should return an empty array if no split filters are defined on the window', async () => {
      global.window.document = { createElement: () => {} };

      expect(getWavetrakSplitFilters()).to.deep.equal([]);
    });

    it('should not try to set the filters if canUseDOM is false', async () => {
      delete global.window.document;
      setWavetrakSplitFilters(splits);

      expect(getWavetrakSplitFilters()).to.deep.equal([]);
      expect(getWindow()?.wavetrakSplitFilters).to.be.undefined();
    });
  });

  describe('setWavetrakFeatureFramework', () => {
    it('should set the feature framework on the window', async () => {
      setWavetrakFeatureFramework({ spoof: true });

      expect(getWavetrakFeatureFramework()).to.deep.equal({ spoof: true });
    });

    it('should not try to set the feature framework if canUseDOM is false', async () => {
      delete global.window.document;
      setWavetrakFeatureFramework({ spoof: true });

      expect(getWindow()?.wavetrakFeatureFramework).to.be.equal(undefined);
      expect(getWavetrakFeatureFramework()).to.be.equal(null);
    });
  });

  describe('setWavetrakFeatureFrameworkCB', () => {
    it('should set the feature framework cb on the window', async () => {
      const stub = sinon.stub();
      setWavetrakFeatureFrameworkCB(stub);

      expect(getWavetrakFeatureFrameworkCB()).to.be.equal(stub);
    });

    it('should be able to clear the feature framework cb on the window', async () => {
      global.window.document = { createElement: () => {} };

      const stub = sinon.stub();
      setWavetrakFeatureFrameworkCB(stub);

      expect(getWavetrakFeatureFrameworkCB()).to.be.equal(stub);

      clearWavetrakFeatureFrameworkCB();

      expect(getWavetrakFeatureFrameworkCB()).to.be.equal(undefined);
    });

    it('should not try to set the feature framework cb if canUseDOM is false', async () => {
      delete global.window.document;
      const stub = sinon.stub();
      setWavetrakFeatureFrameworkCB(stub);

      expect(getWindow()?.wavetrakFeatureFrameworkCB).to.be.equal(undefined);
      expect(getWavetrakFeatureFrameworkCB()).to.be.equal(null);
    });

    it('should not try to clear the feature framework cb if canUseDOM is false', async () => {
      const stub = sinon.stub();
      setWavetrakFeatureFrameworkCB(stub);
      delete global.window.document;

      clearWavetrakFeatureFrameworkCB();

      global.window.document = { createElement: () => null };
      expect(getWavetrakFeatureFrameworkCB()).to.be.equal(stub);
    });
  });

  describe('clearWavetrakIdentity', () => {
    it('should not try to clear if canUseDOM is false', async () => {
      setWavetrakIdentity(true);

      delete global.window.document;
      clearWavetrakIdentity();

      global.window.document = { createElement: () => null };
      expect(getWavetrakIdentity()).to.be.equal(true);
    });
  });

  describe('setWavetrakAnalyticsReadyCB', () => {
    it('should set the analytics ready cb on the window', async () => {
      const stub = sinon.stub();
      setWavetrakAnalyticsReadyCB(stub);

      expect(getWavetrakAnalyticsReadyCB()).to.be.equal(stub);
    });
  });
});
