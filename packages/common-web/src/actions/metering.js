import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import { getWavetrakIdentity } from '../features';
import { incrementAnonymousMeter } from '../api/meter';
import { getMeterAnonymousId } from '../selectors/metering';

export const MISSING_ANONYMOUS_ID_ERROR =
  'Cannot perform anonymous surf check without anonymousId.';
export const setMeterRemaining = createAction('SET_METER_REMAINING');

export const performAnonymousMeteringSurfCheck = createAsyncThunk(
  'PERFORM_ANONYMOUS_METER_SURF_CHECK',
  /**
   * @param {object} args
   * @param {string?} args.anonymousId
   */
  // eslint-disable-next-line default-param-last
  async ({ anonymousId } = {}, thunkAPI) => {
    const id =
      anonymousId || getMeterAnonymousId(thunkAPI.getState()) || getWavetrakIdentity()?.anonymousId;

    if (!id) {
      throw new Error(MISSING_ANONYMOUS_ID_ERROR);
    }

    try {
      const { meterRemaining } = await incrementAnonymousMeter(id);
      return meterRemaining;
    } catch {
      // If for some reasons the patch call fails (user blocked it, or other errors)
      // we can still eagerly set the meter remaining to -1 since anonymous users
      // only get one page view
      return -1;
    }
  },
);
