import { createAsyncThunk, createAction } from '@reduxjs/toolkit';

import {
  postResendEmailVerification,
  postRecentlyVisited,
  fetchUserFavorites,
  postUserFavorite,
  deleteUserFavorite,
  putUserSettings,
} from '../api/user';
import { getUserSettings as userSettingsSelector } from '../selectors/user';
import getProxyUrl from '../utils/getProxyUrl';

import { SL } from '../constants';

export const FETCH_USER_DETAILS = 'FETCH_USER_DETAILS';
export const FETCH_USER_DETAILS_SUCCESS = 'FETCH_USER_DETAILS_SUCCESS';
export const FETCH_USER_DETAILS_FAILURE = 'FETCH_USER_DETAILS_FAILURE';

export const FETCH_USER_ENTITLEMENTS = 'FETCH_USER_ENTITLEMENTS';
export const FETCH_USER_ENTITLEMENTS_SUCCESS = 'FETCH_USER_ENTITLEMENTS_SUCCESS';
export const FETCH_USER_ENTITLEMENTS_FAILURE = 'FETCH_USER_ENTITLEMENTS_FAILURE';

export const FETCH_USER_SETTINGS = 'FETCH_USER_SETTINGS';
export const FETCH_USER_SETTINGS_SUCCESS = 'FETCH_USER_SETTINGS_SUCCESS';
export const FETCH_USER_SETTINGS_FAILURE = 'FETCH_USER_SETTINGS_FAILURE';

export const FETCH_USER_FAVORITES = 'FETCH_USER_FAVORITES';
export const FETCH_USER_FAVORITES_SUCCESS = 'FETCH_USER_FAVORITES_SUCCESS';
export const FETCH_USER_FAVORITES_FAILURE = 'FETCH_USER_FAVORITES_FAILURE';

export const resetEmailVerificationButton = createAction('RESET_EMAIL_VERIFICATION_BUTTON');

/**
 * @typedef {import('../api').WavetrakOptions} Options
 */

export const favoriteSpot = createAsyncThunk(
  'FAVORITE_SPOT',
  /**
   * @param {Object} params
   * @param {string} params.spotId
   * @param {boolean?} params.serverSide
   * @param {boolean?} params.sevenRatings
   * @param {Options['useAccessTokenQueryParam']} params.useAccessTokenQueryParam
   * @param {Options['serverSideCookies']} params.serverSideCookies
   * @returns {{ favorites: {}[] }}
   */
  async ({ spotId, sevenRatings = false, serverSide, ...wavetrakOptions }) => {
    await postUserFavorite(spotId, wavetrakOptions);

    // On the client-side we need to override the URL to point to `/kbyg/favorites` instead of `/favorites`
    const customUrl = serverSide ? null : `${getProxyUrl()}/kbyg`;

    // Refetch favorites to replace in the state
    const { data } = await fetchUserFavorites(null, {
      customUrl,
      query: { sevenRatings },
      ...wavetrakOptions,
    });
    return { favorites: data.favorites };
  },
);

export const unfavoriteSpot = createAsyncThunk(
  'UNFAVORITE_SPOT',
  /**
   * @param {Object} params
   * @param {string} params.spotId
   * @param {Options['useAccessTokenQueryParam']} params.useAccessTokenQueryParam
   * @param {Options['serverSideCookies']} params.serverSideCookies
   * @returns {{ spotId: string }}
   */
  async ({ spotId, ...wavetrakOptions }) => {
    await deleteUserFavorite(spotId, wavetrakOptions);

    return { spotId };
  },
);

export const setRecentlyVisited = createAsyncThunk(
  'SET_RECENTLY_VISITED',
  /**
   * @param {Object} params
   * @param {string} params._id
   * @param {string} params.name
   * @param {'spots' | 'subregions'} params.type
   * @param {Options['useAccessTokenQueryParam']} params.useAccessTokenQueryParam
   * @param {Options['serverSideCookies']} params.serverSideCookies
   * @returns {{ type: 'spots' | 'subregions', _id: string, name: string }[]}
   */
  async ({ type, _id, name, ...wavetrakOptions }) => {
    const { recentlyVisited } = await postRecentlyVisited({ type, _id, name }, wavetrakOptions);
    return {
      recentlyVisited,
    };
  },
);

export const resendEmailVerification = createAsyncThunk(
  'RESEND_EMAIL_VERIFICATION',
  /**
   * @param {Pick<Options, 'useAccessTokenQueryParam' | 'serverSideCookies'>} wavetrakOptions
   * @returns {true}
   */
  async (wavetrakOptions) => {
    await postResendEmailVerification({ brand: SL }, wavetrakOptions);
    return true;
  },
);

export const updateUserSettings = createAsyncThunk(
  'UPDATE_USER_SETTINGS',
  /**
   * @param {Object} params
   * @param {Object} params.updatedSetting
   * @param {Options['useAccessTokenQueryParam']} params.useAccessTokenQueryParam
   * @param {Options['serverSideCookies']} params.serverSideCookies
   * @returns {{ settings: import('../selectors').UserSettings }}
   */
  async ({ updatedSetting, ...wavetrakOptions }) => {
    const settings = await putUserSettings(updatedSetting, wavetrakOptions);
    return {
      settings,
    };
  },
);

export const skipUserOnboarding = createAsyncThunk(
  'SKIP_USER_ONBOARDING',
  /**
   * @param {Pick<Options, 'useAccessTokenQueryParam' | 'serverSideCookies'>} wavetrakOptions
   * @returns {{ onboarded: import('../selectors').UserSettings['onboarded'] }}
   */
  async (wavetrakOptions, thunkAPI) => {
    const settings = userSettingsSelector(thunkAPI.getState());
    const currentOnboarded = settings?.onboarded || [];
    const onboarded = [...currentOnboarded, 'FAVORITE_SPOTS'];
    await putUserSettings({ onboarded }, wavetrakOptions);
    return {
      onboarded,
    };
  },
);
