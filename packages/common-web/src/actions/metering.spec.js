import { expect } from 'chai';
import sinon from 'sinon';
import configureStore from 'redux-mock-store';
import deepFreeze from 'deep-freeze';
import thunk from 'redux-thunk';

import * as features from '../features/splitio';
import * as meterApi from '../api/meter';
import * as actions from './metering';
import { performAnonymousMeteringSurfCheck } from './metering';

describe('actions / metering', () => {
  const mockStore = configureStore([thunk]);

  const meterWithAnonymousId = deepFreeze({
    anonymousId: '1234',
  });

  describe('setMeterRemaining', () => {
    it(`should dispatch ${actions.setMeterRemaining}`, () => {
      const action = actions.setMeterRemaining(1);

      expect(action).to.deep.equal({ type: actions.setMeterRemaining.type, payload: 1 });
    });
  });

  describe('performAnonymousMeteringSurfCheck', () => {
    /** @type {sinon.SinonStub} */
    let getWavetrakIdentityStub;

    /** @type {sinon.SinonStub} */
    let incrementAnonymousMeterStub;

    beforeEach(() => {
      getWavetrakIdentityStub = sinon.stub(features, 'getWavetrakIdentity');
      incrementAnonymousMeterStub = sinon.stub(meterApi, 'incrementAnonymousMeter');
    });

    afterEach(() => {
      getWavetrakIdentityStub.restore();
      incrementAnonymousMeterStub.restore();
    });

    it('should throw an error if there is no available anonymous ID', async () => {
      const store = mockStore();

      await store.dispatch(performAnonymousMeteringSurfCheck());

      const expectedActions = [
        {
          type: performAnonymousMeteringSurfCheck.pending.toString(),
        },
        {
          type: performAnonymousMeteringSurfCheck.rejected.toString(),
          error: { message: actions.MISSING_ANONYMOUS_ID_ERROR },
        },
      ];

      expect(store.getActions()).to.containSubset(expectedActions);
    });

    it('should perform an anonymous meter surf check using a passed anonymousId - returns meterRemaining -1', async () => {
      const store = mockStore({ backplane: { meter: { meterWithAnonymousId } } });

      incrementAnonymousMeterStub.resolves({ meterRemaining: -1 });
      await store.dispatch(performAnonymousMeteringSurfCheck({ anonymousId: '789' }));

      const expectedActions = [
        {
          type: performAnonymousMeteringSurfCheck.pending.toString(),
        },
        {
          type: performAnonymousMeteringSurfCheck.fulfilled.toString(),
          payload: -1,
        },
      ];

      expect(incrementAnonymousMeterStub).to.have.been.calledOnceWithExactly('789');
      expect(store.getActions()).to.containSubset(expectedActions);
    });

    it('should perform an anonymous meter surf check using a passed anonymousId - returns meterRemaining 2', async () => {
      const store = mockStore({ backplane: { meter: { meterWithAnonymousId } } });

      incrementAnonymousMeterStub.resolves({ meterRemaining: 2 });
      await store.dispatch(performAnonymousMeteringSurfCheck({ anonymousId: '789' }));

      const expectedActions = [
        {
          type: performAnonymousMeteringSurfCheck.pending.toString(),
        },
        {
          type: performAnonymousMeteringSurfCheck.fulfilled.toString(),
          payload: 2,
        },
      ];

      expect(incrementAnonymousMeterStub).to.have.been.calledOnceWithExactly('789');
      expect(store.getActions()).to.containSubset(expectedActions);
    });

    it('should perform an anonymous meter surf check using the stored value', async () => {
      const store = mockStore({ backplane: { meter: meterWithAnonymousId } });

      incrementAnonymousMeterStub.resolves({ meterRemaining: -1 });
      await store.dispatch(performAnonymousMeteringSurfCheck());

      const expectedActions = [
        {
          type: performAnonymousMeteringSurfCheck.pending.toString(),
        },
        {
          type: performAnonymousMeteringSurfCheck.fulfilled.toString(),
          payload: -1,
        },
      ];

      expect(store.getActions()).to.containSubset(expectedActions);
      expect(incrementAnonymousMeterStub).to.have.been.calledOnceWithExactly(
        meterWithAnonymousId.anonymousId,
      );
    });

    it('should perform an anonymous meter surf check using the wavetrak identity', async () => {
      const store = mockStore();

      incrementAnonymousMeterStub.resolves({ meterRemaining: -1 });
      getWavetrakIdentityStub.returns({ anonymousId: '567' });

      await store.dispatch(performAnonymousMeteringSurfCheck());

      const expectedActions = [
        {
          type: performAnonymousMeteringSurfCheck.pending.toString(),
        },
        {
          type: performAnonymousMeteringSurfCheck.fulfilled.toString(),
          payload: -1,
        },
      ];

      expect(incrementAnonymousMeterStub).to.have.been.calledOnceWithExactly('567');
      expect(store.getActions()).to.containSubset(expectedActions);
    });

    it('should return -1 in the payload even if the PUT call fails', async () => {
      const store = mockStore();

      incrementAnonymousMeterStub.throws();

      await store.dispatch(performAnonymousMeteringSurfCheck({ anonymousId: '123' }));

      const expectedActions = [
        {
          type: performAnonymousMeteringSurfCheck.pending.toString(),
        },
        {
          type: performAnonymousMeteringSurfCheck.fulfilled.toString(),
          payload: -1,
        },
      ];

      expect(incrementAnonymousMeterStub).to.have.been.calledOnceWithExactly('123');
      expect(store.getActions()).to.containSubset(expectedActions);
    });
  });
});
