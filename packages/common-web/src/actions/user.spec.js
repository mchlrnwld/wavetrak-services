import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as userApi from '../api/user';
import {
  favoriteSpot,
  unfavoriteSpot,
  setRecentlyVisited,
  resendEmailVerification,
  resetEmailVerificationButton,
  updateUserSettings,
  skipUserOnboarding,
} from './user';
import getProxyUrl from '../utils/getProxyUrl';

describe('actions / user', () => {
  let mockStore;

  const wavetrakOptions = { useAccessTokenQueryParam: true, serverSideCookies: { cookies: true } };

  before(() => {
    mockStore = configureStore([thunk]);
  });

  describe('resetEmailVerificationButton', () => {
    it(`should dispatch ${resetEmailVerificationButton.type}`, () => {
      expect(resetEmailVerificationButton()).to.deep.equal({
        type: resetEmailVerificationButton.type,
        payload: undefined,
      });
    });
  });

  describe('favoriteSpot', () => {
    beforeEach(() => {
      sinon.stub(userApi, 'postUserFavorite');
      sinon.stub(userApi, 'fetchUserFavorites');
    });

    afterEach(() => {
      userApi.postUserFavorite.restore();
      userApi.fetchUserFavorites.restore();
    });

    it(`should handle ${favoriteSpot.fulfilled}`, async () => {
      const store = mockStore();
      const favorites = deepFreeze({
        data: { favorites: [{ _id: '123' }] },
      });
      userApi.postUserFavorite.resolves();
      userApi.fetchUserFavorites.resolves(favorites);

      const expectedActions = [
        {
          type: favoriteSpot.pending.toString(),
        },
        {
          type: favoriteSpot.fulfilled.toString(),
          payload: { favorites: favorites.data.favorites },
        },
      ];

      await store.dispatch(favoriteSpot({ spotId: 'spotId', sevenRatings: true }));
      expect(store.getActions()).to.containSubset(expectedActions);
      expect(userApi.postUserFavorite).to.have.been.calledOnceWithExactly('spotId', {});
      expect(userApi.fetchUserFavorites).to.have.been.calledOnceWithExactly(null, {
        customUrl: `${getProxyUrl()}/kbyg`,
        query: {
          sevenRatings: true,
        },
      });
    });

    it(`should handle ${favoriteSpot.fulfilled} - server-side`, async () => {
      const store = mockStore();
      const favorites = deepFreeze({
        data: { favorites: [{ _id: '123' }] },
      });
      userApi.postUserFavorite.resolves();
      userApi.fetchUserFavorites.resolves(favorites);

      const expectedActions = [
        {
          type: favoriteSpot.pending.toString(),
        },
        {
          type: favoriteSpot.fulfilled.toString(),
          payload: { favorites: favorites.data.favorites },
        },
      ];

      await store.dispatch(favoriteSpot({ spotId: 'spotId', serverSide: true }));
      expect(store.getActions()).to.containSubset(expectedActions);
      expect(userApi.postUserFavorite).to.have.been.calledOnceWithExactly('spotId', {});
      expect(userApi.fetchUserFavorites).to.have.been.calledOnceWithExactly(null, {
        customUrl: null,
        query: {
          sevenRatings: false,
        },
      });
    });

    it(`should handle ${favoriteSpot.rejected}`, async () => {
      const store = mockStore({});
      userApi.postUserFavorite.rejects({ message: 'This is an error' });

      const expectedActions = [
        {
          type: favoriteSpot.pending.toString(),
        },
        {
          type: favoriteSpot.rejected.toString(),
          error: { message: 'This is an error' },
        },
      ];
      await store.dispatch(favoriteSpot({ spotId: 'spotId', ...wavetrakOptions }));
      expect(store.getActions()).to.containSubset(expectedActions);
    });
  });

  describe('unfavoriteSpot', () => {
    beforeEach(() => {
      sinon.stub(userApi, 'deleteUserFavorite');
    });

    afterEach(() => {
      userApi.deleteUserFavorite.restore();
    });

    it(`should handle ${unfavoriteSpot.fulfilled}`, async () => {
      const store = mockStore({});
      userApi.deleteUserFavorite.resolves();

      const expectedActions = [
        {
          type: unfavoriteSpot.pending.toString(),
        },
        {
          type: unfavoriteSpot.fulfilled.toString(),
          payload: { spotId: 'spotId' },
        },
      ];

      await store.dispatch(unfavoriteSpot({ spotId: 'spotId', ...wavetrakOptions }));
      expect(store.getActions()).to.containSubset(expectedActions);
      expect(userApi.deleteUserFavorite).to.have.been.calledOnceWithExactly(
        'spotId',
        wavetrakOptions,
      );
    });

    it(`should handle ${unfavoriteSpot.rejected}`, async () => {
      const store = mockStore({});
      userApi.deleteUserFavorite.rejects({ message: 'This is an error' });

      const expectedActions = [
        {
          type: unfavoriteSpot.pending.toString(),
        },
        {
          type: unfavoriteSpot.rejected.toString(),
          error: { message: 'This is an error' },
        },
      ];
      await store.dispatch(unfavoriteSpot({ spotId: 'spotId' }));
      expect(store.getActions()).to.containSubset(expectedActions);
    });
  });

  describe('setRecentlyVisited', () => {
    beforeEach(() => {
      sinon.stub(userApi, 'postRecentlyVisited');
    });

    afterEach(() => {
      userApi.postRecentlyVisited.restore();
    });

    it(`should handle ${setRecentlyVisited.fulfilled}`, async () => {
      const store = mockStore({});
      const spotId = 'firstSpotId';
      const spotName = 'First Spot Name';
      const recentlyVisited = {
        spots: [{ _id: spotId, name: spotName }],
        subregions: [],
      };
      const expectedActions = [
        {
          type: setRecentlyVisited.pending.toString(),
        },
        {
          type: setRecentlyVisited.fulfilled.toString(),
          payload: { recentlyVisited },
        },
      ];

      userApi.postRecentlyVisited.resolves({ recentlyVisited });
      await store.dispatch(
        setRecentlyVisited({ type: 'spots', _id: spotId, name: spotName, ...wavetrakOptions }),
      );
      expect(store.getActions()).to.containSubset(expectedActions);
      expect(userApi.postRecentlyVisited).to.have.been.calledOnceWithExactly(
        {
          type: 'spots',
          _id: spotId,
          name: spotName,
        },
        wavetrakOptions,
      );
    });

    it(`should handle ${setRecentlyVisited.rejected}`, async () => {
      const store = mockStore({});
      userApi.postRecentlyVisited.rejects({ message: 'This is an error' });

      const expectedActions = [
        {
          type: setRecentlyVisited.pending.toString(),
        },
        {
          type: setRecentlyVisited.rejected.toString(),
          error: { message: 'This is an error' },
        },
      ];
      await store.dispatch(setRecentlyVisited({ type: 'spots', spotId: 'spotId', name: 'name' }));
      expect(store.getActions()).to.containSubset(expectedActions);
    });
  });

  describe('resendEmailVerification', () => {
    beforeEach(() => {
      sinon.stub(userApi, 'postResendEmailVerification');
    });

    afterEach(() => {
      userApi.postResendEmailVerification.restore();
    });
    it(`should handle ${resendEmailVerification.fulfilled}`, async () => {
      const store = mockStore({});
      const expectedActions = [
        {
          type: resendEmailVerification.pending.toString(),
        },
        {
          type: resendEmailVerification.fulfilled.toString(),
          payload: true,
        },
      ];

      userApi.postResendEmailVerification.resolves();
      await store.dispatch(resendEmailVerification());
      expect(store.getActions()).to.containSubset(expectedActions);
      expect(userApi.postResendEmailVerification).to.have.been.calledOnceWithExactly(
        { brand: 'sl' },
        undefined,
      );
    });

    it(`should handle ${resendEmailVerification.rejected}`, async () => {
      const store = mockStore({});
      userApi.postResendEmailVerification.rejects({ message: 'This is an error' });

      const expectedActions = [
        {
          type: resendEmailVerification.pending.toString(),
        },
        {
          type: resendEmailVerification.rejected.toString(),
          error: { message: 'This is an error' },
        },
      ];
      await store.dispatch(resendEmailVerification());
      expect(store.getActions()).to.containSubset(expectedActions);
    });
  });

  describe('updateUserSettings', () => {
    beforeEach(() => {
      sinon.stub(userApi, 'putUserSettings');
    });

    afterEach(() => {
      userApi.putUserSettings.restore();
    });

    it(`should handle ${updateUserSettings.fulfilled}`, async () => {
      const store = mockStore();
      const updatedSetting = { homeForecast: 'subregionId' };
      const settings = deepFreeze({
        data: updatedSetting,
      });
      userApi.putUserSettings.resolves(settings);

      const expectedActions = [
        {
          type: updateUserSettings.pending.toString(),
        },
        {
          type: updateUserSettings.fulfilled.toString(),
          payload: { settings },
        },
      ];

      await store.dispatch(updateUserSettings({ updatedSetting }));
      expect(store.getActions()).to.containSubset(expectedActions);
      expect(userApi.putUserSettings).to.have.been.calledOnceWithExactly(updatedSetting, {});
    });

    it(`should handle ${updateUserSettings.rejected}`, async () => {
      const store = mockStore({});
      const updatedSetting = { homeForecast: 'subregionId' };
      userApi.putUserSettings.rejects({ message: 'This is an error' });

      const expectedActions = [
        {
          type: updateUserSettings.pending.toString(),
        },
        {
          type: updateUserSettings.rejected.toString(),
          error: { message: 'This is an error' },
        },
      ];
      await store.dispatch(updateUserSettings({ updatedSetting }));
      expect(store.getActions()).to.containSubset(expectedActions);
    });
  });

  describe('skipUserOnboarding', () => {
    beforeEach(() => {
      sinon.stub(userApi, 'putUserSettings');
    });

    afterEach(() => {
      userApi.putUserSettings.restore();
    });

    it(`should handle ${skipUserOnboarding.fulfilled}`, async () => {
      const store = mockStore({
        backplane: {
          user: {
            settings: {
              onboarded: ['NEW_SURFLINE_ONBOARDING_INTRO'],
            },
          },
        },
      });
      const onboarded = ['NEW_SURFLINE_ONBOARDING_INTRO', 'FAVORITE_SPOTS'];
      userApi.putUserSettings.resolves();

      const expectedActions = [
        {
          type: skipUserOnboarding.pending.toString(),
        },
        {
          type: skipUserOnboarding.fulfilled.toString(),
          payload: { onboarded },
        },
      ];

      await store.dispatch(skipUserOnboarding({}));
      expect(store.getActions()).to.containSubset(expectedActions);
      expect(userApi.putUserSettings).to.have.been.calledOnceWithExactly({ onboarded }, {});
    });

    it(`should handle ${skipUserOnboarding.rejected}`, async () => {
      const store = mockStore({
        backplane: {
          user: {
            settings: {
              onboarded: ['NEW_SURFLINE_ONBOARDING_INTRO'],
            },
          },
        },
      });
      userApi.putUserSettings.rejects({ message: 'This is an error' });

      const expectedActions = [
        {
          type: skipUserOnboarding.pending.toString(),
        },
        {
          type: skipUserOnboarding.rejected.toString(),
          error: { message: 'This is an error' },
        },
      ];
      await store.dispatch(skipUserOnboarding({}));
      expect(store.getActions()).to.containSubset(expectedActions);
    });
  });
});
