/* istanbul ignore file */

/**
 * @description Given a pub ID and ad Server, returns a script tag for injection amazon UAM.
 * @param {string} pubID
 * @param {string} adServer
 * @param {boolean} [withScriptTag=true]
 * @returns {string}
 */
const amazonUAM = (pubID, adServer, withScriptTag = true) => `
${withScriptTag ? '<script type="text/javascript">' : ''}
    !function(a9,a,p,s,t,A,g){if(a[a9])return;function q(c,r){a[a9]._Q.push([c,r])}a[a9]={init:function(){q("i",arguments)},fetchBids:function(){q("f",arguments)},setDisplayBids:function(){},targetingKeys:function(){return[]},_Q:[]};A=p.createElement(s);A.async=!0;A.src=t;g=p.getElementsByTagName(s)[0];g.parentNode.insertBefore(A,g)}("apstag",window,document,"script","//c.amazon-adsystem.com/aax2/apstag.js");
    apstag.init({
        pubID: '${pubID}',
        adServer: '${adServer}'
    });
${withScriptTag ? '</script>' : ''}
`;

export default amazonUAM;
