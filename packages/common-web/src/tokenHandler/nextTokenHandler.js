import cookie from 'cookie';

import { validateToken, doRefreshToken } from './api';
import { ACCESS_TOKEN_AGE, REFRESH_TOKEN_AGE } from '../constants';

const getDomain = () =>
  ({
    development: 'localhost',
    production: 'www.surfline.com',
    staging: 'staging.surfline.com',
    sandbox: 'sandbox.surfline.com',
  }[process.env.APP_ENV || 'development']);

const logger = console.log;

const defaultlogger = {
  info: (msg) => logger(msg),
  debug: (msg) => logger(msg),
  error: (msg) => logger(msg),
};

/**
 * @param {string} name
 * @param {string} value
 * @param {cookie.CookieSerializeOptions} options
 */
const serializeCookie = (name, value, options) => cookie.serialize(name, value, options);

const getCookieConfig = (expires = 86400 * 365 * 1000) => ({
  path: '/',
  domain: `.${getDomain()}`,
  expires: new Date(new Date().getTime() + expires),
});

/**
 * @param {string} trustedEndpoint
 * @param {any} [log]
 */
export const nextTokenHandler =
  (trustedEndpoint, log = defaultlogger) =>
  /**
   * @param {import('http').IncomingMessage & { userId?: string, cookies: Record<string, string>}} req
   * @param {import('http').ServerResponse} res
   */
  async (req, res) => {
    req.userId = null;
    let valid = null;
    const { access_token: accessToken, refresh_token: refreshToken } = req.cookies;

    // check that the access token is valid
    if (accessToken) {
      try {
        valid = await validateToken(accessToken, trustedEndpoint);
      } catch (err) {
        valid = null;
        log.error('tokenHandler: exception occured for validateToken ', err.toString());
      }
      if (valid) {
        req.userId = valid;
      }
    }

    // refresh if the user has a refresh token but no valid access token
    if (refreshToken && (!accessToken || !valid)) {
      try {
        const cookiesToSet = [];

        const refreshedToken = await doRefreshToken(refreshToken, trustedEndpoint);
        valid = await validateToken(refreshedToken.access_token, trustedEndpoint);

        req.userId = valid;

        log.info('tokenHandler: doRefreshToken response ', refreshedToken);

        const accessTokenCookie = serializeCookie(
          'access_token',
          refreshedToken.access_token,
          getCookieConfig(ACCESS_TOKEN_AGE * 1000),
        );

        const refreshTokenCookie = serializeCookie(
          'refresh_token',
          refreshedToken.refresh_token,
          getCookieConfig(REFRESH_TOKEN_AGE * 1000),
        );

        cookiesToSet.push(accessTokenCookie);
        cookiesToSet.push(refreshTokenCookie);

        res.setHeader('Set-Cookie', cookiesToSet);

        req.cookies.access_token = refreshedToken.access_token;
        req.cookies.refresh_token = refreshedToken.refresh_token;
      } catch (err) {
        const domain = getDomain();

        const cookiesToClear = [];

        log.error({
          err: err.toString(),
          valid,
          domain,
          cookies: JSON.stringify(req.cookies),
          message: 'tokenHandler: exception occured while refreshing tokens',
        });

        const clearCookieOptions = { path: '/', domain, maxAge: 0 };
        const clearCookieOptionsWithDotDomain = { path: '/', domain: `.${domain}`, maxAge: 0 };

        cookiesToClear.push(serializeCookie('access_token', '', clearCookieOptions));
        cookiesToClear.push(serializeCookie('refresh_token', '', clearCookieOptions));

        cookiesToClear.push(serializeCookie('access_token', '', clearCookieOptionsWithDotDomain));
        cookiesToClear.push(serializeCookie('refresh_token', '', clearCookieOptionsWithDotDomain));

        res.setHeader('Set-Cookie', cookiesToClear);

        req.cookies.access_token = undefined;
        req.cookies.refresh_token = undefined;
      }
    }
  };
