import sinon from 'sinon';
import * as authApi from './api';
import { tokenHandler } from './tokenHandler';

describe('tokenHandler', () => {
  let validateTokenStub;
  let doRefreshTokenStub;

  const request = {
    cookies: {
      access_token: '6a9f912f6bcd7505106e5ada5c507859cee9c201',
      refresh_token: '9117ff667e3278bbb7ee1386249b0cd5a06d6f83',
    },
    hostname: 'hostname',
  };
  const response = {
    cookie: () => true,
    clearCookie: () => true,
  };
  const nextCallBack = () => {};

  beforeEach(() => {
    validateTokenStub = sinon.stub(authApi, 'validateToken');
    doRefreshTokenStub = sinon.stub(authApi, 'doRefreshToken');
  });
  afterEach(() => {
    validateTokenStub.restore();
    doRefreshTokenStub.restore();
  });

  it('should check for valid access_token', async () => {
    validateTokenStub.returns('5b15b890e2de0c00116554a9');

    const callback = tokenHandler();
    await callback(request, response, nextCallBack);
    // Access token should be validated once
    sinon.assert.calledOnce(validateTokenStub);
    // Since this is a valid access token the "doRefreshToken" should not be called at all
    sinon.assert.notCalled(doRefreshTokenStub);
  });

  it('should check for invalid access_token', async () => {
    validateTokenStub.onCall(0).returns(null);
    validateTokenStub.onCall(1).returns('56fae6fe19b74318a73a786c');
    const payload = {
      token_type: 'bearer',
      access_token: '5d9a519187a0d14155e5ab729302e9beca10c4d4',
      expires_in: '2592000',
      refresh_token: '037386a5f2350173d98c10d66d039f189a9bb24d',
    };
    doRefreshTokenStub.returns(payload);
    const requestSpy = sinon.spy();
    requestSpy(request);
    const callback = tokenHandler();
    await callback(request, response, nextCallBack);

    // Since this is a valid access token the "doRefreshToken" should  be called 1 time
    sinon.assert.callCount(doRefreshTokenStub, 1);
    // validateToken will be called 2 times if the refresh_token is valid
    sinon.assert.callCount(validateTokenStub, 2);
    // the reponse from validateToken will be set in req.userId
    sinon.assert.calledWith(requestSpy, sinon.match.has('userId', '56fae6fe19b74318a73a786c'));
  });
});
