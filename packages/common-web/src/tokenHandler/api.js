import fetch from 'isomorphic-fetch';

/* istanbul ignore next */
export const validateToken = async (accessToken, authService) => {
  const resp = await fetch(`${authService}/authorise?access_token=${accessToken}`);
  if (resp.status === 200) {
    const payload = await resp.json();
    return payload.id;
  }
  return null;
};

/* istanbul ignore next */
export const doRefreshToken = async (refreshToken, authService) => {
  const resp = await fetch(`${authService}/token/`, {
    method: 'POST',
    headers: {
      Authorization: `Basic ${Buffer.from(
        `${process.env.WEB_CLIENT_ID}:${process.env.WEB_CLIENT_SECRET}`,
      ).toString('base64')}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: `grant_type=refresh_token&refresh_token=${refreshToken}`,
  });
  const payload = await resp.json();
  if (resp.status === 200) return payload;
  throw new Error(JSON.stringify(payload));
};
