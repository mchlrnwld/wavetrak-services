/* istanbul ignore file */
/* eslint-disable camelcase */
/* eslint-disable no-console */

import { validateToken, doRefreshToken } from './api';
import { ACCESS_TOKEN_AGE, REFRESH_TOKEN_AGE } from '../constants';

const getDomain = () =>
  ({
    development: 'localhost',
    production: 'www.surfline.com',
    staging: 'staging.surfline.com',
    sandbox: 'sandbox.surfline.com',
  }[process.env.APP_ENV || process.env.NODE_ENV || 'development']);

const logger = console.log;
const defaultlogger = {
  info: (msg) => logger(msg),
  debug: (msg) => logger(msg),
  error: (msg) => logger(msg),
};

const getCookieConfig = (expires = 86400 * 365 * 1000) => ({
  path: '/',
  domain: `.${getDomain()}`,
  expires: new Date(new Date().getTime() + expires),
});

/**
 * Express middleware to handle token-related requests and cookies.
 *
 * @returns modified request and cookies on response, if appropriate
 */

export const tokenHandler =
  (authService, log = defaultlogger) =>
  async (req, res, next) => {
    req.userId = null;
    let valid = null;
    const { access_token, refresh_token } = req.cookies;

    // check that the access token is valid
    if (access_token) {
      try {
        valid = await validateToken(access_token, authService);
      } catch (err) {
        valid = null;
        log.info('tokenHandler: exception occured for validateToken ', err.toString());
      }
      if (valid) req.userId = valid;
    }

    // refresh if the user has a refresh token but no valid access token
    if (refresh_token && (!access_token || !valid)) {
      try {
        const refreshedToken = await doRefreshToken(refresh_token, authService);
        valid = await validateToken(refreshedToken.access_token, authService);
        req.userId = valid;
        log.info('tokenHandler: doRefreshToken response ', refreshedToken);
        res.cookie(
          'access_token',
          refreshedToken.access_token,
          getCookieConfig(ACCESS_TOKEN_AGE * 1000),
        );
        res.cookie(
          'refresh_token',
          refreshedToken.refresh_token,
          getCookieConfig(REFRESH_TOKEN_AGE * 1000),
        );
        req.cookies.access_token = refreshedToken.access_token;
        req.cookies.refresh_token = refreshedToken.refresh_token;
      } catch (err) {
        const domain = getDomain();
        log.error({
          err: err.toString(),
          valid,
          domain,
          cookies: JSON.stringify(req.cookies),
          message: 'tokenHandler: exception occured while refreshing tokens',
        });
        res.clearCookie('access_token', { path: '/', domain });
        res.clearCookie('refresh_token', { path: '/', domain });
        res.clearCookie('access_token', { path: '/', domain: `.${domain}` });
        res.clearCookie('refresh_token', { path: '/', domain: `.${domain}` });
        req.cookies.access_token = undefined;
        req.cookies.refresh_token = undefined;
        // TODO remove this post CF deprecation
        // trying to clear our environment specific tokens that CF sets
        res.clearCookie('access_token', {
          path: '/',
          domain: `.${process.env.NODE_ENV}.surfline.com`,
        });
        res.clearCookie('refresh_token', {
          path: '/',
          domain: `.${process.env.NODE_ENV}.surfline.com`,
        });
      }
    }

    return next();
  };

export default tokenHandler;
