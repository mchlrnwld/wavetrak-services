import sinon from 'sinon';
import { expect } from 'chai';
import * as authApi from './api';
import { nextTokenHandler } from './nextTokenHandler';

const logger = {
  info: () => {},
  debug: () => {},
  error: () => {},
};

describe('nextTokenHandler', () => {
  let validateTokenStub;
  let doRefreshTokenStub;

  let clock;
  const currentDate = 1640995200; // 2022-01-01T00:00:00.000Z

  const request = {
    cookies: {
      access_token: '6a9f912f6bcd7505106e5ada5c507859cee9c201',
      refresh_token: '9117ff667e3278bbb7ee1386249b0cd5a06d6f83',
    },
    hostname: 'hostname',
  };

  const setHeader = sinon.stub();
  const response = {
    setHeader,
  };

  beforeEach(() => {
    clock = sinon.useFakeTimers(currentDate * 1000);
    validateTokenStub = sinon.stub(authApi, 'validateToken');
    doRefreshTokenStub = sinon.stub(authApi, 'doRefreshToken');
  });

  afterEach(() => {
    validateTokenStub.restore();
    doRefreshTokenStub.restore();
    setHeader.reset();
    clock.restore();
  });

  it('should check for valid access_token', async () => {
    validateTokenStub.returns('5b15b890e2de0c00116554a9');

    const callback = nextTokenHandler('', logger);
    await callback(request, response);

    // Access token should be validated once
    sinon.assert.calledOnce(validateTokenStub);
    // Since this is a valid access token the "doRefreshToken" should not be called at all
    sinon.assert.notCalled(doRefreshTokenStub);
  });

  it('should check for invalid access_token', async () => {
    validateTokenStub.onCall(0).returns(null);
    validateTokenStub.onCall(1).returns('56fae6fe19b74318a73a786c');
    const payload = {
      token_type: 'bearer',
      access_token: '5d9a519187a0d14155e5ab729302e9beca10c4d4',
      expires_in: '2592000',
      refresh_token: '037386a5f2350173d98c10d66d039f189a9bb24d',
    };

    doRefreshTokenStub.returns(payload);
    const callback = nextTokenHandler('', logger);
    await callback(request, response);

    // Since this is a valid access token the "doRefreshToken" should  be called 1 time
    sinon.assert.callCount(doRefreshTokenStub, 1);
    // validateToken will be called 2 times if the refresh_token is valid
    sinon.assert.callCount(validateTokenStub, 2);

    sinon.assert.calledWith(setHeader, 'Set-Cookie', [
      'access_token=5d9a519187a0d14155e5ab729302e9beca10c4d4; Domain=.localhost; Path=/; Expires=Mon, 31 Jan 2022 00:00:00 GMT',
      'refresh_token=037386a5f2350173d98c10d66d039f189a9bb24d; Domain=.localhost; Path=/; Expires=Sun, 01 Jan 2023 00:00:00 GMT',
    ]);

    // the reponse from validateToken will be set in req.userId
    expect(request.userId).to.equal('56fae6fe19b74318a73a786c');
    expect(request.cookies.access_token).to.be.equal('5d9a519187a0d14155e5ab729302e9beca10c4d4');
    expect(request.cookies.refresh_token).to.be.equal('037386a5f2350173d98c10d66d039f189a9bb24d');
  });

  it('should clear the tokens if refreshing fails', async () => {
    validateTokenStub.onCall(0).returns(null);
    validateTokenStub.onCall(1).throws(new Error('Invalid token'));
    const payload = {
      token_type: 'bearer',
      access_token: '5d9a519187a0d14155e5ab729302e9beca10c4d4',
      expires_in: '2592000',
      refresh_token: '037386a5f2350173d98c10d66d039f189a9bb24d',
    };

    doRefreshTokenStub.returns(payload);

    const callback = nextTokenHandler('', logger);

    await callback(request, response);

    // Since this is a valid access token the "doRefreshToken" should  be called 1 time
    sinon.assert.callCount(doRefreshTokenStub, 1);
    // validateToken will be called 2 times if the refresh_token is valid
    sinon.assert.callCount(validateTokenStub, 2);

    sinon.assert.calledWith(setHeader, 'Set-Cookie', [
      'access_token=; Max-Age=0; Domain=localhost; Path=/',
      'refresh_token=; Max-Age=0; Domain=localhost; Path=/',
      'access_token=; Max-Age=0; Domain=.localhost; Path=/',
      'refresh_token=; Max-Age=0; Domain=.localhost; Path=/',
    ]);

    expect(request.userId).to.be.null();
    expect(request.cookies.access_token).to.be.undefined();
    expect(request.cookies.refresh_token).to.be.undefined();
  });
});
