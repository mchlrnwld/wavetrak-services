/* istanbul ignore file */
/* eslint-disable no-undef */

const canUseDOM = !!(
  typeof window !== 'undefined' &&
  window.document &&
  window.document.createElement
);

export const getCanUseDOM = () =>
  !!(typeof window !== 'undefined' && window.document && window.document.createElement);

export default canUseDOM;
