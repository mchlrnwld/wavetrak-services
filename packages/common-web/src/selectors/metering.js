/**
 * @typedef {object} MeterState
 * @property {number} meterRemaining
 * @property {number} meterLimit
 * @property {'off' | 'on'} treatmentState
 * @property {'sl_rate_limiting_v3'} treatmentName
 * @property {string} anonymousId
 * @property {string} [_id]
 * @property {string} [user]
 * @property {boolean} [active]
 * @property {string} [countryCode]
 * @property {number} meterLimit
 * @property {string} [createdAt]
 * @property {string} [updatedAt]
 * @property {'sl_metered'} [entitlement]
 */

/**
 * @typedef {import('./').BackplaneState} State
 */

/**
 * @param {State} state
 * @returns {MeterState?}
 */
export const getMeter = (state) => state.backplane?.meter;

/**
 * @param {State} state
 * @returns {MeterState['meterRemaining']?}
 */
export const getMeterRemaining = (state) => state.backplane?.meter?.meterRemaining;

/**
 * @param {State} state
 * @returns {MeterState['treatmentState']?}
 */
export const getMeterTreatmentState = (state) => state.backplane?.meter?.treatmentState;

/**
 * @param {State} state
 * @returns {MeterState['anonymousId']?}
 */
export const getMeterAnonymousId = (state) => state.backplane?.meter?.anonymousId;
