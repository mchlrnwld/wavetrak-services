export * from './user';
export * from './metering';

/**
 * @typedef {object} BackplaneAppState
 * @property {boolean} meterLoaded
 * @property {boolean} shouldRecomputeMeterModal
 */

/**
 * @typedef {object} BackplaneState
 * @property {object} backplane
 * @property {import('./metering').MeterState?} [backplane.meter]
 * @property {import('./user').UserState?} backplane.user
 * @property {string[]} backplane.errors
 * @property {BackplaneAppState?} backplane.state
 */
