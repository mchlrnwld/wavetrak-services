export default {
  backplane: {
    user: {
      details: {
        _id: '456',
        firstName: 'Luigi',
        lastName: 'Doge',
        email: 'luigi@surfline.com',
        isEmailVerified: false,
      },
      favorites: [
        {
          _id: '5842041f4e65fad6a77088e4',
          name: 'South Side/13th Street',
          cameras: [
            {
              title: 'South Side/13th Street',
              streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253451/91961185/playlist.m3u8',
              stillUrl:
                'https://camstills.cdn-surfline.com/sealbeachsscam/latest_small_pixelated.png',
              rewindBaseUrl:
                'http://live-cam-archive.cdn-surfline.com/sealbeachsscam/sealbeachsscam',
              _id: '58dad5a4d572cb0012fdb4b1',
              status: {
                isDown: false,
                message: '',
              },
            },
          ],
          conditions: {
            human: false,
            value: null,
          },
          wind: {
            speed: 11.81,
            direction: 342.58,
          },
          waveHeight: {
            human: false,
            min: 0,
            max: 1,
            occasional: null,
            humanRelation: null,
          },
          tide: {
            previous: {
              type: 'HIGH',
              height: 5,
              timestamp: 1490721448,
              utcOffset: -7,
            },
            next: {
              type: 'LOW',
              height: 0,
              timestamp: 1490743709,
              utcOffset: -7,
            },
          },
          thumbnail:
            'https://spot-thumbnails.cdn-surfline.com/spots/5842041f4e65fad6a77088e4/5842041f4e65fad6a77088e4_1500.jpg',
          rank: 0,
        },
      ],
      entitlements: [
        'sl_no_ads',
        'sl_premium',
        'Single Surf Alert address',
        'Local photos',
        'Uploads',
        'Galleries',
        'Favorites',
        'Follow Photographers',
      ],
      promotionEntitlements: ['sl_promotion'],
      settings: {
        _id: 'abc123',
        user: 'xyzpdq',
        location: {
          coordinates: [0, 0],
          type: 'Point',
        },
        onboarded: [],
        units: {
          temperature: 'C',
          windSpeed: 'KPH',
          tideHeight: 'M',
          swellHeight: 'M',
          surfHeight: 'M',
        },
      },
      loading: false,
      emailSent: false,
      warnings: [{ type: 'failedPayment' }],
      countryCode: 'US',
      subdivisionCode: 'CA',
    },
    errors: ['some_error'],
    state: {
      shouldRecomputeMeterModal: false,
      meterLoaded: true,
    },
    meter: {
      anonymousId: 'f74d123f-faf3-473a-9712-465dae98cd5d',
      treatmentName: 'sl_rate_limiting_v3',
      treatmentState: 'on',
      meterLimit: 0,
      meterRemaining: -1,
    },
  },
};

export const meteredUser = {
  backplane: {
    user: {
      details: {
        _id: '456',
        firstName: 'Luigi',
        lastName: 'Doge',
        email: 'luigi@surfline.com',
        isEmailVerified: false,
      },
      entitlements: ['sl_metered'],
      settings: {
        _id: 'abc123',
        user: 'xyzpdq',
        location: {
          coordinates: [0, 0],
          type: 'Point',
        },
        onboarded: [],
        units: {
          temperature: 'C',
          windSpeed: 'KPH',
          tideHeight: 'M',
          swellHeight: 'M',
          surfHeight: 'M',
        },
      },
      loading: false,
      emailSent: false,
      warnings: [{ type: 'failedPayment' }],
      countryCode: 'US',
      subdivisionCode: 'CA',
    },
    errors: ['some_error'],
    state: {
      shouldRecomputeMeterModal: false,
      meterLoaded: true,
    },
    meter: {
      anonymousId: 'f74d123f-faf3-473a-9712-465dae98cd5d',
      treatmentName: 'sl_rate_limiting_v3',
      treatmentState: 'on',
      treatmentVariant: 'surf_check',
      meterLimit: 0,
      meterRemaining: -1,
    },
  },
};
