import { SL_PREMIUM, SL_FREE_TRIAL, ANONYMOUS, IS_PREMIUM, IS_BASIC, SL_BASIC } from '../constants';

/**
 * @typedef {object} UserDetails
 * @property {string} _id
 * @property {string} email
 * @property {string} firstName
 * @property {string} lastName
 * @property {boolean} isEmailVerified
 */

/**
 * @typedef {object} Units
 * @property {string} surfHeight
 * @property {string} swellHeight
 * @property {string} tideHeight
 * @property {string} windSpeed
 * @property {"C" | "F"} temperature
 */

/**
 * @typedef {object} RecentlyVisited
 * @property {{ _id: string, name: string }[]} subregions
 * @property {{ _id: string, name: string }[]} spots
 */

/**
 * @typedef {object} UserLocation
 * @property {number} latitude
 * @property {number} longitude
 * @property {string} time_zone
 */

/**
 * @typedef {object} UserSettings
 * @property {Units} units
 * @property {Object} feed
 * @property {string} feed.localized
 * @property {Object} date
 * @property {string} date.format
 * @property {Object} navigation
 * @property {string?} navigation.spotsTaxonomyLocation
 * @property {string?} navigation.forecastsTaxonomyLocation
 * @property {RecentlyVisited} recentlyVisited
 * @property {Object} feedFilters
 * @property {string} feedFilters.subregions
 * @property {string[]} onboarded
 * @property {string[]} boardTypes
 * @property {number} travelDistance
 * @property {string} bwWindWaveModel
 * @property {"SURF" | "SWELL"} preferredWaveChart
 * @property {string?} homeSpot
 * @property {string?} homeForecast
 * @property {boolean?} isSwellChartCollapsed
 * @property {boolean?} receivePromotions
 * @property {string} _id
 * @property {string} user
 * @property {string} abilityLevel
 */

/**
 * @typedef {Object} UserState
 * @property {string[]} entitlements
 * @property {string[]} promotionEntitlements
 * @property {Partial<UserDetails>} details
 * @property {UserSettings} settings
 * @property {{}[]} favorites
 * @property {boolean} emailSent
 * @property {Array<{planType: string, type: string}>} warnings
 * @property {string?} countryCode
 * @property {string} ip
 * @property {UserLocation} location
 */

/**
 * @typedef {import('./').BackplaneState} State
 */

/**
 * @param {State} state
 * @returns {UserState}
 */
export const getUser = (state) => state.backplane?.user;

/**
 * @param {State} state
 * @returns {UserState['details']?}
 */
export const getUserDetails = (state) => state.backplane?.user?.details;

/**
 * @param {State} state
 * @returns {UserState['settings']?}
 */
export const getUserSettings = (state) => state.backplane?.user?.settings;

/**
 * @param {State} state
 */
export const getUserFavorites = (state) => state.backplane?.user?.favorites || [];

/**
 * @param {State} state
 */
export const getEntitlements = (state) => state.backplane?.user?.entitlements || [];

/**
 * @param {State} state
 */
export const getPromotionEntitlements = (state) =>
  state.backplane?.user?.promotionEntitlements || [];

/**
 * @param {State} state
 */
export const getUserLoadingStatus = (state) => state.backplane?.user?.loading;

/**
 * @param {State} state
 */
export const getUserEmailVerified = (state) => state.backplane?.user.details?.isEmailVerified;

/**
 * @param {State} state
 */
export const getUserEmailSentStatus = (state) => state.backplane?.user?.emailSent;

/**
 * @param {State} state
 */
export const getUserPremiumStatus = (state) => {
  const entitlements = getEntitlements(state);
  const isPremium = entitlements.includes(SL_PREMIUM);
  return isPremium;
};

/**
 * @param {State} state
 */
export const getUserFreeTrialEligible = (state) => {
  const promotionEntitlements = getPromotionEntitlements(state);
  return promotionEntitlements.includes(SL_FREE_TRIAL);
};

/**
 * @param {State} state
 */
export const getUserSubscriptionWarnings = (state) => state.backplane?.user?.warnings || [];

/**
 * @param {State} state
 */
export const getUserTypeStatus = (state) => {
  const userDetails = getUserDetails(state);
  if (!userDetails) return ANONYMOUS;
  return getUserPremiumStatus(state) ? IS_PREMIUM : IS_BASIC;
};

/**
 * @param {State} state
 */
export const getUserBasicStatus = (state) => state.backplane?.user?.entitlements.includes(SL_BASIC);
