import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import * as selectors from './metering';

describe('selectors / metering', () => {
  const meter = deepFreeze({
    meterRemaining: 1,
    treatmentState: 'off',
    anonymousId: 'anonymousId',
  });

  const state = deepFreeze({
    backplane: {
      meter,
    },
  });

  const noMeterState = deepFreeze({
    backplane: {},
  });

  it('should get the meter state', () => {
    const meterState = selectors.getMeter(state);
    expect(meterState).to.deep.equal(meter);
  });

  it('should handle a missing meter state', () => {
    const meterState = selectors.getMeter(noMeterState);

    expect(meterState).to.equal(undefined);
  });

  it('should get meterRemaining', () => {
    const meterRemaining = selectors.getMeterRemaining(state);

    expect(meterRemaining).to.equal(meter.meterRemaining);
  });

  it('should get treatmentState', () => {
    const treatmentState = selectors.getMeterTreatmentState(state);

    expect(treatmentState).to.equal(meter.treatmentState);
  });

  it('should get anonymousId', () => {
    const anonymousId = selectors.getMeterAnonymousId(state);

    expect(anonymousId).to.equal(meter.anonymousId);
  });
});
