import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import * as selectors from './user';
import stateFixture from './fixtures/state';
import { IS_PREMIUM, ANONYMOUS, SL_BASIC } from '../constants';

describe('selectors / user', () => {
  const state = deepFreeze(stateFixture);

  const nonPremiumState = deepFreeze({
    ...stateFixture,
    backplane: {
      ...stateFixture.backplane,
      user: {
        ...stateFixture.backplane.user,
        entitlements: [],
        promotionEntitlements: [],
      },
    },
  });

  const emptyState = {
    backplane: {},
  };

  it('gets user', () => {
    const user = selectors.getUser(state);

    expect(user).to.deep.equal(state.backplane.user);
  });

  it('getUser handles empty state', () => {
    const user = selectors.getUser(emptyState);

    expect(user).to.deep.equal(undefined);
  });

  it('gets user favorites', () => {
    const favorites = selectors.getUserFavorites(state);
    const [{ _id, name, rank }] = favorites;

    expect(_id).to.deep.equal('5842041f4e65fad6a77088e4');
    expect(name).to.deep.equal('South Side/13th Street');
    expect(rank).to.deep.equal(0);
  });

  it('gets user favorites - empty state', () => {
    const favorites = selectors.getUserFavorites(emptyState);

    expect(favorites).to.deep.equal([]);
  });

  it('gets user details', () => {
    const { firstName, lastName, email } = selectors.getUserDetails(state);
    expect(firstName).to.deep.equal('Luigi');
    expect(lastName).to.deep.equal('Doge');
    expect(email).to.deep.equal('luigi@surfline.com');
  });

  it('gets user settings', () => {
    const { _id, user, onboarded, units, location } = selectors.getUserSettings(state);
    expect(_id).to.deep.equal('abc123');
    expect(user).to.deep.equal('xyzpdq');
    expect(onboarded).to.deep.equal([]);
    expect(location).to.have.property('coordinates');
    expect(units).to.have.property('surfHeight');
  });

  it('gets user premium entitlements from empty state', () => {
    const entitlements = selectors.getEntitlements(emptyState);
    expect(entitlements).to.deep.equal([]);
  });

  it('gets user premium entitlements from state', () => {
    const entitlements = selectors.getEntitlements(state);
    expect(entitlements[0]).to.deep.equal('sl_no_ads');
    expect(entitlements[1]).to.deep.equal('sl_premium');
  });

  it('gets user promotional entitlements from state', () => {
    const entitlements = selectors.getPromotionEntitlements(state);
    expect(entitlements[0]).to.deep.equal('sl_promotion');
  });

  it('gets user premium status from state', () => {
    const isPremium = selectors.getUserPremiumStatus(state);
    expect(isPremium).to.equal(true);
  });

  it('gets user premium status', () => {
    const isPremium = selectors.getUserPremiumStatus(state, true);
    expect(isPremium).to.equal(true);
  });

  it('get user promotional entitlements handles empty state', () => {
    const entitlements = selectors.getPromotionEntitlements(emptyState);
    expect(entitlements).to.deep.equal([]);
  });

  it('gets user non-premium entitlements from local fixture', () => {
    const entitlements = selectors.getEntitlements(nonPremiumState);
    expect(entitlements).to.not.include('sl_premium');
    expect(entitlements).to.have.length(0);
  });

  it('gets user free trial eligibility', () => {
    const freeTrialEligible = selectors.getUserFreeTrialEligible(state);
    expect(freeTrialEligible).to.equal(false);
  });

  it('gets user loading state', () => {
    const loading = selectors.getUserLoadingStatus(state);
    expect(loading).to.equal(false);
  });

  it('gets user email verified state', () => {
    const verified = selectors.getUserEmailVerified(state);
    expect(verified).to.equal(false);
  });

  it('gets email sent state', () => {
    const sent = selectors.getUserEmailSentStatus(state);
    expect(sent).to.equal(false);
  });

  it('gets subscription warnings state ', () => {
    const warnings = selectors.getUserSubscriptionWarnings(state);
    expect(warnings).to.deep.equal([{ type: 'failedPayment' }]);
  });

  it('gets subscription warnings state from an empty state', () => {
    const warnings = selectors.getUserSubscriptionWarnings(emptyState);
    expect(warnings).to.deep.equal([]);
  });

  it('gets user type from state ', () => {
    const userType = selectors.getUserTypeStatus(state);
    expect(userType).to.equal(IS_PREMIUM);
  });

  it('gets user type from empty state ', () => {
    const userType = selectors.getUserTypeStatus(emptyState);
    expect(userType).to.equal(ANONYMOUS);
  });

  it('checks for a basic user from state ', () => {
    const entitlements = [SL_BASIC];
    const basicState = deepFreeze({
      ...stateFixture,
      backplane: {
        ...stateFixture.backplane,
        user: {
          ...stateFixture.backplane.user,
          entitlements,
        },
      },
    });
    const isBasicUser = selectors.getUserBasicStatus(basicState);
    expect(isBasicUser).to.equal(true);
  });
});
