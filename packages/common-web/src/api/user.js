/* istanbul ignore file */

import baseFetch, { fetchWithAuth } from './baseFetch';

/**
 * @module UserAPI
 * For now some of these functions are a little more complicated than they need to be in order
 * to support passing in a different base URL (services proxy url, internal services proxy url,
 * direct service url) and handling the different needs for headers for the different possible
 * routes.
 *
 * This is to support hitting services directly until we are able to do two things:
 * - create an internal URL for the services proxy, which will reduce roundtrip response time
 *   due to avoiding leaving the VPC.
 * - update the services proxy to check for pre-existing Geo headers to avoid duplicate geo requests
 */

/**
 * @template T
 * @param {import('./').WavetrakFetchArgs[0]} [fetchOptions]
 * @param {import('./').WavetrakFetchArgs[1]} [wavetrakOptions]
 * @returns {Promise<T>}
 */
export const fetchUserDetails = (fetchOptions, wavetrakOptions) =>
  fetchWithAuth('/user?', fetchOptions, wavetrakOptions);

/**
 * @template T
 * @param {import('./').WavetrakFetchArgs[0]} [fetchOptions]
 * @param {import('./').WavetrakFetchArgs[1]} [wavetrakOptions]
 * @returns {Promise<T>}
 */
export const fetchUserSettings = async (fetchOptions, wavetrakOptions) =>
  fetchWithAuth('/user/settings?', fetchOptions, wavetrakOptions);

/**
 * @template T
 * @param {keyof (typeof import('../constants').BRAND_MAP)} brand
 * @param {import('./').WavetrakFetchArgs[1]} [wavetrakOptions]
 * @returns {Promise<T>}
 */
export const putUserSettings = (settings, wavetrakOptions) =>
  fetchWithAuth(
    '/user/settings?',
    {
      method: 'PUT',
      body: JSON.stringify(settings),
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
    },
    wavetrakOptions,
  );

/**
 * @template T
 * @param {import('./').WavetrakFetchArgs[0]} [fetchOptions]
 * @param {import('./').WavetrakFetchArgs[1]} [wavetrakOptions]
 * @returns {Promise<T>}
 */
export const fetchUserEntitlements = (fetchOptions, wavetrakOptions) =>
  fetchWithAuth('/entitlements?', fetchOptions, wavetrakOptions);

/**
 * @template T
 * @param {import('./').WavetrakFetchArgs[0]} [fetchOptions]
 * @param {import('./').WavetrakFetchArgs[1]} [wavetrakOptions]
 * @param {boolean} [sevenRatings]
 * @returns {Promise<T>}
 */
export const fetchUserFavorites = async (fetchOptions, wavetrakOptions) =>
  fetchWithAuth('/favorites?', fetchOptions, wavetrakOptions);

/**
 * @template T
 * @param {keyof (typeof import('../constants').BRAND_MAP)} brand
 * @param {import('./').WavetrakFetchArgs[1]} [wavetrakOptions]
 * @returns {Promise<T>}
 */
export const postResendEmailVerification = (brand, wavetrakOptions) =>
  fetchWithAuth(
    '/user/resend-email-verification?',
    {
      method: 'POST',
      body: JSON.stringify({ brand }),
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
    },
    wavetrakOptions,
  );

/**
 * @template T
 * @param {{ type: 'spots' | 'subregions', _id: string, name: string }} params
 * @param {import('./').WavetrakFetchArgs[1]} [wavetrakOptions]
 * @returns {Promise<T>}
 */
export const postRecentlyVisited = ({ _id, type, name }, wavetrakOptions) =>
  fetchWithAuth(
    '/user/settings/recentlyvisited?',
    {
      method: 'POST',
      body: JSON.stringify({ _id, type, name }),
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
    },
    wavetrakOptions,
  );

/**
 * @template T
 * @param {string} spotId
 * @param {import('./').WavetrakFetchArgs[1]} [wavetrakOptions]
 * @returns {Promise<T>}
 */
export const postUserFavorite = (spotId, wavetrakOptions) =>
  fetchWithAuth(
    '/favorites/?',
    {
      method: 'POST',
      body: JSON.stringify({ spotIds: [spotId], type: 'spots' }),
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
    },
    wavetrakOptions,
  );

/**
 * @template T
 * @param {string} spotId
 * @param {import('./').WavetrakFetchArgs[1]} [wavetrakOptions]
 * @returns {Promise<T>}
 */
export const deleteUserFavorite = (spotId, wavetrakOptions) =>
  fetchWithAuth(
    '/favorites/?',
    {
      method: 'DELETE',
      body: JSON.stringify({ spotIds: [spotId], type: 'spots' }),
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
    },
    wavetrakOptions,
  );

/**
 * @template T
 * @param {string} baseUrl
 * @param {string} accessToken
 * @returns {Promise<T>}
 */
export const authenticateAccessToken = (baseUrl, accessToken) =>
  baseFetch(
    `${baseUrl}/trusted/authorise?access_token=${accessToken}`,
    {
      method: 'GET',
    },
    // We don't need to add the access token added by baseFetch
    // because this endpoint uses the one above
    { addAccessToken: false },
  );

/**
 * @template T
 * @param {string} baseUrl
 * @param {string} refreshToken
 * @param {string} clientId
 * @param {string} clientSecret
 * @returns {Promise<T>}
 */
export const refreshAccessToken = async (baseUrl, refreshToken, clientId, clientSecret) =>
  baseFetch(`${baseUrl}/trusted/token/`, {
    method: 'POST',
    headers: {
      Authorization: `Basic ${Buffer.from(`${clientId}:${clientSecret}`).toString('base64')}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: `grant_type=refresh_token&refresh_token=${refreshToken}`,
  });

/**
 * @template T
 * @param {string} baseUrl
 * @param {import('./').WavetrakFetchArgs[1]} [wavetrakOptions]
 * @returns {Promise<T>}
 */
export const getUserRegion = async (baseUrl, wavetrakOptions) =>
  baseFetch(
    `${baseUrl}/geo-target/region`,
    { method: 'GET' },
    { addAccessToken: false, ...wavetrakOptions },
  );
