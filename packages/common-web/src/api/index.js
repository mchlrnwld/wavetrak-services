export { default as renderFetch } from './backplane';
export {
  doExternalFetch,
  createParamString,
  default as baseFetch,
  fetchWithAuth,
} from './baseFetch';
export * from './user';
export * from './subscriptions';
export * from './meter';

/**
 * @typedef {Object} ServerSideFetchParams
 * @property {string?} userId
 * @property {string?} clientIp
 * @property {{longitude: number | string, latitude: number | string }?} location
 * @property {string?} countryIso
 * @property {{}?} cookies
 */

/**
 * @typedef {Object} WavetrakOptions
 * @property {({} | string)} [serverSideCookies]
 * @property {string} [customUrl]
 * @property {boolean} [useAccessTokenQueryParam]
 * @property {boolean} [addAccessToken]
 * @property {Record<string, string>} [query]
 */

/**
 * @typedef {[RequestInit, WavetrakOptions]} WavetrakFetchArgs
 */
