/* istanbul ignore file */

import { fetchWithAuth } from './baseFetch';

/**
 * @template T
 * @param {string} baseUrl
 * @param {import('./').WavetrakFetchArgs[1]} [wavetrakOptions]
 * @returns {Promise<T>}
 */
export const getSubscription = async (wavetrakOptions) =>
  fetchWithAuth('/subscriptions?', { method: 'GET' }, wavetrakOptions);
