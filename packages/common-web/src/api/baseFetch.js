/* istanbul ignore file */
import fetch from 'isomorphic-fetch';
import Cookies from 'universal-cookie';
import canUseDOM from '../canUseDOM';
import doCookiesWarning from '../utils/doCookiesWarning';
import getProxyUrl from '../utils/getProxyUrl';

/**
 * @typedef {import('./').WavetrakOptions} WavetrakOptions
 */

/**
 * @description Given a single level object, creates a param string
 * @param {{[key: string]: string | number | boolean}} params
 * @returns {string}
 */
export const createParamString = (params) =>
  Object.keys(params)
    .filter((key) => params[key] !== undefined && params[key] !== null)
    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join('&');

/**
 * @param {import('express')} res
 * @param {string} path
 * @returns {Response}
 * @throws {{statusCode: number, path: string, res: Response} & Error}
 */
const checkStatus = (res, path) => {
  if (res.status >= 200 && res.status < 300) return res;
  const error = new Error(res.statusText);
  error.response = res;
  error.statusCode = res.status;
  error.path = path;
  throw error;
};

/**
 * @template T
 * @param {Response} response
 * @returns {Promise<T>}
 */
const parseJSON = (response) => response.json();

/**
 * @description Perform a Wavetrak Specific fetch with Authentication Headers added
 * @template T
 * @param {string} path
 * @param {RequestInit} fetchOptions
 * @param {Omit<WavetrakOptions, "addAccessToken">} [wavetrakOptions]
 * @returns {Promise<T>}
 */
export const fetchWithAuth = async (path, fetchOptions = {}, wavetrakOptions = {}) => {
  const { customUrl, serverSideCookies, useAccessTokenQueryParam } = wavetrakOptions;

  const servicesUrl = getProxyUrl();
  const baseUrl = customUrl || servicesUrl;

  let url = `${baseUrl}${path}`;

  doCookiesWarning(url, serverSideCookies);
  const cookies = new Cookies(canUseDOM ? undefined : serverSideCookies);
  const accessToken = cookies.get('access_token');

  const accessTokenHeader = accessToken &&
    !useAccessTokenQueryParam && { 'X-Auth-AccessToken': accessToken };

  // The `X-Auth-AccessToken` header currently causes CORS errors in
  // some services which hinders development, so we want to allow
  // overriding this until we can fix all the services
  if (accessToken && useAccessTokenQueryParam) {
    const hasQueryParams = url.indexOf('?') > -1;
    url += `${hasQueryParams ? '&' : '?'}accesstoken=${accessToken}`;
  }

  if (wavetrakOptions.query) {
    const queryString = createParamString(wavetrakOptions.query);
    url += `&${queryString}`;
  }

  const response = await fetch(url, {
    ...fetchOptions,
    headers: new Headers({
      ...fetchOptions?.headers,
      ...accessTokenHeader,
    }),
  });

  checkStatus(response, path);
  return parseJSON(response);
};

/**
 * @description Base Fetch is a wavetrak helper function for performing
 * wavetrak related fetches. It currently uses the `accesstoken` query
 * param in order to avoid CORS errors. In future versions `fetchWithAuth`
 * will be preferred for authenticated fetches
 * @template T
 * @param {string} path
 * @param {RequestInit} [fetchOptions]
 * @param {Pick<WavetrakOptions, "serverSideCookies" | "addAccessToken">}
 * @returns {Promise<T>}
 */
const baseFetch = async (
  path,
  fetchOptions = {},
  { serverSideCookies = null, addAccessToken = true } = {},
) => {
  doCookiesWarning(path, serverSideCookies);

  const cookies = new Cookies(canUseDOM ? undefined : serverSideCookies);
  const at = cookies.get('access_token');

  if (at && addAccessToken) {
    const hasQueryParams = path.indexOf('?') > -1;
    // eslint-disable-next-line no-param-reassign
    path += `${hasQueryParams ? '&' : '?'}accesstoken=${at}`;
  }

  const response = await fetch(path, fetchOptions);

  checkStatus(response, path);
  return parseJSON(response);
};

/**
 * @template T
 * @description Perform an external fetch and parse the status code and body
 * @param {string} url
 * @param {RequestInit} options
 * @returns {Promise<T>}
 */
export const doExternalFetch = (url, options) =>
  fetch(url, { ...options })
    .then(checkStatus)
    .then(parseJSON);

export default baseFetch;
