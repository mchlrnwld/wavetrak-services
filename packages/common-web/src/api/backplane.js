/* istanbul ignore file */
import fetch from 'isomorphic-fetch';

/**
 *
 * @typedef {{[key: string]: string | boolean | number}} Params
 */

/**
 * @func createParamString
 * @description Given an object with keys and values, creates a param string.
 * @param {Params} params
 * @returns {string}
 */
const createParamString = (params) =>
  Object.keys(params)
    .filter((key) => params[key] !== undefined)
    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join('&');

/**
 * @template T
 * @param {string} baseServicePath
 * @param {string} clientIp
 * @param {Params} opts
 * @returns {Promise<T>}
 */
const renderFetch = async (baseServicePath, clientIp, opts) => {
  const resp = await fetch(`${baseServicePath}/render.json?${createParamString(opts)}`, {
    headers: {
      'x-client-ip': clientIp,
    },
  });
  const body = await resp.json();
  if (resp.status === 200) return body;
  throw body;
};

export default renderFetch;
