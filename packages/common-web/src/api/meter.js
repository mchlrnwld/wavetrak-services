/* istanbul ignore file */
import getProxyUrl from '../utils/getProxyUrl';
import baseFetch from './baseFetch';

/**
 * @param {string} anonymousId
 * @return {Promise<{ meterRemaining: number }>}
 */
export const incrementAnonymousMeter = (anonymousId) => {
  const servicesURL = getProxyUrl();

  return baseFetch(`${servicesURL}/meter/anonymous/increment/${anonymousId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
  });
};
