import { getWindow } from '../getWindow';

/**
 * Returns the Braze Appboy if available
 */
const getAppboy = () => getWindow()?.appboy;

export default getAppboy;
