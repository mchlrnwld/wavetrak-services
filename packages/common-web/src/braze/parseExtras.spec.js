import { expect } from 'chai';
import parseExtras from './parseExtras';

describe('parseExtras', () => {
  it('should parse extras correctly based on a prefix', () => {
    const card = {
      extras: {
        href__url: '/',
        web__href__url: '/upgrade',
        css__color: '#fff',
        css__padding: '6px 8px',
        color: '#000',
      },
    };

    const web = parseExtras(card.extras, 'web__');
    const common = parseExtras(card.extras, 'common__');
    const css = parseExtras(card.extras, 'css__');

    expect(web).to.eql({ href__url: '/upgrade' });
    expect(common).to.eql({});
    expect(css).to.eql({ color: '#fff', padding: '6px 8px' });
  });

  it('should return an empty object for a card with no extras', () => {
    const card = {};
    const web = parseExtras(card.extras, 'web__');
    expect(web).to.eql({});
  });
});
