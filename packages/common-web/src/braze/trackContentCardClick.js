import getAppboy from './getAppboy';

/**
 * Function to track when a Braze content card is clicked.
 *
 * https://js.appboycdn.com/web-sdk/latest/doc/modules/appboy.html#logcardclick
 * @param {object} contentCard The card that was clicked
 * @returns {boolean} success
 */
const trackContentCardClick = (card) => {
  const foundCard = getAppboy()
    ?.getCachedContentCards()
    ?.cards?.find(({ id }) => id === card?.id);

  return foundCard && getAppboy()?.logCardClick(foundCard, true);
};

export default trackContentCardClick;
