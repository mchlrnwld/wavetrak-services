import getAppboy from './getAppboy';
/**
 * Dismisses all cards with matching title. Dismissed cards are no longer shown to the user.
 * Note that this function triggers a Braze dismissal action for the card.
https://js.appboycdn.com/web-sdk/latest/doc/modules/appboy.html#logcarddismissal
 * @param {string} contentCard
 */
const dismissContentCard = (contentCard) => {
  getAppboy()
    ?.getCachedContentCards()
    ?.cards?.forEach((card) => {
      if (card.title === contentCard) {
        getAppboy()?.logCardDismissal(card);
      }
    });
};

export default dismissContentCard;
