/**
 * Maps Braze card extras starting with {prefix}
 * to a new object while removing the prefix from the key.
 * E.g. { web__href__url: '/upgrade', href__text: 'link text' }
 * will be mapped to:
 * { href__url: '/upgrade' } when passing in the 'web__' prefix.
 * @param {Object} extras card.extras
 * @param {string} prefix prefix to filter KVPs
 */
const parseExtras = (extras, prefix) => {
  const parsedExtras = {};
  if (extras) {
    Object.keys(extras).forEach((key) => {
      if (key.startsWith(prefix)) {
        parsedExtras[key.substring(prefix.length)] = extras[key];
      }
    });
  }
  return parsedExtras;
};

export default parseExtras;
