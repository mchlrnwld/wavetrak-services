import getAppboy from './getAppboy';
/**
 * Requests an immediate refresh of content cards from Appboy servers.
 * https://js.appboycdn.com/web-sdk/latest/doc/modules/appboy.html#requestcontentcardsrefresh
 */
const refreshContentCards = () => {
  getAppboy()?.requestContentCardsRefresh();
};

export default refreshContentCards;
