import getAppboy from './getAppboy';
/**
 * Removes all content cards for the user. This should not trigger a dismissal analytics event
 * https://js.appboycdn.com/web-sdk/latest/doc/modules/appboy.html#logcarddismissal
 */
const clearContentCards = () => {
  getAppboy()
    ?.getCachedContentCards()
    ?.cards?.forEach((card) => {
      card.dismissCard();
    });
};

export default clearContentCards;
