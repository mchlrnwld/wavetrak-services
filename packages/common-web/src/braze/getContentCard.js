import getAppboy from './getAppboy';
import parseExtras from './parseExtras';

/**
 * @param {string} contentCard Title of ContentCard
 * @returns ContentCard if found
 */
const getContentCard = (contentCard) => {
  // already sorted by newest
  const card = getAppboy()
    ?.getCachedContentCards()
    ?.cards?.find((c) => c.title === contentCard);

  if (!card) return null;

  const common = parseExtras(card.extras, 'common__');
  const web = parseExtras(card.extras, 'web__');
  const css = parseExtras(card.extras, 'css__');

  return {
    name: contentCard,
    ...card,
    extras: {
      ...common,
      ...web,
      css,
    },
  };
};

export default getContentCard;
