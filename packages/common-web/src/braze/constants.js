export const BrazeEvents = {
  BRAZE_LOADED: 'BRAZE_LOADED',
  BRAZE_TIMEOUT: 'BRAZE_TIMEOUT',
};

export default { BrazeEvents };
