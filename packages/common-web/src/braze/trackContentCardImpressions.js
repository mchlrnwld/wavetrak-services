import getAppboy from './getAppboy';

/**
 * Function to track when a Braze content card is shown to the user.
 *
 * https://js.appboycdn.com/web-sdk/latest/doc/modules/appboy.html#logcardimpressions
 * @param {[object]} cards The cards shown to the user
 * @returns {boolean} Success
 */
const trackContentCardImpressions = (cards) => {
  const foundCards = cards?.map((card) =>
    getAppboy()
      ?.getCachedContentCards()
      ?.cards?.find(({ id }) => id === card?.id),
  );

  return foundCards && getAppboy()?.logCardImpressions(foundCards, true);
};

export default trackContentCardImpressions;
