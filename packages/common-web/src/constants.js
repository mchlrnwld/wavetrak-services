/* instanbul ignore file */

export const MOBILE_WIDTH = 512;
export const TABLET_WIDTH = 769;
export const TABLET_LARGE_WIDTH = 976;
export const SMALL_DESKTOP_WIDTH = 992;

export const SWELL = 'SWELL';
export const SURF = 'SURF';
export const TIDE = 'TIDE';
export const WIND = 'WIND';
export const WEATHER = 'WEATHER';

/* Brands */
export const SL = 'sl';
export const BW = 'bw';
export const FS = 'fs';
export const CW = 'cw';
export const WT = 'wt';
export const SURFLINE = 'Surfline';
export const BUOYWEATHER = 'Buoyweather';
export const FISHTRACK = 'FishTrack';
export const COASTALWATCH = 'CoastalWatch';
export const WAVETRAK = 'Wavetrak';

export const BRAND_MAP = {
  [SL]: SURFLINE,
  [BW]: BUOYWEATHER,
  [FS]: FISHTRACK,
  [WT]: WAVETRAK,
  [CW]: COASTALWATCH,
};
export const BRAND_ABBR_MAP = {
  [SURFLINE]: SL,
  [BUOYWEATHER]: BW,
  [FISHTRACK]: FS,
  [WAVETRAK]: WT,
  [COASTALWATCH]: CW,
};

/* Subscriptions */
export const STRIPE = 'stripe';
export const FREE = 'free';
export const MONTHLY = 'monthly';
export const ANNUAL = 'annual';
export const GIFT = 'gift';
export const YEAR = 'year';
export const MONTH = 'month';
export const SL_PREMIUM = 'sl_premium';
export const SL_METERED = 'sl_metered';
export const SL_PROMOTIONAL = 'sl_promotional';
export const SL_FREE_TRIAL = 'sl_free_trial';
export const SL_VIP = 'sl_vip';
export const SL_BASIC = 'sl_basic';
export const APPLE = 'apple';
export const GOOGLE = 'google';
export const VIP = 'vip';
export const LOGGED_IN = 'logged_in';
export const ANONYMOUS = 'anonymous';
export const IS_PREMIUM = 'isPremium';
export const IS_BASIC = 'isBasic';

/* wave heights */
export const FEET = 'FT';
export const HAWAIIAN = 'HI';
export const METERS = 'M';

export const PROXY_URL = {
  development: 'https://services.sandbox.surfline.com',
  sandbox: 'https://services.sandbox.surfline.com',
  staging: 'https://services.staging.surfline.com',
  production: 'https://services.surfline.com',
};

/* time constants */
export const SECONDS_PER_HOUR = 60 * 60;
export const SECONDS_PER_DAY = 86400;

/* token expiration times in seconds */
export const ACCESS_TOKEN_AGE = 30 * SECONDS_PER_DAY;
export const REFRESH_TOKEN_AGE = 365 * SECONDS_PER_DAY;
