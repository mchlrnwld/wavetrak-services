import { expect } from 'chai';

import getClientIp from './getClientIp';

describe('getClientIp', () => {
  it('should return a normalized address if it is IPV6', () => {
    const req = {
      headers: {
        'x-forwarded-for': '::1',
      },
    };
    const ip = getClientIp(req);
    expect(ip).to.be.equal('8.8.8.8');
  });
  it('should return an IP from req.connection.remoteAddress as a fallback', () => {
    const req = {
      headers: {},
      connection: {
        remoteAddress: '1.2.3.4,1',
      },
    };
    const ip = getClientIp(req);
    expect(ip).to.be.equal('1.2.3.4');
  });
});
