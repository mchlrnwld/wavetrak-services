/* eslint-disable no-undef */
import { getCanUseDOM } from './canUseDOM';

/**
 * Safe way to interface with the window
 * @example
 * const hostname = getWindow()?.location.hostname ?? null;
 * @returns window | null
 */
export const getWindow = () => (getCanUseDOM() ? window : null);

export default getWindow;
