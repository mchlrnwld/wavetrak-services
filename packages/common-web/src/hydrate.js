/* istanbul ignore file */

import {
  FETCH_USER_FAVORITES_SUCCESS,
  FETCH_USER_SETTINGS_SUCCESS,
  FETCH_USER_DETAILS_SUCCESS,
  FETCH_USER_ENTITLEMENTS_SUCCESS,
} from './actions/user';

/**
 * @description Gathers action creators from a `Component`'s static property `fetch` and
 * returns a promise. Resolving the promise  dispatches each action with the
 * matched route params object.
 * @param {import('./features/redux').WavetrakStore} store dispatches the action creators
 * @param {{ components: [{rootContainer?: any}], params: unknown }} renderProps used to destructure component tree and matched route params
 * @param {import('express').Request} req the express req object. be careful passing too much of this into state
 * @param {object} backplane the backplane `api` object.
 * @param {'v1' | 'v2'} [backplane.apiVersion]
 * @returns {Promise} component tree actions creators to dispatch
 */
const hydrate = async (store, { components, params }, req, backplane) => {
  const componentsWithFetch = components
    .filter((comp) => !comp.rootContainer)
    .reduce((prev, current = {}) => (current.fetch || []).concat(prev), []);

  /** @deprecated API Version 1 is deprecated, but we want backwards compatibility */
  if (backplane.apiVersion !== 'v2') {
    store.dispatch({
      type: FETCH_USER_FAVORITES_SUCCESS,
      favorites: backplane.favorites?.data.favorites || [],
    });

    store.dispatch({
      type: FETCH_USER_SETTINGS_SUCCESS,
      settings: backplane.settings,
    });

    if (backplane.details) {
      store.dispatch({
        type: FETCH_USER_DETAILS_SUCCESS,
        details: backplane.details,
      });
    }

    if (backplane.entitlements) {
      store.dispatch({
        type: FETCH_USER_ENTITLEMENTS_SUCCESS,
        entitlements: backplane.entitlements?.entitlements || [],
      });
    }
  }

  return Promise.all(componentsWithFetch.map((doFetch) => store.dispatch(doFetch(params, req))));
};

export default hydrate;
