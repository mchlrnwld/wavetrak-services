/* istanbul ignore file */

/**
 * @param {{
 *  url?: string,
 *  headline?: string
 *  image?: string
 *  description?: string
 *  author?: string
 *  authorType?: string
 *  publisher?: string
 *  cssSelector?: string
 *  type?: string
 *  dateModified?: string | Date
 *  isAccessibleForFree?: boolean
 * }} options
 * @param {object} [extension={}]
 */
export const pageStructuredData = (
  {
    url,
    headline,
    image,
    description,
    author,
    authorType,
    publisher,
    cssSelector,
    dateModified,
    type,
    isAccessibleForFree,
  },
  extension = {},
) => {
  const schema = {
    '@context': 'https://schema.org',
    '@type': type || 'WebPage',
    isAccessibleForFree,
    ...(url && {
      mainEntityOfPage: {
        '@type': 'WebPage',
        '@id': url,
      },
    }),
    ...(headline && { headline }),
    ...(author && {
      author: {
        '@type': authorType || 'Person',
        name: author,
      },
    }),
    ...(publisher && {
      publisher: {
        name: publisher,
      },
    }),
    ...(dateModified && { dateModified }),
    ...(description && { description }),
    ...(image && { image }),
    ...(cssSelector && {
      hasPart: {
        '@type': 'WebPageElement',
        isAccessibleForFree: 'False',
        cssSelector,
      },
    }),
    ...extension,
  };

  return JSON.stringify(schema, null, 2);
};

/**
 * @param {string} name
 * @param {string} spotName
 * @param {string} description
 * @param {string} thumbnailUrl
 * @param {string | Date} uploadDate
 * @param {string | Date} startDate
 * @param {string | Date} endDate
 * @param {boolean} [isLive=true]
 * @param {{}} [extension={}]
 */
export const videoStructuredData = (
  name,
  spotName,
  description,
  thumbnailUrl,
  uploadDate,
  startDate,
  endDate,
  contentUrl,
  isLive = true,
  extension = {},
) => {
  const schema = {
    '@context': 'http://schema.org/',
    '@type': 'VideoObject',
    description,
    thumbnailUrl,
    uploadDate,
    name,
    contentUrl,
    publication: {
      '@type': 'BroadcastEvent',
      name: `${isLive ? 'Live' : ''} ${name}`,
      isLiveBroadcast: true,
      startDate,
      endDate,
      location: {
        '@type': 'Beach',
        name: spotName,
      },
    },
    ...extension,
  };

  return JSON.stringify(schema, null, 2);
};

/**
 * @param {string} name
 * @param {number} latitude
 * @param {number} longitude
 */
export const placeStructuredData = (name, latitude, longitude) => {
  const schema = {
    '@context': 'http://schema.org/',
    '@type': 'Place',
    name,
    geo: {
      '@type': 'GeoCoordinates',
      latitude,
      longitude,
    },
  };

  return JSON.stringify(schema, null, 2);
};
