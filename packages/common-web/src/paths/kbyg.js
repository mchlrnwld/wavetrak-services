import slugify from '../slugify';

/**
 * @description Given a name and id, creates a path to a spot report page.
 * @param {string} name Name or slug for the spot report
 * @param {*} id _id of the spot
 * @param {*} [shouldSlugify=false] optional parameter to slugify the `name` that is passed in
 */
const spotReportPath = (name, id, shouldSlugify = false) =>
  `/surf-report/${shouldSlugify ? slugify(name) : name}/${id}`;

/**
 * @description Given a slug and ID, returns the path to the subregion forecast
 * @param {string} slug
 * @param {string} id
 * @returns {string}
 */
const subregionForecastPath = (slug, id) => `/surf-forecasts/${slug}/${id}`;

/**
 * @description Given a slug and ID, returns the path to the subregion charts
 * @param {string} slug
 * @param {string} id
 * @returns {string}
 */
const subregionForecastChartsPath = (slug, id) => `/surf-forecasts/${slug}/${id}/charts`;

/**
 * @description Given a slug and ID, returns the path to the subregion premium analysis
 * @param {string} slug
 * @param {string} id
 * @returns {string}
 */
const subregionPremiumAnalysisPath = (slug, id) => `/surf-forecasts/${slug}/${id}/premium-analysis`;

/**
 * @description Given a lat, lon, and zoom returns the path to the map page
 * @param {Object} coords
 * @param {string | number} coords.lat
 * @param {string | number} coords.lon
 * @param {string | number} zoom
 * @returns {string}
 */
const mapPath = ({ lat, lon }, zoom) => `surf-reports-forecasts-cams-map/@${lat},${lon},${zoom}z`;

/**
 * @description Given a slug and GeonameID, returns the path to the geoname map
 * @param {string} slug
 * @param {string} geonameId
 * @returns {string}
 */
const geonameMapPath = (slug, geonameId) => `surf-reports-forecasts-cams/${slug}/${geonameId}`;

/**
 * @description Given a Spot ID, returns the path to the cam rewind page
 * @param {string} spotId
 * @returns {string}
 */
const camRewindPath = (spotId) =>
  `https://www.surfline.com/surfdata/video-rewind/video_rewind.cfm?id=${spotId}`;

/**
 * @description Given a Cam Alias, returns the path to the cam rewind page
 * @param {string} camAlias
 * @returns {string}
 */
const camRewindPathByAlias = (camAlias) =>
  `https://www.surfline.com/surfdata/video-rewind/video_rewind.cfm?camAlias=${camAlias}`;

export default {
  spotReportPath,
  subregionForecastPath,
  subregionForecastChartsPath,
  subregionPremiumAnalysisPath,
  mapPath,
  geonameMapPath,
  camRewindPath,
  camRewindPathByAlias,
};
