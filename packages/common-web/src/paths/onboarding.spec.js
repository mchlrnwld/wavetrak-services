import { expect } from 'chai';

import onboardingPaths from './onboarding';

describe('Account Paths', () => {
  it('returs the correct paths', () => {
    expect(onboardingPaths).to.deep.equal({
      addFavoritesPath: '/setup/favorite-surf-spots',
      addFavoritesWithCamsPath: '/setup/favorite-surf-spots/cams',
    });
  });
});
