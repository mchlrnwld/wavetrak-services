/* istanbul ignore file */

const addFavoritesPath = '/setup/favorite-surf-spots';
const addFavoritesWithCamsPath = '/setup/favorite-surf-spots/cams';

export default { addFavoritesPath, addFavoritesWithCamsPath };
