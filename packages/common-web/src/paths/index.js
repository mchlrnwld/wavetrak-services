/* istanbul ignore file */

import kbygPaths from './kbyg';
import accountPaths from './account';
import onboardingPaths from './onboarding';
import legacyPaths from './legacy';

export { kbygPaths, accountPaths, onboardingPaths, legacyPaths };
