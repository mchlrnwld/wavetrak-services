const funnelPath = '/upgrade';
const signInPath = '/sign-in';
const createAccountPath = '/create-account';
const premiumPerksPath = '/premium-perks';
const accountPath = '/account';
const premiumBenefitsPath = '/premium-benefits';
const giftCardsPath = '/gift-cards';

export default {
  funnelPath,
  signInPath,
  createAccountPath,
  premiumPerksPath,
  accountPath,
  premiumBenefitsPath,
  giftCardsPath,
};
