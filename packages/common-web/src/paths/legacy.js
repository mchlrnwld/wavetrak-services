/**
 * @deprecated We are moving off coldfusion, once the transition is complete remove this
 * @description Given a legacy ID, returns a link to the coldfusion ideal conditions
 * @param {string} legacyId
 * @returns {string}
 */
const idealConditionsUrl = (legacyId) =>
  `http://www.surfline.com/surfdata/report_breakdata_new.cfm?id=${legacyId}`;

/**
 * @deprecated We are moving off coldfusion, once the transition is complete remove this
 * @description Given a legacy Subregion ID, returns a link to the coldfusion buoy page
 * @param {string} legacySubregionId
 * @returns {string}
 */
const buoySubregionUrl = (legacySubregionId) =>
  `http://www.surfline.com/surfdata/report_summary_new.cfm?id=${legacySubregionId}&tab=2`;

/**
 * @deprecated We are moving off coldfusion, once the transition is complete remove this
 * @description Given a tide location and units to display in, returns a link
 * to the coldfusion tide predictions page
 * @param {string} tideLocation
 * @param {"FT"| "M"} units
 * @returns {string}
 */
const tidePredictionUrl = (tideLocation, units) =>
  `http://www.surfline.com/surfdata/inc_spot/window_tide_location.cfm?location=${tideLocation}&units=${units}`;

export default {
  idealConditionsUrl,
  buoySubregionUrl,
  tidePredictionUrl,
};
