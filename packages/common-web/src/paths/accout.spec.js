import { expect } from 'chai';

import accountPaths from './account';

describe('Account Paths', () => {
  it('returs the correct paths', () => {
    expect(accountPaths).to.deep.equal({
      funnelPath: '/upgrade',
      signInPath: '/sign-in',
      createAccountPath: '/create-account',
      premiumPerksPath: '/premium-perks',
      accountPath: '/account',
      premiumBenefitsPath: '/premium-benefits',
      giftCardsPath: '/gift-cards',
    });
  });
});
