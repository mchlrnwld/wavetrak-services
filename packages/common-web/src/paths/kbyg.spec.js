import { expect } from 'chai';

import paths from './kbyg';

describe('KBYG Paths', () => {
  it('returs the correct paths', () => {
    expect(paths.spotReportPath('slug1', 'id')).to.equal('/surf-report/slug1/id');
    expect(paths.spotReportPath('HB Pier Southside', 'id', true)).to.equal(
      '/surf-report/hb-pier-southside/id',
    );
    expect(paths.subregionForecastPath('slug', 'id')).to.equal('/surf-forecasts/slug/id');
    expect(paths.subregionForecastChartsPath('slug1', 'id')).to.equal(
      '/surf-forecasts/slug1/id/charts',
    );
    expect(paths.subregionPremiumAnalysisPath('slug1', 'id')).to.equal(
      '/surf-forecasts/slug1/id/premium-analysis',
    );
    expect(paths.mapPath({ lat: 1, lon: 2 }, 1)).to.equal(
      'surf-reports-forecasts-cams-map/@1,2,1z',
    );
    expect(paths.geonameMapPath('slug2', 'somegeoname')).to.equal(
      'surf-reports-forecasts-cams/slug2/somegeoname',
    );
    expect(paths.camRewindPath('123')).to.equal(
      'https://www.surfline.com/surfdata/video-rewind/video_rewind.cfm?id=123',
    );
    expect(paths.camRewindPathByAlias('some-alias')).to.equal(
      'https://www.surfline.com/surfdata/video-rewind/video_rewind.cfm?camAlias=some-alias',
    );
  });
});
