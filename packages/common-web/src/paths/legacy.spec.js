import { expect } from 'chai';

import paths from './legacy';

describe('Legacy Paths', () => {
  it('returs the correct paths', () => {
    expect(paths.buoySubregionUrl('123')).to.equal(
      'http://www.surfline.com/surfdata/report_summary_new.cfm?id=123&tab=2',
    );
    expect(paths.tidePredictionUrl('tideLocation', 'FT')).to.equal(
      'http://www.surfline.com/surfdata/inc_spot/window_tide_location.cfm?location=tideLocation&units=FT',
    );
    expect(paths.idealConditionsUrl('234')).to.equal(
      'http://www.surfline.com/surfdata/report_breakdata_new.cfm?id=234',
    );
  });
});
