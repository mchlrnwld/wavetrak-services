import { terser } from 'rollup-plugin-terser';
import path from 'path';
import babel from 'rollup-plugin-babel';
import replace from 'rollup-plugin-replace';
import resolve from '@rollup/plugin-node-resolve';
import json from '@rollup/plugin-json';
import banner from 'rollup-plugin-license';
import { format } from 'date-fns';

import pkg from './package.json';

function isBareModuleId(id) {
  return !id.startsWith('.') && !id.includes(path.join(process.cwd(), 'src'));
}

const currentDate = format(new Date(), 'yyyy-MM-dd');

const bannerPlugin = banner({
  banner: `${pkg.name} v${pkg.version} ${currentDate}`,
});

const cjs = [
  {
    input: 'src/index.js',
    output: {
      file: `cjs/${pkg.name}.js`,
      sourcemap: true,
      format: 'cjs',
      esModule: false,
    },
    external: isBareModuleId,
    plugins: [
      resolve(),
      json(),
      babel({
        rootMode: 'upward',
        runtimeHelpers: true,
        exclude: /node_modules/,
        sourceMaps: true,
      }),
      replace({
        'process.env.BUILD_FORMAT': JSON.stringify('cjs'),
      }),
      bannerPlugin,
    ],
  },
  {
    input: 'src/index.js',
    output: { file: `cjs/${pkg.name}.min.js`, sourcemap: true, format: 'cjs' },
    external: isBareModuleId,
    plugins: [
      resolve(),
      json(),
      babel({
        rootMode: 'upward',
        runtimeHelpers: true,
        exclude: /node_modules/,
        sourceMaps: true,
      }),
      replace({
        'process.env.BUILD_FORMAT': JSON.stringify('cjs'),
      }),
      terser(),
      bannerPlugin,
    ],
  },
];

const esm = [
  {
    input: 'src/index.js',
    preserveModules: true,
    output: {
      dir: 'esm',
      sourcemap: true,
      format: 'esm',
    },
    external: isBareModuleId,
    plugins: [
      resolve(),
      json(),
      babel({
        exclude: /node_modules/,
        runtimeHelpers: true,
        sourceMaps: true,
        // Disable babel config file since we want to override it anyways
        babelrc: false,
        presets: [['@surfline/babel-preset-web/base', { modules: false }]],
        plugins: ['lodash'],
      }),
      bannerPlugin,
    ],
  },
];

let config;
switch (process.env.BUILD_ENV) {
  case 'cjs':
    config = cjs;
    break;
  case 'esm':
    config = esm;
    break;
  default:
    config = cjs.concat(esm);
}

module.exports = config;
