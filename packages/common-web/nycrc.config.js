const hookRunInThisContextConfig = require('@istanbuljs/nyc-config-hook-run-in-this-context');
const babelConfig = require('@istanbuljs/nyc-config-babel');

module.exports = {
  ...babelConfig,
  ...hookRunInThisContextConfig,
  all: true,
  lines: 75,
  branches: 75,
  statements: 85,
  'check-coverage': true,
  include: ['src/**/*.js'],
  extension: ['.js'],
  exclude: ['**/*.spec.js'],
  reporter: ['lcov', 'text-summary'],
};
