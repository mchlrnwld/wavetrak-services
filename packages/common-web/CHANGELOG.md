# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## <small>14.10.1 (2022-02-14)</small>
* fix: add TikTok to Segment Data Opt-Out ([07af62a](https://github.com/Surfline/wavetrak-services/commit/07af62a))



## [14.10.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/web-common@14.9.3...@surfline/web-common@14.10.0) (2022-02-08)


### Features

* add sevenRatings query param to fetchFavorites ([4b3186a](https://github.com/Surfline/wavetrak-services/commit/4b3186a07e414e46d7bdb3133b7b72b1ab1bbc12))


### Bug Fixes

* lint errors ([6748474](https://github.com/Surfline/wavetrak-services/commit/67484742391d6c115a2051f9a9d340233754f892))


### Chores

* update deps ([06c5670](https://github.com/Surfline/wavetrak-services/commit/06c5670e221653a6467cb97178389ef83818d0c9))



### [14.9.3](https://github.com/Surfline/wavetrak-services/compare/@surfline/web-common@14.9.2...@surfline/web-common@14.9.3) (2022-02-04)


### Chores

* revert timeout on analytics ([c5545c5](https://github.com/Surfline/wavetrak-services/commit/c5545c584467fbc21a6d4c1b3510141d4683ff4c))



### [14.9.2](https://github.com/Surfline/wavetrak-services/compare/@surfline/web-common@14.9.1...@surfline/web-common@14.9.2) (2022-02-03)


### Bug Fixes

* analytics timing out in Next apps ([f1befc1](https://github.com/Surfline/wavetrak-services/commit/f1befc1803d29faa75654033bc79333b8b2e804d))


### Chores

* update lockfile ([024ca06](https://github.com/Surfline/wavetrak-services/commit/024ca068192b9ea14ec82fd9e9a3a7c9be1c2f54))



### [14.9.1](https://github.com/Surfline/wavetrak-services/compare/@surfline/web-common@14.9.0...@surfline/web-common@14.9.1) (2022-02-03)


### Bug Fixes

* token handler not using app env ([8a59423](https://github.com/Surfline/wavetrak-services/commit/8a59423f3d726a188465278dca9bb6220c3913df))



## [14.9.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/web-common@14.8.1...@surfline/web-common@14.9.0) (2022-02-01)


### Features

* add nextjs token handler ([12e1c84](https://github.com/Surfline/wavetrak-services/commit/12e1c84383a1abc42a7d6a7ba6e9af0cf3c4db8a))



### [14.8.1](https://github.com/Surfline/wavetrak-services/compare/@surfline/web-common@14.8.0...@surfline/web-common@14.8.1) (2022-01-21)


### Bug Fixes

* increment meter not recognizing APP_ENV (#6725) ([47bbacf](https://github.com/Surfline/wavetrak-services/commit/47bbacf6c323bff25ce7b944672e7b7b8e538375))



## [14.8.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/web-common@14.7.11...@surfline/web-common@14.8.0) (2021-12-15)


### Features

* add withScriptTag parameters to common script tags ([026a060](https://github.com/Surfline/wavetrak-services/commit/026a06038d66a900ac00f33844a7ba20f9a526d3))



### [14.7.11](https://github.com/Surfline/wavetrak-services/compare/@surfline/web-common@14.7.10...@surfline/web-common@14.7.11) (2021-12-03)


### Bug Fixes

* fix refresh token logic ([bb80756](https://github.com/Surfline/wavetrak-services/commit/bb8075607aae6d28ef6e0bfeb16945ce8a5d8dfc))



### 14.7.10 (2021-10-21)

**Note:** Version bump only for package @surfline/web-common





## <small>14.7.9 (2021-10-19)</small>

* chore(common-web): move surfline-web/common to packages ([7f51049](https://github.com/Surfline/wavetrak-services/commit/7f51049))





### [14.7.8](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.7.5...@surfline/web-common@14.7.8) (2021-10-16)


### Bug Fixes

* number of days incorrect due to DST shift ([d0b3249](https://github.com/Surfline/surfline-web/commit/d0b324910da9b051168a4cad0e9c8e1d75338434))


### Chores

* **deps:** update common dependencies (#6127) ([8d9e4f3](https://github.com/Surfline/surfline-web/commit/8d9e4f3efab12624e3178423972bc48d49196c75))



### [14.7.7](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.7.5...@surfline/web-common@14.7.7) (2021-09-24)


### Bug Fixes

* [[FE-612](https://wavetrak.atlassian.net/browse/FE-612)] Refactor getLocalDateForFutureDay to use date-fns addDays ([ad13ca8](https://github.com/Surfline/surfline-web/commit/ad13ca8d4da5306b5ff65d125a83a0df1f442d81))


### Chores

* **deps:** update common dependencies (#6127) ([8d9e4f3](https://github.com/Surfline/surfline-web/commit/8d9e4f3efab12624e3178423972bc48d49196c75))



### [14.7.6](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.7.5...@surfline/web-common@14.7.6) (2021-09-14)


### Chores

* **deps:** update common dependencies ([e45690b](https://github.com/Surfline/surfline-web/commit/e45690b75929c5651448ec0edd794b4c9bbe3623)), closes [WP-173](https://wavetrak.atlassian.net/browse/WP-173)



### [14.7.5](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.7.4...@surfline/web-common@14.7.5) (2021-08-30)


### Bug Fixes

* [[MED-515](https://wavetrak.atlassian.net/browse/MED-515)] Change Facebook graph endpoint _page for _video ([43aea76](https://github.com/Surfline/surfline-web/commit/43aea7606b1996679715691933645f5acfeae2cd))



### [14.7.4](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.7.3...@surfline/web-common@14.7.4) (2021-08-27)

### Chores

- update README for split docs (#6002) ([492110d](https://github.com/Surfline/surfline-web/commit/492110dc3b868609c74d80e7790f524303b6372c))
- **deps:** bump url-parse from 1.5.1 to 1.5.3 in /common (#6024) ([34f89bc](https://github.com/Surfline/surfline-web/commit/34f89bcdde89a038e46d6fbc9a5efd3317d596ef))



### [14.7.3](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.7.2...@surfline/web-common@14.7.3) (2021-08-04)


### Bug Fixes

* wrong event name for clickedCreateAccountCTA ([4b823c3](https://github.com/Surfline/surfline-web/commit/4b823c38bdabfd290b522a517aefd3c024bdb908))



### [14.7.2](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.7.0...@surfline/web-common@14.7.2) (2021-07-29)


### Bug Fixes

* **structuredData:** remove addressCountry field from placeStructuredData and add publisher to pageStructuredData ([13284b3](https://github.com/Surfline/surfline-web/commit/13284b3aaadb7d16c26b266d1f600fb580d485ad))



## [14.7.1](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.7.0...@surfline/web-common@14.7.1) (2021-07-27)


### Features

* calculate ctaLocation from location property ([6b6fea8](https://github.com/Surfline/surfline-web/commit/6b6fea8c0efcc7ced69141a751f4dba8a3d0f71e))



## [14.7.0](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.5.2...@surfline/web-common@14.7.0) (2021-07-26)


### Features

* **strucutredData:** add placeStructuredData function for geo metadata ([0833fd2](https://github.com/Surfline/surfline-web/commit/0833fd20af131aab9192d496144b942af3653b22)), closes [FE-411](https://wavetrak.atlassian.net/browse/FE-411)


## [14.6.0](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.5.0...@surfline/web-common@14.6.0) (2021-07-02)


### Features

* **eslint:** update eslint and fix lint errors ([748833d](https://github.com/Surfline/surfline-web/commit/748833d6350fa90d8cb1f85a7f0aec5db0955574)), closes [WP-123](https://wavetrak.atlassian.net/browse/WP-123)



### [14.5.2](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.5.0...@surfline/web-common@14.5.2) (2021-07-02)


### Bug Fixes

* change looping logic to avoid appboy sdk violation ([a2edbbb](https://github.com/Surfline/surfline-web/commit/a2edbbbf13403092b1920095f0cf4acb026d14c9))
* optional chain ([ec73cfc](https://github.com/Surfline/surfline-web/commit/ec73cfc086285827a8b084318d477f5b8211f553))
* put dismissContentCard back to former version ([e531bad](https://github.com/Surfline/surfline-web/commit/e531bad51bde21382df3980155da045307f0c429))


### Chores

* lerna ([63a8ca5](https://github.com/Surfline/surfline-web/commit/63a8ca573312f92748a015d345c2801045f5b3db))



### [14.5.1](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.5.0...@surfline/web-common@14.5.1) (2021-07-02)


### Bug Fixes

* change looping logic to avoid appboy sdk violation ([8bd4247](https://github.com/Surfline/surfline-web/commit/8bd4247824f23ad4663fd3051265dc28f2e0991b))
* put dismissContentCard back to former version ([04ad27c](https://github.com/Surfline/surfline-web/commit/04ad27c12ac43b0c77cf8d10419ac626c04aa849))
* **deps:** additional dependency updates ([4deedbf](https://github.com/Surfline/surfline-web/commit/4deedbfea253b47d0ed9abfc0bfdbad28b2903d5))
* **deps:** bump dependencies ([b8991b5](https://github.com/Surfline/surfline-web/commit/b8991b56103ed7830e342fad5b0b2d6fdc0d5082))



## [14.5.0](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.4.0...@surfline/web-common@14.5.0) (2021-07-01)


### Features

* add Braze track functions, update dismiss logic ([774e037](https://github.com/Surfline/surfline-web/commit/774e037f70d5cc97c13d8b70cf16ccdf848e0e88))
* rename trackCard to trackContentCard for new functions, add null check ([c5aad6b](https://github.com/Surfline/surfline-web/commit/c5aad6bca0eb96e5dce6f28ba0863d63306c4a13))


### Bug Fixes

* fix lint errors ([a1c5517](https://github.com/Surfline/surfline-web/commit/a1c5517f6cced6538b2033908ba8faaba5619a4c))
* return name property ([bc8c409](https://github.com/Surfline/surfline-web/commit/bc8c409fd78d678314327ef66875c8679bb2404d))


### Chores

* lerna ([9b28afd](https://github.com/Surfline/surfline-web/commit/9b28afdaf2b410a885be52a7b2e95846c2069bf9))



## [14.4.0](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.3.2...@surfline/web-common@14.4.0) (2021-06-25)


### Features

* **segment:** add subresource integrity to consent manager ([4edf47f](https://github.com/Surfline/surfline-web/commit/4edf47f73d3ec447cf7b27e544b223e9cf2727b4)), closes [WP-89](https://wavetrak.atlassian.net/browse/WP-89)



### [14.3.2](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.1.0...@surfline/web-common@14.3.2) (2021-06-17)


### Bug Fixes

* remove braze functions  from setWavetrakIdentity ([e4057a9](https://github.com/Surfline/surfline-web/commit/e4057a95943cf226933e498a8ab194bee59c9371))



### [14.3.1](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.3.0...@surfline/web-common@14.3.1) (2021-06-14)


### Bug Fixes

* Fix `refreshContentCards` ([c76f649](https://github.com/Surfline/surfline-web/commit/c76f649f0efd8e2921aa0ee45489cdfad2bb1c5f))



## [14.3.0](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.2.0...@surfline/web-common@14.3.0) (2021-06-11)


### Features

* Add `dismissContentCards` and `refreshContentCards` to `braze/` ([e736a5f](https://github.com/Surfline/surfline-web/commit/e736a5fb1a679b08a9e1c95afe04de59066db02f))
* Dismiss and refresh content cards in `setWavetrakIdentity` ([9623cc9](https://github.com/Surfline/surfline-web/commit/9623cc9141cdf353dcf62272a93f3a7754b12317))



## [14.2.0](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.1.0...@surfline/web-common@14.2.0) (2021-06-09)


### Features

* Add parseExtras helper for Braze ([a177af3](https://github.com/Surfline/surfline-web/commit/a177af30040874d6bd323fe601e54ffa8cd2443e))



## [14.1.0](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@14.0.0...@surfline/web-common@14.1.0) (2021-06-04)


### Features

* add getWindow method for safely requesting the window interface ([60890b3](https://github.com/Surfline/surfline-web/commit/60890b3d6082cb447aa01d134629b5e22bb2b927)), closes [WP-52](https://wavetrak.atlassian.net/browse/WP-52)



## [14.0.0](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@13.4.0...@surfline/web-common@14.0.0) (2021-05-28)


### ⚠ BREAKING CHANGES

* Remove all `BrazeContentCards` constants

### Features

* Add `BRAZE_TIMEOUT` constant to `BrazeEvents` ([21f461d](https://github.com/Surfline/surfline-web/commit/21f461d794c2b53517d15a2d8011c9fd00a733dd))



## [13.4.0](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@13.3.3...@surfline/web-common@13.4.0) (2021-05-17)


### Features

* **common:** upgrade dependencies and resolve linting issue ([3899ea9](https://github.com/Surfline/surfline-web/commit/3899ea91e7e200998e6a8c13c75f0772c3525661)), closes [WP-67](https://wavetrak.atlassian.net/browse/WP-67)



### [13.3.3](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@13.3.1...@surfline/web-common@13.3.3) (2021-05-13)

chore: [FE-432] removed isAccessibleForFree default value





## <small>13.3.2 (2021-05-10)</small>

* fix: Fix cookie expiration dates for access / refresh tokens ([2522183](https://github.com/Surfline/surfline-web/commit/2522183))





### [13.3.1](https://github.com/Surfline/surfline-web/compare/@surfline/web-common@13.3.0...@surfline/web-common@13.3.1) (2021-05-10)

**Note:** Version bump only for package @surfline/web-common





## 13.3.0 (2021-05-07)

* chore: update segment function descriptions ([1b659d1](https://github.com/Surfline/surfline-web/commit/1b659d1))
* feat: add trackCampaign from account to common ([cd689dd](https://github.com/Surfline/surfline-web/commit/cd689dd))





## <small>13.2.0 (2021-05-05)</small>

* refactor: move braze utils from backplane to common ([982f480](https://github.com/Surfline/surfline-web/commit/982f480))
* SA-2944: Use refresh tokens in Next apps (#5446) ([1861e97](https://github.com/Surfline/surfline-web/commit/1861e97)), closes [#5446](https://github.com/Surfline/surfline-web/issues/5446)
* WP-29: Docs, Yarn, Docker and Deps cleanup (#5368) ([dba7f4e](https://github.com/Surfline/surfline-web/commit/dba7f4e)), closes [#5368](https://github.com/Surfline/surfline-web/issues/5368)





## 13.1.0 (2021-05-03)

* feat: Add function to get new tokens with a refresh token ([c4ba8cf](https://github.com/Surfline/surfline-web/commit/c4ba8cf))
* WP-29: Docs, Yarn, Docker and Deps cleanup (#5368) ([dba7f4e](https://github.com/Surfline/surfline-web/commit/dba7f4e)), closes [#5368](https://github.com/Surfline/surfline-web/issues/5368)





## <small>13.0.1 (2021-04-28)</small>

* fix: remove `NODE_ENV` replacement during CJS build ([03a7077](https://github.com/Surfline/surfline-web/commit/03a7077))





## 13.0.0 (2021-04-21)

* chore: Remove updateUserForecastModel action ([cc1e908](https://github.com/Surfline/surfline-web/commit/cc1e908))


### BREAKING CHANGE

* Removed `updateUserForecastModel` export




## 12.4.0 (2021-04-15)

* feat: add wavetrakAnalyticsReadyCB ([f0651aa](https://github.com/Surfline/surfline-web/commit/f0651aa))





## 12.3.0 (2021-04-07)

* feat: Add Google Analytics to ConsentManager integrations list ([372c25d](https://github.com/Surfline/surfline-web/commit/372c25d))
* fix: SA-3204 update ccpa copy (#5323) ([c8e673e](https://github.com/Surfline/surfline-web/commit/c8e673e)), closes [#5323](https://github.com/Surfline/surfline-web/issues/5323)
* FE-000: Bump Common Dependencies (#5300) ([cb79e0b](https://github.com/Surfline/surfline-web/commit/cb79e0b)), closes [#5300](https://github.com/Surfline/surfline-web/issues/5300)
* SA-3204: Export ConsentManager scripts (#5309) ([7dffae3](https://github.com/Surfline/surfline-web/commit/7dffae3)), closes [#5309](https://github.com/Surfline/surfline-web/issues/5309)
* SA-3350 - Frontend changes for Anonymous Meter Logic (#5268) ([a985600](https://github.com/Surfline/surfline-web/commit/a985600)), closes [#5268](https://github.com/Surfline/surfline-web/issues/5268)
* SA-3413 - Implement state level Split targeting (#5301) ([75289a0](https://github.com/Surfline/surfline-web/commit/75289a0)), closes [#5301](https://github.com/Surfline/surfline-web/issues/5301)
* chore(deps-dev): bump @babel/core from 7.13.1 to 7.13.14 in /common (#5288) ([ec29dad](https://github.com/Surfline/surfline-web/commit/ec29dad)), closes [#5288](https://github.com/Surfline/surfline-web/issues/5288)
* chore(deps-dev): bump @babel/runtime from 7.13.7 to 7.13.10 in /common (#5218) ([7c0616f](https://github.com/Surfline/surfline-web/commit/7c0616f)), closes [#5218](https://github.com/Surfline/surfline-web/issues/5218)
* chore(deps-dev): bump chai from 4.2.0 to 4.3.4 in /common (#5256) ([8662045](https://github.com/Surfline/surfline-web/commit/8662045)), closes [#5256](https://github.com/Surfline/surfline-web/issues/5256)
* chore(deps-dev): bump core-js from 3.8.3 to 3.9.1 in /common (#5161) ([5a10943](https://github.com/Surfline/surfline-web/commit/5a10943)), closes [#5161](https://github.com/Surfline/surfline-web/issues/5161)
* chore(deps-dev): bump mocha from 8.2.1 to 8.3.2 in /common (#5257) ([e20c5a8](https://github.com/Surfline/surfline-web/commit/e20c5a8)), closes [#5257](https://github.com/Surfline/surfline-web/issues/5257)
* chore(deps-dev): bump rollup from 2.38.5 to 2.42.3 in /common (#5273) ([78e5deb](https://github.com/Surfline/surfline-web/commit/78e5deb)), closes [#5273](https://github.com/Surfline/surfline-web/issues/5273)
* chore(deps): bump @splitsoftware/splitio from 10.15.2 to 10.15.4 in /common (#5272) ([e056731](https://github.com/Surfline/surfline-web/commit/e056731)), closes [#5272](https://github.com/Surfline/surfline-web/issues/5272)
* chore(deps): bump date-fns from 2.16.1 to 2.19.0 in /common (#5219) ([5b66ec8](https://github.com/Surfline/surfline-web/commit/5b66ec8)), closes [#5219](https://github.com/Surfline/surfline-web/issues/5219)
* chore(deps): bump lodash from 4.17.20 to 4.17.21 in /common (#5128) ([178a17e](https://github.com/Surfline/surfline-web/commit/178a17e)), closes [#5128](https://github.com/Surfline/surfline-web/issues/5128)





## <small>12.2.1 (2021-04-07)</small>

* fix: SA-3204 update ccpa copy ([512a78b](https://github.com/Surfline/surfline-web/commit/512a78b))





## 12.2.0 (2021-04-02)

* feat: create and export snippet w/o load ([9b82de9](https://github.com/Surfline/surfline-web/commit/9b82de9))
* feat: Export `consentManagerScript` and `consentManagerConfig` ([9d60331](https://github.com/Surfline/surfline-web/commit/9d60331))
* FE-000: Bump Common Dependencies (#5300) ([cb79e0b](https://github.com/Surfline/surfline-web/commit/cb79e0b)), closes [#5300](https://github.com/Surfline/surfline-web/issues/5300)
* SA-3204: Add default for optional custom category arg ([4887037](https://github.com/Surfline/surfline-web/commit/4887037))
* SA-3204: Export consent manager functions ([83a68f6](https://github.com/Surfline/surfline-web/commit/83a68f6))
* SA-3204: Update consent manager config ([de46851](https://github.com/Surfline/surfline-web/commit/de46851))
* SA-3204: Update Consent Script copy and categories ([b4237da](https://github.com/Surfline/surfline-web/commit/b4237da))
* SA-3204: Updates consent manager categories ([6a0a6ae](https://github.com/Surfline/surfline-web/commit/6a0a6ae))
* SA-3350 - Frontend changes for Anonymous Meter Logic (#5268) ([a985600](https://github.com/Surfline/surfline-web/commit/a985600)), closes [#5268](https://github.com/Surfline/surfline-web/issues/5268)
* SA-3413 - Implement state level Split targeting (#5301) ([75289a0](https://github.com/Surfline/surfline-web/commit/75289a0)), closes [#5301](https://github.com/Surfline/surfline-web/issues/5301)
* chore(deps-dev): bump @babel/core from 7.13.1 to 7.13.14 in /common (#5288) ([ec29dad](https://github.com/Surfline/surfline-web/commit/ec29dad)), closes [#5288](https://github.com/Surfline/surfline-web/issues/5288)
* chore(deps-dev): bump @babel/runtime from 7.13.7 to 7.13.10 in /common (#5218) ([7c0616f](https://github.com/Surfline/surfline-web/commit/7c0616f)), closes [#5218](https://github.com/Surfline/surfline-web/issues/5218)
* chore(deps-dev): bump chai from 4.2.0 to 4.3.4 in /common (#5256) ([8662045](https://github.com/Surfline/surfline-web/commit/8662045)), closes [#5256](https://github.com/Surfline/surfline-web/issues/5256)
* chore(deps-dev): bump core-js from 3.8.3 to 3.9.1 in /common (#5161) ([5a10943](https://github.com/Surfline/surfline-web/commit/5a10943)), closes [#5161](https://github.com/Surfline/surfline-web/issues/5161)
* chore(deps-dev): bump mocha from 8.2.1 to 8.3.2 in /common (#5257) ([e20c5a8](https://github.com/Surfline/surfline-web/commit/e20c5a8)), closes [#5257](https://github.com/Surfline/surfline-web/issues/5257)
* chore(deps-dev): bump rollup from 2.38.5 to 2.42.3 in /common (#5273) ([78e5deb](https://github.com/Surfline/surfline-web/commit/78e5deb)), closes [#5273](https://github.com/Surfline/surfline-web/issues/5273)
* chore(deps): bump @splitsoftware/splitio from 10.15.2 to 10.15.4 in /common (#5272) ([e056731](https://github.com/Surfline/surfline-web/commit/e056731)), closes [#5272](https://github.com/Surfline/surfline-web/issues/5272)
* chore(deps): bump date-fns from 2.16.1 to 2.19.0 in /common (#5219) ([5b66ec8](https://github.com/Surfline/surfline-web/commit/5b66ec8)), closes [#5219](https://github.com/Surfline/surfline-web/issues/5219)
* chore(deps): bump lodash from 4.17.20 to 4.17.21 in /common (#5128) ([178a17e](https://github.com/Surfline/surfline-web/commit/178a17e)), closes [#5128](https://github.com/Surfline/surfline-web/issues/5128)





## 12.1.0 (2021-03-31)

* feat: Add findSubdivisionCode util ([962330b](https://github.com/Surfline/surfline-web/commit/962330b))
* FE-000: Bump Common Dependencies (#5300) ([cb79e0b](https://github.com/Surfline/surfline-web/commit/cb79e0b)), closes [#5300](https://github.com/Surfline/surfline-web/issues/5300)
* Merge branch 'master' of https://github.com/Surfline/surfline-web into SA-3413_State_level_split_tar ([8d82d3e](https://github.com/Surfline/surfline-web/commit/8d82d3e))
* Rename to findSubdivisionCode.js ([5abc81f](https://github.com/Surfline/surfline-web/commit/5abc81f))
* rename to subdivisionCode ([b8fb8d3](https://github.com/Surfline/surfline-web/commit/b8fb8d3))
* SA-3350 - Frontend changes for Anonymous Meter Logic (#5268) ([a985600](https://github.com/Surfline/surfline-web/commit/a985600)), closes [#5268](https://github.com/Surfline/surfline-web/issues/5268)
* SA-3413 Implement state level Split targeting ([d6857db](https://github.com/Surfline/surfline-web/commit/d6857db))
* chore(deps-dev): bump @babel/core from 7.13.1 to 7.13.14 in /common (#5288) ([ec29dad](https://github.com/Surfline/surfline-web/commit/ec29dad)), closes [#5288](https://github.com/Surfline/surfline-web/issues/5288)
* chore(deps-dev): bump @babel/runtime from 7.13.7 to 7.13.10 in /common (#5218) ([7c0616f](https://github.com/Surfline/surfline-web/commit/7c0616f)), closes [#5218](https://github.com/Surfline/surfline-web/issues/5218)
* chore(deps-dev): bump chai from 4.2.0 to 4.3.4 in /common (#5256) ([8662045](https://github.com/Surfline/surfline-web/commit/8662045)), closes [#5256](https://github.com/Surfline/surfline-web/issues/5256)
* chore(deps-dev): bump core-js from 3.8.3 to 3.9.1 in /common (#5161) ([5a10943](https://github.com/Surfline/surfline-web/commit/5a10943)), closes [#5161](https://github.com/Surfline/surfline-web/issues/5161)
* chore(deps-dev): bump mocha from 8.2.1 to 8.3.2 in /common (#5257) ([e20c5a8](https://github.com/Surfline/surfline-web/commit/e20c5a8)), closes [#5257](https://github.com/Surfline/surfline-web/issues/5257)
* chore(deps-dev): bump rollup from 2.38.5 to 2.42.3 in /common (#5273) ([78e5deb](https://github.com/Surfline/surfline-web/commit/78e5deb)), closes [#5273](https://github.com/Surfline/surfline-web/issues/5273)
* chore(deps): bump @splitsoftware/splitio from 10.15.2 to 10.15.4 in /common (#5272) ([e056731](https://github.com/Surfline/surfline-web/commit/e056731)), closes [#5272](https://github.com/Surfline/surfline-web/issues/5272)
* chore(deps): bump date-fns from 2.16.1 to 2.19.0 in /common (#5219) ([5b66ec8](https://github.com/Surfline/surfline-web/commit/5b66ec8)), closes [#5219](https://github.com/Surfline/surfline-web/issues/5219)
* chore(deps): bump lodash from 4.17.20 to 4.17.21 in /common (#5128) ([178a17e](https://github.com/Surfline/surfline-web/commit/178a17e)), closes [#5128](https://github.com/Surfline/surfline-web/issues/5128)





## <small>12.0.3 (2021-03-23)</small>

* SA-3350 Backplane changes for Anonymous Meter logic ([2b9ae2a](https://github.com/Surfline/surfline-web/commit/2b9ae2a))
* SA-3350 Fix anonymous meter logic in KBYG ([29e729c](https://github.com/Surfline/surfline-web/commit/29e729c))





## <small>12.0.2 (2021-03-15)</small>

* chore: bump analytics.js snippet to 4.13.2 ([c1c0709](https://github.com/Surfline/surfline-web/commit/c1c0709))
* chore(deps-dev): bump eslint from 7.19.0 to 7.21.0 in /common (#5144) ([ef48bb7](https://github.com/Surfline/surfline-web/commit/ef48bb7)), closes [#5144](https://github.com/Surfline/surfline-web/issues/5144)





## <small>12.0.1 (2021-03-10)</small>

* fix: use getProxyUrl function in fetchWithAuth ([836f9c7](https://github.com/Surfline/surfline-web/commit/836f9c7))
* SA-3189: Split Client Side - Rate Limiting (#5073) ([075a419](https://github.com/Surfline/surfline-web/commit/075a419)), closes [#5073](https://github.com/Surfline/surfline-web/issues/5073)





## 12.0.0 (2021-03-09)

* fix: FE-147 correct date format (#5041) ([20720d5](https://github.com/Surfline/surfline-web/commit/20720d5)), closes [#5041](https://github.com/Surfline/surfline-web/issues/5041)
* fix: wait For Analytics to load ([a18abb2](https://github.com/Surfline/surfline-web/commit/a18abb2))
* fix: improve user and split related types ([4b2608e](https://github.com/Surfline/surfline-web/commit/4b2608e))
* fix: support APP_ENV environment variable ([bbe26b5](https://github.com/Surfline/surfline-web/commit/bbe26b5))
* feat: allow `withScriptTag` argument for segment snippet ([8fb7a48](https://github.com/Surfline/surfline-web/commit/8fb7a48))
* chore: remove treatmentVariant from meter ([54c5c1b](https://github.com/Surfline/surfline-web/commit/54c5c1b))
* fix: ensure anonymous surf check decrements meter even on failure ([5aa561f](https://github.com/Surfline/surfline-web/commit/5aa561f))
* fix: export `getCanUseDOM` ([6c3c565](https://github.com/Surfline/surfline-web/commit/6c3c565))
* feat: add `performAnonymousMeteringSurfCheck` ([3b9b49c](https://github.com/Surfline/surfline-web/commit/3b9b49c))
* feat: add wavetrakSplitFilters functions ([84210e0](https://github.com/Surfline/surfline-web/commit/84210e0))
* chore: update jsdoc types ([14d2243](https://github.com/Surfline/surfline-web/commit/14d2243))
* feat: create new split client-side implementation ([915ee70](https://github.com/Surfline/surfline-web/commit/915ee70))

* SA-3189: Split IO Client Side (#4929) ([5da9d1d](https://github.com/Surfline/surfline-web/commit/5da9d1d)), closes [#4929](https://github.com/Surfline/surfline-web/issues/4929)
* SA-3354 - Fix domain bug in resetAnonymousIdCookieSnippet  (#5189) ([350577c](https://github.com/Surfline/surfline-web/commit/350577c)), closes [#5189](https://github.com/Surfline/surfline-web/issues/5189)
* SA-3188: defer meter experience (#5108) ([4412647](https://github.com/Surfline/surfline-web/commit/4412647)), closes [#5108](https://github.com/Surfline/surfline-web/issues/5108)
* SA-3354 Implement sl_reset_anonymous_id_v3 logic ([a1ee066](https://github.com/Surfline/surfline-web/commit/a1ee066))
* SA-3307: Add rl_forced to identity for metered users (#5057) ([6676a16](https://github.com/Surfline/surfline-web/commit/6676a16)), closes [#5057](https://github.com/Surfline/surfline-web/issues/5057)

* chore(deps-dev): bump @rollup/plugin-commonjs in /common (#4946) ([65df2c0](https://github.com/Surfline/surfline-web/commit/65df2c0)), closes [#4946](https://github.com/Surfline/surfline-web/issues/4946)
* chore(deps-dev): bump @rollup/plugin-node-resolve in /common (#4948) ([b25a73d](https://github.com/Surfline/surfline-web/commit/b25a73d)), closes [#4948](https://github.com/Surfline/surfline-web/issues/4948)
* chore(deps-dev): bump core-js from 3.8.1 to 3.8.3 in /common (#4949) ([87d5111](https://github.com/Surfline/surfline-web/commit/87d5111)), closes [#4949](https://github.com/Surfline/surfline-web/issues/4949)
* chore(deps-dev): bump eslint from 7.17.0 to 7.19.0 in /common (#4944) ([c46a2b1](https://github.com/Surfline/surfline-web/commit/c46a2b1)), closes [#4944](https://github.com/Surfline/surfline-web/issues/4944)
* chore(deps-dev): bump rollup from 2.36.1 to 2.38.5 in /common (#5060) ([e9eccc9](https://github.com/Surfline/surfline-web/commit/e9eccc9)), closes [#5060](https://github.com/Surfline/surfline-web/issues/5060)
* chore(deps-dev): bump sinon from 9.2.3 to 9.2.4 in /common (#4947) ([d90432a](https://github.com/Surfline/surfline-web/commit/d90432a)), closes [#4947](https://github.com/Surfline/surfline-web/issues/4947)
* chore(deps): [security] bump immer from 8.0.0 to 8.0.1 in /common (#4919) ([4f39615](https://github.com/Surfline/surfline-web/commit/4f39615)), closes [#4919](https://github.com/Surfline/surfline-web/issues/4919)

### BREAKING CHANGE

* Removed `getMeterTreatmentVariant` export
* Removed the `identify` function export
* Split IO implementation is totally new. See `src/features/README.md`




## 11.8.0 (2021-03-08)

* feat: Remove getUserOpenHouseStatus, update getUserPremiumStatus ([3aefe01](https://github.com/Surfline/surfline-web/commit/3aefe01))





## <small>11.7.2 (2021-02-05)</small>

* fix: FE-147 correct date format (#5041) ([20720d5](https://github.com/Surfline/surfline-web/commit/20720d5)), closes [#5041](https://github.com/Surfline/surfline-web/issues/5041)
* fix: FE-147 Fix date format test ([e3e3a1e](https://github.com/Surfline/surfline-web/commit/e3e3a1e))





## <small>11.7.1 (2021-02-05)</small>

* fix: FE-147 correct date format ([62fda4f](https://github.com/Surfline/surfline-web/commit/62fda4f))





## 11.7.0 (2021-02-02)

* feat: Export NewRelic snippet and logger functions ([b194f5a](https://github.com/Surfline/surfline-web/commit/b194f5a))





## 11.6.0 (2021-01-20)

* INF-000 - Replace artifactoryonline.com with jfrog.io (#4813) ([dcebf30](https://github.com/Surfline/surfline-web/commit/dcebf30)), closes [#4813](https://github.com/Surfline/surfline-web/issues/4813)
* INF-337 - Replace artifactoryonline with jfrog domain (#4528) ([7225309](https://github.com/Surfline/surfline-web/commit/7225309)), closes [#4528](https://github.com/Surfline/surfline-web/issues/4528)
* Kbyg 3647 oembed insta graph (#4839) ([f5b0b3e](https://github.com/Surfline/surfline-web/commit/f5b0b3e)), closes [#4839](https://github.com/Surfline/surfline-web/issues/4839)
* KBYG-3525 Common - Split treatment with config (#4513) ([9056732](https://github.com/Surfline/surfline-web/commit/9056732)), closes [#4513](https://github.com/Surfline/surfline-web/issues/4513)
* KBYG-3753 Common - User settings put action (#4910) ([df890dd](https://github.com/Surfline/surfline-web/commit/df890dd)), closes [#4910](https://github.com/Surfline/surfline-web/issues/4910)
* Merge branch 'master' into kbyg-000-add-midnightofdate-to-web-common ([ec12ba2](https://github.com/Surfline/surfline-web/commit/ec12ba2))
* Merge branch 'master' into kbyg-000-add-midnightofdate-to-web-common ([08d76a7](https://github.com/Surfline/surfline-web/commit/08d76a7))
* SA-000: update README for web common (#4915) ([3936ea9](https://github.com/Surfline/surfline-web/commit/3936ea9)), closes [#4915](https://github.com/Surfline/surfline-web/issues/4915)
* fix: KBYG-3525 Common - export getTreatmentWithConfig (#4514) ([3526ce4](https://github.com/Surfline/surfline-web/commit/3526ce4)), closes [#4514](https://github.com/Surfline/surfline-web/issues/4514)
* fix: remove duplicate constant ([cfff8ae](https://github.com/Surfline/surfline-web/commit/cfff8ae))
* feat: add midnightOfDate to web-common ([0da80cb](https://github.com/Surfline/surfline-web/commit/0da80cb))
* feat: add time constants ([ba6f084](https://github.com/Surfline/surfline-web/commit/ba6f084))
* chore: lerna bump for services-common 11.5.0 ([67e9c88](https://github.com/Surfline/surfline-web/commit/67e9c88))
* chore(deps-dev): bump @babel/cli from 7.12.1 to 7.12.8 in /common (#4676) ([d3c3004](https://github.com/Surfline/surfline-web/commit/d3c3004)), closes [#4676](https://github.com/Surfline/surfline-web/issues/4676)
* chore(deps-dev): bump @babel/cli from 7.12.8 to 7.12.10 in /common (#4847) ([91e2636](https://github.com/Surfline/surfline-web/commit/91e2636)), closes [#4847](https://github.com/Surfline/surfline-web/issues/4847)
* chore(deps-dev): bump @babel/core from 7.12.3 to 7.12.9 in /common (#4668) ([4e57310](https://github.com/Surfline/surfline-web/commit/4e57310)), closes [#4668](https://github.com/Surfline/surfline-web/issues/4668)
* chore(deps-dev): bump @babel/core from 7.12.9 to 7.12.10 in /common (#4846) ([7591da0](https://github.com/Surfline/surfline-web/commit/7591da0)), closes [#4846](https://github.com/Surfline/surfline-web/issues/4846)
* chore(deps-dev): bump @babel/register from 7.12.1 to 7.12.10 in /common (#4849) ([b2d8673](https://github.com/Surfline/surfline-web/commit/b2d8673)), closes [#4849](https://github.com/Surfline/surfline-web/issues/4849)
* chore(deps-dev): bump @babel/runtime from 7.11.2 to 7.12.5 in /common (#4634) ([04600e0](https://github.com/Surfline/surfline-web/commit/04600e0)), closes [#4634](https://github.com/Surfline/surfline-web/issues/4634)
* chore(deps-dev): bump @rollup/plugin-commonjs in /common (#4675) ([4fb300f](https://github.com/Surfline/surfline-web/commit/4fb300f)), closes [#4675](https://github.com/Surfline/surfline-web/issues/4675)
* chore(deps-dev): bump @rollup/plugin-node-resolve from 11.0.0 to 11.0.1 in /common (#4850) ([6e7d06f](https://github.com/Surfline/surfline-web/commit/6e7d06f)), closes [#4850](https://github.com/Surfline/surfline-web/issues/4850)
* chore(deps-dev): bump @rollup/plugin-node-resolve in /common (#4673) ([8c4c7d1](https://github.com/Surfline/surfline-web/commit/8c4c7d1)), closes [#4673](https://github.com/Surfline/surfline-web/issues/4673)
* chore(deps-dev): bump @surfline/babel-preset-web from 1.0.3 to 1.0.4 in /common (#4860) ([f1a7e96](https://github.com/Surfline/surfline-web/commit/f1a7e96)), closes [#4860](https://github.com/Surfline/surfline-web/issues/4860)
* chore(deps-dev): bump core-js from 3.7.0 to 3.8.1 in /common (#4776) ([da16182](https://github.com/Surfline/surfline-web/commit/da16182)), closes [#4776](https://github.com/Surfline/surfline-web/issues/4776)
* chore(deps-dev): bump eslint from 7.11.0 to 7.13.0 in /common (#4633) ([a953811](https://github.com/Surfline/surfline-web/commit/a953811)), closes [#4633](https://github.com/Surfline/surfline-web/issues/4633)
* chore(deps-dev): bump eslint from 7.13.0 to 7.15.0 in /common (#4781) ([3907491](https://github.com/Surfline/surfline-web/commit/3907491)), closes [#4781](https://github.com/Surfline/surfline-web/issues/4781)
* chore(deps-dev): bump eslint from 7.15.0 to 7.17.0 in /common (#4881) ([7e68db3](https://github.com/Surfline/surfline-web/commit/7e68db3)), closes [#4881](https://github.com/Surfline/surfline-web/issues/4881)
* chore(deps-dev): bump mocha from 8.1.3 to 8.2.1 in /common (#4615) ([035191e](https://github.com/Surfline/surfline-web/commit/035191e)), closes [#4615](https://github.com/Surfline/surfline-web/issues/4615)
* chore(deps-dev): bump prettier from 2.1.2 to 2.2.1 in /common (#4667) ([ce8f382](https://github.com/Surfline/surfline-web/commit/ce8f382)), closes [#4667](https://github.com/Surfline/surfline-web/issues/4667)
* chore(deps-dev): bump rollup from 2.29.0 to 2.33.1 in /common (#4621) ([5d2cbf1](https://github.com/Surfline/surfline-web/commit/5d2cbf1)), closes [#4621](https://github.com/Surfline/surfline-web/issues/4621)
* chore(deps-dev): bump rollup from 2.33.1 to 2.34.1 in /common (#4758) ([689d600](https://github.com/Surfline/surfline-web/commit/689d600)), closes [#4758](https://github.com/Surfline/surfline-web/issues/4758)
* chore(deps-dev): bump rollup from 2.34.1 to 2.36.1 in /common (#4889) ([efdbb78](https://github.com/Surfline/surfline-web/commit/efdbb78)), closes [#4889](https://github.com/Surfline/surfline-web/issues/4889)
* chore(deps-dev): bump sinon from 9.2.1 to 9.2.3 in /common (#4890) ([93ed413](https://github.com/Surfline/surfline-web/commit/93ed413)), closes [#4890](https://github.com/Surfline/surfline-web/issues/4890)
* chore(deps-dev): bump typescript from 4.0.5 to 4.1.2 in /common (#4666) ([f81c180](https://github.com/Surfline/surfline-web/commit/f81c180)), closes [#4666](https://github.com/Surfline/surfline-web/issues/4666)
* chore(deps-dev): bump typescript from 4.1.2 to 4.1.3 in /common (#4859) ([0750be1](https://github.com/Surfline/surfline-web/commit/0750be1)), closes [#4859](https://github.com/Surfline/surfline-web/issues/4859)
* chore(deps-dev): update common deps (#4637) ([b7f2dd8](https://github.com/Surfline/surfline-web/commit/b7f2dd8)), closes [#4637](https://github.com/Surfline/surfline-web/issues/4637)
* chore(deps): bump @reduxjs/toolkit from 1.4.0 to 1.5.0 in /common (#4672) ([61dfbcd](https://github.com/Surfline/surfline-web/commit/61dfbcd)), closes [#4672](https://github.com/Surfline/surfline-web/issues/4672)
* chore(deps): bump @splitsoftware/splitio in /common (#4770) ([bbf54e7](https://github.com/Surfline/surfline-web/commit/bbf54e7)), closes [#4770](https://github.com/Surfline/surfline-web/issues/4770)





## 11.5.0 (2021-01-19)

* INF-000 - Replace artifactoryonline.com with jfrog.io (#4813) ([dcebf30](https://github.com/Surfline/surfline-web/commit/dcebf30)), closes [#4813](https://github.com/Surfline/surfline-web/issues/4813)
* INF-337 - Replace artifactoryonline with jfrog domain (#4528) ([7225309](https://github.com/Surfline/surfline-web/commit/7225309)), closes [#4528](https://github.com/Surfline/surfline-web/issues/4528)
* Kbyg 3647 oembed insta graph (#4839) ([f5b0b3e](https://github.com/Surfline/surfline-web/commit/f5b0b3e)), closes [#4839](https://github.com/Surfline/surfline-web/issues/4839)
* KBYG-3525 Common - Split treatment with config (#4513) ([9056732](https://github.com/Surfline/surfline-web/commit/9056732)), closes [#4513](https://github.com/Surfline/surfline-web/issues/4513)
* KBYG-3753 Common - User settings put action (#4910) ([df890dd](https://github.com/Surfline/surfline-web/commit/df890dd)), closes [#4910](https://github.com/Surfline/surfline-web/issues/4910)
* Merge branch 'master' into kbyg-000-add-midnightofdate-to-web-common ([08d76a7](https://github.com/Surfline/surfline-web/commit/08d76a7))
* feat: add midnightOfDate to web-common ([0da80cb](https://github.com/Surfline/surfline-web/commit/0da80cb))
* chore(deps-dev): bump @babel/cli from 7.12.1 to 7.12.8 in /common (#4676) ([d3c3004](https://github.com/Surfline/surfline-web/commit/d3c3004)), closes [#4676](https://github.com/Surfline/surfline-web/issues/4676)
* chore(deps-dev): bump @babel/cli from 7.12.8 to 7.12.10 in /common (#4847) ([91e2636](https://github.com/Surfline/surfline-web/commit/91e2636)), closes [#4847](https://github.com/Surfline/surfline-web/issues/4847)
* chore(deps-dev): bump @babel/core from 7.12.3 to 7.12.9 in /common (#4668) ([4e57310](https://github.com/Surfline/surfline-web/commit/4e57310)), closes [#4668](https://github.com/Surfline/surfline-web/issues/4668)
* chore(deps-dev): bump @babel/core from 7.12.9 to 7.12.10 in /common (#4846) ([7591da0](https://github.com/Surfline/surfline-web/commit/7591da0)), closes [#4846](https://github.com/Surfline/surfline-web/issues/4846)
* chore(deps-dev): bump @babel/register from 7.12.1 to 7.12.10 in /common (#4849) ([b2d8673](https://github.com/Surfline/surfline-web/commit/b2d8673)), closes [#4849](https://github.com/Surfline/surfline-web/issues/4849)
* chore(deps-dev): bump @babel/runtime from 7.11.2 to 7.12.5 in /common (#4634) ([04600e0](https://github.com/Surfline/surfline-web/commit/04600e0)), closes [#4634](https://github.com/Surfline/surfline-web/issues/4634)
* chore(deps-dev): bump @rollup/plugin-commonjs in /common (#4675) ([4fb300f](https://github.com/Surfline/surfline-web/commit/4fb300f)), closes [#4675](https://github.com/Surfline/surfline-web/issues/4675)
* chore(deps-dev): bump @rollup/plugin-node-resolve from 11.0.0 to 11.0.1 in /common (#4850) ([6e7d06f](https://github.com/Surfline/surfline-web/commit/6e7d06f)), closes [#4850](https://github.com/Surfline/surfline-web/issues/4850)
* chore(deps-dev): bump @rollup/plugin-node-resolve in /common (#4673) ([8c4c7d1](https://github.com/Surfline/surfline-web/commit/8c4c7d1)), closes [#4673](https://github.com/Surfline/surfline-web/issues/4673)
* chore(deps-dev): bump @surfline/babel-preset-web from 1.0.3 to 1.0.4 in /common (#4860) ([f1a7e96](https://github.com/Surfline/surfline-web/commit/f1a7e96)), closes [#4860](https://github.com/Surfline/surfline-web/issues/4860)
* chore(deps-dev): bump core-js from 3.7.0 to 3.8.1 in /common (#4776) ([da16182](https://github.com/Surfline/surfline-web/commit/da16182)), closes [#4776](https://github.com/Surfline/surfline-web/issues/4776)
* chore(deps-dev): bump eslint from 7.11.0 to 7.13.0 in /common (#4633) ([a953811](https://github.com/Surfline/surfline-web/commit/a953811)), closes [#4633](https://github.com/Surfline/surfline-web/issues/4633)
* chore(deps-dev): bump eslint from 7.13.0 to 7.15.0 in /common (#4781) ([3907491](https://github.com/Surfline/surfline-web/commit/3907491)), closes [#4781](https://github.com/Surfline/surfline-web/issues/4781)
* chore(deps-dev): bump eslint from 7.15.0 to 7.17.0 in /common (#4881) ([7e68db3](https://github.com/Surfline/surfline-web/commit/7e68db3)), closes [#4881](https://github.com/Surfline/surfline-web/issues/4881)
* chore(deps-dev): bump mocha from 8.1.3 to 8.2.1 in /common (#4615) ([035191e](https://github.com/Surfline/surfline-web/commit/035191e)), closes [#4615](https://github.com/Surfline/surfline-web/issues/4615)
* chore(deps-dev): bump prettier from 2.1.2 to 2.2.1 in /common (#4667) ([ce8f382](https://github.com/Surfline/surfline-web/commit/ce8f382)), closes [#4667](https://github.com/Surfline/surfline-web/issues/4667)
* chore(deps-dev): bump rollup from 2.29.0 to 2.33.1 in /common (#4621) ([5d2cbf1](https://github.com/Surfline/surfline-web/commit/5d2cbf1)), closes [#4621](https://github.com/Surfline/surfline-web/issues/4621)
* chore(deps-dev): bump rollup from 2.33.1 to 2.34.1 in /common (#4758) ([689d600](https://github.com/Surfline/surfline-web/commit/689d600)), closes [#4758](https://github.com/Surfline/surfline-web/issues/4758)
* chore(deps-dev): bump rollup from 2.34.1 to 2.36.1 in /common (#4889) ([efdbb78](https://github.com/Surfline/surfline-web/commit/efdbb78)), closes [#4889](https://github.com/Surfline/surfline-web/issues/4889)
* chore(deps-dev): bump sinon from 9.2.1 to 9.2.3 in /common (#4890) ([93ed413](https://github.com/Surfline/surfline-web/commit/93ed413)), closes [#4890](https://github.com/Surfline/surfline-web/issues/4890)
* chore(deps-dev): bump typescript from 4.0.5 to 4.1.2 in /common (#4666) ([f81c180](https://github.com/Surfline/surfline-web/commit/f81c180)), closes [#4666](https://github.com/Surfline/surfline-web/issues/4666)
* chore(deps-dev): bump typescript from 4.1.2 to 4.1.3 in /common (#4859) ([0750be1](https://github.com/Surfline/surfline-web/commit/0750be1)), closes [#4859](https://github.com/Surfline/surfline-web/issues/4859)
* chore(deps-dev): update common deps (#4637) ([b7f2dd8](https://github.com/Surfline/surfline-web/commit/b7f2dd8)), closes [#4637](https://github.com/Surfline/surfline-web/issues/4637)
* chore(deps): bump @reduxjs/toolkit from 1.4.0 to 1.5.0 in /common (#4672) ([61dfbcd](https://github.com/Surfline/surfline-web/commit/61dfbcd)), closes [#4672](https://github.com/Surfline/surfline-web/issues/4672)
* chore(deps): bump @splitsoftware/splitio in /common (#4770) ([bbf54e7](https://github.com/Surfline/surfline-web/commit/bbf54e7)), closes [#4770](https://github.com/Surfline/surfline-web/issues/4770)
* fix: KBYG-3525 Common - export getTreatmentWithConfig (#4514) ([3526ce4](https://github.com/Surfline/surfline-web/commit/3526ce4)), closes [#4514](https://github.com/Surfline/surfline-web/issues/4514)





## 11.4.0 (2021-01-15)

* feat: KBYG-3753 Common - User settings put action ([9031ea2](https://github.com/Surfline/surfline-web/commit/9031ea2))
* chore(deps-dev): bump @babel/cli from 7.12.8 to 7.12.10 in /common (#4847) ([91e2636](https://github.com/Surfline/surfline-web/commit/91e2636)), closes [#4847](https://github.com/Surfline/surfline-web/issues/4847)
* chore(deps-dev): bump @babel/core from 7.12.9 to 7.12.10 in /common (#4846) ([7591da0](https://github.com/Surfline/surfline-web/commit/7591da0)), closes [#4846](https://github.com/Surfline/surfline-web/issues/4846)
* chore(deps-dev): bump @babel/register from 7.12.1 to 7.12.10 in /common (#4849) ([b2d8673](https://github.com/Surfline/surfline-web/commit/b2d8673)), closes [#4849](https://github.com/Surfline/surfline-web/issues/4849)
* chore(deps-dev): bump @rollup/plugin-node-resolve from 11.0.0 to 11.0.1 in /common (#4850) ([6e7d06f](https://github.com/Surfline/surfline-web/commit/6e7d06f)), closes [#4850](https://github.com/Surfline/surfline-web/issues/4850)
* chore(deps-dev): bump @surfline/babel-preset-web from 1.0.3 to 1.0.4 in /common (#4860) ([f1a7e96](https://github.com/Surfline/surfline-web/commit/f1a7e96)), closes [#4860](https://github.com/Surfline/surfline-web/issues/4860)
* chore(deps-dev): bump eslint from 7.15.0 to 7.17.0 in /common (#4881) ([7e68db3](https://github.com/Surfline/surfline-web/commit/7e68db3)), closes [#4881](https://github.com/Surfline/surfline-web/issues/4881)
* chore(deps-dev): bump rollup from 2.34.1 to 2.36.1 in /common (#4889) ([efdbb78](https://github.com/Surfline/surfline-web/commit/efdbb78)), closes [#4889](https://github.com/Surfline/surfline-web/issues/4889)
* chore(deps-dev): bump sinon from 9.2.1 to 9.2.3 in /common (#4890) ([93ed413](https://github.com/Surfline/surfline-web/commit/93ed413)), closes [#4890](https://github.com/Surfline/surfline-web/issues/4890)
* chore(deps-dev): bump typescript from 4.1.2 to 4.1.3 in /common (#4859) ([0750be1](https://github.com/Surfline/surfline-web/commit/0750be1)), closes [#4859](https://github.com/Surfline/surfline-web/issues/4859)





## <small>11.3.3 (2020-12-28)</small>

* fix: KBYG-3647 use graph API for instagram fetch ([800d592](https://github.com/Surfline/surfline-web/commit/800d592))
* INF-000 - Replace artifactoryonline.com with jfrog.io (#4813) ([dcebf30](https://github.com/Surfline/surfline-web/commit/dcebf30)), closes [#4813](https://github.com/Surfline/surfline-web/issues/4813)
* INF-337 - Replace artifactoryonline with jfrog domain (#4528) ([7225309](https://github.com/Surfline/surfline-web/commit/7225309)), closes [#4528](https://github.com/Surfline/surfline-web/issues/4528)
* chore(deps-dev): bump @babel/cli from 7.12.1 to 7.12.8 in /common (#4676) ([d3c3004](https://github.com/Surfline/surfline-web/commit/d3c3004)), closes [#4676](https://github.com/Surfline/surfline-web/issues/4676)
* chore(deps-dev): bump @babel/core from 7.12.3 to 7.12.9 in /common (#4668) ([4e57310](https://github.com/Surfline/surfline-web/commit/4e57310)), closes [#4668](https://github.com/Surfline/surfline-web/issues/4668)
* chore(deps-dev): bump @babel/runtime from 7.11.2 to 7.12.5 in /common (#4634) ([04600e0](https://github.com/Surfline/surfline-web/commit/04600e0)), closes [#4634](https://github.com/Surfline/surfline-web/issues/4634)
* chore(deps-dev): bump @rollup/plugin-commonjs in /common (#4675) ([4fb300f](https://github.com/Surfline/surfline-web/commit/4fb300f)), closes [#4675](https://github.com/Surfline/surfline-web/issues/4675)
* chore(deps-dev): bump @rollup/plugin-node-resolve in /common (#4673) ([8c4c7d1](https://github.com/Surfline/surfline-web/commit/8c4c7d1)), closes [#4673](https://github.com/Surfline/surfline-web/issues/4673)
* chore(deps-dev): bump core-js from 3.7.0 to 3.8.1 in /common (#4776) ([da16182](https://github.com/Surfline/surfline-web/commit/da16182)), closes [#4776](https://github.com/Surfline/surfline-web/issues/4776)
* chore(deps-dev): bump eslint from 7.11.0 to 7.13.0 in /common (#4633) ([a953811](https://github.com/Surfline/surfline-web/commit/a953811)), closes [#4633](https://github.com/Surfline/surfline-web/issues/4633)
* chore(deps-dev): bump eslint from 7.13.0 to 7.15.0 in /common (#4781) ([3907491](https://github.com/Surfline/surfline-web/commit/3907491)), closes [#4781](https://github.com/Surfline/surfline-web/issues/4781)
* chore(deps-dev): bump mocha from 8.1.3 to 8.2.1 in /common (#4615) ([035191e](https://github.com/Surfline/surfline-web/commit/035191e)), closes [#4615](https://github.com/Surfline/surfline-web/issues/4615)
* chore(deps-dev): bump prettier from 2.1.2 to 2.2.1 in /common (#4667) ([ce8f382](https://github.com/Surfline/surfline-web/commit/ce8f382)), closes [#4667](https://github.com/Surfline/surfline-web/issues/4667)
* chore(deps-dev): bump rollup from 2.29.0 to 2.33.1 in /common (#4621) ([5d2cbf1](https://github.com/Surfline/surfline-web/commit/5d2cbf1)), closes [#4621](https://github.com/Surfline/surfline-web/issues/4621)
* chore(deps-dev): bump rollup from 2.33.1 to 2.34.1 in /common (#4758) ([689d600](https://github.com/Surfline/surfline-web/commit/689d600)), closes [#4758](https://github.com/Surfline/surfline-web/issues/4758)
* chore(deps-dev): bump typescript from 4.0.5 to 4.1.2 in /common (#4666) ([f81c180](https://github.com/Surfline/surfline-web/commit/f81c180)), closes [#4666](https://github.com/Surfline/surfline-web/issues/4666)
* chore(deps-dev): update common deps (#4637) ([b7f2dd8](https://github.com/Surfline/surfline-web/commit/b7f2dd8)), closes [#4637](https://github.com/Surfline/surfline-web/issues/4637)
* chore(deps): bump @reduxjs/toolkit from 1.4.0 to 1.5.0 in /common (#4672) ([61dfbcd](https://github.com/Surfline/surfline-web/commit/61dfbcd)), closes [#4672](https://github.com/Surfline/surfline-web/issues/4672)
* chore(deps): bump @splitsoftware/splitio in /common (#4770) ([bbf54e7](https://github.com/Surfline/surfline-web/commit/bbf54e7)), closes [#4770](https://github.com/Surfline/surfline-web/issues/4770)





## <small>11.3.2 (2020-10-28)</small>

* fix: KBYG-3525 Common - export getTreatmentWithConfig ([943c43c](https://github.com/Surfline/surfline-web/commit/943c43c))





## <small>11.3.1 (2020-10-28)</small>

* feat:KBYG-3525 Common - Split treatment with config ([4cd6b50](https://github.com/Surfline/surfline-web/commit/4cd6b50))





## 11.3.0 (2020-10-12)

* chore(deps): upgade dependencies ([e2eb1fd](https://github.com/Surfline/surfline-web/commit/e2eb1fd))
* feat: add `getUserRegion` API function ([88f8eae](https://github.com/Surfline/surfline-web/commit/88f8eae))





## <small>11.2.4 (2020-10-06)</small>

* fix: remove " from segment script tag ([717ad05](https://github.com/Surfline/surfline-web/commit/717ad05))
* chore(deps-dev): bump @rollup/plugin-commonjs in /common (#4388) ([4f1ae91](https://github.com/Surfline/surfline-web/commit/4f1ae91)), closes [#4388](https://github.com/Surfline/surfline-web/issues/4388)
* chore(deps-dev): bump eslint from 7.9.0 to 7.10.0 in /common (#4387) ([43e7a14](https://github.com/Surfline/surfline-web/commit/43e7a14)), closes [#4387](https://github.com/Surfline/surfline-web/issues/4387)
* chore(deps-dev): bump rollup from 2.27.1 to 2.28.2 in /common (#4391) ([93ad395](https://github.com/Surfline/surfline-web/commit/93ad395)), closes [#4391](https://github.com/Surfline/surfline-web/issues/4391)
* chore(deps-dev): bump sinon from 9.0.3 to 9.1.0 in /common (#4390) ([64ba132](https://github.com/Surfline/surfline-web/commit/64ba132)), closes [#4390](https://github.com/Surfline/surfline-web/issues/4390)
* chore(deps-dev): bump typescript from 4.0.2 to 4.0.3 in /common (#4389) ([e4cab89](https://github.com/Surfline/surfline-web/commit/e4cab89)), closes [#4389](https://github.com/Surfline/surfline-web/issues/4389)
* chore(deps): bump @splitsoftware/splitio in /common (#4386) ([fbc74d2](https://github.com/Surfline/surfline-web/commit/fbc74d2)), closes [#4386](https://github.com/Surfline/surfline-web/issues/4386)
* chore(deps): bump isomorphic-fetch from 2.2.1 to 3.0.0 in /common (#4384) ([19be365](https://github.com/Surfline/surfline-web/commit/19be365)), closes [#4384](https://github.com/Surfline/surfline-web/issues/4384)
* chore(deps): bump universal-cookie from 4.0.3 to 4.0.4 in /common (#4385) ([58dd858](https://github.com/Surfline/surfline-web/commit/58dd858)), closes [#4385](https://github.com/Surfline/surfline-web/issues/4385)





## <small>11.2.3 (2020-09-22)</small>

* fix: Add option to remove script tag from segment snippet ([a7d5172](https://github.com/Surfline/surfline-web/commit/a7d5172))
* SA-2790: Update quiver-react and deps (#4359) ([0bca6e4](https://github.com/Surfline/surfline-web/commit/0bca6e4)), closes [#4359](https://github.com/Surfline/surfline-web/issues/4359)





## <small>11.2.2 (2020-09-17)</small>

* fix: Remove extra JSDoc parameter ([01b8a45](https://github.com/Surfline/surfline-web/commit/01b8a45))
* fix: rename duplicate exported jsdoc type ([2c8b7bc](https://github.com/Surfline/surfline-web/commit/2c8b7bc))





## <small>11.2.1 (2020-09-17)</small>

* chore: update deps ([447f5d0](https://github.com/Surfline/surfline-web/commit/447f5d0))
* chore(deps-dev): bump eslint from 7.7.0 to 7.8.1 in /common (#4219) ([2562674](https://github.com/Surfline/surfline-web/commit/2562674)), closes [#4219](https://github.com/Surfline/surfline-web/issues/4219)
* chore(deps-dev): bump typescript from 3.9.7 to 4.0.2 in /common (#4222) ([71ae77a](https://github.com/Surfline/surfline-web/commit/71ae77a)), closes [#4222](https://github.com/Surfline/surfline-web/issues/4222)
* chore(deps): bump date-fns from 2.16.0 to 2.16.1 in /common (#4220) ([7b42260](https://github.com/Surfline/surfline-web/commit/7b42260)), closes [#4220](https://github.com/Surfline/surfline-web/issues/4220)
* fix: ensure `setAnonymousId` is never called if no anonymousId exists ([ccfcad4](https://github.com/Surfline/surfline-web/commit/ccfcad4))





## 11.2.0 (2020-09-08)

- feat: Add deviceInfo util ([d792afa](https://github.com/Surfline/surfline-web/commit/d792afa))
- chore(deps-dev): bump rollup from 2.26.7 to 2.26.10 in /common (#4324) ([cf332c9](https://github.com/Surfline/surfline-web/commit/cf332c9)), closes [#4324](https://github.com/Surfline/surfline-web/issues/4324)

## <small>11.1.3 (2020-09-03)</small>

- fix: Fix `postFavorite` fetching favorites from incorrect URL ([d384614](https://github.com/Surfline/surfline-web/commit/d384614))
- fix: Revert Fetch User Favorites Change ([2246fc5](https://github.com/Surfline/surfline-web/commit/2246fc5))

## <small>11.1.2 (2020-09-03)</small>

- fix: Properly point `fetchUserFavorites` to product API ([363a41c](https://github.com/Surfline/surfline-web/commit/363a41c))

## <small>11.1.1 (2020-09-02)</small>

- fix: Make services URLs https ([ec116df](https://github.com/Surfline/surfline-web/commit/ec116df))

## 11.1.0 (2020-09-02)

- feat: Add getUserSubscriptionWarnings selector ([95c65f8](https://github.com/Surfline/surfline-web/commit/95c65f8))

## 11.0.0 (2020-09-01)

- chore: add new exports ([de66492](https://github.com/Surfline/surfline-web/commit/de66492))
- chore: add test setup ([97f112b](https://github.com/Surfline/surfline-web/commit/97f112b))
- chore: improve test setup and coverage ([b64a091](https://github.com/Surfline/surfline-web/commit/b64a091))
- chore: update deps ([86cd9c3](https://github.com/Surfline/surfline-web/commit/86cd9c3))
- chore: update lock file ([ce06023](https://github.com/Surfline/surfline-web/commit/ce06023))
- chore: update lock file ([b8c23e7](https://github.com/Surfline/surfline-web/commit/b8c23e7))
- feat: Add fetchWithAuth function ([e89569f](https://github.com/Surfline/surfline-web/commit/e89569f))
- feat: Add jsdoc to getClientIp ([d5d3519](https://github.com/Surfline/surfline-web/commit/d5d3519))
- feat: Add metering and user actions ([f76572c](https://github.com/Surfline/surfline-web/commit/f76572c))
- feat: add setAuthCookie function for use when creating or signing in to accounts ([87fe263](https://github.com/Surfline/surfline-web/commit/87fe263))
- feat: add user and meter selectors ([efb72a1](https://github.com/Surfline/surfline-web/commit/efb72a1))
- feat: make hydrate backplane apiVersion v2 compatible ([427169c](https://github.com/Surfline/surfline-web/commit/427169c))
- fix: ignore coverage on warn util ([5216ac6](https://github.com/Surfline/surfline-web/commit/5216ac6))
- feeat: Add new entitlement constants ([1792480](https://github.com/Surfline/surfline-web/commit/1792480))
- feat!: Update API functions to use updated baseFetch ([7d1f92e](https://github.com/Surfline/surfline-web/commit/7d1f92e))
- feat!: Update base fetch to support varying base URL, add access token header ([454db82](https://github.com/Surfline/surfline-web/commit/454db82))

### BREAKING CHANGE

- API function signatures have changed
- API functions no longer take `baseUrl` and `cookies` arguments (see `wavetrakOptions`)
- API functions use `fetchOptions` and `wavetrakOptions`
- Change baseFetch signature to `baseFetch => (path, fetchOptions = {}, { serverSideCookies = null, addAccessToken = true } = {})`

## <small>10.3.1 (2020-08-14)</small>

- fix: kbyg-3381 common - no round on hi surf height ([fa9b52f](https://github.com/Surfline/surfline-web/commit/fa9b52f))

## 10.3.0 (2020-08-11)

- feat: Add subscriptions and promotions related API/utils/constants ([d83b9c6](https://github.com/Surfline/surfline-web/commit/d83b9c6))
- feat: Add Typescript typings to all files ([5b474e3](https://github.com/Surfline/surfline-web/commit/5b474e3))
- feat: Improve typing outputs ([813403c](https://github.com/Surfline/surfline-web/commit/813403c))
- fix: improve export syntax to support typescript typing compilation ([681ed82](https://github.com/Surfline/surfline-web/commit/681ed82))
- chore: upgrade deps ([464cf8f](https://github.com/Surfline/surfline-web/commit/464cf8f))

## <small>10.2.1 (2020-08-05)</small>

- chore: posted date test fix ([a3a9199](https://github.com/Surfline/surfline-web/commit/a3a9199))

## 10.2.0 (2020-08-05)

- fix: kbyg-3220 date format ([8d3df92](https://github.com/Surfline/surfline-web/commit/8d3df92))
- KBYG-3220 Common - postedDate with dateFormat localized (#4158) ([8780b43](https://github.com/Surfline/surfline-web/commit/8780b43)), closes [#4158](https://github.com/Surfline/surfline-web/issues/4158)
- chore(deps-dev): bump @babel/cli from 7.10.4 to 7.10.5 in /common (#4097) ([770d7a9](https://github.com/Surfline/surfline-web/commit/770d7a9)), closes [#4097](https://github.com/Surfline/surfline-web/issues/4097)
- chore(deps-dev): bump @babel/core from 7.10.4 to 7.11.0 in /common (#4086) ([0fc4336](https://github.com/Surfline/surfline-web/commit/0fc4336)), closes [#4086](https://github.com/Surfline/surfline-web/issues/4086)
- chore(deps-dev): bump @babel/register from 7.10.4 to 7.10.5 in /common (#4094) ([060e497](https://github.com/Surfline/surfline-web/commit/060e497)), closes [#4094](https://github.com/Surfline/surfline-web/issues/4094)
- chore(deps-dev): bump @babel/runtime from 7.10.4 to 7.11.1 in /common (#4153) ([e23409e](https://github.com/Surfline/surfline-web/commit/e23409e)), closes [#4153](https://github.com/Surfline/surfline-web/issues/4153)
- chore(deps-dev): bump @rollup/plugin-commonjs in /common (#4080) ([89fbc96](https://github.com/Surfline/surfline-web/commit/89fbc96)), closes [#4080](https://github.com/Surfline/surfline-web/issues/4080)
- chore(deps-dev): bump eslint from 7.4.0 to 7.6.0 in /common (#4092) ([ef261eb](https://github.com/Surfline/surfline-web/commit/ef261eb)), closes [#4092](https://github.com/Surfline/surfline-web/issues/4092)
- chore(deps-dev): bump mocha from 8.0.1 to 8.1.1 in /common (#4149) ([13b6871](https://github.com/Surfline/surfline-web/commit/13b6871)), closes [#4149](https://github.com/Surfline/surfline-web/issues/4149)
- chore(deps-dev): bump regenerator-runtime in /common (#4081) ([0ef82f7](https://github.com/Surfline/surfline-web/commit/0ef82f7)), closes [#4081](https://github.com/Surfline/surfline-web/issues/4081)
- chore(deps-dev): bump rollup from 2.21.0 to 2.23.0 in /common (#4089) ([6736de8](https://github.com/Surfline/surfline-web/commit/6736de8)), closes [#4089](https://github.com/Surfline/surfline-web/issues/4089)
- chore(deps): bump @splitsoftware/splitio in /common (#4084) ([40d3483](https://github.com/Surfline/surfline-web/commit/40d3483)), closes [#4084](https://github.com/Surfline/surfline-web/issues/4084)
- chore(deps): bump date-fns from 2.14.0 to 2.15.0 in /common (#4071) ([f1994b4](https://github.com/Surfline/surfline-web/commit/f1994b4)), closes [#4071](https://github.com/Surfline/surfline-web/issues/4071)

## 10.1.0 (2020-08-03)

- feat: Add cache support to segment snippet to prevent duplicate anonymousIds ([81ecaa1](https://github.com/Surfline/surfline-web/commit/81ecaa1))

## 10.0.0 (2020-08-03)

- feat!: add contentUrl ([cadb6c9](https://github.com/Surfline/surfline-web/commit/cadb6c9))

### BREAKING CHANGE

- add contentUrl argument to videoStructuredData"

## <small>9.1.1 (2020-07-30)</small>

- fix: Update segment snippet to the latest ([cf77f5d](https://github.com/Surfline/surfline-web/commit/cf77f5d))

## 9.1.0 (2020-07-28)

- feat: add getWaveUnits common function, add HI rounding ([26d6c1f](https://github.com/Surfline/surfline-web/commit/26d6c1f))

## 9.0.0 (2020-07-14)

- chore: use npm ci ([7de6b6e](https://github.com/Surfline/surfline-web/commit/7de6b6e))
- fix!: Update trackLink function to be named more accurately to its use ([ca6f412](https://github.com/Surfline/surfline-web/commit/ca6f412))

### BREAKING CHANGE

- Renamed `trackLinkClick` to be `setupLinkTracking`

## 8.1.0 (2020-07-14)

- chore(deps-dev): bump @babel/cli from 7.10.3 to 7.10.4 in /common (#3928) ([43ad142](https://github.com/Surfline/surfline-web/commit/43ad142)), closes [#3928](https://github.com/Surfline/surfline-web/issues/3928)
- chore(deps-dev): bump @babel/core from 7.10.3 to 7.10.4 in /common (#3924) ([0eb14a6](https://github.com/Surfline/surfline-web/commit/0eb14a6)), closes [#3924](https://github.com/Surfline/surfline-web/issues/3924)
- chore(deps-dev): bump @babel/register from 7.10.3 to 7.10.4 in /common (#3932) ([2fbbd3a](https://github.com/Surfline/surfline-web/commit/2fbbd3a)), closes [#3932](https://github.com/Surfline/surfline-web/issues/3932)
- chore(deps-dev): bump @babel/runtime from 7.10.3 to 7.10.4 in /common (#3938) ([385fa53](https://github.com/Surfline/surfline-web/commit/385fa53)), closes [#3938](https://github.com/Surfline/surfline-web/issues/3938)
- chore(deps-dev): bump @rollup/plugin-node-resolve in /common (#3926) ([a1b1d1c](https://github.com/Surfline/surfline-web/commit/a1b1d1c)), closes [#3926](https://github.com/Surfline/surfline-web/issues/3926)
- chore(deps-dev): bump @surfline/eslint-config-web in /common (#3939) ([93e58e4](https://github.com/Surfline/surfline-web/commit/93e58e4)), closes [#3939](https://github.com/Surfline/surfline-web/issues/3939)
- chore(deps-dev): bump eslint from 7.2.0 to 7.3.1 in /common (#3930) ([57fde2f](https://github.com/Surfline/surfline-web/commit/57fde2f)), closes [#3930](https://github.com/Surfline/surfline-web/issues/3930)
- chore(deps-dev): bump rollup from 2.16.1 to 2.18.1 in /common (#3936) ([b86c584](https://github.com/Surfline/surfline-web/commit/b86c584)), closes [#3936](https://github.com/Surfline/surfline-web/issues/3936)
- chore(deps): bump @splitsoftware/splitio in /common (#3934) ([68409b0](https://github.com/Surfline/surfline-web/commit/68409b0)), closes [#3934](https://github.com/Surfline/surfline-web/issues/3934)
- feat: date format of posted date (#3997) ([28e996b](https://github.com/Surfline/surfline-web/commit/28e996b)), closes [#3997](https://github.com/Surfline/surfline-web/issues/3997)

## <small>8.0.1 (2020-06-29)</small>

- chore: update package-lock.json ([abcbe51](https://github.com/Surfline/surfline-web/commit/abcbe51))
- fix: ensure that callback is called even if track call is not reached ([8c80d2c](https://github.com/Surfline/surfline-web/commit/8c80d2c))
- fix: fix daysFromNowUntil export being improperly named ([b6f6aa3](https://github.com/Surfline/surfline-web/commit/b6f6aa3))

## 8.0.0 (2020-06-25)

- fix!: add missing argument to trackEvent and trackNavigatedToPage ([9d59329](https://github.com/Surfline/surfline-web/commit/9d59329))

### BREAKING CHANGE

- `trackEvent` now takes an `options` argument before the
  `callback` argument

## <small>7.0.2 (2020-06-23)</small>

- fix: add category to `trackNavigatedToPage` ([99a0808](https://github.com/Surfline/surfline-web/commit/99a0808))

## <small>7.0.1 (2020-06-22)</small>

- fix: kbyg-3220 - fix next update abbreviation ([121c078](https://github.com/Surfline/surfline-web/commit/121c078))

## 7.0.0 (2020-06-17)

- chore: update dependencies ([e528b17](https://github.com/Surfline/surfline-web/commit/e528b17))
- feat: implement segment tracking helper functions ([7779ed3](https://github.com/Surfline/surfline-web/commit/7779ed3))

### BREAKING CHANGE

- Removed `pageMapper` export

## 6.1.0 (2020-06-03)

- KBYG-2047 Common - Posted date abbreviated day (#3842) ([b5355ed](https://github.com/Surfline/surfline-web/commit/b5355ed)), closes [#3842](https://github.com/Surfline/surfline-web/issues/3842)
- chore(deps-dev): bump @babel/cli from 7.8.4 to 7.10.1 in /common (#3786) ([80ee440](https://github.com/Surfline/surfline-web/commit/80ee440)), closes [#3786](https://github.com/Surfline/surfline-web/issues/3786)
- chore(deps-dev): bump @babel/core from 7.9.6 to 7.10.2 in /common (#3801) ([69febad](https://github.com/Surfline/surfline-web/commit/69febad)), closes [#3801](https://github.com/Surfline/surfline-web/issues/3801)
- chore(deps-dev): bump @babel/register from 7.9.0 to 7.10.1 in /common (#3804) ([85118ef](https://github.com/Surfline/surfline-web/commit/85118ef)), closes [#3804](https://github.com/Surfline/surfline-web/issues/3804)
- chore(deps-dev): bump @babel/runtime from 7.9.6 to 7.10.2 in /common (#3798) ([5075770](https://github.com/Surfline/surfline-web/commit/5075770)), closes [#3798](https://github.com/Surfline/surfline-web/issues/3798)
- chore(deps-dev): bump mocha from 7.1.2 to 7.2.0 in /common (#3788) ([3c9ead0](https://github.com/Surfline/surfline-web/commit/3c9ead0)), closes [#3788](https://github.com/Surfline/surfline-web/issues/3788)
- chore(deps-dev): bump nyc from 15.0.1 to 15.1.0 in /common (#3795) ([fb8e20c](https://github.com/Surfline/surfline-web/commit/fb8e20c)), closes [#3795](https://github.com/Surfline/surfline-web/issues/3795)
- chore(deps-dev): bump prettier-eslint from 9.0.1 to 10.1.1 in /common (#3792) ([b87beea](https://github.com/Surfline/surfline-web/commit/b87beea)), closes [#3792](https://github.com/Surfline/surfline-web/issues/3792)
- chore(deps-dev): bump rollup from 2.10.5 to 2.12.0 in /common (#3785) ([46e5a5e](https://github.com/Surfline/surfline-web/commit/46e5a5e)), closes [#3785](https://github.com/Surfline/surfline-web/issues/3785)
- chore(deps-dev): bump rollup-plugin-license in /common (#3784) ([4db79f7](https://github.com/Surfline/surfline-web/commit/4db79f7)), closes [#3784](https://github.com/Surfline/surfline-web/issues/3784)
- chore(deps-dev): bump rollup-plugin-terser in /common (#3789) ([d0f2eab](https://github.com/Surfline/surfline-web/commit/d0f2eab)), closes [#3789](https://github.com/Surfline/surfline-web/issues/3789)

## 6.0.0 (2020-05-28)

- fix: Change "pageStructuredData" function to be more generic ([5bfab75](https://github.com/Surfline/surfline-web/commit/5bfab75))

### BREAKING CHANGE

- `pageStructuredData` now takes an object as an argument

## 5.2.0 (2020-05-27)

- feat: Add Google Structured Data Generators ([9b3756c](https://github.com/Surfline/surfline-web/commit/9b3756c))

## <small>5.1.2 (2020-05-20)</small>

- chore: Bump core-js from 3.6.4 to 3.6.5 in /common (#3662) ([5301399](https://github.com/Surfline/surfline-web/commit/5301399)), closes [#3662](https://github.com/Surfline/surfline-web/issues/3662)
- chore: Remove unused UUID dependency (#3651) ([c128fc8](https://github.com/Surfline/surfline-web/commit/c128fc8)), closes [#3651](https://github.com/Surfline/surfline-web/issues/3651)
- chore(deps-dev): bump @rollup/plugin-commonjs in /common (#3682) ([ae79239](https://github.com/Surfline/surfline-web/commit/ae79239)), closes [#3682](https://github.com/Surfline/surfline-web/issues/3682)
- chore(deps-dev): bump @rollup/plugin-node-resolve in /common (#3679) ([6aaf0e6](https://github.com/Surfline/surfline-web/commit/6aaf0e6)), closes [#3679](https://github.com/Surfline/surfline-web/issues/3679)
- chore(deps-dev): bump nyc from 15.0.0 to 15.0.1 in /common (#3681) ([d355c22](https://github.com/Surfline/surfline-web/commit/d355c22)), closes [#3681](https://github.com/Surfline/surfline-web/issues/3681)
- chore(deps-dev): bump rollup from 2.10.2 to 2.10.5 in /common (#3683) ([4863800](https://github.com/Surfline/surfline-web/commit/4863800)), closes [#3683](https://github.com/Surfline/surfline-web/issues/3683)
- chore(deps): [security] bump handlebars from 4.0.10 to 4.7.6 in /common (#3680) ([c1088cd](https://github.com/Surfline/surfline-web/commit/c1088cd)), closes [#3680](https://github.com/Surfline/surfline-web/issues/3680)
- chore(deps): Bump @babel/runtime from 7.9.2 to 7.9.6 in /common (#3660) ([fc11f40](https://github.com/Surfline/surfline-web/commit/fc11f40)), closes [#3660](https://github.com/Surfline/surfline-web/issues/3660)
- chore(deps): Bump @splitsoftware/splitio from 9.3.7 to 10.12.1 in /common (#3673) ([51b8901](https://github.com/Surfline/surfline-web/commit/51b8901)), closes [#3673](https://github.com/Surfline/surfline-web/issues/3673)
- chore(deps): Bump eslint-plugin-import from 2.20.1 to 2.20.2 in /common (#3653) ([f969e7f](https://github.com/Surfline/surfline-web/commit/f969e7f)), closes [#3653](https://github.com/Surfline/surfline-web/issues/3653)
- chore(deps): Bump mocha from 7.1.1 to 7.1.2 in /common (#3652) ([6ce0d8b](https://github.com/Surfline/surfline-web/commit/6ce0d8b)), closes [#3652](https://github.com/Surfline/surfline-web/issues/3652)
- chore(deps): Bump prettier from 2.0.2 to 2.0.5 in /common (#3657) ([dd7460b](https://github.com/Surfline/surfline-web/commit/dd7460b)), closes [#3657](https://github.com/Surfline/surfline-web/issues/3657)
- [Security] Bump lodash.merge from 4.6.0 to 4.6.2 in /common (#3647) ([5984003](https://github.com/Surfline/surfline-web/commit/5984003)), closes [#3647](https://github.com/Surfline/surfline-web/issues/3647)
- Bump @rollup/plugin-commonjs from 11.0.2 to 11.1.0 in /common (#3668) ([69c6faf](https://github.com/Surfline/surfline-web/commit/69c6faf)), closes [#3668](https://github.com/Surfline/surfline-web/issues/3668)
- Bump @rollup/plugin-json from 4.0.2 to 4.0.3 in /common (#3665) ([c9126e0](https://github.com/Surfline/surfline-web/commit/c9126e0)), closes [#3665](https://github.com/Surfline/surfline-web/issues/3665)
- Bump @rollup/plugin-replace from 2.3.1 to 2.3.2 in /common (#3667) ([2d23360](https://github.com/Surfline/surfline-web/commit/2d23360)), closes [#3667](https://github.com/Surfline/surfline-web/issues/3667)
- Bump @surfline/babel-preset-web from 0.1.2 to 1.0.2 in /common (#3658) ([505c8a9](https://github.com/Surfline/surfline-web/commit/505c8a9)), closes [#3658](https://github.com/Surfline/surfline-web/issues/3658)
- Bump @surfline/prettier-config from 0.2.4 to 0.2.5 in /common (#3656) ([a7b2441](https://github.com/Surfline/surfline-web/commit/a7b2441)), closes [#3656](https://github.com/Surfline/surfline-web/issues/3656)
- Bump date-fns from 2.11.1 to 2.14.0 in /common (#3669) ([2cd5ddf](https://github.com/Surfline/surfline-web/commit/2cd5ddf)), closes [#3669](https://github.com/Surfline/surfline-web/issues/3669)
- Bump eslint-plugin-prettier from 3.1.2 to 3.1.3 in /common (#3664) ([68c6609](https://github.com/Surfline/surfline-web/commit/68c6609)), closes [#3664](https://github.com/Surfline/surfline-web/issues/3664)
- Bump rollup from 2.2.0 to 2.10.2 in /common (#3650) ([1e22099](https://github.com/Surfline/surfline-web/commit/1e22099)), closes [#3650](https://github.com/Surfline/surfline-web/issues/3650)
- Bump rollup-plugin-license from 0.13.0 to 2.0.1 in /common (#3666) ([674608d](https://github.com/Surfline/surfline-web/commit/674608d)), closes [#3666](https://github.com/Surfline/surfline-web/issues/3666)
- Bump sinon from 9.0.1 to 9.0.2 in /common (#3659) ([c2cafc2](https://github.com/Surfline/surfline-web/commit/c2cafc2)), closes [#3659](https://github.com/Surfline/surfline-web/issues/3659)

## <small>5.1.1 (2020-04-14)</small>

- chore: bump web common version ([9042b36](https://github.com/Surfline/surfline-web/commit/9042b36))
- SA-2561: Update formatPlanPricing utils for new cases (#3582) ([a369804](https://github.com/Surfline/surfline-web/commit/a369804)), closes [#3582](https://github.com/Surfline/surfline-web/issues/3582)

## 5.0.0 (2020-04-07)

- chore: add version injector plugin ([6c544bb](https://github.com/Surfline/surfline-web/commit/6c544bb))
- feat: use rollup for bundle ([e2a693b](https://github.com/Surfline/surfline-web/commit/e2a693b))
- fix: fix type on occassionalUnits function to be occasionalUnits ([96cc337](https://github.com/Surfline/surfline-web/commit/96cc337))
- fix: rename mapTileUrl function to getMapTileUrl ([5a0bbac](https://github.com/Surfline/surfline-web/commit/5a0bbac))
- fix: Fix babel config not properly checking build env ([e5e0ce9](https://github.com/Surfline/surfline-web/commit/e5e0ce9))
- fix: Fix prePublish not performing build ([6269eb0](https://github.com/Surfline/surfline-web/commit/6269eb0))
- fix: Fix the files property to ensure all files get bundled ([044b838](https://github.com/Surfline/surfline-web/commit/044b838))
- fix: fix esm bundle not released ([ebfbc86](https://github.com/Surfline/surfline-web/commit/ebfbc86))
- chore: Bump dependencies ([01304b0](https://github.com/Surfline/surfline-web/commit/01304b0))
- chore: update package-lock.json ([d599e84](https://github.com/Surfline/surfline-web/commit/d599e84))
- chore!: Remove deprecated code from actions folder ([c6a6e06](https://github.com/Surfline/surfline-web/commit/c6a6e06))
- chore!: Removed the KBYG Path function `spotForecastPath` since that page is now consolidated into o ([97e11ce](https://github.com/Surfline/surfline-web/commit/97e11ce))
- feat!: Re-organize code to be exported from index file to support tree-shaking ([6adb980](https://github.com/Surfline/surfline-web/commit/6adb980))
- feat: Add support for ESM build output ([eac543b](https://github.com/Surfline/surfline-web/commit/eac543b))
- feat: Consume @surfline/prettier-config ([6f42a07](https://github.com/Surfline/surfline-web/commit/6f42a07))

### BREAKING CHANGE

- Re-organize all code in the package in order to make it be elegantly exported from the root index file in order to support tree shaking. This is required as importing directly from the ESM build (`import from '/esm/someFile'`) would break server side rendering since it would force node into consuming ESM which it does not support
- Removed the KBYG Path function `spotForecastPath` since that page is now consolidated into one page.
- Removed the user dispatch actions functions that were previously exported from the actions folder. These have been deprecated for quite a long time and it's well past time we remove them.

## 4.2.1 (12/10/19)

- Bug fix: Fixes a bug with the `getLocalDate` function where dates in the past were occasionally not applied the right timezone offset.
- Bumped dependency versions.

## 4.0.0 (12/10/19)

- BREAKING CHANGE: Removed the identity export from `/dist/features/index.js` because it was unused and outdated. The maintained version of this can be found in Backplane.
