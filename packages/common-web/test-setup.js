import chai from 'chai';

import sinonChai from 'sinon-chai';
import dirtyChai from 'dirty-chai';
import chaiSubset from 'chai-subset';

chai.use(dirtyChai);
chai.use(sinonChai);
chai.use(chaiSubset);

global.window = {
  location: {
    hostname: 'surfline.com',
  },
};
