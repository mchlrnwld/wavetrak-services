module.exports = (api) => {
  const isTest = process.env.NODE_ENV === 'test';
  // required by babel see: https://babeljs.io/docs/en/config-files#apicache
  api.cache.invalidate(() => isTest);

  return {
    presets: [['@surfline/babel-preset-web/base']],
    plugins: ['lodash', isTest && 'istanbul'].filter(Boolean),
  };
};
