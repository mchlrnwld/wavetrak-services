/* eslint-disable */
'use strict';

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./cjs/@surfline/web-common.min.js');
} else {
  module.exports = require('./cjs/@surfline/web-common.js');
}
