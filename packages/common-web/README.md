# Web Common

This package can be imported from `@surfline/web-common`. It contains application code that is common across all web applications.

## Publishing NPM package

Within the `/packages/` directory we publish NPM packages using Lerna.

A step by step guide for publishing NPM packages in the monorepo can be found [here](../../README.md#npm-packages).
