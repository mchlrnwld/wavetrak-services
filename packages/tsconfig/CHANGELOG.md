# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## <small>0.0.6 (2021-10-13)</small>

* chore(tsconfig): migrate to packages directory ([9c1395c](https://github.com/Surfline/wavetrak-services/commit/9c1395c))





## <small>0.0.5 (2020-11-04)</small>

* PS-000 - Replace artifactoryonline.com references with jfrog.io. (#3664) ([21430cd](https://github.com/Surfline/wavetrak-services/commit/21430cd)), closes [#3664](https://github.com/Surfline/wavetrak-services/issues/3664)





## [0.0.4](https://github.com/Surfline/wavetrak-services/compare/@surfline/tsconfig@0.0.3...@surfline/tsconfig@0.0.4) (2020-03-18)


### Bug Fixes

* Create a tagging script for Lerna ([9812c76](https://github.com/Surfline/wavetrak-services/commit/9812c76d618b9f54ba34403cf2d026fb983ea856))





## 0.0.3 (2020-03-11)

**Note:** Version bump only for package @surfline/tsconfig





## 0.0.2 (2020-03-09)

**Note:** Version bump only for package @surfline/tsconfig





## 0.0.1 (2020-03-04)

**Note:** Version bump only for package @surfline/tsconfig
