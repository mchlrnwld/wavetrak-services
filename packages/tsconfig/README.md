# `typescript-config`

A package containing a base Typescript config for Surfline Applications

## Publishing NPM package

Within the `/packages/` directory we publish NPM packages using Lerna.

A step by step guide for publishing NPM packages in the monorepo can be found [here](../../README.md#npm-packages).

## Usage

Since the `tsconfig.json` resolves relative paths based on the location of the file the option is found in, we still have to put some options into the `tsconfig.json` file that lives inside the service/app.

Here is an example for an application that will compile files inside `src` to the `dist` directory. It will include types in the `types/` folder and merge them with types in `node_modules`. It will also exclude compiling `node_modules` and `*.spec.ts` files.

```
// tsconfig.json
{
  "extends": "@surfline/tsconfig"
  "compilerOptions": {
    "rootDir": "./src",
    "outDir": "./dist",
    "typeRoots": [
      "./types",
      "node_modules/@types"
    ],
  },
  "include": [
    "./src",
    "./types"
  ],
  "exclude": [
    "./node_modules/**",
    "**/*.spec.ts"
  ]
}
```
