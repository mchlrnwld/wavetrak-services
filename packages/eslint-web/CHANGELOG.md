# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [3.1.1](https://github.com/Surfline/wavetrak-services/compare/@surfline/eslint-config-web@3.1.0...@surfline/eslint-config-web@3.1.1) (2022-02-08)


### Bug Fixes

* disable function component lint rule ([ec22717](https://github.com/Surfline/wavetrak-services/commit/ec22717602d273fb1e6717acebb44f467e780aa5))
* incorrect config values ([8d0701e](https://github.com/Surfline/wavetrak-services/commit/8d0701e8d0230890efc35894e497425ee2fe3f99))



## [3.1.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/eslint-config-web@3.0.4...@surfline/eslint-config-web@3.1.0) (2022-02-07)


### Features

* update rules based on typescript usage in web ([81bf223](https://github.com/Surfline/wavetrak-services/commit/81bf2239d72602014df3c6a54f92637c8a754476))


### Chores

* bump package deps ([4aece25](https://github.com/Surfline/wavetrak-services/commit/4aece2579a8507b7ffe58c728670d3d0e7551d67))



### [3.0.4](https://github.com/Surfline/wavetrak-services/compare/@surfline/eslint-config-web@3.0.3...@surfline/eslint-config-web@3.0.4) (2021-12-09)

**Note:** Version bump only for package @surfline/eslint-config-web





## <small>3.0.3 (2021-10-13)</small>

* chore(eslint-web): move eslint-web to packages and update CODEOWNERS ([d48339c](https://github.com/Surfline/wavetrak-services/commit/d48339c))





## <small>3.0.2 (2021-09-30)</small>

* fix: files not uploaded to package ([bd9b664](https://github.com/Surfline/wavetrak-services/commit/bd9b664))





## <small>3.0.1 (2021-09-30)</small>

* fix: disable jsx in scope for nextjs ([a7c73bb](https://github.com/Surfline/wavetrak-services/commit/a7c73bb))





## 3.0.0 (2021-09-29)

* fix: add @babel/core to allow package linting itself ([b5e9e63](https://github.com/Surfline/wavetrak-services/commit/b5e9e63))
* fix: remove requirement for babel config ([2e021c8](https://github.com/Surfline/wavetrak-services/commit/2e021c8))
* chore(deps): bump @typescript-eslint/parser from 4.9.0 to 4.31.1 (#4894) ([d9bec99](https://github.com/Surfline/wavetrak-services/commit/d9bec99)), closes [#4894](https://github.com/Surfline/wavetrak-services/issues/4894)
* chore(deps): update dependencies ([4c8106c](https://github.com/Surfline/wavetrak-services/commit/4c8106c))
* fix!: split typescript configs for react and non-react ([e34767d](https://github.com/Surfline/wavetrak-services/commit/e34767d))
* feat: add Jest ESLint configuration ([d0852a8](https://github.com/Surfline/wavetrak-services/commit/d0852a8))
* feat: add NextJS config file ([cd25911](https://github.com/Surfline/wavetrak-services/commit/cd25911))


### BREAKING CHANGE

* Now have to opt into react linting for typescript (`/typescript-react`)




## 2.0.0 (2021-06-11)

* feat(eslint-web): remove window from the global scope (#4453) ([45457f4](https://github.com/Surfline/wavetrak-services/commit/45457f4)), closes [#4453](https://github.com/Surfline/wavetrak-services/issues/4453)
* WP-29: Cleanup Docs, Yarn, Docker and Ownership (#4340) ([2c01d4c](https://github.com/Surfline/wavetrak-services/commit/2c01d4c)), closes [#4340](https://github.com/Surfline/wavetrak-services/issues/4340)


### BREAKING CHANGE

* refactor how window, document, history and location globals are referenced. Use the shared abstractions to address the linting issues.
+ [withWindow](https://github.com/Surfline/quiver/blob/master/react/src/withWindow/withWindow.js) component
+ [canUseDOM](https://github.com/Surfline/surfline-web/blob/master/common/src/canUseDOM.js) helper
+ [getWindow](https://github.com/Surfline/surfline-web/blob/master/common/src/getWindow.js) helper




## <small>1.0.3 (2021-04-15)</small>

* chore: bump dependencies ([10cea85](https://github.com/Surfline/wavetrak-services/commit/10cea85))





## <small>1.0.2 (2021-02-23)</small>

* fix: use @babel/eslint-parser ([0efcad8](https://github.com/Surfline/wavetrak-services/commit/0efcad8))
* fix: use new babel parser ([3fbf675](https://github.com/Surfline/wavetrak-services/commit/3fbf675))
* chore: lerna version ([6621e41](https://github.com/Surfline/wavetrak-services/commit/6621e41))





## <small>1.0.1 (2021-02-23)</small>

* fix: use @babel/eslint-parser ([0efcad8](https://github.com/Surfline/wavetrak-services/commit/0efcad8))





## 1.0.0 (2021-02-23)

* chore: small modifications to config in line with version updates ([dea8ac6](https://github.com/Surfline/wavetrak-services/commit/dea8ac6))
* chore!: bump dependencies ([55903e9](https://github.com/Surfline/wavetrak-services/commit/55903e9))


### BREAKING CHANGE

* Bumped several packages by major versions, lint rules may change




## <small>0.2.1 (2020-11-04)</small>

* PS-000 - Replace artifactoryonline.com references with jfrog.io. (#3664) ([21430cd](https://github.com/Surfline/wavetrak-services/commit/21430cd)), closes [#3664](https://github.com/Surfline/wavetrak-services/issues/3664)





## 0.2.0 (2020-08-10)

* feat: Add typescript web eslint config ([f7058dd](https://github.com/Surfline/wavetrak-services/commit/f7058dd))





## <small>0.1.4 (2020-06-29)</small>

* chore: update dependencies ([ba9022d](https://github.com/Surfline/wavetrak-services/commit/ba9022d))





## <small>0.1.3 (2020-05-12)</small>

* chore: update eslint-web config dependencies ([f5f109c](https://github.com/Surfline/wavetrak-services/commit/f5f109c))





## <small>0.1.2 (2020-03-23)</small>

* chore: Move @surfline/eslint-config-web to wavetrak-services repo and update deps ([35b6a84](https://github.com/Surfline/wavetrak-services/commit/35b6a84))
