module.exports = {
  extends: ['prettier', 'airbnb/hooks', 'plugin:jsx-a11y/recommended'],
  settings: {
    react: {
      version: 'detect',
    },
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: ['react', 'react-hooks'],
  rules: {
    'react/jsx-filename-extension': [2, { extensions: ['.js', '.jsx', '.tsx'] }],
    'react-hooks/rules-of-hooks': 'error', // Checks rules of Hooks
    'react-hooks/exhaustive-deps': 'error', // Checks effect dependencies
    'react/jsx-props-no-spreading': 0,
    'jsx-a11y/no-static-element-interactions': [0],
    'react/function-component-definition': 'off',
    'react/sort-comp': [
      2,
      {
        order: ['static-variables', 'static-methods', 'lifecycle', 'everything-else', 'render'],
      },
    ],
  },
};
