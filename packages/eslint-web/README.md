# `@surfline/eslint-config-web`

This package stores the eslint config for Surfline React Apps.

## Usage

This ESLint configuration comes with multiple configurations which can be used individually or in tandem. We have made a distinction here between a
"base" configuration which is generic to code that is intended to be used on the web, and a "react" configuration which is intended for linting react
specific code.

In order to use the configurations, they can be consumed as follows.

#### Base configuration (without React)

```json
// .eslintrc
{
  "extends": ["@surfline/eslint-config-web/base"]
}
```

#### Base configuration (with React)

```json
// .eslintrc
{
  "extends": ["@surfline/eslint-config-web/base", "@surfline/eslint-config-web/react"]
}
```

#### React configuration only (unlikely use case)

```json
// .eslintrc
{
  "extends": ["@surfline/eslint-config-web/react"]
}
```

## What does this config include?

The base configuration includes the following:
- [Airbnb ESLint configuration](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb)
- [Prettier ESLint configuration](https://github.com/prettier/eslint-config-prettier)
- Some basic overrides for rules based on conventions established at Surfline

The react configuration includes:
- [jsx-a11y](https://www.npmjs.com/package/eslint-plugin-jsx-a11y)
- [Eslint Plugin React](https://github.com/yannickcr/eslint-plugin-react)
- [Eslint Plugin React Hooks](https://www.npmjs.com/package/eslint-plugin-react-hooks)
- [Prettier ESLint configuration for react](https://github.com/prettier/eslint-config-prettier/blob/master/react.js)

## Publishing NPM package

Within the `/packages/` directory we publish NPM packages using Lerna.

A step by step guide for publishing NPM packages in the monorepo can be found [here](../../README.md#npm-packages).
