module.exports = {
  root: true,
  extends: ['airbnb', 'prettier'],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    requireConfigFile: false,
  },
  parser: '@babel/eslint-parser',
  globals: {
    after: true,
    afterEach: true,
    before: true,
    beforeEach: true,
    describe: true,
    document: 'off',
    history: 'off',
    it: true,
    location: 'off',
    window: 'off',
  },
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  plugins: ['prettier'],
  rules: {
    'import/no-extraneous-dependencies': [2, { devDependencies: true }],
    'import/prefer-default-export': 'off',
    'no-return-assign': 1,
    'no-shadow': [2, { allow: ['_'] }],
    'no-underscore-dangle': [2, { allow: ['_id'] }],
    'prefer-destructuring': [
      'error',
      {
        VariableDeclarator: {
          array: true,
          object: true,
        },
        AssignmentExpression: {
          array: false,
          object: false,
        },
      },
    ],
    'prettier/prettier': 'error',
    yoda: [2, 'never', { exceptRange: true }],
  },
};
