module.exports = {
  // We use overrides here so that we can apply these rules only to typescript files
  // but also so that we can use these in tandem with JS files for places that have both
  parser: '@typescript-eslint/parser',
  parserOptions: {
    sourceType: 'module',
    project: './tsconfig.json',
  },
  env: {
    browser: true,
    es6: true,
  },
  extends: ['airbnb', 'airbnb-typescript/base', 'prettier'],
  plugins: ['import', 'prettier', '@typescript-eslint'],
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.ts', '.jsx', '.tsx', '.json'],
      },
    },
    'import/extensions': ['.js', '.ts', '.jsx', '.tsx'],
  },
  rules: {
    'prettier/prettier': 'error',
    'no-underscore-dangle': [2, { allow: ['_id'] }],
    '@typescript-eslint/naming-convention': 'off',
    '@typescript-eslint/no-unused-vars': 'error',
    'import/no-extraneous-dependencies': [
      2,
      {
        devDependencies: ['**/*.spec.ts', '**/*.spec.tsx'],
      },
    ],
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        jsx: 'never',
        ts: 'never',
        tsx: 'never',
      },
    ],
  },
};
