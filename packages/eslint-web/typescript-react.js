module.exports = {
  extends: ['airbnb-typescript'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    'react/prop-types': ['off', {}],
    'react/require-default-props': 0,
    'react/function-component-definition': 'off',
  },
};
