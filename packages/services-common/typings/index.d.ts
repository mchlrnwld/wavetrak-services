/// <reference types="@splitsoftware/splitio" />

declare module '@surfline/bunyan-logsene' {
  import stream = require('stream');
  class Logsene extends stream.Writable {
    constructor(options?: { token?: string });
  }
  export = Logsene;
}
