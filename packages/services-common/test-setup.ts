/* eslint-disable import/no-extraneous-dependencies */
import chai from 'chai';
import chaiHttp from 'chai-http';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import chaiAlmost from 'chai-almost';

chai.use(chaiHttp);
chai.use(dirtyChai);
chai.use(sinonChai);
chai.use(chaiAlmost());
