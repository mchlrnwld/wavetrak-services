# Services Common

This package can be imported from `@surfline/services-common`. It contains application code that is common across service applications.

## Namespacing and importing from `services-common`

Following the pattern established in `@surfline/web-common` and `@surfline/admin-common`, we namespace directories to keep code organized and exports isolated. For example, the `/src/science` directory contains code common to science API's and these exports are not available from the root. To import from `@surfline/services-common`, reach into the `/dist` to the relevant path:
```
  import { initBestGrid } from '@surfline/services-common/dist/science/bestGrid';
```

## Publishing `services-common`

See #Lerna in the README in the root of the repository for information on publishing `services-common`
