/* istanbul ignore file */
import bunyan from 'bunyan';
import Logsene from '@surfline/bunyan-logsene';
import PrettyStream from 'bunyan-prettystream';
import { SurflineLogger } from './types';

let logseneStream: Logsene;

export const setupLogsene = (logseneKey: string): void => {
  if (!logseneStream) {
    logseneStream = new Logsene({
      token: logseneKey,
    });
  }
};

// Using hadfieldn's fork of node-bunyan-prettystream
// to handle pretty printing with circular reference safety
const prettyStdOut = new PrettyStream();
prettyStdOut.pipe(process.stdout);

/**
 * Use example:
 *
 * // In ES6 using "import { x } from 'y'" is analogous to overwriting module.exports with a
 * // function in ES5. * as logger makes writing unit tests easier.
 * import * as logger from '../common/logger';
 *
 * export function MyController() {
 *    const log = logger.logger('user-service');
 *    log.trace('MyController created');
 * }
 *
 *
 * Test example:
 *
 * import sinon from 'sinon';
 * import * as logger from '../common/logger';
 * import * as MyController from './MyController';
 *
 * sinon.stub(logger, 'logger').returns({});
 *
 * @param name of service
 */
const logger = (name: string): SurflineLogger => {
  if (!logseneStream) {
    throw new Error(
      'Logsene not instaniated. You must call "setupLogsene(logseneKey)" before setting up a logger',
    );
  }

  const log = bunyan.createLogger({
    name: name || 'default-js-logger',
    serializers: bunyan.stdSerializers,
    streams: [
      {
        level: (process.env.CONSOLE_LOG_LEVEL as bunyan.LogLevel) || 'info',
        type: 'raw',
        stream: prettyStdOut,
      },
      {
        level: (process.env.LOGSENE_LEVEL as bunyan.LogLevel) || 'info',
        stream: logseneStream,
        type: 'raw',
        reemitErrorEvents: true,
      },
    ],
  });

  // TODO: Do we need these err and stream parameters for something? Does changing the signature
  //       of the function we pass in affect the behavior of this stream?
  // eslint-disable-next-line no-unused-vars
  log.on('error', (_err, _stream) => {
    console.error('Error logging.');
  });
  log.trace(`${name} logging started.`);
  return log;
};

export default logger;
