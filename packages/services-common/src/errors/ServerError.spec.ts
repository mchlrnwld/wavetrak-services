import { expect } from 'chai';
import ServerError from './ServerError';

describe('ServerError', () => {
  it('it should throw an error with a message and name', () => {
    const castError = new ServerError({ message: 'TEST', name: 'CastError' });
    expect(castError.message).to.be.equal('TEST');
    expect(castError.statusCode).to.be.equal(400);
    expect(castError.name).to.be.equal('ServerError');

    const validationError = new ServerError({ name: 'ValidationError' });
    expect(validationError.message).to.be.equal('Unknown Server Error');
    expect(validationError.statusCode).to.be.equal(400);
    expect(validationError.name).to.be.equal('ServerError');

    const validatorError = new ServerError({ name: 'ValidatorError' });
    expect(validatorError.message).to.be.equal('Unknown Server Error');
    expect(validatorError.statusCode).to.be.equal(400);
    expect(validatorError.name).to.be.equal('ServerError');

    const mongoError = new ServerError({ name: 'MongoError' });
    expect(mongoError.message).to.be.equal('Unknown Server Error');
    expect(mongoError.statusCode).to.be.equal(400);
    expect(mongoError.name).to.be.equal('ServerError');

    const notFoundError = new ServerError({ name: 'NotFound' });
    expect(notFoundError.message).to.be.equal('Unknown Server Error');
    expect(notFoundError.statusCode).to.be.equal(404);
    expect(notFoundError.name).to.be.equal('ServerError');
  });

  it('it should throw an error with a default message if omitted', () => {
    const error = new ServerError();
    expect(error.message).to.be.equal('Unknown Server Error');
    expect(error.statusCode).to.be.equal(500);
    expect(error.name).to.be.equal('ServerError');
  });
});
