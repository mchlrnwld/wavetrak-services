import { expect } from 'chai';
import NotFound from './NotFound';

describe('NotFound', () => {
  it('it should throw an error with a message', () => {
    const error = new NotFound('not found message');
    expect(error.message).to.be.equal('not found message');
    expect(error.statusCode).to.be.equal(404);
    expect(error.name).to.be.equal('NotFound');
  });

  it('it should throw an error with a default message if omitted', () => {
    const error = new NotFound();
    expect(error.message).to.be.equal('Not Found');
    expect(error.statusCode).to.be.equal(404);
    expect(error.name).to.be.equal('NotFound');
  });
});
