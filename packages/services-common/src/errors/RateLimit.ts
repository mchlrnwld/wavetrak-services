import BaseError from './BaseError';

class RateLimit extends BaseError {
  constructor(message = 'You have exceeded your request limit.', status = 429) {
    super('RateLimit', message, { statusCode: status });
  }
}

export default RateLimit;
