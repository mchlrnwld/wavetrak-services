type Data = Record<string, unknown>;

type Payload = {
  code?: number;
  data?: Data;
  errorHandler: string;
  message: string;
  name: string;
  statusCode: number;
};

export interface BaseErrorOptions
  {
    statusCode?: number,
    wavetrakCode?: number,
    data?: Data,
  }

export class BaseError extends Error {
  data: Data | undefined;
  errorHandler: string;
  notice: boolean;
  statusCode: number;
  wavetrakCode?: number;

  constructor(name: string, message: string, options?: BaseErrorOptions) {
    super();
    this.message = message;
    this.statusCode = options?.statusCode ?? 500;
    this.name = name;
    this.errorHandler = this.name;
    this.stack = new Error().stack;
    this.notice = this.statusCode >= 500;
    this.wavetrakCode = options?.wavetrakCode;
    this.data = options?.data;
  }

  setMessage = (message: string): void => {
    this.message = message;
  };

  setStatusCode = (statusCode: number): void => {
    this.statusCode = statusCode;
  };

  setName = (name: string): void => {
    this.name = name;
    this.errorHandler = name;
  };

  toJSON = (): Payload => {
    const payload: Payload = {
      statusCode: this.statusCode,
      name: this.name,
      message: this.message,
      errorHandler: this.errorHandler,
      ...(this.wavetrakCode ? { code: this.wavetrakCode } : {}),
      ...(this.data ? { data: this.data } : {}),
    };

    return payload;
  };
}

export default BaseError;
