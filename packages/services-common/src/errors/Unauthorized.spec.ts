import { expect } from 'chai';
import Unauthorized from './Unauthorized';

describe('Unauthorized', () => {
  it('it should throw an error with a message and name', () => {
    const error = new Unauthorized('auth', 403);
    expect(error.message).to.be.equal('auth');
    expect(error.statusCode).to.be.equal(403);
    expect(error.name).to.be.equal('Unauthorized');
  });

  it('it should throw an error with a default message if omitted', () => {
    const error = new Unauthorized();
    expect(error.message).to.be.equal('You are unauthorized to view this resource');
    expect(error.statusCode).to.be.equal(401);
    expect(error.name).to.be.equal('Unauthorized');
  });
});
