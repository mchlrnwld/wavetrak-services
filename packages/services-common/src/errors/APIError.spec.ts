import { expect } from 'chai';
import APIError from './APIError';

describe('APIError', () => {
  it('it should throw an error with a message', () => {
    const error = new APIError('some message', { statusCode: 410 });
    expect(error.message).to.be.equal('some message');
    expect(error.statusCode).to.be.equal(410);
    expect(error.name).to.be.equal('APIError');
  });

  it('it should throw an error with a message', () => {
    const error = new APIError('some other message', { data: {
      title: 'API Error Title',
      testID: 100,
    } });
    expect(error.message).to.be.equal('some other message');
    expect(error.statusCode).to.be.equal(400);
    expect(error.data?.title).to.be.equal('API Error Title');
    expect(error.data?.testID).to.be.equal(100);
    expect(error.name).to.be.equal('APIError');
  });
});
