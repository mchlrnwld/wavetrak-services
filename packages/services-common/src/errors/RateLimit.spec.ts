import { expect } from 'chai';
import RateLimit from './RateLimit';

describe('RateLimit', () => {
  it('it should throw an error with a message', () => {
    const error = new RateLimit('exceeded rate', 428);
    expect(error.message).to.be.equal('exceeded rate');
    expect(error.statusCode).to.be.equal(428);
    expect(error.name).to.be.equal('RateLimit');
  });

  it('it should throw an error with a default message if omitted', () => {
    const error = new RateLimit();
    expect(error.message).to.be.equal('You have exceeded your request limit.');
    expect(error.statusCode).to.be.equal(429);
    expect(error.name).to.be.equal('RateLimit');
  });
});
