import { expect } from 'chai';
import BaseError from './BaseError';

describe('BaseError', () => {
  it('should create a base error with defaults', () => {
    const error = new BaseError('TESTNAME', 'TESTMESSAGE');
    expect(error.message).to.be.equal('TESTMESSAGE');
    expect(error.statusCode).to.be.equal(500);
    expect(error.name).to.be.equal('TESTNAME');
    expect(error.errorHandler).to.be.equal('TESTNAME');
    expect(error.notice).to.be.true();
    expect(error.wavetrakCode).to.be.undefined();
  });

  it('should change properties with setters', () => {
    const error = new BaseError('TESTNAME', 'TESTMESSAGE');
    error.setMessage('new message');
    error.setName('some name');
    error.setStatusCode(400);
    const toJSON = error.toJSON();
    expect(error.message).to.be.equal('new message');
    expect(error.statusCode).to.be.equal(400);
    expect(error.name).to.be.equal('some name');
    expect(error.errorHandler).to.be.equal('some name');
    expect(toJSON).to.deep.equal({
      errorHandler: 'some name',
      message: 'new message',
      name: 'some name',
      statusCode: 400,
    });
  });

  it('should add the wavetrak code to the payload if it exists', () => {
    const error = new BaseError('TESTNAME', 'TESTMESSAGE', {
      statusCode: 400,
      wavetrakCode: 1001,
    });
    const toJSON = error.toJSON();
    expect(toJSON).to.deep.equal({
      code: 1001,
      errorHandler: 'TESTNAME',
      message: 'TESTMESSAGE',
      name: 'TESTNAME',
      statusCode: 400,
    });
  });

  it('should create a base error with statusCode and surflineCode', () => {
    const error = new BaseError('TESTNAME', 'TESTMESSAGE', {
      statusCode: 400,
      wavetrakCode: 10001,
    });
    expect(error.message).to.be.equal('TESTMESSAGE');
    expect(error.statusCode).to.be.equal(400);
    expect(error.name).to.be.equal('TESTNAME');
    expect(error.errorHandler).to.be.equal('TESTNAME');
    expect(error.notice).to.be.false();
    expect(error.wavetrakCode).to.be.equal(10001);
  });

  it('should create a base error with data', () => {
    const error = new BaseError('TESTNAME', 'TESTMESSAGE', {
      statusCode: 400,
      data: { test: 'testing' },
    });
    const toJSON = error.toJSON();
    expect(toJSON).to.deep.equal({
      data: { test: 'testing' },
      errorHandler: 'TESTNAME',
      message: 'TESTMESSAGE',
      name: 'TESTNAME',
      statusCode: 400,
    });
  });
});
