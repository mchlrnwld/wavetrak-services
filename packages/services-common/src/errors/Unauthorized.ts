import BaseError from './BaseError';

class Unauthorized extends BaseError {
  constructor(message = 'You are unauthorized to view this resource', status = 401) {
    super('Unauthorized', message, { statusCode: status });
  }
}

export default Unauthorized;
