import { BaseError, BaseErrorOptions } from './BaseError';

class APIError extends BaseError {
  constructor(message: string, options?: BaseErrorOptions | undefined) {
    super(
      'APIError',
      message,
      {
        statusCode: options?.statusCode ?? 400,
        wavetrakCode: options?.wavetrakCode ?? undefined,
        data: options?.data ?? undefined,
      }
    );
  }
}

export default APIError;
