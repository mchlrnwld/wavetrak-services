import BaseError from './BaseError';

class NotFound extends BaseError {
  constructor(message?: string) {
    super('NotFound', message || 'Not Found', { statusCode: 404 });
  }
}

export default NotFound;
