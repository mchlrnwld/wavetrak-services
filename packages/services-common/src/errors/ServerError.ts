import BaseError from './BaseError';

interface ServerErr {
  message?: string;
  name?: string;
}

class ServerError extends BaseError {
  constructor(err?: ServerErr) {
    const message = err?.message || 'Unknown Server Error';
    super('ServerError', message, { statusCode: ServerError.getStatusCode(err) });
  }

  static getStatusCode(err?: ServerErr): number {
    switch (err?.name) {
      case 'CastError':
        return 400;
      case 'ValidationError':
        return 400;
      case 'ValidatorError':
        return 400;
      case 'MongoError':
        return 400;
      case 'NotFound':
        return 404;
      default:
        return 500;
    }
  }
}

export default ServerError;
