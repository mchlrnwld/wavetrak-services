/**
 * Options that can be provided to WavetrakCache.fetch function
 */
type CacheOptions = {
  expiration?: number | null | undefined;
  params?: Record<string, unknown> | null | undefined;
  overwrite?: boolean | null | undefined;
};

export default CacheOptions;
