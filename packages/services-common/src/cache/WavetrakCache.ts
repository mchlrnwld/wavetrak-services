import CacheOptions from './CacheOptions';

interface WavetrakCache {
  /**
   * Fetches the resource given the key and options. If there is nothing
   * in the cache, will call the `resolver` function and cache the result of that.
   *
   * @example
   * const spotReport = await fetch('SPOT_REPORTS', async () => await getSpotReport(spotId), { params: { spotId }});
   *
   * @param {string} key named identifier component for compound hash key
   * @param {function} resolver Function to get the data if it's not in the cache.
   * @param {CacheOptions} options Options for the cache request
   * @return {any} data
   */
  fetch<T>(
    key: string,
    resolver: () => Promise<T>,
    options?: CacheOptions | null | undefined,
  ): Promise<T>;

  /**
   * Clears a given key. Use with caution, most cases should not need to utilize this.
   *
   * @example
   * const spotReport = await clear('SPOT_REPORTS', { params: { spotId }});
   *
   * @param {string} key named identifier component for compound hash key
   * @param {params}
   * @return {Promise}
   */
  clear(key: string, params?: Record<string, unknown> | null | undefined): Promise<void>;
}

export default WavetrakCache;
