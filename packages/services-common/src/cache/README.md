# Description

Cache module which makes it easier to use a centralized cache store.

The module wraps Redis and provides some simple cache functionality methods, without exposing the underlying cache mechanism.



The `cache` currently functions primarily as a look-aside style cache.

More info [here](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/437584148/Caching+Surfline)

# Usage

Use the provided helper function
```js
import { initCache, cache } from '@surfline/services-common';

await initCache(initCacheOptions);

const spotReport = await cache.fetch('SPOT_REPORT_VIEW',  async () => await getSpotReport(spotId, units), cacheOptions);
```

...Or utilize your own instance via the constructor, which takes a `RedisClient`.
```js
import { RedisCache, WavetrakCache } from '@surfline/services-common';

const myRedisCache: WavetrakCache = new RedisCache(myRedisClient);

const spotReport = await myRedisCache.fetch('SPOT_REPORT_VIEW', async () => await getSpotReport(spotId, units), cacheOptions);
```

### Clearing a key
```js
await cache.clear('SPOT_REPORT_VIEW', { spotId: 123 } );
