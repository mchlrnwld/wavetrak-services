import { RedisClient } from 'redis';
import { SurflineLogger } from '../types';

/**
 * Options that can be provided to the initCache function
 */
type CacheInitOptions = {
  defaultCacheExpiration?: number | undefined;
  enabled: boolean;
  logger?: SurflineLogger | null | undefined;
  prefix?: string | undefined;
  readRedis: RedisClient | string;
  writeRedis: RedisClient | string;
};

export default CacheInitOptions;
