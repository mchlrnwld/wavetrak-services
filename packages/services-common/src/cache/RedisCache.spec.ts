import { expect } from 'chai';
import sinon from 'sinon';
import { RedisClient, Callback } from 'redis';
import RedisCache from './RedisCache';
import CacheOptions from './CacheOptions';

describe('WavetrakCache', () => {
  let redisCache: RedisCache;

  /* eslint-disable @typescript-eslint/no-explicit-any */
  const readRedisClientMock: any = {
    get: sinon.stub() as any,
  };
  const writeRedisClientMock: any = {
    set: sinon.stub() as any,
    del: sinon.stub() as any,
  };
  /* eslint-enable @typescript-eslint/no-explicit-any */

  const getMock = (_: string, cb: Callback<string | null>) => { cb(null, JSON.stringify('foo')); return true; };
  const getNullMock = (_: string, cb: Callback<string | null>): boolean => { cb(null, null); return true; };

  beforeEach(() => {
    readRedisClientMock.get = sinon.stub();
    writeRedisClientMock.set = sinon.stub();
    writeRedisClientMock.del = sinon.stub();
  });

  afterEach(() => {
    readRedisClientMock.get.reset();
    writeRedisClientMock.set.reset();
    writeRedisClientMock.del.reset();
  });

  describe('fetch', () => {
    beforeEach(() => {
      redisCache = new RedisCache(
        readRedisClientMock as RedisClient,
        writeRedisClientMock as RedisClient
      );
    });

    it('checks the cache for a key and calls the resolver if not found', async () => {
      readRedisClientMock.get = sinon.stub().callsFake(getNullMock);
      const result = await redisCache.fetch('foo', async () => 'foo');
      expect(readRedisClientMock.get).to.have.been.calledOnce();
      expect(writeRedisClientMock.set).to.have.been.calledOnce();
      expect(result).to.equal('foo');
    });

    it('checks the cache for a key and returns the value if found', async () => {
      readRedisClientMock.get = sinon.stub().callsFake(getMock);
      const result = await redisCache.fetch('foo', async () => 'bar');
      expect(readRedisClientMock.get).to.have.been.calledOnce();
      expect(writeRedisClientMock.set).to.not.have.been.called();
      expect(result).to.equal('foo');
    });

    it('uses options to alter the redis request', async () => {
      const options: CacheOptions = {
        expiration: 1,
        params: { id: 123 },
      };
      readRedisClientMock.get = sinon.stub().callsFake(getNullMock);
      await redisCache.fetch('foo', async () => 'foo', options);
      expect(writeRedisClientMock.set).to.have.been.calledOnceWithExactly(
        'wt-cache:foo:id=123',
        JSON.stringify('foo'),
        'EX',
        1
      );
    });

    it('use options to bypass the look-aside', async () => {
      readRedisClientMock.get = sinon.stub().callsFake(getMock);
      await redisCache.fetch('foo', async () => 'foo', { overwrite: true });
      expect(readRedisClientMock.get).to.not.have.been.called();
      expect(writeRedisClientMock.set).to.have.been.calledOnce();
    });
  });

  describe('clear', () => {
    beforeEach(() => {
      redisCache = new RedisCache(
        readRedisClientMock as RedisClient,
        writeRedisClientMock as RedisClient
      );
    });

    it('calls clear for a given key', async () => {
      redisCache.clear('foo');
      expect(writeRedisClientMock.del).to.have.been.calledOnce();
    });
  });
});
