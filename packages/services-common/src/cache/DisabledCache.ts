import WavetrakCache from './WavetrakCache';

/**
 * An implementation for WavetrakCache that ignores any cache functionality
 */
class DisabledCache implements WavetrakCache {
  /**
   * Calls the resolver immediately
   */
  fetch = async <T>(_: string, resolver: () => Promise<T>): Promise<T> => await resolver();
  /**
   *  Does nothing.
   */
  clear = async (): Promise<void> => undefined;
}

export default DisabledCache;
