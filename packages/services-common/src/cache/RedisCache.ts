import { RedisClient } from 'redis';
import { random, isNil } from 'lodash';
import CacheOptions from './CacheOptions';
import WavetrakCache from './WavetrakCache';

/**
 * A Redis-based cache implementation.
 */
class RedisCache implements WavetrakCache {
  /**
   * RedisClient for making Redis GET commands.
   */
  readonly _readClient: RedisClient;

  /**
   * RedisClient for making Redis SET commands.
   */
  readonly _writeClient: RedisClient;

  /**
   * Default cache expiration (TTL). To be used in all cases without an explicit expiration.
   */
  readonly _defaultCacheExpiration: number;

  constructor(readClient: RedisClient, writeClient: RedisClient, defaultCacheExpiration = 300) {
    this._readClient = readClient;
    this._writeClient = writeClient;
    this._defaultCacheExpiration = defaultCacheExpiration;
  }

  /**
   * Normalizes parameters by sorting based on param key
   * @param {Record<string, unknown>} params Plain object of key/vals to be used to form cache key.
   * @return {string} normalized parameters
   */
  protected static normalizeParameters = (
    params: Record<string, unknown> | null | undefined,
  ): string => {
    return params
      ? Object.keys(params)
          .sort()
          .map((key) => `${key}=${params[key]}`)
          .join('&')
      : 'nofield';
  };

  /**
   * Uses the concept of jitter to add a random percentage of the original cache timing to the TTL.
   * @param {number} ttl original cache TTL
   * @return {number} modified cache TTL
   */
  protected static addCacheJitter = (ttl: number): number => Math.round(ttl + random(0, ttl * 0.2));

  /**
   * Forms internal cacheKey used in Redis commands.
   * @param {string} key key passed to cache
   * @param {string} params normalized params
   * @return {string} internal key
   */
  protected static formKey = (
    key: string,
    params: Record<string, unknown> | null | undefined,
  ): string => {
    const normalized = RedisCache.normalizeParameters(params);
    return `wt-cache:${key}:${normalized}`;
  };

  /**
   * Internally wraps Redis set
   * @param {string} key Key to store the data in Redis with.
   * @param {unknown} data The data to store (will be serialized).
   * @param {number} ttl Expiration time in seconds, null results in the default.
   * @return {bool} Success
   */
  protected putCached = (key: string, data: unknown, ttl: number): boolean => {
    return this._writeClient.set(
      key,
      JSON.stringify(data),
      'EX',
      RedisCache.addCacheJitter(ttl),
    ) as boolean;
  };

  /**
   * Internally wraps Redis get
   * @param {string} key Key to pull from the cache.
   * @return {Promise<T>} Promise resolves with the value or an error.
   */
  protected getCached = <T>(key: string): Promise<T | null> =>
    new Promise((resolve, reject) =>
      this._readClient.get(key, (err, result) => {
        if (err) return reject(err);
        if (isNil(result)) {
          return resolve(result);
        }
        try {
          return resolve(JSON.parse(result));
        } catch (error) {
          return reject(error);
        }
      }),
    );

  /**
   * RedisCache implementation of WavetrakCache.fetch
   */
  fetch = async <T>(
    key: string,
    resolver: () => Promise<T>,
    options?: CacheOptions | null | undefined,
  ): Promise<T> => {
    const internalKey = RedisCache.formKey(key, options?.params);
    if (!options?.overwrite) {
      const data = (await this.getCached(internalKey)) as T;
      if (!isNil(data)) {
        return data;
      }
    }
    const data = await resolver();
    if (!isNil(data)) {
      await this.putCached(internalKey, data, options?.expiration ?? this._defaultCacheExpiration);
    }
    return data;
  };

  /**
   * RedisCache implementation of WavetrakCache.clear
   */
  clear = (key: string, params?: Record<string, unknown> | null | undefined): Promise<void> =>
    new Promise((resolve, reject) => {
      const internalKey = RedisCache.formKey(key, params);

      return this._writeClient.del(internalKey, (err) => {
        if (err) return reject(err);

        return resolve();
      });
    });
}

export default RedisCache;
