/* istanbul ignore file */

import Analytics from 'analytics-node';
import { SurflineLogger, BrandAbbr, Identity, BrandLoggers, BrandAnalytics } from './types';
import { brandMap, SL, BW, FS, CW, WT, MSW } from './constants';

const brandLoggers: BrandLoggers = {
  sl: console,
  bw: console,
  cw: console,
  fs: console,
  wt: console,
  msw: console,
};

const brandAnalytics: BrandAnalytics = {
  sl: null,
  bw: null,
  cw: null,
  fs: null,
  wt: null,
  msw: null,
};

const setupAnalytics = (brandAbbr: BrandAbbr) => (
  writeKey: string | undefined,
  log?: SurflineLogger,
): Analytics => {
  const analytics = brandAnalytics[brandAbbr];
  if (analytics) {
    return analytics;
  }

  // Create a logger specific to this brand instance of the logger
  if (log) {
    brandLoggers[brandAbbr] = log;
  }
  const logger = brandLoggers[brandAbbr];

  const createdAnalytics = new Analytics(writeKey || 'NoWrite', {
    flushAt: 1,
  });
  brandAnalytics[brandAbbr] = createdAnalytics;

  logger.info(`${brandMap[brandAbbr]} Analytics Setup Complete`);

  return createdAnalytics;
};

export const trackEvent = (
  brand: BrandAbbr,
  event: string,
  identity: Identity,
  properties: { [key: string]: unknown },
  callback = (): void => {
    return;
  },
): Analytics => {
  const analytics = brandAnalytics[brand];
  if (!analytics) {
    throw new Error(
      `Called \`trackEvent\` before ${brandMap[brand]} analytics were set up.` +
        'Ensure that you call `setupAnalytics` when starting up the service',
    );
  }
  return analytics.track({ ...identity, event, properties }, callback);
};

export const identify = (
  brand: BrandAbbr,
  userId: string,
  traits: { [key: string]: unknown },
  options: { [key: string]: unknown }, 
  callback = (): void => {
    return;
  },
): Analytics => {
  const analytics = brandAnalytics[brand];
  if (!analytics) {
    throw new Error(
      `Called \`identify\` before ${brandMap[brand]} analytics were set up.` +
        'Ensure that you call `setupAnalytics` when starting up the service',
    );
  }
  return analytics.identify({ userId, traits, ...options }, callback);
};

export const setupSurflineAnalytics = setupAnalytics(SL);
export const setupBuoyweatherAnalytics = setupAnalytics(BW);
export const setupFishtrackAnalytics = setupAnalytics(FS);
export const setupCoastalwatchAnalytics = setupAnalytics(CW);
export const setupWavetrakAnalytics = setupAnalytics(WT);
export const setupMagicseaweedAnalytics = setupAnalytics(MSW);

// Export in order to prevent garbage collection of `brandAnalytics`
export default (): BrandAnalytics => brandAnalytics;

