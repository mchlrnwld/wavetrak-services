import { expect } from 'chai';
import sinon from 'sinon';
import { initCache, cache } from './initCache';
import CacheInitOptions from './cache/CacheInitOptions';
import * as createRedisClient from './utils/createRedisClient';

describe('wt-cache', () => {
  /* eslint-disable @typescript-eslint/no-explicit-any */
  const redisClientMock: any = {
    once: sinon.stub() as any,
    on: sinon.stub() as any,
  };
  /* eslint-enable @typescript-eslint/no-explicit-any */

  const initOptions: CacheInitOptions = {
    enabled: true,
    defaultCacheExpiration: 1,
    logger: null,
    prefix: 'unit-test',
    readRedis: '8.8.8.8:1234',
    writeRedis: '8.8.8.8:4321',
  };

  redisClientMock.on = sinon.stub();
  redisClientMock.once = sinon.stub();
  const createClientStub = sinon.stub(createRedisClient, 'default').resolves(redisClientMock);

  afterEach(() => {
    createClientStub.resetHistory();
    redisClientMock.on.reset();
    redisClientMock.once.reset();
  });

  describe('initCache', () => {
    it('should use the provided host/port', async () => {
      await initCache(initOptions);
      expect(createClientStub).to.have.been.calledTwice();
      expect(cache).to.not.be.null();
      expect(createClientStub.firstCall).to.have.been.calledWith(
        '8.8.8.8',
        '1234',
        console,
        { prefix: 'unit-test'}
      );
      expect(createClientStub.secondCall).to.have.been.calledWith(
        '8.8.8.8',
        '4321',
        console,
        { prefix: 'unit-test'}
      );
    });

    it('should not initiate the redis client if it is not enabled', async () => {
      await initCache({ enabled: false } as CacheInitOptions);
      expect(createClientStub).to.not.have.been.called();
      expect(cache).to.not.be.null();
    });
  });
});
