import setupExpress from './setupExpress';
import wrapErrors from './wrapErrors';
import wrapErrorsToNewRelic from './wrapErrorsToNewRelic';
import { UnauthorizedError, APIError, BaseError, NotFound, RateLimit, ServerError } from './errors';
import {
  authenticateRequest,
  errorHandlerMiddleware,
  geoCountryIso,
  geoLatLon,
} from './middlewares';
import initMongo from './initMongo';
import { initRedis, getPromisifiedRedis, redisClient, PromisifiedRedis } from './setupRedis';
import logger, { setupLogsene } from './logger';
import { setupFeatureFramework, getTreatment, getTreatmentWithConfig } from './splitio';
import {
  setupSurflineAnalytics,
  setupWavetrakAnalytics,
  setupFishtrackAnalytics,
  setupBuoyweatherAnalytics,
  setupCoastalwatchAnalytics,
  setupMagicseaweedAnalytics,
  trackEvent,
  identify,
} from './analytics';
import {
  convertWaveHeightsUnits,
  getSecret,
  generateGrid,
  limitBounds,
  getPlatform,
} from './utils';

import { cache, initCache } from './initCache';

import type {
  CacheOptions,
  DisabledCache,
  InitCacheOptions,
  RedisCache,
  WavetrakCache,
} from './cache';

import type {
  SurflineRequest,
  SurflineLogger,
  SurflineQuery,
  SurflineHeaders,
  BrandLoggers,
  BrandAnalytics,
  BrandAbbr,
  Brand,
  Identity,
} from './types';

export {
  setupExpress,
  wrapErrors,
  wrapErrorsToNewRelic,
  UnauthorizedError,
  APIError,
  BaseError,
  NotFound,
  RateLimit,
  ServerError,
  authenticateRequest,
  errorHandlerMiddleware,
  geoLatLon,
  geoCountryIso,
  cache,
  initCache,
  initMongo,
  initRedis,
  getPromisifiedRedis,
  redisClient,
  logger,
  logger as createLogger,
  setupLogsene,
  getTreatment,
  getTreatmentWithConfig,
  setupFeatureFramework,
  setupMagicseaweedAnalytics,
  setupCoastalwatchAnalytics,
  setupBuoyweatherAnalytics,
  setupSurflineAnalytics,
  setupWavetrakAnalytics,
  setupFishtrackAnalytics,
  trackEvent,
  identify,
  getSecret,
  generateGrid,
  limitBounds,
  convertWaveHeightsUnits,
  getPlatform,
};
export * from './constants';

export type {
  CacheOptions,
  DisabledCache,
  SurflineHeaders,
  SurflineQuery,
  SurflineLogger,
  SurflineRequest,
  BrandAnalytics,
  BrandLoggers,
  Brand,
  BrandAbbr,
  Identity,
  InitCacheOptions,
  PromisifiedRedis,
  RedisCache,
  WavetrakCache,
};
