import newrelic from 'newrelic';
import mongoose, { CastError } from 'mongoose';
import { Request, Response, NextFunction } from 'express';
import * as SurflineErrors from '../errors';
import { SurflineLogger } from '../types';

export interface MongoError {
  message: string;
  name: string;
  errors?: string | string[];
  code?: number;
}

type Errors =
  | Error
  | SurflineErrors.BaseError
  | CastError
  | MongoError
  | mongoose.Error.ValidationError
  | SurflineErrors.APIError
  | SurflineErrors.RateLimit
  | SurflineErrors.NotFound
  | SurflineErrors.ServerError
  | SurflineErrors.UnauthorizedError;

/**
 * @description Pretty prints the error and sends the logs to logsene
 */
/* istanbul ignore next */
const logError = (log: SurflineLogger | Console, err: Error): void => {
  try {
    log.error({
      err,
      message: 'Error encountered (Common Handler)',
    });
  } catch (fatalErr) {
    console.log(err);
    console.log(fatalErr);
  }
};

// next is required in the function signature for the middleware
export const errorHandlerMiddleware = (log: SurflineLogger | Console) => async (
  err: Errors,
  _: Request,
  res: Response,
  _next: NextFunction,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<Response<any>> => {
  logError(log, err);

  const { statusCode } = err as SurflineErrors.BaseError;

  if (err.name === 'CastError') {
    const { reason } = err as CastError;
    return res.status(400).send({
      message: reason?.message || err.message,
    });
  }

  if (err.name === 'ValidationError') {
    const { message, errors } = err as mongoose.Error.ValidationError;
    newrelic.addCustomAttribute('validationFailed', true);
    newrelic.addCustomAttribute('validationError', err.message);
    return res.status(400).send({
      message,
      errors,
    });
  }

  if (err.name === 'APIError') {
    return res.status(statusCode || 400).send(err);
  }

  if (err.name === 'Unauthorized') {
    return res.status(401).send({
      message: err.message,
    });
  }

  if (err.name === 'MongoError' && (err as MongoError).code === 11000) {
    const { message } = err as MongoError;
    return res.status(409).send({
      message,
    });
  }

  if (err.name === 'RateLimit') {
    newrelic.addCustomAttribute('rateLimit', true);
    return res.status(429).send({
      message: err.message,
    });
  }

  if (err.name === 'NotFound') {
    return res.status(404).send({
      message: err.message,
    });
  }

  if (statusCode === 413) {
    newrelic.addCustomAttribute('requestTooLarge', true);
    return res.status(413).send({
      message: 'Request Payload too large',
    });
  }

  newrelic.noticeError(err);
  return res.status(500).send({ message: 'Error encountered' });
};
