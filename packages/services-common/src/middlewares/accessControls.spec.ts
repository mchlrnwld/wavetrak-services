import { expect } from 'chai';
import sinon from 'sinon';
import { Express } from 'express';
import accessControl from './accessControl';

describe('accessControlHeaders', () => {
  it('should set up access control', () => {
    const useStub = sinon.stub();
    const app: Partial<Express> = { use: useStub };

    accessControl(app as Express);

    expect(app.use).to.have.been.calledOnce();
  });

  it('should take custom headers, methods, and origins', () => {
    const useStub = sinon.stub();
    const app: Partial<Express> = { use: useStub };

    accessControl(
      app as Express,
      ['GET'],
      ['X-Auth-Test'],
      ['www.surfline.com', 'www.fishtrack.com'],
    );

    expect(app.use).to.have.been.calledOnce();
  });
});
