import express from 'express';
import { SurflineRequest } from '../types';

const geoLatLon = (req: SurflineRequest, _: express.Response, next: express.NextFunction): void => {
  if (req.headers['x-geo-latitude']) {
    // eslint-disable-next-line no-param-reassign
    req.geoLatitude = req.headers['x-geo-latitude'];
  }
  if (req.headers['x-geo-longitude']) {
    // eslint-disable-next-line no-param-reassign
    req.geoLongitude = req.headers['x-geo-longitude'];
  }
  return next();
};

export default geoLatLon;
