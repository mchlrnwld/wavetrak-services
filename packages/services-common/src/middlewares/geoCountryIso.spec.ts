import sinon from 'sinon';
import { expect } from 'chai';
import express from 'express';
import geoCountryIso from './geoCountryIso';
import { SurflineRequest } from '../types';

describe('geoCountryIso', () => {
  it('should not append the country iso if the header is missing', () => {
    const resStub: Partial<express.Response> = { header: sinon.spy() };
    const reqStub: Partial<SurflineRequest> = {
      headers: {},
    };
    const nextStub = sinon.spy() as express.NextFunction;
    geoCountryIso(reqStub as express.Request, resStub as express.Response, nextStub);

    expect(reqStub.geoCountryIso).to.be.undefined();
  });

  it('should succeed if user ID and client ID are not required', () => {
    const resStub: Partial<express.Response> = { header: sinon.spy() };
    const reqStub: Partial<SurflineRequest> = {
      headers: {
        'x-geo-country-iso': 'US',
      },
    };
    const nextStub = sinon.spy() as express.NextFunction;
    geoCountryIso(reqStub as express.Request, resStub as express.Response, nextStub);

    expect(reqStub.geoCountryIso).to.be.equal('US');
  });
});
