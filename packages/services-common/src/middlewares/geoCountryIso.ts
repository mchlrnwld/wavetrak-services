import express from 'express';
import { SurflineRequest } from '../types';

const geoCountryIso = (
  req: SurflineRequest,
  _: express.Response,
  next: express.NextFunction,
): void => {
  /* eslint-disable no-param-reassign */
  if (req.headers['x-geo-country-iso']) {
    req.geoCountryIso = req.headers['x-geo-country-iso'];
  }
  return next();
  /* eslint-enable no-param-reassign */
};

export default geoCountryIso;
