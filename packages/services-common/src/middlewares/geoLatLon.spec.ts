import sinon from 'sinon';
import { expect } from 'chai';
import express from 'express';
import geoLatLon from './geoLatLon';
import { SurflineRequest } from '../types';

describe('geoLatLon', () => {
  it('should not append the lat and lon if the header is missing', () => {
    const resStub: Partial<express.Response> = { header: sinon.spy() };
    const reqStub: Partial<SurflineRequest> = {
      headers: {},
    };
    const nextStub = sinon.spy() as express.NextFunction;
    geoLatLon(reqStub as SurflineRequest, resStub as express.Response, nextStub);

    expect(reqStub.geoLatitude).to.be.undefined();
    expect(reqStub.geoLongitude).to.be.undefined();
  });

  it('should append the lat and long if the header is present', () => {
    const resStub: Partial<express.Response> = { header: sinon.spy() };
    const reqStub: Partial<SurflineRequest> = {
      headers: {
        'x-geo-latitude': '0',
        'x-geo-longitude': '90',
      },
    };
    const nextStub = sinon.spy() as express.NextFunction;
    geoLatLon(reqStub as SurflineRequest, resStub as express.Response, nextStub);

    expect(reqStub.geoLatitude).to.be.equal('0');
    expect(reqStub.geoLongitude).to.be.equal('90');
  });
});
