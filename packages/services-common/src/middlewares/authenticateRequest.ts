import newrelic from 'newrelic';
import express from 'express';
import Unauthorized from '../errors/Unauthorized';
import { SurflineRequest } from '../types';

const authenticateRequest = ({
  userIdRequired = false,
  clientIdRequired = false,
} = {}): express.RequestHandler => (
  req: SurflineRequest,
  _: express.Response,
  next: express.NextFunction,
): void => {
  const userId = req.headers['x-auth-userid'];
  const clientId = req.headers['x-api-key'];
  const scopes = req.headers['x-auth-scopes'];
  const accessToken = req.headers['x-auth-accesstoken'] || req.query.accesstoken;

  if ((userIdRequired && !userId) || (clientIdRequired && !clientId)) {
    throw new Unauthorized();
  }

  /* eslint-disable no-param-reassign */
  if (userId) {
    req.authenticatedUserId = userId;
    newrelic.addCustomAttribute('userId', userId);
  }
  if (clientId) {
    req.authenticatedClientId = clientId;
    newrelic.addCustomAttribute('clientId', clientId);
  }
  if (scopes) {
    req.scopes = scopes;
    newrelic.addCustomAttribute('scopes', scopes);
  }
  if (accessToken) {
    req.accesstoken = accessToken;
  }

  next();
  /* eslint-enable no-param-reassign */
};

export default authenticateRequest;
