import { expect } from 'chai';
import express, { NextFunction } from 'express';
import sinon from 'sinon';
import { errorHandlerMiddleware, MongoError } from './errorHandlerMiddleware';
import { BaseError, APIError, UnauthorizedError, RateLimit, NotFound } from '../errors';
import { SurflineRequest } from '../types';
import { CastError } from 'mongoose';

describe('errorHandlerMiddleware', () => {
  let infoStub: sinon.SinonStub;
  let errorStub: sinon.SinonStub;
  const loggerStub = console;

  beforeEach(() => {
    infoStub = sinon.stub(console, 'info');
    errorStub = sinon.stub(console, 'error');
  });

  afterEach(() => {
    infoStub.restore();
    errorStub.restore();
  });

  it('should send a 500 by default', () => {
    const sendStub = sinon.stub();
    const error = new BaseError('', 'test-message');

    const resStub: Partial<express.Response> = {
      header: sinon.spy(),
      status: sinon.stub().returns({ send: sendStub }),
    };
    const reqStub: Partial<SurflineRequest> = {};
    const nextStub: NextFunction = sinon.stub();

    errorHandlerMiddleware(loggerStub)(
      error,
      reqStub as SurflineRequest,
      resStub as express.Response,
      nextStub,
    );

    expect(loggerStub.error).to.have.been.calledOnceWithExactly({
      err: error,
      message: 'Error encountered (Common Handler)',
    });

    expect(resStub.status).to.have.been.calledOnceWithExactly(500);
    expect(sendStub).to.have.been.calledOnceWithExactly({ message: 'Error encountered' });
  });

  it('should properly log a CastError error message', () => {
    const sendStub = sinon.stub();
    const error = new BaseError('CastError', 'test-message');

    const resStub: Partial<express.Response> = {
      header: sinon.spy(),
      status: sinon.stub().returns({ send: sendStub }),
    };
    const reqStub: Partial<SurflineRequest> = {};
    const nextStub: NextFunction = sinon.stub();

    errorHandlerMiddleware(loggerStub)(
      error,
      reqStub as SurflineRequest,
      resStub as express.Response,
      nextStub,
    );

    expect(resStub.status).to.have.been.calledOnceWithExactly(400);
    expect(sendStub).to.have.been.calledOnceWithExactly({ message: 'test-message' });
  });

  it('should properly log a CastError error message with a reason', () => {
    const sendStub = sinon.stub();
    const error = new BaseError('CastError', 'test-message') as Partial<CastError>;
    error.reason = { message: 'reason', name: 'reason' };
    const resStub: Partial<express.Response> = {
      header: sinon.spy(),
      status: sinon.stub().returns({ send: sendStub }),
    };
    const reqStub: Partial<SurflineRequest> = {};
    const nextStub: NextFunction = sinon.stub();

    errorHandlerMiddleware(loggerStub)(
      error as CastError,
      reqStub as SurflineRequest,
      resStub as express.Response,
      nextStub,
    );

    expect(resStub.status).to.have.been.calledOnceWithExactly(400);
    expect(sendStub).to.have.been.calledOnceWithExactly({ message: 'reason' });
  });

  it('should properly log a ValidationError error message', () => {
    const sendStub = sinon.stub();
    const error = new BaseError('ValidationError', 'test-message');

    const resStub: Partial<express.Response> = {
      header: sinon.spy(),
      status: sinon.stub().returns({ send: sendStub }),
    };
    const reqStub: Partial<SurflineRequest> = {};
    const nextStub: NextFunction = sinon.stub();

    errorHandlerMiddleware(loggerStub)(
      error,
      reqStub as SurflineRequest,
      resStub as express.Response,
      nextStub,
    );

    expect(resStub.status).to.have.been.calledOnceWithExactly(400);
    expect(sendStub).to.have.been.calledOnceWithExactly({
      message: 'test-message',
      errors: undefined,
    });
  });

  it('should properly log a APIError error message', () => {
    const sendStub = sinon.stub();
    const error = new APIError('', { statusCode: 499 });

    const resStub: Partial<express.Response> = {
      header: sinon.spy(),
      status: sinon.stub().returns({ send: sendStub }),
    };
    const reqStub: Partial<SurflineRequest> = {};
    const nextStub: NextFunction = sinon.stub();

    errorHandlerMiddleware(loggerStub)(
      error,
      reqStub as SurflineRequest,
      resStub as express.Response,
      nextStub,
    );

    expect(resStub.status).to.have.been.calledOnceWithExactly(499);
    expect(sendStub).to.have.been.calledOnceWithExactly(error);
  });

  it('should properly log a APIError error message', () => {
    const sendStub = sinon.stub();
    const error = new APIError('', { statusCode: 499 });

    delete error.statusCode;

    const resStub: Partial<express.Response> = {
      header: sinon.spy(),
      status: sinon.stub().returns({ send: sendStub }),
    };
    const reqStub: Partial<SurflineRequest> = {};
    const nextStub: NextFunction = sinon.stub();

    errorHandlerMiddleware(loggerStub)(
      error,
      reqStub as SurflineRequest,
      resStub as express.Response,
      nextStub,
    );

    expect(resStub.status).to.have.been.calledOnceWithExactly(400);
    expect(sendStub).to.have.been.calledOnceWithExactly(error);
  });

  it('should properly log a DuplicateKeyError error message', () => {
    const sendStub = sinon.stub();
    const error = new BaseError('MongoError', 'some message') as MongoError;
    error.code = 11000;

    const resStub: Partial<express.Response> = {
      header: sinon.spy(),
      status: sinon.stub().returns({ send: sendStub }),
    };
    const reqStub: Partial<SurflineRequest> = {};
    const nextStub: NextFunction = sinon.stub();

    errorHandlerMiddleware(loggerStub)(
      error,
      reqStub as SurflineRequest,
      resStub as express.Response,
      nextStub,
    );

    expect(resStub.status).to.have.been.calledOnceWithExactly(409);
    expect(sendStub).to.have.been.calledOnceWithExactly({
      message: 'some message',
    });
  });

  it('should properly log a Unauthorized error message', () => {
    const sendStub = sinon.stub();
    const error = new UnauthorizedError();

    const resStub: Partial<express.Response> = {
      header: sinon.spy(),
      status: sinon.stub().returns({ send: sendStub }),
    };
    const reqStub: Partial<SurflineRequest> = {};
    const nextStub: NextFunction = sinon.stub();

    errorHandlerMiddleware(loggerStub)(
      error,
      reqStub as SurflineRequest,
      resStub as express.Response,
      nextStub,
    );

    expect(resStub.status).to.have.been.calledOnceWithExactly(401);
    expect(sendStub).to.have.been.calledOnceWithExactly({
      message: 'You are unauthorized to view this resource',
    });
  });

  it('should properly log a RateLimit error message', () => {
    const sendStub = sinon.stub();
    const error = new RateLimit();

    const resStub: Partial<express.Response> = {
      header: sinon.spy(),
      status: sinon.stub().returns({ send: sendStub }),
    };
    const reqStub: Partial<SurflineRequest> = {};
    const nextStub: NextFunction = sinon.stub();

    errorHandlerMiddleware(loggerStub)(
      error,
      reqStub as SurflineRequest,
      resStub as express.Response,
      nextStub,
    );

    expect(resStub.status).to.have.been.calledOnceWithExactly(429);
    expect(sendStub).to.have.been.calledOnceWithExactly({
      message: 'You have exceeded your request limit.',
    });
  });

  it('should properly log a NotFound error message', () => {
    const sendStub = sinon.stub();
    const error = new NotFound();

    const resStub: Partial<express.Response> = {
      header: sinon.spy(),
      status: sinon.stub().returns({ send: sendStub }),
    };
    const reqStub: Partial<SurflineRequest> = {};
    const nextStub: NextFunction = sinon.stub();

    errorHandlerMiddleware(loggerStub)(
      error,
      reqStub as SurflineRequest,
      resStub as express.Response,
      nextStub,
    );

    expect(resStub.status).to.have.been.calledOnceWithExactly(404);
    expect(sendStub).to.have.been.calledOnceWithExactly({
      message: 'Not Found',
    });
  });

  it('should properly log a Too Large error message', () => {
    const sendStub = sinon.stub();
    const error = new BaseError('an-error-has-no-name', 'an error has no message', {
      statusCode: 413,
    });

    const resStub: Partial<express.Response> = {
      header: sinon.spy(),
      status: sinon.stub().returns({ send: sendStub }),
    };
    const reqStub: Partial<SurflineRequest> = {};
    const nextStub: NextFunction = sinon.stub();

    errorHandlerMiddleware(loggerStub)(
      error,
      reqStub as SurflineRequest,
      resStub as express.Response,
      nextStub,
    );

    expect(resStub.status).to.have.been.calledOnceWithExactly(413);
    expect(sendStub).to.have.been.calledOnceWithExactly({
      message: 'Request Payload too large',
    });
  });
});
