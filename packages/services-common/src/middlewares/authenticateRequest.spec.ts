import sinon from 'sinon';
import { expect } from 'chai';
import express from 'express';
import authenticateRequest from './authenticateRequest';
import { SurflineRequest } from '../types';

describe('authenticateRequest', () => {
  it('should succeed if user ID and client ID are not required', () => {
    const resStub: Partial<express.Response> = { header: sinon.spy() };
    const reqStub: Partial<SurflineRequest> = {
      headers: {},
      query: {},
    };
    const nextStub = sinon.spy() as express.NextFunction;
    authenticateRequest()(reqStub as express.Request, resStub as express.Response, nextStub);

    expect(reqStub.authenticatedUserId).to.be.undefined();
    expect(reqStub.authenticatedClientId).to.be.undefined();
    expect(reqStub.scopes).to.be.undefined();
    expect(reqStub.accesstoken).to.be.undefined();
  });

  it('should set the properties on the req if passed in headers', () => {
    const resStub: Partial<express.Response> = { header: sinon.spy() };
    const reqStub: Partial<SurflineRequest> = {
      headers: {
        'x-auth-userid': '1',
        'x-api-key': '2',
        'x-auth-accesstoken': '3',
        'x-auth-scopes': 'read:write',
      },
      query: {},
    };
    const nextStub = sinon.spy() as express.NextFunction;
    authenticateRequest()(reqStub as express.Request, resStub as express.Response, nextStub);

    expect(reqStub.authenticatedUserId).to.be.equal('1');
    expect(reqStub.authenticatedClientId).to.be.equal('2');
    expect(reqStub.scopes).to.be.equal('read:write');
    expect(reqStub.accesstoken).to.be.equal('3');
  });

  it('should prioritize the access token header', () => {
    const resStub: Partial<express.Response> = { header: sinon.spy() };
    const reqStub: Partial<SurflineRequest> = {
      headers: {
        'x-auth-accesstoken': '3',
      },
      query: { accesstoken: '4' },
    };
    const nextStub = sinon.spy() as express.NextFunction;
    authenticateRequest()(reqStub as express.Request, resStub as express.Response, nextStub);

    expect(reqStub.accesstoken).to.be.equal('3');
  });

  it('should throw an error if the user id is not sent when required', () => {
    const resStub: Partial<express.Response> = { header: sinon.spy() };
    const reqStub: Partial<SurflineRequest> = {
      headers: {},
      query: {},
    };
    const nextStub = sinon.spy() as express.NextFunction;
    try {
      authenticateRequest({ userIdRequired: true })(
        reqStub as express.Request,
        resStub as express.Response,
        nextStub,
      );
    } catch (err) {
      expect(err).to.exist();
    }
  });

  it('should throw an error if the client id is not sent when required', () => {
    const resStub: Partial<express.Response> = { header: sinon.spy() };
    const reqStub: Partial<SurflineRequest> = {
      headers: {},
      query: {},
    };
    const nextStub = sinon.spy() as express.NextFunction;
    try {
      authenticateRequest({ clientIdRequired: true })(
        reqStub as express.Request,
        resStub as express.Response,
        nextStub,
      );
    } catch (err) {
      expect(err).to.exist();
    }
  });
});
