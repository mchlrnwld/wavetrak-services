import express from 'express';
import cors from 'cors';

export default (
  app: express.Express,
  allowedMethods?: string[] | string,
  allowedHeaders: string[] = [],
  allowedOrigin?: boolean | string | RegExp | (string | RegExp)[],
): void => {
  const defaultAllowedMethods = 'GET, HEAD, OPTIONS, POST, PUT, DELETE, PATCH';
  const defaultAllowedHeaders = [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'Credentials',
    'Authorization',
    'X-Auth-UserId',
    'X-API-Key',
    'X-Auth-Scopes',
    'X-Auth-Accesstoken',
    'X-Auth-AnonymousId',
  ];

  app.use(
    cors({
      origin: allowedOrigin || '*',
      methods: allowedMethods || defaultAllowedMethods,
      allowedHeaders: [...defaultAllowedHeaders, ...allowedHeaders].join(', '),
      credentials: true,
    }),
  );
};
