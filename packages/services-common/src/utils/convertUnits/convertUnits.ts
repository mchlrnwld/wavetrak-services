import { round, curry } from 'lodash';
import { surfConditions } from './conditions';
import { waveHeights } from './types';

// conversions with pre-defined units
export const convertFeetToMeters = (length: number): number => length * 0.3048;
export const convertMetersToFeet = (length: number): number => length / 0.3048;
export const convertKnotsToMPH = (speed: number): number => speed * 1.15078;
export const convertMPHToKnots = (speed: number): number => speed / 1.15078;
export const convertKnotsToKPH = (speed: number): number => speed * 1.852;
export const convertKPHToKnots = (speed: number): number => speed / 1.852;
export const convertFahrenheitToCelsius = (temp: number): number => (temp - 32) * (5 / 9);
export const convertCelsiusToFahrenheit = (temp: number): number => ((temp * 9) / 5) + 32;


const convertStrArrayToMetric = (arr: string[]): string => {
  if (arr.length === 1) return arr.join('');
  const arrMod = arr.map((key) => {
    if (parseInt(key, 10).toString() === key.toString()) {
      // convert to meters and round
      return round(convertFeetToMeters(parseInt(key, 10)), 1);
    }
    return key;
  });
  return arrMod.join('');
};

const roundToNearestCondition = (units: string, waveHeightInFeet: number, precision: number): number => {
  if (units != 'M' && waveHeightInFeet % 1 !== 0) {
    const matchValidCondtion = Object.keys(surfConditions).filter(key =>
      surfConditions[key].minHeight === waveHeightInFeet ||
      surfConditions[key].maxHeight === waveHeightInFeet
    ).length > 0;
    if (matchValidCondtion) return waveHeightInFeet;
  }
  return round(waveHeightInFeet, precision);
};

export const convertTemperatureUnits = curry((units: string, tempInFahrenheit: number): number | null => {
  if (!tempInFahrenheit) return tempInFahrenheit;
  if (units === 'C') return round(convertFahrenheitToCelsius(tempInFahrenheit));
  return round(tempInFahrenheit);
});

export const convertWindSpeedUnits = curry((units: string, windSpeedInKnots?: number | null): number | null | undefined => {
  if (!windSpeedInKnots) return windSpeedInKnots;
  switch (units) {
    case 'MPH':
      return round(convertKnotsToMPH(windSpeedInKnots));
    case 'KPH':
      return round(convertKnotsToKPH(windSpeedInKnots));
    default:
      return round(windSpeedInKnots);
  }
});

/**
 * @summary
 * Converts wave heights from feet to their given expected unit.
 * @param units - the desired unit (FT|M|HI)
 * @param minHeightInFeet the minimum wave height (in feet)
 * @param maxHeightInFeet the maximum wave height (in feet)
 * @param roundResult determines whether or not to round the returned result (to a default precision)
 * @returns converted wave heights
 */
export const convertWaveHeightsUnits = curry((
  units: string,
  minHeightInFeet: number,
  maxHeightInFeet: number,
  roundResult = true,
): waveHeights => {
  const unitsUc = units.toUpperCase();
  let minHeight: number = minHeightInFeet, maxHeight: number = maxHeightInFeet;
  let precision = 0;
  if (unitsUc === 'M') {
    minHeight = convertFeetToMeters(minHeightInFeet);
    maxHeight = convertFeetToMeters(maxHeightInFeet);
    precision = 1;
  }
  if (roundResult) {
    minHeight = roundToNearestCondition(unitsUc, minHeight, precision);
    maxHeight = roundToNearestCondition(unitsUc, maxHeight, precision);
  }
  return { minHeight, maxHeight } as waveHeights;
});

export const convertSwellHeightUnits =
  curry((units: string, swellHeightInFeet?: number | null): number | null | undefined => {
    if (!swellHeightInFeet) return swellHeightInFeet;
    if (units === 'M') return round(convertFeetToMeters(swellHeightInFeet), 1);
    return round(swellHeightInFeet, 1);
});

export const convertTideHeightUnits = curry((units: string, tideHeightInFeet?: number | null): number | null | undefined => {
  if (!tideHeightInFeet) return tideHeightInFeet;
  if (units === 'M') return round(convertFeetToMeters(tideHeightInFeet), 1);
  return round(tideHeightInFeet, 1);
});

export const convertStringByUnits = curry((units: string, str: string) => {
  if (units !== 'M') return str;
  // create array isolating numbers
  let strMetric = str;
  const strByNumsArray = strMetric.split(/(\d+)/);
  // convert imperial nums to metric
  strMetric = convertStrArrayToMetric(strByNumsArray);
  strMetric = strMetric.replace(/ft/ig, 'm').trim();
  return strMetric;
});

export const getHeightInFeet = (unitsOfValue: string, value: number, precise = false): number => {
  if (unitsOfValue === 'M') {
    const heightInFeet = convertMetersToFeet(value);

    return precise ? heightInFeet : round(heightInFeet);
  }
  return value;
};

export const getSpeedInKnots = (unitsOfValue: string, value: number): number => {
  if (unitsOfValue === 'MPH') {
    return round(convertMPHToKnots(value));
  } else if (unitsOfValue === 'KPH') {
    return round(convertKPHToKnots(value));
  }
  return value;
};

