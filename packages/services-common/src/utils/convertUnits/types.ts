export type waveHeights = {
  minHeight: number;
  maxHeight: number;
};

export type condition = {
  condition: string;
  minHeight: number;
  maxHeight: number;
  humanRelation: string;
};

export type hawaiianScaleWaveHeights = {
  minHeight: number;
  maxHeight: number;
  plus: boolean;
};