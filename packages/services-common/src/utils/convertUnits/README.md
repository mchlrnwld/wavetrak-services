## convertUnits
convertUnits consists of a series of helper functions to convert units from one to another.

### TODO
- Move all units to types ('M', 'FT', 'MPH', etc.)
- Export other functions as needed
- Give more generic conversion options. This is a port of the existing conversion functions mostly untouched from their existing code.