import { expect } from 'chai';
import {
  convertWindSpeedUnits,
  convertWaveHeightsUnits,
  convertTideHeightUnits,
  convertStringByUnits,
} from './convertUnits';

describe('convertUnits', () => {
  describe('convertWindSpeedUnits', () => {
    it('converts wind speed from knots', () => {
      expect(convertWindSpeedUnits('MPH', 10)).to.equal(12);
      expect(convertWindSpeedUnits('MPH', 20)).to.equal(23);
      expect(convertWindSpeedUnits('MPH', 30)).to.equal(35);
      expect(convertWindSpeedUnits('KPH', 10)).to.equal(19);
      expect(convertWindSpeedUnits('KPH', 20)).to.equal(37);
      expect(convertWindSpeedUnits('KPH', 30)).to.equal(56);
      expect(convertWindSpeedUnits('KTS', 10)).to.equal(10);
      expect(convertWindSpeedUnits('KTS', 10.1)).to.equal(10);
      expect(convertWindSpeedUnits('KTS', 10.5)).to.equal(11);
    });

    it('is curried', () => {
      expect(convertWindSpeedUnits('MPH')(10)).to.equal(12);
    });

    it('returns wind speed if falsey', () => {
      expect(convertWindSpeedUnits('M', 0)).to.equal(0);
      expect(convertWindSpeedUnits('M', null)).to.be.null();
      expect(convertWindSpeedUnits('M', undefined)).to.be.undefined();
    });
  });

  describe('convertWaveHeightsUnits', () => {
    it('converts wave height from feet', () => {
      expect(convertWaveHeightsUnits('M', 1, 2)).to.deep.equal({
        minHeight: 0.3,
        maxHeight: 0.6,
      });

      expect(convertWaveHeightsUnits('M', 3, 4)).to.deep.equal({
        minHeight: 0.9,
        maxHeight: 1.2,
      });

      expect(convertWaveHeightsUnits('FT', 0.5, 0.5)).to.deep.equal({
        minHeight: 0.5,
        maxHeight: 0.5,
      });
    });

    it('rounds properly', () => {
      expect(convertWaveHeightsUnits('M', 1, 2)).to.deep.equal({
        minHeight: 0.3,
        maxHeight: 0.6,
      });
      
      expect(convertWaveHeightsUnits('M', 1, 2, false)).to.deep.equal({
        minHeight: 0.3048,
        maxHeight: 0.6096,
      });
      expect(convertWaveHeightsUnits('FT', 1, 1.1, false)).to.deep.equal({
        minHeight: 1,
        maxHeight: 1.1,
      });
      expect(convertWaveHeightsUnits('FT', 1, 1.1)).to.deep.equal({
        minHeight: 1,
        maxHeight: 1,
      });
    });

    it('is curried', () => {
      expect(convertWaveHeightsUnits('M')(1, 2)).to.deep.equal({
        minHeight: 0.3,
        maxHeight: 0.6,
      });
    });

    it('returns wave height if falsey', () => {
      expect(convertWaveHeightsUnits('M', 0, 0)).to.deep.equal({
        minHeight: 0.0,
        maxHeight: 0.0,
      });
      expect(convertWaveHeightsUnits('M', 0, 0)).to.deep.equal({
        minHeight: 0,
        maxHeight: 0,
      });
      expect(convertWaveHeightsUnits('M', 0, 0)).to.deep.equal({
        minHeight: 0,
        maxHeight: 0,
      });
      expect(convertWaveHeightsUnits('FT', 1, 1.1)).to.deep.equal({
        minHeight: 1,
        maxHeight: 1,
      });
    });
  });

  describe('convertTideHeightUnits', () => {
    it('converts wave height from feet', () => {
      expect(convertTideHeightUnits('M', 1)).to.equal(0.3);
      expect(convertTideHeightUnits('M', 2)).to.equal(0.6);
      expect(convertTideHeightUnits('M', 3)).to.equal(0.9);
      expect(convertTideHeightUnits('FT', 1.1)).to.equal(1.1);
      expect(convertTideHeightUnits('FT', 1.11)).to.equal(1.1);
      expect(convertTideHeightUnits('FT', 1.15)).to.equal(1.2);
    });

    it('is curried', () => {
      expect(convertTideHeightUnits('M')(1)).to.equal(0.3);
    });

    it('returns tide height if falsey', () => {
      expect(convertTideHeightUnits('M', 0)).to.equal(0);
      expect(convertTideHeightUnits('M', null)).to.be.null();
      expect(convertTideHeightUnits('M', undefined)).to.be.undefined();
    });
  });

  describe('convertStringByUnits', () => {
    it('should convert feet to meters', () => {
      expect(convertStringByUnits('M')('8 ft overhead')).to.equal('2.4 m overhead');
      expect(convertStringByUnits('FT')('8 ft overhead')).to.equal('8 ft overhead');
      expect(convertStringByUnits('M')('4-5 ft - occasional 6 ft')).to.equal('1.2-1.5 m - occasional 1.8 m');
      expect(convertStringByUnits('FT')('4-5 ft - occasional 6 ft')).to.equal('4-5 ft - occasional 6 ft');
    });
  });
});
