import { condition } from './types';

export const surfConditions: Record<number,condition> = {
  0: {
    condition: 'None',
    minHeight: 0,
    maxHeight: 0,
    humanRelation: 'None',
  },
  1: {
    condition: 'Flat',
    minHeight: 0,
    maxHeight: 0.5,
    humanRelation: 'Flat',
  },
  2: {
    condition: '1-2 ft – knee to thigh high',
    minHeight: 1,
    maxHeight: 2,
    humanRelation: 'Knee to thigh high',
  },
  3: {
    condition: '2-3 ft – thigh to waist high',
    minHeight: 2,
    maxHeight: 3,
    humanRelation: 'Thigh to waist high',
  },
  4: {
    condition: '2-3 ft + – waist to stomach high',
    minHeight: 2,
    maxHeight: 3,
    humanRelation: 'Waist to stomach high',
  },
  5: {
    condition: '3-4 ft – waist to chest high',
    minHeight: 3,
    maxHeight: 4,
    humanRelation: 'Waist to chest high',
  },
  6: {
    condition: '3-4 ft + – waist to shoulder high',
    minHeight: 3,
    maxHeight: 4,
    humanRelation: 'Waist to shoulder high',
  },
  7: {
    condition: '3-5 ft – waist to head high',
    minHeight: 3,
    maxHeight: 5,
    humanRelation: 'Waist to head high',
  },
  8: {
    condition: '4-5 ft – chest to head high',
    minHeight: 4,
    maxHeight: 5,
    humanRelation: 'Chest to head high',
  },
  9: {
    condition: '4-6 ft – shoulder high to 1 ft overhead',
    minHeight: 4,
    maxHeight: 6,
    humanRelation: 'Shoulder high to 1 ft overhead',
  },
  10: {
    condition: '4-7 ft – shoulder high to 2 ft overhead',
    minHeight: 4,
    maxHeight: 7,
    humanRelation: 'Shoulder high to 2 ft overhead',
  },
  11: {
    condition: '5-6 ft – head high to 1 ft overhead',
    minHeight: 5,
    maxHeight: 6,
    humanRelation: 'Head high to 1 ft overhead',
  },
  12: {
    condition: '5-7 ft – head high to 2 ft overhead',
    minHeight: 5,
    maxHeight: 7,
    humanRelation: 'Head high to 2 ft overhead',
  },
  13: {
    condition: '5-8 ft – head high to 3 ft overhead',
    minHeight: 5,
    maxHeight: 8,
    humanRelation: 'Head high to 3 ft overhead',
  },
  14: {
    condition: '6-8 ft – head high + to 3 ft overhead',
    minHeight: 6,
    maxHeight: 8,
    humanRelation: 'Head high + to 3 ft overhead',
  },
  15: {
    condition: '6-10 ft – head high + to double overhead',
    minHeight: 6,
    maxHeight: 10,
    humanRelation: 'Head high + to double overhead',
  },
  16: {
    condition: '8-10 ft – 3 ft overhead to double overhead',
    minHeight: 8,
    maxHeight: 10,
    humanRelation: '3 ft overhead to double overhead',
  },
  17: {
    condition: '8-12 ft – 3 ft overhead to double overhead +',
    minHeight: 8,
    maxHeight: 12,
    humanRelation: '3 ft overhead to double overhead +',
  },
  18: {
    condition: '10-15 ft – 2-3 times overhead',
    minHeight: 10,
    maxHeight: 15,
    humanRelation: '2-3 times overhead',
  },
  19: {
    condition: '12-15 ft – triple overhead',
    minHeight: 12,
    maxHeight: 15,
    humanRelation: 'Triple overhead',
  },
  20: {
    condition: '15-18 ft – triple overhead +',
    minHeight: 15,
    maxHeight: 18,
    humanRelation: 'Triple overhead +',
  },
  21: {
    condition: '15-20 ft – 3-4 times overhead',
    minHeight: 15,
    maxHeight: 20,
    humanRelation: '3-4 times overhead',
  },
  22: {
    condition: '18-20 ft – 4 times overhead',
    minHeight: 18,
    maxHeight: 20,
    humanRelation: '4 times overhead',
  },
  23: {
    condition: '20-25 ft – 4-5 times overhead',
    minHeight: 20,
    maxHeight: 25,
    humanRelation: '4-5 times overhead',
  },
  24: {
    condition: '25-30 ft – 5-6 times overhead',
    minHeight: 25,
    maxHeight: 30,
    humanRelation: '5-6 times overhead',
  },
  25: {
    condition: '30-35 ft – 6-7 times overhead',
    minHeight: 30,
    maxHeight: 35,
    humanRelation: '6-7 times overhead',
  },
  26: {
    condition: '30-40 ft – 6-8 times overhead',
    minHeight: 30,
    maxHeight: 40,
    humanRelation: '6-8 times overhead',
  },
  27: {
    condition: '35-40 ft – 7-8 times overhead',
    minHeight: 35,
    maxHeight: 40,
    humanRelation: '7-8 times overhead',
  },
  28: {
    condition: '35-50 ft – 7-10 times overhead',
    minHeight: 35,
    maxHeight: 50,
    humanRelation: '7-10 times overhead',
  },
  29: {
    condition: '.5-1 ft – shin to knee high',
    minHeight: 0.5,
    maxHeight: 1,
    humanRelation: 'Shin to knee high',
  },
};

export default surfConditions;
