import { convertWaveHeightsUnits } from './convertUnits';

export { convertWaveHeightsUnits };

export default convertWaveHeightsUnits;