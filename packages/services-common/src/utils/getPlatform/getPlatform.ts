import { WEB, ANDROID, IOS } from '../../constants';

/**
 * @description A function which will parse the user-agent string and return the platform from which the request came from
 * @param {String} userAgent user-agent string from the request header
 * @returns the platform string from which the request came from
 */
const getPlatform = (userAgent:string) => {
    if(!userAgent) return null;
    const userAgentParsed = userAgent.toLowerCase();
    if(userAgentParsed.includes('ios')) return IOS;
    if(userAgentParsed.includes('android')) return ANDROID;
    if(userAgentParsed.includes('okhttp/4.9.1')) return ANDROID; // Added for backward comatibility with existing Android apps
    return WEB;
};
export default getPlatform;
