import { expect } from 'chai';
import getPlatform from './getPlatform';
import { WEB, ANDROID, IOS } from '../../constants';

describe('getPlatform', () => {
    it('Returns platform as web for Web requests', () => {
        const uastring = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36";
        expect(getPlatform(uastring)).to.equal(WEB);
    });
    it('Returns platform as ios for iOS App requests', () => {
        const uastring = "Surfline/7.1.1 (com.surfline.surfapp; build:0; iOS 14.4.2) Alamofire/4.9.1";
        expect(getPlatform(uastring)).to.equal(IOS);
    });
    it('Returns platform as android for Android App requests', () => {
        const uastring = "Surfline/4.1.1 (com.surfline.android; build:1; Android 10) okhttp/4.9.1";
        expect(getPlatform(uastring)).to.equal(ANDROID);
    });
    it('Returns platform as android for old versions of Android App requests', () => {
        const uastring = "okhttp/4.9.1";
        expect(getPlatform(uastring)).to.equal(ANDROID);
    });
});
