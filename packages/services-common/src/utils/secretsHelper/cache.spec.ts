import { expect } from 'chai';
import sinon from 'sinon';
import DataCache from './cache';

const wait = async (ms: number): Promise<void> => {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
};

describe('DataCache', () => {
  const cache = new DataCache(0.005);
  let fetchStub: sinon.SinonStub

  beforeEach(() => {
    fetchStub = sinon.stub().resolves({ SecretString: 'cache-value' });
  })

  afterEach(() => {
    fetchStub.resetHistory();
  });

  it('should have a ttl', () => {
    expect(cache.millisecondsToLive).to.be.equal(300);
  });

  it("should be expired for keys that don't exist", () => {
    expect(cache.isCacheExpired('cache-key')).to.be.true();
  });

  it('should set a value in the cache with a ttl', async () => {
    const value = await cache.getData(fetchStub, 'cache-key');
    await cache.getData(fetchStub, 'cache-key');
    expect(value).to.be.equal('cache-value');
    expect(fetchStub).to.have.been.calledOnce();
    await wait(300);
    const value2 = await cache.getData(fetchStub, 'cache-key');
    expect(value2).to.be.equal('cache-value');
    expect(fetchStub).to.have.been.calledTwice();
  }).timeout(10000);

  it('should invalidate cache', async () => {
    const xtraLongCache = new DataCache(1000);
    await xtraLongCache.getData(fetchStub, 'cache-key');
    xtraLongCache.resetCache();
    await xtraLongCache.getData(fetchStub, 'cache-key');
    expect(fetchStub).to.have.been.calledTwice();
  }).timeout(10000);
});
