import DataCache from './cache';
import { getAWSSecret } from '../../external/aws';

const cache = new DataCache();
/**
 * @param {String} prefix - directory structure of name in AWS Secrets Manager
 * @param {String} name - ID of the secret, usually formatted how the env variable will look
 */
const getSecret = async (prefix: string, name: string): Promise<string | undefined> => {
  const secretId = `${prefix}${name}`;
  if (process.env[name]) {
    return process.env[name];
  }

  try {
    const SecretString = await cache.getData(getAWSSecret(secretId), secretId);

    return SecretString;
  } catch (err) {
    throw new Error(`Could not fetch secret ${secretId} from ENV or AWS Secrets Manager`);
  }
};

export default getSecret;
