import { expect } from 'chai';
import { ImportMock } from 'ts-mock-imports';
import * as aws from '../../external/aws';
import sinon from 'sinon';
import getSecret from './getSecret';

describe('getSecret', () => {
  let getSecretValueStub: sinon.SinonStub;
  let getAWSSecretStub: sinon.SinonStub;

  beforeEach(() => {
    getSecretValueStub = sinon.stub();
    getAWSSecretStub = ImportMock.mockFunction(aws, 'getAWSSecret');
  });

  afterEach(() => {
    getAWSSecretStub.restore();
    getSecretValueStub.resetHistory();
  });

  it('should throw an error on failure', async () => {
    getAWSSecretStub.throws();
    try {
      await getSecret('prefix', 'key');
    } catch (err) {
      expect(err.message).to.be.equal(
        `Could not fetch secret prefixkey from ENV or AWS Secrets Manager`,
      );
      expect(getAWSSecretStub).to.have.been.calledOnceWithExactly(`prefixkey`);
    }
  });

  it('should return an environment variable', async () => {
    process.env['SOME_KEY'] = 'somevalue';
    const result = await getSecret('prefix', 'SOME_KEY');
    expect(result).to.have.be.equal('somevalue');
  });

  it('should retrieve a value from AWS', async () => {
    getAWSSecretStub.returns(getSecretValueStub);
    getSecretValueStub.resolves({ SecretString: 'some-value' });
    const result = await getSecret('prefix', 'key');
    expect(getAWSSecretStub).to.have.been.calledOnceWithExactly(`prefixkey`);
    expect(getSecretValueStub).to.have.been.calledOnce();
    expect(result).to.be.equal('some-value');
  });
});
