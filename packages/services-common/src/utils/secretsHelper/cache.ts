import AWS, { AWSError } from 'aws-sdk';
import { PromiseResult } from 'aws-sdk/lib/request';

export default class DataCache {
  millisecondsToLive: number;

  cache: { [key: string]: string | undefined };

  fetchDates: { [key: string]: Date };

  constructor(minutesToLive = 3) {
    this.millisecondsToLive = minutesToLive * 60 * 1000;
    this.cache = {};
    this.fetchDates = {};
  }

  isCacheExpired = (key: string): boolean => {
    if (!this.fetchDates[key]) return true;
    return this.fetchDates[key].getTime() + this.millisecondsToLive < new Date().getTime();
  };

  getData = async (
    fetchFunction: () => Promise<
      PromiseResult<AWS.SecretsManager.GetSecretValueResponse, AWSError>
    >,
    key: string
  ): Promise<string | undefined> => {
    if (!this.cache[key] || this.isCacheExpired(key)) {
      const data = await fetchFunction();
      const secret = data.SecretString;
      this.cache[key] = secret;
      this.fetchDates[key] = new Date();
      return secret;
    }
    return this.cache[key];
  };

  resetCache = (): void => {
    this.fetchDates = {};
  };
}
