class LatLon {
  latitude!: number;
  longitude!: number;
}

class Bounds {
  northWest!: LatLon;
  southEast!: LatLon;
  constructor () {
    this.northWest = new LatLon();
    this.southEast = new LatLon();
  }
}

class GridDef {
  resolution!: number;
  limBounds!: Bounds;
}

const validateLat = (fieldName: string, obj: LatLon): void => {
  if (Math.abs(obj.latitude) > 90) throw new Error(`Validation Error: ${fieldName}.latitude should be between -90 and 90`);
}

/**
 * Transforms longitude to within the -180 -> 180 bounds that some applications use as a standard.
 * @param  {number} longitude
 * @returns number
 */
const transformLon = (longitude: number): number => {
  while (longitude <= -180) longitude += 360;
  while (longitude > 180) longitude -= 360;
  return longitude;
}

/**
 * rounds a float to the nearest point using a resolution
 * @param {number} val
 * @param {number} resolution
 */
export const roundFloat = (val: number, resolution: number): number => (
  Math.round(val / resolution) * resolution
)

/**
 * limit a bounding box to within another bounding box
 * @param  {Bounds} requested desired bounds
 * @param  {Bounds} limBounds actual bounds
 * @returns Bounds overlap between desired and actual
 */
export const limitBounds = (requested: Bounds, { limBounds, resolution }: GridDef): Bounds => {
  validateLat('limBounds.NorthWest', limBounds.northWest);
  validateLat('limBounds.southEast', limBounds.southEast);
  validateLat('requested.NorthWest', requested.northWest);
  validateLat('requested.southEast', requested.southEast);

  if (limBounds.northWest.latitude < requested.southEast.latitude || limBounds.southEast.latitude > requested.northWest.latitude) {
    throw new Error('Validation Error: latitude entirely out of bounds');
  }
  if (requested.northWest.latitude <= requested.southEast.latitude) {
    throw new Error('Validation Error: requested bounds dont vary latitudinally or south > north');
  }

  // Unwrap the bounds objects so that they are on a continuous scale.
  let eastLim = limBounds.southEast.longitude;
  let westLim = limBounds.northWest.longitude;

  while (eastLim <= westLim) eastLim += 360;
  while (eastLim > westLim + 360) eastLim -= 360;
  if (eastLim - 360 + resolution === westLim) {
    eastLim += (resolution + 360);
    westLim -= (resolution + 360);
  }

  let eastReq = requested.southEast.longitude;
  let westReq = requested.northWest.longitude;

  while (eastReq <= westReq) eastReq += 360;
  while (eastReq > westReq + 360) eastReq -= 360;

  // Transform the unwrapped bounds to the same part of the scale.

  while (westReq > eastLim) {
    eastReq -= 360;
    westReq -= 360;
  }
  while (eastReq < westLim) {
    eastReq += 360;
    westReq += 360;
  }

  if (eastReq < westLim || westReq > eastLim) {
    throw new Error('Validation Error: longitude entirely out of bounds');
  }

  if (eastReq === westReq) {
    throw new Error('Validation Error: requested bounds dont vary longitudinally');
  }

  const resultingBounds = new Bounds();
  resultingBounds.northWest.latitude = Math.min(limBounds.northWest.latitude, requested.northWest.latitude);
  resultingBounds.southEast.latitude = Math.max(limBounds.southEast.latitude, requested.southEast.latitude);
  resultingBounds.northWest.longitude = Math.max(westLim, westReq);
  resultingBounds.southEast.longitude = Math.min(eastLim, eastReq);

  return resultingBounds;
}

/* A generic function to generate lat lons within a bounding box from a grid definition (reference point and resolution)
*
* reqBounds: {
*   northWest: a lat lon object representing the northWest corner of the bounds within which the grid could be generated
*   southEast: a lat lon object representing the southEast corner of the bounds within which the grid could be generated
* }
* GridDef: {
*   ref: a reference point from the original grid
*   resolution: the resolution of the grid
*   limBounds: {
*     northWest: a lat lon object representing the northWest corner of the limits of the grid def
*     southEast: a lat lon object representing the southEast corner of the limits of the grid def
*   }
* }
*
* returns: an array of arrays of LatLon objects. In the parent array there is an array for each latitude. In each child array there is a Lat Lon
*          object for each longitude. The index of the parent array goes from north to south and the child arrays are indexed from west to east.
*/
export const generateGrid = (reqBounds: Bounds, gridDef: GridDef): LatLon[][] => {
  const { northWest, southEast } = limitBounds(reqBounds, gridDef);
  const ref = gridDef.limBounds.northWest;
  const { resolution } = gridDef;

  //find the first point in the north west corner of the bounds (potentially on the corner)
  const firstPoint = new LatLon();
  firstPoint.latitude = ref.latitude + Math.floor((northWest.latitude - ref.latitude) / resolution) * resolution;
  firstPoint.longitude = ref.longitude + Math.ceil((northWest.longitude - ref.longitude) / resolution) * resolution;

  //populate the grid
  const grid = [];
  for (let latitude = firstPoint.latitude; latitude >= southEast.latitude; latitude -= resolution) {
    const row = [];
    for (let longitude = firstPoint.longitude; longitude <= southEast.longitude; longitude += resolution) {

      row.push({
        latitude: roundFloat(latitude, resolution),
        longitude: roundFloat(transformLon(longitude), resolution),
      })
    }
    grid.push(row);
  }

  return grid;
}
