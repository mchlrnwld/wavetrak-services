import { expect } from 'chai';
import { generateGrid, limitBounds, roundFloat } from './';

describe('grid generator', () => {
  it('should return a grid of lat lons', () => {
    const northWest = {
      latitude: 1.2,
      longitude: 358.5,
    };

    const southEast = {
      latitude: -2.2,
      longitude: 1.1,
    };

    const ref = {
      latitude: 3,
      longitude: -1,
    };

    const resolution = 1;

    const testGrid = [
      [{ latitude: 1, longitude: -1 }, { latitude: 1, longitude: 0 }, { latitude: 1, longitude: 1 }],
      [{ latitude: 0, longitude: -1 }, { latitude: 0, longitude: 0 }, { latitude: 0, longitude: 1 }],
      [{ latitude: -1, longitude: -1 }, { latitude: -1, longitude: 0 }, { latitude: -1, longitude: 1 }],
      [{ latitude: -2, longitude: -1 }, { latitude: -2, longitude: 0 }, { latitude: -2, longitude: 1 }],
    ]
    expect(generateGrid({ northWest, southEast }, { limBounds: { northWest: ref, southEast }, resolution })).to.eql(testGrid);
  })

  it('should not experience floating point issues', () => {
    const northWest = {
      latitude: 1.2439,
      longitude: 0.1234,
    };

    const southEast = {
      latitude: 1.209,
      longitude: 0.151,
    };

    const limBounds = {
      northWest: {
        latitude: 3,
        longitude: -1,
      },
      southEast: {
        latitude: -3,
        longitude: 2
      }
    };

    const resolution = 0.01;

    const testGrid = [
      [{ latitude: 1.24, longitude: 0.13 }, { latitude: 1.24, longitude: 0.14 }, { latitude: 1.24, longitude: 0.15 }],
      [{ latitude: 1.23, longitude: 0.13 }, { latitude: 1.23, longitude: 0.14 }, { latitude: 1.23, longitude: 0.15 }],
      [{ latitude: 1.22, longitude: 0.13 }, { latitude: 1.22, longitude: 0.14 }, { latitude: 1.22, longitude: 0.15 }],
      [{ latitude: 1.21, longitude: 0.13 }, { latitude: 1.21, longitude: 0.14 }, { latitude: 1.21, longitude: 0.15 }],
    ]
    expect(generateGrid({ northWest, southEast }, { limBounds, resolution })).to.eql(testGrid);
  })

  it('should work if it crosses the antimeridian', () => {
    const northWest = {
      latitude: 1,
      longitude: 179,
    };

    const southEast = {
      latitude: -2,
      longitude: -179,
    };

    const resolution = 1;

    const testGrid = [
      [{ latitude: 1, longitude: 179 }, { latitude: 1, longitude: 180 }, { latitude: 1, longitude: -179 }],
      [{ latitude: 0, longitude: 179 }, { latitude: 0, longitude: 180 }, { latitude: 0, longitude: -179 }],
      [{ latitude: -1, longitude: 179 }, { latitude: -1, longitude: 180 }, { latitude: -1, longitude: -179 }],
      [{ latitude: -2, longitude: 179 }, { latitude: -2, longitude: 180 }, { latitude: -2, longitude: -179 }],
    ]
    expect(generateGrid({ northWest, southEast }, { limBounds: { northWest, southEast }, resolution })).to.eql(testGrid);
  })

  it('should work if limits are used', () => {
    const northWest = {
      latitude: 1,
      longitude: 360 + 178,
    };

    const southEast = {
      latitude: -2,
      longitude: 360 + 180,
    };

    const southEastLim = {
      latitude: -1,
      longitude: 360 + 179,
    }

    const resolution = 1;

    const testGrid = [
      [{ latitude: 1, longitude: 178 }, { latitude: 1, longitude: 179 }],
      [{ latitude: 0, longitude: 178 }, { latitude: 0, longitude: 179 }],
      [{ latitude: -1, longitude: 178 }, { latitude: -1, longitude: 179 }],
    ]
    expect(generateGrid(
      { northWest, southEast },
      {
        resolution,
        limBounds: { northWest, southEast: southEastLim },
      },
    )).to.eql(testGrid);
  })

  it('should work for a global grid', () => {
    const northWest = {
      latitude: 0.5,
      longitude: -1,
    };

    const southEast = {
      latitude: -0.5,
      longitude: 1,
    };

    const limBounds = {
      northWest: {
        latitude: 90,
        longitude: 0,
      },
      southEast: {
        latitude: -90,
        longitude: 359.5,
      }
    }

    const resolution = 0.5;

    const testGrid = [
      [{ longitude: -1, latitude: 0.5 }, { longitude: -0.5, latitude: 0.5 }, { longitude: 0, latitude: 0.5 }, { longitude: 0.5, latitude: 0.5 }, { longitude: 1, latitude: 0.5 }],
      [{ longitude: -1, latitude: 0 }, { longitude: -0.5, latitude: 0 }, { longitude: 0, latitude: 0 }, { longitude: 0.5, latitude: 0 }, { longitude: 1, latitude: 0 }],
      [{ longitude: -1, latitude: -0.5 }, { longitude: -0.5, latitude: -0.5 }, { longitude: 0, latitude: -0.5 }, { longitude: 0.5, latitude: -0.5 }, { longitude: 1, latitude: -0.5 }],
    ]
    expect(generateGrid(
      { northWest, southEast },
      {
        resolution,
        limBounds,
      },
    )).to.eql(testGrid);
  })

  it('should throw an error if latitude is > 90', () => {
    const northWest = {
      latitude: 91,
      longitude: 360 + 178,
    };

    const southEast = {
      latitude: -2,
      longitude: 360 + 180,
    };

    const resolution = 1;

    expect(() => generateGrid({ northWest, southEast }, { limBounds: { northWest, southEast }, resolution }))
      .to.throw('Validation Error: limBounds.NorthWest.latitude should be between -90 and 90');
  })
});

describe('bounds limiter', () => {
  it('should throw an error if a grid is out of longitude limits', () => {
    const requested = {
      northWest: {
        latitude: 1.2,
        longitude: 358.5,
      },
      southEast: {
        latitude: -2.2,
        longitude: 1.1,
      }
    };

    const limBounds = {
      northWest: {
        latitude: 1.2,
        longitude: -365.5,
      },
      southEast: {
        latitude: -2.2,
        longitude: -2.5,
      }
    };

    expect(() => limitBounds(requested, { limBounds, resolution: 0.5 }))
      .to.throw('Validation Error: longitude entirely out of bounds');

  });

  it('should return the reqested bounds if it is entirely the same as the limits', () => {
    const requested = {
      northWest: {
        latitude: 1.2,
        longitude: (360 * 3) + 359.5,
      },
      southEast: {
        latitude: -2.2,
        longitude: 1.1,
      }
    };

    const limBounds = {
      northWest: {
        latitude: 1.2,
        longitude: -360.5,
      },
      southEast: {
        latitude: -2.2,
        longitude: 361.1,
      }
    };

    const result = limitBounds(requested, { limBounds, resolution: 0.5 });

    expect(result).to.deep.almost.equal({
      northWest: {
        latitude: 1.2,
        longitude: -360.5,
      },
      southEast: {
        latitude: -2.2,
        longitude: -358.9,
      }
    });
  });

  it('should return the reqested bounds if it is entirely within the limits', () => {
    const requested = {
      northWest: {
        latitude: 1.1,
        longitude: (360 * 3) + 360.5,
      },
      southEast: {
        latitude: -2.1,
        longitude: 1,
      }
    };

    const limBounds = {
      northWest: {
        latitude: 1.2,
        longitude: -360.5,
      },
      southEast: {
        latitude: -2.2,
        longitude: 361.1,
      }
    };

    const result = limitBounds(requested, { limBounds, resolution: 0.5 });

    expect(result).to.deep.almost.equal({
      northWest: {
        latitude: 1.1,
        longitude: -359.5,
      },
      southEast: {
        latitude: -2.1,
        longitude: -359,
      }
    });
  });

  it('should return the limits if they are entirely within the bounds', () => {
    const limBounds = {
      northWest: {
        latitude: 1.1,
        longitude: 360.5,
      },
      southEast: {
        latitude: -2.1,
        longitude: 1,
      }
    };

    const requested = {
      northWest: {
        latitude: 1.2,
        longitude: -360.5,
      },
      southEast: {
        latitude: -2.2,
        longitude: 361.1,
      }
    };


    const result = limitBounds(requested, { limBounds, resolution: 0.5 });

    expect(result).to.deep.almost.equal({
      northWest: {
        latitude: 1.1,
        longitude: 360.5,
      },
      southEast: {
        latitude: -2.1,
        longitude: 361,
      }
    });
  });

  it('should reshape the bounds if there is overlap between the bounds and the limits', () => {
    const requested = {
      northWest: {
        latitude: 1.2,
        longitude: (360 * 3) + 359.5,
      },
      southEast: {
        latitude: -2.2,
        longitude: 1.1,
      }
    };

    const limBounds = {
      northWest: {
        latitude: 1.1,
        longitude: -360.5,
      },
      southEast: {
        latitude: -2.3,
        longitude: 364.1,
      }
    };

    const result = limitBounds(requested, { limBounds, resolution: 0.5 });

    expect(result).to.deep.almost.equal({
      northWest: {
        latitude: 1.1,
        longitude: -360.5,
      },
      southEast: {
        latitude: -2.2,
        longitude: -358.9,
      }
    });
  });

  it('should work with a global grid def', () => {
    const limBounds = {
      northWest: {
        latitude: 90,
        longitude: 0,
      },
      southEast: {
        latitude: -90,
        longitude: 359.5,
      }
    };

    const requested = {
      northWest: {
        latitude: 1.1,
        longitude: 358.5,
      },
      southEast: {
        latitude: -2.3,
        longitude: 4.1,
      }
    };

    const result = limitBounds(requested, { limBounds, resolution: 0.5 });

    expect(result).to.deep.almost.equal({
      northWest: {
        latitude: 1.1,
        longitude: 358.5,
      },
      southEast: {
        latitude: -2.3,
        longitude: 364.1,
      }
    });
  });
});

describe('float corrector', () => {
  it('should correct a dodgy float to a value that has the correct resolution', () => {
    expect(roundFloat(1.45999999993, 0.01)).to.equal(1.46);
    expect(roundFloat(1.4500000001, 0.01)).to.equal(1.45);
    expect(roundFloat(-1.4500000001, 0.01)).to.equal(-1.45);
    expect(roundFloat(-1.45999999993, 0.01)).to.equal(-1.46);
  })
})
