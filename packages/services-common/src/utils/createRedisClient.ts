import redis, { RedisClient, ClientOpts } from 'redis';
import { SurflineLogger } from '../types';

const createRedisClient = async (
  redisIp: string,
  redisPort: number | string,
  logger: SurflineLogger | Console,
  options?: ClientOpts | undefined,
): Promise<RedisClient> => {
  const log = logger;
  const connectionString = `redis://${redisIp}:${redisPort}`;
  return new Promise((resolve, reject) => {
    try {
      log.info(`Redis ConnectionString: ${connectionString} `);
      const client = redis.createClient(connectionString, options);
      client.on('ready', () => {
        log.info(
          `Redis connected on ${connectionString} with version ${client.server_info.redis_version}`,
        );
        return resolve(client);
      });
      client.on('error', (error) => {
        log.error({
          action: 'Redis:ConnectionError',
          error,
        });
        return reject(error);
      });
    } catch (error) {
      log.error({
        action: 'Redis:ConnectionError',
        error,
      });
      return reject(error);
    }
  });
}

export default createRedisClient;
