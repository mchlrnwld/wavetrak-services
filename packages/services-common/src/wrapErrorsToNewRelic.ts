/* eslint-disable @typescript-eslint/no-explicit-any */
import newrelic from 'newrelic';
import { APIError } from './index';

type argsArray = any[];
type handlerFunc = (...a: argsArray) => Promise<any>;

const wrapErrorsToNewRelic = (fn: handlerFunc) => async (...args: argsArray): Promise<any> => {
  try {
    await fn(...args);
  } catch(error) {
    if (!(error instanceof APIError)){
      newrelic.recordCustomEvent('Uncaught Error', {
        body: JSON.stringify(args[0].body),
        message: (error as APIError)?.message,
        end_point: args[0].originalUrl
      });
    }
    return args[2](error);
  }
}

export default wrapErrorsToNewRelic;
