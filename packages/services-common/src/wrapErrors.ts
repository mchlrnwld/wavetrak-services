/* eslint-disable @typescript-eslint/no-explicit-any */

type argsArray = any[];
type handlerFunc = (...a: argsArray) => Promise<any>;

const wrapErrors = (fn: handlerFunc) => (...args: argsArray): Promise<any> =>
  fn(...args).catch(args[2]);

export default wrapErrors;
