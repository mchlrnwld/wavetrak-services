/* istanbul ignore file */
import { SplitFactory } from '@splitsoftware/splitio';
import { SurflineLogger } from './types';

type Attributes = SplitIO.Attributes;
type Treatment = SplitIO.Treatment;
type TreatmentWithConfig = SplitIO.TreatmentWithConfig
type AnonymousId = string;
type UserId = string;

export interface SplitIdentity extends Attributes {
  anonymousId: AnonymousId;
  userId: UserId;
}

let splitLogger: SurflineLogger | Console = console;
let featureFramework: SplitIO.IClient;

export const setupFeatureFramework = (
  authorizationKey: string,
  logger?: SurflineLogger,
  readyTimeout = 5,
): Promise<SplitIO.IClient> =>
  new Promise((resolve, reject) => {
    if (featureFramework) resolve(featureFramework);

    if (logger) splitLogger = logger;
    const config = {
      core: { authorizationKey },
      startup: {
        requestTimeoutBeforeReady: 1.5, // 1500 ms
        retriesOnFailureBeforeReady: 3, // 3 times
        readyTimeout, // 5 sec default
      },
    };

    const sdk = SplitFactory(config);
    featureFramework = sdk.client();
    featureFramework.on(featureFramework.Event.SDK_READY, () => {
      splitLogger.info('Split Feature Framework Setup');
      resolve(featureFramework);
    });
    featureFramework.on(featureFramework.Event.SDK_READY_TIMED_OUT, () =>
      reject(new Error('Feature Framework Setup Timed Out')),
    );
  });

/**
 * @description Retrieve a treatment for split using the specific identifier for the consumer based on
 * traffic type (userId or anonymousId).
 * @param {AnonymousId | UserId} key The anonymousId or userId that is being used to target this split
 */
export const getTreatment = (
  key: AnonymousId | UserId,
  treatment: string,
  identity: SplitIdentity,
): Treatment => {
  if (!featureFramework) {
    splitLogger.error('Called `getTreatment` without a feature framework.');
    splitLogger.error('Did you forget to set up Split when the server started?');
    throw new Error('Split not set up.');
  }
  return featureFramework.getTreatment(key, treatment, identity);
};

/**
 * @description Retrieves the Split evaluation result for a treatment with configuration. 
 * @param {AnonymousId | UserId} key The anonymousId or userId that is being used to target this split
 * @param {string} treatment The treatment name
 * @param {SplitIdentity} identity Identity assosciated
 * @returns An object containing the treatment and the configuration stringified JSON (or null if there was no config for that treatment).
 */
export const getTreatmentWithConfig = (
  key: AnonymousId | UserId,
  treatment: string,
  identity: SplitIdentity,
 ): TreatmentWithConfig => {
  if (!featureFramework) {
    splitLogger.error('Called `getTreatmentWithConfig` without a feature framework.');
    splitLogger.error('Did you forget to set up Split when the server started?');
    throw new Error('Split not set up.');
  }
  return featureFramework.getTreatmentWithConfig(key, treatment, identity);
 };

// Export in order to prevent garbage collection of `featureFramework`
export default (): SplitIO.IClient => featureFramework;
