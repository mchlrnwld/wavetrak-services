/* istanbul ignore file */
import redis from 'redis';
import { promisify } from 'util';
import logger from './logger';

let client: redis.RedisClient;

export function initRedis(
  redisIp: string,
  redisPort: number | string,
  serviceName?: string,
): Promise<void> {
  const log = logger(`${serviceName || ''}:Redis`);
  const connectionString = `redis://${redisIp}:${redisPort}`;
  return new Promise((resolve, reject) => {
    try {
      log.trace(`Redis ConnectionString: ${connectionString} `);
      client = redis.createClient(connectionString);
      client.on('ready', () => {
        log.info(
          `Redis connected on ${connectionString} with version ${client.server_info.redis_version}`,
        );
        resolve();
      });
      client.on('error', (error) => {
        log.error({
          action: 'Redis:ConnectionError',
          error,
        });
        reject(error);
      });
    } catch (error) {
      log.error({
        action: 'Redis:ConnectionError',
        error,
      });
      reject(error);
    }
  });
}

export const redisClient = (): redis.RedisClient => {
  return client;
};

// Copied from @types/redis in order to modify for promises
interface OverloadedKeyCommand<T, R> {
  (key: string, arg1: T, arg2: T, arg3: T, arg4: T, arg5: T, arg6: T): Promise<R>;
  (key: string, arg1: T, arg2: T, arg3: T, arg4: T, arg5: T): Promise<R>;
  (key: string, arg1: T, arg2: T, arg3: T, arg4: T): Promise<R>;
  (key: string, arg1: T, arg2: T, arg3: T): Promise<R>;
  (key: string, arg1: T, arg2: T): Promise<R>;
  (key: string, arg1: T | T[]): Promise<R>;
  (key: string, ...args: Array<T>): Promise<R>;
  (...args: Array<string | T>): Promise<R>;
}

// Copied from @types/redis in order to modify for promises
interface OverloadedCommand<T, R> {
  (arg1: T, arg2: T, arg3: T, arg4: T, arg5: T, arg6: T): Promise<R>;
  (arg1: T, arg2: T, arg3: T, arg4: T, arg5: T): Promise<R>;
  (arg1: T, arg2: T, arg3: T, arg4: T): Promise<R>;
  (arg1: T, arg2: T, arg3: T): Promise<R>;
  (arg1: T, arg2: T | T[]): Promise<R>;
  (arg1: T | T[]): Promise<R>;
  (...args: Array<T>): Promise<R>;
}

// Copied from @types/redis in order to modify for promises
interface OverloadedSetCommand<T, R> {
  (key: string, arg1: T, arg2: T, arg3: T, arg4: T, arg5: T, arg6: T): Promise<R>;
  (key: string, arg1: T, arg2: T, arg3: T, arg4: T, arg5: T): Promise<R>;
  (key: string, arg1: T, arg2: T, arg3: T, arg4: T): Promise<R>;
  (key: string, arg1: T, arg2: T, arg3: T): Promise<R>;
  (key: string, arg1: T, arg2: T): Promise<R>;
  (key: string, arg1: T | { [key: string]: T } | T[]): Promise<R>;
  (key: string, ...args: Array<T>): Promise<R>;
  (args: [string, ...T[]]): Promise<R>;
}

export interface PromisifiedRedis {
  hgetall: (key: string) => Promise<{ [key: string]: string }>;
  hget: (key: string, field: string) => Promise<string>;
  hmset: OverloadedSetCommand<string | number, 'OK'>;
  expire: (key: string, seconds: number) => Promise<number>;
  del: OverloadedCommand<string, number>;
  hdel: OverloadedKeyCommand<string, number>;
  set(key: string, value: string): Promise<'OK'>;
  set(key: string, value: string, flag: string): Promise<'OK'>;
  set(key: string, value: string, mode: string, duration: number): Promise<'OK' | undefined>;
  set(
    key: string,
    value: string,
    mode: string,
    duration: number,
    flag: string,
  ): Promise<'OK' | undefined>;
  get(key: string): Promise<string | null>;
  zadd: OverloadedKeyCommand<string | number, number>;
  zremrangebyscore(key: string, min: string | number, max: string | number): Promise<number>;
  zrem: OverloadedKeyCommand<string, number>;
  ttl(key: string): Promise<number>;
  inc(key: string): Promise<number>;
  decr(key: string): Promise<number>;
  multi(args?: Array<Array<string | number>>): redis.Multi;
  exists(key: string): Promise<number>;
}

let promisifiedRedis: PromisifiedRedis;

export const getPromisifiedRedis = (): PromisifiedRedis => {
  if (!promisifiedRedis) {
    promisifiedRedis = {
      hgetall: promisify(redisClient().hgetall).bind(redisClient()),
      hget: promisify(redisClient().hget).bind(redisClient()),
      hmset: promisify(redisClient().hmset).bind(redisClient()) as PromisifiedRedis['hmset'],
      expire: promisify(redisClient().expire).bind(redisClient()),
      del: promisify(redisClient().del).bind(redisClient()),
      hdel: promisify(redisClient().hdel).bind(redisClient()),
      set: promisify(redisClient().set).bind(redisClient()) as PromisifiedRedis['set'],
      get: promisify(redisClient().get).bind(redisClient()),
      zadd: promisify(redisClient().zadd).bind(redisClient()),
      zremrangebyscore: promisify(redisClient().zremrangebyscore).bind(redisClient()),
      zrem: promisify(redisClient().zrem).bind(redisClient()),
      ttl: promisify(redisClient().ttl).bind(redisClient()),
      inc: promisify(redisClient().incr).bind(redisClient()),
      decr: promisify(redisClient().decr).bind(redisClient()),
      multi: (promisify(redisClient().multi).bind(
        redisClient(),
      ) as unknown) as PromisifiedRedis['multi'],
      exists: promisify(redisClient().exists).bind(redisClient()),
    };
  }
  return promisifiedRedis;
};
