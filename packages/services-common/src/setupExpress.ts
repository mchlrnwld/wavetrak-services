import http from 'http';
import express, { RequestHandler, ErrorRequestHandler } from 'express';
import { json, urlencoded } from 'body-parser';
import cookieParser from 'cookie-parser';
import _isFunction from 'lodash/isFunction';
import authenticateRequest from './middlewares/authenticateRequest';
import accessControl from './middlewares/accessControl';
import geoCountryIso from './middlewares/geoCountryIso';
import geoLatLon from './middlewares/geoLatLon';
import { errorHandlerMiddleware } from './middlewares/errorHandlerMiddleware';
import { SurflineLogger } from './types';
import createExpessApp from './utils/createExpressApp';

export type PathParams = string | RegExp | (string | RegExp)[];

interface ExpressSetup {
  userIdRequired?: boolean;
  clientIdRequired?: boolean;
  allowedMethods?: string | string[];
  allowedHeaders?: string[];
  allowedOrigins?: boolean | string | RegExp | (string | RegExp)[];
  name?: string;
  port?: number;
  log?: SurflineLogger | Console;
  keepAliveTimeout?: number;
  headersTimeout?: number;
  setupAccessControl?: boolean;
  handlers?: Array<RequestHandler | [PathParams, ...RequestHandler[]] | ErrorRequestHandler>;
  callback?: (app: express.Express) => void;
  bodySizeLimit?: string | number;
  urlEncoding?: boolean;
}

const defaultLog = console;
const defaultPort = 8081;
const defaultKeepAlive = 65000;
const defaultHeadersTimeout = 80000;

/**
 * @description A function which will instantiate an Express server with sensible Surfline
 * defaults
 * @param {ExpressSetup} ExpressSetup
 * @param {ExpressSetup.handlers} handlers An array of middleware handlers or app
 * routes that will be passed to `app.use()`
 * @param {ExpressSetup.callback} callback callback which takes the app as an argument, this
 * can be used to apply any custom logic needed on the app
 * @param {ExpressSetup.userIdRequired} userIdRequired Whether or not to require
 * the `x-auth-userid` header
 * @param {ExpressSetup.clientIdRequired} clientIdRequired Whether or not to require
 * the `x-api-key` header

 * @returns the instance of the app and server like so: `{ app, server }`
 */
const setupExpress = (
  {
    handlers = [],
    userIdRequired,
    clientIdRequired,
    allowedMethods,
    allowedHeaders,
    allowedOrigins,
    name,
    port = defaultPort,
    log = defaultLog,
    keepAliveTimeout,
    headersTimeout,
    callback,
    bodySizeLimit,
    urlEncoding = true,
    setupAccessControl = true,
  }: ExpressSetup = {
    port: defaultPort,
    log: defaultLog,
    handlers: [],
  },
): { app: express.Express; server: http.Server } => {
  const app = createExpessApp();

  app.get('/health', (_: express.Request, res: express.Response) =>
    res.send({
      status: 200,
      message: 'OK',
      version: process.env.APP_VERSION || 'unknown',
    }),
  );
  app.enable('trust proxy');

  if (setupAccessControl) {
    accessControl(app, allowedMethods, allowedHeaders, allowedOrigins);
  }

  if (urlEncoding) {
    app.use(urlencoded({ extended: true, ...(bodySizeLimit && { limit: bodySizeLimit }) }));
  }

  app.use(cookieParser());
  app.use(json({ ...(bodySizeLimit && { limit: bodySizeLimit }) }));

  app.use(authenticateRequest({ userIdRequired, clientIdRequired }));

  app.use(geoCountryIso);
  app.use(geoLatLon);

  handlers.forEach((handler) => {
    if (!Array.isArray(handler) && !_isFunction(handler)) {
      const message =
        'Handlers must either be a request handler function, or an array with a path and a handler (["/path", (req, res, next) => {}])';
      throw new Error(message);
    }
    handler instanceof Array ? app.use(...handler) : app.use(handler);
  });

  // Call the callback before the catch all not found and error handler
  // so that we can ensure any custom logic added can be accessed
  if (_isFunction(callback)) {
    callback(app);
  }

  app.get('*', (_: express.Request, res: express.Response) => res.status(404).send('Not Found'));
  app.use(errorHandlerMiddleware(log));

  const server = app.listen(port, () => {
    log.info(`${name || 'App'} started on port ${port}`);
  });

  // Ensure all inactive connections are terminated by the ALB
  // by setting this a few seconds higher than the ALB idle timeout
  server.keepAliveTimeout = keepAliveTimeout || defaultKeepAlive;
  // This should be bigger than `keepAliveTimeout + server's expected response time`
  // using 80,000 just to ensure it's long enough
  server.headersTimeout = headersTimeout || defaultHeadersTimeout;

  return { app, server };
};

export default setupExpress;
