/* istanbul ignore file */

import type * as types from './types';

export const SURFLINE: types.SURFLINE = 'Surfline';
export const COASTALWATCH: types.COASTALWATCH = 'Coastalwatch';
export const BUOYWEATHER: types.BUOYWEATHER = 'Buoyweather';
export const FISHTRACK: types.FISHTRACK = 'FishTrack';
export const WAVETRAK: types.WAVETRAK = 'Wavetrak';
export const MAGICSEAWEED: types.MAGICSEAWEED = 'Magicseaweed';

export const SL: types.SL = 'sl';
export const BW: types.BW = 'bw';
export const FS: types.FS = 'fs';
export const CW: types.CW = 'cw';
export const MSW: types.MSW = 'msw';
export const WT: types.WT = 'wt';

export const brandMap: { [Abbr in types.BrandAbbr]: types.Brand } = {
  [SL]: SURFLINE,
  [BW]: BUOYWEATHER,
  [FS]: FISHTRACK,
  [MSW]: MAGICSEAWEED,
  [WT]: WAVETRAK,
  [CW]: COASTALWATCH,
};

export const SL_PREMIUM = 'sl_premium';
export const SL_METERED = 'sl_metered';
export const SL_PROMOTIONAL = 'sl_promotional';
export const SL_FREE_TRIAL = 'sl_free_trial';
export const SL_VIP = 'sl_vip';
export const WEB = 'web';
export const ANDROID = 'android';
export const IOS = 'ios';

export const MAX_PASSWORD_LENGTH = 50;
export const MIN_PASSWORD_LENGTH = 6;
