/* istanbul ignore file */
import { Mongoose } from 'mongoose';
import logger from './logger';

export default (
  mongoose: Mongoose,
  connectionString: string,
  serviceName?: string,
  poolSize?: number,
): Promise<void> => {
  const log = logger(`${serviceName || ''}:MongoDB`);
  const mongoDbConfig = {
    poolSize: poolSize || 10,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  };
  return new Promise((resolve, reject) => {
    try {
      log.info(`Using Mongoose version: ${mongoose.version}`);
      log.debug(`Mongo Connection String: ${connectionString} `);

      mongoose.connect(connectionString, mongoDbConfig);
      mongoose.connection.once('open', () => {
        log.info(`MongoDB connected on ${connectionString}`);
        resolve();
      });
      mongoose.connection.on('error', (error) => {
        log.error({
          action: 'MongoDB:ConnectionError',
          error,
        });
        reject(error);
      });
    } catch (error) {
      log.error({
        message: 'MongoDB:ConnectionError',
        stack: error,
      });
      reject(error);
    }
  });
};
