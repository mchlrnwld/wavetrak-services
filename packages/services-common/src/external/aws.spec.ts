import { expect } from 'chai';
import { ImportMock } from 'ts-mock-imports';
import AWS from 'aws-sdk';
import sinon from 'sinon';

const getSecretValueStub = sinon.stub().returns({ promise: sinon.stub().returns('secret') });
ImportMock.mockFunction(AWS, 'SecretsManager', { getSecretValue: getSecretValueStub });

import { getAWSSecret } from './aws';

describe('aws', () => {
  it('should call getSecretValue', () => {
    getAWSSecret('someSecret')();

    expect(getSecretValueStub).to.have.been.calledOnceWithExactly({ SecretId: 'someSecret' });
  });
});
