import AWS, { AWSError } from 'aws-sdk';
import { PromiseResult } from 'aws-sdk/lib/request';

const secretsManager = new AWS.SecretsManager({
  apiVersion: '2017-10-17',
  region: 'us-west-1',
});

export const getAWSSecret = (secretId: string) => (): Promise<
  PromiseResult<AWS.SecretsManager.GetSecretValueResponse, AWSError>
> => secretsManager.getSecretValue({ SecretId: secretId }).promise();
