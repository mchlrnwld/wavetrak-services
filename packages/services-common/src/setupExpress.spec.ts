import { expect } from 'chai';
import sinon from 'sinon';
import newrelic from 'newrelic';
import { ImportMock } from 'ts-mock-imports';
import { RequestHandler } from 'express';
import * as express from './utils/createExpressApp';
import * as authenticateRequest from './middlewares/authenticateRequest';
import * as accessControl from './middlewares/accessControl';
import geoCountryIso from './middlewares/geoCountryIso';
import geoLatLon from './middlewares/geoLatLon';
import * as errorHandlerMiddleware from './middlewares/errorHandlerMiddleware';
import setupExpress, { PathParams } from './setupExpress';
import wrapErrors from './wrapErrors';
import wrapErrorsToNewRelic from './wrapErrorsToNewRelic';
import { APIError } from './errors';

describe('setupExpress', () => {
  let expressStub: sinon.SinonStub;
  let authenticateRequestStub: sinon.SinonStub;
  let accessControlSpy: sinon.SinonSpy;
  let errorHandlerMiddlewareStub: sinon.SinonStub;

  const useStub = sinon.stub();
  const enableStub = sinon.stub();
  const listenStub = sinon.stub().returns({});
  const getStub = sinon.stub();
  const appStub = { use: useStub, enable: enableStub, listen: listenStub, get: getStub };

  let infoStub: sinon.SinonStub;
  let errorStub: sinon.SinonStub;
  const loggerStub = console;

  beforeEach(() => {
    infoStub = sinon.stub(console, 'info');
    errorStub = sinon.stub(console, 'error');
    expressStub = ImportMock.mockFunction(express).returns(appStub);
    authenticateRequestStub = ImportMock.mockFunction(authenticateRequest).returns(
      'authenticateRequest',
    );
    errorHandlerMiddlewareStub = ImportMock.mockFunction(
      errorHandlerMiddleware,
      'errorHandlerMiddleware',
    ).returns('errorHandlerMiddleware');
    accessControlSpy = sinon.spy(accessControl, 'default');
  });

  afterEach(() => {
    useStub.reset();
    enableStub.reset();
    listenStub.resetHistory();
    getStub.reset();
    useStub.reset();

    expressStub.restore();
    authenticateRequestStub.restore();
    accessControlSpy.restore();
    errorHandlerMiddlewareStub.restore();

    infoStub.restore();
    errorStub.restore();
  });

  it('should instantiate an express server', () => {
    const { server } = setupExpress();
    expect(useStub).to.have.been.calledWith('authenticateRequest');
    expect(useStub).to.have.been.calledWith(geoCountryIso);
    expect(useStub).to.have.been.calledWith(geoLatLon);
    expect(useStub.lastCall).to.have.been.calledWith('errorHandlerMiddleware');
    expect(getStub).to.have.been.calledWith('/health');
    expect(getStub.lastCall).to.have.been.calledWith('*');
    expect(listenStub).to.have.been.calledWith(8081);
    expect(authenticateRequestStub).to.have.been.calledOnceWithExactly({
      userIdRequired: undefined,
      clientIdRequired: undefined,
    });
    expect(accessControlSpy).to.have.been.calledOnceWithExactly(
      appStub,
      undefined,
      undefined,
      undefined,
    );
    expect(errorHandlerMiddlewareStub).to.have.been.calledOnce();
    expect(server.keepAliveTimeout).to.be.equal(65000);
    expect(server.headersTimeout).to.be.equal(80000);

    expect(enableStub).to.have.been.calledOnceWithExactly('trust proxy');
  });

  it('should take configuration options', () => {
    const handlerStub: RequestHandler = () => 'a handler has no purpose';
    const routeStub: [PathParams, RequestHandler] = ['/path', handlerStub];
    const multiHandlerRouteStub: [PathParams, ...RequestHandler[]] = [
      '/path',
      handlerStub,
      handlerStub,
    ];
    const callbackStub = sinon.stub();
    const { server } = setupExpress({
      userIdRequired: true,
      clientIdRequired: false,
      allowedMethods: 'GET',
      allowedHeaders: ['x-auth-test'],
      allowedOrigins: 'www.surfline.com',
      name: 'TEST',
      port: 3000,
      log: loggerStub,
      keepAliveTimeout: 100,
      headersTimeout: 200,
      bodySizeLimit: 100,
      handlers: [handlerStub, routeStub, multiHandlerRouteStub],
      callback: callbackStub,
    });

    expect(useStub).to.have.been.calledWith(handlerStub);
    expect(useStub).to.have.been.calledWith(...routeStub);
    expect(useStub).to.have.been.calledWith(...multiHandlerRouteStub);
    expect(listenStub).to.have.been.calledWith(3000);
    expect(authenticateRequestStub).to.have.been.calledOnceWithExactly({
      userIdRequired: true,
      clientIdRequired: false,
    });
    expect(accessControlSpy).to.have.been.calledOnceWithExactly(
      appStub,
      'GET',
      ['x-auth-test'],
      'www.surfline.com',
    );
    expect(errorHandlerMiddlewareStub).to.have.been.calledOnceWithExactly(loggerStub);
    expect(server.keepAliveTimeout).to.be.equal(100);
    expect(server.headersTimeout).to.be.equal(200);
    expect(callbackStub).to.have.been.calledOnceWithExactly(appStub);
  });

  it('should skip calling accessControl if setupAccessControl is false', () => {
    setupExpress({
      setupAccessControl: false,
    });

    expect(accessControlSpy).to.not.have.been.called();
  });

  it('should error if an improper handler is passed', () => {
    const goodHandler: RequestHandler = () => 'a handler has no purpose';
    const badHandler: Partial<RequestHandler> = { very: 'bad handler' };
    try {
      setupExpress({
        handlers: [goodHandler, badHandler as RequestHandler],
      });
    } catch (err) {
      expect(useStub).to.have.been.calledWith(goodHandler);
      expect(err.message).to.contain('Handlers must either be');
    }
  });

  it('should execute a callback if one is passed', () => {
    const callback = sinon.stub();
    setupExpress({
      callback,
    });
    expect(callback).to.have.been.calledOnceWithExactly(appStub);
  });
});

describe('wrapErrors', () => {
  const fnStub = sinon.stub();
  afterEach(() => {
    fnStub.resetHistory();
    fnStub.resetBehavior();
  });

  it('should gracefully handle errors', async () => {
    const req = sinon.spy();
    const res = sinon.spy();
    const next = sinon.stub();
    fnStub.returns(Promise.reject('TEST'));
    const handler = wrapErrors(fnStub);
    await handler(req, res, next);

    expect(next).to.have.been.calledOnceWithExactly('TEST');
  });

  it('should succeed without errors', async () => {
    const req = sinon.spy();
    const res = sinon.spy();
    const next = sinon.spy();
    fnStub.resolves({ success: true });
    const handler = wrapErrors(fnStub);
    const result = await handler(req, res, next);

    expect(result).to.deep.equal({ success: true });
    expect(fnStub).to.have.been.calledOnceWithExactly(req, res, next);
  });
});

describe('wrapErrorsToNewRelic', () => {
  let newrelicStub: sinon.SinonStub;

  beforeEach(() => {
    newrelicStub = sinon.stub(newrelic, 'recordCustomEvent');
  });

  afterEach(() => {
    newrelicStub.restore();
  });

  it('should gracefully handle non-APIErrors errors and post them in NR', async () => {
    const req = { name: 'req', body: { body1: 'testingNR' }, originalUrl: 'originalURL' };
    const res = { name: 'res' };
    const next = sinon.fake.resolves(true);
    // const endpointType = 'endpoint';
    const error = new Error('Test');
    const handler = sinon.fake.throws(error);

    const wrappedHandler = wrapErrorsToNewRelic(handler);
    await wrappedHandler(req, res, next);

    const nrCall = newrelicStub.getCall(0);
    expect(handler).to.have.been.calledOnceWithExactly(req, res, next);
    expect(next).to.have.been.calledOnceWithExactly(error);
    expect(nrCall.args[0]).to.equal('Uncaught Error');
    expect(nrCall.args[1]).to.deep.equal({
      body: JSON.stringify({ body1: 'testingNR' } ),
      message: 'Test',
      end_point: req.originalUrl,
    });
  });

  it('should gracefully handle APIErrors errors and not post them in NR', async () => {
    const req = { name: 'req', body: { body1: 'testingNR' } };
    const res = { name: 'res' };
    const next = sinon.fake.resolves(true);
    // const endpointType = 'endpoint';
    const error = new APIError('Test');
    const handler = sinon.fake.throws(error);

    const wrappedHandler = wrapErrorsToNewRelic(handler);
    await wrappedHandler(req, res, next);
    expect(handler).to.have.been.calledOnceWithExactly(req, res, next);
    expect(next).to.have.been.calledOnceWithExactly(error);
    expect(newrelicStub).to.have.not.been.called();
  });

  it('should succeed without errors', async () => {
    const req = { name: 'req', body: { body1: 'testingNR' } };
    const res = { name: 'res' };
    const next = sinon.fake.resolves(true);
    // const endpointType = 'endpoint';
    const handler = sinon.fake();

    const wrappedHandler = wrapErrorsToNewRelic(handler);
    await wrappedHandler(req, res, next);
    expect(handler).to.have.been.calledOnceWithExactly(req, res, next);
    expect(newrelicStub).to.have.not.been.called();
  });
});
