import { RedisClient, ClientOpts } from 'redis';
import WavetrakCache from './cache/WavetrakCache';
import DisabledCache from './cache/DisabledCache';
import RedisCache from './cache/RedisCache';
import CacheInitOptions from './cache/CacheInitOptions';
import createRedisClient from './utils/createRedisClient';
import { SurflineLogger } from './types';

let cacheInstance: WavetrakCache;

const getClient = async (
  connection: RedisClient | string,
  log: SurflineLogger | Console,
  prefix?: string | undefined,
): Promise<RedisClient | string[]> => {
  if (typeof(connection) === 'string') {
    const splitConn = (connection as string).split(':'); // host & port
    return await createRedisClient(splitConn[0], splitConn[1], log, { prefix } as ClientOpts);
  }
  return connection;
};

/**
 * Init function for the cache that instatiates cacheInstance
 * @param {CacheInitOptions} options CachInitOptions
 */
export const initCache = async (
  options: CacheInitOptions
): Promise<void> => {

  const log = options.logger ?? console;

  if (!options.enabled) {
    cacheInstance = new DisabledCache();
    return;
  }

  const [readClient, writeClient] = await Promise.all([
    getClient(options.readRedis, log, options.prefix),
    getClient(options.writeRedis, log, options.prefix),
  ]);

  if (readClient && writeClient) {
    cacheInstance = new RedisCache(
      readClient as RedisClient,
      writeClient as RedisClient,
      options.defaultCacheExpiration);
  } else {
      throw new Error(`Invalid CacheInitOptions provided: ${options}`);
  }
};

export const cache = (): WavetrakCache => cacheInstance;
