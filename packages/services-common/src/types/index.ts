import type Bunyan from 'bunyan';
import type Analytics from 'analytics-node';
import type { Request } from 'express';
import type { IncomingHttpHeaders } from 'http';

// This import references the @types/express-serve-static-core
// As a result of the actual library express-serve-static-core being outdated
// we don't want to install it. We just have the types version as a dev
// dependency for when it is compiled from TS to JS
// eslint-disable-next-line import/no-unresolved
import type { Query } from 'express-serve-static-core';

export type SurflineLogger = Bunyan;

export type Identity = { userId: string } | { userId?: string; anonymousId: string };

export type SL = 'sl';
export type FS = 'fs';
export type BW = 'bw';
export type CW = 'cw';
export type MSW = 'msw';
export type WT = 'wt';

export type SURFLINE = 'Surfline';
export type FISHTRACK = 'FishTrack';
export type BUOYWEATHER = 'Buoyweather';
export type COASTALWATCH = 'Coastalwatch';
export type WAVETRAK = 'Wavetrak';
export type MAGICSEAWEED = 'Magicseaweed';

export type Brand = SURFLINE | FISHTRACK | BUOYWEATHER | COASTALWATCH | MAGICSEAWEED | WAVETRAK;

export type BrandAbbr = SL | FS | BW | CW | MSW | WT;

export type BrandAnalytics = { [Brand in BrandAbbr]: Analytics | null };
export type BrandLoggers = { [Brand in BrandAbbr]: SurflineLogger | Console };

export type SurflineQuery = {
  accesstoken?: string;
  units?: string;
  start?: string;
  end?: string;
  timezone?: string;
  interval?: string;
  lon?: string;
  lat?: string;
} & Query;

export type SurflineHeaders = {
  'x-auth-userid'?: string;
  'x-auth-accesstoken'?: string;
  'x-api-key'?: string;
  'x-auth-scopes'?: string;
  'x-geo-country-iso'?: string;
  'x-geo-latitude'?: string;
  'x-geo-longitude'?: string;
} & IncomingHttpHeaders;

export interface SurflineRequest extends Request {
  headers: SurflineHeaders;
  query: SurflineQuery;
  location?: {
    lat: number;
    lon: number;
  };
  times?: {
    timezone: string;
    start: number;
    end: number;
    interval: number;
  };
  units?: {
    surfHeight?: string;
    swellHeight?: string;
    tideHeight?: string;
    windSpeed?: string;
    temperature?: string;
    [key: string]: string | undefined;
  };
  authenticatedUserId?: string;
  authenticatedClientId?: string;
  scopes?: string;
  accesstoken?: string;
  geoLatitude?: string | number;
  geoLongitude?: string | number;
  geoCountryIso?: string | number;
}
