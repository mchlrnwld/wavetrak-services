/* istanbul ignore file */

import fetch from 'node-fetch';

const getScienceData = async (
  cdn: string,
  key: string,
  byteStart: string,
  byteEnd: string,
): Promise<Buffer> => {
  const url = `${cdn}/${key}`;
  const response = await fetch(url, {
    headers: {
      Range: `bytes=${byteStart}-${byteEnd}`,
    },
  });
  if (200 <= response.status && response.status < 300) return response.buffer();
  throw await response.text();
};

export default getScienceData;
