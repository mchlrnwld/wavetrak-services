type DateArray = [number, number, number, number, number];

const parseTimesByteData = (bytes: string | Buffer): number[] => {
  const timestamps = bytes
    .toString()
    .split(/(?:\r\n|\r|\n)/g)
    .filter(line => line)
    .map(line => {
      const dateTimeArray = (line.match(/\d+/g) as string[]).map((element: string, i: number):
        | number
        | string => (i === 1 ? parseInt(element, 10) - 1 : parseInt(element, 10)));
      return Date.UTC(...(dateTimeArray as DateArray)) / 1000;
    })
    .filter(Boolean);
  return timestamps;
};

export default parseTimesByteData;
