/* eslint-disable @typescript-eslint/no-explicit-any */
import { expect } from 'chai';
import deepMapObjectWithPath from './deepMapObjectWithPath';

describe('utils / deepMapObjectWithPath', () => {
  it('maps all values in complex object with a map function that takes the path to the value as a parameter', () => {
    const complexObject = {
      foo: [
        {
          bar: [
            'baz',
            {
              foobar: 'bazfoo',
            },
          ],
        },
        'bar',
      ],
      bar: {
        baz: {},
      },
      baz: null,
    };
    const map = (value: any, path: string): string => `${value}:${path}`;
    expect(deepMapObjectWithPath(map, complexObject)).to.deep.equal({
      foo: [
        {
          bar: [
            'baz:foo[0].bar[0]',
            {
              foobar: 'bazfoo:foo[0].bar[1].foobar',
            },
          ],
        },
        'bar:foo[1]',
      ],
      bar: {
        baz: {},
      },
      baz: 'null:baz',
    });
  });
});
