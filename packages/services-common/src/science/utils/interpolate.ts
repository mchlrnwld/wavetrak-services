interface InterpolationPoints {
  x0: number;
  y0: number;
  x1: number;
  y1: number;
  x: number;
}
/**
 * Linear interpolation
 */
const interpolate = ({ x0, y0, x1, y1, x }: InterpolationPoints): number => {
  const slope = (y1 - y0) / (x1 - x0);
  return y0 + (x - x0) * slope;
};

export default interpolate;
