/* eslint-disable @typescript-eslint/no-explicit-any */

interface GenericObject {
  [key: string]: any;
}

interface MapFunc {
  (value: any, path: string): any;
}

/**
 * Deep maps all values in an object to a new object
 * @param  {Function} map Map function takes a value and a path to the value
 * @param  {{}|any} object Value to deep map over
 * @param  {String} [path=''] String prepended to path to value
 * @return {Object} New object with all values mapped to new values
 */
const deepMapObjectWithPath = (map: MapFunc, object: GenericObject | any, path = ''): any => {
  if (Array.isArray(object)) {
    return object.map((value, i) => deepMapObjectWithPath(map, value, `${path}[${i}]`));
  }

  if (typeof object === 'object' && object !== null) {
    const mapped = Object.entries(object).map(([key, value]) => ({
      [key]: deepMapObjectWithPath(map, value, `${path}${path ? '.' : ''}${key}`),
    }));
    return Object.assign({}, ...mapped);
  }

  return map(object, path);
};

export default deepMapObjectWithPath;
