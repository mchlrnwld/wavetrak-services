import { expect } from 'chai';
import parseTimesByteData from './parseTimesByteData';

const bestWxTimes = `
2020           3           5          12           0
2020           3           5          13           0
2020           3           5          14           0
2020           3           5          15           0
2020           3           5          16           0
2020           3           5          17           0`;
const bestWxTimesAsBuffer = Buffer.from(bestWxTimes);

describe('utils / interpolate', () => {
  it('should parse string best weather times', () => {
    expect(parseTimesByteData(bestWxTimes)).to.deep.equal([
      1583409600,
      1583413200,
      1583416800,
      1583420400,
      1583424000,
      1583427600,
    ]);
  });
  it('should parse buffer best weather times', () => {
    expect(parseTimesByteData(bestWxTimesAsBuffer)).to.deep.equal([
      1583409600,
      1583413200,
      1583416800,
      1583420400,
      1583424000,
      1583427600,
    ]);
  });
});
