import { expect } from 'chai';
import interpolate from './interpolate';

describe('utils / interpolate', () => {
  it('returns the linear interpolation of 2 points', () => {
    expect(interpolate({
      x0: 1520838000,
      x1: 1520859600,
      y0: 0.93,
      y1: 0.66,
      x: 1520848800,
    })).to.equal(0.795);
  });
});
