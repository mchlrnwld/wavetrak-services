import { tz } from 'moment-timezone';
import { SurflineRequest } from '../../types';
import { Response, NextFunction } from 'express';

const DEFAULT_DAYS_IN_SECONDS = 17 * 24 * 60 * 60; // 17 days
const DEFAULT_INTERVAL_IN_MINUTES = 6 * 60; // 6 hours;

const times = (req: SurflineRequest, _: Response, next: NextFunction): void => {
  const timezone: string = req.query.timezone ? req.query.timezone : 'UTC';
  const startOfDay: number = parseInt(
    tz(timezone)
      .startOf('day')
      .format('X'),
    10,
  );

  const start: number = req.query.start ? parseInt(req.query.start, 10) : startOfDay;
  const end: number = req.query.end ? parseInt(req.query.end, 10) : start + DEFAULT_DAYS_IN_SECONDS;

  const intervalInMinutes: number = req.query.interval
    ? parseInt(req.query.interval, 10)
    : DEFAULT_INTERVAL_IN_MINUTES;
  const interval: number = 60 * intervalInMinutes;

  req.times = {
    timezone,
    start,
    end,
    interval,
  };

  return next();
};

export default times;
