import chai, { expect } from 'chai';
import 'chai-http';
import express from 'express';
import units from './units';
import { SurflineRequest } from '../../types';

describe('middleware / units', () => {
  let request: ChaiHttp.Agent;

  before(() => {
    const app = express();
    app.use(units, (req, res) => res.send((req as SurflineRequest).units));
    request = chai.request(app).keepOpen();
  });

  after(() => {
    request.close();
  });

  it('parses units from the query parameters and appends it to the request object', async () => {
    const res = await request.get('/?units=wave|ft,wind|kts');
    expect(res).to.have.status(200);
    expect(res.body).to.deep.equal({
      wave: 'ft',
      wind: 'kts',
    });
  });

  it('defaults if units query parameter not given', async () => {
    const res = await request.get('/');
    expect(res).to.have.status(200);
    expect(res.body).to.deep.equal({});
  });
});
