import chai, { expect } from 'chai';
import 'chai-http';
import sinon from 'sinon';
import express from 'express';
import times from './times';
import { SurflineRequest } from '../../types';

describe('middleware / times', () => {
  let request: ChaiHttp.Agent;
  let clock: sinon.SinonFakeTimers;

  before(() => {
    const app = express();
    app.use(times, (req, res) => res.send((req as SurflineRequest).times));
    clock = sinon.useFakeTimers(1521104400000); // 03/15/2018@9:00AM UTC
    request = chai.request(app).keepOpen();
  });

  after(() => {
    request.close();
    clock.restore();
  });

  it('parses times from the query parameters and appends it to the request object', async () => {
    const timezone = 'America/Los_Angeles';
    const start = 1520834400;
    const end = 1521104400;
    const interval = 60;
    const res = await request.get(
      `/?start=${start}&end=${end}&interval=${interval}&timezone=${timezone}`
    );
    expect(res).to.have.status(200);
    expect(res.body).to.deep.equal({
      timezone,
      start,
      end,
      interval: 3600,
    });
  });

  it('defaults to start of current day and 17 days in given timezone', async () => {
    const timezone = 'America/Los_Angeles';
    const res = await request.get(`/?timezone=${timezone}`);
    expect(res).to.have.status(200);
    expect(res.body).to.deep.equal({
      timezone,
      start: 1521097200, // 03/15/2018@7:00AM UTC (midnight PDT)
      end: 1522566000, // 04/01/2018@7:00AM UTC (midnight PDT)
      interval: 21600,
    });
  });

  it('defaults to start of current day and 17 days UTC', async () => {
    const res = await request.get('/');
    expect(res).to.have.status(200);
    expect(res.body).to.deep.equal({
      timezone: 'UTC',
      start: 1521072000, // 03/15/2018@12:00AM UTC
      end: 1522540800, // 04/01/2018@12:00AM UTC
      interval: 21600,
    });
  });
});
