import { Response, NextFunction } from 'express';
import { SurflineRequest } from '../../types';

const location = (req: SurflineRequest, _: Response, next: NextFunction): void => {
  const lat = req.query.lat ? parseFloat(req.query.lat) : 33.655072;
  const lon = req.query.lon ? parseFloat(req.query.lon) : -118.003402;

  req.location = {
    lat,
    lon,
  };

  return next();
};

export default location;
