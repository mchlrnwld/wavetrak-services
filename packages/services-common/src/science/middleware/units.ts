import { Response, NextFunction } from 'express';
import { SurflineRequest } from '../../types';

const units = (req: SurflineRequest, _: Response, next: NextFunction): void => {
  req.units = {};

  if (req.query.units) {
    const keyValues = req.query.units.split(',').map((unit: string) => unit.split('|'));
    Object.assign(req.units, ...keyValues.map((kv: string[]) => ({ [kv[0]]: kv[1] })));
  }

  return next();
};

export default units;
