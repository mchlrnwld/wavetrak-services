import chai, { expect } from 'chai';
import 'chai-http';
import express from 'express';
import location from './location';
import { SurflineRequest } from '../../types';

describe('middleware / location', () => {
  let request: ChaiHttp.Agent;

  before(() => {
    const app = express();
    app.use(location, (req, res) => res.send((req as SurflineRequest).location));
    request = chai.request(app).keepOpen();
  });

  after(() => {
    request.close();
  });

  it('parses lat/lon from the query parameters and appends it to the request object', async () => {
    const res = await request.get('/?lat=33.652&lon=-118.001');
    expect(res).to.have.status(200);
    expect(res.body).to.deep.equal({
      lat: 33.652,
      lon: -118.001,
    });
  });

  it('defaults to HB Pier if lat/lon query parameters not given', async () => {
    const res = await request.get('/');
    expect(res).to.have.status(200);
    expect(res.body).to.deep.equal({
      lat: 33.655072,
      lon: -118.003402,
    });
  });
});
