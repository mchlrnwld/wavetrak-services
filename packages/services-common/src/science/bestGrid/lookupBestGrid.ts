import { round } from 'lodash';
import store from './store';

const getSnapToPoint = (record: number): { lat: number; lon: number } => {
  // record start at `1`, latlons starts at `0`
  const point = store.latLons?.[record - 1];
  return {
    lat: round(point.lat, 4),
    lon: round(point.lon > 180 ? point.lon - 360.0 : point.lon, 4),
  };
};

const getRecord = (lat: number, lon: number): { record: -1 } | number => {
  const eps = 0.00001;
  let rlat = lat;
  let rlon = lon;

  // Bill O'Reilly's lookup algorithm from Fortran

  // Convert the entered lat lon into east long and colatitude coordinate frame
  // FIXME: Need to normalize lat to -90/+90 and lon to -180/+180 range
  if (rlon < 0) {
    rlon += 360;
  }
  rlat += 91;

  /*
   * Extract the individual digits of the lat lon values
   *
   * eg. for longitude:
   *
   * lon10   = the 1 or 2 digit lon in 10s of degrees. eg. 12 = 120 degrees.
   * lon1    = the one degree digit
   * lond(1) = the tenth degree digit
   * lond(2) = the hundredth degree digit
   * lond(3) = the thousandth degree digit
   * lond(4) = the ten-thousandth degree digit
   */
  rlon = Math.round(rlon * 10000);
  const lond = {};
  // i ranges from 4 to 1
  for (let i = 4; i >= 1; i -= 1) {
    lond[i] = Math.floor(rlon - Math.floor(rlon / 10) * 10);
    rlon /= 10;
  }
  const lon1 = Math.floor(rlon - Math.floor(rlon / 10) * 10);
  const lon10 = Math.floor(rlon / 10);

  rlat = Math.round(rlat * 10000);
  const latd = {};
  // i ranges from 4 to 1
  for (let i = 4; i >= 1; i -= 1) {
    latd[i] = Math.floor(rlat - Math.floor(rlat / 10) * 10);
    rlat /= 10;
  }
  const lat1 = Math.floor(rlat - Math.floor(rlat / 10) * 10);
  const lat10 = Math.floor(rlat / 10);

  // Using the lat/lon components, look up the record number in arrays
  const dg = store.lg?.[lon10][lat10];

  // `0` is valid value, must test for undefined
  if (typeof dg === 'undefined') {
    return { record: -1 };
  }

  const d0 = store.l0?.[dg - 1][lon1][lat1];

  // `0` is valid value, must test for undefined
  if (typeof d0 === 'undefined') {
    return { record: -1 };
  }

  // Typecast `store.l0Tot` since Typescript complains about the object possible being null
  // Since this is legacy code, I don't want to mess with the logic so we can cast it and any
  // subsequent cases
  const d1i = Math.floor(d0 / (store.l0Tot as number));
  const d1j = d1i / (d1i - eps);
  const d1n = Math.floor(d1j * (d0 - (store.l0Tot as number)) + 1);
  const d1 = store.l1?.[d1n - 1][lond[1]][latd[1]];

  // `0` is valid value, must test for undefined
  if (typeof d1 === 'undefined') {
    return { record: -1 };
  }

  const d2i = Math.floor(d1 / (store.l1Tot as number));
  const d2j = d2i / (d2i - eps);
  const d2n = Math.floor(d2j * (d1 - (store.l1Tot as number)) + 1);
  const d2 = store.l2?.[d2n - 1][lond[2]][latd[2]];

  // `0` is valid value, must test for undefined
  if (typeof d2 === 'undefined') {
    return { record: -1 };
  }

  return d0 + d1 + d2;
};

const lookupBestGrid = (lat: number, lon: number): { coordinate: { lat: number;  lon: number}; record: number } => {
  // Here we type cast this as a number, since it was implemented in JS and returns an incosistent
  // type that the `getSnapToPoint` function does not handle well in typescript.
  // Rather than refactor the function with little knowledge of it, we can typecast as a way of telling
  // typescript that "we know what we're doing, punk."
  const record = getRecord(lat, lon) as number;
  const { lat: gridLat, lon: gridLon } = getSnapToPoint(record);

  return {
    coordinate: {
      lat: gridLat,
      lon: gridLon,
    },
    record,
  };
};

export default lookupBestGrid;
