/* eslint-disable @typescript-eslint/no-explicit-any */
interface Store {
  lg: any[] | null;
  l0: any[] | null;
  l1: any[] | null;
  l2: any[] | null;
  latLons: any[] | null;
  l0Tot: number | null;
  l1Tot: number | null;
}

const store: Store = {
  // initialize arrays containing lookup grids
  // array dimensions: 36 x 18
  lg: null,

  // array dimensions: 650 x 10 x 10
  l0: null,

  // array dimensions: 100000 x 10 x 10
  l1: null,

  // array dimensions: 100000 x 10 x 10
  l2: null,

  latLons: null,

  // lxTot = total number of grid points that are defined data points
  l0Tot: null,
  l1Tot: null,
};

export default store;
