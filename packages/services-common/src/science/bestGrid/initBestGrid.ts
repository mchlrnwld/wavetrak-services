import fs from 'fs';
import readline from 'readline';
import store from './store';
import { SurflineLogger } from '../../types';

const loadLookupArray = (
  filename: string,
  gridLevel: string,
  dim1: number,
  dim2: number,
  dim3?: number
// eslint-disable-next-line @typescript-eslint/no-explicit-any
): Promise<{ arr: any[]; totalPoints: number }> =>
  new Promise(resolve => {
    const arr = [...Array(dim1)].map(() => {
      const arr2 = [...Array(dim2)];
      if (dim3) return arr2.map(() => [...Array(dim3)]);
      return arr2;
    });
    let isFirstLine = true;
    let itemCount = 0;
    let totalGrids: number;
    let totalPoints: number;

    const rl = readline.createInterface({
      input: fs.createReadStream(filename),
    });

    rl.on('line', line => {
      // grid 'g' is 2d and doesn't have a header line
      if (isFirstLine && gridLevel !== 'g') {
        isFirstLine = false;
        [totalGrids, totalPoints] = line
          .trim()
          .split(/\s+/)
          .map(str => parseInt(str, 10));
      } else if (gridLevel === 'g') {
        line
          .trim()
          .split(/\s+/)
          .map(str => parseInt(str, 10))
          .forEach(point => {
            arr[itemCount % 36][Math.floor(itemCount / 36) % 18] = point;
            itemCount += 1;
          });
      } else {
        line
          .trim()
          .split(/\s+/)
          .map(str => parseInt(str, 10))
          .forEach(point => {
            // FIXME: Confirm this indexing
            const firstIndex = itemCount % totalGrids;
            const secondIndex = Math.floor(itemCount / totalGrids) % 10;
            const thirdIndex = Math.floor(itemCount / totalGrids / 10) % 10;
            arr[firstIndex][secondIndex][thirdIndex] = point;
            itemCount += 1;
          });
      }
    });

    rl.on('close', () => {
      resolve({ arr, totalPoints });
    });
  });

const loadLatLonArray = (filename: string): Promise<{
  lat: number;
  lon: number;
}[]>  =>
  new Promise((resolve, reject) =>
    fs.readFile(filename, (err, data) => {
      if (err) return reject(err);
      const latLons = [...Array(data.length / 8)].map((_, i) => ({
        lat: data.readFloatLE(i * 8),
        lon: data.readFloatLE(i * 8 + 4),
      }));
      return resolve(latLons);
    })
  );

const initBestGrid = async (
  log: SurflineLogger,
  lgFilename: string,
  l0Filename: string,
  l1Filename: string,
  l2Filename: string,
  latLonFilename: string
): Promise<void> => {
  const [lg, l0, l1, l2, latLons] = await Promise.all([
    loadLookupArray(lgFilename, 'g', 36, 18),
    loadLookupArray(l0Filename, '0', 650, 10, 10),
    loadLookupArray(l1Filename, '1', 100000, 10, 10),
    loadLookupArray(l2Filename, '2', 100000, 10, 10),
    loadLatLonArray(latLonFilename),
  ]);
  store.lg = lg.arr;
  store.l0Tot = l0.totalPoints;
  store.l0 = l0.arr;
  store.l1 = l1.arr;
  store.l1Tot = l1.totalPoints;
  store.l2 = l2.arr;
  store.latLons = latLons;
  log.info('Best grid loaded');
};

export default initBestGrid;
