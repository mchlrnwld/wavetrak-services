# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## 6.1.2 (2022-02-16)

* fix: remove support for hawaiian conversion

## 6.1.1 (2021-12-23)

* fix: cache fetch type supports generic type

## [6.1.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/services-common@6.0.1...@surfline/services-common@6.1.0) (2021-11-10)


### Features

* export minimum password length constant ([aff9c0b](https://github.com/Surfline/wavetrak-services/commit/aff9c0b6215c497085a069a3da32572f9d2fe85f))



## <small>6.0.1 (2021-10-15)</small>

* fix(services-common): revert typescript 4 update ([42d2a96](https://github.com/Surfline/wavetrak-services/commit/42d2a96))
* chore(services-common): move to packages directory ([d797f79](https://github.com/Surfline/wavetrak-services/commit/d797f79))





## 6.0.0 (2021-09-29)

* feat(services-common)!: update Error contracts ([8589450](https://github.com/Surfline/wavetrak-services/commit/8589450))


### BREAKING CHANGE

* update BaseError contract by moving all additional parameters to one options object after message parameter

- changes BaseError contract, including APIError, NotFound, RateLimit,
  ServerError and Unauthorized errors
- add data param to options object to support further data in error
  logging
- resolve tests




## 5.5.0 (2021-07-28)

* feat: Add `MAX_PASSWORD_LENGTH` constant ([0b98ad4](https://github.com/Surfline/wavetrak-services/commit/0b98ad4))





## <small>5.4.2 (2021-07-19)</small>

* fix: Export getTreatmentWithConfig Split function ([c451a4b](https://github.com/Surfline/wavetrak-services/commit/c451a4b))





## <small>5.4.1 (2021-06-25)</small>

* fix: Export getPlatform util function ([af23489](https://github.com/Surfline/wavetrak-services/commit/af23489))





## 5.4.0 (2021-06-24)

* feat: Add getPlatform util helper ([062f206](https://github.com/Surfline/wavetrak-services/commit/062f206))





## 5.3.0 (2021-06-21)

* feat: Add getTreatmentWithConfig to split healper ([4ba1f37](https://github.com/Surfline/wavetrak-services/commit/4ba1f37))





## <small>5.2.1 (2021-06-07)</small>

* Add entitlement constants ([982fb38](https://github.com/Surfline/wavetrak-services/commit/982fb38))





## 5.2.0 (2021-04-28)

* feat: Add Segment `identify` function ([3b614ab](https://github.com/Surfline/wavetrak-services/commit/3b614ab))





## 5.1.0 (2021-04-14)

* RT-192 NR wrapper function for POST sessions handlers (#4271) ([0a94a57](https://github.com/Surfline/wavetrak-services/commit/0a94a57)), closes [#4271](https://github.com/Surfline/wavetrak-services/issues/4271)





## <small>5.0.1 (2021-02-25)</small>

* Fix PageView logic in PATCH /anonymous ([7c20a1f](https://github.com/Surfline/wavetrak-services/commit/7c20a1f))





## 5.0.0 (2021-02-01)

* fix: lint use Callback instead of Function in cache unit tests ([1e98f2c](https://github.com/Surfline/wavetrak-services/commit/1e98f2c))
* KBYG-000: remove ts-ignore/eslint rules for redis cache test (#4115) ([8077f0c](https://github.com/Surfline/wavetrak-services/commit/8077f0c)), closes [#4115](https://github.com/Surfline/wavetrak-services/issues/4115)
* KBYG-3433: Add WavetrakCache to services-common (Updated) (#4066) ([fa0e09c](https://github.com/Surfline/wavetrak-services/commit/fa0e09c)), closes [#4066](https://github.com/Surfline/wavetrak-services/issues/4066)


### BREAKING CHANGE

* initCache renamed to initRedis



## 4.1.0 (2021-01-27)

* Merge branch 'master' into kbyg-3433-add-cache-to-services-common ([6513e37](https://github.com/Surfline/wavetrak-services/commit/6513e37))
* refactor: clean up cache tests ([a7b62b0](https://github.com/Surfline/wavetrak-services/commit/a7b62b0))
* refactor: remove lint rules, unused params ([b9bfaad](https://github.com/Surfline/wavetrak-services/commit/b9bfaad))
* refactor: simpler code changes/naming ([abf4558](https://github.com/Surfline/wavetrak-services/commit/abf4558))
* fix: add returns to reject ([011c245](https://github.com/Surfline/wavetrak-services/commit/011c245))
* fix: await all instead of sequentially when creating read/write redis clients ([be19322](https://github.com/Surfline/wavetrak-services/commit/be19322))
* fix: change name for redisClient instance ([c477d1f](https://github.com/Surfline/wavetrak-services/commit/c477d1f))
* fix: disable eslint no-any rule for unit tests ([ef722dc](https://github.com/Surfline/wavetrak-services/commit/ef722dc))
* fix: missing return statement for resolve ([7147768](https://github.com/Surfline/wavetrak-services/commit/7147768))
* fix: more specific lint/ts rules ([6d8ecfe](https://github.com/Surfline/wavetrak-services/commit/6d8ecfe))
* fix: various improvements ([0e7dfa1](https://github.com/Surfline/wavetrak-services/commit/0e7dfa1))
* feat: add prefix parameter to initcache ([7e11681](https://github.com/Surfline/wavetrak-services/commit/7e11681))
* feat: add read/write redis connections to wavetrak cache ([b9dd6cd](https://github.com/Surfline/wavetrak-services/commit/b9dd6cd))
* feat: added unit tests ([5223f38](https://github.com/Surfline/wavetrak-services/commit/5223f38))
* feat: rewrite wavetrak cache into classes and interfaces ([6c8b6b6](https://github.com/Surfline/wavetrak-services/commit/6c8b6b6))
* feat: wt-cache initial commit inside services-common ([ca2547e](https://github.com/Surfline/wavetrak-services/commit/ca2547e))
* chore: change key prefix ([e64fbdf](https://github.com/Surfline/wavetrak-services/commit/e64fbdf))
* chore: lint ([c11eca7](https://github.com/Surfline/wavetrak-services/commit/c11eca7))
* chore: lint warnings and improvements from pr comments ([b6e0e37](https://github.com/Surfline/wavetrak-services/commit/b6e0e37))
* chore: update docs/comments ([be9f99a](https://github.com/Surfline/wavetrak-services/commit/be9f99a))





## 4.0.0 (2020-12-29)

* chore: add coverage to gitignore ([2b00b26](https://github.com/Surfline/wavetrak-services/commit/2b00b26))
* chore: add missing property to test error object ([fd9aed0](https://github.com/Surfline/wavetrak-services/commit/fd9aed0))
* chore: bump dependencies ([1312cce](https://github.com/Surfline/wavetrak-services/commit/1312cce))
* chore: fix broken types ([fa96034](https://github.com/Surfline/wavetrak-services/commit/fa96034))
* fix: ensure access control headers join with a comma and a space ([d2b0acf](https://github.com/Surfline/wavetrak-services/commit/d2b0acf))


### BREAKING CHANGE

* [mongoose 5.11.0](https://github.com/Automattic/mongoose/blob/master/History.md#5110--2020-11-30) added typescript types, consuming packages using typescript will need to upgrade
  to the latest mongoose version and remove the @types/mongoose package




## 3.4.0 (2020-12-09)

* feat: allow manually setting pool size for mongo ([053b66e](https://github.com/Surfline/wavetrak-services/commit/053b66e))





## <small>3.3.2 (2020-11-04)</small>

* fix: added new param for convertWaveHeightUnits ([d9bf5e5](https://github.com/Surfline/wavetrak-services/commit/d9bf5e5))
* PS-000 - Replace artifactoryonline.com references with jfrog.io. (#3664) ([21430cd](https://github.com/Surfline/wavetrak-services/commit/21430cd)), closes [#3664](https://github.com/Surfline/wavetrak-services/issues/3664)





## <small>3.3.1 (2020-07-15)</small>

* fix: KBYG-3291 fix float issues in generate grid ([fda11e6](https://github.com/Surfline/wavetrak-services/commit/fda11e6))





## 3.3.0 (2020-07-14)

* documentation: update comments ([c3b4e03](https://github.com/Surfline/wavetrak-services/commit/c3b4e03))
* chore: lint ([7ecfe0a](https://github.com/Surfline/wavetrak-services/commit/7ecfe0a))
* feat: add precision to convertWaveHeightUnits ([e0cc95b](https://github.com/Surfline/wavetrak-services/commit/e0cc95b))





## <small>3.2.2 (2020-07-09)</small>

* fix: ensure limBounds works for global grids ([76a0c5c](https://github.com/Surfline/wavetrak-services/commit/76a0c5c))
* SA-000: Alter TTL of secrets helper common lib (#3409) ([68edcfe](https://github.com/Surfline/wavetrak-services/commit/68edcfe)), closes [#3409](https://github.com/Surfline/wavetrak-services/issues/3409)





## <small>3.2.1 (2020-07-07)</small>

* improvement: remove null,undefined params in convertWaveHeightUnits ([e6572e4](https://github.com/Surfline/wavetrak-services/commit/e6572e4))
* documentation: added function description for convertWaveHeightUnits ([a5e4f51](https://github.com/Surfline/wavetrak-services/commit/a5e4f51))





## 3.2.0 (2020-07-02)

* feat(add): add convertUnits ([34028ae](https://github.com/Surfline/wavetrak-services/commit/34028ae))





## 3.1.0 (2020-07-02)

* feat: gridpoints in box (KBYG-3135) (#3337) ([506aa39](https://github.com/Surfline/wavetrak-services/commit/506aa39)), closes [#3337](https://github.com/Surfline/wavetrak-services/issues/3337)





## <small>3.0.5 (2020-06-11)</small>

* chore: move services common into /common ([5125848](https://github.com/Surfline/wavetrak-services/commit/5125848))





## <small>3.0.4 (2020-06-08)</small>

* fix: downgrade typescript to 3.8 ([357410d](https://github.com/Surfline/wavetrak-services/commit/357410d))





## <small>3.0.3 (2020-06-08)</small>

* fix: Fix split types not properly included in declaration files ([1df7b9e](https://github.com/Surfline/wavetrak-services/commit/1df7b9e))





## <small>3.0.2 (2020-06-01)</small>

* fix: Fix an issue where mongo duplicate key errors are not caught ([eef4485](https://github.com/Surfline/wavetrak-services/commit/eef4485))





## <small>3.0.1 (2020-05-22)</small>

* fix: Move necessary @types dependencies to production dependencies ([572b237](https://github.com/Surfline/wavetrak-services/commit/572b237))





## 3.0.0 (2020-05-21)

* fix!: Update CORS rules services-common ([e3eed2e](https://github.com/Surfline/wavetrak-services/commit/e3eed2e))


### BREAKING CHANGE

* Remove the `setAccessControlHeaders` in favor of `setupAccessControl`

### Changes

- chore: Use the `cors` library instead of using a custom middleware
- feat: Add Surfline Specific headers to `Access-Control-Allow-Headers`
- feat: Allow configuration of whitelisted origins in `Access-Control-Allow-Origin` using the `allowedOrigins` option for `setupExpress`
- feat: Allow extension of allowed headers using the `allowedHeaders` option for the `setupExpress` function




## 2.1.0 (2020-05-13)

* feat: add decr command to promisified redis ([cec380b](https://github.com/Surfline/wavetrak-services/commit/cec380b))





## <small>2.0.1 (2020-05-12)</small>

* fix: Add missing getSecret export in services common ([706ce22](https://github.com/Surfline/wavetrak-services/commit/706ce22))





## 2.0.0 (2020-04-30)

* feat: Export PromisifiedRedis interface, change redisPromise to getPromisifiedRedis ([a45c197](https://github.com/Surfline/wavetrak-services/commit/a45c197))
* chore: improve test coverage ([1cf75e3](https://github.com/Surfline/wavetrak-services/commit/1cf75e3))
* chore: update dependencies ([2d36670](https://github.com/Surfline/wavetrak-services/commit/2d36670))


### BREAKING CHANGE

* Changed the export `redisPromise` to be `getPromisifiedRedis`




## <small>1.3.1 (2020-04-23)</small>

**Note:** Version bump only for package @surfline/services-common





## 1.3.0 (2020-04-23)

* feat: Add `setupAnalytics` and `trackEvent` implementations ([353e0d5](https://github.com/Surfline/wavetrak-services/commit/353e0d5))
* feat: Add Split IO functions `setupFeatureFramework` & `getTreatment` ([1ec4ce8](https://github.com/Surfline/wavetrak-services/commit/1ec4ce8))
* fix: improve express and SurflineRequest types to be more extendable ([8dc6490](https://github.com/Surfline/wavetrak-services/commit/8dc6490)), closes [/github.com/DefinitelyTyped/DefinitelyTyped/pull/43434#issuecomment-617638746](https://github.com//github.com/DefinitelyTyped/DefinitelyTyped/pull/43434/issues/issuecomment-617638746)





## <small>1.2.2 (2020-04-21)</small>

* fix: Fix issue where multiple mongoose versions could exist causing queries to time out ([8d03de8](https://github.com/Surfline/wavetrak-services/commit/8d03de8))
* chore: fix broken express types resulting from library type updates ([cd4deeb](https://github.com/Surfline/wavetrak-services/commit/cd4deeb)), closes [/github.com/DefinitelyTyped/DefinitelyTyped/pull/43434#issuecomment-607181516](https://github.com//github.com/DefinitelyTyped/DefinitelyTyped/pull/43434/issues/issuecomment-607181516)





## <small>1.2.1 (2020-04-17)</small>

* fix: Fix types for SurflineRequest and export ([17a9086](https://github.com/Surfline/wavetrak-services/commit/17a9086))
* fix: Improve logger setup to enable passing a logseneKey ([ed061fd](https://github.com/Surfline/wavetrak-services/commit/ed061fd))
* fix: improve typings and fix tests ([c097e41](https://github.com/Surfline/wavetrak-services/commit/c097e41))





## 1.2.0 (2020-04-16)

* feat: Add logger, redis, and mongo to services common ([7637f58](https://github.com/Surfline/wavetrak-services/commit/7637f58))





## <small>1.1.6 (2020-04-02)</small>

* fix: Fix errorHandler not adding wavetrakCode to APIError body ([aa3ac1d](https://github.com/Surfline/wavetrak-services/commit/aa3ac1d))





## <small>1.1.5 (2020-04-02)</small>

* fix: Fix wavetrakCode not set for APIErrors ([7ecf6c8](https://github.com/Surfline/wavetrak-services/commit/7ecf6c8))





## <small>1.1.4 (2020-03-30)</small>

* fix: Fix url encoding being enabled by default, add flag to disable it ([f36706e](https://github.com/Surfline/wavetrak-services/commit/f36706e))





## [1.1.3](https://github.com/Surfline/wavetrak-services/compare/@surfline/services-common@1.1.2...@surfline/services-common@1.1.3) (2020-03-20)


### Bug Fixes

* add RequestErrorHandler type to handlers array for setupExpress ([595058a](https://github.com/Surfline/wavetrak-services/commit/595058a8f08d27b8dec588459041e6fdf2392352))





## [1.1.2](https://github.com/Surfline/wavetrak-services/compare/@surfline/services-common@1.1.1...@surfline/services-common@1.1.2) (2020-03-18)


### Bug Fixes

* Create a tagging script for Lerna ([9812c76](https://github.com/Surfline/wavetrak-services/commit/9812c76d618b9f54ba34403cf2d026fb983ea856))





# 1.1.1 (2020-03-18)


### Bug Fixes

* Fix the `code` property missing from the payload on Errors ([504116e](https://github.com/Surfline/wavetrak-services/commit/504116ea50e5adc6ae9ed8b5cb4ddb187a838b58))

### Chores

* Change `BaseError.surflineCode` to `BaseError.wavetrakCode` ([f5201c9](https://github.com/Surfline/wavetrak-services/commit/f5201c966808be570e07b47f85d4ce989c711910))




# 1.1.0 (2020-03-16)


### Features

* Add ability to pass `bodySizeLimit` to pass a size limit to `bodyParser` functions: `json` and `urlencoded` ([056a464](https://github.com/Surfline/wavetrak-services/commit/056a464cd207c272bb365483dbf5a50136604059))




# 1.0.1 (2020-03-11)


### Bug Fixes

* Fix setupExpress not applying all request handlers ([68b1ef4](https://github.com/Surfline/wavetrak-services/commit/6b81ef4d360912b06f2ad0a1f236100e99b48787))
* Remove unneeded dependency, and allow node 8 ([a1c7d76](https://github.com/Surfline/wavetrak-services/commit/a1c7d761957e6859c04c19a9a0383a9f9135bc59))

### Chores

* Add middlewares to main exports ([105f7eb](https://github.com/Surfline/wavetrak-services/commit/105f7eb19bdf70ef47189dd25facd88cb04467bd))




# 1.0.0 (2020-03-11)


### Bug Fixes

* Remove customHandlers in favor of a single callback ([35420f1](https://github.com/Surfline/wavetrak-services/commit/35420f1cbfe3a272bcb9daf01416c190128509a0))


### BREAKING CHANGES

* Removed the `customHandlers` array argument from `setupExpress` in favor of a single `callback` parameter which takes the app as an argument





# 0.2.0 (2020-03-09)


### Features

* Add Express Setup To Services Common ([#3028](https://github.com/Surfline/wavetrak-services/issues/3028)) ([3b4272d](https://github.com/Surfline/wavetrak-services/commit/3b4272de4f790fc0350b104a677cae66135a010b))
* migrate services-common to typescipt ([b4fe33e](https://github.com/Surfline/wavetrak-services/commit/b4fe33ec48b3537b59efb47792b4734f7527a6b1))





## 0.1.5 (2020-03-04)

**Note:** Version bump only for package @surfline/services-common





## 0.1.4 (2020-03-04)

**Note:** Version bump only for package @surfline/services-common





## 0.1.3 (2020-02-25)

**Note:** Version bump only for package @surfline/services-common





## 0.1.3 (2020-02-25)

**Note:** Version bump only for package @surfline/services-common





## 0.1.2 (2020-02-03)

**Note:** Version bump only for package @surfline/services-common

- Added more config options and configured for Lerna releasing the package


## 0.1.1 (2020-02-03)

**Note:** Version bump only for package @surfline/services-common

- Added `secretsHelper` as a util for use in node airflow jobs when needing AWS Secrets Manager secrets
