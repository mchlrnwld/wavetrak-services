/* eslint-disable strict */
/* eslint-disable global-require */

'use strict';

const { declare } = require('@babel/helper-plugin-utils');

module.exports = declare((api, options) => {
  // see docs about api at https://babeljs.io/docs/en/config-files#apicache
  api.assertVersion('^7.0.0');

  const { ssr = false, removePropTypes = true } = options;

  const development =
    typeof options.development === 'boolean'
      ? options.development
      : api.cache.using(() => process.env.NODE_ENV === 'development');

  return {
    presets: [[require('@babel/preset-react'), { development }]],
    plugins: [
      // Remove prop-types from the production builds, since they are only used in development
      removePropTypes ? require('babel-plugin-transform-react-remove-prop-types') : null,

      ssr ? require('react-loadable/babel') : null,

      development ? require('react-hot-loader/babel') : null,
    ].filter(Boolean),
  };
});
