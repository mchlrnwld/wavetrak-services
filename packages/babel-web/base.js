/* eslint-disable strict */
/* eslint-disable global-require */
/* eslint-disable no-console */

'use strict';

const { declare } = require('@babel/helper-plugin-utils');

module.exports = declare((api, options) => {
  // see docs about api at https://babeljs.io/docs/en/config-files#apicache
  api.assertVersion('^7.0.0');

  // Get the consuming package.json
  // eslint-disable-next-line import/no-dynamic-require
  const packageJson = require(`${process.cwd()}/package.json`);

  function buildTargets({ customTargets, debug }) {
    const isServer = api.caller((caller) => caller && caller.target === 'node');
    if (debug) {
      console.log(`@surfline/babel-preset-web: Compiling for ${isServer ? 'node' : 'browser'}\n`);
    }

    if (isServer) {
      return { node: 'current' };
    }

    if (customTargets) {
      return customTargets;
    }

    if (!packageJson.browserslist) {
      // We definitely don't want to transform all code, since this will bloat the output.
      // We'll throw an error here since this package is only internal
      throw new Error(
        'Warning: "browserslist" property not found in package.json. This means that babel will transform' +
          'all ES2015+ code by default, which is not desired.' +
          'See: https://babeljs.io/docs/en/babel-preset-env#targets',
      );
    }

    return {};
  }

  const {
    modules = 'auto',
    targets = buildTargets(options),
    loose = false,
    runtimeVersion,
    runtimeHelpersUseESModules = !modules,
    useBuiltIns = 'usage',
    transformRuntime = true,
  } = options;

  if (typeof modules !== 'boolean' && modules !== 'auto' && modules !== 'cjs') {
    throw new TypeError(
      '@surfline/babel-preset-web only accepts `true`, `false`, `"auto"`,' +
        'or `"cjs"` as the value of the "modules" option',
    );
  }

  const debug = typeof options.debug === 'boolean' ? options.debug : false;

  return {
    presets: [
      [
        require('@babel/preset-env'),
        {
          debug,
          exclude: ['transform-async-to-generator', 'transform-regenerator'],
          loose,
          corejs: 3,
          useBuiltIns,
          modules,
          bugfixes: true,
          targets,
        },
      ],
    ],
    plugins: [
      transformRuntime
        ? [
            require('@babel/plugin-transform-runtime'),
            {
              absoluteRuntime: false,
              corejs: false,
              helpers: true,
              regenerator: false,
              useESModules: runtimeHelpersUseESModules,
              version: runtimeVersion,
            },
          ]
        : null,
      require('@babel/plugin-proposal-export-default-from'),
      require('@babel/plugin-proposal-class-properties'),
    ].filter(Boolean),
  };
});
