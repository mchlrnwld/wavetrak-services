# `@surfline/babel-preset-web`

This package is a babel preset for the babel config for Surfline Wep Applications.

## Publishing NPM package

Within the `/packages/` directory we publish NPM packages using Lerna.

A step by step guide for publishing NPM packages in the monorepo can be found [here](../../README.md#npm-packages).

## Usage

This config should be used in the `presets` property which babel exposes for .babelrc files.

It is important to note that there are different configurations exposed in this package:
 - Base configuration
 - React configuration

These configurations can be used individually, or in tandem (it is however unlikely you would use the `react` config alone)

The configurations can be consumed as follows:

#### Babel base configuration

```javascript
// .eslintrc
{
  "presets": [
    [
      "@surfline/babel-preset-web/base",
      {
        // options go here
      }
    ]
  ],
}
```

#### Babel base configuration with React

```javascript
// .eslintrc
{
  "presets": [
    [
      "@surfline/babel-preset-web/base",
      {
        // base options go here
      }
    ],
    [
      "@surfline/babel-preset-web/react",
      {
        // react options go here
      }
    ]
  ],
}
```

#### Babel react configuration only

```javascript
// .eslintrc
{
  "presets": [
    [
      "@surfline/babel-preset-web/react",
      {
        // react options go here
      }
    ]
  ],
}
```

## Options

Our config exposes a few different options:

### React config options

#### Development

Key: `development`

Default: `process.env.NODE_ENV === "development"`

Valid values: `true, false`

Desciption: Whether or not the current environment is development

#### Server Side Rendering

Key: `ssr`

Default: `false`

Valid values: `true || false`

Description: Whether or not to add the `react-loadable` babel plugin for SSR compatibility

#### Remove PropTypes

Key: `removePropTypes`

Default: `true`

Description: Whether or not to remove prop-types in the production bundle (this will most likely be desired).

https://www.npmjs.com/package/babel-plugin-transform-react-remove-prop-types

### Base config options

#### Debug

Key: `debug`

Default: `false`

Valid values: `true, false`

Description: Whether or not to enable verbose debugging logs

#### Modules

Key: `modules`

Default: `"auto"`

Valid values: `true, false, "auto", "cjs"`

Description: Whether or not to transform modules (defaults to `auto`)

*Note:* When using `sinon` for tests, it is likely you will have to pass `modules: "cjs"` for this option, since
sinon breaks for non commonjs modules.

https://babeljs.io/docs/en/babel-preset-env#modules

#### Targets

Key: `targets`

Default: `[">0.2%", "not dead", "not IE 11", "not op_mini all"]`

Description: A list of environment targets for the project. This value is passed directly to `@babel/preset-env` if set.

https://babeljs.io/docs/en/babel-preset-env#targets

#### Loose

Key: `loose`

Default: `false`

Description: Passes the loose option to `@babel/preset-env`. [See babel documentation on loose option for preset-env](https://babeljs.io/docs/en/babel-preset-env#loose)

#### Transform Runtime

Key: `transformRuntime`

Default: `true`

Description: [See babel documentation](https://babeljs.io/docs/en/babel-plugin-transform-runtime)

#### Runtime Version

Key: `runtimeVersion`

Default: `null`

Description: [see babel documentation](https://babeljs.io/docs/en/babel-plugin-transform-runtime#version)

#### Runtime Helper Use ES Modules

Key: `runtimeHelpersUseESModules`

Default: `!modules`

Description: [see babel documentation](https://babeljs.io/docs/en/babel-plugin-transform-runtime#useesmodules)

