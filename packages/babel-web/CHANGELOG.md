# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## <small>1.0.6 (2021-10-13)</small>

* chore(babel-web): migrate to packages directory ([8e17f58](https://github.com/Surfline/wavetrak-services/commit/8e17f58))





## <small>1.0.5 (2021-06-11)</small>

* chore: clean up changelog and repository url (#4453) ([45457f4](https://github.com/Surfline/wavetrak-services/commit/45457f4)), closes [#4453](https://github.com/Surfline/wavetrak-services/issues/4453)




## <small>1.0.4 (2020-12-03)</small>

* chore(deps): update deps ([a363531](https://github.com/Surfline/wavetrak-services/commit/a363531))





## <small>1.0.3 (2020-11-04)</small>

* PS-000 - Replace artifactoryonline.com references with jfrog.io. (#3664) ([21430cd](https://github.com/Surfline/wavetrak-services/commit/21430cd)), closes [#3664](https://github.com/Surfline/wavetrak-services/issues/3664)





## <small>1.0.2 (2020-05-12)</small>

**Note:** Version bump only for package @surfline/babel-preset-web





## <small>1.0.1 (2020-04-23)</small>

**Note:** Version bump only for package @surfline/babel-preset-web





## 1.0.0 (2020-04-08)

* feat: Add support for compiling server code with node as the target ([9b20372](https://github.com/Surfline/wavetrak-services/commit/9b20372))
* feat: Improve browser targeting support by checking package.json for browserlist property ([3ee7c70](https://github.com/Surfline/wavetrak-services/commit/3ee7c70))
* fix!: Remove extra unneeded plugins causing issues and bloating code ([9f399b4](https://github.com/Surfline/wavetrak-services/commit/9f399b4))
* chore: add prettier and eslint ([bc7405d](https://github.com/Surfline/wavetrak-services/commit/bc7405d))
* chore: update readme to reflect removed property ([1244387](https://github.com/Surfline/wavetrak-services/commit/1244387))


### BREAKING CHANGE

* Removed the `looseClasses` option
* Removed some plugins from the config




## <small>0.1.2 (2020-03-23)</small>

* fix: upgrade dependencies to fix https://github.com/babel/babel/pull/11201 ([89f3164](https://github.com/Surfline/wavetrak-services/commit/89f3164))
* chore: Add post-publish script for Lerna ([7bc989a](https://github.com/Surfline/wavetrak-services/commit/7bc989a))
* chore: Move @surfline/babel-preset-web to wavetrak-services repo ([d5135a8](https://github.com/Surfline/wavetrak-services/commit/d5135a8))
