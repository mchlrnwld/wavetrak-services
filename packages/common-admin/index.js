/* eslint-disable */
'use strict';

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./cjs/@surfline/admin-common.min.js');
} else {
  module.exports = require('./cjs/@surfline/admin-common.js');
}
