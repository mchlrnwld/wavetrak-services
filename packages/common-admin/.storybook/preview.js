import '@surfline/quiver-themes/stylesheets/crossbrand.css';
import '../assets/styles.css';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
};

