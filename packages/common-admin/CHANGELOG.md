# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [2.0.3](https://github.com/Surfline/wavetrak-services/compare/@surfline/admin-common@2.0.2...@surfline/admin-common@2.0.3) (2021-12-07)

**Note:** Version bump only for package @surfline/admin-common





### 2.0.2 (2021-10-21)

**Note:** Version bump only for package @surfline/admin-common





## <small>2.0.1 (2021-10-19)</small>

* chore(common-admin): migrate surfline-admin/modules/common to /packages/common-admin ([aa43e0d](https://github.com/Surfline/wavetrak-services/commit/aa43e0d))





# Admin Common Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## 2.0.0

- Update config to use APP_ENV instead of NODE_ENV
## 1.1.0

- Update quiver themes and other dependencies

## 1.0.4

- Updated Error parameter in fetch

## 1.0.3

- Added amendments to weatherStations CMS
## 1.0.2

- Added weatherStations CMS - this is incorrect, use 1.0.3 for corrections.
## 1.0.1

- Upgrade dependencies

## 1.0.0

- Upgrade to React 16
- Upgrade Storybook to latest version
- Upgrade babel to latest
- Use internal babel/eslint/prettier tools
- Add Typescript typings
- Make library tree-shaking friendly
