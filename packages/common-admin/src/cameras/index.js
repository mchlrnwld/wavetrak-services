import {
  fetchCameraOfTheMoment,
  fetchCameras,
  fetchCamera,
  fetchDownMessages,
  createCamera,
  editCamera,
  publishCameraOfTheMoment,
  removeCameraRecord,
  createCamEmbedConfig,
} from './cameras';

export default {
  fetchCameraOfTheMoment,
  fetchCameras,
  fetchCamera,
  fetchDownMessages,
  createCamera,
  editCamera,
  publishCameraOfTheMoment,
  removeCameraRecord,
  createCamEmbedConfig,
};
