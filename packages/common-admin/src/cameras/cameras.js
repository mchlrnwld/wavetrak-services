import fetch from '../utils/fetch';

export const fetchCameras = () =>
  fetch('cameras/?orderBy=title', {
    method: 'GET',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
  });

export const fetchCameraOfTheMoment = () =>
  fetch('cameras/cotm', {
    method: 'GET',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
  });

export const publishCameraOfTheMoment = (cameraId) =>
  fetch(`cameras/cotm/${cameraId}`, {
    method: 'PUT',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
  });

export const removeCameraRecord = (cameraId) =>
  fetch(`cameras/${cameraId}`, {
    method: 'DELETE',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
  });

export const fetchCamera = (cameraId) => fetch(`cameras/${cameraId}`);

export const fetchDownMessages = () => fetch('cameras/messages');

export const createCamera = (formData) =>
  fetch('cameras/', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify(formData),
  });

export const editCamera = (formData, cameraId) =>
  fetch(`cameras/${cameraId}/UpdateCamera`, {
    method: 'PUT',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify(formData),
  });

export const createCamEmbedConfig = (config) =>
  fetch('cameras/embed/configs', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify(config),
  });
