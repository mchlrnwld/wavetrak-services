import React from 'react';
import TextButton from '.';

// This default export determines where your story goes in the story list
export default {
  title: 'TextButton',
  component: TextButton,
  argTypes: {
    style: {
      description: 'The custom styles for the component',
      control: 'object',
    },
    type: { description: 'A button type', type: 'text' },
  },
};

const Template = (args) => <TextButton {...args} />;

export const Default = Template.bind({});

Default.args = {
  style: { width: 200 },
  children: 'Text Button',
  loading: false,
};

export const active = Template.bind({});

active.args = {
  style: { width: 250 },
  children: 'Warning Button',
  type: 'active',
  loading: false,
};
