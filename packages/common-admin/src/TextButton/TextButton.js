import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@surfline/quiver-react';

const styles = {
  height: 75,
  fontSize: 14,
  fontWeight: 600,
  border: 'none',
  backgroundColor: 'transparent',
  width: 155,
  color: '#52A9F9',
  active: {
    backgroundColor: '#EA686D',
    color: 'white',
  },
};

const TextButton = ({ onClick, loading, children, type, style }) => (
  <Button onClick={onClick} loading={loading} style={{ ...styles, ...styles[type], ...style }}>
    {children}
  </Button>
);

TextButton.propTypes = {
  onClick: PropTypes.func,
  loading: PropTypes.bool,
  style: PropTypes.shape({ width: PropTypes.number }),
  type: PropTypes.string,
  children: PropTypes.node,
};

TextButton.defaultProps = {
  onClick: () => {},
  loading: false,
  style: {},
  type: '',
  children: null,
};

export default TextButton;
