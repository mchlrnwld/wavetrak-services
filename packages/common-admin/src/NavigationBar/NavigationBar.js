import React from 'react';
import crossbrandLogo from '@surfline/quiver-themes/assets/logos/crossbrand.svg';

const Logo = () => <img src={crossbrandLogo} alt="Surfline Admin" />;
const links = [
  { display: 'Users', href: '/users/' },
  { display: 'Spots', href: '/spots/' },
  { display: 'POI', href: '/poi/' },
  { display: 'Reports', href: '/reports/' },
  { display: 'Cameras', href: '/cameras/' },
  { display: 'Forecasts', href: '/forecasts/' },
  { display: 'Promotions', href: '/promotions/' },
  { display: 'Sessions', href: '/sessions/' },
  { display: 'Weather Stations', href: '/weather-stations/' },
];

const Header = () => (
  <header className="admin-nav" style={{ display: 'flex', alignItems: 'center' }}>
    <Logo />
    {links.map((link) => (
      <a key={link.display} href={link.href}>
        {link.display}
      </a>
    ))}
  </header>
);

export default Header;
