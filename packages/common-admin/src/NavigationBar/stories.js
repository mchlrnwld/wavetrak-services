import React from 'react';
import NavigationBar from '.';

// This default export determines where your story goes in the story list
export default {
  title: 'NavigationBar',
  component: NavigationBar,
};

const Template = (args) => <NavigationBar {...args} />;

export const Default = Template.bind({});

Default.args = {};
