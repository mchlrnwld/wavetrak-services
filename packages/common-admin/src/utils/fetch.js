import fetch from 'isomorphic-fetch';
import Cookies from 'js-cookie';
import config from '../config';

const canUseDOM = !!(
  typeof window !== 'undefined' &&
  window.document &&
  window.document.createElement
);

const authenticate = (options = {}) => {
  let accessToken;
  let authenticatedOptions = options;
  if (canUseDOM) {
    accessToken = Cookies.get('access_token');
    authenticatedOptions = { headers: {}, ...options };
    authenticatedOptions.headers.Authorization =
      // eslint-disable-line
      `Bearer ${accessToken}`;
  }

  return authenticatedOptions;
};

/**
 * @async
 * @function fetchWithAuth
 * @template R
 * @param {string} path
 * @param {RequestInit} options
 * @returns {Promise<R>}
 * @throws {Error & { status: number }}
 */
const fetchWithAuth = async (path, options) => {
  const response = await fetch(`${config.adminServicesEndpoint}/${path}`, authenticate(options));
  const contentType = response.headers.get('Content-Type');
  const body =
    contentType && contentType.indexOf('json') > -1 ? await response.json() : await response.text();
  if (response.status > 200) {
    const error = new Error(body?.message ?? JSON.stringify(body));
    error.status = response.status;
    error.body = body;
    throw error;
  }
  return body;
};

export default fetchWithAuth;
