import { intersection as _intersection } from 'lodash';
import fetch from './fetch';
import { DEVELOPING } from './dev';

const DISABLE_AUTH = process.env.DISABLE_AUTH === 'true';

const validateToken = (token) =>
  fetch('auth/authorize', {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
      accept: 'application/json',
      'content-type': 'application/json',
    },
  });

const secureHandler =
  (scopes = ['super_admin']) =>
  async (req, res, next) => {
    if (DEVELOPING || DISABLE_AUTH) return next();
    // the downstream express app will need to have cookie parser enabled.
    const accessToken = req.cookies ? req.cookies.access_token : null;
    if (!accessToken) {
      return res.status(401).send('You are not authorized to view the resource');
    }

    try {
      const valid = await validateToken(accessToken);
      if (valid && valid.scopes) {
        const applied = _intersection(valid.scopes, scopes);
        console.log(`The following scopes are valid ${applied}`);
        if (applied.length > 0) return next();
      }
    } catch (err) {
      console.log(err);
    }

    return res.status(401).send('The provided credentials are not valid');
  };

export default secureHandler;
