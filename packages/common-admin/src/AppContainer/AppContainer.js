import React from 'react';
import PropTypes from 'prop-types';
import NavigationBar from '../NavigationBar';

const App = ({ children }) => (
  <div>
    <NavigationBar />
    <div className="container">{children || <p>You are not logged in.</p>}</div>
  </div>
);

App.propTypes = {
  children: PropTypes.node,
};

App.defaultProps = {
  children: null,
};

export default App;
