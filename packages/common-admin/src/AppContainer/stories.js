import React from 'react';
import AppContainer from '.';

// This default export determines where your story goes in the story list
export default {
  title: 'AppContainer',
  component: AppContainer,
};

const Template = (args) => (
  <AppContainer {...args}>
    <div>
      <h1>App Container</h1>
      <div>Some content</div>
    </div>
  </AppContainer>
);

export const Default = Template.bind({});

Default.args = {};
