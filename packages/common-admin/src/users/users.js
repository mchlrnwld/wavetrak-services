import fetch from '../utils/fetch';

export const fetchUserDetails = (userId) => fetch(`entitlements/${userId}/entitlements`);

export const createUserAccount = (userId) =>
  fetch('subscriptions/', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify({ userId }),
  });

export const removeUserEntitlement = (userId, entitlementId) =>
  fetch(`entitlements/${userId}/entitlements/${entitlementId}`, { method: 'DELETE' });

export const addUserEntitlement = (userId, entitlementId) =>
  fetch(`entitlements/${userId}/entitlements`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify({ entitlementId }),
  });

export const deleteSubscription = (userId, subscriptionId) =>
  fetch(`subscriptions/user/${userId}/subscription/${subscriptionId}`, {
    method: 'DELETE',
    headers: {
      accept: 'application/json',
      credentials: 'same-origin',
    },
  });

export const addVipSubscription = (userId, entitlementName) =>
  fetch('subscriptions/vip', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify({ userId, entitlementName }),
  });

export const findUsers = (query, version) => fetch(`users/?search=${query}&version=${version}`);
