import {
  fetchStations,
  fetchStation,
  createStation,
  editStation,
  removeStationRecord,
} from './weatherStations';

export default {
  fetchStations,
  fetchStation,
  createStation,
  editStation,
  removeStationRecord,
};
