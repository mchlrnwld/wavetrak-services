import fetch from '../utils/fetch';

export const fetchStations = () =>
  fetch('weather-stations/', {
    method: 'GET',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
  });

export const removeStationRecord = (stationId) =>
  fetch(`weather-stations/${stationId}`, {
    method: 'DELETE',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
  });

export const fetchStation = (stationId) => {
  return fetch(`weather-stations/${stationId}`);
};

export const createStation = (formData) =>
  fetch('weather-stations/', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify(formData),
  });

export const editStation = (formData, stationId) =>
  fetch(`weather-stations/${stationId}`, {
    method: 'PATCH',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify(formData),
  });
