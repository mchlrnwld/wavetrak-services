import { fetchSpotDetails, fetchSpots, addSpot, updateSpot, deleteSpot, findSpots } from './spots';

import {
  findSubregions,
  fetchSubregionDetails,
  fetchSubregions,
  fetchSpotsBySubregion,
  createSubregion,
  updateSubregion,
  deleteSubregionBasin,
} from './subregions';

export default {
  fetchSpotDetails,
  fetchSpots,
  addSpot,
  updateSpot,
  deleteSpot,
  findSpots,
  findSubregions,
  fetchSubregionDetails,
  fetchSubregions,
  fetchSpotsBySubregion,
  createSubregion,
  updateSubregion,
  deleteSubregionBasin,
};
