import qs from 'qs';
import fetch from '../utils/fetch';

export const fetchSpotDetails = (spotId) => fetch(`spots/${spotId}`);

export const fetchSpots = (queryObj) => fetch(`spots/?${qs.stringify(queryObj)}`);

export const addSpot = (spot) =>
  fetch('spots/', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify(spot),
  });

export const updateSpot = (spotId, spot) =>
  fetch(`spots/${spotId}`, {
    method: 'PUT',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify(spot),
  });

export const deleteSpot = (spotId) =>
  fetch(`spots/${spotId}`, {
    method: 'DELETE',
    headers: {
      credentials: 'same-origin',
    },
  });

/**
 * Given a query string will return a list of relevant search results
 *
 * @param query the query string to search for
 */
export const findSpots = (query) => fetch(`spots/?search=${query}`);
