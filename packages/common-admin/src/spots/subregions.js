import fetch from '../utils/fetch';
/**
 * Returns a list of relevent search results given a query string
 *
 * @param query the query parameter to search for
 */
export const findSubregions = (query) => fetch(`subregions/?search=${query}`);

export const fetchSubregionDetails = (subregionId) => fetch(`subregions/${subregionId}`);

export const fetchSubregions = () => fetch('subregions/');

export const fetchSpotsBySubregion = (subregionId) => fetch(`subregions/${subregionId}/spots`);

export const createSubregion = (subregion) =>
  fetch('subregions/', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify(subregion),
  });

export const deleteSubregionBasin = (subregion, basin) =>
  fetch(`subregions/${subregion._id}/basin`, {
    method: 'DELETE',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify({ basin }),
  });

export const updateSubregion = (subregion) =>
  fetch(`subregions/${subregion._id}`, {
    method: 'PUT',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify(subregion),
  });
