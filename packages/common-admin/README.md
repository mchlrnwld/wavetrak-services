# `@surfline/admin-common`

Contains code that is common or reused throughout the various Surfline admin tools.

## Developing

It is easiest to develop common functionality in a consuming code base such as the `reports` module.

To do this use `npm link` ([Documentation](https://docs.npmjs.com/cli/link)).

Run link in the project root of the consuming project relative to the target
package. Eg

```
$ pwd
wavetrak-services/services/search-admin-web
$ npm link ../packages/common-admin
~/.nvm/versions/node/v10.24./lib/node_modules/@surfline/admin-common -> ~/Workspace/wavetrak-services/services/search-admin-web
~/Workspace/surfline-admin/modules/reports/node_modules/@surfline/admin-common -> ~/.nvm/versions/node/v10.24./lib/node_modules/@surfline/admin-common -> ~/Workspace/wavetrak-services/services/search-admin-web
```

This will link `@surfline/admin-common` into the `@wavetrak-services/services/search-admin-web`  module. Changes made to
`packages/common-admin` will automatically be reflected into the `@wavetrak-services/services/search-admin-web`  module.

_Remember to run `npm run build` to see your changes._

### UI Components

Use storybook to mock the components.

### Fetch and API Calls

There is wrapped version of `fetch` that should be used to make requests
against any admin tool endpoints. This handle's authentication and error
handling for you.

## Publishing NPM package

Within the `/packages/` directory we publish NPM packages using Lerna.

A step by step guide for publishing NPM packages in the monorepo can be found [here](../../README.md#npm-packages).
