# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.2] - 2021-01-14
### Fixed
- `WTSlackHook._get_slack_message` returns the correct URL for the Airflow log.

## [0.0.1] - 2021-01-07
### Added
- Added 3 helper functions that can be used in Airflow DAGs:
    1. `docker_image`:  Returns the full path for a docker image based on the image name, `ECR_HOST` and `ENVIRONMENT`.
    2. `create_pager_duty_failure_callback`: Creates an on_failure_callback for a task, which either triggers a PagerDuty alert or logs the failure, depending on whether a `pager_duty_off` flag is set in AWS Parameter Store. This function imports the `WTPagerDutyHook` class, which lives in `jobs/airflow/dag_helpers`. The `WTPagerDutyHook` is made available globally on the Airflow instances via `dags/plugins`. NOTE: This pattern for Airflow callback functions is deprecated and all future plugins and hooks will be added to `airflow_helpers/plugins`.
    3. `create_slack_failure_callback`:  Creates an on_failure_callback for a task, which either triggers a Slack alert or logs the failure, depending on whether a `slack_off` flag is set in AWS Parameter Store. This function imports the `WTSlackHook` class from `airflow_helpers/plugins/wt_slack.py`.
