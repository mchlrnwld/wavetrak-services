import setuptools

with open('README.md', 'r') as f:
    long_description = f.read()

setuptools.setup(
    name='wavetrak-airflow-helpers',
    version='0.0.2',
    author='Wavetrak',
    author_email='nerdery@surfline.com',
    description=(
        'A library of useful packages used in Wavetrak Airflow DAGS.'
    ),
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://github.com/Surfline/wavetrak-services/common/airflow-helpers',
    packages=setuptools.find_packages(),
    package_data={'': ['py.typed']},
    install_requires=[
        # 'airflow==1.7.1.3',
        # NOTE: This version of Airflow is required to use this package,
        # but is no longer available via pip.
        # It must be installed manually via an archive or cache.
        'pypd==1.1.0',
        'slackclient==1.3.2',
    ],
    python_requires='==2.7.*',
    # NOTE: The current Airflow version only works with Python 2.7.*
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
    ],
)
