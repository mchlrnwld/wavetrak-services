import logging
from distutils.util import strtobool

import boto3

from airflow_helpers.plugins.wt_slack import WTSlackHook


def create_slack_failure_callback(
    environment=None,
    channel=None,
    job_name=None,
    token_name='slack_api_token',
    aws_region='us-west-1',
):
    """
    Creates an on_failure_callback for a task, which either triggers a
    Slack alert or logs the failure, depending on whether a
    `slack_off` flag is set in AWS Parameter Store.
    Args:
        environment (str): Execution environment.
        channel (str): Slack channel to send the notification to.
        job_name (str): Airflow job name
        token_name (str): The name of the Slack API token to use.
        aws_region (str): The AWS region to use for parameter store.
    Returns:
        A callback function that either triggers a Slack alert or logs the
        failure, depending on whether a `slack_off` flag is set in AWS
        Parameter Store.
    """
    if job_name and environment:
        ssm = boto3.client('ssm', region_name=aws_region)
        param_name = '/{environment}/jobs/{job_name}/slack_off'.format(
            environment=environment, job_name=job_name
        )
        try:
            ssm_response = ssm.get_parameter(Name=param_name)

            # True `strtobool` values are y, yes, t, true, on and 1
            # False `strtobool` values are n, no, f, false, off and 0
            if ssm_response and strtobool(ssm_response['Parameter']['Value']):
                return lambda context: logging.info(
                    (
                        'Skipping Slack notification due to '
                        '{param_name} flag set. Slack event: '
                        '{pd_event}'
                    ).format(
                        param_name=param_name,
                        pd_event=WTSlackHook(
                            context=context
                        ).get_slack_event(),
                    )
                )
        except (ssm.exceptions.ParameterNotFound, KeyError, ValueError):
            pass

    return lambda context: WTSlackHook(
        context=context, token_name=token_name, channel=channel
    ).execute()
