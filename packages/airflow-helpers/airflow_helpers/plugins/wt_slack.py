"""
WTSlack Airflow Plugin
"""
import logging

from slackclient import SlackClient

from airflow.exceptions import AirflowException
from airflow.hooks.base_hook import BaseHook


class WTSlackHook(BaseHook):
    """
    Hook to create Airflow Slack events
    and send notifications
    Args:
        channel (str): Slack Channel to send the notification to.
        token_name (str): Name of the Slack API token to use from Airflow.
        context (dict): Task instance dictionary containing metadata
                        about the task.
    Attributes:
        token (str): Slack API token.
        channel (str): The Slack channel to send the notification to.
        hostname (str): The hostname of the Airflow instance.
        task_id (str): The name of the task.
        dag_id (str): The name of the DAG associated with the task.
        execution_date (str): The execution date of the task.
        state (str): The state of the task.
    """

    def __init__(self, context, channel=None, token_name='slack_api_token'):

        self.token = self._get_token(token_name)
        self.channel = channel
        ti = context['task_instance']
        self.hostname = ti.hostname
        self.task_id = ti.task_id
        self.dag_id = ti.dag_id
        self.execution_date = ti.execution_date
        self.state = ti.state

    def _get_token(self, token_name):
        """
        Returns a Slack API token.
        Args:
            token_name (str): The Slack API token name stored in Airflow.
            `token_name` references a connection defined in
            [Airflow > Admin > Connections] whose `extra` field contains JSON
            containing a `token` field.
            Example:
                {"token": ""}
        Returns:
            A Slack API token (str).
        Raises:
            AirflowException: No Slack API token available.
        """
        conn = self.get_connection(token_name)
        extra = conn.extra_dejson
        token = extra.get('token', None)

        if not token:
            message = 'No Slack API token available.'
            logging.error(message)
            raise AirflowException(message)

        return token

    def _get_slack_message(self):
        """
        Returns a slack message
        used in the notification.
        Returns:
            A Slack message (dict).
        """
        log_url = (
            "http://{}.aws.surfline.com:8080/"
            "admin/airflow/log?"
            "task_id={}&"
            "dag_id={}&"
            "execution_date={}"
        ).format(
            self.hostname,
            self.task_id,
            self.dag_id,
            self.execution_date.isoformat(),
        )
        messages = [
            'An error has occurred:',
            '*DAG:*\n{}'.format(self.dag_id),
            '*Task:*\n{}'.format(self.task_id),
            '*State:*\n{}'.format(self.state),
            '*Execution Date:*\n{}'.format(self.execution_date),
            '*Hostname:*\n{}'.format(self.hostname),
            '*Logs:*\n{}'.format(log_url),
        ]
        return [
            {'type': 'section', 'text': {'type': 'mrkdwn', 'text': message}}
            for message in messages
        ]

    def execute(self):
        """
        Executes the Airflow hook.
        Raises:
            AirflowException: Slack API call failed.
        """
        slack_message = self._get_slack_message()
        logging.info('Creating Slack event: {}'.format(slack_message))
        slack_client = SlackClient(token=self.token)
        response = slack_client.api_call(
            'chat.postMessage', channel=self.channel, blocks=slack_message
        )
        if 'error' in response:
            e = response['error']
            message = 'Slack API call failed: '.format(e)
            logging.error(message)
            raise AirflowException(message)

        logging.info('Slack event sent: {}'.format(slack_message))
