# Airflow Helpers

A library of helper functions used in Wavetrak Airflow DAGs.

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Setup](#setup)
- [Use](#use)
- [Helpers](#helpers)
- [Development](#development)
  - [Setup Environment](#setup-environment)
  - [Install Local Package](#install-local-package)
  - [Deploy](#deploy)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Setup

See https://wavetrak.atlassian.net/wiki/spaces/WT/pages/73334794/PyPI+Package+Server for instructions on getting setup to download/upload packages to Artifactory.

Sample `$HOME/.pypirc` file:

```
[distutils]
index-servers =
  dev
  prod

[dev]
repository: https://surfline.jfrog.io/surfline/api/pypi/pypi-local-dev
username:
password:

[prod]
repository: https://surfline.jfrog.io/surfline/api/pypi/pypi-local
username:
password:
```

## Use

Install with `pip`, but before doing so,
run the following command:
`aws s3 cp s3://sl-artifactory-prod/config/.pip/pip.conf ~/.pip/pip.conf`
```

Then install with `pip`.

```sh
$ pip install wavetrak-Airflow-helpers
```

## Helpers

## Development

### Setup Environment

Use [conda](https://docs.conda.io/en/latest/) to setup a development environment.

```sh
$ conda env create -f environment.yml
$ conda activate wavetrak-airflow-helpers
```

### Install Local Package

To test changes in a local application install as an "editable" package.

```sh
$ pip install -e /path/to/airflow-helpers/
```

### Deploy

Deploy to `dev` repository to test for local development.

```sh
$ ENV=dev make deploy
```
