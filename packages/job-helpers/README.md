# Job Helpers

A library of useful packages used in Wavetrak Airflow job tasks.

## Table of Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Setup](#setup)
- [Use](#use)
- [Helpers](#helpers)
  - [`check_model_ready`](#check_model_ready)
  - [`file_transfer`](#file_transfer)
  - [`job_secrets_helper`](#job_secrets_helper)
  - [`slack_helper`](#slack_helper)
- [Development](#development)
  - [Setup Environment](#setup-environment)
  - [Install Local Package](#install-local-package)
  - [Deploy](#deploy)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Setup

See https://wavetrak.atlassian.net/wiki/spaces/WT/pages/73334794/PyPI+Package+Server for instructions on getting setup to download/upload packages to Artifactory.

Sample `$HOME/.pypirc` file:

```
[distutils]
index-servers =
  dev
  prod

[dev]
repository: https://surfline.jfrog.io/surfline/api/pypi/pypi-local-dev
username:
password:

[prod]
repository: https://surfline.jfrog.io/surfline/api/pypi/pypi-local
username:
password:
```

## Use

Install with `pip`, but before doing so,
run the following command:
`aws s3 cp s3://sl-artifactory-prod/config/.pip/pip.conf ~/.pip/pip.conf`
```

Then install with `pip`.

```sh
$ pip install wavetrak-job-helpers
```

## Helpers

### `check_model_ready`

TODO: Add documentation for `check_model_ready`

### `file_transfer`

#### `async_get_file_from_http`

Asynchronously get a file using HTTP. This returns the contents of the file and raises an `HttpError` if an error occurs getting the file.

##### Arguments

- `url`: The url from which to get http contents.

##### Returns

A list of http contents from the given URL if present, otherwise the empty
list is returned.

##### Raises

`HttpError`

##### Example

```Python
from file_transfer import async_get_file_from_http, HttpError

url = 'https://nomads.ncep.noaa.gov/pub/data/nccf/com/wave/prod/index.txt'

try:
    file_contents = await async_get_file_from_http(url)
except HttpError as e:
    print(f'Error retrieving {http_url}: {e}')
```

#### `async_stream_files_from_http_to_s3`

Asynchronously streams files from an HTTP source to S3 for a list of URLs

##### Arguments

- `urls`: Sequence of tuples where each tuple contains the following:
  - `src_url`: An HTTP URL referencing a file to be streamed
  - `dest_url`: An S3 URL referencing the S3 location to stream to
  - `byte_ranges` (optional): Sequence of tuples of byte start/stop positions
    - Examples
      - `[(0, 1024)]`
      - `[(0, 1024), (1024, 2048)]`
      - `[(0, 1024), (1024, 2048), (2048, None)]`
  - `urls` Example

    ```Python
    [('https://nomads.ncep.noaa.gov/wc_4m.t06z.f117.grib2',
      's3://surfline-science-s3-dev/noaa/ww3/wc_4m.t06z.f117.grib2',
      [(0, 1024)]),
     ('https://nomads.ncep.noaa.gov/wc_4m.t06z.f118.grib2',
      's3://surfline-science-s3-dev/noaa/ww3/wc_4m.t06z.f118.grib2',
      [(0, 1024), (1024, 2048)]),
     ('https://nomads.ncep.noaa.gov/wc_4m.t06z.f119.grib2',
      's3://surfline-science-s3-dev/noaa/ww3/wc_4m.t06z.f119.grib2',
      [])]
    ```

- `max_concurrency`: (optional) The maximum number of concurrent streams.
    Default: `10`
- `chunk_size`: (optional) The number of downloaded bytes to read into
    memory at once. Default: `1048576` (1 MB)

##### Returns

`None`

##### Raises

`StreamingError`

##### Example

```Python
from file_transfer import async_stream_files_from_http_to_s3, StreamingError

urls = [('https://nomads.ncep.noaa.gov/wc_4m.t06z.f117.grib2',
         's3://surfline-science-s3-dev/noaa/ww3/wc_4m.t06z.f117.grib2',
         [(0, 1024)]),
        ('https://nomads.ncep.noaa.gov/wc_4m.t06z.f118.grib2',
         's3://surfline-science-s3-dev/noaa/ww3/wc_4m.t06z.f118.grib2',
         [(0, 1024), (1024, 2048)]),
        ('https://nomads.ncep.noaa.gov/wc_4m.t06z.f119.grib2',
         's3://surfline-science-s3-dev/noaa/ww3/wc_4m.t06z.f119.grib2',
         [])]

try:
    await async_stream_files_from_http_to_s3(urls=urls)
except StreamingError as e:
    print(f'Error retrieving URLs {urls}: {e}')
```

### `job_secrets_helper`

Get value of a secret from the environment or AWS Secrets Manager

If the secret exists in the environment, use it. Otherwise get the secret
value from AWS Secret Manager. If the secret doesn't exist there either,
raise exception.

```Python
from job_secrets_helper import get_secret, SecretError

# Get secret value
AIRTABLE_API_KEY = get_secret(
    'dev/jobs/check-and-update-camera-time-settings/', 'AIRTABLE_API_KEY')

# Non-existent secret
try:
    AIRTABLE_API_KEY = get_secret(
        'dev/jobs/check-and-update-camera-time-settings/', 'AIRTABLE_API_KEY')
except SecretError:
    AIRTABLE_API_KEY = None
```

### `slack_helper`

#### `send_invalid_points_of_interest_notification`

Sends a Slack notification for points of interest with invalid grid points, grouped by grid.

```Python
if SLACK_API_TOKEN and SLACK_CHANNEL and invalid_points_of_interest:
    send_invalid_points_of_interest_notification(
        'process-forecast-platform-noaa-gfs',
        {'0p25': [id for id in invalid_points_of_interest]},
        SLACK_API_TOKEN,
        SLACK_CHANNEL,
        logger.error,
    )
```

## Development

### Setup Environment

Use [conda](https://docs.conda.io/en/latest/) to setup a development environment.

```sh
$ conda env create -f environment.yml
$ conda activate wavetrak-job-helpers
```

### Install Local Package

To test changes in a local application install as an "editable" package.

```sh
$ pip install -e /path/to/job-helpers/
```

### Deploy

Deploy to `dev` repository to test for local development.

```sh
$ ENV=dev make deploy
```
