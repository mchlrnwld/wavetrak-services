import json

from check_model_ready.message import Message

GFS_PREFIX = 'gfs'
LOTUS_PREFIX = 'lotus-ww3'


def create_record_json(key):
    return {'s3': {'object': {'key': key}}}


def test_message_parses_relevant_object_keys():
    keys = [
        'gfs/2019080700/gfs.t00z.pgrb2.0p25.f000',
        (
            'proteus/ww3/fields/GLOB_30m/2019080700/'
            'ww3_msw_GLOB_30m.2019080700.000.grib2'
        ),
        'gfs/2019080700/gfs.t00z.pgrb2.0p25.f001',
    ]
    message_json = {
        'MessageId': 'message id',
        'ReceiptHandle': 'receipt handle',
        'Body': json.dumps(
            {
                'Message': json.dumps(
                    {'Records': [create_record_json(key) for key in keys]}
                )
            }
        ),
    }
    message = Message((GFS_PREFIX,), message_json)
    assert message.id == 'message id'
    assert message.receipt_handle == 'receipt handle'
    assert message.object_keys == keys
    assert message.relevant_object_keys == [keys[0], keys[2]]


def test_message_parses_relevant_object_keys_for_multiple_prefixes():
    keys = [
        'gfs/2019080700/gfs.t00z.pgrb2.0p25.f000',
        (
            'proteus/ww3/fields/GLOB_30m/2019080700/'
            'ww3_msw_GLOB_30m.2019080700.000.grib2'
        ),
        'lotus-ww3/2019080700/ww3_lotus_US_E_3m.spot_290.050_041.900.nc',
    ]
    message_json = {
        'MessageId': 'message id',
        'ReceiptHandle': 'receipt handle',
        'Body': json.dumps(
            {
                'Message': json.dumps(
                    {'Records': [create_record_json(key) for key in keys]}
                )
            }
        ),
    }
    message = Message((GFS_PREFIX, LOTUS_PREFIX), message_json)
    assert message.id == 'message id'
    assert message.receipt_handle == 'receipt handle'
    assert message.object_keys == keys
    assert message.relevant_object_keys == [keys[0], keys[2]]


def test_message_handles_malformed_messages():
    message_blank_body_json = {
        'MessageId': 'blank body message id',
        'ReceiptHandle': 'blank body receipt handle',
        'Body': '',
    }
    message_blank_body = Message((GFS_PREFIX,), message_blank_body_json)
    assert message_blank_body.object_keys == []
    assert message_blank_body.relevant_object_keys == []

    message_no_message_json = {
        'MessageId': 'no message message id',
        'ReceiptHandle': 'no message receipt handle',
        'Body': json.dumps({}),
    }
    message_no_message = Message((GFS_PREFIX,), message_no_message_json)
    assert message_no_message.object_keys == []
    assert message_no_message.relevant_object_keys == []

    message_blank_message_json = {
        'MessageId': 'blank message message id',
        'ReceiptHandle': 'blank message receipt handle',
        'Body': json.dumps({'Message': ''}),
    }
    message_blank_message = Message((GFS_PREFIX,), message_blank_message_json)
    assert message_blank_message.object_keys == []
    assert message_blank_message.relevant_object_keys == []

    message_no_records_json = {
        'MessageId': 'no records message id',
        'ReceiptHandle': 'no records receipt handle',
        'Body': json.dumps({'Message': json.dumps({})}),
    }
    message_no_records = Message((GFS_PREFIX,), message_no_records_json)
    assert message_no_records.object_keys == []
    assert message_no_records.relevant_object_keys == []

    keys = [
        (
            'proteus/ww3/fields/GLOB_30m/2019080700/'
            'ww3_msw_GLOB_30m.2019080700.000.grib2'
        )
    ]
    message_no_relevant_keys_json = {
        'MessageId': 'no records message id',
        'ReceiptHandle': 'no records receipt handle',
        'Body': json.dumps(
            {
                'Message': json.dumps(
                    {'Records': [create_record_json(key) for key in keys]}
                )
            }
        ),
    }
    message_no_relevant_keys = Message(
        (GFS_PREFIX,), message_no_relevant_keys_json
    )
    assert message_no_relevant_keys.object_keys == keys
    assert message_no_relevant_keys.relevant_object_keys == []
