import json
from typing import Any, Dict

import requests


def model_runs_url(sds_host: str) -> str:
    return f'{sds_host}/model_runs'


def create_model_run_json(
    agency: str, model: str, run: int, status: str, run_type: str
) -> Dict[str, Any]:
    """
    Create a dict representing a model run's JSON payload.
    Args:
        agency: Agency of model run.
        model: Model of model run.
        run: Model run time YYYYMMDDHH.
        status: Status of 'ONLINE', 'PENDING', or 'OFFLINE'
        run_type: Run type of POINT_OF_INTEREST, FULL_GRID, or LEGACY.
    Returns:
        Dict with model run meta data.
    """
    return {
        'agency': agency,
        'model': model,
        'run': run,
        'status': status,
        'type': run_type,
    }


def check_model_run_already_exists(
    agency: str, model: str, run: int, run_type: str, sds_host: str
) -> bool:
    """
    Checks if a PENDING or ONLINE model run already exists.

    Args:
        agency: Agency of model run.
        model: Model of model run.
        run: Model run (YYYYMMDDHH) to check.
        sds_host: URL for Science Data Service host.
        run_type: Run type to check.

    Returns:
        True if the object exists, otherwise False
    """
    response = requests.get(
        model_runs_url(sds_host),
        params={
            'agency': agency,
            'model': model,
            'run': str(run),
            'status': 'PENDING,ONLINE,OFFLINE',
            'types': run_type,
        },
    )
    response.raise_for_status()
    model_runs = response.json()
    return len(model_runs) > 0


def set_pending_model_run(
    agency: str, model: str, run: int, run_type: str, sds_host: str
):
    """
    Creates a PENDING model run or sets existing model run status to
    PENDING to indicate that the job has already been started.

    Args:
        agency: Agency of model run.
        model: Model of model run.
        run: Model run (YYYYMMDDHH) to create or set.
        sds_host: URL for Science Data Service host.
        run_type: Run types, e.g. POINT_OF_INTEREST or FULL_GRID.
    """
    set_model_runs_response = requests.put(
        model_runs_url(sds_host),
        headers={'Content-Type': 'application/json'},
        data=json.dumps(
            [create_model_run_json(agency, model, run, 'PENDING', run_type)]
        ),
    )
    set_model_runs_response.raise_for_status()
