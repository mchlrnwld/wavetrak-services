import unittest.mock as mock

import boto3  # type: ignore
from moto import mock_s3  # type: ignore

from check_model_ready.s3 import get_latest_run, get_object_keys

SCIENCE_BUCKET = 'surfline-science-test'
SCIENCE_KEY_PREFIX = 'gfs'
AGENCY = 'NOAA'
MODEL = 'GFS'
RUN_TYPE = 'FULL_GRID'


# NOTE: moto does not provide mocking for paginators,
# so a custom mock was created.
class MockPaginator:
    def paginate(self, Bucket: str, Prefix: str, StartAfter: str):
        if Prefix != f'{SCIENCE_KEY_PREFIX}':
            return []
        else:
            return [
                {
                    'Contents': [
                        {
                            'Key': (
                                f'{SCIENCE_KEY_PREFIX}/2019050206/'
                                f'gfs.t06z.pgrb2.0p25.f001'
                            )
                        },
                        {
                            'Key': (
                                f'{SCIENCE_KEY_PREFIX}/2019091118/'
                                f'gfs.t18z.pgrb2.0p25.f001'
                            )
                        },
                        {
                            'Key': (
                                f'{SCIENCE_KEY_PREFIX}/2020022306/'
                                f'gfs.t06z.pgrb2.0p25.f001'
                            )
                        },
                    ]
                }
            ]


class MockS3:
    def get_paginator(self):
        return MockPaginator()


def test_get_object_keys():
    with mock_s3():
        s3_client = boto3.client('s3')
        s3_client.create_bucket(Bucket=SCIENCE_BUCKET)
        s3_client.put_object(
            Bucket=SCIENCE_BUCKET, Key='root_file', Body='test'.encode('utf-8')
        )
        for i in range(2000):
            s3_client.put_object(
                Bucket=SCIENCE_BUCKET,
                Key=f'{SCIENCE_KEY_PREFIX}/file_{i:04}',
                Body='test'.encode('utf-8'),
            )
        assert len(list(get_object_keys(SCIENCE_BUCKET))) == 2001
        assert (
            len(list(get_object_keys(SCIENCE_BUCKET, SCIENCE_KEY_PREFIX)))
            == 2000
        )
        assert (
            len(
                list(
                    get_object_keys(
                        SCIENCE_BUCKET,
                        SCIENCE_KEY_PREFIX,
                        f'{SCIENCE_KEY_PREFIX}/file_0999',
                    )
                )
            )
            == 1000
        )


def test_get_latest_run():
    with mock.patch('boto3.client', return_value=MockS3):
        # Test calling the function with the online_run parameter.
        assert (
            get_latest_run(SCIENCE_BUCKET, SCIENCE_KEY_PREFIX, 2019081200)
            == 2020022306
        )

        # Test calling the function without the online_run parameter.
        # Expect the same result as above.
        assert (
            get_latest_run(SCIENCE_BUCKET, SCIENCE_KEY_PREFIX, None)
            == 2020022306
        )

        # Test calling the function when the online_run parameter
        # is the same value as the latest model run available.
        # Expect the same online_run parameter to be returned.
        assert (
            get_latest_run(SCIENCE_BUCKET, SCIENCE_KEY_PREFIX, 2020022306)
            is None
        )
