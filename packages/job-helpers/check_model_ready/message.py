import json
from typing import Tuple


class Message:
    """
    A received SQS message representing an S3 upload event.

    Args:
        prefixes (str): Key prefixes for expected S3 objects in the message.
                        This will be used to filter relevant object keys from
                        the list of object keys.
        message (dict): JSON representation of a received SQS message.

    Attributes:
        id (str): Message ID.
        receipt_handle (str): Receipt handle is required to delete a message
                              from the queue.
        object_keys (list): List of object keys parsed from the message.
        relevant_object_keys (list): List of object keys filtered from
                                     object_keys which match the provided
                                     prefix.
        checked (int): Number of times the message has been marked as checked.
                       A message is considered done and ready to be deleted if
                       the checked attribute is equal to the number of
                       relevant_object_keys.
    """

    def __init__(self, prefixes: Tuple[str, ...], message: dict):
        self.id = message['MessageId']
        self.receipt_handle = message['ReceiptHandle']

        try:
            body = json.loads(message['Body'])
            records = json.loads(body['Message'])['Records']
        except Exception:
            records = []

        self.object_keys = [
            record['s3']['object']['key'] for record in records
        ]
        self.relevant_object_keys = [
            key for key in self.object_keys if key.startswith(prefixes)
        ]
        self.checked = 0

    def mark_checked(self):
        """
        Increments the checked attribute by 1.
        """
        self.checked += 1

    def is_done(self) -> int:
        """
        Checks if a message is considered done. This is determined by
        comparing the checked attribute with the number of
        relevant_object_keys.

        Returns:
            True if the message is done being checked and ready to be deleted.
        """
        return self.checked == len(self.relevant_object_keys)
