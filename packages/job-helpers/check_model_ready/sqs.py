import logging
from typing import Callable, Optional, Set, Tuple

import boto3  # type: ignore

from check_model_ready.message import Message

logger = logging.getLogger()


def get_queue_url(queue_name: str) -> str:
    """
    Gets an SQS queue URL from a name.

    Args:
        queue_name: Name of the SQS queue to get.

    Returns:
        Queue URL for the SQS queue. An exception is raised if queue is not
        found.
    """
    sqs_client = boto3.client('sqs')
    return sqs_client.get_queue_url(QueueName=queue_name)['QueueUrl']


def check_messages(
    bucket: str,
    message_prefixes: Tuple[str, ...],
    queue_name: str,
    check_func: Callable[[int], bool],
    model_run_from_key_func: Callable[[str], Optional[int]],
) -> Tuple[Optional[int], int]:
    """
    Checks messages (up to 10) from an SQS queue. Determines if a model run is
    ready for processing and deletes messages that are no longer needed.

    Args:
        bucket: S3 bucket to check for model run file objects.
        message_prefixes: Key prefixes for model run file objects. This is used
                          to match relevant object keys from SQS messages. Ex:
                          'noaa/gfs', 'jobs/completed'
                          prefix used to look up model run file objects in S3.
        queue_name: Name of SQS queue to check messages from.
        check_func: Function that checks if a model run is ready. Returns a
                    boolean.
        model_run_from_key_func: Function that gets the model run from a key.

    Returns:
        A pair of values.
        First value is a model run time that is ready for processing. None if
            no model run is ready from this batch of messages.
        Second value is the number of messages received. This can be used to
            determine if we should continue polling for messages.
    """
    queue_url = get_queue_url(queue_name)
    sqs_client = boto3.client('sqs')
    message_response = sqs_client.receive_message(
        QueueUrl=queue_url, MaxNumberOfMessages=10
    )

    messages = (
        [
            Message(message_prefixes, message)
            for message in message_response['Messages']
        ]
        if 'Messages' in message_response
        else []
    )

    checked_model_runs: Set[int] = set()
    model_run_ready: Optional[int] = None
    for message in messages:
        logger.info(f'Checking message object keys: {message.object_keys}')
        model_runs = [
            run
            for run in [
                model_run_from_key_func(key)
                for key in message.relevant_object_keys
            ]
        ]
        for model_run in model_runs:
            if model_run is None or model_run in checked_model_runs:
                message.mark_checked()
            elif model_run_ready is None:
                if check_func(model_run):
                    model_run_ready = model_run

                message.mark_checked()
                checked_model_runs.add(model_run)

    messages_to_delete = [
        {'Id': message.id, 'ReceiptHandle': message.receipt_handle}
        for message in messages
        if message.is_done()
    ]
    if messages_to_delete:
        sqs_client.delete_message_batch(
            QueueUrl=queue_url, Entries=messages_to_delete
        )

    return model_run_ready, len(messages)
