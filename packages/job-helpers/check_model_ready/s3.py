import re
from typing import Generator, Optional

import boto3  # type: ignore

RUN_REGEX = re.compile(r'\d{10}')


def get_object_keys(
    bucket: str,
    prefix: Optional[str] = None,
    start_after: Optional[str] = None,
) -> Generator[str, None, None]:
    s3_client = boto3.client('s3')
    paginator = s3_client.get_paginator('list_objects_v2')
    pages = paginator.paginate(
        Bucket=bucket, Prefix=(prefix or ''), StartAfter=(start_after or '')
    )

    for page in pages:
        if 'Contents' not in page:
            continue
        for item in page['Contents']:
            yield item['Key']


def get_latest_run(
    bucket: str, prefix: str, online_run: Optional[int]
) -> Optional[int]:
    """
    Get the latest run for a model in S3.

    Args:
        bucket: S3 bucket containing model run data files.
        prefix: S3 key prefix to filter for data files.
        online_run: Runs prior to this run will be ignored.

    Returns:
        Latest run available in S3 after the current online run. None if no
        such run found.
    """
    start_after = f'{prefix}/{online_run}' if online_run else prefix

    all_runs = set()
    for key in get_object_keys(bucket, prefix, start_after):
        run_match = RUN_REGEX.search(key)
        if not run_match:
            continue

        run = int(run_match.group(0))
        if not online_run or run > online_run:
            all_runs.add(run)

    return sorted(list(all_runs), reverse=True)[0] if all_runs else None
