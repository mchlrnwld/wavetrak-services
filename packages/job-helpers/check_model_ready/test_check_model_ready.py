import itertools
import json
import re
from typing import Any, Dict, List, Optional

import boto3  # type: ignore
import pytest  # type: ignore
import requests
from boto3_type_annotations.s3 import Client as s3_client  # type: ignore
from boto3_type_annotations.sqs import Client as sqs_client  # type: ignore
from moto import mock_s3, mock_sqs  # type: ignore

from check_model_ready import (
    check_model_run_already_exists,
    check_sqs_messages,
    set_pending_model_run,
)
from check_model_ready.sds import create_model_run_json

BUCKET = 'surfline-bucket'
SQS_QUEUE_NAME = 'surfline-gfs-queue'
SCIENCE_KEY_PREFIX = 'gfs'
WW3_ENSEMBLE_PREFIX = (
    'jobs/completed/process-forecast-platform-noaa-ww3-ensemble'
)
SDS_HOST = 'http://sdshost'
AGENCY = 'NOAA'
MODEL = 'GFS'
RUN = 2019050200
RUN_TYPE = 'POINT_OF_INTEREST'
GRIB_REGEX = re.compile(r'gfs\.t(00|06|12|18)z\.pgrb2\.0p25\.f\d{3}$')


@pytest.fixture(autouse=True)
def aws_variables(monkeypatch):
    monkeypatch.setenv('AWS_DEFAULT_REGION', 'us-west-1')


class MockResponse:
    def __init__(self, json: Dict[str, Any]):
        self._json = json

    def raise_for_status(self):
        pass

    def json(self):
        return self._json


@pytest.fixture
def mock_sds(monkeypatch):
    model_runs = []

    def mock_put_model_runs(*args, **kwargs):
        model_runs.append(
            create_model_run_json(AGENCY, MODEL, RUN, 'ONLINE', RUN_TYPE)
        )
        return MockResponse({})

    def mock_get_model_runs(*args, **kwargs):
        return MockResponse(model_runs)  # type: ignore

    monkeypatch.setattr(requests, 'get', mock_get_model_runs)
    monkeypatch.setattr(requests, 'put', mock_put_model_runs)


@pytest.fixture
def create_s3_bucket():
    with mock_s3():
        s3_client = boto3.client('s3')
        s3_client.create_bucket(Bucket=BUCKET)
        yield s3_client


@pytest.fixture
def create_sqs_queue():
    with mock_sqs():
        sqs_client = boto3.client('sqs')
        queue_url = sqs_client.create_queue(
            QueueName=SQS_QUEUE_NAME, Attributes={'VisibilityTimeout': '0'}
        )['QueueUrl']
        yield sqs_client, queue_url


def check(model_run: int) -> bool:
    if check_model_run_already_exists(AGENCY, MODEL, RUN, RUN_TYPE, SDS_HOST):
        return False

    expected_timestamps = list(itertools.chain(range(121), range(123, 385, 3)))
    s3_client = boto3.client('s3')
    model_objects_response = s3_client.list_objects_v2(
        Bucket=BUCKET, Prefix=f'{SCIENCE_KEY_PREFIX}/{model_run}'
    )
    if 'Contents' not in model_objects_response:
        return False
    model_timestamps = [
        int(object['Key'][-3:])
        for object in model_objects_response['Contents']
        if GRIB_REGEX.search(object['Key'])
    ]
    return model_timestamps == expected_timestamps


def model_run_from_key(key: str) -> Optional[int]:
    try:
        return int(key.replace(SCIENCE_KEY_PREFIX, '').split('/')[1])
    except ValueError:
        return None


def send_messages_to_queue(
    queue_url: str,
    sqs_client: sqs_client,
    timestamps: List[int],
    model_key_prefix: str = f'{SCIENCE_KEY_PREFIX}/{RUN}',
):
    for time in timestamps:
        sqs_client.send_message_batch(
            QueueUrl=queue_url,
            Entries=[
                {
                    'Id': 'mid0',
                    'MessageBody': create_message_body(
                        [
                            (
                                f'{model_key_prefix}/gfs.t00z.pgrb2.0p25.f'
                                f'{str(time).zfill(3)}'
                            )
                        ]
                    ),
                }
            ],
        )


def create_message_body(object_keys: list):
    return json.dumps(
        {
            'Message': json.dumps(
                {
                    'Records': [
                        {'s3': {'object': {'key': key}}} for key in object_keys
                    ]
                }
            )
        }
    )


def put_items_in_bucket(
    s3_client: s3_client,
    timestamps: list,
    model_key_prefix: str = f'{SCIENCE_KEY_PREFIX}/{RUN}',
):
    for time in timestamps:
        s3_client.put_object(
            Bucket=BUCKET,
            Key=(
                f'{model_key_prefix}/gfs.t00z.pgrb2.0p25.f'
                f'{str(time).zfill(3)}'
            ),
        )


def get_approximate_num_of_messages(
    sqs_client: sqs_client, queue_url: str
) -> int:
    messages = sqs_client.get_queue_attributes(
        QueueUrl=queue_url, AttributeNames=['ApproximateNumberOfMessages']
    )['Attributes']['ApproximateNumberOfMessages']
    return int(messages)


def test_check_model_run_already_exists(mock_sds):
    assert not check_model_run_already_exists(
        AGENCY, MODEL, RUN, RUN_TYPE, SDS_HOST
    )
    set_pending_model_run(AGENCY, MODEL, RUN, RUN_TYPE, SDS_HOST)
    assert check_model_run_already_exists(
        AGENCY, MODEL, RUN, RUN_TYPE, SDS_HOST
    )


def test_check_messages_model_ready(
    create_s3_bucket, create_sqs_queue, mock_sds
):
    sqs_client, queue_url = create_sqs_queue
    expected_timestamps = list(itertools.chain(range(121), range(123, 385, 3)))
    put_items_in_bucket(create_s3_bucket, expected_timestamps)
    send_messages_to_queue(queue_url, sqs_client, [384])
    send_messages_to_queue(queue_url, sqs_client, [384], 'invalid/key/prefix')
    assert check_sqs_messages(
        BUCKET,
        (SCIENCE_KEY_PREFIX,),
        SQS_QUEUE_NAME,
        check,
        model_run_from_key,
    ) == (RUN, 2)


def test_check_messages_no_model_ready(
    create_s3_bucket, create_sqs_queue, mock_sds
):
    sqs_client, queue_url = create_sqs_queue
    expected_timestamps = list(itertools.chain(range(121)))
    put_items_in_bucket(create_s3_bucket, expected_timestamps)
    send_messages_to_queue(queue_url, sqs_client, expected_timestamps)
    assert check_sqs_messages(
        BUCKET,
        (SCIENCE_KEY_PREFIX,),
        SQS_QUEUE_NAME,
        check,
        model_run_from_key,
    ) == (None, 10)
    assert get_approximate_num_of_messages(sqs_client, queue_url) == 111


def test_check_messages_model_already_exists(
    create_s3_bucket, create_sqs_queue, mock_sds
):
    sqs_client, queue_url = create_sqs_queue
    expected_timestamps = list(itertools.chain(range(10)))
    put_items_in_bucket(create_s3_bucket, expected_timestamps)
    set_pending_model_run(AGENCY, MODEL, RUN, RUN_TYPE, SDS_HOST)
    send_messages_to_queue(queue_url, sqs_client, expected_timestamps)
    assert check_sqs_messages(
        BUCKET,
        (SCIENCE_KEY_PREFIX,),
        SQS_QUEUE_NAME,
        check,
        model_run_from_key,
    ) == (None, 10)
    assert get_approximate_num_of_messages(sqs_client, queue_url) == 0


def test_check_messages_model_ready_and_model_not_ready_in_queue(
    create_s3_bucket, create_sqs_queue, mock_sds
):
    sqs_client, queue_url = create_sqs_queue
    expected_timestamps = list(itertools.chain(range(121), range(123, 385, 3)))
    put_items_in_bucket(create_s3_bucket, expected_timestamps)
    send_messages_to_queue(queue_url, sqs_client, [384])
    send_messages_to_queue(
        queue_url, sqs_client, [1, 2, 3, 4], model_key_prefix='gfs/2019060606'
    )
    put_items_in_bucket(
        create_s3_bucket, [1, 2, 3, 4], model_key_prefix='gfs/2019060606'
    )
    assert check_sqs_messages(
        BUCKET,
        (SCIENCE_KEY_PREFIX,),
        SQS_QUEUE_NAME,
        check,
        model_run_from_key,
    ) == (RUN, 5)
    assert get_approximate_num_of_messages(sqs_client, queue_url) == 4


def test_check_messages_batch_empty_queue(
    create_s3_bucket, create_sqs_queue, mock_sds
):
    _, queue_url = create_sqs_queue
    assert check_sqs_messages(
        BUCKET,
        (SCIENCE_KEY_PREFIX,),
        SQS_QUEUE_NAME,
        check,
        model_run_from_key,
    ) == (None, 0)


def test_check_messages_malformed_messages(
    create_s3_bucket, create_sqs_queue, mock_sds
):
    sqs_client, queue_url = create_sqs_queue
    sqs_client.send_message(QueueUrl=queue_url, MessageBody='')
    assert check_sqs_messages(
        BUCKET,
        (SCIENCE_KEY_PREFIX, WW3_ENSEMBLE_PREFIX),
        SQS_QUEUE_NAME,
        check,
        model_run_from_key,
    ) == (None, 1)
    assert get_approximate_num_of_messages(sqs_client, queue_url) == 0
