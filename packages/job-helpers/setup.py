import setuptools  # type: ignore

with open('README.md', 'r') as f:
    long_description = f.read()

setuptools.setup(
    name='wavetrak-job-helpers',
    version='3.0.2',
    author='Wavetrak',
    author_email='nerdery@surfline.com',
    description=(
        'A library of useful packages used in Wavetrak Airflow job tasks.'
    ),
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://github.com/Surfline/wavetrak-services/common/job-helpers',
    packages=setuptools.find_packages(),
    package_data={'': ['py.typed']},
    install_requires=[
        'aiohttp',
        'boto3',
        'botocore',
        'requests',
        'slackclient',
        'smart_open',
    ],
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3',
    ],
)
