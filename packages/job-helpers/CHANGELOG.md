# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.2] - 2021-04-07
### Fixed
- `async_stream_single_file_from_http_to_s3` will skip streaming files that are empty.

## [3.0.1] - 2021-04-07
### Fixed
- `send_invalid_points_of_interest_notification` logs before attempting Slack so that we can capture errors in logs if Slack fails.

## [3.0.0] - 2021-03-16
### Changed
- `check_model_run_already_started` renamed to `check_model_run_already_exists` and includes check for `OFFLINE` runs to prevent reprocessing runs that have already been pruned.

## [2.5.2] - 2021-03-10
### Fixed
- `check_sqs_messages` properly deletes messages for which `None` returned from `model_run_from_key_func`.

## [2.5.1] - 2021-03-01
### Changed
- `check_sqs_messages` to allow `model_run_from_key_func` to return a `None` value to be ignored. This allows for S3 events to include objects that we want to ignore.

## [2.5.0] - 2021-02-11
### Removed
- `max_concurrency` argument to `S3Client`. This implementation was not memory efficient. Applications can implement concurrency management themselves.

## [2.4.0] - 2021-02-09
### Added
- `file_transfer.S3Client` to manage concurrent downloads of data using semaphore.
  - `S3Client.download_object_byte_ranges` is a multiple byte range download.
  - `S3Client.get_file_object_contents` fetches data into memory.

### Changed
- Implemented use of `conda-devenv`.

## [2.3.0] - 2020-07-22
### Added
- `job_secrets_helper.get_secret` now includes an optional `default_value` argument. This value will be returned instead of raising a `SecretError` when the secret is not found.

### Fixed
- `job_secrets_helper.get_secret` reverted to using Secrets Manager client instead of SSM client when looking up secrets from Secrets Manager to fix a bug where applications with `secretsmanager:GetSecretValue` permissions but not `ssm:GetParameter` permissions would error.

## [2.2.1] - 2020-07-22
### Fixed
- Fixed type for `send_invalid_points_of_interest_notification`.

## [2.2.0] - 2020-07-21
### Changed
- Updated `get_secret` in `job_secrets_helper` to access secrets from Parameter Store in addition to Secrets Manager.

## [2.1.0] - 2020-07-21
### Added
- Added `send_invalid_points_of_interest_notification` to `slack_helper` module.

## [2.0.1] - 2020-05-19
### Fixed
- Fixed linting issues from using `flake8`.

## [2.0.0] - 2020-05-11
### Changed
- `check_model_ready.check_messages` was updated to remove the unnecessary `science_key_prefix` parameter.
- Type hints for`check_model_ready.check_messages` and `check_model_ready.Message` were updated from `Tuple[str]` to `Tuple[str, ...]` to account for a variable number of items in the `Tuple` parameters.

## [1.2.0] - 2020-04-17
### Added
- `check_model_ready.get_latest_s3_run` function for looking up the latest model run with data in an S3 bucket.

## [1.1.0] - 2020-01-29
### Added
- Support for type-checking package usage in consuming code. See [PEP-561](https://www.python.org/dev/peps/pep-0561/).

## [1.0.0] - 2020-01-20
### Changed
- Functions exported from `check_model_ready` now have required model run type arguments, to differentiate between `FULL_GRID` and `POINT_OF_INTEREST` runs.

## [0.2.1] - 2020-01-15
### Fixed
- Moved `file_transfer.streaming` HTTP log message into semaphore context

## [0.2.0] - 2020-01-14
### Added
- `file_transfer` helpers added

## [0.1.1] - 2019-10-17
### Fixed
- `check_model_run_already_started` not filtering for the `run` properly.

## [0.1.0] - 2019-10-16
### Added
- Instructions for installing local changes for testing and development.

### Changed
- `check_model_run_already_processed` was replaced by `check_model_run_already_started`, which checks Science Data Service instead of S3 for a `PENDING` or `ONLINE` model run.
- `mark_model_run_processed` was replaced by `set_pending_model_run`, which creates or sets the current model run to `PENDING` in the Science Data Service.

## [0.0.5] - 2019-09-19
### Added
- Added support for checking multiple SQS message prefixes in the `check_model_ready.message` modules.
- Updated the `model_run_processed_key` function to store the S3 file object under the `model_run_processed_prefix/job_name/model_run` prefix rather than under each respective `science_key_prefix/job_name/model_run` folder. This allows for the S3 file object that indicates the `model_run` has already been processed to be stored in an alternative location than the `science_key_prefix`.

## [0.0.4] - 2019-09-19
### Added
- `secrets` module containing `get_secret()` function.

## [0.0.3] - 2019-08-09
### Added
- Unit test to make sure malformed SQS queue messages are deleted.

### Fixed
- Use try/except to handle malformed messages. Previous solution was messy and difficult to account for every edge case.

## [0.0.2] - 2019-08-08
### Added
- This CHANGELOG.md file, based on conventions established in [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

### Fixed
- Fixed a bug in `check_model_ready` module where parsing SQS queue messages not matching S3 event shape would fail.

## [0.0.1] - 2019-07-11
### Added
- `check_model_ready` module exports functions to help check S3 event messages from an SQS queue to determine if a data model run is ready for processing. Exports:
  - `check_model_run_already_processed` for checking S3 for an object indicating the model run is already processed.
  - `mark_model_run_processed` for adding an object to S3 indicating the model run is already processed.
  - `check_sqs_messages` for pulling messages from SQS queue, executing a callback to check each message, and deleting messages from the queue.
