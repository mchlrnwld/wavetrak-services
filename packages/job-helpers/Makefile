ENV ?= dev

all: lint type-check test deploy format

.PHONY: all

format:
	@isort -rc .
	@autoflake --remove-all-unused-imports --remove-unused-variables --ignore-init-module-imports --recursive --in-place .
	@black .

.PHONY: format

lint:
	@flake8 .
	@black --check .

.PHONY: lint

type-check:
	@mypy .

.PHONY: type-check

test:
	@pytest .

.PHONY: test

deploy:
	@docker build -t wavetrak-job-helpers .
	@docker run --rm -t -v ~/.pypirc:/root/.pypirc wavetrak-job-helpers \
		"conda activate wavetrak-job-helpers && python setup.py bdist_wheel upload -r ${ENV}"

.PHONY: deploy

doctoc:
	@docker run --entrypoint doctoc --rm -it -v "$(shell pwd)":/usr/src jorgeandrada/doctoc README.md --notitle --maxlevel 3

.PHONY: doctoc
