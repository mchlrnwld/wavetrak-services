from unittest.mock import patch

import pytest  # type: ignore
import slack  # type: ignore

from slack_helper import send_invalid_points_of_interest_notification


@patch.object(slack.WebClient, 'chat_postMessage')
def test_send_invalid_points_of_interest_notification(mock_chat_postMessage,):
    # Logs invalid POIs even when Slack fails.
    mock_chat_postMessage.side_effect = slack.errors.SlackApiError(
        "Test Message", "Test Response",
    )
    logged = []
    with pytest.raises(slack.errors.SlackApiError):
        send_invalid_points_of_interest_notification(
            job_name='test-job',
            point_of_interest_ids_by_grid={'test-grid': ['test-poi-id']},
            slack_api_token='asdf-asdf-asdf-adsf',
            slack_channel='test-channel',
            log_callback=lambda x: logged.append(x),
        )

    mock_chat_postMessage.assert_called_once()
    assert logged == [
        "Misconfigured points of interest for test-grid: ['test-poi-id']"
    ]
