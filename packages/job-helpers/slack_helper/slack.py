from typing import Callable, Iterable, Mapping, Optional

import slack  # type: ignore


def send_invalid_points_of_interest_notification(
    job_name: str,
    point_of_interest_ids_by_grid: Mapping[str, Iterable[str]],
    slack_api_token: str,
    slack_channel: str,
    log_callback: Optional[Callable[[str], None]] = None,
):
    """
    Sends a Slack message with invalid points of interest by grid for the job.

    Args:
        job_name: Name of the job.
        point_of_interest_ids_by_grid: Map of grids to lists of invalid points
                                       of interest.
        slack_api_token: API token for Slack app used to send message.
        slack_channel: Channel to send message in.
        log_callback: Optional callback for extra logging.
    """
    if log_callback:
        for (
            grid,
            point_of_interest_ids,
        ) in point_of_interest_ids_by_grid.items():
            if not point_of_interest_ids:
                continue

            log_callback(
                f'Misconfigured points of interest for {grid}: '
                f'{point_of_interest_ids}'
            )
    grid_point_of_interest_fields = [
        {'type': 'mrkdwn', 'text': '*Grid*'},
        {'type': 'mrkdwn', 'text': '*Point of Interest IDs*'},
    ]

    for grid, point_of_interest_ids in point_of_interest_ids_by_grid.items():
        grid_point_of_interest_fields.append(
            {'type': 'mrkdwn', 'text': f'`{grid}`'}
        )
        grid_point_of_interest_fields.append(
            {
                'type': 'mrkdwn',
                'text': ', '.join([f'`{id}`' for id in point_of_interest_ids]),
            }
        )

    slack_message_blocks = [
        {
            'type': 'section',
            'text': {
                'type': 'mrkdwn',
                'text': f'`{job_name}` has misconfigured points of interest.',
            },
        },
        {'type': 'divider'},
        {'type': 'section', 'fields': grid_point_of_interest_fields},
    ]

    slack_client = slack.WebClient(token=slack_api_token)
    slack_client.chat_postMessage(
        channel=slack_channel, blocks=slack_message_blocks
    )
