import boto3  # type: ignore
import pytest  # type: ignore
from moto import mock_secretsmanager  # type: ignore

from job_secrets_helper import SecretError, get_secret


@pytest.fixture(autouse=True)
def aws_variables(monkeypatch):
    monkeypatch.setenv('AWS_DEFAULT_REGION', 'us-west-1')


@pytest.fixture
def create_secret_in_aws():
    with mock_secretsmanager():
        sm_client = boto3.client('secretsmanager')

        sm_client.create_secret(
            Name='test/jobs/test-job/AWS_SECRET', SecretString='secret value'
        )
        yield sm_client


@pytest.fixture
def create_binary_secret_in_aws():
    with mock_secretsmanager():
        sm_client = boto3.client('secretsmanager')

        sm_client.create_secret(
            Name='test/jobs/test-job/AWS_BINARY_SECRET',
            SecretBinary='secret value',
        )
        yield sm_client


@pytest.fixture()
def create_secret_in_env(monkeypatch):
    monkeypatch.setenv('ENV_SECRET', 'an env secret value')


@pytest.fixture
def create_secret_in_aws_and_env(monkeypatch):
    monkeypatch.setenv('AWS_AND_ENV_SECRET', 'secret value from env')

    with mock_secretsmanager():
        sm_client = boto3.client('secretsmanager')

        sm_client.create_secret(
            Name='test/jobs/test-job/AWS_AND_ENV_SECRET',
            SecretString='secret value from aws',
        )
        yield sm_client


def test_get_existing_secret_from_aws(create_secret_in_aws):
    assert get_secret('test/jobs/test-job/', 'AWS_SECRET') == 'secret value'


def test_get_existing_secret_from_environment(create_secret_in_env):
    assert (
        get_secret('test/jobs/test-job/', 'ENV_SECRET')
        == 'an env secret value'
    )


def test_get_nonexisting_secret_raises_exception():
    # No need to scope this to AWS or ENV
    # If it doesn't exist, both paths will be tested by this
    with pytest.raises(SecretError):
        get_secret('test/jobs/test-job/', 'NON_EXISTING_SECRET')


def test_get_binary_secret_raises_exception(create_binary_secret_in_aws):
    with pytest.raises(SecretError):
        get_secret('test/jobs/test-job/', 'AWS_BINARY_SECRET')


def test_secret_from_environment_has_precendence(create_secret_in_aws_and_env):
    assert (
        get_secret('test/jobs/test-job/', 'AWS_AND_ENV_SECRET')
        == 'secret value from env'
    )


def test_default_secret_value():
    assert (
        get_secret(
            'test/jobs/test-job/',
            'NON_EXISTING_SECRET',
            'default secret value',
        )
        == 'default secret value'
    )
