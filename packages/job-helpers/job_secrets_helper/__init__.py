import job_secrets_helper.secrets as secrets

get_secret = secrets.get_secret
SecretError = secrets.SecretError
