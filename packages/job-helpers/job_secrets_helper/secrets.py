import os
from typing import Optional

import boto3  # type: ignore
from botocore.exceptions import ClientError  # type: ignore

# This module may be used in an environment where AWS env vars don't exist.
# If so, set a sane default to allow boto3 to instantiate a client.
if 'AWS_DEFAULT_REGION' not in os.environ:
    os.environ['AWS_DEFAULT_REGION'] = 'us-west-1'

sm_client = boto3.client('secretsmanager')
ssm_client = boto3.client('ssm')


def get_secret(
    prefix: str, key: str, default_value: Optional[str] = None
) -> str:
    """
    Get value of a secret from the environment or AWS Secrets Manager
    If the secret exists in the environment, use it. Otherwise get the secret
    value from AWS Secret Manager. If the secret doesn't exist there either,
    raise exception or return the default value.

    Arguments:
        prefix: Prefix of the secret
        key: Secret name
        default_value: Value to return if secret not found

    Returns:
        The value corresponding to a secret name or None if the secret
        doesn't exist

    Raises:
        SecretError
    """
    if key in os.environ:
        return os.environ[key]

    try:
        response = sm_client.get_secret_value(SecretId=f'{prefix}{key}')
        return response['SecretString']
    except (ClientError, KeyError) as e:
        if default_value is not None:
            return default_value

        raise SecretError(e)


class SecretError(Exception):
    pass
