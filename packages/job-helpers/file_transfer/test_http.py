import pytest  # type: ignore
from aioresponses import aioresponses  # type: ignore

from file_transfer import HttpError, async_get_file_from_http


@pytest.mark.asyncio
async def test_async_get_file_from_http():
    with aioresponses() as m:
        m.get('mock://host.com/', status=200, body='HTTP file content')
        response = await async_get_file_from_http('mock://host.com/')
        assert response == 'HTTP file content'


@pytest.mark.asyncio
async def test_async_get_file_from_http_raises_exception_on_error():
    with aioresponses() as m:
        m.get('mock://host.com/', status=500, body='Error text')
        with pytest.raises(HttpError):
            await async_get_file_from_http('mock://host.com/')
