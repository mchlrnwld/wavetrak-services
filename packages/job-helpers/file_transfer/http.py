import logging

import aiohttp  # type: ignore

logger = logging.getLogger(__name__)


async def async_get_file_from_http(url: str) -> str:
    """
    Gets the http contents from the given URL.

    Args:
        url: The url from which to get http contents.

    Returns:
        A list of http contents from the given URL if present,
        otherwise the empty list is returned.

    Raises:
        HttpError
    """
    try:
        async with aiohttp.ClientSession(raise_for_status=True) as session:
            logger.debug(f'Opening HTTP connection to {url}')
            async with session.get(url) as response:
                return await response.text()
    except BaseException as e:
        raise HttpError(e)


class HttpError(Exception):
    pass
