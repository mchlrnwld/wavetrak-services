from typing import Optional, Sequence, Tuple

import boto3  # type: ignore
import botocore  # type: ignore
import pytest  # type: ignore
from aioresponses import aioresponses  # type: ignore
from moto import mock_s3  # type: ignore

from file_transfer import (
    StreamingError,
    async_stream_files_from_http_to_s3,
    async_stream_single_file_from_http_to_s3,
    build_byte_header,
)


@pytest.fixture(autouse=True)
def aws_variables(monkeypatch):
    monkeypatch.setenv('AWS_DEFAULT_REGION', 'us-west-1')


def test_build_byte_header():
    assert build_byte_header([(0, 1024), (1024, 2048)]) == '0-1024,1024-2048'


def test_build_byte_header_with_empty_byte_end():
    assert (
        build_byte_header([(0, 1024), (1024, 2048), (2048, None)])
        == '0-1024,1024-2048,2048-'
    )


def test_build_byte_header_with_empty_bytes_range():
    assert build_byte_header([]) == ''


@pytest.mark.asyncio
async def test_async_stream_single_file_from_http_to_s3():
    http_url = 'mock://host.com/'
    http_file_content = 'HTTP file content'

    s3_bucket = 'mock-s3-bucket'
    s3_key = 'test/key.txt'
    s3_url = f's3://{s3_bucket}/{s3_key}'

    with aioresponses() as mock_http:
        mock_http.get(
            http_url,
            status=200,
            body=http_file_content,
            headers={'content-length': str(125618)},
        )

        with mock_s3():
            conn = boto3.resource('s3', region_name='us-west-1')
            conn.create_bucket(Bucket=s3_bucket)

            await async_stream_single_file_from_http_to_s3(
                src_url=http_url, dest_url=s3_url
            )

            s3_body = (
                conn.Object(s3_bucket, s3_key).get()['Body'].read().decode()
            )

            assert s3_body == http_file_content


@pytest.mark.asyncio
async def test_async_stream_single_file_from_http_to_s3_empty():
    http_url = 'mock://host.com/'
    http_file_content = ''

    s3_bucket = 'mock-s3-bucket'
    s3_key = 'test/key.txt'
    s3_url = f's3://{s3_bucket}/{s3_key}'

    with aioresponses() as mock_http:
        mock_http.get(
            http_url,
            status=200,
            body=http_file_content,
            headers={'content-length': str(0)},
        )

        with mock_s3():
            conn = boto3.resource('s3', region_name='us-west-1')
            conn.create_bucket(Bucket=s3_bucket)

            await async_stream_single_file_from_http_to_s3(
                src_url=http_url, dest_url=s3_url
            )

            with pytest.raises(botocore.exceptions.ClientError) as error:
                conn.Object(s3_bucket, s3_key).get()

            assert str(error.value) == (
                'An error occurred (NoSuchKey) when calling the GetObject '
                'operation: The specified key does not exist.'
            )


@pytest.mark.asyncio
async def test_async_stream_files_from_http_to_s3():
    http_url_1 = 'mock://host.com/file_1.txt'
    http_file_content_1 = 'HTTP file content 1'

    http_url_2 = 'mock://host.com/file_2.txt'
    http_file_content_2 = 'HTTP file content 2'

    s3_bucket = 'mock-s3-bucket'
    s3_key_1 = 'test/key_1.txt'
    s3_url_1 = f's3://{s3_bucket}/{s3_key_1}'
    s3_key_2 = 'test/key_2.txt'
    s3_url_2 = f's3://{s3_bucket}/{s3_key_2}'

    with aioresponses() as mock_http:
        mock_http.get(
            http_url_1,
            status=200,
            body=http_file_content_1,
            headers={'content-length': str(455798)},
        )
        mock_http.get(
            http_url_2,
            status=200,
            body=http_file_content_2,
            headers={'content-length': str(125618)},
        )

        with mock_s3():
            conn = boto3.resource('s3', region_name='us-west-1')
            conn.create_bucket(Bucket=s3_bucket)

            urls: Sequence[
                Tuple[str, str, Optional[Sequence[Tuple[int, Optional[int]]]]]
            ] = [(http_url_1, s3_url_1, []), (http_url_2, s3_url_2, [])]

            await async_stream_files_from_http_to_s3(urls=urls)

            s3_body_1 = (
                conn.Object(s3_bucket, s3_key_1).get()['Body'].read().decode()
            )

            s3_body_2 = (
                conn.Object(s3_bucket, s3_key_2).get()['Body'].read().decode()
            )

            assert s3_body_1 == http_file_content_1
            assert s3_body_2 == http_file_content_2


@pytest.mark.asyncio
async def test_async_stream_files_from_http_to_s3_handles_exceptions():
    http_url_1 = 'mock://host.com/file_1.txt'
    http_file_content_1 = 'HTTP file content 1'

    http_url_2 = 'mock://host.com/file_2.txt'
    http_file_content_2 = 'HTTP file content 2'

    s3_bucket = 'mock-s3-bucket'
    s3_key_1 = 'test/key_1.txt'
    s3_url_1 = f's3://{s3_bucket}/{s3_key_1}'
    s3_key_2 = 'test/key_2.txt'
    s3_url_2 = f's3://{s3_bucket}/{s3_key_2}'

    with aioresponses() as mock_http:
        mock_http.get(
            http_url_1,
            status=200,
            body=http_file_content_1,
            headers={'content-length': str(455798)},
        )
        mock_http.get(
            http_url_2,
            status=500,
            body=http_file_content_2,
            headers={'content-length': str(125618)},
        )

        with mock_s3():
            conn = boto3.resource('s3', region_name='us-west-1')
            conn.create_bucket(Bucket=s3_bucket)

            urls: Sequence[
                Tuple[str, str, Optional[Sequence[Tuple[int, Optional[int]]]]]
            ] = [(http_url_1, s3_url_1, []), (http_url_2, s3_url_2, [])]

            with pytest.raises(StreamingError):
                await async_stream_files_from_http_to_s3(urls=urls)

                s3_body_1 = (
                    conn.Object(s3_bucket, s3_key_1)
                    .get()['Body']
                    .read()
                    .decode()
                )

                assert s3_body_1 == http_file_content_1
