import asyncio
import os
from functools import partial
from typing import List, Optional, Tuple

import boto3  # type: ignore


class S3Client:
    """
    S3Client to interface with S3 and download files asynchronously.

    Attributes:
        client: boto3 client for S3.
    """

    def __init__(self):
        self.client = boto3.client('s3')

    async def download_object_byte_ranges(
        self,
        bucket: str,
        key: str,
        destination_path: str,
        byte_ranges: List[Tuple[int, Optional[int]]],
    ):
        """
        Downloads an object from S3 with support for multiple byte ranges.

        Args:
            bucket: Name of S3 Bucket to download from.
            key: Key for the S3 object to download.
            destination_path: Local file path to save the S3 object to.
            byte_ranges: Optional list of byte ranges for multiple byte
                         ranges. Ex: [(0, 1024), (2048, 4096)]
        """
        os.makedirs(os.path.dirname(destination_path), exist_ok=True)
        file_contents = await asyncio.gather(
            *[
                self.get_object_contents(
                    bucket, key, f'bytes={byte_start}-{byte_end or ""}'
                )
                for byte_start, byte_end in byte_ranges
            ]
        )

        loop = asyncio.get_running_loop()
        await loop.run_in_executor(
            None,
            self._write_file_contents,
            destination_path,
            list(file_contents),
        )

    async def get_object_contents(
        self, bucket: str, key: str, request_range: str = ''
    ) -> bytes:
        """
        Fetches an S3 object into memory.

        Args:
            bucket: Name of S3 Bucket to download from.
            key: The key object in S3 to fetch.
            request_range: HTTP range request header.

        Returns:
            S3 object file contents.
        """
        loop = asyncio.get_running_loop()
        return await loop.run_in_executor(
            None,
            partial(self._get_object_contents, bucket, key, request_range),
        )

    def _get_object_contents(
        self, bucket: str, key: str, request_range: str = ''
    ) -> bytes:
        return self.client.get_object(
            Bucket=bucket, Key=key, Range=request_range
        )['Body'].read()

    def _write_file_contents(self, file_path: str, file_contents: List[bytes]):
        with open(file_path, 'wb') as file:
            for content_bytes in file_contents:
                file.write(content_bytes)
