import asyncio
import logging
from contextlib import AsyncExitStack
from timeit import default_timer
from typing import Optional, Sequence, Tuple

import aiohttp  # type: ignore
import smart_open  # type: ignore

logger = logging.getLogger(__name__)


async def async_stream_files_from_http_to_s3(
    urls: Sequence[
        Tuple[str, str, Optional[Sequence[Tuple[int, Optional[int]]]]]
    ],
    max_concurrency: int = 10,
    chunk_size: int = 1048576,
) -> None:
    """
    Asynchronously streams files from an HTTP source to S3 for a list of URLs

    Args:
        urls: Sequence of tuples where each tuple contains the following:
            src_url
            dest_url
            byte_ranges (optional): Sequence of tuples of byte
                                    start/stop positions
                - [(0, 1024)]
                - [(0, 1024), (1024, 2048)]
                - [(0, 1024), (1024, 2048), (2048, None)]

            Ex.
              [('https://nomads.ncep.noaa.gov/wc_4m.t06z.f117.grib2',
                's3://surfline-science-s3-dev/noaa/ww3/wc_4m.t06z.f117.grib2',
                [(0, 1024)]),
               ('https://nomads.ncep.noaa.gov/wc_4m.t06z.f118.grib2',
                's3://surfline-science-s3-dev/noaa/ww3/wc_4m.t06z.f118.grib2',
                [(0, 1024), (1024, 2048)]),
               ('https://nomads.ncep.noaa.gov/wc_4m.t06z.f119.grib2',
                's3://surfline-science-s3-dev/noaa/ww3/wc_4m.t06z.f119.grib2',
                None)]

        max_concurrency: (optional) The maximum number of concurrent streams.
            Default: 10
        chunk_size: (optional) The number of downloaded bytes to read into
            memory at once. Default: 1048576 (1 MB)

    Returns:
        None
    """

    semaphore = asyncio.Semaphore(max_concurrency)

    results = await asyncio.gather(
        *[
            async_stream_single_file_from_http_to_s3(
                src_url, dest_url, byte_ranges, semaphore, chunk_size
            )
            for (src_url, dest_url, byte_ranges) in urls
        ],
        return_exceptions=True,
    )

    exceptions = [
        result for result in results if isinstance(result, BaseException)
    ]

    if len(exceptions) > 0:
        logger.info(f'Raising the following async exceptions: {exceptions}')
        raise StreamingError(exceptions)

    return


async def async_stream_single_file_from_http_to_s3(
    src_url: str,
    dest_url: str,
    byte_ranges: Optional[Sequence[Tuple[int, Optional[int]]]] = [],
    semaphore: Optional[asyncio.Semaphore] = None,
    chunk_size: int = 1048576,
) -> None:
    """
    Streams a single file from an HTTP source to S3

    Args:
        src_url: HTTP URL where the file should be streamed from.
        dest_url: S3 URL where the file should be streamed to.
        byte_ranges (optional): Sequence of tuples of byte
                                start/stop positions.
            Default: None
        semaphore: (optional) The asyncio Semaphore to control concurrency.
        chunk_size: (optional) The number of downloaded bytes to read into
            memory at once. Default: 1048576 (1 MB)

    Returns:
        None

    Raises:
        StreamingError
    """

    try:
        headers = {'Range': f'bytes={build_byte_header(byte_ranges)}'}

        async with AsyncExitStack() as stack:
            if semaphore:
                await stack.enter_async_context(semaphore)

            start = default_timer()
            logger.info(
                f'Opening HTTP connection to {src_url} with headers {headers}'
            )

            session = await stack.enter_async_context(
                aiohttp.ClientSession(raise_for_status=True)
            )
            response = await stack.enter_async_context(
                session.get(src_url, headers=headers)
            )

            size = int(response.headers.get('Content-Length', 0))
            if size == 0:
                logger.info(f'No contents found, skipping empty file')
                return

            with smart_open.open(dest_url, 'wb') as s3_file:
                while True:
                    chunk = await response.content.read(chunk_size)
                    if not chunk:
                        break
                    logger.debug(f'Writing chunk to {dest_url}')
                    s3_file.write(chunk)

            logger.info(
                f'Streamed {src_url} => {dest_url}'
                f' ({default_timer() - start:.3f}s)'
            )

    except BaseException as e:
        raise StreamingError(e)

    return


def build_byte_header(
    byte_ranges: Optional[Sequence[Tuple[int, Optional[int]]]]
):
    return ','.join(
        [
            f'{byte_start}-{byte_end or ""}'
            for (byte_start, byte_end) in (byte_ranges or [])
        ]
    )


class StreamingError(Exception):
    pass
