# Wavetrak theme

The global theming package for the Surfline and Wavetrak applications. You will find one main `theme` exported here that provides base level styles for all of our MUI components across the various applications.

## How to use - Client side bundling

The theme package is fully typed and exports a single `theme` object which is consumed by applications by a provider. The provider comes from `@mui/material` so you must install the following packages before using this theme: 
```
// with npm
npm install @mui/material @emotion/react @emotion/styled

// with yarn
yarn add @mui/material @emotion/react @emotion/styled
## Publishing NPM package
```

Once these packages are installed at the root of yoru project you can import the `ThemeProvider`. This will wrap your application and you will be able to pass the Surfline `theme` down throughout your application simply by providing it as a prop to the `ThemeProvider`. This theme provides overrides to the core MUI components for both `dark` and `light` themes. This theme only targets MUI components and will not override native HTML elements so it is recommended that MUI components are used wherever possible in your application. That is it, it is that simple! Here is an example of an application implementing our theme at it's base: 

```
import { ThemeProvider } from '@mui/material/styles';
import theme from '@surfline/theme'

function App(props) {
  return (
    <ThemeProvider theme={theme}>
        {props.children}
    </ThemeProvider>
  );
}

export default App;
```

## Overriding theme styles

There may be instances in your applications where you wish to override the custom Surfline theme. If this is the case you will need to wrap your application in one more provider from MUI. This provider will pass down a prop called `injectFirst` which will tell your application to reorganize and reprioritize it's styles in a way that allows your overrides to take priority over the inline styles that MUI sets. By doing this you will be able to override both the root level styles of MUI components as well as their children. Overrides can be done with CSS Modules or a variety of CSS in JS techniques (though we prefer to use CSS Modules for their zero-runtime performance benefits).

### Step one: Adding the StyledEngineProvider

```
import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import theme from '@surfline/theme'

function App(props) {
  return (
    <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
            {props.children}
        </ThemeProvider>
    </StyledEngineProvider>
  );
}

export default App;
```

Notice that everything is the same as the example provided above, with the exception of the extra provider which wrapps the `ThemeProvider` and application children.

### Step two: Overrides with CSS Modules

Mui components provide developers with two main approaches to unique component overrides. The first option is by providing the MUI component with a `classes` prop. This prop takes an object with keys that align with the child components of the parent MUI component. The second, is very similar but the prop provided is `componentsProps` and its structure looks slightly different. 

Every MUI component has a `root` which can be used to override the styles of the base. The children of any given MUI component will be unique to the component in question. More information about compoents and their children can be found [here](https://mui.com/customization/default-theme/). The following is an example of overriding a slider component with CSS Modules:

The CSS Module (Switch.module.css):
```
  .root {
    /* styles here for root */
  }

  .thumb {
    /* styles here for the thumb of the switch */
    background-color: #20b2aa;
  }
```

The JS module (Switch.js):
```
import Switch from '@mui/material/Switch';
import style from './Switch.module.css'

export default function StyledSwitch(props) {
    return (
        <Switch 
            /** Option one - Using classes prop */
            classes={{
                root: style.root,
                thumb: style.thumb
            }} 
            /** Option two - Using compomentsProps prop */ 
            componentsProps={{ 
                thumb: { 
                    className: styles.thumb 
                } 
            }}
            {...props} 
        />
    )
}
```

## Development

While developing this package you can view your changes to compoents by running `Storybook` locally. After you have run `npm i` you can start the `Storybook` local server by running the following command from the `package.json` scripts

```
    npm run storybook
```

If you would like to use your local changes in a n application instead of in Storybook you can `Link` to your local `@surfline/theme` package by doing the following: 
- 1). Make the changes you would like to see and run `npm run build` so the package `/dist` is created to be consumed
- 2). Run ‘npm link’ within this `wavetrak-services/packages/theme` directory to create a symbolic link to this package locally.
- 3). Navigate to your application directory you want to consume the `@surfline/theme` in
- 4). Navigate to the application you want to have consume `@surfline/theme` and run `npm link [module name here]`. This will ensure that the reference to the `@surfline/theme` package is replaced with its local symbolic link.

## Publishing NPM package

Within the `/packages/` directory we publish NPM packages using Lerna.

A step by step guide for publishing NPM packages in the monorepo can be found [here](../../README.md#npm-packages).


## Dark and Light theme

MUI provides overrides for both dark and light themed applications which we have built in for the surfline theme. Your application should be responsible for recognizing wether a browser or user preferences are set to `dark` or `light` and the theme will respond to the given value.

## Visual Diff Testing

Pending setup...
