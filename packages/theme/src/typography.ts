/**
 * TODO: We have all of our measurements written in px which is not very responsive.
 * We should look in to setting the fontSize to rem and the line height to be just a percentage value such as 1.1 or 1 for example.
 * */
/**
 * Default Theme structure:
 * https://mui.com/customization/default-theme/
 */

const fontPrimary = "'Linear Sans'";
const letterSpacing = '0px';

const callout1 = {
  //styleName: Typography/Callout;
  fontSize: '14px',
  fontWeight: 700,
  lineHeight: '18px',
  letterSpacing: letterSpacing,
};

const callout2 = {
  //styleName: Typography/Callout 2;
  fontSize: '12px',
  fontWeight: 600,
  lineHeight: '16px',
  letterSpacing: letterSpacing,
};

const subHeadline = {
  //styleName: Typography/Sub Headline;
  fontSize: '16px',
  fontWeight: 600,
  lineHeight: '20px',
  letterSpacing: letterSpacing,
};

const headline = {
  //styleName: Typography/Headline;
  fontSize: '18px',
  fontWeight: 700,
  lineHeight: '22px',
  letterSpacing: letterSpacing,
};

const title3 = {
  //styleName: Typography/Title 3;
  fontSize: '20px',
  fontWeight: 700,
  lineHeight: '24px',
  letterSpacing: letterSpacing,
};

const title2 = {
  //styleName: Typography/Title 2;
  fontSize: '26px',
  fontWeight: 700,
  lineHeight: '30px',
  letterSpacing: letterSpacing,
};

const title1 = {
  //styleName: Typography/Title 1;
  fontSize: '32px',
  fontWeight: 700,
  lineHeight: '34px',
  letterSpacing: letterSpacing,
};

const body = {
  //styleName: Typography/Body;
  fontSize: '16px',
  fontWeight: 400,
  lineHeight: '24px',
  letterSpacing: '-0.5px',
};

const caption1 = {
  //styleName: Typography/Caption 1;
  fontSize: '12px',
  fontWeight: 400,
  lineHeight: '18px',
  letterSpacing: '-0.5px',
};

const overline1 = {
  //styleName: Typography/Overline 1;
  fontSize: '14px',
  fontWeight: 700,
  lineHeight: '18px',
  letterSpacing: '0.25px',
};

const overline2 = {
  //styleName: Typography/Overline 2;
  fontSize: '12px',
  fontWeight: 700,
  lineHeight: '16px',
  letterSpacing: '0.25px',
};

const overline3 = {
  //styleName: Typography/Overline 3;
  fontSize: '10px',
  fontWeight: 700,
  lineHeight: '10px',
  letterSpacing: '0.25px',
};

const footnote = {
  //styleName: Typography/Footnote;
  fontSize: '14px',
  fontWeight: 400,
  lineHeight: '2px',
  letterSpacing: '-0.5px',
};

export default {
  fontFamily: fontPrimary,

  // MUI overrides
  h1: title1,
  h2: title2,
  h3: title3,
  h4: headline,
  h5: subHeadline,
  subtitle1: callout1,
  subtitle2: callout2,
  body1: body,
  body2: body,
  button: caption1,
  caption: caption1,
  overline: overline1,

  // custom variables as declared in Figma from Surfline designs
  body,
  callout1,
  callout2,
  caption1,
  footnote,
  headline,
  overline1,
  overline2,
  overline3,
  subHeadline,
  title1,
  title2,
  title3,
};
