import {
  brandPrimary,
  brandSecondary,
  error,
  info,
  productDivider,
  success,
  text,
  warning,
} from './colors/mapped-colors';

import { white } from './colors/base-colors';

const palette = {
  background: {
    default: white,
    paper: white,
  },
  common: {
    black: brandSecondary,
    white: white,
  },
  divider: productDivider,
  error,
  info,
  primary: {
    contrastText: white,
    dark: brandPrimary,
    light: brandPrimary,
    main: brandPrimary,
  },
  secondary: {
    contrastText: white,
    dark: brandSecondary,
    light: brandSecondary,
    main: brandSecondary,
  },
  success,
  text,
  warning,
};

export default palette;
