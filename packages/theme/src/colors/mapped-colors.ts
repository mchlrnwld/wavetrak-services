/** Mapping colors from base-colors to more human readable variables */
import {
  aqua40,
  black,
  blue40,
  blue50,
  gray10,
  gray15,
  gray20,
  gray25,
  gray30,
  gray40,
  gray50,
  gray60,
  gray70,
  gray80,
  gray90,
  green40,
  green50,
  orange40,
  orange60,
  pink40,
  pink60,
  red40,
  red60,
  white,
} from './base-colors';

/** base */
export const base = {
  white,
  black,
};

/** brand */
export const brandPrimary = blue40;
export const brandSecondary = gray10;
export const brandTertiary = orange40;

/** product */
export const productBodyPrimary = gray20;
export const productBodySecondary = gray30;
export const productDivider = gray60;

export const text = {
  primary: gray20,
  secondary: gray30,
  disabled: gray70,
};

/** states */
export const disabled = gray70;
export const inactive = gray40;
export const background = gray90;
export const live = red60;
export const stormAlert = pink60;

export const error = {
  contrastText: white,
  dark: red40,
  light: red40,
  main: red40,
};

export const info = {
  contrastText: white,
  dark: blue50,
  light: blue50,
  main: blue50,
};

export const success = {
  contrastText: white,
  dark: green40,
  light: green40,
  main: green40,
};

export const warning = {
  contrastText: white,
  dark: orange60,
  light: orange60,
  main: orange60,
};

/** twentyft */
export const twentyFtAccent = green50;
export const twentyFtDarkGray = gray15;
export const twentyFtMidGray = gray25;

/** conditions */
export const conditionsVeryPoor = gray30;
export const conditionsPoor = blue50;
export const conditionsPoorToFair = aqua40;
export const conditionsFair = green40;
export const conditionsGood = orange60;

/** forecast */
export const forecastPrimary = gray50;
export const forecastSecondary = gray80;

/** swell */
export const swellPrimary = blue40;
export const swellSecond = pink40;

/**
 * Temporary colors
 * these colors are either old, undecided or unestablished states like hover, active, focus that have not been fully
 * decided on by the design team and are subject to change so we are isolating them for easier ability to
 * remove them or update them in the future.
 */

export const buttonColorHover = '#2137A5';
export const borderColorHover = '#2137A5';
export const buttonColor = '#314EE6';
export const borderColor = '#314EE6';
export const swellFifth = '#FE2163';
export const swellThird = '#7B61FFF0';
