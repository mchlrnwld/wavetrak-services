/**
 * Figma color palettes
 * https://www.figma.com/file/2ayZyZwDhwtZr2xyqnLEcQ/Wavetrak-Design-System?node-id=2484%3A1791
 *  https://www.figma.com/file/2ayZyZwDhwtZr2xyqnLEcQ/Wavetrak-Design-System?node-id=2484%3A1724
 */

/** Blues */
export const blue40 = '#314EE6'; // brand/primary
export const blue50 = '#408FFF'; // conditions/poor

/** Aquas */
export const aqua40 = '#20BCDF'; // conditions/poor to fair

/** Greens */
export const green40 = '#1AD64C'; // conditions/fair
export const green50 = '#DBFF00'; // twentyft/accent

/** Neutrals */
export const black = '#000000'; // base black
export const gray10 = '#171717'; // brand/secondary
export const gray15 = '#282828'; // twntyft/dark gray
export const gray20 = '#424242'; // product/body
export const gray25 = '#636363'; // twentyft/mid gray
export const gray30 = '#98A2AF'; // product/body secondary or conditions/very poor
export const gray40 = '#BBBBBB'; // states/inactive
export const gray50 = '#B9D3DF'; // forecast primary
export const gray60 = '#DADDE3'; // product/dividers
export const gray70 = '#DFDFDF'; // states/disabled
export const gray80 = '#DFE9F3'; // forecast secondary // need to be transparent change this
export const gray90 = '#FAFAFA'; // background
export const white = '#FFFFFF'; // base white

/** Oranges */
export const orange40 = '#FFB600'; // brand/tertiary
export const orange60 = '#FFA833'; // conditions/good

/** Pinks */
export const pink40 = '#E12DFF'; // swell direction arrow color
export const pink60 = '#B827EB'; // storm alert

/** Reds */
export const red40 = '#F5424B'; // states/error
export const red60 = '#EC2B2B'; // states/live

/**
 * Temporary colors
 * these colors are either old, undecided or unestablished states like hover, active, focus that have not been fully
 * decided on by the design team and are subject to change so we are isolating them for easier ability to
 * remove them or update them in the future.
 */

export const purple40 = '#7B61FFF0'; // swell direction arrow color (94%)
export const green60 = '#00D695'; // swell direction arrow color
export const red80 = '#FE2163'; // swell direction arrow color
export const blue20 = '#2137A5'; // temp: hover, active, focus
export const blue60 = '#E0E5ff'; // temp: hover, active, focus
export const blue85 = '#0398F6'; // swell direction arrow color
