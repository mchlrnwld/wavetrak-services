import productCDN from '@surfline/quiver-assets';

export default {
  styleOverrides: `
    @font-face {
      font-family: 'Linear Sans';
      font-style: normal;
      font-display: swap;
      font-weight: 400;
      src: url('${productCDN}/fonts/linear-sans/linear-sans-regular.woff2') format('woff2'),
        url('${productCDN}/fonts/linear-sans/linear-sans-regular.woff') format('woff'),
        url('${productCDN}/fonts/linear-sans/linear-sans-regular.eot') format('embedded-opentype');
      unicoderange: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC,
        U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF;
    }
    @font-face {
      font-family: 'Linear Sans';
      font-style: italic;
      font-display: swap;
      font-weight: 400;
      src: url('${productCDN}/fonts/linear-sans/linear-sans-italic.woff2') format('woff2'),
        url('${productCDN}/fonts/linear-sans/linear-sans-italic.woff') format('woff'),
        url('${productCDN}/fonts/linear-sans/linear-sans-italic.eot') format('embedded-opentype');
      unicoderange: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC,
        U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF;
    }
    @font-face {
      font-family: 'Linear Sans';
      font-style: normal;
      font-display: swap;
      font-weight: 700;
      src: url('${productCDN}/fonts/linear-sans/linear-sans-bold.woff2') format('woff2'),
        url('${productCDN}/fonts/linear-sans/linear-sans-bold.woff') format('woff'),
        url('${productCDN}/fonts/linear-sans/linear-sans-bold.eot') format('embedded-opentype');
      unicoderange: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC,
        U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF;
    }
    @font-face {
      font-family: 'Linear Sans';
      font-style: italic;
      font-display: swap;
      font-weight: 700;
      src: url('${productCDN}/fonts/linear-sans/linear-sans-bold-italic.woff2') format('woff2'),
        url('${productCDN}/fonts/linear-sans/linear-sans-bold-italic.woff') format('woff'),
        url('${productCDN}/fonts/linear-sans/linear-sans-bold-italic.eot') format('embedded-opentype');
      unicoderange: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC,
        U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF;
    }
  `,
};
