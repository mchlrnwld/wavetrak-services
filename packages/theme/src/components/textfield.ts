declare module '@mui/material/TextField' {
  interface TextFieldPropsVariantOverrides {
    surfline: true;
  }
}

export default {
  styleOverrides: {
    root: {
      borderRadius: '100px',
      '.MuiOutlinedInput-root, MuiInput-root': {
        borderRadius: '100px',
      },
    },
  },
};
