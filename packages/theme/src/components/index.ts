import MuiButton from './button';
import MuiCssBaseline from './css-baseline';
import MuiSlider from './slider';
import MuiSwitch from './switch';
import MuiAutocomplete from './autocomplete';
import MuiTextField from './textfield';
import MuiBadge from './badge';
import MuiSelect from './select';
import MuiChip from './chip';

export default {
  MuiAutocomplete,
  MuiBadge,
  MuiButton,
  MuiChip,
  MuiCssBaseline,
  MuiSelect,
  MuiSlider,
  MuiSwitch,
  MuiTextField,
};
