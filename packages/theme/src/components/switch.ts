import { gray80 } from '../colors/base-colors';

export default {
  styleOverrides: {
    track: {
      background: gray80,
    },
  },
};
