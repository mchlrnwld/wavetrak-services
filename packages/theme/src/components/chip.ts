import { brandPrimary, brandSecondary } from '../colors/mapped-colors';

export default {
  styleOverrides: {
    colorPrimary: {
      color: brandPrimary,
    },
    outlined: {
      color: brandSecondary,
      borderColor: brandSecondary,
      borderWidth: 2,
    },
    filled: {
      color: brandSecondary,
    },
  },
};
