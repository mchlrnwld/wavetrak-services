import { borderColor, buttonColor, buttonColorHover } from '../colors/mapped-colors';
import { blue60, white } from '../colors/base-colors';

export default {
  styleOverrides: {
    root: {
      borderRadius: 100,
    },
    contained: {
      backgroundColor: buttonColor,
      border: `2px solid ${borderColor}`,
      color: white,
      '&:hover, &:active, &:focus': {
        backgroundColor: buttonColorHover,
        boxShadow: 'none',
        border: `2px solid ${buttonColorHover}`,
        color: white,
      },
    },
    outlined: {
      backgroundColor: white,
      border: `2px solid ${borderColor}`,
      color: borderColor,
      '&:hover, &:active, &:focus': {
        backgroundColor: white,
        border: `2px solid ${buttonColorHover}`,
        color: buttonColorHover,
      },
    },
    text: {
      backgroundColor: 'transparent',
      border: '2px solid transparent',
      color: buttonColor,
      '&:hover, &:active, &:focus': {
        backgroundColor: blue60,
        border: `2px solid ${blue60}`,
        color: buttonColorHover,
      },
    },
  },
};
