import { gray80 } from '../colors/base-colors';

export default {
  styleOverrides: {
    rail: {
      background: gray80,
    },
  },
};
