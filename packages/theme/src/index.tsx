import CssBaseline from '@mui/material/CssBaseline';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import React from 'react';

import components from './components';
import palette from './palette';
import typography from './typography';

type Props = {
  children: React.ReactNode;
};

export const WavetrakThemeProvider: React.FC<Props> = (props) => {
  const { children } = props;
  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        {children}
      </ThemeProvider>
    </StyledEngineProvider>
  );
};

export const theme = createTheme({
  components,
  palette,
  typography,
  shape: {
    borderRadius: 0,
  },
});

export default theme;
