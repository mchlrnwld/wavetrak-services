# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [0.1.1](https://github.com/Surfline/wavetrak-services/compare/@wavetrak/theme@0.1.0...@wavetrak/theme@0.1.1) (2021-12-13)

**Note:** Version bump only for package @wavetrak/theme





## [0.1.0](https://github.com/Surfline/wavetrak-services/compare/@wavetrak/theme@0.0.1...@wavetrak/theme@0.1.0) (2021-12-02)


### Features

* add linear-sans ([debaad5](https://github.com/Surfline/wavetrak-services/commit/debaad585e626d3becfa06263894254f3f4219f5))


### Bug Fixes

* update tsconfig to ensure react cjs imports work ([301412c](https://github.com/Surfline/wavetrak-services/commit/301412ca6af43e3845d74c2ce91b1e44c0cc93e6))



### 0.0.1 (2021-11-16)

**Note:** Version bump only for package @wavetrak/theme
