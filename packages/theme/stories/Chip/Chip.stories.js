import Stack from '@mui/material/Stack';
import Chip from '@mui/material/Chip';
import Box from '@mui/material/Box';

export default {
    title: "Chip",
    component: Chip
}

export const All = () => {
  const handleClick = () => {
    console.info('You clicked the Chip.');
  };

  return (
    <Box sx={{ '& > :not(style)': { m: 1 } }}>
        <Stack direction="row" spacing={1}>
            <Chip label="Chip Filled" />
            <Chip label="Chip Outlined" variant="outlined" />
        </Stack>

        <Stack direction="row" spacing={1}>
            <Chip label="Clickable" onClick={handleClick} />
            <Chip label="Clickable" variant="outlined" onClick={handleClick} />
        </Stack>
    </Box>
  );
}
