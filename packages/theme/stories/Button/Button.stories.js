import Box from '@mui/material/Box';
import Button  from '@mui/material/Button'

export default {
    title: "Button",
    component: Button
}

export const All = () => {
    return (
        <Box sx={{ '& > :not(style)': { m: 1 } }}>
            <Button variant="contained"> Contained </Button>
            <Button variant="outlined"> Outlined </Button>
            <Button variant="text"> Text </Button>
        </Box>

    )
}


export const ContainedButton = () => <Button variant="contained"> Contained </Button>
export const OutlinedButton = () => <Button variant="outlined"> Outlined </Button>
export const TextButton = () => <Button variant="text"> Text </Button>
