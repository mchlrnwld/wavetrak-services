import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField'

export default {
    title: "Text Field",
    component: TextField
}

export const All = () => {
    return (
        <Box sx={{ '& > :not(style)': { m: 1 } }}>
           <TextField id="outlined-basic" label="Outlined" variant="outlined" />
           <TextField id="standard-basic" label="Standard" variant="standard" />
        </Box>

    )
}

export const Outlined = () => <TextField id="outlined-basic" label="Outlined" variant="outlined" />
export const Standard = () =><TextField id="standard-basic" label="Standard" variant="standard" />
