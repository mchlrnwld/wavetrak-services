import Checkbox from '@mui/material/Checkbox';
import Box from '@mui/material/Box';

export default {
    title: "Checkbox",
    component: Checkbox
}

export const All = () => {
    return (
        <Box sx={{ '& > :not(style)': { m: 1 } }}>
            <Checkbox label={{ inputProps: { 'aria-label': 'Default checkbox' } }} defaultChecked />
            <Checkbox label={{ inputProps: { 'aria-label': 'Checked checkbox' } }} />
            <Checkbox label={{ inputProps: { 'aria-label': 'Disabled checkbox' } }} disabled />
            <Checkbox label={{ inputProps: { 'aria-label': 'Disabled checked checkbox' } }} disabled checked />
        </Box>

    )
}
export const DefaultCheckbox = () =>  ( <Checkbox label={{ inputProps: { 'aria-label': 'Default checkbox' } }} defaultChecked /> )
export const CheckedCheckbox = () =>  ( <Checkbox label={{ inputProps: { 'aria-label': 'Checked checkbox' } }} /> )
export const DisabledCheckbox = () =>  ( <Checkbox label={{ inputProps: { 'aria-label': 'Disabled checkbox' } }} disabled /> )
export const DisabledCheckedCheckbox = () =>  ( <Checkbox label={{ inputProps: { 'aria-label': 'Disabled checked checkbox' } }} disabled checked /> )
