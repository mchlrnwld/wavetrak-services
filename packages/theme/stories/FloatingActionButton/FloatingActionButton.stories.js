import Box from '@mui/material/Box';
import Fab from '@mui/material/Fab';


export default {
    title: "Floating action button",
    component: Fab
}

export const All = () => {
  return (
    <Box sx={{ '& > :not(style)': { m: 1 } }}>
      <Fab color="primary" aria-label="add">
      </Fab>
      <Fab color="secondary" aria-label="edit">
      </Fab>
      <Fab variant="extended">
        Navigate
      </Fab>
      <Fab disabled aria-label="like">
      </Fab>
    </Box>
  );
}

export const FabPrimary = () => ( <Fab color="primary" aria-label="add"/> )
export const FabSecondary = () => ( <Fab color="secondary" aria-label="add"/> )
export const FabExtended = () => ( <Fab variant="extended" aria-label="add"> Add </Fab> )
export const FabDisabled = () => ( <Fab disabled aria-label="add"/> )
