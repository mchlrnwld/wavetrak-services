import Box from '@mui/material/Box';
import Badge  from '@mui/material/Badge'
import MailIcon from '@mui/icons-material/Mail';

export default {
    title: "Badge",
    component: Badge
}

export const All = () => {
    return (
        <Box sx={{ '& > :not(style)': { m: 1 } }}>
             <Badge badgeContent={4} color="primary">
                <MailIcon color="action" />
             </Badge>
        </Box>
    )
}
