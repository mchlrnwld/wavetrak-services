import Box from '@mui/material/Box';
import Switch from '@mui/material/Switch';

export default {
    title: "Switch",
    component: Switch
}

export const All = () => {
    return (
        <Box sx={{ '& > :not(style)': { m: 1 } }}>
            <Switch/>
            <Switch disabled/>
        </Box>

    )
}

export const BasicSwitch = () => <Switch/>
export const Disabled = () => <Switch disabled/>
