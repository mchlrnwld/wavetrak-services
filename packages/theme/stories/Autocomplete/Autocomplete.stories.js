import Autocomplete from "@mui/material/Autocomplete";
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField'

export default {
    title: "Autocomplete",
    component: Autocomplete
}

export const All = () => {
    return (
        <Box sx={{ '& > :not(style)': { m: 1 } }}>
          <Autocomplete
            id="surfline-travel-dropdown"
            options={[
                {label: 'Travel', value: 'travel'}
            ]}
            renderInput={(params) => <TextField {...params} label="Surfline" />}
          />
        </Box>

    )
}

export const DropdownAutocomplete = () => {
    return (
        <Autocomplete
        id="surfline-travel-dropdown"
        options={[
            {label: 'Travel', value: 'travel'}
        ]}
        renderInput={(params) => <TextField {...params} label="Surfline" />}
        />
    )
}
