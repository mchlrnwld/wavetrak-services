import * as React from 'react';
import Box from '@mui/material/Box';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';

export default {
    title: "Radio",
    component: Radio
}

export const All = () => {
  return (
    <Box sx={{ '& > :not(style)': { m: 1 } }}>
      <Radio />
      <Radio disabled/>
      <FormControlLabel value="surf" control={<Radio />} label="Surf" />
      <FormControlLabel value="surf" control={<Radio />} label="Surf" disabled />
      <FormControl component="fieldset">
        <FormLabel component="legend">Activities</FormLabel>
        <RadioGroup
          aria-label="activities"
          defaultValue="surf"
          name="radio-buttons-group"
        >
          <FormControlLabel value="surf" control={<Radio />} label="Surf" />
          <FormControlLabel value="skate" control={<Radio />} label="Skate" />
          <FormControlLabel value="snowboard" control={<Radio />} label="Snowboard" disabled/>
          <FormControlLabel value="other" control={<Radio />} label="Other" />
        </RadioGroup>
      </FormControl>
    </Box>
)
}

export const RadioButton = () => <Radio />
export const DisabledRadioButton = () => <Radio disabled/>
export const FormControlRadio = () => <FormControlLabel value="surf" control={<Radio />} label="Surf" />
export const FormControlDisabledRadio = () => <FormControlLabel value="surf" control={<Radio />} label="Surf" disabled />
export const FormControlRadioGroup = () => {
  return (
    <FormControl component="fieldset">
      <FormLabel component="legend">Activities</FormLabel>
      <RadioGroup
        aria-label="activities"
        defaultValue="surf"
        name="radio-buttons-group"
      >
        <FormControlLabel value="surf" control={<Radio />} label="Surf" />
        <FormControlLabel value="skate" control={<Radio />} label="Skate" />
        <FormControlLabel value="snowboard" control={<Radio />} label="Snowboard" disabled/>
        <FormControlLabel value="other" control={<Radio />} label="Other" />
      </RadioGroup>
    </FormControl>
  )
}
