import Slider from "@mui/material/Slider";
import Box from '@mui/material/Box';

export default {
    title: "Slider",
    component: Slider
}


export const All = () => {
    return (
        <Box sx={{ '& > :not(style)': { m: 1 } }}>
            <Slider defaultValue={30} />
            <Slider defaultValue={30} disabled />
        </Box>

    )
}

export const BasicSlider = () => <Slider defaultValue={30} />
export const DisabledSlider = () => <Slider defaultValue={30} disabled />
