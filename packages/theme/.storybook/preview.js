import React from 'react';
import { ThemeProvider as MUIThemeProvider, StyledEngineProvider} from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from 'emotion-theming';
import theme from '../src/index'

export const decorators = [
    (Story) => {
      return (
        <StyledEngineProvider injectFirst>
          <MUIThemeProvider theme={theme}>
              <ThemeProvider theme={theme}>
                    <CssBaseline/>
                    {Story()}
              </ThemeProvider>
          </MUIThemeProvider>
        </StyledEngineProvider>
    )
  }
];

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}
