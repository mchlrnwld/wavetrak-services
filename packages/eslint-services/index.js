module.exports = {
  root: true,
  extends: ['airbnb-base', 'prettier'],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  parser: '@babel/eslint-parser',
  globals: {
    it: true,
    describe: true,
    beforeEach: true,
    afterEach: true,
    before: true,
    after: true,
  },
  env: {
    node: true,
    es6: true,
  },
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': 'error',
    'no-underscore-dangle': [2, { allow: ['_id'] }],
    'import/no-extraneous-dependencies': [
      2,
      {
        devDependencies: [
          '**/dev-index.js',
          '**/babel-index.js',
          '**/babel-tests.js',
          '**/tests.js',
          '**/*.spec.js',
          '**/pact/**/*.js',
        ],
      },
    ],
    'import/prefer-default-export': 'off',
    'no-shadow': [2, { allow: ['_'] }],
    yoda: [2, 'never', { exceptRange: true }],
    // This gives us comma dangles on any multiline code
    // in order to make commit history cleaner
    'comma-dangle': [
      2,
      {
        arrays: 'always-multiline',
        objects: 'always-multiline',
        imports: 'always-multiline',
        exports: 'always-multiline',
        functions: 'always-multiline',
      },
    ],
  },
};
