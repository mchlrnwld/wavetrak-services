# `@surfline/eslint-config-services`

This package stores the eslint configs for Surfline Services.

## Usage

There are two different configurations available in this package. The base configuration is for Javascript files (`.js` extension) and the Typescript configuration is for Typescript files (`.ts` extension).

Both configurations can be include at the same time and will not overlap each other (which is helpful for partial TS migrations).

In order to use the configurations, they can be consumed as follows.

#### Base configuration (without Typescript)

```json
// .eslintrc
{
  "extends": ["@surfline/eslint-config-services"]
}
```

#### Typescript configuration (without Javascript support)

```json
// .eslintrc
{
  "extends": ["@surfline/eslint-config-services/typescript"]
}
```
#### Typescript and Javascript Support

```json
// .eslintrc
{
  "extends": ["@surfline/eslint-config-services", "@surfline/eslint-config-services/typescript"]
}
```


## What does this config include?

The base configuration includes the following:
- [Airbnb Base ESLint configuration](https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb-base)
- [Prettier ESLint configuration](https://github.com/prettier/eslint-config-prettier)
- Some basic overrides for rules based on conventions established at Surfline

The typescript configuration includes:
- [Prettier ESLint configuration for](https://github.com/prettier/eslint-config-prettier)
- [Typescript ESLint plugin](https://github.com/typescript-eslint/typescript-eslint#readme)

