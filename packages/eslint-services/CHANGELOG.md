# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [0.2.10](https://github.com/Surfline/wavetrak-services/compare/@surfline/eslint-config-services@0.2.9...@surfline/eslint-config-services@0.2.10) (2022-02-14)


### Bug Fixes

* services config not using the correct eslint parser ([0119796](https://github.com/Surfline/wavetrak-services/commit/01197968e6385f7213773a3e8971369dceb0b3a8))



### [0.2.9](https://github.com/Surfline/wavetrak-services/compare/@surfline/eslint-config-services@0.2.8...@surfline/eslint-config-services@0.2.9) (2022-02-07)


### Chores

* bump package versions ([03203b5](https://github.com/Surfline/wavetrak-services/commit/03203b5e07de4b17266434c20d35329ed433b8d6))



## <small>0.2.8 (2021-10-13)</small>

* chore(eslint-services): move to packages and update CODEOWNERS ([f7e811f](https://github.com/Surfline/wavetrak-services/commit/f7e811f))
* chore(deps): bump eslint-config-airbnb-base from 14.1.0 to 14.2.1 (#5059) ([bdbf820](https://github.com/Surfline/wavetrak-services/commit/bdbf820)), closes [#5059](https://github.com/Surfline/wavetrak-services/issues/5059)
* chore(deps): bump eslint-plugin-import from 2.20.1 to 2.24.2 (#4892) ([d33d5bb](https://github.com/Surfline/wavetrak-services/commit/d33d5bb)), closes [#4892](https://github.com/Surfline/wavetrak-services/issues/4892)




## <small>0.2.7 (2020-11-04)</small>

* PS-000 - Replace artifactoryonline.com references with jfrog.io. (#3664) ([21430cd](https://github.com/Surfline/wavetrak-services/commit/21430cd)), closes [#3664](https://github.com/Surfline/wavetrak-services/issues/3664)





## <small>0.2.6 (2020-05-12)</small>

* chore: update eslint-services config dependencies ([12119c0](https://github.com/Surfline/wavetrak-services/commit/12119c0))





## <small>0.2.5 (2020-04-23)</small>

**Note:** Version bump only for package @surfline/eslint-config-services





## [0.2.4](https://github.com/Surfline/wavetrak-services/compare/@surfline/eslint-config-services@0.2.3...@surfline/eslint-config-services@0.2.4) (2020-03-18)


### Bug Fixes

* Create a tagging script for Lerna ([9812c76](https://github.com/Surfline/wavetrak-services/commit/9812c76d618b9f54ba34403cf2d026fb983ea856))





## 0.2.3 (2020-03-18)

*### Bug Fixes

* Fix Comma Dangles not being properly applied to multi-line functions ([9bdcbfc](https://github.com/Surfline/wavetrak-services/commit/9bdcbfcf11470c04935e6d63c40b64f9b7d2d591))





## 0.2.2 (2020-03-11)

**Note:** Version bump only for package @surfline/eslint-config-services





## 0.2.1 (2020-03-09)

**Note:** Version bump only for package @surfline/eslint-config-services





# 0.2.0 (2020-03-04)


### Bug Fixes

* Add .tsx files to import parser ([1833500](https://github.com/Surfline/wavetrak-services/commit/1833500c9f28f65b9bbdba0dafc29c3274f16306))
* add .tsx files to linting ([8c89445](https://github.com/Surfline/wavetrak-services/commit/8c89445e1936eb2e9d5523539bb79fcdbd7d7daa))
* add missing modules, fix missing import plugin ([47db11c](https://github.com/Surfline/wavetrak-services/commit/47db11cf95ef4895abb9720e5d78aced90e72887))
* add missing packages for typescript ([90f5683](https://github.com/Surfline/wavetrak-services/commit/90f5683126a103a3b990a95f7bc102e5579548b7))


### Features

* Add typescript configuration eslint-services package ([5c4f5ac](https://github.com/Surfline/wavetrak-services/commit/5c4f5ac0ae314da13932abae2a7628ca6127608f))





## 0.1.1 (2020-03-04)

**Note:** Version bump only for package @surfline/eslint-config-services





# 0.1.0 (2020-02-25)


### Bug Fixes

* fix package versions ([b52885d](https://github.com/Surfline/wavetrak-services/commit/b52885dc221b7c16fc541da550b58457e687eed2))
* README fixes, fix wrong repo link ([49a4329](https://github.com/Surfline/wavetrak-services/commit/49a4329c15885385fc7c7ca5bdb0ace50f4ff0b7))


### Features

* Create Eslint Services Package ([f536e37](https://github.com/Surfline/wavetrak-services/commit/f536e373bcfa74ddc40d3a680b168e897d174441))
