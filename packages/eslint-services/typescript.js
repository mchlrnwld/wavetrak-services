module.exports = {
  root: true,
  // We use overrides here so that we can apply these rules only to typescript files
  // but also so that we can use these in tandem with JS files for places that have both
  overrides: [
    {
      files: ['**/*.ts', '**/*.tsx'],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {
          modules: true,
        },
      },
      extends: ['plugin:@typescript-eslint/recommended', 'plugin:import/typescript', 'prettier'],
      plugins: ['@typescript-eslint', 'import', 'prettier'],
      settings: {
        'import/parsers': {
          '@typescript-eslint/parser': ['.ts', '.tsx'],
        },
        // Allows using typescript to resolve modules
        'import/resolver': {
          typescript: {},
        },
      },
      rules: {
        // turn on errors for missing imports
        'import/no-unresolved': 'error',
        '@typescript-eslint/no-unused-vars': [
          'error',
          {
            vars: 'all',
            args: 'after-used',
            ignoreRestSiblings: true,
            argsIgnorePattern: '^_',
          },
        ],
        'import/extensions': [
          'error',
          'ignorePackages',
          {
            js: 'never',
            jsx: 'never',
            ts: 'never',
            tsx: 'never',
          },
        ],
        'import/no-extraneous-dependencies': [
          2,
          {
            devDependencies: ['**/*.spec.ts'],
          },
        ],
      },
    },
  ],
};
