import React from 'react';
import PropTypes from 'prop-types';

/**
 * @param {object} props
 * @param {string} props.appId
 * @returns {JSX.Element}
 */
const FacebookBrowser = ({ appId }) => {
  const fbStr = `window.fbAsyncInit = function() {
    FB.init({
      appId: ${appId},
      xfbml: true,
      version: 'v2.12'
    });
  };

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));`;

  // eslint-disable-next-line react/no-danger
  return <script dangerouslySetInnerHTML={{ __html: fbStr }} />;
};

FacebookBrowser.propTypes = {
  appId: PropTypes.string.isRequired,
};

export default FacebookBrowser;
