import React from 'react';
import CamOverlay from './index';

const parentContainerStyles = {
  width: '500px',
  height: '500px',
};
const ChildComponent = () => (
  <div
    style={{
      width: '200px',
      height: '200px',
      padding: '15px',
      background: 'white',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      zIndex: '100',
    }}
  >
    Sample Content
  </div>
);

export default {
  title: 'CamOverlay',
};

export const Default = () => (
  <div style={parentContainerStyles}>
    <CamOverlay backgroundImage="url(https://creatives.cdn-surfline.com/promotions/perks/surfnetwork.jpg)">
      <ChildComponent />
    </CamOverlay>
  </div>
);

Default.story = {
  name: 'default',
};
