import React from 'react';
import PropTypes from 'prop-types';

const CamOverlay = ({ children, backgroundImage }) => (
  <div style={{ backgroundImage }} className="quiver-cam-overlay">
    {children}
  </div>
);

CamOverlay.propTypes = {
  backgroundImage: PropTypes.string,
  children: PropTypes.node.isRequired,
};

CamOverlay.defaultProps = {
  backgroundImage: null,
};

export default CamOverlay;
