import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import CamOverlay from './CamOverlay';

describe('CamOverlay', () => {
  const props = {
    backgroundImage: 'url("https://www.surfline.com")',
  };
  const wrapper = mount(<CamOverlay backgroundImage={props.backgroundImage} />);

  it('should render a div with a class name and inline bg image', () => {
    expect(wrapper.find('.quiver-cam-overlay')).to.have.length(1);
  });

  it('should include an inline styled bg image', () => {
    expect(wrapper.html()).to.contain(
      'style="background-image: url(https://www.surfline.com);"'
    );
  });
});
