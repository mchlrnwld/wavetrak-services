import React from 'react';
import LoginForm from './index';

export default {
  title: 'LoginForm',
};

export const Default = () => (
  <LoginForm
    onSubmit={(values) => console.log(values)}
    loading={false}
    error={false}
  />
);

Default.story = {
  name: 'default',
};
