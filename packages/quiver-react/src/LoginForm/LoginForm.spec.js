/* eslint-disable no-unused-expressions */
import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import LoginForm from './LoginForm';

describe('LoginForm', () => {
  it('should render a login form with defaulted props', () => {
    const wrapper = shallow(
      <LoginForm
        onSubmit={(values) => console.log(values)}
        loading={false}
        error={null}
      />
    );
    expect(wrapper.find('input').first().props().id).to.equal('staySignedIn');

    expect(wrapper.prop('onSubmit')).to.exist;
    expect(wrapper.prop('loading')).to.be.false;
    expect(wrapper.prop('error')).to.not.exist;
    expect(wrapper.find('form').prop('onSubmit')).to.exist;
  });
});
