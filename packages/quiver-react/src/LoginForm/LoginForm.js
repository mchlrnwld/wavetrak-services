import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import Button from '../Button';
import Input from '../Input';
import Alert from '../Alert';

/**
 * @function LoginForm
 * @param {object} props
 * @param {(data: object, e: Event) => void} props.onSubmit
 * @param {boolean} props.loading
 * @param {string} props.error
 * @return {JSX.Element}
 */
const LoginForm = ({ onSubmit, loading, error }) => {
  const [email, setEmail] = useState(undefined);
  const [password, setPassword] = useState(undefined);
  const [staySignedIn, setStaySignedIn] = useState(true);
  const [emailErr, setEmailErr] = useState(null);
  const [passwordErr, setPasswordErr] = useState(null);

  const isValid =
    !(emailErr || passwordErr) && email?.length && password?.length;

  const { register, handleSubmit, errors } = useForm();

  return (
    <div className="login-form">
      {error ? <Alert type="error">{error}</Alert> : null}
      <form onSubmit={handleSubmit(onSubmit)}>
        <Input
          value={email}
          label="EMAIL"
          id="email"
          parentWidth
          error={!!errors.email}
          message={errors?.email?.message}
          onError={setEmailErr}
          email
          required
          type="email"
          input={{
            name: 'email',
            onChange: (event) => setEmail(event.target.value),
            ref: register({
              required: true,
            }),
            required: true,
            autoComplete: 'on',
          }}
          style={{ marginBottom: '20px' }}
        />
        <Input
          value={password}
          label="PASSWORD"
          id="password"
          type="password"
          parentWidth
          error={!!errors.password}
          message={errors?.password?.message}
          onError={setPasswordErr}
          isLogin
          password
          input={{
            name: 'password',
            onChange: (event) => setPassword(event.target.value),
            ref: register({ required: true }),
            required: true,
            autoComplete: 'on',
          }}
        />
        <a
          className="forgot-password--link"
          href="/forgot-password"
          target="_blank"
          rel="noopener noreferrer"
        >
          Forgot Password?
        </a>
        <Button
          disabled={!isValid}
          style={{ width: '100%' }}
          loading={loading}
          button={{ type: 'submit' }}
        >
          SIGN IN
        </Button>
        <div className="login-form__checkbox">
          <input
            type="checkbox"
            id="staySignedIn"
            name="staySignedIn"
            ref={register}
            value={staySignedIn}
            defaultChecked={staySignedIn}
            onClick={() => setStaySignedIn(!staySignedIn)}
          />
          <span>Keep me signed in</span>
        </div>
      </form>
    </div>
  );
};

LoginForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  error: PropTypes.string,
};

LoginForm.defaultProps = {
  loading: false,
  error: null,
};

export default LoginForm;
