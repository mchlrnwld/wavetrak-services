import React from 'react';
import worldTaxonomy from './fixtures/worldTaxonomy.json';
import WorldTaxonomy from './WorldTaxonomy';

export default {
  title: 'WorldTaxonomy',
};

export const Default = () => (
  <WorldTaxonomy
    taxonomy={worldTaxonomy}
    countryComponent={({ name, slug }) => (
      <a href={slug}>{name} Cams &amp; Forecasts</a>
    )}
    headerSuffix="Cams &amp; Forecasts"
  />
);

Default.story = {
  name: 'default',
};
