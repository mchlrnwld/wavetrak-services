/** @prettier */

import React from 'react';
import PropTypes from 'prop-types';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import worldTaxonomy from './fixtures/worldTaxonomy.json';
import WorldTaxonomy from './WorldTaxonomy';

describe('WorldTaxonomy', () => {
  const countryComponent = ({ name, slug }) => (
    <a className="country-component" href={slug}>
      {name}
    </a>
  );
  countryComponent.propTypes = {
    name: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired,
  };
  const props = {
    taxonomy: worldTaxonomy,
    countryComponent,
    headerSuffix: 'Cams &amp; Forecasts',
  };

  before(() => {
    sinon.stub(window, 'addEventListener');
    sinon.stub(window, 'removeEventListener');
  });

  afterEach(() => {
    window.addEventListener.reset();
    window.removeEventListener.reset();
  });

  after(() => {
    window.addEventListener.restore();
    window.removeEventListener.restore();
  });

  it('should render headers with header suffix', () => {
    const wrapper = mount(<WorldTaxonomy {...props} />);

    const h1Header = wrapper.find('h1');
    expect(h1Header).to.have.length(1);
    expect(h1Header.text()).to.contain('Cams &amp; Forecasts');

    const h2Headers = wrapper.find('h2');
    expect(h2Headers).to.have.length(6);
    h2Headers.forEach((header) => {
      expect(header.text()).to.contain('Cams &amp; Forecasts');
    });
  });

  it('should render links to countries grouped by continent and letter', () => {
    const wrapper = mount(<WorldTaxonomy {...props} />);
    const continents = wrapper.find('.quiver-world-taxonomy__continent');
    expect(continents).to.have.length(worldTaxonomy.length);
    continents.forEach((continent, continentIndex) => {
      const continentTaxonomy = worldTaxonomy[continentIndex];
      expect(continent.find('h2').text()).to.equal(
        `${worldTaxonomy[continentIndex].name} Cams &amp; Forecasts`
      );

      const letters = continent.find('.quiver-world-taxonomy__letter');
      expect(letters).to.have.length(continentTaxonomy.countries.length);
      letters.forEach((letter, letterIndex) => {
        const letterTaxonomy = continentTaxonomy.countries[letterIndex];
        expect(letter.find('h3').text()).to.equal(letterTaxonomy.key);

        const countries = letter.find('.country-component');
        expect(countries).to.have.length(letterTaxonomy.countries.length);
        countries.forEach((country, countryIndex) => {
          const countryTaxonomy = letterTaxonomy.countries[countryIndex];
          expect(country.prop('href')).to.deep.equal(countryTaxonomy.slug);
          expect(country.text()).to.equal(countryTaxonomy.name);
        });
      });
    });
  });

  it('should render a list of continents twice (normal and fixed)', () => {
    const wrapper = mount(<WorldTaxonomy {...props} />);
    const continentsLists = wrapper.find('.quiver-world-taxonomy__continents');
    expect(continentsLists).to.have.length(2);
    const first = continentsLists.at(0);
    const second = continentsLists.at(1);
    expect(first.find('ul').children()).to.deep.equal(
      second.find('ul').children()
    );
    expect(first.prop('className')).not.to.contain(
      'quiver-world-taxonomy__continents--fixed'
    );
    expect(second.prop('className')).to.contain(
      'quiver-world-taxonomy__continents--fixed'
    );
  });

  it('should check if continents list should be fixed and which continent is active', () => {
    const wrapper = mount(<WorldTaxonomy {...props} />);
    const instance = wrapper.instance();
    const scrollCalls = window.addEventListener
      .getCalls()
      .filter((call) => call.args.indexOf('scroll') > -1);
    expect(scrollCalls).to.have.length(2);
    expect(scrollCalls[0]).to.have.been.calledWith(
      'scroll',
      instance.checkFixContinentsList
    );
    expect(scrollCalls[1]).to.have.been.calledWith(
      'scroll',
      instance.checkActiveContinent
    );

    let removeCalls = window.removeEventListener
      .getCalls()
      .filter((call) => call.args.indexOf('scroll') > -1);
    expect(removeCalls).to.have.length(0);
    wrapper.unmount();
    removeCalls = window.removeEventListener
      .getCalls()
      .filter((call) => call.args.indexOf('scroll') > -1);
    expect(removeCalls).to.have.length(2);
    expect(removeCalls[0]).to.have.been.calledWithExactly(
      'scroll',
      instance.checkFixContinentsList
    );
    expect(removeCalls[1]).to.have.been.calledWithExactly(
      'scroll',
      instance.checkActiveContinent
    );

    mount(<WorldTaxonomy {...props} />);
  });
});
