import React, { Component } from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';

const linkPropType = PropTypes.shape({
  key: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
});

const taxonomyPropTypes = {
  _id: PropTypes.string.isRequired,
  geonameId: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  links: PropTypes.arrayOf(linkPropType),
};

class WorldTaxonomy extends Component {
  constructor() {
    super();
    this.continentRefs = new Map();
    this.state = {
      fixContinentsList: false,
      activeContinent: null,
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.checkFixContinentsList);
    window.addEventListener('scroll', this.checkActiveContinent);
    this.checkFixContinentsList();
    this.checkActiveContinent();
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.checkFixContinentsList);
    window.removeEventListener('scroll', this.checkActiveContinent);
  }

  containerClass = 'quiver-world-taxonomy__container';

  checkFixContinentsList = () => {
    this.setState({
      fixContinentsList: this.continentsRef.getBoundingClientRect().top <= 0,
    });
  };

  checkActiveContinent = () => {
    const continents = [...this.continentRefs].map(([geonameId, ref]) => ({
      geonameId,
      top: ref.getBoundingClientRect().top,
    }));

    const activeContinentIndex =
      continents.findIndex(
        ({ top }) => top >= this.fixedContinentsRef.offsetHeight
      ) - 1;
    const activeContinent =
      activeContinentIndex === -2
        ? continents[continents.length - 1].geonameId
        : continents[Math.max(0, activeContinentIndex)].geonameId;

    this.setState({ activeContinent });
  };

  className = () => {
    const { fixContinentsList } = this.state;
    return classNames({
      'quiver-world-taxonomy': true,
      'quiver-world-taxonomy--fixed': fixContinentsList,
    });
  };

  activeContinentClassName = (geonameId) => {
    const { activeContinent } = this.state;
    return activeContinent === geonameId
      ? 'quiver-world-taxonomy__continents__active'
      : null;
  };

  continentsList = (fixed) => {
    const { taxonomy } = this.props;
    return (
      <ul className={fixed ? this.containerClass : null}>
        {taxonomy.map((continent) => (
          <li
            key={continent.geonameId}
            className={this.activeContinentClassName(continent.geonameId)}
          >
            <a href={`#${continent.slug}`}>{continent.name}</a>
          </li>
        ))}
      </ul>
    );
  };

  render() {
    const { taxonomy, countryComponent, headerSuffix } = this.props;
    return (
      <div className={this.className()}>
        <div className={this.containerClass}>
          <h1>
            World
            {headerSuffix}
          </h1>
          <div
            ref={(ref) => {
              this.continentsRef = ref;
            }}
            className="quiver-world-taxonomy__continents"
          >
            {this.continentsList(false)}
          </div>
          <div
            ref={(ref) => {
              this.fixedContinentsRef = ref;
            }}
            className="quiver-world-taxonomy__continents quiver-world-taxonomy__continents--fixed"
          >
            {this.continentsList(true)}
          </div>
        </div>
        {taxonomy.map((continent) => (
          <div
            key={continent.geonameId}
            className="quiver-world-taxonomy__continent"
          >
            <h2
              ref={(ref) => {
                this.continentRefs.set(continent.geonameId, ref);
              }}
              id={continent.slug}
              className={this.containerClass}
            >
              {continent.name} {headerSuffix}
            </h2>
            <div className="quiver-world-taxonomy__countries">
              <ul className={this.containerClass}>
                {continent.countries.map((letter) => (
                  <li key={letter.key}>
                    <div className="quiver-world-taxonomy__letter">
                      <h3>{letter.key}</h3>
                      <ul>
                        {letter.countries.map((country) => (
                          <li key={country.geonameId}>
                            {countryComponent(country)}
                          </li>
                        ))}
                      </ul>
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        ))}
      </div>
    );
  }
}

WorldTaxonomy.propTypes = {
  taxonomy: PropTypes.arrayOf(
    PropTypes.shape({
      ...taxonomyPropTypes,
      countries: PropTypes.arrayOf(
        PropTypes.shape({
          key: PropTypes.string.isRequired,
          countries: PropTypes.arrayOf(PropTypes.shape(taxonomyPropTypes))
            .isRequired,
        })
      ).isRequired,
    })
  ).isRequired,
  countryComponent: PropTypes.func.isRequired,
  headerSuffix: PropTypes.string,
};

WorldTaxonomy.defaultProps = {
  headerSuffix: null,
};

export default WorldTaxonomy;
