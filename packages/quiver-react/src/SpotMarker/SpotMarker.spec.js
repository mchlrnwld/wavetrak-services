import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SurfHeight from '../SurfHeight';
import SpotMarker from './SpotMarker';

describe('SpotMarker', () => {
  it('should map conditions to class name', () => {
    const defaultWrapper = shallow(<SpotMarker surf={{}} />);
    const fairToGoodWrapper = shallow(
      <SpotMarker conditions="FAIR_TO_GOOD" surf={{}} />
    );
    const epicWrapper = shallow(<SpotMarker conditions="EPIC" surf={{}} />);

    const defaultContainer = defaultWrapper.find(
      '.quiver-spot-marker__container'
    );
    const fairToGoodContainer = fairToGoodWrapper.find(
      '.quiver-spot-marker__container'
    );
    const epicContainer = epicWrapper.find('.quiver-spot-marker__container');

    const lolaClass = 'quiver-spot-marker__container--lola';
    const fairToGoodClass = 'quiver-spot-marker__container--fair-to-good';
    const epicClass = 'quiver-spot-marker__container--epic';

    expect(defaultContainer.hasClass(lolaClass)).to.be.true();
    expect(fairToGoodContainer.hasClass(fairToGoodClass)).to.be.true();
    expect(epicContainer.hasClass(epicClass)).to.be.true();
  });

  it('should add highlight modifier class if highlight is true', () => {
    const defaultWrapper = shallow(<SpotMarker surf={{}} />);
    const highlightWrapper = shallow(<SpotMarker highlight surf={{}} />);
    const defaultContainer = defaultWrapper.find(
      '.quiver-spot-marker__container'
    );
    const highlightContainer = highlightWrapper.find(
      '.quiver-spot-marker__container'
    );
    expect(
      defaultContainer.hasClass('quiver-spot-marker__container--highlight')
    ).to.be.false();
    expect(
      highlightContainer.hasClass('quiver-spot-marker__container--highlight')
    ).to.be.true();
  });

  it('should display surf height', () => {
    const wrapper = shallow(
      <SpotMarker surf={{ min: 2, max: 3, units: 'ft' }} />
    );
    const surfHeightWrapper = wrapper.find(SurfHeight);
    expect(surfHeightWrapper.prop('min')).to.equal(2);
    expect(surfHeightWrapper.prop('max')).to.equal(3);
    expect(surfHeightWrapper.prop('units')).to.equal('ft');
  });

  it('should display camera icon if spot has camera', () => {
    const defaultWrapper = shallow(<SpotMarker surf={{}} />);
    const cameraWrapper = shallow(<SpotMarker hasCamera surf={{}} />);
    expect(
      defaultWrapper.find('.quiver-spot-marker__camera').exists()
    ).to.be.false();
    expect(
      cameraWrapper.find('.quiver-spot-marker__camera').exists()
    ).to.be.true();
  });

  it('should pass custom style object into children', () => {
    const style = {
      fontFamily: 'parent',
      container: {
        fontFamily: 'container',
      },
      main: {
        fontFamily: 'main',
      },
      surf: {
        fontFamily: 'surf',
      },
      camera: {
        fontFamily: 'camera',
      },
    };
    const wrapper = shallow(<SpotMarker surf={{}} hasCamera style={style} />);
    const parentWrapper = wrapper.find('.quiver-spot-marker');
    const containerWrapper = parentWrapper.find(
      '.quiver-spot-marker__container'
    );
    const mainWrapper = containerWrapper.find('.quiver-spot-marker__main');
    const surfWrapper = mainWrapper.find('.quiver-spot-marker__surf');
    const cameraWrapper = mainWrapper.find('.quiver-spot-marker__camera');

    expect(parentWrapper.prop('style')).to.equal(style);
    expect(containerWrapper.prop('style')).to.equal(style.container);
    expect(mainWrapper.prop('style')).to.equal(style.main);
    expect(surfWrapper.prop('style')).to.equal(style.surf);
    expect(cameraWrapper.prop('style')).to.equal(style.camera);
  });
});
