import React from 'react';
import SpotMarker from './index';

export default {
  title: 'SpotMarker',
};

export const Default = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

Default.story = {
  name: 'default',
};

export const Lola = () => (
  <div>
    <SpotMarker
      surf={{
        min: 0,
        max: 0,
        units: 'ft',
      }}
      conditions="LOLA"
    />
    <SpotMarker
      surf={{
        min: 0,
        max: 0,
        units: 'ft',
      }}
      conditions="LOLA"
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

Lola.story = {
  name: 'lola',
};

export const None = () => (
  <div>
    <SpotMarker
      surf={{
        min: 0,
        max: 0,
        units: 'ft',
      }}
      conditions="NONE"
    />
    <SpotMarker
      surf={{
        min: 0,
        max: 0,
        units: 'ft',
      }}
      conditions="NONE"
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

None.story = {
  name: 'none',
};

export const Flat = () => (
  <div>
    <SpotMarker
      surf={{
        min: 0,
        max: 0,
        units: 'ft',
      }}
      conditions="FLAT"
    />
    <SpotMarker
      surf={{
        min: 0,
        max: 0,
        units: 'ft',
      }}
      conditions="FLAT"
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

Flat.story = {
  name: 'flat',
};

export const VeryPoor = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="VERY_POOR"
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="VERY_POOR"
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

VeryPoor.story = {
  name: 'very poor',
};

export const Poor = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="POOR"
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="POOR"
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

Poor.story = {
  name: 'poor',
};

export const PoorToFair = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="POOR_TO_FAIR"
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="POOR_TO_FAIR"
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

PoorToFair.story = {
  name: 'poor to fair',
};

export const Fair = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="FAIR"
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="FAIR"
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

Fair.story = {
  name: 'fair',
};

export const FairToGood = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="FAIR_TO_GOOD"
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="FAIR_TO_GOOD"
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

FairToGood.story = {
  name: 'fair to good',
};

export const Good = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="GOOD"
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="GOOD"
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

Good.story = {
  name: 'good',
};

export const VeryGood = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="VERY_GOOD"
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="VERY_GOOD"
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

VeryGood.story = {
  name: 'very good',
};

export const GoodToEpic = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="GOOD_TO_EPIC"
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="GOOD_TO_EPIC"
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

GoodToEpic.story = {
  name: 'good to epic',
};

export const Epic = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="EPIC"
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="EPIC"
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

Epic.story = {
  name: 'epic',
};

export const WithCamera = () => (
  <div>
    <SpotMarker
      surf={{
        min: 2,
        max: 3,
        units: 'ft',
      }}
      conditions="fair"
      hasCamera
    />
    <SpotMarker
      surf={{
        min: 2,
        max: 3,
        units: 'ft',
      }}
      conditions="fair"
      hasCamera
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

WithCamera.story = {
  name: 'with camera',
};

export const LargeSurfWithCamera = () => (
  <div>
    <SpotMarker
      surf={{
        min: 10,
        max: 12,
        units: 'ft',
      }}
      conditions="epic"
      hasCamera
    />
    <SpotMarker
      surf={{
        min: 10,
        max: 12,
        units: 'ft',
      }}
      conditions="epic"
      hasCamera
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

LargeSurfWithCamera.story = {
  name: 'large surf with camera',
};

export const AbsolutelyPositionedForUseOnMaps = () => (
  <div style={{ position: 'absolute', height: 500 }}>
    <div style={{ position: 'absolute', top: 20, left: 20 }}>
      <SpotMarker
        surf={{
          min: 2,
          max: 3,
          units: 'ft',
        }}
        conditions="fair"
        hasCamera
      />
    </div>
    <div style={{ position: 'absolute', top: 60, left: 20 }}>
      <SpotMarker
        surf={{
          min: 2,
          max: 3,
          units: 'ft',
        }}
        conditions="fair"
        hasCamera
        highlight
      />
    </div>
  </div>
);

AbsolutelyPositionedForUseOnMaps.story = {
  name: 'absolutely positioned (for use on maps)',
};

export const DefaultV2 = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      sevenRatings
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      highlight
      sevenRatings
      style={{ marginTop: 10 }}
    />
  </div>
);

DefaultV2.story = {
  name: 'default v2',
};

export const LotusV2 = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="LOTUS"
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="LOTUS"
      highlight
      style={{ marginTop: 10 }}
    />
  </div>
);

LotusV2.story = {
  name: 'lotus v2',
};

export const NoneV2 = () => (
  <div>
    <SpotMarker
      surf={{
        min: 0,
        max: 0,
        units: 'ft',
      }}
      conditions="NONE"
      sevenRatings
    />
    <SpotMarker
      surf={{
        min: 0,
        max: 0,
        units: 'ft',
      }}
      conditions="NONE"
      highlight
      sevenRatings
      style={{ marginTop: 10 }}
    />
  </div>
);

NoneV2.story = {
  name: 'none v2 (maps to very poor)',
};

export const FlatV2 = () => (
  <div>
    <SpotMarker
      surf={{
        min: 0,
        max: 0,
        units: 'ft',
      }}
      conditions="FLAT"
      sevenRatings
    />
    <SpotMarker
      surf={{
        min: 0,
        max: 0,
        units: 'ft',
      }}
      conditions="FLAT"
      highlight
      sevenRatings
      style={{ marginTop: 10 }}
    />
  </div>
);

FlatV2.story = {
  name: 'flat v2 (maps to very poor)',
};

export const VeryPoorV2 = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="VERY_POOR"
      sevenRatings
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="VERY_POOR"
      highlight
      sevenRatings
      style={{ marginTop: 10 }}
    />
  </div>
);

VeryPoorV2.story = {
  name: 'very poor v2',
};

export const PoorV2 = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="POOR"
      sevenRatings
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="POOR"
      highlight
      sevenRatings
      style={{ marginTop: 10 }}
    />
  </div>
);

PoorV2.story = {
  name: 'poor v2',
};

export const PoorToFairV2 = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="POOR_TO_FAIR"
      sevenRatings
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="POOR_TO_FAIR"
      highlight
      sevenRatings
      style={{ marginTop: 10 }}
    />
  </div>
);

PoorToFairV2.story = {
  name: 'poor to fair v2',
};

export const FairV2 = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="FAIR"
      sevenRatings
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="FAIR"
      highlight
      sevenRatings
      style={{ marginTop: 10 }}
    />
  </div>
);

FairV2.story = {
  name: 'fair v2',
};

export const FairToGoodV2 = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="FAIR_TO_GOOD"
      sevenRatings
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="FAIR_TO_GOOD"
      highlight
      sevenRatings
      style={{ marginTop: 10 }}
    />
  </div>
);

FairToGoodV2.story = {
  name: 'fair to good v2',
};

export const GoodV2 = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="GOOD"
      sevenRatings
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="GOOD"
      highlight
      sevenRatings
      style={{ marginTop: 10 }}
    />
  </div>
);

GoodV2.story = {
  name: 'good v2',
};

export const VeryGoodV2 = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="VERY_GOOD"
      sevenRatings
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="VERY_GOOD"
      highlight
      sevenRatings
      style={{ marginTop: 10 }}
    />
  </div>
);

VeryGoodV2.story = {
  name: 'very good v2 (maps to good)',
};

export const GoodToEpicV2 = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="GOOD_TO_EPIC"
      sevenRatings
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="GOOD_TO_EPIC"
      highlight
      sevenRatings
      style={{ marginTop: 10 }}
    />
  </div>
);

GoodToEpicV2.story = {
  name: 'good to epic v2 (maps to good)',
};

export const EpicV2 = () => (
  <div>
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="EPIC"
      sevenRatings
    />
    <SpotMarker
      surf={{
        min: 1,
        max: 2,
        units: 'ft',
      }}
      conditions="EPIC"
      highlight
      sevenRatings
      style={{ marginTop: 10 }}
    />
  </div>
);

EpicV2.story = {
  name: 'epic v2',
};
