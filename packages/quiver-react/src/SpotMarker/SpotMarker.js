import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';
import CamIcon from '../icons/CamIcon';
import SurfHeight from '../SurfHeight';
import applyConditionVariation from '../helpers/applyConditionVariation';

const containerClassName = (conditions, highlight, sevenRatings = false) => {
  const classObject = {};
  const transformedConditions = (
    sevenRatings ? applyConditionVariation(conditions) : conditions
  )
    .replace(/_/g, '-')
    .toLowerCase();
  classObject['quiver-spot-marker__container'] = true;
  classObject[
    `quiver-spot-marker__container${
      sevenRatings ? '-v2' : ''
    }--${transformedConditions}`
  ] = true;
  classObject['quiver-spot-marker__container--highlight'] = highlight;
  return classNames(classObject);
};

const SpotMarker = ({
  conditions,
  surf: { min, max, units },
  hasCamera,
  highlight,
  sevenRatings,
  style,
}) => (
  <div className="quiver-spot-marker" style={style}>
    <div
      className={containerClassName(conditions, highlight, sevenRatings)}
      style={style && style.container}
    >
      <div className="quiver-spot-marker__main" style={style && style.main}>
        <div className="quiver-spot-marker__surf" style={style && style.surf}>
          <SurfHeight min={min} max={max} units={units} />
        </div>
        {hasCamera ? (
          <div
            className="quiver-spot-marker__camera"
            style={style && style.camera}
          >
            <CamIcon />
          </div>
        ) : null}
      </div>
    </div>
  </div>
);

SpotMarker.propTypes = {
  surf: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number,
    units: PropTypes.string,
  }).isRequired,
  conditions: PropTypes.string,
  hasCamera: PropTypes.bool,
  highlight: PropTypes.bool,
  sevenRatings: PropTypes.bool,
  style: PropTypes.shape({
    camera: PropTypes.shape({}),
    container: PropTypes.shape({}),
    main: PropTypes.shape({}),
    surf: PropTypes.shape({}),
  }),
};

SpotMarker.defaultProps = {
  conditions: 'LOLA',
  hasCamera: false,
  highlight: false,
  sevenRatings: false,
  style: {},
};

export default SpotMarker;
