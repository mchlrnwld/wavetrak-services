import PropTypes from 'prop-types';
import React from 'react';
import WindIndicator from '../WindIndicator';
import PlayMarker from '../icons/PlayMarker';
import unitsPropType from '../PropTypes/units';

const SpotThumbnail = ({ windDirection, windSpeed, units, hasCamera }) => (
  <div className="quiver-spot-thumbnail">
    <div className="quiver-spot-thumbnail__top-container">
      <WindIndicator
        direction={windDirection}
        speed={windSpeed}
        units={units.windSpeed}
      />
    </div>
    <div className="quiver-spot-thumbnail__bottom-container">
      {hasCamera ? <PlayMarker /> : null}
    </div>
  </div>
);

SpotThumbnail.propTypes = {
  windDirection: PropTypes.number.isRequired,
  windSpeed: PropTypes.number.isRequired,
  units: unitsPropType.isRequired,
  hasCamera: PropTypes.bool,
};

SpotThumbnail.defaultProps = {
  hasCamera: false,
};

export default SpotThumbnail;
