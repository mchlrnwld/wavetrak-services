import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import deepFreeze from 'deep-freeze';
import WindIndicator from '../WindIndicator';
import PlayMarker from '../icons/PlayMarker';
import SpotThumbnail from './SpotThumbnail';

describe('components / SpotThumbnail', () => {
  const props = deepFreeze({
    windSpeed: 10,
    windDirection: 275,
    units: {
      windSpeed: 'KT',
    },
  });

  it('passes wind data into WindIndicator', () => {
    const wrapper = shallow(<SpotThumbnail {...props} />);
    const windIndicator = wrapper.find(WindIndicator);
    expect(windIndicator.prop('direction')).to.equal(props.windDirection);
    expect(windIndicator.prop('speed')).to.equal(props.windSpeed);
    expect(windIndicator.prop('units')).to.equal(props.units.windSpeed);
  });

  it('renders PlayMarker if hasCamera is true', () => {
    const wrapper = shallow(<SpotThumbnail {...props} />);
    const cameraWrapper = shallow(<SpotThumbnail {...props} hasCamera />);
    expect(wrapper.find(PlayMarker)).to.have.length(0);
    expect(cameraWrapper.find(PlayMarker)).to.have.length(1);
  });
});
