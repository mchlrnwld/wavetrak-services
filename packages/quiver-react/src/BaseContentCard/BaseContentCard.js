import React from 'react';
import PropTypes from 'prop-types';

const BaseContentCard = ({ meta, tags, onClickTag, showPaywall, children }) => (
  <div className="quiver-base-content-card">
    <div className="quiver-base-content-card__meta">{meta}</div>
    {children}
    {!showPaywall && (
      <ul className="quiver-base-content-card__tags">
        {tags.map((tag) => (
          <li key={tag.name}>
            {tag.url ? (
              <a href={tag.url} onClick={() => onClickTag && onClickTag(tag)}>
                {tag.name}
              </a>
            ) : (
              tag.name
            )}
          </li>
        ))}
      </ul>
    )}
  </div>
);

BaseContentCard.propTypes = {
  meta: PropTypes.string.isRequired,
  tags: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      url: PropTypes.string,
    })
  ).isRequired,
  onClickTag: PropTypes.func,
  children: PropTypes.node.isRequired,
  showPaywall: PropTypes.bool,
};

BaseContentCard.defaultProps = {
  onClickTag: null,
  showPaywall: false,
};

export default BaseContentCard;
