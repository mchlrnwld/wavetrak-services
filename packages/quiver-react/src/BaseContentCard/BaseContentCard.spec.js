import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import BaseContentCard from './BaseContentCard';

describe('BaseContentCard', () => {
  const Meta = () => <span>Expert Forecast • 11 min ago</span>;

  it('it should render meta, children, and tags', () => {
    const tags = [
      { name: 'California' },
      { name: 'Orange County' },
      { name: 'Los Angeles County' },
    ];
    const wrapper = shallow(
      <BaseContentCard meta={<Meta />} tags={tags}>
        <h2>Fun SSW/S Southern Hemi Peaks in Orange County Today</h2>
      </BaseContentCard>
    );

    const metaWrapper = wrapper.find('.quiver-base-content-card__meta');
    expect(metaWrapper.find(Meta)).to.have.length(1);

    const tagsWrapper = wrapper.find('.quiver-base-content-card__tags');
    const tagWrappers = tagsWrapper.find('li');
    expect(tagWrappers).to.have.length(3);
    expect(tagWrappers.at(0).html()).to.equal('<li>California</li>');
    expect(tagWrappers.at(1).html()).to.equal('<li>Orange County</li>');
    expect(tagWrappers.at(2).html()).to.equal('<li>Los Angeles County</li>');
  });

  it('should render clickable link tags if tags contain url', () => {
    const tags = [
      { name: 'Feature', url: '#feature' },
      { name: 'Best Bet', url: '#best-bet' },
    ];
    const onClickTag = sinon.stub();
    const wrapper = shallow(
      <BaseContentCard meta={<Meta />} tags={tags} onClickTag={onClickTag}>
        <h2>Fun SSW/S Southern Hemi Peaks in Orange County Today</h2>
      </BaseContentCard>
    );

    const metaWrapper = wrapper.find('.quiver-base-content-card__meta');
    expect(metaWrapper.find(Meta)).to.have.length(1);

    const tagsWrapper = wrapper.find('.quiver-base-content-card__tags');
    const tagWrappers = tagsWrapper.find('li');
    expect(tagWrappers).to.have.length(2);
    expect(tagWrappers.at(0).html()).to.equal(
      '<li><a href="#feature">Feature</a></li>'
    );
    expect(tagWrappers.at(1).html()).to.equal(
      '<li><a href="#best-bet">Best Bet</a></li>'
    );
    expect(onClickTag).not.to.have.been.called();
    tagWrappers.at(0).find('a').simulate('click');
    expect(onClickTag).to.have.been.calledOnce();
    expect(onClickTag).to.have.been.calledWithExactly(tags[0]);
    tagWrappers.at(1).find('a').simulate('click');
    expect(onClickTag).to.have.been.calledTwice();
    expect(onClickTag).to.have.been.calledWithExactly(tags[1]);
  });
});
