import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';

import Spinner from '../Spinner';
import AsyncDataContainer from './AsyncDataContainer';

describe('AsyncDataContainer', () => {
  it('should render a loading message', () => {
    const wrapper = mount(
      <AsyncDataContainer loading loadingText="This is loading text" />
    );
    expect(wrapper.find(Spinner)).to.have.length(1);
    expect(
      wrapper.find('.quiver-async-data-container__loading_text')
    ).to.have.length(1);
    expect(
      wrapper.find('.quiver-async-data-container__loading_text').text()
    ).to.be.equal('This is loading text');
  });

  it('should show a loading slow message', () => {
    const wrapper = mount(
      <AsyncDataContainer
        loading
        loadingText="This is loading text"
        loadingSlow
      />
    );
    expect(
      wrapper.find('.quiver-async-data-container__loading--slow')
    ).to.have.length(1);
  });
  it('should use the given width and height', () => {
    const wrapper = mount(
      <AsyncDataContainer loading width={100} height={100} />
    );
    expect(
      wrapper.find('.quiver-async-data-container').prop('style')
    ).to.deep.equal({
      width: 100,
      height: 100,
    });
  });
  it('should show an error', () => {
    const wrapper = mount(<AsyncDataContainer error errorMessage="message" />);
    expect(wrapper.find('.quiver-async-data-container__error')).to.have.length(
      1
    );
    expect(
      wrapper.find('.quiver-async-data-container__error').text()
    ).to.be.equal('message');
  });
  it('should render the child if not loading and no errors', () => {
    const wrapper = mount(
      <AsyncDataContainer>
        <div className="child-div" />
      </AsyncDataContainer>
    );
    expect(wrapper.find('.child-div')).to.have.length(1);
  });
});
