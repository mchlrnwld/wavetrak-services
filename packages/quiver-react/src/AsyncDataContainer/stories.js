import React from 'react';
import AsyncDataContainer from './index';

export default {
  title: 'AsyncDataContainer',
};

export const DefaultError = () => (
  <AsyncDataContainer error width={300} height={300} />
);

DefaultError.story = {
  name: 'default error',
};

export const CustomError = () => (
  <AsyncDataContainer
    error
    errorMessage="Custom Error"
    width={300}
    height={300}
  />
);

CustomError.story = {
  name: 'custom error',
};

export const Loading = () => (
  <AsyncDataContainer loading width={300} height={300} />
);

Loading.story = {
  name: 'loading',
};

export const LoadingWithMessage = () => (
  <AsyncDataContainer
    loading
    loadingText="Loading..."
    width={300}
    height={300}
  />
);

LoadingWithMessage.story = {
  name: 'loading with message',
};

export const LoadingSlow = () => (
  <AsyncDataContainer loading loadingSlow width={300} height={300} />
);

LoadingSlow.story = {
  name: 'loadingSlow',
};
