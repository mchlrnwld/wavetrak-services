import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import Spinner from '../Spinner';

const containerClass = (className) =>
  classnames({
    'quiver-async-data-container': true,
    [className]: !!className,
  });

const AsyncDataContainer = ({
  width,
  height,
  error,
  errorMessage,
  loading,
  loadingSlow,
  className,
  children,
  loadingText,
}) => {
  const styles = {
    ...(width && { width }),
    ...(height && { height }),
  };
  if (loading || error) {
    return (
      <div className={containerClass(className)} style={styles}>
        {(() => {
          if (error) {
            return (
              <div className="quiver-async-data-container__error">
                {errorMessage || 'Oops, something went wrong...'}
              </div>
            );
          }
          if (loading) {
            return (
              <div className="quiver-async-data-container__loading">
                <Spinner />
                {loadingText && (
                  <span className="quiver-async-data-container__loading_text">
                    {loadingText}
                  </span>
                )}
                {loadingSlow && (
                  <span className="quiver-async-data-container__loading--slow">
                    The request seems to be loading slow, thanks for your
                    patience...
                  </span>
                )}
              </div>
            );
          }
          return null;
        })()}
      </div>
    );
  }
  return children;
};

AsyncDataContainer.propTypes = {
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  error: PropTypes.bool,
  loading: PropTypes.bool,
  loadingSlow: PropTypes.bool,
  className: PropTypes.string,
  children: PropTypes.node,
  loadingText: PropTypes.string,
  errorMessage: PropTypes.string,
};

AsyncDataContainer.defaultProps = {
  errorMessage: null,
  width: null,
  height: null,
  error: false,
  loading: false,
  loadingSlow: false,
  className: null,
  children: null,
  loadingText: null,
};

export default AsyncDataContainer;
