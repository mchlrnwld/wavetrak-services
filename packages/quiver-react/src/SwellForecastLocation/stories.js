import React from 'react';
import SwellForecastLocation from './SwellForecastLocation';

export default {
  title: 'SwellForecastLocation',
};

export const Default = () => (
  <SwellForecastLocation className="quiver-test" lat={10} lon={11} />
);

Default.story = {
  name: 'default',
};
