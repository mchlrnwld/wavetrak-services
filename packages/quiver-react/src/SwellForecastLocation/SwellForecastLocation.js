import PropTypes from 'prop-types';
import React from 'react';

const SwellForecastLocation = ({ className, lat, lon }) => (
  <a
    className={className}
    href={`https://www.google.com/maps/search/?api=1&query=${lat},${lon}`}
    target="_blank"
    rel="noopener noreferrer"
  >
    at {lat}, {lon}
  </a>
);

SwellForecastLocation.propTypes = {
  className: PropTypes.string,
  lat: PropTypes.number.isRequired,
  lon: PropTypes.number.isRequired,
};

SwellForecastLocation.defaultProps = {
  className: null,
};

export default SwellForecastLocation;
