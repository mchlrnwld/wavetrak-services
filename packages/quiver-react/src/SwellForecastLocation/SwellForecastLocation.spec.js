import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import SwellForecastLocation from './SwellForecastLocation';

describe('SwellForecastLocation', () => {
  it('should render the component with the given props', () => {
    const wrapper = mount(
      <SwellForecastLocation className="quiver-test" lat={10} lon={11} />
    );

    expect(wrapper.find('a.quiver-test')).to.have.length(1);
    expect(wrapper.text()).to.be.equal('at 10, 11');
  });
});
