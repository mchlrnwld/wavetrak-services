import { expect } from 'chai';

import * as quiverReact from './index';

describe('quiver-react', () => {
  it('should export components', () => {
    expect(quiverReact).to.be.ok();
  });
});
