import React from 'react';
import PropTypes from 'prop-types';
import { color } from '@surfline/quiver-themes';
import classNames from 'classnames';
import dayNightShading from './dayNightShading';

/**
 * @param {number} optimalScore
 */
const getHoverColor = (optimalScore) => {
  switch (optimalScore) {
    case 1:
      return '#FFF7CC';
    case 2:
      return '#FFE866';
    default:
      return color('blue-gray', 20);
  }
};

/**
 * @param {object} Props
 * @param {number} Props.activeColumn
 * @param {number} Props.columnNum
 * @param {number} Props.currentColumn
 * @param {0 | 1 | 2} Props.optimalScore
 * @param {number} Props.dawnColumn
 * @param {number} Props.dawnColumnPercentage
 * @param {number} Props.duskColumn
 * @param {number} Props.duskColumnPercentage
 */
const GraphColumn = ({
  activeColumn,
  columnNum,
  currentColumn,
  optimalScore,
  dawnColumn,
  dawnColumnPercentage,
  duskColumn,
  duskColumnPercentage,
}) => (
  <div
    className={classNames({
      'quiver-graph-container__column': true,
      'quiver-graph-container__column--current': columnNum === currentColumn,
      'quiver-graph-container__column--good': optimalScore === 1,
      'quiver-graph-container__column--optimal': optimalScore === 2,
    })}
    style={{
      background:
        columnNum === activeColumn
          ? getHoverColor(optimalScore)
          : dayNightShading(
              columnNum,
              dawnColumn,
              dawnColumnPercentage,
              duskColumn,
              duskColumnPercentage
            ),
    }}
  />
);

GraphColumn.propTypes = {
  columnNum: PropTypes.number.isRequired,
  currentColumn: PropTypes.number,
  optimalScore: PropTypes.oneOf([0, 1, 2]).isRequired,
  dawnColumn: PropTypes.number.isRequired,
  dawnColumnPercentage: PropTypes.number.isRequired,
  duskColumn: PropTypes.number.isRequired,
  duskColumnPercentage: PropTypes.number.isRequired,
  activeColumn: PropTypes.number,
};

GraphColumn.defaultProps = {
  currentColumn: null,
  activeColumn: null,
};

export default GraphColumn;
