import { color } from '@surfline/quiver-themes';

/**
 * @param {string} firstColor
 * @param {string} secondColor
 * @param {number} breakPercentage
 */
export const transitionBackground = (
  firstColor,
  secondColor,
  breakPercentage
) =>
  `linear-gradient(to right, ${firstColor} 0%, ${firstColor} ${
    breakPercentage * 100
  }%, ${secondColor} ${breakPercentage * 100}%, ${secondColor} 100%)`;

/**
 * @param {number} columnIndex
 * @param {number} dawnColumn
 * @param {number} dawnColumnPercentage
 * @param {number} duskColumn
 * @param {number} duskColumnPercentage
 */
const dayNightShading = (
  columnIndex,
  dawnColumn,
  dawnColumnPercentage,
  duskColumn,
  duskColumnPercentage
) => {
  if (columnIndex < dawnColumn || columnIndex > duskColumn)
    return color('blue-gray', 10);
  if (columnIndex === dawnColumn) {
    return transitionBackground(
      color('blue-gray', 10),
      color('white'),
      dawnColumnPercentage
    );
  }
  if (columnIndex === duskColumn) {
    return transitionBackground(
      color('white'),
      color('blue-gray', 10),
      duskColumnPercentage
    );
  }
  return color('white');
};

export default dayNightShading;
