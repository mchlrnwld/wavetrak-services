import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { color } from '@surfline/quiver-themes';
import GraphColumn from './GraphColumn';
import { transitionBackground } from './dayNightShading';

describe('components / GraphColumn', () => {
  it('should render the graph column', () => {
    const wrapper = shallow(
      <GraphColumn
        columnNum={2}
        dawnColumn={1}
        dawnColumnPercentage={0.25}
        duskColumn={4}
        duskColumnPercentage={0.75}
      />
    );
    const column = wrapper.find('.quiver-graph-container__column');
    expect(column).to.have.length(1);
    expect(column.prop('style')).to.deep.equal({ background: color('white') });
  });

  it('should properly shade a column if it is active', () => {
    const wrapper = shallow(
      <GraphColumn
        columnNum={0}
        activeColumn={0}
        optimalScore={0}
        dawnColumn={1}
        dawnColumnPercentage={0.25}
        duskColumn={4}
        duskColumnPercentage={0.75}
      />
    );
    const getColumn = () => wrapper.find('.quiver-graph-container__column');

    expect(getColumn()).to.have.length(1);

    expect(getColumn().prop('style')).to.deep.equal({
      background: color('blue-gray', 20),
    });

    wrapper.setProps({ optimalScore: 1 });
    expect(getColumn().prop('style')).to.deep.equal({ background: '#FFF7CC' });

    wrapper.setProps({ optimalScore: 2 });
    expect(getColumn().prop('style')).to.deep.equal({ background: '#FFE866' });
  });

  it('should set a classname for the current column', () => {
    const wrapper = shallow(
      <GraphColumn
        columnNum={0}
        currentColumn={0}
        dawnColumn={1}
        dawnColumnPercentage={0.25}
        duskColumn={4}
        duskColumnPercentage={0.75}
      />
    );
    const currColumn = wrapper.find('.quiver-graph-container__column--current');

    expect(currColumn).to.have.length(1);
  });

  it('should set a classname for optimal scores', () => {
    const wrapper = shallow(
      <GraphColumn
        columnNum={0}
        optimalScore={0}
        dawnColumn={1}
        dawnColumnPercentage={0.25}
        duskColumn={4}
        duskColumnPercentage={0.75}
      />
    );
    const goodClass = '.quiver-graph-container__column--good';
    const optimalClass = '.quiver-graph-container__column--optimal';

    expect(wrapper.find(goodClass)).to.have.length(0);
    expect(wrapper.find(optimalClass)).to.have.length(0);

    wrapper.setProps({ optimalScore: 1 });

    expect(wrapper.find(goodClass)).to.have.length(1);
    expect(wrapper.find(optimalClass)).to.have.length(0);

    wrapper.setProps({ optimalScore: 2 });

    expect(wrapper.find(goodClass)).to.have.length(0);
    expect(wrapper.find(optimalClass)).to.have.length(1);
  });

  it('should handle dawn/dusk shading', () => {
    const wrapper = shallow(
      <GraphColumn
        columnNum={0}
        dawnColumn={1}
        dawnColumnPercentage={0.5}
        duskColumn={4}
        duskColumnPercentage={0.5}
      />
    );
    const getColumn = () => wrapper.find('.quiver-graph-container__column');

    expect(getColumn()).to.have.length(1);
    expect(getColumn().prop('style')).to.deep.equal({
      background: color('blue-gray', 10),
    });

    wrapper.setProps({ columnNum: 1 });

    expect(getColumn().prop('style')).to.deep.equal({
      background: transitionBackground(
        color('blue-gray', 10),
        color('white'),
        0.5
      ),
    });

    wrapper.setProps({ columnNum: 4 });

    expect(getColumn().prop('style')).to.deep.equal({
      background: transitionBackground(
        color('white'),
        color('blue-gray', 10),
        0.5
      ),
    });

    wrapper.setProps({ columnNum: 5 });

    expect(getColumn().prop('style')).to.deep.equal({
      background: color('blue-gray', 10),
    });
  });
});
