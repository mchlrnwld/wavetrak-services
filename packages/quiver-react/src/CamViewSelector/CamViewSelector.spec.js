import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import CamViewSelector from './CamViewSelector';

describe('CamViewSelector', () => {
  const props = {
    cameras: [1, 2],
    onClick: sinon.stub(),
  };

  const wrapper = mount(<CamViewSelector {...props} />);

  const wrapperWithActiveProp = mount(
    <CamViewSelector {...props} active="MULTI" isPremium />
  );

  const wrapperWithOneCam = mount(<CamViewSelector {...props} cameras={[1]} />);

  const wrapperWithTwoCam = mount(
    <CamViewSelector {...props} cameras={[1, 2]} />
  );

  const wrapperWithThreeCam = mount(
    <CamViewSelector {...props} cameras={[1, 2, 3]} />
  );

  const wrapperWithFourCam = mount(
    <CamViewSelector {...props} cameras={[1, 2, 3, 4]} />
  );

  it('should render a div with class name', () => {
    expect(wrapper.html()).to.contain('<div class="quiver-cam-view-selector"');
  });

  it('should default to single when no active prop passed', () => {
    expect(wrapper.html()).to.contain(
      '<div class="quiver-cam-view-selector__option quiver-cam-view-selector__option--active'
    );
  });

  it('should select multiple view when active prop is MULTI', () => {
    expect(wrapperWithActiveProp.html()).to.contain(
      'quiver-cam-view-selector__option--active quiver-cam-view-selector__option--multi"'
    );
  });

  it('should show single cam option when one camera available', () => {
    expect(wrapperWithOneCam.html()).to.contain(
      'quiver-cam-view-selector__option--one'
    );
  });

  it('should show double cam option when two cameras available', () => {
    expect(wrapperWithTwoCam.html()).to.contain(
      'quiver-cam-view-selector__option--two'
    );
  });

  it('should show triple cam option when three cameras available', () => {
    expect(wrapperWithThreeCam.html()).to.contain(
      'quiver-cam-view-selector__option--three'
    );
  });

  it('should show quad cam option when four cameras available', () => {
    expect(wrapperWithFourCam.html()).to.contain(
      'quiver-cam-view-selector__option--four'
    );
  });

  it('should execute the onClick function on view option click', () => {
    expect(props.onClick).not.to.have.been.called();
    wrapper.find('.quiver-cam-view-selector__option--multi').simulate('click');
    expect(props.onClick).to.have.been.calledOnce();
  });
});
