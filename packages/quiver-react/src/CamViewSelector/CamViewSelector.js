import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import PremiumRibbon from '../icons/PremiumRibbon';
import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

const SINGLE = 'SINGLE';
const MULTI = 'MULTI';

const camViewSelectorClassNames = (centered) =>
  classNames({
    'quiver-cam-view-selector': true,
    'quiver-cam-view-selector--centered': centered,
  });

const multiCamClassNames = (cameraCount, isHoveringMulti) =>
  classNames({
    'quiver-cam-view-selector__option--two': cameraCount === 2,
    'quiver-cam-view-selector__option--three': cameraCount === 3,
    'quiver-cam-view-selector__option--four': cameraCount === 4,
    'quiver-cam-view-selector__option--hovering': isHoveringMulti,
  });

const singleCamClassNames = (isHoveringSingle) =>
  classNames({
    'quiver-cam-view-selector__option--one': true,
    'quiver-cam-view-selector__option--hovering': isHoveringSingle,
  });

const camOptionClassNames = (
  active,
  isPremium,
  isHovering,
  menuType,
  multi = false
) => {
  const isHoveringMulti = isHovering && menuType === MULTI;
  const isHoveringSingle = isHovering && menuType === SINGLE;
  const showPaywall = !isPremium && multi && (active || isHoveringMulti);

  return classNames({
    'quiver-cam-view-selector__option': true,
    'quiver-cam-view-selector__option--active': active,
    'quiver-cam-view-selector__option--free-user': !isPremium,
    'quiver-cam-view-selector__option--premium-paywall': showPaywall,
    'quiver-cam-view-selector__option--multi': multi,
    'quiver-cam-view-selector__option--single': !multi,
    'quiver-cam-view-selector__option--multi-hover': multi && isHoveringMulti,
    'quiver-cam-view-selector__option--single-hover':
      !multi && isHoveringSingle,
  });
};

const CamViewSelector = ({
  active,
  onClick,
  isPremium,
  cameras,
  onMouseEnterMenu,
  onMouseLeaveMenu,
  menuType,
  isHovering,
  centered,
}) => {
  const activeView = active;
  const cameraCount = cameras.length;
  const isHoveringMulti = isHovering && menuType === MULTI;
  const isHoveringSingle = isHovering && menuType === SINGLE;
  const multiCamClassName = multiCamClassNames(cameraCount, isHoveringMulti);
  return (
    <div className={camViewSelectorClassNames(centered)}>
      <div
        onMouseEnter={() => onMouseEnterMenu(SINGLE)}
        onMouseLeave={onMouseLeaveMenu}
        className={camOptionClassNames(
          cameraCount === 1 || activeView === SINGLE,
          true,
          isHovering,
          menuType
        )}
        {...createAccessibleOnClick(() => onClick(SINGLE), BTN)}
      >
        <div className={singleCamClassNames(isHoveringSingle)} />
        <span>SINGLE</span>
      </div>
      {cameraCount >= 2 && (
        <div
          onMouseEnter={() => onMouseEnterMenu(MULTI)}
          onMouseLeave={onMouseLeaveMenu}
          className={camOptionClassNames(
            activeView === MULTI && cameraCount > 1,
            isPremium,
            isHovering,
            menuType,
            true
          )}
          {...createAccessibleOnClick(() => onClick(MULTI), BTN)}
        >
          <div className={multiCamClassName} />
          <div className={multiCamClassName} />
          {cameraCount >= 3 && <div className={multiCamClassName} />}
          {cameraCount === 4 && <div className={multiCamClassName} />}
          {!isPremium && <PremiumRibbon />}
          <span>MULTI</span>
        </div>
      )}
    </div>
  );
};

CamViewSelector.propTypes = {
  active: PropTypes.oneOf([SINGLE, MULTI]),
  cameras: PropTypes.arrayOf(PropTypes.shape({})),
  isPremium: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  onMouseEnterMenu: PropTypes.func,
  onMouseLeaveMenu: PropTypes.func,
  menuType: PropTypes.oneOf([SINGLE, MULTI]),
  isHovering: PropTypes.bool,
  centered: PropTypes.bool,
};

CamViewSelector.defaultProps = {
  active: SINGLE,
  isPremium: false,
  isHovering: false,
  cameras: [],
  onMouseEnterMenu: () => null,
  onMouseLeaveMenu: () => null,
  menuType: SINGLE,
  centered: false,
};

export default CamViewSelector;
