import React from 'react';
import CamViewSelector from './CamViewSelector';

const SINGLE = 'SINGLE';

/* eslint-disable react/prop-types */
const CamViewSelectorWithState = ({
  cameras,
  centered,
  isPremium,
  activeView,
}) => {
  const [active, setActive] = React.useState(activeView || SINGLE);
  return (
    <CamViewSelector
      cameras={cameras}
      active={active}
      onClick={setActive}
      centered={centered}
      isPremium={isPremium}
    />
  );
};

export default {
  title: 'CamViewSelector',
};

export const SingleFree = () => (
  <CamViewSelectorWithState cameras={[1]} centered />
);

SingleFree.story = {
  name: 'Single - Free',
};

export const DoubleFree = () => (
  <CamViewSelectorWithState cameras={[1, 2]} centered />
);

DoubleFree.story = {
  name: 'Double - Free',
};

export const TripleFree = () => (
  <CamViewSelectorWithState cameras={[1, 2, 3]} centered />
);

TripleFree.story = {
  name: 'Triple - Free',
};

export const QuadFree = () => (
  <CamViewSelectorWithState cameras={[1, 2, 3, 4]} centered />
);

QuadFree.story = {
  name: 'Quad - Free',
};

export const SinglePremium = () => (
  <CamViewSelectorWithState cameras={[1]} isPremium centered />
);

SinglePremium.story = {
  name: 'Single - Premium',
};

export const DoublePremium = () => (
  <CamViewSelectorWithState cameras={[1, 2]} isPremium centered />
);

DoublePremium.story = {
  name: 'Double - Premium',
};

export const TriplePremium = () => (
  <CamViewSelectorWithState cameras={[1, 2, 3]} isPremium centered />
);

TriplePremium.story = {
  name: 'Triple - Premium',
};

export const QuadPremium = () => (
  <CamViewSelectorWithState cameras={[1, 2, 3, 4]} isPremium centered />
);

QuadPremium.story = {
  name: 'Quad - Premium',
};

export const QuadSelectedPremium = () => (
  <CamViewSelectorWithState
    activeView="MULTI"
    cameras={[1, 2, 3, 4]}
    isPremium
    centered
  />
);

QuadSelectedPremium.story = {
  name: 'Quad selected - Premium',
};
