/** @prettier */

import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import Chevron from '../Chevron';

const arrowClassName = (direction, position) =>
  classNames({
    'quiver-forecast-graphs-tab-arrows__arrow': true,
    'quiver-forecast-graphs-tab-arrows__arrow--back': direction === 'left',
    'quiver-forecast-graphs-tab-arrows__arrow--forward': direction === 'right',
    'quiver-forecast-graphs-tab-arrows__arrow--wind': position === 'bottom',
  });

const Arrow = ({ direction, position, onClick }) => (
  <button
    type="button"
    className={arrowClassName(direction, position)}
    onClick={onClick}
  >
    <Chevron direction={direction} />
  </button>
);

const ForecastGraphsTabArrows = ({
  days,
  tabBackwards,
  tabForwards,
  daysEnabled,
}) => {
  const showLeftArrows = days.indexOf(0) === -1;
  const showRightArrows = days.indexOf(daysEnabled - 1) === -1;
  return (
    <div className="quiver-forecast-graphs-tab-arrows">
      {showLeftArrows && (
        <div className="quiver-forecast-graphs-tab-arrows__left">
          <Arrow direction="left" onClick={tabBackwards} />
          <Arrow direction="left" position="bottom" onClick={tabBackwards} />
        </div>
      )}
      {showRightArrows && (
        <div className="quiver-forecast-graphs-tab-arrows__right">
          <Arrow direction="right" onClick={tabForwards} />
          <Arrow direction="right" position="bottom" onClick={tabForwards} />
        </div>
      )}
    </div>
  );
};

Arrow.propTypes = {
  onClick: PropTypes.func.isRequired,
  direction: PropTypes.oneOf(['left', 'right']).isRequired,
  position: PropTypes.oneOf(['top', 'bottom']),
};

Arrow.defaultProps = {
  position: 'top',
};

ForecastGraphsTabArrows.propTypes = {
  tabForwards: PropTypes.func.isRequired,
  tabBackwards: PropTypes.func.isRequired,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  daysEnabled: PropTypes.number.isRequired,
};

export default ForecastGraphsTabArrows;
