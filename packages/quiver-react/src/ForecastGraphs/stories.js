import React from 'react';
import AutomatedRatings from './AutomatedRatings';
import createAutomatedRatingsFixtures from './fixtures/automatedRatings';

const { automatedRatings, automatedRatingsError, automatedRatingsLoading } =
  createAutomatedRatingsFixtures();

export default {
  component: AutomatedRatings,
  title: 'AutomatedRatings',
};

const Template = (args) => <AutomatedRatings {...args} />;

export const Hourly = Template.bind({});

Hourly.args = {
  automatedRatings,
  isHourly: true,
  days: [0],
};

export const Default = Template.bind({});

Default.args = {
  automatedRatings,
  isHourly: false,
  days: [0, 1],
};

export const Loading = Template.bind({});

Loading.args = {
  automatedRatings: automatedRatingsLoading,
  isHourly: false,
  days: [0, 1],
};

export const Error = Template.bind({});

Error.args = {
  automatedRatings: automatedRatingsError,
  isHourly: false,
  days: [0, 1],
};
