/* istanbul ignore file */
import classNames from 'classnames';

const dayClassName = (day, type, daysEnabled) =>
  classNames({
    [`quiver-forecast-graphs__${type}__day`]: true,
    [`quiver-forecast-graphs__${type}__day--blurred`]: day >= daysEnabled,
  });

export default dayClassName;
