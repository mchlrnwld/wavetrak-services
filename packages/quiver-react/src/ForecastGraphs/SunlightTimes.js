import React from 'react';
import PropTypes from 'prop-types';
import { getLocalDate } from '@surfline/web-common';
import { format } from 'date-fns';
import { sunlightTimesPropType } from '../PropTypes/sunlightTimes';

/**
 * @param {object} Props
 * @param {import('../PropTypes/sunlightTimes').SunlightTimes} Props.sunlightTimes
 * @param {number} Props.day
 */
const SunlightTimes = ({ sunlightTimes, day }) => {
  const firstLight = {
    utcOffset: sunlightTimes.days[day].dawnUTCOffset,
    timestamp: sunlightTimes.days[day].dawn,
  };
  const sunrise = {
    utcOffset: sunlightTimes.days[day].sunriseUTCOffset,
    timestamp: sunlightTimes.days[day].sunrise,
  };
  const sunset = {
    utcOffset: sunlightTimes.days[day].sunsetUTCOffset,
    timestamp: sunlightTimes.days[day].sunset,
  };
  const lastLight = {
    utcOffset: sunlightTimes.days[day].duskUTCOffset,
    timestamp: sunlightTimes.days[day].dusk,
  };
  const formatString = "h:mmaaaaa'm'";

  const firstLightDate = format(
    getLocalDate(firstLight.timestamp, firstLight.utcOffset),
    formatString
  );
  const sunriseDate = format(
    getLocalDate(sunrise.timestamp, sunrise.utcOffset),
    formatString
  );
  const sunsetDate = format(
    getLocalDate(sunset.timestamp, sunset.utcOffset),
    formatString
  );
  const lastLightDate = format(
    getLocalDate(lastLight.timestamp, lastLight.utcOffset),
    formatString
  );

  return (
    <table className="quiver-forecast-graphs__table quiver-forecast-graphs__table--sunlight-times">
      <tbody>
        <tr>
          <td>First Light</td>
          <td className="quiver-forecast-graphs__table__dawn">
            {firstLightDate}
          </td>
        </tr>
        <tr>
          <td>Sunrise</td>
          <td className="quiver-forecast-graphs__table__sunrise">
            {sunriseDate}
          </td>
        </tr>
        <tr>
          <td>Sunset</td>
          <td className="quiver-forecast-graphs__table__sunset">
            {sunsetDate}
          </td>
        </tr>
        <tr>
          <td>Last Light</td>
          <td className="quiver-forecast-graphs__table__dusk">
            {lastLightDate}
          </td>
        </tr>
      </tbody>
    </table>
  );
};

SunlightTimes.propTypes = {
  sunlightTimes: sunlightTimesPropType.isRequired,
  day: PropTypes.number.isRequired,
};

export default SunlightTimes;
