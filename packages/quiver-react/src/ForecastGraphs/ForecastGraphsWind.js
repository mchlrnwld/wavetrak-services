/* istanbul ignore file */

import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { TABLET_LARGE_WIDTH } from '@surfline/web-common';
import ChartPrimarySpot from '../ChartPrimarySpot';
import GraphContainer from '../GraphContainer';
import OptimalScoreIndicator from '../OptimalScoreIndicator';
import WindGraph from '../WindGraph';
import { sunlightTimesPropType } from '../PropTypes/sunlightTimes';
import windChartPropType from '../PropTypes/windChart';
import devicePropType from '../PropTypes/device';
import renderGraphs from './renderGraphs';
import dayClassName from './dayClassName';
import sunlightTimeXPercentage from './sunlightTimeXPercentage';
import ForecastGraphsUnits from './ForecastGraphsUnits';
import TooltipProvider from '../TooltipProvider';
import QuestionMark from '../icons/QuestionMark';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';

const HOURLY_WIND = 'Hourly Wind';
const SPOT = 'SPOT';

const getOptimalScoreClassName = (isHourly) =>
  classNames({
    'quiver-forecast-graphs__optimal-score': true,
    'quiver-forecast-graphs__optimal-score--hourly': isHourly,
  });

const getClassName = (isHourly) =>
  classNames({
    'quiver-forecast-graphs__wind': true,
    'quiver-forecast-graphs__wind--hourly': isHourly,
  });

class ForecastGraphsWind extends React.PureComponent {
  /**
   * @description Allows a user to click an individual wind graph day
   * to be sent to the hourly forecast for that specific day. Limited
   * to first 5 days for premium users, and first 2 days for free users
   * @param {boolean} showHourlyWindToggle - We only want to allow this to be triggered
   * on desktop and non-hourly views
   * @param {number} dayNum - The day to toggle over to
   * @memberof ForecastGraphsWind
   */
  graphClick = (showHourlyWindToggle, dayNum) => {
    const { changeToHourlyView, isPremium } = this.props;
    if (showHourlyWindToggle) {
      if (dayNum < 2 || (isPremium && dayNum < 5))
        return changeToHourlyView('Graph Click Through', dayNum);
    }
    return null;
  };

  render() {
    const {
      type,
      wind,
      days,
      daysEnabled,
      sunlightTimes,
      spotId,
      spotName,
      showOptimalConditions,
      isHourly,
      device,
      currentMarkerXPercentage,
      extended,
      changeToHourlyView,
      isMultiCamPage,
      dateFormat,
      useNextLink,
    } = this.props;
    const showHourlyWindToggle = extended && days?.length > 1 && type === SPOT;
    return (
      <div className={getClassName(isHourly)}>
        <h3 className="quiver-forecast-graphs__chart-title">
          Wind{' '}
          <ForecastGraphsUnits units={wind.units && wind.units.windSpeed} />
          {type === 'REGIONAL' && spotName ? (
            <ChartPrimarySpot
              className="quiver-forecast-graphs__chart-title__link"
              id={spotId}
              name={spotName}
              useNextLink={useNextLink}
            />
          ) : null}
          {showHourlyWindToggle && (
            <div className="quiver-forecast-graphs__chart-title__hourly-wind-toggle">
              <div
                className="quiver-hourly-wind-toggle__text"
                role="button"
                onClick={() => changeToHourlyView(HOURLY_WIND)}
                onKeyPress={(event) =>
                  event.key === 'Enter' && changeToHourlyView(HOURLY_WIND)
                }
                tabIndex={0}
              >
                Hourly Wind
              </div>
              <TooltipProvider
                renderTooltip={() => (
                  <div className="quiver-hourly-wind-toggle-tooltip">
                    Use the “Hourly” tab at the top or click on the day of
                    interest within the wind graph.
                  </div>
                )}
              >
                <div className="hourly-wind-toggle__tooltip">
                  <QuestionMark />
                </div>
              </TooltipProvider>
            </div>
          )}
        </h3>
        <div className="quiver-forecast-graphs__wind__container">
          {renderGraphs(
            wind.loading || sunlightTimes.loading,
            wind.error,
            wind.message,
            'wind',
            () => {
              const isMobile = device.width < TABLET_LARGE_WIDTH;
              return days.map((day) => {
                const showMobileHourlyGraph = isMobile && day < 5;
                const isHourlyData = isHourly || showMobileHourlyGraph;
                const data =
                  isHourlyData && !isMultiCamPage
                    ? wind.hourly[day]
                    : wind.days[day];
                const optimalScores = data.map((d) => ({
                  score: d.optimalScore,
                  key: d.timestamp,
                }));
                const columns = data.length;
                return (
                  <div
                    key={data[0].timestamp}
                    className={dayClassName(day, 'wind', daysEnabled)}
                  >
                    <GraphContainer
                      timestamps={data}
                      columns={columns}
                      dawnXPercentage={sunlightTimeXPercentage(
                        'dawn',
                        day,
                        sunlightTimes
                      )}
                      duskXPercentage={sunlightTimeXPercentage(
                        'dusk',
                        day,
                        sunlightTimes
                      )}
                      showTimeLabels={!showMobileHourlyGraph}
                      highlightActiveColumn={!showMobileHourlyGraph}
                      day={day}
                      device={device}
                      currentMarkerXPercentage={currentMarkerXPercentage}
                      optimalScores={
                        showOptimalConditions
                          ? optimalScores.map((s) => s.score)
                          : null
                      }
                      isHourly={isHourly}
                      allowMobileTooltip={!showMobileHourlyGraph}
                    >
                      <div
                        className="quiver-forecast-graphs__wind__chart"
                        role="button"
                        onClick={() =>
                          this.graphClick(showHourlyWindToggle, day)
                        }
                        onKeyPress={(event) =>
                          event.key === 'Enter' &&
                          this.graphClick(showHourlyWindToggle, day)
                        }
                        tabIndex={day < 5 && type === SPOT ? day + 1 : null}
                      >
                        <WindGraph
                          dateFormat={dateFormat}
                          isHourly={isHourlyData}
                          showMobileHourlyGraph={showMobileHourlyGraph}
                          day={day}
                          lat={wind.location.lat}
                          lon={wind.location.lon}
                          overallMaxSpeed={wind.overallMaxSpeed}
                          data={data}
                          units={wind.units.windSpeed}
                          showOptimalConditions={showOptimalConditions}
                          currentMarkerXPercentage={
                            day === 0 ? currentMarkerXPercentage : 0
                          }
                          isMultiCamPage={isMultiCamPage}
                        />
                      </div>
                    </GraphContainer>
                    {showOptimalConditions ? (
                      <div className={getOptimalScoreClassName(isHourlyData)}>
                        {optimalScores.map((optimalScore) => (
                          <OptimalScoreIndicator
                            key={data[0].timestamp + optimalScore.key}
                            optimalScore={optimalScore.score}
                          />
                        ))}
                      </div>
                    ) : null}
                  </div>
                );
              });
            }
          )}
        </div>
      </div>
    );
  }
}

ForecastGraphsWind.propTypes = {
  isMultiCamPage: PropTypes.bool,
  type: PropTypes.oneOf(['SPOT', 'REGIONAL']).isRequired,
  wind: windChartPropType.isRequired,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  daysEnabled: PropTypes.number.isRequired,
  sunlightTimes: sunlightTimesPropType.isRequired,
  spotId: PropTypes.string.isRequired,
  spotName: PropTypes.string,
  showOptimalConditions: PropTypes.bool,
  isHourly: PropTypes.bool.isRequired,
  currentMarkerXPercentage: PropTypes.number,
  device: devicePropType.isRequired,
  extended: PropTypes.bool,
  isPremium: PropTypes.bool,
  changeToHourlyView: PropTypes.func.isRequired,
  dateFormat: dateFormatPropType,
  useNextLink: PropTypes.bool,
};

ForecastGraphsWind.defaultProps = {
  isMultiCamPage: false,
  spotName: null,
  showOptimalConditions: false,
  currentMarkerXPercentage: 0,
  extended: false,
  isPremium: false,
  dateFormat: defaultDateFormat,
  useNextLink: false,
};

export default ForecastGraphsWind;
