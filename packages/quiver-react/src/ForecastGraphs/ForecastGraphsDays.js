/* istanbul ignore file */

import PropTypes from 'prop-types';
import React from 'react';
import Sticky from 'react-stickynode';
import { format } from 'date-fns';
import { getLocalDateForFutureDay } from '@surfline/web-common';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';

const ForecastGraphsDays = ({ utcOffset, days, daysEnabled, dateFormat }) => (
  <div className="quiver-forecast-graphs__days">
    {days.map((day) => {
      const localDate = getLocalDateForFutureDay(day, utcOffset);
      if (day >= daysEnabled) {
        return (
          <div className="quiver-forecast-graphs__day" key={day}>
            {format(localDate, dateFormat === 'MDY' ? 'iiii M/d' : 'iiii d/M')}
          </div>
        );
      }
      return (
        <div className="quiver-forecast-graphs__day" key={day}>
          <Sticky
            innerZ={40}
            activeClass="sticky-condition-day"
            bottomBoundary=".quiver-forecast-graphs__container"
          >
            {format(localDate, dateFormat === 'MDY' ? 'iiii M/d' : 'iiii d/M')}
          </Sticky>
        </div>
      );
    })}
  </div>
);

ForecastGraphsDays.propTypes = {
  utcOffset: PropTypes.number.isRequired,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  daysEnabled: PropTypes.number.isRequired,
  dateFormat: dateFormatPropType,
};

ForecastGraphsDays.defaultProps = {
  dateFormat: defaultDateFormat,
};

export default ForecastGraphsDays;
