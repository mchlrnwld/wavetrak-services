/** @prettier */

import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import ForecastGraphsTabArrows from './ForecastGraphsTabArrows';

const tabFunc = () => {};
const props = { tabForwards: tabFunc, tabBackwards: tabFunc };
describe('components / ForecastGraphsTabArrows', () => {
  it('should not render arrows if all the days are shown', () => {
    const wrapper = mount(
      <ForecastGraphsTabArrows days={[0, 1, 2]} daysEnabled={3} {...props} />
    );

    expect(
      wrapper.find('.quiver-forecast-graphs-tab-arrows__right')
    ).to.be.have.length(0);
    expect(
      wrapper.find('.quiver-forecast-graphs-tab-arrows__right')
    ).to.be.have.length(0);
  });

  it('should show the left arrow and not the right arrow', () => {
    const wrapper = mount(
      <ForecastGraphsTabArrows days={[1, 2]} daysEnabled={3} {...props} />
    );

    expect(
      wrapper.find('.quiver-forecast-graphs-tab-arrows__left')
    ).to.be.have.length(1);
    expect(
      wrapper.find('.quiver-forecast-graphs-tab-arrows__right')
    ).to.be.have.length(0);
  });

  it('should show the right arrow and not the left arrow', () => {
    const wrapper = mount(
      <ForecastGraphsTabArrows days={[0, 1]} daysEnabled={3} {...props} />
    );

    expect(
      wrapper.find('.quiver-forecast-graphs-tab-arrows__leftt')
    ).to.be.have.length(0);
    expect(
      wrapper.find('.quiver-forecast-graphs-tab-arrows__right')
    ).to.be.have.length(1);
  });

  it('should show both arrow', () => {
    const wrapper = mount(
      <ForecastGraphsTabArrows days={[1]} daysEnabled={3} {...props} />
    );

    expect(
      wrapper.find('.quiver-forecast-graphs-tab-arrows__right')
    ).to.be.have.length(1);
    expect(
      wrapper.find('.quiver-forecast-graphs-tab-arrows__right')
    ).to.be.have.length(1);
  });
});
