/* istanbul ignore file */

import PropTypes from 'prop-types';
import React from 'react';
import getUnitType from '../helpers/getUnitType';

const ForecastGraphsUnits = ({ units }) => {
  if (units) {
    return (
      <span className="quiver-forecast-graphs__units">
        ({getUnitType(units)})
      </span>
    );
  }

  return null;
};

ForecastGraphsUnits.propTypes = {
  units: PropTypes.string,
};

ForecastGraphsUnits.defaultProps = {
  units: null,
};

export default ForecastGraphsUnits;
