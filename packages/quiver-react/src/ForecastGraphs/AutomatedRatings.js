import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import Skeleton from 'react-loading-skeleton';
import { conditionClassModifier } from '@surfline/web-common';
import classNames from 'classnames';

import { automatedRatingsPropType } from '../PropTypes/automatedRatings';
import applyConditionVariation from '../helpers/applyConditionVariation';

/**
 * @param {string} condition
 */
const conditionClassName = (condition) =>
  `quiver-automated-ratings__bar--${conditionClassModifier(
    applyConditionVariation(condition)
  )}`;

/**
 * @param {string} rating
 */
const getBarClass = (rating) =>
  classNames({
    'quiver-automated-ratings__bar': true,
    [conditionClassName(rating)]: !!rating,
  });

/**
 * @typedef {object} Props
 * @property {import('../PropTypes/automatedRatings').AutomatedRatings} automatedRatings
 * @property {number[]} days
 * @property {boolean} isHourly
 */

/** @type {React.FC<Props>} */
const AutomatedRatings = ({ automatedRatings, isHourly, days }) => {
  const { error, loading, hourly } = automatedRatings;

  const bars = useMemo(() => {
    if (loading || error) {
      return null;
    }
    const data = isHourly ? hourly : automatedRatings.days;
    return days.map((dayNum) => data[dayNum]);
  }, [days, isHourly, error, automatedRatings.days, hourly, loading]);

  if (error) {
    return null;
  }

  return (
    <div className="quiver-automated-ratings">
      {loading && (
        <Skeleton
          className="quiver-automated-ratings__loading"
          wrapper={({ children }) => <>{children}</>}
          width="100%"
          height="100%"
        />
      )}
      {bars &&
        bars.map((day, i) => (
          <div key={days[i]} className="quiver-automated-ratings__day">
            {day.map((bar) => (
              <div
                key={bar.timestamp}
                style={{ width: `calc(${100 / day.length}% - 2px)` }}
                className={getBarClass(bar.rating)}
              />
            ))}
          </div>
        ))}
    </div>
  );
};

AutomatedRatings.propTypes = {
  automatedRatings: automatedRatingsPropType.isRequired,
  isHourly: PropTypes.bool.isRequired,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
};

export default AutomatedRatings;
