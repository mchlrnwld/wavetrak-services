/* istanbul ignore file */

import PropTypes from 'prop-types';
import React from 'react';
import TideGraph from '../TideGraph';
import { sunlightTimesPropType } from '../PropTypes/sunlightTimes';
import tideChartPropType from '../PropTypes/tideChart';
import renderGraphs from './renderGraphs';
import sunlightTimeXPercentage from './sunlightTimeXPercentage';
import ForecastGraphsUnits from './ForecastGraphsUnits';
import DayNightShading from '../DayNightShading';
import ForecastGraphsTideTable from './ForecastGraphsTideTable';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';

/**
 * @typedef {import('../PropTypes/tideChart').Tide} Tide
 */

/**
 * @param {object} Props
 * @param {Tide} Props.tide
 * @param {number[]} Props.days
 * @param {number} Props.daysEnabled
 * @param {{}[]} Props.sunlightTimes
 * @param {number} Props.currentMarkerXPercentage
 * @param {boolean} Props.isHourly
 * @param {boolean} Props.showTideTable
 * @param {boolean} Props.isMultiCamPage
 * @param {string} Props.dateFormat
 * @param {string | number | Date} Props.startDate
 * @param {string | number | Date} Props.endDate
 */
const ForecastGraphsTide = ({
  tide,
  days,
  daysEnabled,
  sunlightTimes,
  currentMarkerXPercentage,
  isHourly,
  showTideTable,
  isMultiCamPage,
  dateFormat,
  startDate,
  endDate,
}) => (
  <div className="quiver-forecast-graphs__tide">
    <h3 className="quiver-forecast-graphs__chart-title">
      Tide <ForecastGraphsUnits units={tide.units && tide.units.tideHeight} />
    </h3>
    <div className="quiver-forecast-graphs__tide__flex-container">
      {renderGraphs(tide.loading, tide.error, tide.message, 'tide', () => {
        const data = days.map((day) => tide.days[day]);
        const graphData = data.flat();
        const tideHighsLows = data.map((day) =>
          day?.filter((point) => point.type !== 'NORMAL')
        );
        const numDays = days.length;
        return (
          <>
            <div className="quiver-forecast-graphs__tide__container">
              {days.map((day, dayNum) => {
                const dawnXPercentage = sunlightTimeXPercentage(
                  'dawn',
                  day,
                  sunlightTimes
                );
                const duskXPercentage = sunlightTimeXPercentage(
                  'dusk',
                  day,
                  sunlightTimes
                );
                return (
                  <DayNightShading
                    key={day}
                    dawnXPercentage={dawnXPercentage}
                    duskXPercentage={duskXPercentage}
                    day={dayNum}
                    isPaywalled={day >= daysEnabled}
                    numDays={days.length}
                  />
                );
              })}
              <div className="quiver-forecast-graphs__tide__chart">
                <TideGraph
                  dateFormat={dateFormat}
                  data={graphData}
                  tideLocation={tide.tideLocation}
                  units={tide.units.tideHeight}
                  isHourly={isHourly}
                  currentMarkerXPercentage={
                    days.indexOf(0) !== -1 ? currentMarkerXPercentage : null
                  }
                  isMultiCamPage={isMultiCamPage}
                  startDate={startDate}
                  endDate={endDate}
                />
              </div>
            </div>
            {showTideTable && (
              <div className="quiver-forecast-graphs__table__container">
                <ForecastGraphsTideTable
                  days={days}
                  tide={tide}
                  tideHighsLows={tideHighsLows}
                  numDays={numDays}
                  daysEnabled={daysEnabled}
                />
              </div>
            )}
          </>
        );
      })}
    </div>
  </div>
);

ForecastGraphsTide.propTypes = {
  tide: tideChartPropType.isRequired,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  daysEnabled: PropTypes.number.isRequired,
  sunlightTimes: sunlightTimesPropType.isRequired,
  currentMarkerXPercentage: PropTypes.number,
  isHourly: PropTypes.bool.isRequired,
  showTideTable: PropTypes.bool,
  isMultiCamPage: PropTypes.bool,
  dateFormat: dateFormatPropType,
  startDate: PropTypes.instanceOf(Date).isRequired,
  endDate: PropTypes.instanceOf(Date).isRequired,
};

ForecastGraphsTide.defaultProps = {
  currentMarkerXPercentage: null,
  showTideTable: true,
  isMultiCamPage: false,
  dateFormat: defaultDateFormat,
};

export default ForecastGraphsTide;
