/* istanbul ignore file */

import PropTypes from 'prop-types';
import React from 'react';

import Chevron from '../Chevron';

const GRAPH_TYPES = ['BAR_GRAPH', 'AREA_GRAPH'];

const GRAPH_TYPE_LABEL = {
  BAR_GRAPH: 'Bar Graph',
  AREA_GRAPH: 'Area Graph',
};

const ForecastTimeViewDropdown = ({ currGraphType, onClick }) => (
  <div className="quiver-forecast-graph-option-dropdown">
    <div className="quiver-forecast-graph-option-dropdown__title">
      {GRAPH_TYPE_LABEL[currGraphType]}
      <Chevron direction="down" />
    </div>
    <div className="quiver-forecast-graph-option-dropdown__options">
      {GRAPH_TYPES.map((graphType) => (
        <button
          type="button"
          key={graphType}
          className="quiver-forecast-graph-option-dropdown__options__option"
          onClick={currGraphType === graphType ? null : onClick}
        >
          <div className="quiver-forecast-graph-option-dropdown__options__option__details">
            <span className="quiver-forecast-graph-option-dropdown__options__option__details__name">
              {GRAPH_TYPE_LABEL[graphType]}
            </span>
          </div>
        </button>
      ))}
    </div>
  </div>
);

ForecastTimeViewDropdown.propTypes = {
  currGraphType: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

ForecastTimeViewDropdown.defaultProps = {};

export default ForecastTimeViewDropdown;
