/* istanbul ignore file */

import PropTypes from 'prop-types';
import React from 'react';
import SurfLineGraph from '../SurfLineGraph';
import { sunlightTimesPropType } from '../PropTypes/sunlightTimes';
import waveChartPropType from '../PropTypes/waveChart';
import devicePropType from '../PropTypes/device';
import renderGraphs from './renderGraphs';
import sunlightTimeXPercentage from './sunlightTimeXPercentage';
import ForecastGraphsUnits from './ForecastGraphsUnits';
import DayNightShading from '../DayNightShading';
import ForecastGraphOptionDropdown from './ForecastGraphOptionDropdown';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';
import ContinuousGraphContainer from '../ContinuousGraphContainer';

const ForecastSurfLineGraph = ({
  wave,
  days,
  daysEnabled,
  sunlightTimes,
  mobile,
  currentMarkerXPercentage,
  isHourly,
  device,
  currGraphType,
  changeGraphType,
  showTitle,
  isMultiCamPage,
  dateFormat,
  timestamps,
}) => (
  <div className="quiver-forecast-graphs__surf quiver-forecast-graphs__surf--line">
    {showTitle && (
      <h3 className="quiver-forecast-graphs__chart-title">
        Surf <ForecastGraphsUnits units={wave.units && wave.units.waveHeight} />
        <ForecastGraphOptionDropdown
          currGraphType={currGraphType}
          onClick={changeGraphType}
        />
      </h3>
    )}
    <div className="quiver-forecast-graphs__surf__container quiver-forecast-graphs__surf__container--line-graph">
      {renderGraphs(
        wave.loading || sunlightTimes.loading,
        wave.error,
        wave.message,
        'surf',
        () => {
          const graphData = days
            .map((day) => (isHourly ? wave.hourly[day] : wave.days[day]))
            .flat();
          return (
            <>
              {days.map((day, dayNum) => {
                const dawnXPercentage = sunlightTimeXPercentage(
                  'dawn',
                  day,
                  sunlightTimes
                );
                const duskXPercentage = sunlightTimeXPercentage(
                  'dusk',
                  day,
                  sunlightTimes
                );
                return (
                  <DayNightShading
                    key={day}
                    dawnXPercentage={dawnXPercentage}
                    duskXPercentage={duskXPercentage}
                    day={dayNum}
                    isPaywalled={day >= daysEnabled}
                    numDays={days.length}
                  />
                );
              })}
              <ContinuousGraphContainer
                device={device}
                isSurfGraph
                showTimeLabels
                isHourly={isHourly}
                numDays={days?.length}
                days={days}
                highlightActiveColumn
                timestamps={timestamps}
                numColumns={graphData.length}
                currentMarkerXPercentage={currentMarkerXPercentage}
              >
                <div className="quiver-forecast-graphs__surf__chart">
                  <SurfLineGraph
                    dateFormat={dateFormat}
                    days={days}
                    device={device}
                    numberOfTicks={3}
                    isHourly={isHourly}
                    mobile={mobile}
                    overallMaxHeight={wave.overallMaxSurfHeight}
                    data={graphData}
                    utcOffset={wave.utcOffset}
                    units={wave.units.waveHeight}
                    currentMarkerXPercentage={
                      days.indexOf(0) !== -1 ? currentMarkerXPercentage : 0
                    }
                    isMultiCamPage={isMultiCamPage}
                  />
                </div>
              </ContinuousGraphContainer>
            </>
          );
        }
      )}
    </div>
  </div>
);

ForecastSurfLineGraph.propTypes = {
  wave: waveChartPropType.isRequired,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  sunlightTimes: sunlightTimesPropType.isRequired,
  daysEnabled: PropTypes.number.isRequired,
  mobile: PropTypes.bool,
  isHourly: PropTypes.bool.isRequired,
  device: devicePropType.isRequired,
  currentMarkerXPercentage: PropTypes.number,
  currGraphType: PropTypes.string,
  changeGraphType: PropTypes.func.isRequired,
  showTitle: PropTypes.bool,
  isMultiCamPage: PropTypes.bool,
  dateFormat: dateFormatPropType,
  timestamps: PropTypes.arrayOf(
    PropTypes.shape({
      timestamp: PropTypes.number,
      utcOffset: PropTypes.number,
    })
  ).isRequired,
};

ForecastSurfLineGraph.defaultProps = {
  currGraphType: 'BAR_GRAPH',
  mobile: false,
  currentMarkerXPercentage: 0,
  showTitle: true,
  isMultiCamPage: false,
  dateFormat: defaultDateFormat,
};

export default ForecastSurfLineGraph;
