/* istanbul ignore file */

const SECONDS_PER_DAY = 86400;

const sunlightTimeXPercentage = (type, index, sunlightTimes) => {
  const day = sunlightTimes?.days?.[index];
  if (!day) return null;
  const secondsFromMidnight = day[type] - day.midnight;
  return secondsFromMidnight / SECONDS_PER_DAY;
};

export default sunlightTimeXPercentage;
