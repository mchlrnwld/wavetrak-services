/* istanbul ignore file */

import PropTypes from 'prop-types';
import React from 'react';
import ChartPrimarySpot from '../ChartPrimarySpot';
import GraphContainer from '../GraphContainer';
import WeatherGraph from '../WeatherGraph';
import renderGraphs from './renderGraphs';
import dayClassName from './dayClassName';
import sunlightTimeXPercentage from './sunlightTimeXPercentage';
import ForecastGraphsUnits from './ForecastGraphsUnits';
import weatherChartPropType from '../PropTypes/weatherChart';
import devicePropType from '../PropTypes/device';
import SunlightTimes from './SunlightTimes';
import { sunlightTimesPropType } from '../PropTypes/sunlightTimes';

const ForecastGraphsWeather = ({
  type,
  weather,
  days,
  daysEnabled,
  sunlightTimes,
  spotId,
  spotName,
  isHourly,
  device,
  currentMarkerXPercentage,
  useNextLink,
}) => (
  <div className="quiver-forecast-graphs__weather">
    <h3 className="quiver-forecast-graphs__chart-title quiver-forecast-graphs__chart-title__weather-title">
      Weather{' '}
      <ForecastGraphsUnits
        units={`º${weather.units && weather.units.temperature}`}
      />
      {type === 'REGIONAL' && spotName ? (
        <ChartPrimarySpot
          className="quiver-forecast-graphs__chart-title__link"
          id={spotId}
          name={spotName}
          useNextLink={useNextLink}
        />
      ) : null}
    </h3>
    <div className="quiver-forecast-graphs__weather__container">
      {renderGraphs(
        weather.loading,
        weather.error,
        weather.message,
        'weather',
        () =>
          days.map((day) => {
            const data = isHourly ? weather.hourly[0] : weather.days[day];
            return (
              <div
                key={data[0].timestamp}
                className={dayClassName(day, 'weather', daysEnabled)}
              >
                <GraphContainer
                  device={device}
                  columns={data.length}
                  dawnXPercentage={sunlightTimeXPercentage(
                    'dawn',
                    day,
                    sunlightTimes
                  )}
                  duskXPercentage={sunlightTimeXPercentage(
                    'dusk',
                    day,
                    sunlightTimes
                  )}
                  showTimeLabels
                  highlightActiveColumn
                  isHourly={isHourly}
                  day={day}
                  timestamps={data}
                  currentMarkerXPercentage={currentMarkerXPercentage}
                >
                  <div className="quiver-forecast-graphs__weather__chart">
                    <WeatherGraph
                      weatherIconPath={weather.weatherIconPath}
                      data={data}
                    />
                  </div>
                </GraphContainer>
                <SunlightTimes sunlightTimes={sunlightTimes} day={day} />
              </div>
            );
          })
      )}
    </div>
  </div>
);

ForecastGraphsWeather.propTypes = {
  type: PropTypes.oneOf(['SPOT', 'REGIONAL']).isRequired,
  weather: weatherChartPropType.isRequired,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  daysEnabled: PropTypes.number.isRequired,
  sunlightTimes: sunlightTimesPropType.isRequired,
  spotId: PropTypes.string.isRequired,
  spotName: PropTypes.string,
  isHourly: PropTypes.bool.isRequired,
  device: devicePropType.isRequired,
  currentMarkerXPercentage: PropTypes.number,
  useNextLink: PropTypes.bool,
};

ForecastGraphsWeather.defaultProps = {
  spotName: null,
  currentMarkerXPercentage: null,
  useNextLink: false,
};

export default ForecastGraphsWeather;
