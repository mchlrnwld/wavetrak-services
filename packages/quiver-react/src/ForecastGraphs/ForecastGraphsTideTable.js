/* istanbul ignore file */

import React from 'react';
import round from 'lodash/round';
import PropTypes from 'prop-types';
import { format } from 'date-fns';
import { color } from '@surfline/quiver-themes';
import { getLocalDate } from '@surfline/web-common';
import dayClassName from './dayClassName';
import tideChartPropType from '../PropTypes/tideChart';

/**
 * @typedef {import('../PropTypes/tideChart').TidePoint & { type: 'HIGH' | 'LOW' }} HighLowTidePoint
 */

/**
 *
 * @param {object} Props
 * @param {number[]} Props.days
 * @param {HighLowTidePoint[][]} Props.tideHighsLows
 * @param {number} Props.numDays
 * @param {import('../PropTypes/tideChart').Tide} Props.tide
 * @param {number} Props.daysEnabled
 */
const ForecastGraphsTideTable = ({
  days,
  tideHighsLows,
  numDays,
  tide,
  daysEnabled,
}) =>
  days.map((day, index) => {
    const tidePoints = tideHighsLows[index];
    const prevDayNumPoints = tideHighsLows[index - 1]?.length;
    const currDayNumPoints = tidePoints?.length;
    const prevDayPointsMatch = prevDayNumPoints === currDayNumPoints;
    const isFirstCol = index === 0;
    const prevDayPointMismatch = !prevDayPointsMatch
      ? currDayNumPoints - prevDayNumPoints
      : 0;
    const border = `1px solid ${color('blue-gray', 20)}`;
    const tableStyle = {
      width: `${100 / numDays}%`,
    };
    const trStyle = {
      border: 'unset',
      display: 'block',
      borderLeft: border,
      marginLeft: '-1.5px',
    };
    return (
      <table
        key={day}
        style={tableStyle}
        className="quiver-forecast-graphs__table"
      >
        <tbody>
          {tidePoints?.map((point, ptIndex) => {
            const needsLeftBorder =
              !(ptIndex < currDayNumPoints - prevDayPointMismatch) &&
              !isFirstCol;
            return point.type !== 'NORMAL' ? (
              <tr
                key={point.timestamp}
                className={dayClassName(day, 'tide', daysEnabled)}
              >
                <td style={needsLeftBorder ? trStyle : null}>
                  {point.type.toLowerCase()}
                </td>
                <td>
                  {format(
                    getLocalDate(
                      point.timestamp,
                      point.utcOffset ?? tide.utcOffset
                    ),
                    "h:mmaaaaa'm'"
                  )}
                </td>
                <td>
                  {round(point.height, 1)}
                  {tide.units.tideHeight.toLowerCase()}
                </td>
              </tr>
            ) : null;
          })}
          {!tidePoints ? (
            <tr>
              <td
                colSpan={3}
                style={{ 'font-weight': '100', borderBottom: border }}
              >
                <i>no data available</i>
              </td>
            </tr>
          ) : null}
        </tbody>
      </table>
    );
  });

ForecastGraphsTideTable.propTypes = {
  tide: tideChartPropType.isRequired,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  tideHighsLows: PropTypes.arrayOf(
    PropTypes.arrayOf(
      PropTypes.shape({
        type: PropTypes.oneOf(['HIGH', 'LOW']),
        timestamp: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        height: PropTypes.number,
      })
    )
  ).isRequired,
  numDays: PropTypes.number.isRequired,
  daysEnabled: PropTypes.number.isRequired,
};

export default ForecastGraphsTideTable;
