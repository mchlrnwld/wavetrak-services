import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import Skeleton from 'react-loading-skeleton';
import AutomatedRatings from './AutomatedRatings';

import createAutomatedRatingsFixtures from './fixtures/automatedRatings';

describe('components / ForecastGraphs / AutomatedRatings', () => {
  const { automatedRatings, automatedRatingsError, automatedRatingsLoading } =
    createAutomatedRatingsFixtures();

  it('should render an empty node when there is an error', () => {
    const wrapper = mount(
      <AutomatedRatings automatedRatings={automatedRatingsError} days={[0]} />
    );

    expect(wrapper).to.be.empty();
  });

  it('should render an loading skeleton when loading', () => {
    const wrapper = mount(
      <AutomatedRatings automatedRatings={automatedRatingsLoading} days={[0]} />
    );

    expect(wrapper.find(Skeleton)).to.have.length(1);
  });

  it('should render bars for each day', () => {
    const wrapper = mount(
      <AutomatedRatings automatedRatings={automatedRatings} days={[0, 1]} />
    );

    expect(wrapper.find('.quiver-automated-ratings__day')).to.have.length(2);
    expect(wrapper.find('.quiver-automated-ratings__bar')).to.have.length(16);
    expect(
      wrapper.find('.quiver-automated-ratings__bar--good').length
    ).to.be.greaterThan(1);
  });

  it('should render bars for each day - hourly', () => {
    const wrapper = mount(
      <AutomatedRatings
        automatedRatings={automatedRatings}
        days={[0]}
        isHourly
      />
    );

    expect(wrapper.find('.quiver-automated-ratings__day')).to.have.length(1);
    expect(wrapper.find('.quiver-automated-ratings__bar')).to.have.length(24);
    expect(
      wrapper.find('.quiver-automated-ratings__bar--fair').length
    ).to.be.greaterThan(1);
  });
});
