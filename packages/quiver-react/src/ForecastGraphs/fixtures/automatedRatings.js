import { chunk } from 'lodash';

const HOUR_IN_SECONDS = 3600;

const RATINGS = [
  'VERY_POOR',
  'POOR',
  'POOR_TO_FAIR',
  'FAIR',
  'FAIR_TO_GOOD',
  'GOOD',
  'EPIC',
];

const createAutomatedRatingsFixtures = () => {
  const date = new Date();
  const ts = +date / 1000;

  const ratings = [...new Array(48)].map((_, i) => ({
    // Derive the timestamp
    timestamp: ts + i * HOUR_IN_SECONDS,
    // We'll do the DST shift at the second data point
    utcOffset: 0,
    // Deterministic value of rating so that we can target test to it more easily
    rating: RATINGS[i % 7],
  }));
  const hourly = chunk(ratings, 24);
  const days = hourly.map((day) => day.filter((_, i) => i % 3 === 0));

  const automatedRatings = {
    error: '',
    loading: false,
    location: {
      lat: 0,
      lon: -1,
    },
    hourly,
    days,
  };

  const automatedRatingsLoading = {
    error: '',
    loading: true,
    location: {
      lat: 0,
      lon: -1,
    },
    hourly: null,
    days: null,
  };

  const automatedRatingsError = {
    error: 'There was an error',
    loading: false,
    location: {
      lat: 0,
      lon: -1,
    },
    hourly: null,
    days: null,
  };

  return { automatedRatings, automatedRatingsError, automatedRatingsLoading };
};

export default createAutomatedRatingsFixtures;
