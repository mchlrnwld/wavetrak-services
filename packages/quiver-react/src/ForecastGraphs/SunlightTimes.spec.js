import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SunlightTimes from './SunlightTimes';

describe('components / ForecastGraphs / SunlightTimes', () => {
  const sunlightTimes = {
    utcOffset: 0,
    days: [
      {
        // Fri Jan 01 2021 06:00:00 GMT+0000
        dawn: 1609480800,
        dawnUTCOffset: 0,
        // Fri Jan 01 2021 07:00:00 GMT+0000
        sunrise: 1609484400,
        sunriseUTCOffset: -1,
        // Thu Dec 31 2020 18:00:00 GMT+0000
        sunset: 1609437600,
        sunsetUTCOffset: -1,
        // Thu Dec 31 2020 19:00:00 GMT+0000
        dusk: 1609441200,
        duskUTCOffset: -1,
      },
    ],
  };

  it('renders first light, sunrise, sunset, and last light - using utc offsets', () => {
    const wrapper = shallow(
      <SunlightTimes sunlightTimes={sunlightTimes} day={0} />
    );
    const dawn = wrapper.find('.quiver-forecast-graphs__table__dawn');
    const sunrise = wrapper.find('.quiver-forecast-graphs__table__sunrise');
    const sunset = wrapper.find('.quiver-forecast-graphs__table__sunset');
    const dusk = wrapper.find('.quiver-forecast-graphs__table__dusk');

    expect(dawn).to.have.length(1);
    expect(dawn.text()).to.be.equal('6:00am');

    expect(sunrise).to.have.length(1);
    expect(sunrise.text()).to.be.equal('6:00am');

    expect(sunset).to.have.length(1);
    expect(sunset.text()).to.be.equal('5:00pm');

    expect(dusk).to.have.length(1);
    expect(dusk.text()).to.be.equal('6:00pm');
  });
});
