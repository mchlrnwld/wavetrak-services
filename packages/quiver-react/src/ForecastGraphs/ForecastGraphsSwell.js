/* istanbul ignore file */

import PropTypes from 'prop-types';
import React from 'react';
import round from 'lodash/round';
import { TABLET_LARGE_WIDTH } from '@surfline/web-common';
import ContinuousGraphContainer from '../ContinuousGraphContainer';
import OptimalScoreIndicator from '../OptimalScoreIndicator';
import OptimalScoreLegend from '../OptimalScoreLegend';
import SwellForecastLocation from '../SwellForecastLocation';
import SwellGraph from '../SwellGraph';
import { sunlightTimesPropType } from '../PropTypes/sunlightTimes';
import waveChartPropType from '../PropTypes/waveChart';
import devicePropType from '../PropTypes/device';
import renderGraphs from './renderGraphs';
import sunlightTimeXPercentage from './sunlightTimeXPercentage';
import ForecastGraphsUnits from './ForecastGraphsUnits';
import DayNightShading from '../DayNightShading';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';

const ForecastGraphsSwell = ({
  showTitle,
  type,
  wave,
  days,
  daysEnabled,
  sunlightTimes,
  mobile,
  showOptimalConditions,
  currentMarkerXPercentage,
  activeSwell,
  setActiveSwell,
  isHourly,
  device,
  isMultiCamPage,
  dateFormat,
  timestamps,
}) => {
  const isMobile = device.width < TABLET_LARGE_WIDTH;
  return (
    <div className="quiver-forecast-graphs__swell">
      {showTitle && (
        <h3 className="quiver-forecast-graphs__chart-title">
          Swell
          <ForecastGraphsUnits units={wave.units && wave.units.waveHeight} />
          {wave.forecastLocation ? (
            <SwellForecastLocation
              className="quiver-forecast-graphs__chart-title__link"
              lat={round(
                wave[type === 'SPOT' ? 'offshoreLocation' : 'forecastLocation']
                  .lat,
                2
              )}
              lon={round(
                wave[type === 'SPOT' ? 'offshoreLocation' : 'forecastLocation']
                  .lon,
                2
              )}
            />
          ) : null}
          {showOptimalConditions ? <OptimalScoreLegend /> : null}
        </h3>
      )}
      <div onMouseLeave={() => setActiveSwell(null)}>
        {renderGraphs(
          wave.loading || sunlightTimes.loading,
          wave.error,
          wave.message,
          'swell',
          () => {
            const graphData = days
              .map((day) => (isHourly ? wave.hourly[day] : wave.days[day]))
              .flat();
            const optimalScores = graphData.map((d) => ({
              score: Math.max(...d.swells.map((s) => s.optimalScore)),
              key: d.timestamp,
            }));

            return (
              <>
                <div className="quiver-forecast-graphs__swell__container">
                  {days.map((day, dayNum) => {
                    const dawnXPercentage = sunlightTimeXPercentage(
                      'dawn',
                      day,
                      sunlightTimes
                    );
                    const duskXPercentage = sunlightTimeXPercentage(
                      'dusk',
                      day,
                      sunlightTimes
                    );
                    return (
                      <DayNightShading
                        key={day}
                        dawnXPercentage={dawnXPercentage}
                        duskXPercentage={duskXPercentage}
                        day={dayNum}
                        isPaywalled={day >= daysEnabled}
                        numDays={days.length}
                      />
                    );
                  })}
                  <ContinuousGraphContainer
                    showTimeLabels
                    isSwellGraph
                    highlightActiveColumn
                    device={device}
                    isHourly={isHourly}
                    numDays={days?.length}
                    days={days}
                    optimalScores={
                      showOptimalConditions
                        ? optimalScores.map((s) => s.score)
                        : null
                    }
                    numColumns={graphData.length}
                    timestamps={timestamps}
                    currentMarkerXPercentage={currentMarkerXPercentage}
                  >
                    <div className="quiver-forecast-graphs__swell__chart">
                      <SwellGraph
                        dateFormat={dateFormat}
                        device={device}
                        numberOfTicks={3}
                        mobile={mobile}
                        lat={wave.offshoreLocation.lat}
                        lon={wave.offshoreLocation.lon}
                        mapZoom={type === 'SPOT' ? 6 : 4}
                        overallMaxHeight={wave.overallMaxSwellHeight}
                        data={graphData}
                        isHourly={isHourly}
                        utcOffset={wave.utcOffset}
                        units={wave.units.swellHeight}
                        showOptimalConditions={showOptimalConditions}
                        activeSwell={activeSwell}
                        onHoverSwell={isMobile ? null : setActiveSwell}
                        obscureInactiveSwells={
                          activeSwell !== null && !isMobile
                        }
                        days={days}
                        isMultiCamPage={isMultiCamPage}
                      />
                    </div>
                  </ContinuousGraphContainer>
                </div>
                {showOptimalConditions ? (
                  <div className="quiver-forecast-graphs__optimal-score">
                    {optimalScores.map((optimalScore) => (
                      <OptimalScoreIndicator
                        key={graphData[0].timestamp + optimalScore.key}
                        optimalScore={optimalScore.score}
                      />
                    ))}
                  </div>
                ) : null}
              </>
            );
          }
        )}
      </div>
    </div>
  );
};

ForecastGraphsSwell.propTypes = {
  showTitle: PropTypes.bool,
  type: PropTypes.oneOf(['SPOT', 'REGIONAL']).isRequired,
  wave: waveChartPropType.isRequired,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  sunlightTimes: sunlightTimesPropType.isRequired,
  daysEnabled: PropTypes.number.isRequired,
  mobile: PropTypes.bool,
  showOptimalConditions: PropTypes.bool,
  activeSwell: PropTypes.number,
  setActiveSwell: PropTypes.func,
  isHourly: PropTypes.bool.isRequired,
  device: devicePropType.isRequired,
  currentMarkerXPercentage: PropTypes.number,
  isMultiCamPage: PropTypes.bool,
  dateFormat: dateFormatPropType,
  timestamps: PropTypes.arrayOf(
    PropTypes.shape({
      timestamp: PropTypes.number,
      utcOffset: PropTypes.number,
    })
  ).isRequired,
};

ForecastGraphsSwell.defaultProps = {
  mobile: false,
  showOptimalConditions: false,
  activeSwell: null,
  setActiveSwell: null,
  currentMarkerXPercentage: 0,
  showTitle: true,
  isMultiCamPage: false,
  dateFormat: defaultDateFormat,
};

export default ForecastGraphsSwell;
