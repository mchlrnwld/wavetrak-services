/* istanbul ignore file */

import React from 'react';
import ErrorBoundary from '../ErrorBoundary';
import ErrorBox from '../ErrorBox';
import ChartLoading from '../ChartLoading';
import ChartMessage from '../ChartMessage';

const renderGraphs = (loading, error, message, chart, render) => (
  <ErrorBoundary error={error} render={() => <ErrorBox type={chart} />}>
    {(() => {
      if (error) return null;
      if (loading) return <ChartLoading chart={chart} />;
      if (message) return <ChartMessage message={message} />;
      return render();
    })()}
  </ErrorBoundary>
);

export default renderGraphs;
