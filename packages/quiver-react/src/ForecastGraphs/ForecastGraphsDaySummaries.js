/* istanbul ignore file */

import PropTypes from 'prop-types';
import React from 'react';
import ConditionDaySummary from '../ConditionDaySummary';
import ConditionDaySummaryV2 from '../ConditionDaySummaryV2';
import summaryPropType from '../PropTypes/summary';
import renderGraphs from './renderGraphs';

const ForecastGraphsDaySummaries = ({
  type,
  summary,
  days,
  daysEnabled,
  lastUpdate,
  conditionsV2,
}) => (
  <div className="quiver-forecast-graphs__day-summaries">
    {renderGraphs(
      summary.loading,
      summary.error,
      summary.message,
      'summary',
      () =>
        days.map((day) => {
          const data = summary.days[day];
          return (
            <div
              key={data.timestamp}
              className="quiver-forecast-graphs__day-summary"
            >
              {conditionsV2 ? (
                <ConditionDaySummaryV2
                  utcOffset={summary.utcOffset}
                  units={summary.units}
                  data={data}
                  showAMPM={type === 'REGIONAL'}
                  obscured={day >= daysEnabled}
                  lastUpdate={lastUpdate}
                />
              ) : (
                <ConditionDaySummary
                  utcOffset={summary.utcOffset}
                  units={summary.units}
                  data={data}
                  showAMPM={type === 'REGIONAL'}
                  obscured={day >= daysEnabled}
                  lastUpdate={lastUpdate}
                />
              )}
            </div>
          );
        })
    )}
  </div>
);

ForecastGraphsDaySummaries.propTypes = {
  type: PropTypes.oneOf(['SPOT', 'REGIONAL']).isRequired,
  summary: summaryPropType.isRequired,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  daysEnabled: PropTypes.number.isRequired,
  lastUpdate: PropTypes.string,
  conditionsV2: PropTypes.bool,
};

ForecastGraphsDaySummaries.defaultProps = {
  lastUpdate: null,
  conditionsV2: false,
};

export default ForecastGraphsDaySummaries;
