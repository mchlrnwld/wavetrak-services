/* istanbul ignore file */

import PropTypes from 'prop-types';
import React from 'react';
import SurfGraph from '../SurfGraph';
import ContinuousGraphContainer from '../ContinuousGraphContainer';
import renderGraphs from './renderGraphs';
import sunlightTimeXPercentage from './sunlightTimeXPercentage';
import ForecastGraphsUnits from './ForecastGraphsUnits';
import DayNightShading from '../DayNightShading';
import ForecastGraphOptionDropdown from './ForecastGraphOptionDropdown';
import { sunlightTimesPropType } from '../PropTypes/sunlightTimes';
import waveChartPropType from '../PropTypes/waveChart';
import devicePropType from '../PropTypes/device';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';
import { automatedRatingsPropType } from '../PropTypes/automatedRatings';

/**
 * @typedef {object} Props
 * @property {import('../PropTypes/automatedRatings').AutomatedRatings} automatedRatings
 * @property {boolean} isHourly
 * @property {number[]} days
 * @property {number} daysEnabled
 */

/** @type {React.FC<Props>} */
const ForecastGraphsSurf = React.memo(
  ({
    wave,
    days,
    daysEnabled,
    sunlightTimes,
    isHourly,
    device,
    currGraphType,
    changeGraphType,
    currentMarkerXPercentage,
    showTitle,
    isMultiCamPage,
    dateFormat,
    timestamps,
    automatedRatings,
  }) => (
    <div className="quiver-forecast-graphs__surf">
      {showTitle && (
        <h3 className="quiver-forecast-graphs__chart-title">
          Surf{' '}
          <ForecastGraphsUnits units={wave.units && wave.units.waveHeight} />
          <ForecastGraphOptionDropdown
            currGraphType={currGraphType}
            onClick={changeGraphType}
          />
        </h3>
      )}
      <div className="quiver-forecast-graphs__surf__container">
        {renderGraphs(
          wave.loading || sunlightTimes.loading,
          wave.error,
          wave.message,
          'surf',
          () => {
            const graphData = days
              .map((day) => (isHourly ? wave.hourly[day] : wave.days[day]))
              .flat();

            const ratings =
              automatedRatings &&
              !automatedRatings.error &&
              !automatedRatings.loading &&
              days
                .map((day) =>
                  isHourly
                    ? automatedRatings.hourly[day]
                    : automatedRatings.days[day]
                )
                .flat();

            return (
              <>
                {days.map((day, dayNum) => {
                  const dawnXPercentage = sunlightTimeXPercentage(
                    'dawn',
                    day,
                    sunlightTimes
                  );
                  const duskXPercentage = sunlightTimeXPercentage(
                    'dusk',
                    day,
                    sunlightTimes
                  );
                  return (
                    <DayNightShading
                      key={day}
                      dawnXPercentage={dawnXPercentage}
                      duskXPercentage={duskXPercentage}
                      day={dayNum}
                      isPaywalled={day >= daysEnabled}
                      numDays={days.length}
                    />
                  );
                })}
                <ContinuousGraphContainer
                  device={device}
                  showTimeLabels
                  isSurfGraph
                  days={days}
                  isHourly={isHourly}
                  numDays={days?.length}
                  highlightActiveColumn
                  currentMarkerXPercentage={currentMarkerXPercentage}
                  timestamps={timestamps}
                  allowMobileTooltip
                >
                  <div className="quiver-forecast-graphs__surf__chart">
                    <SurfGraph
                      dateFormat={dateFormat}
                      device={device}
                      isMultiCamPage={isMultiCamPage}
                      overallMaxHeight={wave.overallMaxSurfHeight}
                      data={graphData}
                      isHourly={isHourly}
                      swellUnits={wave.units.swellHeight}
                      surfUnits={wave.units.waveHeight}
                      utcOffset={wave.utcOffset}
                      currentMarkerXPercentage={
                        days.indexOf(0) !== -1 ? currentMarkerXPercentage : 0
                      }
                      ratings={ratings}
                      automatedRatings={automatedRatings}
                    />
                  </div>
                </ContinuousGraphContainer>
              </>
            );
          }
        )}
      </div>
    </div>
  )
);

ForecastGraphsSurf.propTypes = {
  isMultiCamPage: PropTypes.bool,
  wave: waveChartPropType.isRequired,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  daysEnabled: PropTypes.number.isRequired,
  sunlightTimes: sunlightTimesPropType.isRequired,
  isHourly: PropTypes.bool.isRequired,
  device: devicePropType.isRequired,
  currGraphType: PropTypes.string,
  changeGraphType: PropTypes.func.isRequired,
  currentMarkerXPercentage: PropTypes.number,
  showTitle: PropTypes.bool,
  dateFormat: dateFormatPropType,
  timestamps: PropTypes.arrayOf(
    PropTypes.shape({
      timestamp: PropTypes.number,
      utcOffset: PropTypes.number,
    })
  ).isRequired,
  automatedRatings: automatedRatingsPropType.isRequired,
};

ForecastGraphsSurf.defaultProps = {
  currGraphType: 'BAR_GRAPH',
  currentMarkerXPercentage: 0,
  showTitle: true,
  isMultiCamPage: false,
  dateFormat: defaultDateFormat,
};

export default ForecastGraphsSurf;
