/* istanbul ignore file */

import PropTypes from 'prop-types';
import React, { useRef, useState, useCallback, useMemo } from 'react';
import {
  currentGraphMarkerXPercentageFn as currentMarkerXPercentageFn,
  trackEvent,
  getLocalDateForFutureDay,
} from '@surfline/web-common';
import { useCookies } from 'react-cookie';
import { format } from 'date-fns';

import { sunlightTimesPropType } from '../PropTypes/sunlightTimes';
import summaryPropType from '../PropTypes/summary';
import waveChartPropType from '../PropTypes/waveChart';
import tideChartPropType from '../PropTypes/tideChart';
import { automatedRatingsPropType } from '../PropTypes/automatedRatings';
import devicePropType from '../PropTypes/device';
import weatherChartPropType from '../PropTypes/weatherChart';
import windChartPropType from '../PropTypes/windChart';
import ForecastGraphsDays from './ForecastGraphsDays';
import ForecastGraphsDaySummaries from './ForecastGraphsDaySummaries';
import ForecastGraphsSurf from './ForecastGraphsSurf';
import ForecastGraphsSwell from './ForecastGraphsSwell';
import ForecastGraphsTide from './ForecastGraphsTide';
import ForecastGraphsWeather from './ForecastGraphsWeather';
import ForecastGraphsWind from './ForecastGraphsWind';
import ForecastGraphsTabArrows from './ForecastGraphsTabArrows';
import ForecastGraphsSurfLineGraph from './ForecastGraphsSurfLine';
import AutomatedRatings from './AutomatedRatings';
import ForecastGraphsDaySummariesCTA from '../ForecastGraphsDaySummariesCTA';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';

const SPOT = 'SPOT';
const REGIONAL = 'REGIONAL';
const BAR_GRAPH = 'BAR_GRAPH';
const AREA_GRAPH = 'AREA_GRAPH';

/**
 * @typedef {object} Props
 * @property {import('../PropTypes/automatedRatings').AutomatedRatings} automatedRatings
 * @property {SPOT | REGIONAL} type
 * @property {string} subregionId
 * @property {string} subregionName
 * @property {string} spotId
 * @property {string} spotName
 * @property {string} pageName
 * @property {boolean} isHourly
 * @property {boolean} extended
 * @property {number[]} days
 * @property {number} daysEnabled
 * @property {string} lastUpdate
 * @property {boolean} mobile
 * @property {boolean} conditionsV2
 * @property {boolean} [useNextLink]
 */

/** @type {React.FC<Props>} */
const ForecastGraphs = ({
  type,
  subregionId,
  subregionName,
  spotId,
  spotName,
  pageName,
  isHourly,
  extended,
  days,
  daysEnabled,
  lastUpdate,
  mobile,
  summary,
  sunlightTimes,
  wave,
  tide,
  weather,
  wind,
  utcOffset,
  showCTA,
  now,
  activeSwell,
  setActiveSwell,
  tabForwards,
  tabBackwards,
  isPremium,
  spotPage,
  device,
  changeToHourlyView,
  CTAComponent,
  dateFormat,
  startDate,
  endDate,
  conditionsV2,
  automatedRatings,
  useNextLink,
  showForecastGraphsDaySummariesCTA,
  isAnonymous,
}) => {
  const [cookies, setCookie] = useCookies(['surfGraphType']);
  const surfGraphTypeCookie = useRef(cookies.surfGraphType);
  const [surfGraphType, setSurfGraphType] = useState(
    surfGraphTypeCookie || BAR_GRAPH
  );

  const changeSurfGraphType = (newType) => {
    const trackGraphToggle = () =>
      trackEvent('Toggled Spot Forecast', {
        category: type === SPOT ? 'spot forecast' : 'regional forecast',
        subregionId,
        subregionName,
        spotId,
        spotName,
        pageName,
        buttonAction: newType === BAR_GRAPH ? 'Bar Graph' : 'Area Graph',
        view: isHourly ? 'Hourly Forecast' : '17-Day Spot Forecast',
      });

    setSurfGraphType(newType);
    trackGraphToggle();
    setCookie('surfGraphType', newType);
  };

  const columns = isHourly ? 24 : 8;
  const surfColumns = isHourly ? 24 : 4;
  const currentMarkerXPercentage = currentMarkerXPercentageFn(
    sunlightTimes,
    now
  );
  const currentMarkerContinuousGraphXPercentage =
    currentMarkerXPercentage / days.length;
  const showOptimalConditions = type === 'SPOT' && extended;
  const showLineGraph = surfGraphType === AREA_GRAPH;

  const utcOffsetForMidnightOfSelectedDay =
    wave.days?.[days[0]]?.midnightUTCOffset || utcOffset;

  const localDate = getLocalDateForFutureDay(
    days[0],
    utcOffsetForMidnightOfSelectedDay
  );

  const getTimestamps = useCallback(
    /**
     * @param {'wave' | 'wind' | 'tide' | 'weather'} graphType
     * @returns {{ timestamp: number, utcOffset: number }[]}
     */
    (graphType) => {
      const datum = {
        wave,
        weather,
      };
      // The tide graph data does not have an `hourly` property (only `days`)
      const dataKey = isHourly && graphType !== 'tide' ? 'hourly' : 'days';

      return days
        .map((day) =>
          datum[graphType][dataKey]?.[day].map((point) => ({
            timestamp: point.timestamp,
            utcOffset: point.utcOffset,
          }))
        )
        .flat();
    },
    [wave, weather, isHourly, days]
  );

  const segmentProperties = useMemo(
    () => ({
      location: 'Colored Boxes-Region Forecast',
      subregionId,
      subregionName,
      spotId,
      spotName,
      pageName,
    }),
    [subregionId, subregionName, spotId, spotName, pageName]
  );

  return (
    <div className="quiver-forecast-graphs">
      <div className="quiver-forecast-graphs__container">
        {spotPage && isHourly && (
          <h5 className="quiver-forecast-graphs-hourly-day">
            {format(localDate, dateFormat === 'MDY' ? 'iiii M/d' : 'iiii d/M')}
          </h5>
        )}
        {(isHourly || isPremium) && (
          <ForecastGraphsTabArrows
            tabForwards={tabForwards}
            tabBackwards={tabBackwards}
            days={days}
            daysEnabled={daysEnabled}
          />
        )}
        {extended ? (
          <div>
            <ForecastGraphsDays
              dateFormat={dateFormat}
              utcOffset={utcOffset}
              days={days}
              daysEnabled={daysEnabled}
            />
            {(() => {
              if (!spotPage && showForecastGraphsDaySummariesCTA)
                return (
                  <ForecastGraphsDaySummariesCTA
                    isAnonymous={isAnonymous}
                    days={days}
                    segmentProperties={segmentProperties}
                  />
                );
              if (!spotPage)
                return (
                  <ForecastGraphsDaySummaries
                    type={type}
                    summary={summary}
                    days={days}
                    daysEnabled={daysEnabled}
                    lastUpdate={lastUpdate}
                    conditionsV2={conditionsV2}
                  />
                );
              return null;
            })()}
          </div>
        ) : null}
        <div className="quiver-forecast-graphs__graphs">
          {type === SPOT && !showLineGraph && (
            <ForecastGraphsSurf
              dateFormat={dateFormat}
              wave={wave}
              device={device}
              currGraphType={surfGraphType}
              changeGraphType={() => changeSurfGraphType(AREA_GRAPH)}
              days={days}
              daysEnabled={daysEnabled}
              columns={surfColumns}
              sunlightTimes={sunlightTimes}
              isHourly={isHourly}
              currentMarkerXPercentage={currentMarkerXPercentage}
              timestamps={getTimestamps('wave')}
              automatedRatings={automatedRatings}
            />
          )}
          {type === SPOT && showLineGraph && (
            <ForecastGraphsSurfLineGraph
              dateFormat={dateFormat}
              type={type}
              currGraphType={surfGraphType}
              changeGraphType={() => changeSurfGraphType(BAR_GRAPH)}
              wave={wave}
              days={days}
              daysEnabled={daysEnabled}
              columns={surfColumns}
              sunlightTimes={sunlightTimes}
              mobile={mobile}
              device={device}
              activeSwell={activeSwell}
              setActiveSwell={setActiveSwell}
              isHourly={isHourly}
              currentMarkerXPercentage={currentMarkerXPercentage}
              timestamps={getTimestamps('wave')}
            />
          )}
          {type === SPOT && automatedRatings && (
            <AutomatedRatings
              automatedRatings={automatedRatings}
              isHourly={isHourly}
              days={days}
            />
          )}
          <ForecastGraphsSwell
            dateFormat={dateFormat}
            type={type}
            wave={wave}
            days={days}
            daysEnabled={daysEnabled}
            sunlightTimes={sunlightTimes}
            mobile={mobile}
            device={device}
            showOptimalConditions={showOptimalConditions}
            activeSwell={activeSwell}
            setActiveSwell={setActiveSwell}
            isHourly={isHourly}
            currentMarkerXPercentage={currentMarkerXPercentage}
            timestamps={getTimestamps('wave')}
          />
          <ForecastGraphsWind
            dateFormat={dateFormat}
            type={type}
            extended={extended}
            device={device}
            wind={wind}
            days={days}
            daysEnabled={daysEnabled}
            columns={columns}
            sunlightTimes={sunlightTimes}
            spotId={spotId}
            spotName={spotName}
            showOptimalConditions={showOptimalConditions}
            isHourly={isHourly}
            currentMarkerXPercentage={currentMarkerXPercentage}
            changeToHourlyView={changeToHourlyView}
            isPremium={isPremium}
            useNextLink={useNextLink}
          />
          <ForecastGraphsTide
            dateFormat={dateFormat}
            device={device}
            tide={tide}
            days={days}
            daysEnabled={daysEnabled}
            sunlightTimes={sunlightTimes}
            currentMarkerXPercentage={currentMarkerContinuousGraphXPercentage}
            isHourly={isHourly}
            startDate={startDate}
            endDate={endDate}
          />
          <ForecastGraphsWeather
            type={type}
            weather={weather}
            days={days}
            device={device}
            daysEnabled={daysEnabled}
            columns={columns}
            sunlightTimes={sunlightTimes}
            spotId={spotId}
            spotName={spotName}
            isHourly={isHourly}
            useNextLink={useNextLink}
          />
        </div>
      </div>
      {showCTA && CTAComponent ? <CTAComponent /> : null}
    </div>
  );
};

ForecastGraphs.propTypes = {
  extended: PropTypes.bool,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  daysEnabled: PropTypes.number.isRequired,
  type: PropTypes.oneOf([SPOT, REGIONAL]).isRequired,
  lastUpdate: PropTypes.string,
  mobile: PropTypes.bool,
  pageName: PropTypes.string.isRequired,
  spotId: PropTypes.string.isRequired,
  subregionId: PropTypes.string,
  subregionName: PropTypes.string,
  spotName: PropTypes.string,
  summary: summaryPropType.isRequired,
  wave: waveChartPropType.isRequired,
  tide: tideChartPropType.isRequired,
  weather: weatherChartPropType.isRequired,
  wind: windChartPropType.isRequired,
  sunlightTimes: sunlightTimesPropType.isRequired,
  automatedRatings: automatedRatingsPropType.isRequired,
  utcOffset: PropTypes.number.isRequired,
  showCTA: PropTypes.bool,
  CTAComponent: PropTypes.node,
  now: PropTypes.number.isRequired,
  tabForwards: PropTypes.func.isRequired,
  tabBackwards: PropTypes.func.isRequired,
  activeSwell: PropTypes.number,
  setActiveSwell: PropTypes.func,
  isPremium: PropTypes.bool,
  spotPage: PropTypes.bool,
  isHourly: PropTypes.bool,
  device: devicePropType.isRequired,
  changeToHourlyView: PropTypes.func.isRequired,
  dateFormat: dateFormatPropType,
  startDate: PropTypes.instanceOf(Date).isRequired,
  endDate: PropTypes.instanceOf(Date).isRequired,
  conditionsV2: PropTypes.bool,
  useNextLink: PropTypes.bool,
  showForecastGraphsDaySummariesCTA: PropTypes.bool,
  isAnonymous: PropTypes.bool,
};

ForecastGraphs.defaultProps = {
  spotName: null,
  CTAComponent: false,
  mobile: false,
  extended: false,
  lastUpdate: null,
  subregionId: null,
  subregionName: null,
  showCTA: false,
  activeSwell: null,
  setActiveSwell: null,
  isPremium: true,
  spotPage: false,
  isHourly: true,
  dateFormat: defaultDateFormat,
  conditionsV2: false,
  useNextLink: false,
  showForecastGraphsDaySummariesCTA: false,
  isAnonymous: false,
};

export default ForecastGraphs;
