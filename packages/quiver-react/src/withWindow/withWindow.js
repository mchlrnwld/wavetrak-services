import React, { Component } from 'react';

const withWindow = (WrappedComponent) =>
  class extends Component {
    constructor() {
      super();
      this.state = { window: null };
    }

    componentDidMount() {
      // eslint-disable-next-line react/no-did-mount-set-state
      this.setState({ window });
    }

    render() {
      const { window: windowProp } = this.state;
      // eslint-disable-next-line react/jsx-props-no-spreading
      return <WrappedComponent window={windowProp} {...this.props} />;
    }
  };

export default withWindow;
