import PropTypes from 'prop-types';
import React from 'react';

const Checkbox = ({ copy, checked, onClick, checkboxName }) => {
  const addProps = {};
  if (checked) {
    addProps.checked = checked;
  }

  return (
    <div className="quiver-checkbox">
      <input
        type="checkbox"
        id={checkboxName}
        className="quiver-checkbox__checkbox"
        value={checked}
        onClick={onClick}
        {...addProps}
      />
      <label
        className={
          checked
            ? 'quiver-checkbox__checkbox__checked'
            : 'quiver-checkbox__checkbox__unchecked'
        }
        htmlFor={checkboxName}
      >
        {copy}
      </label>
    </div>
  );
};

Checkbox.propTypes = {
  copy: PropTypes.string.isRequired,
  checked: PropTypes.bool,
  onClick: PropTypes.func,
  checkboxName: PropTypes.string.isRequired,
};

Checkbox.defaultProps = {
  onClick: null,
  checked: false,
};

export default Checkbox;
