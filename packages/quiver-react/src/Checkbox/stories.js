import React from 'react';
import Checkbox from './Checkbox';

export default {
  title: 'Checkbox',
};

export const Checked = () => (
  <Checkbox
    checked
    copy="This is a checkbox that is checked"
    checkboxName="uniqueId"
  />
);

export const NotChecked = () => (
  <Checkbox
    copy="This is a checkbox that is not checked"
    checkboxName="uniqueId"
  />
);
