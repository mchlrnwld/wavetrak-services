import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Checkbox from './Checkbox';

describe('Checkbox', () => {
  it('should render with appropriate class names', () => {
    const checkedWrapper = shallow(
      <Checkbox
        checked
        copy="This is a checkbox that is checked"
        checkboxName="uniqueId"
      />
    );
    const notCheckedWrapper = shallow(
      <Checkbox
        copy="This is a checkbox that is not checked"
        checkboxName="uniqueId"
      />
    );

    const checkedContainer = checkedWrapper.find('div');
    const notCheckedContainer = notCheckedWrapper.find('div');
    expect(checkedContainer.prop('className')).to.equal('quiver-checkbox');
    expect(notCheckedContainer.prop('className')).to.equal('quiver-checkbox');

    const checkedLabel = checkedWrapper.find('label');
    const notCheckedLabel = notCheckedWrapper.find('label');
    expect(checkedLabel.prop('className')).to.equal(
      'quiver-checkbox__checkbox__checked'
    );
    expect(notCheckedLabel.prop('className')).to.equal(
      'quiver-checkbox__checkbox__unchecked'
    );
  });
});
