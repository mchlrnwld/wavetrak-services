import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';
import PersistentMediaPlayer from '.';

describe('PersistentMediaPlayer', () => {
  const props = {
    containerRef: {
      getBoundingClientRect: sinon.stub().returns({ rect: true }),
    },
    stickyToggleCallback: sinon.stub(),
  };

  before(() => {
    sinon.stub(window, 'addEventListener');
    sinon.stub(window, 'removeEventListener');
  });

  afterEach(() => {
    window.addEventListener.reset();
    window.removeEventListener.reset();
    props.containerRef.getBoundingClientRect();
    props.stickyToggleCallback.reset();
  });

  after(() => {
    window.addEventListener.restore();
    window.removeEventListener.restore();
  });

  it('should render the component', () => {
    const wrapper = shallow(
      <PersistentMediaPlayer {...props}>
        <div className="child" />
      </PersistentMediaPlayer>
    );
    const instance = wrapper.instance();
    const state = wrapper.state();
    expect(state.isMobile).to.be.false();
    expect(state.stickyOffset).to.be.null();
    expect(state.stickyPlayerisActive).to.be.false();
    expect(wrapper).to.have.length(1);
    expect(window.addEventListener).to.have.been.calledWith(
      'resize',
      instance.resizeListener
    );
    expect(window.addEventListener).to.have.been.calledWith(
      'scroll',
      instance.scrollListener
    );
    wrapper.unmount();
    expect(window.removeEventListener).to.have.been.calledWith(
      'resize',
      instance.resizeListener
    );
    expect(window.removeEventListener).to.have.been.calledWith(
      'scroll',
      instance.scrollListener
    );
  });

  it('should call the stickyToggleCallback on componentDidUpdate', () => {
    const wrapper = shallow(
      <PersistentMediaPlayer {...props}>
        <div className="child" />
      </PersistentMediaPlayer>
    );
    wrapper.setState({
      stickyPlayerisActive: true,
    });
    wrapper.update();
    expect(props.stickyToggleCallback).to.have.been.calledOnceWithExactly(true);
  });

  it('should call the stickyToggleCallback on componentDidUpdate', () => {
    const wrapper = mount(
      <PersistentMediaPlayer {...props}>
        <div className="child" />
      </PersistentMediaPlayer>
    );
    wrapper.setState({
      stickyPlayerisActive: true,
    });

    expect(props.stickyToggleCallback).to.have.been.calledOnceWithExactly(true);
  });

  it('should handle setStickyPlayer', () => {
    const wrapper = mount(
      <PersistentMediaPlayer {...props}>
        <div className="child" />
      </PersistentMediaPlayer>
    );
    wrapper.setState({ stickyOffset: -100 });
    const instance = wrapper.instance();
    sinon.stub(instance, 'getMediaPlayerOffset').returns(10);
    wrapper.instance().setStickyPlayer();
    wrapper.update();
    expect(wrapper.state('stickyOffset')).to.be.equal(10);
    expect(wrapper.state('stickyPlayerisActive')).to.be.true();
  });

  it('should set stickyPlayerisActive', () => {
    const wrapper = mount(
      <PersistentMediaPlayer {...props}>
        <div className="child" />
      </PersistentMediaPlayer>
    );
    wrapper.setState({
      stickyPlayerisActive: true,
    });
    wrapper.update();
    expect(wrapper.find('.quiver-persistent-media--sticky')).to.have.length(1);
  });

  it('should handle resize', () => {
    const wrapper = mount(
      <PersistentMediaPlayer {...props}>
        <div className="child" />
      </PersistentMediaPlayer>
    );
    wrapper.setState({
      isMobile: true,
    });
    window.innerWidth = 1000;
    wrapper.instance().handleResize();
    expect(wrapper.state('isMobile')).to.be.false();
  });
});
