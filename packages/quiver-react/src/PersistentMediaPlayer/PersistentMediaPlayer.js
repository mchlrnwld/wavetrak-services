import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import _throttle from 'lodash/throttle';

class PersistentMediaPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMobile: false,
      stickyOffset: null,
      stickyPlayerisActive: false,
    };
  }

  componentDidMount() {
    this.handleResize();
    window.addEventListener('resize', this.resizeListener);
    window.addEventListener('scroll', this.scrollListener);
  }

  componentDidUpdate(_, prevState) {
    const { stickyToggleCallback } = this.props;
    const { stickyPlayerisActive } = this.state;
    if (
      stickyToggleCallback &&
      stickyPlayerisActive !== prevState.stickyPlayerisActive
    ) {
      stickyToggleCallback(stickyPlayerisActive);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeListener);
    window.removeEventListener('scroll', this.scrollListener);
  }

  setStickyPlayer = () => {
    const { stickyOffset, isMobile } = this.state;
    const scrollPosition = window.scrollY;
    const mediaPlayerOffset = this.getMediaPlayerOffset();
    if (!stickyOffset || stickyOffset !== mediaPlayerOffset) {
      this.setState({ stickyOffset: mediaPlayerOffset });
    }
    if (stickyOffset && scrollPosition > stickyOffset && !isMobile) {
      this.setState({ stickyPlayerisActive: true });
    } else if ((stickyOffset && scrollPosition < stickyOffset) || isMobile) {
      this.setState({ stickyPlayerisActive: false });
    }
  };

  getMediaPlayerOffset = () => {
    const { containerRef, stickyScrollOffset } = this.props;

    if (!containerRef) return null;

    const docBody = document.body.getBoundingClientRect();
    const mediaPlayer = containerRef.getBoundingClientRect();
    return mediaPlayer.bottom - docBody.top - stickyScrollOffset;
  };

  getPersistentMediaPlayerClasses = (stickyPlayerisActive) =>
    classNames({
      'quiver-persistent-media': true,
      'quiver-persistent-media--sticky': stickyPlayerisActive,
    });

  handleResize = () => {
    const { isMobile } = this.state;
    if (window.innerWidth >= 976) this.setState({ isMobile: false });
    else if (window.innerWidth < 976 && !isMobile) {
      this.setState({
        isMobile: true,
        stickyPlayerisActive: false,
      });
    }
  };

  /* istanbul ignore-next */
  // These need to be placed here or else the `this.function` definitions will be undefined
  // eslint-disable-next-line react/sort-comp
  resizeListener = _throttle(this.handleResize, 250);

  /* istanbul ignore-next */
  scrollListener = _throttle(this.setStickyPlayer, 250);

  render() {
    const { children } = this.props;
    const { stickyPlayerisActive } = this.state;
    return (
      <div className="quiver-persistent-container">
        <div
          className={this.getPersistentMediaPlayerClasses(stickyPlayerisActive)}
        >
          {children}
        </div>
      </div>
    );
  }
}

PersistentMediaPlayer.defaultProps = {
  stickyScrollOffset: 0,
  stickyToggleCallback: null,
};

PersistentMediaPlayer.propTypes = {
  children: PropTypes.node.isRequired,
  containerRef: PropTypes.node.isRequired,
  stickyScrollOffset: PropTypes.number,
  stickyToggleCallback: PropTypes.func,
};

export default PersistentMediaPlayer;
