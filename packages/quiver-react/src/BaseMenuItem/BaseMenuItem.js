import React from 'react';
import PropTypes from 'prop-types';

const BaseMenuItem = ({
  href,
  role,
  tabIndex,
  onClick,
  newWindow,
  children,
}) => (
  // eslint-disable-next-line react/jsx-no-target-blank
  <a
    className="quiver-base-menu-item"
    href={href}
    role={role}
    tabIndex={tabIndex}
    onClick={onClick}
    target={newWindow ? '_blank' : ''}
    rel={newWindow ? 'noreferrer' : ''}
  >
    {children}
  </a>
);

BaseMenuItem.propTypes = {
  href: PropTypes.string,
  role: PropTypes.string,
  tabIndex: PropTypes.string,
  onClick: PropTypes.func,
  newWindow: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

BaseMenuItem.defaultProps = {
  href: null,
  role: null,
  tabIndex: null,
  onClick: null,
  newWindow: false,
};

export default BaseMenuItem;
