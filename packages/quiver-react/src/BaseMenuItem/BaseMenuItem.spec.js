import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import BaseMenuItem from './BaseMenuItem';

describe('BaseMenuItem', () => {
  it('should render an a tag with children', () => {
    const wrapper = shallow(<BaseMenuItem>For Testing</BaseMenuItem>);
    expect(wrapper.html()).to.contain('For Testing');
  });
  it('should not contain an href if none is passed', () => {
    const wrapper = shallow(<BaseMenuItem>For Testing</BaseMenuItem>);
    expect(wrapper.html()).to.not.contain('href=');
  });
  it('should contain an href if one is passed', () => {
    const wrapper = shallow(
      <BaseMenuItem href="www.surfline.com">For Testing</BaseMenuItem>
    );
    expect(wrapper.html()).to.contain('href="www.surfline.com"');
  });
  it('should contain a role or tabIndex is one is passed', () => {
    const wrapper = shallow(
      <BaseMenuItem role="button" tabIndex="0">
        For Testing
      </BaseMenuItem>
    );
    expect(wrapper.html()).to.contain('role="button" tabindex="0"');
  });
});
