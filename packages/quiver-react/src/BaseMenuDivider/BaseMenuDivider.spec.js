import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import BaseMenuDivider from './BaseMenuDivider';

describe('BaseMenuDivider', () => {
  it('should render an empty div', () => {
    const wrapper = shallow(<BaseMenuDivider />);
    expect(wrapper.html()).to.contain(
      '<div class="quiver-base-menu-divider"></div>'
    );
  });
});
