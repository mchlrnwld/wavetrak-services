import React from 'react';

const BaseMenuDivider = () => <div className="quiver-base-menu-divider" />;

export default BaseMenuDivider;
