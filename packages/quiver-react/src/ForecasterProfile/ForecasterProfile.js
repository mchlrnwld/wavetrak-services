import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { conditionClassModifier } from '@surfline/web-common';
import NextForecast from '../NextForecast';
import getForecasterLink from '../utils/getForecasterLink';

const forecasterLastUpdateClassNames = ({ reportExpired }) =>
  classNames({
    'quiver-forecaster-profile__update-container__last-update': true,
    'quiver-forecaster-profile__update-container__last-update--expired':
      reportExpired,
  });

const GRAVATAR_URL =
  'https://d14fqx6aetz9ka.cloudfront.net/wp-content/uploads/2017/05/09151618/SL_logo-2-150x150.jpeg';
const DEFAULT_URL =
  'https://www.gravatar.com/avatar/d41d8cd98f00b204e9800998ecf8427e?d=mm';

const forecasterImageClassNames = (value, expired) =>
  classNames({
    'quiver-forecaster-profile__details-image': true,
    [`quiver-forecaster-profile__details-image--${conditionClassModifier(
      value,
      true
    )}`]: true,
    'quiver-forecaster-profile__details-image--expired': expired,
  });

const ForecasterProfile = ({
  alt,
  forecasterName,
  forecasterTitle,
  lastUpdate,
  nextUpdate,
  url,
  styles,
  reportExpired,
  small,
  text,
  condition,
  expired,
  urlBase,
}) => {
  if (small) {
    return (
      <div className="quiver-forecaster-profile" style={styles}>
        <div className="quiver-forecaster-profile__image-container">
          <img
            className={forecasterImageClassNames(condition, expired)}
            src={url || DEFAULT_URL}
            alt={alt || 'Surfline Avatar'}
          />
        </div>
        <div className="quiver-forecaster-profile__details-container">
          <span className="quiver-forecaster-profile__update-text">{text}</span>
          <br />
          <span className="quiver-forecaster-profile__forecaster-name">
            {forecasterName}
          </span>
          <br />
          <span className="quiver-forecaster-profile__forecaster-title">
            {forecasterTitle}
          </span>
        </div>
      </div>
    );
  }

  const linkUrl = getForecasterLink(urlBase, forecasterName);
  return (
    <div className="quiver-forecaster-profile" style={styles}>
      <a
        href={linkUrl}
        className="quiver-forecaster-profile__details-container"
        target="_blank"
        rel="noopener noreferrer"
      >
        <img
          className={forecasterImageClassNames(condition, expired)}
          src={`${url || GRAVATAR_URL}&s=60`}
          alt={alt || 'Surfline Avatar'}
        />
        <div className="quiver-forecaster-profile__details-container__forecaster">
          <span className="quiver-forecaster-profile__details-container__forecaster-name">
            {forecasterName}
          </span>
          <span className="quiver-forecaster-profile__details-container__forecaster-title">
            {forecasterTitle}
          </span>
        </div>
      </a>
      <div className="quiver-forecaster-profile__update-container">
        <span className={forecasterLastUpdateClassNames({ reportExpired })}>
          <span className="quiver-forecaster-profile__update-container__last-update-description">
            Last updated
          </span>{' '}
          {lastUpdate}
        </span>
        {nextUpdate ? (
          <span className="quiver-forecaster-profile__update-container__next-update">
            <NextForecast nextUpdate={nextUpdate} />
          </span>
        ) : null}
      </div>
    </div>
  );
};

ForecasterProfile.propTypes = {
  alt: PropTypes.string,
  forecasterName: PropTypes.string,
  forecasterTitle: PropTypes.string,
  styles: PropTypes.string,
  lastUpdate: PropTypes.string,
  nextUpdate: PropTypes.string,
  url: PropTypes.string,
  reportExpired: PropTypes.bool,
  small: PropTypes.bool,
  text: PropTypes.string,
  condition: PropTypes.string,
  expired: PropTypes.bool,
  urlBase: PropTypes.string,
};

ForecasterProfile.defaultProps = {
  alt: null,
  forecasterName: '',
  forecasterTitle: '',
  styles: null,
  lastUpdate: null,
  nextUpdate: null,
  url: null,
  reportExpired: false,
  small: false,
  text: '',
  condition: '',
  expired: false,
  urlBase: 'https://www.surfline.com/',
};

export default ForecasterProfile;
