import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import ForecasterProfile from './ForecasterProfile';

describe('ForecasterProfile', () => {
  it('should render an img element', () => {
    const wrapper = mount(
      <ForecasterProfile
        forecasterName="Jeremy"
        forecasterTitle="Monson"
        lastUpdate="Today at 3pm PST. Note: You will have to override the styles on this yourself."
        nextUpdate="Tomorrow at 6am PST"
        url="https://www.gravatar.com/avatar/4eb0adfe0a8e5b2bae1a573ef16ec27f?d=mm"
      />
    );
    expect(wrapper.html()).to.contain('<img');
    expect(
      wrapper.find(
        '.quiver-forecaster-profile__update-container__last-update-description'
      )
    );
  });

  it('should render the small version', () => {
    const wrapper = mount(
      <ForecasterProfile
        url="https://www.gravatar.com/avatar/4eb0adfe0a8e5b2bae1a573ef16ec27f?d=mm"
        alt="Chris profile"
        forecasterTitle="Forecast/Weather Editorial Program Lead"
        styles={{ width: 400 }}
        small
        text="test text"
      />
    );

    expect(
      wrapper.find('.quiver-forecaster-profile__update-text')
    ).to.have.length(1);
  });

  it('should render a default color on hover', () => {
    const wrapper = mount(
      <ForecasterProfile
        text="Updated 1 hour ago"
        forecasterName="Chris Borg"
        forecasterTitle="Forecast/Weather Editorial Program Lead"
        styles={{ width: 400 }}
      />
    );

    expect(
      wrapper.find('.quiver-forecaster-profile__details-image--lola')
    ).to.have.length(1);
  });

  it('should render the correct class for a forecast rating', () => {
    const wrapper = mount(
      <ForecasterProfile
        url="https://www.gravatar.com/avatar/4eb0adfe0a8e5b2bae1a573ef16ec27f?d=mm"
        alt="Chris profile"
        text="Updated 1 hour ago"
        forecasterName="Chris Borg"
        forecasterTitle="Forecast/Weather Editorial Program Lead"
        styles={{ width: 400 }}
        condition="POOR_TO_FAIR"
        expired={false}
        reportExpired={false}
      />
    );

    expect(
      wrapper.find('.quiver-forecaster-profile__details-image--poor-to-fair')
    ).to.have.length(1);
    expect(
      wrapper.find('.quiver-forecaster-profile__details-image--expired')
    ).to.have.length(0);
    expect(
      wrapper
        .find('.quiver-forecaster-profile__details-image--poor-to-fair')
        .prop('alt')
    ).to.equal('Chris profile');
    expect(
      wrapper
        .find('.quiver-forecaster-profile__details-image--poor-to-fair')
        .prop('src')
    ).to.equal(
      'https://www.gravatar.com/avatar/4eb0adfe0a8e5b2bae1a573ef16ec27f?d=mm&s=60'
    );
    expect(
      wrapper.find('.quiver-forecaster-profile__update-container__last-update')
    ).to.have.length(1);
    expect(
      wrapper.find(
        '.quiver-forecaster-profile__update-container__last-update--expired'
      )
    ).to.have.length(0);
  });

  it('should render the correct class for a forecast rating', () => {
    const wrapper = mount(
      <ForecasterProfile
        text="Updated 1 hour ago"
        forecasterName="Chris Borg"
        forecasterTitle="Forecast/Weather Editorial Program Lead"
        styles={{ width: 400 }}
        condition="POOR_TO_FAIR"
        expired
        reportExpired
        nextUpdate="12-12-20"
      />
    );

    expect(
      wrapper.find('.quiver-forecaster-profile__update-container__next-update')
    ).to.have.length(1);
    expect(
      wrapper
        .find('.quiver-forecaster-profile__details-image--poor-to-fair')
        .prop('alt')
    ).to.equal('Surfline Avatar');
    expect(
      wrapper
        .find('.quiver-forecaster-profile__details-image--poor-to-fair')
        .prop('src')
    ).to.equal(
      'https://d14fqx6aetz9ka.cloudfront.net/wp-content/uploads/2017/05/09151618/SL_logo-2-150x150.jpeg&s=60'
    );
    expect(
      wrapper.find('.quiver-forecaster-profile__details-image--expired')
    ).to.have.length(1);
  });
});
