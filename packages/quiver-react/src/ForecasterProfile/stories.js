import React from 'react';
import ForecasterProfile from './index';

export default {
  title: 'ForecasterProfile',
};

export const Default = () => (
  <ForecasterProfile
    forecasterName="Jeremy"
    forecasterTitle="Monson"
    lastUpdate="Today at 3pm PST. Note: You will have to override the styles on this yourself."
    nextUpdate="Tomorrow at 6am PST"
  />
);

Default.story = {
  name: 'default',
};

export const Small = () => (
  <ForecasterProfile
    url="https://www.gravatar.com/avatar/4eb0adfe0a8e5b2bae1a573ef16ec27f?d=mm"
    alt="Chris profile"
    text="Updated 1 hour ago"
    forecasterName="Chris Borg"
    forecasterTitle="Forecast/Weather Editorial Program Lead"
    styles={{ width: 400 }}
    small
  />
);
