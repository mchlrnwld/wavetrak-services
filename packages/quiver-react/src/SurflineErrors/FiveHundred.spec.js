import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import common from '@surfline/web-common';
import sinon from 'sinon';
import FiveHundred from './FiveHundred';

describe('FiveHundred', () => {
  const { location } = window;
  before(() => {
    delete window.location;
    window.location = {
      replace: sinon.spy(),
    };
  });

  after(() => {
    window.location = location;
  });

  beforeEach(() => {
    sinon.stub(common, 'trackNavigatedToPage');
  });

  afterEach(() => {
    common.trackNavigatedToPage.restore();
    window.location.replace.resetHistory();
  });

  it('should render a 500 container with 500 class and send a page event', () => {
    const defaultWrapper = mount(<FiveHundred />);
    expect(defaultWrapper.find('.body__500').exists()).to.be.true();
    expect(common.trackNavigatedToPage).to.have.been.calledWithExactly(
      'Error',
      null,
      '500'
    );
  });

  it('should track the button click', () => {
    const defaultWrapper = shallow(<FiveHundred />);
    const button = defaultWrapper.find('button');
    button.simulate('click');
    expect(window.location.replace).to.have.been.calledOnce();
    expect(window.location.replace).to.have.been.calledWith('/');
  });
});
