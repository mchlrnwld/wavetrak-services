import React, { Component } from 'react';
import productCDN from '@surfline/quiver-assets';
import { trackNavigatedToPage } from '@surfline/web-common';
import en from './translations/base';

export default class NotFound extends Component {
  constructor(props) {
    super(props);
    this.onClickHandler = this.onClickHandler.bind(this);
  }

  componentDidMount() {
    trackNavigatedToPage('Error', null, '404');
  }

  onClickHandler = () => {
    window.location.replace('/');
  };

  render() {
    const image = `${productCDN}/backgrounds/404-mobile-image.jpg`;
    return (
      <div className="body__404">
        <div className="container">
          <h2 className="container__title">{en.not_found.title}</h2>
          <p className="container__paragraph">{en.not_found.paragraph_top}</p>
          <p className="container__paragraph__desktop">
            {en.not_found.paragraph_bottom_before}
            <span>
              <a href="/" className="container__paragraph__link">
                {en.not_found.paragraph_bottom_link}
              </a>
            </span>
            {en.not_found.paragraph_bottom_after}
          </p>
          <button
            type="button"
            onClick={this.onClickHandler}
            className="quiver-button"
          >
            {en.not_found.button_text}
          </button>
          <div className="container_image_container">
            <img className="container_image" src={image} alt="404 Error" />
          </div>
        </div>
      </div>
    );
  }
}
