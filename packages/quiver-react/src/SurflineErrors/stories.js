import React from 'react';
import NotFound from './NotFound';
import FiveHundred from './FiveHundred';

export default {
  title: 'SurflineErrors',
};

export const NotFoundStory = () => <NotFound />;

NotFoundStory.story = {
  name: 'NotFound',
};

export const FiveHundredStory = () => <FiveHundred />;

FiveHundredStory.story = {
  name: 'FiveHundred',
};
