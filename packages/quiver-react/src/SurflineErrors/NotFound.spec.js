import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import common from '@surfline/web-common';
import sinon from 'sinon';
import NotFound from './NotFound';

describe('NotFound', () => {
  const { location } = window;
  before(() => {
    delete window.location;
    window.location = {
      replace: sinon.spy(),
    };
  });

  after(() => {
    window.location = location;
  });

  beforeEach(() => {
    sinon.stub(common, 'trackNavigatedToPage');
  });

  afterEach(() => {
    common.trackNavigatedToPage.restore();
    window.location.replace.resetHistory();
  });

  it('should render a 404 container with 404 background', () => {
    const defaultWrapper = mount(<NotFound />);
    expect(defaultWrapper.find('.body__404').exists()).to.be.true();
    expect(common.trackNavigatedToPage).to.have.been.calledOnceWithExactly(
      'Error',
      null,
      '404'
    );
  });

  it('should send correct click event', () => {
    const defaultWrapper = shallow(<NotFound />);
    const button = defaultWrapper.find('button');
    button.simulate('click');
    expect(window.location.replace).to.have.been.calledOnce();
    expect(window.location.replace).to.have.been.calledWith('/');
  });
});
