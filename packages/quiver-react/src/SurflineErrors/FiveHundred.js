import React, { Component } from 'react';
import productCDN from '@surfline/quiver-assets';
import { trackNavigatedToPage } from '@surfline/web-common';
import en from './translations/base';

export default class FiveHundred extends Component {
  constructor(props) {
    super(props);
    this.onClickHandler = this.onClickHandler.bind(this);
  }

  componentDidMount() {
    trackNavigatedToPage('Error', null, '500');
  }

  onClickHandler = () => {
    window.location.replace('/');
  };

  render() {
    const image = `${productCDN}/backgrounds/500-mobile-image.jpg`;
    return (
      <div className="body__500">
        <div className="container">
          <p className="container__paragraph">
            {en.five_hundred.paragraph_top}
          </p>
          <p className="container__paragraph__desktop">
            {en.five_hundred.paragraph_bottom_before}
            <span>
              <a href="/" className="container__paragraph__link">
                {en.five_hundred.paragraph_bottom_link}
              </a>
            </span>
            {en.five_hundred.paragraph_bottom_after}
          </p>
          <button
            type="button"
            onClick={this.onClickHandler}
            className="quiver-button"
          >
            {en.five_hundred.button_text}
          </button>
          <div className="container_image_container">
            <img className="container_image" src={image} alt="500 Error" />
          </div>
        </div>
      </div>
    );
  }
}
