/* eslint-disable max-len */
export default {
  not_found: {
    title: 'Page Not Found',
    paragraph_top:
      "The link you've followed may be broken, or the page may have been removed.",
    paragraph_bottom_before: 'Go ahead... go shred back to the ',
    paragraph_bottom_link: 'homepage',
    paragraph_bottom_after: ' for surf reports, forecasts and surf news.',
    button_text: 'Back to Surfline.com',
  },
  five_hundred: {
    title: 'Internal Server Error',
    paragraph_top:
      "Looks like we're having a few issues here, thanks for noticing. Rest assured we've been notified of this problem.",
    paragraph_bottom_before: 'Checkout the ',
    paragraph_bottom_link: 'homepage',
    paragraph_bottom_after: ' for surf reports, forecasts and surf news.',
    button_text: 'Back to Surfline.com',
  },
};
