import React from 'react';
import PropTypes from 'prop-types';

const NavigationItem = ({
  linkDisplay,
  href,
  role,
  tabIndex,
  children,
  onClick,
}) => (
  <div className="quiver-navigation-item">
    <a
      className="quiver-navigation-item__container__link"
      href={href}
      role={role}
      tabIndex={tabIndex}
      onClick={href ? null : onClick}
    >
      {linkDisplay}
    </a>
    {children}
  </div>
);

NavigationItem.propTypes = {
  linkDisplay: PropTypes.oneOfType([PropTypes.string, PropTypes.node])
    .isRequired,
  href: PropTypes.string,
  role: PropTypes.string,
  tabIndex: PropTypes.string,
  children: PropTypes.node,
  onClick: PropTypes.func,
};

NavigationItem.defaultProps = {
  href: null,
  role: null,
  tabIndex: null,
  children: null,
  onClick: null,
};

export default NavigationItem;
