import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import NavigationItem from './NavigationItem';

describe('NavigationItem', () => {
  const props = {
    linkDisplay: 'Link text',
  };

  const wrapper = mount(<NavigationItem {...props} />);

  it('should render display text', () => {
    expect(wrapper.text()).to.contain(props.linkDisplay);
  });
});
