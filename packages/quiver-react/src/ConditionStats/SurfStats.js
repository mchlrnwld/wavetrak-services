import React from 'react';
import SurfHeight from '../SurfHeight';
import WaveSummary from '../WaveSummary';
import { waveHeightPropTypes } from '../PropTypes/readings';
import unitsPropTypes from '../PropTypes/units';

const SurfStats = ({ units, waveHeight }) => {
  const { occasional, min, max, plus, humanRelation } = waveHeight;
  const humanRelationStr =
    humanRelation && humanRelation !== 'None' ? humanRelation : '';
  const title = 'Surf height';
  return (
    <>
      <div className="quiver-conditions-stats__title">{title}</div>
      <SurfHeight min={min} max={max} plus={plus} units={units.waveHeight} />
      <WaveSummary
        humanRelation={humanRelationStr}
        occasional={occasional}
        units={units.waveHeight}
      />
    </>
  );
};

SurfStats.propTypes = {
  waveHeight: waveHeightPropTypes.isRequired,
  units: unitsPropTypes.isRequired,
};

export default SurfStats;
