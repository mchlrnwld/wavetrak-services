import React from 'react';
import PropTypes from 'prop-types';
import SwellMeasurements from '../SwellMeasurements';
import unitsPropTypes from '../PropTypes/units';
import { swellsPropTypes } from '../PropTypes/readings';

const EmptySwell = () => (
  <div className="quiver-condition-stats__empty-swells">
    {[1, 2, 3].map((index) => (
      <SwellMeasurements key={index} index={index} isEmptySwell />
    ))}
  </div>
);

const SwellStats = ({ units, swells, title, isEmptySwell }) => {
  const validSwells = swells.filter(
    (swell) => swell.height > 0 && swell.period > 0
  );
  return (
    <>
      <div className="quiver-conditions-stats__title">{title}</div>
      {isEmptySwell ? (
        <EmptySwell />
      ) : (
        <div className="quiver-condition-stats__swells">
          {validSwells.map(({ height, period, direction, index }) => (
            <SwellMeasurements
              key={index}
              height={height}
              period={period}
              direction={direction}
              index={index}
              units={units.swellHeight}
            />
          ))}
        </div>
      )}
    </>
  );
};

SwellStats.propTypes = {
  swells: swellsPropTypes.isRequired,
  units: unitsPropTypes,
  title: PropTypes.string,
  isEmptySwell: PropTypes.bool,
};

SwellStats.defaultProps = {
  units: '',
  title: 'Swells',
  isEmptySwell: false,
};

export default SwellStats;
