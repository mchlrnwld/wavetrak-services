import React from 'react';
import SurfStats from './SurfStats';
import SwellStats from './SwellStats';
import TideStats from './TideStats';
import WindStats from './WindStats';

const stats = {
  units: {
    waveHeight: 'FT',
    tideHeight: 'FT',
    windSpeed: 'MPH',
    speed: 'MPH',
    distance: 'FT',
  },
  windSpeed: {
    speed: 10,
    direction: 320,
  },
  tide: {
    prev: {
      type: 'LOW',
      height: 1.1,
      timestamp: +new Date(),
      utcOffset: -8,
    },
    current: {
      type: 'NORMAL',
      height: 1.1,
      timestamp: +new Date(),
      utcOffset: -8,
    },
    next: {
      type: 'NORMAL',
      height: 1.1,
      timestamp: +new Date(),
      utcOffset: -8,
    },
  },
  waveHeight: {
    human: false,
    humanRelation: 'chest to head high',
    max: 4,
    min: 3,
    occasional: 5,
    plus: false,
  },
  swells: [{ height: 2, period: 16, direction: 120, index: 0 }],
};

export default {
  title: 'ConditionStats',
};

export const SurfStatsStory = () => (
  <div className="quiver-spot-forecast-summary__stat">
    <SurfStats waveHeight={stats.waveHeight} units={stats.units} />
  </div>
);

SurfStatsStory.story = {
  name: 'SurfStats',
};

export const SwellStatsStory = () => (
  <div className="quiver-spot-forecast-summary__stat">
    <SwellStats swells={stats.swells} units={stats.units} />
  </div>
);

SwellStatsStory.story = {
  name: 'SwellStats',
};

export const TideStatsStory = () => (
  <div className="quiver-spot-forecast-summary__stat">
    <TideStats tide={stats.tide} units={stats.units} />
  </div>
);

TideStatsStory.story = {
  name: 'TideStats',
};

export const WindStatsStory = () => (
  <div style={{ width: 300, height: 300 }}>
    <WindStats wind={stats.windSpeed} units={stats.units} />
  </div>
);

WindStatsStory.story = {
  name: 'WindStats',
};
