import React from 'react';
import { windPropTypes } from '../PropTypes/readings';
import unitPropTypes from '../PropTypes/units';

import Reading from '../Reading';
import WindArrow from '../icons/WindArrow';
import WindSummary from '../WindSummary';

const WindStats = ({ wind, units }) => (
  <>
    <div className="quiver-conditions-stats__title">Wind</div>
    <div className="quiver-conditions-stats__stat-reading">
      <Reading value={wind.speed} units={units.windSpeed} />
      <WindArrow
        degrees={wind.direction}
        speed={wind.speed}
        units={units.windSpeed}
      />
    </div>
    <WindSummary direction={wind.direction} />
  </>
);

WindStats.propTypes = {
  wind: windPropTypes.isRequired,
  units: unitPropTypes.isRequired,
};

export default WindStats;
