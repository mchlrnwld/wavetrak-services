import React from 'react';
import Reading from '../Reading';
import TideArrow from '../TideArrow';
import TideSummary from '../TideSummary';
import unitsPropTypes from '../PropTypes/units';
import { tidePropTypes } from '../PropTypes/readings';

const TideStats = ({ tide, units }) => (
  <>
    <div className="quiver-conditions-stats__title">Tide</div>
    <div className="quiver-conditions-stats__stat-reading">
      <Reading value={tide.current.height} units={units.tideHeight} />
      <TideArrow direction={tide.next.type === 'LOW' ? 'DOWN' : 'UP'} />
    </div>
    <TideSummary tide={tide.next} units={units.tideHeight} />
  </>
);

TideStats.propTypes = {
  units: unitsPropTypes.isRequired,
  tide: tidePropTypes.isRequired,
};

export default TideStats;
