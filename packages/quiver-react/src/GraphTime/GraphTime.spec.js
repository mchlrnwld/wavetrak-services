import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import GraphTime from './GraphTime';

describe('components / GraphTime', () => {
  const baseTimestamp = 1609488000;

  it('should render all the timestamps provided', () => {
    const timestamps = [
      {
        timestamp: baseTimestamp,
        utcOffset: -8,
      },
      {
        timestamp: baseTimestamp,
        utcOffset: -7,
      },
    ];
    const wrapper = shallow(<GraphTime timestamps={timestamps} />);
    const graphTime = wrapper.find('.quiver-graph-time');
    expect(graphTime).to.have.length(1);

    const graphTimestamp = wrapper.find('GraphTimestamp');
    expect(graphTimestamp).to.have.length(2);

    expect(graphTimestamp.at(0).prop('timestamp')).to.be.equal(baseTimestamp);
    expect(graphTimestamp.at(0).prop('utcOffset')).to.be.equal(-8);

    expect(graphTimestamp.at(1).prop('timestamp')).to.be.equal(baseTimestamp);
    expect(graphTimestamp.at(1).prop('utcOffset')).to.be.equal(-7);
  });
});
