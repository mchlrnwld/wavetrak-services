import React from 'react';
import GraphTime from './GraphTime';

export default {
  title: 'GraphTime',
};

const baseTimestamp = 1609488000;

const timestamps = [
  {
    timestamp: baseTimestamp,
    utcOffset: -8,
  },
  {
    timestamp: baseTimestamp,
    utcOffset: -7,
  },
];

export const Default = () => <GraphTime timestamps={timestamps} />;

export const WithLabels = () => (
  <GraphTime timestamps={timestamps} showTimeLabels />
);

Default.story = {
  name: 'default',
};

WithLabels.story = {
  name: 'With time labels',
};
