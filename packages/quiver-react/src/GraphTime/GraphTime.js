import PropTypes from 'prop-types';
import React from 'react';
import GraphTimestamp from '../GraphTimestamp';

/**
 *
 * @param {object} Props
 * @param {number} Props.hours
 * @param {number} Props.interval
 * @param {{timestamp: number, utcOffset: number}[]} Props.timestamps
 * @param {boolean} Props.showTimeLabels
 */
const GraphTime = ({ timestamps, showTimeLabels }) => (
  <div className="quiver-graph-time">
    {timestamps.map(({ timestamp, utcOffset }, columnNumber) => (
      <GraphTimestamp
        key={timestamp}
        showTimeLabels={showTimeLabels}
        timestamp={timestamp}
        utcOffset={utcOffset}
        columnNumber={columnNumber}
      />
    ))}
  </div>
);

GraphTime.propTypes = {
  timestamps: PropTypes.arrayOf(
    PropTypes.shape({
      timestamp: PropTypes.number,
      utcOffset: PropTypes.number,
    })
  ),
  showTimeLabels: PropTypes.bool,
};

GraphTime.defaultProps = {
  timestamps: null,
  showTimeLabels: false,
};

export default GraphTime;
