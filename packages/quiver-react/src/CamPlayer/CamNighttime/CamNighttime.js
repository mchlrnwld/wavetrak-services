import React from 'react';
import PropTypes from 'prop-types';

import CamOverlay from '../../CamOverlay';
import AspectRatioContent from '../../AspectRatioContent';

const CamNighttime = ({ message, rewindUrl, stillUrl }) => (
  <div className="quiver-cam-nighttime">
    <AspectRatioContent>
      <CamOverlay backgroundImage={`url(${stillUrl})`} />
      <div className="quiver-cam-nighttime__message">
        <div className="quiver-cam-nighttime__message__header">{message}</div>
        <div className="quiver-cam-nighttime__message__subtext">
          Enjoy a <a href={rewindUrl}>Cam Rewind</a>
        </div>
      </div>
    </AspectRatioContent>
  </div>
);

CamNighttime.propTypes = {
  stillUrl: PropTypes.string,
  message: PropTypes.string,
  rewindUrl: PropTypes.string,
};

CamNighttime.defaultProps = {
  stillUrl: '',
  message: 'Live camera stream unavailable during nighttime hours',
  rewindUrl: null,
};

export default CamNighttime;
