import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import CamRewindMessage from './CamRewindMessage';

describe('CamRewindMessage', () => {
  const wrapper = shallow(<CamRewindMessage href="https://www.surfline.com" />);

  it('should render an anchor tag', () => {
    expect(wrapper.html()).to.contain('<a');
  });

  it('should link to the rewindUrl', () => {
    expect(wrapper.html()).to.contain('href="https://www.surfline.com"');
  });
});
