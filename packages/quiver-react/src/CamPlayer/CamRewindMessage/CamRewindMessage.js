import React from 'react';
import PropTypes from 'prop-types';

const CamRewindMessage = ({ href }) => (
  <div className="quiver-cam-rewind-message">
    It&#39;s nighttime. Enjoy a <a href={href}>cam rewind</a> clip.
  </div>
);

CamRewindMessage.propTypes = {
  href: PropTypes.string.isRequired,
};

export default CamRewindMessage;
