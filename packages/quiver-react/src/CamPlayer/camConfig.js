export const setupConfig = (
  autoStart = false,
  playbackRateControls = false
) => ({
  primary: 'html5',
  width: '100%',
  hlshtml: true,
  androidhls: true,
  skin: 'five',
  autostart: autoStart,
  mute: autoStart, // Safari prevents un-muted cams from auto-play
  advertising: {
    client: 'googima',
  },
  liveTimeout: 0,
  playbackRateControls,
});

export const adTagConfig = {
  sz: '1280x720|640x480',
  iu: '/1024858/Video_Companion',
  ciu_szs: '300x250,320x50',
  impl: 's',
  gdfp_req: '1',
  env: 'vp',
  output: 'vast',
  unviewed_position_start: '1',
  correlator: Math.floor(new Date() / 1000),
};
