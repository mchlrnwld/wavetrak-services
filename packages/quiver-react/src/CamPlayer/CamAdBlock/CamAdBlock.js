import React from 'react';
import PropTypes from 'prop-types';

import productCDN from '@surfline/quiver-assets';
import CamOverlay from '../../CamOverlay';
import AspectRatioContent from '../../AspectRatioContent';

const CamAdblockUpsell = ({ href, onClick, text }) => (
  <div className="quiver-cam-adblock">
    <AspectRatioContent>
      <CamOverlay
        backgroundImage={`url(${productCDN}/backgrounds/cam-down-bg-image.png)`}
      />
      <div className="quiver-cam-adblock-upsell__message">
        <div className="quiver-cam-adblock-upsell__message__header">
          {text.disableText}
        </div>
        <div className="quiver-cam-adblock-upsell__message__subtext">
          <div className="quiver-cam-adblock-upsell__message__or">
            <span>{text.orText}</span>
          </div>
          <div>{text.upsellText}</div>
        </div>
        <a href={href} onClick={onClick}>
          <button type="button">{text.linkText}</button>
        </a>
      </div>
    </AspectRatioContent>
  </div>
);

CamAdblockUpsell.propTypes = {
  href: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.shape({
    disableText: PropTypes.string,
    orText: PropTypes.string,
    upsellText: PropTypes.string,
    linkText: PropTypes.string,
  }),
};

CamAdblockUpsell.defaultProps = {
  href: null,
  onClick: null,
  text: {
    disableText:
      'Please disable your ad blocker in order to view this Surfline cam.',
    orText: 'or',
    upsellText: 'For a completely ad-free cam experience',
    linkText: 'Upgrade to Premium',
  },
};

export default CamAdblockUpsell;
