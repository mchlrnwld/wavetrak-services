import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import CamTimeout from './CamTimeout';
import CamOverlay from '../../CamOverlay';

describe('CamTimeout', () => {
  const props = {
    stillUrl: 'http://www.somestillUrl.com',
    onClick: sinon.stub(),
  };

  it('should render a div with class name', () => {
    const wrapper = mount(<CamTimeout {...props} />);
    expect(wrapper.html()).to.contain('<div class="quiver-cam-timeout"');
  });

  it('should render default text if none is passed', () => {
    const wrapper = mount(<CamTimeout {...props} />);

    expect(wrapper.html()).to.contain('Are you still watching?');
    expect(wrapper.html()).to.contain('continue watching');
  });

  it('should render text passed in as props', () => {
    const wrapperWithText = mount(
      <CamTimeout
        {...props}
        text={{ message: 'This is a test', linkText: 'continue testing' }}
      />
    );
    expect(wrapperWithText.html()).to.contain('This is a test');
    expect(wrapperWithText.html()).to.contain('continue testing');
  });

  it('should render a CamOverlay component', () => {
    const wrapper = mount(<CamTimeout {...props} />);

    const camOverlay = wrapper.find(CamOverlay);
    expect(wrapper.find('.quiver-cam-overlay')).to.have.length(1);
    expect(camOverlay.prop('backgroundImage')).to.equal(
      `url(${props.stillUrl})`
    );
  });

  it('should execute the onClick function on button click', () => {
    const wrapper = mount(<CamTimeout {...props} />);

    expect(props.onClick).not.to.have.been.called();
    wrapper.find('button').simulate('click');
    expect(props.onClick).to.have.been.calledOnce();
  });
});
