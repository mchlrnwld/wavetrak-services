import React from 'react';
import PropTypes from 'prop-types';

import CamOverlay from '../../CamOverlay';

const CamTimeout = ({ stillUrl, onClick, text, isMultiCam }) => (
  <div className="quiver-cam-timeout">
    {!isMultiCam && <CamOverlay backgroundImage={`url(${stillUrl})`} />}
    <div className="quiver-cam-timeout__message">
      <div>{text.message}</div>
      <button type="button" onClick={onClick}>
        {text.linkText}
      </button>
    </div>
  </div>
);

CamTimeout.propTypes = {
  isMultiCam: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  stillUrl: PropTypes.string,
  text: PropTypes.shape({
    message: PropTypes.string,
    linkText: PropTypes.string,
  }),
};

CamTimeout.defaultProps = {
  isMultiCam: false,
  stillUrl: '',
  text: {
    message: 'Are you still watching?',
    linkText: 'continue watching',
  },
};

export default CamTimeout;
