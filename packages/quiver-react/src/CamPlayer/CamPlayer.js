import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { format } from 'date-fns';
import classNames from 'classnames';
import { Cookies } from 'react-cookie';
import productCDN from '@surfline/quiver-assets';
import {
  trackEvent,
  TABLET_LARGE_WIDTH,
  slugify,
  canUseDOM,
  nrNoticeError,
} from '@surfline/web-common';
import CamTimeout from './CamTimeout';
import CamUpsell from './CamUpsell';
import CamNighttime from './CamNighttime';
import CamAdblockUpsell from './CamAdBlock';
import CamRewindMessage from './CamRewindMessage';
import CamError from './CamError';
import AspectRatioContent from '../AspectRatioContent';
import { setupConfig, adTagConfig } from './camConfig';
import CountDownTimer from '../CountDownTimer';
import { cameraPropType } from '../PropTypes/cameras';
import { PLAYED_WEBCAM, STOPPED_WEBCAM } from '../utils/constants';

const ALT = 'ALT';
const SUB = 'SUB';
const MSG = 'MSG';
const AUTOPLAY_DISABLED = 'disabled';
const AUTOPLAY_ENABLED = 'enabled';

/**
 * @typedef {object} Props
 * @property {import('../PropTypes/cameras').Camera} camera
 * @property {string} location
 * @property {boolean} isFavorite
 */

/**
 * @deprecated Use the CamPlayerV2 component
 * @extends {React.Component<Props>}
 */
class CamPlayer extends Component {
  constructor(props) {
    super(props);
    this.camPlayer = null;
    this.adInterval = 30;
    this.upsellDuration = 15; // Seconds
    this.premiumTimeout = 600; // 10 minute premium timeout
    this.camWidth = null;
    this.state = {
      showTimeout: false,
      showUpSell: false,
      isStreaming: false,
      adFreePopup: false,
      adCount: 1,
      adRunning: false,
      controlsVisible: true,
      notFoundCount: 0,
      networkError: {
        isDown: false,
        message: null,
        altMessage: null,
      },
      midRoll: false,
    };
  }

  componentDidMount() {
    const { adblock, onDetectedAdBlock } = this.props;
    if (adblock && onDetectedAdBlock) {
      onDetectedAdBlock();
    }
    this.setupPlayer();
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      camera,
      adblock,
      onDetectedAdBlock,
      onToggleStreaming,
      controlledPlayback,
      isPlaying,
    } = this.props;
    const { isStreaming, midRoll } = this.state;
    if (camera && camera.streamUrl !== prevProps.camera.streamUrl) {
      if (midRoll) this.resetMidRollState();
      this.resetPlayerState();
      this.setupPlayer();
    }
    if (!prevProps.adblock && adblock && onDetectedAdBlock) {
      onDetectedAdBlock();
    }
    if (onToggleStreaming && isStreaming !== prevState.isStreaming) {
      onToggleStreaming(isStreaming);
    }
    if (controlledPlayback) {
      if (isPlaying !== prevProps.isPlaying) {
        if (isPlaying) {
          this.resumePlayer();
        } else {
          this.pausePlayer();
        }
      }
    }
  }

  componentWillUnmount() {
    if (this.camPlayer && this.camPlayer.remove) this.camPlayer.remove();
    const { isStreaming } = this.state;
    if (isStreaming) {
      this.trackPlaybackEvent(STOPPED_WEBCAM);
    }
  }

  resetPlayerState = () =>
    this.setState({
      showTimeout: false,
      showUpSell: false,
      isStreaming: false,
      adCountdownTime: 0,
    });

  getStringFromParams = (params) =>
    Object.keys(params)
      .map((key) => `${key}=${params[key]}`)
      .join('&');

  /* istanbul ignore next */
  getAdTag = (width, height) => {
    const { adCount, midRoll } = this.state;
    const {
      adTag,
      advertisingIds,
      viewType,
      qaFlag,
      isPremium,
      camera,
      adCamId,
    } = this.props;
    if (adCount > 5) return null; // Stop trying to render an ad after 5th failure
    if (adCount > 4)
      return `${productCDN}/adtags/PreRoll_SLAdFreePrem_rincon_MVP_feb18.xml`;

    const adTagOptions = {
      ...adTagConfig,
      ...adTag,
      url: encodeURIComponent(
        `${window.location.protocol}//${window.location.hostname}`
      ),
      description_url: encodeURIComponent(window.location.href),
    };

    const verifiedCamId = adCamId === camera._id ? adCamId : null;

    const customOptions = {
      camId: verifiedCamId,
      spotid: !verifiedCamId ? advertisingIds.spotId : null,
      subregionId: advertisingIds.subregionId,
      viewtype: viewType || 'SPOT',
      qaFlag,
      embedUrl: window.location.href,
      usertype: isPremium ? 'premium' : 'free',
      count: adCount,
      width,
      height,
      ivp_autorefresh: midRoll ? 0 : 1,
    };
    const customParams = encodeURIComponent(
      this.getStringFromParams(customOptions)
    );
    return `https://pubads.g.doubleclick.net/gampad/ads?${this.getStringFromParams(
      adTagOptions
    )}&cust_params=${customParams}`;
  };

  resetMidRollState = () => this.setState({ midRoll: false });

  getRewindUrl() {
    const { spotName, camera } = this.props;
    // If no spotName has been passed then try the camera title just so there's something in there
    // defaults to the first spot associated with the cam id anyway so not really required, defaults to
    // rewind incase there's no title either.
    return `/surf-cams/${slugify(spotName || camera.title || 'rewind')}/${
      camera._id
    }`;
  }

  setupPlayer() {
    if (window && window.jwplayer) {
      const {
        nighttime,
        rewindClip,
        playerId,
        camera,
        isPremium,
        isPersistentCam,
        aspectRatio,
        companionHandler,
        spotId,
        spotName,
        onTogglePreRoll,
        isMultiCam,
        isPrimaryCam,
        timeoutMultiCams,
        setCamReady,
        handleCamReady,
        camReady,
        controlledAds,
        playbackRateControls,
      } = this.props;
      const { adCount, notFoundCount } = this.state;
      window.jwplayer.key = window.jwplayer.defaults.key;
      this.camPlayer = window.jwplayer(playerId);
      this.rewindClipError = false;
      const { isPrerecorded } = camera;

      const rewindUrl = this.getRewindUrl();

      if (!this.camPlayer.setup) return;

      let file = nighttime && rewindClip ? rewindClip : camera.streamUrl;

      const cookie = new Cookies();
      const accessToken = cookie.get('access_token');

      /* istanbul ignore next */
      if (accessToken) {
        file += `?accessToken=${accessToken}`;
      }

      const withCredentials = file.indexOf('authorization') > -1;
      /* istanbul ignore next */
      this.camPlayer.setup({
        file,
        type: file.indexOf('mp4') > -1 ? 'mp4' : 'hls',
        withCredentials,
        image: camera.stillUrl,
        aspectratio: aspectRatio,
        ...setupConfig(
          isPremium || isPersistentCam || (isPremium && isMultiCam),
          playbackRateControls
        ),
      });

      if (!isPrerecorded && rewindUrl) {
        /* istanbul ignore next */
        this.camPlayer.addButton(
          `<svg
            class="rewind-button"
            xmlns="http://www.w3.org/2000/svg"
            width="18"
            height="18"
            viewBox="0 0 48 48"
            fill="rgba(255,255,255,0.8)"
            >
            <path d="M22 36V12L5 24l17 12zm1-12l17 12V12L23 24z"/>
          </svg>`,
          'Cam Rewind',
          () => {
            trackEvent('Clicked Cam Rewind', {
              spotId,
              spotName,
              location: 'cam player',
              camId: camera._id,
              camName: camera.title,
              premiumCam: camera.isPremium,
              isMobileView: window.innerWidth < TABLET_LARGE_WIDTH,
              platform: 'web',
            });
            window.location.href = rewindUrl;
          },
          'contents',
          'jw-cam-rewind'
        );
      }

      if (nighttime && rewindClip) {
        /* istanbul ignore next */
        this.camPlayer.on('error', () => {
          this.rewindClipError = true;
          this.camPlayer.load({
            file: camera.streamUrl,
            withCredentials: camera.streamUrl.indexOf('authorization') > -1,
            image: camera.stillUrl,
          });
        });
      }

      /* istanbul ignore next */
      this.camPlayer.on('userInactive', () => {
        this.setState({ controlsVisible: false });
      });

      /* istanbul ignore next */
      this.camPlayer.on('userActive', () => {
        this.setState({ controlsVisible: true });
      });

      // fires preRoll Ad.
      if (!isPremium && !this.rewindClipError && !controlledAds) {
        /* istanbul ignore next */
        this.camPlayer.on('beforePlay', () => {
          this.playAd();
          this.setState({ adRunning: true });
          if (this.rewindClipError) this.rewindClipError = false;
          if (onTogglePreRoll) onTogglePreRoll(true);
        });
      }

      /* istanbul ignore next */
      this.camPlayer.on('adTime', (obj) => {
        this.setState({
          adCountdownTime: Math.round(obj.duration - obj.position),
        });
      });

      // executes adError waterfall.
      /* istanbul ignore next */
      this.camPlayer.on('adError', () => {
        this.setState((prevState) => ({
          adCount: prevState.adCount + 1,
        }));
        if (adCount <= 5) {
          this.playAd();
        } else {
          this.resumePlayer(); // if an ad fails to load more than 5 times, play the camera.
        }
      });

      /* istanbul ignore next */
      this.camPlayer.on('error', (error) => {
        // If error isn't due to ads, proceed with network error handling
        if (adCount > 5 || error.code) {
          const networkError = {
            isDown: true,
            message: `We're sorry to report that this camera is down.
            The Surfline team is aware of the issue and is currently working toward a resolution.`,
            subMessage: null,
            altMessage: 'Camera Down',
          };
          this.setState({ networkError });
          // if chunklist sends a 404, wait 1 second and resume play
          if (
            error.message.indexOf('404') > -1 ||
            error.code === 232404 ||
            error.code === 232001
          ) {
            this.setState((prevState) => ({
              notFoundCount: prevState.notFoundCount + 1,
            }));
            if (notFoundCount <= 10) {
              this.setState({
                networkError: {
                  isDown: true,
                  message: 'Refreshing camera stream',
                },
              });
              setTimeout(() => {
                this.setupPlayer();
              }, 1000);
            } else {
              nrNoticeError(error, this.camera);
            }
          }
        }

        /* istanbul ignore next */
        if (handleCamReady && camReady !== true) {
          setCamReady(true);
        }
      });

      // sends companion ad styles and html up to parent
      if (companionHandler) {
        this.camPlayer.on('adCompanions', (event) => {
          companionHandler(event.companions);
        });
      }

      // restarts the cam stream when ad finishes.
      /* istanbul ignore next */
      this.camPlayer.on('adComplete', () => {
        this.setState({ showTimeout: false, adCount: 1, adRunning: false });
        if (onTogglePreRoll) onTogglePreRoll(false);
      });

      // resets the adError waterfall count.
      /* istanbul ignore next */
      this.camPlayer.on('play', () => {
        this.setState({
          isStreaming: true,
        });

        this.trackPlaybackEvent(PLAYED_WEBCAM);
      });

      /* istanbul ignore next */
      this.camPlayer.on('pause', () => {
        const { showTimeout } = this.state;
        this.setState({ isStreaming: false });

        this.trackPlaybackEvent(STOPPED_WEBCAM, showTimeout);
      });

      // coordinates mid roll ad chain
      let timeElapsed = 0;
      let currentTime = 0;

      /* istanbul ignore next */
      this.camPlayer.on('time', (event) => {
        const playerPosition = Math.round(event.position);
        /*
          Because this Event is fired multiple times (<=10) a second,
          this conditional is required for tracking total seconds played.
        */
        if (playerPosition !== currentTime) {
          currentTime = playerPosition;
          timeElapsed += 1;
        }

        if (!isPremium && timeElapsed === this.adInterval) {
          timeElapsed = 0;
          this.playMidrollAds();
        } else if (timeElapsed >= this.premiumTimeout) {
          if (!isMultiCam) {
            this.timeoutPlayer();
          } else {
            this.trackPlaybackEvent(STOPPED_WEBCAM);
            if (isPrimaryCam) {
              timeoutMultiCams();
            }
          }
          timeElapsed = 0;
        }
      });

      /* istanbul ignore next */
      this.camPlayer.on('firstFrame', this.handleFirstFrame);

      /* istanbul ignore next */
      this.camPlayer.on('ready', this.onCamReady);
    }
  }

  getHostedAdClassName = () => {
    const { nighttime } = this.props;
    return classNames({
      'quiver-cam-player__player__host-ad': true,
      'quiver-cam-player__player__host-ad--rewind-aware': nighttime,
    });
  };

  /**
   * @description For non-premium users or areas where we have disabled autoplay
   * the `firstFrame` jw-player event won't fire, and with the new cam fade in
   * animation I created we have the `handleCamReady` prop which when true requires
   * us to call `setCamReady` in order to let the consuming app know the cam has loaded.
   * @author jmonson (Jeremy Monson)
   * @memberof CamPlayer
   */
  onCamReady = () => {
    const { handleCamReady, camReady, setCamReady, autoplay, isPremium } =
      this.props;

    const noAutoplay = autoplay === AUTOPLAY_DISABLED;
    if ((!isPremium || noAutoplay) && handleCamReady && !camReady) {
      setCamReady(true);
    }
  };

  handleFirstFrame = () => {
    const {
      handleCamReady,
      controlledPlayback,
      camReady,
      setCamReady,
      isPlaying,
    } = this.props;

    if (handleCamReady && camReady !== true) {
      setCamReady(true);
    }

    if (controlledPlayback && !isPlaying) {
      this.pausePlayer();
    }
  };

  /**
   * @param {PLAYED_WEBCAM | STOPPED_WEBCAM} event
   * @param {boolean} [isTimeout]
   */
  trackPlaybackEvent = (event, isTimeout = false) => {
    const { location, camera, spotName, isFavorite, spotId } = this.props;

    trackEvent(event, {
      category: location,
      premiumCam: camera.isPremium,
      spotName,
      spotId,
      camName: camera.title,
      camId: camera._id,
      favorite: isFavorite,
      isTimeout: event === STOPPED_WEBCAM ? isTimeout : undefined,
    });
  };

  pausePlayer = () => {
    this.setState({ showTimeout: false, isStreaming: false });
    if (this.camPlayer?.pause) {
      this.camPlayer.pause();
    }
  };

  timeoutPlayer = () => {
    this.setState({ showTimeout: true, isStreaming: false });
    if (this.camPlayer?.pause) {
      this.camPlayer.pause();
    }
  };

  resumePlayer = () => {
    this.setState({ showTimeout: false, isStreaming: true });
    if (this.camPlayer?.play) {
      this.camPlayer.play();
    }
  };

  camPlayerClassName = (settings) => {
    const { showUpSell, showTimeout } = this.state;
    const { isPrerecorded, adRunning, isPremium } = settings;
    return classNames({
      'quiver-cam-player': true,
      'quiver-cam-player--upsell': showUpSell,
      'quiver-cam-player--timeout': showTimeout,
      'quiver-cam-player--prerecorded': isPrerecorded,
      'quiver-cam-player--ad-not-running': !adRunning,
      'quiver-cam-player--free': !isPremium,
    });
  };

  messageConfig() {
    const { camera, messageType, isClip } = this.props;
    const { status } = camera;
    const { networkError } = this.state;
    const showSubMessage = messageType === SUB;

    if (isClip) {
      return {
        camStatusMessage: {
          message: status.message,
          subMessage: null,
        },
        camDownMessage: {
          message: status.message,
          subMessage: null,
        },
      };
    }

    const messageData = {
      camStatusMessage: {
        message: messageType === ALT ? status.altMessage : status.message,
        subMessage: showSubMessage ? status.subMessage : null,
      },
      camDownMessage: {
        message:
          messageType === ALT ? networkError.altMessage : networkError.message,
        subMessage: showSubMessage ? networkError.subMessage : null,
      },
    };

    return messageData;
  }

  // Midroll daisychain: 30-15-30
  playMidrollAds() {
    const { onToggleMidRoll } = this.props;
    this.setState({ showUpSell: true, midRoll: true });
    this.pausePlayer();
    if (onToggleMidRoll) onToggleMidRoll(true);

    setTimeout(() => {
      this.setState({
        showUpSell: false,
        adFreePopup: true,
      });
      this.resumePlayer();
      if (onToggleMidRoll) onToggleMidRoll(false);
    }, this.upsellDuration * 1000);
  }

  playAd() {
    const { isPersistentCam } = this.props;
    if (isPersistentCam) {
      return;
    }
    const playerWidth = this.camPlayer.getWidth();
    const playerHeight = this.camPlayer.getHeight();
    const adTag = this.getAdTag(playerWidth, playerHeight);
    if (adTag) this.camPlayer.playAd(adTag);
  }

  render() {
    const {
      cssModules,
      nighttime,
      rewindClip,
      adCountdown,
      camera,
      playerId,
      adblock,
      onClickUpSell,
      onClickAdBlock,
      onDetectedAdBlock,
      funnelUrl,
      camHostAd,
      showOverlays,
      isMultiCam,
      prerecordedTimeRange,
      handleCamReady,
      camReady,
      setCamReady,
      isPremium,
      adFreePopupText,
      playbackRateControls,
    } = this.props;
    const {
      showTimeout,
      showUpSell,
      isStreaming,
      adCountdownTime,
      networkError,
      adRunning,
      controlsVisible,
      adFreePopup,
    } = this.state;
    const { status } = camera;
    const rewindUrl = this.getRewindUrl();

    // This is to remove 'SOCAL - ' from camera name
    const cameraTitle = camera.title ? camera.title : '';
    const parsedCameraTitle =
      cameraTitle.indexOf('- ') !== -1
        ? cameraTitle.split('- ')[1]
        : cameraTitle;

    const prerecordedStart =
      prerecordedTimeRange.start &&
      format(new Date(prerecordedTimeRange.start), 'hh:mm');
    const prerecordedEnd =
      prerecordedTimeRange.end &&
      format(new Date(prerecordedTimeRange.end), 'hh:mma');
    const { isPrerecorded } = camera;

    const isLive = camera.streamUrl && camera.streamUrl.indexOf('.mp4') === -1;

    const messageData = this.messageConfig();

    const isDown = status.isDown || networkError.isDown;
    if (isDown && handleCamReady && camReady !== true) {
      setCamReady(true);
    }

    if (status.isDown || networkError.isDown) {
      return (
        <CamError
          messageData={
            status.isDown
              ? messageData.camStatusMessage
              : messageData.camDownMessage
          }
          rewindUrl={rewindUrl}
        />
      );
    }

    if (adblock && onDetectedAdBlock) {
      return (
        <CamAdblockUpsell
          href={funnelUrl}
          onClick={onClickAdBlock}
          onMount={onDetectedAdBlock}
          rewindUrl={rewindUrl}
        />
      );
    }

    if (nighttime && !rewindClip) {
      return (
        <CamNighttime
          stillUrl={camera.stillUrl}
          rewindUrl={rewindUrl}
          nighttime={nighttime}
        />
      );
    }

    return (
      <div
        className={this.camPlayerClassName({
          isPrerecorded,
          adRunning,
          isPremium,
        })}
      >
        {adFreePopup && adFreePopupText && !isPremium ? (
          <div className="quiver-cam-player--free__ad-free-popup">
            <h3>{adFreePopupText.headline}</h3>
            <p>{adFreePopupText.message}</p>
            <div>
              <button
                type="button"
                onClick={() => {
                  this.setState({ adFreePopup: false });
                }}
              >
                {adFreePopupText.decline}
              </button>
            </div>
            <div>
              <a
                href={funnelUrl}
                onClick={() => {
                  this.setState({ adFreePopup: false });
                }}
              >
                {adFreePopupText.accept}
              </a>
            </div>
          </div>
        ) : null}
        {adCountdown ? (
          <div className="quiver-cam-player__countdown">
            {adCountdownTime ? (
              <span>
                This ad will end in
                {adCountdownTime} seconds
              </span>
            ) : null}
          </div>
        ) : null}
        <div
          className={`quiver-cam-player__player ${
            playbackRateControls
              ? 'quiver-cam-player__player--show-settings'
              : ''
          }`}
        >
          {showOverlays && (
            <div className="quiver-cam-player__player__overlays">
              <div
                style={{ display: isStreaming ? 'block' : 'none' }}
                className={this.getHostedAdClassName()}
              >
                {camHostAd}
              </div>
              {isMultiCam ? (
                <div className="quiver-cam-player__player__overlays__cam-title">
                  {parsedCameraTitle}
                </div>
              ) : null}
            </div>
          )}
          {!adRunning && (
            <div>
              {isPrerecorded && prerecordedStart && !isLive && (
                <div className="quiver-cam-player__player__recorded-earlier">
                  Recorded Earlier&nbsp;&nbsp;|&nbsp;&nbsp;
                  {prerecordedStart} - {prerecordedEnd}
                </div>
              )}
              {isLive && !isPrerecorded && controlsVisible && isStreaming && (
                <div className="quiver-cam-player__player__live">
                  <span>&#8226;</span> Live
                </div>
              )}
            </div>
          )}
          {nighttime && isStreaming && rewindUrl ? (
            <CamRewindMessage href={rewindUrl} />
          ) : null}
          <AspectRatioContent>
            {showTimeout ? (
              <CamTimeout
                stillUrl={camera.stillUrl}
                onClick={this.resumePlayer}
              />
            ) : null}
            {showUpSell ? (
              <div>
                <CamUpsell
                  href={funnelUrl}
                  onClick={onClickUpSell}
                  stillUrl={camera.stillUrl}
                  cssModules={cssModules}
                />
                <div
                  className={
                    cssModules['quiver-cam-player__upsell-countdown'] ||
                    'quiver-cam-player__upsell-countdown'
                  }
                >
                  Next ad will start in{' '}
                  <CountDownTimer countTime={this.upsellDuration} /> seconds
                </div>
              </div>
            ) : null}
            {/* Empty div with playerId must be set dangerously so that
                jwplayer doesn't override a React-managed DOM element
            */}
            <div
              // eslint-disable-next-line react/no-danger
              dangerouslySetInnerHTML={{
                __html: `<div id="${playerId}"></div>`,
              }}
            />
          </AspectRatioContent>
        </div>
      </div>
    );
  }
}

CamPlayer.defaultProps = {
  adFreePopupText: null,
  nighttime: false,
  rewindClip: null,
  adCountdown: false,
  companionHandler: null,
  aspectRatio: '16:9',
  onClickAdBlock: null,
  onClickUpSell: null,
  onDetectedAdBlock: null,
  onToggleMidRoll: null,
  onTogglePreRoll: null,
  onToggleStreaming: null,
  funnelUrl: null,
  advertisingIds: {
    spotId: null,
    subregionId: null,
  },
  isPremium: true,
  isPersistentCam: false,
  adblock: false,
  spotName: null,
  spotId: null,
  viewType: null,
  adTag: null,
  camHostAd: <div />,
  qaFlag: null,
  showOverlays: true,
  timeoutMultiCams: () => null,
  isMultiCam: false,
  isPrimaryCam: false,
  messageType: MSG,
  isClip: false,
  prerecordedTimeRange: {
    start: null,
    end: null,
  },
  setCamReady: () => null,
  controlledPlayback: false,
  camReady: false,
  isPlaying: false,
  handleCamReady: false,
  autoplay: null,
  cssModules: {},
  controlledAds: false,
  adCamId: null,
  playbackRateControls: false,
  location: canUseDOM && window.location.pathname,
  isFavorite: false,
};

CamPlayer.propTypes = {
  adFreePopupText: PropTypes.shape({
    accept: PropTypes.string,
    decline: PropTypes.string,
    message: PropTypes.string,
    headline: PropTypes.string,
  }),
  cssModules: PropTypes.shape(),
  nighttime: PropTypes.bool,
  rewindClip: PropTypes.string,
  adCountdown: PropTypes.bool,
  playerId: PropTypes.string.isRequired,
  camera: cameraPropType.isRequired,
  onDetectedAdBlock: PropTypes.func,
  onToggleMidRoll: PropTypes.func,
  onTogglePreRoll: PropTypes.func,
  onToggleStreaming: PropTypes.func,
  advertisingIds: PropTypes.shape({
    spotId: PropTypes.string,
    subregionId: PropTypes.string,
  }),
  adblock: PropTypes.bool,
  spotName: PropTypes.string,
  spotId: PropTypes.string,
  isPremium: PropTypes.bool,
  isPersistentCam: PropTypes.bool,
  aspectRatio: PropTypes.string,
  funnelUrl: PropTypes.string,
  companionHandler: PropTypes.func,
  onClickAdBlock: PropTypes.func,
  onClickUpSell: PropTypes.func,
  viewType: PropTypes.string,
  adTag: PropTypes.shape(),
  camHostAd: PropTypes.node,
  qaFlag: PropTypes.string,
  showOverlays: PropTypes.bool,
  timeoutMultiCams: PropTypes.func,
  isPrimaryCam: PropTypes.bool,
  isMultiCam: PropTypes.bool,
  isClip: PropTypes.bool,
  messageType: PropTypes.oneOf([ALT, SUB, MSG]),
  prerecordedTimeRange: PropTypes.shape({
    start: PropTypes.string,
    end: PropTypes.string,
  }),
  setCamReady: PropTypes.func,
  controlledPlayback: PropTypes.bool,
  isPlaying: PropTypes.bool,
  camReady: PropTypes.bool,
  handleCamReady: PropTypes.bool,
  autoplay: PropTypes.oneOf([AUTOPLAY_DISABLED, AUTOPLAY_ENABLED, null]),
  controlledAds: PropTypes.bool,
  adCamId: PropTypes.string,
  playbackRateControls: PropTypes.bool,
  location: PropTypes.string,
  isFavorite: PropTypes.bool,
};

export default CamPlayer;
