import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import CamPlayer from './CamPlayer';
import CamError from './CamError';
import CamAdBlock from './CamAdBlock';
import CamRewindMessage from './CamRewindMessage';
import CamTimeout from './CamTimeout';
import CamNighttime from './CamNighttime';

describe('CamPlayer', () => {
  describe('Premium Player', () => {
    beforeEach(() => {
      window.jwplayer = () => ({
        key: null,
        remove: () => true,
      });
      window.jwplayer.defaults = {
        key: 'qPY2++XQlQ4Radjz7uWKEPyvDMsjicsjAmBSzw==',
      };
    });

    afterEach(() => {
      window.jwplayer = () => null;
    });

    const props = {
      playerId: 'quiver-player',
      camera: {
        streamUrl:
          'http://wowzaprod100-i.akamaihd.net/hls/live/254494/c33c37db/playlist.m3u8',
        stillUrl:
          'https://camstills.cdn-surfline.com/hbpierssovcam/latest_small_pixelated.png',
        status: {
          isDown: false,
          message: 'This camera is totally broken...',
        },
      },
      jwPlayerKey: 'TN3oYtsYnZ+cHcCyMwo86x/W68fJWhtWt5w4mA==',
      legacySpotId: '144645',
      camHostAd: (
        <div>
          <a href="#test">cam host ad here</a>
        </div>
      ),
    };

    const newCamProps = {
      streamUrl:
        'http://wowzaprod100-i.akamaihd.net/hls/live/254495/c33c37db/playlist.m3u8',
      stillUrl:
        'https://camstills.cdn-surfline.com/hbpierssovcam/latest_small_pixelated.png',
      status: {
        isDown: false,
        message: 'This camera is totally broken...',
      },
    };

    it('should render the CamPlayer component', () => {
      const wrapper = mount(<CamPlayer {...props} />);
      expect(wrapper.html()).to.contain('<div class="quiver-cam-player');
      expect(wrapper.html()).to.not.contain('<div class="quiver-cam-error');
      expect(wrapper.html()).to.not.contain('<div class="quiver-cam-adblock');
    });

    it('should render the CamPlayer component without overlays', () => {
      const wrapper = mount(<CamPlayer {...props} showOverlays={false} />);
      expect(wrapper.html()).to.not.contain(
        '<div class="quiver-cam-player__player__host-ad'
      );
    });

    it('should be able to resume the player', () => {
      const wrapper = mount(<CamPlayer {...props} />);
      const camPlayerPlayStub = sinon.spy();
      wrapper.instance().camPlayer.play = camPlayerPlayStub;
      wrapper.setState({ showTimeout: true, isStreaming: false });

      wrapper.instance().resumePlayer();
      wrapper.update();
      expect(camPlayerPlayStub).to.have.been.calledOnce();
      expect(wrapper.state('showTimeout')).to.be.false();
      expect(wrapper.state('isStreaming')).to.be.true();
    });

    it('should render the CamTimeout component', () => {
      const wrapper = mount(<CamPlayer {...props} />);
      expect(wrapper.find(CamTimeout)).to.have.length(0);

      wrapper.setState({ isStreaming: false, showTimeout: true });
      expect(wrapper.find(CamTimeout)).to.have.length(1);
    });

    it('should call the appropriate lifecyle hooks', () => {
      const componentDidMount = sinon.spy(
        CamPlayer.prototype,
        'componentDidMount'
      );
      const componentDidUpdate = sinon.spy(
        CamPlayer.prototype,
        'componentDidUpdate'
      );
      const componentWillUnmount = sinon.spy(
        CamPlayer.prototype,
        'componentWillUnmount'
      );
      const wrapper = mount(<CamPlayer {...props} />);

      expect(componentDidMount).to.have.been.called();
      wrapper.setProps({ camera: newCamProps });
      expect(componentDidUpdate).to.have.been.calledTwice();

      wrapper.setProps({ adblock: true, onDetectedAdBlock: () => true });
      expect(componentDidUpdate).to.have.been.calledThrice();

      wrapper.unmount();
      expect(componentWillUnmount).to.have.been.calledOnce();
    });
  });

  describe('Non-Premium Player', () => {
    beforeEach(() => {
      window.jwplayer = () => ({
        addButton: () => true,
        setup: () => true,
        remove: () => true,
        key: null,
        on: () => true,
      });
      window.jwplayer.defaults = {
        key: 'qPY2++XQlQ4Radjz7uWKEPyvDMsjicsjAmBSzw==',
      };
    });

    afterEach(() => {
      window.jwplayer = () => null;
    });
    const props = {
      playerId: 'quiver-player',
      isPremium: false,
      adblock: false,
      camera: {
        streamUrl:
          'http://wowzaprod100-i.akamaihd.net/hls/live/254494/c33c37db/playlist.m3u8',
        stillUrl:
          'https://camstills.cdn-surfline.com/hbpierssovcam/latest_small_pixelated.png',
        status: {
          isDown: false,
          message: 'This camera is totally broken...',
        },
      },
      jwPlayerKey: 'TN3oYtsYnZ+cHcCyMwo86x/W68fJWhtWt5w4mA==',
      legacySpotId: '144645',
      funnelUrl: 'https://www.sufline.com/upgrade',
      advertisingIds: {
        spotId: '1234',
        subregionId: '5678',
      },
      companionHandler: () => true,
      onDetectedAdBlock: () => true,
      onClickUpSell: () => true,
      onClickAdBlock: () => true,
    };

    it('should play preroll ads', () => {
      const setupPlayer = sinon.spy(CamPlayer.prototype, 'setupPlayer');
      mount(<CamPlayer {...props} />);
      expect(setupPlayer).to.have.been.calledOnce();
    });
  });

  describe('Cam Down', () => {
    beforeEach(() => {
      window.jwplayer = () => ({
        key: null,
        remove: () => true,
      });
      window.jwplayer.defaults = {
        key: 'qPY2++XQlQ4Radjz7uWKEPyvDMsjicsjAmBSzw==',
      };
    });

    afterEach(() => {
      window.jwplayer = null;
    });
    const props = {
      playerId: 'quiver-player',
      camera: {
        streamUrl:
          'http://wowzaprod100-i.akamaihd.net/hls/live/254494/c33c37db/playlist.m3u8',
        stillUrl:
          'https://camstills.cdn-surfline.com/hbpierssovcam/latest_small_pixelated.png',
        status: {
          isDown: true,
          message: 'This camera is totally broken...',
        },
      },
      jwPlayerKey: 'TN3oYtsYnZ+cHcCyMwo86x/W68fJWhtWt5w4mA==',
      legacySpotId: '144645',
    };

    it('should display a CamError component', () => {
      const wrapper = mount(<CamPlayer {...props} />);
      const camError = wrapper.find(CamError);
      expect(camError.html()).to.contain('This camera is totally broken');
    });
  });

  describe('Ad Block', () => {
    beforeEach(() => {
      window.jwplayer = () => ({
        key: null,
        remove: () => true,
      });
      window.jwplayer.defaults = {
        key: 'qPY2++XQlQ4Radjz7uWKEPyvDMsjicsjAmBSzw==',
      };
    });

    afterEach(() => {
      window.jwplayer = null;
    });
    const props = {
      playerId: 'quiver-player',
      isPremium: false,
      adblock: true,
      camera: {
        streamUrl:
          'http://wowzaprod100-i.akamaihd.net/hls/live/254494/c33c37db/playlist.m3u8',
        stillUrl:
          'https://camstills.cdn-surfline.com/hbpierssovcam/latest_small_pixelated.png',
        status: {
          isDown: false,
          message: 'This camera is totally broken...',
        },
      },
      jwPlayerKey: 'TN3oYtsYnZ+cHcCyMwo86x/W68fJWhtWt5w4mA==',
      legacySpotId: '144645',
      onDetectedAdBlock: () => true,
    };

    it('should display an CamAdBlock component', () => {
      const wrapper = mount(<CamPlayer {...props} />);
      const camAdBlock = wrapper.find(CamAdBlock);
      expect(camAdBlock.html()).to.contain('<div class="quiver-cam-adblock"');
      expect(camAdBlock.html()).to.contain('Please disable your ad blocker');
    });
  });

  describe('Nighttime', () => {
    beforeEach(() => {
      window.jwplayer = () => ({
        key: null,
        remove: () => true,
      });
      window.jwplayer.defaults = {
        key: 'qPY2++XQlQ4Radjz7uWKEPyvDMsjicsjAmBSzw==',
      };
    });

    afterEach(() => {
      window.jwplayer = null;
    });
    const props = {
      playerId: 'quiver-player',
      isPremium: false,
      nighttime: true,
      camera: {
        streamUrl:
          'http://wowzaprod100-i.akamaihd.net/hls/live/254494/c33c37db/playlist.m3u8',
        stillUrl:
          'https://camstills.cdn-surfline.com/hbpierssovcam/latest_small_pixelated.png',
        status: {
          isDown: false,
          message: 'This camera is totally broken...',
        },
      },
      jwPlayerKey: 'TN3oYtsYnZ+cHcCyMwo86x/W68fJWhtWt5w4mA==',
      legacySpotId: '144645',
      onDetectedAdBlock: () => true,
    };

    it('should display a CamNighttime component', () => {
      const wrapper = mount(<CamPlayer {...props} />);
      const camNight = wrapper.find(CamNighttime);
      expect(camNight.html()).to.contain('<div class="quiver-cam-nighttime"');
      expect(camNight.html()).to.contain(
        'Live camera stream unavailable during nighttime hours'
      );
    });
  });

  describe('Nighttime rewind', () => {
    beforeEach(() => {
      window.jwplayer = () => ({
        key: null,
        remove: () => true,
      });
      window.jwplayer.defaults = {
        key: 'qPY2++XQlQ4Radjz7uWKEPyvDMsjicsjAmBSzw==',
      };
    });

    afterEach(() => {
      window.jwplayer = null;
    });
    const props = {
      nighttime: true,
      rewindClip:
        'http://wowzaprod100-i.akamaihd.net/hls/live/254494/c33c37db/playlist.m3u8',
      playerId: 'quiver-player',
      camera: {
        streamUrl:
          'http://wowzaprod100-i.akamaihd.net/hls/live/254494/c33c37db/playlist.m3u8',
        stillUrl:
          'https://camstills.cdn-surfline.com/hbpierssovcam/latest_small_pixelated.png',
        status: {
          isDown: false,
          message: 'This camera is totally broken...',
        },
      },
      jwPlayerKey: 'TN3oYtsYnZ+cHcCyMwo86x/W68fJWhtWt5w4mA==',
      legacySpotId: '144645',
      camHostAd: (
        <div>
          <a href="#test">cam host ad here</a>
        </div>
      ),
    };

    it('should display a CamRewindMessage component', () => {
      const wrapper = mount(<CamPlayer {...props} />);
      wrapper.setState({ isStreaming: true });
      const camNightRewind = wrapper.find(CamRewindMessage);
      expect(camNightRewind.html()).to.contain(
        '<div class="quiver-cam-rewind-message"'
      );
    });

    it('should display correct text', () => {
      const wrapper = mount(<CamPlayer {...props} />);
      wrapper.setState({ isStreaming: true });
      const camNightRewind = wrapper.find(CamRewindMessage);
      expect(camNightRewind.text()).to.contain(
        "It's nighttime. Enjoy a cam rewind clip."
      );
    });
  });

  describe('Nighttime rewind file unavailable', () => {
    beforeEach(() => {
      window.jwplayer = () => ({
        key: null,
        remove: () => true,
      });
      window.jwplayer.defaults = {
        key: 'qPY2++XQlQ4Radjz7uWKEPyvDMsjicsjAmBSzw==',
      };
    });

    afterEach(() => {
      window.jwplayer = null;
    });
    const props = {
      nighttime: true,
      rewindClip: 'http://null',
      playerId: 'quiver-player',
      camera: {
        streamUrl:
          'http://wowzaprod100-i.akamaihd.net/hls/live/254494/c337db/playlist.m3u8',
        stillUrl:
          'https://camstills.cdn-surfline.com/hbpierssovcam/latest_small_pixelated.png',
        status: {
          isDown: false,
          message: 'This camera is totally broken...',
        },
      },
      jwPlayerKey: 'TN3oYtsYnZ+cHcCyMwo86x/W68fJWhtWt5w4mA==',
      legacySpotId: '144645',
      camHostAd: (
        <div>
          <a href="#test">cam host ad here</a>
        </div>
      ),
    };

    it('should display a CamRewindMessage component and no error', () => {
      const wrapper = mount(<CamPlayer {...props} />);
      wrapper.setState({ isStreaming: true });
      const camNightRewind = wrapper.find(CamRewindMessage);
      expect(camNightRewind.html()).to.contain(
        '<div class="quiver-cam-rewind-message"'
      );
    });

    it('should display correct text', () => {
      const wrapper = mount(<CamPlayer {...props} />);
      wrapper.setState({ isStreaming: true });
      const camNightRewind = wrapper.find(CamRewindMessage);
      expect(camNightRewind.text()).to.contain(
        "It's nighttime. Enjoy a cam rewind clip."
      );
    });

    it('should display component without window.jwplayer', () => {
      window.jwplayer = null;
      const wrapper = mount(<CamPlayer {...props} />);
      wrapper.setState({ isStreaming: true });
      const camNightRewind = wrapper.find(CamRewindMessage);
      expect(camNightRewind.html()).to.contain(
        '<div class="quiver-cam-rewind-message"'
      );
    });
  });
  describe('Multi Cam Page Player', () => {
    let setCamReadyStub;

    beforeEach(() => {
      window.jwplayer = () => ({
        key: null,
        remove: () => true,
      });
      window.jwplayer.defaults = {
        key: 'qPY2++XQlQ4Radjz7uWKEPyvDMsjicsjAmBSzw==',
      };
      setCamReadyStub = sinon.stub();
    });

    afterEach(() => {
      window.jwplayer = () => null;
      setCamReadyStub.reset();
    });

    const props = {
      playerId: 'quiver-player',
      camera: {
        streamUrl:
          'http://wowzaprod100-i.akamaihd.net/hls/live/254494/c33c37db/playlist.m3u8',
        stillUrl:
          'https://camstills.cdn-surfline.com/hbpierssovcam/latest_small_pixelated.png',
        status: {
          isDown: false,
          message: 'This camera is totally broken...',
        },
      },
      jwPlayerKey: 'TN3oYtsYnZ+cHcCyMwo86x/W68fJWhtWt5w4mA==',
      legacySpotId: '144645',
      camHostAd: (
        <div>
          <a href="#test">cam host ad here</a>
        </div>
      ),
      controlledPlayback: true,
      handleCamReady: true,
      setCamReady: setCamReadyStub,
      camReady: false,
      isPlaying: false,
    };

    it('Should change the cam streaming state when the `isPlaying` prop changes', () => {
      const wrapper = mount(<CamPlayer {...props} />);
      const camPlayerPlayStub = sinon.spy();
      const resumePlayerStub = sinon.spy();
      const pausePlayerStub = sinon.spy();
      wrapper.instance().camPlayer.play = camPlayerPlayStub;
      wrapper.instance().resumePlayer = resumePlayerStub;
      wrapper.instance().pausePlayer = pausePlayerStub;
      wrapper.setProps({ isPlaying: true });

      wrapper.update();
      expect(resumePlayerStub).to.have.been.calledOnce();

      wrapper.setProps({ isPlaying: false });

      wrapper.update();
      expect(pausePlayerStub).to.have.been.calledOnce();
    });

    it('Should set the cam ready on the firstFrame event', () => {
      const setCamReadySpy = sinon.spy();
      const wrapper = mount(
        <CamPlayer {...props} setCamReady={setCamReadySpy} />
      );
      wrapper.instance().camPlayer.play = sinon.spy();
      wrapper.instance().camPlayer.pause = sinon.spy();

      wrapper.instance().handleFirstFrame();

      wrapper.update();
      expect(setCamReadySpy).to.have.been.calledOnce();
    });

    it('Should pause the cam on firstFrame if the other cams are not playing', () => {
      const wrapper = mount(<CamPlayer {...props} />);
      const pausePlayerSpy = sinon.spy();
      wrapper.instance().pausePlayer = pausePlayerSpy;
      wrapper.instance().camPlayer.play = sinon.spy();
      wrapper.instance().camPlayer.pause = sinon.spy();

      wrapper.instance().handleFirstFrame();

      expect(pausePlayerSpy).to.have.been.calledOnce();

      wrapper.setProps({ isPlaying: true });
      wrapper.update();
      pausePlayerSpy.resetHistory();

      wrapper.instance().handleFirstFrame();
      expect(pausePlayerSpy).to.not.have.been.called();
    });
  });
});
