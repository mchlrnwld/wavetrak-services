/* eslint-disable react/prop-types */
import React from 'react';
import CamPlayer from './CamPlayer';

const getCamera = (isDown) => ({
  _id: '5834a191e411dc743a5d52f1',
  streamUrl: 'https://cams.cdn-surfline.com/cdn-wc/wc-tstreet/playlist.m3u8',
  stillUrl: 'https://camstills.cdn-surfline.com/hbpierssovcam/latest_small.jpg',
  status: {
    isDown,
    message: `We're sorry to report that this cam is down
            due to a local Internet Service Provider issue.`,
    subMessage: `The Surfline team is aware and currently
            working with their technicians to restore service. Our
            Camera will return as soon as their issue is resolved.
            Thank you for your patience.`,
    altMessage: 'ISP Issue',
  },
  host: {
    text: 'Stay at the Ritz Carlton',
    href: 'http://www.ritzcarlton.com',
  },
});

const getPrerecordedCamera = (isDown) => ({
  _id: '5834a191e411dc743a5d52f1',
  isPrerecorded: true,
  streamUrl:
    'https://prerecorded-clips.staging.surfline.com/1563544920000-1563544980000-5cd2f613e64ac813641c836a.mp4',
  stillUrl: 'https://camstills.cdn-surfline.com/pt-nazareov/latest_small.jpg',
  status: {
    isDown,
    message: `We're sorry to report that this cam is down
            due to a local Internet Service Provider issue.`,
    subMessage: `The Surfline team is aware and currently
            working with their technicians to restore service. Our
            Camera will return as soon as their issue is resolved.
            Thank you for your patience.`,
    altMessage: 'ISP Issue',
  },
  host: {
    text: 'Stay at the Ritz Carlton',
    href: 'http://www.ritzcarlton.com',
  },
  lastPrerecordedClipStartTimeUTC: '2019-07-10T06:10:00.000Z',
  lastPrerecordedClipEndTimeUTC: '2019-07-10T06:11:00.000Z',
});

const getCameraUpload = (isDown) => ({
  _id: '5834a191e411dc743a5d52f1',
  title: 'SOCAL - HB pier, southside',
  streamUrl: 'https://namiaru-upload.cdn-surfline.com/904.mp4',
  stillUrl: 'https://camstills.cdn-surfline.com/hbpierssovcam/latest_small.jpg',
  status: {
    isDown,
    message: 'This camera is totally broken...',
    subMessage: 'This camera is totally broken sub msg...',
    altMessage: 'totally broken...',
  },
  host: {
    text: 'Stay at the Ritz Carlton',
    href: 'http://www.ritzcarlton.com',
  },
});

const getPrerecordedProps = (
  isPremium = false,
  adblock = false,
  isDown = false,
  nighttime = false
) => {
  const camera = getPrerecordedCamera(isDown);
  const { lastPrerecordedClipStartTimeUTC, lastPrerecordedClipEndTimeUTC } =
    camera;
  const prerecordedTimeRange = {
    start: lastPrerecordedClipStartTimeUTC,
    end: lastPrerecordedClipEndTimeUTC,
  };
  return {
    playerId: 'quiver-player',
    camera,
    spotId: '58bdfa7882d034001252e3d8',
    spotName: 'Nazare Overview',
    adblock,
    nighttime,
    rewindClip:
      'https://camrewinds.cdn-surfline.com/pt-nazareov/pt-nazareov.0100.2018-08-11.mp4',
    advertisingIds: {
      spotId: '144645',
      subregionId: '2143',
    },
    isPremium,
    legacySpotId: '144645',
    adUnitCode: 'Video_Companion',
    funnelUrl: 'https://www.surfline.com/upgrade',
    rewindUrl: 'https://www.surfline.com',
    prerecordedTimeRange,
  };
};

const getProps = (adblock, isDown, nighttime) => ({
  playerId: 'quiver-player',
  camera: getCamera(isDown),
  spotId: '58bdea400cec4200133464f0',
  spotName: 'HB Pier Southside Overview',
  adblock,
  nighttime,
  rewindClip:
    'https://camrewinds.cdn-surfline.com/padangcam/padangcam.0100.2017-08-11.mp4',
  advertisingIds: {
    spotId: '144645',
    subregionId: '2143',
  },
  isPremium: false,
  legacySpotId: '144645',
  adUnitCode: 'Video_Companion',
  funnelUrl: 'https://www.surfline.com/upgrade',
  rewindUrl: 'https://www.surfline.com',
});

export default {
  title: 'CamPlayer',
};

export const NonPremium = () => {
  const props = getProps(false, false);
  return (
    <div style={{ width: '50%' }}>
      <CamPlayer
        playerId={props.playerId}
        camera={props.camera}
        advertisingIds={props.advertisingIds}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        isPremium={props.isPremium}
        adblock={props.adblock && !props.isPremium}
        adUnitCode={props.adUnitCode}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        companionHandler={() => true}
        onDetectedAdBlock={() => true}
        onClickAdBlock={() => true}
        onClickUpSell={(properties) => properties}
        camHostAd={
          <div>
            <a href="#test">cam host ad here</a>
          </div>
        }
      />
    </div>
  );
};

NonPremium.story = {
  name: 'Non-premium',
};

export const NonPremiumWAdFreePopup = () => {
  const props = getProps(false, false);
  return (
    <div style={{ width: '50%' }}>
      <CamPlayer
        adFreePopupText={{
          accept: 'START FREE TRIAL',
          decline: 'NO THANKS',
          message: 'Stream cams without the ads.',
          headline: 'Surfline Premium',
        }}
        playerId={props.playerId}
        camera={props.camera}
        advertisingIds={props.advertisingIds}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        isPremium={props.isPremium}
        adblock={props.adblock && !props.isPremium}
        adUnitCode={props.adUnitCode}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        companionHandler={() => true}
        onDetectedAdBlock={() => true}
        onClickAdBlock={() => true}
        onClickUpSell={(properties) => properties}
        camHostAd={
          <div>
            <a href="#test">cam host ad here</a>
          </div>
        }
      />
    </div>
  );
};

NonPremiumWAdFreePopup.story = {
  name: 'Non-premium w/ Ad Free Popup',
};

export const Premium = () => {
  const props = getProps(false, false);
  return (
    <div style={{ width: '50%' }}>
      <CamPlayer
        playerId={props.playerId}
        camera={props.camera}
        rewindUrl={props.rewindUrl}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
      />
    </div>
  );
};

export const PremiumHideOverlays = () => {
  const props = getProps(false, false);
  return (
    <div style={{ width: '50%' }}>
      <CamPlayer
        playerId={props.playerId}
        camera={props.camera}
        rewindUrl={props.rewindUrl}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        showOverlays={false}
      />
    </div>
  );
};

PremiumHideOverlays.story = {
  name: 'Premium - Hide overlays',
};

export const NonPremiumCotm = () => {
  const props = getProps(false, false);
  return (
    <div style={{ width: '50%' }}>
      <CamPlayer
        viewType="HOME"
        adTag={{ iu: '/1024858/Video_Home' }}
        playerId={props.playerId}
        camera={props.camera}
        advertisingIds={props.advertisingIds}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        isPremium={props.isPremium}
        adblock={props.adblock && !props.isPremium}
        adUnitCode={props.adUnitCode}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        companionHandler={() => true}
        onDetectedAdBlock={() => true}
        onClickAdBlock={() => true}
        onClickUpSell={(properties) => properties}
      />
    </div>
  );
};

NonPremiumCotm.story = {
  name: 'Non-premium COTM',
};

export const NonPremiumWithCountdown = () => {
  const props = getProps(false, false);
  return (
    <div style={{ width: '50%' }}>
      <CamPlayer
        adCountdown
        playerId={props.playerId}
        camera={props.camera}
        advertisingIds={props.advertisingIds}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        isPremium={props.isPremium}
        adblock={props.adblock && !props.isPremium}
        adUnitCode={props.adUnitCode}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        companionHandler={() => true}
        onDetectedAdBlock={() => true}
        onClickAdBlock={() => true}
        onClickUpSell={(properties) => properties}
      />
    </div>
  );
};

NonPremiumWithCountdown.story = {
  name: 'Non-premium with countdown',
};

export const AdBlock = () => {
  const props = getProps(true, false);
  return (
    <div style={{ width: '50%' }}>
      <CamPlayer
        playerId={props.playerId}
        camera={props.camera}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        adblock={true && !props.isPremium}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        onDetectedAdBlock={() => true}
      />
    </div>
  );
};

AdBlock.story = {
  name: 'AdBlock',
};

export const CamErrorAltMessage = () => {
  const props = getProps(false, true);
  // pass messageType of 'ALT'
  return (
    <div style={{ width: '50%' }}>
      <CamPlayer
        playerId={props.playerId}
        camera={props.camera}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        adblock={true && !props.isPremium}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        onDetectedAdBlock={() => true}
        messageType="ALT"
      />
    </div>
  );
};

export const CamErrorSubMessage = () => {
  const props = getProps(false, true);
  // pass messageType of 'SUB'
  return (
    <div style={{ width: '50%' }}>
      <CamPlayer
        playerId={props.playerId}
        camera={props.camera}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        adblock={true && !props.isPremium}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        onDetectedAdBlock={() => true}
        messageType="SUB"
      />
    </div>
  );
};

export const CamErrorNoSubMessage = () => {
  // no messageType defaults to 'MSG'
  const props = getProps(false, true);
  return (
    <div style={{ width: '50%' }}>
      <CamPlayer
        playerId={props.playerId}
        camera={props.camera}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        adblock={true && !props.isPremium}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        onDetectedAdBlock={() => true}
      />
    </div>
  );
};

export const CamUpload = () => {
  const props = getProps(true, false);
  return (
    <div style={{ width: '100%' }}>
      <CamPlayer
        playerId={props.playerId}
        camera={getCameraUpload(false)}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        onDetectedAdBlock={() => true}
        camHostAd={
          <div>
            <a href="#test">Camera hosted by Pasea Hotel and Spa</a>
          </div>
        }
      />
    </div>
  );
};

CamUpload.story = {
  name: 'Cam upload',
};

export const CamUploadOnboarding = () => {
  const props = getProps(true, false);
  return (
    <div style={{ width: '100%' }}>
      <CamPlayer
        playerId={props.playerId}
        camera={getCameraUpload(false)}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        onDetectedAdBlock={() => true}
        onboarding
        camHostAd={
          <div>
            <a href="#test">Camera hosted by Pasea Hotel and Spa</a>
          </div>
        }
      />
    </div>
  );
};

CamUploadOnboarding.story = {
  name: 'Cam upload - Onboarding',
};

export const Nighttime = () => {
  const props = getProps(true, false);
  return (
    <div style={{ width: '50%' }}>
      <CamPlayer
        nighttime
        playerId={props.playerId}
        camera={props.camera}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        onDetectedAdBlock={() => true}
      />
    </div>
  );
};

export const NighttimeRewind = () => {
  const props = getProps(true, false);
  return (
    <div style={{ width: '100%' }}>
      <CamPlayer
        nighttime
        rewindClip={props.rewindClip}
        playerId={props.playerId}
        camera={props.camera}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        onDetectedAdBlock={() => true}
        camHostAd={
          <div>
            <a href="#test">Camera hosted by Pasea Hotel and Spa</a>
          </div>
        }
        playbackRateControls
      />
    </div>
  );
};

NighttimeRewind.story = {
  name: 'Nighttime rewind',
};

export const NighttimeRewindNonPremium = () => {
  const props = getProps(false, false);
  return (
    <div style={{ width: '50%' }}>
      <CamPlayer
        nighttime
        rewindClip={props.rewindClip}
        playerId={props.playerId}
        camera={props.camera}
        advertisingIds={props.advertisingIds}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        isPremium={props.isPremium}
        adblock={props.adblock && !props.isPremium}
        adUnitCode={props.adUnitCode}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        companionHandler={() => true}
        onDetectedAdBlock={() => true}
        onClickAdBlock={() => true}
        onClickUpSell={(properties) => properties}
        camHostAd={
          <div>
            <a href="#test">Camera hosted by Pasea Hotel and Spa</a>
          </div>
        }
      />
    </div>
  );
};

NighttimeRewindNonPremium.story = {
  name: 'Nighttime rewind non-premium',
};

export const PrerecordedClipPremium = () => {
  const props = getPrerecordedProps(true);
  return (
    <div style={{ width: '100%' }}>
      <CamPlayer
        playerId={props.playerId}
        camera={props.camera}
        advertisingIds={props.advertisingIds}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        isPremium={props.isPremium}
        adblock={props.adblock && !props.isPremium}
        adUnitCode={props.adUnitCode}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        companionHandler={() => true}
        onDetectedAdBlock={() => true}
        onClickAdBlock={() => true}
        onClickUpSell={(properties) => properties}
        camHostAd={
          <div>
            <a href="#test">cam host ad here</a>
          </div>
        }
        prerecordedTimeRange={props.prerecordedTimeRange}
        playbackRateControls
      />
    </div>
  );
};

PrerecordedClipPremium.story = {
  name: 'Prerecorded clip - Premium',
};

export const PrerecordedClipFree = () => {
  const props = getPrerecordedProps();
  return (
    <div style={{ width: '100%' }}>
      <CamPlayer
        playerId={props.playerId}
        camera={props.camera}
        advertisingIds={props.advertisingIds}
        spotName={props.spotName}
        spotId={props.spotId}
        legacySpotId={props.legacySpotId}
        isPremium={props.isPremium}
        adblock={props.adblock && !props.isPremium}
        adUnitCode={props.adUnitCode}
        funnelUrl={props.funnelUrl}
        rewindUrl={props.rewindUrl}
        companionHandler={() => true}
        onDetectedAdBlock={() => true}
        onClickAdBlock={() => true}
        onClickUpSell={(properties) => properties}
        camHostAd={
          <div>
            <a href="#test">cam host ad here</a>
          </div>
        }
        prerecordedTimeRange={props.prerecordedTimeRange}
      />
    </div>
  );
};

PrerecordedClipFree.story = {
  name: 'Prerecorded clip - Free',
};
