import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import CamUpsell from './CamUpsell';
import CamOverlay from '../../CamOverlay';

describe('CamUpsell', () => {
  const props = {
    stillUrl: 'http://www.somestillUrl.com',
    onClick: sinon.stub(),
    href: 'https://www.surfline.com/upgrade',
  };

  const wrapper = mount(<CamUpsell {...props} />);

  const wrapperWithText = mount(
    <CamUpsell
      {...props}
      text={{
        primary: 'Watch tests, not waves',
        link: 'GO TEST',
        secondary: 'To wait for 30 more seconds, npm run coverage.',
      }}
    />
  );

  it('should render a div with class name', () => {
    expect(wrapper.find('.quiver-cam-upsell')).to.have.length(1);
  });

  it('should render default text if none is passed', () => {
    expect(wrapper.html()).to.contain(
      'Watch waves, not ads. Start your free trial now.'
    );
    expect(wrapper.html()).to.contain('START FREE TRIAL');
    expect(
      wrapper.find('.quiver-cam-upsell__message__subtext').text()
    ).to.contain('To watch for 30 more seconds, \n    refresh the camera.');
  });

  it('should render text passed in as props', () => {
    expect(wrapperWithText.html()).to.contain('Watch tests, not waves');
    expect(wrapperWithText.html()).to.contain(
      'To wait for 30 more seconds, npm run coverage.'
    );
  });

  it('should render a CamOverlay component', () => {
    const camOverlay = wrapper.find(CamOverlay);
    expect(wrapper.find('.quiver-cam-overlay')).to.have.length(1);
    expect(camOverlay.prop('backgroundImage')).to.equal(
      `url(${props.stillUrl})`
    );
  });

  it('should execute the onClick function on button click', () => {
    expect(props.onClick).not.to.have.been.called();
    wrapper.find('button').simulate('click');
    expect(props.onClick).to.have.been.calledOnce();
  });
});
