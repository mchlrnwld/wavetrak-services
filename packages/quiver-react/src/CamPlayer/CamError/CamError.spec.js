import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import CamError from './CamError';
import CamOverlay from '../../CamOverlay';
import ErrorIcon from './ErrorIcon';

describe('CamError', () => {
  const wrapper = mount(<CamError />);

  const wrapperWithText = mount(
    <CamError
      messageData={{
        message: 'This is a cam error test, not a cam test error.',
      }}
    />
  );

  it('should render a div with class name', () => {
    expect(wrapper.html()).to.contain('<div class="quiver-cam-error"');
  });

  it('should render default text if none is passed', () => {
    expect(wrapper.html()).to.contain(
      'We apologize, we are working to restore service ASAP.'
    );
  });

  it('should render text passed in as props', () => {
    expect(wrapperWithText.html()).to.contain(
      'This is a cam error test, not a cam test error.'
    );
  });

  it('should render a CamOverlay component', () => {
    const camOverlay = wrapper.find(CamOverlay);
    expect(camOverlay).to.have.length(1);
    expect(camOverlay.find('.quiver-cam-overlay')).to.have.length(1);
    expect(camOverlay.prop('backgroundImage')).to.contain(
      'cam-down-bg-image.png'
    );
  });
});
describe('ErrorIcon', () => {
  it('should render the ErrorIcon', () => {
    const errorIcon = mount(<ErrorIcon />);
    expect(errorIcon).to.have.length(1);
    expect(errorIcon.find('svg')).to.have.length(1);
  });
});
