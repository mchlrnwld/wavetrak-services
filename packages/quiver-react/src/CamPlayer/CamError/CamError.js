import React from 'react';
import PropTypes from 'prop-types';

import productCDN from '@surfline/quiver-assets';
import CamOverlay from '../../CamOverlay';
import CamRewindIcon from '../../icons/CamRewindIcon';
import AspectRatioContent from '../../AspectRatioContent';

const CamError = ({ messageData, rewindUrl }) => (
  <div className="quiver-cam-error">
    <AspectRatioContent>
      <CamOverlay
        backgroundImage={`url(${productCDN}/backgrounds/cam-down-bg-image.png)`}
      />
      <div className="quiver-cam-error__message">
        <div className="quiver-cam-error__message__text">
          {messageData.message}
        </div>
        <div className="quiver-cam-error__message__subtext">
          {messageData.subMessage}
        </div>
      </div>
      {rewindUrl && (
        <a href={rewindUrl} className="quiver-cam-error__cam-rewind-link">
          <CamRewindIcon />
        </a>
      )}
    </AspectRatioContent>
  </div>
);

CamError.propTypes = {
  messageData: PropTypes.shape({
    message: PropTypes.string,
    subMessage: PropTypes.string,
  }),
  rewindUrl: PropTypes.string,
};

CamError.defaultProps = {
  messageData: {
    message: 'We apologize, we are working to restore service ASAP.',
    subMessage: `The Surfline team is aware and currently working with their
    technicians to restore service. Our Camera will return as soon as their issue
    is resolved. Thank you for your patience.`,
  },
  rewindUrl: null,
};

export default CamError;
