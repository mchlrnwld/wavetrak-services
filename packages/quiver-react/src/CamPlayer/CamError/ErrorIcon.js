import React from 'react';

const ErrorIcon = () => (
  <svg width="64px" height="64px" viewBox="0 0 64 64" version="1.1">
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Group-3-Copy">
        <rect fill="#FFFFFF" x="0" y="0" width="64" height="64" />
        <g id="Group-9" transform="translate(14.000000, 15.000000)">
          <polygon fill="#636665" points="18 0 36 32 0 32" />
          <rect
            fill="#FFFFFF"
            fillRule="nonzero"
            x="17"
            y="10"
            width="2"
            height="12"
          />
          <rect
            fill="#FFFFFF"
            fillRule="nonzero"
            x="17"
            y="25"
            width="2"
            height="3"
          />
        </g>
      </g>
    </g>
  </svg>
);

export default ErrorIcon;
