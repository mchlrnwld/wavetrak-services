import React from 'react';
import PropTypes from 'prop-types';

const NotificationPopup = ({ children }) => (
  <div className="quiver-notification-popup-container">
    <div className="quiver-notification-popup">
      <div className="quiver-notification-popup__triangle" />
      <div className="quiver-notification-popup__content">{children}</div>
    </div>
  </div>
);

NotificationPopup.propTypes = {
  children: PropTypes.node,
};

NotificationPopup.defaultProps = {
  children: null,
};

export default NotificationPopup;
