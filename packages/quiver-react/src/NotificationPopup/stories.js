import React from 'react';
import NotificationPopup from './index';

export default {
  title: 'NotificationPopup',
};

export const StandardNotificationPopup = () => (
  <NotificationPopup>
    <div>
      <h6>This is a popup</h6>
    </div>
  </NotificationPopup>
);
