import PropTypes from 'prop-types';
import React, { useEffect, useState, useCallback } from 'react';
import _difference from 'lodash/difference';
import {
  BrazeEvents,
  getAppboy,
  getContentCard,
  trackContentCardImpressions,
} from '@surfline/web-common';

const isControlCard = (card) => card?.extras?.is_control === 'true';

/**
 * @description Inner functional component for Braze HOC.
 * @param {React.ComponentType} component The component with children
 */
const WithContentCards = ({ children }) => {
  const { card, cards, defaultCard, defaultCards } = children.props;
  const [fetchedCard, setFetchedCard] = useState(card);
  const [fetchedCards, setFetchedCards] = useState(cards);
  const [fetched, setFetched] = useState(false);

  const fetchCards = useCallback(() => {
    if (card?.name) {
      const contentCard = getContentCard(card.name);
      if (isControlCard(contentCard)) {
        trackContentCardImpressions([contentCard]);
        setFetchedCard(defaultCard);
      } else if (defaultCard && contentCard) {
        /*
          Verify all entries in the defaultCard are present in the contentCard from Braze
          If not present then fall back to defaultCard
        */
        const requiredKeys = Object.keys(defaultCard?.extras);
        const keysPresent = Object.keys(contentCard?.extras);
        const isCardValid = _difference(requiredKeys, keysPresent).length === 0;
        setFetchedCard(isCardValid ? contentCard : defaultCard);
      } else {
        setFetchedCard(contentCard ?? defaultCard);
      }
    }
    if (cards) {
      const contentCards = cards.map(({ name }) => {
        const listCard = getContentCard(name);
        if (isControlCard(listCard)) {
          trackContentCardImpressions([listCard]);
          return null;
        }
        return listCard;
      });
      setFetchedCards(
        contentCards.includes(null) ? defaultCards : contentCards
      );
    }
  }, [card, cards, defaultCard, defaultCards]);

  useEffect(() => {
    if (!fetched) {
      if (getAppboy()) fetchCards();
      else {
        window.document.addEventListener(BrazeEvents.BRAZE_LOADED, fetchCards);
        window.document.addEventListener(BrazeEvents.BRAZE_TIMEOUT, fetchCards);
      }

      setFetched(true);
    }
    return () =>
      window.document.removeEventListener(BrazeEvents.BRAZE_LOADED, fetchCards);
  }, [fetchCards, fetched]);

  const componentWithCard = React.cloneElement(children, {
    ...children.props,
    card: fetchedCard,
    cards: fetchedCards,
  });

  return <>{componentWithCard}</>;
};

WithContentCards.propTypes = {
  children: PropTypes.node.isRequired,
};

/**
 * This is a higher-order-component for injecting Braze Content Card(s) into a component. The
 * component should have either a card or cards as a prop type. A card needs to have a name
 * property in order for Braze to do the fetch. Once Braze loads, the prop should be overwritten
 * with the real card.
 *
 * @example card: { name: 'NOTIFICATION_BAR' } => card: { name: 'NOTFICATION_BAR', extras: { ... }}
 * @param {React.ComponentType} Component The component using the card(s)
 * @returns The component with injected card props
 */
const withContentCards = (Component) => (props) =>
  (
    <WithContentCards>
      <Component {...props} />
    </WithContentCards>
  );

withContentCards.propTypes = PropTypes.object();

export default withContentCards;
