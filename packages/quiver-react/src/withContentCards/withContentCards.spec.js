import React from 'react';
import PropTypes from 'prop-types';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import common from '@surfline/web-common';
import withContentCards from './withContentCards';

describe('withContentCards', () => {
  const ComponentWithCardProp = ({ card, cards }) => (
    <>
      <div>{card.id}</div>
      <p className={cards[0].name}>{cards[0].id}</p>
      <p className={cards[1].name}>{cards[1].id}</p>
    </>
  );

  ComponentWithCardProp.propTypes = {
    card: PropTypes.shape({
      name: PropTypes.string,
      extras: PropTypes.shape({
        is_control: PropTypes.string,
        color: PropTypes.string,
      }),
      id: PropTypes.string,
    }),
    cards: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        id: PropTypes.string,
      })
    ),
    defaultCard: PropTypes.shape({
      name: PropTypes.string,
      id: PropTypes.string,
    }),
    /* eslint-disable-next-line react/no-unused-prop-types */
    defaultCards: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        id: PropTypes.string,
      })
    ),
  };

  const DEFAULT_CARD = {
    id: 'defaultId',
    extras: {
      title: 'Default title',
    },
  };
  const DEFAULT_CARDS = [
    { name: 'defaultCard1', id: 'firstDefault' },
    { name: 'defaultCard2', id: 'secondDefault' },
  ];

  ComponentWithCardProp.defaultProps = {
    card: {
      name: 'cardStub',
      id: null,
    },
    cards: [
      {
        name: 'card1',
        id: null,
      },
      {
        name: 'card2',
        id: null,
      },
    ],
    defaultCard: DEFAULT_CARD,
    defaultCards: DEFAULT_CARDS,
  };

  const TEST_CARD = {
    name: 'cardStub',
    id: 'anId',
    extras: {
      title: 'title',
    },
  };

  const TEST_CARD_CONTROL = {
    name: 'cardStub',
    id: 'control',
    extras: {
      color: '#000',
      is_control: 'true',
    },
  };

  const TEST_CARDS = [
    { name: 'card1', id: 'first' },
    { name: 'card2', id: 'second' },
  ];

  const findCard = (cardName) =>
    TEST_CARDS.find((card) => card.name === cardName);

  let getContentCardStub;
  let getAppboyStub;

  beforeEach(() => {
    getAppboyStub = sinon.stub(common, 'getAppboy').returns(true);
    getContentCardStub = sinon.stub(common, 'getContentCard');
  });

  afterEach(() => {
    getAppboyStub.restore();
    getContentCardStub.restore();
  });

  it('inject a card with a matching name', () => {
    getContentCardStub.returns(TEST_CARD);
    const ComponentWithContentCards = withContentCards(ComponentWithCardProp);
    const wrapper = mount(<ComponentWithContentCards />);
    expect(wrapper.find('div')).to.have.length(1);
    expect(wrapper.find('div').first().text()).to.equal(TEST_CARD.id);
  });

  it('inject multiple cards with matching names', () => {
    getContentCardStub.callsFake(findCard);
    const ComponentWithContentCards = withContentCards(ComponentWithCardProp);
    const wrapper = mount(<ComponentWithContentCards />);
    expect(wrapper.find(`.${TEST_CARDS[0].name}`).text()).to.equal(
      TEST_CARDS[0].id
    );
    expect(wrapper.find(`.${TEST_CARDS[1].name}`).text()).to.equal(
      TEST_CARDS[1].id
    );
  });

  it('inject default card if content card not found', () => {
    getContentCardStub.returns(null);
    const ComponentWithDefaultCard = withContentCards(ComponentWithCardProp);
    const wrapper = mount(<ComponentWithDefaultCard />);
    expect(wrapper.find('div')).to.have.length(1);
    expect(wrapper.find('div').first().text()).to.equal(DEFAULT_CARD.id);
  });

  it('inject multiple default cards if content cards not found', () => {
    getContentCardStub.returns(null);
    const ComponentWithDefaultCard = withContentCards(ComponentWithCardProp);
    const wrapper = mount(<ComponentWithDefaultCard />);
    expect(wrapper.find(`.${DEFAULT_CARDS[0].name}`).text()).to.equal(
      DEFAULT_CARDS[0].id
    );
    expect(wrapper.find(`.${DEFAULT_CARDS[1].name}`).text()).to.equal(
      DEFAULT_CARDS[1].id
    );
  });

  it('inject a default card if it finds a ControlCard', () => {
    getContentCardStub.returns(TEST_CARD_CONTROL);
    const ComponentWithDefaultCard = withContentCards(ComponentWithCardProp);
    const wrapper = mount(<ComponentWithDefaultCard />);
    expect(wrapper.find('div')).to.have.length(1);
    expect(wrapper.find('div').first().text()).to.equal(DEFAULT_CARD.id);
  });

  it('inject a default card if the Content Card shape do not match with the predefined Default card shape', () => {
    getContentCardStub.returns({
      name: 'cardStub',
      id: 'cardId',
      extras: {
        title_incorrect_key: 'A title',
      },
    });
    const ComponentWithContentCards = withContentCards(ComponentWithCardProp);
    const wrapper = mount(<ComponentWithContentCards />);
    expect(wrapper.find('div')).to.have.length(1);
    expect(wrapper.find('div').first().text()).to.equal(DEFAULT_CARD.id);
  });
});
