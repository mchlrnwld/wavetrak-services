/* istanbul ignore file */
import React, { Component } from 'react';

const withDevice = (WrappedComponent) =>
  class extends Component {
    constructor() {
      super();
      this.state = {
        mobile: true,
        width: null,
      };
    }

    componentDidMount() {
      window.addEventListener('resize', this.resizeListener);
      this.setDeviceWidth(window.innerWidth);
    }

    componentWillUnmount() {
      if (this.resizeTimeout) clearTimeout(this.resizeTimeout);
      window.removeEventListener('resize', this.resizeListener);
    }

    setDeviceWidth = (width) => {
      this.setState({
        mobile: width < 840,
        width,
      });
    };

    resizeListener = (event) => {
      if (this.resizeTimeout) return;
      this.resizeTimeout = setTimeout(() => {
        this.resizeTimeout = null;
        this.setDeviceWidth(event.target.innerWidth);
      }, 66);
    };

    render() {
      return <WrappedComponent device={{ ...this.state }} {...this.props} />;
    }
  };

export default withDevice;
