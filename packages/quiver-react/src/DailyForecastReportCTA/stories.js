import React from 'react';
import DailyForecastReportCTA from './index';

export default {
  title: 'DailyForecastReportCTA',
};

export const Anonymous = () => (
  <div>
    <DailyForecastReportCTA
      avatarUrl="https://www.gravatar.com/avatar/e37ff1f3ecd59594f25769a7575b571e?d=mm"
      isAnonymous
      forecasterName="Ajith Manmadhan"
      forecasterTitle="Senior Software Engineer"
      lastUpdate="Wed, January 12 at 2:45pm PST"
    />
  </div>
);

export const Registered = () => (
  <div>
    <DailyForecastReportCTA
      avatarUrl="https://www.gravatar.com/avatar/4eb0adfe0a8e5b2bae1a573ef16ec27f?d=mm"
      isAnonymous={false}
      forecasterName="Kurt Korte"
      forecasterTitle="Expert Forecaster"
      lastUpdate="Thursday, Oct 18 at 6am PST"
    />
  </div>
);
