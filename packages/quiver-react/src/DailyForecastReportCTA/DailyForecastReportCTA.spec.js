import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import DailyForecastReportCTA from './DailyForecastReportCTA';

describe('CamCTA', () => {
  it('should render the DailyForecastReport CTA for an Anonymous user', () => {
    const wrapper = mount(
      <DailyForecastReportCTA
        avatarUrl="https://www.gravatar.com/avatar/4eb0adfe0a8e5b2bae1a573ef16ec27f?d=mm"
        isAnonymous
        forecasterName="Kurt Korte"
        forecasterTitle="Expert Forecaster"
        lastUpdate="Thursday, Oct 18 at 6am PST"
      />
    );
    expect(wrapper.find('.quiver-daily-forecast-report-cta')).to.have.length(1);
    expect(wrapper.html()).to.contain(
      'Want the inside scoop on today’s conditions'
    );
  });
  it('should render the DailyForecastReport CTA for a Registered user', () => {
    const wrapper = mount(
      <DailyForecastReportCTA
        avatarUrl="https://www.gravatar.com/avatar/4eb0adfe0a8e5b2bae1a573ef16ec27f?d=mm"
        isAnonymous={false}
        forecasterName="Kurt Korte"
        forecasterTitle="Expert Forecaster"
        lastUpdate="Thursday, Oct 18 at 6am PST"
      />
    );
    expect(wrapper.find('.quiver-daily-forecast-report-cta')).to.have.length(1);
    expect(wrapper.html()).to.contain(
      'Want the inside scoop on today’s conditions'
    );
  });
});
