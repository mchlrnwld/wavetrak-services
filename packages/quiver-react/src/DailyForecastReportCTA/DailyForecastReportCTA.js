import React from 'react';
import PropTypes from 'prop-types';
import productCDN from '@surfline/quiver-assets';
import { trackClickedSubscribeCTA } from '@surfline/web-common';
import { useDeviceSize } from '../hooks/useDeviceSize';
import Button from '../Button';

const DEFAULT_AVATAR_URL =
  'https://www.gravatar.com/avatar/d41d8cd98f00b204e9800998ecf8427e?d=mm';

const DailyForecastReportCTA = ({
  isAnonymous,
  avatarUrl,
  avatarAlt,
  forecasterName,
  forecasterTitle,
  lastUpdate,
  segmentProperties,
}) => {
  const title = isAnonymous
    ? 'Want the inside scoop on today’s conditions?'
    : 'Want the inside scoop on today’s conditions?';
  const subTitle = isAnonymous
    ? 'Go Premium to access daily reports, swell analyses, and forecast outlooks.'
    : 'Go Premium for unlimited access to expert swell analyses and forecast breakdowns.';
  const buttonText = isAnonymous ? 'SIGN UP' : 'START FREE TRIAL';
  const buttonLink = isAnonymous ? '/create-account' : '/upgrade';
  const lastUpdated = `Last updated ${lastUpdate}`;
  const { isMobile } = useDeviceSize();
  const backgroundImage = isMobile
    ? `${productCDN}/backgrounds/daily_forecast_report-mobile_blur.png`
    : `${productCDN}/backgrounds/daily_forecast_report-desktop_blur.png`;
  const onClickHandler = () => {
    trackClickedSubscribeCTA(segmentProperties);
    window.location.assign(buttonLink);
  };
  return (
    <div
      style={{ backgroundImage: `url(${backgroundImage})` }}
      className="quiver-daily-forecast-report-cta"
    >
      <div className="quiver-daily-forecast-report-cta__content">
        <div className="quiver-daily-forecast-report-cta__forecaster-profile">
          <div className="quiver-daily-forecast-report-cta__forecaster-profile_image">
            <img
              src={`${avatarUrl || DEFAULT_AVATAR_URL}&s=60`}
              alt={avatarAlt || 'Surfline Avatar'}
            />
          </div>
          <div className="quiver-daily-forecast-report-cta__forecaster-details">
            <div className="quiver-daily-forecast-report-cta__forecaster-details_info">
              <h6>{forecasterName}</h6>
              <span>{forecasterTitle}</span>
            </div>
            <div className="quiver-daily-forecast-report-cta__forecaster-details_last-update">
              {lastUpdated}
            </div>
          </div>
        </div>
        <h4 className="quiver-daily-forecast-report-cta__title">{title}</h4>
        <span className="quiver-daily-forecast-report-cta__subtitle">
          {subTitle}
        </span>
        <div className="quiver-daily-forecast-report-cta__button">
          <Button onClick={onClickHandler}>{buttonText}</Button>
        </div>
      </div>
    </div>
  );
};

DailyForecastReportCTA.propTypes = {
  isAnonymous: PropTypes.bool,
  avatarUrl: PropTypes.string,
  avatarAlt: PropTypes.string,
  forecasterName: PropTypes.string,
  forecasterTitle: PropTypes.string,
  lastUpdate: PropTypes.string,
  segmentProperties: PropTypes.shape({}).isRequired,
};

DailyForecastReportCTA.defaultProps = {
  isAnonymous: false,
  avatarUrl: null,
  avatarAlt: null,
  forecasterName: null,
  forecasterTitle: null,
  lastUpdate: null,
};

export default DailyForecastReportCTA;
