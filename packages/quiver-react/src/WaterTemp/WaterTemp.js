import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const WaterTemperature = ({
  weatherIconPath,
  spotName,
  waterTemp,
  className,
  units,
}) => {
  const waterClassName = classNames({
    'quiver-water-temp': true,
    [className]: !!className,
  });

  return (
    <div className={waterClassName}>
      <h4>Water Temp</h4>
      <div>
        <img
          src={`${weatherIconPath}/WATER_ICON.svg`}
          alt={`${spotName} Water Temp`}
        />
        {waterTemp.min} - {waterTemp.max} <span>º{units}</span>
      </div>
    </div>
  );
};

WaterTemperature.propTypes = {
  weatherIconPath: PropTypes.string.isRequired,
  spotName: PropTypes.string.isRequired,
  units: PropTypes.string,
  waterTemp: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number,
  }),
  className: PropTypes.string,
};

WaterTemperature.defaultProps = {
  waterTemp: {
    min: 0,
    max: 0,
  },
  units: 'f',
  className: '',
};

export default WaterTemperature;
