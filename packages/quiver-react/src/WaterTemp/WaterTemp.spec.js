import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import WaterTemperature from './WaterTemp';

const props = {
  weather: {
    temperature: 71,
    condition: 'NIGHT_CLEAR_NO_RAIN',
  },
  waterTemp: {
    min: 75,
    max: 77,
  },
  weatherIconPath: 'http://weathericoncdn.surfline.com',
  units: 'F',
  spotName: 'HB Pier Southside',
};

describe('WaterTemp', () => {
  it('should render the water temperature', () => {
    const wrapper = mount(<WaterTemperature {...props} />);
    expect(wrapper).to.have.length(1);
    expect(wrapper.text()).to.contain('75 - 77 ºF');
    expect(wrapper.find('img').prop('src')).to.be.equal(
      `${props.weatherIconPath}/WATER_ICON.svg`
    );
    expect(wrapper.find('img').prop('alt')).to.be.equal(
      `${props.spotName} Water Temp`
    );
  });
});
