import React from 'react';
import PropTypes from 'prop-types';

const NotificationPill = ({ children, bgColor }) => (
  <div className="quiver-notification--pill" style={{ background: bgColor }}>
    {children}
  </div>
);

NotificationPill.propTypes = {
  children: PropTypes.node.isRequired,
  bgColor: PropTypes.string,
};
NotificationPill.defaultProps = {
  bgColor: null,
};
export default NotificationPill;
