import React from 'react';
import NotificationPill from './index';

const parentContainerStyles = {
  width: '530px',
  height: '150px',
};
const ChildComponent = () => (
  <div
    style={{
      width: 'auto',
      height: 'auto',
      padding: '15px',
      background: 'white',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      margin: '15px',
    }}
  >
    Sample Content
  </div>
);

export default {
  title: 'NotificationPill',
};

export const Default = () => (
  <div style={parentContainerStyles}>
    <NotificationPill>
      <ChildComponent />
    </NotificationPill>
  </div>
);

Default.story = {
  name: 'default',
};

export const CustomBackground = () => (
  <div style={parentContainerStyles}>
    <NotificationPill bgColor="#011D38">
      <ChildComponent />
    </NotificationPill>
  </div>
);
