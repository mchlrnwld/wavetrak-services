import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import NotificationPill from './NotificationPill';

describe('NotificationPill', () => {
  it('should render Notification Pill into DOM', () => {
    const wrapper = shallow(<NotificationPill />);
    const container = wrapper.find('.quiver-notification--pill');
    expect(container).to.have.length(1);
  });
  it('should render Notification Pill into DOM with custom BG color', () => {
    const wrapper = shallow(<NotificationPill bgColor="#011D38" />);
    const container = wrapper.find('.quiver-notification--pill');
    expect(container.prop('style')).to.have.property('background', '#011D38');
    expect(container).to.have.length(1);
  });
});
