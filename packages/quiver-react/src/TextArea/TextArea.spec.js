import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import TextArea from './TextArea';

describe('TextArea', () => {
  it('should initialize and set visited state if defaultValue exists', () => {
    const noDefaultWrapper = shallow(<TextArea />);
    const defaultWrapper = shallow(<TextArea defaultValue="Default Value" />);

    const noDefaultTextArea = noDefaultWrapper.find('textarea');
    const defaultTextArea = defaultWrapper.find('textarea');

    expect(noDefaultTextArea.prop('defaultValue')).not.to.be.ok();
    expect(defaultTextArea.prop('defaultValue')).to.equal('Default Value');
    expect(noDefaultWrapper.state('visited')).to.be.false();
    expect(defaultWrapper.state('visited')).to.be.true();
  });

  it('should pass textarea props to textarea component', () => {
    const textarea = {
      name: 'Test Name',
      onChange: () => true,
    };
    const wrapper = shallow(<TextArea textarea={textarea} />);
    const textareaWrapper = wrapper.find('textarea');

    expect(textareaWrapper.prop('name')).to.equal('Test Name');
    expect(textareaWrapper.prop('onChange')).to.equal(textarea.onChange);
  });

  it('should pass id into textarea and label', () => {
    const wrapper = shallow(<TextArea id="testid" />);
    const labelWrapper = wrapper.find('label');
    const textareaWrapper = wrapper.find('textarea');

    expect(labelWrapper.prop('htmlFor')).to.equal('testid');
    expect(textareaWrapper.prop('id')).to.equal('testid');
  });

  it('should display the label', () => {
    const wrapper = shallow(<TextArea label="TextArea Label" />);
    const labelWrapper = wrapper.find('label');
    expect(labelWrapper.text()).to.equal('TextArea Label');
  });

  it('should handle onFocus and onBlur', () => {
    const textarea = {
      onFocus: sinon.spy(),
      onBlur: sinon.spy(),
    };
    const wrapper = shallow(<TextArea textarea={textarea} />);
    const textareaWrapper = wrapper.find('textarea');

    expect(textarea.onFocus.called).to.be.false();
    expect(textarea.onBlur.called).to.be.false();

    textareaWrapper.simulate('focus');
    expect(textarea.onFocus.calledOnce).to.be.true();
    expect(textarea.onBlur.called).to.be.false();

    textareaWrapper.simulate('blur');
    expect(textarea.onFocus.calledOnce).to.be.true();
    expect(textarea.onBlur.calledOnce).to.be.true();
  });

  it('should show error message with correct text', () => {
    const wrapper = shallow(<TextArea message="Error Message" error />);
    const messageWrapper = wrapper.find('.quiver-text-area__message');
    expect(messageWrapper.text()).to.equal('Error Message');
  });

  it('should attach quiver-text-area class name', () => {
    const wrapper = shallow(<TextArea />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal('quiver-text-area');
  });

  it('should attach parent-width modifier class name if parentWidth set', () => {
    const wrapper = shallow(<TextArea parentWidth />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-text-area quiver-text-area--parent-width'
    );
  });

  it('should attach success modifier class name if success set', () => {
    const wrapper = shallow(<TextArea success />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-text-area quiver-text-area--success'
    );
  });

  it('should attach warning modifier class name if warning set', () => {
    const wrapper = shallow(<TextArea warning />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-text-area quiver-text-area--warning'
    );
  });

  it('should attach error modifier class name if error set', () => {
    const wrapper = shallow(<TextArea error />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-text-area quiver-text-area--error'
    );
  });

  it('should pass custom style object into children', () => {
    const style = {
      fontFamily: 'container',
      label: {
        fontFamily: 'label',
      },
      textarea: {
        fontFamily: 'textarea',
      },
      message: {
        fontFamily: 'message',
      },
    };
    const wrapper = shallow(<TextArea message="Some message" style={style} />);
    const containerWrapper = wrapper.find('div');
    const labelWrapper = containerWrapper.find('label');
    const textareaWrapper = containerWrapper.find('textarea');
    const messageWrapper = containerWrapper.find('.quiver-text-area__message');

    expect(containerWrapper.prop('style')).to.equal(style);
    expect(labelWrapper.prop('style')).to.equal(style.label);
    expect(textareaWrapper.prop('style')).to.equal(style.textarea);
    expect(messageWrapper.prop('style')).to.equal(style.message);
  });
});
