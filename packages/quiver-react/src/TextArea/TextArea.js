import React, { Component } from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';

export default class TextArea extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visited: !!props.defaultValue,
    };
  }

  onFocus = (event) => {
    const { textarea } = this.props;
    this.setState({ visited: true });

    if (textarea && textarea.onFocus) {
      textarea.onFocus(event);
    }
  };

  className = () => {
    const { success, warning, error, parentWidth } = this.props;
    const { visited } = this.state;
    return classNames({
      'quiver-text-area': true,
      'quiver-text-area--parent-width': parentWidth,
      'quiver-text-area--visited': visited,
      'quiver-text-area--success': success,
      'quiver-text-area--warning': warning,
      'quiver-text-area--error': error,
    });
  };

  render() {
    const { id, defaultValue, label, textarea, message, style } = this.props;

    return (
      <div className={this.className()} style={style}>
        <label htmlFor={id} style={style && style.label}>
          {label}
        </label>
        <textarea
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...textarea}
          id={id}
          defaultValue={defaultValue}
          onFocus={this.onFocus}
          style={style && style.textarea}
        />
        {message ? (
          <span
            className="quiver-text-area__message"
            style={style && style.message}
          >
            {message}
          </span>
        ) : null}
      </div>
    );
  }
}

TextArea.propTypes = {
  id: PropTypes.string,
  defaultValue: PropTypes.string,
  label: PropTypes.string,
  message: PropTypes.string,
  error: PropTypes.bool,
  touched: PropTypes.bool,
  success: PropTypes.bool,
  warning: PropTypes.bool,
  parentWidth: PropTypes.bool,
  textarea: PropTypes.shape({
    onFocus: PropTypes.func,
  }),
  style: PropTypes.shape({
    textarea: PropTypes.shape({}),
    message: PropTypes.shape({}),
    label: PropTypes.shape({}),
  }),
};

TextArea.defaultProps = {
  id: '',
  defaultValue: '',
  label: '',
  message: '',
  error: false,
  touched: false,
  success: false,
  warning: false,
  parentWidth: false,
  textarea: {},
  style: {},
};
