import React from 'react';
import { action } from '@storybook/addon-actions';
import TextArea from './index';

export default {
  title: 'TextArea',
};

export const Default = () => <TextArea id="initial" label="Initial TextArea" />;

Default.story = {
  name: 'default',
};

export const DefaultWithDefaultValue = () => (
  <TextArea
    id="defaultvalue"
    label="Initial TextArea with Default Value"
    defaultValue="Default Value"
  />
);

DefaultWithDefaultValue.story = {
  name: 'default with default value',
};

export const DefaultWithHelperText = () => (
  <TextArea
    id="initial"
    label="Initial TextArea with Helper Text"
    message="Helper Text"
    helper
  />
);

DefaultWithHelperText.story = {
  name: 'default with helper text',
};

export const DefaultParentWidth = () => (
  <TextArea id="initial" label="Initial TextArea" parentWidth />
);

DefaultParentWidth.story = {
  name: 'default parent width',
};

export const Success = () => (
  <TextArea
    id="success"
    defaultValue="Success"
    label="Success TextArea"
    success
  />
);

Success.story = {
  name: 'success',
};

export const Warning = () => (
  <TextArea
    id="warning"
    defaultValue="Warning"
    label="Warning TextArea"
    warning
  />
);

Warning.story = {
  name: 'warning',
};

export const Error = () => (
  <TextArea
    id="error"
    defaultValue="Error"
    label="Error TextArea"
    message="Error Message"
    error
  />
);

Error.story = {
  name: 'error',
};

export const CustomStyles = () => (
  <TextArea
    id="customstyles"
    label="Custom Styles"
    message="Some Message"
    style={{
      margin: '20px 50px',
      textarea: {
        color: '#22d736',
      },
      label: {
        fontWeight: 'bold',
        fontStyle: 'italic',
      },
      message: {
        color: '#ffbe00',
      },
    }}
  />
);

CustomStyles.story = {
  name: 'custom styles',
};

export const FocusAndBlurEvents = () => (
  <TextArea
    id="focusandblurevents"
    label="Focus and Blur Events"
    textarea={{
      onFocus: action('focused'),
      onBlur: action('blurred'),
    }}
  />
);

FocusAndBlurEvents.story = {
  name: 'focus and blur events',
};
