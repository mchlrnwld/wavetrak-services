import React from 'react';
import Favorite from './index';

export default {
  title: 'Favorite',
};

export const Default = () => (
  <Favorite
    settings={{ units: { surfHeight: 'FT' } }}
    favorite={{
      _id: 'abc123',
      name: 'Log Cabins',
      conditions: { human: false, value: 'EPIC' },
      waveHeight: { min: 12, max: 15, occasional: 6 },
      cameras: [],
    }}
  />
);

Default.story = {
  name: 'default',
};

export const DefaultConditionsV2 = () => (
  <Favorite
    settings={{ units: { surfHeight: 'FT' } }}
    favorite={{
      _id: 'abc123',
      name: 'Log Cabins',
      conditions: { human: false, value: 'VERY_GOOD' },
      waveHeight: { min: 12, max: 15, occasional: 6 },
      cameras: [],
    }}
    sevenRatings
  />
);

DefaultConditionsV2.story = {
  name: 'default with new condition colors',
};

export const Flat = () => (
  <Favorite
    settings={{ units: { surfHeight: 'FT' } }}
    favorite={{
      _id: 'abc123',
      name: 'Log Cabins',
      conditions: { human: false, value: 'FLAT' },
      waveHeight: { min: 0, max: 0, occasional: null },
      cameras: [],
    }}
  />
);

Flat.story = {
  name: 'flat',
};

export const Mobile = () => (
  <Favorite
    settings={{ units: { surfHeight: 'FT' } }}
    favorite={{
      _id: 'abc123',
      name: 'Log Cabins',
      conditions: { human: false, value: 'GOOD' },
      waveHeight: { min: 0, max: 0, occasional: null },
      cameras: [],
    }}
    large
    mobile
  />
);

Mobile.story = {
  name: 'mobile',
};

export const MobileConditionsV2 = () => (
  <Favorite
    settings={{ units: { surfHeight: 'FT' } }}
    favorite={{
      _id: 'abc123',
      name: 'Log Cabins',
      conditions: { human: false, value: 'VERY_GOOD' },
      waveHeight: { min: 0, max: 0, occasional: null },
      cameras: [],
    }}
    large
    mobile
    sevenRatings
  />
);

MobileConditionsV2.story = {
  name: 'mobile with new condition colors',
};

export const MobileWithCamera = () => (
  <Favorite
    settings={{ units: { surfHeight: 'FT' } }}
    favorite={{
      _id: 'abc123',
      name: 'Log Cabins',
      conditions: { human: false, value: 'GOOD' },
      waveHeight: { min: 0, max: 0, occasional: null },
      cameras: [
        {
          _id: '12345',
          title: 'Log Cabins',
          streamUrl: '',
          stillUrl: '',
          rewindBaseUrl: '',
          status: {
            isDown: false,
            message: '',
          },
        },
      ],
    }}
    large
    mobile
  />
);

MobileWithCamera.story = {
  name: 'mobile with camera',
};
