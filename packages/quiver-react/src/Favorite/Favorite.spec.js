import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';
import Favorite from './Favorite';
import CamIcon from '../icons/CamIcon';

const onClickLink = (text, trackingProps) => trackingProps;

describe('Favorite', () => {
  it('should render link, condition, name, height and units', () => {
    const wrapper = shallow(
      <Favorite
        settings={{ units: { surfHeight: 'FT' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: 12, max: 15, occasional: 18 },
        }}
        onClickLink={onClickLink}
      />
    );
    const linkWrappers = wrapper.find('a');
    const conditionWrapper = wrapper.find(
      '.quiver-favorite__item-condition--epic'
    );
    const nameWrapper = wrapper.find(
      '.quiver-favorite__item__wrapper__top-name'
    );
    const heightWrapper = wrapper.find(
      '.quiver-favorite__item__wrapper__bottom-height'
    );
    const unitWrapper = wrapper.find(
      '.quiver-favorite__item__wrapper__bottom-units'
    );
    expect(linkWrappers).to.have.length(1);

    const onClick = sinon.spy();
    expect(onClick.called).to.be.false();
    linkWrappers.simulate('click');

    expect(conditionWrapper).to.have.length(1);
    expect(nameWrapper).to.have.length(1);
    expect(heightWrapper).to.have.length(1);
    expect(unitWrapper).to.have.length(1);
    expect(wrapper.html()).to.contain('12-15');
  });
  it('should render surf condition when large', () => {
    const wrapper = mount(
      <Favorite
        settings={{ units: { surfHeight: 'M' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: 0, max: 0, occasional: 18 },
        }}
        large
        onClickLink={onClickLink}
      />
    );

    const waveHeightWrapper = wrapper.find(
      '.quiver-favorite__item__wrapper__bottom-height'
    );
    expect(waveHeightWrapper).to.have.length(1);
    expect(wrapper.html()).to.contain('FLAT');

    const conditionWrapper = wrapper.find('.quiver-surf-conditions');
    expect(conditionWrapper).to.have.length(1);
    expect(wrapper.html()).to.contain('EPIC');

    // large we do not show condition classNames
    expect(
      wrapper.find('.quiver-favorite__item-condition').exists()
    ).to.be.false();
    expect(
      wrapper.find('.quiver-favorite__item-condition--epic').exists()
    ).to.not.be.true();
    expect(
      wrapper.find('.quiver-favorite__item-condition--lola').exists()
    ).to.not.be.true();
  });
  it('should not render condition dot when large', () => {
    const wrapper = mount(
      <Favorite
        settings={{ units: { surfHeight: 'M' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: 0, max: 0, occasional: 18 },
        }}
        large
        onClickLink={onClickLink}
      />
    );
    const conditionWrapper = wrapper.find('.quiver-favorite__item-condition');
    expect(conditionWrapper).to.have.length(0);
  });
  it('should render cam icon when large and favorite has camera', () => {
    const wrapper = mount(
      <Favorite
        settings={{ units: { surfHeight: 'M' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: 0, max: 0, occasional: 18 },
          cameras: [
            {
              _id: '12345',
              title: 'Pipeline',
              streamUrl: '',
              stillUrl: '',
              rewindBaseUrl: '',
              status: {
                isDown: false,
                message: '',
              },
            },
          ],
        }}
        large
        onClickLink={onClickLink}
      />
    );
    const conditionWrapper = wrapper.find('.quiver-cam-icon');
    expect(conditionWrapper).to.have.length(1);
  });
  it('should render a multi cam icon when large and favorite has multiple cameras', () => {
    const wrapper = mount(
      <Favorite
        settings={{ units: { surfHeight: 'M' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: 0, max: 0, occasional: 18 },
          cameras: [
            {
              _id: '12345',
              title: 'Pipeline',
              streamUrl: '',
              stillUrl: '',
              rewindBaseUrl: '',
              status: {
                isDown: false,
                message: '',
              },
            },
            {
              _id: '12345',
              title: 'Pipeline',
              streamUrl: '',
              stillUrl: '',
              rewindBaseUrl: '',
              status: {
                isDown: false,
                message: '',
              },
            },
          ],
        }}
        large
        onClickLink={onClickLink}
      />
    );
    const conditionWrapper = wrapper.find('.quiver-multi-cam-icon');
    expect(conditionWrapper).to.have.length(1);
  });
  it('should not render cam icon when large and favorite has no cameras', () => {
    const wrapper = mount(
      <Favorite
        settings={{ units: { surfHeight: 'M' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: 0, max: 0, occasional: 18 },
          cameras: [],
        }}
        large
        onClickLink={onClickLink}
      />
    );
    const conditionWrapper = wrapper.find('.quiver-cam-icon');
    expect(conditionWrapper).to.have.length(0);
  });
  it('should not render cam icon when favorite is not large and has camera', () => {
    const wrapper = mount(
      <Favorite
        settings={{ units: { surfHeight: 'M' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: 0, max: 0, occasional: 18 },
          cameras: [
            {
              _id: '12345',
              title: 'Pipeline',
              streamUrl: '',
              stillUrl: '',
              rewindBaseUrl: '',
              status: {
                isDown: false,
                message: '',
              },
            },
          ],
        }}
        onClickLink={onClickLink}
      />
    );
    const conditionWrapper = wrapper.find('.quiver-cam-icon');
    expect(conditionWrapper).to.have.length(0);

    // large we do not show condition classNames
    expect(
      wrapper.find('.quiver-favorite__item-condition').exists()
    ).to.be.true();
    expect(
      wrapper.find('.quiver-favorite__item-condition--epic').exists()
    ).to.be.true();
    expect(
      wrapper.find('.quiver-favorite__item-condition--lola').exists()
    ).to.not.be.true();
  });
  it('should render flat when max <1 and units = M', () => {
    const wrapper = shallow(
      <Favorite
        settings={{ units: { surfHeight: 'M' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: 0, max: 0, occasional: 18 },
        }}
        onClickLink={onClickLink}
      />
    );
    expect(wrapper.html()).to.contain('FLAT');
  });

  it('should render no conditions classes if conditions is blank', () => {
    const wrapper = shallow(
      <Favorite
        settings={{ units: { surfHeight: 'FT' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { value: null },
          waveHeight: { min: 0, max: 0, occasional: 18 },
        }}
        onClickLink={onClickLink}
      />
    );
    expect(wrapper.html()).to.contain('FLAT');
    expect(
      wrapper.find('.quiver-favorite__item-condition').exists()
    ).to.be.true();
    expect(
      wrapper.find('.quiver-favorite__item-condition--epic').exists()
    ).to.not.be.true();
    expect(
      wrapper.find('.quiver-favorite__item-condition--lola').exists()
    ).to.not.be.true();
  });

  it('should render flat when max <1 and units = FT', () => {
    const wrapper = shallow(
      <Favorite
        settings={{ units: { surfHeight: 'FT' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: 0, max: 0, occasional: 18 },
        }}
        onClickLink={onClickLink}
      />
    );
    expect(wrapper.html()).to.contain('FLAT');
  });
  it('should render flat when min and max are missing', () => {
    const wrapper = shallow(
      <Favorite
        settings={{ units: { surfHeight: 'FT' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: null, max: null, occasional: 18 },
        }}
        onClickLink={onClickLink}
      />
    );
    expect(wrapper.html()).to.contain('FLAT');
  });
  it('should have mobile and large classes when mobile and large props are true', () => {
    const wrapper = shallow(
      <Favorite
        settings={{ units: { surfHeight: 'FT' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: null, max: null, occasional: 18 },
        }}
        large
        mobile
        onClickLink={onClickLink}
      />
    );
    const mobileWrapper = wrapper.find('.quiver-favorite--mobile');
    const largeWrapper = wrapper.find('.quiver-favorite--large');
    expect(mobileWrapper).to.have.length(1);
    expect(largeWrapper).to.have.length(1);
  });

  it('should render the single camIcon with `withCamIcon` prop', () => {
    const wrapper = shallow(
      <Favorite
        settings={{ units: { surfHeight: 'FT' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: null, max: null, occasional: 18 },
          cameras: [1],
        }}
        withCamIcon
        onClickLink={onClickLink}
      />
    );
    const camIconWrapper = wrapper.find(CamIcon);
    expect(camIconWrapper).to.have.length(1);
    expect(camIconWrapper.prop('isMultiCam')).to.equal(false);
  });

  it('should render the multi camIcon with `withCamIcon` prop', () => {
    const wrapper = shallow(
      <Favorite
        settings={{ units: { surfHeight: 'FT' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: null, max: null, occasional: 18 },
          cameras: [1, 2],
        }}
        withCamIcon
        onClickLink={onClickLink}
      />
    );
    const camIconWrapper = wrapper.find(CamIcon);
    expect(camIconWrapper).to.have.length(1);
    expect(camIconWrapper.prop('isMultiCam')).to.equal(true);
  });

  it('should render conditions v2 class when sevenRatings prop passed in', () => {
    const wrapper = shallow(
      <Favorite
        settings={{ units: { surfHeight: 'FT' } }}
        favorite={{
          _id: 'abc123',
          name: 'Pipeline',
          conditions: { human: false, value: 'EPIC' },
          waveHeight: { min: 12, max: 15, occasional: 18 },
        }}
        onClickLink={onClickLink}
        sevenRatings
      />
    );
    const conditionWrapper = wrapper.find(
      '.quiver-favorite__item-condition-v2--epic'
    );

    expect(conditionWrapper).to.have.length(1);
  });
});
