import React from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';
import CamIcon from '../icons/CamIcon';
import SurfConditions from '../SurfConditions';
import settingsPropType from '../PropTypes/settingsPropType';
import favoritePropType from '../PropTypes/favoritePropType';
import conditionClassModifier from '../helpers/conditionClassModifier';
import getUnitType from '../helpers/getUnitType';
import generateSlug from '../helpers/generateSlug';
import spotPagePath from '../helpers/spotPagePath';
import conditionIsFlat from '../helpers/conditionIsFlat';
import { getConditionClassWithVariation } from '../helpers/applyConditionVariation';

const favoriteClasses = (large, mobile) => {
  const classObject = {};
  classObject['quiver-favorite'] = true;
  classObject['quiver-favorite--large'] = large;
  classObject['quiver-favorite--mobile'] = mobile;
  return classnames(classObject);
};

const conditionClasses = (condition, expired) => {
  const classObject = {};
  const conditionClass = condition ? conditionClassModifier(condition) : '';
  classObject['quiver-favorite__item-condition'] = true;
  classObject[`quiver-favorite__item-condition--${conditionClass}`] =
    !!condition;
  classObject['quiver-favorite__item-condition--expired'] = expired;
  return classnames(classObject);
};

const conditionClassesV2 = (condition, expired) => {
  const conditionClass = condition
    ? getConditionClassWithVariation(condition)
    : '';
  return classnames({
    'quiver-favorite__item-condition-v2': true,
    [`quiver-favorite__item-condition-v2--${conditionClass}`]: !!condition,
    'quiver-favorite__item-condition-v2--expired': expired,
  });
};

const Favorite = ({
  favorite,
  settings,
  large,
  mobile,
  onClickLink,
  disableLink,
  withCamIcon,
  sevenRatings,
}) => {
  const { _id, name, conditions, waveHeight, cameras } = favorite;
  const isFlat = conditionIsFlat(
    waveHeight.min,
    waveHeight.max,
    settings.units.surfHeight
  );
  const surfHeights =
    isFlat === false ? `${waveHeight.min}-${waveHeight.max}` : 'FLAT';
  const slug = generateSlug(name);
  const trackingProps = {
    favoriteName: name,
  };
  const hasCam = cameras && cameras.length;
  const isMultiCam = cameras && cameras.length > 1;
  return (
    <li className={favoriteClasses(large, mobile)} key={name}>
      {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
      <a
        href={disableLink ? null : spotPagePath(_id, slug)}
        onClick={() =>
          onClickLink({ name: 'Clicked Favorite', properties: trackingProps })
        }
        className="quiver-favorite__item"
      >
        {!large ? (
          <div
            className={
              sevenRatings
                ? conditionClassesV2(
                    conditions.value,
                    conditions.expired && conditions.human
                  )
                : conditionClasses(
                    conditions.value,
                    conditions.expired && conditions.human
                  )
            }
          />
        ) : null}
        <div className="quiver-favorite__item__wrapper">
          <div className="quiver-favorite__item__wrapper__top">
            <span className="quiver-favorite__item__wrapper__top-name">
              {name}
            </span>
            {(large || withCamIcon) && hasCam ? (
              <CamIcon isMultiCam={isMultiCam} />
            ) : null}
          </div>
          <div className="quiver-favorite__item__wrapper__bottom">
            {large ? (
              <SurfConditions
                conditions={conditions.value || 'LOTUS'}
                expired={conditions.expired && conditions.human}
                sevenRatings={sevenRatings}
              />
            ) : null}
            <span className="quiver-favorite__item__wrapper__bottom-height">
              {surfHeights}
            </span>
            <span className="quiver-favorite__item__wrapper__bottom-units">
              {!isFlat ? getUnitType(settings.units.surfHeight) : null}
            </span>
          </div>
        </div>
      </a>
    </li>
  );
};

Favorite.propTypes = {
  disableLink: PropTypes.bool,
  favorite: favoritePropType.isRequired,
  settings: settingsPropType.isRequired,
  large: PropTypes.bool,
  mobile: PropTypes.bool,
  onClickLink: PropTypes.func.isRequired,
  withCamIcon: PropTypes.bool,
  sevenRatings: PropTypes.bool,
};

Favorite.defaultProps = {
  disableLink: false,
  large: false,
  mobile: false,
  withCamIcon: false,
  sevenRatings: false,
};

export default Favorite;
