import React from 'react';
import PropTypes from 'prop-types';

const OrBreak = ({ text }) => (
  <div className="quiver-or">
    <span className="quiver-or__span">{text}</span>
  </div>
);

OrBreak.propTypes = {
  text: PropTypes.string,
};

OrBreak.defaultProps = {
  text: 'or',
};

export default OrBreak;
