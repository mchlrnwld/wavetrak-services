import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';
import Button from './Button';
import Spinner from '../Spinner';
import CheckMark from '../icons/CheckMark';

describe('Button', () => {
  it('should render button component with children', () => {
    const wrapper = shallow(
      <Button>
        <div>Test Children</div>
      </Button>
    );
    const buttonWrapper = wrapper.find('button');

    expect(buttonWrapper).to.be.ok();
    expect(buttonWrapper.children().html()).to.equal(
      '<div>Test Children</div>'
    );
  });

  it('should handle click events', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(<Button onClick={onClick} />);
    expect(onClick.called).to.be.false();

    wrapper.simulate('click');
    expect(onClick.calledOnce).to.be.true();
  });

  it('should pass button type into child button', () => {
    const defaultWrapper = mount(<Button />);
    const resetWrapper = mount(<Button button={{ type: 'reset' }} />);
    const defaultButton = defaultWrapper.find('button');
    const resetButton = resetWrapper.find('button');
    expect(defaultButton.prop('type')).to.be.undefined();
    expect(resetButton.prop('type')).to.equal('reset');
  });

  it('should render spinner, be disabled, and attach loading class on loading state', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(<Button loading onClick={onClick} />);
    const buttonWrapper = wrapper.find('button');
    expect(buttonWrapper.prop('className')).to.equal(
      'quiver-button quiver-button--loading'
    );

    expect(buttonWrapper.prop('disabled')).to.be.true();
    expect(buttonWrapper.childAt(0).is(Spinner)).to.be.true();

    wrapper.simulate('click');
    expect(onClick.called).to.be.false();
  });

  it('should be disabled', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(<Button button={{ disabled: true }} />);
    const buttonWrapper = wrapper.find('button');
    expect(buttonWrapper.prop('className')).to.equal('quiver-button');
    expect(buttonWrapper.prop('disabled')).to.be.true();
    wrapper.simulate('click');
    expect(onClick.called).to.be.false();
  });

  it('should render CheckMark, be disabled, and attach success class on success', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(<Button success />);
    const buttonWrapper = wrapper.find('button');
    expect(buttonWrapper.prop('className')).to.equal(
      'quiver-button quiver-button--success'
    );

    expect(buttonWrapper.prop('disabled')).to.be.true();
    expect(buttonWrapper.childAt(0).is(CheckMark)).to.be.true();

    wrapper.simulate('click');
    expect(onClick.called).to.be.false();
  });

  it('should attach quiver-button class name', () => {
    const wrapper = shallow(<Button />);
    const buttonWrapper = wrapper.find('button');
    expect(buttonWrapper.prop('className')).to.equal('quiver-button');
  });

  it('should attach parent-width modifier class name if parentWidth set', () => {
    const wrapper = shallow(<Button parentWidth />);
    const buttonWrapper = wrapper.find('button');
    expect(buttonWrapper.prop('className')).to.equal(
      'quiver-button quiver-button--parent-width'
    );
  });

  it('should pass custom style object into children', () => {
    const style = {
      fontFamily: 'button',
    };
    const wrapper = shallow(<Button style={style} />);
    const buttonWrapper = wrapper.find('button');
    expect(buttonWrapper.prop('style')).to.equal(style);
  });
});
