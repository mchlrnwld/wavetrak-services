import React from 'react';
import { action } from '@storybook/addon-actions';
import Button from './index';

export default {
  title: 'Button',
  component: Button,
};

const Template = (args) => <Button {...args} />;

export const Default = Template.bind({});

Default.story = {
  name: 'default',
};

Default.args = {
  children: 'Default Button',
  onClick: action('button clicked'),
};

export const Info = Template.bind({});

Info.story = {
  name: 'info',
};

Info.args = {
  children: 'Info Button',
  onClick: action('button clicked'),
  type: 'info',
};

export const ParentWidth = Template.bind({});

ParentWidth.story = {
  name: 'parent width',
};

ParentWidth.args = {
  children: 'Parent Width Button',
  onClick: action('button clicked'),
  parentWidth: true,
};

export const Loading = Template.bind({});

Loading.story = {
  name: 'loading',
};

Loading.args = {
  children: 'Loading Button',
  loading: true,
  onClick: action('button clicked'),
};

export const Success = Template.bind({});

Success.story = {
  name: 'success',
};

Success.args = {
  children: 'Success Button',
  success: true,
  onClick: action('button clicked'),
};

export const Disabled = Template.bind({});

Disabled.story = {
  name: 'disabled',
};

Disabled.args = {
  children: 'Disabled Button',
  disabled: true,
  onClick: action('button clicked'),
};

export const CustomStyles = Template.bind({});

CustomStyles.story = {
  name: 'custom styles',
};

CustomStyles.args = {
  children: 'Custom Styles',
  onClick: action('button clicked'),
  style: {
    margin: '20px 50px',
    fontStyle: 'italic',
  },
};

export const ClickEvent = Template.bind({});

ClickEvent.story = {
  name: 'click event',
};

ClickEvent.args = {
  children: 'Click Button',
  onClick: action('button clicked'),
};
