import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Spinner from '../Spinner';
import CheckMark from '../icons/CheckMark';

export default class Button extends Component {
  getClassNames = (type) => {
    const { loading, success, parentWidth } = this.props;
    const useType = type ? `--${type}` : '';
    const classes = {};
    classes[`quiver-button${useType}`] = true;
    classes['quiver-button--parent-width'] = parentWidth;
    classes['quiver-button--loading'] = loading;
    classes['quiver-button--success'] = success;
    return classNames(classes);
  };

  render() {
    const {
      success,
      loading,
      onClick,
      type,
      children,
      button,
      style,
      disabled,
    } = this.props;

    return (
      // Linter is complaing about button type, but it is part of the button prop
      // eslint-disable-next-line react/button-has-type
      <button
        className={this.getClassNames(type)}
        style={style}
        disabled={loading || success || disabled}
        onClick={!loading ? onClick : undefined}
        {...button}
      >
        {(() => {
          if (loading) {
            return <Spinner />;
          }
          if (success) {
            return <CheckMark fill="white" />;
          }
          return children;
        })()}
      </button>
    );
  }
}

Button.propTypes = {
  loading: PropTypes.bool,
  success: PropTypes.bool,
  parentWidth: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  type: PropTypes.string,
  children: PropTypes.node,
  button: PropTypes.shape({
    type: PropTypes.string,
  }),
  style: PropTypes.shape(),
};

Button.defaultProps = {
  loading: false,
  parentWidth: false,
  disabled: false,
  success: false,
  onClick: () => {},
  children: null,
  type: '',
  style: {},
  button: {},
};
