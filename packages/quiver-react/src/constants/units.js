export const FEET = 'FT';
export const HAWAIIAN = 'HI';
export const METERS = 'M';

// todo: add more units! only basic wave height units exist.
// these units can potentially be grouped by imperial/metric as well
