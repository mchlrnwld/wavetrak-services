/** @prettier */

import PropTypes from 'prop-types';
import React, { Component } from 'react';

import { format } from 'date-fns';
import { scaleLinear, scaleTime, scaleLog } from 'd3-scale';
import { select } from 'd3-selection';
import { getLocalDate, getMaxWindSpeed } from '@surfline/web-common';
import TooltipProvider from '../TooltipProvider';
import {
  getWindItemLeft,
  getWindArrowScale,
  getWindArrowBackground,
  getWindItemDisplay,
  getWindArrowTop,
  getWindArrowRotation,
  getLabelTop,
  getLabelDisplay,
  getBarGradient,
  getHourlyWindBarTop,
  getSelectedBarBottom,
  getAccountForOverflow,
} from './WindGraphHelpers';
import WindTooltip from './WindTooltip';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';

const WIND_ARROW_POLYGON_POINTS =
  '7.5,0 5,8.75 1.5,8.75 7.5,15 13.5,8.75 10,8.75';

class WindGraph extends Component {
  constructor(props) {
    super(props);

    const { data, currentMarkerXPercentage } = props;
    const point = data[Math.floor(data.length * currentMarkerXPercentage)];
    this.state = {
      selectedPoint: point,
      xScale: null,
      yScale: null,
      arrowScale: null,
      canvasRef: null,
    };
    this.windGraphRef = React.createRef();
  }

  componentDidMount() {
    this.renderGraph();
  }

  componentDidUpdate(props, state) {
    const { data } = this.props;
    const { canvasRef } = this.state;

    const canvasRefChanged = canvasRef && canvasRef !== state.canvasRef;
    const dataChanged = props.data !== data;
    if (dataChanged || canvasRefChanged) {
      this.renderGraph();
    }
  }

  componentWillUnmount() {
    /**
     *  Since D3 Manipulates the DOM and we now have two instances of this component, we want to prevent
     *  D3 from doing any dom mutations from an unmounting component
     */
    this.renderGraph = () => null;
  }

  setCanvasRef = (el) =>
    this.setState({
      canvasRef: el,
    });

  /**
   * @description Using the prop `showMobileHourlyGraph` determines the different
   * offsets to use to properly absolutely position graph elements
   *
   * @returns {{windArrowOffset: number, labelOffset: number, windBarOffset: number, selectedBarOffset: number}}
   * @memberof WindGraph
   */
  getGraphOffsets = () => {
    const { showMobileHourlyGraph } = this.props;

    const windArrowOffset = showMobileHourlyGraph ? 4 : 0;
    const labelOffset = showMobileHourlyGraph ? -8 : 16;
    const windBarOffset = showMobileHourlyGraph ? 12 : 0;
    const selectedBarOffset = windArrowOffset * 2;

    return { windArrowOffset, labelOffset, windBarOffset, selectedBarOffset };
  };

  /**
   * @description Parses the current data by each points speed and returns the
   * min and max values
   *
   * @returns {{minWind: number, maxWind: number}} The min and max wind values
   * @memberof WindGraph
   */
  getMinMaxWind = () => {
    const { data } = this.props;
    return {
      minWind: Math.min(...data.map((pt) => pt.speed)),
      maxWind: Math.max(...data.map((pt) => pt.speed)),
    };
  };

  renderGraph = () => {
    const {
      day,
      overallMaxSpeed,
      data,
      units,
      showMobileHourlyGraph,
      currentMarkerXPercentage,
    } = this.props;

    const { canvasRef } = this.state;

    if (!canvasRef) return;

    select(`.quiver-wind-graph--${day} .quiver-wind-graph__nodes`).remove();
    select(`.quiver-wind-graph__current-time`).remove();

    const { windArrowOffset, labelOffset, windBarOffset } =
      this.getGraphOffsets();
    const { minWind, maxWind } = this.getMinMaxWind();
    const percentagePadding = 50 / data.length;

    // Create the scales
    const hourlyWindYOffset = 8;
    const hourlyWindYScaleRange = [
      canvasRef.offsetHeight - hourlyWindYOffset,
      hourlyWindYOffset,
    ];
    const windYScaleRange = [canvasRef.offsetHeight, 0];
    const xScale = scaleTime()
      .range([0 + percentagePadding, 100 - percentagePadding])
      .domain([0, data.length - 1]);

    const yMax = getMaxWindSpeed(
      showMobileHourlyGraph ? maxWind : overallMaxSpeed,
      units,
      showMobileHourlyGraph
    );

    const yScale = scaleLinear()
      .rangeRound(
        showMobileHourlyGraph ? hourlyWindYScaleRange : windYScaleRange
      )
      .domain([0, yMax]);

    const arrowScale = scaleLog()
      .domain([0.5, yMax])
      .range([0, 1.5])
      .clamp(1.5)
      .clamp(true);

    this.setState({
      xScale,
      arrowScale,
      yScale,
    });

    if (showMobileHourlyGraph && day === 0) {
      select(`.quiver-wind-graph__canvas`)
        .append('div')
        .attr('class', 'quiver-wind-graph__current-time')
        .style('left', `${currentMarkerXPercentage * 100}%`);
    }

    const nodes = select(
      `.quiver-wind-graph--${day} .quiver-wind-graph__canvas`
    )
      .append('div')
      .attr('class', 'quiver-wind-graph__nodes')
      .selectAll('.quiver-wind-graph__node')
      .data(data)
      .enter();

    nodes
      .append('svg')
      .attr('class', 'quiver-wind-graph__node')
      .attr('viewBox', '0 0 15 15')
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .style('transform', getWindArrowScale(arrowScale, showMobileHourlyGraph))
      .style('left', getWindItemLeft(xScale))
      .style('display', getWindItemDisplay())
      .style('top', getWindArrowTop(yScale, windArrowOffset))
      .append('polygon')
      .attr('fill', showMobileHourlyGraph ? getWindArrowBackground({}) : '')
      .attr('points', WIND_ARROW_POLYGON_POINTS)
      .attr('transform', getWindArrowRotation());

    nodes
      .append('div')
      .attr('class', 'quiver-wind-graph__label')
      .style('left', getWindItemLeft(xScale))
      .style('top', getLabelTop(yScale, labelOffset))
      .style(
        'display',
        showMobileHourlyGraph ? getLabelDisplay(maxWind, minWind) : 'block'
      )
      .text((d) => Math.round(d.speed));

    if (showMobileHourlyGraph) {
      // Render the hourly wind graph bars
      nodes
        .append('div')
        .attr('class', 'quiver-wind-graph__bar')
        .style('background', getBarGradient({}))
        .style('top', getHourlyWindBarTop(yScale, windBarOffset))
        .style('left', getWindItemLeft(xScale))
        .style('display', getWindItemDisplay());
    }
  };

  /**
   * @description Updates the selected point when triggered by the tooltip provider.
   * Checks that the point has changed before updating so as not to trigger infinite
   * state updates
   *
   * @memberof WindGraph
   */
  setSelectedPoint = (point) => {
    const { selectedPoint } = this.state;

    if (!point) return;

    if (selectedPoint.timestamp !== point.timestamp) {
      this.setState({
        selectedPoint: point,
      });
    }
  };

  renderTooltip = (xPercentage) => (
    <WindTooltip
      {...this.props}
      xPercentage={xPercentage}
      setSelectedPoint={this.setSelectedPoint}
    />
  );

  /**
   * @description Uses the currently selectedPoint to render a vertical bar and label
   * denoting the time of the bar that is currently selected on the graph.
   * [Mobile Hourly Graph Only and requires the graphs to have been rendered]
   *
   * @memberof WindGraph
   */
  renderSelectedPoint = () => {
    const { data } = this.props;
    const { selectedPoint, xScale, yScale, arrowScale } = this.state;
    const { labelOffset, windBarOffset, selectedBarOffset, windArrowOffset } =
      this.getGraphOffsets();
    const { minWind, maxWind } = this.getMinMaxWind();

    const selectedPointIndex = data.findIndex(
      (point) => selectedPoint.timestamp === point.timestamp
    );

    const selectedBarStyle = {
      bottom: getSelectedBarBottom(
        yScale,
        selectedBarOffset,
        labelOffset,
        windBarOffset,
        maxWind,
        minWind
      )(selectedPoint),
      left: getWindItemLeft(xScale)(selectedPoint, selectedPointIndex),
    };
    const selectedLabelStyle = {
      left: getWindItemLeft(xScale)(selectedPoint, selectedPointIndex),
      transform: getAccountForOverflow()(null, selectedPointIndex),
    };
    const selectedWindBarStyle = {
      background: getBarGradient(selectedPoint)(selectedPoint),
      top: getHourlyWindBarTop(yScale, windBarOffset)(selectedPoint),
      left: getWindItemLeft(xScale)(null, selectedPointIndex),
    };

    const selectedWindArrowSvgStyle = {
      transform: getWindArrowScale(arrowScale, true)(selectedPoint),
      left: getWindItemLeft(xScale)(null, selectedPointIndex),
      top: getWindArrowTop(yScale, windArrowOffset)(selectedPoint),
      display: getWindItemDisplay()(selectedPoint),
    };
    const selectedWindArrowPolygonStyle = {
      fill: getWindArrowBackground(selectedPoint)(selectedPoint),
    };

    return (
      <>
        <div
          className="quiver-wind-graph__selected-bar"
          style={selectedBarStyle}
        />
        <div
          className="quiver-wind-graph__selected-day-label"
          style={selectedLabelStyle}
        >
          {format(
            getLocalDate(selectedPoint.timestamp, selectedPoint.utcOffset),
            'haaa'
          )}
        </div>
        <div
          className="quiver-wind-graph__bar quiver-wind-graph__bar--selected"
          style={selectedWindBarStyle}
        />
        <svg
          className="quiver-wind-graph__node quiver-wind-graph__node--selected"
          style={selectedWindArrowSvgStyle}
          viewBox="0 0 15 15"
          preserveAspectRatio="xMinYMin meet"
        >
          <polygon
            style={selectedWindArrowPolygonStyle}
            transform={getWindArrowRotation()(selectedPoint)}
            points={WIND_ARROW_POLYGON_POINTS}
          />
        </svg>
      </>
    );
  };

  render() {
    const {
      day,
      currentMarkerXPercentage,
      showMobileHourlyGraph,
      isHourly,
      isMultiCamPage,
      dateFormat,
    } = this.props;
    const { yScale, xScale, arrowScale } = this.state;
    const shouldRenderSelectedPoint = !!yScale && !!xScale && !!arrowScale;

    return (
      <div
        ref={this.windGraphRef}
        className={`quiver-wind-graph quiver-wind-graph--${day} ${
          showMobileHourlyGraph ? 'quiver-wind-graph--hourly' : ''
        }`}
      >
        <TooltipProvider
          isWindGraph
          showMobileHourlyWindGraph={showMobileHourlyGraph}
          day={day}
          isHourly={isHourly}
          currentMarkerXPercentage={currentMarkerXPercentage}
          renderTooltip={this.renderTooltip}
          dateFormat={dateFormat}
          parentRef={isMultiCamPage && this.windGraphRef.current}
        >
          <div ref={this.setCanvasRef} className="quiver-wind-graph__canvas">
            {showMobileHourlyGraph &&
              shouldRenderSelectedPoint &&
              this.renderSelectedPoint()}
          </div>
        </TooltipProvider>
      </div>
    );
  }
}

WindGraph.propTypes = {
  day: PropTypes.number.isRequired,
  overallMaxSpeed: PropTypes.number.isRequired,
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  lat: PropTypes.number.isRequired,
  lon: PropTypes.number.isRequired,
  currentMarkerXPercentage: PropTypes.number,
  units: PropTypes.string.isRequired,
  showOptimalConditions: PropTypes.bool,
  showMobileHourlyGraph: PropTypes.bool,
  isHourly: PropTypes.bool,
  isMultiCamPage: PropTypes.bool,
  dateFormat: dateFormatPropType,
};

WindGraph.defaultProps = {
  showOptimalConditions: false,
  currentMarkerXPercentage: 0,
  showMobileHourlyGraph: false,
  isHourly: false,
  isMultiCamPage: false,
  dateFormat: defaultDateFormat,
};

export default WindGraph;
