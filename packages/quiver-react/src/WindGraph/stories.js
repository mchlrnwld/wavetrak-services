import React from 'react';
import WindGraph from './WindGraph';

const props = {
  utcOffset: -8,
  day: 0,
  currentMarkerXPercentage: 0,
  lat: 33.654213041213,
  lon: -118.0032588192,
  units: 'KTS',
  data: [
    {
      timestamp: 1518076800,
      direction: 216,
      speed: 1,
      gust: 2,
    },
    {
      timestamp: 1518076800 + 3600,
      direction: 216,
      speed: 1,
      gust: 2,
    },
  ],
  isHourly: false,
};

export default {
  title: 'WindGraph',
};

export const Default = () => <WindGraph {...props} />;

Default.story = {
  name: 'default',
};

export const MobileHourlyGraph = () => (
  <WindGraph {...props} showMobileHourlyGraph />
);

MobileHourlyGraph.story = {
  name: 'mobile hourly graph',
};
