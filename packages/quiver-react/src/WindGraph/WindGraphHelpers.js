/** @prettier */
import { color } from '@surfline/quiver-themes';

const INACTIVE_BAR_GRADIENT =
  'linear-gradient(-180deg, #91B8E0 0%, #ABD7FD 100%)';
const ACTIVE_BAR_GRADIENT = `linear-gradient(-180deg, ${color(
  'dark-blue',
  10
)} 0%, ${color('light-blue')} 100%)`;

/**
 * @description Get the function used by d3 get the left percentage to absolutely position the wind item
 * @param {function} xScale - The xScale used to position the item
 */
export const getWindItemLeft = (xScale) => (_, i) => `${xScale(i)}%`;

/**
 * @description Get the function used by d3 to calculate the top value of the hourly wind bars
 * @param {function} yScale - The yScale used to position the item
 * @param {number} offset - Any offset to be added to the top value
 */
export const getHourlyWindBarTop = (yScale, offset) => (d) =>
  `${yScale(d.speed) + offset}px`;

/**
 * @description Get the function used by D3 to calculate arrow rotation
 */
export const getWindArrowRotation = () => (d) =>
  `rotate(${d.direction} 7.5 7.5)`;

/**
 * @description Get the function used by D3 to decide whether to display a wind item or not
 */
export const getWindItemDisplay = () => (d) =>
  Math.round(d.speed) > 0 ? 'block' : 'none';

/**
 * @description Get the function used by D3 to calculate the wind arrow top value
 * @param {function} yScale - The yScale used to position the item
 * @param {number} offset - Any offset to be added to the top value
 */
export const getWindArrowTop = (yScale, offset) => (d) =>
  `${yScale(d.speed) + offset}px`;

/**
 * @description Get the function used by D3 to calculate the label top value
 * @param {function} yScale - The yScale used to position the item
 * @param {number} offset - Any offset to be added to the top value
 */
export const getLabelTop = (yScale, offset) => (d) =>
  Math.round(d.speed) > 0 ? `${yScale(d.speed) + offset}px` : '75px';

/**
 * @description Get the function used by D3 to calculate the wind arrow background color
 * @param {object} selectedPoint - The currently selected data point
 * @param {number} selectedPoint.timestamp - The timestamp of currently selected data point
 * @returns A color value for the background
 */
export const getWindArrowBackground = (selectedPoint) => (d) =>
  d.timestamp === selectedPoint.timestamp ? color('dark-blue', 10) : '#a7c4e1';

/**
 * @description Get the function used by D3 to calculate the arrow scale
 * @param {function} arrowScale - The arrowScale used to position the item
 * @param {boolean} isHourly - Whether or not the current graph is hourly data
 */
export const getWindArrowScale = (arrowScale, isHourly) => (d) =>
  `translate(-8px, -8px) scale(${!isHourly ? arrowScale(d.speed) : 0.5})`;

/**
 * @description Create a closure to calculate the display value of labels for the hourly graph
 * @param {number} maxWind - The max wind value for the given day
 * @param {number} minWind - The min wind value for the given day
 */
export const getLabelDisplay = (maxWind, minWind) => {
  let didRenderMaxLabel = false;
  let didRenderMinLabel = false;
  return (d) => {
    if (d.speed === maxWind && !didRenderMaxLabel) {
      didRenderMaxLabel = true;
      return 'block';
    }
    if (d.speed === minWind && !didRenderMinLabel) {
      didRenderMinLabel = true;
      return 'block';
    }
    return 'none';
  };
};

/**
 * @description Get the function used by D3 to calculate the bar background gradient
 * @param {object} selectedPoint - The currently selected data point
 * @param {number} selectedPoint.timestamp - The timestamp of currently selected data point
 * @returns {string} A linear gradient value for the background
 */
export const getBarGradient = (selectedPoint) => (d) =>
  d.timestamp === selectedPoint.timestamp
    ? ACTIVE_BAR_GRADIENT
    : INACTIVE_BAR_GRADIENT;

/**
 * @description Get the function used by D3 to calculate the selected point bar display value
 * @param {object} selectedPoint - The currently selected data point
 * @param {number} selectedPoint.timestamp - The timestamp of currently selected data point
 * @returns {string} A display value for the selected point bar
 */
export const getSelectedBarDisplay = (selectedPoint) => (d) =>
  d.timestamp === selectedPoint.timestamp ? 'block' : 'none';

/**
 * @description Get the function used by D3 to calculate the selected point bar bottom value
 * @param {function} yScale - The yScale used to position the item
 * @param {number} barOffset - Any offset to be added to the top value
 * @param {number} labelOffset - The offset for the labels to account for min or max wind bars
 * @returns {string} A string pixel value for the selected bar bottom
 */
export const getSelectedBarBottom = (
  yScale,
  barOffset,
  labelOffset,
  windBarOffset,
  maxWind,
  minWind
) => {
  let didFindMaxWind = false;
  let didFindMinWind = false;
  return (d) => {
    if (Math.round(d.speed) === 0) return '-54px';

    if (d.speed === maxWind && !didFindMaxWind) {
      didFindMaxWind = true;
      return `${-(
        yScale(d.speed) -
        barOffset -
        windBarOffset +
        labelOffset
      )}px`;
    }
    if (d.speed === minWind && !didFindMinWind) {
      didFindMinWind = true;
      return `${-(
        yScale(d.speed) -
        barOffset -
        windBarOffset +
        labelOffset
      )}px`;
    }
    return `${-(yScale(d.speed) - barOffset + labelOffset)}px`;
  };
};

/**
 * @description For the labels on the currently selected hourly wind bar, we have to account for the
 * first and last day labels overflowing the graph container
 */
export const getAccountForOverflow = () => (_, index) => {
  if (index === 0) return 'translateX(-13%)';
  if (index === 23) return 'translateX(-96%)';
  return null;
};
