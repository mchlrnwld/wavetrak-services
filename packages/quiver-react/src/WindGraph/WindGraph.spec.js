import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import { getMapTileUrl } from '@surfline/web-common';
import WindMeasurements from '../WindMeasurements';
import TooltipProvider from '../TooltipProvider';
import WindAnimator from '../WindAnimator';
import WindGraph from './WindGraph';

describe('components / WindGraph', () => {
  const props = {
    utcOffset: -8,
    day: 0,
    currentMarkerXPercentage: 0,
    lat: 33.654213041213,
    lon: -118.0032588192,
    units: 'KTS',
    data: [
      {
        timestamp: 1518076800,
        utcOffset: -8,
        direction: 216,
        speed: 1,
        gust: 2,
      },
      {
        timestamp: 1518076800 + 3600,
        utcOffset: -7,
        direction: 216,
        speed: 1,
        gust: 2,
      },
    ],
    isHourly: false,
  };

  it('renders with tooltip', () => {
    const wrapper = mount(<WindGraph {...props} />);
    const tooltipProvider = wrapper.find(TooltipProvider);
    const tooltipWrapper = shallow(tooltipProvider.prop('renderTooltip')(0));

    const map = tooltipWrapper.find('.quiver-wind-graph__tooltip-map');
    expect(map).to.have.length(1);
    expect(map.prop('style').background).to.equal(
      `url("${getMapTileUrl(152, 88, props.lat, props.lon, 11)}") center`
    );

    const windAnimator = map.find(WindAnimator);
    expect(windAnimator).to.have.length(1);
    expect(windAnimator.prop('direction')).to.deep.equal(
      props.data[0].direction
    );

    const windMeasurements = tooltipWrapper.find(WindMeasurements);
    expect(windMeasurements).to.have.length(1);
    expect(windMeasurements.props()).to.deep.equal({
      speed: props.data[0].speed,
      direction: props.data[0].direction,
      units: props.units,
      gust: props.data[0].gust,
    });
  });

  it('renders with tooltip - using the timestamp utcOffset', () => {
    const wrapper = mount(<WindGraph {...props} />);
    const tooltipProvider = wrapper.find(TooltipProvider);
    const tooltipWrapper = shallow(tooltipProvider.prop('renderTooltip')(0));

    const map = tooltipWrapper.find('.quiver-wind-graph__tooltip-map');
    expect(map).to.have.length(1);
    expect(map.prop('style').background).to.equal(
      `url("${getMapTileUrl(152, 88, props.lat, props.lon, 11)}") center`
    );

    const windAnimator = map.find(WindAnimator);
    expect(windAnimator).to.have.length(1);
    expect(windAnimator.prop('direction')).to.deep.equal(
      props.data[0].direction
    );

    const windMeasurements = tooltipWrapper.find(WindMeasurements);
    expect(windMeasurements).to.have.length(1);
    expect(windMeasurements.props()).to.deep.equal({
      speed: props.data[0].speed,
      direction: props.data[0].direction,
      units: props.units,
      gust: props.data[0].gust,
    });

    const tooltipDateTime = tooltipWrapper.find('TooltipDatetime');
    expect(tooltipDateTime.props()).to.deep.equal({
      utcOffset: props.data[0].utcOffset,
      timestamp: props.data[0].timestamp,
      dateFormat: 'MDY',
      showMinutes: false,
    });
  });

  it('renders the one hourly wind day graph', () => {
    const wrapper = mount(<WindGraph {...props} showMobileHourlyGraph />);
    const hourlyWind = wrapper.find('.quiver-wind-graph--hourly');

    expect(hourlyWind).to.have.length(1);
  });
});
