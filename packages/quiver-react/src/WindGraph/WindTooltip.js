import React from 'react';
import PropTypes from 'prop-types';
import { getMapTileUrl } from '@surfline/web-common';
import WindArrow from '../icons/WindArrow';
import WindMeasurements from '../WindMeasurements';
import CompassRose from '../CompassRose';
import GraphTooltip from '../GraphTooltip';
import OptimalScoreIndicator from '../OptimalScoreIndicator';
import TooltipDatetime from '../TooltipDatetime';
import WindAnimator from '../WindAnimator';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';

/**
 *
 * @typedef {object} Point
 * @property {number} timestamp
 * @property {number} utcOffset
 * @property {number} speed
 * @property {number} direction
 * @property {number} gust
 * @property {number} optimalScore
 */

/**
 *
 * @param {object} props
 * @param {Point[]} props.data
 * @param {number} props.lat
 * @param {number} props.lon
 * @param {number} props.xPercentage
 * @param {boolean} props.showOptimalConditions
 * @param {number} props.utcOffset
 * @param {"KTS" | "MPH" | "KPH"} props.units
 * @param {(point: Point) => void} props.setSelectedPoint
 * @param {string} props.dateFormat
 * @returns {JSX.Element}
 */
const WindTooltip = ({
  data,
  lat,
  lon,
  xPercentage,
  showOptimalConditions,
  utcOffset,
  units,
  setSelectedPoint,
  dateFormat,
}) => {
  const zoom = 11;
  const mapUrl = getMapTileUrl(152, 88, lat, lon, zoom);

  const point = data[Math.floor(data.length * xPercentage)];

  React.useEffect(() => {
    setSelectedPoint(point);
  }, [point, setSelectedPoint]);

  if (!point) return null;

  return (
    <GraphTooltip>
      <div
        className="quiver-wind-graph__tooltip-map"
        style={{ background: `url("${mapUrl}") center` }}
      >
        <div className="quiver-wind-graph__tooltip-rose">
          <CompassRose />
          <WindAnimator
            width={80}
            height={80}
            direction={point.direction}
            speed={Math.round(point.speed)}
          />
        </div>
      </div>
      {showOptimalConditions ? (
        <OptimalScoreIndicator optimalScore={point.optimalScore} />
      ) : null}
      <TooltipDatetime
        utcOffset={point.utcOffset ?? utcOffset}
        timestamp={point.timestamp}
        dateFormat={dateFormat}
      />
      <WindArrow speed={Math.round(point.speed)} degrees={point.direction} />
      <div className="quiver-wind-graph__tooltip-measurements">
        <WindMeasurements
          speed={point.speed}
          direction={point.direction}
          units={units}
          gust={point.gust}
        />
      </div>
    </GraphTooltip>
  );
};

WindTooltip.propTypes = {
  showOptimalConditions: PropTypes.bool.isRequired,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      timestamp: PropTypes.number.isRequired,
      utcOffset: PropTypes.number.isRequired,
      speed: PropTypes.number.isRequired,
      direction: PropTypes.number.isRequired,
      gust: PropTypes.number.isRequired,
      optimalScore: PropTypes.number.isRequired,
    })
  ).isRequired,
  utcOffset: PropTypes.number.isRequired,
  lat: PropTypes.number.isRequired,
  lon: PropTypes.number.isRequired,
  units: PropTypes.string.isRequired,
  xPercentage: PropTypes.number.isRequired,
  setSelectedPoint: PropTypes.func.isRequired,
  dateFormat: dateFormatPropType,
};

WindTooltip.defaultProps = {
  dateFormat: defaultDateFormat,
};

export default WindTooltip;
