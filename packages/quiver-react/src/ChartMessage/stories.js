import React from 'react';
import ChartMessage from './ChartMessage';

export default {
  title: 'ChartMessage',
};

export const Default = () => (
  <ChartMessage message="Some Message For The Charts" />
);

Default.story = {
  name: 'default',
};
