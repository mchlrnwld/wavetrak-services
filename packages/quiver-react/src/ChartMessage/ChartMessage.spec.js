import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import ChartMessage from './ChartMessage';

describe('ChartMessage', () => {
  it('should render the chart message', () => {
    const wrapper = mount(<ChartMessage message="Some Chart Message" />);

    expect(wrapper.find('.quiver-chart-message')).to.have.length(1);
    expect(wrapper.text()).to.be.equal('Some Chart Message');
  });
});
