import PropTypes from 'prop-types';
import React from 'react';

const ChartMessage = ({ message }) => (
  <div className="quiver-chart-message">{message}</div>
);

ChartMessage.propTypes = {
  message: PropTypes.string.isRequired,
};

export default ChartMessage;
