import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const getClass = (rootClass) =>
  classNames({
    'quiver-content-container': true,
    [rootClass]: !!rootClass,
  });

const ContentContainer = ({ children, classes }) => (
  <div className={getClass(classes.root)}>{children}</div>
);

ContentContainer.propTypes = {
  children: PropTypes.node,
  classes: PropTypes.shape({
    root: PropTypes.string,
  }),
};

ContentContainer.defaultProps = {
  classes: {},
  children: null,
};

export default ContentContainer;
