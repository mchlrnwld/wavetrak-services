import React from 'react';
import PageContainer from '../PageContainer';
import ContentContainer from './index';
import PageRail from '../PageRail';

export default {
  title: 'ContentContainer',
};

export const InPageContainer = () => (
  <PageContainer>
    <ContentContainer>
      <div
        style={{
          backgroundColor: 'lightBlue',
          margin: '0',
          height: '100vh',
          width: '100%',
        }}
      >
        Content
      </div>
    </ContentContainer>
  </PageContainer>
);

InPageContainer.story = {
  name: 'in page container',
};

export const WithRails = () => (
  <PageContainer>
    <ContentContainer>
      <PageRail side="left">
        <div
          style={{
            backgroundColor: 'lightBlue',
            margin: '16px 0 0',
            height: 'auto',
          }}
        >
          <div style={{ height: '30vw' }}>Item in Left Rail</div>
        </div>
        <div
          style={{
            backgroundColor: 'lightBlue',
            margin: '16px 0 0',
            height: '200px',
          }}
        >
          Item in Left Rail
        </div>
      </PageRail>
      <PageRail side="right">
        <div
          style={{
            backgroundColor: 'lightBlue',
            margin: '16px 0 0',
            height: '150px',
          }}
        >
          Item in Right Rail
        </div>
        <div
          style={{
            backgroundColor: 'lightBlue',
            margin: '16px 0 0',
            height: '400px',
          }}
        >
          Item in Right Rail
        </div>
      </PageRail>
    </ContentContainer>
  </PageContainer>
);

WithRails.story = {
  name: 'with rails',
};
