import React from 'react';
import PropTypes from 'prop-types';

import LocationIcon from '../../icons/LocationIcon';
import CamIcon from '../../icons/CamIcon';
import ForecastIcon from '../../icons/ForecastIcon';
import TravelIcon from '../../icons/TravelIcon';
import { clickedSearchResults } from '../analytics';

const uuid = () =>
  ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (a) =>
    // eslint-disable-next-line no-bitwise
    (a ^ ((Math.random() * 16) >> (a / 4))).toString(16)
  );

export const renderSuggestion = (searchTerm) => (suggestion) => {
  const {
    _source: { cams, breadCrumbs, name },
    _type,
  } = suggestion;
  const indexOfSearchTerm = name
    .toLowerCase()
    .indexOf(searchTerm.toLowerCase());
  let nameContent;
  if (indexOfSearchTerm > -1) {
    const searchTermLength = searchTerm.length;
    const resultTermLength = name.length;
    const endOfHighlight = indexOfSearchTerm + searchTermLength;
    nameContent = (
      // eslint-disable-next-line jsx-a11y/anchor-is-valid
      <a>
        {name.substring(0, indexOfSearchTerm)}
        <highlight>
          {name.substring(indexOfSearchTerm, endOfHighlight)}
        </highlight>
        {name.substring(endOfHighlight, resultTermLength)}
      </a>
    );
  } else {
    nameContent = (
      // eslint-disable-next-line jsx-a11y/anchor-is-valid
      <a>
        {name
          .toLowerCase()
          .split('')
          .map((char, index) =>
            searchTerm.toLowerCase().indexOf(char) > -1 &&
            index <= name.toLowerCase().indexOf(char) ? (
              <highlight key={uuid()}>{name[index]}</highlight>
            ) : (
              name[index]
            )
          )}
      </a>
    );
  }
  const isMultiCam = cams && cams.length > 1;
  let camClassName = '';
  let iconContent = '';
  let breadCrumbClassName = 'quiver-spot-search__container__subtitle';
  const types = ['geoname', 'subregion', 'spot', 'travel'];
  if (types.indexOf(_type) > -1) {
    if (cams && cams.length && cams[0] !== null) {
      camClassName = 'quiver-spot-search__container__icon__hd-cam';
      iconContent = <CamIcon isMultiCam={isMultiCam} />;
    } else {
      camClassName = 'quiver-spot-search__container__icon__location';
      iconContent = <LocationIcon />;
    }
  }

  let breadcrumb = breadCrumbs ? breadCrumbs.join(' / ') : '';
  breadcrumb =
    breadCrumbs && breadCrumbs.length > 1
      ? `${breadCrumbs[0]} / ${breadCrumbs[breadCrumbs.length - 1]}`
      : null;
  breadCrumbClassName = 'quiver-search__container__subtitle';
  if (types.indexOf(_type) > -1) {
    camClassName = 'quiver-search__suggestion__result';
    if (cams && cams.length && cams[0] !== null) {
      iconContent = <CamIcon isMultiCam={isMultiCam} />;
    } else if (_type.indexOf('spot') !== -1) {
      camClassName = 'quiver-search__suggestion__result__blank';
      iconContent = null;
    } else if (_type.indexOf('subregion') !== -1) {
      iconContent = <ForecastIcon />;
    } else if (_type.indexOf('travel') !== -1) {
      iconContent = <TravelIcon />;
    } else {
      iconContent = <LocationIcon />;
    }
  }

  return (
    <div className={camClassName}>
      {iconContent}
      {nameContent}
      <div className={breadCrumbClassName}>{breadcrumb}</div>
    </div>
  );
};

export const renderSectionTitle = (section) => {
  const {
    hits: { hits },
    suggest,
  } = section;

  const suggestTypes = new Map();
  suggestTypes.set('spot-suggest', 'Surf Spots');
  suggestTypes.set('subregion-suggest', 'Regional Forecast');
  suggestTypes.set('geoname-suggest', 'Map Area');
  suggestTypes.set('travel-suggest', 'Travel Guide');

  if (suggest) {
    const ret = Array.from(suggestTypes.entries()).find(
      (entry) =>
        suggest[entry[0]] &&
        (suggest[entry[0]][0].options.length > 0 || hits.length > 0)
    );
    if (ret) return ret[1];
  }

  return null;
};

const renderSuggestionsContainer = (
  window,
  { containerProps, children, query }
) => (
  <div {...containerProps}>
    {children}
    <div className="footer">
      <a
        href={`/search/${query}`}
        onClick={() =>
          clickedSearchResults(window, null, {
            searchId: null,
            searchResultRank: null,
            destinationPage: `/search/${query}`,
            queryTerm: query,
            resultName: null,
            searchLatency: null,
            resultType: 'see all results',
          })
        }
      >
        See all results
      </a>
    </div>
  </div>
);

renderSuggestionsContainer.propTypes = {
  children: PropTypes.node,
  query: PropTypes.string,
  containerProps: PropTypes.shape,
};

renderSuggestionsContainer.defaultProps = {
  children: null,
  query: '',
  containerProps: {},
};

export const getRenderSuggestionsContainer = (window) => (props) =>
  renderSuggestionsContainer(window, props);
