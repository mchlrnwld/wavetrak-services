import fetchMock from 'fetch-mock';
import { expect } from 'chai';
import {
  getSuggestedSpots,
  getSuggestionValue,
  getSectionSuggestions,
} from './NewSpotSuggestionModel';
import searchFixtures from './fixtures';

describe('NewSpotSuggestionModel', () => {
  describe('getSuggestedSpots', () => {
    const value = 'test';
    const serviceConfig = {
      serviceUrl: 'http://services.sandbox.surfline.com',
    };
    afterEach(() => {
      fetchMock.restore();
    });

    it('should return an empty array if status !== 200', async () => {
      fetchMock.get('*', { body: searchFixtures, status: 400 });
      const spots = await getSuggestedSpots(serviceConfig, value);
      expect(spots).to.deep.equal([]);
    });

    it('should return search data', async () => {
      fetchMock.get('*', { body: searchFixtures });
      const spots = await getSuggestedSpots(serviceConfig, value);
      expect(spots).to.deep.equal(searchFixtures);
    });
  });

  describe('getSuggestionValue', () => {
    it('should return the suggestion text if it exists', () => {
      const suggestion = {
        text: 'Testing',
        _source: {
          name: 'Name from _source object',
        },
      };
      const suggestionValue = getSuggestionValue(suggestion);
      expect(suggestionValue).to.equal('Testing');
    });

    it('should return an empty string if the suggestion does not exist', () => {
      const suggestion = {
        _source: {
          name: 'Name from _source object',
        },
      };
      const suggestionValue = getSuggestionValue(suggestion);
      expect(suggestionValue).to.equal('Name from _source object');
    });
  });

  describe('getSectionSuggestions', () => {
    it('should return empty if an error exists', () => {
      const section = {
        suggest: {},
        error: 'Some Error',
        hits: { hits: [] },
      };
      const suggestions = getSectionSuggestions(section);
      expect(suggestions).to.deep.equal([]);
    });
    it('should return hits if they exist', () => {
      const suggestions = getSectionSuggestions(searchFixtures[0]);
      expect(suggestions).to.deep.equal(searchFixtures[0].hits.hits);
    });
    it('shoudl return empty suggestions if there are no suggestTypes', () => {
      const section = {
        hits: { hits: [] },
        suggest: {
          'spot-suggest': [{ options: [] }],
        },
      };
      const suggestions = getSectionSuggestions(section);
      expect(suggestions).to.deep.equal([]);
    });
  });
});
