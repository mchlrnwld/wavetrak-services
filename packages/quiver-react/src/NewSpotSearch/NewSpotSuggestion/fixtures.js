/* eslint quote-props: 0, max-len: 0 */
export default [
  {
    took: 9,
    timed_out: false,
    _shards: {
      total: 5,
      successful: 5,
      failed: 0,
    },
    hits: {
      total: 298,
      max_score: 10.995068,
      hits: [
        {
          _index: 'spots',
          _type: 'spot',
          _id: '5842041f4e65fad6a7708c51',
          _score: 10.995068,
          _source: {
            breadCrumbs: ['Puerto Rico', 'Aguadilla', 'Montaña Barrio'],
            name: 'Table Top',
            location: {
              lon: -67.1388404959542,
              lat: 18.50806588732911,
            },
            cams: [],
            href: '//new.sandbox.surfline.com/surf-report/table-top/5842041f4e65fad6a7708c51/forecast',
          },
        },
        {
          _index: 'spots',
          _type: 'spot',
          _id: '584204204e65fad6a770920b',
          _score: 8.990909,
          _source: {
            breadCrumbs: [
              'Mexico',
              'Baja California Sur',
              'Mulegé Municipality',
            ],
            name: 'Bahia Tortugas (Turtle Bay)',
            location: {
              lon: -114.899,
              lat: 27.658,
            },
            cams: [],
            href: '//new.sandbox.surfline.com/surf-report/bahia-tortugas-turtle-bay-/584204204e65fad6a770920b/forecast',
          },
        },
        {
          _index: 'spots',
          _type: 'spot',
          _id: '584204204e65fad6a7709263',
          _score: 8.792188,
          _source: {
            breadCrumbs: ['Australia', 'South Australia', 'Onkaparinga'],
            name: 'The Trough',
            location: {
              lon: 138.466553,
              lat: -35.18793,
            },
            cams: [],
            href: '//new.sandbox.surfline.com/surf-report/the-trough/584204204e65fad6a7709263/forecast',
          },
        },
        {
          _index: 'spots',
          _type: 'spot',
          _id: '584204204e65fad6a7709452',
          _score: 8.207378,
          _source: {
            breadCrumbs: ['Australia', 'New South Wales', 'Bega Valley'],
            name: 'Tilba Tilba',
            location: {
              lon: 150.118,
              lat: -36.324,
            },
            cams: [],
            href: '//new.sandbox.surfline.com/surf-report/tilba-tilba/584204204e65fad6a7709452/forecast',
          },
        },
        {
          _index: 'spots',
          _type: 'spot',
          _id: '5842041f4e65fad6a7708fbe',
          _score: 8.1003685,
          _source: {
            breadCrumbs: ['Brazil', 'Ceará', 'São Gonçalo do Amarante'],
            name: 'Tabinha',
            location: {
              lon: -38.911282,
              lat: -3.500935,
            },
            cams: [],
            href: '//new.sandbox.surfline.com/surf-report/tabinha/5842041f4e65fad6a7708fbe/forecast',
          },
        },
      ],
    },
    suggest: {
      'spot-suggest': [
        {
          text: 't',
          offset: 0,
          length: 1,
          options: [
            {
              text: '17th Street',
              _index: 'spots',
              _type: 'spot',
              _id: '5842041f4e65fad6a77088eb',
              _score: 3,
              _source: {
                breadCrumbs: ['United States', 'California', 'Orange County'],
                name: '17th Street',
                location: {
                  lon: -118.0143556189923,
                  lat: 33.66315613358258,
                },
                cams: ['58a37701c9d273fd4f581bf0'],
                href: '//new.sandbox.surfline.com/surf-report/17th-street/5842041f4e65fad6a77088eb/forecast',
              },
              contexts: {
                location: ['9mu'],
              },
            },
            {
              text: '36th St. Newport',
              _index: 'spots',
              _type: 'spot',
              _id: '5842041f4e65fad6a770882a',
              _score: 3,
              _source: {
                breadCrumbs: ['United States', 'California', 'Orange County'],
                name: '36th St. Newport',
                location: {
                  lon: -117.93676257133484,
                  lat: 33.61555743705257,
                },
                cams: ['5834945f3421b20545c4b519'],
                href: '//new.sandbox.surfline.com/surf-report/36th-st-newport/5842041f4e65fad6a770882a/forecast',
              },
              contexts: {
                location: ['9mu'],
              },
            },
            {
              text: '54th St. Newport',
              _index: 'spots',
              _type: 'spot',
              _id: '584204214e65fad6a7709cba',
              _score: 3,
              _source: {
                breadCrumbs: ['United States', 'California', 'Orange County'],
                name: '54th St. Newport',
                location: {
                  lon: -117.94584453105927,
                  lat: 33.62226710774577,
                },
                cams: ['5834949d3421b20545c4b51c'],
                href: '//new.sandbox.surfline.com/surf-report/54th-st-newport/584204214e65fad6a7709cba/forecast',
              },
              contexts: {
                location: ['9mu'],
              },
            },
            {
              text: '56th Street Newport',
              _index: 'spots',
              _type: 'spot',
              _id: '5842041f4e65fad6a7708e54',
              _score: 3,
              _source: {
                breadCrumbs: ['United States', 'California', 'Orange County'],
                name: '56th Street Newport',
                location: {
                  lon: -117.94749140739441,
                  lat: 33.62137370637929,
                },
                cams: ['583494b1e411dc743a5d5268'],
                href: '//new.sandbox.surfline.com/surf-report/56th-street-newport/5842041f4e65fad6a7708e54/forecast',
              },
              contexts: {
                location: ['9mu'],
              },
            },
            {
              text: 'T-Street',
              _index: 'spots',
              _type: 'spot',
              _id: '5842041f4e65fad6a7708830',
              _score: 3,
              _source: {
                breadCrumbs: ['United States', 'California', 'Orange County'],
                name: 'T-Street',
                location: {
                  lon: -117.6207447052,
                  lat: 33.419227624697,
                },
                cams: ['5834a191e411dc743a5d52f1'],
                href: '//new.sandbox.surfline.com/surf-report/t-street/5842041f4e65fad6a7708830/forecast',
              },
              contexts: {
                type: ['spot'],
                location: ['9mu'],
              },
            },
          ],
        },
      ],
    },
    status: 200,
  },
  {
    took: 31,
    timed_out: false,
    _shards: {
      total: 5,
      successful: 5,
      failed: 0,
    },
    hits: {
      total: 27,
      max_score: 7.8029833,
      hits: [
        {
          _index: 'taxonomy',
          _type: 'subregion',
          _id: '58f7efb7dadb30820bb639a1',
          _score: 7.8029833,
          _source: {
            breadCrumbs: null,
            name: 'North Trinidad and Tobago',
            location: null,
            href: '//new.sandbox.surfline.com/surf-forecasts/north-trinidad-and-tobago/58581a836630e24c44879064',
          },
        },
        {
          _index: 'taxonomy',
          _type: 'subregion',
          _id: '58f7f072dadb30820bb7095f',
          _score: 7.7300572,
          _source: {
            breadCrumbs: null,
            name: 'Tokushima',
            location: null,
            href: '//new.sandbox.surfline.com/surf-forecasts/tokushima/58581a836630e24c44879104',
          },
        },
        {
          _index: 'taxonomy',
          _type: 'subregion',
          _id: '58f7f03bdadb30820bb6cd3c',
          _score: 7.7300572,
          _source: {
            breadCrumbs: null,
            name: 'Timor',
            location: null,
            href: '//new.sandbox.surfline.com/surf-forecasts/timor/58581a836630e24c44879073',
          },
        },
        {
          _index: 'taxonomy',
          _type: 'subregion',
          _id: '58f8347bdadb30820bf17b77',
          _score: 7.7300572,
          _source: {
            breadCrumbs: null,
            name: 'Taoyuan',
            location: null,
            href: '//new.sandbox.surfline.com/surf-forecasts/taoyuan/58581a836630e24c448790bf',
          },
        },
        {
          _index: 'taxonomy',
          _type: 'subregion',
          _id: '58f7f0aedadb30820bb74ef2',
          _score: 7.5983133,
          _source: {
            breadCrumbs: null,
            name: 'Tasmania',
            location: null,
            href: '//new.sandbox.surfline.com/surf-forecasts/tasmania/58581a836630e24c44879030',
          },
        },
      ],
    },
    suggest: {
      'subregion-suggest': [
        {
          text: 't',
          offset: 0,
          length: 1,
          options: [
            {
              text: 'Tahiti North',
              _index: 'taxonomy',
              _type: 'subregion',
              _id: '58f7f041dadb30820bb6d303',
              _score: 2,
              _source: {
                breadCrumbs: null,
                name: 'Tahiti North',
                location: null,
                href: '//new.sandbox.surfline.com/surf-forecasts/tahiti-north/58581a836630e24c44878ffb',
              },
              contexts: {
                type: ['subregion'],
              },
            },
            {
              text: 'Tahiti South',
              _index: 'taxonomy',
              _type: 'subregion',
              _id: '58f7f03ddadb30820bb6ceba',
              _score: 2,
              _source: {
                breadCrumbs: null,
                name: 'Tahiti South',
                location: null,
                href: '//new.sandbox.surfline.com/surf-forecasts/tahiti-south/58581a836630e24c44878ffc',
              },
              contexts: {
                type: ['subregion'],
              },
            },
            {
              text: 'Taipei',
              _index: 'taxonomy',
              _type: 'subregion',
              _id: '58f80922dadb30820bcf8df3',
              _score: 2,
              _source: {
                breadCrumbs: null,
                name: 'Taipei',
                location: null,
                href: '//new.sandbox.surfline.com/surf-forecasts/taipei/58581a836630e24c448790bb',
              },
              contexts: {
                type: ['subregion'],
              },
            },
            {
              text: 'Taitung',
              _index: 'taxonomy',
              _type: 'subregion',
              _id: '58f80a43dadb30820bd0cdf8',
              _score: 2,
              _source: {
                breadCrumbs: null,
                name: 'Taitung',
                location: null,
                href: '//new.sandbox.surfline.com/surf-forecasts/taitung/58581a836630e24c448790bd',
              },
              contexts: {
                type: ['subregion'],
              },
            },
            {
              text: 'Tanegashima',
              _index: 'taxonomy',
              _type: 'subregion',
              _id: '58f7f06edadb30820bb705aa',
              _score: 2,
              _source: {
                breadCrumbs: null,
                name: 'Tanegashima',
                location: null,
                href: '//new.sandbox.surfline.com/surf-forecasts/tanegashima/58581a836630e24c4487910b',
              },
              contexts: {
                type: ['subregion'],
              },
            },
          ],
        },
      ],
    },
    status: 200,
  },
  {
    took: 24,
    timed_out: false,
    _shards: {
      total: 5,
      successful: 5,
      failed: 0,
    },
    hits: {
      total: 219,
      max_score: 8.433158,
      hits: [
        {
          _index: 'taxonomy',
          _type: 'geoname',
          _id: '58f7f0b6dadb30820bb7580d',
          _score: 8.433158,
          _source: {
            breadCrumbs: ['Guam', 'Tamuning'],
            name: 'Tamuning-Tumon-Harmon Village',
            location: {
              lon: 144.78138,
              lat: 13.48773,
            },
            href: '//new.sandbox.surfline.com/surf-reports-forecasts-cams/guam/tamuning/tamuning-tumon-harmon-village/4038659',
          },
        },
        {
          _index: 'taxonomy',
          _type: 'geoname',
          _id: '58f7efb7dadb30820bb63961',
          _score: 8.433158,
          _source: {
            breadCrumbs: [],
            name: 'Trinidad and Tobago',
            location: {
              lon: -61,
              lat: 11,
            },
            href: '//new.sandbox.surfline.com/surf-reports-forecasts-cams/trinidad-and-tobago/3573591',
          },
        },
        {
          _index: 'taxonomy',
          _type: 'geoname',
          _id: '58f8336adadb30820bf09b05',
          _score: 7.7300572,
          _source: {
            breadCrumbs: ['Indonesia', 'West Nusa Tenggara'],
            name: 'Tawun',
            location: {
              lon: 116.0081,
              lat: -8.7526,
            },
            href: '//new.sandbox.surfline.com/surf-reports-forecasts-cams/indonesia/west-nusa-tenggara/tawun/7582093',
          },
        },
        {
          _index: 'taxonomy',
          _type: 'geoname',
          _id: '58f80a43dadb30820bd0cdd0',
          _score: 7.7300572,
          _source: {
            breadCrumbs: ['Taiwan', 'Taiwan'],
            name: 'Taitung',
            location: {
              lon: 121.04833,
              lat: 22.88361,
            },
            href: '//new.sandbox.surfline.com/surf-reports-forecasts-cams/taiwan/taiwan/taitung/1668292',
          },
        },
        {
          _index: 'taxonomy',
          _type: 'geoname',
          _id: '58f835dfdadb30820bf29d6b',
          _score: 7.7300572,
          _source: {
            breadCrumbs: ['Greece', 'South Aegean', 'Nomós Kykládon'],
            name: 'Tinos',
            location: {
              lon: 25.13252,
              lat: 37.60393,
            },
            href: '//new.sandbox.surfline.com/surf-reports-forecasts-cams/greece/south-aegean/nom-s-kykl-don/tinos/8133928',
          },
        },
      ],
    },
    suggest: {
      'geoname-suggest': [
        {
          text: 't',
          offset: 0,
          length: 1,
          options: [
            {
              text: 'Taggia',
              _index: 'taxonomy',
              _type: 'geoname',
              _id: '58f83489dadb30820bf189d2',
              _score: 2,
              _source: {
                breadCrumbs: [
                  'Italy',
                  'Liguria',
                  'Provincia di Imperia',
                  'Taggia',
                ],
                name: 'Taggia',
                location: {
                  lon: 7.85223,
                  lat: 43.84612,
                },
                href: '//new.sandbox.surfline.com/surf-reports-forecasts-cams/italy/liguria/provincia-di-imperia/taggia/taggia/3165993',
              },
              contexts: {
                type: ['geoname'],
              },
            },
            {
              text: 'Taggia',
              _index: 'taxonomy',
              _type: 'geoname',
              _id: '58f83489dadb30820bf189ca',
              _score: 2,
              _source: {
                breadCrumbs: ['Italy', 'Liguria', 'Provincia di Imperia'],
                name: 'Taggia',
                location: {
                  lon: 7.85165,
                  lat: 43.84172,
                },
                href: '//new.sandbox.surfline.com/surf-reports-forecasts-cams/italy/liguria/provincia-di-imperia/taggia/6542051',
              },
              contexts: {
                type: ['geoname'],
              },
            },
            {
              text: 'Taghazout',
              _index: 'taxonomy',
              _type: 'geoname',
              _id: '58f7f010dadb30820bb69e8a',
              _score: 2,
              _source: {
                breadCrumbs: ['Morocco', 'Souss-Massa', 'Agadir-Ida-ou-Tnan'],
                name: 'Taghazout',
                location: {
                  lon: -9.67735,
                  lat: 30.58042,
                },
                href: '//new.sandbox.surfline.com/surf-reports-forecasts-cams/morocco/souss-massa/agadir-ida-ou-tnan/taghazout/6545796',
              },
              contexts: {
                type: ['geoname'],
              },
            },
            {
              text: 'Taghazout',
              _index: 'taxonomy',
              _type: 'geoname',
              _id: '58f7f010dadb30820bb69e95',
              _score: 2,
              _source: {
                breadCrumbs: [
                  'Morocco',
                  'Souss-Massa',
                  'Agadir-Ida-ou-Tnan',
                  'Taghazout',
                ],
                name: 'Taghazout',
                location: {
                  lon: -9.71115,
                  lat: 30.54259,
                },
                href: '//new.sandbox.surfline.com/surf-reports-forecasts-cams/morocco/souss-massa/agadir-ida-ou-tnan/taghazout/taghazout/2568012',
              },
              contexts: {
                type: ['geoname'],
              },
            },
            {
              text: 'Tahara',
              _index: 'taxonomy',
              _type: 'geoname',
              _id: '58f7f25fdadb30820bb9cf0b',
              _score: 2,
              _source: {
                breadCrumbs: ['Japan', 'Aichi', 'Tahara-shi'],
                name: 'Tahara',
                location: {
                  lon: 137.26667,
                  lat: 34.66667,
                },
                href: '//new.sandbox.surfline.com/surf-reports-forecasts-cams/japan/aichi/tahara-shi/tahara/1851259',
              },
              contexts: {
                type: ['geoname'],
              },
            },
          ],
        },
      ],
    },
    status: 200,
  },
];
