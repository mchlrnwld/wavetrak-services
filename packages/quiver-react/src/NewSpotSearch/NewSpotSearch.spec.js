import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import NewSpotSearch from './NewSpotSearch';
import {
  renderSectionTitle,
  renderSuggestion,
} from './NewSpotSuggestion/NewSpotSuggestionRender';
import * as suggestionsAPI from './NewSpotSuggestion/NewSpotSuggestionModel';
import sampleResponse from './sampleData/sampleResponse.json';
import * as analytics from './analytics';

describe('NewSpotSearch', () => {
  let clock;
  const now = new Date();
  const props = deepFreeze({
    serviceConfig: {
      serviceUrl: 'http://services.testurl.com',
    },
  });

  const splitProps = deepFreeze({
    serviceConfig: {
      serviceUrl: 'http://services.testurl.com',
    },
  });

  beforeEach(() => {
    clock = sinon.useFakeTimers(now);
    sinon.stub(analytics, 'focusedSearch');
    sinon.stub(analytics, 'query');
    sinon.stub(analytics, 'clickedSearchResults');
    sinon.stub(suggestionsAPI, 'getSuggestedSpots');
  });

  afterEach(() => {
    clock.restore();
    analytics.focusedSearch.restore();
    analytics.query.restore();
    analytics.clickedSearchResults.restore();
    suggestionsAPI.getSuggestedSpots.restore();
  });

  it('should render an autosuggest container', () => {
    const defaultWrapper = mount(<NewSpotSearch {...props} />);
    expect(
      defaultWrapper.find('.quiver-search__container').exists()
    ).to.be.true();
  });

  /* eslint-disable no-underscore-dangle */
  it('should parse spot suggestions', () => {
    const suggestions = suggestionsAPI.getSectionSuggestions(sampleResponse[0]);
    expect(suggestions[0]._type).to.equal('spot');
    expect(renderSectionTitle(sampleResponse[0])).to.equal('Surf Spots');
  });

  /* eslint-disable no-underscore-dangle */
  it('should render spot suggestions', () => {
    const suggestions = suggestionsAPI.getSectionSuggestions(sampleResponse[0]);
    const renderedSuggestion = renderSuggestion('hunt')(suggestions[0]);
    expect(renderedSuggestion.type).to.equal('div');
  });

  /* eslint-disable no-underscore-dangle */
  it('should parse location suggestions', () => {
    const suggestions = suggestionsAPI.getSectionSuggestions(sampleResponse[1]);
    expect(suggestions[0]._type).to.equal('geoname');
    expect(renderSectionTitle(sampleResponse[1])).to.equal('Map Area');
  });

  it('should parse suggestion value from text', () => {
    const suggestions = suggestionsAPI.getSectionSuggestions(sampleResponse[0]);
    const [suggestion] = suggestions;
    expect(suggestionsAPI.getSuggestionValue(suggestion)).to.equal(
      suggestion.text
    );
  });

  it('should send correct analytics on search events', async () => {
    // const searchId = Math.floor(now / 1000);
    const suggestions = deepFreeze([
      {
        took: 10,
        hits: {
          hits: [
            {},
            {
              // Selected suggestion
              suggestion: {
                name: 'Selected suggestion',
                _index: 'spots',
                _type: 'spot',
                _source: {
                  href: 'http://testurl.com/selectedsuggestion',
                },
              },
              suggestionIndex: 0,
            },
          ],
        },
        suggest: {
          'spot-suggest': [{ options: [] }],
        },
      },
      {
        hits: {
          hits: [{}],
        },
        suggest: {
          'subregion-suggest': [{ options: [] }],
        },
      },
      {
        hits: {
          hits: [{}],
        },
        suggest: {
          'geoname-suggest': [{ options: [] }],
        },
      },
      {
        hits: {
          hits: [{}],
        },
        suggest: {
          'travel-suggest': [{ options: [] }],
        },
      },
    ]);
    suggestionsAPI.getSuggestedSpots.resolves(suggestions);

    const wrapper = shallow(<NewSpotSearch {...props} window={window} />);
    const instance = wrapper.instance();

    expect(wrapper.state('searchId')).to.be.null();

    instance.onSearchFieldFocus();
    const searchId = wrapper.state('searchId');
    expect(searchId).length.to.be(36);
    expect(analytics.focusedSearch).to.have.been.calledOnce();
    expect(analytics.focusedSearch).to.have.been.calledWithExactly(
      window,
      props.serviceConfig.serviceUrl,
      { searchId }
    );

    await instance.onSuggestionsFetchRequested({ value: 'foo' });
    expect(suggestionsAPI.getSuggestedSpots).to.have.been.calledOnce();
    expect(suggestionsAPI.getSuggestedSpots).to.have.been.calledWithExactly(
      props.serviceConfig,
      'foo',
      10
    );
    expect(analytics.query).to.have.been.calledOnce();
    expect(analytics.query).to.have.been.calledWithExactly(
      props.serviceConfig.serviceUrl,
      {
        searchId,
        value: 'foo',
        spotHits: 2,
        subregionHits: 1,
        geonameHits: 1,
        travelHits: 1,
      }
    );

    instance.onSuggestionSelected(null, suggestions[0].hits.hits[1]);
    expect(analytics.clickedSearchResults).to.have.been.calledOnce();
    expect(analytics.clickedSearchResults).to.have.been.calledWith(
      window,
      props.serviceConfig.serviceUrl,
      {
        searchId,
        searchResultRank: 0,
        destinationPage: 'http://testurl.com/selectedsuggestion',
        queryTerm: 'foo',
        resultName: 'Selected suggestion',
        searchLatency: 10,
        resultType: 'spot',
      }
    );
  });
  it('should send correct analytics on search events with split test', async () => {
    // const searchId = Math.floor(now / 1000);
    const suggestions = deepFreeze([
      {
        took: 10,
        hits: {
          hits: [
            {},
            {
              // Selected suggestion
              suggestion: {
                name: 'Selected suggestion',
                _index: 'spots',
                _type: 'spot',
                _source: {
                  href: 'http://testurl.com/selectedsuggestion',
                },
              },
              suggestionIndex: 0,
            },
          ],
        },
        suggest: {
          'spot-suggest': [{ options: [] }],
        },
      },
      {
        hits: {
          hits: [{}],
        },
        suggest: {
          'subregion-suggest': [{ options: [] }],
        },
      },
      {
        hits: {
          hits: [{}],
        },
        suggest: {
          'geoname-suggest': [{ options: [] }],
        },
      },
      {
        hits: {
          hits: [{}],
        },
        suggest: {
          'travel-suggest': [{ options: [] }],
        },
      },
    ]);
    suggestionsAPI.getSuggestedSpots.resolves(suggestions);

    const wrapper = shallow(<NewSpotSearch {...splitProps} window={window} />);
    const instance = wrapper.instance();

    expect(wrapper.state('searchId')).to.be.null();

    instance.onSearchFieldFocus();
    const searchId = wrapper.state('searchId');
    expect(searchId).length.to.be(36);
    expect(analytics.focusedSearch).to.have.been.calledOnce();
    expect(analytics.focusedSearch).to.have.been.calledWithExactly(
      window,
      props.serviceConfig.serviceUrl,
      { searchId }
    );

    await instance.onSuggestionsFetchRequested({ value: 'foo' });
    expect(suggestionsAPI.getSuggestedSpots).to.have.been.calledOnce();
    expect(suggestionsAPI.getSuggestedSpots).to.have.been.calledWithExactly(
      props.serviceConfig,
      'foo',
      10
    );
    expect(analytics.query).to.have.been.calledOnce();
    expect(analytics.query).to.have.been.calledWithExactly(
      props.serviceConfig.serviceUrl,
      {
        searchId,
        value: 'foo',
        spotHits: 2,
        subregionHits: 1,
        geonameHits: 1,
        travelHits: 1,
      }
    );

    instance.onSuggestionSelected(null, suggestions[0].hits.hits[1]);
    expect(analytics.clickedSearchResults).to.have.been.calledOnce();
    expect(analytics.clickedSearchResults).to.have.been.calledWith(
      window,
      props.serviceConfig.serviceUrl,
      {
        searchId,
        searchResultRank: 0,
        destinationPage: 'http://testurl.com/selectedsuggestion',
        queryTerm: 'foo',
        resultName: 'Selected suggestion',
        searchLatency: 10,
        resultType: 'spot',
      }
    );
  });
});
