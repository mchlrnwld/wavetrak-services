import withWindow from '../withWindow';
import NewSpotSearch from './NewSpotSearch';

export default withWindow(NewSpotSearch);
