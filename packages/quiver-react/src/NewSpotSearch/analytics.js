import { trackEvent } from '@surfline/web-common';

/* istanbul ignore next */
const shipAnalytics = (serviceUrl, log) =>
  fetch(`${serviceUrl}/search/analytics`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ log }),
  });

/* istanbul ignore next */
export const clickedSearchResults = (
  window,
  serviceUrl,
  {
    searchId,
    searchResultRank,
    destinationPage,
    queryTerm,
    resultName,
    searchLatency,
    resultType,
  },
  callback = () => {}
) => {
  const body = {
    searchId,
    location: 'navigation', // navigation | top of map | bottom of map
    searchResultRank,
    destinationPage,
    sourcePage: window.location.href,
    queryTerm,
    resultName,
    searchLatency,
    resultType,
  };

  if (serviceUrl) {
    shipAnalytics(serviceUrl, { event: 'Clicked Search Results', ...body });
    trackEvent('Clicked Search Results', body, null, callback);
  } else {
    callback();
  }
};

/* istanbul ignore next */
export const focusedSearch = (window, serviceUrl, { searchId }) => {
  shipAnalytics(serviceUrl, { event: 'Focused Search', searchId });

  trackEvent('Focused Search', {
    location: 'navigation', // navigation | top of map | bottom of map
  });
};

/* istanbul ignore next */
export const query = (
  serviceUrl,
  { searchId, value, spotHits, subregionHits, geonameHits }
) => {
  shipAnalytics(serviceUrl, {
    event: 'Query',
    searchId,
    query: value,
    hits: {
      spots: spotHits,
      subregions: subregionHits,
      geonames: geonameHits,
    },
  });
};
