import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Autosuggest from 'react-autosuggest';
import _cloneDeep from 'lodash/cloneDeep';
import serviceConfigurationPropType from '../PropTypes/serviceConfigurationPropType';
import windowPropType from '../PropTypes/windowPropType';
import {
  getSuggestionValue,
  getSectionSuggestions,
  getSuggestedSpots,
} from './NewSpotSuggestion/NewSpotSuggestionModel';
import {
  renderSectionTitle,
  renderSuggestion,
  getRenderSuggestionsContainer,
} from './NewSpotSuggestion/NewSpotSuggestionRender';
import NewSpotSearchInput from '../NewSpotSearchInput';
import { clickedSearchResults, focusedSearch, query } from './analytics';

const uuid = () =>
  ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (a) =>
    // eslint-disable-next-line no-bitwise
    (a ^ ((Math.random() * 16) >> (a / 4))).toString(16)
  );

class NewSpotSearch extends Component {
  constructor(props) {
    super(props);

    // Autosuggest is a controlled component.
    // This means that you need to provide an input value
    // and an onChange handler that updates this value (see below).
    // Suggestions also need to be provided to the Autosuggest,
    // and they are initially empty because the Autosuggest is closed.
    this.state = {
      value: '',
      suggestions: [],
      searchTerm: '',
      searchId: null,
      initialRender: true,
    };

    this.suggestionSelected = false;
  }

  componentDidMount() {
    // React has trouble reconciling aria labels between server/client
    // See: https://github.com/facebook/react/issues/5867
    // The suggested solution is to disable server side rendering for these components
    // eslint-disable-next-line react/no-did-mount-set-state
    this.setState({ initialRender: false });
  }

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue,
    });
  };

  onKeyDown = (event) => {
    const { window: windowProp } = this.props;
    const { value } = this.state;
    if (event.key === 'Enter' && !this.suggestionSelected) {
      event.preventDefault();
      windowProp.location.href = `/search/${value}`;
    }
  };

  onSearchFieldFocus = () => {
    const {
      window,
      serviceConfig: { serviceUrl },
    } = this.props;
    const searchId = uuid();
    this.setState({ searchId });
    focusedSearch(window, serviceUrl, { searchId });
  };

  // Autosuggest will call this function every time you need to update suggestions.
  onSuggestionsFetchRequested = async ({ value }) => {
    const { serviceConfig } = this.props;
    const { searchId } = this.state;
    let suggestions = await getSuggestedSpots(serviceConfig, value, 10);

    const typeResultsCount = {
      'spot-suggest': 10,
      'subregion-suggest': 3,
      'geoname-suggest': 3,
      'travel-suggest': 3,
    };
    suggestions = suggestions.map((suggestion) => {
      let newSuggestion = _cloneDeep(suggestion);
      const [key] = Object.keys(suggestion.suggest);
      newSuggestion = {
        ...newSuggestion,
        hits: {
          ...newSuggestion.hits,
          hits: newSuggestion.hits.hits.slice(0, typeResultsCount[key]),
        },
      };
      newSuggestion.suggest[key][0].options = newSuggestion.suggest[
        key
      ][0].options.slice(0, typeResultsCount[key]);
      return newSuggestion;
    });

    const spotHits = suggestions[0].hits.hits.length;
    const subregionHits = suggestions[1].hits.hits.length;
    const geonameHits = suggestions[2].hits.hits.length;
    const travelHits = suggestions[3].hits.hits.length;

    query(serviceConfig.serviceUrl, {
      searchId,
      value,
      spotHits,
      subregionHits,
      geonameHits,
      travelHits,
    });

    // return hits if available or else all suggestions
    const hits = suggestions.filter((sug) => sug.hits.hits.length > 0);
    this.setState({
      suggestions: hits && hits.length > 0 ? hits : suggestions,
      searchTerm: value,
    });
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  onSuggestionSelected = (event, value) => {
    const {
      suggestion: {
        name,
        _type,
        _source: { href },
      },
      suggestionIndex,
    } = value;
    const {
      window,
      serviceConfig: { serviceUrl },
    } = this.props;
    const { searchId, searchTerm, suggestions } = this.state;
    const took = suggestions[suggestionIndex]
      ? suggestions[suggestionIndex].took
      : null;
    this.suggestionSelected = true;

    clickedSearchResults(
      window,
      serviceUrl,
      {
        searchId,
        searchResultRank: suggestionIndex,
        destinationPage: href,
        queryTerm: searchTerm,
        resultName: name,
        searchLatency: took,
        resultType: _type,
      },
      () => this.navigateToResult(href)
    );
  };

  navigateToResult = (href) => {
    const { window: windowProp } = this.props;
    windowProp.location.assign(href);
  };

  render() {
    const { value, suggestions, searchTerm, initialRender } = this.state;
    if (initialRender) return null;

    const { window, placeholder } = this.props;
    const theme = {
      container: 'quiver-search__container',
      containerOpen: 'quiver-search__container--open',
      input: 'quiver-new-navigation-bar__search__wrapper__input',
      inputOpen: 'quiver-search__input--open',
      inputFocused: 'quiver-search__input--focused',
      suggestionsContainer: 'quiver-search__suggestions-container',
      suggestionsContainerOpen: 'quiver-search__suggestions-container--open',
      suggestionsList: 'quiver-search__suggestions-list',
      suggestion: 'quiver-search__suggestion',
      suggestionFirst: 'quiver-search__suggestion--first',
      suggestionHighlighted: 'quiver-search__suggestion--highlighted',
      sectionContainer: 'quiver-search__section-container',
      sectionContainerFirst: 'quiver-search__section-container--first',
      sectionTitle: 'quiver-search__section-title',
    };

    // Autosuggest will pass through all these props to the input element.
    const inputProps = {
      placeholder,
      value,
      onChange: this.onChange,
      onFocus: this.onSearchFieldFocus,
    };

    const conditionalProps = {};
    conditionalProps.renderSuggestionsContainer =
      getRenderSuggestionsContainer(window);
    inputProps.onKeyDown = this.onKeyDown;

    return (
      <Autosuggest
        id="new-spot-search"
        theme={theme}
        multiSection
        highlightFirstSuggestion={false}
        inputProps={inputProps}
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        onSuggestionSelected={this.onSuggestionSelected}
        getSectionSuggestions={getSectionSuggestions}
        getSuggestionValue={getSuggestionValue}
        renderInputComponent={(props) => (
          <NewSpotSearchInput inputProps={props} />
        )}
        renderSectionTitle={renderSectionTitle}
        renderSuggestion={renderSuggestion(searchTerm)}
        {...conditionalProps}
      />
    );
  }
}

NewSpotSearch.propTypes = {
  serviceConfig: serviceConfigurationPropType.isRequired,
  placeholder: PropTypes.string,
  window: windowPropType,
};

NewSpotSearch.defaultProps = {
  placeholder: 'Search city, county, or spot...',
  window: null,
};

export default NewSpotSearch;
