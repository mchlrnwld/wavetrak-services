const spotPagePath = (_id, slug) => `/surf-report/${slug}/${_id}`;

export default spotPagePath;
