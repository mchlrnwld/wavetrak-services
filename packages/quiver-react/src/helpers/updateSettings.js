import { Cookies } from 'react-cookie';

/* istanbul ignore next */
const updateSettings = async (settings, serviceConfig) => {
  const cookie = new Cookies();
  const accessToken = cookie.get('access_token');
  const url = `${serviceConfig.serviceUrl}/user/settings`;
  const response = await fetch(
    accessToken ? `${url}?accesstoken=${accessToken}` : url,
    {
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'PUT',
      body: JSON.stringify(settings),
    }
  );
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};

export default updateSettings;
