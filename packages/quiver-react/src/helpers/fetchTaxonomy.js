/* istanbul ignore next */
const fetchTaxonomy = async (id, maxDepth, serviceConfig) => {
  const url = `${serviceConfig.serviceUrl}/taxonomy?type=taxonomy&id=${id}&maxDepth=${maxDepth}`;
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};

export default fetchTaxonomy;
