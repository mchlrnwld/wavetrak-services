import { FEET, HAWAIIAN } from '../constants/units';

const getUnitType = (unitType) => {
  if (unitType == null) return null;
  if (unitType === HAWAIIAN) return FEET;
  return unitType; // no transformation needed
};

export default getUnitType;
