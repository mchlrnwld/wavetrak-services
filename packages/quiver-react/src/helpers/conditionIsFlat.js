const conditionIsFlat = (min, max, units) =>
  (!min && !max) ||
  (units &&
    (units.toUpperCase() === 'FT' || units.toUpperCase() === 'HI') &&
    min < 0.25 &&
    max <= 0.5) ||
  (units && units.toUpperCase() === 'M' && min < 0.15 && max <= 0.3);

export default conditionIsFlat;
