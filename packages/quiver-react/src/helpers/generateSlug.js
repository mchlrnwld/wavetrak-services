const generateSlug = (title) =>
  title
    ? title
        .toLowerCase()
        .replace(/[^a-zA-Z0-9]/g, '-')
        .replace(/(-){2,}/g, '-')
    : 'undefined';

export default generateSlug;
