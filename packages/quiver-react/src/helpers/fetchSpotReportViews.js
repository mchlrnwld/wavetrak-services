/* istanbul ignore next */
const fetchSpotReportViews = async (
  spotIds,
  serviceConfig,
  sevenRatings = false
) => {
  const url = `${serviceConfig.serviceUrl}/kbyg/spots/batch${
    sevenRatings ? '?sevenRatings=true' : ''
  }`;
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ spotIds }),
  });
  const body = await response.json();
  if (response.status === 200) return body;
  throw body;
};

export default fetchSpotReportViews;
