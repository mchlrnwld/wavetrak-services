import { conditionClassModifier } from '@surfline/web-common';

/**
 * Convert 10 ratings scale to 7 ratings
 * @param {string} condition
 */
const conditionMap = (condition) => {
  switch (condition) {
    case 'FLAT':
      return 'VERY_POOR';
    case 'VERY_GOOD':
      return 'GOOD';
    case 'GOOD_TO_EPIC':
      return 'GOOD';
    default:
      return condition;
  }
};

/**
 * @param {string} condition
 */
const applyConditionVariation = (condition) => conditionMap(condition);

/**
 * @param {string} condition
 */
export const getConditionClassWithVariation = (condition) =>
  conditionClassModifier(applyConditionVariation(condition));

export default applyConditionVariation;
