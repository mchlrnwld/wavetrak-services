const getSignInHref = (redirectUrl) => {
  const query = redirectUrl
    ? `?redirectUrl=${encodeURIComponent(redirectUrl)}`
    : '';
  return `/sign-in${query}`;
};

export default getSignInHref;
