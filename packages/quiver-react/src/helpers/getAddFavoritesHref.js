const getAddFavoritesHref = (redirectUrl, user = null) => {
  const query = redirectUrl
    ? `?redirectUrl=${encodeURIComponent(redirectUrl)}`
    : '';
  const href = user ? '/setup/favorite-surf-spots' : '/create-account';
  return `${href}${query}`;
};

export default getAddFavoritesHref;
