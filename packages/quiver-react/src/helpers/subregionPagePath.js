const subregionPagePath = (_id, slug) => `/surf-forecasts/${slug}/${_id}`;

export default subregionPagePath;
