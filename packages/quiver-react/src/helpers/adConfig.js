const adConfigMap = {
  favoritesHorizontal: {
    adUnit: '/1024858/Mobile_Favorites_320x50',
    adId: 'mobile-favorites',
    adSizes: [[320, 50]],
    adSizeDesktop: [320, 50],
    adSizeMobile: [320, 50],
    adViewType: 'FAVORITES',
  },
  sponsoredArticle: {
    adUnit: '/1024858/SL_Edit_Logo_White',
    adId: 'SL_Edit_Logo_White',
    adSizes: [[100, 45]],
    adSizeDesktop: [100, 45],
    adSizeMobile: [100, 45],
    adViewType: 'CONTENT_ARTICLE',
  },
};

const loadAdConfig = (adIdentifier, adTargets = [], entitlements = []) => {
  const adConfigResult = adConfigMap[adIdentifier];
  // TODO: Add additional usertype values based on user entitlements
  const usertype = entitlements.includes('sl_premium')
    ? 'PREMIUM'
    : 'ANONYMOUS';
  adConfigResult.adTargets = adTargets;
  adConfigResult.adTargets.push(['usertype', usertype]);
  return adConfigResult;
};

export default loadAdConfig;
