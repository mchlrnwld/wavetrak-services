import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import PremiumRibbonIcon from '../icons/PremiumRibbonIcon';

const getFeatureCardClasses = (small) =>
  classNames({
    'quiver-feature-card': true,
    'quiver-feature-card--small': small,
  });

const getFeatureCardTitleContainerClasses = (feedSuperheader) =>
  classNames({
    'quiver-feature-card__title__container': true,
    'quiver-feature-card__title__container--hasSuperheader': feedSuperheader,
  });

const FeatureCard = ({
  imageUrl,
  onClickLink,
  small,
  title,
  displayTitle,
  permalink,
  feedSuperheader,
  isPremium,
}) => (
  <a
    className={getFeatureCardClasses(small)}
    href={permalink}
    onClick={onClickLink}
  >
    {isPremium ? <PremiumRibbonIcon /> : null}
    <div
      className="quiver-feature-card--inner"
      style={{ backgroundImage: `url(${imageUrl})` }}
    />
    <div className={getFeatureCardTitleContainerClasses(feedSuperheader)}>
      {feedSuperheader && !small ? (
        <div className="quiver-feature-card__superheader">
          {feedSuperheader}
        </div>
      ) : null}
      <h2 className="quiver-feature-card__title">{displayTitle || title}</h2>
    </div>
  </a>
);

FeatureCard.propTypes = {
  imageUrl: PropTypes.string,
  onClickLink: PropTypes.func.isRequired,
  small: PropTypes.bool,
  title: PropTypes.string.isRequired,
  displayTitle: PropTypes.string,
  feedSuperheader: PropTypes.string,
  permalink: PropTypes.string.isRequired,
  isPremium: PropTypes.bool,
};

FeatureCard.defaultProps = {
  imageUrl: null,
  small: false,
  displayTitle: null,
  feedSuperheader: null,
  isPremium: false,
};

export default FeatureCard;
