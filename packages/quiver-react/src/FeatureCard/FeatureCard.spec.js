import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import FeatureCard from './FeatureCard';

describe('FeatureCard', () => {
  const props = {
    title: 'Fun SSW/S Southern Hemi Peaks in Orange County Today',
    permalink: 'https://www.surfline.com',
    imageUrl: 'https://www.surfline.com/image.jpg',
  };

  it('should render permalink and title', () => {
    const wrapper = shallow(<FeatureCard {...props} />);

    const permalink = wrapper.find('a');
    expect(permalink).to.have.length(1);

    const title = wrapper.find('h2');
    expect(title).to.have.length(1);
    expect(title.text()).to.equal(props.title);
  });

  it('should render superheader if prop is available', () => {
    const feedSuperheader = 'Presented By Somebody';
    const defaultWrapper = shallow(<FeatureCard {...props} />);
    const adWrapper = shallow(
      <FeatureCard {...props} feedSuperheader={feedSuperheader} />
    );

    const defaultSuperheader = defaultWrapper.find(
      '.quiver-feature-card__superheader'
    );
    expect(defaultSuperheader).to.have.length(0);

    const hasSuperheader = adWrapper.find('.quiver-feature-card__superheader');
    expect(hasSuperheader).to.have.length(1);
  });

  it('should update classes if small prop is truthy', () => {
    const defaultWrapper = shallow(<FeatureCard {...props} />);
    const smallWrapper = shallow(<FeatureCard {...props} small />);

    const defaultClass = defaultWrapper.find('.quiver-feature-card--small');
    expect(defaultClass).to.have.length(0);

    const smallClass = smallWrapper.find('.quiver-feature-card--small');
    expect(smallClass).to.have.length(1);
  });

  it('should add PremiumRibbonIcon when isPremium is true', () => {
    const wrapper = mount(<FeatureCard {...props} isPremium />);
    expect(wrapper.html()).to.contain('quiver-premium-ribbon-icon');
  });
});
