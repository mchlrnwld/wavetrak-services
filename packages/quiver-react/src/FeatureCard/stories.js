/* eslint-disable max-len */
import React from 'react';
import FeatureCard from './FeatureCard';

const props = {
  imageUrl:
    'https://d14fqx6aetz9ka.cloudfront.net/wp-content/uploads/2018/11/09095449/P1A8027_WestAus_2018-1200x799.jpg',
  permalink: 'https://www.surfline.com',
  title: 'Feature Card Title Goes Here',
};

export default {
  title: 'FeatureCard',
};

export const Default = () => (
  <div style={{ width: '750px' }}>
    <FeatureCard {...props} />
  </div>
);

Default.story = {
  name: 'default',
};

export const WithSuperheader = () => (
  <div style={{ width: '760px' }}>
    <FeatureCard
      {...props}
      feedSuperheader="Presented by Something"
      style={{ width: '760px' }}
    />
  </div>
);

WithSuperheader.story = {
  name: 'with superheader',
};

export const WithAltTitle = () => (
  <div style={{ width: '760px' }}>
    <FeatureCard
      {...props}
      adId="12345"
      style={{ width: '760px' }}
      displayTitle="Alternate Title"
    />
  </div>
);

WithAltTitle.story = {
  name: 'with alt title',
};

export const WithSuperheaderAndPremium = () => (
  <div style={{ width: '760px' }}>
    <FeatureCard
      {...props}
      feedSuperheader="Presented by Something"
      style={{ width: '760px' }}
      isPremium
    />
  </div>
);

WithSuperheaderAndPremium.story = {
  name: 'with superheader and premium',
};

export const Small = () => (
  <div style={{ width: '380px' }}>
    <FeatureCard {...props} small />
  </div>
);

Small.story = {
  name: 'small',
};

export const SmallAndPremium = () => (
  <div style={{ width: '380px' }}>
    <FeatureCard {...props} small isPremium />
  </div>
);

SmallAndPremium.story = {
  name: 'small and premium',
};
