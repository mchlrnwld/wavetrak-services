import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import FeedArticlesList from './FeedArticlesList';
import regionalArticles from './fixtures/regional.json';

describe('FeedArticlesList', () => {
  const props = {
    articles: {
      regional: regionalArticles,
    },
    doFetchForecastArticles: () => {},
    subregionId: '58581a836630e24c44878fd6',
    subregionName: 'North Orange County',
    location: 'homepage',
  };

  it('should render correct heading class', () => {
    const homepageWrapper = shallow(<FeedArticlesList {...props} />);
    const feedWrapper = shallow(
      <FeedArticlesList {...props} location="feed post page" />
    );
    const homeDefault = homepageWrapper.find(
      '.quiver-feed-articles-list__heading'
    );
    const homeArticle = homepageWrapper.find(
      '.quiver-feed-articles-list__heading--article'
    );
    const feedDefault = feedWrapper.find('.quiver-feed-articles-list__heading');
    const feedArticle = feedWrapper.find(
      '.quiver-feed-articles-list__heading--article'
    );
    expect(homeDefault).to.have.length(1);
    expect(homeArticle).to.have.length(0);
    expect(homeDefault.text()).to.equal('Local Headlines');
    expect(feedDefault).to.have.length(1);
    expect(feedArticle).to.have.length(1);
    expect(feedArticle.text()).to.equal('North Orange County Headlines');
  });

  it('should display load more if more regional articles', () => {
    const loadMoreArticles = {
      pageCount: 0,
      regional: [...regionalArticles, ...regionalArticles, ...regionalArticles],
    };
    const pastPageCount = {
      pageCount: 2,
      regional: [...regionalArticles, ...regionalArticles, ...regionalArticles],
    };
    const defaultWrapper = shallow(<FeedArticlesList {...props} />);
    const loadMoreWrapper = shallow(
      <FeedArticlesList {...props} articles={loadMoreArticles} />
    );
    const pastPageCountWrapper = shallow(
      <FeedArticlesList {...props} articles={pastPageCount} />
    );
    const noMore = defaultWrapper.find(
      '.quiver-feed-articles-list__list__load-more'
    );
    expect(noMore).to.have.length(0);
    const loadMore = loadMoreWrapper.find(
      '.quiver-feed-articles-list__list__load-more'
    );
    expect(loadMore).to.have.length(1);
    const pastCount = pastPageCountWrapper.find(
      '.quiver-feed-articles-list__list__load-more'
    );
    expect(pastCount).to.have.length(0);
  });
});
