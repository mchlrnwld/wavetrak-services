import PropTypes from 'prop-types';

export default PropTypes.shape({
  id: PropTypes.string.isRequired,
  contentType: PropTypes.string.isRequired,
  createdAt: PropTypes.number.isRequired,
  updatedAt: PropTypes.number.isRequired,
  promoted: PropTypes.arrayOf(PropTypes.string),
  premium: PropTypes.bool,
  content: PropTypes.shape({
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string,
    body: PropTypes.string,
  }),
  permalink: PropTypes.string,
  media: PropTypes.shape({
    type: PropTypes.string.isRequired,
    feed1x: PropTypes.string.isRequired,
    feed2x: PropTypes.string.isRequired,
    promobox1x: PropTypes.string,
    promobox2x: PropTypes.string,
  }),
  author: PropTypes.shape({
    iconUrl: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }),
  tags: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired,
    })
  ),
});
