/* eslint-disable max-len */
import React from 'react';
import FeedArticlesList from './FeedArticlesList';
import regionalArticles from './fixtures/regional.json';

const props = {
  articles: {
    regional: regionalArticles,
  },
  doFetchForecastArticles: () => {},
  subregionId: '58581a836630e24c44878fd6',
  subregionName: 'North Orange County',
  location: 'feed post page',
};

export default {
  title: 'FeedArticlesList',
};

export const Default = () => (
  <div style={{ width: '350px' }}>
    <FeedArticlesList {...props} />
  </div>
);

Default.story = {
  name: 'default',
};
