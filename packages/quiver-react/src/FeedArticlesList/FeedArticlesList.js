import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';
import { formatArticleDate, trackEvent } from '@surfline/web-common';
import articlePropType from './propTypes/article';
import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

const handleClickedLoadMore = (
  subregionId,
  subregionName,
  doFetchForecastArticles,
  location
) => {
  doFetchForecastArticles(subregionId);
  trackEvent('Clicked Load More', {
    pageName: location,
    subregionId,
    subregionName,
    locationCategory: 'feed post local headlines',
  });
};

const getFeedArticlesListHeadingClasses = (location) =>
  classnames({
    'quiver-feed-articles-list__heading': true,
    'quiver-feed-articles-list__heading--article':
      location === 'feed post page',
  });

const FeedArticlesList = ({
  articles,
  doFetchForecastArticles,
  subregionId,
  subregionName,
  location,
}) => (
  <div className="quiver-feed-articles-list">
    <h5 className={getFeedArticlesListHeadingClasses(location)}>
      {location === 'homepage'
        ? 'Local Headlines'
        : `${subregionName} Headlines`}
    </h5>
    <ul className="quiver-feed-articles-list__list">
      {articles.regional.map((article) => (
        <li className="quiver-feed-articles-list__list__item" key={article.id}>
          <a
            aria-label="article"
            dangerouslySetInnerHTML={{ __html: article.content.title }}
            className="quiver-feed-articles-list__list__item__link"
            href={article.permalink}
            onClick={() =>
              trackEvent(
                `Clicked ${
                  location === 'feed post page' ? 'Article Page' : 'Home Feed'
                } Link`,
                {
                  destinationUrl: article.permalink,
                  mediaType:
                    article.media && article.media.type
                      ? article.media.type
                      : null,
                  contentId: article.id,
                  title: article.content.title,
                  contentType: article.contentType,
                  premiumArticle: article.premium,
                  tags: article.tags,
                  locationCategory: `${location} local headlines`,
                }
              )
            }
          />
          <span className="quiver-feed-articles-list__list__item__date">
            {formatArticleDate(article.updatedAt)}
          </span>
        </li>
      ))}
    </ul>
    {articles.pageCount <= 1 && articles.regional.length >= 6 ? (
      <a
        className="quiver-feed-articles-list__list__load-more"
        {...createAccessibleOnClick(
          () =>
            handleClickedLoadMore(
              subregionId,
              subregionName,
              doFetchForecastArticles,
              location
            ),
          BTN
        )}
      >
        Load More
      </a>
    ) : null}
  </div>
);

FeedArticlesList.propTypes = {
  articles: PropTypes.shape({
    pageCount: PropTypes.number,
    regional: PropTypes.arrayOf(articlePropType),
  }),
  doFetchForecastArticles: PropTypes.func.isRequired,
  subregionId: PropTypes.string.isRequired,
  subregionName: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
};

FeedArticlesList.defaultProps = {
  articles: [],
};

export default FeedArticlesList;
