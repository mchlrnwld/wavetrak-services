import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import Input from '../Input';
import Button from '../Button';
import Alert from '../Alert';
import { MIN_PASSWORD_LENGTH } from '../utils/constants';

const CreateAccountForm = ({
  onSubmit,
  loading,
  error,
  receivePromotionsLabel,
  buttonLabel,
}) => {
  const [password, setPassword] = useState(null);
  const [email, setEmail] = useState(null);
  const [firstName, setFirstName] = useState(null);
  const [lastName, setLastName] = useState(null);
  const [staySignedIn, setStaySignedIn] = useState(true);
  const [receivePromotions, setReceivePromotions] = useState(true);
  const [firstNameErr, setFirstNameErr] = useState(null);
  const [lastNameErr, setLastNameErr] = useState(null);
  const [emailErr, setEmailErr] = useState(null);
  const [passwordErr, setPasswordErr] = useState(null);

  const isValid =
    !(firstNameErr || lastNameErr || emailErr || passwordErr) &&
    firstName?.length &&
    lastName?.length &&
    email?.length &&
    password?.length;

  const { register, handleSubmit, errors } = useForm();
  return (
    <div className="create-form">
      {error ? (
        <Alert type="error" style={{ marginVertical: '12px' }}>
          {error}
        </Alert>
      ) : null}
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="create-form__names">
          <Input
            value={firstName}
            label="FIRST NAME"
            id="firstName"
            parentWidth
            onError={setFirstNameErr}
            error={!!errors.firstName}
            message={errors?.firstName?.message}
            required
            input={{
              name: 'firstName',
              onChange: (event) => setFirstName(event.target.value),
              ref: register({ required: true }),
              required: true,
            }}
          />
          <Input
            value={lastName}
            label="LAST NAME"
            id="lastName"
            parentWidth
            onError={setLastNameErr}
            error={!!errors.lastName}
            message={errors?.lastName?.message}
            required
            input={{
              name: 'lastName',
              onChange: (event) => setLastName(event.target.value),
              ref: register({ required: true }),
              required: true,
            }}
          />
        </div>
        <Input
          value={email}
          label="EMAIL"
          id="email"
          parentWidth
          onError={setEmailErr}
          error={!!errors.email}
          message={errors?.email?.message}
          email
          required
          input={{
            name: 'email',
            onChange: (event) => setEmail(event.target.value),
            ref: register({
              required: true,
            }),
            required: true,
          }}
        />
        <Input
          value={password}
          label={`PASSWORD (Min ${MIN_PASSWORD_LENGTH} Characters)`}
          id="password"
          type="password"
          parentWidth
          onError={setPasswordErr}
          error={!!errors.password}
          message={errors?.password?.message}
          password
          input={{
            name: 'password',
            onChange: (event) => setPassword(event.target.value),
            ref: register({ required: true }),
            required: true,
          }}
        />
        <Button
          disabled={!isValid}
          style={{ width: '100%' }}
          loading={loading}
          button={{ type: 'submit' }}
        >
          {buttonLabel}
        </Button>
        <div>
          <div className="create-form__checkbox">
            <input
              type="checkbox"
              id="staySignedIn"
              name="staySignedIn"
              ref={register}
              value={staySignedIn}
              defaultChecked={staySignedIn}
              onClick={() => setStaySignedIn(!staySignedIn)}
            />
            <span htmlFor="staySignedIn">Keep me signed in</span>
          </div>
          <div className="create-form__checkbox">
            <input
              type="checkbox"
              id="receivePromotions"
              name="receivePromotions"
              ref={register}
              value={receivePromotions}
              defaultChecked={receivePromotions}
              onClick={() => setReceivePromotions(!receivePromotions)}
            />
            <span>{receivePromotionsLabel}</span>
          </div>
        </div>
      </form>
    </div>
  );
};

CreateAccountForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  error: PropTypes.string,
  receivePromotionsLabel: PropTypes.string,
  buttonLabel: PropTypes.string,
};

CreateAccountForm.defaultProps = {
  loading: false,
  error: null,
  receivePromotionsLabel: null,
  buttonLabel: 'CREATE ACCOUNT',
};

export default CreateAccountForm;
