/* eslint-disable no-unused-expressions */
import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import CreateAccountForm from './CreateAccountForm';

describe('CreateAccountForm', () => {
  it('should render a create form with defaulted props', () => {
    const onSubmit = sinon.spy();
    const wrapper = shallow(
      <CreateAccountForm
        onSubmit={onSubmit}
        loading={false}
        error={null}
        receivePromotionsLabel="Receive promotions"
        buttonLabel="CREATE ACCOUNT"
      />
    );

    expect(wrapper.find('input').first().props().id).to.equal('staySignedIn');

    expect(wrapper.find('input').at(1).props().id).to.equal(
      'receivePromotions'
    );

    expect(wrapper.find('.create-form__names').find('quiver-input')).to.exist;
    expect(wrapper.prop('loading')).to.be.false;
    expect(wrapper.prop('error')).to.not.be.null;
    expect(wrapper.find('form').prop('onSubmit')).to.exist;
    expect(wrapper.find('Button').html()).to.contain('CREATE ACCOUNT');
  });
});
