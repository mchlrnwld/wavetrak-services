import React from 'react';
import CreateAccountForm from './index';

export default {
  title: 'CreateAccountForm',
};

export const Default = () => (
  <CreateAccountForm
    onSubmit={(values) => console.log(values)}
    loading={false}
    error={false}
    receivePromotionsLabel="Promotions label here"
    buttonLabel="CREATE ACCOUNT"
  />
);

Default.story = {
  name: 'default',
};
