import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ReadingDescription from '../ReadingDescription';
import WindSummary from './WindSummary';

describe('components / WindSummary', () => {
  it('renders N if degrees between 348.75 and 11.25', () => {
    const wrapper = shallow(<WindSummary direction={5.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('N (5°)');
  });

  it('renders NNE if degrees between 11.25 and 33.75', () => {
    const wrapper = shallow(<WindSummary direction={15.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('NNE (15°)');
  });

  it('renders NE if degrees between 33.75 and 56.25', () => {
    const wrapper = shallow(<WindSummary direction={35.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('NE (35°)');
  });

  it('renders ENE if degrees between 56.25 and 78.75', () => {
    const wrapper = shallow(<WindSummary direction={65.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('ENE (65°)');
  });

  it('renders E if degrees between 78.75 and 101.25', () => {
    const wrapper = shallow(<WindSummary direction={85.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('E (85°)');
  });

  it('renders ESE if degrees between 101.25 and 123.75', () => {
    const wrapper = shallow(<WindSummary direction={105.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('ESE (105°)');
  });

  it('renders SE if degrees between 123.75 and 146.25', () => {
    const wrapper = shallow(<WindSummary direction={125.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('SE (125°)');
  });

  it('renders SSE if degrees between 146.25 and 168.75', () => {
    const wrapper = shallow(<WindSummary direction={155.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('SSE (155°)');
  });

  it('renders S if degrees between 168.75 and 191.25', () => {
    const wrapper = shallow(<WindSummary direction={175.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('S (175°)');
  });

  it('renders SSW if degrees between 191.25 and 213.75', () => {
    const wrapper = shallow(<WindSummary direction={195.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('SSW (195°)');
  });

  it('renders SW if degrees between 213.75 and 236.25', () => {
    const wrapper = shallow(<WindSummary direction={215.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('SW (215°)');
  });

  it('renders WSW if degrees between 236.25 and 258.75', () => {
    const wrapper = shallow(<WindSummary direction={245.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('WSW (245°)');
  });

  it('renders W if degrees between 258.75 and 281.25', () => {
    const wrapper = shallow(<WindSummary direction={265.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('W (265°)');
  });

  it('renders WNW if degrees between 281.25 and 303.75', () => {
    const wrapper = shallow(<WindSummary direction={285.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('WNW (285°)');
  });

  it('renders NW if degrees between 303.75 and 326.25', () => {
    const wrapper = shallow(<WindSummary direction={305.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('NW (305°)');
  });

  it('renders NNW if degrees between 326.25 and 348.75', () => {
    const wrapper = shallow(<WindSummary direction={335.25} />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.equal('NNW (335°)');
  });
});
