import React from 'react';
import WindSummary from './WindSummary';

export default {
  title: 'WindSummary',
};

export const WindSummary320 = () => <WindSummary direction={320} />;
export const WindSummary200 = () => <WindSummary direction={200} />;
export const WindSummary100 = () => <WindSummary direction={100} />;
export const WindSummary10 = () => <WindSummary direction={10} />;
export const WindSummary120 = () => <WindSummary direction={120} />;
