import PropTypes from 'prop-types';
import React from 'react';
import ReadingDescription from '../ReadingDescription';

// TODO: See if this can be replaced by `mapDegreesCardinal`
const cardinalDirection = (degrees) => {
  const wrappedDegrees = degrees % 360;

  switch (true) {
    case wrappedDegrees >= 11.25 && wrappedDegrees < 33.75:
      return 'NNE';
    case wrappedDegrees >= 33.75 && wrappedDegrees < 56.25:
      return 'NE';
    case wrappedDegrees >= 56.25 && wrappedDegrees < 78.75:
      return 'ENE';
    case wrappedDegrees >= 78.75 && wrappedDegrees < 101.25:
      return 'E';
    case wrappedDegrees >= 101.25 && wrappedDegrees < 123.75:
      return 'ESE';
    case wrappedDegrees >= 123.75 && wrappedDegrees < 146.25:
      return 'SE';
    case wrappedDegrees >= 146.25 && wrappedDegrees < 168.75:
      return 'SSE';
    case wrappedDegrees >= 168.75 && wrappedDegrees < 191.25:
      return 'S';
    case wrappedDegrees >= 191.25 && wrappedDegrees < 213.75:
      return 'SSW';
    case wrappedDegrees >= 213.75 && wrappedDegrees < 236.25:
      return 'SW';
    case wrappedDegrees >= 236.25 && wrappedDegrees < 258.75:
      return 'WSW';
    case wrappedDegrees >= 258.75 && wrappedDegrees < 281.25:
      return 'W';
    case wrappedDegrees >= 281.25 && wrappedDegrees < 303.75:
      return 'WNW';
    case wrappedDegrees >= 303.75 && wrappedDegrees < 326.25:
      return 'NW';
    case wrappedDegrees >= 326.25 && wrappedDegrees < 348.75:
      return 'NNW';
    default:
      return 'N';
  }
};

const WindSummary = ({ direction }) => (
  <ReadingDescription>{`${cardinalDirection(direction)} (${Math.round(
    direction
  )}°)`}</ReadingDescription>
);

WindSummary.propTypes = {
  direction: PropTypes.number.isRequired,
};

export default WindSummary;
