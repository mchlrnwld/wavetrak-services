import React from 'react';
import WaveSummary from './WaveSummary';

export default {
  title: 'WaveSummary',
};

export const Default = () => (
  <WaveSummary
    humanRelation="Waist to shoulder high"
    units="FT"
    occasional={5}
  />
);
