import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ReadingDescription from '../ReadingDescription';
import WaveSummary from './WaveSummary';

describe('components / WaveSummary', () => {
  it('renders reading description with capitalized human relation', () => {
    const wrapper = shallow(
      <WaveSummary
        humanRelation="ankle to knee high"
        occasional="0"
        units="FT"
      />
    );
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.prop('children')[0]).to.equal(
      'Ankle to knee high'
    );
  });

  it('renders reading description with occasional data when gt 0', () => {
    const wrapper = shallow(
      <WaveSummary
        humanRelation="ankle to knee high"
        occasional="5"
        units="FT"
      />
    );
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.prop('children')[1]).to.equal(' occ. 5 ft');
  });

  it('renders empty reading description without human relation or occasional', () => {
    const wrapper = shallow(<WaveSummary />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.prop('children')[0]).to.be.null();
    expect(readingDescription.prop('children')[1]).to.be.null();
  });
});
