import PropTypes from 'prop-types';
import React from 'react';
import {
  occasionalUnits,
  capitalizeFirstLetter,
  trimHumanRelation,
} from '@surfline/web-common';
import getUnitType from '../helpers/getUnitType';

import ReadingDescription from '../ReadingDescription';

const WaveSummary = ({ humanRelation, occasional, units }) => (
  <div className="quiver-wave-summary">
    <ReadingDescription>
      {capitalizeFirstLetter(trimHumanRelation(humanRelation))}
      {occasionalUnits(occasional, getUnitType(units))}
    </ReadingDescription>
  </div>
);

WaveSummary.propTypes = {
  humanRelation: PropTypes.string,
  occasional: PropTypes.number,
  units: PropTypes.string,
};

WaveSummary.defaultProps = {
  humanRelation: null,
  occasional: 0,
  units: null,
};

export default WaveSummary;
