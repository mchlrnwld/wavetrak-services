import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import NotificationDialog from './NotificationDialog';

describe('NotificationDialog', () => {
  it('should render Notification Dialog into DOM', () => {
    const wrapper = shallow(<NotificationDialog />);
    const container = wrapper.find('.quiver-notification-dialog');
    expect(container).to.have.length(1);
  });
  it('should render Notification Dialog into DOM with custom BG color', () => {
    const wrapper = shallow(<NotificationDialog bgColor="#011D38" />);
    const container = wrapper.find('.quiver-notification-dialog');
    expect(container.prop('style')).to.have.property('background', '#011D38');
    expect(container).to.have.length(1);
  });
  it('should render Notification Dialog into DOM with pill size', () => {
    const wrapper = shallow(
      <NotificationDialog bgColor="#011D38" expanded={false} />
    );
    const container = wrapper.find('.quiver-notification-dialog--small');
    expect(container).to.have.length(1);

    const content = wrapper.find(
      '.quiver-notification-dialog__children--hidden'
    );
    expect(content).to.have.length(1);
  });
});
