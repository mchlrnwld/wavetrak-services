import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { color } from '@surfline/quiver-themes';

const getClassName = (expanded) =>
  classNames({
    'quiver-notification-dialog': expanded,
    'quiver-notification-dialog--small': !expanded,
  });

const getChildrenClassName = (expanded) =>
  classNames({
    'quiver-notification-dialog__children--expanded': expanded,
    'quiver-notification-dialog__children--hidden': !expanded,
  });

const NotificationDialog = ({ title, children, bgColor, expanded }) => (
  <div className={getClassName(expanded)} style={{ background: bgColor }}>
    {title}
    <div className={getChildrenClassName(expanded)}>
      {expanded ? children : null}
    </div>
  </div>
);

NotificationDialog.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.node.isRequired,
  bgColor: PropTypes.string,
  expanded: PropTypes.bool,
};
NotificationDialog.defaultProps = {
  bgColor: color('white'),
  expanded: true,
};

export default NotificationDialog;
