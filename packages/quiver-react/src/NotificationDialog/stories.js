import React from 'react';
import NotificationDialog from './index';

const parentContainerStyles = {
  width: '530px',
  height: '150px',
};
const ChildComponent = () => (
  <div
    style={{
      width: 'auto',
      height: 'auto',
      padding: '15px',
      background: 'white',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      margin: '15px',
    }}
  >
    Sample Content
  </div>
);

export default {
  title: 'NotificationDialog',
};

export const Default = () => (
  <div style={parentContainerStyles}>
    <NotificationDialog>
      <ChildComponent />
    </NotificationDialog>
  </div>
);

Default.story = {
  name: 'default',
};

export const CustomBackground = () => (
  <div style={parentContainerStyles}>
    <NotificationDialog bgColor="#011D38">
      <ChildComponent />
    </NotificationDialog>
  </div>
);
