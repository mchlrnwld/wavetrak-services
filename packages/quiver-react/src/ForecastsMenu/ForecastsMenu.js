import React from 'react';
import PropTypes from 'prop-types';

import navigationSettingsPropType from '../PropTypes/navigationSettingsPropType';
import recentlyVisitedPropType from '../PropTypes/recentlyVisitedPropType';
import serviceConfigurationPropType from '../PropTypes/serviceConfigurationPropType';
import RecentlyVisited from '../RecentlyVisited';
import TaxonomyNavigator from '../TaxonomyNavigator';
import { forecastsLinks } from '../NewNavigationBar/defaultLinks';

const HIERARCHY = ['🌍', 'Continent', 'Country', 'Region'];

const ForecastsMenu = ({
  loggedIn,
  navigationSettings,
  recentlyVisited,
  serviceConfig,
  menuState,
  setMenuState,
  onClickLink,
  scrollMenuToTop,
  signInUrl,
}) => (
  <div className="quiver-forecast-menu">
    <RecentlyVisited
      listType="subregions"
      loggedIn={loggedIn}
      items={recentlyVisited}
      serviceConfig={serviceConfig}
      staticLinks={forecastsLinks}
      signInUrl={signInUrl}
      onClickLink={(properties, closeMobileMenu) =>
        onClickLink(
          {
            name: 'Clicked Main Nav',
            properties: {
              ...properties,
              tabName: 'Forecasts',
            },
          },
          closeMobileMenu
        )
      }
    />
    <TaxonomyNavigator
      hierarchy={HIERARCHY}
      loggedIn={loggedIn}
      navigationSettings={navigationSettings}
      serviceConfig={serviceConfig}
      savedLocationType="forecastsTaxonomyLocation"
      menuState={menuState}
      setMenuState={setMenuState}
      taxonomyType="subregion"
      scrollMenuToTop={scrollMenuToTop}
      onClickLink={(properties, closeMobileMenu) =>
        onClickLink(
          {
            name: 'Clicked Main Nav',
            properties: {
              ...properties,
              tabName: 'Forecasts',
            },
          },
          closeMobileMenu
        )
      }
    />
  </div>
);

ForecastsMenu.propTypes = {
  loggedIn: PropTypes.bool,
  navigationSettings: navigationSettingsPropType.isRequired,
  recentlyVisited: PropTypes.arrayOf(recentlyVisitedPropType).isRequired,
  serviceConfig: serviceConfigurationPropType.isRequired,
  setMenuState: PropTypes.func.isRequired,
  menuState: PropTypes.shape().isRequired,
  scrollMenuToTop: PropTypes.func,
  signInUrl: PropTypes.string.isRequired,
  onClickLink: PropTypes.func,
};

ForecastsMenu.defaultProps = {
  loggedIn: false,
  scrollMenuToTop: null,
  onClickLink: null,
};

export default ForecastsMenu;
