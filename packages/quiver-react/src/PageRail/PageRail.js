import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

/**
 * @param {string} side
 * @param {boolean} isOpen
 * @param {string} className
 */
const getClassNames = (side, isOpen, className) =>
  classNames({
    'quiver-page-rail': true,
    [`quiver-page-rail--${side}`]: true,
    [`quiver-page-rail--${side}--open`]: isOpen,
    [`quiver-page-rail--${side}--closed`]: !isOpen,
    [className]: !!className,
  });

/**
 * @typedef {Object} Props
 * @property {'left' | 'right'} [side='left'] - The side of the page rail.
 * @property {boolean} [open=false] - Whether the page rail is open.
 * @property {string} [className] - Additional class names.
 */

/**  @type {React.FC<Props>} */
const PageRail = ({ children, side, open, className }) => (
  <div className={getClassNames(side, open, className)}>{children}</div>
);

PageRail.propTypes = {
  children: PropTypes.node,
  side: PropTypes.string.isRequired,
  open: PropTypes.bool,
  className: PropTypes.string,
};

PageRail.defaultProps = {
  children: null,
  open: true,
  className: '',
};

export default PageRail;
