import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import PageRail from './PageRail';

describe('PageRail', () => {
  it('should render with appropriate class name', () => {
    const wrapper = shallow(<PageRail side="right" />);
    expect(wrapper.prop('className')).to.equal(
      'quiver-page-rail quiver-page-rail--right quiver-page-rail--right--open'
    );
  });

  it('should add any passed className', () => {
    const wrapper = shallow(<PageRail side="right" className="test-class" />);
    expect(wrapper.prop('className')).to.equal(
      'quiver-page-rail quiver-page-rail--right quiver-page-rail--right--open test-class'
    );
  });
});
