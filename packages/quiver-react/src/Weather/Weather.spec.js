import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import Weather from './Weather';

const props = {
  weather: {
    temperature: 71,
    condition: 'NIGHT_CLEAR_NO_RAIN',
  },
  weatherIconPath: 'http://weathericoncdn.surfline.com',
  units: 'F',
  spotName: 'HB Pier Southside',
};

describe('Weather', () => {
  it('should render the Weather', () => {
    const wrapper = mount(<Weather {...props} />);
    expect(wrapper).to.have.length(1);
    expect(wrapper.text()).to.contain('71 ºF');
    expect(wrapper.find('img').prop('src')).to.be.equal(
      `${props.weatherIconPath}/NIGHT_CLEAR_NO_RAIN.svg`
    );
    expect(wrapper.find('img').prop('alt')).to.be.equal(
      `${props.spotName} Wind & Weather`
    );
  });
});
