import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Weather = ({ spotName, weatherIconPath, weather, units, className }) => {
  const weatherClass = classNames({
    'quiver-weather-stats': true,
    [className]: !!className,
  });

  return (
    <div className={weatherClass}>
      <h4>Weather</h4>
      <div>
        <img
          src={`${weatherIconPath}/${weather.condition}.svg`}
          alt={`${spotName} Wind & Weather`}
        />
        {weather.temperature} <span>º{units}</span>
      </div>
    </div>
  );
};

Weather.propTypes = {
  weatherIconPath: PropTypes.string.isRequired,
  weather: PropTypes.shape({
    condition: PropTypes.string,
    temperature: PropTypes.number,
  }),
  units: PropTypes.string,
  spotName: PropTypes.string.isRequired,
  className: PropTypes.string,
};

Weather.defaultProps = {
  weather: {
    condition: 'CLEAR_NO_RAIN',
    temperature: 0,
  },
  units: 'f',
  className: null,
};

export default Weather;
