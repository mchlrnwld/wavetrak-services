import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import createAccessibleOnClick from '../utils/createAccessibleOnClick';

const buttonClassName = (isActive) =>
  classNames({
    'quiver-pill-button': true,
    'quiver-pill-button--active': isActive,
  });

/**
 * @description Pill Button component that can be used standalone, or inside
 * a Parent component with sibling <PillButton> components.
 */
const PillButton = ({ children, isActive, onClick }) => (
  <button
    type="button"
    className={buttonClassName(isActive)}
    tabIndex={0}
    disabled={isActive}
    {...createAccessibleOnClick(onClick)}
  >
    {children}
  </button>
);

PillButton.propTypes = {
  isActive: PropTypes.bool,
  children: PropTypes.node,
  onClick: PropTypes.func.isRequired,
};

PillButton.defaultProps = {
  children: null,
  isActive: false,
};

export default PillButton;
