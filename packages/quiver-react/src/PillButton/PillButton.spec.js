import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import PillButton from './PillButton';

describe('PillButton', () => {
  it('should render the pill button', () => {
    const wrapper = mount(
      <PillButton onClick={() => {}}>
        <div className="child" />
      </PillButton>
    );

    expect(wrapper).to.have.length(1);
    expect(wrapper.find('.child')).to.have.length(1);

    wrapper.setProps({ isActive: true });
    wrapper.update();

    expect(wrapper.find('button').prop('disabled')).to.be.true();
  });
});
