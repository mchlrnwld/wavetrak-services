import React from 'react';
import PillButton from './index';

export default {
  title: 'PillButton',
};

export const Default = () => <PillButton>Pill Button</PillButton>;
export const Active = () => <PillButton isActive>Pill Button</PillButton>;

export const TwoButtons = () => (
  <div>
    <PillButton isActive>Pill Button</PillButton>
    <PillButton>Pill Button</PillButton>
  </div>
);
