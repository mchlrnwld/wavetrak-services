import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';
import conditionClassModifier from '../helpers/conditionClassModifier';
import conditionEnumToText from '../helpers/conditionEnumToText';
import applyConditionVariation, {
  getConditionClassWithVariation,
} from '../helpers/applyConditionVariation';

const surfConditionsClass = (conditions, expired) => {
  const classObject = {};
  classObject['quiver-surf-conditions'] = true;
  classObject[`quiver-surf-conditions--${conditionClassModifier(conditions)}`] =
    !!conditions;
  classObject['quiver-surf-conditions--expired'] = expired;
  return classNames(classObject);
};

const surfConditionsClassV2 = (conditions, expired) =>
  classNames({
    'quiver-surf-conditions-v2': true,
    [`quiver-surf-conditions-v2--${getConditionClassWithVariation(
      conditions
    )}`]: !!conditions,
    'quiver-surf-conditions-v2--expired': expired,
  });

const SurfConditions = ({
  conditions,
  defaultCondition,
  expired,
  sevenRatings,
}) => {
  const conditionEnum = sevenRatings
    ? applyConditionVariation(conditions)
    : conditions;
  const conditionText = conditionEnumToText(conditionEnum) || defaultCondition;
  return (
    <span
      className={
        sevenRatings
          ? surfConditionsClassV2(conditions, expired)
          : surfConditionsClass(conditions, expired)
      }
    >
      {!expired ? conditionText : 'Report Coming Soon'}
    </span>
  );
};

SurfConditions.propTypes = {
  conditions: PropTypes.string,
  defaultCondition: PropTypes.string,
  expired: PropTypes.bool,
  sevenRatings: PropTypes.bool,
};

SurfConditions.defaultProps = {
  conditions: null,
  defaultCondition: '',
  expired: false,
  sevenRatings: false,
};

export default SurfConditions;
