import React from 'react';

const Favorites = () => (
  <div className="quiver-intro-modal__item__icon">
    <svg
      width="36px"
      height="33px"
      viewBox="0 0 36 33"
      className="quiver-intro-modal__favorites--icon"
    >
      <defs>
        <polygon
          id="path-1-2-3"
          points={`18 27.7549505 11.8117647 31 13.0117647 24.1336634 8 19.2660891 
            14.9176471 18.2549505 18 12 21.0823529 18.2549505 28 19.2660891 22.9882353 
            24.1336634 24.1882353 31`}
        />
      </defs>
      <g
        id="onboarding-modal"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
        transform="translate(-56.000000, -312.000000)"
      >
        <g id="Group-8">
          <g id="Group-9" transform="translate(56.000000, 312.000000)">
            <rect
              id="Rectangle-11-Copy"
              fillOpacity="0.300000012"
              fill="#96A9B2"
              x="0"
              y="0"
              width="36"
              height="24"
            />
            <rect
              id="Rectangle-3-Copy-8"
              fill="#42A5FC"
              x="0"
              y="3"
              width="36"
              height="5"
            />
            <rect
              id="Rectangle-3-Copy-15"
              fill="#FFFFFF"
              x="4"
              y="5"
              width="4"
              height="1"
            />
            <rect
              id="Rectangle-3-Copy-10"
              fill="#FFFFFF"
              x="10"
              y="5"
              width="4"
              height="1"
            />
            <rect
              id="Rectangle-3-Copy-11"
              fill="#FFFFFF"
              x="16"
              y="5"
              width="4"
              height="1"
            />
            <rect
              id="Rectangle-3-Copy-12"
              fill="#FFFFFF"
              x="22"
              y="5"
              width="4"
              height="1"
            />
            <rect
              id="Rectangle-3-Copy-13"
              fill="#FFFFFF"
              x="28"
              y="5"
              width="4"
              height="1"
            />
            <g id="Shape" fillRule="nonzero">
              <use fill="#FFFFFF" fillRule="evenodd" xlinkHref="#path-1-2-3" />
              <path
                stroke="#FFFFFF"
                strokeWidth="1"
                d={`M18,28.3195267 L11.1443526,31.9145605 L12.4737025,24.3080905 
                  L6.92399789,18.9180531 L14.5853067,17.798215 L18,10.8688536 
                  L21.4146933,17.798215 L29.0760021,18.9180531 L23.5262975,24.3080905 
                  L24.8556474,31.9145605 L18,28.3195267 Z`}
              />
            </g>
            <polygon
              id="Shape"
              fill="#42A5FC"
              fillRule="nonzero"
              points={`18 27.7549505 11.8117647 31 13.0117647 24.1336634 8 19.2660891 
                14.9176471 18.2549505 18 12 21.0823529 18.2549505 28 19.2660891 22.9882353 
                24.1336634 24.1882353 31`}
            />
          </g>
        </g>
      </g>
    </svg>
  </div>
);

export default Favorites;
