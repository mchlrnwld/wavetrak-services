import React from 'react';

const SwellData = () => (
  <div className="quiver-intro-modal__item__icon">
    <svg
      width="36px"
      height="28px"
      viewBox="0 0 36 28"
      className="quiver-intro-modal__swell--data--icon"
    >
      <g
        id="onboarding-modal"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
        transform="translate(-56.000000, -388.000000)"
      >
        <g id="Group-8">
          <g id="Group-2" transform="translate(56.000000, 388.000000)">
            <rect
              id="Rectangle-15"
              fill="#42A5FC"
              x="0"
              y="13"
              width="11"
              height="15"
            />
            <rect
              id="Rectangle-15-Copy-2"
              fill="#42A5FC"
              x="25"
              y="0"
              width="11"
              height="28"
            />
            <rect
              id="Rectangle-15-Copy"
              fillOpacity="0.5"
              fill="#96A9B2"
              x="13"
              y="6"
              width="10"
              height="22"
            />
          </g>
        </g>
      </g>
    </svg>
  </div>
);

export default SwellData;
