import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import ModalContainer from '../ModalContainer';
import IntroModal from './IntroModal';

describe('IntroModal', () => {
  const { location } = window;

  before(() => {
    delete window.location;
    window.location = {
      assign: sinon.stub(),
    };
  });
  after(() => {
    window.location = location;
  });

  it('should render with appropriate props', () => {
    const onClose = () => 1;
    const wrapper = shallow(
      <IntroModal isOpen onboardingUrl="http://url.com" onClose={onClose} />
    );
    const modal = wrapper.find(ModalContainer);
    expect(modal).to.have.length(1);
    expect(wrapper.prop('isOpen')).to.be.true();
    expect(wrapper.prop('onClose')).to.equal(onClose);
  });

  it('should trigger the proper results when clicking "Done"', () => {
    const onClose = sinon.spy();
    const wrapper = shallow(
      <IntroModal isOpen onboardingUrl="http://url.com" onClose={onClose} />
    );

    const button = wrapper.findWhere(
      (node) => node.prop('onClick') === onClose
    );
    expect(button).to.have.length(1);
    button.simulate('click');
    expect(onClose.calledOnce).to.be.true();
  });

  it('should trigger the proper results when clicking onboarding', () => {
    const wrapper = shallow(
      <IntroModal isOpen onboardingUrl="http://url.com" />
    );

    // Stub the handleSubmit method
    const component = wrapper.instance();

    const onboardMeSpy = sinon.spy(component, 'onboardMe');

    // Force the component and wrapper to update so that the stub is used
    component.forceUpdate();
    wrapper.update();

    // Find the onboarding button
    const button = wrapper.findWhere((node) => node.prop('type') === 'info');
    expect(button).to.have.length(1);

    button.simulate('click');
    expect(onboardMeSpy).to.have.been.calledOnce();
    expect(onboardMeSpy).to.have.been.calledWith('http://url.com');
    expect(window.location.assign).to.have.been.calledWith('http://url.com');

    onboardMeSpy.restore();
  });
});
