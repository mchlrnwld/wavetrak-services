import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Button from '../Button';
import ModalContainer from '../ModalContainer';
import SearchDiscoverIcon from './SearchDiscoverIcon';
import HumanForecastIcon from './HumanForecastIcon';
import FavoritesIcon from './FavoritesIcon';
import SwellDataIcon from './SwellDataIcon';

class IntroModal extends Component {
  onboardMe = (onboardingUrl) => {
    window.location.assign(onboardingUrl);
  };

  render() {
    const { isOpen, onClose, onboardingUrl } = this.props;

    const whatsNew = "See What's New";
    const seeWhatsChanged = "See What's Changed";
    return (
      <ModalContainer
        isOpen={isOpen}
        onClose={onClose}
        contentLabel="See Whats New"
      >
        <div className="quiver-intro-modal">
          <div className="quiver-intro-modal__header">
            <h2>{whatsNew}</h2>
            <p>
              <span>
                We`&#39;`ve made some improvements to the Surfline experience.
              </span>
              <br />
              <span>
                Here`&#39;`s a look at some of the new features and updates.
              </span>
            </p>
          </div>
          <div className="quiver-intro-modal__items">
            <div className="quiver-intro-modal__item">
              <SearchDiscoverIcon />
              <div className="quiver-intro-modal__item__text">
                <h3>Search and discover surf spots</h3>
                <p>
                  {`Use our rapid search to find cams and forecasts or our improved
                  navigation and map view to discover new spots.`}
                </p>
              </div>
            </div>
            <div className="quiver-intro-modal__item">
              <HumanForecastIcon />
              <div className="quiver-intro-modal__item__text">
                <h3>Enhanced human forecast analysis</h3>
                <p>
                  {`Our trained surf forecasters now offer real time,
                  detailed analysis for multiple regions around the world.`}
                </p>
              </div>
            </div>
            <div className="quiver-intro-modal__item">
              <FavoritesIcon />
              <div className="quiver-intro-modal__item__text">
                <h3>Easy access to favorites</h3>
                <p>
                  {`You're never more than a click away from your favorite cams
                  and reports anywhere on the site.`}
                </p>
              </div>
            </div>
            <div className="quiver-intro-modal__item">
              <SwellDataIcon />
              <div className="quiver-intro-modal__item__text">
                <h3>More accurate swell data</h3>
                <p>
                  {`We've refined our surf forecasts using observations from
                  our team of meteorologists and more frequent model updates.`}
                </p>
              </div>
            </div>
          </div>
          <div className="quiver-intro-modal__options">
            <Button type="info" onClick={() => this.onboardMe(onboardingUrl)}>
              <span className="quiver-intro-modal__options__mobile--text">
                Learn More
              </span>
              <span className="quiver-intro-modal__options__desktop--text">
                {seeWhatsChanged}
              </span>
            </Button>
            <Button onClick={onClose}>Done</Button>
          </div>
        </div>
      </ModalContainer>
    );
  }
}

IntroModal.propTypes = {
  onboardingUrl: PropTypes.string.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
};

IntroModal.defaultProps = {
  onClose: null,
};

export default IntroModal;
