import React from 'react';
import IntroModal from './index';

export default {
  title: 'IntroModal',
};

export const Default = () => (
  <IntroModal
    isOpen
    onClose={() => {}}
    onboardingUrl="http://new.surfline.com"
  />
);

Default.story = {
  name: 'default',
};
