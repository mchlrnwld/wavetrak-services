import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import HorizontalSwitch from './HorizontalSwitch';

describe('HorizontalSwitch', () => {
  it('should handle clicks', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(<HorizontalSwitch onClick={onClick} />);
    expect(onClick).not.to.have.been.called();
    wrapper.simulate('click');
    expect(onClick).to.have.been.calledOnce();
  });

  it('should position handle according to prop', () => {
    const leftWrapper = shallow(<HorizontalSwitch position="LEFT" />);
    const leftHandle = leftWrapper.find('.quiver-horizontal-switch__handle');
    expect(leftHandle.prop('style')).to.deep.equal({
      position: 'absolute',
      top: 0,
      left: 0,
    });

    const rightWrapper = shallow(<HorizontalSwitch position="RIGHT" />);
    const rightHandle = rightWrapper.find('.quiver-horizontal-switch__handle');
    expect(rightHandle.prop('style')).to.deep.equal({
      position: 'absolute',
      top: 0,
      left: 20,
    });
  });
});
