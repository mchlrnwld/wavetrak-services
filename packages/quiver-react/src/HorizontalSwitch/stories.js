import React, { Component } from 'react';
import PropTypes from 'prop-types';
import HorizontalSwitch from './index';

class PositionStateContainer extends Component {
  constructor() {
    super();
    this.state = { position: 'LEFT' };
  }

  togglePosition = () => {
    const { position } = this.state;
    this.setState({
      position: position === 'LEFT' ? 'RIGHT' : 'LEFT',
    });
  };

  render() {
    const { children } = this.props;
    return React.cloneElement(children, {
      ...this.state,
      onClick: this.togglePosition,
    });
  }
}

PositionStateContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

export default {
  title: 'HorizontalSwitch',
};

export const Default = () => (
  <PositionStateContainer>
    <HorizontalSwitch />
  </PositionStateContainer>
);

Default.story = {
  name: 'default',
};
