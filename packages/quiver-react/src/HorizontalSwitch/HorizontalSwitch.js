import React from 'react';
import PropTypes from 'prop-types';

const handlePositionStyle = (position) => ({
  position: 'absolute',
  top: 0,
  left: position === 'LEFT' ? 0 : 20,
});

const HorizontalSwitch = ({ position, onClick }) => (
  <button type="button" className="quiver-horizontal-switch" onClick={onClick}>
    <div className="quiver-horizontal-switch__slider" />
    <div
      className="quiver-horizontal-switch__handle"
      style={handlePositionStyle(position)}
    />
  </button>
);

HorizontalSwitch.propTypes = {
  position: PropTypes.oneOf(['LEFT', 'RIGHT']),
  onClick: PropTypes.func,
};

HorizontalSwitch.defaultProps = {
  position: 'LEFT',
  onClick: null,
};

export default HorizontalSwitch;
