import React, { useMemo } from 'react';
import { snippet as segment } from '@surfline/web-common';
import PropTypes from 'prop-types';

/**
 * @function SegmentBrowser
 * @param {object} props
 * @param {string} props.segmentKey
 * @param {string} props.anonymousId
 * @param {boolean} props.isPageCacheable
 * @return {JSX.Element}
 */
const SegmentBrowser = ({ segmentKey, isPageCacheable, anonymousId }) => {
  const withScriptTag = false;
  const segmentSnippet = useMemo(
    () => segment(segmentKey, anonymousId, isPageCacheable, withScriptTag),
    [segmentKey, anonymousId, isPageCacheable, withScriptTag]
  );
  return (
    <script
      className="sl-segment-snippet"
      type="text/javascript"
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{ __html: segmentSnippet }}
    />
  );
};

SegmentBrowser.propTypes = {
  segmentKey: PropTypes.string.isRequired,
  isPageCacheable: PropTypes.bool,
  anonymousId: PropTypes.string,
};

SegmentBrowser.defaultProps = {
  isPageCacheable: false,
  anonymousId: null,
};

export default SegmentBrowser;
