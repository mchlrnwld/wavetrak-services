import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SegmentBrowser from './SegmentBrowser';

describe('SegmentBrowser', () => {
  it('should render the Segment script into DOM', () => {
    const wrapper = shallow(<SegmentBrowser segmentKey="sk_test" />);
    return expect(wrapper.find('script')).to.not.be.empty;
  });
});
