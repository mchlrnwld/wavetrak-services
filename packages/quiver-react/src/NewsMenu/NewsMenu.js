import React from 'react';
import PropTypes from 'prop-types';

import BaseMenu from '../BaseMenu';
import BaseMenuSection from '../BaseMenuSection';

const NewsMenu = ({ links, onClickLink }) => (
  <BaseMenu>
    <BaseMenuSection
      links={links}
      onClick={(properties) =>
        onClickLink(
          {
            name: 'Clicked Main Nav',
            properties: {
              ...properties,
              tabName: 'News',
              clickEndLocation: 'News',
            },
          },
          true
        )
      }
    />
  </BaseMenu>
);

NewsMenu.propTypes = {
  links: PropTypes.arrayOf(
    PropTypes.shape({
      display: PropTypes.string,
      href: PropTypes.string,
    })
  ).isRequired,
  onClickLink: PropTypes.func,
};

NewsMenu.defaultProps = {
  onClickLink: null,
};

export default NewsMenu;
