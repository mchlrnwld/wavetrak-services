/* @prettier */
import React from 'react';

/* eslint-disable max-len */
const MultiCamIcon = () => (
  <svg
    width="14px"
    height="10px"
    viewBox="0 0 14 10"
    className="quiver-multi-cam-icon"
  >
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g
        className="quiver-multi-cam-icon__icon"
        transform="translate(-516.000000, -238.000000)"
        fill="#96A9B2"
      >
        <g transform="translate(500.000000, 56.000000)">
          <g transform="translate(16.000000, 176.000000)">
            <g transform="translate(0.000000, 6.000000)">
              <g>
                <path d="M14,2.83377778 L14,8.98844444 L11.3333333,7.33333333 L11.3333333,9.19342222 C11.3333333,9.63893333 11.0324444,10 10.6611852,10 L2.67214815,10 C2.30088889,10 2,9.63893333 2,9.19342222 L2,2.80657778 C2,2.36106667 2.30088889,2 2.67214815,2 L10.6611852,2 C11.0324444,2 11.3333333,2.36106667 11.3333333,2.80657778 L11.3333333,4.48888889 L14,2.83377778 Z" />
                <path
                  d="M1,0 L8.5,0 C8.77614237,-5.07265313e-17 9,0.223857625 9,0.5 L9,0.5 C9,0.776142375 8.77614237,1 8.5,1 L0,1 L0,1 C-6.76353751e-17,0.44771525 0.44771525,1.01453063e-16 1,0 Z"
                  fillRule="nonzero"
                />
                <path
                  d="M0,1 L1,1 L1,7.5 C1,7.77614237 0.776142375,8 0.5,8 L0.5,8 C0.223857625,8 3.38176876e-17,7.77614237 0,7.5 L0,1 Z"
                  fillRule="nonzero"
                />
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default MultiCamIcon;
