import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import MultiCamIcon from './MultiCamIcon';

describe('Favorite', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<MultiCamIcon />);
    const icon = wrapper.find('.quiver-multi-cam-icon');
    expect(icon).to.have.length(1);
  });
});
