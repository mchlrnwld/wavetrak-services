import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import PremiumAnalysisWind from '.';

describe('PremiumAnalysisWind', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<PremiumAnalysisWind />);
    expect(wrapper).to.have.length(1);
  });
});
