import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import RailIcon from '.';

describe('RailIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<RailIcon />);
    expect(wrapper).to.have.length(1);
  });
});
