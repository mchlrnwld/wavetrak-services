import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SwellLines from '.';

describe('SwellLines', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<SwellLines />);
    expect(wrapper).to.have.length(1);
  });
});
