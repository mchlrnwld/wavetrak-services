import React from 'react';

/**
 * This icon did not work properly as a react SVG component. Some of the lines were coming out
 * of the borders, the simplest workaround was just to serve the SVG from static
 * and set it as a background image.
 */
const SwellLines = () => (
  <img alt="swell-lines" className="swell_lines_icon" src="" />
);

export default SwellLines;
