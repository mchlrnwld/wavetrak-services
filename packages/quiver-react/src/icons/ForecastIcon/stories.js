import React from 'react';
import ForecastIcon from './ForecastIcon';

export default {
  title: 'ForecastIcon',
};

export const Default = () => <ForecastIcon />;

Default.story = {
  name: 'default',
};
