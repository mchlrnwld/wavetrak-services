import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Satellite from '.';

describe('Satellite', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<Satellite />);
    expect(wrapper).to.have.length(1);
  });
});
