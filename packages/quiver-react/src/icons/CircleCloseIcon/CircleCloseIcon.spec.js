import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import CircleCloseIcon from './CircleCloseIcon';

describe('CircleCloseIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<CircleCloseIcon />);
    expect(wrapper).to.have.length(1);
  });
});
