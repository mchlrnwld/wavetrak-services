import React from 'react';
import CircleCloseIcon from './CircleCloseIcon';

export default {
  title: 'CircleCloseIcon',
};

export const Default = () => <CircleCloseIcon />;

Default.story = {
  name: 'default',
};

export const Diameter50 = () => <CircleCloseIcon diameter={50} />;

Diameter50.story = {
  name: '50px diameter',
};

export const Diameter100 = () => <CircleCloseIcon diameter={100} />;

Diameter100.story = {
  name: '100px diameter',
};
