import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import PremiumCamIconSmall from './PremiumCamIconSmall';

describe('PremiumCamIconSmall', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<PremiumCamIconSmall />);
    expect(wrapper).to.have.length(1);
  });
});
