import React from 'react';

/* eslint-disable max-len */
const PremiumCamIconSmall = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    width="17"
    height="18"
    viewBox="0 0 17 18"
  >
    <defs>
      <path id="a" d="M0 0h17v18l-8.5-3.522L0 18z" />
    </defs>
    <g fill="none" fillRule="evenodd" opacity=".949">
      <g>
        <use fill="#FFF" xlinkHref="#a" />
        <path
          stroke="#FFF"
          strokeWidth="1.5"
          d="M.75.75v16.127l7.75-3.21 7.75 3.21V.75H.75z"
        />
      </g>
      <path
        fill="#22D737"
        d="M12.952 5.968V9.58l-1.978-.971V9.7a.487.487 0 0 1-.5.474H4.547a.487.487 0 0 1-.498-.474V5.952c0-.262.223-.474.498-.474h5.929c.275 0 .499.212.499.474v.987l1.978-.971z"
      />
    </g>
  </svg>
);

export default PremiumCamIconSmall;
