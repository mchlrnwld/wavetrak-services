import React from 'react';

export default () => (
  <svg
    className="quiver-question-mark-icon"
    width="16px"
    height="16px"
    viewBox="0 0 16 16"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="Spot" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <circle
        className="quiver-question-mark-icon__circle"
        fillRule="nonzero"
        cx="8"
        cy="8"
        r="7.5"
      />
      <text
        className="quiver-question-mark-icon__text"
        fontFamily="SFUIText-Semibold, SF UI Text"
        fontSize="12"
        fontWeight="500"
      >
        <tspan x="5" y="12">
          ?
        </tspan>
      </text>
    </g>
  </svg>
);
