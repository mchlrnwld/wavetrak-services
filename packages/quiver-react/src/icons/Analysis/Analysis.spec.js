import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Analysis from '.';

describe('Analysis', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<Analysis />);
    expect(wrapper).to.have.length(1);
  });
});
