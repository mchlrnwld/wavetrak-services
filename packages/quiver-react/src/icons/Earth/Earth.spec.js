import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Earth from '.';

describe('Earth', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<Earth />);
    expect(wrapper).to.have.length(1);
  });
});
