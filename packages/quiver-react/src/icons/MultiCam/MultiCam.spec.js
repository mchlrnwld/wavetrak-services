import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import MultiCam from '.';

describe('MultiCam', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<MultiCam />);
    expect(wrapper.find('.quiver-multi-cam-icon--large')).to.have.length(0);
    expect(wrapper.find('.quiver-multi-cam-icon--small')).to.have.length(1);
  });
  it('should render the icon', () => {
    const wrapper = shallow(<MultiCam large />);
    expect(wrapper.find('.quiver-multi-cam-icon--large')).to.have.length(1);
    expect(wrapper.find('.quiver-multi-cam-icon--small')).to.have.length(0);
  });
});
