import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ReportsRegistrationIcon from './ReportsRegistrationIcon';

describe('ReportsRegistrationIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<ReportsRegistrationIcon />);
    expect(wrapper).to.have.length(1);
  });
});
