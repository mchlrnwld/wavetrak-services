import React from 'react';

/* eslint-disable max-len */
const ReportsRegistrationIcon = () => (
  <svg
    width="43px"
    height="42px"
    viewBox="0 0 43 42"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g
      id="Desktop"
      stroke="none"
      strokeWidth="1"
      fill="none"
      fillRule="evenodd"
    >
      <g
        id="forced-reg-desktop"
        transform="translate(-547.000000, -367.000000)"
        strokeWidth="1.6"
      >
        <g id="Group-13" transform="translate(468.000000, 145.000000)">
          <g
            id="reports-forecasts"
            transform="translate(80.000000, 222.000000)"
          >
            <g id="Group-26-Copy-2" transform="translate(14.591729, 0.000000)">
              <path
                d="M7.5847424,28.3572115 C7.93289311,28.3971933 8.28497029,28.4173913 8.63983926,28.4173913 C11.1416792,28.4173913 13.484646,27.4080658 15.21571,25.6422179 L15.4127123,25.4412571 L15.6921437,25.4078915 C21.8629971,24.6710598 26.559491,19.4164276 26.559491,13.1478261 C26.559491,6.32831005 21.0311809,0.8 14.2116649,0.8 L13.1478261,0.8 C6.32831005,0.8 0.8,6.32831005 0.8,13.1478261 C0.8,18.4158394 4.12945509,23.0516208 9.01318467,24.7866424 L9.911166,25.1056644 L9.44143152,25.9348176 C8.93576327,26.8273994 8.30974427,27.6426317 7.5847424,28.3572115 Z"
                id="Combined-Shape-Copy-5"
                stroke="#96A9B2"
              />
              <path
                d="M5.4718982,15.032928 C7.77540326,9.88184945 14.2268452,6.42877564 18.2396607,11.9257341 C13.2872763,11.9257341 13.2872763,17.1195652 18.2396607,18.2608696"
                id="Path-3-Copy-10"
                stroke="#0058B0"
              />
            </g>
            <g
              id="Group-29-Copy"
              transform="translate(0.000000, 21.913043)"
              stroke="#96A9B2"
            >
              <g id="Group-27-Copy">
                <path
                  d="M-3.16191517e-13,19.0369565 C-3.16191517e-13,15.75 5.25475938,13.5586957 10.0318134,13.5586957 C14.8088673,13.5586957 20.0636267,15.75 20.0636267,19.0369565"
                  id="Path-5-Copy-3"
                  strokeLinecap="round"
                />
                <ellipse
                  id="Oval-4-Copy-3"
                  cx="10.0318134"
                  cy="4.56521739"
                  rx="4.55991516"
                  ry="4.56521739"
                />
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default ReportsRegistrationIcon;
