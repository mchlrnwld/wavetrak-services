import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Layers from '.';

describe('Layers', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<Layers />);
    expect(wrapper).to.have.length(1);
  });
});
