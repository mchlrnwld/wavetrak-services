import React from 'react';
import CamIconWithRibbon from './CamIconWithRibbon';
import WindIcon from './WindIcon';
import WindArrow from './WindArrow';
import TideIcon from './TideIcon';
import QuestionMark from './QuestionMark';
import Analysis from './Analysis';
import Buoy from './Buoy';
import CamRewind from './CamRewind';
import Cams from './Cams';
import Charts from './Charts';
import Layers from './Layers';
import PremiumAnalysisWind from './PremiumAnalysisWind';
import Satellite from './Satellite';
import SwellLines from './SwellLines';
import Wave from './Wave';
import Waypoint from './Waypoint';
import Pencil from './Pencil';

export default {
  title: 'Icons',
};

export const CamIconWithRibbonStory = () => (
  <div style={{ backgroundColor: 'gray' }}>
    <CamIconWithRibbon />
  </div>
);

CamIconWithRibbonStory.story = {
  name: '<CamIconWithRibbon />',
};

export const CamIconWithRibbonIsFilled = () => <CamIconWithRibbon isFilled />;

CamIconWithRibbonIsFilled.story = {
  name: '<CamIconWithRibbon isFilled/>',
};

export const CamIconWithRibbonIsMulti = () => (
  <div style={{ backgroundColor: 'gray' }}>
    <CamIconWithRibbon isMulti />
  </div>
);

CamIconWithRibbonIsMulti.story = {
  name: '<CamIconWithRibbon isMulti/>',
};

export const CamIconWithRibbonIsMultiIsFilled = () => (
  <CamIconWithRibbon isMulti isFilled />
);

CamIconWithRibbonIsMultiIsFilled.story = {
  name: '<CamIconWithRibbon isMulti isFilled/>',
};

export const WindIconStory = () => <WindIcon />;

WindIconStory.story = {
  name: 'WindIcon',
};

export const WindArrowStory = () => (
  <div style={{ height: 200, width: 200 }}>
    <WindArrow degrees={110} speed={1} />
  </div>
);

WindArrowStory.story = {
  name: 'WindArrow',
};

export const TideIconStory = () => <TideIcon />;

TideIconStory.story = {
  name: 'TideIcon',
};

export const QuestionMarkStory = () => <QuestionMark />;

QuestionMarkStory.story = {
  name: 'QuestionMark',
};

export const AnalysisStory = () => <Analysis />;
export const BuoyStory = () => <Buoy />;
export const CamRewindStory = () => <CamRewind />;

CamRewindStory.story = {
  name: 'CamRewind',
};

export const CamsStory = () => <Cams />;
export const ChartsStory = () => <Charts />;
export const LayersStory = () => <Layers />;
export const PremiumAnalysisWindStory = () => <PremiumAnalysisWind />;

PremiumAnalysisWindStory.story = {
  name: 'PremiumAnalysisWind',
};

export const SatelliteStory = () => <Satellite />;
export const SwellLinesStory = () => <SwellLines />;

SwellLinesStory.story = {
  name: 'SwellLines',
};

export const WaveStory = () => <Wave />;
export const WaypointStory = () => <Waypoint />;
export const PencilStory = () => <Pencil />;
