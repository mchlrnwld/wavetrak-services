import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Buoy from '.';

describe('Buoy', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<Buoy />);
    expect(wrapper).to.have.length(1);
  });
});
