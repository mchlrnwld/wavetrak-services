import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SurflineWaterDrop from './SurflineWaterDrop';

describe('SurflineWaterDrop', () => {
  const wrapper = shallow(<SurflineWaterDrop />);

  it('should render an svg element', () => {
    expect(wrapper.html()).to.contain('quiver-brand-logo__surfline-water-drop');
  });
});
