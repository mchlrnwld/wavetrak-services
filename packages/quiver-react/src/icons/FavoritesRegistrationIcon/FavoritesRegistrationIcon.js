import React from 'react';

/* eslint-disable max-len */
const FavoritesRegistrationIcon = () => (
  <svg
    width="40px"
    height="40px"
    viewBox="0 0 40 40"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g
      id="Desktop"
      stroke="none"
      strokeWidth="1"
      fill="none"
      fillRule="evenodd"
    >
      <g
        id="forced-reg-desktop"
        transform="translate(-549.000000, -453.000000)"
        strokeWidth="1.5"
      >
        <g id="Group-13" transform="translate(468.000000, 145.000000)">
          <g id="Group-10-Copy" transform="translate(81.000000, 308.000000)">
            <g id="favorite-icon">
              <circle
                id="Oval-3"
                stroke="#96A9B2"
                opacity="0.998409601"
                cx="20"
                cy="20"
                r="19.25"
              />
              <g
                id="icons/star-icon-white"
                transform="translate(8.000000, 7.000000)"
                stroke="#0058B0"
              >
                <path
                  d="M18.4414138,21.6115643 L16.7234856,14.211456 L22.4573746,9.23296839 L14.8994885,8.58320757 L11.9480559,1.60642142 L8.99662326,8.58320757 L1.43873723,9.23296839 L7.17262615,14.211456 L5.45469804,21.6115643 L11.9480559,17.6878483 L18.4414138,21.6115643 Z"
                  id="Shape"
                />
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default FavoritesRegistrationIcon;
