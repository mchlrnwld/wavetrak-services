import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import FavoritesRegistrationIcon from './FavoritesRegistrationIcon';

describe('FavoritesRegistrationIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<FavoritesRegistrationIcon />);
    expect(wrapper).to.have.length(1);
  });
});
