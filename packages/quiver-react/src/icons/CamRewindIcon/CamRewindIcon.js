import React from 'react';

const CamRewindIcon = () => (
  <svg
    className="quiver-rewind-button"
    width="18"
    height="18"
    viewBox="0 0 48 48"
    fill="rgba(255,255,255,0.8)"
  >
    <path d="M22 36V12L5 24l17 12zm1-12l17 12V12L23 24z" />
  </svg>
);

export default CamRewindIcon;
