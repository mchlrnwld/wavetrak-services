/* @prettier */
import React from 'react';

/* eslint-disable max-len */
const MultiCamIconWithText = () => (
  <svg
    width="45px"
    height="10px"
    viewBox="0 0 45 10"
    className="quiver-multi-cam-icon-with-text"
  >
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g
        className="quiver-multi-cam-icon-with-text__icon"
        transform="translate(0.000000, -1.000000)"
        fill="#96A9B2"
      >
        <g>
          <g>
            <path d="M45,3.83377778 L45,9.98844444 L42.3333333,8.33333333 L42.3333333,10.1934222 C42.3333333,10.6389333 42.0324444,11 41.6611852,11 L33.6721481,11 C33.3008889,11 33,10.6389333 33,10.1934222 L33,3.80657778 C33,3.36106667 33.3008889,3 33.6721481,3 L41.6611852,3 C42.0324444,3 42.3333333,3.36106667 42.3333333,3.80657778 L42.3333333,5.48888889 L45,3.83377778 Z" />
            <text
              fontFamily="SourceSansPro-Semibold, Source Sans Pro"
              fontSize="10"
              fontWeight="500"
              letterSpacing="0.3333334"
            >
              <tspan x="0" y="10">
                MULTI
              </tspan>
            </text>
            <path
              d="M32,1 L39.5,1 C39.7761424,1 40,1.22385763 40,1.5 L40,1.5 C40,1.77614237 39.7761424,2 39.5,2 L31,2 L31,2 C31,1.44771525 31.4477153,1 32,1 Z"
              fillRule="nonzero"
            />
            <path
              d="M31,2 L32,2 L32,8.5 C32,8.77614237 31.7761424,9 31.5,9 L31.5,9 C31.2238576,9 31,8.77614237 31,8.5 L31,2 Z"
              fillRule="nonzero"
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default MultiCamIconWithText;
