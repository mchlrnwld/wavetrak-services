import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import MultiCamIconWithText from './MultiCamIconWithText';

describe('MultiCamIconWithText', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<MultiCamIconWithText />);
    expect(wrapper).to.have.length(1);
  });
});
