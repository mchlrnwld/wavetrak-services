import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import CamRegistrationIcon from './CamRegistrationIcon';

describe('CamRegistrationIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<CamRegistrationIcon />);
    expect(wrapper).to.have.length(1);
  });
});
