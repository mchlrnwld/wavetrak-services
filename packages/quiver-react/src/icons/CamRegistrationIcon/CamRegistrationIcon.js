import React from 'react';

/* eslint-disable max-len */
const CamRegistrationIcon = () => (
  <svg
    width="40px"
    height="40px"
    viewBox="0 0 40 40"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g
      id="Desktop"
      stroke="none"
      strokeWidth="1"
      fill="none"
      fillRule="evenodd"
    >
      <g
        id="forced-reg-desktop"
        transform="translate(-549.000000, -281.000000)"
        strokeWidth="1.5"
      >
        <g id="Group-13" transform="translate(468.000000, 145.000000)">
          <g id="Group-9-Copy" transform="translate(81.000000, 136.000000)">
            <g id="cams-icon">
              <g
                id="icons/cam-icon"
                transform="translate(11.000000, 13.000000)"
                stroke="#0058B0"
              >
                <path
                  d="M15,8.27272727 L15,11.2666667 C15,12.2239 14.2324286,13 13.2857143,13 L1.71428571,13 C0.767571429,13 0,12.2239 0,11.2666667 L0,1.73333333 C0,0.7761 0.767571429,0 1.71428571,0 L13.2857143,0 C14.2324286,0 15,0.7761 15,1.73333333 L15,4.72727273 L20,1.625 L20,11.375 L15,8.27272727 Z"
                  id="Combined-Shape-Copy"
                />
              </g>
              <circle
                id="Oval-3"
                stroke="#96A9B2"
                opacity="0.998409601"
                cx="20"
                cy="20"
                r="19.25"
              />
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default CamRegistrationIcon;
