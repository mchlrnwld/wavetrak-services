import React from 'react';

const NewCloseIcon = () => (
  <svg className="quiver-new-close-icon" viewBox="0 0 17 17">
    <g fillRule="evenodd">
      <g transform="translate(-1403.000000, -19.000000)" fillRule="nonzero">
        <path
          d={`M1411.46967,26.3428559 L1418.34286,19.4696699
          L1419.46967,20.5964839 L1412.59648,27.4696699 L1419.46967,
          34.3428559 L1418.34286,35.4696699 L1411.46967,28.5964839
          L1404.59648,35.4696699 L1403.46967,34.3428559 L1410.34286,
          27.4696699 L1403.46967,20.5964839 L1404.59648,19.4696699
          L1411.46967,26.3428559 Z`}
        />
      </g>
    </g>
  </svg>
);

export default NewCloseIcon;
