import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import CloseMediaPlayerIcon from './CloseMediaPlayerIcon';

describe('CloseMediaPlayerIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<CloseMediaPlayerIcon />);
    expect(wrapper).to.have.length(1);
  });
});
