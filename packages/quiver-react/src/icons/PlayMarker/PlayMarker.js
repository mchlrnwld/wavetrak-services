import React from 'react';

const PlayMarker = () => (
  <svg
    viewBox="0 0 40 40"
    preserveAspectRatio="xMinYMin"
    className="quiver-play-marker"
  >
    <g>
      <circle cx="20" cy="20" r="20" />
      <polygon points="17.5 15, 25 20, 17.5 25" />
    </g>
  </svg>
);

export default PlayMarker;
