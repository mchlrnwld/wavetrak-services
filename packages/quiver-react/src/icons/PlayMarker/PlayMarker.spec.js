import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import PlayMarker from './PlayMarker';

describe('PlayMarker', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<PlayMarker />);
    expect(wrapper).to.have.length(1);
  });
});
