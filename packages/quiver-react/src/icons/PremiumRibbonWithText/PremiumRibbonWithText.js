import React from 'react';

/* istanbul ignore next */
const PremiumRibbonWithText = () => (
  <svg
    className="quiver-premium-ribbon-with-text"
    width="95px"
    height="16px"
    viewBox="0 0 95 16"
  >
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-325.000000, -383.000000)" fill="#22D736">
        <g id="Group-3" transform="translate(152.000000, 343.000000)">
          <g id="Group-5" transform="translate(173.000000, 40.000000)">
            <g stroke="#22D736" strokeWidth="1.16666667">
              <path
                d={`M0.583333333,0.583333333 L0.583333333,15.1001228 L7,12.2305576
                L13.4166667,15.1001228 L13.4166667,0.583333333 L0.583333333,0.583333333 Z`}
              />
            </g>
            <text
              className="premium-only-text"
              fontFamily="SourceSansPro-Semibold, Source Sans Pro"
              fontSize="12"
              fontWeight="500"
              letterSpacing="0.200000003"
            >
              <tspan x="19" y="12">
                Premium Onl
              </tspan>
              <tspan x="89.228" y="12">
                y
              </tspan>
            </text>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default PremiumRibbonWithText;
