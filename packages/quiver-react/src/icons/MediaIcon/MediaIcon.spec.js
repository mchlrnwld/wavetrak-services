import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import MediaIcon from './MediaIcon';

describe('MediaIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<MediaIcon />);
    expect(wrapper).to.have.length(1);
  });
});
