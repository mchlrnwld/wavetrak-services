import React from 'react';
import SurflineLogo from './index';

export default {
  title: 'SurflineLogo',
};

export const Default = () => (
  <div style={{ width: '105px', height: '18px', background: '#7795c6' }}>
    <SurflineLogo />
  </div>
);

Default.story = {
  name: 'default',
};
