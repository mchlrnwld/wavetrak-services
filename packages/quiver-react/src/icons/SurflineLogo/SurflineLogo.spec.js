import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SurflineLogo from './SurflineLogo';

describe('SurflineLogo', () => {
  const wrapper = shallow(<SurflineLogo />);

  it('should render an svg element', () => {
    expect(wrapper.html()).to.contain('quiver-brand-logo__surfline');
  });
});
