import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import CameraIcon from './CameraIcon';

describe('CameraIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<CameraIcon />);
    expect(wrapper).to.have.length(1);
  });
});
