import React from 'react';

const CameraIcon = () => (
  <svg
    className="quiver-camera-icon"
    viewBox="0 0 12 8"
    preserveAspectRatio="xMinYMin meet"
  >
    <rect x="0" y="0" width="9" height="8" rx="1" ry="1" />
    <polygon points="9,5 9,3 12,1 12,7" />
  </svg>
);

export default CameraIcon;
