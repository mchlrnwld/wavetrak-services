import React from 'react';
import CameraIcon from './CameraIcon';

export default {
  title: 'CameraIcon',
};

export const Default = () => <CameraIcon />;

Default.story = {
  name: 'default',
};
