import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Charts from '.';

describe('Charts', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<Charts />);
    expect(wrapper).to.have.length(1);
  });
});
