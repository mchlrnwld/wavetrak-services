import React from 'react';
import PropTypes from 'prop-types';
import SingleCamIcon from '../SingleCamIcon';
import MultiCamIcon from '../MultiCamIcon';
import MultiCamIconWithText from '../MultiCamIconWithText';

const CamIcon = ({ isMultiCam, withText }) => {
  if (isMultiCam) {
    return withText ? <MultiCamIconWithText /> : <MultiCamIcon />;
  }
  return <SingleCamIcon />;
};

CamIcon.propTypes = {
  isMultiCam: PropTypes.bool,
  withText: PropTypes.bool,
};

CamIcon.defaultProps = {
  isMultiCam: false,
  withText: false,
};

export default CamIcon;
