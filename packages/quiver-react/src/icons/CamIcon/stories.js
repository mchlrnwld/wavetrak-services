import React from 'react';
import CamIcon from './CamIcon';

export default {
  title: 'CamIcon',
};

export const Default = () => <CamIcon />;

Default.story = {
  name: 'default',
};

export const Multi = () => <CamIcon isMultiCam />;

Multi.story = {
  name: 'multi',
};

export const MultiWithText = () => <CamIcon withText isMultiCam />;

MultiWithText.story = {
  name: 'multi with text',
};
