import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import CamIconWithRibbon from '.';

describe('CamIconWithRibbon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<CamIconWithRibbon />);
    expect(wrapper).to.have.length(1);
  });
});
