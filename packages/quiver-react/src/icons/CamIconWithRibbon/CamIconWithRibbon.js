/** @prettier */
/* eslint-disable max-len */

import React from 'react';
import PropTypes from 'prop-types';

const CamIconWithRibbon = ({ isFilled, isMulti }) => (
  <svg
    width="56px"
    height="61px"
    viewBox="0 0 56 61"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <title>{`${isMulti ? 'Multi ' : ''}Cam Icon with a ribbon`}</title>
    <desc>Start Free Trial to get access to Multi and HD cams.</desc>
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <path
        d="M2.33333333,2.33333333 L2.33333333,57.4689797 L28,46.5287623 L53.6666667,57.4689797 L53.6666667,2.33333333 L2.33333333,2.33333333 Z"
        stroke="#22D736"
        strokeWidth="4.66666667"
        fill={isFilled ? '#22D736' : ''}
      />
      <g
        id="Group-6"
        transform="translate(10.500000, 13.507143)"
        fill="#FFFFFF"
      >
        <path d="M35,6.79094603 L35,21.5401651 L28.3333333,17.5738095 L28.3333333,22.0313797 C28.3333333,23.0990152 27.5811111,23.9642857 26.652963,23.9642857 L6.68037037,23.9642857 C5.75222222,23.9642857 5,23.0990152 5,22.0313797 L5,6.72576317 C5,5.65812762 5.75222222,4.79285714 6.68037037,4.79285714 L26.652963,4.79285714 C27.5811111,4.79285714 28.3333333,5.65812762 28.3333333,6.72576317 L28.3333333,10.7573016 L35,6.79094603 Z" />
        {isMulti && (
          <path
            d="M2.39642857,0 L21.3017857,0 C21.9635412,-1.00974093e-15 22.5,0.536458809 22.5,1.19821429 C22.5,1.85996976 21.9635412,2.39642857 21.3017857,2.39642857 L0,2.39642857 C-1.62083345e-16,1.07291762 1.07291762,2.43125018e-16 2.39642857,0 Z"
            fillRule="nonzero"
          />
        )}
        {isMulti && (
          <path
            d="M0,2.39642857 L2.5,2.39642857 L2.5,17.9214286 C2.5,18.6117845 1.94035594,19.1714286 1.25,19.1714286 C0.559644063,19.1714286 8.45442189e-17,18.6117845 0,17.9214286 L0,2.39642857 Z"
            fillRule="nonzero"
          />
        )}
      </g>
    </g>
  </svg>
);

CamIconWithRibbon.propTypes = {
  isFilled: PropTypes.bool,
  isMulti: PropTypes.bool,
};

CamIconWithRibbon.defaultProps = {
  isFilled: false,
  isMulti: false,
};

export default CamIconWithRibbon;
