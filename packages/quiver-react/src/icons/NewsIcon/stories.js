import React from 'react';
import NewsIcon from './NewsIcon';

export default {
  title: 'NewsIcon',
};

export const Default = () => <NewsIcon />;

Default.story = {
  name: 'default',
};
