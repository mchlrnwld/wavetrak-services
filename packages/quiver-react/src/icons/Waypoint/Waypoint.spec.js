import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Waypoint from '.';

describe('Waypoint', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<Waypoint />);
    expect(wrapper).to.have.length(1);
  });
});
