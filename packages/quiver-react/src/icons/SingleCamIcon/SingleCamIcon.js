/* @prettier */
import React from 'react';

/* eslint-disable max-len */
const SingleCamIcon = () => (
  <svg viewBox="0 0 12 12" version="1.1" className="quiver-cam-icon">
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g className="quiver-cam-icon__icon" fill="#96A9B2">
        <path d="M12,3 L12,9 L9,7.09090909 L9,4.90909091 L12,3 Z M9,8.93333333 C9,9.5224 8.53945714,10 7.97142857,10 L1.02857143,10 C0.460542857,10 0,9.5224 0,8.93333333 L0,3.06666667 C0,2.4776 0.460542857,2 1.02857143,2 L7.97142857,2 C8.53945714,2 9,2.4776 9,3.06666667 L9,8.93333333 Z" />
      </g>
    </g>
  </svg>
);

export default SingleCamIcon;
