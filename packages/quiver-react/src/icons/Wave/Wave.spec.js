import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Wave from '.';

describe('Wave', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<Wave />);
    expect(wrapper).to.have.length(1);
  });
});
