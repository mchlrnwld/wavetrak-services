import React from 'react';
import PropTypes from 'prop-types';

const Wave = ({ stroke }) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="16"
    height="12"
    className="wave_icon"
    viewBox="0 0 16 12"
  >
    <path
      fill="none"
      fillRule="evenodd"
      stroke={stroke || '#22D737'}
      strokeWidth="1.244"
      d="M1 12C1 4.24 9.273-3.52 14 4.168c-4.727 0-4.727 5.885 0 7.179"
    />
  </svg>
);

Wave.propTypes = {
  stroke: PropTypes.string,
};

Wave.defaultProps = {
  stroke: null,
};

export default Wave;
