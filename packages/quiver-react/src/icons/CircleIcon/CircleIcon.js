import React from 'react';
import PropTypes from 'prop-types';
import { color } from '@surfline/quiver-themes';

const circle = ({ fill, border, size, onClick, borderWidth }) => (
  <svg onClick={onClick} width={`${size + 1}px`} height={`${size + 1}px`}>
    <g>
      <circle
        cx={`${size / 2}`}
        cy={`${size / 2}`}
        r={`${size / 2 - 1}`} // accounts for 1px border width
        stroke={border}
        strokeWidth={borderWidth}
        fill={fill}
      />
    </g>
  </svg>
);

circle.propTypes = {
  fill: PropTypes.string,
  border: PropTypes.string,
  borderWidth: PropTypes.number,
  size: PropTypes.number,
  onClick: PropTypes.func,
};

circle.defaultProps = {
  fill: color('dark-blue', 20),
  border: color('dark-blue', 20),
  borderWidth: 1,
  size: 12,
  onClick: null,
};

export default circle;
