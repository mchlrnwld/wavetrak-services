import React from 'react';
import CircleIcon from './CircleIcon';

export default {
  title: 'CircleIcon',
};

export const Default = () => <CircleIcon />;

Default.story = {
  name: 'default',
};

export const CustomBorderAndFill = () => (
  <CircleIcon border="#a100b0" fill="#00b0a4" />
);

CustomBorderAndFill.story = {
  name: 'custom border and fill',
};
