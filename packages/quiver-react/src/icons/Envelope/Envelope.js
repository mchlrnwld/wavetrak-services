import React from 'react';

const Envelope = () => (
  <svg width="146" height="95" viewBox="0 0 146 95" fill="none">
    <g filter="url(#filter0_d)">
      <rect x="16" y="12" width="114" height="63" rx="2" fill="white" />
      <rect
        x="16"
        y="12"
        width="114"
        height="63"
        rx="2"
        stroke="#0058B0"
        strokeWidth="3"
      />
    </g>
    <path d="M16 12.7979L73 51.0763L130 12.7979" fill="white" />
    <path
      d="M16 12.7979L73 51.0763L130 12.7979"
      stroke="#0058B0"
      strokeWidth="3"
    />
    <defs>
      <filter
        id="filter0_d"
        x="0.5"
        y="0.5"
        width="145"
        height="94"
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity="0" result="BackgroundImageFix" />
        <feColorMatrix
          in="SourceAlpha"
          type="matrix"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
        />
        <feOffset dy="4" />
        <feGaussianBlur stdDeviation="7" />
        <feColorMatrix
          type="matrix"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.105359 0"
        />
        <feBlend
          mode="normal"
          in2="BackgroundImageFix"
          result="effect1_dropShadow"
        />
        <feBlend
          mode="normal"
          in="SourceGraphic"
          in2="effect1_dropShadow"
          result="shape"
        />
      </filter>
    </defs>
  </svg>
);

export default Envelope;
