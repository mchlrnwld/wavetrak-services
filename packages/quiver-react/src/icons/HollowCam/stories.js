import React from 'react';
import HollowCam from './HollowCam';

export default {
  title: 'HollowCam',
};

export const Default = () => <HollowCam />;

Default.story = {
  name: 'default',
};
