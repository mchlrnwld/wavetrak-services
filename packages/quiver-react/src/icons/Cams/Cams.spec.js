import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Cams from '.';

describe('Cams', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<Cams />);
    expect(wrapper).to.have.length(1);
  });
});
