import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ClipboardIcon from './ClipboardIcon';

describe('ClipboardIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<ClipboardIcon />);
    expect(wrapper).to.have.length(1);
  });
});
