import React from 'react';

const PremiumRibbon = () => (
  <svg
    width="9px"
    height="10px"
    viewBox="0 0 9 10"
    version="1.1"
    className="quiver-premium-ribbon"
  >
    <g
      id="Multiple-Cams"
      stroke="none"
      strokeWidth="1"
      fill="none"
      fillRule="evenodd"
    >
      <g
        id="free-paywall-hover"
        transform="translate(-203.000000, -309.000000)"
        fill="#22D736"
      >
        <polygon
          id="Rectangle-25-Copy"
          points="203 309 212 309 212 319 207.5 317.043478 203 319"
        />
      </g>
    </g>
  </svg>
);

export default PremiumRibbon;
