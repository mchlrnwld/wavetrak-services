import React from 'react';

/* eslint-disable max-len */
const PremiumCamIconMedium = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="27"
    viewBox="0 0 24 27"
  >
    <g fill="none" fillRule="evenodd" strokeWidth="1.152">
      <path
        fill="#FFF"
        stroke="#FFF"
        d="M.576.576v25.541L12 21.088l11.424 5.03V.575H.576z"
      />
      <path
        stroke="#22D737"
        fill="#22D737"
        d="M18.286 8.951v5.42l-2.794-1.458v1.638c0 .392-.315.71-.704.71h-8.37a.707.707 0 0 1-.704-.71V8.928c0-.393.316-.71.704-.71h8.37c.389 0 .704.317.704.71v1.48l2.794-1.457z"
      />
    </g>
  </svg>
);

export default PremiumCamIconMedium;
