import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import PremiumCamIconMedium from './PremiumCamIconMedium';

describe('PremiumCamIconMedium', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<PremiumCamIconMedium />);
    expect(wrapper).to.have.length(1);
  });
});
