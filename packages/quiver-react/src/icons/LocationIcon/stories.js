import React from 'react';
import LocationIcon from './LocationIcon';

export default {
  title: 'LocationIcon',
};

export const Default = () => <LocationIcon />;

Default.story = {
  name: 'default',
};
