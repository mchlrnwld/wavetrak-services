import React from 'react';
import CloseIcon from './index';

export default {
  title: 'CloseIcon',
};

export const Default = () => (
  // eslint-disable-next-line no-alert
  <CloseIcon onClick={() => alert('click')} />
);

Default.story = {
  name: 'default',
};
