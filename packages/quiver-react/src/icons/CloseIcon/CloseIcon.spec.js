import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import CloseIcon from './CloseIcon';

describe('CloseIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<CloseIcon />);
    expect(wrapper).to.have.length(1);
  });
});
