import React from 'react';
import PropTypes from 'prop-types';

const Close = ({ onClick, isCircle }) =>
  isCircle ? (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className="quiver-close-icon"
      onClick={onClick}
    >
      <g>
        <g>
          <circle cx="12" cy="12" r="12" fill="#2B2A2B" fillOpacity="0.5" />
        </g>
        <path
          d="M7.38477 16.6154L16.6155 7.38464"
          stroke="white"
          strokeWidth="1.3125"
        />
        <path
          d="M16.6155 16.6154L7.38477 7.38459"
          stroke="white"
          strokeWidth="1.3125"
        />
      </g>
    </svg>
  ) : (
    <svg
      width="13"
      height="13"
      viewBox="0 0 13 13"
      className="quiver-close-icon"
      onClick={onClick}
    >
      <path
        fill="#000"
        fillRule="evenodd"
        d={`M7.206 6.5L12.855.851a.502.502 0 0 0 0-.706.505.505 0 0 0-.706
        0L6.5 5.794.851.145a.502.502 0 0 0-.706 0 .505.505 0 0 0 0
        .706L5.794 6.5.145 12.149a.502.502 0 0 0 0 .706c.19.189.511.195.706 0L6.5
        7.206l5.649 5.649a.502.502 0 0 0 .706 0 .505.505 0 0 0 0-.706L7.206 6.5z`}
      />
    </svg>
  );

Close.propTypes = {
  onClick: PropTypes.func,
  isCircle: PropTypes.bool,
};

Close.defaultProps = {
  onClick: () => {},
  isCircle: false,
};

export default Close;
