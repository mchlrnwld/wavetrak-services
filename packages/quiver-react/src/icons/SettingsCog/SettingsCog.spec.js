import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import SettingsCog from './SettingsCog';

describe('SettingsCog', () => {
  it('should render the icon', () => {
    const wrapper = mount(<SettingsCog />);

    expect(wrapper).to.have.length(1);
  });
  it('should render the icon with given props', () => {
    const wrapper = mount(
      <SettingsCog
        width={100}
        height={100}
        viewBox="0 0 16 16"
        title="custom title"
        color="blue"
      />
    );

    const wrapperDiv = wrapper.find('.quiver-settings-cog-icon');
    expect(wrapper).to.have.length(1);
    expect(wrapperDiv.prop('style')).to.deep.equal({
      color: 'blue',
      width: 100,
      height: 100,
    });
    expect(wrapper.find('svg').prop('viewBox')).to.be.equal('0 0 16 16');
    expect(wrapper.find('title').prop('id')).to.exist();
    expect(wrapper.find('title').text()).to.be.equal('custom title');
  });
});
