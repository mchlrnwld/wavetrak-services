import React from 'react';

const Bell = () => (
  <svg
    className="bell-icon"
    width="14"
    height="18"
    viewBox="0 0 14 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <circle cx="7" cy="16" r="1.5" stroke="#0058B0" />
    <path
      d="M1.54732 5.97947C1.8037 3.15933 4.16823 1 7 1V1C9.83177 1 12.1963 3.15932 12.4527 5.97946L13 12H1L1.54732 5.97947Z"
      stroke="#0058B0"
    />
  </svg>
);

export default Bell;
