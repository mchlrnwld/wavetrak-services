import PropTypes from 'prop-types';
import React from 'react';

const rotate = (degrees) => `rotate(${degrees} 7.5 7.5)`;

const WindArrow = ({ degrees, speed }) => (
  <svg
    viewBox="0 0 15 15"
    preserveAspectRatio="xMinYMin meet"
    className="quiver-wind-arrow"
  >
    {speed > 0 ? (
      <polygon
        points="7.5,0 5,8.75 1.5,8.75 7.5,15 13.5,8.75 10,8.75"
        transform={rotate(degrees)}
      />
    ) : null}
  </svg>
);

WindArrow.propTypes = {
  degrees: PropTypes.number.isRequired,
  speed: PropTypes.number.isRequired,
};

export default WindArrow;
