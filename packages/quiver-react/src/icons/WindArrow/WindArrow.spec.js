import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import WindArrow from './WindArrow';

describe('components / WindArrow', () => {
  it('render an svg with the polygon rotated by degrees', () => {
    const wrapper = shallow(<WindArrow degrees={85} speed={1} />);
    const polygon = wrapper.find('polygon');
    expect(polygon).to.have.length(1);
    expect(polygon.prop('transform')).to.equal('rotate(85 7.5 7.5)');
  });

  it('does not render the polygon if speed is not greater than 0', () => {
    const wrapper = shallow(<WindArrow degrees={85} speed={0} />);
    expect(wrapper.prop('children')).to.be.null();
  });
});
