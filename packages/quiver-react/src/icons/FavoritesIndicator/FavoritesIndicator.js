import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';

const FavoritesIndicator = ({ favorited, showText }) => {
  const getFavoritesIndicatorClasses = () =>
    classNames({
      'quiver-favorites-indicator': true,
      'quiver-favorites-indicator--favorited': favorited,
      'quiver-favorites-indicator--show-text': showText,
      'quiver-favorites-indicator--show-text--favorited': showText && favorited,
    });

  return (
    <div className={getFavoritesIndicatorClasses()}>
      <svg className="quiver-favorites-indicator__icon">
        <g>
          <circle cx="16" cy="16" r="16" />
          <polygon
            points="16 8, 18.5 13, 24 14, 20 18, 21 23, 16 20, 11 23, 12 18, 8 14, 13.5 13"
            strokeLinejoin="round"
          />
        </g>
      </svg>
      {showText ? (
        <div className="quiver-favorites-indicator__text">
          {favorited ? 'Favorited' : 'Add Favorite'}
        </div>
      ) : null}
    </div>
  );
};

FavoritesIndicator.propTypes = {
  favorited: PropTypes.bool,
  showText: PropTypes.bool,
};

FavoritesIndicator.defaultProps = {
  favorited: false,
  showText: false,
};

export default FavoritesIndicator;
