import React from 'react';
import FavoritesIndicator from './index';

export default {
  title: 'FavoritesIndicator',
};

export const DefaultSelected = () => <FavoritesIndicator favorited />;

DefaultSelected.story = {
  name: 'default (selected)',
};

export const DefaultUnselected = () => <FavoritesIndicator />;

DefaultUnselected.story = {
  name: 'default (unselected)',
};

export const ShowTextSelected = () => <FavoritesIndicator favorited showText />;

ShowTextSelected.story = {
  name: 'show text (selected)',
};

export const ShowTextUnselected = () => <FavoritesIndicator showText />;

ShowTextUnselected.story = {
  name: 'show text (unselected)',
};
