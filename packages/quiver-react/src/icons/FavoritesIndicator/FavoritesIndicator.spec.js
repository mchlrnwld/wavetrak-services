import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import FavoritesIndicator from './FavoritesIndicator';

describe('FavoritesIndicator', () => {
  it('should render unselected favorites icon', () => {
    const defaultWrapper = shallow(<FavoritesIndicator />);
    expect(
      defaultWrapper.find('.quiver-favorites-indicator').exists()
    ).to.be.true();
    expect(
      defaultWrapper.find('.quiver-favorites-indicator--favorited').exists()
    ).to.not.be.true();
  });

  it('should render selected favorites icon', () => {
    const defaultWrapper = shallow(<FavoritesIndicator favorited />);
    expect(
      defaultWrapper.find('.quiver-favorites-indicator').exists()
    ).to.be.true();
    expect(
      defaultWrapper.find('.quiver-favorites-indicator--favorited').exists()
    ).to.be.true();
  });

  it('should render unselected favorites text', () => {
    const defaultWrapper = shallow(<FavoritesIndicator showText />);
    expect(
      defaultWrapper.find('.quiver-favorites-indicator').exists()
    ).to.be.true();
    expect(
      defaultWrapper.find('.quiver-favorites-indicator--favorited').exists()
    ).to.not.be.true();
    expect(
      defaultWrapper.find('.quiver-favorites-indicator--show-text').exists()
    ).to.be.true();
    expect(
      defaultWrapper
        .find('.quiver-favorites-indicator--show-text--favorited')
        .exists()
    ).to.not.be.true();
    expect(
      defaultWrapper.find('.quiver-favorites-indicator--show-text').text()
    ).to.equal('Add Favorite');
  });

  it('should render selected favorites text', () => {
    const defaultWrapper = shallow(<FavoritesIndicator favorited showText />);
    expect(
      defaultWrapper.find('.quiver-favorites-indicator').exists()
    ).to.be.true();
    expect(
      defaultWrapper.find('.quiver-favorites-indicator--favorited').exists()
    ).to.be.true();
    expect(
      defaultWrapper.find('.quiver-favorites-indicator--show-text').exists()
    ).to.be.true();
    expect(
      defaultWrapper
        .find('.quiver-favorites-indicator--show-text--favorited')
        .exists()
    ).to.be.true();
    expect(
      defaultWrapper.find('.quiver-favorites-indicator--show-text').text()
    ).to.equal('Favorited');
  });
});
