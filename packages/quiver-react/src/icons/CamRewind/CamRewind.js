import React from 'react';

const CamRewind = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    className="camrewind_icon"
    viewBox="0 0 35 37"
    width="26"
    height="30"
  >
    <g fill="none" fillRule="evenodd">
      <path
        stroke="#22D737"
        d="M26.621 14.961v7.149l-3.71-1.923v2.16a.936.936 0 0 1-.935.938H10.86a.936.936 0 0 1-.935-.937V14.93c0-.518.419-.937.935-.937h11.116c.516 0 .935.42.935.937v1.954l3.71-1.923z"
      />
      <path
        stroke="#FFF"
        d="M1 19.081c0 9.133 7.42 16.537 16.575 16.537 9.154 0 16.575-7.404 16.575-16.537S26.73 2.544 17.575 2.544c-2.139 0-4.183.404-6.06 1.14"
      />
      <path stroke="#FFF" d="M9.925 1.164L7.161 5.76h5.528L9.925 1.164z" />
    </g>
  </svg>
);

export default CamRewind;
