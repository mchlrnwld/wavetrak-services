import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import CamRewind from '.';

describe('CamRewind', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<CamRewind />);
    expect(wrapper).to.have.length(1);
  });
});
