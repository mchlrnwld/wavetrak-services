import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Pencil from '.';

describe('Pencil', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<Pencil />);
    expect(wrapper).to.have.length(1);
  });
});
