import React from 'react';
import TravelIcon from './TravelIcon';

export default {
  title: 'TravelIcon',
};

export const Default = () => <TravelIcon />;

Default.story = {
  name: 'default',
};
