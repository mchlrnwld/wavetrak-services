import React from 'react';

const PremiumRibbonIcon = () => (
  <svg
    className="quiver-premium-ribbon-icon"
    width="40px"
    height="50px"
    viewBox="0 0 40 50"
  >
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g
        transform="translate(-71.000000, -280.000000)"
        fill="#22D737"
        fillRule="nonzero"
      >
        <polygon points="71 280 111 280 111 330 91 320.955653 71 330" />
      </g>
    </g>
  </svg>
);

export default PremiumRibbonIcon;
