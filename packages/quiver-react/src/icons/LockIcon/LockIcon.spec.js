import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import LockIcon from './LockIcon';

describe('LockIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<LockIcon />);
    expect(wrapper).to.have.length(1);
  });
});
