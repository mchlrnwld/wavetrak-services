import React from 'react';
import PropTypes from 'prop-types';
import { color } from '@surfline/quiver-themes';

/**
 * NewTab Icon
 *
 * @function NewTab
 * @param {object} props
 * @param {number} [props.width=15]
 * @param {number} [props.height=15]
 * @return {JSX.Element}
 */
const NewTab = ({ width, height }) => (
  <svg
    width={width}
    height={height}
    viewBox={`0 0 ${width} ${height}`}
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g
      stroke="none"
      strokeWidth="1"
      fill="none"
      fillRule="evenodd"
      strokeLinecap="round"
    >
      <g
        transform="translate(-397.000000, -395.000000)"
        fillRule="nonzero"
        stroke={color('blue-gray', 10)}
        strokeWidth="1.5"
      >
        <g transform="translate(236.000000, 389.000000)">
          <g transform="translate(162.000000, 7.000000)">
            <path d="M12,8 L12,11.5 C12,12.3284271 11.3284271,13 10.5,13 L1.5,13 C0.671572875,13 0,12.3284271 0,11.5 L0,2.5 C0,1.67157288 0.671572875,1 1.5,1 L5,1" />
            <path d="M8,2.27373675e-13 L12.3333333,2.27373675e-13 C13,2.27373675e-13 13,2.27373675e-13 13,0.666666667 L13,5" />
            <path d="M12.5,0.5 L6,7" />
          </g>
        </g>
      </g>
    </g>
  </svg>
);

NewTab.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
};

NewTab.defaultProps = {
  width: 15,
  height: 15,
};

export default NewTab;
