import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import NewTabIcon from './NewTabIcon';

describe('NewTabIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<NewTabIcon />);
    expect(wrapper).to.have.length(1);
    expect(wrapper.find('svg').prop('width')).to.be.equal(15);
    expect(wrapper.find('svg').prop('height')).to.be.equal(15);
    expect(wrapper.find('svg').prop('viewBox')).to.be.equal('0 0 15 15');
  });
  it('should render the icon with custom width/height', () => {
    const wrapper = shallow(<NewTabIcon width={20} height={20} />);
    expect(wrapper).to.have.length(1);
    expect(wrapper.find('svg').prop('width')).to.be.equal(20);
    expect(wrapper.find('svg').prop('height')).to.be.equal(20);
    expect(wrapper.find('svg').prop('viewBox')).to.be.equal('0 0 20 20');
  });
});
