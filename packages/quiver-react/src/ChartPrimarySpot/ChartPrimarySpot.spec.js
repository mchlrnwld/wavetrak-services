import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { MemoryRouter, Link } from 'react-router-dom';
import ChartPrimarySpot from './ChartPrimarySpot';

describe('components / ChartPrimarySpot', () => {
  it('should render with appropriate class names', () => {
    const wrapper = mount(
      <MemoryRouter>
        <ChartPrimarySpot
          className="quiver-test-classname"
          id="1234"
          name="spot-name"
        />
      </MemoryRouter>
    );

    const linkWrapper = wrapper.find(Link);

    expect(linkWrapper).to.have.length(1);
    expect(linkWrapper.prop('className')).to.have.be.equal(
      'quiver-test-classname'
    );
    expect(linkWrapper.prop('to')).to.have.be.equal(
      '/surf-report/spot-name/1234'
    );
    expect(linkWrapper.text()).to.have.be.equal('for spot-name');
  });

  it('should render with an anchor tag when desired', () => {
    const wrapper = mount(
      <ChartPrimarySpot
        className="quiver-test-classname"
        id="1234"
        name="spot-name"
        useAnchorTag
      />
    );

    const linkWrapper = wrapper.find('a');

    expect(linkWrapper).to.have.length(1);
    expect(linkWrapper.prop('className')).to.have.be.equal(
      'quiver-test-classname'
    );
    expect(linkWrapper.prop('href')).to.have.be.equal(
      '/surf-report/spot-name/1234'
    );
    expect(linkWrapper.text()).to.have.be.equal('for spot-name');
  });
});
