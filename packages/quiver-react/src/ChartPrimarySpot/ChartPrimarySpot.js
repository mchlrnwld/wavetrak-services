import PropTypes from 'prop-types';
import React from 'react';
import NextLink from 'next/link';
import { Link } from 'react-router-dom';
import { kbygPaths } from '@surfline/web-common';

const ChartPrimarySpot = ({
  className,
  id,
  name,
  useAnchorTag,
  useNextLink,
}) => {
  const href = `${kbygPaths.spotReportPath(name, id, true)}`;
  if (useAnchorTag) {
    return (
      <a className={className} href={href}>
        for {name}
      </a>
    );
  }

  if (useNextLink) {
    return (
      <NextLink href={href}>
        <a className={className} href={href}>
          for {name}
        </a>
      </NextLink>
    );
  }

  return (
    <Link className={className} to={href}>
      for {name}
    </Link>
  );
};

ChartPrimarySpot.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  useAnchorTag: PropTypes.bool,
  useNextLink: PropTypes.bool,
};

ChartPrimarySpot.defaultProps = {
  className: null,
  useAnchorTag: false,
  useNextLink: false,
};

export default ChartPrimarySpot;
