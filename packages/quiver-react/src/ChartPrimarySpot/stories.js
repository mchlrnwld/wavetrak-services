import React from 'react';
import { MemoryRouter } from 'react-router-dom';

import ChartPrimarySpot from './ChartPrimarySpot';

export default {
  title: 'ChartPrimarySpot',
};

export const Default = () => (
  <MemoryRouter>
    <ChartPrimarySpot
      className="quiver-custom-className"
      id="1234"
      name="spot-page-name"
    >
      Link to spot page
    </ChartPrimarySpot>
  </MemoryRouter>
);

Default.story = {
  name: 'default',
};
