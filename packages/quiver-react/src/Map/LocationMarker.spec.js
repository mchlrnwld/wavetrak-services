import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import LocationMarker from './LocationMarker';

describe('LocationMarker', () => {
  it('should render the LocationMarker', () => {
    const wrapper = shallow(<LocationMarker />);
    expect(wrapper).to.have.length(1);
  });

  it('should render the LocationMarker with default props', () => {
    const wrapper = shallow(<LocationMarker />);
    const CustomMarker = wrapper.find('CustomMarker');
    expect(CustomMarker).to.have.length(1);
    expect(CustomMarker.prop('iconSize')).to.deep.equal([30, 30]);
    expect(CustomMarker.prop('iconAnchor')).to.deep.equal([15, 30]);
    expect(CustomMarker.prop('onClick')).to.be.null();
    expect(CustomMarker.prop('map')).to.be.null();
    expect(CustomMarker.prop('usingPortal')).to.be.false();
  });

  it('should render the CustomMarker with the portal props', () => {
    const wrapper = shallow(<LocationMarker map={{}} usingPortal />);

    const CustomMarker = wrapper.find('CustomMarker');
    expect(CustomMarker).to.have.length(1);
    expect(CustomMarker.prop('map')).to.deep.equal({});
    expect(CustomMarker.prop('usingPortal')).to.be.true();
  });
});
