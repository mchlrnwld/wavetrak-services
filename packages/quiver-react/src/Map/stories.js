/* eslint-disable no-alert */
import React from 'react';
import { createPortal } from 'react-dom';
import 'leaflet/dist/leaflet.css';
import {
  Map,
  CustomMarker,
  CircleMarker,
  LocationMarker,
  TileLayer,
  Popup,
} from '.';
import SpotMarker from '../SpotMarker';

const sampleSpotMarker = (
  <SpotMarker
    surf={{
      min: 1,
      max: 2,
      units: 'ft',
    }}
    style={{
      surf: {
        lineHeight: '1', // This is to over-ride the leaflet default
      },
    }}
  />
);

const defaultLocation = { lat: 52, lon: -7 };

export default {
  title: 'Map',
};

export const DefaultWithMarkerAndClick = () => (
  <div style={{ height: '100vh' }}>
    <Map
      center={defaultLocation}
      zoom={5}
      onClick={({ lat, lon }) => {
        alert(`${lat}, ${lon}`);
      }}
    >
      <TileLayer />
      <LocationMarker position={defaultLocation} />
    </Map>
  </div>
);

DefaultWithMarkerAndClick.story = {
  name: 'default with marker and click',
};

export const SpotMarkerWithTooltip = () => (
  <div style={{ height: '100vh' }}>
    <Map center={defaultLocation} zoom={5}>
      <TileLayer />
      <CustomMarker
        position={defaultLocation}
        icon={sampleSpotMarker}
        iconSize={[30, 30]}
      >
        <Popup> Sample Popup Text </Popup>
      </CustomMarker>
    </Map>
  </div>
);

SpotMarkerWithTooltip.story = {
  name: 'Spot Marker with tooltip',
};

export const Gridpoints = () => (
  <div style={{ height: '100vh' }}>
    <Map center={defaultLocation} zoom={5}>
      <TileLayer />
      {[1, 2, 3, 4].map((ind) => {
        let { lat, lon } = defaultLocation;
        lon += ind;
        lat += ind;
        return (
          <CircleMarker
            key={`${lat}${lon}`}
            position={{
              lat,
              lon,
            }}
            active={ind === 1}
            onClick={() => {
              alert(`${lat}, ${lon}`); // To check that the onClick binding works
            }}
          />
        );
      })}
    </Map>
  </div>
);

export const MapWithPortaledMarker = () => {
  const [map, setMap] = React.useState();

  return (
    <div style={{ height: '100vh' }}>
      <>
        <Map
          center={defaultLocation}
          zoom={5}
          mapProps={{
            whenCreated: setMap,
          }}
        >
          <TileLayer />
        </Map>
        <>
          {map
            ? createPortal(
                <LocationMarker
                  position={defaultLocation}
                  usingPortal
                  map={map}
                />,
                // eslint-disable-next-line no-underscore-dangle
                map._container
              )
            : null}
        </>
      </>
    </div>
  );
};
