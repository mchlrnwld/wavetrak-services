import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import TileLayer from './TileLayer';

describe('TileLayer', () => {
  it('should render the TileLayer', () => {
    const wrapper = shallow(<TileLayer />);
    expect(wrapper).to.have.length(1);
  });
});
