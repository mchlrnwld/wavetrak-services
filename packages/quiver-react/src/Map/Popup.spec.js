import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Popup from './Popup';
import * as hooks from './useLeaflet';

describe('Popup', () => {
  /** @type {sinon.SinonStub} */
  let useLeafletComponetStub;

  beforeEach(() => {
    useLeafletComponetStub = sinon.stub(hooks, 'useLeafletComponent');
  });

  afterEach(() => {
    useLeafletComponetStub.restore();
  });

  it('should render the Popup', () => {
    const wrapper = shallow(<Popup />);
    expect(wrapper).to.have.length(1);
  });

  it('should render the Popup with default props', () => {
    const Comp = () => <div className="popup" />;
    useLeafletComponetStub.returns(Comp);

    const wrapper = shallow(<Popup />);

    const LeafletPopup = wrapper.find(Comp);
    expect(LeafletPopup).to.have.length(1);
    expect(LeafletPopup.prop('attribution')).to.be.null();
    expect(LeafletPopup.prop('onClose')).to.be.null();
    expect(LeafletPopup.prop('onOpen')).to.be.null();
  });

  it('should render the Popup with the portal props', async () => {
    useLeafletComponetStub.returns(() => <div className="popup" />);

    const wrapper = shallow(<Popup map={{}} usingPortal />);

    const MapContext = wrapper.find('MapContext');

    expect(MapContext).to.have.length(1);
    expect(MapContext.prop('map')).to.deep.equal({});
  });
});
