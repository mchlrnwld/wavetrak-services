import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import MapContext from './MapContext';
import * as hooks from './useLeaflet';

describe('MapContext', () => {
  /** @type {sinon.SinonStub} */
  let useLeafletCoreImportStub;

  beforeEach(() => {
    useLeafletCoreImportStub = sinon.stub(hooks, 'useLeafletCoreImport');
  });

  afterEach(() => {
    useLeafletCoreImportStub.restore();
  });

  it('should render the MapContext', () => {
    const wrapper = shallow(
      <MapContext>
        <div />
      </MapContext>
    );
    expect(wrapper.find('div')).to.have.length(0);
  });

  it('should render the MapContext with the provider', async () => {
    const LeafletProvider = ({ children }) => children;
    useLeafletCoreImportStub.returns(LeafletProvider);

    const wrapper = shallow(
      <MapContext map={{}}>
        <div />
      </MapContext>
    );

    const divWrapper = wrapper.find('div');
    const provider = wrapper.find(LeafletProvider);

    expect(divWrapper).to.have.length(1);
    expect(provider).to.have.length(1);

    expect(provider.prop('value')).to.deep.equal({ map: {} });
  });
});
