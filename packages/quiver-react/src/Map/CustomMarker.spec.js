import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { mount } from 'enzyme';
import Map from './Map';
import CustomMarker from './CustomMarker';
import * as hooks from './useLeaflet';

describe('Custom Marker', () => {
  /** @type {sinon.SinonStub} */
  let useLeafletComponetStub;

  beforeEach(() => {
    useLeafletComponetStub = sinon.stub(hooks, 'useLeafletComponent');
  });

  afterEach(() => {
    useLeafletComponetStub.restore?.();
  });

  // eslint-disable-next-line react/prop-types
  const Marker = ({ children }) => <div className="marker">{children}</div>;

  it('should render a Custom Marker', async () => {
    useLeafletComponetStub.restore();
    const defaultLocation = { lat: 0, lon: 0 };

    const wrapper = mount(
      <Map center={defaultLocation}>
        <CustomMarker
          position={defaultLocation}
          icon={<p>test</p>}
          iconSize={[12, 12]}
          iconAnchor={[6, 12]}
        />
      </Map>
    );

    const wait = () => new Promise((resolve) => setTimeout(resolve, 200));
    await wait();

    wrapper.setProps({ update: 1 });
    wrapper.update();

    const markerWrapper = wrapper.find('ForwardRef(ContainerComponent)');

    expect(markerWrapper).to.have.lengthOf(1);
    expect(markerWrapper.props().position.lon).to.eql(0);
    expect(markerWrapper.props().position.lat).to.eql(0);
  });

  it('should render a Custom Marker with the MapContext if usingPortal is true', async () => {
    useLeafletComponetStub.withArgs('Marker').returns(Marker);

    const defaultLocation = { lat: 0, lon: 0 };

    const wrapper = mount(
      <CustomMarker
        position={defaultLocation}
        icon={<p>test</p>}
        iconSize={[12, 12]}
        iconAnchor={[6, 12]}
        usingPortal
        map={{}}
      />
    );

    const wait = () => new Promise((resolve) => setTimeout(resolve, 200));
    await wait();

    wrapper.update();

    const contextWrapper = wrapper.find('MapContext');
    const markerWrapper = wrapper.find(Marker);

    expect(contextWrapper).to.have.lengthOf(1);
    expect(markerWrapper).to.have.lengthOf(1);
    expect(contextWrapper.prop('map')).to.deep.equal({});
  });
});
