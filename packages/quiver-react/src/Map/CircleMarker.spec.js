import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import CircleMarker from './CircleMarker';

describe('CircleMarker', () => {
  it('should render the CircleMarker', () => {
    const wrapper = shallow(<CircleMarker />);
    expect(wrapper).to.have.length(1);
  });

  it('should render the CircleMarker with default props', () => {
    const wrapper = shallow(<CircleMarker position={[1, 1]} />);
    const CustomMarker = wrapper.find('CustomMarker');
    expect(CustomMarker).to.have.length(1);
    expect(CustomMarker.prop('icon')).to.exist();
    expect(CustomMarker.prop('iconSize')).to.deep.equal([12, 12]);
    expect(CustomMarker.prop('iconAnchor')).to.deep.equal([6, 6]);
    expect(CustomMarker.prop('position')).to.deep.equal([1, 1]);
    expect(CustomMarker.prop('eventHandlers')).to.be.null();
    expect(CustomMarker.prop('map')).to.be.null();
    expect(CustomMarker.prop('usingPortal')).to.be.false();
  });

  it('should render the CustomMarker with the portal props', () => {
    const wrapper = shallow(<CircleMarker map={{}} usingPortal />);
    const CustomMarker = wrapper.find('CustomMarker');
    expect(CustomMarker).to.have.length(1);
    expect(CustomMarker.prop('map')).to.deep.equal({});
    expect(CustomMarker.prop('usingPortal')).to.be.true();
  });
});
