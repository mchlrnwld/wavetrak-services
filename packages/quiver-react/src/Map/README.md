### Using the Leaflet Map components

In order to use the leaflet map components there are a few things to ensure are enabled:

1. Ensure that the webpack config has a loader for CSS

```javascript
  {
    test: /\.css$/,
    use: [
      'style-loader',
      { loader: 'css-loader', options: { importLoaders: 3, sourceMap: true } },
      'resolve-url-loader',
    ],
  }
```

2. Ensure that CSS modules are not enabled on the CSS loader (this will obfuscate the leaflet css classes)


### Using Portals to render markers, popups, and other child components

In order to use portal to render child components of the map we need to ensure that we can get
the map instance context into the child component. This means that the consuming application will
need to manage the map instance itself using state.

Below is an example of what this may look like. In the example we are rendering the `LocationMarker`
from outside the React tree of the `<Map/>` (ie: not as a direct child of it in React). Using the
`usingPortal` and `map` props we are able to flag that this component will need to have the `map`
instance passed into the marker component via context.

```jsx
import React from 'react';
import { createPortal } from 'react-dom';
import { Map, TileLayer, LocationMarker } from '@surfline/quiver-react';

import cloneChildrenWithProps from '../utils/cloneChildrenWithProps';

const MapComponent = () => {
  const [map, setMap] = useMap();

  const childrenWithMap = cloneChildrenWithProps(children, { map });

  return (
    <>
      <Map
        center={location.center}
        zoom={location.zoom}
        mapProps={{
          whenCreated: setMap,
        }}
      >
        <TileLayer attribution={ATTRIBUTION} url={MAP_TILE_URL} />
      </Map>
      <>
        {childrenWithMap}
        {map
          ? createPortal(
              <LocationMarker center={{ lat: 0, lon: 0 }} usingPortal map={map} />,
              map._container,
            )
          : null}
      </>
    </>
  );
}
```
