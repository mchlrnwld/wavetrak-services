import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Map from './Map';

describe('Map', () => {
  it('should render the Map', () => {
    const wrapper = shallow(<Map />);
    expect(wrapper).to.have.length(1);
  });
});
