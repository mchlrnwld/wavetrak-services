import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Pane from './Pane';
import * as hooks from './useLeaflet';

describe('Pane', () => {
  /** @type {sinon.SinonStub} */
  let useLeafletComponetStub;

  beforeEach(() => {
    useLeafletComponetStub = sinon.stub(hooks, 'useLeafletComponent');
  });

  afterEach(() => {
    useLeafletComponetStub.restore();
  });

  it('should render the Pane', () => {
    const wrapper = shallow(<Pane />);
    expect(wrapper).to.have.length(1);
  });

  it('should render the Pane with default props', () => {
    const Comp = () => <div />;
    useLeafletComponetStub.returns(Comp);

    const wrapper = shallow(<Pane name="name" />);

    const LeafletPopup = wrapper.find(Comp);

    expect(LeafletPopup).to.have.length(1);
    expect(LeafletPopup.prop('name')).to.be.equal('name');
    expect(LeafletPopup.prop('style')).to.be.null();
    expect(LeafletPopup.prop('className')).to.be.null();
  });

  it('should render the Popup with the portal props', async () => {
    useLeafletComponetStub.returns(() => <div />);

    const wrapper = shallow(<Pane map={{}} usingPortal />);

    const MapContext = wrapper.find('MapContext');

    expect(MapContext).to.have.length(1);
    expect(MapContext.prop('map')).to.deep.equal({});
  });
});
