import PropTypes from 'prop-types';
import React from 'react';
import { Motion, spring } from 'react-motion';
import WindArrow from '../icons/WindArrow';

const polarToCartesianX = (theta, r, origin) => r * Math.cos(theta) + origin;
const polarToCartesianY = (theta, r, origin) => origin - r * Math.sin(theta);

const WindAnimator = ({ width, height, direction, speed }) => (
  <Motion
    defaultStyle={{ direction }}
    style={{
      direction: spring(direction, { stiffness: 43, damping: 9 }),
    }}
  >
    {(value) => {
      const angle = -(Math.PI * (value.direction - 90)) / 180;
      const originX = width / 2;
      const originY = height / 2;
      return (
        <div className="quiver-wind-animator">
          <div
            className="quiver-wind-animator__arrow"
            style={{
              top: polarToCartesianY(angle, originY - 8, originY),
              left: polarToCartesianX(angle, originX - 8, originX),
            }}
          >
            <WindArrow speed={speed} degrees={value.direction} />
          </div>
        </div>
      );
    }}
  </Motion>
);

WindAnimator.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  direction: PropTypes.number.isRequired,
  speed: PropTypes.number.isRequired,
};

export default WindAnimator;
