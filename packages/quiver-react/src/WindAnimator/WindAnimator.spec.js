import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { Motion, spring } from 'react-motion';
import WindAnimator from './WindAnimator';

describe('components / WindAnimator', () => {
  it('adds renders a wind arrow with Motion', () => {
    const props = {
      width: 80,
      height: 80,
      direction: 133.87,
      speed: 1,
    };
    const wrapper = shallow(<WindAnimator {...props} />);

    const motion = wrapper.find(Motion);
    expect(motion).to.have.length(1);
    expect(motion.prop('defaultStyle')).to.deep.equal({
      direction: props.direction,
    });
    expect(motion.prop('style')).to.deep.equal({
      direction: spring(props.direction, { stiffness: 43, damping: 9 }),
    });

    const children = motion.prop('children');
    const values = {
      degrees: 150,
    };
    const animator = shallow(children(values));
    expect(animator.prop('className')).to.equal('quiver-wind-animator');
  });
});
