import React from 'react';
import WindAnimator from './WindAnimator';

const props = {
  width: 80,
  height: 80,
  direction: 133.87,
  speed: 1,
};

export default {
  title: 'WindAnimator',
};

export const Default = () => <WindAnimator {...props} />;

Default.story = {
  name: 'default',
};
