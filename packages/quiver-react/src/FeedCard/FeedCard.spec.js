import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import FeedCard from './FeedCard';

describe('FeedCard', () => {
  const props = {
    category: {
      name: 'Travel',
      url: 'https://www.surfline.com/surf-news/travel',
    },
    imageUrl:
      'https://d14fqx6aetz9ka.cloudfront.net/wp-content/uploads/2018/11/09095449/P1A8027_WestAus_2018-1200x799.jpg',
    permalink: 'https://www.surfline.com',
    subtitle: 'Feed card subtitle goes right about here and is not too long.',
    title: 'Feed Card Title Goes Here',
    createdAt: 1542307003,
    showTimestamp: true,
  };

  it('should render formatted category and date', () => {
    const wrapper = shallow(<FeedCard {...props} />);

    const categoryWrapper = wrapper.find('.quiver-feed-card__category');
    const permalink = categoryWrapper.find('a');
    expect(permalink).to.have.length(1);

    const date = wrapper.find('.quiver-feed-card__date');
    expect(date).to.have.length(1);
  });

  it('should render title and subtitle', () => {
    const wrapper = shallow(<FeedCard {...props} />);

    const title = wrapper.find('h3');
    expect(title).to.have.length(1);
    expect(title.text()).to.equal(props.title);

    const subtitle = wrapper.find('p');
    expect(subtitle).to.have.length(1);
    expect(subtitle.text()).to.equal(props.subtitle);
  });
});
