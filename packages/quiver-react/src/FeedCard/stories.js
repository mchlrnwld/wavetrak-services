/* eslint-disable max-len */
import React from 'react';
import FeedCard from './FeedCard';

const props = {
  category: {
    name: 'Travel',
    url: 'https://www.surfline.com/surf-news/travel',
  },
  imageUrl:
    'https://d14fqx6aetz9ka.cloudfront.net/wp-content/uploads/2018/11/09095449/P1A8027_WestAus_2018-1200x799.jpg',
  permalink: 'https://www.surfline.com',
  subtitle: 'Feed card subtitle goes right about here and is not too long.',
  title: 'Feed Card Title Goes Here',
  createdAt: 1582185600,
};

export default {
  title: 'FeedCard',
};

export const Default = () => (
  <div style={{ width: '250px' }}>
    <FeedCard {...props} />
  </div>
);

Default.story = {
  name: 'default',
};

export const WithPremium = () => (
  <div style={{ width: '250px' }}>
    <FeedCard {...props} isPremium />
  </div>
);

WithPremium.story = {
  name: 'with premium',
};

export const WithAltTitle = () => (
  <div style={{ width: '250px' }}>
    <FeedCard {...props} displayTitle="Alternate Title" />
  </div>
);

WithAltTitle.story = {
  name: 'with alt title',
};

export const WithTimestamp = () => (
  <div style={{ width: '250px' }}>
    <FeedCard {...props} displayTitle="Alternate Title" showTimestamp />
  </div>
);

WithTimestamp.story = {
  name: 'with timestamp',
};

export const IndexPage = () => (
  <div style={{ width: '250px' }}>
    <FeedCard {...props} showTimestamp fullWidthContent />
  </div>
);

IndexPage.story = {
  name: 'index page',
};

export const IndexPageSubcategory = () => (
  <div style={{ width: '250px' }}>
    <FeedCard {...props} showTimestamp fullWidthContent hideSubtitle />
  </div>
);

IndexPageSubcategory.story = {
  name: 'index page subcategory',
};
