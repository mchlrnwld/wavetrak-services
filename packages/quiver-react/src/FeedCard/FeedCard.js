import PropTypes from 'prop-types';
import React from 'react';
import { formatDistanceStrict } from 'date-fns';
import classnames from 'classnames';
import PremiumRibbonIcon from '../icons/PremiumRibbonIcon';

const getContentClassnames = (fullWidthContent) =>
  classnames({
    'quiver-feed-card__content': true,
    'quiver-feed-card__content__fullwidth': fullWidthContent,
  });

const formatFeedCardDate = (date) => {
  let cardDate = '';
  const formattedDate = formatDistanceStrict(date * 1000, new Date()).split(
    ' '
  );
  if (formattedDate && formattedDate.length && formattedDate.length > 1) {
    cardDate = `${formattedDate[0]} ${formattedDate[1]} ago`;
  }
  return cardDate;
};

const FeedCard = ({
  category,
  imageUrl,
  newWindow,
  onClickLink,
  onClickTag,
  subtitle,
  title,
  displayTitle,
  permalink,
  createdAt,
  showTimestamp,
  isPremium,
  fullWidthContent,
  hideSubtitle,
}) => (
  <div className="quiver-feed-card">
    {isPremium ? <PremiumRibbonIcon /> : null}
    <div className="quiver-feed-card__image">
      {/* eslint-disable-next-line jsx-a11y/anchor-has-content */}
      <a
        tabIndex="0"
        href={permalink}
        onClick={onClickLink}
        aria-label="Feed Card Image"
        style={{ backgroundImage: `url(${imageUrl})` }}
        target={newWindow ? '_blank' : ''}
        rel="noreferrer"
      />
    </div>
    <div className={getContentClassnames(fullWidthContent)}>
      <a
        className="quiver-feed-card__title"
        href={permalink}
        target={newWindow ? '_blank' : ''}
        onClick={onClickLink}
        rel="noreferrer"
      >
        <h3>{displayTitle || title}</h3>
      </a>
      {!hideSubtitle ? (
        <p className="quiver-feed-card__subtitle">{subtitle}</p>
      ) : null}
      <div className="quiver-feed-card__content__bottom">
        {category ? (
          <a
            className="quiver-feed-card__category"
            href={category.url}
            onClick={() => onClickTag(category)}
          >
            {category.name}
          </a>
        ) : null}
        {showTimestamp ? (
          <span className="quiver-feed-card__date">
            {formatFeedCardDate(createdAt)}
          </span>
        ) : null}
      </div>
    </div>
  </div>
);

FeedCard.propTypes = {
  category: PropTypes.shape({
    name: PropTypes.string,
    url: PropTypes.string,
  }),
  imageUrl: PropTypes.string,
  newWindow: PropTypes.bool,
  onClickLink: PropTypes.func.isRequired,
  onClickTag: PropTypes.func.isRequired,
  subtitle: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  displayTitle: PropTypes.string,
  permalink: PropTypes.string.isRequired,
  createdAt: PropTypes.number.isRequired,
  showTimestamp: PropTypes.bool,
  isPremium: PropTypes.bool,
  fullWidthContent: PropTypes.bool,
  hideSubtitle: PropTypes.bool,
};

FeedCard.defaultProps = {
  category: null,
  imageUrl: null,
  newWindow: false,
  displayTitle: null,
  showTimestamp: false,
  hideSubtitle: false,
  isPremium: false,
  fullWidthContent: false,
};

export default FeedCard;
