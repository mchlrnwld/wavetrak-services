import React from 'react';
import SwellEventCardContainer from './index';

const swellEvents = [
  {
    id: 2772,
    permalink:
      'https://sandbox.surfline.com/surf-news/swell-event/hurricane-marie/2772',
    updatedAt: '2019-06-19T19:31:09+00:00',
    name: 'Hurricane Marie',
    summary: 'This is the summary',
    thumbnailUrl:
      'https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2019/06/05122101/klein_hawaii_dec2014_04012.jpg',
    basins: ['north_pacific'],
  },
  {
    id: 2773,
    basins: ['north_pacific', 'south_atlantic', 'mediterranean', 'great_lakes'],
    name: 'European Winter Cruise',
    newWindow: false,
    permalink:
      'https://sandbox.surfline.com/surf-news/swell-event/hurricane-marie/2772',
    summary:
      'Realtime: Another XL swell unloads across Europe and at the Nazare Challenge',
    thumbnailUrl:
      'https://d14fqx6aetz9ka.cloudfront.net/wp-content/uploads/2022/02/10035848/Guincho_thumb.jpg',
    titleTag: 'Alert',
    updatedAt: '2022-01-19T19:31:09+00:00',
  },
  {
    id: 2774,
    permalink:
      'https://sandbox.surfline.com/surf-news/swell-event/hurricane-herman/2773',
    updatedAt: '2019-06-20T19:31:09+00:00',
    name: 'Watch now: North Pacific Kickoff',
    summary: 'We are live at Jaws, click here to watch.',
    thumbnailUrl:
      'https://d14fqx6aetz9ka.cloudfront.net/wp-content/uploads/2022/01/08091734/NazThumbCJ.jpg',
    titleTag: '20ft+',
    basins: ['north_atlantic', 'south_atlantic'],
    isLive: true,
  },
];

export default {
  title: 'SwellEventCardContainer',
};

export const Default = () => (
  <div>
    <SwellEventCardContainer events={swellEvents} />
  </div>
);

Default.story = {
  name: 'default',
};
