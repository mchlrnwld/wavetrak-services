import React from 'react';
import PropTypes from 'prop-types';
import SwellEventCard from '../SwellEventCard';

const SwellEventCardContainer = ({ events, onClickCard }) => (
  <div className="quiver-swell-event-card-container">
    {events.map((event) => (
      <SwellEventCard
        swellEvent={event}
        onClickCard={onClickCard}
        key={event.id}
      />
    ))}
  </div>
);

SwellEventCardContainer.propTypes = {
  events: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      permalink: PropTypes.string,
      name: PropTypes.string,
      newWindow: PropTypes.bool,
      summary: PropTypes.string,
      thumbnailUrl: PropTypes.string,
    })
  ),
  onClickCard: PropTypes.func,
};

SwellEventCardContainer.defaultProps = {
  events: [],
  onClickCard: () => {},
};

export default SwellEventCardContainer;
