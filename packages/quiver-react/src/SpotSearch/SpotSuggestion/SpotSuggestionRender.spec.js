import { expect } from 'chai';
import { shallow } from 'enzyme';
import {
  renderSectionTitle,
  renderSuggestion,
  renderInputComponent,
} from './SpotSuggestionRender';

describe('SpotSuggestionRender', () => {
  it('should render an autosuggestion with a highlight', () => {
    const suggestion = renderSuggestion('hunt')({
      _source: { cams: [], breadCrumbs: [], name: 'huntington' },
      _type: 'geoname',
    });
    const defaultWrapper = shallow(suggestion);
    expect(defaultWrapper.find('highlight').exists()).to.be.true();
  });

  it('should render mobile input component', () => {
    const input = renderInputComponent(true)({
      _source: { cams: [], breadCrumbs: [], name: 'huntington' },
      _type: 'geoname',
    });
    const defaultWrapper = shallow(input);
    expect(defaultWrapper.find('quiver-spot-search__react-autosuggest__container__wrapper-mobile-expanded').exists()).to.not.be.true(); // eslint-disable-line
  });

  it('should render section titles', () => {
    const spotSuggest = {
      hits: { total: 0, max_score: 0, hits: [] },
      suggest: { 'spot-suggest': [{ options: [1] }] },
    };

    const geonameSuggest = {
      hits: { total: 0, max_score: 0, hits: [] },
      suggest: { 'geoname-suggest': [{ options: [1] }] },
    };

    const emptySuggest = {
      hits: {},
      suggest: {},
    };

    expect(renderSectionTitle(spotSuggest)).to.equal('Surf Spots');
    expect(renderSectionTitle(geonameSuggest)).to.equal('Map Area');
    expect(renderSectionTitle(emptySuggest)).to.equal(null);
  });

  it('should not render section titles if suggest is false', () => {
    const spotSuggest = {
      hits: { total: 0, max_score: 0, hits: [] },
      suggest: false,
    };

    expect(renderSectionTitle(spotSuggest)).to.equal(null);
  });
});
