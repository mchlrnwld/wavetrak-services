export const getSuggestedSpots = async (serviceConfig, value) => {
  const querySize = 2;
  const suggestionSize = 2;
  const searchUrl = (
    namePrefix
  ) => `${serviceConfig.serviceUrl}/search/site?q=${namePrefix}\
          &querySize=${querySize}&suggestionSize=${suggestionSize}`;
  const response = await fetch(searchUrl(value), {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });
  const body = await response.json();
  if (response.status === 200) {
    return body;
  }
  return [];
};

export const getSectionSuggestions = (section) => {
  const {
    suggest,
    error,
    hits: { hits },
  } = section;

  if (error) {
    return [];
  }

  const suggestTypes = ['spot-suggest', 'subregion-suggest', 'geoname-suggest'];

  if (hits && hits.length > 0) {
    return hits;
  }
  const suggestTypeVal = suggestTypes.find((type) => suggest[type]);
  return suggestTypeVal ? suggest[suggestTypeVal][0].options : [];
};

export const getSuggestionValue = (suggestion) => {
  if (suggestion && suggestion.text) {
    return suggestion.text;
  }
  // eslint-disable-next-line no-underscore-dangle
  return suggestion._source.name;
};
