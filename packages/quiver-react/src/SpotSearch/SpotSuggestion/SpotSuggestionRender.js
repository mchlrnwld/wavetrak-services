/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import LocationIcon from '../icons/LocationIcon';
import CamIcon from '../icons/CamIcon';
import SearchIcon from '../icons/SearchIcon';
import BackIcon from '../icons/BackIcon';
import createAccessibleOnClick, {
  BTN,
} from '../../utils/createAccessibleOnClick';

/**
 * @deprecated This component does not seem to be used anywhere, marking as deprecated
 */
export const renderSuggestion = (searchTerm) => (suggestion) => {
  const {
    _source: { cams, breadCrumbs, name },
    _type,
  } = suggestion;

  const indexOfSearchTerm = name
    .toLowerCase()
    .indexOf(searchTerm.toLowerCase());
  let nameContent;
  if (indexOfSearchTerm > -1) {
    const searchTermLength = searchTerm.length;
    const resultTermLength = name.length;
    const endOfHighlight = indexOfSearchTerm + searchTermLength;
    nameContent = (
      <a>
        {name.substring(0, indexOfSearchTerm)}
        <highlight>
          {name.substring(indexOfSearchTerm, endOfHighlight)}
        </highlight>
        {name.substring(endOfHighlight, resultTermLength)}
      </a>
    );
  } else {
    nameContent = <a>{name}</a>;
  }

  const isMultiCam = cams.length > 1;
  let camClassName = '';
  let iconContent = '';
  const types = ['geoname', 'subregion', 'spot'];
  if (types.indexOf(_type) > -1) {
    if (cams && cams.length) {
      camClassName =
        'quiver-spot-search__react-autosuggest__container__icon__hd-cam';
      iconContent = <CamIcon isMultiCam={isMultiCam} />;
    } else {
      camClassName =
        'quiver-spot-search__react-autosuggest__container__icon__location';
      iconContent = <LocationIcon />;
    }
  }

  const breadcrumb = breadCrumbs ? breadCrumbs.join(' / ') : '';
  return (
    <div className={camClassName}>
      {iconContent} isMultiCam:
      {isMultiCam}
      {nameContent}
      <br />
      <div className="quiver-spot-search__react-autosuggest__container__subtitle">
        {breadcrumb}
      </div>
    </div>
  );
};

export const renderSectionTitle = (section) => {
  const {
    hits: { hits },
    suggest,
  } = section;

  const suggestTypes = new Map();
  suggestTypes.set('spot-suggest', 'Surf Spots');
  suggestTypes.set('subregion-suggest', 'Forecast');
  suggestTypes.set('geoname-suggest', 'Map Area');

  if (suggest) {
    const ret = Array.from(suggestTypes.entries()).find(
      (entry) =>
        suggest[entry[0]] &&
        (suggest[entry[0]][0].options.length > 0 || hits.length > 0)
    );
    if (ret) return ret[1];
  }
  return null;
};

/* eslint-disable max-len */
export const renderInputComponent =
  (isMobileExpanded, handleToggleSearchOffClick, handleToggleSearchOnClick) =>
  (inputProps) => {
    let mobileVersion;
    if (isMobileExpanded) {
      mobileVersion = (
        <div className="quiver-spot-search__react-autosuggest__container__wrapper-mobile-expanded">
          <div
            className="quiver-spot-search__react-autosuggest__container__wrapper-mobile-expanded__back"
            {...createAccessibleOnClick(handleToggleSearchOffClick, BTN)}
          >
            <BackIcon />
          </div>
          <input {...inputProps} />
          <div className="quiver-spot-search__react-autosuggest__container__wrapper-mobile-expanded__background" />
        </div>
      );
    } else {
      mobileVersion = (
        <div
          className="quiver-spot-search__react-autosuggest__container__wrapper-mobile-button"
          {...createAccessibleOnClick(handleToggleSearchOnClick, BTN)}
        >
          <SearchIcon />
        </div>
      );
    }

    return (
      <div className="quiver-spot-search__react-autosuggest__container__wrapper">
        {mobileVersion}
        <div className="quiver-spot-search__react-autosuggest__container__wrapper-desktop">
          <SearchIcon />
          <input {...inputProps} />
        </div>
      </div>
    );
  };
