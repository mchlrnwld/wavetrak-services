import React from 'react';
import SpotSearch from './index';

export default {
  title: 'SpotSearch',
};

export const Default = () => {
  const config = {
    serviceUrl: 'http://services.sandbox.surfline.com',
  };
  return <SpotSearch serviceConfig={config} />;
};

Default.story = {
  name: 'default',
};
