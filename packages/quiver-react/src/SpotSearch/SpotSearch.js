import Autosuggest from 'react-autosuggest';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { trackEvent } from '@surfline/web-common';
import serviceConfigurationPropType from '../PropTypes/serviceConfigurationPropType';
import {
  getSuggestionValue,
  getSectionSuggestions,
  getSuggestedSpots,
} from './SpotSuggestion/SpotSuggestionModel';
import {
  renderInputComponent,
  renderSectionTitle,
  renderSuggestion,
} from './SpotSuggestion/SpotSuggestionRender';

/**
 * Requires serviceConfig that fits contract for serviceUrl usage:
 * (namePrefix) => `${serviceConfig.serviceUrl}/search/spots?namePrefix=${namePrefix}`;
 */
export default class SpotSearch extends Component {
  constructor() {
    super();

    // Autosuggest is a controlled component.
    // This means that you need to provide an input value
    // and an onChange handler that updates this value (see below).
    // Suggestions also need to be provided to the Autosuggest,
    // and they are initially empty because the Autosuggest is closed.
    this.state = {
      value: '',
      suggestions: [],
      searchTerm: '',
      isMobileExpanded: false,
    };
  }

  theme = {
    container: 'quiver-spot-search__react-autosuggest__container',
    containerOpen: 'quiver-spot-search__react-autosuggest__container--open',
    input: 'quiver-spot-search__react-autosuggest__input',
    inputOpen: 'quiver-spot-search__react-autosuggest__input--open',
    inputFocused: 'quiver-spot-search__react-autosuggest__input--focused',
    suggestionsContainer:
      'quiver-spot-search__react-autosuggest__suggestions-container',
    suggestionsContainerOpen:
      'quiver-spot-search__react-autosuggest__suggestions-container--open',
    suggestionsList: 'quiver-spot-search__react-autosuggest__suggestions-list',
    suggestion: 'quiver-spot-search__react-autosuggest__suggestion',
    suggestionFirst: 'quiver-spot-search__react-autosuggest__suggestion--first',
    suggestionHighlighted:
      'quiver-spot-search__react-autosuggest__suggestion--highlighted',
    sectionContainer:
      'quiver-spot-search__react-autosuggest__section-container',
    sectionContainerFirst:
      'quiver-spot-search__react-autosuggest__section-container--first',
    sectionTitle: 'quiver-spot-search__react-autosuggest__section-title',
  };

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue,
    });
  };

  onSearchFieldFocus = () => {
    trackEvent('Focused Search', {
      location: 'navigation', // navigation | top of map | bottom of map
    });
  };

  // Autosuggest will call this function every time you need to update suggestions.
  onSuggestionsFetchRequested = async ({ value }) => {
    const { serviceConfig } = this.props;
    this.setState({
      suggestions: await getSuggestedSpots(serviceConfig, value),
      searchTerm: value,
    });
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  onSuggestionSelected = (event, value) => {
    const {
      suggestion: {
        name,
        _index,
        _id,
        _source: { href },
      },
      suggestionIndex,
    } = value;
    const { searchTerm } = this.state;
    trackEvent(
      'Clicked Search Results',
      {
        location: 'navigation', // navigation | top of map | bottom of map
        searchResultRank: suggestionIndex,
        destinationPage: _id, // This will be a URL after taxonomy API merged
        sourcePage: window.location.href,
        queryTerm: searchTerm,
        resultName: name,
        resultType: _index.indexOf('spot') > -1 ? 'spot' : 'location',
      },
      null,
      () => this.navigateToResult(href)
    );
  };

  navigateToResult = (href) => {
    window.location.href = href;
  };

  handleToggleSearchOnClick = () =>
    this.setState({
      isMobileExpanded: true,
    });

  handleToggleSearchOffClick = () =>
    this.setState({
      isMobileExpanded: false,
      value: '',
    });

  render() {
    const { value, suggestions, searchTerm, isMobileExpanded } = this.state;
    const { placeholder } = this.props;

    // Autosuggest will pass through all these props to the input element.
    const inputProps = {
      placeholder,
      value,
      onChange: this.onChange,
      onFocus: this.onSearchFieldFocus,
    };

    return (
      <Autosuggest
        theme={this.theme}
        multiSection
        highlightFirstSuggestion
        inputProps={inputProps}
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        onSuggestionSelected={this.onSuggestionSelected}
        getSectionSuggestions={getSectionSuggestions}
        getSuggestionValue={getSuggestionValue}
        renderInputComponent={renderInputComponent(
          isMobileExpanded,
          this.handleToggleSearchOffClick,
          this.handleToggleSearchOnClick
        )}
        renderSectionTitle={renderSectionTitle}
        renderSuggestion={renderSuggestion(searchTerm)}
      />
    );
  }
}

SpotSearch.propTypes = {
  serviceConfig: serviceConfigurationPropType.isRequired,
  placeholder: PropTypes.string,
};

SpotSearch.defaultProps = {
  placeholder: 'Find surf spots',
};
