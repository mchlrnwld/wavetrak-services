import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import common from '@surfline/web-common';
import SpotSearch from './SpotSearch';
import {
  renderSectionTitle,
  renderSuggestion,
} from './SpotSuggestion/SpotSuggestionRender';
import {
  getSuggestionValue,
  getSectionSuggestions,
} from './SpotSuggestion/SpotSuggestionModel';
import sampleResponse from './sampleData/sampleResponse.json';

describe('SpotSearch', () => {
  beforeEach(() => {
    sinon.stub(common, 'trackEvent');
  });

  afterEach(() => {
    common.trackEvent.restore();
  });

  it('should render an autosuggest container', () => {
    const config = {
      serviceUrl: 'http://localhost:8082',
    };
    const defaultWrapper = mount(<SpotSearch serviceConfig={config} />);
    expect(
      defaultWrapper.find('.quiver-spot-search__react-autosuggest__container')
    ).to.have.length(1);
  });

  /* eslint-disable no-underscore-dangle */
  it('should parse spot suggestions', () => {
    const suggestions = getSectionSuggestions(sampleResponse[0]);
    expect(suggestions[0]._type).to.equal('spot');
    expect(renderSectionTitle(sampleResponse[0])).to.equal('Surf Spots');
  });

  /* eslint-disable no-underscore-dangle */
  it('should render spot suggestions', () => {
    const suggestions = getSectionSuggestions(sampleResponse[0]);
    const renderedSuggestion = renderSuggestion('hunt')(suggestions[0]);
    expect(renderedSuggestion.type).to.equal('div');
  });

  /* eslint-disable no-underscore-dangle */
  it('should parse location suggestions', () => {
    const suggestions = getSectionSuggestions(sampleResponse[1]);
    expect(suggestions[0]._type).to.equal('geoname');
    expect(renderSectionTitle(sampleResponse[1])).to.equal('Map Area');
  });

  it('should parse suggestion value from text', () => {
    const suggestions = getSectionSuggestions(sampleResponse[0]);
    const [suggestion] = suggestions;
    expect(getSuggestionValue(suggestion)).to.equal(suggestion.text);
  });

  it('should send correct focused search field event', () => {
    const spotSearch = new SpotSearch();
    spotSearch.onSearchFieldFocus();
    expect(common.trackEvent).to.have.been.calledWithExactly('Focused Search', {
      location: 'navigation',
    });
  });

  it('should send correct clicked search result event', () => {
    const spotSearch = new SpotSearch();

    spotSearch.onSuggestionSelected(undefined, {
      suggestion: {
        name: '',
        _index: '',
        _id: '',
        _source: {
          href: '',
        },
      },
      suggestionIndex: 1,
    });
    expect(common.trackEvent).to.have.been.called('Clicked Search Results', {
      destinationPage: '',
      location: 'navigation',
      queryTerm: '',
      resultName: '',
      resultType: 'location',
      searchResultRank: 1,
      sourcePage: '',
    });
  });
});
