import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import BackIcon from './BackIcon';
import CamIcon from './CamIcon';
import LoadingIcon from './LoadingIcon';
import LocationIcon from './LocationIcon';
import SearchIcon from './SearchIcon';

describe('BackIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<BackIcon />);
    expect(wrapper).to.have.length(1);
  });
});
describe('LoadingIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<LoadingIcon />);
    expect(wrapper).to.have.length(1);
  });
});
describe('CamIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<CamIcon />);
    expect(wrapper).to.have.length(1);
  });
});
describe('LocationIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<LocationIcon />);
    expect(wrapper).to.have.length(1);
  });
});
describe('SearchIcon', () => {
  it('should render the icon', () => {
    const wrapper = shallow(<SearchIcon />);
    expect(wrapper).to.have.length(1);
  });
});
