import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import ErrorBoundary from '.';
import FiveHundred from '../SurflineErrors/FiveHundred';

describe('ErrorBoundary', () => {
  const render = () => <div className="render" />;
  window.newrelic = {
    noticeError: sinon.spy(),
  };

  beforeEach(() => {
    window.newrelic.noticeError.resetHistory();
  });

  it('should render the Error Boundary with Children', () => {
    const wrapper = mount(
      <ErrorBoundary>
        <div />
      </ErrorBoundary>
    );
    expect(wrapper.find('div')).to.have.length(1);
    expect(wrapper.state('hasError')).to.be.false();
  });

  it('should render the `render` prop if the component catches an error', () => {
    const wrapper = mount(
      <ErrorBoundary render={render}>
        <div />
      </ErrorBoundary>
    );
    expect(wrapper.find('.render')).to.have.length(0);

    wrapper.instance().componentDidCatch('testError', 'testErrorInfo');
    wrapper.update();
    expect(window.newrelic.noticeError).to.have.been.calledOnceWithExactly(
      'testError',
      {}
    );
    expect(wrapper.state('hasError')).to.be.true();
    expect(wrapper.find('.render')).to.have.length(1);
  });

  it('should render the `render` prop if the error prop is truthy', () => {
    const wrapper = mount(
      <ErrorBoundary render={render} error="some error">
        <div />
      </ErrorBoundary>
    );

    expect(wrapper.find('.render')).to.have.length(1);
    expect(window.newrelic.noticeError).to.have.not.been.called();
    expect(wrapper.state('hasError')).to.be.false();
  });

  it('should render the FiveHundred component if there is an error and no render prop', () => {
    const wrapper = mount(
      <ErrorBoundary>
        <div />
      </ErrorBoundary>
    );
    expect(wrapper.find('div')).to.have.length(1);

    wrapper.instance().componentDidCatch('testError', 'testErrorInfo');
    wrapper.update();
    expect(window.newrelic.noticeError).to.have.been.calledOnceWithExactly(
      'testError',
      {}
    );
    expect(wrapper.state('hasError')).to.be.true();
    expect(wrapper.find(FiveHundred)).to.have.length(1);
  });

  it('should add the properties to a newrelic noticeError call', () => {
    const sampleComponentStack =
      '\n    in BuoyCurrentReadings (at BuoyReport.js:75)\n    in BuoyReport (at withMetering.js:25)\n    in WrappedWithMetering (at BuoyPage.js:19)\n    in ErrorBoundary (at BuoyPageError.js:9)\n    in BuoysError (at BuoyPage.js:18)\n    in main (at BuoyPage.js:17)\n    in div (created by ContentContainer)\n    in ContentContainer (at BuoyPage.js:16)\n    in div (at BuoyPage.js:15)\n    in BuoyPage (created by LoadableComponent)\n    in LoadableComponent (at Routes.js:63)\n    in Route (at Routes.js:48)\n    in Switch (at Routes.js:46)\n    in div (at KBYGPageContainer.js:42)\n    in KBYGPageContainer (created by Connect(KBYGPageContainer))\n    in Connect(KBYGPageContainer) (at Routes.js:45)\n    in Route (at Routes.js:42)\n    in ErrorBoundary (at Routes.js:41)\n    in div (created by Context.Consumer)\n    in EmotionCssPropInternal (created by SkeletonTheme)\n    in SkeletonTheme (at Routes.js:40)\n    in StrictMode (at Routes.js:39)\n    in Routes (created by _class2)\n    in _class2 (created by HotExported_class2)\n    in AppContainer (created by HotExported_class2)\n    in HotExported_class2 (at client/index.js:47)\n    in Router (created by ConnectedRouter)\n    in ConnectedRouter (created by Context.Consumer)\n    in ConnectedRouterWithContext (created by Connect(ConnectedRouterWithContext))\n    in Connect(ConnectedRouterWithContext) (at client/index.js:46)\n    in Provider (at client/index.js:45)\n    in CookiesProvider (at client/index.js:44)';
    const wrapper = mount(
      <ErrorBoundary
        properties={{
          someProps: true,
          componentStack: sampleComponentStack,
        }}
      >
        <div />
      </ErrorBoundary>
    );

    wrapper.instance().componentDidCatch('testError', 'testErrorInfo');
    wrapper.update();

    expect(window.newrelic.noticeError).to.have.been.calledOnceWithExactly(
      'testError',
      {
        someProps: true,
        componentStack: sampleComponentStack,
      }
    );
  });
});
