import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FiveHundred from '../SurflineErrors/FiveHundred';

/**
 * @typedef {object} Props
 * @property {() => React.ReactNode} [render]
 * @property {Record<string, any>} [properties]
 * @property {Error | string | boolean} [error]
 */

/** @extends {Component<React.PropsWithChildren<Props>, { hasError: boolean }>} */
class ErrorBoundary extends Component {
  static getDerivedStateFromError() {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  /**
   * @description A React Error Boundary Component that handles catching any javascript errors that
   * break the React Component tree. Instead of breaking the React Component tree, this component
   * will intercept the error and render either a component from the `render` prop
   * or the `FiveHundred` error component provided in `quiver/react`
   */
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
    };
  }

  componentDidCatch(error, errorInfo) {
    const { hasError } = this.state;
    const { properties } = this.props;

    if (!hasError) this.setState({ hasError: true });

    // eslint-disable-next-line no-console
    console.error(error, errorInfo);
    if (window && window.newrelic) {
      // Include the component stack trace in the properties sent to NewRelic
      if (errorInfo?.componentStack) {
        // the propTypes definition requires this to be a string.
        // It is a string by default but we'll cast to be 100% sure in case different contexts aren't a string
        properties.componentStack = errorInfo.componentStack.toString();
      }

      // Disable the lint rule as this will be a global in prod.
      // We don't want to make this an eslint global however in order to prevent issues
      // eslint-disable-next-line no-undef
      window.newrelic.noticeError(error, properties);
    }
  }

  render() {
    const { children, render, error } = this.props;
    const { hasError } = this.state;

    if (hasError || error) {
      if (render) {
        return render();
      }
      return <FiveHundred />;
    }

    return children;
  }
}

ErrorBoundary.propTypes = {
  render: PropTypes.func,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
  properties: PropTypes.shape({
    componentStack: PropTypes.string,
  }),
  error: PropTypes.oneOf([
    PropTypes.string,
    PropTypes.bool,
    PropTypes.instanceOf(Error),
  ]),
};

ErrorBoundary.defaultProps = {
  children: null,
  render: null,
  properties: {},
  error: null,
};

export default ErrorBoundary;
