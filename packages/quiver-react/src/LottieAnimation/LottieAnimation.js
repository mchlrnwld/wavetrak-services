/* istanbul ignore file */

import React from 'react';
import PropTypes from 'prop-types';
import { canUseDOM } from '@surfline/web-common';
import classNames from 'classnames';

let Lottie;

if (canUseDOM) {
  // eslint-disable-next-line global-require
  Lottie = require('react-lottie').default;
}

const lottieClass = (className) =>
  classNames({
    'quiver-lottie-animation': true,
    [className]: !!className,
  });

const LottieAnimation = ({
  className,
  loop,
  autoplay,
  animationKey,
  clearCanvas,
  width,
  height,
}) => {
  const options = {
    loop,
    autoplay,
    path: `./animations/${animationKey}.json`,
    rendererSettings: {
      clearCanvas,
    },
  };
  return (
    <div className={lottieClass(className)}>
      {Lottie ? (
        <Lottie options={options} width={width} height={height} />
      ) : null}
    </div>
  );
};

LottieAnimation.propTypes = {
  loop: PropTypes.bool,
  autoplay: PropTypes.bool,
  animationKey: PropTypes.string.isRequired,
  clearCanvas: PropTypes.bool,
  className: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
};

LottieAnimation.defaultProps = {
  loop: true,
  autoplay: true,
  clearCanvas: true,
  className: '',
  width: 64,
  height: 64,
};

export default LottieAnimation;
