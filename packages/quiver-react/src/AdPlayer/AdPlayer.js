/** @prettier */
import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';
import CamUpsell from '../CamPlayer/CamUpsell';
import CamAdblockUpsell from '../CamPlayer/CamAdBlock';
import AspectRatioContent from '../AspectRatioContent';
import CountDownTimer from '../CountDownTimer';
import { setupPlayer } from './setupPlayer';

const UPSELL_DURATION = 15;

const camPlayerClassName = (settings) => {
  const { adRunning } = settings;
  return classNames({
    'quiver-ad-player': true,
    'quiver-ad-player--ad-not-running': !adRunning,
  });
};

/**
 * An AD Player Component that can be used to play an ad. The camera can stream afterwards as well,
 * however this component should be unmounted once the AD has completed since it is not
 * a full featured cam player. This can be achieved with the `onAdComplete` callback.
 * @param {Object} props
 */
const AdPlayer = ({
  adblock,
  onDetectedAdBlock,
  onAdComplete,
  onUpSellComplete,
  onPlayAd,
  companionHandler,
  adCountdown,
  camera,
  playerId,
  onClickUpSell,
  onClickAdBlock,
  funnelUrl,
  isPremium,
  qaFlag,
  viewType,
  advertisingIds,
  aspectRatio,
  showUpSell,
  adTag,
}) => {
  const playerRef = useRef();
  const [adCount, setAdCount] = useState(1);
  const [adCountdownTime, setAdCountdownTime] = useState();
  const [adRunning, setAdRunning] = useState(false);

  useEffect(() => {
    if (adblock && onDetectedAdBlock) {
      onDetectedAdBlock();
    }
  }, [adblock, onDetectedAdBlock]);

  useEffect(() => {
    if (!showUpSell) {
      setupPlayer({
        playerRef,
        playerId,
        camera,
        isPremium,
        aspectRatio,
        companionHandler,
        onAdComplete,
        adCount,
        qaFlag,
        viewType,
        advertisingIds,
        setAdRunning,
        setAdCount,
        setAdCountdownTime,
        adTag,
        onPlayAd,
      });
    }

    const player = playerRef.current;

    return () => {
      if (player && player.remove) {
        player.remove();
      }
    };
    // We only want to run `setupPlayer` when the AD player is mounted.
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (adblock && onDetectedAdBlock) {
    return (
      <CamAdblockUpsell
        href={funnelUrl}
        onClick={onClickAdBlock}
        onMount={onDetectedAdBlock}
      />
    );
  }

  // We shouldn't show ads for premium users
  if (isPremium) {
    onAdComplete(true);
    return null;
  }

  if (showUpSell) {
    return (
      <div>
        <CamUpsell href={funnelUrl} onClick={onClickUpSell} />
        <div className="quiver-cam-player__upsell-countdown">
          Next ad will start in{' '}
          <CountDownTimer
            countTime={UPSELL_DURATION}
            onCountdownComplete={onUpSellComplete}
          />{' '}
          seconds
        </div>
      </div>
    );
  }

  return (
    <div
      className={camPlayerClassName({
        adRunning,
      })}
    >
      {adCountdown ? (
        <div className="quiver-cam-player__countdown">
          {adCountdownTime ? (
            <span>
              This ad will end in
              {adCountdownTime} seconds
            </span>
          ) : null}
        </div>
      ) : null}
      <div className="quiver-ad-player__player">
        <AspectRatioContent>
          {/* Empty div with playerId must be set dangerously so that
                jwplayer doesn't override a React-managed DOM element
            */}
          <div
            dangerouslySetInnerHTML={{
              __html: `<div id="${playerId}"></div>`,
            }}
          />
        </AspectRatioContent>
      </div>
    </div>
  );
};

AdPlayer.defaultProps = {
  adCountdown: false,
  companionHandler: () => {},
  aspectRatio: '16:9',
  onClickAdBlock: null,
  onClickUpSell: null,
  onDetectedAdBlock: null,
  funnelUrl: null,
  advertisingIds: {
    spotId: null,
    subregionId: null,
  },
  isPremium: true,
  adblock: false,
  viewType: null,
  adTag: null,
  qaFlag: null,
  onAdComplete: () => {},
  showUpSell: false,
  onPlayAd: () => {},
  onUpSellComplete: () => {},
};

AdPlayer.propTypes = {
  adCountdown: PropTypes.bool,
  playerId: PropTypes.string.isRequired,
  camera: PropTypes.shape({
    streamUrl: PropTypes.string,
    stillUrl: PropTypes.string,
    status: PropTypes.shape(),
    isPrerecorded: PropTypes.bool,
    title: PropTypes.string,
  }).isRequired,
  onDetectedAdBlock: PropTypes.func,
  advertisingIds: PropTypes.shape({
    spotId: PropTypes.string,
    subregionId: PropTypes.string,
  }),
  aspectRatio: PropTypes.string,
  adblock: PropTypes.bool,
  isPremium: PropTypes.bool,
  funnelUrl: PropTypes.string,
  companionHandler: PropTypes.func,
  onClickAdBlock: PropTypes.func,
  onClickUpSell: PropTypes.func,
  viewType: PropTypes.string,
  adTag: PropTypes.shape(),
  qaFlag: PropTypes.string,
  onAdComplete: PropTypes.func,
  showUpSell: PropTypes.bool,
  onPlayAd: PropTypes.func,
  onUpSellComplete: PropTypes.func,
};

export default AdPlayer;
