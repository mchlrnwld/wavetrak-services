import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import * as setupPlayer from './setupPlayer';
import CamAdBlock from '../CamPlayer/CamAdBlock';
import AdPlayer from './AdPlayer';
import CamUpsell from '../CamPlayer/CamUpsell';

describe('AdPlayer', () => {
  beforeEach(() => {
    window.jwplayer = () => ({
      key: null,
      remove: () => true,
    });
    window.jwplayer.defaults = {
      key: 'qPY2++XQlQ4Radjz7uWKEPyvDMsjicsjAmBSzw==',
    };
  });

  afterEach(() => {
    window.jwplayer = () => null;
  });

  const props = {
    playerId: 'quiver-ad-player',
    onAdComplete: () => {},
    onPlayAd: () => {},
    companionHandler: () => {},
    adCountdown: false,
    camera: {
      streamUrl:
        'http://wowzaprod100-i.akamaihd.net/hls/live/254494/c33c37db/playlist.m3u8',
      stillUrl:
        'https://camstills.cdn-surfline.com/hbpierssovcam/latest_small_pixelated.png',
      status: {
        isDown: false,
        message: 'This camera is totally broken...',
      },
    },
    onClickUpSell: () => {},
    onClickAdBlock: () => {},
    funnelUrl: '/funnel',
    qaFlag: false,
    viewType: 'TEST',
    isPremium: false,
  };

  it('should render the CamPlayer component', () => {
    const wrapper = mount(<AdPlayer {...props} />);
    expect(wrapper.html()).to.contain('<div class="quiver-ad-player');
    expect(wrapper.html()).to.not.contain('<div class="quiver-cam-adblock');
  });

  it('should display an CamAdBlock component', () => {
    const newProps = {
      ...props,
      adblock: true,
      onDetectedAdBlock: sinon.spy(),
    };
    const wrapper = mount(<AdPlayer {...newProps} />);
    const camAdBlock = wrapper.find(CamAdBlock);
    expect(newProps.onDetectedAdBlock).to.have.been.calledOnce();
    expect(camAdBlock.html()).to.contain('<div class="quiver-cam-adblock"');
    expect(camAdBlock.html()).to.contain('Please disable your ad blocker');
  });

  it('should not render an Ad Player for premium users', () => {
    const newProps = {
      ...props,
      isPremium: true,
    };
    const wrapper = mount(<AdPlayer {...newProps} />);
    expect(wrapper.html()).to.be.null();
  });

  it('should render the CamUpsell', () => {
    const newProps = {
      ...props,
      showUpSell: true,
    };
    const wrapper = mount(<AdPlayer {...newProps} />);

    expect(wrapper.find(CamUpsell)).to.have.length(1);
  });

  it('should call setupPlayer', () => {
    const setupStub = sinon.stub(setupPlayer, 'setupPlayer');
    try {
      mount(<AdPlayer {...props} />);

      expect(setupStub).to.have.been.calledOnce();
    } finally {
      setupStub.restore();
    }
  });
});
