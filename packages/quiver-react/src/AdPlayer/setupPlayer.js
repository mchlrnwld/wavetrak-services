/* istanbul ignore file */

// eslint-disable-next-line no-unused-vars
import React from 'react';
import { Cookies } from 'react-cookie';

import { setupConfig } from './camConfig';
import { getAdTag } from './getAdTag';

/**
 * The setup player function used on mount of the Ad Player. This is a similar signature
 * to the `this.setupPlayer` function in the cam player component, but more barebones and
 * not fully featured as it is intended solely for Ad playing.
 * @param {{
 *   playerRef: React.ReactNode,
 *   playerId: String,
 *   camera: {},
 *   isPremium: Boolean,
 *   aspectRatio: String,
 *   companionHandler: Function,
 *   onAdComplete: Function,
 *   adCount: Number,
 *   qaFlag: Boolean,
 *   viewType: String,
 *   advertisingIds: {},
 *   setAdRunning: Function,
 * }}
 * @returns
 */
// eslint-disable-next-line import/prefer-default-export
export const setupPlayer = ({
  playerRef,
  playerId,
  camera,
  isPremium,
  aspectRatio,
  companionHandler,
  onAdComplete,
  qaFlag,
  viewType,
  advertisingIds,
  setAdRunning,
  setAdCount,
  setAdCountdownTime,
  adTag,
  onPlayAd,
}) => {
  let adCount = 1;
  /**
   * @description Using the playerRef, this function triggers an ad in the JWPlayer
   */
  const playAd = () => {
    const width = playerRef.current.getWidth();
    const height = playerRef.current.getHeight();
    const ad = getAdTag({
      width,
      height,
      adCount,
      qaFlag,
      viewType,
      adTag,
      advertisingIds,
    });
    if (ad) playerRef.current.playAd(ad);
    setAdRunning(true);
    onPlayAd(true);
  };

  if (window && window.jwplayer) {
    window.jwplayer.key = window.jwplayer.defaults.key;

    // eslint-disable-next-line no-param-reassign
    playerRef.current = window.jwplayer(playerId);

    if (!playerRef.current.setup) return;

    let file = camera.streamUrl;
    const cookie = new Cookies();
    const accessToken = cookie.get('access_token');
    if (accessToken) {
      file += `?accessToken=${accessToken}`;
    }

    const withCredentials = file.indexOf('authorization') > -1;
    /* istanbul ignore next */
    playerRef.current.setup({
      file,
      type: file.indexOf('mp4') > -1 ? 'mp4' : 'hls',
      withCredentials,
      image: camera.stillUrl,
      aspectratio: aspectRatio,
      ...setupConfig(true),
    });

    // fires preRoll Ad.
    if (!isPremium) {
      playerRef.current.on('beforePlay', () => {
        playAd();
      });
    }

    playerRef.current.on('adTime', (obj) => {
      setAdCountdownTime(Math.round(obj.duration - obj.position));
    });

    // executes adError waterfall.
    playerRef.current.on('adError', () => {
      adCount += 1;
      if (adCount <= 5) {
        playAd();
      } else if (onAdComplete) {
        onAdComplete(); // if an ad fails to load more than 5 times, play the camera.
      }
    });

    // sends companion ad styles and html up to parent
    if (companionHandler) {
      playerRef.current.on('adCompanions', (event) => {
        companionHandler(event.companions);
      });
    }

    // restarts the cam stream when ad finishes.
    playerRef.current.on('adComplete', () => {
      setAdCount(1);
      setAdRunning(false);
      if (onAdComplete) onAdComplete();
    });
  }
};
