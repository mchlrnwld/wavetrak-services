/* istanbul ignore file */
import productCDN from '@surfline/quiver-assets';

import { adTagConfig } from './camConfig';

const getStringFromParams = (params) =>
  Object.keys(params)
    .map((key) => `${key}=${params[key]}`)
    .join('&');

// eslint-disable-next-line import/prefer-default-export
export const getAdTag = ({
  width,
  height,
  adCount,
  qaFlag,
  viewType,
  adTag,
  advertisingIds,
}) => {
  if (adCount > 5) return null; // Stop trying to render an ad after 5th failure
  if (adCount > 4)
    return `${productCDN}/adtags/PreRoll_SLAdFreePrem_rincon_MVP_feb18.xml`;

  const adTagOptions = {
    ...adTagConfig,
    ...adTag,
    url: encodeURIComponent(
      `${window.location.protocol}//${window.location.hostname}`
    ),
    description_url: encodeURIComponent(window.location.href),
  };

  const customOptions = {
    spotid: advertisingIds.spotId,
    subregionId: advertisingIds.subregionId,
    viewtype: viewType || 'SPOT',
    qaFlag,
    embedUrl: window.location.href,
    usertype: 'free',
    count: adCount,
    width,
    height,
  };
  const customParams = encodeURIComponent(getStringFromParams(customOptions));
  return `https://pubads.g.doubleclick.net/gampad/ads?${getStringFromParams(
    adTagOptions
  )}&cust_params=${customParams}`;
};
