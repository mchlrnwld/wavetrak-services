import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _get from 'lodash/get';
import _includes from 'lodash/includes';
import classNames from 'classnames';
import { trackEvent } from '@surfline/web-common';
import BaseContentCard from '../BaseContentCard';
import ForecastArticleBody from '../ForecastArticleBody';

class ForecastArticleCard extends Component {
  constructor(props) {
    super(props);
    this.showAll = props.showFullArticle || this.checkShouldShowAll(props.body);
    this.state = {
      showMore: false,
      clickedVideoIds: [],
    };
  }

  componentDidMount() {
    const vidTags = document.getElementsByTagName('video');
    if (vidTags.length) {
      const videos = [...vidTags];
      videos.forEach((vid) => {
        this.applyVideoEvent(vid);
      });
    }
  }

  applyVideoEvent = (vid) => {
    const { clickedVideoIds } = this.state;
    const { segmentCategory } = this.props;
    const currVid = vid;
    const contentId = _get(currVid, 'offsetParent.id', null);
    const title = _get(currVid, 'attributes.alt.value', null);
    currVid.onplay = () => {
      if (!_includes(clickedVideoIds, contentId)) {
        trackEvent('Played Video', {
          title,
          contentId,
          category: segmentCategory,
        });
        this.setState((prevState) => ({
          clickedVideoIds: prevState.clickedVideoIds.concat([contentId]),
        }));
      }
    };
  };

  className = () => {
    const { premium, expert, showPaywall } = this.props;
    const { showMore } = this.state;
    return classNames({
      'quiver-forecast-article-card': true,
      'quiver-forecast-article-card--premium': premium,
      'quiver-forecast-article-card--expert': expert,
      'quiver-forecast-article-card--show-more': this.showAll || showMore,
      'quiver-forecast-article-card--paywall': showPaywall,
    });
  };

  meta = () => {
    const { author, premium, expert, publishedDateTime } = this.props;
    return `
      ${author ? `By ${author} • ` : ''}
      ${premium ? 'Premium ' : ''}
      ${expert ? 'Expert Forecast •' : ''}
      ${publishedDateTime}
    `;
  };

  toggleShowMore = () =>
    this.setState((prevState) => ({ showMore: !prevState.showMore }));

  // if there are less than 500 characters don't show "read more"
  // https://wavetrak.atlassian.net/wiki/spaces/IE/pages/130496072/Read+More+Truncating+Notes
  checkShouldShowAll = (content = '') => {
    const matchedImages = content ? content.match(/<img/gi) : [];
    if (matchedImages && matchedImages.length > 1) return false;

    const chCount = content ? content.replace(/(<([^>]+)>)/gi, '').length : 0;
    return chCount < 400;
  };

  render() {
    const {
      premium,
      id,
      title,
      body,
      tags,
      showPaywall,
      onClickReadMore,
      onClickShareArticle,
      funnelUrl,
      onClickPaywall,
      fbAppId,
      url,
      showShare,
    } = this.props;
    const { showMore } = this.state;
    return (
      <div className={this.className(premium)}>
        <BaseContentCard
          meta={this.meta()}
          tags={tags.map(({ name }) => ({ name }))}
          showPaywall={showPaywall}
        >
          <ForecastArticleBody
            id={id}
            title={title}
            body={body}
            showMore={showMore}
            fbAppId={fbAppId}
            url={url}
            showShare={showShare}
            onClickShareArticle={onClickShareArticle}
            showPaywall={showPaywall}
            onClickReadMore={onClickReadMore}
            funnelUrl={funnelUrl}
            onClickPaywall={onClickPaywall}
            showAll={this.showAll}
            toggleShowMore={this.toggleShowMore}
          />
        </BaseContentCard>
      </div>
    );
  }
}

ForecastArticleCard.propTypes = {
  author: PropTypes.string,
  publishedDateTime: PropTypes.string.isRequired,
  id: PropTypes.string,
  premium: PropTypes.bool,
  expert: PropTypes.bool,
  title: PropTypes.string.isRequired,
  body: PropTypes.string,
  tags: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
    })
  ).isRequired,
  showPaywall: PropTypes.bool,
  showFullArticle: PropTypes.bool,
  funnelUrl: PropTypes.string,
  onClickPaywall: PropTypes.func,
  onClickReadMore: PropTypes.func,
  onClickShareArticle: PropTypes.func,
  segmentCategory: PropTypes.string,
  fbAppId: PropTypes.string,
  url: PropTypes.string,
  showShare: PropTypes.bool,
};

ForecastArticleCard.defaultProps = {
  author: null,
  id: null,
  premium: false,
  body: null,
  showPaywall: false,
  showFullArticle: false,
  funnelUrl: null,
  onClickPaywall: null,
  onClickReadMore: null,
  onClickShareArticle: null,
  expert: false,
  segmentCategory: null,
  fbAppId: null,
  url: null,
  showShare: true,
};

export default ForecastArticleCard;
