/* eslint-disable max-len */
import React from 'react';
import { action } from '@storybook/addon-actions';
import ForecastArticleCard from './ForecastArticleCard';

const props = {
  publishedDateTime: '1 day ago',
  title: 'Fun SSW/S Southern Hemi Peaks in Orange County Today',
  body: `
    <p><strong>Today:</strong> SSW/S swell builds in and peaks in the PM, minor NW windswell
    blending in. Better breaks through the region are up into the waist-chest-shoulder high range, a
    few head high sets for summer focal points. Shape looks to be a bit more walled.</p><p><strong>
    Wind/Weather: _</strong> Variable to light onshore winds and coastal fog in the morning, giving
    way to light becoming moderate Westerly flow in the afternoon. </p><p><img
    src="https://images.contentful.com/kl4m5s78bekv/5uEoQI0lmo0ISsUKOAiOGy/8e1a28cfddcd82a1c0cf184638469b7e/OC.jpg"
    alt="OC"></p>
  `,
  tags: [
    { name: 'California' },
    { name: 'Orange County' },
    { name: 'Los Angeles County' },
  ],
  funnelUrl: '#',
  author: 'Kelly Slater',
  onClickPaywall: action('clicked paywall'),
};

export default {
  title: 'ForecastArticleCard',
};

export const Default = () => (
  <div style={{ width: '350px' }}>
    <ForecastArticleCard {...props} />
  </div>
);

Default.story = {
  name: 'default',
};

export const DefaultMoreContent = () => (
  <div style={{ width: '350px' }}>
    <ForecastArticleCard
      {...props}
      body={`
          <p><strong>Today:</strong> SSW/S swell builds in and peaks in the PM, minor NW windswell
          blending in. Better breaks through the region are up into the waist-chest-shoulder high range, a
          few head high sets for summer focal points. Shape looks to be a bit more walled.</p><p><strong>
          Wind/Weather: _</strong> Variable to light onshore winds and coastal fog in the morning, giving
          way to light becoming moderate Westerly flow in the afternoon. </p><p><img
          src="https://images.contentful.com/kl4m5s78bekv/5uEoQI0lmo0ISsUKOAiOGy/8e1a28cfddcd82a1c0cf184638469b7e/OC.jpg"
          alt="OC"></p>
          <video alt="Uppers" poster="https://images.contentful.com/kl4m5s78bekv/1Nxn5asu4EK0A2SKku8qE2/9957752c2c33d59389e4d6381784c676/Uppers.jpg" controls=""><source src="https://videos.contentful.com/kl4m5s78bekv/2kstp7bVfOuuKicCMQeKce/50820e2317507a99936e6b56d64048e9/Uppers.mp4" type="video/mp4"></video>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque consequat libero
          sed felis faucibus aliquam. Cras condimentum elit id commodo vulputate. Cras sed odio ligula.
          Nunc tristique eros ut quam gravida, eu rhoncus leo dignissim. In tincidunt sem erat, et ultrices
          turpis commodo finibus. Suspendisse potenti</p>
        `}
    />
  </div>
);

DefaultMoreContent.story = {
  name: 'default (more content)',
};

export const DefaultLessContent = () => (
  <div style={{ width: '350px' }}>
    <ForecastArticleCard
      {...props}
      body={`
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur varius, diam sit amet rhoncus
          condimentum, leo ex molestie massa, at dapibus elit lacus ut ex. In hac habitasse platea dictumst.
          Morbi ac convallis leo, et posuere ex. Nulla quis ligula lacus. Ut a leo massa. Duis nibh diam,
          iaculis nec aliquet vel, pretium a diam. Duis ac mauris et dui semper ornare. Class aptent
          taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec in gravida
          erat, at tristique lectus. In mattis tempor tristique. Fusce in augue quis massa commodo
          scelerisque. Sed eu volutpat sem. Sed sit amet cursus eros.</p>
        `}
    />
  </div>
);

DefaultLessContent.story = {
  name: 'default (less content)',
};

export const PremiumExpertWithAuthor = () => (
  <ForecastArticleCard author="Schaler Perry" premium {...props} expert />
);

PremiumExpertWithAuthor.story = {
  name: 'premium expert with author',
};

export const NonPremiumExpertWithAuthor = () => (
  <ForecastArticleCard
    author="Schaler Perry"
    {...props}
    expert
    premium={false}
  />
);

NonPremiumExpertWithAuthor.story = {
  name: 'non-premium expert with author',
};

export const PremiumUpsellPaywall = () => (
  <ForecastArticleCard {...props} premium expert showPaywall />
);

PremiumUpsellPaywall.story = {
  name: 'premium upsell paywall',
};

export const ShareArticleIcons = () => (
  <ForecastArticleCard
    {...props}
    premium
    expert
    showPaywall
    fbAppId="12345"
    url="https://www.surfline.com"
  />
);

ShareArticleIcons.story = {
  name: 'share article icons',
};

export const ShareArticleIconsHidden = () => (
  <ForecastArticleCard
    {...props}
    premium
    expert
    showPaywall
    fbAppId="12345"
    url="https://www.surfline.com"
    showShare={false}
  />
);

ShareArticleIconsHidden.story = {
  name: 'share article icons hidden',
};
