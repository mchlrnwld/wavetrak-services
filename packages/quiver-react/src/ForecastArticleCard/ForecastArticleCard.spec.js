import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';
import BaseContentCard from '../BaseContentCard';
import ContentCardPaywall from '../ContentCardPaywall';
import ForecastArticleCard from './ForecastArticleCard';

describe('ForecastArticleCard', () => {
  const props = {
    publishedDateTime: '1 day ago',
    title: 'Fun SSW/S Southern Hemi Peaks in Orange County Today',
    body: `
      <p><strong>Today:</strong> SSW/S swell builds in and peaks in the PM, minor NW windswell
      blending in. Better breaks through the region are up into the waist-chest-shoulder high range,
      a few head high sets for summer focal points. Shape looks to be a bit more walled.</p><p>
      <strong>Wind/Weather: _</strong> Variable to light onshore winds and coastal fog in the
      morning, giving way to light becoming moderate Westerly flow in the afternoon. </p><p><img
      src="https://images.contentful.com/kl4m5s78bekv/5uEoQI0lmo0ISsUKOAiOGy/8e1a28cfddcd82a1c0cf184638469b7e/OC.jpg"
      alt="OC"></p>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque consequat libero
      sed felis faucibus aliquam. Cras condimentum elit id commodo vulputate. Cras sed odio
      ligula. Nunc tristique eros ut quam gravida, eu rhoncus leo dignissim. In tincidunt sem era,
      et ultrices turpis commodo finibus. Suspendisse potenti</p>
    `,
    tags: [
      { name: 'California' },
      { name: 'Orange County' },
      { name: 'Los Angeles County' },
    ],
    showFullArticle: false,
    funnelUrl: '#',
    onClickPaywall: sinon.stub(),
    onClickReadMore: sinon.stub(),
  };

  it('should render content inside a BaseContentCard', () => {
    const wrapper = mount(<ForecastArticleCard {...props} />);
    const baseContentCard = wrapper.find(BaseContentCard);
    expect(baseContentCard.prop('meta'))
      .and.to.contain('1 day ago')
      .and.not.to.contain('Premium');
    expect(baseContentCard.prop('tags')).to.deep.equal(
      props.tags.map(({ name }) => ({ name }))
    );

    const children = baseContentCard.children();
    const title = children.find('h2');
    expect(title).to.have.length(1);
    expect(title.text()).to.equal(props.title);

    const body = children.find('.quiver-forecast-article-card__body');
    expect(body).to.have.length(1);
    expect(body.prop('dangerouslySetInnerHTML')).to.deep.equal({
      __html: props.body,
    });
  });

  it('should allow toggling read more/less content', () => {
    const wrapper = mount(<ForecastArticleCard {...props} />);
    const firstReadMoreLess = wrapper.find(
      '.quiver-forecast-article-card__read-more-less'
    );
    expect(firstReadMoreLess).to.have.length(1);
    expect(firstReadMoreLess.find('span').text()).to.equal('Read More');

    firstReadMoreLess.simulate('click');
    const secondReadMoreLess = wrapper.find(
      '.quiver-forecast-article-card__read-more-less'
    );
    expect(secondReadMoreLess).to.have.length(1);
    expect(secondReadMoreLess.find('span').text()).to.equal('Read Less');

    secondReadMoreLess.simulate('click');
    const thirdReadMoreLess = wrapper.find(
      '.quiver-forecast-article-card__read-more-less'
    );
    expect(thirdReadMoreLess).to.have.length(1);
    expect(thirdReadMoreLess.find('span').text()).to.equal('Read More');
  });

  it('should not show read more for shorter content lengths', () => {
    const wrapper = shallow(
      <ForecastArticleCard {...props} body="should not show read more" />
    );
    expect(wrapper.prop('className'))
      .not.to.contain('quiver-forecast-article-card--expert')
      .and.not.to.contain('quiver-forecast-article-card--premium');
  });

  it('should not show read more when showFullArticle is true', () => {
    const wrapper = shallow(<ForecastArticleCard {...props} showFullArticle />);
    expect(wrapper.prop('className'))
      .not.to.contain('quiver-forecast-article-card--expert')
      .and.not.to.contain('quiver-forecast-article-card--premium');
  });

  it('should show read more for shorter content lengths with 2+ images', () => {
    const wrapper = shallow(
      <ForecastArticleCard {...props} body="more <img /> and another <img />" />
    );
    expect(wrapper.prop('className'))
      .not.to.contain('quiver-forecast-article-card--expert')
      .and.not.to.contain('quiver-forecast-article-card--premium');
  });

  it('should show if content is premium', () => {
    const defaultWrapper = shallow(<ForecastArticleCard {...props} />);
    expect(defaultWrapper.prop('className')).not.to.contain(
      'quiver-forecast-article-card--premium'
    );
    const defaultBaseContentCard = defaultWrapper.find(BaseContentCard);
    expect(defaultBaseContentCard.prop('meta')).not.to.contain('Premium');

    const premiumWrapper = shallow(<ForecastArticleCard {...props} premium />);
    expect(premiumWrapper.prop('className')).to.contain(
      'quiver-forecast-article-card--premium'
    );
    const premiumBaseContentCard = premiumWrapper.find(BaseContentCard);
    expect(premiumBaseContentCard.prop('meta')).to.contain('Premium');
  });

  it('should show a default styled card', () => {
    const defaultWrapper = shallow(
      <ForecastArticleCard {...props} premim={false} expert={false} />
    );
    expect(defaultWrapper.prop('className'))
      .not.to.contain('quiver-forecast-article-card--expert')
      .not.to.contain('quiver-forecast-article-card--premium');
    const defaultBaseContentCard = defaultWrapper.find(BaseContentCard);
    expect(defaultBaseContentCard.prop('meta')).not.to.contain('Expert');
  });

  it('should render paywall instead of content if showPaywall is true with no read more', () => {
    const defaultWrapper = mount(<ForecastArticleCard {...props} />);
    const defaultBaseContentCard = defaultWrapper.find(BaseContentCard);
    const defaultChildren = defaultBaseContentCard.children();
    expect(defaultChildren.find(ContentCardPaywall)).to.have.length(0);
    expect(
      defaultChildren.find('.quiver-forecast-article-card__body')
    ).to.have.length(1);

    const showPaywallWrapper = mount(
      <ForecastArticleCard {...props} showPaywall />
    );
    const paywall = showPaywallWrapper.find(ContentCardPaywall);
    expect(paywall).to.have.length(1);
    expect(paywall.prop('funnelUrl')).to.equal(props.funnelUrl);
    expect(paywall.prop('onClick')).to.equal(props.onClickPaywall);
    const readMoreLess = showPaywallWrapper.find(
      '.quiver-forecast-article-card__read-more-less'
    );
    expect(readMoreLess).to.have.length(0);
  });
});
