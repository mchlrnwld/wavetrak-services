import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import { beforeEach, afterEach } from 'mocha';
import sinon from 'sinon';
import ContinuousGraphContainer from './ContinuousGraphContainer';
import * as refHook from '../hooks/useGraphContainerRef';
import * as columnHook from '../hooks/useActiveColumn';

describe('components / ContinuousGraphContainer', () => {
  /** @type {sinon.SinonStub} */
  let useGraphContainerRefStub;
  /** @type {sinon.SinonStub<Parameters<import('./useActiveColumn').useActiveColumn>, ReturnType<import('./useActiveColumn').useActiveColumn>>} */
  let useActiveColumnStub;
  beforeEach(() => {
    useGraphContainerRefStub = sinon.stub(refHook, 'useGraphContainerRef');
    useActiveColumnStub = sinon.stub(columnHook, 'useActiveColumn');
    useActiveColumnStub.returns({
      handleTouch: sinon.stub(),
      handleMouseMove: sinon.stub(),
      activeColumn: undefined,
      removeActiveColumn: sinon.stub(),
    });
  });
  afterEach(() => {
    useGraphContainerRefStub.restore();
    useActiveColumnStub.restore();
  });

  const secondsInAnHour = 3600;
  const secondsInADay = 86400;
  // 2021-01-01 08:00 GMT
  // 2020-12-31 23:00 PDT
  // 2021-01-01 00:00 PST
  const baseTimestamp = 1577865600;

  const createTimestamps = (numDays, columns, intervalHours = 1) => {
    const data = [...Array(numDays)].map((_, dayNum) =>
      [...Array(columns)].map((_, pointNum) => ({
        timestamp:
          baseTimestamp +
          dayNum * secondsInADay +
          pointNum * intervalHours * secondsInAnHour,
        // Simulate time shift off of DST after the first midnight point (12am, 1am, 3am)
        utcOffset: dayNum === 0 && pointNum === 0 ? -8 : -7,
      }))
    );
    return data.flat();
  };

  it('renders columns', () => {
    const wrapper = shallow(
      <ContinuousGraphContainer
        device={{ mobile: false, width: 1200 }}
        timestamps={createTimestamps(1, 2)}
      >
        <div className="graph-container__children" />
      </ContinuousGraphContainer>
    );

    const columns = wrapper.find('.quiver-continuous-graph-container__columns');
    expect(columns).to.have.length(1);
    expect(
      columns.find('.quiver-continuous-graph-container__column')
    ).to.have.length(2);
  });

  it('renders the timestamps using the UTC offset', () => {
    const wrapper = mount(
      <ContinuousGraphContainer
        device={{ mobile: false, width: 1200 }}
        timestamps={createTimestamps(1, 2, 6)}
        showTimeLabels
      >
        <div className="graph-container__children" />
      </ContinuousGraphContainer>
    );

    const times = wrapper.find(
      '.quiver-continuous-graph-container__graph-time__hour'
    );
    expect(times).to.have.length(2);
    expect(times.at(0).text()).to.be.equal('12am');
    expect(times.at(1).text()).to.be.equal('7am');
  });

  it('should render optimal scores', () => {
    const optimalScores = [2, 1];
    const wrapper = shallow(
      <ContinuousGraphContainer
        device={{ mobile: false, width: 1200 }}
        optimalScores={optimalScores}
        timestamps={createTimestamps(1, 2, 6)}
        showTimeLabels
      >
        <div className="graph-container__children" />
      </ContinuousGraphContainer>
    );

    const optimalScoreBlocks = wrapper.find(
      '.quiver-continuous-graph-container__column'
    );
    expect(optimalScoreBlocks).to.have.length(2);

    const goodOptimalScoreBlocks = wrapper.find(
      '.quiver-continuous-graph-container__column--good'
    );
    expect(goodOptimalScoreBlocks).to.have.length(1);

    const optimalBlocks = wrapper.find(
      '.quiver-continuous-graph-container__column--optimal'
    );
    expect(optimalBlocks).to.have.length(1);
  });

  it('selects an active column on mount for mobile surf', () => {
    const createRef = () => {};
    createRef.current = {
      getBoundingClientRect: () => ({
        left: 1,
        top: 1,
        right: 1,
        bottom: 1,
        width: 2,
        height: 2,
      }),
    };
    useGraphContainerRefStub.returns(createRef);

    useActiveColumnStub.restore();

    const wrapper = mount(
      <ContinuousGraphContainer
        highlightActiveColumn
        allowMobileTooltip
        device={{ mobile: true, width: 400 }}
        timestamps={createTimestamps(1, 24)}
        currentMarkerXPercentage={0}
      >
        <div className="graph-container__children" />
      </ContinuousGraphContainer>
    );

    wrapper.update();

    expect(
      wrapper
        .find('.quiver-continuous-graph-container__column-container')
        .at(0)
        .hasClass('quiver-continuous-graph-container__column-container--active')
    ).to.be.true();
  });

  it('selects an active column on mount for mobile swell', () => {
    const createRef = () => {};
    createRef.current = {
      getBoundingClientRect: () => ({
        left: 1,
        top: 1,
        right: 1,
        bottom: 1,
        width: 2,
        height: 2,
      }),
    };
    useGraphContainerRefStub.returns(createRef);

    useActiveColumnStub.restore();
    const wrapper = mount(
      <ContinuousGraphContainer
        highlightActiveColumn
        allowMobileTooltip
        device={{ mobile: true, width: 400 }}
        currentMarkerXPercentage={0.51}
        days={[0]}
        numDays={1}
        timestamps={createTimestamps(1, 24)}
      >
        <div className="graph-container__children" />
      </ContinuousGraphContainer>
    );

    wrapper.update();

    expect(
      wrapper
        .find('.quiver-continuous-graph-container__column-container')
        .at(12)
        .hasClass('quiver-continuous-graph-container__column-container--active')
    ).to.be.true();
  });

  it('selects the first column as active on mount for mobile swell if day 0 is not included', () => {
    const createRef = () => {};
    createRef.current = {
      getBoundingClientRect: () => ({
        left: 1,
        top: 1,
        right: 1,
        bottom: 1,
        width: 2,
        height: 2,
      }),
    };
    useGraphContainerRefStub.returns(createRef);

    useActiveColumnStub.restore();
    const wrapper = mount(
      <ContinuousGraphContainer
        highlightActiveColumn
        allowMobileTooltip
        device={{ mobile: true, width: 400 }}
        days={[1]}
        currentMarkerXPercentage={0.9}
        timestamps={createTimestamps(2, 24)}
      >
        <div className="graph-container__children" />
      </ContinuousGraphContainer>
    );

    wrapper.update();

    expect(
      wrapper
        .find('.quiver-continuous-graph-container__column-container')
        .at(0)
        .hasClass('quiver-continuous-graph-container__column-container--active')
    ).to.be.true();
  });

  it('highlights active column if highlightActiveColumn true', () => {
    const wrapper = mount(
      <ContinuousGraphContainer
        device={{ mobile: false, width: 1200 }}
        isOverlay
        highlightActiveColumn
        timestamps={createTimestamps(1, 24)}
      >
        <div className="graph-container__children" />
      </ContinuousGraphContainer>
    );

    const expectActiveColumnHighlighted = (columnList, activeColumn) => {
      columnList.forEach((column, index) => {
        if (index === activeColumn) {
          expect(column.prop('className')).to.contain(
            'quiver-continuous-graph-container__column-container--active'
          );
        } else {
          expect(column.prop('className')).to.deep.equal(
            'quiver-continuous-graph-container__column-container'
          );
        }
      });
    };

    let columnList = wrapper.find(
      '.quiver-continuous-graph-container__column-container'
    );
    expect(columnList).to.have.length(24);
    expectActiveColumnHighlighted(columnList, null);

    useActiveColumnStub.resetBehavior();
    useActiveColumnStub.returns({
      handleTouch: sinon.stub(),
      handleMouseMove: sinon.stub(),
      activeColumn: 10,
      removeActiveColumn: sinon.stub(),
    });

    // Force a re-render by changing props
    wrapper.setProps({ dummyProp: 'force-re-render' });

    columnList = wrapper.find(
      '.quiver-continuous-graph-container__column-container'
    );
    expectActiveColumnHighlighted(columnList, 10);
  });
});
