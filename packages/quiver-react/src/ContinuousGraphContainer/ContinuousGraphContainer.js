import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { TABLET_LARGE_WIDTH } from '@surfline/web-common';
import devicePropTypes from '../PropTypes/device';
import { useActiveColumn } from '../hooks/useActiveColumn';
import GraphTimestamp from '../GraphTimestamp';

const className = (showTimeLabels) =>
  classNames({
    'quiver-continuous-graph-container': true,
    'quiver-continuous-graph-container--overlay': true,
    'quiver-continuous-graph-container--graph-updates': true,
    'quiver-continuous-graph-container--time-labels': showTimeLabels,
  });

const columnClassName = (column, currentColumn, optimalScore) =>
  classNames({
    'quiver-continuous-graph-container__column': true,
    'quiver-continuous-graph-container__column--good': optimalScore === 1,
    'quiver-continuous-graph-container__column--optimal': optimalScore === 2,
  });

const columnContainerClassName = (column, activeColumn) => {
  const isActive = column === activeColumn;
  return classNames({
    'quiver-continuous-graph-container__column-container': true,
    'quiver-continuous-graph-container__column-container--active': isActive,
  });
};

/**
 * @description This component is used as a wrapper for Graph Components.
 * The previous version of the component had logic for the Day and Night shading
 * of the graphs. Since we have moved to continuous graphs though this logic has
 * been moved to `DayNightShading.js` as a standalone component.
 * This component handles creating the timestamps on the graphs and logic related
 * to setting active columns in the graph.
 *
 * @param {object} props
 * @param {number} [props.currentMarkerXPercentage]
 * @param {boolean} [props.allowMobileTooltip]
 * @param {number[]} props.days An array of numbers that describe the individual days the graph
 * container will be displaying (ex: `[2, 3, 4]` denotes a graph showing the 3rd, 4th, and 5th days)
 * @param {{ width: number }} props.device
 * @param {number} props.numColumns
 * @param {boolean} [props.highlightActiveColumn]
 * @param {React.ReactChild} [props.children]
 * @param {boolean} [props.showTimeLabels]
 * @param {number[]} [props.optimalScores]
 * @param {number} [props.numDays] The number of days that the graph container will contain
 * @param {boolean} [props.isHourly]
 * @param {{ timestamp: number, utcOffset: number }[]} [props.timestamps]
 * @returns {JSX.Element}
 */
const ContinuousGraphContainer = ({
  currentMarkerXPercentage,
  allowMobileTooltip,
  days,
  device,
  highlightActiveColumn,
  children,
  showTimeLabels,
  optimalScores,
  numDays,
  isHourly,
  timestamps,
}) => {
  const isMobile = device.width < TABLET_LARGE_WIDTH;
  const showMobileTooltip = isMobile && allowMobileTooltip;

  const totalColumnNum = timestamps.length;
  const columnsPerDay = totalColumnNum / numDays;

  const hasFirstDay = days.indexOf(0) !== -1;
  const currentColumn = hasFirstDay
    ? Math.floor(columnsPerDay * currentMarkerXPercentage)
    : null;

  const {
    handleTouch,
    handleMouseMove,
    activeColumn,
    removeActiveColumn,
    ref,
  } = useActiveColumn({
    highlightActiveColumn,
    showMobileTooltip,
    totalColumnNum,
    isHourly,
    currentColumn,
  });

  return (
    <div
      ref={ref}
      className={className(showTimeLabels)}
      onMouseMove={handleMouseMove}
      onMouseLeave={!showMobileTooltip ? removeActiveColumn : null}
      onTouchStart={handleTouch}
      onTouchMove={handleTouch}
      onTouchEnd={!showMobileTooltip ? removeActiveColumn : null}
    >
      <div className="quiver-continuous-graph-container__columns">
        {timestamps.map(({ timestamp, utcOffset }, columnNum) => {
          const optimalScore = optimalScores ? optimalScores[columnNum] : null;
          return (
            <div
              key={timestamp}
              className={columnContainerClassName(columnNum, activeColumn)}
            >
              <GraphTimestamp
                timestamp={timestamp}
                utcOffset={utcOffset}
                columnNumber={columnNum}
                currentColumn={currentColumn}
                activeColumn={activeColumn}
                showTimeLabels={showTimeLabels}
              />
              <div
                key={timestamp}
                className={columnClassName(
                  columnNum,
                  currentColumn,
                  optimalScore
                )}
              />
            </div>
          );
        })}
      </div>
      <div className="quiver-continuous-graph-container__canvas">
        {children}
      </div>
    </div>
  );
};

ContinuousGraphContainer.propTypes = {
  children: PropTypes.node.isRequired,
  device: devicePropTypes.isRequired,
  showTimeLabels: PropTypes.bool,
  highlightActiveColumn: PropTypes.bool,
  optimalScores: PropTypes.arrayOf(PropTypes.number),
  allowMobileTooltip: PropTypes.bool,
  isHourly: PropTypes.bool,
  currentMarkerXPercentage: PropTypes.number,
  days: PropTypes.arrayOf(PropTypes.number),
  numDays: PropTypes.number,
  timestamps: PropTypes.arrayOf(
    PropTypes.shape({
      timestamp: PropTypes.number,
      utcOffset: PropTypes.number,
    })
  ).isRequired,
};

ContinuousGraphContainer.defaultProps = {
  isHourly: false,
  showTimeLabels: false,
  highlightActiveColumn: false,
  optimalScores: null,
  allowMobileTooltip: false,
  days: [0],
  currentMarkerXPercentage: 0,
  numDays: 1,
};

export default ContinuousGraphContainer;
