import React from 'react';

import ContinuousGraphContainer from './ContinuousGraphContainer';

const secondsInAnHour = 3600;
const secondsInADay = 86400;
// 2021-01-01 08:00 GMT
// 2020-12-31 23:00 PDT
// 2021-01-01 00:00 PST
const baseTimestamp = 1577865600;

const createTimestampsStub = (numDays, columns, intervalHours = 1) => {
  const data = [...Array(numDays)].map((_, dayNum) =>
    [...Array(columns)].map((_, pointNum) => ({
      timestamp:
        baseTimestamp +
        dayNum * secondsInADay +
        pointNum * intervalHours * secondsInAnHour,
      // Simulate time shift off of DST after the first midnight point (12am, 1am, 3am)
      utcOffset: dayNum === 0 && pointNum === 0 ? -8 : -7,
    }))
  );
  return data.flat();
};

export default {
  title: 'ContinuousGraphContainer',
};

export const Default = () => (
  <div style={{ height: 300, width: 300, position: 'relative' }}>
    <ContinuousGraphContainer
      device={{ mobile: false, width: 1200 }}
      timestamps={createTimestampsStub(1, 24)}
    >
      <div className="graph-container__children" />
    </ContinuousGraphContainer>
  </div>
);

Default.story = {
  name: 'default',
};

export const WindGraph = () => (
  <div style={{ height: 300, width: 300, position: 'relative' }}>
    <ContinuousGraphContainer
      highlightActiveColumn
      isWindGraph
      device={{ mobile: true, width: 400 }}
      currentXMarkerPercentage={0}
      timestamps={createTimestampsStub(1, 24)}
    >
      <div className="graph-container__children" />
    </ContinuousGraphContainer>
  </div>
);

WindGraph.story = {
  name: 'wind graph',
};

export const SwellGraph = () => (
  <div style={{ height: 300, width: 300, position: 'relative' }}>
    <ContinuousGraphContainer
      highlightActiveColumn
      isSwellGraph
      device={{ mobile: true, width: 400 }}
      currentXMarkerPercentage={0}
      timestamps={createTimestampsStub(1, 24)}
    >
      <div className="graph-container__children" />
    </ContinuousGraphContainer>
  </div>
);

SwellGraph.story = {
  name: 'swell graph',
};
