import PropTypes from 'prop-types';
import React, { useMemo, useRef } from 'react';
import TrackableLink from '../TrackableLink';

import premiumAnalysisClickEventPropType from '../PropTypes/premiumAnalysisClickEvent';

const PremiumAnalysisLink = ({ link, clickEventProps }) => {
  const linkRef = useRef();
  const eventProperties = useMemo(
    () => ({
      linkName: 'Premium Analysis',
      linkUrl: link,
      ...clickEventProps,
    }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );
  return (
    <p className="quiver-premium-analysis-link">
      Check out
      <TrackableLink
        ref={linkRef}
        eventName="Clicked Link"
        eventProperties={eventProperties}
      >
        <a ref={linkRef} href={link}>
          {' Premium Analysis '}
        </a>
      </TrackableLink>
      for more details.
    </p>
  );
};

PremiumAnalysisLink.propTypes = {
  link: PropTypes.string.isRequired,
  clickEventProps: premiumAnalysisClickEventPropType.isRequired,
};

export default PremiumAnalysisLink;
