import React from 'react';
import PremiumAnalysisLink from './PremiumAnalysisLink';

export default {
  title: 'PremiumAnalysisLink',
};

export const Default = () => <PremiumAnalysisLink link="/" clickEvent={{}} />;
