import React from 'react';
import PropTypes from 'prop-types';

import BaseMenu from '../BaseMenu';
import Favorite from '../Favorite';
import GoogleDFP from '../GoogleDFP';
import loadAdConfig from '../helpers/adConfig';
import getAddFavoritesHref from '../helpers/getAddFavoritesHref';
import favoritesPropType from '../PropTypes/favoritesPropType';
import windowPropType from '../PropTypes/windowPropType';
import settingsPropType from '../PropTypes/settingsPropType';
import userPropType from '../PropTypes/userPropType';

const FavoritesMenu = ({ favorites, settings, user, window, onClickLink }) => (
  <BaseMenu>
    <div className="quiver-favorites-menu__links">
      <a
        className="quiver-favorites-menu__links-add"
        href={getAddFavoritesHref(window ? window.location.href : null, user)}
        onClick={onClickLink}
      >
        Add Favorites
      </a>
      {user && favorites.length ? (
        <a
          className="quiver-favorites-menu__links-edit"
          href="/account/favorites"
          onClick={onClickLink}
        >
          Edit Favorites
        </a>
      ) : null}
      <a className="quiver-favorites-menu__links-multicam" href="/surf-cams">
        Multi-Cam
      </a>
    </div>
    <div className="quiver-favorites-menu__favorites">
      {favorites.map((favorite) => (
        <Favorite
          key={favorite._id}
          favorite={favorite}
          settings={settings}
          onClickLink={onClickLink}
          large
          mobile
        />
      ))}
    </div>
    <div className="quiver-favorites-menu__ad">
      <GoogleDFP adConfig={loadAdConfig('favoritesHorizontal')} />
    </div>
  </BaseMenu>
);

FavoritesMenu.propTypes = {
  user: userPropType,
  favorites: favoritesPropType,
  settings: settingsPropType,
  onClickLink: PropTypes.func,
  window: windowPropType,
};

FavoritesMenu.defaultProps = {
  favorites: [],
  settings: null,
  user: null,
  onClickLink: null,
  window: null,
};

export default FavoritesMenu;
