import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import FavoritesMenu from './FavoritesMenu';

describe('FavoritesMenu', () => {
  const favorites = [
    {
      _id: 'abc123',
      name: 'Pipeline',
      conditions: { human: false, value: 'EPIC' },
      waveHeight: { min: 0, max: 0, occasional: 18 },
    },
  ];
  const user = {
    email: '',
    firstName: '',
    lastName: '',
    _id: '',
  };
  const settings = { units: 'FT' };
  it('should render favorite condition, name, height and units', () => {
    const wrapper = mount(
      <FavoritesMenu favorites={favorites} user={user} settings={settings} />
    );
    const conditionWrapper = wrapper.find('.quiver-surf-conditions');
    const nameWrapper = wrapper.find(
      '.quiver-favorite__item__wrapper__top-name'
    );
    const heightWrapper = wrapper.find(
      '.quiver-favorite__item__wrapper__bottom-height'
    );
    const unitWrapper = wrapper.find(
      '.quiver-favorite__item__wrapper__bottom-units'
    );

    expect(conditionWrapper).to.have.length(1);
    expect(nameWrapper).to.have.length(1);
    expect(heightWrapper).to.have.length(1);
    expect(unitWrapper).to.have.length(1);
  });
  it('should render add and edit favorites links when user', () => {
    const wrapper = shallow(
      <FavoritesMenu favorites={favorites} user={user} settings={settings} />
    );
    expect(wrapper.html()).to.contain('Add Favorites');
    expect(wrapper.html()).to.contain('Edit Favorites');
  });
  it('should render the multi cam link', () => {
    const wrapper = shallow(
      <FavoritesMenu favorites={favorites} user={user} settings={settings} />
    );
    expect(wrapper.html()).to.contain('Multi-Cam');
  });
  it('should not render edit favorites link when user', () => {
    const wrapper = shallow(
      <FavoritesMenu favorites={favorites} settings={settings} />
    );
    expect(wrapper.html()).to.contain('Add Favorites');
    expect(wrapper.html()).not.to.contain('Edit Favorites');
  });
});
