import withWindow from '../withWindow';
import FavoritesMenu from './FavoritesMenu';

export default withWindow(FavoritesMenu);
