import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ConditionDaySummary from './ConditionDaySummary';

describe('components / ConditionDaySummary', () => {
  const report = {
    condition: 1,
    occasionalHeight: 5,
    rating: 'GOOD',
    humanRelation: 'Waist to shoulder high',
    maxHeight: 6,
    minHeight: 3,
  };

  it('renders a single summary', () => {
    const props = {
      data: {
        timestamp: 0,
        forecaster: {},
        am: report,
        pm: report,
        observation: 'observation',
      },
      utcOffset: -10,
      units: { waveHeight: 'FT' },
      showAMPM: true,
    };
    const wrapper = shallow(<ConditionDaySummary {...props} />);
    const ampm = wrapper.find('.quiver-condition-day-summary__ampm');
    expect(ampm).to.have.length(1);
  });

  it('hides date and AMPM when showAMPM is false', () => {
    const props = {
      data: {
        timestamp: 0,
        forecaster: {},
        am: report,
        pm: report,
        observation: 'observation',
      },
      utcOffset: -10,
      units: { waveHeight: 'FT' },
    };
    const wrapper = shallow(<ConditionDaySummary {...props} />);
    const ampm = wrapper.find('.quiver-condition-day-summary__ampm');
    expect(ampm).to.have.length(0);
  });

  it('does not render an avatar if there is no forecaster', () => {
    const props = {
      data: {
        timestamp: 0,
        am: report,
        pm: report,
        observation: 'observation',
      },
      utcOffset: -10,
      units: { waveHeight: 'FT' },
    };
    const wrapper = shallow(<ConditionDaySummary {...props} />);
    const data = wrapper.find(
      '.quiver-condition-day-summary__observation-avatar'
    );
    expect(data).to.have.length(0);
  });
});
