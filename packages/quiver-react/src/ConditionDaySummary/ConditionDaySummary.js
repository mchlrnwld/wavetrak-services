import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import {
  occasionalUnits,
  conditionEnumToText,
  conditionClassModifier,
  trimHumanRelation,
} from '@surfline/web-common';

import SurfHeight from '../SurfHeight';
import conditionReportPropType from '../PropTypes/conditionReport';
import unitsPropType from '../PropTypes/units';
import getUnitType from '../helpers/getUnitType';

const conditionClassName = (condition) =>
  `quiver-condition-day-summary__condition quiver-condition-day-summary__condition--${conditionClassModifier(
    condition
  )}`;

const conditionBodyClassName = (obscured) =>
  classNames({
    'quiver-condition-day-summary__body': true,
    'quiver-condition-day-summary__body--blurred': obscured,
  });

const ConditionDaySummary = ({
  data,
  units,
  showAMPM,
  obscured,
  lastUpdate,
}) => (
  <div className="quiver-condition-day-summary">
    <div className={conditionBodyClassName(obscured)}>
      {showAMPM ? (
        <div className="quiver-condition-day-summary__ampm">
          <div className="quiver-condition-day-summary__conditions">
            <div className={conditionClassName(data.am.rating)}>
              {data.am.rating ? (
                <span className="quiver-condition-day-summary__condition__rating">
                  {conditionEnumToText(data.am.rating)}
                </span>
              ) : null}
              <span className="quiver-condition-day-summary__condition__ampm">
                AM
              </span>
            </div>
            <div className={conditionClassName(data.pm.rating)}>
              <span className="quiver-condition-day-summary__condition__ampm">
                PM
              </span>
              {data.pm.rating ? (
                <span className="quiver-condition-day-summary__condition__rating">
                  {conditionEnumToText(data.pm.rating)}
                </span>
              ) : null}
            </div>
          </div>
          <div className="quiver-condition-day-summary__wave">
            <div className="quiver-condition-day-summary__wave__ampm">
              <SurfHeight
                min={data.am.minHeight}
                max={data.am.maxHeight}
                units={units.waveHeight}
                plus={data.am.plus}
              />
              <div className="quiver-condition-day-summary__wave__ampm--human-relation">
                {trimHumanRelation(data.am.humanRelation)}
                {occasionalUnits(
                  data.am.occasionalHeight,
                  getUnitType(units.waveHeight)
                )}
              </div>
            </div>
            <div className="quiver-condition-day-summary__wave__ampm">
              <SurfHeight
                min={data.pm.minHeight}
                max={data.pm.maxHeight}
                units={units.waveHeight}
                plus={data.pm.plus}
              />
              <div className="quiver-condition-day-summary__wave__ampm--human-relation">
                {trimHumanRelation(data.pm.humanRelation)}
                {occasionalUnits(
                  data.pm.occasionalHeight,
                  getUnitType(units.waveHeight)
                )}
              </div>
            </div>
          </div>
        </div>
      ) : null}
      <div className="quiver-condition-day-summary__observation">
        {data.forecaster ? (
          <div className="quiver-condition-day-summary__observation-avatar">
            <img src={`${data.forecaster.avatar}&s=18`} alt="Surfline Avatar" />
            <div className="quiver-condition-day-summary__observation-avatar-tooltip">
              {`Updated ${lastUpdate} by ${data.forecaster.name}`}
            </div>
          </div>
        ) : null}
        <span className="quiver-condition-day-summary__observation-text">
          {data.observation}
        </span>
      </div>
    </div>
  </div>
);

ConditionDaySummary.propTypes = {
  data: PropTypes.shape({
    timestamp: PropTypes.number,
    forecaster: PropTypes.shape({
      name: PropTypes.string,
      iconUrl: PropTypes.string,
      avatar: PropTypes.string,
    }),
    am: conditionReportPropType,
    pm: conditionReportPropType,
    observation: PropTypes.string,
  }).isRequired,
  units: unitsPropType.isRequired,
  showAMPM: PropTypes.bool,
  obscured: PropTypes.bool.isRequired,
  lastUpdate: PropTypes.string,
};

ConditionDaySummary.defaultProps = {
  showAMPM: false,
  lastUpdate: null,
};

export default ConditionDaySummary;
