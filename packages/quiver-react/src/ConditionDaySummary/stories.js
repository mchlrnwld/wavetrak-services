import React from 'react';

import ConditionDaySummary from './ConditionDaySummary';

const report = {
  condition: 1,
  occasionalHeight: 5,
  rating: 'GOOD',
  humanRelation: 'Waist to shoulder high',
  maxHeight: 6,
  minHeight: 3,
};

const props = {
  data: {
    timestamp: 0,
    forecaster: {
      name: 'Jeremy',
      iconUrl:
        'http://gravatar.com/avatar/ea1e9a0c570c61d61dec3cf6ea26a85e?d=mm',
      avatar:
        'http://gravatar.com/avatar/ea1e9a0c570c61d61dec3cf6ea26a85e?d=mm',
    },
    am: report,
    pm: report,
    observation:
      'Small short period wind waves from the N. Use extreme caution. Gale warnign with dangerous seas. Winds from the WNW.',
  },
  utcOffset: -10,
  units: { waveHeight: 'FT' },
};

export default {
  title: 'ConditionDaySummary',
};

export const Default = () => <ConditionDaySummary {...props} />;

Default.story = {
  name: 'default',
};

export const ShowAmpm = () => <ConditionDaySummary {...props} showAMPM />;

ShowAmpm.story = {
  name: 'showAMPM',
};
