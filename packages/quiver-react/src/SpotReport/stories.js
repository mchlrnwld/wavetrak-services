import React from 'react';
import SpotReport from './SpotReport';

const associated = {
  advertising: {},
  units: {
    waveHeight: 'FT',
    tideHeight: 'FT',
    speed: 'MPH',
    windSpeed: 'MPH',
    distance: 'FT',
  },
  utcOffset: -8,
  weatherIconPath: 'https://wa.cdn-surfline.com/quiver/0.15.0/weathericons',
};

const waveHeight = {
  min: 1,
  max: 2,
  occasional: 3,
  plus: false,
  human: true,
  humanRelation: 'Chest to head high',
};

const wind = {
  speed: 10,
  direction: 200,
};

const tide = {
  previous: {
    height: 1,
    type: 'NORMAL',
    timestamp: Math.floor(new Date('2021/01/01 1:00:00') / 1000),
    utcOffset: -8,
  },
  current: {
    height: 1,
    type: 'HIGH',
    timestamp: Math.floor(+new Date('2021/01/01 10:00:00') / 1000),
    utcOffset: -8,
  },
  next: {
    height: 1,
    type: 'LOW',
    timestamp: Math.floor(+new Date('2021/01/01 16:00:00') / 1000),
    utcOffset: -8,
  },
};

const swells = [
  { height: 2, period: 16, direction: 120, index: 1 },
  { height: 2, period: 16, direction: 120, index: 2 },
];

const readings = {
  waterTemp: { min: 2, max: 2 },
  weather: { condition: 'CLEAR_NO_RAIN', temperature: 10 },
  tide,
  swells,
  wind,
  waveHeight,
  conditions: {
    human: true,
    sortableCondition: 1,
    value: 'VERY_GOOD',
  },
};

const spot = { name: 'HB Southside', _id: '123', cameras: [] };
const humanReport = {
  body: 'Some body for the forecast',
  forecaster: {
    name: 'Jeremy Monson',
    title: 'Software Engineer',
  },
  timestamp: Math.floor(+new Date('2021/01/01 11:00:00')),
};

export default {
  title: 'SpotReport',
  component: SpotReport,
};

const Template = (args) => <SpotReport {...args} />;

export const SpotReportStory = Template.bind({});

SpotReportStory.story = {
  name: 'SpotReport',
};

SpotReportStory.args = {
  associated,
  entitlements: [],
  note: 'Some forecaster note',
  units: associated.units,
  waveHeight,
  humanReport,
  readings,
  spot,
  userDetails: {},
  subregionAssociated: { abbrTimezone: 'America/Los_Angeles' },
};

export const SpotReportConditionsV2Story = Template.bind({});

SpotReportConditionsV2Story.story = {
  name: 'SpotReportConditionsV2',
};

SpotReportConditionsV2Story.args = {
  associated,
  entitlements: [],
  note: 'Some forecaster note',
  units: associated.units,
  waveHeight,
  humanReport,
  readings,
  spot,
  userDetails: {},
  subregionAssociated: { abbrTimezone: 'America/Los_Angeles' },
  conditionsV2: true,
};
