/* eslint-disable react/no-danger */

import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { postedDate, getTop3Swells } from '@surfline/web-common';

import ForecasterProfile from '../ForecasterProfile';
import ColoredConditionBar from '../ColoredConditionBar';
import ColoredConditionBarV2 from '../ColoredConditionBarV2';
/* KBYG-3028: temp hide PremiumAnalysisLink */
// import PremiumAnalysisLink from '../PremiumAnalysisLink';
import SpotForecastSummary from '../SpotForecastSummary';
import readingsPropType from '../PropTypes/readings';
import spotPropType from '../PropTypes/spot';
import spotAssociatedPropType from '../PropTypes/spotAssociated';
import userDetailsPropType from '../PropTypes/userDetails';

const getConditionValuesClassName = (humanReport) =>
  classNames({
    'quiver-spot-report__condition-values': true,
    'quiver-spot-report__condition-values--no-human-report': !humanReport,
    'quiver-spot-report__condition-values--with-report': humanReport,
  });

const spotModuleClass = (hasCam, isMultiCam) =>
  classNames({
    'quiver-spot-module': true,
    'quiver-spot-module--with-cam': hasCam,
    'quiver-spot-module--no-cam': !hasCam,
    'quiver-spot-module--multi-cam': isMultiCam,
  });

const SpotReport = ({
  adTargets,
  entitlements,
  spot,
  note,
  humanReport,
  readings,
  associated,
  subregionAssociated,
  userDetails,
  isMultiCam,
  baseUrl,
  dateFormat,
  conditionsV2,
}) => {
  const hasCam = spot.cameras.length > 0;
  return (
    <div className={spotModuleClass(hasCam, isMultiCam)}>
      <div className="quiver-spot-report">
        {readings.conditions.value &&
          (conditionsV2 ? (
            <ColoredConditionBarV2
              value={readings.conditions.value}
              expired={readings.conditions.expired}
              conditionsV2={conditionsV2}
            />
          ) : (
            <ColoredConditionBar
              value={readings.conditions.value}
              expired={readings.conditions.expired}
            />
          ))}

        <div className="quiver-spot-report__content-wrapper">
          <div className={getConditionValuesClassName(humanReport)}>
            <SpotForecastSummary
              adTargets={adTargets}
              entitlements={entitlements}
              units={associated.units}
              waveHeight={readings.waveHeight}
              reportExpired={readings.conditions.expired}
              tide={readings.tide}
              wind={readings.wind}
              swells={getTop3Swells(readings.swells)}
              lat={spot.lat}
              lon={spot.lon}
              associated={associated}
              readings={readings}
              spot={spot}
              userDetails={userDetails}
            />
          </div>
          {humanReport ? (
            <div className="quiver-spot-report__report-text">
              {humanReport ? (
                <ForecasterProfile
                  forecasterName={humanReport.forecaster.name}
                  forecasterTitle={humanReport.forecaster.title}
                  reportExpired={
                    readings.conditions.expired && readings.conditions.human
                  }
                  lastUpdate={postedDate(
                    humanReport.timestamp,
                    associated.utcOffset,
                    subregionAssociated.abbrTimezone,
                    true,
                    dateFormat
                  )}
                  url={humanReport.forecaster.iconUrl}
                  condition={readings.conditions.value}
                  expired={readings.conditions.expired}
                  baseUrl={baseUrl}
                />
              ) : null}
              {note ? (
                <div
                  className="quiver-spot-report__note"
                  dangerouslySetInnerHTML={{ __html: note }}
                />
              ) : null}
              <div dangerouslySetInnerHTML={{ __html: humanReport.body }} />
              {/* KBYG-3028: temp hide PremiumAnalysisLink */}
              {/* <PremiumAnalysisLink
                link={`${associated.subregionUrl}/premium-analysis`}
                clickEventProps={{
                  category: 'cams & reports',
                  linkLocation: 'spot report page',
                }}
              /> */}
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

SpotReport.defaultProps = {
  adTargets: [],
  note: null,
  humanReport: null,
  userDetails: null,
  isMultiCam: false,
  baseUrl: null,
  dateFormat: 'MDY',
  conditionsV2: false,
};

SpotReport.propTypes = {
  adTargets: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
  entitlements: PropTypes.arrayOf(PropTypes.string).isRequired,
  spot: spotPropType.isRequired,
  note: PropTypes.string,
  humanReport: PropTypes.shape({
    body: PropTypes.string,
    forecaster: PropTypes.shape({
      name: PropTypes.string,
      iconUrl: PropTypes.string,
      title: PropTypes.string,
    }),
    timestamp: PropTypes.number,
  }),
  readings: readingsPropType.isRequired,
  associated: spotAssociatedPropType.isRequired,
  subregionAssociated: PropTypes.shape({
    abbrTimezone: PropTypes.string,
  }).isRequired,
  userDetails: userDetailsPropType,
  isMultiCam: PropTypes.bool,
  baseUrl: PropTypes.string,
  dateFormat: PropTypes.oneOf(['MDY', 'DMY']),
  conditionsV2: PropTypes.bool,
};

export default SpotReport;
