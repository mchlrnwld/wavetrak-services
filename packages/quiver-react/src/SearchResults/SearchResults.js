import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class SearchResults extends Component {
  resultList = (results, style) =>
    results.map((result) => {
      const { key, primaryVal, secondaryVal, link } = result;
      const resultBody = (
        <div>
          <div
            className="quiver-search-results__description"
            style={style && style.description}
          >
            <div
              className="quiver-search-results__primary"
              style={style && style.primary}
            >
              {primaryVal}
            </div>
            <div
              className="quiver-search-results__secondary"
              style={style && style.secondary}
            >
              {secondaryVal}
            </div>
          </div>
          {result.badge ? (
            <div
              className="quiver-search-results__badge"
              style={style && style.badge}
            >
              {result.badge}
            </div>
          ) : null}
        </div>
      );

      return (
        <li key={key} style={style && style.li}>
          {link ? <a href={link}>{resultBody}</a> : resultBody}
        </li>
      );
    });

  render() {
    const { results, style } = this.props;
    return (
      <ul className="quiver-search-results" style={style}>
        {this.resultList(results, style)}
      </ul>
    );
  }
}

SearchResults.propTypes = {
  results: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string,
      link: PropTypes.string,
      primaryVal: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.element,
      ]),
      secondaryVal: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.element,
      ]),
    })
  ),
  style: PropTypes.shape({
    li: PropTypes.shape({}),
  }),
};

SearchResults.defaultProps = {
  results: [],
  style: {},
};
