import React from 'react';
import SearchResults from './index';

const styles = {
  badge: {
    border: '1px solid #42A5FC',
    fontFamily: 'Source Sans Pro, Helvetica, sans-serif',
    fontWeight: 'bold',
    float: 'right',
    backgroundColor: '#42A5FC',
    color: '#FFFFFF',
  },
};

const Badge = () => <div style={styles.badge}>Premium</div>;

export default {
  title: 'SearchResults',
};

export const ResultsWithBadges = () => {
  const vipBadgeStyle = {
    backgroundColor: '#003366',
    color: '#FFFFFF',
    paddingLeft: 5,
    paddingRight: 5,
  };
  return (
    <SearchResults
      results={[
        {
          primaryVal: 'Larry Fine',
          secondaryVal: 'larry@onionoil.com',
          badge: <Badge />,
        },
        {
          primaryVal: 'Moe Howard',
          secondaryVal: 'moe@cafecasbahbah.com',
        },
        {
          primaryVal: 'Curly Howard',
          secondaryVal: 'curly@costaplentehotel.com',
          badge: <div style={vipBadgeStyle}>VIP</div>,
        },
      ]}
    />
  );
};

ResultsWithBadges.story = {
  name: 'results with badges',
};

export const ResultsWithLinks = () => (
  <SearchResults
    results={[
      {
        primaryVal: 'Larry Fine',
        secondaryVal: 'larry@onionoil.com',
        link: '#larry',
        badge: <Badge />,
      },
      {
        primaryVal: 'Moe Howard',
        secondaryVal: 'moe@cafecasbahbah.com',
        link: '#moe',
        badge: <Badge />,
      },
      {
        primaryVal: 'Curly Howard',
        secondaryVal: 'curly@costaplentehotel.com',
        link: '#curly',
        badge: <Badge />,
      },
    ]}
  />
);

ResultsWithLinks.story = {
  name: 'results with links',
};

export const CustomStyles = () => (
  <SearchResults
    results={[
      {
        primaryVal: 'Larry Fine',
        secondaryVal: 'larry@onionoil.com',
        badge: <Badge />,
      },
      {
        primaryVal: 'Moe Howard',
        secondaryVal: 'moe@cafecasbahbah.com',
        badge: <Badge />,
      },
      {
        primaryVal: 'Curly Howard',
        secondaryVal: 'curly@costaplentehotel.com',
        badge: <Badge />,
      },
    ]}
    style={{
      li: {
        border: '1px solid red',
      },
    }}
  />
);

CustomStyles.story = {
  name: 'custom styles',
};
