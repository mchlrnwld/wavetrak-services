import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import SearchResults from './SearchResults';

describe('SearchResults', () => {
  it('should display search results', () => {
    const Badge = () => <div>Premium</div>;
    const results = [
      { primaryVal: 'Larry Fine', secondaryVal: 'larry@onionoil.com', key: 1 },
      {
        primaryVal: 'Moe Howard',
        secondaryVal: 'moe@cafecasbahbah.com',
        key: 2,
      },
      {
        primaryVal: 'Curly Howard',
        secondaryVal: 'curly@costaplentehotel.com',
        key: 3,
      },
    ];
    const wrapper = mount(
      <SearchResults results={results} badge={<Badge />} />
    );
    expect(wrapper.prop('badge')).to.not.equal(undefined);
  });

  it('should pass custom style object into children', () => {
    const style = {
      fontFamily: 'container',
      li: {
        fontFamily: 'li',
      },
      description: {
        fontFamily: 'description',
      },
      primary: {
        fontFamily: 'primary',
      },
      secondary: {
        fontFamily: 'secondary',
      },
      badge: {
        fontFamily: 'badge',
      },
    };
    const results = [
      {
        primaryVal: 'Larry Fine',
        secondaryVal: 'larry@onionoil.com',
        badge: 'Premium',
        key: 1,
      },
      {
        primaryVal: 'Moe Howard',
        secondaryVal: 'moe@cafecasbahbah.com',
        key: 2,
      },
      {
        primaryVal: 'Curly Howard',
        secondaryVal: 'curly@costaplentehotel.com',
        badge: 'VIP',
        key: 3,
      },
    ];
    const wrapper = shallow(<SearchResults results={results} style={style} />);
    const containerWrapper = wrapper.find('ul');
    const liWrappers = containerWrapper.find('li');
    const descriptionWrappers = containerWrapper.find(
      '.quiver-search-results__description'
    );
    const primaryWrappers = containerWrapper.find(
      '.quiver-search-results__primary'
    );
    const secondaryWrappers = containerWrapper.find(
      '.quiver-search-results__secondary'
    );
    const badgeWrappers = containerWrapper.find(
      '.quiver-search-results__badge'
    );

    expect(liWrappers).to.have.length(3);
    expect(liWrappers.at(0).prop('style')).to.equal(style.li);
    expect(liWrappers.at(1).prop('style')).to.equal(style.li);
    expect(liWrappers.at(2).prop('style')).to.equal(style.li);

    expect(descriptionWrappers).to.have.length(3);
    expect(descriptionWrappers.at(0).prop('style')).to.equal(style.description);
    expect(descriptionWrappers.at(1).prop('style')).to.equal(style.description);
    expect(descriptionWrappers.at(2).prop('style')).to.equal(style.description);

    expect(primaryWrappers).to.have.length(3);
    expect(primaryWrappers.at(0).prop('style')).to.equal(style.primary);
    expect(primaryWrappers.at(1).prop('style')).to.equal(style.primary);
    expect(primaryWrappers.at(2).prop('style')).to.equal(style.primary);

    expect(secondaryWrappers).to.have.length(3);
    expect(secondaryWrappers.at(0).prop('style')).to.equal(style.secondary);
    expect(secondaryWrappers.at(1).prop('style')).to.equal(style.secondary);
    expect(secondaryWrappers.at(2).prop('style')).to.equal(style.secondary);

    expect(badgeWrappers).to.have.length(2);
    expect(badgeWrappers.at(0).prop('style')).to.equal(style.badge);
    expect(badgeWrappers.at(0).text()).to.equal('Premium');
    expect(badgeWrappers.at(1).prop('style')).to.equal(style.badge);
    expect(badgeWrappers.at(1).text()).to.equal('VIP');
  });
});
