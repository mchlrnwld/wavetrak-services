import React from 'react';
import PropTypes from 'prop-types';

import BaseMenuItem from '../BaseMenuItem';
import BaseMenuDivider from '../BaseMenuDivider';

const BaseMenuSection = ({ links, children, onClick }) => (
  <div className="quiver-base-menu-section">
    {links
      ? links.map((link) => (
          <BaseMenuItem
            key={`${link.display}-${link.href}`}
            href={link.href}
            newWindow={link.newWindow}
            onClick={() =>
              onClick(
                {
                  destinationUrl: link.href,
                  linkName: link.display,
                  completion: true,
                },
                true
              )
            }
          >
            {link.display}
          </BaseMenuItem>
        ))
      : null}
    {children}
    <BaseMenuDivider />
  </div>
);

BaseMenuSection.propTypes = {
  links: PropTypes.arrayOf(
    PropTypes.shape({
      display: PropTypes.string,
      href: PropTypes.string,
    })
  ),
  children: PropTypes.node,
  onClick: PropTypes.func,
};

BaseMenuSection.defaultProps = {
  children: null,
  onClick: null,
  links: [],
};

export default BaseMenuSection;
