import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import BaseMenuSection from './BaseMenuSection';
import BaseMenuItem from '../BaseMenuItem';

const links = [
  { display: '1', href: '1' },
  { display: '2', href: '2' },
  { display: '3', href: '3' },
  { display: '4', href: '4' },
];

describe('BaseMenuSection', () => {
  it('should render a BaseMenuItem for every link passed', () => {
    const wrapper = shallow(<BaseMenuSection links={links} />);
    expect(wrapper.find(BaseMenuItem)).to.have.length(links.length);
  });

  it('should not render BaseMenuItems when not passed links', () => {
    const wrapper = shallow(<BaseMenuSection />);
    expect(wrapper.find(BaseMenuItem)).to.have.length(0);
  });

  it('should render a div with children', () => {
    const wrapper = shallow(
      <BaseMenuSection>
        <p>For Testing</p>
      </BaseMenuSection>
    );
    expect(wrapper.containsMatchingElement(<p>For Testing</p>)).to.equal(true);
  });
});
