import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SurfHeight from '../SurfHeight';
import SpotReportSummary from './SpotReportSummary';

describe('components / SpotReportSummary', () => {
  it('adds no-tide class modifier if tide is not provided', () => {
    const noTideWrapper = shallow(
      <SpotReportSummary waveHeight={{}} wind={{}} units={{}} readings={{}} />
    );
    expect(noTideWrapper.prop('className')).to.equal(
      'quiver-spot-report-summary quiver-spot-report-summary--no-tide'
    );

    const tideWrapper = shallow(
      <SpotReportSummary tide={{}} waveHeight={{}} wind={{}} units={{}} />
    );
    expect(tideWrapper.prop('className')).to.equal(
      'quiver-spot-report-summary'
    );
  });

  it('renders a SurfHeight component', () => {
    const waveHeight = {
      min: 2,
      max: 3,
    };
    const units = { waveHeight: 'FT' };
    const waveHeightWrapper = shallow(
      <SpotReportSummary waveHeight={waveHeight} wind={{}} units={units} />
    );
    expect(waveHeightWrapper.find(SurfHeight)).to.exist();
    expect(waveHeightWrapper.find(SurfHeight)).to.exist();
  });

  it('renders tide if provided', () => {
    const noTideWrapper = shallow(
      <SpotReportSummary waveHeight={{}} wind={{}} units={{}} />
    );
    const noTideStats = noTideWrapper.find('.quiver-spot-report-summary__stat');
    expect(noTideStats).to.have.length(2);

    const tide = {
      previous: {
        type: 'LOW',
        height: 0.7,
        timestamp: 1499643689,
        utcOffset: -7,
      },
      current: {
        type: 'NORMAL',
        height: 0.7,
        timestamp: 1499646797,
        utcOffset: -7,
      },
      next: {
        type: 'HIGH',
        height: 1.7,
        timestamp: 1499665931,
        utcOffset: -7,
      },
    };
    const units = { tideHeight: 'FT' };
    const tideWrapper = shallow(
      <SpotReportSummary tide={tide} units={units} waveHeight={{}} wind={{}} />
    );
    const tideStats = tideWrapper.find('.quiver-spot-report-summary__stat');
    expect(tideStats).to.have.length(3);
    const tideStat = tideStats.at(1);
    expect(tideStat.text()).to.include('TIDE (FT)');
    expect(tideStat.text()).to.include('.7');
  });

  it('renders wind Reading, WindArrow, and WindSummary with wind and units', () => {
    const wind = {
      direction: 280,
      speed: 4,
    };
    const units = { windSpeed: 'KTS' };
    const wrapper = shallow(
      <SpotReportSummary wind={wind} units={units} waveHeight={{}} />
    );
    expect(wrapper.text()).to.include('WIND (KTS)');
    expect(wrapper.text()).to.include('4 W');
  });
});
