import React from 'react';
import classNames from 'classnames';
import { mapDegreesCardinal } from '@surfline/web-common';
import SurfHeight from '../SurfHeight';
import TideArrow from '../TideArrow';
import unitsPropType from '../PropTypes/units';
import {
  tidePropTypes,
  windPropTypes,
  waveHeightPropTypes,
} from '../PropTypes/readings';
import getUnitType from '../helpers/getUnitType';

const summaryClass = (tide) =>
  classNames({
    'quiver-spot-report-summary': true,
    'quiver-spot-report-summary--no-tide': !tide,
  });

const SpotReportSummary = ({ waveHeight, tide, wind, units }) => (
  <div className={summaryClass(tide)}>
    <div className="quiver-spot-report-summary__stat">
      <div className="quiver-spot-report-summary__label">
        SURF ({getUnitType(units.waveHeight)})
      </div>
      <div className="quiver-spot-report-summary__value">
        <SurfHeight
          min={waveHeight.min}
          max={waveHeight.max}
          plus={waveHeight.plus}
          units={units.waveHeight}
        />
      </div>
    </div>
    {tide && tide.current && tide.next ? (
      <div className="quiver-spot-report-summary__stat">
        <div className="quiver-spot-report-summary__label">
          TIDE ({units.tideHeight})
        </div>
        <div className="quiver-spot-report-summary__value">
          {tide.current.height}
          <TideArrow direction={tide.next.type === 'LOW' ? 'DOWN' : 'UP'} />
        </div>
      </div>
    ) : null}
    <div className="quiver-spot-report-summary__stat">
      <div className="quiver-spot-report-summary__label">
        WIND ({units.windSpeed})
      </div>
      <div className="quiver-spot-report-summary__value">
        {wind.speed} {mapDegreesCardinal(wind.direction)}
      </div>
    </div>
  </div>
);

SpotReportSummary.propTypes = {
  units: unitsPropType.isRequired,
  waveHeight: waveHeightPropTypes.isRequired,
  tide: tidePropTypes,
  wind: windPropTypes.isRequired,
};

SpotReportSummary.defaultProps = {
  tide: null,
};

export default SpotReportSummary;
