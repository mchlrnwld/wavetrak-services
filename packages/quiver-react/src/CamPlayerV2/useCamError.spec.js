import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { useCamError } from './useCamError';

describe('useCamError', () => {
  let registeredHandlers = {};
  const camPlayer = {
    on: (eventName, handler) => {
      if (registeredHandlers[eventName]) {
        return registeredHandlers[eventName].push(handler);
      }
      registeredHandlers[eventName] = [handler];
      return null;
    },
  };

  const setupPlayerStub = sinon.stub();

  afterEach(() => {
    registeredHandlers = {};
    setupPlayerStub.resetHistory();
  });

  const Component = () => {
    const { networkError } = useCamError(
      camPlayer,
      '',
      '',
      '',
      setupPlayerStub,
      'Cam Rewind'
    );

    return (
      <div className="div">
        <div className="down">{`${networkError.isDown}`}</div>
        <div className="message">{networkError.message}</div>
      </div>
    );
  };

  it('should set the listener callbacks', () => {
    mount(<Component />);
    expect(registeredHandlers.error).to.have.length(1);
  });

  it('should handle a network error', () => {
    const wrapper = mount(<Component />);

    expect(wrapper.find('.down').text()).to.be.equal('false');
    expect(wrapper.find('.message').text()).to.be.equal('');
    registeredHandlers.error[0]({ code: 1000, message: 'some message' });

    wrapper.update();
    expect(wrapper.find('.down').text()).to.be.equal('true');
    expect(wrapper.find('.message').text()).to.contain(
      `We're sorry to report that this camera is down`
    );
  });

  it('should handle a cam rewind network error', () => {
    const wrapper = mount(<Component />);

    expect(wrapper.find('.down').text()).to.be.equal('false');
    expect(wrapper.find('.message').text()).to.be.equal('');
    registeredHandlers.error[0]({ code: 224002, message: 'some message' });

    wrapper.update();
    expect(wrapper.find('.down').text()).to.be.equal('true');
    expect(wrapper.find('.message').text()).to.contain(
      `Unfortunately, we are having an issue loading this Cam Rewind.`
    );
  });

  it('should handle refreshing on error', async () => {
    const wrapper = mount(<Component />);

    registeredHandlers.error[0]({ code: 232404, message: 'some message' });
    registeredHandlers.error[0]({ code: 404, message: 'some 404 message' });

    const wait = () => new Promise((resolve) => setTimeout(resolve, 1001));
    await wait();

    wrapper.update();
    expect(wrapper.find('.down').text()).to.be.equal('true');
    expect(wrapper.find('.message').text()).to.be.equal(
      `Refreshing camera stream`
    );
    expect(setupPlayerStub).to.have.been.calledTwice();
  });
});
