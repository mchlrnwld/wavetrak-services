import { useEffect, useState } from 'react';

const PREMIUM_TIMEOUT = 600; // 10 minute premium timeout

/**
 *
 * @param {?import('.').CamPlayer} camPlayer
 * @param {import('./CamPlayerV2').CamPlayerProps['isMultiCam']} isMultiCam
 * @param {import('./CamPlayerV2').CamPlayerProps['isPrimaryCam']} isPrimaryCam
 * @param {import('./CamPlayerV2').CamPlayerProps['timeoutMultiCams']} timeoutMultiCams
 * @param {() => void} pausePlayer
 * @param {(isTimeout: boolean) => void} trackPause
 */
export const useCamTimeout = (
  camPlayer,
  isMultiCam,
  isPrimaryCam,
  timeoutMultiCams,
  pausePlayer,
  trackPause
) => {
  const [showTimeout, setShowTimeout] = useState(false);

  useEffect(() => {
    let timeElapsed = 0;
    let currentTime = 0;

    camPlayer?.on('time', (event) => {
      const playerPosition = Math.round(event.position);

      /**
       * Because this Event is fired multiple times (<=10) a second,
       * this conditional is required for tracking total seconds played.
       */
      if (playerPosition !== currentTime) {
        currentTime = playerPosition;
        timeElapsed += 1;
      }

      if (timeElapsed >= PREMIUM_TIMEOUT) {
        if (!isMultiCam) {
          setShowTimeout(true);
          pausePlayer();
        } else {
          trackPause(true);
          if (isPrimaryCam && timeoutMultiCams) {
            timeoutMultiCams();
          }
        }
        timeElapsed = 0;
      }
    });
  }, [
    camPlayer,
    isMultiCam,
    isPrimaryCam,
    timeoutMultiCams,
    pausePlayer,
    trackPause,
  ]);

  return [showTimeout, setShowTimeout];
};
