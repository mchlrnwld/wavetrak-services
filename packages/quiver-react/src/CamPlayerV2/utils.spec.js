import { expect } from 'chai';
import sinon from 'sinon';
import {
  addCamRewindButton,
  ALT,
  getRewindUrl,
  messageConfig,
  SUB,
} from './utils';

describe('utils', () => {
  describe('addCamRewindButton', () => {
    const { location } = window;

    const camPlayer = {
      addButton: sinon.stub().callsArg(2),
    };

    const pausePlayer = sinon.stub();
    const resumePlayer = sinon.stub();

    before(() => {
      delete window.location;
      window.location = {
        assign: sinon.spy(),
      };
    });

    after(() => {
      window.location = location;
    });

    afterEach(() => {
      window.location.assign.resetHistory();
      pausePlayer.resetHistory();
      resumePlayer.resetHistory();
      camPlayer.addButton.resetHistory();
    });

    it('should add a button to the cam player and handle the callback', async () => {
      addCamRewindButton(
        camPlayer,
        'spotId',
        'spotName',
        'camId',
        'camTitle',
        false,
        'rewindUrl',
        false
      );

      const wait = () => new Promise((resolve) => setTimeout(resolve, 310));
      await wait();

      expect(camPlayer.addButton).to.have.been.calledOnce();
      expect(window.location.assign).to.have.been.calledOnceWithExactly(
        'rewindUrl'
      );
    });

    it('should not do anything if the camPlayer does not exist', async () => {
      addCamRewindButton(
        null,
        'spotId',
        'spotName',
        'camId',
        'camTitle',
        false,
        'rewindUrl',
        true
      );

      const wait = () => new Promise((resolve) => setTimeout(resolve, 310));
      await wait();

      expect(camPlayer.addButton).to.have.not.been.called();
      expect(window.location.assign).to.have.not.been.called();
    });
  });

  describe('getRewindUrl', () => {
    it('should default to using the spotName', () => {
      const result = getRewindUrl('spotName', { _id: '123' });

      expect(result).to.be.equal('/surf-cams/spotname/123');
    });

    it('should use the camera title if not spotName exists', () => {
      const result = getRewindUrl('', { _id: '123', title: 'camTitle' });

      expect(result).to.be.equal('/surf-cams/camtitle/123');
    });

    it('should use "rewind" if no spotName or camera title exists', () => {
      const result = getRewindUrl('', { _id: '123' });

      expect(result).to.be.equal('/surf-cams/rewind/123');
    });
  });

  describe('messageConfig', () => {
    it('should handle isClip message configs', () => {
      const result = messageConfig({ message: 'clipmessage' }, true);

      expect(result).to.deep.equal({
        camStatusMessage: {
          message: 'clipmessage',
          subMessage: null,
        },
        camDownMessage: {
          message: 'clipmessage',
          subMessage: null,
        },
      });
    });

    it('should return ALT messages', () => {
      const result = messageConfig({ altMessage: 'altMessage' }, false, ALT, {
        altMessage: 'altMessage',
      });

      expect(result).to.deep.equal({
        camStatusMessage: {
          message: 'altMessage',
          subMessage: null,
        },
        camDownMessage: {
          message: 'altMessage',
          subMessage: null,
        },
      });
    });

    it('should return SUB messages', () => {
      const result = messageConfig(
        { message: 'message', subMessage: 'subMessage' },
        false,
        SUB,
        {
          subMessage: 'subMessage',
          message: 'message',
        }
      );

      expect(result).to.deep.equal({
        camStatusMessage: {
          message: 'message',
          subMessage: 'subMessage',
        },
        camDownMessage: {
          message: 'message',
          subMessage: 'subMessage',
        },
      });
    });
  });
});
