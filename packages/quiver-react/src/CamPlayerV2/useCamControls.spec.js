import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { useCamControls } from './useCamControls';

describe('useCamControls', () => {
  let registeredHandlers = {};
  const camPlayer = {
    on: (eventName, handler) => {
      if (registeredHandlers[eventName]) {
        return registeredHandlers[eventName].push(handler);
      }
      registeredHandlers[eventName] = [handler];
      return null;
    },
  };

  afterEach(() => {
    registeredHandlers = {};
  });

  const Component = () => {
    const controlsVisible = useCamControls(camPlayer);

    return <div className="div">{`${controlsVisible}`}</div>;
  };

  it('should set the listener callbacks', () => {
    mount(<Component />);
    expect(registeredHandlers.userActive).to.have.length(1);
    expect(registeredHandlers.userInactive).to.have.length(1);
  });

  it('should handle showing/hiding the controls', () => {
    const wrapper = mount(<Component />);

    expect(wrapper.text()).to.be.equal('true');
    registeredHandlers.userInactive[0]();

    wrapper.update();
    expect(wrapper.text()).to.be.equal('false');
    registeredHandlers.userActive[0]();

    wrapper.update();
    expect(wrapper.text()).to.be.equal('true');
  });
});
