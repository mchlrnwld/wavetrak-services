import { trackEvent } from '@surfline/web-common';
import { useEffect, useCallback, useMemo, useRef } from 'react';
import { useUnmount } from 'react-use';
import { PLAYED_WEBCAM, STOPPED_WEBCAM } from '../utils/constants';

/**
 * @description Because we rely on the JWPlayer `.on()` hooks to fire the track event
 * we want to make sure that we are not subject to the details of how they implement
 * these hooks. To achieve this we can use a ref, and mutate the `.current` property
 * of it. This will prevent us from being at the mercy of whether or not calling `.on`
 * multiple times will actually update the callback correctly
 * @param {import('../PropTypes/cameras').Camera} camera
 * @param {string} location
 * @param {string} spotName
 * @param {string} spotId
 * @param {boolean} isFavorite
 */
const usePlaybackEventProperties = (
  camera,
  location,
  spotName,
  spotId,
  isFavorite
) => {
  const playbackEventProperties = useMemo(
    () => ({
      category: location,
      premiumCam: camera.isPremium,
      spotName,
      camName: camera.title,
      camId: camera._id,
      favorite: isFavorite,
      spotId,
    }),
    [location, camera, isFavorite, spotName, spotId]
  );

  const eventPropertiesRef = useRef(playbackEventProperties);

  // Whenever the memoized properties update, we want to update the value inside the ref
  useEffect(() => {
    eventPropertiesRef.current = playbackEventProperties;
  }, [playbackEventProperties]);

  return eventPropertiesRef;
};

/**
 * @param {import('../PropTypes/cameras').Camera} camera
 * @param {string} location
 * @param {string} spotName
 * @param {string} spotId
 * @param {boolean} isFavorite
 */
export const useTrackCamPlayback = (
  camera,
  location,
  spotName,
  spotId,
  isFavorite
) => {
  const playbackEventProperties = usePlaybackEventProperties(
    camera,
    location,
    spotName,
    spotId,
    isFavorite
  );

  const trackPause = useCallback(
    /**
     * @param {boolean} isTimeout
     */
    (isTimeout = false) => {
      trackEvent(STOPPED_WEBCAM, {
        ...playbackEventProperties.current,
        isTimeout,
      });
    },
    [playbackEventProperties]
  );

  const trackPlay = useCallback(() => {
    trackEvent(PLAYED_WEBCAM, playbackEventProperties.current);
  }, [playbackEventProperties]);

  useUnmount(() => trackPause());

  return { trackPause, trackPlay };
};
