/* eslint-disable react/prop-types */
import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { useCamReady } from './useCamReady';

describe('useCamReady', () => {
  let registeredHandlers = {};
  const camPlayer = {
    on: (eventName, handler) => {
      if (registeredHandlers[eventName]) {
        return registeredHandlers[eventName].push(handler);
      }
      registeredHandlers[eventName] = [handler];
      return null;
    },
  };

  const setCamReadyStub = sinon.stub();

  afterEach(() => {
    registeredHandlers = {};
    setCamReadyStub.resetHistory();
  });

  const Component = ({
    handleCamReady,
    camReady,
    autoplay,
    isPremium,
    isDown,
  }) => {
    useCamReady(
      camPlayer,
      handleCamReady,
      setCamReadyStub,
      camReady,
      autoplay,
      isPremium,
      isDown
    );

    return <div className="div" />;
  };

  it('should set the listener callbacks', () => {
    mount(<Component />);
    expect(registeredHandlers.firstFrame).to.have.length(1);
    expect(registeredHandlers.error).to.have.length(1);
    expect(registeredHandlers.ready).to.have.length(1);
  });

  it('should handle firstFrame event - handleCamReady = true', () => {
    mount(<Component handleCamReady />);

    registeredHandlers.firstFrame[0]();
    expect(setCamReadyStub).to.have.been.calledOnceWithExactly(true);
  });

  it('should handle firstFrame event - handleCamReady = false', () => {
    mount(<Component />);

    registeredHandlers.firstFrame[0]();
    expect(setCamReadyStub).to.not.have.been.called();
  });

  it('should handle error event - handleCamReady = true', () => {
    mount(<Component handleCamReady />);

    registeredHandlers.error[0]();
    expect(setCamReadyStub).to.have.been.calledOnceWithExactly(true);
  });

  it('should handle error event - handleCamReady = false', () => {
    mount(<Component />);

    registeredHandlers.error[0]();
    expect(setCamReadyStub).to.not.have.been.called();
  });

  it('should handle ready event - handleCamReady = false', () => {
    mount(<Component />);

    registeredHandlers.ready[0]();
    expect(setCamReadyStub).to.not.have.been.called();
  });

  it('should handle ready event - handleCamReady = true', () => {
    mount(<Component handleCamReady />);

    registeredHandlers.ready[0]();
    expect(setCamReadyStub).to.have.been.calledOnceWithExactly(true);
  });

  it('should call camReady if that cam is down - handleCamReady = true', () => {
    mount(<Component handleCamReady isDown />);

    expect(setCamReadyStub).to.have.been.calledOnceWithExactly(true);
  });
  it('should not call camReady if that cam is down - handleCamReady = false', () => {
    mount(<Component isDown />);

    expect(setCamReadyStub).to.not.have.been.called();
  });
});
