import { slugify, TABLET_LARGE_WIDTH, trackEvent } from '@surfline/web-common';

export const ALT = 'ALT';
export const SUB = 'SUB';
export const MSG = 'MSG';

/**
 * @param {import('./').CameraStatus} status
 * @param {boolean} isClip
 * @param {ALT | SUB | MSG} messageType
 * @param {import('./').NetworkError} networkError
 */
export const messageConfig = (
  status,
  isClip = false,
  messageType = null,
  networkError = {}
) => {
  const showSubMessage = messageType === SUB;

  if (isClip) {
    return {
      camStatusMessage: {
        message: status.message,
        subMessage: null,
      },
      camDownMessage: {
        message: status.message,
        subMessage: null,
      },
    };
  }

  const messageData = {
    camStatusMessage: {
      message: messageType === ALT ? status.altMessage : status.message,
      subMessage: showSubMessage ? status.subMessage : null,
    },
    camDownMessage: {
      message:
        messageType === ALT ? networkError.altMessage : networkError.message,
      subMessage: showSubMessage ? networkError.subMessage : null,
    },
  };

  return messageData;
};

/**
 * @param {string} spotName
 * @param {import('./').Camera} camera
 */
export const getRewindUrl = (spotName, camera) =>
  // If no spotName has been passed then try the camera title just so there's something in there
  // defaults to the first spot associated with the cam id anyway so not really required, defaults to
  // rewind incase there's no title either.
  `/surf-cams/${slugify(spotName || camera.title || 'rewind')}/${camera._id}`;

/**
 * @param {?import('./').CamPlayer} camPlayer
 * @param {string} spotId
 * @param {string} spotName
 * @param {import('./').Camera['_id']} camId
 * @param {import('./').Camera['title']} camTitle
 * @param {boolean} isPremium
 * @param {string} rewindUrl
 * @param {boolean} premiumCam
 */
export const addCamRewindButton = (
  camPlayer,
  spotId,
  spotName,
  camId,
  camTitle,
  isPremium,
  rewindUrl,
  premiumCam
) => {
  if (camPlayer) {
    camPlayer.addButton(
      `<svg
        class="rewind-button"
        xmlns="http://www.w3.org/2000/svg"
        width="18"
        height="18"
        viewBox="0 0 48 48"
        fill="rgba(255,255,255,0.8)"
      >
      <path d="M22 36V12L5 24l17 12zm1-12l17 12V12L23 24z"/>
    </svg>`,
      'Cam Rewind',
      () => {
        trackEvent('Clicked Cam Rewind', {
          spotId,
          spotName,
          location: 'cam player',
          camId,
          camName: camTitle,
          premiumCam,
          isMobileView: window.innerWidth < TABLET_LARGE_WIDTH,
          platform: 'web',
        });
        setTimeout(() => window.location.assign(rewindUrl), 300);
      },
      'contents',
      'jw-cam-rewind'
    );
  }
};
