import { useState, useEffect } from 'react';
import { nrNoticeError } from '@surfline/web-common';

const MANIFEST_ERROR_CODES = [232404, 232001];

/**
 * @param {?import('./').CamPlayer} camPlayer
 * @param {import('./').Camera['stillUrl']} stillUrl
 * @param {import('./').Camera['streamUrl']} streamUrl
 * @param {import('./CamPlayerV2').CamPlayerProps['rewindClip']} rewindClip
 * @param {() => void} setupPlayer
 */
export const useCamError = (
  camPlayer,
  stillUrl,
  streamUrl,
  rewindClip,
  setupPlayer,
  location
) => {
  /** @type {[import('./').NetworkError, (networkError: import('./').NetworkError) => void]} */
  const [networkError, setNetworkError] = useState({
    isDown: false,
    message: '',
    altMessage: '',
    subMessage: '',
  });

  // We don't need the not found count since we use the callback version
  // of the setState command to use/update it
  // eslint-disable-next-line no-unused-vars
  const [_, setNotFoundCount] = useState(0);

  useEffect(() => {
    camPlayer?.on('error', (error) => {
      /* istanbul ignore else */
      if (error.code) {
        if (error.code === 224002 && location === 'Cam Rewind') {
          setNetworkError({
            isDown: true,
            message: `Unfortunately, we are having an issue loading this Cam Rewind.
              Please try downloading the clip to view this Rewind.`,
            subMessage: null,
            altMessage: 'Camera Down',
          });
        } else {
          setNetworkError({
            isDown: true,
            message: `We're sorry to report that this camera is down.
              The Surfline team is aware of the issue and is currently working toward a resolution.`,
            subMessage: null,
            altMessage: 'Camera Down',
          });
        }

        //
        // if chunklist sends a 404, wait 1 second and resume play
        if (
          error.message.includes('404') ||
          MANIFEST_ERROR_CODES.includes(error.code)
        ) {
          setNotFoundCount((count) => {
            /* istanbul ignore else */
            if (count <= 10) {
              setNetworkError({
                isDown: true,
                message: 'Refreshing camera stream',
              });
              setTimeout(() => {
                setupPlayer();
              }, 1000);
            } else {
              nrNoticeError(error);
            }
            return count + 1;
          });
        }
      }
    });
  }, [camPlayer, setupPlayer, location]);
  return { networkError };
};
