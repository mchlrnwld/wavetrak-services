import { useState, useEffect } from 'react';

/**
 * @param {?import('.').CamPlayer} camPlayer
 */
export const useCamControls = (camPlayer) => {
  const [controlsVisible, setControlsVisible] = useState(true);

  useEffect(() => {
    camPlayer?.on('userInactive', () => setControlsVisible(false));
    camPlayer?.on('userActive', () => setControlsVisible(true));
  }, [camPlayer]);

  return controlsVisible;
};
