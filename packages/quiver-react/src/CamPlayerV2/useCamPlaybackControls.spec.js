import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { useCamPlaybackControls } from './useCamPlaybackControls';

describe('useCamPlaybackControls', () => {
  const pause = sinon.stub();
  const play = sinon.stub();
  const camPlayer = {
    pause,
    play,
  };

  const Component = () => {
    const { pausePlayer, resumePlayer } = useCamPlaybackControls(camPlayer);

    return (
      <div className="div">
        <div
          className="controls"
          pauseplayer={pausePlayer}
          resumeplayer={resumePlayer}
        />
      </div>
    );
  };

  it('should return resume/pause control functions', () => {
    const wrapper = mount(<Component />);
    const controls = wrapper.find('.controls');

    controls.prop('resumeplayer')();
    wrapper.update();

    expect(play).to.have.been.calledOnce();

    controls.prop('pauseplayer')();
    wrapper.update();

    expect(pause).to.have.been.calledOnce();
  });

  it('should handle missing camplay playback properties', () => {
    const wrapper = mount(<Component />);
    const controls = wrapper.find('.controls');

    delete camPlayer.play;
    delete camPlayer.stop;
    delete camPlayer.pause;

    controls.prop('pauseplayer')();
    controls.prop('resumeplayer')();

    wrapper.update();
  });
});
