/* eslint-disable react/prop-types */
import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { useControlledPlayback } from './useControlledPlayback';

describe('useControlledPlayback', () => {
  let registeredHandlers = {};
  const camPlayer = {
    on: (eventName, handler) => {
      if (registeredHandlers[eventName]) {
        return registeredHandlers[eventName].push(handler);
      }
      registeredHandlers[eventName] = [handler];
      return null;
    },
  };

  const pausePlayer = sinon.stub();
  const resumePlayer = sinon.stub();

  afterEach(() => {
    registeredHandlers = {};
    pausePlayer.resetHistory();
    resumePlayer.resetHistory();
  });

  const Component = ({ controlledPlayback, isPlaying }) => {
    useControlledPlayback(
      camPlayer,
      controlledPlayback,
      isPlaying,
      resumePlayer,
      pausePlayer
    );

    return <div className="div" />;
  };

  it('should set the listener callbacks', () => {
    mount(<Component />);
    expect(registeredHandlers.firstFrame).to.have.length(1);
  });

  it('should stop the player on the first frame if isPlaying is false', () => {
    mount(<Component isPlaying={false} controlledPlayback />);

    pausePlayer.resetHistory();

    registeredHandlers.firstFrame[0]();

    expect(pausePlayer).to.have.been.calledOnce();
  });

  it('should stop the player if isPlaying is false', () => {
    mount(<Component controlledPlayback isPlaying={false} />);

    expect(pausePlayer).to.have.been.calledOnce();
  });

  it('should start the player if isPlaying is true', () => {
    mount(<Component controlledPlayback isPlaying />);

    expect(resumePlayer).to.have.been.calledOnce();
  });
});
