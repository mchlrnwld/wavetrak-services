import { useEffect, useState } from 'react';

/**
 * @param {object} props
 * @param {?import('./').CamPlayer} props.camPlayer
 * @param {() => void} props.setupPlayer
 * @param {string} props.streamUrl
 * @param {(showTimeout: boolean) => void} props.setShowTimeout
 * @param {(isStreaming: boolean) => void} props.onToggleStreaming
 * @param {import('../PropTypes/cameras').Camera} props.camera
 * @param {string} props.location
 * @param {string} props.spotName
 * @param {boolean} props.isFavorite
 * @param {boolean} props.showTimeout
 * @param {() => void} props.trackPlay
 * @param {() => void} props.trackPause
 */
export const useCamPlayback = ({
  camPlayer,
  setupPlayer,
  streamUrl,
  setShowTimeout,
  onToggleStreaming,
  trackPlay,
  trackPause,
}) => {
  const [isStreaming, setStreaming] = useState(false);

  useEffect(() => {
    camPlayer?.on('play', () => {
      setShowTimeout(false);
      setStreaming(true);
      trackPlay();
    });
    camPlayer?.on('pause', () => {
      setStreaming(false);
      trackPause();
    });
    camPlayer?.on('idle', () => {
      setStreaming(false);
    });

    // We only want to renew these hooks when we have a new cam player
    // since it will add a new callback to the cam player every single time
    // `on()` is called.
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [camPlayer]);

  // Reset the player if the stream URL changes
  useEffect(() => {
    if (streamUrl) {
      setShowTimeout(false);
      setStreaming(false);
      setupPlayer();
    }
  }, [streamUrl, setupPlayer, setShowTimeout]);

  useEffect(() => {
    /* istanbul ignore else */
    if (onToggleStreaming) {
      onToggleStreaming(isStreaming);
    }
  }, [isStreaming, onToggleStreaming]);

  return isStreaming;
};
