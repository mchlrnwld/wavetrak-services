import { useEffect } from 'react';

export const AUTOPLAY_DISABLED = 'disabled';
export const AUTOPLAY_ENABLED = 'enabled';

/**
 * @param {?import('./').CamPlayer} camPlayer
 * @param {import('./CamPlayerV2').CamPlayerProps['handleCamReady']} handleCamReady
 * @param {import('./CamPlayerV2').CamPlayerProps['setCamReady']} setCamReady
 * @param {import('./CamPlayerV2').CamPlayerProps['camReady']} camReady
 * @param {import('./CamPlayerV2').CamPlayerProps['autoplay']} autoplay
 * @param {import('./CamPlayerV2').CamPlayerProps['isPremium']} isPremium
 * @param {boolean} isDown
 */
export const useCamReady = (
  camPlayer,
  handleCamReady,
  setCamReady,
  camReady,
  autoplay,
  isPremium,
  isDown
) => {
  useEffect(() => {
    camPlayer?.on('error', () => {
      if (handleCamReady && camReady !== true) {
        setCamReady(true);
      }
    });

    camPlayer?.on('firstFrame', () => {
      if (handleCamReady && camReady !== true) {
        setCamReady(true);
      }
    });

    /**
     * @description For non-premium users or areas where we have disabled autoplay
     * the `firstFrame` jw-player event won't fire, and with the new cam fade in
     * animation I created we have the `handleCamReady` prop which when true requires
     * us to call `setCamReady` in order to let the consuming app know the cam has loaded.
     */
    camPlayer?.on('ready', () => {
      const noAutoplay = autoplay === AUTOPLAY_DISABLED;
      if ((!isPremium || noAutoplay) && handleCamReady && !camReady) {
        setCamReady(true);
      }
    });
  }, [camPlayer, handleCamReady, setCamReady, camReady, isPremium, autoplay]);

  useEffect(() => {
    if (isDown && handleCamReady && camReady !== true) {
      setCamReady(true);
    }
  }, [isDown, handleCamReady, setCamReady, camReady]);
};
