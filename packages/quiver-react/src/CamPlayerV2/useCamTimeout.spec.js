/* eslint-disable react/prop-types */
import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { useCamTimeout } from './useCamTimeout';

describe('useCamTimeout', () => {
  let registeredHandlers = {};
  const camPlayer = {
    on: (eventName, handler) => {
      if (registeredHandlers[eventName]) {
        return registeredHandlers[eventName].push(handler);
      }
      registeredHandlers[eventName] = [handler];
      return null;
    },
  };

  const pausePlayer = sinon.stub();
  const trackPause = sinon.stub();
  const timeoutMultiCams = sinon.stub();

  afterEach(() => {
    registeredHandlers = {};
    pausePlayer.resetHistory();
    timeoutMultiCams.resetHistory();
    trackPause.resetHistory();
  });

  const Component = ({ isMultiCam, isPrimaryCam }) => {
    const [showTimeout] = useCamTimeout(
      camPlayer,
      isMultiCam,
      isPrimaryCam,
      timeoutMultiCams,
      pausePlayer,
      trackPause
    );

    return <div className={showTimeout ? 'timeout' : 'div'} />;
  };

  it('should set the listener callbacks', () => {
    mount(<Component />);
    expect(registeredHandlers.time).to.have.length(1);
  });

  it('should handle stopping the player after the timeout', () => {
    const wrapper = mount(<Component />);

    for (let i = 0; i < 1000; i += 1) {
      registeredHandlers.time[0]({ position: i });
    }

    expect(pausePlayer).to.have.been.calledOnce();
    wrapper.update();
    expect(wrapper.find('.timeout')).to.have.length(1);
  });

  it('should handle stopping the player after the timeout - multiCam', () => {
    mount(<Component isMultiCam isPrimaryCam />);

    for (let i = 0; i < 1000; i += 1) {
      registeredHandlers.time[0]({ position: i });
    }

    expect(timeoutMultiCams).to.have.been.calledOnce();
    expect(trackPause).to.have.been.calledOnceWithExactly(true);
  });

  it('should send a track event but not call timeoutMultiCams - multiCam non-primary', () => {
    mount(<Component isMultiCam />);

    for (let i = 0; i < 1000; i += 1) {
      registeredHandlers.time[0]({ position: i });
    }

    expect(timeoutMultiCams).to.have.not.been.calledOnce();
    expect(trackPause).to.have.been.calledOnceWithExactly(true);
  });
});
