import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import common from '@surfline/web-common';

import { useTrackCamPlayback } from './useTrackCamPlayback';
import { PLAYED_WEBCAM, STOPPED_WEBCAM } from '../utils/constants';

describe('useTrackCamPlayback', () => {
  const camera = { _id: '123', isPremium: true, title: 'title' };
  const location = 'quiver-react';
  const spotName = 'Jeremys Desk';
  const spotId = '123454321';
  const isFavorite = true;

  /** @type {sinon.SinonStub} */
  let trackEventStub;

  beforeEach(() => {
    trackEventStub = sinon.stub(common, 'trackEvent');
  });

  afterEach(() => {
    trackEventStub.restore();
  });

  const Component = () => {
    const { trackPlay, trackPause } = useTrackCamPlayback(
      camera,
      location,
      spotName,
      spotId,
      isFavorite
    );

    return (
      <div className="div">
        <div
          className="controls"
          trackPlay={trackPlay}
          trackPause={trackPause}
        />
      </div>
    );
  };

  it('should track a playing event', () => {
    const wrapper = mount(<Component />);
    const controls = wrapper.find('.controls');

    controls.prop('trackPlay')();

    expect(trackEventStub).to.have.been.calledOnceWithExactly(PLAYED_WEBCAM, {
      category: location,
      premiumCam: camera.isPremium,
      spotName,
      spotId,
      camName: camera.title,
      camId: camera._id,
      favorite: isFavorite,
    });
  });

  it('should track a pausing event', () => {
    const wrapper = mount(<Component />);
    const controls = wrapper.find('.controls');

    controls.prop('trackPause')();

    expect(trackEventStub).to.have.been.calledOnceWithExactly(STOPPED_WEBCAM, {
      category: location,
      premiumCam: camera.isPremium,
      spotName,
      spotId,
      camName: camera.title,
      camId: camera._id,
      favorite: isFavorite,
      isTimeout: false,
    });
  });

  it('should track a pausing event triggered by a timeout', () => {
    const wrapper = mount(<Component />);
    const controls = wrapper.find('.controls');

    controls.prop('trackPause')(true);

    expect(trackEventStub).to.have.been.calledOnceWithExactly(STOPPED_WEBCAM, {
      category: location,
      premiumCam: camera.isPremium,
      spotName,
      spotId,
      camName: camera.title,
      camId: camera._id,
      favorite: isFavorite,
      isTimeout: true,
    });
  });
});
