/* eslint-disable react/prop-types */

import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { Cookies } from 'react-cookie';
import { useCamPlayer } from './useCamPlayer';
import * as utils from './utils';

const Component = ({
  aspectRatio,
  autoplay,
  camera,
  isPersistentCam,
  isPremium,
  isPrerecorded,
  playbackRateControls,
  playerId,
  rewindUrl,
  spotId,
  spotName,
  stillUrl,
  streamUrl,
}) => {
  const { setupPlayer, camPlayer } = useCamPlayer({
    aspectRatio,
    autoplay,
    camera,
    isPersistentCam,
    isPremium,
    isPrerecorded,
    playbackRateControls,
    playerId,
    rewindUrl,
    spotId,
    spotName,
    stillUrl,
    streamUrl,
  });

  return (
    <div setupplayer={setupPlayer} camplayer={camPlayer} className="div" />
  );
};

describe('useCamPlayer', () => {
  const removeStub = sinon.stub();
  const setupStub = sinon.stub();
  const camPlayerStub = {
    remove: removeStub,
    setup: setupStub,
  };

  let addCamRewindButtonStub;
  let getCookieStub;

  beforeEach(() => {
    window.jwplayer = sinon.stub().returns(camPlayerStub);
    window.jwplayer.defaults = {
      key: 'jwplayerkey',
    };
    addCamRewindButtonStub = sinon.stub(utils, 'addCamRewindButton');
    getCookieStub = sinon.stub(Cookies.prototype, 'get');
  });

  afterEach(() => {
    removeStub.resetHistory();
    setupStub.resetHistory();
    window.jwplayer?.resetHistory();
    getCookieStub.restore();
    addCamRewindButtonStub.restore();
  });

  const baseProps = {
    aspectRatio: '16:9',
    autoplay: 'disabled',
    camera: { _id: '_id', title: 'title', isPremium: false },
    isPersistentCam: false,
    isPremium: false,
    isPrerecorded: false,
    playbackRateControls: false,
    playerId: 'player',
    legacySpotId: '123',
    rewindUrl: 'rewindUrl',
    spotId: 'spotId',
    spotName: 'spotName',
    stillUrl: 'stillUrl',
    streamUrl: 'streamUrl',
  };

  it('should set up a player', () => {
    getCookieStub.returns('123');
    const wrapper = mount(<Component {...baseProps} />);
    expect(window.jwplayer).to.have.been.calledOnceWithExactly('player');
    expect(setupStub).to.have.been.calledOnceWithExactly({
      file: 'streamUrl?accessToken=123',
      type: 'hls',
      withCredentials: false,
      image: 'stillUrl',
      aspectratio: '16:9',
      primary: 'html5',
      width: '100%',
      hlshtml: true,
      androidhls: true,
      skin: 'five',
      autostart: false,
      mute: false,
      advertising: {
        client: 'googima',
      },
      liveTimeout: 0,
      playbackRateControls: false,
    });
    expect(addCamRewindButtonStub).to.have.been.calledOnceWithExactly(
      camPlayerStub,
      'spotId',
      'spotName',
      '_id',
      'title',
      false,
      'rewindUrl',
      false
    );

    wrapper.setProps({ unusedprop: true });
    wrapper.update();
    const div = wrapper.find('.div');
    expect(div.prop('camplayer')).to.deep.equal(camPlayerStub);
  });

  it('should set up a player with autoplay', () => {
    getCookieStub.returns(null);
    mount(
      <Component
        {...{
          ...baseProps,
          legacySpotId: null,
          autoplay: 'enabled',
          streamUrl: 'some.mp4?authorization=true',
          playbackRateControls: true,
        }}
      />
    );
    expect(window.jwplayer).to.have.been.calledOnceWithExactly('player');
    expect(setupStub).to.have.been.calledOnceWithExactly({
      file: 'some.mp4?authorization=true',
      type: 'mp4',
      withCredentials: true,
      image: 'stillUrl',
      aspectratio: '16:9',
      primary: 'html5',
      width: '100%',
      hlshtml: true,
      androidhls: true,
      skin: 'five',
      autostart: true,
      mute: true,
      advertising: {
        client: 'googima',
      },
      liveTimeout: 0,
      playbackRateControls: true,
    });

    expect(addCamRewindButtonStub).to.have.been.called();
  });

  it('should remove the player on unmount', () => {
    getCookieStub.returns(null);
    const wrapper = mount(
      <Component
        {...{
          ...baseProps,
          legacySpotId: null,
          autoplay: 'enabled',
          streamUrl: 'some.mp4?authorization=true',
          playbackRateControls: true,
        }}
      />
    );
    wrapper.unmount();
    expect(camPlayerStub.remove).to.have.been.calledOnce();
  });

  it('should return early if there is no setup function', () => {
    delete camPlayerStub.setup;

    mount(<Component {...baseProps} />);
    expect(window.jwplayer).to.have.been.calledOnceWithExactly('player');
    expect(setupStub).to.not.have.been.called();
  });
});
