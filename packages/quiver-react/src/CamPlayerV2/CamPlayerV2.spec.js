import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import * as camPlayer from './useCamPlayer';
import * as camControls from './useCamControls';
import * as camError from './useCamError';
import * as camPlayback from './useCamPlayback';
import * as camPlaybackControls from './useCamPlaybackControls';
import * as camReady from './useCamReady';
import * as camTimeout from './useCamTimeout';
import * as controlledPlayback from './useControlledPlayback';

import CamPlayer from './CamPlayerV2';

describe('CamPlayerV2', () => {
  /** @type {sinon.SinonStub} */
  let useCamPlayer;
  /** @type {sinon.SinonStub} */
  let useCamControls;
  /** @type {sinon.SinonStub} */
  let useCamError;
  /** @type {sinon.SinonStub} */
  let useCamPlayback;
  /** @type {sinon.SinonStub} */
  let useCamPlaybackControls;
  /** @type {sinon.SinonStub} */
  let useCamReady;
  /** @type {sinon.SinonStub} */
  let useCamTimeout;
  /** @type {sinon.SinonStub} */
  let useControlledPlayback;

  beforeEach(() => {
    useCamControls = sinon.stub(camControls, 'useCamControls');
    useCamError = sinon.stub(camError, 'useCamError');
    useCamPlayback = sinon.stub(camPlayback, 'useCamPlayback');
    useCamPlaybackControls = sinon.stub(
      camPlaybackControls,
      'useCamPlaybackControls'
    );
    useCamReady = sinon.stub(camReady, 'useCamReady');
    useCamTimeout = sinon.stub(camTimeout, 'useCamTimeout');
    useControlledPlayback = sinon.stub(
      controlledPlayback,
      'useControlledPlayback'
    );
    useCamPlayer = sinon.stub(camPlayer, 'useCamPlayer');

    // Setup basic functionality which can be overridden by each test
    useCamPlayer.returns({ setupPlayer: sinon.stub(), camPlayer: {} });
    useCamPlaybackControls.returns({});
    useCamPlayback.returns(true);
    useCamControls.returns(true);
    useCamError.returns({ networkError: { isDown: false } });
    useCamTimeout.returns([false, () => {}]);
  });

  afterEach(() => {
    useCamControls.restore();
    useCamPlayback.restore();
    useCamError.restore();
    useCamPlaybackControls.restore();
    useCamReady.restore();
    useCamTimeout.restore();
    useControlledPlayback.restore();
    useCamPlayer.restore();
  });

  const camera = {
    _id: 'camId',
    streamUrl: 'streamUrl',
    stillUrl: 'stillUrl',
    status: {
      isDown: false,
      message: 'This camera is totally broken...',
    },
    isPrerecorded: false,
    title: 'Camera - Title',
  };

  const props = {
    playerId: 'quiver-player',
    spotName: 'spotName',
    camera: {
      _id: 'camId',
      streamUrl: 'streamUrl',
      stillUrl: 'stillUrl',
      status: {
        isDown: false,
        message: 'This camera is totally broken...',
      },
      isPrerecorded: false,
      title: 'Camera - Title',
    },
    jwPlayerKey: 'jwPlayerKey',
    legacySpotId: '144645',
    camHostAd: (
      <div className="cam-host-ad">
        <a href="#test">cam host ad here</a>
      </div>
    ),
  };

  const camDownProps = {
    ...camera,
    status: {
      isDown: true,
      message: 'This camera is totally broken...',
    },
  };

  const preRecordedCamProps = {
    ...camera,
    streamUrl: 'streamUrl.mp4',
    isPrerecorded: true,
  };

  it('should render the CamPlayer component', () => {
    const wrapper = mount(<CamPlayer {...props} />);

    expect(wrapper.find('.quiver-cam-player')).to.have.length(1);

    expect(wrapper.find('CamTimeout')).to.have.length(0);
    expect(
      wrapper.find('.quiver-cam-player__player-container').html()
    ).to.contain('id="quiver-player"');
  });

  it('should render the CamError is the cam is down', () => {
    const wrapper = mount(<CamPlayer {...props} camera={camDownProps} />);
    const CamError = wrapper.find('CamError');
    expect(wrapper.find('.quiver-cam-player')).to.have.length(0);

    expect(CamError).to.have.length(1);
    expect(CamError.props()).to.deep.equal({
      rewindUrl: '/surf-cams/spotname/camId',
      messageData: {
        subMessage: null,
        message: 'This camera is totally broken...',
      },
    });
  });

  it('should add the playbackRateControls className', () => {
    const wrapper = mount(<CamPlayer {...props} playbackRateControls />);

    expect(
      wrapper.find('.quiver-cam-player__player--show-settings')
    ).to.have.length(1);
  });

  it('should be able to hide overlays', () => {
    const wrapper = mount(
      <CamPlayer {...props} showOverlays={false} isMultiCam />
    );

    const overlays = wrapper.find('.quiver-cam-player__player__overlays');
    const multiCamTitle = wrapper.find(
      '.quiver-cam-player__player__overlays__cam-title'
    );

    expect(overlays).to.have.length(0);
    expect(multiCamTitle).to.have.length(0);
  });

  it('should not render the timeout', () => {
    const wrapper = mount(<CamPlayer {...props} />);

    expect(wrapper.find('.quiver-cam-player')).to.have.length(1);
    expect(wrapper.find('.quiver-cam-player--timeout')).to.have.length(0);

    expect(wrapper.find('CamTimeout')).to.have.length(0);
  });

  it('should render the timeout', () => {
    const resumePlayerStub = sinon.stub();
    // useCamTimeout.resetBehavior()
    // useCamPlaybackControls.resetBehavior()
    useCamTimeout.returns([true]);
    useCamPlaybackControls.returns({
      resumePlayer: resumePlayerStub,
    });
    useCamPlayback.returns(false);

    const wrapper = mount(<CamPlayer {...props} />);
    const timeout = wrapper.find('CamTimeout');

    expect(wrapper.find('.quiver-cam-player--timeout')).to.have.length(1);

    expect(timeout).to.have.length(1);
    expect(timeout.props()).to.deep.contain({
      stillUrl: props.camera.stillUrl,
      onClick: resumePlayerStub,
    });
  });

  it('should not render the multi cam title for non multi cam', () => {
    const wrapper = mount(<CamPlayer {...props} />);
    const multiCamTitle = wrapper.find(
      '.quiver-cam-player__player__overlays__cam-title'
    );

    expect(multiCamTitle).to.have.length(0);
  });

  it('should render the cam host ad', () => {
    const wrapper = mount(<CamPlayer {...props} />);

    expect(wrapper.find('.cam-host-ad')).to.have.length(1);
  });

  it('should not show the cam host ad when not streaming', () => {
    useCamPlayback.returns(false);

    const wrapper = mount(<CamPlayer {...props} />);

    const camHostAd = wrapper.find('.cam-host-ad');
    const hostAdContainer = wrapper.find('.quiver-cam-player__player__host-ad');

    expect(camHostAd).to.have.length(1);
    expect(hostAdContainer.prop('style')).to.deep.equal({ display: 'none' });
  });

  it('should show the parsed cam title for multi cam', () => {
    const wrapper = mount(<CamPlayer {...props} isMultiCam />);

    const camTitle = wrapper.find(
      '.quiver-cam-player__player__overlays__cam-title'
    );

    expect(camTitle).to.have.length(1);
    expect(camTitle.text()).to.be.equal('Title');
  });

  it('should show pre-recorded clip start and end times', () => {
    const wrapper = mount(
      <CamPlayer
        {...props}
        isMultiCam
        camera={preRecordedCamProps}
        prerecordedTimeRange={{ start: 1577869200000, end: 1577872800000 }}
      />
    );

    expect(wrapper.find('.quiver-cam-player--prerecorded')).to.have.length(1);

    const camPrerecorded = wrapper.find(
      '.quiver-cam-player__player__recorded-earlier'
    );

    expect(camPrerecorded).to.have.length(1);
    expect(camPrerecorded.text()).to.equal(
      'Recorded Earlier  |  09:00 - 10:00AM'
    );
  });

  it('should show red live circle', () => {
    const wrapper = mount(<CamPlayer {...props} />);

    const live = wrapper.find('.quiver-cam-player__player__live');
    const recordedEarlier = wrapper.find(
      '.quiver-cam-player__player__recorded-earlier'
    );
    expect(live).to.have.length(1);
    expect(live.text()).to.contain(' Live');

    expect(recordedEarlier).to.have.length(0);
  });
});
