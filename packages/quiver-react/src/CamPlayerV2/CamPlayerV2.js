import React from 'react';
import PropTypes from 'prop-types';
import { format } from 'date-fns';
import classNames from 'classnames';
import { canUseDOM } from '@surfline/web-common';
import CamTimeout from '../CamPlayer/CamTimeout';
import CamError from '../CamPlayer/CamError';
import AspectRatioContent from '../AspectRatioContent';
import { cameraPropType } from '../PropTypes/cameras';
import { useCamPlayer } from './useCamPlayer';
import {
  AUTOPLAY_DISABLED,
  AUTOPLAY_ENABLED,
  useCamReady,
} from './useCamReady';
import { ALT, SUB, MSG, messageConfig, getRewindUrl } from './utils';
import { useCamPlaybackControls } from './useCamPlaybackControls';
import { useCamPlayback } from './useCamPlayback';
import { useCamError } from './useCamError';
import { useCamTimeout } from './useCamTimeout';
import { useControlledPlayback } from './useControlledPlayback';
import { useCamControls } from './useCamControls';
import { useTrackCamPlayback } from './useTrackCamPlayback';

/**
 * @param {boolean} showTimeout
 * @param {boolean} isPrerecorded
 * @param {boolean} isPremium
 */
const camPlayerClassName = (showTimeout, isPrerecorded, isPremium) =>
  classNames({
    'quiver-cam-player': true,
    'quiver-cam-player--timeout': showTimeout,
    'quiver-cam-player--prerecorded': isPrerecorded,
    'quiver-cam-player--free': !isPremium,
  });

/**
 * @param {boolean} playbackRateControls
 */
const camPlayerSettingsClassName = (playbackRateControls) =>
  classNames({
    'quiver-cam-player__player': true,
    'quiver-cam-player__player--show-settings': !!playbackRateControls,
  });

/**
 * @typedef{Object} CamPlayerProps
 * @property {boolean} [autoplay]
 * @property {string} [aspectRatio]
 * @property {import('./').Camera} camera
 * @property {React.ReactNode} [camHostAd]
 * @property {boolean} [camReady]
 * @property {boolean} [controlledPlayback]
 * @property {boolean} [handleCamReady]
 * @property {boolean} [isClip]
 * @property {boolean} [isMultiCam]
 * @property {boolean} [isPersistentCam]
 * @property {boolean} [isPlaying]
 * @property {boolean} [isPremium]
 * @property {boolean} [isPrimaryCam]
 * @property {string} [legacySpotId]
 * @property {ALT | SUB | MSG} [messageType=MSG]
 * @property {(isStreaming: boolean) => void} [onToggleStreaming]
 * @property {boolean} [playbackRateControls]
 * @property {string} playerId
 * @property {{ start: string, end: string}} [prerecordedTimeRange]
 * @property {string} [rewindClip]
 * @property {(isReady: boolean) => void} [setCamReady]
 * @property {boolean} [showOverlays]
 * @property {string} [spotId]
 * @property {string} [spotName]
 * @property {() => void} [timeoutMultiCams]
 * @property {boolean} [isFavorite]
 * @property {string} [location]
 *
 * @param {CamPlayerProps} props
 */
const CamPlayer = ({
  autoplay,
  aspectRatio,
  camera,
  camHostAd,
  camReady,
  controlledPlayback,
  handleCamReady,
  isClip,
  isMultiCam,
  isPersistentCam,
  isPlaying,
  isPremium,
  isPrimaryCam,
  legacySpotId,
  messageType,
  onToggleStreaming,
  playbackRateControls,
  playerId,
  prerecordedTimeRange,
  rewindClip,
  setCamReady,
  showOverlays,
  spotId,
  spotName,
  timeoutMultiCams,
  location,
  isFavorite,
}) => {
  const { streamUrl, stillUrl, isPrerecorded, status } = camera;

  const rewindUrl = getRewindUrl(spotName, camera);

  // This is to remove 'SOCAL - ' from camera name
  const cameraTitle = camera.title || '';
  const parsedCameraTitle =
    cameraTitle.indexOf('- ') !== -1 ? cameraTitle.split('- ')[1] : cameraTitle;

  const prerecordedStart =
    prerecordedTimeRange.start &&
    format(new Date(prerecordedTimeRange.start), 'hh:mm');
  const prerecordedEnd =
    prerecordedTimeRange.end &&
    format(new Date(prerecordedTimeRange.end), 'hh:mma');

  const isLive = camera.streamUrl?.indexOf('.mp4') === -1;

  const { setupPlayer, camPlayer } = useCamPlayer({
    aspectRatio,
    autoplay,
    camera,
    isPersistentCam,
    isPremium,
    isPrerecorded,
    legacySpotId,
    playbackRateControls,
    playerId,
    rewindUrl,
    spotId,
    spotName,
    stillUrl,
    streamUrl,
  });

  const { resumePlayer, pausePlayer } = useCamPlaybackControls(camPlayer);
  const { trackPlay, trackPause } = useTrackCamPlayback(
    camera,
    location,
    spotName,
    spotId,
    isFavorite
  );

  const [showTimeout, setShowTimeout] = useCamTimeout(
    camPlayer,
    isMultiCam,
    isPrimaryCam,
    timeoutMultiCams,
    pausePlayer,
    trackPause
  );

  const isStreaming = useCamPlayback({
    camPlayer,
    setupPlayer,
    streamUrl,
    setShowTimeout,
    onToggleStreaming,
    trackPause,
    trackPlay,
  });

  const controlsVisible = useCamControls(camPlayer);

  useControlledPlayback(
    camPlayer,
    controlledPlayback,
    isPlaying,
    resumePlayer,
    pausePlayer
  );

  const { networkError } = useCamError(
    camPlayer,
    stillUrl,
    streamUrl,
    rewindClip,
    setupPlayer,
    location
  );

  const isDown = status.isDown || networkError.isDown;

  useCamReady(
    camPlayer,
    handleCamReady,
    setCamReady,
    camReady,
    autoplay,
    isPremium,
    isDown
  );

  const messageData = messageConfig(
    camera.status,
    isClip,
    messageType,
    networkError
  );

  if (isDown) {
    return (
      <CamError
        messageData={
          status.isDown
            ? messageData.camStatusMessage
            : messageData.camDownMessage
        }
        rewindUrl={rewindUrl}
      />
    );
  }

  return (
    <div className={camPlayerClassName(showTimeout, isPrerecorded, isPremium)}>
      <div className={camPlayerSettingsClassName(playbackRateControls)}>
        {showOverlays && (
          <div className="quiver-cam-player__player__overlays">
            <div
              style={{ display: isStreaming ? 'block' : 'none' }}
              className="quiver-cam-player__player__host-ad"
            >
              {camHostAd}
            </div>
            {isMultiCam ? (
              <div className="quiver-cam-player__player__overlays__cam-title">
                {parsedCameraTitle}
              </div>
            ) : null}
          </div>
        )}
        <div>
          {isPrerecorded && prerecordedStart && !isLive && (
            <div className="quiver-cam-player__player__recorded-earlier">
              Recorded Earlier&nbsp;&nbsp;|&nbsp;&nbsp;
              {prerecordedStart} - {prerecordedEnd}
            </div>
          )}
          {isLive && !isPrerecorded && controlsVisible && isStreaming && (
            <div className="quiver-cam-player__player__live">
              <span>&#8226;</span> Live
            </div>
          )}
        </div>
        <AspectRatioContent>
          {showTimeout ? (
            <CamTimeout stillUrl={camera.stillUrl} onClick={resumePlayer} />
          ) : null}
          {/* Empty div with playerId must be set dangerously so that jwplayer doesn't override a React-managed DOM element */}
          <div
            className="quiver-cam-player__player-container"
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: `<div id="${playerId}"></div>`,
            }}
          />
        </AspectRatioContent>
      </div>
    </div>
  );
};

CamPlayer.defaultProps = {
  rewindClip: null,
  aspectRatio: '16:9',
  onToggleStreaming: null,
  isPremium: true,
  isPersistentCam: false,
  spotName: null,
  spotId: null,
  legacySpotId: null,
  camHostAd: null,
  showOverlays: true,
  timeoutMultiCams: null,
  isMultiCam: false,
  isPrimaryCam: false,
  messageType: MSG,
  isClip: false,
  prerecordedTimeRange: {
    start: null,
    end: null,
  },
  setCamReady: null,
  controlledPlayback: false,
  camReady: false,
  isPlaying: false,
  handleCamReady: false,
  autoplay: null,
  playbackRateControls: false,
  location: canUseDOM && window.location.pathname,
  isFavorite: false,
};

CamPlayer.propTypes = {
  rewindClip: PropTypes.string,
  playerId: PropTypes.string.isRequired,
  camera: cameraPropType.isRequired,
  onToggleStreaming: PropTypes.func,
  spotName: PropTypes.string,
  spotId: PropTypes.string,
  legacySpotId: PropTypes.string,
  isPremium: PropTypes.bool,
  isPersistentCam: PropTypes.bool,
  aspectRatio: PropTypes.string,
  camHostAd: PropTypes.node,
  showOverlays: PropTypes.bool,
  timeoutMultiCams: PropTypes.func,
  isPrimaryCam: PropTypes.bool,
  isMultiCam: PropTypes.bool,
  isClip: PropTypes.bool,
  messageType: PropTypes.oneOf([ALT, SUB, MSG]),
  prerecordedTimeRange: PropTypes.shape({
    start: PropTypes.string,
    end: PropTypes.string,
  }),
  setCamReady: PropTypes.func,
  controlledPlayback: PropTypes.bool,
  isPlaying: PropTypes.bool,
  camReady: PropTypes.bool,
  handleCamReady: PropTypes.bool,
  autoplay: PropTypes.oneOf([AUTOPLAY_DISABLED, AUTOPLAY_ENABLED, null]),
  playbackRateControls: PropTypes.bool,
  location: PropTypes.string,
  isFavorite: PropTypes.bool,
};

export default React.memo(CamPlayer);
