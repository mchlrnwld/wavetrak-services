import { useState, useRef, useCallback, useEffect } from 'react';
import { Cookies } from 'react-cookie';

import { setupConfig } from '../CamPlayer/camConfig';
import { AUTOPLAY_ENABLED } from './useCamReady';
import { addCamRewindButton } from './utils';

/**
 * @typedef {import('./CamPlayerV2').CamPlayerProps} PlayerProps
 * @param {Object} props
 * @param {PlayerProps['aspectRatio']} props.aspectRatio,
 * @param {PlayerProps['autoplay']} props.autoplay,
 * @param {PlayerProps['camera']} props.camera,
 * @param {PlayerProps['isPersistentCam']} props.isPersistentCam,
 * @param {PlayerProps['isPremium']} props.isPremium,
 * @param {PlayerProps['isPrerecorded']} props.isPrerecorded,
 * @param {PlayerProps['legacySpotId']} props.legacySpotId // legacySpotId has been depreciated,
 * @param {PlayerProps['playbackRateControls']} props.playbackRateControls,
 * @param {PlayerProps['playerId']} props.playerId,
 * @param {string} props.rewindUrl,
 * @param {PlayerProps['spotId']} props.spotId,
 * @param {PlayerProps['spotName']} props.spotName,
 * @param {PlayerProps['camera']['stillUrl']} props.stillUrl,
 * @param {PlayerProps['camera']['streamUrl']} props.streamUrl,
 */
export const useCamPlayer = ({
  aspectRatio,
  autoplay,
  camera,
  isPersistentCam,
  isPremium,
  isPrerecorded,
  playbackRateControls,
  playerId,
  rewindUrl,
  spotId,
  spotName,
  stillUrl,
  streamUrl,
}) => {
  /**
   * @type {[import('.').CamPlayer | null, import('react').Dispatch<import('react').SetStateAction<import('.').CamPlayer>>]>}
   */
  const [camPlayer, setCamPlayer] = useState(null);

  const setupPlayer = useCallback(() => {
    if (window && window.jwplayer) {
      window.jwplayer.key = window.jwplayer.defaults.key;
      const player = window.jwplayer(playerId);

      const shouldAddRewindButton = !isPrerecorded && rewindUrl;

      if (!player?.setup) return null;

      let file = streamUrl;
      const cookie = new Cookies();
      const accessToken = cookie.get('access_token');

      if (accessToken) {
        file += `?accessToken=${accessToken}`;
      }

      const withCredentials = file.indexOf('authorization') > -1;
      const shouldAutoStart =
        isPremium || isPersistentCam || autoplay === AUTOPLAY_ENABLED;

      player.setup({
        file,
        type: file.indexOf('mp4') > -1 ? 'mp4' : 'hls',
        withCredentials,
        image: stillUrl,
        aspectratio: aspectRatio,
        ...setupConfig(shouldAutoStart, playbackRateControls),
      });

      if (shouldAddRewindButton) {
        addCamRewindButton(
          player,
          spotId,
          spotName,
          camera._id,
          camera.title,
          isPremium,
          rewindUrl,
          camera.isPremium
        );
      }
      return player;
    }
    return null;
  }, [
    aspectRatio,
    autoplay,
    camera._id,
    camera.title,
    camera.isPremium,
    isPersistentCam,
    isPremium,
    isPrerecorded,
    playbackRateControls,
    playerId,
    rewindUrl,
    spotId,
    spotName,
    stillUrl,
    streamUrl,
  ]);

  /**
   * @description We use a ref because we need to be able to make an update to the cam player
   * instantaneously when it is being removed from the page, since state updates are asynchronous
   * we cannot depend on them to happen immediately. Using a ref allows us to mutate the value
   * so that whenever the cleanup effect below runs, we can be sure the ref value is up to date
   * and not running between state updates.
   * @type {import('react').MutableRefObject<import('.').CamPlayer>}
   */
  const camPlayerRef = useRef(null);

  useEffect(() => {
    camPlayerRef.current = camPlayer;
  }, [camPlayer]);

  useEffect(() => {
    if (!camPlayer) {
      const player = setupPlayer();
      setCamPlayer(player);
    }
    return () => {
      if (camPlayerRef.current?.remove) {
        camPlayerRef.current.remove();

        // Clear the ref to ensure we don't try to call remove twice
        camPlayerRef.current = null;
        setCamPlayer(null);
      }
    };
  }, [setupPlayer, camPlayer]);

  return {
    camPlayer,
    setupPlayer,
  };
};
