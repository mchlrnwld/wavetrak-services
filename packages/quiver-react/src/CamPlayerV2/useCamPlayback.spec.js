import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import { useCamPlayback } from './useCamPlayback';

const wait = (ms = 0) => new Promise((resolve) => setTimeout(resolve, ms));

describe('useCamPlayback', () => {
  let registeredHandlers = {};
  const camPlayer = {
    on: (eventName, handler) => {
      if (registeredHandlers[eventName]) {
        return registeredHandlers[eventName].push(handler);
      }
      registeredHandlers[eventName] = [handler];
      return null;
    },
  };

  const onToggleStreamingStub = sinon.stub();
  const setShowTimeoutStub = sinon.stub();
  const setupPlayerStub = sinon.stub();
  const trackPlay = sinon.stub();
  const trackPause = sinon.stub();

  afterEach(() => {
    onToggleStreamingStub.resetHistory();
    setShowTimeoutStub.resetHistory();
    setupPlayerStub.resetHistory();
    trackPlay.resetHistory();
    trackPause.resetHistory();
    registeredHandlers = {};
  });

  // eslint-disable-next-line react/prop-types
  const Component = ({ streamUrl }) => {
    const isStreaming = useCamPlayback({
      camPlayer,
      setupPlayer: setupPlayerStub,
      streamUrl: streamUrl || '',
      setShowTimeout: setShowTimeoutStub,
      onToggleStreaming: onToggleStreamingStub,
      trackPause,
      trackPlay,
    });

    return <div className={isStreaming ? 'streaming' : 'not-streaming'} />;
  };

  it('should set the listener callbacks', () => {
    mount(<Component />);
    expect(registeredHandlers.play).to.have.length(1);
    expect(registeredHandlers.pause).to.have.length(1);
    expect(registeredHandlers.idle).to.have.length(1);
  });

  it('should handle play events', () => {
    const wrapper = mount(<Component />);
    registeredHandlers.play[0]();
    wrapper.update();

    expect(wrapper.find('.streaming')).to.have.length(1);
    expect(setShowTimeoutStub).to.have.been.calledOnceWithExactly(false);
    expect(trackPlay).to.have.been.calledOnceWithExactly();
  });

  it('should handle pause events', () => {
    const wrapper = mount(<Component />);

    registeredHandlers.play[0]();
    wrapper.update();

    sinon.resetHistory();
    expect(wrapper.find('.streaming')).to.have.length(1);

    registeredHandlers.pause[0]();
    wrapper.update();
    expect(wrapper.find('.not-streaming')).to.have.length(1);
    expect(trackPause).to.have.been.calledOnceWithExactly();
  });

  it('should handle idle events', () => {
    mount(<Component />);

    registeredHandlers.idle[0]();
  });

  it('should handle a change in streamUrl', () => {
    const wrapper = mount(<Component />);
    wrapper.setProps({ streamUrl: 'newurl' });
    wrapper.update();

    expect(setShowTimeoutStub).to.have.been.calledOnceWithExactly(false);
    expect(setupPlayerStub).to.have.been.calledOnce();
  });

  it('should call onToggleStreaming when isStreaming changes', async () => {
    const wrapper = mount(<Component />);
    onToggleStreamingStub.resetHistory();

    registeredHandlers.play[0]();

    // allow event loop to clear
    await wait();
    wrapper.update();

    expect(onToggleStreamingStub).to.have.been.calledOnceWithExactly(true);
  });
});
