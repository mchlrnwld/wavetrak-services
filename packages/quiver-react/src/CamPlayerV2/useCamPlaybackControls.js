import { useCallback } from 'react';

/**
 * @param {?import('./').CamPlayer} camPlayer
 */
export const useCamPlaybackControls = (camPlayer) => {
  const pausePlayer = useCallback(() => {
    if (camPlayer?.pause) {
      camPlayer.pause();
    }
  }, [camPlayer]);

  const resumePlayer = useCallback(() => {
    if (camPlayer?.play) {
      camPlayer.play();
    }
  }, [camPlayer]);

  return { pausePlayer, resumePlayer };
};
