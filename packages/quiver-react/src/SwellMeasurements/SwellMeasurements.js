import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import round from 'lodash/round';
import { mapDegreesCardinal } from '@surfline/web-common';

const className = (index, optimalScore) =>
  classNames({
    'quiver-swell-measurements': true,
    'quiver-swell-measurements--good': optimalScore === 1,
    'quiver-swell-measurements--optimal': optimalScore === 2,
    [`quiver-swell-measurements--${index}`]: true,
  });

const directionClassNames = (directionMin) =>
  classNames({
    'quiver-swell-measurements__direction': true,
    'quiver-swell-measurements__direction--plus-minus': directionMin,
  });

const SwellMeasurements = ({
  height,
  period,
  direction,
  directionMin,
  index,
  units,
  optimalScore,
  isEmptySwell,
}) => {
  if (isEmptySwell) {
    return <div className={className(index, optimalScore)}>–</div>;
  }
  const plusMinus = directionMin && direction - directionMin;
  return (
    <div className={className(index, optimalScore)}>
      {round(height, 1)}
      <span className="quiver-swell-measurements__units">{units}</span>
      {` at ${period}s `}
      {directionMin && <br />}
      <span className={directionClassNames(!!directionMin)}>
        {mapDegreesCardinal(direction)} {round(direction)}º{' '}
        {directionMin && `(± ${round(plusMinus)}º)`}
      </span>
    </div>
  );
};

SwellMeasurements.propTypes = {
  height: PropTypes.number,
  period: PropTypes.number,
  direction: PropTypes.number,
  directionMin: PropTypes.number.isRequired,
  optimalScore: PropTypes.number,
  index: PropTypes.number.isRequired,
  units: PropTypes.string,
  isEmptySwell: PropTypes.bool,
};

SwellMeasurements.defaultProps = {
  height: null,
  period: null,
  direction: null,
  units: '',
  optimalScore: null,
  isEmptySwell: false,
};

export default SwellMeasurements;
