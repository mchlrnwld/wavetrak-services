import React from 'react';
import SwellMeasurements from './SwellMeasurements';

export default {
  title: 'SwellMeasurements',
};

export const Default = () => (
  <SwellMeasurements
    index={1}
    height={2}
    period={16}
    direction={120}
    directionMin={130}
    units="FT"
    optimalScore={1}
  />
);
