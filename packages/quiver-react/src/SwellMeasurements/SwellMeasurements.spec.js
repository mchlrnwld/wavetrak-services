/** @format */

import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import SwellMeasurements from './SwellMeasurements';

describe('components / SwellMeasurements', () => {
  it('renders swell measurements with right rounding and cardinal direction', () => {
    const wrapper = shallow(
      <SwellMeasurements
        index={1}
        height={5.25}
        units="FT"
        period={6}
        direction={182.5}
        directionMin={181}
      />
    );
    expect(wrapper.text()).to.have.string('5.3FT at 6s S 183º (± 2º)');
  });

  it('renders with correct index as class modifier', () => {
    const wrapper = shallow(
      <SwellMeasurements
        index={1}
        height={5.25}
        units="FT"
        period={6}
        direction={182.5}
        directionMin={181}
      />
    );
    const container = wrapper.find('div');
    expect(container).to.have.length(1);
    expect(container.prop('className')).to.contain(
      'quiver-swell-measurements--1'
    );
  });

  it('Renders an empty swell properly', () => {
    const wrapper2 = mount(
      <SwellMeasurements
        index={1}
        height={0}
        units="FT"
        period={0}
        direction={0}
        directionMin={0}
        isEmptySwell
      />
    );

    expect(wrapper2.prop('isEmptySwell')).to.be.true();
    const container = wrapper2.find('div');
    expect(container).to.have.length(1);
    expect(container.prop('className')).to.contain(
      'quiver-swell-measurements--1'
    );
    expect(wrapper2.text()).to.equal('–');
  });
});
