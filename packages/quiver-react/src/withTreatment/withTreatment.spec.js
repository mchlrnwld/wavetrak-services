import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import * as hooks from '../hooks/useTreatment';
import withTreatment from './withTreatment';

describe('Treatment', () => {
  const RegularTreatment = withTreatment(
    ({ treatment }) => <div className={treatment}>{treatment}</div>,
    { treatmentName: 'test-treatment' }
  );

  const NoSpinner = withTreatment(
    ({ treatment }) => <div className={treatment}>{treatment}</div>,
    { treatmentName: 'test-treatment', showSpinner: false }
  );

  const WithConfig = withTreatment(
    ({ treatment }) => (
      <div className={treatment.treatment}>{treatment.treatment}</div>
    ),
    { treatmentName: 'test-treatment', hasConfig: true }
  );

  RegularTreatment.displayName = 'Component';
  WithConfig.displayName = 'WithConfig';

  /** @type {sinon.SinonStub} */
  let useTreatmentStub;

  beforeEach(() => {
    useTreatmentStub = sinon.stub(hooks, 'useTreatment');
  });

  afterEach(() => {
    useTreatmentStub.restore();
  });

  it('should render a loading spinner until treatment is loaded', () => {
    useTreatmentStub.returns({ loading: true, treatment: null });
    const wrapper = mount(<RegularTreatment />);

    expect(useTreatmentStub).to.have.been.calledWith('test-treatment');
    expect(wrapper.find('Spinner')).to.have.length(1);
    expect(wrapper.find('.quiver-treatment-wrapper--loading')).to.have.length(
      1
    );
  });

  it('should not render a loading spinner if showSpinner is false', () => {
    useTreatmentStub.returns({ loading: true, treatment: null });
    const wrapper = mount(<NoSpinner />);

    expect(wrapper.find('Spinner')).to.have.length(0);
    expect(wrapper.find('.quiver-treatment-wrapper--loading')).to.have.length(
      1
    );
  });

  it('should render component with the treatment', () => {
    useTreatmentStub.returns({ loading: false, treatment: 'on' });
    const wrapper = mount(<RegularTreatment someExtraProp />);

    const WrappedComponent = wrapper
      .find('.quiver-treatment-wrapper')
      .childAt(0);
    expect(WrappedComponent).to.have.length(1);
    expect(WrappedComponent.prop('treatment')).to.equal('on');
    expect(WrappedComponent.prop('someExtraProp')).to.equal(true);
  });

  it('should render component with a treatment with config', async () => {
    useTreatmentStub.returns({
      loading: false,
      treatment: { treatment: 'on', config: 'config' },
    });
    const wrapper = mount(<WithConfig someExtraProp />);

    const WrappedComponent = wrapper
      .find('.quiver-treatment-wrapper')
      .childAt(0);
    expect(WrappedComponent).to.have.length(1);
    expect(WrappedComponent.prop('treatment')).to.deep.equal({
      treatment: 'on',
      config: 'config',
    });
    expect(WrappedComponent.prop('someExtraProp')).to.equal(true);
  });
});
