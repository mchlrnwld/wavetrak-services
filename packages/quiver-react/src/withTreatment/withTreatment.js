import React from 'react';
import classNames from 'classnames';

import Spinner from '../Spinner';
import { useTreatment } from '../hooks/useTreatment';
import {
  TREATMENT_WRAPPER_CLASS,
  TREATMENT_WRAPPER_LOADING_CLASS,
} from '../utils/constants';

/**
 * @param {boolean} loading
 */
const getTreatmentClass = (loading) =>
  classNames({
    [TREATMENT_WRAPPER_CLASS]: true,
    [TREATMENT_WRAPPER_LOADING_CLASS]: loading,
  });

/**
 * @template BaseProps
 * @template P
 * @param {React.ComponentType<BaseProps & { treatment: string }>} WrappedComponent
 * @param {object} options
 * @param {string} options.treatmentName
 * @param {boolean} [options.showSpinner]
 * @param {boolean} [options.hasConfig]
 * @param {boolean} [options.splitReady]
 * @returns {(props: P) => React.ReactElement<BaseProps & { treatment: string }>}
 */
const withTreatment =
  (
    WrappedComponent,
    { treatmentName, showSpinner = true, hasConfig = false, splitReady = true }
  ) =>
  (props) => {
    const { loading, treatment } = useTreatment(treatmentName, {
      hasConfig,
      splitReady,
    });

    const className = getTreatmentClass(loading);
    if (loading) {
      if (showSpinner) {
        return (
          <div className={className}>
            <Spinner />
          </div>
        );
      }
      return <div className={className} />;
    }

    return (
      <div className={className}>
        <WrappedComponent {...props} treatment={treatment} />
      </div>
    );
  };

export default withTreatment;
