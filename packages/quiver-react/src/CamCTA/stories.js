import React from 'react';
import CamCTA from './index';

export default {
  title: 'CamCTA',
};

export const Anonymous = () => (
  <div>
    <CamCTA
      backgroundImage="url(https://camstills.cdn-surfline.com/wc-cardiffreefsouth/latest_small.jpg)"
      isAnonymous
    />
  </div>
);

export const Registered = () => (
  <div>
    <CamCTA
      backgroundImage="url(https://camstills.cdn-surfline.com/wc-cardiffreefsouth/latest_small.jpg)"
      isAnonymous={false}
    />
  </div>
);
