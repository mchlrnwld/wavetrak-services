import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import CamCTA from './CamCTA';

describe('CamCTA', () => {
  const props = {
    backgroundImage:
      'url("https://camstills.cdn-surfline.com/wc-cardiffreefsouth/latest_small.jpg")',
  };
  it('should render the Cam CTA and inline a bg image for an Anonmous user', () => {
    const wrapper = mount(
      <CamCTA backgroundImage={props.backgroundImage} isAnonymous />
    );
    expect(wrapper.find('.quiver-cam-cta')).to.have.length(1);
    expect(wrapper.html()).to.contain(
      'Never hear, "You should’ve been here yesterday," again'
    );
    expect(wrapper.html()).to.contain(
      'style="background-image: url(https://camstills.cdn-surfline.com/wc-cardiffreefsouth/latest_small.jpg);"'
    );
  });
  it('should render the Cam CTA and inline a bg image for a Registered user', () => {
    const wrapper = mount(
      <CamCTA backgroundImage={props.backgroundImage} isAnonymous={false} />
    );
    expect(wrapper.find('.quiver-cam-cta')).to.have.length(1);
    expect(wrapper.html()).to.contain('Come on in, the water’s fine');
    expect(wrapper.html()).to.contain(
      'style="background-image: url(https://camstills.cdn-surfline.com/wc-cardiffreefsouth/latest_small.jpg);"'
    );
  });
});
