import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { trackClickedSubscribeCTA } from '@surfline/web-common';
import Button from '../Button';
import { useDeviceSize } from '../hooks/useDeviceSize';

const CamCTA = ({
  backgroundImage,
  isAnonymous,
  isMulti,
  segmentProperties,
}) => {
  const title = isAnonymous
    ? 'Never hear, "You should’ve been here yesterday," again.'
    : 'Come on in, the water’s fine.';
  const subTitle = isAnonymous
    ? 'Stay in the loop on surf conditions. Create a free account to access this cam up to three times per week.'
    : 'Go Premium for unlimited access to this cam (and 800 more). Get your first 15 days on us.';
  const buttonText = isAnonymous ? 'SIGN UP' : 'START FREE TRIAL';
  const buttonLink = isAnonymous ? '/create-account' : '/upgrade';
  const { isMobile } = useDeviceSize();
  const className = classNames({
    'quiver-cam-cta': true,
    'quiver-cam-cta--multi': isMulti,
  });
  const onClickHandler = () => {
    trackClickedSubscribeCTA(segmentProperties);
    window.location.assign(buttonLink);
  };
  const onClickFooterHandler = () => {
    trackClickedSubscribeCTA(segmentProperties);
    window.location.assign('/upgrade');
  };
  return (
    <div style={{ backgroundImage }} className={className}>
      <div className="quiver-aspect-ratio-content quiver-cam-cta__content">
        <h4 className="quiver-cam-cta__title">{title}</h4>
        <span className="quiver-cam-cta__subtitle">{subTitle}</span>
        <div className="quiver-cam-cta__button">
          <Button onClick={onClickHandler}>{buttonText}</Button>
        </div>
      </div>
      {isAnonymous && !isMobile && (
        <div className="quiver-aspect-ratio-content quiver-cam-cta__footer">
          <span>Ready for the full experience?</span>
          <span className="quiver-cam-cta__footer__link">
            <button type="button" onClick={onClickFooterHandler}>
              Go Premium
            </button>{' '}
            for unlimited access.
          </span>
        </div>
      )}
    </div>
  );
};

CamCTA.propTypes = {
  backgroundImage: PropTypes.string,
  isAnonymous: PropTypes.bool,
  isMulti: PropTypes.bool,
  segmentProperties: PropTypes.shape({}).isRequired,
};

CamCTA.defaultProps = {
  backgroundImage: null,
  isAnonymous: false,
  isMulti: false,
};

export default CamCTA;
