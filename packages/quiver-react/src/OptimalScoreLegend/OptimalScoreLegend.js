import React from 'react';

const OptimalScoreLegend = () => (
  <div className="quiver-optimal-score-legend">
    <div className="quiver-optimal-score-legend__box quiver-optimal-score-legend__box--optimal" />
    <div className="quiver-optimal-score-legend__text">
      Highlighted Conditions
    </div>
  </div>
);

export default OptimalScoreLegend;
