import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import OptimalScoreLegend from './OptimalScoreLegend';

describe('OptimalScoreLegend', () => {
  it('renders the component', () => {
    const wrapper = mount(<OptimalScoreLegend />);

    expect(
      wrapper.find('.quiver-optimal-score-legend__box--optimal')
    ).to.have.length(1);
    expect(wrapper.text()).to.be.equal('Highlighted Conditions');
  });
});
