import React from 'react';
import OptimalScoreLegend from './OptimalScoreLegend';

export default {
  title: 'OptimalScoreLegend',
};

export const Default = () => <OptimalScoreLegend />;

Default.story = {
  name: 'default',
};
