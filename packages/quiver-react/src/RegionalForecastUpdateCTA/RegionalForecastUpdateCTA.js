import React from 'react';
import PropTypes from 'prop-types';
import productCDN from '@surfline/quiver-assets';
import { trackClickedSubscribeCTA } from '@surfline/web-common';
import Button from '../Button';
import { useDeviceSize } from '../hooks/useDeviceSize';

const RegionalForecastUpdateCTA = ({ isAnonymous, segmentProperties }) => {
  const title = isAnonymous
    ? 'Want the inside scoop on today’s conditions?'
    : 'Come on in, the water’s fine.';
  const subtitle = isAnonymous
    ? 'Register for free to access daily reports, swell analyses, and forecast outlooks.'
    : 'Go Premium for unlimited access to expert swell analyses and forecast breakdowns.';
  const buttonText = isAnonymous ? 'SIGN UP' : 'START FREE TRIAL';
  const buttonLink = isAnonymous ? '/create-account' : '/upgrade';
  const { isMobile } = useDeviceSize();
  const backgroundImage = isMobile
    ? `${productCDN}/backgrounds/regional_report-mobile_blur.png`
    : `${productCDN}/backgrounds/regional_report-desktop_blur.png`;
  const onClickHandler = () => {
    trackClickedSubscribeCTA(segmentProperties);
    window.location.assign(buttonLink);
  };
  return (
    <div
      style={{ backgroundImage: `url(${backgroundImage})` }}
      className="quiver-regional-forecast-update-cta"
    >
      <div className="quiver-regional-forecast-update-cta__content">
        <h4 className="quiver-regional-forecast-update-cta__title">{title}</h4>
        <span className="quiver-regional-forecast-update-cta__subtitle">
          {subtitle}
        </span>
        <Button onClick={onClickHandler}>{buttonText}</Button>
      </div>
    </div>
  );
};

RegionalForecastUpdateCTA.propTypes = {
  isAnonymous: PropTypes.bool,
  segmentProperties: PropTypes.shape({}).isRequired,
};

RegionalForecastUpdateCTA.defaultProps = {
  isAnonymous: false,
};

export default RegionalForecastUpdateCTA;
