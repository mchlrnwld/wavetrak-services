import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import RegionalForecastUpdateCTA from './RegionalForecastUpdateCTA';

describe('RegionalForecastUpdateCTA', () => {
  it('should render the RegionalForecastUpdateCTA for an Anonymous user', () => {
    const wrapper = mount(<RegionalForecastUpdateCTA isAnonymous />);
    expect(wrapper.find('.quiver-regional-forecast-update-cta')).to.have.length(
      1
    );
    expect(wrapper.html()).to.contain(
      'Want the inside scoop on today’s conditions'
    );
  });
  it('should render the RegionalForecastUpdateCTA  for a Registered user', () => {
    const wrapper = mount(<RegionalForecastUpdateCTA isAnonymous={false} />);
    expect(wrapper.find('.quiver-regional-forecast-update-cta')).to.have.length(
      1
    );
    expect(wrapper.html()).to.contain('Come on in, the water’s fine');
  });
});
