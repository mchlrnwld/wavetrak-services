import React from 'react';
import RegionalForecastUpdateCTA from './index';

export default {
  title: 'RegionalForecastUpdateCTA',
};

export const Anonymous = () => (
  <div>
    <RegionalForecastUpdateCTA isAnonymous />
  </div>
);

export const Registered = () => (
  <div>
    <RegionalForecastUpdateCTA isAnonymous={false} />
  </div>
);
