import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';
import common from '@surfline/web-common';
import AddFavorites from './AddFavorites';

describe('AddFavorites', () => {
  beforeEach(() => {
    sinon.stub(common, 'setupLinkTracking');
    window.analytics.reset.resetHistory();
  });

  afterEach(() => {
    common.setupLinkTracking.restore();
    window.analytics.reset.resetHistory();
  });

  it('should render default size', () => {
    const wrapper = shallow(<AddFavorites />);
    const defaultWrapper = wrapper.find('.quiver-add-favorites');
    const largeWrapper = wrapper.find('.quiver-add-favorites--large');
    expect(defaultWrapper).to.have.length(1);
    expect(largeWrapper).to.have.length(0);
  });

  it('should render for anonymous users with a link to create account', () => {
    const wrapper = shallow(<AddFavorites />);
    const defaultWrapper = wrapper.find('a[href="/create-account"]');
    expect(defaultWrapper).to.have.length(1);
  });

  it('should render large size', () => {
    const wrapper = shallow(<AddFavorites large />);
    const defaultWrapper = wrapper.find('.quiver-add-favorites');
    const largeWrapper = wrapper.find('.quiver-add-favorites--large');
    expect(defaultWrapper).to.have.length(1);
    expect(largeWrapper).to.have.length(1);
  });

  it('should call segment event for anonymous user', () => {
    const wrapper = mount(<AddFavorites />);
    const linkWrapper = wrapper.find('a');
    expect(linkWrapper).to.have.length(1);

    linkWrapper.simulate('click');
    expect(common.setupLinkTracking).to.have.been.calledOnceWithExactly(
      linkWrapper.instance(),
      'Clicked Add Favorites',
      {
        location: 'Favorites Bar',
        loginStatus: 'loggedOut',
        userType: 'anonymous',
      }
    );
  });

  it('should call segment event for logged in user with basic entitlements', () => {
    const user = {
      email: 'test@surfline.com',
      firstName: 'Test',
      lastName: 'User',
      _id: '12345',
    };
    const entitlements = ['sl_basic'];
    const wrapper = mount(
      <AddFavorites user={user} entitlements={entitlements} />
    );
    const linkWrapper = wrapper.find('a');
    expect(linkWrapper).to.have.length(1);

    linkWrapper.simulate('click');
    expect(common.setupLinkTracking).to.have.been.calledOnceWithExactly(
      linkWrapper.instance(),
      'Clicked Add Favorites',
      {
        location: 'Favorites Bar',
        loginStatus: 'loggedIn',
        userType: 'sl_basic',
      }
    );
  });

  it('should call segment event for logged in user with premium entitlements', () => {
    const user = {
      email: 'test@surfline.com',
      firstName: 'Test',
      lastName: 'User',
      _id: '12345',
    };
    const entitlements = ['sl_premium'];
    const wrapper = mount(
      <AddFavorites user={user} entitlements={entitlements} />
    );
    const linkWrapper = wrapper.find('a');
    expect(linkWrapper).to.have.length(1);

    linkWrapper.simulate('click');
    expect(common.setupLinkTracking).to.have.been.calledOnceWithExactly(
      linkWrapper.instance(),
      'Clicked Add Favorites',
      {
        location: 'Favorites Bar',
        loginStatus: 'loggedIn',
        userType: 'sl_premium',
      }
    );
  });
});
