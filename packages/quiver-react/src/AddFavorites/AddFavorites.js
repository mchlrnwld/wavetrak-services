import React, { useRef, useMemo } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import TrackableLink from '../TrackableLink';
import AddIcon from '../icons/AddIcon';
import getAddFavoritesHref from '../helpers/getAddFavoritesHref';
import windowPropType from '../PropTypes/windowPropType';
import userPropType from '../PropTypes/userPropType';
import entitlementsPropType from '../PropTypes/entitlementsPropType';

const addFavoritesClasses = (large) => {
  const classes = {};
  classes['quiver-add-favorites'] = true;
  classes['quiver-add-favorites--large'] = large;
  return classnames(classes);
};

const AddFavorites = ({ large, user, entitlements, window }) => {
  const loginStatus = user ? 'loggedIn' : 'loggedOut';
  let userType = 'anonymous';

  if (user) {
    userType =
      entitlements.indexOf('sl_premium') > -1 ? 'sl_premium' : 'sl_basic';
  }

  const linkRef = useRef();
  const eventProperties = useMemo(
    () => ({
      location: 'Favorites Bar',
      loginStatus,
      userType,
    }),
    [loginStatus, userType]
  );

  return (
    <TrackableLink
      ref={linkRef}
      eventName="Clicked Add Favorites"
      eventProperties={eventProperties}
    >
      <a
        ref={linkRef}
        className={addFavoritesClasses(large)}
        href={getAddFavoritesHref(window ? window.location.href : null, user)}
      >
        <div className="quiver-add-favorites__wrapper">
          <AddIcon />
          <div className="quiver-add-favorites__wrapper__content">
            <h5>Add favorites</h5>
            <p>Quickly access the spots you care about most.</p>
          </div>
        </div>
      </a>
    </TrackableLink>
  );
};

AddFavorites.propTypes = {
  large: PropTypes.bool,
  user: userPropType,
  entitlements: entitlementsPropType,
  window: windowPropType,
};

AddFavorites.defaultProps = {
  large: false,
  user: null,
  entitlements: [],
  window: null,
};

export default AddFavorites;
