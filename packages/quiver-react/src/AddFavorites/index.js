import withWindow from '../withWindow';
import AddFavorites from './AddFavorites';

export default withWindow(AddFavorites);
