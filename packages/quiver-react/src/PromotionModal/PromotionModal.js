import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { trackEvent } from '@surfline/web-common';
import Button from '../Button';
import ModalContainer from '../ModalContainer';
import windowPropType from '../PropTypes/windowPropType';
import userPropType from '../PropTypes/userPropType';
import getSignInHref from '../helpers/getSignInHref';

/**
 * @description The PromotionModal component is used for showing modals related to promotions
 * (ie: free premium weekend) and rendering content returned from backplane relevant to the
 * current promotion.
 *
 * @param {object} props
 * @param {boolean} props.isOpen boolean flag determining whether or not show the modal
 * @param {string} props.header Header text for the modal
 * @param {string} props.sponsorName Name of the sponsor for the current promotion
 * @param {object} props.imageUrls Urls for the image and retina images
 * @param {function} props.onClose Function that is called upon closing the modal
 * @param {string} props.description Optional prop for a description string for the promotion
 */
class PromotionModal extends Component {
  /**
   * @description Method used to dynamically extract responsive image sizes
   * and concatenate the URL's into a valid string for the srcSet attribute
   * @param {Array<string>} sizes The imageUrl image size keys to extract (2x, 3x, etc...)
   */
  getSrcSetString = (sizes) => {
    const { imageUrls } = this.props;

    const srcSetStrings = sizes.map((size) =>
      imageUrls[size] ? `${imageUrls[size]} ${size}` : null
    );

    return srcSetStrings.filter((value) => !!value).join(', ');
  };

  getSignInUrl = () => {
    const { window } = this.props;
    return getSignInHref(window ? window.location.href : null);
  };

  signInClick = () => {
    trackEvent('Clicked Link', {
      linkLocation: 'premium open house',
      linkName: 'sign in',
    });
  };

  signUpClick = () => {
    const { window } = this.props;
    trackEvent(
      'Clicked Link',
      {
        linkLocation: 'premium open house',
        linkName: 'sign up',
      },
      null,
      () => window.location.assign('/create-account')
    );
  };

  render() {
    const {
      onClose,
      header,
      sponsorName,
      isOpen,
      imageUrls,
      description,
      userDetails,
    } = this.props;

    return (
      <ModalContainer
        onClose={onClose}
        contentLabel={`${sponsorName} Premium Open House`}
        isOpen={isOpen}
      >
        <div className="quiver-promotion-modal">
          <div className="quiver-promotion-modal__header">
            <h4>{header}</h4>
          </div>
          <div className="quiver-promotion-modal__image">
            <img
              alt=""
              src={imageUrls['1x']}
              srcSet={this.getSrcSetString(['2x', '3x'])}
            />
          </div>
          {description && (
            <p className="quiver-promotion-modal__description">{description}</p>
          )}
          {userDetails ? (
            <div className="quiver-promotion-modal__cta">
              <Button onClick={onClose}>Got it, thanks</Button>
            </div>
          ) : (
            <div className="quiver-promotion-modal__cta">
              <Button onClick={this.signUpClick}>Sign Up</Button>
              <span className="quiver-promotion-modal__cta--sign-in">
                Already have an acccount?{' '}
                <a onClick={this.signInClick} href={this.getSignInUrl()}>
                  Sign In
                </a>
              </span>
            </div>
          )}
        </div>
      </ModalContainer>
    );
  }
}

PromotionModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  header: PropTypes.string.isRequired,
  sponsorName: PropTypes.string.isRequired,
  imageUrls: PropTypes.shape({
    '1x': PropTypes.string.isRequired,
    '2x': PropTypes.string,
    '3x': PropTypes.string,
  }).isRequired,
  onClose: PropTypes.func,
  description: PropTypes.string,
  window: windowPropType,
  userDetails: userPropType,
};

PromotionModal.defaultProps = {
  onClose: null,
  description: null,
  userDetails: null,
  window: null,
};

export default PromotionModal;
