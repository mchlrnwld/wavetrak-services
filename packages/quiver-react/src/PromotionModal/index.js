import withWindow from '../withWindow';
import PromotionModal from './PromotionModal';

export default withWindow(PromotionModal);
