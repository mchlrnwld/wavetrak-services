import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import common from '@surfline/web-common';
import ModalContainer from '../ModalContainer';
import PromotionModal from './PromotionModal';

describe('PromotionModal', () => {
  beforeEach(() => {
    sinon.stub(common, 'trackEvent');
  });

  afterEach(() => {
    common.trackEvent.restore();
  });

  it('should render with appropriate props', () => {
    const onClose = sinon.spy();
    const wrapper = shallow(
      <PromotionModal
        isOpen
        sponsorName="sponsor"
        onClose={onClose}
        imageUrls={{ '1x': 'http://url.com' }}
        header="header"
      />
    );
    const modal = wrapper.find(ModalContainer);
    expect(modal).to.have.length(1);
    expect(wrapper.prop('isOpen')).to.equal(true);
    expect(wrapper.prop('contentLabel')).to.equal('sponsor Premium Open House');
    expect(wrapper.prop('onClose')).to.equal(onClose);
  });

  it('should render with the appropriate CTAs for logged in users', () => {
    const wrapper = shallow(
      <PromotionModal
        userDetails={{
          email: 'test@test.com',
          firstName: 'test',
          lastName: 'tester',
          id: '123',
        }}
        imageUrls={{ '1x': 'http://url.com' }}
        header="header"
        isOpen
        sponsorName="sponsorName"
      />
    );
    const cta = wrapper.find('.quiver-promotion-modal__cta').children();
    expect(cta).to.have.length(1);
    expect(cta.render().text()).to.equal('Got it, thanks');
  });

  it('should render with the appropriate CTAs for logged out users', () => {
    const wrapper = shallow(
      <PromotionModal
        imageUrls={{ '1x': 'http://url.com' }}
        header="header"
        isOpen
        sponsorName="sponsorName"
      />
    );
    const cta = wrapper.find('.quiver-promotion-modal__cta').children();
    const signInLink = wrapper.find('.quiver-promotion-modal__cta--sign-in');
    expect(cta).to.have.length(2);
    expect(signInLink).to.have.length(1);
  });

  it('should render an image with the appropriate src and srcSet', () => {
    const wrapper = shallow(
      <PromotionModal
        imageUrls={{
          '1x': 'http://1x.com',
          '2x': 'http://2x.com',
          '3x': 'http://3x.com',
        }}
        header="header"
        isOpen
        sponsorName="sponsorName"
      />
    );
    let image = wrapper.find('img');
    expect(image.prop('src')).to.equal('http://1x.com');
    expect(image.prop('srcSet')).to.equal('http://2x.com 2x, http://3x.com 3x');
    wrapper.setProps({
      imageUrls: {
        '1x': 'http://1x.com',
        '2x': 'http://2x.com',
      },
    });
    image = wrapper.find('img');
    expect(image.prop('src')).to.equal('http://1x.com');
    expect(image.prop('srcSet')).to.equal('http://2x.com 2x');
  });

  it('Should get the proper sign in URL', () => {
    const window = {
      location: {
        href: 'http://test.com',
      },
    };

    const wrapper = shallow(
      <PromotionModal
        window={window}
        imageUrls={{
          '1x': 'http://1x.com',
          '2x': 'http://2x.com',
          '3x': 'http://3x.com',
        }}
        header="header"
        isOpen
        sponsorName="sponsorName"
      />
    );

    const component = wrapper.instance();
    expect(component.getSignInUrl()).to.equal(
      '/sign-in?redirectUrl=http%3A%2F%2Ftest.com'
    );

    window.location.href = null;
    expect(component.getSignInUrl()).to.equal('/sign-in');
  });

  it('Should handle clicks properly', () => {
    const window = {
      location: {
        assign: (url) => url,
      },
    };

    const wrapper = shallow(
      <PromotionModal
        window={window}
        imageUrls={{ '1x': 'http://1x.com' }}
        header="header"
        isOpen
        sponsorName="sponsorName"
      />
    );

    const component = wrapper.instance();
    const signUpClickSpy = sinon.spy(component, 'signUpClick');
    const signInClickSpy = sinon.spy(component, 'signInClick');
    const locationSpy = sinon.spy(window.location, 'assign');

    try {
      component.forceUpdate();
      wrapper.update();
      component.signInClick();
      expect(signInClickSpy).to.have.been.calledOnce();
      expect(common.trackEvent).to.have.been.calledOnce();

      common.trackEvent.callsArgWith(3, null);
      component.signUpClick();
      expect(signUpClickSpy).to.have.been.calledOnce();
      expect(common.trackEvent).to.have.been.calledTwice();
      expect(locationSpy).to.have.been.calledOnce();
      expect(locationSpy).to.have.been.calledWith('/create-account');
    } finally {
      signUpClickSpy.restore();
      signInClickSpy.restore();
      locationSpy.restore();
    }
  });
});
