import React from 'react';
import PropTypes from 'prop-types';
import PRODUCT_CDN from '@surfline/quiver-assets';
import Button from '../Button';
import PremiumTagLarge from '../icons/PremiumTagLarge';

const wavesUrl = `${PRODUCT_CDN}/backgrounds/paywall-background.png`;

const ContentCardPaywall = ({ funnelUrl, onClick }) => (
  <div
    className="quiver-content-card__paywall"
    style={{ backgroundImage: `url(${wavesUrl})` }}
  >
    <PremiumTagLarge />
    <h5 className="quiver-content-card__paywall__message">
      Want access to Premium Analysis from the Surfline forecast team?
    </h5>
    <Button href={funnelUrl} onClick={onClick}>
      Start Free Trial
    </Button>
  </div>
);

ContentCardPaywall.propTypes = {
  funnelUrl: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

ContentCardPaywall.defaultProps = {
  onClick: null,
};

export default ContentCardPaywall;
