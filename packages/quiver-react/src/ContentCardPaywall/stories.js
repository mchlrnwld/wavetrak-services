import React from 'react';
import ContentCardPaywall from './index';

export default {
  title: 'ContentCardPaywall',
};

export const Default = () => <ContentCardPaywall funnelUrl="/somewhere" />;

Default.story = {
  name: 'default',
};
