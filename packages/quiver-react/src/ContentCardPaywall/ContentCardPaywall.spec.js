import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import ContentCardPaywall from './ContentCardPaywall';

describe('ContentCardPaywall', () => {
  it('should render the paywall with clickable link', () => {
    const onClick = sinon.stub();
    const wrapper = shallow(
      <ContentCardPaywall funnelUrl="#funnelurl" onClick={onClick} />
    );

    const message = wrapper.find('.quiver-content-card__paywall__message');
    expect(message.text()).to.equal(
      'Want access to Premium Analysis from the Surfline forecast team?'
    );
  });
});
