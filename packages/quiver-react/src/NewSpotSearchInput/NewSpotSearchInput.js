import React from 'react';
import PropTypes from 'prop-types';

import SearchIcon from '../icons/SearchIcon';

const NewSpotSearchInput = ({ inputProps }) => (
  <div className="quiver-new-navigation-bar__search__wrapper">
    <div className="quiver-new-navigation-bar__search__icon__container">
      <SearchIcon />
    </div>
    <div className="quiver-new-navigation-bar__search__input__container">
      <input
        {...inputProps}
        className="quiver-new-navigation-bar__search__wrapper__input"
        type="text"
      />
    </div>
  </div>
);

NewSpotSearchInput.propTypes = {
  inputProps: PropTypes.shape().isRequired,
};

export default NewSpotSearchInput;
