/* eslint-disable no-unused-expressions */
import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import SwellEventCard from './SwellEventCard';

describe('SwellEventCard', () => {
  const props = {
    swellEvent: {
      basins: [],
      updatedAt: +new Date(),
    },
    onClickCard: sinon.spy(),
  };

  afterEach(() => {
    props.onClickCard.resetHistory();
  });

  it('should render the swell event card', () => {
    const wrapper = mount(<SwellEventCard {...props} />);
    expect(wrapper).to.have.length(1);
    wrapper.simulate('click');
    expect(props.onClickCard).to.have.been.calledOnce();
    expect(wrapper.find('.quiver-swell-event-card__details__top')).to.not.exist;
    expect(wrapper.find('.quiver-swell-event-card__thumbnail')).to.not.exist;
    expect(wrapper.find('.quiver-swell-event-card__locations__location')).to
      .exist;
  });

  it('should render the swell event card with a title tag', () => {
    const newProps = {
      swellEvent: {
        ...props.swellEvent,
        titleTag: 'SWELL ALERT',
        updatedAt: +new Date(),
      },
    };
    const wrapper = mount(<SwellEventCard {...props} {...newProps} />);
    expect(wrapper.find('.quiver-swell-event-card__details__top')).to.exist;
    expect(wrapper.find('.quiver-swell-event-card__locations__location')).to.not
      .exist;
    expect(wrapper.find('.quiver-swell-event-card__20ft-logo')).to.not.exist;
    expect(wrapper.find('.quiver-swell-event-card__live')).to.not.exist;
  });

  it('should render the swell event card with thumbnail', () => {
    const newProps = {
      swellEvent: {
        ...props.swellEvent,
        thumbnailUrl: 'http://',
      },
    };
    const wrapper = mount(<SwellEventCard {...props} {...newProps} />);
    expect(wrapper.find('.quiver-swell-event-card__thumbnail')).to.exist;
    expect(wrapper.find('.quiver-swell-event-card__live')).to.not.exist;
  });

  it('should render the swell event card without thumbnail', () => {
    const newProps = {
      swellEvent: {
        ...props.swellEvent,
        thumbnailUrl: '',
      },
    };
    const wrapper = mount(<SwellEventCard {...props} {...newProps} />);
    expect(wrapper.find('.quiver-swell-event-card__thumbnail')).to.not.exist;
  });

  it('should render the swell event card with live indicator', () => {
    const newProps = {
      swellEvent: {
        ...props.swellEvent,
        isLive: true,
      },
    };
    const wrapper = mount(<SwellEventCard {...props} {...newProps} />);
    expect(wrapper.find('.quiver-swell-event-card__thumbnail')).to.exist;
    expect(wrapper.find('.quiver-swell-event-card__live')).to.exist;
  });

  it('should render the swell event card with 20ft+ logo, live icon and basins', () => {
    const newProps = {
      swellEvent: {
        ...props.swellEvent,
        isLive: true,
        titleTag: '20ft+',
        basins: ['north_pacific'],
      },
    };
    const wrapper = mount(<SwellEventCard {...props} {...newProps} />);
    expect(wrapper.find('.quiver-swell-event-card__20ft-logo')).to.exist;
    expect(wrapper.find('.quiver-swell-event-card__live')).to.exist;
    expect(
      wrapper.find('.quiver-swell-event-card__locations__location').text()
    ).to.equal('North Pacific');
  });
});
