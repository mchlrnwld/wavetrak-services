import React from 'react';
import SwellEventCard from './index';

export default {
  title: 'SwellEventCard',
  component: SwellEventCard,
};

const Template = (args) => <SwellEventCard {...args} />;

export const Default = Template.bind({});

Default.story = {
  name: 'Default Alert',
};

const defaultSwellEvent = {
  basins: ['north_pacific', 'south_atlantic', 'mediterranean', 'great_lakes'],
  id: 2772,
  isLive: false,
  name: 'European Winter Cruise',
  newWindow: false,
  permalink:
    'https://sandbox.surfline.com/surf-news/swell-event/hurricane-marie/2772',
  summary:
    'Realtime: Another XL swell unloads across Europe and at the Nazare Challenge',
  thumbnailUrl:
    'https://d14fqx6aetz9ka.cloudfront.net/wp-content/uploads/2022/02/10035848/Guincho_thumb.jpg',
  titleTag: 'Alert',
  updatedAt: '2022-01-19T19:31:09+00:00',
};

Default.args = {
  swellEvent: defaultSwellEvent,
  onClickCard: (swellEvent) => {
    console.log('SwellEventCard > Click', swellEvent);
  },
};

export const TwentyFootPlus = Template.bind({});

TwentyFootPlus.story = {
  name: '20ft Plus',
};

TwentyFootPlus.args = {
  swellEvent: {
    ...defaultSwellEvent,
    name: 'Watch now: North Pacific Kickoff',
    summary: 'We are live at Jaws, click here to watch.',
    titleTag: '20ft+',
    isLive: true,
    thumbnailUrl:
      'https://d14fqx6aetz9ka.cloudfront.net/wp-content/uploads/2022/01/08091734/NazThumbCJ.jpg',
  },
  onClickCard: (swellEvent) => {
    console.log('SwellEventCard > Click', swellEvent);
  },
};
