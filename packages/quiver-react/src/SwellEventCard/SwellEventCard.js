import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { formatArticleDate } from '@surfline/web-common';

/* istanbul ignore next */
const formatBasinText = (basin) =>
  basin
    .split('_')
    .map((word) => word?.charAt(0)?.toUpperCase() + word.slice(1))
    .join(' ');

const SwellEventCard = ({ swellEvent, onClickCard }) => {
  const is20Foot = swellEvent?.titleTag?.toUpperCase() === '20FT+';
  const showThumbnail =
    (swellEvent?.thumbnailUrl && swellEvent?.thumbnailUrl !== '') ||
    swellEvent?.isLive;

  return (
    /* eslint-disable-next-line react/jsx-no-target-blank */
    <a
      className={classnames('quiver-swell-event-card', {
        'quiver-swell-event-card--20ft': is20Foot,
      })}
      href={swellEvent.permalink}
      target={swellEvent.newWindow ? '_blank' : ''}
      rel={swellEvent.newWindow ? 'noreferrer' : ''}
      onClick={() => onClickCard(swellEvent)}
    >
      {is20Foot && (
        <div className="quiver-swell-event-card__20ft-logo">
          <svg
            className="quiver-swell-event-card__20ft-logo__svg"
            fill="none"
            height="37"
            viewBox="0 0 94 37"
            width="94"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M0 17.1386L14.0112 10.7159V7.65295H17.1247V12.6657L0 20.5099V36.7339C9.9245 34.9145 15.1164 33.9292 20.4445 32.609C31.5054 30.1448 24.2276 18.1279 15.5678 25.6371C20.6488 19.6527 25.9011 17.8888 31.1357 18.457V0H0V17.1386Z"
              fill="white"
            />
            <path
              d="M70.8358 6.12232H77.8413V3.82637H70.8358V2.29595H79.3981V0H68.5006V9.18358H70.8358V6.12232Z"
              fill="white"
            />
            <path
              d="M82.5115 0V2.29595H86.7927V9.18358H89.1279V2.29595H93.4091V0H82.5115Z"
              fill="white"
            />
            <path
              d="M75.117 12.2446H86.7929V18.7495H93.4091V30.229H86.7929V36.7339H75.117V30.229H68.5006V18.7495H75.117V12.2446Z"
              fill="white"
            />
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M51.6707 28.8153C46.1246 24.8741 40.2231 20.6804 34.2492 19.0576V0H65.3849V36.7339C61.2242 35.6039 56.587 32.3088 51.6707 28.8153ZM51.3739 7.65295H48.2604V24.0512C49.3073 24.7246 50.3463 25.4249 51.3739 26.1328V7.65295Z"
              fill="white"
            />
          </svg>
        </div>
      )}
      <div className="quiver-swell-event-card__details">
        <div className="quiver-swell-event-card__details__top">
          <div className="quiver-swell-event-card__tag">Swell Event</div>
          <span className="quiver-swell-event-card__updated">
            {formatArticleDate(new Date(swellEvent.updatedAt) / 1000)}
          </span>
        </div>
        <h3 className="quiver-swell-event-card__title">{swellEvent.name}</h3>
        <p className="quiver-swell-event-card__summary">{swellEvent.summary}</p>
        {swellEvent?.basins?.length > 0 && (
          <ul className="quiver-swell-event-card__locations">
            {swellEvent.basins.map((basin) => (
              <li
                className="quiver-swell-event-card__locations__location"
                key={basin}
              >
                {formatBasinText(basin)}
              </li>
            ))}
          </ul>
        )}
      </div>
      {showThumbnail && (
        <div className="quiver-swell-event-card__thumbnail">
          <div
            className="quiver-swell-event-card__thumbnail__img"
            style={{ backgroundImage: `url(${swellEvent?.thumbnailUrl})` }}
          />
          {swellEvent?.isLive && (
            <svg
              className="quiver-swell-event-card__live"
              fill="none"
              height="32"
              viewBox="0 0 72 32"
              width="72"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect
                fill="black"
                fillOpacity="0.2"
                height="30.5"
                rx="4.25"
                stroke="white"
                strokeWidth="1.5"
                width="70.5"
                x="0.75"
                y="0.75"
              />
              <path
                d="M24.4683 10.44V21H31.1883V19.2H26.5083V10.44H24.4683ZM36.2847 21V10.44H34.2447V21H36.2847ZM46.2454 10.44L43.5454 18.135L40.8454 10.44H38.5054L42.5854 21H44.5054L48.5854 10.44H46.2454ZM57.7691 12.24V10.44H50.8091V21H57.7691V19.2H52.8491V16.56H56.8091V14.88H52.8491V12.24H57.7691Z"
                fill="white"
              />
              <circle
                className="quiver-swell-event-card__live__dot"
                cx="14"
                cy="16"
                fill="#E80212"
                r="4"
              />
            </svg>
          )}
        </div>
      )}
    </a>
  );
};

SwellEventCard.propTypes = {
  swellEvent: PropTypes.shape({
    basins: PropTypes.arrayOf(PropTypes.string).isRequired,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    isLive: PropTypes.bool,
    name: PropTypes.string,
    newWindow: PropTypes.bool,
    permalink: PropTypes.string,
    summary: PropTypes.string,
    thumbnailUrl: PropTypes.string,
    titleTag: PropTypes.string,
    updatedAt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }).isRequired,
  onClickCard: PropTypes.func,
};

SwellEventCard.defaultProps = {
  onClickCard: () => {},
};

export default SwellEventCard;
