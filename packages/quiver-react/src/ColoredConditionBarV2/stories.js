import React from 'react';
import ColoredConditionBarV2 from './ColoredConditionBarV2';

export default {
  title: 'ColoredConditionBarV2',
};

export const V2FlatDisplayVeryPoor = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBarV2 value="FLAT" />
  </div>
);

V2FlatDisplayVeryPoor.story = {
  name: 'V2 FLAT (display VERY_POOR)',
};

export const V2VeryPoorDisplayVeryPoor = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBarV2 value="VERY_POOR" />
  </div>
);

V2VeryPoorDisplayVeryPoor.story = {
  name: 'V2 VERY_POOR (display VERY_POOR)',
};

export const V2PoorDisplayPoor = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBarV2 value="POOR" />
  </div>
);

V2PoorDisplayPoor.story = {
  name: 'V2 POOR (display POOR)',
};

export const V2PoorToFairDisplayFair = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBarV2 value="POOR_TO_FAIR" />
  </div>
);

V2PoorToFairDisplayFair.story = {
  name: 'V2 POOR_TO_FAIR (display FAIR)',
};

export const V2FairDisplayGood = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBarV2 value="FAIR" />
  </div>
);

V2FairDisplayGood.story = {
  name: 'V2 FAIR (display GOOD)',
};

export const V2FairToGoodDisplayVeryGood = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBarV2 value="FAIR_TO_GOOD" />
  </div>
);

V2FairToGoodDisplayVeryGood.story = {
  name: 'V2 FAIR_TO_GOOD (display VERY_GOOD)',
};

export const V2GoodDisplayVeryGood = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBarV2 value="GOOD" />
  </div>
);

V2GoodDisplayVeryGood.story = {
  name: 'V2 GOOD (display VERY_GOOD)',
};

export const V2VeryGoodDisplayVeryGood = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBarV2 value="VERY_GOOD" />
  </div>
);

V2VeryGoodDisplayVeryGood.story = {
  name: 'V2 VERY_GOOD (display VERY_GOOD)',
};

export const V2EpicDisplayVeryGood = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBarV2 value="EPIC" />
  </div>
);

V2EpicDisplayVeryGood.story = {
  name: 'V2 EPIC (display VERY_GOOD)',
};

export const V2None = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBarV2 value="NONE" />
  </div>
);

V2None.story = {
  name: 'V2 NONE',
};

export const V2ReportComingSoon = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBarV2 expired value="FAIR" />
  </div>
);

V2ReportComingSoon.story = {
  name: 'V2 Report Coming Soon',
};
