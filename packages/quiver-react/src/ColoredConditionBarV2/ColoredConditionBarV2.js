import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';
import {
  conditionClassModifier,
  conditionEnumToText,
} from '@surfline/web-common';

import applyConditionVariation from '../helpers/applyConditionVariation';

const className = (value, expired) =>
  classnames({
    'quiver-colored-condition-bar': true,
    'quiver-colored-condition-bar-v2': true,
    [`quiver-colored-condition-bar-v2--${conditionClassModifier(
      applyConditionVariation(value)
    )}`]: true,
    'quiver-colored-condition-bar-v2--expired': expired,
  });

const ColoredConditionBarV2 = ({ value, expired }) => {
  const conditionText = expired
    ? 'Report Coming Soon'.toUpperCase()
    : conditionEnumToText(applyConditionVariation(value));
  return (
    <div className={className(value, expired)}>
      {value === 'NONE'
        ? 'conditions unobservable'.toUpperCase()
        : conditionText}
    </div>
  );
};

ColoredConditionBarV2.propTypes = {
  value: PropTypes.string.isRequired,
  expired: PropTypes.string,
};

ColoredConditionBarV2.defaultProps = {
  expired: false,
};

export default ColoredConditionBarV2;
