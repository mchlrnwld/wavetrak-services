import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import BaseMenu from './BaseMenu';

describe('BaseMenu', () => {
  it('should attach position modifier class name if passed a position', () => {
    const wrapper = shallow(<BaseMenu position="right" />);
    expect(wrapper.prop('className')).to.contain(
      'quiver-base-menu--open-right'
    );
  });

  it('should render a div with children', () => {
    const wrapper = shallow(
      <BaseMenu>
        <p>For Testing</p>
      </BaseMenu>
    );
    expect(wrapper.containsMatchingElement(<p>For Testing</p>)).to.equal(true);
  });
});
