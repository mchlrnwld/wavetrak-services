import React from 'react';
import PropTypes from 'prop-types';

const BaseMenu = ({ position, children }) => (
  <div className={`quiver-base-menu quiver-base-menu--open-${position}`}>
    {children}
  </div>
);

BaseMenu.propTypes = {
  position: PropTypes.string,
  children: PropTypes.node,
};

BaseMenu.defaultProps = {
  children: null,
  position: 'left',
};

export default BaseMenu;
