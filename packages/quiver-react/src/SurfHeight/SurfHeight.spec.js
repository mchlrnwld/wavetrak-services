import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SurfHeight from './SurfHeight';

describe('SurfHeight', () => {
  it('should display min and max heights with correct units', () => {
    const wrapper = shallow(<SurfHeight min={2} max={3} units="FT" />);
    const unitsWrapper = wrapper.find('sup');
    expect(wrapper.text()).to.equal('2-3FT');
    expect(unitsWrapper.text()).to.equal('FT');
  });

  it('should display one height if min and max are the same', () => {
    const wrapper = shallow(<SurfHeight min={2} max={2} units="FT" />);
    expect(wrapper.text()).to.equal('2FT');
  });

  it('should display FLAT if min and max less than 1 FT', () => {
    expect(shallow(<SurfHeight min={0} max={0} units="FT" />).text()).to.equal(
      'Flat'
    );
    expect(
      shallow(<SurfHeight min={0} max={0.4} units="FT" />).text()
    ).to.equal('Flat');
    expect(shallow(<SurfHeight min={1} max={1} units="FT" />).text()).to.equal(
      '1FT'
    );

    expect(shallow(<SurfHeight min={0} max={0} units="M" />).text()).to.equal(
      'Flat'
    );
    expect(shallow(<SurfHeight min={0} max={0.3} units="M" />).text()).to.equal(
      'Flat'
    );
    expect(
      shallow(<SurfHeight min={0.3} max={0.3} units="M" />).text()
    ).to.equal('0.3M');
  });

  it('should display + if plus is true', () => {
    const wrapper = shallow(<SurfHeight min={1} max={2} units="FT" plus />);
    expect(wrapper.text()).to.equal('1-2FT+');
  });
});
