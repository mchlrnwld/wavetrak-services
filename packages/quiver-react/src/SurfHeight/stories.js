import React from 'react';
import SurfHeight from './index';

export default {
  title: 'SurfHeight',
};

export const Flat = () => <SurfHeight min={0} max={0} units="ft" />;

Flat.story = {
  name: 'flat',
};

export const Feet = () => <SurfHeight min={2} max={3} units="ft" />;

Feet.story = {
  name: 'feet',
};

export const Meters = () => <SurfHeight min={0.6} max={0.9} units="m" />;

Meters.story = {
  name: 'meters',
};

export const Plus = () => <SurfHeight min={4} max={5} units="m" plus />;

Plus.story = {
  name: 'plus',
};

export const Equal = () => <SurfHeight min={0.6} max={0.6} units="m" />;

Equal.story = {
  name: 'equal',
};
