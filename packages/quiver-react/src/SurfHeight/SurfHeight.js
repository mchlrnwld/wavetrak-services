import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import conditionIsFlat from '../helpers/conditionIsFlat';
import getUnitType from '../helpers/getUnitType';

const SurfHeight = ({ min, max, units, plus = false }) => {
  const isFlat = conditionIsFlat(min, max, units);
  const className = classNames({
    'quiver-surf-height': true,
    'quiver-surf-height--flat': isFlat,
  });

  if (isFlat) {
    return <span className={className}>Flat</span>;
  }

  const range = min === max ? max : `${min}-${max}`;

  return (
    <span className={className}>
      {range}
      <sup>{getUnitType(units)}</sup>
      {plus ? '+' : ''}
    </span>
  );
};

SurfHeight.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  units: PropTypes.string,
  plus: PropTypes.bool,
};

SurfHeight.defaultProps = {
  min: 0,
  max: 0,
  units: '',
  plus: false,
};
export default SurfHeight;
