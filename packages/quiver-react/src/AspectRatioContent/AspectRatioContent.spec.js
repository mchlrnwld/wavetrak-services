import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import AspectRatioContent from './AspectRatioContent';

describe('AspectRatioContent', () => {
  it('should render children', () => {
    const wrapper = shallow(
      <AspectRatioContent>
        <p>This is a child.</p>
      </AspectRatioContent>
    );
    expect(wrapper.html()).to.contain('This is a child');
  });
});
