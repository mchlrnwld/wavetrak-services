import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import TooltipDatetime from './TooltipDatetime';

describe('components / TooltipDatetime', () => {
  it('renders date and time', () => {
    const wrapper = shallow(
      <TooltipDatetime utcOffset={-8} timestamp={1518076800} />
    );

    const date = wrapper.find('.quiver-tooltip-datetime__date');
    expect(date).to.have.length(1);
    expect(date.text()).to.equal('Thu 02/08 ');

    const time = wrapper.find('.quiver-tooltip-datetime__time');
    expect(time).to.have.length(1);
    expect(time.text()).to.equal('12AM');
  });
});
