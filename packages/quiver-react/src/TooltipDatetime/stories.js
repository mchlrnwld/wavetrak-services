import React from 'react';
import TooltipDatetime from './TooltipDatetime';

export default {
  title: 'TooltipDatetime',
};

export const Default = () => (
  <TooltipDatetime utcOffset={-8} timestamp={1518076800} />
);

Default.story = {
  name: 'default',
};

export const ShowMinutes = () => (
  <TooltipDatetime utcOffset={-8} timestamp={1518076800} showMinutes />
);

ShowMinutes.story = {
  name: 'showMinutes',
};
