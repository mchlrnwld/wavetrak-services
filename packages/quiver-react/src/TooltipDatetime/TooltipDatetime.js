/** @prettier */

import PropTypes from 'prop-types';
import React from 'react';
import { format } from 'date-fns';
import { getLocalDate } from '@surfline/web-common';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';

const TooltipDatetime = ({ timestamp, utcOffset, showMinutes, dateFormat }) => {
  const localDate = getLocalDate(timestamp, utcOffset);
  const timeFormat = showMinutes ? 'h:mmaaa' : 'haa';
  return (
    <div className="quiver-tooltip-datetime">
      <span className="quiver-tooltip-datetime__date">
        {format(localDate, dateFormat === 'MDY' ? 'iii MM/dd' : 'iii dd/MM')}{' '}
      </span>
      <span className="quiver-tooltip-datetime__time">
        {format(localDate, timeFormat)}
      </span>
    </div>
  );
};

TooltipDatetime.propTypes = {
  timestamp: PropTypes.number.isRequired,
  utcOffset: PropTypes.number.isRequired,
  showMinutes: PropTypes.bool,
  dateFormat: dateFormatPropType,
};

TooltipDatetime.defaultProps = {
  showMinutes: false,
  dateFormat: defaultDateFormat,
};

export default TooltipDatetime;
