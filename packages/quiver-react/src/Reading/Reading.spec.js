import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Reading from './Reading';

describe('components / Reading', () => {
  it('renders value and superscripted units', () => {
    const wrapper = shallow(<Reading value={5.6} units="FT" />);
    expect(wrapper.text()).to.equal('5.6FT');
    expect(wrapper.find('sup').text()).to.equal('FT');
  });
});
