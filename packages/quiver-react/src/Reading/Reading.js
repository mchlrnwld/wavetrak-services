import PropTypes from 'prop-types';
import React from 'react';

const Reading = ({ value, units }) => (
  <span className="quiver-reading">
    {value}
    <sup>{units}</sup>
  </span>
);

Reading.propTypes = {
  value: PropTypes.number.isRequired,
  units: PropTypes.string.isRequired,
};

export default Reading;
