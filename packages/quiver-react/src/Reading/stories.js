import React from 'react';
import Reading from './Reading';

export default {
  title: 'Reading',
};

export const Default = () => <Reading units="FT" value={10} />;
