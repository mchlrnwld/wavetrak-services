import React from 'react';
import PropTypes from 'prop-types';

import recentlyVisitedPropType from '../PropTypes/recentlyVisitedPropType';
import navigationSettingsPropType from '../PropTypes/navigationSettingsPropType';
import serviceConfigurationPropType from '../PropTypes/serviceConfigurationPropType';
import RecentlyVisited from '../RecentlyVisited';
import TaxonomyNavigator from '../TaxonomyNavigator';
import { camsReportsLinks } from '../NewNavigationBar/defaultLinks';

const HIERARCHY = ['🌍', 'Continent', 'Country', 'Region', 'Area'];

const CamsReportsMenu = ({
  loggedIn,
  navigationSettings,
  recentlyVisited,
  serviceConfig,
  menuState,
  setMenuState,
  scrollMenuToTop,
  onClickLink,
  signInUrl,
  sevenRatings,
}) => (
  <div className="quiver-cams-reports-menu">
    <RecentlyVisited
      listType="spots"
      loggedIn={loggedIn}
      items={recentlyVisited}
      serviceConfig={serviceConfig}
      staticLinks={camsReportsLinks}
      signInUrl={signInUrl}
      onClickLink={(properties, closeMobileMenu) =>
        onClickLink(
          {
            name: 'Clicked Main Nav',
            properties: {
              ...properties,
              tabName: 'Cams',
            },
          },
          closeMobileMenu
        )
      }
      sevenRatings={sevenRatings}
    />
    <TaxonomyNavigator
      hierarchy={HIERARCHY}
      loggedIn={loggedIn}
      navigationSettings={navigationSettings}
      serviceConfig={serviceConfig}
      savedLocationType="spotsTaxonomyLocation"
      menuState={menuState}
      setMenuState={setMenuState}
      taxonomyType="spot"
      scrollMenuToTop={scrollMenuToTop}
      onClickLink={(properties, closeMobileMenu) =>
        onClickLink(
          {
            name: 'Clicked Main Nav',
            properties: {
              ...properties,
              tabName: 'Cams',
            },
          },
          closeMobileMenu
        )
      }
      sevenRatings={sevenRatings}
    />
  </div>
);

CamsReportsMenu.propTypes = {
  loggedIn: PropTypes.bool,
  navigationSettings: navigationSettingsPropType.isRequired,
  recentlyVisited: PropTypes.arrayOf(recentlyVisitedPropType).isRequired,
  serviceConfig: serviceConfigurationPropType.isRequired,
  setMenuState: PropTypes.func.isRequired,
  menuState: PropTypes.shape().isRequired,
  scrollMenuToTop: PropTypes.func,
  signInUrl: PropTypes.string.isRequired,
  onClickLink: PropTypes.func,
  sevenRatings: PropTypes.bool,
};

CamsReportsMenu.defaultProps = {
  loggedIn: false,
  scrollMenuToTop: null,
  onClickLink: null,
  sevenRatings: false,
};

export default CamsReportsMenu;
