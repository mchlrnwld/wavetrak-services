import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import CamsReportsMenu from './CamsReportsMenu';
import TaxonomyNavigator from '../TaxonomyNavigator';
import RecentlyVisited from '../RecentlyVisited';

describe('CamsReportsMenu', () => {
  const serviceConfig = { serviceUrl: 'surflinetest.com' };

  it('should render a recently visited menu', () => {
    const wrapper = shallow(<CamsReportsMenu serviceConfig={serviceConfig} />);
    const recentlyVisitedWrapper = wrapper.find(RecentlyVisited);
    expect(recentlyVisitedWrapper).to.have.length(1);
  });

  it('should render a taxonomy navigator', () => {
    const wrapper = shallow(<CamsReportsMenu serviceConfig={serviceConfig} />);
    const taxonomyNavigatorWrapper = wrapper.find(TaxonomyNavigator);
    expect(taxonomyNavigatorWrapper).to.have.length(1);
  });
});
