import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import PageContainer from './PageContainer';

describe('PageContainer', () => {
  it('should render page', () => {
    const serviceConfig = {
      serviceUrl: 'surflinetest.com',
    };
    const page = <div>Test Page</div>;
    const wrapper = shallow(
      <PageContainer serviceConfig={serviceConfig}>{page}</PageContainer>
    );
    expect(wrapper.html()).to.contain('<div>Test Page</div>');
  });
});
