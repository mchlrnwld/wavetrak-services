import React from 'react';
import PropTypes from 'prop-types';

import NewNavigationBar from '../NewNavigationBar';
import FavoritesBar from '../FavoritesBar';
import userPropType from '../PropTypes/userPropType';
import entitlementsPropType from '../PropTypes/entitlementsPropType';
import serviceConfigurationPropType from '../PropTypes/serviceConfigurationPropType';
import settingsPropType from '../PropTypes/settingsPropType';
import favoritesPropType from '../PropTypes/favoritesPropType';
import NotificationBar from '../NotificationBar';

const PageContainer = ({
  children,
  user,
  serviceConfig,
  settings,
  entitlements,
  favorites,
  smallNotification,
  largeNotification,
  footer,
  largeFavoritesBar,
}) => (
  <div className="quiver-page-container">
    <div className="quiver-page-container__top">
      <NotificationBar largeNotification={largeNotification} />
      <NewNavigationBar
        user={user}
        entitlements={entitlements}
        smallNotification={smallNotification}
        serviceConfig={serviceConfig}
        settings={settings}
        favorites={favorites}
      />
      <FavoritesBar
        settings={settings}
        entitlements={entitlements}
        favorites={favorites}
        user={user}
        large={largeFavoritesBar}
      />
    </div>
    <main className="quiver-page-container__content" role="main">
      {children}
    </main>
    {footer}
  </div>
);

PageContainer.propTypes = {
  children: PropTypes.node,
  user: userPropType,
  serviceConfig: serviceConfigurationPropType,
  settings: settingsPropType,
  entitlements: entitlementsPropType,
  favorites: favoritesPropType,
  smallNotification: PropTypes.node,
  largeNotification: PropTypes.node,
  footer: PropTypes.node,
  largeFavoritesBar: PropTypes.bool,
};

PageContainer.defaultProps = {
  children: null,
  user: null,
  serviceConfig: null,
  settings: null,
  entitlements: [],
  favorites: [],
  smallNotification: null,
  largeNotification: null,
  footer: null,
  largeFavoritesBar: false,
};

export default PageContainer;
