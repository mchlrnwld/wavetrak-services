import React from 'react';
import PageContainer from './index';

export default {
  title: 'PageContainer',
};

export const Empty = () => <PageContainer />;

Empty.story = {
  name: 'empty',
};
