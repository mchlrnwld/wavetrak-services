import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { mount } from 'enzyme';
import PremiumCamCTA from './PremiumCamCTA';
import CamRewindIcon from '../icons/CamRewindIcon';
import PremiumCamIconMedium from '../icons/PremiumCamIconMedium';

describe('PremiumCamCTA', () => {
  const { location } = window;

  const props = {
    href: '/',
    onClickPaywall: sinon.spy(),
    camStillUrl: '/still',
  };

  before(() => {
    delete window.location;
    window.location = {
      assign: sinon.spy(),
    };
  });
  after(() => {
    window.location = location;
  });

  afterEach(() => {
    props.onClickPaywall.resetHistory();
  });

  it('should render the premium cam cta', () => {
    const wrapper = mount(<PremiumCamCTA {...props} />);
    expect(wrapper).to.have.length(1);
    expect(wrapper.props()).to.deep.equal({
      ...props,
      Icon: PremiumCamIconMedium,
      copy: null,
    });
    expect(
      wrapper.find('.quiver-premium-cam-cta__camStill').prop('style')
    ).to.deep.equal({ backgroundImage: 'url(/still)' });
  });

  it('should render the premium cam cta with custom text and Icon', () => {
    const newProps = {
      ...props,
      Icon: CamRewindIcon,
      copy: {
        indicatorText: 'cam rewind',
        headerText: 'Relive your session',
        subheaderText:
          "Download your rides – or the waves that got away – then share 'em with your friends",
      },
    };
    const wrapper = mount(<PremiumCamCTA {...newProps} />);
    expect(wrapper).to.have.length(1);
    expect(wrapper.props()).to.deep.equal(newProps);

    const icon = wrapper.find('CamRewindIcon');
    expect(icon).to.have.length(1);
  });

  it('should trigger the onClickPaywall when clicking the button', () => {
    const wrapper = mount(<PremiumCamCTA {...props} />);
    expect(wrapper).to.have.length(1);

    const btn = wrapper.find('button');
    btn.simulate('click');

    expect(props.onClickPaywall).to.have.been.calledOnceWithExactly();
    expect(window.location.assign).to.have.been.calledOnceWithExactly(
      props.href
    );
  });
});
