import PropTypes from 'prop-types';
import React from 'react';
import Button from '../Button';
import PremiumCamIconMedium from '../icons/PremiumCamIconMedium';

const defaultCopy = {
  indicatorText: 'premium cam',
  headerText: 'This is a High Resolution, Premium Only Cam',
  subheaderText: 'Watch live conditions of this spot in full HD, ad-free',
  ctaText: 'Start Free Trial',
  learnMoreText: 'Learn more about Surfline Premium',
};

const PremiumCamCTA = ({ href, onClickPaywall, camStillUrl, copy, Icon }) => {
  const onClickGoPremium = () => {
    onClickPaywall();
    window.location.assign(href);
  };

  const fullCopy = {
    ...defaultCopy,
    ...copy,
  };

  return (
    <div className="quiver-premium-cam-cta__container">
      <div
        className="quiver-premium-cam-cta__camStill"
        style={{ backgroundImage: `url(${camStillUrl})` }}
      />
      <div className="quiver-premium-cam-cta">
        <div className="quiver-aspect-ratio-content quiver-premium-cam-cta__content__container">
          <div className="quiver-premium-cam-cta__content">
            <div className="quiver-premium-cam-cta__heading">
              {fullCopy.headerText}
            </div>
            <div className="quiver-premium-cam-cta__icon">
              <Icon />
            </div>
            <div className="quiver-premium-cam-cta__subheading">
              <span>{fullCopy.subheaderText}</span>
            </div>
            <div className="quiver-premium-cam-cta__links">
              <div className="quiver-premium-cam-cta__links__upgrade">
                <Button onClick={onClickGoPremium}>{fullCopy.ctaText}</Button>
              </div>
              <div className="quiver-premium-cam-cta__links__about">
                <a href="/premium-benefits">{fullCopy.learnMoreText}</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

PremiumCamCTA.defaultProps = {
  href: null,
  onClickPaywall: null,
  camStillUrl: null,
  copy: null,
  Icon: PremiumCamIconMedium,
};

PremiumCamCTA.propTypes = {
  camStillUrl: PropTypes.string,
  href: PropTypes.string,
  onClickPaywall: PropTypes.func,
  copy: PropTypes.shape({
    indicatorText: PropTypes.string,
    headerText: PropTypes.string,
    subheaderText: PropTypes.string,
    ctaText: PropTypes.string,
    learnMoreText: PropTypes.string,
  }),
  Icon: PropTypes.func,
};

export default PremiumCamCTA;
