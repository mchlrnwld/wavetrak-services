import React from 'react';
import UserAvatar from './UserAvatar';

export default {
  title: 'UserAvatar',
};

export const Default = () => (
  <div
    style={{
      height: '54px',
      width: '54px',
      backgroundColor: '#0058B0',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    <UserAvatar />
  </div>
);

Default.story = {
  name: 'default',
};

export const Premium = () => {
  const entitlementStub = ['sl_premium'];
  const accountAlert = true;
  return (
    <div
      style={{
        height: '54px',
        width: '54px',
        backgroundColor: '#0058B0',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <UserAvatar entitlements={entitlementStub} accountAlert={accountAlert} />
    </div>
  );
};

Premium.story = {
  name: 'premium',
};

export const NotificationAlert = () => {
  const entitlementStub = [];
  const accountAlert = true;
  return (
    <div
      style={{
        height: '54px',
        width: '54px',
        backgroundColor: '#0058B0',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <UserAvatar entitlements={entitlementStub} accountAlert={accountAlert} />
    </div>
  );
};

NotificationAlert.story = {
  name: 'notification - alert',
};

export const NotificationWarning = () => {
  const entitlementStub = ['sl_premium'];
  const accountAlert = true;
  const accountWarning = true;
  return (
    <div
      style={{
        height: '54px',
        width: '54px',
        backgroundColor: '#0058B0',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <UserAvatar
        entitlements={entitlementStub}
        accountAlert={accountAlert}
        accountWarning={accountWarning}
      />
    </div>
  );
};

NotificationWarning.story = {
  name: 'notification - warning',
};
