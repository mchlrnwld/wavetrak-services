import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';

/* eslint-disable max-len */
const UserAvatar = ({ entitlements, accountAlert, accountWarning }) => {
  const isPremium = entitlements.includes('sl_premium');
  const notification = (!isPremium && accountAlert) || accountWarning;
  const getRingClassName = () =>
    classNames({
      'quiver-user-avatar__ring': true,
      'quiver-user-avatar__ring--premium': isPremium,
      'quiver-user-avatar__ring--alert': !isPremium && accountAlert,
      'quiver-user-avatar__ring--warning': accountWarning,
    });

  const getNotificationClassName = () =>
    classNames({
      'quiver-user-avatar__notification': true,
      'quiver-user-avatar__notification--alert': !isPremium && accountAlert,
      'quiver-user-avatar__notification--warning': accountWarning,
    });

  return (
    <svg
      className="quiver-user-avatar"
      width="34px"
      height="34px"
      role="img"
      viewBox="0 0 34 34"
      aria-label="[title]"
      style={{
        display: 'inline-flex',
        alignItems: 'center',
        justifyContent: 'center',
        verticalAlign: 'middle',
      }}
    >
      <g
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
        transform="translate(1.000000, 1.000000)"
      >
        <circle stroke="#FFFFFF" cx="16" cy="13.6" r="6.1" />
        <path
          d="M26.4493244,27.5833333 C25.9299768,25.0267564 21.4813083,22.9 16,22.9 C10.5186917,22.9 6.07002319,25.0267564 5.55067563,27.5833333"
          stroke="#FFFFFF"
        />
        <circle
          className={getRingClassName()}
          strokeWidth="2"
          cx="16"
          cy="16"
          r="16"
        />
      </g>
      {notification ? (
        <circle
          className={getNotificationClassName()}
          strokeWidth="2"
          fillRule="nonzero"
          cx="27"
          cy="5"
          r="5"
        />
      ) : null}
    </svg>
  );
};

UserAvatar.propTypes = {
  entitlements: PropTypes.arrayOf(PropTypes.string),
  accountAlert: PropTypes.bool,
  accountWarning: PropTypes.bool,
};

UserAvatar.defaultProps = {
  entitlements: [],
  accountAlert: false,
  accountWarning: false,
};

export default UserAvatar;
