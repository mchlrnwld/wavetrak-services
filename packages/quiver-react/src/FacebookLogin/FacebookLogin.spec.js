/* eslint-disable no-unused-expressions */
import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import FacebookLogin from './FacebookLogin';

describe('FacebookLogin', () => {
  it('should render a FacebookLogin button with defaulted props', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(
      <FacebookLogin onClick={onClick} loading={false} error={false} />
    );

    expect(wrapper.find('.quiver-button')).to.exist;
    expect(wrapper.find('.create-form__error__alert')).to.not.exist;
  });

  it('should render a FacebookLogin with errors', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(
      <FacebookLogin onClick={onClick} loading={false} error />
    );

    expect(wrapper.find('.quiver-button')).to.exist;
    expect(wrapper.find('.create-form__error__alert')).to.exist;
  });
});
