import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';

const FacebookLogin = ({ onClick, loading, error }) => (
  <div className="facebook-login__content">
    {error ? (
      <div className="create-form__error__alert">
        {/* eslint-disable-next-line */}
        <label>{error}</label>
      </div>
    ) : null}
    <Button style={{ width: '100%' }} loading={loading} onClick={onClick}>
      SIGN IN WITH FACEBOOK
    </Button>
  </div>
);

FacebookLogin.propTypes = {
  onClick: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string,
};

FacebookLogin.defaultProps = {
  error: null,
};

export default FacebookLogin;
