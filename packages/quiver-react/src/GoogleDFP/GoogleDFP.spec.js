/* global googletag */
import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import GoogleDFP from './GoogleDFP';

describe('GoogleDFP', () => {
  const adConfig = {
    adId: 'TestAd',
    adUnit: '1234/TestAd',
    adClass: 'ad-class',
  };

  beforeEach(() => {
    googletag.cmd.push.resetHistory();
    googletag.destroySlots.resetHistory();
  });

  it('should render without an ID on initial render (server/client)', () => {
    sinon.stub(GoogleDFP.prototype, 'componentDidMount');
    const wrapper = mount(<GoogleDFP adConfig={adConfig} />);
    try {
      expect(wrapper.state('uuid')).to.be.null();
      expect(wrapper.find('.ad-class')).to.have.length(0);
      expect(googletag.cmd.push).not.to.have.been.called();
    } catch (error) {
      // We need to make sure we restore this so we don't break the other tests
      GoogleDFP.prototype.componentDidMount.restore();
      throw error;
    }
    GoogleDFP.prototype.componentDidMount.restore();
  });

  it('should set a UUID and initialize the ad after initial render', () => {
    const wrapper = mount(<GoogleDFP adConfig={adConfig} />);

    const uuid = wrapper.state('uuid');
    expect(uuid).not.to.be.null();
    wrapper.update();

    const ad = wrapper.find('.ad-class');
    expect(ad).to.have.length(1);
    expect(ad.prop('id')).to.equal(`${adConfig.adUnit}-${uuid}`);
    expect(googletag.cmd.push).to.have.been.calledOnce();
  });

  it('should not set a UUID if isHtl prop is true', () => {
    const wrapper = mount(<GoogleDFP adConfig={adConfig} isHtl />);

    const uuid = wrapper.state('uuid');
    expect(uuid).not.to.be.null();
    wrapper.update();

    const ad = wrapper.find('.ad-class');
    expect(ad).to.have.length(1);
    expect(ad.prop('id')).to.equal(`${adConfig.adId}`);
  });

  it('should maintain UUID but reinitialize ad when ad config updated', () => {
    const wrapper = mount(<GoogleDFP adConfig={adConfig} />);

    const uuid = wrapper.state('uuid');
    expect(uuid).not.to.be.null();

    expect(googletag.cmd.push).to.have.been.calledOnce();

    const updatedAdConfig = {
      ...adConfig,
      adUnit: 'TestAdUpdated',
    };
    wrapper.setProps({ adConfig: updatedAdConfig });
    wrapper.update();
    expect(wrapper.state('uuid')).to.equal(uuid);
    expect(googletag.cmd.push).to.have.been.calledTwice();

    const ad = wrapper.find('.ad-class');
    expect(ad).to.have.length(1);
    expect(ad.prop('id')).to.equal(`${updatedAdConfig.adUnit}-${uuid}`);
  });

  it('should destroy ad slots when unmounted', () => {
    const wrapper = mount(<GoogleDFP adConfig={adConfig} />);
    expect(googletag.cmd.push).to.have.been.calledOnce();
    expect(googletag.destroySlots).not.to.have.been.called();

    wrapper.unmount();
    expect(googletag.cmd.push).to.have.been.calledTwice();

    googletag.cmd.push.secondCall.args[0]();
    expect(googletag.destroySlots).to.have.been.calledOnce();
  });
});
