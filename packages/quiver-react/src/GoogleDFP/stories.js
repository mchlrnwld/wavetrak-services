import React from 'react';
import GoogleDFP from './index';

const heroAdConfig = {
  adUnit: '/1024858/HP_Horizontal_Variable',
  adId: 'hero_ad_unit',
  adSizes: [
    'fluid',
    [1170, 250],
    [1170, 160],
    [990, 90],
    [970, 250],
    [320, 50],
    [970, 90],
    [970, 160],
    [320, 80],
    [300, 250],
    [750, 250],
    [750, 160],
    [728, 90],
  ],
  adSizeMappings: [
    [
      [1200, 0],
      [
        'fluid',
        [1170, 250],
        [1170, 160],
        [990, 90],
        [970, 250],
        [970, 160],
        [970, 90],
        [728, 90],
      ],
    ],
    [
      [992, 0],
      ['fluid', [990, 90], [970, 250], [970, 160], [970, 90], [728, 90]],
    ],
    [
      [768, 0],
      ['fluid', [750, 250], [750, 160], [728, 90]],
    ],
    [
      [320, 0],
      ['fluid', [320, 80], [320, 50]],
    ],
    [
      [0, 0],
      ['fluid', [320, 50]],
    ],
  ],
  adClass: 'sl-hero-ad-unit',
  adViewType: 'HOMEPAGE',
  adTargets: [['position', 'SPONSORSHIP']],
};

const outOfPageAdConfig = {
  ...heroAdConfig,
  adViewType: 'superheader',
  adUnit: '/1024858/Horizontal_Variable',
  isOutOfPage: true,
};

export default {
  title: 'GoogleDFP',
};

export const Default = () => (
  <GoogleDFP
    adConfig={heroAdConfig}
    eventListeners={[{ type: 'slotRenderEnded', listener: console.log }]}
  />
);

Default.story = {
  name: 'default',
};

export const OutOfPage = () => (
  <GoogleDFP
    adConfig={outOfPageAdConfig}
    eventListeners={[{ type: 'slotRenderEnded', listener: console.log }]}
  />
);

OutOfPage.story = {
  name: 'out of page',
};

export const HTL = () => (
  <GoogleDFP
    adConfig={heroAdConfig}
    eventListeners={[{ type: 'slotRenderEnded', listener: console.log }]}
    isHtl
  />
);

HTL.story = {
  name: 'HTL',
};
