/* global googletag */
/* global apstag */
import React, { Component } from 'react';
import isEqual from 'lodash/isEqual';
import PropTypes from 'prop-types';
import { v1 as uuidV1 } from 'uuid';

const getAdId = (adConfig, uuid, isHtl) =>
  isHtl ? adConfig.adId : `${adConfig.adUnit}-${uuid}`;

class GoogleDFP extends Component {
  constructor() {
    super();
    this.state = {
      uuid: null,
    };
  }

  componentDidMount() {
    // eslint-disable-next-line react/no-did-mount-set-state
    this.setState({ uuid: uuidV1() });
  }

  componentDidUpdate(prevProps, prevState) {
    const { uuid } = this.state;
    const { adConfig, unifiedAdMarketplace, isHtl } = this.props;

    if (prevState.uuid !== uuid || !isEqual(prevProps.adConfig, adConfig)) {
      const adId = getAdId(adConfig, uuid, isHtl);
      if (unifiedAdMarketplace && !isHtl) {
        apstag.fetchBids(
          {
            slots: [
              {
                slotID: adId,
                slotName: adConfig.adUnit,
                sizes: adConfig.adSizes,
              },
            ],
            timeout: 2e3,
          },
          () => {
            this.initializeAd(prevProps.adConfig, adId);
          }
        );
      } else {
        this.initializeAd(prevProps.adConfig, adId);
      }
    }
  }

  componentWillUnmount() {
    googletag.cmd.push(() => {
      googletag.destroySlots([this.adSlot]);
    });
  }

  initializeAd =
    /* istanbul ignore next */
    (prevAdConfig, adId) => {
      const {
        adConfig,
        hide,
        companion,
        enableSingleRequest,
        eventListeners,
        preventRefresh,
        unifiedAdMarketplace,
        isHtl,
        isTesting,
      } = this.props;

      googletag.cmd.push(() => {
        if (prevAdConfig) googletag.destroySlots([this.adSlot]);
        if (isHtl) {
          googletag.pubads().disableInitialLoad();
          googletag.pubads().setTargeting('is_testing', isTesting);
        }
        googletag.pubads().enableAsyncRendering();
        if (enableSingleRequest) googletag.pubads().enableSingleRequest();
        googletag.pubads().collapseEmptyDivs();

        eventListeners.forEach(({ type, listener }) => {
          googletag.pubads().addEventListener(type, listener);
        });

        let mapping;
        if (!adConfig.adSizeMappings) {
          mapping = googletag
            .sizeMapping()
            .addSize([1024, 768], adConfig.adSizeDesktop)
            .addSize([980, 690], adConfig.adSizeDesktop)
            .addSize([770, 480], adConfig.adSizeDesktop)
            .addSize([0, 0], adConfig.adSizeMobile)
            .build();
        } else {
          mapping = adConfig.adSizeMappings;
        }

        if (adConfig.isOutOfPage) {
          this.adSlot = googletag.defineOutOfPageSlot(adConfig.adUnit, adId);
        } else {
          this.adSlot = googletag.defineSlot(
            adConfig.adUnit,
            adConfig.adSizes,
            adId
          );
        }

        this.adSlot
          ?.defineSizeMapping(mapping)
          .addService(googletag.pubads())
          .setTargeting(
            adConfig.isOutOfPage ? 'richmedia' : 'viewType',
            adConfig.adViewType
          );

        if (companion) {
          this.adSlot.addService(googletag.companionAds());
          this.adSlot.addService(googletag.pubads());
          googletag.companionAds().setRefreshUnfilledSlots(true);
          googletag.pubads().enableVideoAds();
        }

        adConfig.adTargets.forEach((targetData) => {
          this.adSlot.setTargeting(targetData[0], targetData[1]);
        });

        googletag.enableServices();

        if (isHtl) {
          if (prevAdConfig && !unifiedAdMarketplace && !hide) {
            window?.htlbid?.cmd.push(() => {
              window?.htlbid?.display(adId);
              window?.htlbid?.refresh([this.adSlot]);
            });
          }
        } else {
          if (prevAdConfig && !unifiedAdMarketplace && !preventRefresh) {
            googletag.pubads().refresh([this.adSlot]);
          }
          if (unifiedAdMarketplace) apstag.setDisplayBids();
          if (!hide) googletag.display(adId);
        }
      });
    };

  render() {
    const { uuid } = this.state;
    const { adConfig, isHtl } = this.props;

    return (
      <div
        className="quiver-google-dfp"
        ref={(el) => {
          this.ref = el;
        }}
      >
        {uuid ? (
          <div
            id={getAdId(adConfig, uuid, isHtl)}
            className={adConfig.adClass}
          />
        ) : null}
      </div>
    );
  }
}

GoogleDFP.propTypes = {
  adConfig: PropTypes.shape({
    adClass: PropTypes.string,
    adUnit: PropTypes.string.isRequired,
    adSizes: PropTypes.instanceOf(Array).isRequired,
    adSizeDesktop: PropTypes.instanceOf(Array),
    adSizeMobile: PropTypes.instanceOf(Array),
    adSizeMappings: PropTypes.instanceOf(Array),
    adViewType: PropTypes.string.isRequired,
    adTargets: PropTypes.instanceOf(Array).isRequired,
    isOutOfPage: PropTypes.bool,
  }).isRequired,
  hide: PropTypes.bool,
  companion: PropTypes.bool,
  enableSingleRequest: PropTypes.bool,
  preventRefresh: PropTypes.bool,
  eventListeners: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string.isRequired,
      listener: PropTypes.func.isRequired,
    })
  ),
  unifiedAdMarketplace: PropTypes.bool,
  isHtl: PropTypes.bool,
  isTesting: PropTypes.string,
};

GoogleDFP.defaultProps = {
  hide: false,
  companion: false,
  enableSingleRequest: true,
  eventListeners: [],
  preventRefresh: false,
  unifiedAdMarketplace: false,
  isHtl: false,
  isTesting: 'yes',
};

export default GoogleDFP;
