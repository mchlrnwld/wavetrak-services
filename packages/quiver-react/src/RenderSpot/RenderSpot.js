import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';
import CamIcon from '../icons/CamIcon';
import SurfConditions from '../SurfConditions';
import generateSlug from '../helpers/generateSlug';
import spotPagePath from '../helpers/spotPagePath';
import taxonomyNodePropType from '../PropTypes/taxonomyNodePropType';

const RenderSpot = ({ spot, onClickLink, sevenRatings }) => {
  const getClassName = () =>
    classNames({
      'quiver-render-spot': true,
      'quiver-render-spot--last-of-group': spot.lastOfGroup,
    });

  const getHref = ({ associated, _id, name }) => {
    if (associated) {
      return associated.links.find((link) => link.key === 'www').href;
    }
    const slug = generateSlug(name);
    return spotPagePath(_id, slug);
  };
  const isMultiCam = spot.cameras && spot.cameras.length > 1;
  return (
    <div className={getClassName()}>
      <SurfConditions
        conditions={spot.conditions ? spot.conditions.value || 'NONE' : 'NONE'}
        expired={
          spot.conditions && spot.conditions.expired && spot.conditions.human
        }
        sevenRatings={sevenRatings}
      />
      <a
        href={getHref(spot)}
        onClick={() =>
          onClickLink(
            {
              clickEndLocation: 'Spot',
              destinationUrl: getHref(spot),
              completion: true,
              linkName: spot.name,
            },
            true
          )
        }
      >
        {spot.name}
      </a>
      {spot.hasCamera || (spot.cameras && spot.cameras.length > 0) ? (
        <CamIcon isMultiCam={isMultiCam} withText />
      ) : null}
    </div>
  );
};

RenderSpot.propTypes = {
  spot: taxonomyNodePropType.isRequired,
  onClickLink: PropTypes.func,
  sevenRatings: PropTypes.bool,
};

RenderSpot.defaultProps = {
  onClickLink: null,
  sevenRatings: false,
};

export default RenderSpot;
