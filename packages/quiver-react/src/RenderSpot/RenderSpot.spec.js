import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import spotsFixture from '../TaxonomyNavigator/fixtures/spots.json';
import CamIcon from '../icons/CamIcon';
import SurfConditions from '../SurfConditions';
import RenderSpot from './RenderSpot';

describe('TaxonomyNavigator / RenderSpot', () => {
  const [spot] = spotsFixture[0].spots;

  it('renders a surfConditions, an a tag, and a camIcon', () => {
    const wrapper = shallow(<RenderSpot spot={{ ...spot, hasCamera: true }} />);
    const surfConditionsWrapper = wrapper.find(SurfConditions);
    const aWrapper = wrapper.find('a');
    const camIconWrapper = wrapper.find(CamIcon);
    expect(surfConditionsWrapper).to.have.length(1);
    expect(aWrapper).to.have.length(1);
    expect(aWrapper.text()).to.equal('a spot');
    expect(camIconWrapper).to.have.length(1);
  });

  it('should not render a camIcon if there is no camera', () => {
    const wrapper = shallow(
      <RenderSpot
        spot={{
          ...spot,
          hasCamera: false,
          conditions: { human: false, value: null },
        }}
      />
    );
    const surfConditions = wrapper.find(SurfConditions);
    const camIcon = wrapper.find(CamIcon);
    expect(surfConditions).to.have.length(1);
    expect(camIcon).to.have.length(0);
  });

  it('should pass sevenRatings prop to SurfConditions when true', () => {
    const wrapper = shallow(
      <RenderSpot
        spot={{
          ...spot,
          hasCamera: false,
          conditions: { human: false, value: null },
        }}
        sevenRatings
      />
    );
    const surfConditions = wrapper.find(SurfConditions);
    expect(surfConditions.prop('sevenRatings')).to.be.true();
  });
});
