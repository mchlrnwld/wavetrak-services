import PropTypes from 'prop-types';
import React from 'react';
import LockIcon from '../icons/LockIcon';
import CloseMediaPlayerIcon from '../icons/CloseMediaPlayerIcon';
import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

const PersistentMediaPlayerCTA = ({
  backgroundImageURL,
  ctaHref,
  displayCloseIcon,
  doToggleVisibility,
  message,
  onClickHandler,
}) => (
  <div
    className="quiver-persistent-cta"
    style={{
      backgroundImage: backgroundImageURL ? `url(${backgroundImageURL})` : '',
    }}
  >
    <div className="quiver-persistent-cta__overlay">
      <LockIcon />
      <p>{message}</p>
      <a href={ctaHref} onClick={onClickHandler}>
        Start Free Trial
      </a>
      {displayCloseIcon ? (
        <div
          role="button"
          {...createAccessibleOnClick(doToggleVisibility, BTN)}
          className="quiver-persistent-cta__close"
        >
          <CloseMediaPlayerIcon className="quiver-persistent-cta__close-icon" />
        </div>
      ) : null}
    </div>
  </div>
);

PersistentMediaPlayerCTA.propTypes = {
  backgroundImageURL: PropTypes.string,
  ctaHref: PropTypes.string.isRequired,
  displayCloseIcon: PropTypes.bool,
  doToggleVisibility: PropTypes.func,
  message: PropTypes.string.isRequired,
  onClickHandler: PropTypes.func.isRequired,
};

PersistentMediaPlayerCTA.defaultProps = {
  backgroundImageURL: null,
  displayCloseIcon: false,
  doToggleVisibility: () => {},
};

export default PersistentMediaPlayerCTA;
