import React from 'react';
import SurfLineGraph from './SurfLineGraph';
import data from './fixtures/data';

const props = {
  overallMaxHeight: 4,
  data,
  units: 'FT',
  device: {
    width: 1200,
    mobile: false,
  },
  utcOffset: -8,
  days: [0, 1, 2, 3, 4, 5, 6, 7, 8],
};

export default {
  title: 'SurfLineGraph',
};

export const Default = () => <SurfLineGraph {...props} />;

Default.story = {
  name: 'default',
};
