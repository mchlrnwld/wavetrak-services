/* istanbul ignore file */

import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { color } from '@surfline/quiver-themes';
import {
  getTop3Swells,
  TABLET_LARGE_WIDTH,
  roundSurfHeight,
  DEFAULT_SWELL_MAX,
} from '@surfline/web-common';
import { area, line, curveCardinal } from 'd3-shape';
import { scaleTime, scaleLinear } from 'd3-scale';
import { select } from 'd3-selection';
import _round from 'lodash/round';
import _isEqual from 'lodash/isEqual';
import classNames from 'classnames';

import SwellMeasurements from '../SwellMeasurements';
import SurfHeight from '../SurfHeight';
import TooltipProvider from '../TooltipProvider';
import TooltipDatetime from '../TooltipDatetime';
import devicePropType from '../PropTypes/device';
import GraphTooltip from '../GraphTooltip';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';

const WAVE_DATA_INTERVAL = 6;
const HOURS_IN_A_DAY = 24;
const DATA_POINTS_PER_DAY = HOURS_IN_A_DAY / WAVE_DATA_INTERVAL;

class SurfLineGraph extends Component {
  constructor() {
    super();
    this.state = {
      canvasRef: null,
    };
    this.lineGraphRef = React.createRef();
  }

  componentDidMount() {
    this.renderGraph();
    window.addEventListener('resize', this.resizeListener);
  }

  componentDidUpdate(props, state) {
    const { canvasRef } = this.state;
    const { days, isHourly } = this.props;

    const daysChanged = !_isEqual(props.days, days);
    const canvasRefChanged = canvasRef && canvasRef !== state.canvasRef;

    if (daysChanged || props.isHourly !== isHourly || canvasRefChanged)
      this.renderGraph();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeListener);

    /**
     *  Since D3 Manipulates the DOM and we now have two instances of this component, we want to prevent
     *  D3 from doing any dom mutations from an unmounting component
     */
    this.renderGraph = () => null;
  }

  setCanvasRef = (el) =>
    this.setState({
      canvasRef: el,
    });

  resizeListener = () => {
    if (this.resizeTimeout) return;
    this.resizeTimeout = setTimeout(() => {
      this.resizeTimeout = null;
      this.renderGraph();
    }, 66);
  };

  renderTooltip = (xPercentage) => {
    const { data, utcOffset, units, device, dateFormat } = this.props;
    const point = data[Math.floor(data.length * xPercentage)];
    const isMobile = device.width < TABLET_LARGE_WIDTH;

    if (!point) return null;

    const swells = getTop3Swells(point.swells).filter(
      (swell) => swell.height > 0 && swell.period > 0
    );

    return (
      <GraphTooltip>
        {!isMobile && (
          <TooltipDatetime
            utcOffset={point.utcOffset ?? utcOffset}
            timestamp={point.timestamp}
            dateFormat={dateFormat}
          />
        )}
        <div className="quiver-surf-graph__tooltip-body">
          <SurfHeight
            min={roundSurfHeight(point.surf.min, units)}
            max={roundSurfHeight(point.surf.max, units)}
            units={units}
          />
          {!isMobile &&
            swells.map((swell) => (
              <SwellMeasurements
                key={swell.index}
                height={swell.height}
                period={swell.period}
                direction={swell.direction}
                index={swell.index}
                units={units}
              />
            ))}
        </div>
      </GraphTooltip>
    );
  };

  renderGraph = () => {
    const {
      days,
      overallMaxHeight,
      data,
      numberOfTicks,
      units,
      device,
      currentMarkerXPercentage,
    } = this.props;
    const { canvasRef } = this.state;

    const uiUnits = units === 'M' ? units : 'FT';

    // If the canvasRef is not defined, then the graph won't be visible
    if (!canvasRef) return;

    const surfData = data.map(({ surf }, index) => ({
      hour: index,
      ...surf,
    }));

    select(`.quiver-surf-graph__ticks`).remove();
    select(`.quiver-surf-graph__canvas-svg`).remove();

    const yPadding = 24;

    const x = scaleTime()
      .rangeRound([0, canvasRef.offsetWidth])
      .domain([0, data.length - 1]);

    const digits = units === 'FT' || units === 'HI' ? 1 : 0;
    const yMax = Math.max(
      _round(overallMaxHeight, digits),
      DEFAULT_SWELL_MAX[units] || 0
    );
    const newYMax = yMax + yMax * 0.3;

    const y = scaleLinear()
      .rangeRound([canvasRef.offsetHeight, yPadding])
      .domain([0, newYMax]);

    const path = line()
      .x((d) => x(d.hour))
      .y((d) => y(d.max))
      .curve(curveCardinal);

    const canvas = select(`.quiver-surf-graph__canvas`);

    if (numberOfTicks > 0) {
      const maxTick = _round(newYMax);
      const intermediateTicks = [...Array(numberOfTicks - 2)].map((_, index) =>
        _round((index + 1) * (maxTick / (numberOfTicks - 1)), 1)
      );
      const tickData = [0, ...intermediateTicks, maxTick];

      const ticks = canvas
        .append('div')
        .attr('class', 'quiver-surf-graph__ticks');

      ticks
        .selectAll('.quiver-surf-graph__tick')
        .data(tickData)
        .enter()
        .append('div')
        .attr('class', 'quiver-surf-graph__tick')
        .style('top', (d, index) => `${index === 0 ? y(d) - 5 : y(d)}px`)
        .text((d) => d);
    }

    this.canvasSVG = canvas
      .append('svg')
      .attr('class', 'quiver-surf-graph__canvas-svg');

    const surfGroup = this.canvasSVG
      .append('g')
      .attr('class', `quiver-surf-graph__surf`);

    // Define the area below the line.
    const swellArea = area()
      .x((d) => x(d.hour))
      .y0(y(0))
      .y1((d) => y(d.max))
      .curve(curveCardinal);

    const surfLineSvg = surfGroup
      .append('path')
      .attr('class', 'quiver-surf-graph__visible-line')
      .attr('d', path(surfData));

    // Append the Area under the line.
    surfGroup
      .append('path')
      .attr('class', 'quiver-surf-graph__fill')
      .attr('d', swellArea(surfData));

    surfGroup
      .append('linearGradient')
      .attr('id', 'surf-line-area-gradient')
      .attr('gradientUnits', 'userSpaceOnUse')
      .attr('x1', 0)
      .attr('y1', y(0))
      .attr('x2', 0)
      .attr('y2', y(newYMax))
      .selectAll('stop')
      .data([
        { offset: '0%', color: '#B1D7F9' },
        { offset: '100%', color: '#075EDB' },
      ])
      .enter()
      .append('stop')
      .attr('offset', (d) => d.offset)
      .attr('stop-color', (d) => d.color);

    const surfScrubber = select(`.quiver-surf-graph__canvas-svg`)
      .append('g')
      .attr('class', 'quiver-surf-graph__active-node')
      .style('opacity', 0);

    surfScrubber
      .append('circle')
      .attr('vector-effect', 'non-scaling-stroke')
      .attr('r', 7)
      .attr('fill', color('dark-blue', 10));

    surfScrubber
      .append('circle')
      .attr('vector-effect', 'non-scaling-stroke')
      .attr('r', 3.5)
      .attr('fill', 'white');

    surfScrubber
      .append('line')
      .attr('class', 'quiver-surf-graph__bottom-vertical-line')
      .attr('x1', 0)
      .attr('y1', 7)
      .attr('x2', 0)
      .attr('y2', 200)
      .attr('stroke-width', 1)
      .attr('stroke', 'black');

    surfScrubber
      .append('line')
      .attr('class', 'quiver-surf-graph__top-vertical-line')
      .attr('x1', 0)
      .attr('y1', -100)
      .attr('x2', 0)
      .attr('y2', -7)
      .attr('stroke-width', 1)
      .attr('stroke', 'black');

    const dotNode = surfLineSvg.node();
    const totalLength = dotNode.getTotalLength();
    const renderDot = (xPercentage) => {
      const point = dotNode.getPointAtLength(xPercentage * totalLength);
      surfScrubber
        .style('transform', `translate(${point.x}px, ${point.y}px)`)
        .style('opacity', 1);
    };
    const isMobile = device.width < TABLET_LARGE_WIDTH;

    // On mobile we want to render the vertical line immediately, we want to place it
    // based on `currentMarkerXPercentage` which will be zero for future days
    if (isMobile) {
      renderDot(currentMarkerXPercentage);
    }

    this.setState({
      renderDot,
      hideDot: () => {
        surfScrubber.style('opacity', 0);
      },
    });

    const surfDataByDay = days.map((_, dayNum) => {
      const start = DATA_POINTS_PER_DAY * dayNum;
      const end = start + DATA_POINTS_PER_DAY;
      return data.slice(start, end);
    });

    const dayPercentage = 100 / days.length;
    surfDataByDay.forEach((surfDay, dayNumber) => {
      const labelData = surfDay[Math.round((surfDay.length - 1) / 2)].surf;

      const dayColumnMiddleXPercentage =
        dayPercentage * (dayNumber + 1) - dayPercentage / 2;
      const labelPoint = dotNode.getPointAtLength(
        (dayColumnMiddleXPercentage / 100) * totalLength
      );
      const labelMin = units !== 'HI' ? _round(labelData.min) : labelData.min;
      const labelMax = units !== 'HI' ? _round(labelData.max) : labelData.max;
      const labelSurfSize =
        labelMin === labelMax ? labelMax : `${labelMin}-${labelMax}`;

      surfGroup
        .append('text')
        .attr('class', 'quiver-surf-graph__min-max-text')
        .attr('x', `${dayColumnMiddleXPercentage}%`)
        .attr('y', labelPoint.y - 14)
        .text(`${labelSurfSize} ${uiUnits}`);
    });
  };

  render() {
    const { currentMarkerXPercentage, device, isMultiCamPage } = this.props;
    const { hideDot, renderDot } = this.state;

    const className = classNames({
      'quiver-surf-graph': true,
      [`quiver-surf-graph--active-surf-0`]: true,
      'quiver-surf-graph--obscure-inactive': false,
    });

    const isMobile = device.width < TABLET_LARGE_WIDTH;

    return (
      <div ref={this.lineGraphRef} className={className}>
        <TooltipProvider
          currentMarkerXPercentage={currentMarkerXPercentage}
          renderTooltip={this.renderTooltip}
          onShowTooltip={renderDot}
          onHideTooltip={!isMobile ? hideDot : null}
          isSurfGraph
          parentRef={isMultiCamPage && this.lineGraphRef.current}
        >
          <div ref={this.setCanvasRef} className="quiver-surf-graph__canvas" />
        </TooltipProvider>
      </div>
    );
  }
}

SurfLineGraph.propTypes = {
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  numberOfTicks: PropTypes.number,
  overallMaxHeight: PropTypes.number.isRequired,
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  utcOffset: PropTypes.number.isRequired,
  units: PropTypes.string.isRequired,
  device: devicePropType.isRequired,
  isHourly: PropTypes.bool,
  currentMarkerXPercentage: PropTypes.number,
  isMultiCamPage: PropTypes.bool,
  dateFormat: dateFormatPropType,
};

SurfLineGraph.defaultProps = {
  numberOfTicks: 0,
  isHourly: false,
  currentMarkerXPercentage: 0,
  isMultiCamPage: false,
  dateFormat: defaultDateFormat,
};

export default SurfLineGraph;
