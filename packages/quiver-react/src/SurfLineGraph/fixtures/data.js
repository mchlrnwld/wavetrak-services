export default [
  {
    timestamp: +new Date(),
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: +new Date() / 1000,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: +new Date() / 1000,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: +new Date() / 1000,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: +new Date() / 1000,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: +new Date() / 1000,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: +new Date() / 1000,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: +new Date() / 1000,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: +new Date() / 1000,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
];
