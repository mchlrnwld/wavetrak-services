import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import CompassRose from '../CompassRose';
import SwellWindRose from './SwellWindRose';
import WindArrow from '../icons/WindArrow';

describe('components / SwellWindRose', () => {
  it('renders a compass with bearings, a wind arrow, and a swell rose', () => {
    const wind = {
      direction: 280,
      speed: 4,
    };
    const swells = [
      { direction: 1, period: 2, height: 3 },
      { direction: 4, period: 5, height: 6 },
      { direction: 7, period: 8, height: 9 },
    ];
    const units = { windSpeed: 'KTS', waveHeight: 'FT' };
    const wrapper = mount(
      <SwellWindRose
        swells={swells}
        wind={wind}
        units={units}
        mapHeight={10}
        mapWidth={20}
        maxSwell={30}
      />
    );
    const compassRoseWrapper = wrapper.find(CompassRose);
    expect(compassRoseWrapper).to.have.length(1);
    expect(compassRoseWrapper.props()).to.deep.equal({ displayBearings: true });
    const windArrowWrapper = wrapper.find(WindArrow);
    expect(windArrowWrapper.props()).to.deep.equal({
      degrees: 280,
      speed: 4,
      units: 'KTS',
    });
    const swellRoseWrapper = wrapper.find(
      '.quiver-swell-wind-rose__swell-petals'
    );
    expect(swellRoseWrapper).to.have.length(1);
    wrapper.unmount();
  });
});
