import React from 'react';
import SwellWindRose from './SwellWindRose';

export default {
  title: 'SwellWindRose',
};

export const Default = () => (
  <div className="quiver-spot-forecast-summary__thumbnail">
    <SwellWindRose
      swells={[
        { height: 2, period: 15, direction: 210, index: 0 },
        { height: 1, direction: 320, period: 20, index: 1 },
      ]}
      overallMaxHeight={2}
      wind={{ direction: 100, speed: 1 }}
      units={{
        speed: 'MPH',
        distance: 'FT',
        tideHeight: 'FT',
        windSpeed: 'MPH',
        waveHeight: 'FT',
      }}
      mapWidth={80}
      mapHeight={80}
    />
  </div>
);
