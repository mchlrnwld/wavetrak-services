import PropTypes from 'prop-types';
import React from 'react';
import { DEFAULT_SWELL_MAX, scalePowerWaveHeight } from '@surfline/web-common';
import CompassRose from '../CompassRose';
import WindArrow from '../icons/WindArrow';
import SwellRosePetalAnimator from '../SwellRose/SwellRosePetalAnimator';
import unitsPropType from '../PropTypes/units';

const SwellWindRose = ({
  swells,
  overallMaxHeight,
  wind,
  units,
  mapWidth,
  mapHeight,
}) => {
  const MAX_PIXELS = 24;
  const RADIUS = 64;
  const degreesToRadians = (degrees) => (Math.PI * degrees) / -180;
  const overallMaxSwell = Math.max(
    overallMaxHeight,
    DEFAULT_SWELL_MAX[units.swellHeight] || 0
  );

  return (
    <div className="quiver-swell-wind-rose">
      <CompassRose displayBearings />
      <div
        className="quiver-swell-wind-rose__wind-arrow"
        style={{
          top:
            mapHeight / 2 -
            12 -
            RADIUS * Math.cos(degreesToRadians(wind.direction)),
          left:
            mapWidth / 2 -
            12 -
            RADIUS * Math.sin(degreesToRadians(wind.direction)),
        }}
      >
        <WindArrow
          degrees={wind.direction}
          speed={wind.speed}
          units={units.windSpeed}
        />
      </div>
      <svg
        className="quiver-swell-wind-rose__swell-petals"
        viewBox="0 0 120 120"
        preserveAspectRatio="xMinYMin"
      >
        {[...swells]
          .sort((a, b) => b.height - a.height)
          .map((swell) => (
            <SwellRosePetalAnimator
              key={swell.index}
              swellIndex={swell.index}
              arcRadius={scalePowerWaveHeight(
                overallMaxSwell,
                swell.height,
                MAX_PIXELS
              )}
              direction={swell.direction}
            />
          ))}
      </svg>
    </div>
  );
};

SwellWindRose.propTypes = {
  units: unitsPropType.isRequired,
  wind: PropTypes.shape({
    speed: PropTypes.number,
    direction: PropTypes.number,
  }).isRequired,
  swells: PropTypes.arrayOf(
    PropTypes.shape({
      min: PropTypes.number,
      mean: PropTypes.number,
      max: PropTypes.number,
    })
  ).isRequired,
  overallMaxHeight: PropTypes.number.isRequired,
  mapWidth: PropTypes.number.isRequired,
  mapHeight: PropTypes.number.isRequired,
};

export default SwellWindRose;
