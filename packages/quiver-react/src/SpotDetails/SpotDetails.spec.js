import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import SurfConditions from '../SurfConditions';
import SpotDetails from '.';
import SpotReportSummary from '../SpotReportSummary';
import * as checkConditionValue from '../utils/checkConditionValue';

describe('components / SpotDetails', () => {
  beforeEach(() => {
    sinon.stub(checkConditionValue, 'default');
  });

  afterEach(() => {
    checkConditionValue.default.restore();
  });

  const spot = {
    _id: '5842041f4e65fad6a7708827',
    name: 'HB Pier, Northside',
    lat: 33.656781041213,
    lon: -118.0064678192,
    waveHeight: {
      min: 2,
      max: 3,
    },
    wind: {
      speed: 1,
      direction: 0,
    },
    conditions: {
      human: true,
      value: 'VERY_GOOD',
    },
    cameras: [],
  };

  const units = {
    tideHeight: 'M',
    windSpeed: 'KPH',
    waveHeight: 'M',
  };

  it('displays the spot name', () => {
    const wrapper = shallow(<SpotDetails spot={spot} units={units} />);
    expect(wrapper.find('.quiver-spot-details__name').text()).to.equal(
      spot.name
    );
  });

  it('renders and passes props to children', () => {
    checkConditionValue.default.returns('randomConditionValue');
    const wrapper = shallow(<SpotDetails spot={spot} units={units} />);
    const surfConditionswWrapper = wrapper.find(SurfConditions);
    expect(surfConditionswWrapper.prop('conditions')).to.deep.equal(
      'randomConditionValue'
    );
    const spotReportSummaryWrapper = wrapper.find(SpotReportSummary);
    expect(spotReportSummaryWrapper.prop('waveHeight')).to.deep.equal(
      spot.waveHeight
    );
    expect(spotReportSummaryWrapper.prop('tide')).to.deep.equal(null);
    expect(spotReportSummaryWrapper.prop('wind')).to.deep.equal(spot.wind);
    expect(spotReportSummaryWrapper.prop('units')).to.deep.equal(units);
  });

  it('renders and passes svenRatings prop to surfConditions', () => {
    checkConditionValue.default.returns('randomConditionValue');
    const wrapper = shallow(
      <SpotDetails spot={spot} units={units} sevenRatings />
    );
    const surfConditionswWrapper = wrapper.find(SurfConditions);
    expect(surfConditionswWrapper.prop('sevenRatings')).to.be.true();
  });
});
