import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import SurfConditions from '../SurfConditions';
import SpotReportSummary from '../SpotReportSummary';
import spotPropType from '../PropTypes/spot';
import unitsPropType from '../PropTypes/units';
import checkConditionValue from '../utils/checkConditionValue';

const getDetailsClassName = (showPremiumCamCTA) =>
  classNames({
    'quiver-spot-details': true,
    'quiver-spot-details--premium-cta': showPremiumCamCTA,
  });

const SpotDetails = ({ spot, units, showPremiumCamCTA, sevenRatings }) => (
  <div className={getDetailsClassName(showPremiumCamCTA)}>
    <h3 className="quiver-spot-details__name">{spot.name}</h3>
    <SurfConditions
      conditions={checkConditionValue(spot.conditions.value)}
      expired={spot.conditions.expired && spot.conditions.human}
      sevenRatings={sevenRatings}
    />
    <SpotReportSummary
      waveHeight={spot.waveHeight}
      tide={spot.tide ? spot.tide : null}
      wind={spot.wind}
      units={units}
    />
  </div>
);

SpotDetails.propTypes = {
  spot: spotPropType.isRequired,
  units: unitsPropType.isRequired,
  showPremiumCamCTA: PropTypes.bool,
  sevenRatings: PropTypes.bool,
};

SpotDetails.defaultProps = {
  showPremiumCamCTA: false,
  sevenRatings: false,
};

export default SpotDetails;
