/* istanbul ignore file */

import PropTypes from 'prop-types';
import React, { Component } from 'react';
import round from 'lodash/round';
import { area, curveCardinal, line, symbol, symbolTriangle } from 'd3-shape';
import { select } from 'd3-selection';
import { scaleLinear, scaleTime } from 'd3-scale';
import { closestBinarySearch } from '@surfline/web-common';
import GraphTooltip from '../GraphTooltip';
import TooltipProvider from '../TooltipProvider';
import TooltipDatetime from '../TooltipDatetime';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';

class TideGraph extends Component {
  constructor() {
    super();
    this.state = {};
    this.tideGraphRef = React.createRef();
  }

  componentDidMount() {
    this.renderGraph();
    window.addEventListener('resize', this.resizeListener);
  }

  componentDidUpdate(props) {
    const { data, isHourly } = this.props;
    if (props.data !== data || props.isHourly !== isHourly) this.renderGraph();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeListener);
  }

  dayHasData = (timestamp) => {
    const { data } = this.props;

    if (!timestamp) return false;

    const date = new Date(timestamp * 1000);
    date.setHours(0, 0, 0, 0);

    const dayStart = date.getTime() / 1000;
    const dayEnd = dayStart + 86400;

    return !!data.find(
      (d) => d?.timestamp >= dayStart && d?.timestamp <= dayEnd
    );
  };

  resizeListener = () => {
    if (this.resizeTimeout) return;
    this.resizeTimeout = setTimeout(() => {
      this.resizeTimeout = null;
      this.renderGraph();
    }, 66);
  };

  highlightedTideData = (timestamp) => {
    try {
      const { data, startDate } = this.props;

      if (
        timestamp < startDate.getTime() / 1000 ||
        !this.dayHasData(timestamp)
      ) {
        return null;
      }

      const closestIndex = closestBinarySearch(
        data,
        { timestamp },
        (tide) => tide.timestamp
      );
      const closestTimestamp = data[closestIndex].timestamp;
      const closestUtcOffset = data[closestIndex].utcOffset;
      const t0 =
        closestTimestamp <= timestamp
          ? data[closestIndex]
          : data[closestIndex - 1];
      const t1 =
        closestTimestamp <= timestamp
          ? data[closestIndex + 1]
          : data[closestIndex];
      const timespan = timestamp - t0.timestamp;
      const heightIncrement = t1.height - t0.height;
      const timeIncrement = t1.timestamp - t0.timestamp;
      const interpolatedHeight =
        timeIncrement !== 0
          ? t0.height + (timespan * heightIncrement) / timeIncrement
          : t0.height;

      return {
        height: interpolatedHeight,
        utcOffset: closestUtcOffset,
      };
    } catch (err) {
      return null;
    }
  };

  renderTooltip = (xPercentage) => {
    const { renderDot } = this.state;
    const { units, dateFormat, startDate, endDate } = this.props;
    const start = startDate?.getTime() / 1000;
    const end = endDate?.getTime() / 1000;

    if (!start || !end) {
      return null;
    }

    const timestamp = xPercentage * (end - start) + start;
    const highlightedTideData = this.highlightedTideData(timestamp);

    if (highlightedTideData) {
      renderDot(xPercentage, highlightedTideData.height);
    }

    return highlightedTideData ? (
      <GraphTooltip>
        <TooltipDatetime
          utcOffset={highlightedTideData.utcOffset}
          timestamp={timestamp}
          dateFormat={dateFormat}
          showMinutes
        />
        <div className="quiver-tide-graph__tooltip">
          {round(highlightedTideData.height, 1)} {units}
        </div>
      </GraphTooltip>
    ) : null;
  };

  renderGraph = () => {
    const {
      day,
      data,
      tideLocation,
      units,
      currentMarkerXPercentage,
      startDate,
      endDate,
    } = this.props;
    const { max, min } = tideLocation;

    const tideData = data.map((tide) => ({
      ...tide,
      dateTime: tide ? new Date(tide.timestamp * 1000) : null,
    }));

    const highLowTideData = tideData.filter((point) => point.type !== 'NORMAL');

    /**
     * Provide a bit of extra room top and bottom, since the
     * hight/low tide circles extend above and below their tide
     * corresponding data point.
     */
    const domainPadding = units === 'm' ? 0.3 : 1;
    const maxYDomain = round(max, 1) + domainPadding;
    const minYDomain = round(min, 1) - domainPadding;

    select(`.quiver-tide-graph__area--${day}`).remove();

    // Define the x-axis
    const x = scaleTime()
      .rangeRound([0, this.canvasRef.offsetWidth])
      .domain([startDate, endDate]);

    /**
     * Define the y-axis for Area path, line, and tide
     * point data points.
     */
    const y = scaleLinear()
      .rangeRound([this.canvasRef.offsetHeight, 0])
      .domain([minYDomain, maxYDomain]);

    /**
     * Define the y-axis for the min/max y labels.
     * The range is adjusted at the top label is not
     * obscured outside of the bounds of the scale.
     */
    const yMarker = scaleLinear()
      .rangeRound([this.canvasRef.offsetHeight, 6])
      .domain([minYDomain, maxYDomain]);

    // Define the root tide SVG.
    const canvas = select(
      `.quiver-tide-graph--${day} .quiver-tide-graph__canvas`
    );
    const svg = canvas
      .append('svg')
      .attr('class', `quiver-tide-graph__area quiver-tide-graph__area--${day}`);

    // Define the area below the line.
    const tideArea = area()
      .defined((d) => this.dayHasData(d?.timestamp))
      .x((d) => x(d.dateTime))
      .y0(y(minYDomain))
      .y1((d) => y(d.height));

    // Define the line.
    const tideLine = line()
      .defined((d) => this.dayHasData(d?.timestamp))
      .x((d) => x(d.dateTime))
      .y((d) => y(d.height))
      .curve(curveCardinal);

    // Append the Area under the line.
    svg
      .append('path')
      .datum(data.filter(tideArea.defined()))
      .attr('class', 'quiver-tide-graph__area__fill')
      .attr('d', tideArea(tideData));

    // Append the Line over the Area.
    svg
      .append('path')
      .datum(data.filter(tideLine.defined()))
      .attr('class', 'quiver-tide-graph__area__line')
      .attr('d', tideLine(tideData));

    svg.selectAll('.quiver-tide-graph__area__high-low').remove();

    /**
     * SVG elements cannot possess drop-shadows. These
     * are grey circles are appended at the data point
     * and then repositioned slightly down on the y-axis.
     * They serve as the high-low circles "background shadow".
     */
    highLowTideData.forEach(({ type, dateTime, height }) => {
      const highLow = svg
        .append('g')
        .attr('class', 'quiver-tide-graph__area__high-low');

      highLow
        .append('circle')
        .attr('class', 'quiver-tide-graph__area__high-low__shadow')
        .attr('cx', x(dateTime))
        .attr('cy', y(height) + 1)
        .attr('r', 8);

      highLow
        .append('circle')
        .attr('class', 'quiver-tide-graph__area__high-low__circle')
        .attr('cx', x(dateTime))
        .attr('cy', y(height))
        .attr('r', 8);

      highLow
        .append('path')
        .attr('class', 'quiver-tide-graph__area__high-low__direction')
        .attr(
          'transform',
          type === 'LOW'
            ? `translate(${x(dateTime)}, ${y(height)}) rotate(180)`
            : `translate(${x(dateTime)}, ${y(height)})`
        )
        .attr('d', symbol().size(12).type(symbolTriangle)());
    });

    /**
     *  Append the y-axis lables for to only the first day.
     *  The top label on the y-axis is padded down a few pixes to
     *  prevent it from becoming cut off by the top of the graph.
     */
    if (day === 0) {
      svg
        .selectAll('.quiver-tide-graph__area__scale-marker')
        .data([minYDomain, (maxYDomain + minYDomain) / 2, maxYDomain])
        .enter()
        .append('text')
        .attr('class', 'quiver-tide-graph__area__scale-marker')
        .attr('y', (d, i) =>
          i === 2
            ? `${yMarker(round(d, 1)) + 4}px`
            : `${yMarker(round(d, 1))}px`
        )
        .attr('x', 2)
        .text((d) => round(d, 1));
    }

    if (currentMarkerXPercentage) {
      const start = startDate.getTime() / 1000;
      const end = endDate.getTime() / 1000;
      const timestamp = currentMarkerXPercentage * (end - start) + start;
      const currentTimePoint = this.highlightedTideData(timestamp);
      if (currentTimePoint) {
        select(`.quiver-tide-graph__area--${day}`)
          .append('circle')
          .attr('r', 4)
          .attr(
            'class',
            'quiver-tide-graph__area__dot quiver-tide-graph__area__dot--current'
          )
          .attr('cx', `${currentMarkerXPercentage * 100}%`)
          .attr('cy', y(currentTimePoint.height));
      }
    }

    const tideDot = select(`.quiver-tide-graph__area--${day}`)
      .append('circle')
      .attr('r', 4)
      .attr('class', 'quiver-tide-graph__area__dot')
      .style('opacity', 0);

    this.setState({
      renderDot: (xPercentage, height) => {
        tideDot
          .attr('cx', `${xPercentage * 100}%`)
          .attr('cy', y(height))
          .style('opacity', 1);
      },
      hideDot: () => {
        tideDot.style('opacity', 0);
      },
    });
  };

  render() {
    const { hideDot } = this.state;
    const { day, isMultiCamPage, dateFormat } = this.props;

    return (
      <div
        ref={this.tideGraphRef}
        className={`quiver-tide-graph quiver-tide-graph--${day}`}
      >
        <TooltipProvider
          renderTooltip={this.renderTooltip}
          onHideTooltip={hideDot}
          parentRef={isMultiCamPage && this.tideGraphRef.current}
          dateFormat={dateFormat}
        >
          <div
            ref={(el) => {
              this.canvasRef = el;
            }}
            className={`quiver-tide-graph__canvas quiver-tide-graph__canvas--${day}`}
          />
        </TooltipProvider>
      </div>
    );
  }
}

TideGraph.propTypes = {
  day: PropTypes.number,
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  tideLocation: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number,
  }).isRequired,
  units: PropTypes.string.isRequired,
  currentMarkerXPercentage: PropTypes.number,
  isHourly: PropTypes.bool,
  isMultiCamPage: PropTypes.bool,
  dateFormat: dateFormatPropType,
  startDate: PropTypes.instanceOf(Date).isRequired,
  endDate: PropTypes.instanceOf(Date).isRequired,
};

TideGraph.defaultProps = {
  currentMarkerXPercentage: null,
  isHourly: false,
  day: 0,
  isMultiCamPage: false,
  dateFormat: defaultDateFormat,
};
export default TideGraph;
