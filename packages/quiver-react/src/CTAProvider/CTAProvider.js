import React from 'react';
import PropTypes from 'prop-types';
import { trackClickedSubscribeCTA } from '@surfline/web-common';

const CTAProvider = ({ children, segmentProperties, funnelUrl }) => (
  <span className="sl-cta-provider">
    {React.cloneElement(children, {
      onClickPaywall: () => trackClickedSubscribeCTA(segmentProperties),
      href: funnelUrl,
    })}
  </span>
);

CTAProvider.defaultProps = {
  funnelUrl: '/upgrade',
};

CTAProvider.propTypes = {
  children: PropTypes.node.isRequired,
  segmentProperties: PropTypes.shape({}).isRequired,
  funnelUrl: PropTypes.string,
};

export default CTAProvider;
