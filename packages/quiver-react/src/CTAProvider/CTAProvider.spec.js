import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import common from '@surfline/web-common';
import CTAProvider from './CTAProvider';
import PremiumCamCTA from '../PremiumCamCTA';

describe('components / CTAProvider', () => {
  it('binds segmentProperties to the clickHandler', () => {
    const clickedSubscribeCTA = sinon.stub(common, 'trackClickedSubscribeCTA');
    const segmentProperties = {
      ctaLocation: 'bottom',
      location: 'bottom',
      pageName: 'map',
    };
    const wrapper = mount(
      <CTAProvider segmentProperties={segmentProperties}>
        <PremiumCamCTA
          href="https://surfline.com"
          camStillUrl="https://camstills.cdn-surfline.com/wc-lowers/latest_small.jpg"
        />
      </CTAProvider>
    );
    const providerWrapper = wrapper.find('.sl-cta-provider');
    providerWrapper.find('.quiver-button').prop('onClick')();
    expect(clickedSubscribeCTA).to.have.been.calledOnce();
    expect(clickedSubscribeCTA).to.have.been.calledWithExactly(
      segmentProperties
    );
  });
});
