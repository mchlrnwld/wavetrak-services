import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import common from '@surfline/web-common';
import TrackableLink from './TrackableLink';

const properties = {};

describe('TrackableLink', () => {
  let setupLinkTrackingStub;

  beforeEach(() => {
    setupLinkTrackingStub = sinon.stub(common, 'setupLinkTracking');
  });

  afterEach(() => {
    setupLinkTrackingStub.restore();
  });

  it('should not call setupLinkTracking if no ref exists', () => {
    const ref = { current: null };
    mount(
      <TrackableLink ref={ref} eventName="track" eventProperties={properties}>
        <a href="/">Link</a>
      </TrackableLink>
    );

    expect(setupLinkTrackingStub).to.have.not.been.called();
  });

  it('should render the link and call setupLinkTracking', () => {
    const ref = { current: 'ref' };

    const wrapper = mount(
      <TrackableLink eventName="track" eventProperties={properties} ref={ref}>
        <a href="/">Link</a>
      </TrackableLink>
    );

    const link = wrapper.find(TrackableLink);
    const anchorTag = wrapper.find('a');

    expect(anchorTag).to.have.length(1);
    expect(anchorTag.prop('href')).to.be.equal('/');

    expect(link).to.have.length(1);
    expect(link.prop('eventName')).to.be.equal('track');
    expect(link.prop('eventProperties')).to.be.equal(properties);

    expect(setupLinkTrackingStub).to.have.been.calledOnceWithExactly(
      ref.current,
      'track',
      properties
    );
  });
});
