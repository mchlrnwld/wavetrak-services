import React, { useEffect } from 'react';
import { setupLinkTracking } from '@surfline/web-common';
import PropTypes from 'prop-types';

const TrackableLink = React.forwardRef(
  /**
   * @description A link component which can be used to fire
   * track calls to segment when the link is clicked
   * https://segment.com/docs/connections/sources/catalog/libraries/website/javascript/#track-link
   *
   * @param {object} props
   * @param {React.ReactChild} props.children
   * @param {string | (() => object)} props.eventName An event name, or a function which returns an event name
   * @param {{} | (() => object)} props.eventProperties Event properties, or a function which returns
   * event properties
   * @param {React.RefObject<HTMLAnchorElement>} ref
   */
  ({ children, eventName, eventProperties }, ref) => {
    useEffect(() => {
      if (ref.current) {
        setupLinkTracking(ref.current, eventName, eventProperties);
      }
    }, [ref, eventName, eventProperties]);

    return children;
  }
);

TrackableLink.propTypes = {
  eventName: PropTypes.oneOfType([PropTypes.string, PropTypes.func]).isRequired,
  eventProperties: PropTypes.oneOfType([PropTypes.shape(), PropTypes.func])
    .isRequired,
  children: PropTypes.element,
};

TrackableLink.defaultProps = {
  children: null,
};

export default TrackableLink;
