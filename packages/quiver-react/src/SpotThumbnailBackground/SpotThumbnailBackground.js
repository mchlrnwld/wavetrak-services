import React from 'react';
import spotPropTypes from '../PropTypes/spot';

const SpotThumbnailBackground = ({ spot }) => {
  const spotCardThumbnail =
    (spot.thumbnail && spot.thumbnail['300']) || spot.thumbnail || null;

  return (
    <div
      className="quiver-spot-thumbnail-background"
      style={{
        backgroundImage: `url(${spotCardThumbnail})`,
      }}
    />
  );
};

SpotThumbnailBackground.propTypes = {
  spot: spotPropTypes,
};

SpotThumbnailBackground.defaultProps = {
  spot: {},
};

export default SpotThumbnailBackground;
