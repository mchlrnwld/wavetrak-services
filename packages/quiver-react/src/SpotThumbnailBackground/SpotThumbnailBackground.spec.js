import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import SpotThumbnailBackground from './SpotThumbnailBackground';

describe('components / SpotThumbnail', () => {
  const props = {
    spot: {
      thumbnail: 'test1',
    },
  };

  it('renders the thumbnail as a background', () => {
    const wrapper = mount(<SpotThumbnailBackground {...props} />);
    const background = wrapper.find('.quiver-spot-thumbnail-background');
    expect(background.prop('style')).to.deep.equal({
      backgroundImage: 'url(test1)',
    });
  });

  it('chooses the 300 size thumbnail if present', () => {
    const wrapper = mount(
      <SpotThumbnailBackground spot={{ thumbnail: { 300: 'test2' } }} />
    );
    const background = wrapper.find('.quiver-spot-thumbnail-background');
    expect(background.prop('style')).to.deep.equal({
      backgroundImage: 'url(test2)',
    });
  });
});
