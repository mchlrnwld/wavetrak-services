import React from 'react';
import { action } from '@storybook/addon-actions';
import EditorialArticleCard from './EditorialArticleCard';

const props = {
  publishedDateTime: '8 days ago',
  author: 'Marc Beaty',
  title: 'Slater Wavepool Proposed for Florida',
  subtitle: 'Is Kelly bringing his artificial wave tech to his home state?',
  permalink: '#permalink',
  media: {
    type: 'image',
    feed2x:
      'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/29134430/julian-wilson-wayward-video-huge-alley-oop-bali-1.jpg',
  },
  tags: [
    { name: 'Best Bet', url: '#bestbet' },
    { name: 'Features', url: '#features' },
  ],
  funnelUrl: '#funnel',
  onClickContent: action('clicked content'),
  onClickPaywall: action('clicked paywall'),
  onClickTag: action('clicked tag'),
};

export default {
  title: 'EditorialArticleCard',
};

export const Default = () => <EditorialArticleCard {...props} />;

Default.story = {
  name: 'default',
};

export const NoSubtitle = () => (
  <EditorialArticleCard {...props} subtitle={null} />
);

NoSubtitle.story = {
  name: 'no subtitle',
};

export const PremiumUpsellPaywall = () => (
  <EditorialArticleCard {...props} showPaywall />
);

PremiumUpsellPaywall.story = {
  name: 'premium upsell paywall',
};
