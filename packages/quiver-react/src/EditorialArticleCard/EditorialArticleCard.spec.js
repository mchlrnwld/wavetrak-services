import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import BaseContentCard from '../BaseContentCard';
import BaseContentCardPaywall from '../BaseContentCardPaywall';
import EditorialArticleCard from './EditorialArticleCard';

describe('EditorialArticleCard', () => {
  const props = {
    publishedDateTime: '8 days ago',
    author: 'Marc Beaty',
    title: 'Slater Wavepool Proposed for Florida',
    subtitle: 'Is Kelly bringing his artificial wave tech to his home state?',
    permalink: '#permalink',
    media: {
      type: 'image',
      feed2x:
        'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2017/03/29134430/julian-wilson-wayward-video-huge-alley-oop-bali-1.jpg',
    },
    tags: [
      { name: 'Best Bet', url: '#bestbet' },
      { name: 'Features', url: '#features' },
    ],
    funnelUrl: '#funnel',
    onClickContent: sinon.stub(),
    onClickPaywall: sinon.stub(),
    onClickTag: sinon.stub(),
  };

  it('should render clickable content inside a BaseContentCard', () => {
    const wrapper = shallow(<EditorialArticleCard {...props} />);
    const baseContentCard = wrapper.find(BaseContentCard);
    expect(baseContentCard.prop('meta')).to.equal('By Marc Beaty • 8 days ago');
    expect(baseContentCard.prop('tags')).to.equal(props.tags);
    expect(baseContentCard.prop('onClickTag')).to.equal(props.onClickTag);

    const children = baseContentCard.children();
    expect(children.is('a')).to.be.true();
    expect(children.prop('href')).to.equal(props.permalink);

    expect(props.onClickContent).not.to.have.been.called();
    children.simulate('click');
    expect(props.onClickContent).to.have.been.calledOnce();

    const title = children.find('h2');
    expect(title).to.have.length(1);
    expect(title.text()).to.equal(props.title);

    const subtitle = children.find('.quiver-editorial-article-card__subtitle');
    expect(subtitle).to.have.length(1);
    expect(subtitle.text()).to.equal(props.subtitle);

    const media = children.find('.quiver-editorial-article-card__media');
    expect(media).to.have.length(1);
    expect(media.prop('style')).to.deep.equal({
      backgroundImage: `url(${props.media.feed2x})`,
    });
  });

  it('should render external link', () => {
    const externalProps = {
      ...props,
      externalLink: 'External URL',
      externalSource: 'Google',
      newWindow: true,
    };
    const wrapper = shallow(
      <EditorialArticleCard {...externalProps} media={null} />
    );
    const baseContentCard = wrapper.find(BaseContentCard);
    expect(baseContentCard.prop('meta')).to.equal('Via Google • 8 days ago');
    expect(baseContentCard.prop('tags')).to.equal(props.tags);
    expect(baseContentCard.prop('onClickTag')).to.equal(props.onClickTag);

    const children = baseContentCard.children();
    expect(children.is('a')).to.be.true();
    expect(children.prop('href')).to.equal(props.permalink);
    expect(children.prop('target')).to.equal('_blank');
  });

  it('should not render media if not provided', () => {
    const wrapper = shallow(<EditorialArticleCard {...props} media={null} />);
    const baseContentCard = wrapper.find(BaseContentCard);
    const children = baseContentCard.children();
    const media = children.find('.quiver-editorial-article-card__media');
    expect(media).to.have.length(0);
  });

  it('should render paywall instead of content if showPaywall is true', () => {
    const defaultWrapper = shallow(<EditorialArticleCard {...props} />);
    const defaultBaseContentCard = defaultWrapper.find(BaseContentCard);
    const defaultChildren = defaultBaseContentCard.children();
    expect(defaultChildren.find(BaseContentCardPaywall)).to.have.length(0);
    expect(defaultChildren.find('a')).to.have.length(1);

    const showPaywallWrapper = shallow(
      <EditorialArticleCard {...props} showPaywall />
    );
    const showPaywallBaseContentCard = showPaywallWrapper.find(BaseContentCard);
    const showPaywallChildren = showPaywallBaseContentCard.children();
    const paywall = showPaywallChildren.find(BaseContentCardPaywall);
    expect(paywall).to.have.length(1);
    expect(paywall.prop('funnelUrl')).to.equal(props.funnelUrl);
    expect(paywall.prop('onClick')).to.equal(props.onClickPaywall);
    expect(paywall.prop('message')).to.equal('Unlock Premium Content');
    expect(showPaywallChildren.find('a')).to.have.length(0);
  });

  it('should render paywall instead of content if showPaywall is true', () => {
    const defaultWrapper = shallow(<EditorialArticleCard {...props} />);
    const defaultBaseContentCard = defaultWrapper.find(BaseContentCard);
    const defaultChildren = defaultBaseContentCard.children();
    expect(defaultChildren.find(BaseContentCardPaywall)).to.have.length(0);
    expect(defaultChildren.find('a')).to.have.length(1);

    const showPaywallWrapper = shallow(
      <EditorialArticleCard {...props} showPaywall />
    );
    const showPaywallBaseContentCard = showPaywallWrapper.find(BaseContentCard);
    const showPaywallChildren = showPaywallBaseContentCard.children();
    const paywall = showPaywallChildren.find(BaseContentCardPaywall);
    expect(paywall).to.have.length(1);
    expect(paywall.prop('funnelUrl')).to.equal(props.funnelUrl);
    expect(paywall.prop('onClick')).to.equal(props.onClickPaywall);
    expect(paywall.prop('message')).to.equal('Unlock Premium Content');
    expect(showPaywallChildren.find('a')).to.have.length(0);
  });
});
