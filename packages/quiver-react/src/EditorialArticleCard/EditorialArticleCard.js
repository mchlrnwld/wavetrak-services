import React from 'react';
import PropTypes from 'prop-types';

import BaseContentCard from '../BaseContentCard';
import BaseContentCardPaywall from '../BaseContentCardPaywall';

const getBaseContentMeta = (
  externalLink,
  externalSource,
  author,
  publishedDateTime
) => {
  let metaText = `By ${author} • ${publishedDateTime}`;
  if (externalSource && externalLink === 'External URL') {
    metaText = `Via ${externalSource} • ${publishedDateTime}`;
  }
  return metaText;
};

const EditorialArticleCard = ({
  author,
  publishedDateTime,
  title,
  subtitle,
  permalink,
  media,
  tags,
  showPaywall,
  funnelUrl,
  onClickContent,
  onClickPaywall,
  onClickTag,
  externalLink,
  externalSource,
  newWindow,
}) => (
  <div className="quiver-editorial-article-card">
    <BaseContentCard
      meta={getBaseContentMeta(
        externalLink,
        externalSource,
        author,
        publishedDateTime
      )}
      tags={tags}
      onClickTag={onClickTag}
      showPaywall={showPaywall}
    >
      {showPaywall ? (
        <div>
          <h2>{title}</h2>
          <BaseContentCardPaywall
            funnelUrl={funnelUrl}
            onClick={onClickPaywall}
            message="Unlock Premium Content"
          />
        </div>
      ) : (
        <a
          href={permalink}
          onClick={onClickContent}
          target={newWindow ? '_blank' : '_self'}
          rel="noreferrer"
        >
          <h2>{title}</h2>
          <p className="quiver-editorial-article-card__subtitle">{subtitle}</p>
          {media ? (
            <div
              className="quiver-editorial-article-card__media"
              style={{
                backgroundImage: `url(${media.feed2x})`,
              }}
            />
          ) : null}
        </a>
      )}
    </BaseContentCard>
  </div>
);

EditorialArticleCard.propTypes = {
  author: PropTypes.string.isRequired,
  publishedDateTime: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  permalink: PropTypes.string.isRequired,
  media: PropTypes.shape({
    feed2x: PropTypes.string.isRequired,
  }),
  tags: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired,
    })
  ).isRequired,
  showPaywall: PropTypes.bool,
  funnelUrl: PropTypes.string,
  onClickContent: PropTypes.func,
  onClickPaywall: PropTypes.func,
  onClickTag: PropTypes.func,
  externalLink: PropTypes.string,
  externalSource: PropTypes.string,
  newWindow: PropTypes.bool,
};

EditorialArticleCard.defaultProps = {
  subtitle: null,
  media: null,
  showPaywall: false,
  funnelUrl: null,
  onClickContent: null,
  onClickPaywall: null,
  onClickTag: null,
  externalLink: null,
  externalSource: null,
  newWindow: false,
};

export default EditorialArticleCard;
