import React from 'react';
import ShareArticle from './index';

export default {
  title: 'ShareArticle',
};

export const Default = () => (
  <ShareArticle
    url="https://www.surfline.com"
    onClickLink={(channel) => console.log(channel)}
  />
);

Default.story = {
  name: 'default',
};
