import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { FacebookShareButton, TwitterShareButton } from 'react-share';
import ClipboardIcon from '../icons/ClipboardIcon';
import FacebookIcon from '../icons/FacebookIcon';
import TwitterIcon from '../icons/TwitterIcon';
import Alert from '../Alert';

class ShareArticle extends Component {
  constructor(props) {
    super(props);
    this.state = { copied: false };
  }

  getAlertClassNames = (copied) =>
    classNames({
      'quiver-share-article__alert': true,
      'quiver-share-article__alert--active': copied,
    });

  clickedShareIcon = (shareChannel) => {
    const { onClickLink } = this.props;
    onClickLink(shareChannel);
  };

  copyUrlToClipboard = () => {
    this.setState({ copied: true });
    this.clickedShareIcon('url');
    setTimeout(() => this.setState({ copied: false }), 2500);
  };

  render() {
    const { url } = this.props;
    const { copied } = this.state;
    return (
      <div className="quiver-share-article">
        <FacebookShareButton
          url={url}
          className="quiver-share-article__facebook"
          beforeOnClick={() => this.clickedShareIcon('facebook')}
        >
          <FacebookIcon />
        </FacebookShareButton>
        <TwitterShareButton
          url={url}
          className="quiver-share-article__twitter"
          beforeOnClick={() => this.clickedShareIcon('twitter')}
        >
          <TwitterIcon />
        </TwitterShareButton>
        <CopyToClipboard
          text={url}
          onCopy={this.copyUrlToClipboard}
          className="quiver-share-article__clipboard"
        >
          <button type="button">
            <ClipboardIcon />
          </button>
        </CopyToClipboard>
        <div className={this.getAlertClassNames(copied)}>
          <Alert>
            <span className="quiver-share-article__alert__message">
              Link copied to clipboard
            </span>
          </Alert>
        </div>
      </div>
    );
  }
}

ShareArticle.propTypes = {
  url: PropTypes.string.isRequired,
  onClickLink: PropTypes.func.isRequired,
};

export default ShareArticle;
