import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';
import ShareArticle from './ShareArticle';
import ClipboardIcon from '../icons/ClipboardIcon';
import FacebookIcon from '../icons/FacebookIcon';
import TwitterIcon from '../icons/TwitterIcon';
import Alert from '../Alert';

describe('ShareArticle', () => {
  const props = {
    onClickLink: sinon.spy(),
  };
  it('renders FacebookIcon, TwitterIcon, ClipboardIcon and Alert', () => {
    const wrapper = shallow(
      <ShareArticle {...props} url="https://www.surfline.com" />
    );
    const facebookIconWrapper = wrapper.find(FacebookIcon);
    const twitterIconWrapper = wrapper.find(TwitterIcon);
    const clipboardIconWrapper = wrapper.find(ClipboardIcon);
    const alertWrapper = wrapper.find(Alert);
    expect(facebookIconWrapper).to.have.length(1);
    expect(twitterIconWrapper).to.have.length(1);
    expect(clipboardIconWrapper).to.have.length(1);
    expect(alertWrapper).to.have.length(1);
  });

  it('should execute the onClick function on correct shareIcon click', () => {
    const wrapper = mount(
      <ShareArticle {...props} url="https://www.surfline.com" />
    );

    expect(props.onClickLink).not.to.have.been.called();
    wrapper
      .find('.quiver-share-article__facebook')
      .hostNodes()
      .simulate('click');
    expect(props.onClickLink).to.have.been.calledOnce();
    expect(props.onClickLink).to.have.been.calledWithExactly('facebook');
  });
});
