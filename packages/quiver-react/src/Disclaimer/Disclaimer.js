import React from 'react';
import PropTypes from 'prop-types';

/**
 * @param {Object} props
 * @param {string} props.text
 * @param {string} props.url
 * @returns {JSX.Element}
 */
const DisclaimerLink = ({ text, url }) => (
  <a
    className="quiver-disclaimer-link"
    href={url}
    target="_blank"
    rel="noopener noreferrer"
  >
    {text}
  </a>
);

/**
 * @param {Object} props
 * @param {string} props.privacyPolicyURL
 * @param {string} props.termsConditionsURL
 * @param {boolean} props.hideTrialText
 * @returns {JSX.Element}
 */
const Disclaimer = ({
  privacyPolicyURL,
  termsConditionsURL,
  hideTrialText,
}) => (
  <p className="quiver-disclaimer">
    {`${
      !hideTrialText
        ? `We'll send an email reminder 7 days before your trial ends. Cancellation is easy, just go to Account > Subscriptions. `
        : ''
    }By starting a free trial or subscription, you agree to our `}
    <DisclaimerLink url={privacyPolicyURL} text="Privacy policy" />
    {' and '}
    <DisclaimerLink url={termsConditionsURL} text="Terms of Use" />.
  </p>
);

Disclaimer.propTypes = {
  privacyPolicyURL: PropTypes.string.isRequired,
  termsConditionsURL: PropTypes.string.isRequired,
  hideTrialText: PropTypes.bool,
};

Disclaimer.defaultProps = {
  hideTrialText: false,
};

DisclaimerLink.propTypes = {
  text: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default Disclaimer;
