import React from 'react';
import RegWallContainer from './index';

const parentContainerStyles = {
  width: 'auto',
};
const ChildComponent = () => (
  <div
    style={{
      width: '200px',
      height: '200px',
      padding: '15px',
      background: 'white',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    }}
  >
    Sample Content
  </div>
);

export default {
  title: 'RegWallContainer',
};

export const Default = () => (
  <div style={parentContainerStyles}>
    <RegWallContainer>
      <ChildComponent />
    </RegWallContainer>
  </div>
);

Default.story = {
  name: 'default',
};

export const CustomBackground = () => (
  <div style={parentContainerStyles}>
    <RegWallContainer bgColor="#011D38">
      <ChildComponent />
    </RegWallContainer>
  </div>
);
