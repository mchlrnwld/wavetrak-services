import React from 'react';
import PropTypes from 'prop-types';
import useLockBodyScroll from './useLockBodyScroll';

/**
 * Regwall Container Component
 *
 * @function RegWallContainer
 * @param {Object} props
 * @param {boolean} props.lockScroll - Whether or not the reg wall should lock the scrolling
 * @param {React.ReactChild} props.children
 * @param {string} props.bgColor
 * @return {JSX.Element}
 */
const RegWallContainer = ({ lockScroll, children, bgColor }) => {
  useLockBodyScroll(lockScroll);

  return (
    <div className="quiver-regwall__container" style={{ background: bgColor }}>
      {children}
    </div>
  );
};

RegWallContainer.propTypes = {
  children: PropTypes.node.isRequired,
  bgColor: PropTypes.string,
  lockScroll: PropTypes.bool,
};

RegWallContainer.defaultProps = {
  bgColor: null,
  lockScroll: true,
};

export default RegWallContainer;
