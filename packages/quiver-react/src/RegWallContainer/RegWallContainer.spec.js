import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import RegWallContainer from './RegWallContainer';

describe('RegWallContainer', () => {
  it('should render RegWall Container into DOM', () => {
    const wrapper = shallow(<RegWallContainer />);
    const container = wrapper.find('.quiver-regwall__container');
    expect(container).to.have.length(1);
  });
  it('should render RegWall Container into DOM with custom BG color', () => {
    const wrapper = shallow(<RegWallContainer bgColor="#011D38" />);
    const container = wrapper.find('.quiver-regwall__container');
    expect(container.prop('style')).to.have.property('background', '#011D38');
    expect(container).to.have.length(1);
  });
});
