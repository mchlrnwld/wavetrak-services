import { useRef, useEffect } from 'react';
import {
  disablePageScroll,
  enablePageScroll,
  setFillGapMethod,
} from 'scroll-lock';

setFillGapMethod('none');

/**
 * @param {boolean} [enabled]
 * @param {import('react').RefObject<HTMLElement>} [ref]
 */
const useLockBodyScroll = (enabled = true, ref = null) => {
  const disabledRef = useRef(false);

  // scroll-lock uses an internal queue. we need to ensure that we only enable
  // scrolling here if we have already disabled scrolling.
  useEffect(() => {
    const current = ref?.current;

    if (enabled && !disabledRef.current) {
      disablePageScroll(ref ? current : undefined);
      disabledRef.current = true;
    }
    return () => {
      if (disabledRef.current) {
        enablePageScroll(ref ? current : undefined);
        disabledRef.current = false;
      }
    };
  }, [enabled, ref, disabledRef]);
};

export default useLockBodyScroll;
