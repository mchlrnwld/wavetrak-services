import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import deepFreeze from 'deep-freeze';
import SpotThumbnail from '../SpotThumbnail';
import SpotDetails from '../SpotDetails';
import SpotListItem from '.';

describe('quiver / SpotListItem', () => {
  const defaultSpot = {
    name: '',
    conditions: {},
    waveHeight: {},
    tide: {},
    wind: {},
    cameras: [],
  };

  it('adds spot camera still URL as background image', () => {
    const cameras = [{ stillUrl: 'https://somecameraurl' }];
    const spot = { ...defaultSpot, cameras };
    const wrapper = shallow(<SpotListItem spot={spot} units={{}} />);
    const containerDiv = wrapper.find('div.quiver-spot-list-item');
    expect(containerDiv.prop('style')).to.deep.equal({
      backgroundImage: "url('https://somecameraurl')",
    });
  });

  it('passes spot data into child components', () => {
    const units = deepFreeze({
      tideHeight: 'FT',
      waveHeight: 'FT',
      windSpeed: 'KT',
    });
    const spot = deepFreeze({
      name: 'HB Pier, Southside',
      waveHeight: {
        min: 2,
        max: 3,
      },
      wind: {
        direction: 372,
        speed: 10,
      },
      tide: {
        next: {},
      },
      conditions: {
        human: true,
        value: 'VERY_GOOD',
      },
      cameras: [],
    });
    const wrapper = shallow(<SpotListItem spot={spot} units={units} />);
    const spotThumbnail = wrapper.find(SpotThumbnail);
    const spotDetails = wrapper.find(SpotDetails);

    expect(spotThumbnail.prop('windDirection')).to.equal(spot.wind.direction);
    expect(spotThumbnail.prop('windSpeed')).to.equal(spot.wind.speed);
    expect(spotThumbnail.prop('units')).to.equal(units);
    expect(spotDetails.prop('spot')).to.equal(spot);
    expect(spotDetails.prop('units')).to.equal(units);
  });

  it('passes sevenRatings prop to SpotDetails', () => {
    const units = deepFreeze({
      tideHeight: 'FT',
      waveHeight: 'FT',
      windSpeed: 'KT',
    });
    const spot = deepFreeze({
      name: 'HB Pier, Southside',
      waveHeight: {
        min: 2,
        max: 3,
      },
      wind: {
        direction: 372,
        speed: 10,
      },
      tide: {
        next: {},
      },
      conditions: {
        human: true,
        value: 'VERY_GOOD',
      },
      cameras: [],
    });
    const wrapper = shallow(
      <SpotListItem spot={spot} units={units} sevenRatings />
    );
    const spotDetails = wrapper.find(SpotDetails);

    expect(spotDetails.prop('sevenRatings')).to.be.true();
  });
});
