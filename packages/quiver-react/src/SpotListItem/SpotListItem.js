import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import spotPropType from '../PropTypes/spot';
import unitsPropType from '../PropTypes/units';
import SpotThumbnail from '../SpotThumbnail';
import SpotListCTAThumbnail from '../SpotListCTAThumbnail';
import SpotDetails from '../SpotDetails';

const getBackgroundImg = (spot) =>
  spot.cameras[0] ? spot.cameras[0].stillUrl : spot.thumbnail;

const getClassName = (mobileCard) =>
  classNames({
    'quiver-spot-list-item__container': true,
    'quiver-spot-list-item__container--mobile-card': mobileCard,
  });

const SpotListItem = ({
  spot,
  units,
  showPremiumCamCTA,
  mobileCard,
  sevenRatings,
}) => (
  <div className={getClassName(mobileCard)}>
    <div
      style={{ backgroundImage: `url('${getBackgroundImg(spot)}')` }}
      className="quiver-spot-list-item"
    >
      <div className="quiver-spot-list-item__container">
        {showPremiumCamCTA ? (
          <SpotListCTAThumbnail spot={spot} />
        ) : (
          <SpotThumbnail
            windDirection={spot.wind.direction}
            windSpeed={spot.wind.speed}
            units={units}
            hasCamera={spot.cameras.length > 0}
          />
        )}
        <SpotDetails
          showPremiumCamCTA={showPremiumCamCTA}
          spot={spot}
          units={units}
          sevenRatings={sevenRatings}
        />
      </div>
    </div>
  </div>
);

SpotListItem.propTypes = {
  spot: spotPropType.isRequired,
  units: unitsPropType.isRequired,
  showPremiumCamCTA: PropTypes.bool,
  mobileCard: PropTypes.bool,
  sevenRatings: PropTypes.bool,
};

SpotListItem.defaultProps = {
  showPremiumCamCTA: false,
  mobileCard: false,
  sevenRatings: false,
};

export default SpotListItem;
