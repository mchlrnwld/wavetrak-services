/* eslint-disable max-len */
import React from 'react';
import SpotListItem from './SpotListItem';

const props = {
  location: 'travel page',
  router: false,
  spot: {
    _id: '584204214e65fad6a7709d24',
    thumbnail:
      'https://s3-us-west-1.amazonaws.com/sl-spot-thumbnails-sandbox/spots/584204214e65fad6a7709d24/584204214e65fad6a7709d24_1500.jpg',
    rank: {},
    subregionId: '58581a836630e24c4487900b',
    lat: 33.90362759385167,
    lon: -118.4231436252594,
    name: 'El Porto North',
    tide: {
      previous: {
        type: 'LOW',
        height: 0.1,
        timestamp: 1554729000,
        utcOffset: -7,
      },
      current: {
        type: 'NORMAL',
        height: 1.3,
        timestamp: 1554737792,
        utcOffset: -7,
      },
      next: {
        type: 'HIGH',
        height: 3.4,
        timestamp: 1554751380,
        utcOffset: -7,
      },
    },
    waterTemp: {},
    cameras: {
      0: {
        _id: '5834992fe411dc743a5d528a',
        title: 'SOCAL - El Porto North',
        streamUrl:
          'https://cams.cdn-surfline.com/cdn-wc/wc-elportofixed/playlist.m3u8',
        stillUrl:
          'https://camstills.cdn-surfline.com/wc-elportofixed/latest_small.jpg',
        rewindBaseUrl:
          'https://camrewinds.cdn-surfline.com/wc-elportofixed/wc-elportofixed',
        isPremium: false,
        alias: 'wc-elportofixed',
        status: {
          isDown: false,
          message: '',
          subMessage: '',
          altMessage: '',
        },
        control:
          'https://camstills.cdn-surfline.com/wc-elportofixed/latest_small.jpg',
        nighttime: false,
        rewindClip:
          'https://camrewinds.cdn-surfline.com/wc-elportofixed/wc-elportofixed.1300.2019-04-07.mp4',
      },
    },
    conditions: {
      human: true,
      value: 'FAIR_TO_GOOD',
      sortableCondition: 1,
      expired: false,
    },
    wind: {},
    weather: {},
    waveHeight: {},
    legacyId: 142785,
    legacyRegionId: 2951,
    timezone: 'America/Los_Angeles',
    saveFailures: 0,
    swells: {},
    offshoreDirection: 242,
    parentTaxonomy: {},
    slug: 'DEPRECATED',
  },
  units: {
    tideHeight: 'FT',
    waveHeight: 'FT',
    windSpeed: 'KTS',
  },
  active: false,
  showPremiumCamCTA: true,
};

export default {
  title: 'SpotListItem',
};

export const Default = () => (
  <div style={{ width: '300px' }}>
    <SpotListItem {...props} />
  </div>
);

Default.story = {
  name: 'default',
};

export const MobileCard = () => (
  <div style={{ width: '300px' }}>
    <SpotListItem mobileCard {...props} />
  </div>
);

MobileCard.story = {
  name: 'mobileCard',
};

export const MobileCardConditionsV2 = () => (
  <div style={{ width: '300px' }}>
    <SpotListItem mobileCard {...props} sevenRatings />
  </div>
);

MobileCardConditionsV2.story = {
  name: 'mobileCard new condition colors',
};
