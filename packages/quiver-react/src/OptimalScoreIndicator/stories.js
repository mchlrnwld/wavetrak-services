import React from 'react';
import OptimalScoreIndicator from './OptimalScoreIndicator';

export default {
  title: 'OptimalScoreIndicator',
};

export const OptimalScoreZero = () => (
  <OptimalScoreIndicator optimalScore={0} />
);
export const OptimalScore = () => <OptimalScoreIndicator optimalScore={1} />;
export const OptimalScoreTwo = () => <OptimalScoreIndicator optimalScore={2} />;
