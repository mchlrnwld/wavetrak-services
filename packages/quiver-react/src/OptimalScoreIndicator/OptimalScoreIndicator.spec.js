import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import OptimalScoreIndicator from './OptimalScoreIndicator';

describe('OptimalScoreIndicator', () => {
  it('render suboptimal score', () => {
    const wrapper = mount(<OptimalScoreIndicator optimalScore={0} />);

    expect(
      wrapper.find('.quiver-optimal-score-indicator--suboptimal')
    ).to.have.length(1);
  });
  it('renders good score', () => {
    const wrapper = mount(<OptimalScoreIndicator optimalScore={1} />);

    expect(
      wrapper.find('.quiver-optimal-score-indicator--good')
    ).to.have.length(1);
  });
  it('renders optimal score', () => {
    const wrapper = mount(<OptimalScoreIndicator optimalScore={2} />);

    expect(
      wrapper.find('.quiver-optimal-score-indicator--optimal')
    ).to.have.length(1);
  });
});
