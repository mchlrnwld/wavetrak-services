import PropTypes from 'prop-types';
import classNames from 'classnames';
import React from 'react';

const optimalScoreClass = (optimalValue) =>
  classNames({
    'quiver-optimal-score-indicator': true,
    [`quiver-optimal-score-indicator--${optimalValue?.toLowerCase()}`]:
      optimalValue,
  });

const OPTIMAL_VALUE_MAP = {
  0: 'SUBOPTIMAL',
  1: 'GOOD',
  2: 'OPTIMAL',
};

const OptimalScoreIndicator = ({ optimalScore }) => {
  const optimalValue = OPTIMAL_VALUE_MAP[optimalScore];

  return <div className={optimalScoreClass(optimalValue)} />;
};

OptimalScoreIndicator.propTypes = {
  optimalScore: PropTypes.number.isRequired,
};

export default OptimalScoreIndicator;
