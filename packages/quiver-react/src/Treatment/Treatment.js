import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Spinner from '../Spinner';
import { useTreatment } from '../hooks/useTreatment';
import {
  TREATMENT_WRAPPER_CLASS,
  TREATMENT_WRAPPER_LOADING_CLASS,
} from '../utils/constants';

export const TREATMENT_REQUIRED = 'Treatment name is required';
export const CHILDREN_AND_RENDER_ERROR =
  'Passing both a render function and children to <Treatment /> ' +
  'will cause unexpected behavior. Please choose one or the other.';

/**
 * @param {boolean} loading
 */
const getTreatmentClass = (loading) =>
  classNames({
    [TREATMENT_WRAPPER_CLASS]: true,
    [TREATMENT_WRAPPER_LOADING_CLASS]: loading,
  });

/**
 * @param {object} props
 * @param {string} props.treatmentName
 * @param {boolean?} [props.showSpinner]
 * @param {boolean?} [props.hasConfig]
 * @param {boolean?} [props.splitReady]
 * @param {({ treatment: string, loading: boolean }) => JSX.Element} [props.render]
 * @param {React.ReactChild} [props.children]
 */
const Treatment = ({
  treatmentName,
  children,
  showSpinner,
  hasConfig,
  render,
  splitReady,
}) => {
  if (!treatmentName) {
    throw new Error(TREATMENT_REQUIRED);
  }

  if (render && children) {
    throw new Error(CHILDREN_AND_RENDER_ERROR);
  }

  const loadingRender = useMemo(
    () => (showSpinner ? <Spinner /> : null),
    [showSpinner]
  );

  const { loading, treatment } = useTreatment(treatmentName, {
    hasConfig,
    splitReady,
  });

  if (render) {
    return (
      <div className={getTreatmentClass(loading)}>
        {loading ? loadingRender : render({ loading, treatment })}
      </div>
    );
  }

  const childrenWithTreatment = React.Children.map(children, (child) => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child, { treatment });
    }
    return child;
  });

  return (
    <div className={getTreatmentClass(loading)}>
      {loading ? loadingRender : childrenWithTreatment}
    </div>
  );
};

Treatment.propTypes = {
  treatmentName: PropTypes.string.isRequired,
  showSpinner: PropTypes.bool,
  hasConfig: PropTypes.bool,
  children: PropTypes.node,
  render: PropTypes.func,
  splitReady: PropTypes.bool,
};

Treatment.defaultProps = {
  showSpinner: true,
  hasConfig: false,
  children: null,
  render: null,
  splitReady: true,
};

export default Treatment;
