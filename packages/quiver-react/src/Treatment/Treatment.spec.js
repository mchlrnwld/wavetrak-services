/* eslint-disable react/prop-types */

import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import common from '@surfline/web-common';
import Treatment, {
  CHILDREN_AND_RENDER_ERROR,
  TREATMENT_REQUIRED,
} from './Treatment';

describe('Treatment', () => {
  const Component = ({ treatment }) => (
    <div className={treatment}>{treatment}</div>
  );

  const ComponentWithConfig = ({ treatment }) => (
    <div className={treatment.treatment}>{treatment.treatment}</div>
  );

  /** @type {sinon.SinonStub} */
  let getTreatmentStub;
  /** @type {sinon.SinonStub} */
  let getTreatmentWithConfigStub;

  beforeEach(() => {
    getTreatmentStub = sinon.stub(common, 'getTreatment');
    getTreatmentWithConfigStub = sinon.stub(common, 'getTreatmentWithConfig');
  });

  afterEach(() => {
    getTreatmentStub.restore();
    getTreatmentWithConfigStub.restore();
  });

  it('should throw an error if the treatment name is missing', () => {
    expect(() => Treatment({})).to.throw(TREATMENT_REQUIRED);
  });

  it('should throw an error if both children and a render function are passed', () => {
    expect(() =>
      Treatment({ children: () => {}, render: () => {}, treatmentName: '1' })
    ).to.throw(CHILDREN_AND_RENDER_ERROR);
  });

  it('should render a loading spinner until treatment is loaded', () => {
    getTreatmentStub.returns(null);
    const wrapper = mount(
      <Treatment treatmentName="test-treatment">
        <Component />
      </Treatment>
    );
    expect(wrapper.find('Spinner')).to.have.length(1);
    expect(wrapper.find('.quiver-treatment-wrapper--loading')).to.have.length(
      1
    );
    expect(getTreatmentStub).to.have.been.calledOnceWithExactly(
      'test-treatment'
    );
  });

  it('should render a loading spinner until treatment is loaded - render prop', () => {
    getTreatmentStub.returns(null);
    const wrapper = mount(
      <Treatment
        treatmentName="test-treatment"
        render={({ loading, treatment }) => `${loading} - ${treatment}`}
      />
    );
    expect(wrapper.find('Spinner')).to.have.length(1);
    expect(wrapper.find('.quiver-treatment-wrapper--loading')).to.have.length(
      1
    );
    expect(wrapper.text()).to.be.equal('');
  });

  it('should not render a loading spinner if showSpinner is false', () => {
    getTreatmentStub.returns(null);
    const wrapper = mount(
      <Treatment treatmentName="test-treatment" showSpinner={false}>
        <Component />
      </Treatment>
    );
    expect(wrapper.find('Spinner')).to.have.length(0);
    expect(wrapper.find('.quiver-treatment-wrapper--loading')).to.have.length(
      1
    );
  });

  it('should render a child with a treatment once it is loaded', () => {
    getTreatmentStub.returns('on');
    const wrapper = mount(
      <Treatment treatmentName="test-treatment">
        <Component />
      </Treatment>
    );
    expect(wrapper.find('Spinner')).to.have.length(0);
    expect(wrapper.find('.quiver-treatment-wrapper--loading')).to.have.length(
      0
    );
    const component = wrapper.find(Component);
    expect(component).to.have.length(1);
    expect(component.prop('treatment')).to.equal('on');
  });

  it('should render a child with a treatment once split is ready', () => {
    getTreatmentStub.returns('on');
    const wrapper = mount(
      <Treatment treatmentName="test-treatment" splitReady={false}>
        <Component />
      </Treatment>
    );
    expect(wrapper.find('Spinner')).to.have.length(1);
    expect(wrapper.find('.quiver-treatment-wrapper--loading')).to.have.length(
      1
    );

    const component = wrapper.find(Component);
    expect(component).to.have.length(0);

    wrapper.setProps({ splitReady: true });
    wrapper.update();

    expect(wrapper.find('Spinner')).to.have.length(0);
    expect(wrapper.find('.quiver-treatment-wrapper--loading')).to.have.length(
      0
    );
    const component2 = wrapper.find(Component);
    expect(component2).to.have.length(1);
    expect(component2.prop('treatment')).to.equal('on');
  });

  it('should render multiple children with the treatment', () => {
    // eslint-disable-next-line react/prop-types
    const Component2 = ({ treatment }) => (
      <div className={treatment}>{treatment}</div>
    );
    getTreatmentStub.returns('on');
    const wrapper = mount(
      <Treatment treatmentName="test-treatment">
        <Component />
        <Component2 />
      </Treatment>
    );

    const component = wrapper.find(Component);
    expect(component).to.have.length(1);
    expect(component.prop('treatment')).to.equal('on');

    const component2 = wrapper.find(Component2);
    expect(component2).to.have.length(1);
    expect(component2.prop('treatment')).to.equal('on');
  });

  it('should render non-component children as well', () => {
    getTreatmentStub.returns('on');
    const wrapper = mount(
      <Treatment treatmentName="test-treatment">
        <Component />
        Some text
      </Treatment>
    );
    expect(wrapper.text()).to.have.contain('Some text');
  });

  it('should use a render prop', () => {
    getTreatmentStub.returns('on');
    const wrapper = mount(
      <Treatment
        treatmentName="test-treatment"
        render={({ loading, treatment }) => (
          <div className="render">{`${loading} - ${treatment}`}</div>
        )}
      />
    );
    const div = wrapper.find('.render');
    expect(div).to.have.length(1);
    expect(div.text()).to.equal('false - on');
  });

  it('should allow using treatments with configs', () => {
    getTreatmentWithConfigStub.returns({ treament: 'on', config: 'config' });
    mount(
      <Treatment treatmentName="test-treatment" hasConfig>
        <ComponentWithConfig />
      </Treatment>
    );

    expect(getTreatmentWithConfigStub).to.have.been.calledWithMatch(
      'test-treatment'
    );
  });
});
