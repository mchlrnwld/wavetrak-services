import React, { Component } from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';
import SelectArrows from './SelectArrows';

export default class Select extends Component {
  className = () => {
    const { success, warning, error, parentWidth } = this.props;
    return classNames({
      'quiver-select': true,
      'quiver-select--parent-width': parentWidth,
      'quiver-select--success': success,
      'quiver-select--warning': warning,
      'quiver-select--error': error,
    });
  };

  optionsList = (options) =>
    options.map((option) => (
      <option
        value={option.value}
        key={option.value}
        disabled={option.disabled}
      >
        {option.text}
      </option>
    ));

  render() {
    const { id, label, select, options, defaultValue, message, style } =
      this.props;

    return (
      <div className={this.className()} style={style}>
        <label htmlFor={id} style={style && style.label}>
          {label}
        </label>
        <select
          {...select}
          id={id}
          defaultValue={defaultValue}
          style={style && style.select}
        >
          {this.optionsList(options)}
        </select>
        <SelectArrows />
        {message ? (
          <div
            className="quiver-select__message"
            style={style && style.message}
          >
            {message}
          </div>
        ) : null}
      </div>
    );
  }
}

Select.propTypes = {
  id: PropTypes.string,
  defaultValue: PropTypes.string,
  label: PropTypes.string,
  message: PropTypes.string,
  error: PropTypes.bool,
  success: PropTypes.bool,
  warning: PropTypes.bool,
  parentWidth: PropTypes.bool,
  select: PropTypes.shape({}),
  options: PropTypes.arrayOf(PropTypes.shape({})),
  style: PropTypes.shape({
    message: PropTypes.shape({}),
    select: PropTypes.shape({}),
    label: PropTypes.shape({}),
  }),
};

Select.defaultProps = {
  id: '',
  defaultValue: '',
  label: '',
  message: '',
  error: false,
  success: false,
  warning: false,
  parentWidth: false,
  select: {},
  style: {},
  options: [],
};
