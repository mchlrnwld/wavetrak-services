import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Select from './Select';

describe('Select', () => {
  it('should initialize defaultValue if it exists', () => {
    const noDefaultWrapper = shallow(<Select />);
    const defaultWrapper = shallow(<Select defaultValue="Default Value" />);

    const noDefaultSelect = noDefaultWrapper.find('select');
    const defaultSelect = defaultWrapper.find('select');

    expect(noDefaultSelect.prop('defaultValue')).not.to.be.ok();
    expect(defaultSelect.prop('defaultValue')).to.equal('Default Value');
  });

  it('should render options list', () => {
    const options = [
      { value: 1, text: 'one' },
      { value: 2, text: 'two' },
      { value: 3, text: 'three' },
    ];
    const wrapper = shallow(<Select options={options} />);
    const optionWrappers = wrapper.find('option');

    expect(optionWrappers).to.have.length(3);
    expect(optionWrappers.at(0).prop('value')).to.equal(1);
    expect(optionWrappers.at(0).key()).to.equal('1');
    expect(optionWrappers.at(0).text()).to.equal('one');
    expect(optionWrappers.at(1).prop('value')).to.equal(2);
    expect(optionWrappers.at(1).key()).to.equal('2');
    expect(optionWrappers.at(1).text()).to.equal('two');
    expect(optionWrappers.at(2).prop('value')).to.equal(3);
    expect(optionWrappers.at(2).key()).to.equal('3');
    expect(optionWrappers.at(2).text()).to.equal('three');
  });

  it('should show helper text with correct text when message prop set', () => {
    const defaultWrapper = shallow(<Select />);
    const messageWrapper = shallow(<Select message="Helper Text" />);
    const defaultSpanWrapper = defaultWrapper.find('.quiver-select__message');
    const messageSpanWrapper = messageWrapper.find('.quiver-select__message');

    expect(defaultSpanWrapper.exists()).to.be.false();
    expect(messageSpanWrapper.exists()).to.be.true();
    expect(messageSpanWrapper.text()).to.equal('Helper Text');
  });

  it('should pass select props to select component', () => {
    const select = {
      name: 'Test Name',
      onChange: () => true,
    };
    const wrapper = shallow(<Select select={select} />);
    const selectWrapper = wrapper.find('select');

    expect(selectWrapper.prop('name')).to.equal('Test Name');
    expect(selectWrapper.prop('onChange')).to.equal(select.onChange);
  });

  it('should pass id into select and label', () => {
    const wrapper = shallow(<Select id="testid" />);
    const labelWrapper = wrapper.find('label');
    const selectWrapper = wrapper.find('select');

    expect(labelWrapper.prop('htmlFor')).to.equal('testid');
    expect(selectWrapper.prop('id')).to.equal('testid');
  });

  it('should display the label', () => {
    const wrapper = shallow(<Select label="Select Label" />);
    const labelWrapper = wrapper.find('label');
    expect(labelWrapper.text()).to.equal('Select Label');
  });

  it('should attach quiver-select class name', () => {
    const wrapper = shallow(<Select />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal('quiver-select');
  });

  it('should attach parent-width modifier class name if parentWidth set', () => {
    const wrapper = shallow(<Select parentWidth />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-select quiver-select--parent-width'
    );
  });

  it('should attach success modifier class name if success set', () => {
    const wrapper = shallow(<Select success />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-select quiver-select--success'
    );
  });

  it('should attach warning modifier class name if warning set', () => {
    const wrapper = shallow(<Select warning />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-select quiver-select--warning'
    );
  });

  it('should attach error modifier class name if error set', () => {
    const wrapper = shallow(<Select error />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-select quiver-select--error'
    );
  });

  it('should pass custom style object into children', () => {
    const style = {
      fontFamily: 'container',
      label: {
        fontFamily: 'label',
      },
      select: {
        fontFamily: 'select',
      },
    };
    const wrapper = shallow(<Select style={style} />);
    const containerWrapper = wrapper.find('div');
    const labelWrapper = containerWrapper.find('label');
    const selectWrapper = containerWrapper.find('select');

    expect(containerWrapper.prop('style')).to.equal(style);
    expect(labelWrapper.prop('style')).to.equal(style.label);
    expect(selectWrapper.prop('style')).to.equal(style.select);
  });
});
