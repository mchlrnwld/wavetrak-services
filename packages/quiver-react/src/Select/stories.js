import React from 'react';
import { action } from '@storybook/addon-actions';
import Select from './index';

export default {
  title: 'Select',
};

export const Default = () => (
  <Select
    id="default"
    label="Default Select"
    defaultValue="2"
    options={[
      { text: 'Option 1', value: 1 },
      { text: 'Option 2', value: 2 },
    ]}
  />
);

Default.story = {
  name: 'default',
};

export const DefaultWithHelperText = () => (
  <Select
    id="helper"
    label="Default with Helper Text"
    defaultValue="1"
    message="Helper Text"
    options={[
      { text: 'Option 1', value: 1 },
      { text: 'Option 2', value: 2 },
    ]}
  />
);

DefaultWithHelperText.story = {
  name: 'default with helper text',
};

export const DefaultParentWidth = () => (
  <Select
    id="default"
    label="Default Select"
    defaultValue="2"
    parentWidth
    options={[
      { text: 'Option 1', value: 1 },
      { text: 'Option 2', value: 2 },
    ]}
  />
);

DefaultParentWidth.story = {
  name: 'default parent width',
};

export const Success = () => (
  <Select
    id="success"
    label="Success Select"
    defaultValue="1"
    options={[
      { text: 'Option 1', value: 1 },
      { text: 'Option 2', value: 2 },
    ]}
    success
  />
);

Success.story = {
  name: 'success',
};

export const Warning = () => (
  <Select
    id="warning"
    label="Warning Select"
    defaultValue="1"
    options={[
      { text: 'Option 1', value: 1 },
      { text: 'Option 2', value: 2 },
    ]}
    warning
  />
);

Warning.story = {
  name: 'warning',
};

export const Error = () => (
  <Select
    id="error"
    label="Error Select"
    defaultValue="1"
    message="Error Message"
    options={[
      { text: 'Option 1', value: 1 },
      { text: 'Option 2', value: 2 },
    ]}
    error
  />
);

Error.story = {
  name: 'error',
};

export const CustomStyles = () => (
  <Select
    id="customstyles"
    label="Custom Styles"
    defaultVaue="1"
    options={[
      { text: 'Option 1', value: 1 },
      { text: 'Option 2', value: 2 },
    ]}
    style={{
      margin: '20px 50px',
      label: {
        fontStyle: 'italic',
      },
      select: {
        border: '1px dotted black',
      },
    }}
  />
);

CustomStyles.story = {
  name: 'custom styles',
};

export const FocusAndBlurEvents = () => (
  <Select
    id="focusblur"
    label="Focus & Blur Select"
    defaultValue="1"
    options={[
      { text: 'Option 1', value: 1 },
      { text: 'Option 2', value: 2 },
    ]}
    select={{
      onFocus: action('focused'),
      onBlur: action('blurred'),
    }}
  />
);

FocusAndBlurEvents.story = {
  name: 'focus and blur events',
};
