import React, { Component } from 'react';
import PropTypes from 'prop-types';

import notificationPropType from './notificationPropType';
import notificationPropLevel from './notificationPropLevel';
import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

export default class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      closed: false,
    };
    this.handleCloseClick = this.handleCloseClick.bind(this);
  }

  handleCloseClick = (e) => {
    const { onCloseComplete } = this.props;
    e.preventDefault();
    this.setState({ closed: true });
    if (onCloseComplete) {
      onCloseComplete();
    }
  };

  className = (level, type) =>
    `quiver-notification quiver-notification--${level} quiver-notification--${type}`;

  render() {
    const { children, closable, type, level } = this.props;
    const { closed } = this.state;

    if (closed) {
      return null;
    }
    return (
      <div className={this.className(level, type)}>
        {children}
        {closable ? (
          // eslint-disable-next-line jsx-a11y/anchor-is-valid
          <a
            className="quiver-notification__closable"
            {...createAccessibleOnClick(this.handleCloseClick, BTN)}
          >
            ✕
          </a>
        ) : null}
      </div>
    );
  }
}

Notification.propTypes = {
  children: PropTypes.node.isRequired,
  closable: PropTypes.bool,
  type: notificationPropType,
  level: notificationPropLevel,
  onCloseComplete: PropTypes.func,
};

Notification.defaultProps = {
  closable: false,
  level: 'information',
  type: 'small',
  onCloseComplete: null,
};
