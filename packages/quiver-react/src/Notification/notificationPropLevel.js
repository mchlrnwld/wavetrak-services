import PropTypes from 'prop-types';

const notificationPropLevel = PropTypes.oneOf(['error', 'information']);

export default notificationPropLevel;
