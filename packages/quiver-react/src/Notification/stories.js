import React from 'react';
import Notification from './index';

export default {
  title: 'Notification',
};

export const Warning = () => (
  <Notification type="warning-notification" level="error">
    <span>{`Please upate your payment details.  `}</span>
    <a href="/account/subscription#payment-details">
      Make sure you keep Surfline Premium
    </a>
  </Notification>
);

Warning.story = {
  name: 'warning',
};
