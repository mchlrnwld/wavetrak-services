import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import Notification from './Notification';

describe('Notification', () => {
  const props = {
    level: 'information',
    type: 'small',
  };

  it('should render children', () => {
    const wrapper = shallow(
      <Notification {...props}>
        <div>Test</div>
      </Notification>
    );
    expect(wrapper.children().html()).to.equal('<div>Test</div>');
  });

  it('should render class name with notification level and type', () => {
    const wrapper = shallow(
      <Notification {...props}>
        <div>Test</div>
      </Notification>
    );
    const containerWrapper = wrapper.find('.quiver-notification');
    expect(containerWrapper).to.have.length(1);
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-notification quiver-notification--information quiver-notification--small'
    );
  });

  it('should be closable', () => {
    const onCloseComplete = sinon.spy();
    const preventDefault = sinon.spy();
    const wrapper = shallow(
      <Notification {...props} onCloseComplete={onCloseComplete} closable>
        <div>Test</div>
      </Notification>
    );
    expect(wrapper.state('closed')).to.be.false();
    const closableWrapper = wrapper.find('.quiver-notification__closable');
    expect(closableWrapper).to.have.length(1);

    closableWrapper.simulate('click', { preventDefault });
    expect(wrapper.state('closed')).to.be.true();
    expect(wrapper.find('.quiver-notification')).to.have.length(0);
    expect(onCloseComplete).to.have.been.calledOnce();
    expect(preventDefault).to.have.been.calledOnce();
  });
});
