import PropTypes from 'prop-types';

const notificationPropType = PropTypes.oneOf(['small', 'large']);

export default notificationPropType;
