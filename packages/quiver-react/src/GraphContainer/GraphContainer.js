import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { TABLET_LARGE_WIDTH } from '@surfline/web-common';
import GraphTime from '../GraphTime';
import { useActiveColumn } from '../hooks/useActiveColumn';
import { useDeviceSize } from '../hooks/useDeviceSize';
import GraphColumn from '../GraphColumn';

/**
 * @param {boolean} showTimeLabels
 */
const className = (showTimeLabels) =>
  classNames({
    'quiver-graph-container': true,
    'quiver-graph-container--time-labels': showTimeLabels,
  });

/**
 *
 * @param {object} Props
 * @param {boolean} Props.highlightActiveColumn
 * @param {boolean} Props.isHourly
 * @param {React.ReactChild} Props.children
 * @param {number} Props.dawnXPercentage
 * @param {number} Props.duskXPercentage
 * @param {boolean} Props.showTimeLabels
 * @param {(0 | 1 | 2)[]} Props.optimalScores
 * @param {{timestamp: number, utcOffset: number}[]} Props.timestamps
 * @param {boolean} Props.allowMobileTooltip
 * @param {number} Props.currentMarkerXPercentage
 * @param {number} Props.day
 * @returns
 */
const GraphContainer = ({
  highlightActiveColumn,
  isHourly,
  children,
  dawnXPercentage,
  duskXPercentage,
  showTimeLabels,
  optimalScores,
  timestamps,
  allowMobileTooltip,
  currentMarkerXPercentage,
  day,
}) => {
  const device = useDeviceSize();

  const isMobile = device.width < TABLET_LARGE_WIDTH;
  const showMobileTooltip = isMobile && allowMobileTooltip;

  const totalColumnNum = timestamps.length;

  const hasFirstDay = day === 0;
  const currentColumn = hasFirstDay
    ? Math.floor(totalColumnNum * currentMarkerXPercentage)
    : null;

  const {
    activeColumn,
    handleMouseMove,
    handleTouch,
    removeActiveColumn,
    ref,
  } = useActiveColumn({
    highlightActiveColumn,
    showMobileTooltip,
    totalColumnNum,
    isHourly,
    currentColumn,
  });

  const dawnColumn = Math.floor(dawnXPercentage * totalColumnNum);
  const dawnColumnPercentage = dawnXPercentage * totalColumnNum - dawnColumn;
  const duskColumn = Math.floor(duskXPercentage * totalColumnNum);
  const duskColumnPercentage = duskXPercentage * totalColumnNum - duskColumn;

  return (
    <div
      ref={ref}
      className={className(showTimeLabels)}
      onMouseMove={handleMouseMove}
      onMouseLeave={removeActiveColumn}
      onTouchStart={handleTouch}
      onTouchMove={handleTouch}
      onTouchEnd={removeActiveColumn}
    >
      {showTimeLabels ? (
        <GraphTime timestamps={timestamps} showTimeLabels={showTimeLabels} />
      ) : null}
      <div className="quiver-graph-container__columns">
        {timestamps.map(({ timestamp }, columnNum) => {
          const optimalScore = optimalScores ? optimalScores[columnNum] : null;
          return (
            <GraphColumn
              key={timestamp}
              activeColumn={activeColumn}
              columnNum={columnNum}
              currentColumn={allowMobileTooltip && currentColumn}
              optimalScore={optimalScore}
              dawnColumn={dawnColumn}
              dawnColumnPercentage={dawnColumnPercentage}
              duskColumn={duskColumn}
              duskColumnPercentage={duskColumnPercentage}
            />
          );
        })}
      </div>
      <div className="quiver-graph-container__canvas">{children}</div>
    </div>
  );
};

GraphContainer.propTypes = {
  children: PropTypes.node.isRequired,
  dawnXPercentage: PropTypes.number,
  duskXPercentage: PropTypes.number,
  showTimeLabels: PropTypes.bool,
  highlightActiveColumn: PropTypes.bool,
  optimalScores: PropTypes.oneOf([0, 1, 2]),
  isHourly: PropTypes.bool,
  day: PropTypes.number.isRequired,
  timestamps: PropTypes.arrayOf(
    PropTypes.shape({
      timestamp: PropTypes.number,
      utcOffset: PropTypes.number,
    })
  ).isRequired,
  allowMobileTooltip: PropTypes.bool,
  currentMarkerXPercentage: PropTypes.number,
};

GraphContainer.defaultProps = {
  dawnXPercentage: 0,
  duskXPercentage: 1,
  showTimeLabels: false,
  highlightActiveColumn: false,
  optimalScores: null,
  isHourly: false,
  currentMarkerXPercentage: 0,
  allowMobileTooltip: false,
};
export default GraphContainer;
