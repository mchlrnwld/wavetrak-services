import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { afterEach, beforeEach } from 'mocha';
import GraphContainer from './GraphContainer';
import * as hook from '../hooks/useActiveColumn';
import * as deviceHook from '../hooks/useDeviceSize';

describe('components / GraphContainer', () => {
  const secondsInAnHour = 3600;
  // 2021-01-01 08:00 GMT
  // 2020-12-31 23:00 PDT
  // 2021-01-01 00:00 PST
  const baseTimestamp = 1577865600;

  const handleMouseMove = sinon.stub();
  const handleTouch = sinon.stub();
  const removeActiveColumn = sinon.stub();

  const getUseActiveColumnReturn = (activeColumn) => ({
    activeColumn,
    handleMouseMove,
    handleTouch,
    removeActiveColumn,
  });

  const createTimestamps = (columns, intervalHours = 1) => {
    const data = [...Array(columns)].map((_, pointNum) => ({
      timestamp: baseTimestamp + pointNum * intervalHours * secondsInAnHour,
      // Simulate time shift off of DST after the first midnight point (12am, 1am, 3am)
      utcOffset: pointNum === 0 ? -8 : -7,
    }));
    return data.flat();
  };

  const createOptimalScores = (columns) =>
    [...Array(columns)].map((_, index) => index % 3);

  /** @type {sinon.SinonStub} */
  let useActiveColumnStub;
  let useDeviceSizeStub;

  beforeEach(() => {
    useActiveColumnStub = sinon
      .stub(hook, 'useActiveColumn')
      .returns(getUseActiveColumnReturn());
    useDeviceSizeStub = sinon
      .stub(deviceHook, 'useDeviceSize')
      .returns({ width: 1900 });
  });

  afterEach(() => {
    useActiveColumnStub.restore();
    useDeviceSizeStub.restore();
  });

  it('should render the graph container', () => {
    useActiveColumnStub.returns(getUseActiveColumnReturn());
    const wrapper = shallow(
      <GraphContainer timestamps={createTimestamps(24)}>
        <div className="graph-container__children" />
      </GraphContainer>
    );
    const container = wrapper.find('.quiver-graph-container');
    expect(container).to.have.length(1);
    expect(container.prop('onMouseMove')).to.deep.equal(handleMouseMove);
    expect(container.prop('onMouseLeave')).to.equal(removeActiveColumn);
    expect(container.prop('onTouchStart')).to.equal(handleTouch);
    expect(container.prop('onTouchMove')).to.equal(handleTouch);
    expect(container.prop('onTouchEnd')).to.equal(removeActiveColumn);

    const columns = wrapper.find('.quiver-graph-container__columns');
    expect(columns).to.have.length(1);
    expect(columns.find('GraphColumn')).to.have.length(24);
  });

  it('should pass showMobileTooltip to useActiveColumn', () => {
    useDeviceSizeStub.returns({ width: 475 });
    shallow(
      <GraphContainer
        day={1}
        timestamps={createTimestamps(24)}
        allowMobileTooltip
      >
        <div className="graph-container__children" />
      </GraphContainer>
    );

    expect(useActiveColumnStub).to.have.been.calledOnceWithExactly({
      highlightActiveColumn: false,
      showMobileTooltip: true,
      totalColumnNum: 24,
      isHourly: false,
      currentColumn: null,
    });
  });

  it('should pass highlightActiveColum to useActiveColumn hook', () => {
    useActiveColumnStub.returns(getUseActiveColumnReturn());
    shallow(
      <GraphContainer
        day={1}
        timestamps={createTimestamps(24)}
        highlightActiveColumn
      >
        <div className="graph-container__children" />
      </GraphContainer>
    );

    expect(useActiveColumnStub).to.have.been.calledOnceWithExactly({
      highlightActiveColumn: true,
      showMobileTooltip: false,
      totalColumnNum: 24,
      isHourly: false,
      currentColumn: null,
    });
  });

  it('should pass the active column to GraphColumn', () => {
    useActiveColumnStub.returns(getUseActiveColumnReturn(1));
    const wrapper = shallow(
      <GraphContainer
        day={1}
        timestamps={createTimestamps(24)}
        highlightActiveColumn
      >
        <div className="graph-container__children" />
      </GraphContainer>
    );

    const columns = wrapper.find('GraphColumn');
    expect(columns.at(0).prop('activeColumn')).to.be.equal(1);
  });

  it('should render the GraphTimestamps with showTimeLabels', () => {
    useActiveColumnStub.returns(getUseActiveColumnReturn());
    const timestamps = createTimestamps(24);
    const wrapper = shallow(
      <GraphContainer day={1} timestamps={timestamps} showTimeLabels>
        <div className="graph-container__children" />
      </GraphContainer>
    );

    const columns = wrapper.find('GraphTime');
    expect(columns.at(0).props()).to.deep.include({
      timestamps,
      showTimeLabels: true,
    });
  });

  it('should pass optimalScores to GraphColumn', () => {
    const wrapper = shallow(
      <GraphContainer
        day={1}
        timestamps={createTimestamps(24)}
        optimalScores={createOptimalScores(24)}
      >
        <div className="graph-container__children" />
      </GraphContainer>
    );

    const columns = wrapper.find('GraphColumn');
    for (let i = 0; i <= 23; i += 1) {
      expect(columns.at(i).prop('optimalScore')).to.be.equal(i % 3);
    }
  });

  it('should pass the current column to useActiveColumn', () => {
    shallow(
      <GraphContainer
        day={0}
        currentMarkerXPercentage={0.5}
        timestamps={createTimestamps(24)}
        optimalScores={createOptimalScores(24)}
      >
        <div className="graph-container__children" />
      </GraphContainer>
    );

    expect(useActiveColumnStub).to.have.been.calledOnceWithExactly({
      highlightActiveColumn: false,
      showMobileTooltip: false,
      totalColumnNum: 24,
      isHourly: false,
      currentColumn: 12,
    });
  });

  it('should not pass the current column to GraphColumn if allowMobileTooltip is false', () => {
    const wrapper = shallow(
      <GraphContainer
        day={0}
        currentMarkerXPercentage={0.5}
        timestamps={createTimestamps(24)}
      >
        <div className="graph-container__children" />
      </GraphContainer>
    );

    const columns = wrapper.find('GraphColumn');
    for (let i = 0; i <= 23; i += 1) {
      expect(columns.at(i).prop('currentColumn')).to.be.equal(false);
    }
  });

  it('should calculate the dawn/dusk columns and percentages', () => {
    const wrapper = shallow(
      <GraphContainer
        dawnXPercentage={0.25}
        duskXPercentage={0.75}
        timestamps={createTimestamps(24)}
        optimalScores={createOptimalScores(24)}
      >
        <div className="graph-container__children" />
      </GraphContainer>
    );

    const columns = wrapper.find('GraphColumn');
    for (let i = 0; i <= 23; i += 1) {
      expect(columns.at(i).prop('dawnColumn')).to.be.equal(6);
      expect(columns.at(i).prop('dawnColumnPercentage')).to.be.equal(0);
      expect(columns.at(i).prop('duskColumn')).to.be.equal(18);
      expect(columns.at(i).prop('duskColumnPercentage')).to.be.equal(0);
    }
  });
});
