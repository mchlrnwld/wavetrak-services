import PropTypes from 'prop-types';
import React from 'react';

const viewBox = (direction) => {
  switch (direction) {
    case 'left':
    case 'right':
      return '0 0 7 13';
    case 'up':
    case 'down':
    default:
      return '0 0 13 7';
  }
};

const points = (direction) => {
  switch (direction) {
    case 'left':
      return '6 1, 1 6.5, 6 12';
    case 'right':
      return '1 1, 6 6.5, 1 12';
    case 'up':
      return '1 6, 6.5 1, 12 6';
    case 'down':
    default:
      return '1 1, 6.5 6, 12 1';
  }
};

const Chevron = ({ direction }) => (
  <div className={`quiver-chevron quiver-chevron--${direction}`}>
    <svg viewBox={viewBox(direction)} preserveAspectRatio="xMinYMin meet">
      <polyline points={points(direction)} fill="none" strokeLinejoin="round" />
    </svg>
  </div>
);

Chevron.propTypes = {
  direction: PropTypes.string,
};

Chevron.defaultProps = {
  direction: 'down',
};

export default Chevron;
