import React from 'react';
import ErrorBox from './ErrorBox';

export default {
  title: 'ErrorBox',
};

export const Default = () => <ErrorBox type="Swell" />;

Default.story = {
  name: 'default',
};
