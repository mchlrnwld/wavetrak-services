import PropTypes from 'prop-types';
import classNames from 'classnames';
import React from 'react';

const getClass = (rootClass) =>
  classNames({
    'quiver-error-box': true,
    [rootClass]: !!rootClass,
  });

/**
 * @param {object} props
 * @param {string?} props.message
 * @param {string?} props.type
 * @param {{root?: string }?} props.classes
 */
const ErrorBox = ({ message, type, classes }) => (
  <div className={getClass(classes.root)}>
    {message || `Error loading ${type}`}
  </div>
);

ErrorBox.propTypes = {
  type: PropTypes.string,
  message: PropTypes.string,
  classes: PropTypes.shape({
    root: PropTypes.string,
  }),
};

ErrorBox.defaultProps = {
  message: null,
  type: 'data',
  classes: {},
};

export default ErrorBox;
