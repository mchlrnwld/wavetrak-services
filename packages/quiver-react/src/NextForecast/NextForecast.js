import PropTypes from 'prop-types';
import React from 'react';

const NextForecast = ({ nextUpdate }) => (
  <div className="quiver-next-forecast">{nextUpdate}</div>
);

NextForecast.propTypes = {
  nextUpdate: PropTypes.string.isRequired,
};

export default NextForecast;
