import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { clearWavetrakIdentity } from '@surfline/web-common';
import BaseMenu from '../BaseMenu';
import BaseMenuSection from '../BaseMenuSection';
import BaseMenuItem from '../BaseMenuItem';

import {
  accountLinks,
  promotionalLinks,
  productLinks,
} from '../NewNavigationBar/defaultLinks';

const homeLink = {
  display: 'Home',
  href: '/',
};

class UserMenu extends Component {
  handleSignOut = async () => {
    /**
     * resets Segment session
     * https://segment.com/docs/sources/website/analytics.js/#reset-logout
     */
    window?.analytics?.reset();
    /**
     * resets Braze session (v3 api)
     * https://js.appboycdn.com/web-sdk/latest/doc/modules/appboy.html#wipedata
     */
    window?.appboy?.wipeData();
    clearWavetrakIdentity();
    setTimeout(() => window.location.assign('/logout'), 300);
  };

  renderNotification() {
    const { user, entitlements, accountWarning } = this.props;
    const { isEmailVerified } = user;
    if (accountWarning) {
      return (
        <a
          className="quiver-user-menu__notification quiver-user-menu__notification__warning"
          href="/account/subscription"
        >
          Update Payment Details
        </a>
      );
    }
    if (entitlements.includes('sl_premium')) {
      return <div className="quiver-user-menu__premium-status">Premium</div>;
    }
    if (user && !isEmailVerified) {
      return (
        <a
          className="quiver-user-menu__notification"
          href="/account/edit-profile"
        >
          Verify Your Email
        </a>
      );
    }
    return null;
  }

  render() {
    const { user, showHome, showDetails, onClickLink, freeTrialEligible } =
      this.props;
    return (
      <BaseMenu position="right">
        {showHome ? (
          <BaseMenuSection links={[homeLink]} onClick={onClickLink} />
        ) : null}
        {user && showDetails ? (
          <BaseMenuSection>
            <div className="quiver-user-menu__user-name">
              <span>{user.firstName}</span> <span>{user.lastName}</span>
            </div>
            <div className="quiver-user-menu__user-email">{user.email}</div>
            {this.renderNotification()}
          </BaseMenuSection>
        ) : null}
        {freeTrialEligible ? (
          <BaseMenuSection>
            <BaseMenuItem href="/upgrade" onClick={onClickLink}>
              <div className="quiver-user-menu__cta">
                Try Premium Free for 15 days
              </div>
            </BaseMenuItem>
          </BaseMenuSection>
        ) : null}
        {user ? (
          <>
            <BaseMenuSection links={accountLinks} onClick={onClickLink} />
            <BaseMenuSection links={promotionalLinks} onClick={onClickLink} />
            <BaseMenuSection links={productLinks} onClick={onClickLink} />
            <BaseMenuSection>
              <BaseMenuItem onClick={this.handleSignOut}>Sign Out</BaseMenuItem>
            </BaseMenuSection>
          </>
        ) : null}
      </BaseMenu>
    );
  }
}

UserMenu.propTypes = {
  user: PropTypes.shape({
    email: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    isEmailVerified: PropTypes.bool,
  }),
  entitlements: PropTypes.arrayOf(PropTypes.string),
  serviceConfig: PropTypes.shape({
    serviceUrl: PropTypes.string,
  }),
  showHome: PropTypes.bool,
  showDetails: PropTypes.bool,
  onClickLink: PropTypes.func,
  freeTrialEligible: PropTypes.bool,
  accountWarning: PropTypes.bool,
};

UserMenu.defaultProps = {
  user: null,
  entitlements: [],
  onClickLink: null,
  freeTrialEligible: false,
  accountWarning: false,
  serviceConfig: {
    serviceUrl: '',
  },
  showHome: false,
  showDetails: false,
};

export default UserMenu;
