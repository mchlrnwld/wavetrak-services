import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import UserMenu from './UserMenu';

describe('UserMenu', () => {
  const links = [{ display: 'Account', href: 'www.surfline.com/account' }];
  const user = {
    firstName: 'A',
    lastName: 'User',
    email: 'auser@surfline.com',
  };

  beforeEach(() => {
    window.analytics.reset.resetHistory();
  });

  it('should render a list of links when passed', () => {
    const wrapper = mount(<UserMenu user={user} links={links} />);
    expect(wrapper.html()).to.contain('/account');
  });

  it('should not render a list of links when passed', () => {
    const wrapper = mount(<UserMenu links={null} />);
    expect(wrapper.html()).to.not.contain('/account"');
  });

  it('should render user name and email if specified', () => {
    const wrapper = mount(<UserMenu links={links} user={user} showDetails />);
    expect(wrapper.html()).to.contain('<span>A</span>');
    expect(wrapper.html()).to.contain('<span>User</span>');
    expect(wrapper.html()).to.contain('auser@surfline.com');
  });

  it('should not render user name and email if not specified', () => {
    const wrapper = mount(<UserMenu links={links} user={user} />);
    expect(wrapper.html()).not.to.contain('<span>A</span>');
    expect(wrapper.html()).not.to.contain('<span>User</span>');
    expect(wrapper.html()).not.to.contain('auser@surfline.com');
  });

  it('should render a link to the homepage if specified', () => {
    const wrapper = mount(<UserMenu showHome />);
    expect(wrapper.html()).to.contain('href="/"');
  });

  it('should not render a link to the homepage if not specified', () => {
    const wrapper = mount(<UserMenu />);
    expect(wrapper.html()).not.to.contain('href="/"');
  });
});
