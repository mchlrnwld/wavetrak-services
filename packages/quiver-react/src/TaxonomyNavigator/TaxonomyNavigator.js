import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Breadcrumb from './Breadcrumb';
import Header from './Header';
import ChildList from './ChildList';
import fetchTaxonomy from '../helpers/fetchTaxonomy';
import fetchSpotReportViews from '../helpers/fetchSpotReportViews';
import { getListType, groupItems, alphabetizeItems } from './utils';
import navigationSettingsPropType from '../PropTypes/navigationSettingsPropType';
import serviceConfigurationPropType from '../PropTypes/serviceConfigurationPropType';
import taxonomyStatePropTypes from '../PropTypes/taxonomyStatePropType';
import { loadSavedLocation, saveLocation } from './utils/locationStore';
import { EARTH_TAXONOMY_ID } from './constants';

class TaxonomyNavigator extends Component {
  componentDidMount = () => {
    const { navigationSettings, savedLocationType, menuState } = this.props;
    const activeNodeId = menuState.taxonomy.activeNode._id;
    if (!activeNodeId) {
      const savedLocation = loadSavedLocation(
        navigationSettings,
        savedLocationType
      );
      this.setActiveNodeTaxonomy(
        savedLocation || EARTH_TAXONOMY_ID,
        savedLocation ? 1 : 0
      );
    }
  };

  setActiveNodeTaxonomy = async (taxonomyId, maxDepth) => {
    const {
      taxonomyType,
      hierarchy,
      setMenuState,
      serviceConfig,
      sevenRatings,
    } = this.props;
    try {
      setMenuState({ loading: true, error: false });
      const taxonomy = await fetchTaxonomy(taxonomyId, maxDepth, serviceConfig);
      const inLength = hierarchy.length - 1;
      const contains = taxonomy.contains.filter(
        (item) =>
          (item.type === 'geoname' || item.type === taxonomyType) &&
          (item.type === 'spot' ||
            item.hasSpots ||
            (taxonomyType === 'subregion' && item.hasSpots)) &&
          (taxonomy.in.length >= inLength || item.depth === 0)
      );

      const listType = getListType(contains, taxonomyType);

      let children;
      if (listType === 'spots') {
        const spotIds = contains
          .filter((item) => item.type === 'spot')
          .map((item) => item.spot);
        const { data } = await fetchSpotReportViews(
          spotIds,
          serviceConfig,
          sevenRatings
        );
        children = groupItems(contains, data);
      } else if (listType === 'subregions') {
        const subregions = contains.filter((item) => item.type === 'subregion');
        children = alphabetizeItems(subregions);
      } else {
        children = alphabetizeItems(contains);
      }
      setMenuState({
        taxonomyId,
        loading: false,
        error: false,
        taxonomy: {
          listType,
          parents: taxonomy.in.filter((item) => item.type === 'geoname'),
          activeNode: {
            name: taxonomy.name,
            _id: taxonomy._id,
            type: taxonomy.type,
            taxonomyId: taxonomy.taxonomyId,
            links: taxonomy.associated.links,
          },
          children,
        },
      });
    } catch (e) {
      setMenuState({ error: true });
    }
  };

  updateActiveNodeTaxonomy = async (taxonomyId) => {
    const {
      loggedIn,
      navigationSettings,
      savedLocationType,
      serviceConfig,
      hierarchy,
      scrollMenuToTop,
    } = this.props;
    // No need to await saveLocation. If it errors we can ignore it and try again next time.
    saveLocation(
      taxonomyId,
      savedLocationType,
      loggedIn,
      navigationSettings,
      serviceConfig
    );
    const inLength = hierarchy.length - 1;
    const depth = this.taxonomyDepth(taxonomyId) < inLength ? 0 : 1;
    await this.setActiveNodeTaxonomy(taxonomyId, depth);
    scrollMenuToTop();
  };

  taxonomyDepth = (taxonomyId) => {
    const { menuState } = this.props;
    const {
      taxonomy: { parents, activeNode },
    } = menuState;
    if (taxonomyId === activeNode._id) return parents.length;

    const parentsIndex = parents.findIndex(({ _id }) => _id === taxonomyId);
    if (parentsIndex > -1) return parentsIndex;
    return parents.length + 1;
  };

  render() {
    const { menuState, sevenRatings } = this.props;
    const { loading, error, taxonomy } = menuState;
    const { parents, activeNode, children, listType } = taxonomy;
    const { hierarchy, onClickLink } = this.props;

    return (
      <div className="quiver-taxonomy-navigator">
        <Breadcrumb
          nodes={[...parents, activeNode]}
          nodesPlaceholder={hierarchy}
          listType={listType}
          onClickNode={this.updateActiveNodeTaxonomy}
          onClickLink={(properties, closeMobileMenu) =>
            onClickLink(
              {
                ...properties,
                locationCategory: 'Taxonomy',
                currentTaxonomyLevel: parents.length,
              },
              closeMobileMenu
            )
          }
        />
        <Header
          listType={listType}
          activeNode={activeNode}
          label={hierarchy[parents.length + 1]}
          onClickChange={() => this.updateActiveNodeTaxonomy(EARTH_TAXONOMY_ID)}
          onClickLink={(properties, closeMobileMenu) =>
            onClickLink(
              {
                ...properties,
                locationCategory: 'Taxonomy',
                currentTaxonomyLevel: parents.length,
              },
              closeMobileMenu
            )
          }
        />
        <ChildList
          listType={listType}
          nodes={children}
          loading={loading}
          error={error}
          onClickNode={this.updateActiveNodeTaxonomy}
          onClickLink={(properties, closeMobileMenu) =>
            onClickLink(
              {
                ...properties,
                locationCategory: 'Taxonomy',
                currentTaxonomyLevel: parents.length,
              },
              closeMobileMenu
            )
          }
          sevenRatings={sevenRatings}
        />
      </div>
    );
  }
}

TaxonomyNavigator.propTypes = {
  hierarchy: PropTypes.arrayOf(PropTypes.string).isRequired,
  savedLocationType: PropTypes.string.isRequired,
  loggedIn: PropTypes.bool,
  navigationSettings: navigationSettingsPropType.isRequired,
  serviceConfig: serviceConfigurationPropType.isRequired,
  setMenuState: PropTypes.func.isRequired,
  menuState: taxonomyStatePropTypes.isRequired,
  taxonomyType: PropTypes.string,
  scrollMenuToTop: PropTypes.func,
  onClickLink: PropTypes.func,
  sevenRatings: PropTypes.bool,
};

TaxonomyNavigator.defaultProps = {
  loggedIn: false,
  taxonomyType: 'spot',
  onClickLink: null,
  scrollMenuToTop: null,
  sevenRatings: false,
};

export default TaxonomyNavigator;
