import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import RenderSpot from '../RenderSpot';
import RenderSubregion from '../RenderSubregion';
import RenderGeoname from './RenderGeoname';
import RenderCity from './RenderCity';
import taxonomyNodePropType from '../PropTypes/taxonomyNodePropType';

const flattenNodes = (groups) => {
  let result = [];
  groups.forEach((group) => {
    if (group.spots && group.spots.length === 0) return;
    result.push(group.city);
    const spots = [
      ...group.spots.slice(0, group.spots.length - 1),
      { ...group.spots[group.spots.length - 1], lastOfGroup: true },
    ];
    result = result.concat(spots);
  });
  return result.filter((node) => node !== null);
};

const childListClassNames = (listType) =>
  classNames({
    'quiver-taxonomy-navigator__child-list-spot': listType === 'spots',
    'quiver-taxonomy-navigator__child-list': listType !== 'spots',
  });

const ChildList = ({
  listType,
  nodes,
  onClickNode,
  loading,
  error,
  onClickLink,
  sevenRatings,
}) => (
  <div className={childListClassNames(listType)}>
    {(() => {
      if (error) {
        return <div>Sorry, an error occurred</div>;
      }
      if (loading) {
        return <div>Fetching data</div>;
      }
      if (listType === 'subregions') {
        return nodes
          .filter((node) => !!node._id && !!node.name)
          .map((node) => (
            <RenderSubregion
              key={node._id}
              subregion={node}
              onClickLink={onClickLink}
            />
          ));
      }
      if (listType === 'geonames') {
        return nodes.map((node) => (
          <RenderGeoname
            key={node._id}
            node={node}
            onClickNode={onClickNode}
            onClickLink={onClickLink}
          />
        ));
      }
      return flattenNodes(nodes)
        .filter((node) => !!node.name)
        .map((node) => {
          if (node.type === 'geoname') {
            return <RenderCity node={node} key={node._id} />;
          }
          return (
            <RenderSpot
              spot={node}
              key={node._id}
              onClickLink={onClickLink}
              sevenRatings={sevenRatings}
            />
          );
        });
    })()}
  </div>
);

ChildList.propTypes = {
  nodes: PropTypes.arrayOf(taxonomyNodePropType).isRequired,
  listType: PropTypes.string,
  onClickNode: PropTypes.func,
  loading: PropTypes.bool,
  error: PropTypes.bool,
  onClickLink: PropTypes.func,
  sevenRatings: PropTypes.bool,
};

ChildList.defaultProps = {
  onClickLink: null,
  listType: '',
  onClickNode: () => {},
  loading: false,
  error: false,
  sevenRatings: false,
};

export default ChildList;
