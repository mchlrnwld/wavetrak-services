import React from 'react';
import PropTypes from 'prop-types';

import Chevron from '../Chevron';
import Earth from '../icons/Earth';

const BreadcrumbNode = ({ label, taxonomyId, onClickNode, onClickLink }) => {
  const handleClickedButton = () => {
    onClickNode(taxonomyId);
    onClickLink(
      {
        clickEndLocation: 'Taxonomy Level',
        completion: false,
        linkName: label,
      },
      false
    );
  };

  return (
    <div className="quiver-taxonomy-navigator__breadcrumb-node">
      {onClickNode ? (
        <button
          type="button"
          className="quiver-taxonomy-navigator__breadcrumb-node__button"
          onClick={handleClickedButton}
        >
          {label === 'Earth' ? <Earth /> : label}
        </button>
      ) : (
        <span className="quiver-taxonomy-navigator__breadcrumb-node__placeholder">
          {label}
        </span>
      )}
      <Chevron direction="right" />
    </div>
  );
};

BreadcrumbNode.propTypes = {
  label: PropTypes.string,
  taxonomyId: PropTypes.string,
  onClickNode: PropTypes.func,
  onClickLink: PropTypes.func,
};

BreadcrumbNode.defaultProps = {
  label: '',
  taxonomyId: '',
  onClickLink: () => {},
  onClickNode: () => {},
};

export default BreadcrumbNode;
