import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Header from './Header';

describe('Header', () => {
  const activeNode = { name: 'activeNodeName' };
  const links = [
    { key: 'taxonomy', href: 'http://some.taxonomy.url' },
    { key: 'www', href: 'http://some.www.url' },
    { key: 'travel', href: 'http://some.travel.url' },
  ];

  it('should render instructions to Select a node from a list of Geonames', () => {
    const wrapper = shallow(
      <Header
        listType="geonames"
        label="GeonamePlaceholder"
        activeNode={activeNode}
      />
    );
    const titleWrapper = wrapper.find(
      '.quiver-taxonomy-navigator__header-title'
    );
    expect(titleWrapper.text()).to.equal('Select GeonamePlaceholder');
  });

  it('should render the Geoname name above a list of spots', () => {
    const wrapper = shallow(
      <Header
        listType="spots"
        label="GeonamePlaceholder"
        activeNode={activeNode}
      />
    );
    const titleWrapper = wrapper.find(
      '.quiver-taxonomy-navigator__header-title'
    );
    expect(titleWrapper.text()).to.equal(activeNode.name);
  });

  it('should append "Forecast" to the Geoname of a list of Subregions', () => {
    const wrapper = shallow(
      <Header
        listType="subregions"
        label="GeonamePlaceholder"
        activeNode={activeNode}
      />
    );
    const titleWrapper = wrapper.find(
      '.quiver-taxonomy-navigator__header-title'
    );
    expect(titleWrapper.text()).to.equal(`${activeNode.name} Forecasts`);
  });

  it('should render links to kbyg and travel if passed', () => {
    const wrapper = shallow(
      <Header
        listType="spots"
        label="GeonamePlaceholder"
        activeNode={{ ...activeNode, links }}
      />
    );
    const linksWrapper = wrapper.find('a');
    expect(linksWrapper).to.have.length(2);
  });

  it('should not render links to kbyg travel is listType is subregions', () => {
    const wrapper = shallow(
      <Header
        listType="subregions"
        label="GeonamePlaceholder"
        activeNode={activeNode}
      />
    );
    const linksWrapper = wrapper.find('a');
    expect(linksWrapper).to.have.length(0);
  });

  it('should not render links to kbyg and travel if not passed', () => {
    const wrapper = shallow(
      <Header
        listType="geonames"
        label="GeonamePlaceholder"
        activeNode={activeNode}
      />
    );
    const linksWrapper = wrapper.find('a');
    expect(linksWrapper).to.have.length(0);
  });
});
