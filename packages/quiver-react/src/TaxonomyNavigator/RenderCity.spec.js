import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import geonamesFixture from './fixtures/geonames.json';
import RenderCity from './RenderCity';

describe('TaxonomyNavigator / RenderCity', () => {
  const [geoname] = geonamesFixture;

  it('renders a div', () => {
    const wrapper = shallow(<RenderCity node={geoname} />);
    const div = wrapper.find('div');
    expect(div).to.have.length(1);
    expect(div.text()).to.equal('A Smaller Entity');
  });
});
