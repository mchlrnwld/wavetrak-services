import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount, shallow } from 'enzyme';
import * as fetchTaxonomy from '../helpers/fetchTaxonomy';
import * as locationStore from './utils/locationStore';
import Breadcrumb from './Breadcrumb';
import Header from './Header';
import ChildList from './ChildList';
import TaxonomyNavigator from './TaxonomyNavigator';

describe('TaxonomyNavigator', () => {
  const nodesPlaceholder = ['🌍', 'Continent', 'Country', 'Region', 'Area'];
  const props = {
    savedLocationType: 'spotsTaxonomyLocation',
    hierarchy: nodesPlaceholder,
    loggedIn: true,
    navigationSettings: {
      spotsTaxonomyLocation: '58f7ed5ddadb30820bb39651',
    },
    serviceConfig: {
      serviceUrl: 'http://surfline.service.url',
    },
    menuState: {
      loading: false,
      error: false,
      taxonomy: {
        parents: [],
        activeNode: {},
        children: [],
      },
    },
    setMenuState: () => true,
    scrollMenuToTop: sinon.spy(),
  };

  beforeEach(() => {
    sinon.stub(fetchTaxonomy, 'default');
    sinon.stub(locationStore, 'loadSavedLocation');
    sinon.stub(locationStore, 'saveLocation');
  });

  afterEach(() => {
    fetchTaxonomy.default.restore();
    locationStore.loadSavedLocation.restore();
    locationStore.saveLocation.restore();
  });

  it('should initialize state', () => {
    const shallowWrapper = mount(<TaxonomyNavigator {...props} />);
    const { menuState } = shallowWrapper.props();
    expect(menuState.loading).to.be.false();
    expect(menuState.error).to.be.false();
  });

  it('should fetch taxonomy on componentDidMount', () => {
    mount(<TaxonomyNavigator {...props} />);
    expect(fetchTaxonomy.default).to.have.been.calledOnce();
    expect(fetchTaxonomy.default).to.have.been.calledWithExactly(
      '58f7ed51dadb30820bb38782',
      0,
      props.serviceConfig
    );
  });

  it('should fetch taxonomy with saved location on componentDidMount', () => {
    locationStore.loadSavedLocation.returns('58f7ed5ddadb30820bb39651');
    mount(<TaxonomyNavigator {...props} />);
    expect(fetchTaxonomy.default).to.have.been.calledOnce();
    expect(fetchTaxonomy.default).to.have.been.calledWithExactly(
      '58f7ed5ddadb30820bb39651',
      1,
      props.serviceConfig
    );
    expect(locationStore.loadSavedLocation).to.have.been.calledOnce();
    expect(locationStore.loadSavedLocation).to.have.been.calledWithExactly(
      props.navigationSettings,
      'spotsTaxonomyLocation'
    );
  });

  it('should save location on updateActiveNodeTaxonomy', () => {
    const wrapper = shallow(<TaxonomyNavigator {...props} />);
    const instance = wrapper.instance();
    instance.updateActiveNodeTaxonomy('58f7ed5ddadb30820bb39651');
    expect(locationStore.saveLocation).to.have.been.calledOnce();
    expect(locationStore.saveLocation).to.have.been.calledWithExactly(
      '58f7ed5ddadb30820bb39651',
      'spotsTaxonomyLocation',
      true,
      props.navigationSettings,
      props.serviceConfig
    );
    // Called once in componentDidMount and once in updateActiveNodeTaxonomy
    expect(fetchTaxonomy.default).to.have.been.calledTwice();
    expect(fetchTaxonomy.default).to.have.been.calledWithExactly(
      '58f7ed5ddadb30820bb39651',
      0,
      props.serviceConfig
    );
  });

  it('should be able to determine a new taxonomy depth', () => {
    const menuState = {
      loading: false,
      error: false,
      taxonomy: {
        parents: [
          { _id: '58f7ed51dadb30820bb38782' },
          { _id: '58f7ed51dadb30820bb38791' },
          { _id: '58f7ed51dadb30820bb3879c' },
        ],
        activeNode: { _id: '58f7ed51dadb30820bb387a6' },
        children: [],
      },
    };
    const updatedProps = { ...props, menuState };
    const wrapper = shallow(<TaxonomyNavigator {...updatedProps} />);
    const instance = wrapper.instance();
    expect(instance.taxonomyDepth('58f7ed51dadb30820bb38782')).to.equal(0);
    expect(instance.taxonomyDepth('58f7ed51dadb30820bb3879c')).to.equal(2);
    expect(instance.taxonomyDepth('58f7ed51dadb30820bb387a6')).to.equal(3);
    expect(instance.taxonomyDepth('58f7ed5ddadb30820bb39651')).to.equal(4);
  });

  it('should render a Breadcrumb component', () => {
    const wrapper = shallow(<TaxonomyNavigator {...props} />);
    expect(wrapper.find(Breadcrumb)).to.have.length(1);
  });

  it('should render a Header component', () => {
    const wrapper = shallow(<TaxonomyNavigator {...props} />);
    expect(wrapper.find(Header)).to.have.length(1);
  });

  it('should render a ChildList component', () => {
    const wrapper = shallow(<TaxonomyNavigator {...props} />);
    expect(wrapper.find(ChildList)).to.have.length(1);
  });

  it('should pass sevenRatings prop to ChildList component when true', () => {
    const wrapper = shallow(<TaxonomyNavigator {...props} sevenRatings />);
    const childListWrapper = wrapper.find(ChildList);
    expect(childListWrapper.prop('sevenRatings')).to.be.true();
  });
});
