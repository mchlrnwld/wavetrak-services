import React from 'react';
import taxonomyNodePropType from '../PropTypes/taxonomyNodePropType';

const RenderCity = ({ node }) => (
  <div className="quiver-taxonomy-navigator__render-city">{node.name}</div>
);

RenderCity.propTypes = {
  node: taxonomyNodePropType.isRequired,
};

export default RenderCity;
