import React from 'react';
import PropTypes from 'prop-types';

import BreadcrumbNode from './BreadcrumbNode';
import taxonomyNodePropType from '../PropTypes/taxonomyNodePropType';
import { EARTH_TAXONOMY_ID } from './constants';

const Breadcrumb = ({
  nodes,
  nodesPlaceholder,
  listType,
  onClickNode,
  onClickLink,
}) => {
  const geonameList = nodesPlaceholder.map((placeholder, index) => {
    if (nodes[index]) {
      return (
        <BreadcrumbNode
          label={nodes[index].name}
          taxonomyId={index === 0 ? EARTH_TAXONOMY_ID : nodes[index]._id}
          key={placeholder}
          onClickNode={onClickNode}
          onClickLink={onClickLink}
        />
      );
    }
    if (listType === 'spots' && index < 5) {
      return null;
    }
    return <BreadcrumbNode label={placeholder} key={placeholder} />;
  });

  return (
    <div className="quiver-taxonomy-navigator__breadcrumb">{geonameList}</div>
  );
};

Breadcrumb.propTypes = {
  nodes: PropTypes.arrayOf(taxonomyNodePropType).isRequired,
  nodesPlaceholder: PropTypes.arrayOf(PropTypes.string),
  listType: PropTypes.string,
  onClickNode: PropTypes.func,
  onClickLink: PropTypes.func,
};

Breadcrumb.defaultProps = {
  nodesPlaceholder: [],
  listType: '',
  onClickLink: () => {},
  onClickNode: () => {},
};

export default Breadcrumb;
