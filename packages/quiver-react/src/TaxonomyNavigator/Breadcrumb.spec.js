import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import taxonomyFixture from './fixtures/taxonomy.json';
import BreadcrumbNode from './BreadcrumbNode';
import Breadcrumb from './Breadcrumb';

describe('TaxonomyNavigator / Breadcrumb', () => {
  const nodesPlaceholder = ['🌍', 'Continent', 'Country', 'Region', 'Area'];

  it('renders BreadcrumbNodes', () => {
    const wrapper = shallow(
      <Breadcrumb
        nodes={taxonomyFixture.in}
        nodesPlaceholder={nodesPlaceholder}
        listType="geonames"
        onClickNode={() => {}}
      />
    );
    expect(wrapper.find(BreadcrumbNode)).to.have.length(
      nodesPlaceholder.length
    );
  });

  it('renders fewer BreadcrumbNodes if spots are returned early', () => {
    const wrapper = shallow(
      <Breadcrumb
        nodes={taxonomyFixture.in}
        nodesPlaceholder={nodesPlaceholder}
        listType="spots"
        onClickNode={() => {}}
      />
    );
    expect(wrapper.find(BreadcrumbNode)).to.have.length(
      taxonomyFixture.in.length
    );
  });
});
