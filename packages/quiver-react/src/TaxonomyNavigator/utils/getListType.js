const getListType = (nodes, taxonomyType = 'spot') => {
  const targetTaxonomy = nodes.find((node) => node.type === taxonomyType);

  if (!targetTaxonomy) {
    return 'geonames';
  }
  return `${taxonomyType}s`;
};

export default getListType;
