import { expect } from 'chai';
import groupItems from './groupItems';

describe('TaxonomyNavigator / utils / groupItems', () => {
  const itemList = [
    {
      name: 'City1',
      _id: 'city1Id',
      type: 'geoname',
      hasSpots: true,
    },
    {
      name: 'City2',
      _id: 'city2Id',
      type: 'geoname',
      hasSpots: true,
    },
    {
      name: 'City without spots',
      _id: 'noSpotsCityId',
      type: 'geoname',
      hasSpots: false,
    },
    {
      name: 'Spot1a',
      _id: 'sampleTaxonomyObjectId1',
      spot: 'sampleSpotObjectId1',
      type: 'spot',
      liesIn: ['city1Id'],
    },
    {
      name: 'Spot2',
      _id: 'sampleTaxonomyObjectId2',
      spot: 'sampleSpotObjectId2',
      type: 'spot',
      liesIn: ['city2Id'],
    },
    {
      name: 'Spot1b',
      _id: 'sampleTaxonomyObjectId3',
      spot: 'sampleSpotObjectId3',
      type: 'spot',
      liesIn: ['city1Id'],
    },
  ];

  const spotReportViews = [
    {
      name: 'Spot1a',
      _id: 'sampleSpotObjectId1',
      cameras: [],
      conditions: {
        human: false,
        value: null,
      },
    },
    {
      name: 'Spot2',
      _id: 'sampleSpotObjectId2',
      cameras: [{}, {}],
      conditions: {
        human: true,
        value: 'FAIR',
      },
    },
    {
      name: 'Spot1b',
      _id: 'sampleSpotObjectId3',
      cameras: [],
      conditions: {
        human: true,
        value: 'NONE',
      },
    },
  ];

  it('should return spots grouped by city', () => {
    const result = groupItems(itemList, []);
    expect(result).to.have.length(2);

    expect(result[0].city.name).to.equal('City1');
    expect(result[0].spots).to.have.length(2);
    expect(result[0].spots[0].name).to.equal('Spot1a');
    expect(result[0].spots[1].name).to.equal('Spot1b');

    expect(result[1].city.name).to.equal('City2');
    expect(result[1].spots).to.have.length(1);
    expect(result[1].spots[0].name).to.equal('Spot2');
  });

  it('should return a group of spots without a city', () => {
    const result = groupItems(itemList.slice(3), []);

    expect(result).to.have.length(1);
    expect(result[0].city).to.be.null();
    expect(result[0].spots).to.have.length(3);
    expect(result[0].spots[0].name).to.equal('Spot1a');
    expect(result[0].spots[1].name).to.equal('Spot1b');
    expect(result[0].spots[2].name).to.equal('Spot2');
  });

  it('should merge and return spotreportview data with spots', () => {
    const result = groupItems(itemList.slice(3), spotReportViews);
    const [spot1a, spot1b, spot2] = result[0].spots;

    expect(result).to.have.length(1);
    expect(result[0].city).to.be.null();
    expect(result[0].spots).to.have.length(3);

    expect(spot1a.name).to.equal('Spot1a');
    expect(spot1a.hasCamera).to.be.false();
    expect(spot1a.conditions.human).to.be.false();
    expect(spot1a.conditions.value).to.be.null();

    expect(spot1b.name).to.equal('Spot1b');
    expect(spot1b.hasCamera).to.be.false();
    expect(spot1b.conditions.human).to.be.true();
    expect(spot1b.conditions.value).to.equal('NONE');

    expect(spot2.name).to.equal('Spot2');
    expect(spot2.hasCamera).to.be.true();
    expect(spot2.conditions.human).to.be.true();
    expect(spot2.conditions.value).to.equal('FAIR');
  });
});
