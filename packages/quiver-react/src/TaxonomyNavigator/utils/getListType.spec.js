import { expect } from 'chai';
import getListType from './getListType';

describe('TaxonomyNavigator / utils / getListType', () => {
  const geonamesList = [
    {
      name: 'A Smaller Entity',
      _id: 'sampleTaxonomyObjectId1',
      taxonomyId: 'someTaxonomyId',
      type: 'geoname',
    },
    {
      name: 'Another Smaller Entity',
      _id: 'sampleTaxonomyObjectId2',
      taxonomyId: 'someOtherTaxonomyId',
      type: 'geoname',
    },
    {
      name: 'A third entity (also smaller)',
      _id: 'sampleTaxonomyObjectId3',
      taxonomyId: 'someThirdTaxonomyId',
      type: 'geoname',
    },
  ];

  const spotsList = [
    {
      name: 'a spot',
      _id: 'sampleTaxonomyObjectId1',
      associated: { links: [{ key: 'www', href: 'http://some.taxonomy.url' }] },
      type: 'spot',
    },
    {
      name: 'another spot',
      _id: 'sampleTaxonomyObjectId2',
      associated: { links: [{ key: 'www', href: 'http://some.other.url' }] },
      type: 'spot',
    },
    {
      name: 'a third spot',
      _id: 'sampleTaxonomyObjectId3',
      associated: { links: [{ key: 'www', href: 'http://a.third.url' }] },
      type: 'spot',
    },
  ];

  const mixedList = [
    {
      name: 'A Smaller Entity',
      _id: 'sampleTaxonomyObjectId1',
      taxonomyId: 'someTaxonomyId',
      type: 'geoname',
    },
    {
      name: 'Another Smaller Entity',
      _id: 'sampleTaxonomyObjectId2',
      taxonomyId: 'someOtherTaxonomyId',
      type: 'geoname',
    },
    {
      name: 'A third entity (also smaller)',
      _id: 'sampleTaxonomyObjectId3',
      taxonomyId: 'someThirdTaxonomyId',
      type: 'geoname',
    },
    {
      name: 'a spot',
      _id: 'sampleTaxonomyObjectId1',
      associated: { links: [{ key: 'www', href: 'http://some.taxonomy.url' }] },
      type: 'spot',
    },
    {
      name: 'another spot',
      _id: 'sampleTaxonomyObjectId2',
      associated: { links: [{ key: 'www', href: 'http://some.other.url' }] },
      type: 'spot',
    },
    {
      name: 'a third spot',
      _id: 'sampleTaxonomyObjectId3',
      associated: { links: [{ key: 'www', href: 'http://a.third.url' }] },
      type: 'spot',
    },
  ];

  it('should return the listType spots', () => {
    const spotsListType = getListType(spotsList);
    const mixedListType = getListType(mixedList);
    expect(spotsListType).to.equal('spots');
    expect(mixedListType).to.equal('spots');
  });

  it('should return the listType geonames', () => {
    const listType = getListType(geonamesList);
    expect(listType).to.equal('geonames');
  });
});
