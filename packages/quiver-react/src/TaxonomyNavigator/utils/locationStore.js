import { Cookies } from 'react-cookie';
import updateSettings from '../../helpers/updateSettings';

export const loadSavedLocation = (navigationSettings, savedLocationType) => {
  const cookie = new Cookies();
  return (
    (navigationSettings && navigationSettings[savedLocationType]) ||
    cookie.get(`navigation.${savedLocationType}`, {
      path: '/',
      domain: '.surfline.com',
    })
  );
};

export const saveLocation = async (
  taxonomyId,
  savedLocationType,
  loggedIn,
  navigationSettings,
  serviceConfig
) => {
  if (loggedIn) {
    const settings = {
      navigation: {
        ...navigationSettings,
        [savedLocationType]: taxonomyId,
      },
    };
    await updateSettings(settings, serviceConfig);
  } else {
    const cookie = new Cookies();
    cookie.set(`navigation.${savedLocationType}`, taxonomyId, {
      path: '/',
      domain: '.surfline.com',
    });
  }
};
