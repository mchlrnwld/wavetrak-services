import { expect } from 'chai';
import sinon from 'sinon';
import { Cookies } from 'react-cookie';
import * as updateSettings from '../../helpers/updateSettings';
import { loadSavedLocation, saveLocation } from './locationStore';

describe('TaxonomyNavigator / utils / locationStore', () => {
  describe('loadSavedLocation', () => {
    let getCookieStub;
    beforeEach(() => {
      getCookieStub = sinon.stub(Cookies.prototype, 'get');
    });

    afterEach(() => {
      getCookieStub.restore();
    });

    it('loads saved location from cookie if no navigationSettings', () => {
      const navigationSettings = null;
      getCookieStub.returns('58f7ed5ddadb30820bb39651');
      expect(
        loadSavedLocation(navigationSettings, 'spotsTaxonomyLocation')
      ).to.equal('58f7ed5ddadb30820bb39651');
      expect(getCookieStub).to.have.been.calledOnce();
      expect(getCookieStub).to.have.been.calledWithExactly(
        'navigation.spotsTaxonomyLocation',
        {
          path: '/',
          domain: '.surfline.com',
        }
      );
    });

    it('loads saved location from cookie if navigationSettings has no location stored', () => {
      const navigationSettings = {};
      getCookieStub.returns('58f7ed5ddadb30820bb39651');
      expect(
        loadSavedLocation(navigationSettings, 'spotsTaxonomyLocation')
      ).to.equal('58f7ed5ddadb30820bb39651');
      expect(getCookieStub).to.have.been.calledOnce();
      expect(getCookieStub).to.have.been.calledWithExactly(
        'navigation.spotsTaxonomyLocation',
        {
          path: '/',
          domain: '.surfline.com',
        }
      );
    });

    it('loads saved location from navigationSettings if location is stored', () => {
      const navigationSettings = {
        spotsTaxonomyLocation: '58f7ed5ddadb30820bb39651',
      };
      expect(
        loadSavedLocation(navigationSettings, 'spotsTaxonomyLocation')
      ).to.equal('58f7ed5ddadb30820bb39651');
      expect(getCookieStub).not.to.have.been.called();
    });
  });

  describe('saveLocation', () => {
    const serviceConfig = {
      serviceUrl: 'https://services.test.surfline.com',
    };
    const navigationSettings = {
      forecastsTaxonomyLocation: '58f7ed51dadb30820bb387a6',
    };

    let setCookieStub;

    beforeEach(() => {
      setCookieStub = sinon.stub(Cookies.prototype, 'set');
      sinon.stub(updateSettings, 'default');
    });

    afterEach(() => {
      setCookieStub.restore();
      updateSettings.default.restore();
    });

    it('saves location to user settings', async () => {
      await saveLocation(
        '58f7ed5ddadb30820bb39651',
        'spotsTaxonomyLocation',
        true,
        navigationSettings,
        serviceConfig
      );
      expect(updateSettings.default).to.have.been.calledOnce();
      expect(updateSettings.default).to.have.been.calledWithExactly(
        {
          navigation: {
            forecastsTaxonomyLocation:
              navigationSettings.forecastsTaxonomyLocation,
            spotsTaxonomyLocation: '58f7ed5ddadb30820bb39651',
          },
        },
        serviceConfig
      );
      expect(setCookieStub).not.to.have.been.called();
    });

    it('saves location to cookie if not logged in', async () => {
      await saveLocation(
        '58f7ed5ddadb30820bb39651',
        'spotsTaxonomyLocation',
        false,
        navigationSettings,
        serviceConfig
      );
      expect(updateSettings.default).not.to.have.been.called();
      expect(setCookieStub).to.have.been.calledOnce();
      expect(setCookieStub).to.have.been.calledWithExactly(
        'navigation.spotsTaxonomyLocation',
        '58f7ed5ddadb30820bb39651',
        { path: '/', domain: '.surfline.com' }
      );
    });
  });
});
