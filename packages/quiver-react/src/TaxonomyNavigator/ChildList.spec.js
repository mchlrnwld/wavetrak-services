import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import geonamesFixture from './fixtures/geonames.json';
import spotsFixture from './fixtures/spots.json';
import groupedSpotsFixture from './fixtures/groupedSpots.json';
import RenderSpot from '../RenderSpot';
import RenderGeoname from './RenderGeoname';
import RenderCity from './RenderCity';
import ChildList from './ChildList';

describe('TaxonomyNavigator / ChildList', () => {
  it('renders a list of geonames when passed', () => {
    const wrapper = shallow(
      <ChildList listType="geonames" nodes={geonamesFixture} />
    );
    const nodeListWrapper = wrapper.find(RenderGeoname);
    expect(nodeListWrapper).to.have.length(3);
  });

  it('renders a list of spots when passed', () => {
    const wrapper = shallow(
      <ChildList listType="spots" nodes={spotsFixture} />
    );
    const nodeListWrapper = wrapper.find(RenderSpot);
    expect(nodeListWrapper).to.have.length(3);
  });

  it('renders a list of grouped spots when passed', () => {
    const wrapper = shallow(
      <ChildList listType="groupedSpots" nodes={groupedSpotsFixture} />
    );
    const cityListWrapper = wrapper.find(RenderCity);
    const spotListWrapper = wrapper.find(RenderSpot);
    expect(cityListWrapper).to.have.length(1);
    expect(spotListWrapper).to.have.length(2);
  });
});
