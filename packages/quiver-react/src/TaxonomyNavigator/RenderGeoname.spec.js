import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import geonamesFixture from './fixtures/geonames.json';
import RenderGeoname from './RenderGeoname';

describe('RenderGeoname', () => {
  const [geoname] = geonamesFixture;

  it('renders a button', () => {
    const onClickNode = sinon.spy();
    const onClickLink = sinon.spy();
    const wrapper = shallow(
      <RenderGeoname
        node={geoname}
        onClickNode={onClickNode}
        onClickLink={onClickLink}
      />
    );
    expect(onClickNode.called).to.be.false();
    expect(onClickLink.called).to.be.false();
    const buttonWrapper = wrapper.find('button');
    expect(buttonWrapper).to.have.length(1);
    expect(buttonWrapper.text()).to.contain('A Smaller Entity');
    buttonWrapper.simulate('click');
    expect(onClickNode.calledOnce).to.be.true();
    expect(onClickLink.calledOnce).to.be.true();
    expect(onClickNode).to.have.been.calledWithExactly(
      'sampleTaxonomyObjectId1'
    );
  });
});
