import React from 'react';
import PropTypes from 'prop-types';

import taxonomyNodePropType from '../PropTypes/taxonomyNodePropType';

const Header = ({
  listType,
  label,
  activeNode,
  onClickChange,
  onClickLink,
}) => {
  const handleClickedChange = () => {
    onClickChange();
    onClickLink(
      {
        completion: false,
        linkName: 'change',
      },
      false
    );
  };

  const { name, links } = activeNode;
  const www =
    listType === 'spots' &&
    links &&
    links.find((link) => link && link.key === 'www');
  const travel =
    listType === 'spots' &&
    links &&
    links.find((link) => link && link.key === 'travel');

  const subregionTitle = listType === 'subregions' ? ' Forecasts' : '';

  return (
    <div className="quiver-taxonomy-navigator__header">
      <div className="quiver-taxonomy-navigator__header-title">
        {listType === 'geonames' && label
          ? `Select ${label}`
          : `${name}${subregionTitle}`}
      </div>
      {www ? (
        <a
          href={www.href}
          onClick={() =>
            onClickLink(
              {
                clickEndLocation: 'Map',
                destinationUrl: www.href,
                completion: true,
                linkName: 'View spots on map',
              },
              true
            )
          }
        >
          View spots on map
        </a>
      ) : null}
      {listType === 'spots' && travel ? (
        <a
          href={travel.href}
          onClick={() =>
            onClickLink(
              {
                clickEndLocation: 'Travel',
                destinationUrl: travel.href,
                completion: true,
                linkName: 'View travel information',
              },
              true
            )
          }
        >
          View travel information
        </a>
      ) : null}
      <button type="button" onClick={handleClickedChange}>
        change
      </button>
    </div>
  );
};

Header.propTypes = {
  activeNode: taxonomyNodePropType.isRequired,
  listType: PropTypes.string,
  label: PropTypes.string,
  onClickChange: PropTypes.func,
  onClickLink: PropTypes.func,
};

Header.defaultProps = {
  listType: '',
  label: '',
  onClickChange: () => {},
  onClickLink: () => {},
};

export default Header;
