import React from 'react';
import PropTypes from 'prop-types';

import Chevron from '../Chevron';
import taxonomyNodePropType from '../PropTypes/taxonomyNodePropType';

const RenderGeoname = ({ node, onClickNode, onClickLink }) => {
  const handleClickedButton = () => {
    onClickNode(node._id);
    onClickLink(
      {
        clickEndLocation: 'Taxonomy Level',
        completion: false,
        linkName: node.name,
      },
      false
    );
  };

  return (
    <div className="quiver-taxonomy-navigator__render-geoname">
      <button type="button" onClick={handleClickedButton}>
        <div className="quiver-taxonomy-navigator__render-geoname__button-wrapper">
          {node.name}
          <Chevron direction="right" />
        </div>
      </button>
    </div>
  );
};

RenderGeoname.propTypes = {
  node: taxonomyNodePropType.isRequired,
  onClickNode: PropTypes.func,
  onClickLink: PropTypes.func,
};

RenderGeoname.defaultProps = {
  onClickNode: () => null,
  onClickLink: () => null,
};

export default RenderGeoname;
