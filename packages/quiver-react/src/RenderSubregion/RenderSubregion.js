import React from 'react';
import PropTypes from 'prop-types';

import taxonomyNodePropType from '../PropTypes/taxonomyNodePropType';
import generateSlug from '../helpers/generateSlug';
import subregionPagePath from '../helpers/subregionPagePath';

const RenderSubregion = ({ subregion, onClickLink }) => {
  const getHref = ({ associated, _id, name }) => {
    if (associated) {
      return associated.links.find((link) => link.key === 'www').href;
    }
    const slug = generateSlug(name);
    return subregionPagePath(_id, slug);
  };

  return (
    <div className="quiver-render-subregion">
      <a
        href={getHref(subregion)}
        onClick={() =>
          onClickLink(
            {
              clickEndLocation: 'Region Forecast',
              destinatonUrl: getHref(subregion),
              completion: true,
              linkName: subregion.name,
            },
            true
          )
        }
      >
        {subregion.name}
      </a>
    </div>
  );
};

RenderSubregion.propTypes = {
  subregion: taxonomyNodePropType.isRequired,
  onClickLink: PropTypes.func,
};

RenderSubregion.defaultProps = {
  onClickLink: null,
};

export default RenderSubregion;
