import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import subregionFixture from '../TaxonomyNavigator/fixtures/subregion.json';
import RenderSubregion from './RenderSubregion';

describe('RenderSubregion', () => {
  const [subregion] = subregionFixture[0].subregions;
  it('renders an a tag', () => {
    const wrapper = shallow(<RenderSubregion subregion={subregion} />);
    const aWrapper = wrapper.find('a');
    expect(aWrapper.text()).to.equal('a subregion');
    expect(aWrapper.props().href).to.equal(
      subregion.associated.links.find((link) => link.key === 'www').href
    );
  });
});
