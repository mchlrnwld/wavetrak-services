import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import CloseIcon from '../icons/CloseIcon';

const className = (type) =>
  classNames({
    'quiver-butterbar': true,
    [`quiver-butterbar-${type}`]: !!type,
  });

const ButterBar = ({
  children,
  canClose,
  isClosed,
  onRequestClose,
  title,
  type,
}) => (
  <div className={className(type)}>
    <div className="quiver-butterbar__titlebar">
      <div className="quiver-butterbar__titlebar__title">{title}</div>
    </div>
    {children != null && (
      <div
        className="quiver-butterbar__body"
        style={{ display: !isClosed ? 'block' : 'none' }}
      >
        {children}
        {canClose && (
          <div className="quiver-butterbar__body__close">
            <CloseIcon onClick={onRequestClose} isCircle />
          </div>
        )}
      </div>
    )}
  </div>
);

ButterBar.propTypes = {
  canClose: PropTypes.bool,
  children: PropTypes.node,
  isClosed: PropTypes.bool,
  onRequestClose: PropTypes.func,
  title: PropTypes.node.isRequired,
  type: PropTypes.string,
};

ButterBar.defaultProps = {
  canClose: true,
  children: null,
  isClosed: false,
  onRequestClose: null,
  type: null,
};

export default ButterBar;
