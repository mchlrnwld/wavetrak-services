import React from 'react';
import ButterBar from './ButterBar';
import ContentContainer from '../ContentContainer';
import SurflineCWLogo from '../icons/SurflineCWLogo';

export default {
  title: 'ButterBar',
};

export const Default = () => (
  <ContentContainer>
    <ButterBar title="Storybook">
      <p className="quiver-butterbar__body__tagline">
        This is the ButterBar tagline!
      </p>
      <p>This is the ButterBar body text.</p>
    </ButterBar>
  </ContentContainer>
);

Default.story = {
  name: 'default',
};

export const TitleOnly = () => (
  <ContentContainer>
    <ButterBar title="ONLY THE TITLE - short messages" type="aus" />
  </ContentContainer>
);

TitleOnly.story = {
  name: 'title only',
};

export const NoCloseButton = () => (
  <ContentContainer>
    <ButterBar canClose={false} title="No close button">
      <p>You cannot close this body</p>
    </ButterBar>
  </ContentContainer>
);

NoCloseButton.story = {
  name: 'no close button',
};

export const AusButterBar = () => (
  <ContentContainer>
    <ButterBar closeButton title={<SurflineCWLogo />} type="aus">
      <p className="quiver-butterbar__body__tagline">
        Here from Coastalwatch? Welcome!
      </p>
      <p>
        This is the new home of Surfline with Coastalwatch: the best network of
        free high definition surf-cams in Australia. With long range, in-depth
        surf forecasts, and live reports to help you score more waves.
      </p>
    </ButterBar>
  </ContentContainer>
);

AusButterBar.story = {
  name: 'AUS ButterBar',
};
