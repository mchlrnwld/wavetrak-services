import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames';
import Chevron from '../Chevron';
import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

class CollapsibleModule extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  componentDidUpdate(_, prevState) {
    const { open } = this.state;
    const { onOpen } = this.props;
    if (!prevState.open && open && onOpen) {
      onOpen();
    }
  }

  getCollapsibleModuleClasses = (open) =>
    classNames({
      'quiver-collapsible-module': true,
      'quiver-collapsible-module--open': open,
      'quiver-collapsible-module--closed': !open,
    });

  handleClick = () => {
    const { onClick } = this.props;
    const { open } = this.state;
    if (onClick) onClick();
    this.setState({
      open: !open,
    });
  };

  render() {
    const { title, children } = this.props;
    const { open } = this.state;

    return (
      <div className={this.getCollapsibleModuleClasses(open)}>
        <div
          className="quiver-collapsible-module__header"
          {...createAccessibleOnClick(this.handleClick, BTN)}
        >
          <div className="quiver-collapsible-module__header-text">{title}</div>
          {open ? <Chevron direction="up" /> : <Chevron />}
        </div>
        <div className="quiver-collapsible-module__content">{children}</div>
      </div>
    );
  }
}

CollapsibleModule.propTypes = {
  title: PropTypes.string,
  onOpen: PropTypes.func,
  children: PropTypes.node,
  onClick: PropTypes.func,
};

CollapsibleModule.defaultProps = {
  title: null,
  onOpen: null,
  children: null,
  onClick: null,
};

export default CollapsibleModule;
