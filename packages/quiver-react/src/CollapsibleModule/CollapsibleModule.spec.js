import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import CollapsibleModule from './CollapsibleModule';
import Chevron from '../Chevron';

describe('CollapsibleModule', () => {
  const Child = () => <div className="child" />;
  const props = {
    onOpen: sinon.spy(),
    title: 'test',
    onClick: sinon.spy(),
  };

  afterEach(() => {
    props.onOpen.resetHistory();
  });

  it('should render the collapsible module', () => {
    const wrapper = mount(
      <CollapsibleModule {...props}>
        <Child />
      </CollapsibleModule>
    );

    expect(wrapper.state('open')).to.be.false();
    const module = wrapper.find('.quiver-collapsible-module--closed');
    expect(module).to.have.length(1);
    expect(wrapper.find('.quiver-collapsible-module--open')).to.have.length(0);
    expect(
      wrapper.find('.quiver-collapsible-module__header-text').text()
    ).to.be.equal('test');

    expect(wrapper.find(Child)).to.have.length(1);
  });

  it('should open the collapsible module', () => {
    const wrapper = mount(<CollapsibleModule {...props} />);
    expect(wrapper.find(Chevron).prop('direction')).to.be.equal('down');

    wrapper.find('.quiver-collapsible-module__header').simulate('click');
    wrapper.update();
    expect(props.onOpen).to.have.been.calledOnce();
    expect(props.onClick).to.have.been.calledOnce();
    expect(wrapper.find(Chevron).prop('direction')).to.be.equal('up');
  });
});
