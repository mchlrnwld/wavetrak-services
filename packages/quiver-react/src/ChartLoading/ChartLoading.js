import PropTypes from 'prop-types';
import React from 'react';
import Spinner from '../Spinner';

const ChartLoading = ({ chart }) => (
  <div className="quiver-chart-loading">
    <Spinner />
    Checking the {chart}
  </div>
);

ChartLoading.propTypes = {
  chart: PropTypes.string.isRequired,
};

export default ChartLoading;
