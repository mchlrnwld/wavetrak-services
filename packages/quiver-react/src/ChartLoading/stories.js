import React from 'react';
import ChartLoading from './ChartLoading';

export default {
  title: 'ChartLoading',
};

export const Default = () => <ChartLoading chart="surf" />;

Default.story = {
  name: 'default',
};
