import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import ChartLoading from './ChartLoading';
import Spinner from '../Spinner';

describe('ChartLoading', () => {
  it('should render the chart loading', () => {
    const wrapper = mount(<ChartLoading chart="swell" />);

    expect(wrapper.find(Spinner)).to.have.length(1);
    expect(wrapper.find('.quiver-chart-loading')).to.have.length(1);
    expect(wrapper.text()).to.be.equal('Checking the swell');
  });
});
