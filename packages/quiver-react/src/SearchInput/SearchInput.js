import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Input from '../Input';
import Button from '../Button';

export default class SearchInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: props.defaultValue,
    };
  }

  onChange = (event) => {
    this.setState({ query: event.target.value });
  };

  onSubmit = (event) => {
    const { onSubmit } = this.props;
    const { query } = this.state;
    event.preventDefault();
    onSubmit(query);
  };

  render() {
    const { defaultValue, message, inputLabel, buttonText, loading, style } =
      this.props;

    return (
      <form
        onSubmit={this.onSubmit}
        className="quiver-search-input"
        style={style}
      >
        <Input
          label={inputLabel || 'Search'}
          type="text"
          defaultValue={defaultValue}
          message={message}
          input={{ onChange: this.onChange }}
          style={style && style.Input}
        />
        <Button loading={loading} style={style && style.Button}>
          {buttonText || 'Search'}
        </Button>
      </form>
    );
  }
}

SearchInput.propTypes = {
  defaultValue: PropTypes.string,
  buttonText: PropTypes.string,
  inputLabel: PropTypes.string,
  message: PropTypes.string,
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
  style: PropTypes.shape({
    Button: PropTypes.shape({}),
    Input: PropTypes.shape({}),
  }),
};

SearchInput.defaultProps = {
  defaultValue: '',
  buttonText: '',
  inputLabel: '',
  message: '',
  onSubmit: () => {},
  loading: false,
  style: {},
};
