import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';
import SearchInput from './SearchInput';

describe('SearchInput', () => {
  it('should handle onSubmit events', () => {
    const onSubmit = sinon.spy();

    const wrapper = mount(<SearchInput onSubmit={onSubmit} />);
    const labelWrapper = wrapper.find('label');
    const inputWrapper = wrapper.find('input');

    expect(labelWrapper.text()).to.equal('Search');
    expect(onSubmit).not.to.have.been.called();

    inputWrapper.simulate('change', { target: { value: 'Kelly Slater' } });
    wrapper.simulate('submit');
    expect(onSubmit).to.have.been.calledOnce();
    expect(onSubmit).to.have.been.calledWithExactly('Kelly Slater');
  });

  it('should pass custom style object into children', () => {
    const style = {
      fontFamily: 'container',
      Input: {
        fontFamily: 'Input',
      },
      Button: {
        fontFamily: 'Button',
      },
    };
    const wrapper = shallow(<SearchInput style={style} />);
    const containerWrapper = wrapper.find('form');
    const inputWrapper = containerWrapper.find('Input');
    const buttonWrapper = containerWrapper.find('Button');

    expect(containerWrapper.prop('style')).to.equal(style);
    expect(inputWrapper.prop('style')).to.equal(style.Input);
    expect(buttonWrapper.prop('style')).to.equal(style.Button);
  });

  it('should pass defaultValue into child Input', () => {
    const wrapper = shallow(<SearchInput defaultValue="some query" />);
    const inputWrapper = wrapper.find('Input');
    expect(inputWrapper.prop('defaultValue')).to.equal('some query');
  });

  it('should pass buttonText into child button', () => {
    const defaultWrapper = mount(<SearchInput />);
    const wrapper = mount(<SearchInput buttonText="Button Text" />);
    const defaultButton = defaultWrapper.find('button');
    const button = wrapper.find('button');
    expect(defaultButton.text()).to.equal('Search');
    expect(button.text()).to.equal('Button Text');
  });

  it('should pass inputLabel into child label', () => {
    const defaultWrapper = mount(<SearchInput />);
    const wrapper = mount(<SearchInput inputLabel="Input Label" />);
    const defaultLabel = defaultWrapper.find('label');
    const label = wrapper.find('label');
    expect(defaultLabel.text()).to.equal('Search');
    expect(label.text()).to.equal('Input Label');
  });

  it('should pass message into child Input', () => {
    const wrapper = shallow(<SearchInput message="Some helper text" />);
    const inputWrapper = wrapper.find('Input');
    expect(inputWrapper.prop('message')).to.equal('Some helper text');
  });
});
