import React from 'react';
import { action } from '@storybook/addon-actions';
import SearchInput from './index';

export default {
  title: 'SearchInput',
};

export const Default = () => <SearchInput />;

Default.story = {
  name: 'default',
};

export const DefaultWithDefaultValue = () => (
  <SearchInput defaultValue="some query" />
);

DefaultWithDefaultValue.story = {
  name: 'default with default value',
};

export const DefaultWithHelperText = () => (
  <SearchInput message="Some helper text" />
);

DefaultWithHelperText.story = {
  name: 'default with helper text',
};

export const CustomInputLabelAndButtonText = () => (
  <SearchInput
    inputLabel="Custom Search Label"
    buttonText="Custom Search Button"
  />
);

CustomInputLabelAndButtonText.story = {
  name: 'custom input label and button text',
};

export const Loading = () => <SearchInput loading />;

Loading.story = {
  name: 'loading',
};

export const CustomStyles = () => (
  <SearchInput
    style={{
      margin: '20px 50px',
      Input: {
        input: {
          backgroundColor: '#67d3df',
        },
      },
      Button: {
        backgroundColor: '#22d736',
      },
    }}
  />
);

CustomStyles.story = {
  name: 'custom styles',
};

export const OnSubmitEvent = () => <SearchInput onSubmit={action('submit')} />;

OnSubmitEvent.story = {
  name: 'onSubmit event',
};
