import React from 'react';
import PropTypes from 'prop-types';

const AddToCalendar = ({ event, onClickAdd }) => {
  const formatTime = (date) =>
    new Date(date).toISOString().replace(/-|:|\.\d+/g, '');

  const hrefGenerator = ({ startTime, endTime, title, description }) => {
    const start = formatTime(startTime);
    const end = formatTime(endTime);

    const href = encodeURI(
      `data:text/calendar;charset=utf8,${[
        'BEGIN:VCALENDAR',
        'VERSION:2.0',
        'BEGIN:VEVENT',
        `URL:${document.URL}`,
        `DTSTART:${start || ''}`,
        `DTEND:${end || ''}`,
        `SUMMARY:${title || ''}`,
        `DESCRIPTION:${description || ''}`,
        'END:VEVENT',
        'END:VCALENDAR',
      ].join('\n')}`
    );
    return href;
  };
  return (
    <div className="quiver-add-to-calendar">
      <a href={hrefGenerator(event)} onClick={() => onClickAdd()}>
        Add to Calendar
      </a>
    </div>
  );
};

AddToCalendar.propTypes = {
  event: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    startTime: PropTypes.string,
    endTime: PropTypes.string,
  }),
  onClickAdd: PropTypes.func,
};

AddToCalendar.defaultProps = {
  event: {
    title: '',
    description: '',
    startTime: '',
    endtime: '',
  },
  onClickAdd: () => {},
};

export default AddToCalendar;
