import React from 'react';
import AddToCalendar from './index';

const defaultEvent = {
  title: 'Surf HB Pier SS',
  description: 'https://surfline.com',
  startTime: '2019-10-10T07:30:00-04:00',
  endTime: '2019-10-10T08:30:00-04:00',
};

export default {
  title: 'AddToCalendar',
};

export const Default = () => <AddToCalendar event={defaultEvent} />;

Default.story = {
  name: 'default',
};
