import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import AddToCalendar from './AddToCalendar';

describe('AddToCalendar', () => {
  it('should render the anchor tag with the proper props', () => {
    const wrapper = mount(
      <AddToCalendar
        event={{
          title: 'Surf HB Pier SS',
          description: 'https://surfline.com',
          startTime: '2019-10-10T07:30:00-04:00',
          endTime: '2019-10-10T08:30:00-04:00',
        }}
      />
    );

    const aTag = wrapper.find('a');
    expect(aTag.prop('href')).to.contain('DESCRIPTION:https://surfline.com');
    expect(aTag.prop('href')).to.contain('SUMMARY:Surf%20HB%20Pier%20SS');
    expect(aTag.prop('href')).to.contain('DTSTART:20191010T113000Z');
    expect(aTag.prop('href')).to.contain('DTEND:20191010T123000Z');
  });
});
