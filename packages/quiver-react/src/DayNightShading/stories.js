import React from 'react';
import DayNightShading from './DayNightShading';

export default {
  title: 'DayNightShading',
};

export const Default = () => (
  <div style={{ width: 300, height: 300, display: 'flex' }}>
    <DayNightShading
      dayClass="daynight"
      dawnXPercentage={0.1}
      duskXPercentage={0.9}
      numDays={1}
      day={0}
      isPaywalled={false}
    />
  </div>
);

Default.story = {
  name: 'default',
};
