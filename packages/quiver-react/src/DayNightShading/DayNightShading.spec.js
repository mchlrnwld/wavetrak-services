/** @prettier */

import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { color } from '@surfline/quiver-themes';
import DayNightShading from './DayNightShading';

describe('components / DayNightShading', () => {
  it('shades columns according to dawnXPercentage and duskXPercentage', () => {
    const wrapper = mount(
      <DayNightShading
        dayClass="daynight"
        dawnXPercentage={0.1}
        duskXPercentage={0.9}
        numDays={1}
        day={0}
        isPaywalled={false}
      />
    );

    const dayNightShade = wrapper.find('.quiver-day-night-shading');
    const style = dayNightShade.prop('style');
    expect(style.background.replace(/\s/g, '')).to.equal(
      `linear-gradient(
        to right,
        ${color('blue-gray', 10)} 0%,
        ${color('blue-gray', 10)} 10%,
        ${color('white')} 10%,
        ${color('white')} 90%,
        ${color('blue-gray', 10)} 90%,
        ${color('blue-gray', 10)} 100%
      )`.replace(/\s/g, '')
    );
  });
});
