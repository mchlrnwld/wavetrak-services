import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { color } from '@surfline/quiver-themes';

const containerClassName = (isLastChild, isPaywalled) =>
  classNames({
    'quiver-day-night-shading-container': true,
    'quiver-day-night-shading-container--with-border': !isLastChild,
    'quiver-day-night-shading-container--paywalled': isPaywalled,
  });

const transitionBackground = (
  nightColor,
  dayColor,
  dawnXPercentage,
  duskXPercentage
) =>
  `linear-gradient(
    to right,
    ${nightColor} 0%,
    ${nightColor} ${dawnXPercentage * 100}%,
    ${dayColor} ${dawnXPercentage * 100}%,
    ${dayColor} ${duskXPercentage * 100}%,
    ${nightColor} ${duskXPercentage * 100}%,
    ${nightColor} 100%
  )`;

const DayNightShading = ({
  dawnXPercentage,
  duskXPercentage,
  numDays,
  day,
  isPaywalled,
}) => {
  const background = transitionBackground(
    color('blue-gray', 10),
    color('white'),
    dawnXPercentage,
    duskXPercentage
  );

  const isLastChild = day === numDays - 1;
  const containerStyle = {
    flex: `0 0 calc(${100 / numDays}% - ${isLastChild ? 0 : 1}px)`,
  };

  const style = {
    background,
  };

  return (
    <div
      className={containerClassName(isLastChild, isPaywalled)}
      style={containerStyle}
    >
      <div
        className="quiver-day-night-shading"
        style={!isPaywalled ? style : null}
      />
    </div>
  );
};

DayNightShading.propTypes = {
  dawnXPercentage: PropTypes.number.isRequired,
  duskXPercentage: PropTypes.number.isRequired,
  day: PropTypes.number.isRequired,
  numDays: PropTypes.number,
  isPaywalled: PropTypes.bool,
};

DayNightShading.defaultProps = {
  numDays: 1,
  isPaywalled: false,
};

export default DayNightShading;
