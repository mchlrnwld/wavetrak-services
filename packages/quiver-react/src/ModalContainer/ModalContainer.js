import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Modal from 'react-modal';
import Close from './Close';

const getclassNames = (location) =>
  classnames({
    'quiver-modal-container': true,
    [`quiver-modal-container--${location}`]: !!location,
    [`quiver-modal-container__close--${location}`]: !!location,
  });

/**
 * @description The ModalContainer component is the controller for an individual Modal.
 * The Modal's content can be passed a child prop to the container.
 *
 * @param {object} props
 * @param {string} props.contentLabel string for the aria-label
 * @param {function} props.onClose optional function called upon closing the modal
 * @param {boolean} props.isOpen boolean flag for showing the modal
 */
class ModalContainer extends Component {
  componentDidMount() {
    window.addEventListener('keyup', this.closeOnEscapeKey);
  }

  componentWillUnmount() {
    window.removeEventListener('keyup', this.closeOnEscapeKey);
  }

  className = {
    base: 'quiver-modal-container__container',
    afterOpen: 'quiver-modal-container__container--after-open',
    beforeClose: 'quiver-modal-container__container--before-close',
  };

  closeOnEscapeKey = ({ key }) => {
    const { onClose, location } = this.props;
    if (key === 'Escape' && location !== 'age-verification') {
      onClose();
    }
  };

  render() {
    const { isOpen, onClose, contentLabel, children, location } = this.props;

    const overlayClassName = {
      base: getclassNames(location),
      afterOpen: 'quiver-modal-container--after-open',
      beforeClose: 'quiver-modal-container--before-close',
    };

    return (
      <Modal
        isOpen={isOpen}
        onRequestClose={onClose}
        contentLabel={contentLabel}
        className={this.className}
        overlayClassName={overlayClassName}
        ariaHideApp={false}
        shouldCloseOnOverlayClick={location !== 'age-verification'}
        shouldCloseOnEsc={location !== 'age-verification'}
      >
        <div className="quiver-modal-container__body">
          {onClose ? <Close onClick={onClose} /> : null}
          {children}
        </div>
      </Modal>
    );
  }
}

ModalContainer.propTypes = {
  contentLabel: PropTypes.string.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  children: PropTypes.node.isRequired,
  location: PropTypes.string,
};

ModalContainer.defaultProps = {
  onClose: null,
  location: '',
};

export default ModalContainer;
