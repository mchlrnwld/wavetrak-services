import React from 'react';
import PropTypes from 'prop-types';
import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

const Close = ({ onClick }) => (
  <a
    className="quiver-modal-container__close"
    {...createAccessibleOnClick(onClick, BTN)}
  >
    <svg viewBox="0 0 14 14">
      <line x1="0" y1="0" x2="14" y2="14" />
      <line x1="14" y1="0" x2="0" y2="14" />
    </svg>
  </a>
);

Close.propTypes = {
  onClick: PropTypes.func,
};

Close.defaultProps = {
  onClick: null,
};

export default Close;
