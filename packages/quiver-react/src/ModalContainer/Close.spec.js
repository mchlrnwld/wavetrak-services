import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import Close from './Close';

describe('components / ModalContainer / Close', () => {
  it('handles click event', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(<Close onClick={onClick} />);
    wrapper.simulate('click');
    expect(onClick).to.have.been.calledOnce();
  });
});
