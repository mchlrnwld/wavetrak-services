import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import Modal from 'react-modal';
import Close from './Close';
import ModalContainer from './ModalContainer';

describe('components / ModalContainer', () => {
  it('closes when Escape key is pressed', () => {
    const onClose = sinon.spy();
    shallow(<ModalContainer isOpen onClose={onClose} />);

    const enterKeyupEvent = new Event('keyup');
    enterKeyupEvent.key = 'Enter';

    const escapeKeyupEvent = new Event('keyup');
    escapeKeyupEvent.key = 'Escape';

    const aKeyupEvent = new Event('keyup');
    aKeyupEvent.key = 'A';

    window.dispatchEvent(enterKeyupEvent);
    expect(onClose).not.to.have.been.called();

    window.dispatchEvent(escapeKeyupEvent);
    expect(onClose).to.have.been.calledOnce();

    window.dispatchEvent(aKeyupEvent);
    expect(onClose).to.have.been.calledOnce();
  });

  it('renders ModalContainer with appropriate props', () => {
    const onClose = sinon.spy();
    const wrapper = shallow(
      <ModalContainer isOpen onClose={onClose} contentLabel="contentLabel" />
    );
    const modal = wrapper.find(Modal);
    expect(modal).to.have.length(1);
    expect(modal.prop('isOpen')).to.be.true();
    expect(modal.prop('onRequestClose')).to.equal(onClose);
    expect(modal.prop('contentLabel')).to.equal('contentLabel');
  });

  it('renders Close with onClick handler', () => {
    const onClose = sinon.spy();
    const wrapper = shallow(<ModalContainer onClose={onClose} />);
    const close = wrapper.find(Close);
    expect(close).to.have.length(1);
    expect(Object.keys(close.props())).to.deep.equal(['onClick']);
    expect(close.prop('onClick')).to.equal(onClose);
  });
});
