import PropTypes from 'prop-types';
import conditionReportPropType from './conditionReport';

export default PropTypes.shape({
  timestamp: PropTypes.number,
  forecaster: PropTypes.shape({
    name: PropTypes.string,
    avatar: PropTypes.string,
  }),
  am: conditionReportPropType,
  pm: conditionReportPropType,
  observation: PropTypes.string,
});
