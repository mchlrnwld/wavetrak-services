import PropTypes from 'prop-types';

/**
 * @typedef {object} CameraStatus
 * @property {string} altMessage
 * @property {boolean} isDown
 * @property {string} message
 * @property {string} subMessage
 */

/**
 * @typedef {object} Camera
 * @property {string} _id
 * @property {string} alias
 * @property {string} control
 * @property {boolean} isPremium
 * @property {boolean} isPrerecorded
 * @property {boolean} lastPrerecordedClipEndTimeUTC
 * @property {boolean} lastPrerecordedClipStartTimeUTC
 * @property {boolean} nightime
 * @property {string} rewindBaseUrl
 * @property {string} rewindClip
 * @property {CameraStatus} status
 * @property {string} stillUrl
 * @property {string} streamUrl
 * @property {string} title
 */

export const cameraPropType = PropTypes.shape({
  streamUrl: PropTypes.string,
  stillUrl: PropTypes.string,
  status: PropTypes.shape(),
  isPrerecorded: PropTypes.bool,
  isPremium: PropTypes.bool,
  title: PropTypes.string,
});

export const camerasPropType = PropTypes.arrayOf(cameraPropType);
