import PropTypes from 'prop-types';

const entitlementsPropType = PropTypes.arrayOf(PropTypes.string);

export default entitlementsPropType;
