import PropTypes from 'prop-types';

export const defaultDateFormat = 'MDY';

export default PropTypes.oneOf(['MDY', 'DMY']);
