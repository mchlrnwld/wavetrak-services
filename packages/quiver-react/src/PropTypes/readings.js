import PropTypes from 'prop-types';

export const tideNodePropType = PropTypes.shape({
  type: PropTypes.string,
  height: PropTypes.number,
  timetstamp: PropTypes.number,
  utcOffset: PropTypes.number,
});

export const tidePropTypes = PropTypes.shape({
  current: tideNodePropType,
  next: tideNodePropType,
  previous: tideNodePropType,
});

export const conditionsPropTypes = PropTypes.shape({
  human: PropTypes.bool,
  sortableCondition: PropTypes.number,
  value: PropTypes.string,
});

export const waterPropTypes = PropTypes.shape({
  min: PropTypes.number,
  max: PropTypes.number,
});

export const waveHeightPropTypes = PropTypes.shape({
  human: PropTypes.bool,
  humanRelation: PropTypes.string,
  max: PropTypes.number,
  min: PropTypes.number,
  occasional: PropTypes.number,
  plus: PropTypes.bool,
});

export const weatherPropTypes = PropTypes.shape({
  condition: PropTypes.string,
  temperature: PropTypes.number,
});

export const windPropTypes = PropTypes.shape({
  direction: PropTypes.number,
  speed: PropTypes.number,
});

export const swellsPropTypes = PropTypes.arrayOf(
  PropTypes.shape({
    height: PropTypes.number,
    period: PropTypes.number,
    direction: PropTypes.number,
    index: PropTypes.number,
  })
);

export default PropTypes.shape({
  conditions: conditionsPropTypes,
  tide: tidePropTypes,
  waterTemp: waterPropTypes,
  waveHeight: waveHeightPropTypes,
  weather: weatherPropTypes,
  wind: windPropTypes,
  swells: swellsPropTypes,
  note: PropTypes.string,
});
