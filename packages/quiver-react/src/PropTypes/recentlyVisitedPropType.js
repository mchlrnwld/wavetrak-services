import PropTypes from 'prop-types';

const taxonomyNodePropType = PropTypes.shape({
  _id: PropTypes.string,
  name: PropTypes.string,
});

export default taxonomyNodePropType;
