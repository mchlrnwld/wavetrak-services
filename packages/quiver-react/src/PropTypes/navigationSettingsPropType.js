import PropTypes from 'prop-types';

export default PropTypes.shape({
  spotsTaxonomyLocation: PropTypes.string,
  forecastsTaxonomyLocation: PropTypes.string,
});
