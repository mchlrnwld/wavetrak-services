import PropTypes from 'prop-types';

export const forecasterPropTypes = PropTypes.shape({
  iconUrl: PropTypes.string,
  name: PropTypes.string,
  title: PropTypes.string,
});

export const reportPropType = PropTypes.shape({
  forecast: forecasterPropTypes,
  body: PropTypes.string,
  timestamp: PropTypes.number,
});
