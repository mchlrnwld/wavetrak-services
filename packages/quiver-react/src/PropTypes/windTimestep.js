import PropTypes from 'prop-types';

export default PropTypes.shape({
  timestamp: PropTypes.number,
  direction: PropTypes.number,
  speed: PropTypes.number,
});
