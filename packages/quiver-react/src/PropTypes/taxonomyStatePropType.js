import PropTypes from 'prop-types';

export default PropTypes.shape({
  loading: PropTypes.bool,
  error: PropTypes.bool,
  taxonomy: PropTypes.shape({
    parents: PropTypes.arrayOf(
      PropTypes.shape({
        _id: PropTypes.string,
      })
    ),
    activeNode: PropTypes.shape(),
    children: PropTypes.oneOf([
      PropTypes.element,
      PropTypes.arrayOf(PropTypes.element),
    ]),
  }),
});
