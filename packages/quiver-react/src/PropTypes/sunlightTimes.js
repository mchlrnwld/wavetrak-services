import PropTypes from 'prop-types';

export const sunlightTimesPropType = PropTypes.shape({
  utcOffset: PropTypes.number,
  loading: PropTypes.bool,
  days: PropTypes.arrayOf(
    PropTypes.shape({
      dawn: PropTypes.number.isRequired,
      dawnUTCOffset: PropTypes.number.isRequired,
      sunrise: PropTypes.number.isRequired,
      sunriseUTCOffset: PropTypes.number.isRequired,
      sunset: PropTypes.number.isRequired,
      sunsetUTCOffset: PropTypes.number.isRequired,
      dusk: PropTypes.number.isRequired,
      duskUTCOffset: PropTypes.number.isRequired,
    })
  ),
});

/**
 * @typedef {object} SunlightDay
 * @property {number} dawn
 * @property {number} dawnUTCOffset
 * @property {number} sunrise
 * @property {number} sunriseUTCOffset
 * @property {number} sunset
 * @property {number} sunsetUTCOffset
 * @property {number} dusk
 * @property {number} duskUTCOffset
 */

/**
 * @typedef {object} SunlightTimes
 * @property {number} utcOffset
 * @property {boolean} loading
 * @property {SunlightDay[]} days
 */
