import PropTypes from 'prop-types';

/**
 * @typedef {object} AutomatedRating
 * @property {number} timestamp
 * @property {string} rating
 * @property {number} utcOffset
 */

/**
 * @typedef {object} AutomatedRatings
 * @property {string} [error]
 * @property {boolean} loading
 * @property {{ lat: number, lon: number }} [location]
 * @property {AutomatedRating[][]} [days]
 * @property {AutomatedRating[][]} [hourly]
 */

const AutomatedRatingPropType = PropTypes.shape({
  timestamp: PropTypes.number,
  utcOffset: PropTypes.number,
  rating: PropTypes.string,
});

export const automatedRatingsPropType = PropTypes.shape({
  error: PropTypes.string,
  loading: PropTypes.bool,
  location: PropTypes.shape({
    lat: PropTypes.number,
    lon: PropTypes.number,
  }),
  days: PropTypes.arrayOf(PropTypes.arrayOf(AutomatedRatingPropType)),
  hourly: PropTypes.arrayOf(PropTypes.arrayOf(AutomatedRatingPropType)),
});

export default AutomatedRatingPropType;
