import PropTypes from 'prop-types';

export default PropTypes.shape({
  mobile: PropTypes.bool,
  width: PropTypes.number,
  height: PropTypes.number,
});
