import PropTypes from 'prop-types';

const taxonomyNodePropType = PropTypes.shape({
  _id: PropTypes.string,
  name: PropTypes.string,
  geonameId: PropTypes.number,
  type: PropTypes.string,
  associated: PropTypes.shape({
    links: PropTypes.arrayOf(
      PropTypes.shape({
        key: PropTypes.string,
        href: PropTypes.string,
      })
    ),
  }),
  depth: PropTypes.number,
});

export default taxonomyNodePropType;
