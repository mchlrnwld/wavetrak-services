import PropTypes from 'prop-types';

export default PropTypes.arrayOf(
  PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    thumbnail: PropTypes.string,
    rank: PropTypes.number,
    conditions: PropTypes.shape({
      human: PropTypes.bool,
      value: PropTypes.string,
    }),
    wind: PropTypes.shape({
      speed: PropTypes.number,
      direction: PropTypes.number,
    }),
    waveHeight: PropTypes.shape({
      human: PropTypes.bool,
      min: PropTypes.number,
      max: PropTypes.number,
      occasional: PropTypes.number,
      humanRelation: PropTypes.string,
    }),
    tide: PropTypes.shape({
      previous: PropTypes.shape({
        type: PropTypes.string,
        height: PropTypes.number,
        timestamp: PropTypes.number,
        utcOffset: PropTypes.number,
      }),
      next: PropTypes.shape({
        type: PropTypes.string,
        height: PropTypes.number,
        timestamp: PropTypes.number,
        utcOffset: PropTypes.number,
      }),
    }),
    cameras: PropTypes.arrayOf(
      PropTypes.shape({
        _id: PropTypes.string,
        title: PropTypes.string,
        streamUrl: PropTypes.string,
        stillUrl: PropTypes.string,
        rewindBaseUrl: PropTypes.string,
        status: PropTypes.shape({
          isDown: PropTypes.bool,
          message: PropTypes.string,
        }),
      })
    ),
  })
);
