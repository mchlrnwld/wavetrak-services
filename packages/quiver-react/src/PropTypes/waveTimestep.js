import PropTypes from 'prop-types';
import swellPropType from './swell';
import surfPropType from './surf';

export default PropTypes.shape({
  timestamp: PropTypes.number,
  surf: surfPropType,
  swells: PropTypes.arrayOf(swellPropType),
});
