import PropTypes from 'prop-types';

export default PropTypes.shape({
  timestamp: PropTypes.number,
  temperature: PropTypes.number.isRequired,
  condition: PropTypes.string.isRequired,
});
