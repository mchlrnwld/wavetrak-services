import PropTypes from 'prop-types';

export default PropTypes.shape({
  height: PropTypes.number.isRequired,
  direction: PropTypes.number.isRequired,
  period: PropTypes.number.isRequired,
  pixels: PropTypes.number,
  color: PropTypes.string,
  index: PropTypes.number,
});
