import PropTypes from 'prop-types';

export default PropTypes.shape({
  category: PropTypes.string,
  linkLocation: PropTypes.string,
});
