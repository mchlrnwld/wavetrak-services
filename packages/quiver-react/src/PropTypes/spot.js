import PropTypes from 'prop-types';
import { camerasPropType } from './cameras';

export default PropTypes.shape({
  _id: PropTypes.string,
  name: PropTypes.string,
  lat: PropTypes.number,
  lon: PropTypes.number,
  cameras: camerasPropType,
  thumbnail: PropTypes.oneOfType([
    PropTypes.shape({ 300: PropTypes.string }), // eslint-disable-line quote-props
    PropTypes.string,
  ]),
});
