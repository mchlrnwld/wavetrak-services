import PropTypes from 'prop-types';
import navigationSettingsPropType from './navigationSettingsPropType';

export default PropTypes.shape({
  units: PropTypes.shape({
    temperature: PropTypes.string,
    windSpeed: PropTypes.string,
    tideHeight: PropTypes.string,
    swellHeight: PropTypes.string,
    surfHeight: PropTypes.string,
  }),
  navigation: navigationSettingsPropType,
});
