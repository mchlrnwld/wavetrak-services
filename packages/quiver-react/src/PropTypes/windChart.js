import PropTypes from 'prop-types';
import unitsPropType from './units';
import windTimestepPropType from './windTimestep';

export default PropTypes.shape({
  error: PropTypes.shape(),
  loading: PropTypes.bool,
  units: unitsPropType,
  days: PropTypes.arrayOf(PropTypes.arrayOf(windTimestepPropType)),
});
