import PropTypes from 'prop-types';

const userPropType = PropTypes.shape({
  email: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  _id: PropTypes.string,
});

export default userPropType;
