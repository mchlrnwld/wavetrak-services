import PropTypes from 'prop-types';

export default PropTypes.shape({
  tideHeight: PropTypes.string,
  waveHeight: PropTypes.string,
  windSpeed: PropTypes.string,
  speed: PropTypes.string,
  distance: PropTypes.string,
});

/**
 * @typedef {object} Units
 * @property {'LOLA' | 'LOTUS'} model
 * @property {'FT' | 'M' | 'HI'} waveHeight
 * @property {'FT' | 'M'} swellHeight
 * @property {'F' | 'C'} temperature
 * @property {'FT' | 'M'} tideHeight
 * @property {'KTS' | 'KPH' | 'MPH'} windSpeed
 */
