import PropTypes from 'prop-types';
import unitsPropType from './units';

export default PropTypes.shape({
  error: PropTypes.shape(),
  loading: PropTypes.bool,
  units: unitsPropType,
  days: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.shape)),
});

/**
 * @typedef {object} TidePoint
 * @property {number} height
 * @property {number} timestamp
 * @property {number} utcOffset
 * @property {"LOW" | "HIGH" | "NORMAL"} type
 */

/**
 * @typedef {object} TideLocation
 * @property {number} lat
 * @property {number} lon
 * @property {number} max
 * @property {number} mean
 * @property {number} min
 * @property {string} name
 */

/**
 * @typedef {object} Tide
 * @property {TidePoint[][]} days
 * @property {string} error
 * @property {boolean} loading
 * @property {TideLocation} tideLocation
 * @property {import('./units').Units} units
 * @property {number} utcOffset
 */
