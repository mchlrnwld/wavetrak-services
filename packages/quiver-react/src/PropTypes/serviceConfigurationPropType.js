import PropTypes from 'prop-types';

const serviceConfigurationPropType = PropTypes.shape({
  serviceUrl: PropTypes.string,
});

export default serviceConfigurationPropType;
