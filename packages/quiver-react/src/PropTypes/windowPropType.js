import PropTypes from 'prop-types';

export default PropTypes.shape({
  location: PropTypes.shape({
    href: PropTypes.string,
    assign: PropTypes.func,
  }),
  analytics: PropTypes.shape({
    track: PropTypes.func,
  }),
});
