import PropTypes from 'prop-types';

export default PropTypes.shape({
  condition: PropTypes.number,
  occasionalHeight: PropTypes.number,
  rating: PropTypes.string,
  humanRelation: PropTypes.string,
  maxHeight: PropTypes.number,
  minHeight: PropTypes.number,
  plus: PropTypes.boolen,
});
