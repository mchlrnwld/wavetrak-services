import React from 'react';
import PropTypes from 'prop-types';
import LinearScale from './LinearScale';
import MultiScale from './MultiScale';

const SpotDescriptor = ({ kind, value, scale, options, description }) => {
  const linearScale = options.length === 2;
  const percentage = linearScale ? (scale / 10) * 100 : null;

  return (
    <div
      className={`quiver-spot-descriptor ${
        linearScale
          ? 'quiver-spot-descriptor--linear'
          : 'quiver-spot-descriptor--multi'
      }`}
    >
      <h3 className="quiver-spot-descriptor__kind">{kind}</h3>
      <h2 className="quiver-spot-descriptor__value">{value}</h2>
      <div className="quiver-spot-descriptor__scale">
        {linearScale ? (
          <LinearScale percentage={percentage} />
        ) : (
          <MultiScale options={options} />
        )}
        <div className="quiver-spot-descriptor__scale__legend">
          {options.map((option) => {
            const name = linearScale ? option : option.name;

            return (
              <span
                className="quiver-spot-descriptor__scale__legend__item"
                key={name}
              >
                {name}
              </span>
            );
          })}
        </div>
      </div>
      <p className="quiver-spot-descriptor__description">{description}</p>
    </div>
  );
};

SpotDescriptor.propTypes = {
  kind: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  scale: PropTypes.number,
  options: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.shape({
        name: PropTypes.string,
        value: PropTypes.bool,
      }),
      PropTypes.string,
    ])
  ).isRequired,
  description: PropTypes.string.isRequired,
};

SpotDescriptor.defaultProps = {
  scale: null,
};

export default SpotDescriptor;
