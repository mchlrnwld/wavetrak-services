import React from 'react';
import { storiesOf } from '@storybook/react';
import SpotDescriptor from './SpotDescriptor';

storiesOf('SpotDescriptor', module)
  .add('Linear scale', () => (
    <SpotDescriptor
      kind="Local Vibe"
      value="Welcoming"
      scale={3}
      options={['Welcoming', 'Intimidating']}
      description="Can be heavy at the pier, a few grumps at Cliffs, otherwise mild."
    />
  ))
  .add('Multi Scale - full', () => (
    <SpotDescriptor
      kind="Ability Level"
      value="All Abilties"
      options={[
        { name: 'Beg', value: true },
        { name: 'Int', value: true },
        { name: 'Adv', value: true },
      ]}
      description="Beginner to advanced, but generally above average around the pier."
    />
  ))
  .add('Multi Scale - partially full', () => (
    <SpotDescriptor
      kind="Ability Level"
      value="Intermediate - Advanced"
      options={[
        { name: 'Beg', value: false },
        { name: 'Int', value: true },
        { name: 'Adv', value: true },
      ]}
      description="Intermediate to advanced - above average around the pier."
    />
  ));
