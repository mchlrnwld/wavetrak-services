import React from 'react';
import PropTypes from 'prop-types';

const MultiScale = ({ options }) => (
  <div className="quiver-spot-descriptor__scale__bar">
    {options.map(({ name, value }) => (
      /* eslint-disable-next-line react/self-closing-comp */
      <div
        key={`scale-item-${name}`}
        className={`
          quiver-spot-descriptor__scale__bar__item
          ${!value ? 'quiver-spot-descriptor__scale__bar__item--empty' : ''}
        `}
      ></div>
    ))}
  </div>
);

MultiScale.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      value: PropTypes.bool,
    })
  ).isRequired,
};

export default MultiScale;
