import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import SpotDescriptor from './SpotDescriptor';
import LinearScale from './LinearScale';
import MultiScale from './MultiScale';

describe('SpotDescriptor', () => {
  it('should render a linear scale for a component with 2 options', () => {
    const wrapper = shallow(
      <SpotDescriptor
        kind="Local Vibe"
        value="Welcoming"
        scale={35}
        options={['Welcoming', 'Intimidating']}
        description="Can be heavy at the pier."
      />
    );

    expect(wrapper.find('.quiver-spot-descriptor')).to.have.length(1);
    expect(wrapper.find('.quiver-spot-descriptor--linear')).to.have.length(1);
    expect(wrapper.find(LinearScale)).to.have.length(1);
  });

  it('should render a multi scale component when there are > 2 options', () => {
    const wrapper = mount(
      <SpotDescriptor
        kind="Ability"
        value="All Levels"
        options={[
          { name: 'Beg', value: true },
          { name: 'Int', value: true },
          { name: 'Adv', value: true },
        ]}
        description="Can be ridden at all levels"
      />
    );

    expect(wrapper.find('.quiver-spot-descriptor')).to.have.length(1);
    expect(wrapper.find('.quiver-spot-descriptor--multi')).to.have.length(1);
    expect(
      wrapper.find('.quiver-spot-descriptor__scale__bar__item')
    ).to.have.length(3);
    expect(
      wrapper.find('.quiver-spot-descriptor__scale__bar__item--empty')
    ).to.have.length(0);
    expect(wrapper.find(MultiScale)).to.have.length(1);
  });

  it('should render a multi coloured scale component when passed false value(s)', () => {
    const wrapper = mount(
      <SpotDescriptor
        kind="Ability"
        value="Advanced"
        options={[
          { name: 'Beg', value: false },
          { name: 'Int', value: false },
          { name: 'Adv', value: true },
        ]}
        description="Advanced surfers only here"
      />
    );

    expect(wrapper.find('.quiver-spot-descriptor')).to.have.length(1);
    expect(wrapper.find('.quiver-spot-descriptor--multi')).to.have.length(1);
    expect(
      wrapper.find('.quiver-spot-descriptor__scale__bar__item')
    ).to.have.length(3);
    expect(
      wrapper.find('.quiver-spot-descriptor__scale__bar__item--empty')
    ).to.have.length(2);
    expect(wrapper.find(MultiScale)).to.have.length(1);
  });
});
