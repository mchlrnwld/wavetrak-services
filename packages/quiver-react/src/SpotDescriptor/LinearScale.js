import React from 'react';
import PropTypes from 'prop-types';
import { color } from '@surfline/quiver-themes';

const LinearScale = ({ percentage }) => (
  /* eslint-disable-next-line react/self-closing-comp */
  <div
    className="quiver-spot-descriptor__scale__bar"
    style={{
      background: `linear-gradient(
        to right,
        ${color('dark-blue', 10)} ${percentage}%,
        ${color('blue-gray', 15)} 0
      )`,
    }}
  ></div>
);

LinearScale.propTypes = {
  percentage: PropTypes.number.isRequired,
};

export default LinearScale;
