import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import Chevron from '../Chevron';
import { useHover } from '../hooks/useHover';
import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

const menuOptionClass = (selected) =>
  classNames({
    'quiver-hover-menu__menu-item': true,
    'quiver-hover-menu__menu-item--selected': selected,
  });

const getClass = (isHovering, useSelectOnMobile) =>
  classNames({
    'quiver-hover-menu': true,
    'quiver-hover-menu--hover': isHovering,
    'quiver-hover-menu--mobile-select': useSelectOnMobile,
  });

const HoverMenu = ({
  onClick,
  Icon,
  onClickMenuItem,
  menuTitle,
  menuOptions,
  label,
  disabled,
  useSelectOnMobile,
  selectDescription,
  defaultValue,
}) => {
  const { hoverProps, isHovering } = useHover({
    className: '',
    bubble: false,
  });

  const handleSelect = (event) => {
    const { value } = event.target;
    const option = menuOptions.find(({ name }) => name === value);
    onClickMenuItem(option.value);
  };

  return (
    <div
      type="button"
      className={getClass(isHovering, useSelectOnMobile)}
      aria-haspopup={!disabled}
      aria-expanded={disabled ? false : isHovering}
      disabled={disabled}
      {...hoverProps}
      {...(!!onClick && { ...createAccessibleOnClick(onClick, BTN, false) })}
    >
      <div className="quiver-hover-menu-target">
        {label && <h6 className="quiver-hover-menu-label">{label}</h6>}
        {Icon ? (
          <div className="quiver-hover-menu-icon">
            <Icon isHovering={isHovering} />
          </div>
        ) : (
          <Chevron direction="down" />
        )}
        {useSelectOnMobile && (
          // eslint-disable-next-line jsx-a11y/no-onchange
          <select
            className="quiver-hover-menu-select"
            id={`quiver-hover-menu-select--${label}`}
            onChange={handleSelect}
            value={defaultValue}
          >
            {selectDescription && <option disabled>{selectDescription}</option>}
            {menuOptions.map(({ selected, name }) => (
              <option value={name} key={name} disabled={selected}>
                {name}
              </option>
            ))}
          </select>
        )}
      </div>
      <ul role="menu" className="quiver-hover-menu__menu">
        {menuTitle && (
          <h1 className="quiver-hover-menu__menu-title">{menuTitle}</h1>
        )}
        {menuOptions.map(({ name, selected, value }) => (
          <li key={name} className="quiver-hover-menu__menu-item-container">
            <button
              type="button"
              role="menuitem"
              className={menuOptionClass(selected)}
              disabled={!!selected}
              {...createAccessibleOnClick(
                () => onClickMenuItem(value),
                BTN,
                false
              )}
            >
              {name}
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
};

HoverMenu.propTypes = {
  onClick: PropTypes.func,
  Icon: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
  onClickMenuItem: PropTypes.func.isRequired,
  menuTitle: PropTypes.string,
  menuOptions: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      // eslint-disable-next-line react/forbid-prop-types
      value: PropTypes.any.isRequired,
      selected: PropTypes.bool,
    }).isRequired
  ).isRequired,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  useSelectOnMobile: PropTypes.bool,
  defaultValue: PropTypes.string,
  selectDescription: PropTypes.string,
};

HoverMenu.defaultProps = {
  onClick: null,
  menuTitle: null,
  Icon: null,
  label: '',
  disabled: false,
  useSelectOnMobile: false,
  defaultValue: '',
  selectDescription: '',
};

export default HoverMenu;
