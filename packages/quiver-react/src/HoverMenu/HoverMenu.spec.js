import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import HoverMenu from './HoverMenu';

describe('components / HoverMenu', () => {
  const defaultProps = {
    onClick: sinon.stub(),
    onClickMenuItem: sinon.stub(),
    Icon: () => <div className="test-icon" />,
    menuTitle: 'Test',
    menuOptions: [
      { value: '1', name: 1, selected: false },
      { value: '2', name: 2, selected: false },
    ],
  };

  beforeEach(() => {});

  afterEach(() => {
    defaultProps.onClick.reset();
    defaultProps.onClickMenuItem.reset();
  });

  it('should render the hover menu', () => {
    const props = {
      ...defaultProps,
    };

    const wrapper = mount(<HoverMenu {...props} />);
    expect(wrapper).to.have.length(1);
  });

  it('should render the hover menu with the given icon', () => {
    const props = {
      ...defaultProps,
    };

    const wrapper = mount(<HoverMenu {...props} />);
    const iconWrapper = wrapper.find(props.Icon);
    expect(iconWrapper).to.have.length(1);
  });

  it('should trigger a click when clicking the menu', () => {
    const props = {
      ...defaultProps,
    };

    const wrapper = mount(<HoverMenu {...props} />);

    wrapper.simulate('click');

    expect(props.onClick).to.have.been.calledOnce();
  });

  it('should trigger a click when clicking the menu items', () => {
    const props = {
      ...defaultProps,
    };

    const wrapper = mount(<HoverMenu {...props} />);

    wrapper.find('.quiver-hover-menu__menu-item').at(0).simulate('click');
    expect(props.onClickMenuItem).to.have.been.calledOnceWithExactly('1');

    wrapper.find('.quiver-hover-menu__menu-item').at(1).simulate('click');
    expect(props.onClickMenuItem).to.have.been.calledTwice();
    expect(props.onClickMenuItem.getCall(0)).to.have.been.calledWithExactly(
      '1'
    );
    expect(props.onClickMenuItem.getCall(1)).to.have.been.calledWithExactly(
      '2'
    );
  });

  it('should disable "selected" menu items', () => {
    const props = {
      ...defaultProps,
      useSelectOnMobile: true,
    };

    // Set the first menu item to selected
    props.menuOptions[0].selected = true;

    const wrapper = mount(<HoverMenu {...props} useSelectOnMobile />);

    wrapper.find('.quiver-hover-menu__menu-item--selected').simulate('click');
    expect(props.onClickMenuItem).to.not.have.been.called();
    expect(
      wrapper.find('option').filterWhere((option) => option.prop('disabled'))
    ).to.have.length(1);
  });

  it('should render the menu title', () => {
    const props = {
      ...defaultProps,
    };

    const wrapper = mount(<HoverMenu {...props} />);

    const titleWrapper = wrapper.find('.quiver-hover-menu__menu-title');
    expect(titleWrapper).to.have.length(1);
    expect(titleWrapper.text()).to.be.equal(props.menuTitle);
  });
});
