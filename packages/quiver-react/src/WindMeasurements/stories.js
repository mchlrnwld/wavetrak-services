import React from 'react';
import WindMeasurements from './WindMeasurements';

export default {
  title: 'WindMeasurements',
};

export const Default = () => (
  <WindMeasurements direction={220} gust={5} speed={4} units="MPH" />
);
