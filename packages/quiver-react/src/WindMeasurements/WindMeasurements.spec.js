import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import WindMeasurements from './WindMeasurements';

describe('components / WindMeasurements', () => {
  it('renders appropriate wind measurements and units', () => {
    const wrapper = shallow(
      <WindMeasurements speed={14.2} direction={66.7} units="KTS" gust={15.6} />
    );
    expect(wrapper.text()).to.equal('14KTSENE (67°)16kts Gusts');
  });
});
