/** @format */

import PropTypes from 'prop-types';
import React from 'react';
import { mapDegreesCardinal } from '@surfline/web-common';

const WindMeasurements = ({ speed, direction, units, gust }) => (
  <>
    <div className="quiver-wind-measurements">
      <div className="quiver-wind-speed">{Math.round(speed)}</div>
      <span className="quiver-wind-measurements__units">{units}</span>
      <div className="quiver-wind-direction">
        {mapDegreesCardinal(direction)} ({Math.round(direction)}°)
      </div>
    </div>
    <div className="quiver-wind-gusts">{`${Math.round(
      gust
    )}${units?.toLowerCase()} Gusts`}</div>
  </>
);

WindMeasurements.propTypes = {
  direction: PropTypes.number.isRequired,
  speed: PropTypes.number.isRequired,
  units: PropTypes.string.isRequired,
  gust: PropTypes.number.isRequired,
};

export default WindMeasurements;
