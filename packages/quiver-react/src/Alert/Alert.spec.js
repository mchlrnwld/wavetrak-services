import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Alert from './Alert';

describe('Alert', () => {
  it('should render children with appropriate alert type class name', () => {
    const defaultWrapper = shallow(
      <Alert>Heads up! This alert needs your attention in a good way.</Alert>
    );
    const successWrapper = shallow(
      <Alert type="success">Stoked! This alert means success.</Alert>
    );
    const warningWrapper = shallow(
      <Alert type="warning">Warning! Check yourself.</Alert>
    );
    const errorWrapper = shallow(
      <Alert type="error">Brah! Change some things and try again.</Alert>
    );

    const defaultContainer = defaultWrapper.find('div');
    const successContainer = successWrapper.find('div');
    const warningContainer = warningWrapper.find('div');
    const errorContainer = errorWrapper.find('div');

    expect(defaultContainer.prop('className')).to.equal('quiver-alert');
    expect(successContainer.prop('className')).to.equal(
      'quiver-alert quiver-alert--success'
    );
    expect(warningContainer.prop('className')).to.equal(
      'quiver-alert quiver-alert--warning'
    );
    expect(errorContainer.prop('className')).to.equal(
      'quiver-alert quiver-alert--error'
    );
  });

  it('should pass custom style object into children', () => {
    const style = { fontFamily: 'container' };
    const wrapper = shallow(<Alert style={style}>Custom styles!</Alert>);
    const container = wrapper.find('div');
    expect(container.prop('style')).to.equal(style);
  });
});
