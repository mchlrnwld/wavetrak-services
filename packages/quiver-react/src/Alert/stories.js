import React from 'react';
import Alert from './index';

export default {
  title: 'Alert',
};

export const Default = () => (
  <Alert>Heads up! This alert needs your attention in a good way.</Alert>
);

Default.story = {
  name: 'default',
};

export const Success = () => (
  <Alert type="success">Stoked! This alert means success.</Alert>
);

Success.story = {
  name: 'success',
};

export const Warning = () => (
  <Alert type="warning">Warning! Check yourself.</Alert>
);

Warning.story = {
  name: 'warning',
};

export const Error = () => (
  <Alert type="error">Brah! Change some things and try again.</Alert>
);

Error.story = {
  name: 'error',
};

export const CustomStyles = () => (
  <Alert style={{ backgroundColor: 'silver', color: 'gray' }}>
    Custom styles alert.
  </Alert>
);

CustomStyles.story = {
  name: 'custom styles',
};
