import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';

const className = (type) => {
  const classObject = {};
  classObject['quiver-alert'] = true;
  classObject[`quiver-alert--${type}`] = !!type;
  return classNames(classObject);
};

const Alert = ({ children, type, style }) => (
  <div className={className(type)} style={style}>
    {children}
  </div>
);

Alert.propTypes = {
  type: PropTypes.string,
  children: PropTypes.node,
  style: PropTypes.shape(),
};

Alert.defaultProps = {
  type: '',
  children: null,
  style: {},
};

export default Alert;
