import { slugify } from '@surfline/web-common';

const forecasterNames = [
  'Kevin Wallis',
  'Kurt Korte',
  'Jonathan Warren',
  'Schaler Perry',
  'Mike Watson',
  'Chris Borg',
  'Charlie Hutcherson',
  'Keaton Browning',
  'Rob Mitstifer',
  'Matt Kibby',
  'Tim Kent',
];

export default (urlBase, forecasterName) =>
  forecasterNames.some((name) => name === forecasterName)
    ? `${urlBase}lp/forecasters/${slugify(forecasterName)}`
    : `${urlBase}lp/forecasters`;
