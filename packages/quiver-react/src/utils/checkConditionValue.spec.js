import { expect } from 'chai';
import { describe, it } from 'mocha';
import checkConditionValue from './checkConditionValue';

describe('utils / checkConditionValue', () => {
  const conditions = {
    human: true,
    value: 'FAIR',
    expired: false,
    sortableCondition: 4,
  };
  const conditionsNoValue = { human: false, value: null, expired: false };

  it('should return LOTUS if there is no value for conditions', () => {
    const conditionsValue = checkConditionValue(conditionsNoValue.value);

    expect(conditionsValue).to.equal('LOTUS');
  });

  it('should return condition value if there is a value', () => {
    const conditionsValue = checkConditionValue(conditions.value);

    expect(conditionsValue).to.equal('FAIR');
  });
});
