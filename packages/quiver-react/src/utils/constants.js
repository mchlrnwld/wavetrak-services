export const TREATMENT_WRAPPER_CLASS = 'quiver-treatment-wrapper';
export const TREATMENT_WRAPPER_LOADING_CLASS = `${TREATMENT_WRAPPER_CLASS}--loading`;
export const PLAYED_WEBCAM = 'Played Webcam';
export const STOPPED_WEBCAM = 'Stopped Webcam';

export const MIN_PASSWORD_LENGTH = 6;
export const MAX_PASSWORD_LENGTH = 50;
