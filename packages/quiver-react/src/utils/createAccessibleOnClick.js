export const LINK = 'link';
export const BTN = 'button';
export const ENTER_KEY = 'Enter';
export const SPACE_KEY = ' ';
export const SPACE_KEY_CODE = 32;
export const ENTER_KEY_CODE = 13;
/**
 * We want the site to be as useable as possible for regular users, but also
 * for anyone with disabilities or screen readers. To ensure this we use the
 * JSX A11y library which gives us lint rules to ensure that we make the site
 * accesible. Creating `keydown` events for every `onClick` can be a pain
 * so this factory function will help streamline creating accessible click events
 *
 * @param {Function} handlerFn The function to be called for both `onClick` and `onKeyDown`
 * @param {String} [type="LINK"] The type of onClick ('LINK' or 'BUTTON'). Buttons will listen
 * for the `SPACE` and `ENTER` keys (<button> should fire on space key by default) and links for the `ENTER` key
 */
const createAccessibleOnClick = (handlerFn, type = LINK, bubble = true) => ({
  role: type,
  onClick: (event) => {
    if (!bubble) {
      event.stopPropagation();
    }
    handlerFn(event);
  },
  onTouchEnd: (event) => {
    if (!bubble) {
      event.stopPropagation();
    }
    handlerFn(event);
  },
  tabIndex: 0,
  onKeyDown: (event) => {
    const isButton = type === BTN;
    const keys = [ENTER_KEY, isButton && SPACE_KEY].filter(Boolean);
    const keyCodes = [ENTER_KEY_CODE, isButton && SPACE_KEY_CODE].filter(
      Boolean
    );

    if (keyCodes.includes(event?.keycode) || keys.includes(event?.key)) {
      if (!bubble) {
        event.stopPropagation();
      }
      handlerFn(event);
    }
  },
});

export default createAccessibleOnClick;
