/* istanbul ignore next */
const checkConditionValue = (condition) => {
  if (!condition) return 'LOTUS';
  if (condition === 'NONE') return 'LOTUS';
  return condition;
};

export default checkConditionValue;
