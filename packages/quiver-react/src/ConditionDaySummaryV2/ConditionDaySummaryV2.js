import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import {
  occasionalUnits,
  conditionEnumToText,
  conditionClassModifier,
  trimHumanRelation,
} from '@surfline/web-common';

import SurfHeight from '../SurfHeight';
import conditionReportPropType from '../PropTypes/conditionReport';
import unitsPropType from '../PropTypes/units';
import getUnitType from '../helpers/getUnitType';
import applyConditionVariation from '../helpers/applyConditionVariation';

const conditionClassNameV2 = (condition) =>
  classNames({
    'quiver-condition-day-summary-v2__condition': true,
    [`quiver-condition-day-summary-v2__condition--${conditionClassModifier(
      applyConditionVariation(condition)
    )}`]: true,
  });

const conditionBodyClassNameV2 = (obscured) =>
  classNames({
    'quiver-condition-day-summary-v2__body': true,
    'quiver-condition-day-summary-v2__body--blurred': obscured,
  });

const ConditionDaySummaryV2 = ({
  data,
  units,
  showAMPM,
  obscured,
  lastUpdate,
}) => (
  <div className="quiver-condition-day-summary-v2">
    <div className={conditionBodyClassNameV2(obscured)}>
      {showAMPM ? (
        <div className="quiver-condition-day-summary-v2__ampm">
          <div className="quiver-condition-day-summary-v2__conditions">
            <div className={conditionClassNameV2(data.am.rating)}>
              {data.am.rating ? (
                <span className="quiver-condition-day-summary-v2__condition__rating">
                  {conditionEnumToText(applyConditionVariation(data.am.rating))}
                </span>
              ) : null}
              <span className="quiver-condition-day-summary-v2__condition__ampm">
                AM
              </span>
            </div>
            <div className={conditionClassNameV2(data.pm.rating)}>
              <span className="quiver-condition-day-summary-v2__condition__ampm">
                PM
              </span>
              {data.pm.rating ? (
                <span className="quiver-condition-day-summary-v2__condition__rating">
                  {conditionEnumToText(applyConditionVariation(data.pm.rating))}
                </span>
              ) : null}
            </div>
          </div>
          <div className="quiver-condition-day-summary-v2__wave">
            <div className="quiver-condition-day-summary-v2__wave__ampm">
              <SurfHeight
                min={data.am.minHeight}
                max={data.am.maxHeight}
                units={units.waveHeight}
                plus={data.am.plus}
              />
              <div className="quiver-condition-day-summary-v2__wave__ampm--human-relation">
                {trimHumanRelation(data.am.humanRelation)}
                {occasionalUnits(
                  data.am.occasionalHeight,
                  getUnitType(units.waveHeight)
                )}
              </div>
            </div>
            <div className="quiver-condition-day-summary-v2__wave__ampm">
              <SurfHeight
                min={data.pm.minHeight}
                max={data.pm.maxHeight}
                units={units.waveHeight}
                plus={data.pm.plus}
              />
              <div className="quiver-condition-day-summary-v2__wave__ampm--human-relation">
                {trimHumanRelation(data.pm.humanRelation)}
                {occasionalUnits(
                  data.pm.occasionalHeight,
                  getUnitType(units.waveHeight)
                )}
              </div>
            </div>
          </div>
        </div>
      ) : null}
      <div className="quiver-condition-day-summary-v2__observation">
        {data.forecaster ? (
          <div className="quiver-condition-day-summary-v2__observation-avatar">
            <img src={`${data.forecaster.avatar}&s=18`} alt="Surfline Avatar" />
            <div className="quiver-condition-day-summary-v2__observation-avatar-tooltip">
              {`Updated ${lastUpdate} by ${data.forecaster.name}`}
            </div>
          </div>
        ) : null}
        <span className="quiver-condition-day-summary-v2__observation-text">
          {data.observation}
        </span>
      </div>
    </div>
  </div>
);

ConditionDaySummaryV2.propTypes = {
  data: PropTypes.shape({
    timestamp: PropTypes.number,
    forecaster: PropTypes.shape({
      name: PropTypes.string,
      iconUrl: PropTypes.string,
      avatar: PropTypes.string,
    }),
    am: conditionReportPropType,
    pm: conditionReportPropType,
    observation: PropTypes.string,
  }).isRequired,
  units: unitsPropType.isRequired,
  showAMPM: PropTypes.bool,
  obscured: PropTypes.bool.isRequired,
  lastUpdate: PropTypes.string,
};

ConditionDaySummaryV2.defaultProps = {
  showAMPM: false,
  lastUpdate: null,
};

export default ConditionDaySummaryV2;
