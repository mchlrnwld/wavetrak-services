import React from 'react';

import ConditionDaySummaryV2 from './ConditionDaySummaryV2';

const report = {
  condition: 1,
  occasionalHeight: 5,
  humanRelation: 'Waist to shoulder high',
  maxHeight: 6,
  minHeight: 3,
};

const props = {
  data: {
    timestamp: 0,
    forecaster: {
      name: 'Jeremy',
      iconUrl:
        'http://gravatar.com/avatar/ea1e9a0c570c61d61dec3cf6ea26a85e?d=mm',
      avatar:
        'http://gravatar.com/avatar/ea1e9a0c570c61d61dec3cf6ea26a85e?d=mm',
    },
    am: {
      ...report,
      rating: 'POOR_TO_FAIR',
    },
    pm: {
      ...report,
      rating: 'FAIR_TO_GOOD',
    },
    observation:
      'Small short period wind waves from the N. Use extreme caution. Gale warnign with dangerous seas. Winds from the WNW.',
  },
  utcOffset: -10,
  units: { waveHeight: 'FT' },
};

const lolaProps = {
  ...props,
  data: {
    ...props.data,
    am: {
      ...props.data.am,
      rating: 'NONE',
    },
    pm: {
      ...props.data.am,
      rating: 'LOLA',
    },
  },
};

export default {
  title: 'ConditionDaySummaryV2',
};

export const Default = () => <ConditionDaySummaryV2 {...props} />;

Default.story = {
  name: 'default',
};

export const ConditionVariationV2Lola = () => (
  <ConditionDaySummaryV2 {...lolaProps} showAMPM />
);

ConditionVariationV2Lola.story = {
  name: 'conditionVariation V2 LOLA',
};

export const ConditionVariationV2 = () => (
  <ConditionDaySummaryV2 {...props} showAMPM />
);

ConditionVariationV2.story = {
  name: 'conditionVariation V2 ',
};
