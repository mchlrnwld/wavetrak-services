import PropTypes from 'prop-types';
import React from 'react';
import { loadAdConfig } from '@surfline/web-common';
import GoogleDFP from '../GoogleDFP';
import WaterTemperature from '../WaterTemp';
import Weather from '../Weather';
import { waterPropTypes, weatherPropTypes } from '../PropTypes/readings';

const adConfig = (sst, adTargets, entitlements, isUser) =>
  loadAdConfig(
    'spotReportWetsuit',
    [...adTargets, ['sst', sst], ['className', 'wetsuit-recommender--v2']],
    entitlements,
    isUser
  );

const WetsuitRecommender = ({
  spotName,
  weather,
  waterTemp,
  adTargets,
  entitlements,
  isUser,
  sst,
  weatherIconPath,
  units,
}) => (
  <div className="quiver-wetsuit-recommender">
    <div className="quiver-wetsuit-recommender__conditions">
      <WaterTemperature
        weatherIconPath={weatherIconPath}
        spotName={spotName}
        waterTemp={waterTemp}
        units={units}
      />
      <Weather
        weatherIconPath={weatherIconPath}
        spotName={spotName}
        weather={weather}
        units={units}
      />
    </div>
    <div className="quiver-wetsuit-recommender__ad">
      <div>
        {/* eslint-disable-next-line react/jsx-curly-brace-presence */}
        <h3>{"Best Suit for Today's Temp"}</h3>
        <GoogleDFP adConfig={adConfig(sst, adTargets, entitlements, isUser)} />
      </div>
    </div>
  </div>
);

WetsuitRecommender.propTypes = {
  spotName: PropTypes.string.isRequired,
  weather: weatherPropTypes.isRequired,
  waterTemp: waterPropTypes.isRequired,
  adTargets: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
  entitlements: PropTypes.arrayOf(PropTypes.string),
  isUser: PropTypes.bool,
  sst: PropTypes.string.isRequired,
  weatherIconPath: PropTypes.string.isRequired,
  units: PropTypes.string.isRequired,
};

WetsuitRecommender.defaultProps = {
  adTargets: [],
  entitlements: [],
  isUser: false,
};

export default WetsuitRecommender;
