import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import { loadAdConfig } from '@surfline/web-common';
import GoogleDFP from '../GoogleDFP';
import WetsuitRecommender from './WetsuitRecommender';
import Weather from '../Weather';
import WaterTemperature from '../WaterTemp';

describe('components / WetsuitRecommender', () => {
  let wrapper;
  const props = {
    spotName: 'Pipeline',
    weather: {
      temperature: 71,
      condition: 'NIGHT_CLEAR_NO_RAIN',
    },
    waterTemp: {
      min: 75,
      max: 77,
    },
    adTargets: [['spotid', '5842041f4e65fad6a7708890']],
    entitlements: ['sl_premium'],
    isUser: true,
    sst: '76-80',
    weatherIconPath: 'http://weathericoncdn.surfline.com',
    units: 'F',
  };

  before(() => {
    wrapper = shallow(<WetsuitRecommender {...props} />);
  });

  it('renders GoogleDFP ad', () => {
    const googleDFP = wrapper.find(GoogleDFP);
    expect(googleDFP).to.have.length(1);
    expect(googleDFP.prop('adConfig')).to.deep.equal(
      loadAdConfig(
        'spotReportWetsuit',
        [
          props.adTargets[0],
          ['sst', props.sst],
          ['className', 'wetsuit-recommender--v2'],
        ],
        props.entitlements
      )
    );
  });

  it('renders temperatures and weather condition icon', () => {
    const mounted = mount(<WetsuitRecommender {...props} />);
    const water = mounted.find(WaterTemperature);
    const weather = mounted.find(Weather);
    expect(water).to.have.length(1);
    expect(water.text()).to.contain(
      `${props.waterTemp.min} - ${props.waterTemp.max} º${props.units}`
    );
    expect(weather.text()).to.contain(
      `${props.weather.temperature} º${props.units}`
    );

    const icons = mounted.find('img');
    expect(icons).to.have.length(2);
    expect(
      icons.someWhere(
        (icon) =>
          icon.prop('src') === `${props.weatherIconPath}/WATER_ICON.svg` &&
          icon.prop('alt') === `${props.spotName} Water Temp`
      )
    ).to.be.true();
    expect(
      icons.someWhere(
        (icon) =>
          icon.prop('src') ===
            `${props.weatherIconPath}/${props.weather.condition}.svg` &&
          icon.prop('alt') === `${props.spotName} Wind & Weather`
      )
    ).to.be.true();
  });
});
