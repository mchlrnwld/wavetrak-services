import React from 'react';
import PropTypes from 'prop-types';

const NotificationBar = ({ largeNotification }) => (
  <div className="quiver-notification-bar">{largeNotification}</div>
);

NotificationBar.propTypes = {
  largeNotification: PropTypes.node,
};

NotificationBar.defaultProps = {
  largeNotification: null,
};

export default NotificationBar;
