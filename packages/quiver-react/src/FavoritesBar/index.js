import { withCookies } from 'react-cookie';

import FavoritesBar from './FavoritesBar';

export default withCookies(FavoritesBar);
