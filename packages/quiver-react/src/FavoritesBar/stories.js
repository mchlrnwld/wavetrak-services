import React from 'react';
import FavoritesBar from './index';

export default {
  title: 'FavoritesBar',
};

export const Default = () => (
  <FavoritesBar
    user={{ firstName: 'Surfline' }}
    settings={{ units: { surfHeight: 'FT' } }}
    favorites={[
      {
        _id: 'abc1230',
        name: 'Pipeline',
        conditions: { human: false },
        waveHeight: { min: 12, max: 15, occasional: 18 },
        cameras: [],
      },
      {
        _id: '123abc1',
        name: 'Rocky Point',
        conditions: { human: false, value: 'GOOD' },
        waveHeight: { min: 8, max: 12, occasional: 6 },
        cameras: [],
      },
      {
        _id: 'abc1232',
        name: 'Sunset',
        conditions: { human: false, value: 'FAIR' },
        waveHeight: { min: 12, max: 14, occasional: 12 },
        cameras: [],
      },
      {
        _id: 'abc1233',
        name: 'Log Cabins',
        conditions: { human: false, value: 'EPIC' },
        waveHeight: { min: 12, max: 15, occasional: 6 },
        cameras: [],
      },
      {
        _id: 'abc1234',
        name: 'Rockpile',
        conditions: { human: false, value: 'EPIC' },
        waveHeight: { min: 10, max: 12, occasional: 18 },
        cameras: [],
      },
      {
        _id: '123abc5',
        name: 'Waimea Bay',
        conditions: { human: false, value: 'FAIR' },
        waveHeight: { min: 20, max: 22, occasional: 6 },
        cameras: [],
      },
      {
        _id: '123abc6',
        name: 'Haleiwa',
        conditions: { human: false, value: 'FAIR' },
        waveHeight: { min: 9, max: 11, occasional: 6 },
        cameras: [],
      },
      {
        _id: '123abc7',
        name: 'Ala Moana Bowls',
        conditions: { human: false, value: 'POOR' },
        waveHeight: { min: 1, max: 2, occasional: 3 },
        cameras: [],
      },
    ]}
  />
);

Default.story = {
  name: 'default',
};

export const DefaultConditionsV2 = () => (
  <FavoritesBar
    user={{ firstName: 'Surfline' }}
    settings={{ units: { surfHeight: 'FT' } }}
    favorites={[
      {
        _id: 'abc1230',
        name: 'Pipeline',
        conditions: { human: false },
        waveHeight: { min: 12, max: 15, occasional: 18 },
        cameras: [],
      },
      {
        _id: '123abc1',
        name: 'Rocky Point',
        conditions: { human: false, value: 'GOOD' },
        waveHeight: { min: 8, max: 12, occasional: 6 },
        cameras: [],
      },
      {
        _id: 'abc1232',
        name: 'Sunset',
        conditions: { human: false, value: 'FAIR' },
        waveHeight: { min: 12, max: 14, occasional: 12 },
        cameras: [],
      },
      {
        _id: 'abc1233',
        name: 'Log Cabins',
        conditions: { human: false, value: 'EPIC' },
        waveHeight: { min: 12, max: 15, occasional: 6 },
        cameras: [],
      },
      {
        _id: 'abc1234',
        name: 'Rockpile',
        conditions: { human: false, value: 'EPIC' },
        waveHeight: { min: 10, max: 12, occasional: 18 },
        cameras: [],
      },
      {
        _id: '123abc5',
        name: 'Waimea Bay',
        conditions: { human: false, value: 'FAIR' },
        waveHeight: { min: 20, max: 22, occasional: 6 },
        cameras: [],
      },
      {
        _id: '123abc6',
        name: 'Haleiwa',
        conditions: { human: false, value: 'FAIR' },
        waveHeight: { min: 9, max: 11, occasional: 6 },
        cameras: [],
      },
      {
        _id: '123abc7',
        name: 'Ala Moana Bowls',
        conditions: { human: false, value: 'POOR' },
        waveHeight: { min: 1, max: 2, occasional: 3 },
        cameras: [],
      },
    ]}
    sevenRatings
  />
);

DefaultConditionsV2.story = {
  name: 'default with new condition colors',
};

export const UserWithoutFavorites = () => (
  <FavoritesBar
    user={{ firstName: 'Tester' }}
    settings={{ units: { surfHeight: 'FT' } }}
    favorites={[]}
  />
);

UserWithoutFavorites.story = {
  name: 'user without favorites',
};

export const AnonymousUser = () => (
  <FavoritesBar settings={{ units: { surfHeight: 'FT' } }} favorites={[]} />
);

AnonymousUser.story = {
  name: 'anonymous user',
};

export const LargeWithFavorites = () => (
  <FavoritesBar
    user={{ firstName: 'Surfline' }}
    settings={{ units: { surfHeight: 'FT' } }}
    favorites={[
      {
        _id: 'abc1230',
        name: 'Pipeline',
        conditions: { human: false },
        waveHeight: { min: 12, max: 15, occasional: 18 },
        cameras: [
          {
            _id: '12345',
            title: 'Log Cabins',
            streamUrl: '',
            stillUrl: '',
            rewindBaseUrl: '',
            status: {
              isDown: false,
              message: '',
            },
          },
        ],
      },
      {
        _id: '123abc1',
        name: 'Rocky Point',
        conditions: { human: false, value: 'GOOD' },
        waveHeight: { min: 8, max: 12, occasional: 6 },
        cameras: [
          {
            _id: '12345',
            title: 'Log Cabins',
            streamUrl: '',
            stillUrl: '',
            rewindBaseUrl: '',
            status: {
              isDown: false,
              message: '',
            },
          },
        ],
      },
      {
        _id: 'abc1232',
        name: 'Sunset',
        conditions: { human: false, value: 'FAIR' },
        waveHeight: { min: 12, max: 14, occasional: 12 },
        cameras: [],
      },
      {
        _id: 'abc1233',
        name: 'Log Cabins',
        conditions: { human: false, value: 'EPIC' },
        waveHeight: { min: 12, max: 15, occasional: 6 },
        cameras: [],
      },
      {
        _id: 'abc1234',
        name: 'Rockpile',
        conditions: { human: false, value: 'EPIC' },
        waveHeight: { min: 10, max: 12, occasional: 18 },
        cameras: [
          {
            _id: '12345',
            title: 'Log Cabins',
            streamUrl: '',
            stillUrl: '',
            rewindBaseUrl: '',
            status: {
              isDown: false,
              message: '',
            },
          },
        ],
      },
      {
        _id: '123abc5',
        name: 'Waimea Bay',
        conditions: { human: false, value: 'FAIR' },
        waveHeight: { min: 20, max: 22, occasional: 6 },
        cameras: [],
      },
      {
        _id: '123abc6',
        name: 'Haleiwa',
        conditions: { human: false, value: 'FAIR' },
        waveHeight: { min: 9, max: 11, occasional: 6 },
        cameras: [],
      },
      {
        _id: '123abc7',
        name: 'Ala Moana Bowls',
        conditions: { human: false, value: 'POOR' },
        waveHeight: { min: 1, max: 2, occasional: 3 },
        cameras: [
          {
            _id: '12345',
            title: 'Log Cabins',
            streamUrl: '',
            stillUrl: '',
            rewindBaseUrl: '',
            status: {
              isDown: false,
              message: '',
            },
          },
        ],
      },
    ]}
    large
  />
);

LargeWithFavorites.story = {
  name: 'large with favorites',
};

export const LargeWithFavoritesConditionsV2 = () => (
  <FavoritesBar
    sevenRatings
    user={{ firstName: 'Surfline' }}
    settings={{ units: { surfHeight: 'FT' } }}
    favorites={[
      {
        _id: 'abc1230',
        name: 'Pipeline',
        conditions: { human: false },
        waveHeight: { min: 12, max: 15, occasional: 18 },
        cameras: [
          {
            _id: '12345',
            title: 'Log Cabins',
            streamUrl: '',
            stillUrl: '',
            rewindBaseUrl: '',
            status: {
              isDown: false,
              message: '',
            },
          },
        ],
      },
      {
        _id: '123abc1',
        name: 'Rocky Point',
        conditions: { human: false, value: 'GOOD' },
        waveHeight: { min: 8, max: 12, occasional: 6 },
        cameras: [
          {
            _id: '12345',
            title: 'Log Cabins',
            streamUrl: '',
            stillUrl: '',
            rewindBaseUrl: '',
            status: {
              isDown: false,
              message: '',
            },
          },
        ],
      },
      {
        _id: 'abc1232',
        name: 'Sunset',
        conditions: { human: false, value: 'FAIR' },
        waveHeight: { min: 12, max: 14, occasional: 12 },
        cameras: [],
      },
      {
        _id: 'abc1233',
        name: 'Log Cabins',
        conditions: { human: false, value: 'VERY_GOOD' },
        waveHeight: { min: 12, max: 15, occasional: 6 },
        cameras: [],
      },
      {
        _id: 'abc1234',
        name: 'Rockpile',
        conditions: { human: false, value: 'EPIC' },
        waveHeight: { min: 10, max: 12, occasional: 18 },
        cameras: [
          {
            _id: '12345',
            title: 'Log Cabins',
            streamUrl: '',
            stillUrl: '',
            rewindBaseUrl: '',
            status: {
              isDown: false,
              message: '',
            },
          },
        ],
      },
      {
        _id: '123abc5',
        name: 'Waimea Bay',
        conditions: { human: false, value: 'FAIR' },
        waveHeight: { min: 20, max: 22, occasional: 6 },
        cameras: [],
      },
      {
        _id: '123abc6',
        name: 'Haleiwa',
        conditions: { human: false, value: 'FAIR' },
        waveHeight: { min: 9, max: 11, occasional: 6 },
        cameras: [],
      },
      {
        _id: '123abc7',
        name: 'Ala Moana Bowls',
        conditions: { human: false, value: 'POOR' },
        waveHeight: { min: 1, max: 2, occasional: 3 },
        cameras: [
          {
            _id: '12345',
            title: 'Log Cabins',
            streamUrl: '',
            stillUrl: '',
            rewindBaseUrl: '',
            status: {
              isDown: false,
              message: '',
            },
          },
        ],
      },
    ]}
    large
  />
);

LargeWithFavoritesConditionsV2.story = {
  name: 'large with favorites and new condition colors',
};

export const LargeWithoutFavorites = () => (
  <FavoritesBar
    user={{ firstName: 'Surfline' }}
    settings={{ units: { surfHeight: 'FT' } }}
    favorites={[]}
    large
  />
);

LargeWithoutFavorites.story = {
  name: 'large without favorites',
};
