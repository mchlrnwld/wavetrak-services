import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { trackEvent } from '@surfline/web-common';

import classnames from 'classnames';
import settingsPropType from '../PropTypes/settingsPropType';
import favoritesPropType from '../PropTypes/favoritesPropType';
import userPropType from '../PropTypes/userPropType';
import AddFavorites from '../AddFavorites';
import Chevron from '../Chevron';
import Favorite from '../Favorite';
import MultiCam from '../icons/MultiCam';
import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

class FavoritesBar extends Component {
  constructor(props) {
    super(props);
    this.scrollPos = 0;
    this.scrollTS = new Date().getTime();
  }

  componentDidMount() {
    const { large } = this.props;
    this.initScrollPosition(this.ulRef, large);
  }

  getFavoritesBarClassNames = () => {
    const { large } = this.props;
    const classes = {};
    classes['quiver-favorites-bar'] = true;
    classes['quiver-favorites-bar--large'] = large;
    return classnames(classes);
  };

  saveScrollPosition = (to, large) => {
    const { cookies } = this.props;
    const cookieSuffix = large ? 'large' : 'small';
    const hour = 60 * 60;
    const day = hour * 24;
    const month = day * 30;
    cookies.set(`favoritesPosition${cookieSuffix}`, to, { path: '/', month });
  };

  detectScroll = (element) => {
    const { large } = this.props;
    this.scrollPos = element.scrollLeft;
    const currentScrollTS = new Date().getTime();
    const scrollTSDiff = currentScrollTS - this.scrollTS;
    // only check every second during scrolls, wait a second to set the latest value.
    if (scrollTSDiff > 1000) {
      // reset the timer
      this.scrollTS = currentScrollTS;
      setTimeout(() => {
        this.saveScrollPosition(this.scrollPos, large);
      }, 1000);
    }
  };

  adjustScrollPosition = (direction) => {
    if (direction === 'left') {
      this.handleScroll(
        this.ulRef,
        this.ulRef.scrollLeft - this.ulRef.clientWidth,
        150
      );
    } else if (direction === 'right') {
      this.handleScroll(
        this.ulRef,
        this.ulRef.scrollLeft + this.ulRef.clientWidth,
        150
      );
    }
  };

  initScrollPosition = (element, large) => {
    const { cookies } = this.props;
    const cookieSuffix = large ? 'large' : 'small';
    const favoritesPositionCookie = cookies.get(
      `favoritesPosition${cookieSuffix}`
    );
    if (favoritesPositionCookie) {
      this.handleScroll(element, favoritesPositionCookie, 10);
    }
  };

  handleScroll = (element, to, duration) => {
    const { large } = this.props;
    if (duration <= 0) {
      // save scroll position cookie on last scroll event
      this.saveScrollPosition(to, large);
      return;
    }
    const el = element;
    const difference = to - el.scrollLeft;
    const perTick = (difference / duration) * 10;
    setTimeout(() => {
      el.scrollLeft += perTick;
      this.handleScroll(el, to, duration - 10);
    }, 10);
  };

  render() {
    const { style, user, favorites, settings, large, sevenRatings } =
      this.props;

    return (
      <div className={this.getFavoritesBarClassNames()} style={style}>
        <div className="quiver-favorites-bar__content">
          <div
            className="quiver-favorites-bar__content-favorites"
            ref={(el) => {
              this.scrollRef = el;
            }}
          >
            <div className="quiver-favorites-bar__content-multi-cam-link">
              <a
                href="/surf-cams"
                className={classnames({
                  'quiver-favorites-bar__content-multi-cam-link_anchor': true,
                  'quiver-favorites-bar__content-multi-cam-link_anchor--large':
                    large,
                })}
              >
                <MultiCam large={large} />
                <div className="quiver-favorites-bar__content-multi-cam-link_anchor__multi-cam">
                  Multi-cam
                </div>
              </a>
            </div>
            <ul
              className="quiver-favorites-bar__content-favorites-list"
              ref={(el) => {
                this.ulRef = el;
              }}
              onScroll={() => this.detectScroll(this.ulRef)}
            >
              {favorites.map((favorite) => (
                <Favorite
                  key={favorite._id}
                  favorite={favorite}
                  settings={settings}
                  large={large}
                  onClickLink={({ name, properties }) =>
                    trackEvent(name, properties)
                  }
                  sevenRatings={sevenRatings}
                />
              ))}
              <AddFavorites large={large} user={user} />
            </ul>
            <div className="quiver-favorites-bar__content-favorites-arrows">
              {['left', 'right'].map((dir) => (
                <div
                  key={dir}
                  className="quiver-favorites-bar__content-favorites-arrow"
                  {...createAccessibleOnClick(
                    () => this.adjustScrollPosition(dir),
                    BTN
                  )}
                >
                  <Chevron direction={dir} />
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

FavoritesBar.propTypes = {
  settings: settingsPropType,
  favorites: favoritesPropType,
  style: PropTypes.shape(),
  user: userPropType,
  large: PropTypes.bool,
  cookies: PropTypes.shape({
    get: PropTypes.func.isRequired,
    set: PropTypes.func.isRequired,
    remove: PropTypes.func.isRequired,
  }).isRequired,
  sevenRatings: PropTypes.bool,
};

FavoritesBar.defaultProps = {
  settings: null,
  user: null,
  favorites: [],
  style: {},
  large: false,
  sevenRatings: false,
};

export default FavoritesBar;
