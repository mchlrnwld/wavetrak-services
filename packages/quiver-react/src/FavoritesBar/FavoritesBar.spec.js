import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import FavoritesBar from './FavoritesBar';
import MultiCam from '../icons/MultiCam';

describe('FavoritesBar', () => {
  const cookies = {
    get: sinon.stub(),
    set: sinon.stub(),
    remove: sinon.stub(),
  };

  beforeEach(() => {
    cookies.get.resetHistory();
    cookies.set.resetHistory();
    cookies.remove.resetHistory();
  });
  it('should render favorites list when user has favorites', () => {
    const wrapper = mount(
      <FavoritesBar
        cookies={cookies}
        user={{ name: 'Test' }}
        settings={{ units: { surfHeight: 'FT' } }}
        favorites={[
          {
            _id: 'abc123',
            name: 'Pipeline',
            conditions: { human: false, value: 'EPIC' },
            waveHeight: { min: 12, max: 15, occasional: 18 },
          },
          {
            _id: '123abc',
            name: 'Huntington Beach',
            conditions: { human: false, value: 'GOOD' },
            waveHeight: { min: 3, max: 5, occasional: 6 },
          },
        ]}
      />
    );

    const listWrappers = wrapper.find('li');
    expect(listWrappers).to.have.length(2);
  });

  it('should handleScroll when adjustScrollPosition is called', () => {
    const wrapper = mount(
      <FavoritesBar
        cookies={cookies}
        user={{ name: 'Test' }}
        settings={{ units: { surfHeight: 'FT' } }}
        favorites={[
          {
            _id: 'abc123',
            name: 'Pipeline',
            conditions: { human: false, value: 'EPIC' },
            waveHeight: { min: 12, max: 15, occasional: 18 },
          },
        ]}
      />
    );

    const initScrollPositionStub = sinon.stub(
      wrapper.instance(),
      'initScrollPosition'
    );
    initScrollPositionStub.returns('initialized scroll position');

    wrapper.instance().componentDidMount();
    expect(initScrollPositionStub.called).to.be.true();

    const handleScrollStub = sinon.stub(wrapper.instance(), 'handleScroll');
    handleScrollStub.returns('handled scroll');

    wrapper.instance().adjustScrollPosition('left');
    expect(handleScrollStub.called).to.be.true();

    wrapper.instance().adjustScrollPosition('right');
    expect(handleScrollStub.called).to.be.true();
    handleScrollStub.restore();
  });

  it('should not handleScroll if duration is equal 0', () => {
    const wrapper = mount(
      <FavoritesBar
        cookies={cookies}
        user={{ name: 'Test' }}
        settings={{ units: { surfHeight: 'FT' } }}
        favorites={[
          {
            _id: 'abc123',
            name: 'Pipeline',
            conditions: { human: false, value: 'EPIC' },
            waveHeight: { min: 12, max: 15, occasional: 18 },
          },
        ]}
      />
    );
    const saveScrollPositionSpy = sinon.spy(
      wrapper.instance(),
      'saveScrollPosition'
    );
    const handleScrollSpy = sinon.spy(wrapper.instance(), 'handleScroll');
    const element = { scrollLeft: 100 };
    wrapper.instance().handleScroll(element, 500, 0);
    expect(handleScrollSpy.calledOnce).to.be.true();
    expect(saveScrollPositionSpy.called).to.be.true();
    saveScrollPositionSpy.restore();
    handleScrollSpy.restore();
  });

  it('should handleScroll if duration is greater than 0', () => {
    const wrapper = mount(
      <FavoritesBar
        cookies={cookies}
        user={{ name: 'Test' }}
        settings={{ units: { surfHeight: 'FT' } }}
        favorites={[
          {
            _id: 'abc123',
            name: 'Pipeline',
            conditions: { human: false, value: 'EPIC' },
            waveHeight: { min: 12, max: 15, occasional: 18 },
          },
        ]}
      />
    );
    const saveScrollPositionSpy = sinon.spy(
      wrapper.instance(),
      'saveScrollPosition'
    );
    const handleScrollSpy = sinon.spy(wrapper.instance(), 'handleScroll');
    const element = { scrollLeft: 100 };
    wrapper.instance().handleScroll(element, 500, 150);
    expect(handleScrollSpy.called).to.be.true();
    expect(saveScrollPositionSpy.called).to.be.false();
    saveScrollPositionSpy.restore();
    handleScrollSpy.restore();
  });

  it('should render the multi-cam link', () => {
    const wrapper = mount(
      <FavoritesBar
        cookies={cookies}
        user={{ name: 'Test' }}
        settings={{ units: { surfHeight: 'FT' } }}
        favorites={[
          {
            _id: 'abc123',
            name: 'Pipeline',
            conditions: { human: false, value: 'EPIC' },
            waveHeight: { min: 12, max: 15, occasional: 18 },
          },
        ]}
        large
      />
    );
    expect(
      wrapper.find('.quiver-favorites-bar__content-multi-cam-link')
    ).to.have.length(1);
    expect(wrapper.find(MultiCam).prop('large')).to.be.true();
  });

  it('should render favorites list with new condition colors when sevenRatings prop is passed', () => {
    const wrapper = mount(
      <FavoritesBar
        cookies={cookies}
        user={{ name: 'Test' }}
        settings={{ units: { surfHeight: 'FT' } }}
        favorites={[
          {
            _id: 'abc123',
            name: 'Pipeline',
            conditions: { human: false, value: 'EPIC' },
            waveHeight: { min: 12, max: 15, occasional: 18 },
          },
          {
            _id: '123abc',
            name: 'Huntington Beach',
            conditions: { human: false, value: 'GOOD' },
            waveHeight: { min: 3, max: 5, occasional: 6 },
          },
        ]}
        sevenRatings
      />
    );

    const conditionWrapper1 = wrapper.find(
      '.quiver-favorite__item-condition-v2--epic'
    );
    const conditionWrapper2 = wrapper.find(
      '.quiver-favorite__item-condition-v2--good'
    );

    expect(conditionWrapper1).to.have.length(1);
    expect(conditionWrapper2).to.have.length(1);
  });
});
