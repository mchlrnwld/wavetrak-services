import React from 'react';
import PropTypes from 'prop-types';
import scales from './scales.json';
import WindBarbsLegend from './WindBarbsLegend';

const ChartLegend = ({ type, userUnits }) => {
  const swellUnit = userUnits?.swellHeight.toLowerCase();
  const { blocks, labels, unit } =
    type === 'swell' ? scales[`${type}-${swellUnit || 'ft'}`] : scales[type];

  const linearGradientSteps = blocks.reduce((accum, block, index, array) => {
    const prevBlock = array[index - 1];
    if (index === 0) {
      return `${block.color} ${block.position}%, `;
    }
    if (index < array.length - 1) {
      return `${accum} ${block.color} ${prevBlock.position}%, ${block.color} ${block.position}%, `;
    }
    return `${accum} ${block.color} ${prevBlock.position}%`;
  }, '');

  const linearGradient = `linear-gradient(90deg, ${linearGradientSteps})`;

  return (
    <div className="quiver-chart-legend">
      <div className="quiver-chart-legend__labels">
        <div className="quiver-chart-legend__units">({unit})</div>
        {labels.map((label) => (
          <div
            key={label.value}
            className="quiver-chart-legend__label"
            style={{ right: `${100 - label.position}%` }}
          >
            {label.value}
          </div>
        ))}
      </div>
      <div
        className="quiver-chart-legend__bar"
        style={{ background: linearGradient }}
      />
      {type === 'wind' ? <WindBarbsLegend /> : null}
    </div>
  );
};

ChartLegend.propTypes = {
  type: PropTypes.string.isRequired,
  userUnits: PropTypes.shape({
    surfHeight: PropTypes.string,
    swellHeight: PropTypes.string,
    tideHeight: PropTypes.string,
    windSpeed: PropTypes.string,
    temperature: PropTypes.string,
  }),
};

ChartLegend.defaultProps = {
  userUnits: null,
};

export default ChartLegend;
