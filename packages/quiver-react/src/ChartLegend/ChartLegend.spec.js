import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ChartLegend from '.';
import WindBarbsLegend from './WindBarbsLegend';

describe('Chart Legend', () => {
  it('accepts only appropriate types', () => {
    const swellWrapper = shallow(<ChartLegend type="swell" />);
    const windWrapper = shallow(<ChartLegend type="wind" />);
    const periodWrapper = shallow(<ChartLegend type="period" />);
    const hiresWindWrapper = shallow(<ChartLegend type="hireswind" />);
    expect(swellWrapper).to.exist();
    expect(windWrapper).to.exist();
    expect(periodWrapper).to.exist();
    expect(hiresWindWrapper).to.exist();

    expect(() => shallow(<ChartLegend type="unacceptablekey" />)).to.throw();
  });

  it('should render wind barbs for charts with type: wind', () => {
    const windWrapper = shallow(<ChartLegend type="wind" />);
    const hiresWindWrapper = shallow(<ChartLegend type="hireswind" />);
    const windBarbsLegend = windWrapper.find(WindBarbsLegend);
    const noWindBarbsLegend = hiresWindWrapper.find(WindBarbsLegend);
    expect(windBarbsLegend).to.have.length(1);
    expect(noWindBarbsLegend).to.have.length(0);
  });
});
