import React from 'react';

const calm = (
  <svg width="7px" height="7px" viewBox="0 0 7 7">
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-59.000000, -37.000000)" fill="#333333">
        <rect id="calm" x="59" y="37" width="7" height="7" />
      </g>
    </g>
  </svg>
);

const fiveKts = (
  <svg width="47px" height="14px" viewBox="0 0 47 14">
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-59.000000, -84.000000)" fill="#333333">
        <g transform="translate(59.000000, 84.000000)">
          <rect x="0" y="0" width="7" height="7" />
          <rect x="7" y="2" width="40" height="3" />
          <rect
            transform="translate(35.781643, 8.810898) rotate(40) translate(-35.781643, -8.810898)"
            x="28.2816429"
            y="8.31089832"
            width="15"
            height="1"
          />
        </g>
      </g>
    </g>
  </svg>
);

const tenKts = (
  <svg width="63px" height="17px" viewBox="0 0 63 17">
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-218.000000, -37.000000)" fill="#333333">
        <g transform="translate(218.000000, 37.000000)">
          <rect x="0" y="0" width="7" height="7" />
          <rect x="7" y="3" width="40" height="1" />
          <rect
            transform="translate(54.281643, 9.810898) rotate(40) translate(-54.281643, -9.810898)"
            x="44.2816429"
            y="9.31089832"
            width="20"
            height="1"
          />
        </g>
      </g>
    </g>
  </svg>
);

const fifteenKts = (
  <svg width="62px" height="18px" viewBox="0 0 62 18" version="1.1">
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-218.000000, -84.000000)" fill="#333333">
        <g transform="translate(218.000000, 84.000000)">
          <rect x="0" y="0" width="7" height="7" />
          <rect x="7" y="2" width="40" height="3" />
          <rect
            transform="translate(53.981838, 10.810898) rotate(40) translate(-53.981838, -10.810898)"
            x="43.9818382"
            y="10.3108983"
            width="20"
            height="1"
          />
          <rect
            transform="translate(40.481838, 8.810898) rotate(40) translate(-40.481838, -8.810898)"
            x="32.9818382"
            y="8.31089832"
            width="15"
            height="1"
          />
        </g>
      </g>
    </g>
  </svg>
);

const twentyKts = (
  <svg width="63px" height="17px" viewBox="0 0 63 17" version="1.1">
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-390.000000, -37.000000)" fill="#333333">
        <g transform="translate(390.000000, 37.000000)">
          <rect x="0" y="0" width="7" height="7" />
          <rect x="7" y="3" width="40" height="1" />
          <rect
            transform="translate(54.281643, 9.810898) rotate(40) translate(-54.281643, -9.810898)"
            x="44.2816429"
            y="9.31089832"
            width="20"
            height="1"
          />
          <rect
            transform="translate(43.981838, 9.810898) rotate(40) translate(-43.981838, -9.810898)"
            x="33.9818382"
            y="9.31089832"
            width="20"
            height="1"
          />
        </g>
      </g>
    </g>
  </svg>
);

const fiftyKts = (
  <svg width="64px" height="22px" viewBox="0 0 64 22" version="1.1">
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-390.000000, -84.000000)" fill="#333333">
        <g transform="translate(390.000000, 84.000000)">
          <rect x="0" y="0" width="7" height="7" />
          <rect x="7" y="2" width="40" height="3" />
          <path
            d="M61,5 L49,5 L61,15 L61,5 Z M64,2 L64,21.4051248 L40.7138502,2 L64,2 Z"
            fillRule="nonzero"
          />
        </g>
      </g>
    </g>
  </svg>
);

const sixtyFiveKts = (
  <svg width="64px" height="22px" viewBox="0 0 64 22" version="1.1">
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-562.000000, -37.000000)" fill="#333333">
        <g transform="translate(562.000000, 37.000000)">
          <g>
            <rect x="0" y="0" width="7" height="7" />
            <rect x="7" y="2" width="40" height="3" />
          </g>
          <rect
            transform="translate(41.981838, 10.810898) rotate(40) translate(-41.981838, -10.810898)"
            x="31.9818382"
            y="10.3108983"
            width="20"
            height="1"
          />
          <rect
            transform="translate(30.066727, 9.203929) rotate(40) translate(-30.066727, -9.203929)"
            x="22.5667271"
            y="8.70392929"
            width="15"
            height="1"
          />
          <path
            d="M61,5 L49,5 L61,15 L61,5 Z M64,2 L64,21.4051248 L40.7138502,2 L64,2 Z"
            fillRule="nonzero"
          />
        </g>
      </g>
    </g>
  </svg>
);

const windBarbs = [
  {
    label: 'Calm',
    icon: calm,
  },
  {
    label: '5kts',
    icon: fiveKts,
  },
  {
    label: '10kts',
    icon: tenKts,
  },
  {
    label: '15kts',
    icon: fifteenKts,
  },
  {
    label: '20kts',
    icon: twentyKts,
  },
  {
    label: '50kts',
    icon: fiftyKts,
  },
  {
    label: '65kts',
    icon: sixtyFiveKts,
  },
];

export default windBarbs;
