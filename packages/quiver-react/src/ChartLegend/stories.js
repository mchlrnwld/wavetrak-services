import React from 'react';
import ChartLegend from './ChartLegend';

export default {
  title: 'ChartLegend',
};

export const Swell = () => <ChartLegend type="swell" />;
export const Wind = () => <ChartLegend type="wind" />;
export const Period = () => <ChartLegend type="period" />;
export const HiResWind = () => <ChartLegend type="hireswind" />;

HiResWind.story = {
  name: 'HiRes Wind',
};
