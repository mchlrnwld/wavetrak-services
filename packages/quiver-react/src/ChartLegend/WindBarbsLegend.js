import React from 'react';
import windBarbs from './windBarbs';

const WindBarbsLegend = () => (
  <div className="quiver-wind-barbs-legend">
    <div className="quiver-wind-barbs-legend__example">
      <p className="quiver-wind-barbs-legend__example__header">Read as:</p>
      <p className="quiver-wind-barbs-legend__example__label">
        NW wind at 20kts
      </p>
      <div className="quiver-wind-barbs-legend__example__icon">
        {windBarbs[4].icon}
      </div>
    </div>
    <div className="quiver-wind-barbs-legend__key">
      <div className="quiver-wind-barbs-legend__key__header">
        Wind Speed & Direction (Barbs point to the direction the wind is coming
        from)
      </div>
      <div className="quiver-wind-barbs-legend__key__items">
        {windBarbs.map((item) => (
          <div key={item.label} className="quiver-wind-barbs-legend__key__item">
            {item.icon}
            {item.label}
          </div>
        ))}
      </div>
    </div>
  </div>
);

export default WindBarbsLegend;
