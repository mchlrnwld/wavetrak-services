import PropTypes from 'prop-types';
import React from 'react';
import weatherPropType from '../PropTypes/weather';

const WeatherGraph = ({ weatherIconPath, data }) => (
  <div className="quiver-weather-graph">
    {data.map(({ timestamp, condition, temperature }) => (
      <div key={timestamp}>
        <img
          className="quiver-weather-graph__condition"
          src={`${weatherIconPath}/${condition}.svg`}
          alt={condition}
        />
        <div className="quiver-weather-graph__temperature">
          {Math.round(temperature)}º
        </div>
      </div>
    ))}
  </div>
);

WeatherGraph.propTypes = {
  weatherIconPath: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(weatherPropType).isRequired,
};

export default WeatherGraph;
