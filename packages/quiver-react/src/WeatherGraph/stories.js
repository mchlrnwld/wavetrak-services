import React from 'react';
import WeatherGraph from './WeatherGraph';

const props = {
  weatherIconPath: 'https://weathericonpath.surfline.com',
  data: [
    {
      timestamp: 0,
      condition: 'NIGHT_CLEAR_NO_RAIN',
      temperature: 50,
    },
    {
      timestamp: 1,
      condition: 'CLEAR_NO_RAIN',
      temperature: 51,
    },
    {
      timestamp: 2,
      condition: 'PARTIAL_CLOUDY_NO_RAIN',
      temperature: 52,
    },
  ],
};

export default {
  title: 'WeatherGraph',
};

export const Default = () => <WeatherGraph {...props} />;

Default.story = {
  name: 'default',
};
