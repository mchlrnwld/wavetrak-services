import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import WeatherGraph from './WeatherGraph';

describe('components / WeatherGraph', () => {
  it('renders weather data', () => {
    const props = {
      weatherIconPath: 'https://weathericonpath.surfline.com',
      data: [
        {
          timestamp: 0,
          condition: 'NIGHT_CLEAR_NO_RAIN',
          temperature: 50,
        },
        {
          timestamp: 1,
          condition: 'CLEAR_NO_RAIN',
          temperature: 51,
        },
        {
          timestamp: 2,
          condition: 'PARTIAL_CLOUDY_NO_RAIN',
          temperature: 52,
        },
      ],
    };
    const wrapper = shallow(<WeatherGraph {...props} />);
    const data = wrapper.children();
    expect(data).to.have.length(3);
  });
});
