import React from 'react';
import SwellGraph from './SwellGraph';
import data from './fixtures/data';

const props = {
  utcOffset: -8,
  day: 0,
  lat: 33.654213041213,
  lon: -118.0032588192,
  overallMaxHeight: 1.3,
  units: 'FT',
  isSwellGraph: true,
  data,
  mapZoom: 7,
  showOptimalConditions: true,
  activeSwell: 3,
  device: { width: 1200, mobile: false },
  doLoadTreatment: () => 'on',
};

export default {
  title: 'SwellGraph',
};

export const Default = () => <SwellGraph {...props} />;

Default.story = {
  name: 'default',
};
