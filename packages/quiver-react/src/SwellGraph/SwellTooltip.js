/* istanbul ignore file */
import React from 'react';
import PropTypes from 'prop-types';
import { getMapTileUrl, TABLET_LARGE_WIDTH } from '@surfline/web-common';

import SwellRose from '../SwellRose';
import SwellMeasurements from '../SwellMeasurements';
import GraphTooltip from '../GraphTooltip';
import OptimalScoreIndicator from '../OptimalScoreIndicator';
import TooltipDatetime from '../TooltipDatetime';
import devicePropTypes from '../PropTypes/device';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';

const SwellTooltip = ({
  device,
  overallMaxHeight,
  data,
  lat,
  lon,
  utcOffset,
  units,
  mapZoom,
  showOptimalConditions,
  activeSwell,
  xPercentage,
  dateFormat,
}) => {
  const isMobile = device.width < TABLET_LARGE_WIDTH;
  const mapUrl = getMapTileUrl(152, 88, lat, lon, mapZoom);
  const point = data[Math.floor(data.length * xPercentage)];

  if (!point) return null;

  if (isMobile) {
    // isEmptySwell is there so that we can have 4 swells on the mobile tooltip
    // It will be a empty swell reading with the colored dot and a em-dash (–)
    const mobileTooltipSwells = point.swells
      .filter(({ height, period }) => height > 0 && period > 0)
      .concat(
        [...new Array(4)].map(() => ({
          isEmptySwell: true,
        }))
      )
      .slice(0, 4);

    return (
      <GraphTooltip>
        <div className="quiver-swell-graph__tooltip-swells--mobile">
          {mobileTooltipSwells.map((swell, index) => (
            <SwellMeasurements
              key={swell.index}
              height={swell.height}
              period={swell.period}
              direction={swell.direction}
              directionMin={swell.directionMin}
              optimalScore={showOptimalConditions ? swell.optimalScore : null}
              index={swell.index ?? index}
              units={units}
              isEmptySwell={swell.isEmptySwell}
            />
          ))}
        </div>
      </GraphTooltip>
    );
  }

  const swell = point.swells.find(
    ({ index, height, period }) =>
      index === activeSwell && height > 0 && period > 0
  );

  if (!swell) return null;

  return (
    <GraphTooltip>
      <div
        className="quiver-swell-graph__tooltip-map"
        style={{ background: `url("${mapUrl}") center` }}
      >
        <SwellRose overallMaxHeight={overallMaxHeight} swells={[swell]} />
      </div>
      {showOptimalConditions ? (
        <OptimalScoreIndicator optimalScore={swell.optimalScore} />
      ) : null}
      <TooltipDatetime
        utcOffset={point.utcOffset ?? utcOffset}
        timestamp={point.timestamp}
        dateFormat={dateFormat}
      />
      <div className="quiver-swell-graph__tooltip-swells">
        <SwellMeasurements
          key={swell.index}
          height={swell.height}
          period={swell.period}
          direction={swell.direction}
          directionMin={swell.directionMin}
          optimalScore={showOptimalConditions ? swell.optimalScore : null}
          index={swell.index}
          units={units}
        />
      </div>
    </GraphTooltip>
  );
};

SwellTooltip.propTypes = {
  overallMaxHeight: PropTypes.number.isRequired,
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  utcOffset: PropTypes.number.isRequired,
  lat: PropTypes.number.isRequired,
  lon: PropTypes.number.isRequired,
  units: PropTypes.string.isRequired,
  mapZoom: PropTypes.number.isRequired,
  showOptimalConditions: PropTypes.bool.isRequired,
  activeSwell: PropTypes.number,
  xPercentage: PropTypes.number.isRequired,
  device: devicePropTypes.isRequired,
  dateFormat: dateFormatPropType,
};

SwellTooltip.defaultProps = {
  activeSwell: null,
  dateFormat: defaultDateFormat,
};

export default SwellTooltip;
