import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { TABLET_LARGE_WIDTH, DEFAULT_SWELL_MAX } from '@surfline/web-common';
import { line } from 'd3-shape';
import { scaleTime, scaleLinear } from 'd3-scale';
import { select } from 'd3-selection';
import _isEqual from 'lodash/isEqual';
import _round from 'lodash/round';
import classNames from 'classnames';

import splitSwellLines from './splitSwellLines';
import SwellTooltip from './SwellTooltip';
import TooltipProvider from '../TooltipProvider';
import devicePropType from '../PropTypes/device';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';

class SwellGraph extends Component {
  constructor() {
    super();
    this.state = {
      activeHour: null,
      canvasRef: null,
    };
    this.swellGraphRef = React.createRef();
  }

  componentDidMount() {
    this.renderGraph();
    this.activateSwell();
    window.addEventListener('resize', this.resizeListener);
  }

  componentDidUpdate(props, state) {
    const { activeHour, canvasRef } = this.state;
    const { days, activeSwell, isHourly } = this.props;
    const daysChanged = !_isEqual(props.days, days);
    const canvasRefChanged = canvasRef && canvasRef !== state.canvasRef;

    if (daysChanged || props.isHourly !== isHourly || canvasRefChanged)
      this.renderGraph();
    if (props.activeSwell !== activeSwell) this.activateSwell();
    if (props.activeSwell !== activeSwell || state.activeHour !== activeHour) {
      this.activateSwellNode();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeListener);
    /**
     *  Since D3 Manipulates the DOM and we now have two instances of this component, we want to prevent
     *  D3 from doing any dom mutations from an unmounting component
     */
    this.renderGraph = () => null;
    this.activateSwell = () => null;
    this.activateSwellNode = () => null;
  }

  setCanvasRef = (el) =>
    this.setState({
      canvasRef: el,
    });

  handleShowTooltip = (xPercentage) => {
    const { data } = this.props;
    this.setState({ activeHour: Math.floor(data.length * xPercentage) });
  };

  handleHideTooltip = () => {
    this.setState({ activeHour: null });
  };

  resizeListener = () => {
    if (this.resizeTimeout) return;
    this.resizeTimeout = setTimeout(() => {
      this.resizeTimeout = null;
      this.renderGraph();
      this.activateSwell();
    }, 66);
  };

  activateSwell = () => {
    const { activeSwell } = this.props;
    const { canvasRef } = this.state;

    // If the canvasRef is not defined, then the graph won't be visible
    if (!canvasRef) return;

    const previousActiveSwellSVG = this.canvasSVG.select(
      '.quiver-swell-graph__swell--active'
    );

    // Remove active class and move back to original position
    if (!previousActiveSwellSVG.empty()) {
      const previousActiveSwell = parseInt(
        previousActiveSwellSVG.attr('class').match(/swell--(\d+)/)[1],
        10
      );

      const nextSibling = this.canvasSVG.select(
        `.quiver-swell-graph__swell--${previousActiveSwell + 1}`
      );

      previousActiveSwellSVG
        .classed('quiver-swell-graph__swell--active', false)
        .each(function moveBackToOriginalPosition() {
          if (nextSibling.empty()) {
            this.parentNode.appendChild(this);
          } else {
            this.parentNode.insertBefore(this, nextSibling.node());
          }
        });
    }

    // Add active class and move to front
    if (activeSwell !== null) {
      this.canvasSVG
        .select(`.quiver-swell-graph__swell--${activeSwell}`)
        .classed('quiver-swell-graph__swell--active', true)
        .each(function moveToFront() {
          this.parentNode.appendChild(this);
        });
    }
  };

  activateSwellNode = () => {
    const { activeHour, canvasRef } = this.state;
    const { activeSwell } = this.props;

    // If the canvasRef is not defined, then the graph won't be visible
    if (!canvasRef) return;

    this.canvasSVG
      .select('.quiver-swell-graph__node--active')
      .classed('quiver-swell-graph__node--active', false);

    this.canvasSVG
      .select(
        `.quiver-swell-graph__swell--${activeSwell} .quiver-swell-graph__node--${activeHour}`
      )
      .classed('quiver-swell-graph__node--active', true);
  };

  renderGraph = () => {
    const { overallMaxHeight, data, numberOfTicks, units, onHoverSwell } =
      this.props;
    const { canvasRef } = this.state;

    // If the canvasRef is not defined, then the graph won't be visible
    if (!canvasRef) return;

    const arrowScale = scaleLinear()
      .domain([5, 20])
      .range([0.6, 1.3])
      .clamp(1.3)
      .clamp(true);

    const swellData = [...Array(6)].map((_, swellIndex) =>
      data.map(({ swells }, index) => ({
        hour: index,
        scale: arrowScale(swells[swellIndex].period),
        ...swells[swellIndex],
      }))
    );

    select(`.quiver-swell-graph__ticks`).remove();
    select(`.quiver-swell-graph__canvas-svg`).remove();

    const xPadding = canvasRef.offsetWidth / (2 * data.length);
    const yPadding = 24;

    const x = scaleTime()
      .rangeRound([xPadding, canvasRef.offsetWidth - xPadding])
      .domain([0, data.length - 1]);

    const digits = units === 'FT' ? 1 : 2;
    const yMax = Math.max(
      _round(overallMaxHeight, digits),
      DEFAULT_SWELL_MAX[units] || 0
    );

    const y = scaleLinear()
      .rangeRound([canvasRef.offsetHeight - yPadding, yPadding])
      .domain([0, yMax]);

    const path = line()
      .x((d) => x(d.hour))
      .y((d) => y(d.height));

    const canvas = select(`.quiver-swell-graph__canvas`);

    if (numberOfTicks > 0) {
      const maxTick = _round(yMax);
      const intermediateTicks = [...Array(numberOfTicks - 2)].map((_, index) =>
        _round((index + 1) * (maxTick / (numberOfTicks - 1)), 1)
      );
      const tickData = [0, ...intermediateTicks, maxTick];

      const ticks = canvas
        .append('div')
        .attr('class', 'quiver-swell-graph__ticks');

      ticks
        .selectAll('.quiver-swell-graph__tick')
        .data(tickData)
        .enter()
        .append('div')
        .attr('class', 'quiver-swell-graph__tick')
        .style('top', (d) => `${y(d)}px`)
        .text((d) => d);
    }

    this.canvasSVG = canvas
      .append('svg')
      .attr('class', 'quiver-swell-graph__canvas-svg');

    swellData.forEach((swell, swellIndex) => {
      const swellPoints = swell.filter(
        ({ height, period }) => height > 0 && period > 0
      );
      const swellLines = splitSwellLines(swellPoints);
      const swellGroup = this.canvasSVG
        .append('g')
        .attr(
          'class',
          `quiver-swell-graph__swell quiver-swell-graph__swell--${swellIndex}`
        );

      if (onHoverSwell) {
        swellGroup.on('mouseover', () => onHoverSwell(swellIndex));
      }

      swellLines.forEach((swellLine) => {
        swellGroup
          .append('path')
          .attr('class', 'quiver-swell-graph__visible-line')
          .attr('d', path(swellLine));

        // Add an invisible line path with a larger stroke-width to increase the mousemove hot spot
        swellGroup
          .append('path')
          .attr('class', 'quiver-swell-graph__invisible-line')
          .attr('d', path(swellLine));
      });

      const nodeGroup = swellGroup
        .selectAll('.quiver-swell-graph__node')
        .data(swellPoints)
        .enter()
        .append('g')
        .attr(
          'class',
          (d) => `quiver-swell-graph__node quiver-swell-graph__node--${d.hour}`
        )
        .attr('transform', (d) => {
          const scaledCenter = arrowScale(d.period) * 8;
          const cx = x(d.hour) - scaledCenter;
          const cy = y(d.height) - scaledCenter;

          return `translate(${cx}, ${cy})`;
        });

      nodeGroup
        .append('polygon')
        .attr('class', 'quiver-swell-graph__inactive-node')
        .attr('vector-effect', 'non-scaling-stroke')
        .attr('points', '8,16 16,8.4 11.2,8.4 11.2,0 4.75,0 4.75,8.4 0,8.4')
        .attr('transform', (d) => {
          const scaledCenter = d.scale * 8;
          return `rotate(${d.direction} ${scaledCenter} ${scaledCenter}) scale(${d.scale})`;
        });

      const activeNode = nodeGroup
        .append('g')
        .attr('class', 'quiver-swell-graph__active-node');

      activeNode
        .append('circle')
        .attr('vector-effect', 'non-scaling-stroke')
        .attr('cx', (d) => d.scale * 8)
        .attr('cy', (d) => d.scale * 8)
        .attr('r', 7);

      activeNode
        .append('circle')
        .attr('vector-effect', 'non-scaling-stroke')
        .attr('cx', (d) => d.scale * 8)
        .attr('cy', (d) => d.scale * 8)
        .attr('r', 4.5);
    });
  };

  renderTooltip = (xPercentage) => (
    <SwellTooltip xPercentage={xPercentage} {...this.props} />
  );

  render() {
    const {
      activeSwell,
      obscureInactiveSwells,
      currentMarkerXPercentage,
      device,
      isMultiCamPage,
      dateFormat,
    } = this.props;
    const className = classNames({
      'quiver-swell-graph': true,
      [`quiver-swell-graph--active-swell-${activeSwell}`]:
        activeSwell || activeSwell === 0,
      'quiver-swell-graph--obscure-inactive': obscureInactiveSwells,
    });

    const isMobile = device.width < TABLET_LARGE_WIDTH;
    return (
      <div ref={this.swellGraphRef} className={className}>
        <TooltipProvider
          currentMarkerXPercentage={currentMarkerXPercentage}
          renderTooltip={this.renderTooltip}
          onShowTooltip={this.handleShowTooltip}
          onHideTooltip={!isMobile ? this.handleHideTooltip : null}
          dateFormat={dateFormat}
          isSwellGraph
          parentRef={isMultiCamPage && this.swellGraphRef.current}
        >
          <div ref={this.setCanvasRef} className="quiver-swell-graph__canvas" />
        </TooltipProvider>
      </div>
    );
  }
}

SwellGraph.propTypes = {
  numberOfTicks: PropTypes.number,
  overallMaxHeight: PropTypes.number.isRequired,
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  utcOffset: PropTypes.number.isRequired,
  lat: PropTypes.number.isRequired,
  lon: PropTypes.number.isRequired,
  units: PropTypes.string.isRequired,
  mapZoom: PropTypes.number,
  showOptimalConditions: PropTypes.bool,
  activeSwell: PropTypes.number,
  onHoverSwell: PropTypes.func,
  device: devicePropType.isRequired,
  obscureInactiveSwells: PropTypes.bool,
  isHourly: PropTypes.bool,
  currentMarkerXPercentage: PropTypes.number,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  isMultiCamPage: PropTypes.bool,
  dateFormat: dateFormatPropType,
};

SwellGraph.defaultProps = {
  numberOfTicks: 0,
  mapZoom: 6,
  showOptimalConditions: false,
  activeSwell: null,
  onHoverSwell: null,
  obscureInactiveSwells: false,
  currentMarkerXPercentage: 0,
  isHourly: false,
  isMultiCamPage: false,
  dateFormat: defaultDateFormat,
};

export default SwellGraph;
