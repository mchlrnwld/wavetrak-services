/** @prettier */

import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import { getMapTileUrl } from '@surfline/web-common';
import SwellRose from '../SwellRose';
import SwellMeasurements from '../SwellMeasurements';

import TooltipProvider from '../TooltipProvider';
import SwellGraph from './SwellGraph';

describe('components / SwellGraph', () => {
  const props = {
    utcOffset: -8,
    day: 0,
    lat: 33.654213041213,
    lon: -118.0032588192,
    overallMaxHeight: 1.3,
    units: 'FT',
    isSwellGraph: true,
    data: [
      {
        timestamp: 1518076800,
        swells: [
          {
            height: 0.7,
            period: 4,
            direction: 269,
            directionMin: 268,
            optimalScore: 0,
            index: 0,
          },
          {
            height: 1.3,
            period: 9,
            direction: 172,
            directionMin: 171,
            optimalScore: 0,
            index: 1,
          },
          {
            height: 0.7,
            period: 11,
            direction: 198,
            directionMin: 197,
            optimalScore: 0,
            index: 2,
          },
          {
            height: 0.3,
            period: 9,
            direction: 283,
            directionMin: 282,
            optimalScore: 0,
            index: 3,
          },
          {
            height: 0.3,
            period: 9,
            direction: 283,
            directionMin: 282,
            optimalScore: 0,
            index: 4,
          },
          {
            height: 0.3,
            period: 9,
            direction: 283,
            directionMin: 282,
            optimalScore: 0,
            index: 5,
          },
        ],
      },
      {
        timestamp: 1518076800 + 3600,
        swells: [
          {
            height: 0.7,
            period: 4,
            direction: 269,
            directionMin: 268,
            optimalScore: 0,
            index: 0,
          },
          {
            height: 1.3,
            period: 9,
            direction: 172,
            directionMin: 171,
            optimalScore: 0,
            index: 1,
          },
          {
            height: 0.7,
            period: 11,
            direction: 198,
            directionMin: 197,
            optimalScore: 0,
            index: 2,
          },
          {
            height: 0.3,
            period: 9,
            direction: 283,
            directionMin: 282,
            optimalScore: 0,
            index: 3,
          },
          {
            height: 0.3,
            period: 9,
            direction: 283,
            directionMin: 282,
            optimalScore: 0,
            index: 4,
          },
          {
            height: 0.3,
            period: 9,
            direction: 283,
            directionMin: 282,
            optimalScore: 0,
            index: 5,
          },
        ],
      },
    ],
    mapZoom: 7,
    showOptimalConditions: true,
    activeSwell: 3,
    device: { width: 1200, mobile: false },
    doLoadTreatment: () => 'on',
  };

  it('renders with tooltip', () => {
    const wrapper = mount(<SwellGraph {...props} />);
    const tooltipProvider = wrapper.find(TooltipProvider);
    const tooltipWrapper = shallow(tooltipProvider.prop('renderTooltip')(0));

    const map = tooltipWrapper.find('.quiver-swell-graph__tooltip-map');
    expect(map).to.have.length(1);
    expect(map.prop('style').background).to.equal(
      `url("${getMapTileUrl(152, 88, props.lat, props.lon, 7)}") center`
    );

    const rose = map.find(SwellRose);
    expect(rose).to.have.length(1);
    expect(rose.props()).to.deep.equal({
      overallMaxHeight: props.overallMaxHeight,
      swells: [props.data[0].swells[3]],
    });

    const swellMeasurementsList = tooltipWrapper.find(SwellMeasurements);
    expect(swellMeasurementsList).to.have.length(1);
    expect(swellMeasurementsList.at(0).props()).to.deep.equal({
      ...props.data[0].swells[3],
      units: 'FT',
      isEmptySwell: false,
    });
  });

  it('renders a different tooltip on mobile', () => {
    const wrapper = mount(
      <SwellGraph {...props} device={{ width: 340, mobile: true }} />
    );
    const tooltipProvider = wrapper.find(TooltipProvider);
    const tooltipWrapper = shallow(tooltipProvider.prop('renderTooltip')(0));

    const map = tooltipWrapper.find('.quiver-swell-graph__tooltip-map');
    expect(map).to.have.length(0);

    const rose = map.find(SwellRose);
    expect(rose).to.have.length(0);

    const swellMeasurementsList = tooltipWrapper.find(SwellMeasurements);
    expect(swellMeasurementsList).to.have.length(4);
    expect(swellMeasurementsList.at(3).props()).to.deep.equal({
      ...props.data[0].swells[3],
      units: 'FT',
      isEmptySwell: false,
    });
  });

  it('renders tooltip with different map zoom for subregion', () => {
    const propClone = { ...props };
    propClone.mapZoom = 5;
    const wrapper = mount(<SwellGraph {...propClone} />);
    const tooltipProvider = wrapper.find(TooltipProvider);
    const tooltipWrapper = shallow(tooltipProvider.prop('renderTooltip')(0));
    const map = tooltipWrapper.find('.quiver-swell-graph__tooltip-map');
    expect(map.prop('style').background).to.equal(
      `url("${getMapTileUrl(152, 88, props.lat, props.lon, 5)}") center`
    );
  });
});
