const splitSwellLines = (swellPoints) => {
  if (swellPoints.length === 0) return [];
  const splitPoints = swellPoints
    .map(({ hour }, index) => ({ hour, index }))
    .filter(({ hour }, index, points) => {
      if (index === 0) return true;
      return hour > points[index - 1].hour + 1;
    });
  const swellLines = splitPoints.map(({ index }, i) => {
    const endPoint = splitPoints[i + 1];
    const endIndex = endPoint ? endPoint.index : swellPoints.length;
    return swellPoints.slice(index, endIndex);
  });
  return swellLines;
};

export default splitSwellLines;
