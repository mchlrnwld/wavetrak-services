import { expect } from 'chai';
import splitSwellLines from './splitSwellLines';

describe('components / SwellGraph / splitSwellLines', () => {
  it('returns an empty array when there are no swell points', () => {
    expect(splitSwellLines([])).to.deep.equal([]);
  });

  it('returns an array of swell lines', () => {
    const swellPoints = [
      { hour: 1 },
      { hour: 2 },
      { hour: 3 },
      { hour: 5 },
      { hour: 7 },
      { hour: 8 },
      { hour: 15 },
    ];
    expect(splitSwellLines(swellPoints)).to.deep.equal([
      [{ hour: 1 }, { hour: 2 }, { hour: 3 }],
      [{ hour: 5 }],
      [{ hour: 7 }, { hour: 8 }],
      [{ hour: 15 }],
    ]);
  });
});
