import React from 'react';
import PropTypes from 'prop-types';
import { format } from 'date-fns';
import { getLocalDate } from '@surfline/web-common';
import classNames from 'classnames';

/**
 *
 * @param {object} props
 * @param {number} props.timestamp
 * @param {number} props.utcOffset
 * @param {number} props.columnNumber
 * @param {number?} props.currentColumn
 * @param {number?} props.activeColumn
 * @param {boolean?} props.showTimeLabels
 */
const GraphTimestamp = ({
  timestamp,
  utcOffset,
  columnNumber,
  currentColumn,
  activeColumn,
  showTimeLabels,
}) => {
  const isCurrentColumn = columnNumber === currentColumn;
  const isActiveColumn = columnNumber === activeColumn;
  const showCurrentColumn = isCurrentColumn && !isActiveColumn;

  const timeClass = classNames({
    'quiver-continuous-graph-container__graph-time__hour': true,
    'quiver-continuous-graph-container__graph-time__hour--active':
      isActiveColumn,
  });

  return (
    <>
      {showCurrentColumn && (
        <div className="quiver-continuous-graph-container__graph-time__current" />
      )}
      {showTimeLabels && (
        <div key={timestamp} className={timeClass}>
          {format(getLocalDate(timestamp, utcOffset), 'haaa')}
        </div>
      )}
    </>
  );
};

GraphTimestamp.propTypes = {
  timestamp: PropTypes.number.isRequired,
  utcOffset: PropTypes.number.isRequired,
  columnNumber: PropTypes.number.isRequired,
  currentColumn: PropTypes.number,
  activeColumn: PropTypes.number,
  showTimeLabels: PropTypes.bool,
};

GraphTimestamp.defaultProps = {
  currentColumn: null,
  activeColumn: null,
  showTimeLabels: false,
};

export default GraphTimestamp;
