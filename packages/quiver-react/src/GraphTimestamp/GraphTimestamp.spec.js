import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { describe, it } from 'mocha';
import GraphTimestamp from './GraphTimestamp';

describe('components / ContinuousGraphContainer / GraphTimestamp', () => {
  // 2021-01-01 08:00 GMT
  // 2020-01-01 1:00 PDT
  // 2021-01-01 00:00 PST
  const timestamp = 1577865600;
  const pstOffset = -8;
  const pdtOffset = -7;

  it('should render a valid local date using the UTC Offset', () => {
    const pstWrapper = mount(
      <GraphTimestamp
        timestamp={timestamp}
        utcOffset={pstOffset}
        columnNumber={1}
        showTimeLabels
      />
    );

    const pdtWrapper = mount(
      <GraphTimestamp
        timestamp={timestamp}
        utcOffset={pdtOffset}
        columnNumber={1}
        showTimeLabels
      />
    );

    expect(pstWrapper.text()).to.be.equal('12am');
    expect(pdtWrapper.text()).to.be.equal('1am');
  });

  it('should not render a time label without `showTimeLabels`', () => {
    const wrapper = mount(
      <GraphTimestamp
        timestamp={timestamp}
        utcOffset={pstOffset}
        columnNumber={1}
      />
    );

    expect(
      wrapper.find('.quiver-continuous-graph-container__graph-time__hour')
    ).to.have.length(0);
  });

  it('should render the current column', () => {
    const wrapper = mount(
      <GraphTimestamp
        timestamp={timestamp}
        utcOffset={pstOffset}
        columnNumber={1}
        currentColumn={1}
      />
    );

    expect(
      wrapper.find('.quiver-continuous-graph-container__graph-time__current')
    ).to.have.length(1);
  });
  it('should not render the current column if it is the same as the active column', () => {
    const wrapper = mount(
      <GraphTimestamp
        timestamp={timestamp}
        utcOffset={pstOffset}
        columnNumber={1}
        currentColumn={1}
        activeColumn={1}
        showTimeLabels
      />
    );

    expect(
      wrapper.find('.quiver-continuous-graph-container__graph-time__current')
    ).to.have.length(0);
    expect(
      wrapper.find(
        '.quiver-continuous-graph-container__graph-time__hour--active'
      )
    ).to.have.length(1);
  });
});
