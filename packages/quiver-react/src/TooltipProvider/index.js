import withDevice from '../withDevice';
import TooltipProvider from './TooltipProvider';

export default withDevice(TooltipProvider);
