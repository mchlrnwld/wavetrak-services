/** @prettier */
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames';
import { TABLET_LARGE_WIDTH } from '@surfline/web-common';
import devicePropType from '../PropTypes/device';

const OFFSET_FROM_CURSOR = 8;

// Useful shim for being able to use `Element` for ref proptypes
const SafeHTMLElement =
  typeof Element === 'undefined' ? function () {} : Element;

class TooltipProvider extends Component {
  constructor() {
    super();
    this.state = { tooltip: null };
  }

  componentDidUpdate(prevProps) {
    const {
      currentMarkerXPercentage,
      isWindGraph,
      isSwellGraph,
      isSurfGraph,
      device,
      isHourly,
    } = this.props;
    const didCalculateDeviceSize = !prevProps.device.width && device.width;
    const isMobile = device.width < TABLET_LARGE_WIDTH;
    if (
      didCalculateDeviceSize &&
      isMobile &&
      (isWindGraph || isSwellGraph || isSurfGraph)
    ) {
      this.showTooltip(0, 0, true, currentMarkerXPercentage);
    }
    if (prevProps.isHourly !== isHourly) {
      this.hideTooltip();
    }
  }

  handleMouseMove = ({ clientX, clientY }) => {
    this.showTooltip(clientX, clientY);
  };

  handleTouch = ({ changedTouches }) => {
    const [{ clientX, clientY }] = changedTouches;
    this.showTooltip(clientX, clientY, true);
  };

  hideTooltip = () => {
    const { onHideTooltip } = this.props;
    if (onHideTooltip) onHideTooltip();
    this.setState({ tooltip: null });
  };

  showTooltip = (
    clientX,
    clientY,
    touch = false,
    currentMarkerXPercentage = null
  ) => {
    const {
      onShowTooltip,
      isWindGraph,
      isSwellGraph,
      isSurfGraph,
      device,
      showMobileHourlyWindGraph,
      isBarGraph,
      columns,
      parentRef,
    } = this.props;

    const isMobile = device.width < TABLET_LARGE_WIDTH;
    const isUpdatedGraph = isWindGraph || isSwellGraph || isSurfGraph;
    const showMobileTooltip = isMobile && isUpdatedGraph;
    const box = this.ref.getBoundingClientRect();
    const currentMarkerExists =
      currentMarkerXPercentage || currentMarkerXPercentage === 0;
    let xFromLeft = currentMarkerExists
      ? box.width * currentMarkerXPercentage
      : clientX - box.left;

    /**
     * On the mobile bar graph tooltip we want to center the tooltip in the selected column
     * To do this we can figure out which column is selected using the `xFromLeft` value
     * and then adjust the `xFromLeft` to be that new centered value
     */
    if (isBarGraph) {
      const columnSize = box.width / columns;
      const currentColumn = Math.floor(xFromLeft / columnSize) + 1;
      xFromLeft = currentColumn * columnSize - columnSize / 2;
    }

    const yFromTop = clientY - box.top;

    if (
      !showMobileTooltip &&
      (yFromTop <= 0 || xFromLeft > box.width || yFromTop > box.height)
    ) {
      return this.hideTooltip();
    }

    const xPercentage = currentMarkerExists
      ? currentMarkerXPercentage
      : xFromLeft / box.width;

    if (onShowTooltip) onShowTooltip(xPercentage);

    const style = {};

    if (touch || showMobileTooltip) {
      style.top = 0;

      if (isWindGraph) {
        style.top = showMobileHourlyWindGraph ? -48 : -64;
        style.left = xFromLeft + OFFSET_FROM_CURSOR;
      } else if (isSwellGraph) {
        style.top = -100;
        style.left = 0;
      } else if (isSurfGraph) {
        style.top = -62;
        style.left = xFromLeft;
      } else if (xPercentage < 0.5) {
        style.right = 0;
      } else {
        style.left = 0;
      }
    } else {
      style.top = yFromTop;
      style.left = xFromLeft + OFFSET_FROM_CURSOR;
    }

    const tooltip = {
      style,
      xPercentage,
    };

    return this.setState({ tooltip }, () => {
      if (touch || showMobileTooltip) {
        if (isWindGraph || isSurfGraph) {
          const tooltipWidth = this.tooltipRef.getBoundingClientRect().width;
          let tooltipLeft = style.left;

          // This is used to center the tooltip above the selected column
          tooltipLeft -= tooltipWidth / 2;

          // Account for the tooltip going off screen
          const tooltipRightEdge = tooltipLeft + tooltipWidth;
          const isOutsideBox = tooltipRightEdge > box.width;
          if (isOutsideBox) {
            tooltipLeft -= tooltipRightEdge - box.width;
          } else if (tooltipLeft < 0) {
            tooltipLeft = 0;
          }
          this.tooltipRef.style.left = `${tooltipLeft}px`;
          this.tooltipRef.style.transform = null;
        } else {
          this.tooltipRef.style.transform = null;
        }
      } else {
        const minX = this.tooltipRef.offsetWidth + 30;

        const tooltipLeftEdge = clientX - this.tooltipRef.offsetWidth;
        const shouldStayWithinParent =
          parentRef && tooltipLeftEdge < parentRef.getBoundingClientRect().left;
        if (clientX <= minX) {
          this.tooltipRef.style.transform = 'translateY(-100%)';
        } else if (shouldStayWithinParent) {
          // If we pass a parent ref, then it means we want the tooltip to stay within the X axis
          // bounds of the parent. So above we have calculated the `tooltipLeftEdge` and comapre it
          // to the parent left edge
          this.tooltipRef.style.transform = 'translateY(-100%)';
        } else {
          this.tooltipRef.style.transform = `translate(-${
            this.tooltipRef.offsetWidth + 2 * OFFSET_FROM_CURSOR
          }px, -100%)`;
        }
      }
    });
  };

  className = () => {
    const { tooltip } = this.state;
    return classNames({
      'quiver-tooltip-provider': true,
      'quiver-tooltip-provider--hide': !tooltip,
    });
  };

  render() {
    const { tooltip } = this.state;
    const {
      renderTooltip,
      children,
      device,
      isWindGraph,
      isSwellGraph,
      isSurfGraph,
    } = this.props;
    const isMobile = device.width < TABLET_LARGE_WIDTH;
    const isUpdatedGraph = isWindGraph || isSwellGraph || isSurfGraph;
    const showMobileTooltip = isMobile && isUpdatedGraph;
    return (
      <div
        ref={(el) => {
          this.ref = el;
        }}
        className={this.className()}
        onMouseMove={this.handleMouseMove}
        onMouseLeave={showMobileTooltip ? null : this.hideTooltip}
        onTouchStart={this.handleTouch}
        onTouchMove={this.handleTouch}
        onTouchEnd={showMobileTooltip ? null : this.hideTooltip}
      >
        {children}
        <div
          className="quiver-tooltip-provider__tooltip"
          ref={(el) => {
            this.tooltipRef = el;
          }}
          style={tooltip ? tooltip.style : null}
        >
          {tooltip ? renderTooltip(tooltip.xPercentage) : null}
        </div>
      </div>
    );
  }
}

TooltipProvider.propTypes = {
  renderTooltip: PropTypes.func.isRequired,
  onShowTooltip: PropTypes.func,
  onHideTooltip: PropTypes.func,
  children: PropTypes.node.isRequired,
  isWindGraph: PropTypes.bool,
  showMobileHourlyWindGraph: PropTypes.bool,
  isSwellGraph: PropTypes.bool,
  isBarGraph: PropTypes.bool,
  isSurfGraph: PropTypes.bool,
  isHourly: PropTypes.bool,
  device: devicePropType.isRequired,
  currentMarkerXPercentage: PropTypes.number,
  columns: PropTypes.number,
  parentRef: PropTypes.instanceOf(SafeHTMLElement),
};

TooltipProvider.defaultProps = {
  currentMarkerXPercentage: 0,
  columns: 0,
  onShowTooltip: null,
  onHideTooltip: null,
  isHourly: false,
  isWindGraph: false,
  showMobileHourlyWindGraph: false,
  isSwellGraph: false,
  isBarGraph: false,
  isSurfGraph: false,
  parentRef: null,
};

export default TooltipProvider;
