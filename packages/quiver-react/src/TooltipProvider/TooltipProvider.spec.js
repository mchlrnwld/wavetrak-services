/** @prettier */

import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import TooltipProvider from './TooltipProvider';

describe('components / TooltipProvider', () => {
  const renderTooltip = (xPercentage) => <div>{xPercentage}</div>;

  it('renders tooltip with xPercentage if tooltip is in state', () => {
    const wrapper = mount(
      <TooltipProvider
        day={1}
        device={{ mobile: false, width: 1200 }}
        currentMarkerXPercentage={0.5}
        renderTooltip={renderTooltip}
      >
        <div>Test</div>
      </TooltipProvider>
    );
    const hiddenTooltip = wrapper.find('.quiver-tooltip-provider__tooltip');
    expect(hiddenTooltip).to.have.length(1);
    expect(hiddenTooltip.children()).to.have.length(0);

    wrapper.setState({ tooltip: { xPercentage: 0.5 } });
    const tooltip = wrapper.find('.quiver-tooltip-provider__tooltip');
    expect(tooltip).to.have.length(1);
    const tooltipChildren = tooltip.children();
    expect(tooltipChildren).to.have.length(1);
    expect(tooltipChildren.html()).to.equal('<div>0.5</div>');
  });

  it('renders tooltip by default on mobile wind graph', () => {
    const wrapper = mount(
      <TooltipProvider
        isWindGraph
        device={{ mobile: true, width: null }}
        currentMarkerXPercentage={0.5}
        renderTooltip={renderTooltip}
      >
        <div>Test</div>
      </TooltipProvider>
    );

    wrapper.setProps({
      device: {
        mobile: true,
        width: 400,
      },
    });
    wrapper.update();
    const tooltip = wrapper.find('.quiver-tooltip-provider__tooltip');
    expect(tooltip).to.have.length(1);
    const tooltipChildren = tooltip.children();
    expect(tooltipChildren).to.have.length(1);
    expect(tooltipChildren.html()).to.equal('<div>0.5</div>');
  });
});
