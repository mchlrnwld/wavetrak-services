import React from 'react';
import TooltipProvider from './TooltipProvider';

const renderTooltip = (xPercentage) => <div>{xPercentage}</div>;

export default {
  title: 'TooltipProvider',
};

export const Default = () => (
  <TooltipProvider
    day={1}
    device={{ mobile: false, width: 1200 }}
    currentMarkerXPercentage={0.5}
    renderTooltip={renderTooltip}
  >
    <div>Test</div>
  </TooltipProvider>
);

Default.story = {
  name: 'default',
};

export const WindGraph = () => (
  <TooltipProvider
    isWindGraph
    device={{ mobile: true, width: null }}
    currentMarkerXPercentage={0.5}
    renderTooltip={renderTooltip}
  >
    <div>Test</div>
  </TooltipProvider>
);

WindGraph.story = {
  name: 'wind graph',
};
