import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import sinon from 'sinon';
import RecentlyVisited from './RecentlyVisited';
import RenderSpot from '../RenderSpot';
import RenderSubregion from '../RenderSubregion';
import * as fetchSpotReportViews from '../helpers/fetchSpotReportViews';

describe('Recently Visited', () => {
  const serviceConfig = { serviceUrl: 'surflinetest.com' };
  const multipleSpotItems = [
    {
      _id: 'spotId1',
      name: 'spot1',
    },
    {
      _id: 'spotId2',
      name: 'spot2',
    },
  ];

  beforeEach(() => {
    sinon.stub(fetchSpotReportViews, 'default');
  });

  afterEach(() => {
    fetchSpotReportViews.default.restore();
  });

  it('should update component state on click', () => {
    fetchSpotReportViews.default.resolves({ data: multipleSpotItems });
    const wrapper = mount(<RecentlyVisited />);

    expect(
      wrapper.find('.quiver-recently-visited__header').prop('className')
    ).not.to.contain('quiver-recently-visited__header--open');
    wrapper.find('.quiver-recently-visited__header').simulate('click');
    wrapper.update();
    expect(
      wrapper.find('.quiver-recently-visited__header').prop('className')
    ).to.contain('quiver-recently-visited__header--open');
  });

  it('renders a different header when open or closed', () => {
    const wrapper = shallow(<RecentlyVisited />);
    wrapper.setState({ open: true });
    const openHeaderWrapper = wrapper.find('.quiver-recently-visited__header');
    wrapper.setState({ open: false });
    const closedHeaderWrapper = wrapper.find(
      '.quiver-recently-visited__header'
    );
    expect(openHeaderWrapper.prop('className')).to.contain(
      'quiver-recently-visited__header--open'
    );
    expect(closedHeaderWrapper.prop('className')).not.to.contain(
      'quiver-recently-visited__header--open'
    );
  });

  it('renders an open or closed list', () => {
    fetchSpotReportViews.default.resolves({ data: multipleSpotItems });
    const wrapper = shallow(<RecentlyVisited />);
    wrapper.setState({ open: true });
    const listWrapper = wrapper.find('.quiver-recently-visited__list');
    wrapper.setState({ open: false });
    const hiddenListWrapper = wrapper.find('.quiver-recently-visited__list');
    expect(listWrapper.prop('className')).not.to.contain(
      'quiver-recently-visited__list--hidden'
    );
    expect(hiddenListWrapper.prop('className')).to.contain(
      'quiver-recently-visited__list--hidden'
    );
  });

  it('renders CTA only if the user is not logged in', () => {
    fetchSpotReportViews.default.resolves({ data: multipleSpotItems });
    const loggedInWrapper = shallow(<RecentlyVisited loggedIn />);
    const noCTAWrapper = loggedInWrapper.find(
      '.quiver-recently-visited__list__sign-in'
    );
    expect(noCTAWrapper).to.have.length(0);
    const loggedOutWrapper = shallow(<RecentlyVisited loggedIn={false} />);
    const CTAWrapper = loggedOutWrapper.find(
      '.quiver-recently-visited__list__sign-in'
    );
    expect(CTAWrapper).to.have.length(1);
  });

  it('gets spot report views if listType is spots', () => {
    const spotItems = [{ _id: 'spotId', name: 'Spot Name' }];
    fetchSpotReportViews.default.resolves({ data: spotItems });
    mount(
      <RecentlyVisited
        loggedIn
        listType="spots"
        items={spotItems}
        serviceConfig={serviceConfig}
      />
    );
    expect(fetchSpotReportViews.default).to.have.been.calledOnce();
    expect(fetchSpotReportViews.default).to.have.been.calledWithExactly(
      ['spotId'],
      serviceConfig,
      false
    );
  });

  it('renders spots if listType is spots', async () => {
    fetchSpotReportViews.default.resolves({ data: multipleSpotItems });
    const wrapper = mount(
      <RecentlyVisited
        loggedIn
        listType="spots"
        items={multipleSpotItems}
        serviceConfig={serviceConfig}
      />
    );

    // Allow us to flush the event loop so that the async componentDidMount can complete
    await (() => new Promise((resolve) => setImmediate(resolve)))();

    const spotsWrapper = wrapper.update().find(RenderSpot);
    expect(spotsWrapper).to.have.length(2);
  });

  it('renders subregions if listType is subregions', async () => {
    const subregionItems = [
      {
        _id: 'subregionId1',
        name: 'subregion1',
      },
      {
        _id: 'subregionId2',
        name: 'subregion2',
      },
    ];
    const wrapper = await mount(
      <RecentlyVisited
        loggedIn
        listType="subregions"
        items={subregionItems}
        serviceConfig={serviceConfig}
      />
    );
    const subregionsWrapper = wrapper.find(RenderSubregion);
    expect(subregionsWrapper).to.have.length(2);
  });

  it('renders static links', () => {
    const staticLinks = [
      { href: 'www.1.com', display: 'first link' },
      { href: 'www.2.com', display: 'second link' },
      { href: 'www.3.com', display: 'third link' },
    ];
    const wrapper = shallow(
      <RecentlyVisited
        loggedIn
        listType="subregions"
        staticLinks={staticLinks}
      />
    );
    const linkListWrapper = wrapper.find(
      '.quiver-recently-visited__static-links'
    );
    const linksWrapper = linkListWrapper.find(
      '.quiver-recently-visited__static-link'
    );
    expect(linksWrapper).to.have.length(3);
    expect(linksWrapper.at(0).html()).to.contain('first link');
    expect(linksWrapper.at(1).html()).to.contain('second link');
    expect(linksWrapper.at(2).html()).to.contain('third link');
  });
});
