import React, { Component } from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';
import RenderSpot from '../RenderSpot';
import RenderSubregion from '../RenderSubregion';
import Chevron from '../Chevron';
import fetchSpotReportViews from '../helpers/fetchSpotReportViews';
import taxonomyNodePropType from '../PropTypes/taxonomyNodePropType';
import serviceConfigurationPropType from '../PropTypes/serviceConfigurationPropType';

class RecentlyVisited extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nodes: [],
      open: false,
    };
  }

  componentDidMount = async () => {
    const { items, listType, serviceConfig, sevenRatings } = this.props;
    const nodes = items || [];
    if (listType === 'subregions') {
      this.setState({
        nodes,
      });
    } else {
      try {
        const spotIds = nodes.map((spot) => spot._id);
        const { data } = await fetchSpotReportViews(
          spotIds,
          serviceConfig,
          sevenRatings
        );
        this.setState({
          nodes: nodes.map((item) => ({
            ...item,
            ...data.find((d) => d._id === item._id),
          })),
        });
      } catch (error) {
        window?.newrelic?.noticeError(error);
      }
    }
  };

  toggleShowList = () => {
    const { open } = this.state;
    this.setState({ open: !open });
  };

  headerClassName = (open) =>
    classNames({
      'quiver-recently-visited__header': true,
      'quiver-recently-visited__header--open': open,
    });

  listClassName = (open) =>
    classNames({
      'quiver-recently-visited__list': true,
      'quiver-recently-visited__list--hidden': !open,
    });

  render() {
    const {
      loggedIn,
      listType,
      staticLinks,
      onClickLink,
      signInUrl,
      sevenRatings,
    } = this.props;
    const { nodes, open } = this.state;
    return (
      <div className="quiver-recently-visited">
        <button
          type="button"
          className={this.headerClassName(open)}
          onClick={this.toggleShowList}
        >
          <div className="quiver-recently-visited__header__button-wrapper">
            Recently Visited
            <Chevron direction="right" />
          </div>
        </button>
        <div className={this.listClassName(open)}>
          {(() => {
            if (!loggedIn) {
              return (
                <div className="quiver-recently-visited__list__sign-in">
                  <a
                    href={signInUrl}
                    className="quiver-recently-visited__list__sign-in__link"
                    onClick={() =>
                      onClickLink(
                        {
                          locationCategory: 'Recently Visited',
                          clickEndLocation: 'sign-in',
                          destinationUrl: signInUrl,
                          completion: true,
                          linkName: 'Sign in or create account',
                        },
                        true
                      )
                    }
                  >
                    Sign in or create account
                  </a>
                  {` to view your recently visited ${listType}`}
                </div>
              );
            }
            if (listType === 'subregions') {
              return nodes
                .filter((node) => !!node._id && !!node.name)
                .map((node) => (
                  <RenderSubregion
                    subregion={node}
                    key={node._id}
                    onClickLink={(properties, closeMobileMenu) =>
                      onClickLink(
                        {
                          ...properties,
                          locationCategory: 'Recently Visited',
                        },
                        closeMobileMenu
                      )
                    }
                  />
                ));
            }
            if (listType === 'spots') {
              return nodes
                .filter((node) => !!node._id && !!node.name)
                .map((node) => (
                  <RenderSpot
                    spot={node}
                    key={node._id}
                    onClickLink={(properties, closeMobileMenu) =>
                      onClickLink(
                        {
                          ...properties,
                          locationCategory: 'Recently Visited',
                        },
                        closeMobileMenu
                      )
                    }
                    sevenRatings={sevenRatings}
                  />
                ));
            }
            return null;
          })()}
        </div>
        <div className="quiver-recently-visited__static-links">
          {(() => {
            if (listType === 'subregions') {
              return (
                <div className="quiver-recently-visited__header">
                  Forecast Tools
                </div>
              );
            }
            return null;
          })()}
          {staticLinks.map((link) => (
            <a
              className="quiver-recently-visited__static-link"
              href={link.href}
              key={link.display}
              target={link.newWindow ? '_blank' : ''}
              rel="noreferrer"
              onClick={() =>
                onClickLink(
                  {
                    locationCategory: 'Tools',
                    clickEndLocation: 'Tools',
                    destinationUrl: link.href,
                    completion: true,
                    linkName: link.display,
                  },
                  true
                )
              }
            >
              {link.display}
            </a>
          ))}
        </div>
      </div>
    );
  }
}

RecentlyVisited.propTypes = {
  loggedIn: PropTypes.bool,
  items: PropTypes.arrayOf(taxonomyNodePropType).isRequired,
  listType: PropTypes.string,
  serviceConfig: serviceConfigurationPropType,
  staticLinks: PropTypes.arrayOf(
    PropTypes.shape({
      href: PropTypes.string,
      display: PropTypes.string,
    })
  ),
  signInUrl: PropTypes.string.isRequired,
  onClickLink: PropTypes.func,
  sevenRatings: PropTypes.bool,
};

RecentlyVisited.defaultProps = {
  loggedIn: false,
  listType: '',
  serviceConfig: {},
  staticLinks: [],
  onClickLink: null,
  sevenRatings: false,
};

export default RecentlyVisited;
