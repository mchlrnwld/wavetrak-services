import React from 'react';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import { Motion, spring } from 'react-motion';
import SwellRosePetalAnimator from './SwellRosePetalAnimator';

describe('components / SwellRose', () => {
  describe('SwellRosePetalAnimator', () => {
    it('adds renders a wind arrow with Motion', () => {
      const props = {
        swellIndex: 1,
        arcRadius: 30,
        direction: 133.87,
      };
      const wrapper = shallow(<SwellRosePetalAnimator {...props} />);

      const motion = wrapper.find(Motion);
      expect(motion).to.have.length(1);
      expect(motion.prop('defaultStyle')).to.deep.equal({
        arcRadius: 0,
        direction: props.direction,
      });
      expect(motion.prop('style')).to.deep.equal({
        arcRadius: spring(props.arcRadius),
        direction: spring(props.direction),
      });

      const children = motion.prop('children');
      const values = {
        arcRadius: 150,
        direction: 100,
      };
      const swellRosePetal = mount(children(values)).find('path');
      expect(swellRosePetal.prop('className')).to.contain(
        'quiver-swell-rose__petal'
      );
      wrapper.unmount();
    });
  });
});
