import PropTypes from 'prop-types';
import React from 'react';
import { Motion, spring } from 'react-motion';
import SwellRosePetal from './SwellRosePetal';

const SwellRosePetalAnimator = ({ swellIndex, arcRadius, direction }) => (
  <Motion
    defaultStyle={{
      arcRadius: 0,
      direction,
    }}
    style={{
      arcRadius: spring(arcRadius),
      direction: spring(direction),
    }}
  >
    {(value) => (
      <SwellRosePetal
        swellIndex={swellIndex}
        arcRadius={value.arcRadius}
        direction={value.direction}
      />
    )}
  </Motion>
);

SwellRosePetalAnimator.propTypes = {
  arcRadius: PropTypes.number.isRequired,
  direction: PropTypes.number.isRequired,
  swellIndex: PropTypes.number.isRequired,
};

export default SwellRosePetalAnimator;
