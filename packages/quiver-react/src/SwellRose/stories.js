import React from 'react';
import SwellRose from './SwellRose';

export default {
  title: 'SwellRose',
};

export const Default = () => (
  <div className="quiver-swell-graph">
    <SwellRose
      swells={[
        { height: 2, period: 15, direction: 210, index: 0 },
        { height: 1, direction: 320, period: 20, index: 1 },
      ]}
      overallMaxHeight={2}
    />
  </div>
);
