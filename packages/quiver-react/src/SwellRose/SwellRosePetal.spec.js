import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import SwellRosePetal from './SwellRosePetal';

describe('components / SwellRose', () => {
  describe('SwellRosePetal', () => {
    it('renders with color and is rotated by degrees around the center', () => {
      const wrapper = mount(
        <SwellRosePetal swellIndex={0} arcRadius={20} direction={90} />
      );

      const group = wrapper.find('g');
      expect(group.prop('transform')).to.equal(
        'translate(60, 60) rotate(75 0 0)'
      );

      const path = group.find('path');
      expect(path.prop('className')).to.equal('quiver-swell-rose__petal--0');
      wrapper.unmount();
    });
  });
});
