import PropTypes from 'prop-types';
import React from 'react';
import { arc } from 'd3-shape';

const makeSwellRosePetal = (arcRadius) => {
  const swellArc = arc()
    .outerRadius(arcRadius)
    .innerRadius(0)
    .startAngle(0)
    .endAngle(Math.PI / 6);
  return swellArc();
};

const SwellRosePetal = ({ swellIndex, arcRadius, direction }) => {
  const swellPath = makeSwellRosePetal(arcRadius);
  return (
    <g transform={`translate(60, 60) rotate(${direction - 15} 0 0)`}>
      <path
        className={`quiver-swell-rose__petal--${swellIndex}`}
        d={swellPath}
      />
    </g>
  );
};

SwellRosePetal.propTypes = {
  arcRadius: PropTypes.number.isRequired,
  direction: PropTypes.number.isRequired,
  swellIndex: PropTypes.number.isRequired,
};

export default SwellRosePetal;
