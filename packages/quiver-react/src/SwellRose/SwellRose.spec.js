import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SwellRosePetalAnimator from './SwellRosePetalAnimator';
import SwellRose from './SwellRose';

describe('components / SwellRose', () => {
  it('renders three SwellRosePetalAnimators sorted by swell index', () => {
    const swells = [
      { height: 0.2, direction: 164.53, period: 9, index: 0 },
      { height: 0.2, direction: 181.41, period: 16, index: 2 },
      { height: 0.1, direction: 262.97, period: 12, index: 3 },
    ];
    const wrapper = shallow(
      <SwellRose overallMaxHeight={10} swells={swells} />
    );
    const swellRosePetalAnimators = wrapper.find(SwellRosePetalAnimator);
    expect(swellRosePetalAnimators).to.have.length(3);
    expect(swellRosePetalAnimators.at(0).props()).to.deep.equal({
      swellIndex: 0,
      arcRadius: 5.65685424949238,
      direction: 164.53,
    });
    expect(swellRosePetalAnimators.at(1).props()).to.deep.equal({
      swellIndex: 2,
      arcRadius: 5.65685424949238,
      direction: 181.41,
    });
    expect(swellRosePetalAnimators.at(2).props()).to.deep.equal({
      swellIndex: 3,
      arcRadius: 4,
      direction: 262.97,
    });
  });
});
