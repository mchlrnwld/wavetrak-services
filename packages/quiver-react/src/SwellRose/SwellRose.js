import PropTypes from 'prop-types';
import React from 'react';
import { scalePowerWaveHeight } from '@surfline/web-common';
import SwellRosePetalAnimator from './SwellRosePetalAnimator';
import CompassRose from '../CompassRose';
import { swellsPropTypes } from '../PropTypes/readings';

const MAX_PIXELS = 40;

const SwellRose = ({ overallMaxHeight, swells }) => (
  <div className="quiver-swell-rose">
    <CompassRose />
    <svg
      className="quiver-swell-rose__rose"
      viewBox="0 0 120 120"
      preserveAspectRatio="xMinYMin"
    >
      {swells.map((swell) => (
        <SwellRosePetalAnimator
          key={swell.index}
          swellIndex={swell.index}
          arcRadius={scalePowerWaveHeight(
            overallMaxHeight,
            swell.height,
            MAX_PIXELS
          )}
          direction={swell.direction}
        />
      ))}
    </svg>
  </div>
);

SwellRose.propTypes = {
  swells: swellsPropTypes.isRequired,
  overallMaxHeight: PropTypes.number.isRequired,
};

export default SwellRose;
