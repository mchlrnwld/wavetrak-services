import React from 'react';
import Spinner from './index';

export default {
  title: 'Spinner',
};

export const Default = () => <Spinner />;

Default.story = {
  name: 'default',
};

export const Error = () => <Spinner error />;

Error.story = {
  name: 'error',
};
