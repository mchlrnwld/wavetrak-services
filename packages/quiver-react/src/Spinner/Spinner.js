import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';

const getClassNames = (error) => {
  const classes = {};
  classes['quiver-spinner'] = true;
  classes['quiver-spinner--error'] = error;
  return classNames(classes);
};

/**
 * @param {object} props
 * @param {boolean} [props.error=false]
 * @returns {JSX.Element}
 */
const Spinner = ({ error }) => (
  <svg
    preserveAspectRatio="xMinYMin meet"
    viewBox="0 0 17 19"
    className={getClassNames(error)}
  >
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-112.000000, -18.000000)" fill="#FFFFFF">
        <path
          d="M120.206127,36.9991844 C119.456147,36.988462 118.730774,36.8807022 118.040772,
             36.6855551 C121.797335,35.7280475 124.607067,32.2116462 124.661406,27.9875686
             C124.676272,26.7330513 125.660525,25.7299736 126.860595,25.7482017 C128.057589,
             25.7653575 129.015697,26.7941689 128.999805,28.0454694 C128.934188,33.0597857
             124.998205,37.0656631 120.206127,36.9991844 Z M114.141981,29.2523738 C112.942424,
             29.2336096 111.983803,28.2074788 112.000207,26.9540338 C112.064799,21.9402536
             116.000783,17.9327679 120.793886,18.0008549 C121.542327,18.0094328 122.268726,
             18.1204093 122.960266,18.3160926 C119.202165,19.2709196 116.393458,22.7841041
             116.338607,27.0135429 C116.323228,28.266988 115.338975,29.2695296 114.141981,
             29.2523738 Z"
        />
      </g>
    </g>
  </svg>
);

Spinner.propTypes = {
  error: PropTypes.bool,
};

Spinner.defaultProps = {
  error: false,
};

export default Spinner;
