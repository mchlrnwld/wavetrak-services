import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Spinner from './Spinner';

describe('Spinner', () => {
  it('should render svg into DOM', () => {
    const wrapper = shallow(<Spinner />);

    const spinner = wrapper.find('.quiver-spinner');
    expect(spinner).to.have.length(1);
  });

  it('should render error', () => {
    const wrapper = shallow(<Spinner error />);

    const spinnerError = wrapper.find('.quiver-spinner--error');
    expect(spinnerError).to.have.length(1);
  });
});
