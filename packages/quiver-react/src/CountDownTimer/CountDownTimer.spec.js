import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import CountDownTimer from './CountDownTimer';

describe('CountDownTimer', () => {
  const props = {
    countTime: 10,
  };

  it('should render a span', () => {
    const wrapper = mount(<CountDownTimer {...props} />);
    expect(wrapper.html()).to.contain('<span>');
  });

  it('should render count time within range', () => {
    const wrapper = mount(<CountDownTimer {...props} />);
    expect(parseInt(wrapper.text(), 10)).to.be.within(0, props.countTime);
  });
});
