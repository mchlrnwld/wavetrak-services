import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import ModalContainer from '../ModalContainer';

class EmailVerificationModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showResend: true,
    };
  }

  clickedResendEmail = () => {
    const { doTriggerEmail } = this.props;
    doTriggerEmail();
    this.setState({
      showResend: false,
    });
  };

  render() {
    const { showResend } = this.state;
    const {
      user: { email },
    } = this.props;
    return (
      <ModalContainer isOpen contentLabel="Email Verification">
        <div className="quiver-email-verification-modal">
          <div>
            <h4>{showResend ? 'Verify Your Email' : 'Email Sent'}</h4>
          </div>
          <div className="quiver-email-verification-modal__text">
            Check
            <span className="quiver-email-verification-modal__text__email">
              {email}
            </span>{' '}
            for a verification email from Surfline, and follow the link to
            verify your account.
          </div>
          <div className="quiver-email-verification-modal__text">
            Make sure to check your spam folder.
          </div>
          {showResend ? (
            <div>
              <div className="quiver-email-verification-modal__text">
                Didn`&#39;`t receive an email?
              </div>
              <Button onClick={() => this.clickedResendEmail()}>
                Resend Verification Email
              </Button>
            </div>
          ) : (
            <div className="quiver-email-verification-modal__text">
              If you are unable to recieve this email, please contact
              <a
                href="mailto:support@surfline.com"
                className="quiver-email-verification-modal__text__link"
              >
                support@surfline.com
              </a>
            </div>
          )}
        </div>
      </ModalContainer>
    );
  }
}

EmailVerificationModal.propTypes = {
  doTriggerEmail: PropTypes.func.isRequired,
  user: PropTypes.shape({
    email: PropTypes.string,
  }).isRequired,
};

export default EmailVerificationModal;
