import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import EmailVerificationModal from '.';
import Button from '../Button';
import ModalContainer from '../ModalContainer';

describe('EmailVerificationModal', () => {
  const props = {
    user: {
      email: 'mmalchal@surfline.com',
    },
    doTriggerEmail: () => {},
  };

  it('should render a email verification modal', () => {
    const wrapper = shallow(<EmailVerificationModal {...props} />);
    const modalContainerWrapper = wrapper.find(ModalContainer);
    expect(modalContainerWrapper).to.have.length(1);
  });

  it('should show a resend button on email verificiation modal render', () => {
    const wrapper = shallow(<EmailVerificationModal {...props} />);
    const buttonWrapper = wrapper.find(Button);
    expect(buttonWrapper.contains('Resend Verification Email')).to.equal(true);
    buttonWrapper.simulate('click');
    expect(wrapper.state('showResend')).to.equal(false);
    const buttonWrapperUpdated = wrapper.find(Button);
    expect(buttonWrapperUpdated).have.length(0);
  });

  it('should trigger the email resend function when button is clicked', () => {
    const wrapper = shallow(<EmailVerificationModal {...props} />);
    const buttonWrapper = wrapper.find(Button);
    expect(buttonWrapper.contains('Resend Verification Email')).to.equal(true);
    const wrapperInstance = wrapper.instance();
    wrapperInstance.clickedResendEmail = sinon.spy();
    buttonWrapper.simulate('click');
    expect(wrapperInstance.clickedResendEmail).to.have.been.calledOnce();
  });
});
