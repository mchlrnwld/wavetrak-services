import React from 'react';
import EmailVerificationModal from './EmailVerificationModal';

export default {
  title: 'Email Verification Modal',
};

export const Default = () => (
  <EmailVerificationModal
    doTriggerEmail={() => {}}
    user={{ email: 'mmalchak@surfline.com' }}
  />
);

Default.story = {
  name: 'default',
};
