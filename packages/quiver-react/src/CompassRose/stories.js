import React from 'react';
import CompassRose from './CompassRose';

export default {
  title: 'CompassRose',
};

export const CompassRoseStory = () => (
  <div style={{ width: 300, height: 300 }}>
    <CompassRose />
  </div>
);

export const CompassRoseWithBearings = () => (
  <div style={{ width: 300, height: 300 }}>
    <CompassRose displayBearings />
  </div>
);

CompassRoseWithBearings.story = {
  name: 'CompassRose with bearings',
};
