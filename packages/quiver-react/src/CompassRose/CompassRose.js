import PropTypes from 'prop-types';
import React from 'react';

import { color } from '@surfline/quiver-themes';

const { PI } = Math;
const CENTER_X = 130;
const CENTER_Y = 97.5;
const RADIUS = 64;
const NUMBER_OF_TICKS = 24;
const TICK_LENGTH = 6;

const polarToCartesianX = (theta, r, origin) => origin + r * Math.cos(theta);
const polarToCartesianY = (theta, r, origin) => origin - r * Math.sin(theta);

const CompassRose = ({ displayBearings }) => (
  <svg
    className="quiver-compass-rose"
    viewBox="0 0 260 195"
    preserveAspectRatio="xMinYMin"
  >
    <circle cx={CENTER_X} cy={CENTER_Y} r={RADIUS} />
    {[...Array(NUMBER_OF_TICKS)].map((_, index) => {
      const angle = (2 * PI * index) / NUMBER_OF_TICKS;
      return (
        <line
          key={angle}
          x1={polarToCartesianX(angle, RADIUS, CENTER_X)}
          y1={polarToCartesianY(angle, RADIUS, CENTER_Y)}
          x2={polarToCartesianX(angle, RADIUS - TICK_LENGTH, CENTER_X)}
          y2={polarToCartesianY(angle, RADIUS - TICK_LENGTH, CENTER_Y)}
        />
      );
    })}
    {displayBearings ? (
      <g>
        <text fill={color('gray', 90)} fontSize="14" letterSpacing=".5">
          <tspan x="126" y="28">
            0º
          </tspan>
        </text>
        <text fill={color('gray', 90)} fontSize="10">
          <tspan x="174" y="49">
            45º
          </tspan>
        </text>
        <text fill={color('gray', 90)} fontSize="14" letterSpacing=".5">
          <tspan x="198" y="101">
            90º
          </tspan>
        </text>
        <text fill={color('gray', 90)} fontSize="10">
          <tspan x="176" y="149">
            135º
          </tspan>
        </text>
        <text fill={color('gray', 90)} fontSize="14" letterSpacing=".5">
          <tspan x="118" y="174">
            180º
          </tspan>
        </text>
        <text fill={color('gray', 90)} fontSize="10">
          <tspan x="60" y="149">
            225º
          </tspan>
        </text>
        <text fill={color('gray', 90)} fontSize="14" letterSpacing=".5">
          <tspan x="34" y="101">
            270º
          </tspan>
        </text>
        <text fill={color('gray', 90)} fontSize="10">
          <tspan x="67" y="49">
            315º
          </tspan>
        </text>
      </g>
    ) : null}
  </svg>
);

CompassRose.propTypes = {
  displayBearings: PropTypes.bool,
};

CompassRose.defaultProps = {
  displayBearings: false,
};

export default CompassRose;
