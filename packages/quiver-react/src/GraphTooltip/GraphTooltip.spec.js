import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import GraphTooltip from './GraphTooltip';

describe('GraphTooltip', () => {
  it('renders children', () => {
    const wrapper = shallow(
      <GraphTooltip>
        <div className="quiver-graph-tooltip-child">Child Text</div>
      </GraphTooltip>
    );

    const children = wrapper.children();

    expect(children.at(0).html()).to.equal(
      '<div class="quiver-graph-tooltip-child">Child Text</div>'
    );
  });
});
