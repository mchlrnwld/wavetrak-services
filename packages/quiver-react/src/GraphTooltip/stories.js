import React from 'react';
import GraphTooltip from './GraphTooltip';

export default {
  title: 'GraphTooltip',
};

export const Default = () => (
  <GraphTooltip>
    <div className="quiver-graph-tooltip-child">Child Text</div>
  </GraphTooltip>
);

Default.story = {
  name: 'default',
};
