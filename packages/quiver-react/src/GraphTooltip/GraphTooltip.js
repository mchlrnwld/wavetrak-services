import PropTypes from 'prop-types';
import React from 'react';

const GraphTooltip = ({ children }) => (
  <div className="quiver-graph-tooltip">{children}</div>
);

GraphTooltip.propTypes = {
  children: PropTypes.node.isRequired,
};

export default GraphTooltip;
