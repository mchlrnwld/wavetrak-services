import React from 'react';
import PropTypes from 'prop-types';
import MediaIcon from '../icons/MediaIcon';
import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

const ClosedMediaPlayer = ({ onClickHandler }) => (
  <div
    className="quiver-closed-media-player"
    {...createAccessibleOnClick(onClickHandler, BTN)}
  >
    <MediaIcon />
  </div>
);

ClosedMediaPlayer.defaultProps = {
  onClickHandler: null,
};

ClosedMediaPlayer.propTypes = {
  onClickHandler: PropTypes.func,
};

export default ClosedMediaPlayer;
