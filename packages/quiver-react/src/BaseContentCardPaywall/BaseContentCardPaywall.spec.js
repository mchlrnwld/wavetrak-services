import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import BaseContentCardPaywall from './BaseContentCardPaywall';

describe('BaseContentCardPaywall', () => {
  it('should render paywall with clickable link', () => {
    const onClick = sinon.stub();
    const wrapper = shallow(
      <BaseContentCardPaywall
        funnelUrl="#funnelurl"
        onClick={onClick}
        message="Unlock Premium Content"
      />
    );

    const message = wrapper.find('.quiver-base-content-card__paywall__message');
    expect(message.text()).to.equal('Unlock Premium Content');

    const link = wrapper.find('.quiver-base-content-card__paywall__link');
    expect(link).to.have.length(1);
    expect(link.prop('href')).to.equal('#funnelurl');
    expect(onClick).not.to.have.been.called();
    link.simulate('click');
    expect(onClick).to.have.been.calledOnce();
  });
});
