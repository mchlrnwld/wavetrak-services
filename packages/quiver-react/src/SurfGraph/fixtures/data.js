const date = Math.round(+new Date() / 1000);
export default [
  {
    timestamp: date,
    utcOffset: -8,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: date + 1000,
    utcOffset: -7,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: date + 2000,
    utcOffset: -7,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: date + 3000,
    utcOffset: -7,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: date + 4000,
    utcOffset: -7,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: date + 5000,
    utcOffset: -7,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: date + 6000,
    utcOffset: -7,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: date + 7000,
    utcOffset: -7,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
  {
    timestamp: date + 8000,
    utcOffset: -7,
    surf: {
      min: 2,
      max: 3,
      optimalScore: 0,
    },
    swells: [
      { height: 0.7, period: 4, direction: 269, index: 0, optimalScore: 0 },
      { height: 1.3, period: 9, direction: 172, index: 1, optimalScore: 0 },
      { height: 0.7, period: 11, direction: 198, index: 2, optimalScore: 0 },
    ],
  },
];
