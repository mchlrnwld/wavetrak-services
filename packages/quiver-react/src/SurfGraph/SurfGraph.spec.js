import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import webCommon, { scaleWaveHeight } from '@surfline/web-common';

import Skeleton from 'react-loading-skeleton';
import SwellMeasurements from '../SwellMeasurements';
import SurfHeight from '../SurfHeight';
import data from './fixtures/data';
import TooltipProvider from '../TooltipProvider';
import SurfGraph from './SurfGraph';
import createAutomatedRatingsFixtures from '../ForecastGraphs/fixtures/automatedRatings';

describe('components / SurfGraph', () => {
  beforeEach(() => {
    sinon.stub(webCommon, 'getTop3Swells').returns([]);
  });

  afterEach(() => {
    webCommon.getTop3Swells.restore();
  });

  const { automatedRatings, automatedRatingsLoading, automatedRatingsError } =
    createAutomatedRatingsFixtures();

  const props = {
    overallMaxHeight: 4,
    data,
    surfUnits: 'FT',
    device: {
      width: 1200,
      mobile: false,
    },
    swellUnits: 'FT',
    automatedRatings,
    ratings: automatedRatings.days[0].flat(),
  };

  it('displays columns for data with label and bar', () => {
    const wrapper = mount(<SurfGraph {...props} />);
    const columns = wrapper.find('.quiver-surf-graph__column');
    expect(columns).to.have.length(data.length);
    columns.forEach((column, index) => {
      expect(column.key()).to.equal(`${data[index].timestamp}`);
      expect(column.prop('style')).to.deep.equal({
        height: `${scaleWaveHeight(6, data[index].surf.max, null, 80)}%`,
      });

      const height = column.find('.quiver-surf-graph__height');
      expect(height).to.have.length(1);
      expect(height.text()).to.equal(`${data[index].surf.max}`);
    });
  });

  it('displays flat height when max height rounds to 0', () => {
    const wrapper = mount(
      <SurfGraph {...props} data={[{ surf: { min: 0.1, max: 0.3 } }]} />
    );
    const column = wrapper.find('.quiver-surf-graph__column');
    expect(column).to.have.length(1);
    const height = column.find('.quiver-surf-graph__height');
    expect(height).to.have.length(1);
    expect(height.text()).to.equal('Flat');
  });

  it('renders with tooltip with top 3 swells with positive height and period', () => {
    const top3Swells = [
      data[4].swells[0],
      data[4].swells[1],
      { height: 0, period: 0 },
    ];

    webCommon.getTop3Swells.returns(top3Swells);
    const wrapper = mount(<SurfGraph {...props} />);
    const tooltipProvider = wrapper.find(TooltipProvider);
    const tooltipWrapper = mount(tooltipProvider.prop('renderTooltip')(0.5));

    const surfHeight = tooltipWrapper.find(SurfHeight);
    expect(surfHeight).to.have.length(1);
    expect(surfHeight.props()).to.deep.equal({
      min: 2,
      max: 3,
      units: props.surfUnits,
      plus: false,
    });

    const swellMeasurementsList = tooltipWrapper.find(SwellMeasurements);
    expect(swellMeasurementsList).to.have.length(2);
    expect(swellMeasurementsList.at(0).props()).to.deep.equal({
      ...top3Swells[0],
      optimalScore: null,
      units: props.swellUnits,
      isEmptySwell: false,
    });
    expect(swellMeasurementsList.at(1).props()).to.deep.equal({
      ...top3Swells[1],
      optimalScore: null,
      units: props.swellUnits,
      isEmptySwell: false,
    });
  });

  it('renders the tooltip with automated ratings', () => {
    const top3Swells = [
      data[4].swells[0],
      data[4].swells[1],
      { height: 0, period: 0 },
    ];

    webCommon.getTop3Swells.returns(top3Swells);
    const wrapper = mount(<SurfGraph {...props} />);
    const tooltipProvider = wrapper.find(TooltipProvider);
    const tooltipWrapper = mount(tooltipProvider.prop('renderTooltip')(0.5));

    expect(
      tooltipWrapper.find('.quiver-surf-graph__tooltip-body__rating').text()
    ).to.be.equal('GOOD');
    expect(
      tooltipWrapper.find('.quiver-surf-graph__tooltip-body__rating--good')
    ).to.have.length(1);
  });

  it('renders the tooltip with automated ratings - loading', () => {
    const top3Swells = [
      data[4].swells[0],
      data[4].swells[1],
      { height: 0, period: 0 },
    ];

    webCommon.getTop3Swells.returns(top3Swells);
    const wrapper = mount(
      <SurfGraph
        {...props}
        automatedRatings={automatedRatingsLoading}
        ratings={false}
      />
    );
    const tooltipProvider = wrapper.find(TooltipProvider);
    const tooltipWrapper = mount(tooltipProvider.prop('renderTooltip')(0.5));

    expect(
      tooltipWrapper.find('.quiver-surf-graph__tooltip-body__rating')
    ).to.have.length(1);

    expect(tooltipWrapper.find(Skeleton)).to.have.length(1);
  });

  it('renders the tooltip without automated ratings on error', () => {
    const top3Swells = [
      data[4].swells[0],
      data[4].swells[1],
      { height: 0, period: 0 },
    ];

    webCommon.getTop3Swells.returns(top3Swells);
    const wrapper = mount(
      <SurfGraph
        {...props}
        automatedRatings={automatedRatingsError}
        ratings={false}
      />
    );
    const tooltipProvider = wrapper.find(TooltipProvider);
    const tooltipWrapper = mount(tooltipProvider.prop('renderTooltip')(0.5));

    expect(
      tooltipWrapper.find('.quiver-surf-graph__tooltip-body__rating')
    ).to.be.empty();
  });

  it('renders the tooltip using the UTC offset passed with the timestamp', () => {
    const wrapper = mount(<SurfGraph {...props} />);
    const tooltipProvider = wrapper.find(TooltipProvider);
    const tooltipWrapper = mount(tooltipProvider.prop('renderTooltip')(0));

    const tooltipDatetime = tooltipWrapper.find('TooltipDatetime');
    expect(tooltipDatetime).to.have.length(1);
    expect(tooltipDatetime.prop('utcOffset')).to.equal(-8);

    const tooltipWrapper2 = mount(tooltipProvider.prop('renderTooltip')(0.25));

    const tooltipDatetime2 = tooltipWrapper2.find('TooltipDatetime');
    expect(tooltipDatetime2).to.have.length(1);
    expect(tooltipDatetime2.prop('utcOffset')).to.equal(-7);
  });
});
