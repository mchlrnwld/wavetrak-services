import PropTypes from 'prop-types';
import React, { useRef } from 'react';
import classNames from 'classnames';
import Skeleton from 'react-loading-skeleton';
import {
  scaleWaveHeight,
  getTop3Swells,
  TABLET_LARGE_WIDTH,
  roundSurfHeight,
  conditionEnumToText,
} from '@surfline/web-common';
import { color } from '@surfline/quiver-themes';

import SwellMeasurements from '../SwellMeasurements';
import SurfHeight from '../SurfHeight';
import wavePropType from '../PropTypes/wave';
import devicePropType from '../PropTypes/device';
import GraphTooltip from '../GraphTooltip';
import TooltipProvider from '../TooltipProvider';
import TooltipDatetime from '../TooltipDatetime';
import dateFormatPropType, { defaultDateFormat } from '../PropTypes/dateFormat';
import AutomatedRatingPropType, {
  automatedRatingsPropType,
} from '../PropTypes/automatedRatings';
import { getConditionClassWithVariation } from '../helpers/applyConditionVariation';

const surfColumnStyle = (overallMaxHeight, surfHeight, units) => ({
  height: `${scaleWaveHeight(overallMaxHeight, surfHeight, units, 80)}%`,
});

const roundWithMappingCheck = (min, max, waveHeightUnit) => {
  if (
    max > 0.5 &&
    max <= 1 &&
    (waveHeightUnit === 'FT' || waveHeightUnit === 'HI')
  )
    return {
      min: 0.5,
      max: 1,
      unit: 'ft',
    };
  if (
    max > 0 &&
    max <= 0.5 &&
    (waveHeightUnit === 'FT' || waveHeightUnit === 'HI')
  )
    return {
      min: 0,
      max: 0.5,
      unit: 'ft',
    };

  return {
    min: roundSurfHeight(min, waveHeightUnit),
    max: roundSurfHeight(max, waveHeightUnit),
    unit: waveHeightUnit,
  };
};

const surfBarStyle = (min, max) => {
  const minMaxPercentage = 100 * (min / max);
  const minColor = color('dark-blue', 10);
  const maxColor = color('light-blue', 40);
  return {
    background: `linear-gradient(to top, ${minColor} 0%, ${minColor} ${minMaxPercentage}%, ${maxColor} ${minMaxPercentage}%, ${maxColor} 100%)`,
  };
};

const surfGraphClassName = (isHourly) =>
  classNames({
    'quiver-surf-graph': true,
    'quiver-surf-graph--hourly': isHourly,
  });

/**
 * @param {string} rating
 */
const getTooltipRatingClass = (rating) =>
  classNames({
    'quiver-surf-graph__tooltip-body__rating': true,
    [`quiver-surf-graph__tooltip-body__rating--${getConditionClassWithVariation(
      rating
    )}`]: !!rating,
  });

/**
 * @typedef {object} Props
 * @property {import('../PropTypes/automatedRatings').AutomatedRating[]} [ratings]
 * @property {import('../PropTypes/automatedRatings').AutomatedRatings} [automatedRatings]
 */

/**
 * @type {React.FC<Props>}
 */
const SurfGraph = ({
  overallMaxHeight,
  data,
  surfUnits,
  isHourly,
  currentMarkerXPercentage,
  utcOffset,
  device,
  isMultiCamPage,
  dateFormat,
  swellUnits,
  ratings,
  automatedRatings,
}) => {
  const surfGraphRef = useRef(null);
  const renderTooltip = (xPercentage) => {
    const point = data[Math.floor(data.length * xPercentage)];

    const rating = ratings && ratings[Math.floor(data.length * xPercentage)];

    const isMobile = device.width < TABLET_LARGE_WIDTH;

    if (!point) return null;

    const swells = getTop3Swells(point.swells).filter(
      (swell) => swell.height > 0 && swell.period > 0
    );

    const surfValues = roundWithMappingCheck(
      point.surf.min,
      point.surf.max,
      surfUnits
    );

    return (
      <GraphTooltip>
        {!isMobile && (
          <TooltipDatetime
            utcOffset={point.utcOffset ?? utcOffset}
            timestamp={point.timestamp}
            dateFormat={dateFormat}
          />
        )}
        <div className="quiver-surf-graph__tooltip-body">
          <SurfHeight
            min={surfValues.min}
            max={surfValues.max}
            units={surfValues.unit}
          />
          {automatedRatings && (
            <p className={getTooltipRatingClass(rating?.rating)}>
              {automatedRatings.loading ? (
                <Skeleton />
              ) : (
                conditionEnumToText(rating?.rating || '')
              )}
            </p>
          )}
          {!isMobile &&
            swells.map((swell) => (
              <SwellMeasurements
                key={swell.index}
                height={swell.height}
                period={swell.period}
                direction={swell.direction}
                index={swell.index}
                units={swellUnits}
              />
            ))}
        </div>
      </GraphTooltip>
    );
  };

  return (
    <div className={surfGraphClassName(isHourly)} ref={surfGraphRef}>
      <TooltipProvider
        currentMarkerXPercentage={currentMarkerXPercentage}
        renderTooltip={renderTooltip}
        isBarGraph
        columns={data.length}
        isSurfGraph
        parentRef={isMultiCamPage && surfGraphRef.current}
      >
        <div className="quiver-surf-graph__canvas">
          {data.map(({ timestamp, surf: { min, max } }) => {
            const maxHeight = roundSurfHeight(max, surfUnits);
            return (
              <div
                key={timestamp}
                className="quiver-surf-graph__column"
                style={surfColumnStyle(overallMaxHeight, maxHeight, surfUnits)}
              >
                <div
                  className="quiver-surf-graph__bar"
                  style={surfBarStyle(min, maxHeight)}
                />
                <div className="quiver-surf-graph__height">
                  {maxHeight > 0 ? maxHeight : 'Flat'}
                </div>
              </div>
            );
          })}
        </div>
      </TooltipProvider>
    </div>
  );
};

SurfGraph.propTypes = {
  overallMaxHeight: PropTypes.number.isRequired,
  data: PropTypes.arrayOf(wavePropType).isRequired,
  surfUnits: PropTypes.string.isRequired,
  isHourly: PropTypes.bool.isRequired,
  device: devicePropType.isRequired,
  currentMarkerXPercentage: PropTypes.number,
  utcOffset: PropTypes.number.isRequired,
  isMultiCamPage: PropTypes.bool,
  dateFormat: dateFormatPropType,
  swellUnits: PropTypes.string.isRequired,
  automatedRatings: automatedRatingsPropType,
  ratings: PropTypes.arrayOf(AutomatedRatingPropType),
};

SurfGraph.defaultProps = {
  currentMarkerXPercentage: 0,
  isMultiCamPage: false,
  dateFormat: defaultDateFormat,
  automatedRatings: null,
  ratings: null,
};

export default SurfGraph;
