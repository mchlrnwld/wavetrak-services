import React from 'react';
import SurfGraph from './SurfGraph';
import data from './fixtures/data';

const props = {
  overallMaxHeight: 4,
  data,
  units: 'FT',
  device: {
    width: 1200,
    mobile: false,
  },
  utcOffset: -8,
};

export default {
  title: 'SurfGraph',
};

export const Default = () => <SurfGraph {...props} />;

Default.story = {
  name: 'default',
};
