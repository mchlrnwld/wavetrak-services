import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow, mount } from 'enzyme';
import Input from './Input';
import { MAX_PASSWORD_LENGTH } from '../utils/constants';

describe('Input', () => {
  it('should initialize and set visited state if defaultValue or value exist', () => {
    const noDefaultWrapper = shallow(<Input />);
    const defaultWrapper = shallow(<Input defaultValue="Default Value" />);
    const valueWrapper = shallow(
      <Input value="Value" input={{ readOnly: true }} />
    );

    const noDefaultInput = noDefaultWrapper.find('input');
    const defaultInput = defaultWrapper.find('input');
    const valueInput = valueWrapper.find('input');

    expect(noDefaultInput.prop('defaultValue')).not.to.be.ok();
    expect(noDefaultInput.prop('value')).not.to.be.ok();
    expect(defaultInput.prop('defaultValue')).to.equal('Default Value');
    expect(defaultInput.prop('value')).not.to.be.ok();
    expect(valueInput.prop('defaultValue')).not.to.be.ok();
    expect(valueInput.prop('value')).to.equal('Value');
  });

  it('should pass input props to input component', () => {
    const input = {
      name: 'Test Name',
    };
    const wrapper = shallow(<Input input={input} />);
    const inputWrapper = wrapper.find('input');

    expect(inputWrapper.prop('name')).to.equal('Test Name');
  });

  it('should pass id into input and label', () => {
    const wrapper = shallow(<Input id="testid" />);
    const labelWrapper = wrapper.find('label');
    const inputWrapper = wrapper.find('input');

    expect(labelWrapper.prop('htmlFor')).to.equal('testid');
    expect(inputWrapper.prop('id')).to.equal('testid');
  });

  it('should render the correct input type', () => {
    const textWrapper = shallow(<Input type="text" />);
    const passwordWrapper = shallow(<Input type="password" />);
    const emailWrapper = shallow(<Input type="email" />);

    const textInput = textWrapper.find('input');
    const passwordInput = passwordWrapper.find('input');
    const emailInput = emailWrapper.find('input');

    expect(textInput.prop('type')).to.equal('text');
    expect(passwordInput.prop('type')).to.equal('password');
    expect(emailInput.prop('type')).to.equal('email');
  });

  it('should display the label', () => {
    const wrapper = shallow(<Input label="Input Label" />);
    const labelWrapper = wrapper.find('label');
    expect(labelWrapper.text()).to.equal('Input Label');
  });

  it('should show helper text with correct text when message prop set', () => {
    const defaultWrapper = shallow(<Input />);
    const messageWrapper = shallow(<Input message="Helper Text" />);
    const defaultSpanWrapper = defaultWrapper.find('.quiver-input__message');
    const messageSpanWrapper = messageWrapper.find('.quiver-input__message');

    expect(defaultSpanWrapper.exists()).to.be.false();
    expect(messageSpanWrapper.exists()).to.be.true();
    expect(messageSpanWrapper.text()).to.equal('Helper Text');
  });

  it('should call onFocus on focus', () => {
    const input = { onFocus: sinon.spy() };
    const wrapper = shallow(<Input type="text" input={input} />);
    expect(input.onFocus.called).to.be.false();

    const inputWrapper = wrapper.find('input');
    inputWrapper.simulate('focus');
    expect(input.onFocus.calledOnce).to.be.true();
    expect(input.onFocus.lastCall.args).to.have.length(1);
  });

  it('should call onBlur on blur', () => {
    const input = { onBlur: sinon.spy() };
    const wrapper = shallow(<Input type="text" input={input} />);
    expect(input.onBlur.called).to.be.false();

    const inputWrapper = wrapper.find('input');
    inputWrapper.simulate('focus');
    inputWrapper.simulate('blur');
    expect(input.onBlur.calledOnce).to.be.true();
    expect(input.onBlur.lastCall.args).to.have.length(1);
  });

  it('should attach quiver-input class name', () => {
    const wrapper = shallow(<Input type="text" />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal('quiver-input');
  });

  it('should attach parent-width modifier class name if parentWidth set', () => {
    const wrapper = shallow(<Input type="text" parentWidth />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-input quiver-input--parent-width'
    );
  });

  it('should attach visited modifier class name if visited state set', () => {
    const wrapper = mount(<Input type="text" />);
    wrapper.setProps({ value: 'value ' });
    wrapper.update();
    expect(wrapper.find('div').prop('className')).to.equal(
      'quiver-input quiver-input--visited'
    );
  });

  it('should attach success modifier class name if success set', () => {
    const wrapper = shallow(<Input type="text" success />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-input quiver-input--success'
    );
  });

  it('should attach warning modifier class name if warning set', () => {
    const wrapper = shallow(<Input type="text" warning />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-input quiver-input--warning'
    );
  });

  it('should attach error modifier class name if error set', () => {
    const wrapper = shallow(<Input type="text" error />);
    const containerWrapper = wrapper.find('div');
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-input quiver-input--error'
    );
  });

  it('should attach error modifier if password is too short', () => {
    const wrapper = mount(
      <Input
        type="password"
        id="password"
        label="Password"
        password
        value="1234"
      />
    );
    const containerWrapper = wrapper.find('div').first();
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-input quiver-input--visited quiver-input--error'
    );
  });

  it('should attach error modifier if password is too long', () => {
    const password = '#'.repeat(MAX_PASSWORD_LENGTH + 1);
    const wrapper = mount(
      <Input
        type="password"
        id="password"
        label="Password"
        password
        value={password}
      />
    );
    const containerWrapper = wrapper.find('div').first();
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-input quiver-input--visited quiver-input--error'
    );
  });

  it('should not attach error modifier if it is a login form', () => {
    const wrapper = mount(
      <Input
        type="password"
        id="password"
        label="Password"
        password
        isLogin
        value="1234"
      />
    );
    const containerWrapper = wrapper.find('div').first();
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-input quiver-input--visited quiver-input--success'
    );
  });

  it('should not attach error modifier if password is 6 characters', () => {
    const wrapper = mount(
      <Input
        type="password"
        id="password"
        label="Password"
        password
        value="123456"
      />
    );
    const containerWrapper = wrapper.find('div').first();
    expect(containerWrapper.prop('className')).to.equal(
      'quiver-input quiver-input--visited quiver-input--success'
    );
  });

  it('should show an error if the field is required and has been visited', () => {
    const wrapper = mount(<Input type="text" required />);

    const inputWrapper = wrapper.find('input');
    inputWrapper.simulate('focus');
    inputWrapper.simulate('blur');
    expect(wrapper.find('div').first().prop('className')).to.equal(
      'quiver-input quiver-input--visited quiver-input--error'
    );
  });

  it('should show an error if the field is email and has invalid format', () => {
    const wrapper = mount(<Input type="text" email />);
    const inputWrapper = wrapper.find('input');

    inputWrapper.simulate('change', { target: { value: 'asdfa' } });

    const messageWrapper = wrapper.find('.quiver-input__message');

    expect(messageWrapper.text()).to.equal('Not a valid email address format');
  });

  it('should not show an error if the field is email and has invalid format', () => {
    const wrapper = mount(<Input type="text" email />);
    const inputWrapper = wrapper.find('input');

    inputWrapper.simulate('change', {
      target: { value: 'ryanoillataguerre@surfline.com' },
    });

    expect(wrapper.find('.quiver-input__message').exists()).to.equal(false);
  });

  it('should pass custom style object into children', () => {
    const style = {
      fontFamily: 'container',
      label: {
        fontFamily: 'label',
      },
      input: {
        fontFamily: 'input',
      },
      message: {
        fontFamily: 'message',
      },
    };
    const wrapper = shallow(
      <Input type="text" message="Some message" style={style} />
    );
    const containerWrapper = wrapper.find('.quiver-input');
    const labelWrapper = containerWrapper.find('label');
    const inputWrapper = containerWrapper.find('input');
    const messageWrapper = containerWrapper.find('.quiver-input__message');

    expect(containerWrapper.prop('style')).to.equal(style);
    expect(labelWrapper.prop('style')).to.equal(style.label);
    expect(inputWrapper.prop('style')).to.equal(style.input);
    expect(messageWrapper.prop('style')).to.equal(style.message);
  });

  it('should not render value & defaultValue props together', () => {
    const firstWrapper = shallow(
      <Input type="text" value="Surfing" defaultValue="Sport" />
    );
    const firstInput = firstWrapper.find('input');
    const { value: val1, defaultValue: defaultVal1 } = firstInput.props();
    expect(val1).to.equal('Surfing');
    // eslint-disable-next-line no-unused-expressions
    expect(defaultVal1).to.be.undefined;

    const secondWrapper = shallow(<Input type="text" defaultValue="Ocean" />);
    const secondInput = secondWrapper.find('input');
    const { value: val2, defaultValue: defaultVal2 } = secondInput.props();
    // eslint-disable-next-line no-unused-expressions
    expect(val2).to.be.undefined;
    expect(defaultVal2).to.equal('Ocean');
  });
});
