import React, { useState, Component } from 'react';
import { action } from '@storybook/addon-actions';
import Input from './index';

export default {
  title: 'Input',
};

export const Default = () => (
  <Input type="text" id="initial" label="Initial Input" />
);

Default.story = {
  name: 'default',
};

export const DefaultWithDefaultValue = () => (
  <Input
    type="text"
    id="defaultvalue"
    label="Initial Input with Default Value"
    defaultValue="Default Value"
  />
);

DefaultWithDefaultValue.story = {
  name: 'default with default value',
};

class InputWithState extends Component {
  constructor() {
    super();
    this.state = {
      value: '',
    };
  }

  onChange = (event) => {
    action('Value Changed')(event);
    this.setState({
      value: event.target.value,
    });
  };

  render() {
    const { value } = this.state;
    return (
      <Input
        type="text"
        id="defaultvalue"
        label="Initial Input with Value"
        value={value}
        input={{ onChange: this.onChange }}
      />
    );
  }
}

export const DefaultWithPassedInValueAndOnChange = () => <InputWithState />;

DefaultWithPassedInValueAndOnChange.story = {
  name: 'default with passed in value and onChange',
};

export const DefaultWithHelperText = () => (
  <Input
    type="text"
    id="initial"
    label="Initial Input with Helper Text"
    message="Helper Text"
  />
);

DefaultWithHelperText.story = {
  name: 'default with helper text',
};

export const DefaultParentWidth = () => (
  <Input type="text" id="initial" label="Initial Input" parentWidth />
);

DefaultParentWidth.story = {
  name: 'default parent width',
};

export const Password = () => (
  <Input type="password" id="password" label="Password" password />
);

Password.story = {
  name: 'password',
};

export const Email = () => (
  <Input type="email" id="email" label="Email" email required />
);

Email.story = {
  name: 'email',
};

export const Success = () => (
  <Input
    type="text"
    id="success"
    defaultValue="Success"
    label="Success Input"
    success
  />
);

Success.story = {
  name: 'success',
};

export const Warning = () => (
  <Input
    type="text"
    id="warning"
    defaultValue="Warning"
    label="Warning Input"
    warning
  />
);

Warning.story = {
  name: 'warning',
};

export const Error = () => (
  <Input
    type="text"
    id="error"
    defaultValue="Error"
    label="Error Input"
    message="Error Message"
    error
  />
);

Error.story = {
  name: 'error',
};

export const Required = () => (
  <Input type="text" id="required" label="Required Input" required />
);

Required.story = {
  name: 'required',
};

export const CustomStyles = () => (
  <Input
    type="text"
    id="customstyles"
    label="Custom Styles"
    message="Some Message"
    style={{
      margin: '20px 50px',
      input: {
        color: '#22d736',
      },
      label: {
        fontWeight: 'bold',
        fontStyle: 'italic',
      },
      message: {
        color: '#67d3df',
      },
    }}
  />
);

CustomStyles.story = {
  name: 'custom styles',
};

export const FocusAndBlurEvents = () => (
  <Input
    type="text"
    id="focusandblurevents"
    label="Focus and Blur Events"
    input={{
      onFocus: action('focused'),
      onBlur: action('blurred'),
    }}
  />
);

FocusAndBlurEvents.story = {
  name: 'focus and blur events',
};

export const WithoutSuccessStateOnDefault = () => {
  const [text, setText] = useState('Some initial value');

  return (
    <Input
      type="text"
      id="withoutsuccessstateondefault"
      label="without success state on default"
      validateDefault={false}
      value={text}
      input={{
        onChange: ({ target }) => setText(target.value),
      }}
    />
  );
};

WithoutSuccessStateOnDefault.story = {
  name: 'without success state on default',
};
