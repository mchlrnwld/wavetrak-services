import React, { useEffect, useState, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH } from '../utils/constants';

/**
 * @function validateEmail
 * @param {string} email
 * @return {boolean}
 */
const validateEmail = (email) => {
  const emailRegex =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return emailRegex.test(email);
};

const Input = React.forwardRef(
  /**
   * @param {object} props
   * @param {HTMLInputElement['type']} props.type
   * @param {HTMLLabelElement['htmlFor']} props.id
   * @param {HTMLInputElement['defaultValue']} props.defaultValue
   * @param {HTMLInputElement['value']} props.value
   * @param {Omit<HTMLInputElement, 'type' | 'id' | 'defaultValue' | 'onChange' | 'onFocus' | 'onBlur' | 'style'>} props.input
   * @param {string} props.label
   * @param {string} props.message
   * @param {React.CSSProperties} props.style
   * @param {boolean} props.parentWidth
   * @param {boolean} props.warning
   * @param {boolean} props.error
   * @param {boolean} props.success
   * @param {boolean} props.required
   * @param {boolean} props.password
   * @param {boolean} props.email
   * @param {(errorState: boolean) => void} props.onError
   * @param {boolean} props.validateDefault
   * @param {boolean} props.isLogin
   * @param {boolean} props.controlled
   * @param {React.RefObject} ref
   * @returns {JSX.Element}
   */
  (
    {
      type,
      id,
      defaultValue,
      value,
      label,
      input,
      message,
      style,
      parentWidth,
      warning,
      error,
      success,
      required,
      password,
      email,
      onError,
      validateDefault,
      isLogin,
      controlled,
    },
    ref
  ) => {
    const [visited, setVisited] = useState(!!defaultValue);
    const [active, setActive] = useState(false);
    const [errorState, setErrorState] = useState(error);
    const [messageState, setMessageState] = useState(message);
    const [successState, setSuccessState] = useState(success);
    const [valueState, setValueState] = useState(value);
    const initialState = useRef(value || defaultValue);

    const handleValidationSetting = useCallback(() => {
      if (password) {
        if (visited && valueState?.length && !active) {
          if (valueState?.length < MIN_PASSWORD_LENGTH) {
            setMessageState(`Minimum ${MIN_PASSWORD_LENGTH} characters`);
            setErrorState(true);
          } else if (valueState?.length > MAX_PASSWORD_LENGTH) {
            setMessageState(`Maximum ${MAX_PASSWORD_LENGTH} characters`);
            setErrorState(true);
          } else {
            setMessageState(null);
            setErrorState(false);
          }
        }
      }
      if (email && valueState?.length) {
        if (validateEmail(valueState)) {
          setErrorState(false);
          setMessageState(null);
        }
        if (!validateEmail(valueState) && !active) {
          setErrorState(true);
          setMessageState('Not a valid email address format');
        }
      }
      // Disabling eslint rule here because we need to rerun effect on valueState change
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [password, email, valueState, visited, active]);

    useEffect(() => {
      onError(errorState);
      if (errorState) {
        setSuccessState(false);
      }
    }, [errorState, onError]);

    // Props to initial state
    useEffect(() => {
      if (!controlled) {
        if (message) {
          setMessageState(message);
        }
        if (error) {
          setErrorState(error);
        }
        if (success) {
          setSuccessState(success);
        }
      }
    }, [controlled, message, error, success]);

    // Default/parent value handling
    useEffect(() => {
      if (defaultValue || value) {
        setVisited(true);
      }
    }, [defaultValue, value]);

    // Required
    useEffect(() => {
      if (visited && !active && required && !valueState?.length) {
        setErrorState(true);
        setMessageState('Required');
      }
    }, [visited, active, required, valueState]);

    // Validation
    useEffect(() => {
      // Don't perform password validation on the sign in sheet
      // since we have users that created passwords before we added validation
      if (password && !isLogin) {
        handleValidationSetting();
      }
      if (email) {
        handleValidationSetting();
      }
    }, [valueState, password, email, handleValidationSetting, isLogin]);

    // Success
    useEffect(() => {
      const hasChanged = initialState.current !== valueState;
      // eslint-disable-next-line prettier/prettier
      const isFilledWithoutErrors =
        !active && visited && !errorState && valueState?.length;

      if (!validateDefault) {
        if (hasChanged && isFilledWithoutErrors) {
          setSuccessState(true);
        } else if (!hasChanged) {
          setSuccessState(false);
        }
      } else if (isFilledWithoutErrors) {
        setSuccessState(true);
      }
    }, [validateDefault, active, visited, errorState, valueState]);

    const onFocus = (event) => {
      setVisited(true);
      setActive(true);
      if (!password && !message) {
        setMessageState(false);
      }
      setErrorState(false);
      if (input && input.onFocus) {
        input.onFocus(event);
      }
    };

    const onBlur = (event) => {
      setActive(false);
      handleValidationSetting();
      if (input && input.onBlur) {
        input.onBlur(event);
      }
    };

    const getClassName = useCallback(
      () =>
        classNames({
          'quiver-input': true,
          'quiver-input--parent-width': parentWidth,
          'quiver-input--visited': visited,
          'quiver-input--success': controlled ? success : successState,
          'quiver-input--warning': warning,
          'quiver-input--error': controlled ? error : errorState,
        }),
      [
        visited,
        successState,
        errorState,
        parentWidth,
        warning,
        controlled,
        success,
        error,
      ]
    );

    const handleValueChange = (event) => {
      setValueState(event.target.value);
      if (input.onChange) {
        input.onChange(event);
      }
    };

    const inputProps = {
      ...input,
      type,
      id,
      onChange: (event) => handleValueChange(event),
      onFocus,
      onBlur,
      style: style && style.input,
      [value ? 'value' : 'defaultValue']: value || defaultValue,
    };

    return (
      <div className={getClassName()} style={style}>
        <label htmlFor={id} style={style && style.label}>
          {label}
        </label>
        <input ref={ref} {...inputProps} />
        {messageState || message ? (
          <div className="quiver-input__message" style={style && style.message}>
            {messageState || message}
          </div>
        ) : null}
      </div>
    );
  }
);

Input.displayName = 'Input';

Input.propTypes = {
  type: PropTypes.string,
  id: PropTypes.string,
  defaultValue: PropTypes.string,
  value: PropTypes.string,
  label: PropTypes.string,
  message: PropTypes.string,
  error: PropTypes.bool,
  success: PropTypes.bool,
  warning: PropTypes.bool,
  parentWidth: PropTypes.bool,
  input: PropTypes.shape(),
  style: PropTypes.shape(),
  visited: PropTypes.bool,
  required: PropTypes.bool,
  password: PropTypes.bool,
  email: PropTypes.bool,
  onError: PropTypes.func,
  validateDefault: PropTypes.bool,
  isLogin: PropTypes.bool,
  controlled: PropTypes.bool,
};

Input.defaultProps = {
  type: '',
  id: '',
  defaultValue: null,
  value: null,
  label: '',
  message: '',
  error: false,
  success: false,
  warning: false,
  parentWidth: false,
  input: {},
  style: {},
  visited: false,
  required: false,
  password: false,
  email: false,
  onError: () => null,
  validateDefault: true,
  isLogin: false,
  controlled: false,
};

export default Input;
