import React from 'react';
import SpotForecastSummary from './SpotForecastSummary';

const associated = {
  advertising: {},
  units: {
    waveHeight: 'FT',
    tideHeight: 'FT',
    speed: 'MPH',
    windSpeed: 'MPH',
    distance: 'FT',
  },
  weatherIconPath: 'https://wa.cdn-surfline.com/quiver/0.15.0/weathericons',
};

const tide = {
  previous: {
    height: 1,
    type: 'NORMAL',
    timestamp: Math.floor(+new Date('2021/01/01 1:00:00') / 1000),
    utcOffset: -8,
  },
  current: {
    height: 1,
    type: 'HIGH',
    timestamp: Math.floor(+new Date('2021/01/01 10:00:00') / 1000),
    utcOffset: -8,
  },

  next: {
    height: 1,
    type: 'LOW',
    timestamp: Math.floor(+new Date('2021/01/01 16:00:00') / 1000),
    utcOffset: -8,
  },
};

export default {
  title: 'SpotForecastSummary',
};

export const FreeUser = () => (
  <SpotForecastSummary
    associated={associated}
    entitlements={[]}
    units={associated.units}
    waveHeight={{
      min: 1,
      max: 2,
      occasional: 3,
      plus: false,
      human: true,
      humanRelation: 'Chest to head high',
    }}
    tide={tide}
    wind={{
      speed: 1,
      direction: 200,
    }}
    swells={[
      { height: 2, period: 16, direction: 120, index: 1 },
      { height: 2, period: 16, direction: 120, index: 2 },
    ]}
    lat={40}
    lon={40}
    readings={{
      waterTemp: { min: 1, max: 2 },
      weather: { condition: 'CLEAR_NO_RAIN', temperature: 10 },
    }}
    spot={{ name: 'HB Southside', _id: '123', cameras: [] }}
    userDetails={{}}
  />
);

export const PremiumUser = () => (
  <SpotForecastSummary
    associated={associated}
    entitlements={['sl_premium']}
    units={associated.units}
    waveHeight={{
      min: 1,
      max: 2,
      occasional: 3,
      plus: false,
      human: false,
      humanRelation: '',
    }}
    tide={tide}
    wind={{
      speed: 1,
      direction: 200,
    }}
    swells={[
      { height: 2, period: 16, direction: 120, index: 1 },
      { height: 2, period: 16, direction: 120, index: 2 },
    ]}
    lat={40}
    lon={40}
    readings={{
      waterTemp: { min: 1, max: 2 },
      weather: { condition: 'CLEAR_NO_RAIN', temperature: 10 },
    }}
    spot={{ name: 'Some spot name', _id: '123', cameras: [] }}
    userDetails={{}}
  />
);

export const ReportExpired = () => (
  <SpotForecastSummary
    associated={associated}
    entitlements={['sl_premium']}
    units={associated.units}
    waveHeight={{
      min: 1,
      max: 2,
      occasional: 3,
      plus: false,
      human: false,
      humanRelation: '',
    }}
    tide={tide}
    wind={{
      speed: 1,
      direction: 200,
    }}
    swells={[
      { height: 2, period: 16, direction: 120, index: 1 },
      { height: 2, period: 16, direction: 120, index: 2 },
    ]}
    lat={40}
    lon={40}
    readings={{
      waterTemp: { min: 1, max: 2 },
      weather: { condition: 'CLEAR_NO_RAIN', temperature: 10 },
    }}
    spot={{ name: 'Some spot name', _id: '123', cameras: [] }}
    userDetails={{}}
    reportExpired
  />
);

export const NoTideNextData = () => (
  <SpotForecastSummary
    associated={associated}
    entitlements={['sl_premium']}
    units={associated.units}
    waveHeight={{
      min: 1,
      max: 2,
      occasional: 3,
      plus: false,
      human: false,
      humanRelation: '',
    }}
    tide={{
      previous: {
        height: 1,
        type: 'NORMAL',
        timestamp: +new Date('2021/01/01 1:00:00') / 1000,
      },
      current: {
        height: 1,
        type: 'HIGH',
        timestamp: +new Date('2021/01/01 10:00:00') / 1000,
      },
      utcOffset: -8,
    }}
    wind={{
      speed: 1,
      direction: 200,
    }}
    swells={[
      { height: 2, period: 16, direction: 120, index: 1 },
      { height: 2, period: 16, direction: 120, index: 2 },
    ]}
    lat={40}
    lon={40}
    readings={{
      waterTemp: { min: 1, max: 2 },
      weather: { condition: 'CLEAR_NO_RAIN', temperature: 10 },
    }}
    spot={{ name: 'Some spot name', _id: '123', cameras: [] }}
    userDetails={{}}
    reportExpired
  />
);
