import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { getMapTileUrl } from '@surfline/web-common';
import WetsuitRecommender from '../WetsuitRecommender';
import WaterTemp from '../WaterTemp';
import Weather from '../Weather';
import SwellWindRose from '../SwellWindRose';
import unitsPropType from '../PropTypes/units';
import readingsPropType, {
  tidePropTypes,
  windPropTypes,
  swellsPropTypes,
  waveHeightPropTypes,
} from '../PropTypes/readings';
import spotAssociatedPropType from '../PropTypes/spotAssociated';
import spotPropType from '../PropTypes/spot';
import userDetailsPropType from '../PropTypes/userDetails';
import { SurfStats, TideStats, WindStats, SwellStats } from '../ConditionStats';

const getSummaryClass = (tide) =>
  classNames({
    'quiver-spot-forecast-summary': true,
    'quiver-spot-forecast-summary--no-tide': !tide,
  });

const getStatContainerClass = (statType, expired = false) =>
  classNames({
    'quiver-spot-forecast-summary__stat-container': true,
    [`quiver-spot-forecast-summary__stat-container--${statType}`]: !!statType,
    [`quiver-spot-forecast-summary__stat-container--${statType}--expired`]:
      expired,
  });

const SpotForecastSummary = ({
  adTargets,
  showWetsuitAd,
  associated,
  entitlements,
  waveHeight,
  tide,
  wind,
  swells,
  units,
  lat,
  lon,
  readings,
  spot,
  userDetails,
  reportExpired,
}) => {
  const { human, max } = waveHeight;
  const mapWidth = 260;
  const mapHeight = 195;
  const surfHeightExpired = reportExpired && human;

  const mapUrl = getMapTileUrl(mapWidth, mapHeight, lat, lon);
  const validSwells = swells.filter(
    (swell) => swell.height > 0 && swell.period > 0
  );

  return (
    <div className={getSummaryClass(tide)}>
      <div className="quiver-spot-forecast-summary__wrapper">
        <div className="quiver-spot-forecast-summary__stat">
          <div
            className={getStatContainerClass('surf-height', surfHeightExpired)}
          >
            <SurfStats waveHeight={waveHeight} units={units} />
          </div>
        </div>
        {tide && tide.current && tide.next ? (
          <div className="quiver-spot-forecast-summary__stat">
            <div className={getStatContainerClass('tide')}>
              <TideStats tide={tide} units={units} />
            </div>
          </div>
        ) : null}
        <div className="quiver-spot-forecast-summary__stat">
          <div className={getStatContainerClass('wind')}>
            <WindStats wind={wind} units={units} />
          </div>
        </div>
        <div className="quiver-spot-forecast-summary__stat">
          <div className={getStatContainerClass('swells')}>
            <SwellStats swells={swells} units={units} />
          </div>
        </div>
        {!showWetsuitAd && (
          <div className="quiver-spot-forecast-summary__stat">
            <div className={getStatContainerClass('waterTemp')}>
              <WaterTemp
                weatherIconPath={associated.weatherIconPath}
                units={associated.units.temperature}
                waterTemp={readings.waterTemp}
                spotName={spot?.name}
              />
            </div>
          </div>
        )}
        {!showWetsuitAd && (
          <div className="quiver-spot-forecast-summary__stat">
            <div className={getStatContainerClass('weather')}>
              <Weather
                weatherIconPath={associated.weatherIconPath}
                units={associated.units.temperature}
                weather={readings.weather}
                spotName={spot?.name}
              />
            </div>
          </div>
        )}
      </div>
      <div
        className="quiver-spot-forecast-summary__thumbnail"
        style={{ backgroundImage: `url("${mapUrl}")` }}
      >
        <SwellWindRose
          swells={validSwells}
          wind={wind}
          units={units}
          mapHeight={mapHeight}
          mapWidth={mapWidth}
          overallMaxHeight={max}
        />
      </div>
      {showWetsuitAd && (
        <WetsuitRecommender
          spotName={spot && spot.name}
          weather={readings.weather}
          waterTemp={readings.waterTemp}
          sst={associated.advertising.sst}
          weatherIconPath={associated.weatherIconPath}
          units={associated.units.temperature}
          adTargets={adTargets}
          entitlements={entitlements}
          isUser={!!userDetails}
        />
      )}
    </div>
  );
};

SpotForecastSummary.propTypes = {
  showWetsuitAd: PropTypes.bool,
  adTargets: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
  associated: spotAssociatedPropType.isRequired,
  entitlements: PropTypes.arrayOf(PropTypes.string).isRequired,
  units: unitsPropType.isRequired,
  waveHeight: waveHeightPropTypes.isRequired,
  tide: tidePropTypes,
  wind: windPropTypes.isRequired,
  swells: swellsPropTypes.isRequired,
  lat: PropTypes.number.isRequired,
  lon: PropTypes.number.isRequired,
  readings: readingsPropType.isRequired,
  spot: spotPropType.isRequired,
  userDetails: userDetailsPropType,
  reportExpired: PropTypes.bool,
};

SpotForecastSummary.defaultProps = {
  adTargets: [],
  showWetsuitAd: true,
  tide: null,
  userDetails: null,
  reportExpired: false,
};

export default SpotForecastSummary;
