import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import SurfHeight from '../SurfHeight';
import Reading from '../Reading';
import TideSummary from '../TideSummary';
import WaveSummary from '../WaveSummary';
import WindArrow from '../icons/WindArrow';
import WindSummary from '../WindSummary';
import SwellMeasurements from '../SwellMeasurements';
import SpotForecastSummary from './SpotForecastSummary';
import SwellWindRose from '../SwellWindRose';

describe('components / SpotForecastSummary', () => {
  const defaultProps = {
    waveHeight: {
      min: 2,
      max: 3,
      humanRelation: 'ankle to knee high',
      plus: true,
    },
    wind: { direction: 280, speed: 4 },
    readings: {
      weather: { condition: 'condition', temperature: 60 },
      waterTemp: {
        min: 1,
        max: 2,
      },
    },
    swells: [
      { direction: 1, period: 2, height: 3, index: 0, optimalScore: 2 },
      { direction: 4, period: 5, height: 6, index: 1, optimalScore: 1 },
      { direction: 0, period: 0, height: 0, index: 3, optimalScore: 0 },
    ],
  };

  it('adds no-tide class modifier if tide is not provided', () => {
    const noTideWrapper = mount(
      <SpotForecastSummary
        units={{}}
        associated={{ units: {}, advertising: { sst: {} } }}
        {...defaultProps}
      />
    );
    expect(
      noTideWrapper.find('.quiver-spot-forecast-summary--no-tide')
    ).to.have.length(1);

    const tideWrapper = mount(
      <SpotForecastSummary
        tide={{}}
        units={{}}
        associated={{ units: {}, advertising: { sst: {} } }}
        {...defaultProps}
      />
    );
    expect(
      tideWrapper.find('.quiver-spot-forecast-summary--no-tide')
    ).to.have.length(0);

    noTideWrapper.unmount();
    tideWrapper.unmount();
  });

  it('renders SurfHeight and WaveSummary with wave height and units', () => {
    const units = { waveHeight: 'FT' };
    const waveHeightWrapper = mount(
      <SpotForecastSummary
        units={units}
        associated={{ units: {}, advertising: { sst: {} } }}
        {...defaultProps}
      />
    );
    const waveHeightSurfHeight = waveHeightWrapper.find(SurfHeight);
    const waveSummary = waveHeightWrapper.find(WaveSummary);
    expect(waveHeightSurfHeight.props()).to.deep.equal({
      min: 2,
      max: 3,
      units: 'FT',
      plus: true,
    });
    expect(waveSummary.prop('humanRelation')).to.equal('ankle to knee high');
    waveHeightWrapper.unmount();
  });

  it('renders tide Reading and TideSummary if tide is provided', () => {
    const noTideWrapper = mount(
      <SpotForecastSummary
        units={{}}
        associated={{ units: {}, advertising: { sst: {} } }}
        {...defaultProps}
      />
    );
    const noTideStats = noTideWrapper.find(
      '.quiver-spot-forecast-summary__stat'
    );
    expect(noTideStats).to.have.length(3);

    const tide = {
      previous: {
        type: 'LOW',
        height: 0.7,
        timestamp: 1499643689,
        utcOffset: -7,
      },
      current: {
        type: 'NORMAL',
        height: 0.7,
        timestamp: 1499646797,
        utcOffset: -7,
      },
      next: {
        type: 'HIGH',
        height: 1.7,
        timestamp: 1499665931,
        utcOffset: -7,
      },
    };
    const units = { tideHeight: 'FT' };
    const tideWrapper = mount(
      <SpotForecastSummary
        tide={tide}
        units={units}
        associated={{ units: {}, advertising: { sst: {} } }}
        {...defaultProps}
      />
    );
    const tideStats = tideWrapper.find('.quiver-spot-forecast-summary__stat');
    const tideStat = tideStats.at(1);
    const tideReading = tideStat.find(Reading);
    const tideSummary = tideStat.find(TideSummary);
    expect(tideStats).to.have.length(4);
    expect(tideReading.props()).to.deep.equal({ value: 0.7, units: 'FT' });
    expect(tideSummary.props()).to.deep.equal({ tide: tide.next, units: 'FT' });
    noTideWrapper.unmount();
    tideWrapper.unmount();
  });

  it('renders wind Reading, WindArrow, and WindSummary with wind and units', () => {
    const units = { windSpeed: 'KTS' };
    const wrapper = mount(
      <SpotForecastSummary
        units={units}
        associated={{ units: {}, advertising: { sst: {} } }}
        {...defaultProps}
      />
    );
    const reading = wrapper.find(Reading);
    const windArrow = wrapper.find(WindArrow).at(0);
    const windSummary = wrapper.find(WindSummary);
    expect(reading.props()).to.deep.equal({ value: 4, units: 'KTS' });
    expect(windArrow.props()).to.deep.equal({
      degrees: 280,
      speed: 4,
      units: 'KTS',
    });
    expect(windSummary.props()).to.deep.equal({ direction: 280 });
    wrapper.unmount();
  });

  it('renders swells with positive height and period', () => {
    const swells = [
      { direction: 1, period: 2, height: 3, index: 0, optimalScore: 2 },
      { direction: 4, period: 5, height: 6, index: 1, optimalScore: 1 },
      { direction: 0, period: 0, height: 0, index: 3, optimalScore: 0 },
    ];
    const units = { swellHeight: 'FT', waveHeight: 'FT' };
    const wrapper = mount(
      <SpotForecastSummary
        swells={swells}
        units={units}
        waveHeight={{
          human: false,
          max: 1,
          min: 0,
          occasional: 2,
          plus: false,
        }}
        wind={{ direction: 320, speed: 1 }}
        readings={{
          weather: { condition: 'condition', temperature: 60 },
          waterTemp: {
            min: 1,
            max: 2,
          },
        }}
        associated={{ units: {}, advertising: { sst: {} } }}
      />
    );

    const swellMeasurementsList = wrapper.find(SwellMeasurements);
    expect(swellMeasurementsList).to.have.length(2);
    expect(swellMeasurementsList.at(0).props()).to.deep.equal({
      direction: 1,
      period: 2,
      height: 3,
      index: 0,
      optimalScore: null,
      units: 'FT',
      isEmptySwell: false,
    });
    expect(swellMeasurementsList.at(1).props()).to.deep.equal({
      direction: 4,
      period: 5,
      height: 6,
      index: 1,
      optimalScore: null,
      units: 'FT',
      isEmptySwell: false,
    });
    wrapper.unmount();
  });

  it('renders a thumbnail with a SwellWindRose', () => {
    const wind = {
      direction: 280,
      speed: 4,
    };
    const swells = [
      { direction: 1, period: 2, height: 3 },
      { direction: 4, period: 5, height: 6 },
      { direction: 7, period: 8, height: 9 },
    ];
    const waveHeight = {
      min: 2,
      max: 3,
      humanRelation: 'ankle to knee high',
      plus: true,
    };
    const units = { windSpeed: 'KT', waveHeight: 'FT' };
    const wrapper = shallow(
      <SpotForecastSummary
        wind={wind}
        swells={swells}
        waveHeight={waveHeight}
        units={units}
        readings={{}}
        associated={{ units: {}, advertising: { sst: {} } }}
      />
    );
    const thumbnailWrapper = wrapper.find(
      '.quiver-spot-forecast-summary__thumbnail'
    );
    expect(thumbnailWrapper).to.have.length(1);
    const swellWindRoseWrapper = wrapper.find(SwellWindRose);
    expect(swellWindRoseWrapper).to.have.length(1);
    expect(swellWindRoseWrapper.props()).to.deep.equal({
      swells,
      wind,
      units,
      mapHeight: 195,
      mapWidth: 260,
      overallMaxHeight: waveHeight.max,
    });
    wrapper.unmount();
  });
});
