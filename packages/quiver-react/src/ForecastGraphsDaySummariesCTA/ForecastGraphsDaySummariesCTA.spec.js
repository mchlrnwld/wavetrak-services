import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import ForecastGraphsDaySummariesCTA from './ForecastGraphsDaySummariesCTA';

describe('ForecastGraphsDaySummariesCTA', () => {
  it('should render the ForecastGraphsDaySummaries CTA for an Anonymous user', () => {
    const wrapper = mount(
      <ForecastGraphsDaySummariesCTA isAnonymous days={[0, 1, 2, 3, 4, 5]} />
    );
    expect(
      wrapper.find('.quiver-forecast-graphs-day-summaries-cta')
    ).to.have.length(1);
    expect(wrapper.html()).to.contain('We predict more waves in your future');
  });
  it('should render the ForecastGraphsDaySummaries CTA for a Registered user', () => {
    const wrapper = mount(
      <ForecastGraphsDaySummariesCTA
        isAnonymous={false}
        days={[0, 1, 2, 3, 4, 5]}
      />
    );
    expect(
      wrapper.find('.quiver-forecast-graphs-day-summaries-cta')
    ).to.have.length(1);
    expect(wrapper.html()).to.contain('Don’t miss the next swell');
  });
});
