import React from 'react';
import PropTypes from 'prop-types';
import productCDN from '@surfline/quiver-assets';
import { trackClickedSubscribeCTA } from '@surfline/web-common';
import Button from '../Button';

const ForecastGraphsDaySummariesCTA = ({
  isAnonymous,
  days,
  segmentProperties,
}) => {
  const title = isAnonymous
    ? 'We predict more waves in your future.'
    : 'Don’t miss the next swell.';
  const subTitle = isAnonymous
    ? 'Create a free account to view detailed forecasts and swell analyses.'
    : 'Go Premium for unlimited access to detailed long-range swell analyses.';
  const buttonText = isAnonymous ? 'SIGN UP' : 'START FREE TRIAL';
  const buttonLink = isAnonymous ? '/create-account' : '/upgrade';
  const imagePath = `${productCDN}/backgrounds/single_day-report_blur.png`;
  const onClickHandler = () => {
    trackClickedSubscribeCTA(segmentProperties);
    window.location.assign(buttonLink);
  };
  return (
    <div className="quiver-forecast-graphs-day-summaries-cta">
      {days.map(() => (
        <div className="quiver-forecast-graphs__day-summary">
          <img src={imagePath} alt={title} />
        </div>
      ))}
      <div className="quiver-forecast-graphs-day-summaries-cta__content">
        <h4 className="quiver-forecast-graphs-day-summaries-cta__title">
          {title}
        </h4>
        <span className="quiver-forecast-graphs-day-summaries-cta__subtitle">
          {subTitle}
        </span>
        <div className="quiver-forecast-graphs-day-summaries-cta__button">
          <Button onClick={onClickHandler}>{buttonText}</Button>
        </div>
      </div>
    </div>
  );
};

ForecastGraphsDaySummariesCTA.propTypes = {
  isAnonymous: PropTypes.bool,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  segmentProperties: PropTypes.shape({}).isRequired,
};

ForecastGraphsDaySummariesCTA.defaultProps = {
  isAnonymous: false,
};

export default ForecastGraphsDaySummariesCTA;
