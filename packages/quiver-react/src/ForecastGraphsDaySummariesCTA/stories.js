import React from 'react';
import ForecastGraphsDaySummariesCTA from './index';

export default {
  title: 'ForecastGraphsDaySummariesCTA',
};

export const Anonymous = () => (
  <div>
    <ForecastGraphsDaySummariesCTA isAnonymous days={[0, 1, 2, 3, 4, 5]} />
  </div>
);

export const Registered = () => (
  <div>
    <ForecastGraphsDaySummariesCTA
      isAnonymous={false}
      days={[0, 1, 2, 3, 4, 5]}
    />
  </div>
);
