import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import SurfConditions from '../SurfConditions';
import PremiumCamIconMedium from '../icons/PremiumCamIconMedium';
import spotPropType from '../PropTypes/spot';
import checkConditionValue from '../utils/checkConditionValue';

const getThumbnailClassName = (isTooltip) =>
  classNames({
    'quiver-spot-list-cta-thumbnail': true,
    'quiver-spot-list-cta-thumbnail--tooltip': isTooltip,
  });

const SpotListCTAThumbnail = ({ spot, isTooltip }) => (
  <div className={getThumbnailClassName(isTooltip)}>
    <div className="quiver-spot-list-cta-thumbnail__indicator">
      <PremiumCamIconMedium />
    </div>
    <div className="quiver-spot-list-cta-thumbnail__spot-details">
      <h3 className="quiver-spot-list-cta-thumbnail__spot-details__name">
        {spot.name}
      </h3>
      <SurfConditions
        conditions={checkConditionValue(spot.conditions.value)}
        expired={spot.conditions.expired && spot.conditions.human}
      />
    </div>
  </div>
);

SpotListCTAThumbnail.defaultProps = {
  isTooltip: false,
};

SpotListCTAThumbnail.propTypes = {
  spot: spotPropType.isRequired,
  isTooltip: PropTypes.bool,
};

export default SpotListCTAThumbnail;
