import React from 'react';
import PropTypes from 'prop-types';

import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

class VerticalScrollbar extends React.Component {
  /* istanbul ignore next */
  constructor() {
    super();
    this.state = {
      height: 0,
      dragging: false,
      start: 0,
    };

    this.jump = this.jump.bind(this);
    this.startDrag = this.startDrag.bind(this);
  }

  /* istanbul ignore next */
  componentDidMount() {
    this.calculateSize(this.props);

    // Put the Listener
    document.addEventListener('mousemove', this.onDrag.bind(this));
    document.addEventListener('touchmove', this.onDrag.bind(this));
    document.addEventListener('mouseup', this.stopDrag.bind(this));
    document.addEventListener('touchend', this.stopDrag.bind(this));
  }

  /* istanbul ignore next */
  componentWillReceiveProps(nextProps) {
    const { wrapper, area } = this.props;
    if (
      nextProps.wrapper.height !== wrapper.height ||
      nextProps.area.height !== area.height
    )
      this.calculateSize(nextProps);
  }

  /* istanbul ignore next */
  componentWillUnmount() {
    // Remove the Listener
    document.removeEventListener('mousemove', this.onDrag.bind(this));
    document.removeEventListener('touchmove', this.onDrag.bind(this));
    document.removeEventListener('mouseup', this.stopDrag.bind(this));
    document.removeEventListener('touchend', this.stopDrag.bind(this));
  }

  /* istanbul ignore next */
  onDrag(event) {
    const { dragging, start } = this.state;
    const { onDragging, wrapper, scrolling, onChangePosition } = this.props;
    if (dragging) {
      // Make The Parent being in the Dragging State
      onDragging();

      event.preventDefault();
      event.stopPropagation();

      const e = event.changedTouches ? event.changedTouches[0] : event;

      const yMovement = e.clientY - start;
      const yMovementPercentage = (yMovement / wrapper.height) * 100;

      // Update the last e.clientY
      this.setState({ start: e.clientY }, () => {
        // The next Vertical Value will be
        const next = scrolling + yMovementPercentage;

        // Tell the parent to change the position
        onChangePosition(next, 'vertical');
      });
    }
  }

  /* istanbul ignore next */
  getSize() {
    // The Elements
    const $scrollArea = this.container.parentElement;
    const $scrollWrapper = $scrollArea.parentElement;

    // Get new Elements Size
    const elementSize = {
      // Scroll Area Height and Width
      scrollAreaHeight: $scrollArea.children[0].clientHeight,
      scrollAreaWidth: $scrollArea.children[0].clientWidth,

      // Scroll Wrapper Height and Width
      scrollWrapperHeight: $scrollWrapper.clientHeight,
      scrollWrapperWidth: $scrollWrapper.clientWidth,
    };
    return elementSize;
  }

  /* istanbul ignore next */
  startDrag(event) {
    event.preventDefault();
    event.stopPropagation();

    const e = event.changedTouches ? event.changedTouches[0] : event;

    // Prepare to drag
    this.setState({
      dragging: true,
      start: e.clientY,
    });
  }

  /* istanbul ignore next */
  stopDrag() {
    const { dragging } = this.state;
    const { onStopDrag } = this.props;
    if (dragging) {
      // Parent Should Change the Dragging State
      onStopDrag();
      this.setState({ dragging: false });
    }
  }

  /* istanbul ignore next */
  jump(e) {
    const { height } = this.state;
    const { wrapper, scrolling, onChangePosition } = this.props;
    const isContainer = e.target === this.container;

    if (isContainer) {
      // Get the Element Position
      const position = this.scrollbar.getBoundingClientRect();

      // Calculate the vertical Movement
      const yMovement = e.clientY - position.top;
      const centerize = height / 2;
      const yMovementPercentage =
        (yMovement / wrapper.height) * 100 - centerize;

      // Update the last e.clientY
      this.setState({ start: e.clientY }, () => {
        // The next Vertical Value will be
        const next = scrolling + yMovementPercentage;

        // Tell the parent to change the position
        onChangePosition(next, 'vertical');
      });
    }
  }

  /* istanbul ignore next */
  calculateSize(source) {
    // Scrollbar Width
    this.setState({
      height: (source.wrapper.height / source.area.height) * 100,
    });
  }

  /* istanbul ignore next */
  render() {
    const { height, dragging } = this.state;
    const { draggingFromParent, scrolling } = this.props;
    const className = (base, name, pos, act, isAct) =>
      [
        base + name,
        base + name + pos,
        isAct ? base + name + act : '',
        isAct ? base + name + pos + act : '',
      ].join(' ');

    if (height < 100) {
      return (
        <div
          role="button"
          className={className(
            'quiver-scrollbar__scroll',
            '-track',
            ':vertical',
            ':dragging',
            dragging || draggingFromParent
          )}
          ref={(c) => {
            this.container = c;
          }}
          {...createAccessibleOnClick(this.jump, BTN)}
          style={{ position: 'absolute' }}
        >
          <div
            role="button"
            tabIndex={0}
            aria-label="scrollbar"
            className={className(
              'quiver-scrollbar__scroll',
              '-thumb',
              ':vertical',
              ':dragging',
              dragging || draggingFromParent
            )}
            ref={(c) => {
              this.scrollbar = c;
            }}
            onTouchStart={this.startDrag}
            onMouseDown={this.startDrag}
            style={{
              position: 'relative',
              height: `calc(${height}% - 3px)`,
              top: `${scrolling}%`,
            }}
          />
        </div>
      );
    }
    return null;
  }
}

// The Props
VerticalScrollbar.propTypes = {
  draggingFromParent: PropTypes.bool.isRequired,
  scrolling: PropTypes.number.isRequired,
  wrapper: PropTypes.shape().isRequired,
  area: PropTypes.shape().isRequired,
  onChangePosition: PropTypes.func.isRequired,
  onDragging: PropTypes.func.isRequired,
  onStopDrag: PropTypes.func.isRequired,
};

export default VerticalScrollbar;
