import React from 'react';
import PropTypes from 'prop-types';
import VerticalScrollbar from './VerticalScrollbar';
import HorizontalScrollbar from './HorizontalScrollbar';
import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

class ScrollWrapper extends React.Component {
  /* istanbul ignore next */
  constructor() {
    super();
    this.state = {
      ready: false,
      top: 0,
      left: 0,
      scrollAreaHeight: null,
      scrollAreaWidth: null,
      scrollWrapperHeight: null,
      scrollWrapperWidth: null,
      vMovement: 0,
      hMovement: 0,
      dragging: false, // note: dragging - fake pseudo class
      scrolling: false, // changes: scrolling (new fake pseudo class)
      reset: false, // changes: change state without rendering
      start: { y: 0, x: 0 },
    };
  }

  /* istanbul ignore next */
  componentDidMount() {
    this.updateSize();

    // Attach The Event for Responsive View~
    window.addEventListener('resize', this.updateSize);

    // Only allow wheel event when wrapper height is less than  scroll area
    this.calculateSize(() => {
      const { scrollAreaHeight, scrollWrapperHeight } = this.state;
      if (scrollAreaHeight > scrollWrapperHeight) {
        this.scrollArea.addEventListener('wheel', this.scroll);
      }
    });
  }

  /* istanbul ignore next */
  // changes: update scrollbars when parent resizing
  componentWillReceiveProps() {
    this.updateSize();
  }

  /* istanbul ignore next */
  // changes: reset settings without rerendering (need for scrolling state)
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.reset) {
      this.setState({ reset: false });
      return false;
    }
    return true;
  }

  /* istanbul ignore next */
  componentWillUnmount() {
    // Remove Event
    window.removeEventListener('resize', this.updateSize);
    this.scrollArea.removeEventListener('wheel', this.scroll);
  }

  /* istanbul ignore next */
  onDrag = (event) => {
    const { dragging, start, top, left } = this.state;
    if (dragging) {
      event.preventDefault();
      const e = event.changedTouches ? event.changedTouches[0] : event;

      // Invers the Movement
      const yMovement = start.y - e.clientY;
      const xMovement = start.x - e.clientX;

      // Update the last e.client
      this.setState({ start: { y: e.clientY, x: e.clientX } });

      // The next Vertical Value will be
      const nextY = top + yMovement;
      const nextX = left + xMovement;

      this.normalizeVertical(nextY);
      this.normalizeHorizontal(nextX);
    }
  };

  /* istanbul ignore next */
  getSize = () => {
    // The Elements
    const $scrollArea = this.scrollArea;
    const $scrollWrapper = this.scrollWrapper;

    // Get new Elements Size
    const elementSize = {
      // Scroll Area Height and Width

      // changes: support margin and no one child
      scrollAreaHeight: $scrollArea.getBoundingClientRect().height,
      scrollAreaWidth: $scrollArea.children[0].clientWidth, // fixme: not working same way

      // Scroll Wrapper Height and Width
      scrollWrapperHeight: $scrollWrapper.clientHeight,
      scrollWrapperWidth: $scrollWrapper.clientWidth,
    };

    return elementSize;
  };

  /* istanbul ignore next */
  stopDrag = () => {
    this.setState({ dragging: false });
  };

  /* istanbul ignore next */
  scrollToY = (pos) => {
    const { scrollAreaHeight, scrollWrapperHeight } = this.state;
    const maxVal = scrollAreaHeight - scrollWrapperHeight;
    let val = pos;
    if (typeof pos === 'string') {
      const valK = parseInt(pos.match(/-?[\d]*%$/)[0], 10) / 100;
      val = valK * maxVal;
    }

    if (val < 0) {
      val = maxVal + val;
    }
    this.normalizeVertical(val);
  };

  /* istanbul ignore next */
  scrollToX = (pos) => {
    const { scrollAreaWidth, scrollWrapperWidth } = this.state;
    const maxVal = scrollAreaWidth - scrollWrapperWidth;
    let val = pos;
    if (typeof pos === 'string') {
      const valK = parseInt(pos.match(/-?[\d]*%$/)[0], 10) / 100;
      val = valK * maxVal;
    }

    if (val < 0) {
      val = maxVal + val;
    }
    this.normalizeHorizontal(val);
  };

  /* istanbul ignore next */
  normalizeVertical = (nextPos, nextState) => {
    const { scrollAreaHeight, scrollWrapperHeight } = this.state;
    // Vertical Scrolling
    const lowerEnd = scrollAreaHeight - scrollWrapperHeight;

    // Max Scroll Down
    // Max Scroll Up
    const trim = (max, min, val) => {
      const tmax = val > max ? max : val;
      const tmin = tmax < min ? min : tmax;
      return tmin;
    };
    const next = trim(lowerEnd, 0, nextPos);

    // Update the Vertical Value
    this.setState(
      {
        top: next,
        vMovement: (next / scrollAreaHeight) * 100,
      },
      () => this.setState({ ...nextState })
    ); // changes: update state after operation
  };

  /* istanbul ignore next */
  normalizeHorizontal = (nextPos, nextState) => {
    const { scrollAreaWidth, scrollWrapperWidth } = this.state;
    // Horizontal Scrolling
    const rightEnd = scrollAreaWidth - scrollWrapperWidth;

    // Max Scroll Right
    // Max Scroll Right
    const trim = (max, min, val) => {
      const tmax = val > max ? max : val;
      const tmin = tmax < min ? min : tmax;
      return tmin;
    };
    const next = trim(rightEnd, 0, nextPos);

    // Update the Horizontal Value
    this.setState(
      {
        left: next,
        hMovement: (next / scrollAreaWidth) * 100,
      },
      () => this.setState({ ...nextState })
    ); // changes: update state after operation
  };

  /* istanbul ignore next */
  handleChangePosition = (movement, orientation) => {
    const { scrollAreaHeight, scrollAreaWidth } = this.state;
    // Make sure the content height is not changed
    this.calculateSize(() => {
      // Convert Percentage to Pixel
      const next = movement / 100;
      if (orientation === 'vertical')
        this.normalizeVertical(next * scrollAreaHeight);
      if (orientation === 'horizontal')
        this.normalizeHorizontal(next * scrollAreaWidth);
    });
  };

  /* istanbul ignore next */
  handleScrollbarDragging = () => {
    this.setState({ dragging: true });
  };

  /* istanbul ignore next */
  handleScrollbarStopDrag = () => {
    this.setState({ dragging: false });
  };

  /* istanbul ignore next */
  updateSize = () => {
    const {
      scrollAreaHeight,
      scrollAreaWidth,
      scrollWrapperHeight,
      scrollWrapperWidth,
    } = this.state;
    const elementSize = this.getSize();

    if (
      elementSize.scrollWrapperHeight !== scrollWrapperHeight ||
      elementSize.scrollWrapperWidth !== scrollWrapperWidth ||
      elementSize.scrollAreaHeight !== scrollAreaHeight ||
      elementSize.scrollAreaWidth !== scrollAreaWidth
    ) {
      // Set the State!
      this.setState({
        // Scroll Area Height and Width
        scrollAreaHeight: elementSize.scrollAreaHeight,
        scrollAreaWidth: elementSize.scrollAreaWidth,

        // Scroll Wrapper Height and Width
        scrollWrapperHeight: elementSize.scrollWrapperHeight,
        scrollWrapperWidth: elementSize.scrollWrapperWidth,

        // Make sure The wrapper is Ready, then render the scrollbar
        ready: true,
      });
    }
  };

  /* istanbul ignore next */
  calculateSize = (cb) => {
    const {
      scrollAreaHeight,
      scrollAreaWidth,
      scrollWrapperHeight,
      scrollWrapperWidth,
    } = this.state;
    const elementSize = this.getSize();

    if (
      elementSize.scrollWrapperHeight !== scrollWrapperHeight ||
      elementSize.scrollWrapperWidth !== scrollWrapperWidth ||
      elementSize.scrollAreaHeight !== scrollAreaHeight ||
      elementSize.scrollAreaWidth !== scrollAreaWidth
    ) {
      // Set the State!
      this.setState(
        {
          // Scroll Area Height and Width
          scrollAreaHeight: elementSize.scrollAreaHeight,
          scrollAreaWidth: elementSize.scrollAreaWidth,

          // Scroll Wrapper Height and Width
          scrollWrapperHeight: elementSize.scrollWrapperHeight,
          scrollWrapperWidth: elementSize.scrollWrapperWidth,

          // Make sure The wrapper is Ready, then render the scrollbar
          ready: true,
        },
        cb
      );
    } else cb();
  };

  /* istanbul ignore next */
  // DRAG EVENT JUST FOR TOUCH DEVICE~
  startDrag = (event) => {
    event.preventDefault();
    event.stopPropagation();

    const e = event.changedTouches ? event.changedTouches[0] : event;

    // Make sure the content height is not changed
    this.calculateSize(() => {
      // Prepare to drag
      this.setState({
        dragging: true,
        start: { y: e.pageY, x: e.pageX },
      });
    });
  };

  /* istanbul ignore next */
  scroll = (e) => {
    e.preventDefault();

    // Make sure the content height is not changed
    this.calculateSize(() => {
      // Set the wheel step
      const { speed } = this.props;
      const {
        top,
        left,
        scrollAreaHeight,
        scrollAreaWidth,
        scrollWrapperHeight,
        scrollWrapperWidth,
      } = this.state;
      const num = speed;

      // DOM events
      const shifted = e.shiftKey;
      const scrollY = e.deltaY > 0 ? num : -num;
      let scrollX = e.deltaX > 0 ? num : -num;

      // Fix Mozilla Shifted Wheel~
      if (shifted && e.deltaX === 0) scrollX = e.deltaY > 0 ? num : -num;

      // Next Value
      const nextY = top + scrollY;
      const nextX = left + scrollX;

      // Is it Scrollable?
      const canScrollY = scrollAreaHeight > scrollWrapperHeight;
      const canScrollX = scrollAreaWidth > scrollWrapperWidth;

      // changes: Set scrolling state before changing position
      this.setState({ scrolling: true }, () => {
        // Vertical Scrolling
        if (canScrollY && !shifted) {
          this.normalizeVertical(nextY, { scrolling: false, reset: true });
        }

        // Horizontal Scrolling
        if (shifted && canScrollX) {
          this.normalizeHorizontal(nextX, { scrolling: false, reset: true });
        }
      });
    });
  };

  /* istanbul ignore next */
  render() {
    const { style, className, children } = this.props;
    const {
      dragging,
      scrolling,
      top,
      left,
      ready,
      scrollAreaHeight,
      scrollAreaWidth,
      scrollWrapperWidth,
      scrollWrapperHeight,
      vMovement,
      hMovement,
    } = this.state;
    const getClassName = (base, name, pos, isDrg, isScr) =>
      [
        base + name,
        base + name + pos,
        isDrg ? `${base + name}:dragging` : '',
        isDrg ? `${base + name + pos}:dragging` : '',
        isScr ? `${base + name}:scrolling` : '',
        isScr ? `${base + name + pos}:scrolling` : '',
      ].join(' ');

    return (
      <div
        {...createAccessibleOnClick(this.updateSize, BTN)}
        className={className}
        ref={(c) => {
          this.scrollWrapper = c;
        }}
        style={{
          ...style,
          overflow: 'hidden',
          position: 'relative',
        }}
      >
        <div
          className={getClassName(
            'quiver-scrollbar__scroll',
            '-area',
            '',
            dragging,
            scrolling
          )}
          ref={(c) => {
            this.scrollArea = c;
          }}
          onTouchStart={this.startDrag}
          onTouchMove={this.onDrag}
          onTouchEnd={this.stopDrag}
          onChange={this.updateSize}
          style={{
            marginTop: `${top * -1}px`,
            marginLeft: `${left * -1}px`,
          }}
        >
          {children}

          {ready ? (
            <VerticalScrollbar
              area={{ height: scrollAreaHeight }}
              wrapper={{ height: scrollWrapperHeight }}
              scrolling={vMovement}
              draggingFromParent={dragging}
              onChangePosition={this.handleChangePosition}
              onDragging={this.handleScrollbarDragging}
              onStopDrag={this.handleScrollbarStopDrag}
            />
          ) : null}

          {ready ? (
            <HorizontalScrollbar
              area={{ width: scrollAreaWidth }}
              wrapper={{ width: scrollWrapperWidth }}
              scrolling={hMovement}
              draggingFromParent={dragging}
              onChangePosition={this.handleChangePosition}
              onDragging={this.handleScrollbarDragging}
              onStopDrag={this.handleScrollbarStopDrag}
            />
          ) : null}
        </div>
      </div>
    );
  }
}

// The Props
ScrollWrapper.propTypes = {
  speed: PropTypes.number,
  className: PropTypes.string,
  style: PropTypes.shape(),
  children: PropTypes.node,
};

ScrollWrapper.defaultProps = {
  speed: 53,
  className: 'quiver-scrollbar',
  style: {},
  children: null,
};

export default ScrollWrapper;
