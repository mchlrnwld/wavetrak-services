import React from 'react';
import PropTypes from 'prop-types';
import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

class HorizontalScrollbar extends React.Component {
  /* istanbul ignore next */
  constructor() {
    super();
    this.state = {
      width: 0,
      dragging: false,
      start: 0,
    };
  }

  /* istanbul ignore next */
  componentDidMount() {
    this.calculateSize(this.props);

    // Put the Listener
    document.addEventListener('mousemove', this.onDrag.bind(this));
    document.addEventListener('touchmove', this.onDrag.bind(this));
    document.addEventListener('mouseup', this.stopDrag.bind(this));
    document.addEventListener('touchend', this.stopDrag.bind(this));
  }

  /* istanbul ignore next */
  componentWillReceiveProps(nextProps) {
    const { wrapper, area } = this.props;
    if (
      nextProps.wrapper.width !== wrapper.width ||
      nextProps.area.width !== area.width
    )
      this.calculateSize(nextProps);
  }

  /* istanbul ignore next */
  componentWillUnmount() {
    // Remove the Listener
    document.removeEventListener('mousemove', this.onDrag.bind(this));
    document.removeEventListener('touchmove', this.onDrag.bind(this));
    document.removeEventListener('mouseup', this.stopDrag.bind(this));
    document.removeEventListener('touchend', this.stopDrag.bind(this));
  }

  /* istanbul ignore next */
  onDrag = (event) => {
    const { dragging, start } = this.state;
    const { onDragging, wrapper, scrolling, onChangePosition } = this.props;
    if (dragging) {
      // Make The Parent being in the Dragging State
      onDragging();

      event.preventDefault();
      event.stopPropagation();

      const e = event.changedTouches ? event.changedTouches[0] : event;

      const xMovement = e.clientX - start;
      const xMovementPercentage = (xMovement / wrapper.width) * 100;

      // Update the last e.clientX
      this.setState({ start: e.clientX }, () => {
        // The next Horizontal Value will be
        const next = scrolling + xMovementPercentage;

        // Tell the parent to change the position
        onChangePosition(next, 'horizontal');
      });
    }
  };

  /* istanbul ignore next */
  startDrag = (event) => {
    event.preventDefault();
    event.stopPropagation();

    const e = event.changedTouches ? event.changedTouches[0] : event;

    // Prepare to drag
    this.setState({
      dragging: true,
      start: e.clientX,
    });
  };

  /* istanbul ignore next */
  stopDrag = () => {
    const { dragging } = this.state;
    const { onStopDrag } = this.props;
    if (dragging) {
      // Parent Should Change the Dragging State
      onStopDrag();
      this.setState({ dragging: false });
    }
  };

  /* istanbul ignore next */
  jump = (e) => {
    const { width } = this.state;
    const { wrapper, scrolling, onChangePosition } = this.props;
    const isContainer = e.target === this.container;

    if (isContainer) {
      // Get the Element Position
      const position = this.scrollbar.getBoundingClientRect();

      // Calculate the horizontal Movement
      const xMovement = e.clientX - position.left;
      const centerize = width / 2;
      const xMovementPercentage = (xMovement / wrapper.width) * 100 - centerize;

      // Update the last e.clientX
      this.setState({ start: e.clientX }, () => {
        // The next Horizontal Value will be
        const next = scrolling + xMovementPercentage;

        // Tell the parent to change the position
        onChangePosition(next, 'horizontal');
      });
    }
  };

  /* istanbul ignore next */
  calculateSize(source) {
    // Scrollbar Width
    this.setState({ width: (source.wrapper.width / source.area.width) * 100 });
  }

  /* istanbul ignore next */
  render() {
    const { width, dragging } = this.state;
    const { draggingFromParent, scrolling } = this.props;
    const getClassName = (base, name, pos, act, isAct) =>
      [
        base + name,
        base + name + pos,
        isAct ? base + name + act : '',
        isAct ? base + name + pos + act : '',
      ].join(' ');

    if (width < 100) {
      return (
        <div
          className={getClassName(
            'quiver-scrollbar__scroll',
            '-track',
            ':horizontal',
            ':dragging',
            dragging || draggingFromParent
          )}
          ref={(c) => {
            this.container = c;
          }}
          {...createAccessibleOnClick(this.jump, BTN)}
          style={{ position: 'absolute' }}
        >
          <div
            role="button"
            aria-label="scrollbar"
            tabIndex="0"
            className={getClassName(
              'quiver-scrollbar__scroll',
              '-thumb',
              ':horizontal',
              ':dragging',
              dragging || draggingFromParent
            )}
            ref={(c) => {
              this.scrollbar = c;
            }}
            onTouchStart={this.startDrag}
            onMouseDown={this.startDrag}
            style={{
              position: 'relative',
              width: `${width}%`,
              left: `${scrolling}%`,
            }}
          />
        </div>
      );
    }
    return null;
  }
}

// The Props
HorizontalScrollbar.propTypes = {
  draggingFromParent: PropTypes.bool.isRequired,
  scrolling: PropTypes.number.isRequired,
  wrapper: PropTypes.shape().isRequired,
  area: PropTypes.shape().isRequired,
  onChangePosition: PropTypes.func.isRequired,
  onDragging: PropTypes.func.isRequired,
  onStopDrag: PropTypes.func.isRequired,
};

export default HorizontalScrollbar;
