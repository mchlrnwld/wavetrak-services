import React from 'react';
import Scrollbar from './index';

export default {
  title: 'Scrollbar',
};

export const Default = () => (
  <Scrollbar style={{ height: 200, width: 200 }}>
    <ol>
      <li>Content Here (1)</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here</li>
      <li>Content Here (18)</li>
    </ol>
  </Scrollbar>
);

Default.story = {
  name: 'default',
};
