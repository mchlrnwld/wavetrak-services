import React from 'react';
import PropTypes from 'prop-types';

const arrowDirection = (direction) =>
  direction === 'DOWN' ? 'rotate(180 5.5 7.5)' : null;

const TideArrow = ({ direction }) => (
  <svg
    viewBox="0 0 11 15"
    preserveAspectRatio="xMinYMin meet"
    className="quiver-tide-arrow"
  >
    <polygon
      points="5.5,0 11,5.5 8,5.5 8,15 3,15 3,5.5 0,5.5"
      transform={arrowDirection(direction)}
    />
  </svg>
);

TideArrow.propTypes = {
  direction: PropTypes.oneOf(['UP', 'DOWN']),
};

TideArrow.defaultProps = {
  direction: 'UP',
};

export default TideArrow;
