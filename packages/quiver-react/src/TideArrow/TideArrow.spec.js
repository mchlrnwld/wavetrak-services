import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import TideArrow from './TideArrow';

describe('components / TideArrow', () => {
  it('rotates 180 degrees around the center if direction is DOWN', () => {
    const upTideArrow = shallow(<TideArrow direction="UP" />);
    const upPolygon = upTideArrow.find('polygon');
    expect(upPolygon.prop('transform')).to.be.null();

    const downTideArrow = shallow(<TideArrow direction="DOWN" />);
    const downPolygon = downTideArrow.find('polygon');
    expect(downPolygon.prop('transform')).to.equal('rotate(180 5.5 7.5)');
  });
});
