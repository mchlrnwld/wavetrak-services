import React from 'react';
import PropTypes from 'prop-types';
import ShareArticle from '../ShareArticle';
import ContentCardPaywall from '../ContentCardPaywall';
import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

const ForecastArticleBody = ({
  id,
  title,
  body,
  showMore,
  url,
  showShare,
  onClickShareArticle,
  showPaywall,
  onClickReadMore,
  funnelUrl,
  onClickPaywall,
  showAll,
  toggleShowMore,
}) => (
  <div className="quiver-forecast-article-card__body-wrapper">
    <h2 dangerouslySetInnerHTML={{ __html: title }} />
    <div className="quiver-forecast-article-card__body-container">
      <div
        className="quiver-forecast-article-card__body"
        dangerouslySetInnerHTML={{ __html: body }}
        id={id}
      />
      {(showAll || showMore) && url && showShare ? (
        <div className="quiver-forecast-article-card__body__share">
          <span className="quiver-forecast-article-card__body__share__title">
            Share this post
          </span>
          <ShareArticle url={url} onClickLink={onClickShareArticle} />
        </div>
      ) : null}
      {!showAll && !showPaywall ? (
        <a
          role="button"
          className="quiver-forecast-article-card__read-more-less"
          {...createAccessibleOnClick(toggleShowMore, BTN)}
        >
          <span {...createAccessibleOnClick(onClickReadMore, BTN)}>
            {showMore ? 'Read Less' : 'Read More'}
          </span>
        </a>
      ) : null}
      {showPaywall && (
        <div className="quiver-forecast-article-card__content-fade" />
      )}
    </div>
    {showPaywall && (
      <ContentCardPaywall funnelUrl={funnelUrl} onClick={onClickPaywall} />
    )}
  </div>
);

ForecastArticleBody.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string.isRequired,
  body: PropTypes.string,
  showPaywall: PropTypes.bool,
  showMore: PropTypes.bool,
  funnelUrl: PropTypes.string,
  onClickPaywall: PropTypes.func,
  onClickReadMore: PropTypes.func,
  onClickShareArticle: PropTypes.func,
  url: PropTypes.string,
  showShare: PropTypes.bool,
  showAll: PropTypes.bool,
  toggleShowMore: PropTypes.func,
};

ForecastArticleBody.defaultProps = {
  id: null,
  body: '',
  showPaywall: false,
  showMore: false,
  funnelUrl: '',
  onClickPaywall: null,
  onClickReadMore: null,
  onClickShareArticle: null,
  url: '',
  showShare: true,
  showAll: true,
  toggleShowMore: null,
};

export default ForecastArticleBody;
