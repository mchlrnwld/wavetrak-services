import React from 'react';
import { color } from '@surfline/quiver-themes';
import ProgressBar from './index';

export default {
  title: 'ProgressBar',
};

export const DefaultStep1 = () => (
  <ProgressBar
    currentStep="1"
    totalSteps="3"
    progressText="Step 1 of 3"
    bgColor={color('blue-gray', 90)}
  />
);

DefaultStep1.story = {
  name: 'default - Step 1',
};

export const Step2 = () => (
  <ProgressBar
    currentStep="2"
    totalSteps="3"
    progressText="Step 2 of 3"
    bgColor={color('blue-gray', 90)}
  />
);

export const FinalStep = () => (
  <ProgressBar
    currentStep="3"
    totalSteps="3"
    progressText="Completed"
    bgColor={color('green')}
  />
);
