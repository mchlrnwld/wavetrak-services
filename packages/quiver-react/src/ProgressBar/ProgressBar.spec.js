import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ProgressBar from './ProgressBar';

describe('ProgressBar', () => {
  it('should render ProgressBar into DOM', () => {
    const wrapper = shallow(
      <ProgressBar currentStep="1" totalSteps="4" progressText="Step 1 of 4" />
    );
    const progressbar = wrapper.find('.quiver-progress-bar');
    expect(progressbar).to.have.length(1);
  });
});
