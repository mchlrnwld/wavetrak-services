import React from 'react';
import PropTypes from 'prop-types';

const ProgressBar = ({ currentStep, totalSteps, progressText, bgColor }) => {
  const progress = (currentStep / totalSteps) * 100;
  return currentStep ? (
    <div className="quiver-progress-bar">
      <div
        className="quiver-progress-bar__filler"
        style={{ width: `${progress}%`, background: bgColor }}
      />
      <div className="quiver-progress-bar__text">{progressText}</div>
    </div>
  ) : null;
};

ProgressBar.propTypes = {
  currentStep: PropTypes.number.isRequired,
  totalSteps: PropTypes.number.isRequired,
  progressText: PropTypes.string.isRequired,
  bgColor: PropTypes.string,
};
ProgressBar.defaultProps = {
  bgColor: null,
};
export default ProgressBar;
