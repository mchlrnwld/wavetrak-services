/* istanbul ignore file */

import { useEffect, useState, useMemo, useRef, useCallback } from 'react';

/**
 * @description  useTimeout is a custom hook that sets a leak-safe timeout and returns
 * an array with the timeout state, and a function to update that state
 * The timer is restarted every time an item in `triggers` changes. `triggers`
 * should be an array (any non-primitives should be immutable), so that React's `useEffect` can perform a comparison
 * between the current and previous instance of the array to determine if the timeout should be reset.
 * If a new `timeoutLength` is passed, the hook cancels the current timer and
 * creates a new one with the new timeout length.
 *
 * @param {Object} params
 * @param {number} params.timeoutLength Length of the timeout that is desired
 * @param {Array<Any>} [params.triggers=[]] Triggers that cause the timer to reset
 * @param {Boolean} [params.initialState=false] A callback to execute at the end of the timeout
 * @param {Function} [params.cb=false] A callback to execute at the end of the timeout
 * @param {Boolean} [params.start=true] A condition under which we start the timer
 * @returns {[ boolean, function ]}
 */
export const useTimeout = ({
  timeoutLength,
  triggers = [],
  initialState = false,
  cb = false,
  start = true,
}) => {
  const timer = useRef();
  const callback = useRef(cb);
  const [timeout, setTimeoutState] = useState(initialState);

  // update ref when function changes
  useEffect(() => {
    callback.current = cb;
  }, [cb]);

  /**
   * We can disable this lint rule on this line. We leave it up to the consumer
   * to ensure that any non-primitives are memoized in order to ensure that the
   * timeout works properly
   */
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const memoizedTriggers = useMemo(() => triggers, triggers);

  // Create the timer
  const set = useCallback(() => {
    setTimeoutState(false);
    if (timer.current) {
      clearTimeout(timer.current);
    }

    timer.current = setTimeout(() => {
      setTimeoutState(true);
      callback.current();
    }, timeoutLength);
  }, [timeoutLength]);

  // Cleanup function for the timer
  const clear = useCallback(() => {
    if (timer.current) {
      clearTimeout(timer.current);
    }
  }, []);

  useEffect(() => {
    if (start) {
      set();
    }

    return clear;
  }, [clear, set, start, memoizedTriggers]);

  return [timeout, setTimeoutState];
};

export default useTimeout;
