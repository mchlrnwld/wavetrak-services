/* istanbul ignore file */
import { useRef } from 'react';

/**
 * @description In order to more easily stub and test the functionality
 * on this logic we want to create a wrapper function
 */
export const useGraphContainerRef = () => {
  /** @type {React.MutableRefObject<HTMLDivElement>} */
  const ref = useRef();

  return ref;
};
