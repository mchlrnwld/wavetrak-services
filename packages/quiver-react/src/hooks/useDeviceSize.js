/* istanbul ignore file */
import { useState, useEffect, useCallback } from 'react';

/**
 * @description A react hook that will return an object with the
 * width and height of the browser window
 *
 * @returns {{
 *  width: number,
 *  height: number,
 *  isDesktop: boolean,
 *  isMobile: boolean,
 *  isTablet: boolean,
 * }}
 */
export function useDeviceSize() {
  const isClient = typeof window === 'object';

  const getSize = useCallback(
    () => ({
      width: isClient ? window.innerWidth : null,
      height: isClient ? window.innerHeight : null,
      isDesktop: isClient ? window.innerWidth > 976 : null,
      isMobile: isClient ? window.innerWidth < 512 : null,
      mobile: isClient ? window.innerWidth < 512 : null,
      isTablet: isClient
        ? window.innerWidth > 512 && window.innerWidth < 976
        : null,
    }),
    [isClient]
  );

  const [windowSize, setWindowSize] = useState(getSize);

  useEffect(() => {
    if (!isClient) {
      return false;
    }

    function handleResize() {
      setWindowSize(getSize());
    }

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, [getSize, isClient]);

  return windowSize;
}

export default useDeviceSize;
