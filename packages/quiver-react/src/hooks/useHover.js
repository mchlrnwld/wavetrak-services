/* istanbul ignore file */

import { useState, useEffect, useCallback } from 'react';
import classNames from 'classnames';

import { useSpring } from 'framer-motion';

export const useSpringAnimation = (value, defaultValue, isHovering) => {
  const spring = useSpring(defaultValue);

  useEffect(() => {
    if (!!value && isHovering) {
      spring.set(value);
    } else {
      spring.set(defaultValue);
    }
  }, [defaultValue, isHovering, spring, value]);

  return spring;
};

/**
 * @description A hook used to implement hover states on a DOM node. This hook will track the hover state and
 * return that state in the first position of the array, the second position will contain DOM attributes
 * to be set on the DOM node using the hover state. The second position of the array is a pseudo `props` object
 * to be used on the DOM node like so `<HoverableDomNode {...hoverAttributes} />`
 *
 * @param {Object} options
 * @param {String} [options.className=""] Optional className that the hook will add `--hover`
 * to the className when the hover state is true
 * @param {Boolean|Number} [options.animateScale=false] Optionally animate the scale, if desired pass the
 * desired scale number to animate to (`0.9` being the non-hover size)
 * @param {Boolean|Number} [options.animateRotation=false] Optionally animate the rotation, if desired pass the
 * desired rotation number to animate to (`1` being the non-hover size)
 * @param {Boolean} [options.bubble=true] If we want to event to bubble we need to use `onMouseOut` and `onMouseOver`
 *
 * @returns {{
 *   hoverProps: {
 *     onMouseOver?: Function,
 *     onMouseOut?: Function,
 *     onMouseLeave?: Function,
 *     onMouseEnter?: Function,
 *     onFocus: Function,
 *     onBlur: Function,
 *     className?: String
 *   },
 *   isHovering: boolean,
 *   scale: import('framer-motion').MotionValue,
 *   rotation: import('framer-motion').MotionValue
 * }}
 */
export const useHover = ({
  className = '',
  animateScale = false,
  animateRotation = false,
  bubble = true,
} = {}) => {
  const [isHovering, setHovering] = useState(false);
  const enableHovering = useCallback(() => setHovering(true), []);
  const disableHovering = useCallback(() => setHovering(false), []);
  const scale = useSpringAnimation(animateScale, 0.95, isHovering);
  const rotation = useSpringAnimation(animateRotation, 0, isHovering);

  const classes = classNames({
    [className]: true,
    [`${className}--hover`]: isHovering,
  });

  return {
    hoverProps: {
      // Optionally assign the classNames key if a className arg was passed
      ...(!!className && { className: classes }),
      ...(bubble && { onMouseOver: () => enableHovering() }),
      ...(bubble && { onMouseOut: () => disableHovering() }),
      ...(!bubble && { onMouseEnter: () => enableHovering() }),
      ...(!bubble && { onMouseLeave: () => disableHovering() }),
      tabIndex: 0,
      onFocus: () => enableHovering(),
      onBlur: () => {
        disableHovering();
      },
    },
    isHovering,
    scale,
    rotation,
    disableHovering,
  };
};
