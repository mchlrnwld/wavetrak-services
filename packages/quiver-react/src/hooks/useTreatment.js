/* istanbul ignore file */

import { useState, useEffect } from 'react';
import { getTreatment, getTreatmentWithConfig } from '@surfline/web-common';

/**
 * @typedef {object} Result
 * @property {import('@surfline/web-common').Treatment} treatment
 * @property {boolean} loading
 */

/**
 * @typedef {object} ResultWithConfig
 * @property {import('@surfline/web-common').TreatmentWithConfig} treatment
 * @property {boolean} loading
 */

/**
 * @typedef {object} Options
 * @property {boolean} [splitReady=true]
 */

/**
 * @typedef {object} OptionsWithConfig
 * @property {boolean} [hasConfig=false]
 * @property {boolean} [splitReady=true]
 */

/**
 * @typedef {(treatmentName: string, options?: Options) => Result} Treatment
 */

/**
 * @typedef {(treatmentName: string, options: OptionsWithConfig) => ResultWithConfig} TreatmentWithConfig
 */

/**
 * @typedef {?(Treatment | TreatmentWithConfig)} TreatmentState
 */

/**
 * @type {Treatment & TreatmentWithConfig}
 */
export const useTreatment = (
  treatmentName,
  { hasConfig, splitReady } = { hasConfig: false, splitReady: true }
) => {
  /** @type {[TreatmentState, React.Dispatch<React.SetStateAction<TreatmentState>>]} */
  const [treatment, setTreatment] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (splitReady) {
      const result = hasConfig
        ? getTreatmentWithConfig(treatmentName)
        : getTreatment(treatmentName);

      if (result) {
        setTreatment(result);
        setLoading(false);
      }
    }
  }, [splitReady, hasConfig, treatmentName]);

  return { loading, treatment };
};
