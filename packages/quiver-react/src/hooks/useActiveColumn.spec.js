/* eslint-disable react/prop-types */
import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { beforeEach, afterEach } from 'mocha';
import sinon from 'sinon';
import { useActiveColumn } from './useActiveColumn';
import * as refHook from './useGraphContainerRef';

describe('components / ContinuousGraphContainer / useActiveColumn', () => {
  /** @type {sinon.SinonStub} */
  let useGraphContainerRefStub;

  const getBoundingClientRectStub = sinon.stub();
  beforeEach(() => {
    useGraphContainerRefStub = sinon.stub(refHook, 'useGraphContainerRef');

    getBoundingClientRectStub.returns({
      width: 100,
      height: 100,
      left: 0,
      right: 100,
      top: 0,
    });

    useGraphContainerRefStub.returns({
      current: {
        getBoundingClientRect: getBoundingClientRectStub,
      },
    });
  });

  afterEach(() => {
    getBoundingClientRectStub.reset();
    useGraphContainerRefStub.restore();
  });

  const Component = ({
    highlightActiveColumn,
    showMobileTooltip,
    totalColumnNum,
    isHourly,
    currentColumn,
  }) => {
    const { activeColumn, handleMouseMove, handleTouch } = useActiveColumn({
      highlightActiveColumn,
      showMobileTooltip,
      totalColumnNum,
      currentColumn,
      isHourly,
    });

    return (
      <div
        className="dummy-div"
        onMouseMove={handleMouseMove}
        onTouchStart={handleTouch}
        onTouchMove={handleTouch}
      >
        {activeColumn}
      </div>
    );
  };

  it('should handle setting an active column on mouse move', () => {
    const wrapper = mount(
      <Component highlightActiveColumn totalColumnNum={24} days={[0]} />
    );

    wrapper
      .find('.dummy-div')
      .prop('onMouseMove')
      .apply(this, [{ clientX: 5, clientY: 5 }]);

    expect(wrapper.text()).to.be.equal('1');
  });

  it('should handle setting an active column on touch', () => {
    const wrapper = mount(
      <Component highlightActiveColumn totalColumnNum={24} days={[0]} />
    );

    wrapper
      .find('.dummy-div')
      .prop('onTouchStart')
      .apply(this, [
        {
          changedTouches: {
            item: () => ({ clientX: 10, clientY: 10 }),
          },
        },
      ]);

    expect(wrapper.text()).to.be.equal('2');
  });

  it('should short circuit active column logic if highlightActiveColum is not set', () => {
    const wrapper = mount(
      <Component highlightActiveColumn={false} totalColumnNum={24} days={[0]} />
    );

    wrapper
      .find('.dummy-div')
      .prop('onMouseMove')
      .apply(this, [{ clientX: 5, clientY: 5 }]);

    expect(wrapper.text()).to.be.equal('');

    wrapper.setProps({ isHourly: true });

    expect(wrapper.text()).to.be.equal('');
  });

  it('should remove an activeColumn if isHourly changes', () => {
    const wrapper = mount(
      <Component
        highlightActiveColumn
        totalColumnNum={24}
        days={[0]}
        isHourly={false}
      />
    );

    wrapper
      .find('.dummy-div')
      .prop('onMouseMove')
      .apply(this, [{ clientX: 5, clientY: 5 }]);

    expect(wrapper.text()).to.be.equal('1');

    wrapper.setProps({ isHourly: true });

    expect(wrapper.text()).to.be.equal('');

    wrapper.setProps({ isHourly: true, days: [0] });

    expect(wrapper.text()).to.be.equal('');
  });

  it('should set an initial active column for mobile tooltips', () => {
    const wrapper = mount(
      <Component
        highlightActiveColumn
        totalColumnNum={24}
        days={[0]}
        showMobileTooltip
        currentColumn={2}
      />
    );

    expect(wrapper.text()).to.be.equal('2');
  });
});
