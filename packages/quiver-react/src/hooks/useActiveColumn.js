import { useCallback, useState } from 'react';
import { useUpdateEffect } from 'react-use';
import { useGraphContainerRef } from './useGraphContainerRef';

/**
 * @param {object} props
 * @param {boolean} props.highlightActiveColumn
 * @param {boolean} props.showMobileTooltip
 * @param {number} props.totalColumnNum
 * @param {boolean} props.isHourly
 * @param {number?} props.currentColumn
 */
export const useActiveColumn = ({
  highlightActiveColumn,
  showMobileTooltip,
  totalColumnNum,
  isHourly,
  currentColumn,
}) => {
  const ref = useGraphContainerRef();

  const getInitialActiveColumn = useCallback(() => {
    // We only want to set an initial activeColumn if we are showing the mobile tooltip
    if (showMobileTooltip) {
      // we use the `currentColumn` if defined, if not we want to just set the
      // active column to be the first one so we use 0
      return currentColumn ?? 0;
    }
    return null;
  }, [showMobileTooltip, currentColumn]);

  const [activeColumn, setActiveColumn] = useState(getInitialActiveColumn());

  const removeActiveColumn = useCallback(() => {
    if (!highlightActiveColumn) {
      return null;
    }
    return setActiveColumn(null);
  }, [highlightActiveColumn]);

  useUpdateEffect(() => {
    removeActiveColumn();
  }, [isHourly]);

  /** @param {React.MouseEvent["clientX"]} clientX */
  const doSetActiveColumn = (clientX) => {
    if (!highlightActiveColumn || !ref.current) {
      return null;
    }

    const box = ref.current.getBoundingClientRect();

    const xFromLeft = clientX - box.left;

    const singleColumnWidth = box.width / totalColumnNum;

    // The active column is derived by dividing the X position of the cursor within the box
    // by the width of a single column and rouding down. This will return the closest column
    // number
    return setActiveColumn(Math.floor(xFromLeft / singleColumnWidth));
  };

  /** @param {React.MouseEvent} props */
  const handleMouseMove = ({ clientX }) => {
    doSetActiveColumn(clientX);
  };

  /** @param {React.TouchEvent} props */
  const handleTouch = ({ changedTouches }) => {
    const touch = changedTouches.item(0);
    if (touch) {
      doSetActiveColumn(touch.clientX);
    }
  };

  return {
    activeColumn,
    handleMouseMove,
    handleTouch,
    removeActiveColumn,
    ref,
  };
};
