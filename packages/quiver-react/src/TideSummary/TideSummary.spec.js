import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ReadingDescription from '../ReadingDescription';
import TideSummary from './TideSummary';

describe('components / TideSummary', () => {
  it('renders summary with formatted tide data', () => {
    const tide = {
      type: 'HIGH',
      height: 3.4,
      timestamp: 1484774262,
      utcOffset: -8,
    };
    const wrapper = shallow(<TideSummary tide={tide} units="FT" />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.contain('High tide 3.4ft');
  });

  it('renders summary', () => {
    const wrapper = shallow(<TideSummary />);
    const readingDescription = wrapper.find(ReadingDescription);
    expect(readingDescription.children().text()).to.contain(
      'No Tide Data Available'
    );
  });
});
