import React from 'react';
import TideSummary from './TideSummary';

export default {
  title: 'TideSummary',
};

export const Default = () => (
  <TideSummary
    tide={{
      height: 1.1,
      timestamp: Math.floor(+new Date() / 1000),
      utcOffset: -8,
      type: 'LOW',
    }}
    units="FT"
  />
);
