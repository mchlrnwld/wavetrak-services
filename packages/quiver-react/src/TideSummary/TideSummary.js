import PropTypes from 'prop-types';
import React from 'react';
import { format } from 'date-fns';
import { getLocalDate, capitalizeFirstLetter } from '@surfline/web-common';

import ReadingDescription from '../ReadingDescription';

const summary = (tide, units) => {
  if (!tide) return 'No Tide Data Available';

  const time = format(
    getLocalDate(tide.timestamp, tide.utcOffset),
    "hh:mmaaaaa'm'"
  );
  return (
    <span>
      {`${capitalizeFirstLetter(tide.type)} tide ${
        tide.height
      }${units.toLowerCase()}`}
      <br />
      {`at ${time}`}
    </span>
  );
};

const TideSummary = ({ tide, units }) => (
  <ReadingDescription>{summary(tide, units)}</ReadingDescription>
);

TideSummary.propTypes = {
  units: PropTypes.string.isRequired,
  tide: PropTypes.shape({
    type: PropTypes.string,
    height: PropTypes.number,
    timestamp: PropTypes.number,
    utcOffset: PropTypes.number,
  }),
};

TideSummary.defaultProps = {
  tide: null,
};

export default TideSummary;
