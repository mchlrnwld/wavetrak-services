import withWindow from '../withWindow';
import NewNavigationBar from './NewNavigationBar';

export default withWindow(NewNavigationBar);
