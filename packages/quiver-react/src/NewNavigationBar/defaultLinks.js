const legacyUrl = {
  development: 'http://qa.surfline.com',
  sandbox: 'http://qa.surfline.com',
  staging: 'http://staging.surfline.com',
  production: 'http://www.surfline.com',
}[process.env.NODE_ENV || 'production'];

export const defaultNewsLinks = [
  { display: 'Breaking', href: '/category/breaking' },
  { display: 'Features', href: '/category/features' },
  { display: 'Video', href: '/category/video' },
  { display: 'Travel', href: '/category/travel' },
  { display: 'Gear', href: '/category/gear' },
  { display: 'Training', href: '/category/training' },
  { display: 'All', href: '/surf-news' },
];

export const accountLinks = [
  { display: 'Account', href: '/account', newWindow: false },
];

export const promotionalLinks = [
  { display: 'Premium Perks', href: '/premium-perks', newWindow: false },
  { display: 'Gift Cards', href: '/gift-cards', newWindow: false },
];

export const productLinks = [
  {
    display: 'Multi-Cam',
    href: `/surf-cams`,
    newWindow: true,
  },
  { display: 'Favorites', href: '/account/favorites', newWindow: false },
];

export const defaultAccountLinks = [
  ...accountLinks,
  ...promotionalLinks,
  ...productLinks,
];

export const camsReportsLinks = [
  {
    display: 'All Surf Spots',
    href: '/surf-reports-forecasts-cams',
    newWindow: false,
  },
  {
    display: 'Map',
    href: '/surf-reports-forecasts-cams-map',
    newWindow: false,
  },
  {
    display: 'Multi-Cam',
    href: `/surf-cams`,
    newWindow: false,
  },
];

export const forecastsLinks = [
  { display: 'Charts', href: `${legacyUrl}/surf-charts/`, newWindow: false },
  {
    display: 'Meet the Forecasters',
    href: '/lp/forecasters',
    newWindow: true,
  },
  { display: 'Hurricanes', href: '/series/hurricane', newWindow: true },
];
