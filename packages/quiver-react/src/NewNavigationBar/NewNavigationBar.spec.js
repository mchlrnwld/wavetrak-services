import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import common from '@surfline/web-common';
import { defaultAccountLinks } from './defaultLinks';
import NavigationItem from '../NavigationItem';
import CamsReportsMenu from '../CamsReportsMenu';
import ForecastsMenu from '../ForecastsMenu';
import FavoritesMenu from '../FavoritesMenu';
import NewsMenu from '../NewsMenu';
import UserMenu from '../UserMenu';
import SiteSearch from '../SiteSearch';
import NewNavigationBar from './NewNavigationBar';

describe('NewNavigationBar', () => {
  const props = {
    user: {},
    serviceConfig: {
      serviceUrl: 'surflinetest.com',
    },
  };

  beforeEach(() => {
    sinon.stub(common, 'trackEvent');
  });

  afterEach(() => {
    common.trackEvent.restore();
  });

  it('should render a news menu with links', () => {
    const wrapper = shallow(<NewNavigationBar {...props} />);
    const newsMenuWrapper = wrapper.find(NewsMenu);
    expect(newsMenuWrapper).to.have.length(1);
  });

  it('should display a CTA that handles clicks if user is not premium', () => {
    const wrapper = shallow(<NewNavigationBar {...props} />);
    const ctaWrapper = wrapper.find('.quiver-new-navigation-bar__cta__link');
    expect(ctaWrapper).to.have.length(1);
  });

  it('should not display CTA if user is premium', () => {
    const wrapper = shallow(
      <NewNavigationBar {...props} entitlements={['sl_premium']} />
    );
    const ctaWrapper = wrapper.find(
      '.quiver-new-navigation-bar__account__cta__link'
    );
    expect(ctaWrapper).to.have.length(0);
  });

  it('should not display CTA if noCTA prop is true', () => {
    const wrapper = shallow(
      <NewNavigationBar {...props} noCTA entitlements={[]} />
    );
    const ctaWrapper = wrapper.find(
      '.quiver-new-navigation-bar__account__cta__link'
    );
    expect(ctaWrapper).to.have.length(0);
  });

  it('should render a user menu with user information, entitlements, and account links', () => {
    const entitlements = [];
    const wrapper = shallow(
      <NewNavigationBar {...props} entitlements={entitlements} />
    );
    const userMenuWrapper = wrapper.find(UserMenu);
    expect(userMenuWrapper).to.have.length(1);
    expect(userMenuWrapper.prop('user')).to.equal(props.user);
    expect(userMenuWrapper.prop('entitlements')).to.equal(entitlements);
    expect(userMenuWrapper.prop('links')).to.equal(defaultAccountLinks);
  });

  it('should be able to render and close a mobile menu', () => {
    const wrapper = shallow(<NewNavigationBar {...props} />);
    wrapper.setState({ viewport: 'mobile' });
    const mobileNavWrapper = wrapper.find(
      '.quiver-new-navigation-bar__mobile__nav'
    );
    const mobileNavigationItems = mobileNavWrapper.find(NavigationItem);
    expect(mobileNavigationItems).to.have.length(4);
    expect(
      wrapper.find('.quiver-new-navigation-bar__mobile__nav__menu')
    ).to.have.length(0);

    expect(mobileNavigationItems.at(0).prop('linkDisplay')).to.equal('Cams');
    mobileNavigationItems.at(0).simulate('click');
    const camsMenuWrapper = wrapper.find(
      '.quiver-new-navigation-bar__mobile__nav__menu'
    );
    expect(camsMenuWrapper).to.have.length(1);
    expect(camsMenuWrapper.find(CamsReportsMenu)).to.have.length(1);
    const camsMenuWrapperCloseIcon = camsMenuWrapper.find(
      '.quiver-new-navigation-bar__mobile__nav__menu__close'
    );
    expect(camsMenuWrapperCloseIcon).to.have.length(1);

    camsMenuWrapperCloseIcon.simulate('click');
    expect(
      wrapper.find('.quiver-new-navigation-bar__mobile__nav__menu')
    ).to.have.length(0);

    expect(mobileNavigationItems.at(2).prop('linkDisplay')).to.equal(
      'Favorites'
    );
    mobileNavigationItems.at(2).simulate('click');
    const favoritesMenuWrapper = wrapper.find(
      '.quiver-new-navigation-bar__mobile__nav__menu'
    );
    expect(favoritesMenuWrapper).to.have.length(1);
    expect(favoritesMenuWrapper.find(FavoritesMenu)).to.have.length(1);
    const favoritesMenuWrapperCloseIcon = favoritesMenuWrapper.find(
      '.quiver-new-navigation-bar__mobile__nav__menu__close'
    );
    expect(favoritesMenuWrapperCloseIcon).to.have.length(1);

    favoritesMenuWrapperCloseIcon.simulate('click');
    expect(
      wrapper.find('.quiver-new-navigation-bar__mobile__nav__menu')
    ).to.have.length(0);

    expect(mobileNavigationItems.at(1).prop('linkDisplay')).to.equal(
      'Forecasts'
    );
    expect(mobileNavigationItems.at(3).prop('linkDisplay')).to.equal('News');
  });

  it('should render a Cams and Reports Menu and a Forecasts Menu', () => {
    const wrapper = shallow(<NewNavigationBar {...props} />);
    const camsReportsWrapper = wrapper.find(CamsReportsMenu);
    const forecastsWrapper = wrapper.find(ForecastsMenu);
    expect(camsReportsWrapper).to.have.length(1);
    expect(forecastsWrapper).to.have.length(1);
  });

  it('should render a SiteSearch if slNewsSearch enabled', () => {
    const defaultWrapper = shallow(<NewNavigationBar {...props} />);
    const defaultSiteSearch = defaultWrapper.find(SiteSearch);
    expect(defaultSiteSearch).to.have.length(1);
  });

  it('should pass sevenRatings prop to Cams and Reports Menu when true', () => {
    const wrapper = shallow(<NewNavigationBar {...props} sevenRatings />);
    const camsReportsWrapper = wrapper.find(CamsReportsMenu);
    expect(camsReportsWrapper.prop('sevenRatings')).to.be.true();
  });
});
