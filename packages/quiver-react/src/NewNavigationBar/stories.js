import React from 'react';
import NewNavigationBar from '.';

export default {
  title: 'NewNavigationBar',
};

export const NotLoggedIn = () => {
  const serviceConfig = {
    serviceUrl: 'http://services.sandbox.surfline.com',
  };
  const settingsStub = { units: { surfHeight: 'FT' } };
  const entitlements = ['sl_premium'];
  return (
    <NewNavigationBar
      wwwHost="https://new.staging.surfline.com"
      entitlements={entitlements}
      serviceConfig={serviceConfig}
      favorites={[]}
      settings={settingsStub}
    />
  );
};

export const LoggedInNotPremium = () => {
  const serviceConfig = {
    serviceUrl: 'http://services.sandbox.surfline.com',
  };
  const settingsStub = {
    units: { surfHeight: 'FT' },
    recentlyVisited: {
      spots: [
        { _id: '5842041f4e65fad6a77088f2', name: 'Newport Point' },
        { _id: '58bdebbc82d034001252e3d2', name: 'Huntington St. Overview' },
        { _id: '584204204e65fad6a770998c', name: 'Huntington State Beach' },
      ],
      subregions: [
        { _id: '58581a836630e24c44878fd6', name: 'North Orange County' },
        { _id: '58581a836630e24c4487900a', name: 'South Orange County' },
      ],
    },
  };
  const favorites = [
    {
      _id: 'abc123',
      name: 'Pipeline',
      conditions: { human: false, value: 'EPIC' },
      waveHeight: { min: 0, max: 0, occasional: 18 },
    },
  ];
  const entitlements = [];
  const userStub = {
    email: 'mmalchak@surfline.com',
    _id: '58ff79ecb016012247d2cfb1',
    firstName: 'Averylongname',
    lastName: 'SurflineUser',
    isEmailVerified: true,
  };
  return (
    <div style={{ backgroundColor: 'gray', width: '100%', height: '2000px' }}>
      <NewNavigationBar
        wwwHost="https://new.staging.surfline.com"
        user={userStub}
        entitlements={entitlements}
        serviceConfig={serviceConfig}
        favorites={favorites}
        settings={settingsStub}
      />
    </div>
  );
};

LoggedInNotPremium.story = {
  name: 'Logged In - Not Premium',
};

export const LoggedInPremium = () => {
  const serviceConfig = {
    serviceUrl: 'http://services.sandbox.surfline.com',
  };
  const settingsStub = {
    units: { surfHeight: 'FT' },
    recentlyVisited: {
      spots: [
        { _id: '5842041f4e65fad6a77088f2', name: 'Newport Point' },
        { _id: '5842041f4e65fad6a77088ed', name: 'HB Pier, Southside' },
        { _id: '584204204e65fad6a770998c', name: 'Huntington State Beach' },
      ],
      subregions: [
        { _id: '58581a836630e24c44878fd6', name: 'North Orange County' },
        { _id: '58581a836630e24c4487900a', name: 'South Orange County' },
      ],
    },
  };
  const favorites = [
    {
      _id: 'abc123',
      name: 'Pipeline',
      conditions: { human: false, value: 'EPIC' },
      waveHeight: { min: 0, max: 0, occasional: 18 },
    },
  ];
  const entitlements = ['sl_premium'];
  const userStub = {
    email: 'mmalchak@surfline.com',
    _id: '58ff79ecb016012247d2cfb1',
    firstName: 'Averylongname',
    lastName: 'SurflineUser',
    isEmailVerified: false,
  };
  const accountAlert = true;
  return (
    <div style={{ backgroundColor: 'gray', width: '100%', height: '2000px' }}>
      <NewNavigationBar
        wwwHost="https://new.staging.surfline.com"
        user={userStub}
        entitlements={entitlements}
        serviceConfig={serviceConfig}
        favorites={favorites}
        settings={settingsStub}
        accountAlert={accountAlert}
      />
    </div>
  );
};

LoggedInPremium.story = {
  name: 'Logged In - Premium',
};

export const LoggedInPremiumConditionsV2 = () => {
  const serviceConfig = {
    serviceUrl: 'http://services.sandbox.surfline.com',
  };
  const settingsStub = {
    units: { surfHeight: 'FT' },
    recentlyVisited: {
      spots: [
        { _id: '5842041f4e65fad6a77088f2', name: 'Newport Point' },
        { _id: '5842041f4e65fad6a77088ed', name: 'HB Pier, Southside' },
        { _id: '584204204e65fad6a770998c', name: 'Huntington State Beach' },
      ],
      subregions: [
        { _id: '58581a836630e24c44878fd6', name: 'North Orange County' },
        { _id: '58581a836630e24c4487900a', name: 'South Orange County' },
      ],
    },
  };
  const favorites = [
    {
      _id: 'abc123',
      name: 'Pipeline',
      conditions: { human: false, value: 'EPIC' },
      waveHeight: { min: 0, max: 0, occasional: 18 },
    },
  ];
  const entitlements = ['sl_premium'];
  const userStub = {
    email: 'mmalchak@surfline.com',
    _id: '58ff79ecb016012247d2cfb1',
    firstName: 'Averylongname',
    lastName: 'SurflineUser',
    isEmailVerified: false,
  };
  const accountAlert = true;
  return (
    <div style={{ backgroundColor: 'gray', width: '100%', height: '2000px' }}>
      <NewNavigationBar
        wwwHost="https://new.staging.surfline.com"
        user={userStub}
        entitlements={entitlements}
        serviceConfig={serviceConfig}
        favorites={favorites}
        settings={settingsStub}
        accountAlert={accountAlert}
        sevenRatings
      />
    </div>
  );
};

LoggedInPremiumConditionsV2.story = {
  name: 'Logged In - Premium with new condition colors',
};

export const NotLoggedInWithModalSiteSearch = () => {
  const serviceConfig = {
    serviceUrl: 'http://services.sandbox.surfline.com',
  };
  const settingsStub = { units: { surfHeight: 'FT' } };
  const entitlements = ['sl_premium'];
  return (
    <NewNavigationBar
      wwwHost="https://new.staging.surfline.com"
      entitlements={entitlements}
      serviceConfig={serviceConfig}
      favorites={[]}
      settings={settingsStub}
      slNewsSearch
    />
  );
};

NotLoggedInWithModalSiteSearch.story = {
  name: 'Not Logged In with Modal Site Search',
};

export const LoggedInPremiumWithModalSiteSearch = () => {
  const serviceConfig = {
    serviceUrl: 'http://services.sandbox.surfline.com',
  };
  const settingsStub = {
    units: { surfHeight: 'FT' },
    recentlyVisited: {
      spots: [
        { _id: '5842041f4e65fad6a77088f2', name: 'Newport Point' },
        { _id: '5842041f4e65fad6a77088ed', name: 'HB Pier, Southside' },
        { _id: '584204204e65fad6a770998c', name: 'Huntington State Beach' },
      ],
      subregions: [
        { _id: '58581a836630e24c44878fd6', name: 'North Orange County' },
        { _id: '58581a836630e24c4487900a', name: 'South Orange County' },
      ],
    },
  };
  const favorites = [
    {
      _id: 'abc123',
      name: 'Pipeline',
      conditions: { human: false, value: 'EPIC' },
      waveHeight: { min: 0, max: 0, occasional: 18 },
    },
  ];
  const entitlements = ['sl_premium'];
  const userStub = {
    email: 'mmalchak@surfline.com',
    _id: '58ff79ecb016012247d2cfb1',
    firstName: 'Averylongname',
    lastName: 'SurflineUser',
  };
  return (
    <div style={{ backgroundColor: 'gray', width: '100%', height: '2000px' }}>
      <NewNavigationBar
        wwwHost="https://new.staging.surfline.com"
        user={userStub}
        entitlements={entitlements}
        serviceConfig={serviceConfig}
        favorites={favorites}
        settings={settingsStub}
        slNewsSearch
      />
    </div>
  );
};

LoggedInPremiumWithModalSiteSearch.story = {
  name: 'Logged In - Premium with Modal Site Search',
};

export const LoggedInAccountAlert = () => {
  const serviceConfig = {
    serviceUrl: 'http://services.sandbox.surfline.com',
  };
  const settingsStub = {
    units: { surfHeight: 'FT' },
    recentlyVisited: {
      spots: [],
      subregions: [],
    },
  };
  const favorites = [];
  const entitlements = [];
  const userStub = {
    email: 'mmalchak@surfline.com',
    _id: '58ff79ecb016012247d2cfb1',
    firstName: 'Averylongname',
    lastName: 'SurflineUser',
    isEmailVerified: false,
  };
  const accountAlert = true;
  const accountWarning = false;
  return (
    <div style={{ backgroundColor: 'gray', width: '100%', height: '2000px' }}>
      <NewNavigationBar
        wwwHost="https://new.staging.surfline.com"
        user={userStub}
        entitlements={entitlements}
        serviceConfig={serviceConfig}
        favorites={favorites}
        settings={settingsStub}
        accountAlert={accountAlert}
        accountWarning={accountWarning}
      />
    </div>
  );
};

LoggedInAccountAlert.story = {
  name: 'Logged In - Account Alert',
};

export const LoggedInPremiumAccountWarning = () => {
  const serviceConfig = {
    serviceUrl: 'http://services.sandbox.surfline.com',
  };
  const settingsStub = {
    units: { surfHeight: 'FT' },
    recentlyVisited: {
      spots: [],
      subregions: [],
    },
  };
  const favorites = [];
  const entitlements = ['sl_premium'];
  const userStub = {
    email: 'mmalchak@surfline.com',
    _id: '58ff79ecb016012247d2cfb1',
    firstName: 'Averylongname',
    lastName: 'SurflineUser',
    isEmailVerified: false,
  };
  const accountAlert = true;
  const accountWarning = true;
  return (
    <div style={{ backgroundColor: 'gray', width: '100%', height: '2000px' }}>
      <NewNavigationBar
        wwwHost="https://new.staging.surfline.com"
        user={userStub}
        entitlements={entitlements}
        serviceConfig={serviceConfig}
        favorites={favorites}
        settings={settingsStub}
        accountAlert={accountAlert}
        accountWarning={accountWarning}
      />
    </div>
  );
};

LoggedInPremiumAccountWarning.story = {
  name: 'Logged In - Premium - Account Warning',
};
