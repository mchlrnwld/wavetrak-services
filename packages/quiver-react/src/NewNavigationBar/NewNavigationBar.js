import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { TABLET_LARGE_WIDTH, trackEvent } from '@surfline/web-common';
import SiteSearch from '../SiteSearch';
import SurflineLogo from '../icons/SurflineLogo';
import SurflineWaterDrop from '../icons/SurflineWaterDrop';
import UserAvatar from '../UserAvatar';
import NewsMenu from '../NewsMenu';
import UserMenu from '../UserMenu';
import CamsReportsMenu from '../CamsReportsMenu';
import ForecastsMenu from '../ForecastsMenu';
import FavoritesMenu from '../FavoritesMenu';
import NavigationItem from '../NavigationItem';
import TrackableLink from '../TrackableLink';
import CloseIcon from '../icons/CloseIcon';
import NewSearchIcon from '../icons/NewSearchIcon';
import { userPropType } from './navigationPropTypes';
import favoritesPropType from '../PropTypes/favoritesPropType';
import windowPropType from '../PropTypes/windowPropType';
import settingsPropType from '../PropTypes/settingsPropType';
import { defaultNewsLinks, defaultAccountLinks } from './defaultLinks';
import isPremiumOrVIP from '../helpers/isPremiumOrVip';
import getSignInHref from '../helpers/getSignInHref';
import createAccessibleOnClick, { BTN } from '../utils/createAccessibleOnClick';

const SUBSCRIBE_EVENT_PROPERTIES = { location: 'button top' };

export default class NewNavigationBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      renderMobileMenu: null,
      mobileMenuTitle: null,
      viewport: null,
      mobileBreakpoint: 768,
      camsReportsMenuState: {
        loading: false,
        error: false,
        taxonomy: {
          parents: [],
          activeNode: {},
          children: [],
        },
      },
      forecastsMenuState: {
        loading: false,
        error: false,
        taxonomy: {
          parents: [],
          activeNode: {},
          children: [],
        },
      },
      searchIsOpen: false,
      newsLinks: [],
    };
    this.mobileMenuRef = null;
    this.linkRef = React.createRef();
    this.signInLinkRef = React.createRef();
  }

  componentDidMount() {
    this.setViewport();
    const modifiedNewsLinks = Array.from(defaultNewsLinks);

    modifiedNewsLinks.unshift({
      display: 'Cuervo Challenge',
      href: '/contests/spirit-of-surfing/cuervo-challenge/2021',
    });

    this.setState({
      newsLinks: modifiedNewsLinks,
    });
    window.addEventListener('resize', this.resizeListener);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeListener);
    this.removeMobileMenu();
  }

  setViewport = () => {
    this.setState((prevState) => ({
      viewport:
        window.innerWidth > prevState.mobileBreakpoint ? 'desktop' : 'mobile',
    }));
  };

  setMenuState = (menuType) => (stateUpdate) => {
    this.setState((prevState) => ({
      [menuType]: {
        ...prevState[menuType],
        ...stateUpdate,
      },
    }));
  };

  setMobileMenu = (renderComponent, title) => {
    const { viewport } = this.state;
    if (viewport === 'mobile') {
      this.setState({
        renderMobileMenu: renderComponent,
        mobileMenuTitle: title,
      });
      document.body.style.position = 'fixed';
      document.documentElement.style.overflow = 'hidden';
    }
  };

  getSiteSearchButtonClasses = (cta) =>
    classnames({
      'quiver-new-navigation-bar__site-search__button': true,
      'quiver-new-navigation-bar__site-search__button--ctaVisible': cta,
    });

  removeMobileMenu = () => {
    this.setState({
      renderMobileMenu: null,
      searchIsOpen: false,
    });
    document.body.style.position = 'relative';
    document.documentElement.style.overflow = 'visible';
  };

  resizeListener = () => {
    if (this.resizeTimeout) return;
    this.resizeTimeout = setTimeout(() => {
      this.resizeTimeout = null;
      this.updateScrollLock();
    }, 66);

    const { viewport, mobileBreakpoint } = this.state;
    if (viewport === 'mobile' && window.innerWidth > mobileBreakpoint) {
      this.setState({ viewport: 'desktop' });
    } else if (
      viewport === 'desktop' &&
      window.innerWidth <= mobileBreakpoint
    ) {
      this.setState({ viewport: 'mobile' });
    }
  };

  scrollMenuToTop = () => {
    if (this.mobileMenuRef) {
      this.mobileMenuRef.scrollTop = 0;
    }
  };

  updateScrollLock = () => {
    const { renderMobileMenu } = this.state;
    if (renderMobileMenu && window.innerWidth > 768) {
      this.removeMobileMenu();
    }
  };

  handleClickedLink = (clickProperties, closeMobileMenu) => {
    if (clickProperties) {
      const { name, properties } = clickProperties;
      trackEvent(name, properties);
    }
    if (closeMobileMenu) {
      this.removeMobileMenu();
    }
  };

  handleCloseSearch = () => this.setState({ searchIsOpen: false });

  signInUrl = () => {
    const { window } = this.props;
    return getSignInHref(window ? window.location.href : null);
  };

  renderCamsReportsMenu = () => {
    const { camsReportsMenuState } = this.state;
    const { user, settings, serviceConfig, sevenRatings } = this.props;
    return (
      <CamsReportsMenu
        loggedIn={!!user}
        navigationSettings={settings && settings.navigation}
        recentlyVisited={
          settings && settings.recentlyVisited && settings.recentlyVisited.spots
        }
        serviceConfig={serviceConfig}
        menuState={camsReportsMenuState}
        setMenuState={this.setMenuState('camsReportsMenuState')}
        scrollMenuToTop={this.scrollMenuToTop}
        signInUrl={this.signInUrl()}
        onClickLink={this.handleClickedLink}
        sevenRatings={sevenRatings}
      />
    );
  };

  renderForecastsMenu = () => {
    const { forecastsMenuState } = this.state;
    const { user, settings, serviceConfig } = this.props;
    return (
      <ForecastsMenu
        loggedIn={!!user}
        navigationSettings={settings && settings.navigation}
        recentlyVisited={
          settings &&
          settings.recentlyVisited &&
          settings.recentlyVisited.subregions
        }
        serviceConfig={serviceConfig}
        menuState={forecastsMenuState}
        setMenuState={this.setMenuState('forecastsMenuState')}
        scrollMenuToTop={this.scrollMenuToTop}
        signInUrl={this.signInUrl()}
        onClickLink={this.handleClickedLink}
      />
    );
  };

  renderFavoritesMenu = () => {
    const { favorites, settings, user } = this.props;
    return (
      <FavoritesMenu
        favorites={favorites}
        settings={settings}
        user={user}
        onClickLink={(properties) => this.handleClickedLink(properties, true)}
      />
    );
  };

  renderUserMenu = () => {
    const {
      user,
      entitlements,
      serviceConfig,
      freeTrialEligible,
      accountWarning,
    } = this.props;
    return (
      <UserMenu
        user={user}
        links={defaultAccountLinks}
        entitlements={entitlements}
        freeTrialEligible={freeTrialEligible}
        accountWarning={accountWarning}
        serviceConfig={serviceConfig}
        showDetails
        onClickLink={this.handleClickedLink}
      />
    );
  };

  renderNewsMenu = () => {
    const { newsLinks } = this.state;
    return <NewsMenu links={newsLinks} onClickLink={this.handleClickedLink} />;
  };

  getSignInEventProperties = () => ({
    pageName: window.location.pathname,
    tabName: 'Sign In',
    linkName: 'Sign In',
    platform: 'web',
    isMobileView: window.innerWidth <= TABLET_LARGE_WIDTH,
  });

  render() {
    const {
      renderMobileMenu,
      mobileMenuTitle,
      searchIsOpen,
      viewport,
      newsLinks,
    } = this.state;
    const {
      user,
      entitlements,
      accountAlert,
      accountWarning,
      serviceConfig,
      hideCTA,
      window,
    } = this.props;
    const signInUrl = getSignInHref(window ? window.location.href : null);
    const userAvatar = user ? (
      <UserAvatar
        entitlements={entitlements}
        accountAlert={accountAlert}
        accountWarning={accountWarning}
      />
    ) : null;

    return (
      <header className="quiver-new-navigation-bar">
        <div className="quiver-new-navigation-bar__content">
          <div className="quiver-new-navigation-bar__logo-desktop">
            <a href="/">
              <SurflineLogo />
            </a>
          </div>
          <div className="quiver-new-navigation-bar__logo-mobile">
            <a href="/">
              <SurflineWaterDrop />
            </a>
          </div>
          <div className="quiver-new-navigation-bar__navigation">
            <NavigationItem linkDisplay="Cams & Reports">
              <div className="quiver-new-navigation-bar__menu-wrapper">
                {this.renderCamsReportsMenu()}
              </div>
            </NavigationItem>
            <NavigationItem linkDisplay="Forecasts">
              <div className="quiver-new-navigation-bar__menu-wrapper">
                {this.renderForecastsMenu()}
              </div>
            </NavigationItem>
            <NavigationItem linkDisplay="News" href="/surf-news">
              <div className="quiver-new-navigation-bar__menu-wrapper">
                <NewsMenu
                  links={newsLinks}
                  onClickLink={this.handleClickedLink}
                />
              </div>
            </NavigationItem>
          </div>
          <div className="quiver-new-navigation-bar__site-search">
            <div
              role="button"
              {...createAccessibleOnClick(
                () => this.setState({ searchIsOpen: true }),
                BTN
              )}
              className={this.getSiteSearchButtonClasses(
                !hideCTA && (!user || !isPremiumOrVIP(entitlements))
              )}
            >
              <NewSearchIcon />
              <span>Search</span>
            </div>
            <SiteSearch
              handleCloseSearch={this.handleCloseSearch}
              serviceUrl={serviceConfig.serviceUrl}
              searchIsOpen={searchIsOpen}
              viewport={viewport}
            />
          </div>
          <div className="quiver-new-navigation-bar__cta">
            {!hideCTA && (!user || !isPremiumOrVIP(entitlements)) ? (
              <TrackableLink
                ref={this.linkRef}
                eventName="Clicked Subscribe CTA"
                eventProperties={SUBSCRIBE_EVENT_PROPERTIES}
              >
                <a
                  ref={this.linkRef}
                  className="quiver-new-navigation-bar__cta__link"
                  href="/upgrade"
                >
                  Start Free Trial
                </a>
              </TrackableLink>
            ) : null}
          </div>
          {user ? (
            <div className="quiver-new-navigation-bar__account">
              <NavigationItem
                linkDisplay={userAvatar}
                role="button"
                onClick={() =>
                  this.setMobileMenu(this.renderUserMenu, 'Account')
                }
                tabIndex="0"
              >
                <div className="quiver-new-navigation-bar__menu-wrapper">
                  {this.renderUserMenu()}
                </div>
              </NavigationItem>
            </div>
          ) : (
            <TrackableLink
              eventName="Clicked Main Nav"
              eventProperties={this.getSignInEventProperties}
              ref={this.signInLinkRef}
            >
              <a
                ref={this.signInLinkRef}
                href={signInUrl}
                className="quiver-new-navigation-bar__sign-in"
              >
                Sign In
              </a>
            </TrackableLink>
          )}
        </div>
        <div className="quiver-new-navigation-bar__mobile">
          <div className="quiver-new-navigation-bar__mobile__content">
            <div className="quiver-new-navigation-bar__mobile__container">
              <div className="quiver-new-navigation-bar__mobile__nav">
                {renderMobileMenu ? (
                  <div
                    ref={(el) => {
                      this.mobileMenuRef = el;
                    }}
                    className="quiver-new-navigation-bar__mobile__nav__menu"
                  >
                    <div
                      data-scroll-lock-scrollable
                      className="quiver-new-navigation-bar__mobile__nav__menu__inner"
                    >
                      <a
                        className="quiver-new-navigation-bar__mobile__nav__menu__close"
                        role="button"
                        tabIndex="0"
                        {...createAccessibleOnClick(
                          () =>
                            this.handleClickedLink(
                              {
                                name: 'Clicked Main Nav',
                                properties: {
                                  completion: false,
                                  linkName: 'close',
                                  tabName: mobileMenuTitle,
                                },
                              },
                              true
                            ),
                          BTN
                        )}
                      >
                        <span />
                        <span className="quiver-new-navigation-bar__mobile__nav__menu__close__title">
                          {mobileMenuTitle}
                        </span>
                        <CloseIcon />
                      </a>
                      {renderMobileMenu()}
                    </div>
                  </div>
                ) : null}
                <NavigationItem
                  linkDisplay="Cams"
                  role="button"
                  tabIndex="0"
                  onClick={() =>
                    this.setMobileMenu(this.renderCamsReportsMenu, 'Cams')
                  }
                />
                <NavigationItem
                  linkDisplay="Forecasts"
                  role="button"
                  tabIndex="0"
                  onClick={() =>
                    this.setMobileMenu(this.renderForecastsMenu, 'Forecasts')
                  }
                />
                <NavigationItem
                  linkDisplay="Favorites"
                  role="button"
                  tabIndex="0"
                  onClick={() =>
                    this.setMobileMenu(this.renderFavoritesMenu, 'Favorites')
                  }
                />
                <NavigationItem
                  linkDisplay="News"
                  role="button"
                  tabIndex="0"
                  onClick={() =>
                    this.setMobileMenu(this.renderNewsMenu, 'News')
                  }
                />
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

NewNavigationBar.propTypes = {
  user: PropTypes.shape(userPropType),
  favorites: favoritesPropType,
  settings: settingsPropType,
  large: PropTypes.bool,
  entitlements: PropTypes.arrayOf(PropTypes.string),
  freeTrialEligible: PropTypes.bool,
  style: PropTypes.shape(),
  badge: PropTypes.node,
  smallNotification: PropTypes.node,
  serviceConfig: PropTypes.shape({
    serviceUrl: PropTypes.string,
  }).isRequired,
  window: windowPropType,
  hideCTA: PropTypes.bool,
  accountAlert: PropTypes.bool,
  accountWarning: PropTypes.bool,
  sevenRatings: PropTypes.bool,
};

NewNavigationBar.defaultProps = {
  user: null,
  large: false,
  badge: null,
  style: {},
  entitlements: [],
  freeTrialEligible: false,
  smallNotification: null,
  favorites: [],
  settings: {},
  hideCTA: false,
  accountAlert: false,
  accountWarning: false,
  window: null,
  sevenRatings: false,
};
