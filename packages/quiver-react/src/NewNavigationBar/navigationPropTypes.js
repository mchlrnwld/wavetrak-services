import PropTypes from 'prop-types';

export const navigationLinkPropType = PropTypes.shape({
  display: PropTypes.string,
  href: PropTypes.string,
});

export const userPropType = {
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  email: PropTypes.string,
  _id: PropTypes.string,
};
