import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import NewRelicBrowser from './NewRelicBrowser';

describe('NewRelicBrowser', () => {
  it('should render the Newrelic script into DOM', () => {
    const wrapper = shallow(
      <NewRelicBrowser licenseKey="test_key" applicationID="test_id" />
    );
    return expect(wrapper.find('script')).to.not.be.empty;
  });
});
