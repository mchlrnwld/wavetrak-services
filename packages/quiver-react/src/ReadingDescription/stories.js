import React from 'react';
import ReadingDescription from './ReadingDescription';

export default {
  title: 'ReadingDescription',
};

export const Default = () => (
  <ReadingDescription>
    Some child component with a description
  </ReadingDescription>
);
