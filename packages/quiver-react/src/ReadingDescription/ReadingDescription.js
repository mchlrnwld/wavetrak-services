import PropTypes from 'prop-types';
import React from 'react';

const ReadingDescription = ({ children }) => (
  <span className="quiver-reading-description">{children}</span>
);

ReadingDescription.propTypes = {
  children: PropTypes.node,
};

ReadingDescription.defaultProps = {
  children: null,
};

export default ReadingDescription;
