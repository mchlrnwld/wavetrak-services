import React from 'react';
import SurflineFooter from './index';

export default {
  title: 'SurflineFooter',
};

export const Empty = () => {
  window.consentManager = {
    openConsentManager: () => true,
  };
  return <SurflineFooter />;
};

Empty.story = {
  name: 'empty',
};

export const Premium = () => {
  window.consentManager = {
    openConsentManager: () => true,
  };
  return <SurflineFooter entitlements={['sl_premium']} />;
};

Premium.story = {
  name: 'premium',
};

export const Small = () => {
  window.consentManager = {
    openConsentManager: () => true,
  };
  return <SurflineFooter entitlements={[]} small />;
};

Small.story = {
  name: 'small',
};
