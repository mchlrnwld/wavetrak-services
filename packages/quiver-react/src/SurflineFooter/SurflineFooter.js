import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { canUseDOM, trackEvent } from '@surfline/web-common';

import classnames from 'classnames';
import SurflineLogo from '../icons/SurflineLogo';
import SocialLogos from './SocialLogos';
import ExternalLink from './ExternalLink';
import entitlementsPropType from '../PropTypes/entitlementsPropType';

export default class SurflineFooter extends Component {
  getFooterClassNames = (small) =>
    classnames({
      'quiver-page-container__footer': true,
      'quiver-page-container__footer--small': small,
    });

  isPremium = (entitlements) => entitlements.indexOf('sl_premium') > -1;

  handleConsentManagerButtonClick = () =>
    trackEvent('Clicked Link', {
      linkLocation: 'footer',
      linkName: 'Do Not Sell My Personal Information',
    });

  render() {
    const { entitlements, small } = this.props;

    return (
      <footer className={this.getFooterClassNames(small)}>
        <ul className="quiver-page-container__footer__icons">
          <li className="quiver-page-container__footer__icons__logo">
            <SurflineLogo />
          </li>
          <li>
            <SocialLogos />
          </li>
        </ul>
        <ul className="quiver-page-container__footer__link-group">
          <li>Company</li>
          <li>
            <ExternalLink
              href="http://careers.surfline.com/who-we-are/"
              text="About Surfline"
              location="footer"
            />
          </li>
          <li>
            <ExternalLink
              href="http://careers.surfline.com/join-our-team/"
              text="Careers"
              location="footer"
            />
          </li>
          <li>
            <ExternalLink
              href="https://discover.surfline.com/ads/"
              text="Advertise"
              location="footer"
            />
          </li>
        </ul>
        <ul className="quiver-page-container__footer__link-group">
          <li>Support</li>
          <li>
            <ExternalLink
              href="//support.surfline.com/"
              text="Support Center"
              location="footer"
            />
          </li>
          <li>
            <a href="/account/">My Account</a>
          </li>
          {!this.isPremium(entitlements) ? (
            <li>
              <ExternalLink
                href="https://surfline.com/upgrade"
                text="Get Premium"
                location="footer"
              />
            </li>
          ) : null}
          <li>
            <ExternalLink
              href="https://surfline.com/gift-cards"
              text="Gift Cards"
              location="footer"
            />
          </li>
        </ul>
        <ul className="quiver-page-container__footer__copyright">
          <li className="quiver-page-container__footer__copyright__logo">
            <SurflineLogo />
          </li>
          <li>{`©${new Date().getFullYear()} Surfline\\Wavetrak, Inc.`}</li>
          <li>
            <ExternalLink
              href="https://www.surfline.com/terms-of-use"
              text="Terms of Use"
              location="footer"
            />{' '}
            and{' '}
            <ExternalLink
              href="https://www.surfline.com/privacy-policy"
              text="Privacy Policy"
              location="footer"
            />
            .
          </li>
          <li>
            <ExternalLink
              href="https://www.surfline.com/privacy-notice-california-residents"
              text="California Privacy Policy"
              location="footer"
            />
          </li>
          <li>
            <button
              type="button"
              onClick={() =>
                canUseDOM &&
                this.handleConsentManagerButtonClick() &&
                window.consentManager.openConsentManager()
              }
            >
              Do Not Sell My Personal Information
            </button>
          </li>
          <li>Partner of USATODAY Lifestyle/Action Sports</li>
        </ul>
      </footer>
    );
  }
}

SurflineFooter.propTypes = {
  entitlements: entitlementsPropType,
  small: PropTypes.bool,
};

SurflineFooter.defaultProps = {
  entitlements: [],
  small: false,
};
