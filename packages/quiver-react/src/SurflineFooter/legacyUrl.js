const legacyUrl = {
  sandbox: 'https://sandbox.surfline.com',
  staging: 'https://staging.surfline.com',
  production: 'https://www.surfline.com',
};

export default legacyUrl[process.env.NODE_ENV || 'sandbox'];
