import React from 'react';
import FacebookIcon from '../../icons/FacebookIcon';
import InstagramIcon from '../../icons/InstagramIcon';
import TwitterIcon from '../../icons/TwitterIcon';
import YouTubeIcon from '../../icons/YouTubeIcon';

const SocialLogos = () => (
  <div className="socialLogos">
    <a
      href="https://www.facebook.com/Surfline/"
      target="_blank"
      rel="noopener noreferrer"
    >
      <div>
        <FacebookIcon />
      </div>
    </a>
    <a
      href="https://www.instagram.com/surfline/"
      target="_blank"
      rel="noopener noreferrer"
    >
      <div>
        <InstagramIcon />
      </div>
    </a>
    <a
      href="https://www.youtube.com/surfline"
      target="_blank"
      rel="noopener noreferrer"
    >
      <div>
        <YouTubeIcon />
      </div>
    </a>
    <a
      href="https://twitter.com/surfline"
      target="_blank"
      rel="noopener noreferrer"
    >
      <div>
        <TwitterIcon />
      </div>
    </a>
  </div>
);

export default SocialLogos;
