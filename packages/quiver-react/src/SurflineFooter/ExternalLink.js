import React from 'react';
import PropTypes from 'prop-types';
import { trackEvent } from '@surfline/web-common';

const handleClick = ({ href, text, location }) =>
  trackEvent('Clicked Link', {
    linkLocation: location,
    linkUrl: href,
    linkName: text,
  });

const ExternalLink = ({ href, text, location }) => (
  <a
    target="_blank"
    rel="noopener noreferrer"
    href={href}
    onClick={() => handleClick({ href, text, location })}
  >
    {text}
  </a>
);

ExternalLink.propTypes = {
  href: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  location: PropTypes.string,
};

ExternalLink.defaultProps = {
  location: null,
};

export default ExternalLink;
