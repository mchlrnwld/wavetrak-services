import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SurflineFooter from './SurflineFooter';

describe('SurflineFooter', () => {
  it('should render footer', () => {
    const wrapper = shallow(<SurflineFooter />);
    expect(wrapper.html()).to.contain('quiver-page-container__footer');
  });
});
