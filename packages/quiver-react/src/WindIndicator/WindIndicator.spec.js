import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import WindIndicator from './WindIndicator';

describe('WindIndicator', () => {
  const wrapper = mount(
    <WindIndicator direction={90} speed={12.51} units="KT" />
  );

  it('should render an svg element', () => {
    expect(wrapper.html()).to.contain('<svg class="quiver-wind-indicator">');
  });

  it('should rotate relative to the direction prop', () => {
    const { direction } = wrapper.props();
    const transform = `transform="rotate(${direction} 17.5 17.5)"`;
    expect(wrapper.html()).to.contain(transform);
  });

  it('should display a round number relative to the speed prop', () => {
    const text = '<text x="50%" y="51%">13</text>';
    expect(wrapper.html()).to.contain(text);
  });
});
