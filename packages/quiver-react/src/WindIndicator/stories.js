import React from 'react';
import WindIndicator from './index';

export default {
  title: 'WindIndicator',
};

export const Default = () => (
  <div style={{ width: '35px', height: '35px', background: '#7795c6' }}>
    <WindIndicator direction={90.5} speed={12.3} units="KT" />
  </div>
);

Default.story = {
  name: 'default',
};
