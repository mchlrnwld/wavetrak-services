import React from 'react';
import PropTypes from 'prop-types';

const WindIndicator = ({ direction, speed, units }) => {
  const adjustedSpeed = Math.round(speed);
  return (
    <svg className="quiver-wind-indicator">
      <g>
        <g transform={`rotate(${direction} 17.5 17.5)`}>
          <circle cx="17.5" cy="17.5" r="16" />
          <polygon points="14.5 2.75, 17.5 2.5, 20.5 2.75, 17.5 6" />
        </g>
        <g>
          <text x="50%" y="51%">
            {adjustedSpeed}
          </text>
          <text x="50%" y="70%">
            {units}
          </text>
        </g>
      </g>
    </svg>
  );
};

WindIndicator.propTypes = {
  direction: PropTypes.number,
  speed: PropTypes.number,
  units: PropTypes.string,
};

WindIndicator.defaultProps = {
  direction: 0,
  speed: 0,
  units: '',
};

export default WindIndicator;
