import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon from 'sinon';
import SiteSearch from './SiteSearch';

describe('SiteSearch', () => {
  const { location } = window;

  before(() => {
    delete window.location;
    window.location = {
      assign: sinon.stub(),
    };
  });
  after(() => {
    window.location = location;
  });

  const props = {
    handleCloseSearch: () => {},
    searchIsOpen: true,
    serviceUrl: 'https://www.surfline.com',
  };

  it('should not render on server init', () => {
    const wrapper = mount(<SiteSearch {...props} />);
    wrapper.setState({ initialRender: true });
    wrapper.update();
    const content = wrapper.find('div.quiver-site-search');
    expect(content).to.have.length(0);
    expect(wrapper.state('initialRender')).to.be.true();
    wrapper.instance().debouncedInputChange.flush();
  });

  it('should not render selection message if no selection', () => {
    const wrapper = mount(<SiteSearch {...props} />);
    const message = wrapper.find('.quiver-site-search__selection-message');
    expect(message).to.have.length(0);
    wrapper.instance().debouncedInputChange.flush();
  });

  it('should set initialRender to false on mount', async () => {
    const wrapper = mount(<SiteSearch {...props} />);
    const siteSearch = wrapper.find('div.quiver-site-search');
    expect(siteSearch).to.have.length(1);
    expect(wrapper.state('initialRender')).to.be.false();
    wrapper.instance().debouncedInputChange.flush();
  });
});
