const getSearchResults = async (
  serviceUrl,
  value,
  searchId,
  size = 10,
  newsSearch = true
) => {
  const searchUrl = (namePrefix) => {
    let baseUrl =
      `${serviceUrl}/search/site` +
      `?q=${namePrefix}&querySize=${size}&suggestionSize=${size}`;
    if (newsSearch) baseUrl += '&newsSearch=true';
    return baseUrl;
  };
  const response = await fetch(searchUrl(value), {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  });

  const body = await response.json();
  if (response.status === 200) {
    return body;
  }
  return [];
};

export default getSearchResults;
