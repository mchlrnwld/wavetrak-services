import PropTypes from 'prop-types';

const resultPropType = PropTypes.shape({
  _source: PropTypes.shape({
    content: PropTypes.shape({
      href: PropTypes.string,
      title: PropTypes.string,
    }),
    _index: PropTypes.string,
    _type: PropTypes.string,
  }),
});

export default resultPropType;
