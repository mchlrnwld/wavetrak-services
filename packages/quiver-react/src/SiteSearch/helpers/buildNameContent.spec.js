import { expect } from 'chai';
import { shallow } from 'enzyme';
import buildNameContent from './buildNameContent';

describe('SiteSearch', () => {
  const searchTerm = 'a';
  describe('buildNameContent', () => {
    it('should return no highlight for news suggestion', async () => {
      const content = { title: 'Article Title' };
      const wrapper = shallow(
        buildNameContent(content, null, true, true, searchTerm)
      );
      const highlight = wrapper.find('highlight');
      expect(highlight).to.have.length(0);
    });

    it('should return highlight for non-news suggestions', async () => {
      const name = 'Article Title';
      const wrapper = shallow(
        buildNameContent(null, name, false, false, searchTerm)
      );
      const highlight = wrapper.find('highlight');
      expect(highlight).to.have.length(1);
    });
  });
});
