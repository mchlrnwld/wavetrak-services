import React from 'react';
import CamIcon from '../../icons/CamIcon';
import ForecastIcon from '../../icons/ForecastIcon';
import LocationIcon from '../../icons/LocationIcon';
import NewsIcon from '../../icons/NewsIcon';
import TravelIcon from '../../icons/TravelIcon';

const buildIconContent = (cams, type, newsSuggestion) => {
  let iconContent;
  if (cams && cams.length && cams[0] !== null) {
    iconContent = <CamIcon isMultiCam={cams && cams.length > 1} />;
  } else if (type === 'spot' || type === 'subregion') {
    iconContent = <ForecastIcon />;
  } else if (type === 'travel') {
    iconContent = <TravelIcon />;
  } else if (newsSuggestion) {
    iconContent = <NewsIcon />;
  } else {
    iconContent = <LocationIcon />;
  }
  return iconContent;
};

export default buildIconContent;
