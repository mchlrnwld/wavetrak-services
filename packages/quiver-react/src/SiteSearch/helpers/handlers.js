import { canUseDOM } from '@surfline/web-common';
import getSearchResults from './getSearchResults';
import { trackClickedSearchResults, trackQuery } from './analytics';

// clear highlighted item if input changes
export const handleChange = (setHighlightedIndex) => {
  setHighlightedIndex(null);
};

// close search modal on enter key when close icon focused
export const handleCloseKeyDown = (e, handleCloseSearch) => {
  if (e.key === 'Enter') {
    handleCloseSearch();
  }
};

// keep results visible if user moves focus away from input
export const handleInputBlur = (event) => {
  // eslint-disable-next-line no-param-reassign
  event.nativeEvent.preventDownshiftDefault = true;
};

// set input value in state and fetch search results
export const handleInputChange = async (
  value,
  state,
  searchId,
  serviceUrl,
  setState
) => {
  if (!state.selectedItem) {
    const searchResults = await getSearchResults(serviceUrl, value, searchId);
    trackQuery(serviceUrl, value, searchId, searchResults);
    setState({
      searchResults,
      searchTerm: value,
    });
  }
};

export const handleInputKeyDown = (
  event,
  searchTerm,
  highlightedIndex,
  viewAllLink,
  setHighlightedIndex,
  sectionLimits
) => {
  // trigger view all link with enter key
  if (searchTerm && event.key === 'Enter' && highlightedIndex === null) {
    // eslint-disable-next-line no-param-reassign
    event.nativeEvent.preventDownshiftDefault = true;
    if (canUseDOM) window.location.assign(viewAllLink);
  }

  // jump to next result section
  if (searchTerm && event.key === 'ArrowRight' && highlightedIndex !== null) {
    const sections = [];
    sectionLimits.forEach((value) => sections.push(value));
    const currentSection = sections.findIndex(
      (item) => highlightedIndex < item
    );
    setHighlightedIndex(currentSection < 4 ? sections[currentSection] : 0);
  }
};

// navigate to selected item
export const handleSelect = (
  selection,
  state,
  searchTerm,
  searchId,
  serviceUrl,
  setState
) => {
  setState({ selection });
  const { inputValue } = state;
  const { _type, _source, _index, itemSectionIndex, sectionTook } = selection;
  const newsResult = _index === 'feed';
  const resultUrl = newsResult ? _source.permalink : _source.href;
  if (canUseDOM) {
    trackClickedSearchResults(
      window,
      serviceUrl,
      {
        searchId,
        searchResultRank: itemSectionIndex,
        destinationPage: resultUrl,
        queryTerm: searchTerm,
        resultName: inputValue,
        searchLatency: sectionTook,
        resultType: newsResult ? 'news' : _type,
      },
      // eslint-disable-next-line no-underscore-dangle
      () => window.location.assign(resultUrl)
    );
  }
};

// track view all button click
export function handleViewAllClick() {
  if (!this.state) {
    // eslint-disable-next-line no-console
    console.error(
      'No `this.state` found for handleViewAllClick. Maybe you need to bind the function to the react component?'
    );
  }

  const { searchTerm } = this.state;
  if (canUseDOM) {
    trackClickedSearchResults(window, null, {
      searchId: null,
      searchResultRank: null,
      destinationPage: `/search/${searchTerm}`,
      queryTerm: searchTerm,
      resultName: null,
      searchLatency: null,
      resultType: 'see all results',
    });
  }
}
