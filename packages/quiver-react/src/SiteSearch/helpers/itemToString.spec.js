import { expect } from 'chai';
import itemToString from './itemToString';

describe('SiteSearch', () => {
  describe('itemToString', () => {
    it('should return empty string if no item or source', async () => {
      const nullItem = itemToString(null);
      const noSourceItem = itemToString({});
      expect(nullItem).to.equal('');
      expect(noSourceItem).to.equal('');
    });

    it('should return name or content title', async () => {
      const newsResult = { _source: { content: { title: 'Title' } } };
      const spotResult = { _source: { name: 'Name' } };
      const newsItem = itemToString(newsResult);
      const spotItem = itemToString(spotResult);
      /* eslint-disable no-underscore-dangle */
      expect(newsItem).to.equal(newsResult._source.content.title);
      expect(spotItem).to.equal(spotResult._source.name);
      /* eslint-enable no-underscore-dangle */
    });
  });
});
