import { expect } from 'chai';
import sinon from 'sinon';
import {
  handleCloseKeyDown,
  handleInputKeyDown,
  handleSelect,
} from './handlers';

describe('SiteSearch', () => {
  const { location } = window;

  before(() => {
    delete window.location;
    window.location = {
      assign: sinon.stub(),
    };
  });
  after(() => {
    window.location = location;
  });

  describe('handlers', () => {
    describe('handleCloseKeyDown', () => {
      it('should call handle close search on enter key', async () => {
        const enterEvent = { key: 'Enter' };
        const handleCloseSearch = sinon.spy();
        handleCloseKeyDown(enterEvent, handleCloseSearch);
        expect(handleCloseSearch).to.have.been.calledOnce();
      });

      it('should not call handle close search on other key', async () => {
        const enterEvent = { key: 'Esc' };
        const handleCloseSearch = sinon.spy();
        handleCloseKeyDown(enterEvent, handleCloseSearch);
        expect(handleCloseSearch).to.not.have.been.calledOnce();
      });
    });

    describe('handleInputKeyDown', () => {
      beforeEach(() => {
        window.location.assign = sinon.spy();
      });

      it('should update event default on enter key if highlightedIndex is null', async () => {
        const enterEvent = {
          key: 'Enter',
          nativeEvent: { preventDownshiftDefault: false },
        };
        handleInputKeyDown(enterEvent, 'a', null, null, null, null);
        expect(enterEvent.nativeEvent.preventDownshiftDefault).to.equal(true);
        expect(window.location.assign).to.have.been.calledOnce();
      });

      it('should not update event default if highlightedIndex is not null', async () => {
        const enterEvent = {
          key: 'Enter',
          nativeEvent: { preventDownshiftDefault: false },
        };
        handleInputKeyDown(enterEvent, 'a', 1, null, null, null);
        expect(enterEvent.nativeEvent.preventDownshiftDefault).to.equal(false);
        expect(window.location.assign).to.not.have.been.calledOnce();
      });

      it('should setHighlightedIndex on ArrowRight key', async () => {
        const arrowEvent = { key: 'ArrowRight' };
        const sections = [0, 2, 3, 5, 10];
        const setHighlightedIndex = sinon.spy();
        handleInputKeyDown(
          arrowEvent,
          'a',
          1,
          null,
          setHighlightedIndex,
          sections
        );
        expect(setHighlightedIndex).to.have.been.calledOnce();
        expect(setHighlightedIndex).to.have.been.calledWithExactly(2);
      });

      it('should setHighlightedIndex on ArrowRight key to 0 if last section', async () => {
        const arrowEvent = { key: 'ArrowRight' };
        const sections = [0, 2, 3, 5, 10];
        const setHighlightedIndex = sinon.spy();
        handleInputKeyDown(
          arrowEvent,
          'a',
          8,
          null,
          setHighlightedIndex,
          sections
        );
        expect(setHighlightedIndex).to.have.been.calledOnce();
        expect(setHighlightedIndex).to.have.been.calledWithExactly(0);
      });
    });

    describe('handleSelect', () => {
      beforeEach(() => {
        window.location.assign = sinon.spy();
      });
      const setState = sinon.spy();

      it('should set correct resultUrl on handleSelect', async () => {
        const selection = {
          _source: {
            permalink: 'https://www.surfline.com/surf-news/article-title/12345',
          },
          _type: 'editorial',
          _index: 'feed',
        };
        const state = { inputValue: 'a' };
        handleSelect(selection, state, 'a', '12345', null, setState);
        expect(window.location.assign).to.have.been.calledOnce();
        expect(window.location.assign).to.have.been.calledWithExactly(
          // eslint-disable-next-line no-underscore-dangle
          selection._source.permalink
        );
      });

      it('should set correct resultUrl on handleSelect for news suggestion', async () => {
        const selection = {
          _source: {
            href: 'https://www.surfline.com/surf-report/huntington-st-overview/58bdebbc82d034001252e3d2',
          },
          _type: 'spot',
          _index: 'spots',
        };
        const state = { inputValue: 'a' };
        handleSelect(selection, state, 'a', '12345', null, setState);
        expect(window.location.assign).to.have.been.calledOnce();
        expect(window.location.assign).to.have.been.calledWithExactly(
          // eslint-disable-next-line no-underscore-dangle
          selection._source.href
        );
      });
    });
  });
});
