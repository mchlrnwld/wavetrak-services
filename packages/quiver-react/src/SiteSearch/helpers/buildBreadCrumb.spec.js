import { expect } from 'chai';
import buildBreadCrumb from './buildBreadCrumb';

describe('SiteSearch', () => {
  describe('buildBreadCrumb', () => {
    const breadCrumbs = ['United States', 'California', 'Orange County'];
    const tags = [
      { name: 'Travel', href: 'https://www.surfline.com/category/travel' },
      { name: 'Gear', href: 'https://www.surfline.com/categroy/gear' },
    ];

    it('should return a breadCrumb string', async () => {
      const breadcrumb = buildBreadCrumb(breadCrumbs);
      const expectedResult = 'United States / Orange County';
      expect(breadcrumb).to.equal(expectedResult);
    });

    it('should return empty string if no breadCrumbs', async () => {
      const breadcrumb = buildBreadCrumb([]);
      const breadcrumbNull = buildBreadCrumb(null);
      const expectedResult = '';
      expect(breadcrumb).to.equal(expectedResult);
      expect(breadcrumbNull).to.equal(expectedResult);
    });

    it('should return a category string for news suggestion', async () => {
      const breadcrumb = buildBreadCrumb(breadCrumbs, true, tags);
      const expectedResult = 'Travel';
      expect(breadcrumb).to.equal(expectedResult);
    });

    it('should return emptry string for news suggestion with no tags', async () => {
      const breadcrumb = buildBreadCrumb(breadCrumbs, true, []);
      const breadcrumbNull = buildBreadCrumb(breadCrumbs, true, null);
      const expectedResult = '';
      expect(breadcrumb).to.equal(expectedResult);
      expect(breadcrumbNull).to.equal(expectedResult);
    });
  });
});
