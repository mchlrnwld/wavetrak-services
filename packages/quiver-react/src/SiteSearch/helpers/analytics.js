import { trackEvent } from '@surfline/web-common';

/* istanbul ignore next */
export const shipAnalytics = (serviceUrl, log) =>
  fetch(`${serviceUrl}/search/analytics`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ log }),
  });

/* istanbul ignore next */
export const trackClickedSearchResults = (
  window,
  serviceUrl,
  {
    searchId,
    searchResultRank,
    destinationPage,
    queryTerm,
    resultName,
    searchLatency,
    resultType,
  },
  callback = () => {}
) => {
  const body = {
    searchId,
    location: 'navigation',
    searchResultRank,
    destinationPage,
    sourcePage: window && window.location ? window.location.href : null,
    queryTerm,
    resultName,
    searchLatency,
    resultType,
  };
  if (serviceUrl) {
    shipAnalytics(serviceUrl, { event: 'Clicked Search Results', ...body });
    trackEvent('Clicked Search Results', body, null, callback);
  } else {
    callback();
  }
};

/* istanbul ignore next */
export const trackFocusedSearch = (window, serviceUrl, { searchId }) => {
  shipAnalytics(serviceUrl, { event: 'Focused Search', searchId });

  trackEvent('Focused Search', {
    location: 'navigation',
  });
};

/* istanbul ignore next */
export const trackQuery = (serviceUrl, value, searchId, searchResults) => {
  const searchResultHits = searchResults.map((section) => section.hits.hits);
  shipAnalytics(serviceUrl, {
    event: 'Query',
    searchId,
    query: value,
    hits: {
      spots: searchResultHits[0].slice(0, 10).length,
      subregions: searchResultHits[1].slice(0, 3).length,
      geonames: searchResultHits[2].slice(0, 3).length,
      news: searchResultHits[3].slice(0, 10).length,
      travel: searchResultHits[4].slice(0, 3).length,
    },
  });
};
