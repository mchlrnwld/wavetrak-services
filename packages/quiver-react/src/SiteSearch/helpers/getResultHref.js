const getResultHref = (result) => {
  /* eslint-disable no-underscore-dangle */
  if (!result || !result._source) return '';
  return result._source.href || result._source.permalink;
  /* eslint-enable no-underscore-dangle */
};

export default getResultHref;
