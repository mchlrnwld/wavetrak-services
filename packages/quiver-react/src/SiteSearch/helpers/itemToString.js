const itemToString = (item) => {
  /* eslint-disable no-underscore-dangle */
  if (item && item._source) {
    return item._source.name ? item._source.name : item._source.content.title;
  }
  return '';
  /* eslint-enable no-underscore-dangle */
};

export default itemToString;
