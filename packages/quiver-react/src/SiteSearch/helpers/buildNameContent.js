import React from 'react';

const buildNameContent = (
  content,
  name,
  isSuggestion,
  newsSuggestion,
  searchTerm
) => {
  let nameContent;
  const suggestionName = newsSuggestion ? content.title : name;
  const indexOfSearchTerm = suggestionName
    .toLowerCase()
    .indexOf(searchTerm.toLowerCase());

  if (newsSuggestion && isSuggestion) {
    nameContent = <span>{suggestionName}</span>;
  } else if (indexOfSearchTerm > -1) {
    const searchTermLength = searchTerm.length;
    const resultTermLength = suggestionName.length;
    const endOfHighlight = indexOfSearchTerm + searchTermLength;
    nameContent = (
      <span>
        {suggestionName.substring(0, indexOfSearchTerm)}
        <highlight>
          {suggestionName.substring(indexOfSearchTerm, endOfHighlight)}
        </highlight>
        {suggestionName.substring(endOfHighlight, resultTermLength)}
      </span>
    );
  } else {
    nameContent = (
      <span>
        {suggestionName
          .toLowerCase()
          .split('')
          .map((char, index) =>
            searchTerm.toLowerCase().indexOf(char) > -1 &&
            index <= suggestionName.toLowerCase().indexOf(char) ? (
              <highlight>{suggestionName[index]}</highlight>
            ) : (
              suggestionName[index]
            )
          )}
      </span>
    );
  }
  return nameContent;
};

export default buildNameContent;
