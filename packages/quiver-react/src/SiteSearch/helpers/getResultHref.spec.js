import { expect } from 'chai';
import getResultHref from './getResultHref';

describe('SiteSearch', () => {
  describe('getResultHref', () => {
    it('should return empty string if no result or source', async () => {
      const resultHref = getResultHref(null);
      const noSourceResultHref = getResultHref({});
      expect(resultHref).to.equal('');
      expect(noSourceResultHref).to.equal('');
    });

    it('should return href or permalink for result with source', async () => {
      const newsResult = {
        _source: {
          permalink: 'https://www.surfline.com/surf-news/news-article/1',
        },
      };
      const spotResult = {
        _source: { href: 'https://www.surfline.com/surf-news/news-article/2' },
      };
      const newsResultHref = getResultHref(newsResult);
      const spotResultHref = getResultHref(spotResult);
      /* eslint-disable no-underscore-dangle */
      expect(newsResultHref).to.equal(newsResult._source.permalink);
      expect(spotResultHref).to.equal(spotResult._source.href);
      /* eslint-enable no-underscore-dangle */
    });
  });
});
