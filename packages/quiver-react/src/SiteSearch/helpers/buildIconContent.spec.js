import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import buildIconContent from './buildIconContent';

describe('SiteSearch', () => {
  describe('buildIconContent', () => {
    const cams = [{ title: 'Mock Cam Title' }];
    const multiCams = [...cams, { title: 'Another Mock Cam Title' }];

    it('should return CamIcon for spot result with cam', async () => {
      const wrapper = mount(buildIconContent(cams));
      const iconClass = wrapper.find('.quiver-cam-icon');
      expect(iconClass).to.have.length(1);
    });

    it('should return multi cam CamIcon for spot suggestion with cams', async () => {
      const wrapper = mount(buildIconContent(multiCams));
      const iconClass = wrapper.find('.quiver-multi-cam-icon');
      expect(iconClass).to.have.length(1);
    });

    it('should return ForecastIcon for spot suggestion with no cams', async () => {
      const wrapper = shallow(buildIconContent(null, 'spot', false));
      const iconClass = wrapper.find('.quiver-forecast-icon');
      expect(iconClass).to.have.length(1);
    });

    it('should return ForecastIcon for subregion suggestion', async () => {
      const wrapper = shallow(buildIconContent([], 'subregion', false));
      const iconClass = wrapper.find('.quiver-forecast-icon');
      expect(iconClass).to.have.length(1);
    });

    it('should return TravelIcon for travel suggestion', async () => {
      const wrapper = shallow(buildIconContent([], 'travel', false));
      const iconClass = wrapper.find('.quiver-travel-icon');
      expect(iconClass).to.have.length(1);
    });

    it('should return NewsIcon for news suggestion', async () => {
      const wrapper = shallow(buildIconContent([], 'editorial', true));
      const iconClass = wrapper.find('.quiver-news-icon');
      expect(iconClass).to.have.length(1);
    });

    it('should return LocationIcon as default', async () => {
      const wrapper = shallow(buildIconContent([], 'geoname', false));
      const iconClass = wrapper.find('.quiver-location-icon');
      expect(iconClass).to.have.length(1);
    });
  });
});
