import React from 'react';
import PropTypes from 'prop-types';
import SearchResult from './SearchResult';
import { trackClickedSearchResults, resultPropType } from '../helpers';

const handleViewAllClick = (searchTerm, type) =>
  trackClickedSearchResults(window, null, {
    searchId: null,
    searchResultRank: null,
    destinationPage: `/search/${searchTerm}`,
    queryTerm: searchTerm,
    resultName: null,
    searchLatency: null,
    resultType: `see all ${type} results`,
  });

const SearchSection = ({
  getItemProps,
  highlightedIndex,
  result,
  searchTerm,
  sectionLimit,
  sectionTitle,
  sectionTook,
  sectionResults,
  viewAllLink,
}) => (
  <div>
    <h5 className="quiver-site-search__section__title">{sectionTitle}</h5>
    {sectionResults.length ? (
      <ul className="quiver-site-search__section__results">
        {sectionResults.map((item, itemSectionIndex) => {
          // eslint-disable-next-line no-param-reassign, no-plusplus
          const index = result.itemIndex++;
          return (
            <div key={item._id}>
              <SearchResult
                result={item}
                searchTerm={searchTerm}
                itemProps={getItemProps({
                  item: { ...item, itemSectionIndex, sectionTook },
                  index,
                  isActive: highlightedIndex === index,
                })}
              />
            </div>
          );
        })}
      </ul>
    ) : (
      <span className="quiver-site-search__section__no-results">
        No results found
      </span>
    )}
    {sectionResults.length >= sectionLimit ? (
      <a
        className="quiver-site-search__section__view-all"
        href={viewAllLink.href}
        onClick={() => handleViewAllClick(searchTerm, viewAllLink.title)}
      >
        {`View all ${viewAllLink.title}`}
      </a>
    ) : null}
  </div>
);

SearchSection.propTypes = {
  getItemProps: PropTypes.func.isRequired,
  highlightedIndex: PropTypes.number,
  result: resultPropType.isRequired,
  searchTerm: PropTypes.string.isRequired,
  sectionLimit: PropTypes.number.isRequired,
  sectionTitle: PropTypes.string.isRequired,
  sectionTook: PropTypes.number.isRequired,
  sectionResults: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  viewAllLink: PropTypes.shape({
    title: PropTypes.string,
    href: PropTypes.string,
  }).isRequired,
};

SearchSection.defaultProps = {
  highlightedIndex: null,
};

export default SearchSection;
