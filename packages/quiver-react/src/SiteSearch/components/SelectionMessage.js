import React from 'react';
import Spinner from '../../Spinner';
import resultPropType from '../helpers/resultPropType';

const SelectionMessage = ({ selection }) => (
  <div className="quiver-site-search__selection-message">
    <div className="quiver-site-search__selection-message__content">
      <p>Navigating to</p>
      {/* eslint-disable no-underscore-dangle */}
      <h5>
        {selection._index === 'feed'
          ? selection._source.content.title
          : selection._source.name}
      </h5>
      {/* eslint-enable no-underscore-dangle */}
      <Spinner />
    </div>
  </div>
);

SelectionMessage.propTypes = {
  selection: resultPropType.isRequired,
};

export default SelectionMessage;
