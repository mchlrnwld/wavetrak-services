import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {
  buildBreadCrumb,
  buildNameContent,
  buildIconContent,
  getResultHref,
  resultPropType,
} from '../helpers';

const getSearchResultTitleClasses = (index) =>
  classnames({
    'quiver-site-search__result__title': true,
    'quiver-site-search__result__title--news': index === 'feed',
  });

const getSearchResultClasses = (active) =>
  classnames({
    'quiver-site-search__result': true,
    'quiver-site-search__result--active': active,
  });

const SearchResult = ({ result, searchTerm, itemProps }) => {
  const {
    _index,
    isSuggestion,
    _source: { cams, breadCrumbs, name, content, tags },
    _type,
  } = result;
  const newsSuggestion = _index === 'feed';
  const nameContent = buildNameContent(
    content,
    name,
    isSuggestion,
    newsSuggestion,
    searchTerm
  );
  const iconContent = buildIconContent(cams, _type, newsSuggestion);
  const breadCrumb = buildBreadCrumb(breadCrumbs, newsSuggestion, tags);

  return (
    <li className={getSearchResultClasses(itemProps.isActive)} {...itemProps}>
      <a href={getResultHref(result)}>
        {iconContent}
        {/* eslint-disable no-underscore-dangle */}
        <div className={getSearchResultTitleClasses(result._index)}>
          {/* eslint-enable no-underscore-dangle */}
          {nameContent}
          <span className="quiver-site-search__result__subtitle">
            {breadCrumb}
          </span>
        </div>
      </a>
    </li>
  );
};

SearchResult.propTypes = {
  itemProps: PropTypes.shape({
    item: resultPropType,
    index: PropTypes.number,
    isActive: PropTypes.bool,
  }).isRequired,
  result: resultPropType.isRequired,
  searchTerm: PropTypes.string.isRequired,
};

export default SearchResult;
