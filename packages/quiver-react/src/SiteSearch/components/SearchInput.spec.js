import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SearchInput from './SearchInput';

describe('SearchInput', () => {
  const props = {
    handleViewAllClick: () => {},
    inputProps: {
      onBlur: () => {},
      onChange: () => {},
      onKeyDown: () => {},
      inputValue: 'a',
    },
    viewAllLink: '/search/a#spots',
  };

  it('should render view all link if search term exists', () => {
    const defaultWrapper = shallow(
      <SearchInput {...props} searchTerm={null} />
    );
    const linkWrapper = shallow(<SearchInput {...props} searchTerm="a" />);
    const noViewAllLink = defaultWrapper.find('a');
    const viewAllLink = linkWrapper.find('a');
    expect(noViewAllLink).to.have.length(0);
    expect(viewAllLink).to.have.length(1);
  });
});
