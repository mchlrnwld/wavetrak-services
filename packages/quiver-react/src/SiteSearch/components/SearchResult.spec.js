import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SearchResult from './SearchResult';

describe('SearchResult', () => {
  it('should render active class if isActive', () => {
    const props = {
      result: {
        _index: 'spots',
        _type: 'spot',
        _source: { name: 'Pipeline', href: 'https://www.surfline.com' },
      },
      searchTerm: 'a',
    };
    const defaultWrapper = shallow(
      <SearchResult {...props} itemProps={{ isActive: false }} />
    );
    const activeWrapper = shallow(
      <SearchResult {...props} itemProps={{ isActive: true }} />
    );
    const defaultDefault = defaultWrapper.find('.quiver-site-search__result');
    const defaultActive = defaultWrapper.find(
      '.quiver-site-search__result--active'
    );
    const activeDefault = activeWrapper.find('.quiver-site-search__result');
    const activeActive = activeWrapper.find(
      '.quiver-site-search__result--active'
    );
    expect(defaultDefault).to.have.length(1);
    expect(defaultActive).to.have.length(0);
    expect(activeDefault).to.have.length(1);
    expect(activeActive).to.have.length(1);
  });

  it('should render news title class if feed index', () => {
    const props = {
      itemProps: { isActive: false },
      searchTerm: 'a',
    };
    const spotResult = {
      _index: 'spots',
      _type: 'spot',
      _source: { name: 'Pipeline', href: 'https://www.surfline.com' },
    };
    const newsResult = {
      _index: 'feed',
      _type: 'editorial',
      _source: {
        content: { title: 'Pipeline', permalink: 'https://www.surfline.com' },
      },
    };
    const spotWrapper = shallow(
      <SearchResult {...props} result={spotResult} />
    );
    const newsWrapper = shallow(
      <SearchResult {...props} result={newsResult} />
    );
    const spotDefault = spotWrapper.find('.quiver-site-search__result__title');
    const spotNews = spotWrapper.find(
      '.quiver-site-search__result__title--news'
    );
    const newsDefault = newsWrapper.find('.quiver-site-search__result__title');
    const newsNews = newsWrapper.find(
      '.quiver-site-search__result__title--news'
    );
    expect(spotDefault).to.have.length(1);
    expect(spotNews).to.have.length(0);
    expect(newsDefault).to.have.length(1);
    expect(newsNews).to.have.length(1);
  });
});
