import React from 'react';
import PropTypes from 'prop-types';
import Chevron from '../../Chevron';
import NewSearchIcon from '../../icons/NewSearchIcon';

const SearchInput = ({
  handleViewAllClick,
  inputProps,
  searchTerm,
  viewAllLink,
}) => (
  <div className="quiver-site-search__top__input">
    <NewSearchIcon />
    <input
      // eslint-disable-next-line jsx-a11y/no-autofocus
      autoFocus
      placeholder="Search spots, regions, forecasts, articles, etc."
      spellCheck="false"
      {...inputProps}
    />
    {searchTerm ? (
      <a
        href={viewAllLink}
        className="quiver-site-search__top__input__link"
        onClick={() => handleViewAllClick(searchTerm)}
      >
        View All Results
        <Chevron direction="right" />
      </a>
    ) : null}
  </div>
);

SearchInput.propTypes = {
  handleViewAllClick: PropTypes.func.isRequired,
  inputProps: PropTypes.shape({
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onKeyDown: PropTypes.func,
    inputValue: PropTypes.string,
  }),
  searchTerm: PropTypes.string,
  viewAllLink: PropTypes.string.isRequired,
};

SearchInput.defaultProps = {
  searchTerm: null,
  inputProps: {},
};

export default SearchInput;
