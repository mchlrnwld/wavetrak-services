import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SearchSection from './SearchSection';

describe('SearchSection', () => {
  const props = {
    getItemProps: () => {},
    highlightedIndex: 0,
    result: { itemIndex: 2 },
    searchTerm: 'a',
    sectionLimit: 3,
    sectionTitle: 'Map Area',
    sectionTook: 4,
    viewAllLink: '/search/a#map-area',
  };

  const sectionResults = [
    { name: 'Mock Result One' },
    { name: 'Mock Result Two' },
    { name: 'Mock Result Three' },
    { name: 'Mock Result Four' },
  ];

  it('should render search results list if search results', () => {
    const defaultWrapper = shallow(
      <SearchSection {...props} sectionResults={[]} />
    );
    const resultWrapper = shallow(
      <SearchSection {...props} sectionResults={sectionResults} />
    );
    const noResultList = defaultWrapper.find(
      '.quiver-site-search__section__results'
    );
    const resultList = resultWrapper.find(
      '.quiver-site-search__section__results'
    );
    expect(noResultList).to.have.length(0);
    expect(resultList).to.have.length(1);
  });

  it('should render view all link if search results greater than section limit', () => {
    const defaultWrapper = shallow(
      <SearchSection {...props} sectionResults={[]} />
    );
    const linkWrapper = shallow(
      <SearchSection {...props} sectionResults={sectionResults} />
    );
    const noViewAllLink = defaultWrapper.find(
      '.quiver-site-search__section__view-all'
    );
    const viewAllLink = linkWrapper.find(
      '.quiver-site-search__section__view-all'
    );
    expect(noViewAllLink).to.have.length(0);
    expect(viewAllLink).to.have.length(1);
  });
});
