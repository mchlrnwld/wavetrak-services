import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Downshift from 'downshift';
import classnames from 'classnames';
import Modal from 'react-modal';
import { v1 as uuid } from 'uuid';
import debounce from 'lodash/debounce';
import { canUseDOM, slugify } from '@surfline/web-common';
import NewCloseIcon from '../icons/NewCloseIcon';
import SearchInput from './components/SearchInput';
import SearchSection from './components/SearchSection';
import SelectionMessage from './components/SelectionMessage';
import {
  buildSectionData,
  getViewAllLink,
  handleChange,
  handleCloseKeyDown,
  handleInputBlur,
  handleInputChange,
  handleInputKeyDown,
  handleSelect,
  handleViewAllClick,
  itemToString,
  trackFocusedSearch,
} from './helpers';

class SiteSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      initialRender: true,
      searchId: null,
      searchResults: null,
      searchTerm: null,
      selection: null,
    };
    this.handleInputChange = handleInputChange.bind(this);
    this.handleViewAllClick = handleViewAllClick.bind(this);
    this.debouncedInputChange = debounce(this.handleInputChange, 100);
    this.sectionLimits = new Map();
  }

  componentDidMount() {
    this.setInitialRender();
  }

  componentDidUpdate(prevProps) {
    const { searchIsOpen, viewport, serviceUrl } = this.props;
    const { searchId } = this.state;
    if (canUseDOM && searchIsOpen && !prevProps.searchIsOpen) {
      if (viewport === 'mobile') {
        this.prevPosition = document.body.style?.position;
        document.body.style.position = 'fixed';
      }
      this.prevOverflow = document.documentElement.style?.overflow;
      document.documentElement.style.overflow = 'hidden';
      trackFocusedSearch(window, serviceUrl, {
        searchId,
      });
    }
    if (canUseDOM && !searchIsOpen && prevProps.searchIsOpen) {
      if (viewport === 'mobile') {
        document.body.style.position = this.prevPosition;
      }
      document.documentElement.style.overflow = this.prevOverflow;
      this.clearSearchTerm();
    }
  }

  clearSearchTerm = () => this.setState({ searchTerm: '' });

  boundSetState = this.setState.bind(this);

  getSectionClasses = (section) =>
    classnames({
      'quiver-site-search__section': true,
      [`quiver-site-search__section__${slugify(section)}`]: true,
    });

  getSiteSearchClasses = (viewport) =>
    classnames({
      'quiver-site-search': true,
      'quiver-site-search--mobile': viewport === 'mobile',
    });

  setInitialRender = () => {
    const searchId = uuid();
    this.setState({ initialRender: false, searchId });
  };

  render() {
    const { searchIsOpen, serviceUrl, handleCloseSearch, viewport } =
      this.props;
    const { initialRender, searchResults, searchTerm, selection, searchId } =
      this.state;
    const viewAllLink = `/search/${searchTerm}`;

    if (initialRender) return null;
    return (
      <Modal
        ariaHideApp={false}
        isOpen={searchIsOpen}
        onRequestClose={handleCloseSearch}
        contentLabel="Site Search"
        className={this.getSiteSearchClasses(viewport)}
        overlayClassName="quiver-site-search__overlay"
      >
        <Downshift
          onChange={(item, state) =>
            handleSelect(
              item,
              state,
              searchTerm,
              searchId,
              serviceUrl,
              this.boundSetState
            )
          }
          onInputValueChange={(value, state) =>
            this.debouncedInputChange(
              value,
              state,
              searchId,
              serviceUrl,
              this.boundSetState
            )
          }
          itemToString={(item) => itemToString(item)}
        >
          {({
            getInputProps,
            getItemProps,
            getMenuProps,
            highlightedIndex,
            setHighlightedIndex,
          }) => (
            <div>
              <div className="quiver-site-search__top">
                <SearchInput
                  handleViewAllClick={this.handleViewAllClick}
                  searchTerm={searchTerm}
                  viewAllLink={viewAllLink}
                  inputProps={getInputProps({
                    onBlur: (event) => handleInputBlur(event),
                    onChange: () => handleChange(setHighlightedIndex),
                    onKeyDown: (event) =>
                      handleInputKeyDown(
                        event,
                        searchTerm,
                        highlightedIndex,
                        viewAllLink,
                        setHighlightedIndex,
                        this.sectionLimits
                      ),
                  })}
                />
                <div
                  className="quiver-site-search__top__close"
                  role="button"
                  onClick={handleCloseSearch}
                  onKeyDown={(e) => handleCloseKeyDown(e, handleCloseSearch)}
                  tabIndex="0"
                >
                  <NewCloseIcon />
                </div>
              </div>
              {selection ? (
                <SelectionMessage selection={selection} />
              ) : (
                <div
                  {...getMenuProps()}
                  className="quiver-site-search__results"
                >
                  {searchResults && searchTerm.length ? (
                    searchResults.reduce(
                      (result, section, sectionIndex) => {
                        const {
                          sectionLimit,
                          sectionResults,
                          sectionTitle,
                          sectionTook,
                        } = buildSectionData(section);
                        // eslint-disable-next-line no-param-reassign
                        result.totalCount += sectionResults.length;
                        this.sectionLimits.set(sectionTitle, result.totalCount);
                        result.sections.push(
                          <div
                            // eslint-disable-next-line react/no-array-index-key
                            key={sectionIndex}
                            className={this.getSectionClasses(sectionTitle)}
                          >
                            <SearchSection
                              getItemProps={getItemProps}
                              highlightedIndex={highlightedIndex}
                              result={result}
                              searchTerm={searchTerm}
                              sectionLimit={sectionLimit}
                              sectionTitle={sectionTitle}
                              sectionTook={sectionTook}
                              sectionResults={sectionResults}
                              viewAllLink={getViewAllLink(
                                searchTerm,
                                sectionTitle
                              )}
                            />
                          </div>
                        );
                        return result;
                      },
                      { sections: [], itemIndex: 0, totalCount: 0 }
                    ).sections
                  ) : (
                    <div className="quiver-site-search__results__placeholder">
                      <p>
                        Search for surf spots, travel maps, regional forecasts
                        and surf news.
                      </p>
                    </div>
                  )}
                </div>
              )}
            </div>
          )}
        </Downshift>
      </Modal>
    );
  }
}

SiteSearch.propTypes = {
  handleCloseSearch: PropTypes.func.isRequired,
  searchIsOpen: PropTypes.bool.isRequired,
  serviceUrl: PropTypes.string.isRequired,
  viewport: PropTypes.string,
};

SiteSearch.defaultProps = {
  viewport: null,
};

export default SiteSearch;
