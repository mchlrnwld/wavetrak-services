import React, { useState } from 'react';
import {
  dismissContentCard,
  trackContentCardClick,
  trackContentCardImpressions,
} from '@surfline/web-common';
import PropTypes from 'prop-types';
import CloseIcon from '../icons/CloseIcon';
import withContentCards from '../withContentCards';

const ContentCard = ({ card }) => {
  const [closed, setClosed] = useState(false);
  const [rendered, setRendered] = useState(false);

  // Component CTA was clicked
  const handleClick = (e) => {
    e.preventDefault();
    trackContentCardClick(card);
    setTimeout(() => {
      window.location.assign(card.extras.href__url);
    }, 200);
  };

  // Component was dismissed
  const handleCloseClick = (e) => {
    e.preventDefault();
    setClosed(true);
    dismissContentCard(card?.name);
  };

  // Component was displayed for the first time
  if (!rendered && card?.extras) {
    setRendered(true);
    trackContentCardImpressions([card]);
  }

  return card?.extras && !closed ? (
    <div className="quiver-content-card">
      <div className="quiver-content-card__body">
        <div className="quiver-content-card__close">
          <CloseIcon onClick={handleCloseClick} isCircle />
        </div>
        <img
          src={card.extras.icon__url}
          className="quiver-content-card__image"
          alt=""
        />
        <div className="quiver-content-card__text-container">
          <div className="quiver-content-card__tagline">
            {card.extras.tagline__text}
          </div>
          {card.extras.href__url ? (
            <a
              href={card.extras.href__url}
              className="quiver-content-card__href"
              onClick={handleClick}
            >
              {card.extras.href__text}
            </a>
          ) : null}
        </div>
      </div>
    </div>
  ) : null;
};

ContentCard.propTypes = {
  card: PropTypes.shape({
    name: PropTypes.string,
    extras: PropTypes.shape({
      href__text: PropTypes.string,
      href__url: PropTypes.string,
      icon__url: PropTypes.string,
      tagline__text: PropTypes.string,
    }),
    title: PropTypes.string,
  }).isRequired,
};

export default withContentCards(ContentCard);
