import React from 'react';
import ContentCard from './index';

export default {
  title: 'ContentCard',
};

const testCard = {
  name: 'Braze Content Card - Spot',
  extras: {
    href__url: '/',
    href__text: 'Learn more about this spot',
    tagline__text:
      'Most likely to match. These conditions could be great for your skill level.',
    icon__url:
      'https://wa.cdn-surfline.com/quiver/0.18.2/weathericons/CLEAR.svg',
  },
};

export const Default = () => <ContentCard card={testCard} />;

Default.story = {
  name: 'default',
};
