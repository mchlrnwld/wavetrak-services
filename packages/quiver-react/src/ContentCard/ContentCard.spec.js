import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import ContentCard from './ContentCard';
import CloseIcon from '../icons/CloseIcon';

describe('ContentCard', () => {
  it('should render the closable content card with a clickable link', () => {
    const testCard = {
      name: 'Braze Content Card - Spot',
      extras: {
        href__url: '/',
        href__text: 'Learn more about this spot',
        tagline__text: 'Some tagline text.',
        icon__url: 'https://wa.cdn-surfline.com/test_icon.svg',
      },
    };

    const wrapper = mount(<ContentCard card={testCard} />);
    const tagline = wrapper.find('.quiver-content-card__tagline');
    expect(tagline.text()).to.equal(testCard.extras.tagline__text);

    wrapper.find(CloseIcon).simulate('click');
    expect(wrapper.find('.quiver-content-card__tagline')).to.have.lengthOf(0);
  });
});
