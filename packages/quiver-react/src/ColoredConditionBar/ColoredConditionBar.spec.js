import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import ColoredConditionBar from './ColoredConditionBar';

describe('ColoredConditionBar', () => {
  it('should render the condition bar', () => {
    const wrapper = mount(<ColoredConditionBar value="GOOD" />);

    const condition = wrapper.find('.quiver-colored-condition-bar--good');

    expect(condition).to.have.length(1);
  });

  it('should render the condition bar when expired report', () => {
    const wrapper = mount(<ColoredConditionBar value="GOOD" expired />);

    const expired = wrapper.find('.quiver-colored-condition-bar--expired');
    expect(expired).to.have.length(1);
    expect(expired.text()).to.be.equal('REPORT COMING SOON');
  });

  it('should render unobservable conditions', () => {
    const wrapper = mount(<ColoredConditionBar value="NONE" />);

    const expired = wrapper.find('.quiver-colored-condition-bar');
    expect(expired).to.have.length(1);
    expect(expired.text()).to.be.equal('CONDITIONS UNOBSERVABLE');
  });
});
