import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';
import {
  conditionClassModifier,
  conditionEnumToText,
} from '@surfline/web-common';

const className = (value, expired) =>
  classnames({
    'quiver-colored-condition-bar': true,
    [`quiver-colored-condition-bar--${conditionClassModifier(value)}`]: true,
    'quiver-colored-condition-bar--expired': expired,
  });

const ColoredConditionBar = ({ value, expired }) => {
  const conditionText = expired
    ? 'Report Coming Soon'.toUpperCase()
    : conditionEnumToText(value);
  return (
    <div className={className(value, expired)}>
      {value === 'NONE'
        ? 'conditions unobservable'.toUpperCase()
        : conditionText}
    </div>
  );
};

ColoredConditionBar.propTypes = {
  value: PropTypes.string.isRequired,
  expired: PropTypes.string,
};

ColoredConditionBar.defaultProps = {
  expired: false,
};

export default ColoredConditionBar;
