import React from 'react';
import ColoredConditionBar from './ColoredConditionBar';

export default {
  title: 'ColoredConditionBar',
};

export const Good = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBar value="GOOD" />
  </div>
);

Good.story = {
  name: 'GOOD',
};

export const FairToGood = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBar value="FAIR-TO-GOOD" />
  </div>
);

FairToGood.story = {
  name: 'FAIR-TO-GOOD',
};

export const Fair = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBar value="FAIR" />
  </div>
);

Fair.story = {
  name: 'FAIR',
};

export const PoorToFair = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBar value="POOR-TO-FAIR" />
  </div>
);

PoorToFair.story = {
  name: 'POOR-TO-FAIR',
};

export const Poor = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBar value="POOR" />
  </div>
);

Poor.story = {
  name: 'POOR',
};

export const Epic = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBar value="EPIC" />
  </div>
);

Epic.story = {
  name: 'EPIC',
};

export const None = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBar value="NONE" />
  </div>
);

None.story = {
  name: 'NONE',
};

export const ReportComingSoon = () => (
  <div className="quiver-spot-report">
    <ColoredConditionBar expired value="FAIR" />
  </div>
);
