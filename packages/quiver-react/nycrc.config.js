const babelConfig = require('@istanbuljs/nyc-config-babel');
const hookRunInThisContextConfig = require('@istanbuljs/nyc-config-hook-run-in-this-context');

module.exports = {
  ...babelConfig,
  ...hookRunInThisContextConfig,
  all: true,
  lines: 75,
  branches: 70,
  statements: 80,
  'check-coverage': true,
  include: ['src/**/*.js'],
  extension: ['.js'],
  exclude: ['**/*.spec.js', '**/jwplayer.js', '**/stories.js'],
  reporter: ['lcov', 'text-summary'],
};
