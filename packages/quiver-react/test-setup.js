import Enzyme from 'enzyme';
import { platform } from 'process';
import Adapter from 'enzyme-adapter-react-16';
import chai from 'chai';
import sinon from 'sinon';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import { JSDOM } from 'jsdom';
import Mocha from 'mocha';
import glob from 'glob';

const jsdom = new JSDOM(undefined, {
  url: 'http://localhost/',
  pretendToBeVisual: true,
});

const { window } = jsdom;

function copyProps(src, target) {
  Object.defineProperties(target, {
    ...Object.getOwnPropertyDescriptors(src),
    ...Object.getOwnPropertyDescriptors(target),
  });
}

global.window = window;
global.document = window.document;
global.navigator = {
  userAgent: 'node.js',
  platform,
};
global.requestAnimationFrame = function (callback) {
  return setTimeout(callback, 0);
};
global.cancelAnimationFrame = function (id) {
  clearTimeout(id);
};
copyProps(window, global);

// Instantiate a Mocha instance.
const mocha = new Mocha({
  allowUncaught: false,
  checkLeaks: true,
});

Enzyme.configure({ adapter: new Adapter() });

// Setup dirty-chai and sinon-chai for all tests
chai.use(dirtyChai);
chai.use(sinonChai);

global.googletag = {
  cmd: {
    push: sinon.spy(),
  },
  destroySlots: sinon.spy(),
};

global.window.analytics = {
  identify: sinon.spy(),
  page: sinon.spy(),
  reset: sinon.spy(),
  track: sinon.spy(),
  trackLink: sinon.spy(),
};

const testFiles = glob.sync('src/**/*.spec.js');

testFiles.forEach((file) => {
  mocha.addFile(file);
});

mocha.timeout(30000);

// Run the tests.
mocha.run((failures) => {
  process.exitCode = failures ? 1 : 0;
});
