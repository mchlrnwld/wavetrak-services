# @surfline/quiver-react

## Installation

```sh
npm install --save @surfline/quiver-react
```

## Usage

`quiver-react` components do not have any styling associated with them. `quiver-themes` provides stylesheets that can be used with the components.

Component styles can be overwritten locally by passing a `style` prop into the component.

Here is a quick example to get you started:

#### ./app.js

```JavaScript
import React from 'react';
import ReactDOM from 'react-dom';
import '@surfline/quiver-themes/stylesheets/crossbrand.css';
import AwesomeApp from './AwesomeApp';

ReactDOM.render(<AwesomeApp />, document.getElementById('app'));
```

#### ./AwesomeApp.js

```JavaScript
import React from 'react';
import { Input } from '@surfline/quiver-react';

const style = {
  margin: '15px 30px',
  label: {
    fontStyle: 'italic'
  }
};

const AwesomeApp = () => (
  <div>
    <Input type="text" label="Default Style" />
    <Input type="password" label="Custom Style" style={style} />
  </div>
);

export default AwesomeApp;
```

## Development

If developing in conjunction with `@surfline/quiver-themes`, use `npm link`.

```sh
npm install
```

#### Adding new components

New components that are added should live in the top level of `src/` unless they are icons which go in `src/icons` or they are hooks which go in `/src/hooks`. If a component is not meant to be exported it should be put into
the folder of the component that consumes it, for example:

```
SomeComponent /
├── UnExportedChild/
│   ├── index.js
│   ├── UnExportedChild.js
├── SomeComponent.js
└── index.js
```

In order to be included into the bundle components *MUST* be exported from `/src/index.js`. This allows us to avoid exporting any code that may bloat the library. Rollup uses `src/index.js` as the entrypoint to create all 3 bundles

#### Bundles and Building

Building the app is achieved by running `npm run build` which invokes rollup to create the 3 bundles. The 3 bundles are as follows:
  - `cjs/@surfline/quiver-react.js` is a development Common JS bundle that is not minified. It used by Node in development mode since it is not an ES Module.
  - `cjs/@surfline/quiver-react.min.js` is a production Common JS bundle that is minified. It used by Node in production mode since it is not an ES Module.
  - `esm/` is an unminified ES module version of the bundle which can be used by another Rollup or Webpack build to achieve tree shaking. (achieved by having the `module` property set in the package.json)

#### React Hooks And Linking

If developing with any of the various front-ends and using react hooks that live in `/src/hooks`
it is important to link quiver-react's version of React to the one used by the front-end so
that you are not using two seperate instances of React. This can be done with the following command
from inside the `/react/` folder:

```sh
npm link ../../surfline-web/homepage/node_modules/react
```

It may also be helpful to link `react-dom` if you get an error `cannot find module "react-dom"`
```sh
npm link ../../surfline-web/homepage/node_modules/react-dom
```

### Storybook

Use [React Storybook](https://github.com/kadirahq/react-storybook/) to develop your components.

```sh
npm run storybook
```

Write stories in the same directory as the component in a `stories.js` file. Provide thorough stories for all useful component states and functionalities.

## React Router and linking

When developing with components that have a `react-router-dom` `<Link>` component, there is the possibility of
having an invariant error. This happens as a result of multiple instances of the library being used in the code.

The fix is to link directly to the `react-router` instance in the front-end being linked to the `@surfline/quiver-react`

```sh
cd node_modules/react-router-dom
npm link ../../../surfline-web/kbyg/node_modules/react-router
```

### Style Management

Component styles should be decoupled from React components. Components should be targetable using CSS classes following naming conventions in the [themes README.md](../themes/README.md). They should also allow inline styles from `prop.style`.

## Publishing NPM package

Within the `/packages/` directory we publish NPM packages using Lerna.

A step by step guide for publishing NPM packages in the monorepo can be found [here](../../README.md#npm-packages).
