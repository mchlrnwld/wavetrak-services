# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## <small>19.17.5 (2022-02-23)</small>

* fix: update freemium cta button text ([d1fa1de](https://github.com/Surfline/wavetrak-services/commit/d1fa1de))





### [19.17.4](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.17.3...@surfline/quiver-react@19.17.4) (2022-02-18)


### Bug Fixes

* support new condition colors in spot marker component ([b505fd0](https://github.com/Surfline/wavetrak-services/commit/b505fd0daa48f992c3fe1daac03aeaff7fc6e059))



### [19.17.3](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.17.2...@surfline/quiver-react@19.17.3) (2022-02-17)


### Bug Fixes

* **SwellEvent:** hide thumbnail with prop is an empty string ([59e7c44](https://github.com/Surfline/wavetrak-services/commit/59e7c44ed3da739351d95db805c4e2efeec6a198)), closes [CP-138](https://wavetrak.atlassian.net/browse/CP-138)



### [19.17.2](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.17.1...@surfline/quiver-react@19.17.2) (2022-02-16)


### Bug Fixes

* **SwellEvent:** hide thumbnail when it does not exist ([0f1a587](https://github.com/Surfline/wavetrak-services/commit/0f1a587c69492c5f9d8cb43e189df7972983f6a7)), closes [CP-138](https://wavetrak.atlassian.net/browse/CP-138)



## <small>19.17.1 (2022-02-16)</small>

* fix: update freemium cta links ([9337bbe](https://github.com/Surfline/wavetrak-services/commit/9337bbe))





## [19.17.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.16.0...@surfline/quiver-react@19.17.0) (2022-02-16)


### Features

* **SwellEvent:** add 20ft+ features to SwellEventCard ([f01c7a8](https://github.com/Surfline/wavetrak-services/commit/f01c7a84de940f7f47e01b9f9799ec6fea30d022)), closes [CP-138](https://wavetrak.atlassian.net/browse/CP-138)


### Bug Fixes

* **tests:** add start-server-and-test dependency for e2e tests ([18b55aa](https://github.com/Surfline/wavetrak-services/commit/18b55aa05e19a52c6b9ceebe19f9c006ebc6de35))


### Chores

* **deps:** update dependencies ([ed57243](https://github.com/Surfline/wavetrak-services/commit/ed57243ae5e14c26279335fffaad4c96a00ce7ce))



## [19.16.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.15.2...@surfline/quiver-react@19.16.0) (2022-02-14)


### Features

* fe1179: add sevenRatings prop to various components; bump quiver-themes version for new colors ([e6fb0b9](https://github.com/Surfline/wavetrak-services/commit/e6fb0b938bf6d3a7b4f243a40e85df66b80809ce))
* fe1179: Add sevenRatings prop to various quiver components; quiver-themes version bump ([70f17aa](https://github.com/Surfline/wavetrak-services/commit/70f17aad5f34ee090c0a19e6e22be3f7aceb20d1))
* fe1179: update with master ([75b5844](https://github.com/Surfline/wavetrak-services/commit/75b5844f610eaa8436a4f682fedfa489d5dbf284))



## <small>19.15.2 (2022-02-11)</small>

* fix: ui updates to freemium ctas ([cf013bb](https://github.com/Surfline/wavetrak-services/commit/cf013bb))
* wip ([2c4d05a](https://github.com/Surfline/wavetrak-services/commit/2c4d05a))





## <small>19.15.1 (2022-02-08)</small>

* fix: ui polishes for freemium ctas ([5f7cae3](https://github.com/Surfline/wavetrak-services/commit/5f7cae3))





## 19.15.0 (2022-02-03)

* feat: add segment support for freemium ctas ([6c865ac](https://github.com/Surfline/wavetrak-services/commit/6c865ac))





## <small>19.14.3 (2022-02-02)</small>

* fix: camCTA style updates ([e8ff1a5](https://github.com/Surfline/wavetrak-services/commit/e8ff1a5))





### [19.14.2](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.14.1...@surfline/quiver-react@19.14.2) (2022-01-31)


### Chores

* **e2e:** resolve e2e testing script issue ([5ed400c](https://github.com/Surfline/wavetrak-services/commit/5ed400cba3eafbeed75d1f2f45987c8e0a8a3e99))



### [19.14.1](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.14.0...@surfline/quiver-react@19.14.1) (2022-01-31)


### Bug Fixes

* cam-player timeout not setting showTimeout flag ([75ebfaa](https://github.com/Surfline/wavetrak-services/commit/75ebfaa8854d8a7d9787a65bd6ca7b09600578cf))



## 19.14.0 (2022-01-27)

* feat: add ForecastGraphsDaySummariesCTA paywall ([677f32f](https://github.com/Surfline/wavetrak-services/commit/677f32f))


## <small>19.13.1 (2022-01-14)</small>

* fix: updates to DailyForecastReportCTA component ([2c9d38f](https://github.com/Surfline/wavetrak-services/commit/2c9d38f))





## [19.13.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.12.2...@surfline/quiver-react@19.13.0) (2022-01-13)


### Features

* support Next.js app `splitReady` flag for Split Treaments ([a571b31](https://github.com/Surfline/wavetrak-services/commit/a571b31b4d02529d93af2d0900d7b32452ac1f72))



## <small>19.12.2 (2022-01-07)</small>

* fix: sa-3917 ui updates to CamCTA component ([647e872](https://github.com/Surfline/wavetrak-services/commit/647e872))





### [19.12.1](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.12.0...@surfline/quiver-react@19.12.1) (2022-01-06)


### Bug Fixes

* make next a dependency to prevent compile errors ([82ff2ab](https://github.com/Surfline/wavetrak-services/commit/82ff2aba4217335d35c1e3b08ab10c0fadb12d63))



## [19.12.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.11.0...@surfline/quiver-react@19.12.0) (2022-01-04)


### Features

* allow using NextJS Link for forecast graphs ([b876ab1](https://github.com/Surfline/wavetrak-services/commit/b876ab144ffe596184585c14e25dbabd2cfc7fc2))



## [19.11.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.10.0...@surfline/quiver-react@19.11.0) (2022-01-03)


### Features

* [FE-1041](https://wavetrak.atlassian.net/browse/FE-1041) Remove legacy wave buoys link from forecast navigation ([fce5a1b](https://github.com/Surfline/wavetrak-services/commit/fce5a1be2f28c5f901135df706279cb196dd670b))



## [19.10.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.9.0...@surfline/quiver-react@19.10.0) (2021-12-23)


### Features

* add css modules classes support ContentContainer ([f5d5f6e](https://github.com/Surfline/wavetrak-services/commit/f5d5f6eceda251c80282fcb3e7bc8b9c74567841))
* add css modules classes support ErrorBox ([52cd83a](https://github.com/Surfline/wavetrak-services/commit/52cd83a43a2d8f1fb6dcf12c163753baa80156ff))



## 19.9.0 (2021-12-23)

* feat: sa-4056 Add Forecast Report Paywall ([8c4b698](https://github.com/Surfline/wavetrak-services/commit/8c4b698))





## 19.8.0 (2021-12-21)

* feat: add Regional Forecast Update CTA ([3a05fab](https://github.com/Surfline/wavetrak-services/commit/3a05fab))





### [19.7.1](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.7.0...@surfline/quiver-react@19.7.1) (2021-12-17)


### Bug Fixes

* **GoogleDFP:** fix htl ad display calls ([e103a9f](https://github.com/Surfline/wavetrak-services/commit/e103a9fab1dc81eea0d29d9f22f7ace7ab50fd4b))



## 19.7.0 (2021-12-16)

* feat: add CamCTA component ([9e847d7](https://github.com/Surfline/wavetrak-services/commit/9e847d7))





## [19.6.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.5.1...@surfline/quiver-react@19.6.0) (2021-12-16)


### Features

* CP-23 Add isTesting prop to GoogleDFP quiver-react component ([3f8ccbf](https://github.com/Surfline/wavetrak-services/commit/3f8ccbf4933e0140282c7855a3f550f4070e3b51))



### [19.5.1](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.5.0...@surfline/quiver-react@19.5.1) (2021-12-15)


### Bug Fixes

* support css modules using className prop on PageRail ([c59379f](https://github.com/Surfline/wavetrak-services/commit/c59379f4b3e2abab0a14a41bb71f6316114ca907))



## [19.5.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.4.2...@surfline/quiver-react@19.5.0) (2021-12-10)


### Features

* CP-23 Update quiver-react GoogleDFP adId value for HTL ([f4e0523](https://github.com/Surfline/wavetrak-services/commit/f4e0523721034f7a71255d314d59d113f327abc4))



### [19.4.2](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.4.1...@surfline/quiver-react@19.4.2) (2021-12-07)

**Note:** Version bump only for package @surfline/quiver-react





### [19.4.1](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.4.0...@surfline/quiver-react@19.4.1) (2021-12-01)


### Bug Fixes

* wrap rendered graphs in error boundary ([ec9c327](https://github.com/Surfline/wavetrak-services/commit/ec9c327214f2f56c8dc38df2504694c392677096))



## [19.4.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.2.0...@surfline/quiver-react@19.4.0) (2021-11-29)


### Features

* CP-23 Add isHtl prop to GoogleDFP component ([#6312](https://github.com/Surfline/wavetrak-services/issues/6312)) ([666f468](https://github.com/Surfline/wavetrak-services/commit/666f46802acb9f45f0ed20f0c7eb474e3c43711b))


### Bug Fixes

* CP-22 Pass isHtl prop to GoogleDFP component getAdId ([17b76c6](https://github.com/Surfline/wavetrak-services/commit/17b76c687422c5ff25ad6d7e870c5362ef094db5))



## [19.3.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.2.0...@surfline/quiver-react@19.3.0) (2021-11-23)


### Features

* CP-23 Add useHtl prop to GoogleDFP component ([edf249c](https://github.com/Surfline/wavetrak-services/commit/edf249cf130d056951423e53cc80364e1d211308))



## [19.2.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.1.0...@surfline/quiver-react@19.2.0) (2021-11-05)


### Features

* MED-571 Enable custom permalink for SwellEventCard ([78c3024](https://github.com/Surfline/wavetrak-services/commit/78c30246536027261f5dcfb5ee68a07dcc85ced2))



## [19.1.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@19.0.0...@surfline/quiver-react@19.1.0) (2021-11-03)


### Features

* add ratings hover state to surf graph tooltip ([80f14c0](https://github.com/Surfline/wavetrak-services/commit/80f14c067583de5407d4373316f866c654a41e91))



## [19.0.0](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-react@18.1.6...@surfline/quiver-react@19.0.0) (2021-10-29)


### ⚠ BREAKING CHANGES

* removed the `conditionVariation` prop from `ForecastGraphs` and `SpotReport` components

### Features

* apply condition variation modifications to automated ratings ([8792512](https://github.com/Surfline/wavetrak-services/commit/87925121474b875276d8414f5577585390fdae00))


### Miscellaneous Chores

* remove conditionVariations in favor of conditionsV2 ([92ff879](https://github.com/Surfline/wavetrak-services/commit/92ff87955e9b860074f817b71a2a4acfc21b9a14))



## 18.1.6 (2021-10-21)

### Features

* create automated ratings boxes for forecast graphs ([b6fdb33](https://github.com/Surfline/wavetrak-services/commit/b6fdb330dbac05178e7a6df51d36b9d495c01938))



### 18.1.5 (2021-10-20)


### Bug Fixes

* jsdom testing errors ([823acc7](https://github.com/Surfline/wavetrak-services/commit/823acc726497e0765df4819e6657cac13741be00))



### 18.1.4 (2021-10-19)

**Note:** Version bump only for package @surfline/quiver-react





## <small>18.1.3 (2021-10-18)</small>

* chore(quiver-react): fix package bundling issue ([32d8c63](https://github.com/Surfline/wavetrak-services/commit/32d8c63))
* FE-606: Remove deleted subregions from taxonomy navigator (#5838) ([10448e5](https://github.com/Surfline/wavetrak-services/commit/10448e5)), closes [#5838](https://github.com/Surfline/wavetrak-services/issues/5838)
* INF-000: CI/CD Pipeline Bugs - Add surfline-dev AWS credentials to test jobs (#5837) ([ad566ef](https://github.com/Surfline/wavetrak-services/commit/ad566ef)), closes [#5837](https://github.com/Surfline/wavetrak-services/issues/5837)
* WP-94: Migrate quiver-react to /packages (#5802) ([443a269](https://github.com/Surfline/wavetrak-services/commit/443a269)), closes [#5802](https://github.com/Surfline/wavetrak-services/issues/5802)





### 18.1.2 (2021-10-18)


### Bug Fixes

* update contains logic in TaxonomyNavigator to check for spots if taxonomyType is subregion ([5365a51](https://github.com/Surfline/wavetrak-services/commit/5365a51ee4cfefdb770dcc38e27d2f51b3ff6458))



## <small>18.1.1 (2021-10-15)</small>

* chore(quiver-react): migrate quiver-react to packages ([56921ce](https://github.com/Surfline/wavetrak-services/commit/56921ce))





# Quiver React Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## 18.0.0
  - fix (WP-182): Remove `DetectAdBlock` package and refs

  BREAKING CHANGE:
    - Removal of `DetectAdBlock` will affect any application currently consuming this package.

## 17.11.1
  - fix (FE-806): Properly remove remaining split values for DST Split

## 17.11.0
  - feat: Remove the LOLA Classic link from the forecast navigation menu

## 17.10.7
  - fix: SA-3775 - Add Braze key/shape validations in WithContentCards HOC

## 17.10.6
  - fix: Updated ErrorBoundary to include stack trace in error details that are sent to NewRelic

## 17.10.5
- RT-661: Remove legacyId logic in cam components

## 17.10.4
  - SA-3811: Add control variant conditon for ContentCards

## 17.10.3
  - fix: Change default return value from `LOLA` to `LOTUS` in `checkConditionValue`

## 17.10.1
  - chore(defaultLinks): Remove Science and Hurricanes
  - chore(README): Update read me script for launching storybook

## 17.10.0
  - fix: Use `eventHandlers` prop with `Marker` from leaflet as referenced [here](https://react-leaflet.js.org/docs/api-components/#evented-behavior)
  - fix: Memoize Icon in `CircleMarker` to prevent re-renders of icon that may break popups
  - feat: Add `borderWidth` to `CircleMarker`
  - fix: Only render one marker per position and rely on `worldCopyJump` to be set on map
  - feat: Allow `ref` objects to be created for `CircleMarker` and `Popup` and `CustomMarker`
  - fix: Misc bug-fixes and tweaks to marker components to make them work for map implementation
  - feat: Add `Pane` component from `leaflet`

## 17.9.1
  - fix: Add back `className` prop on `Popup` (accidentally removed)
  - fix: `onClick` moved into `CircleIcon` since `Marker` does not take an `onClick`
  - fix: prevent state update on unmounted leaflet component import

## 17.9.0
  - feat: Add new `iconClassName` and `zIndexOffset` props to `CircleMarker`
  - feat: Allow forwarding refs to the `Popup` component to allow parent to control the popup

## 17.8.1
  - SA-3619: Replace ctaLocation with location for trackSubscribedCTA

## 17.8.0
  - feat: Add max password length limit to `Input`
  - feat: Add `MIN_PASSWORD_LENGTH` and `MAX_PASSWORD_LENGTH` constants

## 17.7.0
  - feat: Add `error` prop to `ErrorBoundary` to allow using it to render fallback UI as well as an error catcher

## 17.6.0
  - feat(NewNavigationBar): update news menu items WP-138
  - feat: update node version from 12 to 14 including `.nvmrc` for local development WP-129

## 17.5.4
  - feat(NewNavigationBar): remove premium news link WP-132

## 17.5.3
  - WP-65: Add `eslint:fix` to package scripts for autofixing lint errors
  - SA-3695: bump @surfline/web-common dep to 14.5.2

## 17.5.2
  - SA-3695: Add Braze tracking to ContentCard

## 17.5.1
  - FE-440: Error message for cam rewind
  - FE-440: Fix lint issues

## 17.5.0
  - FE-416: Add optional 'title' and 'isEmptySwell' props to SwellStats component

## 17.4.0
  - feat: Add support for rendering map markers/popups using React-DOM portals
    - See `src/Map/README.md` for more information on how to achieve this
    - Adds the `map` and `usingPortal` props to `CircleMarker`, `Popup`, `LocationMarker`, and `CustomMarker` components
  - MED-426: Update nav link for News/Cuervo Challenge

## 17.3.0
  - feat: Add `ContentCard` component
  - fix: Pass HOC props to children in `withContentCards` component

## 17.2.1
  - MED-466: Hide `Cuervo Challenge` nav link from News menu

## 17.2.0
  - feat: Use `defaultCard`, `defaultCards` props to set defaults in `withContentCards` HOC

## 17.1.0
  - feat: Add `properties` prop to `ErrorBoundary` to enable sending error properties to NewRelic

## 17.0.1
  - fix: replace `react-leaflet` with `@monsonjeremy/react-leaflet` until the owner of the library can
    resolve the issues
    - https://github.com/PaulLeCam/react-leaflet/pull/885

## 17.0.0
  - FE-422: Update Leaflet to V3
  - FE-422: Add news props to `Map` component to improve extensibility
  - Updated to Storybook 6
  - Updated storybook stories to the new story format

  BREAKING CHANGE:
    - `react-leaflet` was updated to V3 which was a major version bump. I haven't seen any specific
      breaking changes but it's possible that current uses will break.

## 16.6.1
  - FE-374: Add Condition Ratings Variant 3

## 16.6.0
  - feat: SA-3472: Add ClickedLink tracking to SL Footer
  - FE-374: Add Condition Ratings Variant 4

## 16.5.7
  - feat: FE-316: Default surf condtion to LOTUS
  - fix: FFE-175: Fix broken segment attribute in `SiteSearch`

## 16.5.6
  - fix: send missing spot ID in cam events

## 16.5.5
  - fix: add missing properties to cam player playback events

## 16.5.4
  - FE-300: Remove DST timestamps split logic
    - Moved `GraphContainerV2` into `GraphContainer`
    - Moved `ContinuousGraphContainerV2` into `ContinuousGraphContainer`

## 16.5.2
  - fix: Fix non-fatal JS Errors in some components

## 16.5.1
  - FE-231: Remove surf-alerts from nav
  - fix: RT-197: Added New Relic cam error logging for `CamPlayer` and `useCamError`
  - FE-305: Remove future tide link

## 16.5.0
  - FE-285: Add playback event tracking to `CamPlayer` and `CamPlayerV2`

## 16.4.0
  - SA-2951: Export credit card icons and `OrBreak` component

## 16.3.0
  - SA-3204: Add CCPA links to `SurflineFooter`

## 16.2.0
  - SA-3364: Add label for `CreateAccountForm` button

## 16.1.0
  - FE-256: Make wind graphs consume `utcOffset` property behind split
  - FE-256: Make weather graphs consume `utcOffset` property behind split
  - FE-256: Make tide graphs consume `utcOffset` property behind split

## 16.0.0
  - Remove `addQuiverTreatmentToWindow`

## 15.1.6
  - FE-256: Create `GraphContainerV2` helper `GraphColumn`
  - FE-256: Move `GraphTimestamp` component into it's own folder
  - fix: Remove destructuring of `window` object since it is not iterable

## 15.1.5
  - FE-256: Duplicate `GraphContainer` component into `GraphContainerV2` to prepare for large change-set

## 15.1.4
  - FE-256: Update `SwellGraph` `SurfGraph` and `SurfLineGraph` to use the `point.utcOffset` if the
    timestamp utc offsets split is enabled

## 15.1.3
  - FE-256: Consume `sl_all_timestamp_utc_offsets` split in `ForecastGraphSurfLine` component to conditionally
    render the `ContinuousGraphContainerV2`

## 15.1.2
  - FE-256: Consume `sl_all_timestamp_utc_offsets` split in `ForecastGraphs`
  - FE-256: Consume `sl_all_timestamp_utc_offsets` split in `ForecastGraphSurf` component to conditionally
    render the `ContinuousGraphContainerV2`
  - FE-256: Consume `sl_all_timestamp_utc_offsets` split in `ForecastGraphSurfLine` component to conditionally
    render the `ContinuousGraphContainerV2`
  - Duplicated `ColoredConditionBar` component as `ColoredConditionBarV2` for testing updated condition mappings
  - Duplicated `ConditionDaySummary` component as `ConditionDaySummaryV2` for testing updated condition mappings
  - Added `applyConditionMapping` helper to map conditions to variations

## 15.1.1
  - FE-256: Update the `ContinuousGraphContainerV2` component to consume the `utcOffset` that is now
    being sent along with timestamps
  - FE-256: Remove some extra logic that exist in the `ContinuousGraphContainerV2` component

## 15.1.0
  - chore: Bump dependencies
  - chore: Remove use of the deprecated `rollup-plugin-babel` package in favor of `@rollup/plugin-babel`
  - feat: add `addQuiverTreatmentToWindow` which is a helper function for whitelisting splits to be
    loaded by the consuming client

## 15.0.0
  - BREAKING CHANGE: Removed `SplitBrowser` component
  - BREAKING CHANGE: Updated `@surfline/web-common` to `12.0.0` which contains non-backwards compatible changes to our Split implementation (see its changelog for details)
  - Added `Treatment` component
  - Added `useTreatment` hook
  - Added `withTreatment` HoC
  - Removed unused split treatment logic

## 14.0.0
  - This version was accidentally skipped during a release. This package does not have any `14.x.x` versions. Please use `^15.0.0`

## 13.3.1

- fix: Fixed an issue where Account doesn't receive the referrer so it redirects to the homepage instead of the referring page

## 13.3.0

- Include OutOfPageSlot functionality to GoogleDFP component

## 13.0.3

- fix: Stop datetime bar scrolling past forecast graphs

## 13.0.2

  - :bug:: changed camera.name to camera.title in 'Clicked Cam Rewind' event as camera.name isn't a
    thing

## 13.0.0

  - BREAKING CHANGE: upgraded to `react-cookie: ^4.0.0`. The `ForecastGraphs`component will now need
    to be wrapped in a `CookieProvider` in some part of the app tree. See `react-cookie` docs
    [here](https://www.npmjs.com/package/react-cookie)
  - chore: Update `downshift` to `v6.0.0`
  - fix: Fixed an issue where opening the search bar would break the scroll lock for rate-limiting

## 12.22.0

  - Added a `CLICKED_MAIN_NAV` event to the sign in button on the nav bar

## 12.21.3

  - :bug:: fix an issue where the `RecentlyVisited` component would throw a javascript error if a data
    fetch failed

## 12.21.2

  - Upgraded Prettier to `^2.0.0` and auto-formatted files

## 12.21.0

  - :bug:: Fixed an issue where `Button` onClick was set to `false` when submitting
  - :bug:: Fixed an issue where the checkmark on button success was invisible
  - :raised_hands: Added a `fill` prop to the `Checkmark` component
  - :raised_hands: Added a `message` prop to the `ErrorBox` to override the default message
  - :raised_hands: Added `forwardRef` to the `Input` component to allow it to be controlled by `react-hook-form`
  - :raised_hands: Added a `controlled` prop to the `Input` component to disable some internal logic for controlled inputs

## 12.19.2

  - Remove gradient from reg-wall

## 12.19.1

  - Fix an issue where `NODE_ENV` was written into the bundle at build time rather than using
    the value from consuming apps at runtime
  - Skip lib check when getting typescript type output
  - Fix scroll locking issues for metering walls

## 12.18.1

  - Fixed a bug where `CamPlayerV2` would throw an error trying to remove an already removed player

## 12.18.0

  - Create `CamPlayerV2` component for metering (cam player with no ads)
  - Upgrade outdate dependencies

## 12.17.5

  - Update login form auto complete property name and value

## 12.17.4

  - Fix login form not autocompleting email/password

## 12.17.3

  - Patch: add option to show jw player speed controls

## 12.17.2

  - Patch: increment quiver themes dep to 6.9.1

## 12.17.1

  - Patch: increment quiver themes dep

## 12.17.0

  - Feat: Add SpotDescriptor component

## 12.16.1

  - fix: change segment props to ones from tracking plan

## 12.16.0

  - Feat: update CamPlayer to generate rewindUrl and supply new segment props

## 12.15.1

  - fix: include missing props in default PremiumCamCTA test

## 12.15.0

  - make PremiumCamCTA reusable for cam-rewind

## 12.14.8

  - Fix an incorrect union type that needed parentheses around it
  - Fix broken propType types that had a clashing namespace

## 12.14.7

  - Fix an issue where password length validation was being performed on the login form (some users may have short passwors since we did
    always validate passwords)
  - Fix an issue where the `TrackableLink` component gave a React development error related to `forwardRef`

## 12.14.2

  - Fix an issue where an extra `<script/>` tag was injected into the `SegmentBrowser` causing it to not get loaded properly

## 12.14.0

  - Update `SegmentBrowser` to use the segment snippet from web common, which allows us to use the cache and pre-loaded `anonymousId` logic
  - Update registry url to use `jfrog.io` instead of `artifactoryonline.com`

## 12.9.0

  - Add `NewTabIcon` component
  - Add JSDoc Typings to some components
  - Add Typescript types into build output

## 12.4.1

  - fix: Make Click handler conditional on `Map` so leaflet doesn't attach a listener when there's no function.

## 12.4.0

  - Add `swellUnit` prop to `SurfGraph`
  - Fix `FLAT` condition for `HI` waveHeight in `favorites`

## 12.0.3

  - Use `<TrackableLink>` component on subscribe CTA links

## 12.0.0

  - Moved Leaflet css file import to themes

## 11.2.2

  - Fixed Leaflet Map Components not being supported in SSR Applications

## 11.2.0

  - Create Base Map component and components for displaying on said map
  - New `TrackableLink` component which wraps anchor tags (`<a/>`) in order to give segment calls time to fire before the anchor tag performs a redirect
  - Update dependencies
  - Fix linting errors

## 11.1.0

  - New `Input` component that handles errors and warnings, as well as any validation to go with password or email field types

## 11.0.1

 - Bring back `react-router-dom` as a production dependency, but make it loosely versioned (`^5.0.0`) in order to prevent build errors in apps
   that do not use react router

## 11.0.0

 - BREAKING CHANGE: Removed `SurflinePageContainer` and `CTA` exports
 - Feat: Use `@surfline/web-common@^7.0.0` in order to consume the new segment functions that do
   not rely on the redux store.
 - fix: Fixed `createAccessibleOnClick` to properly use `Enter` or `Space` as triggers for a `Button`
   as per the spec (https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Roles/button_role).

## 10.0.0

 - BREAKING CHANGE: `react-router-dom` has been moved to `devDependencies` and `peerDependencies`
   in order to prevent an issue where multiple instances of `react-router` are bundled/used, which
   creates an invariant error because the `Context` cannot be used across instances.

## 9.0.2

 - Add a default export for CTA component

## 9.0.0

- [BREAKING]: Removed the `dist` folder where the build output used to live. There is now an ESM bundle under `/esm` and a CJS bundle under `/cjs`. The CJS bundle is exported by the `index.js`
    - Any imports to `@surfline/quiver-react/dist/..` will now be broken. Use `import { Component } from '@surfline/quiver-react'` instead.
- [BREAKING]: Due to the way we are using rollup (instead of just babel) to create the bundles all components meant to be consumed by an outside application need to be exported in the `/src/index.js` file.
- [BREAKING]: Moved the following components from `src/..` into `src/CamPlayer` since they are not designed to be consumed externally:
    - CamTimeout
    - CamUpsell
    - CamNighttime
    - CamAdblockUpsell
    - CamRewindMessage
    - CamError
- [BREAKING]: Moved all Icon components into the `/src/icons` folder. They will all be exported from `/src/index` but some names may have changed
- Bug Fix: Fixed a bug where `this.state` was not defined and was causing a track event from firing when users clicked to view all search results
- Improvement: Added all top level components as exports
- Improvement: Moved all icons into `/src/icons`
- Improvement: Sorted all exports alphabetically by path in `src/index.js`
- Improvement: Use `@surfline/babel-preset-web`, `@surfline/eslint-config-web`
- Feature: Added [Babel Plugin Lodash](https://github.com/lodash/babel-plugin-lodash) to ensure that any lodash imports are properly modified to allow tree-shaking (This will be moved to `@surfline/babel-preset-web`)

## 8.6.0

- Add NewRelic, SegmentBrowser and SplitBrowser components

## 8.4.5

- Open the forecaster profile in a new tab (KBYG-2806)

## 8.0.7

- Add `ProgressBar` component

## 8.0.0

- [BREAKING]: Updated all `sl-` prefixed classnames that used to live in `@surfline/quiver-react` to properly be prefixed with `quiver-`.
  Anyone upgrading to this version will want to upgrade to the `@surfline/quiver-themes@6.0.0`
