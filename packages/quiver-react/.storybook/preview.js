// import './custom.css';
import '@surfline/quiver-themes/stylesheets/crossbrand.css';

// Spot Report Module
import '@surfline/quiver-themes/stylesheets/surfline/SpotReport.css';
// Forecast Graphs
import '@surfline/quiver-themes/stylesheets/surfline/ForecastGraphs.css';

import '@surfline/quiver-themes/stylesheets/surfline/AsyncDataContainer.css';
import '@surfline/quiver-themes/stylesheets/surfline/PillButton.css';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};
