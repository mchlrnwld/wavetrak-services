module.exports = {
  stories: ['../src/**/stories.mdx', '../src/**/stories.js'],
  addons: [
    '@storybook/addon-essentials',
    '@storybook/addon-actions',
    '@storybook/addon-links',
  ],
};
