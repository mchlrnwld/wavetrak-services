/* eslint-disable */
'use strict';

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./cjs/@surfline/quiver-react.min.js');
} else {
  module.exports = require('./cjs/@surfline/quiver-react.js');
}
