import fs from 'fs';
import path from 'path';
import chai, { expect } from 'chai';
import dirtyChai from 'dirty-chai';
import { parse } from 'scss-parser';
import createQueryWrapper from 'query-ast';
import packagejson from '../package.json';
import index from './';

chai.use(dirtyChai);

const readFile = file => new Promise((resolve, reject) => {
  fs.readFile(file, (err, data) => {
    if (err) return reject(err);
    return resolve(data);
  });
});

const scssPath = path.resolve(__dirname, '..', 'productcdn.scss');

const PRODUCT_CDN = `https://wa.cdn-surfline.com/quiver/${packagejson.version}`;

// This is here simply to catch regressions
describe('JS', () => {
  it('exports a constant pointing to an absolute path for the CDN assets', () => {
    expect(index).to.equal(PRODUCT_CDN);
  });
});

describe('SCSS', () => {
  it('exports a variable pointing to an absolute path for the CDN assets', async () => {
    const scssAST = parse(String(await readFile(scssPath)));
    const $ = createQueryWrapper(scssAST);

    const productCDNDeclaration = $(n => n.node.value === 'sl-product-cdn' && n.parent.node.type === 'property')
      .first()
      .parent()
      .parent();

    const cdnPath = productCDNDeclaration
      .children(n => n.node.type === 'value')
      .children(n => n.node.type === 'string_single')
      .first()
      .value();

    expect(cdnPath).to.equal(PRODUCT_CDN);
  });
});
