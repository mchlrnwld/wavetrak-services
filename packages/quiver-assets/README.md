# @surfline/quiver-assets

## Installation

```sh
npm install --save @surfline/quiver-assets
```

## Usage

`quiver-assets` default exports an absolute path to the current version of the assets in our product CDN in JavaScript and SCSS.

```JavaScript
import PRODUCT_CDN from '@surfline/quiver-assets';

const weatherIconUrl = `${PRODUCT_CDN}/weathericons/CLEAR_SKIES_NO_RAIN.svg`;
```

```SCSS
@import '~@surfline/quiver-assets/productcdn.scss';

$font-path: '#{$sl-product-cdn}/fonts';

@font-face {
  font-family: 'Futura-Dem';
  src: url('#{$font-path}/futura-demi.eot');
  src: url('#{$font-path}/futura-demi.eot?#iefix') format('embedded-opentype'),
       url('#{$font-path}/futura-demi.woff2') format('woff2'),
       url('#{$font-path}/futura-demi.woff') format('woff'),
       url('#{$font-path}/futura-demi.ttf') format('truetype'),
       url('#{$font-path}/futura-demi.svg#youworkforthem') format('svg');
  font-weight: normal;
  font-style: normal;
}
```

## CDN Assets Directory Tree
The `assets` directory served out of our product CDN.

```
.
├-- backgrounds
|   ├-- 404.jpg
|   ├-- 404-mobile-image.jpg
|   ├-- 500.jpg
|   └-- 500-mobile-image.jpg
├-- fonts
|   ├-- futura-demi.eot
|   ├-- futura-demi.svg
|   ├-- futura-demi.ttf
|   ├-- futura-demi.woff
|   └-- futura-demi.woff2
|   ├-- linear-sans
|   |   ├-- linear-sans-black.eot
|   |   ├-- linear-sans-black.woff
|   |   ├-- linear-sans-black.woff2
|   |   ├-- linear-sans-black-italic.eot
|   |   ├-- linear-sans-black-italic.woff
|   |   ├-- linear-sans-black-italic.woff2
|   |   ├-- linear-sans-bold.eot
|   |   ├-- linear-sans-bold.woff
|   |   ├-- linear-sans-bold.woff2
|   |   ├-- linear-sans-bold-italic.eot
|   |   ├-- linear-sans-bold-italic.woff
|   |   ├-- linear-sans-bold-italic.woff2
|   |   ├-- linear-sans-heavy.eot
|   |   ├-- linear-sans-heavy.woff
|   |   ├-- linear-sans-heavy.woff2
|   |   ├-- linear-sans-heavy-italic.eot
|   |   ├-- linear-sans-heavy-italic.woff
|   |   ├-- linear-sans-heavy-italic.woff2
|   |   ├-- linear-sans-italic.eot
|   |   ├-- linear-sans-italic.woff
|   |   ├-- linear-sans-italic.woff2
|   |   ├-- linear-sans-regular.eot
|   |   ├-- linear-sans-regular.woff
|   |   ├-- linear-sans-regular.woff2
|   |   ├-- linear-sans-semibold.eot
|   |   ├-- linear-sans-semibold.woff
|   |   ├-- linear-sans-semibold.woff2
|   |   ├-- linear-sans-semibold-italic.eot
|   |   ├-- linear-sans-semibold-italic.woff
|   |   └-- linear-sans-semibold-italic.woff2
├-- logos
|   ├-- crossbrand.svg
|   └-- surfline.svg
├-- scripts
|   └-- jwplayer.js
|   └-- readymag.js
    └-- ad
    |   └-- ads.js
    |   └-- adframe.js
    |   └-- adsbox.js
    |   └-- doubleclick.js
    |   └-- test-ads-doubleclick.js
    |   └-- test-ads-doubleclick.min.js
└-- weathericons
    ├-- CLEAR_FREEZING_RAIN.png
    ├-- CLEAR_FREEZING_RAIN.svg
    ├-- CLEAR_HAIL.png
    ├-- CLEAR_HAIL.svg
    ├-- CLEAR_NO_RAIN.png
    ├-- CLEAR_NO_RAIN.svg
    ├-- CLEAR_RAIN.png
    ├-- CLEAR_RAIN.svg
    ├-- CLEAR_SNOW.png
    ├-- CLEAR_SNOW.svg
    ├-- MOSTLY_CLEAR_FREEZING_RAIN.png
    ├-- MOSTLY_CLEAR_FREEZING_RAIN.svg
    ├-- MOSTLY_CLEAR_HAIL.png
    ├-- MOSTLY_CLEAR_HAIL.svg
    ├-- MOSTLY_CLEAR_NO_RAIN.png
    ├-- MOSTLY_CLEAR_NO_RAIN.svg
    ├-- MOSTLY_CLEAR_RAIN.png
    ├-- MOSTLY_CLEAR_RAIN.svg
    ├-- MOSTLY_CLEAR_SNOW.png
    ├-- MOSTLY_CLEAR_SNOW.svg
    ├-- MOSTLY_CLOUDY_FREEZING_RAIN.png
    ├-- MOSTLY_CLOUDY_FREEZING_RAIN.svg
    ├-- MOSTLY_CLOUDY_HAIL.png
    ├-- MOSTLY_CLOUDY_HAIL.svg
    ├-- MOSTLY_CLOUDY_NO_RAIN.png
    ├-- MOSTLY_CLOUDY_NO_RAIN.svg
    ├-- MOSTLY_CLOUDY_RAIN.png
    ├-- MOSTLY_CLOUDY_RAIN.svg
    ├-- MOSTLY_CLOUDY_SNOW.png
    ├-- MOSTLY_CLOUDY_SNOW.svg
    ├-- NIGHT CLEAR_FREEZING_RAIN.png
    ├-- NIGHT CLEAR_FREEZING_RAIN.svg
    ├-- NIGHT_CLEAR_HAIL.png
    ├-- NIGHT_CLEAR_HAIL.svg
    ├-- NIGHT_CLEAR_NO_RAIN.png
    ├-- NIGHT_CLEAR_NO_RAIN.svg
    ├-- NIGHT_CLEAR_RAIN.png
    ├-- NIGHT_CLEAR_RAIN.svg
    ├-- NIGHT_CLEAR_SNOW.png
    ├-- NIGHT_CLEAR_SNOW.svg
    ├-- NIGHT_MOSTLY_CLEAR_FREEZING_RAIN.png
    ├-- NIGHT_MOSTLY_CLEAR_FREEZING_RAIN.svg
    ├-- NIGHT_MOSTLY_CLEAR_HAIL.png
    ├-- NIGHT_MOSTLY_CLEAR_HAIL.svg
    ├-- NIGHT_MOSTLY_CLEAR_NO_RAIN.png
    ├-- NIGHT_MOSTLY_CLEAR_NO_RAIN.svg
    ├-- NIGHT_MOSTLY_CLEAR_RAIN.png
    ├-- NIGHT_MOSTLY_CLEAR_RAIN.svg
    ├-- NIGHT_MOSTLY_CLEAR_SNOW.png
    ├-- NIGHT_MOSTLY_CLEAR_SNOW.svg
    ├-- NIGHT_MOSTLY_CLOUDY_FREEZING_RAIN.png
    ├-- NIGHT_MOSTLY_CLOUDY_FREEZING_RAIN.svg
    ├-- NIGHT_MOSTLY_CLOUDY_HAIL.png
    ├-- NIGHT_MOSTLY_CLOUDY_HAIL.svg
    ├-- NIGHT_MOSTLY_CLOUDY_NO_RAIN.png
    ├-- NIGHT_MOSTLY_CLOUDY_NO_RAIN.svg
    ├-- NIGHT_MOSTLY_CLOUDY_RAIN.png
    ├-- NIGHT_MOSTLY_CLOUDY_RAIN.svg
    ├-- NIGHT_MOSTLY_CLOUDY_SNOW.png
    ├-- NIGHT_MOSTLY_CLOUDY_SNOW.svg
    ├-- NIGHT_OVERCAST_HAIL.png
    ├-- NIGHT_OVERCAST_HAIL.svg
    ├-- NIGHT_OVERCAST_HAIL_FREEZING_RAIN.png
    ├-- NIGHT_OVERCAST_HAIL_FREEZING_RAIN.svg
    ├-- NIGHT_OVERCAST_NO_RAIN.png
    ├-- NIGHT_OVERCAST_NO_RAIN.svg
    ├-- NIGHT_OVERCAST_RAIN.png
    ├-- NIGHT_OVERCAST_RAIN.svg
    ├-- NIGHT_OVERCAST_SNOW.png
    ├-- NIGHT_OVERCAST_SNOW.svg
    ├-- NIGHT_PARTLY_CLOUDY_FREEZING_RAIN.png
    ├-- NIGHT_PARTLY_CLOUDY_FREEZING_RAIN.svg
    ├-- NIGHT_PARTLY_CLOUDY_HAIL.png
    ├-- NIGHT_PARTLY_CLOUDY_HAIL.svg
    ├-- NIGHT_PARTLY_CLOUDY_NO_RAIN.png
    ├-- NIGHT_PARTLY_CLOUDY_NO_RAIN.svg
    ├-- NIGHT_PARTLY_CLOUDY_RAIN.png
    ├-- NIGHT_PARTLY_CLOUDY_RAIN.svg
    ├-- NIGHT_PARTLY_CLOUDY_SNOW.png
    ├-- NIGHT_PARTLY_CLOUDY_SNOW.svg
    ├-- OVERCAST_HAIL.png
    ├-- OVERCAST_HAIL.svg
    ├-- OVERCAST_HAIL_FREEZING_RAIN.png
    ├-- OVERCAST_HAIL_FREEZING_RAIN.svg
    ├-- OVERCAST_NO_RAIN.png
    ├-- OVERCAST_NO_RAIN.svg
    ├-- OVERCAST_RAIN.png
    ├-- OVERCAST_RAIN.svg
    ├-- OVERCAST_SNOW.png
    ├-- OVERCAST_SNOW.svg
    ├-- PARTLY_CLOUDY_FREEZING_RAIN.png
    ├-- PARTLY_CLOUDY_FREEZING_RAIN.svg
    ├-- PARTLY_CLOUDY_HAIL.png
    ├-- PARTLY_CLOUDY_HAIL.svg
    ├-- PARTLY_CLOUDY_NO_RAIN.png
    ├-- PARTLY_CLOUDY_NO_RAIN.svg
    ├-- PARTLY_CLOUDY_RAIN.png
    ├-- PARTLY_CLOUDY_RAIN.svg
    ├-- PARTLY_CLOUDY_SNOW.png
    └-- PARTLY_CLOUDY_SNOW.svg
```
