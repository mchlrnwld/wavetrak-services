# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## <small>0.20.5 (2022-02-11)</small>

* fix: Update bg images for freemium cta ([79fca9a](https://github.com/Surfline/wavetrak-services/commit/79fca9a))





## <small>0.20.4 (2022-02-08)</small>

* fix: update freemium bg images ([a12b3dc](https://github.com/Surfline/wavetrak-services/commit/a12b3dc))





## <small>0.20.3 (2022-01-26)</small>

* fix: add freemium assets ([be9d393](https://github.com/Surfline/wavetrak-services/commit/be9d393))





## <small>0.20.2 (2021-12-22)</small>

* fix: sa-4056 Fix failing test case ([948ad88](https://github.com/Surfline/wavetrak-services/commit/948ad88))
* SA-4056 - Add assets for Forecast Report CTA (#6547) ([0a20112](https://github.com/Surfline/wavetrak-services/commit/0a20112)), closes [#6547](https://github.com/Surfline/wavetrak-services/issues/6547)
* SA-4056 - quiver-assets - update index versions to 0.20.1 (#6548) ([b4e2c87](https://github.com/Surfline/wavetrak-services/commit/b4e2c87)), closes [#6548](https://github.com/Surfline/wavetrak-services/issues/6548)





## <small>0.20.1 (2021-12-22)</small>

- fix: change index file to reference 0.20.0 version ([20ba8e7](https://github.com/Surfline/wavetrak-services/commit/20ba8e7))

## 0.20.0 (2021-12-22)

- feat: Add assets for Forecast Report CTA ([f349392](https://github.com/Surfline/wavetrak-services/commit/f349392))

### [0.19.6](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-assets@0.19.4...@surfline/quiver-assets@0.19.6) (2021-10-22)

### Bug Fixes

- **deploy:** simplify s3 deployment ([3cc3d39](https://github.com/Surfline/wavetrak-services/commit/3cc3d391b55f10e2500556982cba5f2abf72e932))

### [0.19.5](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-assets@0.19.4...@surfline/quiver-assets@0.19.5) (2021-10-22)

### Bug Fixes

- **deploy:** resolve postpublish s3 upload script ([4734968](https://github.com/Surfline/wavetrak-services/commit/4734968f325823cbf17ffd88197384eccadfd636))

### [0.19.4](https://github.com/Surfline/wavetrak-services/compare/@surfline/quiver-assets@0.19.3...@surfline/quiver-assets@0.19.4) (2021-10-22)

**Note:** Version bump only for package @surfline/quiver-assets

### 0.19.3 (2021-10-21)

**Note:** Version bump only for package @surfline/quiver-assets

## <small>0.19.2 (2021-10-19)</small>

- fix(quiver-assets): ensure build files exist when publishing ([689dedf](https://github.com/Surfline/wavetrak-services/commit/689dedf))
- WP-92: Migrate quiver/quiver-assets to packages/quiver-assets (#5571) ([3049bd4](https://github.com/Surfline/wavetrak-services/commit/3049bd4)), closes [#5571](https://github.com/Surfline/wavetrak-services/issues/5571)

## <small>0.19.1 (2021-10-15)</small>

- fix(quiver-assets): fix cdn version reference ([b5ee9bb](https://github.com/Surfline/wavetrak-services/commit/b5ee9bb))
- chore(quiver-assets): migrate quiver/quiver-assets to packages/quiver-assets ([1d9b0bd](https://github.com/Surfline/wavetrak-services/commit/1d9b0bd))

# Quiver Assets Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## 0.19.0

- Add linear-sans fonts in WP-185

## 0.18.5

- Add several ad-detection files to update ad-blocker detection in WP-86
- Move ad-detection files to live in `/scripts/ad/` instead of `/scripts/` in WP-86

## 0.18.4

- Add `doubleclick.js` file to update ad-blocker detection in WP-86

## 0.18.3

- feat: update node version from 8 to 10 and Yarn usage cleanup WP-129
- Add `eslint:fix` to package scripts for autofixing lint errors

## 0.17.2

- Update registry url to use `jfrog.io` instead of `artifactoryonline.com`

## 0.17.1

- Fix an incorrect font path for futura surfline fonts

## 0.17.0

- Add swell event background pattern

## 0.16.2

- Add Account logos and background images

## 0.16.1

- Add `/lottie` assets folder
- Add `/lottie/WaveLoader.json` animation file

## 0.16.0

- [POTENTIAL BREAKING CHANGE] Update the font paths to use different import variables so that they are not overriding each other

## 0.15.1

- Remove Subheading copy from the 404 error page

## 0.15.0

- Remove FBI Copy from the 500 Error Page

## 0.13.0

- Added unsupported browser HTML error page

## 0.12.5

- Added ping plugin with new 8.8.2 target and key to jwplayer.js v8.8.2

## 0.12.4

- Add jwplayer.js v8.8.2

## 0.5.0 (8/30/17)

- Add jwplayer.js v7.12.2
