# `wavetrak-prettier-config`

## Description

A Wavetrak opinionated Prettier config whose use is strongly recommended.

## Usage

(As explained in the prettier docs, the usage is pretty simple.)[https://prettier.io/docs/en/configuration.html#sharing-configurations]

Sharing a Prettier configuration is simple: just publish a module that exports a configuration object, say @company/prettier-config, and reference it in your .prettierc file:

```
"@surfline/prettier-config"
```

## Publishing NPM package

Within the `/packages/` directory we publish NPM packages using Lerna.

A step by step guide for publishing NPM packages in the monorepo can be found [here](../../README.md#npm-packages).