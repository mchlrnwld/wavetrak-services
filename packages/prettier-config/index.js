module.exports = {
  printWidth: 100,
  semi: true,
  bracketSpacing: true,
  singleQuote: true,
  tabWidth: 2,
  trailingComma: 'all',
  useTabs: false,
  overrides: [
    {
      files: ['.eslintrc', '.prettierrc', '.babelrc', '.nycrc'],
      options: {
        parser: 'json',
      },
    },
  ],
};
