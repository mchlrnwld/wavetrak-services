# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [0.2.8](https://github.com/Surfline/wavetrak-services/compare/@surfline/prettier-config@0.2.7...@surfline/prettier-config@0.2.8) (2021-12-09)

**Note:** Version bump only for package @surfline/prettier-config





## <small>0.2.7 (2021-04-15)</small>

* chore: bump dependencies ([be504f7](https://github.com/Surfline/wavetrak-services/commit/be504f7))





## <small>0.2.6 (2020-11-04)</small>

* PS-000 - Replace artifactoryonline.com references with jfrog.io. (#3664) ([21430cd](https://github.com/Surfline/wavetrak-services/commit/21430cd)), closes [#3664](https://github.com/Surfline/wavetrak-services/issues/3664)





## <small>0.2.5 (2020-04-23)</small>

* chore: Add Prettier 2.0.0 peer dependency ([02139e1](https://github.com/Surfline/wavetrak-services/commit/02139e1))
* fix: Remove top level parser in order to allow parser inference ([c68bc29](https://github.com/Surfline/wavetrak-services/commit/c68bc29))





## [0.2.4](https://github.com/Surfline/wavetrak-services/compare/@surfline/prettier-config@0.2.3...@surfline/prettier-config@0.2.4) (2020-03-18)


### Bug Fixes

* Create a tagging script for Lerna ([9812c76](https://github.com/Surfline/wavetrak-services/commit/9812c76d618b9f54ba34403cf2d026fb983ea856))
* Fix Prettier config `trailingComma` setting conflicting with ESLint ([9338711](https://github.com/Surfline/wavetrak-services/commit/93387119302d7e9c58d90deb86430182a8804a1b))




## 0.2.3 (2020-03-11)

**Note:** Version bump only for package @surfline/prettier-config





## 0.2.2 (2020-03-09)

**Note:** Version bump only for package @surfline/prettier-config





## 0.2.1 (2020-03-04)

**Note:** Version bump only for package @surfline/prettier-config





# 0.2.0 (2020-03-04)


### Features

* Add Typescript support to prettier config ([bdc03c8](https://github.com/Surfline/wavetrak-services/commit/bdc03c86ae533c404185cb1ec33dcea113b6e04c))





# 0.1.0 (2020-02-25)


### Bug Fixes

* fix package versions ([b52885d](https://github.com/Surfline/wavetrak-services/commit/b52885dc221b7c16fc541da550b58457e687eed2))
* README fixes, fix wrong repo link ([49a4329](https://github.com/Surfline/wavetrak-services/commit/49a4329c15885385fc7c7ca5bdb0ace50f4ff0b7))


### Features

* Create Eslint Services Package ([f536e37](https://github.com/Surfline/wavetrak-services/commit/f536e373bcfa74ddc40d3a680b168e897d174441))
* Create prettier config package ([9bb7636](https://github.com/Surfline/wavetrak-services/commit/9bb7636454e80d06b6243c729f347ac4b38e08d4))
