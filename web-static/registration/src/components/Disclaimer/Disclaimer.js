import React from 'react';
import PropTypes from 'prop-types';
import getConfig from '../../config';
import brandToString from '../../utils/brandToString';

const config = getConfig();

/**
 * A component that displays the Terms of Use and Privacy Policy links.
 */
const Disclaimer = ({ migratedBrandName, buttonText }) => (
  <p className="disclaimer">
    {migratedBrandName
      ? `By clicking on "${buttonText}" you authorise ${migratedBrandName} and ` +
        `${brandToString()} and their payment processing providers to hold, use and ` +
        `disclose the credit card information collected for your ${migratedBrandName} subscription ` +
        `for the purpose of creating your ${brandToString()} subscription. You also ` +
        `confirm you have read and agree to ${brandToString()}'s `
      : 'By clicking on Create Account, you agree to our '}
    <DisclaimerLink url={config.privacyPolicy} text="Privacy policy" />
    {' and '}
    <DisclaimerLink url={config.termsConditions} text="Terms of Use" />.
  </p>
);

const DisclaimerLink = ({ text, url }) => (
  <a className="disclaimer-link" href={url} target="_blank" rel="noopener noreferrer">
    {text}
  </a>
);

Disclaimer.propTypes = {
  buttonText: PropTypes.string,
  migratedBrandName: PropTypes.string,
};

Disclaimer.defaultProps = {
  buttonText: 'Set New Password',
  migratedBrandName: null,
};

DisclaimerLink.propTypes = {
  text: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default Disclaimer;
