import PropTypes from 'prop-types';

/**
 * @description The MobileRailCTA only display on mobile view. This component renders a heading
 * for the mobile page that is placed above the form.
 *
 * @param {string} Background Image for the Rail CTA
 */
const MobileRailCTA = ({ backgroundImage }) => (
  <div className="mobile-rail-cta" style={{ backgroundImage }} />
);

MobileRailCTA.propTypes = {
  backgroundImage: PropTypes.string,
};

MobileRailCTA.defaultProps = {
  backgroundImage: null,
};

export default MobileRailCTA;
