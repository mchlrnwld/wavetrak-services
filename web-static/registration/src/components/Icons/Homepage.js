const Homepage = () => (
  <svg
    width="40px"
    height="32px"
    viewBox="0 0 40 32"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <g id="Desktop" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g
        id="create-account-mobile"
        transform="translate(-831.000000, -392.000000)"
        fillRule="nonzero"
      >
        <g id="homepage" transform="translate(831.000000, 392.000000)">
          <rect
            id="Rectangle"
            stroke="#FFFFFF"
            strokeWidth="1.5"
            x="5.75"
            y="4.75"
            width="16.5"
            height="8.5"
            rx="1"
          />
          <rect id="Rectangle" fill="#22D736" x="27" y="4" width="8" height="24" rx="1" />
          <rect
            id="Rectangle"
            stroke="#FFFFFF"
            strokeWidth="1.5"
            x="0.75"
            y="0.75"
            width="38.5"
            height="30.5"
            rx="2"
          />
          <rect id="Rectangle" fill="#FFFFFF" x="5" y="17" width="18" height="1.5" rx="0.75" />
          <rect
            id="Rectangle-Copy-4"
            fill="#FFFFFF"
            x="5"
            y="21.5"
            width="18"
            height="1.5"
            rx="0.75"
          />
          <rect
            id="Rectangle-Copy-5"
            fill="#FFFFFF"
            x="5"
            y="26"
            width="18"
            height="1.5"
            rx="0.75"
          />
        </g>
      </g>
    </g>
  </svg>
);

export default Homepage;
