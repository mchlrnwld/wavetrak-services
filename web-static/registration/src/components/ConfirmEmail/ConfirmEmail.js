import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { Input, Button, Alert } from '@surfline/quiver-react';
import RegistrationHelmet from '../RegistrationHelmet';
import { confirmEmail } from '../../store/actions/confirmEmail';
import {
  getConfirmEmailError,
  isSubmitSuccess,
  isSubmitting,
} from '../../store/selectors/confirmEmail';
import { getAccessToken } from '../../store/selectors/auth';
import performRedirect from '../../utils/performRedirect';
import en from '../../international/translations/en';

const ConfirmEmail = () => {
  const dispatch = useDispatch();

  const accessToken = useSelector(getAccessToken);
  const submitting = useSelector(isSubmitting);
  const submitSuccess = useSelector(isSubmitSuccess);
  const error = useSelector(getConfirmEmailError);

  const [email, setEmail] = useState(undefined);

  const { register, handleSubmit, errors } = useForm();

  const onConfirmEmailSubmit = (values) => {
    if (!errors.email) {
      dispatch(confirmEmail(values.email));
    }
  };

  useEffect(() => {
    if (accessToken) {
      performRedirect();
    }
  }, [accessToken]);

  if (submitSuccess) return window.location.assign('/confirm-email/email-sent');

  return (
    <div className="confirm_email-wrapper">
      <RegistrationHelmet title={en.confirmEmail.title} />
      <h1 className="confirm_email__title">{en.confirmEmail.title}</h1>
      <p className="confirm_email__title-subtitle">{en.confirmEmail.subtitle}</p>
      {error ? <Alert type="error">{error}</Alert> : null}
      <form onSubmit={handleSubmit(onConfirmEmailSubmit)}>
        <Input
          email
          value={email}
          label="EMAIL ADDRESS"
          id="email"
          parentWidth
          required
          error={!!errors.email}
          message={errors?.email?.message}
          input={{
            name: 'email',
            ref: register({
              required: true,
              onChange: (event) => setEmail(event.target.value),
            }),
          }}
          style={{ marginBottom: '20px' }}
        />
        <Button style={{ width: '100%' }} loading={submitting} button={{ type: 'submit' }}>
          REQUEST RESET
        </Button>
      </form>
    </div>
  );
};

export default ConfirmEmail;
