import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Alert } from '@surfline/quiver-react';
import { getVerifyEmailStatus } from '../../store/selectors/verifyEmail';

import SurflineLogo from '../Icons/SurflineLogo';

const VerifyEmailLayout = ({ children }) => {
  const { success, error } = useSelector(getVerifyEmailStatus);
  return (
    <div className="sl-verify-email-wrapper">
      <div className="sl-verify-email-header">
        <SurflineLogo />
        {!success && error ? <Alert type="error">{error}</Alert> : null}
        {success ? (
          <h5 className="sl-verify-email-header__title">You&apos;ve verified your email!</h5>
        ) : null}
      </div>
      <div className="sl-verify-email-content">{children}</div>
      <div className="sl-verify-email-footer">
        <a href="/" className="sl-verify-email-footer__continue">
          Or Continue to Surfline
        </a>
      </div>
    </div>
  );
};

VerifyEmailLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default VerifyEmailLayout;
