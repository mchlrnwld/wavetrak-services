import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import { Input, Button, Alert } from '@surfline/quiver-react';
import { trackNavigatedToPage } from '@surfline/web-common';
import { Cookies } from 'react-cookie';
import en from '../../international/translations/en';
import getConfig from '../../config';
import RegistrationHelmet from '../RegistrationHelmet';
import { requestReset } from '../../store/actions/forgotPassword';
import {
  getForgotPasswordSubmitting,
  getRequestResetError,
  getRequestResetSuccess,
} from '../../store/selectors/forgotPassword';

const config = getConfig();
const cookie = new Cookies();

const ForgotPassword = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const [email, setEmail] = useState('');

  const accessToken = cookie.get('access_token');
  const submitting = useSelector(getForgotPasswordSubmitting);
  const resetError = useSelector(getRequestResetError);
  const resetSuccess = useSelector(getRequestResetSuccess);

  const handleChange = (event) => {
    event.preventDefault();
    setEmail(event.target.value);
  };

  useEffect(() => {
    if (resetSuccess) {
      router.push('/forgot-password/email-sent');
    }
  }, [resetSuccess, router]);

  useEffect(() => {
    if (!accessToken && !submitting) {
      trackNavigatedToPage('Authentication', 'Forgot Password');
    }
  }, [accessToken, submitting]);

  if (accessToken) window.location.assign(config.redirectUrl);
  return (
    <div className="forgot-password-wrapper">
      <RegistrationHelmet title={en.forgot_password.enter_email.meta_title} />
      <h1 className="forgot-password__title">{en.sign_in.form.forgot_password}</h1>
      {resetError ? (
        <Alert style={{ marginBottom: '20px', textAlign: 'center' }} type="error">
          You need to specify a valid email
        </Alert>
      ) : null}
      <form
        onSubmit={(event) => {
          event.preventDefault();
          dispatch(requestReset(email));
        }}
      >
        <Input
          defaultValue={email}
          label="EMAIL ADDRESS"
          id="email"
          type="email"
          input={{ name: 'email', onChange: handleChange, required: true }}
        />
        <Button style={{ width: '100%' }} loading={submitting} button={{ type: 'submit' }}>
          REQUEST RESET
        </Button>
        <p>
          <a
            className="forgot-password__need-help"
            href={config.needHelpUrl}
            target="_blank"
            rel="noopener noreferrer"
          >
            {en.forgot_password.enter_email.need_help_message}
          </a>
        </p>
      </form>
    </div>
  );
};

export default ForgotPassword;
