import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';
import { trackClickedSubscribeCTA, trackNavigatedToPage } from '@surfline/web-common';

import VerifyEmailCTA from '../VerifyEmailCTA/VerifyEmailCTA';
import { verifyEmail } from '../../store/actions/verifyEmail';
import getConfig from '../../config';

const config = getConfig();

const VerifyEmail = () => {
  const dispatch = useDispatch();
  const {
    query: { verification, promotion_id: promotionId },
  } = useRouter();

  useEffect(() => {
    trackNavigatedToPage('Email Verification', {}, 'Account');
    dispatch(verifyEmail(verification));
  }, [dispatch, verification]);

  const onPressCTA = () => {
    trackClickedSubscribeCTA({
      location: 'Email Verification Success',
      withOffer: !!promotionId,
    });

    // Using delays to guarantee the segment event gets fired before page is changed
    if (promotionId) {
      setTimeout(() => {
        window.location.assign(`${config.redirectUrl}/${promotionId}`);
      }, 300);
    } else {
      setTimeout(() => {
        window.location.assign(config.redirectUrl);
      }, 300);
    }
  };

  return <VerifyEmailCTA onClickCTA={onPressCTA} promotionId={promotionId} />;
};

export default VerifyEmail;
