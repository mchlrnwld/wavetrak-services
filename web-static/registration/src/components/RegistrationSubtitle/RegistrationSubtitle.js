import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import Link from 'next/link';
import { toggleRegistrationForm } from '../../store/actions/interactions';

const RegistrationSubtitle = ({ showLogin, location }) => {
  const dispatch = useDispatch();
  const doToggleForm = () => {
    dispatch(toggleRegistrationForm(showLogin, location));
  };
  return showLogin ? (
    <p className="registration-subtitle">
      Need an account?
      <Link href="/create-account">
        <button type="button" className="register-link__action" onClick={doToggleForm}>
          {' Sign Up'}
        </button>
      </Link>
    </p>
  ) : (
    <p className="registration-subtitle">
      Already have an account?
      <Link href="/sign-in">
        <button type="button" className="register-link__action" onClick={doToggleForm}>
          {' Sign In'}
        </button>
      </Link>
    </p>
  );
};

RegistrationSubtitle.propTypes = {
  showLogin: PropTypes.bool,
  location: PropTypes.string,
};

RegistrationSubtitle.defaultProps = {
  showLogin: false,
  location: null,
};

export default RegistrationSubtitle;
