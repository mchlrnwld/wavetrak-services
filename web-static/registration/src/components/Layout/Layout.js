import PropTypes from 'prop-types';
import { RailLogo, MobileRailCTA } from '..';
import en from '../../international/translations/en';
import RegistrationCTA from '../RegistrationCTA';

export const LeftRail = ({ children }) => (
  <div className="left-rail">
    <RailLogo />
    {children}
  </div>
);

export const RightRail = ({ children }) => <div className="right-rail">{children}</div>;

const Layout = ({ children }) => {
  const ctaCopy = {
    ...en.rail_cta,
    ...en.premiumFeatures,
  };
  return ctaCopy ? (
    <div className="registration-wrapper">
      <MobileRailCTA copy={ctaCopy} />
      <LeftRail>
        <div className="registration-wrapper__content">{children}</div>
      </LeftRail>
      <RightRail>
        <RegistrationCTA copy={ctaCopy} />
      </RightRail>
    </div>
  ) : null;
};

LeftRail.propTypes = {
  children: PropTypes.node.isRequired,
};
RightRail.propTypes = {
  children: PropTypes.node.isRequired,
};
Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
