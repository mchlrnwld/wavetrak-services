/* eslint-disable */
import getConfig from '../../config';

const config = getConfig();

const RailLogo = () => {
  const { redirectUrl } = config;

  return <a href={redirectUrl} className="rail-logo" />;
};
export default RailLogo;
