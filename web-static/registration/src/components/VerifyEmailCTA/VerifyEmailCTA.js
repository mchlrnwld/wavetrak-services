import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@surfline/quiver-react';
import { Check } from '../Icons';

const getHeaderText = (promo) => (promo ? 'Limited Time Offer' : '15-day Free Trial');

const getButtonText = (promo) => (promo ? 'REDEEM OFFER' : 'START FREE TRIAL');

const VerifyEmailCTA = ({ promotionId, onClickCTA }) => {
  return (
    <div className="sl-verify-email-cta">
      <h4 className="sl-verify-email-cta__title">Go Premium</h4>
      <h5 className="sl-verify-email-cta__header">{getHeaderText(promotionId)}</h5>
      {promotionId ? (
        <div className="sl-verify-email-cta__discount">
          <h2 className="sl-verify-email-cta__discount-header">2 MONTHS FREE</h2>
          <p className="sl-verify-email-cta__discount-text">12 months for the price of 10</p>
        </div>
      ) : null}
      <p className="sl-verify-email-cta__benefits-header">PREMIUM BENEFITS</p>
      <div className="sl-verify-email-cta__benefits-matrix">
        <ul>
          <li className="sl-verify-email-cta__premium-benefit">
            <Check />
            <p className="sl-verify-email-cta__premium-benefit-text">Ad-Free and Premium Cams</p>
          </li>
          <li className="sl-verify-email-cta__premium-benefit">
            <Check />
            <p className="sl-verify-email-cta__premium-benefit-text">Trusted Forecast Team</p>
          </li>
          <li className="sl-verify-email-cta__premium-benefit">
            <Check />
            <p className="sl-verify-email-cta__premium-benefit-text">Exclusive Forecast Tools</p>
          </li>
          <li className="sl-verify-email-cta__premium-benefit">
            <Check />
            <p className="sl-verify-email-cta__premium-benefit-text">Cam Rewind</p>
          </li>
        </ul>
      </div>
      <div className="sl-verify-email-cta__button-container">
        <Button onClick={onClickCTA} style={{ width: '90%' }}>
          {getButtonText(promotionId)}
        </Button>
      </div>
    </div>
  );
};

VerifyEmailCTA.propTypes = {
  promotionId: PropTypes.string,
  onClickCTA: PropTypes.func.isRequired,
};

VerifyEmailCTA.defaultProps = {
  promotionId: null,
};

export default VerifyEmailCTA;
