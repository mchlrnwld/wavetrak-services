import PropTypes from 'prop-types';
import RegistrationHelmet from '../RegistrationHelmet';
import en from '../../international/translations/en';

const EmailSent = (props) => {
  const { isForgotPassword } = props;
  const { metaTitle, metaDescription, notify, info, title } = isForgotPassword
    ? en.forgot_password.emailSent
    : en.confirmEmail.emailSent;

  return (
    <div className="forgot-password__card">
      <RegistrationHelmet title={metaTitle} metaDescription={metaDescription} />
      <h4 className="enterCode_container_title">{title}</h4>
      <p className="enterCode_container_notify_message">{notify}</p>
      <p className="enterCode_container_info_message">{info}</p>
    </div>
  );
};

EmailSent.propTypes = {
  isForgotPassword: PropTypes.bool,
};

EmailSent.defaultProps = {
  isForgotPassword: true,
};

export default EmailSent;
