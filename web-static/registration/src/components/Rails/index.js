import PropTypes from 'prop-types';
import RailLogo from '../RailLogo';

export const LeftRail = ({ children }) => (
  <div className="left-rail">
    <RailLogo />
    {children}
  </div>
);

export const RightRail = ({ children }) => <div className="right-rail">{children}</div>;

LeftRail.propTypes = {
  children: PropTypes.node.isRequired,
};

RightRail.propTypes = {
  children: PropTypes.node.isRequired,
};
