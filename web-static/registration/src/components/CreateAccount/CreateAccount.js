import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { CreateAccountForm, FacebookLogin } from '@surfline/quiver-react';
import { trackNavigatedToPage } from '@surfline/web-common';
import { isFormSubmitting } from '../../store/selectors/interactions';
import en from '../../international/translations/en';
import { getUserError, getFacebookLoading } from '../../store/selectors/user';
import { register, loginWithFacebook } from '../../store/actions/user';
import getConfig from '../../config';
import RegistrationSubtitle from '../RegistrationSubtitle';
import OrBreak from '../OrBreak';
import RegistrationHelmet from '../RegistrationHelmet';
import Disclaimer from '../Disclaimer';

import performRedirect from '../../utils/performRedirect';
import { getAccessToken } from '../../store/selectors/auth';

const config = getConfig();

const CreateAccount = () => {
  const dispatch = useDispatch();

  const accessToken = useSelector(getAccessToken);
  const submitting = useSelector(isFormSubmitting);
  const error = useSelector(getUserError);
  const facebookSubmitting = useSelector(getFacebookLoading);

  const onCreateSubmit = (data) => {
    const values = {
      ...data,
      location: 'register',
    };
    dispatch(register(values));
  };

  useEffect(() => {
    if (accessToken) {
      performRedirect();
    }
  }, [accessToken]);

  useEffect(() => {
    if (!accessToken && !submitting) {
      trackNavigatedToPage('Create Account', {}, 'Authentication');
    }
  }, [accessToken, submitting]);

  return (
    <div className="create-account-wrapper">
      <RegistrationHelmet
        title={en.create.meta_title}
        description={en.create.meta_description}
        showLogin={false}
      />
      <h1 className="create-account__title">{en.create.title}</h1>
      <RegistrationSubtitle showLogin={false} location="register" />
      <div className="create-account__facebook--wrapper">
        <FacebookLogin
          onClick={() => dispatch(loginWithFacebook('register'))}
          loading={facebookSubmitting}
        />
      </div>
      <OrBreak />
      <CreateAccountForm
        onSubmit={onCreateSubmit}
        loading={submitting}
        error={error}
        receivePromotionsLabel={en.create.form.receivePromotions_label(config.brand)}
      />
      <Disclaimer />
    </div>
  );
};

export default CreateAccount;
