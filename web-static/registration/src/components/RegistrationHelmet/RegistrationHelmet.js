import Head from 'next/head';
import PropTypes from 'prop-types';
import en from '../../international/translations/en';

const defaultMeta = [
  { name: 'description', content: en.sign_in.meta_description },
  { property: 'fb:app_id', content: '218714858155230' },
  { property: 'fb:page_id', content: '255110695512' },
  { property: 'og:site_name', content: 'Surfline' },
  {
    name: 'apple-itunes-app',
    content:
      'app-id=393782096,affiliate-data=at=1000lb9Z&ct=surfline-website-smart-banner&pt=261378',
  },
];

const links = [
  { rel: 'manifest', href: '/manifest.json' },
  { rel: 'publisher', href: 'https://plus.google.com/+Surfline' },
  {
    rel: 'apple-touch-icon',
    href: 'https://wa.cdn-surfline.com/quiver/0.4.0/apple/surfline-apple-touch-icon.png',
  },
];

const RegistrationHelemt = ({ title, metaDescription }) => {
  const description = metaDescription ? { name: 'description', content: metaDescription } : null;
  if (description) {
    defaultMeta.push(description);
  }

  return (
    <Head>
      <title>{title}</title>
      {defaultMeta.map((meta) => (
        <meta {...meta} />
      ))}
      {links.map((link) => (
        <link {...link} />
      ))}
    </Head>
  );
};

RegistrationHelemt.propTypes = {
  metaDescription: PropTypes.string.isRequired,
  title: PropTypes.string,
};

RegistrationHelemt.defaultProps = {
  title: null,
};

export default RegistrationHelemt;
