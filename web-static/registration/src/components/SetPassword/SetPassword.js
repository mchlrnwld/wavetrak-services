import { useState, useEffect } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { Input, Button, Alert } from '@surfline/quiver-react';
import { useRouter } from 'next/router';
import { Cookies } from 'react-cookie';
import { trackNavigatedToPage } from '@surfline/web-common';

import getConfig from '../../config';
import en from '../../international/translations/en';
import RegistrationHelmet from '../RegistrationHelmet';
import { setPassword as setPasswordAction, validate } from '../../store/actions/forgotPassword';
import { fetchShowTerms } from '../../store/actions/user';
import {
  getUserResetCode,
  getForgotPasswordError,
  getForgotPasswordUserID,
  getForgotPasswordSubmitting,
  getUserHashValidatedError,
  getPasswordResetSuccess,
} from '../../store/selectors/forgotPassword';
import { getShowTerms } from '../../store/selectors/user';
import Disclaimer from '../Disclaimer';
import Loading from '../Loading';

import { identifyUser } from '../../utils/identifyUser';

const config = getConfig();
const cookie = new Cookies();

const SetPassword = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const {
    query: { hash },
  } = router;

  const userId = useSelector(getForgotPasswordUserID, shallowEqual);
  const resetCode = useSelector(getUserResetCode);
  const passwordFail = useSelector(getForgotPasswordError);
  const passwordResetSuccess = useSelector(getPasswordResetSuccess);
  const { showTerms, migratedBrandName } = useSelector(getShowTerms);

  const doShowTerms = config.company === 'sl' && showTerms && migratedBrandName;

  const accessToken = cookie.get('access_token');
  const validateHashError = useSelector(getUserHashValidatedError);

  useEffect(() => {
    if (userId) {
      identifyUser(userId);
      trackNavigatedToPage('Authentication', 'Set New Password');
    }
  }, [userId, hash, dispatch]);

  useEffect(() => {
    const doFetchShowTerms = async (id) => {
      await dispatch(fetchShowTerms(id));
    };
    if (userId) {
      doFetchShowTerms(userId);
    }
  }, [userId, dispatch]);

  useEffect(() => {
    if (validateHashError) {
      router.push('/forgot-password');
    }
  }, [validateHashError, router]);

  useEffect(() => {
    if (passwordResetSuccess) {
      // doShowTerms signifies a new account password setup
      const url = doShowTerms ? '/onboarding' : config.redirectUrl;
      window.location.assign(url);
    }
  }, [passwordResetSuccess, doShowTerms]);

  useEffect(() => {
    dispatch(validate(hash));
  }, [dispatch, hash]);

  const [password, setPassword] = useState(null);
  const [passwordConfirm, setPasswordConfirm] = useState(null);

  const submitting = useSelector(getForgotPasswordSubmitting);
  const buttonText = doShowTerms ? 'Confirm Account Updates' : 'Set New Password';

  if (accessToken) window.location.assign(config.redirectUrl);
  if (showTerms === undefined)
    return (
      <div className="set-password-wrapper">
        <Loading />
      </div>
    );
  return (
    <div className="set-password-wrapper">
      <RegistrationHelmet title={en.forgot_password.set_password.meta_title} />

      {validateHashError ? (
        <Alert style={{ marginBottom: '20px', textAlign: 'center' }} type="error">
          Invalid Hash
        </Alert>
      ) : null}
      <h1 className="set-password__title">
        {doShowTerms
          ? 'Setup Account and Premium Subscription'
          : en.forgot_password.set_password.title}
      </h1>
      <form
        onSubmit={(event) => {
          event.preventDefault();
          dispatch(setPasswordAction(password, passwordConfirm, userId, resetCode));
        }}
      >
        <Input
          value={password}
          label="NEW PASSWORD"
          id="password"
          parentWidth
          type="password"
          password
          input={{
            name: 'password',
            onChange: (event) => setPassword(event.target.value),
            required: true,
            minLength: 6,
          }}
          style={{ marginBottom: '20px' }}
        />
        <Input
          value={passwordConfirm}
          label="CONFIRM NEW PASSWORD"
          id="passwordConfirm"
          parentWidth
          type="password"
          password
          input={{
            name: 'passwordConfirm',
            onChange: (event) => setPasswordConfirm(event.target.value),
            required: true,
            minLength: 6,
          }}
          style={{ marginBottom: '20px' }}
        />

        {passwordFail ? (
          <Alert style={{ marginVertical: '8px', textAlign: 'center' }} type="error">
            {passwordFail}
          </Alert>
        ) : null}

        <Button style={{ width: '100%' }} loading={submitting} button={{ type: 'submit' }}>
          {buttonText}
        </Button>
        {doShowTerms ? (
          <Disclaimer migratedBrandName={migratedBrandName} buttonText={buttonText} />
        ) : null}
      </form>
    </div>
  );
};

export default SetPassword;
