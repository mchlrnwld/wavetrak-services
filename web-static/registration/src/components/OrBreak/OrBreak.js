const OrBreak = () => (
  <div className="or">
    <span className="or__span">OR</span>
  </div>
);

export default OrBreak;
