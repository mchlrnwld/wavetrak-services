import { Spinner } from '@surfline/quiver-react';

export default () => {
  return (
    <div className="sl-registration-loading">
      <Spinner />
    </div>
  );
};
