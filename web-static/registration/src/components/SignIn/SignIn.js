import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { LoginForm, FacebookLogin } from '@surfline/quiver-react';
import { trackNavigatedToPage } from '@surfline/web-common';
import { isFormSubmitting } from '../../store/selectors/interactions';
import en from '../../international/translations/en';
import { getUserError, getFacebookLoading, getConfirmEmail } from '../../store/selectors/user';
import { login, loginWithFacebook } from '../../store/actions/user';
import RegistrationSubtitle from '../RegistrationSubtitle';
import OrBreak from '../OrBreak';
import RegistrationHelmet from '../RegistrationHelmet';

import performRedirect from '../../utils/performRedirect';
import { getAccessToken } from '../../store/selectors/auth';

const SignIn = () => {
  const dispatch = useDispatch();

  const accessToken = useSelector(getAccessToken);
  const submitting = useSelector(isFormSubmitting);
  const error = useSelector(getUserError);
  const facebookSubmitting = useSelector(getFacebookLoading);
  const confirmEmail = useSelector(getConfirmEmail);
  useEffect(() => {
    if (!accessToken && !submitting) {
      trackNavigatedToPage('Authentication', 'Sign In');
    }
  }, [accessToken, submitting]);

  useEffect(() => {
    if (accessToken) {
      performRedirect();
    }
  }, [accessToken]);

  const onLoginSubmit = (data) => {
    const values = {
      ...data,
      location: 'register',
    };
    dispatch(login(values));
  };

  if (confirmEmail) return window.location.assign('/confirm-email');

  return (
    <div className="sign-in-wrapper">
      <RegistrationHelmet
        title={en.sign_in.meta_title}
        metaDescription={en.sign_in.meta_description}
      />
      <h1 className="sign-in__title">{en.sign_in.title}</h1>
      <RegistrationSubtitle showLogin location="signin" />
      <div className="sign-in__facebook--wrapper">
        <FacebookLogin
          onClick={() => dispatch(loginWithFacebook('register'))}
          loading={facebookSubmitting}
        />
      </div>
      <OrBreak />
      <LoginForm onSubmit={onLoginSubmit} loading={submitting} error={error} />
    </div>
  );
};

export default SignIn;
