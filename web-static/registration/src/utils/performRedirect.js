import { Cookies } from 'react-cookie';
import urlCheck from './urlCheck';
import getConfig from '../config';

const config = getConfig();
const cookie = new Cookies();

/**
 * @function performRedirect
 * @return {void}
 */
const performRedirect = () => {
  // Set the fallback redirect as the base homepage
  let redirectLocation = config.redirectUrl;

  const storedRedirect = sessionStorage.getItem('redirectUrl');
  const cookieRedirect = cookie.get('redirectUrl');
  if (storedRedirect && urlCheck(storedRedirect, redirectLocation)) {
    redirectLocation = storedRedirect;
  } else if (cookieRedirect && urlCheck(cookieRedirect, redirectLocation)) {
    redirectLocation = cookieRedirect;
  }
  window.location.assign(redirectLocation);
};

export default performRedirect;
