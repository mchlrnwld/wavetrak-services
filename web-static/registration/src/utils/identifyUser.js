/**
 * @param {string} userId
 * @returns {void}
 */
export const identifyUser = (userId) => window?.analytics?.identify(userId);
