import { Cookies } from 'react-cookie';
import config from '../config';

const cookie = new Cookies();

/**
 * @function setRedirectCookie
 * @param {string} redirectUrl
 * @return {string}
 */
const setRedirectCookie = (redirectUrl) => {
  // eslint-disable-next-line
  const domainName = config.company === 'fs' ? '.fishtrack.com' : location.hostname;
  cookie.set('redirectUrl', redirectUrl, {
    path: '/',
    maxAge: 1800,
    expires: new Date(new Date().getTime() + 1800 * 1000),
    domain: domainName,
    secure: false,
  });

  return { redirectUrl };
};

export default setRedirectCookie;
