import getConfig from '../config';

const config = getConfig();

/**
 * @function brandToString
 * @param {'sl' | 'bw' | 'fs'} [company]
 */
const brandToString = (company = null) => {
  const brand = company || config.company;
  if (brand === 'sl') return 'Surfline';
  if (brand === 'bw') return 'Buoyweather';
  if (brand === 'fs') return 'FishTrack';
  return 'Wavetrak';
};

export default brandToString;
