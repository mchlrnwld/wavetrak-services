import queryString from 'query-string';
import LogRocket from 'logrocket';
import getConfig from '../config';

const config = getConfig();

/**
 * @function logRocketEnabled
 * @returns {boolean}
 * @description Checks if LogRocket is enabled
 */
export const logRocketEnabled = () => {
  const { logrocket } = queryString.parse(window?.location?.search);
  const isSignIn = window?.location?.href.indexOf('sign-in') !== -1;
  return logrocket === 'on' && isSignIn;
};

export const setupLogRocket = () => {
  LogRocket.init(config.logRocket, {
    network: {
      requestSanitizer: (request) => {
        // if the url contains 'trusted/token'
        if (request.url.toLowerCase().indexOf('trusted/token') !== -1 && request?.body) {
          // scrub out the body
          const reqBody = JSON.parse(request.body);
          delete reqBody.password; // Remove password so that we don't log in LogRocket
          request.body = reqBody;
        }
        return request;
      },
    },
  });
};
