import { setAuthCookies } from '@surfline/web-common';
import getConfig from '../config';

const config = getConfig();

/**
 * @function setLoginCookie
 * @param {string} accessToken
 * @param {string} refreshToken
 * @param {number} expiresIn
 */
const setLoginCookie = (accessToken, refreshToken, staySignedIn = false, expiresIn = 2592000) => {
  const domainName = config.company === 'fs' ? '.fishtrack.com' : window.location.hostname;

  return setAuthCookies(accessToken, refreshToken, expiresIn, staySignedIn, domainName);
};

export default setLoginCookie;
