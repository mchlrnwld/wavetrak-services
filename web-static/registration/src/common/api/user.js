import fetch from '../utils/fetch';

export const getUser = (accessToken) => fetch('user', accessToken, { method: 'GET' });

export const postUser = (isShortLived, user) =>
  fetch(`user?isShortLived=${isShortLived}`, null, {
    method: 'POST',
    body: JSON.stringify(user),
  });

export const verifyEmail = (hash, brand) =>
  fetch('user/verify-email', null, {
    method: 'POST',
    body: JSON.stringify({ hash, brand }),
  });

export const resendEmailVerification = (accessToken, brand) =>
  fetch('/user/resend-email-verification', accessToken, {
    method: 'POST',
    body: JSON.stringify({ brand }),
  });

export const validateEmail = (email) =>
  fetch(`/user/validateEmail?email=${email}`, null, { method: 'GET' });

export const authorise = (accessToken) =>
  fetch(`trusted/authorise?access_token=${accessToken}`, null, { method: 'GET' });

export const getShowTerms = (userId) =>
  fetch(`/user/show-terms?id=${userId}`, null, { method: 'GET' });
