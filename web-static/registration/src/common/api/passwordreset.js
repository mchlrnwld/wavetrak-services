import fetch from '../utils/fetch';
import getConfig from '../../config';

const config = getConfig();

export const forgotPassword = (email) =>
  fetch(`/password-reset?email=${encodeURIComponent(email)}&brand=${config.company}`);

export const validateHash = (hash, brand) =>
  fetch('/password-reset/validate-hash', null, {
    method: 'POST',
    body: JSON.stringify({ hash, brand }),
  });

export const resetPassword = (password, userId, resetCode) =>
  fetch('/password-reset', null, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      Authorization: `Basic ${window.btoa(`${config.clientId}:${config.clientSecret}`)}`,
      'Content-Type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify({
      password,
      userId,
      resetCode,
      brand: config.company,
    }),
  });
