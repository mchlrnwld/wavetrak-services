import fetch from '../utils/fetch';

export const getLocation = (accessToken) =>
  fetch(`geo-target/region`, accessToken, { method: 'GET' });
