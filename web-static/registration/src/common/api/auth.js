import axios from 'axios';
import LogRocket from 'logrocket';
import { nrAddPageAction } from '@surfline/web-common';
import getConfig from '../../config';
import { logRocketEnabled } from '../../utils/logrocket';

const config = getConfig();

export const generateTokenOld = (isShortLived, userInfo) => {
  const authorizationString = `Basic ${window.btoa(`${config.clientId}:${config.clientSecret}`)}`;
  return fetch(`/trusted/token?isShortLived=${isShortLived}`, null, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      Authorization: authorizationString,
      'Content-Type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify(userInfo),
  });
};

export const generateToken = async (isShortLived, userInfo) => {
  const authorizationString = `Basic ${window.btoa(`${config.clientId}:${config.clientSecret}`)}`;
  let url = `${config.servicesEndpoint}/trusted/token?isShortLived=${isShortLived}`;
  if (logRocketEnabled()) {
    url = `${config.servicesEndpoint}/trusted/token?logger=on&isShortLived=${isShortLived}`;
  }
  const payload = {
    ...userInfo,
    authorizationString,
  };
  try {
    const { data } = await axios({
      method: 'post',
      url,
      headers: {
        accept: 'application/json',
        Authorization: authorizationString,
        'Content-Type': 'application/json',
        credentials: 'same-origin',
      },
      data: payload,
    });
    return data;
  } catch (err) {
    LogRocket.identify(userInfo?.username, {
      email: userInfo?.username,
      status: err?.response?.status,
    });
    LogRocket.captureException(err, {
      tags: {
        email: userInfo?.username,
      },
    });
    const errorObj = err?.response?.data;
    if (err?.response) {
      let lrRecordingURL;
      LogRocket.getSessionURL((sessionURL) => {
        lrRecordingURL = sessionURL;
      });
      nrAddPageAction('axios generateToken Error', {
        errorObj: JSON.stringify(errorObj),
        email: userInfo?.username,
        authorizationString,
        url,
        status: err?.response?.status,
        headers: JSON.stringify(err?.response?.headers),
        errorJson: JSON.stringify(err.toJSON()),
        lrRecordingURL,
      });
    }
    throw errorObj;
  }
};

/**
 * @description Api handler for authorising an access_token
 * @param {string} accessToken
 * @returns {object} Response from the authorise endpoint
 */
export const authorise = async (accessToken) => {
  const bearerAuth = `Bearer ${accessToken}`;
  const url = `${config.servicesEndpoint}/trusted/authorise`;
  const { data } = await axios({
    method: 'get',
    url,
    headers: {
      Authorization: bearerAuth,
    },
  });
  return data;
};
