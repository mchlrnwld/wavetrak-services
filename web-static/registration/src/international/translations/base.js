import { Wave } from '../../components/Icons';
import brandToString from '../../utils/brandToString';
import getConfig from '../../config';

const config = getConfig();

export default {
  index: {
    browserUpdateTextBefore:
      'You are using an outdated browser. Surfline no longer supports this browser. Please ',
    browserUpdateLink: 'upgrade your browser',
    browserUpdateTextAfter: ' to improve your experience.',
  },

  app: {
    sign_in_link: 'Sign in',
    create_account_link: 'Create account',
    brand_icons: 'One account for everything.',
  },

  duplicateCard: {
    header: 'This card has been used for a free trial before',
    description: (amount, symbol) =>
      `This card is not eligible for a free trial. Please use another card to start your trial, or continue to Go Premium. You will be charged ${symbol}${amount} today.`,
    goPremium: 'GO PREMIUM',
    expirationDate: (date) => `EXPIRES ${date}`,
    goBack: 'GO BACK',
  },

  giftCards: {
    rightRailHeader: `${brandToString(config.company)} Premium Gift Cards`,
    learn_more_link: '',
    select: {
      headerText: `Give the Gift of ${brandToString(config.company)} Premium`,
      subHeaderText: `Stoke out a friend or loved one with a digital ${brandToString(
        config.company,
      )} Premium gift card.`,
    },
    success: {
      headerText: 'Success',
      subHeaderText: (email) =>
        `A receipt and confirmation has been sent to ${email}. Please let the recipient know to check their inbox as well as their Promotion or Spam folders to ensure they receive their gift card.`,
    },
  },
  rail_cta: {
    sl: {
      header: 'Premium Benefits',
      mobileHeader: 'Go Premium',
      mobileSubtitle: 'Learn More About Surfline Premium',
      MobileIcon: Wave,
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
    },
    bw: {
      mobileHeader: 'Go Premium',
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
      header: 'Buoyweather',
    },
    fs: {
      mobileHeader: 'Go Premium',
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
      header: 'FishTrack',
    },
  },

  confirmEmail: {
    metaTitle: 'Confirm Email - Surfline',
    title: 'Confirm Email',
    subtitle: 'Please confirm your email address to setup a new password for Surfline.',
    emailSent: {
      metaTitle: 'Confirm Email - Surfline',
      info:
        'If you do not receive an email shortly, check your junk folder or contact us. Once you recieve the email please click the link to setup your password.',
      title: 'Email Sent',
    },
  },

  create: {
    title: 'Create account',
    onboarding_title: "You're almost there!",
    onboarding_subtitle:
      'Save your settings by creating an account below. Already have an account?',
    onboarding_alert_nofav: 'Please create an account or sign in to save favorites.',
    meta_title: 'Create Account - Surfline',
    meta_description:
      'Don’t miss another good surf – create an account with Surfline to gain instant access to Surfline.',

    error_email_in_use: 'Email in use, ',
    error_form_error: 'Login failed!',

    form: {
      already_have_account: 'Already have an account?',
      sign_in_link: ' Sign In',
      discount_applied: 'Your discount has been applied.',
      label_first: 'First Name',
      label_last: 'Last Name',
      label_email: 'Email',
      email_error_message: 'Email in use, ',
      email_error_message_link: 'sign in?',
      label_password: 'Password',
      create_account_button: 'Create account',
      staySignedIn_label: 'Keep me signed in',
      receivePromotions_label: (brand) =>
        `Send me product and marketing updates from ${brandToString(brand)} and its partners`,
      disclaimer_text_start: 'By clicking "Create Account" you agree to Surfline ',
      terms_of_use_link_text: 'Terms of use',
      disclaimer_and: ' and ',
      privacy_policy_link_text: 'Privacy policy',
      password_helper_text: 'Minimum 6 characters',
    },
  },

  create_funnel: {
    meta_title: 'Create Your Premium Account - Surfline',
    title_no_trial: 'Join Surfline Premium',
  },

  sign_in: {
    title: 'Sign in',
    onboarding_title: "You're almost there!",
    onboarding_subtitle: 'Save your settings by signing in below. Need an account?',
    meta_title: 'Sign In - Surfline',
    meta_description:
      'Sign in to Surfline and start surfing more often by customizing your Surfline experience.',

    error_email_does_not_exist: 'No account with this email. ',
    error_form_error: 'Incorrect email / password combination',
    internal_server_error: 'Internal Server Error',

    form: {
      need_account: 'Need an account?',
      create_account_link: ' Sign Up',
      label_email: 'Email',
      email_error_message: 'No account with this email. ',
      email_error_message_link: 'Create account?',
      label_password: 'Password',
      forgot_password: 'Forgot Password?',
      sign_in_button: 'Sign in',
      staySignedIn_label: 'Keep me signed in',
      submit_success: '',
    },
    continueform: {
      title: 'It appears too many people are using your account right now',
      info:
        'Surfline has a limit on the total number of browsers and devices that you may be logged into at once.',
      continue_button: 'CONTINUE',
      go_back_link: 'CANCEL',
      recover_button: 'RECOVER ACCOUNT',
      faq_text: 'Why am I seeing this?',
    },
  },

  sign_in_funnel: {
    title: 'Sign in to start free trial',
    title_no_trial: 'Sign in to join Surfline Premium',
    meta_title: 'Sign In to Go Premium - Surfline',
    meta_description:
      "Don't miss another good surf - sign in with your Surfline account to go Premium.",
  },

  forgot_password: {
    message: 'Not all those who wander are lost',
    emailSent: {
      title: 'Email Sent',
      metaTitle: 'Email Sent - Surfline',
      notify: 'Emails can take up to 10 minutes to arrive.',
      info:
        'If you do not receive an email, check your junk folder or contact us. Once you receive the email please click the link to reset your password.',
      internal_server_error: 'Internal Server Error',
      passcode_error: 'Invalid Pass Code.',
      form_error: 'Forgot Password failed!',
    },
    enter_email: {
      title: 'Forgot Password?',
      meta_title: 'Forgot Password - Surfline',
      internal_server_error: 'Internal Server Error',
      email_error: 'Email not found',
      form_error: 'Login failed!',
      need_help_message: 'Still need help?',
    },
    set_password: {
      title: 'New Password',
      meta_title: 'Create New Password - Surfline',
    },
    email_form: {
      label_email: 'Email Address',
      request_reset_button: 'Request Reset',
    },
    passcode_form: {
      label_passcode: 'Enter 4-Digit Code',
      reset_password_button: 'Submit',
    },
    reset_password_form: {
      label_new_password: 'New Password',
      label_new_password_confirm: 'Confirm New Password',
      set_password_button: 'Set New Password',
      password_helper_text: 'Minimum 6 characters',
    },
  },

  edit_profile: {
    meta_title: {
      sl: 'Edit Profile - Surfline',
      bw: 'Edit Profile - Buoyweather',
      fs: 'Edit Profile - FishTrack',
    },
    title: 'Profile',
    form: {
      save_button: 'Save',
      label_first: 'First Name',
      label_last: 'Last Name',
      label_email: 'Email',
    },
    emailVerificationMessage: (
      email,
      company,
    ) => `Check ${email} for a verification email from ${company},
      and follow the link to verify your account.`,
  },

  edit_settings: {
    meta_title: {
      sl: 'Settings - Surfline',
      bw: 'Settings - Buoyweather',
      fs: 'Settings - FishTrack',
    },
    title: 'Settings',
    form: {
      save_button: 'Save',
      label_windSpeed: 'Wind Speed',
      label_surfHeight: 'Surf/Swell Height',
      label_tideHeight: 'Tide Height',
      label_temperature: 'Temperature',
    },
  },

  favorites: {
    meta_title: {
      sl: 'Favorites - Surfline',
      bw: 'Favorites - Buoyweather',
      fs: 'Favorites - FishTrack',
    },
    title: 'Surfline Favorites',
    message: 'Add some favorites',
    no_favs_title: 'Welcome',
    no_favs_button: 'View Map',
    no_favs_message:
      'Use the map or search bar to find your favorite spots, then press star on the report page to save Cams and Forecasts to your Favorites.',
  },

  change_password: {
    meta_title: {
      sl: 'Change Password - Surfline',
      bw: 'Change Password - Buoyweather',
      fs: 'Change Password - FishTrack',
    },
    title: 'Change Password',
    form: {
      save_button: 'Update Password',
      label_current: 'Current Password',
      label_new: 'New Password',
      label_confirm: 'Confirm Password',
    },
  },
  not_found: {
    title: 'Page Not Found',
    subheading: 'Yo, Johnny! See you in the next life!',
    paragraph_top: "The link you've followed may be broken, or the page may have been removed.",
    paragraph_bottom_before: 'Go ahead... go shred back to the ',
    paragraph_bottom_link: 'homepage',
    paragraph_bottom_after: ' for surf reports, forecasts and surf news.',
    button_text: 'Back to Surfline.com',
  },

  five_hundred: {
    title: 'Internal Server Error',
    subheading: 'Bodhi! This is your wakeup call I am an F... B... I... agent!',
    paragraph_top:
      "Looks like we're having a few issues here, thanks for noticing. Rest assured we've been notified of this problem.",
    paragraph_bottom_before: 'Checkout the ',
    paragraph_bottom_link: 'homepage',
    paragraph_bottom_after: ' for surf reports, forecasts and surf news.',
    button_text: 'Back to Surfline.com',
  },

  utils: {
    email: 'Not a valid email address format',
    required: 'Required',
    min_length_start: 'Minimum ',
    min_length_end: ' characters',
    max_length_start: 'Maximum ',
    max_length_end: ' characters',
    match: 'Do not match',
    integer: 'Must be a number',
    five_hundred_four: 'Server Timed Out. Please Try Again.',
    five_hundred: 'Internal Server Error',
    free_trial_text: 'Start your 15-day free trial of Surfline Premium',
    inactive_promotion: (brandName) =>
      `This promotion has expired, but you can still sign-up for ${brandName} Premium`,
  },

  facebook: {
    facebook_sign_in: 'Sign in with Facebook',
  },

  or_break: {
    or: 'or',
  },
  account: {
    header: {
      go_back_text: (brand) => `< Back to ${brand}`,
      go_back_link: (referrer, brand) => {
        if (referrer.includes(brand.toLowerCase())) {
          return referrer;
        }
        return config[`${brand.toLowerCase()}Home`];
      },
    },
    overview: {
      meta_title: 'Account Overview - Surfline',
      benefits_card: {
        message_calc: (days) =>
          `We're stoked that you have been a Premium member for ${days} days.`,
        message: "We're stoked to have you as a Premium member.",
      },
      promo_card: {
        sl_premium: {
          title: "You're Premium!",
          message: 'One of the benefits of being Premium is access to discounts on surf gear.',
          link_text: 'See Savings',
        },
        free: {
          title: 'Enjoy ad-free cams',
          message: '',
          link_text: 'Join Premium Now',
        },
        expired: {
          title: 'Enjoy ad-free cams',
          message: '',
          link_text: 'Join Premium Now',
        },
        expiring: {
          title: "Don't miss out",
          message: "Ads get in the way, don't miss out on the next set.",
          link_text: 'Stay Premium',
        },
      },
      info_card: {
        premium: {
          title: "You're Premium!",
          message: {
            sl: "We're stoked to have you as a Surfline Premium member.",
            bw: 'Thanks for being a Buoyweather Premium member.',
            fs: "We're glad to have you as a FishTrack Premium member.",
          },
          link_text: 'Premium Benefits',
          link_text_secondary: 'View More Discounts',
        },
        free: {
          title: 'Upgrade to Premium!',
          message: {
            sl: 'Enjoy ad-free cams, 16-day forecasts, discount codes and much more.',
            bw: 'Get point-based 16-day wind, wave and tide forecasts.',
            fs: 'View the latest sea surface temperature and chlorophyll charts.',
          },
          link_text: 'Add Premium',
          link_text_secondary: '',
        },
        expiring: {
          title: "Don't miss out",
          message: {
            sl: 'Keep your access to ad-free cams, 16-day forecasts and much more.',
            bw: 'Keep your access to point-based 16-day wind, wave and tide forecasts.',
            fs: 'Keep your access to the latest sea surface temperature and chlorophyll charts.',
          },
          link_text: 'Stay Premium',
          link_text_secondary: '',
        },
        discount: {
          title: 'Save 15% off surf gear',
          message:
            'Surfline Store powered by evo - UP TO 15% OFF.  Use the code below when you check out.',
          link_text: 'Get Code',
          link_text_secondary: 'More Discounts',
        },
        vip: {
          title: "You're a VIP Member!",
          message: {
            sl: 'Please enjoy your Premium-level access to 16-day forecasts and much more.',
            bw: 'Please enjoy your Premium-level access to 16-day wind, wave and tide forecasts.',
            fs:
              'Please enjoy your Premium-level access to the latest sea surface temperature and chlorophyll charts.',
          },
          link_text: 'Premium Benefits',
          link_text_secondary: 'View More Discounts',
        },
      },
      statusCard: {
        expired: {
          title: 'Your Premium memebership has expired.',
          message: () =>
            "You've lost access to ad-free HD cams and the world's most trusted forecast created by Surfline's team of experts.",
          link_text: 'Renew Premium',
        },
        delinquent: {
          message: (days, brand) =>
            `There was a billing error and your ${brand} Premium membership will expire in ${days} ${
              days === 1 ? 'day' : 'days'
            }.`,
          link_text: 'Update Payment Details',
        },
        monthly: {
          message: () => 'Switch to yearly payments and $84.',
          link_text: 'Go Yearly and Save $84',
        },
      },
      failedPaymentMessage: {
        title: 'payment failure',
        subtitle: (brand) => `Update payment details to keep ${brand} Premium`,
      },
    },
  },
};
