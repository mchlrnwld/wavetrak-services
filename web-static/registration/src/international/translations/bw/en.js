import base from '../base';
import { Buoy, Wind, SwellLines } from '../../../components/Icons';

export default {
  index: {
    ...base.index,
    browserUpdateTextBefore:
      'You are using an outdated browser. Buoyweather no longer supports this browser. Please ',
  },

  app: {
    ...base.app,
  },

  confirmEmail: {
    ...base.confirmEmail,
  },

  upgradeFunnel: {
    upgradeHeaderSubtitle: (symbol, monthlyPrice) =>
      `Buoyweather plans start at ${symbol}${monthlyPrice}/mo`,
  },

  duplicateCard: {
    ...base.duplicateCard,
  },

  premiumFeatures: {
    features: [
      {
        Icon: SwellLines,
        header: 'Marine Forecasts For Any Point',
        description: 'View accurate 16-day wind, wave and tide forecasts.',
      },
      {
        Icon: Wind,
        header: 'High-Resolution Wind Forecasts',
        description: 'Access a full collection of weather charts and forecasts.',
      },
      {
        Icon: Buoy,
        header: 'Pick Your Weather Window',
        description: 'Long-range forecasts help you stay safe on the water.',
      },
    ],
  },
  promotionFeatures: {
    promoCodeError:
      'Invalid Code. To receive this offer you must enter a valid code. If you do not have one, you can still upgrade to BuoyWeather Premium at www.buoyweather.com/upgrade/',
  },
  rail_cta: {
    ...base.rail_cta.bw,
  },

  create: {
    ...base.create,
    meta_title: 'Create Account - Buoyweather',
    meta_description:
      'Stay safe on the water – create an account with Buoyweather to gain instant access to point-based marine forecasts.',

    form: {
      ...base.create.form,
      disclaimer_text_start: 'By clicking "Create Account" you agree to Buoyweather ',
    },
  },

  create_funnel: {
    ...base.create_funnel,
    meta_title: 'Create Your Premium Account - Buoyweather',
    title_no_trial: 'Join Buoyweather Premium',
  },

  sign_in: {
    ...base.sign_in,
    meta_title: 'Sign In - Buoyweather',
    meta_description:
      'Sign in to Buoyweather and get point-based marine forecasts anywhere in the world',

    form: {
      ...base.sign_in.form,
      disclaimer_text_start:
        'If you click "Sign In with Facebook" and are not a Buoyweather user, you will be registered and you agree to Buoyweather ',
    },
  },

  sign_in_funnel: {
    ...base.sign_in_funnel,
    title_no_trial: 'Sign in to join<br/>Buoyweather Premium',
    meta_title: 'Sign In to Go Premium - Buoyweather',
    meta_description:
      'Get the latest 16-day marine forecasts – sign in with your Buoyweather account to go Premium.',
  },

  payment_success: {
    subTitle: 'Welcome to Buoyweather Premium!',
    message:
      "Thank you for joining the Buoyweather family.  You're now on your way to safer seas with the most trusted marine weather information.",
    linkText: 'Return to Buoyweather',
    linkAddress: 'https://buoyweather.com',
  },

  not_found: {
    ...base.not_found,
    button_text: 'Back to Buoyweather.com',
  },

  five_hundred: {
    ...base.five_hundred,
    button_text: 'Back to Buoyweather.com',
  },

  utils: {
    ...base.utils,
  },

  facebook: {
    ...base.facebook,
  },

  or_break: {
    ...base.or_break,
  },
  forgot_password: {
    ...base.forgot_password,
    emailSent: {
      ...base.forgot_password.emailSent,
      meta_title: 'Enter Code - Buoyweather',
    },
    enter_email: {
      ...base.forgot_password.enter_email,
      meta_title: 'Forgot Password - Buoyweather',
    },
    set_password: {
      ...base.forgot_password.set_password,
      meta_title: 'Create New Password - Buoyweather',
    },
    email_form: {
      ...base.forgot_password.email_form,
    },
    passcode_form: {
      ...base.forgot_password.passcode_form,
    },
    reset_password_form: {
      ...base.forgot_password.reset_password_form,
    },
  },
};
