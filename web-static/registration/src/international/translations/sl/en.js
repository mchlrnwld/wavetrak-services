/* Surfline text is default as defined in base.js */
import base from '../base';
import { Cams, Analysis, Charts, CamRewind } from '../../../components/Icons';

export default {
  index: {
    ...base.index,
  },

  app: {
    ...base.app,
  },

  confirmEmail: {
    ...base.confirmEmail,
  },

  upgradeFunnel: {
    upgradeHeaderSubtitle: (symbol, monthlyPrice) =>
      `Surfline plans start at ${symbol || '$'}${monthlyPrice || 7.99}/mo`,
  },

  duplicateCard: {
    ...base.duplicateCard,
  },

  premiumFeatures: {
    features: [
      {
        Icon: Cams,
        header: 'Ad-Free and Premium Cams',
        description: 'Watch live conditions ad-free -- and access exclusive, high-resolution cams.',
      },
      {
        Icon: Analysis,
        header: 'Trusted Forecast Team',
        description: 'Plan your next trip or session with Surfline’s expert forecast guidance.',
      },
      {
        Icon: Charts,
        header: 'Exclusive Forecast Tools',
        description: 'Access curated condition reports and proprietary surf, wind and buoy data.',
      },
      {
        Icon: CamRewind,
        header: 'Cam Rewind',
        description:
          'Relive an epic session by downloading your rides -- then share ‘em with friends.',
      },
    ],
  },

  promotionFeatures: {
    promoCodeError:
      'Invalid Code. To receive this offer you must enter a valid code. If you do not have one, you can still upgrade to Surfline Premium at www.surfline.com/upgrade/',
  },

  rail_cta: {
    ...base.rail_cta.sl,
  },

  create: {
    ...base.create,

    form: {
      ...base.create.form,
    },
  },

  create_funnel: {
    ...base.create_funnel,
  },

  sign_in: {
    ...base.sign_in,

    form: {
      ...base.sign_in.form,
    },
  },

  sign_in_funnel: {
    ...base.sign_in_funnel,
  },
  forgot_password: {
    ...base.forgot_password,
    emailSent: {
      ...base.forgot_password.emailSent,
    },
    enter_email: {
      ...base.forgot_password.enter_email,
    },
    set_password: {
      ...base.forgot_password.set_password,
    },
    email_form: {
      ...base.forgot_password.email_form,
    },
    passcode_form: {
      ...base.forgot_password.passcode_form,
    },
    reset_password_form: {
      ...base.forgot_password.reset_password_form,
    },
  },

  payment_success: {
    subTitle: 'Welcome to Surfline Premium!',
    message:
      "Thank you for joining the Surfline family.  You're now on your way to scoring better waves, more often.  See you in the water.",
    linkText: 'Return to Surfline',
    linkAddress: 'https://surfline.com',
  },
  not_found: {
    ...base.not_found,
  },

  five_hundred: {
    ...base.five_hundred,
  },

  utils: {
    ...base.utils,
  },

  facebook: {
    ...base.facebook,
  },

  or_break: {
    ...base.or_break,
  },
};
