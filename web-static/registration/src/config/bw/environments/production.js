module.exports = {
  cdnHost: 'https://wa.cdn-surfline.com/registration/bw/',
  staticHost: 'https://wa.cdn-surfline.com/registration/bw/static/',
  redirectUrl: 'http://buoyweather.com',
  benefitsRedirectUrl: 'http://benefits.buoyweather.com/7-day-forecast/',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: 'https://www.buoyweather.com/upgrade',
  accountLinkProtocol: 'https://www.',
  segment: 'CvbvzychXPuprLT31PUJjn2797aKS8Ez',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '562790161',
    accountID: '356245',
    trustKey: '356245',
    agentID: '562790161',
  },
  servicesEndpoint: 'https://services.surfline.com',
  stripe: {
    publishableKey:
      'pk_live_518MSMSJ4F5N75cpXxyXhiIIfbYxuWIna19yhOzriQDepMQfxMqDvwuNNhQXbKbOV5icocdsHvbp0gizP6L0wMCjn00dPOhEeeW',
  },
  termsConditions: 'http://www.buoyweather.com/info.jsp?id=21738',
  privacyPolicy: 'http://www.buoyweather.com/info.jsp?id=21737',
  needHelpUrl: 'http://www.buoyweather.com/info.jsp?id=21736',
  clientId: '5c59e826f0b6cb1ad02baf68',
  clientSecret: 'sk_4Z4sSmWpPrQsme6nsSuG',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'in20v2ebf6q1gh97ldcn1bp8ufdbr404e514',
  facebook: {
    appId: '218714858155230',
  },
  logRocket: 'h57nhx/sl_prod',
};
