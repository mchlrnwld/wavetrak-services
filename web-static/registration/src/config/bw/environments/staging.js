module.exports = {
  cdnHost: 'https://product-cdn.staging.surfline.com/registration/bw/',
  staticHost: 'https://product-cdn.staging.surfline.com/registration/bw/static/',
  redirectUrl: 'http://staging.buoyweather.com',
  benefitsRedirectUrl: 'http://benefits.buoyweather.com/7-day-forecast/',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: 'https://staging.buoyweather.com/upgrade',
  accountLinkProtocol: 'https://staging.',
  segment: 'wGHvUUloHAZXW8STk3qUxvKD0NswRkHu',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '17259048',
    accountID: '356245',
    trustKey: '356245',
    agentID: '562790161',
  },
  servicesEndpoint: 'https://services.staging.surfline.com',
  stripe: {
    publishableKey: 'pk_test_xBhwMLUOtBN1ORmA47xmqNGg',
  },
  termsConditions: 'http://staging.buoyweather.com/info.jsp?id=21738',
  privacyPolicy: 'http://staging.buoyweather.com/info.jsp?id=21737',
  needHelpUrl: 'http://staging.buoyweather.com/info.jsp?id=21736',
  clientId: '5c59e53d2d11e750809d572f',
  clientSecret: 'sk_dKW7DWJ4XMxxfP5BWDx8',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || '3prmuinjgigdvi7m5haujp8jtq1un82rc64n',
  facebook: {
    appId: '564041023804928',
  },
  logRocket: 'h57nhx/sl_sandbox',
};
