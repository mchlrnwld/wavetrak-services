module.exports = {
  cdnHost: 'https://wa.cdn-surfline.com/registration/sl/',
  staticHost: 'https://wa.cdn-surfline.com/registration/sl/static/',
  redirectUrl: 'https://www.surfline.com',
  benefitsRedirectUrl: 'https://www.surfline.com/premium-benefits',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  accountLinkProtocol: 'https://www.',
  segment: 'Se6IGuzcvUhLx55hLHL0RiXkS6oUTz8L',
  termsConditions: '/terms-of-use',
  privacyPolicy: '/privacy-policy',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '562790161',
    accountID: '356245',
    trustKey: '356245',
    agentID: '562790161',
  },
  servicesEndpoint: 'https://services.surfline.com',
  stripe: {
    publishableKey:
      'pk_live_518MSMSJ4F5N75cpXxyXhiIIfbYxuWIna19yhOzriQDepMQfxMqDvwuNNhQXbKbOV5icocdsHvbp0gizP6L0wMCjn00dPOhEeeW',
  },
  surflineHome: '/',
  clientId: '5c59e7c3f0b6cb1ad02baf66',
  clientSecret: 'sk_QqXJdn6Ny5sMRu27Amg3',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'in20v2ebf6q1gh97ldcn1bp8ufdbr404e514',
  facebook: {
    appId: '218714858155230',
  },
  needHelpUrl: 'https://support.surfline.com/hc/en-us/requests/new',
  logRocket: 'h57nhx/sl_prod',
};
