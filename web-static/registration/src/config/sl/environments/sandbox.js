module.exports = {
  cdnHost: 'https://product-cdn.sandbox.surfline.com/registration/sl/',
  staticHost: 'https://product-cdn.sandbox.surfline.com/registration/sl/static/',
  redirectUrl: 'https://sandbox.surfline.com',
  benefitsRedirectUrl: 'https://sandbox.surfline.com/premium-benefits',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: 'https://sandbox.surfline.com/upgrade',
  segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
  termsConditions: '/terms-of-use',
  privacyPolicy: '/privacy-policy',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '16905555',
    accountID: '356245',
    trustKey: '356245',
    agentID: '562790161',
  },
  servicesEndpoint: 'https://services.sandbox.surfline.com',
  stripe: {
    publishableKey: 'pk_test_Meb7MPzMWgbBYwU3t6gmdHGt',
  },
  surflineHome: '/',
  clientId: '5bff1583ffda1e1eb8d35c4b',
  clientSecret: 'sk_Hmxh7RJEJgJnZeaNF3Kx',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
  facebook: {
    appId: '564041023804928',
  },
  needHelpUrl: 'https://support.surfline.com/hc/en-us/requests/new',
  logRocket: 'h57nhx/sl_sandbox',
};
