module.exports = {
  cdnHost: 'https://product-cdn.staging.surfline.com/registration/fs/',
  staticHost: 'https://product-cdn.staging.surfline.com/registration/fs/static/',
  redirectUrl: 'http://staging.fishtrack.com',
  benefitsRedirectUrl: 'http://benefits.fishtrack.com/latest-sst-imagery/',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: 'https://staging.fishtrack.com/upgrade',
  accountLinkProtocol: 'https://staging.',
  segment: 'wcxCswdPqynOfwHMDhKE4xCVSLF8knV0',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '17259048',
    accountID: '356245',
    trustKey: '356245',
    agentID: '562790161',
  },
  servicesEndpoint: 'https://services.staging.surfline.com',
  stripe: {
    publishableKey: 'pk_test_xBhwMLUOtBN1ORmA47xmqNGg',
  },
  termsConditions: 'http://www.fishtrack.com/terms-and-conditions/',
  privacyPolicy: 'http://www.fishtrack.com/privacy-policy/',
  needHelpUrl: 'http://www.fishtrack.com/contact-us/',
  clientId: '5c59e4fb2d11e750809d572e',
  clientSecret: 'sk_h63qPbkRrqNLXxEZFbXE',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || '3prmuinjgigdvi7m5haujp8jtq1un82rc64n',
  facebook: {
    appId: '564041023804928',
  },
  logRocket: 'h57nhx/sl_sandbox',
};
