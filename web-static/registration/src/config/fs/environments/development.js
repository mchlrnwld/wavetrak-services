module.exports = {
  cdnHost: '/',
  staticHost: '/static/',
  redirectUrl: '/',
  benefitsRedirectUrl: 'http://benefits.fishtrack.com/latest-sst-imagery/',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: '/upgrade',
  accountLinkProtocol: 'https://staging.',
  segment: 'wcxCswdPqynOfwHMDhKE4xCVSLF8knV0',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '17227951',
    accountID: '356245',
    trustKey: '356245',
    agentID: '562790161',
  },
  servicesEndpoint: 'https://services.sandbox.surfline.com',
  stripe: {
    publishableKey: 'pk_test_Meb7MPzMWgbBYwU3t6gmdHGt',
  },
  termsConditions: 'http://www.fishtrack.com/terms-and-conditions/',
  privacyPolicy: 'http://www.fishtrack.com/privacy-policy/',
  needHelpUrl: 'http://www.fishtrack.com/contact-us/',
  clientId: '5c59e4fb2d11e750809d572e',
  clientSecret: 'sk_h63qPbkRrqNLXxEZFbXE',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
  facebook: {
    appId: '564041023804928',
  },
  logRocket: 'h57nhx/sl_sandbox',
};
