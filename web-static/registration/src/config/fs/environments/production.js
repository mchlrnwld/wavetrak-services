module.exports = {
  cdnHost: 'https://wa.cdn-surfline.com/registration/fs/',
  staticHost: 'https://wa.cdn-surfline.com/registration/fs/static/',
  redirectUrl: 'http://www.fishtrack.com',
  benefitsRedirectUrl: 'http://benefits.fishtrack.com/latest-sst-imagery/',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: 'http://www.fishtrack.com/upgrade',
  accountLinkProtocol: 'https://www.',
  segment: 'NcHLO1P5K4Dv1XP2hRXvgXAQMgFLJ3Ch',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '562790161',
    accountID: '356245',
    trustKey: '356245',
    agentID: '562790161',
  },
  servicesEndpoint: 'https://services.surfline.com',
  stripe: {
    publishableKey:
      'pk_live_518MSMSJ4F5N75cpXxyXhiIIfbYxuWIna19yhOzriQDepMQfxMqDvwuNNhQXbKbOV5icocdsHvbp0gizP6L0wMCjn00dPOhEeeW',
  },
  termsConditions: 'http://www.fishtrack.com/terms-and-conditions/',
  privacyPolicy: 'http://www.fishtrack.com/privacy-policy/',
  needHelpUrl: 'http://www.fishtrack.com/contact-us/',
  clientId: '5c59e80cf0b6cb1ad02baf67',
  clientSecret: 'sk_PvRkUPFXC39AWZwhK6Da',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'in20v2ebf6q1gh97ldcn1bp8ufdbr404e514',
  facebook: {
    appId: '218714858155230',
  },
  logRocket: 'h57nhx/sl_prod',
};
