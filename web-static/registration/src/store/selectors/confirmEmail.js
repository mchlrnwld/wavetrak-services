export const getConfirmEmailError = (state) => {
  const error = state?.confirmEmail?.confirmEmailError;
  if (error) return error;
  return null;
};

export const isSubmitting = (state) => {
  const submitting = state?.confirmEmail?.submitting;
  if (submitting) return submitting;
  return null;
};

export const isSubmitSuccess = (state) => {
  const submitSuccess = state?.confirmEmail?.submitSuccess;
  if (submitSuccess) return submitSuccess;
  return null;
};
