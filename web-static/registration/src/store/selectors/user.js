export const getUser = (state) => {
  if (state) {
    const { user } = state;
    return user;
  }
  return null;
};

export const getUserError = (state) => {
  if (state) {
    const {
      user: { error },
    } = state;
    return error;
  }
  return null;
};

export const getUserSuccess = (state) => {
  if (state) {
    const {
      user: { success },
    } = state;
    return success;
  }
  return null;
};

export const getFacebookLoading = (state) => {
  if (state) {
    const {
      user: { facebookLoading },
    } = state;
    return facebookLoading;
  }
  return null;
};

export const getConfirmEmail = (state) => {
  if (state) {
    const {
      user: { confirmEmail },
    } = state;
    return confirmEmail;
  }
  return null;
};

export const getShowTerms = (state) => {
  if (state) {
    const {
      user: { showTerms, migratedBrandName },
    } = state;
    return { showTerms, migratedBrandName };
  }
  return null;
};
