export const getUserResetCode = (state) => {
  const resetCode = state?.forgotPassword?.user?.resetCode;
  if (resetCode) return resetCode;
  return null;
};

export const getForgotPasswordError = (state) => {
  const { passwordFail } = state.forgotPassword;
  if (passwordFail) return passwordFail;
  return null;
};

export const getForgotPasswordUserID = (state) => {
  const user = state?.forgotPassword?.user?.id;
  if (user) return user;
  return null;
};

export const getForgotPasswordSubmitting = (state) => {
  const submitting = state?.forgotPassword?.submitting;
  if (submitting) return submitting;
  return null;
};

export const getRequestResetError = (state) => {
  const { requestResetError } = state.forgotPassword;
  if (requestResetError) return requestResetError;
  return null;
};

export const getRequestResetSuccess = (state) => {
  const { resetSuccess } = state.forgotPassword;
  if (resetSuccess) return resetSuccess;
  return null;
};

export const getUserHashValidatedError = (state) => {
  const { validateHashError } = state.forgotPassword;
  if (validateHashError) return validateHashError;
  return null;
};

export const getPasswordResetSuccess = (state) => {
  const { passwordReset } = state.forgotPassword;
  if (passwordReset) return passwordReset;
  return null;
};
