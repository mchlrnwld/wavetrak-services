export const getShowLogin = (state) => {
  if (state) {
    const {
      interactions: { showLogin },
    } = state;
    return showLogin;
  }
  return null;
};

export const isFormSubmitting = (state) => {
  if (state) {
    const {
      interactions: { submitting },
    } = state;
    return submitting;
  }
  return null;
};
