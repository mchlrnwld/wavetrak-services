export const getVerifyEmailStatus = (state) => {
  if (state) {
    const { verifyEmail } = state;
    return verifyEmail;
  }
  return null;
};
