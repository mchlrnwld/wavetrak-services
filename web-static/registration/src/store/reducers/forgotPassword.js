import createReducer from './createReducer';
import {
  REQUEST,
  REQUEST_SUCCESS,
  REQUEST_FAIL,
  GETUSERID,
  GETUSERID_SUCCESS,
  GETUSERID_FAIL,
  REDIRECT,
  REDIRECT_SUCCESS,
  REDIRECT_FAIL,
  SETPASSWORD,
  SETPASSWORD_SUCCESS,
  SETPASSWORD_FAIL,
  CLEAR,
  CLEAR_SUCCESS,
  CLEAR_FAIL,
} from '../actions/forgotPassword';

const initialState = {
  user: null,
  isValidated: false,
  submitting: false,
  submitSuccess: false,
};

const handlers = {};

handlers[REQUEST] = (state) => ({
  ...state,
  submitting: true,
  submitSuccess: true,
});

handlers[REQUEST_SUCCESS] = (state, { result }) => ({
  ...state,
  submitting: false,
  resetSuccess: true,
  submitSuccess: true,
  user: result,
});

handlers[REQUEST_FAIL] = (state, { error }) => ({
  ...state,
  submitting: false,
  user: null,
  submitSuccess: false,
  requestResetError: error,
});

handlers[GETUSERID] = (state) => ({
  ...state,
  requestingUserId: true,
});

handlers[GETUSERID_SUCCESS] = (state, { result }) => ({
  ...state,
  user: result,
  isValidated: true,
  requestingUserId: false,
});

handlers[GETUSERID_FAIL] = (state, { error }) => ({
  ...state,
  requestingUserId: false,
  user: null,
  validateHashError: error,
});

handlers[REDIRECT] = (state) => ({
  ...state,
  redirecting: true,
});

handlers[REDIRECT_SUCCESS] = (state, { result }) => ({
  ...state,
  redirecting: false,
  user: result,
});

handlers[REDIRECT_FAIL] = (state, { error }) => ({
  ...state,
  redirecting: false,
  user: null,
  redirectError: error,
});

handlers[SETPASSWORD] = (state) => ({
  ...state,
  submitting: true,
  requestingPasswordReset: true,
  passwordSubmitSuccess: true,
});

handlers[SETPASSWORD_SUCCESS] = (state) => ({
  ...state,
  submitting: false,
  requestingPasswordReset: false,
  passwordReset: true,
  passwordSubmitSuccess: true,
});

handlers[SETPASSWORD_FAIL] = (state, { error }) => ({
  ...state,
  requestingPasswordReset: false,
  passwordReset: false,
  passwordSubmitSuccess: false,
  passwordFail: error,
  submitting: false,
});

handlers[CLEAR] = (state) => ({
  ...state,
  requestingClear: true,
});

handlers[CLEAR_SUCCESS] = (state, { result }) => ({
  ...state,
  requestingClear: false,
  user: result,
  isValidated: false,
});

handlers[CLEAR_FAIL] = (state, { error }) => ({
  ...state,
  requestingReset: false,
  user: null,
  requestResetError: error,
});

export default createReducer(handlers, initialState);
