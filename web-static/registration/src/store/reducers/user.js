import createReducer from './createReducer';
import {
  CONFIRM_EMAIL,
  FETCH_USER,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
  LOGIN,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGIN_RESET,
  CREATE,
  CREATE_SUCCESS,
  CREATE_FAIL,
  FBLOGIN,
  FBLOGIN_FAIL,
  FBLOGIN_SUCCESS,
  FETCH_SHOW_TERMS,
  FETCH_SHOW_TERMS_SUCCESS,
  FETCH_SHOW_TERMS_FAILURE,
} from '../actions/user';

const initialState = {
  error: null,
  success: null,
  deleted: false,
  loading: false,
  facebookLoading: false,
  planChanged: false,
  upcomingInvoice: null,
  confirmEmail: false,
  details: null,
};
const handlers = {};

handlers[FETCH_USER] = (state) => ({
  ...state,
  loading: true,
});
handlers[FETCH_USER_SUCCESS] = (state, { user }) => ({
  ...state,
  details: {
    ...user,
  },
  error: null,
  loading: false,
});
handlers[FETCH_USER_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});
handlers[CREATE] = (state) => ({
  ...state,
  loading: true,
});
handlers[CREATE_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});
handlers[CREATE_SUCCESS] = (state, { accessToken }) => ({
  ...state,
  accessToken,
  loading: false,
});
handlers[LOGIN] = (state) => ({
  ...state,
  loading: true,
});
handlers[LOGIN_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});
handlers[LOGIN_SUCCESS] = (state, { accessToken }) => ({
  ...state,
  accessToken,
  loading: false,
});
handlers[LOGIN_RESET] = (state) => ({
  ...state,
  ...initialState,
});

handlers[FBLOGIN] = (state) => ({
  ...state,
  facebookLoading: true,
});
handlers[FBLOGIN_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
  facebookLoading: false,
});

handlers[FBLOGIN_SUCCESS] = (state, { accessToken }) => ({
  ...state,
  accessToken,
  facebookLoading: false,
});

handlers[CONFIRM_EMAIL] = (state, { email }) => ({
  ...state,
  email,
  confirmEmail: true,
});

handlers[FETCH_SHOW_TERMS] = (state) => ({
  ...state,
});

handlers[FETCH_SHOW_TERMS_SUCCESS] = (state, { migratedBrandName, showTerms }) => ({
  ...state,
  migratedBrandName,
  showTerms,
});

handlers[FETCH_SHOW_TERMS_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
});

export default createReducer(handlers, initialState);
