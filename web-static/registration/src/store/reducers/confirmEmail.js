import createReducer from './createReducer';
import { REQUEST, REQUEST_SUCCESS, REQUEST_FAIL } from '../actions/confirmEmail';

const initialState = {
  submitting: false,
  submitSuccess: false,
  confirmEmailError: null,
  loading: false,
};

const handlers = {};

handlers[REQUEST] = (state) => ({
  ...state,
  submitting: true,
  submitSuccess: true,
  confirmEmailError: null,
});

handlers[REQUEST_SUCCESS] = (state) => ({
  ...state,
  submitting: false,
  submitSuccess: true,
  confirmEmailError: null,
});

handlers[REQUEST_FAIL] = (state, { error }) => ({
  ...state,
  submitting: false,
  submitSuccess: false,
  confirmEmailError: error,
});
export default createReducer(handlers, initialState);
