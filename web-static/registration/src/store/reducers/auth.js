import createReducer from './createReducer';

import { SET_ACCESS_TOKEN } from '../actions/auth';

const initialState = {};
const handlers = {};

handlers[SET_ACCESS_TOKEN] = (state, { accessToken }) => {
  return {
    ...state,
    accessToken,
  };
};

export default createReducer(handlers, initialState);
