import { combineReducers } from 'redux';
import user from './user';
import auth from './auth';
import verifyEmail from './verifyEmail';
import interactions from './interactions';
import forgotPassword from './forgotPassword';
import confirmEmail from './confirmEmail';

export default combineReducers({
  user,
  auth,
  verifyEmail,
  interactions,
  forgotPassword,
  confirmEmail,
});
