import { forgotPassword } from '../../common/api/passwordreset';

export const REQUEST = 'REQUEST';
export const REQUEST_SUCCESS = 'REQUEST_SUCCESS';
export const REQUEST_FAIL = 'REQUEST_FAIL';

export const confirmEmail = (email) => async (dispatch) => {
  dispatch({ type: REQUEST });
  try {
    const response = await forgotPassword(email);

    return dispatch({ type: REQUEST_SUCCESS, response });
  } catch (err) {
    return dispatch({ type: REQUEST_FAIL, err });
  }
};
