import { trackEvent } from '@surfline/web-common';
import { forgotPassword, validateHash, resetPassword } from '../../common/api/passwordreset';
import getConfig from '../../config';
import setLoginCookie from '../../utils/setLoginCookie';

export const REQUEST = 'REQUEST';
export const REQUEST_SUCCESS = 'REQUEST_SUCCESS';
export const REQUEST_FAIL = 'REQUEST_FAIL';
export const GETUSERID = 'GETUSERID';
export const GETUSERID_SUCCESS = 'GETUSERID_SUCCESS';
export const GETUSERID_FAIL = 'GETUSERID_FAIL';
export const REDIRECT = 'REDIRECT';
export const REDIRECT_SUCCESS = 'REDIRECT_SUCCESS';
export const REDIRECT_FAIL = 'REDIRECT_FAIL';
export const SETPASSWORD = 'SETPASSWORD';
export const SETPASSWORD_SUCCESS = 'SETPASSWORD_SUCCESS';
export const SETPASSWORD_FAIL = 'SETPASSWORD_FAIL';
export const CLEAR = 'CLEAR';
export const CLEAR_SUCCESS = 'CLEAR_SUCCESS';
export const CLEAR_FAIL = 'CLEAR_FAIL';

const config = getConfig();

export const requestReset = (email) => async (dispatch) => {
  dispatch({ type: REQUEST });
  try {
    const response = await forgotPassword(email);
    return dispatch({ type: REQUEST_SUCCESS, response });
  } catch (error) {
    return dispatch({ type: REQUEST_FAIL, error });
  }
};

export const setPassword = (password, passwordConfirm, userId, resetCode) => async (dispatch) => {
  dispatch({ type: SETPASSWORD });
  try {
    if (password !== passwordConfirm) {
      return dispatch({ type: SETPASSWORD_FAIL, error: 'Password much match password confirm' });
    }
    const resetResponse = await resetPassword(password, userId, resetCode);
    trackEvent('Signed In', {
      method: 'forgot password flow',
      platform: 'web',
    });
    const accessToken = resetResponse.access_token;
    const refreshToken = resetResponse.refresh_token;
    const staySignedIn = false; // set 30 minute access token for password reset
    setLoginCookie(accessToken, refreshToken, staySignedIn);
    return dispatch({ type: SETPASSWORD_SUCCESS });
  } catch (error) {
    return dispatch({ type: SETPASSWORD_FAIL, error });
  }
};

export const validate = (hash) => async (dispatch) => {
  dispatch({ type: GETUSERID });
  try {
    const response = await validateHash(hash, config.brand);
    const result = { id: response.userId, resetCode: response.resetCode };

    return dispatch({ type: GETUSERID_SUCCESS, result });
  } catch (error) {
    return dispatch({ type: GETUSERID_FAIL, error });
  }
};
