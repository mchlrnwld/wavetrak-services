import { Cookies } from 'react-cookie';
import { serializeError } from 'serialize-error';
import {
  trackEvent,
  SL,
  nrNoticeError,
  nrAddPageAction,
  setWavetrakIdentity,
  getWavetrakIdentity,
  getWindow,
} from '@surfline/web-common';
import setLoginCookie from '../../utils/setLoginCookie';
import getConfig from '../../config';
import { generateToken, authorise } from '../../common/api/auth';
import { postUser, getUser, validateEmail, getShowTerms } from '../../common/api/user';
import { setAccessToken, validateAccessToken } from './auth';
import { stopSubmitSpinner, startSubmitSpinner } from './interactions';
import deviceInfo from '../../common/utils/deviceInfo';
import { identifyUser } from '../../utils/identifyUser';
import { getAccessToken } from '../../utils/getAccessToken';

const config = getConfig();
const { brand, clientId } = config;

export const CONFIRM_EMAIL = 'CONFIRM_EMAIL';
export const CREATE = 'CREATE';
export const CREATE_SUCCESS = 'CREATE_SUCCESS';
export const CREATE_FAIL = 'CREATE_FAIL';
export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';
export const APP_READY = 'APP_READY';

export const LOGIN = 'LOGIN';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_RESET = 'LOGIN_RESET';

export const FBLOGIN = 'FBLOGIN';
export const FBLOGIN_RESET = 'FBLOGIN_RESET';
export const FBLOGIN_SUCCESS = 'FBLOGIN_SUCCESS';
export const FBLOGIN_FAIL = 'FBLOGIN_FAIL';
export const FBLOGINERROR = 'FBLOGINERROR';
export const FBLOGINERROR_SUCCESS = 'FBLOGINERROR_SUCCESS';
export const FBLOGINERROR_FAIL = 'FBLOGINERROR_FAIL';
export const FBSUBMIT = 'FBSUBMIT';
export const FBSUBMIT_SUCCESS = 'FBSUBMIT_SUCCESS';
export const FBSUBMIT_FAIL = 'FBSUBMIT_FAIL';

export const FETCH_SHOW_TERMS = 'FETCH_SHOW_TERMS';
export const FETCH_SHOW_TERMS_FAILURE = 'FETCH_SHOW_TERMS_FAILURE';
export const FETCH_SHOW_TERMS_SUCCESS = 'FETCH_SHOW_TERMS_SUCCESS';

const cookies = new Cookies();

export const login = (values) => async (dispatch) => {
  const { email, password, staySignedIn, location, promotionId } = values;
  try {
    dispatch(startSubmitSpinner());
    dispatch({ type: LOGIN });
    nrAddPageAction('LoginProgress Step 1', {
      email,
      payload: {
        staySignedIn,
        location,
        promotionId,
        atCookie: cookies.get('access_token'),
        rtCookie: cookies.get('refresh_token'),
      },
    });

    const { deviceId, deviceType } = deviceInfo();

    const payload = {
      grant_type: 'password',
      username: email,
      password,
      device_id: deviceId,
      device_type: deviceType,
      forced: true,
    };

    nrAddPageAction('LoginProgress Step 2', { email });
    const {
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: expiresIn,
    } = await generateToken(!staySignedIn, payload);
    nrAddPageAction('LoginProgress Step 3', {
      email,
      payload: {
        accessToken,
        refreshToken,
        expiresIn,
      },
    });
    const { id } = await authorise(accessToken);
    nrAddPageAction('LoginProgress Step 4', { payload: id, email });

    identifyUser(id);

    trackEvent('Signed In', {
      method: 'email',
      email,
      location,
      promotionId,
      platform: 'web',
    });

    setLoginCookie(accessToken, refreshToken, staySignedIn, expiresIn);
    nrAddPageAction('LoginProgress Step 5', { payload: accessToken, email });
    dispatch(setAccessToken(accessToken));
    nrAddPageAction('LoginProgress Step 6', { email });
    dispatch(stopSubmitSpinner());
    return dispatch({ type: LOGIN_SUCCESS, accessToken });
  } catch (error) {
    const errorString = JSON.stringify(serializeError(error));
    if (getWindow()?.newrelic) {
      const errorObj = {
        message: 'Error in Login',
        errorString,
      };
      // eslint-disable-next-line no-param-reassign
      delete values.password; // Remove password so that we don't log in NewRelic
      const customErrorAttributes = {
        email,
        errorString,
        payloadString: JSON.stringify(values),
      };
      nrNoticeError(errorObj, customErrorAttributes);
      nrAddPageAction('LoginProgress Error', { email, errorString });
    }
    if (config.brand === SL) {
      try {
        const validated = await validateEmail(email);
        nrAddPageAction('LoginProgress validateEmail', {
          email,
          validated,
        });
        if (!validated?.hasPassword) {
          return dispatch({ type: CONFIRM_EMAIL, email });
        }
      } catch (innerError) {
        nrAddPageAction('LoginProgress validateEmail Error', {
          email,
          error: innerError,
        });
      } // eslint-disable-line
    }
    dispatch(stopSubmitSpinner());
    return dispatch({
      type: LOGIN_FAIL,
      message: error?.error_description || 'Something went wrong.',
    });
  }
};

const facebookSDKLogin = () =>
  new Promise((resolve, reject) => {
    // eslint-disable-next-line
    FB.login(
      (response, error) => {
        if (error) return reject(error);
        if (response.authResponse) return resolve(response);
        return resolve({});
      },
      { scope: 'public_profile, email' },
    );
  });

const facebookSDKGetUserInfo = () =>
  new Promise((resolve, reject) => {
    // eslint-disable-next-line
    FB.api(
      '/me',
      {
        fields: 'name, email, first_name, last_name',
      },
      (userInfo, error) => {
        if (error) return reject(error);
        if (!userInfo.email) return reject(new Error({ message: 'An email address is required ' }));
        return resolve(userInfo);
      },
    );
  });

export const loginWithFacebook = (location) => async (dispatch) => {
  try {
    const { authResponse: facebookAuthResponse } = await facebookSDKLogin();
    if (!facebookAuthResponse) return dispatch({ type: FBLOGIN_RESET });

    const {
      id: facebookId,
      email,
      first_name: firstName,
      last_name: lastName,
    } = await facebookSDKGetUserInfo();

    dispatch({ type: FBLOGIN });
    const { deviceId, deviceType } = deviceInfo();
    const payload = {
      grant_type: 'password',
      fbToken: facebookAuthResponse.accessToken,
      brand: config.company,
      username: email,
      password: 'null',
      facebookId,
      firstName,
      lastName,
      device_id: deviceId,
      device_type: deviceType,
    };

    const staySignedIn = true;
    const {
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: expiresIn,
    } = await generateToken(!staySignedIn, payload);

    const { id } = await authorise(accessToken);

    identifyUser(id);

    trackEvent('Signed In', { method: 'facebook', email, location, platform: 'web' });

    setWavetrakIdentity({
      ...getWavetrakIdentity(),
      email,
      userId: id,
      type: 'logged_in',
    });

    setLoginCookie(accessToken, refreshToken, staySignedIn, expiresIn);
    dispatch(setAccessToken(accessToken));

    return dispatch({ type: FBLOGIN_SUCCESS });
  } catch (error) {
    if (error.status === 1001) {
      return dispatch({ type: FBLOGIN_RESET });
    }
    const errorMessage = error.error_description || error.message;
    return dispatch({ type: FBLOGIN_FAIL, error: errorMessage });
  }
};

export const register = (values) => async (dispatch) => {
  try {
    dispatch(startSubmitSpinner());
    dispatch({ type: LOGIN });
    const {
      firstName,
      lastName,
      email,
      password,
      receivePromotions,
      staySignedIn,
      location,
      promotionId,
    } = values;
    const { deviceId, deviceType } = deviceInfo();

    const valuesForRegistration = {
      firstName,
      lastName,
      email,
      password,
      brand,
      settings: {
        receivePromotions,
      },
      client_id: clientId,
      device_id: deviceId,
      device_type: deviceType,
    };

    trackEvent('Clicked Create Account', {
      location,
      pageName: getWindow()?.location?.pathname,
      promotionId,
    });

    const {
      token: { access_token: accessToken, refresh_token: refreshToken, expires_in: expiresIn },
      user: { _id: userId },
    } = await postUser(!staySignedIn, valuesForRegistration);

    identifyUser(userId);

    setLoginCookie(accessToken, refreshToken, staySignedIn, expiresIn);
    dispatch(setAccessToken(accessToken));
    dispatch(stopSubmitSpinner());
    if (brand === 'sl') getWindow()?.location.assign('/onboarding');
    return dispatch({ type: CREATE_SUCCESS, accessToken });
  } catch (error) {
    let errorDescription = 'Sorry, we could not complete this request';
    if (error.status === 400) {
      errorDescription = error.message || errorDescription;
    }
    const err = { _error: errorDescription };
    dispatch({ type: CREATE_FAIL, message: errorDescription });
    dispatch(stopSubmitSpinner());
    throw err;
  }
};

export const fetchUser = () => async (dispatch) => {
  try {
    dispatch({ type: FETCH_USER });
    const accessToken = cookies.get('access_token');
    const user = await getUser(accessToken);
    return dispatch({ type: FETCH_USER_SUCCESS, user });
  } catch (err) {
    const { message } = err;
    return dispatch({ type: FETCH_USER_FAILURE, message });
  }
};

export const fetchShowTerms = (userId) => async (dispatch) => {
  try {
    dispatch({ type: FETCH_SHOW_TERMS });
    const { showTerms, migratedBrandName } = await getShowTerms(userId);
    return dispatch({ type: FETCH_SHOW_TERMS_SUCCESS, migratedBrandName, showTerms });
  } catch (error) {
    return dispatch({ type: FETCH_SHOW_TERMS_FAILURE });
  }
};

export const loadUser = () => async (dispatch) => {
  const accessToken = getAccessToken();

  if (!accessToken) return null;

  const { isValid } = await dispatch(validateAccessToken(accessToken));

  if (isValid) {
    await dispatch(fetchUser());
  }

  return true;
};
