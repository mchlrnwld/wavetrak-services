import Head from 'next/head';
import dynamic from 'next/dynamic';
import { Loading } from '../../components';
import getConfig from '../../config';
import brandToString from '../../utils/brandToString';

const { brand } = getConfig();

const VerifyEmailContainer = dynamic(import('../../components/VerifyEmail'), {
  loading: () => <Loading />,
  ssr: false,
});

const VerifyEmail = (props) => (
  <>
    <Head>
      <title>Verify Your Email - {brandToString(brand)}</title>
    </Head>
    <VerifyEmailContainer {...props} />
  </>
);

export default VerifyEmail;
