import Head from 'next/head';
import dynamic from 'next/dynamic';
import { Loading } from '../../components';

const ConfirmEmailContainer = dynamic(import('../../components/ConfirmEmail'), {
  loading: () => <Loading />,
  ssr: false,
});

const ConfirmEmail = (props) => (
  <>
    <Head>
      <title>Confirm Email - Surfline</title>
    </Head>
    <ConfirmEmailContainer {...props} />
  </>
);

export default ConfirmEmail;
