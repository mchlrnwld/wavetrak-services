import Head from 'next/head';
import dynamic from 'next/dynamic';
import { Loading } from '../../components';

const EmailSentContainer = dynamic(import('../../components/EmailSent'), {
  loading: () => <Loading />,
  ssr: false,
});

const EmailSent = (props) => (
  <>
    <Head>
      <title>Email Sent - Surfline</title>
    </Head>
    <EmailSentContainer {...props} isForgotPassword={false} />
  </>
);

export default EmailSent;
