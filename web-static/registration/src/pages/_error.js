import Link from 'next/link';
import { useRouter } from 'next/router';
import getConfig from '../config';

const config = getConfig();

const ErrorPage = ({ errorCode }) => {
  const router = useRouter();
  let errTemplate = null;
  if (errorCode) {
    switch (errorCode) {
      case 200:
      case 404:
        errTemplate = (
          <div>
            <p>
              The page <strong>{router.asPath}</strong> does not exist.
            </p>
            <p>
              Go to <a href={config.redirectUrl}>Home</a>
            </p>
          </div>
        );
        break;
      case 500:
        errTemplate = (
          <div>
            <p>
              An error occured at <strong>{router.pathname}</strong>
            </p>
            <p>
              Go to{' '}
              <Link href={config.redirectUrl}>
                <a href={config.redirectUrl}>Home</a>
              </Link>
            </p>
          </div>
        );
        break;
      default:
        errTemplate = (
          <div>
            <p>
              An HTTP error <strong>{errorCode}</strong> occurred while trying to access{' '}
              <strong>{router.pathname}</strong>
            </p>
            <p>
              Go to{' '}
              <Link href={config.redirectUrl}>
                <a href={config.redirectUrl}>Home</a>
              </Link>
            </p>
          </div>
        );
    }
  }
  return errTemplate;
};

ErrorPage.getInitialProps = async ({ res, xhr }) => {
  // eslint-disable-next-line no-nested-ternary
  const errorCode = res ? res.statusCode : xhr ? xhr.status : null;
  return { errorCode };
};

export default ErrorPage;
