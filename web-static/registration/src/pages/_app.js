import { useMount } from 'react-use';
import { useState } from 'react';
import { Provider } from 'react-redux';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { createSplitFilter, setupFeatureFramework } from '@surfline/web-common';

import getConfig from '../config';
import AppHead from '../components/AppHead/AppHead';
import store from '../store';
import { loadUser } from '../store/actions/user';
import { logRocketEnabled, setupLogRocket } from '../utils/logrocket';
import * as treatments from '../utils/treatments';
import Loading from '../components/Loading';
import VerifyEmailLayout from '../components/VerifyEmailLayout';
import Layout from '../components/Layout';
import { getUser } from '../store/selectors/user';

import '../common/themes/main.scss';

const config = getConfig();

/**
 * @param {object} props
 * @param {import('react-cookie').Cookies} props.cookies
 * @param {import('next/app').AppProps["Component"]} props.Component
 * @param {import('next/app').AppProps["pageProps"]} props.pageProps
 */
const RegistrationApp = ({ Component, pageProps }) => {
  const [ready, setReady] = useState(false);
  useMount(async () => {
    if (logRocketEnabled()) {
      setupLogRocket();
    }

    await store.dispatch(loadUser());

    try {
      await setupFeatureFramework({
        authorizationKey: config.splitio,
        user: getUser(store.getState()),
        splitFilters: createSplitFilter(treatments),
      });
    } finally {
      setReady(true);
    }
  });

  const router = useRouter();

  const isVerifyEmail = router.pathname.includes('verify-email');

  const LayoutWrapper = isVerifyEmail ? VerifyEmailLayout : Layout;

  return (
    <Provider store={store}>
      <AppHead config={config} />
      <LayoutWrapper>
        {ready ? <Component {...pageProps} ready={ready} /> : <Loading />}
      </LayoutWrapper>
    </Provider>
  );
};

RegistrationApp.propTypes = {
  pageProps: PropTypes.shape().isRequired,
  Component: PropTypes.node.isRequired,
};

export default RegistrationApp;
