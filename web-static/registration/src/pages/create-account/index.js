import Head from 'next/head';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { Loading } from '../../components';
import getConfig from '../../config';
import brandToString from '../../utils/brandToString';

const { brand } = getConfig();

const CreateAccountContainer = dynamic(import('../../components/CreateAccount'), {
  loading: () => <Loading />,
  ssr: false,
});

const CreateAccount = (props) => {
  const router = useRouter();
  const { redirectUrl } = router.query;
  if (redirectUrl) sessionStorage.setItem('redirectUrl', redirectUrl);
  return (
    <>
      <Head>
        <title>Create Account - {brandToString(brand)}</title>
      </Head>
      <CreateAccountContainer {...props} />
    </>
  );
};

export default CreateAccount;
