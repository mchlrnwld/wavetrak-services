import Head from 'next/head';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { Loading } from '../../components';
import getConfig from '../../config';
import brandToString from '../../utils/brandToString';

const { brand } = getConfig();

const SignInContainer = dynamic(import('../../components/SignIn'), {
  loading: () => <Loading />,
  ssr: false,
});

const SignIn = (props) => {
  const router = useRouter();
  const { redirectUrl } = router.query;
  if (redirectUrl) sessionStorage.setItem('redirectUrl', redirectUrl);
  return (
    <>
      <Head>
        <title>Sign In - {brandToString(brand)}</title>
      </Head>
      <SignInContainer {...props} />
    </>
  );
};

export default SignIn;
