import Head from 'next/head';
import dynamic from 'next/dynamic';
import { Loading } from '../../components';
import getConfig from '../../config';
import brandToString from '../../utils/brandToString';

const { brand } = getConfig();

const ForgotPasswordContainer = dynamic(import('../../components/ForgotPassword'), {
  loading: () => <Loading />,
  ssr: false,
});

const ForgotPassword = (props) => (
  <>
    <Head>
      <title>Forgot Password - {brandToString(brand)}</title>
    </Head>
    <ForgotPasswordContainer {...props} />
  </>
);

export default ForgotPassword;
