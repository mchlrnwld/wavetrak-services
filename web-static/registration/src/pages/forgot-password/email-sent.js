import Head from 'next/head';
import dynamic from 'next/dynamic';
import { Loading } from '../../components';
import getConfig from '../../config';
import brandToString from '../../utils/brandToString';

const { brand } = getConfig();

const EmailSentContainer = dynamic(import('../../components/EmailSent'), {
  loading: () => <Loading />,
  ssr: false,
});

const EmailSent = (props) => (
  <>
    <Head>
      <title>Email Sent - {brandToString(brand)}</title>
    </Head>
    <EmailSentContainer {...props} />
  </>
);

export default EmailSent;
