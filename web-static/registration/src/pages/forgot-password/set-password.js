import Head from 'next/head';
import dynamic from 'next/dynamic';
import { Loading } from '../../components';
import getConfig from '../../config';
import brandToString from '../../utils/brandToString';

const { brand } = getConfig();

const SetPasswordContainer = dynamic(import('../../components/SetPassword'), {
  loading: () => <Loading />,
  ssr: false,
});

const SetPassword = (props) => (
  <>
    <Head>
      <title>Create New Password - {brandToString(brand)}</title>
    </Head>
    <SetPasswordContainer {...props} />
  </>
);

export default SetPassword;
