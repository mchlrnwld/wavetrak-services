# Promotions Funnel

## Description

Promotions Funnel frontend App. Created with NextJS + React + Redux.

Uses: node version 12.

### Development
```bash
npm install
npm run dev
```

Since this is a Promotion Funnel we would need a valid `promotionId` to load the funnel. You can get the `promotionId` from Promotions CMS.
For local development make sure to set the `promotionId` here in `_app.js` - https://github.com/Surfline/surfline-web/blob/bd4bedfb0d76199f48d6f3ffb178544268a87dfc/account/promotions-next/src/pages/_app.js#L22

```
const { promotionId } = window; // remove this line in _app.js and use below line
const promotionId = <promotionId for the Funnel>; // Replace with a valid promotionId
```

In Staging/Sandbox/Prod the `promotionId` would be set automatically in the window scope by the web-proxy based on the promotion url.

The site would be available on `http://localhost:3000`

### Deployment

This site is exported as a staic website and stored in an S3 bucket per brand- `sl-product-cdn-prod/promotions`

Jenkins deploy job: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/build-and-deploy-account-to-s3/
