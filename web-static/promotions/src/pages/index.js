import { useEffect } from 'react';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { trackCampaign } from '@surfline/web-common';
import Loading from '../components/Loading';

const PromotionContainer = dynamic(import('../components/Promotion/Promotion'), {
  loading: () => <Loading />,
  ssr: false,
});

const PromotionFunnel = (props) => {
  const { query } = useRouter();

  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => trackCampaign(query), []);
  /* eslint-enable react-hooks/exhaustive-deps */

  return <PromotionContainer {...props} />;
};
export default PromotionFunnel;
