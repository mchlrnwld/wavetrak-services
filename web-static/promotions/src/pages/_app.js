/* eslint-disable react/jsx-props-no-spreading */
import { useMount } from 'react-use';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { Provider } from 'react-redux';
import { setupFeatureFramework, createSplitFilter } from '@surfline/web-common';
import * as treatments from '../utils/treatments';
import getConfig from '../config';
import { AppHead, Layout } from '../components';
import store from '../store';
import { fetchPromotion } from '../store/actions/promotion';
import { loadUser, fetchCountryCode } from '../store/actions/user';
import { fetchCurrency } from '../store/actions/subscription';
import { getUser } from '../store/selectors/user';
import Loading from '../components/Loading';

const config = getConfig();

/**
 * @param {object} props
 * @param {import('react-cookie').Cookies} props.cookies
 * @param {import('next/app').AppProps["Component"]} props.Component
 * @param {import('next/app').AppProps["pageProps"]} props.pageProps
 */
const PromotionApp = ({ Component, pageProps }) => {
  const [appReady, setAppReady] = useState(false);

  useMount(async () => {
    const { splitio } = getConfig();

    // Load user before setting up feature framework
    await store.dispatch(loadUser());
    await store.dispatch(fetchCountryCode());

    try {
      await setupFeatureFramework({
        authorizationKey: splitio,
        user: getUser(store.getState()),
        splitFilters: createSplitFilter(treatments),
      });
    } finally {
      await Promise.all([
        [fetchCurrency, fetchPromotion].map((action) => store.dispatch(action())),
      ]);

      setAppReady(true);
    }
  });

  return (
    <Provider store={store}>
      <AppHead config={config} />
      <Layout>{appReady ? <Component {...pageProps} appReady={appReady} /> : <Loading />}</Layout>
    </Provider>
  );
};

PromotionApp.propTypes = {
  pageProps: PropTypes.shape({}).isRequired,
  Component: PropTypes.func.isRequired,
};

export default PromotionApp;
