module.exports = {
  cdnHost: 'https://product-cdn.sandbox.surfline.com/promotions/fs/',
  staticHost: 'https://product-cdn.sandbox.surfline.com/promotions/fs/static/',
  redirectUrl: 'http://sandbox.fishtrack.com',
  segment: 'wcxCswdPqynOfwHMDhKE4xCVSLF8knV0',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '16905555',
    accountID: '356245',
    trustKey: '356245',
    agentID: '16905555',
  },
  servicesEndpoint: 'https://services.sandbox.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
  clientId: '5c59e4fb2d11e750809d572e',
  clientSecret: 'sk_h63qPbkRrqNLXxEZFbXE',
  termsConditions: 'http://www.fishtrack.com/terms-and-conditions/',
  privacyPolicy: 'http://www.fishtrack.com/privacy-policy/',
  stripe: {
    publishableKey: 'pk_test_Meb7MPzMWgbBYwU3t6gmdHGt',
  },
  facebook: {
    appId: '564041023804928',
  },
  upgradeRedirectUrl: 'https://sandbox.fishtrack.com/upgrade',
};
