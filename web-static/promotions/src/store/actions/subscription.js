import { trackEvent, nrAddPageAction, nrNoticeError } from '@surfline/web-common';

import { stopSubmitSpinner, startSubmitSpinner } from './interactions';
import { upgrade, getCurrencyForUser } from '../../common/api/subscription';
import { showDuplicateCard, clearDuplicateCard } from './duplicateCard';

import getConfig from '../../config';
import en from '../../international/translations/en';
import { getCountryCode } from '../selectors/user';
import { getSelectedPlan } from '../selectors/plans';

export const SUBMIT_SUBSCRIPTION = 'SUBMIT_SUBSCRIPTION';
export const SUBMIT_SUBSCRIPTION_SUCCESS = 'SUBMIT_SUBSCRIPTION_SUCCESS';
export const SUBMIT_SUBSCRIPTION_FAIL = 'SUBMIT_SUBSCRIPTION_FAIL';

export const FETCH_CURRENCY = 'FETCH_CURRENCY';
export const FETCH_CURRENCY_SUCCESS = 'FETCH_CURRENCY_SUCCESS';
export const FETCH_CURRENCY_FAIL = 'FETCH_CURRENCY_FAIL';

const { brand } = getConfig();
export const submitSubscription = (values) => async (dispatch, getState) => {
  const {
    plan,
    source,
    selectedCard,
    defaultCardId,
    accessToken,
    coupon,
    trialEligible,
    trialPeriodDays,
    promotionId,
    promoCodesEnabled,
    promoCode,
    digitalWallet,
  } = values;
  dispatch(startSubmitSpinner());

  nrAddPageAction('Submit subscription started', {
    plan,
    payload: {
      selectedCard,
      defaultCardId,
      coupon,
      trialEligible,
      trialPeriodDays,
      promotionId,
      promoCodesEnabled,
      promoCode,
    },
  });

  // we must have either new CC entered OR existing default CC for submission
  const paymentSource = source || selectedCard;
  if (!plan || !paymentSource) {
    dispatch(stopSubmitSpinner());
    nrAddPageAction('Submit subscription failed - credit card required', {
      plan,
      payload: {
        selectedCard,
        defaultCardId,
        coupon,
      },
    });
    return dispatch({
      type: SUBMIT_SUBSCRIPTION_FAIL,
      message: 'A valid credit card is required.',
    });
  }
  if (promoCodesEnabled && !promoCode) {
    dispatch(stopSubmitSpinner());
    nrAddPageAction('Submit subscription failed - promo code required', {
      plan,
      payload: {
        promotionId,
        promoCodesEnabled,
        promoCode,
      },
    });
    return dispatch({ type: SUBMIT_SUBSCRIPTION_FAIL, message: 'A valid promo code is required.' });
  }

  const freeTrial = trialEligible && !!trialPeriodDays;

  try {
    dispatch({ type: SUBMIT_SUBSCRIPTION });
    nrAddPageAction('Subscription submitted', {
      plan,
    });
    const subscriptionApiParams = {
      planId: plan,
      coupon,
      freeTrial,
      source: source ? source.id : null,
      cardId: selectedCard !== defaultCardId ? selectedCard : defaultCardId,
      promotionId,
      brand,
      promoCode,
    };
    const { currency, name, amount } = getSelectedPlan(getState());
    const { subscriptionId } = await upgrade(accessToken, subscriptionApiParams);

    nrAddPageAction('Subscription checkout completed', {
      plan,
      payload: {
        subscriptionId,
        effectivePrice: amount,
      },
    });

    trackEvent('Completed Checkout Step', { step: 2, promotionId });
    trackEvent('Completed Order', {
      orderId: subscriptionId,
      total: 0,
      coupon,
      currency: currency.toUpperCase(),
      products: [
        {
          productId: plan,
          sku: plan,
          name,
          price: amount / 100,
          quantity: 1,
          category: 'Subscription',
        },
      ],
      promotionId,
      paymentSource: digitalWallet || 'credit card',
    });

    dispatch({ type: SUBMIT_SUBSCRIPTION_SUCCESS });
    nrAddPageAction('Subscription created: SUCCESS', {
      plan,
      payload: {
        subscriptionId,
      },
    });
    return dispatch(stopSubmitSpinner());
  } catch (error) {
    dispatch(stopSubmitSpinner());
    const { code, message } = error;
    if (code === 1001) {
      nrAddPageAction('Submit subscription failed - Duplicate card', { plan, error });
      return dispatch(showDuplicateCard({ message, source, plan }));
    }
    dispatch(clearDuplicateCard());
    if (code === 1002) {
      const errMessage = en.promotionFeatures.promoCodeError;
      nrAddPageAction('Submit subscription failed - Invalid promo code', { plan, error });
      return dispatch({ type: SUBMIT_SUBSCRIPTION_FAIL, message: errMessage });
    }
    nrAddPageAction('Submit subscription failed', { plan, error });
    nrNoticeError(error);
    return dispatch({ type: SUBMIT_SUBSCRIPTION_FAIL, message });
  }
};

export const fetchCurrency = () => async (dispatch, getState) => {
  try {
    dispatch({ type: FETCH_CURRENCY });

    const countryCode = getCountryCode(getState());

    const { currency } = await getCurrencyForUser(countryCode);
    return dispatch({
      type: FETCH_CURRENCY_SUCCESS,
      currency,
    });
  } catch (err) {
    const { message } = err;
    return dispatch({ type: FETCH_CURRENCY_FAIL, message });
  }
};
