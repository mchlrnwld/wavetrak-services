import { getWavetrakIdentity, setWavetrakIdentity, trackEvent } from '@surfline/web-common';
import getConfig from '../../config';
import { setAccessToken } from './auth';
import { generateToken, authorise } from '../../common/api/user';
import setLoginCookie from '../../utils/setLoginCookie';
import deviceInfo from '../../utils/deviceInfo';
import { getFunnelConfig } from '../selectors/promotion';

const config = getConfig();

export const FBLOGIN = 'FBLOGIN';
export const FBLOGIN_RESET = 'FBLOGIN_RESET';
export const FBLOGIN_SUCCESS = 'FBLOGIN_SUCCESS';
export const FBLOGIN_FAIL = 'FBLOGIN_FAIL';
export const FBLOGINERROR = 'FBLOGINERROR';
export const FBLOGINERROR_SUCCESS = 'FBLOGINERROR_SUCCESS';
export const FBLOGINERROR_FAIL = 'FBLOGINERROR_FAIL';
export const FBSUBMIT = 'FBSUBMIT';
export const FBSUBMIT_SUCCESS = 'FBSUBMIT_SUCCESS';
export const FBSUBMIT_FAIL = 'FBSUBMIT_FAIL';

const facebookSDKLogin = () =>
  new Promise((resolve, reject) => {
    // eslint-disable-next-line
    const { FB } = window;
    FB.login(
      (response, error) => {
        if (error) return reject(error);
        if (response.authResponse) return resolve(response);
        return resolve({});
      },
      { scope: 'public_profile, email' },
    );
  });

const facebookSDKGetUserInfo = () =>
  new Promise((resolve, reject) => {
    // eslint-disable-next-line
    const { FB } = window;
    FB.api(
      '/me',
      {
        fields: 'name, email, first_name, last_name',
      },
      (userInfo, error) => {
        if (error) return reject(error);
        if (!userInfo.email) return reject(new Error({ message: 'An email address is required ' }));
        return resolve(userInfo);
      },
    );
  });

export const loginWithFacebook = (location) => async (dispatch, getState) => {
  try {
    const { authResponse: facebookAuthResponse } = await facebookSDKLogin();
    if (!facebookAuthResponse) return dispatch({ type: FBLOGIN_RESET });

    const {
      id: facebookId,
      email,
      first_name: firstName,
      last_name: lastName,
    } = await facebookSDKGetUserInfo();

    dispatch({ type: 'FBLOGIN' });
    const { deviceId, deviceType } = deviceInfo();
    const valuesToEncode = {
      grant_type: 'password',
      fbToken: facebookAuthResponse.accessToken,
      brand: config.company,
      username: email,
      password: null,
      facebookId,
      firstName,
      lastName,
      device_id: deviceId,
      device_type: deviceType,
    };

    const urlEncodedFormValues = [];
    const keysToEncode = Object.keys(valuesToEncode);

    keysToEncode.forEach((key) => {
      const encodedKey = encodeURIComponent(key);
      const encodedValue = encodeURIComponent(valuesToEncode[key]);
      urlEncodedFormValues.push(`${encodedKey}=${encodedValue}`);
    });
    const staySignedIn = true;
    const urlEncodedFormString = urlEncodedFormValues.join('&');
    const {
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: expiresIn,
    } = await generateToken(!staySignedIn, urlEncodedFormString);

    const { id } = await authorise(accessToken);

    if (window?.analytics) {
      window.analytics.identify(id);
    }

    trackEvent('Signed In', { method: 'facebook', email, location, platform: 'web' });

    setWavetrakIdentity({
      ...getWavetrakIdentity(),
      email,
      userId: id,
      type: 'logged_in',
    });

    const { _id: promotionId } = getFunnelConfig(getState());

    if (location !== 'register') {
      trackEvent('Completed Checkout Step', { step: 1, promotionId });
    }

    const maxAge = staySignedIn ? expiresIn : 1800;
    const refreshMaxAge = staySignedIn ? 31536000 : 1800;

    await setLoginCookie(accessToken, refreshToken, maxAge, refreshMaxAge);
    dispatch(setAccessToken(accessToken));

    return dispatch({ type: FBLOGIN_SUCCESS });
  } catch (error) {
    if (error.status === 1001) {
      return dispatch({ type: FBLOGIN_RESET });
    }
    const errorMessage = error.error_description || error.message;
    return dispatch({ type: FBLOGIN_FAIL, error: errorMessage });
  }
};
