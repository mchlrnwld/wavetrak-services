import { Cookies } from 'react-cookie';
import getConfig from '../../config';
import { getEntitlement } from '../../common/api/entitlement';

const config = getConfig();
const cookie = new Cookies();

export const FETCH_ENTITLEMENTS = 'FETCH_ENTITLEMENTS';
export const FETCH_ENTITLEMENTS_SUCCESS = 'FETCH_ENTITLEMENTS_SUCCESS';
export const FETCH_ENTITLEMENTS_FAIL = 'FETCH_ENTITLEMENTS_FAIL';
export const DISABLE_FREE_TRIAL = 'DISABLE_FREE_TRIAL';
export const ENABLE_FREE_TRIAL = 'ENABLE_FREE_TRIAL';

export const fetchEntitlements = () => async (dispatch) => {
  dispatch({ type: FETCH_ENTITLEMENTS });
  try {
    const accessToken = cookie.get('access_token');
    const { entitlements, promotions } = await getEntitlement(accessToken);
    if (entitlements.includes(`${config.company}__premium`)) {
      window.location.assign(`${config.redirectUrl}/account/subscription`);
    }
    dispatch({ type: FETCH_ENTITLEMENTS_SUCCESS, entitlements, promotions });
  } catch (error) {
    dispatch({ type: FETCH_ENTITLEMENTS_FAIL, error });
  }
};

export const disableFreeTrial = () => async (dispatch) =>
  dispatch({ type: DISABLE_FREE_TRIAL, company: config.company });

export const enableFreeTrial = () => async (dispatch) =>
  dispatch({ type: ENABLE_FREE_TRIAL, company: config.company });
