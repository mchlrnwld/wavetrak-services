import { Cookies } from 'react-cookie';
import { getPlans } from '../../common/api/subscription';
import { getFunnelConfig } from '../selectors/promotion';
import getConfig from '../../config';

const config = getConfig();
const cookie = new Cookies();

export const FETCH_PLANS = 'FETCH_PLANS';
export const FETCH_PLANS_SUCCESS = 'FETCH_PLANS_SUCCESS';
export const FETCH_PLANS_FAIL = 'FETCH_PLANS_FAIL';
export const TOGGLE_SELECTED_PLANID = 'TOGGLE_SELECTED_PLANID';
export const SET_DISCOUNTED_PLANS = 'SET_DISCOUNTED_PLANS';
export const SET_PLANS_CURRENCY = 'SET_PLANS_CURRENCY';

export const fetchFilteredPlans = (countryISO) => async (dispatch, getState) => {
  dispatch({ type: FETCH_PLANS });
  try {
    const { company: brand } = config;
    const accessToken = cookie.get('access_token');
    if (accessToken) {
      const funnelConfig = getFunnelConfig(getState());
      const filter = funnelConfig.plans || 'all';
      const { plans } = await getPlans(accessToken, brand, filter, countryISO);

      const [{ symbol, iso }] = plans;
      dispatch({ type: SET_PLANS_CURRENCY, iso, symbol });

      const { id } =
        funnelConfig.plans === 'all'
          ? plans.find((plan) => plan.interval.includes('year'))
          : plans[0];
      dispatch({ type: FETCH_PLANS_SUCCESS, plans, selectedPlanId: id });
    }
  } catch (error) {
    dispatch({ type: FETCH_PLANS_FAIL, error });
  }
};

export const toggleSelectedPlanId = (selectedPlanId) => async (dispatch) => {
  dispatch({ type: TOGGLE_SELECTED_PLANID, selectedPlanId });
};

export const setDiscountedPlans = (plans) => async (dispatch) => {
  const { id } = plans.find((plan) => plan.interval.includes('year'));
  dispatch({ type: 'SET_DISCOUNTED_PLANS', plans, selectedPlanId: id });
};
