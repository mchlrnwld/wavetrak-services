import {
  trackEvent,
  SL,
  nrNoticeError,
  setWavetrakIdentity,
  getWavetrakIdentity,
} from '@surfline/web-common';
import { generateToken, authorise, validateEmail } from '../../common/api/user';
import { getEntitlement } from '../../common/api/entitlement';
import setLoginCookie from '../../utils/setLoginCookie';
import deviceInfo from '../../utils/deviceInfo';
import { setAccessToken } from './auth';
import { stopSubmitSpinner, startSubmitSpinner } from './interactions';
import getConfig from '../../config';

export const CONFIRM_EMAIL = 'CONFIRM_EMAIL';
export const LOGIN = 'LOGIN';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';

export const login = (values) => async (dispatch) => {
  const { email, password, staySignedIn, location, promotionId } = values;
  const { company, redirectUrl, brand } = getConfig();

  try {
    dispatch(startSubmitSpinner());
    const { deviceId, deviceType } = deviceInfo();

    const valuesToEncode = {
      grant_type: 'password',
      username: email,
      password,
      device_id: deviceId,
      device_type: deviceType,
      forced: true,
    };

    const urlEncodedFormValues = [];
    const keysToEncode = Object.keys(valuesToEncode);

    keysToEncode.forEach((key) => {
      const encodedKey = encodeURIComponent(key);
      const encodedValue = encodeURIComponent(valuesToEncode[key]);
      urlEncodedFormValues.push(`${encodedKey}=${encodedValue}`);
    });

    const urlEncodedFormString = urlEncodedFormValues.join('&');

    const {
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: expiresIn,
    } = await generateToken(!staySignedIn, urlEncodedFormString);

    const { id } = await authorise(accessToken);

    const maxAge = values.staySignedIn ? expiresIn : 1800;
    const refreshMaxAge = values.staySignedIn ? 31536000 : 1800;
    await setLoginCookie(accessToken, refreshToken, maxAge, refreshMaxAge);

    if (window?.analytics) {
      window.analytics.identify(id);
    }
    trackEvent('Signed In', {
      method: 'email',
      email,
      location,
      promotionId,
      platform: 'web',
    });

    setWavetrakIdentity({
      ...getWavetrakIdentity(),
      email,
      userId: id,
      type: 'logged_in',
    });

    const { entitlements } = await getEntitlement(accessToken);

    if (entitlements.includes(`${company}_premium`)) {
      return window.location.assign(`${redirectUrl}/account/subscription`);
    }

    if (location !== 'register') {
      trackEvent('Completed Checkout Step', { step: 1, promotionId });
    }

    dispatch(setAccessToken(accessToken));
    dispatch(stopSubmitSpinner());

    return dispatch({ type: LOGIN_SUCCESS });
  } catch (error) {
    if (brand === SL) {
      try {
        const validated = await validateEmail(email);
        if (!validated?.hasPassword) {
          return dispatch({ type: CONFIRM_EMAIL, email });
        }
      } catch (innerError) {} // eslint-disable-line
    }
    const err = { _error: error.error_description };
    nrNoticeError(error.error_description);
    dispatch(stopSubmitSpinner());
    dispatch({ type: LOGIN_FAIL, error: error.error_description });
    throw err;
  }
};
