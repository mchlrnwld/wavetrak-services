import { Cookies } from 'react-cookie';
import { getCards } from '../../common/api/subscription';

const cookie = new Cookies();

export const FETCH_CARDS = 'FETCH_CARDS';
export const FETCH_CARDS_SUCCESS = 'FETCH_CARDS_SUCCESS';
export const FETCH_CARDS_FAIL = 'FETCH_CARDS_FAIL';
export const TOGGLE_SHOW_CARDS = 'TOGGLE_SHOW_CARDS';
export const SET_SELECTED_CARD_SOURCE = 'SET_SELECTED_CARD_SOURCE';

export const fetchCards = () => async (dispatch) => {
  try {
    dispatch({ type: FETCH_CARDS });
    const accessToken = cookie.get('access_token');
    const { cards, defaultSource } = await getCards(accessToken);
    const showCards = !!cards?.length;
    return dispatch({
      type: FETCH_CARDS_SUCCESS,
      cards,
      defaultSource,
      showCards,
    });
  } catch (error) {
    const { message } = error;
    return dispatch({ type: FETCH_CARDS_FAIL, error: message });
  }
};

export const toggleShowCards = (showCards) => async (dispatch) => {
  dispatch({ type: TOGGLE_SHOW_CARDS, showCards: !showCards });
};

export const setSelectedCardSource = (cardId) => async (dispatch) => {
  dispatch({ type: SET_SELECTED_CARD_SOURCE, selectedCard: cardId });
};
