import { trackEvent } from '@surfline/web-common';
import getConfig from '../../config';

const config = getConfig();

export const TOGGLE_REGISTRATION_FORM = 'TOGGLE_REGISTRATION_FORM';
export const START_SUBMIT_SPINNER = 'START_SUBMIT_SPINNER';
export const STOP_SUBMIT_SPINNER = 'STOP_SUBMIT_SPINNER';

export const toggleRegistrationForm = (showLogin, location) => async (dispatch) => {
  const event = showLogin
    ? 'Sign In Form Clicked Create Account'
    : 'Create Account Form Clicked Sign In';
  const promotionId = config.funnelConfig ? config.funnelConfig._id : undefined;
  trackEvent(event, { location, promotionId });
  dispatch({ type: TOGGLE_REGISTRATION_FORM, showLogin: !showLogin });
};

export const startSubmitSpinner = () => async (dispatch) => {
  dispatch({ type: START_SUBMIT_SPINNER });
};

export const stopSubmitSpinner = () => async (dispatch) => {
  dispatch({ type: STOP_SUBMIT_SPINNER });
};
