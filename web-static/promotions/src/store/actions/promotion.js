import { getPromotion } from '../../common/api/promotion';
import getPromotionConfig from '../../utils/getPromotionConfig';
import { getTrialEligibility } from '../selectors/entitlements';

export const FETCH_PROMOTION = 'FETCH_PROMOTION';
export const FETCH_PROMOTION_SUCCESS = 'FETCH_PROMOTION_SUCCESS';
export const FETCH_PROMOTION_FAILURE = 'FETCH_PROMOTION_FAILURE';
export const SET_PROMO_CODE = 'SET_PROMO_CODE';
export const SET_PROMO_CODE_FAILURE = 'SET_PROMO_CODE_FAILURE';

export const fetchPromotion = () => async (dispatch, getState) => {
  // The promotionId is set within the window scope by web-proxy
  const { promotionId } = window;

  try {
    dispatch({ type: FETCH_PROMOTION });
    const trialEligible = getTrialEligibility(getState());
    const promotion = await getPromotion(promotionId);
    const funnelConfig = await getPromotionConfig(promotion, trialEligible);
    return dispatch({ type: FETCH_PROMOTION_SUCCESS, promotion, funnelConfig });
  } catch (err) {
    const { message } = err;
    return dispatch({ type: FETCH_PROMOTION_FAILURE, message });
  }
};

export const setPromoCode = (promoCode) => async (dispatch) => {
  try {
    dispatch({ type: SET_PROMO_CODE, promoCode });
  } catch (error) {
    dispatch({ type: SET_PROMO_CODE_FAILURE, error });
  }
};
