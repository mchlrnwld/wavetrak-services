import createReducer from './createReducer';
import {
  FETCH_USER,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
  CREATE,
  CREATE_SUCCESS,
  CREATE_FAIL,
  SET_COUNTRY_CODE,
} from '../actions/user';

const initialState = {
  error: null,
  loading: false,
  details: null,
  countryCode: null,
};

const handlers = {};

handlers[FETCH_USER] = (state) => ({
  ...state,
  loading: true,
});
handlers[FETCH_USER_SUCCESS] = (state, { user, accessToken }) => ({
  ...state,
  details: {
    ...user,
  },
  accessToken,
  error: null,
  loading: false,
});
handlers[FETCH_USER_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});
handlers[CREATE] = (state) => ({
  ...state,
  loading: true,
});
handlers[CREATE_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});
handlers[CREATE_SUCCESS] = (state, { accessToken }) => ({
  ...state,
  accessToken,
  loading: false,
});

handlers[SET_COUNTRY_CODE] = (state, { countryCode }) => {
  return {
    ...state,
    countryCode,
  };
};

export default createReducer(handlers, initialState);
