import createReducer from './createReducer';
import {
  FETCH_PROMOTION,
  FETCH_PROMOTION_SUCCESS,
  FETCH_PROMOTION_FAILURE,
  SET_PROMO_CODE,
  SET_PROMO_CODE_FAILURE,
} from '../actions/promotion';

const initialState = {
  error: null,
  loading: false,
};
const handlers = {};

handlers[FETCH_PROMOTION] = (state) => {
  return {
    ...state,
    loading: true,
  };
};

handlers[FETCH_PROMOTION_SUCCESS] = (state, { promotion, funnelConfig }) => {
  return {
    ...state,
    ...promotion,
    funnelConfig,
    error: null,
    loading: false,
  };
};

handlers[FETCH_PROMOTION_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});

handlers[SET_PROMO_CODE] = (state, { promoCode }) => {
  return {
    ...state,
    promoCode,
    error: null,
    loading: false,
  };
};

handlers[SET_PROMO_CODE_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});

export default createReducer(handlers, initialState);
