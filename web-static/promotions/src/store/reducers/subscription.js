import createReducer from './createReducer';
import {
  SUBMIT_SUBSCRIPTION,
  SUBMIT_SUBSCRIPTION_SUCCESS,
  SUBMIT_SUBSCRIPTION_FAIL,
  FETCH_CURRENCY,
  FETCH_CURRENCY_SUCCESS,
  FETCH_CURRENCY_FAIL,
} from '../actions/subscription';

const initialState = {
  error: null,
  success: null,
};
const handlers = {};

handlers[SUBMIT_SUBSCRIPTION] = (state) => ({
  ...state,
  error: null,
});

handlers[SUBMIT_SUBSCRIPTION_SUCCESS] = (state) => ({
  ...state,
  error: null,
  success: true,
});

handlers[SUBMIT_SUBSCRIPTION_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
});

handlers[FETCH_CURRENCY] = (state) => ({
  ...state,
});

handlers[FETCH_CURRENCY_SUCCESS] = (state, { currency }) => ({
  ...state,
  currency: currency.toLowerCase(),
});

handlers[FETCH_CURRENCY_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
});

export default createReducer(handlers, initialState);
