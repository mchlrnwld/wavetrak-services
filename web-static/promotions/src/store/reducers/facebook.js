import createReducer from './createReducer';
import { FBLOGIN, FBLOGIN_RESET, FBLOGIN_SUCCESS, FBLOGIN_FAIL } from '../actions/facebook';

const initialState = {
  fbSubmitting: false,
  fbSubmitSuccess: false,
  fbLoginError: null,
};

const handlers = {};

handlers[FBLOGIN_RESET] = (state) => ({
  ...state,
  ...initialState,
});

handlers[FBLOGIN] = (state) => ({
  ...state,
  fbSubmitting: true,
  fbSubmitSuccess: false,
});

handlers[FBLOGIN_SUCCESS] = (state) => ({
  ...state,
  fbSubmitting: false,
  fbSubmitSuccess: true,
});

handlers[FBLOGIN_FAIL] = (state, { error }) => ({
  ...state,
  fbSubmitting: false,
  fbLoginError: error,
  fbSubmitSuccess: false,
});

export default createReducer(handlers, initialState);
