import { combineReducers } from 'redux';

import user from './user';
import promotion from './promotion';
import interactions from './interactions';
import plans from './plans';
import cards from './cards';
import auth from './auth';
import facebook from './facebook';
import entitlements from './entitlements';
import login from './login';
import subscription from './subscription';
import duplicateCard from './duplicateCard';

export default combineReducers({
  user,
  promotion,
  interactions,
  plans,
  cards,
  auth,
  facebook,
  entitlements,
  login,
  subscription,
  duplicateCard,
});
