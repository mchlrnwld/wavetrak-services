import createReducer from './createReducer';
import { CONFIRM_EMAIL, LOGIN, LOGIN_FAIL, LOGIN_SUCCESS } from '../actions/login';

const initialState = {
  loginError: null,
  confirmEmail: false,
};
const handlers = {};

handlers[LOGIN] = (state) => ({
  ...state,
});

handlers[LOGIN_SUCCESS] = (state) => ({
  ...state,
});

handlers[LOGIN_FAIL] = (state, { error }) => ({
  ...state,
  loginError: error,
});

handlers[CONFIRM_EMAIL] = (state) => ({
  ...state,
  confirmEmail: true,
});

export default createReducer(handlers, initialState);
