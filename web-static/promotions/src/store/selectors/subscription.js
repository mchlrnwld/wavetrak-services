export const getSubscriptionCreateError = (state) => {
  if (state) {
    const {
      subscription: { error },
    } = state;
    return error;
  }
  return null;
};

export const getSubscriptionSuccess = (state) => {
  if (state) {
    const {
      subscription: { success },
    } = state;
    return success;
  }
  return null;
};
