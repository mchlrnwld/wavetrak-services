export const getFunnelConfig = (state) => {
  if (state) {
    const {
      promotion: { funnelConfig },
    } = state;
    return funnelConfig;
  }
  return null;
};

export const getPromotionId = (state) => {
  if (state) {
    const {
      promotion: {
        funnelConfig: { _id: promotionId },
      },
    } = state;
    return promotionId;
  }
  return null;
};

export const getTrialPeriodDays = (state) => {
  if (state) {
    const {
      promotion: {
        funnelConfig: { trialLength },
      },
    } = state;
    return trialLength;
  }
  return null;
};

export const getPromoCode = (state) => {
  if (state) {
    const {
      promotion: { promoCode },
    } = state;
    return promoCode;
  }
  return null;
};

export const getCurrencyEligibility = (state) => {
  if (state) {
    const {
      promotion: { currency },
      subscription: { currency: currencyOfRegion },
    } = state;
    const promoCurrency = currency.toLowerCase();
    if (promoCurrency !== 'any' && promoCurrency !== currencyOfRegion) {
      return false;
    }
    return true;
  }
  return null;
};
