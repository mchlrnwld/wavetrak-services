export const getAccessToken = (state) => {
  if (state) {
    const {
      user: { accessToken },
    } = state;
    return accessToken;
  }
  return null;
};

export const getUser = (state) => {
  if (state) {
    const { user } = state;
    return user;
  }
  return null;
};

export const getUserEntitlements = (state) => {
  if (state) {
    const {
      user: { entitlements },
    } = state;
    return entitlements;
  }
  return null;
};

export const isUserPremium = (state) => {
  if (state) {
    const {
      user: { isPremium },
    } = state;
    return isPremium;
  }
  return null;
};

export const isUserTrialEligible = (state) => {
  if (state) {
    const {
      user: { trialEligible },
    } = state;
    return trialEligible;
  }
  return null;
};

export const getTrialEligibility = (state) => {
  const { promotion } = state;
  const { funnelConfig } = promotion;
  if (funnelConfig) {
    if (funnelConfig.trialLength === 0) return false;
  }
  const { brand } = funnelConfig;
  const data = state.user.promotions;
  if (data && data.includes(`${brand}_free_trial`)) return true;
  return false;
};

export const getPlans = (state) => {
  if (state) {
    const {
      user: { plans },
    } = state;
    return plans;
  }
  return null;
};

export const getCards = (state) => {
  if (state) {
    const {
      user: { cards },
    } = state;
    return cards;
  }
  return null;
};

export const getCreateUserError = (state) => {
  if (state) {
    const {
      user: { error },
    } = state;
    return error;
  }
  return null;
};

export const getCountryCode = (state) => state.user?.countryCode;
