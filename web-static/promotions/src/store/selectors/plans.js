export const getPremiumPlans = (state) => {
  const { plans } = state.plans;
  if (plans) return plans;
  return null;
};

export const getSelectedPlanId = (state) => {
  const { selectedPlanId } = state.plans;
  if (selectedPlanId) return selectedPlanId;
  return null;
};

export const getSelectedPlanInterval = (state) => {
  const { selectedPlanId, plans } = state.plans;
  if (selectedPlanId) {
    const { interval } = plans.find(({ id }) => id === selectedPlanId);
    const intervalString = interval.includes('year') ? 'Annual' : 'Monthly';
    return intervalString;
  }
  return null;
};

export const getSelectedPlan = (state) => {
  const { selectedPlanId, plans } = state.plans;
  const selectedPlan = plans.find(({ id }) => id === selectedPlanId);
  return selectedPlan;
};

export const getCurrency = (state) => {
  const { iso } = state.plans;
  if (iso) {
    return iso;
  }
  return null;
};
