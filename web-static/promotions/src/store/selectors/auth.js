export const getAccessToken = (state) => {
  const { accessToken } = state.auth;
  if (accessToken) return accessToken;
  return null;
};

export const getRefreshToken = (state) => {
  const { refreshToken } = state.auth;
  if (refreshToken) return refreshToken;
  return null;
};

export const getLoginError = (state) => {
  if (state) {
    const {
      login: { loginError },
    } = state;
    return loginError;
  }
  return null;
};

export const getFbLoginError = (state) => {
  if (state) {
    const {
      facebook: { fbLoginError },
    } = state;
    return fbLoginError;
  }
  return null;
};

export const isUserInitiatedLogin = (state) => {
  if (state) {
    const {
      auth: { userInitiated },
    } = state;
    return userInitiated;
  }
  return null;
};

export const getConfirmEmail = (state) => {
  if (state) {
    const {
      login: { confirmEmail },
    } = state;
    return confirmEmail;
  }
  return null;
};
