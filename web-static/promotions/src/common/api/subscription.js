import fetch from '../../utils/fetch';

export const getPlans = (accessToken, brand, filter = 'all', countryISO = 'US') => {
  const urlPath = `/pricing-plans?brand=${brand}&filter=${filter}&countryISO=${countryISO}`;
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const getCards = (accessToken) => {
  const urlPath = '/credit-cards';
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const upgrade = (accessToken, subscriptionValues) =>
  fetch('/subscriptions/promotion', accessToken, {
    method: 'POST',
    body: JSON.stringify({ ...subscriptionValues }),
  });

export const getCurrencyForUser = (countryISO) => {
  const urlPath = `/subscriptions/currency?countryISO=${countryISO}`;
  return fetch(urlPath, null, { method: 'GET' });
};

export default getPlans;
