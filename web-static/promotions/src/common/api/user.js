import fetch from '../../utils/fetch';
import getConfig from '../../config';

const { clientId, clientSecret } = getConfig();

export const getUser = (accessToken) => fetch('user', accessToken, { method: 'GET' });

export const postUser = (isShortLived, user) =>
  fetch(`user?isShortLived=${isShortLived}`, null, {
    method: 'POST',
    body: JSON.stringify(user),
  });

export const generateToken = (isShortLived, userInfo) =>
  fetch(
    `/trusted/token?isShortLived=${isShortLived}`,
    null,
    {
      method: 'POST',
      headers: {
        accept: 'application/json',
        Authorization: `Basic ${Buffer.from(`${clientId}:${clientSecret}`).toString('base64')}`,
        'content-type': 'application/x-www-form-urlencoded',
        credentials: 'same-origin',
      },
      body: userInfo,
    },
    'application/x-www-form-urlencoded',
  );

export const authorise = (accessToken) =>
  fetch(`trusted/authorise?access_token=${accessToken}`, null, { method: 'GET' });

export const validateEmail = (email) =>
  fetch(`/user/validateEmail?email=${email}`, null, { method: 'GET' });
