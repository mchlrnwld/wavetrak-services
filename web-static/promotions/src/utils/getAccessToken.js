import { Cookies } from 'react-cookie';

const cookies = new Cookies();

export const getAccessToken = () => cookies.get('access_token');

export const getRefreshToken = () => cookies.get('refresh_token');
