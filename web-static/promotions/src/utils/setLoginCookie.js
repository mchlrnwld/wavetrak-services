/* eslint-disable no-restricted-globals */
import { Cookies } from 'react-cookie';
import getConfig from '../config';

const { company } = getConfig();
const cookie = new Cookies();

export default async (accessToken, refreshToken, maxAge, refreshMaxAge = 31536000) => {
  const domainName = company === 'fs' ? '.fishtrack.com' : location.hostname;
  cookie.set('access_token', accessToken, {
    path: '/',
    maxAge,
    expires: new Date(new Date().getTime() + maxAge * 1000),
    domain: domainName,
    secure: false,
  });

  cookie.set('refresh_token', refreshToken, {
    path: '/',
    maxAge: refreshMaxAge,
    expires: new Date(new Date().getTime() + refreshMaxAge * 1000),
    domain: domainName,
    secure: false,
  });

  return { accessToken, refreshToken };
};
