import { useRef, useEffect } from 'react';

/**
 * This hook is a replacement for componentDidUpdate.
 * Use it to mimic the behavior of componentDidUpdate, not running on mount
 */
const useComponentDidUpdate = (callback, deps) => {
  const hasMount = useRef(false);

  useEffect(() => {
    if (hasMount.current) {
      callback();
    } else {
      hasMount.current = true;
    }
  }, deps); // eslint-disable-line react-hooks/exhaustive-deps
};

export default useComponentDidUpdate;
