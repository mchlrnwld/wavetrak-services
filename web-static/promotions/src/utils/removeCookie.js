/* eslint-disable no-restricted-globals */
import { Cookies } from 'react-cookie';
import getConfig from '../config';

const { company } = getConfig();
const cookie = new Cookies();

export default async (cookieName) => {
  const domainName = company === 'fs' ? '.fishtrack.com' : location.hostname;
  return cookie.remove(cookieName, {
    path: '/',
    domain: domainName,
    secure: false,
  });
};
