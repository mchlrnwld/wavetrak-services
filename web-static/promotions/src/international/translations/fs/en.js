import { SatelliteIcon, WaypointIcon, LayersIcon } from '@surfline/quiver-react';
import {
  FishtrackLogo,
  CurrentsAlt as PromotionCurrents,
  SatelliteAlt as PromotionSatellite,
  WaypointAlt as PromotionWaypoint,
} from '../../../common/components/icons';
import base from '../base';

export default {
  rail_cta: {
    ...base.rail_cta.fs,
  },
  premiumFeatures: {
    features: [
      {
        Icon: SatelliteIcon,
        header: 'Latest Satellite Imagery',
        description: 'Locate temperature and color breaks offshore.',
      },
      {
        Icon: WaypointIcon,
        header: 'Waypoints And Routes',
        description: 'Measure distances and plot routes for trips offshore.',
      },
      {
        Icon: LayersIcon,
        header: 'Currents, Bathymetry And Altimetry',
        description: 'Use premium overlays to pinpoint areas of activity.',
      },
    ],
  },
  promotionFeatures: {
    BrandLogo: FishtrackLogo,
    header: 'Get everything you need to find more fish',
    features: [
      {
        Icon: PromotionSatellite,
        header: 'Satellite Imagery',
      },
      {
        Icon: PromotionWaypoint,
        header: 'Waypoint and Routes',
      },
      {
        Icon: PromotionCurrents,
        header: 'Currents and Bathymetry',
      },
    ],
    promoCodeError:
      'Invalid Code. To receive this offer you must enter a valid code. If you do not have one, you can still upgrade to FishTrack Premium at www.fishtrack.com/upgrade/',
  },
  payment_success: {
    subTitle: 'Welcome to Fistrack Premium!',
    message:
      'Thank you for joining the Fistrack family.  You can now access the latest satellite fishing chards and overlays.  See you in the water.',
    linkText: 'Return to Fishtrack',
    linkAddress: 'https://fishtrack.com',
  },
  create_form: {
    ...base.create_form,
    meta_title: 'Create Your Premium Account - FishTrack',
    meta_description:
      'FishTrack Fishing Charts - Create a FishTrack account to view the latest SST, Chlorophyll and True-Color satellite charts.',
  },
  sign_in_funnel: {
    ...base.sign_in_funnel,
    title_no_trial: 'Sign in to join<br/>FishTrack Premium',
    meta_title: 'Sign In to Go Premium - FishTrack',
    meta_description:
      'Get the latest satellite imagery - sign in with your FishTrack account to go Premium.',
  },
  duplicateCard: {
    ...base.duplicateCard,
  },
  utils: {
    ...base.utils,
  },
  header: 'FishTrack',
};
