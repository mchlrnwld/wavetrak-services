import { SwellLinesIcon, PremiumAnalysisWindIcon, BuoyIcon } from '@surfline/quiver-react';
import {
  BuoyweatherLogo,
  ForecastsAlt as PromotionForecasts,
  WeatherAlt as PromotionWeather,
  WindAlt as PromotionWind,
} from '../../../common/components/icons';
import base from '../base';

export default {
  rail_cta: {
    ...base.rail_cta.bw,
  },
  premiumFeatures: {
    features: [
      {
        Icon: SwellLinesIcon,
        header: 'Marine Forecasts For Any Point',
        description: 'View accurate 16-day wind, wave and tide forecasts.',
      },
      {
        Icon: PremiumAnalysisWindIcon,
        header: 'High-Resolution Wind Forecasts',
        description: 'Access a full collection of weather charts and forecasts.',
      },
      {
        Icon: BuoyIcon,
        header: 'Pick Your Weather Window',
        description: 'Long-range forecasts help you stay safe on the water.',
      },
    ],
  },
  promotionFeatures: {
    BrandLogo: BuoyweatherLogo,
    header: 'Get the most important information needed to maximize your time on the water',
    features: [
      {
        Icon: PromotionForecasts,
        header: 'Marine Forecasts',
      },
      {
        Icon: PromotionWind,
        header: 'Wind Forecasts',
      },
      {
        Icon: PromotionWeather,
        header: 'Pick Weather Windows',
      },
    ],
    promoCodeError:
      'Invalid Code. To receive this offer you must enter a valid code. If you do not have one, you can still upgrade to BuoyWeather Premium at www.buoyweather.com/upgrade/',
  },
  payment_success: {
    subTitle: 'Welcome to Buoyweather Premium!',
    message:
      "Thank you for joining the Buoyweather family.  You're now on your way to safer seas with the most trusted marine weather information.",
    linkText: 'Return to Buoyweather',
    linkAddress: 'https://buoyweather.com',
  },
  create_form: {
    ...base.create_form,
    meta_title: 'Create Account - Buoyweather',
    meta_description:
      'Stay safe on the water – create an account with Buoyweather to gain instant access to point-based marine forecasts.',
  },
  sign_in_funnel: {
    ...base.sign_in_funnel,
    title_no_trial: 'Sign in to join<br/>Buoyweather Premium',
    meta_title: 'Sign In to Go Premium - Buoyweather',
    meta_description:
      'Get the latest 16-day marine forecasts – sign in with your Buoyweather account to go Premium.',
  },
  duplicateCard: {
    ...base.duplicateCard,
  },
  utils: {
    ...base.utils,
  },
  header: 'Buoyweather',
};
