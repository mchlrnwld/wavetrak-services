import { CamsIcon, AnalysisIcon, ChartsIcon, CamRewindIcon } from '@surfline/quiver-react';
import {
  SurflineLogo,
  CamsAlt as PromotionCams,
  AnalysisAlt as PromotionAnalysis,
  ChartsAlt as PromotionCharts,
} from '../../../common/components/icons';
import base from '../base';

export default {
  rail_cta: {
    ...base.rail_cta.sl,
  },
  premiumFeatures: {
    features: [
      {
        Icon: CamsIcon,
        header: 'Ad-Free and Premium Cams',
        description: 'Watch live conditions ad-free -- and access exclusive, high-resolution cams.',
      },
      {
        Icon: AnalysisIcon,
        header: 'Trusted Forecast Team',
        description: 'Plan your next trip or session with Surfline’s expert forecast guidance.',
      },
      {
        Icon: ChartsIcon,
        header: 'Exclusive Forecast Tools',
        description: 'Access curated condition reports and proprietary surf, wind and buoy data.',
      },
      {
        Icon: CamRewindIcon,
        header: 'Cam Rewind',
        description:
          'Relive an epic session by downloading your rides -- then share ‘em with friends.',
      },
    ],
  },
  promotionFeatures: {
    BrandLogo: SurflineLogo,
    header: 'Score better waves more often',
    features: [
      {
        Icon: PromotionCams,
        header: 'Ad-Free and Premium Cams',
      },
      {
        Icon: PromotionAnalysis,
        header: 'Trusted Forecast Team',
      },
      {
        Icon: PromotionCharts,
        header: 'Exclusive Forecast Tools',
      },
    ],
    promoCodeError:
      'Invalid Code. To receive this offer you must enter a valid code. If you do not have one, you can still upgrade to Surfline Premium at www.surfline.com/upgrade/',
  },
  payment_success: {
    subTitle: 'Welcome to Surfline Premium!',
    message:
      "Thank you for joining the Surfline family.  You're now on your way to scoring better waves, more often.  See you in the water.",
    linkText: 'Return to Surfline',
    linkAddress: 'https://surfline.com',
  },
  create_form: {
    ...base.create_form,
  },
  sign_in_funnel: {
    ...base.sign_in_funnel,
  },
  duplicateCard: {
    ...base.duplicateCard,
  },
  utils: {
    ...base.utils,
  },
  header: 'Surfline',
};
