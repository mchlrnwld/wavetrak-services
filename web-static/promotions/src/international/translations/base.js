import { WaveIcon } from '@surfline/quiver-react';
import { brandToString } from '@surfline/web-common';

export default {
  rail_cta: {
    sl: {
      header: 'Premium Benefits',
      mobileHeader: 'Go Premium',
      mobileSubtitle: 'Learn More About Surfline Premium',
      MobileIcon: WaveIcon,
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
    },
    bw: {
      mobileHeader: 'Go Premium',
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
      header: 'Buoyweather',
    },
    fs: {
      mobileHeader: 'Go Premium',
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
      header: 'FishTrack',
    },
  },
  create_form: {
    already_have_account: 'Already have an account?',
    sign_in_link: ' Sign In',
    discount_applied: 'Your discount has been applied.',
    label_first: 'First Name',
    label_last: 'Last Name',
    label_email: 'Email',
    email_error_message: 'Email in use, ',
    email_error_message_link: 'sign in?',
    label_password: 'Password',
    create_account_button: 'Create account',
    staySignedIn_label: 'Keep me signed in',
    receivePromotions_label: (brand) =>
      `Send me product and marketing updates from ${brandToString(brand)} and its partners`,
    disclaimer_text_start: 'By clicking "Create Account" you agree to Surfline ',
    terms_of_use_link_text: 'Terms of use',
    disclaimer_and: ' and ',
    privacy_policy_link_text: 'Privacy policy',
    password_helper_text: 'Minimum 6 characters',
    meta_title: 'Create Account - Surfline',
    meta_description:
      'Don’t miss another good surf – create an account with Surfline to gain instant access to Surfline.',
  },
  sign_in_funnel: {
    title: 'Sign in to start free trial',
    title_no_trial: 'Sign in to join Surfline Premium',
    meta_title: 'Sign In to Go Premium - Surfline',
    meta_description:
      "Don't miss another good surf - sign in with your Surfline account to go Premium.",
  },
  duplicateCard: {
    header: 'This card has been used for a free trial before',
    description: (amount) =>
      `This card is not eligible for a free trial. Please use another card to start your trial, or continue to Go Premium. You will be charged $${amount} today.`,
    goPremium: 'GO PREMIUM',
    expirationDate: (date) => `EXPIRES ${date}`,
    goBack: 'GO BACK',
  },
  utils: {
    inactive_promotion: (brandName) =>
      `This promotion has expired, but you can still sign-up for ${brandName} Premium.`,
  },
};
