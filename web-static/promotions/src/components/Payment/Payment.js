import { useEffect } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import getConfig from '../../config';
import { PaymentForm, Loading } from '..';
import { fetchCards } from '../../store/actions/cards';
import { fetchFilteredPlans } from '../../store/actions/plans';
import { getPremiumPlans } from '../../store/selectors/plans';
import { getCountryCode } from '../../store/selectors/user';

const config = getConfig();
const stripePromise = loadStripe(config.stripe.publishableKey);

const Payment = () => {
  const dispatch = useDispatch();
  const plans = useSelector(getPremiumPlans, shallowEqual);
  const countryCode = useSelector(getCountryCode);

  useEffect(() => {
    dispatch(fetchCards());
  }, [dispatch]);

  useEffect(() => {
    dispatch(fetchFilteredPlans(countryCode));
  }, [countryCode, dispatch]);

  return (
    <div>
      <div>
        {plans ? (
          <Elements stripe={stripePromise}>
            <PaymentForm />
          </Elements>
        ) : (
          <Loading />
        )}
      </div>
    </div>
  );
};

export default Payment;
