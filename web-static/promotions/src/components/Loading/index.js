import './Loading.scss';
import { Spinner } from '@surfline/quiver-react';

export default () => {
  return (
    <div className="sl-promotions-loading">
      <Spinner />
    </div>
  );
};
