import PropTypes from 'prop-types';

import './PremiumAnnualPlan.scss';

const PremiumAnnualPlan = ({ plan }) => {
  const { amount, trialPeriodDays } = plan;
  const cost = amount / 100;
  return (
    <div className="premium-plans__container">
      <div className="premium-plans">
        <div className="premium-plan">
          <div className="premium-plan__details">
            <div className="premium-plan__name">
              <div className="premium-plan__name--plan">Yearly Subscription</div>
              <div className="premium-plan__name--trial">{`${trialPeriodDays}-Day Free Trial`}</div>
            </div>
            <div className="premium-plan__price">
              <div className="premium-plan__price--cost">
                ${parseFloat(amount / 100 / 12).toFixed(2)}
              </div>
              <div className="premium-plan__price--total">
                {`/month - Billed $${cost} once a year`}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

PremiumAnnualPlan.propTypes = {
  plan: PropTypes.shape({
    amount: PropTypes.number.isRequired,
    trialPeriodDays: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  }).isRequired,
};

export default PremiumAnnualPlan;
