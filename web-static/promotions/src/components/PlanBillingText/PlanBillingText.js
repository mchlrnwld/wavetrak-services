import { useSelector, shallowEqual } from 'react-redux';
import { addDays, addYears, addMonths, format } from 'date-fns';
import { getTrialEligibility } from '../../store/selectors/entitlements';
import { getTrialPeriodDays } from '../../store/selectors/promotion';
import { getSelectedPlanInterval, getCurrency } from '../../store/selectors/plans';

import './PlanBillingText.scss';

const formatTrialEndDate = (trialPeriodDays) => {
  const trialEnd = addDays(new Date(), trialPeriodDays);
  return format(trialEnd, 'MM/dd/yy');
};

const formatRenewalDate = (trialPeriodDays = 0, interval) => {
  const daysToAdd = interval === 'Annual' ? trialPeriodDays : 0;
  const firstBillingDate = addDays(new Date(), daysToAdd);
  const renewal =
    interval === 'Annual' ? addYears(firstBillingDate, 1) : addMonths(firstBillingDate, 1);
  return format(renewal, 'MM/dd/yy');
};

const getSubtitleText = (trialEligible, trialPeriodDays, interval, currency) => {
  const eligible = trialEligible && !!trialPeriodDays && interval === 'Annual';

  const trialText = eligible
    ? `after free trial ends on ${formatTrialEndDate(trialPeriodDays)}`
    : 'today';

  return `${interval} billing begins ${trialText} and recurs on ${formatRenewalDate(
    trialPeriodDays,
    interval,
  )}. Billing is in ${currency.toUpperCase()}.`;
};

const PlanBillingText = () => {
  const trialEligible = useSelector(getTrialEligibility, shallowEqual);
  const trialPeriodDays = useSelector(getTrialPeriodDays);
  const interval = useSelector(getSelectedPlanInterval);
  const currency = useSelector(getCurrency);

  return (
    <div className="billing-text">
      {getSubtitleText(trialEligible, trialPeriodDays, interval, currency)}
    </div>
  );
};

export default PlanBillingText;
