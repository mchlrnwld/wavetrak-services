export VisaCard from './Visa';
export MasterCard from './Mastercard';
export AmexCard from './Amex';
export DiscoverCard from './Discover';
export DinersClubCard from './DinersClub';
export JCBCard from './JCB';
export UnionPayCard from './Unionpay';
export ChevronAlt from './ChevronAlt';
