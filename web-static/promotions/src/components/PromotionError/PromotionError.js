import React from 'react';
import { brandToString } from '@surfline/web-common';
import getConfig from '../../config';
import en from '../../international/translations/en';
import './PromotionError.scss';

const { upgradeRedirectUrl, company } = getConfig();
const PromotionError = () => (
  <div className="promotion-error__container">
    <h4 className="promotion-error__title">Oops!</h4>
    <p>{en.utils.inactive_promotion(brandToString(company))}</p>
    <a className="quiver-button" type="button" href={upgradeRedirectUrl}>
      START FREE TRIAL
    </a>
  </div>
);

export default PromotionError;
