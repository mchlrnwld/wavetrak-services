import { useEffect, useState } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { FacebookLogin, CreateAccountForm, LoginForm } from '@surfline/quiver-react';
import { trackNavigatedToPage, trackEvent } from '@surfline/web-common';
import { OrBreak, Disclaimer } from '..';
import { login } from '../../store/actions/login';
import { getFunnelConfig } from '../../store/selectors/promotion';
import {
  getShowLogin,
  isFBFormSubmitting,
  isFormSubmitting,
} from '../../store/selectors/interactions';
import { getFbLoginError, getLoginError, getConfirmEmail } from '../../store/selectors/auth';
import getConfig from '../../config';
import { loginWithFacebook } from '../../store/actions/facebook';
import { register as createUser } from '../../store/actions/user';
import { getCreateUserError } from '../../store/selectors/user';
import en from '../../international/translations/en';

const Registration = () => {
  const config = getConfig();
  const dispatch = useDispatch();
  const showLogin = useSelector(getShowLogin);
  const { _id: promotionId } = useSelector(getFunnelConfig, shallowEqual);
  const [registrationRendered, setRegistrationRendered] = useState(false);
  const fbSubmitting = useSelector(isFBFormSubmitting, shallowEqual);
  const fbError = useSelector(getFbLoginError, shallowEqual);
  const submitting = useSelector(isFormSubmitting, shallowEqual);
  const createUserError = useSelector(getCreateUserError, shallowEqual);
  const loginError = useSelector(getLoginError, shallowEqual);
  const confirmEmail = useSelector(getConfirmEmail);

  const onFBClick = () => {
    dispatch(loginWithFacebook('facebook'));
  };

  const onCreateSubmit = (data) => {
    dispatch(createUser(data));
  };

  const onLoginSubmit = (data) => {
    dispatch(login(data));
  };

  useEffect(() => {
    trackNavigatedToPage('Funnel', { promotionId }, 'Promotion', { Wootric: false });
  }, [promotionId]);

  useEffect(() => {
    if (confirmEmail) window.location.assign('/confirm-email');
  }, [confirmEmail]);

  const viewedRegistration = () => {
    if (!registrationRendered) {
      setRegistrationRendered(true);
      trackEvent('Viewed Checkout Step', { step: 1, promotionId });
    }
  };
  viewedRegistration();
  return (
    <div>
      <FacebookLogin loading={fbSubmitting} error={fbError} onClick={onFBClick} />
      <OrBreak />
      {showLogin ? (
        <LoginForm onSubmit={onLoginSubmit} loading={submitting} error={loginError} />
      ) : (
        <CreateAccountForm
          onSubmit={onCreateSubmit}
          loading={submitting}
          error={createUserError}
          receivePromotionsLabel={en.create_form.receivePromotions_label(config.brand)}
        />
      )}
      {!showLogin ? <Disclaimer location="registration" /> : null}
    </div>
  );
};

export default Registration;
