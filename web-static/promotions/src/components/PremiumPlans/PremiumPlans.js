import { useState } from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import classNames from 'classnames';
import { ChevronAlt } from '../Icons';
import { toggleSelectedPlanId } from '../../store/actions/plans';
import { getPremiumPlans, getSelectedPlanId } from '../../store/selectors/plans';
import { getTrialEligibility } from '../../store/selectors/entitlements';
import { getFunnelConfig } from '../../store/selectors/promotion';
import './PremiumPlans.scss';

const getPlanName = (interval) => (interval.includes('year') ? 'Yearly' : 'Monthly');

const getTrialText = (funnelConfig, trialPeriodDays, trialEligible) => {
  if (!trialPeriodDays) return null;
  if (!trialEligible) return null;
  if (!funnelConfig?.trialLength) return null;
  const trialLength = funnelConfig && trialPeriodDays ? funnelConfig.trialLength : trialPeriodDays;
  return `${trialLength}-Day Free Trial`;
};

const getProjectedCost = (interval, amount) =>
  interval.includes('year') ? parseFloat(amount / 100 / 12).toFixed(2) : amount / 100;

const getActualCost = (interval, amount, symbol) =>
  interval.includes('year')
    ? `/month - Billed ${symbol}${amount / 100} once a year`
    : '/month - Billed month-to-month';

const PremiumPlans = () => {
  const dispatch = useDispatch();
  const plans = useSelector(getPremiumPlans, shallowEqual);
  const selectedPlanId = useSelector(getSelectedPlanId, shallowEqual);
  const trialEligible = useSelector(getTrialEligibility, shallowEqual);
  const funnelConfig = useSelector(getFunnelConfig, shallowEqual);
  const [statePlans, setStatePlans] = useState(
    plans.sort((planA, planB) => {
      if (planA.id < planB.id) return -1;
      if (planA.id > planB.id) return 1;
      return 0;
    }),
  );
  const [open, setOpen] = useState(false);

  const getActiveClassName = (id) => {
    return classNames({
      'premium-plan': true,
      'premium-plan--hidden': !open && selectedPlanId !== id,
    });
  };

  const toggleSelectedPlan = (id) => {
    if (open && id !== selectedPlanId) {
      setStatePlans(statePlans.reverse());
      dispatch(toggleSelectedPlanId(id));
    }
    setOpen(!open);
  };

  return (
    <div className="premium-plans__container">
      <div className="premium-plans">
        {statePlans.map(({ id, amount, trialPeriodDays, symbol, interval }) => (
          <div
            className={getActiveClassName(id)}
            onClick={() => toggleSelectedPlan(id)}
            onKeyPress={() => toggleSelectedPlan(id)}
            key={id}
          >
            <div className="premium-plan__details">
              <div className="premium-plan__name">
                <div className="premium-plan__name--plan">{`${getPlanName(
                  interval,
                )} Subscription`}</div>
                <div className="premium-plan__name--trial">
                  {getTrialText(funnelConfig, trialPeriodDays, trialEligible)}
                </div>
              </div>
              <div className="premium-plan__price">
                <div className="premium-plan__price--cost">
                  {symbol}
                  {getProjectedCost(interval, amount)}
                </div>
                <div className="premium-plan__price--total">
                  {getActualCost(interval, amount, symbol)}
                </div>
              </div>
            </div>
            {!open && plans.length > 1 ? (
              <div className="premium-plan__action">
                <div className="premium-plan__action--text">Select Plan</div>
                <div className="premiium-plan__action--icon">
                  <ChevronAlt />
                </div>
              </div>
            ) : null}
          </div>
        ))}
      </div>
    </div>
  );
};

export default PremiumPlans;
