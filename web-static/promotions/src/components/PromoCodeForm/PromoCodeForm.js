import { useSelector, useDispatch } from 'react-redux';
import { Input } from '@surfline/quiver-react';

import { getPromoCode } from '../../store/selectors/promotion';
import { setPromoCode } from '../../store/actions/promotion';

const PromoCodeForm = () => {
  const dispatch = useDispatch();
  const promoCode = useSelector(getPromoCode);
  const handleChange = (value) => {
    dispatch(setPromoCode(value));
  };
  return (
    <div style={{ marginBottom: '20px' }}>
      <Input
        type="text"
        name="promoCode"
        value={promoCode || null}
        label="PROMO CODE"
        maxLength="15"
        input={{
          name: 'promoCode',
          onChange: (event) => handleChange(event.target.value),
          required: true,
          maxLength: 15,
        }}
      />
    </div>
  );
};

export default PromoCodeForm;
