import React from 'react';
import { brandToString } from '@surfline/web-common';
import getConfig from '../../config';
import './PromotionIneligible.scss';

const { redirectUrl, company } = getConfig();
const PromotionIneligible = () => {
  return (
    <div className="promotion-ineligible__container">
      <h4 className="promotion-ineligible__title">Sorry!</h4>
      <p>Premium subscribers are not eligible for this promotion.</p>
      <a className="quiver-button" type="button" href={redirectUrl}>
        {`RETURN TO ${brandToString(company)}`}
      </a>
    </div>
  );
};

export default PromotionIneligible;
