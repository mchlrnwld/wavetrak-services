import { useSelector, shallowEqual } from 'react-redux';
import { brandToString } from '@surfline/web-common';
import { getFunnelConfig } from '../../store/selectors/promotion';
import { getPremiumPlans } from '../../store/selectors/plans';
import { getAccessToken } from '../../store/selectors/auth';
import getConfig from '../../config';
import './PromotionHeader.scss';

const { brand } = getConfig();
const getTitleText = (funnelConfig, accessToken, plans) => {
  const { paymentHeader, signUpHeader } = funnelConfig;
  if (paymentHeader || signUpHeader) {
    return accessToken && plans ? paymentHeader : signUpHeader;
  }
  return `Start your 15-day free trial of ${brandToString(brand)} Premium`;
};

const PromotionHeader = () => {
  const funnelConfig = useSelector(getFunnelConfig, shallowEqual);
  const accessToken = useSelector(getAccessToken);
  const plans = useSelector(getPremiumPlans, shallowEqual);
  return (
    <div className="promotion-header">
      <h4 className="promotion-header__title">{getTitleText(funnelConfig, accessToken, plans)}</h4>
    </div>
  );
};

export default PromotionHeader;
