/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import { useState, useEffect, useCallback } from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { Alert, Button } from '@surfline/quiver-react';
import { trackEvent } from '@surfline/web-common';
import {
  PaymentRequestSubscription,
  Disclaimer,
  CreditCard,
  PremiumPlans,
  PlanBillingText,
  DuplicateCard,
  PromoCodeForm,
} from '..';
import { startSubmitSpinner } from '../../store/actions/interactions';
import { submitSubscription } from '../../store/actions/subscription';
import { setSelectedCardSource, toggleShowCards } from '../../store/actions/cards';
import { getAccessToken } from '../../store/selectors/auth';
import { getTrialEligibility } from '../../store/selectors/entitlements';
import { getSubscriptionCreateError } from '../../store/selectors/subscription';
import { getSelectedPlanId } from '../../store/selectors/plans';
import {
  getTrialPeriodDays,
  getFunnelConfig,
  getPromoCode,
  getPromotionId,
} from '../../store/selectors/promotion';
import {
  getCreditCards,
  getDefaultSource,
  getSelectedCardId,
  getShowCards,
  getShowDuplicateCard,
  getCardError,
} from '../../store/selectors/cards';
import { isFormSubmitting } from '../../store/selectors/interactions';
import './PaymentForm.scss';

const PaymentForm = () => {
  const dispatch = useDispatch();
  // Selectors
  const accessToken = useSelector(getAccessToken);
  const trialEligible = useSelector(getTrialEligibility);
  const trialPeriodDays = useSelector(getTrialPeriodDays);
  const selectedPlanId = useSelector(getSelectedPlanId);
  const defaultCardId = useSelector(getDefaultSource);
  const submitting = useSelector(isFormSubmitting);
  const showCards = useSelector(getShowCards);
  const showDuplicateCard = useSelector(getShowDuplicateCard);
  const promoCode = useSelector(getPromoCode);
  const subscriptionError = useSelector(getSubscriptionCreateError);
  const cardError = useSelector(getCardError);
  const selectedCardId = useSelector(getSelectedCardId, shallowEqual);
  const promotionId = useSelector(getPromotionId);
  const { promoCodesEnabled } = useSelector(getFunnelConfig, shallowEqual);
  const cards = useSelector(getCreditCards, shallowEqual);

  // Dispatch actions
  const doStartSubmitSpinner = () => dispatch(startSubmitSpinner());
  const doSubmitSubscription = (values) => dispatch(submitSubscription(values));
  const doSetSelectedCardSource = (cardId) => dispatch(setSelectedCardSource(cardId));
  const doToggleShowCards = (showCard) => dispatch(toggleShowCards(showCard));

  // State
  const [stripeSource, setStripeSource] = useState(null);
  const [checkoutRendered, setCheckoutRendered] = useState(false);

  const viewedCheckout = useCallback(() => {
    if (!checkoutRendered) {
      setCheckoutRendered(true);
      trackEvent('Viewed Checkout Step', { step: 2, promotionId });
    }
  }, [checkoutRendered, promotionId]);

  useEffect(() => {
    viewedCheckout();
  }, [viewedCheckout]);

  const stripe = useStripe();
  const elements = useElements();

  const submitPaymentForm = async (values) => {
    // event.preventDefault();
    doStartSubmitSpinner();
    let token;
    if (!stripe || !elements) return;
    const cardElement = elements.getElement(CardElement);
    if (cardElement) {
      const tokenObj = (!showCards && (await stripe.createToken(cardElement))) || null;
      token = tokenObj.token;
      setStripeSource(tokenObj.token);
    }
    const subscriptionValues = {
      plan: selectedPlanId,
      source: token || stripeSource,
      selectedCard: showCards ? selectedCardId : null,
      trialEligible,
      trialPeriodDays,
      location: 'promotions',
      defaultCardId,
      accessToken,
      coupon: null,
      shouldRedirectToOnboarding: false,
      promotionId,
      promoCodesEnabled,
      promoCode,
      ...values,
    };
    await doSubmitSubscription(subscriptionValues);
  };

  if (showDuplicateCard) {
    return <DuplicateCard submitting={submitting} submitPayment={submitPaymentForm} />;
  }
  const error = subscriptionError || cardError;

  return (
    <div>
      <div className="checkout-form__container">
        {error ? (
          <Alert style={{ marginBottom: '0px', textAlign: 'center' }} type="error">
            {error}
          </Alert>
        ) : null}
        <div className="section-label">Choose your plan</div>
        <PremiumPlans />
        <PlanBillingText />
        <div className="credit-card-container">
          {!cards || !showCards ? (
            <div>
              <div className="section-label">Enter your card</div>
              <div className="stripe-card-element__wrapper">
                <CardElement
                  options={{
                    style: {
                      base: {
                        fontSize: '18px',
                        color: '#424770',
                        letterSpacing: '0.025em',
                        fontFamily: 'Source Sans Pro, Helvetica, sans-serif',
                        '::placeholder': {
                          color: '#aab7c4',
                        },
                        padding: '16px 14px',
                      },
                      invalid: {
                        color: '#9e2146',
                      },
                    },
                    disabled: submitting,
                  }}
                />
              </div>
            </div>
          ) : (
            <div className="credit-card-list">
              <div className="section-label">Select a card</div>
              {cards.map((card, index) => (
                <CreditCard
                  card={card}
                  selectedCard={selectedCardId}
                  doSetSelectedCardSource={doSetSelectedCardSource}
                  tabIndex={index}
                  key={card.id}
                />
              ))}
            </div>
          )}
          {cards ? (
            <div>
              <p onClick={() => doToggleShowCards(showCards)} className="credit-card__form__toggle">
                {showCards ? '+ Add New Credit Card' : 'Use An Existing Credit card'}
              </p>
            </div>
          ) : null}
        </div>
        {promoCodesEnabled ? <PromoCodeForm /> : null}
        <div className="checkout-form__container__payment-button">
          <Button
            disabled={submitting}
            style={{ width: '100%' }}
            loading={submitting}
            onClick={submitPaymentForm}
          >
            {!trialEligible || !trialPeriodDays ? 'Go Premium' : 'Start Free Trial'}
          </Button>
        </div>
        <PaymentRequestSubscription paymentHandler={submitPaymentForm} />
        <Disclaimer trialEligible={trialEligible} />
      </div>
    </div>
  );
};

export default PaymentForm;
