import { useSelector, shallowEqual } from 'react-redux';
import PropTypes from 'prop-types';
import { PromotionMobileRailCTA, PromotionRailCTA, RailLogo } from '..';
import { getFunnelConfig } from '../../store/selectors/promotion';
import en from '../../international/translations/en';
import './Layout.scss';

export const LeftRail = ({ children }) => (
  <div className="left-rail">
    <RailLogo />
    {children}
  </div>
);

export const RightRail = ({ children }) => <div className="right-rail">{children}</div>;

const Layout = ({ children }) => {
  const funnelConfig = useSelector(getFunnelConfig, shallowEqual);
  const promotionFeatures = { ...en.promotionFeatures };
  if (funnelConfig) {
    funnelConfig.promotionFeatures = promotionFeatures;
  }
  return funnelConfig ? (
    <div>
      <div className="promotion-wrapper">
        <PromotionMobileRailCTA funnelConfig={funnelConfig} />
        <LeftRail>
          <div className="promotion-wrapper__content">{children}</div>
        </LeftRail>
        <RightRail>
          <PromotionRailCTA funnelConfig={funnelConfig} />
        </RightRail>
      </div>
    </div>
  ) : null;
};
LeftRail.propTypes = {
  children: PropTypes.shape({}).isRequired,
};
RightRail.propTypes = {
  children: PropTypes.shape({}).isRequired,
};
Layout.propTypes = {
  children: PropTypes.shape({}).isRequired,
};

export default Layout;
