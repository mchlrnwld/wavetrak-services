import { useEffect } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { trackNavigatedToPage } from '@surfline/web-common';
import {
  PromotionHeader,
  RegistrationSubtitle,
  PremiumSuccess,
  FunnelHelmet,
  Registration,
  Payment,
  PromotionError,
  PromotionIneligible,
  CurrencyIneligible,
} from '..';
import { fetchEntitlements } from '../../store/actions/entitlements';
import { fetchPromotion } from '../../store/actions/promotion';
import { fetchUser } from '../../store/actions/user';
import { getFunnelConfig, getCurrencyEligibility } from '../../store/selectors/promotion';
import { getAccessToken, isUserInitiatedLogin } from '../../store/selectors/auth';
import { getPremiumStatus } from '../../store/selectors/entitlements';
import { getSubscriptionSuccess } from '../../store/selectors/subscription';

const Promotion = () => {
  const dispatch = useDispatch();
  const accessToken = useSelector(getAccessToken);
  const userInitiated = useSelector(isUserInitiatedLogin);
  const isPremium = useSelector(getPremiumStatus);
  const subscriptionSuccess = useSelector(getSubscriptionSuccess);
  const currencyIsEligible = useSelector(getCurrencyEligibility);
  const { isActive, _id: promotionId } = useSelector(getFunnelConfig, shallowEqual);

  useEffect(() => {
    const reloadUser = async () => {
      await dispatch(fetchUser());
      await dispatch(fetchEntitlements());
      await dispatch(fetchPromotion(promotionId));
    };
    if (accessToken && userInitiated) reloadUser();
  }, [accessToken, dispatch, promotionId, userInitiated]);

  useEffect(() => {
    trackNavigatedToPage('Funnel', { promotionId }, 'Promotion', { Wootric: false });
  }, [promotionId]);

  if (!isActive) {
    return <PromotionError />;
  }

  if (subscriptionSuccess) {
    return <PremiumSuccess />;
  }

  if (!currencyIsEligible) {
    return <CurrencyIneligible />;
  }

  if (isPremium) {
    return <PromotionIneligible />;
  }
  return (
    <div>
      <div>
        <FunnelHelmet />
        <PromotionHeader />
        {accessToken ? null : <RegistrationSubtitle />}
        {accessToken ? <Payment /> : <Registration />}
      </div>
    </div>
  );
};

export default Promotion;
