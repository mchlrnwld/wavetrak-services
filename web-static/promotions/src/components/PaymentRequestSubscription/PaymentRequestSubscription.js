import { useState, useEffect, useCallback } from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import PropTypes from 'prop-types';
import { useStripe, PaymentRequestButtonElement } from '@stripe/react-stripe-js';
import useComponentDidUpdate from '../../utils/hooks/useComponentDidUpdate';
import { getPremiumPlans, getSelectedPlanId } from '../../store/selectors/plans';
import OrBreak from '../OrBreak';

const PaymentRequestSubscription = ({ paymentHandler }) => {
  const stripe = useStripe();
  const selectedPlanId = useSelector(getSelectedPlanId);
  const plans = useSelector(getPremiumPlans, shallowEqual);
  const [digitalWallet, setDigitalWalletState] = useState(false);
  const [paymentRequest, setPaymentRequestState] = useState(null);
  const enableDigitalWallet = true;

  const setupPaymentRequest = useCallback(async () => {
    if (!stripe) {
      return;
    }
    const { amount, currency, interval } = plans.find(({ id }) => id === selectedPlanId);
    const cost = amount / 100;
    const label = interval.includes('year')
      ? `Billed $${cost} once a year`
      : 'Billed month-to-month';
    const paymentRequestObject = {
      country: 'US',
      currency,
      total: {
        label,
        amount,
      },
    };
    const stripeRequest = stripe.paymentRequest(paymentRequestObject);
    const isDigitalWallet = await stripeRequest.canMakePayment();
    setPaymentRequestState(stripeRequest);
    setDigitalWalletState(isDigitalWallet);
  }, [plans, selectedPlanId, stripe]);

  const updatePaymentRequest = async () => {
    const { amount, currency, interval } = plans.find(({ id }) => id === selectedPlanId);
    const cost = amount / 100;
    const label = interval.includes('year')
      ? `Billed $${cost} once a year`
      : 'Billed month-to-month';
    const paymentRequestObject = {
      currency,
      total: {
        label,
        amount,
      },
    };
    await paymentRequest.update(paymentRequestObject);
    setPaymentRequestState(paymentRequest);
    const isDigitalWallet = await paymentRequest.canMakePayment();
    setDigitalWalletState(isDigitalWallet);
  };

  const handlePaymentRequestButtonClick = () => {
    paymentRequest.on('token', async (ev) => {
      const { applePay } = digitalWallet;
      const paymentSource = applePay ? 'Apple Pay' : 'other';
      const paymentValues = {
        plan: selectedPlanId,
        source: ev.token,
        selectedCard: null,
        digitalWallet: paymentSource,
      };
      try {
        await paymentHandler(paymentValues);
        ev.complete('success');
      } catch (err) {
        ev.complete('fail');
      }
    });
  };

  useEffect(() => {
    const paymentRequestAsync = async () => {
      await setupPaymentRequest();
    };
    paymentRequestAsync();
  }, [setupPaymentRequest, stripe]);

  useComponentDidUpdate(updatePaymentRequest, [selectedPlanId]);
  const options = {
    paymentRequest,
    style: {
      paymentRequestButton: {
        height: '48px',
        borderRadius: '5px',
        padding: '0px',
      },
    },
  };
  return enableDigitalWallet && digitalWallet && paymentRequest ? (
    <>
      <OrBreak />
      <div>
        <PaymentRequestButtonElement options={options} onClick={handlePaymentRequestButtonClick} />
      </div>
    </>
  ) : null;
};
PaymentRequestSubscription.propTypes = {
  paymentHandler: PropTypes.func.isRequired,
};
export default PaymentRequestSubscription;
