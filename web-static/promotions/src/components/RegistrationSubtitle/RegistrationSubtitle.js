/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-return-assign */
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { toggleRegistrationForm } from '../../store/actions/interactions';
import { getShowLogin } from '../../store/selectors/interactions';
import './RegistrationSubtitle.scss';

const RegistrationSubtitle = () => {
  const showLogin = useSelector(getShowLogin, shallowEqual);
  const dispatch = useDispatch();
  const doToggleForm = () => {
    dispatch(toggleRegistrationForm(showLogin));
  };
  return showLogin ? (
    <p>
      Need an account?
      <span className="register-link__action" onClick={doToggleForm}>
        {' Sign Up'}
      </span>
    </p>
  ) : (
    <p>
      Already have an account?
      <span className="register-link__action" onClick={doToggleForm}>
        {' Sign In'}
      </span>
    </p>
  );
};

export default RegistrationSubtitle;
