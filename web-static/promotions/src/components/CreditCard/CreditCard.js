import classnames from 'classnames';
import PropTypes from 'prop-types';
import { CreditCardIcon } from '..';

import './CreditCard.scss';

const getActiveCardClassName = (cardId, selectedCard) =>
  classnames({
    'credit-card': true,
    'credit-card--active': cardId === selectedCard,
  });

const formatExpirationDate = (month, year) => {
  const formattedMonth = month.toString().length === 1 ? `0${month.toString()}` : month;
  const parsedYear = year.toString().substring(2);
  return `${formattedMonth}/${parsedYear}`;
};

const CreditCard = ({
  card: { id, exp_month: exMonth, exp_year: exYear, expMonth, expYear, brand, last4 },
  tabIndex,
  selectedCard,
  doSetSelectedCardSource,
}) => {
  const setSelectedCardSource = () => {
    if (doSetSelectedCardSource) doSetSelectedCardSource(id);
  };

  const expiresYear = expYear || exYear;
  const expiresMonth = expMonth || exMonth;
  return (
    <div
      onClick={() => setSelectedCardSource()}
      onKeyPress={() => setSelectedCardSource()}
      className={getActiveCardClassName(id, selectedCard)}
      role="radio"
      tabIndex={tabIndex}
      aria-checked={id === selectedCard}
    >
      <div className="credit-card__element credit-card__element--card-brand">
        <CreditCardIcon ccBrand={brand} />
      </div>
      <div className="credit-card__element credit-card__element--card-number">**** {last4}</div>
      <div className="credit-card__element">{formatExpirationDate(expiresMonth, expiresYear)}</div>
    </div>
  );
};

CreditCard.propTypes = {
  card: PropTypes.shape({
    id: PropTypes.string,
    exp_month: PropTypes.string,
    exp_year: PropTypes.string,
    expMonth: PropTypes.string,
    expYear: PropTypes.string,
    brand: PropTypes.string,
    last4: PropTypes.string,
  }).isRequired,
  doSetSelectedCardSource: PropTypes.func.isRequired,
  tabIndex: PropTypes.number,
  selectedCard: PropTypes.string,
};

CreditCard.defaultProps = {
  tabIndex: 0,
  selectedCard: null,
};

export default CreditCard;
