import { brandToString } from '@surfline/web-common';
import getConfig from '../../config';
import './CurrencyIneligible.scss';

const { redirectUrl, company } = getConfig();
const CurrencyIneligible = () => {
  return (
    <div className="currency-ineligible__container">
      <h4 className="currency-ineligible__title">Sorry!</h4>
      <p>This promotion is not available in your region.</p>
      <a className="quiver-button" type="button" href={redirectUrl}>
        {`RETURN TO ${brandToString(company)}`}
      </a>
    </div>
  );
};

export default CurrencyIneligible;
