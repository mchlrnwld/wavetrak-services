import { useEffect } from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import getConfig from '../../config';
import en from '../../international/translations/en';
import './PremiumSuccess.scss';
import { getFunnelConfig } from '../../store/selectors/promotion';

const { funnelConfig, redirectUrl } = getConfig();

const PremiumSuccess = () => {
  const { successUrl, successMessage, successHeader } = useSelector(getFunnelConfig, shallowEqual);
  useEffect(() => {
    if (successUrl) {
      setTimeout(() => {
        window.location.assign(successUrl);
      }, 2000);
    }
  }, [successUrl]);

  let { message } = en.payment_success;
  const { subTitle } = en.payment_success;
  let header = 'Success';
  if (funnelConfig) {
    message = successMessage || message;
    header = successHeader || header;
  }

  const successRedirectUrl = successUrl || redirectUrl;
  return (
    <div className="premium-success__container">
      <h4>{header}</h4>
      <div className="premium-success__subtitle">{subTitle}</div>
      <div className="premium-success__message">{message}</div>
      <a className="quiver-button" type="button" href={successRedirectUrl}>
        {en.payment_success.linkText}
      </a>
    </div>
  );
};

export default PremiumSuccess;
