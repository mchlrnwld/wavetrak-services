module.exports = {
  cdnHost: 'https://wa.cdn-surfline.com/gifts/fs/',
  staticHost: 'https://wa.cdn-surfline.com/gifts/fs/static',
  redirectUrl: 'http://www.fishtrack.com',
  segment: 'NcHLO1P5K4Dv1XP2hRXvgXAQMgFLJ3Ch',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '639252597',
    accountID: '356245',
    trustKey: '356245',
    agentID: '639252597',
  },
  servicesEndpoint: 'https://services.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'in20v2ebf6q1gh97ldcn1bp8ufdbr404e514',
  clientId: '5c59e80cf0b6cb1ad02baf67',
  clientSecret: 'sk_PvRkUPFXC39AWZwhK6Da',
  termsConditions: 'http://www.fishtrack.com/terms-and-conditions/',
  privacyPolicy: 'http://www.fishtrack.com/privacy-policy/',
  stripe: {
    publishableKey:
      'pk_live_518MSMSJ4F5N75cpXxyXhiIIfbYxuWIna19yhOzriQDepMQfxMqDvwuNNhQXbKbOV5icocdsHvbp0gizP6L0wMCjn00dPOhEeeW',
  },
  facebook: {
    appId: '218714858155230',
  },
  upgradeRedirectUrl: 'http://www.fishtrack.com/upgrade',
  onboardingRedirectUrl: 'http://www.fishtrack.com',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
};
