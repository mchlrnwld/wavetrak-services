module.exports = {
  cdnHost: '/',
  staticHost: '',
  redirectUrl: 'https://sandbox.surfline.com',
  segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '17227951',
    accountID: '356245',
    trustKey: '356245',
    agentID: '17227951',
  },
  servicesEndpoint: 'https://services.sandbox.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
  clientId: '5bff1583ffda1e1eb8d35c4b',
  clientSecret: 'sk_Hmxh7RJEJgJnZeaNF3Kx',
  termsConditions: '/terms_conditions.cfm',
  privacyPolicy: '/privacy.cfm',
  stripe: {
    publishableKey: 'pk_test_Meb7MPzMWgbBYwU3t6gmdHGt',
  },
  facebook: {
    appId: '564041023804928',
  },
  upgradeRedirectUrl: '/upgrade',
  onboardingRedirectUrl: '/onboarding/',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
};
