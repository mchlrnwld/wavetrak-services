module.exports = {
  cdnHost: 'https://product-cdn.sandbox.surfline.com/gifts/bw/',
  staticHost: 'https://product-cdn.sandbox.surfline.com/gifts/bw/static',
  redirectUrl: 'http://sandbox.buoyweather.com',
  segment: 'wGHvUUloHAZXW8STk3qUxvKD0NswRkHu',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '16905555',
    accountID: '356245',
    trustKey: '356245',
    agentID: '16905555',
  },
  servicesEndpoint: 'https://services.sandbox.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
  clientId: '5c59e53d2d11e750809d572f',
  clientSecret: 'sk_dKW7DWJ4XMxxfP5BWDx8',
  termsConditions: 'http://www.buoyweather.com/info.jsp?id=21738',
  privacyPolicy: 'http://www.buoyweather.com/info.jsp?id=21737',
  stripe: {
    publishableKey: 'pk_test_Meb7MPzMWgbBYwU3t6gmdHGt',
  },
  facebook: {
    appId: '564041023804928',
  },
  upgradeRedirectUrl: 'https://sandbox.buoyweather.com/upgrade',
  onboardingRedirectUrl: 'http://sandbox.buoyweather.com',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
};
