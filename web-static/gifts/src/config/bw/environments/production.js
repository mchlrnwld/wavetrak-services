module.exports = {
  cdnHost: 'https://wa.cdn-surfline.com/gifts/bw/',
  staticHost: 'https://wa.cdn-surfline.com/gifts/bw/static',
  redirectUrl: 'http://buoyweather.com',
  segment: 'CvbvzychXPuprLT31PUJjn2797aKS8Ez',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '639252597',
    accountID: '356245',
    trustKey: '356245',
    agentID: '639252597',
  },
  servicesEndpoint: 'https://services.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'in20v2ebf6q1gh97ldcn1bp8ufdbr404e514',
  clientId: '5c59e826f0b6cb1ad02baf68',
  clientSecret: 'sk_4Z4sSmWpPrQsme6nsSuG',
  termsConditions: 'http://www.buoyweather.com/info.jsp?id=21738',
  privacyPolicy: 'http://www.buoyweather.com/info.jsp?id=21737',
  stripe: {
    publishableKey:
      'pk_live_518MSMSJ4F5N75cpXxyXhiIIfbYxuWIna19yhOzriQDepMQfxMqDvwuNNhQXbKbOV5icocdsHvbp0gizP6L0wMCjn00dPOhEeeW',
  },
  facebook: {
    appId: '218714858155230',
  },
  upgradeRedirectUrl: 'https://www.buoyweather.com/upgrade',
  onboardingRedirectUrl: 'http://buoyweather.com',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
};
