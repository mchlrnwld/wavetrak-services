module.exports = {
  cdnHost: 'https://product-cdn.staging.surfline.com/gifts/bw/',
  staticHost: 'https://product-cdn.staging.surfline.com/gifts/bw/static',
  redirectUrl: 'http://staging.buoyweather.com',
  segment: 'wGHvUUloHAZXW8STk3qUxvKD0NswRkHu',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '17259048',
    accountID: '356245',
    trustKey: '356245',
    agentID: '17259048',
  },
  servicesEndpoint: 'https://services.staging.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || '3prmuinjgigdvi7m5haujp8jtq1un82rc64n',
  clientId: '5c59e53d2d11e750809d572f',
  clientSecret: 'sk_dKW7DWJ4XMxxfP5BWDx8',
  termsConditions: 'http://www.buoyweather.com/info.jsp?id=21738',
  privacyPolicy: 'http://www.buoyweather.com/info.jsp?id=21737',
  stripe: {
    publishableKey: 'pk_test_xBhwMLUOtBN1ORmA47xmqNGg',
  },
  facebook: {
    appId: '564041023804928',
  },
  upgradeRedirectUrl: 'https://staging.buoyweather.com/upgrade',
  onboardingRedirectUrl: 'http://staging.buoyweather.com',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
};
