/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */

const APP_ENV = process.env.APP_ENV || 'development';
const brand = process.env.BRAND || 'sl';
const config = {
  bw: require(`./bw/environments/${APP_ENV}`),
  sl: require(`./sl/environments/${APP_ENV}`),
  fs: require(`./fs/environments/${APP_ENV}`),
};

module.exports = () => ({
  APP_ENV,
  brand,
  company: brand,
  giftRedeemUrl: '/redeem',
  ...config[brand],
});
