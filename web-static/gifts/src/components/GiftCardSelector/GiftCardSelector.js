import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { selectGiftProduct } from '../../store/actions/gifts';
import { getGiftProducts, getSelectedGiftProduct } from '../../store/selectors/gifts';
import './GiftCardSelector.scss';

const GiftCardSelectorItem = ({ index, item, selectedProductId, doSelectGiftProduct }) => {
  const cardClass = classNames(
    'gift-card-selector-bar__item',
    `gift-card-selector-bar__item-${index}`,
    { [`gift-card-selector-bar__item-${index}--selected`]: selectedProductId === item._id },
  );
  const bestValueClass = classNames('gift-card-selector-bar__bestvalue', {
    'gift-card-selector-bar__bestvalue_0': selectedProductId === item._id && index === 0,
  });
  return (
    <div
      className={cardClass}
      onClick={() => {
        doSelectGiftProduct(item._id);
      }}
      onKeyPress={() => {
        doSelectGiftProduct(item._id);
      }}
    >
      {item.bestValue ? <div className={bestValueClass}>BEST VALUE</div> : null}
      <div className="gift-card-selector-bar__item-content">{item.name}</div>
    </div>
  );
};

const GiftCardSelector = () => {
  const dispatch = useDispatch();
  const giftProducts = useSelector(getGiftProducts);
  const { _id: selectedProductId } = useSelector(getSelectedGiftProduct);
  const doSelectGiftProduct = (data) => {
    dispatch(selectGiftProduct(data));
  };
  return (
    <div className="gift-card-selector__container">
      <span>SELECT DURATION</span>
      <div className="gift-card-selector-bar">
        {giftProducts.map((item, index) => (
          <GiftCardSelectorItem
            index={index}
            key={item._id}
            item={item}
            doSelectGiftProduct={doSelectGiftProduct}
            selectedProductId={selectedProductId}
          />
        ))}
      </div>
    </div>
  );
};
GiftCardSelectorItem.propTypes = {
  index: PropTypes.string.isRequired,
  item: PropTypes.string.isRequired,
  selectedProductId: PropTypes.string.isRequired,
  doSelectGiftProduct: PropTypes.func.isRequired,
};

export default GiftCardSelector;
