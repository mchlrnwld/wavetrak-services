/* eslint-disable no-nested-ternary */
import { useSelector } from 'react-redux';
import Head from 'next/head';
import { canUseDOM } from '@surfline/web-common';
import en from '../../international/translations/en';
import { getShowLogin } from '../../store/selectors/interactions';
import { getAccessToken } from '../../store/selectors/auth';

const GiftsHelmet = () => {
  const showLogin = useSelector(getShowLogin);
  const accessToken = useSelector(getAccessToken);
  return (
    <Head>
      {canUseDOM && window.location.href.includes('gift-cards') ? (
        <title>Purchase Your Gift Card</title>
      ) : accessToken ? (
        <title>Redeem Your Gift</title>
      ) : (
        <title>{showLogin ? en.sign_in_funnel.meta_title : en.create_form.meta_title}</title>
      )}

      {showLogin ? (
        <meta name="description" content={en.sign_in_funnel.meta_description} />
      ) : (
        <>
          <meta name="description" content={en.create_form.meta_description} />
        </>
      )}
      <meta property="fb:app_id" content="218714858155230" />
      <meta property="fb:page_id" content="255110695512" />
      <meta property="og:site_name" content="Surfline" />
      <link rel="manifest" href="/manifest.json" />
      <link rel="publisher" href="https://plus.google.com/+Surfline" />
      <link
        rel="apple-touch-icon"
        href="https://wa.cdn-surfline.com/quiver/0.4.0/apple/surfline-apple-touch-icon.png"
      />
    </Head>
  );
};

export default GiftsHelmet;
