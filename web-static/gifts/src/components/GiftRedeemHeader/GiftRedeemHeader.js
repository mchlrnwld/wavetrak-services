import { useSelector, shallowEqual } from 'react-redux';
import PropTypes from 'prop-types';
import { getShowLogin } from '../../store/selectors/interactions';
import { getGiftRedeemPromotion } from '../../store/selectors/gifts';
import { getRedeemCode, getValidationErrorCode } from '../../store/selectors/redeem';
import en from '../../international/translations/en';

const GiftRedeemHeader = ({ hideSubtitle }) => {
  const showLogin = useSelector(getShowLogin);
  const funnelConfig = useSelector(getGiftRedeemPromotion);
  const validationErrorCode = useSelector(getValidationErrorCode, shallowEqual);
  const code = useSelector(getRedeemCode);
  const getTitleText = () => {
    const baseText = validationErrorCode ? en.giftCards.error.headerText : 'Redeem Your Gift';
    if (funnelConfig) {
      const { redeemHeader } = funnelConfig;
      if (!hideSubtitle) return redeemHeader || baseText;
    }
    if (!hideSubtitle) return baseText;
    const verbText = showLogin ? 'Sign in to' : 'Create an Account to';
    return `${verbText} ${baseText} `;
  };
  if (!code) {
    return (
      <div className="gift-redeem-header">
        <h4 className="gift-redeem-header__title">{getTitleText()}</h4>
      </div>
    );
  }

  return null;
};

GiftRedeemHeader.propTypes = {
  hideSubtitle: PropTypes.bool,
};

GiftRedeemHeader.defaultProps = {
  hideSubtitle: false,
};

export default GiftRedeemHeader;
