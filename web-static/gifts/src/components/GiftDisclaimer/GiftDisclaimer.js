import PropTypes from 'prop-types';

import getConfig from '../../config';
import './GiftDisclaimer.scss';

const { termsConditions } = getConfig();
const GiftDisclaimer = () => (
  <p className="disclaimer">
    {'Gift Cards are subject to '}
    <DisclaimerLink url={termsConditions} text="Terms of Use" />.
    {'  Gift Cards are not returnable after purchase (except as required by law).'}
    {'  Have a gift card? '}
    <DisclaimerLink url="/redeem" text="Redeem" />
    {' your gift card.'}
  </p>
);

const DisclaimerLink = ({ text, url }) => (
  <a className="disclaimer-link" href={url} target="_blank" rel="noopener noreferrer">
    {text}
  </a>
);

DisclaimerLink.propTypes = {
  text: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default GiftDisclaimer;
