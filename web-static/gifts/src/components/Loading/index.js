import './Loading.scss';
import { Spinner } from '@surfline/quiver-react';

export default () => {
  return (
    <div className="loading">
      <Spinner />
    </div>
  );
};
