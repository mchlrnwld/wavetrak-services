import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { brandToString } from '@surfline/web-common';
import { Button } from '@surfline/quiver-react';
import getConfig from '../../config';
import { getValidationErrorCode } from '../../store/selectors/redeem';
import { reset } from '../../store/actions/redeem';
import './RedeemError.scss';

const config = getConfig();

const RedeemError = () => {
  const dispatch = useDispatch();
  const validationErrorCode = useSelector(getValidationErrorCode, shallowEqual);
  const product = brandToString(config.brand).toLowerCase();

  const SupportTag = () => (
    <a
      className="support-tag"
      href={`https://support.${product}.com`}
    >{`support@${product}.com`}</a>
  );

  const getErrorMessage = () => {
    if (validationErrorCode === 1003) {
      return `There is an outstanding issue with this gift card.
        We are unable to apply this gift card to your account.`;
    }
    if (validationErrorCode === 1002) {
      return `Your current subscription is managed by a 3rd party.
        We are unable to apply this gift card to your account.`;
    }
    if (validationErrorCode === 1003) {
      return `Your current subscription billing has been disputed.
      We are unable to apply this gift card to your account.`;
    }
    if (validationErrorCode === 1004) {
      return (
        <span>
          We apologize for the inconvenience. Please contact <SupportTag />
          {` for more information.`}
        </span>
      );
    }
    return (
      <span>
        {`There was a problem applying this gift card to your account. Please try again or contact`}
        {` `}
        <SupportTag />.
      </span>
    );
  };
  const doResetRedeem = () => {
    dispatch(reset());
  };
  const navigateToHelp = () => window.location.assign(config.giftRedeemHelpUrl);
  return (
    <div className="redeem-error">
      <p>{getErrorMessage()}</p>
      <Button onClick={navigateToHelp}>Learn More</Button>
      <Button onClick={() => doResetRedeem()} type="info">
        Cancel
      </Button>
    </div>
  );
};

export default RedeemError;
