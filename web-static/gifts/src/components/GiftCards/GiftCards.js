import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { trackEvent, trackNavigatedToPage, brandToString } from '@surfline/web-common';
import { Button, Alert } from '@surfline/quiver-react';
import { getShowPaymentForm, getPurchaseSuccess } from '../../store/selectors/gifts';
import { getCountryCode } from '../../store/selectors/user';
import {
  GiftCardSelector,
  GiftCard,
  GiftCardMessageForm,
  GiftDisclaimer,
  GiftMessage,
  GiftPaymentForm,
  GiftsHelmet,
} from '..';
import getConfig from '../../config';

const config = getConfig();
const stripePromise = loadStripe(config.stripe.publishableKey);
const viewedGiftPurchase = (step) => {
  trackEvent('Viewed Gift Purchase Step', step);
};

const renderSuccessButton = () => {
  viewedGiftPurchase({ step: 3 });
  return (
    <div className="gift-purchase-success__button">
      <Button
        onClick={() =>
          window.location.assign(config[`${brandToString(config.brand).toLowerCase()}Home`])
        }
      >
        {`RETURN TO ${brandToString(config.brand)}`}
      </Button>
    </div>
  );
};
const GiftCards = () => {
  const countryCode = useSelector(getCountryCode);
  const showPaymentForm = useSelector(getShowPaymentForm);
  const purchaseSuccess = useSelector(getPurchaseSuccess);
  useEffect(() => {
    trackNavigatedToPage('Funnel', {}, 'Gift Purchase', { Wootric: false });
    trackEvent('Viewed Gift Purchase Step', { step: 1 });
  }, []);

  if (countryCode.toLowerCase() !== 'us') {
    return (
      <Alert type="warning">
        Sorry! At the moment, gift cards are only available for US customers.
      </Alert>
    );
  }

  return (
    <div>
      <GiftsHelmet />
      {showPaymentForm || purchaseSuccess ? (
        <div>
          <div className="saved-gift-card-container">
            <div className="gift-card-wrapper">
              <GiftCard />
            </div>
            <GiftMessage />
          </div>
          {showPaymentForm ? (
            <Elements stripe={stripePromise}>
              <GiftPaymentForm viewedGiftPurchase={viewedGiftPurchase} />
            </Elements>
          ) : (
            renderSuccessButton()
          )}
        </div>
      ) : (
        <div>
          <GiftCardSelector />
          <GiftCard />
          <GiftCardMessageForm />
        </div>
      )}
      <GiftDisclaimer />
    </div>
  );
};
export default GiftCards;
