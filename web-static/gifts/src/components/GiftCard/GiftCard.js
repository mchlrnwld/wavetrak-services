import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { getSelectedGiftProduct } from '../../store/selectors/gifts';
import './GiftCard.scss';

const amountToFixed = (amount) => parseFloat(amount / 100).toFixed(2);

const GiftCard = () => {
  const giftProduct = useSelector(getSelectedGiftProduct);
  return (
    <div className="gift-card-container">
      <div
        className="gift-card-image"
        style={giftProduct.imagePath ? { backgroundImage: `url(${giftProduct.imagePath})` } : null}
      />
      <div className="gift-card-details">
        <span>${amountToFixed(giftProduct.price)}</span>
        <p>{giftProduct.description}</p>
      </div>
    </div>
  );
};

GiftCard.propTypes = {
  giftProduct: PropTypes.shape({
    imagePath: PropTypes.string,
    price: PropTypes.string,
    description: PropTypes.string,
  }).isRequired,
};

export default GiftCard;
