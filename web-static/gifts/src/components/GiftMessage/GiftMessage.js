import { useSelector, useDispatch } from 'react-redux';
import { PencilIcon } from '@surfline/quiver-react';
import { getSavedGiftData, getPurchaseSuccess } from '../../store/selectors/gifts';
import { editGift } from '../../store/actions/gifts';
import './GiftMessage.scss';

const GiftMessage = () => {
  const dispatch = useDispatch();
  const savedGiftData = useSelector(getSavedGiftData);
  const purchaseSuccess = useSelector(getPurchaseSuccess);
  const doEditGift = () => dispatch(editGift());
  const { recipientName, recipientEmail, purchaserName, purchaserEmail, message } =
    savedGiftData || {};
  return (
    <div className="gift-message-container">
      <div className="gift-message-user__details">
        <div className="gift-message-email">
          <span>To:</span>
          <p>{recipientName}</p>
          <p>{recipientEmail}</p>
        </div>
        <div className="gift-message-email">
          <span>From:</span>
          <p>{purchaserName}</p>
          <p>{purchaserEmail}</p>
        </div>
      </div>
      {message && message.length ? (
        <div className="gift-message">
          <span>Message:</span>
          <div>{message}</div>
        </div>
      ) : null}
      {purchaseSuccess ? null : (
        <div className="gift-message-edit" onClick={doEditGift} onKeyPress={doEditGift}>
          <PencilIcon />
          <span>EDIT</span>
        </div>
      )}
    </div>
  );
};

export default GiftMessage;
