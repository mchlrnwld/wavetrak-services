import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { Button } from '@surfline/quiver-react';
import { brandToString } from '@surfline/web-common';
import getConfig from '../../config';
import {
  getSubscription,
  getRedeemSuccess,
  getGiftProduct,
  getRedeemCode,
} from '../../store/selectors/redeem';
import { isFormSubmitting } from '../../store/selectors/interactions';
import { confirmRedemption } from '../../store/actions/redeem';
import { getGiftRedeemPromotion } from '../../store/selectors/gifts';

const config = getConfig();

const RedeemConfirmation = () => {
  const dispatch = useDispatch();
  const giftProduct = useSelector(getGiftProduct, shallowEqual);
  const subscription = useSelector(getSubscription, shallowEqual);
  const redeemSuccess = useSelector(getRedeemSuccess, shallowEqual);
  const submitting = useSelector(isFormSubmitting, shallowEqual);
  const code = useSelector(getRedeemCode, shallowEqual);
  const funnelConfig = useSelector(getGiftRedeemPromotion);
  const getHeader = () => {
    const { price, description } = giftProduct;
    const baseText = 'Success!';
    if (funnelConfig) {
      const { successHeader } = funnelConfig;
      if (redeemSuccess) return successHeader || baseText;
    }
    if (redeemSuccess) return baseText;
    if (subscription) {
      return `Apply $${price / 100} Gift Credit?`;
    }
    return `Redeem ${description}?`;
  };

  const getMessage = () => {
    const { name, price } = giftProduct;
    if (redeemSuccess && subscription) {
      return `Your $${price / 100} credit has been applied.`;
    }
    if (redeemSuccess && !subscription) {
      return `Your ${name} gift card has been applied.`;
    }
    if (subscription) {
      return `The balance will be credited to your subscription
        on the next billing period.`;
    }
    return `A ${name} gift card will immediately be applied to your account.`;
  };

  const submitConfirmation = async () => {
    await dispatch(confirmRedemption(code));
  };

  const returnToHompage = () => {
    window.location.assign(config.redirectUrl);
  };
  const goToOnboarding = () => {
    window.location.assign(config.onboardingRedirectUrl);
  };

  const { brand } = config;
  return (
    <div className="redeem-confirmation">
      <h4>{getHeader()}</h4>
      <p>{getMessage()}</p>
      {redeemSuccess ? (
        <Button onClick={goToOnboarding}>{`Continue to ${brandToString(brand)}`}</Button>
      ) : (
        <div>
          <Button loading={submitting} onClick={submitConfirmation}>
            Redeem
          </Button>
          <Button onClick={returnToHompage} type="info">
            Cancel
          </Button>
        </div>
      )}
    </div>
  );
};

export default RedeemConfirmation;
