import { useState } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { Button, Input } from '@surfline/quiver-react';
import getConfig from '../../config';
import { isFormSubmitting } from '../../store/selectors/interactions';
import { validateCode } from '../../store/actions/redeem';

const { giftRedeemHelpUrl } = getConfig();
const RedeemForm = () => {
  const dispatch = useDispatch();
  const [code, setCode] = useState('');
  const submitting = useSelector(isFormSubmitting, shallowEqual);
  const handleChange = (event) => {
    event.preventDefault();
    setCode(event.target.value);
  };
  return (
    <div>
      <form
        className="redeem-form"
        onSubmit={(event) => {
          event.preventDefault();
          dispatch(validateCode(code));
        }}
      >
        <Input
          required
          label="CODE"
          id="code"
          type="text"
          input={{ name: 'code', onChange: handleChange, maxLength: '100' }}
        />
        <Button loading={submitting}>Redeem</Button>
      </form>
      <a href={giftRedeemHelpUrl} className="gift-card-learn-more">
        Need help redeeming your gift?
      </a>
    </div>
  );
};

export default RedeemForm;
