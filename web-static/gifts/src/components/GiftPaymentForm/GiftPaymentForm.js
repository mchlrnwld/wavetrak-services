import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { Button, Alert } from '@surfline/quiver-react';
import { PaymentRequestGift } from '..';
import { purchaseGift } from '../../store/actions/gifts';
import { getSavedGiftData, getGiftCardsError } from '../../store/selectors/gifts';
import { isFormSubmitting } from '../../store/selectors/interactions';
import './GiftPaymentForm.scss';

const GiftPaymentForm = ({ viewedGiftPurchase }) => {
  const dispatch = useDispatch();
  const stripe = useStripe();
  const elements = useElements();
  const savedGiftData = useSelector(getSavedGiftData);
  const submitting = useSelector(isFormSubmitting);
  const error = useSelector(getGiftCardsError);
  const submitPayment = async (walletToken, paymentSource = 'card') => {
    let token = walletToken;
    if (!stripe || !elements) return;
    const cardElement = elements.getElement(CardElement);
    if (paymentSource === 'card' && cardElement) {
      const { token: cardToken = null } = await stripe.createToken(cardElement);
      token = cardToken;
    }
    const { id: source } = token || {};
    const giftCard = {
      ...savedGiftData,
      source,
    };
    dispatch(purchaseGift(giftCard));
  };

  useEffect(() => {
    viewedGiftPurchase({ step: 2 });
  }, [viewedGiftPurchase]);
  return (
    <div className="gift-payment-form-container">
      <span>PAYMENT DETAILS</span>
      {error ? (
        <Alert type="error" style={{ marginBottom: '5px', textAlign: 'center' }}>
          {error}
        </Alert>
      ) : null}
      <div className="stripe-card-element__wrapper">
        <CardElement
          options={{
            style: {
              base: {
                fontSize: '18px',
                color: '#424770',
                letterSpacing: '0.025em',
                fontFamily: 'Source Sans Pro, Helvetica, sans-serif',
                '::placeholder': {
                  color: '#aab7c4',
                },
                padding: '16px 14px',
              },
              invalid: {
                color: '#9e2146',
              },
            },
            disabled: submitting,
          }}
        />
      </div>
      <Button loading={submitting} onClick={submitPayment}>
        PURCHASE GIFT CARD
      </Button>
      <PaymentRequestGift paymentHandler={submitPayment} />
    </div>
  );
};

GiftPaymentForm.propTypes = {
  viewedGiftPurchase: PropTypes.func.isRequired,
};

export default GiftPaymentForm;
