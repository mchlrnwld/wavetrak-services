import { useSelector, useDispatch } from 'react-redux';
import { set } from 'lodash';
import { Input, Button, TextArea } from '@surfline/quiver-react';
import getConfig from '../../config';
import { getSelectedGiftProduct, getSavedGiftData } from '../../store/selectors/gifts';
import { saveGift } from '../../store/actions/gifts';
import './GiftCardMessageForm.scss';

const { giftPurchaseHelpUrl } = getConfig();
const GiftCardMessageForm = () => {
  const dispatch = useDispatch();
  const { _id: selectedProductId } = useSelector(getSelectedGiftProduct);
  const savedGiftData = useSelector(getSavedGiftData);
  const { recipientName, recipientEmail, purchaserName, purchaserEmail, message } =
    savedGiftData || {};
  return (
    <div>
      <form
        onSubmit={(event) => {
          event.preventDefault();
          dispatch(
            saveGift(
              [...new FormData(event.target)].reduce((formFields, [field, value]) => {
                set(formFields, field, value);
                return formFields;
              }, {}),
            ),
          );
        }}
        className="gift-card-message-form"
      >
        <input type="hidden" name="giftProductId" value={selectedProductId} />
        <div className="gift-card-message-form__label">TO:</div>
        <div className="gift-card-recipient">
          <Input
            defaultValue={recipientName}
            label="RECIPIENT NAME"
            id="recipientName"
            type="text"
            input={{ name: 'recipientName' }}
          />
          <Input
            defaultValue={recipientEmail}
            label="RECIPIENT EMAIL"
            id="recipientEmail"
            type="email"
            input={{ name: 'recipientEmail' }}
          />
        </div>
        <div className="gift-card-message-form__label">FROM:</div>
        <div className="gift-card-sender">
          <Input
            defaultValue={purchaserName}
            label="SENDER NAME"
            id="purchaserName"
            type="text"
            input={{ name: 'purchaserName', required: true }}
          />
          <Input
            defaultValue={purchaserEmail}
            label="SENDER EMAIL"
            id="purchaserEmail"
            type="email"
            input={{ name: 'purchaserEmail', required: true }}
          />
        </div>
        <div className="gift-card-message-form__label">MESSAGE:</div>
        <div className="gift-card-message">
          <TextArea
            defaultValue={message}
            label="OPTIONAL(UP TO 140 CHARACTERS)"
            textarea={{ name: 'message', maxLength: 140 }}
          />
        </div>
        <Button label="Continue">CONTINUE</Button>
        <a href={giftPurchaseHelpUrl} className="gift-card-learn-more">
          Learn more about gift cards.
        </a>
      </form>
    </div>
  );
};

export default GiftCardMessageForm;
