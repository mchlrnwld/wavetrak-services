import PropTypes from 'prop-types';
import getConfig from '../../config';
import './GiftHeader.scss';

const { giftRedeemUrl } = getConfig();

const GiftHeader = ({ copy }) => {
  const { headerText, subHeaderText } = copy;
  return (
    <div className="gift-purchase__header">
      <h4 className="gift-purchase-header__title">{headerText}</h4>
      <p>{subHeaderText}</p>
      <p>
        <a href={giftRedeemUrl}> Have a gift card? Redeem here</a>
      </p>
    </div>
  );
};

GiftHeader.propTypes = {
  copy: PropTypes.shape({
    headerText: PropTypes.string,
    subHeaderText: PropTypes.string,
  }),
};

GiftHeader.defaultProps = {
  copy: null,
};

export default GiftHeader;
