import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { canUseDOM } from '@surfline/web-common';
import { MobileGiftRailCTA, GiftRailCTA, RailLogo, GiftHeader, GiftRedeemHeader } from '..';
import {
  getSavedGiftData,
  getPurchaseSuccess,
  getGiftRedeemPromotion,
} from '../../store/selectors/gifts';
import en from '../../international/translations/en';
import './Layout.scss';

export const LeftRail = ({ children }) => (
  <div className="left-rail">
    <RailLogo />
    {children}
  </div>
);

export const RightRail = ({ children }) => <div className="right-rail">{children}</div>;

const Layout = ({ children }) => {
  const savedGiftData = useSelector(getSavedGiftData);
  const purchaseSuccess = useSelector(getPurchaseSuccess);
  const funnelConfig = useSelector(getGiftRedeemPromotion);
  let ctaCopy = {
    ...en.giftCards,
  };
  if (funnelConfig) {
    ctaCopy = {
      backgroundImage: `url(${funnelConfig.bgDesktopImgPath})`,
      layeredCardImage: `url(${funnelConfig.layeredCardImgPath})`,
      rightRailHeader: funnelConfig.detailCopy,
    };
  }
  const renderHeader = () => {
    const copy = purchaseSuccess
      ? {
          headerText: en.giftCards.success.headerText,
          subHeaderText: en.giftCards.success.subHeaderText(savedGiftData.purchaserEmail),
        }
      : { ...en.giftCards.select };
    return canUseDOM && window.location.href.includes('redeem') ? (
      <GiftRedeemHeader />
    ) : (
      <GiftHeader copy={copy} />
    );
  };
  return (
    <div>
      <div className="gift-purchase">
        <MobileGiftRailCTA ctaCopy={ctaCopy} funnelConfig={!!funnelConfig} />
        <LeftRail>
          <div className="gift-purchase__content">
            {renderHeader()}
            {children}
          </div>
        </LeftRail>
        <RightRail>
          <GiftRailCTA ctaCopy={ctaCopy} />
        </RightRail>
      </div>
    </div>
  );
};

LeftRail.propTypes = {
  children: PropTypes.node.isRequired,
};
RightRail.propTypes = {
  children: PropTypes.node.isRequired,
};
Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
