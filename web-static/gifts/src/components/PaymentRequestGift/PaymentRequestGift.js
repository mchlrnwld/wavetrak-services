import { useState, useEffect, useCallback } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { useStripe, PaymentRequestButtonElement } from '@stripe/react-stripe-js';
import { getSelectedGiftProduct } from '../../store/selectors/gifts';

const PaymentRequestGift = ({ paymentHandler }) => {
  const stripe = useStripe();
  const [digitalWallet, setDigitalWalletState] = useState(false);
  const [paymentRequest, setPaymentRequestState] = useState(null);
  const enableDigitalWallet = true;
  const selectedProduct = useSelector(getSelectedGiftProduct);
  const setupPaymentRequest = useCallback(async () => {
    if (!stripe) {
      return;
    }
    const { description: label, price: amount } = selectedProduct;
    const paymentRequestObject = {
      country: 'US',
      currency: 'usd',
      total: {
        label,
        amount,
      },
    };
    const stripeRequest = stripe.paymentRequest(paymentRequestObject);
    const isDigitalWallet = await stripeRequest.canMakePayment();
    setPaymentRequestState(stripeRequest);
    setDigitalWalletState(isDigitalWallet);
  }, [selectedProduct, stripe]);

  const handlePaymentRequestButtonClick = () => {
    paymentRequest.on('token', async (ev) => {
      try {
        await paymentHandler(ev.token, 'wallet');
        ev.complete('success');
      } catch (err) {
        ev.complete('fail');
      }
    });
  };

  useEffect(() => {
    const paymentRequestAsync = async () => {
      await setupPaymentRequest();
    };
    paymentRequestAsync();
  }, [setupPaymentRequest, stripe]);
  const options = {
    paymentRequest,
    className: 'checkout-form__container__digital_wallet__button',
    style: {
      paymentRequestButton: {
        height: '48px',
        borderRadius: '5px',
        padding: '0px',
      },
    },
  };
  return enableDigitalWallet && digitalWallet && paymentRequest ? (
    <div>
      <div className="checkout-form__container__digital_wallet__section" />
      <PaymentRequestButtonElement options={options} onClick={handlePaymentRequestButtonClick} />
    </div>
  ) : null;
};

PaymentRequestGift.propTypes = {
  paymentHandler: PropTypes.func.isRequired,
};

export default PaymentRequestGift;
