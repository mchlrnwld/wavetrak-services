import PropTypes from 'prop-types';
import classNames from 'classnames';
/**
 * @description The MobileGiftRailCTA only display on mobile view. This component renders a heading
 * for the mobile page that is placed above the form.
 */
const MobileGiftRailCTA = ({ ctaCopy, funnelConfig }) => {
  const { backgroundImage, layeredCardImage } = ctaCopy;
  const getMobileRailCtaClass = () =>
    classNames({
      'mobile-rail-cta': true,
      'mobile-rail-cta--custom': funnelConfig,
    });
  const getRailCtaConentClass = () =>
    classNames({
      'gift-mobile-rail-cta__content': true,
      'gift-mobile-rail-cta__content--custom': funnelConfig,
    });
  return (
    <div className={getMobileRailCtaClass()} style={{ backgroundImage }}>
      <div className={getRailCtaConentClass()} style={{ backgroundImage: layeredCardImage }} />
    </div>
  );
};

MobileGiftRailCTA.propTypes = {
  ctaCopy: PropTypes.shape({
    layeredCardImage: PropTypes.string,
    backgroundImage: PropTypes.string,
  }),
  funnelConfig: PropTypes.shape({}),
};

MobileGiftRailCTA.defaultProps = {
  ctaCopy: null,
  funnelConfig: null,
};

export default MobileGiftRailCTA;
