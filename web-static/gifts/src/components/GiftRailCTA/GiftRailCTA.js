import PropTypes from 'prop-types';
import './GiftRailCTA.scss';
/**
 * @description The `GiftRailCTA` component is used on the right rail. It renders a
 * background image which covers the block and on top of it renders a heading with sub-text and
 * a in image that shows the layered cards
 *
 * @param {object} ctaCopy - The ctaCopy for the Gift Product
 */

const GiftRailCTA = ({ ctaCopy }) => {
  const { backgroundImage, layeredCardImage, rightRailHeader } = ctaCopy;
  return (
    <div className="gift-rail-cta" style={{ backgroundImage }}>
      <div className="gift-rail-cta__container">
        <span>{rightRailHeader}</span>
        <div className="gift-rail-cta__cards" style={{ backgroundImage: layeredCardImage }} />
      </div>
    </div>
  );
};

GiftRailCTA.propTypes = {
  ctaCopy: PropTypes.shape({
    layeredCardImage: PropTypes.string,
    backgroundImage: PropTypes.string,
    rightRailHeader: PropTypes.string,
  }),
};

GiftRailCTA.defaultProps = {
  ctaCopy: null,
};

export default GiftRailCTA;
