import { useEffect, useState } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { trackNavigatedToPage, trackEvent } from '@surfline/web-common';
import { FacebookLogin, CreateAccountForm, LoginForm, Alert } from '@surfline/quiver-react';
import {
  OrBreak,
  RegistrationSubtitle,
  RedeemForm,
  RedeemError,
  RedeemConfirmation,
  GiftsHelmet,
} from '..';
import { loginWithFacebook } from '../../store/actions/facebook';
import { register as createUser } from '../../store/actions/user';
import { login } from '../../store/actions/login';
import { getAccessToken, getFbLoginError, getLoginError } from '../../store/selectors/auth';
import {
  getShowLogin,
  isFBFormSubmitting,
  isFormSubmitting,
} from '../../store/selectors/interactions';
import { getCreateUserError } from '../../store/selectors/user';
import {
  getValidationError,
  getValidationErrorCode,
  getGiftProduct,
} from '../../store/selectors/redeem';
import getConfig from '../../config';
import en from '../../international/translations/en';
import './Redeem.scss';

const config = getConfig();
const GiftCards = () => {
  const dispatch = useDispatch();
  const [registrationRendered, setRegistrationRendered] = useState(false);
  const [redeemRendered, setRedeemRendered] = useState(false);
  const fbSubmitting = useSelector(isFBFormSubmitting, shallowEqual);
  const fbError = useSelector(getFbLoginError, shallowEqual);
  const accessToken = useSelector(getAccessToken);
  const showLogin = useSelector(getShowLogin);
  const submitting = useSelector(isFormSubmitting, shallowEqual);
  const loginError = useSelector(getLoginError, shallowEqual);
  const createUserError = useSelector(getCreateUserError, shallowEqual);
  const validationError = useSelector(getValidationError, shallowEqual);
  const validationErrorCode = useSelector(getValidationErrorCode, shallowEqual);
  const giftProduct = useSelector(getGiftProduct, shallowEqual);

  const onFBClick = () => {
    dispatch(loginWithFacebook('facebook'));
  };
  const onLoginSubmit = (data) => {
    dispatch(login(data));
  };
  const onCreateSubmit = (data) => {
    dispatch(createUser(data));
  };
  useEffect(() => {
    trackNavigatedToPage('Funnel', {}, 'Redeem', { Wootric: false });
  }, []);
  const viewedRedeem = () => {
    if (!redeemRendered) {
      setRedeemRendered(true);
      trackEvent('Viewed Redeem Step', { step: 2 });
    }
  };
  const viewedRegistration = () => {
    if (!registrationRendered) {
      setRegistrationRendered(true);
      trackEvent('Viewed Redeem Step', { step: 1 });
    }
  };
  const renderRegistrationForm = () => {
    viewedRegistration();
    return (
      <div>
        <FacebookLogin loading={fbSubmitting} error={fbError} onClick={onFBClick} />
        <OrBreak />
        {showLogin ? (
          <LoginForm onSubmit={onLoginSubmit} loading={submitting} error={loginError} />
        ) : (
          <CreateAccountForm
            onSubmit={onCreateSubmit}
            loading={submitting}
            error={createUserError}
            receivePromotionsLabel={en.create_form.receivePromotions_label(config.brand)}
          />
        )}
      </div>
    );
  };
  const renderRedeemForm = () => {
    viewedRedeem();
    return <RedeemForm />;
  };
  const renderRedeemConfirmation = () => {
    if (validationErrorCode) {
      return <RedeemError />;
    }
    return <RedeemConfirmation />;
  };
  return (
    <div className="redeem-funnel__content">
      <GiftsHelmet />
      {accessToken ? null : <RegistrationSubtitle />}
      {validationError ? <Alert type="error">{validationError}</Alert> : null}
      {giftProduct || validationErrorCode ? (
        <div>{renderRedeemConfirmation()}</div>
      ) : (
        <div>{accessToken ? renderRedeemForm() : renderRegistrationForm()}</div>
      )}
    </div>
  );
};
export default GiftCards;
