import dynamic from 'next/dynamic';
import Loading from '../components/Loading';

const RedeemContainer = dynamic(import('../components/Redeem/Redeem'), {
  loading: () => <Loading />,
  ssr: false,
});

const Redeem = (props) => <RedeemContainer {...props} />;

export default Redeem;
