import { useMount } from 'react-use';
import { useState } from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { setupFeatureFramework, createSplitFilter } from '@surfline/web-common';

import getConfig from '../config';
import { AppHead, Layout } from '../components';
import store from '../store';
import { fetchCountryCode, loadUser } from '../store/actions/user';
import { fetchGiftProducts, getGiftRedeemPromotion } from '../store/actions/gifts';
import { getUser } from '../store/selectors/user';
import * as treatments from '../utils/treatments';
import Loading from '../components/Loading';

const config = getConfig();

/**
 * @param {object} props
 * @param {import('next/app').AppProps["Component"]} props.Component
 * @param {import('next/app').AppProps["pageProps"]} props.pageProps
 */
const GiftApp = ({ Component, pageProps }) => {
  const [appReady, setAppReady] = useState(false);
  useMount(async () => {
    await store.dispatch(loadUser());
    await store.dispatch(fetchCountryCode());

    try {
      await setupFeatureFramework({
        authorizationKey: config.splitio,
        user: getUser(store.getState()),
        splitFilters: createSplitFilter(treatments),
      });
    } finally {
      await Promise.all(
        [fetchGiftProducts, fetchGiftProducts, getGiftRedeemPromotion].map((action) =>
          store.dispatch(action()),
        ),
      );

      setAppReady(true);
    }
  });

  return (
    <Provider store={store}>
      <AppHead config={config} />
      <Layout>{appReady ? <Component {...pageProps} /> : <Loading />}</Layout>
    </Provider>
  );
};

GiftApp.propTypes = {
  pageProps: PropTypes.shape({}).isRequired,
  Component: PropTypes.node.isRequired,
};

export default GiftApp;
