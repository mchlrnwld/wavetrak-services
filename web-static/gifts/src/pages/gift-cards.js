import dynamic from 'next/dynamic';
import Loading from '../components/Loading';

const GiftCardsContainer = dynamic(import('../components/GiftCards/GiftCards'), {
  loading: () => <Loading />,
  ssr: false,
});

const GiftCards = (props) => <GiftCardsContainer {...props} />;

export default GiftCards;
