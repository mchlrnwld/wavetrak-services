import createReducer from './createReducer';
import {
  VALIDATE_CODE,
  VALIDATE_CODE_SUCCESS,
  VALIDATE_CODE_FAIL,
  REDEEM,
  REDEEM_SUCCESS,
  REDEEM_FAIL,
  REDEEM_RESET,
} from '../actions/redeem';

const initialState = {
  giftProduct: null,
  subscription: null,
  validationError: null,
  validationErrorCode: null,
};
const handlers = {};

handlers[VALIDATE_CODE] = (state) => ({
  ...state,
});

handlers[VALIDATE_CODE_SUCCESS] = (state, { giftProduct, subscription, code }) => ({
  ...state,
  giftProduct,
  subscription,
  code,
  validationError: null,
  validationErrorCode: null,
});

handlers[VALIDATE_CODE_FAIL] = (state, { error, errorCode }) => ({
  ...state,
  validationError: error,
  validationErrorCode: errorCode,
});

handlers[REDEEM] = (state) => ({
  ...state,
});

handlers[REDEEM_SUCCESS] = (state, { accountBalance }) => ({
  ...state,
  validationError: null,
  validationErrorCode: null,
  accountBalance,
  redeemSuccess: true,
});

handlers[REDEEM_FAIL] = (state, { error, errorCode }) => ({
  ...state,
  validationError: error,
  validationErrorCode: errorCode,
});

handlers[REDEEM_RESET] = () => ({
  ...initialState,
});

export default createReducer(handlers, initialState);
