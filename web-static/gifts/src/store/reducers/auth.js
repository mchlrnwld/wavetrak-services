import createReducer from './createReducer';

import { SET_ACCESS_TOKEN, REMOVE_ACCESS_TOKEN } from '../actions/auth';

const initialState = {
  userInitiated: true,
};
const handlers = {};

handlers[SET_ACCESS_TOKEN] = (state, { accessToken, userInitiated }) => ({
  ...state,
  accessToken,
  userInitiated,
});

handlers[REMOVE_ACCESS_TOKEN] = (state) => ({
  ...state,
  accessToken: null,
  userInitiated: false,
});

export default createReducer(handlers, initialState);
