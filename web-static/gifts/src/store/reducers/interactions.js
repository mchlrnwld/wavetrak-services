import createReducer from './createReducer';
import {
  TOGGLE_REGISTRATION_FORM,
  START_SUBMIT_SPINNER,
  STOP_SUBMIT_SPINNER,
} from '../actions/interactions';

const initialState = {
  showLogin: false,
  showOfferLogin: true,
  submitting: false,
  authenticated: false,
};
const handlers = {};

handlers[TOGGLE_REGISTRATION_FORM] = (state, { showLogin }) => ({
  ...state,
  showLogin,
});

handlers[START_SUBMIT_SPINNER] = (state) => ({
  ...state,
  submitting: true,
});

handlers[STOP_SUBMIT_SPINNER] = (state) => ({
  ...state,
  submitting: false,
});

export default createReducer(handlers, initialState);
