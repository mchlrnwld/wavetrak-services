import { combineReducers } from 'redux';

import user from './user';
import interactions from './interactions';
import auth from './auth';
import gifts from './gifts';
import login from './login';
import facebook from './facebook';
import redeem from './redeem';

export default combineReducers({
  user,
  interactions,
  auth,
  gifts,
  login,
  facebook,
  redeem,
});
