import createReducer from './createReducer';
import {
  FETCH_GIFT_PRODUCTS,
  FETCH_GIFT_PRODUCTS_SUCCESS,
  FETCH_GIFT_PRODUCTS_FAILURE,
  SELECT_GIFT_PRODUCT,
  SAVE_GIFT_DATA,
  EDIT_GIFT,
  PURCHASE_GIFT_FAILURE,
  PURCHASE_GIFT_SUCCESS,
  FETCH_PROMOTION,
  FETCH_PROMOTION_SUCCESS,
  FETCH_PROMOTION_FAILURE,
} from '../actions/gifts';

const initialState = {
  showPaymentForm: false,
  savedGiftData: null,
};
const handlers = {};

handlers[FETCH_GIFT_PRODUCTS] = (state) => ({
  ...state,
  loading: true,
});
handlers[FETCH_GIFT_PRODUCTS_SUCCESS] = (state, { gifts }) => ({
  ...state,
  ...gifts,
  error: null,
  loading: false,
});
handlers[FETCH_GIFT_PRODUCTS_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});

handlers[SELECT_GIFT_PRODUCT] = (state, { selectedProductId }) => ({
  ...state,
  selectedProduct:
    state.products && state.products.find((product) => product._id === selectedProductId),
});

handlers[SAVE_GIFT_DATA] = (state, { savedGiftData, showPaymentForm }) => ({
  ...state,
  savedGiftData,
  showPaymentForm,
});

handlers[EDIT_GIFT] = (state, { showPaymentForm }) => ({
  ...state,
  showPaymentForm,
});

handlers[PURCHASE_GIFT_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
});

handlers[PURCHASE_GIFT_SUCCESS] = (state) => ({
  ...state,
  showPaymentForm: false,
  purchaseSuccess: true,
});

handlers[FETCH_PROMOTION] = (state) => {
  return {
    ...state,
    loading: true,
  };
};

handlers[FETCH_PROMOTION_SUCCESS] = (state, { promotion }) => {
  return {
    ...state,
    promotion,
    error: null,
    loading: false,
  };
};

handlers[FETCH_PROMOTION_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});

export default createReducer(handlers, initialState);
