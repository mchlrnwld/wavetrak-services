import { Cookies } from 'react-cookie';
import { nrAddPageAction, nrNoticeError } from '@surfline/web-common';
import getConfig from '../../config';
import { validateRedemptionCode, redeem } from '../../common/api/redeem';
import { startSubmitSpinner, stopSubmitSpinner } from './interactions';

const cookies = new Cookies();
const config = getConfig();

export const VALIDATE_CODE = 'VALIDATE_CODE';
export const VALIDATE_CODE_SUCCESS = 'VALIDATE_CODE_SUCCESS';
export const VALIDATE_CODE_FAIL = 'VALIDATE_CODE_FAIL';
export const REDEEM = 'REDEEM';
export const REDEEM_SUCCESS = 'REDEEM_SUCCESS';
export const REDEEM_FAIL = 'REDEEM_FAIL';
export const REDEEM_RESET = 'REDEEM_RESET';

export const reset = () => ({ type: REDEEM_RESET });

export const validateCode = (code) => async (dispatch) => {
  try {
    const { brand } = config;
    if (!code.length) {
      const error = 'Code cannot be blank.';
      return dispatch({ type: VALIDATE_CODE_FAIL, error });
    }
    const accessToken = cookies.get('access_token');
    dispatch(startSubmitSpinner());
    const { giftProduct, subscription } = await validateRedemptionCode(accessToken, code, brand);
    const subscriptionToCredit =
      subscription && subscription.type === 'stripe' ? subscription : null;
    dispatch(stopSubmitSpinner());
    return dispatch({
      type: VALIDATE_CODE_SUCCESS,
      giftProduct,
      subscription: subscriptionToCredit,
      code,
    });
  } catch (err) {
    dispatch(stopSubmitSpinner());
    nrAddPageAction('Error validating redemption code', err);
    nrNoticeError('Error validating redemption code', err);
    return dispatch({
      type: VALIDATE_CODE_FAIL,
      error: err.message,
      errorCode: err.code,
    });
  }
};

export const confirmRedemption = (code) => async (dispatch) => {
  try {
    const { brand } = config;
    const accessToken = cookies.get('access_token');
    dispatch(startSubmitSpinner());
    if (!code.length) {
      const error = 'Code cannot be blank.';
      return dispatch({ type: VALIDATE_CODE_FAIL, error });
    }
    nrAddPageAction('Redeeming gift', code);
    const { subscription, accountBalance } = await redeem(accessToken, code, brand);
    dispatch(stopSubmitSpinner());
    return dispatch({ type: REDEEM_SUCCESS, subscription, accountBalance });
  } catch (err) {
    dispatch(stopSubmitSpinner());
    nrAddPageAction('Error redeeming gift', err);
    nrNoticeError('Error redeeming gift', err);
    return dispatch({
      type: REDEEM_FAIL,
      error: err.message,
      errorCode: err.code,
    });
  }
};
