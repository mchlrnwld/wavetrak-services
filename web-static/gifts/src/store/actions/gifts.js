import { nrAddPageAction, nrNoticeError, trackEvent } from '@surfline/web-common';
import { stopSubmitSpinner, startSubmitSpinner } from './interactions';
import { getGiftProducts, purchase, getPromotion } from '../../common/api/gifts';
import getConfig from '../../config';

const { brand } = getConfig();

export const FETCH_GIFT_PRODUCTS = 'FETCH_GIFT_PRODUCTS';
export const FETCH_GIFT_PRODUCTS_SUCCESS = 'FETCH_GIFT_PRODUCTS_SUCCESS';
export const FETCH_GIFT_PRODUCTS_FAILURE = 'FETCH_GIFT_PRODUCTS_FAILURE';
export const SELECT_GIFT_PRODUCT = 'SELECT_GIFT_PRODUCT';
export const SAVE_GIFT_DATA = 'SAVE_GIFT_DATA';
export const EDIT_GIFT = 'EDIT_GIFT';
export const PURCHASE_GIFT_FAILURE = 'PURCHASE_GIFT_FAILURE';
export const PURCHASE_GIFT_SUCCESS = 'PURCHASE_GIFT_SUCCESS';
export const FETCH_PROMOTION = 'FETCH_PROMOTION';
export const FETCH_PROMOTION_SUCCESS = 'FETCH_PROMOTION_SUCCESS';
export const FETCH_PROMOTION_FAILURE = 'FETCH_PROMOTION_FAILURE';

export const fetchGiftProducts = () => async (dispatch) => {
  try {
    dispatch({ type: FETCH_GIFT_PRODUCTS });
    const gifts = await getGiftProducts(brand);
    const { products } = gifts;

    products.sort((gift1, gift2) => parseInt(gift2.duration, 10) - parseInt(gift1.duration, 10));
    const selectedProduct = products.find((product) => !!product.bestValue);

    return dispatch({ type: FETCH_GIFT_PRODUCTS_SUCCESS, gifts: { ...gifts, selectedProduct } });
  } catch (err) {
    const { message } = err;
    return dispatch({ type: FETCH_GIFT_PRODUCTS_FAILURE, message });
  }
};

export const selectGiftProduct = (selectedProductId) => ({
  type: SELECT_GIFT_PRODUCT,
  selectedProductId,
});

export const saveGift = (form) => async (dispatch) => {
  const { recipientName, recipientEmail } = form;
  const showPaymentForm = true;
  const savedGiftData = {
    ...form,
    recipientEmail: recipientEmail || form.purchaserEmail,
    recipientName: recipientName || form.purchaserName,
  };
  dispatch({ type: SAVE_GIFT_DATA, savedGiftData, showPaymentForm });
};

export const editGift = () => ({ type: EDIT_GIFT, showPaymentForm: false });

export const purchaseGift = (gift) => async (dispatch) => {
  const { source } = gift;
  dispatch(startSubmitSpinner());
  nrAddPageAction('Started Gift Purchase', gift.giftProductId);
  if (!source) {
    dispatch(stopSubmitSpinner());
    nrAddPageAction('Gift purchase failure - credit card required', gift.giftProductId);
    return dispatch({ type: PURCHASE_GIFT_FAILURE, message: 'A valid credit card is required.' });
  }
  try {
    await purchase(gift);
    nrAddPageAction('Completed gift purchase', gift.giftProductId);
    trackEvent('Completed Order - Gift Purchase');
    dispatch(stopSubmitSpinner());
    return dispatch({ type: PURCHASE_GIFT_SUCCESS });
  } catch (err) {
    const { message } = err;
    dispatch(stopSubmitSpinner());
    nrAddPageAction('Gift purchase failed', gift.giftProductId, message);
    nrNoticeError('Gift purchase failed', message);
    return dispatch({ type: PURCHASE_GIFT_FAILURE, message });
  }
};

export const getGiftRedeemPromotion = () => async (dispatch) => {
  // promotionId would be set in the window scope by web-proxy
  const { promotionId } = window;
  if (!promotionId) return false;

  try {
    dispatch({ type: FETCH_PROMOTION });
    const promotion = await getPromotion(promotionId);
    return dispatch({ type: FETCH_PROMOTION_SUCCESS, promotion });
  } catch (err) {
    const { message } = err;
    return dispatch({ type: FETCH_PROMOTION_FAILURE, message });
  }
};
