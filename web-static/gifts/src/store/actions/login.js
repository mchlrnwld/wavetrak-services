import {
  getWavetrakIdentity,
  nrNoticeError,
  setWavetrakIdentity,
  trackEvent,
} from '@surfline/web-common';
import { generateToken, authorise } from '../../common/api/user';
import { getEntitlement } from '../../common/api/entitlement';
import setLoginCookie from '../../utils/setLoginCookie';
import deviceInfo from '../../utils/deviceInfo';
import { setAccessToken } from './auth';
import { stopSubmitSpinner, startSubmitSpinner } from './interactions';
import getConfig from '../../config';

export const LOGIN = 'LOGIN';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';

export const login = (values) => async (dispatch) => {
  try {
    dispatch(startSubmitSpinner());
    const { email, password, staySignedIn, location, promotionId } = values;

    const { deviceId, deviceType } = deviceInfo();

    const valuesToEncode = {
      grant_type: 'password',
      username: email,
      password,
      device_id: deviceId,
      device_type: deviceType,
      forced: true,
    };

    const urlEncodedFormValues = [];
    const keysToEncode = Object.keys(valuesToEncode);

    keysToEncode.forEach((key) => {
      const encodedKey = encodeURIComponent(key);
      const encodedValue = encodeURIComponent(valuesToEncode[key]);
      urlEncodedFormValues.push(`${encodedKey}=${encodedValue}`);
    });

    const urlEncodedFormString = urlEncodedFormValues.join('&');

    const {
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: expiresIn,
    } = await generateToken(!staySignedIn, urlEncodedFormString);

    const { id } = await authorise(accessToken);

    const maxAge = values.staySignedIn ? expiresIn : 1800;
    const refreshMaxAge = values.staySignedIn ? 31536000 : 1800;
    await setLoginCookie(accessToken, refreshToken, maxAge, refreshMaxAge);

    if (window?.analytics) {
      window.analytics.identify(id);
    }
    trackEvent('Signed In', {
      method: 'email',
      email,
      location,
      promotionId,
      platform: 'web',
    });
    const identity = getWavetrakIdentity();
    setWavetrakIdentity({
      ...identity,
      email,
      userId: id,
      type: 'logged_in',
    });
    const { entitlements } = await getEntitlement(accessToken);
    const { company, redirectUrl } = getConfig();
    if (entitlements.includes(`${company}_premium`)) {
      return window.location.assign(`${redirectUrl}/account/subscription`);
    }
    dispatch(setAccessToken(accessToken));
    dispatch(stopSubmitSpinner());
    return dispatch({ type: LOGIN_SUCCESS });
  } catch (error) {
    const err = { _error: error.error_description };
    nrNoticeError('Login failed', error.error_description);
    dispatch(stopSubmitSpinner());
    dispatch({ type: LOGIN_FAIL, error: error.error_description });
    throw err;
  }
};
