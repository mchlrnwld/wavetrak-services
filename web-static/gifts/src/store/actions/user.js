import { Cookies } from 'react-cookie';
import {
  getWavetrakIdentity,
  getWindow,
  setWavetrakIdentity,
  trackEvent,
} from '@surfline/web-common';
import { getUser, postUser } from '../../common/api/user';
import { stopSubmitSpinner, startSubmitSpinner } from './interactions';
import deviceInfo from '../../utils/deviceInfo';
import setLoginCookie from '../../utils/setLoginCookie';
import getConfig from '../../config';
import { setAccessToken, validateAccessToken } from './auth';
import { getLocation } from '../../common/api/geo';
import { getAccessToken } from '../../utils/getAccessToken';

export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';
export const CREATE = 'CREATE';
export const CREATE_SUCCESS = 'CREATE_SUCCESS';
export const CREATE_FAIL = 'CREATE_FAIL';
export const SET_COUNTRY_CODE = 'SET_COUNTRY_CODE';

const cookies = new Cookies();
const { brand, clientId } = getConfig();

export const fetchUser = () => async (dispatch) => {
  try {
    dispatch({ type: FETCH_USER });
    let user;
    const accessToken = cookies.get('access_token');
    if (accessToken) user = await getUser(accessToken);
    return dispatch({ type: FETCH_USER_SUCCESS, user, accessToken });
  } catch (err) {
    const { message } = err;
    return dispatch({ type: FETCH_USER_FAILURE, message });
  }
};

export const register = (values) => async (dispatch) => {
  dispatch(startSubmitSpinner());
  try {
    const {
      firstName,
      lastName,
      email,
      password,
      receivePromotions,
      staySignedIn,
      location,
      promotionId,
    } = values;
    const { deviceId, deviceType } = deviceInfo();

    const valuesForRegistration = {
      firstName,
      lastName,
      email,
      password,
      brand,
      settings: {
        receivePromotions,
      },
      client_id: clientId,
      device_id: deviceId,
      device_type: deviceType,
    };

    trackEvent('Clicked Create Account', {
      location,
      pageName: getWindow()?.location?.pathname,
      promotionId,
    });

    const {
      token: { access_token: accessToken, refresh_token: refreshToken, expires_in: expiresIn },
      user: { _id: userId },
    } = await postUser(!staySignedIn, valuesForRegistration);

    if (getWindow()?.analytics) {
      getWindow().analytics.identify(userId);
    }

    setWavetrakIdentity({
      ...getWavetrakIdentity(),
      email,
      userId,
      type: 'logged_in',
    });

    if (location !== 'register') {
      trackEvent('Completed Checkout Step', { step: 1, promotionId });
    }

    const maxAge = values.staySignedIn ? expiresIn : 1800;
    const refreshMaxAge = values.staySignedIn ? 31536000 : 1800;

    await setLoginCookie(accessToken, refreshToken, maxAge, refreshMaxAge);
    dispatch(setAccessToken(accessToken));
    dispatch(stopSubmitSpinner());
    return dispatch({ type: CREATE_SUCCESS });
  } catch (error) {
    const errorDescription =
      error.status === 400
        ? 'Invalid email. Already in use.'
        : 'Sorry, we could not complete this request';
    const err = { _error: errorDescription };
    dispatch({ type: CREATE_FAIL, message: errorDescription });
    dispatch(stopSubmitSpinner());
    throw err;
  }
};

export const loadUser = () => async (dispatch) => {
  const accessToken = getAccessToken();

  if (!accessToken) return null;

  const { isValid } = await dispatch(validateAccessToken(accessToken));

  if (isValid) {
    await dispatch(fetchUser());
  }

  return true;
};

export const fetchCountryCode = () => async (dispatch) => {
  try {
    const {
      country: { iso_code: countryCode },
    } = await getLocation(getAccessToken());
    return dispatch({ type: SET_COUNTRY_CODE, countryCode });
  } catch {
    return dispatch({ type: SET_COUNTRY_CODE, countryCode: 'US' });
  }
};
