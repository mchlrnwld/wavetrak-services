export const getGiftProducts = (state) => {
  if (state) {
    const {
      gifts: { products },
    } = state;
    return products;
  }
  return null;
};

export const getSelectedGiftProduct = (state) => {
  if (state) {
    const {
      gifts: { selectedProduct },
    } = state;
    return selectedProduct;
  }
  return null;
};

export const getSavedGiftData = (state) => {
  if (state) {
    const {
      gifts: { savedGiftData },
    } = state;
    return savedGiftData;
  }
  return null;
};

export const getPurchaseSuccess = (state) => {
  if (state) {
    const {
      gifts: { purchaseSuccess },
    } = state;
    return purchaseSuccess;
  }
  return null;
};

export const getGiftCardsError = (state) => {
  if (state) {
    const {
      gifts: { error },
    } = state;
    return error;
  }
  return null;
};

export const getShowPaymentForm = (state) => {
  if (state) {
    const {
      gifts: { showPaymentForm },
    } = state;
    return showPaymentForm;
  }
  return null;
};

export const getGiftRedeemPromotion = (state) => {
  if (state) {
    const {
      gifts: { promotion },
    } = state;
    return promotion;
  }
  return null;
};
