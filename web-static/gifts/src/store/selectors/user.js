export const getAccessToken = (state) => {
  if (state) {
    const {
      user: { accessToken },
    } = state;
    return accessToken;
  }
  return null;
};

export const getUser = (state) => {
  if (state) {
    const { user } = state;
    return user;
  }
  return null;
};

export const isUserPremium = (state) => {
  if (state) {
    const {
      user: { isPremium },
    } = state;
    return isPremium;
  }
  return null;
};

export const getUserSuccess = (state) => {
  if (state) {
    const {
      user: { success },
    } = state;
    return success;
  }
  return null;
};

export const getCreateUserError = (state) => {
  if (state) {
    const {
      user: { error },
    } = state;
    return error;
  }
  return null;
};

export const getCountryCode = (state) => state.user?.countryCode;
