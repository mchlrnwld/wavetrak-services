export const getValidationError = (state) => {
  if (state) {
    const {
      redeem: { validationError },
    } = state;
    return validationError;
  }
  return null;
};

export const getValidationErrorCode = (state) => {
  if (state) {
    const {
      redeem: { validationErrorCode },
    } = state;
    return validationErrorCode;
  }
  return null;
};

export const getGiftProduct = (state) => {
  if (state) {
    const {
      redeem: { giftProduct },
    } = state;
    return giftProduct;
  }
  return null;
};

export const getSubscription = (state) => {
  if (state) {
    const {
      redeem: { subscription },
    } = state;
    return subscription;
  }
  return null;
};

export const getRedeemSuccess = (state) => {
  if (state) {
    const {
      redeem: { redeemSuccess },
    } = state;
    return redeemSuccess;
  }
  return null;
};

export const getRedeemCode = (state) => {
  if (state) {
    const {
      redeem: { code },
    } = state;
    return code;
  }
  return null;
};
