import base from '../base';
import getConfig from '../../../config';

const { staticHost } = getConfig();
export default {
  rail_cta: {
    ...base.rail_cta.sl,
  },
  giftCards: {
    ...base.giftCards,
    backgroundImage: `url(${staticHost}/gifts/sl/rail_cta_desktop_bg_image.jpg)`,
    layeredCardImage: `url(${staticHost}/gifts/sl/rail_cta_desktop_cards.png)`,
    brandIcon: 'url(https://wa.cdn-surfline.com/account/static/surfline-waterdrop.svg)',
  },
  payment_success: {
    subTitle: 'Welcome to Surfline Premium!',
    message:
      "Thank you for joining the Surfline family.  You're now on your way to scoring better waves, more often.  See you in the water.",
    linkText: 'Return to Surfline',
    linkAddress: 'https://surfline.com',
  },
  create_form: {
    ...base.create_form,
  },
  sign_in_funnel: {
    ...base.sign_in_funnel,
  },
  duplicateCard: {
    ...base.duplicateCard,
  },
  utils: {
    ...base.utils,
  },
  header: 'Surfline',
};
