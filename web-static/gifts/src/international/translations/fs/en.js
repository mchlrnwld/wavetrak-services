import base from '../base';
import getConfig from '../../../config';

const { staticHost } = getConfig();
export default {
  rail_cta: {
    ...base.rail_cta.fs,
  },
  giftCards: {
    ...base.giftCards,
    backgroundImage: `url(${staticHost}/gifts/fs/rail_cta_desktop_bg_image.jpg)`,
    layeredCardImage: `url(${staticHost}/gifts/fs/rail_cta_desktop_cards.png)`,
    brandIcon: 'url(https://wa.cdn-surfline.com/account/static/fishtrack-icon.svg)',
  },
  payment_success: {
    subTitle: 'Welcome to Fistrack Premium!',
    message:
      'Thank you for joining the Fistrack family.  You can now access the latest satellite fishing chards and overlays.  See you in the water.',
    linkText: 'Return to Fishtrack',
    linkAddress: 'https://fishtrack.com',
  },
  create_form: {
    ...base.create_form,
    meta_title: 'Create Your Premium Account - FishTrack',
    meta_description:
      'FishTrack Fishing Charts - Create a FishTrack account to view the latest SST, Chlorophyll and True-Color satellite charts.',
  },
  sign_in_funnel: {
    ...base.sign_in_funnel,
    title_no_trial: 'Gift Redeem - Sign in to join<br/>FishTrack Premium',
    meta_title: 'Gift Redeem - Sign In to Go Premium - FishTrack',
    meta_description:
      'Get the latest satellite imagery - sign in with your FishTrack account to go Premium.',
  },
  duplicateCard: {
    ...base.duplicateCard,
  },
  utils: {
    ...base.utils,
  },
  header: 'FishTrack',
};
