import base from '../base';
import getConfig from '../../../config';

const { staticHost } = getConfig();

export default {
  rail_cta: {
    ...base.rail_cta.bw,
  },
  giftCards: {
    ...base.giftCards,
    backgroundImage: `url(${staticHost}/gifts/bw/rail_cta_desktop_bg_image.jpg)`,
    layeredCardImage: `url(${staticHost}/gifts/bw/rail_cta_desktop_cards.png)`,
    brandIcon: 'url(https://wa.cdn-surfline.com/account/static/bw-icon.svg)',
  },
  payment_success: {
    subTitle: 'Welcome to Buoyweather Premium!',
    message:
      "Thank you for joining the Buoyweather family.  You're now on your way to safer seas with the most trusted marine weather information.",
    linkText: 'Return to Buoyweather',
    linkAddress: 'https://buoyweather.com',
  },
  create_form: {
    ...base.create_form,
    meta_title: 'Create Account - Buoyweather',
    meta_description:
      'Stay safe on the water – create an account with Buoyweather to gain instant access to point-based marine forecasts.',
  },
  sign_in_funnel: {
    ...base.sign_in_funnel,
    title_no_trial: 'Gift Redeem - Sign in to join<br/>Buoyweather Premium',
    meta_title: 'Gift Redeem - Sign In to Go Premium - Buoyweather',
    meta_description:
      'Get the latest 16-day marine forecasts – sign in with your Buoyweather account to go Premium.',
  },
  duplicateCard: {
    ...base.duplicateCard,
  },
  utils: {
    ...base.utils,
  },
  header: 'Buoyweather',
};
