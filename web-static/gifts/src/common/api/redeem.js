import fetch from '../../utils/fetch';

export const validateRedemptionCode = (accessToken, code, company) => {
  const urlPath = `/gift/redeem?code=${code}&brand=${company}`;
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const redeem = (accessToken, code, company) =>
  fetch('/gift/redeem', accessToken, {
    method: 'POST',
    body: JSON.stringify({ code, brand: company }),
  });
