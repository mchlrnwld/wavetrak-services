import fetch from '../../utils/fetch';

export const getGiftProducts = (brand) =>
  fetch(`gift/payment?product=${brand}`, null, { method: 'GET' });

export const purchase = (payload) =>
  fetch('gift/payment', null, {
    method: 'POST',
    body: JSON.stringify({ ...payload }),
  });

export const getPromotion = (id) => fetch(`promotions/${id}`, null, { method: 'GET' });
