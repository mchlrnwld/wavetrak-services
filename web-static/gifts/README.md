# Gifts Purchase/Redeem Funnel

## Description

Gifts Purchase/Redeem Funnel frontend App. Created with NextJS + React + Redux.

Uses: node version 12.

### Development
```bash
npm install
npm run dev
```

The site would be available on `http://localhost:3000`

### Deployment

This site is exported as a static website and stored in an S3 bucket per brand- `sl-product-cdn-prod/gifts`

Jenkins deploy job: https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/build-and-deploy-account-to-s3/
