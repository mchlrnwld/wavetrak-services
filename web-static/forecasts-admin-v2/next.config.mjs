// eslint-disable-next-line import/extensions
import { PHASE_DEVELOPMENT_SERVER, PHASE_EXPORT } from 'next/constants.js';
import withTM from 'next-transpile-modules';
import withImages from 'next-images';

const APP_ENV = process.env.APP_ENV || 'development';

const nextConfig = (phase) =>
  /**
   * @type {import('next').NextConfig}
   */
  ({
    swcMinify: true,
    reactStrictMode: true,
    ...(phase !== PHASE_DEVELOPMENT_SERVER && { assetPrefix: '/forecasts-v2/' }),
    trailingSlash: phase === PHASE_EXPORT,
    publicRuntimeConfig: {
      APP_ENV: process.env.APP_ENV,
    },
  });

const config = (phase) => withTM(['@surfline/admin-common'])(withImages(nextConfig(phase)));

export default config;
