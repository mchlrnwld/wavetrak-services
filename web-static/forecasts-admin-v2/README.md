## Spot Forecast Admin Tool

### Description

This is the repository for the spot forecast admin tool. This admin tool is primarily used by the
forecasters to update spot forecasts and subregion forecasts.

The application is built in NextJS and statically rendered into a client-side application that is
deployed to S3.

#### Development

```sh
npm install
npm run dev
```

#### Code Generation

- `plopfile.js` and `plop_templates` contain configuration for generating custom boilerplate code. 

- To create relevant boilerplate code for new TS React containers, run the following command from the root directory: 

```
> npm run plop container SomeContainer

✔  +! 3 files added
 -> /src/containers/SomeContainer/index.tsx
 -> /src/containers/SomeContainer/SomeContainer.test.tsx
 -> /src/containers/SomeContainer/SomeContainer.tsx
```
