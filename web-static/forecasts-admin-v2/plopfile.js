module.exports = function (plop) {
  plop.setGenerator('container', {
    description: 'TS React Container',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Name: ',
      },
    ],
    actions: [
      {
        type: 'addMany',
        destination: 'src/containers/{{name}}',
        templateFiles: 'plop_templates/container/*.hbs',
        base: 'plop_templates/container',
      },
    ],
  });
};
