import React from 'react';
import { render, screen, waitFor } from '../testUtils';
import Index from '../pages';

const useRouter = jest.spyOn(require('next/router'), 'useRouter');

describe('pages / forecasts', () => {
  useRouter.mockImplementation(() => ({ query: {} }));
  it('should render the title page', () => {
    render(<Index />, {});
    waitFor(() => {
      expect(document.title).toBe('Surfline Admin - Subregion Forecasts');
    });
  });

  it('should render the subregion search container', () => {
    render(<Index />, {});

    expect(screen.getByTestId('subregion-search-container')).toBeInTheDocument();
  });
});
