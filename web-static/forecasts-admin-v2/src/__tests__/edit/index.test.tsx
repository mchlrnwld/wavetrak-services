import React from 'react';
import { render, screen, waitFor } from '../../testUtils';
import Index from '../../pages/edit';

const useRouter = jest.spyOn(require('next/router'), 'useRouter');

describe('pages / forecasts/edit', () => {
  useRouter.mockImplementation(() => ({ query: {} }));
  it('should render the title page', () => {
    render(<Index />, {});
    waitFor(() => {
      expect(document.title).toBe('Surfline Admin - Edit Forecast');
    });
  });

  it('should render the subregion forecast page with a subregionId', () => {
    useRouter.mockImplementationOnce(() => ({ query: { subregionId: '123' } }));
    render(<Index />, {});
    expect(screen.queryByTestId(/forecast-container/i)).not.toBeNull();
    expect(screen.getByTestId('forecast-container')).toBeInTheDocument();
  });

  it('should not render the subregion forecast page without a subregionId', () => {
    render(<Index />, {});
    expect(screen.queryByTestId(/forecast-container/i)).toBeNull();
  });
});
