import { renderHook } from '@testing-library/react-hooks';
import useConditionsForm from './useConditionsForm';

describe('hooks / useConditionsForm', () => {
  const conditions = {
    waveHeight: {
      max: 0,
      min: 0,
      plus: false,
      humanRelation: 'None',
      occasionalHeight: null,
    },
    rating: 'FLAT',
  };

  let updateConditions;

  beforeEach(() => {
    updateConditions = jest.fn();
  });

  it('should load valid values from hook', () => {
    // const updateConditions = jest.fn(() => null);

    const {
      result: {
        current: {
          values: { ratings, surfHeightsMenuItems, localConditions },
        },
      },
    } = renderHook(() =>
      useConditionsForm({
        title: 'AM',
        conditions,
        surfHeight: 0,
        setSurfHeight: jest.fn(),
        updateConditions,
      }),
    );
    expect(ratings).toBeDefined();
    expect(surfHeightsMenuItems).toBeDefined();
    expect(localConditions).toEqual(conditions);
  });

  it('should call updateConditions when hangleHeightChange is called', () => {
    // const updateConditions = jest.fn(() => null);

    const { result } = renderHook(() =>
      useConditionsForm({
        title: 'AM',
        conditions,
        surfHeight: 0,
        setSurfHeight: jest.fn(),
        updateConditions,
      }),
    );

    result.current.handlers.handleHeightChange({
      target: { value: '1' },
    } as React.ChangeEvent<HTMLInputElement>);
    expect(updateConditions).toHaveBeenCalledTimes(1);
  });

  it('should call updateConditions when hangleRatingChange is called', () => {
    // const updateConditions = jest.fn(() => null);

    const { result } = renderHook(() =>
      useConditionsForm({
        title: 'AM',
        conditions,
        surfHeight: 0,
        setSurfHeight: jest.fn(),
        updateConditions,
      }),
    );

    result.current.handlers.handleRatingChange({
      target: { value: 'GOOD' },
    } as React.ChangeEvent<HTMLInputElement>);

    expect(updateConditions).toHaveBeenCalledTimes(1);
  });
});
