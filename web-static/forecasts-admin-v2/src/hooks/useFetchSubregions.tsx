import useSWR from 'swr';
import fetcher from '../utils/fetcher';

const useFetchSubregions = (subregionToSearch: string) => {
  const { data, error } = useSWR(`/subregions/?search=${subregionToSearch}`, fetcher);
  return {
    subregions: data,
    isLoading: !error && !data,
    errorMessage: error?.message,
  };
};

export default useFetchSubregions;
