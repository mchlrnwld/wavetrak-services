import subregionForecastFixture from '../fixtures/fetchedForecast.json';

const useFetchSubregionForecast = (subregionId: string) =>
  // TODO: Replace this with actual API call
  ({
    subregionForecast: { ...subregionForecastFixture, subregionId },
    // isLoading: !error && !subregionForecastFixture,
    isLoading: false,
    errorMessage: '',
  });
export default useFetchSubregionForecast;
