import { ChangeEvent, useCallback, useState } from 'react';
import COLOR_CONDITIONS from '../constants/colorConditions';
import surfConditions from '../fixtures/surfConditions';
import Condition from '../types/condition';

interface Props {
  title: 'AM' | 'PM';
  conditions: Condition;
  surfHeight: number;
  setSurfHeight(height: number): void;
  updateConditions(time: 'am' | 'pm', data: Condition): void;
}

const useConditionsForm = ({
  title,
  conditions,
  surfHeight,
  setSurfHeight,
  updateConditions,
}: Props) => {
  const [localConditions, setLocalConditions] = useState({ ...conditions });
  const conditionsTitle = title === 'AM' ? 'am' : 'pm';
  const ratings = Object.keys(COLOR_CONDITIONS);
  const surfHeightsMenuItems = Object.keys(surfConditions);

  const updateHeight = useCallback(
    (direction: 'inc' | 'dec') => () => {
      if (
        (direction === 'dec' && surfHeight === 0) ||
        (direction === 'inc' && surfHeight === surfHeightsMenuItems.length - 1)
      ) {
        return;
      }

      updateConditions(conditionsTitle, {
        ...localConditions,
        waveHeight: {
          ...localConditions.waveHeight,
          min:
            direction === 'inc'
              ? surfConditions[surfHeight + 1].minHeight
              : surfConditions[surfHeight - 1].minHeight,
          max:
            direction === 'inc'
              ? surfConditions[surfHeight + 1].maxHeight
              : surfConditions[surfHeight - 1].maxHeight,
          humanRelation:
            direction === 'inc'
              ? surfConditions[surfHeight + 1].humanRelation
              : surfConditions[surfHeight - 1].humanRelation,
          plus:
            direction === 'inc'
              ? surfConditions[surfHeight + 1].condition.includes('ft +')
              : surfConditions[surfHeight - 1].condition.includes('ft +'),
        },
      });
      setSurfHeight(direction === 'inc' ? surfHeight + 1 : surfHeight - 1);
    },
    [
      localConditions,
      conditionsTitle,
      updateConditions,
      setSurfHeight,
      surfHeight,
      surfHeightsMenuItems.length,
    ],
  );

  const updateRating = useCallback(
    (direction: 'inc' | 'dec') => () => {
      if (
        (direction === 'dec' && ratings.indexOf(localConditions.rating) === 0) ||
        (direction === 'inc' && ratings.indexOf(localConditions.rating) === ratings.length - 1)
      ) {
        return;
      }

      updateConditions(conditionsTitle, {
        ...localConditions,
        rating:
          direction === 'inc'
            ? ratings[ratings.indexOf(localConditions.rating) + 1]
            : ratings[ratings.indexOf(localConditions.rating) - 1],
      });
    },
    [ratings, localConditions, conditionsTitle, updateConditions],
  );

  const handleHeightChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setSurfHeight(parseInt(event.target.value, 10));
      updateConditions(conditionsTitle, {
        ...localConditions,
        waveHeight: {
          ...localConditions.waveHeight,
          min: surfConditions[event.target.value].minHeight,
          max: surfConditions[event.target.value].maxHeight,
          humanRelation: surfConditions[event.target.value].humanRelation,
          plus: surfConditions[event.target.value].condition.includes('ft +'),
        },
      });
    },
    [localConditions, conditionsTitle, setSurfHeight, updateConditions],
  );

  const handleRatingChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      updateConditions(conditionsTitle, {
        ...localConditions,
        rating: event.target.value,
      });
    },
    [localConditions, conditionsTitle, updateConditions],
  );

  return {
    setLocalConditions,
    functions: { updateHeight, updateRating },
    handlers: { handleHeightChange, handleRatingChange },
    values: {
      ratings,
      surfHeightsMenuItems,
      localConditions,
    },
  };
};

export default useConditionsForm;
