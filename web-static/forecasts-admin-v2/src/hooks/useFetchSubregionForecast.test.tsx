import { renderHook } from '@testing-library/react-hooks';
import subregionForecastFixture from '../fixtures/fetchedForecast.json';
import * as hooks from './useFetchSubregionForecast';

const mockSubregionsForecastFixture = { ...subregionForecastFixture, subregionId: '123' };

describe('hooks / useFetchSubregions', () => {
  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should load subregions forecast from hook', async () => {
    jest.spyOn(hooks, 'default').mockImplementation(() => ({
      subregionForecast: mockSubregionsForecastFixture,
      isLoading: false,
      errorMessage: '',
    }));

    const { result } = await renderHook(() => hooks.default('123'));
    expect(result.current.isLoading).toBe(false);
    expect(result.current.subregionForecast).toBe(mockSubregionsForecastFixture);
    expect(result.current.errorMessage).toBe('');
  });

  it('should receive an error from hook', async () => {
    jest.spyOn(hooks, 'default').mockImplementation(() => ({
      subregionForecast: null,
      isLoading: false,
      errorMessage: 'Something went wrong',
    }));

    const { result } = await renderHook(() => hooks.default('Australia'));
    expect(result.current.isLoading).toBe(false);
    expect(result.current.subregionForecast).toBeNull();
    expect(result.current.errorMessage).toBe('Something went wrong');
  });

  // eslint-disable-next-line jest/no-commented-out-tests
  /* TODO: Uncomment these tests when useFetchSubregionForecast is using API instead of Fixture
  it('should bubble up API errors', async () => {
    jest.spyOn(axios, 'get').mockImplementation(() => {
      const error = new Error('The API returned an error') as AxiosError;
      error.response = {
        status: 500,
        statusText: 'Internal Server Error',
        headers: {},
        config: {},
        data: {
          message: 'specific error',
        },
      };
      throw error;
    });

    await act(async () => {
      const { result } = await renderHook(() => hooks.default('Australia'));
      expect(result.current.isLoading).toBe(false);
      expect(result.current.subregionForecast).toBeUndefined();
      expect(result.current.errorMessage).toBe(
        'Internal Server Error. Status Code: 500. specific error',
      );
    });
  });

  it('should bubble up API errors with a default', async () => {
    jest.spyOn(axios, 'get').mockImplementation(() => {
      const error = new Error('The API returned an error') as AxiosError;
      error.response = {
        status: 500,
        statusText: 'Internal Server Error',
        headers: {},
        config: {},
        data: {},
      };
      throw error;
    });

    await act(async () => {
      const { result } = await renderHook(() => hooks.default('Australia'));
      expect(result.current.isLoading).toBe(false);
      expect(result.current.subregionForecast).toBeUndefined();
      expect(result.current.errorMessage).toBe(
        'Internal Server Error. Status Code: 500. An error occurred fetching data at url: http://spots-api.sandbox.surfline.com/admin/subregions?search=Australia',
      );
    });
  });
  */
});
