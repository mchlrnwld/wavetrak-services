import { renderHook, act } from '@testing-library/react-hooks';
import axios, { AxiosError } from 'axios';
import subregionsFixture from '../fixtures/subregionsFixture.json';
import * as hooks from './useFetchSubregions';

const mockSubregionsFixture = subregionsFixture;

describe('hooks / useFetchSubregions', () => {
  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should load subregions from hook', async () => {
    jest.spyOn(hooks, 'default').mockImplementation(() => ({
      subregions: mockSubregionsFixture,
      isLoading: false,
      errorMessage: '',
    }));

    const { result } = await renderHook(() => hooks.default('Australia'));
    expect(result.current.isLoading).toBe(false);
    expect(result.current.subregions).toBe(mockSubregionsFixture);
    expect(result.current.errorMessage).toBe('');
  });

  it('should receive an error from hook', async () => {
    jest.spyOn(hooks, 'default').mockImplementation(() => ({
      subregions: null,
      isLoading: false,
      errorMessage: 'Something went wrong',
    }));

    const { result } = await renderHook(() => hooks.default('Australia'));
    expect(result.current.isLoading).toBe(false);
    expect(result.current.subregions).toBeNull();
    expect(result.current.errorMessage).toBe('Something went wrong');
  });

  it('should bubble up API errors', async () => {
    jest.spyOn(axios, 'get').mockImplementation(() => {
      const error = new Error('The API returned an error') as AxiosError;
      error.response = {
        status: 500,
        statusText: 'Internal Server Error',
        headers: {},
        config: {},
        data: {
          message: 'Specific error',
        },
      };
      throw error;
    });

    await act(async () => {
      const { result } = await renderHook(() => hooks.default('Australia'));
      expect(result.current.isLoading).toBe(false);
      expect(result.current.subregions).toBeUndefined();
      expect(result.current.errorMessage).toBe(
        'Specific error. Internal Server Error. Status Code: 500.',
      );
    });
  });

  it('should bubble up API errors with a default', async () => {
    jest.spyOn(axios, 'get').mockImplementation(() => {
      const error = new Error('The API returned an error') as AxiosError;
      error.response = {
        status: 500,
        statusText: 'Internal Server Error',
        headers: {},
        config: {},
        data: {},
      };
      throw error;
    });

    await act(async () => {
      const { result } = await renderHook(() => hooks.default('Australia'));
      expect(result.current.isLoading).toBe(false);
      expect(result.current.subregions).toBeUndefined();
      expect(result.current.errorMessage).toBe(
        'An error occurred fetching data at url: /subregions/?search=Australia. Internal Server Error. Status Code: 500.',
      );
    });
  });
});
