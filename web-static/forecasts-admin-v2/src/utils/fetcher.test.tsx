import axios, { AxiosError } from 'axios';
import fetcher from './fetcher';

describe('utils / fetcher', () => {
  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should return data with 200 response', async () => {
    const mockResponse = {
      _id: '123',
      name: 'something',
    };
    jest.spyOn(axios, 'get').mockResolvedValueOnce({
      data: mockResponse,
    });

    const data = await fetcher('http://anyurl.valid.com');
    expect(data).toBe(mockResponse);
  });

  it('should throw a 500 error with custom error message', async () => {
    jest.spyOn(axios, 'get').mockImplementation(() => {
      const error = new Error('The API returned an error') as AxiosError;
      error.response = {
        status: 500,
        statusText: 'Internal Server Error',
        headers: {},
        config: {},
        data: {
          message: 'Specific error',
        },
      };
      throw error;
    });

    await expect(fetcher('http://anyurl.com')).rejects.toThrow(
      'Specific error. Internal Server Error. Status Code: 500.',
    );
  });

  it('should throw a 500 error with default error message', async () => {
    jest.spyOn(axios, 'get').mockImplementation(() => {
      const error = new Error('The API returned an error') as AxiosError;
      error.response = {
        status: 500,
        statusText: 'Internal Server Error',
        headers: {},
        config: {},
        data: {},
      };
      throw error;
    });

    await expect(fetcher('http://anyurl.com')).rejects.toThrow(
      'An error occurred fetching data at url: http://anyurl.com. Internal Server Error. Status Code: 500.',
    );
  });
});
