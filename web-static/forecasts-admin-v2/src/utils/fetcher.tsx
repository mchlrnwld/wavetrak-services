import axios from 'axios';
import Cookies from 'js-cookie';
import config from '../config';

const fetcher = async (url: string) => {
  try {
    const token = Cookies.get('access_token');
    const res = await axios.get(url, {
      baseURL: config.adminServicesEndpoint,
      headers: { Authorization: `Bearer ${token}` },
    });
    return res.data;
  } catch (error) {
    const apiErrorMessage =
      error.response?.data?.message || `An error occurred fetching data at url: ${url}`;
    const apiStatusText = error.response?.statusText ? `${error.response?.statusText}. ` : '';
    const apiStatusCode = error.response?.status ? `Status Code: ${error.response?.status}.` : '';
    const errorMessage = `${apiErrorMessage}. ${apiStatusText}${apiStatusCode}`;
    throw new Error(errorMessage);
  }
};

export default fetcher;
