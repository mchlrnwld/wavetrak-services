import React, { useState, useEffect } from 'react';
import { Container, TextField, Button, Stack } from '@mui/material';

import styles from './Search.module.scss';

interface Props {
  searchLabel?: string;
  defaultValue?: string;
  onSearchClick(arg: string): void;
  validationRegex?: RegExp;
  onInvalidInput?: () => void;
}
const Search: React.FC<Props> = ({
  searchLabel = 'Search',
  defaultValue = '',
  onSearchClick,
  validationRegex = /^[^+$*\\]*$/,
  onInvalidInput,
}) => {
  const [searchQuery, setSearchQuery] = useState<string>('');

  useEffect(() => {
    if (defaultValue) {
      setSearchQuery(defaultValue);
    }
  }, [defaultValue]);

  const validateInput = (input: string) => {
    const filter = new RegExp(validationRegex);
    const isValid = filter.test(input);
    if (!isValid && onInvalidInput) {
      onInvalidInput();
    }

    return isValid;
  };

  const onSearchQueryChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (validateInput(e.target.value)) {
      setSearchQuery(e.target.value);
    }
  };

  const onClick = () => {
    onSearchClick(searchQuery);
  };

  const onKeyDown = (e: React.KeyboardEvent<HTMLDivElement>) => {
    if (e.code === 'Enter') {
      onClick();
    }
  };

  return (
    <Container maxWidth="sm" data-testid="search-component">
      <Stack direction="row" sx={{ my: 2, width: 1 }}>
        <TextField
          autoFocus
          className={styles.searchInput}
          label={searchLabel}
          variant="outlined"
          type="search"
          onKeyDown={onKeyDown}
          inputProps={{ 'data-testid': 'search-component-input' }}
          onChange={onSearchQueryChanged}
          value={searchQuery}
        />
        <Button
          variant="contained"
          className={styles.searchButton}
          color="primary"
          size="large"
          onClick={onClick}
        >
          SEARCH
        </Button>
      </Stack>
    </Container>
  );
};

export default Search;
