import React from 'react';

import { render, screen, fireEvent } from '../../testUtils';
import Search from './Search';

describe('components / Search', () => {
  it('should render the search component with input and button', () => {
    const searchLabel = 'My Search Box';
    render(<Search searchLabel={searchLabel} onSearchClick={jest.fn()} />, {});
    expect(screen.getByTestId('search-component')).toBeInTheDocument();
    expect(screen.getByLabelText(searchLabel)).toBeInTheDocument();
    expect(screen.getByText('SEARCH')).toBeInTheDocument();
  });

  it('should render the search component with a default value', () => {
    const searchText = 'Central California';
    render(<Search defaultValue={searchText} onSearchClick={jest.fn()} />, {});
    expect(screen.getByTestId('search-component-input')).toHaveValue(searchText);
  });

  it('should allow a valid input search term', () => {
    const searchTermValid = 'Australia';
    const onSearchClick = jest.fn();
    render(<Search onSearchClick={onSearchClick} />, {});

    const inputEl = screen.getByTestId('search-component-input') as HTMLInputElement;
    fireEvent.change(inputEl, { target: { value: searchTermValid } });
    expect(screen.getByTestId('search-component')).toBeInTheDocument();
    expect(screen.getByTestId('search-component-input')).toHaveValue(searchTermValid);

    fireEvent.click(screen.getByRole('button'));
    expect(onSearchClick).toHaveBeenCalled();
    expect(onSearchClick).toHaveBeenLastCalledWith(searchTermValid);
  });

  it('should not allow an invalid input search term', () => {
    const searchTermInvalid = 'Australia.$**_ Test';
    const onSearchClick = jest.fn();
    render(<Search onSearchClick={onSearchClick} />, {});

    const inputEl = screen.getByTestId('search-component-input') as HTMLInputElement;
    fireEvent.change(inputEl, { target: { value: searchTermInvalid } });
    expect(screen.getByTestId('search-component')).toBeInTheDocument();
    expect(screen.getByTestId('search-component-input')).toHaveValue('');

    fireEvent.click(screen.getByRole('button'));
    expect(onSearchClick).toHaveBeenCalled();
    expect(onSearchClick).toHaveBeenLastCalledWith('');
  });

  it('Pressing enter in search input should trigger search', () => {
    const searchTermValid = 'Australia';
    const onSearchClick = jest.fn();
    render(<Search onSearchClick={onSearchClick} />, {});

    const inputEl = screen.getByTestId('search-component-input') as HTMLInputElement;
    fireEvent.change(inputEl, { target: { value: searchTermValid } });
    expect(screen.getByTestId('search-component')).toBeInTheDocument();
    expect(screen.getByTestId('search-component-input')).toHaveValue(searchTermValid);

    fireEvent.keyDown(inputEl, { key: 'Enter', code: 'Enter', charCode: 13 });
    expect(onSearchClick).toHaveBeenCalled();
    expect(onSearchClick).toHaveBeenLastCalledWith(searchTermValid);
  });

  it('Pressing non-enter key in search input should not trigger search', () => {
    const searchTermValid = 'Australia';
    const onSearchClick = jest.fn();
    render(<Search onSearchClick={onSearchClick} />, {});

    const inputEl = screen.getByTestId('search-component-input') as HTMLInputElement;
    fireEvent.change(inputEl, { target: { value: searchTermValid } });
    expect(screen.getByTestId('search-component')).toBeInTheDocument();
    expect(screen.getByTestId('search-component-input')).toHaveValue(searchTermValid);
    fireEvent.keyDown(inputEl, { key: ' ', code: 'Space', charCode: 32 });
    expect(onSearchClick).toHaveBeenCalledTimes(0);
  });
});
