import React from 'react';

import { act, render, screen, fireEvent, waitFor } from '../../testUtils';
import Alert from './Alert';

describe('components / Alert', () => {
  it('should render the alert with a title', () => {
    act(() => {
      render(
        <Alert
          severity="info"
          title="This is a title!"
          message="Whoa, this is a message"
          onClose={jest.fn()}
        />,
        {},
      );
    });

    const alert = screen.getByRole('alert');
    expect(alert).toBeInTheDocument();
    expect(screen.getByTestId('alert-title')).toBeInTheDocument();
    expect(alert).toHaveTextContent(/this is a title!/i);
    expect(alert).toHaveTextContent(/whoa, this is a message/i);
  });

  it('should render the alert without a title', () => {
    act(() => {
      render(<Alert severity="info" message="Whoa, this is a message" onClose={jest.fn()} />, {});
    });

    const alert = screen.getByRole('alert');
    expect(alert).toBeInTheDocument();
    expect(screen.queryByTestId('alert-title')).toBeNull();
  });

  it('should render the alert without a severity', () => {
    act(() => {
      render(<Alert severity="error" message="Whoa, this is a message" onClose={jest.fn()} />, {});
    });

    const alert = screen.getByRole('alert');
    expect(alert).toBeInTheDocument();
    expect(screen.queryByTestId('alert-title')).toBeNull();
  });

  it('should not render close button without onclose handler', async () => {
    act(() => {
      render(<Alert severity="info" message="Whoa, this is a message" />, {});
    });
    const alert = screen.getByRole('alert');
    expect(alert).toBeInTheDocument();
    expect(screen.queryByRole('button', { name: /close/i })).toBeNull();
  });

  it('should call onclose handler when close icon is clicked', async () => {
    const onClose = jest.fn();
    act(() => {
      render(<Alert severity="info" message="Whoa, this is a message" onClose={onClose} />, {});
    });
    const alert = screen.getByRole('alert');
    expect(alert).toBeInTheDocument();
    const closeBtn = screen.getByRole('button', { name: /close/i });
    expect(closeBtn).toBeInTheDocument();
    act(() => {
      fireEvent.click(closeBtn);
    });

    waitFor(() => expect(screen.queryByRole('alert')).toBeNull());

    expect(onClose).toHaveBeenCalledTimes(1);
  });
});
