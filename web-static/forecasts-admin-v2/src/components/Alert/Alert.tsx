import React from 'react';
import { Alert as MUIAlert, AlertColor, AlertTitle, AlertProps, IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';

interface Props {
  message: string;
  className?: string;
  title?: string;
  variant?: AlertProps['variant'];
  severity: AlertColor;
  onClose?(): void;
}
const Alert: React.FC<Props> = ({
  title = null,
  className = '',
  variant = 'standard',
  severity,
  onClose,
  message,
}) => (
  <MUIAlert
    className={className}
    severity={severity}
    variant={variant}
    action={
      onClose && (
        <IconButton
          aria-label="close"
          color="inherit"
          size="small"
          onClick={() => {
            onClose();
          }}
        >
          <CloseIcon />
        </IconButton>
      )
    }
  >
    {title && (
      <AlertTitle data-testid="alert-title">
        <strong>{title}</strong>
      </AlertTitle>
    )}
    {message}
  </MUIAlert>
);

export default Alert;
