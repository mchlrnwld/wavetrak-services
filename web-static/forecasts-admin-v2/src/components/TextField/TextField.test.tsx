import React from 'react';
import { act, render, screen, fireEvent, waitFor } from '../../testUtils';
import TextField from './TextField';

describe('components / ForecastStatusSwitch', () => {
  it('should render the textfield', () => {
    act(() => {
      render(
        <TextField
          className="testClass"
          value="my value"
          label="textfield test"
          onChange={jest.fn()}
        />,
        {},
      );
    });
    const textbox = screen.getByRole('textbox', { name: /textfield test/i });
    expect(textbox).toBeInTheDocument();
    expect(textbox).toHaveValue('my value');
  });

  it('should only accept text with valid length', async () => {
    const onChange = jest.fn();
    act(() => {
      render(
        <TextField
          className="testClass"
          value="my value"
          maxLength={20}
          rows={3}
          label="textfield test"
          onChange={onChange}
          multiline
        />,
        {},
      );
    });
    const inputValid = 'Short message';
    const inputTooLong = '012345678901234567890';

    // toggle to inactive (default fixture is active)
    const inputEl = screen.getByRole('textbox', { name: /textfield test/i });
    expect(inputEl).toBeInTheDocument();
    expect(inputEl).toHaveProperty('maxLength', 20);
    expect(inputEl).toHaveValue('my value');
    act(() => {
      fireEvent.change(inputEl, { target: { value: inputValid } });
    });
    waitFor(() => expect(inputEl).toHaveValue(inputValid));
    // try to set with value that is too long and it should still equal previous value
    act(() => {
      fireEvent.change(inputEl, { target: { value: inputTooLong } });
    });
    waitFor(() => expect(inputEl).toHaveValue(inputValid));

    expect(onChange).toHaveBeenCalledTimes(1);
  });

  it('should render the textfield without a label or className', () => {
    act(() => {
      render(<TextField value="my value" onChange={jest.fn()} />, {});
    });

    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });
});
