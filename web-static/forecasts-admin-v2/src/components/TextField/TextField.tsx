import React from 'react';
import { InputAdornment, TextField as MUITextField } from '@mui/material';
import styles from './TextField.module.scss';

interface Props {
  onChange(args: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void;
  value: string;
  id?: string;
  className?: string;
  label?: string;
  maxLength?: number;
  multiline?: boolean;
  rows?: number;
}
const TextField: React.FC<Props> = ({
  maxLength = 250,
  onChange,
  value,
  className = '',
  id = null,
  multiline = false,
  rows = 1,
  label = '',
}) => (
  <MUITextField
    id={id}
    className={className}
    value={value}
    variant="standard"
    multiline={multiline}
    label={label}
    rows={rows}
    InputProps={{
      endAdornment: (
        <InputAdornment className={styles.characterCount} position="end">
          {value?.length} / {maxLength}
        </InputAdornment>
      ),
      inputProps: { maxLength },
    }}
    onChange={(e) => {
      if (e.target.value.length <= maxLength) {
        onChange(e);
      }
    }}
  />
);

export default TextField;
