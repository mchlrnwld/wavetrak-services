import React from 'react';
import { Stack, Typography, Switch, FormControl, FormLabel, FormGroup } from '@mui/material';

interface Props {
  forecastActive: boolean;
  onForecastStatusToggled(arg: boolean): void;
}
const ForecastStatusSwitch: React.FC<Props> = ({ forecastActive, onForecastStatusToggled }) => (
  <FormControl component="fieldset" variant="standard">
    <FormLabel component="legend">Human Forecast</FormLabel>
    <FormGroup>
      <Stack direction="row" spacing={1} alignItems="center">
        <Typography>Inactive</Typography>
        <Switch
          data-testid="forecast-status-switch"
          checked={forecastActive}
          onChange={(e) => onForecastStatusToggled(e.target.checked)}
        />
        <Typography>Active</Typography>
      </Stack>
    </FormGroup>
  </FormControl>
);

export default ForecastStatusSwitch;
