import React from 'react';

import { render, screen, fireEvent } from '../../testUtils';
import ForecastStatusSwitch from './ForecastStatusSwitch';

describe('components / ForecastStatusSwitch', () => {
  it('should render the switch as checked', () => {
    render(<ForecastStatusSwitch forecastActive onForecastStatusToggled={jest.fn()} />, {});

    const switchEl = screen.getByRole('checkbox');
    expect(switchEl).toBeInTheDocument();
    expect(switchEl).toHaveProperty('checked', true);
  });

  it('should call onForecastStatusToggled when clicked', () => {
    const onForecastStatusToggled = jest.fn();
    const forecastActive = false;
    render(
      <ForecastStatusSwitch
        forecastActive={forecastActive}
        onForecastStatusToggled={onForecastStatusToggled}
      />,
      {},
    );

    expect(screen.getByRole('checkbox')).toHaveProperty('checked', false);
    screen.getByRole('checkbox').click();
    fireEvent.change(screen.getByRole('checkbox'), { target: { checked: true } });

    expect(screen.getByRole('checkbox')).toHaveProperty('checked', true);
    expect(onForecastStatusToggled).toHaveBeenCalledTimes(1);
    expect(onForecastStatusToggled).toHaveBeenLastCalledWith(true);
  });
});
