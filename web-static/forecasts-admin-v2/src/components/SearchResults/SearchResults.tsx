import React from 'react';
import Link from 'next/link';

import { Container, List, ListItem, ListItemText } from '@mui/material';

import styles from './SearchResults.module.scss';

interface Props {
  baseUrl: string;
  results: Array<{
    name: string;
    _id: string;
  }>;
}
const SearchResults: React.FC<Props> = ({ results, baseUrl }) => (
  <Container maxWidth="sm" data-testid="search-results-component" className={styles.container}>
    <List>
      {results &&
        results.map((result) => (
          <Link key={result._id} href={`${baseUrl}${result._id}`} passHref>
            <ListItem className={styles.resultItem}>
              <ListItemText primary={result.name} />
              <p className={styles.link}>EDIT</p>
            </ListItem>
          </Link>
        ))}
    </List>
  </Container>
);

export default SearchResults;
