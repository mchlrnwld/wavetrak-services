import React from 'react';
import subregionsFixture from '../../fixtures/subregionsFixture.json';
import { render, screen, within } from '../../testUtils';
import SearchResults from './SearchResults';

describe('components / SearchResults', () => {
  const BASEURL = '/forecasts/edit?subregionId=';
  it('should render the search results component', () => {
    render(<SearchResults results={[]} baseUrl={BASEURL} />, {});
    expect(screen.getByTestId('search-results-component')).toBeInTheDocument();

    const listItems = screen.queryAllByRole('listitem');
    expect(listItems).toHaveLength(0);

    const editLinks = screen.queryAllByText('EDIT');
    expect(editLinks).toHaveLength(0);
  });

  it('should render search results with four items', () => {
    render(<SearchResults results={subregionsFixture} baseUrl={BASEURL} />, {});
    expect(screen.getByTestId('search-results-component')).toBeInTheDocument();

    const list = screen.getByRole('list');
    const listItems = within(list).getAllByRole('listitem');
    expect(listItems).toHaveLength(4);

    const subWesternAus = screen.queryAllByText('Western Australia');
    const subSouthwestAus = screen.queryAllByText('Southwest Australia');
    const subSoutheastAus = screen.queryAllByText('Southeast Australia');
    const subNorthAus = screen.queryAllByText('Australia North Coast');
    expect(subWesternAus).toHaveLength(1);
    expect(subSouthwestAus).toHaveLength(1);
    expect(subSoutheastAus).toHaveLength(1);
    expect(subNorthAus).toHaveLength(1);

    const editLinks = screen.queryAllByText('EDIT');
    expect(editLinks).toHaveLength(4);
  });

  it('should render clickable search results with link to redirect', async () => {
    render(<SearchResults results={[subregionsFixture[0]]} baseUrl={BASEURL} />, {});
    expect(screen.getByRole('listitem')).toHaveAttribute(
      'href',
      '/forecasts/edit?subregionId=58581a836630e24c4487902f',
    );
  });
});
