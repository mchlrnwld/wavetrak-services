import React, { ChangeEvent, useCallback, useMemo, useState } from 'react';
import { Select, Typography, MenuItem, Input, Box } from '@mui/material';
import styles from './SelectMenu.module.scss';

interface Props<T> {
  value: string | number;
  menuItemsList: Array<number | string>;
  onChange(e: ChangeEvent<HTMLInputElement>): void;
  selectValueClassName?: string;
  className?: string;
  formatSelectedValue?(item: T): T;
  formatMenuItem?(item: T): T;
}

const SelectMenu = <T extends string | number>({
  value,
  selectValueClassName = '',
  menuItemsList,
  className = '',
  onChange,
  formatSelectedValue,
  formatMenuItem,
}: Props<T>) => {
  const [menuOpen, setMenuOpen] = useState(false);

  const openMenu = useCallback(() => setMenuOpen(true), []);
  const closeMenu = useCallback(() => setMenuOpen(false), []);
  const handleChange = useCallback((event) => onChange(event), [onChange]);

  const renderValue = useCallback(
    (selected: T) => (
      <Typography
        variant="h3"
        role="heading"
        data-testid="select-menu-display"
        className={selectValueClassName}
      >
        {formatSelectedValue ? formatSelectedValue(selected) : selected}
      </Typography>
    ),
    [formatSelectedValue, selectValueClassName],
  );

  const menuList = useMemo(
    () =>
      menuItemsList.map((item: T) => (
        <MenuItem key={item} value={item}>
          {formatMenuItem ? formatMenuItem(item) : item}
        </MenuItem>
      )),
    [formatMenuItem, menuItemsList],
  );
  return (
    <Box sx={{ m: 1 }}>
      <Select
        open={menuOpen}
        onClose={closeMenu}
        onOpen={openMenu}
        value={value}
        className={className}
        onChange={handleChange}
        renderValue={renderValue}
        IconComponent="p"
        input={<Input disableUnderline />}
        inputProps={{ 'data-testid': 'select-menu-input' }}
        SelectDisplayProps={{ className: styles.selectDisplay }}
      >
        {menuList}
      </Select>
    </Box>
  );
};

export default SelectMenu;
