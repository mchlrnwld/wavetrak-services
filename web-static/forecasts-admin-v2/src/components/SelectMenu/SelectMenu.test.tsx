import React from 'react';
import surfConditions from '../../fixtures/surfConditions';

import { render, screen, fireEvent } from '../../testUtils';
import SelectMenu from './SelectMenu';

describe('components / SelectMenu', () => {
  const menuItemsList = Object.keys(surfConditions);

  it('should have heading with value equal to value prop', () => {
    render(<SelectMenu menuItemsList={menuItemsList} value={1} onChange={jest.fn()} />, {});

    const val = screen.getByRole('heading');
    expect(val).toBeInTheDocument();
    expect(val).toHaveTextContent('1');
  });

  it('should open menu on clicked input', () => {
    const onChangeFunction = jest.fn();
    render(<SelectMenu menuItemsList={menuItemsList} value={1} onChange={onChangeFunction} />, {});

    const menu = screen.getByTestId('select-menu-input');
    fireEvent.change(menu, { target: { value: 2 } });

    expect(onChangeFunction).toHaveBeenCalledTimes(1);
  });
});
