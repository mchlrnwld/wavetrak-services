import React from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import Forecast from '../../containers/Forecast';

const Index: React.FunctionComponent = () => {
  const router = useRouter();
  const subregionId = router.query?.subregionId as string;
  return (
    <>
      <Head>
        <title>Surfline Admin - Edit Forecast</title>
      </Head>
      {subregionId && <Forecast subregionId={subregionId} />}
    </>
  );
};

export default Index;
