/* istanbul ignore file */
import type { AppProps } from 'next/app';
import Head from 'next/head';
import { theme, WavetrakThemeProvider } from '@wavetrak/theme';
import { AppContainer } from '@surfline/admin-common';
import '../theme/app.scss';

const MyApp: React.FunctionComponent<AppProps> = ({ Component, pageProps }) => (
  <>
    <Head>
      <meta name="viewport" content="initial-scale=1, width=device-width" />
      <meta name="theme-color" content={theme.palette.primary.main} />
    </Head>
    <WavetrakThemeProvider>
      <AppContainer>
        <Component {...pageProps} />
      </AppContainer>
    </WavetrakThemeProvider>
  </>
);

export default MyApp;
