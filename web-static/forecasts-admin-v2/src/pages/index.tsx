import React from 'react';
import Head from 'next/head';

import SearchSubregions from '../containers/SearchSubregions';

const Index: React.FunctionComponent = () => (
  <>
    <Head>
      <title>Surfline Admin - Subregion Forecasts</title>
    </Head>
    <SearchSubregions />
  </>
);

export default Index;
