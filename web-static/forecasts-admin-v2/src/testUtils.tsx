import React, { ReactElement } from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { render, RenderOptions } from '@testing-library/react';
import { WavetrakThemeProvider } from '@wavetrak/theme';

const Providers = ({ children }) => <WavetrakThemeProvider>{children}</WavetrakThemeProvider>;

export const customRender = (ui: ReactElement, options?: Omit<RenderOptions, 'wrapper'>) =>
  render(ui, { wrapper: Providers, ...options });

// re-export everything
// eslint-disable-next-line import/no-extraneous-dependencies
export * from '@testing-library/react';
// override render method
// export { customRender as render };
