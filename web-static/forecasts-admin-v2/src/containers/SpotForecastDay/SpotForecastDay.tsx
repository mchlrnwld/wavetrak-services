import React, { useState, useEffect, useCallback } from 'react';
import { Box, Button, Card, CardContent, Divider, Stack, Typography } from '@mui/material';
import SpotForecastDayConditions from '../SpotForecastDayConditions';
import TextField from '../../components/TextField';
import styles from './SpotForecastDay.module.scss';
import Condition from '../../types/condition';
import Conditions from '../../types/conditions';

type ForecastDayData = {
  am: Condition;
  pm: Condition;
  observation: string;
};

interface Props {
  day: number;
  forecastDay: Conditions;
  hideApplyNextButton?: boolean;
  updateAt(day: number, time: 'am' | 'pm', forecastDayFormData: Conditions, data: Condition): void;
  applyNext(day: number, data: ForecastDayData): void;
  setWrittenObservation(day: number, obs: string): void;
}

const SpotForecastDay = React.memo<Props>(
  ({
    day,
    forecastDay,
    hideApplyNextButton = false,
    updateAt,
    applyNext,
    setWrittenObservation,
  }) => {
    const [forecastDayFormData, setForecastDayFormData] = useState(forecastDay);

    // keep this in a separate state to allow for local changes without rerendering component before
    const [localWrittenObservation, setLocalWrittenObservation] = useState(forecastDay.observation);

    // let `forecastDay` prop change update local state
    useEffect(() => {
      setForecastDayFormData(forecastDay);
    }, [forecastDay]);

    // make sure textfield value has up to date observation with state in parent
    useEffect(() => {
      setLocalWrittenObservation(forecastDay.observation);
    }, [forecastDay.observation]);

    // write conditions updates to top level parent state and let `forecastDay` prop change update local state
    const updateConditions = useCallback(
      (time: 'am' | 'pm', data: Condition) => {
        updateAt(day, time, forecastDayFormData, data);
      },
      [day, forecastDayFormData, updateAt],
    );

    // update the local textfield state and make an update using setWrittenObservation to change parent state (with debounce for performance)
    const handleWrittenObservationChange = useCallback<React.ChangeEventHandler<HTMLInputElement>>(
      (e) => {
        setLocalWrittenObservation(e.target.value);
        setWrittenObservation(day, e.target.value);
      },
      [day, setWrittenObservation],
    );

    return (
      <Card className={styles.spotForecastDayCard}>
        <Box className={styles.spotForecastDayContainer}>
          <CardContent>
            <Stack direction="row">
              <Typography component="h3" variant="h3">
                Day {day + 1} - {forecastDay.forecastDay}
              </Typography>
              {!hideApplyNextButton && (
                <Button
                  className={styles.applyNextButton}
                  onClick={() => {
                    applyNext(day + 1, forecastDayFormData);
                  }}
                  data-testid="apply-next"
                >
                  Apply Next
                </Button>
              )}
            </Stack>
            <Divider className={styles.break} />
          </CardContent>
          <Box className={styles.conditionsContainer}>
            <SpotForecastDayConditions
              title="AM"
              conditions={forecastDayFormData.am}
              updateConditions={updateConditions}
            />
            <SpotForecastDayConditions
              title="PM"
              conditions={forecastDayFormData.pm}
              updateConditions={updateConditions}
            />
          </Box>
          <Box my={4} mx={4} sx={{ display: 'none' }}>
            <TextField
              className={styles.humanObservation}
              label="Notes"
              maxLength={200}
              multiline
              rows={3}
              value={localWrittenObservation}
              onChange={handleWrittenObservationChange}
            />
          </Box>
        </Box>
      </Card>
    );
  },
);

export default SpotForecastDay;
