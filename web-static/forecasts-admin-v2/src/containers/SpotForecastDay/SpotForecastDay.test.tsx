import React from 'react';
import { render, screen, fireEvent } from '../../testUtils';
import SpotForecastDay from './SpotForecastDay';
import spotForecastBySubregion from '../../fixtures/spotForecastBySubregion.json';

describe('containers / SpotForecastDay', () => {
  const { spots } = spotForecastBySubregion.data;
  const forecastDay = spots[0].conditions[0];

  it('should render SpotForecastDay card', () => {
    render(
      <SpotForecastDay
        day={0}
        forecastDay={spots[0].conditions[0]}
        updateAt={jest.fn()}
        applyNext={jest.fn()}
        setWrittenObservation={jest.fn()}
      />,
      {},
    );

    expect(screen.getAllByRole('heading')[0]).toHaveTextContent(
      `Day 1 - ${forecastDay.forecastDay}`,
    );
  });

  it('should fire applyNext function onClick apply-next button', () => {
    const applyNext = jest.fn();
    render(
      <SpotForecastDay
        day={0}
        forecastDay={spots[0].conditions[0]}
        updateAt={jest.fn()}
        applyNext={applyNext}
        setWrittenObservation={jest.fn()}
      />,
      {},
    );
    const applyNextButton = screen.getByTestId('apply-next');
    expect(applyNextButton).toBeInTheDocument();
    fireEvent.click(applyNextButton);

    expect(applyNext).toHaveBeenCalledTimes(1);
  });

  it('should fire setWrittenObservation function onChange observation input', () => {
    const setWrittenObservation = jest.fn();
    render(
      <SpotForecastDay
        day={0}
        forecastDay={spots[0].conditions[0]}
        updateAt={jest.fn()}
        applyNext={jest.fn()}
        setWrittenObservation={setWrittenObservation}
      />,
      {},
    );
    const input = screen.getByLabelText('Notes');
    expect(input).toBeInTheDocument();
    fireEvent.change(input, { target: { value: 'test observation' } });

    expect(setWrittenObservation).toHaveBeenCalledTimes(1);
  });

  it('should fire updateAt function on button click inside SpotForecastDayConditions', () => {
    const updateAt = jest.fn();
    render(
      <SpotForecastDay
        day={0}
        forecastDay={spots[0].conditions[0]}
        updateAt={updateAt}
        applyNext={jest.fn()}
        setWrittenObservation={jest.fn()}
      />,
      {},
    );
    const arrowAdjustButton = screen.getAllByRole('button')[1];
    expect(arrowAdjustButton).toBeInTheDocument();
    fireEvent.click(arrowAdjustButton);

    // ensure update function has been called to update state with local form changes
    expect(updateAt).toHaveBeenCalledTimes(1);
  });
});
