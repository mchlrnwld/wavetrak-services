import React from 'react';
import { act, fireEvent, render, screen } from '../../testUtils';
import SpotForecast from './SpotForecast';
import regionOverview from '../../fixtures/regionOverview.json';

describe('containers / SpotForecast', () => {
  const { spots } = regionOverview.data;

  it('should render the Spot Forecasts tabs and forecast toggle switch', () => {
    act(() => {
      render(<SpotForecast subregionId="123" />, {});
    });

    expect(screen.getAllByRole('heading')[0]).toHaveTextContent(spots[0].name);
    expect(screen.getAllByRole('tab')[0]).toHaveTextContent(spots[0].name);
    expect(screen.getByTestId('forecast-status-switch')).toBeInTheDocument();
  });

  it('should call setTab when clicking a tab', () => {
    render(<SpotForecast subregionId="123" />, {});

    const classNameSelected = 'Mui-selected';
    const tabs = screen.getAllByRole('tab');
    expect(tabs[0]).toHaveClass(classNameSelected);
    expect(tabs[1]).not.toHaveClass(classNameSelected);
    fireEvent.click(tabs[1]);
    expect(tabs[0]).not.toHaveClass(classNameSelected);
    expect(tabs[1]).toHaveClass(classNameSelected);
  });

  it('should hide the SpotForecastDays Grid when human forecast is set to inactive', () => {
    render(<SpotForecast subregionId="123" />, {});

    expect(screen.getByRole('checkbox')).toHaveProperty('checked', true);
    expect(screen.getByTestId('forecast-days-grid')).toBeInTheDocument();
    screen.getByRole('checkbox').click();
    fireEvent.change(screen.getByRole('checkbox'), { target: { checked: false } });

    expect(screen.getByRole('checkbox')).toHaveProperty('checked', false);

    // SpotForecastDays grid should be hidden after human forecast is set to inactive
    expect(() => screen.getByTestId('forecast-days-grid')).toThrow();
  });

  it('should update the UI when a SpotForecastDayCondition is changed', () => {
    render(<SpotForecast subregionId="123" />, {});

    const before = screen.getAllByTestId('select-menu-display')[0].textContent;
    const adjustArrow = screen.getAllByTestId('surf-height-adjustment')[0];
    fireEvent.click(adjustArrow);
    const after = screen.getAllByTestId('select-menu-display')[0].textContent;
    expect(before).not.toEqual(after);
  });

  it('should update the UI when an observation field is changed', () => {
    render(<SpotForecast subregionId="123" />, {});

    const val = 'test';

    const input = screen.getAllByLabelText('Notes')[0];
    expect(input).toBeInTheDocument();
    fireEvent.change(input, { target: { value: val } });

    expect(input).toHaveTextContent(val);
  });

  it('should render the info alert when toggled to active', async () => {
    act(() => {
      render(<SpotForecast subregionId="123" />, {});
    });

    // human forecast initially enabled
    const toggleSwitch = screen.getAllByRole('checkbox')[0];
    expect(toggleSwitch).toHaveProperty('checked', true);

    // info alert should be present
    const alert = screen.getByRole('alert');
    expect(alert).toBeInTheDocument();
    expect(alert).toHaveTextContent(/Human forecast enabled for/i);
    expect(alert).toHaveTextContent(
      /The forecast is NOT yet published. You will need to complete all required fields before publishing./i,
    );
    expect(screen.getByTestId('alert-title')).toBeInTheDocument();

    // info alert should be closable
    const closeBtn = screen.getByRole('button', { name: /close/i });
    expect(closeBtn).toBeInTheDocument();
    act(() => {
      fireEvent.click(closeBtn);
    });
    expect(screen.queryByRole('alert')).toBeNull();

    // toggle off
    toggleSwitch.click();
    fireEvent.change(toggleSwitch, { target: { checked: false } });
    expect(toggleSwitch).toHaveProperty('checked', false);
    expect(screen.queryByRole('alert')).toBeNull();
  });

  it('should hide spot forecast content when toggled to inactive', async () => {
    act(() => {
      render(<SpotForecast subregionId="123" />, {});
    });
    const toggleSwitch = screen.getAllByRole('checkbox')[0];

    expect(toggleSwitch).toHaveProperty('checked', true);
    expect(screen.getByTestId('forecast-days-grid')).toBeInTheDocument();

    // toggle off
    toggleSwitch.click();
    fireEvent.change(toggleSwitch, { target: { checked: false } });
    expect(toggleSwitch).toHaveProperty('checked', false);
    expect(screen.queryByTestId('forecast-days-grid')).toBeNull();
  });
});
