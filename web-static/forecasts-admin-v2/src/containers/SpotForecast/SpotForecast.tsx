import React, { useCallback, useState, useEffect } from 'react';
import { Stack } from '@mui/material';
import { useList } from 'react-use';
import SpotNavigation from '../SpotNavigation/SpotNavigation';
import regionOverview from '../../fixtures/regionOverview.json';
import spotForecastBySubregion from '../../fixtures/spotForecastBySubregion.json';
import SpotForecastDays from '../SpotForecastDays';
import Conditions from '../../types/conditions';
import ForecastStatusSwitch from '../../components/ForecastStatusSwitch';
import Alert from '../../components/Alert';

interface Props {
  subregionId: string;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const SpotForecast: React.FC<Props> = ({ subregionId }) => {
  // fetch nearby spots from subregionID
  const { spots } = regionOverview.data;
  const { spots: spotForecasts } = spotForecastBySubregion.data;

  const [tab, setTab] = useState(0);
  const [observations, { updateAt }] = useList(spotForecasts);
  const [infoMessageOpen, setInfoMessageOpen] = useState(observations[tab].human);

  useEffect(() => {
    setInfoMessageOpen(observations[tab].human);
  }, [tab, observations]);

  const updateSpotForecast = useCallback(
    (day: number, newConditions: Conditions) => {
      const tempObservations = observations[tab].conditions;
      tempObservations[day] = newConditions;
      updateAt(tab, { ...observations[tab], conditions: tempObservations });
    },
    [updateAt, observations, tab],
  );

  const updateObservation = useCallback(
    (day: number, obs: string) => {
      const tempObservations = observations[tab].conditions;
      tempObservations[day].observation = obs;
      updateAt(tab, { ...observations[tab], conditions: tempObservations });
    },
    [updateAt, observations, tab],
  );

  return (
    <Stack width={1}>
      <SpotNavigation spots={spots} tab={tab} setTab={(i: number) => setTab(i)} />
      <Stack direction="row" spacing={2} mx={4} mt={4}>
        <ForecastStatusSwitch
          forecastActive={observations[tab].human}
          onForecastStatusToggled={(enabled) => {
            updateAt(tab, { ...observations[tab], human: enabled });
            setInfoMessageOpen(enabled);
          }}
        />

        {infoMessageOpen && (
          <Alert
            onClose={() => setInfoMessageOpen(false)}
            severity="info"
            title={`Human forecast enabled for ${spots[tab].name}`}
            message="The forecast is NOT yet published. You will need to complete all required fields before publishing."
          />
        )}
      </Stack>
      {observations[tab].human && (
        <SpotForecastDays
          name={spots[tab].name}
          spotForecast={observations[tab]}
          updateSpotForecast={updateSpotForecast}
          setWrittenObservations={updateObservation}
        />
      )}
    </Stack>
  );
};

export default SpotForecast;
