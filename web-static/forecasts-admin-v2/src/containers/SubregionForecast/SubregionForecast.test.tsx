import React from 'react';

import { render, screen, fireEvent, waitFor } from '../../testUtils';
import SubregionForecast from './SubregionForecast';
import subregionForecastFixture from '../../fixtures/fetchedForecast.json';

describe('containers / SubregionForecast', () => {
  it('should render the subregion forecast content for active subregions', () => {
    render(
      <SubregionForecast
        subregionForecast={subregionForecastFixture}
        humanForecastActive
        inactiveMessage=""
        onForecastStatusToggled={jest.fn()}
        setInactiveMessage={jest.fn()}
      />,
      {},
    );

    expect(screen.getByText('Forecast Highlights')).toBeInTheDocument();
    expect(screen.getByText('Subregion Observations')).toBeInTheDocument();
    expect(screen.getByRole('alert')).toBeInTheDocument();
  });

  it('should render the subregion forecast with custom inactive message for inactive forecasts', () => {
    render(
      <SubregionForecast
        subregionForecast={subregionForecastFixture}
        humanForecastActive={false}
        inactiveMessage="The forecast is not active right. Check back later."
        onForecastStatusToggled={jest.fn()}
        setInactiveMessage={jest.fn()}
      />,
      {},
    );

    expect(screen.queryByText('Forecast Highlights')).not.toBeInTheDocument();
    expect(screen.queryByText('Subregion Observations')).not.toBeInTheDocument();
    expect(screen.queryByText('Inactive Message')).toBeInTheDocument();
    const inputBox = screen.getByRole('textbox', { name: /inactive message/i });
    expect(inputBox).toHaveValue('The forecast is not active right. Check back later.');
  });

  it('should hide forecast content when toggled to inactive', async () => {
    const onForecastStatusToggled = jest.fn();
    render(
      <SubregionForecast
        subregionForecast={subregionForecastFixture}
        humanForecastActive
        inactiveMessage=""
        onForecastStatusToggled={onForecastStatusToggled}
        setInactiveMessage={jest.fn()}
      />,
      {},
    );
    expect(screen.getByText('Forecast Highlights')).toBeInTheDocument();
    expect(screen.getByText('Subregion Observations')).toBeInTheDocument();
    expect(screen.getByRole('alert')).toBeInTheDocument();
    expect(screen.queryByText('Inactive Message')).not.toBeInTheDocument();

    const toggleSwitch = screen.getAllByRole('checkbox')[0];
    toggleSwitch.click();
    fireEvent.change(toggleSwitch, { target: { checked: false } });

    expect(screen.getAllByRole('checkbox')[0]).toHaveProperty('checked', false);
    expect(onForecastStatusToggled).toHaveBeenCalledTimes(1);
    expect(onForecastStatusToggled).toHaveBeenLastCalledWith(false);
  });

  it('should remove info message when closed', async () => {
    const onForecastStatusToggled = jest.fn();
    render(
      <SubregionForecast
        subregionForecast={subregionForecastFixture}
        humanForecastActive
        inactiveMessage=""
        onForecastStatusToggled={onForecastStatusToggled}
        setInactiveMessage={jest.fn()}
      />,
      {},
    );

    expect(screen.getByRole('alert')).toBeInTheDocument();
    screen.getByRole('button', { name: /close/i }).click();
    await waitFor(() => expect(screen.queryByRole('alert')).toBeNull());
  });

  it('should fire setInactiveMessage handler when textbox value is changed.', async () => {
    const onInactiveMessageChanged = jest.fn();
    render(
      <SubregionForecast
        subregionForecast={subregionForecastFixture}
        humanForecastActive={false}
        inactiveMessage=""
        onForecastStatusToggled={jest.fn()}
        setInactiveMessage={onInactiveMessageChanged}
      />,
      {},
    );

    const inactiveMessageValid = 'Forecast is off for the season.';
    const inputEl = screen.getByRole('textbox', { name: /inactive message/i });
    fireEvent.change(inputEl, { target: { value: inactiveMessageValid } });
    expect(inputEl).toHaveProperty('maxLength', 150);
    expect(onInactiveMessageChanged).toHaveBeenCalledTimes(1);
    expect(onInactiveMessageChanged).toHaveBeenLastCalledWith(inactiveMessageValid);
  });
});
