import { Grid, Collapse } from '@mui/material';
import React, { useEffect, useState } from 'react';
import SubregionForecastAnalysis from '../SubregionForecastAnalysis';
import SubregionForecastObservations from '../SubregionForecastObservations';
import Alert from '../../components/Alert';
import ForecastStatusSwitch from '../../components/ForecastStatusSwitch';
import TextField from '../../components/TextField';
import styles from './SubregionForecast.module.scss';

interface Props {
  onForecastStatusToggled(arg: boolean): void;
  humanForecastActive: boolean;
  inactiveMessage: string;
  setInactiveMessage(arg: string): void;
  subregionForecast;
}
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const SubregionForecast: React.FC<Props> = ({
  humanForecastActive,
  onForecastStatusToggled,
  inactiveMessage,
  setInactiveMessage,
  subregionForecast,
}) => {
  const [infoMessageOpen, setInfoMessageOpen] = useState(humanForecastActive);
  const onForecastStatusToggledLocal = (enabled) => {
    setInfoMessageOpen(enabled);
    onForecastStatusToggled(enabled);
  };

  useEffect(() => {
    if (humanForecastActive) {
      setInfoMessageOpen(true);
    }
  }, [humanForecastActive]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={3}>
        <ForecastStatusSwitch
          forecastActive={humanForecastActive}
          onForecastStatusToggled={onForecastStatusToggledLocal}
        />
      </Grid>
      <Grid item xs={12} md={9}>
        {humanForecastActive && (
          <Collapse in={infoMessageOpen}>
            <Alert
              severity="info"
              onClose={() => setInfoMessageOpen(false)}
              title="Important!"
              message="The forecast is NOT yet published. You will need to complete all required fields before publishing."
            />
          </Collapse>
        )}
      </Grid>
      {humanForecastActive ? (
        <>
          <Grid item xs={12} md={6}>
            <SubregionForecastAnalysis
              notesType="Forecast Highlights"
              fetchedNotes={subregionForecast.highlights}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <SubregionForecastAnalysis
              notesType="Best Bet"
              fetchedNotes={subregionForecast.bestBets}
            />
          </Grid>

          <Grid item xs={12}>
            <SubregionForecastObservations subregionForecastDays={subregionForecast.observations} />
          </Grid>
        </>
      ) : (
        <Grid item xs={12} md={6}>
          <TextField
            className={styles.inactiveMessage}
            value={inactiveMessage}
            maxLength={150}
            rows={3}
            label="Inactive Message"
            onChange={(e) => setInactiveMessage(e.target.value)}
            multiline
          />
        </Grid>
      )}
    </Grid>
  );
};

export default SubregionForecast;
