import React from 'react';
import { render, screen, fireEvent } from '../../testUtils';
import SubregionForecastObservation from './SubregionForecastObservation';

describe('containers / SubregionForecastObservation', () => {
  const applyNext = jest.fn();
  const props = {
    day: 1,
    forecastDate: '2021-12-08',
    forecastObservation:
      'Sluggish with the AM high tide but will improve into midday as tide drops. Fresh NW swell on tap along with some reinforcing SSW swell.',
  };

  it('should render the subregion forecast heading', () => {
    render(
      <SubregionForecastObservation
        day={props.day}
        forecastDate={props.forecastDate}
        forecastObservation={props.forecastObservation}
        applyNext={applyNext}
      />,
      {},
    );

    expect(screen.getByRole('heading')).toHaveTextContent(
      `Day ${props.day} - ${props.forecastDate}`,
    );
  });

  it('should render the observation input', () => {
    render(
      <SubregionForecastObservation
        day={props.day}
        forecastDate={props.forecastDate}
        forecastObservation={props.forecastObservation}
        applyNext={applyNext}
      />,
      {},
    );
    const input = screen.getByRole('textbox');

    expect(input).toBeInTheDocument();

    expect(input).toHaveValue(props.forecastObservation);

    fireEvent.change(input, {
      target: {
        value:
          'Increasing potential for a clean up session as NW swell mix eases and modest SSW swell blends in. Wind stays light into the afternoon.',
      },
    });

    expect(input).toHaveValue(
      'Increasing potential for a clean up session as NW swell mix eases and modest SSW swell blends in. Wind stays light into the afternoon.',
    );
  });

  it('should render apply next button', () => {
    render(
      <SubregionForecastObservation
        day={props.day}
        forecastDate={props.forecastDate}
        forecastObservation={props.forecastObservation}
        applyNext={applyNext}
      />,
      {},
    );

    const btn = screen.getByRole('button');

    expect(btn).toHaveTextContent('Apply Next');
  });
});
