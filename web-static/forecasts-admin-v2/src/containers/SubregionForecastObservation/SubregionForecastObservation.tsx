import { Button, Card, Divider, Stack, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import TextField from '../../components/TextField';
import styles from './SubregionForecastObservation.module.scss';

interface Props {
  day: number;
  forecastDate: string;
  forecastObservation?: string;
  observationCharacterLimit?: number;
  rows?: number;
  applyNext: (day: number, observation: string) => void;
  hideApplyNextButton?: boolean;
}

const SubregionForecastObservation: React.FC<Props> = ({
  day,
  forecastDate,
  forecastObservation = '',
  observationCharacterLimit = 240,
  rows = 5,
  applyNext,
  hideApplyNextButton = false,
}) => {
  const [observation, setObservation] = useState(forecastObservation);

  useEffect(() => {
    setObservation(forecastObservation);
  }, [forecastObservation]);

  return (
    <Card className={styles.observationCard} data-testid={`observation-${day}`}>
      <Stack direction="row">
        <Typography variant="h6" component="h6">
          Day {day} - {forecastDate}
        </Typography>
        {!hideApplyNextButton && (
          <Button
            className={styles.applyNextButton}
            onClick={() => {
              applyNext(day, observation);
            }}
          >
            Apply Next
          </Button>
        )}
      </Stack>
      <Divider className={styles.break} />
      <TextField
        className={styles.observation}
        maxLength={observationCharacterLimit}
        value={observation}
        id={`${day} Observation`}
        multiline
        rows={rows}
        onChange={(e) => {
          setObservation(e.target.value);
        }}
      />
    </Card>
  );
};

SubregionForecastObservation.defaultProps = {
  forecastObservation: '',
  observationCharacterLimit: 240,
  rows: 5,
  hideApplyNextButton: false,
};

export default SubregionForecastObservation;
