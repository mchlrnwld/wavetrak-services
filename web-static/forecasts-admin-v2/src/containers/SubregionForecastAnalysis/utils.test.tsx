import validateInput from './utils';

describe('containers / SubregionForecastAnalysis / utils', () => {
  describe('validateInput', () => {
    const notes = [
      {
        note: 'Best Surf in the Next 7 Days: Small but clean Mon AM, watching mid next week', // length = 76
        uuid: 'uuid-1',
      },
      {
        note: 'Stronger run of NW swell mix this week, lingering small SSW swell', // length = 65
        uuid: 'uuid-2',
      },
      {
        note: 'Better wind second half of the week with fun swell in the water', // length = 63
        uuid: 'uuid-3',
      },
    ];

    it('should return true for valid notes array', () => {
      const isValid = validateInput(notes, 86);
      expect(isValid).toBeTruthy();
    });

    it('should return false for notes array with an entry that exceeds the character limit', () => {
      const isValid = validateInput(notes, 75);
      expect(isValid).toBeFalsy();
    });

    it('should return true for notes array that has only has 1 note', () => {
      const singleNote = [notes[0]];
      const isValid = validateInput(singleNote, 86);
      expect(isValid).toBeTruthy();
    });
  });
});
