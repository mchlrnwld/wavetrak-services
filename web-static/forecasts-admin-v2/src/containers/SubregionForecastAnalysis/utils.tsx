// Ensure that Array of notes that is passed in contains at least one entry between 0 and `noteMaxLength` characters
const validateInput = (
  notes: Array<{ note: string; uuid: string }>,
  noteMaxLength: number = 86,
) => {
  if (notes.some((entry) => entry.note?.length > noteMaxLength)) {
    return false;
  }
  return (
    notes.filter((entry) => entry.note?.length > 0 && entry.note?.length <= noteMaxLength).length >
    0
  );
};

export default validateInput;
