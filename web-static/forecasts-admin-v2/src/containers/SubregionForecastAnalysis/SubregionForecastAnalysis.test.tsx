import React from 'react';

import { render, screen, fireEvent } from '../../testUtils';
import SubregionForecastAnalysis from './SubregionForecastAnalysis';

import subregionForecast from '../../fixtures/fetchedForecast.json';

describe('containers / SubregionForecastAnalysis', () => {
  const props = {
    notesType: 'Best Bet',
    fetchedNotes: subregionForecast.bestBets,
  };
  it('should render the subregion forecast heading', () => {
    render(
      <SubregionForecastAnalysis notesType={props.notesType} fetchedNotes={props.fetchedNotes} />,
      {},
    );

    expect(screen.getByRole('heading', { level: 5 })).toHaveTextContent('Best Bet');

    expect(screen.getByRole('columnheader')).toHaveTextContent('Best Bet');
    expect(screen.getAllByRole('row')[0]).toHaveTextContent('Best Bet');
  });

  it('should render the 3 input textboxes for notes', () => {
    render(
      <SubregionForecastAnalysis notesType="Best Bet" fetchedNotes={subregionForecast.bestBets} />,
      {},
    );

    const notes = screen.getAllByRole('textbox');

    // there should be 3 text inputs for 3 separate notes
    expect(notes).toHaveLength(3);
  });

  it('should render the observation input with empy string if no fetchedForecast is passed in', () => {
    render(<SubregionForecastAnalysis notesType={props.notesType} />, {});

    const notes = screen.getAllByRole('textbox');

    expect(notes[0]).toHaveTextContent('');
  });

  it('should only accept noteCharacterLimit characters of input', () => {
    const noteCharacterLimit = 100;
    render(
      <SubregionForecastAnalysis
        notesType={props.notesType}
        fetchedNotes={props.fetchedNotes}
        noteCharacterLimit={noteCharacterLimit}
      />,
      {},
    );
    const notes = screen.getAllByRole('textbox');
    const firstInput = notes[0];

    expect(firstInput).toHaveProperty('maxLength', noteCharacterLimit);

    fireEvent.change(firstInput, {
      target: {
        value:
          'This string is greater than 100 characters and should not fit in the text box. If we change the input to use this value, it should reject the change',
      },
    });

    // should fail to programatically add input > maxLength property
    expect(firstInput).toHaveValue(props.fetchedNotes[0]);

    fireEvent.change(firstInput, {
      target: {
        value: 'This string is less than 100 characters.',
      },
    });

    // should succeed when passing in value that follows maxLength property limit
    expect(firstInput).toHaveValue('This string is less than 100 characters.');
  });
});
