import React, { useState } from 'react';
import {
  InputAdornment,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Paper,
  Typography,
} from '@mui/material';

import { v4 as uuidv4 } from 'uuid';
import styles from './SubregionForecastAnalysis.module.scss';

interface Props {
  notesType: string;
  fetchedNotes?: Array<string>;
  noteLines?: number;
  noteCharacterLimit?: number;
}

const SubregionForecastAnalysis: React.FC<Props> = ({
  notesType,
  fetchedNotes = null,
  noteLines = 3,
  noteCharacterLimit = 86,
}) => {
  // extend initial state of notes array length to equal `noteLines`
  // check if fetchedNotes exists before getting note from index
  const fitFetchedNotes = () =>
    Array(noteLines)
      .fill('')
      .map((_, i) => ({ note: (fetchedNotes && fetchedNotes[i]) || '', uuid: uuidv4() }));

  const [notes, setNotes] = useState(fitFetchedNotes());

  const handleChange = (note, index) => {
    if (note.length <= noteCharacterLimit) {
      const tempNotes = [...notes];
      tempNotes[index] = { ...tempNotes[index], note };
      setNotes(tempNotes);
    }
  };

  return (
    <Paper>
      <TableContainer className={styles.tableContainer}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell className={styles.tableTitle} colSpan={2}>
                <Typography variant="h5">{notesType}</Typography>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {notes.map(({ note, uuid }, i) => (
              <TableRow key={uuid}>
                <TableCell className={styles.characterCount} scope="row">
                  {note.length} / {noteCharacterLimit}
                </TableCell>
                <TableCell align="left">
                  <TextField
                    className={styles.noteInput}
                    variant="standard"
                    value={note}
                    onChange={(e) => {
                      handleChange(e.target.value, i);
                    }}
                    InputProps={{
                      startAdornment: <InputAdornment position="start">-</InputAdornment>,
                      inputProps: { maxLength: noteCharacterLimit },
                    }}
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};

SubregionForecastAnalysis.defaultProps = {
  fetchedNotes: null,
  noteLines: 3,
  noteCharacterLimit: 86,
};

export default SubregionForecastAnalysis;
