import React from 'react';
import { render, screen } from '../../testUtils';
import SpotForecastDays from './SpotForecastDays';
import spotForecastBySubregion from '../../fixtures/spotForecastBySubregion.json';

describe('containers / SpotForecastDays', () => {
  const { spots } = spotForecastBySubregion.data;

  it('should render the spot forecast heading', () => {
    render(
      <SpotForecastDays
        name={spots[0].name}
        spotForecast={spots[0]}
        updateSpotForecast={jest.fn()}
        setWrittenObservations={jest.fn()}
      />,
      {},
    );

    expect(screen.getByTestId('spot-forecast-title')).toHaveTextContent(spots[0].name);
  });
});
