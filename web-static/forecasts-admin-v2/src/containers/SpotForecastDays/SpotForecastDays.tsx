import React, { useCallback, useEffect } from 'react';
import { Box, Grid, Typography } from '@mui/material';
import { useList } from 'react-use';
import _debounce from 'lodash/debounce';
import SpotForecastDay from '../SpotForecastDay';
import Condition from '../../types/condition';
import SpotForecast from '../../types/spotForecast';
import Conditions from '../../types/conditions';

interface Data {
  am: Condition;
  pm: Condition;
  observation: string;
}

interface Props {
  name: string;
  spotForecast: SpotForecast;
  updateSpotForecast: (day: number, newConditions: Conditions) => void;
  setWrittenObservations: (day: number, obs: string) => void;
}

const SpotForecastDays: React.FC<Props> = ({
  name,
  spotForecast,
  updateSpotForecast,
  setWrittenObservations,
}) => {
  const [spotForecastForm, { set }] = useList<Conditions>(spotForecast.conditions);

  useEffect(() => {
    set(spotForecast.conditions);
  }, [spotForecast.human, spotForecast.conditions, set]);

  const applyNext = useCallback(
    (day: number, data: Data) => {
      updateSpotForecast(day, {
        ...spotForecastForm[day],
        am: data.am,
        pm: data.pm,
        observation: data.observation,
      });
    },
    [updateSpotForecast, spotForecastForm],
  );

  const updateConditions = useCallback(
    (day: number, time: 'am' | 'pm', forecastDayFormData: Conditions, data: Condition) => {
      updateSpotForecast(day, { ...forecastDayFormData, [time]: data });
    },
    [updateSpotForecast],
  );

  // disabling this warning and including `setWrittenObservations` in deps to capture tab changes and update function accordingly.
  // _debounce does not allow TS to evalute function dependencies
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const updateWrittenObservation = useCallback(_debounce(setWrittenObservations, 500), [
    setWrittenObservations,
  ]);

  return (
    <Box mx={4} my={4}>
      <Typography variant="h5" data-testid="spot-forecast-title">
        {name}
      </Typography>

      {spotForecastForm && (
        <Grid container spacing={2} data-testid="forecast-days-grid">
          {spotForecastForm.map((forecastDay, forecastDayIndex) => (
            <Grid item sm={12} md={12} lg={6} my={2} key={`${name}-${forecastDay.forecastDay}`}>
              <SpotForecastDay
                day={forecastDayIndex}
                forecastDay={forecastDay}
                hideApplyNextButton={spotForecastForm.length === forecastDayIndex + 1}
                updateAt={updateConditions}
                applyNext={applyNext}
                setWrittenObservation={updateWrittenObservation}
              />
            </Grid>
          ))}
        </Grid>
      )}
    </Box>
  );
};

export default SpotForecastDays;
