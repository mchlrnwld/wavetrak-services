import React from 'react';

import { act, render, screen, fireEvent, waitFor } from '../../testUtils';
import Forecast from './Forecast';
import * as fetchSubregionForecastHook from '../../hooks/useFetchSubregionForecast';
import subregionForecastFixture from '../../fixtures/fetchedForecast.json';

describe('containers / Forecasts', () => {
  it('should render the Subregion and Spot Forecast headers', () => {
    jest.spyOn(fetchSubregionForecastHook, 'default').mockImplementation(() => ({
      subregionForecast: subregionForecastFixture,
      isLoading: false,
      errorMessage: '',
    }));
    act(() => {
      render(<Forecast subregionId="123" />, {});
    });
    expect(screen.getAllByRole('heading', { level: 1 })[0]).toHaveTextContent(
      'North Orange County Forecast',
    );
    expect(screen.getAllByRole('heading', { level: 1 })[1]).toHaveTextContent('Spot Forecasts');
    expect(screen.getAllByRole('checkbox')[0]).toHaveProperty('checked', true);
    expect(screen.queryByRole('progressbar')).toBeNull();
  });

  it('should show inactive message textbox when toggled to inactive', () => {
    jest.spyOn(fetchSubregionForecastHook, 'default').mockImplementation(() => ({
      subregionForecast: subregionForecastFixture,
      isLoading: false,
      errorMessage: '',
    }));

    act(() => {
      render(<Forecast subregionId="123" />, {});
    });
    expect(screen.getAllByRole('heading', { level: 1 })[1]).toHaveTextContent('Spot Forecasts');
    expect(screen.queryByRole('textbox', { name: /inactive message/i })).toBeNull();
    expect(screen.getAllByRole('checkbox')[0]).toHaveProperty('checked', true);
    screen.getAllByRole('checkbox')[0].click();
    fireEvent.change(screen.getAllByRole('checkbox')[0], { target: { checked: false } });
    expect(screen.getByRole('textbox', { name: /inactive message/i })).toBeInTheDocument();
  });

  it('should only accept inactive message with valid length', async () => {
    jest.spyOn(fetchSubregionForecastHook, 'default').mockImplementation(() => ({
      subregionForecast: subregionForecastFixture,
      isLoading: false,
      errorMessage: '',
    }));

    act(() => {
      render(<Forecast subregionId="123" />, {});
    });

    const inactiveMessageValid = 'Forecast is off for the season.';
    const inactiveMessageTooLong =
      '0123459789012345978901234597890123459789012345978901234597890123459789012345978901234597890123459789012345978901234597890123459789012345978901234597890123459789';

    // toggle to inactive (default fixture is active)
    screen.getAllByRole('checkbox')[0].click();
    fireEvent.change(screen.getAllByRole('checkbox')[0], { target: { checked: false } });
    expect(screen.getByRole('textbox', { name: /inactive message/i })).toBeInTheDocument();

    const inputEl = screen.getByRole('textbox', { name: /inactive message/i });
    fireEvent.change(inputEl, { target: { value: inactiveMessageValid } });
    expect(inputEl).toHaveProperty('maxLength', 150);
    expect(inputEl).toHaveValue(inactiveMessageValid);

    // try to set with value that is too long and it should still equal previous value
    fireEvent.change(inputEl, { target: { value: inactiveMessageTooLong } });
    expect(inputEl).toHaveValue(inactiveMessageValid);
  });

  it('should render a loading bar', async () => {
    jest.spyOn(fetchSubregionForecastHook, 'default').mockImplementation(() => ({
      subregionForecast: null,
      isLoading: true,
      errorMessage: '',
    }));

    act(() => {
      render(<Forecast subregionId="123" />, {});
    });
    await waitFor(() => expect(fetchSubregionForecastHook.default).toHaveBeenCalled());
    await waitFor(() => screen.getByTestId('subregion-forecast-progressbar'));

    expect(screen.getByRole('progressbar')).toBeInTheDocument();
    expect(screen.queryByRole('alert')).toBeNull();
  });

  it('should render an error', async () => {
    jest.spyOn(fetchSubregionForecastHook, 'default').mockImplementation(() => ({
      subregionForecast: null,
      isLoading: false,
      errorMessage: 'Something went wrong',
    }));

    act(() => {
      render(<Forecast subregionId="123" />, {});
    });
    await waitFor(() => expect(fetchSubregionForecastHook.default).toHaveBeenCalled());
    expect(screen.getByRole('alert')).toBeInTheDocument();
    expect(screen.queryByRole('progressbar')).toBeNull();
    expect(screen.getByText('Something went wrong')).toBeInTheDocument();
  });
});
