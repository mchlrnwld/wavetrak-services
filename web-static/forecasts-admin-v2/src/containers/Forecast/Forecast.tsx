import React, { useState, useEffect } from 'react';
import { Container, Box, Typography, LinearProgress } from '@mui/material';
import SubregionForecast from '../SubregionForecast';
import SpotForecast from '../SpotForecast';
import Alert from '../../components/Alert';
import styles from './Forecast.module.scss';
import useFetchSubregionForecast from '../../hooks/useFetchSubregionForecast';

interface Props {
  subregionId: string;
}
const Forecast: React.FC<Props> = ({ subregionId }) => {
  const { subregionForecast, isLoading, errorMessage } = useFetchSubregionForecast(subregionId);
  const [subregionForecastActive, setSubregionForecastActive] = useState(false);
  const [inactiveMessage, setInactiveMessage] = useState<string>('');

  useEffect(() => {
    if (subregionForecast?.forecastStatus) {
      setSubregionForecastActive(subregionForecast.forecastStatus.status === 'active');
      setInactiveMessage(subregionForecast.forecastStatus.inactiveMessage);
    }
  }, [subregionForecast?.forecastStatus]);

  const onForecastStatusToggled = (checked: boolean) => {
    setSubregionForecastActive(checked);
  };

  return (
    <Container maxWidth="xl" data-testid="forecast-container" className={styles.container}>
      <Typography variant="h4" component="h1">
        {subregionForecast?.subregionName || 'Subregion'} Forecast
      </Typography>

      {isLoading && (
        <Box sx={{ my: 4, display: 'flex', flexDirection: 'row', width: 1 }}>
          <LinearProgress data-testid="subregion-forecast-progressbar" className={styles.loading} />
        </Box>
      )}

      {errorMessage && (
        <Box sx={{ my: 4, display: 'flex', flexDirection: 'row', width: 1 }}>
          <Alert
            severity="error"
            className={styles.error}
            message={errorMessage}
            data-testid="subregion-forecast-loaderror"
          />
        </Box>
      )}

      {!isLoading && !errorMessage && (
        <>
          <Box sx={{ my: 4, display: 'flex', flexDirection: 'row', width: 1 }}>
            <SubregionForecast
              subregionForecast={subregionForecast}
              humanForecastActive={subregionForecastActive}
              inactiveMessage={inactiveMessage}
              onForecastStatusToggled={onForecastStatusToggled}
              setInactiveMessage={setInactiveMessage}
            />
          </Box>

          {subregionForecastActive && (
            <>
              <Typography variant="h4" component="h1">
                Spot Forecasts
              </Typography>
              <Box sx={{ my: 4, display: 'flex', flexDirection: 'row', width: 1 }}>
                <SpotForecast subregionId={subregionId} />
              </Box>
            </>
          )}
        </>
      )}
    </Container>
  );
};

export default Forecast;
