import React from 'react';
import { render, screen, fireEvent } from '../../testUtils';
import SpotForecastDayConditions from './SpotForecastDayConditions';

describe('containers / SpotForecastDayConditions', () => {
  const lowCondition = {
    waveHeight: {
      max: 0.5,
      min: 0,
      plus: false,
      humanRelation: 'Flat',
      occasionalHeight: null,
    },
    rating: 'POOR',
  };

  const highConditions = {
    waveHeight: {
      max: 50,
      min: 35,
      plus: false,
      humanRelation: '7-10 times overhead',
      occasionalHeight: null,
    },
    rating: 'FAIR',
  };

  it('Should render a spot forecast conditions card', () => {
    render(
      <SpotForecastDayConditions
        title="AM"
        conditions={lowCondition}
        updateConditions={jest.fn()}
      />,
      {},
    );

    const title = screen.getByTestId('conditions-card-title');
    expect(title).toBeInTheDocument();
    expect(title).toHaveTextContent('AM');
  });

  it('Should call updateConditions function on decrease button click', () => {
    const updateConditions = jest.fn();
    render(
      <SpotForecastDayConditions
        title="AM"
        conditions={lowCondition}
        updateConditions={updateConditions}
      />,
      {},
    );
    const adjustArrow = screen.getAllByTestId('surf-height-adjustment')[0];

    // decrease once on low end to get to index 0 of menu list
    fireEvent.click(adjustArrow);
    expect(updateConditions).toHaveBeenCalledTimes(1);
    expect(screen.getAllByTestId('select-menu-input')[0]).toHaveValue('0');

    // check that another decrease returns without changing input at lowest value
    fireEvent.click(adjustArrow);
    expect(updateConditions).toHaveBeenCalledTimes(1);
    expect(screen.getAllByTestId('select-menu-input')[0]).toHaveValue('0');
  });

  it('Should call updateConditions function on increase button click', () => {
    const updateConditions = jest.fn();
    render(
      <SpotForecastDayConditions
        title="AM"
        conditions={highConditions}
        updateConditions={updateConditions}
      />,
      {},
    );
    const adjustArrow = screen.getAllByTestId('surf-height-adjustment')[1];

    fireEvent.click(adjustArrow);
    expect(updateConditions).toHaveBeenCalledTimes(1);
    expect(screen.getAllByTestId('select-menu-input')[0]).toHaveValue('29');

    fireEvent.click(adjustArrow);
    expect(updateConditions).toHaveBeenCalledTimes(1);
    expect(screen.getAllByTestId('select-menu-input')[0]).toHaveValue('29');
  });

  it('Should call updateConditions function on decrease rating button click', () => {
    const updateConditions = jest.fn();
    render(
      <SpotForecastDayConditions
        title="AM"
        conditions={lowCondition}
        updateConditions={updateConditions}
      />,
      {},
    );
    const adjustArrow = screen.getAllByTestId('ratings-adjustment')[0];

    // decrease once on low end to get to index 0 of menu list
    fireEvent.click(adjustArrow);
    expect(updateConditions).toHaveBeenCalledTimes(1);

    // check that another decrease returns without changing input at lowest value
    fireEvent.click(adjustArrow);
    expect(updateConditions).toHaveBeenCalledTimes(2);
  });

  it('Should call updateConditions function on increase rating button click', () => {
    const updateConditions = jest.fn();
    render(
      <SpotForecastDayConditions
        title="AM"
        conditions={highConditions}
        updateConditions={updateConditions}
      />,
      {},
    );
    const adjustArrow = screen.getAllByTestId('ratings-adjustment')[1];

    // decrease once on low end to get to index 0 of menu list
    fireEvent.click(adjustArrow);
    expect(updateConditions).toHaveBeenCalledTimes(1);

    // check that another decrease returns without changing input at lowest value
    fireEvent.click(adjustArrow);
    expect(updateConditions).toHaveBeenCalledTimes(2);
  });
});
