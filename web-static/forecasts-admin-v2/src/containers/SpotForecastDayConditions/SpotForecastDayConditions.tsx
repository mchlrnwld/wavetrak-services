import React, { useCallback, useEffect, useState } from 'react';
import { Box, Card, CardContent, IconButton, Stack, Typography } from '@mui/material';
import { KeyboardArrowDown, KeyboardArrowUp } from '@mui/icons-material';
import styles from './SpotForecastDayConditions.module.scss';
import COLOR_CONDITIONS from '../../constants/colorConditions';
import SelectMenu from '../../components/SelectMenu';
import Condition from '../../types/condition';
import surfConditions from '../../fixtures/surfConditions';
import useConditionsForm from '../../hooks/useConditionsForm';

interface Props {
  title: 'AM' | 'PM';
  conditions: Condition;
  updateConditions(time: 'am' | 'pm', data: Condition): void;
}

const SpotForecastDayConditions = React.memo<Props>(({ title, conditions, updateConditions }) => {
  // find key in surfConditions given current min and max height (and plus) from conditions
  const findSurfHeight = useCallback(
    () =>
      parseInt(
        Object.keys(surfConditions).filter((key) => {
          if (
            surfConditions[key].minHeight === conditions.waveHeight.min &&
            surfConditions[key].maxHeight === conditions.waveHeight.max
          ) {
            if (conditions.waveHeight.plus && surfConditions[key].condition.includes('ft +')) {
              return key;
            }
            if (!conditions.waveHeight.plus) {
              return key;
            }
          }
          return false;
        })[0],
        10,
      ),
    [conditions.waveHeight.max, conditions.waveHeight.min, conditions.waveHeight.plus],
  );

  const [surfHeight, setSurfHeight] = useState<number>(findSurfHeight);

  useEffect(() => {
    setSurfHeight(findSurfHeight);
  }, [findSurfHeight]);

  const {
    setLocalConditions,
    functions: { updateHeight, updateRating },
    handlers: { handleHeightChange, handleRatingChange },
    values: { ratings, surfHeightsMenuItems, localConditions },
  } = useConditionsForm({ title, conditions, surfHeight, setSurfHeight, updateConditions });

  useEffect(() => {
    setLocalConditions(conditions);
  }, [setLocalConditions, conditions]); // , findSurfHeight]);

  const formatSelectedHeight = useCallback(
    (selected: string): string => surfConditions[selected].condition.split(' – ')[0],
    [],
  );

  const formatHeightMenuItems = useCallback(
    (selected: string): string => surfConditions[selected].condition,
    [],
  );

  const formatRatings = (selected: string): string => selected.split('_').join(' ');

  return (
    <Card
      className={styles.conditionsCard}
      sx={{ border: `4px solid ${COLOR_CONDITIONS[localConditions.rating]}` }}
    >
      <Box className={styles.conditionsBox}>
        <CardContent>
          <Typography
            variant="h4"
            data-testid="conditions-card-title"
            className={styles.conditionsTitle}
          >
            {title}
          </Typography>
          <Stack direction="row" className={styles.stack}>
            <IconButton onClick={updateHeight('dec')} data-testid="surf-height-adjustment">
              <KeyboardArrowDown />
            </IconButton>
            <SelectMenu
              value={surfHeight}
              menuItemsList={surfHeightsMenuItems}
              className={styles.select}
              onChange={handleHeightChange}
              formatSelectedValue={formatSelectedHeight}
              formatMenuItem={formatHeightMenuItems}
            />
            <IconButton onClick={updateHeight('inc')} data-testid="surf-height-adjustment">
              <KeyboardArrowUp />
            </IconButton>
          </Stack>
          <Box className={styles.humanRelation}>
            <Typography variant="h4">{surfConditions[surfHeight].humanRelation}</Typography>
          </Box>
          <Stack direction="row" className={styles.stack}>
            <IconButton onClick={updateRating('dec')} data-testid="ratings-adjustment">
              <KeyboardArrowDown />
            </IconButton>
            <SelectMenu
              value={localConditions.rating}
              menuItemsList={ratings}
              className={styles.select}
              onChange={handleRatingChange}
              formatSelectedValue={formatRatings}
              formatMenuItem={formatRatings}
            />
            <IconButton onClick={updateRating('inc')} data-testid="ratings-adjustment">
              <KeyboardArrowUp />
            </IconButton>
          </Stack>
        </CardContent>
      </Box>
    </Card>
  );
});

export default SpotForecastDayConditions;
