import React from 'react';

import { RouterContext } from 'next/dist/shared/lib/router-context';
import { render, screen, act, within, waitFor, fireEvent } from '../../testUtils';
import SearchSubregions from './SearchSubregions';
import subregionsFixture from '../../fixtures/subregionsFixture.json';
import * as hooks from '../../hooks/useFetchSubregions';
import createMockRouter from '../../test-utils/createMockRouter';

const mockSubregionsFixture = subregionsFixture;

describe('containers / SearchSubregions', () => {
  it('should render the subregion search container with search input', async () => {
    act(() => {
      render(<SearchSubregions />, {});
    });
    expect(screen.getByTestId('subregion-search-container')).toBeInTheDocument();
    expect(screen.getByLabelText('Search by subregion')).toBeInTheDocument(); // search input
    expect(screen.getByText('SEARCH')).toBeInTheDocument(); // search button

    // no search results until search complete
    expect(screen.queryByTestId('search-results-component')).toBeNull();
  });

  it('should render the search results with valid input', async () => {
    jest.spyOn(hooks, 'default').mockImplementation(() => ({
      subregions: mockSubregionsFixture,
      isLoading: false,
      errorMessage: '',
    }));

    act(() => {
      render(<SearchSubregions />, {});
    });
    await waitFor(() => expect(hooks.default).toHaveBeenCalled());
    await waitFor(() => screen.getByTestId('search-results-component'));
    const list = screen.getByRole('list');
    const listItems = within(list).getAllByRole('listitem');
    expect(listItems).toHaveLength(4);

    const subWesternAus = screen.queryAllByText('Western Australia');
    const subSouthwestAus = screen.queryAllByText('Southwest Australia');
    const subSoutheastAus = screen.queryAllByText('Southeast Australia');
    const subNorthAus = screen.queryAllByText('Australia North Coast');
    expect(subWesternAus).toHaveLength(1);
    expect(subSouthwestAus).toHaveLength(1);
    expect(subSoutheastAus).toHaveLength(1);
    expect(subNorthAus).toHaveLength(1);

    const editLinks = screen.queryAllByText('EDIT');
    expect(editLinks).toHaveLength(4);
  });

  it('should not render the search results with invalid input and should show snackbar', async () => {
    jest.spyOn(hooks, 'default').mockImplementation(() => ({
      subregions: [],
      isLoading: false,
      errorMessage: '',
    }));

    act(() => {
      render(<SearchSubregions />, {});
    });

    await waitFor(() => expect(hooks.default).toHaveBeenCalled());

    const inputEl = screen.getByRole('searchbox', { name: /search by subregion/i });
    fireEvent.change(inputEl, { target: { value: 'Australia**' } });
    expect(screen.getByTestId('subregion-search-invalidinput')).toBeInTheDocument();

    const alert = screen.getByRole('alert');
    const alertTitle = screen.getByTestId('alert-title');
    expect(alert).toBeInTheDocument();
    expect(alertTitle).toBeInTheDocument();
    expect(alertTitle).toHaveTextContent(/invalid subregion name/i);

    // alert should be closable
    const closeBtn = screen.getByRole('button', { name: /close/i });
    expect(closeBtn).toBeInTheDocument();
    act(() => {
      fireEvent.click(closeBtn);
    });
    await waitFor(() => expect(screen.queryByRole('alert')).toBeNull());

    const listItems = screen.queryAllByRole('listitem');
    expect(listItems).toHaveLength(0);
    const editLinks = screen.queryAllByText('EDIT');
    expect(editLinks).toHaveLength(0);
  });

  it('should render an error message for a server error', async () => {
    jest.spyOn(hooks, 'default').mockImplementation(() => ({
      subregions: null,
      isLoading: false,
      errorMessage: 'Server error. Status code: 500',
    }));

    act(() => {
      render(<SearchSubregions />, {});
    });
    await waitFor(() => expect(hooks.default).toHaveBeenCalled());
    const listItems = screen.queryAllByRole('listitem');
    expect(listItems).toHaveLength(0);
    const editLinks = screen.queryAllByText('EDIT');
    expect(editLinks).toHaveLength(0);
    await waitFor(() => screen.getByRole('alert'));
    expect(screen.getByRole('alert')).toBeInTheDocument();
  });

  it('should render a loading bar', async () => {
    jest.spyOn(hooks, 'default').mockImplementation(() => ({
      subregions: null,
      isLoading: true,
      errorMessage: '',
    }));

    act(() => {
      render(<SearchSubregions />, {});
    });
    await waitFor(() => expect(hooks.default).toHaveBeenCalled());
    const listItems = screen.queryAllByRole('listitem');
    expect(listItems).toHaveLength(0);
    const editLinks = screen.queryAllByText('EDIT');
    expect(editLinks).toHaveLength(0);
    await waitFor(() => screen.getByTestId('subregion-search-progressbar'));
  });

  it('should render no results found message when subregions array is null', async () => {
    jest.spyOn(hooks, 'default').mockImplementation(() => ({
      subregions: null,
      isLoading: false,
      errorMessage: '',
    }));

    act(() => {
      render(
        <RouterContext.Provider value={createMockRouter({ query: { q: 'test' } })}>
          <SearchSubregions />
        </RouterContext.Provider>,
        {},
      );
    });
    await waitFor(() => expect(hooks.default).toHaveBeenCalled());
    await waitFor(() => expect(screen.getByRole('alert')).toBeInTheDocument());
    const alert = screen.getByRole('alert');
    expect(alert).toBeInTheDocument();
    expect(alert).toHaveTextContent(/no results found for/i);
  });

  it('should render no results found message when subregions array is empty', async () => {
    jest.spyOn(hooks, 'default').mockImplementation(() => ({
      subregions: [],
      isLoading: false,
      errorMessage: '',
    }));

    act(() => {
      render(
        <RouterContext.Provider value={createMockRouter({ query: { q: 'test' } })}>
          <SearchSubregions />
        </RouterContext.Provider>,
        {},
      );
    });
    await waitFor(() => expect(hooks.default).toHaveBeenCalled());
    await waitFor(() => expect(screen.getByRole('alert')).toBeInTheDocument());
    const alert = screen.getByRole('alert');
    expect(alert).toBeInTheDocument();
    expect(alert).toHaveTextContent(/no results found for/i);
  });

  it('should add search query to url with a valid search term', async () => {
    jest.spyOn(hooks, 'default').mockImplementation(() => ({
      subregions: [],
      isLoading: false,
      errorMessage: '',
    }));

    const mockRouter = createMockRouter({ query: { q: '' } });
    act(() => {
      render(
        <RouterContext.Provider value={mockRouter}>
          <SearchSubregions />
        </RouterContext.Provider>,
        {},
      );
    });
    await waitFor(() => expect(hooks.default).toHaveBeenCalled());

    // add a search term and press enter to trigger a search
    const inputEl = screen.getByTestId('search-component-input') as HTMLInputElement;
    fireEvent.change(inputEl, { target: { value: 'Australia' } });
    expect(screen.getByTestId('search-component-input')).toHaveValue('Australia');
    fireEvent.keyDown(inputEl, { key: 'Enter', code: 'Enter', charCode: 13 });

    expect(mockRouter.push).toHaveBeenCalledWith('/?q=Australia');
  });

  it('should clear search query from url with a blank search term', async () => {
    jest.spyOn(hooks, 'default').mockImplementation(() => ({
      subregions: [],
      isLoading: false,
      errorMessage: '',
    }));

    const mockRouter = createMockRouter({ query: { q: 'test' } });
    act(() => {
      render(
        <RouterContext.Provider value={mockRouter}>
          <SearchSubregions />
        </RouterContext.Provider>,
        {},
      );
    });
    await waitFor(() => expect(hooks.default).toHaveBeenCalled());

    // add a search term and press enter to trigger a search
    const inputEl = screen.getByTestId('search-component-input') as HTMLInputElement;
    fireEvent.change(inputEl, { target: { value: '' } });
    expect(screen.getByTestId('search-component-input')).toHaveValue('');
    fireEvent.keyDown(inputEl, { key: 'Enter', code: 'Enter', charCode: 13 });

    expect(mockRouter.push).toHaveBeenCalledWith('/');
  });
});
