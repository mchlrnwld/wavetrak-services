import React, { useState } from 'react';
import { useRouter } from 'next/router';

import { Container, LinearProgress, Typography, Snackbar } from '@mui/material';
import Alert from '../../components/Alert';
import Search from '../../components/Search';
import SearchResults from '../../components/SearchResults';
import useFetchSubregions from '../../hooks/useFetchSubregions';

import styles from './SearchSubregions.module.scss';

const SearchSubregions = () => {
  const router = useRouter();
  const searchQuery = router?.query?.q as string;
  const { subregions, isLoading, errorMessage } = useFetchSubregions(searchQuery);
  const [showInvalidInput, setShowInvalidInput] = useState(false);
  const noSearchResults =
    !isLoading && searchQuery && (!subregions || subregions.length <= 0) && !errorMessage;

  const onSearchClick = async (subregionToSearch: string) => {
    if (subregionToSearch) {
      router.push(`/?q=${subregionToSearch}`);
    } else {
      router.push(`/`);
    }
  };

  const onInvalidInput = () => {
    setShowInvalidInput(true);
  };

  const onInvalidInputClosed = () => {
    setShowInvalidInput(false);
  };

  return (
    <Container maxWidth="sm" data-testid="subregion-search-container">
      <Typography variant="h4" component="h1" className={styles.heading}>
        Search Forecasts
      </Typography>

      <Search
        searchLabel="Search by subregion"
        defaultValue={searchQuery}
        onSearchClick={onSearchClick}
        onInvalidInput={onInvalidInput}
        validationRegex={/^[^+$*\\]*$/}
      />

      {!isLoading && subregions?.length > 0 && (
        <SearchResults results={subregions} baseUrl="/edit?subregionId=" />
      )}

      <Container maxWidth="sm" data-testid="subregion-search-info-container">
        {isLoading && <LinearProgress data-testid="subregion-search-progressbar" />}

        {errorMessage && (
          <Alert severity="error" message={errorMessage} data-testid="subregion-search-error" />
        )}

        {noSearchResults && (
          <Alert
            severity="info"
            message={`No results found for ${searchQuery}`}
            variant="outlined"
          />
        )}

        <Snackbar
          open={showInvalidInput}
          onClose={onInvalidInputClosed}
          autoHideDuration={3000}
          data-testid="subregion-search-invalidinput"
          anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        >
          {/* Must wrap in div to support forwardRef() required by snackback */}
          <div>
            <Alert
              onClose={onInvalidInputClosed}
              severity="error"
              title="Invalid Subregion Name"
              message="The search term cannot contain any of the following: + $ * \"
            />
          </div>
        </Snackbar>
      </Container>
    </Container>
  );
};

export default SearchSubregions;
