import React, { useState } from 'react';
import { Box, Grid, Typography, Stack, Divider, Switch } from '@mui/material';
import { useList } from 'react-use';
import SubregionForecastObservation from '../SubregionForecastObservation';
import styles from './SubregionForecastObservations.module.scss';

interface Props {
  subregionForecastDays: Array<any>;
}
const SubregionForecastObservations: React.FC<Props> = ({ subregionForecastDays }) => {
  const [days, setDays] = useState(3);

  const [subregionForecast, { updateAt }] = useList(
    [...Array(16)].map((_, day) => ({
      observation: '',
      day,
      // Overwrite defaults with existing day
      ...subregionForecastDays[day],
    })),
  );

  const applyNext = (day: number, observation: string) => {
    updateAt(day, { ...subregionForecast[day], observation });
  };

  return (
    <Box className={styles.observationContainer}>
      <Stack direction="row" spacing={2}>
        <Typography className={styles.subregionObservationHeader} variant="h4">
          Subregion Observations
        </Typography>
        <Stack direction="row" spacing={1} alignItems="center">
          <Typography>3 days</Typography>
          <Switch checked={days === 16} onChange={() => setDays(days === 3 ? 16 : 3)} />
          <Typography>16 days</Typography>
        </Stack>
      </Stack>
      <Divider className={styles.break} />
      <Grid container className={styles.observationGridContainer} spacing={2}>
        {subregionForecast.map((subregionForecastDay, i) => {
          // only show `days` number of Observations
          if (i < days) {
            return (
              <Grid
                item
                key={subregionForecastDay.day}
                className={styles.observationGridItem}
                sm={12}
                md={days === 3 ? 4 : 6}
              >
                <SubregionForecastObservation
                  day={i + 1}
                  forecastDate={subregionForecastDay.forecastDay}
                  forecastObservation={subregionForecastDay.observation}
                  rows={days === 3 ? 5 : 3}
                  applyNext={applyNext}
                  hideApplyNextButton={i === days - 1}
                />
              </Grid>
            );
          }
          return null;
        })}
      </Grid>
    </Box>
  );
};

export default SubregionForecastObservations;
