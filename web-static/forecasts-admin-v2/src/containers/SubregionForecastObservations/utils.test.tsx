import validateInput from './utils';

describe('containers / SubregionForecastObservations / utils', () => {
  describe('validateInput', () => {
    const observations = [
      {
        observation:
          'Steep-angled, long period NW swell to build through the day. Wind looks ok for the morning but shape will likely be walled.',
      },
      {
        observation:
          "Chunky mix of S windswell and NW swell mix. Pretty good size but there's also a high probability it will be a blown out mess most of the day. A few spots could be serviceable if the wind stay S early.",
      },
      {
        observation:
          "Decent pulse of WNW swell gradually trending down through the day. There's potential for improving conditions as the early high tide drops if winds can cooperate.",
      },
    ];

    it('should return true for valid observation array', () => {
      const isValid = validateInput(observations, 200);
      expect(isValid).toBeTruthy();
    });

    it('should return false for observation array with an entry that exceeds the character limit', () => {
      const isValid = validateInput(observations, 150);
      expect(isValid).toBeFalsy();
    });

    it('should return false for observation array that is not of length `forecastDays`', () => {
      const isValid = validateInput(observations, 150, 4);
      expect(isValid).toBeFalsy();
    });

    it('should return false for observation array that has an empty observation', () => {
      const invalidObservations = [observations[0], observations[1], { observation: '' }];
      const isValid = validateInput(invalidObservations, 150);
      expect(isValid).toBeFalsy();
    });
  });
});
