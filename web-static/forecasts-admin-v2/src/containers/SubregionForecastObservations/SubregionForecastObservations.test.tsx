import React from 'react';

import { fireEvent } from '@testing-library/react';
import { render, screen } from '../../testUtils';
import SubregionForecastObservations from './SubregionForecastObservations';

import fetchedForecast from '../../fixtures/fetchedForecast.json';

describe('containers / SubregionForecastObservations', () => {
  const subregionForecastDays = fetchedForecast.observations;

  it('should render the subregion forecast heading', () => {
    render(<SubregionForecastObservations subregionForecastDays={subregionForecastDays} />, {});

    expect(screen.getByRole('heading', { level: 4 })).toHaveTextContent(`Subregion Observations`);
  });

  it('should render the 3/16 day toggle', () => {
    render(<SubregionForecastObservations subregionForecastDays={subregionForecastDays} />, {});

    const checkbox = screen.getByRole('checkbox');
    expect(checkbox).not.toBeChecked();

    fireEvent.click(checkbox);
    expect(checkbox).toBeChecked();

    const observationInputs = screen.getAllByRole('textbox');

    // toggling 16 day forecast should bring up 16 observation inputs
    expect(observationInputs).toHaveLength(16);
  });

  it('should render the observation input', () => {
    render(<SubregionForecastObservations subregionForecastDays={subregionForecastDays} />, {});
    const applyNextButtons = screen.getAllByRole('button');

    // default 3 day forecast observations should only have 2 "Apply Next" buttons, with none on the last index
    expect(applyNextButtons).toHaveLength(2);

    const observationInputs = screen.getAllByRole('textbox');

    // default 3 day forecast observations should have 3 observation inputs
    expect(observationInputs).toHaveLength(3);
  });

  it('should render apply next button', () => {
    render(<SubregionForecastObservations subregionForecastDays={subregionForecastDays} />, {});
    const applyNextButtons = screen.getAllByRole('button');

    const applyNext = applyNextButtons[0];

    const observationInputs = screen.getAllByRole('textbox');

    const observation1 = observationInputs[0];
    const observation2 = observationInputs[1];

    expect(observation1).toHaveTextContent(subregionForecastDays[0].observation);
    expect(observation2).toHaveTextContent(subregionForecastDays[1].observation);

    fireEvent.click(applyNext);

    expect(observation2).toHaveTextContent(subregionForecastDays[0].observation);
  });
});
