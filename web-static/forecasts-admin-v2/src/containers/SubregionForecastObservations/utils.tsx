// Ensure that Array of notes that is passed in contains at least one entry between 0 and `observationCharacterLimit` characters
const validateInput = (
  subregionForecast: Array<{ observation: string }>,
  observationCharacterLimit: number,
  forecastDays: number = 3,
) =>
  subregionForecast.filter(
    (entry) =>
      entry.observation?.length > 0 && entry.observation?.length <= observationCharacterLimit,
  ).length === forecastDays;

export default validateInput;
