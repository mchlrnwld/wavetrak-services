import React from 'react';

import { fireEvent } from '@testing-library/react';
import { render, screen } from '../../testUtils';
import SpotNavigation from './SpotNavigation';

import regionOverview from '../../fixtures/regionOverview.json';

describe('containers / SpotNavigation', () => {
  const setTab = jest.fn();
  const { spots } = regionOverview.data;

  it('should render `spots.length` tabs', () => {
    render(<SpotNavigation spots={spots} tab={0} setTab={setTab} />, {});

    const tab = screen.getAllByRole('tab');
    expect(tab).toHaveLength(spots.length);
  });

  it('should call setTab when other tab is clicked', () => {
    render(<SpotNavigation spots={spots} tab={0} setTab={setTab} />, {});
    const secondTab = screen.getAllByRole('tab')[1];

    fireEvent.click(secondTab);
    expect(setTab).toHaveBeenCalled();
  });
});
