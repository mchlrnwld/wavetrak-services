import React from 'react';
import { Tabs, Tab } from '@mui/material';

interface Props {
  // todo: define spot type and use array of that
  spots: { _id: string; name: string; [x: string]: any }[];
  tab: number;
  setTab: Function;
}

const SpotNavigation: React.FC<Props> = ({ spots, tab, setTab }) => {
  const handleChangeTab = (_: any, newValue: number) => {
    setTab(newValue);
  };

  return (
    <Tabs variant="scrollable" scrollButtons="auto" onChange={handleChangeTab} value={tab}>
      {spots.map((spot) => (
        <Tab label={spot.name} key={spot._id} />
      ))}
    </Tabs>
  );
};

export default SpotNavigation;
