const base = {
  adminServicesEndpoint: 'https://tools.sandbox.surflineadmin.com/api',
};

export default base;
