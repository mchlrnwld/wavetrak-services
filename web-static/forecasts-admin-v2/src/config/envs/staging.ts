const staging = {
  adminServicesEndpoint: 'https://tools.staging.surflineadmin.com/api',
};

export default staging;
