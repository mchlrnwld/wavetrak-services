const development = {
  adminServicesEndpoint: 'https://tools.sandbox.surflineadmin.com/api',
};

export default development;
