const sandbox = {
  adminServicesEndpoint: 'https://tools.sandbox.surflineadmin.com/api',
};

export default sandbox;
