const production = {
  adminServicesEndpoint: 'https://tools.surflineadmin.com/api',
};

export default production;
