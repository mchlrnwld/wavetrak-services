/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
import getConfig from 'next/config';
import baseConfig from './base';

const APP_ENV = getConfig()?.publicRuntimeConfig?.APP_ENV || 'development';

interface EnvConfig {
  adminServicesEndpoint: string;
}

const envConfig = require(`./envs/${APP_ENV}`) as { default: EnvConfig };

const config = {
  ...baseConfig,
  ...(envConfig.default || {}),
};

export default config;
