import Condition from './condition';

export default interface Conditions {
  _id: string;
  forecastDay: string;
  forecaster: {
    name: string;
    avatar: string;
  };
  human: boolean;
  am: Condition;
  pm: Condition;
  observation: string;
  forecastDate: string;
  spotId: string;
  updatedAt: string;
  createdAt: string;
}
