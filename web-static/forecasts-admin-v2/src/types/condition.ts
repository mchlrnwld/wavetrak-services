export default interface Condition {
  waveHeight: {
    max: number;
    min: number;
    plus: boolean;
    humanRelation: string;
    occasionalHeight: number;
  };
  rating: string;
}
