import Conditions from './conditions';

export default interface SpotForecast {
  _id: string;
  name: string;
  human: boolean;
  conditions: Conditions[];
}
