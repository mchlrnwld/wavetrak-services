require("dotenv").config();

module.exports = {
  version: 2,
  snapshot: {
    widths: [375, 1280],
    minHeight: 1024,
    percyCSS: `
      .percy-hide { visibility: hidden !important; }
      .sl-hero-ad-unit { display: none !important; }
    `,
  },
  upload: {
    files: "**/*.{png,jpg,jpeg}",
    ignore: "",
  },
  // Include if you want to limit the amount of stories processed automagically for visual diffs
  // exclude can also be used as a property - https://docs.percy.io/docs/storybook#configuration
  storybook: {
    // include: ["MyStory"],
  },
};
