// eslint-disable-next-line import/extensions
import { PHASE_EXPORT } from 'next/constants.js';
import withTM from 'next-transpile-modules';
import withImages from 'next-images';

const APP_ENV = process.env.APP_ENV || 'development';

const nextConfig = (phase) =>
  /**
   * @type {import('next').NextConfig}
   */
  ({
    swcMinify: true,
    reactStrictMode: true,
    trailingSlash: phase === PHASE_EXPORT,
    publicRuntimeConfig: {
      APP_ENV: process.env.APP_ENV,
    },
  });

const config = (phase) => withTM()(withImages(nextConfig(phase)));

export default config;
