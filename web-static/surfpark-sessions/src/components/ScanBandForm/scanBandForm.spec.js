import React from "react";
import { render, screen } from '@testing-library/react'
import ScanBandForm from "./index";

describe("Scan band page", () => {
  it("should render the heading", () => {
    render(<ScanBandForm />);

    const input = screen.getByTestId('rfid-input');

    expect(input).toBeTruthy()
  });
});
