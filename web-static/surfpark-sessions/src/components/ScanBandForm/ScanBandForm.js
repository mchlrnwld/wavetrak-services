import React, { useEffect, useRef } from 'react';
import { useRouter } from 'next/router'
import postBandMapping from '../../common/api/postBandMapping';
import { ADD_SESSIONS_URL } from '../../utils/pageConstants';
import { useBandContext } from '../../common/BandContext/BandContext';
import styles from './scanBandForm.module.scss';

function ScanBandForm() {
  const rfidInputRef = useRef(null);
  const router = useRouter();
  const bandContext = useBandContext();
  
  useEffect(() => {
    rfidInputRef.current.focus()
  }, []);


  const validateRFID = (rfid) => {
    // TODO: Validate RFID
    console.log('VALIDATE ->', rfid);
  }

  const handleScanBand = async (e) => {
    const rfid = e.target.value;

    validateRFID(rfid);

    const bandMapping = await postBandMapping(rfid);

    bandContext.setRfid(rfid);
    bandContext.setBandId(bandMapping._id);
  
    if (bandMapping.totalPaidSessions > 0) {
      bandContext.setTotalPaidSessions(bandMapping.totalPaidSessions);
    }

   router.push(ADD_SESSIONS_URL);  
  }

  const handleInputBlur = (e) => {
    e.preventDefault();

    rfidInputRef.current.focus();
  }

  return(
    <form>
      <div className={styles.scanBandImage}></div>
      <input type="text" data-testid="rfid-input" className={styles.input} onChange={handleScanBand} ref={rfidInputRef} onBlur={handleInputBlur} />
    </form>
  )
}



export default ScanBandForm;
