import React, { useEffect, useState } from 'react';
import { Button } from '@surfline/quiver-react';
import { useRouter } from 'next/router';

import { useBandContext } from '../../common/BandContext/BandContext';
import patchBandMapping from '../../common/api/patchBandMapping';
import { QR_CODE_URL } from '../../utils/pageConstants';

import styles from './addSessionsForm.module.scss';

const AddSessionsForm = () => {

  const bandContext = useBandContext();
  const router = useRouter();
  const [sessionsValue, setSessionsValue] = useState(bandContext.totalPaidSessions);

  const handleSubmit = async (e) => {
    e.preventDefault();

    await patchBandMapping(bandContext.bandId, {totalSessions: sessionsValue});

    router.push(QR_CODE_URL)
  }

  const increment = 'INCREMENT';
  const decrement = 'DECREMENT';

  const handleTotalSessionsChange = (e, type) => {
    e.preventDefault();
  
    let newValue;
    if (type === increment) {
      newValue = sessionsValue + 1;
    } else {
      newValue = sessionsValue > 1 ? sessionsValue - 1 : 0;
    }

    if (sessionsValue !== newValue) {
      setSessionsValue(newValue)
    }
  }

  return (
    <form onSubmit={handleSubmit}>
      <input type="hidden" data-testid="total-sessions-input" className={styles.totalSessionsInput} defaultValue={sessionsValue} />
      <div className={styles.totalSessionsWrapper}>
        <button className={styles.plus} data-testid="increment-sessions-button" onClick={(e) => handleTotalSessionsChange(e, increment)}>+</button>
        <div className={styles.totalSessionsValue}>{sessionsValue}</div>
        <button className={styles.minus} data-testid="decrement-sessions-button"  onClick={(e) => handleTotalSessionsChange(e, decrement)}>-</button>
      </div>
      <div className={styles.buttonWrapper}>
        <Button data-testid="add-sessions-submit-button">Add sessions to band</Button>
      </div>
    </form>
  );
}

export default AddSessionsForm;
