import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import { useBandContext } from '../../common/BandContext/BandContext';
import { HOME_PAGE_URL, SCAN_BAND_URL } from '../../utils/pageConstants';

import styles from './pageLayout.module.scss';

const PageLayout = ({children}) => {
  const bandContext = useBandContext();
  const router = useRouter();

  useEffect(() => {
    const isHomepage = window.location.pathname === HOME_PAGE_URL;
    const isScanBandPage = window.location.pathname === SCAN_BAND_URL;
  
    // Routing logic to make sure you can't get to routes without scanning band
    if (!bandContext.rfid && !isHomepage && !isScanBandPage) {
      router.push(SCAN_BAND_URL);
    }
  }, [bandContext.rfid, router])

  return (
    <main id={styles.main}>
      {children}
    </main>
  );
};

PageLayout.proptypes = {
  Component: PropTypes.node.isRequired,
};

export default PageLayout;
