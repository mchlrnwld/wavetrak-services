export const HOME_PAGE_URL = '/';
export const SCAN_BAND_URL = '/scanBand';
export const ADD_SESSIONS_URL = '/addSessions';
export const QR_CODE_URL = '/qrCode';