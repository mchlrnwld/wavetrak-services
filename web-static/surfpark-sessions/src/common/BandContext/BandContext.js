import React, { useContext, createContext, useState } from "react";

const BandContext = createContext({});

export const BandContextProvider = ({children}) => {
  const [rfid, setRfid] = useState();
  const [bandId, setBandId] = useState();
  const [totalPaidSessions, setTotalPaidSessions] = useState(0);

  return <BandContext.Provider value={{setRfid, rfid, bandId, setBandId, totalPaidSessions, setTotalPaidSessions}}>{children}</BandContext.Provider>
}

export const useBandContext = () => useContext(BandContext);
