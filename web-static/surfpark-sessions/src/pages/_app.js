import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { BandContextProvider } from '../common/BandContext/BandContext';
import '../common/themes/main.scss';

const App = ({ Component, pageProps }) => {
  return (
    <BandContextProvider>
      <Component {...pageProps} />
    </BandContextProvider>
  )
};

App.propTypes = {
  pageProps: PropTypes.shape({}).isRequired,
  Component: PropTypes.elementType.isRequired,
};

export default App;
