import { Button } from '@surfline/quiver-react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import PageLayout from '../components/PageLayout'
import { SCAN_BAND_URL } from '../utils/pageConstants';


export default function Index() {
  const router = useRouter();

  const handleButtonClick = (e) => {
    e.preventDefault();
  
    router.push(SCAN_BAND_URL);
  }
  return (
    <>
      <Head>
        <title>Surpark sessions - Home</title>
      </Head>
      <PageLayout>
        <h2>Welcome to surfparks sessions</h2>
        <Button data-testid="welcome-button-continue" onClick={handleButtonClick} style={{margin: '0 auto'}}>Click here to scan bands</Button>
      </PageLayout>
    </>
  )
}
