import Head from 'next/head';
import AddSessionsForm from '../components/AddSessionsForm';
import PageLayout from '../components/PageLayout';

export default function AddSessions() {
  return (
    <>
      <Head>
        <title>Surfpark sessions - Add sessions</title>
      </Head>
      <PageLayout>
        <h2>Select number of sessions</h2>
        <AddSessionsForm />
      </PageLayout>
    </>
  )
}
