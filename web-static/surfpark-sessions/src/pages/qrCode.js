import { Button } from '@surfline/quiver-react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useBandContext } from '../common/BandContext/BandContext';
import PageLayout from '../components/PageLayout'
import { HOME_PAGE_URL } from '../utils/pageConstants';

export default function QrCode() {
  const router = useRouter();
  const bandContext = useBandContext();

  const handleButtonClick = (e) => {
    e.preventDefault();

    router.push(HOME_PAGE_URL);
  }

  return (
    <>
      <Head>
        <title>Surpark sessions - Scan QR code</title>
      </Head>
      <PageLayout>
        <h2>Thanks for mapping your band</h2>
        <p>Now scan this QR code with your phone to sync with your surfline account</p>
        <div>BAND ID: {bandContext.bandId}</div>
        <div>BAND RFIF: {bandContext.rfid}</div>
        <Button data-testid="back-home-button" onClick={handleButtonClick} style={{margin: '30px auto'}}>Back home</Button>
      </PageLayout>
    </>
  )
}
