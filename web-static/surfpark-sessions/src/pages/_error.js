import { useRouter } from 'next/router';
import { FiveHundred, NotFound } from '@surfline/quiver-react';
// import getConfig from '../config';

// const config = getConfig();

const ErrorPage = ({ errorCode }) => {
  const router = useRouter();
  let errTemplate = null;
  if (errorCode) {
    switch (errorCode) {
      case 404:
        errTemplate = <NotFound />;
        break;
      case 500:
        errTemplate = <FiveHundred />;
        break;
      default:
        errTemplate = (
          <div>
            <p>
              An HTTP error <strong>{errorCode}</strong> occurred while trying to access{' '}
              <strong>{router.pathname}</strong>
            </p>
            {/* <p>
              Go to{' '}
              <Link href={config.redirectUrl}>
                <a href={config.redirectUrl}>Home</a>
              </Link>
            </p> */}
          </div>
        );
    }
  }
  return errTemplate;
};

ErrorPage.getInitialProps = async ({ res, xhr }) => {
  // eslint-disable-next-line no-nested-ternary
  const errorCode = res ? res.statusCode : xhr ? xhr.status : null;
  return { errorCode };
};

export default ErrorPage;
