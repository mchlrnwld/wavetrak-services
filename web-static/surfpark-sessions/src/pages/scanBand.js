import Head from 'next/head';
import ScanBandForm from '../components/ScanBandForm';
import PageLayout from '../components/PageLayout';

export default function ScanBand() {
  return (
    <>
      <Head>
        <title>Surpark sessions - Scan a band</title>
      </Head>
      <PageLayout>
        <h2>Scan a band on the reader</h2>
        <ScanBandForm />
      </PageLayout>
    </>
  )
}
