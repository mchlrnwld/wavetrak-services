/// <reference types="cypress" />
/* eslint-disable jest/expect-expect */

describe('displays Page', () => {
  beforeEach(() => {
    console.log('FIRE BEFORE EACH')
    cy.visit(Cypress.config('baseUrl'));
  });

  it('Should render default page', () => {

    console.log('FIRED');
    cy.get('[data-test=scan-band-title]').should('contain.text', 'Scan Band Page');
  });
});
