---
id: overview
title: Overview
sidebar_label: Overview
---

The Compatible API is a low-level HTTP-based REST API which can be used by third-party apps to access surf forecasts and push session data into the Surfline platform on behalf of users.

For the purposes of the Sessions experience Surfline doesn't require read access to any partner APIs, our platform enables partners to push data to our service vs the other way around. We believe this provides a better experience for the user. Since data is pushed from partner apps to Surfline once a user finishes or syncs their session with a partners app, the session should be immediately available on Surfline.

# Base URL

Surfline platform APIs are available at `https://platform.surfline.com`

# Authentication

There's two authentication methods for accessing the compatible platform:

* API access authentication
* User access authentication

API Access authentication is required for all API requests made to the platform. User access authentication is required to make any requests on a user's behalf.

### API Access Authentication

Simply pass the following as part of the HTTP request headers

```
x-api-key: yourclientid
```

### User Access Authentication

User authentication to get an access token is covered in detail [here](/docs/accesstokens.html).