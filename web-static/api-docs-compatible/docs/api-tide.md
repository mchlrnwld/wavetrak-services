---
id: api-tide
title: /forecasts/tides
sidebar_label: /forecasts/tides
---

# GET /forecasts/tides

Returns tide forecast data for a given spot.

## Request Parameters
|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|legacySpotId|	Integer|	Yes|	Spot ID to get the tides for.|
|days|	Integer|	No|	Number of days into the future to get tides for. Default 1.|

## Response Object
|Parameter|Type|Description|
| ------------- | ----- | ----- |
|associated.utcOffset|Int|UTC offset for tide location in hours.|
|associated.units.temperature|String|Temperature unit.|
|associated.units.tideHeight|String|Tide height unit.|
|associated.units.waveHeight|String|Wave height unit.|
|associated.units.windSpeed|String|Wind speed unit.|
|associated.tideLocation.name|String|Name of the tide location for this spot.|
|associated.tideLocation.min|Int|Lowest low tide in 20 year period.|
|associated.tideLocation.max|Int|Heighest high tide in 20 year period. Use this plus `min` to set the min/max values of a y axis when rendering a tide graph.|
|associated.tideLocation.lon|Float|Tide location longitude.|
|associated.tideLocation.lat|Float|Tide location latitude.|
|associated.tideLocation.mean|Float|Tide location mean sea level.|
|data.tides[].timestamp|Int|Unix timestamp for tide time.|
|data.tides[].type|String|NORMAL, LOW, HIGH enum indicates type of tide change.|
|data.tides[].height|Float|Sea level at tide increment.|
