---
id: sessionsync
title: Syncing Sessions
sidebar_label: Syncing Sessions
---

# When to Sync Sessions

There's two events which should trigger a session sync: after a surf and upon initial connection.

Appropriately, partners are advised to have two methods to push sessions to Surfline.

## Post Surf

After the user has completed a surf and a request is made to the partners server to store that session, a request should be sent to the Surfline server with that session. Surfline's server does very little data processing with that initial ingestion, so will respond within milliseconds with a Surfline ID for that session. Partners are advised to store that ID with their own session record for future updates.

Two errors can be expected to occur with this request:

- The users `access_token` and `refresh_token` are invalid. One instance where this can happen is if the user changes their password. The partner app should let the user know this session was not sync'd with Surfline and should present the user the OAuth flow to get a new access token. More on that in the [Access Tokens guide](/docs/accesstokens.html). After getting a valid access token, the user should be instructed to perform a manual sync (more on this below).
- Something went wrong posting the session to Surfline (we plan that this doesn't happen but expect it will at some point). In this instance the user should be notified and instructed to perform a manual sync (more on this below).

## Initial Connection

When a user first connects their Surfline account with a partner app, the partner app should initiate a sync of all unsynced sessions with Surfline. This process can be done asyncronously, but ideally should begin immediately so when the user goes back to the Surfline app they're not presented with an empty list if they have old sessions in the partner app. Ideally this sync should look for all sessions recorded by that user that don't have a coresponding Surfline session ID and sync those in date ascending order. That way when the user goes back to the Surfline app they'll see their newest sessions immediately and depending on how long the sync takes older sessions filter in over time.

## Manual Session Sync

A button in the partner's app should be provided to allow the user to sync any unsync'ed sessions to Surfline. This allows the user to resolve any sync issues that might occur in the above flows. This button should in-effect start the same process that takes place with the initial sync.

## Surf Spots

Today there's no API available to provide a list of Surfline surf spots inside partner apps. When a new session is sent to Surfline we use the first coordinate of the first wave ridden to identify the spot and allow the user to change this if incorrect.