---
id: api-sessions
title: /sessions
sidebar_label: /sessions
---

# POST /sessions

Adds a new surf session for the user (identified by bearer token).

### Session Privacy

If your app allows the user to set a privacy setting for a session please send that with the session post. While all sessions are private in the Surfline experience today we will eventually enable public sessions and want to maintain the user's existing settings. If your app doesn't provide this then please set this to `PRIVATE`.

## Request Object
|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|startTimestamp|	Integer|	Yes|	Unix timestamp, session start time.|
|endTimestamp|	Integer|	Yes|	Unix timestamp, session end time.|
|visibility|	Enum<String>|	Yes	| Denotes whether this session should be publically accessible or not. `PUBLIC` or `PRIVATE`.|
|waveCount|	Integer|	Yes|	Number of waves caught.|
|speedMax|	Float|	Yes|	Maximum speed on waves (meters per second).|
|distancePaddled|	Float|	Yes|	Distance paddled (meters).|
|distanceWaves|	Float|	Yes|	Distance surfed (meters).|
| distanceLongestWave |	Float |	Yes |	Distance of longest wave surfed (meters). |
|deviceType|	Enum<String>|	Yes|	Identifier for this device make/model, no set standard by us but should be consistent for all devices of the same make/model.|
|calories|  Integer | No  | Calories burned during the session|
|events|	Array<Event>|	Yes	|Array of Event objects (see Linked Objects).|

## Response Object
|Parameter|Type|Description|
| ------------- | ----- | ----- |
|success|	Bool|	true/false. See Error Handling.|
|id|	String |	ID of session created on Surfline's side.|
| message |	String|	Message related to status of api response |

# PUT /sessions/:id

Allows app to update a user's session. This will only work for sessions that user owns. We identify that user using the access_token passed. Not all attributes have to be passed, just the ones that need updating. Of course all could be passed.

**Note:** to update the waves property, all wave events should be resent. It's not possible to modify a single wave event without resending all events.

## Request Object
|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|startTimestamp|	Integer|	Yes|	Unix timestamp, session start time.|
|endTimestamp|	Integer|	Yes|	Unix timestamp, session end time.|
|visibility|	Enum<String>|	Yes	| Denotes whether this session should be publically accessible or not. `PUBLIC` or `PRIVATE`.|
|waveCount|	Integer|	Yes|	Number of waves caught.|
|speedMax|	Float|	Yes|	Maximum speed on waves (meters per second).|
|distancePaddled|	Float|	Yes|	Distance paddled (meters).|
|distanceWaves|	Float|	Yes|	Distance surfed (meters).|
|deviceType|	Enum<String>|	Yes|	Identifier for this device make/model, no set standard by us but should be consistent for all devices of the same make/model.|
| calories | Integer | No |	Calories burned during the session |
|events|	Array<Event>|	Yes	|Array of Event objects (see Linked Objects).|

## Response Object
|Parameter|Type|Description|
| ------------- | ----- | ----- |
|success|	Bool|	true/false. See Error Handling.|

# DELETE /sessions/:id

Used to delete a users session. Like the PUT, it'll only allow the user to delete their own sessions.

## Response Object
|Parameter|Type|Description|
| ------------- | ----- | ----- |
|success|	Bool|	true/false. See Error Handling.|


# Linked Objects

## Event

Object is used to describe an event during the user's session.

### Object Properties

|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|startTimestamp|	Integer|	Yes|	UTC timestamp when the event started.|
|endTimestamp|	Integer|	Yes|	UTC timestamp when the event ended.|
|type|	Enum<String>|	Yes|	Key to detonate the event type: `PADDLE` - Used when the user is paddling. `WAVE` - Used when the user is actively surfing a wave.|
|outcome|	Enum<String>|	No|	Key to detonate the outcome for this event: `ATTEMPTED` - This wave event was attempted only. `CAUGHT` - This wave event was considered as having been caught. (default is `CAUGHT`)|
|distance|	Float|	Yes|	Distance in meters of this event (sum of distance between each point).|
|speedMax|	Float|	Yes|	Maximum speed the user traveled during this event in meters per second.|
|speedAverage|	Float|	Yes|	Average speed the user traveled during this event in meters per second.|
|positions|	Array<Position>|	No|	Array of Position objects (see Postion object detail below) that are included as part of this event. Position objects should be at a frequency no higher than 1 per second.|

## Position

Object is used to describe a discrete position in a user's session.

### Object Properties

|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|timestamp|	Integer|	Yes|	Unix timestamp in milliseconds when this position object occurred.|
|latitude|	Float|	Yes|	Coorindate latitude, degrees, decimal format.|
|longitude|	Float|	Yes|	Coorindate longitude, degrees, decimal format.|

# Error Handling

Errors happen, here's what to expect when they do.

We'll return a json object in the body of the response. The header will be a non-200 HTTP code.

## Error Object

|Property|Type|Description|
| ------------- | ----- | ----- |
|success|	Bool|	true/false. If false then code and message below will exist.|
|code	|Int|	Semi-unique error code. This will be unique to the end point being called but not nessacerily unique across endpoints. For example all session endpoints will return unique codes but session and auth might share codes.|
|message|	String|	Explanation of what went wrong.|

## Success Object

The below success object is used when any mutations are performed.

|Property|Type|Description|
| ------------- | ----- | ----- |
|success|	Bool|	true will be returned if everything went ok.|
