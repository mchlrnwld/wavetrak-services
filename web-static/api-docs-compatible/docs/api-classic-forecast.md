---
id: api-classic-forecast
title: /classic/forecasts
sidebar_label: /classic/forecasts
---

# GET /classic/forecasts

Returns forecast data for spots and subregions.

## Request Parameters
|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|legacySpotId|	Integer|	No|	Spot ID to get the forecast for.|
|legacySubregionId|	Integer|	No|	Subregion ID to get the forecast for.|

## Response Object
|Parameter|Type|Description|
| ------------- | ----- | ----- |
|lat|String|Surf spot latitude.|
|lon|String|Surf spot longitude.|
|name|String|Name of surf spot.|
|timezone|Int|Timezone offset in hours for spot.|
|id|String|Legacy ID for surf spot.|
|timeZoneString|String|Unix timezone identifier.|
|new_hd_cam|String|Deprecated.|
|theValue|String|Deprecated.|
|Surf.startDate_GMT|Int|Current unix timestamp.|
|Surf.startDate_LOCAL|Int|Timezone adjusted unix timestamp for now.|
|Surf.startDate_pretty_GMT|String|Date formatted version of `Surf.startDate_GMT`.|
|Surf.startDate_pretty_LOCAL|String|Date formatted version of `Surf.startDate_LOCAL`.|
|Surf.dateStamp[][]|String|Date formatted string for the time at which each data point in `Surf.swell_*` and `Surf.surf_*` arrays is for.|
|Surf.swell_height1[][]|Float|Primary swell height, each array is a day, each value is an hour on that day corresponding with `Surf.dateStamp`.|
|Surf.swell_height2[][]|Float|Secondary swell height.|
|Surf.swell_height3[][]|Float|Tertiary swell height.|
|Surf.swell_direction1[][]|Float|Primary swell direction.|
|Surf.swell_direction2[][]|Float|Secondary swell direction.|
|Surf.swell_direction3[][]|Float|Tertiary swell direction.|
|Surf.swell_period1[][]|Float|Primary swell period.|
|Surf.swell_period2[][]|Float|Secondary swell period.|
|Surf.swell_period3[][]|Float|Tertiary swell period.|
|Surf.surf_max[][]|Float|Max surf height for the day/hour.|
|Surf.surf_min[][]|Float|Min surf heigth for the day/hour. Combine with `Surf.surf_max` to create 1-2ft|
|Surf.modelRun[][]|Int|Date stamp of the model run. Deprecated.|
|Surf.modelCode[][]|String|Deprecated.|
|Surf.surf_max_maximum|Float|Max surf height in all data returned.|
|Surf.periodSchedule[][]|String|Deprecated.|
|Surf.modelRunDisplay|Int|Deprecated.|
|Surf.modelCodeDisplay|String|Deprecated.|
|Surf.SwellSource|String|Deprecated.|
|Surf.units|String|Units key for data e,m.|
|Surf.spot.dateStamp[][]|String|Date formatted string for the time at which each data point in `Surf.swell_*` and `Surf.surf_*` arrays is for.|
|Surf.spot.swell_height1[][]|Float|Primary swell height, each array is a day, each value is an hour on that day corresponding with `Surf.dateStamp`.|
|Surf.spot.swell_height2[][]|Float|Secondary swell height.|
|Surf.spot.swell_height3[][]|Float|Tertiary swell height.|
|Surf.spot.swell_direction1[][]|Float|Primary swell direction.|
|Surf.spot.swell_direction2[][]|Float|Secondary swell direction.|
|Surf.spot.swell_direction3[][]|Float|Tertiary swell direction.|
|Surf.spot.swell_period1[][]|Float|Primary swell period.|
|Surf.spot.swell_period2[][]|Float|Secondary swell period.|
|Surf.spot.swell_period3[][]|Float|Tertiary swell period.|
|Surf.spot.modelRun[][]|Int|Date stamp of the model run. Deprecated.|
|Surf.spot.modelCode[][]|String|Deprecated.|
|Surf.spot.periodSchedule[][]|String|Deprecated.|
|Tide.startDate_GMT|Int|Unix timestamp for start of the tide forecast.|
|Tide.startDate_pretty_GMT|String|Date formatted version of unix timestamp for start of tide forecast.|
|Tide.startDate_LOCAL|Int|Timezone adjusted unix timestamp for start of the tide forecast.|
|Tide.startDate_pretty_LOCAL|String|Timezone adjusted date formatted version of the above.|
|Tide.SunPoints[].type|String|Sunrise,Sunset denoting state of the sun event.|
|Tide.SunPoints[].Localtime|String|Date formatted string of when the sun event will occur in local time.|
|Tide.SunPoints[].time|Int|Timezone adjusted unix timestamp of when the event will occur.|
|Tide.SunPoints[].utctime|String|UTC date formatter string of event time.|
|Tide.SunPoints[].Rawtime|String|Date formatted local string of when the event will occur.|
|Tide.Error|String|Deprecated.|
|Tide.TideStation|String|URL encoded string of the tide station/location name.|
|Tide.TideType|String|`StationBased` deprecated.|
|Tide.units|String|e,m units identifier.|
|Tide.datePoints[].Localtime|String|Date formatted string of when the sun event will occur in local time.|
|Tide.datePoints[].time|Int|Timezone adjusted unix timestamp of when the event will occur.|
|Tide.datePoints[].type|String|LOW, NORMAL, HIGH denoting the tide movement.|
|Tide.datePoints[].height|Float|Height of the tide at this time.|
|Tide.datePoints[].utctime|String|UTC date formatter string of event time.|
|Tide.datePoints[].Rawtime|String|Date formatted local string of when the event will occur.|
|Tide.timezone|Int|Timezone offset in hours.|
|Weather.dateStamp[]|String|Date formatted string for when each weather point is for.|
|Weather.units|String|e,m units identifier.|
|Weather.startDate_GMT|Int|Unix timestamp for start of the weather forecast.|
|Weather.startDate_pretty_GMT|String|Date formatted version of unix timestamp for start of weather forecast.|
|Weather.startDate_LOCAL|Int|Timezone adjusted unix timestamp for start of the weather forecast.|
|Weather.startDate_pretty_LOCAL|String|Timezone adjusted date formatted version of the above.|
|Weather.temp_max|Int|Air temperature minimum.|
|Weather.temp_min|Int|Air temperature maximum.|
|Weather.weather_type|String|Weather string which can be used as a key to show an icon.|
|WaterTemp.units|String|Units identifier.|
|WaterTemp.watertemp_min|Int|Water temperature minimum.|
|WaterTemp.watertemp_max|Int|Water temperature maximum.|
|WaterTemp.startDate_GMT|Int|Unix timestamp for water temp.|
|WaterTemp.startDate_LOCAL|Int|Timezone adjusted unix timestamp for the water temp.|
|WaterTemp.startDate_pretty_GMT|String|Date formatted version of unix timestamp for the water temp.|
|WaterTemp.startDate_pretty_LOCAL|String|Timezone adjusted date formatted version of the `_LOCAL`.|
|Wind.windsource|String|Deprectated.|
|Wind.alternatewind|String|Deprecated.|
|Wind.dateStamp[][]|String|Date formatted string for when each wind forecast is for.|
|Wind.periodSchedule[][]|String|Deprecated.|
|Wind.wind_speed[][]|Float|Wind speed, each array is a day and then hour within the day, matching teh `dateStamp` for this attribute.|
|Wind.wind_direction[][]|Float|Wind direction.|
|Wind.startDate_GMT|Int|Unix timestamp for start of the wind forecast.|
|Wind.startDate_LOCAL|Int|Timezone adjusted unix timestamp for start of the wind forecast.|
|Wind.startDate_pretty_GMT|String|Date formatted version of unix timestamp for start of tide forecast.|
|Wind.startDate_pretty_LOCAL|String|Timezone adjusted date formatted version of the `_LOCAL`.|
|Wind.units|String|Wind units identifier.|
|Analysis.surfRange[]|String|Surf height range string per day.|
|Analysis.surfText[]|String|Human height description of the above.|
|Analysis.surfMax[]|Int|Surf height maximum.|
|Analysis.surfMin[]|Int|Surf height minimum.|
|Analysis.reporter|String|Name of the surf reporter.|
|Analysis.forecasterTitle|String|Forecaster job title.|
|Analysis.forecasterEmail|String|Deprecated.|
|Analysis.reportdate|String|Local time the report was made.|
|Analysis.report_time|String|Date formatted string for the report.|
|Analysis.report_day|String|Day of the report.|
|Analysis.report_period|String|morning,afternoon.|
|Analysis.generalCondition[]|String|Conditions rating enum.|
|Analysis.units|String|Units identifier.|
|Analysis.short_term_forecast|String|Description of the surf condtions.|
|Analysis.startDate_GMT|Int|Unix timestamp for start of the analysis text.|
|Analysis.startDate_LOCAL|Int|Timezone adjusted unix timestamp for the analysis.|
|Analysis.startDate_pretty_GMT|String|Date formatted version of unix timestamp for start of the analysis.|
|Analysis.startDate_pretty_LOCAL|String|Timezone adjusted date formatted version of the `_LOCAL`.|
|Analysis.isLOLA[]|String|Deprecated.|
|Location.regioname|String|Name of the region this spot is within.|
|Location.tide_location|String|Name of the tide station for this spot.|
|Location.subregionalias|String|Subregion alias/key.|
|Location.regionalias|String|Region alias/key.|
|_metadata.canonicalUrl|String|URL for this spot.|
|_metadata.dateCreated|String|Deprecated.|