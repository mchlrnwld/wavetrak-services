---
id: accesstokens
title: Access Tokens
sidebar_label: Access Tokens
---

The following guide details how to get a `access_token` and `refresh_token` for a Surfline user.

Surfline has implemented the OAuth 2.0 [authorization code grant flow](https://tools.ietf.org/html/rfc6749#section-4.1). There's multiple oauth flows in the oauth specification. The authorization code flow is implemented because it's the optimal flow for server to server communication. This should not be used on the client to communicate to Surfline, since that'll expose your `client_secret`. The below is an overview of this flow (provided by Slack).

![OAuth authorization code grant flow.](/img/slack-oauth-flow.png)

For native app implementations the flow below should happen within a web view within your app. Ideally this webview is a iOS `SFSafariViewController` since that shares cookies with Safari and thus if the user is already logged into Surfline.com within Safari they won't need to login again when this prompt appears.

For the purposes of the Sessions experience, data from partner apps is pushed to Surfline, and because of this the connection to Surfline is done within the partner app. In order to promote this experience within the Surfline app we use deep links out to partner apps to create that connection and listen to `surfline://sessions` for partner apps to send user data back to the Surfline experience once connected from that flow. Here's how this flow works from both the partner and Surfline apps.

![App hop flow.](/img/app-hops.png)

Session syncing is discussed in the [Session Sync guide](/docs/sessionsync.md).

# OAuth Server Flow

The following details each step of the OAuth flow.

## Step 1: Redirect to auth URL

Your web or mobile app should redirect users to this URL with the parameters below to kick off the [OAuth 2 authorization code grant flow](https://tools.ietf.org/html/rfc6749#section-4.1).

```
GET: https://platform.surfline.com/connect/authorize
```

### Request Parameters
|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|client_id|String|Yes|Issued by Surfline for your app. This is the same as the `client_id` using in the the `x-api-key` header.|
|scopes|String|Yes|Space separated list of scopes to allow for sessions. This value should be `sessions:write:user`.|
|redirect_uri|String|Yes|URL on your server which the user should be redirected back to upon completion or the authorization page on Surfline.|
|response_type|String|Yes|Value must be set to `code`.|
|state|String|No|Unique identifier to protect [against CSRF](https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-4.1.1).|

## Step 2: User Authentication and Approval

The user will land on a page that looks similar to this:

![3-legged auth approval flow.](/img/rip-curl-3-legged-auth.png)

After signing in or creating an account, a user will be asked to grant your app permission to read/write data on their behalf.

![Sessions write permission approval screen.](/img/rip-curl-approval.png)

## Step 3: Users are redirected back to your site

After completing the authorization page users will be redirected back to the `redirect_uri` page URL supplied in the oauth/authorize request. Within the URL there will be a code parameter that can be used in step 3.

The following parameters will be passed as `GET` parameters to the `redirect_uri`.

|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|code|	String|	No	|This will only exist if the user authorized your app. If they didn't, they clicked "cancel" on the authorize page.|
|state|	String|	No	|If their state was passed in the original redirect by your app this will be echoed back.|

## Step 4: Exchange the verification code for an access token

The code provided by the above is temporary and cannot be used to access resources on behalf of the user. It needs to be converted to an access_token which can be used for that purpose. To do that call the below API.

```
POST: https://platform.surfline.com/oauth/token
```

The above should be called with a JSON object in the request body. That object should have the following properties:

|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|client_id|	String|	Yes|	Issued by Surfline for your app.|
|client_secret|	String|	Yes|	Issued by Surfline for your app. This should not be stored in a publicly accessible place.|
|grant_type|	String|	Yes	|Either `authorization_code` or `refresh_token`. In this part of the OAuth flow it should be `authorization_code`.|
|code|	String|	Yes (in this context)| Returned in step 2.|

The following headers should be passed:

|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|Authorization|	String|	Yes|Basic auth header using the base64 encoding of a client_id combined with the client_secret. `base64(client_id:client_secret)` a sample value might look like: `Basic 9999Y2MwODJiNWFjZmEzNjVmNDQ3YjZhOn9999xjUmdHc0VmVmpab09zRkFo9999`.|
|Content-Type|	String|	Yes|`application/json`|

If the request was successful Surfline will respond with a JSON object with the following properties:

|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|access_token|	String|	Yes|	This is the access token which is granted to your app and can be used to make requests on bahalf of a user.|
|refresh_token|	String|	Yes|	The access token has a relatively short lifespan and needs to be refreshed using this token. The refresh token cannot be used to make requests on bahalf of a user but can be used to get another access token. See handling refreshes below. The refresh token has no expiration date but will not be exchanged for an access token if the user has deauthorized your app on the Surfline side. Should that occur your app should ask the user to authenticate the app again.|
|expires_in|	Integer|	Yes|	This is when the access token will expire.|

#### Sample Request

```
POST /oauth/token HTTP/1.1
Host: platform.surfline.com
Content-Type: application/x-www-form-urlencoded
Authorization: Basic 9999Y2MwODJiNWFjZmEzNjVmNDQ3YjZhOn9999xjUmdHc0VmVmpab09zRkFo9999

grant_type=authorization_code&code=f61e91e690ed9f08f6d7070be54e49ddc7e1b967
{
    "access_token": "0e49c2f0780ffb208d7999d49106477a8aa1a874",
    "refresh_token": "714ba3e0b56df66565f390f9f4c03269e6b36af0",
    "expires_in": 2592000,
    "token_type": "Bearer"
}
```

## Step 5: Make a request with the access token

For requests that require a user access token in addition to the x-api-key header value add the following to the header:

```
Authorization: Bearer {access_token}
```

## Step 6: Handling The `refresh_token`

The access token provided is short lived. If an API request that requires the access token returns a 401 unauthorized use the /oauth/token API to request a new access token.

```
POST: https://platform.surfline.com/oauth/token
```

The above should be called with a JSON object in the request body. That object should have the following properties:

|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|client_id|	String|	Yes|	Issued by Surfline for your app.|
|client_secret|	String|	Yes|	Issued by Surfline for your app. This should not be stored in a publicly accessible place.|
|grant_type|	String|	Yes|	Either `authorization_code` or `refresh_token`. In this part of the OAuth flow it should be `refresh_token`.|
|refresh_token|	String|	Yes (in this context)|	The users refresh token.|

The following headers should be passed:

|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|Authorization|	String|	Yes|Basic auth header using the base64 encoding of a client_id combined with the client_secret. `base64(client_id:client_secret)` a sample value might look like: `Basic 9999Y2MwODJiNWFjZmEzNjVmNDQ3YjZhOn9999xjUmdHc0VmVmpab09zRkFo9999`.|
|Content-Type|	String|	Yes|`application/json`|

#### Sample Request

```
POST /oauth/token HTTP/1.1
Host: platform.surfline.com
Content-Type: application/json
Authorization: Basic 9999Y2MwODJiNWFjZmEzNjVmNDQ3YjZhOn9999xjUmdHc0VmVmpab09zRkFo9999

{
    "grant_type": "refresh_token",
    "refresh_token": "4431eb1a17c4238db70cbefdf4e27eeb1b2c09b2"
}

## response ##

{
    "access_token": "7421e2d125ccb072dfebf0dff585c1d9a1994694",
    "refresh_token": "1f4732742e79c1e4f86e4964fa1889f1e93605ed",
    "expires_in": 2592000,
    "token_type": "Bearer"
}
```

If the request is successful, Surfline will respond with a JSON object identical to step 3. If this request returns a HTTP 401 unauthorized then the user has deauthorized your app on the Surfline side. You'll want to ask the user to re-authenticate your app.

## Step 7.  Invalidating/Revoking Tokens
Access Tokens and/or Refresh Tokens can be invalidated/revoked by making requests to the API at /oauth/revoke.
```
POST: https://platform.surfline.com/oauth/revoke
```

The above should be called with the following form parameters in the request body.

|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|token|	String|	Yes|	Either the access_token or refresh_token value to be invalidated.|
|token_type_hint| String|	No|	If provided, this value MUST be equal to either "access_token" or "refresh_token".|

The following headers should be included in the request.

|Parameter|Type|Required|Description|
| ------------- | ------------- | ----- | ----- |
|Authorization|	String|	Yes|Basic auth header using the base64 encoding of a client_id combined with the client_secret. `base64(client_id:client_secret)` a sample value might look like: `Basic 9999Y2MwODJiNWFjZmEzNjVmNDQ3YjZhOn9999xjUmdHc0VmVmpab09zRkFo9999`.|
|Content-Type|	String|	Yes|`application/x-www-form-urlencoded`|

If the request is successful, Surfline will respond with a status of **200 OK** without any additional information.  Otherwise, this request will return a **400 Bad Request** if a bad token_type_hint parameter value is sent, or the token parameter value is not defined.  If this API is down, the API will respond with a **500 Internal Server Error**.

#### Sample Request
```
POST /oauth/revoke HTTP/1.1
Host: platform.staging.surfline.com
Content-Type: application/x-www-form-urlencoded
Authorization: Basic 9999Y2MwODJiNWFjZmEzNjVmNDQ3YjZhOn9999xjUmdHc0VmVmpab09zRkFo9999

'token=0e87a59c95a6c965c6677347a14bae159ee8a1b9&token_type_hint=access_token'

## response ##
200
{
    "message": "Successfully processed request."
}
```

## Step 8: Go surfing.