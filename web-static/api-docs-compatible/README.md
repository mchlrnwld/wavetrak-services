# Surfline Compatible Docs Minisite

Site is hosted in a S3 bucket and generated with [Docusaurus](https://docusaurus.io/).

It's available at: http://sl-docs-compatible-prod.s3-website-us-west-1.amazonaws.com/

To modify first install Docusaurus

```bash
make install
```

Then:

```bash
make run
```

Markdown files for pages are in `docs/`. To add a new page add a Markdown file to the same directory and configure it to appear in `website/sidebars.json`.

Deploying with Github Actions: 

Our updated CI/CD pipeline leverages Github actions and workflows. A step by step guide for deploying with this pipeline can be found [here](https://github.com/Surfline/wavetrak-services/pull/5669)

Deploying without Github Actions:

```bash
# Your shell environment should be set to access surfline-prod. With named AWS profiles that might look something like this:
AWS_PROFILE=surfline-prod make publish
```

Where `prod` is the production AWS profile on your local machine. This account
needs write access to the `sl-docs-compatible-prod` bucket.

## Infratructure

This is hand roled. The following was setup:

- S3 bucket `sl-docs-compatible-prod` with static site serving enabled.
