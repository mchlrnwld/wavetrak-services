/* eslint-disable camelcase */
const concurrently = require('concurrently');

const build_sl = `BRAND=sl npm run build-deploy --$APP_ENV`;
const build_fs = `BRAND=fs npm run build-deploy --$APP_ENV`;
const build_bw = `BRAND=bw npm run build-deploy --$APP_ENV`;

concurrently([build_sl, build_fs, build_bw]);
