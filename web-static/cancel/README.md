# Cancellation Funnel

An application that enables subscription cancellation for authorized premium members. 
Tech stack: Nextjs + React + Redux. 

### Setup

```
nvm use 12
npm install
```

### Local developement:

```
npm run dev
```

Access the app as `http://localhost:3000/cancel?subscriptionId=<subscriptionId>`

___Note: There should be a valid `accessToken` and `subscriptionId` present in the request.___

If you want a static site running in local then use:

```
npm run dev-static
```
This builds and exports the app and serves the static site from the `out` folder.
Access the static site as `http://localhost:5000/cancel?subscriptionId=<subscriptionId>`

#### Optional Args
| Name       | Values                                            | Default       |
|------------|---------------------------------------------------|---------------|
| `$BRAND`   | `sl`, `fs`, `bw`                                  | `sl`           |
| `$APP_ENV` | `development`, `sandbox`, `staging`, `production` | `development` |

___Note: dev-static does not support hot reloading now___

To analyze the build output use the `ANALYZE` option.
```
ANALYZE=true APP_ENV=sandbox BRAND=sl npm run build-deploy
```

#### Build and Deploy to higher environments:

```
npm run build-brand
```
This command is for brand-specific deploys to each environment and outputs files to the /build directory.
```
build
 |
 +-- sl
        |    
        +-- out
        +-- _next files
```        
When deploying to higher environments we would build the app for three brands and copy the files over to brand specific folders in Product CDN.

Use the following Jenkins job to build and deploy this application: [build-and-deploy-account-to-s3](https://surfline-jenkins-master-prod.surflineadmin.com/job/surfline/job/Web/job/build-and-deploy-account-to-s3/).

## General Development Guidelines

### Setting up Routes

Since Nextjs comes with a file system based routing we set up the routes under `pages` directory as files. With Next.js, each page is represented by a JavaScript file under the pages subdirectory. Each file exports a React component that is used by Next.js to render the page (default root route is index.js).
In order to support client-side navigation we use `next/link` in each page. `<Link />` will prefetch the next page and navigation will happen without a page refresh.

* pages — The top-level components and routes for the App

* components — The smaller components used to build larger pages

### Using _app.js

Next.js uses the `App` component to initialize pages. We use this file to customize the App component to control page initialization and to add common layouts.

https://github.com/zeit/next.js#custom-app

### Using getInitialProps

For initial data population in your pages you can use `getInitialProps` function. Note that `getInitialProps` cannot be used in children components. Only in pages.
Also note that adding a custom `getInitialProps` will affect [Automatic Static Optimization](https://github.com/zeit/next.js#automatic-static-optimization) of your page. In this app we do not use `getInitialProps` function. For a client side app we can use React Hooks to fetch the data on page initialization. 

### Dynamic URLS in Static Site

In non dev environments we serve the app as a static website. While building and deploying we export the site as a completely static site. Hence it does not support dynamic urls. `next export` is not intended to work similar as CRA + React router because CRA will create a single page app with a single html file.  In our case `next export` just creates HTML files (for each page) with the content and is not a SPA. It is a bunch of HTML files. The browser just goes to the URL you tell it to (for example: /cancel/verify) and gets the corresponding HTML file. 

In order to support dynamic urls we have added custom logic in `web-proxy` that route requests to appropriate pages. See [here](https://github.com/Surfline/surfline-web/blob/master/web-proxy/confd/templates/surfline-public.conf.tmpl#L388).

Another option would be to use a custom Node.js server for supporting dynamic urls.

In Prod the url is expected to be in this format: `/cancel/{subscriptionId}/survey?{...query_params}`. In Dev the above format is not supported because Next does not support dynamic urls for client side only apps. The dev format is : `/cancel/survey?subscriptionId={subscriptionId}&{...query_params}` . Take a look at the `urlHelper` util function.

NOTE: `process.env.NODE_ENV` is set to `production` when doing a `next export`. This is done default by Nextjs. This means that when building and deploying to Sandbox/Staging/Production the `process.env.NODE_ENV` would be `production`. If you want custom env variables for each environment then add it to `config` directory.
