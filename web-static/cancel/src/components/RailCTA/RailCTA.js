import { Component } from 'react';
import PropTypes from 'prop-types';
import './RailCTA.scss';

/**
 * @description The `RailCTA` component is used on the right rail. It renders a
 * background image which covers the block and on top of it renders a heading with sub-text and
 * icons. The subtext and icons are passed in from the translations (`copy` prop) under the `features`
 * array.
 *
 * @param {object} props - The props for the component
 * @param {object} props.copy - The brand and feature flag specific copy/icons from the translation file
 * @param {string} props.backgroundImage - (optional) background image url
 *
 * @returns {JSX.Element} FunnelCTA JSX
 */
class RailCTA extends Component {
  /**
   * @description Function which takes a `feature` from the features array in the copy
   * and destructures it to render the icon, header, and description.
   *
   * @param {object} feature - The feature
   * @param {Function} feature.Icon - Icon react component
   * @param {string} feature.header - Header string for the feature
   * @param {string} feature.description - Description string for the feature
   */
  feature = ({ Icon, header, description }) => (
    <div key={header} className="feature">
      <Icon />
      <div className="feature__content">
        <h6 className="feature__content_header">{header}</h6>
        <p className="feature__content_desc">{description}</p>
      </div>
    </div>
  );

  /**
   * @description Function for creating the railCTA JSX
   * @param {object} copy - Copy object
   */
  railCTA = (copy, backgroundImage) => (
    <div className="rail-cta" style={{ backgroundImage }}>
      <div className="rail-cta__content">
        <h4 className="rail-cta__header">{copy.header}</h4>
        <div className="rail-cta__features">
          {copy.features.map((feature) => this.feature(feature))}
        </div>
        {copy.description && <p className="rail-cta__description">{copy.description}</p>}
      </div>
    </div>
  );

  render() {
    const { copy, backgroundImage } = this.props;
    return this.railCTA(copy, backgroundImage);
  }
}

RailCTA.propTypes = {
  copy: PropTypes.instanceOf(Object).isRequired,
  backgroundImage: PropTypes.string,
};

RailCTA.defaultProps = {
  backgroundImage: null,
};

export default RailCTA;
