import { useState, useEffect } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { trackEvent, trackNavigatedToPage, brandToString } from '@surfline/web-common';
import { useRouter } from 'next/router';
import { Button, Input } from '@surfline/quiver-react';

import en from '../../international/translations/en';
import { verifyAndDeleteSubscription } from '../../store/actions/subscription';
import { showUserCancellationProgress } from '../../store/actions/user';
import { getSubscription, getCancellationStatus } from '../../store/selectors/subscription';
import { getUserEmail } from '../../store/selectors/user';
import { formatUrlDisplay } from '../../utils/urlHelper';
import getConfig from '../../config';
import './CancelVerify.scss';

const { brand } = getConfig();

const keepAccount = () => {
  const goToSubscription = () => window.location.assign('/account/subscription');
  trackEvent('Kept Account', { location: 'cancel-funnel-verification' }, null, goToSubscription);
};

const CancelVerify = () => {
  const [password, setPassword] = useState('');
  const [isVerified, setIsVerified] = useState(false);
  const dispatch = useDispatch();
  const router = useRouter();
  const subscription = useSelector(getSubscription, shallowEqual);
  const { deleted, loading, error } = useSelector(getCancellationStatus, shallowEqual);
  const email = useSelector(getUserEmail);
  const { subscriptionId } = subscription;

  useEffect(() => {
    trackNavigatedToPage('Cancel Funnel', {}, 'Verification');
    const userProgress = {
      cancelStep: 3,
    };
    dispatch(showUserCancellationProgress(userProgress));
  }, [dispatch]);

  useEffect(() => {
    if (isVerified && deleted) {
      trackEvent(
        'Clicked Cancelled Subscription',
        {
          location: 'cancel-funnel-verification',
        },
        null,
      );
    }
    if (deleted) {
      router.push(
        `/cancel/thanks?subscriptionId=${subscriptionId}`,
        formatUrlDisplay(`/cancel/${subscriptionId}/thanks`, '/cancel/thanks'),
      );
    }
  }, [deleted, router, subscriptionId, isVerified]);

  const submitHandler = (evt) => {
    evt.preventDefault();
    dispatch(verifyAndDeleteSubscription(email, password, subscription));
    setIsVerified(true);
  };

  return (
    <div>
      <h4>{en.cancellation_funnel.title_verify_password}</h4>
      <form className="cancel-verify__container" onSubmit={submitHandler} action="">
        <Input
          value={password || null}
          label="PASSWORD"
          id="password"
          type="password"
          error={!!error}
          message={error || null}
          input={{
            name: 'password',
            onChange: (event) => setPassword(event.target.value),
            required: true,
          }}
        />
        <a className="cancel-verify__container--link" href="/forgot-password">
          Forgot Password?
        </a>
        <Button style={{ width: '100%' }} button={{ type: 'submit' }} loading={loading}>
          {en.cancellation_funnel.action_password_continue}
        </Button>
      </form>
      <Button type="ghost-shade" onClick={keepAccount}>
        {en.cancellation_funnel.action_password_keep_account(brandToString(brand))}
      </Button>
    </div>
  );
};
export default CancelVerify;
