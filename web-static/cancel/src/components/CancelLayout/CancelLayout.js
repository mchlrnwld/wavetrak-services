import { useSelector, shallowEqual } from 'react-redux';
import PropTypes from 'prop-types';
import { ProgressBar } from '@surfline/quiver-react';
import { color } from '@surfline/quiver-themes';
import classNames from 'classnames';

import { RailLogo, RailCTA } from '..';
import './CancelLayout.scss';
import en from '../../international/translations/en';
import { getUserCancellationProgress } from '../../store/selectors/user';

const contentClass = (appReady) =>
  classNames({
    'cancel-wrapper__content': true,
    'cancel-wrapper__content--loading': !appReady,
  });

export const LeftRail = ({ children }) => (
  <div className="left-rail">
    <RailLogo />
    {children}
  </div>
);

export const RightRail = ({ children }) => <div className="right-rail">{children}</div>;

const CancelLayout = ({ children, appReady }) => {
  const copy = {
    ...en.rail_cta,
    ...en.premiumFeatures,
  };
  const { cancelStep } = useSelector(getUserCancellationProgress, shallowEqual);
  const progressText = cancelStep === 4 ? 'Cancellation complete' : `Step ${cancelStep} of 4`;
  const bgColor = cancelStep === 4 ? color('green') : color('blue-gray', 90);

  return (
    <div className="cancel-container">
      <div className="cancel-wrapper">
        <div className="cancel-wrapper__mobile__header" />
        <LeftRail>
          <div className={contentClass(appReady)}>
            <ProgressBar
              currentStep={cancelStep}
              totalSteps={4}
              progressText={progressText}
              bgColor={bgColor}
            />
            {children}
          </div>
        </LeftRail>
        <RightRail>
          <RailCTA copy={copy} />
        </RightRail>
      </div>
    </div>
  );
};
LeftRail.propTypes = {
  children: PropTypes.node.isRequired,
};
RightRail.propTypes = {
  children: PropTypes.node.isRequired,
};
CancelLayout.propTypes = {
  children: PropTypes.node.isRequired,
  appReady: PropTypes.bool.isRequired,
};

export default CancelLayout;
