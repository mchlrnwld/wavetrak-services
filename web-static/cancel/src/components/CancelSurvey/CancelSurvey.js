import { useEffect, useRef } from 'react';
import { useRouter } from 'next/router';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import Link from 'next/link';
import { Button, TrackableLink } from '@surfline/quiver-react';
import { trackNavigatedToPage, trackEvent } from '@surfline/web-common';
import en from '../../international/translations/en';
import { getSubscription } from '../../store/selectors/subscription';
import { showUserCancellationProgress } from '../../store/actions/user';
import { formatUrlDisplay } from '../../utils/urlHelper';
import './CancelSurvey.scss';

const skipSurveyEventProperties = {
  location: 'cancel-funnel-reason',
};

const CancelSurvey = () => {
  const router = useRouter();
  const linkRef = useRef();
  const text = en.cancellation_funnel;
  const reasons = Object.keys(text.survey_reasons).map((key) => text.survey_reasons[key]);
  const feelings = Object.keys(text.survey_feelings).map((key) => text.survey_feelings[key]);
  const { subscriptionId } = useSelector(getSubscription, shallowEqual);
  const dispatch = useDispatch();

  useEffect(() => {
    trackNavigatedToPage('Cancel Funnel', 'Reason');
    const userProgress = {
      cancelStep: 2,
    };
    dispatch(showUserCancellationProgress(userProgress));
  }, [dispatch]);

  const submitSurvey = (evt) => {
    evt.preventDefault();
    const surveyForm = evt.target;
    trackEvent('Submitted Cancel Survey', {
      reason: surveyForm.leaving_reason.value,
      feeling: surveyForm.leaving_feeling.value,
      location: 'cancel-funnel-reason',
    });
    setTimeout(() => {
      router.push(
        `/cancel/verify?subscriptionId=${subscriptionId}`,
        formatUrlDisplay(`/cancel/${subscriptionId}/verify`, '/cancel/verify'),
      );
    }, 200);
  };

  return (
    <div>
      <h4 className="cancel-survey__title">{text.title_survey}</h4>
      <form className="cancel-survey__container" onSubmit={submitSurvey} action="">
        <fieldset className="cancel-survey__container--reasons">
          {reasons.map((reason, id) => (
            <label key={`reason_${reason}`} htmlFor={`reason_${id}`}>
              <input
                type="radio"
                name="leaving_reason"
                value={reason}
                id={`reason_${id}`}
                required
              />
              {reason}
            </label>
          ))}
        </fieldset>
        <fieldset className="cancel-survey__container--feelings">
          <div className="cancel-survey__container--legend">{text.survey_feeling_prompt}</div>
          {feelings.map((feeling, id) => (
            <label key={`feeling_${feeling}`} htmlFor={`feeling_${id}`}>
              <input
                type="radio"
                name="leaving_feeling"
                value={feeling}
                id={`feeling_${id}`}
                required
              />
              {feeling}
            </label>
          ))}
        </fieldset>
        <div>
          <Button style={{ width: '100%' }} button={{ type: 'submit' }}>
            {text.action_survey_continue}
          </Button>
          <TrackableLink
            ref={linkRef}
            eventName="Clicked No Thanks"
            eventProperties={skipSurveyEventProperties}
          >
            <Link
              href={{ pathname: '/cancel/verify', query: { subscriptionId } }}
              prefetch
              as={formatUrlDisplay(`/cancel/${subscriptionId}/verify`, '/cancel/verify')}
            >
              <a className="cancel-link quiver-button--ghost-shade" ref={linkRef}>
                {text.action_survey_skip}
              </a>
            </Link>
          </TrackableLink>
        </div>
      </form>
    </div>
  );
};

export default CancelSurvey;
