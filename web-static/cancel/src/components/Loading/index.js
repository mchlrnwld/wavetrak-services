import { Spinner } from '@surfline/quiver-react';
import './Spinner.scss';

const Loading = () => (
  <div className="sl-cancel-loading">
    <Spinner />
  </div>
);

export default Loading;
