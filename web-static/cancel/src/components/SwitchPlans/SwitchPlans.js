import { useSelector, shallowEqual } from 'react-redux';
import { addMonths, format } from 'date-fns';
import Link from 'next/link';
import { Button } from '@surfline/quiver-react';
import { convertToMonthlyCost } from '@surfline/web-common';
import {
  getSubscription,
  getOfferedPlans,
  getPlanChangeStatus,
} from '../../store/selectors/subscription';
import { formatUrlDisplay } from '../../utils/urlHelper';
import en from '../../international/translations/en';
import './SwitchPlans.scss';

const parseOfferedPlan = (offeredPlan) => {
  const { amount, interval, symbol } = offeredPlan;
  const monthlyCost = convertToMonthlyCost(amount, interval);
  const fixedMonthlyAmount = parseFloat(monthlyCost / 100).toFixed(2);
  const numberString = fixedMonthlyAmount;
  const decimalIndex = numberString.indexOf('.');
  const dollars = numberString.substring(0, decimalIndex > -1 ? decimalIndex : numberString.length);
  const cents = decimalIndex > -1 ? numberString.substring(decimalIndex + 1) : '';
  const renewalDate = format(addMonths(new Date(), 1), 'MM/dd/yyyy');
  return { fixedMonthlyAmount, renewalDate, dollars, cents, symbol };
};
const SwitchPlans = () => {
  const text = en.cancellation_funnel;
  const subscription = useSelector(getSubscription, shallowEqual);
  const offeredPlans = useSelector(getOfferedPlans, shallowEqual);
  const { error } = useSelector(getPlanChangeStatus, shallowEqual);
  const { subscriptionId } = subscription;
  const [offeredPlan] = offeredPlans;
  const { dollars, cents, symbol } = parseOfferedPlan(offeredPlan);
  return (
    <div>
      <h4>{en.cancellation_funnel.meta_title_switch_plans}</h4>
      <div className="switch-plans__description">{text.switch_plans_description}</div>
      <div className="switch-plans__container">
        <div className="switch-plans__offer">
          <div className="switch-plans__name">{text.switch_plans_available_plan_text}</div>
          <div className="switch-plans__price">
            <div className="switch-plans__currency">{symbol}</div>
            <div className="switch-plans__dollars">{dollars}</div>
            <div className="switch-plans__price__small">
              <div className="switch-plans__cents">{cents}</div>
              <div className="switch-plans__interval">/mo</div>
            </div>
          </div>
        </div>
        <div className="switch-plans__credit">{text.switch_plans_credit_text}</div>
      </div>
      {error ? <div className="switch-plans_error__alert">{error}</div> : null}
      <div>
        <Link
          href={{ pathname: '/cancel/verify', query: { subscriptionId } }}
          prefetch
          as={formatUrlDisplay(`/cancel/${subscriptionId}/verify`, '/cancel/verify')}
        >
          <a className="cancel-link">
            <Button type="ghost-shade">{text.switch_plans_action_skip}</Button>
          </a>
        </Link>
      </div>
    </div>
  );
};
export default SwitchPlans;
