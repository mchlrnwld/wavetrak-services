import { useEffect, useRef } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import Link from 'next/link';
import { Button, TrackableLink } from '@surfline/quiver-react';
import {
  brandToString,
  daysFromNowUntil,
  formatDateToMMDDYY,
  trackEvent,
  trackNavigatedToPage,
} from '@surfline/web-common';
import { showUserCancellationProgress } from '../../store/actions/user';
import { getSubscription } from '../../store/selectors/subscription';

import { formatUrlDisplay } from '../../utils/urlHelper';
import en from '../../international/translations/en';
import getConfig from '../../config';

import './CancelLanding.scss';

const { brand } = getConfig();

const keepAccount = () => {
  trackEvent('Kept Account', { location: 'cancel-funnel-landing-page' }, {});

  setTimeout(() => window.location.assign('/account/subscription'), 300);
};

const cancelSubscriptionProperties = { location: 'cancel-funnel-landing-page' };

const Landing = () => {
  const linkRef = useRef();
  const subscription = useSelector(getSubscription, shallowEqual);
  const dispatch = useDispatch();

  useEffect(() => {
    trackNavigatedToPage('Cancel Funnel', {}, 'Landing Page');
    const userProgress = {
      cancelStep: 1,
    };
    dispatch(showUserCancellationProgress(userProgress));
  }, [dispatch]);

  const { trialing, subscriptionId } = subscription;
  const copy = trialing ? en.cancellation_funnel.freetrial_landing : en.cancellation_funnel.landing;
  return (
    <div>
      <h4>{copy.title}</h4>
      <hr />
      {trialing ? (
        <div>
          <div className="cancel-landing__free-trial-text">
            {copy.free_trial_text(daysFromNowUntil(subscription.end * 1000), brandToString(brand))}
          </div>
          <div className="cancel-landing__free-trial-billing-info">
            {copy.free_trial_billing_info(
              formatDateToMMDDYY(subscription.end * 1000),
              brandToString(),
            )}
          </div>
        </div>
      ) : null}
      <div>
        <Button onClick={keepAccount} type="keep-premium" style={{ outline: 'none' }}>
          {en.cancellation_funnel.action_keep_account}
        </Button>

        <TrackableLink
          ref={linkRef}
          eventName="Continued To Cancel"
          eventProperties={cancelSubscriptionProperties}
        >
          <Link
            href={{ pathname: '/cancel/survey', query: { subscriptionId } }}
            prefetch
            as={formatUrlDisplay(`/cancel/${subscriptionId}/survey`, '/cancel/survey')}
          >
            <a ref={linkRef} className="cancel-link quiver-button--ghost-shade">
              {en.cancellation_funnel.action_continue}
            </a>
          </Link>
        </TrackableLink>
      </div>
    </div>
  );
};

export default Landing;
