/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable jsx-a11y/control-has-associated-label */

import getConfig from '../../config';
import './RailLogo.scss';

const config = getConfig();

const RailLogo = () => {
  const { redirectUrl } = config;

  return <a href={redirectUrl} className="rail-logo" />;
};
export default RailLogo;
