import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { trackNavigatedToPage } from '@surfline/web-common';
import { SwitchPlans, OfferConfirmation } from '..';
import { getPlanChangeStatus, getSubscription } from '../../store/selectors/subscription';
import { getUser } from '../../store/selectors/user';
import { showUserCancellationProgress } from '../../store/actions/user';

const CancelOffer = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { planChanged, upcomingInvoice } = useSelector(getPlanChangeStatus, shallowEqual);
  const subscription = useSelector(getSubscription, shallowEqual);
  const user = useSelector(getUser, shallowEqual);
  const { subscriptionId, onTrial } = subscription;

  useEffect(
    () => {
      const plan = subscription.stripePlanId || subscription.planId;
      if (onTrial || plan.indexOf('annual') === -1) {
        /**
         * Do not show this page for all users that are
         * in Free Trial or not an Annual subscriber.
         */
        router.push(
          `/cancel/verify?subscriptionId=${subscriptionId}`,
          `/cancel/${subscriptionId}/verify`,
        );
      }
      trackNavigatedToPage('Cancel Funnel', 'Cancel Offer');
      const userProgress = {
        cancelStep: 3,
      };
      dispatch(showUserCancellationProgress(userProgress));
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return (
    <div>
      {planChanged ? (
        <OfferConfirmation upcomingInvoice={upcomingInvoice} user={user} />
      ) : (
        <SwitchPlans />
      )}
    </div>
  );
};
export default CancelOffer;
