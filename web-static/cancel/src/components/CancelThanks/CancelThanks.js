/* eslint-disable react/no-danger */
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { trackNavigatedToPage } from '@surfline/web-common';
import { showUserCancellationProgress } from '../../store/actions/user';
import en from '../../international/translations/en';

const CancelThanks = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const userProgress = {
      cancelStep: 4,
    };
    dispatch(showUserCancellationProgress(userProgress));
    trackNavigatedToPage('Cancel Funnel', 'Redirect');
  }, [dispatch]);

  return (
    <div>
      <h4>{en.cancellation_funnel.title_thanks}</h4>
      <p
        className="cancel-thanks__message"
        dangerouslySetInnerHTML={{ __html: en.cancellation_funnel.thanks_message_redirect }}
      />
    </div>
  );
};
export default CancelThanks;
