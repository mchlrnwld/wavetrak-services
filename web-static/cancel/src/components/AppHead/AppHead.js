import PropTypes from 'prop-types';
import Head from 'next/head';
import { Cookies } from 'react-cookie';
import { snippet as segment, resetAnonymousIdCookieSnippet } from '@surfline/web-common';
import { NewRelicBrowser } from '@surfline/quiver-react';

const cookie = new Cookies();

const AppHead = ({ config }) => {
  const { newRelic } = config;
  const shouldResetAnonymousIdCookie = !cookie.get('sl_reset_anonymous_id_v3');
  return (
    <Head>
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="description" content="Cancel Funnel" />
      {shouldResetAnonymousIdCookie ? (
        <script
          dangerouslySetInnerHTML={{
            __html: resetAnonymousIdCookieSnippet(false),
          }}
        />
      ) : null}
      <script
        type="text/javascript"
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{ __html: segment(config.segment, false) }}
      />
      <NewRelicBrowser licenseKey={newRelic.licenseKey} applicationID={newRelic.applicationID} />
    </Head>
  );
};
AppHead.propTypes = {
  config: PropTypes.shape({
    segment: PropTypes.string.isRequired,
    newRelic: PropTypes.shape({
      licenseKey: PropTypes.string.isRequired,
      applicationID: PropTypes.string.isRequired,
    }),
  }).isRequired,
};
export default AppHead;
