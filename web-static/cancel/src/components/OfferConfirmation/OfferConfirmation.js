import { format } from 'date-fns';
import PropTypes from 'prop-types';
import { Button } from '@surfline/quiver-react';
import { amountToFixed, brandToString } from '@surfline/web-common';
import en from '../../international/translations/en';
import getConfig from '../../config';
import './OfferConfirmation.scss';

const { brand } = getConfig();

const OfferConfirmation = ({ upcomingInvoice, user }) => {
  const text = en.cancellation_funnel.offer_confirmation;
  const { email } = user;
  const {
    amountDue,
    plan: { amount, interval, symbol },
  } = upcomingInvoice;
  const date = format(new Date(), 'MM/dd/yyyy');
  const chargeAmount = amountToFixed(amountDue);
  const renewalAmount = amountToFixed(amount);
  return (
    <div className="offer-confirmation">
      <h4>{text.title}</h4>
      <div className="offer-confirmation__charge">{text.charge(chargeAmount, date, symbol)}</div>
      <div className="offer-confirmation__renewal">
        {text.renewal(brandToString(brand), renewalAmount, interval, symbol)}
      </div>
      <div className="offer-confirmation__email-notification">{text.notification(email)}</div>
      <a
        href="/"
        className="button-link offer-confirmation__button"
        style={{ textDecoration: 'none' }}
      >
        <Button
          style={{
            width: '100%',
            backgroundColor: '#23d737',
            borderColor: '#23d737',
          }}
        >
          {text.buttonText(brandToString(brand))}
        </Button>
      </a>
    </div>
  );
};

OfferConfirmation.defaultProps = {
  upcomingInvoice: {
    amountDue: null,
    plan: null,
  },
  user: {
    email: null,
  },
};
OfferConfirmation.propTypes = {
  upcomingInvoice: PropTypes.shape({
    amountDue: PropTypes.number,
    plan: PropTypes.string,
  }),
  user: PropTypes.shape({
    email: PropTypes.string,
  }),
};
export default OfferConfirmation;
