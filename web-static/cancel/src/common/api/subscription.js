import fetch from '../../utils/fetch';

export const getOfferedPlans = (accessToken, brand) => {
  const urlPath = `/subscriptions/cancellation-offer?brand=${brand}`;
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const getSubscription = (accessToken, brand) => {
  const urlPath = `/subscriptions/billing?product=${brand}`;
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const deleteSubscription = (subscriptionId, accessToken) =>
  fetch(`/subscriptions/billing`, accessToken, {
    method: 'DELETE',
    body: JSON.stringify({ subscriptionId }),
  });

export const updateSubscription = (accessToken, subInfo) =>
  fetch('/subscriptions', accessToken, {
    method: 'PUT',
    body: JSON.stringify(subInfo),
  });
