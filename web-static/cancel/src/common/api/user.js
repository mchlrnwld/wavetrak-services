import fetch from '../../utils/fetch';

export const getUser = (accessToken) => fetch('user', accessToken, { method: 'GET' });

export default getUser;
