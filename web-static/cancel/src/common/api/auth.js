import fetch from '../../utils/fetch';
import getConfig from '../../config';

const { clientId, clientSecret } = getConfig();

export const signIn = (values, isShortLived = false) => {
  const userInfo = {
    grant_type: 'password',
    ...values,
  };
  let formBody = [];
  Object.keys(userInfo).forEach((property) => {
    if (property !== null) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(userInfo[property]);
      formBody.push(`${encodedKey}=${encodedValue}`);
    }
  });
  formBody = formBody.join('&');
  return fetch(
    `/trusted/token?isShortLived=${isShortLived}`,
    null,
    {
      method: 'POST',
      headers: {
        accept: 'application/json',
        Authorization: `Basic ${Buffer.from(`${clientId}:${clientSecret}`).toString('base64')}`,
        'content-type': 'application/x-www-form-urlencoded',
        credentials: 'same-origin',
      },
      body: formBody,
    },
    'application/x-www-form-urlencoded',
  );
};

export default signIn;
