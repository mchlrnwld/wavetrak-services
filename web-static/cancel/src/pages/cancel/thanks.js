import Head from 'next/head';
import dynamic from 'next/dynamic';
import Loading from '../../components/Loading';

const CancelThanks = dynamic(import('../../components/CancelThanks/CancelThanks'), {
  loading: () => <Loading />,
  ssr: false,
});

const Thanks = (props) => (
  <div className="sl-cancel-route">
    <Head>
      <title>Cancel Thanks</title>
    </Head>
    <CancelThanks {...props} />
  </div>
);

export default Thanks;
