import Head from 'next/head';
import dynamic from 'next/dynamic';
import Loading from '../../components/Loading';

const CancelLanding = dynamic(import('../../components/CancelLanding/CancelLanding'), {
  loading: () => <Loading />,
  ssr: false,
});

const Cancel = (props) => (
  <div className="sl-cancel-route">
    <Head>
      <title>Cancel Landing</title>
    </Head>
    <CancelLanding {...props} />
  </div>
);

export default Cancel;
