import Head from 'next/head';
import dynamic from 'next/dynamic';
import Loading from '../../components/Loading';

const CancelOffer = dynamic(import('../../components/CancelOffer/CancelOffer'), {
  loading: () => <Loading />,
  ssr: false,
});

const Offer = (props) => (
  <div className="sl-cancel-route">
    <Head>
      <title>Cancel Offer Step</title>
    </Head>
    <CancelOffer {...props} />
  </div>
);

export default Offer;
