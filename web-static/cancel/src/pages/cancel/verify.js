import Head from 'next/head';
import dynamic from 'next/dynamic';
import Loading from '../../components/Loading';

const CancelVerify = dynamic(import('../../components/CancelVerify/CancelVerify'), {
  loading: () => <Loading />,
  ssr: false,
});

const Cancel = (props) => (
  <div className="sl-cancel-route">
    <Head>
      <title>Cancel Verify Step</title>
    </Head>
    <CancelVerify {...props} />
  </div>
);

export default Cancel;
