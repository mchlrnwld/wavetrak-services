import Head from 'next/head';
import dynamic from 'next/dynamic';
import Loading from '../../components/Loading';

const CancelSurvey = dynamic(import('../../components/CancelSurvey/CancelSurvey'), {
  loading: () => <Loading />,
  ssr: false,
});

const Survey = (props) => (
  <div className="sl-cancel-route">
    <Head>
      <title>Cancel Survey Step</title>
    </Head>
    <CancelSurvey {...props} />
  </div>
);

export default Survey;
