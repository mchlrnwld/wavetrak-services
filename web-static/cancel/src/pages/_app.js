/* eslint-disable react/jsx-props-no-spreading */
import { useState } from 'react';
import PropTypes from 'prop-types';
import { useMount } from 'react-use';
import { Provider } from 'react-redux';
import { withCookies } from 'react-cookie';
import { createSplitFilter, setupFeatureFramework } from '@surfline/web-common';

import * as treatments from '../utils/treatments';
import getConfig from '../config';
import AppHead from '../components/AppHead/AppHead';
import CancelLayout from '../components/CancelLayout/CancelLayout';
import { getUrlSubscriptionId } from '../utils/urlHelper';
import store from '../store';
import { fetchUser } from '../store/actions/user';
import { fetchSubscription } from '../store/actions/subscription';
import { getUser } from '../store/selectors/user';
import Loading from '../components/Loading';

/*
_app.js - rendered both on server and client
_document.js - rendered only on server
*/

const config = getConfig();

/**
 *
 * @param {object} props
 * @param {import('react-cookie').Cookies} props.cookies
 * @param {import('next/app').AppProps["Component"]} props.Component
 * @param {import('next/app').AppProps["pageProps"]} props.pageProps
 * @returns
 */
const CancelApp = ({ cookies, Component, pageProps }) => {
  const [subscriptionId, setSubscriptionId] = useState();
  const [appReady, setAppReady] = useState(false);

  useMount(async () => {
    const accessToken = cookies.get('access_token');
    const subId = getUrlSubscriptionId();

    if (!subId || !accessToken) {
      window.location.assign(config.redirectUrl);
    }

    setSubscriptionId(subId);
    await store.dispatch(fetchUser());

    const user = getUser(store.getState());

    try {
      await setupFeatureFramework({
        authorizationKey: config.splitio,
        user,
        splitFilters: createSplitFilter(treatments),
      });
    } finally {
      await Promise.all([fetchSubscription].map((action) => store.dispatch(action())));
      setAppReady(true);
    }
  });

  const componentProps = {
    ...pageProps,
    subscriptionId,
  };

  return (
    <Provider store={store}>
      <CancelLayout appReady={appReady}>
        <AppHead config={config} />
        {appReady ? <Component {...componentProps} /> : <Loading />}
      </CancelLayout>
    </Provider>
  );
};

CancelApp.propTypes = {
  cookies: PropTypes.shape({
    get: PropTypes.func.isRequired,
  }).isRequired,
  pageProps: PropTypes.shape({}).isRequired,
  Component: PropTypes.node.isRequired,
};

export default withCookies(CancelApp);
