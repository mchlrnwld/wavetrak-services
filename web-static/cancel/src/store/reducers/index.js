import { combineReducers } from 'redux';

import user from './user';
import subscription from './subscription';

export default combineReducers({
  user,
  subscription,
});
