import createReducer from './createReducer';
import {
  FETCH_SUBSCRIPTION,
  FETCH_SUBSCRIPTION_SUCCESS,
  FETCH_SUBSCRIPTION_FAILURE,
  VERIFY_SUBSCRIPTION,
  VERIFY_SUBSCRIPTION_SUCCESS,
  VERIFY_SUBSCRIPTION_FAILURE,
  CHANGE_PLANS,
  CHANGE_PLANS_SUCCESS,
  CHANGE_PLANS_FAILURE,
} from '../actions/subscription';

const initialState = {
  error: null,
  deleted: false,
  loading: false,
  planChanged: false,
  currentSubscriptionForBrand: null,
  upcomingInvoice: null,
};
const handlers = {};

handlers[FETCH_SUBSCRIPTION] = (state) => {
  return {
    ...state,
    loading: true,
  };
};

handlers[FETCH_SUBSCRIPTION_SUCCESS] = (state, { subscription }) => {
  return {
    ...state,
    subscription,
    error: null,
    loading: false,
  };
};

handlers[FETCH_SUBSCRIPTION_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});

handlers[VERIFY_SUBSCRIPTION] = (state) => ({
  ...state,
  loading: true,
});

handlers[VERIFY_SUBSCRIPTION_SUCCESS] = (state) => {
  return {
    ...state,
    error: null,
    loading: false,
    deleted: true,
  };
};

handlers[VERIFY_SUBSCRIPTION_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
  deleted: false,
});

handlers[CHANGE_PLANS] = (state) => ({
  ...state,
  loading: true,
  planChanged: false,
});

handlers[CHANGE_PLANS_SUCCESS] = (state, { currentSubscriptionForBrand, upcomingInvoice }) => ({
  ...state,
  currentSubscriptionForBrand,
  upcomingInvoice,
  error: null,
  loading: false,
  planChanged: true,
});

handlers[CHANGE_PLANS_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
  planChanged: false,
});

export default createReducer(handlers, initialState);
