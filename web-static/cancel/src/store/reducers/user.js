import createReducer from './createReducer';
import {
  FETCH_USER,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
  USER_CANCELLATION_PROGRESS,
} from '../actions/user';

const initialState = {
  error: null,
  loading: false,
  details: null,
};
const handlers = {};

handlers[FETCH_USER] = (state) => {
  return {
    ...state,
    loading: true,
  };
};

handlers[FETCH_USER_SUCCESS] = (state, { user }) => {
  return {
    ...state,
    details: {
      ...user,
    },
    error: null,
    loading: false,
  };
};

handlers[FETCH_USER_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});

handlers[USER_CANCELLATION_PROGRESS] = (state, { userProgress }) => ({
  ...state,
  ...userProgress,
});

export default createReducer(handlers, initialState);
