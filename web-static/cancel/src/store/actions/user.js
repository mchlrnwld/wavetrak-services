import { Cookies } from 'react-cookie';
import { getUser } from '../../common/api/user';

export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';
export const USER_CANCELLATION_PROGRESS = 'USER_CANCELLATION_PROGRESS';

const cookies = new Cookies();

export const fetchUser = () => async (dispatch) => {
  try {
    dispatch({ type: FETCH_USER });
    const accessToken = cookies.get('access_token');
    const user = await getUser(accessToken);
    return dispatch({ type: FETCH_USER_SUCCESS, user });
  } catch (err) {
    const { message } = err;
    return dispatch({ type: FETCH_USER_FAILURE, message });
  }
};

export const showUserCancellationProgress = (userProgress) => async (dispatch) => {
  return dispatch({ type: USER_CANCELLATION_PROGRESS, userProgress });
};
