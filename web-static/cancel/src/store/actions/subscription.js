import { Cookies } from 'react-cookie';
import { deleteSubscription, getSubscription } from '../../common/api/subscription';
import getConfig from '../../config';
import { signIn } from '../../common/api/auth';

export const FETCH_SUBSCRIPTION = 'FETCH_SUBSCRIPTION';
export const FETCH_SUBSCRIPTION_SUCCESS = 'FETCH_SUBSCRIPTION_SUCCESS';
export const FETCH_SUBSCRIPTION_FAILURE = 'FETCH_SUBSCRIPTION_FAILURE';
export const VERIFY_SUBSCRIPTION = 'VERIFY_SUBSCRIPTION';
export const VERIFY_SUBSCRIPTION_SUCCESS = 'VERIFY_SUBSCRIPTION_SUCCESS';
export const VERIFY_SUBSCRIPTION_FAILURE = 'VERIFY_SUBSCRIPTION_FAILURE';
export const CHANGE_PLANS = 'CHANGE_PLANS';
export const CHANGE_PLANS_SUCCESS = 'CHANGE_PLANS_SUCCESS';
export const CHANGE_PLANS_FAILURE = 'CHANGE_PLANS_FAILURE';
export const APP_READY = 'APP_READY';

const cookies = new Cookies();
const { brand } = getConfig();
export const fetchSubscription = () => async (dispatch) => {
  try {
    dispatch({ type: FETCH_SUBSCRIPTION });
    const accessToken = cookies.get('access_token');
    const subscription = await getSubscription(accessToken, brand);
    dispatch({ type: APP_READY });
    return dispatch({ type: FETCH_SUBSCRIPTION_SUCCESS, subscription });
  } catch (err) {
    const { message } = err;
    return dispatch({ type: FETCH_SUBSCRIPTION_FAILURE, message });
  }
};

export const verifyAndDeleteSubscription = (email, password, subscription) => async (dispatch) => {
  try {
    dispatch({ type: VERIFY_SUBSCRIPTION });
    const values = {
      username: email,
      password,
    };
    await signIn(values, true);
    const accessToken = cookies.get('access_token');
    const { subscriptionId } = subscription;
    await deleteSubscription(subscriptionId, accessToken);
    return dispatch({ type: VERIFY_SUBSCRIPTION_SUCCESS });
  } catch (err) {
    const { error_description: message } = err;
    return dispatch({ type: VERIFY_SUBSCRIPTION_FAILURE, message });
  }
};
