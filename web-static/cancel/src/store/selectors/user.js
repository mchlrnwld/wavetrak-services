export const getUser = (state) => {
  if (state) {
    const { user } = state;
    return user;
  }
  return null;
};

export const getUserEmail = (state) => state.user?.details?.email;

export const getUserCancellationProgress = (state) => {
  if (state) {
    const {
      user: { cancelStep },
    } = state;
    return { cancelStep };
  }
  return null;
};
