export const getSubscription = (state) => {
  if (state) {
    const {
      subscription: { subscription },
    } = state;
    return subscription;
  }
  return null;
};

export const getOfferedPlans = (state) => {
  if (state) {
    const {
      subscription: { offeredPlans },
    } = state;
    return offeredPlans;
  }
  return null;
};

export const getCancellationStatus = (state) => {
  if (state) {
    const {
      subscription: { deleted, loading, error },
    } = state;
    return { deleted, loading, error };
  }
  return null;
};

export const getPlanChangeStatus = (state) => {
  if (state) {
    const {
      subscription: { planChanged, upcomingInvoice, loading, error },
    } = state;
    return { planChanged, upcomingInvoice, loading, error };
  }
  return null;
};
