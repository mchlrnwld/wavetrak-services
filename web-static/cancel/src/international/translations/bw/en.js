import { SwellLinesIcon, PremiumAnalysisWindIcon, BuoyIcon } from '@surfline/quiver-react';
import base from '../base';

export default {
  rail_cta: {
    ...base.rail_cta.bw,
  },
  premiumFeatures: {
    features: [
      {
        Icon: SwellLinesIcon,
        header: 'Marine Forecasts For Any Point',
        description: 'View accurate 16-day wind, wave and tide forecasts.',
      },
      {
        Icon: PremiumAnalysisWindIcon,
        header: 'High-Resolution Wind Forecasts',
        description: 'Access a full collection of weather charts and forecasts.',
      },
      {
        Icon: BuoyIcon,
        header: 'Pick Your Weather Window',
        description: 'Long-range forecasts help you stay safe on the water.',
      },
    ],
  },
  cancellation_funnel: {
    ...base.cancellation_funnel,
    landing: {
      title:
        'Premium members get long-range marine forecasts. Are you sure you want to give them up?',
      meta_title: 'Cancel your Subscription',
      meta_description: 'Buoyweather offers the latest 16-day marine forecasts.',
    },
    meta_title: 'Cancel your subscription - Buoyweather',
    meta_title_landing: 'Cancel your Subscription',
    meta_description_landing: 'Buoyweather offers the latest 16-day marine forecasts.',
    meta_title_survey: 'Why are you unsubscribing? - Buoyweather',
    meta_title_verify: 'Verify your Buoyweather password',
    title:
      'Premium members get long-range marine forecasts. Are you sure you want to give them up?',
    title_thanks: 'Thanks for submitting your reason for leaving Buoyweather.',
    thanks_message_redirect:
      'If you are not redirected to buoyweather.com in the next 30 seconds, please click <a href="/">here</a> to head back to buoyweather.com',
    list_reason_1_heading: '16 DAY MARINE WEATHER FORECASTS',
    list_reason_1_sub: 'Know what to expect when planning your trip.',
    list_reason_2_heading: 'HIGH-RESOLUTION WIND FORECASTS',
    list_reason_2_sub: 'View local conditions before setting sail.',
    list_reason_3_heading: 'STAY SAFE ON THE WATER',
    list_reason_3_sub: 'Ensure the protection of life and property.',
    survey_reasons: {
      ...base.cancellation_funnel.survey_reasons,
    },
    survey_feelings: {
      ...base.cancellation_funnel.survey_feelings,
    },
    survey_feeling_prompt: 'Buoyweather left me feeling:',
  },
  header: 'Buoyweather',
};
