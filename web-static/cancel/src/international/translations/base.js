import { WaveIcon } from '@surfline/quiver-react';

export default {
  rail_cta: {
    sl: {
      header: 'Premium Benefits',
      mobileHeader: 'Go Premium',
      mobileSubtitle: 'Learn More About Surfline Premium',
      MobileIcon: WaveIcon,
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
    },
    bw: {
      mobileHeader: 'Go Premium',
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
      header: 'Buoyweather',
    },
    fs: {
      mobileHeader: 'Go Premium',
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
      header: 'FishTrack',
    },
  },
  cancellation_funnel: {
    landing: {
      title: 'Premium members score better waves, more often. Are you sure you want to miss out?',
      meta_title: 'Cancel your Surfline subscription',
      meta_description: '',
    },
    freetrial_landing: {
      title: 'Keep your Premium benefits',
      meta_title: 'Cancel your Premium subscription',
      meta_description: '',
      free_trial_text: (days, brandName) =>
        `You have ${days} days remaining on your free trial. If you cancel now, you will immediately lose access to ${brandName} Premium.`,
      free_trial_billing_info: (trialEnd, brandName) =>
        `Billing for your recurring ${brandName} Premium subscription will begin once your free trial ends on ${trialEnd}.`,
    },
    meta_title: 'Cancel your subscription - Surfline',
    meta_title_landing: 'Cancel your Surfline subscription',
    meta_description_landing: '',
    meta_title_survey: 'Why are you unsubscribing?',
    meta_title_thanks: 'Your subscription has been cancelled',
    meta_title_verify: 'Verify your Surfline password',
    title: 'Premium members score better waves, more often. Are you sure you want to miss out?',
    title_verify_password: 'Enter your password to confirm you are a human.',
    title_survey: 'Why do you want to cancel Surfline Premium?',
    title_thanks: 'Your premium membership has now been canceled.',
    thanks_message_redirect:
      'We\'re sorry to see you go. If you are not redirected to Surfline within the next 30 seconds, please click <a href="/">here</a>.',
    list_reason_1_heading: 'EXPERT FORECAST TEAM',
    list_reason_1_sub:
      'Our Surfline forecast team is dedicated to getting you on the best waves around.',
    list_reason_2_heading: 'AD-FREE HD CAMS',
    list_reason_2_sub:
      'Over 300 live streaming HD cams, available on any device, helping you check the surf faster.',
    list_reason_3_heading: "TRUSTED BY THE WORLD'S BEST",
    list_reason_3_sub:
      'Used to anticipate swells by surfers around the globe, including the World Surf League.',
    action_keep_account: 'KEEP ACCOUNT',
    action_continue: 'CONTINUE TO CANCEL',
    action_survey_skip: 'SKIP',
    action_survey_continue: 'CONTINUE',
    action_password_continue: 'YES, I WANT TO CANCEL',
    action_password_keep_account: (brandName) => `KEEP ${brandName} PREMIUM`,
    survey_reasons: {
      technical: 'I have a technical problem I need help to solve.',
      beneficial: "I don't see the benefit of Premium products.",
      cost: 'Premium service is too expensive for me.',
      competition: 'I use other websites or services now.',
      experience: 'Frustrating experience.',
      account: 'I am a Premium member under a different email.',
    },
    survey_feeling_prompt: 'Surfline left me feeling:',
    survey_feelings: {
      sad: 'Sad',
      happy: 'Happy',
      angry: 'Angry',
      indifferent: 'Indifferent',
      confused: 'Confused',
    },
    meta_title_switch_plans: 'Switch to Monthly Billing',
    switch_plans_title: 'Switch to Monthly Billing',
    switch_plans_description: 'Keep Surfline Premium and reduce the cost of your next bill.',
    switch_plans_sub_text: 'SWITCH TO MONTHLY',
    switch_plans_available_plan_text: 'SURFLINE PREMIUM',
    switch_plans_credit_text:
      'You will be credited the remainder of your annual subscription. This credit will be applied to any future charges.',
    switch_plans_credit_invoice_text: (planAmount, renewalDate) =>
      `You will be charged $${planAmount} on ${renewalDate}. `,
    switch_plans_credit_invoice_sub_text:
      'Thereafter, your Surfline Premium Membership will renew each month at the same rate.',
    switch_plans_button: 'YES, SWITCH TO MONTHLY',
    switch_plans_action_skip: 'NO THANKS',
    offer_confirmation: {
      title: "You're all set!",
      charge: (price, date, symbol) => `You will be charged ${symbol}${price} on ${date}.`,
      renewal: (brand, price, interval, symbol) =>
        `Thereafter, your ${brand} Premium subscription will renew each ${interval} at ${symbol}${price}.`,
      notification: (email) => `A confirmation email has been sent to ${email}.`,
      buttonText: (brand) => `back to ${brand}`,
    },
  },
};
