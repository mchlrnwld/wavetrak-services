import { CamsIcon, AnalysisIcon, ChartsIcon, CamRewindIcon } from '@surfline/quiver-react';
import base from '../base';

export default {
  rail_cta: {
    ...base.rail_cta.sl,
  },
  premiumFeatures: {
    features: [
      {
        Icon: CamsIcon,
        header: 'Ad-Free and Premium Cams',
        description: 'Watch live conditions ad-free -- and access exclusive, high-resolution cams.',
      },
      {
        Icon: AnalysisIcon,
        header: 'Trusted Forecast Team',
        description: 'Plan your next trip or session with Surfline’s expert forecast guidance.',
      },
      {
        Icon: ChartsIcon,
        header: 'Exclusive Forecast Tools',
        description: 'Access curated condition reports and proprietary surf, wind and buoy data.',
      },
      {
        Icon: CamRewindIcon,
        header: 'Cam Rewind',
        description:
          'Relive an epic session by downloading your rides -- then share ‘em with friends.',
      },
    ],
  },
  cancellation_funnel: {
    ...base.cancellation_funnel,
    survey_reasons: {
      ...base.cancellation_funnel.survey_reasons,
    },
    survey_feelings: {
      ...base.cancellation_funnel.survey_feelings,
    },
  },
  header: 'Surfline',
};
