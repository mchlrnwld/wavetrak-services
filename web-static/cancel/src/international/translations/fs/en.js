import { SatelliteIcon, WaypointIcon, LayersIcon } from '@surfline/quiver-react';
import base from '../base';

export default {
  rail_cta: {
    ...base.rail_cta.fs,
  },
  premiumFeatures: {
    features: [
      {
        Icon: SatelliteIcon,
        header: 'Latest Satellite Imagery',
        description: 'Locate temperature and color breaks offshore.',
      },
      {
        Icon: WaypointIcon,
        header: 'Waypoints And Routes',
        description: 'Measure distances and plot routes for trips offshore.',
      },
      {
        Icon: LayersIcon,
        header: 'Currents, Bathymetry And Altimetry',
        description: 'Use premium overlays to pinpoint areas of activity.',
      },
    ],
  },
  cancellation_funnel: {
    ...base.cancellation_funnel,
    landing: {
      title:
        'Premium members have the tools to catch more fish. Are you sure you want to give them up?',
      meta_title: 'Cancel your Subscription',
      meta_description: 'FishTrack helps you find more fish with the latest satellite imagery.',
    },
    meta_title: 'Cancel your subscription - FishTrack',
    meta_title_landing: 'Cancel your Subscription',
    meta_description_landing:
      'FishTrack helps you find more fish with the latest satellite imagery.',
    meta_title_survey: 'Why are you unsubscribing? - FishTrack',
    meta_title_verify: 'Verify your password - FishTrack',
    title:
      'Premium members have the tools to catch more fish. Are you sure you want to give them up?',
    title_thanks: 'Thanks for submitting your reason for leaving FishTrack.',
    thanks_message_redirect:
      'If you are not redirected to fishtrack.com in the next 30 seconds, please click <a href="/">here</a> to head back to fishtrack.com',
    list_reason_1_heading: 'SATELLITE IMAGERY',
    list_reason_1_sub: 'View the latest SST and chlorophyll charts.',
    list_reason_2_heading: 'WAYPOINTS AND ROUTES',
    list_reason_2_sub: 'Plan your trip to save time and fuel.',
    list_reason_3_heading: 'PREMIUM OVERLAYS',
    list_reason_3_sub: 'Add bathymetry, altimetry and currents.',
    survey_reasons: {
      ...base.cancellation_funnel.survey_reasons,
    },
    survey_feelings: {
      ...base.cancellation_funnel.survey_feelings,
    },
    survey_feeling_prompt: 'FishTrack left me feeling:',
  },
  header: 'FishTrack',
};
