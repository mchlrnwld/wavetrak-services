module.exports = (config) => {
  config.set({
    browsers: ['PhantomJS'],
    frameworks: ['mocha'],
    singleRun: true,
    files: [
      { pattern: 'node_modules/babel-polyfill/browser.js', instrument: false },
      './node_modules/phantomjs-polyfill-find/find-polyfill.js',
      'tests.webpack.js'
    ],
    reporters: ['mocha'],
    preprocessors: {
      'tests.webpack.js': ['webpack', 'sourcemap']
    },
    webpack: {
      mode: 'development',
      resolve: {
        modules: ['node_modules'],
        alias: {
          sinon: 'sinon/pkg/sinon'
        }
      },
      devtool: 'inline-source-map',
      module: {
        rules: [
          {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: {
              loader: 'babel-loader',
              query: {
                presets: ['react', 'env', 'stage-2']
              }
            }
          },
          {
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
          },
          {
            test: /\.scss$/,
            use: ['style-loader', 'css-loader', 'sass-loader']
          },
          {
            test: /.(png|woff(2)?|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
            use: 'url-loader'
          },
          {
            test: /sinon.js$/,
            use: 'imports-loader?define=>false,require=>false'
          }
        ]
      }
    }
  });
};
