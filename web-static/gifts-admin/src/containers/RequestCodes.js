import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Alert } from '@surfline/quiver-react';
import RequestCodesForm from '../components/RequestCodesForm';
import RequestCodesConfirmation from '../components/RequestCodesConfirmation';
import {
  reviewRequestGiftCodes,
  resetRequestGiftCodes,
  confirmRequestGiftCodes,
} from '../actions/requestCodes';
import { history } from '../store';


const styles = {
  field: {
    marginBottom: '32px',
  },
  button: {
    minWidth: '410px',
    marginBottom: '32px',
  },
  error: {
    marginBottom: '32px',
  },
};
class RequestCodes extends Component {
  componentWillUnmount() {
    const { doResetRequestCodes } = this.props;
    doResetRequestCodes();
  }

  cancelRequestCodes = () => history.push('/gifts/');

  submitRequest = async () => {
    const {
      doConfirmRequestGiftCodes,
      requestData,
    } = this.props;
    await doConfirmRequestGiftCodes(requestData);
  };

  render() {
    const {
      loading,
      error,
      success,
      requestData,
      giftCodes,
      doReviewRequestCodes,
    } = this.props;

    return (
      <div className="request-codes">
        {error ? (
          <Alert style={styles.error} type="error">{error}</Alert>
        ) : null}
        {requestData ? (
          <RequestCodesConfirmation
            requestData={requestData}
            giftCodes={giftCodes}
            loading={loading}
            submitRequest={() => this.submitRequest()}
            cancel={() => this.cancelRequestCodes()}
          />
        ) : (
          <RequestCodesForm
            cancel={() => this.cancelRequestCodes()}
            submit={params => doReviewRequestCodes(params)}
            loading={loading}
            success={success}
          />
        )}
      </div>
    );
  }
}

RequestCodes.propTypes = {
  loading: PropTypes.bool,
  success: PropTypes.bool,
  error: PropTypes.string,
  requestData: PropTypes.shape({
    brand: PropTypes.string,
    department: PropTypes.string,
    campaign: PropTypes.string,
    product: PropTypes.string,
    total: PropTypes.string,
    prefix: PropTypes.string,
  }),
  giftCodes: PropTypes.arrayOf(
    PropTypes.shape({
      brand: PropTypes.string,
      department: PropTypes.string,
      campaign: PropTypes.string,
      product: PropTypes.string,
      redeemCode: PropTypes.string,
    })
  ),
  doReviewRequestCodes: PropTypes.func.isRequired,
  doConfirmRequestGiftCodes: PropTypes.func.isRequired,
  doResetRequestCodes: PropTypes.func.isRequired,
};

RequestCodes.defaultProps = {
  loading: false,
  success: false,
  error: null,
  requestData: null,
  giftCodes: null,
};

export default connect(
  state => ({
    loading: state.requestCodes.loading,
    success: state.requestCodes.success,
    error: state.requestCodes.error,
    requestData: state.requestCodes.requestData,
    giftCodes: state.requestCodes.giftCodes,
  }),
  dispatch => ({
    doReviewRequestCodes: formParams => dispatch(reviewRequestGiftCodes(formParams)),
    doConfirmRequestGiftCodes: requestData => dispatch(confirmRequestGiftCodes(requestData)),
    doResetRequestCodes: () => dispatch(resetRequestGiftCodes()),
  }),
)(RequestCodes);
