import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchGifts } from '../actions/gifts';
import '../styles/gifts.scss';

class Gifts extends Component {
  componentDidMount() {
    const { doFetchGifts } = this.props;
    doFetchGifts();
  }

  convertCentToDollar = cents => `$${cents / 100}`;

  render() {
    const { giftProducts, loading, error } = this.props;
    if (error) return <div>Error</div>;
    if (loading) return <div>Loading...</div>;

    return (
      <div className="gifts-container">
        <div className="gifts-container__header">
          <h4 className="gifts-container__header__title">Gift Products</h4>
        </div>
        <table className="gifts-table">
          <thead>
            <tr className="gifts-table__row">
              <th>Brand</th>
              <th>Name (dispayed)</th>
              <th>Duration (Months)</th>
              <th>Price</th>
              <th>Is Active?</th>
              <th>Best Value?</th>
              <th>Desc.</th>
              <th />
            </tr>
          </thead>
          {giftProducts ? (
            <tbody>
              {giftProducts.map(gift => (
                <div
                  className={`gifts-table__row gifts-table__row--${gift.brand}`}
                  key={gift._id}
                >
                  <tr>
                    <td>{gift.brand.toUpperCase()}</td>
                    <td>{gift.name}</td>
                    <td>{gift.duration}</td>
                    <td>{this.convertCentToDollar(gift.price)}</td>
                    <td>{gift.isActive.toString()}</td>
                    <td>{gift.bestValue.toString()}</td>
                    <td>{gift.description}</td>
                    <td className="gifts-table__action">
                      <Link to={`/gifts/edit/${gift._id}`}>Edit</Link>
                    </td>
                  </tr>
                </div>
              ))}
            </tbody>
          ) : null}
        </table>
        <Link className="gifts-container__link" to="/gifts/create">
          Create New Gift
        </Link>
        <Link className="gifts-container__link" to="/gifts/request-codes">
          Request Gift Codes
        </Link>
      </div>
    );
  }
}

Gifts.propTypes = {
  doFetchGifts: PropTypes.func.isRequired,
  giftProducts: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  loading: PropTypes.bool,
  error: PropTypes.string,
};

Gifts.defaultProps = {
  loading: false,
  error: null,
};

export default connect(
  state => ({
    giftProducts: state.gifts.giftProducts,
    loading: state.gifts.loading,
    error: state.gifts.error,
  }),
  dispatch => ({
    doFetchGifts: () => dispatch(fetchGifts()),
  }),
)(Gifts);
