import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Alert } from '@surfline/quiver-react';
import { createGift, resetGift } from '../actions/create';
import GiftForm from '../components/GiftForm';
import { history } from '../store';

const styles = {
  field: {
    marginBottom: '32px',
  },
  button: {
    minWidth: '410px',
    marginBottom: '32px',
  },
  error: {
    marginBottom: '32px',
  },
};
class Create extends Component {
  componentDidUpdate(prevProps) {
    const { prevSuccess } = prevProps;
    const { success } = this.props;
    if (!prevSuccess && success) history.push('/gifts/');
  }

  componentWillUnmount() {
    const { doResetGift } = this.props;
    doResetGift();
  }

  cancelCreate = () => history.push('/gifts/');

  render() {
    const {
      error,
      loading,
      success,
      doCreateGift,
    } = this.props;
    return (
      <div>
        <h4>Create a new Gift Product</h4>
        {error ? (
          <Alert style={styles.error} type="error">{error}</Alert>
        ) : null}
        <GiftForm
          submit={params => doCreateGift(params)}
          cancel={() => this.cancelCreate()}
          gift={{}}
          loading={loading}
          success={success}
          error={error}
        />
      </div>
    );
  }
}

Create.propTypes = {
  loading: PropTypes.bool,
  success: PropTypes.bool,
  error: PropTypes.string,
  doCreateGift: PropTypes.func.isRequired,
  doResetGift: PropTypes.func.isRequired,
};

Create.defaultProps = {
  loading: false,
  success: false,
  error: null,
};

export default connect(
  state => ({
    loading: state.create.loading,
    success: state.create.success,
    error: state.create.error,
  }),
  dispatch => ({
    doCreateGift: formParams => dispatch(createGift(formParams)),
    doResetGift: () => dispatch(resetGift()),
  }),
)(Create);
