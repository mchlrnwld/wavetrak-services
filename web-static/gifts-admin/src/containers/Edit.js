import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Alert } from '@surfline/quiver-react';
import { fetchGift, resetGift, editGift } from '../actions/edit';
import GiftForm from '../components/GiftForm';
import { history } from '../store';


const styles = {
  error: {
    marginBottom: '32px',
  },
};

class Edit extends Component {
  componentDidMount() {
    const { doFetchGift, match } = this.props;
    const { giftId } = match.params;
    doFetchGift(giftId);
  }

  componentDidUpdate(prevProps) {
    const { prevSuccess } = prevProps;
    const { success } = this.props;
    if (!prevSuccess && success) history.push('/gifts/');
  }

  componentWillUnmount() {
    const { doResetGift } = this.props;
    doResetGift();
  }

  cancelCreate = () => history.push('/gifts/');

  render() {
    const {
      giftProduct,
      loading,
      success,
      error,
      doEditGift,
    } = this.props;
    return (
      <div>
        <h4>Edit Gift</h4>
        {error ? (
          <Alert style={styles.error} type="error">{error}</Alert>
        ) : null}
        {giftProduct ? (
          <GiftForm
            submit={params => doEditGift({ ...params, id: giftProduct._id })}
            cancel={() => this.cancelCreate()}
            gift={giftProduct}
            loading={loading}
            success={success}
            error={error}
          />
        ) : null}
      </div>
    );
  }
}

Edit.propTypes = {
  giftProduct: PropTypes.shape({}),
  loading: PropTypes.bool,
  success: PropTypes.bool,
  error: PropTypes.string,
  doFetchGift: PropTypes.func.isRequired,
  doResetGift: PropTypes.func.isRequired,
  doEditGift: PropTypes.func.isRequired,
  match: PropTypes.object.isRequired,
};

Edit.defaultProps = {
  giftProduct: null,
  loading: false,
  success: false,
  error: null,
};

export default connect(
  state => ({
    giftProduct: state.edit.giftProduct,
    loading: state.edit.loading,
    success: state.edit.success,
    error: state.edit.error,
  }),
  dispatch => ({
    doFetchGift: giftId => dispatch(fetchGift(giftId)),
    doResetGift: () => dispatch(resetGift()),
    doEditGift: params => dispatch(editGift(params)),
  })
)(Edit);
