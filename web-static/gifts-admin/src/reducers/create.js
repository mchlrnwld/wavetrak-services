import createReducer from './createReducer';
import {
  CREATE_GIFT,
  CREATE_GIFT_SUCCESS,
  CREATE_GIFT_FAIL,
  RESET_GIFT,
} from '../actions/create';

const handlers = {};
const initialState = {
  loading: false,
  success: false,
  error: null,
  giftProduct: null,
};

handlers[CREATE_GIFT] = state => ({
  ...state,
  loading: true,
});

handlers[CREATE_GIFT_SUCCESS] = (state, { giftProduct }) => ({
  ...state,
  success: true,
  loading: false,
  giftProduct,
});

handlers[CREATE_GIFT_FAIL] = (state, { error }) => ({
  ...state,
  success: false,
  loading: false,
  error,
});

handlers[RESET_GIFT] = state => ({
  ...state,
  giftProduct: null,
  success: false,
});

export default createReducer(handlers, initialState);
