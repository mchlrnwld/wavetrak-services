import createReducer from './createReducer';
import {
  FETCH_GIFT,
  FETCH_GIFT_SUCCESS,
  FETCH_GIFT_FAIL,
  RESET_GIFT,
  EDIT_GIFT,
  EDIT_GIFT_SUCCESS,
  EDIT_GIFT_FAIL,
} from '../actions/edit';

const handlers = {};
const initialState = {
  loading: false,
  success: false,
  error: null,
  giftProduct: null,
};

handlers[FETCH_GIFT] = state => ({
  ...state,
});

handlers[FETCH_GIFT_SUCCESS] = (state, { giftProduct }) => ({
  ...state,
  giftProduct,
});

handlers[FETCH_GIFT_FAIL] = (state, { error }) => ({
  ...state,
  error,
});

handlers[RESET_GIFT] = state => ({
  ...state,
  giftProduct: null,
  success: false,
});

handlers[EDIT_GIFT] = state => ({
  ...state,
  loading: true,
  success: false,
});

handlers[EDIT_GIFT_SUCCESS] = (state, { giftProduct }) => ({
  ...state,
  giftProduct,
  loading: false,
  success: true,
});

handlers[EDIT_GIFT_FAIL] = (state, { error }) => ({
  ...state,
  error,
  loading: false,
  success: false,
});

export default createReducer(handlers, initialState);
