import createReducer from './createReducer';
import {
  FETCH_GIFTS,
  FETCH_GIFTS_SUCCESS,
  FETCH_GIFTS_FAIL,
} from '../actions/gifts';

const handlers = {};

const initialState = {
  loading: false,
  giftProducts: [],
  error: null,
};

handlers[FETCH_GIFTS] = state => ({
  ...state,
  loading: true,
});

handlers[FETCH_GIFTS_SUCCESS] = (state, { giftProducts }) => ({
  ...state,
  loading: false,
  giftProducts,
});

handlers[FETCH_GIFTS_FAIL] = (state, { error }) => ({
  ...state,
  error,
});

export default createReducer(handlers, initialState);
