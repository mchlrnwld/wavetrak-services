import { combineReducers } from 'redux';
import gifts from './gifts';
import create from './create';
import edit from './edit';
import requestCodes from './requestCodes';

export const reducers = combineReducers({
  gifts,
  create,
  edit,
  requestCodes,
});

export default reducers;
