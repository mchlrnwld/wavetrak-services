import createReducer from './createReducer';
import {
  REQUEST_CODES,
  REQUEST_CODES_SUCCESS,
  REQUEST_CODES_FAIL,
  REVIEW_REQUEST_CODES,
  RESET_REQUEST_CODES,
} from '../actions/requestCodes';

const handlers = {};

const initialState = {
  loading: false,
  success: false,
  error: null,
  requestData: null,
  giftCodes: null,
};

handlers[REQUEST_CODES] = state => ({
  ...state,
  loading: true,
});

handlers[REVIEW_REQUEST_CODES] = (state, { params }) => ({
  ...state,
  requestData: params,
});

handlers[REQUEST_CODES_SUCCESS] = (state, { giftCodes }) => ({
  ...state,
  loading: false,
  giftCodes,
});

handlers[REQUEST_CODES_FAIL] = (state, { error }) => ({
  ...state,
  error,
  loading: false,
  requestData: null,
  giftCodes: null,
});

handlers[RESET_REQUEST_CODES] = state => ({
  ...state,
  ...initialState,
});

export default createReducer(handlers, initialState);
