export default {
  development: {
    adminServicesEndpoint: 'https://tools.sandbox.surflineadmin.com/api',
  },
  sandbox: {
    adminServicesEndpoint: 'https://tools.sandbox.surflineadmin.com/api',
  },
  staging: {
    adminServicesEndpoint: 'https://tools.staging.surflineadmin.com/api',
  },
  production: {
    adminServicesEndpoint: 'https://tools.surflineadmin.com/api',
  },
}[process.env.APP_ENV || 'development'];
