import React from 'react';
import PropTypes from 'prop-types';
import set from 'lodash.set';
import { Button, Input, Select } from '@surfline/quiver-react';

const styles = {
  field: {
    marginBottom: '32px',
  },
  button: {
    minWidth: '410px',
    marginBottom: '32px',
  },
  error: {
    marginBottom: '32px',
  },
};

const RequestCodesForm = ({
  submit,
  cancel,
  loading,
  success,
}) => (
  <div>
    <h4>Request Gift Codes</h4>

    <form
      onSubmit={(event) => {
        event.preventDefault();
        submit(
          [...new FormData(event.target)].reduce(
            (formFields, [field, value]) => {
              set(formFields, field, value);
              return formFields;
            },
            {},
          ),
        );
      }}
    >
      <Select
        type="select"
        label="Brand"
        select={{ name: 'brand' }}
        id="brand"
        options={[
          { text: 'Surfline', value: 'sl' },
          { text: 'Buoyweather', value: 'bw' },
          { text: 'Fishtrack', value: 'fs' },
        ]}
        style={styles.field}
      />
      <Select
        type="select"
        label="Department"
        select={{ name: 'department' }}
        id="department"
        options={[
          { text: 'Ad Sales', value: 'Ad Sales' },
          { text: 'AFrame', value: 'AFrame' },
          { text: 'Brand Ambassador', value: 'Brand Ambassador' },
          { text: 'Cam Ops', value: 'Cam Ops' },
          { text: 'Customer Support', value: 'Customer Support' },
          { text: 'Forecast', value: 'Forecast' },
          { text: 'Marketing', value: 'Marketing' },
          { text: 'Operations', value: 'Operations' },
        ]}
        style={styles.field}
      />
      <Input
        type="text"
        label="Campaign Name"
        id="campaign"
        input={{ name: 'campaign', required: true }}
        style={styles.field}
      />
      <Input
        type="email"
        label="Employee Sponsor (email)"
        id="sponsor"
        input={{ name: 'sponsor', required: true }}
        style={styles.field}
      />
      <Select
        type="select"
        label="Product"
        select={{ name: 'product' }}
        id="product"
        options={[
          { text: '1 Month', value: 1 },
          { text: '3 Months', value: 3 },
          { text: '6 Months', value: 6 },
          { text: '1 Year', value: 12 },
        ]}
        style={styles.field}
      />
      <Input
        type="tel"
        label="Quantity"
        id="total"
        input={{ name: 'total', required: true }}
        style={styles.field}
      />
      <Button
        loading={loading}
        success={success}
        label="submit"
        style={styles.button}
      >
        Submit
      </Button>
      <Button
        type="info"
        label="Cancel"
        style={styles.button}
        onClick={() => cancel()}
        button={{ disabled: loading }}
      >
        Cancel
      </Button>
    </form>
  </div>
);

RequestCodesForm.propTypes = {
  submit: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  success: PropTypes.bool,
};

RequestCodesForm.defaultProps = {
  loading: false,
  success: false,
};

export default RequestCodesForm;
