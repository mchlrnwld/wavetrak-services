import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@surfline/quiver-react';
import { CSVLink } from 'react-csv';
import getStringFromBrand from '../utils/getStringFromBrand';
import getStringFromDuration from '../utils/getStringFromDuration';
import formatFilename from '../utils/formatFilename';

const RequestCodesConfirmation = ({
  requestData,
  giftCodes,
  loading,
  submitRequest,
  cancel,
}) => {
  const {
    brand,
    department,
    prefix,
    campaign,
    product,
    total,
  } = requestData;
  const headers = [
    { label: 'Brand', key: 'brand' },
    { label: 'Campaign', key: 'campaign' },
    { label: 'Employee Sponsor', key: 'sponsor' },
    { label: 'Product', key: 'product' },
    { label: 'Code', key: 'redeemCode' },
    { label: 'Processed By', key: 'processedBy' },
    { label: 'Date Processed', key: 'processedOn' },
  ];
  return (
    <div className="confirm-request-codes">
      <h4>{giftCodes ? 'Dowload Your Gift Codes' : 'Review Your Gift Codes Request'}</h4>
      <div className="confirm-request-codes__details">
        <div className="confirm-request-codes__label">
          <span>Brand:</span> {getStringFromBrand(brand)}
        </div>
        <div className="confirm-request-codes__label">
          <span>Department:</span> {department}
        </div>
        <div className="confirm-request-codes__label">
          <span>Campain Name:</span> {campaign}
        </div>
        <div className="confirm-request-codes__label">
          <span>Product:</span> {getStringFromDuration(product)}
        </div>
        <div className="confirm-request-codes__label">
          <span>Quanity:</span> {total}
        </div>
        <div className="confirm-request-codes__prefix-notification">
          {`** Each code generated will be prefixed with the corresponding department initials; eg: ${prefix}2N1PSVS45 **`}
        </div>
      </div>
      {giftCodes ? (
        <div className="download-gift-codes">
          <CSVLink
            data={giftCodes}
            filename={formatFilename(campaign)}
            headers={headers}
            className="download-gift-codes__link"
          >
            Download Codes
          </CSVLink>
          <Button
            type="info"
            label="Done"
            onClick={() => cancel()}
            button={{ disabled: loading }}
          >
            Done
          </Button>
        </div>
      ) : (
        <div>
          <Button
            loading={loading}
            label="confirm"
            onClick={submitRequest}
            button={{ disabled: loading }}
          >
            Confirm
          </Button>
          <Button
            type="info"
            label="Cancel"
            onClick={() => cancel()}
            button={{ disabled: loading }}
          >
            Cancel
          </Button>
        </div>
      )}
    </div>
  );
};

RequestCodesConfirmation.propTypes = {
  requestData: PropTypes.shape({
    brand: PropTypes.string,
    department: PropTypes.string,
    campaign: PropTypes.string,
    product: PropTypes.string,
    total: PropTypes.string,
    prefix: PropTypes.string,
  }).isRequired,
  giftCodes: PropTypes.arrayOf(
    PropTypes.shape({
      brand: PropTypes.string,
      department: PropTypes.string,
      campaign: PropTypes.string,
      product: PropTypes.string,
      redeemCode: PropTypes.string,
    })
  ),
  loading: PropTypes.bool,
  submitRequest: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired,
};

RequestCodesConfirmation.defaultProps = {
  loading: false,
  giftCodes: null,
};

export default RequestCodesConfirmation;
