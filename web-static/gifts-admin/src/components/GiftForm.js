import React from 'react';
import PropTypes from 'prop-types';
import set from 'lodash.set';
import { Button, Input, Select } from '@surfline/quiver-react';

const styles = {
  field: {
    marginBottom: '32px',
  },
  button: {
    minWidth: '410px',
    marginBottom: '32px',
  },
  error: {
    marginBottom: '32px',
  },
};

const GiftForm = ({
  submit,
  cancel,
  gift,
  loading,
  success,
}) => (
  <form
    onSubmit={(event) => {
      event.preventDefault();
      submit(
        [...new FormData(event.target)].reduce((formFields, [field, value]) => {
          set(formFields, field, value);
          return formFields;
        }, {}),
      );
    }}
  >
    <Select
      defaultValue={gift.brand}
      type="select"
      label="Brand"
      select={{ name: 'brand' }}
      id="brand"
      options={[
        { text: 'Surfline', value: 'sl' },
        { text: 'Buoyweather', value: 'bw' },
        { text: 'Fishtrack', value: 'fs' },
      ]}
      style={styles.field}
    />
    <Input
      defaultValue={gift.name}
      type="text"
      label="Name (Displayed in UI)"
      id="name"
      input={{ name: 'name', required: true }}
      style={styles.field}
    />
    <Input
      defaultValue={gift.description}
      type="text"
      label="Description"
      id="description"
      input={{ name: 'description', required: true }}
      style={styles.field}
    />
    <Input
      defaultValue={gift.duration}
      type="number"
      label="Duration (In Months)"
      id="duration"
      input={{ name: 'duration', required: true }}
      style={styles.field}
    />
    <Input
      defaultValue={gift.price}
      type="number"
      label="Price (In Cents)"
      id="price"
      input={{ name: 'price', required: true }}
      style={styles.field}
    />
    <Input
      defaultValue={gift.imagePath}
      type="text"
      label="Image Url"
      id="imagePath"
      input={{ name: 'imagePath', required: true }}
      style={styles.field}
    />
    <Select
      defaultValue={gift.bestValue}
      label="Best Value?"
      id="bestValue"
      type="select"
      options={[{ text: 'Yes', value: true }, { text: 'No', value: false }]}
      select={{ name: 'bestValue', required: true }}
      style={styles.field}
    />
    <Select
      defaultValue={gift.isActive}
      label="Is Active?"
      id="isActive"
      type="select"
      options={[{ text: 'Yes', value: true }, { text: 'No', value: false }]}
      select={{ name: 'isActive', required: true }}
      style={styles.field}
    />
    <Button
      loading={loading}
      success={success}
      label="Submit"
      style={styles.button}
    >
      Submit
    </Button>
    <Button
      type="info"
      label="Cancel"
      style={styles.button}
      onClick={() => cancel()}
      button={{ disabled: loading }}
    >
      Cancel
    </Button>
  </form>
);


GiftForm.propTypes = {
  submit: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired,
  gift: PropTypes.shape({}),
  loading: PropTypes.bool,
  success: PropTypes.bool,
};

GiftForm.defaultProps = {
  gift: null,
  loading: false,
  success: false,
};


export default GiftForm;
