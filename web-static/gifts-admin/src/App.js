import React from 'react';
import PropTypes from 'prop-types';
import { ConnectedRouter } from 'connected-react-router';
import routes from './routes';

import '@surfline/quiver-themes/stylesheets/crossbrand.css';
import '@surfline/admin-common/assets/styles.css';

const App = ({ history }) => (
  <ConnectedRouter history={history}>
    {routes}
  </ConnectedRouter>
);
App.propTypes = {
  history: PropTypes.object.isRequired,
};

export default App;
