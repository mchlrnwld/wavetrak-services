import chai, { expect } from 'chai';
import sinon from 'sinon';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as api from '../common/api';
import giftsStub from './stubs/gifts';
import {
  fetchGifts,
  FETCH_GIFTS,
  FETCH_GIFTS_SUCCESS,
  FETCH_GIFTS_FAIL,
} from './gifts';

chai.use(dirtyChai);
chai.use(sinonChai);
sinonStubPromise(sinon);

describe('Actions / Gifts', () => {
  let mockStore;
  let fetchGiftsStub;

  before(() => {
    mockStore = configureMockStore([thunk]);
  });

  beforeEach(() => {
    fetchGiftsStub = sinon.stub(api, 'fetchGifts');
  });

  afterEach(() => {
    fetchGiftsStub.restore();
  });

  it('fetches all gift products', async () => {
    try {
      const store = mockStore({});
      expect(store.getActions().to.be.empty());
      fetchGiftsStub.resolves(giftsStub);

      const { giftProducts } = giftsStub;

      const expectedActions = [
        { type: FETCH_GIFTS },
        { type: FETCH_GIFTS_SUCCESS, giftProducts },
      ];

      await store.dispatch(fetchGifts());
      return expect(store.getActions()).to.deep.equal(expectedActions);
    } catch (error) {
      return error;
    }
  });

  it('returns an error when fetchGifts fails', async () => {
    try {
      const store = mockStore({});
      expect(store.getActions().to.be.empty());
      const error = { message: 'Error fetchig gifts' };
      fetchGiftsStub.rejects(error);

      const expectedActions = [
        { type: FETCH_GIFTS },
        { type: FETCH_GIFTS_FAIL, error },
      ];

      await store.dispatch(fetchGifts());
      return expect(store.getActions()).to.deep.equal(expectedActions);
    } catch (error) {
      return error;
    }
  });
});
