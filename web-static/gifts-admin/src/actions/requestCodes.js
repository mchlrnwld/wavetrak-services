import { requestCodes } from '../common/api';
import getPrefixFromDept from '../utils/getPrefixFromDept';

export const REQUEST_CODES = 'REQUEST_CODES';
export const REQUEST_CODES_SUCCESS = 'REQUEST_CODES_SUCCESS';
export const REQUEST_CODES_FAIL = 'REQUEST_CODES_FAIL';
export const RESET_REQUEST_CODES = 'RESET_REQUEST_CODES';
export const REVIEW_REQUEST_CODES = 'REVIEW_REQUEST_CODES';

export const reviewRequestGiftCodes = params => async (dispatch) => {
  const { department } = params;
  const requestParams = {
    ...params,
    prefix: getPrefixFromDept(department),
  };
  dispatch({
    type: REVIEW_REQUEST_CODES,
    params: requestParams,
  });
};

export const confirmRequestGiftCodes = params => async (dispatch) => {
  dispatch({ type: REQUEST_CODES });
  try {
    const {
      campaign,
      total,
      department,
      product,
    } = params;

    if (!campaign) {
      return dispatch({ type: REQUEST_CODES_FAIL, error: 'Invalid Campaign Name.' });
    }
    if (total > 600) {
      return dispatch({ type: REQUEST_CODES_FAIL, error: 'Total exceeds limit.' });
    }

    const reqParams = {
      ...params,
      total: parseInt(total, 10),
      campaign: campaign.trim(),
      prefix: getPrefixFromDept(department),
      product: parseInt(product, 10),
    };
    const giftCodes = await requestCodes(reqParams);
    return dispatch({ type: REQUEST_CODES_SUCCESS, giftCodes });
  } catch (error) {
    return dispatch({ type: REQUEST_CODES_FAIL, error: error.message });
  }
};

export const resetRequestGiftCodes = () => async dispatch => dispatch({
  type: RESET_REQUEST_CODES,
});
