import { createGift as create } from '../common/api';

export const CREATE_GIFT = 'CREATE_GIFT';
export const CREATE_GIFT_SUCCESS = 'CREATE_GIFT_SUCCESS';
export const CREATE_GIFT_FAIL = 'CREATE_GIFT_FAIL';
export const RESET_GIFT = 'RESET_GIFT';

export const createGift = params => async (dispatch) => {
  dispatch({ type: CREATE_GIFT });
  try {
    const {
      duration,
      price,
      isActive,
      bestValue,
    } = params;
    const reqParams = {
      ...params,
      duration: parseInt(duration, 10),
      price: parseInt(price, 10),
      isActive: isActive === 'true',
      bestValue: bestValue === 'true',
    };
    const newGiftProduct = await create(reqParams);
    dispatch({ type: CREATE_GIFT_SUCCESS, giftProduct: newGiftProduct });
  } catch (error) {
    dispatch({ type: CREATE_GIFT_FAIL, error: error.message });
  }
};

export const resetGift = () => async dispatch => dispatch({ type: RESET_GIFT });
