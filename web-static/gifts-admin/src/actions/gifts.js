import { fetchGifts as getGifts } from '../common/api';

export const FETCH_GIFTS = 'FETCH_GIFTS';
export const FETCH_GIFTS_SUCCESS = 'FETCH_GIFTS_SUCCESS';
export const FETCH_GIFTS_FAIL = 'FETCH_GIFTS_FAIL';

export const fetchGifts = () => async (dispatch) => {
  dispatch({ type: FETCH_GIFTS });
  try {
    const { giftProducts } = await getGifts();
    const sortedByBrand = giftProducts.sort((prev, next) => {
      if (prev.brand < next.brand) return -1;
      if (prev.brand > next.brand) return 1;
      if (prev.brand === next.brand) {
        return prev.price - next.price;
      }
      return 0;
    });
    dispatch({ type: FETCH_GIFTS_SUCCESS, giftProducts: sortedByBrand });
  } catch (error) {
    dispatch({ type: FETCH_GIFTS_FAIL, message: error.message });
  }
};
