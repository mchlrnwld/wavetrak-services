import {
  fetchGift as getGift,
  editGift as edit,
} from '../common/api';

export const FETCH_GIFT = 'FETCH_GIFT';
export const FETCH_GIFT_SUCCESS = 'FETCH_GIFT_SUCCESS';
export const FETCH_GIFT_FAIL = 'FETCH_GIFT_FAIL';
export const RESET_GIFT = 'RESET_GIFT';
export const EDIT_GIFT = 'EDIT_GIFT';
export const EDIT_GIFT_SUCCESS = 'EDIT_GIFT_SUCCESS';
export const EDIT_GIFT_FAIL = 'EDIT_GIFT_FAIL';

export const fetchGift = giftId => async (dispatch) => {
  dispatch({ type: FETCH_GIFT });
  try {
    const { giftProduct } = await getGift(giftId);
    dispatch({ type: FETCH_GIFT_SUCCESS, giftProduct });
  } catch (error) {
    dispatch({ type: FETCH_GIFT_FAIL, error: error.message });
  }
};

export const editGift = params => async (dispatch) => {
  dispatch({ type: EDIT_GIFT });
  try {
    const {
      duration,
      price,
      isActive,
      bestValue,
    } = params;
    const reqParams = {
      ...params,
      duration: parseInt(duration, 10),
      price: parseInt(price, 10),
      isActive: isActive === 'true',
      bestValue: bestValue === 'true',
    };
    const { giftProduct } = await edit(reqParams);
    dispatch({ type: EDIT_GIFT_SUCCESS, giftProduct });
  } catch (error) {
    dispatch({ type: EDIT_GIFT_FAIL, error: error.message });
  }
};

export const resetGift = () => async dispatch => dispatch({ type: RESET_GIFT });
