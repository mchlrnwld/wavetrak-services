export default (brand) => {
  if (brand === 'sl') return 'Surfline';
  if (brand === 'bw') return 'Buoyweather';
  if (brand === 'fs') return 'Fishtrack';
  if (brand === 'bl') return 'Beachlive';
  return 'No Brand Specified';
};
