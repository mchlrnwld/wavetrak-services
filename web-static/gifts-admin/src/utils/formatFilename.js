export default (campaign) => {
  const fileName = campaign.replace(/\s+/g, '-').toLowerCase();
  return `${fileName}.csv`;
};
