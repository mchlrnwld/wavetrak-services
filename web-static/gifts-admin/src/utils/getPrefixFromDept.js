export default (department) => {
  switch (department) {
    case 'Ad Sales':
      return 'AS';
    case 'AFrame':
      return 'AFRM';
    case 'Brand Ambassador':
      return 'VIP';
    case 'Cam Ops':
      return 'CAM';
    case 'Customer Support':
      return 'CS';
    case 'Forecast':
      return 'FRCST';
    case 'Marketing':
      return 'MRK';
    case 'Operations':
      return 'OPS';
    default:
      return 'ENG';
  }
};
