export default (duration) => {
  if (duration > 1) return `${duration} Months`;
  return `${duration} Month`;
};
