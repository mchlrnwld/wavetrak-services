import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { AppContainer } from '@surfline/admin-common';
import Gifts from '../containers/Gifts';
import Create from '../containers/Create';
import Edit from '../containers/Edit';
import RequestCodes from '../containers/RequestCodes';

const routes = (
  <AppContainer>
    <Switch>
      <Route exact path="/gifts" component={Gifts} />
      <Route path="/gifts/create" component={Create} />
      <Route path="/gifts/edit/:giftId" component={Edit} />
      <Route path="/gifts/request-codes" component={RequestCodes} />
    </Switch>
  </AppContainer>
);

export default routes;
