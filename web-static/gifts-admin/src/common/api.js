import fetch from './fetch';

export const fetchGifts = () => fetch('gifts/');

export const fetchGift = giftId => fetch(`gifts/${giftId}`);

export const createGift = formData => fetch('gifts/', {
  method: 'POST',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
    credentials: 'same-origin',
  },
  body: JSON.stringify(formData),
});

export const editGift = formData => fetch('gifts/', {
  method: 'PUT',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
    credentials: 'same-origin',
  },
  body: JSON.stringify(formData),
});

export const requestCodes = formData => fetch('gifts/generate-codes', {
  method: 'POST',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
    credentials: 'same-origin',
  },
  body: JSON.stringify(formData),
});
