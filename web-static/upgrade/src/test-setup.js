import fs from 'fs';
import path from 'path';
import Mocha from 'mocha';
import chai from 'chai';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import jsdom from 'jsdom-global';
import 'ignore-styles';

jsdom();

Enzyme.configure({ adapter: new Adapter() });

chai.use(dirtyChai);
chai.use(sinonChai);

const findTestFiles = (dir) => {
  const contents = fs.readdirSync(dir);

  const filePaths = [];

  contents.forEach((element) => {
    const elementPath = path.join(dir, element);
    const stats = fs.statSync(elementPath);

    if (stats.isDirectory()) {
      Array.prototype.push.apply(filePaths, findTestFiles(elementPath));
    } else if (element.includes('.spec.js')) {
      filePaths.push(elementPath);
    }
  });

  return filePaths;
};

const runMocha = (dir) =>
  new Promise((resolve, reject) => {
    try {
      const mocha = new Mocha();
      const files = findTestFiles(dir);
      files.forEach((file) => {
        mocha.addFile(file);
      });
      mocha.run(resolve);
    } catch (err) {
      reject(err);
    }
  });

const tests = async () => {
  const failures = await runMocha(__dirname);
  if (failures) process.exit(failures ? 1 : 0);
};

tests()
  .then(() => process.exit(0))
  .catch((err) => {
    // eslint-disable-next-line no-console
    console.error(err);
    process.exit(1);
  });
