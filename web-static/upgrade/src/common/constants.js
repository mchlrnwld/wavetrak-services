export const CONTROL = 'control';
export const OFF = 'off';
export const ON = 'on';
export const US = 'US';
export const OVER_UNDER = 'over-under';
export const SIDE_BY_SIDE = 'side-by-side';
export const OLD = 'old';
