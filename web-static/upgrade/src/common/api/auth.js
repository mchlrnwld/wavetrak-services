import fetch from '../utils/fetch';
import getConfig from '../../config';

const config = getConfig();

export const generateToken = (isShortLived, userInfo) =>
  fetch(
    `/trusted/token?isShortLived=${isShortLived}`,
    null,
    {
      method: 'POST',
      headers: {
        accept: 'application/json',
        Authorization: `Basic ${Buffer.from(`${config.clientId}:${config.clientSecret}`).toString(
          'base64',
        )}`,
        'content-type': 'application/x-www-form-urlencoded',
        credentials: 'same-origin',
      },
      body: userInfo,
    },
    'application/x-www-form-urlencoded',
  );

export const authorise = (accessToken) =>
  fetch(`trusted/authorise?access_token=${accessToken}`, null, { method: 'GET' });
