import { fetchWithAuth, baseFetch } from '@surfline/web-common';
import getConfig from '../../config';

const { servicesEndpoint } = getConfig();

/**
 * @function getMeter
 * @return {Promise<import('@surfline/web-common').MeterState}
 */
export const getMeter = () => fetchWithAuth(`/meter`, {}, { customUrl: servicesEndpoint });

/**
 * @param {string} anonymousId
 * @return {Promise<{ meterRemaining: number | null }>}
 */
export const getAnonymousMeter = async (anonymousId) => {
  try {
    const response = await baseFetch(`${servicesEndpoint}/meter/anonymous/${anonymousId}/`, {
      method: 'GET',
    });
    return response;
  } catch (err) {
    return null;
  }
};
