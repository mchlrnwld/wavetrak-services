import fetch from '../utils/fetch';

export const getPlans = (accessToken, brand, filter = 'all', countryISO = 'US') => {
  const urlPath = `/pricing-plans?brand=${brand}&filter=${filter}&countryISO=${countryISO}`;
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const validateCoupon = (accessToken, brand, coupon, countryISO) => {
  const urlPath = `/subscriptions/coupons?brand=${brand}&coupon=${coupon}&countryISO=${countryISO}`;
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const getCards = (accessToken) => {
  const urlPath = '/credit-cards';
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const upgrade = (accessToken, subscriptionValues) =>
  fetch('/subscriptions/billing', accessToken, {
    method: 'POST',
    body: JSON.stringify({ ...subscriptionValues }),
  });

export const getCurrencyForUser = (countryISO, brand) => {
  const urlPath = `/subscriptions/currency?countryISO=${countryISO}&brand=${brand}`;
  return fetch(urlPath, null, { method: 'GET' });
};

export const getPricingPlans = (accessToken, brand, filter = 'all', countryISO = 'US') => {
  const urlPath = `/pricing-plans?brand=${brand}&filter=${filter}&countryISO=${countryISO}`;
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const initiateStripeCheckout = (accessToken, values) =>
  fetch('/subscriptions/billing/checkout', accessToken, {
    method: 'POST',
    body: JSON.stringify(values),
  });

export const verifyStripeSubscription = (accessToken, values) =>
  fetch('/subscriptions/billing/verify', accessToken, {
    method: 'POST',
    body: JSON.stringify(values),
  });
