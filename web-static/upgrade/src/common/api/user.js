import fetch from '../utils/fetch';

export const getUser = (accessToken) => fetch('user', accessToken, { method: 'GET' });

export const postUser = (isShortLived, user) =>
  fetch(`user?isShortLived=${isShortLived}`, null, {
    method: 'POST',
    body: JSON.stringify(user),
  });
