import { useState } from 'react';
import PropTypes from 'prop-types';
import { useMount } from 'react-use';
import { Provider } from 'react-redux';
import { createSplitFilter, setupFeatureFramework, SL } from '@surfline/web-common';
import getConfig from '../config';
import Loading from '../components/Loading';
import AppHead from '../components/AppHead/AppHead';
import store from '../store';
import { fetchUser, setGeoData } from '../store/actions/user';
import { fetchCurrency } from '../store/actions/subscription';
import { fetchPlans } from '../store/actions/plans';
import { fetchCards } from '../store/actions/cards';
import { validateAccessToken } from '../store/actions/auth';
import { fetchEntitlements } from '../store/actions/entitlements';
import { setupMetering } from '../store/actions/meter';
import { getStripeCheckoutTreatment } from '../store/actions/interactions';
import { fetchPromotion } from '../store/actions/promotion';

import Layout from '../components/Layout';

import { setupLogRocket } from '../utils/logrocket';
import { getUser } from '../store/selectors/user';
import * as treatments from '../common/utils/treatments';

const config = getConfig();
const IS_SURFLINE = config.brand === SL;

/**
 * @param {object} props
 * @param {import('next/app').AppProps["Component"]} props.Component
 * @param {import('next/app').AppProps["pageProps"]} props.pageProps
 */
const UpgradeApp = ({ Component, pageProps }) => {
  const [appReady, setAppReady] = useState(false);
  useMount(async () => {
    await store.dispatch(validateAccessToken());
    await store.dispatch(fetchEntitlements());

    const doAction = (action) => store.dispatch(action());
    await Promise.all([fetchUser, fetchEntitlements, setGeoData].map(doAction));

    try {
      await setupFeatureFramework({
        authorizationKey: config.splitio,
        user: getUser(store.getState()),
        splitFilters: createSplitFilter(treatments),
      });
    } finally {
      setupLogRocket();

      await Promise.all(
        [
          fetchCurrency,
          fetchPlans,
          fetchCards,
          fetchPromotion,
          IS_SURFLINE && setupMetering,
          getStripeCheckoutTreatment,
        ]
          .filter(Boolean)
          .map(doAction),
      );

      setAppReady(true);
    }
  });

  return (
    <Provider store={store}>
      <Layout>
        <AppHead config={config} />
        {appReady ? <Component {...pageProps} /> : <Loading />}
      </Layout>
    </Provider>
  );
};

UpgradeApp.propTypes = {
  pageProps: PropTypes.shape().isRequired,
  Component: PropTypes.node.isRequired,
};

export default UpgradeApp;
