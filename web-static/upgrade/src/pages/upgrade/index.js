import Head from 'next/head';
import { useEffect } from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import dynamic from 'next/dynamic';
import { trackCampaign } from '@surfline/web-common';
import Loading from '../../components/Loading';
import { SET_COUPON } from '../../store/actions/coupon';
import { SUBMIT_SUBSCRIPTION_SUCCESS } from '../../store/actions/user';
import { getFunnelConfig, getCurrencyEligibility } from '../../store/selectors/promotion';
import { getPremiumStatus } from '../../store/selectors/entitlements';
import PromotionError from '../../components/PromotionError';
import CurrencyIneligible from '../../components/CurrencyIneligible';
import PromotionIneligible from '../../components/PromotionIneligible';

const UpgradeContainer = dynamic(import('../../components/Upgrade'), {
  loading: () => <Loading />,
  ssr: false,
});

const Upgrade = (props) => {
  const dispatch = useDispatch();
  const { query } = useRouter();
  const { discountCode, discountcode, subscriptionSuccess } = query;
  const funnelConfig = useSelector(getFunnelConfig, shallowEqual);
  const { isActive } = funnelConfig || {};
  const isPremium = useSelector(getPremiumStatus);
  const currencyIsEligible = useSelector(getCurrencyEligibility);

  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => trackCampaign(query), []);
  /* eslint-enable react-hooks/exhaustive-deps */

  useEffect(() => {
    if (discountCode || discountcode) {
      dispatch({ type: SET_COUPON, coupon: discountCode || discountcode });
    }
  }, [discountCode, discountcode, dispatch]);

  useEffect(() => {
    if (subscriptionSuccess) {
      dispatch({ type: SUBMIT_SUBSCRIPTION_SUCCESS });
    }
  });

  if (funnelConfig && !subscriptionSuccess) {
    if (!isActive) {
      return <PromotionError />;
    }

    if (!currencyIsEligible) {
      return <CurrencyIneligible />;
    }

    if (isPremium) {
      return <PromotionIneligible />;
    }
  }

  return (
    <>
      <Head>
        <title>Create Your Premium Account - Surfline</title>
      </Head>
      <UpgradeContainer {...props} />
    </>
  );
};

export default Upgrade;
