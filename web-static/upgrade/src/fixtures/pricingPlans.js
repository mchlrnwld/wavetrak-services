const pricingPlansFixture = {
  plans: [
    {
      amount: 999,
      product: 'prod_D4dMKyEc0lqSyy',
      entitlementKey: 'sl_premium',
      upgradable: '1',
      interval: 'month',
      intervalCount: 1,
      currency: 'usd',
      trialPeriodDays: null,
      iso: 'USD',
      symbol: '$',
      country: 'US',
      renewalPrice: 999,
      brand: 'sl',
    },
    {
      amount: 9588,
      product: 'prod_D4dIpnxIIiKZCZ',
      entitlementKey: 'sl_premium',
      upgradable: '1',
      interval: 'year',
      intervalCount: 1,
      currency: 'usd',
      trialPeriodDays: 15,
      iso: 'USD',
      symbol: '$',
      country: 'US',
      renewalPrice: 9588,
      brand: 'sl',
    },
  ],
};

export default pricingPlansFixture;
