/* eslint-disable global-require */
const getConfig = require('../../config');

const { brand } = getConfig();

const translations = {
  bw: require('./bw/en'),
  sl: require('./sl/en'),
  fs: require('./fs/en'),
};

module.exports = translations[brand];
