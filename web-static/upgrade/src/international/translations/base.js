import { Wave } from '../../components/Icons';
import brandToString from '../../utils/brandToString';

export default {
  app: {
    sign_in_link: 'Sign in',
  },
  upgradeFunnel: {
    freeTrialMessage: (trialEndDate) =>
      `It's easy to cancel anytime before your trial is over on ${trialEndDate}`,
  },
  duplicateCard: {
    header: 'This card has been used for a free trial before',
    description: (amount, symbol) =>
      `This card is not eligible for a free trial. Please use another card to start your trial, or continue to Go Premium. You will be charged ${symbol}${amount} today.`,
    goPremium: 'GO PREMIUM',
    expirationDate: (date) => `EXPIRES ${date}`,
    goBack: 'GO BACK',
  },
  rail_cta: {
    sl: {
      header: 'Premium Benefits',
      mobileHeader: 'Go Premium',
      mobileSubtitle: 'Learn More About Surfline Premium',
      MobileIcon: Wave,
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
    },
    bw: {
      mobileHeader: 'Go Premium',
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
      header: 'Buoyweather',
    },
    fs: {
      mobileHeader: 'Go Premium',
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
      header: 'FishTrack',
    },
  },

  create: {
    title: 'Create account',
    onboarding_title: "You're almost there!",
    onboarding_subtitle:
      'Save your settings by creating an account below. Already have an account?',
    onboarding_alert_nofav: 'Please create an account or sign in to save favorites.',
    meta_title: 'Create Account - Surfline',
    meta_description:
      'Don’t miss another good surf – create an account with Surfline to gain instant access to Surfline.',
    form: {
      already_have_account: 'Already have an account?',
      sign_in_link: ' Sign In',
      discount_applied: 'Your discount has been applied.',
      label_first: 'First Name',
      label_last: 'Last Name',
      create_account_button: 'Create account',
      staySignedIn_label: 'Keep me signed in',
      receivePromotions_label: (brand) =>
        `Send me product and marketing updates from ${brandToString(brand)} and its partners`,
      disclaimer_text_start: 'By clicking "Create Account" you agree to Surfline ',
      terms_of_use_link_text: 'Terms of use',
      disclaimer_and: ' and ',
      privacy_policy_link_text: 'Privacy policy',
      password_helper_text: 'Minimum 6 characters',
    },
  },

  create_funnel: {
    meta_title: 'Create Your Premium Account - Surfline',
    title_no_trial: 'Join Surfline Premium',
  },

  sign_in: {
    title: 'Sign in',
    onboarding_title: "You're almost there!",
    onboarding_subtitle: 'Save your settings by signing in below. Need an account?',
    meta_title: 'Sign In - Surfline',
    meta_description:
      'Sign in to Surfline and start surfing more often by customizing your Surfline experience.',
    form: {
      need_account: 'Need an account?',
      label_email: 'Email',
      forgot_password: 'Forgot Password?',
      sign_in_button: 'Sign in',
      staySignedIn_label: 'Keep me signed in',
      submit_success: '',
    },
  },

  sign_in_funnel: {
    title: 'Sign in to start free trial',
    title_no_trial: 'Sign in to join Surfline Premium',
    meta_title: 'Sign In to Go Premium - Surfline',
    meta_description:
      "Don't miss another good surf - sign in with your Surfline account to go Premium.",
  },
  subscription: {
    meta_title: {
      sl: 'Subscriptions - Surfline',
      bw: 'Subscriptions - Buoyweather',
      fs: 'Subscriptions - FishTrack',
    },
    title: {
      subscription: 'Subscription',
      subscriptions: 'Subscriptions',
    },
    upgrade: 'Upgrade your experience',
    add: 'Add Credit/Debit Card',
    card: {
      default: 'Default method',
      actions: {
        default: 'Use As Default',
        delete: 'Delete',
      },
    },
    plans: {
      daily: 'daily',
      weekly: 'weekly',
      monthly: 'monthly',
      yearly: 'yearly',
    },
  },
  utils: {
    email: 'Not a valid email address format',
    required: 'Required',
    min_length_start: 'Minimum ',
    min_length_end: ' characters',
    max_length_start: 'Maximum ',
    max_length_end: ' characters',
    match: 'Do not match',
    integer: 'Must be a number',
    inactive_promotion: (brandName) =>
      `This promotion has expired, but you can still sign-up for ${brandName} Premium.`,
  },

  facebook: {
    facebook_sign_in: 'Sign in with Facebook',
  },

  or_break: {
    or: 'or',
  },
};
