import base from '../base';
import { Buoy, Wind, SwellLines } from '../../../components/Icons';

export default {
  app: {
    ...base.app,
  },

  upgradeFunnel: {
    ...base.upgradeFunnel,
    upgradeHeaderSubtitle: (symbol, monthlyPrice) =>
      `Buoyweather plans start at ${symbol}${monthlyPrice}/mo`,
  },

  duplicateCard: {
    ...base.duplicateCard,
  },

  premiumFeatures: {
    features: [
      {
        Icon: SwellLines,
        header: 'Marine Forecasts For Any Point',
        description: 'View accurate 16-day wind, wave and tide forecasts.',
      },
      {
        Icon: Wind,
        header: 'High-Resolution Wind Forecasts',
        description: 'Access a full collection of weather charts and forecasts.',
      },
      {
        Icon: Buoy,
        header: 'Pick Your Weather Window',
        description: 'Long-range forecasts help you stay safe on the water.',
      },
    ],
  },
  rail_cta: {
    ...base.rail_cta.bw,
  },

  create: {
    ...base.create,
    meta_title: 'Create Account - Buoyweather',
    meta_description:
      'Stay safe on the water – create an account with Buoyweather to gain instant access to point-based marine forecasts.',

    form: {
      ...base.create.form,
      disclaimer_text_start: 'By clicking "Create Account" you agree to Buoyweather ',
    },
  },

  create_funnel: {
    ...base.create_funnel,
    meta_title: 'Create Your Premium Account - Buoyweather',
    title_no_trial: 'Join Buoyweather Premium',
  },

  sign_in: {
    ...base.sign_in,
    meta_title: 'Sign In - Buoyweather',
    meta_description:
      'Sign in to Buoyweather and get point-based marine forecasts anywhere in the world',

    form: {
      ...base.sign_in.form,
      disclaimer_text_start:
        'If you click "Sign In with Facebook" and are not a Buoyweather user, you will be registered and you agree to Buoyweather ',
    },
  },

  sign_in_funnel: {
    ...base.sign_in_funnel,
    title_no_trial: 'Sign in to join<br/>Buoyweather Premium',
    meta_title: 'Sign In to Go Premium - Buoyweather',
    meta_description:
      'Get the latest 16-day marine forecasts – sign in with your Buoyweather account to go Premium.',
  },
  payment_success: {
    subTitle: 'Welcome to Buoyweather Premium!',
    message:
      "Thank you for joining the Buoyweather family.  You're now on your way to safer seas with the most trusted marine weather information.",
    linkText: 'Return to Buoyweather',
    linkAddress: 'https://buoyweather.com',
  },
  payment_verification: {
    subTitle: 'Congrats on your new Buoyweather Premium membership!',
    message:
      "Your subscription is processing, and you'll be taken back to Buoyweather in a moment so you can get started",
    error:
      'It looks like something went wrong while processing your subscription, please contact us by emailing support@buoyweather.com and we will get it sorted out ASAP',
  },

  subscription: {
    ...base.subscription,
    title: {
      ...base.subscription.title,
    },
    meta_title: {
      ...base.subscription.meta_title,
    },
    expiring_message: (renewDate, brand) =>
      `Your ${brand} subscription will expire on ${renewDate}. Stay Premium to keep 16-Day Forecasts, High Resolution Wind, and more.`,
    cards: {
      ...base.subscription.cards,
    },
    card: {
      ...base.subscription.card,
      actions: {
        ...base.subscription.card.actions,
      },
    },
    plans: {
      ...base.subscription.plans,
    },
  },
  utils: {
    ...base.utils,
  },

  facebook: {
    ...base.facebook,
  },

  or_break: {
    ...base.or_break,
  },
};
