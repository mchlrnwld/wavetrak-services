/* Surfline text is default as defined in base.js */
import base from '../base';
import {
  Cams,
  Analysis,
  Charts,
  CamRewind,
  SurflineLogo,
  CamsAlt as PromotionCams,
  AnalysisAlt as PromotionAnalysis,
  ChartsAlt as PromotionCharts,
} from '../../../components/Icons';

export default {
  app: {
    ...base.app,
  },

  upgradeFunnel: {
    ...base.upgradeFunnel,
    upgradeHeaderSubtitle: (symbol, monthlyPrice) =>
      `Surfline plans start at ${symbol || '$'}${monthlyPrice || 7.99}/mo`,
  },

  duplicateCard: {
    ...base.duplicateCard,
  },

  premiumFeatures: (isUserMetered) => ({
    features: [
      {
        Icon: Cams,
        header: isUserMetered ? 'Ad-Free and HD Cams' : 'Ad-Free and Premium Cams',
        description: 'Watch live conditions ad-free -- and access exclusive, high-resolution cams.',
      },
      {
        Icon: Analysis,
        header: 'Trusted Forecast Team',
        description: 'Plan your next trip or session with Surfline’s expert forecast guidance.',
      },
      {
        Icon: Charts,
        header: 'Exclusive Forecast Tools',
        description: 'Access curated condition reports and proprietary surf, wind and buoy data.',
      },
      {
        Icon: CamRewind,
        header: 'Cam Rewind',
        description:
          'Relive an epic session by downloading your rides -- then share ‘em with friends.',
      },
    ],
  }),

  promotionFeatures: {
    BrandLogo: SurflineLogo,
    header: 'Score better waves more often',
    features: [
      {
        Icon: PromotionCams,
        header: 'Ad-Free and Premium Cams',
      },
      {
        Icon: PromotionAnalysis,
        header: 'Trusted Forecast Team',
      },
      {
        Icon: PromotionCharts,
        header: 'Exclusive Forecast Tools',
      },
    ],
    promoCodeError:
      'Invalid Code. To receive this offer you must enter a valid code. If you do not have one, you can still upgrade to Surfline Premium at www.surfline.com/upgrade/',
  },

  rail_cta: {
    ...base.rail_cta.sl,
  },

  create: {
    ...base.create,

    form: {
      ...base.create.form,
    },
  },

  create_funnel: {
    ...base.create_funnel,
  },

  sign_in: {
    ...base.sign_in,

    form: {
      ...base.sign_in.form,
    },
  },

  sign_in_funnel: {
    ...base.sign_in_funnel,
  },
  payment_success: {
    subTitle: 'Welcome to Surfline Premium!',
    message:
      "Thank you for joining the Surfline family.  You're now on your way to scoring better waves, more often.  See you in the water.",
    linkText: 'Return to Surfline',
    linkAddress: 'https://surfline.com',
  },
  payment_verification: {
    subTitle:
      'Congrats — your new Surfline Premium membership will help you find and surf better waves.',
    message:
      "Your subscription is processing, and you'll be taken back to Surfline in a moment so you can get started scoping the cams",
    error:
      'It looks like something went wrong while processing your subscription, please contact us by emailing support@surfline.com and we will get it sorted out ASAP',
  },

  subscription: {
    ...base.subscription,
    title: {
      ...base.subscription.title,
    },
    meta_title: {
      ...base.subscription.meta_title,
    },
    cards: {
      ...base.subscription.cards,
    },
    card: {
      ...base.subscription.card,
      actions: {
        ...base.subscription.card.actions,
      },
    },
    plans: {
      ...base.subscription.plans,
    },
  },
  utils: {
    ...base.utils,
  },

  facebook: {
    ...base.facebook,
  },

  or_break: {
    ...base.or_break,
  },
};
