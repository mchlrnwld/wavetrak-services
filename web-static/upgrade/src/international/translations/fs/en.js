import base from '../base';
import { Satellite, Layers, Waypoint } from '../../../components/Icons';

export default {
  app: {
    ...base.app,
  },

  upgradeFunnel: {
    ...base.upgradeFunnel,
    upgradeHeaderSubtitle: (symbol, monthlyPrice) =>
      `Fishtrack plans start at ${symbol}${monthlyPrice}/mo`,
  },

  duplicateCard: {
    ...base.duplicateCard,
  },

  premiumFeatures: {
    features: [
      {
        Icon: Satellite,
        header: 'Latest Satellite Imagery',
        description: 'Locate temperature and color breaks offshore.',
      },
      {
        Icon: Waypoint,
        header: 'Waypoints And Routes',
        description: 'Measure distances and plot routes for trips offshore.',
      },
      {
        Icon: Layers,
        header: 'Currents, Bathymetry And Altimetry',
        description: 'Use premium overlays to pinpoint areas of activity.',
      },
    ],
  },

  rail_cta: {
    ...base.rail_cta.fs,
  },

  create: {
    ...base.create,
    meta_title: 'Create Account - FishTrack',
    meta_description:
      'Find more fish – create an account with FishTrack to gain instant access to the latest satellite imagery.',

    form: {
      ...base.create.form,
      disclaimer_text_start: 'By clicking "Create Account" you agree to FishTrack ',
    },
  },

  create_funnel: {
    ...base.create_funnel,
    meta_title: 'Create Your Premium Account - FishTrack',
    meta_description:
      'FishTrack Fishing Charts - Create a FishTrack account to view the latest SST, Chlorophyll and True-Color satellite charts.',
    title_no_trial: 'Join FishTrack Premium',
  },

  sign_in: {
    ...base.sign_in,
    meta_title: 'Sign In - FishTrack',
    meta_description: 'Sign in to FishTrack and find fish faster offshore.',

    form: {
      ...base.sign_in.form,
      disclaimer_text_start:
        'If you click "Sign In with Facebook" and are not a FishTrack user, you will be registered and you agree to FishTrack ',
    },
  },

  sign_in_funnel: {
    ...base.sign_in_funnel,
    title_no_trial: 'Sign in to join<br/>FishTrack Premium',
    meta_title: 'Sign In to Go Premium - FishTrack',
    meta_description:
      'Get the latest satellite imagery - sign in with your FishTrack account to go Premium.',
  },
  payment_success: {
    subTitle: 'Welcome to Fistrack Premium!',
    message:
      'Thank you for joining the Fistrack family.  You can now access the latest satellite fishing chards and overlays.  See you in the water.',
    linkText: 'Return to Fishtrack',
    linkAddress: 'https://fishtrack.com',
  },
  payment_verification: {
    subTitle: 'Congrats on your new FishTrack Premium membership!',
    message:
      "Your subscription is processing, and you'll be taken back to FishTrack in a moment so you can get started",
    error:
      'It looks like something went wrong while processing your subscription, please contact us by emailing support@fishtrack.com and we will get it sorted out ASAP',
  },

  subscription: {
    ...base.subscription,
    title: {
      ...base.subscription.title,
    },
    meta_title: {
      ...base.subscription.meta_title,
    },
    cards: {
      ...base.subscription.cards,
    },
    card: {
      ...base.subscription.card,
      actions: {
        ...base.subscription.card.actions,
      },
    },
    plans: {
      ...base.subscription.plans,
    },
  },
  utils: {
    ...base.utils,
  },

  facebook: {
    ...base.facebook,
  },

  or_break: {
    ...base.or_break,
  },
};
