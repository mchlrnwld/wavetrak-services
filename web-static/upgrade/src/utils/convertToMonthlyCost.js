export default function convertToMonthlyCost(amount, interval) {
  if (!amount) return 0;

  switch (interval) {
    case 'day':
      return (amount * 1461) / 48;
    case 'week':
      return ((amount / 7) * 1461) / 48;
    case 'month':
      return amount;
    case 'year':
      return amount / 12;
    default:
      return 0;
  }
}
