import { addDays, format } from 'date-fns';

/**
 * @param {Plan['interval']} interval
 */
export const getPlanName = (interval) => (interval === 'year' ? 'Yearly' : 'Monthly');

/**
 * @param {Object} funnelConfig
 * @param {Plan['trialPeriodDays']} trialPeriodDays
 * @param {boolean} trialEligible
 * @returns {string}
 */
export const getTrialText = (funnelConfig, trialPeriodDays, trialEligible) => {
  if (!trialPeriodDays) return null;
  if (!trialEligible) return null;
  const trialLength = funnelConfig?.trialLength || trialPeriodDays;
  return `${trialLength}-Day Free Trial`;
};

/**
 * @param {Plan['interval']} interval
 * @param {Plan['amount']} amount
 * @returns {number}
 */
export const getProjectedCost = (interval, amount) =>
  interval === 'year' ? parseFloat(amount / 100 / 12).toFixed(2) : amount / 100;

/**
 * @param {Plan['id']} interval
 * @param {Plan['amount']} amount
 * @param {Plan['symbol']} symbol
 * @returns {string}
 */
export const getActualCost = (interval, amount, symbol) =>
  interval === 'year' ? `Billed ${symbol}${amount / 100} once a year` : 'Billed month-to-month';

/**
 * @param {Plan['trialPeriodDays']} trialPeriodDays
 * @returns {string}
 */
export const formatTrialEndDate = (trialPeriodDays = 15) => {
  const trialEnd = addDays(new Date(), trialPeriodDays);
  return format(trialEnd, 'MM/dd/yy');
};
