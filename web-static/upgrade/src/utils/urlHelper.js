import queryString from 'query-string';

const isProd = process.env.NODE_ENV === 'production';

export const getUrlSubscriptionId = () => {
  const urlPath = window.location.pathname.split('/');
  const subscriptionId = urlPath.find((item) => item.startsWith('sub'));
  if (subscriptionId) return subscriptionId;
  const { subscriptionId: subId } = queryString.parse(window.location.search);
  return subId;
};

/** *
 * This function formats the URL display in Dev vs Prod.
 * In Prod the url is expected to be in this format: /cancel/{subscriptionId}/survey?{...query_params}
 * In Dev the above format is not supported because Next does not support dynamic urls for client side only apps.
 * The dev format is : /cancel/survey?subscriptionId={subscriptionId}&{...query_params}
 */
export const formatUrlDisplay = (prodUrl, nonProdUrl) => {
  const qString = window.location.search;
  return isProd ? `${prodUrl}${qString}` : `${nonProdUrl}${qString}`;
};
