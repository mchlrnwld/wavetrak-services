import queryString from 'query-string';
import LogRocket from 'logrocket';
import { getUserDetails } from '@surfline/web-common';
import store from '../store';

import getConfig from '../config';
import { ON } from '../common/constants';

const config = getConfig();

/**
 * @description Checks if LogRocket is enabled
 * @returns {boolean}
 */
const logRocketEnabled = () => {
  const { company } = config;
  const { logrocket } = queryString.parse(window?.location?.search);
  return company === 'sl' && logrocket === ON;
};

export const setupLogRocket = () => {
  if (logRocketEnabled()) {
    LogRocket.init(config.logRocket, {
      network: {
        requestSanitizer: (request) => {
          // if the url contains 'trusted/token'
          if (request.url.toLowerCase().indexOf('trusted/token') !== -1 && request?.body) {
            // scrub out the body
            const reqBody = JSON.parse(request.body);
            delete reqBody.password; // Remove password so that we don't log in LogRocket
            request.body = reqBody;
          }
          return request;
        },
      },
    });

    const user = getUserDetails(store.getState());

    if (user) {
      LogRocket.identify(user._id, { email: user.email });
    }
  }
};
