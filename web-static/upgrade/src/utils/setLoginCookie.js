import { Cookies } from 'react-cookie';
import getConfig from '../config';

const config = getConfig();
const cookie = new Cookies();

export default async (accessToken, refreshToken, maxAge, refreshMaxAge = 31536000) => {
  /* eslint-disable no-restricted-globals */
  const domainName = config.company === 'fs' ? '.fishtrack.com' : location.hostname;
  /* eslint-enable no-restricted-globals */
  cookie.set('access_token', accessToken, {
    path: '/',
    maxAge,
    expires: new Date(new Date().getTime() + maxAge * 1000),
    domain: domainName,
    secure: false,
  });

  cookie.set('refresh_token', refreshToken, {
    path: '/',
    maxAge: refreshMaxAge,
    expires: new Date(new Date().getTime() + refreshMaxAge * 1000),
    domain: domainName,
    secure: false,
  });

  return { accessToken, refreshToken };
};
