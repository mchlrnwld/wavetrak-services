import { trackEvent } from '@surfline/web-common';

/**
 * Util function for sending a track event when a user logs in.
 * @param {object} values The form submission values object
 */
export default function (values) {
  const { facebookId, email, location } = values;
  const method = facebookId ? 'facebook' : 'email';
  const segmentTrackName = 'Signed In';
  const trackProperties = { method, email, location, platform: 'web' };
  trackEvent(segmentTrackName, trackProperties);
}
