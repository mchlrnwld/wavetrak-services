export const identifyUser = (userId) => {
  if (window) window.analytics.identify(userId);
};
