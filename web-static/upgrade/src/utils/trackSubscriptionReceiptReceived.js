import { trackEvent } from '@surfline/web-common';
import brandToString from './brandToString';

const getPlanName = (priceKey) => {
  const keyName = priceKey.toLowerCase();
  const brand = keyName.substring(0, 2);
  const brandName = brandToString(brand);
  const interval = keyName.includes('year') ? 'Annual' : 'Monthly';
  return `${brandName} ${interval}`;
};

/**
 * Util function for sending a track event to verify a subscription purchase on the client.
 * @param {object} subscription The new subscription created.
 */
export default function trackSubscriptionReceiptReceived(subscription) {
  const { subscriptionId, planId, entitlement, trialing, priceKey } = subscription;

  const segmentTrackName = 'Subscription Receipt Received';
  const trackProperties = {
    platform: 'web',
    billingPlatform: 'stripe',
    orderId: subscriptionId,
    trial: trialing,
    interval: priceKey?.toLowerCase().includes('year') ? 'annual' : 'monthly',
    products: [
      {
        category: 'Subscription',
        quantity: 1,
        productId: planId,
        sku: planId,
        brand: entitlement.substring(0, 2),
        name: getPlanName(priceKey),
      },
    ],
  };
  trackEvent(segmentTrackName, trackProperties);
}
