import en from '../international/translations/en';

export default function readableInterval(interval) {
  if (!interval) return '';
  let readableString = '';

  // For Apple IAP product values
  if (
    interval.indexOf('year') !== -1 ||
    interval.indexOf('365') !== -1 ||
    interval.indexOf('annual') !== -1
  ) {
    readableString = 'year';
  } else if (interval.indexOf('month') !== -1 || interval.indexOf('30') !== -1) {
    readableString = 'month';
  } else if (interval.indexOf('daily') !== -1) {
    readableString = 'day';
  }

  switch (readableString) {
    case 'day':
      return en.subscription.plans.daily;
    case 'week':
      return en.subscription.plans.weekly;
    case 'month':
      return en.subscription.plans.monthly;
    case 'year':
      return en.subscription.plans.yearly;
    default:
      return readableString;
  }
}
