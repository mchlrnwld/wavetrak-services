import { expect } from 'chai';
import readableInterval from '../readableInterval';
import en from '../../international/translations/en';

describe('Utils / readableInterval', () => {
  it('should get header from interval', () => {
    expect(readableInterval('day')).to.equal(en.subscription.plans.daily);
    expect(readableInterval('week')).to.equal(en.subscription.plans.weekly);
    expect(readableInterval('month')).to.equal(en.subscription.plans.monthly);
    expect(readableInterval('year')).to.equal(en.subscription.plans.yearly);
    expect(readableInterval(undefined)).to.equal('');
  });
});
