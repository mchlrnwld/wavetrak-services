import { expect } from 'chai';
import convertToMonthlyCost from '../convertToMonthlyCost';

describe('Utils / convertToMonthlyCost', () => {
  const epsilon = 10 ** -13;

  it('should convert daily cost to monthly cost', () => {
    expect(convertToMonthlyCost(5, 'day')).to.be.closeTo(152.1875, epsilon);
  });

  it('should convert weekly cost to monthly cost', () => {
    expect(convertToMonthlyCost(10, 'week')).to.be.closeTo(43.4821428571429, epsilon);
  });

  it('should preserve monthly cost', () => {
    expect(convertToMonthlyCost(20, 'month')).to.equal(20);
  });

  it('should convert yearly cost to monthly cost', () => {
    expect(convertToMonthlyCost(30, 'year')).to.be.closeTo(2.5, epsilon);
  });

  it('should default to 0', () => {
    expect(convertToMonthlyCost(40, undefined)).to.equal(0);
    expect(convertToMonthlyCost(undefined, 'month')).to.equal(0);
  });
});
