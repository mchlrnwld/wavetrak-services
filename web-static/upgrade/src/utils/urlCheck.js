const REGEX_HTTP_OR_SLASH = /^(https?:)?\/\//i;
const REGEX_HTTP = /^https?:\/\//i;
const REGEX_SLASH = /^\/\//;
const REGEX_PRINTABLE_ASCII_ONLY = /[^ -~]+/g;

/**
 * This function does a basic check for urls starting with http or //. It assumes that the
 * the url is correct after that. If a host parameter is passed, it will validate the url
 * matches the same host. It will return true for a relative url.
 * @param {*} url The url to check valid
 * @param {*} host The host to validate the url against
 * @returns
 */
const urlCheck = (url, host = null) => {
  const formattedUrl = url.replace(REGEX_PRINTABLE_ASCII_ONLY, ''); // match all characters out of the printable range and replace them with nothing
  const fullUrl = formattedUrl.replace(REGEX_SLASH, 'http://');
  const fullHost = host?.replace(REGEX_HTTP_OR_SLASH, '');

  if (fullHost && REGEX_HTTP.test(fullUrl)) {
    const { hostname } = new URL(fullUrl);
    return fullHost === hostname;
  }
  if (fullUrl.startsWith('/')) {
    return true;
  }
  return REGEX_HTTP_OR_SLASH.test(url);
};

export default urlCheck;
