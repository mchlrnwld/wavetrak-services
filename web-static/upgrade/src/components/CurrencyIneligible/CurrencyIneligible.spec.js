import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import CurrencyIneligible from './CurrencyIneligible';

describe('CurrencyIneligible', () => {
  it('renders currency ineligible component', () => {
    const ineligibleText = 'This promotion is not available in your region.';
    const buttonText = 'RETURN TO SURFLINE';
    const wrapper = shallow(<CurrencyIneligible />);
    expect(wrapper.find('.currency-ineligible__title')).to.have.lengthOf(1);
    expect(wrapper.find('p').text()).to.equal(ineligibleText);
    expect(wrapper.find('.quiver-button').text().toUpperCase()).to.equal(buttonText);
  });
});
