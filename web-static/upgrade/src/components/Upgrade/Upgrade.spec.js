import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import common from '@surfline/web-common';
import * as reactRedux from 'react-redux';
import * as auth from '../../store/selectors/auth';
import * as interactions from '../../store/selectors/interactions';
import * as user from '../../store/selectors/user';
import * as entitlements from '../../store/selectors/entitlements';
import * as plans from '../../store/selectors/plans';
import * as pricingPlans from '../../store/selectors/pricingPlans';
import * as coupon from '../../store/selectors/coupon';
import Upgrade from '.';
import PaymentForm from '../PaymentForm/PaymentForm';
import PricingPlans from '../PricingPlans/PricingPlans';
import PremiumSuccess from '../PremiumSuccess/PremiumSuccess';
import pricingPlansFixture from '../../fixtures/pricingPlans';
import { OFF, OVER_UNDER, SIDE_BY_SIDE, OLD } from '../../common/constants';

describe('Upgrade', () => {
  const dispatchStub = sinon.stub();
  let useSelectorStub;
  let useDispatchStub;
  let getAccessTokenStub;
  let getPremiumStatusStub;
  let getTrialEligibilityStub;
  let getTrialPeriodDaysStub;
  let getPremiumPlansStub;
  let getSelectedPlanIdStub;
  let getUserSuccessStub;
  let getShowLoginStub;
  let getCouponStub;
  let getCurrencyISOStub;
  let isFormSubmittingStub;
  let getUserErrorStub;
  let getFacebookLoadingStub;
  let getTreatmentStub;
  let trackNavigatedToPageStub;
  let trackEventStub;
  let getPricingPlansStub;
  let getPricingPlanCurrencyISOStub;
  let getStripeCheckoutTreatmentStub;
  let getIsStripeCheckoutEnabledStub;

  before(() => {
    useSelectorStub = sinon.stub(reactRedux, 'useSelector');
    useDispatchStub = sinon.stub(reactRedux, 'useDispatch');
    getShowLoginStub = sinon.stub(interactions, 'getShowLogin');
    getCouponStub = sinon.stub(coupon, 'getCoupon');
    getUserErrorStub = sinon.stub(user, 'getUserError');
    isFormSubmittingStub = sinon.stub(interactions, 'isFormSubmitting');
    getTrialEligibilityStub = sinon.stub(entitlements, 'getTrialEligibility');
    getTrialPeriodDaysStub = sinon.stub(plans, 'getTrialPeriodDays');
    getPremiumPlansStub = sinon.stub(plans, 'getPremiumPlans');
    getSelectedPlanIdStub = sinon.stub(plans, 'getSelectedPlanId');
    getCurrencyISOStub = sinon.stub(plans, 'getCurrencyISO');
    getFacebookLoadingStub = sinon.stub(user, 'getFacebookLoading');
    getTreatmentStub = sinon.stub(common, 'getTreatment');
    getPricingPlansStub = sinon.stub(pricingPlans, 'getPricingPlans');
    getPricingPlanCurrencyISOStub = sinon.stub(pricingPlans, 'getPricingPlanCurrencyISO');
    getStripeCheckoutTreatmentStub = sinon.stub(interactions, 'getStripeCheckoutTreatment');
    getIsStripeCheckoutEnabledStub = sinon.stub(interactions, 'getIsStripeCheckoutEnabled');

    useSelectorStub.callsArg(0);
    useDispatchStub.returns(dispatchStub);
    getCurrencyISOStub.returns('USD');
    getPremiumPlansStub.returns([]);
    trackNavigatedToPageStub = sinon.stub(common, 'trackNavigatedToPage');
    trackEventStub = sinon.stub(common, 'trackEvent');
  });

  after(() => {
    useSelectorStub.restore();
    useDispatchStub.restore();
    getShowLoginStub.restore();
    getCouponStub.restore();
    getUserErrorStub.restore();
    isFormSubmittingStub.restore();
    getTrialEligibilityStub.restore();
    getTrialPeriodDaysStub.restore();
    getPremiumPlansStub.restore();
    getSelectedPlanIdStub.restore();
    getCurrencyISOStub.restore();
    getFacebookLoadingStub.restore();
    getTreatmentStub.restore();
    trackNavigatedToPageStub.restore();
    trackEventStub.restore();
    getPricingPlansStub.restore();
    getPricingPlanCurrencyISOStub.restore();
    getStripeCheckoutTreatmentStub.restore();
    getIsStripeCheckoutEnabledStub.restore();
  });

  beforeEach(() => {
    getAccessTokenStub = sinon.stub(auth, 'getAccessToken');
    getPremiumStatusStub = sinon.stub(entitlements, 'getPremiumStatus');
    getUserSuccessStub = sinon.stub(user, 'getUserSuccess');
  });

  afterEach(() => {
    getAccessTokenStub.restore();
    getPremiumStatusStub.restore();
    getUserSuccessStub.restore();
  });

  describe('Anonymous users', () => {
    beforeEach(() => {
      getAccessTokenStub.returns(null);
    });

    it('renders registration form', () => {
      const wrapper = shallow(<Upgrade />);
      expect(wrapper.find('.registration-wrapper')).to.have.lengthOf(1);
    });
  });

  describe('Non-premium users', () => {
    beforeEach(() => {
      getAccessTokenStub.returns('12345');
      getPremiumStatusStub.returns(false);
    });

    it('renders payment form', () => {
      const wrapper = shallow(<Upgrade />);
      expect(wrapper.find(PaymentForm)).to.have.lengthOf(1);
    });

    it('renders Stripe Checkout Plan Selection Page - over-under', () => {
      getPricingPlansStub.returns(pricingPlansFixture);
      getPricingPlanCurrencyISOStub.returns('USD');
      getStripeCheckoutTreatmentStub.returns(OVER_UNDER);
      getIsStripeCheckoutEnabledStub.returns(true);
      const wrapper = shallow(<Upgrade />);
      expect(wrapper.find(PricingPlans)).to.have.lengthOf(1);
      expect(wrapper.find(PricingPlans).prop('stripeCheckoutTreatment')).to.equal(OVER_UNDER);
    });

    it('renders Stripe Checkout Plan Selection Page - side-by-side', () => {
      getPricingPlansStub.returns(pricingPlansFixture);
      getPricingPlanCurrencyISOStub.returns('USD');
      getStripeCheckoutTreatmentStub.returns(SIDE_BY_SIDE);
      getIsStripeCheckoutEnabledStub.returns(true);
      const wrapper = shallow(<Upgrade />);
      expect(wrapper.find(PricingPlans)).to.have.lengthOf(1);
      expect(wrapper.find(PricingPlans).prop('stripeCheckoutTreatment')).to.equal(SIDE_BY_SIDE);
    });

    it('renders Stripe Checkout Plan Selection Page - old', () => {
      getPricingPlansStub.returns(pricingPlansFixture);
      getPricingPlanCurrencyISOStub.returns('USD');
      getStripeCheckoutTreatmentStub.returns(OLD);
      getIsStripeCheckoutEnabledStub.returns(true);
      const wrapper = shallow(<Upgrade />);
      expect(wrapper.find(PricingPlans)).to.have.lengthOf(1);
      expect(wrapper.find(PricingPlans).prop('stripeCheckoutTreatment')).to.equal(OLD);
    });

    it('renders default Plan Selection page if treatment is off', () => {
      getStripeCheckoutTreatmentStub.returns(OFF);
      getIsStripeCheckoutEnabledStub.returns(false);
      const wrapper = shallow(<Upgrade />);
      expect(wrapper.find(PricingPlans)).to.have.lengthOf(0);
      expect(wrapper.find(PaymentForm)).to.have.lengthOf(1);
    });

    it('renders default Plan Selection page if treatment is undefined', () => {
      getStripeCheckoutTreatmentStub.returns(undefined);
      getIsStripeCheckoutEnabledStub.returns(undefined);
      const wrapper = shallow(<Upgrade />);
      expect(wrapper.find(PricingPlans)).to.have.lengthOf(0);
      expect(wrapper.find(PaymentForm)).to.have.lengthOf(1);
    });

    it('renders success page', () => {
      getUserSuccessStub.returns(true);
      const wrapper = shallow(<Upgrade />);
      expect(wrapper.find(PremiumSuccess)).to.have.lengthOf(1);
    });
  });

  describe('Premium users', () => {
    const { location } = window;
    before(() => {
      delete window.location;
      window.location = {
        assign: sinon.spy(),
      };
    });

    after(() => {
      window.location = location;
    });

    beforeEach(() => {
      getAccessTokenStub.returns('12345');
      getPremiumStatusStub.returns(true);
    });

    it('redirects to /account/subscription', () => {
      shallow(<Upgrade />);
      expect(window.location.assign).to.have.been.calledWith('/account/subscription');
    });
  });
});
