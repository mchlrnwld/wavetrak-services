import { useState, useEffect } from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { trackNavigatedToPage, trackEvent } from '@surfline/web-common';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { CreateAccountForm, LoginForm, FacebookLogin } from '@surfline/quiver-react';
import { useMount } from 'react-use';

import { fetchEntitlements } from '../../store/actions/entitlements';
import {
  register as createUser,
  fetchUser,
  login,
  loginWithFacebook,
} from '../../store/actions/user';
import { fetchPlans } from '../../store/actions/plans';
import { fetchPricingPlans } from '../../store/actions/pricingPlans';
import { fetchCards } from '../../store/actions/cards';
import { toggleRegistrationForm } from '../../store/actions/interactions';
import { getAccessToken } from '../../store/selectors/auth';
import { getPremiumStatus, getTrialEligibility } from '../../store/selectors/entitlements';
import {
  getSelectedPlanId,
  getPremiumPlans,
  getTrialPeriodDays,
  getCurrencyISO,
} from '../../store/selectors/plans';
import { getPricingPlans, getPricingPlanCurrencyISO } from '../../store/selectors/pricingPlans';
import { getUserSuccess, getUserError, getFacebookLoading } from '../../store/selectors/user';

import {
  getShowLogin,
  isFormSubmitting,
  getStripeCheckoutTreatment,
  getIsStripeCheckoutEnabled,
} from '../../store/selectors/interactions';
import { getCoupon } from '../../store/selectors/coupon';
import { fetchCoupon } from '../../store/actions/coupon';
import CouponForm from '../CouponForm/CouponForm';

import {
  PremiumSuccess,
  UpgradeHeader,
  RegistrationSubtitle,
  FunnelHelmet,
  PaymentForm,
  Disclaimer,
  PricingPlans,
} from '..';

import getConfig from '../../config';
import en from '../../international/translations/en';
import OrBreak from '../OrBreak/OrBreak';
import Loading from '../Loading';
import { fetchPromotion } from '../../store/actions/promotion';
import { getPromotionId } from '../../store/selectors/promotion';

const config = getConfig();

const stripePromise = loadStripe(config.stripe.publishableKey);

const Upgrade = () => {
  const dispatch = useDispatch();
  const doToggleForm = (showLogin) => dispatch(toggleRegistrationForm(showLogin, 'funnel'));

  // Selectors
  const accessToken = useSelector(getAccessToken);
  const isPremium = useSelector(getPremiumStatus);
  const trialEligible = useSelector(getTrialEligibility);
  const trialPeriodDays = useSelector(getTrialPeriodDays);
  const plans = useSelector(getPremiumPlans, shallowEqual);
  const selectedPlanId = useSelector(getSelectedPlanId);
  const subscriptionSuccess = useSelector(getUserSuccess);
  const showLogin = useSelector(getShowLogin);
  const coupon = useSelector(getCoupon, shallowEqual);
  const planCurrencyISO = useSelector(getCurrencyISO);
  const submitting = useSelector(isFormSubmitting);
  const error = useSelector(getUserError);
  const facebookSubmitting = useSelector(getFacebookLoading);
  const stripeCheckoutTreatment = useSelector(getStripeCheckoutTreatment);
  const isStripeCheckoutEnabled = useSelector(getIsStripeCheckoutEnabled);
  const promotionId = useSelector(getPromotionId);

  // Pricing Plan Selectors
  const pricingPlans = useSelector(getPricingPlans, shallowEqual);
  const pricingPlanCurrencyISO = useSelector(getPricingPlanCurrencyISO, shallowEqual);
  const currencyISO = isStripeCheckoutEnabled ? pricingPlanCurrencyISO : planCurrencyISO;

  const onFacebookLogin = () => {
    dispatch(loginWithFacebook('facebook', currencyISO));
  };

  const onCreateSubmit = (data) => {
    const values = {
      ...data,
      location: 'funnel',
    };
    dispatch(createUser(values, currencyISO));
  };

  const onLoginSubmit = (data) => {
    dispatch(login(data, currencyISO));
  };

  useEffect(() => {
    const reloadUser = async () => {
      await dispatch(fetchUser());
      await dispatch(fetchEntitlements());
      await dispatch(fetchPromotion());
      await dispatch(fetchCards());
    };
    if (accessToken) {
      reloadUser();
    }
  }, [accessToken, dispatch]);

  useEffect(() => {
    if (isStripeCheckoutEnabled) {
      dispatch(fetchPricingPlans());
    } else {
      dispatch(fetchPlans());
    }
  }, [accessToken, isStripeCheckoutEnabled, dispatch]);

  useEffect(() => {
    if (coupon && accessToken) {
      dispatch(fetchCoupon(coupon));
    }
  }, [coupon, accessToken, dispatch]);

  // State
  const [registrationRendered, setRegistrationRendered] = useState(false);
  const [checkoutRendered, setCheckoutRendered] = useState(false);

  useMount(() => {
    trackNavigatedToPage('Funnel', {}, 'Upgrade', { Wootric: false });
  });

  const viewedCheckout = () => {
    if (!checkoutRendered) {
      setCheckoutRendered(true);
      trackEvent('Viewed Checkout Step', { step: 2, currency: currencyISO.toUpperCase() });
    }
  };

  const viewedRegistration = () => {
    if (!registrationRendered) {
      setRegistrationRendered(true);
      trackEvent('Viewed Checkout Step', { step: 1, currency: currencyISO.toUpperCase() });
    }
  };

  const renderRegistrationForm = () => {
    viewedRegistration();
    return (
      <div className="registration-wrapper">
        <FunnelHelmet showLogin={showLogin} />
        <UpgradeHeader hideSubtitle>
          <RegistrationSubtitle showLogin={showLogin} doToggleForm={doToggleForm} />
        </UpgradeHeader>
        <FacebookLogin onClick={onFacebookLogin} loading={facebookSubmitting} />
        <OrBreak />
        {showLogin ? (
          <LoginForm onSubmit={onLoginSubmit} loading={submitting} error={error} />
        ) : (
          <CreateAccountForm
            onSubmit={onCreateSubmit}
            loading={submitting}
            error={error}
            receivePromotionsLabel={en.create.form.receivePromotions_label(config.brand)}
          />
        )}
        {!showLogin ? <Disclaimer location="registration" /> : null}
      </div>
    );
  };

  const renderPaymentForm = () => {
    if (isPremium) return window.location.assign('/account/subscription');
    /* eslint-disable no-nested-ternary */
    return (
      <>
        {isStripeCheckoutEnabled ? (
          pricingPlans ? (
            <section>
              <UpgradeHeader coupon={coupon} trialEligible={trialEligible} />
              <PricingPlans
                stripeCheckoutTreatment={stripeCheckoutTreatment}
                plans={pricingPlans}
                viewedCheckout={viewedCheckout}
                shouldRedirectToOnboarding={registrationRendered}
              />
              <Disclaimer />
              {!promotionId ? <CouponForm /> : null}
            </section>
          ) : (
            <Loading />
          )
        ) : plans ? (
          <Elements stripe={stripePromise}>
            <PaymentForm
              formLocation="upgrade"
              viewedCheckout={viewedCheckout}
              shouldRedirectToOnboarding={registrationRendered}
            >
              <UpgradeHeader
                coupon={coupon}
                trialEligible={trialEligible}
                selectedPlanId={selectedPlanId}
                trialPeriodDays={trialPeriodDays}
              />
            </PaymentForm>
          </Elements>
        ) : (
          <Loading />
        )}
      </>
    );
  };

  return subscriptionSuccess ? (
    <PremiumSuccess />
  ) : (
    <div>{accessToken ? renderPaymentForm() : renderRegistrationForm()}</div>
  );
};

export default Upgrade;
