import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import * as reactRedux from 'react-redux';
import * as nextRouter from 'next/router';
import PricingPlans from './PricingPlans';
import pricingPlansFixture from '../../fixtures/pricingPlans';
import { OLD } from '../../common/constants';
import * as entitlements from '../../store/selectors/entitlements';
import * as interactions from '../../store/selectors/interactions';
import * as user from '../../store/selectors/user';
import * as promotions from '../../store/selectors/promotion';

describe('PricingPlans', () => {
  const dispatchStub = sinon.stub();
  let useSelectorStub;
  let useDispatchStub;
  let getTrialEligibilityStub;
  let isFormSubmittingStub;
  let getUserErrorStub;
  let useRouterStub;
  let getFunnelConfigStub;

  const viewedCheckout = () => {};
  before(() => {
    useSelectorStub = sinon.stub(reactRedux, 'useSelector');
    useDispatchStub = sinon.stub(reactRedux, 'useDispatch');
    getTrialEligibilityStub = sinon.stub(entitlements, 'getTrialEligibility');
    isFormSubmittingStub = sinon.stub(interactions, 'isFormSubmitting');
    getUserErrorStub = sinon.stub(user, 'getUserError');
    useRouterStub = sinon.stub(nextRouter, 'useRouter');
    getFunnelConfigStub = sinon.stub(promotions, 'getFunnelConfig');

    useSelectorStub.callsArg(0);
    useDispatchStub.returns(dispatchStub);
  });
  after(() => {
    useSelectorStub.restore();
    useDispatchStub.restore();
    getTrialEligibilityStub.restore();
    isFormSubmittingStub.restore();
    getUserErrorStub.restore();
    useRouterStub.restore();
    getFunnelConfigStub.restore();
  });
  it('renders default "PricingPlans" component', () => {
    getTrialEligibilityStub.returns(true);
    isFormSubmittingStub.returns(false);
    getUserErrorStub.returns(null);
    useRouterStub.returns({ query: { selectedInterval: 'year' } });
    const wrapper = shallow(
      <PricingPlans
        stripeCheckoutTreatment={OLD}
        plans={pricingPlansFixture.plans}
        viewedCheckout={viewedCheckout}
      />,
    );
    expect(wrapper.find('.drop-down')).to.have.lengthOf(1);
    expect(wrapper.find('.pricing-plans__title').text().trim()).to.equal(
      'Choose your subscription',
    );
  });
  it('renders default "PricingPlans" component with trial length of promotion', () => {
    getFunnelConfigStub.returns({ trialLength: 30 });
    getTrialEligibilityStub.returns(true);
    isFormSubmittingStub.returns(false);
    getUserErrorStub.returns(null);
    useRouterStub.returns({ query: { selectedInterval: 'year' } });
    const wrapper = shallow(
      <PricingPlans
        stripeCheckoutTreatment={OLD}
        plans={pricingPlansFixture.plans}
        viewedCheckout={viewedCheckout}
      />,
    );
    expect(
      wrapper.contains(<div className="premium-plan__name--trial">30-Day Free Trial</div>),
    ).to.equal(true);
  });
});
