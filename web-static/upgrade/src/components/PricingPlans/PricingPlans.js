import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import dynamic from 'next/dynamic';
import { Button, Alert } from '@surfline/quiver-react';
import { TABLET_LARGE_WIDTH, trackEvent } from '@surfline/web-common';
import Loading from '../Loading';
import { getTrialEligibility } from '../../store/selectors/entitlements';
import { isFormSubmitting } from '../../store/selectors/interactions';
import { startSubmitSpinner } from '../../store/actions/interactions';
import { startStripeCheckout } from '../../store/actions/user';
import { getUserError } from '../../store/selectors/user';
import { OVER_UNDER, SIDE_BY_SIDE } from '../../common/constants';
import { ChevronAlt } from '../Icons';
import en from '../../international/translations/en';
import {
  getPlanName,
  getTrialText,
  getProjectedCost,
  getActualCost,
  formatTrialEndDate,
} from '../../utils/plansHelper';
import '../PremiumPlans/PremiumPlans.scss';
import './PricingPlans.scss';
import { getFunnelConfig, getPromotionId } from '../../store/selectors/promotion';
import { getCoupon } from '../../store/selectors/coupon';

const OverUnder = dynamic(() => import('../OverUnder/OverUnder'), {
  loading: () => <Loading />,
  ssr: false,
});

const SideBySide = dynamic(() => import('../SideBySide/SideBySide'), {
  loading: () => <Loading />,
  ssr: false,
});

/**
 * @param {Plan[]} plans
 */
const sortPlans = (plans) =>
  plans.sort((planA, planB) => {
    if (planA.amount > planB.amount) return -1;
    if (planA.amount < planB.amount) return 1;
    return 0;
  });

const PricingPlans = ({
  stripeCheckoutTreatment,
  plans,
  viewedCheckout,
  shouldRedirectToOnboarding,
}) => {
  const {
    query: { selectedInterval = 'year' },
  } = useRouter();
  const {
    upgradeFunnel: { freeTrialMessage },
  } = en;
  const dispatch = useDispatch();
  const [selectedPlan, setSelectedPlan] = useState(selectedInterval);
  const trialEligibility = useSelector(getTrialEligibility, shallowEqual);
  const submitting = useSelector(isFormSubmitting, shallowEqual);
  const error = useSelector(getUserError, shallowEqual);
  const promotionId = useSelector(getPromotionId);
  const funnelConfig = useSelector(getFunnelConfig, shallowEqual);
  const coupon = useSelector(getCoupon);
  // Actions
  const doStartSubmitSpinner = () => dispatch(startSubmitSpinner());
  const doStartStripeCheckout = (values) => dispatch(startStripeCheckout(values));

  // For supporting Drop Down View
  const [open, setOpen] = useState(false);
  const [sortedPlans, setSortedPlans] = useState(sortPlans(plans));

  const isTrialEligible = !coupon && trialEligibility;

  const getActiveClassName = (interval) =>
    classNames({
      'premium-plan': true,
      'premium-plan--hidden': !open && selectedPlan !== interval,
    });

  useEffect(() => {
    setSortedPlans(sortPlans(plans));
  }, [plans]);

  useEffect(() => {
    viewedCheckout();
  }, [viewedCheckout]);

  const trackEventHandler = (interval) => {
    const plan = sortedPlans.find((p) => p.interval === interval);
    if (plan) {
      trackEvent('Added Product', {
        platform: 'web',
        isMobileView: window.innerWidth <= TABLET_LARGE_WIDTH,
        quantity: 1,
        price: plan.amount / 100,
        paymentPlatform: 'stripe',
        productName: 'Surfline Premium',
        sku: plan.id,
        intervalCount: 1,
        currency: plan.currency.toUpperCase(),
        interval: plan.interval,
      });
    }
  };

  const selectPlanHandler = (interval) => {
    setSelectedPlan(interval);
    trackEventHandler(interval);
  };

  const toggleSelectedPlan = (interval) => {
    if (open && interval !== selectedPlan) {
      setSortedPlans(sortedPlans.reverse());
      setSelectedPlan(interval);
      trackEventHandler(interval);
    }
    setOpen(!open);
  };

  const submitButtonHandler = async () => {
    doStartSubmitSpinner();
    const { brand, interval, country, currency } = sortedPlans.find(
      (p) => p.interval === selectedPlan,
    );
    const { urlKey: promotionUrlKey } = funnelConfig || {};
    const payload = {
      brand,
      interval,
      currency,
      country,
      coupon,
      promotionId,
      shouldRedirectToOnboarding,
      promotionUrlKey,
    };
    await doStartStripeCheckout(payload);
  };

  if (stripeCheckoutTreatment === OVER_UNDER)
    return (
      <OverUnder
        plans={sortedPlans}
        selectedPlan={selectedPlan}
        trialEligible={isTrialEligible}
        selectPlanHandler={selectPlanHandler}
        submitButtonHandler={submitButtonHandler}
        submitting={submitting}
        error={error}
        funnelConfig={funnelConfig}
      />
    );
  if (stripeCheckoutTreatment === SIDE_BY_SIDE && sortedPlans.length > 1)
    return (
      <SideBySide
        plans={sortedPlans}
        selectedPlan={selectedPlan}
        trialEligible={isTrialEligible}
        selectPlanHandler={selectPlanHandler}
        submitButtonHandler={submitButtonHandler}
        submitting={submitting}
        error={error}
        funnelConfig={funnelConfig}
      />
    );

  return (
    <div className="drop-down pricing-plans">
      {error ? (
        <Alert style={{ textAlign: 'center' }} type="error">
          {error}
        </Alert>
      ) : null}
      <div className="pricing-plans__title">Choose your subscription</div>
      <div className="premium-plans__container">
        <div className="premium-plans">
          {sortedPlans.map(({ interval, amount, trialPeriodDays, symbol }) => (
            <div
              key={interval}
              className={getActiveClassName(interval)}
              onClick={() => toggleSelectedPlan(interval)}
              onKeyPress={() => toggleSelectedPlan(interval)}
            >
              <div className="premium-plan__details">
                <div className="premium-plan__name">
                  <div className="premium-plan__name--plan">{`${getPlanName(
                    interval,
                  )} Subscription`}</div>
                  <div className="premium-plan__name--trial">
                    {getTrialText(funnelConfig, trialPeriodDays, isTrialEligible)}
                  </div>
                </div>
                <div className="premium-plan__price">
                  <div className="premium-plan__price--cost">
                    {symbol}
                    {getProjectedCost(interval, amount)}
                  </div>
                  <div className="premium-plan__cost__hyphen">{' - '}</div>
                  <div className="premium-plan__price--total">
                    {getActualCost(interval, amount, symbol)}
                  </div>
                </div>
              </div>
              {!open && sortedPlans.length > 1 ? (
                <div className="premium-plan__action">
                  <div className="premium-plan__action--text">Select Plan</div>
                  <div className="premiium-plan__action--icon">
                    <ChevronAlt />
                  </div>
                </div>
              ) : null}
            </div>
          ))}
        </div>
      </div>
      {selectedPlan === 'year' && isTrialEligible ? (
        <div className="premium-plan__message">
          {freeTrialMessage(formatTrialEndDate(funnelConfig?.trialLength))}
        </div>
      ) : null}
      <div className="pricing-plans__button">
        <Button loading={submitting} onClick={submitButtonHandler}>
          {selectedPlan === 'year' && isTrialEligible ? 'Start Free Trial' : 'Go Premium'}
        </Button>
      </div>
    </div>
  );
};

PricingPlans.propTypes = {
  plans: PropTypes.arrayOf(
    PropTypes.shape({
      interval: PropTypes.string.isRequired,
      amount: PropTypes.number.isRequired,
      trialPeriodDays: PropTypes.number,
      symbol: PropTypes.string.isRequired,
    }),
  ).isRequired,
  stripeCheckoutTreatment: PropTypes.string.isRequired,
  viewedCheckout: PropTypes.func.isRequired,
  shouldRedirectToOnboarding: PropTypes.bool.isRequired,
};

export default PricingPlans;
