import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import PromotionError from './PromotionError';

describe('PromotionError', () => {
  it('renders promotion error component', () => {
    const expiredText =
      'This promotion has expired, but you can still sign-up for Surfline Premium.';
    const buttonText = 'START FREE TRIAL';
    const wrapper = shallow(<PromotionError />);
    expect(wrapper.find('.promotion-error__title')).to.have.lengthOf(1);
    expect(wrapper.find('p').text()).to.equal(expiredText);
    expect(wrapper.find('.quiver-button').text().toUpperCase()).to.equal(buttonText);
  });
});
