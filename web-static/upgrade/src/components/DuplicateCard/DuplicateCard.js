import PropTypes from 'prop-types';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { Button } from '@surfline/quiver-react';
import { amountToFixed } from '@surfline/web-common';
import en from '../../international/translations/en';
import CreditCard from '../CreditCard/CreditCard';
import { reloadFromDuplicateCard } from '../../store/actions/duplicateCard';
import { getPremiumPlans } from '../../store/selectors/plans';
import {
  getSelectedCardId,
  getSelectedCard,
  getDuplicateCardSource,
  getDuplicateCardPlan,
} from '../../store/selectors/cards';

import './DuplicateCard.scss';

const DuplicateCard = ({ submitting, error, submitPayment }) => {
  const dispatch = useDispatch();

  const selectedPlanId = useSelector(getDuplicateCardPlan);
  const selectedCardId = useSelector(getSelectedCardId, shallowEqual);
  const selectedCard = useSelector(getSelectedCard, shallowEqual);
  const plans = useSelector(getPremiumPlans, shallowEqual);
  const source = useSelector(getDuplicateCardSource, shallowEqual);
  const doReloadFromDuplicateCard = () => dispatch(reloadFromDuplicateCard());

  const { amount, symbol } = plans.find(({ id }) => id === selectedPlanId);
  return (
    <div className="duplicate-card">
      <p className="duplicate-card__description">
        {en.duplicateCard.description(amountToFixed(amount), symbol)}
      </p>
      {error && <div className="duplicate-card__error">{error}</div>}
      <CreditCard card={source ? source.card : selectedCard} />
      <Button
        loading={submitting}
        onClick={() =>
          submitPayment({
            plan: selectedPlanId,
            source,
            selectedCard: selectedCardId,
            trialEligible: false,
          })
        }
      >
        {en.duplicateCard.goPremium}
      </Button>
      <button type="button" className="duplicate-card__go-back" onClick={doReloadFromDuplicateCard}>
        {en.duplicateCard.goBack}
      </button>
    </div>
  );
};

DuplicateCard.propTypes = {
  submitting: PropTypes.bool,
  error: PropTypes.bool,
  submitPayment: PropTypes.func.isRequired,
};

DuplicateCard.defaultProps = {
  submitting: false,
  error: false,
};

export default DuplicateCard;
