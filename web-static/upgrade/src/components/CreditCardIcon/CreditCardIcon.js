import PropTypes from 'prop-types';
import VisaCard from '../Icons/Visa';
import MasterCard from '../Icons/Mastercard';
import AmexCard from '../Icons/Amex';
import DiscoverCard from '../Icons/Discover';
import DinersClubCard from '../Icons/DinersClub';
import JCBCard from '../Icons/JCB';
import UnionPayCard from '../Icons/Unionpay';

const CreditCardIcon = ({ ccBrand }) => {
  const ccFormatted = ccBrand.replace(/\s+/g, '-').toLowerCase();
  if (ccFormatted === 'visa') return <VisaCard />;
  if (ccFormatted === 'mastercard') return <MasterCard />;
  if (ccFormatted === 'american-express') return <AmexCard />;
  if (ccFormatted === 'discover') return <DiscoverCard />;
  if (ccFormatted === 'diners-club') return <DinersClubCard />;
  if (ccFormatted === 'jcb') return <JCBCard />;
  if (ccFormatted === 'unionpay') return <UnionPayCard />;
  return null;
};

CreditCardIcon.propTypes = {
  ccBrand: PropTypes.string.isRequired,
};

export default CreditCardIcon;
