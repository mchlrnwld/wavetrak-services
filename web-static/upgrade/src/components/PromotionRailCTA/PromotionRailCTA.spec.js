import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import PromotionRailCTA from './PromotionRailCTA';
import PromotionPartnerContent from './PromotionPartnerContent';
import PromotionValueProps from './PromotionValueProps';

describe('PromotionRailCTA', () => {
  it('renders promotion rail CTA', () => {
    const funnelConfig = { bgDesktopImgPath: 'path_to_img' };
    const wrapper = shallow(<PromotionRailCTA funnelConfig={funnelConfig} />);
    expect(wrapper.find('.promotion-rail-cta')).to.have.lengthOf(1);
    expect(wrapper.find('.promotion-rail-cta').get(0).props.style).to.have.property(
      'backgroundImage',
      'url(path_to_img)',
    );
    expect(wrapper.find('.promotion-rail-cta-container')).to.have.lengthOf(1);
    expect(wrapper.find('.promotion-rail-cta-partner-container')).to.have.lengthOf(1);
    expect(wrapper.find(PromotionPartnerContent)).to.have.lengthOf(1);
    expect(wrapper.find(PromotionValueProps)).to.have.lengthOf(1);
  });
});
