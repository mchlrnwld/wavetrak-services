import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import PromotionPartnerContent from './PromotionPartnerContent';

describe('PromotionPartnerContent', () => {
  it('renders promotion partner content with logo', () => {
    const funnelConfig = {
      logo: 'path_to_logo',
      logoAltText: 'logo_alt_text',
      detailCopy: 'detail_copy',
      promotionFeatures: { BrandLogo: () => {} },
    };
    const wrapper = shallow(<PromotionPartnerContent funnelConfig={funnelConfig} />);
    expect(wrapper.find('.promotion-partner-content')).to.have.lengthOf(1);
    expect(wrapper.find('.promotion-partner-content__logo')).to.have.lengthOf(1);
    expect(
      wrapper.find('.promotion-partner-content__partner_logo').get(0).props.style,
    ).to.have.property('backgroundImage', 'url(path_to_logo)');
    expect(wrapper.find('.promotion-partner-content__detail_copy').text()).to.be.equal(
      'detail_copy',
    );
  });

  it('renders promotion partner content with logo alt text', () => {
    const funnelConfig = {
      logo: undefined,
      logoAltText: 'logo_alt_text',
      detailCopy: 'detail_copy',
      promotionFeatures: { BrandLogo: () => {} },
    };
    const wrapper = shallow(<PromotionPartnerContent funnelConfig={funnelConfig} />);
    expect(wrapper.find('.promotion-partner-content__partner_logo_text').text()).to.be.equal(
      'logo_alt_text',
    );
  });
});
