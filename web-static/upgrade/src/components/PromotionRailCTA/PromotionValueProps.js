import PropTypes from 'prop-types';
import { Component } from 'react';

import './PromotionValueProps.scss';
/**
 * @description The `PromotionValueProps` component is used on the right rail.
 * It renders the brand specific value propositions for the Promotion.
 *
 * @param {object} props - The props for the component
 * @param {object} props.funnelConfig - The funnel configuration for the Promotion.
 *
 * @returns {JSX.Element} PromotionValueProps JSX
 */

class PromotionValueProps extends Component {
  customHeaderStyle = (brand) => {
    let style;
    switch (brand) {
      case 'bw':
        style = {
          'font-size': '18px',
        };
        break;
      case 'fs':
        style = {
          'font-size': '23px',
        };
        break;
      default:
        style = {};
    }
    return style;
  };

  /**
   * @description Function which takes a `feature` from the features array in the copy
   * and destructures it to render the icon and header.
   *
   * @param {object} feature - The feature
   * @param {Function} feature.Icon - Icon react component
   * @param {string} feature.header - Header string for the feature
   */
  renderFeature = ({ Icon, header }) => (
    <div key={header} className="feature">
      <Icon />
      <div className="feature__content">
        <h6 className="feature__content_header">{header}</h6>
      </div>
    </div>
  );

  /**
   * @description Function for creating the PromotionValueProps JSX
   * @param {object} funnelConfig - The funnel configuration for the Promotion.
   */
  renderValueProps = (funnelConfig) => {
    const { promotionFeatures } = funnelConfig;
    return (
      <div className="promotion-value-props">
        <div className="promotion-value-props__triangle" />
        <div className="promotion-value-props-container">
          <h4
            className="promotion-rail-cta__header"
            style={this.customHeaderStyle(funnelConfig.brand)}
          >
            {promotionFeatures.header}
          </h4>
          <div className="promotion-rail-cta__features">
            {promotionFeatures.features.map((feature) => this.renderFeature(feature))}
          </div>
        </div>
      </div>
    );
  };

  render() {
    const { funnelConfig } = this.props;
    return this.renderValueProps(funnelConfig);
  }
}

PromotionValueProps.propTypes = {
  funnelConfig: PropTypes.instanceOf(Object).isRequired,
};

export default PromotionValueProps;
