import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import PromotionMobileRailCTA from './PromotionMobileRailCTA';
import PromotionPartnerContent from './PromotionPartnerContent';

describe('PromotionMobileRailCTA', () => {
  it('renders promotion mobile rail CTA', () => {
    const funnelConfig = { bgMobileImgPath: 'path_to_img' };
    const wrapper = shallow(<PromotionMobileRailCTA funnelConfig={funnelConfig} />);
    expect(wrapper.find('.mobile-rail-cta')).to.have.lengthOf(1);
    expect(wrapper.find('.mobile-rail-cta').get(0).props.style).to.have.property(
      'backgroundImage',
      'url(path_to_img)',
    );
    expect(wrapper.find('.mobile-rail-cta__content')).to.have.lengthOf(1);
    expect(wrapper.find(PromotionPartnerContent)).to.have.lengthOf(1);
  });
});
