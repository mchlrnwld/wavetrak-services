const Layers = () => (
  <svg xmlns="http://www.w3.org/2000/svg" className="layers_icon" viewBox="0 0 44 46">
    <g fill="none" fillRule="evenodd" strokeWidth="1.6">
      <path stroke="#FFF" d="M8.832 27.765L2 32.342l19.366 12.654L42 32.342l-7.308-4.595" />
      <path stroke="#67D3DF" d="M8.774 18.62L2 23.158l19.366 12.654L42 23.158l-7.33-4.61" />
      <path stroke="#FFF" d="M21.366 1L2 13.974l19.366 12.654L42 13.974z" />
    </g>
  </svg>
);

export default Layers;
