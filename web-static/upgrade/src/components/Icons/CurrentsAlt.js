const CurrentsAlt = () => (
  <svg
    width="36px"
    height="37px"
    viewBox="0 0 36 37"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <defs />
    <g id="Extended-trials" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Fishtrack-Funnel---extended-trials" transform="translate(-1097.000000, -318.000000)">
        <rect fill="#FFFFFF" x="0" y="0" width="1440" height="909" />
        <g id="FT/currents" transform="translate(1099.000000, 319.000000)" strokeWidth="1.5">
          <polyline
            id="Path-2-Copy-2"
            stroke="#011D38"
            points="5.46570925 21.2905811 0 24.9314224 15.4928383 34.9967957 32 24.9314224 26.1532996 21.2761088"
          />
          <polyline
            id="Path-2"
            stroke="#67D3DF"
            points="5.41905672 14.0160237 0 17.6257886 15.4928383 27.6911619 32 17.6257886 26.135327 13.9592386"
          />
          <polygon
            id="Path-2-Copy"
            stroke="#011D38"
            points="15.4928383 0 0 10.3201548 15.4928383 20.3855281 32 10.3201548"
          />
        </g>
      </g>
    </g>
  </svg>
);

export default CurrentsAlt;
