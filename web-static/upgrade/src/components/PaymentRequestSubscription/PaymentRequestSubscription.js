import { useMemo, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';
import { useStripe, PaymentRequestButtonElement } from '@stripe/react-stripe-js';
import { getPremiumPlans, getSelectedPlanId } from '../../store/selectors/plans';
import OrBreak from '../OrBreak/OrBreak';

const usePaymentRequest = ({ options, paymentHandler }) => {
  const stripe = useStripe();
  const [paymentRequest, setPaymentRequest] = useState(null);
  const [canMakePayment, setCanMakePayment] = useState(false);
  const [digitalWallet, setDigitalWallet] = useState(false);
  const selectedPlanId = useSelector(getSelectedPlanId);

  useEffect(() => {
    if (stripe && paymentRequest === null) {
      const pr = stripe.paymentRequest(options);
      setPaymentRequest(pr);
    }
  }, [stripe, options, paymentRequest]);

  useEffect(() => {
    let subscribed = true;
    if (paymentRequest) {
      paymentRequest.canMakePayment().then((res) => {
        if (res && subscribed) {
          setCanMakePayment(true);
          setDigitalWallet(res);
        }
      });
    }

    return () => {
      subscribed = false;
    };
  }, [paymentRequest]);

  useEffect(() => {
    if (paymentRequest) {
      paymentRequest.on('token', async (ev) => {
        const { applePay } = digitalWallet;
        const paymentSource = applePay ? 'Apple Pay' : 'other';
        const paymentValues = {
          plan: selectedPlanId,
          source: ev.token,
          selectedCard: null,
          digitalWallet: paymentSource,
        };
        try {
          await paymentHandler(paymentValues);
          ev.complete('success');
        } catch (err) {
          ev.complete('fail');
        }
      });
    }
    return () => {
      if (paymentRequest) {
        paymentRequest.off('token');
      }
    };
  }, [paymentRequest, paymentHandler, digitalWallet, selectedPlanId]);

  return canMakePayment ? paymentRequest : null;
};
const useOptions = (paymentRequest) => {
  const options = useMemo(
    () => ({
      paymentRequest,
      style: {
        paymentRequestButton: {
          height: '48px',
          borderRadius: '5px',
          padding: '0px',
        },
      },
    }),
    [paymentRequest],
  );
  return options;
};

const PaymentRequestSubscription = ({ paymentHandler }) => {
  const plans = useSelector(getPremiumPlans, shallowEqual);
  const selectedPlanId = useSelector(getSelectedPlanId);
  const { amount, currency, symbol, interval } = plans.find(({ id }) => id === selectedPlanId);
  const cost = amount / 100;
  const label = interval.includes('year')
    ? `Billed ${symbol}${cost} once a year`
    : 'Billed month-to-month';
  const paymentRequest = usePaymentRequest({
    options: {
      country: 'US',
      currency,
      total: {
        label,
        amount,
      },
    },
    paymentHandler,
  });

  const handlePaymentRequestButtonClick = () => {
    paymentRequest.update({
      currency,
      total: {
        label,
        amount,
      },
    });
  };

  const options = useOptions(paymentRequest);
  return paymentRequest ? (
    <>
      <div className="section-label section-label--digitalwallet">Or pay using</div>
      <OrBreak />
      <div>
        <PaymentRequestButtonElement options={options} onClick={handlePaymentRequestButtonClick} />
      </div>
    </>
  ) : null;
};

PaymentRequestSubscription.propTypes = {
  paymentHandler: PropTypes.func.isRequired,
};

export default PaymentRequestSubscription;
