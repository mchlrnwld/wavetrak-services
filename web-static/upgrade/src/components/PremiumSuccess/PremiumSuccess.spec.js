/* eslint-disable no-unused-expressions */
import React from 'react';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import * as reactRedux from 'react-redux';
import * as queryString from 'query-string';
import { Alert } from '@surfline/quiver-react';
import PremiumSuccess from './PremiumSuccess';
import * as promotions from '../../store/selectors/promotion';
import * as interactions from '../../store/selectors/interactions';
import * as subscription from '../../store/selectors/subscription';
import Loading from '../Loading';

describe('PremiumSuccess', () => {
  const dispatchStub = sinon.stub();
  let useSelectorStub;
  let useDispatchStub;
  let queryStringStub;
  let getFunnelConfigStub;
  let getIsStripeCheckoutEnabledStub;
  let submittingStub;
  let verificationStatusStub;
  const { location } = window;
  let clock;

  before(() => {
    useSelectorStub = sinon.stub(reactRedux, 'useSelector').callsArg(0);
    useDispatchStub = sinon.stub(reactRedux, 'useDispatch');
    queryStringStub = sinon.stub(queryString, 'parse');
    getIsStripeCheckoutEnabledStub = sinon.stub(interactions, 'getIsStripeCheckoutEnabled');
    submittingStub = sinon.stub(interactions, 'isFormSubmitting');
    verificationStatusStub = sinon.stub(subscription, 'getSubcriptionVerificationStatus');
    useDispatchStub.returns(dispatchStub);
    delete window.location;
    window.location = {
      assign: sinon.spy(),
    };
  });

  after(() => {
    useSelectorStub.restore();
    useDispatchStub.restore();
    queryStringStub.restore();
    submittingStub.restore();
    verificationStatusStub.restore();
    getIsStripeCheckoutEnabledStub.restore();
    window.location = location;
  });

  beforeEach(() => {
    getFunnelConfigStub = sinon.stub(promotions, 'getFunnelConfig');
    clock = sinon.useFakeTimers();
  });

  afterEach(() => {
    getFunnelConfigStub.restore();
    clock.restore();
  });

  it('redirects if success URL is set', () => {
    queryStringStub.returns({});
    getFunnelConfigStub.returns({ successUrl: '/onboarding' });
    const wrapper = mount(<PremiumSuccess />);
    clock.tick(2000);
    expect(wrapper.find('.premium-success__container')).to.have.lengthOf(1);
    expect(window.location.assign).to.have.been.calledWith('/onboarding');
  });

  it('renders success page if no success URL is set', () => {
    queryStringStub.returns({});
    getFunnelConfigStub.returns(null);
    const wrapper = shallow(<PremiumSuccess />);
    clock.tick(2000);
    expect(wrapper.find('.premium-success__container')).to.have.lengthOf(1);
    expect(window.location.assign).to.have.not.been.called;
  });

  it('render Loading when verifying the subscription for Stripe Checkout Success', () => {
    getIsStripeCheckoutEnabledStub.returns(true);
    queryStringStub.returns({ shouldRedirectToOnboarding: true });
    submittingStub.returns(true);
    verificationStatusStub.returns(null);
    getFunnelConfigStub.returns(null);
    const wrapper = shallow(<PremiumSuccess />);
    expect(wrapper.find('.premium-success__container')).to.have.lengthOf(0);
    expect(wrapper.find(Loading).dive().find('.loading')).to.have.lengthOf(1);
  });

  it('render success message if the subscription verification is a success for Stripe Checkout', () => {
    getIsStripeCheckoutEnabledStub.returns(true);
    queryStringStub.returns({ shouldRedirectToOnboarding: true });
    submittingStub.returns(false);
    verificationStatusStub.returns('success');
    getFunnelConfigStub.returns(null);
    const wrapper = shallow(<PremiumSuccess />);
    expect(wrapper.find('.premium-success__container')).to.have.lengthOf(1);
    expect(wrapper.find('.premium-success__container').text()).to.contain('Success');
  });

  it('render failure message if the subscription verification is a failure for Stripe Checkout', () => {
    getIsStripeCheckoutEnabledStub.returns(true);
    queryStringStub.returns({ shouldRedirectToOnboarding: true });
    submittingStub.returns(false);
    verificationStatusStub.returns('fail');
    getFunnelConfigStub.returns(null);
    const wrapper = shallow(<PremiumSuccess />);
    expect(wrapper.find('.premium-success__error')).to.have.lengthOf(1);
    expect(wrapper.find(Alert).dive().find('.quiver-alert').text()).to.contain(
      'It looks like something went wrong while processing your subscription',
    );
  });
});
