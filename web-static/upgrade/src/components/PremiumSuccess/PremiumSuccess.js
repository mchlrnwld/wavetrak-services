import { useEffect } from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import queryString from 'query-string';
import { useMount } from 'react-use';
import { Alert } from '@surfline/quiver-react';
import getConfig from '../../config';
import en from '../../international/translations/en';
import './PremiumSuccess.scss';
import { getFunnelConfig } from '../../store/selectors/promotion';
import { getSubcriptionVerificationStatus } from '../../store/selectors/subscription';
import { verifySubscription } from '../../store/actions/subscription';
import { getIsStripeCheckoutEnabled, isFormSubmitting } from '../../store/selectors/interactions';
import { redirectToOnboarding, redirectToMultiCam } from '../../store/actions/interactions';
import Loading from '../Loading';

const { redirectUrl } = getConfig();

const PremiumSuccess = () => {
  const dispatch = useDispatch();
  const { shouldRedirectToOnboarding = true } = queryString.parse(window?.location?.search, {
    parseBooleans: true,
  });
  const isStripeCheckoutEnabled = useSelector(getIsStripeCheckoutEnabled);
  const funnelConfig = useSelector(getFunnelConfig, shallowEqual);
  const submitting = useSelector(isFormSubmitting);
  const verificationStatus = useSelector(getSubcriptionVerificationStatus);
  const { successUrl, successMessage, successHeader } = funnelConfig || {};
  const readyToRedirect = verificationStatus === 'success' && !submitting;
  useMount(async () => {
    if (isStripeCheckoutEnabled) await dispatch(verifySubscription());
  });

  useEffect(() => {
    if (successUrl && !isStripeCheckoutEnabled) {
      setTimeout(() => {
        window.location.assign(successUrl);
      }, 2000);
    }
  }, [successUrl, isStripeCheckoutEnabled]);
  useEffect(() => {
    if (readyToRedirect) {
      if (shouldRedirectToOnboarding) {
        dispatch(redirectToOnboarding());
      } else {
        // For regular upgrade funnel users that have been onboarded, send them to the multi-cam page.
        dispatch(redirectToMultiCam());
      }
    }
  }, [readyToRedirect, shouldRedirectToOnboarding, successUrl, dispatch]);

  const { subTitle } = isStripeCheckoutEnabled ? en.payment_verification : en.payment_success;
  let { message } = isStripeCheckoutEnabled ? en.payment_verification : en.payment_success;
  let header = 'Success';
  if (funnelConfig) {
    message = successMessage || message;
    header = successHeader || header;
  }

  const successRedirectUrl = successUrl || redirectUrl;

  if (isStripeCheckoutEnabled) {
    if (submitting || !verificationStatus) return <Loading />;
    if (verificationStatus === 'fail') {
      const errorMessage = en.payment_verification.error;
      return (
        <div className="premium-success__error">
          <Alert style={{ textAlign: 'center' }} type="error">
            {errorMessage}
          </Alert>
        </div>
      );
    }
  }

  return (
    <div className="premium-success__container">
      <h4>{header}</h4>
      <div className="premium-success__subtitle">{subTitle}</div>
      <div className="premium-success__message">{message}</div>
      <a className="quiver-button" type="button" href={successRedirectUrl}>
        {en.payment_success.linkText}
      </a>
    </div>
  );
};

export default PremiumSuccess;
