import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import classNames from 'classnames';
import { TABLET_LARGE_WIDTH, trackEvent } from '@surfline/web-common';

import { ChevronAlt } from '../Icons';
import { getCoupon } from '../../store/selectors/coupon';
import './PremiumPlans.scss';

/**
 * @typedef {object} Plan
 * @property {string} id
 * @property {number} amount
 * @property {number} trialPeriodDays
 * @property {string} country
 * @property {string} currency
 * @property {string} description
 * @property {string} entitlementKey
 * @property {string} interval
 * @property {string} intervalCount
 * @property {string} iso
 * @property {string} symbol
 * @property {'0' | '1' | boolean} upgradable
 */

/**
 * @param {Plan['id']} id
 */
const getPlanName = (interval) => (interval.includes('year') ? 'Yearly' : 'Monthly');

/**
 * @param {Plan['trialPeriodDays']} trialPeriodDays
 * @param {boolean} trialEligible
 * @returns {string}
 */
const getTrialText = (trialPeriodDays, trialEligible) => {
  if (!trialPeriodDays) return null;
  if (!trialEligible) return null;
  return `${trialPeriodDays}-Day Free Trial`;
};

/**
 * @param {Plan['id']} id
 * @param {Plan['amount']} amount
 * @returns {number}
 */
const getProjectedCost = (interval, amount) =>
  interval.includes('year') ? parseFloat(amount / 100 / 12).toFixed(2) : amount / 100;

/**
 * @param {Plan['id']} id
 * @param {Plan['amount']} amount
 * @param {Plan['symbol']} symbol
 * @returns {string}
 */
const getActualCost = (interval, amount, symbol) =>
  interval.includes('year')
    ? `/month - Billed ${symbol}${amount / 100} once a year`
    : '/month - Billed month-to-month';

/**
 * @param {Plan[]} plans
 */
const sortPlans = (plans) =>
  plans.sort((planA, planB) => {
    if (planA.id < planB.id) return -1;
    if (planA.id > planB.id) return 1;
    return 0;
  });

/**
 * @param {object} props
 * @param {Plan[]} props.plans
 * @param {string} props.selectedPlanId
 * @param {(planId: string) => void} props.toggleSelectedPlanId
 * @param {boolean} props.trialEligible
 */
const PremiumPlans = ({ plans, selectedPlanId, toggleSelectedPlanId, trialEligible }) => {
  const [statePlans, setStatePlans] = useState(sortPlans(plans));
  const [open, setOpen] = useState(false);

  const coupon = useSelector(getCoupon);

  const getActiveClassName = (id) =>
    classNames({
      'premium-plan': true,
      'premium-plan--hidden': !open && selectedPlanId !== id,
    });

  /**
   * @param {string} id
   */
  const toggleSelectedPlan = (id) => {
    if (open && id !== selectedPlanId) {
      setStatePlans(statePlans.reverse());
      toggleSelectedPlanId(id);
      const plan = plans.find((p) => p.id === id);
      if (plan) {
        trackEvent('Added Product', {
          platform: 'web',
          isMobileView: window.innerWidth <= TABLET_LARGE_WIDTH,
          quantity: 1,
          price: plan.amount / 100,
          paymentPlatform: 'stripe',
          productName: 'Surfline Premium',
          sku: plan.id,
          intervalCount: 1,
          currency: plan.currency.toUpperCase(),
          interval: plan.interval,
        });
      }
    }
    setOpen(!open);
  };

  useEffect(() => {
    setStatePlans(sortPlans(plans));
  }, [plans]);

  return (
    <div className="premium-plans__container">
      <div className="premium-plans">
        {statePlans.map(({ id, amount, trialPeriodDays, symbol, interval }) => (
          <div
            key={id}
            className={getActiveClassName(id)}
            onClick={() => toggleSelectedPlan(id)}
            onKeyPress={() => toggleSelectedPlan(id)}
          >
            <div className="premium-plan__details">
              <div className="premium-plan__name">
                <div className="premium-plan__name--plan">{`${getPlanName(
                  interval,
                )} Subscription`}</div>
                {!coupon ? (
                  <div className="premium-plan__name--trial">
                    {getTrialText(trialPeriodDays, trialEligible)}
                  </div>
                ) : null}
              </div>
              <div className="premium-plan__price">
                <div className="premium-plan__price--cost">
                  {symbol}
                  {getProjectedCost(interval, amount)}
                </div>
                <div className="premium-plan__price--total">
                  {getActualCost(interval, amount, symbol)}
                </div>
              </div>
            </div>
            {!open && plans.length > 1 ? (
              <div className="premium-plan__action">
                <div className="premium-plan__action--text">Select Plan</div>
                <div className="premiium-plan__action--icon">
                  <ChevronAlt />
                </div>
              </div>
            ) : null}
          </div>
        ))}
      </div>
    </div>
  );
};

PremiumPlans.propTypes = {
  plans: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      trialPeriodDays: PropTypes.number,
      amount: PropTypes.number,
    }),
  ),
  selectedPlanId: PropTypes.string.isRequired,
  toggleSelectedPlanId: PropTypes.func.isRequired,
  trialEligible: PropTypes.bool,
};

PremiumPlans.defaultProps = {
  plans: null,
  trialEligible: true,
};

export default PremiumPlans;
