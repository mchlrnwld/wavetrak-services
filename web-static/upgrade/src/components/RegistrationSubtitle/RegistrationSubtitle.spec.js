import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import RegistrationSubtitle from './RegistrationSubtitle';

describe('RegistrationSubtitle', () => {
  it('renders "Sign Up" text', () => {
    const wrapper = shallow(<RegistrationSubtitle showLogin />);
    expect(wrapper.find('button').text().trim()).to.equal('Sign Up');
  });

  it('renders "Sign In" text', () => {
    const wrapper = shallow(<RegistrationSubtitle showLogin={false} />);
    expect(wrapper.find('button').text().trim()).to.equal('Sign In');
  });
});
