import PropTypes from 'prop-types';
import './RegistrationSubtitle.scss';

const RegistrationSubtitle = ({ showLogin, doToggleForm }) =>
  showLogin ? (
    <p>
      Need an account?
      <button
        type="button"
        className="register-link__action"
        onClick={() => doToggleForm(showLogin)}
      >
        {' Sign Up'}
      </button>
    </p>
  ) : (
    <p>
      Already have an account?
      <button
        type="button"
        className="register-link__action"
        onClick={() => doToggleForm(showLogin)}
      >
        {' Sign In'}
      </button>
    </p>
  );

RegistrationSubtitle.propTypes = {
  showLogin: PropTypes.bool.isRequired,
  doToggleForm: PropTypes.func.isRequired,
};

export default RegistrationSubtitle;
