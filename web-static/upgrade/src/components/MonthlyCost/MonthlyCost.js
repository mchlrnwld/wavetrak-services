import PropTypes from 'prop-types';
import classNames from 'classnames';

import './MonthlyCost.scss';

const MonthlyCost = ({ cost, theme, brand, symbol }) => {
  const parseCost = () => {
    const numberString = cost.toString();
    const decimalIndex = numberString.indexOf('.');
    const dollars = numberString.substring(
      0,
      decimalIndex > -1 ? decimalIndex : numberString.length,
    );
    const cents = decimalIndex > -1 ? numberString.substring(decimalIndex + 1) : '';
    return { dollars, cents };
  };

  const { dollars, cents } = parseCost();
  const containerClasses = classNames({
    monthlyCost__container: true,
    'monthlyCost__container--alt': theme === 'alt',
    'monthlyCost__container--overview': theme === 'overview',
  });

  return (
    <div className={`monthlyCost__container--${brand}`}>
      <div className={containerClasses}>
        <sup className="monthlyCost__currency">{symbol}</sup>
        <h3 className="monthlyCost__dollars">{dollars}</h3>
        <span className="monthlyCost__centsPerMonth">
          <sup>{cents}</sup>
          <sub>/mo</sub>
        </span>
      </div>
    </div>
  );
};

MonthlyCost.propTypes = {
  cost: PropTypes.string,
  theme: PropTypes.string,
  brand: PropTypes.string,
  symbol: PropTypes.string,
};

MonthlyCost.defaultProps = {
  brand: 'default',
  cost: '7.99',
  theme: null,
  symbol: '$',
};

export default MonthlyCost;
