import { addDays, addYears, format } from 'date-fns';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { getTrialEligibility } from '../../store/selectors/entitlements';
import {
  getSelectedPlanId,
  getCurrencySymbol,
  getTrialPeriodDays,
} from '../../store/selectors/plans';

import './PlanBillingText.scss';

const formatTrialEndDate = () => {
  const trialEnd = addDays(new Date(), 15);
  return format(trialEnd, 'MM/dd/yy');
};
const getRenewalDate = () => {
  const trialEnd = addDays(new Date(), 15);
  const renewal = addYears(trialEnd, 1);
  return format(renewal, 'MM/dd/yy');
};

const getSubtitleText = (plan) => {
  const { trialEligible, trialPeriodDays, coupon, renewalPrice, interval, currency, symbol } = plan;
  if (trialEligible && coupon) {
    if (interval === 'year') {
      return `Discounted subscriptions are not eligible for free trials. Your subscription will auto-renew annually at the full subscription price of ${symbol}${
        renewalPrice / 100
      } after the first year.`;
    }
    return `Discounted subscriptions are not eligible for free trials. Billing is in ${currency} and begins today.`;
  }
  let text = `Annual billing begins after free trial ends on ${formatTrialEndDate()} and recurs on ${getRenewalDate()}. Billing is in ${currency}.`;
  if (!trialEligible || !trialPeriodDays) {
    if (interval === 'year' && coupon) {
      return `Your subscription will auto-renew annually at the full subscription price of ${symbol}${
        renewalPrice / 100
      } after the first year.`;
    }
    text = `Billing is in ${currency} and begins today.`;
  }
  return text;
};

const PlanBillingText = ({ plans, coupon, currency }) => {
  const symbol = useSelector(getCurrencySymbol);
  const trialEligible = useSelector(getTrialEligibility);
  const trialPeriodDays = useSelector(getTrialPeriodDays);
  const selectedPlanId = useSelector(getSelectedPlanId);
  const { renewalPrice, interval } = plans.find(({ id }) => id === selectedPlanId);
  const plan = {
    trialEligible,
    trialPeriodDays,
    coupon,
    renewalPrice,
    interval,
    currency: currency.toUpperCase(),
    symbol,
  };
  return <div className="billing-text">{getSubtitleText(plan)}</div>;
};

PlanBillingText.propTypes = {
  plans: PropTypes.objectOf(PropTypes.shape).isRequired,
  coupon: PropTypes.string,
  currency: PropTypes.string,
};

PlanBillingText.defaultProps = {
  coupon: null,
  currency: 'USD',
};

export default PlanBillingText;
