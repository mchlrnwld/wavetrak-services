import './Loading.scss';
import { Spinner } from '@surfline/quiver-react';

export default () => (
  <div className="loading">
    <Spinner />
  </div>
);
