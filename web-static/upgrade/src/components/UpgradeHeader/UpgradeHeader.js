import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';
import brandToString from '../../utils/brandToString';
import en from '../../international/translations/en';
import { getCurrencySymbol, getComputedMonthlyCost } from '../../store/selectors/plans';
import { getFunnelConfig } from '../../store/selectors/promotion';
import { getAccessToken } from '../../store/selectors/auth';

import './UpgradeHeader.scss';

const UpgradeHeader = ({ trialEligible, children, hideSubtitle }) => {
  const getTitleText = (funnelConfig, accessToken) => {
    if (funnelConfig) {
      const { paymentHeader, signUpHeader } = funnelConfig;
      if (paymentHeader && signUpHeader) {
        return accessToken ? paymentHeader : signUpHeader;
      }
    }

    if (!trialEligible)
      return 'It looks like you have already taken a free trial or promotional offer.';
    return `Start your ${brandToString()} Premium subscription today!`;
  };
  const symbol = useSelector(getCurrencySymbol);
  const computedMonthlyCost = useSelector(getComputedMonthlyCost);

  const funnelConfig = useSelector(getFunnelConfig, shallowEqual);
  const accessToken = useSelector(getAccessToken);

  const {
    upgradeFunnel: { upgradeHeaderSubtitle },
  } = en;
  return (
    <div className="upgrade-header">
      <h1 className="upgrade-header__title">{getTitleText(funnelConfig, accessToken)}</h1>
      {hideSubtitle ? (
        <div>
          <p className="upgrade-header__subtitle">
            {upgradeHeaderSubtitle(symbol, computedMonthlyCost)}
          </p>
          {children}
        </div>
      ) : null}
    </div>
  );
};

UpgradeHeader.propTypes = {
  trialEligible: PropTypes.bool,
  children: PropTypes.node,
  hideSubtitle: PropTypes.bool,
};

UpgradeHeader.defaultProps = {
  trialEligible: true,
  hideSubtitle: false,
  children: null,
};

export default UpgradeHeader;
