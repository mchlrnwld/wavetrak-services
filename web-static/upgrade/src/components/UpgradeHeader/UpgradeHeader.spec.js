import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import * as reactRedux from 'react-redux';

import UpgradeHeader from './UpgradeHeader';
import * as plans from '../../store/selectors/plans';
import * as promotions from '../../store/selectors/promotion';
import * as auth from '../../store/selectors/auth';
import brandToString from '../../utils/brandToString';

describe('UpgradeHeader', () => {
  let getCurrencySymbolStub;
  let getComputedMonthlyCostSelectorStub;
  let getFunnelConfigStub;
  let getPremiumPlansStub;
  let getAccessTokenStub;
  let useSelectorStub;

  const signUpHeader = 'Sign up for a 30 day free trial';
  const paymentHeader = 'Sign up for a 30 day free trial';
  const funnelConfig = { signUpHeader, paymentHeader };

  before(() => {
    useSelectorStub = sinon.stub(reactRedux, 'useSelector');
    getComputedMonthlyCostSelectorStub = sinon.stub(plans, 'getComputedMonthlyCost');
    getCurrencySymbolStub = sinon.stub(plans, 'getCurrencySymbol');
    getPremiumPlansStub = sinon.stub(plans, 'getPremiumPlans');
    getFunnelConfigStub = sinon.stub(promotions, 'getFunnelConfig');
    getAccessTokenStub = sinon.stub(auth, 'getAccessToken');
    useSelectorStub.callsArg(0);
    getComputedMonthlyCostSelectorStub.returns(98.55);
    getCurrencySymbolStub.returns('$');
  });

  after(() => {
    useSelectorStub.restore();
    getCurrencySymbolStub.restore();
    getComputedMonthlyCostSelectorStub.restore();
    getPremiumPlansStub.restore();
    getFunnelConfigStub.restore();
    getAccessTokenStub.restore();
  });

  it('renders trialeligible header', () => {
    const headerText = `Start your ${brandToString()} Premium subscription today!`;
    const wrapper = shallow(<UpgradeHeader trialEligible />);
    expect(wrapper.find('.upgrade-header')).to.have.lengthOf(1);
    expect(wrapper.find('.upgrade-header__title').text()).to.equal(headerText);
  });

  it('renders non-trialeligible header', () => {
    const headerText = 'It looks like you have already taken a free trial or promotional offer.';
    const wrapper = shallow(<UpgradeHeader trialEligible={false} />);
    expect(wrapper.find('.upgrade-header')).to.have.lengthOf(1);
    expect(wrapper.find('.upgrade-header__title').text()).to.equal(headerText);
  });

  it('renders promotion header if available', () => {
    getFunnelConfigStub.returns(funnelConfig);
    const wrapper = shallow(<UpgradeHeader trialEligible />);
    expect(wrapper.find('.upgrade-header')).to.have.lengthOf(1);
    expect(wrapper.find('.upgrade-header__title').text()).to.equal(signUpHeader);
  });
});
