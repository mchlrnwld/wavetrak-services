import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import SideBySide from './SideBySide';
import pricingPlansFixture from '../../fixtures/pricingPlans';

describe('SideBySide', () => {
  const selectPlanHandler = () => {};
  const submitButtonHandler = () => {};

  it('renders "SideBySide" component', () => {
    const wrapper = shallow(
      <SideBySide
        plans={pricingPlansFixture.plans}
        selectedPlan="month"
        trialEligible
        selectPlanHandler={selectPlanHandler}
        submitButtonHandler={submitButtonHandler}
        submitting={false}
        error={null}
      />,
    );
    expect(wrapper.find('.side-by-side')).to.have.lengthOf(1);
    expect(wrapper.find('.pricing-plans__title').text().trim()).to.equal(
      'Choose your subscription',
    );
    expect(
      wrapper.find('.pricing-plan--selected').contains([<span>Monthly Subscription</span>]),
    ).to.be.equal(true);
  });
});
