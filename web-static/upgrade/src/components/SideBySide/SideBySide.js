import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Button, Alert } from '@surfline/quiver-react';
import {
  getPlanName,
  getTrialText,
  getProjectedCost,
  getActualCost,
  formatTrialEndDate,
} from '../../utils/plansHelper';
import en from '../../international/translations/en';
import './SideBySide.scss';

const {
  upgradeFunnel: { freeTrialMessage },
} = en;
const SideBySide = ({
  plans,
  selectedPlan,
  trialEligible,
  selectPlanHandler,
  submitButtonHandler,
  submitting,
  error,
  funnelConfig,
}) => (
  <div className="side-by-side pricing-plans">
    {error ? (
      <Alert style={{ textAlign: 'center' }} type="error">
        {error}
      </Alert>
    ) : null}
    <div className="pricing-plans__title">Choose your subscription</div>
    <div
      className={classNames({
        'pricing-plans__container': true,
        'pricing-plans__container--trial-eligible': trialEligible,
      })}
    >
      {plans.map(({ interval, amount, trialPeriodDays, symbol }) => {
        const getSelectedClassName = () =>
          classNames({
            'pricing-plan': true,
            'pricing-plan--selected': selectedPlan === interval,
            'pricing-plan--monthly': interval === 'month',
            'pricing-plan--trial-eligible': trialEligible,
          });
        return (
          <div className="pricing-plan__container" key={interval}>
            <div
              className={getSelectedClassName()}
              onClick={() => selectPlanHandler(interval)}
              onKeyPress={() => selectPlanHandler(interval)}
            >
              <div className="pricing-plan__name">
                <span>{`${getPlanName(interval)} Subscription`}</span>
                <div className="pricing-plan__trialtext">
                  {getTrialText(funnelConfig, trialPeriodDays, trialEligible)}
                </div>
              </div>
              <div className="pricing-plan__cost">
                <div className="pricing-plan__cost-symbol">
                  {symbol}
                  {getProjectedCost(interval, amount)}{' '}
                  <div className="pricing-plan__cost-interval">/month</div>
                </div>
                <div className="pricing-plan__cost-total">
                  {getActualCost(interval, amount, symbol)}
                </div>
              </div>
              {getTrialText(funnelConfig, trialPeriodDays, trialEligible) ? (
                <div className="pricing-plan__trialtext_bottom-container">
                  <div className="pricing-plan__trialtext_bottom">
                    {getTrialText(funnelConfig, trialPeriodDays, trialEligible)}
                  </div>
                </div>
              ) : null}
            </div>
            {interval === 'year' && trialEligible ? (
              <div className="pricing-plan__message">
                <div>{freeTrialMessage(formatTrialEndDate(funnelConfig?.trialLength))}</div>
              </div>
            ) : null}
          </div>
        );
      })}
    </div>
    <div className="pricing-plans__button">
      <Button loading={submitting} onClick={submitButtonHandler}>
        {selectedPlan === 'year' && trialEligible ? 'Start Free Trial' : 'Go Premium'}
      </Button>
    </div>
  </div>
);

SideBySide.propTypes = {
  plans: PropTypes.arrayOf(
    PropTypes.shape({
      interval: PropTypes.string.isRequired,
      amount: PropTypes.number.isRequired,
      trialPeriodDays: PropTypes.number,
      symbol: PropTypes.string.isRequired,
    }),
  ).isRequired,
  selectedPlan: PropTypes.string.isRequired,
  trialEligible: PropTypes.bool.isRequired,
  selectPlanHandler: PropTypes.func.isRequired,
  submitButtonHandler: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  error: PropTypes.string,
  funnelConfig: PropTypes.shape({
    trialLength: PropTypes.number,
  }),
};

SideBySide.defaultProps = {
  error: null,
  funnelConfig: null,
};

export default SideBySide;
