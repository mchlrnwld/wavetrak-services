import { useState, useEffect } from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { Alert, Button } from '@surfline/quiver-react';
import PaymentRequestSubscription from '../PaymentRequestSubscription/PaymentRequestSubscription';
import Disclaimer from '../Disclaimer/Disclaimer';
import CreditCard from '../CreditCard/CreditCard';
import PremiumPlans from '../PremiumPlans/PremiumPlans';
import PlanBillingText from '../PlanBillingText/PlanBillingText';
import CouponForm from '../CouponForm/CouponForm';
import DuplicateCard from '../DuplicateCard/DuplicateCard';
import { startSubmitSpinner } from '../../store/actions/interactions';
import { submitSubscription } from '../../store/actions/subscription';
import { toggleSelectedPlanId } from '../../store/actions/plans';
import { setSelectedCardSource, toggleShowCards } from '../../store/actions/cards';
import { getAccessToken } from '../../store/selectors/auth';
import { getTrialEligibility } from '../../store/selectors/entitlements';
import { getSelectedPlanId, getPremiumPlans } from '../../store/selectors/plans';
import {
  getCreditCards,
  getDefaultSource,
  getSelectedCardId,
  getShowCards,
  getShowDuplicateCard,
  getCardError,
} from '../../store/selectors/cards';
import { isFormSubmitting } from '../../store/selectors/interactions';
import { getUserError } from '../../store/selectors/user';
import { getCoupon } from '../../store/selectors/coupon';

import './PaymentForm.scss';

const APP_ENV = process.env.APP_ENV || 'development';

const PaymentForm = ({ viewedCheckout, formLocation, shouldRedirectToOnboarding, children }) => {
  const dispatch = useDispatch();
  // Selectors
  const accessToken = useSelector(getAccessToken);
  const trialEligible = useSelector(getTrialEligibility);
  const selectedPlanId = useSelector(getSelectedPlanId);
  const cards = useSelector(getCreditCards, shallowEqual);
  const plans = useSelector(getPremiumPlans, shallowEqual);
  const defaultCardId = useSelector(getDefaultSource);
  const selectedCard = useSelector(getSelectedCardId, shallowEqual);
  const submitting = useSelector(isFormSubmitting);
  const showCards = useSelector(getShowCards);
  const showDuplicateCard = useSelector(getShowDuplicateCard);
  const coupon = useSelector(getCoupon, shallowEqual);
  const subscriptionError = useSelector(getUserError);
  const cardError = useSelector(getCardError);

  // Dispatch actions
  const doToggleSelectedPlanId = (planId) => dispatch(toggleSelectedPlanId(planId));
  const doStartSubmitSpinner = () => dispatch(startSubmitSpinner());
  const doSubmitSubscription = (values) => dispatch(submitSubscription(values));
  const doSetSelectedCardSource = (cardId) => dispatch(setSelectedCardSource(cardId));
  const doToggleShowCards = (shouldShowCards) => dispatch(toggleShowCards(shouldShowCards));

  // State
  const [stripeSource, setStripeSource] = useState(null);

  // ComponentDidMount
  useEffect(() => {
    viewedCheckout();
  }, [viewedCheckout]);

  const stripe = useStripe();
  const elements = useElements();

  const submitPaymentForm = async (values) => {
    doStartSubmitSpinner();

    let token;
    if (!stripe || !elements) {
      // Stripe.js has not loaded yet
      return;
    }
    /**
     * Need to access CardElement in order to generate a token
     * Set token in state so when we get a DuplicateCard popup,
     * we still have access to the card info
     */
    const cardElement = elements.getElement(CardElement);
    if (cardElement) {
      const tokenObj = (!showCards && (await stripe.createToken(cardElement))) || null;
      token = tokenObj.token;
      setStripeSource(tokenObj.token);
    }

    const subscriptionValues = {
      plan: selectedPlanId,
      source: token || stripeSource,
      selectedCard: showCards ? selectedCard : null,
      trialEligible: coupon ? false : trialEligible,
      location: formLocation,
      defaultCardId,
      accessToken,
      coupon,
      shouldRedirectToOnboarding,
      ...values,
    };
    await doSubmitSubscription(subscriptionValues);
  };

  const error = subscriptionError || cardError;
  const { interval } = plans.find(({ id }) => id === selectedPlanId);
  const disableTrialText = coupon || !trialEligible || interval !== 'year';

  if (showDuplicateCard && !coupon) {
    return <DuplicateCard submitting={submitting} submitPayment={submitPaymentForm} />;
  }

  const [{ iso: currency }] = plans;

  return (
    <div>
      {children}
      <div className="checkout-form__container">
        {error ? (
          <Alert style={{ marginBottom: '0px', textAlign: 'center' }} type="error">
            {error}
          </Alert>
        ) : null}
        <div className="section-label">Choose your plan</div>
        <PremiumPlans
          plans={plans}
          selectedPlanId={selectedPlanId}
          toggleSelectedPlanId={doToggleSelectedPlanId}
          trialEligible={trialEligible}
        />
        <PlanBillingText plans={plans} coupon={coupon} currency={currency} />
        <div className="credit-card-container">
          {!cards || !showCards ? (
            <div>
              <div className="section-label">Enter your card</div>
              <div className="stripe-card-element__wrapper">
                <CardElement
                  options={{
                    style: {
                      base: {
                        fontSize: '18px',
                        color: '#424770',
                        letterSpacing: '0.025em',
                        fontFamily: 'Source Sans Pro, Helvetica, sans-serif',
                        '::placeholder': {
                          color: '#aab7c4',
                        },
                        padding: '16px 14px',
                      },
                      invalid: {
                        color: '#9e2146',
                      },
                    },
                    disabled: submitting,
                  }}
                />
                {APP_ENV === 'sandbox' || APP_ENV === 'staging' || APP_ENV === 'development' ? (
                  <a
                    target="_blank"
                    rel="noreferrer"
                    href="https://stripe.com/docs/testing#cards"
                    className="stripe-card-element__test-text"
                  >
                    Need a test credit card?
                  </a>
                ) : null}
              </div>
            </div>
          ) : (
            <div className="credit-card-list">
              <div className="section-label">Select a card</div>
              {cards.map((card, index) => (
                <CreditCard
                  card={card}
                  selectedCard={selectedCard}
                  doSetSelectedCardSource={doSetSelectedCardSource}
                  tabIndex={index}
                  key={card.id}
                />
              ))}
            </div>
          )}
          {cards ? (
            <div>
              <button
                type="button"
                onClick={() => doToggleShowCards(showCards)}
                className="credit-card__form__toggle"
              >
                {showCards ? '+ Add New Credit Card' : 'Use An Existing Credit card'}
              </button>
            </div>
          ) : null}
        </div>
        <div className="checkout-form__container__payment-button">
          <Button loading={submitting} onClick={submitPaymentForm}>
            {disableTrialText ? 'Go Premium' : 'Start Free Trial'}
          </Button>
        </div>
        <PaymentRequestSubscription paymentHandler={submitPaymentForm} />
        <Disclaimer trialEligible={!disableTrialText} />
      </div>
      <CouponForm />
    </div>
  );
};

PaymentForm.propTypes = {
  viewedCheckout: PropTypes.func.isRequired,
  formLocation: PropTypes.string.isRequired,
  shouldRedirectToOnboarding: PropTypes.bool,
  children: PropTypes.node.isRequired,
};

PaymentForm.defaultProps = {
  shouldRedirectToOnboarding: true,
};

export default PaymentForm;
