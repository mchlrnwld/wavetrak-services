import PropTypes from 'prop-types';
import getConfig from '../../config';
import './Disclaimer.scss';

const { privacyPolicy, termsConditions } = getConfig();

const RegistrationDisclaimer = () => (
  <p className="disclaimer">
    {`By clicking Create Account you agree to our `}
    <DisclaimerLink url={privacyPolicy} text="Privacy policy" />
    {' and '}
    <DisclaimerLink url={termsConditions} text="Terms of Use" />.
  </p>
);

const FunnelDisclaimer = ({ trialEligible }) => {
  const trialText = trialEligible
    ? 'We will send an email reminder 7 days before your trial ends.  Cancellation is easy, just go to Account > Subscription.  By starting a free trial or subscription, '
    : 'By starting a subscription';
  return (
    <p className="disclaimer">
      {`${trialText} you agree to our `}
      <DisclaimerLink url={privacyPolicy} text="Privacy policy" />
      {' and '}
      <DisclaimerLink url={termsConditions} text="Terms of Use" />.
    </p>
  );
};

const Disclaimer = ({ location, trialEligible }) =>
  location === 'registration' ? (
    <RegistrationDisclaimer />
  ) : (
    <FunnelDisclaimer trialEligible={trialEligible} />
  );

const DisclaimerLink = ({ text, url }) => (
  <a className="disclaimer-link" href={url} target="_blank" rel="noopener noreferrer">
    {text}
  </a>
);

FunnelDisclaimer.propTypes = {
  trialEligible: PropTypes.bool.isRequired,
};

Disclaimer.propTypes = {
  location: PropTypes.string,
  trialEligible: PropTypes.bool.isRequired,
};

Disclaimer.defaultProps = {
  location: null,
};

DisclaimerLink.propTypes = {
  text: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default Disclaimer;
