/* eslint-disable react/no-danger */
import PropTypes from 'prop-types';
import Head from 'next/head';
import { Cookies } from 'react-cookie';
import { FacebookBrowser } from '@surfline/quiver-react';
import {
  nrBrowserSnippet,
  snippet as segment,
  resetAnonymousIdCookieSnippet,
} from '@surfline/web-common';
import StripeJS from '../StripeJS/StripeJS';

const cookie = new Cookies();

const AppHead = ({ config }) => {
  const { newRelic, facebook } = config;
  const shouldResetAnonymousIdCookie = !cookie.get('sl_reset_anonymous_id_v3');
  return (
    <div>
      <Head>
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Premium Upgrade Funnel" />
        {shouldResetAnonymousIdCookie ? (
          <script
            dangerouslySetInnerHTML={{
              __html: resetAnonymousIdCookieSnippet(false),
            }}
          />
        ) : null}
        <script
          type="text/javascript"
          dangerouslySetInnerHTML={{
            __html: nrBrowserSnippet(newRelic),
          }}
        />
        <script
          type="text/javascript"
          dangerouslySetInnerHTML={{
            __html: segment(config.segment, false),
          }}
        />
        <StripeJS />
        <FacebookBrowser appId={facebook.appId} />
      </Head>
    </div>
  );
};

AppHead.propTypes = {
  config: PropTypes.shape({
    segment: PropTypes.string.isRequired,
    newRelic: PropTypes.shape({
      licenseKey: PropTypes.string.isRequired,
      applicationID: PropTypes.string.isRequired,
    }),
    splitio: PropTypes.string.isRequired,
    facebook: PropTypes.shape({
      appId: PropTypes.string.isRequired,
    }),
  }).isRequired,
};
export default AppHead;
