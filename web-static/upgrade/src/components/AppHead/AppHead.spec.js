import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import Head from 'next/head';

import AppHead from './AppHead';
import getConfig from '../../config';

const config = getConfig();

describe('AppHead', () => {
  it('renders AppHead', () => {
    const description = 'Premium Upgrade Funnel';
    const wrapper = shallow(<AppHead config={config} />);
    expect(wrapper.find(Head)).to.have.length(1);
    expect(wrapper.find("meta[name='description']").props().content).to.equal(description);
  });
});
