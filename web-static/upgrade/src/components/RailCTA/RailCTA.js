import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { useDeviceSize } from '@surfline/quiver-react';
import { isUserMetered as computeTreatmentState } from '../../store/selectors/meter';
import './RailCTA.scss';

/**
 * @description The `RailCTA` component is used on the right rail. It renders a
 * background image which covers the block and on top of it renders a heading with sub-text and
 * icons. The subtext and icons are passed in from the translations (`copy` prop) under the `features`
 * array.
 *
 * @param {object} props - The props for the component
 * @param {object} props.copy - The brand and feature flag specific copy/icons from the translation file
 * @param {string} props.backgroundImage - (optional) background image url
 *
 * @returns {JSX.Element} FunnelCTA JSX
 */

const RailCTA = ({ copy, backgroundImage }) => {
  const { isDesktop } = useDeviceSize();
  const isUserMetered = useSelector(computeTreatmentState);
  const headerClass = isUserMetered && isDesktop ? 'rail-cta__header--metered' : 'rail-cta__header';
  return (
    <div className="rail-cta" style={{ backgroundImage }}>
      <div className="rail-cta__content">
        <h4 className={headerClass}>
          {isUserMetered ? 'Get Unlimited Access with Surfline Premium' : copy.header}
        </h4>
        <div className="rail-cta__features">
          {copy.features.map(({ Icon, header, description }) => (
            <div key={header} className="feature">
              <Icon />
              <div className="feature__content">
                <h6 className="feature__content_header">{header}</h6>
                <p className="feature__content_desc">{description}</p>
              </div>
            </div>
          ))}
        </div>
        {copy.description && <p className="rail-cta__description">{copy.description}</p>}
      </div>
    </div>
  );
};
RailCTA.propTypes = {
  copy: PropTypes.instanceOf(Object).isRequired,
  backgroundImage: PropTypes.string,
};

RailCTA.defaultProps = {
  backgroundImage: null,
};

export default RailCTA;
