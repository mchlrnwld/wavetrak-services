import PropTypes from 'prop-types';
import './RailCTA.scss';

/**
 * @description The MobileRailCTA only display on mobile view. This component renders a heading
 * for the mobile page that is placed above the form.
 *
 * @param {object} copy Feature flag specific copy and icons from translations
 */
const MobileRailCTA = ({ copy: { mobileHeader, mobileSubtitle }, backgroundImage }) => (
  <div className="mobile-rail-cta" style={{ backgroundImage }}>
    <div className="mobile-rail-cta__content">
      <h4 className="mobile-rail-cta__header">{mobileHeader}</h4>
      {mobileSubtitle && (
        <a href="/premium-benefits" className="mobile-rail-cta__subtitle">
          {mobileSubtitle}
        </a>
      )}
    </div>
  </div>
);

MobileRailCTA.propTypes = {
  copy: PropTypes.shape({
    mobileHeader: PropTypes.string,
    mobileSubtitle: PropTypes.string,
    MobileIcon: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
  }),
  backgroundImage: PropTypes.string,
};

MobileRailCTA.defaultProps = {
  copy: {},
  backgroundImage: null,
};

export default MobileRailCTA;
