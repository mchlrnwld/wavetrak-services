import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { Alert, Button, Input } from '@surfline/quiver-react';
import { fetchCoupon, clearCouponError } from '../../store/actions/coupon';
import './CouponForm.scss';
import {
  getCouponSubmitting,
  getCouponError,
  getCouponSuccess,
  getCoupon,
} from '../../store/selectors/coupon';

const CouponForm = () => {
  const dispatch = useDispatch();

  const couponSubmitting = useSelector(getCouponSubmitting);
  const couponError = useSelector(getCouponError);
  const couponSuccess = useSelector(getCouponSuccess);
  const couponCode = useSelector(getCoupon);

  const [coupon, setCoupon] = useState(undefined);
  const [isFormOpen, setIsFormOpen] = useState(!!couponCode);
  const { register, handleSubmit, errors } = useForm();

  useEffect(() => {
    if (couponCode) {
      setIsFormOpen(true);
    }
  }, [couponCode]);

  useEffect(() => {
    if (couponCode) {
      setCoupon(couponCode);
      setIsFormOpen(true);
    }
  }, [couponCode, isFormOpen]);

  const onSubmit = ({ coupon: submittedCode }) => {
    if (submittedCode) {
      dispatch(fetchCoupon(submittedCode));
    }
  };

  const toggleCouponForm = () => {
    if (isFormOpen) {
      dispatch(clearCouponError());
    }
    setIsFormOpen(!isFormOpen);

    if (couponCode) {
      setCoupon(null);
      window.location.assign('/upgrade');
    }
  };

  return (
    <div className="coupon-form">
      {couponSuccess && !couponError ? (
        <div>
          <Alert style={{ margin: '16px 0 0 0', textAlign: 'center' }} type="success">
            {couponSuccess}
          </Alert>
        </div>
      ) : null}
      {couponError && !couponSuccess ? (
        <Alert style={{ marginBottom: '8px', textAlign: 'center' }} type="error">
          {couponError}
        </Alert>
      ) : null}
      {isFormOpen ? (
        <div>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Input
              style={{
                flex: coupon ? 0.7 : 1.0,
              }}
              value={coupon}
              label="Discount Code"
              id="coupon"
              error={!!errors.coupon}
              input={{
                name: 'coupon',
                onChange: (event) => setCoupon(event.target.value),
                ref: register(),
              }}
            />
            <div className="coupon-form__button-container">
              <Button style={{ width: '100%' }} loading={couponSubmitting}>
                Apply
              </Button>
              <Button style={{ width: '100%' }} onClick={() => toggleCouponForm()}>
                {couponCode ? 'Remove' : 'Cancel'}
              </Button>
            </div>
          </form>
        </div>
      ) : (
        <button
          type="button"
          className="coupon-form__toggle-button"
          onKeyPress={() => toggleCouponForm()}
          onClick={() => toggleCouponForm()}
        >
          Have a discount code?
        </button>
      )}
    </div>
  );
};

export default CouponForm;
