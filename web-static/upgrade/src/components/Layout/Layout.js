import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';
import { SL } from '@surfline/web-common';
import { Spinner } from '@surfline/quiver-react';
import { RailLogo, RailCTA, MobileRailCTA, PromotionRailCTA, PromotionMobileRailCTA } from '..';
import en from '../../international/translations/en';
import getConfig from '../../config';
import {
  isUserMetered as computeTreatmentState,
  getMeterLoaded,
} from '../../store/selectors/meter';
import { getFunnelConfig } from '../../store/selectors/promotion';
import './Layout.scss';

const { brand } = getConfig();
export const LeftRail = ({ children }) => (
  <div className="left-rail">
    <RailLogo />
    {children}
  </div>
);

export const RightRail = ({ children }) => {
  const meterLoaded = useSelector(getMeterLoaded);
  if (brand !== SL) return <div className="right-rail">{children}</div>;
  if (!meterLoaded)
    return (
      <div className="loading">
        <Spinner />
      </div>
    );
  return <div className="right-rail">{children}</div>;
};

const Layout = ({ children }) => {
  const isUserMetered = useSelector(computeTreatmentState);
  const { premiumFeatures } = en;
  const features = brand === SL ? premiumFeatures(isUserMetered) : premiumFeatures;
  const ctaCopy = {
    ...en.rail_cta,
    ...features,
  };
  const funnelConfig = useSelector(getFunnelConfig, shallowEqual);
  const promotionFeatures = { ...en.promotionFeatures };
  if (funnelConfig) {
    funnelConfig.promotionFeatures = promotionFeatures;
    return (
      <div>
        <div className="promotion-wrapper">
          <PromotionMobileRailCTA funnelConfig={funnelConfig} />
          <LeftRail>
            <div className="promotion-wrapper__content">{children}</div>
          </LeftRail>
          <RightRail>
            <PromotionRailCTA funnelConfig={funnelConfig} />
          </RightRail>
        </div>
      </div>
    );
  }

  return ctaCopy ? (
    <div className="upgrade-wrapper">
      <MobileRailCTA copy={ctaCopy} />
      <LeftRail>
        <div className="upgrade-wrapper__content">{children}</div>
      </LeftRail>
      <RightRail>
        <RailCTA copy={ctaCopy} />
      </RightRail>
    </div>
  ) : null;
};

LeftRail.propTypes = {
  children: PropTypes.node.isRequired,
};
RightRail.propTypes = {
  children: PropTypes.node.isRequired,
};
Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
