import React from 'react';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import * as reactRedux from 'react-redux';
import * as meter from '../../store/selectors/meter';
import * as promotion from '../../store/selectors/promotion';

import Layout, { LeftRail, RightRail } from './Layout';
import MobileRailCTA from '../RailCTA/MobileRailCTA';
import PromotionMobileRailCTA from '../PromotionRailCTA/PromotionMobileRailCTA';

describe('Layout', () => {
  const dispatchStub = sinon.stub();
  let useSelectorStub;
  let useDispatchStub;
  let getFunnelConfigStub;
  let getMeterLoadedStub;
  let computeTreatmentStateStub;

  before(() => {
    useSelectorStub = sinon.stub(reactRedux, 'useSelector');
    useDispatchStub = sinon.stub(reactRedux, 'useDispatch');
    getMeterLoadedStub = sinon.stub(meter, 'getMeterLoaded');
    computeTreatmentStateStub = sinon.stub(meter, 'isUserMetered');
    getFunnelConfigStub = sinon.stub(promotion, 'getFunnelConfig');

    useSelectorStub.callsArg(0);
    useDispatchStub.returns(dispatchStub);
  });

  after(() => {
    getMeterLoadedStub.restore();
    computeTreatmentStateStub.restore();
    useSelectorStub.restore();
    useDispatchStub.restore();
    getFunnelConfigStub.restore();
  });

  it('renders upgrade wrapper', () => {
    const wrapper = shallow(
      <Layout>
        <div />
      </Layout>,
    );
    expect(wrapper.find('.upgrade-wrapper')).to.have.length(1);
    expect(wrapper.find(LeftRail)).to.have.length(1);
    expect(wrapper.find(RightRail)).to.have.length(1);
    expect(wrapper.find(MobileRailCTA)).to.have.length(1);
  });

  it('renders promotion wrapper', () => {
    const funnelConfig = { name: 'Test promo' };
    getFunnelConfigStub.returns(funnelConfig);
    const wrapper = mount(
      <Layout>
        <div />
      </Layout>,
    );
    expect(wrapper.find('.promotion-wrapper')).to.have.length(1);
    expect(wrapper.find(LeftRail)).to.have.length(1);
    expect(wrapper.find(RightRail)).to.have.length(1);
    expect(wrapper.find(PromotionMobileRailCTA).props().funnelConfig.name).to.equal('Test promo');
  });
});
