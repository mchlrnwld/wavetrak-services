import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import OverUnder from './OverUnder';
import pricingPlansFixture from '../../fixtures/pricingPlans';

describe('OverUnder', () => {
  const selectPlanHandler = () => {};
  const submitButtonHandler = () => {};

  it('renders "OverUnder" component', () => {
    const wrapper = shallow(
      <OverUnder
        plans={pricingPlansFixture.plans}
        selectedPlan="year"
        trialEligible
        selectPlanHandler={selectPlanHandler}
        submitButtonHandler={submitButtonHandler}
        submitting={false}
        error={null}
      />,
    );
    expect(wrapper.find('.over-under')).to.have.lengthOf(1);
    expect(wrapper.find('.pricing-plans__title').text().trim()).to.equal(
      'Choose your subscription',
    );
    expect(
      wrapper.find('.pricing-plan--selected').contains([<span>Yearly Subscription</span>]),
    ).to.be.equal(true);
  });
});
