import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';

import PromotionIneligible from './PromotionIneligible';

describe('PromotionIneligible', () => {
  it('renders promotion ineligible component', () => {
    const ineligibleText = 'Premium subscribers are not eligible for this promotion.';
    const buttonText = 'RETURN TO SURFLINE';
    const wrapper = shallow(<PromotionIneligible />);
    expect(wrapper.find('.promotion-ineligible__title')).to.have.lengthOf(1);
    expect(wrapper.find('p').text()).to.equal(ineligibleText);
    expect(wrapper.find('.quiver-button').text().toUpperCase()).to.equal(buttonText);
  });
});
