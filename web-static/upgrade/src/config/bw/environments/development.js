module.exports = {
  cdnHost: '/',
  staticHost: '/static/',
  redirectUrl: 'http://localhost:3000',
  benefitsRedirectUrl: 'http://benefits.buoyweather.com/7-day-forecast/',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: '/upgrade',
  accountLinkProtocol: 'https://sandbox.',
  segment: 'wGHvUUloHAZXW8STk3qUxvKD0NswRkHu',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '17227951',
    accountID: '356245',
    trustKey: '356245',
    agentID: '17227951',
  },
  servicesEndpoint: 'https://services.sandbox.surfline.com',
  stripe: {
    publishableKey: 'pk_test_Meb7MPzMWgbBYwU3t6gmdHGt',
  },
  termsConditions: 'http://www.buoyweather.com/info.jsp?id=21738',
  privacyPolicy: 'http://www.buoyweather.com/info.jsp?id=21737',
  needHelpUrl: 'http://www.buoyweather.com/info.jsp?id=21736',
  clientId: '5c59e53d2d11e750809d572f',
  clientSecret: 'sk_dKW7DWJ4XMxxfP5BWDx8',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
  facebook: {
    appId: '564041023804928',
  },
};
