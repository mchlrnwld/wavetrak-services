/* eslint-disable import/no-extraneous-dependencies */
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import LogRocket from 'logrocket';
import reducers from './reducers';

const initialState = {};
const enhancers = composeWithDevTools(applyMiddleware(thunk, LogRocket.reduxMiddleware()));
const store = createStore(reducers, initialState, enhancers);

export default store;
