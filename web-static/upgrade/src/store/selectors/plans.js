export const getPremiumPlans = (state) => {
  const { plans } = state.plans;
  if (plans) return plans;
  return null;
};

export const getSelectedPlanId = (state) => {
  const { selectedPlanId } = state.plans;
  if (selectedPlanId) return selectedPlanId;
  return null;
};

export const getSelectedPlan = (state) => {
  const { selectedPlanId, plans } = state.plans;
  const selectedPlan = plans.find(({ id }) => id === selectedPlanId);
  return selectedPlan;
};

export const getTrialPeriodDays = ({ plans }) => {
  const { selectedPlanId, plans: premiumPlans } = plans;
  if (!premiumPlans) return null;
  const { trialPeriodDays } = premiumPlans.find(({ id }) => id === selectedPlanId);
  return trialPeriodDays;
};

export const getCurrencySymbol = (state) => {
  const { symbol } = state.plans;
  if (symbol) return symbol;
  return null;
};

export const getCurrencyISO = (state) => {
  const { iso } = state.plans;
  if (iso) return iso;
  return null;
};

export const getCurrencyMonthlyCost = (state) => {
  const { monthly } = state.plans;
  if (monthly) return monthly;
  return null;
};

export const getCurrencyAnnualCost = (state) => {
  const { annual } = state.plans;
  if (annual) return annual;
  return null;
};

export const getComputedMonthlyCost = (state) => {
  const { annual } = state.plans;
  if (!annual) return null;
  return (Math.round((100 * annual) / 12) / 100).toFixed(2);
};
