export const getCoupon = (state) => state?.coupon?.coupon || null;

export const getCouponFormOpen = (state) => state?.coupon?.isFormOpen || null;

export const getCouponSubmitting = (state) => state?.coupon?.couponSubmitting || null;

export const getCouponError = (state) => state?.coupon?.couponError || null;
export const getCouponSuccess = (state) => state?.coupon?.couponSuccess || null;
