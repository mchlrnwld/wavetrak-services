export const getCreditCards = (state) => {
  const { cards } = state.cards;
  if (cards) return cards;
  return null;
};

export const getDefaultSource = (state) => {
  const { defaultSource } = state.cards;
  if (defaultSource) return defaultSource;
  return null;
};

export const getSelectedCardId = (state) => {
  const { selectedCard } = state.cards;
  if (selectedCard) return selectedCard;
  return null;
};

export const getSelectedCard = (state) => {
  const { selectedCard, cards } = state.cards;
  return cards?.find(({ id }) => id === selectedCard);
};

export const getShowCards = (state) => {
  const { showCards } = state.cards;
  if (showCards || showCards === false) return showCards;
  return null;
};

export const getShowDuplicateCard = (state) => {
  const { showDuplicateCard } = state.duplicateCard;
  if (showDuplicateCard || showDuplicateCard === false) return showDuplicateCard;
  return null;
};

export const getCardError = (state) => {
  const { error } = state.cards;
  if (error) return error;
  return null;
};

export const getDuplicateCardPlan = (state) => {
  const { plan } = state.duplicateCard;
  if (plan) return plan;
  return null;
};

export const getDuplicateCardSource = (state) => {
  const { source } = state.duplicateCard;
  if (source) return source;
  return null;
};
