export const getSubcriptionVerificationStatus = (state) =>
  state?.subscription?.verificationStatus || null;
