export const getAccessToken = (state) => {
  const { accessToken } = state.auth;
  if (accessToken) return accessToken;
  return null;
};

export const getRefreshToken = (state) => {
  const { refreshToken } = state.auth;
  if (refreshToken) return refreshToken;
  return null;
};
