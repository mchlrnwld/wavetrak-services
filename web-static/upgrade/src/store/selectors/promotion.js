export const getFunnelConfig = (state) => state?.promotion?.funnelConfig || null;

export const getPromotionId = (state) => state?.promotion?.funnelConfig?._id || null;

export const getTrialPeriodDays = (state) => state?.promotion?.funnelConfig?.trialLength || null;

export const getPromoCode = (state) => state?.promotion?.promoCode || null;

export const getCurrencyEligibility = (state) => {
  if (state) {
    const {
      promotion: { currency } = {},
      plans: { iso },
    } = state;
    const promoCurrency = currency?.toLowerCase();
    const currencyOfRegion = iso?.toLowerCase();
    if (promoCurrency !== 'any' && promoCurrency !== currencyOfRegion) {
      return false;
    }
    return true;
  }
  return null;
};
