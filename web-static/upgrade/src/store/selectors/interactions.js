import { OFF } from '../../common/constants';

export const getShowLogin = (state) => {
  if (state) {
    const {
      interactions: { showLogin },
    } = state;
    return showLogin;
  }
  return null;
};

export const isFormSubmitting = (state) => {
  if (state) {
    const {
      interactions: { submitting },
    } = state;
    return submitting;
  }
  return null;
};

export const getStripeCheckoutTreatment = (state) => {
  if (state) {
    const {
      interactions: { stripeCheckoutTreatment },
    } = state;
    return stripeCheckoutTreatment;
  }
  return OFF;
};

export const getIsStripeCheckoutEnabled = (state) => {
  if (state) {
    const {
      interactions: { isStripeCheckoutEnabled },
    } = state;
    return isStripeCheckoutEnabled;
  }
  return false;
};
