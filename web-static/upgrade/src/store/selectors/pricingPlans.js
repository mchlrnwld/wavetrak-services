export const getPricingPlans = (state) => {
  const { pricingPlans } = state.pricingPlans;
  if (pricingPlans && pricingPlans.length > 0) return pricingPlans;
  return null;
};

export const getPricingPlanCurrencyISO = (state) => {
  const { iso } = state.pricingPlans;
  if (iso) return iso;
  return null;
};
