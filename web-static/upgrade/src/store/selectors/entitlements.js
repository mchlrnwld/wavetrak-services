import getConfig from '../../config';

const config = getConfig();

export const getPremiumStatus = (state) => {
  const { company } = config;
  const data = state.entitlements.entitlements;
  if (data && data.includes(`${company}_premium`)) return true;
  return false;
};

export const getTrialEligibility = (state) => {
  const { company } = config;
  const data = state.entitlements.promotions;
  if (data && data.includes(`${company}_free_trial`)) return true;
  return false;
};

export const getEntitlements = (state) => {
  const { entitlements } = state.entitlements;
  if (entitlements) return entitlements;
  return null;
};
