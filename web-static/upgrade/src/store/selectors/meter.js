import { SL } from '@surfline/web-common';
import { ON } from '../../common/constants';

import getConfig from '../../config';

const { brand } = getConfig();

export const isUserMetered = (state) => {
  const treatmentState = state.meter?.treatmentState;
  return brand === SL && treatmentState === ON;
};

export const getMeter = (state) => {
  if (state) {
    const { meter } = state;
    return meter;
  }
  return null;
};

export const getMeterLoaded = (state) => state.meter?.meterLoaded;
