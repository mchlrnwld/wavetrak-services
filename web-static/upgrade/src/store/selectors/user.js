export const getUser = (state) => {
  if (state) {
    const { user } = state;
    return user;
  }
  return null;
};

/**
 *
 * @param {{ user: { details: import('@surfline/web-common').UserState["details"] }} state
 */
export const getUserDetails = (state) => state.user?.details;

export const getUserError = (state) => {
  if (state) {
    const {
      user: { error },
    } = state;
    return error;
  }
  return null;
};

export const getSubmitSuccess = (state) => {
  if (state) {
    const {
      user: { submitSuccess },
    } = state;
    return submitSuccess;
  }
  return null;
};

export const getUserSuccess = (state) => {
  if (state) {
    const {
      user: { success },
    } = state;
    return success;
  }
  return null;
};

export const getFacebookLoading = (state) => {
  if (state) {
    const {
      user: { facebookLoading },
    } = state;
    return facebookLoading;
  }
  return null;
};

/**
 * @param {{ user: Pick<import('@surfline/web-common').User, 'countryCode'> }} state
 */
export const getCountryCode = (state) => state.user?.countryCode || 'US';
