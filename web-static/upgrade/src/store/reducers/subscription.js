import createReducer from './createReducer';
import {
  SUBMIT_SUBSCRIPTION,
  SUBMIT_SUBSCRIPTION_SUCCESS,
  SUBMIT_SUBSCRIPTION_FAIL,
  FETCH_SUBSCRIPTION_VERIFICATION,
  FETCH_SUBSCRIPTION_VERIFICATION_SUCCESS,
  FETCH_SUBSCRIPTION_VERIFICATION_FAIL,
} from '../actions/subscription';

const initialState = {
  error: null,
  success: null,
};
const handlers = {};

handlers[SUBMIT_SUBSCRIPTION] = (state) => ({
  ...state,
  error: null,
});

handlers[SUBMIT_SUBSCRIPTION_SUCCESS] = (state) => ({
  ...state,
  error: null,
  success: true,
});

handlers[SUBMIT_SUBSCRIPTION_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
});

handlers[FETCH_SUBSCRIPTION_VERIFICATION] = (state) => ({
  ...state,
  error: null,
});

handlers[FETCH_SUBSCRIPTION_VERIFICATION_SUCCESS] = (
  state,
  { verificationStatus, verificationMessage },
) => ({
  ...state,
  error: null,
  verificationMessage,
  verificationStatus,
});

handlers[FETCH_SUBSCRIPTION_VERIFICATION_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
});

export default createReducer(handlers, initialState);
