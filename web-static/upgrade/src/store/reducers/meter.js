import createReducer from './createReducer';
import { SETUP_METERING, SETUP_METERING_FAIL, SET_METER } from '../actions/meter';

const initialState = { meterLoaded: false };
const handlers = {};

handlers[SET_METER] = (state, { meter }) => ({
  ...state,
  meterLoaded: true,
  loading: false,
  ...meter,
});

handlers[SETUP_METERING] = (state) => ({
  ...state,
  loading: true,
});

handlers[SETUP_METERING_FAIL] = (state, { error }) => ({
  ...state,
  loading: false,
  meterLoaded: true,
  error,
});

export default createReducer(handlers, initialState);
