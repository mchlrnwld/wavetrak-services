import createReducer from './createReducer';

import {
  VALIDATE_COUPON,
  VALIDATE_COUPON_SUCCESS,
  VALIDATE_COUPON_FAIL,
  OPEN_COUPON_FORM,
  CLOSE_COUPON_FORM,
  TOGGLE_COUPON_FORM,
  SET_COUPON,
  CLEAR_COUPON_ERROR,
} from '../actions/coupon';

const initialState = {
  isFormOpen: false,
  couponSubmitting: false,
  couponError: null,
  couponSuccess: null,
  coupon: null,
};
const handlers = {};

handlers[SET_COUPON] = (state, { coupon }) => ({
  ...state,
  coupon,
});

handlers[VALIDATE_COUPON] = (state) => ({
  ...state,
  couponSubmitting: true,
});

handlers[VALIDATE_COUPON_SUCCESS] = (state, { coupon }) => ({
  ...state,
  couponSubmitting: false,
  isFormOpen: false,
  couponSuccess: 'Coupon Applied!',
  couponError: null,
  coupon,
});

handlers[VALIDATE_COUPON_FAIL] = (state, { error }) => ({
  ...state,
  couponSubmitting: false,
  couponError: error.message,
  couponSuccess: null,
  coupon: null,
});

handlers[OPEN_COUPON_FORM] = (state) => ({
  ...state,
  couponSubmitting: false,
  isFormOpen: true,
});

handlers[CLOSE_COUPON_FORM] = (state) => ({
  ...state,
  couponSubmitting: false,
  isFormOpen: false,
});

handlers[TOGGLE_COUPON_FORM] = (state, { isFormOpen }) => ({
  ...state,
  couponSubmitting: false,
  couponError: null,
  isFormOpen: !isFormOpen,
});

handlers[CLEAR_COUPON_ERROR] = (state) => ({
  ...state,
  couponError: null,
});

export default createReducer(handlers, initialState);
