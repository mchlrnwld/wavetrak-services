import createReducer from './createReducer';
import {
  TOGGLE_REGISTRATION_FORM,
  START_SUBMIT_SPINNER,
  STOP_SUBMIT_SPINNER,
  FETCH_STRIPE_CHECKOUT_TREATMENT,
  FETCH_STRIPE_CHECKOUT_TREATMENT_SUCCESS,
  FETCH_STRIPE_CHECKOUT_TREATMENT_FAIL,
} from '../actions/interactions';
import { OFF } from '../../common/constants';

const initialState = {
  showLogin: false,
  showOfferLogin: true,
  submitting: false,
  fetchingTreatment: false,
  stripeCheckoutTreatment: OFF,
  isStripeCheckoutEnabled: false,
};
const handlers = {};

handlers[TOGGLE_REGISTRATION_FORM] = (state, { showLogin }) => ({
  ...state,
  showLogin,
});

handlers[START_SUBMIT_SPINNER] = (state) => ({
  ...state,
  submitting: true,
});

handlers[STOP_SUBMIT_SPINNER] = (state) => ({
  ...state,
  submitting: false,
});

handlers[FETCH_STRIPE_CHECKOUT_TREATMENT] = (state) => ({
  ...state,
  fetchingTreatment: true,
});

handlers[FETCH_STRIPE_CHECKOUT_TREATMENT_SUCCESS] = (
  state,
  { stripeCheckoutTreatment, isStripeCheckoutEnabled },
) => ({
  ...state,
  submitting: false,
  stripeCheckoutTreatment,
  isStripeCheckoutEnabled,
  fetchingTreatment: false,
});

handlers[FETCH_STRIPE_CHECKOUT_TREATMENT_FAIL] = (state, { stripeCheckoutTreatment }) => ({
  ...state,
  submitting: false,
  stripeCheckoutTreatment,
  isStripeCheckoutEnabled: false,
  fetchingTreatment: false,
});

export default createReducer(handlers, initialState);
