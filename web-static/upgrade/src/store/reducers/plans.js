import createReducer from './createReducer';
import {
  FETCH_PLANS,
  FETCH_PLANS_SUCCESS,
  FETCH_PLANS_FAIL,
  TOGGLE_SELECTED_PLANID,
  SET_DISCOUNTED_PLANS,
  SET_PLANS_CURRENCY,
} from '../actions/plans';

const initialState = {
  iso: 'USD',
  symbol: '$',
};
const handlers = {};

handlers[FETCH_PLANS] = (state) => ({
  ...state,
  fetchingPlans: true,
  plans: null,
  selectedPlanId: null,
});

handlers[FETCH_PLANS_SUCCESS] = (state, { plans, selectedPlanId }) => ({
  ...state,
  fetchingPlans: false,
  plans,
  selectedPlanId,
});

handlers[FETCH_PLANS_FAIL] = (state, { error }) => ({
  ...state,
  fetchingPlans: false,
  plans: null,
  error,
});

handlers[TOGGLE_SELECTED_PLANID] = (state, { selectedPlanId }) => ({
  ...state,
  selectedPlanId,
});

handlers[SET_DISCOUNTED_PLANS] = (state, { plans, selectedPlanId }) => ({
  ...state,
  plans,
  selectedPlanId,
});

handlers[SET_PLANS_CURRENCY] = (state, { iso, symbol, monthly, annual }) => ({
  ...state,
  iso,
  symbol,
  monthly,
  annual,
});

export default createReducer(handlers, initialState);
