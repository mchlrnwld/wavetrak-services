import { combineReducers } from 'redux';

import user from './user';
import entitlements from './entitlements';
import plans from './plans';
import interactions from './interactions';
import auth from './auth';
import coupon from './coupon';
import duplicateCard from './duplicateCard';
import cards from './cards';
import warnings from './warnings';
import meter from './meter';
import pricingPlans from './pricingPlans';
import promotion from './promotion';
import subscription from './subscription';

export default combineReducers({
  auth,
  entitlements,
  plans,
  user,
  interactions,
  coupon,
  duplicateCard,
  cards,
  warnings,
  meter,
  pricingPlans,
  promotion,
  subscription,
});
