import createReducer from './createReducer';
import { SET_ACCESS_TOKEN, REMOVE_ACCESS_TOKEN } from '../actions/auth';

const initialState = {};
const handlers = {};

handlers[SET_ACCESS_TOKEN] = (state, { accessToken }) => ({
  ...state,
  accessToken,
});

handlers[REMOVE_ACCESS_TOKEN] = (state) => ({
  ...state,
  accessToken: null,
});

export default createReducer(handlers, initialState);
