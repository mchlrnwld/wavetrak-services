import createReducer from './createReducer';
import {
  FETCH_PRICING_PLANS,
  FETCH_PRICING_PLANS_SUCCESS,
  FETCH_PRICING_PLANS_FAIL,
  SET_PRICING_PLANS_CURRENCY,
  SET_DISCOUNTED_PRICING_PLANS,
} from '../actions/pricingPlans';

const initialState = {
  iso: 'USD',
  symbol: '$',
};
const handlers = {};

handlers[FETCH_PRICING_PLANS] = (state) => ({
  ...state,
  fetchingPricingPlans: true,
  pricingPlans: null,
});

handlers[FETCH_PRICING_PLANS_SUCCESS] = (state, { pricingPlans }) => ({
  ...state,
  fetchingPricingPlans: false,
  pricingPlans,
});

handlers[FETCH_PRICING_PLANS_FAIL] = (state, { error }) => ({
  ...state,
  fetchingPricingPlans: false,
  pricingPlans: null,
  error,
});

handlers[SET_PRICING_PLANS_CURRENCY] = (state, { iso, symbol }) => ({
  ...state,
  iso,
  symbol,
});

handlers[SET_DISCOUNTED_PRICING_PLANS] = (state, { pricingPlans }) => ({
  ...state,
  fetchingPricingPlans: false,
  pricingPlans,
});

export default createReducer(handlers, initialState);
