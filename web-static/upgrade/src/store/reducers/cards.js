import createReducer from './createReducer';
import {
  FETCH_CARDS,
  FETCH_CARDS_FAIL,
  FETCH_CARDS_SUCCESS,
  TOGGLE_SHOW_CARDS,
  TOGGLE_SHOW_NEW_CARD_FORM,
  SET_SHOW_EDIT_CARD_FORM,
  SET_SELECTED_CARD_SOURCE,
  SET_DEFAULT_CARD_SOURCE,
  SET_DEFAULT_CARD_SOURCE_SUCCESS,
  SET_DEFAULT_CARD_SOURCE_FAIL,
} from '../actions/cards';

const initialState = {
  cards: null,
  defaultSource: null,
  selectedCard: null,
  showCards: true,
  showNewCardForm: null,
  submitting: false,
  cardToEdit: null,
  error: null,
  editCardError: null,
};

const handlers = {};

handlers[FETCH_CARDS] = (state) => ({
  ...state,
});

handlers[FETCH_CARDS_SUCCESS] = (state, { cards, defaultSource, showCards }) => ({
  ...state,
  cards,
  defaultSource,
  selectedCard: defaultSource,
  showCards,
});

handlers[FETCH_CARDS_FAIL] = (state, { error }) => ({
  ...state,
  error,
});

handlers[TOGGLE_SHOW_CARDS] = (state, { showCards }) => ({
  ...state,
  showCards,
});

handlers[TOGGLE_SHOW_NEW_CARD_FORM] = (state, { showNewCardForm }) => ({
  ...state,
  showNewCardForm,
  cardToEdit: null,
});

handlers[SET_SHOW_EDIT_CARD_FORM] = (state, { cardToEdit }) => {
  const { cardToEdit: currentCardToEdit } = state;
  return {
    ...state,
    cardToEdit: cardToEdit !== currentCardToEdit ? cardToEdit : null,
    showNewCardForm: false,
  };
};

handlers[SET_SELECTED_CARD_SOURCE] = (state, { selectedCard }) => ({
  ...state,
  selectedCard,
});

handlers[SET_DEFAULT_CARD_SOURCE] = (state, { defaultSource }) => ({
  ...state,
  defaultSource,
  submitting: true,
});

handlers[SET_DEFAULT_CARD_SOURCE_SUCCESS] = (state, { defaultSource }) => ({
  ...state,
  defaultSource,
  cardToEdit: null,
  submitting: false,
  editCardError: null,
});

handlers[SET_DEFAULT_CARD_SOURCE_FAIL] = (state, { error }) => ({
  ...state,
  editCardError: error,
  submitting: false,
});

export default createReducer(handlers, initialState);
