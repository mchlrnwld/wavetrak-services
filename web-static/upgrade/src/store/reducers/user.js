import createReducer from './createReducer';
import {
  FETCH_USER,
  FETCH_USER_SUCCESS,
  FETCH_USER_FAILURE,
  LOGIN,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGIN_RESET,
  CREATE,
  CREATE_SUCCESS,
  CREATE_FAIL,
  SUBMIT_SUBSCRIPTION,
  SUBMIT_SUBSCRIPTION_SUCCESS,
  SUBMIT_SUBSCRIPTION_FAIL,
  FBLOGIN,
  FBLOGIN_FAIL,
  FBLOGIN_SUCCESS,
  SET_GEO_DATA,
  STRIPE_CHECKOUT,
  STRIPE_CHECKOUT_FAIL,
} from '../actions/user';

const initialState = {
  countryISO: null,
  timeZone: null,
  error: null,
  success: null,
  deleted: false,
  loading: false,
  facebookLoading: false,
  planChanged: false,
  currentSubscriptionForBrand: null,
  upcomingInvoice: null,
  details: null,
};
const handlers = {};

handlers[FETCH_USER] = (state) => ({
  ...state,
  loading: true,
});
handlers[FETCH_USER_SUCCESS] = (state, { user }) => ({
  ...state,
  details: {
    ...user,
  },
  error: null,
  loading: false,
});
handlers[FETCH_USER_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});
handlers[CREATE] = (state) => ({
  ...state,
  loading: true,
});
handlers[CREATE_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});
handlers[CREATE_SUCCESS] = (state, { accessToken }) => ({
  ...state,
  accessToken,
  loading: false,
});
handlers[LOGIN] = (state) => ({
  ...state,
  loading: true,
});
handlers[LOGIN_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});
handlers[LOGIN_SUCCESS] = (state, { accessToken }) => ({
  ...state,
  accessToken,
  loading: false,
});
handlers[LOGIN_RESET] = (state) => ({
  ...state,
  ...initialState,
});

handlers[FBLOGIN] = (state) => ({
  ...state,
  facebookLoading: true,
});
handlers[FBLOGIN_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
  facebookLoading: false,
});

handlers[FBLOGIN_SUCCESS] = (state, { accessToken }) => ({
  ...state,
  accessToken,
  facebookLoading: false,
});

handlers[SUBMIT_SUBSCRIPTION] = (state) => ({
  ...state,
  error: null,
});

handlers[SUBMIT_SUBSCRIPTION_SUCCESS] = (state) => ({
  ...state,
  error: null,
  success: true,
});

handlers[SUBMIT_SUBSCRIPTION_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
});

handlers[SET_GEO_DATA] = (state, { countryCode, timeZone, subdivisionCode }) => ({
  ...state,
  countryCode,
  subdivisionCode,
  location: {
    time_zone: timeZone,
  },
});

handlers[STRIPE_CHECKOUT] = (state) => ({
  ...state,
  error: null,
});

handlers[STRIPE_CHECKOUT_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
});

export default createReducer(handlers, initialState);
