import { getWavetrakIdentity, SL_METERED } from '@surfline/web-common';
import { getAnonymousMeter, getMeter } from '../../common/api/meter';
import { getAccessToken } from '../selectors/auth';
import { getEntitlements } from '../selectors/entitlements';
import { SL_RATE_LIMITING_V3 } from '../../common/utils/treatments';
import { ON } from '../../common/constants';

export const SETUP_METERING = 'SETUP_METERING';
export const SETUP_METERING_FAIL = 'SETUP_METERING_FAIL';
export const SET_METER = 'SET_METER';

export const setMeter = (meter) => ({ type: SET_METER, meter });

export const setupMetering = () => async (dispatch, getState) => {
  dispatch({ type: SETUP_METERING });
  try {
    const state = getState();
    const identity = getWavetrakIdentity();
    const accessToken = getAccessToken(state);
    const entitlements = getEntitlements(state);
    const isMetered = entitlements.indexOf(SL_METERED) > -1;

    // If it is a registered user with `sl_metered` load the meter document
    if (accessToken && isMetered) {
      const meter = await getMeter();
      return dispatch(setMeter(meter));
    }

    // If it is not a registered user check if they have an existing
    // anonymous meter and load it if so
    if (!accessToken) {
      const meter = await getAnonymousMeter(identity.anonymousId);
      if (meter) {
        return dispatch(
          setMeter({
            ...meter,
            anonymousId: identity.anonymousId,
            treatmentName: SL_RATE_LIMITING_V3,
            treatmentState: ON,
          }),
        );
      }
    }

    // If they do not have a meter document in redis or mongo, then complete
    // setting up the meter with null
    return dispatch(setMeter(null));
  } catch (error) {
    return dispatch({ type: SETUP_METERING_FAIL, error: error.message });
  }
};
