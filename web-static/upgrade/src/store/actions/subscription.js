import { trackEvent, SL, nrAddPageAction, nrNoticeError } from '@surfline/web-common';
import LogRocket from 'logrocket';
import getConfig from '../../config';
import {
  stopSubmitSpinner,
  startSubmitSpinner,
  redirectToOnboarding,
  redirectToMultiCam,
} from './interactions';
import {
  upgrade,
  getCurrencyForUser,
  verifyStripeSubscription,
} from '../../common/api/subscription';
import { showDuplicateCard, clearDuplicateCard } from './duplicateCard';
import { SET_PLANS_CURRENCY, FETCH_PLANS_FAIL } from './plans';
import { getCountryCode } from '../selectors/user';
import { getSelectedPlan } from '../selectors/plans';
import { getAccessToken } from '../selectors/auth';
import trackSubscriptionReceiptReceived from '../../utils/trackSubscriptionReceiptReceived';

const config = getConfig();
const { brand } = config;

export const SUBMIT_SUBSCRIPTION = 'SUBMIT_SUBSCRIPTION';
export const SUBMIT_SUBSCRIPTION_SUCCESS = 'SUBMIT_SUBSCRIPTION_SUCCESS';
export const SUBMIT_SUBSCRIPTION_FAIL = 'SUBMIT_SUBSCRIPTION_FAIL';
export const FETCH_SUBSCRIPTION_VERIFICATION = 'FETCH_SUBSCRIPTION_VERIFICATION';
export const FETCH_SUBSCRIPTION_VERIFICATION_SUCCESS = 'FETCH_SUBSCRIPTION_VERIFICATION_SUCCESS';
export const FETCH_SUBSCRIPTION_VERIFICATION_FAIL = 'FETCH_SUBSCRIPTION_VERIFICATION_FAIL';

export const submitSubscription = (values) => async (dispatch, getState) => {
  const {
    plan,
    source,
    selectedCard,
    defaultCardId,
    accessToken,
    coupon,
    trialEligible,
    digitalWallet,
    shouldRedirectToOnboarding,
  } = values;
  dispatch(startSubmitSpinner());
  nrAddPageAction('Submit subscription started', {
    plan,
    payload: {
      selectedCard,
      defaultCardId,
      coupon,
      trialEligible,
    },
  });

  // we must have either new CC entered OR existing default CC for submission
  const paymentSource = source || selectedCard;

  if (!plan || !paymentSource) {
    dispatch(stopSubmitSpinner());
    nrAddPageAction('Submit subscription failed - credit card required', {
      plan,
      payload: {
        selectedCard,
        defaultCardId,
        coupon,
      },
    });
    return dispatch({
      type: SUBMIT_SUBSCRIPTION_FAIL,
      message: 'A valid credit card is required.',
    });
  }

  try {
    dispatch({ type: SUBMIT_SUBSCRIPTION });
    nrAddPageAction('Subscription submitted', {
      plan,
    });
    const subscriptionApiParams = {
      planId: plan,
      coupon,
      source: source ? source.id : null,
      cardId: selectedCard !== defaultCardId ? selectedCard : defaultCardId,
      trialEligible,
    };
    const { currency, name, amount } = getSelectedPlan(getState());
    const { subscriptionId } = await upgrade(accessToken, subscriptionApiParams);

    nrAddPageAction('Subscription checkout completed', {
      plan,
      payload: {
        subscriptionId,
        effectivePrice: amount,
      },
    });

    trackEvent('Completed Checkout Step', {
      step: 2,
      currency: currency.toUpperCase(),
    });

    trackEvent('Completed Order', {
      orderId: subscriptionId,
      total: 0,
      coupon,
      currency: currency.toUpperCase(),
      products: [
        {
          product_id: plan,
          sku: plan,
          name,
          price: amount / 100,
          quantity: 1,
          category: 'Subscription',
        },
      ],
      paymentSource: digitalWallet || 'credit card',
    });

    if (config.company === SL && shouldRedirectToOnboarding) {
      nrAddPageAction('Subscription created, redirecting to onboarding', subscriptionId);
      return dispatch(redirectToOnboarding());
    }

    // For regular upgrade funnel users that have been onboarded, send them to the multi-cam page.
    if (config.company === SL) {
      nrAddPageAction('Subscription created, redirecting to multi-cam');
      return dispatch(redirectToMultiCam());
    }

    dispatch({ type: SUBMIT_SUBSCRIPTION_SUCCESS });
    nrAddPageAction('Subscription created: SUCCESS', {
      plan,
      payload: {
        subscriptionId,
      },
    });
    return dispatch(stopSubmitSpinner());
  } catch (error) {
    dispatch(stopSubmitSpinner());
    const { code, message } = error;

    if (code === 1001) {
      nrAddPageAction('Submit subscription failed - Duplicate card', { plan, error });
      return dispatch(showDuplicateCard({ message, source, plan }));
    }
    dispatch(clearDuplicateCard());

    LogRocket.captureException(error, {
      tags: {
        message: error.message,
        status: error.status,
      },
    });
    let lrRecordingURL;
    LogRocket.getSessionURL((sessionURL) => {
      lrRecordingURL = sessionURL;
    });
    nrNoticeError(error);
    nrAddPageAction('Submit subscription failed', { plan, error, lrRecordingURL });

    return dispatch({ type: SUBMIT_SUBSCRIPTION_FAIL, message });
  }
};

export const fetchCurrency = () => async (dispatch, getState) => {
  try {
    const countryISO = getCountryCode(getState());
    const { iso, symbol, monthly, annual } = await getCurrencyForUser(countryISO, brand);
    return dispatch({
      type: SET_PLANS_CURRENCY,
      iso,
      symbol,
      monthly,
      annual,
    });
  } catch (err) {
    const { message } = err;
    return dispatch({ type: FETCH_PLANS_FAIL, message });
  }
};

export const verifySubscription = () => async (dispatch, getState) => {
  try {
    dispatch(startSubmitSpinner());
    const accessToken = getAccessToken(getState());
    if (!accessToken) {
      dispatch(stopSubmitSpinner());
      return dispatch({
        type: FETCH_SUBSCRIPTION_VERIFICATION_FAIL,
        message: 'A valid token is required.',
      });
    }
    dispatch({ type: FETCH_SUBSCRIPTION_VERIFICATION });
    const {
      status: verificationStatus,
      message: verificationMessage,
      subscription,
    } = await verifyStripeSubscription(accessToken, { brand });
    dispatch(stopSubmitSpinner());
    if (subscription) {
      trackSubscriptionReceiptReceived(subscription);
    }
    return dispatch({
      type: FETCH_SUBSCRIPTION_VERIFICATION_SUCCESS,
      verificationMessage,
      verificationStatus,
    });
  } catch (err) {
    const { message } = err;
    dispatch(stopSubmitSpinner());
    return dispatch({ type: FETCH_SUBSCRIPTION_VERIFICATION_FAIL, message });
  }
};
