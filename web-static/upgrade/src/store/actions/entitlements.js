import getConfig from '../../config';
import { getEntitlement } from '../../common/api/entitlement';
import { getAccessToken } from '../selectors/auth';

const config = getConfig();

export const FETCH_ENTITLEMENTS = 'FETCH_ENTITLEMENTS';
export const FETCH_ENTITLEMENTS_SUCCESS = 'FETCH_ENTITLEMENTS_SUCCESS';
export const FETCH_ENTITLEMENTS_FAIL = 'FETCH_ENTITLEMENTS_FAIL';
export const DISABLE_FREE_TRIAL = 'DISABLE_FREE_TRIAL';
export const ENABLE_FREE_TRIAL = 'ENABLE_FREE_TRIAL';

export const fetchEntitlements = () => async (dispatch, getState) => {
  const accessToken = getAccessToken(getState());
  if (!accessToken) return null;

  dispatch({ type: FETCH_ENTITLEMENTS });
  try {
    const { entitlements, promotions } = await getEntitlement(accessToken);
    return dispatch({ type: FETCH_ENTITLEMENTS_SUCCESS, entitlements, promotions });
  } catch (error) {
    return dispatch({ type: FETCH_ENTITLEMENTS_FAIL, error });
  }
};

export const disableFreeTrial = () => async (dispatch) =>
  dispatch({ type: DISABLE_FREE_TRIAL, company: config.company });

export const enableFreeTrial = () => async (dispatch) =>
  dispatch({ type: ENABLE_FREE_TRIAL, company: config.company });
