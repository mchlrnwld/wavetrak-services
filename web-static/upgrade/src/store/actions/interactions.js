import { SL, trackEvent, getTreatment } from '@surfline/web-common';
import getConfig from '../../config';
import performRedirect from '../../utils/performRedirect';
import { SL_FT_BW_WEB_STRIPE_CHECKOUT } from '../../common/utils/treatments';
import { OFF, OVER_UNDER, SIDE_BY_SIDE, OLD } from '../../common/constants';

const config = getConfig();

export const TOGGLE_REGISTRATION_FORM = 'TOGGLE_REGISTRATION_FORM';
export const START_SUBMIT_SPINNER = 'START_SUBMIT_SPINNER';
export const STOP_SUBMIT_SPINNER = 'STOP_SUBMIT_SPINNER';
export const FETCH_STRIPE_CHECKOUT_TREATMENT = 'FETCH_STRIPE_CHECKOUT_TREATMENT';
export const FETCH_STRIPE_CHECKOUT_TREATMENT_SUCCESS = 'FETCH_STRIPE_CHECKOUT_TREATMENT_SUCCESS';
export const FETCH_STRIPE_CHECKOUT_TREATMENT_FAIL = 'FETCH_STRIPE_CHECKOUT_TREATMENT_FAIL';

export const toggleRegistrationForm = (showLogin, location) => async (dispatch) => {
  const event = showLogin
    ? 'Sign In Form Clicked Create Account'
    : 'Create Account Form Clicked Sign In';
  const promotionId = config.funnelConfig ? config.funnelConfig._id : undefined;
  trackEvent(event, { location, promotionId });
  dispatch({ type: TOGGLE_REGISTRATION_FORM, showLogin: !showLogin });
};

export const startSubmitSpinner = () => async (dispatch) => {
  dispatch({ type: START_SUBMIT_SPINNER });
};

export const stopSubmitSpinner = () => async (dispatch) => {
  dispatch({ type: STOP_SUBMIT_SPINNER });
};

export const redirectToOnboarding = () => async () => {
  if (config.company === SL) return window.location.assign('/onboarding');
  return performRedirect();
};

export const redirectToMultiCam = () => async () => {
  if (config.company === SL) return window.location.assign('/surf-cams');
  return performRedirect();
};

export const getStripeCheckoutTreatment = () => async (dispatch) => {
  dispatch({ type: FETCH_STRIPE_CHECKOUT_TREATMENT });
  try {
    const stripeCheckoutTreatment = getTreatment(SL_FT_BW_WEB_STRIPE_CHECKOUT);
    const isStripeCheckoutEnabled =
      stripeCheckoutTreatment === OVER_UNDER ||
      stripeCheckoutTreatment === SIDE_BY_SIDE ||
      stripeCheckoutTreatment === OLD;
    return dispatch({
      type: FETCH_STRIPE_CHECKOUT_TREATMENT_SUCCESS,
      stripeCheckoutTreatment,
      isStripeCheckoutEnabled,
    });
  } catch (error) {
    return dispatch({
      type: FETCH_STRIPE_CHECKOUT_TREATMENT_FAIL,
      stripeCheckoutTreatment: OFF,
    });
  }
};
