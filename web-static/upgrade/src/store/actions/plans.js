import { getPlans } from '../../common/api/subscription';
import getConfig from '../../config';
import { getAccessToken } from '../selectors/auth';
import { getCountryCode } from '../selectors/user';

const config = getConfig();

export const FETCH_PLANS = 'FETCH_PLANS';
export const FETCH_PLANS_SUCCESS = 'FETCH_PLANS_SUCCESS';
export const FETCH_PLANS_FAIL = 'FETCH_PLANS_FAIL';
export const TOGGLE_SELECTED_PLANID = 'TOGGLE_SELECTED_PLANID';
export const SET_DISCOUNTED_PLANS = 'SET_DISCOUNTED_PLANS';
export const SET_PLANS_CURRENCY = 'SET_PLANS_CURRENCY';

export const fetchPlans = () => async (dispatch, getState) => {
  const accessToken = getAccessToken(getState());
  if (!accessToken) return null;

  dispatch({ type: FETCH_PLANS });
  try {
    const { company: brand } = config;
    const countryISO = getCountryCode(getState());

    const { plans } = await getPlans(accessToken, brand, 'all', countryISO);
    const [{ symbol, iso }] = plans;

    dispatch({ type: SET_PLANS_CURRENCY, iso, symbol });
    const { id } =
      plans.length > 1 ? plans.find((plan) => plan.interval.includes('year')) : plans[0];
    return dispatch({ type: FETCH_PLANS_SUCCESS, plans, selectedPlanId: id });
  } catch (error) {
    return dispatch({ type: FETCH_PLANS_FAIL, error });
  }
};

export const toggleSelectedPlanId = (selectedPlanId) => async (dispatch) => {
  dispatch({ type: TOGGLE_SELECTED_PLANID, selectedPlanId });
};

export const setDiscountedPlans = (plans) => async (dispatch) => {
  const { id } = plans.length > 1 ? plans.find((plan) => plan.interval.includes('year')) : plans[0];
  dispatch({ type: SET_DISCOUNTED_PLANS, plans, selectedPlanId: id });
};
