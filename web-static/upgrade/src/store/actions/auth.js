import { authenticateAccessToken, refreshAccessToken } from '@surfline/web-common';
import removeCookie from '../../utils/removeCookie';
import { getAccessToken, getRefreshToken } from '../../utils/getAccessToken';
import setLoginCookie from '../../utils/setLoginCookie';
import getConfig from '../../config';

const config = getConfig();
const ACCESS_TOKEN_MAX_AGE = 2592000;

export const SET_ACCESS_TOKEN = 'SET_ACCESS_TOKEN';
export const REMOVE_ACCESS_TOKEN = 'REMOVE_ACCESS_TOKEN';

export const setAccessToken = () => (dispatch) => {
  const accessToken = getAccessToken();

  if (accessToken) {
    dispatch({ type: SET_ACCESS_TOKEN, accessToken });
  }
};

export const validateAccessToken = () => async (dispatch) => {
  const { servicesEndpoint, clientId, clientSecret } = config;
  try {
    const accessToken = getAccessToken();
    if (!accessToken) return null;
    await authenticateAccessToken(servicesEndpoint, accessToken, clientId, clientSecret);
    dispatch({ type: SET_ACCESS_TOKEN, accessToken, userInitiated: false });
    return { isValid: true };
  } catch (error) {
    // if we have a refresh token, get new accessToken from /trusted/token
    const refreshToken = getRefreshToken();
    if (refreshToken) {
      try {
        const {
          access_token: newAccessToken,
          refresh_token: newRefreshToken,
        } = await refreshAccessToken(servicesEndpoint, refreshToken, clientId, clientSecret);
        setLoginCookie(newAccessToken, newRefreshToken, ACCESS_TOKEN_MAX_AGE);
        dispatch({ type: SET_ACCESS_TOKEN, accessToken: newAccessToken, userInitiated: false });
        return { isValid: true };
      } catch (err) {
        await removeCookie('access_token');
        await removeCookie('refresh_token');
        dispatch({ type: REMOVE_ACCESS_TOKEN });
        return { isValid: false };
      }
    }
    await removeCookie('access_token');
    await removeCookie('refresh_token');
    dispatch({ type: REMOVE_ACCESS_TOKEN });
    return { isValid: false };
  }
};
