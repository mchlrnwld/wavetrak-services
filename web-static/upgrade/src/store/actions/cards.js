import { getCards } from '../../common/api/subscription';
import { getAccessToken } from '../selectors/auth';

export const FETCH_CARDS = 'FETCH_CARDS';
export const FETCH_CARDS_SUCCESS = 'FETCH_CARDS_SUCCESS';
export const FETCH_CARDS_FAIL = 'FETCH_CARDS_FAIL';
export const TOGGLE_SHOW_CARDS = 'TOGGLE_SHOW_CARDS';
export const TOGGLE_SHOW_NEW_CARD_FORM = 'TOGGLE_SHOW_NEW_CARD_FORM';
export const SET_SHOW_EDIT_CARD_FORM = 'SET_SHOW_EDIT_CARD_FORM';
export const SET_SELECTED_CARD_SOURCE = 'SET_SELECTED_CARD_SOURCE';
export const SET_DEFAULT_CARD_SOURCE = 'SET_DEFAULT_CARD_SOURCE';
export const SET_DEFAULT_CARD_SOURCE_SUCCESS = 'SET_DEFAULT_CARD_SOURCE_SUCCESS';
export const SET_DEFAULT_CARD_SOURCE_FAIL = 'SET_DEFAULT_CARD_SOURCE_FAIL';

export const fetchCards = () => async (dispatch, getState) => {
  const accessToken = getAccessToken(getState());
  if (!accessToken) return null;

  try {
    const { cards, defaultSource } = await getCards(accessToken);
    const showCards = !!cards;
    return dispatch({
      type: FETCH_CARDS_SUCCESS,
      cards,
      defaultSource,
      showCards,
    });
  } catch (error) {
    const { message } = error;
    return dispatch({ type: FETCH_CARDS_FAIL, error: message });
  }
};

export const toggleShowCards = (showCards) => async (dispatch) => {
  dispatch({ type: TOGGLE_SHOW_CARDS, showCards: !showCards });
};

export const toggleShowNewCardForm = (showNewCardForm) => async (dispatch) => {
  dispatch({ type: TOGGLE_SHOW_NEW_CARD_FORM, showNewCardForm: !showNewCardForm });
};

export const setSelectedCardSource = (cardId) => async (dispatch) => {
  dispatch({ type: SET_SELECTED_CARD_SOURCE, selectedCard: cardId });
};
