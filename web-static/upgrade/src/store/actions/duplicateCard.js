import { disableFreeTrial, enableFreeTrial } from './entitlements';

export const SHOW_DUPLICATE_CARD = 'SHOW_DUPLICATE_CARD';
export const CLEAR_DUPLICATE_CARD = 'CLEAR_DUPLICATE_CARD';

export const showDuplicateCard = ({ message, plan, source }) => async (dispatch) => {
  dispatch(disableFreeTrial());
  dispatch({ type: SHOW_DUPLICATE_CARD, message, plan, source });
};

export const clearDuplicateCard = () => async (dispatch) => {
  dispatch(enableFreeTrial());
  dispatch({ type: CLEAR_DUPLICATE_CARD });
};

export const reloadFromDuplicateCard = () => {
  window.location.reload();
};
