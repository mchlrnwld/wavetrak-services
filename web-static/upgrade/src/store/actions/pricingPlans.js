import { getPricingPlans } from '../../common/api/subscription';
import getConfig from '../../config';
import { getAccessToken } from '../selectors/auth';
import { getCountryCode } from '../selectors/user';
import { getFunnelConfig } from '../selectors/promotion';

const config = getConfig();

export const FETCH_PRICING_PLANS = 'FETCH_PRICING_PLANS';
export const FETCH_PRICING_PLANS_SUCCESS = 'FETCH_PRICING_PLANS_SUCCESS';
export const FETCH_PRICING_PLANS_FAIL = 'FETCH_PRICING_PLANS_FAIL';
export const SET_PRICING_PLANS_CURRENCY = 'SET_PRICING_PLANS_CURRENCY';
export const SET_DISCOUNTED_PRICING_PLANS = 'SET_DISCOUNTED_PRICING_PLANS';

export const fetchPricingPlans = () => async (dispatch, getState) => {
  const accessToken = getAccessToken(getState());
  if (!accessToken) return null;

  dispatch({ type: FETCH_PRICING_PLANS });
  try {
    const { company: brand } = config;
    const countryISO = getCountryCode(getState());

    const funnelConfig = getFunnelConfig(getState());
    const filter = funnelConfig?.plans || 'all';

    const { plans } = await getPricingPlans(accessToken, brand, filter, countryISO);
    const [{ symbol, iso }] = plans;
    dispatch({ type: SET_PRICING_PLANS_CURRENCY, iso, symbol });
    return dispatch({
      type: FETCH_PRICING_PLANS_SUCCESS,
      pricingPlans: plans,
    });
  } catch (error) {
    return dispatch({ type: FETCH_PRICING_PLANS_FAIL, error });
  }
};

export const setDiscountedPricingPlans = (plans) => async (dispatch) => {
  const { company: brand } = config;
  dispatch({
    type: SET_DISCOUNTED_PRICING_PLANS,
    pricingPlans: plans.map((plan) => ({
      ...plan,
      brand,
    })),
  });
};
