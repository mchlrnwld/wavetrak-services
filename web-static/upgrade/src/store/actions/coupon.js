import { Cookies } from 'react-cookie';
import { validateCoupon } from '../../common/api/subscription';
import getConfig from '../../config';
import { getCountryCode } from '../selectors/user';
import { setDiscountedPlans } from './plans';
import { setDiscountedPricingPlans } from './pricingPlans';

const cookie = new Cookies();
const config = getConfig();

export const SET_COUPON = 'SET_COUPON';
export const VALIDATE_COUPON = 'VALIDATE_COUPON';
export const VALIDATE_COUPON_SUCCESS = 'VALIDATE_COUPON_SUCCESS';
export const VALIDATE_COUPON_FAIL = 'VALIDATE_COUPON_FAIL';
export const OPEN_COUPON_FORM = 'OPEN_COUPON_FORM';
export const CLOSE_COUPON_FORM = 'CLOSE_COUPON_FORM';
export const TOGGLE_COUPON_FORM = 'TOGGLE_COUPON_FORM';
export const CLEAR_COUPON_ERROR = 'CLEAR_COUPON_ERROR';

export const fetchCoupon = (coupon) => async (dispatch, getState) => {
  dispatch({ type: VALIDATE_COUPON });
  try {
    if (!coupon?.length) {
      return dispatch({
        type: VALIDATE_COUPON_FAIL,
        error: {
          message: 'Coupon cannont be blank',
        },
      });
    }
    const { company } = config;
    const accessToken = cookie.get('access_token');
    const countryISO = getCountryCode(getState());
    const { plans } = await validateCoupon(accessToken, company, coupon, countryISO);
    dispatch(setDiscountedPlans(plans));
    dispatch(setDiscountedPricingPlans(plans));
    return dispatch({ type: VALIDATE_COUPON_SUCCESS, coupon });
  } catch (error) {
    return dispatch({ type: VALIDATE_COUPON_FAIL, error });
  }
};

export const toggleCouponForm = () => async (dispatch) => dispatch({ type: TOGGLE_COUPON_FORM });

export const clearCouponError = () => async (dispatch) => dispatch({ type: CLEAR_COUPON_ERROR });
