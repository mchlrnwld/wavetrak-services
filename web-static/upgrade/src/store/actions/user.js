import { serializeError } from 'serialize-error';
import {
  trackEvent,
  SL,
  nrAddPageAction,
  nrNoticeError,
  setWavetrakIdentity,
  getWavetrakIdentity,
  findSubdivisionCode,
  getWindow,
} from '@surfline/web-common';
import setLoginCookie from '../../utils/setLoginCookie';
import getConfig from '../../config';
import { generateToken, authorise } from '../../common/api/auth';
import { postUser, getUser } from '../../common/api/user';
import { setAccessToken } from './auth';
import { getMeter } from '../../common/api/meter';
import { stopSubmitSpinner, startSubmitSpinner } from './interactions';
import deviceInfo from '../../common/utils/deviceInfo';
import { initiateStripeCheckout } from '../../common/api/subscription';
import { setMeter } from './meter';
import { identifyUser } from '../../utils/identifyUser';
import { getLocation } from '../../common/api/geo';
import { getAccessToken } from '../selectors/auth';
import { getIsStripeCheckoutEnabled } from '../selectors/interactions';

const config = getConfig();
const { brand, clientId, redirectUrl } = config;

export const CREATE = 'CREATE';
export const CREATE_SUCCESS = 'CREATE_SUCCESS';
export const CREATE_FAIL = 'CREATE_FAIL';
export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';

export const LOGIN = 'LOGIN';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_RESET = 'LOGIN_RESET';

export const FBLOGIN = 'FBLOGIN';
export const FBLOGIN_RESET = 'FBLOGIN_RESET';
export const FBLOGIN_SUCCESS = 'FBLOGIN_SUCCESS';
export const FBLOGIN_FAIL = 'FBLOGIN_FAIL';
export const FBLOGINERROR = 'FBLOGINERROR';
export const FBLOGINERROR_SUCCESS = 'FBLOGINERROR_SUCCESS';
export const FBLOGINERROR_FAIL = 'FBLOGINERROR_FAIL';
export const FBSUBMIT = 'FBSUBMIT';
export const FBSUBMIT_SUCCESS = 'FBSUBMIT_SUCCESS';
export const FBSUBMIT_FAIL = 'FBSUBMIT_FAIL';

export const SUBMIT_SUBSCRIPTION = 'SUBMIT_SUBSCRIPTION';
export const SUBMIT_SUBSCRIPTION_SUCCESS = 'SUBMIT_SUBSCRIPTION_SUCCESS';
export const SUBMIT_SUBSCRIPTION_FAIL = 'SUBMIT_SUBSCRIPTION_FAIL';
export const STRIPE_CHECKOUT = 'STRIPE_CHECKOUT';
export const STRIPE_CHECKOUT_FAIL = 'STRIPE_CHECKOUT_FAIL';

export const login = (values, currency) => async (dispatch) => {
  try {
    dispatch(startSubmitSpinner());
    dispatch({ type: LOGIN });
    const { email, password, staySignedIn, location, promotionId } = values;
    const { deviceId, deviceType } = deviceInfo();

    const valuesToEncode = {
      grant_type: 'password',
      username: email,
      password,
      device_id: deviceId,
      device_type: deviceType,
      forced: true,
    };

    const urlEncodedFormValues = [];
    const keysToEncode = Object.keys(valuesToEncode);

    keysToEncode.forEach((key) => {
      const encodedKey = encodeURIComponent(key);
      const encodedValue = encodeURIComponent(valuesToEncode[key]);
      urlEncodedFormValues.push(`${encodedKey}=${encodedValue}`);
    });

    const urlEncodedFormString = urlEncodedFormValues.join('&');

    const {
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: expiresIn,
    } = await generateToken(!staySignedIn, urlEncodedFormString);

    const { id } = await authorise(accessToken);

    identifyUser(id);

    trackEvent('Signed In', {
      method: 'email',
      email,
      location,
      promotionId,
      platform: 'web',
    });

    if (location !== 'register') {
      trackEvent('Completed Checkout Step', {
        step: 1,
        promotionId,
        currency: currency.toUpperCase(),
      });
    }

    const maxAge = values.staySignedIn ? expiresIn : 1800;
    const refreshMaxAge = values.staySignedIn ? 31536000 : 1800;

    await setLoginCookie(accessToken, refreshToken, maxAge, refreshMaxAge);
    dispatch(setAccessToken(accessToken));

    if (accessToken && brand === SL) {
      try {
        const meter = await getMeter();
        dispatch(setMeter(meter));
      } catch (err) {
        // Do nothing
      }
    }

    dispatch(stopSubmitSpinner());
    return dispatch({ type: LOGIN_SUCCESS, accessToken });
  } catch (error) {
    const errorString = JSON.stringify(serializeError(error));
    if (getWindow()?.newrelic) {
      const errorObj = {
        message: 'Error in Login',
        errorString,
      };
      // eslint-disable-next-line no-param-reassign
      delete values.password; // Remove password so that we don't log in NewRelic
      const customErrorAttributes = {
        errorString,
        payloadString: JSON.stringify(values),
      };
      nrNoticeError(errorObj, customErrorAttributes);
      nrAddPageAction('LoginProgress Error', { errorString });
    }
    dispatch(stopSubmitSpinner());
    return dispatch({ type: LOGIN_FAIL, message: error.error_description });
  }
};

export const register = (values, currency) => async (dispatch) => {
  try {
    dispatch(startSubmitSpinner());
    dispatch({ type: LOGIN });
    const {
      firstName,
      lastName,
      email,
      password,
      receivePromotions,
      staySignedIn,
      location,
      promotionId,
    } = values;
    const { deviceId, deviceType } = deviceInfo();

    const valuesForRegistration = {
      firstName,
      lastName,
      email,
      password,
      brand,
      settings: {
        receivePromotions,
      },
      client_id: clientId,
      device_id: deviceId,
      device_type: deviceType,
    };

    trackEvent('Clicked Create Account', {
      location,
      pageName: getWindow()?.location?.pathname,
      promotionId,
    });

    const {
      token: { access_token: accessToken, refresh_token: refreshToken, expires_in: expiresIn },
      user: { _id: userId },
    } = await postUser(!staySignedIn, valuesForRegistration);

    identifyUser(userId);

    if (location !== 'register') {
      trackEvent('Completed Checkout Step', {
        step: 1,
        promotionId,
        currency: currency.toUpperCase(),
      });
    }

    const maxAge = values.staySignedIn ? expiresIn : 1800;
    const refreshMaxAge = values.staySignedIn ? 31536000 : 1800;
    await setLoginCookie(accessToken, refreshToken, maxAge, refreshMaxAge);
    dispatch(setAccessToken());

    dispatch(stopSubmitSpinner());
    return dispatch({ type: CREATE_SUCCESS, accessToken });
  } catch (error) {
    let errorDescription = 'Sorry, we could not complete this request';
    if (error.status === 400) {
      errorDescription = error.message || errorDescription;
    }
    const err = { _error: errorDescription };
    dispatch({ type: CREATE_FAIL, message: errorDescription });
    dispatch(stopSubmitSpinner());
    throw err;
  }
};

export const fetchUser = () => async (dispatch, getState) => {
  const accessToken = getAccessToken(getState());
  if (!accessToken) return null;
  try {
    dispatch({ type: FETCH_USER });
    const user = await getUser(accessToken);
    return dispatch({ type: FETCH_USER_SUCCESS, user });
  } catch (err) {
    const { message } = err;
    return dispatch({ type: FETCH_USER_FAILURE, message });
  }
};

const facebookSDKLogin = () =>
  new Promise((resolve, reject) => {
    // eslint-disable-next-line
    FB.login(
      (response, error) => {
        if (error) return reject(error);
        if (response.authResponse) return resolve(response);
        return resolve({});
      },
      { scope: 'public_profile, email' },
    );
  });

const facebookSDKGetUserInfo = () =>
  new Promise((resolve, reject) => {
    // eslint-disable-next-line
    FB.api(
      '/me',
      {
        fields: 'name, email, first_name, last_name',
      },
      (userInfo, error) => {
        if (error) return reject(error);
        if (!userInfo.email) return reject(new Error({ message: 'An email address is required ' }));
        return resolve(userInfo);
      },
    );
  });

export const loginWithFacebook = (location, currency) => async (dispatch) => {
  try {
    const { authResponse: facebookAuthResponse } = await facebookSDKLogin();
    if (!facebookAuthResponse) return dispatch({ type: FBLOGIN_RESET });

    const {
      id: facebookId,
      email,
      first_name: firstName,
      last_name: lastName,
    } = await facebookSDKGetUserInfo();

    dispatch({ type: 'FBLOGIN' });
    const { deviceId, deviceType } = deviceInfo();
    const valuesToEncode = {
      grant_type: 'password',
      fbToken: facebookAuthResponse.accessToken,
      brand: config.company,
      username: email,
      password: null,
      facebookId,
      firstName,
      lastName,
      device_id: deviceId,
      device_type: deviceType,
    };

    const urlEncodedFormValues = [];
    const keysToEncode = Object.keys(valuesToEncode);

    keysToEncode.forEach((key) => {
      const encodedKey = encodeURIComponent(key);
      const encodedValue = encodeURIComponent(valuesToEncode[key]);
      urlEncodedFormValues.push(`${encodedKey}=${encodedValue}`);
    });
    const staySignedIn = true;
    const urlEncodedFormString = urlEncodedFormValues.join('&');
    const {
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: expiresIn,
    } = await generateToken(!staySignedIn, urlEncodedFormString);

    const { id } = await authorise(accessToken);
    identifyUser(id);

    trackEvent('Signed In', { method: 'facebook', email, location, platform: 'web' });

    setWavetrakIdentity({
      ...getWavetrakIdentity(),
      email,
      userId: id,
      type: 'logged_in',
    });

    if (location !== 'register') {
      trackEvent('Completed Checkout Step', { step: 1, currency: currency.toUpperCase() });
    }

    const maxAge = staySignedIn ? expiresIn : 1800;
    const refreshMaxAge = staySignedIn ? 31536000 : 1800;

    await setLoginCookie(accessToken, refreshToken, maxAge, refreshMaxAge);
    dispatch(setAccessToken(accessToken));
    if (accessToken && brand === SL) {
      try {
        const meter = await getMeter();
        dispatch(setMeter(meter));
      } catch (err) {
        // Do nothing
      }
    }
    return dispatch({ type: FBLOGIN_SUCCESS });
  } catch (error) {
    if (error.status === 1001) {
      return dispatch({ type: FBLOGIN_RESET });
    }
    const errorMessage = error.error_description || error.message;
    return dispatch({ type: FBLOGIN_FAIL, error: errorMessage });
  }
};

export default register;

export const SET_GEO_DATA = 'SET_GEO_DATA';

export const setGeoData = () => async (dispatch, getState) => {
  const accessToken = getAccessToken(getState());
  try {
    const {
      country: { iso_code: countryCode },
      location: { time_zone: timeZone },
      subdivisions = null,
    } = await getLocation(accessToken);
    const subdivisionCode = findSubdivisionCode(subdivisions) || null;
    await dispatch({ type: SET_GEO_DATA, countryCode, timeZone, subdivisionCode });
  } catch (error) {
    await dispatch({
      type: SET_GEO_DATA,
      countryCode: 'US',
      timeZone: 'America/Los_Angeles',
      subdivisionCode: 'CA',
    });
  }
};

export const startStripeCheckout = (values) => async (dispatch, getState) => {
  dispatch({ type: STRIPE_CHECKOUT });
  const accessToken = getAccessToken(getState());
  if (!accessToken) {
    dispatch(stopSubmitSpinner());
    return dispatch({
      type: STRIPE_CHECKOUT_FAIL,
      message: 'A valid token is required.',
    });
  }
  const isStripeCheckoutEnabled = getIsStripeCheckoutEnabled(getState());
  if (!isStripeCheckoutEnabled) {
    dispatch(stopSubmitSpinner());
    return dispatch({
      type: STRIPE_CHECKOUT_FAIL,
      message: 'User is not in Stripe checkout treatment',
    });
  }
  try {
    const {
      // eslint-disable-next-line no-shadow
      brand,
      currency,
      country,
      coupon,
      interval,
      promotionId,
      promotionUrlKey,
      shouldRedirectToOnboarding,
    } = values;

    let path = `/upgrade?subscriptionSuccess=true&shouldRedirectToOnboarding=${shouldRedirectToOnboarding}`;
    if (promotionUrlKey)
      path = `/${promotionUrlKey}?subscriptionSuccess=true&shouldRedirectToOnboarding=${shouldRedirectToOnboarding}`;
    const successUrl = `${redirectUrl}${path}`;
    const payload = {
      brand,
      interval,
      coupon,
      currency,
      country,
      promotionId,
      successUrl,
      cancelUrl: `${redirectUrl}/upgrade?selectedInterval=${interval}`,
    };
    const { url: stripeCheckoutUrl } = await initiateStripeCheckout(accessToken, payload);
    return (window.location.href = stripeCheckoutUrl);
  } catch (error) {
    dispatch(stopSubmitSpinner());
    return dispatch({ type: STRIPE_CHECKOUT_FAIL, message: error.message });
  }
};
