/* eslint-disable no-console */
const mkdirp = require('mkdirp');
const { exec } = require('child_process');

const { BRAND, APP_ENV } = process.env;
const outDir = `build/${BRAND}/out`;
const build = `npm run build-brand --$BRAND --$APP_ENV --outdir ${outDir}`;
mkdirp(outDir, err => {
  console.log('Building for: ', BRAND);
  console.log('Building for APP_ENV: ', APP_ENV);
  if (err) {
    console.error('Failed to create directory', outDir, err);
    throw Error(err);
  }
  exec(build, (error, stdout, stderr) => {
    console.log('Build output: ', stdout);
    if (error) {
      console.log('Failed to build: ', stderr);
      console.log(`Build exec error: ${error}`);
      throw Error(error);
    }
    return true;
  });
});
