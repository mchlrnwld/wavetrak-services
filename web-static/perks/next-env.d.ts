/// <reference types="next" />
/// <reference types="next/types/global" />
/// <reference types="next-images" />

interface Analytics {
  user: () => { anonymousId: () => string };
}

// eslint-disable-next-line import/prefer-default-export
export declare global {
  interface Window {
    analytics: Analytics;
  }
}
