import fs from 'fs';
import path from 'path';
import Mocha from 'mocha';
import chai from 'chai';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import chaiSubset from 'chai-subset';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import jsdom from 'jsdom-global';
import sinon from 'sinon';
import 'ignore-styles';

jsdom();

Enzyme.configure({ adapter: new Adapter() });

chai.use(dirtyChai);
chai.use(sinonChai);
chai.use(chaiSubset);

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
window.analytics = {
  user: () => ({
    anonymousId: () => 'anonymousId',
  }),
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  page: () => null,
};

delete window.location;
const assignStub = sinon.spy();

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
window.location = {
  href: '/',
  assign: assignStub,
};

const findTestFiles = (dir: string): string[] => {
  const contents = fs.readdirSync(dir);

  const filePaths: string[] = [];

  contents.forEach((element) => {
    const elementPath = path.join(dir, element);
    const stats = fs.statSync(elementPath);

    if (stats.isDirectory()) {
      Array.prototype.push.apply(filePaths, findTestFiles(elementPath));
    } else if (element.includes('spec.ts')) {
      filePaths.push(elementPath);
    }
  });

  return filePaths;
};

const runMocha = (dir: string): Promise<unknown> =>
  new Promise((resolve, reject) => {
    try {
      const mocha = new Mocha();
      const files = findTestFiles(dir);
      files.forEach((file) => {
        mocha.addFile(file);
      });
      mocha.run(resolve);
    } catch (err) {
      reject(err);
    }
  });

const tests = async (): Promise<void> => {
  const failures = await runMocha(__dirname);
  if (failures) process.exit(failures ? 1 : 0);
};

tests()
  .then(() => process.exit(0))
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
