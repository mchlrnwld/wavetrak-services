/* istanbul ignore file */

import { trackEvent, trackClickedSubscribeCTA, MONTHLY } from '@surfline/web-common';
import config from '../config';
import type { PlanType } from '../types/subscription';

export const trackPerkEvents = (
  planType: PlanType,
  props: { perkId?: string; perkName?: string },
): boolean => {
  const { perkId, perkName } = props;

  if (planType === MONTHLY) {
    return trackEvent('Clicked Switched to Yearly', {
      perkId,
      perkName: perkName || `${config.company} plan change`,
    });
  }

  return trackClickedSubscribeCTA({
    location: 'Premium Perks',
    perkId,
    perkName: perkName || `${config.company} go premium`,
    brand: config.company,
  });
};

export default trackPerkEvents;
