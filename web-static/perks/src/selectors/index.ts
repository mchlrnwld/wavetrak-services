import { RootState } from '../store';

export type Selector<T> = (state: RootState) => T;
