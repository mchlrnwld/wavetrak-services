import { expect } from 'chai';
import { getSubscription, getSubscriptionError, getSubscriptionLoading } from './subscription';
import { RootState } from '../store';

describe('Selectors / subscription', () => {
  it('getSubscription - should return existing state', () => {
    expect(getSubscription({ subscription: { loading: false } } as RootState)).to.deep.equal({
      loading: false,
    });
  });

  it('getSubscriptionError - should return existing error', () => {
    expect(getSubscriptionError({ subscription: { error: 'error' } } as RootState)).to.equal(
      'error',
    );
  });

  it('getSubscriptionError - should handle missing state', () => {
    expect(getSubscriptionError({} as RootState)).to.equal(undefined);
  });

  it('getSubscriptionLoading - should return the state', () => {
    expect(getSubscriptionLoading({ subscription: { loading: true } } as RootState)).to.be.true();
  });

  it('getSubscriptionLoading - should handle missing state', () => {
    expect(getSubscriptionLoading({} as RootState)).to.equal(undefined);
  });
});
