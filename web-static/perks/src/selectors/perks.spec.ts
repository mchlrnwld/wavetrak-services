import { expect } from 'chai';
import { getPerks, getPerksError, getPerksKey, getPerkList, getPerksLoading } from './perks';
import { RootState } from '../store';

describe('Selectors / perks', () => {
  it('getPerks - should return existing state', () => {
    expect(getPerks({ perks: { perks: [] } } as RootState)).to.deep.equal({ perks: [] });
  });

  it('getPerksError - should return existing error', () => {
    expect(getPerksError({ perks: { error: 'error' } } as RootState)).to.equal('error');
  });

  it('getPerksError - should handle missing state', () => {
    expect(getPerksError({} as RootState)).to.equal(undefined);
  });

  it('getPerksKey - should return the key', () => {
    expect(getPerksKey('error')({ perks: { error: 'error' } } as RootState)).to.equal('error');
  });

  it('getPerksKey - should handle missing state', () => {
    expect(getPerksKey('error')({} as RootState)).to.equal(undefined);
  });

  it('getPerkList - should handle missing state', () => {
    expect(getPerkList({} as RootState)).to.equal(undefined);
  });

  it('getPerkList - should return the state', () => {
    expect(getPerkList({ perks: { perks: [] } } as RootState)).to.deep.equal([]);
  });

  it('getPerksLoading - should return the state', () => {
    expect(getPerksLoading({ perks: { loading: true } } as RootState)).to.be.true();
  });

  it('getPerksLoading - should handle missing state', () => {
    expect(getPerksLoading({} as RootState)).to.equal(undefined);
  });
});
