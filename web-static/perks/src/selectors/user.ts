import type { RootState } from '../store';
import type { UserState } from '../reducers/user';

export type UserSelector<K extends keyof UserState> = (state: RootState) => UserState[K];

export const getAuthenticatedStatus: UserSelector<'authenticated'> = (state: RootState) => {
  const { user } = state;
  return user?.authenticated;
};

export const getUser = (state: RootState): UserState => {
  return state.user;
};

export const getUserError: UserSelector<'error'> = (state: RootState) => {
  const { user } = state;
  return user?.error;
};

export const getUserLoading: UserSelector<'loading'> = (state: RootState) => {
  const { user } = state;
  return user?.loading;
};

export const getUserCountryCode: UserSelector<'countryCode'> = (state: RootState) =>
  state?.user?.countryCode;
