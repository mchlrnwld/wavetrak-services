import type { RootState } from '../store';
import { PerksState } from '../reducers/perks';

export type PerksSelector<K extends keyof PerksState> = (state: RootState) => PerksState[K];

export const getPerks = (state: RootState): PerksState => state.perks;

export const getPerksError: PerksSelector<'error'> = (state) => {
  const { perks } = state;
  return perks?.error;
};

export const getPerksKey = <K extends keyof PerksState>(key: K): PerksSelector<K> => (state) => {
  const { perks } = state;
  return perks?.[key];
};

export const getPerkList: PerksSelector<'perks'> = (state) => state.perks?.perks;

export const getPerksLoading: PerksSelector<'loading'> = (state) => {
  const { perks } = state;
  return perks?.loading;
};
