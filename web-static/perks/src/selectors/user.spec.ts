import { expect } from 'chai';
import {
  getAuthenticatedStatus,
  getUser,
  getUserCountryCode,
  getUserError,
  getUserLoading,
} from './user';
import { RootState } from '../store';

describe('Selectors / user', () => {
  it('getUser - should return existing state', () => {
    expect(getUser({ user: { loading: false } } as RootState)).to.deep.equal({ loading: false });
  });

  it('getUserError - should return existing error', () => {
    expect(getUserError({ user: { error: 'error' } } as RootState)).to.equal('error');
  });

  it('getUserError - should handle missing state', () => {
    expect(getUserError({} as RootState)).to.equal(undefined);
  });

  it('getAuthenticatedStatus - should handle missing state', () => {
    expect(getAuthenticatedStatus({} as RootState)).to.equal(undefined);
  });

  it('getAuthenticatedStatus - should return the state', () => {
    expect(getAuthenticatedStatus({ user: { authenticated: true } } as RootState)).to.be.true();
  });

  it('getUserLoading - should return the state', () => {
    expect(getUserLoading({ user: { loading: true } } as RootState)).to.be.true();
  });

  it('getUserLoading - should handle missing state', () => {
    expect(getUserLoading({} as RootState)).to.equal(undefined);
  });

  it('should handle an empty state when getting the user country code', () => {
    expect(getUserCountryCode({} as RootState)).to.equal(undefined);
  });

  it('should handle a missing country code when getting the user country code', () => {
    expect(getUserCountryCode({ user: {} } as RootState)).to.equal(undefined);
  });

  it('should get a country code', () => {
    expect(getUserCountryCode({ user: { countryCode: 'US' } } as RootState)).to.equal('US');
  });
});
