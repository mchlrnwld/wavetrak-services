import { shallowEqual, useSelector } from 'react-redux';
import type { RootState } from '../store';
import { SubscriptionState } from '../reducers/subscription';

export type SubscriptionSelector<K extends keyof SubscriptionState> = (
  state: RootState,
) => SubscriptionState[K];

export const getSubscription = (state: RootState): SubscriptionState => state.subscription;
export const useSubscriptionSelector = (): SubscriptionState =>
  useSelector(getSubscription, shallowEqual);

export const getSubscriptionLoading: SubscriptionSelector<'loading'> = (state) => {
  const { subscription } = state;
  return subscription?.loading;
};

export const getSubscriptionError: SubscriptionSelector<'error'> = (state) => {
  const { subscription } = state;
  return subscription?.error;
};
