/* istanbul ignore file */
/* eslint-disable global-require */

// eslint-disable-next-line import/no-extraneous-dependencies
import { BrandAbbr } from '@surfline/types';

/* eslint-disable import/no-dynamic-require */
type CONFIG = {
  staticHost: string;
  redirectUrl: string;
  segment?: string;
  newRelic: {
    licenseKey: string;
    applicationID: string;
  };
  servicesEndpoint: string;
  splitio: string;
  clientId: string;
  clientSecret: string;
  termsConditions: string;
  privacyPolicy: string;
  stripe: {
    publishableKey: string;
  };
  facebook: {
    appId: string;
  };
  upgradeRedirectUrl: string;
  giftPurchaseHelpUrl: string;
};

const APP_ENV = process.env.APP_ENV || 'development';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const { default: config } = require(`./${APP_ENV}`) as { default: CONFIG };
const brand: BrandAbbr = 'sl';

export default {
  APP_ENV,
  company: brand,
  ...config,
};
