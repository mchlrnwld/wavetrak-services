/* istanbul ignore file */

export default {
  staticHost: 'https://product-cdn.staging.surfline.com/perks/sl/static/',
  redirectUrl: 'https://staging.surfline.com',
  segment: process.env.SEGMENT_WRITE_KEY || 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '17259048',
  },
  servicesEndpoint: 'https://services.staging.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || '3prmuinjgigdvi7m5haujp8jtq1un82rc64n',
  clientId: '5bff1583ffda1e1eb8d35c4b',
  clientSecret: 'sk_Hmxh7RJEJgJnZeaNF3Kx',
  termsConditions: '/terms_conditions.cfm',
  privacyPolicy: '/privacy.cfm',
  stripe: {
    publishableKey: 'pk_test_xBhwMLUOtBN1ORmA47xmqNGg',
  },
  facebook: {
    appId: '564041023804928',
  },
  upgradeRedirectUrl: 'https://staging.surfline.com/upgrade',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
};
