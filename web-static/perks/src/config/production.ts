/* istanbul ignore file */

export default {
  staticHost: 'https://wa.cdn-surfline.com/perks/sl/static/',
  redirectUrl: 'https://www.surfline.com',
  segment: process.env.SEGMENT_WRITE_KEY || 'Se6IGuzcvUhLx55hLHL0RiXkS6oUTz8L',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '16659845',
  },
  servicesEndpoint: 'https://services.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'in20v2ebf6q1gh97ldcn1bp8ufdbr404e514',
  clientId: '5c59e7c3f0b6cb1ad02baf66',
  clientSecret: 'sk_QqXJdn6Ny5sMRu27Amg3',
  termsConditions: '/terms_conditions.cfm',
  privacyPolicy: '/privacy.cfm',
  stripe: {
    publishableKey: 'pk_live_2iMz631tJR1t0SxF5oSLnDKG',
  },
  facebook: {
    appId: '218714858155230',
  },
  upgradeRedirectUrl: 'https://www.surfline.com/upgrade',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
};
