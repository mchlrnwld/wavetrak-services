import { MONTHLY } from '@surfline/web-common';
import type { Brand } from '@surfline/types';
import { PlanType } from '../../types/subscription';
import { Perks } from '../../types/perks';

const translations = {
  footerText:
    'The listed merchant(s) are in no way affiliated with Surfline nor are the listed merchant(s) considered sponsors or co-sponsors of this program. All trademarks are the property of their respective owner(s). Surfline makes no guarantee as to the availability of products or services from listed merchant(s). Further, Surfline does not guarantee discounts or benefits can be applied to all products or services offered by listed merchant(s).',
  unlockDeal: 'Unlock Deal',
  comingSoon: 'Coming Soon',
  getCode: 'Get Code',
  partnerUrl: (partnerUrl: string): string => `Shop on ${partnerUrl}`,
  premiumPerks: 'Premium Perks',
  perksHeaderSubText: (brand: Brand): string =>
    `${brand} Premium members score exclusive discounts on boards, gear, and travel. All Premium members can generate one code per brand every 30 days, which will remain visible on the Perks page. Each code will be saved to the member’s Surfline account and may only be used once.`,
  titleText: 'We’ve partnered with your favorite brands to save you up to 40%.',
  subtitleText: (planType: PlanType, eligibleCount: Perks['eligibleCount']): string | null => {
    if (planType === MONTHLY) {
      return `Subscribers on a yearly subscription have immediate access to Premium Perks. If you stay on your monthly plan, you can access perks after your ${eligibleCount}rd month of membership.`;
    }
    return null;
  },
  titleBtnText: (planType: PlanType): string => {
    if (planType === MONTHLY) {
      return 'SWITCH TO YEARLY';
    }
    return 'GO PREMIUM';
  },
};

export default translations;
