import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import sinon from 'sinon';
import { fetchPerkCode, fetchPerks } from './perks';
import * as promotions from '../api/promotions';
import { AppDispatch } from '../store';

const middlewares = [thunk];
const mockStore = configureStore<unknown, AppDispatch>(middlewares);

describe('Actions / Perks', () => {
  let getPerksStub: sinon.SinonStub;
  let generatePerkCodeStub: sinon.SinonStub;

  beforeEach(() => {
    getPerksStub = sinon.stub(promotions, 'getPerks');
    generatePerkCodeStub = sinon.stub(promotions, 'generatePerkCode');
  });
  afterEach(() => {
    getPerksStub.restore();
    generatePerkCodeStub.restore();
  });

  it('should dispatch fetchPerks actions', async () => {
    const initialState = {};
    const store = mockStore(initialState);

    getPerksStub.resolves({
      perks: [],
    });

    await store.dispatch(fetchPerks());

    const actions = store.getActions();
    const expectedPayload = [
      { type: fetchPerks.pending.toString() },
      { type: fetchPerks.fulfilled.toString(), payload: { perks: [] } },
    ];

    expect(actions).to.have.length(2);
    expect(actions).to.containSubset(expectedPayload);
    expect(getPerksStub).to.have.been.calledOnce();
  });

  it('should dispatch fetchPerks actions when an error occurs', async () => {
    const initialState = {};
    const store = mockStore(initialState);

    getPerksStub.rejects(new Error('message'));

    await store.dispatch(fetchPerks());

    const actions = store.getActions();
    const expectedPayload = [
      { type: fetchPerks.pending.toString() },
      { type: fetchPerks.rejected.toString(), error: { message: 'message' } },
    ];
    expect(actions).to.have.length(2);
    expect(actions).to.containSubset(expectedPayload);
    expect(getPerksStub).to.have.been.calledOnce();
  });

  it('should dispatch fetchPerkCode actions', async () => {
    const perkId = '123';
    const perkCode = '345';
    const initialState = {};
    const store = mockStore(initialState);

    generatePerkCodeStub.resolves({
      code: perkCode,
    });

    await store.dispatch(fetchPerkCode(perkId));

    const actions = store.getActions();
    const expectedPayload = [
      { type: fetchPerkCode.pending.toString() },
      { type: fetchPerkCode.fulfilled.toString(), payload: { perkCode, perkId } },
    ];

    expect(actions).to.have.length(2);
    expect(actions).to.containSubset(expectedPayload);
    expect(generatePerkCodeStub).to.have.been.calledOnce();
  });

  it('should dispatch fetchPerkCode actions when a perkId is not passed', async () => {
    const initialState = {};
    const store = mockStore(initialState);

    await store.dispatch(fetchPerkCode(''));

    const actions = store.getActions();
    const expectedPayload = [
      { type: fetchPerkCode.pending.toString() },
      {
        type: fetchPerkCode.rejected.toString(),
        error: { message: 'A valid perkId is required.' },
      },
    ];

    expect(actions).to.have.length(2);
    expect(actions).to.containSubset(expectedPayload);
    expect(generatePerkCodeStub).to.not.have.been.called();
  });

  it('should dispatch fetchPerkCode actions when an error occurs', async () => {
    const perkId = '123';
    const initialState = {};
    const store = mockStore(initialState);

    generatePerkCodeStub.rejects(new Error('message'));
    await store.dispatch(fetchPerkCode(perkId));

    const actions = store.getActions();
    const expectedPayload = [
      { type: fetchPerkCode.pending.toString() },
      {
        type: fetchPerkCode.rejected.toString(),
        payload: { message: 'message', perkId },
      },
    ];

    expect(actions).to.have.length(2);
    expect(actions).to.containSubset(expectedPayload);
    expect(generatePerkCodeStub).to.have.been.calledOnce();
  });
});
