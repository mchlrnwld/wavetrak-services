import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import common from '@surfline/web-common';
import thunk from 'redux-thunk';
import sinon from 'sinon';
import { fetchSubscription } from './subscription';
import * as cookies from '../utils/cookies';
import { AppDispatch } from '../store';

const middlewares = [thunk];
const mockStore = configureStore<unknown, AppDispatch>(middlewares);

describe('Actions / Subscription', () => {
  let getCookieStub: sinon.SinonStub;
  let getSubscriptionStub: sinon.SinonStub;

  beforeEach(() => {
    getCookieStub = sinon.stub(cookies.default, 'get');
    getSubscriptionStub = sinon.stub(common, 'getSubscription');
  });
  afterEach(() => {
    getCookieStub.restore();
    getSubscriptionStub.restore();
  });

  it('should dispatch fetchSubscription actions with an access token', async () => {
    const initialState = {};
    const store = mockStore(initialState);
    const surflineSub = { subscriptionId: '1', entitlement: 'sl_premium' };
    const buoyweatherSub = { subscriptionId: '2', entitlement: 'bw_premium' };

    getCookieStub.withArgs('access_token').returns('1234');
    getSubscriptionStub.resolves({
      subscriptions: [surflineSub, buoyweatherSub],
    });
    // Dispatch the action
    await store.dispatch(fetchSubscription());

    // Test if your store dispatched the expected actions
    const actions = store.getActions();
    const expectedPayload = [
      { type: fetchSubscription.pending.toString() },
      { type: fetchSubscription.fulfilled.toString(), payload: { subscription: surflineSub } },
    ];
    expect(actions).to.have.length(2);
    expect(actions).to.containSubset(expectedPayload);
    expect(getSubscriptionStub).to.have.been.calledOnce();
  });

  it('should dispatch fetchSubscription actions without an access token', async () => {
    const initialState = {};
    const store = mockStore(initialState);

    getCookieStub.withArgs('access_token').returns(null);
    // Dispatch the action
    await store.dispatch(fetchSubscription());

    // Test if your store dispatched the expected actions
    const actions = store.getActions();
    const expectedPayload = [
      { type: fetchSubscription.pending.toString() },
      { type: fetchSubscription.fulfilled.toString(), payload: {} },
    ];
    expect(actions).to.have.length(2);
    expect(actions).to.containSubset(expectedPayload);
    expect(getSubscriptionStub).to.not.have.been.called();
  });

  it('should dispatch fetchSubscription actions when an error occurs', async () => {
    const initialState = {};
    const store = mockStore(initialState);

    getCookieStub.withArgs('access_token').returns('1234');
    getSubscriptionStub.rejects(new Error('message'));
    // Dispatch the action
    await store.dispatch(fetchSubscription());

    // Test if your store dispatched the expected actions
    const actions = store.getActions();
    const expectedPayload = [
      { type: fetchSubscription.pending.toString() },
      { type: fetchSubscription.rejected.toString(), error: { message: 'message' } },
    ];
    expect(actions).to.have.length(2);
    expect(actions).to.containSubset(expectedPayload);
    expect(getSubscriptionStub).to.have.been.calledOnce();
  });
});
