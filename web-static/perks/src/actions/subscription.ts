import { getSubscription, SL } from '@surfline/web-common';
import { createAsyncThunk } from '@reduxjs/toolkit';
import cookies from '../utils/cookies';
import type { Subscription } from '../types/subscription';

export enum SUBSCRIPTION_ACTIONS {
  FETCH_SUBSCRIPTION = 'FETCH_SUBSCRIPTION',
}

export const fetchSubscription = createAsyncThunk(
  SUBSCRIPTION_ACTIONS.FETCH_SUBSCRIPTION,
  async () => {
    const accessToken = cookies.get<string | undefined>('access_token');
    if (!accessToken) {
      return {};
    }
    const subscriptions = await getSubscription<{ subscriptions: Subscription[] }>();
    // Return first Surfline subscription since perks are only supported for
    // surfline currently
    return {
      subscription: subscriptions.subscriptions.filter((subscription: Subscription) =>
        subscription.entitlement.startsWith(SL),
      )[0],
    };
  },
);
