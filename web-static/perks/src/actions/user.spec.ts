import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import common from '@surfline/web-common';
import thunk from 'redux-thunk';
import sinon from 'sinon';
import { fetchUser, fetchUserRegion } from './user';
import * as cookies from '../utils/cookies';
import { AppDispatch } from '../store';

const middlewares = [thunk];
const mockStore = configureStore<unknown, AppDispatch>(middlewares);

describe('Actions / User', () => {
  let getCookieStub: sinon.SinonStub;
  let fetchUserDetailsStub: sinon.SinonStub;
  let getUserRegionStub: sinon.SinonStub;

  beforeEach(() => {
    getCookieStub = sinon.stub(cookies.default, 'get');
    fetchUserDetailsStub = sinon.stub(common, 'fetchUserDetails');
    getUserRegionStub = sinon.stub(common, 'getUserRegion');
  });
  afterEach(() => {
    getCookieStub.restore();
    fetchUserDetailsStub.restore();
    getUserRegionStub.restore();
  });

  it('should dispatch fetchUser actions with an access token', async () => {
    const initialState = {};
    const store = mockStore(initialState);

    getCookieStub.withArgs('access_token').returns('1234');
    fetchUserDetailsStub.resolves({ user: {}, accessToken: '1234' });

    // Dispatch the action
    await store.dispatch(fetchUser());

    // Test if your store dispatched the expected actions
    const actions = store.getActions();
    const expectedPayload = [
      { type: fetchUser.pending.toString() },
      {
        type: fetchUser.fulfilled.toString(),
        payload: { user: {}, accessToken: '1234' },
      },
    ];
    expect(actions).to.have.length(2);
    expect(actions).to.containSubset(expectedPayload);
    expect(fetchUserDetailsStub).to.have.been.calledOnce();
  });

  it('should dispatch fetchUser actions without an access token', async () => {
    const initialState = {};
    const store = mockStore(initialState);

    getCookieStub.withArgs('access_token').returns(null);

    // Dispatch the action
    await store.dispatch(fetchUser());

    // Test if your store dispatched the expected actions
    const actions = store.getActions();
    const expectedPayload = [
      { type: fetchUser.pending.toString() },
      { type: fetchUser.fulfilled.toString(), payload: {} },
    ];
    expect(actions).to.have.length(2);
    expect(actions).to.containSubset(expectedPayload);
    expect(fetchUserDetailsStub).to.have.not.been.called();
  });

  it('should dispatch fetchUser actions when an error occurs', async () => {
    const initialState = {};
    const store = mockStore(initialState);

    getCookieStub.withArgs('access_token').returns('1234');
    fetchUserDetailsStub.rejects(new Error('message'));

    // Dispatch the action
    await store.dispatch(fetchUser());

    // Test if your store dispatched the expected actions
    const actions = store.getActions();
    const expectedPayload = [
      { type: fetchUser.pending.toString() },
      { type: fetchUser.rejected.toString(), error: { message: 'message' } },
    ];
    expect(actions).to.have.length(2);
    expect(actions).to.containSubset(expectedPayload);
    expect(fetchUserDetailsStub).to.have.been.calledOnce();
  });

  it(`should dispatch ${fetchUserRegion.fulfilled.toString()} when the getUserRegion call succeeds`, async () => {
    getUserRegionStub.resolves({ country: { iso_code: 'AU' } });
    const initialState = {};
    const store = mockStore(initialState);

    // Dispatch the action
    await store.dispatch(fetchUserRegion());

    // Test if your store dispatched the expected actions
    const actions = store.getActions();

    const expectedPayload = [
      { type: fetchUserRegion.pending.toString() },
      { type: fetchUserRegion.fulfilled.toString(), payload: { countryCode: 'AU' } },
    ];

    expect(actions).to.have.length(2);
    expect(actions).to.containSubset(expectedPayload);

    expect(getUserRegionStub).to.have.been.calledOnce();
  });

  it(`should dispatch ${fetchUserRegion.fulfilled.toString()} with US as the fallback ISO when the getUserRegion call fails`, async () => {
    getUserRegionStub.throws({ message: 'ignored' });
    const initialState = {};
    const store = mockStore(initialState);

    // Dispatch the action
    await store.dispatch(fetchUserRegion());

    // Test if your store dispatched the expected actions
    const actions = store.getActions();

    const expectedPayload = [
      { type: fetchUserRegion.pending.toString() },
      { type: fetchUserRegion.fulfilled.toString(), payload: { countryCode: 'US' } },
    ];

    expect(actions).to.have.length(2);
    expect(actions).to.containSubset(expectedPayload);

    expect(getUserRegionStub).to.have.been.calledOnce();
  });
});
