/* eslint-disable camelcase */
import { fetchUserDetails, getUserRegion } from '@surfline/web-common';
import { createAsyncThunk } from '@reduxjs/toolkit';
import cookies from '../utils/cookies';
import type { User } from '../types/user';
import config from '../config';

export enum USER_ACTIONS {
  FETCH_USER = 'FETCH_USER',
  FETCH_REGION = 'FETCH_REGION',
}

interface GeoName {
  geoname_id: string;
  name: string;
}

type GeoNameWithISO = GeoName & { iso_code: string };

interface GeoData {
  city: GeoName;
  continent: GeoName;
  country: GeoNameWithISO;
  location: {
    latitude: number;
    longitude: number;
    time_zone: string;
  };
  postal: { code: string };
  subdivisions: Array<GeoName | GeoNameWithISO>;
}

export const fetchUserRegion = createAsyncThunk(USER_ACTIONS.FETCH_REGION, async () => {
  try {
    const geo = await getUserRegion<GeoData>(config.servicesEndpoint);
    return { countryCode: geo.country.iso_code };
  } catch (_) {
    return { countryCode: 'US' };
  }
});

export const fetchUser = createAsyncThunk(USER_ACTIONS.FETCH_USER, async () => {
  const accessToken = cookies.get<string | null>('access_token');

  if (!accessToken) {
    return {};
  }

  const user = await fetchUserDetails<User>();

  return {
    user,
    accessToken,
  };
});
