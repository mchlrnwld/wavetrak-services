import { createAsyncThunk, AsyncThunk } from '@reduxjs/toolkit';
import config from '../config';
import { getPerks, generatePerkCode } from '../api/promotions';
import { Perk } from '../types/perks';

export enum PERKS_ACTION {
  FETCH_PERK = 'FETCH_PERK',
  FETCH_PERKS = 'FETCH_PERKS',
}

type PerkId = Perk['_id'];
type FetchPerkCodeReturnType = { perkCode: Perk['perkCode']; perkId: PerkId };
type FetchPerkCodeFailureType = { message: Perk['error']; perkId: PerkId };

type FetchPerkCodeThunk = AsyncThunk<
  FetchPerkCodeReturnType,
  PerkId,
  { rejectValue: FetchPerkCodeFailureType }
>;

export const fetchPerkCode: FetchPerkCodeThunk = createAsyncThunk(
  PERKS_ACTION.FETCH_PERK,
  async (perkId, thunkAPI) => {
    if (!perkId) {
      throw new Error('A valid perkId is required.');
    }
    try {
      const { code } = await generatePerkCode(perkId, config.company);
      return { perkCode: code, perkId };
    } catch (error) {
      const { message } = error as Error;
      return thunkAPI.rejectWithValue({ message, perkId });
    }
  },
);

export const fetchPerks = createAsyncThunk(PERKS_ACTION.FETCH_PERKS, async () =>
  getPerks(config.company),
);
