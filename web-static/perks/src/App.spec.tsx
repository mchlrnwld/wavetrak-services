/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import common from '@surfline/web-common';
import App from './App';
import * as userActions from './actions/user';
import wait from './utils/wait';
import { AppHead } from './components';

describe('App', () => {
  let fetchUserStub: sinon.SinonStub;
  let setupFeatureFrameworkStub: sinon.SinonStub;
  let fetchUserRegionStub: sinon.SinonStub;

  beforeEach(() => {
    fetchUserStub = sinon.stub(userActions, 'fetchUser');
    fetchUserRegionStub = sinon.stub(userActions, 'fetchUserRegion');
    setupFeatureFrameworkStub = sinon.stub(common, 'setupFeatureFramework');
  });

  afterEach(() => {
    fetchUserStub.restore();
    fetchUserRegionStub.restore();
    setupFeatureFrameworkStub.restore();
  });

  const Component = ({ appReady }: { appReady: boolean }) =>
    appReady && <div className="component" />;

  it('should mount the App component', () => {
    const wrapper = shallow(<App Component={Component} />);

    expect(wrapper.find(Component)).to.have.length(1);
  });

  it('should mount the App component', async () => {
    fetchUserStub.returns(() => ({
      type: 'test',
    }));
    fetchUserRegionStub.returns(() => ({
      type: 'fetchUserRegion',
    }));
    const wrapper = mount(<App Component={Component} />);

    expect(wrapper.find(Component).prop('appReady')).to.be.false();

    // Trigger the event loop finishing async dispatches
    await wait(0);

    wrapper.update();

    expect(wrapper.find(Component).prop('appReady')).to.be.true();
    expect(setupFeatureFrameworkStub).to.have.been.calledOnce();
  });

  it('should render the AppHead', () => {
    fetchUserStub.returns(() => ({
      type: 'test',
    }));
    fetchUserRegionStub.returns(() => ({
      type: 'fetchUserRegion',
    }));

    const wrapper = mount(<App Component={Component} />);

    expect(wrapper.find(AppHead)).to.have.length(1);
  });
});
