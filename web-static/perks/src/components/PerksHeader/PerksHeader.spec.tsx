import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import { Button } from '@surfline/quiver-react';
import { FREE, MONTHLY } from '@surfline/web-common';
import * as reactRedux from 'react-redux';
import PerksHeader from './PerksHeader';
import * as perksSelectors from '../../selectors/perks';
import * as trackUtil from '../../utils/trackPerkEvents';
import { FREE_REDIRECT_URL, MONTHLY_REDIRECT_URL } from '../../utils/constants';

describe('<PerksHeader/>', () => {
  let getPerksKeyStub: sinon.SinonStub;
  let useSelectorStub: sinon.SinonStub;
  let trackPerkEventsStub: sinon.SinonStub;

  // eslint-disable-next-line @typescript-eslint/unbound-method
  const assignStub = window.location.assign as sinon.SinonSpy;

  beforeEach(() => {
    useSelectorStub = sinon.stub(reactRedux, 'useSelector').callsFake((arg) => arg);
    getPerksKeyStub = sinon.stub(perksSelectors, 'getPerksKey');

    trackPerkEventsStub = sinon.stub(trackUtil, 'trackPerkEvents');
  });

  afterEach(() => {
    useSelectorStub.restore();
    getPerksKeyStub.restore();
    trackPerkEventsStub.restore();
    assignStub.resetHistory();
  });

  it('should render <PerksHeader/> for a free user', () => {
    const wrapper = shallow(<PerksHeader planType={FREE} />);

    getPerksKeyStub.withArgs('isCommonEligible').returns(false);

    expect(wrapper.find('.header-right__item--free')).to.have.length(1);

    const btn = wrapper.find(Button);

    // eslint-disable-next-line no-underscore-dangle
    btn.prop<() => { _onTimeout: () => null }>('onClick')()._onTimeout();

    expect(trackPerkEventsStub).to.have.been.calledOnceWithExactly(FREE, {
      perkId: null,
      perkName: null,
    });
    expect(assignStub).to.be.have.been.calledOnceWithExactly(FREE_REDIRECT_URL);
  });

  it('should render <PerksHeader/> for a monthly premium user', () => {
    const wrapper = shallow(<PerksHeader planType={MONTHLY} />);

    getPerksKeyStub.withArgs('isCommonEligible').returns(false);

    expect(wrapper.find('.header-right__item--free')).to.have.length(0);

    const btn = wrapper.find(Button);

    // eslint-disable-next-line no-underscore-dangle
    btn.prop<() => { _onTimeout: () => null }>('onClick')()._onTimeout();

    expect(trackPerkEventsStub).to.have.been.calledOnceWithExactly(MONTHLY, {
      perkId: null,
      perkName: null,
    });
    expect(assignStub).to.be.have.been.calledOnceWithExactly(MONTHLY_REDIRECT_URL);
  });
});
