import React from 'react';
import classNames from 'classnames';
import { Button } from '@surfline/quiver-react';
import { SURFLINE, BUOYWEATHER, FISHTRACK, MONTHLY, FREE } from '@surfline/web-common';
import { useSelector } from 'react-redux';
import config from '../../config';
import en from '../../international/translations/en';
import { trackPerkEvents } from '../../utils/trackPerkEvents';
import { FREE_REDIRECT_URL, MONTHLY_REDIRECT_URL } from '../../utils/constants';
import './PerksHeader.scss';
import { getPerksKey } from '../../selectors/perks';
import { PlanType } from '../../types/subscription';

const brandName = { sl: SURFLINE, bw: BUOYWEATHER, fs: FISHTRACK };

const getCTAClass = (planType: PlanType) =>
  classNames({
    'header-right__item': true,
    'header-right__item--free': planType === FREE,
  });

const getTitleText = (planType: PlanType, eligibleCount?: number) => ({
  title: en.titleText,
  subtitle: en.subtitleText(planType, eligibleCount),
  buttonText: en.titleBtnText(planType),
});

const handleButtonClick = (planType: PlanType) => {
  trackPerkEvents(planType, { perkId: null, perkName: null });

  const redirect = () => {
    window.location.assign(planType === MONTHLY ? MONTHLY_REDIRECT_URL : FREE_REDIRECT_URL);
  };
  return setTimeout(redirect, 300);
};

const PerksHeader = React.memo(
  ({ planType }: { planType: PlanType }): JSX.Element => {
    const isCommonEligible = useSelector(getPerksKey('isCommonEligible'));
    const eligibleCount = useSelector(getPerksKey('eligibleCount'));
    return (
      <div className="perks-header-container">
        <div className="perks-header">
          <div className="header-left__item">
            <h1 className="perks-header__text">{en.premiumPerks}</h1>
            <h2 className="perks-header__sub-text">
              {en.perksHeaderSubText(brandName[config.company])}
            </h2>
          </div>
          {!isCommonEligible ? (
            <div className={getCTAClass(planType)}>
              <div className="header-right__text">
                <div className="header-right__title">
                  {getTitleText(planType, eligibleCount).title}
                </div>
                <div className="header-right__sub-text">
                  {getTitleText(planType, eligibleCount).subtitle}
                </div>
              </div>
              <div className="header-right__button">
                <Button onClick={() => handleButtonClick(planType)}>
                  {getTitleText(planType).buttonText}
                </Button>
              </div>
            </div>
          ) : (
            <div className="header-right__item--premium" />
          )}
        </div>
      </div>
    );
  },
);

export default PerksHeader;
