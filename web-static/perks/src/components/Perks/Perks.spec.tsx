import React from 'react';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import { SurflineLogo } from '@surfline/quiver-react';
import common, { FREE } from '@surfline/web-common';
import { Provider } from 'react-redux';
import Perks from './Perks';
import * as subscriptionSelectors from '../../selectors/subscription';
import { PerksList, PerksHeader } from '..';
import store from '../../store';

describe('<Perks/>', () => {
  let useSubscriptionSelectorStub: sinon.SinonStub;
  let trackNavigatedToPageStub: sinon.SinonStub;
  let getPlanTypeStub: sinon.SinonStub;

  beforeEach(() => {
    trackNavigatedToPageStub = sinon.stub(common, 'trackNavigatedToPage');
    useSubscriptionSelectorStub = sinon.stub(subscriptionSelectors, 'useSubscriptionSelector');
    getPlanTypeStub = sinon.stub(common, 'getPlanType');
  });

  afterEach(() => {
    trackNavigatedToPageStub.restore();
    useSubscriptionSelectorStub.restore();
    getPlanTypeStub.restore();
  });

  it('should render <Perks/> for a free user', () => {
    const wrapper = shallow(<Perks />);
    const header = wrapper.find('header');
    expect(header).to.have.length(1);
    expect(header.prop('style')).to.deep.equal({ backgroundImage: 'url([object Object])' });

    const perksHeader = wrapper.find(PerksHeader);
    expect(perksHeader).to.have.length(1);
    expect(perksHeader.props()).to.deep.equal({ planType: FREE });

    const perksList = wrapper.find(PerksList);
    expect(perksList).to.have.length(1);
    expect(perksList.props()).to.deep.equal({ planType: FREE, subscription: undefined });

    expect(wrapper.find(SurflineLogo)).to.have.length(1);
  });

  it('should render <Perks/> for a stripe user', () => {
    const subscription = { planId: 'price_1KGWbpCUgh9bySt68b', plan: { interval: 'year' } };
    useSubscriptionSelectorStub.returns(subscription);

    const wrapper = shallow(<Perks />);

    const perksHeader = wrapper.find(PerksHeader);
    expect(perksHeader).to.have.length(1);
    expect(perksHeader.props()).to.deep.equal({ planType: 'annual' });

    const perksList = wrapper.find(PerksList);
    expect(perksList).to.have.length(1);
    expect(perksList.props()).to.deep.equal({ planType: 'annual', subscription });
  });

  it('should render <Perks/> for a premium user', () => {
    const subscription = { planId: 'surfline.1year.ios.subscription.notrial' };
    useSubscriptionSelectorStub.returns(subscription);
    getPlanTypeStub.returns('annual');

    const wrapper = shallow(<Perks />);

    const perksHeader = wrapper.find(PerksHeader);
    expect(perksHeader).to.have.length(1);
    expect(perksHeader.props()).to.deep.equal({ planType: 'annual' });

    const perksList = wrapper.find(PerksList);
    expect(perksList).to.have.length(1);
    expect(perksList.props()).to.deep.equal({ planType: 'annual', subscription });
  });

  it('should render <Perks/> and perform a page call', () => {
    const subscription = {};
    useSubscriptionSelectorStub.returns(subscription);
    // Shallow does not call useEffect
    mount(
      <Provider store={store}>
        <Perks />
      </Provider>,
    );
    expect(trackNavigatedToPageStub).to.have.been.calledOnceWithExactly('Premium Perks');
  });

  it('should render <AUSBanner/> for australian users', () => {
    const wrapper = shallow(<Perks countryCode="AU" />);

    const ausBanner = wrapper.find('AUSBanner');
    expect(ausBanner).to.have.length(1);
  });

  it('should not render <AUSBanner/> for non-australian users', () => {
    const wrapper = shallow(<Perks countryCode="US" />);

    const ausBanner = wrapper.find('AUSBanner');
    expect(ausBanner).to.have.length(0);
  });
});
