import React, { memo, useMemo } from 'react';
import {
  trackNavigatedToPage,
  FREE,
  MONTH,
  MONTHLY,
  ANNUAL,
  GIFT,
  YEAR,
  UserState,
} from '@surfline/web-common';
import { SurflineLogo } from '@surfline/quiver-react';

import { useMount } from 'react-use';
import PerksHeader from '../PerksHeader';
import PerksList from '../PerksList';
import en from '../../international/translations/en';
import './Perks.scss';
import { useSubscriptionSelector } from '../../selectors/subscription';
import swellLines from '../../assets/swelllines.svg';
import AUSBanner from '../AUSBanner';

const AUSTRALIAN_ISO_CODE = 'AU';
const Perks = ({ countryCode }: { countryCode?: UserState['countryCode'] }) => {
  useMount(() => {
    trackNavigatedToPage('Premium Perks');
  });

  const subscription = useSubscriptionSelector();

  const planType = useMemo(() => {
    const { plan, planId } = subscription || {};
    if (plan || planId) {
      const identifier = plan?.interval || planId;
      if (identifier.includes(MONTH)) return MONTHLY;
      if (identifier.includes(YEAR)) return ANNUAL;
      if (identifier.includes(GIFT)) return GIFT;
      return FREE;
    }
    return FREE;
  }, [subscription]);

  return (
    <div className="perks-container">
      <header
        style={{
          backgroundImage: `url(${swellLines})`,
        }}
      >
        {countryCode === AUSTRALIAN_ISO_CODE && <AUSBanner />}
        <PerksHeader planType={planType} />
      </header>
      <div className="perks-common-container">
        <PerksList planType={planType} subscription={subscription} />
      </div>
      <div className="perks-container__footer">
        <div className="perks-container__footer--logo">
          <SurflineLogo />
        </div>
        <div className="perks-footer">{en.footerText}</div>
      </div>
    </div>
  );
};

Perks.defaultProps = {
  countryCode: 'US',
};

export default memo(Perks);
