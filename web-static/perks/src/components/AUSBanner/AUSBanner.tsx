import React from 'react';
import PeaceSign from '../Icons/PeaceSign';
import './AUSBanner.scss';

const AUSBanner = (): JSX.Element => {
  return (
    <section className="sl-perks-aus-banner">
      <div className="sl-perks-aus-banner__gradient-background" />
      <PeaceSign />
      <h2 className="sl-perks-aus-banner__header">More Australian Perks Offers Coming 2021</h2>
    </section>
  );
};

export default AUSBanner;
