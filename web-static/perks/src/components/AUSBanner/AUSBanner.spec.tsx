import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import AUSBanner from './AUSBanner';

describe('<AUSBanner/>', () => {
  it('should render <AUSBanner/>', () => {
    const wrapper = shallow(<AUSBanner />);
    const banner = wrapper.find('.sl-perks-aus-banner');
    expect(banner).to.have.length(1);
    const gradient = wrapper.find('.sl-perks-aus-banner__gradient-background');
    expect(gradient).to.have.length(1);
    const peaceSign = wrapper.find('PeaceSign');
    expect(peaceSign).to.have.length(1);
    const header = wrapper.find('.sl-perks-aus-banner__header');
    expect(header).to.have.length(1);
  });
});
