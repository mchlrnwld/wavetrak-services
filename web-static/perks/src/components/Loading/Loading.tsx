import React from 'react';
import { Spinner } from '@surfline/quiver-react';
import './Loading.scss';

const Loading = (): JSX.Element => (
  <div className="sl-perks-loading">
    <Spinner />
  </div>
);

export default Loading;
