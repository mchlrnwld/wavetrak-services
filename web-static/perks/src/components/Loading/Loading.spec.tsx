import React from 'react';
import { shallow } from 'enzyme';
import { Spinner } from '@surfline/quiver-react';
import { expect } from 'chai';
import Loading from './Loading';

describe('<Loading/>', () => {
  it('should render <Loading/>', () => {
    const wrapper = shallow(<Loading />);
    expect(wrapper.find(Spinner)).to.have.length(1);
    expect(wrapper.find('.sl-perks-loading')).to.have.length(1);
  });
});
