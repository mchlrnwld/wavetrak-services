import React from 'react';
import { color } from '@surfline/quiver-themes';

interface Props {
  width?: number;
  height?: number;
}
const Lock = ({ width, height }: Props): JSX.Element => (
  <svg
    width={width}
    height={height}
    viewBox={`0 0 ${width} ${height}`}
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    className="sl-lock-icon"
  >
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-24.000000, -233.000000)" fillRule="nonzero">
        <g transform="translate(24.000000, 233.000000)">
          <rect fill={color('blue-gray', 70)} x="0" y="6" width="14" height="11" rx="1" />
          <rect fill={color('blue-gray', 10)} x="6" y="9" width="2" height="5" />
          <path
            d="M6,0 L8,0 C10.209139,-4.05812251e-16 12,1.790861 12,4 L12,9 L12,9 L9.5,9 L9.5,4 C9.5,3.44771525 9.05228475,3 8.5,3 L5.5,3 C4.94771525,3 4.5,3.44771525 4.5,4 L4.5,9 L4.5,9 L2,9 L2,4 C2,1.790861 3.790861,4.05812251e-16 6,0 Z"
            fill={color('blue-gray', 70)}
          />
        </g>
      </g>
    </g>
  </svg>
);

Lock.defaultProps = {
  width: 14,
  height: 17,
};
export default Lock;
