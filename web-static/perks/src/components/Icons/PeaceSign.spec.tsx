import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import PeaceSign from './PeaceSign';

describe('<PeaceSign/>', () => {
  it('should render <PeaceSign/>', () => {
    const wrapper = shallow(<PeaceSign />);
    const icon = wrapper.find('.sl-perks__peace-sign');
    expect(icon).to.have.length(1);
    expect(icon.prop('width')).to.equal('86');
    expect(icon.prop('height')).to.equal('77');
    expect(icon.prop('viewBox')).to.equal('0 0 86 77');
  });
});
