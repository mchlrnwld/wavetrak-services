import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import Lock from './Lock';

describe('<Lock/>', () => {
  it('should render <Lock/>', () => {
    const wrapper = shallow(<Lock />);
    const icon = wrapper.find('.sl-lock-icon');
    expect(icon).to.have.length(1);
    expect(icon.prop('width')).to.equal(14);
    expect(icon.prop('height')).to.equal(17);
    expect(icon.prop('viewBox')).to.equal('0 0 14 17');
  });
  it('should render <Lock/> - custom size', () => {
    const wrapper = shallow(<Lock width={20} height={20} />);
    const icon = wrapper.find('.sl-lock-icon');
    expect(icon).to.have.length(1);
    expect(icon.prop('width')).to.equal(20);
    expect(icon.prop('height')).to.equal(20);
    expect(icon.prop('viewBox')).to.equal('0 0 20 20');
  });
});
