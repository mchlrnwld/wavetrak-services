import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import CopyToClipboard from 'react-copy-to-clipboard';
import { FREE, MONTHLY, ANNUAL } from '@surfline/web-common';
import * as reactRedux from 'react-redux';
import { Button, ClipboardIcon, TrackableLink, NewTabIcon, Alert } from '@surfline/quiver-react';

import * as perksSelectors from '../../selectors/perks';
import * as userSelectors from '../../selectors/user';
import * as subscriptionSelectors from '../../selectors/subscription';
import * as perksActions from '../../actions/perks';
import * as trackUtils from '../../utils/trackPerkEvents';
import Perk from './Perk';
import type { Perk as TPerk } from '../../types/perks';
import { Lock } from '../Icons';
import en from '../../international/translations/en';
import { FREE_REDIRECT_URL, MONTHLY_REDIRECT_URL } from '../../utils/constants';

describe('<Perk/>', () => {
  let getPerkListStub: sinon.SinonStub;
  let getPerksErrorStub: sinon.SinonStub;
  let getSubscriptionErrorStub: sinon.SinonStub;
  let getUserErrorStub: sinon.SinonStub;
  let useDispatchStub: sinon.SinonStub;
  let fetchPerkCodeStub: sinon.SinonStub;
  let trackPerkEventsStub: sinon.SinonStub;
  const dispatchStub = sinon.stub();
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const assignStub = window.location.assign as sinon.SinonSpy;

  beforeEach(() => {
    useDispatchStub = sinon.stub(reactRedux, 'useDispatch').returns(dispatchStub);
    getPerkListStub = sinon.stub(perksSelectors, 'getPerkList');
    getPerksErrorStub = sinon.stub(perksSelectors, 'getPerksError');
    getSubscriptionErrorStub = sinon.stub(subscriptionSelectors, 'getSubscriptionError');
    getUserErrorStub = sinon.stub(userSelectors, 'getUserError');
    fetchPerkCodeStub = sinon.stub(perksActions, 'fetchPerkCode');
    trackPerkEventsStub = sinon.stub(trackUtils, 'trackPerkEvents');
  });

  afterEach(() => {
    useDispatchStub.restore();
    getPerkListStub.restore();
    getPerksErrorStub.restore();
    getSubscriptionErrorStub.restore();
    getUserErrorStub.restore();
    fetchPerkCodeStub.restore();
    trackPerkEventsStub.restore();
    assignStub.resetHistory();
  });

  const perk = {
    eligible: false,
    isCodeAvailable: false,
    _id: '123',
    lastRequestedCode: null,
    partnerUrl: 'partner',
    bgDesktopImgPath: 'background',
    logoImgPath: 'logo',
    discountText: 'discount',
    name: 'perkname',
    description: 'description',
    freeDescription: 'free description',
    perkCode: null,
  } as TPerk;
  const subscription = { onTrial: false, error: null, loading: false };

  it('should render <Perk/> - background image', () => {
    const wrapper = shallow(<Perk perk={perk} planType={FREE} subscription={subscription} />);

    const background = wrapper.find('.perk-container__image');
    expect(background).to.have.length(1);
    expect(background.prop('style')).to.deep.equal({
      backgroundImage: 'url(background)',
    });

    const wrapper2 = shallow(
      <Perk
        perk={{ ...perk, bgDesktopImgPath: null }}
        planType={FREE}
        subscription={subscription}
      />,
    );

    const background2 = wrapper2.find('.perk-container__image');
    expect(background2).to.have.length(1);
    expect(background2.prop('style')).to.equal(null);
  });

  it('should render <Perk/> - logo image', () => {
    const wrapper = shallow(<Perk perk={perk} planType={FREE} subscription={subscription} />);

    const background = wrapper.find('.perk-logo');
    expect(background).to.have.length(1);
    expect(background.prop('style')).to.deep.equal({
      backgroundImage: 'url(logo)',
    });

    const wrapper2 = shallow(
      <Perk perk={{ ...perk, logoImgPath: null }} planType={FREE} subscription={subscription} />,
    );

    const background2 = wrapper2.find('.perk-logo');
    expect(background2).to.have.length(1);
    expect(background2.prop('style')).to.equal(null);
  });

  it('should render <Perk/>', () => {
    const wrapper = shallow(<Perk perk={perk} planType={FREE} subscription={subscription} />);

    expect(wrapper.find('.perk-name').text()).to.contain(perk.name);
    expect(wrapper.find('.perk-discounttext').text()).to.contain(perk.discountText);
  });

  it('should render <Perk/> - on trial', () => {
    const wrapper = shallow(
      <Perk perk={perk} planType={FREE} subscription={{ ...subscription, onTrial: true }} />,
    );

    const button = wrapper.find(Button);
    expect(button).to.have.length(1);
    expect(button.prop('button')).to.deep.equal({ disabled: true });
  });
  it('should render <Perk/> - not eligible', () => {
    const wrapper = shallow(<Perk perk={perk} planType={FREE} subscription={subscription} />);

    expect(wrapper.find(Lock)).to.have.length(1);
    expect(wrapper.find('.perk-description').text()).to.be.equal(perk.freeDescription);
  });

  it('should render <Perk/> - not eligible / free', () => {
    const wrapper = shallow(<Perk perk={perk} planType={FREE} subscription={subscription} />);

    expect(wrapper.find('.perk-button--free')).to.have.length(1);
    expect(wrapper.find('.perk-button--code-unavailable')).to.have.length(0);
    const button = wrapper.find(Button);
    expect(button).to.have.length(1);
    expect(button.dive().text()).to.be.equal(en.unlockDeal);
    expect(button.prop('button')).to.deep.equal({ disabled: false });

    // eslint-disable-next-line no-underscore-dangle
    button.prop<() => { _onTimeout: () => null }>('onClick')()._onTimeout();
    expect(trackPerkEventsStub).to.have.been.calledOnceWithExactly(FREE, {
      perkId: perk._id,
      perkName: perk.name,
    });
    expect(assignStub).to.have.been.calledOnceWithExactly(FREE_REDIRECT_URL);
    expect(fetchPerkCodeStub).to.not.have.been.called();
    expect(dispatchStub).to.not.have.been.called();
  });

  it('should render <Perk/> - not eligible / monthly', () => {
    const planType = MONTHLY;
    const wrapper = shallow(<Perk perk={perk} planType={planType} subscription={subscription} />);

    expect(wrapper.find('.perk-button--monthly')).to.have.length(1);
    expect(wrapper.find('.perk-button--code-unavailable')).to.have.length(1);
    const button = wrapper.find(Button);
    expect(button).to.have.length(1);
    expect(button.dive().text()).to.be.equal(en.getCode);
    expect(button.prop('button')).to.deep.equal({ disabled: true });
    // eslint-disable-next-line no-underscore-dangle
    button.prop<() => { _onTimeout: () => null }>('onClick')()._onTimeout();
    expect(trackPerkEventsStub).to.have.been.calledOnceWithExactly(planType, {
      perkId: perk._id,
      perkName: perk.name,
    });
    expect(assignStub).to.have.been.calledOnceWithExactly(MONTHLY_REDIRECT_URL);
    expect(fetchPerkCodeStub).to.not.have.been.called();
    expect(dispatchStub).to.not.have.been.called();
  });

  it('should render <Perk/> - not eligible / annual', () => {
    const planType = ANNUAL;
    const wrapper = shallow(<Perk perk={perk} planType={planType} subscription={subscription} />);

    expect(wrapper.find('.perk-button--free')).to.have.length(0);
    expect(wrapper.find('.perk-button--monthly')).to.have.length(0);
    expect(wrapper.find('.perk-button--code-unavailable')).to.have.length(1);
    const button = wrapper.find(Button);
    expect(button).to.have.length(1);
    expect(button.dive().text()).to.contain(en.getCode);
    expect(button.prop('button')).to.deep.equal({ disabled: true });
  });

  it('should render <Perk/> - eligible / code not available', () => {
    const planType = ANNUAL;
    const wrapper = shallow(
      <Perk
        perk={{ ...perk, eligible: true, isCodeAvailable: false }}
        planType={planType}
        subscription={subscription}
      />,
    );

    expect(wrapper.find('.perk-button--code-unavailable')).to.have.length(1);
    const button = wrapper.find(Button);
    expect(button).to.have.length(1);
    expect(button.dive().text()).to.be.equal(en.comingSoon);
    expect(button.prop('button')).to.deep.equal({ disabled: true });
  });

  it('should render <Perk/> - eligible / code available', () => {
    dispatchStub.resolves();
    const action = { type: 'fetchPerkCodeStub' };
    const planType = ANNUAL;
    fetchPerkCodeStub.returns(action);
    const wrapper = shallow(
      <Perk
        perk={{ ...perk, eligible: true, isCodeAvailable: true }}
        planType={planType}
        subscription={subscription}
      />,
    );

    expect(wrapper.find(Lock)).to.have.length(0);
    expect(wrapper.find('.perk-description').text()).to.be.equal(perk.description);

    expect(wrapper.find('.perk-button--premium')).to.have.length(1);
    const button = wrapper.find(Button);
    expect(button).to.have.length(1);
    expect(button.dive().text()).to.be.equal(en.getCode);
    expect(button.prop('button')).to.deep.equal({ disabled: false });

    button.prop<() => null>('onClick')();

    expect(trackPerkEventsStub).to.not.have.been.called();
    expect(assignStub).to.not.have.been.called();
    expect(fetchPerkCodeStub).to.have.been.calledOnceWithExactly(perk._id);
    expect(dispatchStub).to.have.been.calledOnceWithExactly(action);
  });

  it('should render <Perk/> - code exists', () => {
    const planType = ANNUAL;
    const code = 'PERK_CODE';
    const wrapper = shallow(
      <Perk perk={{ ...perk, perkCode: code }} planType={planType} subscription={subscription} />,
    );

    const codeContainer = wrapper.find('.sl-perk-code-container');
    const copyButton = wrapper.find(CopyToClipboard);
    const copyIcon = wrapper.find(ClipboardIcon);
    const linkContainer = wrapper.find('.perkcode-link-container');
    const trackableLink = wrapper.find(TrackableLink);
    const newTab = wrapper.find(NewTabIcon);

    expect(codeContainer).to.have.length(1);
    expect(codeContainer.text()).to.contain(code);

    expect(copyButton).to.have.length(1);
    expect(copyButton.prop('text')).to.be.equal(code);

    expect(copyIcon).to.have.length(1);

    expect(linkContainer).to.have.length(1);

    expect(trackableLink).to.have.length(1);
    expect(trackableLink.prop('eventName')).to.be.equal('Clicked Link');
    expect(trackableLink.prop('eventProperties')).to.deep.equal({
      category: 'premium perks',
      linkUrl: perk.partnerUrl,
      pageName: window.location.href,
      perk: perk.name,
    });

    expect(newTab).to.have.length(1);
  });

  it('should render <Perk/> - error', () => {
    const planType = ANNUAL;
    const wrapper = shallow(
      <Perk
        perk={{ ...perk, error: 'perk error' }}
        planType={planType}
        subscription={subscription}
      />,
    );

    const alert = wrapper.find(Alert);
    expect(alert).to.have.length(1);
    expect(alert.dive().text()).to.be.equal('Something went wrong.');
  });
});
