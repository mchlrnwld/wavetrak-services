import React, { useCallback, useMemo, useRef, useState, useEffect } from 'react';
import { Button, Alert, TrackableLink, NewTabIcon, ClipboardIcon } from '@surfline/quiver-react';
import CopyToClipboard from 'react-copy-to-clipboard';
import { MONTHLY, FREE } from '@surfline/web-common';
import classNames from 'classnames';
import { Lock } from '../Icons';
import en from '../../international/translations/en';
import { trackPerkEvents } from '../../utils/trackPerkEvents';
import { FREE_REDIRECT_URL, MONTHLY_REDIRECT_URL } from '../../utils/constants';
import './Perk.scss';
import type { PlanType } from '../../types/subscription';
import type { Perk as TPerk } from '../../types/perks';
import { fetchPerkCode } from '../../actions/perks';
import { SubscriptionState } from '../../reducers/subscription';
import { useAppDispatch } from '../../store';

const getButtonClass = (eligible: boolean, planType: PlanType, isCodeAvailable: boolean) =>
  classNames({
    'perk-button': true,
    'perk-button--premium': eligible && isCodeAvailable,
    'perk-button--free': !eligible && planType === FREE,
    'perk-button--monthly': !eligible && planType === MONTHLY,
    'perk-button--code-unavailable':
      (eligible && !isCodeAvailable) || (!eligible && planType !== FREE),
  });

const getCodeClass = (copied: boolean) =>
  classNames({
    'sl-perk-code': true,
    'sl-perk-code--copied': copied,
  });

const getButtonText = (planType: PlanType, eligible: boolean, isCodeAvailable: boolean) => {
  if (!eligible && planType === FREE) return en.unlockDeal;
  if (eligible && !isCodeAvailable) return en.comingSoon;
  return en.getCode;
};

interface Props {
  perk: TPerk;
  planType: PlanType;
  subscription: SubscriptionState;
}

const Perk = ({ perk, planType, subscription }: Props): JSX.Element => {
  const dispatch = useAppDispatch();
  const doFetchPerkCode = useCallback((perkId: string) => dispatch(fetchPerkCode(perkId)), [
    dispatch,
  ]);
  const linkRef = useRef<HTMLAnchorElement>();

  const [copied, setCopied] = useState(false);
  useEffect(() => {
    let timeout: NodeJS.Timeout;
    if (copied) {
      timeout = setTimeout(() => setCopied(false), 500);
    }
    return () => clearTimeout(timeout);
  }, [copied]);

  const [submitting, setSubmitting] = useState(false);
  const handleButtonClick = useCallback(
    (perkId: string = null, eligible: boolean) => {
      if (!eligible) {
        trackPerkEvents(planType, { perkId, perkName: perk.name });
      }

      if (!eligible && planType === FREE) {
        return setTimeout(() => window.location.assign(FREE_REDIRECT_URL), 300);
      }

      if (!eligible && planType === MONTHLY) {
        return setTimeout(() => window.location.assign(MONTHLY_REDIRECT_URL), 300);
      }

      if (!eligible) {
        return null;
      }

      setSubmitting(true);
      return doFetchPerkCode(perkId).then(() => setSubmitting(false));
    },
    [doFetchPerkCode, perk.name, planType],
  );

  const {
    eligible,
    isCodeAvailable,
    _id: perkId,
    lastRequestedCode,
    partnerUrl,
    bgDesktopImgPath,
    logoImgPath,
    discountText,
    name,
    description,
    freeDescription,
    perkCode,
    error,
  } = perk;

  const onTrial = subscription?.onTrial;
  const codeToShow = perkCode || lastRequestedCode;
  const isButtonDisabled =
    (eligible && !isCodeAvailable) || (!eligible && planType !== FREE) || onTrial;

  const eventProperties = useMemo(
    () => ({
      category: 'premium perks',
      linkUrl: perk.partnerUrl,
      pageName: window.location.href,
      perk: perk.name,
    }),
    [perk.name, perk.partnerUrl],
  );

  return (
    <div className="perk-container">
      <div
        className="perk-container__image"
        style={bgDesktopImgPath ? { backgroundImage: `url(${bgDesktopImgPath})` } : null}
      >
        <div
          className="perk-logo"
          style={logoImgPath ? { backgroundImage: `url(${logoImgPath})` } : null}
        />
        {discountText ? (
          <div>
            <div className="perk-discounttext">{discountText}</div>
          </div>
        ) : null}
      </div>
      <div className="perk-container__details">
        <div className="perk-name">
          {!eligible && !codeToShow ? <Lock /> : null}
          {name}
        </div>
        <div className="perk-description">{eligible ? description : freeDescription}</div>
        <div className="perkcode-container">
          {codeToShow ? (
            <div className={getCodeClass(copied)}>
              <div className="sl-perk-code-container">
                {codeToShow}{' '}
                <CopyToClipboard text={codeToShow} onCopy={() => setCopied(true)}>
                  <div className="sl-perk-code-copy-button">
                    <ClipboardIcon />
                  </div>
                </CopyToClipboard>
              </div>
            </div>
          ) : (
            <div className={getButtonClass(eligible, planType, isCodeAvailable)}>
              <Button
                loading={submitting}
                button={{ disabled: isButtonDisabled }}
                onClick={() => handleButtonClick(perkId, eligible)}
              >
                {getButtonText(planType, eligible, isCodeAvailable)}
              </Button>
            </div>
          )}
          {codeToShow && partnerUrl && (
            <div className="perkcode-link-container">
              <TrackableLink
                eventName="Clicked Link"
                eventProperties={eventProperties}
                ref={linkRef}
              >
                <a
                  className="perkcode-link"
                  target="_blank"
                  rel="noopener noreferrer"
                  ref={linkRef}
                  href={partnerUrl}
                >
                  <div className="perkcode-partnerurl">
                    <span>{en.partnerUrl(partnerUrl)}</span>
                    <div>
                      <NewTabIcon />
                    </div>
                  </div>
                </a>
              </TrackableLink>
            </div>
          )}
          {error ? (
            <Alert type="error" style={{ marginBottom: '5px', textAlign: 'center' }}>
              Something went wrong.
            </Alert>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default Perk;
