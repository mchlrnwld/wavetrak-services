import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import Head from 'next/head';

import AppHead from './AppHead';

describe('<AppHead/>', () => {
  it('should render <AppHead/>', () => {
    const wrapper = shallow(<AppHead />);
    expect(wrapper.find(Head)).to.have.length(1);
    expect(wrapper.find('title').text()).to.equal('Premium Perks - Surfline');
  });
});
