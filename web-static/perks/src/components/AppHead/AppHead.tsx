import React from 'react';
import Head from 'next/head';

const AppHead = (): JSX.Element => {
  return (
    <Head>
      <title>Premium Perks - Surfline</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
  );
};

export default AppHead;
