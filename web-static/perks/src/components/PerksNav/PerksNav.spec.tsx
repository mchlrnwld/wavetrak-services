import React from 'react';
import { shallow } from 'enzyme';
import { SurflineLogo } from '@surfline/quiver-react';
import { expect } from 'chai';
import PerksNav from './PerksNav';

describe('<PerksNav/>', () => {
  it('should render <PerksNav/> - authenticated', () => {
    const wrapper = shallow(<PerksNav authenticated />);
    expect(wrapper.find(SurflineLogo)).to.have.length(1);
    expect(wrapper.find('.sl-perks-account-link')).to.have.length(1);
  });

  it('should render <PerksNav/> - unauthenticated', () => {
    const wrapper = shallow(<PerksNav />);
    expect(wrapper.find(SurflineLogo)).to.have.length(1);
    expect(wrapper.find('.sl-perks-account-link')).to.have.length(0);
  });
});
