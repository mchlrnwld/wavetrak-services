import React from 'react';
import { SurflineLogo } from '@surfline/quiver-react';
import './PerksNav.scss';

interface Props {
  authenticated?: boolean;
}

const PerksNav = ({ authenticated }: Props): JSX.Element => {
  return (
    <div className="sl-perks-nav">
      <div className="sl-perks-nav__logo">
        <a className="sl-perks-nav__logo__link" href="/">
          <SurflineLogo />
        </a>
      </div>
      <div className="sl-perks-nav__account">
        {authenticated && (
          <a className="sl-perks-account-link" href="/account">
            MY ACCOUNT
          </a>
        )}
      </div>
    </div>
  );
};

PerksNav.defaultProps = {
  authenticated: false,
};

export default PerksNav;
