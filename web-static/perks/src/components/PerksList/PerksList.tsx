import React from 'react';
import { useSelector } from 'react-redux';
import classNames from 'classnames';
import { ErrorBox } from '@surfline/quiver-react';
import Perk from '../Perk';
import { getPerksError, getPerkList } from '../../selectors/perks';
import { getUserError } from '../../selectors/user';
import './PerksList.scss';
import type { PlanType } from '../../types/subscription';
import type { SubscriptionState } from '../../reducers/subscription';

const getClass = (error: string) =>
  classNames({
    'perks-list': true,
    'perks-list--error': !!error,
  });

const PerksList = ({
  planType,
  subscription,
}: {
  planType: PlanType;
  subscription: SubscriptionState;
}): JSX.Element => {
  const perksError = useSelector(getPerksError);
  const userError = useSelector(getUserError);
  const perks = useSelector(getPerkList);

  const error =
    (perksError && 'Perks') || (userError && 'User') || (subscription.error && 'Subscription');

  return (
    <div className={getClass(error)}>
      {!error ? (
        perks.map((perk) => (
          <Perk key={perk._id} perk={perk} planType={planType} subscription={subscription} />
        ))
      ) : (
        <ErrorBox type={error} />
      )}
    </div>
  );
};

export default PerksList;
