import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';
import { ErrorBox } from '@surfline/quiver-react';
import { FREE } from '@surfline/web-common';
import * as reactRedux from 'react-redux';
import * as perksSelectors from '../../selectors/perks';
import * as userSelectors from '../../selectors/user';
import * as subscriptionSelectors from '../../selectors/subscription';
import PerksList from './PerksList';
import Perk from '../Perk/Perk';

describe('<PerksList/>', () => {
  let getPerkListStub: sinon.SinonStub;
  let getPerksErrorStub: sinon.SinonStub;
  let getSubscriptionErrorStub: sinon.SinonStub;
  let getUserErrorStub: sinon.SinonStub;
  let useSelectorStub: sinon.SinonStub;

  beforeEach(() => {
    useSelectorStub = sinon.stub(reactRedux, 'useSelector').callsFake((arg) => arg({}));
    getPerkListStub = sinon.stub(perksSelectors, 'getPerkList');
    getPerksErrorStub = sinon.stub(perksSelectors, 'getPerksError');
    getSubscriptionErrorStub = sinon.stub(subscriptionSelectors, 'getSubscriptionError');
    getUserErrorStub = sinon.stub(userSelectors, 'getUserError');
  });

  afterEach(() => {
    useSelectorStub.restore();
    getPerkListStub.restore();
    getPerksErrorStub.restore();
    getSubscriptionErrorStub.restore();
    getUserErrorStub.restore();
  });

  it('should render <PerksList/> - free user', () => {
    const perk = { _id: '1' };
    const perk2 = { _id: '2' };
    const subscription = { id: '1', error: null, loading: false };

    getPerksErrorStub.returns(null);
    getUserErrorStub.returns(null);
    getPerkListStub.returns([perk, perk2]);

    const wrapper = shallow(<PerksList planType={FREE} subscription={subscription} />);

    const perksWrapper = wrapper.find(Perk);
    expect(perksWrapper).to.have.length(2);
    expect(perksWrapper.at(0).props()).to.deep.equal({
      perk,
      planType: FREE,
      subscription,
    });
  });

  it('should render <PerksList/> - perks error', () => {
    const subscription = { id: '1', error: null, loading: false };

    getPerksErrorStub.returns('perks');

    const wrapper = shallow(<PerksList planType={FREE} subscription={subscription} />);

    expect(wrapper.find(Perk)).to.have.length(0);

    const errorBox = wrapper.find(ErrorBox);
    expect(errorBox).to.have.length(1);
    expect(errorBox.prop('type')).to.be.equal('Perks');
  });

  it('should render <PerksList/> - user error', () => {
    const subscription = { id: '1', error: null, loading: false };

    getUserErrorStub.returns('user');

    const wrapper = shallow(<PerksList planType={FREE} subscription={subscription} />);

    expect(wrapper.find(Perk)).to.have.length(0);

    const errorBox = wrapper.find(ErrorBox);
    expect(errorBox).to.have.length(1);
    expect(errorBox.prop('type')).to.be.equal('User');
  });

  it('should render <PerksList/> - subscription error', () => {
    const subscription = { id: '1', error: 'sub', loading: false };

    const wrapper = shallow(<PerksList planType={FREE} subscription={subscription} />);

    expect(wrapper.find(Perk)).to.have.length(0);

    const errorBox = wrapper.find(ErrorBox);
    expect(errorBox).to.have.length(1);
    expect(errorBox.prop('type')).to.be.equal('Subscription');
  });
});
