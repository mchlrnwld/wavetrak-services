import React, { useState } from 'react';
import { Provider } from 'react-redux';
import { useMount } from 'react-use';
import type { AppProps } from 'next/app';

import { CookiesProvider } from 'react-cookie';
import { createSplitFilter, setupFeatureFramework } from '@surfline/web-common';
import config from './config';
import { AppHead } from './components';
import store, { dispatch } from './store';
import { fetchUser, fetchUserRegion } from './actions/user';
import { getUser } from './selectors/user';
import * as treatments from './utils/treatments';

type ComponentType = AppProps<{ appReady: boolean }>['Component'];

interface Props {
  Component: ComponentType;
}

const App = ({ Component }: Props): JSX.Element => {
  const [appReady, setAppReady] = useState<boolean>(false);
  useMount(() => {
    const setup = async () => {
      await Promise.all([fetchUser(), fetchUserRegion()].map(dispatch));

      try {
        await setupFeatureFramework({
          authorizationKey: config.splitio,
          user: getUser(store.getState()),
          splitFilters: createSplitFilter(treatments),
        });
      } finally {
        setAppReady(true);
      }
    };
    // eslint-disable-next-line no-void
    void setup();
  });

  return (
    <Provider store={store}>
      <CookiesProvider>
        <AppHead />
        <Component appReady={appReady} />
      </CookiesProvider>
    </Provider>
  );
};

export default App;
