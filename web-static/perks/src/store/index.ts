/* istanbul ignore file */

import { configureStore } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
import reducer from '../reducers';
import config from '../config';

const store = configureStore({
  reducer,
  devTools: config.APP_ENV !== 'production',
});

export type RootState = ReturnType<typeof reducer>;
export type AppDispatch = typeof store.dispatch;
export const { dispatch } = store;
export const useAppDispatch = (): AppDispatch => useDispatch<AppDispatch>();
export default store;
