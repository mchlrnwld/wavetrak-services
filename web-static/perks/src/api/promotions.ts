/* istanbul ignore file */

import { baseFetch } from '@surfline/web-common';
import type { BrandAbbr } from '@surfline/types';
import config from '../config';
import type { Perk, Perks } from '../types/perks';

export const getPerks = (brand: BrandAbbr): Promise<Perks> =>
  baseFetch(`${config.servicesEndpoint}/promotions/perks?brand=${brand}`, {
    method: 'GET',
  });

/**
 * @template T
 * @param {string} baseUrl
 * @param {string} perkId
 * @param {SL | FS | BW | CW | WT } brand
 * @returns {Promise<T>}
 */
export const generatePerkCode = (
  perkId: Perk['_id'],
  brand: BrandAbbr,
): Promise<{ code: string }> =>
  baseFetch(`${config.servicesEndpoint}/promotions/${perkId}/perk-code?brand=${brand}`, {
    method: 'POST',
  });
