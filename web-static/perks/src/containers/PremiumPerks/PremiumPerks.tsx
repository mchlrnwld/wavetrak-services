import React, { useState, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { useMount } from 'react-use';
import Loading from '../../components/Loading';
import { fetchPerks } from '../../actions/perks';
import { fetchSubscription } from '../../actions/subscription';
import { getAuthenticatedStatus, getUserCountryCode } from '../../selectors/user';
import { Perks, PerksNav } from '../../components';
import '../../styles/main.scss';
import { useAppDispatch } from '../../store';

interface PageProps {
  appReady?: boolean;
}

const PerksPage = ({ appReady }: PageProps): JSX.Element => {
  const dispatch = useAppDispatch();
  const [pageReady, setPageReady] = useState(false);

  const authenticated = useSelector(getAuthenticatedStatus);
  const countryCode = useSelector(getUserCountryCode);

  const fetchData = useCallback(async () => {
    await Promise.all([fetchPerks(), fetchSubscription()].map(dispatch));
    setPageReady(true);
  }, [dispatch]);

  useMount(() => {
    // eslint-disable-next-line no-void
    void fetchData();
  });

  return (
    <div className="sl-perks">
      <PerksNav authenticated={authenticated} />
      {appReady && pageReady ? <Perks countryCode={countryCode} /> : <Loading />}
    </div>
  );
};

PerksPage.defaultProps = {
  appReady: false,
};

export default PerksPage;
