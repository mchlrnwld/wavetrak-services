import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { Provider } from 'react-redux';
import sinon from 'sinon';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import PremiumPerks from './PremiumPerks';
import * as subscriptionActions from '../../actions/subscription';
import * as perksActions from '../../actions/perks';
import * as userSelectors from '../../selectors/user';
import * as perksSelectors from '../../selectors/perks';
import * as subscriptionSelectors from '../../selectors/subscription';
import PerksNav from '../../components/PerksNav';
import { AppDispatch } from '../../store';
import { Perks } from '../../components';
import wait from '../../utils/wait';
import { storeState } from '../../store/storeFixture';

const middlewares = [thunk];
const mockStore = configureStore<unknown, AppDispatch>(middlewares);

describe('<PremiumPerks/>', () => {
  let fetchPerksStub: sinon.SinonStub;
  let fetchSubscriptionStub: sinon.SinonStub;
  let getAuthenticatedStatusStub: sinon.SinonStub;
  let getUserLoadingStub: sinon.SinonStub;
  let getPerksLoadingStub: sinon.SinonStub;
  let getSubscriptionLoadingStub: sinon.SinonStub;

  beforeEach(() => {
    fetchPerksStub = sinon.stub(perksActions, 'fetchPerks');
    fetchSubscriptionStub = sinon.stub(subscriptionActions, 'fetchSubscription');
    getAuthenticatedStatusStub = sinon.stub(userSelectors, 'getAuthenticatedStatus');
    getUserLoadingStub = sinon.stub(userSelectors, 'getUserLoading');
    getPerksLoadingStub = sinon.stub(perksSelectors, 'getPerksLoading');
    getSubscriptionLoadingStub = sinon.stub(subscriptionSelectors, 'getSubscriptionLoading');
  });

  afterEach(() => {
    fetchPerksStub.restore();
    fetchSubscriptionStub.restore();
    getAuthenticatedStatusStub.restore();
    getUserLoadingStub.restore();
    getPerksLoadingStub.restore();
    getSubscriptionLoadingStub.restore();
  });

  it('should render <PremiumPerks/>', async () => {
    const store = mockStore(storeState);
    fetchPerksStub.returns({ type: 'test' });
    fetchSubscriptionStub.returns({ type: 'test' });
    getAuthenticatedStatusStub.returns(true);

    const wrapper = mount(
      <Provider store={store}>
        <PremiumPerks appReady />
      </Provider>,
    );

    const perksNav = wrapper.find(PerksNav);

    // await event loop
    await wait(50);
    wrapper.update();

    expect(perksNav).to.have.length(1);
    expect(perksNav.prop('authenticated')).to.be.true();
    expect(wrapper.find(Perks)).to.have.length(1);
    expect(wrapper.find(Perks).prop('countryCode')).to.be.equal('US');

    expect(fetchPerksStub).to.have.been.calledOnce();
    expect(fetchSubscriptionStub).to.have.been.calledOnce();
  });
});
