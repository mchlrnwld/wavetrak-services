import { createReducer } from '@reduxjs/toolkit';
import type { UserDetails } from '@surfline/web-common';
import { fetchUser, fetchUserRegion } from '../actions/user';

export type UserState = {
  error: string | null;
  loading: boolean;
  authenticated?: boolean;
  details?: Partial<UserDetails>;
  countryCode?: string;
  accessToken?: string;
};

export const initialState = {
  error: null,
  loading: true,
  authenticated: false,
  countryCode: null,
  details: null,
};

export default createReducer<UserState>(initialState, (builder) => {
  builder.addCase(fetchUser.pending, (state) => ({
    ...state,
    loading: true,
  }));
  builder.addCase(fetchUser.fulfilled, (state, { payload: { user, accessToken } }) => ({
    ...state,
    details: user
      ? {
          _id: user._id,
          email: user.email,
          firstName: user.firstName,
          lastName: user.lastName,
          isEmailVerified: user.isEmailVerified,
        }
      : null,
    accessToken,
    authenticated: !!user,
    error: null,
    loading: false,
  }));
  builder.addCase(fetchUser.rejected, (state, action) => ({
    ...state,
    error: action.error.message,
    loading: false,
  }));

  builder.addCase(fetchUserRegion.fulfilled, (state, { payload: { countryCode } }) => ({
    ...state,
    countryCode,
  }));
});
