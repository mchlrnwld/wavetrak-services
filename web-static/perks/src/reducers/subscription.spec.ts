import { expect } from 'chai';
import subscription, { initialState } from './subscription';
import { fetchSubscription } from '../actions/subscription';

describe('Reducers / Subscription', () => {
  const subscriptionFixture = {
    createdAt: new Date().toISOString(),
    effectivePrice: 1,
    entitlement: 'sl_premium',
    expiring: false,
    failedPaymentAttempts: 1,
    onTrial: false,
    periodEnd: new Date('01-01-2050').toISOString(),
    periodStart: new Date().toISOString(),
    plan: {
      interval: 'year',
      amount: 1,
      currency: '$',
      id: '123',
      name: 'sl_annual_v2',
      symbol: '$',
      trialPeriodDays: 1,
      upgradable: false,
    },
    renewalCount: 0,
    renewalPrice: 0,
    stripePlanId: '1234',
    subscriptionId: '1234',
    type: 'stripe',
  };

  it('should handle initial state', () => {
    expect(subscription(undefined, { type: '' })).to.deep.equal(initialState);
  });

  it(`should handle the action ${fetchSubscription.pending.toString()}`, () => {
    expect(subscription(undefined, { type: fetchSubscription.pending })).to.deep.equal({
      ...initialState,
      loading: true,
    });
  });
  it(`should handle the action ${fetchSubscription.fulfilled.toString()}`, () => {
    expect(
      subscription(undefined, {
        type: fetchSubscription.fulfilled,
        payload: { subscription: subscriptionFixture },
      }),
    ).to.deep.equal({
      ...initialState,
      loading: false,
      ...subscriptionFixture,
    });
  });
  it(`should handle the action ${fetchSubscription.rejected.toString()}`, () => {
    const message = 'message';
    expect(
      subscription(undefined, {
        type: fetchSubscription.rejected,
        error: { message },
      }),
    ).to.deep.equal({
      ...initialState,
      loading: false,
      error: message,
    });
  });
});
