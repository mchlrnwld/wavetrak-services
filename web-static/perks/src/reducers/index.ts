import { combineReducers } from '@reduxjs/toolkit';
import user from './user';
import perks from './perks';
import subscription from './subscription';

const rootReducer = combineReducers({
  user,
  perks,
  subscription,
});

export default rootReducer;
