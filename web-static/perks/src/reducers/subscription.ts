import { createReducer } from '@reduxjs/toolkit';
import { fetchSubscription } from '../actions/subscription';
import type { Subscription } from '../types/subscription';

export type SubscriptionState = {
  error?: string | null;
  loading: boolean;
} & Partial<Subscription>;

export const initialState: SubscriptionState = {
  error: null,
  loading: true,
};

export default createReducer(initialState, (builder) => {
  builder.addCase(fetchSubscription.pending, (state) => ({
    ...state,
    loading: true,
  }));
  builder.addCase(fetchSubscription.fulfilled, (state, { payload: { subscription } }) => ({
    ...state,
    error: null,
    loading: false,
    ...subscription,
  }));
  builder.addCase(fetchSubscription.rejected, (state, action) => ({
    ...state,
    error: action.error.message,
    loading: false,
  }));
});
