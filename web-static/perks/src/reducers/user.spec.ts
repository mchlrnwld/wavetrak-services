import { expect } from 'chai';
import user, { initialState } from './user';
import { fetchUser, fetchUserRegion } from '../actions/user';

describe('Reducers / User', () => {
  const userFixture = {
    details: null,
    accessToken: 'accessToken',
  };

  const details = {
    isEmailVerified: true,
    _id: '1234',
    firstName: 'Jeremy',
    lastName: 'Monson',
    email: 'jmonson@surfline.com',
  };

  it('should handle initial state', () => {
    expect(user(undefined, { type: '' })).to.deep.equal(initialState);
  });

  it(`should handle the action ${fetchUser.pending.toString()}`, () => {
    expect(user(undefined, { type: fetchUser.pending })).to.deep.equal({
      ...initialState,
      loading: true,
    });
  });
  it(`should handle the action ${fetchUser.fulfilled.toString()} with a user`, () => {
    expect(
      user(undefined, {
        type: fetchUser.fulfilled,
        payload: { user: details, accessToken: userFixture.accessToken },
      }),
    ).to.deep.equal({
      ...initialState,
      details,
      accessToken: userFixture.accessToken,
      authenticated: true,
      error: null,
      loading: false,
    });
  });
  it(`should handle the action ${fetchUser.fulfilled.toString()} without a user`, () => {
    expect(
      user(undefined, {
        type: fetchUser.fulfilled,
        payload: {},
      }),
    ).to.deep.equal({
      ...initialState,
      accessToken: undefined,
      authenticated: false,
      loading: false,
    });
  });
  it(`should handle the action ${fetchUser.rejected.toString()}`, () => {
    const message = 'message';
    expect(
      user(undefined, {
        type: fetchUser.rejected,
        error: { message },
      }),
    ).to.deep.equal({
      ...initialState,
      loading: false,
      error: message,
    });
  });

  it(`should handle the action ${fetchUserRegion.fulfilled.toString()}`, () => {
    const countryCode = 'AU';
    expect(
      user(undefined, {
        type: fetchUserRegion.fulfilled,
        payload: { countryCode },
      }),
    ).to.deep.equal({
      ...initialState,
      countryCode,
    });
  });
});
