import { createReducer } from '@reduxjs/toolkit';
import { fetchPerkCode, fetchPerks } from '../actions/perks';
import type { Perk, Perks } from '../types/perks';

export type PerksState = {
  error: string | null;
  loading: boolean;
} & Partial<Perks>;

export const initialState: PerksState = {
  error: null,
  perks: [],
  loading: true,
};

const findPerk = (perks: Perk[], perkId: string): Perk | undefined =>
  perks.find(({ _id: id }) => id === perkId);

export default createReducer(initialState, (builder) => {
  builder.addCase(fetchPerkCode.fulfilled, (state, action) => {
    const perk = findPerk(state.perks, action.payload.perkId);

    // Using redux toolkit, we can mutate state since there is an
    // immutability middleware, it makes this code WAY simpler :^)
    // https://redux-toolkit.js.org/api/createReducer#direct-state-mutation
    if (perk) {
      perk.perkCode = action.payload.perkCode;
    }
  });
  builder.addCase(fetchPerkCode.rejected, (state, action) => {
    // Perk Code was not passed so action failed
    if (action.payload) {
      const perk = findPerk(state.perks, action.payload.perkId);
      if (perk) {
        perk.error = action.payload.message;
      }
    } else {
      const { message } = action.error;
      // We can disable the lint error here since we are allowed to mutate
      // eslint-disable-next-line no-param-reassign
      state.error = message;
    }
  });
  builder.addCase(fetchPerks.pending, (state) => ({
    ...state,
    loading: true,
  }));
  builder.addCase(fetchPerks.fulfilled, (state, action) => ({
    ...state,
    loading: false,
    ...action.payload,
  }));
  builder.addCase(fetchPerks.rejected, (state, action) => ({
    ...state,
    error: action.error.message,
    loading: false,
  }));
});
