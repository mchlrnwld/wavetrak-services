import { expect } from 'chai';
import perks, { initialState } from './perks';
import { fetchPerkCode, fetchPerks } from '../actions/perks';
import { Perk } from '../types/perks';

describe('Reducers / Perks', () => {
  it('should handle initial state', () => {
    expect(perks(undefined, { type: '' })).to.deep.equal(initialState);
  });

  it(`should handle the action ${fetchPerkCode.fulfilled.toString()}`, () => {
    const state = {
      ...initialState,
      perks: [{ _id: '1' } as Perk],
    };
    const payload = { perkId: '1', perkCode: '123' };
    expect(perks(state, { type: fetchPerkCode.fulfilled, payload })).to.deep.equal({
      ...initialState,
      perks: [{ _id: '1', perkCode: payload.perkCode }],
    });
  });

  it(`should handle the action ${fetchPerkCode.fulfilled.toString()} - no matching perk`, () => {
    const state = {
      ...initialState,
      perks: [],
    };
    const payload = { perkId: '1', perkCode: '123' };
    expect(perks(state, { type: fetchPerkCode.fulfilled, payload })).to.deep.equal(initialState);
  });

  it(`should handle the action ${fetchPerkCode.rejected.toString()} with a thrown error`, () => {
    const state = {
      ...initialState,
    };
    const message = 'message';
    expect(perks(state, { type: fetchPerkCode.rejected, error: { message } })).to.deep.equal({
      ...initialState,
      error: message,
    });
  });

  it(`should handle the action ${fetchPerkCode.rejected.toString()} with a handled error`, () => {
    const state = {
      ...initialState,
      perks: [{ _id: '1' } as Perk],
    };
    const message = 'message';
    expect(
      perks(state, { type: fetchPerkCode.rejected, payload: { message, perkId: '1' } }),
    ).to.deep.equal({
      ...initialState,
      perks: [{ _id: '1', error: message }],
    });
  });

  it(`should handle the action ${fetchPerkCode.rejected.toString()} with a handled error - no matching perk`, () => {
    const state = {
      ...initialState,
      perks: [],
    };
    const message = 'message';
    const action = { type: fetchPerkCode.rejected, payload: { message, perkId: '1' } };
    expect(perks(state, action)).to.deep.equal(initialState);
  });

  it(`should handle the action ${fetchPerks.pending.toString()}`, () => {
    expect(perks(initialState, { type: fetchPerks.pending })).to.deep.equal({
      ...initialState,
      loading: true,
    });
  });

  it(`should handle the action ${fetchPerks.fulfilled.toString()}`, () => {
    expect(
      perks(initialState, { type: fetchPerks.fulfilled, payload: { perks: [] } }),
    ).to.deep.equal({
      ...initialState,
      loading: false,
      perks: [],
    });
  });

  it(`should handle the action ${fetchPerks.rejected.toString()}`, () => {
    const message = 'message';
    expect(perks(initialState, { type: fetchPerks.rejected, error: { message } })).to.deep.equal({
      ...initialState,
      loading: false,
      error: message,
    });
  });
});
