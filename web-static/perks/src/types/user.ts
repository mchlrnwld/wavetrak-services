export type User = {
  isEmailVerified: boolean;
  isActive: boolean;
  brand: string;
  locale: string;
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
  usID: number;
  modifiedDate: string;
  linkedClients: Record<string, unknown>[];
  createdAt: string;
  updatedAt: string;
  hasSessionCompatibleDevice: boolean;
  hasPassword: boolean;
  id: string;
};
