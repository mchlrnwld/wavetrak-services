import {
  YEAR,
  MONTH,
  STRIPE,
  APPLE,
  GOOGLE,
  VIP,
  MONTHLY,
  ANNUAL,
  FREE,
  GIFT,
} from '@surfline/web-common';

export type PlanType = typeof MONTHLY | typeof ANNUAL | typeof FREE | typeof GIFT;

export type Subscription = {
  createdAt: string;
  effectivePrice: number;
  entitlement: string;
  expiring: boolean;
  failedPaymentAttempts: number;
  onTrial: boolean;
  periodEnd: string;
  periodStart: string;
  plan: {
    interval: typeof YEAR | typeof MONTH;
    amount: number;
    currency: string;
    id: string;
    name: string;
    symbol: string;
    trialPeriodDays: number;
    upgradable: string | number | boolean;
  };
  renewalCount: number;
  renewalPrice: number;
  stripePlanId: string;
  planId: string;
  subscriptionId: string;
  type: typeof STRIPE | typeof VIP | typeof APPLE | typeof GOOGLE;
};
