import type { BrandAbbr } from '@surfline/types';

export type Perk = {
  bgDesktopImgPath: string;
  bgMobileImgPath: string;
  brand: BrandAbbr;
  codesPerUser: number;
  createdAt: string;
  description: string;
  discountText: string;
  eligible: boolean;
  freeDescription: string;
  frequency: number;
  isActive: boolean;
  isCodeAvailable: boolean;
  isDeleted: boolean;
  isPremium: boolean;
  kind: string;
  logoImgPath: string;
  maxRedemption: number;
  name: string;
  partnerUrl: string;
  signUpHeader?: string;
  successHeader?: string;
  successMessage?: string;
  successUrl?: string;
  totalRedemption: number;
  updatedAt: string;
  urlKey: string;
  _id: string;
  perkCode?: string;
  lastRequestedCode?: string;
  error?: string;
};

export type Perks = {
  eligibleCount: number;
  heroConfig: {
    productImgPath: string;
    productName: string;
    brand: BrandAbbr;
  };
  isCommonEligible: boolean;
  perks: Perk[];
  renewalCount: number;
};
