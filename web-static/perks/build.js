/* eslint-disable no-console */
const mkdirp = require('mkdirp');
const { exec } = require('child_process');

const { APP_ENV, BRAND } = process.env;
const outDir = `build/${BRAND}/out`;
const build = `npm run build-app --outdir ${outDir}`;
mkdirp(outDir)
  .then(() => {
    console.log('Building for: ', BRAND);
    console.log('Building for APP_ENV: ', APP_ENV);
    console.log('Building for NODE_ENV: ', process.env.NODE_ENV);
    exec(build, (error, stdout, stderr) => {
      console.log('Build output: ', stdout);
      if (error) {
        console.log('Failed to build: ', stderr);
        console.log(`Build exec error: ${error}`);
        throw Error(error);
      }
      return true;
    });
  })
  .catch((err) => {
    console.error('Failed to create directory', outDir, err);
    throw Error(err);
  });
