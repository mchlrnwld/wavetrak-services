import React from 'react';
import dynamic from 'next/dynamic';
import { Loading } from '../src/components';

const Page = dynamic(() => import('../src/containers/PremiumPerks'), {
  loading: Loading,
  ssr: false,
});

const PremiumPerksPage = ({ appReady }: { appReady: boolean }): JSX.Element => (
  <Page appReady={appReady} />
);

export default PremiumPerksPage;
