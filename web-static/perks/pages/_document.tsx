/* eslint-disable react/no-danger */
/* istanbul ignore file */

import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';
import { snippet as segment, resetAnonymousIdCookieSnippet } from '@surfline/web-common';
import { NewRelicBrowser } from '@surfline/quiver-react';
import cookies from '../src/utils/cookies';
import config from '../src/config';

class Doc extends Document {
  render(): JSX.Element {
    const { newRelic } = config;
    const shouldResetAnonymousIdCookie = !cookies.get<string | null>('sl_reset_anonymous_id_v3');
    return (
      <Html lang="en">
        <Head>
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta name="description" content="Premium Perks for Surfline Premium users" />
          {shouldResetAnonymousIdCookie ? (
          <script
            dangerouslySetInnerHTML={{
              __html: resetAnonymousIdCookieSnippet(false),
            }}
          />
        ) : null}
          <script
            type="text/javascript"
            dangerouslySetInnerHTML={{ __html: segment(config.segment, false) }}
          />
          <NewRelicBrowser
            licenseKey={newRelic.licenseKey}
            applicationID={newRelic.applicationID}
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default Doc;
