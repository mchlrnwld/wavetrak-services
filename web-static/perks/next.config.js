const withPlugins = require('next-compose-plugins');
const withSass = require('@zeit/next-sass');
const withImages = require('next-images');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

const cdnHost = {
  development: '',
  production: 'https://wa.cdn-surfline.com/perks/sl',
  sandbox: 'https://product-cdn.sandbox.surfline.com/perks/sl',
  staging: 'https://product-cdn.staging.surfline.com/perks/sl',
};

const APP_ENV = process.env.APP_ENV || 'development';
const brand = process.env.BRAND || 'sl';

const sassData = `$cdnHostname: '${cdnHost[APP_ENV]}/static/';`;
const distDir = `build/${brand}`;
const nextConfig = {
  trailingSlash: true,
  assetPrefix: cdnHost[APP_ENV],
  distDir,
  env: {
    APP_ENV,
  },
  webpack(config) {
    if (config.mode === 'production') {
      if (Array.isArray(config.optimization.minimizer)) {
        config.optimization.minimizer.push(new OptimizeCSSAssetsPlugin({}));
      }
    }
    return config;
  },
};

module.exports = withPlugins(
  [
    [
      withImages,
      {
        esModule: true,
        assetPrefix: cdnHost[APP_ENV],
      },
    ],
    [
      withSass,
      {
        sassLoaderOptions: {
          prependData: sassData,
        },
      },
    ],
    withBundleAnalyzer,
  ],
  nextConfig,
);
