const lotus = [
  {
    agency: 'Wavetrak',
    model: 'Lotus-WW3',
    types: ['SURF', 'SWELL'],
    grids: [
      {
        grid: 'AUS_E_3m',
        startLatitude: -36,
        endLatitude: -24,
        startLongitude: 150,
        endLongitude: 154.5,
        resolution: 0.05,
      },
      {
        grid: 'AUS_SW_3m',
        startLatitude: -36,
        endLatitude: -31,
        startLongitude: 114,
        endLongitude: 116,
        resolution: 0.05,
      },
      {
        grid: 'UK_3m',
        startLatitude: 45,
        endLatitude: 52,
        startLongitude: 353,
        endLongitude: 0,
        resolution: 0.05,
      },
    ],
  },
];

export const proteus = [
  {
    agency: 'Wavetrak',
    model: 'Proteus-WW3',
    types: ['SURF', 'SWELL'],
    grids: [
      {
        grid: 'AUS_10m',
        startLatitude: null,
        endLatitude: null,
        startLongitude: null,
        endLongitude: null,
        resolution: 0.16666666666666666,
      },
      {
        grid: 'BALI_4m',
        startLatitude: null,
        endLatitude: null,
        startLongitude: null,
        endLongitude: null,
        resolution: 0.06666666666666667,
      },
    ],
  },
];

export default [...proteus, ...lotus];
