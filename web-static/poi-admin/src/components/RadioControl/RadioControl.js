import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './RadioControl.scss';

const RadioControl = ({ name, label, options, choice, error, message, onChange }) => {
  const [currentValue, setCurrentValue] = useState(choice);

  const updateChoice = async (val) => {
    await setCurrentValue(val);
    onChange(val);
  };

  return (
    <div className="poi-tool-radio-control">
      {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
      <h3 className="poi-tool-radio-control__label">{label}</h3>
      <ul className="poi-tool-radio-control__choices">
        {options.map((option) => (
          <li className="poi-tool-radio-control__choices__choice" key={`${name}${option}`}>
            <label
              className="poi-tool-radio-control__choices__choice__label"
              htmlFor={`${name}${option}`}
            >
              <input
                type="radio"
                className="poi-tool-radio-control__choices__choice__input"
                name={name}
                id={`${name}${option}`}
                value={option}
                onChange={({ target }) => updateChoice(target.value)}
                checked={currentValue === option}
              />
              {option}
            </label>
          </li>
        ))}
      </ul>
      {error && <p className="error">{message}</p>}
    </div>
  );
};

RadioControl.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
  choice: PropTypes.string.isRequired,
  error: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default RadioControl;
