import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { Alert, Input, Button } from '@surfline/quiver-react';
import { useMutation } from '@apollo/client';
import { CreatePOI, UpsertSurfSpotConfiguration } from '../../graphql/schema';
import Map from '../PointOfInterestMap';
import '../../App.scss';
import './CreatePOI.scss';

export default () => {
  const [mutationStatus, setMutationStatus] = useState({});
  const [createPOI, { loading: creatingPointOfInterest }] = useMutation(CreatePOI);
  const [formValues, setFormValues] = useState({ name: '', lat: 0, lon: 0 });
  const [updateSurfSpotConfiguration] = useMutation(UpsertSurfSpotConfiguration);

  const onSubmit = async (event) => {
    event.preventDefault();

    const { name, lat, lon } = formValues;

    try {
      const {
        data: {
          createPointOfInterest: { pointOfInterest, success },
        },
      } = await createPOI({
        variables: {
          name,
          lat: +lat,
          lon: +lon,
        },
      });

      const {
        data: {
          upsertSurfSpotConfiguration: { success: upsertSurfSpotConfigurationSuccess },
        },
      } = await updateSurfSpotConfiguration({
        variables: {
          pointOfInterestId: pointOfInterest.pointOfInterestId,
          surfSpotConfiguration: {
            offshoreDirection: 0,
            breakingWaveHeightAlgorithm: 'POLYNOMIAL',
          },
        },
      });

      if (success && upsertSurfSpotConfigurationSuccess) {
        setMutationStatus({
          success: true,
          pointOfInterestId: pointOfInterest.pointOfInterestId,
        });
      } else {
        setMutationStatus({
          success: false,
          error: 'Error creating point of interest',
        });
      }
    } catch (error) {
      setMutationStatus({
        success: false,
        error: 'Error creating point of interest',
      });
    }
  };

  return (
    <>
      {mutationStatus.success && mutationStatus.pointOfInterestId && (
        <Redirect to={`/poi/edit/${mutationStatus.pointOfInterestId}`} />
      )}

      <h4>New Point of Interest</h4>
      <div className="poi-tool__create">
        <form onSubmit={onSubmit}>
          <Input
            type="text"
            id="name"
            name="name"
            label="Name"
            input={{
              name: 'name',
              value: formValues.name,
              onChange: ({ target }) => setFormValues({ ...formValues, name: target.value }),
            }}
          />
          <Input
            type="number"
            id="lat"
            name="lat"
            label="Lat"
            defaultValue={`${formValues.lat}`}
            input={{
              name: 'lat',
              step: 'any',
              value: formValues.lat,
              onChange: ({ target }) => setFormValues({ ...formValues, lat: +target.value }),
            }}
          />
          <Input
            type="number"
            id="lon"
            name="lon"
            label="Lon"
            defaultValue={`${formValues.lon}`}
            input={{
              name: 'lon',
              step: 'any',
              value: formValues.lon,
              onChange: ({ target }) => setFormValues({ ...formValues, lon: +target.value }),
            }}
          />

          <Button parentWidth loading={creatingPointOfInterest}>
            Create POI
          </Button>
          {mutationStatus.success === false && <Alert type="error">{mutationStatus.error}</Alert>}
        </form>
        <Map
          location={{
            lat: formValues.lat,
            lon: formValues.lon,
          }}
          zoom={2}
          updateLocation={({ lat, lon }) => setFormValues({ ...formValues, lat, lon })}
        />
      </div>
    </>
  );
};
