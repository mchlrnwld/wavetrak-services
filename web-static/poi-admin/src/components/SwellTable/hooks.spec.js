/* eslint-disable react/prop-types */
import React from 'react';
import { expect } from '@jest/globals';
import { mount } from 'enzyme';
import { useSwellPreview } from './hooks';
import * as breakingWaveHeightsAPI from '../../api/breakingWaveHeights';
import * as swellAPI from '../../api/swell';
import {
  spectralRefractionMatrix,
  swellPreview,
  swellPreviewWithWaveHeights,
  emptySwellPreview,
} from '../../../fixtures/swellPreview';

describe('SwellTable / hooks', () => {
  const inputStub = {
    algorithm: 'SPECTRAL_REFRACTION',
    breakingWaveHeightCoefficient: 1,
    breakingWaveHeightIntercept: 0,
    spectralRefractionMatrix,
  };
  const Component = ({ agency, model, grid, lat, lon, input, isNewModel }) => {
    const {
      loading,
      swellPreview: data,
      error,
    } = useSwellPreview(agency, model, grid, lat, lon, input, isNewModel);

    return (
      <div className="swell-preview">
        {loading && <div className="loading" />}
        {data && <div className="data" data={data} />}
        {error && <div className="error" />}
      </div>
    );
  };

  /** @type {import('jest-mock').SpyInstance} */
  let useGetBreakingWaveHeightsStub;
  /** @type {import('jest-mock').SpyInstance} */
  let useGetSwellDataStub;

  beforeEach(() => {
    useGetBreakingWaveHeightsStub = jest.spyOn(breakingWaveHeightsAPI, 'useGetBreakingWaveHeights');
    useGetSwellDataStub = jest.spyOn(swellAPI, 'useGetSwellData');
  });

  afterEach(() => {
    useGetBreakingWaveHeightsStub.mockRestore();
  });

  describe('useSwellPreview', () => {
    it('should fetch the swell data and breaking wave heights', () => {
      useGetSwellDataStub
        .mockReturnValueOnce({
          data: undefined,
          isValidating: true,
          error: null,
        })
        .mockReturnValue({
          data: swellPreview,
          isValidating: false,
          error: null,
        });

      useGetBreakingWaveHeightsStub
        .mockReturnValueOnce({
          data: undefined,
          isValidating: true,
          error: null,
        })
        .mockReturnValue({
          data: swellPreviewWithWaveHeights,
          isValidating: false,
          error: null,
        });

      const wrapper = mount(
        <Component
          agency="Wavetrak"
          model="Lotus-WW3"
          grid="CAL_3m"
          lat="36"
          lon="-122"
          input={inputStub}
          isNewModel={false}
        />,
      );

      wrapper.setProps({ reRender: true });
      wrapper.update();

      expect(wrapper.find('.data')).toHaveLength(1);
      expect(wrapper.find('.data').prop('data')).toStrictEqual(swellPreviewWithWaveHeights);
    });

    it('should fetch only the swell data if isNewModel is true', () => {
      useGetSwellDataStub
        .mockReturnValueOnce({
          data: undefined,
          isValidating: true,
          error: null,
        })
        .mockReturnValue({
          data: swellPreview,
          isValidating: false,
          error: null,
        });

      useGetBreakingWaveHeightsStub.mockReturnValue({
        data: undefined,
        isValidating: false,
        error: null,
      });

      const wrapper = mount(
        <Component
          agency="Wavetrak"
          model="Lotus-WW3"
          grid="CAL_3m"
          lat="36"
          lon="-122"
          input={inputStub}
          isNewModel
        />,
      );

      wrapper.setProps({ reRender: true });
      wrapper.update();
      expect(wrapper.find('.data')).toHaveLength(1);
      expect(wrapper.find('.data').prop('data')).toStrictEqual(swellPreview);
      expect(useGetBreakingWaveHeightsStub.mock.calls.length).toBeGreaterThanOrEqual(2);
      expect(useGetBreakingWaveHeightsStub).toHaveBeenLastCalledWith(
        swellPreview,
        inputStub,
        true,
        false,
      );
    });

    it('should skip the breaking wave heights API call if all swells are empty', () => {
      useGetSwellDataStub
        .mockReturnValueOnce({
          data: undefined,
          isValidating: true,
          error: null,
        })
        .mockReturnValue({
          data: emptySwellPreview,
          isValidating: false,
          error: null,
        });

      useGetBreakingWaveHeightsStub.mockReturnValue({
        data: undefined,
        isValidating: false,
        error: null,
      });

      const wrapper = mount(
        <Component
          agency="Wavetrak"
          model="Lotus-WW3"
          grid="CAL_3m"
          lat="36"
          lon="-122"
          input={inputStub}
          isNewModel={false}
        />,
      );

      wrapper.setProps({ reRender: true });
      wrapper.update();
      expect(wrapper.find('.data')).toHaveLength(1);
      expect(wrapper.find('.data').prop('data')).toStrictEqual(emptySwellPreview);

      expect(useGetBreakingWaveHeightsStub.mock.calls.length).toBeGreaterThanOrEqual(2);
      expect(useGetBreakingWaveHeightsStub).toHaveBeenLastCalledWith(
        emptySwellPreview,
        inputStub,
        false,
        true,
      );
    });

    it('should return a loading state if the breaking wave heights API is loading', () => {
      useGetSwellDataStub.mockReturnValue({
        data: swellPreview,
        isValidating: false,
        error: null,
      });
      useGetBreakingWaveHeightsStub.mockReturnValue({
        data: undefined,
        isValidating: true,
        error: null,
      });

      const wrapper = mount(
        <Component
          agency="Wavetrak"
          model="Lotus-WW3"
          grid="CAL_3m"
          lat="36"
          lon="-122"
          input={inputStub}
          isNewModel={false}
        />,
      );
      expect(wrapper.find('.loading')).toHaveLength(1);
      expect(wrapper.find('.data')).toHaveLength(0);
    });

    it('should return an error if the swell data errors', () => {
      useGetSwellDataStub.mockReturnValue({
        data: swellPreview,
        isValidating: false,
        error: 'some error',
      });
      useGetBreakingWaveHeightsStub.mockReturnValue({
        data: undefined,
        isValidating: false,
        error: null,
      });

      const wrapper = mount(
        <Component
          agency="Wavetrak"
          model="Lotus-WW3"
          grid="CAL_3m"
          lat="36"
          lon="-122"
          input={inputStub}
          isNewModel={false}
        />,
      );
      expect(wrapper.find('.loading')).toHaveLength(0);
      expect(wrapper.find('.data')).toHaveLength(0);
      expect(wrapper.find('.error')).toHaveLength(1);
    });

    it('should return an error if the breaking wave heights data errors', () => {
      useGetSwellDataStub.mockReturnValue({
        data: swellPreview,
        isValidating: false,
        error: false,
      });
      useGetBreakingWaveHeightsStub.mockReturnValue({
        data: undefined,
        isValidating: false,
        error: 'some error',
      });

      const wrapper = mount(
        <Component
          agency="Wavetrak"
          model="Lotus-WW3"
          grid="CAL_3m"
          lat="36"
          lon="-122"
          input={inputStub}
          isNewModel={false}
        />,
      );
      expect(wrapper.find('.loading')).toHaveLength(0);
      expect(wrapper.find('.data')).toHaveLength(0);
      expect(wrapper.find('.error')).toHaveLength(1);
    });
  });
});
