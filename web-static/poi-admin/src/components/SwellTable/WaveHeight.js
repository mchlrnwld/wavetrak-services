import React from 'react';
import PropTypes from 'prop-types';
import { round } from 'lodash';
import { Spinner } from '@surfline/quiver-react';

/**
 * @param {object} props
 * @param {{ min: number, max: number }?} props.waveHeights
 * @param {boolean?} props.loading
 */
const WaveHeight = ({ waveHeights, loading }) => {
  if (!waveHeights && !loading) {
    return 'N/A';
  }

  if (loading) {
    return <Spinner />;
  }

  const { min, max } = waveHeights;

  return (
    <>
      {round(min)}-{round(max)}
      <sup>FT</sup>
    </>
  );
};

WaveHeight.propTypes = {
  waveHeights: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number,
  }),
  loading: PropTypes.bool,
};

WaveHeight.defaultProps = {
  waveHeights: null,
  loading: false,
};

export default WaveHeight;
