import { useState, useMemo } from 'react';
import { useUpdateEffect } from 'react-use';
import { isEmpty } from 'lodash';
import { useGetBreakingWaveHeights } from '../../api/breakingWaveHeights';
import { useGetSwellData } from '../../api/swell';

/**
 * Fetches the current forecast from the SDS for a given grid point
 *
 * @param {String} agency
 * @param {String} model
 * @param {String} grid
 * @param {Number} lat
 * @param {Number} lon
 * @param {import('../../api/breakingWaveHeights').InputWithoutPartitions} input
 * @param {boolean} isNewModel
 */
export const useSwellPreview = (agency, model, grid, lat, lon, input, isNewModel) => {
  const { data: swellData, error: swellError } = useGetSwellData({ agency, model, grid, lat, lon });
  // If all the runs have empty swell arrays then we don't need to fetch breaking wave heights
  // so we'll memoize this value and pass to the breaking wave heights hook
  const allSwellsEmpty = useMemo(
    () => swellData && swellData.every((d) => d.runs.every((run) => isEmpty(run.swells))),
    [swellData],
  );

  const {
    data,
    isValidating,
    error: breakingError,
  } = useGetBreakingWaveHeights(swellData, input, isNewModel, allSwellsEmpty);

  const [swellPreview, setSwellPreview] = useState(data);

  // Whenever the breaking wave heights data updates, update the state. This allows us
  // to persist the data between re-fetches (from form updates). In cases where we don't
  // fetch the breaking wave heights data, we just set the swell data as the swell preview.
  useUpdateEffect(() => {
    // When creating a new model, we don't have the algorithm available
    // so we are just showing swell previews and N/A for wave heights
    if (isNewModel || allSwellsEmpty) {
      setSwellPreview(swellData);
    } else if (data) {
      setSwellPreview(data);
    }
  }, [data, swellData, allSwellsEmpty]);

  return {
    // we only return the loading state for the breaking wave heights hook since that is the final
    // fetch we rely on. If we don't end up fetching it, this value will be false as expected
    loading: isValidating,
    swellPreview,
    error: swellError || breakingError,
  };
};
