import React from 'react';
import { expect } from '@jest/globals';
import { shallow } from 'enzyme';
import WaveHeight from './WaveHeight';

describe('SwellTable / WaveHeight', () => {
  it('return "N/A" if wave heights is empty and loading is false', () => {
    const wrapper = shallow(<WaveHeight />);

    expect(wrapper.text()).toEqual('N/A');
  });

  it('return <Spinner /> if loading', () => {
    const wrapper = shallow(<WaveHeight loading />);

    expect(wrapper.find('Spinner')).toHaveLength(1);
  });

  it('return rounded wave heights', () => {
    const wrapper = shallow(<WaveHeight waveHeights={{ min: 2.35, max: 3.6 }} />);

    expect(wrapper.text()).toEqual('2-4FT');
  });
});
