/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Fragment, useState } from 'react';
import classNames from 'classnames';
import { AsyncDataContainer, ErrorBox } from '@surfline/quiver-react';
import { swellForecastProps } from '../../proptypes/pointOfInterest';
import { useSwellPreview } from './hooks';
import WaveHeight from './WaveHeight';
import './SwellTable.scss';

/**
 * @param {boolean} waveHeights
 */
const columnClassName = (waveHeights) =>
  classNames({
    'swell-table__col': true,
    'swell-table__col--wave-heights': waveHeights,
  });

const SwellTable = ({ agency, model, grid, lat, lon, breakingWaveHeightInputs, isNewModel }) => {
  const [currentFocus, setCurrentFocus] = useState('');

  const { loading, swellPreview, error } = useSwellPreview(
    agency,
    model,
    grid,
    lat,
    lon,
    breakingWaveHeightInputs,
    isNewModel,
  );

  const toggleRuns = (day) => {
    if (currentFocus === day) {
      setCurrentFocus('');
    } else {
      setCurrentFocus(day);
    }
  };

  if (!swellPreview || (loading && !swellPreview)) {
    return (
      <AsyncDataContainer
        loading
        loadingText="Loading swell preview..."
        width="100%"
        height={200}
        error={error}
        errorMessage="Something went wrong fething the swell preview"
      />
    );
  }

  return (
    <>
      {error && <ErrorBox message="Something went wrong updating the swell preview" />}
      <table className="swell-table">
        <thead>
          <tr>
            <th className={columnClassName()}>Day</th>
            <th className={columnClassName()}>Wave Height</th>
            <th className={columnClassName()}>Swell 1</th>
            <th className={columnClassName()}>Swell 2</th>
            <th className={columnClassName()}>Swell 3</th>
          </tr>
        </thead>
        <tbody>
          {swellPreview.map(({ weekDay, dayOfMonth, month, runs }) => {
            const day = `${weekDay} ${dayOfMonth}/${month}`;
            const highlight = runs.find((run) => run.time === '12pm') || runs[0];
            const { swells: swellHighlights } = highlight;
            const focused = currentFocus === day;

            if (focused) {
              return (
                <Fragment key={day}>
                  <tr
                    className="swell-table__row swell-table__row--heading"
                    onClick={() => toggleRuns(day)}
                  >
                    <td className="swell-table__col swell-table__col--day" colSpan="5">
                      <span className="swell-table__col__text">{day}</span>
                    </td>
                  </tr>
                  {runs.map(({ time, swells, waveHeights }) => (
                    <tr className="swell-table__row swell-table__row--single-run" key={time}>
                      <td className={columnClassName()}>{time}</td>
                      <td className={columnClassName(waveHeights)}>
                        <WaveHeight waveHeights={waveHeights} loading={loading} />
                      </td>
                      {swells.slice(0, 3).map(({ height, period, direction }) => (
                        <td className={columnClassName()} key={`${height}-${period}-${direction}`}>
                          {`${height} @ ${period} / ${direction}`}
                        </td>
                      ))}
                    </tr>
                  ))}
                </Fragment>
              );
            }

            return (
              <tr className="swell-table__row" key={day} onClick={() => toggleRuns(day)}>
                <td className="swell-table__col swell-table__col--day">
                  <span className="swell-table__col__text">{day}</span>
                </td>

                <td className={columnClassName(highlight.waveHeights)}>
                  <WaveHeight waveHeights={highlight.waveHeights} loading={loading} />
                </td>
                {swellHighlights.slice(0, 3).map(({ height, period, direction }) => (
                  <td className={columnClassName()} key={`${height}${period}${direction}`}>
                    {`${height} @ ${period} / ${direction}`}
                  </td>
                ))}
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
};

SwellTable.propTypes = swellForecastProps;

export default SwellTable;
