import { format } from 'date-fns';
import { round as _round, groupBy as _groupBy } from 'lodash';

/**
 * @typedef {object} Associated
 * @typedef {{waveHeight: 'ft' | 'm' }} units
 * @typedef {{ name: string, refId: number, abbr: string, offset: number }[]} timezones
 */

/**
 * @typedef {object} Swell
 * @property {number} height
 * @property {number} direction
 * @property {number} period
 */

/**
 * @typedef {object} SwellData
 * @property {number} timestamp
 * @property {number} timezoneRefId
 * @property {{ combined: Swell, components: Swell[] }} swells
 */

/**
 * @param {object} props
 * @param {Associated} props.associated
 * @param {SwellData[]} props.data
 */
export const formatSwellData = ({ associated, data }) => {
  const unitHeight = associated.units.waveHeight;
  const unitPeriod = 's';
  const unitDirection = '°';
  const [{ offset }] = associated.timezones;
  const jsTime = (unix) => unix * 1000 - offset;

  /** @param {SwellData} run */
  const getDateOfRun = (run) => format(jsTime(run.timestamp), 'dd-MM-yy');

  const runsGroupedByDay = _groupBy(data, getDateOfRun);

  return Object.values(runsGroupedByDay).map((runs) => {
    const [{ timestamp: firstRun }] = runs;
    const [weekDay, dayOfMonth, month] = format(jsTime(firstRun), 'EEEE dd MMM').split(' ');

    return {
      weekDay,
      dayOfMonth,
      month,
      runs: runs.map(({ timestamp, swells }) => ({
        time: format(jsTime(timestamp), 'haaa'),
        swells: swells.components
          .filter(({ height, direction, period }) => height && direction && period)
          .splice(0, 3)
          .map(({ height, direction, period }) => ({
            height: `${_round(height, 2)}${unitHeight}`,
            direction: `${_round(direction, 2)}${unitDirection}`,
            period: `${_round(period, 2)}${unitPeriod}`,
          })),
      })),
    };
  });
};
