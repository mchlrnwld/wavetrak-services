import { expect } from '@jest/globals';
import { formatSwellData } from './formatSwellData';

describe('SwellTable / formatSwellData', () => {
  const associated = { units: { waveHeight: 'ft' }, timezones: [{ offset: 0 }] };
  const data = [
    {
      // Fri Jan 01 2021 00:00:00 GMT+0000
      timestamp: 1609459200,
      timezoneRefId: 0,
      swells: {
        combined: {
          height: 1,
          direction: 1,
          period: 1,
        },
        components: [
          { height: 0, direction: 0, period: 0 },
          { height: 0.001, direction: 0.001, period: 0.001 },
          { height: 1.111, direction: 1.111, period: 1.111 },
          { height: 2, direction: 2, period: 2 },
          { height: 3, direction: 3, period: 3 },
        ],
      },
    },
  ];
  it('should format the swell data', () => {
    const output = formatSwellData({ associated, data });

    expect(output).toStrictEqual([
      {
        weekDay: 'Friday',
        dayOfMonth: '01',
        month: 'Jan',
        runs: [
          {
            time: '12am',
            swells: [
              { height: '0ft', direction: '0°', period: '0s' },
              { height: '1.11ft', direction: '1.11°', period: '1.11s' },
              { height: '2ft', direction: '2°', period: '2s' },
            ],
          },
        ],
      },
    ]);
  });
});
