import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Input, WindArrow } from '@surfline/quiver-react';
import RangeInput from '../RangeInput';
import './DirectionControl.scss';

const DirectionControl = ({ name, label, value = 0, onChange }) => {
  const [direction, setDirection] = useState(value);

  useEffect(() => setDirection(value), [value]);

  const changeDirection = (val) => {
    setDirection(val);
    onChange(val);
  };

  return (
    <div className="poi-tool-direction-control">
      <div className="poi-tool-direction-control__text-input">
        <Input
          id={name}
          label={label}
          value={direction}
          validateDefault={false}
          type="number"
          input={{
            step: 'any',
            name,
            onChange: ({ target }) => changeDirection(target.value),
          }}
        />
      </div>
      <div className="poi-tool-direction-control__arrow">
        <WindArrow degrees={direction} speed={1} />
      </div>
      <div className="poi-tool-direction-control__slider">
        <RangeInput value={direction} range={[0, 360]} onChange={(val) => changeDirection(val)} />
      </div>
    </div>
  );
};

DirectionControl.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default DirectionControl;
