import { gql } from '@apollo/client';

export const brandQuery = gql`
  query POI_CMS_getSpots($pointOfInterestId: String!) {
    pointOfInterest(pointOfInterestId: $pointOfInterestId) {
      pointOfInterestId
      spots {
        ... on SLSpot {
          _id
          brand
        }
        ... on MSWSpot {
          _id
          brand
        }
      }
    }
  }
`;
