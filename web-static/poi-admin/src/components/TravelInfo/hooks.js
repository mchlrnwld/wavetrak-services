import { useQuery } from '@apollo/client';
import { brandQuery } from './schema';

/**
 * @param {string} pointOfInterestId
 */
export const useTravelNav = (pointOfInterestId) => {
  const { data, loading, error } = useQuery(brandQuery, {
    variables: { pointOfInterestId },
  });

  if (loading || !data) {
    return {
      error,
      loading: true,
    };
  }

  const { spots } = data.pointOfInterest;
  const [mswSpot] = spots.filter(({ brand }) => brand === 'MSW');
  const [slSpot] = spots.filter(({ brand }) => brand === 'SL');

  const hasMSWSpot = !!mswSpot;
  const hasSLSpot = !!slSpot;

  return {
    loading,
    error,
    slSpot,
    mswSpot,
    hasMSWSpot,
    hasSLSpot,
  };
};
