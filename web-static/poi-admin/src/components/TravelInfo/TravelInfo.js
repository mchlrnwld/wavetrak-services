import React from 'react';
import { useRouteMatch, Switch, Route, Redirect } from 'react-router-dom';
import { noSlash } from '../../utils';
import MSWSpotTravelInfo from '../POIForm/MSWSpotTravelInfo';
import SLSpotTravelInfo from '../POIForm/SLSpotTravelInfo';
import SpotInfo from '../POIForm/SpotInfo';
import Navbar from '../Navbar';
import { useTravelNav } from './hooks';
import './TravelInfo.scss';

const TravelInfo = () => {
  const {
    url,
    path,
    params: { id: pointOfInterestId },
  } = useRouteMatch();
  const { loading, error, hasMSWSpot, hasSLSpot, mswSpot, slSpot } =
    useTravelNav(pointOfInterestId);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  const goToMSW = (hasMSWSpot && !hasSLSpot) || (hasMSWSpot && hasSLSpot);
  const goToSL = !hasMSWSpot && hasSLSpot;

  return (
    <div className="poi-tool__travel-info">
      <header className="poi-tool__travel-info__header">
        <Navbar
          childNav
          links={[
            {
              title: 'magicseaweed',
              href: `${url}/magicseaweed`,
            },
            {
              title: 'Surfline',
              href: `${url}/surfline`,
            },
            {
              title: 'Spot Info',
              href: `${url}/spot-info`,
            },
          ]}
        />
      </header>

      <Switch>
        <Route exact path={path}>
          {goToMSW && <Redirect to={`${noSlash(url)}/magicseaweed`} />}
          {goToSL && <Redirect to={`${noSlash(url)}/surfline`} />}
        </Route>
        <Route path={`${path}/magicseaweed`}>
          <MSWSpotTravelInfo surflineSpotId={slSpot?._id} mswSpotId={mswSpot?._id} />
        </Route>
        <Route path={`${path}/surfline`}>
          <SLSpotTravelInfo surflineSpotId={slSpot?._id} mswSpotId={mswSpot?._id} />
        </Route>
        <Route path={`${path}/spot-info`}>
          <SpotInfo
            brandSpotMissing={!hasSLSpot}
            redirectTo={`${url}/surfline`}
            surflineSpotId={slSpot?._id}
            mswSpotId={mswSpot?._id}
          />
        </Route>
      </Switch>
    </div>
  );
};

export default TravelInfo;
