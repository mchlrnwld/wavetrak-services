/* eslint-disable react/prop-types */
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Button, Alert, Select, TextArea, Input } from '@surfline/quiver-react';
import { useForm, Controller } from 'react-hook-form';
import Map from '../../PointOfInterestMap';
import CheckboxControl from '../../CheckboxControl';
import { useSLSpotUpsert, useTravelData } from '../../../hooks/SLTravelHooks';
import './TravelDetails.scss';

const allAbilityLevels = [
  { name: 'Beginner', _id: 'BEGINNER' },
  { name: 'Intermediate', _id: 'INTERMEDIATE' },
  { name: 'Advanced', _id: 'ADVANCED' },
];

const allBoardTypes = [
  { name: 'Shortboard', _id: 'SHORTBOARD' },
  { name: 'Fish', _id: 'FISH' },
  { name: 'Funboard', _id: 'FUNBOARD' },
  { name: 'Longboard', _id: 'LONGBOARD' },
  { name: 'Gun', _id: 'GUN' },
  { name: 'Tow', _id: 'TOW' },
  { name: 'SUP', _id: 'SUP' },
  { name: 'Foil', _id: 'FOIL' },
  { name: 'Skimming', _id: 'SKIMMING' },
  { name: 'Bodyboard', _id: 'BODYBOARD' },
  { name: 'Bodysurfing', _id: 'BODYSURFING' },
  { name: 'Kiteboard', _id: 'KITEBOARD' },
];

const allBreakTypes = [
  { name: 'Beach Break', _id: 'Beach_Break' },
  { name: 'Canyon', _id: 'Canyon' },
  { name: 'Jetty', _id: 'Jetty' },
  { name: 'Offshore', _id: 'Offshore' },
  { name: 'Pier', _id: 'Pier' },
  { name: 'Point', _id: 'Point' },
  { name: 'Reef', _id: 'Reef' },
  { name: 'Slab', _id: 'Slab' },
  { name: 'Lefts', _id: 'Lefts' },
  { name: 'Rights', _id: 'Rights' },
];

const allBottomTypes = [
  { name: 'Coral', _id: 'Coral' },
  { name: 'Lava', _id: 'Lava' },
  { name: 'Rock', _id: 'Rock' },
  { name: 'Sand', _id: 'Sand' },
];

const allTides = ['Low', 'Medium_Low', 'Medium', 'Medium_High', 'High'];

const allSeasons = ['Spring', 'Summer', 'Autumn', 'Winter'];

const TravelDetailsItemInput =
  ({ field, label }) =>
  ({ hasError, getError, getValues, register, watchStatus }) => {
    let registerOptions = {};
    if (watchStatus === 'PUBLISHED') {
      registerOptions = { required: `${field} is required` };
    }
    const descriptionName = `travelDetails.${field}.description`;
    const ratingName = `travelDetails.${field}.rating`;
    return [
      <TextArea
        type="text"
        label={`${label} Description`}
        id={descriptionName}
        defaultValue={getValues(descriptionName)}
        textarea={{ ref: register(registerOptions), name: descriptionName }}
        error={hasError(descriptionName)}
        message={getError(descriptionName)}
        parentWidth
      />,
      <Select
        label={`${label}${!label.match('Rating') ? ' Rating' : ''}`}
        id={ratingName}
        options={[...Array(10)].map((val, index) => ({
          text: `${index + 1}`,
          value: index + 1,
        }))}
        defaultValue={getValues(ratingName)}
        select={{ ref: register(registerOptions), name: ratingName }}
        error={hasError(ratingName)}
        message={getError(ratingName)}
        parentWidth
      />,
    ];
  };

const StringTextarea =
  ({ field, label }) =>
  ({ hasError, getError, getValues, register, watchStatus }) => {
    let registerOptions = {};
    if (watchStatus === 'PUBLISHED') {
      registerOptions = { required: `${field} is required` };
    }
    const name = `travelDetails.${field}`;
    return [
      <TextArea
        type="text"
        label={label}
        id={name}
        defaultValue={getValues(name)}
        textarea={{ ref: register(registerOptions), name }}
        error={hasError(name)}
        message={getError(name)}
        parentWidth
      />,
    ];
  };

const StringInput =
  ({ field, label }) =>
  ({ hasError, getError, getValues, register, watchStatus }) => {
    let registerOptions = {};
    if (watchStatus === 'PUBLISHED' && field !== 'relatedArticleId') {
      registerOptions = { required: `${field} is required` };
    }
    const name = `travelDetails.${field}`;

    return [
      <Input
        type="text"
        label={label}
        id={name}
        defaultValue={getValues(name)}
        input={{ ref: register(registerOptions), name }}
        error={hasError(name)}
        message={getError(name)}
        parentWidth
      />,
    ];
  };

const CheckboxInput =
  ({ field, label, options, isTravelDetail = false }) =>
  ({ hasError, getError, getValues, control, watchStatus }) => {
    let rulesOptions = { required: false };
    if (watchStatus === 'PUBLISHED') {
      rulesOptions = {
        required: true,
        validate: (fieldValues) => fieldValues.length > 0,
      };
    }
    const name = `${isTravelDetail ? 'travelDetails.' : ''}${field}`;
    const values = getValues(name);
    return (
      <Controller
        control={control}
        name={name}
        as={<CheckboxControl />}
        label={label}
        options={options}
        values={
          values
            ? values.map((val) => ({
                _id: val,
              }))
            : []
        }
        error={hasError(name)}
        message={getError(name)}
        rules={rulesOptions}
      />
    );
  };

const fields = [
  CheckboxInput({ field: 'abilityLevels', label: 'Ability Levels', options: allAbilityLevels }),
  StringTextarea({ field: 'abilityLevels.description', label: 'Ability Level Description' }),
  CheckboxInput({ field: 'boardTypes', label: 'Board Types', options: allBoardTypes }),
  StringTextarea({ field: 'access', label: 'Access' }),
  StringTextarea({ field: 'best.season.description', label: 'Best Season Desription' }),
  CheckboxInput({
    field: 'best.season.value',
    label: 'Best Seasons',
    options: allSeasons.map((season) => ({ _id: season, name: season })),
    isTravelDetail: true,
  }),
  StringTextarea({ field: 'best.size.description', label: 'Best Surf Height Size Description' }),
  CheckboxInput({
    field: 'breakType',
    label: 'Break Types',
    options: allBreakTypes,
    isTravelDetail: true,
  }),
  StringTextarea({
    field: 'best.swellDirection.description',
    label: 'Best Swell Direction Description',
  }),
  StringTextarea({ field: 'best.tide.description', label: 'Best Tide Description' }),
  CheckboxInput({
    field: 'best.tide.value',
    label: 'Best Tides',
    options: allTides.map((tide) => ({ _id: tide, name: tide })),
    isTravelDetail: true,
  }),
  StringTextarea({
    field: 'best.windDirection.description',
    label: 'Best Wind Direction Description',
  }),
  StringTextarea({ field: 'bottom.description', label: 'Seabed Description' }),
  CheckboxInput({
    field: 'bottom.value',
    label: 'Seabed',
    options: allBottomTypes,
    isTravelDetail: true,
  }),
  TravelDetailsItemInput({ field: 'crowdFactor', label: 'Crowd Factor' }),
  StringTextarea({ field: 'description', label: 'Spot Description' }),
  StringTextarea({ field: 'hazards', label: 'Hazards' }),
  TravelDetailsItemInput({ field: 'localVibe', label: 'Local Vibe' }),
  TravelDetailsItemInput({ field: 'shoulderBurn', label: 'Shoulder Burn' }),
  StringInput({ field: 'relatedArticleId', label: 'Related Article Id' }),
  TravelDetailsItemInput({ field: 'spotRating', label: 'Spot Rating' }),
  TravelDetailsItemInput({ field: 'waterQuality', label: 'Water Quality' }),
];

/**
 * @typedef {object} Props
 * @property {string} [surflineSpotId]
 * @property {string} [mswSpotId]
 */

/** @type {React.FC<Props>} */
const TravelDetails = ({ surflineSpotId, mswSpotId }) => {
  const { id: pointOfInterestId } = useParams();
  const { upsert, upsertStatus, upsertingSLSpot } = useSLSpotUpsert(pointOfInterestId, true);
  const { data: travelData, loading, error } = useTravelData(pointOfInterestId);

  const { register, errors, getValues, control, reset, watch, handleSubmit } = useForm();

  useEffect(() => {
    if (travelData) {
      reset(travelData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [travelData]);

  const watchStatus = watch('travelDetails.status');

  const abilityLevels = watch('abilityLevels');

  const boardTypes = watch('boardTypes');

  const getErrorPath = (field) => {
    const fieldPath = field.split('.');

    let built = {};
    let loopSetOpen = true;
    fieldPath.forEach((depth) => {
      if (loopSetOpen) {
        if (errors[depth] !== undefined) {
          built = errors[depth];
        } else if (built[depth] !== undefined) {
          built = built[depth];
        } else {
          // Path step was skipped
          loopSetOpen = false;
        }
      }
    });

    // Validation path does not work like other fields in form
    if (field === 'abilityLevels') {
      const fieldValues = getValues(field);
      if (fieldValues.length === 0) {
        built = {
          message: 'Select an ability level',
          type: 'required',
        };
      }
    }

    // Validation path does not work like other fields in form
    if (field === 'boardTypes') {
      const fieldValues = getValues(field);
      if (fieldValues.length === 0) {
        built = {
          message: 'Select a board type',
          type: 'required',
        };
      }
    }

    return built;
  };

  const hasError = (field) => {
    const fieldError = getErrorPath(field);
    let doesError = false;
    if (fieldError.message !== undefined) {
      doesError = true;
    } else if (errors[field] !== undefined) {
      doesError = true;
    }

    return doesError;
  };

  const getError = (field) => {
    const fieldError = getErrorPath(field);
    if (hasError(field)) {
      if (fieldError.message === '') {
        return 'This field is required';
      }
      return fieldError.message;
    }
    return null;
  };

  // don't render anything while the data's not yet fetched
  // or while the form state has not been filled
  if (loading || !watchStatus) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  const disableSubmit = !(
    abilityLevels.length > 0 &&
    boardTypes.length > 0 &&
    Object.keys(errors).length === 0
  );

  return (
    <section className="poi-tool__travel-info__content">
      <form onSubmit={handleSubmit(upsert)} className="poi-tool__sl-spot-data">
        {surflineSpotId && (
          <p>
            <strong>Surfline Spot ID:</strong> {surflineSpotId}
          </p>
        )}
        {mswSpotId && (
          <p>
            <strong>MSW Spot ID:</strong> {mswSpotId}
          </p>
        )}
        {fields.map((field) =>
          field({ hasError, getError, getValues, control, register, watchStatus }),
        )}

        <Select
          label="Status"
          options={['DRAFT', 'PUBLISHED'].map((statusOption) => ({
            text: statusOption,
            value: statusOption,
          }))}
          defaultValue={getValues('travelDetails.status')}
          id="travelDetails.status"
          select={{
            ref: register(),
            name: 'travelDetails.status',
          }}
          error={hasError('travelDetails.status')}
          message={getError('travelDetails.status')}
          parentWidth
        />

        {upsertStatus && (
          <section className="poi-tool__travel-info__form__status">
            <Alert type={upsertStatus.success ? 'success' : 'error'}>{upsertStatus.message}</Alert>
          </section>
        )}

        {disableSubmit && (
          <div className="error">Prior to saving, all fields must be completed</div>
        )}

        <Button
          parentWidth
          loading={upsertingSLSpot}
          disabled={disableSubmit && watchStatus === 'PUBLSIHED'}
        >
          {watchStatus === 'PUBLISHED' && travelData.travelDetails.status !== 'PUBLISHED'
            ? 'Publish'
            : 'Save'}
        </Button>
      </form>
      {travelData?.lat && travelData?.lon && (
        <Map
          location={{
            lat: travelData.lat,
            lon: travelData.lon,
          }}
        />
      )}
    </section>
  );
};

TravelDetails.propTypes = {
  surflineSpotId: PropTypes.string,
  mswSpotId: PropTypes.string,
};

TravelDetails.defaultProps = {
  surflineSpotId: '',
  mswSpotId: '',
};

export default TravelDetails;
