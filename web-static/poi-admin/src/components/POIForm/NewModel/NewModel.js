import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useForm } from 'react-hook-form';
import { Link, useRouteMatch, Redirect } from 'react-router-dom';
import { Input, Alert, Select, Button } from '@surfline/quiver-react';
import { useNewModelData, useModelUpsert } from './hooks';
import { listAllModels, setValues } from '../../../utils';
import { FooterAlert } from '../../styles';
import Map from '../../PointOfInterestMap';
import './NewModel.scss';

const NewModel = () => {
  const {
    params: { id: pointOfInterestId },
  } = useRouteMatch();
  const { data, loading, error } = useNewModelData(pointOfInterestId);
  const { upsert, upsertStatus, isUpserting } = useModelUpsert(pointOfInterestId);

  const [thisModel, setModel] = useState('');
  const [showSwellPreview, setShowSwellPreview] = useState(false);
  const [canPreviewSwell, setCanPreviewSwell] = useState(false);
  const [formVals, setFormVals] = useState({});

  const { register, handleSubmit, errors, setValue, getValues } = useForm({
    agency: '',
    model: thisModel,
    grid: '',
    lat: '',
    lon: '',
  });

  useEffect(() => {
    const { agency, model, grid, lat, lon } = formVals;

    if (agency === 'Wavetrak' && model === 'Lotus-WW3' && grid && lat && lon) {
      setCanPreviewSwell(true);
    } else {
      setCanPreviewSwell(false);
    }
  }, [formVals]);

  if (loading || !data) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  const { pointOfInterest, models } = data;

  return (
    <>
      <Helmet>
        <title>{data.pointOfInterest.name} | New Model | POI CMS</title>
      </Helmet>
      {upsertStatus && upsertStatus.success && <Redirect to="." />}
      <div className="poi-tool-new-model">
        <div className="poi-tool-add-model__header">
          <button
            type="button"
            disabled={!canPreviewSwell}
            className={`poi-tool__swell-preview-toggle ${
              canPreviewSwell ? 'poi-tool__swell-preview-toggle--available' : ''
            }`}
            onClick={() => setShowSwellPreview(!showSwellPreview)}
          >
            {canPreviewSwell
              ? `${showSwellPreview ? 'Show map' : 'Show swell preview'}`
              : 'swell preview unavailable'}
          </button>
        </div>
        <div className="poi-tool-add-model__content">
          <form className="poi-tool-add-model__form" onSubmit={handleSubmit(upsert)}>
            <Select
              id="model"
              name="model"
              label="model"
              error={errors && errors.model}
              select={{
                name: 'model',
                ref: register({ required: true }),
                onChange: ({ target }) => {
                  setModel(getValues('model'));
                  const [agency, model, grid] = target.value.split(' ');
                  setFormVals({
                    ...formVals,
                    agency,
                    model,
                    grid,
                  });
                },
              }}
              options={[
                {
                  text: '...',
                  value: '',
                },
                ...listAllModels(models, pointOfInterest).map((name) => ({
                  text: name,
                  value: name,
                })),
              ]}
            />
            <p>
              Models with a * at the end dont have a grid description we can use to plot the grid
              points on the map
            </p>

            <Input
              label="lat"
              id="lat"
              type="number"
              value={formVals.lat}
              input={{
                name: 'lat',
                step: 'any',
                ref: register({ required: true }),
                onChange: ({ target }) => setFormVals({ ...formVals, lat: +target.value }),
              }}
            />

            <Input
              label="lon"
              id="lon"
              type="number"
              value={formVals.lon}
              input={{
                name: 'lon',
                step: 'any',
                ref: register({ required: true }),
                onChange: ({ target }) => setFormVals({ ...formVals, lon: +target.value }),
              }}
            />

            <footer className="poi-tool-add-model__form__footer">
              <Button loading={isUpserting}>Add</Button>

              <Link to=".">
                <Button
                  style={{
                    backgroundColor: 'tomato',
                    borderColor: 'tomato',
                    width: '100%',
                  }}
                  button={{
                    disabled: isUpserting,
                  }}
                >
                  Cancel
                </Button>
              </Link>
            </footer>

            {upsertStatus && !upsertStatus.success && (
              <FooterAlert>
                <Alert type="error">{upsertStatus.message}</Alert>
              </FooterAlert>
            )}
          </form>
          <Map
            gridParams={{ model: thisModel, models, location: pointOfInterest }}
            gridPointLocation={{ lat: formVals.lat, lon: formVals.lon }}
            updateGridpointLocation={(values) => {
              setValues({ values, setValue, keys: ['lat', 'lon'] });

              setFormVals({
                ...formVals,
                lat: values.lat,
                lon: values.lon,
              });
            }}
            location={{
              lat: pointOfInterest.lat,
              lon: pointOfInterest.lon,
            }}
            showSwellPreview={showSwellPreview}
            swellProps={formVals}
            isNewModel
          />
        </div>
      </div>
    </>
  );
};

export default NewModel;
