import { useState } from 'react';
import { useMutation, useQuery } from '@apollo/client';
import { modelUpsertData, UpsertPOIGridPoints } from '../../../graphql/schema';

export const useNewModelData = (pointOfInterestId) => {
  const { data, loading, error } = useQuery(modelUpsertData, {
    variables: { pointOfInterestId },
  });

  return {
    data,
    loading,
    error,
  };
};

export const useModelUpsert = (pointOfInterestId) => {
  const [upsertGridPoint, { loading: isUpserting }] = useMutation(UpsertPOIGridPoints);
  const [upsertStatus, setUpsertStatus] = useState();

  const upsert = async (gridPoint) => {
    const [agency, model, grid] = gridPoint.model.split(' ');

    try {
      setUpsertStatus(undefined);

      const {
        data: {
          upsertPointOfInterestGridPoints: { success, message },
        },
      } = await upsertGridPoint({
        variables: {
          pointOfInterestId,
          gridPoints: [
            {
              agency,
              model,
              grid,
              lat: +gridPoint.lat, // convert lat
              lon: +gridPoint.lon, // and lon to numbers
            },
          ],
        },
      });

      setUpsertStatus({
        success,
        message,
      });
    } catch (error) {
      setUpsertStatus({
        succes: false,
        message: error,
      });
    }
  };

  return {
    upsert,
    upsertStatus,
    isUpserting,
  };
};
