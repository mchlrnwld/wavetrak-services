import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { useParams } from 'react-router-dom';
import { Button, Input, Select, Alert, TextArea } from '@surfline/quiver-react';
import { useForm, Controller } from 'react-hook-form';
import { isNumber } from 'lodash';
import ThumbnailSelector from './ThumbnailSelector';
import Map from '../../PointOfInterestMap';
import SpotCams from '../../SpotCams';
import WeatherStations from '../../WeatherStations';
import './SLSpotTravelInfo.scss';
import RadioControl from '../../RadioControl';
import { useTidePorts, useTimezones } from '../../../hooks/remoteDataHooks';
import { useSLSpotData, useSLSpotUpsert, useNearbySpots } from '../../../hooks/SLTravelHooks';
import { requiredDataSourceModels, dataSourcesCheck } from '../../../utils/dataSourceValidation';
import {
  requiredModelTypes,
  defaultForecastModelCheck,
} from '../../../utils/forecastSettingModelValidation';

/**
 * @typedef {object} Props
 * @property {string} [surflineSpotId]
 * @property {string} [mswSpotId]
 */

/** @type {React.FC<Props>} */
const SLSpotTravelInfo = ({ surflineSpotId, mswSpotId }) => {
  const { id: pointOfInterestId } = useParams();

  const {
    loading,
    error,
    spotExists,
    lat,
    lon,
    spotData,
    poiData,
    spotTypes,
    subregions,
    cams,
    weatherStations,
  } = useSLSpotData(pointOfInterestId);

  let requiredDataSources = { valid: false };
  let requiredForecastModelTypes = { valid: false };
  if (poiData) {
    const { defaultForecastModels, gridPoints } = poiData;
    requiredDataSources = dataSourcesCheck(gridPoints, requiredDataSourceModels);
    requiredForecastModelTypes = defaultForecastModelCheck(
      defaultForecastModels,
      requiredModelTypes,
    );
  }

  const { timezones } = useTimezones();
  const [coordinates, setCoords] = useState();
  useEffect(() => {
    if (isNumber(lat) && isNumber(lon)) {
      setCoords({ lat, lon });
    }
  }, [lat, lon]);

  const { register, errors, setValue, getValues, watch, control, reset, handleSubmit } = useForm();

  const { nearbySpotReportOptions } = useNearbySpots(!loading && coordinates);
  const { upsert, upsertStatus, upsertingSLSpot } = useSLSpotUpsert(pointOfInterestId, false);

  const {
    nearestTidePorts,
    loading: tidePortsLoading,
    error: tidePortsError,
  } = useTidePorts(!loading && coordinates);

  const watchHumanReport = watch('humanReport');

  const setCoordinates = ({ lat: newLat, lon: newLon }) => {
    setValue('lat', newLat);
    setValue('lon', newLon);
    setCoords({ lat: newLat, lon: newLon });
  };

  useEffect(() => {
    if (spotExists) {
      reset(spotData);
    } else if (poiData) {
      reset({
        name: poiData.name,
        lat: poiData.lat,
        lon: poiData.lon,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [spotExists]);

  const hasError = (field) => errors[field] !== undefined;
  const getError = (field) => hasError(field) && errors[field].message;

  if (loading || tidePortsLoading) return <p>Loading...</p>;
  if (error || tidePortsError) return <p>Error: {error?.message || tidePortsError}</p>;

  return (
    <section className="poi-tool__travel-info__content">
      <Helmet>
        <title>{poiData.name} | SL Spot | POI CMS</title>
      </Helmet>
      <form onSubmit={handleSubmit(upsert)} className="poi-tool__sl-spot-data">
        {!spotExists && (
          <Alert
            type="warning"
            style={{
              marginBottom: '10px',
            }}
          >
            A Surfline spot does not yet exist for this point of interest. Fill out the form below
            to create it.
          </Alert>
        )}
        {!requiredDataSources.valid && (
          <section className="poi-tool__forecast-settings__data__form__status">
            <Alert type="warning">
              Missing required <a href="../data-sources">data source models</a>:
              <ul>
                {requiredDataSources.requiredModelsNotFound.map((model) => (
                  <li>{model}</li>
                ))}
              </ul>
              You will not be able to create or update until resolved.
            </Alert>
          </section>
        )}
        {!requiredForecastModelTypes.valid && (
          <section className="poi-tool__forecast-settings__data__form__status">
            <Alert type="warning">
              Missing required <a href="../forecast-settings">forecast model settings</a>:
              <ul>
                {requiredForecastModelTypes.requiredForecastModelsNotFound.map((model) => (
                  <li>{model}</li>
                ))}
              </ul>
              You will not be able to create or update until resolved.
            </Alert>
          </section>
        )}
        {surflineSpotId && (
          <p>
            <strong>Surfline Spot ID:</strong> {surflineSpotId}
          </p>
        )}
        {mswSpotId && (
          <p>
            <strong>MSW Spot ID:</strong> {mswSpotId}
          </p>
        )}
        <Input
          type="text"
          label="Name"
          id="name"
          defaultValue={getValues().name}
          input={{ ref: register({ required: 'Enter a valid name' }), name: 'name' }}
          error={hasError('name')}
          message={getError('name')}
          parentWidth
        />
        <Select
          label="Spot Type"
          options={spotTypes.map(({ name, type }) => ({ text: name, value: type }))}
          id="spotType"
          select={{
            ref: register({ required: 'Select a Spot Type' }),
            defaultValue: getValues().spotType,
            name: 'spotType',
          }}
          error={hasError('spotType')}
          message={getError('spotType')}
          parentWidth
        />

        {spotExists && (
          <>
            <p>Spot Thumbnails</p>
            <ThumbnailSelector
              id="slSpotThumbnailSelector"
              spot={spotData}
              className="poi-tool__sl-spot-data__dropzone"
            />
          </>
        )}

        <Input
          type="text"
          id="lat"
          label="lat"
          error={hasError('lat')}
          message={getError('lat')}
          defaultValue={getValues().lat}
          input={{
            ref: register({ required: 'Enter a valid lat' }),
            name: 'lat',
            onBlur: (event) => {
              setCoordinates({
                lat: +event.target.value,
                lon: coordinates?.lon,
              });
            },
          }}
        />
        <Input
          type="text"
          id="lon"
          label="lon"
          error={hasError('lon')}
          message={getError('lon')}
          defaultValue={getValues().lon}
          input={{
            ref: register({ required: 'Enter a valid lon' }),
            name: 'lon',
            onBlur: (event) => {
              setCoordinates({
                lat: coordinates?.lat,
                lon: +event.target.value,
              });
            },
          }}
        />

        <h4>Cams</h4>
        <Controller control={control} as={<SpotCams />} camIds={cams} name="cams" />

        <h4>Weather Stations</h4>
        <Controller
          control={control}
          as={<WeatherStations />}
          weatherStationIds={weatherStations}
          name="weatherStations"
        />

        <h4>Location</h4>

        {subregions?.length && (
          <Select
            label="Subregion"
            options={subregions}
            id="subregion"
            select={{
              ref: register({ required: 'Select a Subregion' }),
              defaultValue: getValues().subregion || '',
              name: 'subregion',
            }}
            error={hasError('subregion')}
            message={getError('subregion')}
            parentWidth
          />
        )}

        <Select
          label="Tide Station"
          options={nearestTidePorts}
          defaultValue={spotData?.tideStation}
          id="tideStation"
          select={{ ref: register(), name: 'tideStation' }}
          error={hasError('tideStation')}
          message={getError('tideStation')}
          parentWidth
        />

        {timezones.length > 0 && (
          <Select
            label="Timezone"
            options={timezones}
            defaultValue={getValues().timezone && getValues().timezone.replace(' ', '_')}
            id="timezone"
            select={{ ref: register({ required: 'Select a timezone' }), name: 'timezone' }}
            error={hasError('timezone')}
            message={getError('timezone')}
            parentWidth
          />
        )}

        <Input
          label="Parent Geoname Id"
          id="geoname"
          defaultValue={getValues().geoname}
          input={{ ref: register({ required: 'Enter a valid Geoname Id' }), name: 'geoname' }}
          error={hasError('geoname')}
          message={getError('geoname')}
          parentWidth
        />

        <h4>Ideal Conditions</h4>
        <Input
          style={{ marginTop: 0 }}
          type="text"
          label="Swell Window"
          id="swellWindow"
          defaultValue={getValues().swellWindow}
          input={{ ref: register(), name: 'swellWindow' }}
          error={hasError('swellWindow')}
          message={getError('swellWindow')}
          parentWidth
        />
        <Input
          type="text"
          label="Best Wind Direction"
          id="best.windDirection"
          defaultValue={getValues().best && getValues().best.windDirection}
          input={{ ref: register(), name: 'best.windDirection' }}
          error={hasError('best.windDirection')}
          message={getError('best.windDirection')}
          parentWidth
        />
        <Input
          type="text"
          label="Best Swell Period"
          id="best.swellPeriod"
          defaultValue={getValues().best && getValues().best.swellPeriod}
          input={{ ref: register(), name: 'best.swellPeriod' }}
          error={hasError('best.swellPeriod')}
          message={getError('best.swellPeriod')}
          parentWidth
        />
        <Input
          type="text"
          label="Best Swell Window"
          id="best.swellWindow"
          defaultValue={getValues().best && getValues().best.swellWindow}
          input={{ ref: register(), name: 'best.swellWindow' }}
          error={hasError('best.swellWindow')}
          message={getError('best.swellWindow')}
          parentWidth
        />
        <Input
          type="text"
          label="Best Size Range"
          id="best.sizeRange"
          defaultValue={getValues().best && getValues().best.sizeRange}
          input={{ ref: register(), name: 'best.sizeRange' }}
          error={hasError('best.sizeRange')}
          message={getError('best.sizeRange')}
          parentWidth
        />

        <h4>Spot Details</h4>
        <div className="spots-form__relivable-rating">
          <div className="spots-form__board-type__types-container">
            <div>
              <Select
                label="Relivable Rating"
                id="relivableRating"
                options={[...Array(6)].map((val, index) => ({ text: `${index}`, value: index }))}
                defaultValue={getValues().relivableRating}
                select={{ ref: register(), name: 'relivableRating' }}
                error={hasError('relivableRating')}
                message={getError('relivableRating')}
                parentWidth
              />
            </div>
          </div>
        </div>
        <div className="poi-tool__sl-spot-data__human-report">
          <Controller
            control={control}
            as={<RadioControl />}
            name="humanReport.status"
            choice={spotData?.humanReport.status}
            label="Human Report"
            options={['OFF', 'ON', 'CLONE', 'AVERAGE']}
          />

          {watchHumanReport?.status === 'CLONE' && (
            <>
              {nearbySpotReportOptions.length > 1 ? (
                <Select
                  label="Clone From"
                  id="humanReport.clone.spotId"
                  options={nearbySpotReportOptions}
                  defaultValue={getValues().humanReport?.clone.spotId}
                  select={{
                    ref: register(),
                    name: 'humanReport.clone.spotId',
                  }}
                  error={hasError('humanReport.clone.spotId')}
                  message={getError('humanReport.clone.spotId')}
                  parentWidth
                />
              ) : (
                <p className="poi-tool__sl-spot-data__human-report__loading-msg">
                  Loading nearby spots...
                </p>
              )}
            </>
          )}

          {watchHumanReport?.status === 'AVERAGE' && (
            <>
              {nearbySpotReportOptions.length > 1 ? (
                <>
                  <Select
                    label="Spot One"
                    id="humanReport.average.spotOneId"
                    options={nearbySpotReportOptions}
                    defaultValue={getValues().humanReport?.average.spotOneId}
                    select={{
                      ref: register(),
                      name: 'humanReport.average.spotOneId',
                    }}
                    error={hasError('humanReport.average.spotOneId')}
                    message={getError('humanReport.average.spotOneId')}
                    parentWidth
                  />
                  <Select
                    label="Spot Two"
                    id="humanReport.average.spotTwoId"
                    options={nearbySpotReportOptions}
                    defaultValue={getValues().humanReport?.average.spotTwoId}
                    select={{
                      ref: register(),
                      name: 'humanReport.average.spotTwoId',
                    }}
                    error={hasError('humanReport.average.spotTwoId')}
                    message={getError('humanReport.average.spotTwoId')}
                    parentWidth
                  />
                </>
              ) : (
                <p className="poi-tool__sl-spot-data__human-report__loading-msg">
                  Loading nearby spots...
                </p>
              )}
            </>
          )}
        </div>
        <TextArea
          type="text"
          label="Title Tag"
          id="titleTag"
          defaultValue={getValues().titleTag}
          textarea={{ ref: register(), name: 'titleTag' }}
          error={hasError('titleTag')}
          message={getError('titleTag')}
          parentWidth
        />
        <TextArea
          type="text"
          label="Meta Description"
          id="metaDescription"
          defaultValue={getValues().metaDescription}
          textarea={{
            ref: register(),
            name: 'metaDescription',
          }}
          error={hasError('metaDescription')}
          message={getError('metaDescription')}
          parentWidth
        />
        <Select
          label="status"
          options={[
            { text: 'DRAFT', value: 'DRAFT' },
            { text: 'PUBLISH', value: 'PUBLISHED' },
          ]}
          id="status"
          select={{ ref: register(), defaultValue: getValues().status, name: 'status' }}
          parentWidth
        />
        {upsertStatus && (
          <section className="poi-tool__forecast-settings__data__form__status">
            <Alert type={upsertStatus.success ? 'success' : 'error'}>{upsertStatus.message}</Alert>
          </section>
        )}
        <Button
          parentWidth
          loading={upsertingSLSpot}
          disabled={!requiredDataSources.valid || !requiredForecastModelTypes.valid}
        >
          {spotExists ? 'Update' : 'Create'}
        </Button>
      </form>
      <Map location={coordinates} updateLocation={setCoordinates} />
    </section>
  );
};

SLSpotTravelInfo.propTypes = {
  surflineSpotId: PropTypes.string,
  mswSpotId: PropTypes.string,
};

SLSpotTravelInfo.defaultProps = {
  surflineSpotId: '',
  mswSpotId: '',
};

export default SLSpotTravelInfo;
