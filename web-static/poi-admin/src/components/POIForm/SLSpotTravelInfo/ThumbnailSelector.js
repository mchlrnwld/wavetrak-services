import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DropZone from 'react-dropzone';
import { uploadThumbnail } from '../../../utils';

class ThumbnailSelector extends Component {
  constructor(props) {
    super(props);
    this.state = { uploading: false, error: false, success: false, dirty: false };
  }

  componentDidMount() {
    const { spot } = this.props;
    if (spot.hasThumbnail) {
      const container = document.getElementsByClassName('container');
      if (container.length > 0) {
        container[0].style.background = `url(${spot.thumbnail['3000']}) no-repeat center center fixed`;
      }
    }
  }

  doUploadThumbnail = async (files) => {
    this.setState((prevState) => ({
      ...prevState,
      dirty: true,
      uploading: true,
    }));
    const data = new FormData();
    data.append('thumbnail', files[0]);
    try {
      const {
        spot: { _id },
      } = this.props;
      await uploadThumbnail(_id, data);
      this.setState((prevState) => ({
        ...prevState,
        uploading: false,
        success: true,
      }));
    } catch (err) {
      this.setState((prevState) => ({
        ...prevState,
        uploading: false,
        error: true,
      }));
    }
  };

  render() {
    const { spot, className } = this.props;
    const { uploading, error, success, dirty } = this.state;

    return (
      <div>
        <div>
          {spot && spot.thumbnail
            ? Object.keys(spot.thumbnail).map((thumb) => (
                <a key={thumb} href={spot.thumbnail[thumb]}>
                  {thumb}
                </a>
              ))
            : null}
        </div>
        <DropZone onDrop={this.doUploadThumbnail}>
          {({ getRootProps, getInputProps }) => (
            <div {...getRootProps({ className })}>
              {!dirty ? 'Drag image or click here to upload spot thumbnail.' : null}
              {uploading ? 'Uploading Thumbnail' : null}
              {error ? 'There was a problem uploading.' : null}
              <input {...getInputProps()} />
              {success && dirty ? (
                <span>
                  Succesfully uploaded the thumbnail.
                  <br />
                  It may take a moment to process the image.
                </span>
              ) : null}
            </div>
          )}
        </DropZone>
      </div>
    );
  }
}

ThumbnailSelector.defaultProps = {
  spot: null,
  className: '',
};

ThumbnailSelector.propTypes = {
  spot: PropTypes.shape({
    _id: PropTypes.string,
    thumbnail: PropTypes.shape({
      3000: PropTypes.string,
    }),
    hasThumbnail: PropTypes.bool,
  }),
  className: PropTypes.string,
};

export default ThumbnailSelector;
