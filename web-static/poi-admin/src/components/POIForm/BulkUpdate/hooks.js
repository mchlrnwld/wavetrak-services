import { useState } from 'react';
import { useMutation } from '@apollo/client';
import { UpsertSurfSpotConfiguration } from '../../../graphql/schema';
import { coercePropsToNumber, areGridPointsUniform, makeEverythingAString } from '../../../utils';
import { matrixToString, stringToMatrix } from '../../../matrixUtils';

export const useBulkUpdateData = (models, surfSpotConfiguration) => {
  const gridPointsUniform = areGridPointsUniform(models);
  const [firstModel] = models;

  const defaultModelValues = gridPointsUniform ? makeEverythingAString(firstModel) : {};

  const surfSpotConfigurationValues = makeEverythingAString(surfSpotConfiguration || {});

  const {
    ratingsMatrix: matrix1,
    spectralRefractionMatrix: matrix2,
    ratingsOptions,
  } = surfSpotConfiguration || {};

  return {
    defaultValues: {
      ...defaultModelValues,
      ...surfSpotConfigurationValues,
      ratingsMatrix: matrix1 ? matrixToString(matrix1, 1) : '',
      spectralRefractionMatrix: matrix2 ? matrixToString(matrix2) : '',
      ratingsOptions: ratingsOptions ? JSON.stringify(ratingsOptions, null, 2) : '',
    },
    gridPointsUniform,
  };
};

export const useBulkUpdate = (pointOfInterestId, surfSpotConfiguration) => {
  const [updateStatus, setUpdateStatus] = useState();
  const [updateSurfSpotConfiguration, { loading: isUpdating }] = useMutation(
    UpsertSurfSpotConfiguration,
  );

  const update = async (overrideValues) => {
    // eslint-disable-next-line
    const doubleCheck = confirm('Are you sure you want to override all individual model settings?');

    if (!doubleCheck) return;

    setUpdateStatus(undefined);

    const propertiesThatShouldBeNumbers = [
      'offshoreDirection',
      'optimalSwellDirection',
      'breakingWaveHeightIntercept',
      'breakingWaveHeightCoefficient',
    ];

    const newSurfSpotConfiguration = {
      ...surfSpotConfiguration,
      ...coercePropsToNumber(overrideValues, propertiesThatShouldBeNumbers),
      ratingsMatrix: stringToMatrix(overrideValues.ratingsMatrix, 7),
      ratingsAlgorithm: overrideValues.ratingsAlgorithm || null,
      ratingsOptions: overrideValues.ratingsOptions
        ? JSON.parse(overrideValues.ratingsOptions)
        : null,
    };

    try {
      const {
        data: {
          upsertSurfSpotConfiguration: { success },
        },
      } = await updateSurfSpotConfiguration({
        variables: {
          pointOfInterestId,
          surfSpotConfiguration: newSurfSpotConfiguration,
        },
      });

      setUpdateStatus({
        success,
        message: success
          ? 'All models updated on this point of interest'
          : 'Error performing update',
      });
    } catch (e) {
      setUpdateStatus({
        success: false,
        message: 'Error performing update',
      });
    }
  };

  return {
    update,
    isUpdating,
    updateStatus,
  };
};
