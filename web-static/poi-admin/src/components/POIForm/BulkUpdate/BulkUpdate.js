import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Alert, Input, Button, Select, Checkbox } from '@surfline/quiver-react';
import { useForm } from 'react-hook-form';
import { useBulkUpdateData, useBulkUpdate } from './hooks';
import { bwhaOptions } from '../../../constants';
import {
  oppositeAngle,
  areAnglesOpposite,
  areValuesZero,
  validateOffshoreAndOptimalSwellDirections,
  validateJSON,
} from '../../../utils';
import {
  flattenedGridPointPropTypes,
  surfSpotConfigurationPropTypes,
} from '../../../proptypes/pointOfInterest';
import DirectionControl from '../../DirectionControl';
import './BulkUpdate.scss';
import { validateStringAsNumberMatrixWithDimensions } from '../../../matrixUtils';

const BulkUpdate = ({ pointOfInterestId, models, surfSpotConfiguration }) => {
  const { defaultValues, gridPointsUniform } = useBulkUpdateData(models, surfSpotConfiguration);
  const { update, isUpdating, updateStatus } = useBulkUpdate(
    pointOfInterestId,
    surfSpotConfiguration,
  );

  const { register, getValues, watch, setValue, reset, handleSubmit, formState, errors } = useForm({
    defaultValues,
  });

  const swell = watch('optimalSwellDirection');
  const wind = watch('offshoreDirection');

  const [anglesLinked, setAnglesLinked] = useState(
    areAnglesOpposite(swell, wind) || areValuesZero(swell, wind),
  );

  useEffect(() => {
    register(
      { name: 'optimalSwellDirection' },
      {
        required: true,
        validate: (newOptimalSwellDirection) =>
          validateOffshoreAndOptimalSwellDirections(
            +getValues('offshoreDirection'),
            newOptimalSwellDirection,
          ),
      },
    );
    register(
      { name: 'offshoreDirection' },
      {
        required: true,
        validate: (newOffshoreDirection) =>
          validateOffshoreAndOptimalSwellDirections(
            newOffshoreDirection,
            +getValues('optimalSwellDirection'),
          ),
      },
    );
    register('ratingsMatrix', {
      validate: (value) =>
        validateStringAsNumberMatrixWithDimensions(value, 7) ||
        'Ratings matrix requires 7 entries per row',
    });
    register('ratingsOptions', {
      validate: (ratingsOptions) =>
        !ratingsOptions || validateJSON(ratingsOptions) || 'Ratings options must be valid JSON',
    });
  }, [register, getValues]);

  useEffect(() => {
    if (updateStatus && updateStatus.success) {
      reset(getValues());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [updateStatus]);

  const changeAngle = (type, angle) => {
    setValue(type, angle);

    if (anglesLinked) {
      const opposite = oppositeAngle(angle);

      if (type === 'optimalSwellDirection') {
        setValue('offshoreDirection', opposite);
      } else if (type === 'offshoreDirection') {
        setValue('optimalSwellDirection', opposite);
      }
    }
  };

  return (
    <form className="poi-tool__forecast-settings__data__form" onSubmit={handleSubmit(update)}>
      {updateStatus && (
        <section className="poi-tool__forecast-settings__data__form__status">
          <Alert type={updateStatus.success ? 'success' : 'error'}>{updateStatus.message}</Alert>
        </section>
      )}
      {gridPointsUniform ? (
        <Alert>Surf models for this point of interest are configured uniformly at present.</Alert>
      ) : (
        <Alert type="warning">
          Heads up! Surf models for this point of interest are configured independently at present.
          Any configuration here will override individual settings.
        </Alert>
      )}
      <section className="poi-tool__forecast-settings__data__form__section">
        <p className="section__label">Optimal Swell Direction</p>
        <DirectionControl
          name="optimalSwellDirection"
          label="Optimal Swell Direction"
          value={swell}
          onChange={(angle) => changeAngle('optimalSwellDirection', +angle)}
        />
      </section>

      <section className="poi-tool__forecast-settings__data__form__section">
        {/* eslint-disable-next-line react/self-closing-comp */}
        <p className="section__label"></p>
        <div>
          <Checkbox
            checked={anglesLinked}
            onClick={() => setAnglesLinked(!anglesLinked)}
            copy="Link swell & wind angle?"
          />
        </div>
      </section>

      <section className="poi-tool__forecast-settings__data__form__section">
        <p className="section__label">Offshore Direction</p>
        <DirectionControl
          name="offshoreDirection"
          label="Offshore Direction"
          value={wind}
          onChange={(angle) => changeAngle('offshoreDirection', +angle)}
        />
      </section>
      <section className="poi-tool__forecast-settings__data__form__section">
        <p className="section__label">Breaking Wave Height Algorithm</p>
        <Select
          id="breakingWaveHeightAlgorithm"
          label="Breaking Wave Height Algorithm"
          defaultValue={getValues().breakingWaveHeightAlgorithm}
          select={{
            ref: register,
            name: 'breakingWaveHeightAlgorithm',
          }}
          options={[
            {
              text: '...',
              value: '',
            },
            ...bwhaOptions.map((name) => ({ text: name, value: name })),
          ]}
          parentWidth
        />
      </section>
      <section className="poi-tool__forecast-settings__data__form__section">
        <p className="section__label">Breaking Wave Height Coefficient</p>
        <Input
          type="number"
          id="breakingWaveHeightCoefficient"
          label="Breaking Wave Height Coefficient"
          defaultValue={getValues().breakingWaveHeightCoefficient}
          validateDefault={false}
          input={{
            step: 'any',
            ref: register,
            name: 'breakingWaveHeightCoefficient',
          }}
        />
      </section>
      <section className="poi-tool__forecast-settings__data__form__section">
        <p className="section__label">Breaking Wave Height Intercept</p>
        <Input
          type="number"
          id="breakingWaveHeightIntercept"
          label="Breaking Wave Height Intercept"
          defaultValue={getValues().breakingWaveHeightIntercept}
          validateDefault={false}
          input={{
            step: 'any',
            ref: register,
            name: 'breakingWaveHeightIntercept',
          }}
        />
      </section>
      <section className="poi-tool__forecast-settings__data__form__section">
        <p className="section__label">Ratings Algorithm</p>
        <Select
          id="ratingsAlgorithm"
          label="Ratings Algorithm"
          defaultValue={getValues().ratingsAlgorithm}
          select={{
            ref: register,
            name: 'ratingsAlgorithm',
          }}
          options={[
            { text: 'NONE', value: '' },
            { text: 'MATRIX', value: 'MATRIX' },
            { text: 'ML_RATINGS_MINIMAL', value: 'ML_RATINGS_MINIMAL' },
            { text: 'ML_SPECTRA_REF_MATRIX', value: 'ML_SPECTRA_REF_MATRIX' },
          ]}
          parentWidth
        />
      </section>
      <section className="poi-tool__forecast-settings__data__form__section">
        <p className="section__label">Ratings Matrix</p>
        <textarea
          id="ratingsMatrix"
          name="ratingsMatrix"
          defaultValue={getValues().ratingsMatrix}
          className="poi-tool__forecast-settings__ratings-matrix"
          onChange={(e) => setValue('ratingsMatrix', e.target.value)}
        />
      </section>
      <section className="poi-tool__forecast-settings__data__form__section">
        <p className="section__label">Ratings Options</p>
        <textarea
          id="ratingsOptions"
          name="ratingsOptions"
          defaultValue={getValues().ratingsOptions}
          className="poi-tool__forecast-settings__ratings-options"
          onChange={(e) => setValue('ratingsOptions', e.target.value)}
        />
      </section>
      {Object.keys(errors).length > 0 && (
        <Alert type="error">{Object.values(errors)[0].message}</Alert>
      )}
      <footer className="poi-tool__forecast-settings__data__form__footer">
        <Button disabled={!formState.dirty} loading={isUpdating}>
          Bulk Update All Models
        </Button>
      </footer>
    </form>
  );
};

BulkUpdate.propTypes = {
  pointOfInterestId: PropTypes.string.isRequired,
  models: PropTypes.arrayOf(flattenedGridPointPropTypes).isRequired,
  surfSpotConfiguration: surfSpotConfigurationPropTypes.isRequired,
};

export default BulkUpdate;
