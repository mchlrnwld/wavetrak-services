import { useState, useMemo } from 'react';
import { useQuery, useMutation, useApolloClient } from '@apollo/client';
import { useParams } from 'react-router-dom';
import { useDebounce } from 'react-use';
import {
  flattenGridPointData,
  makeEverythingAString,
  isSurfModel,
  coercePropsToNumberInverted,
} from '../../../utils';
import {
  UpsertPOIGridPoints,
  RemovePointOfInterestGridPoints,
  modelUpsertData,
  QueryGridPoints,
  UpsertSurfSpotConfiguration,
} from '../../../graphql/schema';
import { matrixToString, stringToMatrix } from '../../../matrixUtils';

const gridPointFormValues = (grid, surfSpotConfiguration, isASurfModel) => {
  // Converting all data to strings before registering it
  // with react-hook-form as anything that is initially
  // registered as a number, will be converted to a string in a
  // HTML input which will put the form into a "modified" state
  // when the value hasn't really been updated..
  const gridPointData = makeEverythingAString(grid);
  const surfSpotConfigurationData = makeEverythingAString(surfSpotConfiguration || {});

  const requiredFields = {
    lat: gridPointData.lat,
    lon: gridPointData.lon,
  };

  if (!isASurfModel) {
    return requiredFields;
  }

  const {
    breakingWaveHeightCoefficient,
    breakingWaveHeightIntercept,
    breakingWaveHeightAlgorithm,
    optimalSwellDirection,
    offshoreDirection,
    ratingsMatrix: matrix1,
    spectralRefractionMatrix: matrix2,
  } = surfSpotConfigurationData;

  return {
    ...requiredFields,
    breakingWaveHeightCoefficient,
    breakingWaveHeightIntercept,
    breakingWaveHeightAlgorithm,
    optimalSwellDirection,
    offshoreDirection,
    ratingsMatrix: matrix1 ? matrixToString(matrix1, 1) : '',
    spectralRefractionMatrix: matrix2 ? matrixToString(matrix2) : '',
  };
};

export const useGridPointData = () => {
  const { id: pointOfInterestId, dataSource } = useParams();
  const [agency, model, grid] = dataSource.split('--');
  const { data, loading, error } = useQuery(modelUpsertData, {
    variables: { pointOfInterestId },
  });

  if (loading || !data) {
    return {
      loading: true,
      pointOfInterestId,
    };
  }

  const { pointOfInterest, models } = data;

  const point = pointOfInterest.gridPoints.find(
    ({ grid: gridDef }) =>
      gridDef.agency === agency && gridDef.model === model && gridDef.grid === grid,
  );

  if (!point) {
    return {
      loading: false,
      modelNotFound: true,
      pointOfInterest,
      agency,
      model,
      grid,
    };
  }

  const gridPoint = { ...point, ...point.grid };
  const isASurfModel = isSurfModel(data.models, gridPoint);

  return {
    loading,
    error,
    data: gridPointFormValues(gridPoint, pointOfInterest.surfSpotConfiguration, isASurfModel),
    isASurfModel,
    models,
    pointOfInterestId,
    pointOfInterest,
    canPreviewSwell: agency === 'Wavetrak' && model === 'Lotus-WW3',
    agency,
    model,
    grid,
  };
};

const createPOIGridPointInput = (gridPoint, formValues) => {
  const [flattenedGrid] = flattenGridPointData([gridPoint]);
  const { agency, model, grid } = flattenedGrid;
  const { spectralRefractionMatrix: matrix, ...formVals } = coercePropsToNumberInverted(
    formValues,
    ['breakingWaveHeightAlgorithm', 'spectralRefractionMatrix'],
  );

  return {
    agency,
    model,
    grid,
    lat: formVals.lat,
    lon: formVals.lon,
  };
};

const createSurfSpotConfigurationInput = (formValues) => {
  const {
    ratingsMatrix: matrix1,
    spectralRefractionMatrix: matrix2,
    lat,
    lon,
    ...formVals
  } = coercePropsToNumberInverted(formValues, [
    'breakingWaveHeightAlgorithm',
    'ratingsMatrix',
    'spectralRefractionMatrix',
  ]);

  return {
    ...formVals,
    ratingsMatrix: stringToMatrix(matrix1, 7),
    spectralRefractionMatrix: stringToMatrix(matrix2, 18, 24),
  };
};

export const useGridPointUpdate = (pointOfInterestId, isASurfModel) => {
  const client = useApolloClient();
  const [updateStatus, setUpdateStatus] = useState(undefined);
  const { dataSource } = useParams();
  const [updateGridPoint, { loading: gridPointIsUpdating }] = useMutation(UpsertPOIGridPoints);
  const [updateSurfSpotConfiguration, { loading: surfSpotConfigurationIsUpdating }] = useMutation(
    UpsertSurfSpotConfiguration,
  );

  const [agency, model, grid] = dataSource.split('--');

  const update = async (formValues) => {
    const {
      pointOfInterest: { gridPoints },
    } = client.readQuery({
      query: QueryGridPoints,
      variables: { pointOfInterestId },
    });

    const gridPoint = gridPoints.find(
      ({ grid: gridDef }) =>
        gridDef.agency === agency && gridDef.model === model && gridDef.grid === grid,
    );

    const updatedPoint = createPOIGridPointInput(gridPoint, formValues);

    try {
      setUpdateStatus(undefined);

      const [gridPointUpdateResponse, surfSpotConfigurationUpdateResponse] = await Promise.all([
        updateGridPoint({
          variables: {
            pointOfInterestId,
            gridPoints: [updatedPoint],
          },
        }),
        isASurfModel
          ? updateSurfSpotConfiguration({
              variables: {
                pointOfInterestId,
                surfSpotConfiguration: createSurfSpotConfigurationInput(formValues),
              },
            })
          : null,
      ]);

      const {
        data: {
          upsertPointOfInterestGridPoints: { success: gridPointUpdateSuccess },
        },
      } = gridPointUpdateResponse;

      let surfSpotConfigurationIgnoredOrUpdateSuccessfully = true;
      if (isASurfModel) {
        const {
          data: {
            upsertSurfSpotConfiguration: { success },
          },
        } = surfSpotConfigurationUpdateResponse;
        surfSpotConfigurationIgnoredOrUpdateSuccessfully = success;
      }

      if (gridPointUpdateSuccess && surfSpotConfigurationIgnoredOrUpdateSuccessfully) {
        setUpdateStatus({
          success: true,
          message: 'Model updated',
        });
      } else {
        setUpdateStatus({
          success: false,
          message: 'Error updating model',
        });
      }
    } catch (error) {
      setUpdateStatus({
        success: false,
        message: 'Error updating model',
      });
    }
  };

  return {
    update,
    isUpdating: gridPointIsUpdating || surfSpotConfigurationIsUpdating,
    updateStatus,
  };
};

export const useGridPointRemove = (pointOfInterestId) => {
  const [removeStatus, setRemoveStatus] = useState(undefined);
  const [removeGridPoint, { loading: isRemoving }] = useMutation(RemovePointOfInterestGridPoints);
  const { dataSource } = useParams();
  const [agency, model, grid] = dataSource.split('--');

  const remove = async () => {
    try {
      setRemoveStatus(undefined);

      const {
        data: {
          removePointOfInterestGridPoints: { success },
        },
      } = await removeGridPoint({
        variables: {
          pointOfInterestId,
          gridPoints: [
            {
              agency,
              model,
              grid,
            },
          ],
        },
      });

      if (success) {
        setRemoveStatus({
          success: true,
        });
      } else {
        setRemoveStatus({
          success: false,
          message: 'Error deleting model',
        });
      }
    } catch (error) {
      setRemoveStatus({
        success: false,
        message: 'Error deleting model',
      });
    }
  };

  return {
    remove,
    isRemoving,
    removeStatus,
  };
};

/**
 * @description A custom hook used to memoize the input field values from the `EditGridPoint` component
 * which will be used as arguments to the breaking wave heights API call
 * @param {import('react-hook-form').FormContextValues["watch"]} watch
 */
export const useBreakingWaveHeightInputs = (watch) => {
  /** @type {string} */
  const optimalSwellDirection = watch('optimalSwellDirection');
  /** @type {string} */
  const breakingWaveHeightCoefficient = watch('breakingWaveHeightCoefficient');
  /** @type {string} */
  const breakingWaveHeightIntercept = watch('breakingWaveHeightIntercept');
  /** @type {string} */
  const breakingWaveHeightAlgorithm = watch('breakingWaveHeightAlgorithm');
  /** @type {string} */
  const spectralRefractionMatrix = watch('spectralRefractionMatrix');

  const memoizedInputs = useMemo(
    () => ({
      algorithm: breakingWaveHeightAlgorithm,
      breakingWaveHeightCoefficient: parseFloat(breakingWaveHeightCoefficient, 10),
      breakingWaveHeightIntercept: parseFloat(breakingWaveHeightIntercept, 10),
      optimalSwellDirection:
        optimalSwellDirection ?? false ? parseFloat(optimalSwellDirection) : undefined,
      spectralRefractionMatrix,
    }),
    [
      breakingWaveHeightAlgorithm,
      breakingWaveHeightCoefficient,
      breakingWaveHeightIntercept,
      optimalSwellDirection,
      spectralRefractionMatrix,
    ],
  );

  const [inputs, setInputs] = useState(memoizedInputs);

  useDebounce(() => setInputs(memoizedInputs), 1000, [memoizedInputs]);

  return inputs;
};
