import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Link, Redirect } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { Alert, Input, Button, Select, Checkbox } from '@surfline/quiver-react';
import {
  setValues,
  oppositeAngle,
  areAnglesOpposite,
  areValuesZero,
  validateOffshoreAndOptimalSwellDirections,
} from '../../../utils';
import Map from '../../PointOfInterestMap';
import { bwhaOptions } from '../../../constants';
import { usePrevious } from '../../../hooks';
import {
  useGridPointData,
  useGridPointUpdate,
  useGridPointRemove,
  useBreakingWaveHeightInputs,
} from './hooks';
import DirectionControl from '../../DirectionControl';
import { EditModelFooter } from '../../styles';
import MissingDataWarning from '../../MissingDataWarning';
import SpectralRefractionMatrixEdit from '../SpectralRefractionMatrixEdit';
import './EditGridPoint.scss';
import { validateStringAsNumberMatrixWithDimensions } from '../../../matrixUtils';

const EditGridPoint = () => {
  const {
    loading,
    error,
    data,
    isASurfModel,
    models,
    pointOfInterest,
    canPreviewSwell,
    agency,
    model,
    grid,
    modelNotFound,
    pointOfInterestId,
  } = useGridPointData();

  const { update, isUpdating, updateStatus } = useGridPointUpdate(pointOfInterestId, isASurfModel);
  const { remove, isRemoving, removeStatus } = useGridPointRemove(pointOfInterestId);
  const previousUpdateStatus = usePrevious(updateStatus ? updateStatus.succcess : undefined);
  const [showSwellPreview, setShowSwellPreview] = useState(false);
  const [coords, setCoords] = useState();
  const [showRefractionMatrixEdit, setShowRefractionMatrixEdit] = useState(false);

  const { register, getValues, setValue, errors, formState, watch, handleSubmit, reset } =
    useForm();

  useEffect(() => {
    if (!loading && data) {
      reset(data);

      const { lat, lon } = data;
      setCoords({ lat, lon });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  const onChange =
    (field) =>
    ({ target }) => {
      setCoords({ ...coords, [field]: target.value });
    };

  const swell = watch('optimalSwellDirection');
  const wind = watch('offshoreDirection');

  const [anglesLinked, setAnglesLinked] = useState(
    areAnglesOpposite(swell, wind) || areValuesZero(swell, wind),
  );

  useEffect(() => {
    if (!isASurfModel) return;
    register(
      { name: 'optimalSwellDirection' },
      {
        validate: (newOptimalSwellDirection) =>
          validateOffshoreAndOptimalSwellDirections(
            +getValues('offshoreDirection'),
            newOptimalSwellDirection,
          ),
      },
    );
    register(
      { name: 'offshoreDirection' },
      {
        validate: (newOffshoreDirection) =>
          validateOffshoreAndOptimalSwellDirections(
            newOffshoreDirection,
            +getValues('optimalSwellDirection'),
          ),
      },
    );
    register({ name: 'ratingsMatrix' });
    register('spectralRefractionMatrix', {
      validate: (value) =>
        validateStringAsNumberMatrixWithDimensions(value, 18, 24) ||
        'Spectral refraction matrix requires 24 rows with 18 entries in each row',
    });
  }, [isASurfModel, register, getValues]);

  useEffect(() => {
    const hasUpdated = !previousUpdateStatus && updateStatus && updateStatus.success;

    if (hasUpdated) {
      reset(getValues());
    }
  }, [previousUpdateStatus, updateStatus, getValues, reset]);

  const changeAngle = (type, angle) => {
    setValue(type, angle);

    if (anglesLinked) {
      const opposite = oppositeAngle(angle);

      if (type === 'optimalSwellDirection') {
        setValue('offshoreDirection', opposite);
      } else if (type === 'offshoreDirection') {
        setValue('optimalSwellDirection', opposite);
      }
    }
  };

  const handleMatrixChange = (event) => {
    setValue('spectralRefractionMatrix', event.target.value);
  };

  const breakingWaveHeightInputs = useBreakingWaveHeightInputs(watch);

  if (removeStatus && removeStatus.success) {
    return <Redirect to="." />;
  }

  if (!loading && modelNotFound) {
    return (
      <MissingDataWarning
        warning={`No such model ${agency}-${model}-${grid} for ${pointOfInterest.name}`}
        cta="View All Models"
        redirect="."
        alert="error"
      />
    );
  }

  if (loading || !coords) return <p>Loading...</p>;

  if (error) return <p>Error :(</p>;

  if (showRefractionMatrixEdit) {
    return (
      <SpectralRefractionMatrixEdit
        close={() => setShowRefractionMatrixEdit(false)}
        location={{
          lat: pointOfInterest.lat,
          lon: pointOfInterest.lon,
        }}
        matrix={data.spectralRefractionMatrix}
        name={pointOfInterest.name}
      />
    );
  }

  return (
    <div className="poi-tool-edit-grid-point">
      <Helmet>
        <title>
          {pointOfInterest.name} | {agency} - {model} - {grid} | POI CMS
        </title>
      </Helmet>
      <div className="poi-tool-edit-grid-point__header">
        <Link to="." className="poi-tool-edit-grid-point__header__back-btn">
          Show all models
        </Link>

        <h5>
          {agency} {model} {grid}
        </h5>

        <button
          type="button"
          disabled={!canPreviewSwell}
          className={`poi-tool__swell-preview-toggle ${
            canPreviewSwell ? 'poi-tool__swell-preview-toggle--available' : ''
          }`}
          onClick={() => setShowSwellPreview(!showSwellPreview)}
        >
          {canPreviewSwell
            ? `${showSwellPreview ? 'Show map' : 'Show swell preview'} `
            : 'swell preview unavailable'}
        </button>
      </div>
      <div className="poi-tool-edit-grid-point__content">
        <form>
          {['lat', 'lon'].map((field, index) =>
            index >= 2 && !isASurfModel ? null : (
              <Input
                key={field}
                id={field}
                label={field}
                type="text"
                validateDefault={false}
                defaultValue={`${getValues()[field]}`}
                error={errors[field]}
                message={errors[field] ? `Invalid or empty ${field}` : null}
                input={{
                  ref: register({
                    required: true,
                  }),
                  name: field,
                  onChange: onChange(field),
                }}
              />
            ),
          )}
          {isASurfModel && (
            <>
              <Input
                id="breakingWaveHeightCoefficient"
                label="breakingWaveHeightCoefficient"
                validateDefault={false}
                defaultValue={`${getValues().breakingWaveHeightCoefficient}`}
                error={errors.breakingWaveHeightCoefficient}
                message={
                  errors.breakingWaveHeightCoefficient
                    ? `Invalid or empty breakingWaveHeightCoefficient`
                    : null
                }
                input={{
                  ref: register,
                  name: 'breakingWaveHeightCoefficient',
                }}
              />

              <Input
                id="breakingWaveHeightIntercept"
                label="breakingWaveHeightIntercept"
                validateDefault={false}
                defaultValue={`${getValues().breakingWaveHeightIntercept}`}
                error={errors.breakingWaveHeightIntercept}
                message={
                  errors.breakingWaveHeightIntercept
                    ? `Invalid or empty breakingWaveHeightIntercept`
                    : null
                }
                input={{
                  ref: register,
                  name: 'breakingWaveHeightIntercept',
                }}
              />

              <Select
                key="breakingWaveHeightAlgorithm"
                id="breakingWaveHeightAlgorithm"
                label="breakingWaveHeightAlgorithm"
                defaultValue={getValues().breakingWaveHeightAlgorithm}
                error={errors.breakingWaveHeightAlgorithm}
                message={
                  errors.breakingWaveHeightAlgorithm ? `Invalid breakingWaveHeightAlgorithm` : null
                }
                select={{
                  ref: register,
                  name: 'breakingWaveHeightAlgorithm',
                }}
                options={[
                  {
                    text: '...',
                    value: '',
                  },
                  ...bwhaOptions.map((name) => ({ text: name, value: name })),
                ]}
                parentWidth
              />

              <section className="poi-tool__forecast-settings__data__form__section">
                <p className="section__label">Optimal Swell Direction</p>
                <DirectionControl
                  name="optimalSwellDirection"
                  label="Optimal Swell Direction"
                  value={swell}
                  onChange={(angle) => changeAngle('optimalSwellDirection', +angle)}
                />
              </section>

              <section className="poi-tool__forecast-settings__data__form__section">
                {/* eslint-disable-next-line react/self-closing-comp */}
                <p className="section__label"></p>
                <div>
                  <Checkbox
                    checked={anglesLinked}
                    onClick={() => setAnglesLinked(!anglesLinked)}
                    copy="Link swell & wind angle?"
                  />
                </div>
              </section>

              <section className="poi-tool__forecast-settings__data__form__section">
                <p className="section__label">Offshore Direction</p>
                <DirectionControl
                  name="offshoreDirection"
                  label="Offshore Direction"
                  value={wind}
                  onChange={(angle) => changeAngle('offshoreDirection', +angle)}
                />
              </section>

              <section className="poi-tool__forecast-settings__data__form__section">
                <p className="section__label">Spectral Refraction Matrix</p>
                <Button
                  style={{ marginBottom: '8px' }}
                  parentWidth
                  onClick={(e) => {
                    e.preventDefault();
                    setShowRefractionMatrixEdit(true);
                  }}
                >
                  Edit
                </Button>
                <textarea
                  id="spectralRefractionMatrix"
                  name="spectralRefractionMatrix"
                  defaultValue={data.spectralRefractionMatrix}
                  className="poi-tool__forecast-settings__refraction-matrix"
                  onChange={handleMatrixChange}
                />
              </section>
            </>
          )}
          {Object.keys(errors).length > 0 && (
            <Alert type="error">{Object.values(errors)[0].message}</Alert>
          )}
          {updateStatus && (
            <div className="poi-tool-edit-grid-point__content__msg">
              <Alert type={updateStatus.success ? 'success' : 'error'}>
                {updateStatus.message}
              </Alert>
            </div>
          )}
          {removeStatus && !removeStatus.success && (
            <div className="poi-tool-edit-grid-point__content__msg">
              <Alert type="error">{removeStatus.message}</Alert>
            </div>
          )}
          <EditModelFooter>
            <Button
              loading={isUpdating}
              disabled={!formState.dirtyFields.size}
              onClick={handleSubmit(update)}
            >
              Update
            </Button>

            <Button
              onClick={(event) => {
                event.preventDefault();
                remove();
              }}
              loading={isRemoving}
              style={{
                borderColor: 'LightCoral',
                backgroundColor: 'LightCoral',
              }}
            >
              Delete
            </Button>
          </EditModelFooter>
        </form>
        <Map
          gridParams={{ model: `${agency} ${model} ${grid}`, models, location: pointOfInterest }}
          gridPointLocation={coords}
          updateGridpointLocation={(values) => {
            setValues({ values, setValue, keys: ['lat', 'lon'] });
            setCoords({ ...coords, ...values });
          }}
          location={{
            lat: pointOfInterest.lat,
            lon: pointOfInterest.lon,
          }}
          showSwellPreview={showSwellPreview}
          swellProps={{
            agency,
            model,
            grid,
            lat: coords.lat,
            lon: coords.lon,
          }}
          breakingWaveHeightInputs={breakingWaveHeightInputs}
        />
      </div>
    </div>
  );
};

export default EditGridPoint;
