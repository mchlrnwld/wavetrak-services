import { useState } from 'react';
import { useQuery, useMutation } from '@apollo/client';
import { flattenGridPointData, filterSurfModels, filterSwellModels } from '../../../utils';
import { UpsertPOIDefaultForecastModels } from '../../../graphql/schema';
import { defaultForecastModelsQuery } from './schema';

const TYPE_SWELLS = 'SWELLS';
const TYPE_SURF = 'SURF';

const modelName = (model) => (model ? `${model.agency} ${model.model} ${model.grid.grid}` : '');

const modelDetails = (name) => {
  const [agency, model, grid] = name.split(' ');
  return { agency, model, grid };
};

export const useDefaultModelData = (pointOfInterestId) => {
  const { data, loading, error } = useQuery(defaultForecastModelsQuery, {
    variables: { pointOfInterestId },
  });

  if (loading || !data) {
    return {
      loading: true,
    };
  }

  const {
    models,
    pointOfInterest: { defaultForecastModels: defaultModels, gridPoints },
  } = data;

  const swellModel = defaultModels.find(({ forecastType }) => forecastType === TYPE_SWELLS);
  const surfModel = defaultModels.find(({ forecastType }) => forecastType === TYPE_SURF);

  const grids = flattenGridPointData(gridPoints);
  const availableSwellModels = filterSwellModels(models, grids);
  const availableSurfModels = filterSurfModels(models, grids);

  return {
    loading,
    error,
    availableSwellModels,
    availableSurfModels,
    defaultValues: {
      defaultSwellModel: modelName(swellModel),
      defaultSurfModel: modelName(surfModel),
    },
  };
};

export const useDefaultModelUpsert = (pointOfInterestId) => {
  const [upsertStatus, setUpsertStatus] = useState(undefined);
  const [upsertDefaultForecastModels, { loading: isUpserting }] = useMutation(
    UpsertPOIDefaultForecastModels,
  );

  const onSubmit = async ({ defaultSwellModel, defaultSurfModel }) => {
    setUpsertStatus(undefined);

    const PointOfInterestDefaultForecastModelInput = {
      pointOfInterestId,
      defaultForecastModels: [
        {
          forecastType: TYPE_SWELLS,
          ...modelDetails(defaultSwellModel),
        },
        {
          forecastType: TYPE_SURF,
          ...modelDetails(defaultSurfModel),
        },
      ],
    };

    try {
      const {
        data: {
          upsertPointOfInterestDefaultForecastModels: { success },
        },
      } = await upsertDefaultForecastModels({
        variables: PointOfInterestDefaultForecastModelInput,
      });

      setUpsertStatus({
        success,
        message: success ? 'default swell model updated' : 'Error updating default swell model',
      });
    } catch (error) {
      setUpsertStatus({
        success: false,
        message: 'Error updating default swell model',
      });
    }
  };

  return {
    upsertStatus,
    setUpsertStatus,
    isUpserting,
    onSubmit,
  };
};
