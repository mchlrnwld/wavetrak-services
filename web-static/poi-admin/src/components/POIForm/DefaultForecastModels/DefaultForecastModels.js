import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import { Select, Button, Alert } from '@surfline/quiver-react';
import { useDefaultModelData, useDefaultModelUpsert } from './hooks';

const DefaultForecastModels = ({ pointOfInterestId }) => {
  const { loading, error, defaultValues, availableSwellModels, availableSurfModels } =
    useDefaultModelData(pointOfInterestId);
  const { onSubmit, isUpserting, upsertStatus, setUpsertStatus } =
    useDefaultModelUpsert(pointOfInterestId);
  const { handleSubmit, register, getValues, formState, errors, reset } = useForm({
    mode: 'onBlur',
  });

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => setUpsertStatus(undefined), [errors.defaultSwellModel]);

  useEffect(() => {
    if (!loading) {
      reset(defaultValues);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading]);

  useEffect(() => {
    if (upsertStatus && upsertStatus.success) {
      reset(getValues());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [upsertStatus]);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  const options = (models) =>
    models.map(({ agency, model, grid }) => ({
      text: `${agency} ${model} ${grid}`,
      value: `${agency} ${model} ${grid}`,
    }));

  return (
    <>
      <form className="poi-tool__forecast-settings__data__form" onSubmit={handleSubmit(onSubmit)}>
        {upsertStatus && (
          <Alert type={upsertStatus.success ? 'success' : 'error'}>{upsertStatus.message}</Alert>
        )}
        <section className="poi-tool__forecast-settings__data__form__section">
          <p className="section__label">Default Swell Model</p>
          <Select
            parentWidth
            label="Default Swell Model"
            name="defaultSwellModel"
            error={errors.defaultSwellModel}
            message={errors.defaultSwellModel ? 'Please select a model' : ''}
            select={{
              name: 'defaultSwellModel',
              ref: register({ required: true, min: 1 }),
            }}
            options={[{ text: '...', value: '' }, ...options(availableSwellModels)]}
          />
        </section>
        <section className="poi-tool__forecast-settings__data__form__section">
          <p className="section__label">Default Surf Model</p>
          <Select
            parentWidth
            label="Default Surf Model"
            name="defaultSurfModel"
            error={errors.defaultSurfModel}
            message={errors.defaultSurfModel ? 'Please select a model' : ''}
            select={{
              name: 'defaultSurfModel',
              ref: register({ required: true, min: 1 }),
            }}
            options={[{ text: '...', value: '' }, ...options(availableSurfModels)]}
          />
        </section>
        <footer className="poi-tool__forecast-settings__data__form__footer">
          <Button disabled={!formState.dirtyFields.size} loading={isUpserting}>
            Save
          </Button>
        </footer>
      </form>
    </>
  );
};

DefaultForecastModels.propTypes = {
  pointOfInterestId: PropTypes.string.isRequired,
};

export default DefaultForecastModels;
