import { gql } from '@apollo/client';
import { GridPointFields, DefaultForecastModelFields } from '../../../graphql/schema';

export const defaultForecastModelsQuery = gql`
  query POI_CMS_getDefaultForecastModelsData($pointOfInterestId: String!) {
    pointOfInterest(pointOfInterestId: $pointOfInterestId) {
      pointOfInterestId
      ...GridPointFields
      ...DefaultForecastModelFields
    }
    models {
      agency
      model
      types
      grids {
        grid
      }
    }
  }
  ${DefaultForecastModelFields}
  ${GridPointFields}
`;
