import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { useQuery } from '@apollo/client';
import { useParams } from 'react-router-dom';
import { basicInfoQuery } from '../../../graphql/schema';
import TravelDetails from '../TravelDetails';
import MissingDataWarning from '../../MissingDataWarning';

/**
 * @typedef {object} Props
 * @property {boolean} brandSpotMissing
 * @property {string} redirectTo
 * @property {string} [surflineSpotId]
 * @property {string} [mswSpotId]
 */

/** @type {React.FC<Props>} */
const SpotInfo = ({ brandSpotMissing, redirectTo, surflineSpotId, mswSpotId }) => {
  const { id: pointOfInterestId } = useParams();
  const { data, loading, error } = useQuery(basicInfoQuery, {
    variables: { pointOfInterestId },
  });

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  const {
    pointOfInterest: { name },
  } = data;

  return (
    <>
      <Helmet>
        <title>{name} | Travel Info | POI CMS</title>
      </Helmet>
      {brandSpotMissing ? (
        <MissingDataWarning
          warning="A surfline spot must exist before adding additional spot info."
          cta="Create surfline spot"
          redirect={redirectTo}
        />
      ) : (
        <TravelDetails surflineSpotId={surflineSpotId} mswSpotId={mswSpotId} />
      )}
    </>
  );
};

SpotInfo.propTypes = {
  brandSpotMissing: PropTypes.bool.isRequired,
  redirectTo: PropTypes.string.isRequired,
  surflineSpotId: PropTypes.string,
  mswSpotId: PropTypes.string,
};

SpotInfo.defaultProps = {
  surflineSpotId: '',
  mswSpotId: '',
};

export default SpotInfo;
