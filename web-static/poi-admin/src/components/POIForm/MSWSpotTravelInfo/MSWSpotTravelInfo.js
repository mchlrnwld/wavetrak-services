import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { useParams } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import { Input, Select, Button, Alert } from '@surfline/quiver-react';
import { useTidePorts } from '../../../hooks/remoteDataHooks';
import RadioControl from '../../RadioControl';
import CheckboxControl from '../../CheckboxControl';
import { ratingTypes } from '../../../constants';

import { useMSWSpotData, useMSWSpotUpsert } from './hooks';
import Map from '../../PointOfInterestMap';
import './MSWSpotTravelInfo.scss';
import { requiredDataSourceModels, dataSourcesCheck } from '../../../utils/dataSourceValidation';
import {
  requiredModelTypes,
  defaultForecastModelCheck,
} from '../../../utils/forecastSettingModelValidation';

/**
 * @typedef {object} Props
 * @property {string} [surflineSpotId]
 * @property {string} [mswSpotId]
 */

/** @type {React.FC<Props>} */
const MSWSpotTravelInfo = ({ surflineSpotId, mswSpotId }) => {
  const { id: pointOfInterestId } = useParams();
  const {
    loading,
    error,
    spotExists,
    lat,
    lon,
    mswSurfArea,
    mswCountry,
    mswSpotType,
    allTimezones,
    spotData,
    poiData,
  } = useMSWSpotData(pointOfInterestId);

  let requiredDataSources = { valid: false };
  let requiredForecastModelTypes = { valid: false };
  if (poiData) {
    const { defaultForecastModels, gridPoints } = poiData;
    requiredDataSources = dataSourcesCheck(gridPoints, requiredDataSourceModels);
    requiredForecastModelTypes = defaultForecastModelCheck(
      defaultForecastModels,
      requiredModelTypes,
    );
  }
  const [coords, setCoords] = useState({ lat, lon });

  const { register, errors, control, handleSubmit, getValues, setValue, reset, formState } =
    useForm({
      mode: 'onBlur',
    });

  const { dirtyFields } = formState;

  const {
    nearestTidePorts,
    loading: tidePortsLoading,
    error: tidePortsError,
  } = useTidePorts(coords);

  const { upsert, upsertStatus, isUpserting } = useMSWSpotUpsert(pointOfInterestId);

  useEffect(() => {
    register({ name: 'types' });
    register({ name: 'topLevelNav' });
    register({ name: 'hidden' });
  }, [register]);

  useEffect(() => setCoords({ lat, lon }), [lat, lon]);

  useEffect(() => {
    if (spotExists) {
      reset(spotData);
    } else if (poiData) {
      reset({
        name: poiData.name,
        lat: poiData.lat,
        lon: poiData.lon,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [spotExists]);

  const hasError = (field) => errors[field] !== undefined;
  const getError = (field) => (hasError(field) ? errors[field].message : '');

  if (loading || tidePortsLoading) return <p>Loading.. </p>;
  if (error || tidePortsError) return <p>Error: {error?.message || tidePortsError}</p>;

  return (
    <section className="poi-tool__travel-info__content">
      <Helmet>
        <title>{poiData.name} | MSW Spot | POI CMS</title>
      </Helmet>
      <form className="poi-tool__msw-spot-data" onSubmit={handleSubmit(upsert)}>
        {!spotExists && (
          <Alert
            type="warning"
            style={{
              marginBottom: '10px',
            }}
          >
            A magicseaweed spot does not yet exist for this point of interest. Fill out the form
            below to create it.
          </Alert>
        )}
        {!requiredDataSources.valid && (
          <section className="poi-tool__forecast-settings__data__form__status">
            <Alert type="warning">
              Missing required <a href="../data-sources">data source models</a>:
              <ul>
                {requiredDataSources.requiredModelsNotFound.map((model) => (
                  <li>{model}</li>
                ))}
              </ul>
              You will not be able to create or update until resolved.
            </Alert>
          </section>
        )}
        {!requiredForecastModelTypes.valid && (
          <section className="poi-tool__forecast-settings__data__form__status">
            <Alert type="warning">
              Missing required <a href="../forecast-settings">forecast model settings</a>:
              <ul>
                {requiredForecastModelTypes.requiredForecastModelsNotFound.map((model) => (
                  <li>{model}</li>
                ))}
              </ul>
              You will not be able to create or update until resolved.
            </Alert>
          </section>
        )}
        {surflineSpotId && (
          <p>
            <strong>Surfline Spot ID:</strong> {surflineSpotId}
          </p>
        )}
        {mswSpotId && (
          <p>
            <strong>MSW Spot ID:</strong> {mswSpotId}
          </p>
        )}
        <Input
          type="text"
          id="name"
          label="Name"
          error={hasError('name')}
          message={getError('name')}
          defaultValue={getValues().name}
          validateDefault={false}
          input={{
            ref: register({ required: 'Enter a valid name' }),
            name: 'name',
          }}
        />

        <Input
          type="number"
          id="lat"
          label="lat"
          error={hasError('lat')}
          message={getError('lat')}
          defaultValue={getValues().lat}
          validateDefault={false}
          input={{
            ref: register({ required: 'Enter a valid lat' }),
            name: 'lat',
            step: 'any',
            onChange: (event) => {
              setCoords({ ...coords, lat: +event.target.value });
            },
          }}
        />

        <Input
          type="number"
          id="lon"
          label="lon"
          error={hasError('lon')}
          message={getError('lon')}
          defaultValue={getValues().lon}
          validateDefault={false}
          input={{
            ref: register({ required: 'Enter a valid lon' }),
            name: 'lon',
            step: 'any',
            onChange: (event) => {
              setCoords({ ...coords, lon: +event.target.value });
            },
          }}
        />

        <Input
          type="text"
          id="surflineSpotId"
          label="Surfline Spot ID"
          error={hasError('surflineSpotId')}
          message={getError('surflineSpotId')}
          defaultValue={surflineSpotId}
          validateDefault={false}
          input={{
            ref: register,
            name: 'surflineSpotId',
          }}
        />

        <Controller
          control={control}
          name="types"
          as={<CheckboxControl />}
          label="Spot Types"
          options={mswSpotType}
          values={spotData?.types}
          error={hasError('types')}
          message={getError('types')}
        />

        <Controller
          control={control}
          as={<RadioControl />}
          name="topLevelNav"
          choice={spotData?.topLevelNav}
          label="Show in Nav"
          options={['yes', 'no']}
        />

        <Controller
          control={control}
          as={<RadioControl />}
          name="hidden"
          label="Hidden"
          choice={spotData?.hidden}
          error={hasError('hidden')}
          message={getError('hidden')}
          options={['yes', 'no']}
        />

        {nearestTidePorts.length > 1 && (
          <Select
            parentWidth
            id="tidalPort"
            name="tidalPort"
            label="Tidal Port"
            select={{
              ref: register,
              name: 'tidalPort',
            }}
            options={nearestTidePorts}
            error={hasError('tidalPort')}
            message={getError('tidalPort')}
          />
        )}

        <Select
          parentWidth
          id="surfAreaId"
          name="surfAreaId"
          label="Surf Area"
          error={hasError('surfAreaId')}
          message={getError('surfAreaId')}
          select={{
            name: 'surfAreaId',
            ref: register({ required: 'Please select a surfarea' }),
            onChange: (event) => {
              const surfArea = mswSurfArea.find(({ _id }) => +event.target.value === _id);
              setValue('countryId', surfArea.countryId);
              setValue('timezone', surfArea.timezone.replace(' ', '_'));
            },
          }}
          defaultValue={`${getValues().surfAreaId}`}
          options={[
            { text: 'Please Select A surfArea', value: '' },
            ...mswSurfArea.map(({ _id, name }) => ({
              text: name,
              value: _id,
            })),
          ]}
        />

        {allTimezones && allTimezones.length > 0 && (
          <Select
            parentWidth
            id="timezone"
            name="timezone"
            label="Timezone"
            error={hasError('timezone')}
            message={getError('timezone')}
            select={{
              name: 'timezone',
              ref: register({ required: 'Please select a timezone' }),
            }}
            defaultValue={getValues().timezone}
            options={allTimezones}
          />
        )}

        <Select
          parentWidth
          id="countryId"
          name="countryId"
          label="Country"
          error={hasError('countryId')}
          message={getError('countryId')}
          select={{
            name: 'countryId',
            ref: register({ required: 'Please select a country' }),
          }}
          defaultValue={`${getValues().countryId}`}
          options={[
            { text: 'Please Select A Country', value: '' },
            ...mswCountry.map(({ _id, name }) => ({
              text: name,
              value: _id,
            })),
          ]}
        />

        <Select
          parentWidth
          id="ratingType"
          name="ratingType"
          label="Rating Type"
          select={{
            ref: register({ required: 'Select a rating type' }),
            name: 'ratingType',
          }}
          options={ratingTypes.map((name) => ({
            text: name,
            value: name,
          }))}
          defaultValue={getValues().ratingType}
          error={hasError('ratingType')}
          message={getError('ratingType')}
        />

        {upsertStatus && (
          <section className="poi-tool__forecast-settings__data__form__status">
            <Alert type={upsertStatus.success ? 'success' : 'error'}>{upsertStatus.message}</Alert>
          </section>
        )}

        <Button
          parentWidth
          disabled={
            !dirtyFields.size ||
            !requiredDataSources.valid ||
            !requiredForecastModelTypes.valid ||
            loading ||
            tidePortsLoading
          }
          loading={isUpserting}
        >
          {spotExists ? 'Update' : 'Create'}
        </Button>
      </form>
      <Map
        location={coords}
        updateLocation={({ lat: newLat, lon: newLon }) => {
          setValue('lat', newLat);
          setValue('lon', newLon);
          setCoords({ lat: newLat, lon: newLon });
        }}
      />
    </section>
  );
};

MSWSpotTravelInfo.propTypes = {
  surflineSpotId: PropTypes.string,
  mswSpotId: PropTypes.string,
};

MSWSpotTravelInfo.defaultProps = {
  surflineSpotId: '',
  mswSpotId: '',
};

export default MSWSpotTravelInfo;
