import { gql } from '@apollo/client';
import {
  MSWSpotFields,
  GridPointFields,
  DefaultForecastModelFields,
} from '../../../graphql/schema';

export const mswSpotData = gql`
  query POI_CMS_getMSWSpotTravelInfo($pointOfInterestId: String!) {
    pointOfInterest(pointOfInterestId: $pointOfInterestId) {
      pointOfInterestId
      name
      lat
      lon
      ...GridPointFields
      ...DefaultForecastModelFields
      spots {
        ...MSWSpotFields
      }
    }
    mswSurfArea {
      _id
      name
      timezone
      countryId
    }
    mswCountry {
      _id
      name
    }
    mswSpotType {
      _id
      name
    }
  }
  ${GridPointFields}
  ${DefaultForecastModelFields}
  ${MSWSpotFields}
`;
