import { useState } from 'react';
import { useQuery, useApolloClient, useMutation } from '@apollo/client';
import { mswSpotData } from './schema';
import { mswSpotFormValues } from './utils';
import { useTimezones } from '../../../hooks/remoteDataHooks';
import { createMSWSpotInput } from '../../../graphql/utils';
import { upsertMSWSpot } from '../../../graphql/schema';

export const useMSWSpotData = (pointOfInterestId) => {
  const {
    data: mswData,
    loading,
    error,
  } = useQuery(mswSpotData, {
    variables: { pointOfInterestId },
  });

  const { timezones } = useTimezones();

  const spots = mswData?.pointOfInterest.spots.filter(({ brand }) => brand === 'MSW');
  const spotExists = spots && !!spots[0];

  // Returns an object of lat & lon
  // values for either the MSW spot if it exists
  // or the POI, if no MSW spot exists
  const getLatLon = () => {
    if (spotExists) {
      const [{ lat, lon }] = spots;
      return { lat, lon };
    }

    return {
      lat: mswData?.pointOfInterest.lat,
      lon: mswData?.pointOfInterest.lon,
    };
  };

  const getSpotData = () =>
    spotExists ? mswSpotFormValues(spots[0], mswData?.mswSpotType) : undefined;

  const getPOIData = () =>
    mswData
      ? {
          name: mswData.pointOfInterest.name,
          lat: mswData.pointOfInterest.lat,
          lon: mswData.pointOfInterest.lon,
          gridPoints: mswData.pointOfInterest.gridPoints,
          defaultForecastModels: mswData.pointOfInterest.defaultForecastModels,
        }
      : undefined;

  return {
    loading,
    error,
    spotExists,
    ...getLatLon(),
    mswSurfArea: mswData?.mswSurfArea,
    mswCountry: mswData?.mswCountry,
    mswSpotType: mswData?.mswSpotType,
    allTimezones: timezones,
    spotData: getSpotData(),
    poiData: getPOIData(),
  };
};

export const useMSWSpotUpsert = (pointOfInterestId) => {
  const client = useApolloClient();
  const [upsertStatus, setUpsertStatus] = useState();
  const [upsertSpot, { loading: isUpserting }] = useMutation(upsertMSWSpot);

  const upsert = async (formValues) => {
    try {
      setUpsertStatus(undefined);

      const {
        pointOfInterest: { spots },
      } = client.readQuery({
        query: mswSpotData,
        variables: { pointOfInterestId },
      });

      const [cachedSpot] = spots.filter(({ brand }) => brand === 'MSW');
      const updatedSpot = createMSWSpotInput(pointOfInterestId, cachedSpot, formValues);

      const {
        data: { upsertMSWSpot: upsertResult },
      } = await upsertSpot({ variables: { spot: updatedSpot } });

      const success = upsertResult.success || !upsertResult.errors.length;

      if (success) {
        setUpsertStatus({
          success: true,
          message: upsertResult.message,
        });
      } else {
        setUpsertStatus({
          success: false,
          message: upsertResult.errors?.map(({ message }) => message).join(' ; '),
        });
      }
    } catch (error) {
      setUpsertStatus({
        success: false,
        message: error,
      });
    }
  };

  return {
    upsert,
    upsertStatus,
    isUpserting,
  };
};
