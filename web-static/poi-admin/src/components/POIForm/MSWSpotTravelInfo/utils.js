export const mswSpotFormValues = (spot, mswSpotType) => {
  const {
    name: spotName,
    lat: spotLat,
    lon: spotLon,
    hidden,
    topLevelNav,
    surflineSpotId,
    types,
    tidalPort,
    timezone,
    surfArea,
    country,
    ratingType,
  } = spot;

  const spotTypeIds = types ? types.map(({ _id }) => _id) : [];

  // hidden & topLevelNav properties need to be converted from booleans to strings
  // as react hook form doesn't handle boolean values well (with radio buttons at least)
  return {
    name: spotName,
    lat: `${spotLat}`,
    lon: `${spotLon}`,
    hidden: hidden ? 'yes' : 'no',
    topLevelNav: topLevelNav ? 'yes' : 'no',
    surflineSpotId: surflineSpotId || '',
    types: mswSpotType.filter(({ _id }) => spotTypeIds.includes(_id)),
    tidalPort: tidalPort || '',
    timezone,
    surfAreaId: `${surfArea._id}`,
    countryId: `${country._id}`,
    ratingType,
  };
};
