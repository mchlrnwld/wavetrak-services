import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { useQuery } from '@apollo/client';
import { useParams, Link, useRouteMatch } from 'react-router-dom';
import { Button } from '@surfline/quiver-react';
import { isSurfModel, noSlash } from '../../../utils';
import GetDataSources from './schema';
import MissingDataWarning from '../../MissingDataWarning';
import './ModelTable.scss';

const ModelTable = ({ redirectTo }) => {
  const { id: pointOfInterestId } = useParams();
  const { data, loading, error } = useQuery(GetDataSources, {
    variables: { pointOfInterestId },
  });

  const { url } = useRouteMatch();

  if (loading || !data) return <p>Loading.. </p>;
  if (error) return <p>Error :(</p>;

  const { gridPoints, surfSpotConfiguration } = data.pointOfInterest;

  return (
    <>
      <Helmet>
        <title>{data.pointOfInterest.name} | Data Sources | POI CMS</title>
      </Helmet>
      {!gridPoints.length ? (
        <MissingDataWarning
          warning="This point of interest has no models yet."
          cta="Add Model"
          redirect={redirectTo}
        />
      ) : (
        <>
          <Link to={`${noSlash(url)}/new`}>
            <Button
              type="info"
              style={{
                height: 'auto',
                width: 'auto',
                margin: '0 auto 15px auto',
                padding: '10px 15px',
              }}
            >
              New Model
            </Button>
          </Link>
          <div className="poi-tool-model-table">
            <div className="table-row table-row--header">
              <div className="table-cell">Agency</div>
              <div className="table-cell">Model</div>
              <div className="table-cell">Grid</div>
              <div className="table-cell">Lat</div>
              <div className="table-cell">Lon</div>
              <div className="table-cell">BWH Algoridivm</div>
              <div className="table-cell">BWH Coefficient</div>
              <div className="table-cell">BWH Intercept</div>
              <div className="table-cell">Offshore Dir</div>
              <div className="table-cell">Optimal Swell Dir</div>
            </div>
            {gridPoints.map((gridPoint) => {
              const point = { ...gridPoint, ...gridPoint.grid };
              const isASurfModel = isSurfModel(data.models, point);
              const { agency, model, grid, lat, lon } = point;
              const {
                breakingWaveHeightAlgorithm,
                breakingWaveHeightCoefficient,
                breakingWaveHeightIntercept,
                offshoreDirection,
                optimalSwellDirection,
              } = surfSpotConfiguration || {};

              const key = `${agency}--${model}--${grid}`;

              return (
                <Link key={key} to={`${noSlash(url)}/${key}`} className="table-row">
                  <div className="table-cell">{agency}</div>
                  <div className="table-cell">{model}</div>
                  <div className="table-cell">{grid}</div>
                  <div className="table-cell">{lat}</div>
                  <div className="table-cell">{lon}</div>
                  <div className="table-cell">
                    {isASurfModel ? breakingWaveHeightAlgorithm : 'n/a'}
                  </div>
                  <div className="table-cell">
                    {isASurfModel ? breakingWaveHeightCoefficient : 'n/a'}
                  </div>
                  <div className="table-cell">
                    {isASurfModel ? breakingWaveHeightIntercept : 'n/a'}
                  </div>
                  <div className="table-cell">{isASurfModel ? offshoreDirection : 'n/a'}</div>
                  <div className="table-cell">{isASurfModel ? optimalSwellDirection : 'n/a'}</div>
                </Link>
              );
            })}
          </div>
        </>
      )}
    </>
  );
};

ModelTable.propTypes = {
  redirectTo: PropTypes.string.isRequired,
};

export default ModelTable;
