import { gql } from '@apollo/client';

const GetDataSources = gql`
  query POI_CMS_getDataSources($pointOfInterestId: String!) {
    pointOfInterest(pointOfInterestId: $pointOfInterestId) {
      pointOfInterestId
      name
      lat
      lon
      gridPoints {
        grid {
          agency
          model
          grid
          startLatitude
          endLatitude
          startLongitude
          endLongitude
          resolution
        }
        lat
        lon
      }
      surfSpotConfiguration {
        offshoreDirection
        optimalSwellDirection
        breakingWaveHeightAlgorithm
        breakingWaveHeightIntercept
        breakingWaveHeightCoefficient
        spectralRefractionMatrix
      }
    }
    models {
      agency
      model
      types
      grids {
        grid
        startLatitude
        endLatitude
        startLongitude
        endLongitude
        resolution
      }
    }
  }
`;

export default GetDataSources;
