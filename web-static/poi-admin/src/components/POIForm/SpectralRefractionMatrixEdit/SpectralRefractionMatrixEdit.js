import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { Button } from '@surfline/quiver-react';
import { Grid, LeftRail, RightRailTop, RightRailBottom, ErrorMessage } from './styles';
import SpectralRefractionMap from './SpectralRefractionMap';
import SpectralRefractionMatrixTable from './SpectralRefractionMatrixTable';
import { generateBreakingWaveHeightsGraphicData } from './SpectralRefractionOperations/utils';
import { stringToMatrix } from '../../../matrixUtils';

export const DEFAULT_BWH_STATE = {
  data: false,
  error: false,
};

/**
 * @typedef {object} Props
 * @property {() => void} close
 * @property {string} matrix
 * @property {{ lat: number, lon: number }} location
 */

/** @type {React.FunctionComponent<Props>} */
const SpectralRefractionMatrixEdit = ({ close, matrix, name, location }) => {
  const [breakingWaveHeights, setBreakingWaveHeights] = useState(DEFAULT_BWH_STATE);
  const [editingBreakingWaveHeights, setEditingBreakingWaveHeights] = useState(DEFAULT_BWH_STATE);
  const [graphicLoading, setGraphicLoading] = useState(false);

  const errorMessage =
    'There was an error generating the breaking wave height chart - please refresh the page and try again in a few minutes.';

  const fetchBWHs = useCallback(async () => {
    const bwh = await generateBreakingWaveHeightsGraphicData(stringToMatrix(matrix));
    setBreakingWaveHeights(bwh);
    setEditingBreakingWaveHeights(bwh);
  }, [matrix]);

  if (!breakingWaveHeights.data && !breakingWaveHeights.error) {
    fetchBWHs();
  }

  return (
    <>
      <Button onClick={close} parentWidth style={{ marginBottom: 8 }}>
        CLOSE
      </Button>
      <Grid>
        <LeftRail>
          <SpectralRefractionMatrixTable
            matrix={matrix}
            name={name}
            graphicVersion="adjusted-matrix"
            setEditingBreakingWaveHeights={setEditingBreakingWaveHeights}
            setGraphicLoading={setGraphicLoading}
            graphicLoading={graphicLoading || !breakingWaveHeights.data}
          />
        </LeftRail>
        <RightRailTop>
          <h5>Adjusted Matrix</h5>
          <SpectralRefractionMap
            location={location}
            graphicVersion="adjusted-matrix"
            breakingWaveHeights={
              !editingBreakingWaveHeights.error && editingBreakingWaveHeights.data
            }
            graphicLoading={graphicLoading || !editingBreakingWaveHeights.data}
          />
          {editingBreakingWaveHeights.error && <ErrorMessage>{errorMessage}</ErrorMessage>}
        </RightRailTop>
        <RightRailBottom>
          <h5>Default Matrix</h5>
          <SpectralRefractionMap
            location={location}
            graphicVersion="default-matrix"
            breakingWaveHeights={!breakingWaveHeights.error && breakingWaveHeights.data}
            graphicLoading={!breakingWaveHeights.data}
          />
          {breakingWaveHeights.error && <ErrorMessage>{errorMessage}</ErrorMessage>}
        </RightRailBottom>
      </Grid>
    </>
  );
};

SpectralRefractionMatrixEdit.propTypes = {
  close: PropTypes.func.isRequired,
  matrix: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  location: PropTypes.shape({ lat: PropTypes.number.isRequired, lon: PropTypes.number.isRequired })
    .isRequired,
};

export default SpectralRefractionMatrixEdit;
