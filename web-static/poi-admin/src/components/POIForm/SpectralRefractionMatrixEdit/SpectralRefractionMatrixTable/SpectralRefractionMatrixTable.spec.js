import React from 'react';
import { expect } from '@jest/globals';
import { mount } from 'enzyme';
import { Button, Input } from '@surfline/quiver-react';
import { act } from 'react-dom/test-utils';

import SpectralRefractionMatrixTable from './SpectralRefractionMatrixTable';
import { Table, Cell, ColumnHeader, BtnWrapper, ButtonHeader, RowBtnWrapper } from './styles';
import { matrix } from '../fixtures/matrix';

describe('SpectralRefractionMatrixTable', () => {
  it('Should render the table', () => {
    const wrapper = mount(
      <SpectralRefractionMatrixTable matrix={matrix} name="Santa Monica Beach South" />,
    );

    expect(wrapper.find(Table)).toHaveLength(1);
    expect(wrapper.find(ColumnHeader)).toHaveLength(1);
    expect(wrapper.find(ButtonHeader)).toHaveLength(42);
    expect(wrapper.find(BtnWrapper)).toHaveLength(36);
    expect(wrapper.find(RowBtnWrapper)).toHaveLength(48);
    expect(wrapper.find(Cell)).toHaveLength(432);
    act(() => {
      wrapper.find(Button).first().prop('onClick')();
    });
  });

  it('Should render Input for cell editing on double click', () => {
    const wrapper = mount(
      <SpectralRefractionMatrixTable matrix={matrix} name="Santa Monica Beach South" />,
    );

    const cell = wrapper.find(Cell).first();
    act(() => {
      cell.props().onDoubleClick();
      wrapper.setProps({ update: true });
      wrapper.update();
    });

    const editCell = wrapper.find(Input);

    act(() => {
      editCell.props().input.onKeyPress({ key: 'Enter', target: { value: 10 } });
      wrapper.setProps({ update: true });
      wrapper.update();
    });

    expect(wrapper.find(Cell).first().prop('children')).toBe('10');
  });
});
