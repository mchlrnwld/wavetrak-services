const LOWER_TABLE_VALUE_LIMIT = 0.005;

export const roundToThree = (val) => Math.round(val * 1000) / 1000;

const increment = (val) => roundToThree(val * 1.1);
const decrement = (val) => roundToThree(val * 0.9);

export const addRow = (table, rowIndex) =>
  table[rowIndex].map((val) => (val > 0 ? increment(val) : val));

export const subtractRow = (table, rowIndex) =>
  table[rowIndex].map((val) => (val > LOWER_TABLE_VALUE_LIMIT ? decrement(val) : 0));

export const addColumn = (table, columnIndex) =>
  table.map((row) => {
    const temprow = [...row];
    if (temprow[columnIndex] > 0) {
      temprow[columnIndex] = increment(temprow[columnIndex]);
    }
    return temprow;
  });

export const subtractColumn = (table, columnIndex) =>
  table.map((row) => {
    const temprow = [...row];
    if (temprow[columnIndex] > LOWER_TABLE_VALUE_LIMIT) {
      temprow[columnIndex] = decrement(temprow[columnIndex]);
    } else {
      temprow[columnIndex] = 0;
    }
    return temprow;
  });

export const getTableHeaders = () => {
  const periods = [24, 22, 20, 18, 16, 15, 14, 12, 11, 10, 9, 8, 7.5, 7, 6.5, 6, 5.2, 4.7];
  const directions = [];

  let i = 0;
  while (i < 360) {
    directions.push((i += 15));
  }
  return { periods, directions };
};

export const getMedian = (matrix) => {
  const values = matrix.flat().sort();
  return values[Math.floor(values.length / 2)];
};

// https://stackoverflowcolor.com/questions/15689845/rgb-to-hex-conversion
function componentToHex(c) {
  const hex = c.toString(16);
  return hex.length === 1 ? `0${hex}` : hex;
}

function rgbToHex({ red, green, blue }) {
  return `#${componentToHex(red)}${componentToHex(green)}${componentToHex(blue)}`;
}

export const getColor = (median, val) => {
  const minColor = {
    red: 248,
    green: 105,
    blue: 107,
  };
  const midColor = {
    red: 252,
    green: 252,
    blue: 255,
  };
  const maxColor = {
    red: 90,
    green: 138,
    blue: 198,
  };

  if (val <= 0) {
    return rgbToHex(minColor);
  }
  if (val === median) {
    return rgbToHex(midColor);
  }

  if (val >= 1) {
    return rgbToHex(maxColor);
  }

  const upperColor = val > median ? maxColor : midColor;
  const lowerColor = val > median ? midColor : minColor;
  const percent = val > median ? (val - median) / (1 - median) : val / median;

  return rgbToHex({
    red: Math.floor(lowerColor.red + percent * (upperColor.red - lowerColor.red)),
    green: Math.floor(lowerColor.green + percent * (upperColor.green - lowerColor.green)),
    blue: Math.floor(lowerColor.blue + percent * (upperColor.blue - lowerColor.blue)),
  });
};
