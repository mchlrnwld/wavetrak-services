import React, { useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Chevron, Input } from '@surfline/quiver-react';
import { stringToMatrix, matrixToString } from '../../../../matrixUtils';
import {
  addRow,
  addColumn,
  subtractRow,
  subtractColumn,
  getTableHeaders,
  getMedian,
  getColor,
  roundToThree,
} from './utils';
import {
  XAxisLabel,
  YAxisLabel,
  Grid,
  Table,
  SpectralRefractionOperationButtons,
  Cell,
  ColumnLabel,
  RowLabel,
  ColumnHeader,
  RowHeader,
  BtnWrapper,
  ButtonHeader,
  RowBtnWrapper,
} from './styles';
import SpectralRefractionOperations from '../SpectralRefractionOperations';

/**
 * @typedef {object} Props
 * @property {string} matrix
 * @property {string} name
 * @property {string} graphicVersion
 * @property {function} setEditingBreakingWaveHeights
 * @property {function} setGraphicLoading
 * @property {boolean} graphicLoading
 */

/** @type {React.FunctionComponent<Props>} */
const SpectralRefractionMatrixTable = ({
  matrix,
  name,
  graphicVersion,
  setEditingBreakingWaveHeights,
  setGraphicLoading,
  graphicLoading,
}) => {
  const { periods, directions } = getTableHeaders();

  // convert to array of arrays
  // eslint-disable-next-line no-unused-vars
  const refractionMatrix = stringToMatrix(matrix, periods.length, directions.length);

  const [editableMatrix, setEditableMatrix] = useState(refractionMatrix);
  const [median, setMedian] = useState(getMedian(editableMatrix));

  const [editing, setEditing] = useState(false);
  const clearChanges = () => setEditableMatrix(refractionMatrix);
  const saveChanges = () => {
    localStorage.removeItem(`${window.location.pathname}`);
    localStorage.setItem(`${window.location.pathname}`, matrixToString(editableMatrix));
  };
  const [importChanges, setImportChanges] = useState(
    localStorage.getItem(window.location.pathname),
  );

  useMemo(() => {
    if (importChanges === false) {
      setImportChanges(localStorage.getItem(window.location.pathname));
    }
  }, [importChanges, setImportChanges]);

  useMemo(() => {
    setMedian(getMedian(editableMatrix));
  }, [editableMatrix]);

  return (
    <Grid>
      <XAxisLabel>
        {importChanges && (
          <Button
            onClick={() => {
              setImportChanges(false);
              setEditableMatrix(stringToMatrix(importChanges));
            }}
            parentWidth
            style={{ marginBottom: 8 }}
          >
            import working changes
          </Button>
        )}
        Period
      </XAxisLabel>
      <YAxisLabel>Direction</YAxisLabel>
      <Table>
        <thead>
          <ColumnHeader>
            {/* two empty column headers above direction and control columns */}
            <td />
            <td />
            {periods.map((period) => (
              <ColumnLabel key={period}>{period}</ColumnLabel>
            ))}
          </ColumnHeader>
          <tr>
            <td />
            <td />
            {periods.map((val, index) => (
              <ButtonHeader key={`${val} button`}>
                <BtnWrapper>
                  <Button
                    onClick={() => {
                      const tableValues = addColumn(editableMatrix, index);
                      setEditableMatrix(tableValues);
                    }}
                  >
                    <Chevron direction="up" />
                  </Button>
                </BtnWrapper>
                <BtnWrapper>
                  <Button
                    onClick={() => {
                      const tableValues = subtractColumn(editableMatrix, index);
                      setEditableMatrix(tableValues);
                    }}
                  >
                    <Chevron direction="down" />
                  </Button>
                </BtnWrapper>
              </ButtonHeader>
            ))}
          </tr>
        </thead>
        <tbody>
          {editableMatrix.map((row, index) => (
            <RowHeader key={directions[index]}>
              <RowLabel>{directions[index]}</RowLabel>
              <ButtonHeader>
                <RowBtnWrapper>
                  <Button
                    onClick={() => {
                      const tableValues = [...editableMatrix];
                      tableValues[index] = addRow(editableMatrix, index);
                      setEditableMatrix(tableValues);
                    }}
                  >
                    <Chevron direction="up" />
                  </Button>
                </RowBtnWrapper>
                <RowBtnWrapper>
                  <Button
                    onClick={() => {
                      const tableValues = [...editableMatrix];
                      tableValues[index] = subtractRow(editableMatrix, index);
                      setEditableMatrix(tableValues);
                    }}
                  >
                    <Chevron direction="down" />
                  </Button>
                </RowBtnWrapper>
              </ButtonHeader>
              {row.map((val, i) => (
                <Cell
                  key={`${index * periods.length + i} Cell`}
                  onDoubleClick={() => {
                    setEditing(`${index * periods.length + i} Cell`);
                  }}
                  color={getColor(median, val)}
                >
                  {editing !== `${index * periods.length + i} Cell` ? (
                    `${roundToThree(val)}`
                  ) : (
                    <Input
                      id="val"
                      label=""
                      type="number"
                      defaultValue={editableMatrix[index][i]}
                      validateDefault={false}
                      style={{ minWidth: '100px' }}
                      input={{
                        onKeyPress: (event) => {
                          if (event.key === 'Enter') {
                            setEditing(false);
                            const temp = editableMatrix[index];
                            temp[i] = +event.target.value;
                            if (temp[i] < 0) {
                              temp[i] = 0;
                            }
                            const newMatrix = [...editableMatrix];
                            newMatrix[index] = temp;
                            setEditableMatrix(newMatrix);
                          }
                        },
                      }}
                    />
                  )}
                </Cell>
              ))}
            </RowHeader>
          ))}
        </tbody>
      </Table>
      <SpectralRefractionOperationButtons>
        <SpectralRefractionOperations
          matrix={editableMatrix}
          poiName={name}
          clearChanges={clearChanges}
          saveChanges={saveChanges}
          graphicVersion={graphicVersion}
          setEditingBreakingWaveHeights={setEditingBreakingWaveHeights}
          setGraphicLoading={setGraphicLoading}
          graphicLoading={graphicLoading}
        />
      </SpectralRefractionOperationButtons>
    </Grid>
  );
};

SpectralRefractionMatrixTable.propTypes = {
  matrix: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  graphicVersion: PropTypes.string.isRequired,
  setEditingBreakingWaveHeights: PropTypes.func.isRequired,
  setGraphicLoading: PropTypes.func.isRequired,
  graphicLoading: PropTypes.bool.isRequired,
};

export default SpectralRefractionMatrixTable;
