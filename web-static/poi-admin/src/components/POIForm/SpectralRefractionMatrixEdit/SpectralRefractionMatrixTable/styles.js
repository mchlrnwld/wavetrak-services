import styled from '@emotion/styled';

export const Grid = styled.div`
  width: 90%;
  padding-right: 10px;
  height: calc(100vh - 40px);
  display: grid;
  grid-template-columns: min-content auto;
  grid-template-rows: min-content auto;
  grid-column-gap: 0px;
  grid-row-gap: 0px;
`;

export const XAxisLabel = styled.section`
  grid-area: 1 / 2 / 2 / 3;
  text-align: center;
  margin-top: auto;
  margin-bottom: auto;
  padding: 10px;
`;

export const YAxisLabel = styled.section`
  grid-area: 2 / 1 / 2 / 1;
  margin-top: auto;
  margin-bottom: auto;
  offset-rotate: 90;
  transform: rotate(-90deg);
`;

export const Table = styled.table`
  grid-area: 2 / 2 / 3 / 3;
  width: 100%;
  height: 100%;
  margin: auto;
  border-collapse: collapse;
  border: 1px solid black;
`;

export const SpectralRefractionOperationButtons = styled.section`
  grid-area: 3 / 2 / 3 / 3;
  margin: 20px;
  padding-bottom: 120px;
`;

export const ColumnHeader = styled.tr`
  border: 1px solid black;
`;

export const ColumnLabel = styled.th`
  text-align: center;
  font-size: 14px;
  padding: 0 12px;
`;

export const RowHeader = styled.tr`
  border: 1px solid black;
`;

export const RowLabel = styled.th`
  text-align: center;
  font-size: 16px;
  padding: 8px;
`;

export const Cell = styled.td(
  {
    textAlign: 'center',
    border: '1px solid black',
    // padding: '5px',
    fontSize: '10px',
  },
  (props) => ({
    backgroundColor: props.color,
  }),
);

export const ButtonHeader = styled.th`
  border: 1px solid black;
`;

export const BtnWrapper = styled.div`
  .quiver-button {
    color: black;
    background-color: transparent;
    border: none;
    width: 100%;
    padding: 4px;
    display: flex;
    align-items: center;
    justify-content: center;
    height: auto;
  }

  .quiver-chevron {
    width: 24px;
    fill: black;
  }

  .quiver-chevron polyline {
    fill: black;
  }
`;

export const RowBtnWrapper = styled.div`
  .quiver-button {
    color: black;
    background-color: transparent;
    border: none;
    width: 100%;
    padding: 4px;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 20px;
  }

  .quiver-chevron {
    width: 24px;
    fill: black;
  }

  .quiver-chevron polyline {
    fill: black;
  }
`;
