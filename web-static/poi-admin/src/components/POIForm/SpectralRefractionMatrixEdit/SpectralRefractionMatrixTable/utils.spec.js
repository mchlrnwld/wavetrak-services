import { describe, expect } from '@jest/globals';

import {
  addRow,
  addColumn,
  subtractColumn,
  subtractRow,
  getTableHeaders,
  getColor,
  roundToThree,
} from './utils';

describe('components / SpectralRefractionMatrixTable / utils', () => {
  const matrix = [
    [0.01, 0.02, 0.03],
    [0.4, 0.5, 0.6],
    [0.996, 0.997, 0.998],
  ];
  // filling buoy readings array with objects containing timestamps from [now - 4 days] to now
  const periodList = [24, 22, 20, 18, 16, 15, 14, 12, 11, 10, 9, 8, 7.5, 7, 6.5, 6, 5.2, 4.7];
  const directionList = [
    15, 30, 45, 60, 75, 90, 105, 120, 135, 150, 165, 180, 195, 210, 225, 240, 255, 270, 285, 300,
    315, 330, 345, 360,
  ];

  describe('table row operations', () => {
    it('should apply cell * 1.1 to each value in the provided row index', () => {
      let row = addRow(matrix, 0);
      expect(row).toStrictEqual([0.011, 0.022, 0.033]);

      row = addRow(matrix, 2);
      expect(row).toStrictEqual([1.096, 1.097, 1.098]);
    });

    it('should apply cell * 0.9 to each value in the provided row index', () => {
      let row = subtractRow(matrix, 0);
      expect(row).toStrictEqual([0.009, 0.018, 0.027]);

      row = subtractRow(matrix, 2);
      expect(row).toStrictEqual([0.896, 0.897, 0.898]);
    });
  });

  describe('table column operations', () => {
    it('should apply cell * 1.1 to each value in the provided column index', () => {
      let table = addColumn(matrix, 0);
      expect(table).toStrictEqual([
        [0.011, 0.02, 0.03],
        [0.44, 0.5, 0.6],
        [1.096, 0.997, 0.998],
      ]);

      table = addColumn(matrix, 2);
      expect(table).toStrictEqual([
        [0.01, 0.02, 0.033],
        [0.4, 0.5, 0.66],
        [0.996, 0.997, 1.098],
      ]);
    });

    it('should apply cell * 0.9 to each value in the provided column index', () => {
      let table = subtractColumn(matrix, 0);
      expect(table).toStrictEqual([
        [0.009, 0.02, 0.03],
        [0.36, 0.5, 0.6],
        [0.896, 0.997, 0.998],
      ]);

      table = subtractColumn(matrix, 2);
      expect(table).toStrictEqual([
        [0.01, 0.02, 0.027],
        [0.4, 0.5, 0.54],
        [0.996, 0.997, 0.898],
      ]);
    });
  });

  describe('roundToThree rounding function', () => {
    it('should round number to 3 decimal places', () => {
      expect(roundToThree(0.9999)).toEqual(1);
      expect(roundToThree(0.00001)).toEqual(0);
      expect(roundToThree(0.1234)).toEqual(0.123);
      expect(roundToThree(0.1235)).toEqual(0.124);
    });
  });

  describe('get table headers', () => {
    it('should return arrays of period and direction values respectively', () => {
      const { periods, directions } = getTableHeaders();

      expect(periods).toStrictEqual(periodList);
      expect(directions).toStrictEqual(directionList);
    });
  });

  describe('get color', () => {
    it('should return middle hex value given cell value & median of matrix values', () => {
      const lowHex = getColor(0.5, 0);
      const middleHex = getColor(0.5, 0.5);
      const highHex = getColor(0.5, 1);
      expect(lowHex).toStrictEqual('#f8696b');
      expect(middleHex).toStrictEqual('#fcfcff');
      expect(highHex).toStrictEqual('#5a8ac6');
    });
  });
});
