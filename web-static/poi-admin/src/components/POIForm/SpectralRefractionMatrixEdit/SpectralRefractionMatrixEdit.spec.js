import React from 'react';
import { expect } from '@jest/globals';
import { mount } from 'enzyme';
import { Button } from '@surfline/quiver-react';

import SpectralRefractionMatrixEdit from './SpectralRefractionMatrixEdit';
import { Grid, LeftRail, RightRailBottom, RightRailTop } from './styles';

describe('SpectralRefractionMatrixEdit', () => {
  it('Should render the grid', () => {
    const close = jest.fn();
    const location = { lat: 33.95, lon: -118.6 };
    let matrix = '';

    for (let i = 0; i < 24; i += 1) {
      matrix += '0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0';
      if (i !== 23) {
        matrix += '\n';
      }
    }

    const wrapper = mount(
      <SpectralRefractionMatrixEdit
        close={close}
        matrix={matrix}
        location={location}
        name="Santa Monica Beach South"
      />,
    );
    expect(wrapper.find(Grid)).toHaveLength(1);
    expect(wrapper.find(LeftRail)).toHaveLength(1);
    expect(wrapper.find(RightRailBottom)).toHaveLength(1);
    expect(wrapper.find(RightRailTop)).toHaveLength(1);
    wrapper.find(Button).first().prop('onClick')();
    expect(close).toHaveBeenNthCalledWith(1);
  });
});
