import styled from '@emotion/styled';

export const SpinnerContainer = styled.div`
  margin-top: auto;
  margin-bottom: auto;
`;
