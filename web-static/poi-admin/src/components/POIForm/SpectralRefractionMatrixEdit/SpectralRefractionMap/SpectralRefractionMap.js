import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { select } from 'd3-selection';
import { getMapTileUrl } from '@surfline/web-common';
import { Spinner } from '@surfline/quiver-react';
import { colorLookup, quantize } from './utils';
import { SpinnerContainer } from './styles';

const GRAPHIC_SIZE = { width: 400, height: 400 };
// Centering <g> circle with transform: translate causes
// auto width/height to be set. Scaling radius by x1.67
// makes up for this slighly smaller <g> child element.
const CHART_SCALE = 1.67;

/**
 * @typedef {object} Props
 * @property {{ lat: number, lon: number }} location
 * @property {string} graphicVersion
 * @property {{period: number, heights: number[]}[]} breakingWaveHeights
 * @property {bool} graphicLoading
 */

/** @type {React.FunctionComponent<Props>} */
const SpectralRefractionMap = ({
  location,
  graphicVersion,
  breakingWaveHeights,
  graphicLoading,
}) => {
  const mapUrl = getMapTileUrl(
    GRAPHIC_SIZE.width,
    GRAPHIC_SIZE.height,
    location.lat,
    location.lon,
    7,
  );

  const svgContainer = `#svgcontainer-${graphicVersion}`;

  // calculate point on circle to place BWH text
  const textAngle = (index, numPolarChartPoints) =>
    (index / numPolarChartPoints) * 2 * Math.PI - Math.PI / 2;

  const xOffset = (angle, radius) => Math.cos(angle) * radius;
  const yOffset = (angle, radius) => Math.sin(angle) * radius;

  useEffect(() => {
    if (breakingWaveHeights) {
      const { width, height } = GRAPHIC_SIZE;
      const radius = width / CHART_SCALE;

      // add radius value to each point object in bwh array
      const bwhData = breakingWaveHeights.map((bwh, i) => ({
        ...bwh,
        // calculate radii with 1/24 increments for all periods
        radius: (1 / 24) * (i + 1) * radius,
      }));

      const svg = select(svgContainer).append('svg').attr('width', width).attr('height', height);

      const polarChart = svg
        .append('g')
        .attr('transform', `translate(${width / 2},${height / 2})`)
        .selectAll('g')
        .data(bwhData)
        .join('g');

      polarChart
        .append('circle')
        .attr('fill', 'none')
        .attr('stroke', (d) => (d.period === 10 || d.period === 20 ? 'black' : 'none'))
        .attr('stroke-width', 1)
        .attr('r', (d) => d.radius);

      const polarChartPoint = polarChart
        .selectAll('g')
        .data((point) =>
          point.heights.map((t, i) => ({
            bwh: t,
            direction: i * 5,
            period: point.period,
            point,
          })),
        )
        .join('g')
        .attr('class', 'polar-chart-point')
        .attr('transform', (circle, i) => {
          const angle = textAngle(i, circle.point.heights.length);
          return `translate(${xOffset(angle, circle.point.radius)},${yOffset(
            angle,
            circle.point.radius,
          )})`;
        });

      polarChartPoint
        .append('text')
        .attr('dy', '0.35em')
        .attr('dx', '-0.35em')
        .attr('fill', (point) => colorLookup[quantize(point.bwh)])
        .attr('font-size', 8)
        .attr('font-weight', 'bold')
        .text((point) => (point.bwh >= 0.5 ? Math.round(point.bwh) : ''));
    }
  }, [svgContainer, graphicVersion, breakingWaveHeights]);

  return (
    <div
      id={`svgcontainer-${graphicVersion}`}
      style={{
        ...GRAPHIC_SIZE,
        backgroundImage: `url("${mapUrl}")`,
        display: 'flex',
        justifyContent: 'center',
      }}
    >
      {graphicLoading && (
        <SpinnerContainer>
          <Spinner />
        </SpinnerContainer>
      )}
    </div>
  );
};

SpectralRefractionMap.propTypes = {
  location: PropTypes.shape({ lat: PropTypes.number.isRequired, lon: PropTypes.number.isRequired })
    .isRequired,
  graphicVersion: PropTypes.string.isRequired,
  breakingWaveHeights: PropTypes.arrayOf(
    PropTypes.shape({
      period: PropTypes.number,
      heights: PropTypes.arrayOf(PropTypes.number),
    }),
  ).isRequired,
  graphicLoading: PropTypes.bool.isRequired,
};

export default SpectralRefractionMap;
