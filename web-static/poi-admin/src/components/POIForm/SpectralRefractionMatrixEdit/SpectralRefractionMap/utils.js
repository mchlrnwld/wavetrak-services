export const colorLookup = [
  '#0142fe',
  '#01cafe',
  '#01f74c',
  '#fdf800',
  '#ffaa00',
  '#fe6100',
  '#fe0000',
  '#fe2993',
  '#8c8c8c',
  '#161616',
];

// Determine index to use on colorLookup ([0-9])
// *logic pulled from python polar chart script*
// BWH's are given a color code index after rounding up
export const quantize = (breakingWaveHeight) =>
  Math.ceil(breakingWaveHeight) > 9 ? 9 : Math.ceil(breakingWaveHeight) - 1;
