import React from 'react';
import { expect } from '@jest/globals';
import { mount, render } from 'enzyme';

import SpectralRefractionMap from './SpectralRefractionMap';
import { breakingWaveHeights } from '../fixtures/breakingWaveHeights';

describe('SpectralRefractionMap', () => {
  const wait = (ms) => new Promise((res) => setTimeout(res, ms));

  it('Should render the Map', () => {
    const location = { lat: 33.95, lon: -118.6 };
    const wrapper = mount(
      <SpectralRefractionMap
        location={location}
        graphicVersion="adjusted-matrix"
        breakingWaveHeights={breakingWaveHeights}
      />,
    );
    expect(wrapper.find('div').prop('style').backgroundImage).toEqual(
      'url("https://api.maptiler.com/maps/062c0d04-1842-4a45-8181-c5bec3bf2214/static/-118.6,33.95,7/400x400.png?key=3tFgnOQBQixe61aigsBT&attribution=0")',
    );
  });

  it('Should render the svg', async () => {
    const location = { lat: 33.95, lon: -118.6 };
    const wrapper = render(
      <SpectralRefractionMap
        location={location}
        graphicVersion="adjusted-matrix"
        breakingWaveHeights={breakingWaveHeights}
      />,
    );

    await wait(0);
    const svg = wrapper.find('svg');
    expect(svg).toBeTruthy();
  });
});
