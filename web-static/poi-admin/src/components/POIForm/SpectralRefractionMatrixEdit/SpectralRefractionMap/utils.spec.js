import { expect } from '@jest/globals';

import { quantize } from './utils';

describe('components / SpectralRefractionMap / utils', () => {
  describe('quantize function', () => {
    it('should return index 0 for BWH of 1 (min index)', () => {
      expect(quantize(1)).toEqual(0);
    });

    it('should return index 9 for BWH of 10 (max index)', () => {
      expect(quantize(10)).toEqual(9);
    });

    it('should return index 1 for BWH of 1.1 (round up before choosing index)', () => {
      expect(quantize(1.1)).toEqual(1);
    });

    it('should return index 9 for BWH of anything > 9)', () => {
      expect(quantize(9.1)).toEqual(9);
      expect(quantize(10)).toEqual(9);
      expect(quantize(20)).toEqual(9);
    });
  });
});
