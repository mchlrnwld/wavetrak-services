import { describe, expect } from '@jest/globals';

import sinon from 'sinon';
import { generateBreakingWaveHeightsGraphicData } from './utils';
import { matrix } from '../fixtures/matrix';

import * as bwh from '../../../../api/breakingWaveHeights';

describe('components / SpectralRefractionOperations / utils', () => {
  describe('generateBreakingWaveHeightsGraphicData', () => {
    let fetchBreakingWaveHeightsStub;
    let fetchBreakingWaveHeightsSpy;

    beforeEach(() => {
      fetchBreakingWaveHeightsStub = sinon
        .stub(bwh, 'fetchBreakingWaveHeights')
        .returns(
          Promise.resolve({ data: { breakingWaveHeights: [{ min: 1, max: 3 }] }, error: false }),
        );
      fetchBreakingWaveHeightsSpy = jest.spyOn(bwh, 'fetchBreakingWaveHeights');
    });

    afterEach(() => {
      fetchBreakingWaveHeightsStub.restore();
    });

    it('should return an array with 20 objects (one for each swell period), and a heights array of length 72 within each object', async () => {
      const { data } = await generateBreakingWaveHeightsGraphicData(matrix);
      expect(fetchBreakingWaveHeightsSpy).toHaveBeenCalledTimes(72 * 20);
      expect(data).toHaveLength(20);
      data.forEach((circle) => {
        expect(circle.heights).toHaveLength(72);
      });
    });
  });
});
