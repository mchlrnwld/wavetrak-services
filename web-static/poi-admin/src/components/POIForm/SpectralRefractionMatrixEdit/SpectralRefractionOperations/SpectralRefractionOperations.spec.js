import React from 'react';
import { expect } from '@jest/globals';
import { mount } from 'enzyme';
import { Button } from '@surfline/quiver-react';

import SpectralRefractionOperations from './SpectralRefractionOperations';
import { ButtonGroup, ExportLink } from './styles';
import { stringToMatrix } from '../../../../matrixUtils';
import { matrix } from '../fixtures/matrix';

describe('SpectralRefractionMatrixTable', () => {
  it('Should render the table operation buttons', () => {
    const saveChanges = jest.fn();
    const clearChanges = jest.fn();

    const wrapper = mount(
      <SpectralRefractionOperations
        matrix={stringToMatrix(matrix)}
        poiName="Santa Monica Beach South"
        saveChanges={saveChanges}
        clearChanges={clearChanges}
      />,
    );

    expect(wrapper.find(ButtonGroup)).toHaveLength(1);
    expect(wrapper.find(Button)).toHaveLength(3);
    expect(wrapper.find(ExportLink)).toHaveLength(1);
    wrapper.find(Button).first().prop('onClick')();
    expect(clearChanges).toHaveBeenNthCalledWith(1);

    wrapper.find(Button).at(1).prop('onClick')();
    expect(saveChanges).toHaveBeenNthCalledWith(1);
  });

  it('Should render the matrix download link', () => {
    const saveChanges = jest.fn();
    const clearChanges = jest.fn();

    const wrapper = mount(
      <SpectralRefractionOperations
        matrix={stringToMatrix(matrix)}
        poiName="Santa Monica Beach South"
        saveChanges={saveChanges}
        clearChanges={clearChanges}
      />,
    );

    expect(wrapper.find(ExportLink).first().prop('href')).toBe(
      `data:text/plain;charset=utf-8,${encodeURIComponent(matrix)}`,
    );
  });

  it('Should render generate graphic button with onClick function', () => {
    const saveChanges = jest.fn();
    const clearChanges = jest.fn();
    const setEditingBreakingWaveHeights = jest.fn();
    const setGraphicLoading = jest.fn();

    const wrapper = mount(
      <SpectralRefractionOperations
        matrix={stringToMatrix(matrix)}
        poiName="Santa Monica Beach South"
        saveChanges={saveChanges}
        clearChanges={clearChanges}
        graphicVersion="adjusted-matrix"
        setEditingBreakingWaveHeights={setEditingBreakingWaveHeights}
        setGraphicLoading={setGraphicLoading}
        graphicLoading={false}
      />,
    );

    wrapper.find(Button).at(2).prop('onClick')();
    expect(setEditingBreakingWaveHeights).toHaveBeenCalledWith({
      data: false,
      error: false,
    });
    expect(setGraphicLoading).toHaveBeenCalledWith(true);
  });
});
