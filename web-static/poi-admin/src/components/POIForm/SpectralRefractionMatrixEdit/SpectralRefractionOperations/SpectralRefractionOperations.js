import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@surfline/quiver-react';
import { slugify } from '@surfline/web-common';
import { ButtonGroup, ExportLink } from './styles';
import { matrixToString } from '../../../../matrixUtils';
import { generateBreakingWaveHeightsGraphicData } from './utils';
import { DEFAULT_BWH_STATE } from '../SpectralRefractionMatrixEdit';

/**
 * @typedef {object} Props
 * @property {Array<Array<string>>} matrix
 * @property {function} clearChanges
 * @property {function} saveChanges
 * @property {string} graphicVersion
 * @property {function} setEditingBreakingWaveHeights
 * @property {function} setGraphicLoading
 * @property {boolean} graphicLoading
 */

/** @type {React.FunctionComponent<Props>} */
const SpectralRefractionOperations = ({
  matrix,
  poiName,
  clearChanges,
  saveChanges,
  graphicVersion,
  setEditingBreakingWaveHeights,
  setGraphicLoading,
  graphicLoading,
}) => {
  const svgContainer = `svgcontainer-${graphicVersion}`;
  return (
    <>
      <ButtonGroup>
        <Button
          onClick={() => {
            clearChanges();
          }}
          style={{ marginBottom: 8, width: '33%', margin: '10px 10px 10px 0' }}
        >
          clear changes
        </Button>
        <Button
          onClick={() => {
            saveChanges();
          }}
          style={{ marginBottom: 8, width: '33%', margin: '10px 10px 10px 0' }}
        >
          save working changes
        </Button>
        <ExportLink
          href={`data:text/plain;charset=utf-8,${encodeURIComponent(matrixToString(matrix))}`}
          target="_blank"
          rel="noreferrer"
          download={`${slugify(poiName)}-matrix.txt`}
        >
          export
        </ExportLink>
      </ButtonGroup>
      <Button
        onClick={async () => {
          setEditingBreakingWaveHeights(DEFAULT_BWH_STATE);
          setGraphicLoading(true);
          // Remove old svg node from div
          const svgAdjustedMatrix = document.getElementById(svgContainer);
          if (svgAdjustedMatrix?.childNodes[0]) {
            svgAdjustedMatrix.removeChild(svgAdjustedMatrix.childNodes[0]);
          }
          const newBWH = await generateBreakingWaveHeightsGraphicData(matrix);
          setEditingBreakingWaveHeights(newBWH);
          setGraphicLoading(false);
        }}
        parentWidth
        style={{ margin: '20px 0 20px 0' }}
        disabled={graphicLoading}
      >
        generate new graphic
      </Button>
    </>
  );
};

SpectralRefractionOperations.propTypes = {
  matrix: PropTypes.arrayOf(PropTypes.array).isRequired,
  poiName: PropTypes.string.isRequired,
  clearChanges: PropTypes.func.isRequired,
  saveChanges: PropTypes.func.isRequired,
  graphicVersion: PropTypes.string.isRequired,
  setEditingBreakingWaveHeights: PropTypes.func.isRequired,
  setGraphicLoading: PropTypes.func.isRequired,
  graphicLoading: PropTypes.bool.isRequired,
};

export default SpectralRefractionOperations;
