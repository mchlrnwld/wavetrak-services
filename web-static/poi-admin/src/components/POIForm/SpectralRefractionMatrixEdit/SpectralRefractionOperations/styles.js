import styled from '@emotion/styled';

export const ButtonGroup = styled.div`
  width: 100%;
  display: flex;
`;

export const ExportLink = styled.a`
  width: 33%;
  margin: 10px 0 10px 0;
  padding: 0;
  text-align: center;
  display: flex;
  justify-content: center;
  align-items: center;
  font-family: 'Futura-Dem', Helvetica, sans-serif;
  background-color: #42a5fc;
  color: #ffffff;
  border: 2px solid #42a5fc;
  height: 55px;
  font-size: 16px;
  text-transform: uppercase;
  letter-spacing: 1.5px;
  padding: 0 42px;
  transition: background 0.25s ease, border 0.25s ease;
  cursor: pointer;
  outline: none;
`;
