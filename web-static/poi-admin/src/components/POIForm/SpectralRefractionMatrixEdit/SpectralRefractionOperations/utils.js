import { fetchBreakingWaveHeights } from '../../../../api/breakingWaveHeights';

// Number error responses from /breaking-wave-heights/ to allow before returning error
const MAX_ERROR_COUNT_THRESHOLD = 72;

export async function generateBreakingWaveHeightsGraphicData(
  spectralRefractionMatrix,
  inputHeight = 4,
) {
  const breakingWaveHeights = [];
  let errorCount = 0;

  // pass errorCount increment to fetchBreakingWaveHeight to keep track of errors in loop
  const incrementErrorCount = () => {
    errorCount += 1;
    return errorCount;
  };

  // Making requests synchronously on period level to avoid 502's -
  // rather than 20 * 72 calls at once, execute 20 groups of 72 calls
  for (let period = 6; period < 26; period += 1) {
    const heights = [];
    const promises = [];

    // make async calls by period across all directions (increments of 5deg)
    for (let direction = 0; direction < 360; direction += 5) {
      promises.push(
        fetchBreakingWaveHeights(
          [
            {
              algorithm: 'SPECTRAL_REFRACTION',
              breakingWaveHeightCoefficient: 1,
              breakingWaveHeightIntercept: 0,
              swellPartitions: [
                {
                  height: inputHeight,
                  period,
                  direction,
                },
              ],
              spectralRefractionMatrix,
            },
          ],
          incrementErrorCount,
          true,
        ),
      );
    }

    // eslint-disable-next-line no-await-in-loop
    const bwhByPeriod = await Promise.all(promises);

    // if error rate exceeds 5% of BWH values, return error
    if (errorCount > MAX_ERROR_COUNT_THRESHOLD) {
      return {
        data: [],
        error: true,
      };
    }

    bwhByPeriod.forEach((currHeight) => heights.push(currHeight.data.breakingWaveHeights[0].max));

    breakingWaveHeights.push({
      period,
      heights,
    });
  }

  return {
    data: breakingWaveHeights,
    error: false,
  };
}
