import styled from '@emotion/styled';

export const Grid = styled.div`
  width: 100%;
  height: calc(100vh - 40px);
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: repeat(2, 1fr);
  grid-column-gap: 0px;
  grid-row-gap: 0px;
`;

export const LeftRail = styled.section`
  grid-area: 1 / 1 / 3 / 2;
  width: 65vw;
`;

export const RightRailTop = styled.section`
  grid-area: 1 / 2 / 2 / 3;
  width: 30vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 40px 0 40px 0;
`;

export const RightRailBottom = styled.section`
  grid-area: 2 / 2 / 3 / 3;
  width: 30vw;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 40px 0 40px 0;
`;

export const ErrorMessage = styled.p`
  text-align: center;
  width: 400px;
`;
