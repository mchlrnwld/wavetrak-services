import { useState } from 'react';

export const useMultiSelectList = (initialValues) => {
  const [values, setValues] = useState(initialValues.length ? initialValues : ['']);

  const addValue = () => {
    setValues([...values, '']);
  };

  const removeValue = (listItemIndex) => {
    const updatedValues = values.filter((_, index) => index !== listItemIndex);
    setValues(updatedValues.length ? updatedValues : ['']);
  };

  const updateValue = (listItemIndex, newValue) => {
    let updatedList;

    if (newValue === '') {
      updatedList = values.filter((_, index) => index !== listItemIndex);
    } else {
      updatedList = values.map((item, index) => {
        if (listItemIndex === index) {
          return newValue;
        }

        return item;
      });
    }

    setValues(updatedList);
  };

  return {
    values,
    addValue,
    removeValue,
    updateValue,
  };
};
