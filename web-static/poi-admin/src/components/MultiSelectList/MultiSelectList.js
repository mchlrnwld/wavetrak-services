import React, { useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Select, CircleCloseIcon } from '@surfline/quiver-react';
import './MultiSelectList.scss';
import { useMultiSelectList } from './hooks';

const MultiSelectList = ({
  initialValues,
  selectOptions,
  addItemButtonText,
  primaryListItemText,
  additionalListItemText,
  onChange,
}) => {
  const { values, addValue, removeValue, updateValue } = useMultiSelectList(initialValues);

  const options = useMemo(() => {
    const updatedOptions = selectOptions.map((item) => ({
      ...item,
      disabled: values.includes(item.value),
    }));

    return [
      {
        text: 'None',
        value: '',
      },
      ...updatedOptions,
    ];
  }, [selectOptions, values]);

  const emptyItemValue = useMemo(() => values.filter((item) => item === '').length > 0, [values]);

  useEffect(() => {
    onChange(values.filter((id) => id !== ''));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [values]);

  return (
    <>
      {!!values.length && (
        <>
          <div className="multi-select-list">
            {values.map((item, index) => (
              <div className="multi-select-list__list__item" key={item}>
                <Select
                  id={item}
                  label={
                    index === 0 ? primaryListItemText : `${additionalListItemText} ${index + 1}`
                  }
                  options={options}
                  defaultValue={item}
                  parentWidth
                  select={{
                    onChange: ({ target }) => updateValue(index, target.value),
                  }}
                />
                {item !== '' && (
                  // eslint-disable-next-line jsx-a11y/click-events-have-key-events
                  <div
                    className="multi-select-list__list__item__remove"
                    onClick={() => removeValue(index)}
                  >
                    <CircleCloseIcon />
                  </div>
                )}
              </div>
            ))}
          </div>
          {!emptyItemValue && (
            <button
              type="button"
              className="multi-select-list__add-item"
              onClick={() => addValue()}
            >
              {addItemButtonText}
            </button>
          )}
        </>
      )}
    </>
  );
};

MultiSelectList.defaultProps = {
  addItemButtonText: 'ADD ITEM',
  primaryListItemText: 'Primary Item',
  additionalListItemText: 'Item',
};

MultiSelectList.propTypes = {
  initialValues: PropTypes.arrayOf(PropTypes.string).isRequired,
  onChange: PropTypes.func.isRequired,
  selectOptions: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      disabled: PropTypes.bool.isRequired,
    }),
  ).isRequired,
  addItemButtonText: PropTypes.string,
  primaryListItemText: PropTypes.string,
  additionalListItemText: PropTypes.string,
};

export default MultiSelectList;
