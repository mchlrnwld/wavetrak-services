import React from 'react';
import { expect, jest } from '@jest/globals';
import { shallow, mount } from 'enzyme';
import MultiSelectList from '.';
import { useMultiSelectList } from './hooks';

jest.mock('./hooks');

describe('MultiSelectList', () => {
  const MOCK_ONCHANGE_PROP = jest.fn();
  const MOCK_VALUE_1 = '123';
  const MOCK_TEXT_1 = 'TEXT 123';
  const MOCK_VALUE_2 = '456';
  const MOCK_TEXT_2 = 'TEXT 456';
  const MOCK_VALUE_3 = '789';
  const MOCK_TEXT_3 = 'TEXT 789';
  const PRIMARY_ITEM_TEXT_PROP = 'Primary';
  const ADDITIONAL_ITEM_TEXT_PROP = 'Other';
  const ADD_BUTTON_TEXT = 'ADD BUTTON';
  const MOCK_SELECT_OPTIONS = [
    {
      text: MOCK_TEXT_1,
      value: MOCK_VALUE_1,
    },
    {
      text: MOCK_TEXT_2,
      value: MOCK_VALUE_2,
    },
    {
      text: MOCK_TEXT_3,
      value: MOCK_VALUE_3,
    },
  ];

  it('should setup the component hooks correctly', () => {
    useMultiSelectList.mockReturnValue({ values: [''] });

    const MOCK_INITIAL_VALUES = ['TEST'];

    shallow(
      <MultiSelectList
        initialValues={MOCK_INITIAL_VALUES}
        selectOptions={[]}
        onChange={MOCK_ONCHANGE_PROP}
        addItemButtonText="ADD BUTTON"
        primaryListItemText="PRIMARY ITEM"
        additionalListItemText="OTHER ITEM"
      />,
    );

    expect(useMultiSelectList).toHaveBeenCalledWith(MOCK_INITIAL_VALUES);
  });

  it('should render default props correctly', () => {
    const MOCK_VALUES = [MOCK_VALUE_1, MOCK_VALUE_2];
    useMultiSelectList.mockReturnValue({ values: MOCK_VALUES });

    const wrapper = shallow(
      <MultiSelectList initialValues={[]} selectOptions={[]} onChange={MOCK_ONCHANGE_PROP} />,
    );

    expect(wrapper.find('button.multi-select-list__add-item').text()).toEqual('ADD ITEM');

    expect(wrapper.find('Select').at(0).props().label).toEqual('Primary Item');
    expect(wrapper.find('Select').at(1).props().label).toEqual('Item 2');
  });

  it('should render correctly when no initial data', () => {
    useMultiSelectList.mockReturnValue({ values: [''] });
    const wrapper = shallow(
      <MultiSelectList
        initialValues={[]}
        selectOptions={[]}
        onChange={MOCK_ONCHANGE_PROP}
        addItemButtonText="ADD BUTTON"
        primaryListItemText={PRIMARY_ITEM_TEXT_PROP}
        additionalListItemText="OTHER ITEM"
      />,
    );

    expect(wrapper.find('button.multi-select-list__add-item').length).toEqual(0);

    expect(wrapper.find('Select').at(0).props().id).toEqual('');
    expect(wrapper.find('Select').at(0).props().label).toEqual(PRIMARY_ITEM_TEXT_PROP);
    expect(wrapper.find('Select').at(0).props().defaultValue).toEqual('');
  });

  it('should render correctly initial values correctly', () => {
    const MOCK_INITIAL_VALUES = [MOCK_VALUE_1, MOCK_VALUE_2];

    useMultiSelectList.mockReturnValue({ values: MOCK_INITIAL_VALUES });

    const wrapper = shallow(
      <MultiSelectList
        initialValues={MOCK_INITIAL_VALUES}
        selectOptions={MOCK_SELECT_OPTIONS}
        onChange={MOCK_ONCHANGE_PROP}
        primaryListItemText={PRIMARY_ITEM_TEXT_PROP}
        addItemButtonText="ADD BUTTON"
        additionalListItemText={ADDITIONAL_ITEM_TEXT_PROP}
      />,
    );

    expect(wrapper.find('Select').length).toEqual(2);

    expect(wrapper.find('Select').at(0).props().id).toEqual(MOCK_VALUE_1);
    expect(wrapper.find('Select').at(0).props().label).toEqual(PRIMARY_ITEM_TEXT_PROP);
    expect(wrapper.find('Select').at(0).props().defaultValue).toEqual(MOCK_VALUE_1);
    expect(wrapper.find('Select').at(0).props().options).toEqual([
      {
        text: 'None',
        value: '',
      },
      {
        text: MOCK_TEXT_1,
        value: MOCK_VALUE_1,
        disabled: true,
      },
      {
        text: MOCK_TEXT_2,
        value: MOCK_VALUE_2,
        disabled: true,
      },
      {
        text: MOCK_TEXT_3,
        value: MOCK_VALUE_3,
        disabled: false,
      },
    ]);

    expect(wrapper.find('Select').at(1).props().id).toEqual(MOCK_VALUE_2);
    expect(wrapper.find('Select').at(1).props().label).toEqual(`${ADDITIONAL_ITEM_TEXT_PROP} 2`);
    expect(wrapper.find('Select').at(1).props().defaultValue).toEqual(MOCK_VALUE_2);
    expect(wrapper.find('Select').at(1).props().options).toEqual([
      {
        text: 'None',
        value: '',
      },
      {
        text: MOCK_TEXT_1,
        value: MOCK_VALUE_1,
        disabled: true,
      },
      {
        text: MOCK_TEXT_2,
        value: MOCK_VALUE_2,
        disabled: true,
      },
      {
        text: MOCK_TEXT_3,
        value: MOCK_VALUE_3,
        disabled: false,
      },
    ]);
  });

  it('should render correctly when adding a new value i.e values has a empty string', () => {
    const MOCK_VALUES = [MOCK_VALUE_1, ''];
    useMultiSelectList.mockReturnValue({ values: MOCK_VALUES });

    const wrapper = shallow(
      <MultiSelectList
        initialValues={[]}
        selectOptions={[]}
        onChange={MOCK_ONCHANGE_PROP}
        addItemButtonText="ADD BUTTON"
        primaryListItemText={PRIMARY_ITEM_TEXT_PROP}
        additionalListItemText={ADDITIONAL_ITEM_TEXT_PROP}
      />,
    );

    expect(wrapper.find('button.multi-select-list__add-item').length).toEqual(0);

    expect(wrapper.find('Select').at(0).props().id).toEqual(MOCK_VALUE_1);
    expect(wrapper.find('Select').at(0).props().label).toEqual(PRIMARY_ITEM_TEXT_PROP);
    expect(wrapper.find('Select').at(0).props().defaultValue).toEqual(MOCK_VALUE_1);

    expect(wrapper.find('Select').at(1).props().id).toEqual('');
    expect(wrapper.find('Select').at(1).props().label).toEqual(`${ADDITIONAL_ITEM_TEXT_PROP} 2`);
    expect(wrapper.find('Select').at(1).props().defaultValue).toEqual('');
  });

  it('should render correctly when values returns a list', () => {
    const MOCK_VALUES = [MOCK_VALUE_1, MOCK_VALUE_2, MOCK_VALUE_3];
    useMultiSelectList.mockReturnValue({ values: MOCK_VALUES });

    const wrapper = shallow(
      <MultiSelectList
        initialValues={[]}
        selectOptions={MOCK_SELECT_OPTIONS}
        onChange={MOCK_ONCHANGE_PROP}
        addItemButtonText={ADD_BUTTON_TEXT}
        primaryListItemText={PRIMARY_ITEM_TEXT_PROP}
        additionalListItemText={ADDITIONAL_ITEM_TEXT_PROP}
      />,
    );

    expect(wrapper.find('.multi-select-list__list__item').length).toEqual(3);
    expect(wrapper.find('CircleCloseIcon').length).toEqual(3);
    expect(wrapper.find('Select').length).toEqual(3);

    expect(wrapper.find('button.multi-select-list__add-item').length).toEqual(1);
    expect(wrapper.find('button.multi-select-list__add-item').text()).toEqual(ADD_BUTTON_TEXT);

    expect(wrapper.find('Select').at(0).props().id).toEqual(MOCK_VALUE_1);
    expect(wrapper.find('Select').at(0).props().label).toEqual(PRIMARY_ITEM_TEXT_PROP);
    expect(wrapper.find('Select').at(0).props().defaultValue).toEqual(MOCK_VALUE_1);
    expect(wrapper.find('Select').at(0).props().options).toEqual([
      {
        text: 'None',
        value: '',
      },
      {
        text: MOCK_TEXT_1,
        value: MOCK_VALUE_1,
        disabled: true,
      },
      {
        text: MOCK_TEXT_2,
        value: MOCK_VALUE_2,
        disabled: true,
      },
      {
        text: MOCK_TEXT_3,
        value: MOCK_VALUE_3,
        disabled: true,
      },
    ]);

    expect(wrapper.find('Select').at(1).props().id).toEqual(MOCK_VALUE_2);
    expect(wrapper.find('Select').at(1).props().label).toEqual(`${ADDITIONAL_ITEM_TEXT_PROP} 2`);
    expect(wrapper.find('Select').at(1).props().defaultValue).toEqual(MOCK_VALUE_2);
    expect(wrapper.find('Select').at(1).props().options).toEqual([
      {
        text: 'None',
        value: '',
      },
      {
        text: MOCK_TEXT_1,
        value: MOCK_VALUE_1,
        disabled: true,
      },
      {
        text: MOCK_TEXT_2,
        value: MOCK_VALUE_2,
        disabled: true,
      },
      {
        text: MOCK_TEXT_3,
        value: MOCK_VALUE_3,
        disabled: true,
      },
    ]);

    expect(wrapper.find('Select').at(2).props().id).toEqual(MOCK_VALUE_3);
    expect(wrapper.find('Select').at(2).props().label).toEqual(`${ADDITIONAL_ITEM_TEXT_PROP} 3`);
    expect(wrapper.find('Select').at(2).props().defaultValue).toEqual(MOCK_VALUE_3);
    expect(wrapper.find('Select').at(2).props().options).toEqual([
      {
        text: 'None',
        value: '',
      },
      {
        text: MOCK_TEXT_1,
        value: MOCK_VALUE_1,
        disabled: true,
      },
      {
        text: MOCK_TEXT_2,
        value: MOCK_VALUE_2,
        disabled: true,
      },
      {
        text: MOCK_TEXT_3,
        value: MOCK_VALUE_3,
        disabled: true,
      },
    ]);
  });

  it('should call updateValue within custom hook correctly', () => {
    const MOCK_UPDATE_VALUE = jest.fn();
    const MOCK_VALUES = [MOCK_VALUE_1, MOCK_VALUE_2, MOCK_VALUE_3];
    useMultiSelectList.mockReturnValue({
      values: MOCK_VALUES,
      updateValue: MOCK_UPDATE_VALUE,
    });

    const wrapper = shallow(
      <MultiSelectList
        initialValues={[]}
        selectOptions={[]}
        onChange={MOCK_ONCHANGE_PROP}
        addItemButtonText={ADD_BUTTON_TEXT}
        primaryListItemText={PRIMARY_ITEM_TEXT_PROP}
        additionalListItemText={ADDITIONAL_ITEM_TEXT_PROP}
      />,
    );

    const MOCK_CHANGE_VALUE = 'TEST';
    const MOCK_ONCHANGE_EVENT = { target: { value: MOCK_CHANGE_VALUE } };

    wrapper.find('Select').at(0).props().select.onChange(MOCK_ONCHANGE_EVENT);

    expect(MOCK_UPDATE_VALUE).toHaveBeenCalledWith(0, MOCK_CHANGE_VALUE);
  });

  it('should call removeValue from custom hook correctly', () => {
    const MOCK_REMOVE_VALUE = jest.fn();
    const MOCK_VALUES = [MOCK_VALUE_1, MOCK_VALUE_2, MOCK_VALUE_3];
    useMultiSelectList.mockReturnValue({
      values: MOCK_VALUES,
      removeValue: MOCK_REMOVE_VALUE,
    });

    const wrapper = shallow(
      <MultiSelectList
        initialValues={[]}
        selectOptions={[]}
        onChange={MOCK_ONCHANGE_PROP}
        addItemButtonText={ADD_BUTTON_TEXT}
        primaryListItemText={PRIMARY_ITEM_TEXT_PROP}
        additionalListItemText={ADDITIONAL_ITEM_TEXT_PROP}
      />,
    );

    wrapper.find('.multi-select-list__list__item__remove').at(0).simulate('click');

    expect(MOCK_REMOVE_VALUE).toHaveBeenCalledWith(0);
  });

  it('should call addValue within custom hook correctly', () => {
    const MOCK_ADD_VALUE = jest.fn();
    const MOCK_VALUES = [MOCK_VALUE_1, MOCK_VALUE_2, MOCK_VALUE_3];
    useMultiSelectList.mockReturnValue({ values: MOCK_VALUES, addValue: MOCK_ADD_VALUE });

    const wrapper = shallow(
      <MultiSelectList
        initialValues={[]}
        selectOptions={[]}
        onChange={MOCK_ONCHANGE_PROP}
        addItemButtonText={ADD_BUTTON_TEXT}
        primaryListItemText={PRIMARY_ITEM_TEXT_PROP}
        additionalListItemText={ADDITIONAL_ITEM_TEXT_PROP}
      />,
    );

    wrapper.find('button.multi-select-list__add-item').simulate('click');

    expect(MOCK_ADD_VALUE).toHaveBeenCalled();
  });

  it('should call onChange prop with the correct values list and filter out any empty strings', () => {
    const MOCK_VALUES = [MOCK_VALUE_1, MOCK_VALUE_2, MOCK_VALUE_3, ''];

    useMultiSelectList.mockReturnValue({
      values: MOCK_VALUES,
    });

    mount(
      <MultiSelectList
        initialValues={[]}
        selectOptions={[]}
        onChange={MOCK_ONCHANGE_PROP}
        addItemButtonText={ADD_BUTTON_TEXT}
        primaryListItemText={PRIMARY_ITEM_TEXT_PROP}
        additionalListItemText={ADDITIONAL_ITEM_TEXT_PROP}
      />,
    );

    expect(MOCK_ONCHANGE_PROP).toHaveBeenCalledWith([MOCK_VALUE_1, MOCK_VALUE_2, MOCK_VALUE_3]);
  });
});
