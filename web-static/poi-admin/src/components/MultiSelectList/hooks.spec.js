/* eslint-disable react/prop-types, jsx-a11y/click-events-have-key-events */

import React from 'react';
import { act } from 'react-dom/test-utils';
import { expect } from '@jest/globals';
import { mount } from 'enzyme';
import { useMultiSelectList } from './hooks';

describe('MultiSelectList / hooks', () => {
  describe('useMultiSelectList', () => {
    const Component = ({ initialValues, newValue }) => {
      const { values, addValue, removeValue, updateValue } = useMultiSelectList(initialValues);

      return (
        <div className="multi-select-list">
          <div className="addValue" onClick={addValue} />
          <div className="removeValue" onClick={() => removeValue(1)} />
          <div className="updateValue" onClick={() => updateValue(1, newValue)} />
          {values && <div className="values" data={values} />}
        </div>
      );
    };

    it('should set default values state', async () => {
      let wrapper;

      await act(async () => {
        wrapper = mount(<Component initialValues={[]} />);
      });

      wrapper.update();

      expect(wrapper.find('.values')).toHaveLength(1);
      expect(wrapper.find('.values').prop('data')).toStrictEqual(['']);
    });

    it('should update state correctly when addValue is called', async () => {
      let wrapper;

      await act(async () => {
        wrapper = mount(<Component initialValues={['123']} />);
      });

      wrapper.update();

      expect(wrapper.find('.values')).toHaveLength(1);
      expect(wrapper.find('.values').prop('data')).toStrictEqual(['123']);

      wrapper.find('.addValue').simulate('click');

      wrapper.update();

      expect(wrapper.find('.values').prop('data')).toStrictEqual(['123', '']);
    });

    it('should update state correctly when removeValue is called', async () => {
      let wrapper;

      await act(async () => {
        wrapper = mount(<Component initialValues={['123', '456']} />);
      });

      wrapper.update();

      expect(wrapper.find('.values')).toHaveLength(1);
      expect(wrapper.find('.values').prop('data')).toStrictEqual(['123', '456']);

      wrapper.find('.removeValue').simulate('click');

      wrapper.update();

      expect(wrapper.find('.values').prop('data')).toStrictEqual(['123']);
    });

    it('should update state correctly when updateValue is called and newValue is empty', async () => {
      let wrapper;

      await act(async () => {
        wrapper = mount(<Component initialValues={['123', '456']} newValue="" />);
      });

      wrapper.update();

      expect(wrapper.find('.values')).toHaveLength(1);
      expect(wrapper.find('.values').prop('data')).toStrictEqual(['123', '456']);

      wrapper.find('.updateValue').simulate('click');

      wrapper.update();

      expect(wrapper.find('.values').prop('data')).toStrictEqual(['123']);
    });

    it('should update state correctly when updateValue is called with a newValue', async () => {
      let wrapper;

      await act(async () => {
        wrapper = mount(<Component initialValues={['123', '456']} newValue="000" />);
      });

      wrapper.update();

      expect(wrapper.find('.values')).toHaveLength(1);
      expect(wrapper.find('.values').prop('data')).toStrictEqual(['123', '456']);

      wrapper.find('.updateValue').simulate('click');

      wrapper.update();

      expect(wrapper.find('.values').prop('data')).toStrictEqual(['123', '000']);
    });
  });
});
