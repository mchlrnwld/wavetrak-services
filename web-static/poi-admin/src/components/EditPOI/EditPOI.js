import React from 'react';
import { useRouteMatch, Switch, Route, Redirect } from 'react-router-dom';
import { useQuery, gql } from '@apollo/client';
import { FormContainerStyles } from '../styles';
import BasicInfo from '../BasicInfo';
import TravelInfo from '../TravelInfo';
import DataSources from '../DataSources';
import ForecastSettings from '../ForecastSettings';
import Navbar from '../Navbar';
import { noSlash } from '../../utils';

const POIQuery = gql`
  query POI_CMS_getPOIData($id: String!) {
    pointOfInterest(pointOfInterestId: $id) {
      pointOfInterestId
      name
    }
  }
`;

const EditPOI = () => {
  const {
    path,
    url,
    params: { id: pointOfInterestId },
  } = useRouteMatch();
  const { data, loading, error } = useQuery(POIQuery, {
    variables: { id: pointOfInterestId },
  });

  if (loading) return <p>Loading...</p>;
  if (error) return <p>ERROR</p>;
  if (!data || !data.pointOfInterest) {
    return <p>No point of interest with ID {pointOfInterestId}</p>;
  }

  return (
    <>
      <h4>Point of Interest: {data.pointOfInterest.name}</h4>

      <Navbar
        links={[
          {
            title: 'Basic Info',
            href: `${url}/basic-info`,
          },
          {
            title: 'Travel Info',
            href: `${url}/travel-info`,
          },
          {
            title: 'Data Sources',
            href: `${url}/data-sources`,
          },
          {
            title: 'Forecast Settings',
            href: `${url}/forecast-settings`,
          },
        ]}
      />

      <FormContainerStyles>
        <Switch>
          <Route path={`${path}/basic-info`}>
            <BasicInfo />
          </Route>

          <Route path={`${path}/travel-info`}>
            <TravelInfo />
          </Route>

          <Route path={`${path}/data-sources`}>
            <DataSources />
          </Route>

          <Route path={`${path}/forecast-settings`}>
            <ForecastSettings redirectTo={`${noSlash(url)}/data-sources/new`} />
          </Route>

          <Redirect to={`${noSlash(url)}/basic-info`} />
        </Switch>
      </FormContainerStyles>
    </>
  );
};

export default EditPOI;
