import React from 'react';
import { Link } from 'react-router-dom';
import { Button, SearchInput } from '@surfline/quiver-react';
import './MainNav.scss';

const searchRedirect = (path) => (query) => {
  window.location.href = `/search/${path}/?q=${query}`;
};

const MainNav = () => (
  <div>
    <h4>POI CMS</h4>
    <div className="poi-tool-nav-item">
      <Link to="/poi/create">
        <Button>Create POI</Button>
      </Link>
    </div>
    <div className="poi-tool-nav-item">
      <h4>Surfline Spots</h4>
      <div style={{ display: 'flex' }}>
        <div styles={{ flex: 2 }}>
          <SearchInput onSubmit={searchRedirect('spotspoi')} />
        </div>
      </div>
    </div>
    <div className="poi-tool-nav-item">
      <h4>Magicseaweed Spots</h4>
      <div style={{ display: 'flex' }}>
        <div styles={{ flex: 2 }}>
          <SearchInput onSubmit={searchRedirect('mswspots')} />
        </div>
      </div>
    </div>
  </div>
);

export default MainNav;
