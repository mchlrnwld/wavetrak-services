import React from 'react';
import PropTypes from 'prop-types';
import { Alert, Button } from '@surfline/quiver-react';
import { Link } from 'react-router-dom';
import './MissingDataWarning.scss';

const MissingDataWarning = ({ warning, cta, redirect, alert = 'warning' }) => (
  <header className="missing-data-warning">
    <Alert type={alert}>{warning}</Alert>
    <Link to={redirect}>
      <Button>{cta}</Button>
    </Link>
  </header>
);

MissingDataWarning.propTypes = {
  warning: PropTypes.string.isRequired,
  cta: PropTypes.string.isRequired,
  redirect: PropTypes.string.isRequired,
  alert: PropTypes.string,
};

MissingDataWarning.defaultProps = {
  alert: 'warning',
};

export default MissingDataWarning;
