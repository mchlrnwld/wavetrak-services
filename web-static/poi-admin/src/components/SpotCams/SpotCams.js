import React from 'react';
import PropTypes from 'prop-types';
import { useCameraAPI } from './hooks';
import MultiSelectList from '../MultiSelectList';

import './SpotCams.scss';

const SpotCams = ({ camIds, onChange }) => {
  const { fetchingCams, allCams } = useCameraAPI();

  const multiSelectOptions = allCams.map(({ title, _id }) => ({
    text: title,
    value: _id,
  }));

  return (
    <div className="spot-cameras">
      {fetchingCams ? (
        <p className="spot-cameras__loading">Fetching available cams...</p>
      ) : (
        <MultiSelectList
          onChange={onChange}
          initialValues={camIds}
          selectOptions={multiSelectOptions}
          addItemButtonText="+ ADD CAM"
          primaryListItemText="Primary Cam"
          additionalListItemText="Cam"
        />
      )}
    </div>
  );
};

SpotCams.propTypes = {
  camIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  onChange: PropTypes.func.isRequired,
};

export default SpotCams;
