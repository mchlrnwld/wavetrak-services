import React from 'react';
import { expect, jest } from '@jest/globals';
import { shallow } from 'enzyme';
import { useCameraAPI } from './hooks';
import SpotCams from './SpotCams';

jest.mock('./hooks');

describe('SpotCams', () => {
  const MOCK_ONCHANGE_PROP = jest.fn();

  it('should setup the component hooks correctly', () => {
    useCameraAPI.mockReturnValue({ fetchingCams: true, allCams: [] });

    const MOCK_CAM_IDS = ['TEST'];

    shallow(<SpotCams camIds={MOCK_CAM_IDS} onChange={MOCK_ONCHANGE_PROP} />);

    expect(useCameraAPI).toHaveBeenCalledWith();
  });

  it('should render correctly when fetching cams', () => {
    useCameraAPI.mockReturnValue({ fetchingCams: true, allCams: [] });

    const wrapper = shallow(<SpotCams camIds={[]} onChange={MOCK_ONCHANGE_PROP} />);

    expect(wrapper.find('.spot-cameras__loading').length).toEqual(1);
    expect(wrapper.find('.spot-cameras__loading').text()).toEqual('Fetching available cams...');
  });

  it('should render MultiSelectList correctly', () => {
    const MOCK_CAM_ID_1 = '123';
    const MOCK_CAM_IDS = [MOCK_CAM_ID_1, ''];
    const MOCK_CAM = { title: 'TEST CAM', _id: '', ip: '127.0.0.1' };

    useCameraAPI.mockReturnValue({ fetchingCams: false, allCams: [MOCK_CAM] });

    const wrapper = shallow(<SpotCams camIds={MOCK_CAM_IDS} onChange={MOCK_ONCHANGE_PROP} />);

    expect(wrapper.find('MultiSelectList').length).toEqual(1);
    expect(wrapper.find('MultiSelectList').props()).toEqual({
      onChange: MOCK_ONCHANGE_PROP,
      initialValues: MOCK_CAM_IDS,
      selectOptions: [
        {
          text: MOCK_CAM.title,
          value: MOCK_CAM._id,
        },
      ],
      addItemButtonText: '+ ADD CAM',
      primaryListItemText: 'Primary Cam',
      additionalListItemText: 'Cam',
    });
  });
});
