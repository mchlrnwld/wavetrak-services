import { useState, useEffect } from 'react';
import { cameras as camerasAPI } from '@surfline/admin-common';

export const useCameraAPI = () => {
  const [allCams, setAllCams] = useState([]);
  const [fetchingCams, setFetchingCams] = useState(false);

  useEffect(() => {
    const fetchCams = async () => {
      try {
        setFetchingCams(true);
        const cams = await camerasAPI.fetchCameras();
        setAllCams(cams);
        setFetchingCams(false);
      } catch (error) {
        setFetchingCams(false);
      }
    };

    fetchCams();
  }, []);

  return {
    fetchingCams,
    allCams,
  };
};
