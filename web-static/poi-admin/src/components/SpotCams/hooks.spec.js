import React from 'react';
import { act } from 'react-dom/test-utils';
import { expect, jest } from '@jest/globals';
import { mount } from 'enzyme';
import { cameras as camerasAPI } from '@surfline/admin-common';
import { useCameraAPI } from './hooks';

jest.mock('@surfline/admin-common');

describe('SpotCams / hooks', () => {
  const MOCK_CAMERA = { test: 'TEST ' };

  describe('useCameraAPI', () => {
    const Component = () => {
      const { fetchingCams, allCams } = useCameraAPI();

      return (
        <div className="spot-camera">
          {fetchingCams && <div className="fetchingCams" />}
          {allCams && <div className="allCams" data={allCams} />}
        </div>
      );
    };

    it('should fetch cams successfully', async () => {
      camerasAPI.fetchCameras = jest.fn(() => Promise.resolve([MOCK_CAMERA]));

      let wrapper;

      await act(async () => {
        wrapper = mount(<Component />);
      });

      wrapper.update();

      expect(wrapper.find('.fetchingCams')).toHaveLength(0);

      expect(wrapper.find('.allCams')).toHaveLength(1);
      expect(wrapper.find('.allCams').prop('data')).toStrictEqual([MOCK_CAMERA]);
    });

    it('should catch error on fetching cams', async () => {
      camerasAPI.fetchCameras = jest.fn(() => Promise.reject(new Error('ERROR')));

      let wrapper;

      await act(async () => {
        wrapper = mount(<Component />);
      });

      wrapper.update();

      expect(wrapper.find('.fetchingCams')).toHaveLength(0);

      expect(wrapper.find('.allCams')).toHaveLength(1);
      expect(wrapper.find('.allCams').prop('data')).toStrictEqual([]);
    });
  });
});
