import propTypes from 'prop-types';
import React, { useState } from 'react';
import { Map, CircleMarker, LocationMarker, TileLayer } from '@surfline/quiver-react';
import {
  swellForecastProps,
  breakingWaveHeightInputsPropTypes,
} from '../../proptypes/pointOfInterest';
import { getGridPoints } from '../../utils';
import SwellTable from '../SwellTable';
import './PointOfInterestMap.scss';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'leaflet/dist/leaflet.css';

const round5dp = (val) => Math.round(val * 100000) / 100000;

const moveMarker = (updateLocation) => (event) => {
  updateLocation({
    lat: round5dp(event.lat),
    lon: round5dp(event.lon),
  });
};

const getRes = (resolution) => {
  if (resolution < 0.05) return 14;
  if (resolution < 0.1) return 12;
  if (resolution < 0.3) return 10;
  if (resolution < 0.6) return 9;
  return 12;
};

const PointOfInterestMap = ({
  zoom,
  location,
  updateLocation,
  gridPointLocation,
  updateGridpointLocation,
  gridParams,
  showSwellPreview,
  swellProps,
  children,
  breakingWaveHeightInputs,
  isNewModel,
}) => {
  const [selectable, setSelectable] = useState(false);
  const { lat: currentLat, lon: currentLon } = gridPointLocation;

  const isActive = ({ lat, lon }) => {
    const latMatches = Math.abs(lat - currentLat) < 0.0001;
    const lonMatches =
      Math.abs(lon - currentLon) < 0.0001 ||
      Math.abs(lon - (currentLon + 360)) < 0.0001 ||
      Math.abs(lon - (currentLon - 360)) < 0.0001;

    return latMatches && lonMatches;
  };

  const [resolution, gridPoints] = gridParams ? getGridPoints(gridParams, gridPointLocation) : [];
  const [currentResolution, setResolution] = useState(resolution);

  const [currentZoom, setZoom] = useState(resolution ? getRes(resolution) : zoom);

  if (currentResolution !== resolution) {
    setResolution(resolution);
    setZoom(resolution ? getRes(resolution) : null);
  }

  return (
    <div className="poi-map">
      {updateLocation ? (
        <button
          type="button"
          className="poi-map__selectability-toggle quiver-button"
          onClick={() => {
            setSelectable(!selectable);
          }}
        >{`${selectable ? 'disable' : 'enable'} click`}</button>
      ) : null}

      {swellProps && showSwellPreview ? (
        <div className="poi-map__forecast-preview">
          <SwellTable
            {...swellProps}
            breakingWaveHeightInputs={breakingWaveHeightInputs}
            isNewModel={isNewModel}
          />
        </div>
      ) : (
        <Map
          center={location}
          zoom={currentZoom}
          onClick={selectable && updateLocation ? moveMarker(updateLocation) : undefined}
          // eslint-disable-next-line no-underscore-dangle
          onZoomend={(event) => setZoom(event.sourceTarget._zoom)}
        >
          <TileLayer
            attribution={
              '<a href="https://www.maptiler.com/copyright/" target="_blank">© MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a>'
            }
            url="https://api.maptiler.com/maps/hybrid/{z}/{x}/{y}.jpg?key=3tFgnOQBQixe61aigsBT"
          />
          {location ? <LocationMarker position={location} fill="#8207D0" /> : null}
          {gridPoints &&
            gridPoints.map(({ latitude: lat, longitude: lon }) => {
              const latlon = { lat: round5dp(lat), lon: round5dp(lon) };
              return (
                <CircleMarker
                  key={`${lat}${lon}`}
                  position={latlon}
                  active={isActive(latlon)}
                  onClick={() => {
                    updateGridpointLocation(latlon);
                  }}
                  inactiveFill="#B5DBFC"
                  activeFill="#1DD007"
                />
              );
            })}
          {children}
        </Map>
      )}
    </div>
  );
};

PointOfInterestMap.propTypes = {
  zoom: propTypes.number,
  location: propTypes.shape({
    lat: propTypes.number,
    lon: propTypes.number,
  }),
  updateLocation: propTypes.func,
  gridPointLocation: propTypes.shape({
    lat: propTypes.number,
    lon: propTypes.number,
  }),
  updateGridpointLocation: propTypes.func,
  gridParams: propTypes.shape({
    model: propTypes.string,
    models: propTypes.shape([]),
    pointOfInterest: propTypes.shape({}),
  }),
  showSwellPreview: propTypes.bool,
  swellProps: swellForecastProps,
  children: propTypes.string,
  breakingWaveHeightInputs: breakingWaveHeightInputsPropTypes.isRequired,
  isNewModel: propTypes.bool,
};

PointOfInterestMap.defaultProps = {
  zoom: 12,
  location: null,
  updateLocation: null,
  gridPointLocation: { lat: null, lon: null },
  updateGridpointLocation: null,
  gridParams: null,
  showSwellPreview: false,
  swellProps: null,
  children: null,
  isNewModel: false,
};

export default PointOfInterestMap;
