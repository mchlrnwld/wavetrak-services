import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './CheckboxControl.scss';

const checkboxClassName = (error) =>
  classNames({
    'poi-tool-checkbox-control': true,
    'poi-tool-checkbox-control--error': error,
  });

const CheckboxControl = ({ name, label, options, values = [], error, message, onChange }) => {
  const [selected, setSelected] = useState(values);
  const isSelected = (_id) =>
    selected.length > 0 && selected.filter((item) => `${item._id}` === `${_id}`).length > 0;

  const onCheckboxChange = (_id) => {
    const isAlreadyChecked = isSelected(_id);

    if (isAlreadyChecked) {
      setSelected(selected.filter((item) => `${item._id}` !== `${_id}`));
    } else {
      const newItem = options.find((option) => `${option._id}` === `${_id}`);
      setSelected([...selected, newItem]);
    }
  };

  useEffect(() => {
    onChange(selected);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selected]);

  return (
    <>
      <div className={checkboxClassName(error)} id={name}>
        {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
        <h3 className="poi-tool-checkbox-control__label">{label}</h3>
        <ul className="poi-tool-checkbox-control__choices">
          {options.map((option) => (
            <li className="poi-tool-checkbox-control__choices__choice" key={`${option.name}`}>
              <label
                className="poi-tool-checkbox-control__choices__choice__label"
                htmlFor={`option-${name}-${option.name}`}
              >
                <input
                  name={name}
                  type="checkbox"
                  className="poi-tool-checkbox-control__choices__choice__input"
                  value={option._id}
                  id={`option-${name}-${option.name}`}
                  onChange={({ target }) => onCheckboxChange(target.value)}
                  checked={isSelected(option._id)}
                />
                {option.name}
              </label>
            </li>
          ))}
        </ul>
      </div>
      {error && <p className="error">{message}</p>}
    </>
  );
};

CheckboxControl.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.string).isRequired,
  values: PropTypes.arrayOf([
    PropTypes.shape({
      _id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
    }),
  ]),
  error: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

CheckboxControl.defaultProps = {
  values: [],
};

export default CheckboxControl;
