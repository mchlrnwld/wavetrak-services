import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useParams } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { Input, Button, Alert } from '@surfline/quiver-react';
import { FormFieldset } from '../styles';
import { useBasicInfoData, usePOIUpdate } from './hooks';
import { usePrevious } from '../../hooks';
import Map from '../PointOfInterestMap';
import './BasicInfo.scss';

const BasicInfo = () => {
  const { id: pointOfInterestId } = useParams();
  const { data, loading, error } = useBasicInfoData(pointOfInterestId);
  const { update, isUpdating, updateStatus } = usePOIUpdate(pointOfInterestId);
  const previousUpdateStatus = usePrevious(updateStatus ? updateStatus.succcess : undefined);
  const [coords, setCoords] = useState({ lat: 0, lon: 0 });
  const { register, errors, reset, formState, getValues, setValue, handleSubmit } = useForm({
    defaultValues: {
      poiID: pointOfInterestId,
      name: '',
      lat: 0,
      lon: 0,
    },
    mode: 'onBlur',
  });

  useEffect(() => {
    if (data) {
      const { name, lat, lon } = data.pointOfInterest;
      reset({ name, lat, lon });
      setCoords({ lat, lon });
    }
  }, [data, reset]);

  useEffect(() => {
    const hasUpdated = !previousUpdateStatus && updateStatus && updateStatus.success;

    if (hasUpdated) {
      const { name, lat, lon } = getValues();
      reset({ name, lat, lon });
    }
  }, [previousUpdateStatus, updateStatus, getValues, reset]);

  if (loading) return <p>Loading....</p>;
  if (error) return <p>error :(</p>;

  return (
    <div className="poi-tool__basic-info">
      <Helmet>
        <title>{data.pointOfInterest.name} | Basic Info | POI CMS</title>
      </Helmet>
      <div>
        <form onSubmit={handleSubmit((formData) => update(formData))}>
          <FormFieldset>
            <Input
              id="poiID"
              label="POI ID"
              type="string"
              defaultValue={pointOfInterestId}
              validateDefault={false}
              input={{
                readOnly: true,
                name: 'poiID',
              }}
            />

            <Input
              id="name"
              label="Name"
              type="text"
              defaultValue={data.pointOfInterest.name}
              message={errors.name ? 'Name is required' : ''}
              error={errors.name}
              validateDefault={false}
              input={{
                name: 'name',
                ref: register({ required: true }),
              }}
            />

            <Input
              id="lat"
              label="Lat"
              type="number"
              defaultValue={data.pointOfInterest.lat}
              error={errors.lat}
              message={errors.lat ? 'Invalid or empty Lat' : ''}
              validateDefault={false}
              input={{
                ref: register({ required: true }),
                name: 'lat',
                step: 'any',
                onChange: ({ target }) => {
                  setCoords({ ...coords, lat: +target.value });
                },
              }}
            />

            <Input
              id="lon"
              label="Lon"
              type="number"
              defaultValue={data.pointOfInterest.lon}
              error={errors.lon}
              message={errors.lon ? 'Invalid or empty Lon' : ''}
              validateDefault={false}
              input={{
                ref: register({ required: true }),
                name: 'lon',
                step: 'any',
                onChange: ({ target }) => {
                  setCoords({ ...coords, lon: +target.value });
                },
              }}
            />

            {updateStatus && (
              <Alert type={updateStatus.success ? 'success' : 'error'}>
                {updateStatus.message}
              </Alert>
            )}

            <Button
              disabled={isUpdating || !formState.dirtyFields.size}
              parentWidth
              loading={isUpdating}
            >
              Update
            </Button>
          </FormFieldset>
        </form>
      </div>
      <Map
        location={coords}
        updateLocation={({ lat: newLat, lon: newLon }) => {
          setCoords({ lat: newLat, lon: newLon });
          setValue('lat', newLat);
          setValue('lon', newLon);
        }}
      />
    </div>
  );
};

export default BasicInfo;
