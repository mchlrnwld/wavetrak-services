import { useState } from 'react';
import { useMutation, useQuery } from '@apollo/client';
import { UpdatePOI, basicInfoQuery } from '../../graphql/schema';

export const useBasicInfoData = (pointOfInterestId) => {
  const { data, loading, error } = useQuery(basicInfoQuery, {
    variables: { pointOfInterestId },
  });

  return {
    data,
    loading,
    error,
  };
};

export const usePOIUpdate = (pointOfInterestId) => {
  const [updateStatus, setUpdateStatus] = useState(undefined);
  const [updatePointOfInterest, { loading: isUpdating }] = useMutation(UpdatePOI);

  const update = async (data) => {
    setUpdateStatus(undefined);

    try {
      const {
        data: {
          updatePointOfInterest: { success },
        },
      } = await updatePointOfInterest({
        variables: {
          poiId: pointOfInterestId,
          name: data.name,
          lat: +data.lat,
          lon: +data.lon,
        },
      });

      setUpdateStatus({
        success,
        message: success ? 'POI updated' : 'Error updating POI',
      });
    } catch (error) {
      setUpdateStatus({
        success: false,
        message: 'Error updating POI',
      });
    }
  };

  return {
    update,
    isUpdating,
    updateStatus,
  };
};
