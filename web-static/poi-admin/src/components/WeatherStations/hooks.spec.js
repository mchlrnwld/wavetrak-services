import React from 'react';
import { act } from 'react-dom/test-utils';
import { expect, jest } from '@jest/globals';
import { mount } from 'enzyme';
import { weatherStations as weatherStationsAPI } from '@surfline/admin-common';
import { useWeatherStationAPI } from './hooks';

jest.mock('@surfline/admin-common');

describe('WeatherStations / hooks', () => {
  const MOCK_WEATHER_STATION = { test: 'TEST ' };

  describe('useWeatherStationAPI', () => {
    const Component = () => {
      const { fetchingWeatherStations, allWeatherStations } = useWeatherStationAPI();

      return (
        <div className="weather-stations">
          {fetchingWeatherStations && <div className="fetchingWeatherStations" />}
          {allWeatherStations && <div className="allWeatherStations" data={allWeatherStations} />}
        </div>
      );
    };

    it('should fetch weather stations successfully', async () => {
      weatherStationsAPI.fetchStations = jest.fn(() => Promise.resolve([MOCK_WEATHER_STATION]));

      let wrapper;

      await act(async () => {
        wrapper = mount(<Component />);
      });

      wrapper.update();

      expect(wrapper.find('.fetchingWeatherStations')).toHaveLength(0);

      expect(wrapper.find('.allWeatherStations')).toHaveLength(1);
      expect(wrapper.find('.allWeatherStations').prop('data')).toStrictEqual([
        MOCK_WEATHER_STATION,
      ]);
    });

    it('should catch error on fetching weather stations', async () => {
      weatherStationsAPI.fetchStations = jest.fn(() => Promise.reject(new Error('ERROR')));

      let wrapper;

      await act(async () => {
        wrapper = mount(<Component />);
      });

      wrapper.update();

      expect(wrapper.find('.fetchingWeatherStations')).toHaveLength(0);

      expect(wrapper.find('.allWeatherStations')).toHaveLength(1);
      expect(wrapper.find('.allWeatherStations').prop('data')).toStrictEqual([]);
    });
  });
});
