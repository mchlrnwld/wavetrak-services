import React from 'react';
import PropTypes from 'prop-types';
import { useWeatherStationAPI } from './hooks';
import MultiSelectList from '../MultiSelectList';

import './WeatherStations.scss';

const WeatherStations = ({ weatherStationIds, onChange }) => {
  const { fetchingWeatherStations, allWeatherStations } = useWeatherStationAPI();

  const multiSelectOptions = allWeatherStations.map((weatherStation) => ({
    text: weatherStation.station_name,
    value: weatherStation._id,
  }));

  return (
    <div className="weather-stations">
      {fetchingWeatherStations ? (
        <p className="weather-stations__loading">Fetching available weather stations...</p>
      ) : (
        <MultiSelectList
          onChange={onChange}
          initialValues={weatherStationIds}
          selectOptions={multiSelectOptions}
          addItemButtonText="+ ADD Weather Station"
          primaryListItemText="Primary Weather Station"
          additionalListItemText="Weather Station"
        />
      )}
    </div>
  );
};

WeatherStations.propTypes = {
  weatherStationIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  onChange: PropTypes.func.isRequired,
};

export default WeatherStations;
