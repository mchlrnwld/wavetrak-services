import { useState, useEffect } from 'react';
import { weatherStations as weatherStationsAPI } from '@surfline/admin-common';

export const useWeatherStationAPI = () => {
  const [allWeatherStations, setAllWeatherStations] = useState([]);
  const [fetchingWeatherStations, setFetchingWeatherStations] = useState(false);

  useEffect(() => {
    const fetchWeatherStations = async () => {
      try {
        setFetchingWeatherStations(true);
        const weatherStations = await weatherStationsAPI.fetchStations();
        setAllWeatherStations(weatherStations);
        setFetchingWeatherStations(false);
      } catch (error) {
        setFetchingWeatherStations(false);
      }
    };

    fetchWeatherStations();
  }, []);

  return {
    fetchingWeatherStations,
    allWeatherStations,
  };
};
