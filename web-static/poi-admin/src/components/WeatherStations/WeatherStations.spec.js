import React from 'react';
import { expect, jest } from '@jest/globals';
import { shallow } from 'enzyme';
import { useWeatherStationAPI } from './hooks';
import WeatherStations from './WeatherStations';

jest.mock('./hooks');

describe('WeatherStations', () => {
  const MOCK_ONCHANGE_PROP = jest.fn();

  it('should setup the component hooks correctly', () => {
    useWeatherStationAPI.mockReturnValue({ fetchingWeatherStations: true, allWeatherStations: [] });

    const MOCK_WEATHER_STATION_IDS = ['TEST'];

    shallow(
      <WeatherStations
        weatherStationIds={MOCK_WEATHER_STATION_IDS}
        onChange={MOCK_ONCHANGE_PROP}
      />,
    );

    expect(useWeatherStationAPI).toHaveBeenCalledWith();
  });

  it('should render correctly when fetching weather stations', () => {
    useWeatherStationAPI.mockReturnValue({ fetchingWeatherStations: true, allWeatherStations: [] });

    const wrapper = shallow(
      <WeatherStations weatherStationIds={[]} onChange={MOCK_ONCHANGE_PROP} />,
    );

    expect(wrapper.find('.weather-stations__loading').length).toEqual(1);
    expect(wrapper.find('.weather-stations__loading').text()).toEqual(
      'Fetching available weather stations...',
    );
  });

  it('should render MultiSelectList correctly', () => {
    const MOCK_WEATHER_STATION_ID_1 = '123';
    const MOCK_WEATHER_STATION_IDS = [MOCK_WEATHER_STATION_ID_1, ''];
    const MOCK_WEATHER_STATION = { station_name: 'TEST WEATHER_STATION', _id: '', sensors: [] };

    useWeatherStationAPI.mockReturnValue({
      fetchingWeatherStations: false,
      allWeatherStations: [MOCK_WEATHER_STATION],
    });

    const wrapper = shallow(
      <WeatherStations
        weatherStationIds={MOCK_WEATHER_STATION_IDS}
        onChange={MOCK_ONCHANGE_PROP}
      />,
    );

    expect(wrapper.find('MultiSelectList').length).toEqual(1);
    expect(wrapper.find('MultiSelectList').props()).toEqual({
      onChange: MOCK_ONCHANGE_PROP,
      initialValues: MOCK_WEATHER_STATION_IDS,
      selectOptions: [
        {
          text: MOCK_WEATHER_STATION.station_name,
          value: MOCK_WEATHER_STATION._id,
        },
      ],
      addItemButtonText: '+ ADD Weather Station',
      primaryListItemText: 'Primary Weather Station',
      additionalListItemText: 'Weather Station',
    });
  });
});
