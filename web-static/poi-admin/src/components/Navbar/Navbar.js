/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import './Navbar.scss';

const Navbar = ({ links, childNav = false }) => (
  <ul
    className={`nav ${childNav ? 'nav--child-nav' : ''}`}
    style={{
      gridTemplateColumns: `repeat(${links.length}, minmax(0, 1fr))`,
    }}
  >
    {links.map(({ title, href }) => (
      <li className="nav__nav-item" key={href}>
        <NavLink
          to={href}
          className="nav__nav-item-link"
          activeClassName="nav__nav-item-link--active"
        >
          {title}
        </NavLink>
      </li>
    ))}
  </ul>
);

Navbar.propTypes = {
  links: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      href: PropTypes.string,
    }),
  ).isRequired,
  childNav: PropTypes.bool,
};

Navbar.defaultProps = {
  childNav: false,
};

export default Navbar;
