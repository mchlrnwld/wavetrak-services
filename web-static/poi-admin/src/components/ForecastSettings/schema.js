import { gql } from '@apollo/client';

const GetForecastSettings = gql`
  query POI_CMS_getForecastSettings($pointOfInterestId: String!) {
    pointOfInterest(pointOfInterestId: $pointOfInterestId) {
      pointOfInterestId
      name
      lat
      lon
      gridPoints {
        grid {
          agency
          model
          grid
        }
        lat
        lon
      }
      surfSpotConfiguration {
        offshoreDirection
        optimalSwellDirection
        breakingWaveHeightAlgorithm
        breakingWaveHeightIntercept
        breakingWaveHeightCoefficient
        spectralRefractionMatrix
        ratingsAlgorithm
        ratingsMatrix
        ratingsOptions
      }
    }
    models {
      agency
      model
      types
      grids {
        grid
        startLatitude
        endLatitude
        startLongitude
        endLongitude
        resolution
      }
    }
  }
`;

export default GetForecastSettings;
