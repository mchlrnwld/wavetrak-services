import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { useParams } from 'react-router-dom';
import { flattenGridPointData, filterSurfModels } from '../../utils';
import { useForecastSettingsData } from './hooks';
import Map from '../PointOfInterestMap';
import BulkUpdate from '../POIForm/BulkUpdate';
import DefaultForecastModels from '../POIForm/DefaultForecastModels';
import MissingDataWarning from '../MissingDataWarning';
import './ForecastSettings.scss';

const ForecastSettings = ({ redirectTo }) => {
  const { id: pointOfInterestId } = useParams();
  const { loading, error, models, name, lat, lon, gridPoints, surfSpotConfiguration } =
    useForecastSettingsData(pointOfInterestId);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  return (
    <>
      {!gridPoints.length ? (
        <MissingDataWarning
          warning="This point of interest has no models yet."
          cta="Add Model"
          redirect={redirectTo}
        />
      ) : (
        <div className="poi-tool__forecast-settings">
          <Helmet>
            <title>{name} | Forecast Settings | POI CMS</title>
          </Helmet>
          <div className="poi-tool__forecast-settings__data">
            <DefaultForecastModels pointOfInterestId={pointOfInterestId} />
            <BulkUpdate
              pointOfInterestId={pointOfInterestId}
              models={filterSurfModels(models, flattenGridPointData(gridPoints))}
              surfSpotConfiguration={surfSpotConfiguration}
            />
          </div>

          <div className="poi-tool__forecast-settings__map">
            <Map location={{ lat, lon }} zoom={14} />
          </div>
        </div>
      )}
    </>
  );
};

ForecastSettings.propTypes = {
  redirectTo: PropTypes.string.isRequired,
};

export default ForecastSettings;
