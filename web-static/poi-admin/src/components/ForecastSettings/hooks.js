import { useQuery } from '@apollo/client';
import GetForecastSettings from './schema';
import { isSurfModel } from '../../utils';

export const useForecastSettingsData = (pointOfInterestId) => {
  const { data, loading, error } = useQuery(GetForecastSettings, {
    variables: { pointOfInterestId },
  });

  const surfGridPoints = loading
    ? null
    : data.pointOfInterest.gridPoints.filter((point) => isSurfModel(data.models, point.grid));

  return {
    loading,
    error,
    name: loading ? null : data.pointOfInterest.name,
    models: loading ? null : data.models,
    lat: loading ? null : data.pointOfInterest.lat,
    lon: loading ? null : data.pointOfInterest.lon,
    gridPoints: loading ? null : data.pointOfInterest.gridPoints,
    surfGridPoints,
    surfSpotConfiguration: loading ? null : data.pointOfInterest.surfSpotConfiguration,
  };
};
