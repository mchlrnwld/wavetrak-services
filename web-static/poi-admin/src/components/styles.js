import styled from '@emotion/styled';

export const FormContainerStyles = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  .quiver-input,
  .quiver-select,
  .quiver-text-area {
    margin-bottom: 15px;
  }

  .quiver-text-area textarea {
    min-width: 100%;
  }
`;

export const FormFieldset = styled.fieldset`
  border: none;
  margin: 25px 0;
  padding: 0;

  header {
    display: inline-block;
    padding: 2px 4px;
    background-color: white;
    transform: translate(-2px, -25px);
  }

  input[type='text'] {
    padding: 20px 10px;
    display: block;
    width: 100%;
    margin: 0 0 5px 0;
    border: 1px solid color('gray', 40);
  }

  input[type='text'],
  textarea {
    margin: 15px 0 5px 0;
  }

  .error {
    color: tomato;
    margin: 5px 0;
  }
`;

export const CenterContent = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const GridPointsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: auto;
  grid-column-gap: 10px;
  grid-row-gap: 40px;

  .model-heading {
    margin: 10px 0;
  }
`;

export const EditModelFooter = styled.footer`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: 10px;
`;

export const FooterAlert = styled.div`
  text-align: center;
  margin: 10px 0;
`;
