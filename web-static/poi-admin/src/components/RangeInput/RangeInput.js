import React from 'react';
import PropTypes from 'prop-types';
import './RangeInput.scss';

const RangeInput = ({ range, value = 0, step = 1, onChange }) => {
  const [min, max] = range;

  return (
    <div className="poi-tool-range-input">
      <input
        type="range"
        className="poi-tool-range-input__control"
        min={min}
        max={max}
        value={value}
        step={step}
        onChange={({ target }) => {
          onChange(target.value);
        }}
      />
      <div className="poi-tool-range-input__legend">
        <span>{min}</span>
        <span>{(max + min) / 2}</span>
        <span>{max}</span>
      </div>
    </div>
  );
};

RangeInput.propTypes = {
  range: PropTypes.arrayOf(PropTypes.number).isRequired,
  value: PropTypes.number,
  step: PropTypes.number,
  onChange: PropTypes.func.isRequired,
};

RangeInput.defaultProps = {
  value: 0,
  step: 1,
};

export default RangeInput;
