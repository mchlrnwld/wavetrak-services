import { gql } from '@apollo/client';

const GetDataSources = gql`
  query POI_CMS_getDataSources($pointOfInterestId: String!) {
    pointOfInterest(pointOfInterestId: $pointOfInterestId) {
      pointOfInterestId
      lat
      lon
      gridPoints {
        grid {
          agency
          model
          grid
          startLatitude
          endLatitude
          startLongitude
          endLongitude
          resolution
        }
        lat
        lon
        offshoreDirection
        optimalSwellDirection
        breakingWaveHeightAlgorithm
        breakingWaveHeightIntercept
        breakingWaveHeightCoefficient
        spectralRefractionMatrix
      }
    }
  }
`;

export default GetDataSources;
