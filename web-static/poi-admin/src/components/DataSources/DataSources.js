import React from 'react';
import { Switch, useRouteMatch, Route } from 'react-router-dom';
import ModelTable from '../POIForm/ModelTable';
import NewModel from '../POIForm/NewModel';
import EditGridPoint from '../POIForm/EditGridPoint';
import { noSlash } from '../../utils';

const DataSources = () => {
  const { path, url } = useRouteMatch();

  return (
    <Switch>
      <Route path={`${path}/new`}>
        <NewModel />
      </Route>

      <Route path={`${path}/:dataSource`}>
        <EditGridPoint />
      </Route>

      <Route path={path}>
        <ModelTable redirectTo={`${noSlash(url)}/new`} />
      </Route>
    </Switch>
  );
};

export default DataSources;
