import PropTypes from 'prop-types';

export const gridPointGridPropTypes = PropTypes.shape({
  agency: PropTypes.string.isRequired,
  model: PropTypes.string.isRequired,
  grid: PropTypes.string.isRequired,
});

/**
 * @typedef {object} GridPoint
 * @property {string} agency
 * @property {string} model
 * @property {string} grid
 * @property {number} lat
 * @property {number} lon
 * @property {string} breakingWaveHeightAlgorithm
 * @property {number} breakingWaveHeightCoefficient
 * @property {number} breakingWaveHeightIntercept
 * @property {number} offshoreDirection
 * @property {number} optimalSwellDirection
 * @property {string} spectralRefractionMatrix
 */

export const gridPointPropTypes = PropTypes.shape({
  grid: gridPointGridPropTypes.isRequired,
  lat: PropTypes.number.isRequired,
  lon: PropTypes.number.isRequired,
});

export const flattenedGridPointPropTypes = PropTypes.shape({
  agency: PropTypes.string.isRequired,
  model: PropTypes.string.isRequired,
  grid: PropTypes.string.isRequired,
  lat: PropTypes.number.isRequired,
  lon: PropTypes.number.isRequired,
});

export const surfSpotConfigurationPropTypes = PropTypes.shape({
  offshoreDirection: PropTypes.string,
  optimalSwellDirection: PropTypes.string,
  breakingWaveHeightAlgorithm: PropTypes.string,
  breakingWaveHeightIntercept: PropTypes.string,
  breakingWaveHeightCoefficient: PropTypes.string,
  spectralRefractionMatrix: PropTypes.string,
});

export const MSWPropFieldTypes = PropTypes.shape({
  spot: PropTypes.oneOf([
    {},
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      lat: PropTypes.number.isRequired,
      lon: PropTypes.number.isRequired,
      timezone: PropTypes.string.isRequired,
      surfarea: PropTypes.number.isRequired,
      country: PropTypes.number.isRequired,
      spotType: PropTypes.arrayOf(PropTypes.string).isRequired,
      topLevelNav: PropTypes.bool.isRequired,
      hideSpot: PropTypes.bool.isRequired,
      tidalPort: PropTypes.string,
      surflineSpotId: PropTypes.string,
    }),
  ]),
});

export const breakingWaveHeightInputsPropTypes = PropTypes.shape({
  algorithm: PropTypes.string,
  breakingWaveHeightCoefficient: PropTypes.number,
  breakingWaveHeightIntercept: PropTypes.number,
  optimalSwellDirection: PropTypes.number,
  spectralRefractionMatrix: PropTypes.string,
});

export const swellForecastProps = PropTypes.shape({
  agency: PropTypes.string.isRequired,
  model: PropTypes.string.isRequired,
  grid: PropTypes.string.isRequired,
  lat: PropTypes.number.isRequired,
  lon: PropTypes.number.isRequired,
  breakingWaveHeightInputs: breakingWaveHeightInputsPropTypes.isRequired,
  isNewModel: PropTypes.bool,
});

/**
 * @typedef {"SURF" | "SWELL" | "WIND" | "WEATHER"} ModelTypes
 */

/**
 * @typedef {object} Model
 * @property {"Model"} __typeName
 * @property {string} agency
 * @property {string} model
 * @property {ModelTypes[]} types
 * @property {Grid[]} grids
 */

/**
 * @typedef {object} Grid
 * @property {"ModelGrid"} __typeName
 * @property {number} startLatitude
 * @property {number} endLatitude
 * @property {number} startLongitude
 * @property {number} endLongitude
 * @property {number} resolution
 */

export const modelTypePropTypes = PropTypes.shape({
  agency: PropTypes.string.isRequired,
  model: PropTypes.string.isRequired,
  types: PropTypes.arrayOf(PropTypes.string).isRequired,
  grids: PropTypes.arrayOf(
    PropTypes.shape({
      endLatitude: PropTypes.number,
      endLongitude: PropTypes.number,
      grid: PropTypes.string,
      resolution: PropTypes.number,
      startLatitude: PropTypes.number,
      startLongitude: PropTypes.number,
    }),
  ).isRequired,
});
