import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from '@apollo/client';
import { AppContainer } from '@surfline/admin-common';
import App from './App';
import client from './client';

ReactDOM.render(
  <ApolloProvider client={client}>
    <AppContainer>
      <App />
    </AppContainer>
  </ApolloProvider>,
  document.getElementById('root'),
);
