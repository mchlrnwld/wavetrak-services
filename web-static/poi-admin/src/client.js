import { ApolloClient, ApolloLink, concat, createHttpLink } from '@apollo/client';
import { InMemoryCache } from '@apollo/client/cache';
import config from './config';

const cache = new InMemoryCache({
  typePolicies: {
    PointOfInterest: {
      keyFields: ['pointOfInterestId'],
      fields: {
        spots: {
          keyArgs: () => null,
        },
        surfSpotConfiguration: {
          merge(existing, incoming) {
            return incoming;
          },
        },
      },
    },
  },
  possibleTypes: {
    Spot: ['MSWSpot', 'SLSpot'],
    SpotCommonFields: ['MSWSpot', 'SLSpot'],
  },
});

const link = createHttpLink({
  uri: `${config.graphqlGateway}?api_key=temporary_and_insecure_wavetrak_api_key`,
});

// See this github issue:
// https://github.com/apollographql/apollo-feature-requests/issues/6
//
// Objects containing the __typename property are causing mutations to
// fail. This middleware implementation strips out the offending property
// before making a request to the server, without needing to be concerned
// for this in the application code.
const stripTypename = new ApolloLink((operation, forward) => {
  if (operation.variables) {
    // eslint-disable-next-line no-param-reassign
    operation.variables = JSON.parse(JSON.stringify(operation.variables), (key, value) =>
      key === '__typename' ? undefined : value,
    );
  }
  return forward(operation);
});

const client = new ApolloClient({
  cache,
  link: concat(stripTypename, link),
});

export default client;
