export const requiredDataSourceModels = ['MUR-SST', 'GFS', 'Lotus-*'];

export const dataSourcesCheck = (gridPoints, reqModels) => {
  let lotusFound = false;
  let murSSTFound = false;
  let gfsFound = false;
  const requiredModelsFound = [];
  gridPoints.forEach((model) => {
    if (model.grid.model.indexOf('Lotus') > -1) {
      lotusFound = true;
      requiredModelsFound.push(model.grid.model);
    }
    if (model.grid.model === 'MUR-SST') {
      murSSTFound = true;
      requiredModelsFound.push(model.grid.model);
    }
    if (model.grid.model === 'GFS') {
      gfsFound = true;
      requiredModelsFound.push(model.grid.model);
    }
  });
  const requiredModelsNotFound = reqModels.filter((model) => {
    if (requiredModelsFound.indexOf(model) > -1) return false;
    if (model.indexOf('Lotus') > -1) {
      let lotusNotFound = true;
      requiredModelsFound.forEach((reqModel) => {
        if (reqModel.indexOf('Lotus') > -1) lotusNotFound = false;
      });
      return lotusNotFound;
    }
    return true;
  });
  return {
    valid: lotusFound && murSSTFound && gfsFound,
    requiredModelsFound,
    requiredModelsNotFound,
  };
};
