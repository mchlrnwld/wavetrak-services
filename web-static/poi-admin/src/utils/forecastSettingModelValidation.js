export const requiredModelTypes = ['SURF', 'SWELLS'];

export const defaultForecastModelCheck = (models, reqModels) => {
  const forecastTypes = models.map((model) => model.forecastType);
  const requiredForecastModelsNotFound = reqModels.filter(
    (model) => forecastTypes.indexOf(model) === -1,
  );
  return {
    valid: requiredForecastModelsNotFound.length === 0,
    requiredForecastModelsNotFound,
  };
};
