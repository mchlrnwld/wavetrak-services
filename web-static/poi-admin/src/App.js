import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import MainNav from './components/MainNav';
import CreatePOI from './components/CreatePOI';
import EditPOI from './components/EditPOI';
import './App.scss';

const App = () => (
  <Router>
    <div className="poi-tool-container">
      <Switch>
        <Route path="/poi/create">
          <CreatePOI />
        </Route>
        <Route path="/poi/edit/:id">
          <EditPOI />
        </Route>
        <Route path="/poi">
          <MainNav />
        </Route>
      </Switch>
    </div>
  </Router>
);

export default App;
