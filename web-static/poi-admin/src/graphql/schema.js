import { gql } from '@apollo/client';

export const basicInfoQuery = gql`
  query POI_CMS_getBasicInfo($pointOfInterestId: String!) {
    pointOfInterest(pointOfInterestId: $pointOfInterestId) {
      pointOfInterestId
      name
      lat
      lon
    }
  }
`;

export const SLSpotFields = gql`
  fragment SLSpotFields on SLSpot {
    _id
    brand
    pointOfInterestId
    name
    lat
    lon
    timezone
    tideStation
    cams
    weatherStations
    status
    spotType
    abilityLevels
    boardTypes
    relivableRating
    subregion
    geoname
    swellWindow {
      min
      max
    }
    best {
      windDirection {
        min
        max
      }
      swellPeriod {
        min
        max
      }
      swellWindow {
        min
        max
      }
      sizeRange {
        min
        max
      }
    }
    humanReport {
      status
      clone {
        spotId
      }
      average {
        spotOneId
        spotTwoId
      }
    }
    titleTag
    metaDescription
    travelDetails {
      travelId
      abilityLevels {
        description
      }
      access
      best {
        season {
          description
          value
        }
        size {
          description
        }
        tide {
          description
          value
        }
        windDirection {
          description
        }
        swellDirection {
          description
        }
      }
      bottom {
        description
        value
      }
      breakType
      crowdFactor {
        description
        rating
      }
      description
      hazards
      localVibe {
        description
        rating
      }
      shoulderBurn {
        description
        rating
      }
      relatedArticleId
      spotRating {
        description
        rating
      }
      waterQuality {
        description
        rating
      }
      status
    }
  }
`;

export const MSWSpotFields = gql`
  fragment MSWSpotFields on MSWSpot {
    _id
    brand
    pointOfInterestId
    name
    lat
    lon
    timezone
    country {
      _id
      name
    }
    surfArea {
      _id
      name
    }
    types {
      _id
      name
    }
    tidalPort
    topLevelNav
    hidden
    surflineSpotId
    ratingType
  }
`;

export const GridPointFields = gql`
  fragment GridPointFields on PointOfInterest {
    gridPoints {
      grid {
        agency
        model
        grid
        startLatitude
        endLatitude
        startLongitude
        endLongitude
        resolution
      }
      lat
      lon
    }
  }
`;

export const DefaultForecastModelFields = gql`
  fragment DefaultForecastModelFields on PointOfInterest {
    defaultForecastModels {
      forecastType
      agency
      model
      grid {
        agency
        model
        grid
      }
    }
  }
`;

export const SurfSpotConfigurationFields = gql`
  fragment SurfSpotConfigurationFields on PointOfInterest {
    surfSpotConfiguration {
      offshoreDirection
      optimalSwellDirection
      breakingWaveHeightAlgorithm
      breakingWaveHeightIntercept
      breakingWaveHeightCoefficient
      spectralRefractionMatrix
      ratingsMatrix
    }
  }
`;

const POIFields = gql`
  fragment POIFields on PointOfInterest {
    pointOfInterestId
    name
    lat
    lon
    ...DefaultForecastModelFields
    ...GridPointFields
    ...SurfSpotConfigurationFields
    spots {
      ...SLSpotFields
      ...MSWSpotFields
    }
  }
  ${MSWSpotFields}
  ${SLSpotFields}
  ${GridPointFields}
  ${DefaultForecastModelFields}
  ${SurfSpotConfigurationFields}
`;

export const QueryAllModels = gql`
  query POI_CMS_getAllModels {
    models {
      agency
      model
      types
      grids {
        grid
        startLatitude
        endLatitude
        startLongitude
        endLongitude
        resolution
      }
    }
  }
`;

export const CreatePOI = gql`
  mutation POI_CMS_createPointOfInterest($name: String!, $lat: Latitude!, $lon: Longitude!) {
    createPointOfInterest(name: $name, lat: $lat, lon: $lon) {
      success
      message
      errorJSON
      pointOfInterest {
        ...POIFields
      }
    }
  }
  ${POIFields}
`;

export const UpdatePOI = gql`
  mutation POI_CMS_updatePointOfInterest(
    $poiId: String!
    $name: String!
    $lat: Latitude!
    $lon: Longitude!
  ) {
    updatePointOfInterest(pointOfInterestId: $poiId, name: $name, lat: $lat, lon: $lon) {
      success
      message
      errorJSON
      pointOfInterest {
        ...POIFields
      }
    }
  }
  ${POIFields}
`;

export const UpsertPOIDefaultForecastModels = gql`
  mutation POI_CMS_upsertPointOfInterestDefaultForecastModels(
    $pointOfInterestId: String!
    $defaultForecastModels: [PointOfInterestDefaultForecastModelInput]!
  ) {
    upsertPointOfInterestDefaultForecastModels(
      pointOfInterestId: $pointOfInterestId
      defaultForecastModels: $defaultForecastModels
    ) {
      success
      message
      errorJSON
      pointOfInterest {
        pointOfInterestId
        defaultForecastModels {
          forecastType
          agency
          model
          grid {
            grid
          }
        }
      }
    }
  }
`;

export const UpsertPOIGridPoints = gql`
  mutation POI_CMS_upsertPointOfInterestGridPoints(
    $pointOfInterestId: String!
    $gridPoints: [PointOfInterestGridPointInput]!
  ) {
    upsertPointOfInterestGridPoints(
      pointOfInterestId: $pointOfInterestId
      gridPoints: $gridPoints
    ) {
      success
      message
      errorJSON
      pointOfInterest {
        ...POIFields
      }
    }
  }
  ${POIFields}
`;

export const UpsertSurfSpotConfiguration = gql`
  mutation POI_CMS_upsertSurfSpotConfiguration(
    $pointOfInterestId: String!
    $surfSpotConfiguration: SurfSpotConfigurationInput!
  ) {
    upsertSurfSpotConfiguration(
      pointOfInterestId: $pointOfInterestId
      surfSpotConfiguration: $surfSpotConfiguration
    ) {
      success
      message
      errorJSON
      pointOfInterest {
        ...POIFields
      }
    }
  }
  ${POIFields}
`;

export const RemovePointOfInterestGridPoints = gql`
  mutation POI_CMS_removePointOfInterestGridPoints(
    $pointOfInterestId: String!
    $gridPoints: [RemovePointOfInterestGridPointInput]!
  ) {
    removePointOfInterestGridPoints(
      pointOfInterestId: $pointOfInterestId
      gridPoints: $gridPoints
    ) {
      success
      message
      pointOfInterest {
        ...POIFields
      }
    }
  }
  ${POIFields}
`;

export const QueryMSWSpot = gql`
  query POI_CMS_getMSWSpot($pointOfInterestId: String!) {
    pointOfInterest(pointOfInterestId: $pointOfInterestId) {
      spots {
        ...MSWSpotFields
      }
    }
  }
  ${MSWSpotFields}
`;

export const upsertMSWSpot = gql`
  mutation POI_CMS_upsertMSWSpot($spot: MSWSpotInput!) {
    upsertMSWSpot(spot: $spot) {
      success
      message
      validationErrors {
        key
        errors
      }
      spot {
        ... on MSWSpot {
          pointOfInterest {
            pointOfInterestId
            spots {
              ...MSWSpotFields
            }
          }
        }
      }
    }
  }
  ${MSWSpotFields}
`;

export const querySLSpot = gql`
  query POI_CMS_getSLSpotTravelInfo($pointOfInterestId: String!) {
    pointOfInterest(pointOfInterestId: $pointOfInterestId) {
      pointOfInterestId
      name
      lat
      lon
      ...GridPointFields
      ...DefaultForecastModelFields
      spots {
        ...SLSpotFields
      }
    }
  }
  ${GridPointFields}
  ${DefaultForecastModelFields}
  ${SLSpotFields}
`;

export const upsertSLSpot = gql`
  mutation POI_CMS_upsertSLSpot($spot: SLSpotInput!) {
    upsertSLSpot(spot: $spot) {
      success
      message
      validationErrors {
        key
        errors
      }
      spot {
        ... on SLSpot {
          pointOfInterest {
            pointOfInterestId
            spots {
              ...SLSpotFields
            }
          }
        }
      }
    }
  }
  ${SLSpotFields}
`;

export const QueryGridPoints = gql`
  query POI_CMS_getGridPoints($pointOfInterestId: String!) {
    pointOfInterest(pointOfInterestId: $pointOfInterestId) {
      lat
      lon
      ...GridPointFields
    }
  }
  ${GridPointFields}
`;

export const QueryDefaultForecastModels = gql`
  query POI_CMS_getDefaultForecastModels($pointOfInterestId: String!) {
    pointOfInterest(pointOfInterestId: $pointOfInterestId) {
      ...DefaultForecastModelFields
    }
  }
  ${DefaultForecastModelFields}
`;

export const modelUpsertData = gql`
  query POI_CMS_getModelUpsertData($pointOfInterestId: String!) {
    pointOfInterest(pointOfInterestId: $pointOfInterestId) {
      pointOfInterestId
      name
      lat
      lon
      ...GridPointFields
      ...SurfSpotConfigurationFields
    }
    models {
      agency
      model
      types
      grids {
        grid
        startLatitude
        endLatitude
        startLongitude
        endLongitude
        resolution
      }
    }
  }
  ${GridPointFields}
  ${SurfSpotConfigurationFields}
`;
