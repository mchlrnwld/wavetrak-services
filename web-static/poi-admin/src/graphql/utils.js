import {
  coercePropsToNumber,
  flattenGridPointData,
  rangesObjects,
  coerceCheckboxesToList,
} from '../utils';

/**
 * Constructs an object conforming to the "MSWSpotInput" type
 * where originalSpot contains default properties (eg. coming from cache),
 * each of which can be overriden by anything contained within spot.
 * Any scalar Int or Float types are coerced here to a number
 * to avoid non compliance with the schema.
 * @param {String} pointOfInterestId
 * @param {Object} originalSpot
 * @param {Object} spot
 */
export const createMSWSpotInput = (pointOfInterestId, originalSpot, spot) => {
  // Fields which require coercion
  const propertiesThatShouldBeNumbers = ['lat', 'lon', 'surfAreaId', 'countryId'];

  // 1) Remove unwanted fields from the object
  const { country, surfArea, hidden, topLevelNav, ...spotProps } = originalSpot || {
    country: { _id: '' },
    surfArea: { _id: '' },
    hidden: true,
    topLevelNav: true,
  };

  // 2) Construct a new object with all spot applied
  const output = {
    pointOfInterestId,
    ...spotProps,
    ...spot,
    countryId: spot.countryId || country._id,
    surfAreaId: spot.surfAreaId || surfArea._id,
    hidden: typeof spot.hidden === 'string' ? spot.hidden === 'yes' : hidden,
    topLevelNav: typeof spot.topLevelNav === 'string' ? spot.topLevelNav === 'yes' : topLevelNav,
    types: spot.types ? spot.types.map((type) => ({ ...type, _id: +type._id })) : null,
  };

  // 3 Return the object with all required properties made numbers
  return coercePropsToNumber(output, propertiesThatShouldBeNumbers);
};

export const createSLSpotInput = (pointOfInterestId, cachedSpot, spot, withTravel) => {
  const propertiesThatShouldBeNumbers = [
    'lat',
    'lon',
    ...(spot.travelDetails
      ? [
          'travelDetails.crowdFactor.rating',
          'travelDetails.localVibe.rating',
          'travelDetails.shoulderBurn.rating',
          'travelDetails.spotRating.rating',
          'travelDetails.waterQuality.rating',
        ]
      : []),
  ];

  const checkBoxes = [
    'abilityLevels',
    'boardTypes',
    ...(spot.travelDetails
      ? [
          'travelDetails.breakType',
          'travelDetails.bottom.value',
          'travelDetails.best.season.value',
          'travelDetails.best.tide.value',
        ]
      : []),
  ];

  const originalSpot = cachedSpot || {
    boardTypes: [],
    abilityLevels: [],
    best: {},
    humanReport: { clone: {}, average: {} },
  };

  const best = spot.best && {
    best: {
      windDirection: spot.best.windDirection
        ? rangesObjects(spot.best.windDirection)
        : originalSpot.best.windDirection,
      swellPeriod: spot.best.swellPeriod
        ? rangesObjects(spot.best.swellPeriod)
        : originalSpot.best.swellPeriod,
      swellWindow: spot.best.swellWindow
        ? rangesObjects(spot.best.swellWindow)
        : originalSpot.best.swellWindow,
      sizeRange: spot.best.sizeRange
        ? rangesObjects(spot.best.sizeRange)
        : originalSpot.best.sizeRange,
    },
  };

  const output = {
    pointOfInterestId,
    ...originalSpot,
    ...spot,
    ...best,
    swellWindow: spot.swellWindow ? rangesObjects(spot.swellWindow) : originalSpot.swellWindow,
    geoname: parseInt(spot.geoname || originalSpot.geoname, 10),
    relivableRating: parseInt(spot.relivableRating || originalSpot.relivableRating, 10),
    subregion: spot.subregion || originalSpot.subregion,
  };

  if (withTravel) {
    output.travelDetails = spot.travelDetails || originalSpot.travelDetails;
  } else {
    // when not saving travelDetails, make sure to not include in the object or it can get reset to empty/defaults
    delete output.travelDetails;
  }

  return coerceCheckboxesToList(
    coercePropsToNumber(output, propertiesThatShouldBeNumbers),
    checkBoxes,
  );
};

/**
 * Accepts grid points and constructs a new object
 * conforming to the PointOfInterestGridPointInput type.
 * @param {Array} gridPoints
 */
export const createPointOfInterestGridPointInput = (gridPoints) => {
  const propertiesThatShouldBeNumbers = [
    'offshoreDirection',
    'optimalSwellDirection',
    'breakingWaveHeightIntercept',
    'breakingWaveHeightCoefficient',
  ];

  return flattenGridPointData(gridPoints).map((gridPoint) =>
    coercePropsToNumber(gridPoint, propertiesThatShouldBeNumbers),
  );
};
