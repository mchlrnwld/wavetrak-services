const whitespace = /\s+/;

export const matrixToString = (matrix, scale = 5) => {
  if (matrix === null) return '';
  return matrix.map((row) => row.map((num) => num.toFixed(scale)).join(' ')).join('\n');
};

export const stringToMatrix = (str, requiredNumberOfEntriesInRow, requiredNumberOfRows) => {
  if (str === '') return null;

  const lines = str.split('\n').map((line) => line.trim());

  if (requiredNumberOfRows && lines.length !== requiredNumberOfRows)
    throw new Error(`Matrix should have ${requiredNumberOfRows} rows`);

  const matrix = lines.map((line) => line.split(whitespace).map(parseFloat));

  matrix.forEach((row, i) => {
    if (requiredNumberOfEntriesInRow && row.length !== requiredNumberOfEntriesInRow)
      throw new Error(`Row ${i + 1} does not have ${requiredNumberOfEntriesInRow} entries`);

    if (row.some(Number.isNaN)) throw new Error(`Row ${i + 1} has non-numeric entry`);
  });

  return matrix;
};

export const validateStringAsNumberMatrixWithDimensions = (
  str,
  requiredNumberOfEntriesInRow,
  requiredNumberOfRows,
) => {
  try {
    stringToMatrix(str, requiredNumberOfEntriesInRow, requiredNumberOfRows);
  } catch {
    return false;
  }
  return true;
};
