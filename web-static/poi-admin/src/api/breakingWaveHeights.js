import axios from 'axios';
import { useMemo } from 'react';
import useSWR from 'swr';

import config from '../config';
import { SWR_CONFIG } from '../constants';
import { authenticate } from '../fetch';
import { stringToMatrix } from '../matrixUtils';

/** @typedef {import('./swell').FormattedSwellData} FormattedSwellData */

/**
 * @typedef {object} Input
 * @property {string} algorithm
 * @property {number} breakingWaveHeightCoefficient
 * @property {number} breakingWaveHeightIntercept
 * @property {number} [optimalSwellDirection]
 * @property {{ height: number, period: number, direction: number }[]} [swellPartitions]
 * @property {string} [spectralRefractionMatrix]
 */

/** @typedef {Omit<Input, 'swellPartitions'>} InputWithoutPartitions */

/**
 *
 * @typedef {object} WaveHeight
 * @property {number} min
 * @property {number} max
 */

/**
 * @param {Input[]} inputs
 * @param {function} callback
 * @param {bool} failGracefully
 */
export const fetchBreakingWaveHeights = async (
  inputs,
  callback = () => {},
  failGracefully = false,
) => {
  /** @type {import('axios').AxiosResponse<{data: { breakingWaveHeights: WaveHeight[] }>} */
  try {
    const response = await axios.post(
      `${config.adminServicesEndpoint}/breaking-wave-heights/`,
      {
        inputs,
      },
      authenticate(),
    );

    return response.data;
  } catch (err) {
    callback();
    if (failGracefully) {
      return {
        data: {
          breakingWaveHeights: [
            {
              min: 0,
              max: 0,
            },
          ],
        },
      };
    }

    return err;
  }
};

/**
 * @param {SwellData} swellData
 * @param {InputWithoutPartitions} input
 * @returns {Promise<FormattedSwellData[]>}
 */
export const getBreakingWaveHeights = async (swellData, input) => {
  // We need to flatten all the runs into one array to pass into the breaking wave heights call
  const runs = swellData.map((data) => data.runs).flat();

  const breakingWaveHeights = await fetchBreakingWaveHeights(
    runs.map((run) => ({
      ...input,
      spectralRefractionMatrix: stringToMatrix(input.spectralRefractionMatrix, 18, 24),
      swellPartitions:
        // If there are no swells (point on land) just fill with a dummy swell
        // to prevent an error
        run.swells.length > 0
          ? run.swells.map((swell) => ({
              height: parseFloat(swell.height.slice(0, -2), 10),
              direction: parseFloat(swell.direction.slice(0, -1), 10),
              period: parseFloat(swell.period.slice(0, -1), 10),
            }))
          : [{ height: 0, direction: 0, period: 0 }],
    })),
  );

  if (!breakingWaveHeights?.data.breakingWaveHeights) {
    return swellData;
  }

  const waveHeights = breakingWaveHeights.data.breakingWaveHeights;

  let currIndex = 0;
  return swellData.map((swell) => ({
    ...swell,
    runs: swell.runs.map((run) => {
      currIndex += 1;
      return { ...run, waveHeights: waveHeights[currIndex] };
    }),
  }));
};

/**
 * @param {FormattedSwellData[]} swellData
 * @param {InputWithoutPartitions} input
 * @param {boolean} isNewModel
 * @param {boolean} allSwellsEmpty
 */
export const useGetBreakingWaveHeights = (swellData, input, isNewModel, allSwellsEmpty) => {
  const shouldFetchBreakingWaveHeights = swellData && !allSwellsEmpty && !isNewModel;
  const breakingWaveHeightsArgs = useMemo(() => [swellData, input], [swellData, input]);

  return useSWR(
    shouldFetchBreakingWaveHeights ? breakingWaveHeightsArgs : null,
    getBreakingWaveHeights,
    SWR_CONFIG,
  );
};
