import { useMemo } from 'react';
import useSWR from 'swr';
import { queryParams } from '../utils';
import config from '../config';
import { SWR_CONFIG } from '../constants';
import { formatSwellData } from '../components/SwellTable/formatSwellData';

/** @typedef {import('../proptypes/pointOfInterest').GridPoint} GridPoint */
/** @typedef {import('./breakingWaveHeights').Input} Input */
/** @typedef {import('../components/SwellTable/formatSwellData').Swell} Swell */
/** @typedef {import('./breakingWaveHeights').WaveHeight} WaveHeight */

/**
 * @typedef {object} FormattedSwellData
 * @property {string} weekDay
 * @property {string} dayOfMonth
 * @property {string} month
 * @property {{ time: string, swells: Swell[], waveHeights?: WaveHeight }[]} runs
 */

/**
 * @param {object} props
 * @param {GridPoint["agency"]} props.agency
 * @param {GridPoint["model"]} props.model
 * @param {GridPoint["grid"]} props.grid
 * @param {GridPoint["lat"]} props.lat
 * @param {GridPoint["lon"]} props.lon
 * @returns {FormattedSwellData[]}
 */
export const getSwellData = async ({ agency, model, grid, lat, lon }) => {
  const queryParameters = queryParams({
    agency,
    model,
    grid,
    lat,
    lon,
    'units[waveHeight]': 'ft',
    api_key: 'temporary_and_insecure_wavetrak_api_key',
  });

  const url = `${config.forecastEndpoint}/swells${queryParameters}`;

  const response = await fetch(url);
  return formatSwellData(await response.json());
};

/**
 * @param {String} agency
 * @param {String} model
 * @param {String} grid
 * @param {Number} lat
 * @param {Number} lon
 */
export const useGetSwellData = ({ agency, model, grid, lat, lon }) => {
  const swellDataArgs = useMemo(
    () => [{ agency, model, grid, lat, lon }],
    [agency, model, grid, lat, lon],
  );

  return useSWR(swellDataArgs, getSwellData, SWR_CONFIG);
};
