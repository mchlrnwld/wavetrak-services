// Run this query at http://graphql-gateway.sandbox.surfline.com/graphql
// to keep the following constant up to date
//
// __type(name: "BreakingWaveHeightAlgorithm") {
//   name
//   enumValues {
//     name
//   }
// }
export const bwhaOptions = ['SPECTRAL_REFRACTION', 'POLYNOMIAL'];

// Run this query at http://graphql-gateway.sandbox.surfline.com/graphql
// to keep the following constant up to date
//
// query getRatingTypes {
//   __type(name: "MSWRatingType") {
//     name
//     enumValues {
//       name
//     }
//   }
// }
export const ratingTypes = ['RATING_DIRECTIONAL', 'RATING_DOMINANT', 'RATING_BIGWAVE'];

export const SWR_CONFIG = {
  errorRetryCount: 1,
  revalidateOnFocus: false,
};
