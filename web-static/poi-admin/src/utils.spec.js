import { expect } from '@jest/globals';
import {
  listAllModels,
  flattenGridPointData,
  areGridPointsUniform,
  validateOffshoreAndOptimalSwellDirections,
} from './utils';
import modelData from '../fixtures/models';
import gridPoints from '../fixtures/gridpoints.json';
import swellgridPoints from '../fixtures/swell-gridpoints.json';

describe('areGridPointsUniform()', () => {
  it('should return true for grid points which are uniform', () => {
    const uniformGridPoints = swellgridPoints;

    // All sample grid points should be uniform by default
    expect(areGridPointsUniform(uniformGridPoints)).toBe(true);
  });

  it('should return false for grid points which are not uniform', () => {
    const nonUniformGridPoints = swellgridPoints.map((gridPoint, index) => ({
      ...gridPoint,
      offshoreDirection: gridPoint.offshoreDirection + index,
    }));

    expect(areGridPointsUniform(nonUniformGridPoints)).toBe(false);
  });
});

describe('flattenGridPointData()', () => {
  it('should flatten nested properties on grid point objects', () => {
    const nested = gridPoints;
    const flattened = flattenGridPointData(gridPoints);

    nested.forEach((before, index) => {
      expect(before).not.toHaveProperty('agency');
      expect(before).not.toHaveProperty('model');
      expect(before).toHaveProperty('grid');
      expect(before.grid instanceof Object).toBe(true);
      expect(before.grid).toHaveProperty('agency');
      expect(before.grid).toHaveProperty('model');
      expect(before.grid).toHaveProperty('grid');

      expect(flattened[index] instanceof Object).toBe(true);
      expect(flattened[index]).toHaveProperty('agency');
      expect(flattened[index]).toHaveProperty('model');
      expect(flattened[index]).toHaveProperty('grid');
      expect(typeof flattened[index].grid).toBe('string');
    });
  });
});

describe('listAllModels()', () => {
  it('should generate an array of strings', () => {
    expect(listAllModels(modelData, { lat: 50.4184, lon: 354.9003 }) instanceof Array).toBe(true);
  });

  it(`should generate a list of 'AGENCY MODEL GRID' names for models that are in bounds,
    add a star where there isn't a grid definition
    and sort the models alphabeticaly`, () => {
    const expectedArray = ['Wavetrak Lotus-WW3 UK_3m'];

    const output = listAllModels(modelData, { lat: 50.4184, lon: 354.9003 });

    expectedArray.forEach((expected, index) => {
      expect(output[index]).toStrictEqual(expected);
    });
  });
});

describe('validateOffshoreAndOptimalSwellDirections', () => {
  const msg = 'Offshore and optimal swell directions are required and must not be equal.';

  it('should return false when offshore direction is undefined', () => {
    expect(validateOffshoreAndOptimalSwellDirections(undefined, 123)).toEqual(msg);
  });

  it('should return false when optimal swell direction is undefined', () => {
    expect(validateOffshoreAndOptimalSwellDirections(123, undefined)).toEqual(msg);
  });

  it('should return false when offshore direction equals optimal swell direction', () => {
    expect(validateOffshoreAndOptimalSwellDirections(123, 123)).toEqual(msg);
  });

  it('should return true when offshore direction does not equal optimal swell direction', () => {
    expect(validateOffshoreAndOptimalSwellDirections(111, 123)).toBeTruthy();
  });

  it('should return true when offshore direction is 0 and does not equal optimal swell direction', () => {
    expect(validateOffshoreAndOptimalSwellDirections(0, 120)).toBeTruthy();
  });

  it('should return true when optimal swell direction is 0 and does not equal offshore direction', () => {
    expect(validateOffshoreAndOptimalSwellDirections(220, 0)).toBeTruthy();
  });
});
