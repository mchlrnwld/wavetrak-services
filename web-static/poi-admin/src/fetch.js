import qs from 'qs';
import fetch from 'isomorphic-fetch';
import Cookies from 'js-cookie';
import config from './config';

const canUseDOM = !!(
  typeof window !== 'undefined' &&
  window.document &&
  window.document.createElement
);

export const authenticate = (options = {}) => {
  let accessToken;
  let authenticatedOptions = options;
  if (canUseDOM) {
    accessToken = Cookies.get('access_token');
    authenticatedOptions = { headers: {}, ...options };
    authenticatedOptions.headers.Authorization =
      // eslint-disable-line
      `Bearer ${accessToken}`;
  }

  return authenticatedOptions;
};

// TODO (FE-350): replace isomorphic fetch with axios
const fetchWrapper = async (path, options) => {
  const response = await fetch(`${config.adminServicesEndpoint}/${path}`, authenticate(options));
  const contentType = response.headers.get('Content-Type');
  const body =
    contentType && contentType.indexOf('json') > -1 ? await response.json() : await response.text();
  if (response.status > 200) throw body;
  return body;
};

export const fetchSpots = (queryObj) => fetchWrapper(`spots/?${qs.stringify(queryObj)}`);

export const fetchSubregions = () => fetchWrapper('subregions/');

/**
 * Returns all tide ports from the shared tide service within 100km of the specified coordinates
 * @param coordinates
 * @returns {Promise<any>}
 */
export const fetchSharedTideServiceTidePorts = ({ lat, lon, limit = 100 }) =>
  fetchWrapper(`tide/ports?lat=${lat}&lon=${lon}&limit=${limit}`);

export const fetchCameras = () =>
  fetchWrapper('cameras/?orderBy=title', {
    method: 'GET',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
  });
