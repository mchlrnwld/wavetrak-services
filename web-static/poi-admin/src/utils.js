import { pick, isEqual } from 'lodash';
// TODO remove this dependency since it contains service dependencies like newrelic
// that can break tests
import { limitBounds, generateGrid } from '@surfline/services-common/dist/utils';
import { fetchWithAuth as slCommonFetch } from '@surfline/admin-common';
import {
  fetchSpots,
  fetchSubregions as fetchSubregionsFromAPI,
  fetchCameras as fetchCamerasFromAPI,
} from './fetch';
import config from './config';

const maxGridRadius = 1;

/** normalize a number between 0 and 360 (copied from sds)
 * @param {number} longitude number to be normalized to between 0 and 360
 */
const normalizeLongitude = (longitude) => {
  const lon = longitude % 360;
  if (lon < 0) return lon + 360;
  return lon;
};

/**
 * returns the max bounds that we would want grid points for
 * adjusts for resolution so higher res models don't have as many points
 * @param  {{ lat: number, lon: number }} points lat/lon of location
 * @param  {number} resolution resolution of grid
 * @param  {{ lat: number, lon: number }} gridPoint lat/lon of current grid point
 */
const createBounds = ({ lat, lon }, resolution, { lat: gridLat, lon: gridLon }) => {
  const gridRadius = Math.min(maxGridRadius, resolution * 3);

  const normLon = normalizeLongitude(lon);
  const normGridLon = normalizeLongitude(gridLon);

  return {
    northWest: {
      latitude: gridLat > lat + gridRadius ? gridLat : lat + gridRadius,
      longitude: normGridLon < normLon - gridRadius ? normGridLon : normLon - gridRadius,
    },
    southEast: {
      latitude: gridLat < lat - gridRadius ? gridLat : lat - gridRadius,
      longitude: normGridLon > normLon + gridRadius ? normGridLon : normLon + gridRadius,
    },
  };
};

/**
 * Accepts an array of models (as defined on the graph)
 * and a lat lon object.
 * Generates a list of `AGENCY MODEL GRID` names
 * for models that are in bounds (as defined by createBounds),
 * adds a star where there isn't a grid definition,
 * sorts the models alphabeticaly
 * @param {Array} models
 * @param {Object} location an object that has lat and lon values
 * @return {Array} a flat array of strings
 */
export const listAllModels = (models, location) =>
  models
    .map(({ agency, model, grids }) => {
      if (model.match(/(Proteus|LOLA)/)) return false;
      return grids.map(
        ({ grid, startLatitude, startLongitude, endLatitude, endLongitude, resolution }) => {
          const bounds = createBounds(location, resolution, location);
          const gridName = `${agency} ${model} ${grid}`;
          if (startLatitude !== null && startLatitude !== undefined) {
            try {
              limitBounds(bounds, {
                limBounds: {
                  northWest: {
                    latitude: endLatitude,
                    longitude: startLongitude,
                  },
                  southEast: {
                    latitude: startLatitude,
                    longitude: endLongitude,
                  },
                },
                resolution,
              });
            } catch (error) {
              return false;
            }
            return gridName;
          }
          return `${gridName} *`; // Star denotes that there's no grid def for this grid.
        },
      );
    })
    .flat()
    .filter((value) => value)
    .sort();

/**
 * Finds the grid object that corresponds to a `agency-model-grid` name
 * @param {array} models an array of model objects
 * @param {string} modelName the concatentated model name
 */
export const findGrid = (models, modelName) => {
  const [agency, model, grid] = modelName.split(' ');
  const thisModel = models.find(
    (listModel) => listModel.agency === agency && listModel.model === model,
  );
  if (!thisModel) return [];
  return thisModel.grids.find((listGrid) => listGrid.grid === grid) || {};
};

/**
 * Gets an array of lat lon objects that represent the grid points for a given model and location
 * @param  {} {model
 * @param  {} models
 * @param  {} location}
 */
export const getGridPoints = ({ model, models, location }, gridPointLocation) => {
  const { startLatitude, endLatitude, startLongitude, endLongitude, resolution } = findGrid(
    models,
    model,
  );

  try {
    return startLatitude !== undefined && startLatitude !== null
      ? [
          resolution,
          generateGrid(createBounds(location, resolution, gridPointLocation), {
            limBounds: {
              northWest: {
                latitude: endLatitude,
                longitude: startLongitude,
              },
              southEast: {
                latitude: startLatitude,
                longitude: endLongitude,
              },
            },
            resolution,
          }).flat(),
        ]
      : [];
  } catch (error) {
    return [];
  }
};

/**
 * Accepts an array of grid point objects and returns it
 * with all objects flattened to a depth of 1
 * @param {Array} gridPoints
 */
export const flattenGridPointData = (gridPoints) =>
  gridPoints.map((gridPoint) => ({
    ...gridPoint,
    ...gridPoint.grid,
  }));

/**
 * Updates the given keys with the values provided using a setValue func
 * intended to be used with react-hook-form which provides a setValue function
 * @param {object} values the values to update the keys with
 * @param {function} setValue the function that will set the value
 * @param {array} keys the keys to set values for from the object
 */
export const setValues = ({ values, setValue, keys }) => {
  keys.forEach((key) => {
    setValue(key, `${values[key]}`, false);
  });
};

export const isModelType = (model, type) => model.types.includes(type);

export const findGridPointModel = (models, gridPoint) =>
  models.find(({ agency, model }) => agency === gridPoint.agency && model === gridPoint.model);

export const isSurfModel = (models, gridPoint) =>
  isModelType(findGridPointModel(models, gridPoint), 'SURF');

export const isSwellModel = (models, gridPoint) =>
  isModelType(findGridPointModel(models, gridPoint), 'SWELL');

/**
 * using an array of keys, this recursively traverses
 * an object to set a specified value
 * @param  {} object the object to have the value set
 * @param  {} keys an array of keys, in order parent -> child
 * @param  {} func function to set the value
 */
const setValueByPath = (object, keys, func) => {
  const thisKey = keys.shift();
  return {
    ...object,
    [thisKey]:
      keys.length === 0 ? func(object[thisKey]) : setValueByPath(object[thisKey], keys, func),
  };
};

/**
 * Accepts and returns an object with specific properties
 * coerced to numbers.
 *
 * all properties which are contained within
 * listOfProperties will have their values coerced to numbers.
 *
 * ListOfProperties accepts dot notation property names and the object can have nested objects
 *
 * @param {Object} object
 * @param {Array} listOfProperties
 */
export const coercePropsToNumber = (object, listOfProperties = []) =>
  listOfProperties.reduce(
    (newObject, property) => setValueByPath(newObject, property.split('.'), (value) => +value),
    object,
  );

/**
 * Accepts and returns an object with all bar specific properties
 * coerced to numbers
 *
 * all properties which are not contained within
 * listOfProperties will have their values coerced to numbers.
 *
 * ListOfProperties doesn't accept dot notation property names
 *
 * @param {Object} object
 * @param {Array} listOfProperties
 */
export const coercePropsToNumberInverted = (object, listOfProperties = []) =>
  Object.keys(object).reduce(
    (newObject, property) => ({
      ...newObject,
      [property]: !listOfProperties.includes(property) ? +object[property] : object[property],
    }),
    {},
  );

export const coerceCheckboxesToList = (object, listOfProperties = []) =>
  listOfProperties.reduce(
    (newObject, property) =>
      setValueByPath(newObject, property.split('.'), (selected) =>
        selected && selected[0] && selected[0]._id ? selected.map(({ _id }) => _id) : selected,
      ),
    object,
  );

export const fetchTimezones = async () => {
  const response = await fetch(`${config.spotsCms}/spots/timezones`, { credentials: 'include' });
  const data = await response.json();
  return data.map((timezone) => timezone.replace('_', ' '));
};

/**
 * Returns all tideports within a radius equal to
 * the given distance for the lat lon coordinates passed,
 * ordered by the closest.
 * @param {Object} coordinates
 * @param {Number} distance
 */
export const fetchTidePorts = async (coordinates, distance = 100) => {
  const credentialsOptions = config.mswCredentials
    ? {
        credentials: 'include',
        headers: new Headers({
          Authorization: `Basic ${btoa(config.mswCredentials)}`,
        }),
      }
    : null;

  const response = await fetch(
    `${config.mswEndpoint}tidePort/?lat=${coordinates.lat}&lon=${coordinates.lon}&distance=${distance}`,
    { ...credentialsOptions },
  );

  const result = await response.json();

  return result;
};

/**
 * Accepts an array of model descriptors and
 * model grid points and returns only grid points
 * whose model is of type surf
 * @param {Array} models
 * @param {Arrray} gridPoints
 * @return {Array}
 */
export const filterSurfModels = (models, gridPoints) =>
  gridPoints.filter((gridPoint) => isSurfModel(models, gridPoint));

/**
 * Accepts an array of model descriptors and
 * model grid points and returns only grid points
 * whose model is of type swell
 * @param {Array} models
 * @param {Arrray} gridPoints
 * @return {Array}
 */
export const filterSwellModels = (models, gridPoints) =>
  gridPoints.filter((gridPoint) => isSwellModel(models, gridPoint));

/**
 * Accepts an array of grid point objects and
 * checks if they are uniformly configured
 * based on the following properties:
 * - optimalSwellDirection
 * - offshoreDirection
 * - breakingWaveHeightAlgorithm
 * - breakingWaveHeightCoefficient
 * - breakingWaveHeightIntercept
 * @param {Array} gridPoints
 * @return {Boolean}
 */
export const areGridPointsUniform = ([firstGridPoint, ...remainingGridPoints]) => {
  const comparisonProperties = [
    'optimalSwellDirection',
    'offshoreDirection',
    'breakingWaveHeightAlgorithm',
    'breakingWaveHeightCoefficient',
    'breakingWaveHeightIntercept',
  ];

  const comparator = pick(firstGridPoint, comparisonProperties);

  return remainingGridPoints.reduce(
    (allEqual, gridPoint) => allEqual && isEqual(comparator, pick(gridPoint, comparisonProperties)),
    true,
  );
};

/**
 * Accepts an array of grid point objects and returns
 * a mutated version where each grid point has had
 * all properties from overrides applied
 * @param {Array} gridPoints
 * @param {Object} overrides
 */
export const bulkUpdateGridPoints = (gridPoints, overrides) =>
  gridPoints.map((gridPoint) => ({
    ...gridPoint,
    ...overrides,
  }));

/**
 * Fetches all Cameras
 */
export const fetchCameras = async () => {
  let cameras;
  try {
    cameras = await fetchCamerasFromAPI();
  } catch (error) {
    cameras = [];
  }
  return [
    { text: 'None', value: '', __typeName: 'SelectOption' },
    ...cameras.map((camera) => ({
      text: camera.title,
      value: camera._id,
      __typeName: 'SelectOption',
    })),
  ];
};

/**
 * Fetches all Subregions
 */
export const fetchSubregions = async () => {
  let subRegions;
  try {
    subRegions = await fetchSubregionsFromAPI();
  } catch {
    subRegions = [];
  }

  return [
    { text: 'Select a Subregion', value: '', __typeName: 'SelectOption' },
    ...subRegions.map((subregion) => ({
      text: subregion.name,
      value: subregion._id,
      __typeName: 'SelectOption',
    })),
  ];
};

/**
 * Fetches at most, 100 spots within 100 miles of a given location
 * @param  {} coordinates the coordinates of the spot
 */
export const fetchNearbySpots = async ({ lat, lon }) => {
  let nearbySpots;

  try {
    nearbySpots = await fetchSpots({
      coordinates: [lon, lat],
      distance: 100, // Specify in miles
      limit: 100,
      select: 'name',
      filter: 'nearby',
      report: 'ON',
    });
  } catch {
    nearbySpots = [];
  }

  return [
    {
      text: 'Select a Nearby Spot',
      value: '',
      __typeName: 'SelectOption',
    },
    ...nearbySpots.map((nearbySpot) => ({
      text: nearbySpot.name,
      value: nearbySpot._id,
      __typeName: 'SelectOption',
    })),
  ];
};

/**
 * Uploads a thumbnail to the server
 * @param  {} spotId The id of the spot that the thumbnail is for
 * @param  {} file the thumbnail to be uploaded
 */
export const uploadThumbnail = (spotId, file) =>
  slCommonFetch(`spots/${spotId}/upload`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      credentials: 'same-origin',
    },
    body: file,
  });

/**
 * Converts a range Object to a string
 * @param  {object} ranges a range object
 */
export const rangesString = (ranges) => {
  if (ranges) {
    return ranges.map((range) => `${range.min}-${range.max}`).join(', ');
  }
  return '';
};

/**
 * Converts a range string to an object
 * @param  {String} ranges a range string
 */
export const rangesObjects = (ranges) => {
  if (ranges) {
    return ranges.split(',').map((range) => {
      // eslint-disable-next-line no-useless-escape
      const [min, max] = range.replace(/[\[\]]/gm, '').split('-');
      return {
        min: Number(min),
        max: Number(max),
      };
    });
  }
  return [];
};

/*
 * Converts an object to a string in url query paramter format
 * @param {Object} obj
 */
export const queryParams = (obj) =>
  `?${Object.keys(obj)
    .map((key) => `${key}=${obj[key]}`)
    .join('&')}`;

/**
 * Accepts an object and returns it with
 * all number values converted to strings
 * @param {Object} object
 */
export const makeEverythingAString = (object) =>
  Object.keys(object).reduce(
    (accumulated, key) => ({
      ...accumulated,
      [key]: typeof object[key] === 'number' ? `${object[key]}` : object[key],
    }),
    {},
  );

export const oppositeAngle = (value) => (+value + 180) % 360;

export const areAnglesOpposite = (valA, valB) => oppositeAngle(valA) === +valB;

export const areValuesZero = (valA, valB) => valA === '0' && valB === '0';

export const noSlash = (url) => (url.endsWith('/') ? url.slice(0, -1) : url);

// allows inline && comparison without having 0 return false
const hasValidValue = (val) => val !== null && val !== undefined;

export const validateOffshoreAndOptimalSwellDirections = (
  offshoreDirection,
  optimalSwellDirection,
) =>
  (hasValidValue(offshoreDirection) &&
    hasValidValue(optimalSwellDirection) &&
    offshoreDirection !== optimalSwellDirection) ||
  'Offshore and optimal swell directions are required and must not be equal.';

/**
 * Ensures input is valid JSON
 * @param  {String} ratingsOptions a JSON string
 */
export const validateJSON = (ratingsOptions) => {
  try {
    JSON.parse(ratingsOptions);
  } catch (e) {
    return false;
  }
  return true;
};
