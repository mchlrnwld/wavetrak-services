import { useState, useEffect } from 'react';
import { useApolloClient, useQuery, useMutation } from '@apollo/client';
import { rangesString, fetchNearbySpots } from '../utils';
import { createSLSpotInput } from '../graphql/utils';
import { useTimezones, useSubregions } from './remoteDataHooks';
import { querySLSpot, upsertSLSpot } from '../graphql/schema';

const spotTypes = [
  { name: 'Surf Break', type: 'SURFBREAK' },
  { name: 'Wave Pool', type: 'WAVEPOOL' },
  { name: 'Ski Slope', type: 'SKISLOPE' },
];

const slSpotFormValues = (spot) => {
  const defaultSpotVals = {
    name: '',
    lat: '',
    lon: '',
    boardTypes: [],
    abilityLevels: [],
    best: {},
    humanReport: { clone: {}, average: {} },
    status: 'DRAFT',
  };

  const values = spot || defaultSpotVals;

  return {
    ...values,
    swellWindow: rangesString(values.swellWindow),
    best: {
      windDirection: rangesString(values.best.windDirection),
      swellPeriod: rangesString(values.best.swellPeriod),
      swellWindow: rangesString(values.best.swellWindow),
      sizeRange: rangesString(values.best.sizeRange),
    },
    boardTypes: values.boardTypes
      ? values.boardTypes.map((val) => ({
          _id: val,
        }))
      : [],
    abilityLevels: values.abilityLevels
      ? values.abilityLevels.map((val) => ({
          _id: val,
        }))
      : [],
  };
};

export const useSLSpotData = (pointOfInterestId) => {
  const {
    data: slData,
    loading,
    error,
  } = useQuery(querySLSpot, {
    variables: {
      pointOfInterestId,
    },
  });

  const { timezones } = useTimezones();
  const { subregions } = useSubregions();

  const spots = slData?.pointOfInterest.spots.filter(({ brand }) => brand === 'SL');
  const spotExists = spots && !!spots[0];

  // Returns an object of lat & lon
  // values for either the MSW spot if it exists
  // or the POI, if no MSW spot exists
  const getLatLon = () => {
    if (spotExists) {
      const [{ lat, lon }] = spots;
      return { lat, lon };
    }

    return {
      lat: slData ? slData.pointOfInterest.lat : undefined,
      lon: slData ? slData.pointOfInterest.lon : undefined,
    };
  };

  const getSpotData = () => (spotExists ? slSpotFormValues(spots[0]) : undefined);

  const getPOIData = () =>
    slData
      ? {
          name: slData.pointOfInterest.name,
          lat: slData.pointOfInterest.lat,
          lon: slData.pointOfInterest.lon,
          gridPoints: slData.pointOfInterest.gridPoints,
          defaultForecastModels: slData.pointOfInterest.defaultForecastModels,
        }
      : undefined;

  return {
    loading,
    error,
    spotExists,
    ...getLatLon(),
    spotTypes,
    timezones,
    subregions,
    spotData: getSpotData(),
    poiData: getPOIData(),
    cams: spotExists ? spots[0].cams : [],
    weatherStations: spotExists ? spots[0].weatherStations : [],
  };
};

export const useNearbySpots = (coordinates) => {
  const [nearbySpotReportOptions, setNearbySpotReportOptions] = useState([]);

  useEffect(() => {
    const setNearbySpots = async () => {
      setNearbySpotReportOptions(
        await fetchNearbySpots({ lat: coordinates.lat, lon: coordinates.lon }),
      );
    };
    if (coordinates) {
      setNearbySpots();
    }
  }, [coordinates]);

  return {
    nearbySpotReportOptions,
  };
};

export const useSLSpotUpsert = (pointOfInterestId, withTravel) => {
  const client = useApolloClient();
  const [upsertStatus, setUpsertStatus] = useState(undefined);
  const [upsertSpot, { loading: upsertingSLSpot }] = useMutation(upsertSLSpot);

  const upsert = async (formData) => {
    const {
      pointOfInterest: { spots },
    } = client.readQuery({
      query: querySLSpot,
      variables: {
        pointOfInterestId,
      },
    });

    const [cachedSpot] = spots.filter(({ brand }) => brand === 'SL');
    const updatedSpot = createSLSpotInput(pointOfInterestId, cachedSpot, formData, withTravel);

    try {
      setUpsertStatus(undefined);
      const {
        data: {
          upsertSLSpot: { success, message },
        },
      } = await upsertSpot({
        variables: {
          spot: updatedSpot,
        },
      });

      setUpsertStatus({
        success,
        message,
      });
    } catch (error) {
      setUpsertStatus({
        success: false,
        message: 'Error upserting SL Spot',
      });
    }
  };

  return {
    upsert,
    upsertStatus,
    upsertingSLSpot,
  };
};

const travelDetailsFormValues = ({ pointOfInterest: { spots } }) => {
  const [spot] = spots.filter(({ brand }) => brand === 'SL');

  return spot.travelDetails
    ? {
        ...spot,
        travelDetails: {
          ...spot.travelDetails,
          status: spot.travelDetails.status || 'DRAFT',
        },
      }
    : {
        ...spot,
        travelDetails: {
          crowdFactor: {},
          localVibe: {},
          paddleRating: {},
          surfRating: {},
          waterQuality: {},
          best: {
            swellDirection: {},
            windDirection: {},
            size: {},
            tide: {},
            season: {},
          },
          breakType: [],
          bottom: {},
          status: 'DRAFT',
        },
      };
};

export const useTravelData = (pointOfInterestId) => {
  const [data, setData] = useState();

  const {
    data: slSpotData,
    loading,
    error,
  } = useQuery(querySLSpot, {
    variables: { pointOfInterestId },
  });

  useEffect(() => {
    if (slSpotData) {
      setData(travelDetailsFormValues(slSpotData));
    }
  }, [slSpotData]);

  return { data, loading, error };
};
