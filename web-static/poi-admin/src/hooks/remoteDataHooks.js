import { useState, useEffect } from 'react';
import { fetchTimezones, fetchSubregions } from '../utils';
import { fetchSharedTideServiceTidePorts } from '../fetch';

export const useSubregions = () => {
  const [loading, setLoading] = useState(true);
  const [subregions, setSubregions] = useState();

  useEffect(() => {
    let componentMounted = true;

    const loadSubregions = async () => {
      try {
        const response = await fetchSubregions();

        if (componentMounted) {
          setSubregions(response);
          setLoading(false);
        }
      } catch (error) {
        if (componentMounted) {
          setLoading(false);
          setSubregions([]);
        }
      }
    };

    loadSubregions();

    return () => {
      componentMounted = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    loading,
    subregions,
  };
};

/**
 * @param {{ lat: number, lon: number }} [coords]
 */
export const useTidePorts = (coords) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState();
  const [nearestTidePorts, setNearestTidePorts] = useState([
    {
      text: 'NONE',
      value: '',
    },
  ]);

  useEffect(() => {
    let componentMounted = true;

    const fetchPorts = async (coordinates) => {
      try {
        const ports = await fetchSharedTideServiceTidePorts(coordinates);

        if (componentMounted) {
          setNearestTidePorts([...ports.map(({ name }) => ({ text: name, value: name }))]);
        }
      } catch (err) {
        setError('Something went wrong fetching tide ports.');
      } finally {
        setLoading(false);
      }
    };

    if (coords) {
      fetchPorts(coords);
    }

    return () => {
      componentMounted = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [coords]);

  return {
    loading,
    nearestTidePorts,
    error,
  };
};

export const useTimezones = () => {
  const [loading, setLoading] = useState(true);
  const [timezones, setTimezones] = useState([]);

  useEffect(() => {
    let componentMounted = true;

    const fetchAllTimezones = async () => {
      try {
        const timezoneResponse = await fetchTimezones();

        if (componentMounted) {
          setTimezones(
            timezoneResponse.map((timezone) => ({
              text: timezone,
              value: timezone.replace(' ', '_'),
            })),
          );

          setLoading(false);
        }
      } catch (error) {
        if (componentMounted) {
          setTimezones([]);
        }
      }
    };

    fetchAllTimezones();

    return () => {
      componentMounted = false;
    };
  }, []);

  return {
    loading,
    timezones,
  };
};
