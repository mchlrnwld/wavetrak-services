export default {
  development: {
    adminServicesEndpoint: 'https://tools.sandbox.surflineadmin.com/api',
    mswEndpoint: 'https://sandbox.magicseaweed.com/api/mdkey/',
    mswCredentials: 'msw:bantham',
    spotsCms: '',
    forecastEndpoint: 'https://services.sandbox.surfline.com/forecasts',
    graphqlGateway: 'https://services.sandbox.surfline.com/graphql',
  },
  sandbox: {
    adminServicesEndpoint: 'https://tools.sandbox.surflineadmin.com/api',
    mswEndpoint: 'https://sandbox.magicseaweed.com/api/mdkey/',
    mswCredentials: 'msw:bantham',
    spotsCms: '',
    forecastEndpoint: 'https://services.sandbox.surfline.com/forecasts',
    graphqlGateway: 'https://services.sandbox.surfline.com/graphql',
  },
  staging: {
    adminServicesEndpoint: 'https://tools.staging.surflineadmin.com/api',
    mswEndpoint: 'https://sandbox.magicseaweed.com/api/mdkey/',
    mswCredentials: 'msw:bantham',
    spotsCms: '',
    forecastEndpoint: 'https://services.staging.surfline.com/forecasts',
    graphqlGateway: 'https://services.staging.surfline.com/graphql',
  },
  production: {
    adminServicesEndpoint: 'https://tools.surflineadmin.com/api',
    mswEndpoint: 'https://magicseaweed.com/api/mdkey/',
    spotsCms: '',
    forecastEndpoint: 'https://services.surfline.com/forecasts',
    graphqlGateway: 'https://services.surfline.com/graphql',
  },
}[process.env.APP_ENV || 'development'];
