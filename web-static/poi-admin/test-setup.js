import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

process.env.NEW_RELIC_ENABLED = false;
process.env.NEW_RELIC_NO_CONFIG_FILE = true;
