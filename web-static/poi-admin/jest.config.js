module.exports = {
  setupFiles: ['jest-canvas-mock'],
  setupFilesAfterEnv: ['<rootDir>/test-setup.js'],
  collectCoverage: true,
  coverageReporters: ['lcov', 'text-summary'],
  collectCoverageFrom: ['src/**/*!(.spec).js'],
  moduleNameMapper: {
    '\\.(svg)$': '<rootDir>/moduleStub.js',
    "\\.(css|less|scss)$": "identity-obj-proxy",
  },
};
