# Offer Funnel

## Description

Offer Funnel frontend App. Created with NextJS + React + Redux.

Uses: node version 14.

### Development

```bash
npm install
npm run dev
```

Similarly to the Promotions app, the Offer app also requires a valid `promotionId` to load the funnel. You can get the `promotionId` from Promotions CMS.
For local development make sure to set the `promotionId` in `store/actions/promotions.js`

```
const { promotionId } = window; // remove this line in store/actions/promotions.js and insert following line
const promotionId = <promotionId for the Offer>; // Replace with a valid promotionId
```

In Staging/Sandbox/Prod the `promotionId` would be set automatically in the window scope by the web-proxy based on the offer url.

The site would be available on `http://localhost:3000`

### Deployment

TODO: add deployment info
