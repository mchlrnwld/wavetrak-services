/* eslint-disable import/no-extraneous-dependencies */
const withPlugins = require('next-compose-plugins');
const withSass = require('@zeit/next-sass');
const withImages = require('next-images');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});
const getConfig = require('./src/config');

const appConfig = getConfig();
const { cdnHost, brand, APP_ENV } = appConfig;
const assetDir = APP_ENV === 'development' ? '' : 'static/';
const sassData = [`$cdnHostname: '${cdnHost}${assetDir}';`, `$companyTheme: '${brand}';`].join('');
const distDir = `build/${brand}`;
const nextConfig = {
  trailingSlash: true,
  assetPrefix: cdnHost,
  distDir,
  env: {
    APP_ENV,
    BRAND: brand,
  },
  webpack(config) {
    config.module.rules.push({
      test: /.(png|woff(2)?|eot|ttf|svg)(\?[a-z0-9=\.]+)?$/,
      use: {
        loader: 'file-loader',
      },
    });
    if (config.mode === 'production') {
      if (Array.isArray(config.optimization.minimizer)) {
        config.optimization.minimizer.push(new OptimizeCSSAssetsPlugin({}));
      }
    }
    return config;
  },
};

module.exports = withPlugins(
  [
    withImages,
    [
      withSass,
      {
        sassLoaderOptions: {
          data: sassData,
        },
      },
    ],
    withBundleAnalyzer,
  ],
  nextConfig,
);
