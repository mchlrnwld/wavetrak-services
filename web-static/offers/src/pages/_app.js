import { Provider } from 'react-redux';
import { useMount } from 'react-use';
import PropTypes from 'prop-types';
import { useState } from 'react';
import store from '../store';
import getConfig from '../config';
import { AppHead, Layout } from '../components';
import { fetchPromotion } from '../store/actions/promotion';
import { loadUser, fetchCountryCode } from '../store/actions/user';
import { fetchCurrency } from '../store/actions/subscription';
import Loading from '../components/Loading';

const config = getConfig();

const OfferApp = ({ Component, pageProps }) => {
  const [appReady, setAppReady] = useState(false);

  useMount(async () => {
    await store.dispatch(loadUser());
    await store.dispatch(fetchCountryCode());
    await Promise.all([[fetchCurrency, fetchPromotion].map((action) => store.dispatch(action()))]);
    setAppReady(true);
  });

  return (
    <Provider store={store}>
      <AppHead config={config} />
      <Layout>{appReady ? <Component {...pageProps} appReady={appReady} /> : <Loading />}</Layout>
    </Provider>
  );
};

OfferApp.propTypes = {
  pageProps: PropTypes.shape({}).isRequired,
  Component: PropTypes.func.isRequired,
};

export default OfferApp;
