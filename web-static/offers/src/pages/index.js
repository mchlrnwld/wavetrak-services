import dynamic from 'next/dynamic';
import Loading from '../components/Loading';

const OfferContainer = dynamic(import('../components/Offer/Offer'), {
  loading: () => <Loading />,
  ssr: false,
});

const OfferFunnel = (props) => <OfferContainer {...props} />;

export default OfferFunnel;
