export const getAccessToken = (state) => {
  const { accessToken } = state.auth;
  if (accessToken) return accessToken;
  return null;
};

export const getLoginError = (state) => {
  if (state) {
    const {
      login: { loginError },
    } = state;
    return loginError;
  }
  return null;
};

export const getFbLoginError = (state) => {
  if (state) {
    const {
      facebook: { fbLoginError },
    } = state;
    return fbLoginError;
  }
  return null;
};

export const getConfirmEmail = (state) => {
  if (state) {
    const {
      login: { confirmEmail },
    } = state;
    return confirmEmail;
  }
  return null;
};
