export const getOfferSuccess = (state) => {
  if (state) {
    const {
      offer: { success },
    } = state;
    return success;
  }
  return null;
};
