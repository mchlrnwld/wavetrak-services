import getConfig from '../../config';

const config = getConfig();

export const getTrialEligibility = (state) => {
  const { company, funnelConfig } = config;
  if (funnelConfig) {
    if (funnelConfig.trialLength === 0) return false;
  }
  const data = state.entitlements.promotions;
  if (data && data.includes(`${company}_free_trial`)) return true;
  return false;
};
