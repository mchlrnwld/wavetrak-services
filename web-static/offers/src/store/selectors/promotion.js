export const getFunnelConfig = (state) => {
  if (state) {
    const {
      promotion: { funnelConfig },
    } = state;
    return funnelConfig;
  }
  return null;
};

export const getPromotionId = (state) => {
  if (state) {
    const {
      promotion: {
        funnelConfig: { _id: promotionId },
      },
    } = state;
    return promotionId;
  }
  return null;
};

export const getOfferEligibility = (state) => {
  if (state) {
    const {
      offer: { isEligible },
    } = state;
    return isEligible;
  }
  return null;
};

export const getOfferDetails = (state) => {
  if (state) {
    const {
      offer: { details },
    } = state;
    return details;
  }
  return null;
};

export const getOfferError = (state) => {
  if (state) {
    const {
      offer: { error },
    } = state;
    return error;
  }
  return null;
};
