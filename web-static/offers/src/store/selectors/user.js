export const getTrialEligibility = (state) => {
  const { promotion } = state;
  const { funnelConfig } = promotion;
  if (funnelConfig) {
    if (funnelConfig.trialLength === 0) return false;
  }
  const { brand } = funnelConfig;
  const data = state.user.promotions;
  if (data && data.includes(`${brand}_free_trial`)) return true;
  return false;
};

export const getCountryCode = (state) => state.user?.countryCode;
