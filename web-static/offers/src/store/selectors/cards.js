export const getCreditCards = (state) => {
  const { cards } = state.cards;
  if (cards) return cards;
  return null;
};

export const getDefaultSource = (state) => {
  const { defaultSource } = state.cards;
  if (defaultSource) return defaultSource;
  return null;
};

export const getSelectedCardId = (state) => {
  const { selectedCard } = state.cards;
  if (selectedCard) return selectedCard;
  return null;
};

export const getShowCards = (state) => {
  const { showCards } = state.cards;
  if (showCards || showCards === false) return showCards;
  return null;
};

export const getCardError = (state) => {
  const { error } = state.cards;
  if (error) return error;
  return null;
};
