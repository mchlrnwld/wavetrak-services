export const isFormSubmitting = (state) => {
  if (state) {
    const {
      interactions: { submitting },
    } = state;
    return submitting;
  }
  return null;
};

export const isFBFormSubmitting = (state) => {
  if (state) {
    const {
      interactions: { fbSubmitting },
    } = state;
    return fbSubmitting;
  }
  return null;
};
