import getConfig from '../../config';
import { getEntitlement } from '../../common/api/entitlement';
import { getAccessToken } from '../../utils/getAccessToken';

const config = getConfig();

export const FETCH_ENTITLEMENTS = 'FETCH_ENTITLEMENTS';
export const FETCH_ENTITLEMENTS_SUCCESS = 'FETCH_ENTITLEMENTS_SUCCESS';
export const FETCH_ENTITLEMENTS_FAIL = 'FETCH_ENTITLEMENTS_FAIL';
export const DISABLE_FREE_TRIAL = 'DISABLE_FREE_TRIAL';
export const ENABLE_FREE_TRIAL = 'ENABLE_FREE_TRIAL';

export const fetchEntitlements = () => async (dispatch) => {
  dispatch({ type: FETCH_ENTITLEMENTS });
  try {
    const accessToken = getAccessToken();
    const { entitlements, promotions } = await getEntitlement(accessToken);
    dispatch({ type: FETCH_ENTITLEMENTS_SUCCESS, entitlements, promotions });
  } catch (error) {
    dispatch({ type: FETCH_ENTITLEMENTS_FAIL, error });
  }
};

export const disableFreeTrial = () => async (dispatch) =>
  dispatch({ type: DISABLE_FREE_TRIAL, company: config.company });

export const enableFreeTrial = () => async (dispatch) =>
  dispatch({ type: ENABLE_FREE_TRIAL, company: config.company });
