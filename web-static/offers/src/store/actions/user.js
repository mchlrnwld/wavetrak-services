import { getUser } from '../../common/api/user';
import { validateAccessToken } from './auth';
import { fetchEntitlements } from './entitlements';
import { getAccessToken } from '../../utils/getAccessToken';
import { getLocation } from '../../common/api/geo';

export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';
export const FETCH_PLANS_FAIL = 'FETCH_PLANS_FAIL';
export const SET_COUNTRY_CODE = 'SET_COUNTRY_CODE';

export const fetchUser = () => async (dispatch) => {
  try {
    dispatch({ type: FETCH_USER });
    let user;
    const accessToken = getAccessToken();
    if (accessToken) user = await getUser(accessToken);
    return dispatch({ type: FETCH_USER_SUCCESS, user, accessToken });
  } catch (err) {
    const { message } = err;
    return dispatch({ type: FETCH_USER_FAILURE, message });
  }
};

export const loadUser = () => async (dispatch) => {
  const accessToken = getAccessToken();
  if (!accessToken) return false;

  const { isValid } = await dispatch(validateAccessToken(accessToken));
  if (isValid) {
    await dispatch(fetchUser());
    await dispatch(fetchEntitlements());
  }
  return true;
};

/**
 * @returns {Promise<{ countryCode: string, type: string }>}
 */
export const fetchCountryCode = () => async (dispatch) => {
  const accessToken = getAccessToken();
  try {
    const {
      country: { iso_code: countryCode },
    } = await getLocation(accessToken);
    return dispatch({ type: SET_COUNTRY_CODE, countryCode });
  } catch {
    return dispatch({ type: SET_COUNTRY_CODE, countryCode: 'US' });
  }
};
