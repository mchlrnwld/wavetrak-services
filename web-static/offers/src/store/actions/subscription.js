import { getCurrencyForUser } from '../../common/api/subscription';
import { getCountryCode } from '../selectors/user';

export const FETCH_CURRENCY = 'FETCH_CURRENCY';
export const FETCH_CURRENCY_SUCCESS = 'FETCH_CURRENCY_SUCCESS';
export const FETCH_CURRENCY_FAIL = 'FETCH_CURRENCY_FAIL';

export const fetchCurrency = () => async (dispatch, getState) => {
  try {
    dispatch({ type: FETCH_CURRENCY });

    const countryCode = getCountryCode(getState());

    const { currency } = await getCurrencyForUser(countryCode);
    return dispatch({
      type: FETCH_CURRENCY_SUCCESS,
      currency,
    });
  } catch (err) {
    const { message } = err;
    return dispatch({ type: FETCH_CURRENCY_FAIL, message });
  }
};
