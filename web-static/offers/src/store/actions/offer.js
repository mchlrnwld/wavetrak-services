import { trackEvent } from '@surfline/web-common';
import { getOffer, acceptOffer as claimOffer } from '../../common/api/subscription';
import { stopSubmitSpinner } from './interactions';
import { getFunnelConfig } from '../selectors/promotion';
import getConfig from '../../config';
import { getAccessToken } from '../../utils/getAccessToken';

const config = getConfig();

export const FETCH_OFFER = 'FETCH_OFFER';
export const FETCH_OFFER_SUCCESS = 'FETCH_OFFER_SUCCESS';
export const FETCH_OFFER_FAIL = 'FETCH_OFFER_FAIL';
export const ACCEPT_OFFER = 'ACCEPT_OFFER';
export const ACCEPT_OFFER_SUCCESS = 'ACCEPT_OFFER_SUCCESS';
export const ACCEPT_OFFER_FAIL = 'ACCEPT_OFFER_FAIL';

export const fetchOffer = () => async (dispatch, getState) => {
  dispatch({ type: FETCH_OFFER });
  try {
    const funnelConfig = getFunnelConfig(getState());
    const { _id: promotionId } = funnelConfig;
    const { company } = config;
    const accessToken = getAccessToken();
    const details = await getOffer(accessToken, company, promotionId);
    return dispatch({ type: FETCH_OFFER_SUCCESS, details });
  } catch (error) {
    return dispatch({ type: FETCH_OFFER_FAIL, error });
  }
};

export const acceptOffer = (values) => async (dispatch) => {
  dispatch({ type: ACCEPT_OFFER });
  try {
    const accessToken = getAccessToken();
    const { brand, promotionId, source, selectedCard, defaultCardId } = values;

    const offerApiValues = {
      source: source ? source.id : null,
      cardId: selectedCard !== defaultCardId ? selectedCard : defaultCardId,
    };
    await claimOffer(accessToken, brand, promotionId, offerApiValues);
    trackEvent('Completed Checkout Step', { step: 2, promotionId });
    dispatch(stopSubmitSpinner());
    return dispatch({ type: ACCEPT_OFFER_SUCCESS });
  } catch (error) {
    const { message } = error;
    dispatch(stopSubmitSpinner());
    return dispatch({ type: ACCEPT_OFFER_FAIL, error: message });
  }
};
