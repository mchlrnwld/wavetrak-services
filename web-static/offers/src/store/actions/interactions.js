export const START_SUBMIT_SPINNER = 'START_SUBMIT_SPINNER';
export const STOP_SUBMIT_SPINNER = 'STOP_SUBMIT_SPINNER';

export const startSubmitSpinner = () => async (dispatch) => {
  dispatch({ type: START_SUBMIT_SPINNER });
};

export const stopSubmitSpinner = () => async (dispatch) => {
  dispatch({ type: STOP_SUBMIT_SPINNER });
};
