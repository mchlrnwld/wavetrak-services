import createReducer from './createReducer';
import { START_SUBMIT_SPINNER, STOP_SUBMIT_SPINNER } from '../actions/interactions';

const initialState = {
  showLogin: false,
  showOfferLogin: true,
  submitting: false,
};
const handlers = {};

handlers[START_SUBMIT_SPINNER] = (state) => ({
  ...state,
  submitting: true,
});

handlers[STOP_SUBMIT_SPINNER] = (state) => ({
  ...state,
  submitting: false,
});

export default createReducer(handlers, initialState);
