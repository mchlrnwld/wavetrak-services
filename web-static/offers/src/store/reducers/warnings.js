import createReducer from './createReducer';
import { UPDATE_WARNING } from '../actions/warnings';

const initialState = {
  warnings: null,
};

const handlers = {};

handlers[UPDATE_WARNING] = (warnings, { cardId: updatedCard }) => {
  const updatedWarnings = warnings.filter(({ cardId }) => cardId !== updatedCard);
  return updatedWarnings;
};

export default createReducer(handlers, initialState);
