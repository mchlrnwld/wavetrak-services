import createReducer from './createReducer';
import {
  FETCH_PROMOTION,
  FETCH_PROMOTION_SUCCESS,
  FETCH_PROMOTION_FAILURE,
} from '../actions/promotion';

const initialState = {
  error: null,
  loading: false,
};
const handlers = {};

handlers[FETCH_PROMOTION] = (state) => ({
  ...state,
  loading: true,
});

handlers[FETCH_PROMOTION_SUCCESS] = (state, { promotion, funnelConfig }) => ({
  ...state,
  ...promotion,
  funnelConfig,
  error: null,
  loading: false,
});

handlers[FETCH_PROMOTION_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  loading: false,
});

export default createReducer(handlers, initialState);
