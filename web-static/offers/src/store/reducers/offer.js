import createReducer from './createReducer';
import {
  FETCH_OFFER,
  FETCH_OFFER_SUCCESS,
  FETCH_OFFER_FAIL,
  ACCEPT_OFFER,
  ACCEPT_OFFER_SUCCESS,
  ACCEPT_OFFER_FAIL,
} from '../actions/offer';

const initialState = {
  offerDetails: null,
  isEligible: null,
  success: false,
};
const handlers = {};

handlers[FETCH_OFFER] = (state) => ({
  ...state,
  loading: true,
});

handlers[FETCH_OFFER_SUCCESS] = (state, { details }) => ({
  ...state,
  details,
  isEligible: true,
  loading: false,
  error: null,
});

handlers[FETCH_OFFER_FAIL] = (state, { error }) => ({
  ...state,
  loading: false,
  isEligible: false,
  error,
});

handlers[ACCEPT_OFFER] = (state) => ({
  ...state,
  error: null,
});

handlers[ACCEPT_OFFER_SUCCESS] = (state) => ({
  ...state,
  error: null,
  success: true,
});

handlers[ACCEPT_OFFER_FAIL] = (state, { error }) => ({
  ...state,
  error,
});

export default createReducer(handlers, initialState);
