import createReducer from './createReducer';
import {
  FETCH_CURRENCY,
  FETCH_CURRENCY_SUCCESS,
  FETCH_CURRENCY_FAIL,
} from '../actions/subscription';

const initialState = {
  error: null,
  success: null,
};
const handlers = {};

handlers[FETCH_CURRENCY] = (state) => ({
  ...state,
});

handlers[FETCH_CURRENCY_SUCCESS] = (state, { currency }) => ({
  ...state,
  currency: currency.toLowerCase(),
});

handlers[FETCH_CURRENCY_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
});

export default createReducer(handlers, initialState);
