import createReducer from './createReducer';

import { SET_COUNTRY_CODE } from '../actions/user';

const initialState = {
  user: null,
  countryCode: null,
};
const handlers = {};

handlers[SET_COUNTRY_CODE] = (state, { countryCode }) => ({
  ...state,
  countryCode,
});

export default createReducer(handlers, initialState);
