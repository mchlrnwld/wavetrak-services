import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import user from './user';
import promotion from './promotion';
import interactions from './interactions';
import cards from './cards';
import auth from './auth';
import facebook from './facebook';
import entitlements from './entitlements';
import login from './login';
import subscription from './subscription';
import offer from './offer';

export default combineReducers({
  user,
  promotion,
  interactions,
  cards,
  auth,
  facebook,
  entitlements,
  login,
  subscription,
  offer,
  form: formReducer,
});
