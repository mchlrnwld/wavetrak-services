import createReducer from './createReducer';
import {
  FETCH_CARDS,
  FETCH_CARDS_FAIL,
  FETCH_CARDS_SUCCESS,
  TOGGLE_SHOW_CARDS,
  SET_SELECTED_CARD_SOURCE,
} from '../actions/cards';

const initialState = {
  cards: null,
  defaultSource: null,
  selectedCard: null,
  showCards: null,
  showNewCardForm: null,
  submitting: false,
  cardToEdit: null,
  error: null,
  editCardError: null,
};

const handlers = {};

handlers[FETCH_CARDS] = (state) => ({
  ...state,
});

handlers[FETCH_CARDS_SUCCESS] = (state, { cards, defaultSource, showCards }) => ({
  ...state,
  cards,
  defaultSource,
  selectedCard: defaultSource,
  showCards,
});

handlers[FETCH_CARDS_FAIL] = (state, { error }) => ({
  ...state,
  error,
});

handlers[TOGGLE_SHOW_CARDS] = (state, { showCards }) => ({
  ...state,
  showCards,
});

handlers[SET_SELECTED_CARD_SOURCE] = (state, { selectedCard }) => ({
  ...state,
  selectedCard,
});

export default createReducer(handlers, initialState);
