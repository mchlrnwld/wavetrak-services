import fetch from '../../utils/fetch';

export const getPromotion = (id) => fetch(`promotions/${id}`, null, { method: 'GET' });

export default getPromotion;
