import fetch from '../../utils/fetch';

export const getCards = (accessToken) => {
  const urlPath = '/credit-cards';
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const getOffer = (accessToken, brand, promotionId) => {
  const urlPath = `/subscriptions/retention-offer?brand=${brand}&promotionId=${promotionId}`;
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const acceptOffer = (accessToken, brand, promotionId, values) => {
  const urlPath = `/subscriptions/retention-offer?brand=${brand}&promotionId=${promotionId}`;
  return fetch(urlPath, accessToken, {
    method: 'POST',
    body: JSON.stringify({ ...values }),
  });
};

export const getCurrencyForUser = (countryISO) => {
  const urlPath = `/subscriptions/currency?countryISO=${countryISO}`;
  return fetch(urlPath, null, { method: 'GET' });
};
