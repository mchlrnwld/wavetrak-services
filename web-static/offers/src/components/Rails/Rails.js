import PropTypes from 'prop-types';
import { RailLogo } from '..';
import './Rails.scss';

export const LeftRail = ({ children }) => (
  <div className="left-rail">
    <RailLogo />
    {children}
  </div>
);

LeftRail.propTypes = {
  children: PropTypes.node,
};

LeftRail.defaultProps = {
  children: null,
};

export const RightRail = ({ children }) => <div className="right-rail">{children}</div>;

RightRail.propTypes = {
  children: PropTypes.node,
};

RightRail.defaultProps = {
  children: null,
};
