import PropTypes from 'prop-types';
import getConfig from '../../config';

const config = getConfig();

const Disclaimer = () => (
  <p className="disclaimer">
    {'By starting a free trial or subscription, you agree to our '}
    <DisclaimerLink url={config.privacyPolicy} text="Privacy policy" />
    {' and '}
    <DisclaimerLink url={config.termsConditions} text="Terms of Use" />.
  </p>
);

const DisclaimerLink = ({ text, url }) => (
  <a className="disclaimer-link" href={url} target="_blank" rel="noopener noreferrer">
    {text}
  </a>
);

DisclaimerLink.propTypes = {
  text: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default Disclaimer;
