import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';
import { getFunnelConfig } from '../../store/selectors/promotion';
import './OfferHeader.scss';

const OfferHeader = ({ isLoggedIn }) => {
  const funnelConfig = useSelector(getFunnelConfig, shallowEqual);
  const { signUpHeader, paymentHeader, paymentMessage } = funnelConfig;
  const titleText = isLoggedIn ? paymentHeader : signUpHeader;
  const subtitleText = isLoggedIn ? paymentMessage : '';

  return (
    <div className="offer-header">
      <h1 className="offer-header__title">{titleText}</h1>
      <p className="offer-header__subtitle">{subtitleText}</p>
    </div>
  );
};

OfferHeader.propTypes = {
  isLoggedIn: PropTypes.bool,
};

OfferHeader.defaultProps = {
  isLoggedIn: false,
};

export default OfferHeader;
