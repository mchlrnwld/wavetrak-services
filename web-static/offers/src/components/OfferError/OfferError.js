import PropTypes from 'prop-types';
import { brandToString } from '@surfline/web-common';
import getConfig from '../../config';
import './OfferError.scss';

const config = getConfig();

const offerErrorMap = {
  1001: 'Your account is not eligible for this offer.',
  1002: 'Your subscription is not eligible for this offer.',
  1003: 'Your subscription is not eligible for this offer.',
  1004: 'The details of this offer have expired',
};

const OfferError = ({ error }) => {
  const { code, status, message } = error;
  const { redirectUrl, brand } = config;
  const errorMessage =
    status === 500
      ? 'We are currently experiencing issues with this offer.  Please try again.'
      : `${message}: ${offerErrorMap[code]}`;
  return (
    <div className="offer-error">
      <h4 className="offer-error__title">Oops! There was an issue loading this offer.</h4>
      <p className="offer-error__subtitle">{errorMessage}</p>
      <div className="offer-error__button">
        <a className="quiver-button" href={redirectUrl}>
          {`Return to ${brandToString(brand)}`}
        </a>
      </div>
    </div>
  );
};

OfferError.defaultProps = {
  error: {
    code: null,
    status: null,
    mesasge: null,
  },
};

OfferError.propTypes = {
  error: PropTypes.shape({
    code: PropTypes.number,
    status: PropTypes.number,
    message: PropTypes.string,
  }),
};

export default OfferError;
