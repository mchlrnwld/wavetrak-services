import { Spinner } from '@surfline/quiver-react';
import './Loading.scss';

const Loading = () => (
  <div className="sl-offers-loading">
    <Spinner />
  </div>
);
export default Loading;
