import { useCallback, useState, useEffect } from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { Button, OrBreak } from '@surfline/quiver-react';
import { trackEvent } from '@surfline/web-common';
import { CreditCard, Disclaimer } from '..';
import getConfig from '../../config';
import {
  getFunnelConfig,
  getPromotionId,
  getOfferEligibility,
} from '../../store/selectors/promotion';
import {
  getCreditCards,
  getDefaultSource,
  getSelectedCardId,
  getShowCards,
} from '../../store/selectors/cards';
import { isFormSubmitting } from '../../store/selectors/interactions';
import { setSelectedCardSource, toggleShowCards } from '../../store/actions/cards';
import { startSubmitSpinner } from '../../store/actions/interactions';
import { acceptOffer } from '../../store/actions/offer';
import './OfferForm.scss';

const config = getConfig();

const OfferForm = () => {
  const dispatch = useDispatch();
  const stripe = useStripe();
  const elements = useElements();

  // Selectors
  const funnelConfig = useSelector(getFunnelConfig, shallowEqual);
  const defaultCardId = useSelector(getDefaultSource);
  const cards = useSelector(getCreditCards, shallowEqual);
  const promotionId = useSelector(getPromotionId);
  const submitting = useSelector(isFormSubmitting);
  const showCards = useSelector(getShowCards);
  const selectedCardId = useSelector(getSelectedCardId, shallowEqual);
  const isEligible = useSelector(getOfferEligibility);

  // Dispatch actions
  const doStartSubmitSpinner = () => dispatch(startSubmitSpinner());
  const doAcceptOffer = (values) => dispatch(acceptOffer(values));
  const doSetSelectedCardSource = (cardId) => dispatch(setSelectedCardSource(cardId));
  const doToggleShowCards = (showCard) => dispatch(toggleShowCards(showCard));

  // State
  const [checkoutRendered, setCheckoutRendered] = useState(false);

  const viewedCheckout = useCallback(() => {
    if (!checkoutRendered) {
      setCheckoutRendered(true);
      trackEvent('Viewed Checkout Step', { step: 2, promotionId });
    }
  }, [checkoutRendered, promotionId]);

  useEffect(() => {
    viewedCheckout();
  }, [viewedCheckout]);

  const createOptions = (fontSize, padding) => ({
    style: {
      base: {
        fontSize,
        color: '#424770',
        letterSpacing: '0.025em',
        fontFamily: 'Source Sans Pro, Helvetica, sans-serif',
        '::placeholder': {
          color: '#aab7c4',
        },
        padding,
      },
      invalid: {
        color: '#9e2146',
      },
    },
  });

  const submitPaymentForm = async () => {
    const { brand } = config;
    doStartSubmitSpinner();
    let token;
    if (!stripe || !elements) return;
    const cardElement = elements.getElement(CardElement);
    if (cardElement) {
      const tokenObj = (!showCards && (await stripe.createToken(cardElement))) || null;
      token = tokenObj.token;
    }

    const acceptOfferValues = {
      brand,
      promotionId,
      defaultCardId,
      source: token,
      selectedCard: showCards ? selectedCardId : null,
    };
    await doAcceptOffer(acceptOfferValues);
  };

  const renderCards = () => {
    if (!cards || !showCards) {
      return (
        <div>
          <OrBreak text="Enter your credit card" />
          <CardElement {...createOptions('18px')} disabled={submitting} />
        </div>
      );
    }
    return (
      <div className="credit-card-list">
        <OrBreak text="Select a card" />
        {cards.map((card, index) => (
          <CreditCard
            card={card}
            selectedCard={selectedCardId}
            doSetSelectedCardSource={doSetSelectedCardSource}
            tabIndex={index}
            key={card.id}
          />
        ))}
      </div>
    );
  };

  const { buttonText } = funnelConfig;
  if (isEligible) {
    return (
      <div className="offer-form">
        <div className="offer-form__cards">
          <div className="credit-card-container">
            {renderCards()}
            {cards ? (
              <div>
                <button
                  type="button"
                  onClick={() => doToggleShowCards(showCards)}
                  onKeyDown={() => {}}
                  className="credit-card__form__toggle"
                >
                  {showCards ? '+ Add New Credit Card' : 'Use An Existing Credit card'}
                </button>
              </div>
            ) : null}
          </div>
        </div>
        <div className="offer-form__checkout">
          <Button
            disabled={submitting}
            style={{ width: '100%' }}
            loading={submitting}
            onClick={() => submitPaymentForm()}
          >
            {buttonText || 'Redeem offer'}
          </Button>
        </div>
        <div className="offer-form__disclaimer">
          <Disclaimer referenceText={buttonText} />
        </div>
      </div>
    );
  }
  return null;
};

export default OfferForm;
