import { useSelector, shallowEqual } from 'react-redux';
import PropTypes from 'prop-types';
import { getFunnelConfig } from '../../store/selectors/promotion';
import en from '../../international/translations/en';
import { RailCTA, MobileRailCTA, LeftRail, RightRail } from '..';
import './Layout.scss';

const Layout = ({ children }) => {
  const ctaCopy = {
    ...en.rail_cta,
    ...en.premiumFeatures,
  };

  const funnelConfig = useSelector(getFunnelConfig, shallowEqual);

  return funnelConfig ? (
    <div>
      <div className="offer-wrapper">
        <MobileRailCTA copy={ctaCopy} />
        <LeftRail>
          <div className="offer-wrapper__content">{children}</div>
        </LeftRail>
        <RightRail>
          <RailCTA copy={ctaCopy} />
        </RightRail>
      </div>
    </div>
  ) : null;
};

Layout.propTypes = {
  children: PropTypes.shape({}).isRequired,
};

export default Layout;
