import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { Alert } from '@surfline/quiver-react';
import { OfferDetails, OfferLoading, OfferError, OfferForm } from '..';
import {
  getOfferEligibility,
  getOfferDetails,
  getOfferError,
} from '../../store/selectors/promotion';
import { getCardError } from '../../store/selectors/cards';
import { fetchOffer } from '../../store/actions/offer';
import { fetchCards } from '../../store/actions/cards';
import getConfig from '../../config';

const config = getConfig();
const stripePromise = loadStripe(config.stripe.publishableKey);

const OfferPaymentWrapper = ({ children, loading }) => {
  const dispatch = useDispatch();

  const isEligible = useSelector(getOfferEligibility);
  const offerDetails = useSelector(getOfferDetails);
  const offerError = useSelector(getOfferError);
  const cardError = useSelector(getCardError);

  useEffect(() => {
    dispatch(fetchOffer());
    dispatch(fetchCards());
  }, [dispatch]);

  if (isEligible === false) {
    return <OfferError error={offerError} />;
  }

  const error = cardError || offerError;
  if (isEligible && !loading) {
    return (
      <div>
        {error ? <Alert type="error">{error}</Alert> : null}
        {children}
        <div>
          <OfferDetails details={offerDetails} />
        </div>
        <Elements stripe={stripePromise}>
          <OfferForm offerDetails={offerDetails} error={offerError || cardError} />
        </Elements>
      </div>
    );
  }
  return <OfferLoading />;
};

OfferPaymentWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  loading: PropTypes.bool,
};
OfferPaymentWrapper.defaultProps = {
  loading: false,
};

export default OfferPaymentWrapper;
