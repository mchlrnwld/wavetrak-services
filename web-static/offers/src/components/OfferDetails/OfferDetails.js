import PropTypes from 'prop-types';
import { ANNUAL, MONTH } from '@surfline/web-common';
import './OfferDetails.scss';

const convertToDollars = (cents) => parseFloat(cents / 100).toFixed(2);
const getAdverb = (plan) => {
  if (plan.includes(ANNUAL)) return 'yearly';
  if (plan.includes(MONTH)) return 'monthly';
  return 'subscription';
};
const getNoun = (plan) => {
  if (plan.includes(ANNUAL)) return 'year';
  if (plan.includes(MONTH)) return 'month';
  return 'billing cycle';
};

const OfferDetails = ({ details }) => {
  const {
    currentAnnualCost,
    offeredSavings,
    totalCredits,
    totalCost,
    renewalRate,
    currentPlan,
    offeredPlan,
  } = details;
  return (
    <div className="offer-details">
      <div className="offer-details__notification">
        <div>Limited Time Offer</div>
      </div>
      <div className="offer-details__lines">
        <div className="offer-details__line-item">
          <div>One year on your current plan:</div>
          <div>{`$${convertToDollars(currentAnnualCost)}`}</div>
        </div>
        <div className="offer-details__line-item">
          <div>Savings with this offer:</div>
          <div>{`$${convertToDollars(offeredSavings)}`}</div>
        </div>
        <div className="offer-details__line-item">
          <div>*Total credit:</div>
          <div>{`$${convertToDollars(totalCredits)}`}</div>
        </div>
        <div className="offer-details__line-item">
          <div>Total:</div>
          <div>{`$${convertToDollars(totalCost)}`}</div>
        </div>
      </div>
      <div className="offer-details__appendix">
        <div>{`Your ${getAdverb(
          offeredPlan,
        )} plan will renew at a regular price of $${convertToDollars(
          renewalRate,
        )} after the first ${getNoun(offeredPlan)}.`}</div>
        <div>{`*You will be credited $${convertToDollars(
          totalCredits,
        )} from the remainder of your ${getAdverb(currentPlan)} plan.`}</div>
      </div>
      <div className="offer-details__billing-info">
        {`Your yearly plan starts today, you will be billed $${convertToDollars(totalCost)}.`}
      </div>
    </div>
  );
};

OfferDetails.propTypes = {
  details: PropTypes.shape({
    currentAnnualCost: PropTypes.number,
    offeredSavings: PropTypes.number,
    totalCredits: PropTypes.number,
    totalCost: PropTypes.number,
    renewalRate: PropTypes.number,
    currentPlan: PropTypes.string,
    offeredPlan: PropTypes.string,
  }),
};
OfferDetails.defaultProps = {
  details: {
    currentAnnualCost: null,
    offeredSavings: null,
    totalCredits: null,
    totalCost: null,
    renewalRate: null,
    currentPlan: null,
    offeredPlan: null,
  },
};

export default OfferDetails;
