import getConfig from '../../config';
import './RailLogo.scss';

const config = getConfig();

/**
 * @description - Component for rendering the brand specific logo on the top left (top center for mobile)
 * of the funnel. This component is flex positioned at the top left on desktop and absolutely positioned
 * in the top left for mobile. The logo also links to the homepage of the specific brand.
 */
const RailLogo = () => {
  const { redirectUrl } = config;

  // eslint-disable-next-line jsx-a11y/anchor-has-content
  return <a href={redirectUrl} className="rail-logo" aria-label="logo" />;
};

export default RailLogo;
