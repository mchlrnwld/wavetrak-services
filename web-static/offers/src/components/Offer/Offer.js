import { useState } from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import { trackEvent } from '@surfline/web-common';
import { PremiumSuccess, FunnelHelmet, OfferHeader, Registration, OfferPaymentWrapper } from '..';
import { getFunnelConfig } from '../../store/selectors/promotion';
import { getAccessToken } from '../../store/selectors/auth';
import { getOfferSuccess } from '../../store/selectors/subscription';

const Offer = () => {
  const accessToken = useSelector(getAccessToken);
  const offerSuccess = useSelector(getOfferSuccess);
  const { _id: promotionId } = useSelector(getFunnelConfig, shallowEqual);
  const [checkoutRendered, setCheckoutRendered] = useState(false);

  const viewedCheckout = () => {
    if (!checkoutRendered) {
      setCheckoutRendered(true);
      trackEvent('Viewed Checkout Step', { step: 2, promotionId });
    }
  };

  if (accessToken) {
    if (offerSuccess) return <PremiumSuccess />;
    return (
      <OfferPaymentWrapper viewedCheckout={viewedCheckout}>
        <OfferHeader isLoggedIn />
      </OfferPaymentWrapper>
    );
  }

  return (
    <div>
      <FunnelHelmet />
      <OfferHeader />
      <Registration />
    </div>
  );
};

export default Offer;
