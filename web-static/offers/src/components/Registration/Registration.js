import { useEffect, useState } from 'react';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { FacebookLogin, LoginForm, OrBreak } from '@surfline/quiver-react';
import { trackNavigatedToPage, trackEvent } from '@surfline/web-common';
import { login } from '../../store/actions/login';
import { getFunnelConfig } from '../../store/selectors/promotion';
import { isFBFormSubmitting, isFormSubmitting } from '../../store/selectors/interactions';
import { getFbLoginError, getLoginError, getConfirmEmail } from '../../store/selectors/auth';
import { loginWithFacebook } from '../../store/actions/facebook';

const Registration = () => {
  const dispatch = useDispatch();
  const { _id: promotionId } = useSelector(getFunnelConfig, shallowEqual);
  const [registrationRendered, setRegistrationRendered] = useState(false);
  const fbSubmitting = useSelector(isFBFormSubmitting, shallowEqual);
  const fbError = useSelector(getFbLoginError, shallowEqual);
  const submitting = useSelector(isFormSubmitting, shallowEqual);
  const loginError = useSelector(getLoginError, shallowEqual);
  const confirmEmail = useSelector(getConfirmEmail);

  const onFBClick = () => {
    dispatch(loginWithFacebook('facebook'));
  };

  const onLoginSubmit = (data) => {
    dispatch(login(data));
  };

  useEffect(() => {
    trackNavigatedToPage('Funnel', { promotionId }, 'Promotion', { Wootric: false });
  }, [promotionId]);

  useEffect(() => {
    if (confirmEmail) window.location.assign('/confirm-email');
  }, [confirmEmail]);

  const viewedRegistration = () => {
    if (!registrationRendered) {
      setRegistrationRendered(true);
      trackEvent('Viewed Checkout Step', { step: 1, promotionId });
    }
  };

  viewedRegistration();

  return (
    <div>
      <FacebookLogin loading={fbSubmitting} error={fbError} onClick={onFBClick} />
      <OrBreak />
      <LoginForm onSubmit={onLoginSubmit} loading={submitting} error={loginError} />
    </div>
  );
};

export default Registration;
