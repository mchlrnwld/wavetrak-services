import { useEffect } from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import getConfig from '../../config';
import en from '../../international/translations/en';
import { getFunnelConfig } from '../../store/selectors/promotion';
import './PremiumSuccess.scss';

const config = getConfig();

const PremiumSuccess = () => {
  const funnelConfig = useSelector(getFunnelConfig, shallowEqual);

  useEffect(() => {
    if (funnelConfig) {
      const { successUrl } = funnelConfig;
      if (successUrl) {
        setTimeout(() => {
          window.location.assign(successUrl);
        }, 2000);
      }
    }
  }, [funnelConfig]);

  let { message } = en.payment_success;
  const { subTitle } = en.payment_success;
  let header = 'Success';

  if (funnelConfig) {
    const { successHeader, successMessage } = funnelConfig;
    message = successMessage || message;
    header = successHeader || header;
  }
  return (
    <div className="premium-success__container">
      <h4>{header}</h4>
      <div className="premium-success__subtitle">{subTitle}</div>
      <div className="premium-success__message">{message}</div>
      <a className="quiver-button" type="button" href={config.redirectUrl}>
        {en.payment_success.linkText}
      </a>
    </div>
  );
};
export default PremiumSuccess;
