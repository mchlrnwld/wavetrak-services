import PropTypes from 'prop-types';
import {
  AmexIcon,
  DiscoverIcon,
  DinersClubIcon,
  JCBIcon,
  MastercardIcon,
  UnionPayIcon,
  VisaIcon,
} from '@surfline/quiver-react';

const CreditCardIcon = ({ ccBrand }) => {
  const ccFormatted = ccBrand.replace(/\s+/g, '-').toLowerCase();
  if (ccFormatted === 'visa') return <VisaIcon />;
  if (ccFormatted === 'mastercard') return <MastercardIcon />;
  if (ccFormatted === 'american-express') return <AmexIcon />;
  if (ccFormatted === 'discover') return <DiscoverIcon />;
  if (ccFormatted === 'diners-club') return <DinersClubIcon />;
  if (ccFormatted === 'jcb') return <JCBIcon />;
  if (ccFormatted === 'unionpay') return <UnionPayIcon />;
  return null;
};

CreditCardIcon.propTypes = {
  ccBrand: PropTypes.string.isRequired,
};

export default CreditCardIcon;
