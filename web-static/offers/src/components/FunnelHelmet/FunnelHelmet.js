import { Helmet } from 'react-helmet';
import en from '../../international/translations/en';

const links = [
  { rel: 'manifest', href: '/manifest.json' },
  { rel: 'publisher', href: 'https://plus.google.com/+Surfline' },
  {
    rel: 'apple-touch-icon',
    href: 'https://wa.cdn-surfline.com/quiver/0.4.0/apple/surfline-apple-touch-icon.png',
  },
];

const FunnelHelmet = () => (
  <Helmet
    title={en.sign_in_funnel.meta_title}
    meta={[{ name: 'description', content: en.sign_in_funnel.meta_description }]}
    link={links}
  />
);

export default FunnelHelmet;
