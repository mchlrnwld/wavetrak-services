import { Spinner } from '@surfline/quiver-react';
import './OfferLoading.scss';

const OfferLoading = () => (
  <div className="offer-loading">
    <div className="offer-loading__message">
      The details of your offer are being calculated. This could take a few seconds. Please standby.
    </div>
    <Spinner />
  </div>
);

export default OfferLoading;
