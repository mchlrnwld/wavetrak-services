/* eslint-disable no-async-promise-executor */
import fetch from 'isomorphic-fetch';
import getConfig from '../config';

const config = getConfig();

const parseRequest = (resp) =>
  new Promise(async (resolve, reject) => {
    const contentType = resp.headers.get('Content-Type');
    const body = contentType.indexOf('json') > -1 ? await resp.json() : await resp.text();

    if (resp.status > 200) {
      const { status } = resp;
      const errResponse = {
        ...body,
        status,
      };

      return reject(errResponse);
    }

    return resolve(body);
  });

/**
 * Wraps Surfline's public services proxy.
 *
 * Default request options
 *  - accept: 'application/json'
 *  - content-type: 'application/json'
 *  - credentials: 'same-origin'
 *
 * @param path the path to append to the services endpoint. should not start with a `/`
 * @param authToken AccessToken used to authenticate the services endpoint
 * @param opts options to merge into the isomorphic-fetch options object.
 */
export default async (path, authToken, opts, contentArg) => {
  // TODO authenticate the request using a header rather than url param.
  let formattedPath = path.startsWith('/') ? path.substr(1, path.length) : path;
  if (authToken) {
    formattedPath =
      path.indexOf('?') > -1
        ? `${formattedPath}&accesstoken=${authToken}`
        : `${formattedPath}?accesstoken=${authToken}`;
  }

  let contentType = 'application/json';
  if (contentArg) contentType = contentArg;
  return fetch(`${config.servicesEndpoint}/${formattedPath}`, {
    headers: {
      accept: 'application/json',
      'content-type': contentType,
      credentials: 'same-origin',
    },
    ...opts,
  }).then(parseRequest);
};
