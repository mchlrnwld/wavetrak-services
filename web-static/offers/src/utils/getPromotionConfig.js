import { pick } from 'lodash';
import { addDays, format } from 'date-fns';

const formatToday = () => format(new Date(), 'MM/dd/yy');

const formatTrialEndDate = (days = 15) => {
  const trialEnd = addDays(new Date(), days);
  return format(trialEnd, 'MM/dd/yy');
};

const getExtendedTrialProperties = (promotion, trialEligible) => {
  const { paymentHeader, trialLength, promoCodesEnabled } = promotion;
  const ineligiblePaymentHeader =
    'It looks like you have already taken a free trial or promotional offer.';
  const ineligiblePaymentMessage = `Billing will begin today, ${formatToday()}.`;
  const eligiblePaymentMessage = `Billing begins after free trial ends on ${formatTrialEndDate(
    trialLength,
  )}.`;

  const computedPaymentHeader = trialEligible ? paymentHeader : ineligiblePaymentHeader;
  const computedPaymentMessage =
    !trialEligible || trialLength === 0 ? ineligiblePaymentMessage : eligiblePaymentMessage;

  return {
    paymentHeader: computedPaymentHeader,
    paymentMessage: computedPaymentMessage,
    trialLength,
    promoCodesEnabled,
  };
};

const getBaseProperties = (promotion) =>
  pick(promotion, [
    '_id',
    'name',
    'urlKey',
    'kind',
    'brand',
    'isActive',
    'isPremium',
    'valuePropStyle',
    'signUpHeader',
    'paymentHeader',
    'paymentMessage',
    'successHeader',
    'successMessage',
    'successUrl',
    'redeemHeader',
    'buttonText',
  ]);

const getCreativeProperties = (promotion) =>
  pick(promotion, [
    'detailCopy',
    'logo',
    'logoAltText',
    'bgDesktopImgPath',
    'bgMobileImgPath',
    'layeredCardImgPath',
  ]);

const getPromotionConfig = async (promotion, trialEligible = true) => {
  try {
    const { kind, plans, currency } = promotion;
    const baseProperties = getBaseProperties(promotion);
    const creativeProperties = getCreativeProperties(promotion);
    const extendedTrialProperties =
      kind === 'Extended Free Trial' ? getExtendedTrialProperties(promotion, trialEligible) : {};
    const planProperties = plans ? { plans, currency } : {};
    return {
      ...baseProperties,
      ...creativeProperties,
      ...extendedTrialProperties,
      ...planProperties,
    };
  } catch (err) {
    return null;
  }
};

export default getPromotionConfig;
