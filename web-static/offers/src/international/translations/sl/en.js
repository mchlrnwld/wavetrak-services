/* Surfline text is default as defined in base.js */
import { CamsIcon, AnalysisIcon, ChartsIcon, CamRewindIcon } from '@surfline/quiver-react';
import base from '../base';

export default {
  premiumFeatures: {
    features: [
      {
        Icon: CamsIcon,
        header: 'Ad-Free and Premium Cams',
        description: 'Watch live conditions ad-free -- and access exclusive, high-resolution cams.',
      },
      {
        Icon: AnalysisIcon,
        header: 'Trusted Forecast Team',
        description: 'Plan your next trip or session with Surfline’s expert forecast guidance.',
      },
      {
        Icon: ChartsIcon,
        header: 'Exclusive Forecast Tools',
        description: 'Access curated condition reports and proprietary surf, wind and buoy data.',
      },
      {
        Icon: CamRewindIcon,
        header: 'Cam Rewind',
        description:
          'Relive an epic session by downloading your rides -- then share ‘em with friends.',
      },
    ],
  },

  rail_cta: {
    ...base.rail_cta.sl,
  },

  sign_in_funnel: {
    ...base.sign_in_funnel,
  },

  payment_success: {
    subTitle: 'Welcome to Surfline Premium!',
    message:
      "Thank you for joining the Surfline family.  You're now on your way to scoring better waves, more often.  See you in the water.",
    linkText: 'Return to Surfline',
  },

  or_break: {
    ...base.or_break,
  },
};
