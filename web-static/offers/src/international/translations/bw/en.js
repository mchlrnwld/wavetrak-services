import { SwellLinesIcon, BuoyIcon, PremiumAnalysisWindIcon } from '@surfline/quiver-react';
import base from '../base';

export default {
  premiumFeatures: {
    features: [
      {
        Icon: SwellLinesIcon,
        header: 'Marine Forecasts For Any Point',
        description: 'View accurate 16-day wind, wave and tide forecasts.',
      },
      {
        Icon: PremiumAnalysisWindIcon,
        header: 'High-Resolution Wind Forecasts',
        description: 'Access a full collection of weather charts and forecasts.',
      },
      {
        Icon: BuoyIcon,
        header: 'Pick Your Weather Window',
        description: 'Long-range forecasts help you stay safe on the water.',
      },
    ],
  },

  rail_cta: {
    ...base.rail_cta.bw,
  },

  sign_in_funnel: {
    ...base.sign_in_funnel,
    meta_title: 'Sign In to Go Premium - Buoyweather',
    meta_description:
      'Get the latest 16-day marine forecasts – sign in with your Buoyweather account to go Premium.',
  },

  payment_success: {
    subTitle: 'Welcome to Buoyweather Premium!',
    message:
      "Thank you for joining the Buoyweather family.  You're now on your way to safer seas with the most trusted marine weather information.",
    linkText: 'Return to Buoyweather',
  },

  or_break: {
    ...base.or_break,
  },
};
