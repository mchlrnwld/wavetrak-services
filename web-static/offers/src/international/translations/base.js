export default {
  rail_cta: {
    sl: {
      header: 'Premium Benefits',
      mobileHeader: 'Go Premium',
      mobileSubtitle: 'Learn More About Surfline Premium',
    },
    bw: {
      mobileHeader: 'Go Premium',
      header: 'Buoyweather',
    },
    fs: {
      mobileHeader: 'Go Premium',
      header: 'FishTrack',
    },
  },

  sign_in_funnel: {
    meta_title: 'Sign In to Go Premium - Surfline',
    meta_description:
      "Don't miss another good surf - sign in with your Surfline account to go Premium.",
  },

  or_break: {
    or: 'or',
  },
};
