import { SatelliteIcon, LayersIcon, WaypointIcon } from '@surfline/quiver-react';
import base from '../base';

export default {
  premiumFeatures: {
    features: [
      {
        Icon: SatelliteIcon,
        header: 'Latest Satellite Imagery',
        description: 'Locate temperature and color breaks offshore.',
      },
      {
        Icon: WaypointIcon,
        header: 'Waypoints And Routes',
        description: 'Measure distances and plot routes for trips offshore.',
      },
      {
        Icon: LayersIcon,
        header: 'Currents, Bathymetry And Altimetry',
        description: 'Use premium overlays to pinpoint areas of activity.',
      },
    ],
  },
  rail_cta: {
    ...base.rail_cta.fs,
  },

  sign_in_funnel: {
    ...base.sign_in_funnel,
    meta_title: 'Sign In to Go Premium - FishTrack',
    meta_description:
      'Get the latest satellite imagery - sign in with your FishTrack account to go Premium.',
  },

  payment_success: {
    subTitle: 'Welcome to Fishtrack Premium!',
    message:
      'Thank you for joining the Fishtrack family.  You can now access the latest satellite fishing chards and overlays.  See you in the water.',
    linkText: 'Return to Fishtrack',
  },

  or_break: {
    ...base.or_break,
  },
};
