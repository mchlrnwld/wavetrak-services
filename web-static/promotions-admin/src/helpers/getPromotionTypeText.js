
const getPromotionTypeText = (type) => {
  if (type === 'extendTrial') return 'Extended Free Trial';
  if (type === 'giftRedeem') return 'Gift Redeem';
  return null;
};

export default getPromotionTypeText;
