const displayField = (type) => {
  if (type === 'giftRedeem') return { display: 'none' };
  return null;
};

export default displayField;
