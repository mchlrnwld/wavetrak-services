import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { AppContainer } from '@surfline/admin-common';
import Promotions from '../containers/Promotions';

const routes = (
  <AppContainer>
    <Router>
      <Route path="/promotions" component={Promotions} />
    </Router>
  </AppContainer>
);

export default routes;
