import { push } from 'connected-react-router';
import * as api from '../common/api';
import { CLEAR_PROMOTION_CODES } from './promotions';

export const CREATE_PROMOTION = 'CREATE_PROMOTION';
export const CREATE_PROMOTION_SUCCESS = 'CREATE_PROMOTION_SUCCESS';
export const CREATE_PROMOTION_FAILURE = 'CREATE_PROMOTION_FAILURE';

export const createPromotion = form => async (dispatch) => {
  dispatch({ type: CREATE_PROMOTION });
  try {
    const promotion = await api.createPromotion(form);
    dispatch({
      type: CREATE_PROMOTION_SUCCESS,
      promotionCreated: promotion
    });
    dispatch({ type: CLEAR_PROMOTION_CODES });
    dispatch(push('/promotions/'));
  } catch ({ errors }) {
    dispatch({ type: CREATE_PROMOTION_FAILURE, errors });
  }
};
