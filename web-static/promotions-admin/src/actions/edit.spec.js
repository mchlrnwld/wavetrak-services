/* import chai, { expect } from 'chai';
import sinon from 'sinon';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as api from '../common/api';
import {
  fetchPromotion,
  editPromotion,
  FETCH_PROMOTION_DETAILS,
  FETCH_PROMOTION_DETAILS_SUCCESS,
  FETCH_PROMOTION_DETAILS_FAILURE,
  EDIT_PROMOTION,
  EDIT_PROMOTION_SUCCESS,
  EDIT_PROMOTION_FAILURE
} from './edit';

chai.use(dirtyChai);
chai.use(sinonChai);
sinonStubPromise(sinon);

describe('Actions / Edit', () => {
  let mockStore;
  let fetchPromotionStub;
  let editPromotionStub;

  before(() => {
    mockStore = configureMockStore([thunk]);
  });

  beforeEach(() => {
    fetchPromotionStub = sinon.stub(api, 'fetchPromotion');
    editPromotionStub = sinon.stub(api, 'editPromotion');
  });

  afterEach(() => {
    fetchPromotionStub.restore();
    editPromotionStub.restore();
  });

  describe('fetchPromotion', () => {
    it('gets promotion details for store', async () => {
      try {
        const store = mockStore({});
        expect(store.getActions()).to.be.empty();
        fetchPromotionStub.resolves({
          _id: '57c9e77174d8f8d01e3ccd83',
          name: 'Test Promotion',
          urlKey: 'testpromotion',
          trialLength: 100,
          isActive: true,
          type: 'openHouse',
          brand: 'sl',
          bgDesktopImgPath: 'bgDesktopImgPath',
          bgMobileImgPath: 'bgMobileImgPath',
          partnerDetails_logo: 'partnerDetails_logo',
          partnerDetails_logoAltText: 'partnerDetails_logoAltText',
          partnerDetails_detailCopy: 'partnerDetails_detailCopy',
          signup_headingText: 'signup_headingText',
          payment_headingText: 'payment_headingText',
          success_headingText: 'success_headingText'
        });
        const expectedActions = [
          { type: FETCH_PROMOTION_DETAILS },
          {
            type: FETCH_PROMOTION_DETAILS_SUCCESS,
            promotion: {
              _id: '57c9e77174d8f8d01e3ccd83',
              name: 'Test Promotion',
              urlKey: 'testpromotion',
              trialLength: 100,
              isActive: true,
              type: 'openHouse',
              brand: 'sl',
              bgDesktopImgPath: 'bgDesktopImgPath',
              bgMobileImgPath: 'bgMobileImgPath',
              partnerDetails_logo: 'partnerDetails_logo',
              partnerDetails_logoAltText: 'partnerDetails_logoAltText',
              partnerDetails_detailCopy: 'partnerDetails_detailCopy',
              signup_headingText: 'signup_headingText',
              payment_headingText: 'payment_headingText',
              success_headingText: 'success_headingText'
            }
          }
        ];

        await store.dispatch(fetchPromotion('57c9e77174d8f8d01e3ccd83'));
        expect(store.getActions()).to.deep.equal(expectedActions);
        expect(fetchPromotionStub).to.have.been.calledOnce();
        expect(fetchPromotionStub).to.have.been.calledWithExactly('57c9e77174d8f8d01e3ccd83');
        return 'success';
      } catch (err) {
        return err;
      }
    });


    it('errors on failure', async () => {
      try {
        const store = mockStore({});
        expect(store.getActions()).to.be.empty();
        fetchPromotionStub.rejects({
          fetchPromotionStub: 'Promotion not found.'
        });
        const expectedActions = [
          { type: FETCH_PROMOTION_DETAILS },
          {
            type: FETCH_PROMOTION_DETAILS_FAILURE,
            fetchPromotionError: 'Promotion not found.'
          }
        ];
        await store.dispatch(fetchPromotion('57c9e77174d8f8d01e3ccd83'));
        expect(store.getActions()).to.deep.equal(expectedActions);
        expect(fetchPromotionStub).to.have.been.calledOnce();
        expect(fetchPromotionStub).to.have.been.calledWithExactly('57c9e77174d8f8d01e3ccd83');
        return 'success';
      } catch (err) {
        return err;
      }
    });
  });

  describe('editPromotion', () => {
    it('edit promotion fields', async () => {
      try {
        const formFields = {
          name: 'Updated Name',
          urlKey: 'updated URL key'
        };
        const store = mockStore({
          edit: {
            promotion: {
              _id: '57c9e77174d8f8d01e3ccd83'
            }
          }
        });
        expect(store.getActions()).to.be.empty();
        editPromotionStub.resolves(formFields);
        const expectedActions = [
          { type: EDIT_PROMOTION },
          {
            type: EDIT_PROMOTION_SUCCESS,
            promotionUpdated: formFields
          }
        ];

        await store.dispatch(editPromotion(formFields, '57c9e77174d8f8d01e3ccd83'));
        expect(store.getActions()).to.deep.equal(expectedActions);
        expect(editPromotionStub).to.have.been.calledOnce();
        expect(editPromotionStub).to.have.been.calledWithExactly(
          formFields,
          '57c9e77174d8f8d01e3ccd83'
        );
        return 'success';
      } catch (err) {
        return err;
      }
    });

    it('errors on failure', async () => {
      try {
        const formFields = {
          name: '',
          urlKey: 'updated Key'
        };
        const store = mockStore({
          edit: {
            promotion: {
              _id: '57c9e77174d8f8d01e3ccd83'
            }
          }
        });
        expect(store.getActions()).to.be.empty();
        editPromotionStub.rejects({
          errors: {
            name: {
              message: 'Name cannot be blank.'
            }
          }
        });
        const expectedActions = [
          { type: EDIT_PROMOTION },
          {
            type: EDIT_PROMOTION_FAILURE,
            errors: {
              name: {
                message: 'Name cannot be blank.'
              }
            }
          }
        ];

        await store.dispatch(editPromotion(formFields, '57c9e77174d8f8d01e3ccd83'));
        expect(store.getActions()).to.deep.equal(expectedActions);
        expect(editPromotionStub).to.have.been.calledOnce();
        expect(editPromotionStub).to.have.been.calledWithExactly(
          formFields,
          '57c9e77174d8f8d01e3ccd83'
        );
        return 'success';
      } catch (err) {
        return err;
      }
    });
  });
});
 */
