export const LOCATION_CHANGE = 'LOCATION_CHANGE';

export const routeChanged = location => ({
  type: LOCATION_CHANGE,
  location
});
