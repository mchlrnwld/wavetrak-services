/* import chai, { expect } from 'chai';
import sinon from 'sinon';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as api from '../common/api';
import {
  createPromotion,
  CREATE_PROMOTION,
  CREATE_PROMOTION_SUCCESS,
  CREATE_PROMOTION_FAILURE
} from './create';

chai.use(dirtyChai);
chai.use(sinonChai);
sinonStubPromise(sinon);

describe('Actions / Create', () => {
  let mockStore;
  let createPromotionStub;

  before(() => {
    mockStore = configureMockStore([thunk]);
  });

  beforeEach(() => {
    createPromotionStub = sinon.stub(api, 'createPromotion');
  });

  afterEach(() => {
    createPromotionStub.restore();
  });

  describe('createPromotion', () => {
    it('gets promotion details for store', async () => {
      try {
        const formFields = {
          name: 'Test Promotion',
          urlKey: 'testpromotion',
          trialLength: 100
        };
        const resultFields = {
          name: 'cam01',
          urlKey: 'Test title',
          _id: '57c9e77174d8f8d01e3ccd83',
          trialLength: '100'
        };
        const store = mockStore({});
        expect(store.getActions()).to.be.empty();
        createPromotionStub.resolves(resultFields);
        const expectedActions = [
          { type: CREATE_PROMOTION },
          {
            type: CREATE_PROMOTION_SUCCESS,
            promotionCreated: resultFields
          }
        ];

        await store.dispatch(createPromotion(formFields));
        expect(store.getActions()).to.deep.equal(expectedActions);
        expect(createPromotionStub).to.have.been.calledOnce();
        expect(createPromotionStub).to.have.been.calledWithExactly(formFields);
        return 'success';
      } catch (err) {
        return err;
      }
    });

    it('errors on failure', async () => {
      try {
        const formFields = {
          name: 'Test Promotion',
          trialLength: 100
        };
        const store = mockStore({});
        expect(store.getActions()).to.be.empty();
        createPromotionStub.rejects({
          errors: {
            urlKey: {
              message: 'urlKey cannot be blank.'
            }
          }
        });
        const expectedActions = [
          { type: CREATE_PROMOTION },
          {
            type: CREATE_PROMOTION_FAILURE,
            errors: {
              urlKey: {
                message: 'urlKey cannot be blank.'
              }
            }
          }
        ];
        await store.dispatch(createPromotion(formFields));
        expect(store.getActions()).to.deep.equal(expectedActions);
        expect(createPromotionStub).to.have.been.calledOnce();
        expect(createPromotionStub).to.have.been.calledWithExactly(formFields);
        return 'success';
      } catch (err) {
        return err;
      }
    });
  });
});
 */
