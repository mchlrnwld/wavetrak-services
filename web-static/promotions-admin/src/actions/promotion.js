import uniq from 'lodash.uniq';
import {
  fetchPromotion as getPromotionById,
  removePromotion as removePromotionById,
  fetchPromotionCodes,
  uploadPartnerCodes,
  createInternalPromoCodes,
  editPromotion,
} from '../common/api';

export const FETCH_PROMOTION = 'FETCH_PROMOTION';
export const FETCH_PROMOTION_SUCCESS = 'FETCH_PROMOTION_SUCCESS';
export const FETCH_PROMOTION_FAILURE = 'FETCH_PROMOTION_FAILURE';
export const REMOVE_PROMOTION = 'REMOVE_PROMOTION';
export const CANCEL_REMOVE_PROMOTION = 'CANCEL_REMOVE_PROMOTION';
export const CONFIRM_REMOVE_PROMOTION = 'CONFIRM_REMOVE_PROMOTION';
export const REMOVE_PROMOTION_SUCCESS = 'REMOVE_PROMOTION_SUCCESS';
export const REMOVE_PROMOTION_FAILURE = 'REMOVE_PROMOTION_FAILURE';
export const FETCH_PROMO_CODES = 'FETCH_PROMO_CODES';
export const FETCH_PROMO_CODES_SUCCESS = 'FETCH_PROMO_CODES_SUCCESS';
export const FETCH_PROMO_CODES_FAILURE = 'FETCH_PROMO_CODES_FAILURE';
export const CREATE_CODES = 'CREATE_CODES';
export const CREATE_CODES_SUCCESS = 'CREATE_CODES_SUCCESS';
export const CREATE_CODES_FAILURE = 'CREATE_CODES_FAILURE';

export const fetchPromotion = promotionId => async (dispatch) => {
  dispatch({ type: FETCH_PROMOTION });
  try {
    const { promotion } = await getPromotionById(promotionId);
    if (promotion.kind === 'Extented Free Trial' || 'Direct To Pay') {
      const { promotionCodes: codes } = await fetchPromotionCodes(promotionId);
      return dispatch({ type: FETCH_PROMOTION_SUCCESS, promotion: { ...promotion, codes } });
    }
    return dispatch({ type: FETCH_PROMOTION_SUCCESS, promotion });
  } catch (error) {
    return dispatch({ type: FETCH_PROMOTION_FAILURE, error: error.message });
  }
};

export const removePromotion = promotion => async (dispatch) => {
  dispatch({ type: REMOVE_PROMOTION });
  try {
    await removePromotionById(promotion);
    dispatch({
      type: REMOVE_PROMOTION_SUCCESS,
    });
  } catch (error) {
    dispatch({
      type: REMOVE_PROMOTION_FAILURE,
      promotion,
      error: 'Unable to remove promotion'
    });
  }
};

export const fetchPromoCodes = promotionId => async (dispatch) => {
  try {
    dispatch({ type: FETCH_PROMO_CODES });
    const { promotionCodes } = await fetchPromotionCodes(promotionId);
    const codes = promotionCodes.map(({ code }) => ({ code }));
    return dispatch({ type: FETCH_PROMO_CODES_SUCCESS, promotionCodes: codes });
  } catch (error) {
    return dispatch({ type: FETCH_PROMO_CODES_FAILURE });
  }
};

export const uploadCodes = (codes, promotionId) => async (dispatch) => {
  try {
    dispatch({ type: CREATE_CODES });

    const hasDuplicateCode = uniq(codes).length !== codes.length;
    if (hasDuplicateCode) {
      return dispatch({ type: CREATE_CODES_FAILURE, error: 'Duplicate Codes found.' });
    }

    const formattedCodes = codes.map(code => code.replace(/(\r\n|\n|\r)(^,)|(,$)/gm, ''));
    const { message } = await uploadPartnerCodes(promotionId, { codes: formattedCodes });

    return dispatch({ type: CREATE_CODES_SUCCESS, message, codes: formattedCodes });
  } catch (error) {
    return dispatch({ type: CREATE_CODES_FAILURE, error: error.message });
  }
};

export const generateCodes = (promotionId, opts) => async (dispatch) => {
  try {
    dispatch({ type: CREATE_CODES });
    const {
      kind,
      promotionCodePrefix: promoCodePrefix,
    } = opts;
    if (!promoCodePrefix) {
      return dispatch({ type: CREATE_CODES_FAILURE, error: 'Promo Code Prefix cannot be blank.' });
    }
    const promotion = await editPromotion({
      promoCodePrefix,
      kind,
      promoCodesEnabled: true
    }, promotionId);
    const { codes, message } = await createInternalPromoCodes(promotionId, opts);
    const formattedCodes = codes.map(code => ({ code }));
    return dispatch({
      type: CREATE_CODES_SUCCESS,
      codes: formattedCodes,
      message,
      promotion
    });
  } catch (error) {
    return dispatch({ type: CREATE_CODES_FAILURE, error: error.message });
  }
};
