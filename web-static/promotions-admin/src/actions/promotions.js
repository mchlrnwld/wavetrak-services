import * as api from '../common/api';

export const FETCH_PROMOTIONS = 'FETCH_PROMOTIONS';
export const FETCH_PROMOTIONS_SUCCESS = 'FETCH_PROMOTIONS_SUCCESS';
export const FETCH_PROMOTIONS_FAILURE = 'FETCH_PROMOTIONS_FAILURE';

export const SAVE_DISCOUNT_CODES = 'SAVE_DISCOUNT_CODES';
export const SAVE_DISCOUNT_CODES_FAILURE = 'SAVE_DISCOUNT_CODES_FAILURE';
export const CLEAR_PROMOTION_CODES = 'CLEAR_PROMOTION_CODES';
export const TOGGLE_PROMOCODES_DISPLAY = 'TOGGLE_PROMOCODES_DISPLAY';
export const SET_PROMOTION_KIND = 'SET_PROMOTION_TYPE';

export const fetchPromotions = () => async (dispatch) => {
  dispatch({ type: FETCH_PROMOTIONS });

  try {
    const promotions = await api.fetchPromotions();
    dispatch({
      type: FETCH_PROMOTIONS_SUCCESS,
      promotions
    });
  } catch (error) {
    dispatch({
      type: FETCH_PROMOTIONS_FAILURE,
      error: error.message
    });
  }
};

export const togglePromoCodesDisplay = value => async (dispatch) => {
  const promoCodesEnabled = (value === 'true');
  return dispatch({ type: TOGGLE_PROMOCODES_DISPLAY, promoCodesEnabled });
};

export const setPromotionKind = kind => async dispatch => (
  dispatch({ type: SET_PROMOTION_KIND, kind })
);
