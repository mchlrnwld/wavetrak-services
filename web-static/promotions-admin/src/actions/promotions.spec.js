/*
import chai, { expect } from 'chai';
import sinon from 'sinon';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import sinonStubPromise from 'sinon-stub-promise';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as api from '../common/api';
import {
  fetchPromotions,
  FETCH_PROMOTIONS,
  FETCH_PROMOTIONS_SUCCESS,
  FETCH_PROMOTIONS_FAILURE,
} from './promotions';

chai.use(dirtyChai);
chai.use(sinonChai);
sinonStubPromise(sinon);

describe('Actions / Promotions', () => {
  let mockStore;
  let fetchPromotionsStub;
  let removePromotionStub;

  before(() => {
    mockStore = configureMockStore([thunk]);
  });

  beforeEach(() => {
    fetchPromotionsStub = sinon.stub(api, 'fetchPromotions');
    removePromotionStub = sinon.stub(api, 'removePromotion');
  });

  afterEach(() => {
    fetchPromotionsStub.restore();
    removePromotionStub.restore();
  });

  describe('fetchPromotions', () => {
    it('gets all promotions', async () => {
      try {
        const store = mockStore({});
        expect(store.getActions()).to.be.empty();
        fetchPromotionsStub.resolves(
          [
            {
              _id: '57c9e77174d8f8d01e3ccd83',
              name: 'Test Promotion',
              urlKey: 'testpromotion',
              trialLength: 100,
              isActive: true,
              type: 'openHouse',
              brand: 'sl',
              bgDesktopImgPath: 'bgDesktopImgPath',
              bgMobileImgPath: 'bgMobileImgPath',
              partnerDetails_logo: 'partnerDetails_logo',
              partnerDetails_logoAltText: 'partnerDetails_logoAltText',
              partnerDetails_detailCopy: 'partnerDetails_detailCopy',
              signup_headingText: 'signup_headingText',
              payment_headingText: 'payment_headingText',
              success_headingText: 'success_headingText'
            },
            {
              _id: '57c9e77174d8f8d01e3ccd83',
              name: 'Test Promotion',
              urlKey: 'testpromotion',
              trialLength: 100,
              isActive: true,
              type: 'openHouse',
              brand: 'sl',
              bgDesktopImgPath: 'bgDesktopImgPath',
              bgMobileImgPath: 'bgMobileImgPath',
              partnerDetails_logo: 'partnerDetails_logo',
              partnerDetails_logoAltText: 'partnerDetails_logoAltText',
              partnerDetails_detailCopy: 'partnerDetails_detailCopy',
              signup_headingText: 'signup_headingText',
              payment_headingText: 'payment_headingText',
              success_headingText: 'success_headingText'
            }
          ]
        );
        const expectedActions = [
          { type: FETCH_PROMOTIONS },
          {
            type: FETCH_PROMOTIONS_SUCCESS,
            promotions: [
              {
                _id: '57c9e77174d8f8d01e3ccd83',
                name: 'Test Promotion',
                urlKey: 'testpromotion',
                trialLength: 100,
                isActive: true,
                type: 'openHouse',
                brand: 'sl',
                bgDesktopImgPath: 'bgDesktopImgPath',
                bgMobileImgPath: 'bgMobileImgPath',
                partnerDetails_logo: 'partnerDetails_logo',
                partnerDetails_logoAltText: 'partnerDetails_logoAltText',
                partnerDetails_detailCopy: 'partnerDetails_detailCopy',
                signup_headingText: 'signup_headingText',
                payment_headingText: 'payment_headingText',
                success_headingText: 'success_headingText'
              },
              {
                _id: '57c9e77174d8f8d01e3ccd83',
                name: 'Test Promotion',
                urlKey: 'testpromotion',
                trialLength: 100,
                isActive: true,
                type: 'openHouse',
                brand: 'sl',
                bgDesktopImgPath: 'bgDesktopImgPath',
                bgMobileImgPath: 'bgMobileImgPath',
                partnerDetails_logo: 'partnerDetails_logo',
                partnerDetails_logoAltText: 'partnerDetails_logoAltText',
                partnerDetails_detailCopy: 'partnerDetails_detailCopy',
                signup_headingText: 'signup_headingText',
                payment_headingText: 'payment_headingText',
                success_headingText: 'success_headingText'
              }
            ]
          }
        ];

        await store.dispatch(fetchPromotions());
        expect(store.getActions()).to.deep.equal(expectedActions);
        expect(fetchPromotionsStub).to.have.been.calledOnce();
        return 'success';
      } catch (err) {
        return err;
      }
    });

    it('errors on failure', async () => {
      try {
        const store = mockStore({});
        expect(store.getActions()).to.be.empty();
        fetchPromotionsStub.rejects({
          fetchPromotionsStub: 'Promotion not found.'
        });
        const expectedActions = [
          { type: FETCH_PROMOTIONS },
          {
            type: FETCH_PROMOTIONS_FAILURE,
            fetchPromotionsError: 'Promotion not found.'
          }
        ];
        await store.dispatch(fetchPromotions());
        expect(store.getActions()).to.deep.equal(expectedActions);
        expect(fetchPromotionsStub).to.have.been.calledOnce();
        return 'success';
      } catch (err) {
        return err;
      }
    });
  });

  describe('removePromotion', () => {
    it('removes a promotion', async () => {
      try {
        const store = mockStore({
          promotions: {
            promotion: {
              _id: '57c9e77174d8f8d01e3ccd83'
            }
          }
        });
        expect(store.getActions()).to.be.empty();
        removePromotionStub.resolves();
        const expectedActions = [
          { type: CONFIRM_REMOVE_PROMOTION, promotion: '57c9e77174d8f8d01e3ccd83' },
          {
            type: REMOVE_PROMOTION_SUCCESS,
            promotion: '57c9e77174d8f8d01e3ccd83'
          }
        ];

        await store.dispatch(removePromotion('57c9e77174d8f8d01e3ccd83'));
        expect(store.getActions()).to.deep.equal(expectedActions);
        expect(removePromotionStub).to.have.been.calledOnce();
        expect(removePromotionStub).to.have.been.calledWithExactly('57c9e77174d8f8d01e3ccd83');
        return 'success';
      } catch (err) {
        return err;
      }
    });
  });
});
*/
