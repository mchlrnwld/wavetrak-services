import * as api from '../common/api';

export const FETCH_PROMOTION_DETAILS = 'FETCH_PROMOTION_DETAILS';
export const FETCH_PROMOTION_DETAILS_SUCCESS = 'FETCH_PROMOTION_DETAILS_SUCCESS';
export const FETCH_PROMOTION_DETAILS_FAILURE = 'FETCH_PROMOTION_DETAILS_FAILURE';

export const EDIT_PROMOTION = 'EDIT_PROMOTION';
export const EDIT_PROMOTION_SUCCESS = 'EDIT_PROMOTION_SUCCESS';
export const EDIT_PROMOTION_FAILURE = 'EDIT_PROMOTION_FAILURE';

export const SELECT_PROMOTION_TO_EDIT = 'SELECT_PROMOTION_TO_EDIT';

export const fetchPromotion = promotionId => async (dispatch) => {
  dispatch({
    type: FETCH_PROMOTION_DETAILS
  });

  try {
    const { promotion } = await api.fetchPromotion(promotionId);
    dispatch({
      type: FETCH_PROMOTION_DETAILS_SUCCESS,
      promotion: {
        _id: promotion._id,
        name: promotion.name,
        urlKey: promotion.urlKey,
        trialLength: promotion.trialLength,
        isActive: promotion.isActive,
        type: promotion.type,
        brand: promotion.brand,
        plans: promotion.plans,
        maxRedemption: promotion.maxRedemption,
        bgDesktopImgPath: promotion.bgDesktopImgPath,
        bgMobileImgPath: promotion.bgMobileImgPath,
        promoCodesEnabled: promotion.promoCodesEnabled,
        promoCodePrefix: promotion.promoCodePrefix,
        logo: promotion.logo,
        logoAltText: promotion.logoAltText,
        detailCopy: promotion.detailCopy,
        signup_headingText: promotion.signup.headingText,
        payment_headingText: promotion.payment.headingText,
        success_headingText: promotion.success.headingText,
        success_successMessage: promotion.success.successMessage,
        codes: promotion.codes
      }
    });
  } catch (error) {
    dispatch({
      type: FETCH_PROMOTION_DETAILS_FAILURE,
      fetchPromotionError: 'Promotion not found.'
    });
  }
};

export const editPromotion = form => async (dispatch) => {
  dispatch({ type: EDIT_PROMOTION });
  try {
    const { id: promotionId } = form;
    const promotion = await api.editPromotion(form, promotionId);
    dispatch({
      type: EDIT_PROMOTION_SUCCESS,
      promotionUpdated: promotion
    });
    window.location.assign(`/promotions/${promotionId}`);
  } catch ({ errors }) {
    dispatch({
      type: EDIT_PROMOTION_FAILURE,
      errors
    });
  }
};
