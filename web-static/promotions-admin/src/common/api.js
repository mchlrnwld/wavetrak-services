import fetch from './fetch';

export const fetchPromotions = () => fetch('promotions/');

export const removePromotion = promotionId => fetch(`promotions/${promotionId}`, {
  method: 'DELETE',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
    credentials: 'same-origin'
  }
});

export const createPromotion = formData => fetch('promotions/', {
  method: 'POST',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
    credentials: 'same-origin'
  },
  body: JSON.stringify(formData)
});

export const fetchPromotion = promotionId => fetch(`promotions/${promotionId}`);

export const editPromotion = (formData, promotionId) => fetch(`promotions/${promotionId}`, {
  method: 'PUT',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
    credentials: 'same-origin'
  },
  body: JSON.stringify(formData)
});

export const fetchPromotionCodes = promotionId => fetch(`promotions/${promotionId}/codes`);

export const uploadPartnerCodes = (promotionId, codes) => fetch(`promotions/${promotionId}/partner-codes`, {
  method: 'POST',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
    credentials: 'same-origin'
  },
  body: JSON.stringify(codes),
});

export const createInternalPromoCodes = (promotionId, opts) => fetch(`promotions/${promotionId}/internal-codes`, {
  method: 'POST',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
    credentials: 'same-origin'
  },
  body: JSON.stringify(opts),
});

export const getPerksHero = () => fetch('promotions/perks-hero');

export const createPerksHero = opts => fetch('promotions/perks-hero', {
  method: 'POST',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
    credentials: 'same-origin'
  },
  body: JSON.stringify(opts),
});

export const deletePerksHero = brand => fetch(`promotions/perks-hero/?brand=${brand}`, {
  method: 'DELETE',
  headers: {
    accept: 'application/json',
    'content-type': 'application/json',
    credentials: 'same-origin'
  }
});
