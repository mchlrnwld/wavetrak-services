import config from '../config';

export const getMaxRedemptions = (maxRedemptions) => {
  if (maxRedemptions > 0) return `/${maxRedemptions}`;
  return '';
};


export const getBrandName = (brand) => {
  if (brand === 'sl') return 'surfline';
  if (brand === 'bw') return 'buoyweather';
  if (brand === 'fs') return 'fishrack';
  return null;
};


export const getUrlPath = (brand, urlPath) => {
  const company = getBrandName(brand);
  const { environment } = config;
  if (environment) return `https://${environment}.${company}.com/${urlPath}`;
  return `https://${company}.com/${urlPath}`;
};
