export default {
  development: {
    adminServicesEndpoint: 'https://tools.sandbox.surflineadmin.com/api',
    environment: 'sandbox',
  },
  sandbox: {
    adminServicesEndpoint: 'https://tools.sandbox.surflineadmin.com/api',
    environment: 'sandbox',
  },
  staging: {
    adminServicesEndpoint: 'https://tools.staging.surflineadmin.com/api',
    environment: 'staging',
  },
  production: {
    adminServicesEndpoint: 'https://tools.surflineadmin.com/api',
    environment: null,
  }
}[process.env.APP_ENV || 'development'];
