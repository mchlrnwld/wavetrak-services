import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Alert, Button } from '@surfline/quiver-react';
import PromotionForm from '../components/PromotionForm';
import { editPromotion } from '../actions/edit';
import { fetchPromotion, removePromotion } from '../actions/promotion';

class EditPromotion extends Component {
  styles = {
    container: {
      padding: 0,
      paddingBottom: 100
    }
  };

  componentDidMount() {
    const { doFetchPromotion, match } = this.props;
    const { id: promotionId } = match.params;
    doFetchPromotion(promotionId);
  }

  deletePromotion = async (promotionId) => {
    const { doRemovePromotion } = this.props;
    await doRemovePromotion(promotionId);
    window.location.assign('/promotions');
  };

  render() {
    const {
      promotion,
      errors,
      doEditPromotion,
      editing,
      fetchPromotionError,
      loading,
      success,
      kind,
      removing,
    } = this.props;
    return (
      <div style={this.styles.container}>
        {loading ? (
          <h3>Loading... Edit</h3>
        ) : null}
        {!loading && fetchPromotionError ? (
          <Alert type="error">{fetchPromotionError}</Alert>
        ) : null}
        {!loading && promotion ? (
          <PromotionForm
            action={doEditPromotion}
            promotion={promotion}
            errors={errors}
            processing={editing}
            success={success}
            kind={kind || promotion.kind}
          />
        ) : null}
        <div className="promotion__button--remove">
          <Button onClick={() => this.deletePromotion(promotion._id)} loading={removing}>
            Delete Promotion
          </Button>
        </div>
      </div>
    );
  }
}

EditPromotion.propTypes = {
  loading: PropTypes.bool,
  errors: PropTypes.object,
  promotion: PropTypes.object.isRequired,
  doFetchPromotion: PropTypes.func.isRequired,
  doEditPromotion: PropTypes.func.isRequired,
  doRemovePromotion: PropTypes.func.isRequired,
  editing: PropTypes.bool,
  removing: PropTypes.bool,
  success: PropTypes.bool,
  fetchPromotionError: PropTypes.string,
  match: PropTypes.object.isRequired,
  kind: PropTypes.string,
};

EditPromotion.defaultProps = {
  errors: {},
  loading: false,
  success: false,
  editing: true,
  removing: false,
  fetchPromotionError: null,
  kind: null,
};

export default connect(
  state => ({
    ...state.edit,
    ...state.promotions,
    ...state.promotion
  }),
  dispatch => ({
    doFetchPromotion: promotionId => dispatch(fetchPromotion(promotionId)),
    doEditPromotion: promotionId => dispatch(editPromotion(promotionId)),
    doRemovePromotion: promotionId => dispatch(removePromotion(promotionId)),
  }),
)(EditPromotion);
