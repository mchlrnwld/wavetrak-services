import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Alert } from '@surfline/quiver-react';
import {
  fetchPromotion,
  fetchPromoCodes,
  uploadCodes,
  generateCodes
} from '../actions/promotion';
import CodeUploader from '../components/CodeUploader';
import CodeGenerator from '../components/CodeGenerator';

class PromoCodes extends Component {
  componentDidMount() {
    const {
      doFetchPromotion,
      doFetchPromoCodes,
      match: { params },
    } = this.props;

    if (params) {
      const { id: promotionId } = params;
      doFetchPromotion(promotionId);
      doFetchPromoCodes(promotionId);
    }
  }

  handleUploadedFile = (files) => {
    const { doUploadCodes, promotion: { _id: id } } = this.props;
    const reader = new FileReader();
    reader.onload = () => {
      const result = reader.result.split('\n');
      doUploadCodes(result, id);
    };
    reader.readAsText(files.fileList[0]);
  };

  handleGenerateCodes = (opts) => {
    const {
      promotion: { _id: promotionId, kind },
      doGenerateCodes,
    } = this.props;
    doGenerateCodes(promotionId, { ...opts, kind });
  }

  renderCodeComponent() {
    const {
      promotion,
      promotionCodes,
      uploading,
      error,
      promoCodeSuccess,
    } = this.props;
    const { kind, promoCodePrefix, maxRedemption } = promotion;
    return (
      <div>
        {error ? <Alert type="error">{error}</Alert> : null}
        {promoCodeSuccess ? <Alert type="success">{promoCodeSuccess}</Alert> : null}
        {kind === 'Extended Free Trial'
          ? (
            <CodeGenerator
              promoCodePrefix={promoCodePrefix}
              maxRedemption={maxRedemption}
              doGenerateCodes={opts => this.handleGenerateCodes(opts)}
              promotionCodes={promotionCodes}
              uploading={uploading}
            />
          )
          : (
            <CodeUploader
              promotionCodes={promotionCodes}
              uploading={uploading}
              maxRedemption={maxRedemption}
              doUploadCodes={files => this.handleUploadedFile(files)}
            />
          )
        }
      </div>
    );
  }

  render() {
    const {
      promotion,
    } = this.props;
    if (!promotion) return <div>loading...</div>;

    const { kind } = promotion;
    return (
      <div className="promo-code__container">
        {kind === 'Extended Free Trial' || kind === 'Direct To Pay' || kind === 'Perk'
          ? (
            this.renderCodeComponent()
          ) : (
            <Alert type="error">
              Codes are only valid for Extended Free Trial, Direct To Pay or Perk promotions
            </Alert>
          )
        }
      </div>
    );
  }
}


PromoCodes.propTypes = {
  promotion: PropTypes.shape({}),
  promotionCodes: PropTypes.arrayOf(PropTypes.shape({})),
  doFetchPromotion: PropTypes.func.isRequired,
  doFetchPromoCodes: PropTypes.func.isRequired,
  doUploadCodes: PropTypes.func.isRequired,
  doGenerateCodes: PropTypes.func.isRequired,
  uploading: PropTypes.bool,
  error: PropTypes.string,
  promoCodeSuccess: PropTypes.string,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }),
};

PromoCodes.defaultProps = {
  promotion: null,
  promotionCodes: null,
  match: null,
  uploading: false,
  error: null,
  promoCodeSuccess: null,
};

export default connect(
  state => ({
    promotion: state.promotion.promotion,
    promotionCodes: state.promotion.promotionCodes,
    uploading: state.promotion.uploading,
    error: state.promotion.error,
    promoCodeSuccess: state.promotion.promoCodeSuccess,
  }),
  dispatch => ({
    doFetchPromotion: promotionId => dispatch(fetchPromotion(promotionId)),
    doFetchPromoCodes: promotionId => dispatch(fetchPromoCodes(promotionId)),
    doUploadCodes: (codes, promotionId) => dispatch(uploadCodes(codes, promotionId)),
    doGenerateCodes: (promotionId, opts) => dispatch(generateCodes(promotionId, opts))
  }),
)(PromoCodes);
