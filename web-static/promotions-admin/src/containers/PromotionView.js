import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getMaxRedemptions, getUrlPath } from '../common/utils';
import {
  DirectToPay,
  ExtendedFreeTrial,
  GiftRedeemFunnel,
  RetentionOffer,
  Perk,
} from '../components/PromotionViewTypes';

class PromotionView extends Component {
  getPromotionViewFromType = (kind) => {
    const componentMap = {
      'Extended Free Trial': ExtendedFreeTrial,
      'Direct To Pay': DirectToPay,
      'Gift Redeem': GiftRedeemFunnel,
      'Retention Offer': RetentionOffer,
      Perk,
    };

    return componentMap[kind];
  };

  render() {
    const { promotion } = this.props;
    const {
      kind,
      brand,
      urlKey,
      totalRedemption,
      maxRedemption,
      isActive
    } = promotion;
    const PromotionViewType = this.getPromotionViewFromType(kind);
    return (
      <div className="promotion__view__container">
        {
        kind === 'Perk' ? null
          : (
            <div className="promotion__property">
              <div>Url: </div>
              <div>
                <a href={getUrlPath(brand, urlKey)}>{getUrlPath(brand, urlKey)}</a>
              </div>
            </div>
          )
        }
        <div className="promotion__property">
          <div>Active: </div>
          <div>{`${isActive}`}</div>
        </div>
        <div className="promotion__property">
          <div>totalRedemptions: </div>
          <div>{`${totalRedemption}${getMaxRedemptions(maxRedemption)}`}</div>
        </div>
        <PromotionViewType promotion={promotion} />
      </div>
    );
  }
}

PromotionView.propTypes = {
  promotion: PropTypes.shape({}),
};

PromotionView.defaultProps = {
  promotion: null,
};

export default connect(
  state => ({
    promotion: state.promotion.promotion,
  })
)(PromotionView);
