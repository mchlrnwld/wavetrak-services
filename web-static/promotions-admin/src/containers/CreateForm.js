import React from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PromotionForm from '../components/PromotionForm';
import { createPromotion } from '../actions/create';

const styles = {
  container: {
    padding: 0,
    paddingBottom: 100
  }
};

const CreatePromotion = ({
  doCreatePromotion,
  creating,
  errors,
  success,
  kind,
}) => {
  if (!kind) return <Redirect to="/promotions/create" />;
  return (
    <div style={styles.container}>
      <div className="promotion__create__header">
        <div className="promotion__create__header__title">
          {kind}
        </div>
      </div>
      <PromotionForm
        action={doCreatePromotion}
        processing={creating}
        errors={errors}
        success={success}
        kind={kind}
      />
    </div>
  );
};

CreatePromotion.propTypes = {
  errors: PropTypes.shape({}),
  doCreatePromotion: PropTypes.func.isRequired,
  creating: PropTypes.bool,
  success: PropTypes.bool,
  kind: PropTypes.string,
};
CreatePromotion.defaultProps = {
  errors: {},
  creating: false,
  success: false,
  kind: null,
};

export default connect(
  state => ({
    ...state.create,
    ...state.promotions
  }),
  dispatch => ({
    doCreatePromotion: formData => dispatch(createPromotion(formData)),
  })
)(CreatePromotion);
