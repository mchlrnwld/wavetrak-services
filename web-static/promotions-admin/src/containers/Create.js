import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';
import { setPromotionKind } from '../actions/promotions';
import CreateForm from './CreateForm';
import PromotionKindSelector from '../components/PromotionKindSelector';

const Create = ({ doSetPromotionKind, match }) => (
  <div>
    <Route path="/promotions/create/:slug" component={CreateForm} />
    <Route
      exact
      path={match.path}
      render={() => (
        <PromotionKindSelector doSetPromotionKind={doSetPromotionKind} />
      )}
    />
  </div>
);

Create.propTypes = {
  doSetPromotionKind: PropTypes.func.isRequired,
  match: PropTypes.shape({
    path: PropTypes.string,
  }).isRequired,
};

export default connect(
  state => ({
    ...state.promotions,
  }),
  dispatch => ({
    doSetPromotionKind: kind => dispatch(setPromotionKind(kind)),
  }),
)(Create);
