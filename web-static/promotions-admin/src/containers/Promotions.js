import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import PromotionsHome from '../components/PromotionsHome';
import Promotion from './Promotion';
import Create from './Create';

const Promotions = ({ match }) => (
  <div style={{ width: '100%' }}>
    <Switch>
      <Route exact path={match.path} component={PromotionsHome} />
      <Route path="/promotions/create" component={Create} />
      <Route path="/promotions/:id" component={Promotion} />
    </Switch>
  </div>
);

Promotions.propTypes = {
  match: PropTypes.shape({
    path: PropTypes.string,
  }).isRequired,
};

export default Promotions;
