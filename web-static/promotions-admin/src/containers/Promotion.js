import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, Switch, Link } from 'react-router-dom';
import { fetchPromotion } from '../actions/promotion';
import { setPromotionKind } from '../actions/promotions';
import Edit from './Edit';
import PromoCodes from './PromoCodes';
import PromotionView from './PromotionView';
import PromotionHeader from '../components/PromotionHeader';

class Promotion extends Component {
  componentDidMount() {
    const {
      doFetchPromotion,
      match: { params },
    } = this.props;

    if (params) {
      const { id: promotionId } = params;
      doFetchPromotion(promotionId);
    }
  }

  renderPromoCodesEnabled = (kind, id) => {
    if (kind === 'Direct To Pay' || kind === 'Extended Free Trial' || kind === 'Perk') {
      return (
        <Link className="quiver-button" to={`/promotions/${id}/promo-codes`}>
          Manage PromoCodes
        </Link>
      );
    }
    return null;
  };

  render() {
    const { promotion, doSetPromotionKind, match } = this.props;
    if (!promotion) return <div>loading...</div>;
    return (
      <div className="promotion__container">
        <PromotionHeader
          promotion={promotion}
          doSetPromotionKind={doSetPromotionKind}
          match={match}
        />
        <Switch>
          <Route exact path={`${match.path}/edit`} component={Edit} />
          <Route
            exact
            path={`${match.path}/promo-codes`}
            component={PromoCodes}
          />
          <Route path={match.path} component={PromotionView} />
        </Switch>
      </div>
    );
  }
}

Promotion.propTypes = {
  promotion: PropTypes.shape({}),
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string,
    }),
  }),
  doFetchPromotion: PropTypes.func.isRequired,
  doSetPromotionKind: PropTypes.func.isRequired,
};

Promotion.defaultProps = {
  promotion: null,
  match: null,
};

export default connect(
  state => ({
    promotion: state.promotion.promotion,
    removing: state.promotion.removing,
    success: state.promotion.success,
  }),
  dispatch => ({
    doFetchPromotion: promotionId => dispatch(fetchPromotion(promotionId)),
    doSetPromotionKind: kind => dispatch(setPromotionKind(kind)),
  }),
)(Promotion);
