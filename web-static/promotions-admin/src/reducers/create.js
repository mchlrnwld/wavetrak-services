import createReducerFromHandlers from '@surfline/admin-common/dist/utils/createReducerFromHandlers';
import { LOCATION_CHANGE } from '../actions/router';
import {
  CREATE_PROMOTION,
  CREATE_PROMOTION_SUCCESS,
  CREATE_PROMOTION_FAILURE
} from '../actions/create';

const initialState = {
  creating: false,
  success: false,
  promotionCreated: null,
  errors: null
};

const handlers = {};

handlers[LOCATION_CHANGE] = () => initialState;

handlers[CREATE_PROMOTION] = (state, { errors }) => ({
  ...state,
  creating: true,
  success: false,
  errors: errors || null
});

handlers[CREATE_PROMOTION_SUCCESS] = (state, { promotionCreated }) => ({
  ...state,
  promotionCreated,
  creating: false,
  success: true,
  errors: null
});

handlers[CREATE_PROMOTION_FAILURE] = (state, { errors }) => ({
  ...state,
  errors,
  creating: false
});

export default createReducerFromHandlers(handlers, initialState);
