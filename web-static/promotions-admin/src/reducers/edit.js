import createReducerFromHandlers from '@surfline/admin-common/dist/utils/createReducerFromHandlers';
import { LOCATION_CHANGE } from '../actions/router';
import {
  FETCH_PROMOTION_DETAILS,
  FETCH_PROMOTION_DETAILS_SUCCESS,
  FETCH_PROMOTION_DETAILS_FAILURE,
  EDIT_PROMOTION,
  EDIT_PROMOTION_SUCCESS,
  EDIT_PROMOTION_FAILURE
} from '../actions/edit';

const initialState = {
  promotionUpdated: null,
  editing: false,
  errors: null,
  fetchPromotionError: null,
  loading: false,
  success: false
};

const handlers = {};

handlers[LOCATION_CHANGE] = () => initialState;

handlers[FETCH_PROMOTION_DETAILS] = state => ({
  ...state,
  loading: true,
  errors: null,
  promotion: null
});

handlers[FETCH_PROMOTION_DETAILS_SUCCESS] = (state, { promotion }) => ({
  ...state,
  promotion,
  errors: null,
  loading: false
});

handlers[FETCH_PROMOTION_DETAILS_FAILURE] = (state, { fetchPromotionError }) => ({
  ...state,
  fetchPromotionError,
  loading: false
});

handlers[EDIT_PROMOTION] = state => ({
  ...state,
  editing: true,
  errors: null,
  success: false
});

handlers[EDIT_PROMOTION_SUCCESS] = (state, { promotionUpdated }) => ({
  ...state,
  promotionUpdated,
  editing: false,
  errors: null,
  success: true
});

handlers[EDIT_PROMOTION_FAILURE] = (state, { errors }) => ({
  ...state,
  editing: false,
  errors
});

export default createReducerFromHandlers(handlers, initialState);
