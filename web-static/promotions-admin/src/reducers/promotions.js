import createReducerFromHandlers from '@surfline/admin-common/dist/utils/createReducerFromHandlers';
import {
  FETCH_PROMOTIONS,
  FETCH_PROMOTIONS_SUCCESS,
  FETCH_PROMOTIONS_FAILURE,
  SAVE_DISCOUNT_CODES,
  SAVE_DISCOUNT_CODES_FAILURE,
  CLEAR_PROMOTION_CODES,
  TOGGLE_PROMOCODES_DISPLAY,
  SET_PROMOTION_KIND,
} from '../actions/promotions';

const initialState = {
  loading: false,
  error: null,
  promotions: null,
  codes: null,
  hasDuplicateCode: false,
};

const handlers = {};

handlers[FETCH_PROMOTIONS] = state => ({
  ...state,
  loading: true,
  error: null,
  promotions: [],
});

handlers[FETCH_PROMOTIONS_SUCCESS] = (state, { promotions }) => ({
  ...state,
  loading: false,
  promotions,
});

handlers[FETCH_PROMOTIONS_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

handlers[SAVE_DISCOUNT_CODES] = (state, { codes }) => ({
  ...state,
  codes,
  hasDuplicateCode: false,
});

handlers[SAVE_DISCOUNT_CODES_FAILURE] = (state, { hasDuplicateCode }) => ({
  ...state,
  codes: [],
  hasDuplicateCode,
});

handlers[TOGGLE_PROMOCODES_DISPLAY] = (state, { promoCodesEnabled }) => ({
  ...state,
  promoCodesEnabled,
});

handlers[CLEAR_PROMOTION_CODES] = () => initialState;

handlers[SET_PROMOTION_KIND] = (state, { kind }) => ({
  ...state,
  kind,
});

export default createReducerFromHandlers(handlers, initialState);
