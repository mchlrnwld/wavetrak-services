import { combineReducers } from 'redux';
import promotions from './promotions';
import promotion from './promotion';
import create from './create';
import edit from './edit';

export const reducers = combineReducers({
  promotions,
  promotion,
  create,
  edit
});

export default reducers;
