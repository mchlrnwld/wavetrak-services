import createReducerFromHandlers from '@surfline/admin-common/dist/utils/createReducerFromHandlers';

import {
  FETCH_PROMOTION,
  FETCH_PROMOTION_SUCCESS,
  FETCH_PROMOTION_FAILURE,
  REMOVE_PROMOTION,
  REMOVE_PROMOTION_SUCCESS,
  REMOVE_PROMOTION_FAILURE,
  FETCH_PROMO_CODES,
  FETCH_PROMO_CODES_SUCCESS,
  FETCH_PROMO_CODES_FAILURE,
  CREATE_CODES,
  CREATE_CODES_SUCCESS,
  CREATE_CODES_FAILURE,

} from '../actions/promotion';

const initialState = {
  removing: false,
  success: false,
};
const handlers = {};

handlers[FETCH_PROMOTION] = state => ({
  ...state,
});

handlers[FETCH_PROMOTION_SUCCESS] = (state, { promotion }) => ({
  ...state,
  promotion,
});

handlers[FETCH_PROMOTION_FAILURE] = (state, { error }) => ({
  ...state,
  error
});

handlers[REMOVE_PROMOTION] = state => ({
  ...state,
  removing: true,
});

handlers[REMOVE_PROMOTION_SUCCESS] = state => ({
  ...state,
  removing: false,
  success: true,
});

handlers[REMOVE_PROMOTION_FAILURE] = (state, { error }) => ({
  ...state,
  removing: false,
  error,
});

handlers[FETCH_PROMO_CODES] = state => ({
  ...state,
  loading: true,
});

handlers[FETCH_PROMO_CODES_SUCCESS] = (state, { promotionCodes }) => ({
  ...state,
  promotionCodes,
  loading: false,
});

handlers[FETCH_PROMO_CODES_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

handlers[CREATE_CODES] = state => ({
  ...state,
  uploading: true,
  error: null,
  promoCodeSuccess: null,
});

handlers[CREATE_CODES_SUCCESS] = (state, { message, codes, promotion }) => ({
  ...state,
  promoCodeSuccess: message,
  promotion: promotion || state.promotion,
  promotionCodes: [...state.promotionCodes, ...codes],
  uploading: false,
});

handlers[CREATE_CODES_FAILURE] = (state, { error }) => ({
  ...state,
  uploading: false,
  error,
});

export default createReducerFromHandlers(handlers, initialState);
