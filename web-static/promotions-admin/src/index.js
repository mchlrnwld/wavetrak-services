import React from 'react';
import 'babel-polyfill';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import store, { history } from './store';
import App from './App';

const target = document.querySelector('#root');
render(
  <Provider store={store}>
    <App history={history} />
  </Provider>,
  target,
);
