import React, { useState, useEffect } from 'react';
import {
  Button, Select, Input, Alert
} from '@surfline/quiver-react';
import { getPerksHero, createPerksHero, deletePerksHero } from '../common/api';

const brands = [
  { value: 'sl', text: 'Surfline' },
  { value: 'fs', text: 'Fishtrack' },
  { value: 'bw', text: 'BuoyWeather' },
];

const productStyle = {
  width: '180px',
  height: '40px',
  margin: '0 10px',
};
const successStyle = {
  width: '150px',
  margin: '10px 10px',
};

const PerksHero = () => {
  const [perksHero, setPerksHero] = useState(null);
  const [fetchData, setFetchData] = useState(true);
  const [showLoading, setShowLoading] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  const [brand, setBrand] = useState('sl');
  const [productName, setProductName] = useState('');
  const [productImgPath, setProductImgPath] = useState('');

  const onChange = (evt) => {
    if (evt.target.value) {
      setBrand(evt.target.value);
      setShowSuccess(false);
    }
  };
  useEffect(() => {
    const fetchPerksHeroData = async () => {
      const data = await getPerksHero();
      setPerksHero(data);
      setShowLoading(false);
    };
    fetchPerksHeroData();
    setFetchData(false);
  }, [fetchData]);
  const saveItem = async () => {
    if (!productName.trim().length || !productImgPath.trim().length) return false;
    setShowLoading(true);
    const data = {
      brand,
      productName,
      productImgPath,
    };
    await createPerksHero(data);
    setProductName(' '); // Reset to defaults. The value '' is not rerendering the component. So I am setting ' '.
    setProductImgPath(' ');
    setShowLoading(false);
    setShowSuccess(true);
    setFetchData(true);
    return true;
  };

  const deleteItem = async (selectedItem) => {
    try {
      setShowLoading(true);
      await deletePerksHero(selectedItem);
      setShowLoading(false);
      setShowSuccess(true);
      setFetchData(true);
    } catch (err) {
      setShowLoading(false);
      setShowSuccess(false);
      setFetchData(true);
    }
  };
  return (
    <div className="perks-hero__container">
      <h4>Add/Edit</h4>
      {
        showSuccess ? <Alert type="success" style={successStyle}>Success!!</Alert> : null
      }
      <div className="perks-hero__form">
        <Select
          type="select"
          select={{ onChange }}
          options={brands}
          style={productStyle}
          defaultValue={`${brand}` || null}
        />
        <Input
          value={productName || null}
          label="Name"
          id="productName"
          input={{
            name: 'productName',
            maxLength: 50,
            onChange: event => setProductName(event.target.value),
          }}
          style={productStyle}
        />
        <Input
          value={productImgPath || null}
          label="Image Path"
          id="productImgPath"
          input={{
            name: 'productImgPath',
            maxLength: 100,
            onChange: event => setProductImgPath(event.target.value),
          }}
          style={productStyle}
        />
        <Button
          loading={showLoading}
          onClick={
              () => saveItem()
            }
        >
              Save
        </Button>
      </div>
      <div className="promotions-list__container perks-hero__list">
        <div className="promotions-list__container__heading">
          <div className="promotions-list__container__row">
            <div className="promotions-list__container__cell perks-hero-list__cell--brand">Brand</div>
            <div className="promotions-list__container__cell perks-hero-list__cell--name">Name</div>
            <div className="promotions-list__container__cell perks-hero-list__cell--img">Image Path</div>
            <div className="promotions-list__container__cell perks-hero-list__cell--action">Action</div>
          </div>
        </div>
        <div className="promotions-list__container__body">
          {perksHero && perksHero.map((hero) => {
            if (!hero) return null;
            return (
              <div className="promotions-list__container__row">
                <div className="promotions-list__container__cell perks-hero-list__cell--brand">
                  {hero.brand}
                </div>
                <div className="promotions-list__container__cell perks-hero-list__cell--name">
                  {hero.productName}
                </div>
                <div className="promotions-list__container__cell perks-hero-list__cell--img">
                  {hero.productImgPath}
                </div>
                <div className="promotions-list__container__cell perks-hero-list__cell--action">
                  <Button
                    loading={showLoading}
                    onClick={
            () => deleteItem(hero.brand)
          }
                  >Delete
                  </Button>
                </div>
              </div>
            );
          })}
        </div>

      </div>
    </div>
  );
};

export default PerksHero;
