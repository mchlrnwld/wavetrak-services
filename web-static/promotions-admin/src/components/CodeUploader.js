import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@surfline/quiver-react';
import ReactFileReader from 'react-file-reader';

const CodeUploader = ({
  doUploadCodes,
  uploading
}) => (
  <div>
    <div className="promo-code__info">
      {`Promo Codes for Direct To Pay promotions are distributed
        via email after a subscription has been successfully created.
        They are not redeemed through this funnel.`}
    </div>
    <ReactFileReader
      elementId="discountcode_uploader"
      fileTypes={['.csv']}
      base64
      handleFiles={doUploadCodes}
    >
      <div className="discountcode_uploader">
        <Button
          loading={uploading}
          label="Upload"
          onClick={event => event.preventDefault()}
        >
          Upload Promo Codes
        </Button>
      </div>
    </ReactFileReader>
  </div>
);

CodeUploader.propTypes = {
  doUploadCodes: PropTypes.func.isRequired,
  uploading: PropTypes.bool.isRequired
};

CodeUploader.defaultProps = {
};

export default CodeUploader;
