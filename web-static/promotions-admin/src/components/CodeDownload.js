import React from 'react';
import PropTypes from 'prop-types';
import { CSVLink } from 'react-csv';

const CodeDownload = ({ codes, promoCodePrefix }) => (
  <div>
    <CSVLink
      data={codes}
      filename={`${promoCodePrefix}_promocodes.csv`}
      className="promo-code__download-link"
    >
      Download Promo Codes
    </CSVLink>
  </div>
);

CodeDownload.propTypes = {
  codes: PropTypes.arrayOf(PropTypes.shape({})),
  promoCodePrefix: PropTypes.string
};

CodeDownload.defaultProps = {
  codes: null,
  promoCodePrefix: null,
};

export default CodeDownload;
