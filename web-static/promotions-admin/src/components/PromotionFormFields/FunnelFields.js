import React from 'react';
import PropTypes from 'prop-types';
import { Input, TextArea } from '@surfline/quiver-react';

const FunnelFields = ({
  promotion,
  errors,
  styles,
}) => (
  <div>
    <Input
      defaultValue={promotion.urlKey || null}
      label="URL KEY"
      id="urlKey"
      style={styles.field}
      input={{ name: 'urlKey' }}
      error={errors && 'urlKey' in errors}
      message={errors && errors.urlKey ? errors.urlKey.message : null}
    />
    <TextArea
      defaultValue={promotion.signUpHeader || null}
      label="Signup Header"
      id="signUpHeader"
      style={styles.field}
      textarea={{ name: 'signUpHeader' }}
      error={errors && 'signUpHeader' in errors}
      message={
          errors && errors.signUpHeader ? errors.signUpHeader.message : null
        }
    />
    <TextArea
      defaultValue={promotion.successHeader || null}
      label="Success Header"
      id="successHeader"
      style={styles.field}
      textarea={{ name: 'successHeader' }}
      error={errors && 'successHeader' in errors}
      message={
          errors && errors.successHeader ? errors.successHeader.message : null
        }
    />
    <TextArea
      defaultValue={promotion.successMessage || null}
      label="Success Message"
      id="successMessage"
      style={styles.field}
      textarea={{ name: 'successMessage' }}
      error={errors && 'successMessage' in errors}
      message={
          errors && errors.successMessage ? errors.successMessage.message : null
        }
    />
    <Input
      defaultValue={promotion.successUrl || null}
      label="Success Url"
      id="successUrl"
      style={styles.field}
      input={{ name: 'successUrl' }}
      error={errors && 'successUrl' in errors}
      message={
          errors && errors.successUrl ? errors.successUrl.message : null
        }
    />
  </div>
);

FunnelFields.propTypes = {
  promotion: PropTypes.shape({
    name: PropTypes.string,
    urlKey: PropTypes.string,
    brand: PropTypes.string,
    isActive: PropTypes.bool,
    signUpHeader: PropTypes.string,
    successHeader: PropTypes.string,
    successMessage: PropTypes.string,
  }),
  styles: PropTypes.shape({}).isRequired,
  errors: PropTypes.shape({}),
};

FunnelFields.defaultProps = {
  promotion: {
    name: null,
    urlKey: null,
    brand: null,
    isActive: null,
    signUpHeader: null,
    successHeader: null,
    successMessage: null,
  },
  errors: {},
};

export default FunnelFields;
