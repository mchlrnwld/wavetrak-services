import React from 'react';
import PropTypes from 'prop-types';
import {
  Input,
  TextArea,
  Select
} from '@surfline/quiver-react';
import PartnerFields from './PartnerFields';
import FunnelFields from './FunnelFields';
import CurrencyField from './CurrencyField';

const DirectToPayFields = ({
  promotion,
  styles,
  errors,
}) => (
  <div>
    <TextArea
      defaultValue={promotion.paymentHeader || null}
      label="Payment Header"
      id="paymentHeader"
      style={styles.field}
      textarea={{ name: 'paymentHeader' }}
      error={errors && 'paymentHeader' in errors}
      message={
        errors && errors.paymentHeader
          ? errors.paymentHeader.message
          : null
      }
    />
    <PartnerFields promotion={promotion} styles={styles} errors={errors} />
    <Input
      defaultValue={promotion.buttonText || null}
      label="Payment Button Text"
      id="buttonText"
      style={styles.field}
      input={{ name: 'buttonText' }}
      error={errors && ('buttonText' in errors)}
    />
    <Input
      defaultValue={promotion.maxRedemption || 0}
      label="Max Redemption"
      id="maxRedemption"
      style={styles.field}
      input={{ name: 'maxRedemption' }}
      error={errors && ('maxRedemption' in errors || 'discountCodes' in errors)}
      // message={showCustomError(errors)}
    />
    <Select
      defaultValue={`${promotion.plans}` || null}
      label="Plans"
      id="plans"
      style={{ width: 230, ...styles.field }}
      type="select"
      options={[
        { text: 'Annual Only', value: 'annual' },
        { text: 'Monthly Only', value: 'monthly' },
        { text: 'Annual & Monthly', value: 'all' },
      ]}
      select={{ name: 'plans' }}
    />
    <CurrencyField promotion={promotion} styles={styles} />
    <FunnelFields promotion={promotion} errors={errors} styles={styles} />
  </div>
);

DirectToPayFields.propTypes = {
  promotion: PropTypes.shape({
    paymentHeader: PropTypes.string,
    maxRedemption: PropTypes.string,
  }),
  styles: PropTypes.shape({}).isRequired,
  errors: PropTypes.shape({}),
};

DirectToPayFields.defaultProps = {
  promotion: {
    paymentHeader: null,
    maxRedemption: null,
  },
  errors: {},
};

export default DirectToPayFields;
