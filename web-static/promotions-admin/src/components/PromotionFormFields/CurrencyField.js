import React from 'react';
import PropTypes from 'prop-types';
import { Select } from '@surfline/quiver-react';


const currencyField = ({ promotion, styles }) => (
  <Select
    defaultValue={`${promotion.currency}` || null}
    label="Eligibe Currency"
    id="currency"
    style={{ width: 230, ...styles.field }}
    type="select"
    options={[
      { text: 'Any', value: 'ANY' },
      { text: 'USD (US Dollar)', value: 'USD' },
      { text: 'EUR (Euro)', value: 'EUR' },
      { text: 'ILS (Israeli New Shekel)', value: 'ILS' },
      { text: 'GBP (Pound Sterling)', value: 'GBP' },
      { text: 'AUD (Austrailian Dollar)', value: 'AUD' },
      { text: 'NZD (New Zealand Dollar)', value: 'NZD' },
    ]}
    select={{ name: 'currency' }}
  />
);

currencyField.propTypes = {
  promotion: PropTypes.shape({
    currency: PropTypes.string
  }),
  styles: PropTypes.shape({}).isRequired
};

currencyField.defaultProps = {
  promotion: {
    currency: 'ANY'
  }
};

export default currencyField;
