import React from 'react';
import PropTypes from 'prop-types';
import { Input, Select } from '@surfline/quiver-react';

const BaseFields = ({
  promotion,
  errors,
  styles,
  kind
}) => (
  <div>
    {promotion._id ? (
      <Input
        id="id"
        type="hidden"
        defaultValue={promotion._id || null}
        input={{ name: 'id' }}
      />
    ) : null}
    <Input
      id="kind"
      type="hidden"
      defaultValue={promotion.kind || kind}
      input={{ name: 'kind' }}
    />
    <Input
      defaultValue={promotion.name || null}
      label="Name"
      id="name"
      style={styles.field}
      type="text"
      input={{ name: 'name', required: true }}
      error={errors && 'name' in errors}
      message={errors && errors.name ? errors.name.message : null}
    />
    <Select
      defaultValue={`${promotion.brand}` || null}
      label="Brand"
      id="brand"
      style={{ width: 230, ...styles.field }}
      type="select"
      options={[
        { text: 'Surfline', value: 'sl' },
        { text: 'Fishtrack', value: 'fs' },
        { text: 'Buoyweather', value: 'bw' },
      ]}
      select={{ name: 'brand' }}
    />
    <Select
      defaultValue={`${!!promotion.isActive}` || true}
      label="Status"
      id="isActive"
      style={{ width: 230, ...styles.field }}
      type="select"
      options={[
        { text: 'ACTIVE', value: true },
        { text: 'INACTIVE', value: false },
      ]}
      select={{ name: 'isActive' }}
    />
  </div>
);

BaseFields.propTypes = {
  promotion: PropTypes.shape({
    name: PropTypes.string,
    urlKey: PropTypes.string,
    brand: PropTypes.string,
    isActive: PropTypes.bool,
    signUpHeader: PropTypes.string,
    successHeader: PropTypes.string,
    successMessage: PropTypes.string,
  }),
  kind: PropTypes.string.isRequired,
  styles: PropTypes.shape({}).isRequired,
  errors: PropTypes.shape({}),
};

BaseFields.defaultProps = {
  promotion: {
    name: null,
    urlKey: null,
    brand: null,
    isActive: null,
    signUpHeader: null,
    successHeader: null,
    successMessage: null,
  },
  errors: {},
};

export default BaseFields;
