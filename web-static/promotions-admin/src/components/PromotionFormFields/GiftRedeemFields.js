import React from 'react';
import PropTypes from 'prop-types';
import { TextArea } from '@surfline/quiver-react';
import FunnelFields from './FunnelFields';

const GiftRedeemFields = ({ promotion, styles, errors }) => (
  <div>
    <TextArea
      defaultValue={promotion.paymentHeader || null}
      label="Payment Header"
      id="paymentHeader"
      style={styles.field}
      textarea={{ name: 'paymentHeader' }}
      error={errors && 'paymentHeader' in errors}
      message={
        errors && errors.paymentHeader
          ? errors.paymentHeader.message
          : null
      }
    />
    <FunnelFields promotion={promotion} errors={errors} styles={styles} />
  </div>
);

GiftRedeemFields.propTypes = {
  promotion: PropTypes.shape({
    paymentHeader: PropTypes.string,
  }),
  styles: PropTypes.shape({}).isRequired,
  errors: PropTypes.shape({})
};

GiftRedeemFields.defaultProps = {
  promotion: {
    paymentHeader: null,
  },
  errors: {},
};

export default GiftRedeemFields;
