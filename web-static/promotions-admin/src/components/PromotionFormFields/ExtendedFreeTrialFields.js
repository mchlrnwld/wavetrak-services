import React from 'react';
import PropTypes from 'prop-types';
import { Input, TextArea, Select } from '@surfline/quiver-react';
import PartnerFields from './PartnerFields';
import FunnelFields from './FunnelFields';
import CurrencyField from './CurrencyField';

const ExtendedFreeTrialFeilds = ({ promotion, styles, errors }) => (
  <div>
    <TextArea
      defaultValue={promotion.paymentHeader || null}
      label="Payment Header"
      id="paymentHeader"
      style={styles.field}
      textarea={{ name: 'paymentHeader' }}
      error={errors && 'paymentHeader' in errors}
      message={
        errors && errors.paymentHeader ? errors.paymentHeader.message : null
      }
    />
    <PartnerFields promotion={promotion} styles={styles} errors={errors} />
    <Select
      defaultValue={`${promotion.plans}` || null}
      label="Plans"
      id="plans"
      style={{ width: 230, ...styles.field }}
      type="select"
      options={[
        { text: 'Annual Only', value: 'annual' },
        { text: 'Monthly Only', value: 'monthly' },
        { text: 'Annual & Monthly', value: 'all' },
      ]}
      select={{ name: 'plans' }}
    />
    <CurrencyField promotion={promotion} styles={styles} />
    <Input
      defaultValue={promotion.trialLength || null}
      label="Trial Length"
      id="trialLength"
      style={styles.field}
      input={{ name: 'trialLength' }}
      error={errors && 'trialLength' in errors}
      message={errors && errors.trialLength ? errors.trialLength.message : null}
    />
    <Input
      defaultValue={promotion.maxRedemption || 0}
      label="Max Redemption"
      id="maxRedemption"
      style={styles.field}
      input={{ name: 'maxRedemption' }}
      error={errors && ('maxRedemption' in errors || 'discountCodes' in errors)}
      // message={showCustomError(errors)}
    />
    <FunnelFields promotion={promotion} errors={errors} styles={styles} />
  </div>
);

ExtendedFreeTrialFeilds.propTypes = {
  promotion: PropTypes.shape({
    paymentHeader: PropTypes.string,
    trialLength: PropTypes.string,
    maxRedemption: PropTypes.string,
  }),
  styles: PropTypes.shape({}).isRequired,
  errors: PropTypes.shape({}),
};

ExtendedFreeTrialFeilds.defaultProps = {
  promotion: {
    paymentHeader: null,
    trialLength: null,
    maxRedemption: null,
  },
  errors: {},
};

export default ExtendedFreeTrialFeilds;
