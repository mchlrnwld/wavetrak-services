import React from 'react';
import PropTypes from 'prop-types';
import { Input, TextArea } from '@surfline/quiver-react';

const PartnerFields = ({ promotion, styles, errors }) => (
  <div>
    <Input
      defaultValue={promotion.bgDesktopImgPath || null}
      label="Background Desktop Image Path"
      id="bgDesktopImgPath"
      style={styles.field}
      input={{ name: 'bgDesktopImgPath' }}
      error={errors && 'bgDesktopImgPath' in errors}
      message={
        errors && errors.bgDesktopImgPath
          ? errors.bgDesktopImgPath.message
          : null
      }
    />
    <Input
      defaultValue={promotion.bgMobileImgPath || null}
      label="Background Mobile Image Path"
      id="bgMobileImgPath"
      style={styles.field}
      input={{ name: 'bgMobileImgPath' }}
      error={errors && 'bgMobileImgPath' in errors}
      message={
        errors && errors.bgMobileImgPath ? errors.bgMobileImgPath.message : null
      }
    />
    <Input
      defaultValue={promotion.logo || null}
      label="Logo"
      id="logo"
      style={styles.field}
      input={{ name: 'logo' }}
      error={errors && 'logo' in errors}
      message={
        errors && errors.logo
          ? errors.logo.message
          : null
      }
    />
    <Input
      defaultValue={promotion.logoAltText || null}
      label="Logo Alt Text"
      id="logoAltText"
      style={styles.field}
      input={{ name: 'logoAltText' }}
      error={errors && 'logoAltText' in errors}
      message={
        errors && errors.logoAltText
          ? errors.logoAltText.message
          : null
      }
    />
    <TextArea
      defaultValue={promotion.detailCopy || null}
      label="Detail Copy"
      id="detailCopy"
      style={styles.field}
      textarea={{ name: 'detailCopy' }}
      error={errors && 'detailCopy' in errors}
      message={
        errors && errors.detailCopy
          ? errors.detailCopy.message
          : null
      }
    />
  </div>
);

PartnerFields.propTypes = {
  promotion: PropTypes.shape({
    bgDesktopImgPath: PropTypes.string,
    bgMobileImgPath: PropTypes.string,
    logo: PropTypes.string,
    logoAltText: PropTypes.string,
    detailCopy: PropTypes.string,
  }),
  styles: PropTypes.shape({}).isRequired,
  errors: PropTypes.shape({}),
};

PartnerFields.defaultProps = {
  promotion: {
    bgDesktopImgPath: null,
    bgMobileImgPath: null,
    logo: null,
    logoAltText: null,
    detailCopy: null,
  },
  errors: {}
};

export default PartnerFields;
