import React from 'react';
import PropTypes from 'prop-types';
import { Input, TextArea, Select } from '@surfline/quiver-react';
import FunnelFields from './FunnelFields';

const RetentionOfferFields = ({ promotion, styles, errors }) => (
  <div>
    <TextArea
      defaultValue={promotion.paymentHeader || null}
      label="Payment Header"
      id="paymentHeader"
      style={styles.field}
      textarea={{ name: 'paymentHeader' }}
      error={errors && 'paymentHeader' in errors}
      message={
        errors && errors.paymentHeader
          ? errors.paymentHeader.message
          : null
      }
    />
    <TextArea
      defaultValue={promotion.paymentMessage || null}
      label="Payment Message"
      id="paymentMessage"
      style={styles.field}
      textarea={{ name: 'paymentMessage' }}
      error={errors && 'paymentMessage' in errors}
      message={
        errors && errors.paymentMessage
          ? errors.paymentMessage.message
          : null
      }
    />
    <Input
      defaultValue={promotion.offeredPlan || null}
      label="Offered Plan Id"
      id="offeredPlan"
      style={styles.field}
      input={{ name: 'offeredPlan' }}
      error={errors && 'offeredPlan' in errors}
      message={
        errors && errors.offeredPlan ? errors.offeredPlan.message : null
      }
    />
    <Input
      defaultValue={promotion.stripeCouponCode || null}
      label="Stripe Coupon Code"
      id="stripeCouponCode"
      style={styles.field}
      input={{ name: 'stripeCouponCode' }}
      error={errors && 'stripeCouponCode' in errors}
      message={
        errors && errors.stripeCouponCode ? errors.stripeCouponCode.message : null
      }
    />
    <Select
      defaultValue={`${promotion.eligibilityType}` || null}
      label="Eligibility Type"
      id="eligibilityType"
      style={{ width: 230, ...styles.field }}
      type="select"
      options={[
        { text: 'Consecutive Renewals', value: 'renewals' },
        { text: 'Duration of Premium', value: 'duration' },
      ]}
      select={{ name: 'eligibilityType' }}
    />
    <Input
      defaultValue={promotion.eligibilityTarget || 0}
      type="number"
      label="Eligibility Target"
      id="eligibilityTarget"
      style={styles.field}
      input={{ name: 'eligibilityTarget' }}
      error={errors && 'eligibilityTarget' in errors}
      message={
        errors && errors.eligibilityTarget ? errors.eligibilityTarget.message : null
      }
    />
    <Select
      defaultValue={`${promotion.eligiblePlan}` || null}
      label="Eligible Plan(s)"
      id="eligiblePlan"
      style={{ width: 230, ...styles.field }}
      type="select"
      options={[
        { text: 'sl_monthly_v2', value: 'sl_monthly_v2' },
        { text: 'sl_annual_v2', value: 'sl_annual_v2' },
      ]}
      select={{ name: 'eligiblePlan' }}
    />
    <Select
      defaultValue={`${promotion.eligibleStatus}` || null}
      label="Eligible Status"
      id="eligibleStatus"
      style={{ width: 230, ...styles.field }}
      type="select"
      options={[
        { text: 'Active', value: 'active' },
        { text: 'Expiring', value: 'expiring' }
      ]}
      select={{ name: 'eligibleStatus' }}
    />
    <FunnelFields promotion={promotion} errors={errors} styles={styles} />
  </div>
);

RetentionOfferFields.propTypes = {
  promotion: PropTypes.shape({
    paymentHeader: PropTypes.string,
    paymentMessage: PropTypes.string,
    offeredPlan: PropTypes.string,
    stripeCouponCode: PropTypes.string,
    eligibilityType: PropTypes.string,
    eligibiltyTarget: PropTypes.string,
    eligiblePlan: PropTypes.string,
    eligibleStatus: PropTypes.string,
  }),
  styles: PropTypes.shape({}).isRequired,
  errors: PropTypes.shape({}),
};

RetentionOfferFields.defaultProps = {
  promotion: {
    paymentHeader: null,
    paymentMessage: null,
    offeredPlan: null,
    stripeCouponCode: null,
    eligibilityType: null,
    eligibiltyTarget: null,
    eligiblePlan: null,
    eligibleStatus: null,
  },
  errors: {},
};

export default RetentionOfferFields;
