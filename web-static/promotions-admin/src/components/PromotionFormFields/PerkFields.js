import React from 'react';
import PropTypes from 'prop-types';
import { TextArea, Input, Select } from '@surfline/quiver-react';

const PerkFields = ({ promotion, styles, errors }) => (
  <div>
    <Select
      defaultValue={`${!!promotion.isPremium}` || true}
      label="Premium"
      id="isPremium"
      style={{ width: 230, ...styles.field }}
      type="select"
      options={[
        { text: 'YES', value: true },
        { text: 'NO', value: false },
      ]}
      select={{ name: 'isPremium' }}
    />
    <Input
      defaultValue={promotion.codesPerUser || null}
      label="Codes Per User"
      id="codesPerUser"
      style={styles.field}
      input={{ name: 'codesPerUser', required: true }}
      error={errors && 'codesPerUser' in errors}
    />
    <Input
      defaultValue={promotion.frequency || null}
      label="Recurring Frequency In Days"
      id="frequency"
      style={styles.field}
      input={{ name: 'frequency', required: true }}
      error={errors && 'frequency' in errors}
    />
    <TextArea
      defaultValue={promotion.description || null}
      label="Perk Description"
      id="description"
      style={styles.field}
      textarea={{ name: 'description', maxLength: 300 }}
      error={errors && 'description' in errors}
      message={
        errors && errors.description
          ? errors.description.message
          : null
      }
    />
    <TextArea
      defaultValue={promotion.freeDescription || null}
      label="Free Perk Description"
      id="freeDescription"
      style={styles.field}
      textarea={{ name: 'freeDescription', maxLength: 300 }}
      error={errors && 'freeDescription' in errors}
      message={
        errors && errors.freeDescription
          ? errors.freeDescription.message
          : null
      }
    />
    <TextArea
      defaultValue={promotion.discountText || null}
      label="Discount Text"
      id="discountText"
      style={styles.field}
      textarea={{ name: 'discountText', maxLength: 100 }}
      error={errors && 'discountText' in errors}
      message={
        errors && errors.discountText
          ? errors.discountText.message
          : null
      }
    />
    <Input
      defaultValue={promotion.bgDesktopImgPath || null}
      label="Perk Desktop Image Path"
      id="bgDesktopImgPath"
      style={styles.field}
      input={{ name: 'bgDesktopImgPath' }}
      error={errors && 'bgDesktopImgPath' in errors}
      message={
        errors && errors.bgDesktopImgPath
          ? errors.bgDesktopImgPath.message
          : null
      }
    />
    <Input
      defaultValue={promotion.bgMobileImgPath || null}
      label="Perk Mobile Image Path"
      id="bgMobileImgPath"
      style={styles.field}
      input={{ name: 'bgMobileImgPath' }}
      error={errors && 'bgMobileImgPath' in errors}
      message={
        errors && errors.bgMobileImgPath
          ? errors.bgMobileImgPath.message
          : null
      }
    />
    <Input
      defaultValue={promotion.logoImgPath || null}
      label="Perk Logo Image Path"
      id="logoImgPath"
      style={styles.field}
      input={{ name: 'logoImgPath' }}
      error={errors && 'logoImgPath' in errors}
      message={
        errors && errors.logoImgPath
          ? errors.logoImgPath.message
          : null
      }
    />
    <Input
      defaultValue={promotion.partnerUrl || null}
      label="Partner Url"
      id="partnerUrl"
      style={styles.field}
      input={{ name: 'partnerUrl', maxLength: 300 }}
      error={errors && 'partnerUrl' in errors}
      message={
        errors && errors.partnerUrl
          ? errors.partnerUrl.message
          : null
      }
    />
  </div>
);

PerkFields.propTypes = {
  promotion: PropTypes.shape({
    paymentHeader: PropTypes.string,
  }),
  styles: PropTypes.shape({}).isRequired,
  errors: PropTypes.shape({})
};

PerkFields.defaultProps = {
  promotion: {
    paymentHeader: null,
  },
  errors: {},
};

export default PerkFields;
