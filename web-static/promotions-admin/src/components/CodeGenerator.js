import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from '@surfline/quiver-react';
import CodeDownload from './CodeDownload';

const styles = {
  field: {
    marginTop: '24px',
    marginBottom: '24px',
    width: '100%',
  }
};

const CodeGenerator = ({
  promoCodePrefix,
  maxRedemption,
  promotionCodes: codes,
  doGenerateCodes,
  uploading,
}) => {
  const codesGenerated = codes ? codes.length : 0;
  const canGenerateCodes = codesGenerated < maxRedemption;
  const codesToGenerate = maxRedemption - codesGenerated;

  const [prefixValue, setPrefixValue] = useState(null);
  const handleSubmit = (event) => {
    event.preventDefault();
    doGenerateCodes({
      promotionCodePrefix: promoCodePrefix || prefixValue,
      amount: codesToGenerate,
    });
  };

  if (!maxRedemption) {
    return (
      <div className="promo-code__warning">
        Please add a max redemptions value to this promotion before proceeding.  Thanks!
      </div>
    );
  }

  if (!canGenerateCodes) {
    return (
      <div>
        <div className="promo-code__warning">
          {`The promo codes for this promotion have reached their limit.
            A total of ${codesGenerated} out of a possible ${maxRedemption} already exist
            and are available for download.  To generate more codes, please increase your
            max redemptions.`}
        </div>
        <CodeDownload codes={codes} promoCodePrefix={promoCodePrefix} />
      </div>
    );
  }
  return (
    <div>
      <div className="promo-code__info">
        {`You are about to create ${codesToGenerate}
          codes out of ${maxRedemption} redemptions.
          Currently, ${codesGenerated || 0} codes exist for this promotion.`}
        <div className="promo-code__info__prefix">
          {promoCodePrefix ? (
            <div>
              Each code will be prefixed with the following characters:
              <h4>{promoCodePrefix}</h4>
            </div>
          ) : (
            <div>
              {`Promo Codes must contain a prefix before generation.
                Please enter a prefix below.`}
            </div>
          )}
        </div>
      </div>
      {promoCodePrefix ? (
        <Button
          loading={uploading}
          onClick={handleSubmit}
        >
          GENERATE
        </Button>
      ) : (
        <div>
          <form onSubmit={handleSubmit}>
            <Input
              defaultValue={prefixValue}
              label="PROMO CODE PREFIX"
              style={styles.field}
              type="text"
              input={{
                maxLength: 5,
                onChange: event => setPrefixValue(event.target.value),
              }}
            />
            <Button loading={uploading}>GENERATE</Button>
          </form>
        </div>
      )}
      {codes && codes.length ? (
        <CodeDownload codes={codes} promoCodePrefix={promoCodePrefix} />
      ) : null}
    </div>
  );
};

CodeGenerator.propTypes = {
  promoCodePrefix: PropTypes.string,
  maxRedemption: PropTypes.number.isRequired,
  promotionCodes: PropTypes.arrayOf(PropTypes.shape({})),
  doGenerateCodes: PropTypes.func.isRequired,
  uploading: PropTypes.bool.isRequired
};

CodeGenerator.defaultProps = {
  promoCodePrefix: null,
  promotionCodes: null
};

export default CodeGenerator;
