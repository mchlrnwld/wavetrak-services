import React from 'react';
import PropTypes from 'prop-types';
import PromotionForm from '../PromotionForm';

const styles = {
  formWrapper: {
    width: 410,
    margin: '0 auto'
  }
};

const Promotion = ({
  create,
  handleDiscountCodes,
  codes,
  hasDuplicateCode,
  creating,
  errors,
  success,
  handlePromoCodes,
  promoCodesEnabled,
  handlePromoType,
  promoType
}) => (
  <div style={styles.formWrapper}>
    <h4>
Create Promotion
    </h4>
    <PromotionForm
      action={create}
      handleDiscountCodes={handleDiscountCodes}
      codes={codes}
      hasDuplicateCode={hasDuplicateCode}
      processing={creating}
      errors={errors}
      success={success}
      handlePromoCodes={handlePromoCodes}
      promoCodesEnabled={promoCodesEnabled}
      handlePromoType={handlePromoType}
      promoType={promoType}
    />
  </div>
);

Promotion.propTypes = {
  errors: PropTypes.shape({}),
  create: PropTypes.func.isRequired,
  codes: PropTypes.array,
  hasDuplicateCode: PropTypes.bool,
  handleDiscountCodes: PropTypes.func.isRequired,
  creating: PropTypes.bool,
  success: PropTypes.bool,
  handlePromoCodes: PropTypes.func.isRequired,
  promoCodesEnabled: PropTypes.bool,
  handlePromoType: PropTypes.func.isRequired,
  promoType: PropTypes.string
};

Promotion.defaultProps = {
  errors: {},
  creating: false,
  success: false,
  hasDuplicateCode: false,
  codes: [],
  promoCodesEnabled: false,
  promoType: null
};


export default Promotion;
