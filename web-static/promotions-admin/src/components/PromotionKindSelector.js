import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const types = [
  {
    kind: 'Extended Free Trial',
    link: '/promotions/create/extended-trial',
    description: 'Useful for redeeming partner promo-codes.',
    disclaimer: '**Annual Plans Only**',
  },
  {
    kind: 'Direct To Pay',
    link: '/promotions/create/direct-to-pay',
    description: 'Useful for distributing gifts and partner codes',
    disclaimer: '',
  },
  {
    kind: 'Gift Redeem',
    link: '/promotions/create/gift-redeem',
    description: 'Redeeming partner affiliated Surfline Gift Cards',
  },
  {
    kind: 'Retention Offer',
    link: '/promotions/create/offer',
    description: 'Useful for offering discounted upgrades or winbacks',
  },
  {
    kind: 'Perk',
    link: '/promotions/create/perk',
    description: 'Create premium perks',
  },
];

const PromotionKindSelector = ({ doSetPromotionKind }) => (
  <div className="promotions__select-type">
    <div className="promotions__select-type__header">
      <div className="promotions__select-type__header__title">
        What type of promotion would you like to create?
      </div>
    </div>
    <div className="promotions__select-type__list">
      {types.map(({
        kind,
        description,
        link,
        disclaimer
      }) => (
        <Link
          onClick={() => doSetPromotionKind(kind)}
          to={link}
          className="promotions__select-type__link"
          key={kind}
        >
          <div className="promotions__select-type__content">
            <div className="promotions__select-type__name">
              {kind}
              <span>{disclaimer || null}</span>
            </div>
            <div className="promotions__select-type__description">
              {description}
            </div>
          </div>
        </Link>
      ))}
    </div>
  </div>
);

PromotionKindSelector.propTypes = {
  doSetPromotionKind: PropTypes.func.isRequired,
};

export default PromotionKindSelector;
