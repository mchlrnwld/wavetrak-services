import React from 'react';
import PropTypes from 'prop-types';
import PromotionListRow from './PromotionListRow';

const PerkPromotions = ({ promotions }) => (
  <div className="promotions-list__container">
    <div className="promotions-list__container__heading">
      <div className="promotions-list__container__row">
        <div className="promotions-list__container__cell">ID</div>
        <div className="promotions-list__container__cell promotions-list__container__cell--brand">
          Brand
        </div>
        <div className="promotions-list__container__cell">Name</div>
        <div className="promotions-list__container__cell promotions-list__container__cell--status">
          Status
        </div>
        <div className="promotions-list__container__cell">Type</div>
        <div className="promotions-list__container__cell promotions-list__container__cell--status">
          Redemptions
        </div>
      </div>
    </div>
    <div className="promotions-list__container__body">
      {promotions.map(promotion => (
        <PromotionListRow key={promotion._id} promotion={promotion} />
      ))}
    </div>
  </div>
);

PerkPromotions.propTypes = {
  promotions: PropTypes.array,
};

PerkPromotions.defaultProps = {
  promotions: null,
};

export default PerkPromotions;
