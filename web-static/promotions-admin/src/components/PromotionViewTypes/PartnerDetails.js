import React from 'react';
import PropTypes from 'prop-types';

const PartnerDetails = ({ promotion }) => {
  const {
    logo,
    logoAltText,
    detailCopy,
    bgDesktopImgPath,
    bgMobileImgPath,
  } = promotion;
  return (
    <div>
      <div className="promotion__property">
        <div>Detail Copy: </div>
        <div>{detailCopy}</div>
      </div>
      <div className="promotion__property">
        <div>Logo Alt Text: </div>
        <div>{logoAltText}</div>
      </div>
      <div className="promotion__property">
        <div>Logo Image: </div>
        <div>{logo}</div>
      </div>
      <div className="promotion__property">
        <div>Background Image (Desktop): </div>
        <div>{bgDesktopImgPath}</div>
      </div>
      <div className="promotion__property">
        <div>Background Image (Mobile): </div>
        <div>{bgMobileImgPath}</div>
      </div>
    </div>
  );
};

PartnerDetails.propTypes = {
  promotion: PropTypes.shape({
    logo: PropTypes.string,
    logoAltText: PropTypes.string,
    detailCopy: PropTypes.string,
    bgDesktopImgPath: PropTypes.string,
    bgMobileImgPath: PropTypes.string,
  }).isRequired,
};

PartnerDetails.defaultProps = {};

export default PartnerDetails;
