import React from 'react';
import PropTypes from 'prop-types';
import PartnerDetails from './PartnerDetails';
import SignupAndSuccess from './SignupAndSuccess';

const ExtendedFreeTrial = ({ promotion }) => {
  const {
    trialLength,
    paymentHeader,
    promoCodesEnabled,
    promoCodePrefix,
    codes,
    plans,
    currency
  } = promotion;
  return (
    <div>
      <div className="promotion__property">
        <div>Trial Length: </div>
        <div>{trialLength}</div>
      </div>
      <div className="promotion__property">
        <div>Promo Codes Enabled: </div>
        <div>{`${promoCodesEnabled}`}</div>
      </div>
      <div className="promotion__property">
        <div>Promo Code Prefix: </div>
        <div>{promoCodePrefix}</div>
      </div>
      <div className="promotion__property">
        <div>Promo Codes Created</div>
        <div>{codes ? codes.length : 'None'}</div>
      </div>
      <div className="promotion__property">
        <div>Targeted Plans: </div>
        <div>{plans}</div>
      </div>
      <div className="promotion__property">
        <div>Eligible Currency: </div>
        <div>{currency}</div>
      </div>
      <SignupAndSuccess promotion={promotion} />
      <div className="promotion__property">
        <div>Payment Header: </div>
        <div>{paymentHeader}</div>
      </div>
      <PartnerDetails promotion={promotion} />
    </div>
  );
};

ExtendedFreeTrial.propTypes = {
  promotion: PropTypes.shape({
    trialLength: PropTypes.number,
    paymentHeader: PropTypes.string,
    promoCodesEnabled: PropTypes.bool,
    promoCodePrefix: PropTypes.string,
  }).isRequired,
};

ExtendedFreeTrial.defaultProps = {};

export default ExtendedFreeTrial;
