import React from 'react';
import PropTypes from 'prop-types';
import SignupAndSuccess from './SignupAndSuccess';

const GiftRedeemFunnel = ({ promotion }) => {
  const {
    redeemHeader,
    layeredCardImgPath,
    bgDesktopImgPath,
    bgMobileImgPath,
  } = promotion;
  return (
    <div>
      <SignupAndSuccess promotion={promotion} />
      <div className="promotion__property">
        <div>Redeem Header: </div>
        <div>{redeemHeader}</div>
      </div>
      <div className="promotion__property">
        <div>Layered Card Image: </div>
        <div>{layeredCardImgPath}</div>
      </div>
      <div className="promotion__property">
        <div>Background Image (Desktop): </div>
        <div>{bgDesktopImgPath}</div>
      </div>
      <div className="promotion__property">
        <div>Background Image (Mobile): </div>
        <div>{bgMobileImgPath}</div>
      </div>
    </div>
  );
};

GiftRedeemFunnel.propTypes = {
  promotion: PropTypes.shape({
    redeemHeader: PropTypes.string,
    layeredCardImgPath: PropTypes.string,
    bgDesktopImgPath: PropTypes.string,
    bgMobileImgPath: PropTypes.string,
  }).isRequired,
};

GiftRedeemFunnel.defaultProps = {};

export default GiftRedeemFunnel;
