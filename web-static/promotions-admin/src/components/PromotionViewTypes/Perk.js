import React from 'react';
import PropTypes from 'prop-types';

const Perk = ({ promotion }) => {
  const {
    description,
    freeDescription,
    bgDesktopImgPath,
    bgMobileImgPath,
    logoImgPath,
    discountText,
    codesPerUser,
    frequency,
    isPremium,
    codes,
    partnerUrl,
  } = promotion;
  return (
    <div>
      <div className="promotion__property">
        <div>Premium: </div>
        <div>{`${isPremium}`}</div>
      </div>
      <div className="promotion__property">
        <div>Codes Per User: </div>
        <div>{codesPerUser}</div>
      </div>
      <div className="promotion__property">
        <div>Recurring Frequency In Days: </div>
        <div>{frequency}</div>
      </div>
      <div className="promotion__property">
        <div>Perk Description: </div>
        <div>{description}</div>
      </div>
      <div className="promotion__property">
        <div>Free Perk Description: </div>
        <div>{freeDescription}</div>
      </div>
      <div className="promotion__property">
        <div>Discount Text: </div>
        <div>{discountText}</div>
      </div>
      <div className="promotion__property">
        <div>Promo Codes Created:</div>
        <div>{codes ? codes.length : 'None'}</div>
      </div>
      <div className="promotion__property">
        <div>Perk Desktop Image Path:</div>
        <div>{bgDesktopImgPath}</div>
      </div>
      <div className="promotion__property">
        <div>Perk Mobile Image Path:</div>
        <div>{bgMobileImgPath}</div>
      </div>
      <div className="promotion__property">
        <div>Perk Logo Image Path:</div>
        <div>{logoImgPath}</div>
      </div>
      <div className="promotion__property">
        <div>Partner Url:</div>
        <div>{partnerUrl}</div>
      </div>
    </div>
  );
};

Perk.propTypes = {
  promotion: PropTypes.shape({
    trialLength: PropTypes.number,
    paymentHeader: PropTypes.string,
    promoCodesEnabled: PropTypes.bool,
    promoCodePrefix: PropTypes.string,
  }).isRequired,
};

Perk.defaultProps = {};

export default Perk;
