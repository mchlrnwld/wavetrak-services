import React from 'react';
import PropTypes from 'prop-types';
import SignupAndSuccess from './SignupAndSuccess';
import PartnerDetails from './PartnerDetails';

const DirectToPay = ({ promotion }) => {
  const {
    paymentHeader,
    plans,
    codes,
    buttonText,
    currency
  } = promotion;
  return (
    <div>
      <div className="promotion__property">
        <div>Partner Codes Uploaded: </div>
        <div>{codes ? codes.length : 'None'}</div>
      </div>
      <div className="promotion__property">
        <div>Targeted Plans: </div>
        <div>{plans}</div>
      </div>
      <div className="promotion__property">
        <div>Eligible Currency: </div>
        <div>{currency}</div>
      </div>
      <SignupAndSuccess promotion={promotion} />
      <div className="promotion__property">
        <div>Payment Header: </div>
        <div>{paymentHeader}</div>
      </div>
      <PartnerDetails promotion={promotion} />
      <div className="promotion__property">
        <div>Button Text: </div>
        <div>{buttonText}</div>
      </div>
    </div>
  );
};

DirectToPay.propTypes = {
  promotion: PropTypes.shape({
    plans: PropTypes.string,
    paymentHeader: PropTypes.string,
    buttonText: PropTypes.string,
  }).isRequired
};

DirectToPay.defaultProps = {

};

export default DirectToPay;
