import React from 'react';
import PropTypes from 'prop-types';

const SignupAndSuccess = ({ promotion }) => {
  const {
    signUpHeader,
    successHeader,
    successMessage,
    successUrl
  } = promotion;
  return (
    <div>
      <div className="promotion__property">
        <div>Sign Up Header: </div>
        <div>{signUpHeader}</div>
      </div>
      <div className="promotion__property">
        <div>Success Header: </div>
        <div>{successHeader}</div>
      </div>
      <div className="promotion__property">
        <div>Success Message: </div>
        <div>{successMessage}</div>
      </div>
      <div className="promotion__property">
        <div>Success Url: </div>
        <div>{successUrl}</div>
      </div>
    </div>
  );
};

SignupAndSuccess.propTypes = {
  promotion: PropTypes.shape({
    signUpHeader: PropTypes.string,
    successHeader: PropTypes.string,
    successMessage: PropTypes.string,
    successUrl: PropTypes.string,
  }).isRequired,
};

SignupAndSuccess.defaultProps = {};

export default SignupAndSuccess;
