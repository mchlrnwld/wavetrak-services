import React from 'react';
import PropTypes from 'prop-types';
import SignupAndSuccess from './SignupAndSuccess';

const RetentionOffer = ({ promotion }) => {
  const {
    paymentHeader,
    paymentMessage,
    buttonText,
    stripeCouponCode,
    eligibilityType,
    eligibilityTarget,
    eligiblePlan,
    eligibleStatus,
    offeredPlan,
  } = promotion;
  return (
    <div>
      <SignupAndSuccess promotion={promotion} />
      <div className="promotion__property">
        <div>Payment Header: </div>
        <div>{paymentHeader}</div>
      </div>
      <div className="promotion__property">
        <div>Payment Message: </div>
        <div>{paymentMessage}</div>
      </div>
      <div className="promotion__property">
        <div>Button Text: </div>
        <div>{buttonText}</div>
      </div>
      <div className="promotion__property">
        <div>Stripe Coupon Code: </div>
        <div>{stripeCouponCode}</div>
      </div>
      <div className="promotion__property">
        <div>Eligibility Type: </div>
        <div>{eligibilityType}</div>
      </div>
      <div className="promotion__property">
        <div>Eligibility Target: </div>
        <div>{eligibilityTarget}</div>
      </div>
      <div className="promotion__property">
        <div>Eligibile Plan: </div>
        <div>{eligiblePlan}</div>
      </div>
      <div className="promotion__property">
        <div>Eligibility Status: </div>
        <div>{eligibleStatus}</div>
      </div>
      <div className="promotion__property">
        <div>Offered Plan: </div>
        <div>{offeredPlan}</div>
      </div>
    </div>
  );
};

RetentionOffer.propTypes = {
  promotion: PropTypes.shape({
    paymentHeader: PropTypes.string,
    paymentMessage: PropTypes.string,
    buttonText: PropTypes.string,
    stripeCouponCode: PropTypes.string,
    eligibilityType: PropTypes.string,
    eligibilityTarget: PropTypes.string,
    eligiblePlan: PropTypes.string,
    eligibleStatus: PropTypes.string,
    offeredPlan: PropTypes.string,
  }).isRequired,
};

RetentionOffer.defaultProps = {};

export default RetentionOffer;
