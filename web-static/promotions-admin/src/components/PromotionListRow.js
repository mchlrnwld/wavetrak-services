import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { getBrandName, getMaxRedemptions } from '../common/utils';

const PromotionItem = ({ promotion }) => (
  <Link
    to={`/promotions/${promotion._id.toString()}`}
    className="promotions-list__container__row"
  >
    <div className="promotions-list__container__cell">
      {promotion._id.toString()}
    </div>
    <div className="promotions-list__container__cell promotions-list__container__cell--brand">
      {getBrandName(promotion.brand)}
    </div>
    <div className="promotions-list__container__cell">{promotion.name}</div>
    {promotion.kind === 'Perk' ? null : (
      <div className="promotions-list__container__cell">
        {`/${promotion.urlKey}`}
      </div>
    )}
    <div className="promotions-list__container__cell promotions-list__container__cell--status">
      {promotion.isActive ? 'ACTIVE' : 'INACTIVE'}
    </div>
    <div className="promotions-list__container__cell">
      {promotion.kind}
    </div>
    <div className="promotions-list__container__cell promotions-list__container__cell--status">
      {`${promotion.totalRedemption}${getMaxRedemptions(
        promotion.maxRedemption,
      )}`}
    </div>
  </Link>
);

PromotionItem.propTypes = {
  promotion: PropTypes.object.isRequired,
};

export default PromotionItem;
