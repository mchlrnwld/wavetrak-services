import React from 'react';
import PropTypes from 'prop-types';
import PromotionForm from '../PromotionForm';

const styles = {
  formWrapper: {
    width: 410,
    margin: '0 auto',
  }
};

const Promotion = ({
  edit,
  editing,
  errors,
  promotion,
  success,
  handleDiscountCodes,
  codes,
  hasDuplicateCode,
  handlePromoCodes,
  promoCodesEnabled,
  handlePromoType,
  promoType
}) => (
  <div style={styles.formWrapper}>
    <h4>
Edit Promotion
    </h4>
    <PromotionForm
      action={edit}
      promotion={promotion}
      errors={errors}
      processing={editing}
      success={success}
      handleDiscountCodes={handleDiscountCodes}
      codes={codes}
      hasDuplicateCode={hasDuplicateCode}
      handlePromoCodes={handlePromoCodes}
      promoCodesEnabled={promoCodesEnabled}
      handlePromoType={handlePromoType}
      promoType={promoType}
    />
  </div>
);

Promotion.propTypes = {
  edit: PropTypes.func.isRequired,
  editing: PropTypes.bool,
  errors: PropTypes.shape({}),
  promotion: PropTypes.object.isRequired,
  success: PropTypes.bool,
  codes: PropTypes.array,
  hasDuplicateCode: PropTypes.bool,
  handleDiscountCodes: PropTypes.func.isRequired,
  handlePromoCodes: PropTypes.func.isRequired,
  promoCodesEnabled: PropTypes.bool,
  handlePromoType: PropTypes.func.isRequired,
  promoType: PropTypes.string
};

Promotion.defaultProps = {
  errors: {},
  editing: true,
  success: false,
  codes: [],
  hasDuplicateCode: false,
  promoCodesEnabled: false,
  promoType: null
};

export default Promotion;
