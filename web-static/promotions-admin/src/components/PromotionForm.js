import React, { Component } from 'react';
import PropTypes from 'prop-types';
import set from 'lodash.set';
import { Button } from '@surfline/quiver-react';
import {
  BaseFields,
  ExtendedFreeTrialFields,
  DirectToPayFields,
  GiftRedeemFields,
  RetentionOfferFields,
  PerkFields,
} from './PromotionFormFields';

const styles = {
  field: {
    marginTop: '30px',
    width: '100%',
  },
  form: {
    width: '500px',
    margin: '0 auto',
  },
};

class PromotionForm extends Component {
  getPromotionFieldsFromType = (kind) => {
    const componentMap = {
      'Extended Free Trial': ExtendedFreeTrialFields,
      'Direct To Pay': DirectToPayFields,
      'Gift Redeem': GiftRedeemFields,
      'Retention Offer': RetentionOfferFields,
      Perk: PerkFields,
    };
    return componentMap[kind];
  };

  showCustomError = (errors) => {
    let errorMessage;
    if (errors && errors.maxRedemption) {
      errorMessage = errors.maxRedemption.message;
    } else if (errors && errors.discountCodes) {
      errorMessage = errors.discountCodes.message;
    }
    return errorMessage;
  };

  handleCancelClick = (event) => {
    event.preventDefault();
    const { promotion: { _id: id } } = this.props;
    if (!id) return window.location.assign('/promotions');
    return window.location.assign(`/promotions/${id}`);
  }

  render() {
    const {
      action,
      processing,
      promotion,
      errors,
      success,
      kind,
    } = this.props;
    const PromotionFeilds = this.getPromotionFieldsFromType(kind);
    return (
      <form
        style={styles.form}
        onSubmit={(event) => {
          event.preventDefault();
          action(
            [...new FormData(event.target)].reduce(
              (formFields, [field, value]) => {
                set(formFields, field, value);
                return formFields;
              },
              {},
            ),
          );
        }}
      >
        <BaseFields
          promotion={promotion}
          errors={errors}
          styles={styles}
          kind={kind}
        />
        <PromotionFeilds
          promotion={promotion}
          errors={errors}
          styles={styles}
        />
        <div className="promotions__form__buttons">
          <Button loading={processing} success={success} label="Submit">
            Save
          </Button>
          <Button
            label="Cancel"
            onClick={this.handleCancelClick}
          >
            Cancel
          </Button>
        </div>
      </form>
    );
  }
}

PromotionForm.propTypes = {
  action: PropTypes.func.isRequired,
  promotion: PropTypes.shape({}),
  errors: PropTypes.shape({}),
  processing: PropTypes.bool,
  success: PropTypes.bool,
  kind: PropTypes.string.isRequired,
};

PromotionForm.defaultProps = {
  promotion: {},
  errors: {},
  processing: false,
  success: false,
};

export default PromotionForm;
