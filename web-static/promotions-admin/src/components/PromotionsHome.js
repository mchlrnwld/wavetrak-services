import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Tab, Tabs, TabList, TabPanel
} from 'react-tabs';
import PropTypes from 'prop-types';
import FunnelPromotions from './FunnelPromotions';
import PerkPromotions from './PerkPromotions';
import PerksHero from './PerksHero';
import { fetchPromotions } from '../actions/promotions';
import 'react-tabs/style/react-tabs.scss';

class PromotionsHome extends Component {
  componentDidMount() {
    const { doFetchPromotions } = this.props;
    doFetchPromotions();
  }

  render() {
    const { promotions } = this.props;
    if (!promotions) return <div>Loading... Promotions list</div>;
    const funnelPromotions = promotions.filter(({ kind }) => kind !== 'Perk');
    const perkPromotions = promotions.filter(({ kind }) => kind === 'Perk');
    return (
      <div>
        <div className="promotions-list__header">
          <div className="promotions-list__header__title">Wavetrak Promotions</div>
          <div className="promotions-list__header__link">
            <Link className="quiver-button" to="/promotions/create">Create Promotion</Link>
          </div>
        </div>
        <div>
          <Tabs className="promotions-tabs">
            <TabList>
              <Tab selectedClassName="promotions-tab--selected">Promotions</Tab>
              <Tab selectedClassName="promotions-tab--selected">Perks</Tab>
              <Tab selectedClassName="promotions-tab--selected">Perks Hero</Tab>
            </TabList>
            <TabPanel>
              <FunnelPromotions promotions={funnelPromotions} />
            </TabPanel>
            <TabPanel>
              <PerkPromotions promotions={perkPromotions} />
            </TabPanel>
            <TabPanel>
              <PerksHero />
            </TabPanel>
          </Tabs>
        </div>
      </div>
    );
  }
}

PromotionsHome.propTypes = {
  promotions: PropTypes.array,
  doFetchPromotions: PropTypes.func.isRequired,
};

PromotionsHome.defaultProps = {
  promotions: null,
};

export default connect(
  state => ({
    promotions: state.promotions.promotions,
    loading: state.promotions.loading,
  }),
  dispatch => ({
    doFetchPromotions: () => dispatch(fetchPromotions()),
  }),
)(PromotionsHome);
