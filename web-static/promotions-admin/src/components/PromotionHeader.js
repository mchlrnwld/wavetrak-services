import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import {
  SurflineIcon,
  BuoyweatherIcon,
  FishtrackIcon,
} from './Icons';

const PromotionHeader = ({ doSetPromotionKind, match, promotion }) => {
  const getIconClassName = brand => classNames({
    promotion__header__brand: true,
    [`promotion__header__brand--${brand}`]: true,
  });

  const getIconFromBrand = (brand) => {
    const iconMap = {
      sl: SurflineIcon,
      bw: BuoyweatherIcon,
      fs: FishtrackIcon
    };
    return iconMap[brand];
  };

  const renderPromoCodesEnabled = (kind, id) => {
    if (kind === 'Direct To Pay' || kind === 'Extended Free Trial' || kind === 'Perk') {
      return (
        <div className="promotion__button--codes">
          <Link
            className="quiver-button"
            to={`/promotions/${id}/promo-codes`}
          >
            Manage PromoCodes
          </Link>
        </div>
      );
    }
    return null;
  };

  const {
    _id: id,
    name,
    kind,
    brand,
  } = promotion;
  const BrandIcon = getIconFromBrand(brand);
  return (
    <div className="promotion__header">
      <div className="promotion__header__name">{name}</div>
      <div className="promotion__header__details">
        <div className={getIconClassName(brand)}>
          <BrandIcon />
        </div>
        <div className="promotion__header__kind">{kind}</div>
      </div>
      <div className="promotion__button__container">
        <div className="promotion__button--edit">
          <Link
            className="quiver-button"
            onClick={() => doSetPromotionKind(kind)}
            to={`${match.url}/edit`}
          >
            Edit Promotion
          </Link>
        </div>
        {renderPromoCodesEnabled(kind, id)}
      </div>
    </div>
  );
};

PromotionHeader.propTypes = {
  doSetPromotionKind: PropTypes.func.isRequired,
  match: PropTypes.shape({
    url: PropTypes.string,
  }).isRequired,
  promotion: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    kind: PropTypes.string,
    brand: PropTypes.string,
  }).isRequired,
};

export default PromotionHeader;
