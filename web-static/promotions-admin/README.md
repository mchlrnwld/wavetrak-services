# Surfline Admin Promotions Manager

Unlike the other cms modules, the `promotions` module is a a client side html5 React application. This app is deployed to an s3 bucket `sl-admin-tools`.

Node version: `12`

To run the application locally:

```
npm install
npm start
```

Build:
Uses Webpack 4
```
APP_ENV=<environment> npm run build  // sandbox, staging, production
```

Test:
Uses Karma (test runner), Mocha (testing framework), Chai (assertion library) and Sinon (testing plugin)

```
npm run test
```
