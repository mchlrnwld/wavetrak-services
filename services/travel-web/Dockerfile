FROM node:12 as base
WORKDIR /opt/app
COPY .npmrc package.json package-lock.json ./
RUN npm ci --prefer-offline
RUN touch .env


FROM monsonjeremy/node-chrome:12 as test
WORKDIR /opt/app
COPY --from=base /opt/app/node_modules /opt/app/node_modules
COPY . .
RUN npm run lint && npm run test


FROM test AS build
WORKDIR /opt/app
ARG APP_ENV=sandbox
ARG APP_VERSION=master
ENV APP_ENV=$APP_ENV
ENV NODE_ENV=production
ENV APP_VERSION=$APP_VERSION
RUN npm run build && \
    npm install --production --ignore-scripts --prefer-offline


FROM node:12-alpine
WORKDIR /opt/app
COPY --from=build /opt/app/build build
COPY --from=build /opt/app/node_modules /opt/app/node_modules

# execute npm start equivalent. Prevent extra process calls
# https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md#cmd
CMD ["node", "build/server/main.js"]
