import sh from 'shelljs';
import webpackCompiler from './webpackCompiler';
import config from '../config/webpack';

const clean = () => {
  sh.rm('-rf', './build');
};

export default () => {
  const conf = config({}, 'prod');

  const compileApp = () => webpackCompiler(conf.clientConfig, (clientStats) => { // build client
    if (clientStats.hasErrors()) return;
    sh.cp('-r', './src/public/*', './build/public/');
    webpackCompiler(conf.serverConfig, (serverStats) => { // build server
      sh.cp('./package.json', './build/server');
      sh.cp('./config/newrelic.js', './build/server/');
      if (serverStats.hasErrors()) return;
    }).run(() => undefined);
  });

  clean();
  compileApp().run(() => undefined);
};
