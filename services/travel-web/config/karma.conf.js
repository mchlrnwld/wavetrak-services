const puppeteer = require('puppeteer');
const testWebpackConfig = require('./webpack/tests.config.js')({});

process.env.CHROME_BIN = puppeteer.executablePath();

module.exports = (config) => {
  config.set({
    basePath: '',
    client: {
      mocha: {
        timeout: 60000, // 6 seconds - upped from 2 seconds
      },
    },
    frameworks: ['mocha'],
    files: ['./bundleTests.js'],
    exclude: [],
    customLaunchers: {
      ChromeCustom: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox'],
      },
    },
    preprocessors: {
      './bundleTests.js': ['webpack', 'sourcemap', 'sourcemap-writer'],
    },
    webpack: testWebpackConfig,
    reporters: ['mocha', 'coverage'],
    coverageReporter: {
      dir: '../coverage',
      reporters: [
        { type: 'text-summary' },
        { type: 'json' },
        { type: 'html' },
        { type: 'lcovonly' },
      ],
    },
    webpackMiddleware: {
      stats: 'errors-only',
    },
    plugins: [
      'karma-chrome-launcher',
      'karma-webpack',
      'karma-mocha',
      'karma-mocha-reporter',
      'karma-sourcemap-loader',
      'karma-sourcemap-writer',
      'karma-coverage',
    ],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['ChromeCustom'],
    singleRun: process.env.NODE_ENV !== 'development',
    concurrency: Infinity,
    browserNoActivityTimeout: 30000,
  });
};
