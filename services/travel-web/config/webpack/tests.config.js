/** @prettier */

const webpack = require('webpack');
const path = require('path');

module.exports = () => {
  const config = {
    mode: 'development',
    devtool: 'inline-cheap-module-source-map',
    module: {
      rules: [
        {
          test: /\.(jpg|jpeg|png|gif|eot|svg|ttf|woff|woff2|mp4)$/,
          use: {
            loader: 'file-loader',
            options: {
              outputPath: '/',
            },
          },
        },
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                '@babel/preset-react',
                [
                  '@babel/preset-env',
                  {
                    corejs: 3,
                    // Only includes polyfills based on usage
                    useBuiltIns: 'entry',
                    // Webpack 4 and Babel 7 has changed the way modules are
                    // compiled which breaks sinon using CJS modules allows sinon to work properly
                    modules: 'cjs',
                  },
                ],
              ],
              plugins: ['istanbul', 'react-hot-loader/babel'],
            },
          },
        },
        {
          test: /sinon\.js$/,
          use: 'imports-loader?define=>false,require=>false',
        },
      ],
    },
    resolve: {
      extensions: ['.js', '.jsx'],
      alias: {
        sinon: 'sinon/pkg/sinon',
        react: require.resolve('react'),
      },
    },
    externals: [
      'react/addons',
      'react/lib/ExecutionEnvironment',
      'react/lib/ReactContext',
      'react-hot-loader/root',
    ],
    plugins: [
      new webpack.DefinePlugin({
        'process.env.APP_ENV': JSON.stringify('development'),
        'process.env.NODE_ENV': JSON.stringify('test'),
      }),
    ],
  };

  return config;
};
