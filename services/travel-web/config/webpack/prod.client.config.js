import { ReactLoadablePlugin } from 'react-loadable/webpack';
import path from 'path';
import webpack from 'webpack';
import autoprefixer from 'autoprefixer';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import AssetsPlugin from 'assets-webpack-plugin';
import paths from '../paths';
import config from '../../src/config/';

module.exports = () => {
  const { clientSrcPath, assetsBuildPath, buildPath } = paths;

  return {
    target: 'web',
    mode: 'production',
    devtool: 'source-map',

    entry: {
      main: [path.resolve(clientSrcPath, 'index.js')],
    },

    externals: {
      react: 'vendor.React',
      'react-dom': 'vendor.ReactDOM',
    },

    output: {
      path: assetsBuildPath,
      filename: '[name]-[chunkhash].js',
      chunkFilename: '[name]-[chunkhash].js',
      publicPath: config.cdn,
    },

    optimization: {
      splitChunks: {
        name: 'manifest',
        minChunks: Infinity,
      },
    },

    module: {
      rules: [
        {
          test: /\.(jpg|jpeg|png|gif|eot|svg|ttf|woff|woff2|mp4)$/,
          loader: 'file-loader',
        },
        {
          test: /\.css$/,
          use: [
            MiniCssExtractPlugin.loader,
            { loader: 'css-loader', options: { importLoaders: 3, sourceMap: true } },
            { loader: 'resolve-url-loader' },
          ],
        },
        {
          test: /\.scss$/,
          use: [
            MiniCssExtractPlugin.loader,
            { loader: 'css-loader', options: { importLoaders: 3, sourceMap: true } },
            {
              loader: 'postcss-loader',
              options: {
                postcssOptions: {
                  plugins: [autoprefixer],
                },
              },
            },
            { loader: 'resolve-url-loader' },
            { loader: 'sass-loader', options: { sourceMap: true } },
          ],
        },
      ],
    },

    plugins: [
      new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: true,
        options: {
          context: process.cwd(),
          output: { path: buildPath },
        },
      }),

      new MiniCssExtractPlugin({
        filename: '[name]-[contenthash].css',
        chunkFilename: '[id]-[contenthash].css',
      }),

      new AssetsPlugin({
        filename: 'clientAssets.json',
        path: buildPath,
      }),

      new ReactLoadablePlugin({
        filename: path.resolve(buildPath, 'react-loadable.json'),
      }),
    ],
  };
};
