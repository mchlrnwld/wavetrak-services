import TravelContainer from '../containers/routes/TravelContainer';
import routesConfig from '../routes/config';

const setupRoutes = async () => [
  {
    component: TravelContainer,
    routes: await Promise.all(
      routesConfig.map(async ({ routeProps, loader }) => ({
        ...routeProps,
        component: (await loader()).default,
      })),
    ),
  },
];

export default setupRoutes;
