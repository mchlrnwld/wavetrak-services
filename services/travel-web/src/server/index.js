/* eslint-disable no-console */
import newrelic from 'newrelic';
import express from 'express';
import compression from 'compression';
import cookieParser from 'cookie-parser';
import Loadable from 'react-loadable';
import { getBundles } from 'react-loadable/webpack';
import { matchRoutes } from 'react-router-config';
import {
  hydrate,
  tokenHandler,
  renderFetch,
  doExternalFetch,
  getClientIp,
} from '@surfline/web-common';
import path from 'path';
import React from 'react';
import Helmet from 'react-helmet';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { CookiesProvider } from 'react-cookie';
import cookiesMiddleware from 'universal-cookie-express';
import { StaticRouter } from 'react-router';
import { createMemoryHistory } from 'history';
import setupRoutes from './setupRoutes';
import { getStatusCode } from '../selectors/status';
import template from './template';
import Routes from '../routes/Routes';
import config from '../config';
import logger from '../common/logger';

import createStore from '../stores';

const clientAssets = require(ASSETS_MANIFEST); // eslint-disable-line import/no-dynamic-require, no-undef, max-len
const app = express();

const serverPort = process.env.SERVER_PORT ? parseInt(process.env.SERVER_PORT, 10) : 8080;
const log = logger('web-travel');

function getBackplaneOptions(pathname, query) {
  let renderOpts = {};
  const isMobile = query.native === 'true';
  if (pathname.indexOf('/surf-news/local') > -1) {
    renderOpts = { 'renderOptions[bannerAd]': 'forecast_article' };
  } else {
    renderOpts = { 'renderOptions[bannerAd]': false };
  }
  renderOpts = { ...renderOpts, 'renderOptions[showAdBlock]': true };
  if (isMobile) renderOpts = { ...renderOpts, 'renderOptions[hideNav]': true };
  return renderOpts;
}

/**
 * SA-3062: reset anonymous IDs
 * @param {import('express').Request} req
 */
const resetAnonymousIdCookie = (req) => {
  const hasExistingResetCookie = req.cookies.sl_reset_anonymous_id_v2;
  if (!hasExistingResetCookie) {
    // Remove the cookie from the req object so it does not get sent to backplane
    delete req.cookies.ajs_anonymous_id;
  }
  return !hasExistingResetCookie;
};

const server = (loadableManifest, routes) => {
  app.disable('x-powered-by');
  app.use(compression());
  app.use(cookieParser());
  app.use(cookiesMiddleware());

  app.get('/health', (_, res) =>
    res.send({
      status: 200,
      message: 'OK',
      version: process.env.APP_VERSION || 'unknown',
    }),
  );

  app.use('/travel/static', express.static(path.join(process.cwd(), './build/public')));

  app.use(tokenHandler(`${config.servicesURL}/trusted`, log));

  app.use('/travel/oembed', async (req, res) => {
    try {
      const { url } = req.query;
      const fbAccessToken = process.env.FB_ACCESS_TOKEN;
      let embedUrl = url;
      if (fbAccessToken && url.indexOf('insta') > -1) {
        embedUrl = `${url}&access_token=${fbAccessToken}`;
      }
      const response = await doExternalFetch(embedUrl);
      return res.json(response);
    } catch (err) {
      let errorMessage = 'Error fetching video. Please try again.';
      if (err.statusCode === 404) errorMessage = 'Error: Video not found';
      return res.status(err.statusCode).send(errorMessage);
    }
  });

  app.use('/travel/surfnetwork', async (_, res) => {
    try {
      const encodedCustomer = encodeURIComponent(process.env.SURFNETWORK_CUSTOMER);
      const requestUrl = `${config.surfNetworkBaseUrl}internal/authentication/access_token/generate.json`;
      const requestBody = `customer=${encodedCustomer}&site=surf`;
      const requestHeaders = {
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
        'X-API_KEY': process.env.SURFNETWORK_API_KEY,
      };
      const options = {
        method: 'POST',
        body: requestBody,
        headers: requestHeaders,
      };
      // eslint-disable-next-line camelcase
      const { access_token } = await doExternalFetch(requestUrl, options);
      return res.json(access_token);
    } catch (err) {
      let errorMessage = 'Error fetching video. Please try again.';
      if (err.statusCode === 404) errorMessage = 'Error: Video not found';
      return res.status(err.statusCode).send(errorMessage);
    }
  });

  const wrapErrors = (fn) => (...args) => fn(...args).catch(args[2]);

  // eslint-disable-next-line
  const errorHandlerMiddleware = (log) => async (err, req, res, next) => {
    newrelic.noticeError(err);
    console.log(err);
    return res.status(500).send('Error encountered');
  };

  app.use(
    wrapErrors(async (req, res) => {
      // SA-3062: reset anonymous IDs
      const shouldResetAnonymousIdCookie = resetAnonymousIdCookie(req);

      try {
        const backplaneOpts = getBackplaneOptions(req.path, req.query);
        const opts = {
          ...(req.userId ? { userId: req.userId } : null),
          ...(req.cookies.ajs_anonymous_id
            ? {
                anonymousId: JSON.parse(req.cookies.ajs_anonymous_id),
              }
            : null),
          bundle: 'margins',
          'renderOptions[mvpForced]': req.headers['x-surfline-mvp-forced'],
          vendor: 'v2',
          accessToken: req.cookies.access_token ?? null,
          ...backplaneOpts,
        };

        const backplane = await renderFetch(process.env.BACKPLANE_HOST, getClientIp(req), opts);

        const history = createMemoryHistory({
          initialEntries: [req.url],
        });
        const store = createStore(backplane.data.redux, history);

        // Iterate all routes matching the URL and get the components and params to send to hydrate
        const matchedRoutes = matchRoutes(routes, req.path);
        const params = Object.assign({}, ...matchedRoutes.map(({ match }) => match.params));
        const components = matchedRoutes.map(({ route }) => route.component);

        await hydrate(store, { components, params }, req, backplane.data.api);
        const initialState = store.getState();

        const context = {};
        const modules = [];
        const root = renderToString(
          <CookiesProvider cookies={req.universalCookies}>
            <Provider store={store}>
              <StaticRouter location={req.url} context={context}>
                <Loadable.Capture report={(moduleName) => modules.push(moduleName)}>
                  <Routes />
                </Loadable.Capture>
              </StaticRouter>
            </Provider>
          </CookiesProvider>,
        );

        const loadableBundles = getBundles(loadableManifest, modules);

        // Handle the 404 route
        if (context.status === 404) {
          return res.status(404).send('Not Found');
        }
        // Handle redirect status
        if (context.url) {
          return res.redirect(302, `${context.url}`);
        }

        const head = Helmet.rewind();

        return res.status(getStatusCode(initialState)).send(
          template({
            root,
            cssBundle: clientAssets.main.css,
            jsBundle: clientAssets.main.js,
            manifest: clientAssets.manifest.js,
            initialState: JSON.stringify(initialState),
            backplane,
            head,
            config,
            loadableBundles,
            shouldResetAnonymousIdCookie,
          }),
        );
      } catch (error) {
        console.log(error);
        newrelic.noticeError(error);
        return res.status(500).send('Something went wrong');
      }
    }),
  );

  app.use((err, req, res) => {
    if (err) {
      console.log(err);
      newrelic.noticeError(err);
    }
    return res.status(500).send('Something went wrong');
  });
  return app;
};

/* eslint-disable import/no-unresolved */
Promise.all([import('../../build/react-loadable'), setupRoutes(), Loadable.preloadAll()])
  .then(([loadableManifest, routes]) => {
    server(loadableManifest, routes).listen(serverPort, () => {
      console.log(`Express Server listening on ${serverPort}`);
    });
  })
  .catch((err) => {
    console.error(err);
    console.log('Exiting...');
    process.exit(-1);
  });
