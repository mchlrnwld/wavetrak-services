import PropTypes from 'prop-types';

export default PropTypes.shape({
  _id: PropTypes.string,
  legacyRegionId: PropTypes.number,
  name: PropTypes.string,
  cameras: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      title: PropTypes.string,
      streamUrl: PropTypes.string,
      stillUrl: PropTypes.string,
      rewindBaseUrl: PropTypes.string,
      status: PropTypes.shape({
        isDown: PropTypes.bool,
        message: PropTypes.string,
      }),
    }),
  ),
});
