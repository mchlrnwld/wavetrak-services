import PropTypes from 'prop-types';
import children from './children';
import taxonomy from './taxonomy';

export default PropTypes.shape({
  loading: PropTypes.bool,
  children,
  url: PropTypes.string,
  taxonomy,
  conditions: PropTypes.shape({
    human: PropTypes.bool,
    value: PropTypes.string,
  }),
  boundingBox: PropTypes.shape({
    north: PropTypes.number,
    south: PropTypes.number,
    east: PropTypes.number,
    west: PropTypes.number,
  }),
  wind: PropTypes.shape({
    speed: PropTypes.number,
    direction: PropTypes.number,
  }),
  waveHeight: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number,
    plus: PropTypes.bool,
  }),
  tide: PropTypes.shape({
    current: PropTypes.shape({
      type: PropTypes.string,
      height: PropTypes.number,
      timestamp: PropTypes.number,
      utcOffset: PropTypes.number,
    }),
    next: PropTypes.shape({
      type: PropTypes.string,
      height: PropTypes.number,
      timestamp: PropTypes.number,
      utcOffset: PropTypes.number,
    }),
  }),
  thumbnail: PropTypes.string,
  rank: PropTypes.arrayOf({
    0: PropTypes.number,
    1: PropTypes.number,
  }),
});
