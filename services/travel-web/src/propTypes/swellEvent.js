import PropTypes from 'prop-types';
import articlePropType from './article';

export default PropTypes.shape({
  id: PropTypes.string,
  permalink: PropTypes.string,
  name: PropTypes.string,
  summary: PropTypes.string,
  thumbnailUrl: PropTypes.string,
  dfpKeyword: PropTypes.string,
  jwPlayerVideoId: PropTypes.string,
  updates: PropTypes.arrayOf(articlePropType),
  active: PropTypes.bool,
});
