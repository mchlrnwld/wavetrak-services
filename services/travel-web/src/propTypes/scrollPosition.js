import PropTypes from 'prop-types';

export default PropTypes.shape({
  top: PropTypes.number,
  left: PropTypes.number,
});
