import PropTypes from 'prop-types';
import articlePropType from './article';
import unitsPropType from './units';

export default PropTypes.shape({
  activeArticle: {
    loading: PropTypes.bool,
    success: PropTypes.bool,
    article: articlePropType,
  },
  success: PropTypes.bool,
  overview: PropTypes.shape({
    name: PropTypes.string,
  }),
  associated: PropTypes.shape({
    utcOffset: PropTypes.number,
    units: unitsPropType,
  }),
});
