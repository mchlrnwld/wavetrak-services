import PropTypes from 'prop-types';

export default PropTypes.arrayOf(
  PropTypes.shape({
    id: PropTypes.string,
    createdAt: PropTypes.number,
    updatedAt: PropTypes.number,
    content: PropTypes.shape({
      title: PropTypes.string,
    }),
    media: PropTypes.shape({
      feed1x: PropTypes.string,
      feed2x: PropTypes.string,
      type: PropTypes.string,
    }),
    permalink: PropTypes.string,
    tags: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        url: PropTypes.string,
      }),
    ),
  }),
);
