import PropTypes from 'prop-types';

export default PropTypes.shape({
  'og:type': PropTypes.string,
  'og:site_name': PropTypes.string,
  'og:title': PropTypes.string,
  'og:image': PropTypes.string,
  'og:description': PropTypes.string,
  'og:url': PropTypes.string,
  'og:updated_time': PropTypes.string,
  canonical: PropTypes.string,
  'article:section': PropTypes.string,
  'article:published_time': PropTypes.string,
  'article:updated_time': PropTypes.string,
  'twitter:card': PropTypes.string,
  'twitter:title': PropTypes.string,
  'twitter:image': PropTypes.string,
  'twitter:description': PropTypes.string,
  'twitter:url': PropTypes.string,
});
