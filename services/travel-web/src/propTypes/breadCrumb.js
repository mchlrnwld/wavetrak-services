import PropTypes from 'prop-types';

export default PropTypes.shape({
  name: PropTypes.String,
  url: PropTypes.String,
  geonameId: PropTypes.Integer,
  id: PropTypes.String,
});
