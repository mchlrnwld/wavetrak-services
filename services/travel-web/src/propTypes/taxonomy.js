import PropTypes from 'prop-types';

export default PropTypes.shape({
  name: PropTypes.string,
  associated: PropTypes.shape({
    links: PropTypes.arrayOf(
      PropTypes.shape({
        key: PropTypes.string,
        href: PropTypes.string,
      }),
    ),
  }),
});
