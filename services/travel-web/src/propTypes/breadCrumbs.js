import PropTypes from 'prop-types';
import breadCrumb from './breadCrumb';

export default PropTypes.arrayOf(breadCrumb);
