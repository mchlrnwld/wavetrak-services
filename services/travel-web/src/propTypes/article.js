import PropTypes from 'prop-types';

export default PropTypes.shape({
  id: PropTypes.number.isRequired,
  contentType: PropTypes.string,
  createdAt: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.string]),
  updatedAt: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.string]),
  promoted: PropTypes.arrayOf(PropTypes.string),
  premium: PropTypes.shape({
    premium: PropTypes.boolean,
    paywallHeading: PropTypes.string,
    paywallDescription: PropTypes.string,
    teaser: PropTypes.string,
    paywallPosts: [PropTypes.shape({})],
  }),
  content: PropTypes.shape({
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string,
    body: PropTypes.string,
  }),
  permalink: PropTypes.string,
  media: PropTypes.shape({
    type: PropTypes.string.isRequired,
    feed1x: PropTypes.string.isRequired,
    feed2x: PropTypes.string.isRequired,
    promobox1x: PropTypes.string,
    promobox2x: PropTypes.string,
  }),
  author: PropTypes.shape({
    iconUrl: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }),
  tags: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired,
    }),
  ),
});
