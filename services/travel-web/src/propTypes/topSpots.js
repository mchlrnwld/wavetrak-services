import PropTypes from 'prop-types';
import spot from './spot';

export default PropTypes.arrayOf(spot);
