import PropTypes from 'prop-types';

export default PropTypes.arrayOf(
  PropTypes.shape({
    name: PropTypes.string,
    media: PropTypes.shape({
      medium: PropTypes.string,
    }),
    url: PropTypes.string,
  }),
);
