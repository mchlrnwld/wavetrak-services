import PropTypes from 'prop-types';
import relatedArticles from './relatedArticles';

export default PropTypes.shape({
  id: PropTypes.string,
  contentType: PropTypes.string,
  createdAt: PropTypes.number,
  updatedAt: PropTypes.number,
  content: PropTypes.shape({
    title: PropTypes.string,
    subtitle: PropTypes.string,
    summary: PropTypes.string,
    body: PropTypes.string,
  }),
  media: PropTypes.shape({
    large: PropTypes.string,
    medium: PropTypes.string,
    type: PropTypes.string,
    thumbnail: PropTypes.string,
    full: PropTypes.string,
  }),
  relatedPosts: relatedArticles,
});
