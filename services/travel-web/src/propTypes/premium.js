import PropTypes from 'prop-types';
/* eslint-disable import/no-cycle */
import { mediaShape, taxonomyShape } from './editorialArticle';

export default PropTypes.shape({
  premium: PropTypes.bool.isRequired,
  paywallHeading: PropTypes.string,
  paywallDescription: PropTypes.string,
  teaser: PropTypes.string,
  paywallPosts: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      subtitle: PropTypes.string,
      permalink: PropTypes.string,
      createdAt: PropTypes.string,
      categories: taxonomyShape,
      media: mediaShape,
      series: taxonomyShape,
    }),
  ),
});
