import PropTypes from 'prop-types';

export default PropTypes.shape({
  tideHeight: PropTypes.string,
  waveHeight: PropTypes.string,
  windSpeed: PropTypes.string,
});
