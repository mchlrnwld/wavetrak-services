import PropTypes from 'prop-types';
/* eslint-disable import/no-cycle */
import premiumPropType from './premium';

export const mediaShape = PropTypes.shape({
  type: PropTypes.string.isRequired,
  feed1x: PropTypes.string.isRequired,
  feed2x: PropTypes.string.isRequired,
});

export const taxonomyShape = PropTypes.shape({
  name: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  taxonomy: PropTypes.string.isRequired,
});

export default PropTypes.shape({
  author: PropTypes.shape({
    iconUrl: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }),
  categories: taxonomyShape,
  content: PropTypes.shape({
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string,
    displayTitle: PropTypes.string,
    body: PropTypes.string,
  }),
  createdAt: PropTypes.number.isRequired,
  displayOptions: PropTypes.shape({
    hideComments: PropTypes.bool.isRequired,
    hideAds: PropTypes.bool.isRequired,
  }),
  externalLink: PropTypes.shape({
    externalSournce: PropTypes.string,
    newWindow: PropTypes.bool,
    externalUrl: PropTypes.string,
  }),
  hero: PropTypes.shape({
    showHero: PropTypes.bool,
    type: PropTypes.bool,
    img: PropTypes.string,
    location: PropTypes.string,
    align: PropTypes.shape({
      horizontal: PropTypes.string,
      vertical: PropTypes.string,
    }),
    credit: PropTypes.string,
    gradient: PropTypes.bool,
  }),
  id: PropTypes.string.isRequired,
  media: mediaShape,
  permalink: PropTypes.string,
  promotions: taxonomyShape,
  premium: premiumPropType,
  relatedPosts: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      subtitle: PropTypes.string,
      permalink: PropTypes.string,
      createdAt: PropTypes.string,
      categories: taxonomyShape,
      media: mediaShape,
      series: taxonomyShape,
    }),
  ),
  series: taxonomyShape,
  sponsoredArticle: PropTypes.shape({
    sponsoredArticle: PropTypes.bool.isRequired,
    attributionText: PropTypes.string,
    sponsorName: PropTypes.string,
    dfpKeyword: PropTypes.string,
    partnerContent: PropTypes.bool,
    showAttributionInFeed: PropTypes.bool,
  }),
  status: PropTypes.string.isRequired,
  tags: taxonomyShape,
  updatedAt: PropTypes.number.isRequired,
  yoastMeta: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    yoast_wpseo_title: PropTypes.string,
    yoast_wpseo_metadesc: PropTypes.string,
    yoast_wpseo_canonical: PropTypes.string,
    'yoast_wpseo_opengraph-title': PropTypes.string,
    'yoast_wpseo_opengraph-description': PropTypes.string,
    'yoast_wpseo_opengraph-image': PropTypes.string,
    'yoast_wpseo_twitter-title': PropTypes.string,
    'yoast_wpseo_twitter-description': PropTypes.string,
    'yoast_wpseo_twitter-image': PropTypes.string,
    'yoast_wpseo_meta-robots-noindex': PropTypes.string,
    'yoast_wpseo_meta-robots-nofollow': PropTypes.string,
  }),
});
