import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import * as selectors from './status';
import stateFixture from './fixtures/state.json';

describe('selectors / status', () => {
  const state = deepFreeze(stateFixture);

  it('gets a status code', () => {
    const code = selectors.getStatusCode(state);
    expect(code).to.equal(200);
  });
});
