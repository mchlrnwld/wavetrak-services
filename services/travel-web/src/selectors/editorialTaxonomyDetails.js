export const getTaxonomyDetails = (state) => {
  const data = state.editorialTaxonomyDetails.details;
  if (data) {
    return data;
  }
  return null;
};
