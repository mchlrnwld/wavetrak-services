export const getTaxonomyPosts = (state) => {
  const data = state.editorialTaxonomyPosts;
  if (data) {
    return data;
  }
  return null;
};
