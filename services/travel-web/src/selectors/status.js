export const getStatusCode = (state) => state.status.code;

export const getRedirectDestination = (state) => state.status.dest;
