export const getLocationView = (state) => state.locationView;

export const getUnits = (state) => {
  const data = state.locationView.associated;
  if (data) {
    return data.units;
  }
  return null;
};
