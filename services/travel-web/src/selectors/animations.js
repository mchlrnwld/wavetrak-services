const getScrollPosition = (component) => (state) => state.animations.scrollPositions[component];

export const getTopSpotsScrollPosition = getScrollPosition('topSpots');

export default (state) => state.animations;
