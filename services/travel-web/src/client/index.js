import React from 'react';
import { ConnectedRouter } from 'connected-react-router';
import Loadable from 'react-loadable';
import { hydrate, unmountComponentAtNode } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { CookiesProvider } from 'react-cookie';
import { Provider } from 'react-redux';
import '@surfline/quiver-themes/stylesheets/normalize.css';
import history from '../utils/history';

import configureStore from '../stores';
import rootReducer from '../reducers';
import '../styles/base.scss';

const root = document.querySelector('#root');
const preloadedState = window.__DATA__; // eslint-disable-line no-underscore-dangle

// Create a history of your choosing (we're using a browser history in this case)
const store = configureStore(preloadedState, history);

window.wavetrakStore = store;

const mount = () => {
  // eslint-disable-next-line global-require,import/newline-after-import
  const Routes = require('../routes/Routes').default;

  Loadable.preloadReady().then(() => {
    hydrate(
      <CookiesProvider>
        <AppContainer>
          <Provider store={store}>
            <ConnectedRouter history={history}>
              <Routes />
            </ConnectedRouter>
          </Provider>
        </AppContainer>
      </CookiesProvider>,
      root,
    );
  });
};

mount();

if (module.hot) {
  module.hot.accept('../routes/Routes', () => {
    unmountComponentAtNode(root);
    mount();
  });
  module.hot.accept('../reducers', () => {
    store.replaceReducer(rootReducer(history));
  });
}
