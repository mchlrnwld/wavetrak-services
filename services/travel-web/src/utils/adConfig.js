import { getUserType } from '@surfline/web-common';

const editorialHeroAdSizesPremium = {
  adUnit: '/1024858/Horizontal_Variable',
  adId: 'Horizontal_Variable',
  adSizes: [
    [1170, 250],
    [750, 250],
    [320, 80],
  ],
  adSizeMappings: [
    [[1200, 0], [[1170, 250]]],
    [[750, 0], [[750, 250]]],
    [[320, 0], [[320, 80]]],
    [[0, 0], [[320, 80]]],
  ],
};

const editorialHeroAdSizes = {
  adUnit: '/1024858/Horizontal_Variable',
  adId: 'Horizontal_Variable',
  adSizes: [
    'fluid',
    [1170, 250],
    [1170, 160],
    [990, 90],
    [970, 250],
    [320, 50],
    [970, 90],
    [970, 160],
    [320, 80],
    [300, 250],
    [750, 250],
    [750, 160],
    [728, 90],
  ],
  adSizeMappings: [
    [
      [1200, 0],
      ['fluid', [1170, 250], [1170, 160], [990, 90], [970, 250], [970, 160], [970, 90], [728, 90]],
    ],
    [
      [992, 0],
      ['fluid', [990, 90], [970, 250], [970, 160], [970, 90], [750, 250], [728, 90]],
    ],
    [
      [768, 0],
      ['fluid', [750, 250], [750, 160], [728, 90]],
    ],
    [
      [320, 0],
      ['fluid', [320, 80], [320, 50]],
    ],
    [
      [0, 0],
      ['fluid', [320, 50]],
    ],
  ],
};

const editorialTaxonomySizes = {
  //  300x250, 728x90, 970x90, 970x250
  adSizes: [
    [970, 90],
    [728, 90],
    [300, 250],
  ],
  adSizeMappings: [
    [
      [1200, 0],
      [
        [970, 90],
        [970, 250],
        [728, 90],
      ],
    ],
    [
      [992, 0],
      [
        [970, 90],
        [970, 250],
        [728, 90],
      ],
    ],
    [[768, 0], [[728, 90]]],
    [[320, 0], [[300, 250]]],
    [
      [0, 0],
      [
        [970, 90],
        [970, 250],
        [728, 90],
      ],
    ],
  ],
};

const adConfigMap = {
  sponsoredArticleLogo: {
    adUnit: '/1024858/Logo',
    adId: 'div-gpt-ad-article-unit-2',
    adSizes: [[100, 40]],
    adSizeDesktop: [100, 40],
    adSizeMobile: [100, 40],
    adViewType: 'CONTENT_ARTICLE',
  },
  stickyRightRail: {
    adUnit: '/1024858/SL_Forecast_ExpertContent_RightRail',
    adId: 'SL_Forecast_ExpertContent_RightRail',
    adSizes: [
      [300, 250],
      [300, 600],
    ],
    adSizeMappings: [
      [
        [976, 250],
        [300, 600],
      ],
      [
        [300, 250],
        [300, 250],
      ],
    ],
    adClass: 'sl-feed-ad__ad-item',
    adViewType: 'FORECAST',
  },
  editorialTaxonomyMidPage: {
    adUnit: '/1024858/Horizontal_Variable_MidPage',
    adId: 'Horizontal_Variable_MidPage',
    adClass: 'sl-feed-ad__mid-page',
    ...editorialTaxonomySizes,
  },
  editorialTaxonomyBelowNav: {
    adUnit: '/1024858/Horizontal_Variable_BelowNav',
    adId: 'Horizontal_Variable_BelowNav',
    adClass: 'sl-feed-ad__below-nav',
    adSizes: [
      [970, 90],
      [970, 250],
      [728, 90],
      [320, 50],
    ],
    adSizeMappings: [
      [
        [1200, 0],
        [
          [970, 90],
          [970, 250],
          [728, 90],
        ],
      ],
      [
        [992, 0],
        [
          [970, 90],
          [970, 250],
          [728, 90],
        ],
      ],
      [[768, 0], [[728, 90]]],
      [[320, 0], [[320, 50]]],
      [
        [0, 0],
        [
          [970, 90],
          [970, 250],
          [728, 90],
        ],
      ],
    ],
  },
  editorial_article_superheader_premium: {
    adViewType: 'CONTENT_ARTICLE',
    ...editorialHeroAdSizesPremium,
  },
  editorial_article_superheader: {
    adViewType: 'CONTENT_ARTICLE',
    ...editorialHeroAdSizes,
  },
  editorial_home: {
    adViewType: 'CONTENT_HOME',
    ...editorialHeroAdSizes,
  },
  editorial_category: {
    adViewType: 'CONTENT_CATEGORY',
    ...editorialHeroAdSizes,
  },
  editorial_tag: {
    adViewType: 'CONTENT_TAG',
    ...editorialHeroAdSizes,
  },
  editorial_series: {
    adViewType: 'CONTENT_SERIES',
    ...editorialHeroAdSizes,
  },
  editBody1: {
    adUnit: '/1024858/SL_Edit_Article_InCopy1',
    adId: 'SL_Edit_Article_InCopy1',
    adSizes: [
      [970, 90],
      [970, 250],
      [728, 90],
      [300, 250],
      [320, 50],
    ],
    adSizeMappings: [
      [
        [976, 250],
        [
          [970, 90],
          [970, 250],
          [728, 90],
        ],
      ],
      [
        [300, 250],
        [
          [300, 250],
          [320, 50],
        ],
      ],
    ],
    adClass: 'sl-feed-ad__ad-body-1',
    adViewType: 'CONTENT_ARTICLE',
  },
  editBody2: {
    adUnit: '/1024858/SL_Edit_Article_InCopy2',
    adId: 'SL_Edit_Article_InCopy2',
    adSizes: [
      [970, 90],
      [970, 250],
      [728, 90],
      [300, 250],
    ],
    adSizeMappings: [
      [
        [976, 250],
        [
          [970, 90],
          [970, 250],
          [728, 90],
        ],
      ],
      [
        [300, 250],
        [300, 250],
      ],
    ],
    adClass: 'sl-feed-ad__ad-body-2',
    adViewType: 'CONTENT_ARTICLE',
  },
  editBody3: {
    adUnit: '/1024858/SL_Edit_Article_InCopy3',
    adId: 'SL_Edit_Article_InCopy3',
    adSizes: [
      [970, 90],
      [970, 250],
      [728, 90],
      [300, 250],
    ],
    adSizeMappings: [
      [
        [976, 250],
        [
          [970, 90],
          [970, 250],
          [728, 90],
        ],
      ],
      [
        [300, 250],
        [300, 250],
      ],
    ],
    adClass: 'sl-feed-ad__ad-body-3',
    adViewType: 'CONTENT_ARTICLE',
  },
  editFooter: {
    adUnit: '/1024858/SL_Edit_Article_Comments',
    adId: 'SL_Edit_Article_Comments',
    adSizes: [
      [300, 250],
      [300, 600],
    ],
    adSizeMappings: [
      [
        [976, 250],
        [300, 600],
      ],
      [
        [300, 250],
        [300, 250],
      ],
    ],
    adClass: 'sl-feed-ad__ad-comments',
    adViewType: 'CONTENT_ARTICLE',
  },
  editFooterBottom: {
    adUnit: '/1024858/SL_Edit_Article_Bottom',
    adId: 'SL_Edit_Article_Bottom',
    adSizes: [
      [970, 90],
      [970, 250],
      [728, 90],
      [300, 250],
    ],
    adSizeMappings: [
      [
        [976, 250],
        [
          [970, 90],
          [970, 250],
          [728, 90],
        ],
      ],
      [
        [300, 250],
        [300, 250],
      ],
    ],
    adClass: 'sl-feed-ad__ad-bottom',
    adViewType: 'CONTENT_ARTICLE',
  },
  legacyEditContent: {
    adUnit: '/1024858/Box',
    adId: 'div-gpt-ad-article-unit-3',
    adSizes: [[300, 250]],
    adSizeMappings: [
      [
        [300, 250],
        [300, 250],
      ],
    ],
    adClass: 'sl-feed-ad__ad-body',
    adViewType: 'CONTENT_ARTICLE',
  },
  swellEventRight1: {
    adUnit: '/1024858/SL_Storm_Feed_Right1',
    adId: 'SL_Storm_Feed_Right1',
    adSizes: [[300, 250]],
    adSizeMappings: [
      [
        [300, 250],
        [300, 250],
      ],
    ],
    adClass: 'sl-swell-event-ad__ad-right1',
    adViewType: 'SWELL_EVENT',
  },
  swellEventRight2: {
    adUnit: '/1024858/SL_Storm_Feed_Right2',
    adId: 'SL_Storm_Feed_Right2',
    adSizes: [
      [300, 250],
      [300, 600],
    ],
    adSizeMappings: [
      [
        [976, 250],
        [300, 600],
      ],
      [
        [300, 250],
        [300, 250],
      ],
    ],
    adClass: 'sl-swell-event-ad__ad-right2',
    adViewType: 'SWELL_EVENT',
  },
  swellEventHeroAd: {
    adUnit: '/1024858/SL_Storm_Feed_Top',
    adId: 'SL_Storm_Feed_Top',
    adViewType: 'SWELL_EVENT',
    adSizes: [
      'fluid',
      [1170, 250],
      [1170, 160],
      [990, 90],
      [970, 250],
      [320, 50],
      [970, 90],
      [970, 160],
      [320, 80],
      [300, 250],
      [750, 250],
      [750, 160],
      [728, 90],
    ],
    adSizeMappings: [
      [
        [1200, 0],
        [
          'fluid',
          [1170, 250],
          [1170, 160],
          [990, 90],
          [970, 250],
          [970, 160],
          [970, 90],
          [728, 90],
        ],
      ],
      [
        [992, 0],
        ['fluid', [990, 90], [970, 250], [970, 160], [970, 90], [750, 250], [728, 90]],
      ],
      [
        [768, 0],
        ['fluid', [750, 250], [750, 160], [728, 90]],
      ],
      [
        [320, 0],
        ['fluid', [320, 80], [320, 50]],
      ],
      [
        [0, 0],
        ['fluid', [320, 50]],
      ],
    ],
  },
};

const loadAdConfig = (
  adIdentifier,
  adTargets = [],
  entitlements = [],
  isUser = false,
  adViewType,
) => ({
  adViewType,
  ...adConfigMap[adIdentifier],
  adTargets: [...adTargets, ['usertype', getUserType(entitlements, isUser)]],
});

export default loadAdConfig;
