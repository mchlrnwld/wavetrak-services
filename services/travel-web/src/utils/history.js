/* eslint-disable import/no-mutable-exports */
/* eslint-disable global-require */

let history;

if (typeof document !== 'undefined') {
  const createBrowserHistory = require('history/createBrowserHistory').default;

  history = createBrowserHistory();
}

export default history;
