const getClientIp = (req) => {
  let clientIp;
  if (req && req.headers) {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    clientIp = ip ? ip.split(',')[0] : null;
  }
  return clientIp;
};

export default getClientIp;
