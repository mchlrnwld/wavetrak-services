const checkConditionValue = (condition) => {
  if (!condition) return 'LOLA';
  if (condition === 'NONE') return 'LOLA';
  return condition;
};

export default checkConditionValue;
