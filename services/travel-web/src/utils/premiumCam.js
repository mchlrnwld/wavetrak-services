// eslint-disable-next-line import/prefer-default-export
export const showPremiumCamCTA = ({ spot, isPremium }) => {
  const [camera] = spot.cameras;
  if (!camera) return false;

  return camera.isPremium && !isPremium;
};
