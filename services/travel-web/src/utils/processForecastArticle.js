import React from 'react';
import HtmlToReact from 'html-to-react';

const shouldProcessIframe = (node) =>
  node?.name === 'p' &&
  node?.children?.length &&
  [...node.children].filter((el) => el.name === 'iframe').length &&
  [...node.children].filter(
    (el) => el.attribs && el.attribs.src && !el.attribs.src.includes('soundcloud'),
  ).length;
// div with id including jwplayer
const shouldProcessJWPlayer = (node) =>
  node && node.attribs && node.attribs.id && node.attribs.id.indexOf('jwplayer') > -1;

// div with id including sl cam embed class
const shouldProcessCamEmbed = (node) =>
  node && node.attribs && node.attribs.class && node.attribs.class.indexOf('sl-cam-embed') > -1;

const processNodeDefinitions = new HtmlToReact.ProcessNodeDefinitions(React);
const articleProcessingInstructions = () => [
  {
    shouldProcessNode: (node) => shouldProcessIframe(node),
    processNode: (_, children) => (
      <div className="video-wrap-container">
        <div className="video-wrap">{children}</div>
      </div>
    ),
  },
  {
    shouldProcessNode: (node) => shouldProcessJWPlayer(node),
    processNode: (node, children) => (
      <div className="video-wrap-container">
        <div className="sl-jw-video-wrap">
          {processNodeDefinitions.processDefaultNode(node, children)}
        </div>
      </div>
    ),
  },
  {
    shouldProcessNode: (node) => shouldProcessCamEmbed(node),
    processNode: (node, children) => (
      <div className="video-wrap-container">
        <div className="video-wrap">
          {processNodeDefinitions.processDefaultNode(node, children)}
        </div>
      </div>
    ),
  },
  {
    shouldProcessNode: () => true,
    processNode: processNodeDefinitions.processDefaultNode,
  },
];

export default articleProcessingInstructions;
