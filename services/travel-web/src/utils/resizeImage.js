import config from '../config';

const resizeImage = (media, imageResizing, settings = 'q=85,f=auto,fit=contain') =>
  imageResizing ? `${config.cloudflareImageResizingUrl(media, settings)}` : media;

export default resizeImage;
