import { expect } from 'chai';
import checkConditionValue from './checkConditionValue';

describe('utils / checkConditionValue', () => {
  it('converts NONE and NULL conditions to LOLA', () => {
    expect(checkConditionValue(null)).to.equal('LOLA');
    expect(checkConditionValue('NONE')).to.equal('LOLA');
  });

  it('returns valid conditions unaltered', () => {
    expect(checkConditionValue('FLAT')).to.equal('FLAT');
    expect(checkConditionValue('VERY_POOR')).to.equal('VERY_POOR');
    expect(checkConditionValue('POOR')).to.equal('POOR');
    expect(checkConditionValue('POOR_TO_FAIR')).to.equal('POOR_TO_FAIR');
    expect(checkConditionValue('FAIR')).to.equal('FAIR');
    expect(checkConditionValue('FAIR_TO_GOOD')).to.equal('FAIR_TO_GOOD');
    expect(checkConditionValue('GOOD')).to.equal('GOOD');
    expect(checkConditionValue('VERY_GOOD')).to.equal('VERY_GOOD');
    expect(checkConditionValue('GOOD_TO_EPIC')).to.equal('GOOD_TO_EPIC');
    expect(checkConditionValue('EPIC')).to.equal('EPIC');
  });
});
