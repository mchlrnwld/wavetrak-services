import getClientIp from './getClientIp';
/**
 * Gathers action creators from a `Component`'s static property `fetch` and
 * returns a promise. Resolving the promise  dispatches each action with the
 * matched route params object.
 *
 * @param store dispatches the action creators
 * @param renderProps used to destructure component tree and matched route params
 *
 * @returns {Promise} component tree actions creators to dispatch
 */
const hydrate = async (store, { components, params }, req) => {
  const clientIp = await getClientIp(req);
  return Promise.all(
    components
      .reduce((prev, current = {}) => (current.fetch || []).concat(prev), [])
      .map((doFetch) => store.dispatch(doFetch(params, req, clientIp))),
  );
};

export default hydrate;
