import { slugify } from '@surfline/web-common';

export const spotForecastPaths = {
  base: 'surf-report',
  forecast: 'forecast',
  region: 'region',
};

const spotForecastPath = ({ _id, name }) => `${spotForecastPaths.base}/${slugify(name)}/${_id}`;

export default spotForecastPath;
