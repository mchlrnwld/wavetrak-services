import { expect } from 'chai';
import { geonameMapPath } from './travelPath';

describe('utils / geonamePath', () => {
  it('creates a path to the travel page from geonameId and slug', () => {
    expect(
      geonameMapPath({
        slug: 'united-states',
        geonameId: '6252001',
      }),
    ).to.equal('travel/united-states-surfing-and-beaches/6252001');
  });
});
