export default {
  cdn: 'https://product-cdn.integration.surfline.com/travel/',
  surflineHost: 'https://integration.surfline.com',
  servicesURL: 'https://services.sandbox.surfline.com',
  defaultImageUrl: 'https://spot-thumbnails.sandbox.surfline.com/spots/default/default_1500.jpg',
  legacyUrl: 'http://sandbox.surfline.com',
  bonsaiURL: 'https://staging.elements.widget.shopbonsai.ca/index.js',
  robots: 'noindex,nofollow',
  appKeys: {
    bonsai: process.env.BONSAI_PARTNER_ID || 'ckhm921rg000101mfhkwi0379',
    segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
    newrelic: '',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
  },
};
