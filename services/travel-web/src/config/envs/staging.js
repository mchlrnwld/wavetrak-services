export default {
  baseSiteUrl: 'https://staging.surfline.com/',
  cdn: 'https://product-cdn.staging.surfline.com/travel/',
  surflineHost: 'https://staging.surfline.com',
  servicesURL: 'https://services.staging.surfline.com',
  surflineEmbedURL: 'https://embed.staging.surfline.com',
  defaultImageUrl: 'https://spot-thumbnails.staging.surfline.com/spots/default/default_1500.jpg',
  legacyUrl: 'http://staging.surfline.com',
  bonsaiURL: 'https://staging.elements.widget.shopbonsai.ca/index.js',
  robots: 'noindex,nofollow',
  htl: {
    scriptUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.js',
    cssUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.css',
    isTesting: 'yes',
  },
  appKeys: {
    bonsai: process.env.BONSAI_PARTNER_ID || 'ckhm921rg000101mfhkwi0379',
    segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
    newrelic: '',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || '3prmuinjgigdvi7m5haujp8jtq1un82rc64n',
    fbAppId: process.env.FB_APP_ID || '564041023804928',
  },
};
