export default {
  cdn: 'https://wa.cdn-surfline.com/travel/',
  surflineHost: 'https://www.surfline.com',
  servicesURL: 'https://services.surfline.com',
  surflineEmbedURL: 'https://embed.cdn-surfline.com',
  defaultImageUrl: 'https://spot-thumbnails.cdn-surfline.com/spots/default/default_1500.jpg',
  legacyUrl: 'http://www.surfline.com',
  bonsaiURL: 'https://elements.widget.shopbonsai.ca/index.js',
  htl: {
    scriptUrl: 'https://htlbid.com/v3/surfline.com/htlbid.js',
    cssUrl: 'https://htlbid.com/v3/surfline.com/htlbid.css',
    isTesting: 'no',
  },
  appKeys: {
    bonsai: process.env.BONSAI_PARTNER_ID || 'ckhm921rg000101mfhkwi0379',
    segment: 'Se6IGuzcvUhLx55hLHL0RiXkS6oUTz8L',
    newrelic: '',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'in20v2ebf6q1gh97ldcn1bp8ufdbr404e514',
    fbAppId: process.env.FB_APP_ID || '218714858155230',
  },
};
