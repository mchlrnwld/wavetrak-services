const envConfig = require(`./envs/${process.env.APP_ENV}`); // eslint-disable-line import/no-dynamic-require
const baseConfig = require('./base');

export default {
  ...baseConfig.default,
  ...(envConfig.default || {}),
};
