export default {
  facebookOGImage: '/facebook-og-default.png',
  googleMapAPI: 'AIzaSyAbRbL0Y-QvLi5LF-LWgHKG7LZqFQPZaNw',
  jwPlayerKey: 'TN3oYtsYnZ+cHcCyMwo86x/W68fJWhtWt5w4mA==',
  mobileWidth: 512,
  funnelUrl: '/upgrade',
  hostForSEO: 'https://www.surfline.com',
  signInUrl: '/sign-in',
  createAccountUrl: '/create-account',
  mapBasePath: 'https://surfline.tilehosting.com/styles/basic/static/',
  mapSize: '500x300',
  childGridTwoCol: 680,
  childGridThreeCol: 1090,
  surfNetworkBaseUrl: 'https://api.nodplatform.com/',
  cloudflareImageResizingUrl: (mediaUrl, settings = 'q=85,f=auto,fit=contain') =>
    `https://www.surfline.com/cdn-cgi/image/${settings}/${mediaUrl}`,
};
