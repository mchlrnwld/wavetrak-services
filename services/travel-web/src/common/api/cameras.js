import fetch from 'isomorphic-fetch';
import config from '../../config';

// eslint-disable-next-line import/prefer-default-export
export const fetchCamEmbedDetails = async (cameraId) => {
  const response = await fetch(`${config.surflineEmbedURL}/cam-details/${cameraId}.json`);
  return response.json();
};
