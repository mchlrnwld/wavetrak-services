import baseFetch, { createParamString } from '../baseFetch';

export const fetchEditorialArticleById = (id, preview, geotarget, cookies) => {
  const opts = {};
  let url = `/wp-json/sl/v1/posts?${createParamString({ id })}`;
  if (preview) {
    opts.credentials = 'include';
    url += `&${createParamString({ preview: true, uuid: Date.now() })}`;
  }
  if (geotarget) {
    url += `&${createParamString({ geotarget })}`;
  }
  return baseFetch(url, opts, { cookies });
};

export const fetchEditorialTaxonomyDetails = (taxonomy, term, subCategory, geotarget) => {
  const opts = {};
  let url = `/wp-json/sl/v1/taxonomy/details/${taxonomy}`;
  if (term) url += `/${term}`;
  if (geotarget) {
    url += `?${createParamString({ geotarget })}`;
  }
  // if (subCategory) url += `/${subCategory}`;
  return baseFetch(url, opts);
};

export const fetchEditorialTaxonomyPosts = (
  taxonomy,
  term,
  subCategory,
  limit,
  offset,
  geotarget,
) => {
  const opts = {};
  let url = `/wp-json/sl/v1/taxonomy/posts/${taxonomy}`;
  if (term) url += `/${term}`;
  // if (subCategory) url += `/${subCategory}`;
  return baseFetch(`${url}?${createParamString({ limit, offset, geotarget })}`, opts);
};
