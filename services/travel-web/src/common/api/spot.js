import baseFetch, { createParamString } from '../baseFetch';

export const fetchRegionalConditionForecast = (subregionId, days) =>
  baseFetch(`/kbyg/regions/forecasts/conditions?${createParamString({ subregionId, days })}`);

export const fetchRegionalOverview = (subregionId) =>
  baseFetch(`/kbyg/regions/overview?${createParamString({ subregionId })}`);

export const fetchNearestSpot = (lat, lon) =>
  baseFetch(`/kbyg/mapview/spot?${createParamString({ lat, lon })}`);

export const fetchBatchSubregions = (subregionIds) =>
  baseFetch(`/subregions/batch?${createParamString({ subregionIds })}`);

export const fetchReport = (spotId) => baseFetch(`/kbyg/spots/reports?spotId=${spotId}`);
