import baseFetch from '../baseFetch';

// eslint-disable-next-line
export const getTopSpots = async (geonameId, cookies) =>
  baseFetch(`/kbyg/spots/topspots?id=${geonameId}&type=geoname`, { cookies });
