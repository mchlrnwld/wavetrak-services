import baseFetch, { createParamString } from '../baseFetch';

export const fetchRegionalArticles = async (subregionId, start, limit, includeLocalNews) =>
  baseFetch(
    `/feed/regional?${createParamString({
      subregionId,
      start,
      limit,
      includeLocalNews,
    })}`,
  );

export const fetchRegionalArticleById = (id, cookies) =>
  baseFetch(`/feed/local?${createParamString({ id })}`, { cookies });

export const fetchSwellEventById = (id, cookies) => baseFetch(`/feed/events/${id}?`, { cookies });

export const fetchAllSwellEvents = () => baseFetch('/feed/events?');
