import fetch from 'isomorphic-fetch';
import { Cookies } from 'react-cookie';
import { canUseDOM, doCookiesWarning } from '@surfline/web-common';
import config from '../config';

const checkStatus = (res) => {
  if (res.status >= 200 && res.status < 300) return res;
  const error = new Error(res.statusText);
  error.response = res;
  error.statusCode = res.status;
  throw error;
};

const parseJSON = (response) => response.json();

const doFetch = (path, options = {}) => {
  doCookiesWarning(path, options);
  const cookies = new Cookies(canUseDOM ? undefined : options.cookies);
  const wpApi = path.indexOf('wp-json') > -1;
  const at = cookies.get('access_token');
  if (at && !wpApi) {
    // todo remove
    path += `&accesstoken=${at}`; // eslint-disable-line no-param-reassign
  }

  const baseUrl = wpApi ? config.baseSiteUrl || config.surflineHost : config.servicesURL;
  return fetch(`${baseUrl}${path}`, {
    // headers: {
    //   'X-Auth-AccessToken': cookie.load('access_token'),
    // },
    ...options,
  })
    .then(checkStatus)
    .then(parseJSON);
};

export const createParamString = (params) =>
  Object.keys(params)
    .filter((key) => params[key] !== undefined && params[key] !== null)
    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join('&');

export const doJSFetch = async (path) => {
  // https://developer.mozilla.org/en-US/docs/Web/API/ReadableStream
  const result = await fetch(path, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
    },
  })
    .then((response) => response.body)
    .then((rb) => {
      const reader = rb.getReader();
      return new ReadableStream({
        start(controller) {
          function push() {
            reader.read().then(({ done, value }) => {
              if (done) {
                controller.close();
                return;
              }
              controller.enqueue(value);
              push();
            });
          }

          push();
        },
      });
    })
    .then((stream) => {
      return new Response(stream, { headers: { 'Content-Type': 'text/html' } }).text();
    })
    .then((res) => {
      return res;
    });

  return result;
};

export default doFetch;
