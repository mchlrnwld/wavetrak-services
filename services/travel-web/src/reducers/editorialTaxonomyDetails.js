import createReducer from './createReducer';
import {
  FETCH_EDITORIAL_TAXONOMY_DETAILS,
  FETCH_EDITORIAL_TAXONOMY_DETAILS_SUCCESS,
  FETCH_EDITORIAL_TAXONOMY_DETAILS_FAILURE,
} from '../actions/editorialTaxonomyDetails';

const initialState = {
  loading: false,
  error: false,
  success: false,
  details: null,
};

const handlers = {};

handlers[FETCH_EDITORIAL_TAXONOMY_DETAILS] = (state) => ({
  ...state,
  loading: true,
  success: initialState.success,
  error: initialState.error,
});

handlers[FETCH_EDITORIAL_TAXONOMY_DETAILS_SUCCESS] = (state, { details }) => ({
  ...state,
  details,
  loading: false,
  success: true,
});

handlers[FETCH_EDITORIAL_TAXONOMY_DETAILS_FAILURE] = (state, { error }) => ({
  ...state,
  error,
  loading: false,
  success: false,
});

export default createReducer(handlers, initialState);
