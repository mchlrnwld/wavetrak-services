import createReducer from './createReducer';
import {
  FETCH_LOCATION_VIEW,
  FETCH_LOCATION_VIEW_SUCCESS,
  FETCH_LOCATION_VIEW_FAILURE,
} from '../actions/locationView';

const initialState = {
  loading: false,
  taxonomy: null,
  url: null,
  units: null,
  boundingBox: null,
  travelContent: null,
};

const handlers = {};

handlers[FETCH_LOCATION_VIEW] = (state) => ({
  ...state,
  loading: true,
});

handlers[FETCH_LOCATION_VIEW_SUCCESS] = (
  state,
  { taxonomy, associated, boundingBox, children, travelContent, breadCrumbs, url },
) => ({
  ...state,
  loading: false,
  taxonomy,
  associated,
  boundingBox,
  children,
  travelContent,
  breadCrumbs,
  url,
});

handlers[FETCH_LOCATION_VIEW_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

export default createReducer(handlers, initialState);
