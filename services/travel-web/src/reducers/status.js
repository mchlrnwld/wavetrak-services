import createReducer from './createReducer';
import { NOT_FOUND, REDIRECT_TEMPORARY, INTERNAL_SERVER_ERROR } from '../actions/status';

const initialState = { code: 200 };
const handlers = {};

handlers[NOT_FOUND] = (state) => ({
  ...state,
  code: 404,
});

handlers[REDIRECT_TEMPORARY] = (state, { dest }) => ({
  ...state,
  code: 302,
  dest,
});

handlers[INTERNAL_SERVER_ERROR] = (state) => ({
  ...state,
  code: 500,
});

export default createReducer(handlers, initialState);
