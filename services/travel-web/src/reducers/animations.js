import createReducer from './createReducer';
import { SET_SCROLL_POSITION } from '../actions/animations';

const initialState = {
  scrollPositions: {
    topSpots: {
      top: 0,
      left: 0,
    },
  },
};

const handlers = {};

handlers[SET_SCROLL_POSITION] = (state, { component, scrollPosition }) => ({
  ...state,
  scrollPositions: {
    ...state.scrollPositions,
    [component]: scrollPosition,
  },
});

export default createReducer(handlers, initialState);
