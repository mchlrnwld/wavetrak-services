import createReducer from './createReducer';

import { FETCH_WORLD_TAXONOMY_SUCCESS } from '../actions/worldTaxonomy';

const initialState = [];

const handlers = {};

handlers[FETCH_WORLD_TAXONOMY_SUCCESS] = (state, { taxonomy }) => taxonomy;

export default createReducer(handlers, initialState);
