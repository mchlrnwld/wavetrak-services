import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  FETCH_EDITORIAL_ARTICLE,
  FETCH_EDITORIAL_ARTICLE_SUCCESS,
  FETCH_EDITORIAL_ARTICLE_FAILURE,
} from '../actions/editorial';
import editorial from './editorial';
import editorialArticle from '../actions/fixtures/editorialArticle.json';

describe('reducers / editorial', () => {
  const initialState = deepFreeze({
    loading: false,
    error: false,
    success: false,
    article: null,
  });

  it('initializes with default values', () => {
    expect(editorial()).to.deep.equal(initialState);
  });

  it('sets loading on FETCH_EDITORIAL_ARTICLE', () => {
    const action = deepFreeze({
      type: FETCH_EDITORIAL_ARTICLE,
    });

    expect(editorial(initialState, action)).to.deep.equal({
      loading: true,
      error: false,
      success: false,
      article: null,
    });
  });

  it('sets loading, success and article on FETCH_EDITORIAL_ARTICLE_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_EDITORIAL_ARTICLE_SUCCESS,
      article: editorialArticle,
    });

    expect(editorial(initialState, action)).to.deep.equal({
      loading: false,
      error: false,
      success: true,
      article: editorialArticle,
    });
  });

  it('sets error and loading on FETCH_EDITORIAL_ARTICLE_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_EDITORIAL_ARTICLE_FAILURE,
      error: {
        message: 'An error occurred',
        status: 500,
      },
    });

    expect(editorial(initialState, action)).to.deep.equal({
      loading: false,
      error: {
        message: 'An error occurred',
        status: 500,
      },
      success: false,
      article: null,
    });
  });
});
