import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import animations from './animations';
import editorial from './editorial';
import editorialTaxonomyDetails from './editorialTaxonomyDetails';
import editorialTaxonomyPosts from './editorialTaxonomyPosts';
import forecast from './forecast';
import status from './status';
import locationView from './locationView';
import swellEvents from './swellEvents';
import topSpots from './topSpots';
import worldTaxonomy from './worldTaxonomy';

export default (history, asyncReducers) =>
  combineReducers({
    router: connectRouter(history),
    animations,
    editorial,
    forecast,
    status,
    locationView,
    swellEvents,
    topSpots,
    worldTaxonomy,
    editorialTaxonomyDetails,
    editorialTaxonomyPosts,
    ...asyncReducers,
  });
