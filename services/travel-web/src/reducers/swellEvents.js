import createReducer from './createReducer';
import {
  FETCH_SWELL_EVENT,
  FETCH_SWELL_EVENT_SUCCESS,
  FETCH_SWELL_EVENT_FAILURE,
  FETCH_SWELL_EVENTS,
  FETCH_SWELL_EVENTS_SUCCESS,
  FETCH_SWELL_EVENTS_FAILURE,
} from '../actions/swellEvents';

const initialState = {
  activeEvent: {
    loading: false,
    error: null,
    event: null,
    success: false,
  },
  allEvents: {
    events: [],
    error: null,
    loading: false,
    success: false,
  },
};

const handlers = {};

handlers[FETCH_SWELL_EVENT] = (state) => ({
  ...state,
  activeEvent: {
    ...state.activeEvent,
    loading: true,
    success: initialState.activeEvent.success,
  },
});

handlers[FETCH_SWELL_EVENT_SUCCESS] = (state, { event }) => ({
  ...state,
  activeEvent: {
    ...state.activeEvent,
    loading: false,
    success: true,
    event,
  },
});

handlers[FETCH_SWELL_EVENT_FAILURE] = (state, { error }) => ({
  ...state,
  activeEvent: {
    ...state.activeEvent,
    loading: false,
    success: false,
    error,
  },
});

handlers[FETCH_SWELL_EVENTS] = (state) => ({
  ...state,
  allEvents: {
    ...state.allEvents,
    loading: true,
  },
});

handlers[FETCH_SWELL_EVENTS_SUCCESS] = (state, { events }) => ({
  ...state,
  allEvents: {
    ...state.allEvents,
    events,
    loading: false,
    success: true,
  },
});

handlers[FETCH_SWELL_EVENTS_FAILURE] = (state, { error }) => ({
  ...state,
  allEvents: {
    ...state.allEvents,
    loading: false,
    success: false,
    error,
  },
});

export default createReducer(handlers, initialState);
