import React from 'react';
import { Route, Switch } from 'react-router';
import Loadable from 'react-loadable';
import { ErrorBoundary } from '@surfline/quiver-react';

import TravelContainer from '../containers/routes/TravelContainer';
import routesConfig from './config';

/**
 * NOTE: For server side rendering to work properly, we have to instantiate the Loadable components
 * outside of the JSX so that they are preloaded when we call `Loadable.preloadAll()` on the server
 * side.
 */
const routes = routesConfig.map(({ routeProps, loader, loading, modules, webpack }) => {
  const route = {
    routeProps,
    component: Loadable({ loader, loading, modules, webpack }),
  };
  return route;
});

const Routes = (props) => (
  <ErrorBoundary>
    <div>
      <TravelContainer {...props}>
        <Switch>
          {routes.map(({ routeProps, component }) => (
            <Route key={routeProps.path} {...routeProps} component={component} />
          ))}
        </Switch>
      </TravelContainer>
    </div>
  </ErrorBoundary>
);

export default Routes;
