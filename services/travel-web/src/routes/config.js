// Disabling comma dangle rule because @babel/loader broke with dangling comma
/* eslint-disable comma-dangle */
const TravelLocationRoute = {
  routeProps: {
    path: '/travel/*/:geonameId',
  },
  loader: () =>
    import(
      /* webpackChunkName: "TravelLocation" */
      '../containers/routes/TravelLocation/index.js'
    ),
  loading: () => null,
  modules: ['../containers/routes/TravelLocation/index.js'],
  webpack: () => [require.resolveWeak('../containers/routes/TravelLocation/index.js')],
};

const TravelIndexRoute = {
  routeProps: {
    path: '/travel',
  },
  loader: () =>
    import(
      /* webpackChunkName: "TravelIndex" */
      '../containers/routes/TravelIndex/index.js'
    ),
  loading: () => null,
  modules: ['../containers/routes/TravelIndex/index.js'],
  webpack: () => [require.resolveWeak('../containers/routes/TravelIndex/index.js')],
};

const ForecastArticleRoute = {
  routeProps: {
    path: '/surf-news/local/:slug?/:id',
  },
  loader: () =>
    import(
      /* webpackChunkName: "ForecastArticle" */
      '../containers/routes/ForecastArticle/index.js'
    ),
  loading: () => null,
  modules: ['../containers/routes/ForecastArticle/index.js'],
  webpack: () => [require.resolveWeak('../containers/routes/ForecastArticle/index.js')],
};

const SwellEventRoute = {
  routeProps: {
    path: '/surf-news/swell-alert/:slug?/:id',
  },
  loader: () =>
    import(
      /* webpackChunkName: "SwellEvent" */
      '../containers/routes/SwellEvent/index.js'
    ),
  loading: () => null,
  modules: ['../containers/routes/SwellEvent/index.js'],
  webpack: () => [require.resolveWeak('../containers/routes/SwellEvent/index.js')],
};

const EditorialArticlePreviewRoute = {
  routeProps: {
    path: '/surf-news/_preview/:previewId',
  },
  loader: () =>
    import(
      /* webpackChunkName: "EditorialArticlePreview" */
      '../containers/routes/EditorialArticle/index.js'
    ),
  loading: () => null,
  modules: ['../containers/routes/EditorialArticle/index.js'],
  webpack: () => [require.resolveWeak('../containers/routes/EditorialArticle/index.js')],
};

const EditorialArticleRoute = {
  routeProps: {
    path: '/surf-news/:slug?/:id',
  },
  loader: () =>
    import(
      /* webpackChunkName: "EditorialArticle" */
      '../containers/routes/EditorialArticle/index.js'
    ),
  loading: () => null,
  modules: ['../containers/routes/EditorialArticle/index.js'],
  webpack: () => [require.resolveWeak('../containers/routes/EditorialArticle/index.js')],
};

const EditorialTaxonomyIndexRoute = {
  routeProps: {
    path: '/surf-news/:term?/:subCategory?',
  },
  loader: () => import('../containers/routes/EditorialTaxonomy/index.js'),
  loading: () => null,
  modules: ['../containers/routes/EditorialTaxonomy/index.js'],
  webpack: () => [require.resolveWeak('../containers/routes/EditorialTaxonomy/index.js')],
};

const EditorialTaxonomyRoute = {
  routeProps: {
    path: '/:taxonomy?/:term?/:subCategory?',
  },
  loader: () => import('../containers/routes/EditorialTaxonomy/index.js'),
  loading: () => null,
  modules: ['../containers/routes/EditorialTaxonomy/index.js'],
  webpack: () => [require.resolveWeak('../containers/routes/EditorialTaxonomy/index.js')],
};

const PremiumSeriesRoute = {
  routeProps: {
    path: '/premium-series/:name',
    exact: true,
  },
  loader: () => import('../containers/routes/PremiumSeries/index.js'),
  loading: () => null,
  modules: ['../containers/routes/PremiumSeries/index.js'],
  webpack: () => [require.resolveWeak('../containers/routes/PremiumSeries/index.js')],
};

const NotFoundRoute = {
  routeProps: {
    path: '*',
    status: 404,
  },
  loader: () =>
    import(
      /* webpackChunkName: "NotFound" */
      './NotFound.js'
    ),
  loading: () => null,
  modules: ['./NotFound.js'],
  webpack: () => [require.resolveWeak('./NotFound.js')],
};
/* eslint-enable comma-dangle */

export default [
  TravelLocationRoute,
  TravelIndexRoute,
  ForecastArticleRoute,
  SwellEventRoute,
  EditorialArticlePreviewRoute,
  EditorialArticleRoute,
  EditorialTaxonomyIndexRoute,
  EditorialTaxonomyRoute,
  PremiumSeriesRoute,
  NotFoundRoute,
];
