import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import MetaTags from '../../../components/MetaTags';

const PremiumSeries = React.memo(
  ({
    match: {
      params: { name },
    },
  }) => {
    return (
      <div>
        <MetaTags
          title={`Surfing ${name} Articles:
          Latest Surf ${name} News, Videos, & Photos at Surfline`}
          description={`Access Surfline's daily digest of the latest in surf
          ${name} journalism. Get breaking news in all things surf,
          featured stories, and our renowned surf forecast and science articles.`}
          yoastMeta={null /* TODO: Add yoast meta from response here */}
          permalink={null /* taxonomy.permalink from response */}
        />
        {`name: ${name}`}
      </div>
    );
  },
);

PremiumSeries.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      name: PropTypes.string,
    }),
  }).isRequired,
};

export default connect()(withRouter(PremiumSeries));
