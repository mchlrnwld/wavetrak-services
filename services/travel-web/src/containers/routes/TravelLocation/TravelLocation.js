import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Element } from 'react-scroll';
import { get as _get } from 'lodash';
import { getUserPremiumStatus, trackNavigatedToPage } from '@surfline/web-common';
import { setTopSpotsScrollPosition } from '../../../actions/animations';
import { fetchLocationView } from '../../../actions/locationView';
import { fetchTopSpots } from '../../../actions/topSpots';
import scrollPositionPropType from '../../../propTypes/scrollPosition';
import locationViewPropType from '../../../propTypes/locationView';
import topSpotsPropType from '../../../propTypes/topSpots';
import unitsPropType from '../../../propTypes/units';
import { getTopSpotsScrollPosition } from '../../../selectors/animations';
import { getLocationView, getUnits } from '../../../selectors/locationView';
import getTopSpots from '../../../selectors/topSpots';
import MotionScrollWrapper from '../../../components/MotionScrollWrapper';
import Hero from '../../../components/Hero';
import TravelSummary from '../../../components/TravelSummary';
import ChildGrid from '../../../components/ChildGrid';
import RelatedArticles from '../../../components/RelatedArticles';
import TopSpots from '../../../components/TopSpots';
import TravelMap from '../../../components/TravelMap';
import ViewForecast from '../../../components/ViewForecast';
import MetaTags from '../../../components/MetaTags';
import { travelPaths } from '../../../utils/urls/travelPath';

class TravelLocation extends Component {
  componentDidMount() {
    const {
      match: {
        params: { geonameId },
      },
    } = this.props;
    trackNavigatedToPage(`${_get(this.props, 'locationView.taxonomy.name', null)} Beaches`, {
      category: 'beaches',
      geonameId,
      geonameName: _get(this.props, 'locationView.taxonomy.name', null),
    });
  }

  componentDidUpdate(prevProps) {
    /* fetch location view on client route change */
    const {
      doFetchLocationView,
      doFetchTopSpots,
      match: {
        params: { geonameId },
      },
    } = this.props;

    if (
      _get(prevProps, 'locationView.taxonomy.name', null) !==
      _get(this.props, 'locationView.taxonomy.name', null)
    ) {
      trackNavigatedToPage(`${_get(this.props, 'locationView.taxonomy.name', null)} Beaches`, {
        category: 'beaches',
        geonameId,
        geonameName: _get(this.props, 'locationView.taxonomy.name', null),
      });
    }

    if (geonameId !== prevProps.match.params.geonameId) {
      doFetchLocationView(geonameId);
      doFetchTopSpots(geonameId);
    }
  }

  render() {
    const {
      locationView,
      topSpots,
      units,
      doSetTopSpotsPosition,
      topSpotsScrollPosition,
      isPremium,
    } = this.props;
    const { taxonomy, url, children, travelContent, breadCrumbs } = locationView;

    const name = _get(locationView, 'taxonomy.name', '');
    const title = `Best Beaches in ${name} - Expert Guide to Traveling & Surfing in ${name} - Surfline`;
    const description = `Know Before You Go. Get the best ${name} travel and weather info along with live HD ${name} cams. Access to the world's best surf forecast team at Surfline.`;
    const urlPath = `${travelPaths.base}${url}`;

    /* Temp fix for travel jpgs routing here */
    if (!travelContent && !breadCrumbs) return null;

    return (
      <div>
        <MetaTags title={title} description={description} urlPath={urlPath} />
        <Hero
          travelContent={travelContent}
          taxonomy={taxonomy}
          breadCrumbs={breadCrumbs}
          imageResizing
        />
        <Element name="sl-travel-anchor" className="sl-travel-anchor" />
        {travelContent && travelContent.media.large ? (
          <TravelSummary travelContent={travelContent} />
        ) : null}
        {!isPremium ? <ViewForecast taxonomy={taxonomy} /> : null}
        {children && children.length ? (
          <ChildGrid taxonomy={taxonomy} geoChildren={children} imageResizing />
        ) : null}
        <MotionScrollWrapper scrollPosition={topSpotsScrollPosition}>
          <TopSpots
            taxonomy={taxonomy}
            topSpots={topSpots}
            units={units}
            doSetTopSpotsPosition={(pos) => doSetTopSpotsPosition(pos)}
            isPremium={isPremium}
          />
        </MotionScrollWrapper>
        <TravelMap taxonomy={taxonomy} />
        {travelContent && travelContent.relatedPosts.length ? (
          <RelatedArticles
            relatedArticles={travelContent.relatedPosts.filter((article) => !!article)}
            imageResizing
          />
        ) : null}
      </div>
    );
  }
}

TravelLocation.propTypes = {
  locationView: locationViewPropType,
  units: unitsPropType.isRequired,
  doSetTopSpotsPosition: PropTypes.func.isRequired,
  topSpots: topSpotsPropType.isRequired,
  topSpotsScrollPosition: scrollPositionPropType.isRequired,
  isPremium: PropTypes.bool.isRequired,
  doFetchLocationView: PropTypes.func.isRequired,
  doFetchTopSpots: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      geonameId: PropTypes.string,
    }),
  }).isRequired,
};

TravelLocation.defaultProps = {
  locationView: {
    taxonomy: null,
    url: null,
    associated: null,
    boundingBox: null,
  },
};

TravelLocation.fetch = [
  ({ geonameId }, { cookies }) => fetchLocationView(geonameId, cookies),
  ({ geonameId }, { cookies }) => fetchTopSpots(geonameId, cookies),
];

export default connect(
  (state) => ({
    locationView: getLocationView(state),
    topSpots: getTopSpots(state),
    units: getUnits(state),
    topSpotsScrollPosition: getTopSpotsScrollPosition(state),
    isPremium: getUserPremiumStatus(state),
  }),
  (dispatch) => ({
    doSetTopSpotsPosition: (scrollPosition) => dispatch(setTopSpotsScrollPosition(scrollPosition)),
    doFetchLocationView: (geonameId) => dispatch(fetchLocationView(geonameId)),
    doFetchTopSpots: (geonameId) => dispatch(fetchTopSpots(geonameId)),
  }),
)(TravelLocation);
