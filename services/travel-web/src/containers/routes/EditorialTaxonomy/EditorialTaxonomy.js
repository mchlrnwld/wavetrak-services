import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import { ContentContainer, FeedCard, FeatureCard, GoogleDFP } from '@surfline/quiver-react';
import {
  getUserPremiumStatus,
  getUserDetails,
  getEntitlements,
  trackNavigatedToPage,
} from '@surfline/web-common';
import MetaTags from '../../../components/MetaTags';
import { PostsBlock, LoadMorePosts } from '../../../components/Editorial';
import PageLoading from '../../../components/PageLoading';
import Chevron from '../../../components/Chevron';
import { fetchEditorialTaxonomyDetails } from '../../../actions/editorialTaxonomyDetails';
import {
  fetchEditorialTaxonomyPosts,
  resetEditorialTaxonomyPosts,
} from '../../../actions/editorialTaxonomyPosts';
import { getTaxonomyDetails } from '../../../selectors/editorialTaxonomyDetails';
import { getTaxonomyPosts } from '../../../selectors/editorialTaxonomyPosts';
import loadAdConfig from '../../../utils/adConfig';
import config from '../../../config';

const getRecentPostsClassName = (hasSubcategories, isPremium) =>
  classnames({
    'sl-taxonomy-term-recent-posts': true,
    'sl-taxonomy-term-recent-posts--with-categories': hasSubcategories,
    'sl-taxonomy-term-recent-posts--not-premium': !isPremium,
  });

const getNavClassName = (isActive) =>
  classnames({
    'sl-editorial-taxonomy__header__nav__terms__term': true,
    'sl-editorial-taxonomy__header__nav__terms__term--active': isActive,
  });

const getArticleImage = (media, imageResizing, feature = false) => {
  const mediaUrl = feature && media?.promobox1x ? media?.promobox1x : media?.feed2x;
  const settings = feature ? 'w=1200,q=85,f=auto,fit=contain' : 'w=740,q=75,f=auto,fit=contain';
  return imageResizing ? `${config.cloudflareImageResizingUrl(mediaUrl, settings)}` : mediaUrl;
};

class EditoralTaxonomy extends Component {
  componentDidMount() {
    const {
      doFetchTaxonomyDetails,
      doFetchTaxonomyPosts,
      doResetEditorialTaxonomyPosts,
      taxonomyDetails,
      match: {
        params: { subCategory, taxonomy, term },
      },
    } = this.props;

    if (taxonomyDetails) {
      this.trackPage({
        taxonomy,
        name: taxonomyDetails.name,
        pageId: taxonomyDetails.id,
      });
      this.aboveHeaderAd(taxonomyDetails.id, taxonomyDetails.taxonomy);
    }

    doResetEditorialTaxonomyPosts();
    doFetchTaxonomyDetails(taxonomy, term, subCategory);
    doFetchTaxonomyPosts(taxonomy, term, subCategory);
  }

  componentDidUpdate(prevProps) {
    const {
      doFetchTaxonomyDetails,
      doFetchTaxonomyPosts,
      doResetEditorialTaxonomyPosts,
      taxonomyDetails,
      match: {
        params: { subCategory, taxonomy, term },
      },
    } = this.props;

    if (
      taxonomy !== prevProps.match.params.taxonomy ||
      term !== prevProps.match.params.term ||
      subCategory !== prevProps.match.params.subCategory
    ) {
      doResetEditorialTaxonomyPosts();
      doFetchTaxonomyDetails(taxonomy, term, subCategory);
      doFetchTaxonomyPosts(taxonomy, term, subCategory);
    }

    if (
      taxonomyDetails &&
      prevProps.taxonomyDetails &&
      prevProps.taxonomyDetails.name !== taxonomyDetails.name
    ) {
      this.trackPage({
        taxonomy,
        name: taxonomyDetails.name,
        pageId: taxonomyDetails.id,
      });
      this.aboveHeaderAd(taxonomyDetails.id, taxonomyDetails.taxonomy);
    }
  }

  aboveHeaderAd = (taxonomyId = 0, taxonomy = '') => {
    const { entitlements, userDetails } = this.props;
    const headerAdElement = document.getElementById('sl-header-ad');
    const adTarget = [];
    let adConfigName = 'editorial_home';
    if (taxonomy.indexOf('series') !== -1) {
      adTarget.push(['contentSeriesId', taxonomyId]);
      adConfigName = 'editorial_series';
    } else if (taxonomy.indexOf('category') !== -1) {
      adTarget.push(['contentCategoryId', taxonomyId]);
      adConfigName = 'editorial_category';
    } else if (taxonomy.indexOf('tag') !== -1) {
      adTarget.push(['contentTagId', taxonomyId]);
      adConfigName = 'editorial_tag';
    }
    adTarget.push(['qaFlag', false]);
    if (headerAdElement) {
      ReactDOM.render(
        <GoogleDFP
          adConfig={loadAdConfig(adConfigName, adTarget, entitlements, !!userDetails)}
          isHtl
          isTesting={config.htl.isTesting}
        />,
        headerAdElement,
      );
    }
  };

  trackPage({ taxonomy, name, pageId }) {
    const { location } = this.props;

    const pageProperties = {
      channel: 'editorial',
      category: 'editorial',
      path: location && location.pathname,
      url: window && window.location.href,
    };

    const pageName = `${name.charAt(0).toUpperCase()}${name.slice(1)}`;

    if (!taxonomy || taxonomy === 'category') {
      pageProperties.categoryName = pageName || 'Category';
      pageProperties.categoryId = pageId || 0;
      pageProperties.name = pageProperties.categoryName;
    } else if (taxonomy === 'series') {
      pageProperties.seriesName = pageName || 'Series';
      pageProperties.seriesId = pageId || 0;
      pageProperties.name = pageProperties.seriesName;
    } else if (taxonomy === 'tag') {
      pageProperties.tagName = pageName || 'Tag';
      pageProperties.tagId = pageId || 0;
      pageProperties.name = pageProperties.tagName;
    }

    trackNavigatedToPage(pageName, pageProperties);
  }

  render() {
    const {
      match: {
        params: { subCategory, term },
      },
      taxonomyDetails,
      taxonomyPosts,
      isPremium,
      entitlements,
      userDetails,
    } = this.props;

    if (!taxonomyDetails || !taxonomyPosts) return null;

    const { taxonomy } = taxonomyDetails;

    let pageTitle = taxonomy === 'category' ? null : taxonomy;

    if (term || subCategory) pageTitle = term || subCategory;

    pageTitle = pageTitle ? ` ${pageTitle.charAt(0).toUpperCase()}${pageTitle.slice(1)} ` : ' ';

    const subCategories = taxonomyDetails && taxonomyDetails.subCategories;

    const navTerms = taxonomyDetails && taxonomyDetails.navTerms;

    const isAllNews = taxonomyDetails.slug === 'category';

    const currentSlug = taxonomyDetails && taxonomyDetails.slug;

    const taxonomyId = taxonomyDetails && taxonomyDetails.id;

    const breadcrumbs = taxonomyDetails && taxonomyDetails.breadcrumbs;

    const hasSubcategories = subCategories && subCategories.length > 0;

    const categoryWhitelist = [
      'ALL',
      'PREMIUM',
      'BREAKING',
      'FEATURES',
      'VIDEO',
      'TRAVEL',
      'GEAR',
      'TRAINING',
      'SCIENCE',
    ];

    const adTarget = [];
    let adViewType = 'CONTENT_HOME';
    if (taxonomy) {
      if (taxonomy.indexOf('series') !== -1) {
        adTarget.push(['contentSeriesId', taxonomyId]);
        adViewType = 'CONTENT_SERIES';
      } else if (taxonomy.indexOf('category') !== -1) {
        adTarget.push(['contentCategoryId', taxonomyId]);
        adViewType = 'CONTENT_CATEGORY';
      } else if (taxonomy.indexOf('tag') !== -1) {
        adTarget.push(['contentTagId', taxonomyId]);
        adViewType = 'CONTENT_TAG';
      }
      adTarget.push(['qaFlag', false]);
    }
    return (
      <div className="sl-editorial-taxonomy">
        <MetaTags
          title={`Surfing${pageTitle}Articles:
            Latest Surf${pageTitle}News, Videos, & Photos at Surfline`}
          description={`Access Surfline's daily digest of the latest in surf
            ${pageTitle.toLowerCase()}journalism. Get breaking news in all things surf,
            featured stories, and our renowned surf forecast and science articles.`}
          yoastMeta={null /* TODO: Add yoast meta from response here */}
          permalink={null /* TODO: Add taxonomy.permalink from response */}
        />
        <div className="sl-editorial-taxonomy__ad">
          {!isPremium && (
            <GoogleDFP
              adConfig={loadAdConfig(
                'editorialTaxonomyBelowNav',
                adTarget,
                entitlements,
                !!userDetails,
                adViewType,
              )}
              isHtl
              isTesting={config.htl.isTesting}
            />
          )}
        </div>
        <ContentContainer>
          <div className="sl-editorial-taxonomy__header">
            {breadcrumbs && (
              <div className="sl-editorial-taxonomy__header__breadcrumbs">
                <div className="sl-editorial-taxonomy__header__breadcrumbs__crumb">
                  <Link to="/surf-news/">All Surf News</Link>
                  <Chevron direction="right" />
                </div>
                {breadcrumbs.map((crumb) => (
                  <div className="sl-editorial-taxonomy__header__breadcrumbs__crumb">
                    <Link to={`/${taxonomy}/${crumb.slug}`}>{crumb.name}</Link>
                    <Chevron direction="right" />
                  </div>
                ))}
              </div>
            )}
            <h1>{isAllNews ? 'Surf News' : taxonomyDetails.name}</h1>
            {!breadcrumbs && (
              <div className="sl-editorial-taxonomy__header__nav">
                <div className="sl-editorial-taxonomy__header__nav__terms">
                  <Link className={getNavClassName(isAllNews)} to="/surf-news/">
                    All
                  </Link>
                  {navTerms &&
                    navTerms
                      .filter(
                        (navTerm) => categoryWhitelist.indexOf(navTerm.slug.toUpperCase()) !== -1,
                      )
                      .map((navTerm) => (
                        <Link
                          className={getNavClassName(currentSlug === navTerm.slug)}
                          to={`/category/${navTerm.slug}`}
                        >
                          {navTerm.name}
                        </Link>
                      ))}
                </div>
              </div>
            )}
          </div>
          {taxonomyPosts.loading && !taxonomyPosts.infiniteScroll ? (
            <PageLoading />
          ) : (
            <>
              <div className="sl-tax-cat-blocks">
                {subCategories &&
                  subCategories.map((cat) => {
                    const { posts, name, slug } = cat;
                    return (
                      posts &&
                      !(cat.breadcrumbs && isAllNews) && (
                        <PostsBlock
                          name={name}
                          posts={posts}
                          slug={slug}
                          taxonomy={taxonomy}
                          term={term}
                          imageResizing
                        />
                      )
                    );
                  })}
              </div>
              <div className={getRecentPostsClassName(hasSubcategories, isPremium)}>
                {!isPremium && hasSubcategories && (
                  <div className="sl-taxonomy-term-recent-posts__ad">
                    <GoogleDFP
                      adConfig={loadAdConfig(
                        'editorialTaxonomyMidPage',
                        adTarget,
                        entitlements,
                        !!userDetails,
                        adViewType,
                      )}
                      isHtl
                      isTesting={config.htl.isTesting}
                    />
                  </div>
                )}
                <h2>Recent</h2>
                <div className="sl-taxonomy-term-recent-posts__posts">
                  {taxonomyPosts &&
                    taxonomyPosts.posts &&
                    taxonomyPosts.posts.map((post) => {
                      if (!post) return null;
                      const timestamp = Math.floor(new Date(post.createdAt) / 1000);
                      return post.isFeatured ? (
                        <section>
                          {!isPremium && (
                            <div className="sl-taxonomy-term-recent-posts__ad">
                              <GoogleDFP
                                adConfig={loadAdConfig(
                                  'editorialTaxonomyMidPage',
                                  adTarget,
                                  entitlements,
                                  !!userDetails,
                                  adViewType,
                                )}
                                isHtl
                                isTesting={config.htl.isTesting}
                              />
                            </div>
                          )}
                          {post.id && (
                            <FeatureCard
                              isPremium={post.premium}
                              imageUrl={getArticleImage(post.media, true, true)}
                              permalink={post.permalink}
                              title={post.title}
                            />
                          )}
                        </section>
                      ) : (
                        <FeedCard
                          isPremium={post.premium}
                          category={post.categories[0]}
                          imageUrl={getArticleImage(post.media, true)}
                          title={post.title}
                          subtitle={post.subtitle}
                          createdAt={timestamp}
                          permalink={post.permalink}
                          showTimestamp={!Number.isNaN(Number(timestamp))}
                        />
                      );
                    })}
                </div>
                <LoadMorePosts subCategory={subCategory} taxonomy={taxonomy} term={term} />
              </div>
            </>
          )}
        </ContentContainer>
      </div>
    );
  }
}

EditoralTaxonomy.fetch = [
  ({ taxonomy, term, subCategory }) => fetchEditorialTaxonomyDetails(taxonomy, term, subCategory),
  ({ taxonomy, term, subCategory }) => fetchEditorialTaxonomyPosts(taxonomy, term, subCategory),
];

EditoralTaxonomy.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      term: PropTypes.string,
      taxonomy: PropTypes.string,
      subCategory: PropTypes.string,
    }),
  }).isRequired,
  taxonomyDetails: PropTypes.shape({
    name: PropTypes.string,
    slug: PropTypes.string,
    taxonomy: PropTypes.string,
    id: PropTypes.number,
    subCategories: PropTypes.arrayOf(PropTypes.shape({})),
    navTerms: PropTypes.arrayOf(PropTypes.shape({})),
    breadcrumbs: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  taxonomyPosts: PropTypes.shape({
    posts: PropTypes.arrayOf(PropTypes.shape({})),
    loading: PropTypes.bool,
    infiniteScroll: PropTypes.bool,
  }),
  doFetchTaxonomyDetails: PropTypes.func.isRequired,
  doFetchTaxonomyPosts: PropTypes.func.isRequired,
  doResetEditorialTaxonomyPosts: PropTypes.func.isRequired,
  isPremium: PropTypes.bool.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  entitlements: PropTypes.arrayOf(PropTypes.string).isRequired,
  userDetails: PropTypes.shape({}).isRequired,
};

EditoralTaxonomy.defaultProps = {
  taxonomyDetails: null,
  taxonomyPosts: null,
};

export default connect(
  (state) => ({
    taxonomyDetails: getTaxonomyDetails(state),
    taxonomyPosts: getTaxonomyPosts(state),
    isPremium: getUserPremiumStatus(state),
    userDetails: getUserDetails(state),
    entitlements: getEntitlements(state),
  }),
  (dispatch) => ({
    doFetchTaxonomyDetails: (taxonomy, term, subCategory) =>
      dispatch(fetchEditorialTaxonomyDetails(taxonomy, term, subCategory)),
    doFetchTaxonomyPosts: (taxonomy, term, subCategory) =>
      dispatch(fetchEditorialTaxonomyPosts(taxonomy, term, subCategory)),
    doResetEditorialTaxonomyPosts: () => dispatch(resetEditorialTaxonomyPosts()),
  }),
)(withRouter(EditoralTaxonomy));
