import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { WorldTaxonomy } from '@surfline/quiver-react';
import MetaTags from '../../../components/MetaTags';
import { travelPaths, geonameMapPath } from '../../../utils/urls/travelPath';
import { fetchWorldTaxonomy } from '../../../actions/worldTaxonomy';
import worldTaxonomyPropType from '../../../propTypes/worldTaxonomy';

class TravelIndex extends Component {
  componentDidMount() {
    const { worldTaxonomy, doFetchWorldTaxonomy } = this.props;
    if (!worldTaxonomy) {
      doFetchWorldTaxonomy();
    }
  }

  suffix = 'Beaches';

  countryComponent = (country) => (
    <a href={geonameMapPath(country)}>
      {country.name} {this.suffix}
    </a>
  );

  render() {
    const { worldTaxonomy } = this.props;
    return (
      <div className="sl-cams-and-forecasts">
        <MetaTags
          title="Surfline Travel Info for Every Beach in the World"
          description="Know Before You Go. Get the best travel and weather info along with live HD beach cams. Access the world's best surf forecast team at Surfline."
          urlPath={travelPaths.base}
        />
        <WorldTaxonomy
          taxonomy={worldTaxonomy}
          countryComponent={this.countryComponent}
          headerSuffix={this.suffix}
        />
      </div>
    );
  }
}

TravelIndex.propTypes = {
  worldTaxonomy: PropTypes.arrayOf(worldTaxonomyPropType),
  doFetchWorldTaxonomy: PropTypes.func.isRequired,
};

TravelIndex.defaultProps = {
  worldTaxonomy: [],
};

TravelIndex.fetch = [fetchWorldTaxonomy];

export default connect(
  (state) => ({
    worldTaxonomy: state.worldTaxonomy,
  }),
  (dispatch) => ({
    doFetchWorldTaxonomy: () => dispatch(fetchWorldTaxonomy),
  }),
)(TravelIndex);
