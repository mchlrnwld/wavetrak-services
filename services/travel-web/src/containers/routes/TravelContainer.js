import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { getUserPremiumStatus } from '@surfline/web-common';
import { getStatusCode } from '../../selectors/status';
import NotFound from './NotFound';

class TravelContainer extends Component {
  getClassName = (isPremium) =>
    classNames({
      'sl-travel-page-container': true,
      'sl-travel-page-container--is-premium': isPremium,
    });

  render() {
    const { children, status, isPremium } = this.props;
    return (
      <div className={this.getClassName(isPremium)}>{status === 404 ? <NotFound /> : children}</div>
    );
  }
}

TravelContainer.propTypes = {
  children: PropTypes.node,
  isPremium: PropTypes.bool.isRequired,
  status: PropTypes.number.isRequired,
};

TravelContainer.defaultProps = {
  children: null,
};

export default connect((state) => ({
  isPremium: getUserPremiumStatus(state),
  status: getStatusCode(state),
}))(TravelContainer);
