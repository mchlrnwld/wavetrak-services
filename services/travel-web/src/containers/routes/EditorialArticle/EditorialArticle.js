import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import queryString from 'query-string';
import classnames from 'classnames';
import { GoogleDFP, PremiumRibbonIcon } from '@surfline/quiver-react';
import {
  trackNavigatedToPage,
  trackEvent,
  pageStructuredData,
  getUserPremiumStatus,
  getUserSettings,
  getUserDetails,
  getEntitlements,
} from '@surfline/web-common';
import EditorialArticleBody from '../../../components/EditorialArticleBody';
import EditorialArticleFooter from '../../../components/EditorialArticleFooter';
import EditorialArticleHeading from '../../../components/EditorialArticleHeading';
import EditorialAuthor from '../../../components/EditorialAuthor';
import PreviewBanner from '../../../components/PreviewBanner';
import MetaTags from '../../../components/MetaTags';
import { fetchEditorialArticle } from '../../../actions/editorial';
import editorialArticlePropType from '../../../propTypes/article';
import userDetailsPropTypes from '../../../propTypes/userDetails';
import userSettingsPropTypes from '../../../propTypes/userSettings';
import resizeImage from '../../../utils/resizeImage';
import loadAdConfig from '../../../utils/adConfig';
import config from '../../../config';

class EditorialArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMobile: false,
    };
  }

  componentDidMount() {
    const {
      match: {
        params: { previewId },
      },
      article,
      isPremium,
    } = this.props;
    this.setIsMobile();
    if (!previewId) {
      const { sponsoredArticle, displayOptions } = article;
      this.aboveHeaderAd(sponsoredArticle?.dfpKeyword, displayOptions?.hideAds, isPremium);
      trackNavigatedToPage('editorial', {
        channel: 'editorial',
        postId: article.id,
        postDate: article.createdAt,
        postCategories: this.getCSL(article.categories),
        postTags: this.getCSL(article.tags),
        postSeries: this.getCSL(article.series),
        postCurrentPromotion: this.getCSL(article.promotions),
        postContentPillar: this.getCSL(article.contentPillar),
        isPremiumArticle: article.premium.premium ? article.premium.premium : false,
        isSponsoredArticle: article.sponsoredArticle.sponsoredArticle === true,
        authorName: article.author.name,
        isVideo: document.getElementsByClassName('video-wrap-container').length > 0,
        category: 'editorial',
        title: article.content.displayTitle || article.content.title,
        url: article.permalink,
      });
    }
  }

  aboveHeaderAd = (dfpKeyword, hideAds, isPremium) => {
    const { entitlements, userDetails } = this.props;
    const headerAdElement = document.getElementById('sl-header-ad');
    const adConfigName = isPremium
      ? 'editorial_article_superheader_premium'
      : 'editorial_article_superheader';
    const adTarget = [['qaFlag', false]];
    if (dfpKeyword) adTarget.push(['contentKeyword', dfpKeyword]);
    if (headerAdElement && !hideAds) {
      ReactDOM.render(
        <GoogleDFP
          adConfig={loadAdConfig(adConfigName, adTarget, entitlements, !!userDetails)}
          isHtl
          isTesting={config.htl.isTesting}
        />,
        headerAdElement,
      );
    }
  };

  setIsMobile = () => this.setState({ isMobile: window.innerWidth < 976 });

  onClickShareArticle = (shareChannel) => {
    const { article } = this.props;
    trackEvent('Clicked Share Icon', {
      title: article.content.title,
      contentType: 'Editorial',
      contentId: `${article.id}`,
      postCategories: article.categories.map((category) => category.name).join(','),
      postContentPillar: article.contentPillar.map((category) => category.name).join(','),
      locationCategory: 'Article Page',
      destinationUrl: article.permalink,
      mediaType: article.media ? article.media.type : null,
      shareChannel,
    });
  };

  getCSL = (arr) => arr.map((el) => el.name).join(',');

  getHeroClasses = () => {
    const {
      article: {
        hero: { align, gradient, location, type, hideTitle },
      },
    } = this.props;
    const textOnHero = location && type !== 'Fixed' && !hideTitle;
    return classnames({
      'sl-editorial-article__hero': true,
      'sl-editorial-article__hero--fixed': type === 'Fixed',
      'sl-editorial-article__hero--heading': textOnHero,
      'sl-editorial-article__hero--gradient': gradient,
      'sl-editorial-article__hero--v-center': textOnHero && align.vertical === 'Middle',
      'sl-editorial-article__hero--bottom': textOnHero && align.vertical === 'Bottom',
      'sl-editorial-article__hero--h-center': textOnHero && align.horizontal === 'Center',
      'sl-editorial-article__hero--right': textOnHero && align.horizontal === 'Right',
    });
  };

  getCreditClasses = () => {
    const {
      article: {
        hero: {
          credit: { alignment },
        },
      },
    } = this.props;
    return classnames({
      'sl-editorial-article__hero__cred': true,
      'sl-editorial-article__hero__cred--center': alignment === 'Center',
      'sl-editorial-article__hero__cred--right': alignment === 'Right',
    });
  };

  render() {
    const {
      article,
      isPremium,
      location: { search },
      match: {
        params: { wpnonce },
      },
      userSettings,
    } = this.props;
    const { isMobile } = this.state;
    const { qaFlag, native } = queryString.parse(search);

    if (!article || !article.content) return null;

    const showTitleBelowHero =
      !article.hero.showHero || !article.hero.location || article.hero.type === 'Fixed';

    let dateFormat = 'MDY';
    if (userSettings && userSettings.date && userSettings.date.format)
      dateFormat = userSettings.date.format;
    return (
      <div>
        {wpnonce ? (
          <PreviewBanner article={article} />
        ) : (
          <MetaTags
            title={article.yoastMeta.title || article.content.title}
            yoastMeta={article.yoastMeta}
            tags={article.tags}
            permalink={article.permalink}
          >
            <script type="application/ld+json">
              {pageStructuredData({
                url: article.permalink,
                headline: article.yoastMeta.title || article.content.title,
                image: article.hero?.imageUrl || article.media.feed1x,
                dateModified: article.updatedAt,
                author: article.author.name,
                description: article.yoastMeta['og:description'] || article.content.title,
                cssSelector: '.sl-editorial-article-body',
              })}
            </script>
          </MetaTags>
        )}
        {article.hero.showHero ? (
          <div
            className={this.getHeroClasses()}
            style={{
              background: `url('${resizeImage(article.hero.imageUrl, true)}')`,
              backgroundSize: 'cover',
              backgroundPosition: 'center center',
            }}
          >
            {article.premium.premium ? <PremiumRibbonIcon /> : null}
            {!article.hero.hideTitle ? <EditorialArticleHeading article={article} /> : null}
            <div className={this.getCreditClasses()}>{article.hero.credit.text}</div>
          </div>
        ) : null}
        <div>
          {showTitleBelowHero && !article.hero.hideTitle ? (
            <EditorialArticleHeading article={article} />
          ) : null}
          <EditorialAuthor
            author={article.author}
            createdAt={article.createdAt}
            hideAuthor={article.displayOptions.hideAuthor}
            updatedAt={article.updatedAt}
            url={article.permalink}
            onClickLink={(shareChannel) => this.onClickShareArticle(shareChannel)}
            sponsoredArticle={article.sponsoredArticle}
            dateFormat={dateFormat}
          />
        </div>
        <EditorialArticleBody
          article={article}
          qaFlag={qaFlag}
          isPremium={isPremium}
          onClickLink={(shareChannel) => this.onClickShareArticle(shareChannel)}
          native={native}
          imageResizing
        />
        <EditorialArticleFooter
          article={article}
          qaFlag={qaFlag}
          isPremium={isPremium}
          isMobile={isMobile}
          imageResizing
        />
      </div>
    );
  }
}

EditorialArticle.fetch = [
  ({ previewId, id }, { cookies }) => {
    if (previewId) return fetchEditorialArticle(previewId, true);
    return fetchEditorialArticle(id, null, cookies);
  },
];

EditorialArticle.propTypes = {
  article: editorialArticlePropType,
  match: PropTypes.shape({
    params: PropTypes.shape({
      previewId: PropTypes.string,
      wpnonce: PropTypes.string,
    }),
    id: PropTypes.string,
    wpnonce: PropTypes.string,
  }).isRequired,
  isPremium: PropTypes.bool.isRequired,
  location: PropTypes.shape({
    search: PropTypes.string,
  }),
  userSettings: userSettingsPropTypes,
  entitlements: PropTypes.arrayOf(PropTypes.string).isRequired,
  userDetails: userDetailsPropTypes.isRequired,
};

EditorialArticle.defaultProps = {
  article: null,
  location: {
    search: null,
  },
  userSettings: {},
};

export default connect(
  (state) => ({
    article: state.editorial.article,
    isPremium: getUserPremiumStatus(state),
    userSettings: getUserSettings(state),
    userDetails: getUserDetails(state),
    entitlements: getEntitlements(state),
  }),
  (dispatch) => ({
    doFetchEditorialArticle: (id, wpnonce) => dispatch(fetchEditorialArticle(id, wpnonce)),
  }),
)(withRouter(EditorialArticle));
