import React, { Component, useRef, useCallback } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import Sticky from 'react-stickynode';
import queryString from 'query-string';
import { format } from 'date-fns';
import {
  GoogleDFP,
  ShareArticle,
  ForecastArticleBody,
  SwellEventCard,
  TrackableLink,
} from '@surfline/quiver-react';
import {
  canUseDOM,
  formatArticleDate,
  trackNavigatedToPage,
  trackEvent,
  trackClickedSubscribeCTA,
  getUserPremiumStatus,
} from '@surfline/web-common';
import {
  replaceWithClickableImage,
  renderJWElements,
  renderIgElements,
  trackSwellEventClickLinks,
  renderTwitterElements,
  renderCameraEmbeds,
} from '../../../utils/renderCustomComponents';
import { fetchSwellEvent, fetchSwellEvents } from '../../../actions/swellEvents';
import swellEventPropType from '../../../propTypes/swellEvent';
import { getActiveSwellEvent, getSwellEvents } from '../../../selectors/getActiveSwellEvent';
import FeaturedVideo from '../../../components/FeaturedVideo';
import MetaTags from '../../../components/MetaTags';
import StickyAdUnit from '../../../components/StickyAdUnit';
import SwellEventHighlights from '../../../components/SwellEventHighlights';
import SwellEventModal from '../../../components/SwellEventModal';
import loadAdConfig from '../../../utils/adConfig';
import config from '../../../config';

const TaxonomyLink = ({ taxonomy, onClickRegionalForecast }) => {
  const linkRef = useRef();
  const eventPropertiesCallback = useCallback(() => onClickRegionalForecast(taxonomy), [
    taxonomy,
    onClickRegionalForecast,
  ]);

  return (
    <TrackableLink
      ref={linkRef}
      eventName="Clicked Swell Alert Link"
      eventProperties={eventPropertiesCallback}
    >
      <a ref={linkRef} href={taxonomy.href}>
        <p className="sl-swell-event-related-tax">{taxonomy.name}</p>
      </a>
    </TrackableLink>
  );
};
class SwellEvent extends Component {
  constructor(props) {
    super(props);
    const {
      activeSwellEvent: { updates },
    } = this.props;
    this.state = {
      highlights: updates && updates?.length ? updates.slice(0, 5) : [],
      highlightsLoaded: false,
      articleImageIndex: null,
      articleImages: [],
      modalIsOpen: false,
    };
  }

  componentDidMount() {
    const {
      activeSwellEvent: { id, name },
      doFetchSwellEvents,
      activeSwellEvent,
      isPremium,
    } = this.props;
    renderIgElements();
    renderTwitterElements();
    renderJWElements();
    renderCameraEmbeds(isPremium, id, name, 'forecast');
    doFetchSwellEvents();
    replaceWithClickableImage(
      (articleImageIndex) => {
        this.setState({ articleImageIndex, modalIsOpen: true });
      },
      (images) => this.setState({ articleImages: images }),
    );
    this.aboveHeaderAd(isPremium);

    trackNavigatedToPage('Swell Alert', {
      category: 'editorial',
      channel: 'Swell Alert',
      subCategory: activeSwellEvent.basins ? activeSwellEvent.basins.join(',') : null,
      name: 'Swell Alert',
      postDate: activeSwellEvent.createdAt,
      title: activeSwellEvent.name,
      url: activeSwellEvent.permalink,
    });
    trackSwellEventClickLinks(activeSwellEvent.id);
  }

  aboveHeaderAd = (isPremium) => {
    const {
      activeSwellEvent,
      location: { search },
    } = this.props;
    const headerAdElement = document.getElementById('sl-header-ad');
    const { qaFlag } = queryString.parse(search);
    if (!isPremium) {
      ReactDOM.render(
        <GoogleDFP
          adConfig={loadAdConfig('swellEventHeroAd', [
            ['qaFlag', qaFlag],
            ['contentKeyword', activeSwellEvent.dfpKeyword],
          ])}
          isHtl
          isTesting={config.htl.isTesting}
        />,
        headerAdElement,
      );
    }
  };

  onClickShareEvent = (shareChannel) => {
    const { activeSwellEvent } = this.props;
    trackEvent('Clicked Share Icon', {
      title: activeSwellEvent.name,
      contentType: 'Forecast',
      contentId: `${activeSwellEvent.id}`,
      locationCategory: 'Swell Alert',
      destinationUrl: activeSwellEvent.permalink,
      shareChannel,
    });
  };

  onClickCard = (swellEvent) => {
    const { activeSwellEvent } = this.props;
    trackEvent('Clicked Swell Alert Link', {
      title: activeSwellEvent.name,
      locationCategory: 'Current Swell Alerts',
      destinationUrl: swellEvent.permalink,
      destinationTitle: swellEvent.name,
    });
  };

  onClickedHighlight = (highlight) => {
    const { activeSwellEvent } = this.props;
    trackEvent('Clicked Swell Alert Link', {
      title: activeSwellEvent.name,
      destinationTitle: highlight.title,
      locationCategory: 'Swell Alert Highlights',
    });
  };

  onClickRegionalForecast = (taxonomy) => {
    const { activeSwellEvent } = this.props;

    return {
      title: activeSwellEvent.name,
      locationCategory: 'Related Regional Forecasts',
      destinationUrl: taxonomy.href,
      subregionName: taxonomy.name,
      subregionId: taxonomy.id,
    };
  };

  onClickFeaturedVideo = () => {
    const { activeSwellEvent } = this.props;
    trackEvent('Clicked Swell Alert Link', {
      title: activeSwellEvent.name,
      locationCategory: 'Featured Video',
    });
  };

  onClickSubscribeCTA = () => {
    const { activeSwellEvent } = this.props;

    trackClickedSubscribeCTA({
      category: 'editorial',
      contentType: 'Forecast',
      location: 'Swell Alert',
      title: activeSwellEvent.name,
    });

    if (canUseDOM) {
      setTimeout(() => window.location.assign(`${config.surflineHost}${config.funnelUrl}`), 300);
    }
  };

  loadMoreHighlights = () => {
    const {
      activeSwellEvent: { updates },
    } = this.props;
    this.setState({
      highlights: updates,
      highlightsLoaded: true,
    });
  };

  eventOverviewContent = (event, highlights, highlightsLoaded, loadMoreHighlights, location) => (
    <div>
      <div className="sl-swell-event__content__video">
        <FeaturedVideo
          location={location}
          jwPlayerId={event.jwPlayerVideoId}
          bannerImageUrl={event.banner}
          onClickFeaturedVideo={this.onClickFeaturedVideo}
        />
      </div>
      {highlights.length ? (
        <SwellEventHighlights
          highlights={highlights}
          highlightsLoaded={highlightsLoaded}
          loadMoreHighlights={loadMoreHighlights}
          swellEvent={event}
          onClickHighlight={this.onClickedHighlight}
        />
      ) : null}
    </div>
  );

  render() {
    const {
      activeSwellEvent,
      isPremium,
      swellEvents,
      location: { search },
    } = this.props;
    const {
      articleImageIndex,
      articleImages,
      highlights,
      highlightsLoaded,
      modalIsOpen,
    } = this.state;
    const subregions = activeSwellEvent.taxonomies.filter(
      (taxonomy) => taxonomy.type === 'subregion',
    );
    const defaultUrl = 'https://www.gravatar.com/avatar/d41d8cd98f00b204e9800998ecf8427e?d=mm';
    const description = `${activeSwellEvent.summary} Get the latest ${activeSwellEvent.name} updates at Surfline so you can Know Before You Go.`;
    const { qaFlag } = queryString.parse(search);
    if (!activeSwellEvent) return null;
    return (
      <div className="sl-swell-event">
        {modalIsOpen && (
          <SwellEventModal
            onBackdropClick={(isOpen) => this.setState({ modalIsOpen: isOpen })}
            setOpen={(isOpen) => this.setState({ modalIsOpen: isOpen })}
            articleImages={articleImages}
            articleImageIndex={articleImageIndex}
            open={modalIsOpen}
          />
        )}
        <MetaTags
          title={activeSwellEvent.name}
          permalink={activeSwellEvent.permalink}
          description={description}
          image={activeSwellEvent.banner}
        />
        <div className="sl-swell-event__header">
          <div className="sl-swell-event__header__content">
            {activeSwellEvent.titleTag && activeSwellEvent.titleTag.toUpperCase() !== 'ALERT' ? (
              <div className="sl-swell-event__tag">
                {activeSwellEvent.titleTag
                  ? activeSwellEvent.titleTag.toUpperCase()
                  : 'SWELL ALERT'}
              </div>
            ) : null}
            <h1 className="sl-swell-event__title">
              {activeSwellEvent.name}
              <span className="sl-swell-event__updated">
                {formatArticleDate(new Date(activeSwellEvent.updatedAt) / 1000)}
              </span>
            </h1>
            <h2 className="sl-swell-event__summary">{activeSwellEvent.summary}</h2>
            <ShareArticle
              url={activeSwellEvent.permalink}
              onClickLink={(shareChannel) => this.onClickShareEvent(shareChannel)}
            />
          </div>
        </div>
        <div className="sl-swell-event__content__wrapper" id="sl-swell-event__page_wrapper">
          <div className="sl-swell-event__content">
            <div className="sl-swell-event__content__overview">
              <div className="sl-swell-event__content__overview__mobile">
                {this.eventOverviewContent(
                  activeSwellEvent,
                  highlights,
                  highlightsLoaded,
                  this.loadMoreHighlights,
                  'overview-mobile',
                )}
              </div>
              <div className="sl-swell-event__content__overview__sticky">
                <Sticky bottomBoundary="#sl-swell-event__page_wrapper">
                  {this.eventOverviewContent(
                    activeSwellEvent,
                    highlights,
                    highlightsLoaded,
                    this.loadMoreHighlights,
                    'overview-sticky',
                  )}
                </Sticky>
              </div>
            </div>
            <div className="sl-swell-event__content__feed__wrapper">
              {highlights.length ? (
                <SwellEventHighlights
                  onClickHighlight={this.onClickedHighlight}
                  highlights={highlights}
                  highlightsLoaded={highlightsLoaded}
                  loadMoreHighlights={this.loadMoreHighlights}
                  swellEvent={activeSwellEvent}
                />
              ) : null}
              {!isPremium ? (
                <div className="sl-swell-event-ad__right-1__overview">
                  <GoogleDFP
                    adConfig={loadAdConfig('swellEventRight1', [
                      ['qaFlag', qaFlag],
                      ['contentKeyword', activeSwellEvent.dfpKeyword],
                    ])}
                    isHtl
                    isTesting={config.htl.isTesting}
                  />
                </div>
              ) : null}
              <div className="sl-swell-event__content__feed" id="sl-swell-event__content__feed">
                {activeSwellEvent.updates.map((article) => (
                  <div
                    key={article.id}
                    className="sl-swell-event__article-container"
                    id={article.id}
                  >
                    <div className="sl-swell-event__article-author">
                      <img
                        className="sl-swell-event__author-icon"
                        src={`${article.author.iconUrl || defaultUrl}`}
                        alt={article.author.name || 'Surfline Avatar'}
                      />
                      <div className="sl-swell-event__author-details">
                        <div className="sl-swell-event__author-name">{article.author.name}</div>
                        <div className="sl-swell-event__author-updated">
                          {format(article.updatedAt * 1000, 'MMM DD, YYYY')}
                        </div>
                      </div>
                    </div>
                    <div className="sl-swell-event__article-time">
                      {format(article.updatedAt * 1000, 'h:mma')}
                    </div>
                    <ForecastArticleBody
                      key={article.id}
                      id={article.id}
                      title={article.title}
                      showPaywall={article.premiumContent && !isPremium}
                      body={article.content}
                      onClickPaywall={this.onClickSubscribeCTA}
                      showAll
                      showMore
                    />
                    <hr />
                  </div>
                ))}
              </div>
            </div>
            <div className="sl-swell-event__content__related">
              {!isPremium ? (
                <div className="sl-swell-event-ad__right-1__related">
                  <GoogleDFP
                    adConfig={loadAdConfig('swellEventRight1', [
                      ['qaFlag', qaFlag],
                      ['contentKeyword', activeSwellEvent.dfpKeyword],
                    ])}
                    isHtl
                    isTesting={config.htl.isTesting}
                  />
                </div>
              ) : null}
              <div className="sl-swell-event__content__video">
                <FeaturedVideo
                  location="related"
                  jwPlayerId={activeSwellEvent.jwPlayerVideoId}
                  bannerImageUrl={activeSwellEvent.banner}
                  onClickFeaturedVideo={this.onClickFeaturedVideo}
                />
              </div>
              {subregions?.length > 0 &&
              activeSwellEvent.titleTag &&
              activeSwellEvent.titleTag.toUpperCase() !== 'ALERT' ? (
                <div>
                  <div className="sl-swell-event__content__related-subregions">
                    <h6>Related Regional Forecasts</h6>
                    {subregions.map((taxonomy) => (
                      <TaxonomyLink
                        key={taxonomy.name}
                        taxonomy={taxonomy}
                        onClickRegionalForecast={this.onClickRegionalForecast}
                      />
                    ))}
                  </div>
                  {swellEvents.filter((event) => event.id !== activeSwellEvent.id).length > 1 ? (
                    <div className="sl-swell-event__current-events">
                      <h6 className="sl-swell-event__current-events__title">
                        Current Swell Alerts
                      </h6>
                      {swellEvents
                        .filter((event) => event.id !== activeSwellEvent.id)
                        .map((event) => (
                          <SwellEventCard
                            onClickCard={(swellEvent) => this.onClickCard(swellEvent)}
                            swellEvent={event}
                          />
                        ))}
                    </div>
                  ) : null}
                </div>
              ) : null}
              {!isPremium ? (
                <StickyAdUnit
                  dfpKeyword={activeSwellEvent.dfpKeyword}
                  isDesktop
                  qaFlag={qaFlag}
                  adUnit="swellEventRight2"
                  bottomBoundary=".sl-swell-event__content__related"
                  className="sl-swell-event-ad__right2"
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SwellEvent.fetch = [(props, { cookies }) => fetchSwellEvent(props.id, cookies)];

SwellEvent.propTypes = {
  activeSwellEvent: swellEventPropType.isRequired,
  doFetchSwellEvents: PropTypes.func.isRequired,
  isPremium: PropTypes.bool.isRequired,
  swellEvents: PropTypes.arrayOf(swellEventPropType),
  location: PropTypes.shape({
    search: PropTypes.string,
  }),
};

TaxonomyLink.propTypes = {
  taxonomy: PropTypes.shape({
    href: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
  }).isRequired,
  onClickRegionalForecast: PropTypes.func.isRequired,
};

SwellEvent.defaultProps = {
  swellEvents: [],
  location: {
    search: null,
  },
};

export default connect(
  (state) => ({
    isPremium: getUserPremiumStatus(state),
    activeSwellEvent: getActiveSwellEvent(state),
    swellEvents: getSwellEvents(state),
  }),
  (dispatch) => ({
    doFetchSwellEvents: () => dispatch(fetchSwellEvents()),
  }),
)(withRouter(SwellEvent));
