import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { withRouter } from 'react-router';
import {
  ContentCardPaywall,
  ContentContainer,
  FeedArticlesList,
  ForecasterProfile,
  PageRail,
  ShareArticle,
} from '@surfline/quiver-react';
import queryString from 'query-string';
import {
  canUseDOM,
  trackClickedSubscribeCTA,
  trackNavigatedToPage,
  trackEvent,
  pageStructuredData,
  getUserPremiumStatus,
  getUserSettings,
} from '@surfline/web-common';
import ReactDOMServer from 'react-dom/server';
import { Parser } from 'html-to-react';
import processForecastArticle from '../../../utils/processForecastArticle';
import ArticleMeta from '../../../components/ArticleMeta';
import StickyAdUnit from '../../../components/StickyAdUnit';
import {
  fetchForecastArticle,
  fetchForecastArticles,
  fetchSubregionForecast,
} from '../../../actions/forecast';
import forecastPropType from '../../../propTypes/forecast';
import userSettingsProptype from '../../../propTypes/userSettings';
import getSubregionForecast from '../../../selectors/getSubregionForecast';
import {
  renderJWElements,
  renderIgElements,
  renderTwitterElements,
  renderCameraEmbeds,
  renderResizedImages,
} from '../../../utils/renderCustomComponents';
import config from '../../../config';
import ForecastImg from './forecast_seo_img.jpg';

class ForecastArticle extends Component {
  constructor(props) {
    super(props);
    this.htmlToReactParser = new Parser();
    this.isValidNode = () => true;
  }

  componentDidMount() {
    const { doFetchSubregionForecast, userSettings, isPremium } = this.props;

    const {
      id,
      forecast: { article },
    } = this.props;

    const subregion = userSettings && userSettings.homeForecast ? userSettings.homeForecast : null;
    doFetchSubregionForecast(subregion);
    trackNavigatedToPage('Article', { category: 'article' });

    renderIgElements();
    renderTwitterElements();
    renderJWElements();
    renderCameraEmbeds(isPremium, id, article?.content?.title, 'article');

    renderResizedImages();
  }

  onClickShareArticle(shareChannel) {
    const {
      forecast: { article },
    } = this.props;

    trackEvent('Clicked Share Icon', {
      title: article.content.title,
      contentId: `${article.id}`,
      tags: article.tags.map((tag) => tag.name).join(','),
      locationCategory: 'Article Page',
      destinationUrl: article.permalink,
      mediaType: article.media ? article.media.type : null,
      shareChannel,
    });
  }

  onClickPaywall = () => {
    trackClickedSubscribeCTA({ location: 'Surf News Article' });
    if (canUseDOM) {
      setTimeout(() => window.location.assign(`${config.surflineHost}${config.funnelUrl}`), 300);
    }
  };

  render() {
    const {
      doFetchForecastArticles,
      forecast,
      isPremium,
      location: { search },
    } = this.props;
    const { qaFlag, native } = queryString.parse(search);
    const {
      activeArticle: { article },
      articles,
      overview,
    } = forecast;
    const helmetMeta = [
      { property: 'og:title', content: article.content.title },
      { property: 'twitter:title', content: article.content.title },
    ];

    const paywalled = !isPremium && article.premium && !native;

    return (
      <div className="sl-forecast-article">
        <Helmet meta={helmetMeta} title={article.content.title}>
          <script type="application/ld+json">
            {pageStructuredData({
              url: article.permalink,
              headline: article.content.title,
              image: ForecastImg,
              dateModified: article.updatedAt,
              author: article.author.name,
              description: article.content.subtitle || article.content.title,
              cssSelector: '.sl-forecast-article__content',
            })}
          </script>
        </Helmet>
        <ContentContainer>
          <PageRail side="left">
            <ArticleMeta
              expert={article.promoted ? article.promoted.includes('REGIONAL') : false}
              premium={article.premium}
              updatedAt={article.updatedAt}
            />
            <h1
              className="sl-forecast-article__title"
              dangerouslySetInnerHTML={{ __html: article.content.title }}
            />
            <ShareArticle
              url={article.permalink}
              onClickLink={(shareChannel) => this.onClickShareArticle(shareChannel)}
            />
            <div className="sl-forecast-article__break" />
            <ForecasterProfile
              forecasterName={article.author.name}
              url={article.author.iconUrl}
              small
            />
            <div className="sl-forecast-article__content-container">
              <div
                className="sl-forecast-article__content"
                /* eslint-disable-next-line react/no-danger */
                dangerouslySetInnerHTML={{
                  __html: ReactDOMServer.renderToStaticMarkup(
                    this.htmlToReactParser.parseWithInstructions(
                      article.content.body,
                      this.isValidNode,
                      processForecastArticle(),
                    ),
                  ),
                }}
              />
              {paywalled ? <div className="quiver-forecast-article-card__content-fade" /> : null}
            </div>
            {paywalled ? (
              <ContentCardPaywall
                funnelUrl={config.funnelUrl}
                onClick={() => this.onClickPaywall()}
              />
            ) : null}
          </PageRail>
          <PageRail side="right">
            {forecast.success ? (
              <div className="sl-forecast-article__forecast-articles">
                <FeedArticlesList
                  articles={articles}
                  doFetchForecastArticles={doFetchForecastArticles}
                  subregionId={overview._id}
                  subregionName={overview.name}
                  location="feed post page"
                />
              </div>
            ) : null}
            <StickyAdUnit isDesktop qaFlag={qaFlag} adUnit="stickyRightRail" />
          </PageRail>
        </ContentContainer>
      </div>
    );
  }
}

ForecastArticle.fetch = [(props, { cookies }) => fetchForecastArticle(props.id, cookies)];

ForecastArticle.propTypes = {
  id: PropTypes.string.isRequired,
  doFetchForecastArticles: PropTypes.func.isRequired,
  doFetchSubregionForecast: PropTypes.func.isRequired,
  forecast: forecastPropType.isRequired,
  isPremium: PropTypes.bool.isRequired,
  userSettings: userSettingsProptype,
  location: PropTypes.shape({
    search: PropTypes.string,
  }),
};

ForecastArticle.defaultProps = {
  userSettings: null,
  location: {
    search: null,
  },
};

export default connect(
  (state) => ({
    forecast: getSubregionForecast(state),
    isPremium: getUserPremiumStatus(state),
    userSettings: getUserSettings(state),
  }),
  (dispatch) => ({
    doFetchForecastArticles: (subregionId) => dispatch(fetchForecastArticles(subregionId)),
    doFetchSubregionForecast: (subregionId) => dispatch(fetchSubregionForecast(subregionId)),
  }),
)(withRouter(ForecastArticle));
