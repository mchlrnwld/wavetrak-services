import React, { useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Box, Skeleton, Typography, IconButton, useMediaQuery } from '@mui/material';
import useSWR from 'swr';
import { doExternalFetch } from '@surfline/web-common';
import ChevronLeft from '../SwellEventModal/ChevronLeft';
import ChevronRight from '../SwellEventModal/ChevronRight';

const ContentCarousel = ({
  background,
  carouselItem,
  dataTransform,
  fetchConfig,
  itemAspectRatio,
  numberOfitems,
  numberOfItemsCalculator,
  rawData,
  showPageCount,
  subtitle,
  title,
}) => {
  const [translation, setTranslation] = useState(0);

  const mediumScreen = useMediaQuery('(max-width:1200px)');
  const smallScreen = useMediaQuery('(max-width:1000px)');
  const mobileScreen = useMediaQuery('(max-width:600px)');

  const mobileScrollWrapper = useRef(null);
  const contentWrapper = useRef(null);
  const itemRef = useRef(null);

  const { data } = useSWR(
    fetchConfig?.endpoint,
    doExternalFetch,
    fetchConfig?.options || {
      revalidateOnFocus: false,
      revalidateOnMount: true,
      revalidateOnReconnect: false,
      refreshWhenOffline: false,
      refreshWhenHidden: false,
      refreshInterval: 0,
    },
  );

  const carouselData = rawData || dataTransform(data) || [];

  const itemsDisplayed = () => {
    if (numberOfItemsCalculator) return numberOfItemsCalculator();

    if (mobileScreen) return 1;
    if (smallScreen) return numberOfitems - 2;
    if (mediumScreen) return numberOfitems - 1;
    return numberOfitems;
  };

  const calcualtedItemsDisplayed = itemsDisplayed();

  const pages = Math.ceil(carouselData?.length / calcualtedItemsDisplayed);
  const currentPage = (translation / 100) * -1 + 1;

  const disableLeftButton = !carouselData?.length || (!mobileScreen && translation === 0);
  const disableRightButton =
    !carouselData?.length ||
    (!mobileScreen &&
      translation <= (carouselData?.length / calcualtedItemsDisplayed) * -100 + 100);

  /** Set transofrmX back to 0 when the cards change in display and size like in AirBnb */
  useEffect(() => {
    setTranslation(0);
  }, [calcualtedItemsDisplayed]);

  /** Scroll back to beginning when switching between mobile and non-mobile */
  useEffect(() => {
    mobileScrollWrapper?.current?.scrollTo(0, 0);
  }, [mobileScreen]);

  return (
    <div key={mobileScreen} className="sl-content-carousel-wrapper" style={{ background }}>
      <div className="sl-content-carousel-header">
        <div className="sl-content-carousel-header-text">
          <Typography variant="title2" className="sl-content-carousel-header-title">
            {title}
          </Typography>
          <Typography variant="body" className="sl-content-carousel-header-subtitle">
            {subtitle}
          </Typography>
        </div>
        {!mobileScreen && carouselData?.length && (
          <div className="sl-content-carousel-header-buttons">
            {showPageCount && (
              <Typography variant="subHeadline" className="sl-content-carousel-page-count">
                {currentPage} of {pages}
              </Typography>
            )}
            <IconButton
              onClick={() => setTranslation(translation + 100)}
              disabled={disableLeftButton}
            >
              <ChevronLeft />
            </IconButton>
            <IconButton
              onClick={() => setTranslation(translation - 100)}
              disabled={disableRightButton}
            >
              <ChevronRight />
            </IconButton>
          </div>
        )}
      </div>

      <div className="sl-content-carousel">
        <div className="sl-content-carousel-inner-wrapper">
          <div
            ref={mobileScrollWrapper}
            className="sl-content-carousel-mobile-wrapper"
            style={{ overflow: mobileScreen ? 'auto' : 'hidden' }}
          >
            <div
              className="sl-content-carousel-content-wrapper"
              ref={contentWrapper}
              style={{
                '--sl-card-aspect-ratio': `${itemAspectRatio}%`,
                transform: `translateX(${translation}%)`,
              }}
            >
              {carouselData?.length
                ? carouselData?.map((item) => (
                    <div
                      ref={itemRef}
                      className="sl-content-carousel-card-wrapper"
                      style={{
                        width: `calc(${100 / calcualtedItemsDisplayed}% - ${
                          mobileScreen ? '40px' : '16px'
                        })`,
                      }}
                    >
                      <div className="sl-content-carousel-card-inner-wrapper">
                        {carouselItem(itemRef, item)}
                      </div>
                    </div>
                  ))
                : new Array(numberOfitems).fill(1).map(() => {
                    return (
                      <div
                        ref={itemRef}
                        className="sl-content-carousel-card-wrapper"
                        style={{
                          width: `calc(${100 / calcualtedItemsDisplayed}% - ${
                            mobileScreen ? '40px' : '16px'
                          })`,
                        }}
                      >
                        <div className="sl-content-carousel-card-inner-wrapper">
                          <Box className="sl-content-carousel-skeleton-wrapper">
                            <Skeleton
                              className="sl-content-carousel-skeleton-item-big"
                              varaiant="rectangle"
                            />
                            <Skeleton className="sl-content-carousel-skeleton-item" />
                            <Skeleton className="sl-content-carousel-skeleton-item" width="60%" />
                          </Box>
                        </div>
                      </div>
                    );
                  })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

ContentCarousel.defaultProps = {
  background: '#171717',
  carouselItem: null,
  dataTransform: null,
  fetchConfig: null,
  itemAspectRatio: (1 / 1) * 100,
  numberOfitems: 2,
  numberOfItemsCalculator: null,
  rawData: null,
  showPageCount: false,
  subtitle: '',
  title: '',
};

ContentCarousel.propTypes = {
  background: PropTypes.string,
  carouselItem: PropTypes.node,
  dataTransform: PropTypes.func,
  fetchConfig: PropTypes.shapeOf({
    endpoint: PropTypes.string,
    options: PropTypes.objectOf,
  }),
  itemAspectRatio: PropTypes.string,
  numberOfitems: PropTypes.number,
  numberOfItemsCalculator: PropTypes.func,
  rawData: PropTypes.arrayOf(PropTypes.object),
  showPageCount: PropTypes.bool,
  subtitle: PropTypes.string,
  title: PropTypes.string,
};

export default ContentCarousel;
