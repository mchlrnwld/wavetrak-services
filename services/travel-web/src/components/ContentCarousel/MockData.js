export const mockData = [
  {
    travel_region: 'Costa Rica',
    title: 'Playa Grande surf camp',
    details: 'Surfing and fun in playa grande',
    reseller: {
      id: 202020202,
      name: 'Surfing and fun in playa grande',
    },
    price: {
      from: 1070,
      currency: 'USD',
    },
    link: 'https://www.surfline.com',
    type: 'camp',
    images: [
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/09132226/Matt-Mike-SDC00255-2.jpeg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/01/14142714/Screen-Shot-2021-01-03-at-9.57.21-PM.png',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/16090839/klein_JAWS_maui_Nov2021-0207.jpg',
      },
    ],
  },
  {
    travel_region: 'Costa Rica',
    title: 'Dreamsea surf camp',
    details: 'Live it up and party hard in the jungle by Tamarindo',
    reseller: {
      id: 202020202,
      name: 'Live it up and party hard in the jungle by Tamarindo',
    },
    price: {
      from: 100,
      currency: 'USD',
    },
    link: 'https://www.surfline.com',
    type: 'hotel',
    images: [
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/01/14161913/13400050135494724810.jpeg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/16090839/klein_JAWS_maui_Nov2021-0207.jpg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/01/14142714/Screen-Shot-2021-01-03-at-9.57.21-PM.png',
      },
    ],
  },
  {
    travel_region: 'Costa Rica',
    title: 'Tamarindo surf camp',
    details: 'Right on the beach camp to surf day and night',
    price: {
      from: 70,
      currency: 'USD',
    },
    reseller: {
      id: 202020202,
      name: 'Right on the beach camp to surf day and night',
    },
    link: 'https://www.surfline.com',
    type: 'camp',
    images: [
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/11134849/klein_hawaii_dec2015_004931.jpg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/16090839/klein_JAWS_maui_Nov2021-0207.jpg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/07/22125655/P9A5594-1560x1039.jpg',
      },
    ],
  },
  {
    travel_region: 'Costa Rica',
    title: 'San Jose urban surfing',
    details: 'Nice cool place for skating',
    reseller: {
      id: 202020202,
      name: 'Nice cool place for skating',
    },
    price: {
      from: 470,
      currency: 'USD',
    },
    link: 'https://www.surfline.com',
    type: 'camp',
    images: [
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/07/22125655/P9A5594-1560x1039.jpg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/04/20102129/096C3428-A482-472C-AFD7-880A8CC4F7BC.jpeg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/16090839/klein_JAWS_maui_Nov2021-0207.jpg',
      },
    ],
  },
  {
    travel_region: 'Costa Rica',
    title: 'Nosara rad peeps squad',
    details: 'Lets get crazy in Nosara and surf with pigs',
    reseller: {
      id: 202020202,
      name: 'Lets get crazy in Nosara and surf with pigs',
    },
    price: {
      from: 370,
      currency: 'USD',
    },
    link: 'https://www.surfline.com',
    type: 'hotel',
    images: [
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/04/20102129/096C3428-A482-472C-AFD7-880A8CC4F7BC.jpeg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/01/14142714/Screen-Shot-2021-01-03-at-9.57.21-PM.png',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/16090839/klein_JAWS_maui_Nov2021-0207.jpg',
      },
    ],
  },
  {
    travel_region: 'Costa Rica',
    title: 'Wowzer camp for you',
    details: 'Dope stuff here, only thing better than the cheese is the kangaroo sancutary',
    reseller: {
      id: 202020202,
      name: 'Dope stuff here, only thing better than the cheese is the kangaroo sancutary',
    },
    price: {
      from: 700,
      currency: 'USD',
    },
    type: 'camp',
    link: 'https://www.surfline.com',
    images: [
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/01/14142714/Screen-Shot-2021-01-03-at-9.57.21-PM.png',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/04/20102129/096C3428-A482-472C-AFD7-880A8CC4F7BC.jpeg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/16090839/klein_JAWS_maui_Nov2021-0207.jpg',
      },
    ],
  },
  {
    travel_region: 'Costa Rica',
    title: 'Playa Grande surf camp',
    details: 'Surfing and fun in playa grande',
    reseller: {
      id: 202020202,
      name: 'Surfing and fun in playa grande',
    },
    price: {
      from: 1070,
      currency: 'USD',
    },
    link: 'https://www.surfline.com',
    type: 'camp',
    images: [
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/09132226/Matt-Mike-SDC00255-2.jpeg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/01/14142714/Screen-Shot-2021-01-03-at-9.57.21-PM.png',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/16090839/klein_JAWS_maui_Nov2021-0207.jpg',
      },
    ],
  },
  {
    travel_region: 'Costa Rica',
    title: 'Dreamsea surf camp',
    details: 'Live it up and party hard in the jungle by Tamarindo',
    reseller: {
      id: 202020202,
      name: 'Live it up and party hard in the jungle by Tamarindo',
    },
    price: {
      from: 100,
      currency: 'USD',
    },
    link: 'https://www.surfline.com',
    type: 'hotel',
    images: [
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/01/14161913/13400050135494724810.jpeg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/16090839/klein_JAWS_maui_Nov2021-0207.jpg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/01/14142714/Screen-Shot-2021-01-03-at-9.57.21-PM.png',
      },
    ],
  },
  {
    travel_region: 'Costa Rica',
    title: 'Playa Grande surf camp',
    details: 'Surfing and fun in playa grande',
    reseller: {
      id: 202020202,
      name: 'Surfing and fun in playa grande',
    },
    price: {
      from: 8870,
      currency: 'USD',
    },
    link: 'https://www.surfline.com',
    type: 'camp',
    images: [
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/09132226/Matt-Mike-SDC00255-2.jpeg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/01/14142714/Screen-Shot-2021-01-03-at-9.57.21-PM.png',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/16090839/klein_JAWS_maui_Nov2021-0207.jpg',
      },
    ],
  },
  {
    travel_region: 'Costa Rica',
    title: 'Dreamsea surf camp',
    details: 'Live it up and party hard in the jungle by Tamarindo',
    reseller: {
      id: 202020202,
      name: 'Live it up and party hard in the jungle by Tamarindo',
    },
    price: {
      from: 0,
      currency: 'USD',
    },
    link: 'https://www.surfline.com',
    type: 'hotel',
    images: [
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/01/14161913/13400050135494724810.jpeg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/16090839/klein_JAWS_maui_Nov2021-0207.jpg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/01/14142714/Screen-Shot-2021-01-03-at-9.57.21-PM.png',
      },
    ],
  },
  {
    travel_region: 'Costa Rica',
    title: 'Playa Grande surf camp',
    details: 'Surfing and fun in playa grande',
    reseller: {
      id: 202020202,
      name: 'Surfing and fun in playa grande',
    },
    price: {
      from: 606,
      currency: 'USD',
    },
    link: 'https://www.surfline.com',
    type: 'camp',
    images: [
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/09132226/Matt-Mike-SDC00255-2.jpeg',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/01/14142714/Screen-Shot-2021-01-03-at-9.57.21-PM.png',
      },
      {
        src:
          'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/11/16090839/klein_JAWS_maui_Nov2021-0207.jpg',
      },
    ],
  },
];
