import PropTypes from 'prop-types';
import React from 'react';

const CircleArrow = ({ className }) => (
  <svg className={className} width="42px" height="42px" viewBox="0 0 42 42" version="1.1">
    <g id="Index-page-mvp" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g
        id="category-page---mobile"
        transform="translate(-262.000000, -1212.000000)"
        fillRule="nonzero"
        stroke="#0058B0"
      >
        <g id="Group-Copy" transform="translate(-768.000000, 554.000000)">
          <g id="Group-24-Copy-2" transform="translate(0.000000, 554.000000)">
            <g id="Group-6" transform="translate(0.000000, 28.000000)">
              <g id="Group-5" transform="translate(976.000000, 0.000000)">
                <g id="Group-2" transform="translate(52.000000, 77.000000)">
                  <g id="Group-7" transform="translate(3.000000, 0.000000)">
                    <rect id="Rectangle" x="0" y="0" width="40" height="40" rx="20" />
                    <g id="Group-3" transform="translate(14.000000, 16.000000)">
                      <g id="Group-4">
                        <polyline id="Path-2" points="7 0 11 4 7 8" />
                        <line x1="0" y1="4" x2="11" y2="4" id="Path" />
                      </g>
                    </g>
                  </g>
                </g>
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

CircleArrow.propTypes = {
  className: PropTypes.string,
};

CircleArrow.defaultProps = {
  className: '',
};

export default CircleArrow;
