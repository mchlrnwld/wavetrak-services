import PropTypes from 'prop-types';
import React from 'react';

const PlayMarker = ({ size }) => {
  if (size === 'small') {
    return (
      <svg className="sl-play-marker">
        <g>
          <circle cx="13" cy="13" r="13" />
          <polygon points="11.37 9.75, 16.25 13, 11.37 16.25" />
        </g>
      </svg>
    );
  }
  return (
    <svg className="sl-play-marker">
      <g>
        <circle cx="20" cy="20" r="20" />
        <polygon points="17.5 15, 25 20, 17.5 25" />
      </g>
    </svg>
  );
};

PlayMarker.propTypes = {
  size: PropTypes.string,
};

PlayMarker.defaultProps = {
  size: null,
};

export default PlayMarker;
