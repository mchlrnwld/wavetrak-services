import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { renderFeaturedVideo } from '../../utils/renderCustomComponents';

class FeaturedVideo extends Component {
  componentDidMount() {
    const { jwPlayerId } = this.props;
    renderFeaturedVideo(jwPlayerId);
  }

  render() {
    const { jwPlayerId, bannerImageUrl, onClickFeaturedVideo, location } = this.props;
    return (
      /* eslint-disable jsx-a11y/click-events-have-key-events */
      <div className="sl-featured-video__container" onClick={onClickFeaturedVideo}>
        <div
          id={`sl-swell-event-featured-media-${location}`}
          className="sl-swell-event-featured-media"
        >
          {!jwPlayerId && bannerImageUrl ? (
            <img
              className="sl-swell-event-featured-media__banner"
              src={bannerImageUrl}
              alt="bannerImage"
            />
          ) : null}
        </div>
      </div>
    );
  }
}

FeaturedVideo.propTypes = {
  jwPlayerId: PropTypes.string,
  bannerImageUrl: PropTypes.string,
  location: PropTypes.string,
  onClickFeaturedVideo: PropTypes.func,
};

FeaturedVideo.defaultProps = {
  jwPlayerId: '',
  bannerImageUrl: '',
  location: 'base',
  onClickFeaturedVideo: () => null,
};

export default FeaturedVideo;
