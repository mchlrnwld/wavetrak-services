import React from 'react';
import { Button, FeedCard } from '@surfline/quiver-react';
import { canUseDOM, trackEvent, trackClickedSubscribeCTA } from '@surfline/web-common';
import PRODUCT_CDN from '@surfline/quiver-assets';
import config from '../../config';
import PremiumPropType from '../../propTypes/premium';
import articlePropType from '../../propTypes/article';
import PremiumTagLarge from '../PremiumTagLarge';

const wavesUrl = `${PRODUCT_CDN}/backgrounds/paywall-background.png`;

const defaultMessaging = {
  paywallHeading: 'Get access to this story and more by going Premium',
  paywallDescription: `Dive deeper into our premium only stories, showcasing
  insights into surfing by top and emerging authors.\nYou'll also get access
  to 17-day forecasts, charts, premium cams, and more.`,
};

const onClickGoPremium = (article) => {
  const tags = [];
  article.tags.forEach((tag) => tags.push(tag.name));

  trackClickedSubscribeCTA({
    location: 'Premium Article',
    title: article.content.title,
    category: 'editorial',
    contentType: 'Editorial',
    contentId: article.id.toString(),
    tags,
    mediaType: article.media.type,
  });

  if (canUseDOM) {
    setTimeout(() => window.location.assign(`${config.surflineHost}${config.funnelUrl}`), 300);
  }
};

const onClickSignIn = () => {
  sessionStorage.setItem('redirectUrl', window.location.href);
};

const EditorialPaywall = ({ article, premium }) => (
  <div className="sl-editorial-paywall">
    <div
      className="sl-editorial-paywall__container"
      style={{ backgroundImage: `url(${wavesUrl})` }}
    >
      <PremiumTagLarge className="sl-editorial-paywall__tag" />
      <h4 className="sl-editorial-paywall__header">
        {premium.paywallHeading || defaultMessaging.paywallHeading}
      </h4>
      <p className="sl-editorial-paywall__description">
        {premium.paywallDescription || defaultMessaging.paywallDescription}
      </p>
      <Button onClick={() => onClickGoPremium(article)}>Start Free Trial</Button>
      <p className="sl-editorial-paywall__cta">
        Already a Premium subscriber?{' '}
        <a href={config.signInUrl} onClick={onClickSignIn} className="sl-editorial-paywall__login">
          Sign In
        </a>
      </p>
      <h6 className="sl-editorial-paywall__heading">MORE PREMIUM ARTICLES</h6>
      <div className="sl-editorial-paywall__related__wrapper">
        <div className="sl-editorial-paywall__related">
          <div className="sl-editorial-paywall__related__list">
            {premium.paywallPosts.slice(0, 3).map((post) => (
              <FeedCard
                category={post.series[0] || post.categories[0]}
                imageUrl={post.media ? post.media.feed2x : null}
                createdAt={post.createdAt}
                permalink={post.permalink}
                title={post.title}
                subtitle={post.subtitle}
                isPremium={post.premium}
                onClickLink={() =>
                  trackEvent('Clicked Link', {
                    linkUrl: post.permalink,
                    category: 'editorial',
                    linkLocation: 'premium related article',
                    linkName: post.title,
                    articleId: post.id,
                  })
                }
                onClickTag={() =>
                  trackEvent('Clicked Link', {
                    linkUrl: post.permalink,
                    category: 'editorial',
                    linkLocation: 'premium related article tag',
                    linkName: post.title,
                    articleId: post.id,
                  })
                }
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  </div>
);

EditorialPaywall.propTypes = {
  article: articlePropType.isRequired,
  premium: PremiumPropType.isRequired,
};

export default EditorialPaywall;
