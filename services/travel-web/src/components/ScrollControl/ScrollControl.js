import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import Chevron from '../Chevron';

const getClassNames = (size, direction) =>
  classNames({
    'sl-scroll-control': true,
    'sl-scroll-control--tall': size === 'tall',
    'sl-scroll-control--left': direction === 'left',
    'sl-scroll-control--right': direction === 'right',
  });

const ScrollControl = ({ direction, size, clickHandler }) => (
  /* eslint-disable jsx-a11y/click-events-have-key-events */
  <div className={getClassNames(size, direction)} onClick={clickHandler}>
    <Chevron direction={direction} />
  </div>
);

ScrollControl.propTypes = {
  direction: PropTypes.string.isRequired,
  clickHandler: PropTypes.func,
  size: PropTypes.string,
};

ScrollControl.defaultProps = {
  clickHandler: null,
  size: null,
};

export default ScrollControl;
