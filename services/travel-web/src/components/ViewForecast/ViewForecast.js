import React from 'react';
import taxonomyPropType from '../../propTypes/taxonomy';

const ViewForecast = ({ taxonomy }) => {
  const { name, associated } = taxonomy;
  return (
    <div className="sl-view-forecast">
      <div className="sl-view-forecast__container">
        <h5>
          {name} <span>Surf Report</span>
        </h5>
        <p>
          See the forecast for <span>{name}</span>
        </p>
        <div className="sl-view-forecast__container__link">
          {associated &&
            associated.links &&
            associated.links.map((link) => {
              if (link.key === 'www') {
                return (
                  <a href={link.href} key={link.key}>
                    View Surf Spots
                  </a>
                );
              }
              return null;
            })}
        </div>
      </div>
    </div>
  );
};

ViewForecast.propTypes = {
  taxonomy: taxonomyPropType.isRequired,
};

export default ViewForecast;
