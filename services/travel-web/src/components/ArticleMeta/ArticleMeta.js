import PropTypes from 'prop-types';
import React from 'react';
import { formatArticleDate } from '@surfline/web-common';

const ArticleMeta = ({ expert, premium, updatedAt }) => (
  <div className="sl-article-meta">
    {`${premium ? 'Premium ' : ''} ${expert ? 'Expert Forecast •' : ''} `}
    <span className="sl-article-meta__date">{formatArticleDate(updatedAt)}</span>
  </div>
);

ArticleMeta.propTypes = {
  expert: PropTypes.bool.isRequired,
  premium: PropTypes.bool.isRequired,
  updatedAt: PropTypes.number.isRequired,
};

export default ArticleMeta;
