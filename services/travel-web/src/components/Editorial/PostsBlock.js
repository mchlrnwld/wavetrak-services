import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FeedCard } from '@surfline/quiver-react';
import CircleArrow from '../CircleArrow';
import resizeImage from '../../utils/resizeImage';

const PostsBlock = ({ name, posts, slug, taxonomy, term, imageResizing }) => {
  const imageSettings = 'w=740,q=85,f=auto,fit=contain';
  const category = posts.length > 0 && posts[0].categories.filter((cat) => cat.name === name)[0];
  return (
    <div className="sl-tax-cat-block">
      <h2>{name}</h2>
      <Link className="sl-tax-cat-block__view-all" to={`/${taxonomy}/${term}/${slug}`}>
        View All {name}
      </Link>
      <div className="sl-tax-cat-block__posts">
        <div className="sl-tax-cat-block__posts__cards">
          {posts.map((post) => {
            const timestamp = Math.floor(new Date(post.createdAt) / 1000);
            return (
              <FeedCard
                isPremium={post.premium}
                category={category}
                imageUrl={resizeImage(post?.media?.feed2x || '', imageResizing, imageSettings)}
                title={post.title}
                createdAt={timestamp}
                permalink={post.permalink}
                showTimestamp={!Number.isNaN(Number(timestamp))}
              />
            );
          })}
          <Link
            className="sl-tax-cat-block__posts__cards__view-all"
            to={`/${taxonomy}/${term}/${slug}`}
          >
            <div className="sl-tax-cat-block__posts__cards__view-all__arrow">
              <CircleArrow />
              <br />
              View All
            </div>
          </Link>
        </div>
      </div>
    </div>
  );
};

PostsBlock.propTypes = {
  name: PropTypes.string.isRequired,
  posts: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      title: PropTypes.string,
      subtitle: PropTypes.string,
      premium: PropTypes.bool,
      permalink: PropTypes.string,
      createdAt: PropTypes.string,
      categories: PropTypes.arrayOf(
        PropTypes.shape({
          name: PropTypes.string,
          slug: PropTypes.string,
          url: PropTypes.string,
          imageUrl: PropTypes.string,
          taxonomy: PropTypes.string,
        }),
      ),
      media: PropTypes.shape({
        type: PropTypes.string,
        feed1x: PropTypes.string,
        feed2x: PropTypes.string,
      }),
    }),
  ).isRequired,
  slug: PropTypes.string.isRequired,
  taxonomy: PropTypes.string.isRequired,
  term: PropTypes.string.isRequired,
  imageResizing: PropTypes.bool.isRequired,
};

export default PostsBlock;
