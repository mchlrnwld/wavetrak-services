import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { Waypoint } from 'react-waypoint';
import { Button } from '@surfline/quiver-react';
import {
  fetchEditorialTaxonomyPosts,
  startInfiniteScrolling,
} from '../../actions/editorialTaxonomyPosts';

const LoadMorePosts = ({
  infiniteScrollingEnabled,
  doFetchTaxonomyPosts,
  doStartInfiniteScrolling,
  taxonomy,
  term,
  subCategory,
}) => (
  <div className="sl-load-more-posts">
    {infiniteScrollingEnabled ? (
      <Waypoint onEnter={() => doFetchTaxonomyPosts(taxonomy, term, subCategory)} />
    ) : (
      <Button onClick={() => doStartInfiniteScrolling()}>Load More</Button>
    )}
  </div>
);

LoadMorePosts.propTypes = {
  doStartInfiniteScrolling: PropTypes.func.isRequired,
  doFetchTaxonomyPosts: PropTypes.func.isRequired,
  infiniteScrollingEnabled: PropTypes.bool.isRequired,
  term: PropTypes.string,
  taxonomy: PropTypes.string,
  subCategory: PropTypes.string,
};

LoadMorePosts.defaultProps = {
  term: '',
  taxonomy: '',
  subCategory: '',
};

export default connect(
  (state) => ({
    infiniteScrollingEnabled: state.editorialTaxonomyPosts.infiniteScroll,
  }),
  (dispatch) => ({
    doFetchTaxonomyPosts: (taxonomy, term, subCategory) =>
      dispatch(fetchEditorialTaxonomyPosts(taxonomy, term, subCategory)),
    doStartInfiniteScrolling: () => dispatch(startInfiniteScrolling()),
  }),
)(LoadMorePosts);
