import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-scroll';
import { get as _get } from 'lodash';
import breadCrumbsProps from '../../propTypes/breadCrumbs';
import travelContentProps from '../../propTypes/travelContent';
import taxonomyProps from '../../propTypes/taxonomy';
import resizeImage from '../../utils/resizeImage';
import Breadcrumb from '../Breadcrumb';
import config from '../../config';

const Hero = ({ travelContent, taxonomy, breadCrumbs, imageResizing }) => {
  const getBackgroundImage = () => {
    if (_get(travelContent, 'media.large', null)) {
      return travelContent.media.large;
    }
    return config.defaultImageUrl;
  };
  return (
    <div
      className="sl-hero"
      style={{
        backgroundImage: `url("${resizeImage(getBackgroundImage(), imageResizing)}")`,
      }}
    >
      <div className="sl-hero__content">
        <Breadcrumb breadCrumbs={breadCrumbs.slice(0, -1)} baseUrl="/travel" />
        <h1>{taxonomy.name} Travel &amp; Surf Guide</h1>
        <div className="sl-hero__content__subtitle">
          <h2 className="sl-hero__content__subtitle__h2">
            {_get(travelContent, 'content.subtitle', null)
              ? travelContent.content.subtitle
              : 'Get the best tips on local beaches, travel seasons and surf spots'}
          </h2>
        </div>
      </div>
      <Link className="sl-hero__anchor" to="sl-travel-anchor" duration={300} smooth>
        <div className="sl-hero__anchor__icon" />
      </Link>
    </div>
  );
};

Hero.propTypes = {
  travelContent: travelContentProps,
  taxonomy: taxonomyProps.isRequired,
  breadCrumbs: breadCrumbsProps.isRequired,
  imageResizing: PropTypes.bool.isRequired,
};

Hero.defaultProps = {
  travelContent: null,
};

export default Hero;
