import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { distanceInWordsStrict } from 'date-fns';
import { Button as QuiverButton } from '@surfline/quiver-react';
import ClockIcon from '../ClockIcon';
import relatedArticlesPropType from '../../propTypes/relatedArticles';
import resizeImage from '../../utils/resizeImage';

class RelatedArticles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seeMore: false,
    };
  }

  getRelatedArticlesClasses = (seeMore) =>
    classnames({
      'sl-related__articles': true,
      'sl-related__articles--open': seeMore,
    });

  getSeeMoreClasses = (seeMore) =>
    classnames({
      'sl-related__more': true,
      'sl-related__more--open': seeMore,
    });

  toggleSeeMore = () => {
    this.setState({
      seeMore: true,
    });
  };

  render() {
    const { relatedArticles, imageResizing } = this.props;
    const { seeMore } = this.state;
    return (
      <div className="sl-related">
        <h3>Related Articles</h3>
        <div
          className={
            relatedArticles.length > 2
              ? this.getRelatedArticlesClasses(seeMore)
              : 'sl-related__articles sl-related__articles--open'
          }
        >
          {relatedArticles.slice(0, 6).map((article) => (
            <a
              href={article.permalink}
              key={article.id}
              className="sl-related__articles__article"
              target={article.newWindow ? '_blank' : '_self'}
              rel="noreferrer"
            >
              <div
                className="sl-related__articles__article__img"
                style={{
                  backgroundImage: `url(${resizeImage(
                    article.media?.feed1x || '',
                    imageResizing,
                  )})`,
                }}
              />
              <div className="sl-related__articles__article__content">
                <div className="sl-related__articles__article__content__top">
                  {article.tags.length && <a href={article.tags[0].url}>{article.tags[0].name}</a>}
                  <div className="sl-related__articles__article__content__top__time">
                    <ClockIcon className="sl-related__articles__article__content__top__time__clock" />
                    {`${distanceInWordsStrict(article.createdAt * 1000, new Date())} ago`}
                    {article.createdAt !== article.updatedAt &&
                    article.externalLink !== 'External URL' ? (
                      <div className="sl-related__articles__article__content__top__time__updated">
                        <span>Updated</span>
                        {` ${distanceInWordsStrict(article.updatedAt * 1000, new Date())} ago`}
                      </div>
                    ) : null}
                    {article.externalLink === 'External URL' ? (
                      <div className="sl-related__articles__article__content__top__time__updated">
                        {`Via ${article.externalSource}`}
                      </div>
                    ) : null}
                  </div>
                </div>
                <h4>{article.content.title}</h4>
              </div>
            </a>
          ))}
        </div>
        {relatedArticles.length > 2 ? (
          <div className={this.getSeeMoreClasses(seeMore)}>
            <QuiverButton type="info" onClick={() => this.toggleSeeMore()}>
              Load More
            </QuiverButton>
          </div>
        ) : null}
      </div>
    );
  }
}

RelatedArticles.propTypes = {
  relatedArticles: relatedArticlesPropType.isRequired,
  imageResizing: PropTypes.bool.isRequired,
};

export default RelatedArticles;
