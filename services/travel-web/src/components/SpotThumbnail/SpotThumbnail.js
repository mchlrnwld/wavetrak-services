import PropTypes from 'prop-types';
import React from 'react';
import { WindIndicator } from '@surfline/quiver-react';
import unitsPropType from '../../propTypes/units';
import PlayMarker from '../PlayMarker';
import LockIcon from '../LockIcon';

const SpotThumbnail = ({ windDirection, windSpeed, units, hasCamera, isPremium }) => (
  <div className="sl-spot-thumbnail">
    <div className="sl-spot-thumbnail__top-container">
      <WindIndicator direction={windDirection} speed={windSpeed} units={units.windSpeed} />
      {!isPremium ? <LockIcon /> : null}
    </div>
    <div className="sl-spot-thumbnail__bottom-container">{hasCamera ? <PlayMarker /> : null}</div>
  </div>
);

SpotThumbnail.propTypes = {
  windDirection: PropTypes.number.isRequired,
  windSpeed: PropTypes.number.isRequired,
  units: unitsPropType.isRequired,
  isPremium: PropTypes.bool.isRequired,
  hasCamera: PropTypes.bool,
};

SpotThumbnail.defaultProps = {
  hasCamera: false,
};

export default SpotThumbnail;
