import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardMedia, Typography, Link } from '@mui/material';

const OverlayCard = ({ data, className, innerRef, styles }) => {
  const { title, subtitle, caption, link, src } = data;

  return (
    <Card className={`sl-card-wrapper ${className}`} ref={innerRef} sx={styles}>
      <div className="sl-content-card-overlay">
        <CardMedia
          component="img"
          height="100%"
          width="100%"
          src={src}
          alt={title || 'travel location'}
        />
        <Link className="sl-content-card-overlay-link" href={link} underline="none">
          {title && (
            <Typography variant="title1" className="sl-content-card-overlay-title">
              {title}
            </Typography>
          )}
          {subtitle && (
            <Typography variant="callout" className="sl-content-card-overlay-subtitle">
              {subtitle}
            </Typography>
          )}
          {caption && (
            <Typography variant="callout2" className="sl-content-card-overlay-caption">
              {caption}
            </Typography>
          )}
        </Link>
      </div>
    </Card>
  );
};

const defaultData = {
  link: 'https://www.surfline.com',
  src:
    'https://d2pn5sjh0l9yc4.cloudfront.net/wp-content/uploads/2021/01/14142714/Screen-Shot-2021-01-03-at-9.57.21-PM.png',
  title: 'North Costa Rica, Costa Rica',
  subtitle: 'Jan 1, 2022',
  caption: '6 episodes',
};

OverlayCard.defaultProps = {
  className: '',
  styles: {},
  data: defaultData,
  innerRef: null,
};

OverlayCard.propTypes = {
  className: PropTypes.string,
  data: PropTypes.shapeOf({
    caption: PropTypes.string,
    subtitle: PropTypes.string,
    src: PropTypes.string,
    title: PropTypes.string,
    link: PropTypes.string,
  }),
  innerRef: PropTypes.node,
  styles: PropTypes.objectOf,
};

export default OverlayCard;
