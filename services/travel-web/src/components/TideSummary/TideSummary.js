import PropTypes from 'prop-types';
import React from 'react';
import { format } from 'date-fns';
import { getLocalDate } from '@surfline/web-common';
import capitalizeFirstLetter from '../../utils/capitalizeFirstLetter';
import ReadingDescription from '../ReadingDescription';

const summary = (tide, units) => {
  if (!tide) return 'No Tide Data Available';

  const time = format(getLocalDate(tide.timestamp, tide.utcOffset), 'hh:mma');
  return `${capitalizeFirstLetter(tide.type)} tide ${tide.height}${units.toLowerCase()} at ${time}`;
};

const TideSummary = ({ tide, units }) => (
  <ReadingDescription>{summary(tide, units)}</ReadingDescription>
);

TideSummary.propTypes = {
  units: PropTypes.string.isRequired,
  tide: PropTypes.shape({
    type: PropTypes.string,
    height: PropTypes.number,
    timestamp: PropTypes.number,
    utcOffset: PropTypes.number,
  }),
};

TideSummary.defaultProps = {
  tide: null,
};

export default TideSummary;
