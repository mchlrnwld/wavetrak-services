import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { SpotListItem } from '@surfline/quiver-react';
import { slugify, kbygPaths } from '@surfline/web-common';
import taxonomyPropType from '../../propTypes/taxonomy';
import topSpotsPropType from '../../propTypes/topSpots';
import unitsPropType from '../../propTypes/units';
import ScrollControl from '../ScrollControl';
import { showPremiumCamCTA } from '../../utils/premiumCam';

class TopSpots extends Component {
  componentDidUpdate() {
    const { scrollLeft } = this.props;
    this.spotList.scrollLeft = scrollLeft;
  }

  scrollLeft() {
    const { doSetTopSpotsPosition } = this.props;
    const left = this.spotList.scrollLeft - this.spotList.clientWidth;
    doSetTopSpotsPosition({ top: 0, left });
  }

  scrollRight() {
    const { doSetTopSpotsPosition } = this.props;
    const left = this.spotList.scrollLeft + this.spotList.clientWidth;
    doSetTopSpotsPosition({ top: 0, left });
  }

  render() {
    const { taxonomy, topSpots, units, isPremium } = this.props;
    return (
      <div className="sl-top-spots">
        <h3 className="sl-top-spots__title">Top Surf Spots &amp; Beaches in {taxonomy.name}</h3>
        <div className="sl-top-spots__container">
          <ScrollControl size="tall" direction="left" clickHandler={() => this.scrollLeft()} />
          <div
            ref={(el) => {
              this.spotList = el;
            }}
            className="sl-top-spots__container__list"
          >
            {topSpots.map((spot) => (
              <a
                href={kbygPaths.spotReportPath(slugify(spot.name), spot._id)}
                className="sl-top-spots__link"
              >
                <SpotListItem
                  spot={spot}
                  units={units}
                  showPremiumCamCTA={showPremiumCamCTA({
                    spot,
                    isPremium,
                  })}
                  mobileCard
                />
              </a>
            ))}
          </div>
          <ScrollControl size="tall" direction="right" clickHandler={() => this.scrollRight()} />
        </div>
      </div>
    );
  }
}

TopSpots.propTypes = {
  topSpots: topSpotsPropType.isRequired,
  taxonomy: taxonomyPropType.isRequired,
  units: unitsPropType.isRequired,
  doSetTopSpotsPosition: PropTypes.func.isRequired,
  scrollLeft: PropTypes.number,
  isPremium: PropTypes.bool.isRequired,
};

TopSpots.defaultProps = {
  scrollLeft: 0,
};

export default TopSpots;
