import React, { Component, createRef } from 'react';
import ReactDOMServer from 'react-dom/server';
import PropTypes from 'prop-types';
import { Parser } from 'html-to-react';
import { ShareArticle } from '@surfline/quiver-react';
import { canUseDOM, trackClickedSubscribeCTA } from '@surfline/web-common';
import articlePropType from '../../propTypes/article';
import { articleProcessingInstructions } from './helpers';
import {
  renderArticleAds,
  renderIgElements,
  renderJWElements,
  trackArticleLinkClicks,
  renderVideoPaywalls,
  renderCalendarEvents,
  renderCalendarCinemaEvents,
  renderCameraEmbeds,
  renderTwitterElements,
  renderSurfNetworkVideo,
  renderResizedImages,
  renderTravelCardCarousel,
} from '../../utils/renderCustomComponents';
import config from '../../config';
import EditorialPaywall from '../EditorialPaywall';

class EditorialArticleBody extends Component {
  constructor(props) {
    super(props);
    this.articleRef = createRef();
    this.htmlToReactParser = new Parser();
    this.isValidNode = () => true;
  }

  componentDidMount() {
    const {
      article: {
        id,
        createdAt,
        sponsoredArticle: { dfpKeyword },
        displayOptions: { hideAds },
        premium: { premium },
        content: { title },
      },
      qaFlag,
      isPremium,
      native,
      imageResizing,
    } = this.props;
    const children = [...this.articleRef.current.children];
    if (!hideAds && (dfpKeyword || !isPremium) && !premium) {
      renderArticleAds(this.articleRef, children, dfpKeyword, createdAt, qaFlag);
    }
    renderIgElements();
    renderTwitterElements();
    renderJWElements();
    trackArticleLinkClicks(id);

    // Render video paywalls if premium conditions are met and split is on
    const paywall = premium && !isPremium && !native;
    renderVideoPaywalls(this.doTrackVideoPaywallClicks, this.onClickPaywallSignIn, paywall);
    renderCalendarEvents(id);
    renderCalendarCinemaEvents(id);
    renderCameraEmbeds(isPremium, id, title, 'editorial');
    renderSurfNetworkVideo();
    renderTravelCardCarousel();
    if (imageResizing) renderResizedImages();
  }

  onClickPaywallSignIn = () => {
    sessionStorage.setItem('redirectUrl', window.location.href);
  };

  doTrackVideoPaywallClicks = () => {
    const { article } = this.props;

    const tags = [];
    article.tags.forEach((tag) => tags.push(tag.name));
    trackClickedSubscribeCTA({
      location: 'Premium Article - Video Paywall',
      title: article.content.title,
      category: 'editorial',
      contentType: 'Editorial',
      contentId: article.id.toString(),
      tags,
      mediaType: article.media.type,
    });
    if (canUseDOM) {
      setTimeout(() => window.location.assign(`${config.surflineHost}${config.funnelUrl}`), 300);
    }
  };

  render() {
    const { article, isPremium, onClickLink, native } = this.props;
    const paywalled = article.premium.premium && !isPremium && !native;
    const bodyContent = paywalled ? article.premium.teaser : article.content.body;
    return (
      <div className={paywalled ? 'paywall' : null}>
        <div className="sl-editorial-article-body__container">
          <div
            ref={this.articleRef}
            id="sl-editorial-article-body"
            className="sl-editorial-article-body"
            /* eslint-disable-next-line react/no-danger */
            dangerouslySetInnerHTML={{
              __html: ReactDOMServer.renderToStaticMarkup(
                this.htmlToReactParser.parseWithInstructions(
                  bodyContent,
                  this.isValidNode,
                  articleProcessingInstructions(isPremium),
                ),
              ),
            }}
          />
          {paywalled ? <div className="sl-editorial-paywall__fade" /> : null}
        </div>
        {!paywalled && article.tags.length ? (
          <ul className="sl-article-tags">
            {article.tags.map((tag) => (
              <li key={tag.name}>
                <a href={tag.url}>{tag.name}</a>
              </li>
            ))}
          </ul>
        ) : null}
        {!paywalled ? (
          <div className="sl-editorial-article-bottom__social">
            <hr />
            <h6>Share this story</h6>
            <ShareArticle url={article.permalink} onClickLink={onClickLink} />
          </div>
        ) : (
          <EditorialPaywall article={article} premium={article.premium} />
        )}
      </div>
    );
  }
}

EditorialArticleBody.propTypes = {
  article: articlePropType.isRequired,
  isPremium: PropTypes.bool,
  qaFlag: PropTypes.string,
  onClickLink: PropTypes.func.isRequired,
  native: PropTypes.bool,
  imageResizing: PropTypes.bool.isRequired,
};

EditorialArticleBody.defaultProps = {
  isPremium: false,
  qaFlag: null,
  native: false,
};

export default EditorialArticleBody;
