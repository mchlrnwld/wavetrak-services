import React from 'react';
import PropTypes from 'prop-types';
import { format } from 'date-fns';
import articlePropType from '../../propTypes/article';
import swellEventPropType from '../../propTypes/swellEvent';

const compareHighlightDays = (date, prevDate) =>
  format(date * 1000, 'MMM D') === format(prevDate * 1000, 'MMM D');

/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
const SwellEventHighlights = ({
  highlights,
  highlightsLoaded,
  loadMoreHighlights,
  swellEvent,
  onClickHighlight,
}) => (
  <div className="sl-swell-event-highlights">
    <h6>{swellEvent.name} Highlights</h6>
    <ul>
      {highlights.map((highlight, index) => (
        <li key={highlight.id} onClick={() => onClickHighlight(highlight)}>
          {index === 0 ||
          !compareHighlightDays(highlight.updatedAt, swellEvent.updates[index - 1].updatedAt) ? (
            <span className="sl-swell-event-highlights__date">
              {format(highlight.updatedAt * 1000, 'MMM D')}
            </span>
          ) : null}
          <a className="sl-swell-event-highlights__highlight" href={`#${highlight.id}`}>
            <span className="sl-swell-event-highlights__time">
              {format(highlight.updatedAt * 1000, 'h:mma')}
            </span>
            <span
              className="sl-swell-event-highlights__title"
              /* eslint-disable-next-line react/no-danger */
              dangerouslySetInnerHTML={{ __html: highlight.title }}
            />
          </a>
        </li>
      ))}
    </ul>
    {!highlightsLoaded && highlights.length < swellEvent.updates.length ? (
      <div className="sl-swell-event-highlights__load-more" onClick={() => loadMoreHighlights()}>
        Load More
      </div>
    ) : null}
  </div>
);
/* eslint-enable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-enable jsx-a11y/click-events-have-key-events */

SwellEventHighlights.propTypes = {
  highlights: PropTypes.arrayOf(articlePropType).isRequired,
  highlightsLoaded: PropTypes.bool.isRequired,
  loadMoreHighlights: PropTypes.func.isRequired,
  swellEvent: swellEventPropType.isRequired,
  onClickHighlight: PropTypes.func.isRequired,
};

export default SwellEventHighlights;
