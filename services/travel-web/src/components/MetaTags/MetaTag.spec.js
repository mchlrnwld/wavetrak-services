import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Helmet from 'react-helmet';
import MetaTags from './MetaTags';
import config from '../../config';

describe('components / MetaTags', () => {
  const title = 'Surfline Travel Info for Every Beach in the World';
  const description =
    "Know Before You Go. Get the best travel and weather info along with live HD beach cams. Access the world's best surf forecast team at Surfline.";
  const urlPath = 'travel';
  const image = 'https://camstills.cdn-surfline.com/hbpiersscam/latest_small.jpg';
  const url = `${config.surflineHost}/${urlPath}`;
  const seoUrl = `${config.hostForSEO}/${urlPath}`;
  const tags = [
    {
      name: 'Tag One',
      slug: 'tag-one',
      url: 'https://sandbox.surfline.com/tag/tag-one',
      taxonomy: 'post_tag',
    },
  ];

  it('renders react-helmet with appropriate props', () => {
    const wrapper = shallow(
      <MetaTags
        title={title}
        description={description}
        image={image}
        urlPath={urlPath}
        tags={tags}
      />,
    );
    const helmet = wrapper.find(Helmet);

    expect(helmet).to.have.length(1);
    expect(helmet.prop('title')).to.equal(`${title} - Surfline`);

    const meta = helmet.prop('meta');
    if (config.robots) {
      expect(meta).to.have.length(19);
    } else {
      expect(meta).to.have.length(18);
    }
    expect(meta.find((m) => m.name === 'description')).to.deep.equal({
      name: 'description',
      content: description,
    });
    expect(meta.find((m) => m.property === 'og:title')).to.deep.equal({
      property: 'og:title',
      content: title,
    });
    expect(meta.find((m) => m.property === 'og:url')).to.deep.equal({
      property: 'og:url',
      content: url,
    });
    expect(meta.find((m) => m.property === 'og:image')).to.deep.equal({
      property: 'og:image',
      content: image,
    });
    expect(meta.find((m) => m.property === 'og:description')).to.deep.equal({
      property: 'og:description',
      content: description,
    });
    expect(meta.find((m) => m.property === 'twitter:title')).to.deep.equal({
      property: 'twitter:title',
      content: title,
    });
    expect(meta.find((m) => m.property === 'twitter:url')).to.deep.equal({
      property: 'twitter:url',
      content: url,
    });
    expect(meta.find((m) => m.property === 'twitter:image')).to.deep.equal({
      property: 'twitter:image',
      content: image,
    });
    expect(meta.find((m) => m.property === 'twitter:description')).to.deep.equal({
      property: 'twitter:description',
      content: description,
    });

    const link = helmet.prop('link');
    expect(link).to.have.length(2);
    expect(link.find((l) => l.rel === 'canonical')).to.deep.equal({
      rel: 'canonical',
      href: seoUrl,
    });
  });

  it('defaults image to Facebook OG image', () => {
    const wrapper = shallow(<MetaTags title={title} description={description} urlPath={urlPath} />);
    const helmet = wrapper.find(Helmet);

    expect(helmet).to.have.length(1);

    const meta = helmet.prop('meta');
    expect(meta.find((m) => m.property === 'og:image')).to.deep.equal({
      property: 'og:image',
      content: '/facebook-og-default.png',
    });
    expect(meta.find((m) => m.property === 'twitter:image')).to.deep.equal({
      property: 'twitter:image',
      content: '/facebook-og-default.png',
    });
  });
});
