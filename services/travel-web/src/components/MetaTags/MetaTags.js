import PropTypes from 'prop-types';
import React from 'react';
import { Helmet } from 'react-helmet';
import config from '../../config';
import yoastPropType from '../../propTypes/yoastMeta';
import { travelPaths } from '../../utils/urls/travelPath';

const HelmetWrapper = ({
  title,
  description,
  urlPath,
  image,
  yoastMeta,
  tags,
  permalink,
  children,
}) => {
  const seoUrl = `${config.hostForSEO}/${urlPath}`;
  const url = permalink || `${config.surflineHost}/${urlPath}`;

  const meta = [
    {
      name: 'description',
      content: yoastMeta ? yoastMeta['og:description'] : description,
    },
    {
      name: 'og:type',
      content: 'Article',
    },
    {
      name: 'og:site_name',
      content: yoastMeta ? yoastMeta['og:site_name'] : 'Surfline',
    },
    {
      property: 'og:title',
      content: yoastMeta ? yoastMeta['og:title'] : title,
    },
    {
      property: 'og:url',
      content: yoastMeta ? yoastMeta['og:url'] : url,
    },
    {
      property: 'og:image',
      content: yoastMeta ? yoastMeta['og:image'] : image,
    },
    {
      property: 'og:description',
      content: yoastMeta ? yoastMeta['og:description'] : description,
    },
    {
      property: 'og:updated_time',
      content: yoastMeta ? yoastMeta['og:updated_time'] : null,
    },
    {
      property: 'canonical',
      content: yoastMeta ? yoastMeta.canonical : url,
    },
    {
      property: 'article:section',
      content: yoastMeta ? yoastMeta['article:section'] : null,
    },
    {
      property: 'article:published_time',
      content: yoastMeta ? yoastMeta['article:published_time'] : null,
    },
    {
      property: 'article:updated_time',
      content: yoastMeta ? yoastMeta['article:updated_time'] : null,
    },
    {
      property: 'twitter:card',
      content: yoastMeta ? yoastMeta['twitter:card'] : null,
    },
    {
      property: 'twitter:title',
      content: yoastMeta ? yoastMeta['twitter:title'] : title,
    },
    {
      property: 'twitter:image',
      content: yoastMeta ? yoastMeta['twitter:image'] : image,
    },
    {
      property: 'twitter:description',
      content: yoastMeta ? yoastMeta['twitter:description'] : description,
    },
    {
      property: 'twitter:url',
      content: yoastMeta ? yoastMeta['twitter:url'] : url,
    },
  ];

  tags.forEach((tag) => {
    meta.push({
      property: 'article:tag',
      content: tag.name,
    });
  });

  const robotsMeta = config.robots
    ? {
        name: 'robots',
        content: config.robots,
      }
    : null;

  if (robotsMeta) {
    meta.push(robotsMeta);
  }

  const link = permalink
    ? [
        {
          rel: 'canonical',
          href: permalink,
        },
        {
          rel: 'amphtml',
          href: `${permalink}/amp`,
        },
      ]
    : [
        {
          rel: 'canonical',
          href: seoUrl,
        },
        {
          rel: 'amphtml',
          href: `${seoUrl}/amp`,
        },
      ];

  return (
    <Helmet title={`${title} - Surfline`} meta={meta} link={link}>
      {children}
    </Helmet>
  );
};

HelmetWrapper.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  permalink: PropTypes.string,
  urlPath: PropTypes.string,
  image: PropTypes.string,
  yoastMeta: yoastPropType,
  tags: PropTypes.arrayOf(PropTypes.string),
  children: PropTypes.shape({}),
};

HelmetWrapper.defaultProps = {
  image: config.facebookOGImage,
  permalink: null,
  description: '',
  urlPath: travelPaths.base,
  yoastMeta: null,
  tags: [],
  children: null,
};

export default HelmetWrapper;
