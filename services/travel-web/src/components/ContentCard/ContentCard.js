import React from 'react';
import PropTypes from 'prop-types';
import { Card, Tooltip, CardContent, CardActions, Link, Typography } from '@mui/material';
import { useTheme } from '@mui/system';
import CardCarousel from '../CardCarousel';
import HotelIcon from './HotelIcon';
import CampIcon from './CampIcon';

const ContentCard = ({ data, className, innerRef, styles }) => {
  const cardData = { ...data, region: data.travel_region };
  const { region, reseller, price, link, images, type } = cardData;

  const theme = useTheme();
  const imageSources = images?.map((image) => image.src);

  const getResortIcon = (supplierType) => {
    if (!supplierType) return <HotelIcon />;

    switch (supplierType?.trim()?.toLowerCase()) {
      case 'camp':
        return <CampIcon />;
      case 'hotel':
        return <HotelIcon />;
      default:
        return <HotelIcon />;
    }
  };

  return (
    <Card className={`sl-card-wrapper ${className}`} ref={innerRef} sx={styles}>
      <div className="sl-content-card-detailed">
        <CardCarousel images={imageSources} />
        <CardContent sx={{ display: 'flex', padding: '6% 20px' }}>
          <div className="sl-content-card-detailed-details">
            <Typography sx={{ lineHeight: '20px' }} variant="footnote">
              {region}
            </Typography>
            <div className="sl-content-card-detailed-resort">
              <Tooltip arrow placement="top" title={reseller?.name}>
                <Typography variant="title3">{reseller?.name}</Typography>
              </Tooltip>
            </div>
          </div>
          {getResortIcon(type)}
        </CardContent>
        <CardActions sx={{ position: 'absolute', bottom: '20px', left: '12px' }}>
          <Typography variant="subHeadline">
            <Link sx={{ color: theme?.palette?.primary?.main }} href={link} underline="none">
              From {price?.from} {price?.currency} / night
            </Link>
          </Typography>
        </CardActions>
      </div>
    </Card>
  );
};

ContentCard.defaultProps = {
  className: '',
  styles: {},
  data: null,
  innerRef: null,
};

ContentCard.propTypes = {
  className: PropTypes.string,
  data: PropTypes.shape({
    travel_region: PropTypes.string,
    link: PropTypes.string,
    images: PropTypes.arrayOf(PropTypes.object),
    price: PropTypes.shape({ currency: PropTypes.string, from: PropTypes.number }),
    reseller: PropTypes.shape({ id: PropTypes.number, name: PropTypes.string }),
    type: PropTypes.string,
  }),
  innerRef: PropTypes.node,
  /* eslint-disable-next-line */
  styles: PropTypes.any,
};

export default ContentCard;
