import React from 'react';
import travelContentProps from '../../propTypes/travelContent';

const Summary = ({ travelContent }) => (
  <div
    className="sl-travel-summary"
    /* eslint-disable-next-line react/no-danger */
    dangerouslySetInnerHTML={{ __html: travelContent.content.body }}
  />
);

Summary.propTypes = {
  travelContent: travelContentProps.isRequired,
};

export default Summary;
