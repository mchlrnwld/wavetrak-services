import PropTypes from 'prop-types';
import React from 'react';
import Sticky from 'react-stickynode';
import { GoogleDFP } from '@surfline/quiver-react';
import loadAdConfig from '../../utils/adConfig';
import config from '../../config';

const StickyAdUnit = ({
  isDesktop,
  qaFlag,
  adUnit,
  bottomBoundary,
  dfpKeyword,
  className,
  activeClass,
}) => {
  const feedAdUnit = (
    <div className="sl-sticky-ad-unit">
      <GoogleDFP
        adConfig={loadAdConfig(adUnit, [
          ['qaFlag', qaFlag],
          ['contentKeyword', dfpKeyword],
        ])}
        isHtl
        isTesting={config.htl.isTesting}
      />
    </div>
  );
  return isDesktop ? (
    <Sticky activeClass={activeClass} className={className} bottomBoundary={bottomBoundary}>
      {feedAdUnit}
    </Sticky>
  ) : (
    feedAdUnit
  );
};

StickyAdUnit.propTypes = {
  activeClass: PropTypes.string,
  className: PropTypes.string,
  isDesktop: PropTypes.bool,
  qaFlag: PropTypes.string,
  adUnit: PropTypes.string,
  bottomBoundary: PropTypes.string,
  dfpKeyword: PropTypes.string,
};

StickyAdUnit.defaultProps = {
  activeClass: '',
  className: '',
  isDesktop: false,
  qaFlag: null,
  adUnit: null,
  bottomBoundary: '.quiver-page-rail--right',
  dfpKeyword: null,
};

export default StickyAdUnit;
