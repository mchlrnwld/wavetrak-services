import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { throttle as _throttle } from 'lodash';
import taxonomyPropType from '../../propTypes/taxonomy';
import childrenPropType from '../../propTypes/children';
import canUseDOM from '../../common/canUseDom';
import resizeImage from '../../utils/resizeImage';
import config from '../../config';

class ChildGrid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMore: false,
    };
  }

  componentDidMount() {
    if (canUseDOM) {
      this.handleWindowResize();
      window.addEventListener('resize', _throttle(this.handleWindowResize, 250));
    }
  }

  componentWillUnmount() {
    if (canUseDOM) window.removeEventListener('resize', _throttle(this.handleWindowResize, 250));
  }

  getGridClasses = (showMore) =>
    classnames({
      'sl-child-grid__grid': true,
      'sl-child-grid__grid--open': showMore,
    });

  getShowMoreClasses = (showMore) =>
    classnames({
      'sl-child-grid__more': true,
      'sl-child-grid__more--hidden': showMore,
    });

  getChildClass = (child) =>
    classnames({
      'sl-child-grid__grid__child': true,
      'sl-child-grid__grid__child--image': !!child.media && !!child.media.medium,
    });

  handleWindowResize = () => {
    const {
      geoChildren: { length },
    } = this.props;
    const windowWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    if (length < 3 && windowWidth < config.childGridTwoCol) {
      this.setState({
        showMore: true,
      });
    } else if (
      length < 5 &&
      windowWidth >= config.childGridTwoCol &&
      windowWidth < config.childGridThreeCol
    ) {
      this.setState({
        showMore: true,
      });
    } else if (length < 7 && windowWidth >= config.childGridThreeCol) {
      this.setState({
        showMore: true,
      });
    }
  };

  toggleGridHeight = () => {
    const { showMore } = this.state;
    this.setState({
      showMore: !showMore,
    });
  };

  render() {
    const { taxonomy, geoChildren, imageResizing } = this.props;
    const { showMore } = this.state;
    return (
      <div className="sl-child-grid">
        <h3>
          Areas within <span>{taxonomy.name}</span>
        </h3>
        <div className={this.getGridClasses(showMore)}>
          {geoChildren.map((child) => (
            <a
              href={child.url}
              key={child.url}
              style={{
                backgroundImage: `url('${resizeImage(child.media?.medium || '', imageResizing)}')`,
              }}
              className={this.getChildClass(child)}
            >
              <h6>
                {child.name} <span>Travel &amp; Surf Guide</span>
              </h6>
            </a>
          ))}
        </div>
        {/* eslint-disable jsx-a11y/click-events-have-key-events */}
        <div onClick={() => this.toggleGridHeight()} className={this.getShowMoreClasses(showMore)}>
          <span>See More</span>
        </div>
      </div>
    );
  }
}

ChildGrid.propTypes = {
  taxonomy: taxonomyPropType.isRequired,
  geoChildren: childrenPropType.isRequired,
  imageResizing: PropTypes.bool.isRequired,
};

export default ChildGrid;
