import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Modal from '@mui/material/Modal';
import PrismaZoom from 'react-prismazoom';
import IconButton from '@mui/material/IconButton';
import ChevronRight from './ChevronRight';
import ChevronLeft from './ChevronLeft';
import { ModalCloseButton } from './SwellEventModalButton';

const SwellEventModal = (props) => {
  const { articleImageIndex, articleImages, setOpen, open } = props;
  const [currentImage, setCurrentImage] = useState(articleImages[articleImageIndex]);

  const handleClose = () => setOpen(false);

  const nextImage = () =>
    currentImage?.index < articleImages.length - 1 &&
    setCurrentImage(articleImages[currentImage?.index + 1]);

  const previousImage = () =>
    currentImage?.index > 0 && setCurrentImage(articleImages[currentImage?.index - 1]);

  const greaterWidth = window?.innerWidth >= window?.innerHeight;
  const zoomContentStyles = {
    width: currentImage.w >= currentImage.h && !greaterWidth && '100%',
    height: currentImage.h > currentImage.w && !greaterWidth && '100%',
    maxWidth: '100%',
    maxHeight: '100%',
    pointerEvents: 'all',
  };

  return (
    <Modal
      open={open}
      onClose={handleClose}
      className="swell-event__modal"
      aria-labelledby="swell-event__modal__title"
      aria-describedby="swell-event__modal__description"
    >
      <div className="swell-event__modal__content">
        <div className="prisma-zoom__wrapper" style={{ alignItems: !greaterWidth && 'center' }}>
          <PrismaZoom key={currentImage.src} style={zoomContentStyles}>
            <img src={currentImage.src} alt="Modal article" style={zoomContentStyles} />
          </PrismaZoom>
        </div>

        <div className="swell-event__modal__buttons">
          <div className="carousel-button-wrapper">
            <IconButton
              className="carousel-button"
              onClick={previousImage}
              disabled={currentImage?.index === 0}
            >
              <ChevronLeft />
            </IconButton>
          </div>
          <div className="carousel-button-wrapper">
            <IconButton
              className="carousel-button"
              onClick={nextImage}
              disabled={currentImage?.index >= articleImages.length - 1}
            >
              <ChevronRight />
            </IconButton>
          </div>
        </div>
        <ModalCloseButton onClick={handleClose} />
      </div>
    </Modal>
  );
};

SwellEventModal.propTypes = {
  articleImages: PropTypes.oneOfType([{ index: PropTypes.number, src: PropTypes.string }])
    .isRequired,
  setOpen: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  articleImageIndex: PropTypes.number.isRequired,
};

export default SwellEventModal;
