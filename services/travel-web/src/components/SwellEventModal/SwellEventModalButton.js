import React from 'react';
import PropTypes from 'prop-types';
import CloseIcon from './CloseIcon';

export const ModalCloseButton = (props) => {
  const { onClick, className } = props;
  return (
    <button
      className={`sl-swell-event__modal__button ${className}`}
      onClick={onClick}
      type="button"
    >
      <CloseIcon />
    </button>
  );
};

ModalCloseButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string,
};

ModalCloseButton.defaultProps = {
  className: '',
};
