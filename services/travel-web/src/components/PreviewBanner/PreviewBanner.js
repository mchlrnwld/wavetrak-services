import React from 'react';
import articlePropType from '../../propTypes/article';

const PreviewBanner = ({ article }) => (
  <a className="sl-preview-banner" href={`/wp-admin/post.php?post=${article.id}&action=edit`}>
    Edit Post
  </a>
);

PreviewBanner.propTypes = {
  article: articlePropType.isRequired,
};

export default PreviewBanner;
