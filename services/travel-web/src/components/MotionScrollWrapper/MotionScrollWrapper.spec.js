import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount, shallow } from 'enzyme';
import { Motion } from 'react-motion';
import MotionScrollWrapper from './MotionScrollWrapper';

describe('components / MotionScrollWrapper', () => {
  const Children = () => (
    <div className="children">This should render inside of MotionScrollWrapper.</div>
  );
  const scrollPosition = { top: 10, left: 15 };

  it('passes interpolated scroll positions and force scrolling flag to children', () => {
    const wrapper = mount(
      <MotionScrollWrapper scrollPosition={scrollPosition}>
        <Children />
      </MotionScrollWrapper>,
    );
    const childrenWrapper = wrapper.find(Children);
    expect(childrenWrapper).to.have.length(1);
    expect(childrenWrapper.prop('scrollTop')).to.equal(10);
    expect(childrenWrapper.prop('scrollLeft')).to.equal(15);
    expect(childrenWrapper.prop('forceScrolling')).to.be.true();
  });

  it('simply renders children if disabled', () => {
    const wrapper = mount(
      <MotionScrollWrapper scrollPosition={scrollPosition} disable>
        <Children />
      </MotionScrollWrapper>,
    );
    const childrenWrapper = wrapper.find(Children);
    expect(childrenWrapper).to.have.length(1);
    expect(childrenWrapper.props()).to.deep.equal({});
  });

  it('passes onRest function into Motion wrapper', () => {
    const onRest = sinon.spy();
    const wrapper = shallow(
      <MotionScrollWrapper scrollPosition={scrollPosition} onRest={onRest} />,
    );
    const motionWrapper = wrapper.find(Motion);
    expect(motionWrapper.prop('onRest')).to.equal(onRest);
  });
});
