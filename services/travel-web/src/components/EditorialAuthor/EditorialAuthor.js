import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { distanceInWordsToNow, format } from 'date-fns';
import { GoogleDFP, ShareArticle } from '@surfline/quiver-react';
import loadAdConfig from '../../utils/adConfig';
import config from '../../config';

const getAuthorSocialClasses = (hideAuthor) =>
  classnames({
    'sl-editorial-author__social': true,
    'sl-editorial-author__social--author': !hideAuthor,
  });

const EditorialAuthor = ({
  author,
  createdAt,
  hideAuthor,
  onClickLink,
  sponsoredArticle,
  updatedAt,
  url,
  dateFormat,
}) => {
  const defaultUrl = 'https://www.gravatar.com/avatar/d41d8cd98f00b204e9800998ecf8427e?d=mm';
  const date = dateFormat === 'MDY' ? 'MMM Do, YYYY' : 'Do, MMM YYYY';
  return (
    <div className="sl-editorial-author__container">
      {sponsoredArticle.dfpKeyword ? (
        <div className="sl-editorial-author__presented-by">
          <span>{sponsoredArticle.attributionText}</span>
          <GoogleDFP
            adConfig={loadAdConfig('sponsoredArticleLogo', [
              ['contentKeyword', sponsoredArticle.dfpKeyword],
            ])}
            isHtl
            isTesting={config.htl.isTesting}
          />
        </div>
      ) : null}
      <div className="sl-editorial-author__author">
        {!hideAuthor ? (
          <div className="sl-editorial-author">
            <img
              className="sl-editorial-author__image"
              src={`${author.iconUrl || defaultUrl}`}
              alt={author.name || 'Surfline Avatar'}
            />
            <div className="sl-editorial-author__details">
              <div className="sl-editorial-author__details__name">{author.name || null}</div>
              <div className="sl-editorial-author__details__date">
                {`${format(createdAt, date)}. `}
                {createdAt !== updatedAt ? (
                  <span>
                    <span className="sl-editorial-author__details__date__updated">Updated</span>
                    {` ${distanceInWordsToNow(updatedAt)} ago.`}
                  </span>
                ) : null}
              </div>
            </div>
          </div>
        ) : null}
        <div className={getAuthorSocialClasses(hideAuthor)}>
          <ShareArticle url={url} onClickLink={onClickLink} />
        </div>
      </div>
    </div>
  );
};

EditorialAuthor.propTypes = {
  author: PropTypes.shape({
    iconUrl: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
  createdAt: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.string]).isRequired,
  hideAuthor: PropTypes.bool,
  updatedAt: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.string]).isRequired,
  url: PropTypes.string.isRequired,
  onClickLink: PropTypes.func.isRequired,
  sponsoredArticle: PropTypes.shape({
    attributionText: PropTypes.string,
    dfpKeyword: PropTypes.string,
  }).isRequired,
  dateFormat: PropTypes.oneOf(['MDY', 'DMY']),
};

EditorialAuthor.defaultProps = {
  hideAuthor: false,
  dateFormat: 'MDY',
};

export default EditorialAuthor;
