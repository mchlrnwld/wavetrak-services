import React from 'react';
import articlePropType from '../../propTypes/article';

const EditorialArticleHeading = ({ article }) => (
  <div className="sl-editorial-article-heading">
    <div className="sl-editorial-article-heading__taxonomy">
      {article.sponsoredArticle.partnerContent ? (
        <span className="sl-editorial-article-heading__partner">Partner Content</span>
      ) : null}
      {article.series.length ? <a href={article.series[0].url}>{article.series[0].name}</a> : null}
      {article.categories.length ? (
        <a href={article.categories[0].url}>{article.categories[0].name}</a>
      ) : null}
    </div>
    <h1 className="sl-editorial-article-heading__title">{article.content.title}</h1>
    <h2 className="sl-editorial-article-heading__subtitle">{article.content.subtitle}</h2>
  </div>
);

EditorialArticleHeading.propTypes = {
  article: articlePropType.isRequired,
};

export default EditorialArticleHeading;
