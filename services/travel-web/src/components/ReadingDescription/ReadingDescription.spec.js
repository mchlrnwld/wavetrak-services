import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ReadingDescription from './ReadingDescription';

describe('components / ReadingDescription', () => {
  it('renders children in a span', () => {
    const wrapper = shallow(
      <ReadingDescription>
        <h1>Description text.</h1>
      </ReadingDescription>,
    );
    expect(wrapper.find('span').children().html()).to.equal('<h1>Description text.</h1>');
  });
});
