import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import breadCrumbsPropType from '../../propTypes/breadCrumbs';

const Breadcrumb = ({ baseUrl, breadCrumbs }) => (
  <ul className="sl-breadcrumb">
    {/* eslint-disable jsx-a11y/anchor-is-valid */}
    {breadCrumbs.map((breadcrumb) => (
      <li key={breadcrumb.name}>
        {baseUrl ? (
          <Link to={`${baseUrl}${breadcrumb.url}`}>{breadcrumb.name}</Link>
        ) : (
          <a>{breadcrumb.name}</a>
        )}
      </li>
    ))}
    {/* eslint-enable jsx-a11y/anchor-is-valid */}
  </ul>
);

Breadcrumb.propTypes = {
  breadCrumbs: breadCrumbsPropType.isRequired,
  baseUrl: PropTypes.string,
};

Breadcrumb.defaultProps = {
  baseUrl: '',
};

export default Breadcrumb;
