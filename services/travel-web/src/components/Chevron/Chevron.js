import PropTypes from 'prop-types';
import React from 'react';

const Chevron = ({ direction }) => {
  const points = direction === 'left' ? '7 0, 0 6.5, 7 13' : '0 0, 7 6.5, 0 13';
  return (
    <svg width="7px" height="13px" viewBox="0 0 7 13">
      <polyline points={points} fill="none" strokeLinejoin="round" />
    </svg>
  );
};

Chevron.propTypes = {
  direction: PropTypes.string.isRequired,
};

export default Chevron;
