import React, { Component } from 'react';
import canUseDom from '../../common/canUseDom';
import animationData from './animationData.json';

let Lottie;

if (canUseDom) {
  // eslint-disable-next-line global-require
  Lottie = require('react-lottie').default;
}

const options = {
  loop: true,
  autoplay: true,
  animationData,
  rendererSettings: {
    clearCanvas: true,
  },
};

class PageLoading extends Component {
  componentDidMount() {
    if (!canUseDom) return;
    this.footer = document.getElementById('backplane-footer');
    if (this.footer) this.footer.style.display = 'none';
  }

  componentWillUnmount() {
    if (!canUseDom) return;
    if (this.footer) this.footer.style.display = null;
  }

  render() {
    return (
      <div className="sl-page-loading">
        {Lottie ? <Lottie options={options} width={64} height={64} /> : null}
      </div>
    );
  }
}

export default PageLoading;
