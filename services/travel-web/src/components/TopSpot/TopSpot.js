import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import LazyLoad from 'react-lazyload';
import { SurfConditions, SurfHeight } from '@surfline/quiver-react';
import spotPropType from '../../propTypes/spot';
import unitsPropType from '../../propTypes/units';
import SpotThumbnail from '../SpotThumbnail';
import TideSummary from '../TideSummary';
import { spotForecastPath } from '../../utils/urls';
import checkConditionValue from '../../utils/checkConditionValue';

const getClassName = (activeSpotId, spotId) =>
  classNames({
    'sl-top-spot': true,
    'sl-top-spot--active': activeSpotId === spotId,
  });

const getBackgroundImg = (spot) => (spot.cameras[0] ? spot.cameras[0].stillUrl : spot.thumbnail);

const TopSpot = ({ spot, units, activeSpotId, onMouseEnter, onMouseLeave, isPremium }) => (
  <a
    href={`/${spotForecastPath(spot)}`}
    className={getClassName(activeSpotId, spot._id)}
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
  >
    <div className="sl-top-spot__container">
      <LazyLoad>
        <div
          style={{ backgroundImage: `url('${getBackgroundImg(spot)}')`, backgroundSize: 'cover' }}
        >
          <SpotThumbnail
            windDirection={spot.wind.direction}
            windSpeed={spot.wind.speed}
            units={units}
            hasCamera={spot.cameras.length > 0}
            isPremium={isPremium}
          />
        </div>
      </LazyLoad>
      <div className="sl-top-spot__details">
        <div className="sl-top-spot__name">{spot.name}</div>
        <SurfConditions conditions={checkConditionValue(spot.conditions.value)} />
        <SurfHeight
          min={spot.waveHeight.min}
          max={spot.waveHeight.max}
          units={units.waveHeight}
          plus={spot.waveHeight.plus}
        />
        <div className="sl-top-spot__tide-summary">
          <TideSummary tide={spot.tide && spot.tide.next} units={units.tideHeight} />
        </div>
      </div>
    </div>
  </a>
);

TopSpot.propTypes = {
  spot: spotPropType.isRequired,
  units: unitsPropType.isRequired,
  activeSpotId: PropTypes.string,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  isPremium: PropTypes.bool.isRequired,
};

TopSpot.defaultProps = {
  activeSpotId: null,
  onMouseEnter: () => null,
  onMouseLeave: () => null,
};

export default TopSpot;
