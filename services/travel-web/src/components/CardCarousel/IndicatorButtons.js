import React from 'react';
import PropTypes from 'prop-types';

const IndicatorButtons = ({ images, currentImageIndex, next }) => {
  return (
    <div className="sl-indicator-buttons-wrapper">
      {images.map((__, index) => (
        <button
          className={`sl-indicator-button ${
            currentImageIndex === index ? 'sl-indicator-button-active' : ''
          }`}
          type="button"
          onClick={() => next(index)}
          aria-label={`image ${index} button`}
        />
      ))}
    </div>
  );
};

IndicatorButtons.propTypes = {
  currentImageIndex: PropTypes.number.isRequired,
  images: PropTypes.arrayOf({ src: PropTypes.string }).isRequired,
  next: PropTypes.func.isRequired,
};

export default IndicatorButtons;
