import React, { useState } from 'react';
import PropTypes from 'prop-types';
import IconButton from '@mui/material/IconButton';
import ChevronLeft from '../SwellEventModal/ChevronLeft';
import ChevronRight from '../SwellEventModal/ChevronRight';
import IndicatorButtons from './IndicatorButtons';

const CardCarousel = ({ images }) => {
  const [currentImage, setCurrentImage] = useState(images[0]);

  const next = () => {
    if (!images[images.indexOf(currentImage) + 1]) return;
    setCurrentImage(images[images.indexOf(currentImage) + 1]);
  };

  const previous = () => {
    if (!images[images.indexOf(currentImage) - 1]) return;
    setCurrentImage(images[images.indexOf(currentImage) - 1]);
  };

  const navigateImage = (index) => {
    setCurrentImage(images[index]);
  };

  return (
    <div className="sl-card-carousel">
      <div
        style={{
          height: '100%',
          width: '100%',
          backgroundSize: 'cover',
          backgroundImage: `url(${currentImage})`,
          backgroundPosition: 'center',
        }}
      />
      <div className="sl-card-carousel-previous-button-wrapper" style={{ left: 0 }}>
        <IconButton
          className="sl-card-carousel-previous-button"
          disabled={images.indexOf(currentImage) === 0}
          onClick={previous}
        >
          <ChevronLeft />
        </IconButton>
      </div>
      <div className="sl-card-carousel-next-button-wrapper" style={{ right: 0 }}>
        <IconButton
          className="sl-card-carousel-next-button"
          disabled={images.indexOf(currentImage) === images.length - 1}
          onClick={next}
        >
          <ChevronRight />
        </IconButton>
      </div>
      <IndicatorButtons
        images={images}
        currentImageIndex={images.indexOf(currentImage)}
        next={navigateImage}
      />
    </div>
  );
};

CardCarousel.propTypes = {
  images: PropTypes.arrayOf(PropTypes.shape({ src: PropTypes.string })).isRequired,
};

export default CardCarousel;
