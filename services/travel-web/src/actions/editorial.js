import { get as _get } from 'lodash';
import * as editorialAPI from '../common/api/editorial';
import { NOT_FOUND } from './status';
import sanitizeArticleHTML from '../utils/sanitizeArticleHTML';
import getFeedGeotarget from '../utils/getFeedGeotarget';

export const FETCH_EDITORIAL_ARTICLE = 'FETCH_EDITORIAL_ARTICLE';
export const FETCH_EDITORIAL_ARTICLE_SUCCESS = 'FETCH_EDITORIAL_ARTICLE_SUCCESS';
export const FETCH_EDITORIAL_ARTICLE_FAILURE = 'FETCH_EDITORIAL_ARTICLE_FAILURE';

export const fetchEditorialArticle = (id, wpnonce = null, cookies = null) => async (
  dispatch,
  getState,
) => {
  dispatch({
    type: FETCH_EDITORIAL_ARTICLE,
  });

  try {
    const state = getState();
    const geotarget = getFeedGeotarget(state);
    const article = await editorialAPI.fetchEditorialArticleById(id, wpnonce, geotarget, cookies);
    dispatch({
      type: FETCH_EDITORIAL_ARTICLE_SUCCESS,
      article: {
        ...article,
        premium: {
          ...article.premium,
          teaser: article.premium.teaser ? sanitizeArticleHTML(article.premium.teaser) : null,
        },
        content: {
          ...article.content,
          body: sanitizeArticleHTML(article.content.body),
        },
      },
    });
  } catch (error) {
    const wpStatusCode = _get(error, 'response.status', null);
    if (error.statusCode === 400 || wpStatusCode === 404) {
      dispatch({
        type: NOT_FOUND,
        message: 'The article was not found',
      });
    } else {
      dispatch({
        type: FETCH_EDITORIAL_ARTICLE_FAILURE,
        error,
      });
    }
  }
};
