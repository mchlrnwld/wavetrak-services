import { fetchLocationViewWorld } from '../common/api/locationView';

export const FETCH_WORLD_TAXONOMY_SUCCESS = 'FETCH_WORLD_TAXONOMY_SUCCESS';

export const fetchWorldTaxonomy = (_, { cookies }) => async (dispatch) => {
  // If this fails we'll handle the error at the router level and send them a 500
  const { data } = await fetchLocationViewWorld(cookies);
  dispatch({ type: FETCH_WORLD_TAXONOMY_SUCCESS, taxonomy: data });
};
