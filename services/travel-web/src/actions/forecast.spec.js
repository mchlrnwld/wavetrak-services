import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import sinon from 'sinon';
import * as feedApi from '../common/api/feed';
import {
  FETCH_FORECAST_ARTICLES,
  FETCH_FORECAST_ARTICLES_SUCCESS,
  FETCH_FORECAST_ARTICLES_FAILURE,
  fetchForecastArticles,
} from './forecast';

describe('actions / forecast', () => {
  let mockStore;
  before(() => {
    mockStore = configureStore([thunk]);
  });

  describe('fetchForecastArticles', () => {
    beforeEach(() => {
      sinon.stub(feedApi, 'fetchRegionalArticles');
    });

    afterEach(() => {
      feedApi.fetchRegionalArticles.restore();
    });

    it('dispatches FETCH_FORECAST_ARTICLES with articles', async () => {
      const store = mockStore({
        forecast: {
          loading: false,
          error: false,
          success: false,
          associated: null,
          overview: null,
          subregions: [],
          conditions: null,
          articles: {
            regional: [],
            error: null,
            loading: false,
            nextStart: null,
            pageCount: 0,
          },
        },
      });
      const articles = {
        associated: {},
        data: {},
      };
      feedApi.fetchRegionalArticles.resolves(articles);
      const expectedActions = [
        {
          type: FETCH_FORECAST_ARTICLES,
        },
        {
          type: FETCH_FORECAST_ARTICLES_SUCCESS,
          articles,
        },
      ];
      await store.dispatch(fetchForecastArticles());
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(feedApi.fetchRegionalArticles).to.have.been.calledOnce();
    });

    it('dispatches FETCH_FORECAST_ARTICLES_FAILURE with error', async () => {
      const store = mockStore({
        forecast: {
          loading: false,
          error: false,
          success: false,
          associated: null,
          overview: null,
          subregions: [],
          conditions: null,
          articles: {
            regional: [],
            error: null,
            loading: false,
            nextStart: null,
            pageCount: 0,
          },
        },
      });
      feedApi.fetchRegionalArticles.rejects({ message: 'This is an error', status: 500 });

      const expectedActions = [
        {
          type: FETCH_FORECAST_ARTICLES,
        },
        {
          type: FETCH_FORECAST_ARTICLES_FAILURE,
          error: { message: 'This is an error', status: 500 },
        },
      ];
      await store.dispatch(fetchForecastArticles());
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(feedApi.fetchRegionalArticles).to.have.been.calledOnce();
    });
  });
});
