import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import sinon from 'sinon';
import * as editorialAPI from '../common/api/editorial';
import {
  FETCH_EDITORIAL_ARTICLE,
  FETCH_EDITORIAL_ARTICLE_SUCCESS,
  FETCH_EDITORIAL_ARTICLE_FAILURE,
  fetchEditorialArticle,
} from './editorial';
import editorialArticle from './fixtures/editorialArticle.json';

describe('actions / editorial', () => {
  let mockStore;
  before(() => {
    mockStore = configureStore([thunk]);
  });

  describe('fetchEditorialArticle', () => {
    beforeEach(() => {
      sinon.stub(editorialAPI, 'fetchEditorialArticleById');
    });

    afterEach(() => {
      editorialAPI.fetchEditorialArticleById.restore();
    });

    it('dispatches FETCH_EDITORIAL_ARTICLE with articles', async () => {
      const store = mockStore({
        editorial: {
          loading: false,
          error: false,
          success: false,
          article: null,
        },
      });
      editorialAPI.fetchEditorialArticleById.resolves(editorialArticle);
      const expectedActions = [
        {
          type: FETCH_EDITORIAL_ARTICLE,
        },
        {
          type: FETCH_EDITORIAL_ARTICLE_SUCCESS,
          article: editorialArticle,
        },
      ];
      await store.dispatch(fetchEditorialArticle());
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(editorialAPI.fetchEditorialArticleById).to.have.been.calledOnce();
    });

    it('dispatches FETCH_EDITORIAL_ARTICLE_FAILURE with error', async () => {
      const store = mockStore({
        forecast: {
          loading: false,
          error: false,
          success: false,
          article: null,
        },
      });
      editorialAPI.fetchEditorialArticleById.rejects({ message: 'This is an error', status: 500 });

      const expectedActions = [
        {
          type: FETCH_EDITORIAL_ARTICLE,
        },
        {
          type: FETCH_EDITORIAL_ARTICLE_FAILURE,
          error: { message: 'This is an error', status: 500 },
        },
      ];
      await store.dispatch(fetchEditorialArticle());
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(editorialAPI.fetchEditorialArticleById).to.have.been.calledOnce();
    });
  });
});
