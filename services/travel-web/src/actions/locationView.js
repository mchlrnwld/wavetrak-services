import { fetchLocationViewTravel } from '../common/api/locationView';
import { NOT_FOUND } from './status';

export const FETCH_LOCATION_VIEW = 'FETCH_LOCATION_VIEW';
export const FETCH_LOCATION_VIEW_SUCCESS = 'FETCH_LOCATION_VIEW_SUCCESS';
export const FETCH_LOCATION_VIEW_FAILURE = 'FETCH_LOCATION_VIEW_FAILURE';

export const fetchLocationView = (geonameId, cookies) => async (dispatch) => {
  dispatch({
    type: FETCH_LOCATION_VIEW,
  });
  try {
    const response = await fetchLocationViewTravel(geonameId, cookies);
    const {
      taxonomy,
      url,
      associated,
      boundingBox,
      children,
      travelContent,
      breadCrumbs,
    } = response;
    await dispatch({
      type: FETCH_LOCATION_VIEW_SUCCESS,
      url,
      taxonomy,
      associated,
      boundingBox,
      children,
      travelContent,
      breadCrumbs,
    });
  } catch (error) {
    if (error.statusCode === 400) {
      dispatch({
        type: NOT_FOUND,
        message: 'The Spot Report Could not be found',
      });
    }
    dispatch({
      type: FETCH_LOCATION_VIEW_FAILURE,
      error,
    });
  }
};
