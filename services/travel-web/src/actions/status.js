export const NOT_FOUND = 'NOT_FOUND';

export const REDIRECT_TEMPORARY = 'REDIRECT_TEMPORARY';

export const INTERNAL_SERVER_ERROR = 'INTERNAL_SERVER_ERROR';
