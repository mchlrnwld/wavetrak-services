import { fetchSwellEventById, fetchAllSwellEvents } from '../common/api/feed';
import { NOT_FOUND } from './status';
import sanitizeArticleHTML from '../utils/sanitizeArticleHTML';

export const FETCH_SWELL_EVENT = 'FETCH_SWELL_EVENT';
export const FETCH_SWELL_EVENT_SUCCESS = 'FETCH_SWELL_EVENT_SUCCESS';
export const FETCH_SWELL_EVENT_FAILURE = 'FETCH_SWELL_EVENT_FAILURE';

export const FETCH_SWELL_EVENTS = 'FETCH_SWELL_EVENTS';
export const FETCH_SWELL_EVENTS_SUCCESS = 'FETCH_SWELL_EVENTS_SUCCESS';
export const FETCH_SWELL_EVENTS_FAILURE = 'FETCH_SWELL_EVENTS_FAILURE';

export const fetchSwellEvent = (id, cookies) => async (dispatch) => {
  dispatch({
    type: FETCH_SWELL_EVENT,
  });

  try {
    const { event } = await fetchSwellEventById(id, cookies);
    const sanitizedEvent = {
      ...event,
      updates: event.updates.map((update) => ({
        ...update,
        content: sanitizeArticleHTML(update.content),
      })),
    };
    dispatch({
      type: FETCH_SWELL_EVENT_SUCCESS,
      event: sanitizedEvent,
    });
  } catch (error) {
    if (error.statusCode === 400) {
      dispatch({
        type: NOT_FOUND,
        message: 'The swell event was not found',
      });
    } else {
      dispatch({
        type: FETCH_SWELL_EVENT_FAILURE,
        error,
      });
    }
  }
};

export const fetchSwellEvents = () => async (dispatch) => {
  dispatch({
    type: FETCH_SWELL_EVENTS,
  });
  try {
    const { events } = await fetchAllSwellEvents();
    dispatch({
      type: FETCH_SWELL_EVENTS_SUCCESS,
      events,
    });
  } catch (error) {
    dispatch({
      type: FETCH_SWELL_EVENTS_FAILURE,
      error,
    });
  }
};
