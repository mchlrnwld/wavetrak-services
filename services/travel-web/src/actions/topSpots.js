import { getTopSpots } from '../common/api/spots';

export const FETCH_TOP_SPOTS = 'FETCH_TOP_SPOTS';
export const FETCH_TOP_SPOTS_SUCCESS = 'FETCH_TOP_SPOTS_SUCCESS';
export const FETCH_TOP_SPOTS_FAILURE = 'FETCH_TOP_SPOTS_FAILURE';

export const fetchTopSpots = (geonameId, cookies) => async (dispatch) => {
  dispatch({
    type: FETCH_TOP_SPOTS,
  });
  try {
    const response = await getTopSpots(geonameId, cookies);
    const {
      data: { topSpots },
    } = response;
    dispatch({
      type: FETCH_TOP_SPOTS_SUCCESS,
      topSpots,
    });
  } catch (error) {
    dispatch({
      type: FETCH_TOP_SPOTS_FAILURE,
      error,
    });
  }
};
