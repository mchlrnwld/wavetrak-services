import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as fetchLocationViewAPI from '../common/api/locationView';
import {
  FETCH_LOCATION_VIEW,
  FETCH_LOCATION_VIEW_SUCCESS,
  FETCH_LOCATION_VIEW_FAILURE,
  fetchLocationView,
} from './locationView';

describe('actions / locationView', () => {
  let mockStore;

  before(() => {
    mockStore = configureStore([thunk]);
  });

  describe('fetchLocationViewTravel', () => {
    beforeEach(() => {
      sinon.stub(fetchLocationViewAPI, 'fetchLocationViewTravel');
    });

    afterEach(() => {
      fetchLocationViewAPI.fetchLocationViewTravel.restore();
    });

    it('dispatches FETCH_LOCATION_VIEW_SUCCESS with geonameId', async () => {
      const store = mockStore({});
      const geonameId = '12345';
      const locationViewResponse = deepFreeze({
        url: 'https://www.surfline.com/travel/12345',
        taxonomy: {},
        associated: {},
        boundingBox: {},
        children: [],
        travelContent: {},
        breadCrumbs: [],
      });

      fetchLocationViewAPI.fetchLocationViewTravel.resolves(locationViewResponse);

      await store.dispatch(fetchLocationView(geonameId));
      expect(store.getActions()).to.deep.equal([
        {
          type: FETCH_LOCATION_VIEW,
        },
        {
          type: FETCH_LOCATION_VIEW_SUCCESS,
          url: locationViewResponse.url,
          taxonomy: locationViewResponse.taxonomy,
          associated: locationViewResponse.associated,
          boundingBox: locationViewResponse.boundingBox,
          children: locationViewResponse.children,
          travelContent: locationViewResponse.travelContent,
          breadCrumbs: locationViewResponse.breadCrumbs,
        },
      ]);
      expect(fetchLocationViewAPI.fetchLocationViewTravel).to.have.been.calledOnce();
    });

    it('dispatches FETCH_USER_DETAILS_FAILURE with invalid geonameId', async () => {
      const store = mockStore({});
      const geonameId = '12345';

      fetchLocationViewAPI.fetchLocationViewTravel.rejects({ statusCode: 404 });

      await store.dispatch(fetchLocationView(geonameId));
      expect(store.getActions()).to.deep.equal([
        {
          type: FETCH_LOCATION_VIEW,
        },
        {
          type: FETCH_LOCATION_VIEW_FAILURE,
          error: { statusCode: 404 },
        },
      ]);
      expect(fetchLocationViewAPI.fetchLocationViewTravel).to.have.been.calledOnce();
    });
  });
});
