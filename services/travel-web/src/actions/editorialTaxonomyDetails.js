import { get as _get } from 'lodash';
import * as editorialAPI from '../common/api/editorial';
import getFeedGeotarget from '../utils/getFeedGeotarget';
import { NOT_FOUND } from './status';

export const FETCH_EDITORIAL_TAXONOMY_DETAILS = 'FETCH_EDITORIAL_TAXONOMY_DETAILS';
export const FETCH_EDITORIAL_TAXONOMY_DETAILS_SUCCESS = 'FETCH_EDITORIAL_TAXONOMY_DETAILS_SUCCESS';
export const FETCH_EDITORIAL_TAXONOMY_DETAILS_FAILURE = 'FETCH_EDITORIAL_TAXONOMY_DETAILS_FAILURE';

export const fetchEditorialTaxonomyDetails = (taxonomy = 'category', term, subCategory) => async (
  dispatch,
  getState,
) => {
  dispatch({
    type: FETCH_EDITORIAL_TAXONOMY_DETAILS,
  });
  try {
    const termOverride = subCategory || term;
    const state = getState();
    const geotarget = getFeedGeotarget(state);
    const details = await editorialAPI.fetchEditorialTaxonomyDetails(
      taxonomy,
      termOverride,
      subCategory,
      geotarget,
    );
    dispatch({
      type: FETCH_EDITORIAL_TAXONOMY_DETAILS_SUCCESS,
      details,
    });
  } catch (error) {
    const wpStatusCode = _get(error, 'response.status', null);
    if (error.statusCode === 400 || wpStatusCode === 404) {
      dispatch({
        type: NOT_FOUND,
        message: 'The taxonomy details was not found',
      });
    } else {
      dispatch({
        type: FETCH_EDITORIAL_TAXONOMY_DETAILS_FAILURE,
        error,
      });
    }
  }
};
