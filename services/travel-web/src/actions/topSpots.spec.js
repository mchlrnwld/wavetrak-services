import { expect } from 'chai';
import sinon from 'sinon';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as spotsAPI from '../common/api/spots';
import {
  FETCH_TOP_SPOTS,
  FETCH_TOP_SPOTS_SUCCESS,
  FETCH_TOP_SPOTS_FAILURE,
  fetchTopSpots,
} from './topSpots';
import topSpotsFixture from './fixtures/topSpots.json';

describe('actions / locationView', () => {
  let mockStore;

  before(() => {
    mockStore = configureStore([thunk]);
  });

  describe('fetchTopSpots', () => {
    beforeEach(() => {
      sinon.stub(spotsAPI, 'getTopSpots');
    });

    afterEach(() => {
      spotsAPI.getTopSpots.restore();
    });

    it('dispatches FETCH_TOP_SPOTS_SUCCESS with geonameId', async () => {
      const store = mockStore({});
      const geonameId = '12345';

      spotsAPI.getTopSpots.resolves(topSpotsFixture);

      await store.dispatch(fetchTopSpots(geonameId));
      expect(store.getActions()).to.deep.equal([
        {
          type: FETCH_TOP_SPOTS,
        },
        {
          type: FETCH_TOP_SPOTS_SUCCESS,
          topSpots: topSpotsFixture.data.topSpots,
        },
      ]);
      expect(spotsAPI.getTopSpots).to.have.been.calledOnce();
    });

    it('dispatches FETCH_TOP_SPOTS_FAILURE with invalid geonameId', async () => {
      const store = mockStore({});
      const geonameId = '12345';

      spotsAPI.getTopSpots.rejects({ message: 'Unable to find a suitable node for that taxonomy' });

      await store.dispatch(fetchTopSpots(geonameId));
      expect(store.getActions()).to.deep.equal([
        {
          type: FETCH_TOP_SPOTS,
        },
        {
          type: FETCH_TOP_SPOTS_FAILURE,
          error: { message: 'Unable to find a suitable node for that taxonomy' },
        },
      ]);
      expect(spotsAPI.getTopSpots).to.have.been.calledOnce();
    });
  });
});
