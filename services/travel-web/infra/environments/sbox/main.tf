provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/travel/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-2-sandbox"
}

variable "load_balancer_arn" {
  default = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-2-sandbox/224dfd1fa8567f7e"
}

locals {
  dns_name = "travel.sandbox.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "host-header"
      value = "internal-travel.sandbox.surfline.com"
    },
  ]
}

module "travel" {
  source = "../../"

  company     = "sl"
  application = "travel"
  environment = "sandbox"

  default_vpc = "vpc-981887fd"
  dns_name    = local.dns_name
  dns_zone_id = "Z3DM6R3JR1RYXV"
  ecs_cluster = "sl-core-svc-sandbox"

  service_lb_rules = local.service_lb_rules
  service_td_count = 1

  aws_listener_arn_http = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-2-sandbox/224dfd1fa8567f7e/2270815960a67034"
  iam_role_arn          = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn     = data.aws_alb.main_internal.arn

  auto_scaling_enabled = false
}

resource "aws_route53_record" "travel_internal" {
  zone_id = "Z3DM6R3JR1RYXV"
  name    = "internal-travel.sandbox.surfline.com"
  type    = "A"

  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = true
  }
}
