provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/travel/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-2-staging"
}

variable "load_balancer_arn" {
  default = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-2-staging/4042d775e313f612"
}

locals {
  dns_name = "travel.staging.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "host-header"
      value = "internal-travel.staging.surfline.com"
    },
  ]
}

module "travel" {
  source = "../../"

  company     = "sl"
  application = "travel"
  environment = "staging"

  default_vpc = "vpc-981887fd"
  dns_name    = local.dns_name
  dns_zone_id = "Z3JHKQ8ELQG5UE"
  ecs_cluster = "sl-core-svc-staging"

  service_lb_rules = local.service_lb_rules
  service_td_count = 1

  aws_listener_arn_http = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-2-staging/4042d775e313f612/e1f601835f792a75"
  iam_role_arn          = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn     = data.aws_alb.main_internal.arn

  auto_scaling_enabled = false
}

resource "aws_route53_record" "travel_internal" {
  zone_id = "Z3JHKQ8ELQG5UE"
  name    = "internal-travel.staging.surfline.com"
  type    = "A"

  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = true
  }
}
