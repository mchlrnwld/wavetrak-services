provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/travel/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-2-prod"
}

variable "load_balancer_arn" {
  default = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-2-prod/99e5fd75fdbf20fd"
}

locals {
  dns_name = "travel.prod.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "host-header"
      value = "internal-travel.surfline.com"
    },
  ]
}

module "travel" {
  source = "../../"

  company     = "sl"
  application = "travel"
  environment = "prod"

  default_vpc = "vpc-116fdb74"
  dns_name    = local.dns_name
  dns_zone_id = "Z3LLOZIY0ZZQDE"
  ecs_cluster = "sl-core-svc-prod"

  service_lb_rules = local.service_lb_rules
  service_td_count = 2

  aws_listener_arn_http = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-2-prod/99e5fd75fdbf20fd/b668bfe144c94994"
  iam_role_arn          = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn     = data.aws_alb.main_internal.arn

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 100
  auto_scaling_min_size     = 2
  auto_scaling_max_size     = 5000
}

resource "aws_route53_record" "travel_internal" {
  zone_id = "ZY7MYOQ65TY5X"
  name    = "internal-travel.surfline.com"
  type    = "A"

  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = true
  }
}
