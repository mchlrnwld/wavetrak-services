#!/bin/bash

## Change URI to be that of the appropriate environment 
URI="mongodb://127.0.0.1:27017/UserDB"
DB="UserDB"
COLLECTION="EvoPromoCodes"

## CREATE COLLECTION AND INDEX IN REMOTE DB
echo "Create collection schema and index in '$DB'..."
mongo $URI --eval '
db.createCollection("EvoPromoCodes", {
   validator: {
      $jsonSchema: {
         bsonType: "object",
         required: [ "code", "used"],
         properties: {
            code: {
               bsonType: "string",
               description: "must be a string and is required"
            },
            used: {
               bsonType: "bool",
               description: "must be a boolean and is required"
            },
         }
      }
   }
})
db.EvoPromoCodes.createIndex( { "code": 1 }, { unique: true } )'

## IMPORT TO REMOTE DB
echo "Import '$URI'..."
mongoimport --uri $URI --collection $COLLECTION --type csv --headerline --columnsHaveTypes --file ./evopromocodes.csv

echo "Done."

