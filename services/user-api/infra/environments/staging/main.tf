provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/user-service/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "user_service" {
  source = "../../"

  company     = "sl"
  application = "user-service"
  environment = "staging"

  default_vpc      = "vpc-981887fd"
  dns_name         = "user-service.staging.surfline.com"
  dns_zone_id      = "Z3JHKQ8ELQG5UE"
  ecs_cluster      = "sl-core-svc-staging"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "user-service.staging.surfline.com"
    },
  ]

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-2-staging/4042d775e313f612/e1f601835f792a75"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-2-staging/4042d775e313f612"

  auto_scaling_enabled      = false
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = ""
}
