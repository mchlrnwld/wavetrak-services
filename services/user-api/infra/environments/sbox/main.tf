provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/user-service/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

module "user_service" {
  source = "../../"

  company     = "sl"
  application = "user-service"
  environment = "sandbox"

  default_vpc      = "vpc-981887fd"
  dns_name         = "user-service.sandbox.surfline.com"
  dns_zone_id      = "Z3DM6R3JR1RYXV"
  ecs_cluster      = "sl-core-svc-sandbox"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "user-service.sandbox.surfline.com"
    },
  ]

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-2-sandbox/224dfd1fa8567f7e/2270815960a67034"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-2-sandbox/224dfd1fa8567f7e"

  auto_scaling_enabled      = false
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = ""
}
