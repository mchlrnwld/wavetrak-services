# User service

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


  - [Authentication](#authentication)
  - [User API Endpoints](#user-api-endpoints)
    - [User Analytics](#user-analytics)
      - [`GET /user/identify`](#get-useridentify)
    - [Settings API](#settings-api)
    - [User Promocode](#user-promocode)
      - [`GET /user/promocode`](#get-userpromocode)
- [Admin search](#admin-search)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


Configure node environemnt

```bash
nvm install v12.17.0
nvm use v12.17.0
```

Configure .env appropriately (.env.sample provided)
`cp .env.sample .env`

Configuring the project
`npm install`

Testing the project
`npm run test`

Run the project
`npm run startlocal`

I highly recommend configuring WebStorm/IntelliJ with File Watchers to debug in your IDE:

[File Watchers in Webstorm/IntelliJ](http://blog.jetbrains.com/webstorm/2015/05/ecmascript-6-in-webstorm-transpiling/)

## Authentication
Every endpoint on user-service, besides `POST /` (Create User) has optional authentication.

As an API user, either add a query param `?AccessToken=abcdefg123` or header `X-Auth-AccessToken` to requests when using the public services. Our public service proxy will authenticate the request and pass the authenticated user id as header `X-Auth-UserId` for these services.

User-service uses the header `X-Auth-UserId` to ensure API actions are authorized.


## User API Endpoints

The User schema is:

    {
      "_id": "59f164c72d787c00115ee1f0",
      "firstName": "Test",
      "lastName": "User",
      "email": "testuser@surfline.com",
      "usID": 791125,
      "postal": "92614",
      "linkedClients": [
        {
            "createdAt": "2019-05-21T17:06:08.613Z",
            "clientId": "5af1ce73b5acf7c6dd2592ee",
            "scopes": [
                "5b16c373b5ac7eb84822f365"
            ]
        }
    ],
    "linkedAccount": {
        "facebook": {
            "id": "10157750313828942"
        }
    },
    "locale": "en-US",
    "brand": "sl",
    "modifiedDate": "2019-05-24T04:38:29.048Z",
    "isActive": true,
}

To get a user you should GET to `/User`. You include the id *or* the email as a query param, for example:

    GET /User?id=dadfafafdfsff1231&email=pran@ran.com HTTP/1.1
    Host: server.example.com

And the response will be:

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache

    {
      "_id": "59f164c72d787c00115ee1f0",
      "firstName": "Test",
      "lastName": "User",
      "email": "testuser@surfline.com",
      "usID": 791125,
      "postal": "92614",
      "linkedClients": [
        {
            "createdAt": "2019-05-21T17:06:08.613Z",
            "clientId": {
                "_id": "5af1ce73b5acf7c6dd2592ee",
                "name": "Rip Curl",
                "appImageURL": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/ripcurl-icon.png"
            },
            "scopes": [
                "5b16c373b5ac7eb84822f365"
            ]
        }
    ],
    "linkedAccount": {
        "facebook": {
            "id": "10157750313828942"
        }
    },
    "locale": "en-US",
    "brand": "sl",
    "modifiedDate": "2019-05-24T04:38:29.048Z",
    "isActive": true,
    "isEmailVerified": false,
    "hasSessionCompatibleDevice": true,
    "hasPassword": true,
    "id": "59f164c72d787c00115ee1f0"
}

The `linkedClients` property is an array of objects that contain the following fields:

```
createdAt: // Date as a String,
clientId : {
  _id: // String - Unique ClientId - Mongo identifier
  name: // This would be the Client’s Name
  appImageURL: // URL to show Client Icon
},
scopes: [] // This is an array which contains a unique id specific to the client's permission.
```

If a user has not connected to an external device, `linkedClients` will return an empty array.

```
{
  ...
  "linkedClients": [],
  ...
}
```

NOTE: `linkedClients.clientId` and `hasSessionCompatibleDevice` are virtual fields that are computed in the mongoose end and are not stored in Mongo Schema.



To change a password for a user, you should PUT to `/User/ChangePassword`. You need to specify an _id and password (hashed) in your body, for example:
Optional arguments are client id, device Id, device Type.

    PUT /User/ChangePassword HTTP/1.1
    Host: server.example.com
    Content-Type: application/json
    { _id: 123123123, password: 12312313123 }

NOTE: Refer Auth-service documentation to know more details about client_id, device_id and device_type.

And the response will be:

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {
      "modifiedDate": "Wed Dec 19 2012 01:03:25 GMT-0000 (GMT)",
      "firstName": "Gavin",
      "lastName": "Cooper",
      ...
    }

To update a user, you should PUT to `/User`. We do not update _id, password, or email with this call. You need to specify the _id and fields to change in your body, for example:

    PUT /User HTTP/1.1
    Host: server.example.com
    Content-Type: application/json
    { _id: 123123123, firstName: "Gav" }

And the response will be:

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {
      "modifiedDate": "Wed Dec 19 2012 01:03:25 GMT-0000 (GMT)",
      "firstName": "Gavin",
      "lastName": "Cooper",
      ...
    }

To create a user, you should POST to `/User`. You need to specify the email (req), password (req), first name (opt), last name (opt), and brand (opt), settings.receivePromotions (req), client id(opt), device Id(opt), device Type(opt) in your body, for example:

    POST /User HTTP/1.1
    Host: server.example.com
    Content-Type: application/json
    { firstName: "Gav", lastName: "Coops", password: "a12fb", email: "gcoops@surfline.com", brand: 'sl', settings: { receivePromotions: false },client_id:'WEB_VTM8QWOFNZ4X', device_id:'USR1001', device_type:'Chrome' }

NOTE: Refer Auth-service documentation to know more details about client_id, device_id and device_type.

You can also set a query param `?isShortLived=true` if you want the new user's auth token to expire in 30m.

And the response will be:

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {
      "user": {
        "firstName": "Gav",
        "lastName": "Coops",
        "pasword": "abc123",
        "brand": "bl",
        "settings": {
          "receivePromotions": true
        }
      },
      "token": {
        "token_type": "bearer",
        "access_token": "daab4b21237313b5a71e67ec28bd5e3e98648ac9",
        "expires_in": 2592000,
        "refresh_token": "929ee4bed4892d4fd278fe34d52220413b4a9dee"
      }
    }

Validation errors are returned as 400s with the validation details in the body:

    {
      "message": "Validation failed",
      "name": "ValidationError",
      "errors": {
        "brand": {
          "properties": {
            "type": "user defined",
            "message": "Valid Brand values: sl || bw || ft .",
            "path": "brand",
            "value": "surfline"
          },
          "message": "Valid Brand values: sl || bw || ft .",
          "name": "ValidatorError",
          "kind": "user defined",
          "path": "brand",
          "value": "surfline"
        }
      }
    }


### User Analytics

#### `GET /user/identify`

The user identify endpoint manages user properties for analytic services.
```
GET /user/identify HTTP/1.1
x-auth-userid: 56faeb5b19b74318a73cb89f

{
  "id": "56faeb5b19b74318a73cb89f",
  "entitlements": ["sl_premium"]
}
```

It can also be used to identify anonymous users and returns a response that can be used with segment `identify` calls or split.io `getTreatment` requests.

```
GET /user/identify HTTP/1.1

{
  "id": "dfe47590-56aa-11e7-9566-a1120b39ff5d"
  "entitlements": []
}

```

The `id` is generated as a v1 (timestamped) UUID.

### Settings API

Refer to `src/model/UserSettingsModel.js` for the schema and validation. Units will be geo-located if user's have not overridden the UserSettings model.

To get a user you should GET to `/User/Settings`. The GET api is optionally authenticated.
You can specify the `geoCountryIso` parameter as an override for what OpenResty injects.
If you call the user-service directly (without OpenResty proxy), you can pass in the `x-auth-userid` header.

    GET /User/Settings?geoCountryIso=US

And the response will be:

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {
    "_id": "58d2f2f472ccfb0d0b4069ce",
    "user": "56fae75b19b74318a73aa4c0",
    "__v": 0,
    "location": {
        "type": "Point",
        "coordinates": [
            0,
            0
        ]
    },
    "location": {
      "type": "Point",
      "coordinates": [
        0,
        0
      ]
    },
    "updatedAt": "2019-02-12T23:58:39.872Z",
    "receivePromotions": false,
    "isSwellChartCollapsed": false,
    "feedFilters": {
        "subregions": ""
    },
    "homeForecast": "58581a836630e24c44879142",
    "homeSpot": "5842041f4e65fad6a7708801",
    "preferredWaveChart": "SURF",
    "recentlyVisited": {
        "subregions": [
            {
                "name": "North Los Angeles",
                "_id": "58581a836630e24c44878fd5"
            },
            {
                "name": "South Orange County",
                "_id": "58581a836630e24c4487900a"
            },
            {
                "name": "North San Diego",
                "_id": "58581a836630e24c44878fd7"
            },
            {
                "name": "Northern Outer Banks",
                "_id": "58581a836630e24c44878fdd"
            },
            {
                "name": "Hawaii Kona",
                "_id": "58581a836630e24c44879080"
            },
            {
                "name": "Hawaii Hilo",
                "_id": "58581a836630e24c44879081"
            },
            {
                "name": "Zona Oeste Portugal",
                "_id": "58581a836630e24c4487900f"
            },
            {
                "name": "South Texas",
                "_id": "58581a836630e24c44879045"
            },
            {
                "name": "Central Texas",
                "_id": "58581a836630e24c44879043"
            },
            {
                "name": "North Texas",
                "_id": "58581a836630e24c44879044"
            }
        ],
        "spots": [
            {
                "name": "South Ocean Beach",
                "_id": "5842041f4e65fad6a77087f9"
            },
            {
                "name": "HB Pier, Southside Overview",
                "_id": "58bdea400cec4200133464f0"
            },
            {
                "name": "Malibu First Point",
                "_id": "584204214e65fad6a7709b9f"
            },
            {
                "name": "Malibu Second to Third Point",
                "_id": "5842041f4e65fad6a7708817"
            },
            {
                "name": "Anderson St.",
                "_id": "5842041f4e65fad6a7708822"
            },
            {
                "name": "Huntington State Beach",
                "_id": "584204204e65fad6a770998c"
            },
            {
                "name": "40 St. Newport",
                "_id": "5842041f4e65fad6a7708829"
            },
            {
                "name": "56th St.",
                "_id": "5842041f4e65fad6a7708e54"
            },
            {
                "name": "HB Pier, Northside Overview",
                "_id": "584204204e65fad6a77091aa"
            },
            {
                "name": "Jalama",
                "_id": "5842041f4e65fad6a7708991"
            }
        ]
    },
    "navigation": {
        "forecastsTaxonomyLocation": "58581a836630e24c44879142",
        "spotsTaxonomyLocation": null
    },
    "bwWindWaveModel": "LOLA",
    "travelDistance": 30,
    "boardTypes": [
        "GUN",
        "TOW",
        "SUP",
        "FOIL"
    ],
    "abilityLevel": "BEGINNER",
    "onboarded": [
        "FAVORITE_SPOTS",
        "NEW_SURFLINE_INTRODUCTION",
        "NEW_SURFLINE_ONBOARDING_INTRO"
    ],
    "units": {
        "temperature": "C",
        "windSpeed": "KTS",
        "tideHeight": "FT",
        "swellHeight": "FT",
        "surfHeight": "FT"
    }
}

To update a user, you should PUT to `/User/Settings`.

    PUT /User/Settings
    Content-Type: application/json
    {
      "preferredWaveChart": "SURF",
      "navigation": {
        "spotsTaxonomyLocation": "58f7ed5fdadb30820bb3987c",
        "forecastsTaxonomyLocation": "58f7ed51dadb30820bb387a6",
      },
      "feedFilters": {
        "subregions": "58f7ed51dadb30820bb387a6,58581a836630e24c44879074"
      },
      "homeSpot": "5842041f4e65fad6a7708801",
      "travelDistance": 30,
      "boardTypes": [
         "GUN",
         "TOW",
         "SUP",
         "FOIL"
      ],
      "abilityLevel": "INTERMEDIATE",
      "onboarded": ["FAVORITE_SPOTS"],
      "units": {
        "temperature": "C",
        "windSpeed": "KPH",
        "tideHeight": "M",
        "swellHeight": "M",
        "surfHeight": "M"
      }
    }

And the response will be:

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache
    {
    "_id": "58d2f2f472ccfb0d0b4069ce",
    "user": "56fae75b19b74318a73aa4c0",
    "__v": 0,
    "location": {
        "type": "Point",
        "coordinates": [
            0,
            0
        ]
    },
    "updatedAt": "2019-02-12T23:58:39.872Z",
    "receivePromotions": false,
    "isSwellChartCollapsed": false,
    "feedFilters": {
      "subregions": "58f7ed51dadb30820bb387a6,58581a836630e24c44879074"
    },
    "homeForecast": "58581a836630e24c44879142",
    "homeSpot": "5842041f4e65fad6a7708801",
    "preferredWaveChart": "SURF",
    "recentlyVisited": {
      "subregions": [
          {
            "name": "North Los Angeles",
            "_id": "58581a836630e24c44878fd5"
          },
          {
            "name": "South Orange County",
            "_id": "58581a836630e24c4487900a"
           },
           {
            "name": "North San Diego",
            "_id": "58581a836630e24c44878fd7"
           },
           {
            "name": "Northern Outer Banks",
            "_id": "58581a836630e24c44878fdd"
            },
            {
             "name": "Hawaii Kona",
             "_id": "58581a836630e24c44879080"
            },
            {
             "name": "Hawaii Hilo",
             "_id": "58581a836630e24c44879081"
            },
            {
             "name": "Zona Oeste Portugal",
             "_id": "58581a836630e24c4487900f"
            },
            {
             "name": "South Texas",
             "_id": "58581a836630e24c44879045"
            },
            {
             "name": "Central Texas",
             "_id": "58581a836630e24c44879043"
            },
            {
             "name": "North Texas",
             "_id": "58581a836630e24c44879044"
            }
      ],
      "spots": [
            {
             "name": "South Ocean Beach",
             "_id": "5842041f4e65fad6a77087f9"
            },
            {
             "name": "HB Pier, Southside Overview",
             "_id": "58bdea400cec4200133464f0"
            },
            {
             "name": "Malibu First Point",
             "_id": "584204214e65fad6a7709b9f"
            },
            {
             "name": "Malibu Second to Third Point",
             "_id": "5842041f4e65fad6a7708817"
            },
            {
             "name": "Anderson St.",
             "_id": "5842041f4e65fad6a7708822"
            },
            {
             "name": "Huntington State Beach",
             "_id": "584204204e65fad6a770998c"
            },
            {
             "name": "40 St. Newport",
             "_id": "5842041f4e65fad6a7708829"
            },
            {
             "name": "56th St.",
             "_id": "5842041f4e65fad6a7708e54"
            },
            {
             "name": "HB Pier, Northside Overview",
             "_id": "584204204e65fad6a77091aa"
            },
            {
             "name": "Jalama",
             "_id": "5842041f4e65fad6a7708991"
            }
        ]
    },
    "navigation": {
      "spotsTaxonomyLocation": "58f7ed5fdadb30820bb3987c",
      "forecastsTaxonomyLocation": "58f7ed51dadb30820bb387a6",
    },
    "bwWindWaveModel": "LOLA",
    "travelDistance": 30,
    "boardTypes": [
        "GUN",
        "TOW",
        "SUP",
        "FOIL"
    ],
    "abilityLevel": "INTERMEDIATE",
    "onboarded": ["FAVORITE_SPOTS"],
    "units": {
        "temperature": "C",
        "windSpeed": "KTS",
        "tideHeight": "FT",
        "swellHeight": "FT",
        "surfHeight": "FT"
    }
}

### User Promocode

#### `GET /user/promocode`

To get a promocode you should GET to `/user/promocode`. You include the type as a query param, for example:

    GET /user/promocode?type=evo HTTP/1.1
    Host: server.example.com

And the response will be:

```
GET /user/promocode HTTP/1.1
x-auth-userid: 56faeb5b19b74318a73cb89f

{
  "code": "37CHJRVJ"
}
```

# Admin search

To search for users, you should GET to `/Admin/User`. You include the parameter search and your string,
which can be a first name, last name, or email.

For example (assuming URLencoding on special characters such as a space), searching for
"Scott D":

    GET /Admin/Users?search=Scott%20D HTTP/1.1
    Host: server.example.com

The response will be an array of results:

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache

    [
      {
        "_id": "56f9eb8646f566a11a5ce971",
        "firstName": "Scott",
        "lastName": "David",
        "email": "scott.david@david.com",
        ...
      },
      {
        "_id": "56f9eafa46f566a11a5c6357",
        "firstName": "Scott",
        "lastName": "Deen",
        "email": "sdeen@companyx.com",
        ...
      },
      {
        "_id": "56f9eafa46f566a11a5c6357",
        "firstName": "Scott",
        "lastName": "Digiornio",
        "email": "sdigiornio@surfline.com",
        ...
      },
      {
        "_id": "56f9eb0446f566a11a5c6cb0",
        "firstName": "scott",
        "lastName": "diorio",
        "email": "scott.diorio@surfline.com",
        ...
      }
    ]


Another search case, involving an email search (again, assuming URLencoding)
for "scott.diorio@":

    GET /Admin/Users?search=scott.diorio%40 HTTP/1.1
    Host: server.example.com

And the response will be an array of results:

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Cache-Control: no-store
    Pragma: no-cache

    [
      {
        "_id": "56f9eb0446f566a11a5c6cb0",
        "firstName": "scott",
        "lastName": "diorio",
        "email": "scott.diorio@surfline.com",
        ...
      }
    ]

Searching on email domain:

   GET /Admin/Users?search=%40surfline.com HTTP/1.1
   Host: server.example.com

And the response will be an array of results:

   HTTP/1.1 200 OK
   Content-Type: application/json;charset=UTF-8
   Cache-Control: no-store
   Pragma: no-cache

   [
     {
       "_id": "56f9eafa46f566a11a5c6357",
       "firstName": "Scott",
       "lastName": "Digiornio",
       "email": "sdigiornio@surfline.com",
       ...
     },
     {
       "_id": "56f9eb0446f566a11a5c6cb0",
       "firstName": "scott",
       "lastName": "diorio",
       "email": "scott.diorio@surfline.com",
       ...
     }
   ]


# Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `user-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd4TmpZME5qWXdNdyJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eE5qWTBOall3TXciLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the endpoints detailed in the [User API Endpoints](#user-api-endpoints) documentation.

**Note:** Using an `X-Auth-UserId` will make your `curl` request verifications more robust. This value corresponds to the `_id` value in the Mongo `userInfo` collection.

**Ex:**

Test `GET` requests against the `/User` endpoint:

```
curl \
	-i \
	-X GET \
 	-H 'X-Auth-UserId: <X-Auth-UserId>' \
 	-H 'Content-Type: application/json' \
 	http://subscription-service.sandbox.surfline.com/User?&email=<email>
```

**Expected response:** See sample responses above in the [user api endpoint](#user-api-endpoints) section.