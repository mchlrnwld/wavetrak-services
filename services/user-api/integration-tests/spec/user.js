import { expect } from 'chai';
import fetch from 'node-fetch';
import { ObjectID } from 'mongodb';
import connectMongo from './helpers/connectMongo';
import cleanMongo from './helpers/cleanMongo';
import config from './config';

const user = () => {
  describe('/user', () => {
    let db;

    before(async () => {
      db = await connectMongo();
    });

    afterEach(() => cleanMongo(db));

    after(() => {
      db.close();
    });

    describe('/settings', () => {
      const userSettingsId = new ObjectID('5842041f4e65fad6a7708001');
      const userId = new ObjectID('5842041f4e65fad6a7708002');
      const spotId = new ObjectID('5842041f4e65fad6a7708003');
      const subregionId = new ObjectID('5842041f4e65fad6a7708004');

      describe('/recentlyvisited', () => {
        describe('POST', () => {
          beforeEach(async () => {
            await db.collection('UserSettings').insert({ _id: userSettingsId, user: userId });
          });

          it('updates recently visited spots in user settings', async () => {
            const response = await fetch(`${config.USER_SERVICE}/user/settings/recentlyvisited`, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'x-auth-userId': userId.toString(),
              },
              body: JSON.stringify({
                type: 'spots',
                _id: spotId,
                name: 'Spot Name',
              }),
            });
            expect(response.status).to.equal(200);
            const body = await response.json();
            expect(body).to.be.ok();

            const userSettings = await db.collection('UserSettings').findOne({
              _id: userSettingsId,
            });
            expect(userSettings).to.exist();
            expect(userSettings.recentlyVisited.spots[0]._id).to.deep.equal(spotId);
          });

          it('updates recently visited subregions in user settings', async () => {
            const response = await fetch(`${config.USER_SERVICE}/user/settings/recentlyvisited`, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'x-auth-userId': userId.toString(),
              },
              body: JSON.stringify({
                type: 'subregions',
                _id: subregionId,
                name: 'Subregion Name',
              }),
            });
            expect(response.status).to.equal(200);
            const body = await response.json();
            expect(body).to.be.ok();

            const userSettings = await db.collection('UserSettings').findOne({
              _id: userSettingsId,
            });
            expect(userSettings).to.exist();
            expect(userSettings.recentlyVisited.subregions[0]._id).to.deep.equal(subregionId);
          });
        });
      });
    });
  });
};

export default user;
