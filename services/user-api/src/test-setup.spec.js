import chai from 'chai';
import chaihttp from 'chai-http';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';

chai.should();
chai.use(chaihttp);
chai.use(dirtyChai);
chai.use(sinonChai);
