import newrelic from 'newrelic';
import { Router } from 'express';
import { createLogger } from '../common/logger';
import * as UserSettings from '../model/UserSettings';
import { identify } from '../common/analytics';

const RECENTLY_VISITED_MAX = 10;

export function userSettings() {
  const api = Router();
  const log = createLogger('user-settings-service');

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/User',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  const handleErrorForResponse = (errorCode, res, action) => (err) => {
    if (typeof err === 'object') {
      log.error({
        message: `userService:settings:${action}`,
        stack: err,
        trace: err.stack,
      });
    } else {
      log.error({
        message: `userService:settings:${action}`,
        errorMessage: err,
        trace: err.stack,
      });
    }
    newrelic.noticeError(err);
    res.status(errorCode).send(err);
  };

  const updateUserSettingsHandler = async (req, res) => {
    const { authenticatedUserId, body } = req;
    const traits = {};
    const { travelDistance, abilityLevel } = body;

    if (travelDistance) {
      traits.travelDistance = travelDistance;
    }
    if (abilityLevel) {
      traits.abilityLevel = abilityLevel;
    }

    if (Object.keys(traits).length > 0) {
      identify(authenticatedUserId, traits, 'sl');
    }

    try {
      const settings = await UserSettings.updateUserSettings(authenticatedUserId, body);
      return res.status(200).json(settings);
    } catch (error) {
      return handleErrorForResponse(500, res, 'updateUserSettings')(error);
    }
  };

  const getUserSettingsHandler = async (req, res) => {
    const { authenticatedUserId, geoCountryIso, query } = req;
    const geoCountryIsoIdentifier = query.geoCountryIso || geoCountryIso;
    try {
      const settings = await UserSettings.getUserSettings(
        authenticatedUserId,
        geoCountryIsoIdentifier,
      );
      return res.status(200).json(settings);
    } catch (error) {
      return handleErrorForResponse(500, res, 'getUserSettings')(error);
    }
  };

  const updateRecentlyVisited = async (req, res) => {
    try {
      const userId = req.authenticatedUserId;
      const { type, _id, name } = req.body;
      if (!type || !_id || !name) {
        return res.status(400).send({ message: 'type, _id, and name are required.' });
      }
      const { recentlyVisited } = await UserSettings.getUserSettings(userId);
      const places = recentlyVisited[type];
      const duplicate = places.find((el) => el._id.toString() === _id);
      const index = places.indexOf(duplicate);
      const result =
        index === -1
          ? [{ _id, name }, ...places].slice(0, RECENTLY_VISITED_MAX)
          : [{ _id, name }, ...places.slice(0, index), ...places.slice(index + 1)];

      const updatedUserSettings = await UserSettings.updateUserSettings(userId, {
        recentlyVisited: {
          subregions: type === 'subregions' ? result : recentlyVisited.subregions,
          spots: type === 'spots' ? result : recentlyVisited.spots,
        },
      });
      return res.status(200).send(updatedUserSettings);
    } catch (err) {
      return handleErrorForResponse(500, res, 'updateRecentlyVisited')(err);
    }
  };

  api.get('/', trackRequest, getUserSettingsHandler);
  api.put('/', trackRequest, updateUserSettingsHandler);
  api.post('/recentlyvisited', trackRequest, updateRecentlyVisited);

  return api;
}
