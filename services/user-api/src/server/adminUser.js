import { Router } from 'express';
import mongoose from 'mongoose';
import { createLogger } from '../common/logger';
import { trackAccountCreated, trackDeletedAccount } from '../common/trackEvent';
import * as linkedClients from '../external/linkedClients';
import * as entitlements from '../external/entitlements';
import * as favorites from '../external/favorites';
import * as metering from '../external/metering';
import { syncUserDetails, deleteSubscriptionUser } from '../external/subscriptions';
import * as integrations from '../external/integrations';
import * as auth from '../external/auth';
import { createUser } from '../model/admin';
import { getPendingDeletedUsers, markUserAsDeleted } from '../model/deleted-users/DeletedUsers';
import * as userModel from '../model/User';
import * as userSettings from '../model/UserSettings';

export function adminUser() {
  const api = Router();
  const log = createLogger('adminUser-service');

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/Admin/User',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  const getUsersHandler = async (req, res) => {
    const term = req.query.search;
    let results = [];
    if (!term) return res.status(400).send({ message: 'search term required' });
    try {
      results = await userModel.getUserByNameEmailSearch(term);

      return res.send(results);
    } catch (err) {
      return res.status(500).send({
        message: 'Could not fetch user results',
        stack: err.stack,
      });
    }
  };

  /**
   * @description Handler to return the UserInfo data if a single Wavetrak User
   *
   * @param {object} req - request body
   * @param {object} req.query - request query params
   * @param {string} userId - The userId value in the request query param
   *
   * @param {object} res - The response body
   * @param {object} user - The user document for the corresponding request id
   */
  const getUserHandler = async (req, res) => {
    const {
      query: { userId },
    } = req;
    try {
      const user = await userModel.getUserById(userId);
      const userObj = typeof user === 'object' ? user : user.toObject();
      delete userObj.password;
      return res.send({ user: userObj });
    } catch (error) {
      return res.status(500).send({
        message: 'Error fetching user by id',
        stack: error.stack,
      });
    }
  };

  /**
   * @description Handler for creating a user account information in MongoDB.
   * @param {import('express').Request} req
   * @param {object} req.body.user - user document properites
   * @param {string} req.body.settings - user-settings document properites
   * @param {string} req.body.countryCode - ISO code used from location based settings
   * @param {import('express').Response<{ user: { _id: string }}>} res
   */
  const createUserHandler = async (req, res) => {
    const {
      body: { user, settings, countryCode },
    } = req;
    try {
      const { password } = user;
      if (password) {
        return res
          .status(400)
          .send({ message: 'This API does not support user password creation' });
      }
      const newUser = await createUser(user);
      const { _id: id } = newUser;
      const { receivePromotions } = await userSettings.createUserSettings(
        id,
        countryCode,
        settings,
      );

      trackAccountCreated({ user: newUser, receivePromotions, platform: 'Web' });
      return res.send({ user: newUser });
    } catch (error) {
      return res.status(500).send({
        message: 'Error creating user',
        error: error.stack,
      });
    }
  };

  /**
   * @description Handler for updating the user's account information in both Stripe and MongoDB.
   * This handler will take a request body with user information to change
   * in Mongo (and stripe if applies). If the body contains an email, the handler will ensure
   * it does not already exist. The ID for the user should be passed as a URL parameter.
   * @param {object} req - request body
   * @param {object} req.params - request parameters
   * @param {string} req.params.id - User ID to update
   * @param {*} res - response body
   */
  const updateUserHandler = async (req, res) => {
    const { body } = req;
    const userId = req.params.id;
    const isEmptyBody = Object.keys(body).length === 0 && body.constructor === Object;

    if (!body || isEmptyBody) {
      return res.status(400).send({ message: 'You need to have a request body.' });
    }

    try {
      if (body.email) {
        const userIdentifier = body.email.toLowerCase();
        const foundUser = await userModel.getUserByEmail(userIdentifier);

        if (foundUser) {
          return res.status(400).send({ message: 'This email is already in use.' });
        }
      }

      // Get the old user info in case rollback is needed
      const oldUserDetails = await userModel.getUserById(userId);
      const oldInfo = oldUserDetails[0];

      const updatedUser = await userModel.updateUser(userId, body);
      if (body.firstName || body.lastName || body.gender || body.locale || body.email) {
        try {
          // TODO: Refactor the nested try catch with a better solution
          await syncUserDetails(userId, {
            firstName: updatedUser.firstName,
            lastName: updatedUser.lastName,
            email: updatedUser.email,
          });
        } catch (error) {
          // If sync with stripe fails, rollback and throw error
          await userModel.updateUser(userId, oldInfo);
          return res.status(500).send({
            message: 'Could not update user in stripe.',
            stack: error.stack,
          });
        }
      }

      return res.status(200).json(updatedUser);
    } catch (err) {
      return res.status(500).send({
        message: 'Could not update user.',
        stack: err.stack,
      });
    }
  };

  /**
   * Handler for returning DeletedUsers documents
   * @returns The list of pending users to be deleted based on their dateRequestedAt.
   */
  const getDeletedUsersHandler = async (req, res) => {
    const date = req.query?.date ? new Date(req.query.date) : new Date();

    try {
      const result = await getPendingDeletedUsers(date);
      return res
        .status(200)
        .json(result?.map(({ user, deleteRequestedAt }) => ({ user, deleteRequestedAt })));
    } catch (err) {
      log.error({
        message: 'getDeletedUsersHandler',
        errorMessage: err.message,
        trace: err.stack,
      });
      return res.status(500).send({
        message: 'Could not get DeletedUsers',
        stack: err.stack,
      });
    }
  };

  /**
   * @description Endpoint used to deregister a garmin to surfline connection.
   *
   * We don't need to any specific or fancy error handling here. Any errors
   * will return a 500 to garmin, which will make them retry the ping at a later
   * date with the same body as before. Eventually this will succeed an all the
   * entries will have been pulled.
   */
  const garminDeregisterHandler = async (req, res) => {
    const { deregistrations } = req.body;

    if (!deregistrations) {
      return res.sendStatus(400);
    }

    const userAccessTokens = deregistrations.map((entry) => entry.userAccessToken);

    await userModel.deregisterLinkedClients(userAccessTokens);

    log.info(`Deregistered Garmin access token ${userAccessTokens}`);

    return res.sendStatus(200);
  };

  /**
   * @description This endpoint is for handling GDPR requests from Admin CMS.
   *
   * For a valid user this endpoint should do the following:
   * - Delete `UserInfo`, `UserSettings` documents
   * - Requests the `Meter` document to be deleted
   * - Requests to delete favorties (BW/SL)
   * - Sends a data suppression call to user integrations (Segment, CustomerIO, Amplitude, Braze)
   */
  const deleteUserHandler = async (req, res) => {
    const { id: userId } = req.params;
    if (!userId) {
      return res.status(400).send({ message: 'You need to specify an id' });
    }
    try {
      const isPremium = await entitlements.isPremium(userId);
      if (isPremium) {
        return res.status(200).send({
          deleted: false,
          message: 'User has a premium entitlement',
        });
      }

      let brand = 'sl';
      const session = await mongoose.startSession();
      await session.withTransaction(async () => {
        const userDocDeleted = userModel.deleteUser(userId, { session });
        const userSettingsDeleted = userSettings.deleteUserSettings(userId, { session });
        const [userDoc] = await Promise.all([userDocDeleted, userSettingsDeleted]);

        const linkedGarminClient = userDoc
          ?.toObject()
          ?.linkedClients.find((client) => client?.name === 'Garmin');

        if (linkedGarminClient) {
          await linkedClients.invalidateGarminToken(
            linkedGarminClient?.token,
            linkedGarminClient?.tokenSecret,
          );
        }

        await Promise.all([
          metering.deleteMeter(userId),
          favorites.deleteFavorites(userId),
          favorites.deleteBwFavorites(userId),
          deleteSubscriptionUser(userId),
          integrations.createSegmentRegulation(userId),
          integrations.deleteCustomerIOUser(userId),
          integrations.deleteAmplitudeUser(userId),
          integrations.deleteBrazeUser(userId),
        ]);
        brand = userDoc?.brand;
        await auth.invalidateToken(userId);
      });
      await session.endSession();

      const { deleteRequestedAt } = await markUserAsDeleted(userId);

      trackDeletedAccount({
        brand,
        deleteRequestedAt,
        userId,
      });

      return res.status(200).send({
        deleted: true,
        message: 'User was successfully deleted',
      });
    } catch (err) {
      log.error({
        message: 'deleteUserHandler',
        errorMessage: err.message,
        trace: err.stack,
      });
      return res.status(500).send({
        deleted: false,
        message: 'Could not delete user',
        error: err.stack,
      });
    }
  };

  /**
   * @description This endpoint is for handling email verification requests from Admin Tools
   *
   * For a valid user this endpoint sets the isEmailVerified boolean property to true
   */
  const verifyEmailHandler = async (req, res) => {
    const { userId } = req.params;
    const { body } = req;
    if (!userId) {
      return res.status(400).send({ message: 'You need to specify an id' });
    }
    try {
      const updatedUser = await userModel.verifyEmail({
        userId,
        email: body.email,
        brand: body.brand,
      });
      return res.status(200).send(updatedUser);
    } catch (error) {
      return res.status(400).send({
        message: 'Could not verify user',
        error,
      });
    }
  };

  api.get('/user', trackRequest, getUserHandler);
  api.post('/user', trackRequest, createUserHandler);
  api.get('/Users', trackRequest, getUsersHandler);
  api.put('/Users/:id', trackRequest, updateUserHandler);
  api.get('/users/deleted', trackRequest, getDeletedUsersHandler);
  api.delete('/Users/:id', trackRequest, deleteUserHandler);
  api.post('/garmin/deregister', trackRequest, garminDeregisterHandler);
  api.post('/user/verify-email/:userId', trackRequest, verifyEmailHandler);
  return api;
}
