import { setupExpress, initMongo } from '@surfline/services-common';
import mongoose from 'mongoose';
import { user } from './user';
import promocode from './promocode';
import { adminUser } from './adminUser';
import log from '../common/logger';

const startApp = () =>
  setupExpress({
    log,
    name: 'user-service',
    port: process.env.EXPRESS_PORT || 8080,
    allowedMethods: ['GET', 'HEAD', 'OPTIONS', 'POST', 'PUT', 'PATCH', 'DELETE'],
    handlers: [
      ['/User/Promocode', promocode()],
      ['/User', user()],
      ['/Admin', adminUser()],
    ],
  });

initMongo(mongoose, process.env.MONGO_CONNECTION_STRING, 'user-service')
  .then(startApp)
  .catch((err) => {
    log.error({ message: "App Initialization failed. Can't connect to database", stack: err });
    process.exit(1);
  });

export default startApp;
