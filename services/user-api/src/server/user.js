import newrelic from 'newrelic';
import { Router } from 'express';
import {
  authenticateRequest,
  wrapErrors,
  getPlatform,
  MAX_PASSWORD_LENGTH,
  MIN_PASSWORD_LENGTH,
} from '@surfline/services-common';
import mongoose from 'mongoose';
import log from '../common/logger';
import { decrypt } from '../common/encryption';
import {
  getUserById,
  getUserByEmail,
  getUserByUsID,
  getUserByLinkedAccount,
  getUserByClientAccessToken,
  createUser,
  validateUser,
  confirmAndChangeUserPassword,
  changeUserPassword,
  verifyEmail,
  triggerEmailVerification,
  updateUser,
} from '../model/User';
import * as userSettingsModel from '../model/UserSettings';
import * as authService from '../external/auth';
import { syncUserDetails } from '../external/subscriptions';
import { trackAccountGeoIP } from '../common/trackEvent';
import { userSettings } from './userSettings';
import { createDeletedUser } from '../model/deleted-users/DeletedUsers';
import * as entitlementsService from '../external/entitlements';
import { invalidatePasswordResetCode } from '../external/passwordReset';
import { getMigratedBrandName } from '../common/utils';

export function user() {
  const api = Router();

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/User',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  const handleErrorForResponse = (errorCode, res, action) => (err) => {
    if (typeof err === 'object') {
      log.error({
        message: `userService:${action}`,
        stack: err,
        trace: err.stack,
      });
    } else {
      log.error({
        message: `userService:${action}`,
        errorMessage: err,
        trace: err.stack,
      });
    }
    newrelic.noticeError(err);
    return res.status(errorCode).send(err);
  };

  /**
   * Handler for a DELETE user request
   */
  const deleteUserHandler = async (req, res) => {
    const { authenticatedUserId } = req;

    if (!authenticatedUserId || !mongoose.Types.ObjectId.isValid(authenticatedUserId))
      return res.status(401).send({ message: 'No authenticated user' });

    try {
      const isPremium = await entitlementsService.isPremium(authenticatedUserId);
      if (isPremium)
        return res.status(200).send({
          message: 'User has an active subscription',
          queued: false,
        });

      const added = await createDeletedUser(authenticatedUserId);

      if (!added)
        return res.status(409).send({
          message: 'User already queued for deletion',
          queued: true,
        });

      return res.status(202).send({
        message: 'User successfully queued for deletion',
        queued: true,
      });
    } catch (err) {
      return handleErrorForResponse(500, res, 'deleteUserHandler')(err);
    }
  };

  const getUserHandler = async (req, res) => {
    const {
      authenticatedUserId,
      query: { id, email, usID, linkedAccountFb, clientToken },
    } = req;
    let userDoc;
    try {
      if (authenticatedUserId || id) {
        const userObjectId = authenticatedUserId || id;
        if (!mongoose.Types.ObjectId.isValid(userObjectId)) {
          log.info({
            message: `The id ${userObjectId} for getUser is an invalid user identifier`,
            id: userObjectId,
            authenticatedUserId: req.authenticatedUserId,
          });
          return res.status(400).send({
            message: 'The id passed is not a valid user identifier',
          });
        }
        userDoc = await getUserById(userObjectId);
      } else if (email) {
        userDoc = await getUserByEmail(decodeURIComponent(email.toLowerCase()));
      } else if (usID) {
        userDoc = await getUserByUsID(usID);
      } else if (linkedAccountFb) {
        userDoc = await getUserByLinkedAccount(linkedAccountFb);
      } else if (clientToken) {
        userDoc = await getUserByClientAccessToken(clientToken);
      }

      if (userDoc) {
        const userObj = typeof userDoc === 'object' ? userDoc : userDoc.toObject();
        delete userObj.password;
        return res.status(200).json(userObj);
      }
      log.warn('User requested without an identifier.');
      return res
        .status(400)
        .send({ message: 'You need to specify an email, id, or usID as a query param.' });
    } catch (error) {
      return handleErrorForResponse(500, res, 'getUser')(error);
    }
  };

  const validateEmailHandler = async (req, res) => {
    const {
      query: { email },
    } = req;
    if (!email) {
      return res.status(400).send({ message: 'You need to specify an email as a query param.' });
    }

    try {
      const lowerCaseEmail = email.toLowerCase();
      const userDoc = await getUserByEmail(lowerCaseEmail);
      if (!userDoc) {
        log.debug(`User ${lowerCaseEmail} not found.`);
        return res.status(400).json({ message: 'User not found' });
      }
      const { email: userEmail, hasPassword } = userDoc;
      return res.status(200).json({ email: userEmail, hasPassword });
    } catch (error) {
      return handleErrorForResponse(500, res, 'validateEmailHandler')(error);
    }
  };

  const showTermsHandler = async (req, res) => {
    const {
      query: { id },
    } = req;
    if (!id) {
      return res.status(400).send({ message: 'You need to specify an userid as a query param.' });
    }

    try {
      const userDoc = await getUserById(id);
      if (!userDoc) {
        log.debug(`User not found.`);
        return res.status(400).json({ message: 'Invalid request' });
      }
      const { hasPassword, migratedUser } = userDoc;
      const doShowTerms = migratedUser && !hasPassword;
      const migratedBrand = migratedUser?.substring(0, migratedUser?.indexOf('_')) || null;
      const migratedBrandName = getMigratedBrandName(migratedBrand);

      return res.status(200).json({ showTerms: !!doShowTerms, migratedBrand, migratedBrandName });
    } catch (error) {
      return handleErrorForResponse(500, res, 'showTermsHandler')(error);
    }
  };

  const verifyEmailHandler = async (req, res) => {
    try {
      const {
        body: { hash, brand },
      } = req;
      const { userId, email } = decrypt(hash);
      const platform = getPlatform(req.headers['user-agent']);
      await verifyEmail({ userId, email, brand, platform });
      return res.status(200).json({ message: 'Email verification was successful' });
    } catch (error) {
      log.error({
        message: 'userService:verifiyEmailHandler',
        errorMessage: error.message,
        trace: error.stack,
      });
      newrelic.noticeError(error);
      return res.status(400).send({ message: 'Email verification failed.' });
    }
  };

  const resendEmailVerificationHandler = async (req, res) => {
    try {
      const {
        authenticatedUserId: userId,
        body: { brand },
      } = req;
      const platform = getPlatform(req.headers['user-agent']);
      const email = await triggerEmailVerification({ userId, brand, platform });
      return res.status(200).json({ message: `Email verification sent to ${email}` });
    } catch (error) {
      log.error({
        message: 'userService:resendEmailVerificationHandler',
        errorMessage: error.message,
        trace: error.stack,
      });
      return res.status(400).send({ message: 'Could not send email verification.' });
    }
  };

  const updateUserHandler = async (req, res) => {
    const { body } = req;
    if (!body || !body._id) {
      return res.status(400).send({ message: 'You need to specify an _id in your body.' });
    }
    const userId = req.authenticatedUserId || body._id;
    try {
      const updatedUser = await updateUser(userId, body);
      if (body.firstName || body.lastName || body.gender || body.locale || body.email) {
        await invalidatePasswordResetCode(userId);
        await syncUserDetails(userId, {
          firstName: updatedUser.firstName,
          lastName: updatedUser.lastName,
          email: updatedUser.email,
        });
      }
      return res.status(200).json(updatedUser);
    } catch (error) {
      return handleErrorForResponse(500, res, 'updateUser')(error);
    }
  };

  const patchUserHandler = async (req, res) => {
    const userId = req.authenticatedUserId;
    const WHITE_LIST = ['timezone'];

    const changedFields = WHITE_LIST.reduce((fields, key) => {
      if (req.body[key]) {
        const value = req.body[key];
        return {
          ...fields,
          [key]: value,
        };
      }
      return fields;
    }, {});

    await updateUser(userId, changedFields);

    return res.status(200).send({ message: 'User updated successfully' });
  };

  const changePasswordHandler = async (req, res) => {
    const { body } = req;
    const authorization = req.headers?.authorization;
    if (!body) {
      log.warn('User requested to change password without a body.');
      return res
        .status(400)
        .send({ message: 'You need to specify an _id and password (hashed) in your body.' });
    }
    const userId = req.authenticatedUserId || body._id;
    const password = body.password || body.currentPassword;
    const { newPassword } = body;
    if (newPassword?.length > MAX_PASSWORD_LENGTH) {
      log.warn(`User requested to set a password longer than ${MAX_PASSWORD_LENGTH} characters.`);
      return res
        .status(400)
        .send({ message: `Password length must not exceed ${MAX_PASSWORD_LENGTH} characters.` });
    }
    if (newPassword?.length < MIN_PASSWORD_LENGTH) {
      log.warn(
        `User requested to set a password with less than ${MIN_PASSWORD_LENGTH} characters.`,
      );
      return res.status(400).send({
        message: `Password must include a minimum of ${MIN_PASSWORD_LENGTH} characters.`,
      });
    }
    // eslint-disable-next-line camelcase
    const { client_id } = body;
    if (!userId || !password) {
      log.warn('User requested to change password without a _id, or password.');
      return res
        .status(400)
        .send({ message: 'You need to specify an _id and password (hashed) in your body.' });
    }
    const isShortLived = false;
    const platform = getPlatform(req.headers['user-agent']);
    try {
      if (body?.newPassword) {
        const updatedUser = await confirmAndChangeUserPassword(
          userId,
          password,
          newPassword,
          client_id,
          isShortLived,
          authorization,
          platform,
        );
        return res.status(200).json(updatedUser);
      }
      const updatedUser = await changeUserPassword(userId, password, platform);
      return res.status(200).json(updatedUser);
    } catch (error) {
      log.error({
        message: 'Error when changePassword',
        error: JSON.stringify(error),
      });
      newrelic.noticeError(error);
      const { status, message } = error;
      return res.status(status).send({ message });
    }
  };

  const createUserAndSettings = async (userBody, req, generatedPassword, method) => {
    const { settings } = userBody;
    const { geoCountryIso, clientName } = req;

    const userInfo = {
      ...userBody,
      locale: geoCountryIso,
    };

    const createdUser = await createUser(userInfo, generatedPassword, method, clientName);
    await userSettingsModel.createUserSettings(createdUser._id, geoCountryIso, settings);
    return createdUser;
  };

  const createUserHandler = async (req, res) => {
    const { body } = req;
    if (!body || !body.email || !body.password) {
      log.warn('User requested to create an account without a body, email, or password.');
      return res.status(400).send({ message: 'You need a body with email and password.' });
    }

    if (body.password.length > MAX_PASSWORD_LENGTH) {
      log.warn(
        `User requested to create an account with a password longer than ${MAX_PASSWORD_LENGTH} characters`,
      );
      return res
        .status(400)
        .send({ message: `Password length must not exceed ${MAX_PASSWORD_LENGTH} characters.` });
    }
    if (body.password.length < MIN_PASSWORD_LENGTH) {
      log.warn(
        `User requested to create an account with a password less than ${MIN_PASSWORD_LENGTH} characters`,
      );
      return res.status(400).send({
        message: `Password must include a minimum of ${MIN_PASSWORD_LENGTH} characters.`,
      });
    }

    const validationErrors = validateUser(body);
    if (validationErrors) {
      log.trace({ message: 'Validation error for creating new account', stack: validationErrors });
      return res.status(400).send(validationErrors);
    }

    body.email = body.email.toLowerCase();

    const foundUser = await getUserByEmail(body.email.toLowerCase());
    if (foundUser) {
      log.debug(`User ${body.email} requested to create an account with an existing email.`);
      return res.status(400).send({ message: `User already exists with email: ${body.email}` });
    }

    try {
      const {
        query: { isShortLived },
      } = req;

      let clientName;
      if (body.client_id) {
        try {
          const { clientName: authClientName } = await authService.getClientNameFromClientId(
            body.client_id,
          );
          log.info(`Client Name returned as ${authClientName} for: ${body.client_id}`);
          clientName = authClientName;
        } catch (error) {
          log.warn(`Client Name request failed or was not found for: ${body.client_id}`);
          clientName = null;
        }
      }
      // eslint-disable-next-line no-param-reassign
      req.clientName = clientName;

      const createdUser = await createUserAndSettings(body, req);
      trackAccountGeoIP(req.ip, createdUser);

      const authorization = req.headers?.authorization;
      const token = await authService.getTokenForUser(
        body.email.toLowerCase(),
        body.password,
        body.client_id,
        isShortLived,
        authorization,
      );
      return res.status(200).json({ user: createdUser, token });
    } catch (err) {
      return handleErrorForResponse(500, res, 'createUser')(err);
    }
  };

  const createorUpdateIntegrationHandler = async (req, res) => {
    const { body } = req;
    if (!body || !body.email) {
      log.warn('User requested without an identifier.');
      return res.status(400).send({ message: 'You need to specify an email as a query param.' });
    }

    /* find existing user by email */
    const foundUser = await getUserByEmail(body.email.toLowerCase());
    if (!foundUser) {
      /* create new user */
      /* generate password for new integrated user */
      const generatedPassword = Math.random().toString(36).slice(-8);
      body.password = generatedPassword;

      const validationErrors = validateUser(body);
      if (validationErrors) {
        log.trace({
          message: 'Validation error for creating new account',
          stack: validationErrors,
        });
        return res.status(400).send(validationErrors);
      }

      body.email = body.email.toLowerCase();

      try {
        const createdUser = await createUserAndSettings(body, req, generatedPassword, 'facebook');
        trackAccountGeoIP(req.ip, createdUser, null);
        return res.status(200).json({ user: createdUser });
      } catch (err) {
        return handleErrorForResponse(500, res, 'createIntegratedUser')(err);
      }
    }

    try {
      const updatedUser = await updateUser(foundUser._id, {
        linkedAccount: body.linkedAccount,
      });
      syncUserDetails(foundUser._id, {
        firstName: updatedUser.firstName,
        lastName: updatedUser.lastName,
        email: updatedUser.email,
      });
      return res.status(200).json(updatedUser);
    } catch (err) {
      return handleErrorForResponse(500, res, 'updateUser')(err);
    }
  };

  const getEntitlementsHandler = (_, res) => res.status(410).send('Gone');
  const getUserIdentity = async (_, res) => res.status(410).send('Gone');

  api.delete('/', trackRequest, authenticateRequest({ userIdRequired: true }), deleteUserHandler);
  api.get('/', trackRequest, getUserHandler);
  api.put('/', trackRequest, updateUserHandler);
  api.post('/', trackRequest, createUserHandler);
  api.patch(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    wrapErrors(patchUserHandler),
  );
  api.post('/CreateIntegratedUser', trackRequest, createorUpdateIntegrationHandler);
  api.put('/ChangePassword', trackRequest, changePasswordHandler);
  api.get('/Entitlements', trackRequest, getEntitlementsHandler);
  api.get('/ValidateEmail', trackRequest, validateEmailHandler);
  api.post('/verify-email', trackRequest, verifyEmailHandler);
  api.post('/resend-email-verification', trackRequest, resendEmailVerificationHandler);
  api.use('/identify', trackRequest, getUserIdentity);
  api.use('/Settings', userSettings());
  api.get('/show-terms', trackRequest, showTermsHandler);

  return api;
}
