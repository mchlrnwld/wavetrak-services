import express from 'express';
import bodyParser from 'body-parser';
import chai, { expect } from 'chai';
import sinon from 'sinon';
import { authenticateRequest } from '@surfline/services-common';
import * as model from '../model/UserSettings';
import { userSettings } from './userSettings';

describe('user Settings route', () => {
  const app = express();

  before(() => {
    process.env.NEW_RELIC_ENABLED = false;
    process.env.NEW_RELIC_NO_CONFIG_FILE = true;
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(authenticateRequest());
    app.use('/userSettings', userSettings());
  });

  beforeEach(() => {
    sinon.stub(model, 'getUserSettings');
    sinon.stub(model, 'updateUserSettings');
  });

  afterEach(() => {
    model.getUserSettings.restore();
    model.updateUserSettings.restore();
  });

  describe('POST /recentlyvisited', () => {
    it('should require a type, id and name', async () => {
      try {
        const res = await chai
          .request(app)
          .post('/userSettings/recentlyVisited')
          .send({ type: 'spots' });
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response.body.message).to.equal('type, _id, and name are required.');
      }
      expect(model.getUserSettings).not.to.have.been.called();
      expect(model.updateUserSettings).not.to.have.been.called();
    });

    it('should add a recently visited spot or subregion', async () => {
      const initial = {
        recentlyVisited: {
          spots: [],
          subregions: [],
        },
      };
      const expected = {
        recentlyVisited: {
          spots: [{ _id: 'spotObjectId', name: 'Spot Name' }],
          subregions: [],
        },
      };
      model.getUserSettings.resolves(initial);
      model.updateUserSettings.resolves(expected);
      const res = await chai
        .request(app)
        .post('/userSettings/recentlyvisited')
        .set('x-auth-userId', 'authenticatedUserId')
        .send({
          type: 'spots',
          _id: 'spotObjectId',
          name: 'Spot Name',
        });
      expect(model.getUserSettings).to.have.been.calledOnce();
      expect(model.updateUserSettings).to.have.been.calledOnce();
      expect(model.getUserSettings).to.have.been.calledWith('authenticatedUserId');
      expect(model.updateUserSettings).to.have.been.calledWith('authenticatedUserId', expected);
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal(expected);
    });

    it('should not add a duplicate spot or subregion', async () => {
      const initial = {
        recentlyVisited: {
          spots: [
            { _id: 'spot1', name: 'Spot 1' },
            { _id: 'spot2', name: 'Spot 2' },
          ],
          subregions: [{ _id: 'subregion1', name: 'Subreagion 1' }],
        },
      };
      const expected = {
        recentlyVisited: {
          spots: [
            { _id: 'spot1', name: 'Spot 1' },
            { _id: 'spot2', name: 'Spot 2' },
          ],
          subregions: [{ _id: 'subregion1', name: 'Subregion 1' }],
        },
      };
      model.getUserSettings.resolves(initial);
      model.updateUserSettings.resolves(expected);
      const res = await chai
        .request(app)
        .post('/userSettings/recentlyvisited')
        .set('x-auth-userId', 'authenticatedUserId')
        .send({
          type: 'subregions',
          _id: 'subregion1',
          name: 'Subregion 1',
        });
      expect(model.getUserSettings).to.have.been.calledOnce();
      expect(model.updateUserSettings).to.have.been.calledOnce();
      expect(model.getUserSettings).to.have.been.calledWith('authenticatedUserId');
      expect(model.updateUserSettings).to.have.been.calledWith('authenticatedUserId', expected);
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal(expected);
    });

    it('should move a recently visited spot or subregion to the front of the list', async () => {
      const initial = {
        recentlyVisited: {
          spots: [
            { _id: 'spot1', name: 'Spot 1' },
            { _id: 'spot2', name: 'Spot 2' },
            { _id: 'spot3', name: 'Spot 3' },
            { _id: 'spot4', name: 'Spot 4' },
          ],
          subregions: [{ _id: 'subregion1', name: 'Subreagion 1' }],
        },
      };
      const expected = {
        recentlyVisited: {
          spots: [
            { _id: 'spot2', name: 'Spot 2' },
            { _id: 'spot1', name: 'Spot 1' },
            { _id: 'spot3', name: 'Spot 3' },
            { _id: 'spot4', name: 'Spot 4' },
          ],
          subregions: [{ _id: 'subregion1', name: 'Subreagion 1' }],
        },
      };
      model.getUserSettings.resolves(initial);
      model.updateUserSettings.resolves(expected);
      const res = await chai
        .request(app)
        .post('/userSettings/recentlyvisited')
        .set('x-auth-userId', 'authenticatedUserId')
        .send({
          type: 'spots',
          _id: 'spot2',
          name: 'Spot 2',
        });
      expect(model.getUserSettings).to.have.been.calledOnce();
      expect(model.updateUserSettings).to.have.been.calledOnce();
      expect(model.getUserSettings).to.have.been.calledWith('authenticatedUserId');
      expect(model.updateUserSettings).to.have.been.calledWith('authenticatedUserId', expected);
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal(expected);
    });
  });
});
