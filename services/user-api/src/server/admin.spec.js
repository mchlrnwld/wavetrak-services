import express from 'express';
import bodyParser from 'body-parser';
import chai, { expect } from 'chai';
import mongoose from 'mongoose';
import sinon from 'sinon';
import * as DeletedUsers from '../model/deleted-users/DeletedUsers';
import * as model from '../model/User';
import UserModel from '../model/UserModel';
import * as userSettings from '../model/UserSettings';
import UserSettingsModel from '../model/UserSettingsModel';
import * as entitlements from '../external/entitlements';
import * as metering from '../external/metering';
import * as favorites from '../external/favorites';
import * as subscriptions from '../external/subscriptions';
import * as integrations from '../external/integrations';
import * as linkedClients from '../external/linkedClients';
import * as auth from '../external/auth';
import * as trackEvent from '../common/trackEvent';
import { user } from './user';
import { adminUser } from './adminUser';

describe('admin user route', () => {
  let modelGetUserByNameEmailSearchStub;
  let modelGetUserByNameEmailSearchPromise;
  let modelGetUserByEmailStub;
  let modelGetUserByEmailPromise;
  let modelUpdateUserEmailStub;
  let modelUpdateUserEmailPromise;
  let modelGetUserByIdStub;
  let modelGetUserByIdPromise;
  let syncStripeDetailsStub;
  let syncStripeDetailsPromise;
  let createSegmentRegulationStub;
  let deleteCustomerIOUserStub;
  let deleteAmplitudeUserStub;
  let deleteBrazeUserStub;
  let modelVerifyEmailStub;
  let modelVerifyEmailPromise;

  const app = express();

  before(() => {
    process.env.NEW_RELIC_ENABLED = false;
    process.env.NEW_RELIC_NO_CONFIG_FILE = true;
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use('/user', user());
    app.use('/admin', adminUser());
  });

  beforeEach(() => {
    modelGetUserByNameEmailSearchStub = sinon.stub(model, 'getUserByNameEmailSearch');
    modelGetUserByNameEmailSearchPromise = modelGetUserByNameEmailSearchStub;
    modelGetUserByEmailStub = sinon.stub(model, 'getUserByEmail');
    modelGetUserByEmailPromise = modelGetUserByEmailStub;
    modelUpdateUserEmailStub = sinon.stub(model, 'updateUser');
    modelUpdateUserEmailPromise = modelUpdateUserEmailStub;
    modelGetUserByIdStub = sinon.stub(model, 'getUserById');
    modelGetUserByIdPromise = modelGetUserByIdStub;
    modelVerifyEmailStub = sinon.stub(model, 'verifyEmail');
    modelVerifyEmailPromise = modelVerifyEmailStub;
    syncStripeDetailsStub = sinon.stub(subscriptions, 'syncUserDetails');
    syncStripeDetailsPromise = syncStripeDetailsStub;
    createSegmentRegulationStub = sinon.stub(integrations, 'createSegmentRegulation');
    deleteCustomerIOUserStub = sinon.stub(integrations, 'deleteCustomerIOUser');
    deleteAmplitudeUserStub = sinon.stub(integrations, 'deleteAmplitudeUser');
    deleteBrazeUserStub = sinon.stub(integrations, 'deleteBrazeUser');
    sinon.stub(UserModel, 'updateMany');
    sinon.stub(UserModel, 'findByIdAndDelete');
    sinon.stub(UserSettingsModel, 'findOneAndDelete');
  });
  afterEach(() => {
    modelGetUserByNameEmailSearchStub.restore();
    modelGetUserByEmailStub.restore();
    modelUpdateUserEmailStub.restore();
    modelGetUserByIdStub.restore();
    modelVerifyEmailStub.restore();
    syncStripeDetailsStub.restore();
    createSegmentRegulationStub.restore();
    deleteCustomerIOUserStub.restore();
    deleteAmplitudeUserStub.restore();
    deleteBrazeUserStub.restore();
    UserModel.updateMany.restore();
    UserModel.findByIdAndDelete.restore();
    UserSettingsModel.findOneAndDelete.restore();
  });

  it('should initialize as an object', () => {
    expect(user).to.exist();
  });

  it('should return a user when requesting with id', (done) => {
    chai
      .request(app)
      .get('/admin/user')
      .query({ userId: '56f9eb8646f566a11a5ce971' })
      .end((err, res) => {
        expect(err).to.be.null();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        const {
          body: {
            user: { email },
          },
        } = res;
        expect(email).to.equal('scott@test.com');
        done();
      });

    modelGetUserByIdPromise.resolves({
      _id: '56f9eb8646f566a11a5ce971',
      firstName: 'Scott',
      lastName: 'Diorio',
      email: 'scott@test.com',
      password: '$2a$10$GGv3.QDdb54spIKzX2Vjr.Ed7aIvoPnqjy5vcjKsOmJNdDeOYr77i',
      gender: null,
      usID: 123456,
      linkedAccount: {
        facebook: {
          id: null,
          ageRange: {
            min: null,
          },
        },
      },
      score: 12.5,
      locale: 'en-US',
      brand: 'sl',
      modifiedDate: '2016-03-29T02:42:14.665Z',
      isActive: true,
    });
  });
  it('should return a user when requesting with email', (done) => {
    chai
      .request(app)
      .get('/Admin/Users')
      .query({ search: 'Scott' })
      .end((err, res) => {
        expect(err).to.be.null();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        done();
      });

    modelGetUserByNameEmailSearchPromise.resolves([
      {
        _id: '56f9eb8646f566a11a5ce971',
        firstName: 'Scott',
        lastName: 'Diorio',
        email: 'scott@test.com',
        password: '$2a$10$GGv3.QDdb54spIKzX2Vjr.Ed7aIvoPnqjy5vcjKsOmJNdDeOYr77i',
        gender: null,
        usID: 123456,
        linkedAccount: {
          facebook: {
            id: null,
            ageRange: {
              min: null,
            },
          },
        },
        score: 12.5,
        locale: 'en-US',
        brand: 'sl',
        modifiedDate: '2016-03-29T02:42:14.665Z',
        isActive: true,
      },
    ]);
  });

  it('should not return a user when requesting without params', (done) => {
    chai
      .request(app)
      .get('/admin/users')
      .end((err, res) => {
        expect(res.error).to.not.be.null();
        expect(res).to.have.status(400);
        expect(res).to.be.json();
        done();
      });
  });

  it('should return a user with an updated email when updating email', (done) => {
    chai
      .request(app)
      .put('/Admin/Users/123')
      .send({ email: 'somenew@email.com' })
      .end((err, res) => {
        expect(err).to.be.null();
        expect(res).to.have.status(200);
        expect(res.body).to.deep.equal({
          _id: '123',
          firstName: 'Jeremy',
          lastName: 'Monson',
          email: 'somenew@email.com',
          gender: null,
          locale: 'en-US',
        });
        done();
      });

    modelGetUserByEmailPromise.resolves(null);
    modelGetUserByIdPromise.resolves({
      _id: '123',
      firstName: 'Jeremy',
      lastName: 'Monson',
      email: 'jeremy@test.com',
      gender: null,
      locale: 'en-US',
    });
    modelUpdateUserEmailPromise.resolves({
      _id: '123',
      firstName: 'Jeremy',
      lastName: 'Monson',
      email: 'somenew@email.com',
      gender: null,
      locale: 'en-US',
    });
    syncStripeDetailsPromise.resolves();
  });

  it('should return an error when requesting without a body', (done) => {
    chai
      .request(app)
      .put('/admin/users/123')
      .send()
      .end((err, res) => {
        expect(res.error).to.not.be.null();
        expect(res).to.have.status(400);
        expect(res).to.be.json();
        done();
      });
  });

  it('should roll back if stripe sync fails', (done) => {
    chai
      .request(app)
      .put('/admin/users/123')
      .send({ email: 'thiswontwork@email.com' })
      .end((err, res) => {
        expect(res.error).to.not.be.null();
        expect(res).to.have.status(500);
        expect(res.body).to.include({ message: 'Could not update user in stripe.' });
        expect(modelUpdateUserEmailStub).to.have.been.calledTwice();
        done();
      });

    modelGetUserByEmailPromise.resolves(null);
    modelGetUserByIdPromise.resolves({
      _id: '123',
      firstName: 'Jeremy',
      lastName: 'Monson',
      email: 'jeremy@test.com',
      gender: null,
      locale: 'en-US',
    });
    modelUpdateUserEmailPromise.resolves({
      _id: '123',
      firstName: 'Jeremy',
      lastName: 'Monson',
      email: 'somenew@email.com',
      gender: null,
      locale: 'en-US',
    });
    syncStripeDetailsPromise.rejects({
      stack: 'somestacktrace',
    });
  });

  describe('/admin/user DELETE', () => {
    let deleteBwFavoritesStub;
    let deleteFavoritesStub;
    let deleteMeterStub;
    let deleteSettingsStub;
    let deleteUserStub;
    let deleteSubscriptionUserStub;
    let invalidateTokenStub;
    let invalidateGarminTokenStub;
    let isPremiumStub;
    let markUserAsDeletedStub;
    let trackDeletedAccountStub;
    let transactionStub;

    const sessionFixture = {
      withTransaction: async (fn) => fn(),
      endSession: sinon.stub(),
    };

    const accessToken = 'a9a6a829-bb8b-4851-92c7-088c9dbcee40';
    const tokenSecret = 'Y1eDfrE5usk4w8BOjHKyA1duf5jwut0i5T7';
    const userDocFixture = {
      _id: '56f9eb8646f566a11a5ce971',
      brand: 'sl',
      linkedClients: [
        {
          name: 'Garmin',
          token: accessToken,
          tokenSecret,
        },
      ],
      toObject() {
        return this;
      },
    };

    beforeEach(() => {
      deleteBwFavoritesStub = sinon.stub(favorites, 'deleteBwFavorites');
      deleteFavoritesStub = sinon.stub(favorites, 'deleteFavorites');
      deleteMeterStub = sinon.stub(metering, 'deleteMeter');
      deleteSettingsStub = sinon.stub(userSettings, 'deleteUserSettings');
      deleteUserStub = sinon.stub(model, 'deleteUser');
      deleteSubscriptionUserStub = sinon.stub(subscriptions, 'deleteSubscriptionUser');

      invalidateTokenStub = sinon.stub(auth, 'invalidateToken');
      invalidateGarminTokenStub = sinon.stub(linkedClients, 'invalidateGarminToken');
      isPremiumStub = sinon.stub(entitlements, 'isPremium');
      markUserAsDeletedStub = sinon.stub(DeletedUsers, 'markUserAsDeleted');
      trackDeletedAccountStub = sinon.stub(trackEvent, 'trackDeletedAccount');
      transactionStub = sinon.stub(mongoose, 'startSession');
    });

    afterEach(() => {
      deleteBwFavoritesStub.restore();
      deleteFavoritesStub.restore();
      deleteMeterStub.restore();
      deleteSettingsStub.restore();
      deleteUserStub.restore();
      deleteSubscriptionUserStub.restore();

      invalidateTokenStub.restore();
      invalidateGarminTokenStub.restore();
      isPremiumStub.restore();
      markUserAsDeletedStub.restore();
      trackDeletedAccountStub.restore();
      transactionStub.restore();

      sessionFixture.endSession.reset();
    });

    it('should delete the documents and integrations associated for a user', async () => {
      transactionStub.returns(sessionFixture);
      isPremiumStub.returns(false);
      deleteUserStub.returns(userDocFixture);
      markUserAsDeletedStub.returns({});
      const userId = '56f9eb8646f566a11a5ce971';
      const res = await chai.request(app).delete(`/Admin/Users/${userId}`);
      expect(deleteUserStub).to.have.been.calledWith(userId, { session: sessionFixture });
      expect(deleteSettingsStub).to.have.been.calledWith(userId, { session: sessionFixture });

      expect(deleteMeterStub).to.have.been.calledOnceWithExactly(userId);
      expect(deleteFavoritesStub).to.have.been.calledOnceWithExactly(userId);
      expect(deleteBwFavoritesStub).to.have.been.calledOnceWithExactly(userId);
      expect(deleteSubscriptionUserStub).to.have.been.calledOnceWithExactly(userId);
      expect(createSegmentRegulationStub).to.have.been.calledOnceWithExactly(userId);
      expect(deleteCustomerIOUserStub).to.have.been.calledOnceWithExactly(userId);
      expect(deleteAmplitudeUserStub).to.have.been.calledOnceWithExactly(userId);
      expect(deleteBrazeUserStub).to.have.been.calledOnceWithExactly(userId);
      expect(invalidateTokenStub).to.have.been.calledOnceWithExactly(userId);
      expect(invalidateGarminTokenStub).to.have.been.calledOnceWithExactly(
        accessToken,
        tokenSecret,
      );
      expect(markUserAsDeletedStub).to.have.been.calledOnceWithExactly(userId);
      expect(trackDeletedAccountStub).to.have.been.calledOnce();
      expect(trackDeletedAccountStub).to.have.been.calledWithMatch(
        sinon.match({
          brand: userDocFixture.brand,
          deletedRequestedAt: undefined,
          userId: userDocFixture._id,
        }),
      );
      expect(sessionFixture.endSession).to.have.been.calledOnceWithExactly();
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal({
        deleted: true,
        message: 'User was successfully deleted',
      });
    });

    it('should not commit the Mongo transaction if there was a subsequent failure', async () => {
      transactionStub.returns(sessionFixture);
      deleteBwFavoritesStub.throws('error');
      const userId = '56f9eb8646f566a11a5ce971';
      const res = await chai.request(app).delete(`/Admin/Users/${userId}`);
      expect(deleteUserStub).to.have.been.calledWith(userId, { session: sessionFixture });
      expect(markUserAsDeletedStub).to.not.have.been.called();
      expect(trackDeletedAccountStub).to.not.have.been.called();
      expect(sessionFixture.endSession).to.not.have.been.called();
      expect(res).to.have.status(500);
    });

    it('should not invalidate tokens if there was an error deleting the user', async () => {
      transactionStub.returns(sessionFixture);
      deleteBwFavoritesStub.throws('error');
      const userId = '56f9eb8646f566a11a5ce971';
      const res = await chai.request(app).delete(`/Admin/Users/${userId}`);
      expect(deleteUserStub).to.have.been.calledWith(userId, { session: sessionFixture });
      expect(invalidateTokenStub).to.not.have.been.called();
      expect(res).to.have.status(500);
    });

    it('should not delete the user if they have a premium subscription', async () => {
      transactionStub.returns(sessionFixture);
      isPremiumStub.returns(true);
      const userId = '56f9eb8646f566a11a5ce971';
      const res = await chai.request(app).delete(`/Admin/Users/${userId}`);
      expect(deleteUserStub).to.not.have.been.called();
      expect(markUserAsDeletedStub).to.not.have.been.called();
      expect(invalidateTokenStub).to.not.have.been.called();
      expect(sessionFixture.endSession).to.not.have.been.called();
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal({
        deleted: false,
        message: 'User has a premium entitlement',
      });
    });
  });

  describe('/admin/users/deleted GET', () => {
    let getPendingDeletedUsersStub;
    let clock;

    beforeEach(() => {
      getPendingDeletedUsersStub = sinon.stub(DeletedUsers, 'getPendingDeletedUsers');
      clock = sinon.useFakeTimers(new Date('2021-01-02T00:00:00.000Z'));
    });

    afterEach(() => {
      getPendingDeletedUsersStub.restore();
      clock.restore();
    });

    it('should fetch pending deleted users', async () => {
      getPendingDeletedUsersStub.returns([
        {
          user: '56f9eb8646f566a11a5ce971',
          deleteRequestedAt: new Date('2020-01-01T00:00:00.000Z'),
          deletedAt: null,
        },
      ]);
      const res = await chai
        .request(app)
        .get(
          `/admin/users/deleted?date=${encodeURIComponent(
            new Date('2021-01-01T00:00:00.000Z').toString(),
          )}`,
        )
        .send();
      expect(getPendingDeletedUsersStub).to.have.been.called.calledOnceWithExactly(
        new Date('2021-01-01T00:00:00.000Z'),
      );
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal([
        {
          user: '56f9eb8646f566a11a5ce971',
          deleteRequestedAt: '2020-01-01T00:00:00.000Z',
        },
      ]);
    });

    it('should use new Date as default', async () => {
      const res = await chai.request(app).get(`/admin/users/deleted`).send();
      expect(getPendingDeletedUsersStub).to.have.been.called.calledOnceWithExactly(
        new Date('2021-01-02T00:00:00.000Z'),
      );
      expect(res).to.have.status(200);
    });
  });

  describe('/admin/garmin/deregistration', () => {
    it('should process a valid ping', async () => {
      UserModel.updateMany.resolves();
      const res = await chai
        .request(app)
        .post('/Admin/garmin/deregister')
        .send({ deregistrations: [{ userAccessToken: '123' }, { userAccessToken: '345' }] });

      expect(res).to.have.status(200);
      expect(UserModel.updateMany).to.have.been.calledOnceWithExactly(
        { 'linkedClients.token': { $in: ['123', '345'] } },
        { $pull: { linkedClients: { token: { $in: ['123', '345'] } } } },
        { multi: true },
      );
    });

    it('should error for an invalid ping', async () => {
      const res = await chai.request(app).post('/Admin/garmin/deregister').send({});

      expect(res).to.have.status(400);
      expect(UserModel.updateMany).to.have.not.been.called();
    });
  });

  it('should set false isEmailVerified property of the user to true', async () => {
    modelVerifyEmailPromise.resolves({
      _id: '123456',
      firstName: 'test',
      lastName: 'Test',
      email: 'test@test.com',
      isEmailVerified: true,
    });
    const res = await chai
      .request(app)
      .post('/Admin/user/verify-email/123456')
      .send({ email: 'test@test.com', brand: 'sl' });
    expect(modelVerifyEmailStub).to.have.been.calledOnceWithExactly({
      userId: '123456',
      email: 'test@test.com',
      brand: 'sl',
    });
    expect(res).to.have.status(200);
    expect(res.body).to.deep.equal({
      _id: '123456',
      firstName: 'test',
      lastName: 'Test',
      email: 'test@test.com',
      isEmailVerified: true,
    });
  });

  it('should return 400 error if user is not found', async () => {
    modelVerifyEmailPromise.rejects('Invalid email verification. User not found');
    const res = await chai.request(app).post('/Admin/user/verify-email/123456');
    expect(res).to.have.status(400);
    expect(res.body).to.deep.equal({
      error: {
        name: 'Invalid email verification. User not found',
      },
      message: 'Could not verify user',
    });
  });

  it('should return 404 error in case of no UserId', async () => {
    const res = await chai.request(app).post('/Admin/user/verify-email/');
    expect(res).to.have.status(404);
  });
});
