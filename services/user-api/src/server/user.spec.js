import express from 'express';
import bodyParser from 'body-parser';
import chai, { expect } from 'chai';
import sinon from 'sinon';
import {
  errorHandlerMiddleware,
  MAX_PASSWORD_LENGTH,
  MIN_PASSWORD_LENGTH,
} from '@surfline/services-common';
import * as model from '../model/User';
import * as settings from '../model/UserSettings';
import { user } from './user';
import * as authService from '../external/auth';
import * as geoTargetService from '../external/geotarget';
import * as encryption from '../common/encryption';
import * as track from '../common/trackEvent';
import userModel from '../model/UserModel';
import * as DeletedUsers from '../model/deleted-users/DeletedUsers';
import * as entitlements from '../external/entitlements';
import * as passwordReset from '../external/passwordReset';
import * as subscriptions from '../external/subscriptions';

describe('user route', () => {
  let modelGetUserByEmailStub;
  let modelGetUserByIdStub;
  let modelGetUserByLinkedAccountStub;
  let modelUpdateUserPromise;
  let modelUpdateUserStub;
  let modelChangePasswordPromise;
  let modelChangePasswordStub;
  let modelConfirmAndChangePasswordPromise;
  let modelConfirmAndChangePasswordStub;
  let modelCreateUserStub;
  let modelCreateUserSettingsStub;
  let authServiceClientNameStub;
  let authServiceTokenStub;
  let getTargetServiceRegionStub;
  let modelGetUserByClientAccessTokenStub;
  let invalidatePasswordResetCodeStub;
  let syncUserDetailsStub;

  /** @type {sinon.SinonStub} */
  let modelUserFindOneStub;

  /** @type {sinon.SinonStub} */
  let decryptStub;

  /** @type {sinon.SinonStub} */
  let trackEmailVerifiedStub;

  const app = express();

  before(() => {
    process.env.NEW_RELIC_ENABLED = false;
    process.env.NEW_RELIC_NO_CONFIG_FILE = true;
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use('/user', user());
    app.use(errorHandlerMiddleware());
  });

  beforeEach(() => {
    modelGetUserByEmailStub = sinon.stub(model, 'getUserByEmail');
    modelGetUserByIdStub = sinon.stub(model, 'getUserById');
    modelGetUserByLinkedAccountStub = sinon.stub(model, 'getUserByLinkedAccount');
    modelGetUserByClientAccessTokenStub = sinon.stub(model, 'getUserByClientAccessToken');
    modelUpdateUserStub = sinon.stub(model, 'updateUser');
    modelUpdateUserPromise = modelUpdateUserStub;
    modelChangePasswordStub = sinon.stub(model, 'changeUserPassword');
    modelChangePasswordPromise = modelChangePasswordStub;
    modelConfirmAndChangePasswordStub = sinon.stub(model, 'confirmAndChangeUserPassword');
    modelConfirmAndChangePasswordPromise = modelConfirmAndChangePasswordStub;
    modelCreateUserStub = sinon.stub(model, 'createUser');
    modelCreateUserSettingsStub = sinon.stub(settings, 'createUserSettings');
    authServiceClientNameStub = sinon.stub(authService, 'getClientNameFromClientId');
    authServiceTokenStub = sinon.stub(authService, 'getTokenForUser');
    getTargetServiceRegionStub = sinon.stub(geoTargetService, 'getRegion');
    modelUserFindOneStub = sinon.stub(userModel, 'findOne');
    decryptStub = sinon.stub(encryption, 'decrypt');
    trackEmailVerifiedStub = sinon.stub(track, 'trackEmailVerified');
    invalidatePasswordResetCodeStub = sinon.stub(passwordReset, 'invalidatePasswordResetCode');
    syncUserDetailsStub = sinon.stub(subscriptions, 'syncUserDetails');
  });

  afterEach(() => {
    modelGetUserByEmailStub.restore();
    modelGetUserByIdStub.restore();
    modelGetUserByLinkedAccountStub.restore();
    modelGetUserByClientAccessTokenStub.restore();
    modelUpdateUserStub.restore();
    modelChangePasswordStub.restore();
    modelConfirmAndChangePasswordStub.restore();
    modelCreateUserStub.restore();
    modelCreateUserSettingsStub.restore();
    authServiceClientNameStub.restore();
    authServiceTokenStub.restore();
    getTargetServiceRegionStub.restore();
    modelUserFindOneStub.restore();
    decryptStub.restore();
    trackEmailVerifiedStub.restore();
    invalidatePasswordResetCodeStub.restore();
    syncUserDetailsStub.restore();
  });

  it('should return a user when requesting with email', (done) => {
    chai
      .request(app)
      .get('/user')
      .query({ email: 'test@test.com' })
      .end((err, res) => {
        expect(err).to.be.null();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        done();
      });
    modelGetUserByEmailStub.returns({ _id: 111 });
  });

  it('should return a user when requesting with linkedAccountId', (done) => {
    chai
      .request(app)
      .get('/user')
      .query({ linkedAccountFb: '1111111111111' })
      .end((err, res) => {
        expect(err).to.be.null();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        done();
      });
    modelGetUserByLinkedAccountStub.returns({ _id: 111 });
  });

  it('should return a user when requesting with client access token', (done) => {
    modelGetUserByClientAccessTokenStub.returns({ _id: 111 });
    chai
      .request(app)
      .get('/user')
      .query({ clientToken: '1234' })
      .end((err, res) => {
        expect(err).to.be.null();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal({ _id: 111 });
        done();
      });
  });

  it('should not return a user when requesting without params', (done) => {
    chai
      .request(app)
      .get('/user')
      .end((err, res) => {
        expect(res.error).to.not.be.null();
        expect(res).to.have.status(400);
        expect(res).to.be.json();
        done();
      });
  });

  it('should update a user when requesting with _id in body', (done) => {
    invalidatePasswordResetCodeStub.resolves({});
    syncUserDetailsStub.resolves({});
    chai
      .request(app)
      .put('/user')
      .send({ _id: '111111', email: 'email@domain.com' })
      .end((err, res) => {
        expect(err).to.be.null();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(invalidatePasswordResetCodeStub).to.have.been.calledOnce();
        done();
      });
    modelUpdateUserPromise.resolves({ _id: 111 });
  });

  it('should not update a user when requesting without params', (done) => {
    chai
      .request(app)
      .put('/user')
      .end((err, res) => {
        expect(res.error).to.not.be.null();
        expect(res).to.have.status(400);
        expect(res).to.be.json();
        done();
      });
  });

  it('should update a user password when requesting with _id and password in body', (done) => {
    chai
      .request(app)
      .put('/user/ChangePassword')
      .send({ _id: '111111', password: 'allNewFirstName' })
      .end((err, res) => {
        expect(err).to.be.null();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        done();
      });
    modelChangePasswordPromise.resolves({ _id: 111 });
  });

  it('should update a user password when requesting with _id and newPassword in body', (done) => {
    chai
      .request(app)
      .put('/user/ChangePassword')
      .send({ _id: '111111', currentPassword: 'anOldPassword', newPassword: 'aNewPassword' })
      .end((err, res) => {
        expect(err).to.be.null();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        done();
      });
    modelConfirmAndChangePasswordPromise.resolves({ _id: 111 });
  });

  it('should not update a user when requesting without password', (done) => {
    chai
      .request(app)
      .put('/user/ChangePassword')
      .send({ _id: '111111' })
      .end((err, res) => {
        expect(res.error).to.not.be.null();
        expect(res).to.have.status(400);
        expect(res).to.be.json();
        done();
      });
  });

  it('should not update a user when requesting with too long password', (done) => {
    const password = '#'.repeat(MAX_PASSWORD_LENGTH + 1);
    chai
      .request(app)
      .put('/user/ChangePassword')
      .send({ _id: '111111', currentPassword: 'anOldPassword', newPassword: password })
      .end((err, res) => {
        expect(res.error).to.not.be.null();
        expect(res).to.have.status(400);
        expect(res).to.be.json();
        done();
      });
  });

  it('should not update a user when requesting with password length less than 6 chars', (done) => {
    const password = '#'.repeat(MIN_PASSWORD_LENGTH - 1);
    chai
      .request(app)
      .put('/user/ChangePassword')
      .send({ _id: '111111', currentPassword: 'anOldPassword', newPassword: password })
      .end((err, res) => {
        expect(res.error).to.not.be.null();
        expect(res).to.have.status(400);
        expect(res).to.be.json();
        done();
      });
  });

  it('should not create a user when requesting with invalid body params', (done) => {
    chai
      .request(app)
      .post('/user')
      .send({ firstName: 'f', lastName: 'l', password: 'abcdefg', email: 'e' })
      .end((err, res) => {
        expect(res.body.errors.email).to.not.be.null();
        expect(res.body.errors.password).to.not.be.null();
        expect(res).to.have.status(400);
        expect(res).to.be.json();
        done();
      });
  });

  it('should not create a user with password longer than max length', (done) => {
    const password = '#'.repeat(MAX_PASSWORD_LENGTH + 1);
    chai
      .request(app)
      .post('/user')
      .send({ firstName: 'f', lastName: 'l', password, email: 'e' })
      .end((err, res) => {
        expect(res.body.error).to.not.be.null();
        expect(res).to.have.status(400);
        expect(res).to.be.json();
        done();
      });
  });

  it('should create a new user with client_id in body', (done) => {
    chai
      .request(app)
      .post('/user')
      .send({
        firstName: 'michael',
        lastName: 'malchak',
        password: 'beachlive',
        email: 'mmalchak@surfline.com',
        client_id: '123',
      })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
    modelGetUserByEmailStub.returns(null);
    authServiceClientNameStub.resolves({ clientName: 'Surfline Web' });
    modelCreateUserStub.resolves({ email: 'mmalchak@surfline.com ' });
    modelCreateUserSettingsStub.resolves({});
    authServiceTokenStub.resolves({ token: '1234' });
    getTargetServiceRegionStub.resolves(null);
  });

  it('should create a new user with invalid client_id in body', (done) => {
    chai
      .request(app)
      .post('/user')
      .send({
        firstName: 'michael',
        lastName: 'malchak',
        password: 'beachlive',
        email: 'mmalchak@surfline.com',
        client_id: '123',
      })
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
    modelGetUserByEmailStub.returns(null);
    authServiceClientNameStub.rejects({ message: 'Internal Server Error' });
    modelCreateUserStub.resolves({ email: 'mmalchak@surfline.com ' });
    modelCreateUserSettingsStub.resolves({});
    authServiceTokenStub.resolves({ token: '1234' });
    getTargetServiceRegionStub.resolves(null);
  });

  it('should eject early when creating a user without required params', (done) => {
    chai
      .request(app)
      .post('/user')
      .send({ firstName: '', lastName: '', password: '', email: '' })
      .end((err, res) => {
        expect(res.body).to.deep.equal({
          message: 'You need a body with email and password.',
        });
        expect(res).to.have.status(400);
        expect(res).to.be.json();
        done();
      });
  });

  it('should only return a user email when validating email', (done) => {
    const email = 'test@test.com';
    chai
      .request(app)
      .get('/user/validateEmail')
      .query({ email })
      .end((err, res) => {
        expect(err).to.be.null();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.text).to.equal('{"email":"test@test.com"}');
        done();
      });
    modelGetUserByEmailStub.returns({ _id: 111, email, firstName: 'patrick' });
  });

  it('should return showTerms as true and correct migration properties for migrated users without a password', (done) => {
    const id = '12345';
    chai
      .request(app)
      .get('/user/show-terms')
      .query({ id })
      .end((err, res) => {
        expect(err).to.be.null();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal({
          showTerms: true,
          migratedBrand: 's2s',
          migratedBrandName: 'Surf 2 Surf',
        });
        done();
      });
    modelGetUserByIdStub.returns({
      _id: 12345,
      firstName: 'patrick',
      hasPassword: false,
      migratedUser: 's2s_1234',
    });
  });

  it('should return showTerms as false for non-migrated users without a password', (done) => {
    const id = '12345';
    chai
      .request(app)
      .get('/user/show-terms')
      .query({ id })
      .end((err, res) => {
        expect(err).to.be.null();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal({
          showTerms: false,
          migratedBrand: null,
          migratedBrandName: null,
        });
        done();
      });
    modelGetUserByIdStub.returns({
      _id: 12345,
      firstName: 'patrick',
      hasPassword: false,
    });
  });

  it('should return showTerms as false for migrated users with a password', (done) => {
    const id = '12345';
    chai
      .request(app)
      .get('/user/show-terms')
      .query({ id })
      .end((err, res) => {
        expect(err).to.be.null();
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal({
          showTerms: false,
          migratedBrand: 's2s',
          migratedBrandName: 'Surf 2 Surf',
        });
        done();
      });
    modelGetUserByIdStub.returns({
      _id: 12345,
      firstName: 'patrick',
      hasPassword: true,
      migratedUser: 's2s_1234',
    });
  });

  it('should allow patching user details for whitelisted fields', async () => {
    modelUpdateUserPromise.resolves({ _id: '111', timezone: 'America/Los_Angeles' });
    const response = await chai
      .request(app)
      .patch('/user')
      .set('x-auth-userId', '111')
      .send({ timezone: 'America/Los_Angeles' });

    expect(response).to.have.status(200);
    expect(response.body).to.deep.equal({ message: 'User updated successfully' });
    expect(modelUpdateUserPromise).to.have.been.calledOnceWithExactly('111', {
      timezone: 'America/Los_Angeles',
    });
  });

  it('should allow patching user details and remove non-whitelisted fields', async () => {
    modelUpdateUserPromise.resolves({ _id: '111' });
    const response = await chai.request(app).patch('/user').set('x-auth-userId', '111').send({
      nonwhitelistedfield: true,
    });

    expect(response).to.have.status(200);
    expect(response.body).to.deep.equal({ message: 'User updated successfully' });
    expect(modelUpdateUserPromise).to.have.been.calledOnceWithExactly('111', {});
  });

  describe('/verify-email', () => {
    it('should verify email for a registered user', async () => {
      const userId = '123';
      const email = 'email@email.com';
      const markModified = sinon.stub();
      const save = sinon.stub().resolves({ _id: userId });
      decryptStub.returns({ userId, email });
      modelUserFindOneStub.resolves({
        isEmailVerified: false,
        _id: userId,
        email,
        markModified,
        save,
      });

      const response = await chai
        .request(app)
        .post('/user/verify-email')
        .send({ hash: 123, brand: 'sl' });

      expect(response).to.have.status(200);
      expect(response.body).to.deep.equal({ message: 'Email verification was successful' });
      expect(decryptStub).to.have.been.calledOnceWithExactly(123);
      expect(modelUserFindOneStub).to.have.been.calledOnceWithExactly({ _id: userId, email });
      expect(markModified).to.have.been.calledOnceWithExactly('isEmailVerified');
      expect(save).to.have.been.calledOnce();
      expect(trackEmailVerifiedStub).to.have.been.calledOnceWithExactly(userId, 'sl', 'web');
    });

    it('should throw an error if the user is not found', async () => {
      const userId = '123';
      const email = 'email@email.com';
      decryptStub.returns({ userId, email });
      modelUserFindOneStub.resolves(null);

      const response = await chai
        .request(app)
        .post('/user/verify-email')
        .send({ hash: 123, brand: 'sl' });

      expect(response).to.have.status(400);
      expect(response.body).to.deep.equal({ message: 'Email verification failed.' });
      expect(decryptStub).to.have.been.calledOnceWithExactly(123);
      expect(modelUserFindOneStub).to.have.been.calledOnceWithExactly({ _id: userId, email });
      expect(trackEmailVerifiedStub).to.have.not.been.called();
    });

    it('should throw an error if something goes wrong', async () => {
      const userId = '123';
      const email = 'email@email.com';
      decryptStub.returns({ userId, email });
      modelUserFindOneStub.throws();

      const response = await chai
        .request(app)
        .post('/user/verify-email')
        .send({ hash: 123, brand: 'sl' });

      expect(response).to.have.status(400);
      expect(response.body).to.deep.equal({ message: 'Email verification failed.' });
      expect(decryptStub).to.have.been.calledOnceWithExactly(123);
      expect(modelUserFindOneStub).to.have.been.calledOnceWithExactly({ _id: userId, email });
      expect(trackEmailVerifiedStub).to.have.not.been.called();
    });
  });

  describe('/user DELETE', async () => {
    let createDeletedUserStub;
    let entitlementsStub;
    let consoleStub;

    beforeEach(() => {
      createDeletedUserStub = sinon.stub(DeletedUsers, 'createDeletedUser');
      entitlementsStub = sinon.stub(entitlements, 'isPremium');
      consoleStub = sinon.stub(console, 'log');
    });

    afterEach(() => {
      createDeletedUserStub.restore();
      entitlementsStub.restore();
      consoleStub.restore();
    });

    it('should return a 401 without an authenticatedUserId', async () => {
      const response = await chai.request(app).delete('/user').send();
      expect(response.status).to.equal(401);
      expect(createDeletedUserStub).to.not.have.been.called();
    });

    it('should return a 202 when after queuing the deleted user', async () => {
      createDeletedUserStub.returns(true);
      entitlementsStub.returns(false);
      const response = await chai
        .request(app)
        .delete('/user')
        .set('x-auth-userid', '56f9eb8646f566a11a5ce971')
        .send();
      expect(response.status).to.equal(202);
      expect(createDeletedUserStub).to.have.been.calledOnceWithExactly('56f9eb8646f566a11a5ce971');
      expect(response.body.message).to.equal('User successfully queued for deletion');
    });

    it('should not create a document if there is already a DeletedUser', async () => {
      createDeletedUserStub.returns(false);
      entitlementsStub.returns(false);
      const response = await chai
        .request(app)
        .delete('/user')
        .set('x-auth-userid', '56f9eb8646f566a11a5ce971')
        .send();
      expect(response.status).to.equal(409);
      expect(createDeletedUserStub).to.have.been.calledOnceWithExactly('56f9eb8646f566a11a5ce971');
      expect(response.body).to.deep.equal({
        message: 'User already queued for deletion',
        queued: true,
      });
    });

    it('should not create a document if the user has a premium entitlement', async () => {
      entitlementsStub.returns(true);
      const response = await chai
        .request(app)
        .delete('/user')
        .set('x-auth-userid', '56f9eb8646f566a11a5ce971')
        .send();
      expect(response.status).to.equal(200);
      expect(createDeletedUserStub).to.not.have.been.called();
      expect(response.body).to.deep.equal({
        message: 'User has an active subscription',
        queued: false,
      });
    });
  });
});
