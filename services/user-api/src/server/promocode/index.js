import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import { trackPromoCodeRequests, getPromoCodeHandler } from './promocode';

const promocode = () => {
  const api = Router();

  api.use('*', trackPromoCodeRequests);
  api.get('/', wrapErrors(getPromoCodeHandler));

  return api;
};

export default promocode;
