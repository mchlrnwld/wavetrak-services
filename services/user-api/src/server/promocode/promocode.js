import { getPlatform } from '@surfline/services-common';
import * as promoCodeModel from '../../model/promocode/PromoCode';
import { createLogger } from '../../common/logger';
import { trackPromoCodeGenerated } from '../../common/trackEvent';

const log = createLogger('user-service:server:promocode');

export const trackPromoCodeRequests = (req, res, next) => {
  log.trace({
    action: '/User/Promocode',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export const getPromoCodeHandler = async (req, res) => {
  const { type } = req.query;
  const userId = req.authenticatedUserId;

  if (!userId) {
    log.warn('User requested promo code without authentication.');
    return res.status(400).send({ message: 'You need to be authenticated.' });
  }
  let promocode = null;
  let promoCodesAvailable = 0;
  try {
    if (type === 'evo') {
      promocode = await promoCodeModel.getEvoPromoCode();
      promoCodesAvailable = await promoCodeModel.getEvoPromoCodesRemaining();
    }
  } catch (err) {
    log.debug({
      message: 'Promo code could not be retrieved: getPromoCodeHandler()',
      stack: err,
    });
    return res.status(500).send({ message: 'Error retrieving promo code.' });
  }
  try {
    if (type && promocode && userId) {
      const platform = getPlatform(req.headers['user-agent']);
      trackPromoCodeGenerated(userId, {
        code: promocode.code,
        type,
        promoCodesAvailable,
        platform,
      });
    } else {
      log.warn('Promo code requested without specifying promo type.');
    }
  } catch (err) {
    log.debug({
      message: 'Promo code could not be tracked: trackPromoCodeGenerated()',
      stack: err,
    });
  }
  return res.send({
    code: promocode && promocode.code,
  });
};
