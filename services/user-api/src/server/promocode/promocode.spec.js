import express from 'express';
import bodyParser from 'body-parser';
import chai, { expect } from 'chai';
import { authenticateRequest } from '@surfline/services-common';
import sinon from 'sinon';
import * as model from '../../model/promocode/PromoCode';
import promocode from './index';

describe('promocode route', () => {
  const app = express();

  before(() => {
    process.env.NEW_RELIC_ENABLED = false;
    process.env.NEW_RELIC_NO_CONFIG_FILE = true;
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(authenticateRequest());
    app.use('/', promocode());
  });

  beforeEach(() => {
    sinon.stub(model, 'getEvoPromoCode');
    sinon.stub(model, 'getEvoPromoCodesRemaining');
  });

  afterEach(() => {
    model.getEvoPromoCode.restore();
    model.getEvoPromoCodesRemaining.restore();
  });

  describe('GET /user/promocode', () => {
    it('should throw 400 with non-authenticated message', async () => {
      try {
        const res = await chai.request(app).get('/');
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response.body.message).to.equal('You need to be authenticated.');
      }
      expect(model.getEvoPromoCode).not.to.have.been.called();
    });

    it('should have status of 200 when authenticated', async () => {
      const res = await chai.request(app).get('/').set('x-auth-userid', 'authenticatedUserId');
      expect(res).to.have.status(200);
      expect(model.getEvoPromoCode).not.to.have.been.called();
    });

    it('should return null when no query param "type"', async () => {
      const res = await chai.request(app).get('/').set('x-auth-userid', 'authenticatedUserId');
      expect(res).to.have.status(200);
      expect(res.body).to.deep.equal({ code: null });
      expect(model.getEvoPromoCode).not.to.have.been.called();
    });

    it('should return evo code when query param "type=evo" and authenticated', async () => {
      model.getEvoPromoCode.resolves({ code: 'EEFG78', used: false });
      model.getEvoPromoCodesRemaining.resolves(4555);
      const res = await chai
        .request(app)
        .get('/?type=evo')
        .set('x-auth-userid', 'authenticatedUserId');
      expect(res).to.have.status(200);
      expect(model.getEvoPromoCode).to.have.been.called();
      expect(res.body).to.deep.equal({ code: 'EEFG78' });
    });
  });
});
