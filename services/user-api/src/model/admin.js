import UserModel from './UserModel';
import { createLogger } from '../common/logger';

const log = createLogger('user-model');

export const createUser = async (props) => {
  try {
    const userProps = { ...props, usID: undefined };
    const user = new UserModel(userProps);
    await user.save();
    return user;
  } catch (error) {
    log.error({
      message: 'Error returned from admin:createUser',
      stack: error.stack,
    });
    throw new Error(error.message);
  }
};
