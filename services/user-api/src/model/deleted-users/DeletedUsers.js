import DeletedUsersModel from './DeletedUsersModel';

/**
 * Creates a DeletedUser record and sets the deleteRequestedAt property
 * @param {*} user The user to delete
 * @returns
 */
export const createDeletedUser = async (user) => {
  const res = await DeletedUsersModel.findOne({ user });
  if (res) return false;
  return DeletedUsersModel.create({
    deleteRequestedAt: new Date(),
    user,
  });
};

/**
 * Marks the DeletedUser as deleted by using the deletedAt property
 * @param {*} user The user to delete
 */
export const markUserAsDeleted = async (user) =>
  DeletedUsersModel.findOneAndUpdate(
    { user },
    {
      deletedAt: new Date(),
      user,
    },
    {
      new: true,
      upsert: true,
    },
  )?.lean();

/**
 * Returns the list of users who have requested to be deleted before the given date
 * @param {Date} date - Cutoff date to for retrieval
 * @returns
 */
export const getPendingDeletedUsers = async (date) =>
  DeletedUsersModel.find({
    deleteRequestedAt: { $lt: date },
    deletedAt: null,
  })?.lean();
