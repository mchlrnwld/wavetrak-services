import { expect } from 'chai';
import mongoose from 'mongoose';
import sinon from 'sinon';
import DeletedUsersModel from './DeletedUsersModel';
import { markUserAsDeleted, createDeletedUser, getPendingDeletedUsers } from './DeletedUsers';

describe('DeletedUsers', () => {
  const userId = mongoose.Schema.Types.ObjectId('41224d776a326fb40f000001');

  let findOneStub;
  let findOneAndUpdateStub;
  let findStub;
  let createStub;
  let clock;

  beforeEach(() => {
    findOneStub = sinon.stub(DeletedUsersModel, 'findOne');
    findOneAndUpdateStub = sinon.stub(DeletedUsersModel, 'findOneAndUpdate');
    findStub = sinon.stub(DeletedUsersModel, 'find');
    createStub = sinon.stub(DeletedUsersModel, 'create');
    clock = sinon.useFakeTimers(new Date() * 1000);
  });

  afterEach(() => {
    findOneAndUpdateStub.restore();
    findStub.restore();
    findOneStub.restore();
    createStub.restore();
    clock.restore();
  });

  describe('markUserAsDeleted', () => {
    it('should call DeletedUsersModel.findOneAndUpdate with upsert', async () => {
      await markUserAsDeleted(userId);
      expect(findOneAndUpdateStub).to.have.been.calledOnce();
      expect(findOneAndUpdateStub).to.have.been.calledWith(
        { user: userId },
        {
          deletedAt: new Date(),
          user: userId,
        },
        {
          new: true,
          upsert: true,
        },
      );
    });
  });

  describe('createDeletedUser', () => {
    it('should call DeletedUsersModel.create', async () => {
      await createDeletedUser(userId);
      expect(createStub).to.have.been.calledOnce();
      expect(createStub).to.have.been.calledWith({
        deleteRequestedAt: new Date(),
        user: userId,
      });
    });
    it('should not call DeletedUsersModel.create if a DeletedUser record is found', async () => {
      findOneStub.returns({
        _id: '56f9eb8646f566a11a5ce971',
        deleteRequestedAt: '2021-01-01T00:00:00.000Z',
        user: userId,
      });
      const res = await createDeletedUser(userId);
      expect(findOneStub).to.have.been.called();
      expect(createStub).to.not.have.been.called();
      expect(res).to.equal(false);
    });
  });

  describe('getPendingDeletedUsers', () => {
    it('should call DeletedUsersModel.find with the date', async () => {
      await getPendingDeletedUsers(new Date());
      expect(findStub).to.have.been.calledOnce();
      expect(findStub).to.have.been.calledWith({
        deleteRequestedAt: { $lt: new Date() },
        deletedAt: null,
      });
    });
  });
});
