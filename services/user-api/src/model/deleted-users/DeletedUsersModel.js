import mongoose from 'mongoose';

/**
 * DeletedUsers - Collection for recording the request and status of users who request to be
 * deleted.
 */

const DeletedUsersSchema = mongoose.Schema(
  {
    deletedAt: {
      type: Date,
    },
    deleteRequestedAt: {
      type: Date,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'UserInfo',
      required: true,
      unique: true,
    },
  },
  {
    collection: 'DeletedUsers',
    timestamps: true,
  },
);

DeletedUsersSchema.index({ user: 1 }, { unique: true });

const DeletedUsersModel = mongoose.model('DeletedUsers', DeletedUsersSchema);

export default DeletedUsersModel;
