import mongoose from 'mongoose';

const EvoPromoCodeSchema = mongoose.Schema({
  code: {
    type: String,
  },
  used: {
    type: Boolean,
    default: false,
  },
});

EvoPromoCodeSchema.index({ code: 1 }, { unique: true });

const EvoPromoCodeModel = mongoose.model('EvoPromoCodes', EvoPromoCodeSchema, 'EvoPromoCodes');

export default EvoPromoCodeModel;
