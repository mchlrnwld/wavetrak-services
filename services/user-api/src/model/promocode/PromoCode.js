import { createLogger } from '../../common/logger';
import EvoPromoCodeModel from './EvoPromoCodeModel';

const log = createLogger('promocode-model');

export const getEvoPromoCode = () =>
  new Promise((resolve, reject) => {
    EvoPromoCodeModel.findOneAndUpdate({ used: false }, { used: true }, (err, res) => {
      if (err) {
        log.debug({
          message: 'getEvoPromoCode()',
          stack: err,
        });
        reject(err);
      } else {
        resolve(res);
      }
    });
  });

export const getEvoPromoCodesRemaining = () => EvoPromoCodeModel.find({ used: false }).count();
