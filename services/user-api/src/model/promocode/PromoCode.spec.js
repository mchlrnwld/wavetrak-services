import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import EvoPromoCodeModel from './EvoPromoCodeModel';
import { getEvoPromoCode } from './PromoCode';

const { assert } = chai;
chai.should();
chai.use(sinonChai);

describe('promo code model', () => {
  beforeEach(() => {
    sinon.spy(EvoPromoCodeModel, 'findOneAndUpdate');
  });

  afterEach(() => {
    EvoPromoCodeModel.findOneAndUpdate.restore();
  });

  it('should return a code with getEvoPromoCode()', () => {
    getEvoPromoCode();
    assert(EvoPromoCodeModel.findOneAndUpdate.calledOnce);
    assert(EvoPromoCodeModel.findOneAndUpdate.calledWith({ used: false }, { used: true }));
  });
});
