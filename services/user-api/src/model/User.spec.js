import { expect } from 'chai';
import sinon from 'sinon';
import UserModel from './UserModel';
import {
  getUserById,
  getUserByEmail,
  getUserByNameEmailSearch,
  getUserByLinkedAccount,
  updateUser,
  deactivateUser,
  createUser,
} from './User';

describe('user model', () => {
  const objectId = '41224d776a326fb40f000001';

  beforeEach(() => {
    // https://www.exratione.com/2015/12/es6-use-of-import-property-from-module-is-not-a-great-plan/
    sinon.spy(UserModel, 'find');
    sinon.spy(UserModel, 'findOne');
    sinon.spy(UserModel, 'findOneAndUpdate');
    sinon.spy(UserModel, 'aggregate');
  });

  afterEach(() => {
    UserModel.find.restore();
    UserModel.findOne.restore();
    UserModel.findOneAndUpdate.restore();
    UserModel.aggregate.restore();
  });

  it('should return a user with getUserById(id)', () => {
    getUserById(objectId);
    expect(UserModel.findOne).to.have.been.calledOnce();
    UserModel.find.calledWith({ _id: objectId });
  });

  it('should return a user with getUserByEmail(email)', () => {
    getUserByEmail('email@email.com');
    expect(UserModel.findOne).to.have.been.calledOnce();
    expect(UserModel.findOne).to.have.been.calledWith({ email: 'email@email.com' });
  });

  it('should run aggregation pipeline with correct arguments for "firstname lastname" type query with getUserByNameEmailSearch(query)', () => {
    getUserByNameEmailSearch('Gavin Cooper');
    expect(UserModel.aggregate).to.have.been.calledOnce();
    expect(UserModel.aggregate).to.have.been.calledWith([
      {
        $match: {
          $or: [
            {
              firstName: { $regex: /^gavin$/i },
              lastName: { $regex: /^cooper$/i },
            },
            {
              firstName: { $regex: /^cooper$/i },
              lastName: { $regex: /^gavin$/i },
            },
            {
              firstName: { $regex: /gavin/i },
              lastName: { $regex: /cooper/i },
            },
            {
              firstName: { $regex: /cooper/i },
              lastName: { $regex: /gavin/i },
            },
          ],
        },
      },
      {
        $project: {
          _id: 1,
          email: 1,
          firstName: 1,
          lastName: 1,
          score: {
            $cond: {
              if: {
                $and: [
                  {
                    $regexMatch: {
                      input: '$firstName',
                      regex: /^gavin$/i,
                    },
                  },

                  {
                    $regexMatch: {
                      input: '$lastName',
                      regex: /^cooper$/i,
                    },
                  },
                ],
              },
              then: 2,
              else: 1,
            },
          },
        },
      },
      {
        $limit: 200,
      },
      {
        $sort: { score: -1 },
      },
    ]);
  });

  it('should run find with correct arguments for query including only an email with getUserByNameEmailSearch(query)', () => {
    getUserByNameEmailSearch('gavinsworld@gmail.com');
    expect(UserModel.find).to.have.been.calledOnce();
    expect(UserModel.find).to.have.been.calledWith({
      $or: [{ email: 'gavinsworld@gmail.com' }],
    });
  });

  it('should run aggregation pipeline with correct arguments for query consisting of one non-email search term', async () => {
    getUserByNameEmailSearch('cooper');
    expect(UserModel.aggregate).to.have.been.calledOnce();
    expect(UserModel.aggregate).to.have.been.calledWith([
      {
        $search: {
          index: 'firstLastEmail',
          compound: {
            should: [
              {
                compound: {
                  must: [
                    {
                      text: {
                        path: 'email',
                        query: ['cooper'],
                        fuzzy: {
                          maxEdits: 1,
                          prefixLength: 4,
                        },
                      },
                    },
                  ],
                },
              },
              {
                compound: {
                  must: [
                    {
                      text: {
                        path: ['firstName', 'lastName'],
                        query: ['cooper'],
                        fuzzy: {
                          maxEdits: 1,
                          prefixLength: 3,
                        },
                      },
                    },
                  ],
                },
              },
            ],
          },
        },
      },
      {
        $project: {
          _id: 1,
          email: 1,
          firstName: 1,
          lastName: 1,
          score: { $meta: 'searchScore' },
        },
      },
      {
        $limit: 200,
      },
    ]);
  });

  it('should run aggregation pipeline with correct arguments for queries for users with middle names with getUserByNameEmailSearch(query)', async () => {
    getUserByNameEmailSearch('John John Florence');
    expect(UserModel.find).to.have.been.calledOnce();
    expect(UserModel.find).to.have.been.calledWith(
      {
        $or: [
          {
            firstName: /john john/i,
            lastName: /florence/i,
          },
          {
            firstName: /florence/i,
            lastName: /john john/i,
          },
          {
            firstName: /john/i,
            lastName: /john florence/i,
          },
          {
            firstName: /john florence/i,
            lastName: /john/i,
          },
        ],
      },
      {
        _id: 1,
        email: 1,
        firstName: 1,
        lastName: 1,
      },
    );
  });

  it('should return a user with getUserByLinkedAccount(linkedAccountId)', () => {
    getUserByLinkedAccount('111111111111111');
    expect(UserModel.findOne).to.have.been.calledOnce();
    expect(UserModel.findOne).to.have.been.calledWith({
      'linkedAccount.facebook.id': '111111111111111',
    });
  });

  it('should update a user with updateUser', () => {
    const spy = sinon.spy();
    updateUser(objectId, { firstName: 'first' }, spy);
    expect(UserModel.findOneAndUpdate).to.have.been.calledOnce();
  });

  it('should deactivate a user with deactivateUser', () => {
    const spy = sinon.spy();
    deactivateUser(objectId, { firstName: 'first' }, spy);
    expect(UserModel.findOneAndUpdate).to.have.been.calledOnce();
  });

  it('should create a user with createUser', () => {
    const createdUser = createUser({
      firstName: 'first',
      lastName: 'last',
      email: 'email@email.com',
    });
    expect(createdUser).to.exist();
  });
});
