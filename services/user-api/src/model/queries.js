import UserModel from './UserModel';

export const aggregationPipelineEmail = (searchTerms) => {
  return UserModel.aggregate([
    {
      $search: {
        index: 'firstLastEmail',
        compound: {
          must: [
            {
              text: {
                path: 'email',
                query: searchTerms,
                fuzzy: {
                  maxEdits: 1,
                  prefixLength: 4,
                },
              },
            },
          ],
        },
      },
    },
    {
      $project: {
        _id: 1,
        email: 1,
        firstName: 1,
        lastName: 1,
        score: { $meta: 'searchScore' },
      },
    },
    {
      $limit: 200,
    },
  ]);
};

export const aggregationPipelineName = (searchTerms) => {
  const firstName = searchTerms[0];
  const lastName = searchTerms[1];

  const firstNameRegexExact = new RegExp(`^${firstName}$`, 'i');
  const lastNameRegexExact = new RegExp(`^${lastName}$`, 'i');

  const firstNameRegexFuzzy = new RegExp(`${firstName}`, 'i');
  const lastNameRegexFuzzy = new RegExp(`${lastName}`, 'i');
  return UserModel.aggregate([
    {
      $match: {
        $or: [
          {
            firstName: { $regex: firstNameRegexExact },
            lastName: { $regex: lastNameRegexExact },
          },
          {
            firstName: { $regex: lastNameRegexExact },
            lastName: { $regex: firstNameRegexExact },
          },
          {
            firstName: { $regex: firstNameRegexFuzzy },
            lastName: { $regex: lastNameRegexFuzzy },
          },
          {
            firstName: { $regex: lastNameRegexFuzzy },
            lastName: { $regex: firstNameRegexFuzzy },
          },
        ],
      },
    },
    {
      $project: {
        _id: 1,
        email: 1,
        firstName: 1,
        lastName: 1,
        score: {
          // prioritize the exact matches
          $cond: {
            if: {
              $and: [
                {
                  $regexMatch: {
                    input: '$firstName',
                    regex: firstNameRegexExact,
                  },
                },

                {
                  $regexMatch: {
                    input: '$lastName',
                    regex: lastNameRegexExact,
                  },
                },
              ],
            },
            then: 2,
            else: 1,
          },
        },
      },
    },
    {
      $limit: 200,
    },
    {
      $sort: { score: -1 },
    },
  ]);
};

export const aggregationPipelineFirstLastEmail = (searchTerms, maxResults) => {
  return UserModel.aggregate([
    {
      $search: {
        index: 'firstLastEmail',
        compound: {
          should: [
            {
              compound: {
                must: [
                  {
                    text: {
                      path: 'email',
                      query: searchTerms,
                      fuzzy: {
                        maxEdits: 1,
                        prefixLength: 4,
                      },
                    },
                  },
                ],
              },
            },
            {
              compound: {
                must: [
                  {
                    text: {
                      path: ['firstName', 'lastName'],
                      query: searchTerms,
                      fuzzy: {
                        maxEdits: 1,
                        prefixLength: 3,
                      },
                    },
                  },
                ],
              },
            },
          ],
        },
      },
    },
    {
      $project: {
        _id: 1,
        email: 1,
        firstName: 1,
        lastName: 1,
        score: { $meta: 'searchScore' },
      },
    },
    {
      $limit: maxResults,
    },
  ]);
};

export const findUserWithMiddleName = (searchTerms) => {
  // handle searches for names with middle names
  // 2x2 cases:
  //   - firstName or lastName field can contain middle name
  //   - query can be in first-last or last-first order
  const firstMiddle = searchTerms.slice(0, 2).join(' ');
  const last = searchTerms.slice(2, 3).join(' ');

  const first = searchTerms.slice(0, 1).join(' ');
  const lastMiddle = searchTerms.slice(1, 3).join(' ');

  return UserModel.find(
    {
      $or: [
        {
          firstName: new RegExp(firstMiddle, 'i'),
          lastName: new RegExp(last, 'i'),
        },
        {
          firstName: new RegExp(last, 'i'),
          lastName: new RegExp(firstMiddle, 'i'),
        },
        {
          firstName: new RegExp(first, 'i'),
          lastName: new RegExp(lastMiddle, 'i'),
        },
        {
          firstName: new RegExp(lastMiddle, 'i'),
          lastName: new RegExp(first, 'i'),
        },
      ],
    },
    {
      _id: 1,
      email: 1,
      firstName: 1,
      lastName: 1,
    },
  );
};
