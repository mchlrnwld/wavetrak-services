/* eslint-disable no-underscore-dangle */
import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import { getUserSettings, updateUserSettings } from './UserSettings';
import UserSettingsModel from './UserSettingsModel';

const { expect } = chai;
const { assert } = chai;
chai.should();
chai.use(sinonChai);

describe('user settings model', () => {
  const objectId = '41224d776a326fb40f000001';

  beforeEach(() => {
    sinon.spy(UserSettingsModel, 'findOneAndUpdate');
  });

  afterEach(() => {
    UserSettingsModel.findOneAndUpdate.restore();
  });

  it('should return a user with updateUserSettings(id)', () => {
    updateUserSettings(objectId);
    assert(UserSettingsModel.findOneAndUpdate.calledOnce);
    assert(UserSettingsModel.findOneAndUpdate.calledWith({ user: objectId }));
  });
  it('should return return US units', async () => {
    const usSettings = await getUserSettings(undefined, 'US');
    expect(usSettings.units._doc.units.temperature).to.equal('F');
    expect(usSettings.units._doc.units.windSpeed).to.equal('KTS');
  });
  it('should return return GB units', async () => {
    const gbSettings = await getUserSettings(undefined, 'GB');
    expect(gbSettings.units._doc.units.temperature).to.equal('C');
    expect(gbSettings.units._doc.units.windSpeed).to.equal('MPH');
  });
  it('should return return Default units', async () => {
    const defaultSettings = await getUserSettings();
    expect(defaultSettings.units._doc.units.temperature).to.equal('C');
    expect(defaultSettings.units._doc.units.windSpeed).to.equal('KPH');
  });
});
