import mongoose from 'mongoose';

const sessionClientIds = process.env.SESSION_CLIENT_IDS || '';

const ClientIdSchema = mongoose.Schema(
  {
    name: String,
    appImageURL: String,
  },
  { collection: 'ClientIds' },
);
mongoose.model('ClientId', ClientIdSchema);

const linkedClientsSchema = mongoose.Schema(
  {
    clientId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'ClientId',
    },
    scopes: [String],
    createdAt: Date,
    token: String,
    tokenSecret: String,
  },
  { _id: false },
);

const UserSchema = mongoose.Schema(
  {
    firstName: {
      type: String,
      trim: true,
    },
    lastName: {
      type: String,
      trim: true,
    },
    usID: {
      type: Number,
      unique: true,
    },
    email: {
      type: String,
      required: true,
      trim: true,
      validate: {
        validator: (v) => /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/.test(v.toUpperCase()),
        message: 'Email address needs to be in the form name@domain.com',
      },
    },
    isEmailVerified: { type: Boolean, default: false },
    isActive: { type: Boolean, default: true },
    modifiedDate: {
      type: Date,
      default: Date.now,
    },
    password: {
      type: String,
      trim: true,
      // required: true, // Disabled for CW migration
      validate: {
        validator: (v) => v && v.length > 1,
        message: 'Password needs to be the output of bcrypt.',
      },
      // select: false, // Disabled for CW migration
    },
    brand: {
      type: String,
      default: 'sl',
      lowercase: true,
      trim: true,
      validate: {
        validator: (v) => v && (v === 'sl' || v === 'bw' || v === 'fs'),
        message: 'Valid Brand values: sl || bw || fs .',
      },
    },
    gender: { type: String, lowercase: true, trim: true },
    locale: { type: String, default: 'en-US' },
    linkedAccount: {
      facebook: {
        id: { type: String },
        ageRange: {
          min: { type: String },
        },
      },
    },
    linkedClients: [linkedClientsSchema],
    stripeCustomerId: {
      type: String,
    },
    timezone: String,
    cwUserId: {
      type: Number,
      unique: true,
    },
    migratedUser: {
      type: String,
      trim: true,
    },
  },
  {
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
    collection: 'UserInfo',
    timestamps: true,
  },
);

UserSchema.virtual('hasSessionCompatibleDevice').get(function setSessionCompatibleDevice() {
  if (!this.linkedClients || !this.linkedClients.length) return false;
  return this.linkedClients.some(({ clientId }) => {
    const { _id } = clientId; // clientId will be an object if populate is called.
    const client = _id || clientId;
    return sessionClientIds.includes(client.toString());
  });
});

UserSchema.virtual('hasPassword').get(function setHasPassword() {
  if (this.password) return true;
  return false;
});

UserSchema.index({ email: 1 }, { unique: true });
UserSchema.index({ usID: 1 }, { sparse: true });
UserSchema.index(
  {
    'linkedClients.token': 1,
  },
  { sparse: true, name: 'Garmin Token Index' },
);
UserSchema.index({
  email: 'text',
  firstName: 'text',
  lastName: 'text',
});
UserSchema.index(
  {
    'linkedAccount.facebook.id': 1,
  },
  { sparse: true },
);
UserSchema.index(
  {
    timezone: 1,
  },
  { sparse: true, name: 'Metering Reset Index' },
);

const UserModel = mongoose.model('User', UserSchema);

export default UserModel;
