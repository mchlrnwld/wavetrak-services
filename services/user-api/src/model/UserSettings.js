import { createLogger } from '../common/logger';
import UserSettingsModel from './UserSettingsModel';

const log = createLogger('user-settings-model');

const updateMongooseOptions = {
  upsert: true,
  new: true,
  runValidators: true,
  setDefaultsOnInsert: true,
};

const userSettingsDefaults = {
  AUSTRALIA: {
    units: {
      surfHeight: 'HI',
      swellHeight: 'FT',
      tideHeight: 'M',
      windSpeed: 'KTS',
      temperature: 'C',
    },
    date: {
      format: 'DMY',
    },
    feed: {
      localized: 'AU',
    },
  },
  BRITAIN_IRELAND: {
    units: {
      surfHeight: 'FT',
      swellHeight: 'FT',
      tideHeight: 'M',
      windSpeed: 'MPH',
      temperature: 'C',
    },
    date: {
      format: 'DMY',
    },
    feed: {
      localized: 'LOCALIZED',
    },
  },
  NEW_ZEALAND: {
    units: {
      surfHeight: 'HI',
      swellHeight: 'FT',
      tideHeight: 'M',
      windSpeed: 'KTS',
      temperature: 'C',
    },
    date: {
      format: 'DMY',
    },
    feed: {
      localized: 'NZ',
    },
  },
  US_LIBERIA_MYANMAR: {
    units: {
      surfHeight: 'FT',
      swellHeight: 'FT',
      tideHeight: 'FT',
      windSpeed: 'KTS',
      temperature: 'F',
    },
    date: {
      format: 'MDY',
    },
    feed: {
      localized: 'US',
    },
  },
};

const getCountryDefaultSettings = (geoCountryIso) => {
  switch (geoCountryIso) {
    // Australia
    case 'AU':
      return new UserSettingsModel(userSettingsDefaults.AUSTRALIA);
    // Great Britain, Ireland
    case 'GB':
    case 'IE':
      return new UserSettingsModel(userSettingsDefaults.BRITAIN_IRELAND);

    // US, Liberia, Myanmar
    case 'US':
    case 'LR':
    case 'MM':
      return new UserSettingsModel(userSettingsDefaults.US_LIBERIA_MYANMAR);

    case 'NZ':
      return new UserSettingsModel(userSettingsDefaults.NEW_ZEALAND);

    default:
      return new UserSettingsModel();
  }
};

/**
 * Gets user settings.
 *
 * Gets user settings if they exist, otherwise defaults
 *
 * @param userInfoId      (optional) Mongo-id for UserInfo
 * @param geoCountryIso   (optional) 2-digit country ISO code
 * @returns {userSettings}
 */
export const getUserSettings = async (userInfoId, geoCountryIso) => {
  if (!userInfoId) return getCountryDefaultSettings(geoCountryIso);
  const userSettings = await UserSettingsModel.findOne({ user: userInfoId });
  if (!userSettings) return getCountryDefaultSettings(geoCountryIso);
  return userSettings;
};

/**
 * Updates user settings.
 *
 * Creates a new user settings object if it doesn't exist
 *
 * @param userInfoId
 * @param changedFields
 * @returns {Promise}
 */
export const updateUserSettings = async (userInfoId, changedFields = {}) => {
  const settings = await UserSettingsModel.findOneAndUpdate(
    { user: userInfoId },
    { $set: Object.assign(changedFields, { user: userInfoId }) },
    {
      upsert: true,
      new: true,
      runValidators: true,
      setDefaultsOnInsert: true,
    },
  );
  return settings;
};

export const updateUserSettingsOld = (userInfoId, changedFields = {}) => {
  const userParams = { user: userInfoId };
  return new Promise((resolve, reject) =>
    UserSettingsModel.findOneAndUpdate(
      userParams,
      { $set: Object.assign(changedFields, userParams) },
      updateMongooseOptions,
      (err, userSettings) => {
        if (err) {
          log.error({
            message: `UserSettings.updateUserSettings(${userInfoId}):findOneAndUpdate`,
            stack: err,
          });
          reject(err);
        } else {
          resolve(userSettings);
        }
      },
    ),
  );
};

/**
 * Creates user settings.
 *
 * Creates user settings for a user with defaults for country ISO
 * unless user settings already exists.
 *
 * @param userInfoId      Mongo-id for UserInfo
 * @param geoCountryIso   (optional) 2-digit country ISO code
 * @param {object} settings A settings object that can contain custom settings
 * @returns {Promise}
 */
export const createUserSettings = (userInfoId, geoCountryIso, settings) => {
  const userSettings = {
    ...getCountryDefaultSettings(geoCountryIso).toJSON(),
    ...settings,
  };
  return updateUserSettings(userInfoId, userSettings);
};

/**
 * Deletes the user's UserSettings document
 * @param {string} userId
 * @param {object} opts
 * @returns
 */
export const deleteUserSettings = async (userId, opts = null) =>
  UserSettingsModel.deleteOne({ _id: userId }, opts);
