import mongoose from 'mongoose';

const abilityLevels = ['BEGINNER', 'INTERMEDIATE', 'ADVANCED', 'PRO'];

const boardTypes = [
  'SHORTBOARD',
  'FISH',
  'FUNBOARD',
  'LONGBOARD',
  'GUN',
  'TOW',
  'SUP',
  'FOIL',
  'SKIMMING',
  'BODYBOARD',
  'BODYSURFING',
  'KITEBOARD',
];

const latitude = Number;
const longitude = Number;

const UserSettingsSchema = mongoose.Schema(
  {
    units: {
      surfHeight: {
        type: String,
        enum: ['FT', 'M', 'HI'],
        default: 'M',
      },
      swellHeight: {
        type: String,
        enum: ['FT', 'M'],
        default: 'M',
      },
      tideHeight: {
        type: String,
        enum: ['FT', 'M'],
        default: 'M',
      },
      windSpeed: {
        type: String,
        enum: ['MPH', 'KTS', 'KPH'],
        default: 'KPH',
      },
      temperature: {
        type: String,
        enum: ['F', 'C'],
        default: 'C',
      },
    },
    feed: {
      localized: {
        type: String,
        enum: ['US', 'AU', 'NZ', 'LOCALIZED'],
        default: 'LOCALIZED',
      },
    },
    date: {
      format: {
        type: String,
        enum: ['DMY', 'MDY'],
        default: 'MDY',
      },
    },
    onboarded: [
      {
        type: String,
        enum: ['FAVORITE_SPOTS', 'NEW_SURFLINE_INTRODUCTION', 'NEW_SURFLINE_ONBOARDING_INTRO'],
      },
    ],
    abilityLevel: {
      type: String,
      enum: abilityLevels,
    },
    boardTypes: [
      {
        type: String,
        enum: boardTypes,
      },
    ],
    travelDistance: {
      type: Number,
      default: null,
    },
    bwWindWaveModel: {
      type: String,
      enum: ['LOLA', 'LOTUS', 'NCEP'],
      default: 'LOLA',
    },
    bwUnitPreference: {
      type: String,
      enum: ['m', 'e', '', null],
      default: null,
    },
    navigation: {
      spotsTaxonomyLocation: {
        type: mongoose.Schema.Types.ObjectId,
        default: null,
      },
      forecastsTaxonomyLocation: {
        type: mongoose.Schema.Types.ObjectId,
        default: null,
      },
    },
    /*
    recentlyVisited shouldn't store more
    than the id and name of the spot or subregion
    */
    recentlyVisited: {
      spots: [
        {
          name: { type: String, required: true },
        },
      ],
      subregions: [
        {
          name: { type: String, required: true },
        },
      ],
    },
    preferredWaveChart: {
      type: String,
      enum: ['SURF', 'SWELL'],
      default: 'SURF',
    },
    homecam: {
      type: String,
    },
    homeSpot: {
      type: mongoose.Schema.Types.ObjectId,
      default: null,
    },
    homeForecast: {
      type: mongoose.Schema.Types.ObjectId,
      default: null,
    },
    feedFilters: {
      subregions: {
        type: String,
        default: '',
      },
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
    isSwellChartCollapsed: {
      type: mongoose.Schema.Types.Boolean,
      default: true,
    },
    receivePromotions: {
      type: mongoose.Schema.Types.Boolean,
      default: false,
    },
    location: {
      type: {
        type: mongoose.Schema.Types.Point,
      },
      coordinates: [longitude, latitude],
    },
  },
  {
    collection: 'UserSettings',
    timestamps: true,
  },
);

UserSettingsSchema.index({
  location: '2dsphere',
});
UserSettingsSchema.index({
  user: 1,
});

const UserSettingsModel = mongoose.model('UserSettings', UserSettingsSchema);

export default UserSettingsModel;
