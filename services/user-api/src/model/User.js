import bcrypt from 'bcrypt';
import { createLogger } from '../common/logger';
import UserModel from './UserModel';
import * as authService from '../external/auth';
import {
  trackAccountCreated,
  trackAccountUpdated,
  trackEmailVerified,
  trackTriggerEmailVerification,
  trackSetNewPassword,
} from '../common/trackEvent';
import {
  aggregationPipelineEmail,
  aggregationPipelineName,
  aggregationPipelineFirstLastEmail,
  findUserWithMiddleName,
} from './queries';

const log = createLogger('user-model');

/**
 *
 * @param password
 */
export const hashPassword = (password) =>
  new Promise((resolve, reject) =>
    bcrypt.genSalt(10, (err, salt) => {
      if (!salt) {
        reject();
      } else {
        bcrypt.hash(password, salt, (error, hash) => {
          if (!hash) {
            reject();
          } else {
            resolve(hash);
          }
        });
      }
    }),
  );

export const matchPassword = (password, passwordHash) =>
  new Promise((resolve, reject) => {
    bcrypt.compare(password, passwordHash, (err, res) => {
      if (err || !res) {
        log.debug({
          message: 'Invalid password match',
          stack: err,
        });
        reject(new Error({ message: 'Invalid password' }));
      } else {
        resolve(res);
      }
    });
  });

export const getUserPassword = (id) =>
  new Promise((resolve, reject) => {
    UserModel.findOne({ _id: id }, { password: 1, email: 1 }, (err, res) => {
      if (err) {
        log.debug({
          message: `getUserPassword(${id})`,
          stack: err,
        });
        reject(err);
      } else {
        resolve(res);
      }
    });
  });

/**
 * @param searchParameters
 * @returns {Promise}
 */
export const getUserByAggregate = (aggregatePipeline) =>
  UserModel.aggregate(aggregatePipeline, (err, users) => {
    if (err) {
      log.error({
        action: 'getUserByAggregate():aggregate',
        stack: err,
      });
      return null;
    }

    return users;
  });

/**
 * @param aggregatePipeline list of options for Mongo aggregate search
 * @returns {Promise}
 */
export const getUserByTextSearch = (terms) => {
  const query = terms.split(' ');
  return UserModel.aggregate([
    {
      $search: {
        index: 'firstLastEmail',
        compound: {
          must: [
            {
              text: {
                path: ['email', 'firstName', 'lastName'],
                query,
                fuzzy: {
                  maxEdits: 1,
                  prefixLength: 4,
                },
              },
            },
          ],
        },
      },
    },
    {
      $project: {
        _id: 1,
        email: 1,
        firstName: 1,
        lastName: 1,
        score: { $meta: 'searchScore' },
      },
    },
    {
      $limit: 200,
    },
  ]);
};

/**
 * This search is optimized primarily for email searches, secondarily for name searches
 *
 * @param query text query search string to try and find users with matching name/email addresses.
 * @returns {Promise}
 */

export const getUserByNameEmailSearch = async (query) => {
  let queryString = query.toLowerCase();
  let searchResults = [];
  /*
    Determine if query contains an email.
    Extract email(s).
    Reg: https://rubular.com/r/aSW7uFKGtV8Nrf
    Regex doesn't have to be perfect we're optimizing a search query here
    not validating logins for the NSA even then we'd not use a regex.
  */
  const emailRegex = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi;
  const email = queryString.match(emailRegex);

  // Do we have emails in this query?
  if (email && email.length) {
    /*
      We've got 1 or more emails in the query.
      Strip the domain from the email and add the username to the query string. We do this because many people use the same
      username across email domains. Keep the full email as well, because sometimes the domain is unique but the
      username is very common, e.g. john@some.wierd.domain.edu in which case stripping the domain throws away useful info
      Reg: https://rubular.com/r/UIH5KYMvrh6Odl
    */
    const emailUsernames = email.map((e) => e.replace(/(@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi, ''));
    queryString += ` ${emailUsernames.join(' ')}`;

    // Strip the 3 most popular domain names (google, hotmail, yahoo), as they don't have much valuable information but tend to slow down queries
    // https://stackoverflow.com/questions/16200965/regular-expression-validate-gmail-addresses
    const popularEmailRegex = /[a-z0-9]((\.|\+|_|-)?[a-z0-9]){5,}@(g(oogle)?mail|hotmail|yahoo)\.com/gi;
    queryString = queryString.replace(popularEmailRegex, '').trim();
  }

  const searchTerms = queryString.split(' ');

  if (email) {
    const mongoQueryOr = email.map((e) => {
      return { email: e };
    });
    try {
      // Query for exact matches of the raw emails in the query (containing domains)
      const directEmailResults = await UserModel.find({ $or: mongoQueryOr });
      searchResults = searchResults.concat(directEmailResults);
    } catch (error) {
      throw new Error(error);
    }
    try {
      // Use Mongo Atlas search to get further email search results
      const allEmailResults = await aggregationPipelineEmail(searchTerms);
      searchResults = searchResults.concat(allEmailResults);
    } catch (error) {
      throw new Error(error);
    }
  }

  if (searchTerms.length === 2 && !email) {
    try {
      // if we have 2 search terms, optimize for names searches
      const nameSearchResults = await aggregationPipelineName(searchTerms);
      searchResults = searchResults.concat(nameSearchResults);
    } catch (error) {
      throw new Error(error);
    }
  }

  if (searchTerms.length === 3 && !email) {
    try {
      const middleNameResults = await findUserWithMiddleName(searchTerms);
      searchResults = searchResults.concat(middleNameResults);
    } catch (error) {
      throw new Error(error);
    }
  }

  // finally, if number of results so far is under 200, run an aggregation pipeline using Atlas Search
  if (searchResults.length < 200) {
    const maxResults = 200 - searchResults.length;
    try {
      const textSearchResults = await aggregationPipelineFirstLastEmail(searchTerms, maxResults);
      searchResults = searchResults.concat(textSearchResults);
    } catch (error) {
      throw new Error(error);
    }
  }

  // Clever piece of code reduces the array of results to uniques based on the object id.
  // The smart bit is Set only allows uniques and will silently not add a duplicate.
  // I take not credit in coming up with this solution.
  const ids = searchResults.map((a) => a._id.toString());
  const uniqueIds = Array.from(new Set(ids));
  const uniqueResults = uniqueIds.map((id) => searchResults.find((a) => a._id.toString() === id));

  return uniqueResults;
};

/**
 * @param id
 * @returns {Promise}
 */
export const getUserById = (id) =>
  UserModel.findOne({ _id: id }).populate('linkedClients.clientId', ['name', 'appImageURL']);

/**
 * @param email
 * @returns {Promise}
 */
export const getUserByEmail = (email) =>
  UserModel.findOne({ email: email.toLowerCase() }).populate('linkedClients.clientId', [
    'name',
    'appImageURL',
  ]);

/**
 * @param usID SQL identifier we store to link users
 * @returns {Promise}
 */
export const getUserByUsID = (usID) => UserModel.findOne({ usID });

/**
 * @param linkedAccount
 * @returns {Promise}
 */
export const getUserByLinkedAccount = (linkedAccount) =>
  UserModel.findOne({
    'linkedAccount.facebook.id': linkedAccount,
  }).populate('linkedClients.clientId', ['name', 'appImageURL']);

/**
 * @param id
 * @param changedFields
 * @returns {Promise}
 */
export const updateUser = (id, changedFields) => {
  const updateRequest = changedFields;
  if (updateRequest.email) {
    updateRequest.email = updateRequest.email.toLowerCase();
  }
  updateRequest.modifiedDate = new Date();
  delete updateRequest._id;
  delete updateRequest.password;
  return new Promise((resolve, reject) =>
    UserModel.findOneAndUpdate(
      { _id: id },
      { $set: updateRequest },
      { new: true, runValidators: true },
      (err, user) => {
        if (err) {
          log.error({
            message: `updateUser(${id}):findOneAndUpdate`,
            stack: err,
          });
          reject(err);
        } else if (!user) {
          // Catch rare case where user doesn't exist but error isn't returned
          log.error({
            message: `updateUser(${id}):findOneAndUpdate user returned from findOneAndUpdate is null`,
          });
          reject(new Error('User returned from findOneAndUpdate is null'));
        } else {
          trackAccountUpdated(user);
          const userObj = user.toObject();
          delete userObj.password;
          resolve(userObj);
        }
      },
    ),
  );
};

/**
 * @param {string} id
 * @param {string} password
 * @param {string} platform
 * @returns {Promise}
 */
export const changeUserPassword = async (_id, password, platform) => {
  let hasExistingPassword;
  try {
    const foundUser = await getUserById(_id);
    const { hasPassword } = foundUser;
    hasExistingPassword = hasPassword;
  } catch (err) {
    log.error({
      message: `changeUserPassword(${_id}):getUserById`,
      stack: err,
    });
  }

  const hashedPassword = await hashPassword(password);

  try {
    const user = await UserModel.findOneAndUpdate(
      { _id },
      {
        $set: {
          password: hashedPassword,
          modifiedDate: new Date(),
        },
      },
      { new: true, runValidators: true },
    );
    trackSetNewPassword(user._id, hasExistingPassword, platform);
    return user;
  } catch (error) {
    log.error({
      message: `changeUserPassword(${_id}):findOneAndUpdate`,
      stack: error,
    });
    throw error;
  }
};

/**
 * @param {string} id
 * @param {string} password (not hashed)
 * @param {string} newPassword (not hashed)
 * @param {string} clientId
 * @param {boolean} isShortLived
 * @param {string} authorization
 * @param {string} platform
 */
export const confirmAndChangeUserPassword = async (
  id,
  password,
  newPassword,
  clientId,
  isShortLived,
  authorization,
  platform,
) => {
  try {
    const user = await getUserPassword(id);
    await matchPassword(password, user.password);
    const updatedUser = await changeUserPassword(id, newPassword, platform);
    await authService.invalidateToken(id);
    const tokens = await authService.getTokenForUser(
      user.email,
      newPassword,
      clientId,
      isShortLived,
      authorization,
    );
    const response = Object.assign(
      updatedUser.toObject(),
      { accessToken: tokens.access_token },
      { refreshToken: tokens.refresh_token },
      { expiresIn: tokens.expires_in },
    );
    return response;
  } catch (err) {
    log.debug({
      message: `confirmAndChangeUserPassword(${id})`,
      stack: err,
    });
    throw err;
  }
};

/**
 * @param userId
 * @returns {Promise}
 */
export const deactivateUser = (userId) => updateUser(userId, { isActive: false });

/**
 * @param userFields
 * @returns {Promise|*}
 */
export function createUser(userFields, generatedPassword, method, clientName) {
  const { email, settings } = userFields;
  const lowerCaseEmail = email.toLowerCase();
  const receivePromotions = settings && settings.receivePromotions;
  return new Promise((resolve, reject) => {
    // Using Object.assign so that password isn't changed in the body.password
    const newUser = { ...userFields };
    newUser.email = lowerCaseEmail;
    hashPassword(newUser.password)
      .then((hashedPassword) => {
        newUser.password = hashedPassword;
        const user = new UserModel(newUser);
        return user.save((err) => {
          if (err) {
            log.error({
              message: 'createUser:UserModel()',
              stack: err,
            });
            reject(err);
          } else {
            trackAccountCreated({
              user,
              generatedPassword,
              method,
              receivePromotions,
              platform: clientName,
            });
            const userObj = user.toObject();
            delete userObj.password;
            resolve(userObj);
          }
        });
      })
      .catch((err) => reject(err));
  });
}

/**
 *
 * @param userFields
 * @returns {MongooseError|undefined}
 */
export function validateUser(userFields) {
  const user = new UserModel(userFields);
  return user.validateSync();
}

/**
 * Verifies a user's email against the params passed into this function
 *
 * @param {string} userId
 * @param {string} email
 * @param {string} brand
 * @param {string} platform
 *
 * @returns { Promise<{isEmailVerified: string, _id: string}> }
 */
export const verifyEmail = async ({ userId, email, brand, platform }) => {
  try {
    const user = await UserModel.findOne({ _id: userId, email });

    if (!user) {
      log.error(`Invalid email verification. User not found ${email}`);
      throw new Error('Invalid email verification. User not found');
    }

    user.isEmailVerified = true;
    user.markModified('isEmailVerified');

    const { _id: id } = await user.save();
    trackEmailVerified(id.toString(), brand, platform);

    return user;
  } catch (error) {
    log.error(`Invalid email verification. ${email}`);
    throw error;
  }
};

/**
 * Looks up a user and encrypts the id and email for email verification
 *
 * @param {string} userId
 * @param {string} brand
 * @param {string} platform
 *
 * @returns Promise
 */
export async function triggerEmailVerification({ userId, brand, platform }) {
  try {
    const { _id, email } = await UserModel.findOne({ _id: userId });
    const trackingParams = { userId: _id.toString(), email, brand, platform };
    trackTriggerEmailVerification(trackingParams);
    return email;
  } catch (error) {
    log.error(`Invalid email verification request. ${userId}`);
    throw new Error('Invalid email verification request');
  }
}

export const getUserByClientAccessToken = (accessToken) =>
  UserModel.findOne({ 'linkedClients.token': accessToken }).lean();

/**
 * Deregisters a user's linked clients
 * @param {string[]} userAccessTokens Linked client tokens to delete for a user
 * @returns {Promise}
 */
export const deregisterLinkedClients = (userAccessTokens) =>
  UserModel.updateMany(
    { 'linkedClients.token': { $in: userAccessTokens } },
    { $pull: { linkedClients: { token: { $in: userAccessTokens } } } },
    { multi: true },
  );

/**
 * Deletes a UserInfo document
 * @param {string} userId
 * @param {object} opts
 * @returns {Promise}
 */
export const deleteUser = async (userId, opts) => UserModel.findOneAndDelete({ _id: userId }, opts);
