import request from 'request';
import { createLogger } from '../common/logger';

const log = createLogger('user-service:authenticate');

/**
 * To obtain a token you should POST to /auth/token/.
 * You should include grant_type ("password"), username and password in the request body
 * Optional fields - clientId, deviceId, deviceType
 * -
 * POST /auth/token/ HTTP/1.1
 * Host: server.example.com
 * Content-Type: application/x-www-form-urlencoded
 * grant_type=password&username=johndoe&password=A3ddj3w
 * &client_id=WEB_VTM8QWOFNZ4X&device_id=UNQ123&device_type=Web_Browser
 * -
 * And the response will be..
 * -
 * HTTP/1.1 200 OK
 * Content-Type: application/json;charset=UTF-8
 * Cache-Control: no-store
 * Pragma: no-cache
 * {
 *    "access_token":"2YotnFZFEjr1zCsicMWpAA",
 *    "token_type":"bearer",
 *    "expires_in":3600,
 *    "refresh_token":"tGzv3JOkF0XG5Qx2TlKWIA"
 * }
 *
 * @param userEmail
 * @param passwordHash
 * @param clientId
 * @param deviceId
 * @param deviceType
 * @param isShortLived 'true' or else false
 * @returns {Promise.<T>}
 */
export const getTokenForUser = (userEmail, passwordHash, clientId, isShortLived, authorization) =>
  new Promise((resolve, reject) => {
    const authEndpoint = authorization ? '/trusted/token/' : '/auth/token/';
    const options = {
      uri: `${process.env.AUTH_SERVICE}${authEndpoint}`,
      method: 'POST',
      form: {
        username: userEmail,
        password: passwordHash,
        grant_type: 'password',
        client_id: clientId,
      },
      json: true,
      headers: {
        authorization,
      },
    };
    if (isShortLived === 'true') {
      options.qs = { isShortLived: 'true' };
    }
    request(options, (error, response, body) => {
      if (response && body && response.statusCode === 200) {
        resolve(body);
      } else {
        log.error({
          message: `getTokenForUser(${userEmail})`,
          stack: error,
        });
        reject(response);
      }
    });
  });

export const invalidateToken = (userId) =>
  new Promise((resolve, reject) => {
    const options = {
      uri: `${process.env.AUTH_SERVICE}/auth/invalidate-token/`,
      method: 'POST',
      form: { id: userId },
      json: true,
    };
    request(options, (error, response, body) => {
      if (response && body && response.statusCode === 200) {
        resolve(body);
      } else {
        log.error({
          message: `invalidateToken(${userId})`,
          stack: error,
        });
        reject(new Error(error));
      }
    });
  });

export const getClientNameFromClientId = (clientId) =>
  new Promise((resolve, reject) => {
    const options = {
      uri: `${process.env.AUTH_SERVICE}/trusted/client-name?clientId=${clientId}`,
      method: 'GET',
      json: true,
    };
    request(options, (error, response, body) => {
      if (response && body && response.statusCode === 200) {
        resolve(body);
      } else {
        log.error({
          message: `invalid client id: ${clientId}`,
          stack: error,
        });
        reject(response);
      }
    });
  });
