import fetch from 'isomorphic-fetch';

/**
 * Calls the favorites service to delete the associated user's favorites
 * @param {*} userId The user to delete (meter)
 * @returns
 */
export const deleteFavorites = async (userId) => {
  const response = await fetch(`${process.env.FAVORITES_API}/favorites/${userId}`, {
    method: 'DELETE',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
  });

  const responseBody = await response.json();
  if (response.status === 200) return responseBody;
  throw responseBody;
};

export const deleteBwFavorites = async (userId) => {
  const response = await fetch(
    `http://${process.env.UPSTREAM_BUOYWEATHER_API}/admin/userfavorites/${userId}`,
    {
      method: 'DELETE',
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
      },
    },
  );

  const responseBody = await response.json();
  if (response.status === 200) return responseBody;
  throw responseBody;
};
