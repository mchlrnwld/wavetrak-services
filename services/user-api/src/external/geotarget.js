import request from 'request';
import { createLogger } from '../common/logger';

const log = createLogger('user-service:geo-target');

export const getRegion = (ipAddress) =>
  new Promise((resolve, reject) => {
    const query = { ipAddress };
    const options = {
      uri: `${process.env.GEO_TARGET_SERVICE}/Region`,
      qs: query,
      json: true,
      timeout: 8000,
    };
    request(options, (error, response, body) => {
      if (error) {
        log.error({
          message: `Error from external:getRegion ${ipAddress}`,
          stack: error.stack,
        });
        reject(error);
      } else if (response.statusCode > 200) {
        reject(response);
      } else {
        resolve(body);
      }
    });
  });
