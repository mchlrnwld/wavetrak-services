import fetch from 'isomorphic-fetch';

const parseRequest = async (resp) => {
  const contentType = resp.headers.get('Content-Type');
  const body = contentType.indexOf('json') > -1 ? await resp.json() : await resp.text();
  if (resp.status > 200) {
    throw body;
  }
  return body;
};

export const syncUserDetails = (userId, details) =>
  fetch(`${process.env.SUBSCRIPTION_SERVICE_URL}/subscriptions/billing/sync`, {
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      'x-auth-userid': userId,
    },
    method: 'PUT',
    body: JSON.stringify(details),
  }).then(parseRequest);

/**
 * Calls the subscription-api to delete the associated subscription-user document
 * @param {*} userId The user to delete (subscription-user)
 * @returns
 */
export const deleteSubscriptionUser = async (userId) =>
  fetch(`${process.env.SUBSCRIPTION_SERVICE_URL}/admin/subscription-users`, {
    method: 'DELETE',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
    body: JSON.stringify({ userId }),
  }).then(parseRequest);
