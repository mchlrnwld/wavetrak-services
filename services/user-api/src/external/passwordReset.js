import fetch from 'isomorphic-fetch';

/**
 * Calls password-reset-service to delete any existing password reset codes
 * @param {userId} the user's id
 */
export const invalidatePasswordResetCode = async (userId) => {
  const response = await fetch(
    `http://${process.env.UPSTREAM_PASSWORD_RESET_SERVICE}/password-reset/invalidate-code`,
    {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
      },
      body: JSON.stringify({ userId }),
    },
  );
  const responseBody = await response.json();
  if (response.status === 200) return responseBody;
  throw responseBody;
};

export default invalidatePasswordResetCode;
