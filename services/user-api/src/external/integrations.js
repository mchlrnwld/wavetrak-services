import fetch from 'isomorphic-fetch';
import CIO from 'customerio-node';

const bwCIO = new CIO(process.env.BW_CIO_SITEID, process.env.BW_CIO_APIKEY);
const fsCIO = new CIO(process.env.FS_CIO_SITEID, process.env.FS_CIO_APIKEY);
const slCIO = new CIO(process.env.SL_CIO_SITEID, process.env.SL_CIO_APIKEY);

const segmentApi = 'https://platform.segmentapis.com/v1beta/workspaces/surfline';
const segmentToken = process.env.SEGMENT_ACCESS_TOKEN;

const amplitudeApi = 'https://amplitude.com/api/2';
const amplitudeApiKey = process.env.AMPLITUDE_API_KEY;
const amplitudeSecretKey = process.env.AMPLITUDE_SECRET_KEY;

const brazeApiUrl = process.env.BRAZE_API_URL;
const brazeApiKey = process.env.BRAZE_API_KEY;

export const createSegmentRegulation = async (userId) => {
  const authHeader = `Bearer ${segmentToken}`;
  const body = {
    regulation_type: 'Suppress_With_Delete',
    attributes: {
      name: 'userId',
      values: [userId],
    },
  };
  const response = await fetch(`${segmentApi}/regulations`, {
    method: 'POST',
    headers: {
      Authorization: authHeader,
      'content-type': 'application/json',
    },
    body: JSON.stringify(body),
  });
  const responseBody = await response.json();
  if (response.status === 200) return responseBody;
  throw responseBody;
};

export const deleteCustomerIOUser = (userId) => {
  slCIO.destroy(userId);
  bwCIO.destroy(userId);
  fsCIO.destroy(userId);
};

export const deleteAmplitudeUser = async (userId) => {
  const body = {
    user_ids: [userId],
    requester: 'Surfline User Admin',
    ignore_invalid_id: 'True',
  };
  const response = await fetch(`${amplitudeApi}/deletions/users`, {
    method: 'POST',
    headers: {
      Authorization: `Basic ${Buffer.from(`${amplitudeApiKey}:${amplitudeSecretKey}`).toString(
        'base64',
      )}`,
      'content-type': 'application/json',
    },
    body: JSON.stringify(body),
  });
  if (response.status === 200) return response;
  throw response;
};

/**
 * @description Deletes the data associated with {userId} from Braze by
 * making a POST request to the /users/delete Braze endpoint.
 * https://www.braze.com/docs/api/endpoints/user_data/post_user_delete/
 *
 * @param {string} userId
 */
export const deleteBrazeUser = async (userId) => {
  const body = {
    external_ids: [userId],
  };
  const response = await fetch(`${brazeApiUrl}/users/delete`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${brazeApiKey}`,
      'content-type': 'application/json',
    },
    body: JSON.stringify(body),
  });
  if (response.status === 201) return response;
  throw response;
};
