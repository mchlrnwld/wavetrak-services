import fetch from 'isomorphic-fetch';
import { createOauthHeaders } from '../common/oauth';
import { createLogger } from '../common/logger';

const log = createLogger('user-service:garmin');

/**
 * Sends a request to Garmin to disconnect the user by invalidating their access token.
 * If successful, Garmin will send a Deregistration ping to the `/garmin/deregister` endpoint,
 * which deletes the user's access token and secret from Surfline.
 * Documentation: https://developerportal.garmin.com/download/file/fid/611
 * @param {string} accessToken The access token to delete
 * @param {string} tokenSecret The secret corresponding to the access token
 */
export const invalidateGarminToken = async (accessToken, tokenSecret) => {
  const garminInvalidateTokenApi = 'https://apis.garmin.com/wellness-api/rest/user/registration';
  const method = 'DELETE';

  const consumer = {
    key: process.env.GARMIN_CONSUMER_KEY,
    secret: process.env.GARMIN_CONSUMER_SECRET,
  };

  const OAuthHeaders = createOauthHeaders(
    consumer,
    accessToken,
    tokenSecret,
    garminInvalidateTokenApi,
    method,
  );

  const response = await fetch(garminInvalidateTokenApi, {
    method: 'DELETE',
    headers: {
      'content-type': 'application/json',
      ...OAuthHeaders,
    },
  });

  if (response.status === 204) {
    log.info(`Deleted access token ${accessToken} from Garmin`);
    return response.body;
  }
  if (response.status === 403) {
    log.info(`Invalid Garmin user access token: ${accessToken}`);
    return response.body;
  }
  throw response.body;
};
