import fetch from 'isomorphic-fetch';

/**
 * Calls metering service to delete a meter for a given user
 * @param {string} userId The user to delete (meter)
 */
export const deleteMeter = async (userId) => {
  const response = await fetch(`${process.env.METERING_SERVICE}/admin/meters`, {
    method: 'DELETE',
    headers: {
      'content-type': 'application/json',
    },
    body: JSON.stringify({ user: userId }),
  });
  const responseBody = await response.json();
  if (response.status === 200 || response.status === 404) return responseBody;
  throw responseBody;
};

export default deleteMeter;
