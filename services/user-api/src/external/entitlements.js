import fetch from 'isomorphic-fetch';

const getEntitlements = async (userId) => {
  const query = `objectId=${encodeURIComponent(userId)}`;
  const url = `${process.env.ENTITLEMENTS_SERVICE}/entitlements?${query}`;

  const response = await fetch(url);

  if (response.status === 200) return response.json();
  if (response.status === 400) return { entitlements: [] };

  throw new Error(`Error making request to ${url}: ${await response.text()}`);
};

/**
 * Calls the entitlement service to check for premium entitlements
 * @param {*} userId The user's id
 * @returns whether the user has any premium entitlement
 */
export const isPremium = async (userId) => {
  if (!userId) return false;
  const { entitlements } = await getEntitlements(userId);
  return (
    entitlements.includes('sl_premium') ||
    entitlements.includes('bw_premium') ||
    entitlements.includes('fs_premium')
  );
};

export default getEntitlements;
