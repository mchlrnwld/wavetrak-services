import { createLogger, setupLogsene } from '@surfline/services-common';

setupLogsene(process.env.LOGSENE_KEY);

const logger = createLogger('user-service');
logger.setMaxListeners(50);

export { createLogger };
export default logger;
