import crypto from 'crypto';
import OAuth from 'oauth-1.0a';

/**
 * @description Util function to create the OAuth 1.0a request signing header.
 * @param {object} consumer
 * @param {string} userAccessToken
 * @param {string} tokenSecret
 * @param {string} url
 * @param {string} method
 * @returns {{Authorization: string}}
 */
export const createOauthHeaders = (consumer, userAccessToken, tokenSecret, url, method) => {
  const oauth = OAuth({
    consumer,
    signature_method: 'HMAC-SHA1',
    hash_function(baseString, key) {
      return crypto.createHmac('sha1', key).update(baseString).digest('base64');
    },
  });

  const token = {
    key: userAccessToken,
    secret: tokenSecret,
  };

  const requestData = {
    url,
    method,
  };

  return oauth.toHeader(oauth.authorize(requestData, token));
};
