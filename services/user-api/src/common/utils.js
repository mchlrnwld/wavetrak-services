export const getMigratedBrandName = (migratedBrand) => {
  if (migratedBrand?.toLowerCase() === 's2s') {
    return 'Surf 2 Surf';
  }
  return null;
};
