import crypto from 'crypto';

const algorithm = process.env.ENCRYPTION_ALGORITHM;
const password = process.env.ENCRYPTION_KEY;

export function encrypt(userId, email) {
  try {
    const cipher = crypto.createCipher(algorithm, password);
    const text = `${userId}#${email}`;
    let crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
  } catch (err) {
    throw new Error(err);
  }
}

export function decrypt(text) {
  try {
    const decipher = crypto.createDecipher(algorithm, password);
    let dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    const [userId, email] = dec.split('#');
    return {
      userId,
      email,
    };
  } catch (err) {
    throw new Error(err);
  }
}
