import * as analytics from './analytics';
import { encrypt } from './encryption';
import { getRegion } from '../external/geotarget';
import log from './logger';

const mapPlatformToTrackingPlan = (platform) => {
  if (platform && platform.includes('iOS')) return 'ios';
  if (platform && platform.includes('Droid')) return 'android';
  if (platform && platform.includes('Web')) return 'web';
  return null;
};

export function trackAccountCreated({
  user,
  accountRecovered,
  generatedPassword,
  method,
  receivePromotions,
  platform,
}) {
  const fullBrandNameMap = { sl: 'surfline', bw: 'buoyweather', fs: 'fishtrack' };
  const {
    _id,
    firstName,
    lastName,
    email,
    gender,
    locale,
    usID,
    brand,
    isEmailVerified,
    hasPassword,
    cwUserId,
  } = user;
  const emailVerificationHash = encrypt(_id.toString(), email);

  analytics.identifyAll(`${_id}`, {
    firstName,
    lastName,
    email,
    gender,
    locale,
    createdAt: Date.now(),
    usID,
    cwUserId,
    receivePromotions,
    isEmailVerified,
    brand: fullBrandNameMap[brand],
    'subscription.entitlement': `${brand}_default`,
    'subscription.trial_eligible': true,
  });

  if (!cwUserId) {
    // TODO: remove cwUserId condition after CW Migration
    analytics.track(
      'Account Created',
      `${_id}`,
      {
        method: method || 'email', // email || facebook || ...
        accountRecovered,
        brand: fullBrandNameMap[brand],
        generatedPassword: generatedPassword || null,
        emailVerificationHash,
        platform: mapPlatformToTrackingPlan(platform),
        hasPassword,
      },
      brand,
    );
  }
}

export function trackAccountUpdated(user) {
  if (!user) return;
  const { _id, firstName, lastName, email, locale, gender, usID, isEmailVerified, cwUserId } = user;
  analytics.identifyAll(`${_id}`, {
    firstName,
    lastName,
    email,
    gender,
    locale,
    usID,
    isEmailVerified,
    cwUserId,
  });
}

export const trackAccountGeoIP = async (ip, user) => {
  let continentName = null;
  let countryName = null;
  let countryIso = null;
  let locationSubdivisionOne = null;
  let locationSubdivisionTwo = null;
  let locationSubdivisionTwoIso = null;
  try {
    const regionInfo = await getRegion(ip);
    if (regionInfo) {
      continentName = regionInfo.continent.name ? regionInfo.continent.name : null;
      countryName = regionInfo.country.name ? regionInfo.country.name : null;
      countryIso = regionInfo.country.iso_code ? regionInfo.country.iso_code : null;
      const { subdivisions } = regionInfo;
      if (subdivisions) {
        if (subdivisions.length >= 1) {
          locationSubdivisionOne = subdivisions[0].name ? subdivisions[0].name : null;
        }
        if (subdivisions.length === 2) {
          locationSubdivisionTwo = subdivisions[1].name ? subdivisions[1].name : null;
          locationSubdivisionTwoIso = subdivisions[1].iso_code ? subdivisions[1].iso_code : null;
        }
      }
    }
  } catch (err) {
    log.warn({ message: `User created from a bad IP Address: ${ip}.`, stack: err.stack });
  }

  analytics.identifyAll(`${user._id}`, {
    'location.continent': continentName,
    'location.country': countryName,
    'location.countryIso': countryIso,
    'location.subdivisionOne': locationSubdivisionOne,
    'location.subdivisionTwo': locationSubdivisionTwo,
    'location.subdivisionTwoIso': locationSubdivisionTwoIso,
  });
};

export const trackPromoCodeGenerated = (userId, properties) => {
  analytics.track('Generated Promo Code', userId, { ...properties });
};

export const trackEmailVerified = (userId, brand, platform) => {
  analytics.identifyAll(userId, { isEmailVerified: true });
  analytics.track('Verified Email', userId, { platform }, brand);
};

export const trackTriggerEmailVerification = (params) => {
  const { userId, email, brand, platform } = params;
  const emailVerificationHash = encrypt(userId.toString(), email);
  analytics.track(
    'Requested Email Verification',
    userId,
    { email, brand, emailVerificationHash, platform },
    brand,
  );
};

export const trackSetNewPassword = (userId, hasExistingPassword, platform) => {
  analytics.trackAll('Set New Password', userId.toString(), {
    enabledAccount: !hasExistingPassword,
    platform,
  });
};

/**
 * Tracking event for when an account is deleted
 */
export const trackDeletedAccount = (params) => {
  const { brand, deleteRequestedAt, userId } = params;
  analytics.track(
    'Deleted Account',
    userId,
    {
      trigger: deleteRequestedAt ? 'user' : 'admin',
    },
    brand,
  );
};
