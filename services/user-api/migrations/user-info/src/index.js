import { initMongo, setupLogsene, createLogger } from '@surfline/services-common';
import Analytics from 'analytics-node';
import csvtojson from 'csvtojson';
import fs from 'fs';
import { createObjectCsvWriter } from 'csv-writer';
import mongoose from 'mongoose';
import { argv } from 'yargs';
import UserInfoModel from './models/UserInfoModel';

setupLogsene(process.env.LOGSENE_KEY);
const log = createLogger('user-api/migrations/user-info');
const analytics = new Analytics(process.env.SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });
const { import_filename: importFilename, live } = argv;
const isLive = live === 'true';

let successCount = 0;
let duplicateCount = 0;
let recordCount = 0;
let errCount = 0;

/**
 * Parses the input CSV file to get the headers (if any)
 * @returns {string[]}
 */
const getCSVHeaders = async () =>
  new Promise((resolve, reject) => {
    try {
      csvtojson()
        .fromFile(importFilename)
        .on('header', (headers) => {
          resolve(headers.map((header) => ({ title: header, id: header })));
        })
        .on('done', (err) => {
          if (err) reject(err);
          resolve([]);
        });
    } catch (err) {
      reject(err);
    }
  });

/**
 * Parses the CSV and attempts to call the callback for each row
 * @param {function} callback
 * @returns
 */
const migrateCSV = (callback) =>
  new Promise((resolve, reject) => {
    csvtojson()
      .fromFile(importFilename)
      .subscribe(async (csvRow) => await callback(csvRow))
      .on('done', (err) => {
        if (err) reject(err);
        resolve();
      });
  });

/**
 * Executes the user-info migration script.
 * @returns
 */
const runMigration = async () => {
  try {
    log.info(`Starting user-info migration`);

    if (!importFilename || !fs.existsSync(`${importFilename}`)) {
      log.warn('No input file detected');
      return;
    }

    log.info('args', { importFilename, isLive });

    const headers = await getCSVHeaders();

    log.info('Headers', headers);

    // Note any duplicate users based on email address
    const duplicateUsersFile = createObjectCsvWriter({
      path: `duplicate-users.csv`,
      header: [...headers, { id: '_id', title: '_id' }],
    });

    // Results of executing the script
    const executionResultsFile = createObjectCsvWriter({
      path: `execution-results.csv`,
      header: [...headers, { id: '_id', title: '_id' }],
    });

    const migrateRow = async (csvRow) => {
      recordCount += 1;
      try {
        const { brand, email, firstName, lastName, systemName, systemId } = csvRow;
        if (!email) {
          log.warn('No email in this record', csvRow);
          return;
        }
        const existingUser = await UserInfoModel.findOne({
          email: email?.toLowerCase().trim(),
        })?.lean();

        if (existingUser) {
          log.warn('Existing user found', existingUser);
          await duplicateUsersFile.writeRecords([{ ...csvRow, _id: existingUser._id }]);
          duplicateCount += 1;
        } else {
          let _id;
          const formattedEmail = email.toLowerCase().trim();
          if (isLive) {
            _id = (
              await UserInfoModel.create({
                brand,
                email: formattedEmail,
                firstName,
                lastName,
                migratedUser: `${systemName || 'system'}_${systemId}`,
              })
            )?._id;
            analytics.identify({
              userId: _id.toString(),
              traits: {
                firstName,
                lastName,
                email: formattedEmail,
                migratedBrand: systemName,
              },
            });
          }
          await executionResultsFile.writeRecords([{ ...csvRow, _id }]);
          successCount += 1;
        }
      } catch (err) {
        log.error(err);
        errCount += 1;
      }
    };

    await Promise.all([initMongo(mongoose, process.env.MONGO_CONNECTION_STRING)]);
    await migrateCSV(migrateRow);
    log.info(
      `Finished user-info migration. Success Records: ${successCount}, Duplicate Records: ${duplicateCount}, Total Records: ${recordCount}, Error Count: ${errCount}`,
    );
  } catch (err) {
    log.error(err);
    throw err;
  }
};

runMigration()
  .then(() => process.exit(0))
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
