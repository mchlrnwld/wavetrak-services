# Create UserInfo for Users

This script takes an input file and attempts to create `UserInfo` documents based on the records in the file. This is useful for migrating from

## Quickstart

## S2S
Runs this script looking for the `s2s_import.csv`
```
npm install
node run import:s2s
```

### args
```
-- import_filename // (required) CSV to import
-- live // (optional) Determines whether to save the documents to the database
```

