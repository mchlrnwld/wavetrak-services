import { MongoClient } from 'mongodb';
import { config } from 'dotenv';
import { argv } from 'yargs';

config();

const main = async () => {
  console.log('Connecting to MongoDB...');
  const db = await MongoClient.connect(process.env.MONGO_CONNECTION_STRING);

  try {
    console.log('Finding US users with default metric units...');
    const metricUserSettings = await db.collection('UserSettings').find({
      $and: [
        { 'units.surfHeight': 'M' },
        { 'units.swellHeight': 'M' },
        { 'units.tideHeight': 'M' },
        { 'units.windSpeed': 'KPH' },
        { 'units.temperature': 'C' },
      ],
    }).toArray();
    const usUsersWithMetric = await db.collection('UserInfo').find({
      $and: [
        {
          locale: 'en-US',
        },
        {
          _id: {
            $in: metricUserSettings.map(({ user }) => user),
          },
        },
      ],
    }).toArray();

    if (!argv.save) return usUsersWithMetric;

    if (!usUsersWithMetric.length) return 'All users have settings already.';

    console.log('Converting user settings to imperial units...');
    const result = await db.collection('UserSettings').updateMany(
      {
        user: {
          $in: usUsersWithMetric.map(({ _id }) => _id),
        },
      },
      {
        $set: {
          'units.surfHeight': 'FT',
          'units.swellHeight': 'FT',
          'units.tideHeight': 'FT',
          'units.windSpeed': 'KTS',
          'units.temperature': 'F',
        },
      },
    );
    return result;
  } finally {
    db.close();
  }
};

main().then((result) => {
  console.log(result);
  process.exit(0);
}).catch((err) => {
  console.error(err);
  process.exit(1);
});
