# Convert US Users with Default Metric Units to Imperial

This script finds all US users who have the default metric `units` in `UserSettings` and converts them to imperial `units`.

## Quick Start

Running this command will `console.log` a list of `UserInfos` for users whose `UserSettings` are going to be converted. It does not actually save the `UserSettings`.

```sh
$ yarn install --pure-lockfile # or npm install
$ yarn start
```

## Convert UserSettings

```sh
$ yarn start -- --save
```
