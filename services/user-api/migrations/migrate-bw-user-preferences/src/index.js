/* eslint-disable no-restricted-syntax */
/* eslint-disable no-await-in-loop */
import { initMongo, setupLogsene, createLogger } from '@surfline/services-common';
import mongoose from 'mongoose';
import mssql from 'mssql';
import cliProgress from 'cli-progress';
import initSQL from './initSQL';
import UserSettingsModel from './models/UserSettingsModel';
import UserInfoModel from './models/UserInfoModel';

setupLogsene(process.env.LOGSENE_KEY);
const log = createLogger('migrate-bw-user-preferences');

const runMigration = async () => {
  let updatedCount = 0;
  let notUpdatedCount = 0;
  let errorCount = 0;
  let missingUserCount = 0; // for users from the legacy db with no matching record in mongo
  let bwUsers;

  try {
    const queryResult = await mssql.query`select usID, bwPreferredUnits from tblUsers where bwPreferredUnits is not null`;
    bwUsers = queryResult.recordset;
  } catch (error) {
    throw new Error(error.message);
  }

  const total = bwUsers.length;

  const progressBar = new cliProgress.SingleBar({
    format: `BW unit preference migration | {percentage}% || {value}/{total} Users\n`,
    barCompleteChar: '\u2588',
    barIncompleteChar: '\u2591',
  });
  progressBar.start(total, 0);

  for (const bwUser of bwUsers) {
    const { usID, bwPreferredUnits = null } = bwUser;
    try {
      const user = await UserInfoModel.findOne({ usID });
      if (user) {
        const updatedDoc = await UserSettingsModel.findOneAndUpdate(
          { user: user._id, bwUnitPreference: null },
          { bwUnitPreference: bwPreferredUnits },
        );
        if (updatedDoc) {
          updatedCount += 1;
        } else {
          notUpdatedCount += 1;
        }
      } else {
        missingUserCount += 1;
      }
      progressBar.increment();
    } catch (error) {
      errorCount += 1;
    }
  }

  log.info(
    `Finished BW unit preference migration. Total Records: ${total}, Records updated: ${updatedCount}, Records not updated: ${notUpdatedCount}, Missing Records: ${missingUserCount}, Error Count: ${errorCount}`,
  );
};

Promise.all([initMongo(mongoose, process.env.MONGO_CONNECTION_STRING), initSQL(mssql)])
  .then(async () => {
    await runMigration();
    process.exit(0);
  })
  .catch((err) => {
    log.error({
      message: 'Fatal Error running user migration script',
      errorMessage: err.message,
      stack: err.stack,
    });
    process.exit(1);
  });
