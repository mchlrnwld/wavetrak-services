import mongoose, { Schema } from 'mongoose';

const options = {
  collection: process.env.MONGO_USER_SETTINGS_COLLECTION || 'UserSettings',
  timestamps: true,
};

const UserSettingsSchema = Schema(
  {
    user: { type: Schema.Types.ObjectId },
    bwUnitPreference: { type: String },
  },
  options,
);

export default mongoose.model('UserSettings', UserSettingsSchema);
