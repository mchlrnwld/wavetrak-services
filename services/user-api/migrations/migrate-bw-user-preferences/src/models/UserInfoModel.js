import mongoose, { Schema } from 'mongoose';

const options = {
  collection: process.env.MONGO_USER_INFO_COLLECTION || 'UserInfo',
  timestamps: true,
};

const UserInfoSchema = Schema(
  {
    usID: { type: Number },
  },
  options,
);

export default mongoose.model('UserInfo', UserInfoSchema);
