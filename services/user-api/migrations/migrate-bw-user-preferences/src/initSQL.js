const sqlConfig = {
  user: process.env.LEGACY_DB_USER,
  password: process.env.LEGACY_DB_PWD,
  database: process.env.LEGACY_DB_NAME,
  server: process.env.LEGACY_DB_HOST,
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 30000,
  },
  options: {
    encrypt: true,
    trustServerCertificate: true,
  },
};

export default async (mssql) => {
  try {
    await mssql.connect(sqlConfig);
  } catch (error) {
    throw new Error(error.message);
  }
};
