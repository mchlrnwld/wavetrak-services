# Create UserSettings for Users

This script creates the `UserSettings` document for `Users` who don't already have one. It defaults the `User`'s units based on their `locale`.

## Quick Start

Running this command will `console.log` a list of `UserSettings` to be created. It does not actually save the `UserSettings`.

```sh
$ yarn install --pure-lockfile # or npm install
$ yarn start
```

## Save Created UserSettings

```sh
$ yarn start -- --save
```
