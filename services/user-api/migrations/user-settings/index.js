import { MongoClient } from 'mongodb';
import { config } from 'dotenv';
import { argv } from 'yargs';

config();

const defaultUnits = (locale) => {
  if (locale.match(/GB|IE/)) {
    return {
      surfHeight: 'FT',
      swellHeight: 'FT',
      tideHeight: 'M',
      windSpeed: 'MPH',
      temperature: 'C',
    };
  } else if (locale.match(/US|LR|MM/)) {
    return {
      surfHeight: 'FT',
      swellHeight: 'FT',
      tideHeight: 'FT',
      windSpeed: 'KTS',
      temperature: 'F',
    };
  }

  return {
    surfHeight: 'M',
    swellHeight: 'M',
    tideHeight: 'M',
    windSpeed: 'KPH',
    temperature: 'C',
  };
};

const main = async () => {
  console.log('Connecting to MongoDB...');
  const db = await MongoClient.connect(process.env.MONGO_CONNECTION_STRING);

  try {
    console.log('Finding users without user settings...');
    const allUserSettings = await db.collection('UserSettings').find({}).toArray();
    const usersWithSettings = allUserSettings.map(({ user }) => user);
    const usersWithoutSettings = await db.collection('UserInfo').find({ _id: { $nin: usersWithSettings } }).toArray();
    const newUserSettings = usersWithoutSettings.map(({ _id, locale }) => ({
      user: _id,
      units: defaultUnits(locale),
      onboarded: [],
      preferredWaveChart: 'SURF',
    }));

    if (!argv.save) return newUserSettings;

    if (!newUserSettings.length) return 'All users have settings already.';

    console.log('Creating new user settings...');
    const result = await db.collection('UserSettings').insertMany(newUserSettings);
    return result;
  } finally {
    db.close();
  }
};

main().then((result) => {
  console.log(result);
  process.exit(0);
}).catch((err) => {
  console.error(err);
  process.exit(1);
});
