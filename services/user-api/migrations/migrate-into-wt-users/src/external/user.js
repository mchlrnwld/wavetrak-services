import axios from 'axios';

export const createUser = async (migrationDoc) => {
  const { firstName, lastName, email, cwUserId } = migrationDoc;
  const user = {
    firstName,
    lastName,
    email,
    cwUserId,
    isEmailVerified: true,
  };
  try {
    const {
      data: { user: newUser },
    } = await axios({
      method: 'POST',
      url: `${process.env.ADMIN_SERVICES_URL}/user/`,
      headers: { Authorization: `Bearer ${process.env.ACCESS_TOKEN}` },
      data: { user, settings: {}, countryCode: 'AU' },
    });

    return newUser;
  } catch (error) {
    console.log(error);
    throw new Error(error.message);
  }
};

export const getUser = async (migrationDoc) => {
  const { email, cwUserId } = migrationDoc;
  try {
    const {
      data: [user],
    } = await axios({
      method: 'GET',
      url: `${process.env.ADMIN_SERVICES_URL}/users/?search=${email}`,
      headers: { Authorization: `Bearer ${process.env.ACCESS_TOKEN}` },
    });
    if (!user) return null;
    const { id } = user;
    await axios({
      method: 'PUT',
      url: `${process.env.ADMIN_SERVICES_URL}/users/${id}`,
      headers: { Authorization: `Bearer ${process.env.ACCESS_TOKEN}` },
      data: { cwUserId, isEmailVerified: true },
    });
    return user;
  } catch (error) {
    throw new Error(error.message);
  }
};
