import { initMongo, setupLogsene, createLogger } from '@surfline/services-common';
import mongoose from 'mongoose';
import migration from './migration';

setupLogsene(process.env.LOGSENE_KEY);
const log = createLogger('migrate-into-wt-users');

Promise.all([initMongo(mongoose, process.env.MONGO_CONNECTION_STRING)])
  .then(async () => {
    await migration();
  })
  .catch((err) => {
    log.error({
      message: 'Fatal Error running user migration script',
      errorMessage: err.message,
      stack: err.stack,
    });
    process.exit(1);
  });
