import mongoose, { Schema } from 'mongoose';

const options = {
  collection: 'MigratedUsers',
};

const MigratedUsersSchema = new mongoose.Schema(
  {
    cwUserId: {
      type: Schema.Types.Number,
    },
    wtUserId: {
      type: Schema.Types.ObjectId,
    },
    firstName: {
      type: Schema.Types.String,
    },
    lastName: {
      type: Schema.Types.String,
    },
    email: {
      type: Schema.Types.String,
    },
    hasExistingWtAccount: {
      type: Schema.Types.Boolean,
      default: false,
    },
  },
  options,
);

export default mongoose.model('MigratedUsers', MigratedUsersSchema);
