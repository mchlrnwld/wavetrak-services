import cliProgress from 'cli-progress';
import MigrateUsersModel from './models/MigratedUsersModel';
import { createUser, getUser } from './external/user';

export default async () => {
  try {
    const total = await MigrateUsersModel.countDocuments({ wtUserId: { $exists: false } });
    const progressBar = new cliProgress.SingleBar(
      {
        format: `CW User Migration | {percentage}% || {value}/${total} User`,
        barCompleteChar: '\u2588',
        barIncompleteChar: '\u2591',
      },
      cliProgress.Presets.shades_classic,
    );
    progressBar.start(total, 0);

    await MigrateUsersModel.find({ wtUserId: { $exists: false } })
      .cursor()
      .eachAsync(async (doc) => {
        const existingUser = await getUser(doc);
        if (existingUser) {
          // eslint-disable-next-line no-param-reassign
          doc.hasExistingWtAccount = true;
        }
        const { _id: wtUserId } = existingUser || (await createUser(doc));

        // eslint-disable-next-line no-param-reassign
        doc.wtUserId = wtUserId;
        await doc.save();

        progressBar.increment();
      });
  } catch (error) {
    throw new Error(error.message);
  }
};
