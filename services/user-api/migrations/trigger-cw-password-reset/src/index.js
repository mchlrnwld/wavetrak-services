import { initMongo, setupLogsene, createLogger } from '@surfline/services-common';
import mongoose from 'mongoose';
import execute from './execute';

setupLogsene(process.env.LOGSENE_KEY);
const log = createLogger('trigger-cw-passord-reset');

Promise.all([initMongo(mongoose, process.env.MONGO_CONNECTION_STRING)])
  .then(async () => {
    await execute();
  })
  .catch((err) => {
    log.error({
      message: 'Fatal Error running user migration script',
      errorMessage: err.message,
      stack: err.stack,
    });
    process.exit(1);
  });
