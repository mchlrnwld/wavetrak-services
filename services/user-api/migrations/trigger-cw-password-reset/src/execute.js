import cliProgress from 'cli-progress';
import MigratedUserModel from './models/MigratedUserModel';

import { triggerPasswordReset } from './external/password-reset';

export default async () => {
  try {
    const query = {
      triggeredPasswordReset: { $exists: false },
      hasExistingWtAccount: false,
    };
    const total = await MigratedUserModel.countDocuments(query);
    const progressBar = new cliProgress.SingleBar({
      format: `CW Password Reset Trigger | {percentage}% || {value}/{total} Users`,
      barCompleteChar: '\u2588',
      barIncompleteChar: '\u2591',
    });
    progressBar.start(total, 0);

    await MigratedUserModel.find(query)
      .cursor()
      .eachAsync(async (doc) => {
        if (!doc.triggeredPasswordReset) {
          const { email } = doc;
          await triggerPasswordReset(email);

          // eslint-disable-next-line no-param-reassign
          doc.triggeredPasswordReset = true;
          await doc.save();
        }
        progressBar.increment();
      });
  } catch (error) {
    throw new Error(error.message);
  }
};
