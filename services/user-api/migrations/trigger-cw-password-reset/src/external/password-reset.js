import axios from 'axios';

export const triggerPasswordReset = async (email) => {
  try {
    const { data: message } = await axios({
      method: 'GET',
      url: `${process.env.SERVICES_URL}/password-reset?email=${email.toLowerCase()}&brand=sl`,
    });
    return message;
  } catch (error) {
    throw new Error(error.message);
  }
};
