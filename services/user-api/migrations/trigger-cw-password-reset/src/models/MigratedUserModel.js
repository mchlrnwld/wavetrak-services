import mongoose, { Schema } from 'mongoose';

const options = {
  collection: 'MigratedUsers',
};

const MigratedUsersSchema = new mongoose.Schema(
  {
    wtUserId: {
      type: Schema.Types.ObjectId,
    },
    triggeredPasswordReset: {
      type: Schema.Types.Boolean,
    },
    email: {
      type: Schema.Types.String,
    },
    hasExistingWtAccount: {
      type: Schema.Types.Boolean,
    },
  },
  options,
);

export default mongoose.model('MigratedUsers', MigratedUsersSchema);
