import mongoose, { Schema } from 'mongoose';

const options = {
  collection: 'UserInfo',
};

const UserInfoSchema = new mongoose.Schema(
  {
    cwUserId: {
      type: Schema.Types.Number,
    },
    password: {
      type: Schema.Types.String,
    },
    email: {
      type: Schema.Types.String,
    },
  },
  options,
);

export default mongoose.model('UserInfo', UserInfoSchema);
