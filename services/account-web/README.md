# surfline-web/account
A universal javascript application servicing the Surfline, FishTrack, and Buoyweather sign-in/create/upgrade/update/cancel account sections.

## Installation
```bash
nvm use 8
npm install
```

## Running Dev Server
```bash
export NODE_ENV=development
export NODE_PATH=./src
npm run dev
```
The development builds default to use `https://services.staging.surfline.com` services endpoints.

## Running Prod Server
```bash
npm run build
npm run start
```

## Testing Facebook Connect Locally
Because Facebook login requires a valid url listed within the Facebook app, to run and test Facebook login locally update the config development file to use `http://local.surfline.com:8100` in place of `localhost:8100`, and navigate to `http://local.surfline.com:8100/sign-in` to run the app in your browser instead of `localhost:8100`.

This requires the following entry to your local `/etc/hosts` file:
```127.0.0.1 local.surfline.com```

## Setting up ApplePay locally

Before you start, you need to:

1. Add a payment method to your browser. For example, you can save a card in Chrome, or add a card to your Wallet for Safari.

2. Serve Account application over HTTPS. This is a requirement both in development and in production. One way to get up and running is to use a service like ngrok(https://ngrok.com/) to set up an SSH tunnel. 
In development first tell ngrok what port Account app is listening on.
```
ngrok http 8100
```
Grab the `ngrokUrl` from the output and run the account app as below:
```
ngrokUlr=<Ngrok URL from above> npm run dev
```

3. Register your Ngrok domain with Apple Pay In Stripe. Make sure to copy the `apple-developer-merchantid-domain-association` file to `.well-known` in the `build` directory.

For sandbox, staging and prod environments the domain-association files are stored in  `static/certs`.

