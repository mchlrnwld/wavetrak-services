import path from 'path';
import configServer from '../src/configServer';
import autoprefixer from 'autoprefixer';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import WebpackAssetsManifest from 'webpack-assets-manifest';
import webpack from 'webpack';
import getConfig from '../src/config/server';
import cssnano from 'cssnano';

const brand = process.env.BRAND || 'sl';
const config = getConfig(brand);
const isDev = process.env.NODE_ENV !== 'production';
const PATHS = {
  clientBuild: path.resolve(__dirname, `../build/${brand}/dist/`),
  serverBuild: path.resolve(__dirname, '../build/server/'),
  buildDirectory: path.resolve(__dirname, '../build/'),
  serverEntry: path.resolve(__dirname, '../src/server.js'),
};

const sassData = [
  `$hostname: '${configServer.proxiedHost}';`,
  `$cdnHostname: '${config.cdnHost}static/';`,
  `$companyTheme: '${brand}';`,
].join('');

const postCssLoader = {
  loader: 'postcss-loader',
  options: {
    postcssOptions: {
      plugins: [autoprefixer, !isDev && cssnano].filter(Boolean),
    },
  },
};

/**
 * Load the entry point configuration for the client. This should include
 * any entry points for your app. You can create an object and define multiple bundles
 * with different entry points
 * @param {Array} entryPoints extra entry points
 */
export const loadClientEntryPoints = (entryPoints = []) => {
  const scssEntry = './src/theme/index.scss';

  return {
    entry: {
      account: ['./src/routes/Account/account.client.js', scssEntry, ...entryPoints],
    },
  };
};

export const loadServerEntrypoints = () => ({
  entry: {
    main: [PATHS.serverEntry],
  },
});

export const setClientOutput = ({ publicPath, filename, chunkFilename, pathinfo } = {}) => ({
  output: {
    path: PATHS.clientBuild,
    pathinfo,
    filename,
    chunkFilename,
    publicPath,
  },
});

export const setServerOutput = ({ publicPath } = {}) => ({
  output: {
    path: PATHS.serverBuild,
    filename: '[name].js',
    publicPath,
    libraryTarget: 'commonjs2',
  },
});

/**
 * Generate the loader for assets (jpg|jpeg|png|gif|eot|svg|ttf|woff|woff2|mp4)
 */
export const loadAssets = () => ({
  module: {
    rules: [
      {
        test: /\.(jpg|jpeg|png|gif|eot|svg|ttf|woff|woff2|mp4)$/,
        use: {
          loader: 'file-loader',
        },
      },
    ],
  },
});

/**
 * Generic babel-loader configuration to compile and load javascript bundle
 * @param {any} include path for any modules you want to explicitly include
 * @param {any} exclude path for any modules you want to explicitly exclude
 */
export const loadJavaScript = ({ include, exclude, options } = {}) => ({
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include,
        exclude,
        use: {
          loader: 'babel-loader',
          options,
        },
      },
    ],
  },
});

/**
 * The resolve configuration tells webpack where to look for imports and requires.
 * Generally this should be pointed into the src directory and node modules.
 * @param {Array<string>} modules An array of module paths to resolve to
 * @param {Array<string>} extensions An array of file extensions to resolve
 */
export const loadResolver = ({ modules, extensions } = {}) => ({
  resolve: {
    modules,
    extensions,
  },
});

/**
 * This configuration tells webpack where to look for the loaders that it uses.
 * This should always be node_modules unless you are doing something very specific.
 */
export const loadLoaderResolver = () => ({
  resolveLoader: {
    modules: ['./node_modules'],
  },
});

// /**
//  * Configuration for the dev server. Should only be used in dev mode.
//  */
// export const devServer = ({ hostName, portNumber, hot, publicPath, contentBase } = {}) => ({
//   devServer: {
//     hot,
//     contentBase,
//     publicPath,
//     host: hostName, // Defaults to `localhost`
//     port: portNumber, // Defaults to 8080
//   }
// });

/**
 * Configuration for loading css for the production bundle. This will output a CSS
 * bundle that is minified, as this is more optimized for production
 * @param {RegExp} include any specific paths to include
 * @param {RegExp} exclude any specific paths to exclude
 */
export const loadProdScss = ({ include, exclude } = {}) => ({
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        include,
        exclude,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              importLoaders: 3,
              sourceMap: true,
            },
          },
          postCssLoader,
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              additionalData: sassData,
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name]-[chunkhash].css',
      chunkFilename: '[id]-[chunkhash].css',
    }),
  ],
});

/**
 * Sass loader for dev mode which includes some extra configurations to help with development.
 * This will inject the styles into style tags in the head.
 * In production we don't want this however since it's slower to parse the CSS this way than to include
 * stylesheets. So we have a separate config for that.
 */
export const loadDevScss = () => ({
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 2,
              sourceMap: true,
            },
          },
          postCssLoader,
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                outputStyle: 'expanded',
              },
              sourceMap: true,
              additionalData: sassData,
            },
          },
        ],
      },
    ],
  },
});

/**
 * To run the server we may need to load some scss in that bundle as well.
 * This loader will handle any of those files we may encounter
 */
export const loadServerScss = () => ({
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        use: [
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              additionalData: sassData,
            },
          },
        ],
      },
    ],
  },
});

/**
 * Generate the source maps option for the webpack configuration
 * @param {string} sourceMapType (optional) a string determing what type of source maps you want
 */
export const generateSourceMaps = (sourceMapType = 'source-map') => ({
  devtool: sourceMapType,
});

/**
 * Part for setting environment variables using DefinePlugin
 * @param {object} obj - Environment variables object
 */
export const setEnvVariables = (obj) => {
  return {
    plugins: [new webpack.DefinePlugin(obj)],
  };
};

/**
 * Use the NoEmitOnErrorsPlugin to skip the emitting phase whenever there are errors while compiling.
 * This ensures that no assets are emitted that include errors.
 */
export const setNoErrors = () => {
  return new webpack.NoEmitOnErrorsPlugin();
};

/**
 * Set plugin to generate the asset manifest
 * @param {string} outputPath (optional) output path relative to directory of webpack config
 */
export const setWebpackAssetManifest = (outputPath = 'manifest.json') => ({
  plugins: [
    new WebpackAssetsManifest({
      output: outputPath,
      publicPath: true,
      writeToDisk: true,
      done(manifest) {
        console.log(`The manifest has been written to ${manifest.getOutputPath()}`);
      },
    }),
  ],
});

/**
 * Set plugin to enable hot module replacement (Development only)
 */
export const setHotModuleReplacement = () => ({
  plugins: [new webpack.HotModuleReplacementPlugin()],
});

/**
 * This plugin will cause the relative path of the module to be displayed when HMR is enabled.
 * Suggested for use in development.
 */
export const setNamedModulesPlugin = () => ({
  plugins: [new webpack.NamedModulesPlugin()],
});

/**
 * Set plugin to ignore a given file regex
 * @param {RegExp} regex file regex to ignore
 */
export const setIgnorePlugin = (regex) => ({
  plugins: [new webpack.IgnorePlugin(regex)],
});

/**
 * Limit the amount of chunk that webpack is able to split the bundle into
 * This is mainly used for the server side where bundle size does not matter
 * @param {number} maxChunks
 */
export const setChunkLimit = (maxChunks) => ({
  plugins: [new webpack.optimize.LimitChunkCountPlugin({ maxChunks })],
});

/**
 * This sets the plugin to pass options to all the loaders used.
 * In this case we want to use this to configure all the plugins
 * minify and output to the proper directory.
 */
export const setProductionLoaderOptions = () => ({
  plugins: [
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: true,
      options: {
        context: process.cwd(),
        output: { path: PATHS.buildDirectory },
      },
    }),
  ],
});
