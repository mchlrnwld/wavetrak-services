import webpackConfig from './webpack.config.js';

/**
 * Given an environment return the proper client and server configs
 * @param {string} env either 'development' or 'production'
 */
const getWebpackConfigs = (env = 'development', opts) => {
  const configurations = webpackConfig(env, opts);

  return {
    clientConfig: configurations.client,
    serverConfig: configurations.server,
  };
};

export default getWebpackConfigs;
