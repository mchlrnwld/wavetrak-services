import path from 'path';
import merge from 'webpack-merge';
import nodeExternals from 'webpack-node-externals';
import * as parts from './webpack.parts';
import getConfig from '../src/config/server';

const host = process.env.HOST || 'localhost';
const brand = process.env.BRAND || 'sl';
const port = parseInt(process.env.PORT, 10) + 1 || 8101;
const config = getConfig(brand);
console.log(`==> Building for brand: ${config.company}`);

const baseConfig = merge([
  {
    context: path.resolve(__dirname, '..'),
  },
  parts.loadResolver({
    modules: ['./src', './node_modules'],
    extensions: ['.json', '.js', '.jsx'],
  }),
  parts.loadLoaderResolver(),
  parts.setEnvVariables({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    'process.env.BRAND': JSON.stringify(process.env.BRAND),
  }),
  parts.setWebpackAssetManifest(),
]);

const devClientConfig = (opts = null) => {
  let publicPath = `http://${host}:${port}/${brand}/dist/`;
  let clientEntryPoint = `webpack-hot-middleware/client?path=http://${host}:${port}/__webpack_hmr`;
  if (opts) {
    const { ngrokUrl } = opts;
    publicPath = `${ngrokUrl}/${brand}/dist/`;
    clientEntryPoint = `webpack-hot-middleware/client?path=${ngrokUrl}/__webpack_hmr`;
  }
  return merge([
    {
      bail: true,
      watch: true,
      cache: true,
      target: 'web',
      resolve: {
        alias: {
          'react-dom': '@hot-loader/react-dom',
        },
      },
    },
    parts.setClientOutput({
      filename: '[name].js',
      chunkFilename: '[name].js',
      publicPath,
    }),
    parts.generateSourceMaps('inline-source-map'),
    parts.loadClientEntryPoints([clientEntryPoint]),
    parts.loadDevScss(),
    parts.loadAssets(),
    parts.loadJavaScript({
      exclude: /node_modules/,
      options: {
        cacheDirectory: true,
      },
    }),
    parts.setHotModuleReplacement(),
    parts.setNamedModulesPlugin(),
    parts.setIgnorePlugin(/webpack-stats\.json$/),
    parts.setIgnorePlugin(/leaflet/g),
    parts.setEnvVariables({
      __CLIENT__: true,
      __SERVER__: false,
      __DEVELOPMENT__: true,
      __DEVTOOLS__: true,
    }),
  ]);
};

const devServerConfig = merge([
  {
    bail: true,
    watch: true,
    cache: true,
    externals: nodeExternals(),
    target: 'node',
    node: {
      __dirname: false,
      __filename: false,
    },
  },
  parts.setServerOutput({ publicPath: `${config.cdnHost}` }),
  parts.loadServerEntrypoints(),
  parts.loadServerScss(),
  parts.loadJavaScript({ exclude: /node_modules/ }),
  parts.setChunkLimit(1),
  parts.setNoErrors(),
  parts.setEnvVariables({
    __CLIENT__: false,
    __SERVER__: true,
    __DEVELOPMENT__: true,
    __DEVTOOLS__: false,
  }),
]);

const prodClientConfig = merge([
  {
    target: 'web',
    performance: {
      hints: 'warning',
    },
    stats: 'verbose',
  },
  parts.setClientOutput({
    pathinfo: false,
    filename: '[name]-[chunkhash].js',
    chunkFilename: '[name]-[chunkhash].js',
    publicPath: `${config.cdnHost}${brand}/dist/`,
  }),
  parts.generateSourceMaps('source-map'),
  parts.loadClientEntryPoints(),
  parts.loadProdScss(),
  parts.loadAssets(),
  parts.loadJavaScript({ exclude: /node_modules/ }),
  parts.setProductionLoaderOptions(),
  parts.setNoErrors(),
  parts.setEnvVariables({
    __CLIENT__: true,
    __SERVER__: false,
    __DEVELOPMENT__: false,
    __DEVTOOLS__: false,
  }),
]);

const prodServerConfig = merge([
  {
    externals: nodeExternals(),
    target: 'node',
    node: {
      __dirname: false,
      __filename: false,
    },
  },
  parts.setServerOutput({ publicPath: `${config.cdnHost}${brand}/dist/` }),
  parts.loadServerEntrypoints(),
  parts.loadServerScss(),
  parts.loadJavaScript({ exclude: /node_modules/ }),
  parts.setChunkLimit(1),
  parts.setNoErrors(),
  parts.setEnvVariables({
    __CLIENT__: false,
    __SERVER__: true,
    __DEVELOPMENT__: false,
    __DEVTOOLS__: false,
  }),
]);

module.exports = (mode, opts) => {
  const modeConfig =
    mode === 'production'
      ? { client: prodClientConfig, server: prodServerConfig }
      : { client: devClientConfig(opts), server: devServerConfig };
  return {
    client: merge(baseConfig, modeConfig.client, { mode }),
    server: merge(baseConfig, modeConfig.server, { mode }),
  };
};
