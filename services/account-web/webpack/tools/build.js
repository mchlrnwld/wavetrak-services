import sh from 'shelljs';
import webpackCompiler from './webpackCompiler';
import getWebpackConfigs from '../';

const clientWebpackConfig = getWebpackConfigs('production').clientConfig;

const brand = process.env.BRAND;

const clean = () => {
  console.log(`Cleaning ${brand} build directory`);
  sh.rm('-rf', `./build/${brand}/dist/`);
};

export default () => {
  clean();

  const compileApp = () => webpackCompiler(clientWebpackConfig);

  compileApp().run();
};
