import once from 'lodash/fp/once';
import chokidar from 'chokidar';
import sh from 'shelljs';
import Express from 'express';
import nodemon from 'nodemon';
import devMiddleware from 'webpack-dev-middleware';
import hotMiddleware from 'webpack-hot-middleware';
import webpackCompiler from './webpackCompiler';
import getWebpackConfigs from '../';
import serverConfig from '../../src/configServer';

const brand = process.env.BRAND || 'sl';
const host = serverConfig.host || 'localhost';
const port = (serverConfig.exposePort + 1) || 3001;

const serverWebpackConfig = getWebpackConfigs().serverConfig;
const clientWebpackConfig = getWebpackConfigs().clientConfig;

const devServerOptions = {
  contentBase: 'http://' + host + ':' + port,
  quiet: true,
  noInfo: true,
  hot: true,
  inline: true,
  lazy: false,
  publicPath: `http://${host}:${port}/${brand}/dist/`,
  headers: {'Access-Control-Allow-Origin': '*'},
  stats: {colors: true}
};
const watcher = chokidar.watch(['./src']);

const startServer = once(() => {
  sh.cp('-R', 'static/', 'build/static');
  nodemon({ script: './build/server/main.js', watch: './src' })
    .on('start', () => {
      console.log('Development server started');
    })
    .on('restart', () => console.log('Restarted development server'))
    .on('quit', process.exit);
});

const serverCompiler = webpackCompiler(serverWebpackConfig, (stats) => {
  if (stats.hasErrors()) return;
  startServer();
});

const compileServer = () => serverCompiler.run(() => {
  console.log('Server compile started');
});

const clientCompiler = webpackCompiler(clientWebpackConfig, (stats) => {
  if (stats.hasErrors()) return;
  compileServer();
});

const startClientServer = () => {
  const app = new Express();
  app.use(devMiddleware(clientCompiler, devServerOptions));
  app.use(hotMiddleware(clientCompiler));
  app.listen(port, (err) => {
    if (err) {
      return console.error(err);
    }
    return console.log(`==> 🚧  Client server listening on ${port}`);
  });
};

const startNgrokServer = ngrokUrl => {
  try{
    console.log('Ngrok URL:', ngrokUrl);
    const clientWebpackConfig = getWebpackConfigs('development', { ngrokUrl }).clientConfig;
    const compileApp = () => webpackCompiler(clientWebpackConfig);
    compileApp().run(()=>{
      compileServer();
    });
  } catch(err){
    console.log('error in startNgrokServer', err);
  }
};

const runDev = () => {
  const ngrokUrl = process.argv[3] || false;
  process.on('SIGINT', () => {
    console.log('Exiting...');
    process.exit();
  });

  sh.rm('-rf', './build');

  watcher.on('ready', () => {
    watcher
      .on('add', compileServer)
      .on('addDir', compileServer)
      .on('change', compileServer)
      .on('unlink', compileServer)
      .on('unlinkDir', compileServer);
  });

  if(ngrokUrl){
    startNgrokServer(ngrokUrl);
  } else {
    startClientServer();
  }
};

export default runDev;
