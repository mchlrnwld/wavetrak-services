import sh from 'shelljs';
import webpackCompiler from './webpackCompiler';
import getWebpackConfigs from '../';

const serverWebpackConfig = getWebpackConfigs('production').serverConfig;

const clean = () => {
  console.log(`Cleaning server build directory`);
  sh.rm('-rf', `./build/server`);
};

export default () => {
  clean();

  const compileServer = () => webpackCompiler(serverWebpackConfig, () => { // build client
    sh.cp('./package.json', './build/server');
  });

  compileServer().run();
};
