var webpack = require('webpack');

module.exports = function (config) {
  config.set({
    browsers: ['PhantomJS'],
    // singleRun: !!process.env.CONTINUOUS_INTEGRATION,
    singleRun: true,
    frameworks: [ 'mocha', 'sinon-chai' ],
    preprocessors: {
      'tests.webpack.js': [ 'webpack', 'sourcemap' ],
      'src/**/*.js': ['coverage']
    },
    files: [
      './node_modules/phantomjs-polyfill/bind-polyfill.js',
      'tests.webpack.js'
    ],
    reporters: [ 'mocha', 'coverage' ],
    coverageReporter: {
      instrumenterOptions: {
        istanbul: { noCompact: true }
      },
      type : 'text-summary',
      dir : 'coverage/'
    },
    webpack: {
      devtool: 'inline-source-map',
      module: {
        loaders: [
          { test: /\.(jpe?g|png|gif|svg)$/, loader: 'url', query: {limit: 10240} },
          { test: /\.js$/, exclude: /node_modules/, loaders: ['babel-loader']},
          { test: /\.json$/, loader: 'json-loader' },
          { test: /\.less$/, loader: 'style!css!less' },
          { test: /\.scss$/, loader: 'style!css?modules&importLoaders=2&sourceMap&localIdentName=[local]___[hash:base64:5]!autoprefixer?browsers=last 2 version!sass?outputStyle=expanded&sourceMap' },
          { test: /sinon/, loader: 'imports?define=>false,require=>false' }
        ],
        postLoaders: [ {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'istanbul-instrumenter' }
        ]
      },
      sassLoader: {
        data: `$hostname: 'https://sandbox.surfline.com'; $cdnHostname: 'https://drcb7kk0emt05.cloudfront.net/'; $companyTheme: '${config.company}';`
      },
      resolve: {
        modulesDirectories: [
          'src',
          'node_modules'
        ],
        extensions: ['', '.json', '.js'],
        alias: {
          'sinon': 'sinon/pkg/sinon'
        }
      },
      plugins: [
        new webpack.IgnorePlugin(/\.json$/),
        new webpack.NoErrorsPlugin(),
        new webpack.DefinePlugin({
          __CLIENT__: true,
          __SERVER__: false,
          __DEVELOPMENT__: true,
          __CDN_HOST__: JSON.stringify(process.env.CDN_HOST || 'http://localhost:8100/'),
          __DEVTOOLS__: false  // <-------- DISABLE redux-devtools HERE
        })
      ],
      externals: {
        'cheerio': 'window',
        'react/lib/ExecutionEnvironment': true,
        'react/lib/ReactContext': true
      },
    },
    webpackServer: {
      noInfo: true
    }
  });
};
