provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "web-account/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  dns_name = "web-account.prod.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "web-account" {
  source = "../../"

  company     = "sl"
  application = "web-account"
  environment = "prod"

  default_vpc = "vpc-116fdb74"
  dns_name    = local.dns_name
  dns_zone_id = "Z3LLOZIY0ZZQDE"
  ecs_cluster = "sl-core-svc-prod"

  service_lb_rules = local.service_lb_rules
  service_td_count = 3

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-1-prod/9fed27702f8f890f/05e6f65280962f71"
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-1-prod/9fed27702f8f890f"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 200
  auto_scaling_min_size     = 2
  auto_scaling_max_size     = 5000
}
