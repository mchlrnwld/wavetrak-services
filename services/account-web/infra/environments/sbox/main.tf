provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "web-account/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  dns_name = "web-account.sandbox.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "web-account" {
  source = "../../"

  company     = "sl"
  application = "web-account"
  environment = "sandbox"

  default_vpc = "vpc-981887fd"
  dns_name    = local.dns_name
  dns_zone_id = "Z3DM6R3JR1RYXV"
  ecs_cluster = "sl-core-svc-sandbox"

  service_lb_rules = local.service_lb_rules
  service_td_count = 1

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57/a0f3c0f67e6842bf"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57"

  auto_scaling_enabled = false
}
