export const getCreditCards = (state) => {
  const { cards } = state.cards;
  if (cards) return cards;
  return null;
};

export const getDefaultSource = (state) => {
  const { defaultSource } = state.cards;
  if (defaultSource) return defaultSource;
  return null;
};

export const getSelectedCard = (state) => {
  const { selectedCard } = state.cards;
  if (selectedCard) return selectedCard;
  return null;
};
