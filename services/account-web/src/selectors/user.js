/**
 * @param {State} state
 * @returns {Object}
 */
export const getUser = (state) => {
  const user = {
    details: state?.user,
    entitlements: state?.entitlement?.entitlements,
    promotionEntitlements: state?.entitlement?.promotions,
    legacyEntitlements: state?.entitlement?.legacyEntitlements,
    settings: state?.userSettings,
  };
  return user;
};
