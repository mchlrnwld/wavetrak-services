import config from '../config';

export const getPremiumPlans = (state) => {
  const { plans } = state.plans;
  if (plans) return plans;
  return null;
};

export const getSelectedPlanId = (state) => {
  const { selectedPlanId } = state.plans;
  if (selectedPlanId) return selectedPlanId;
  return null;
};

export const getTrialPeriodDays = ({ plans }) => {
  const { selectedPlanId, plans: premiumPlans } = plans;
  if (!premiumPlans) return null;
  const promoTrialLength = config.funnelConfig && config.funnelConfig.trialLength;
  const { trialPeriodDays } = premiumPlans.find(({ id }) => id === selectedPlanId);
  const trialLength = (trialPeriodDays && promoTrialLength) ? promoTrialLength : trialPeriodDays;
  return trialLength;
};
