import config from '../config';

export const getPremiumStatus = (state) => {
  const { company } = config;
  const data = state.entitlements.entitlements;
  if (data && data.includes(`${company}_premium`)) return true;
  return false;
};

export const getTrialEligibility = (state) => {
  const { company, funnelConfig } = config;
  if (funnelConfig) {
    if (funnelConfig.trialLength === 0) return false;
  }
  const data = state.entitlements.promotions;
  if (data && data.includes(`${company}_free_trial`)) return true;
  return false;
};
