import config from '../../config';

const filterByBrand = (brand, itemToFilterBy, element) => {
  let result = false;
  if (element[itemToFilterBy]) result = element[itemToFilterBy].startsWith(brand);
  return result;
};

const filterByBrandWrapper = (itemToFilter, propToFilterBy) => {
  return itemToFilter.filter(filterByBrand.bind(this, config.company, propToFilterBy));
};

export default (subResponse) => {
  const brandPlans = filterByBrandWrapper(subResponse.availablePlans, 'id');
  const brandArchivedSubs = filterByBrandWrapper(subResponse.archivedSubscriptions, 'entitlement');
  const brandSubs = filterByBrandWrapper(subResponse.subscriptions, 'entitlement');

  const brandSubscriptionObj = {
    ...subResponse,
    availablePlans: brandPlans,
    archivedSubscriptions: brandArchivedSubs,
    subscriptions: brandSubs,
  };
  return brandSubscriptionObj;
};
