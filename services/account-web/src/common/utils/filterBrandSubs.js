import config from '../../config';

const filterSubsByBrand = (sub) => sub.entitlement.startsWith(config.company);

export default (subResponse) => {
  return subResponse.subscriptions.filter(filterSubsByBrand);
};
