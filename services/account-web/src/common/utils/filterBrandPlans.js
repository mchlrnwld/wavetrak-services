import config from '../../config';

const filterPlansByBrand = (plan) => {
  let result = false;
  if (plan.id) result = plan.id.startsWith(config.company);
  return result;
};

export default (subResponse) => {
  const brandPlans = subResponse.availablePlans.filter(filterPlansByBrand);
  const brandSubscriptionObj = {
    ...subResponse,
    availablePlans: brandPlans
  };

  return brandSubscriptionObj;
};
