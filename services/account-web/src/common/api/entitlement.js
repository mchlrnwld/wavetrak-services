import fetch from '../utils/fetch';

export const getEntitlement = (accessToken) =>
  fetch('entitlements', accessToken, { method: 'GET' });
