import fetch from '../utils/fetch';

export const postOnboarding = (accessToken, onboardingInfo) =>
  fetch(`onboarding`, accessToken, {
    method: 'POST',
    body: JSON.stringify(onboardingInfo)
  });
