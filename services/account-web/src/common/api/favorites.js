import fetch from '../utils/fetch';

export const getFavorites = accessToken =>
  fetch('kbyg/favorites', accessToken, { method: 'GET' });

export const deleteFavorite = (accessToken, favoriteId) =>
  fetch('favorites/', accessToken, {
    method: 'DELETE',
    body: JSON.stringify({
      type: 'spots',
      spotIds: [favoriteId]
    }),
  });

export const updateFavorites = (accessToken, favoriteIds) =>
  fetch('favorites/', accessToken, {
    method: 'PUT',
    body: JSON.stringify({
      type: 'spots',
      spotIds: [...favoriteIds]
    }),
  });
