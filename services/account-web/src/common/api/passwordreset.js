import fetch from '../utils/fetch';

export const validateHash = (hashBody) =>
  fetch('/password-reset/validate-hash', null, {
    method: 'POST',
    body: JSON.stringify(hashBody)
  });
