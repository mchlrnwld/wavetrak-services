import fetch from '../utils/fetch';

export const getLocation = (accessToken, ip) =>
  fetch(`geo-target/region?ipAddress=${ip}`, accessToken, { method: 'GET' });
