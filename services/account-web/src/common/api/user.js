import fetch from '../utils/fetch';

export const getUser = (accessToken) => fetch('user', accessToken, { method: 'GET' });

export const postUser = (isShortLived, user) =>
  fetch(`user?isShortLived=${isShortLived}`, null, {
    method: 'POST',
    body: JSON.stringify(user),
  });

export const updateUser = (accessToken, user) =>
  fetch('user', accessToken, { method: 'PUT', body: JSON.stringify(user) });

export const updateUserTimezone = (accessToken, timezone) =>
  fetch('user', accessToken, { method: 'PATCH', body: JSON.stringify({ timezone }) });

export const updatePassword = (accessToken, passwordData) =>
  fetch('user/ChangePassword', accessToken, { method: 'PUT', body: JSON.stringify(passwordData) });

export const validateEmail = (email) =>
  fetch(`user/ValidateEmail?email=${email}`, null, { method: 'GET' });

export const updateUserSettings = (accessToken, userSettings) =>
  fetch('user/Settings', accessToken, { method: 'PUT', body: JSON.stringify(userSettings) });

export const getUserSettings = (accessToken, countryCode = 'US') =>
  fetch(`user/Settings?geoCountryIso=${countryCode}`, accessToken, { method: 'GET' });

export const verifyEmail = (hash, brand) =>
  fetch('user/verify-email', null, {
    method: 'POST',
    body: JSON.stringify({ hash, brand }),
  });

export const resendEmailVerification = (accessToken, brand) =>
  fetch('/user/resend-email-verification', accessToken, {
    method: 'POST',
    body: JSON.stringify({ brand }),
  });
