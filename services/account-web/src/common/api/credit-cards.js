import fetch from '../utils/fetch';

export const deleteCard = (accessToken, cardId) =>
  fetch(`/credit-cards/${cardId}`, accessToken, {
    method: 'DELETE',
  });

export const addCard = (accessToken, source) =>
  fetch('/credit-cards', accessToken, {
    method: 'POST',
    body: JSON.stringify({ source }),
  });

export const updateCard = (accessToken, cardId, opts) =>
  fetch(`/credit-cards/${cardId}`, accessToken, {
    method: 'PUT',
    body: JSON.stringify(opts),
  });

export const setDefaultCard = (accessToken, cardId) =>
  fetch(`/credit-cards/default/${cardId}`, accessToken, {
    method: 'PUT',
    body: JSON.stringify(),
  });

export const getCards = (accessToken) =>
  fetch('/credit-cards', accessToken, {
    method: 'GET',
  });
