import fetch from '../utils/fetch';

export const getSubscription = (accessToken, coupon) => {
  let urlPath = '/subscriptions';
  if (coupon) {
    urlPath = `/subscriptions?coupon=${coupon}`;
  }
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const updateDefaultCard = (accessToken, details) =>
  fetch('/subscriptions/defaultcard', accessToken, {
    method: 'PUT',
    body: JSON.stringify(details),
  });

export const deleteCard = (accessToken, cardId) =>
  fetch(`/subscriptions/cards/${cardId}`, accessToken, {
    method: 'DELETE',
  });

export const addCard = (accessToken, source) =>
  fetch('/subscriptions/cards', accessToken, {
    method: 'POST',
    body: JSON.stringify(source),
  });

export const updateCard = (accessToken, cardId, opts) =>
  fetch(`subscriptions/cards/${cardId}`, accessToken, {
    method: 'PUT',
    body: JSON.stringify(opts),
  });

export const postSubscription = (subInfo, accessToken) =>
  fetch('/subscriptions', accessToken, {
    method: 'POST',
    body: JSON.stringify(subInfo),
  });

export const getOfferedPlans = (accessToken, brand) => {
  const urlPath = `/subscriptions/cancellation-offer?brand=${brand}`;
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const setDefaultCard = (accessToken, cardId) =>
  updateDefaultCard(accessToken, {
    defaultStripeSource: cardId,
  });

export const getPlans = (accessToken, brand, filter = 'all') => {
  const urlPath = `/subscriptions/plans?brand=${brand}&filter=${filter}`;
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const validateCoupon = (accessToken, brand, coupon) => {
  const urlPath = `/subscriptions/coupons?brand=${brand}&coupon=${coupon}`;
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const getCards = (accessToken) => {
  const urlPath = '/subscriptions/cards';
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const upgrade = (accessToken, subscriptionValues) =>
  fetch('/subscriptions/upgrade', accessToken, {
    method: 'POST',
    body: JSON.stringify({ ...subscriptionValues }),
  });

export const getWarnings = (accessToken) => {
  const urlPath = '/subscriptions/warnings';
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const getPaymentAlerts = (accessToken, brand) => {
  const urlPath = `/payment/alerts?product=${brand}`;
  return fetch(urlPath, accessToken, { method: 'GET' });
};

export const updateSubscription = (accessToken, opts) =>
  fetch('/subscriptions/billing', accessToken, {
    method: 'PUT',
    body: JSON.stringify(opts),
  });
