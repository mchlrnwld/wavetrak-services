import { renderFetch, getClientIp } from '@surfline/web-common';
import getConfig from '../../config/server';
import fetch from '../utils/fetch';

export const backplane = async (req, features = false) => {
  const config = getConfig(req.brand);
  const backplaneData = await renderFetch(config.backPlaneHost, getClientIp(req), {
    ...(req.cookies.access_token
      ? { accessToken: req.cookies.access_token }
      : { accessToken: null }),
    ...(req.cookies.ajs_anonymous_id
      ? {
          anonymousId: JSON.parse(req.cookies.ajs_anonymous_id),
        }
      : null),
    bundle: 'margins',
  });
  return features ? backplaneData.data.api.features : backplaneData;
};

export const backplaneClient = (accessToken) =>
  fetch(`backplane/render.json?accessToken=${accessToken}&bundle=margins`, { method: 'GET' });
