/* eslint-disable no-undef */
import config from '../../config';

/**
 * Get's a stripe token given CC details
 * @param details
 * @returns {Promise}
 */
export const getSource = (details) => {
  return new Promise((resolve, reject) => {
    if (!window) {
      return reject('Invariant violation: Stripe is not available on the server');
    } else if (!window.Stripe) {
      return reject('Invariant violation: Stripe is not available');
    }

    const { number, cvc, exp_month, exp_year, address_zip, name } = details;

    Stripe.setPublishableKey(config.stripe.publishableKey);

    // not error will be resolved
    Stripe.card.createToken({
      number, cvc, exp_month, exp_year, address_zip, name
    }, (status, response) => {
      resolve(response);
    });
  });
};
