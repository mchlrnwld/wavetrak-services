import React from 'react';

const StripeJS = () => (
  <script type="text/javascript" src="https://js.stripe.com/v3/" async></script>
);

export default StripeJS;
