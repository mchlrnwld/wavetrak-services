export const changeInput = (el, value) =>
  el.simulate('change', { target: { value } });
