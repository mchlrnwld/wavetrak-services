/* eslint no-unused-vars: 0 */
/* eslint prefer-template: 0 */
import React from 'react';
import ReactDOM from 'react-dom/server';
import getConfig from '../../config/server';
import getPromotionConfig from '../../server/utils/getPromotionConfig';
import getTranslations from '../../international/translations/server';
import Html from './promotion.html';

import * as entitlementApi from '../../common/api/entitlement';
import { backplane } from '../../common/api/backplane';
import * as subscriptionApi from '../../common/api/subscription';
import loadManifest from '../../helpers/loadManifest';

export default async (req, res, next) => {
  try {
    const accessToken = req.cookies.access_token;
    const config = getConfig(req.brand);
    const translations = getTranslations(req.brand);
    const assets = loadManifest(config, 'promotion');
    const { brand, promotion } = req;

    const plans = {
      plans: null,
      selectedPlanId: null,
    };

    const cards = {
      cards: null,
      defaultSource: null,
      showCards: true,
    };

    let entitlements;
    let isPremium;
    let trialEligible;

    if (accessToken) {
      entitlements = await entitlementApi.getEntitlement(accessToken);
      isPremium = entitlements.entitlements.includes(`${brand}_premium`);
      trialEligible = entitlements.promotions.includes(`${brand}_free_trial`);
    }

    const funnelConfig = await getPromotionConfig(promotion, trialEligible);
    config.funnelConfig = funnelConfig;
    const { plans: filter } = funnelConfig;
    const planFilter = filter || 'all';

    if (isPremium) return res.redirect('/account/subscription');

    if (accessToken && !isPremium) {
      const { plans: subscriptionPlans } = await subscriptionApi.getPlans(
        accessToken,
        brand,
        planFilter,
      );
      const { id: defaultPlanId } = subscriptionPlans.find(({ id }) => {
        if (planFilter !== 'all') return id.includes(planFilter);
        return id.includes('annual');
      });
      plans.plans = subscriptionPlans;
      plans.selectedPlanId = defaultPlanId;

      const { cards: customerCards, defaultSource } = await subscriptionApi.getCards(accessToken);
      cards.cards = customerCards;
      cards.defaultSource = defaultSource;
      cards.selectedCard = defaultSource;
      cards.showCards = !!customerCards;
    }

    const {
      data: {
        api: { features, geo },
      },
    } = await backplane(req, false);

    const initialState = {
      brand: req.brand,
      auth: { accessToken },
      entitlements,
      plans,
      cards,
      flags: features,
      geo,
    };

    return res.send(
      '<!doctype html>\n' +
        ReactDOM.renderToString(
          <Html assets={assets} config={config} state={initialState} translations={translations} />,
        ),
    );
  } catch (err) {
    console.log(err);
    return res.status(500).send('Internal Server Error');
  }
};
