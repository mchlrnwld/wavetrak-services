/* eslint no-unused-vars: 0 */
import Express from 'express';
import React from 'react';
import ReactDOM from 'react-dom/server';
import getConfig from '../../config/server';
import getTranslations from '../../international/translations/server';
import Html from './account.html';

import * as userApi from '../../common/api/user';
import * as entitlementApi from '../../common/api/entitlement';
import * as favoritesApi from '../../common/api/favorites';
import * as geoApi from '../../common/api/geo';
import { getPaymentAlerts } from '../../common/api/subscription';
import { getCards } from '../../common/api/credit-cards';
import getSubscriptionWrapper from '../utils/getSubscriptionWrapper';
import loadManifest from '../../helpers/loadManifest';

export default async (req, res) => {
  try {
    const accessToken = req.cookies.access_token;
    const config = getConfig(req.brand);
    const translations = getTranslations(req.brand);
    const assets = loadManifest(config, 'account');

    // TODO get a better error message and handling
    if (!accessToken) return res.redirect('/sign-in');

    const favoritesData = await favoritesApi.getFavorites(accessToken);

    const favorites = favoritesData.data ? favoritesData.data.favorites : [];

    let countryCode = 'US';
    try {
      const loc = await geoApi.getLocation(accessToken, req.ip);
      countryCode = loc.country.iso_code;
    } catch (err) {
      countryCode = 'US';
    }

    const { cards, defaultSource } = await getCards(accessToken);
    const alertResponse = await getPaymentAlerts(accessToken, config.company);

    const initialState = {
      user: req.user || (await userApi.getUser(accessToken)),
      userSettings: await userApi.getUserSettings(accessToken, countryCode),
      subscription: await getSubscriptionWrapper(accessToken, res),
      entitlement: await entitlementApi.getEntitlement(accessToken),
      favorites: { userFavorites: favorites },
      cards: {
        cards,
        defaultSource,
      },
      session: {
        accessToken,
      },
      auth: {
        user: {
          accessToken,
        },
      },
      paymentAlert: alertResponse,
      referrer: req.get('Referrer') || '',
    };
    // SA-3062: reset anonymous IDs
    const shouldResetAnonymousIdCookie = !req.cookies.sl_reset_anonymous_id_v3;
    return res.send(
      `<!doctype html>\n${ReactDOM.renderToString(
        <Html
          assets={assets}
          config={config}
          state={initialState}
          translations={translations}
          shouldResetAnonymousIdCookie={shouldResetAnonymousIdCookie}
        />,
      )}`,
    );
  } catch (err) {
    console.log(err);
    return res.status(500).send('Internal Server Error');
  }
};
