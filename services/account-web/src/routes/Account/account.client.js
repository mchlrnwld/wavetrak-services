/**
 * This is the entry point for the Accounts Client.
 * */
import createHistory from 'history/createBrowserHistory';
import {
  setupFeatureFramework,
  createSplitFilter,
  setWavetrakFeatureFrameworkCB,
} from '@surfline/web-common';

import { configureStore } from '../../redux/Account/store';
import getConfig from '../../config/server';
import App from './account';
import { mount } from '../client';
import * as treatments from '../../common/utils/treatments';
import { getUser } from '../../selectors/user';

const destination = document.getElementById('content');
const history = createHistory();
// eslint-disable-next-line no-underscore-dangle
const store = configureStore(window.__data, history);
const config = getConfig(store.getState().user.brand);

window.wavetrakStore = store;

setWavetrakFeatureFrameworkCB(() => {
  mount({
    store,
    history,
    destination,
    App,
  });
});

const mountApp = async () => {
  try {
    // TODO: Remove this once the full cut-over to split client side is finished
    // the setup will only have to happen in backplane
    await setupFeatureFramework({
      authorizationKey: config.splitio,
      user: getUser(store.getState()),
      splitFilters: createSplitFilter(treatments),
    });
  } catch (error) {
    window.newrelic?.noticeError(error);
  }
};

mountApp();
