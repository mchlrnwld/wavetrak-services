import PropTypes from 'prop-types';
import React from 'react';
import { snippet as segmentSnippet, resetAnonymousIdCookieSnippet } from '@surfline/web-common';
import BrowserUpdateMessage from '../../helpers/BrowserUpdateMessage';
import NewRelicBrowser from '../../common/components/NewRelicBrowser';
import ZendeskJS from '../../common/components/ZendeskJS';

/**
 * Wrapper component containing HTML metadata tags.
 * Used in server-side code only to wrap the string output of the
 * rendered route component.
 *
 * The only thing this component doesn't (and can't) include is the
 * HTML doctype declaration, which is added to the rendered output
 * by the server.js file.
 */
const Html = ({
  assets,
  favicon,
  browserUpdate,
  config,
  translations,
  state,
  shouldResetAnonymousIdCookie,
}) => {
  const { newRelic, segment } = config;
  return (
    <html lang="en-us">
      <head>
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        {shouldResetAnonymousIdCookie ? (
          <script
            type="text/javascript"
            dangerouslySetInnerHTML={{
              __html: `${resetAnonymousIdCookieSnippet(false)}`,
            }}
          />
        ) : null}

        <script src="https://js.stripe.com/v3/" />
        <NewRelicBrowser licenseKey={newRelic.licenseKey} applicationID={newRelic.applicationID} />
        <script
          type="text/javascript"
          dangerouslySetInnerHTML={{
            __html: `${segmentSnippet(segment, false)}`,
          }}
        />
        <link rel="shortcut icon" href={favicon} />
        <link
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400"
          rel="stylesheet"
          type="text/css"
        />
        {/* Font-Awesome and Bootstrap */}
        <script
          src="https://code.jquery.com/jquery-1.12.4.min.js"
          integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ"
          crossOrigin="anonymous"
        />
        <link
          rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"
        />
        <link
          rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
          integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu"
          crossOrigin="anonymous"
        />
        <script
          src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
          integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
          crossOrigin="anonymous"
        />
        {assets.css}
      </head>
      <body className="body__account">
        <BrowserUpdateMessage browserUpdate={browserUpdate} />
        <div id="content" />
        <script
          dangerouslySetInnerHTML={{ __html: `window.__data=${JSON.stringify(state)};` }}
          charSet="UTF-8"
        />
        <script
          dangerouslySetInnerHTML={{
            __html: `var wt={};wt.brand=${JSON.stringify(
              config.company,
            )};wt.funnel_config=${JSON.stringify(config)};wt.funnel_translations=${JSON.stringify(
              translations,
            )};`,
          }}
        />
        {assets.js}
        <ZendeskJS />
      </body>
    </html>
  );
};

Html.propTypes = {
  assets: PropTypes.shape({}).isRequired,
  store: PropTypes.shape({}).isRequired,
  favicon: PropTypes.string.isRequired,
  browserUpdate: PropTypes.bool.isRequired,
};

export default Html;
