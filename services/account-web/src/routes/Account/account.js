import React, { Component } from 'react';
import { Route, Redirect, Switch } from 'react-router';
import { hot } from 'react-hot-loader';
import { ErrorBoundary, NotFound } from '@surfline/quiver-react';
import {
  Account,
  Benefits,
  ChangePassword,
  EditProfile,
  EditSettings,
  Favorites,
  Notifications,
  Overview,
  Subscription,
} from 'containers';
import initializeAnonymousId from '../../utils/initializeAnonymousId';
import config from '../../config';

class App extends Component {
  constructor(props) {
    super(props);
    initializeAnonymousId('account');
  }

  render() {
    return (
      <ErrorBoundary>
        <Account>
          <Switch>
            <Route exact path="/account" render={() => <Redirect to="/account/overview" />} />
            <Route path="/account/overview" component={Overview} />
            <Route path="/account/benefits" component={Benefits} />
            <Route path="/account/change-password" component={ChangePassword} />
            {config.company === 'sl' ? (
              <Route path="/account/favorites" component={Favorites} />
            ) : null}
            {config.company === 'sl' ? (
              <Route path="/account/edit-settings" component={EditSettings} />
            ) : null}
            <Route path="/account/edit-profile" component={EditProfile} />
            <Route path="/account/notifications" component={Notifications} />
            <Route path="/account/subscription" component={Subscription} />
            <Route path="*" component={NotFound} status={404} />
          </Switch>
        </Account>
      </ErrorBoundary>
    );
  }
}

export default hot(module)(App);
