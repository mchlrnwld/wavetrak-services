import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

/**
 * @description Util for creating the Client for the React Apps within Account
 *
 * @param {{
 *   store: {},
 *   history: {},
 *   destination: HTMLElement,
 *   App: React.Component,
 * }} props
 * @param {Object} store Redux Store
 * @param {Object} history React Router history object
 * @param {object} App App component containing all client-side routes/components
 * @param {HTMLElement} destination Root DOM node to mount React to
 */
export const mount = ({
  // eslint-disable-line import/prefer-default-export
  store,
  history,
  destination,
  App,
}) => {
  ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>,
    destination,
  );
};
