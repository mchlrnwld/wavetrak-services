import * as subscriptionApi from '../../common/api/subscription';
import formatSubscriptionData from '../../utils/formatSubscriptionData';

const getSubscriptionWrapper = async (accessToken, res) => {
  try {
    const subscription = await subscriptionApi.getSubscription(accessToken);
    return formatSubscriptionData(subscription);
  } catch (err) {
    res.status(500).send('Internal Server Error');
    throw new Error('There was an error retrieving the subscription.');
  }
};

export default getSubscriptionWrapper;
