import { updatePassword } from '../../common/api/user';
import setLoginCookie from '../../utils/setLoginCookie';
import { updateSession } from './session';
import deviceInfo from '../../common/utils/deviceInfo';
import config from '../../config';

export const CHANGE_PASSWORD = 'redux/Account/CHANGE_PASSWORD';
export const CHANGE_PASSWORD_SUCCESS = 'redux/Account/CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_ERROR = 'redux/Account/CHANGE_PASSWORD_ERROR';
export const CHANGE_PASSWORD_RESET = 'redux/Account/CHANGE_PASSWORD_RESET';

export function changePassword(passwordData) {
  return async (dispatch, getState) => {
    dispatch({ type: CHANGE_PASSWORD });
    try {
      const { deviceId, deviceType } = deviceInfo();
      const data = {
        ...passwordData,
        client_id: config.client_id,
        device_id: deviceId,
        device_type: deviceType,
      };
      const result = await updatePassword(getState().session.accessToken, data);
      await setLoginCookie(result.accessToken, result.refreshToken, result.expiresIn);
      dispatch(updateSession(result.accessToken));
      dispatch({ type: CHANGE_PASSWORD_SUCCESS, result, message: 'Password Updated Successfully' });
    } catch (error) {
      dispatch({ type: CHANGE_PASSWORD_ERROR, error });
    }
  };
}

export function resetState() {
  return dispatch => dispatch({ type: CHANGE_PASSWORD_RESET });
}

const initialState = {
  passwordErrorMessage: null,
  passwordSuccessMessage: null,
  changePasswordSubmitting: false,
  changePasswordSuccess: false,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CHANGE_PASSWORD:
      return {
        ...state,
        changePasswordSubmitting: true
      };
    case CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        ...action.result,
        passwordSuccessMessage: action.message,
        changePasswordSubmitting: false,
        changePasswordSuccess: true,
      };
    case CHANGE_PASSWORD_ERROR:
      return {
        ...state,
        passwordErrorMessage: action.error.message,
        changePasswordSubmitting: false
      };
    case CHANGE_PASSWORD_RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
  return state;
}
