import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import reducer, {
  FAVORITE_MESSAGE_RESET,
  REMOVE_FAVORITE,
  REMOVE_FAVORITE_SUCCESS,
  REMOVE_FAVORITE_FAILURE,
  UPDATE_FAVORITES,
  UPDATE_FAVORITES_SUCCESS,
} from '../favorites';

const initialState = deepFreeze({
  userFavorites: [],
  updatingFavorites: false,
  updateFavoritesErrorMessage: null,
  updateFavoritesSuccessMessage: null,
  error: null,
});

describe('Reducers / Account / <Favorites>', () => {
  it('should return the state by default', () => {
    const state = deepFreeze({
      userFavorites: [],
      updatingFavorites: false,
      updateFavoritesErrorMessage: null,
      updateFavoritesSuccessMessage: null,
      error: null,
    });

    expect(reducer(state, undefined)).to.deep.equal(state);
  });

  it('should set updatingFavorites on UPDATE_FAVORITE', () => {
    const action = deepFreeze({ type: UPDATE_FAVORITES });

    expect(reducer(initialState, action)).to.deep.equal({
      userFavorites: [],
      updatingFavorites: true,
      updateFavoritesErrorMessage: null,
      updateFavoritesSuccessMessage: null,
      error: null,
    });
  });

  it('should update userFavorites on UPDATE_FAVORITES_SUCCESS', () => {
    const action = deepFreeze({
      type: UPDATE_FAVORITES_SUCCESS,
      updatedFavorites: ['1'],
      message: 'Your Favorites order has been updated'
    });

    expect(reducer(initialState, action)).to.deep.equal({
      userFavorites: ['1'],
      updatingFavorites: false,
      updateFavoritesErrorMessage: null,
      updateFavoritesSuccessMessage: 'Your Favorites order has been updated',
      error: null,
    });
  });

  it('should set updatingFavorites on REMOVE_FAVORITE', () => {
    const action = deepFreeze({ type: REMOVE_FAVORITE });

    expect(reducer(initialState, action)).to.deep.equal({
      userFavorites: [],
      updatingFavorites: true,
      updateFavoritesErrorMessage: null,
      updateFavoritesSuccessMessage: null,
      error: null,
    });
  });

  it('should update userFavorites on REMOVE_FAVORITE_SUCCESS', () => {
    const action = deepFreeze({
      type: REMOVE_FAVORITE_SUCCESS,
      updatedFavorites: ['1'],
      message: 'Your Favorites order has been updated',
    });

    expect(reducer(initialState, action)).to.deep.equal({
      userFavorites: ['1'],
      updatingFavorites: false,
      updateFavoritesErrorMessage: null,
      updateFavoritesSuccessMessage: 'Your Favorites order has been updated',
      error: null,
    });
  });

  it('should update userFavorites on REMOVE_FAVORITE_FAILURE', () => {
    const action = deepFreeze({
      type: REMOVE_FAVORITE_FAILURE,
      message: 'There was an error updating your favorites. Please try again.',
      error: { message: 'Fetch error'}
    });

    expect(reducer(initialState, action)).to.deep.equal({
      userFavorites: [],
      updatingFavorites: false,
      updateFavoritesErrorMessage: 'There was an error updating your favorites. Please try again.',
      updateFavoritesSuccessMessage: null,
      error: 'Fetch error',
    });
  });

  it('should reset on FAVORITE_MESSAGE_RESET', () => {
    const state = deepFreeze({
      userFavorites: [],
      updatingFavorites: false,
      updateFavoritesErrorMessage: 'error message',
      updateFavoritesSuccessMessage: 'success message',
      error: null,
    });
    const action = deepFreeze({ type: FAVORITE_MESSAGE_RESET });

    expect(reducer(state, action)).to.deep.equal(initialState);
  });
});
