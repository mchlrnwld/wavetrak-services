import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { expect } from 'chai';
import * as EditProfile from '../editProfile';
import sinon from 'sinon';
import sinonStubPromise from 'sinon-stub-promise';
import * as userAPI from '../../../common/api/user';
sinonStubPromise(sinon);

const middlewares = [ thunk ];
const mockStore = configureMockStore(middlewares);

describe('Actions (async) / Account / <EditProfile>', () => {
  let userProfileStub;
  let userProfilePromise;

  beforeEach(() => {
    userProfileStub = sinon.stub(userAPI, 'updateUser');
    userProfilePromise = userProfileStub.returnsPromise();
  });

  afterEach(() => {
    userProfileStub.restore();
  });

  const profile = {
    firstName: 'Turtle',
    lastName: 'Yoooo',
    email: 'turtle@yoooo.com',
    _id: '5744895de9bda2ba18a377ed'
  };

  it('should update user profile successfully', () => {
    const store = mockStore({ firstName: 'turtle', session: { accessToken: '1234' } });
    const expectedActions = [
      { type: EditProfile.UPDATE_PROFILE },
      { type: EditProfile.UPDATE_PROFILE_SUCCESS, result: { firstName: 'Turtle' }, message: 'Profile Updated Successfully!' }
    ];

    userProfilePromise.resolves({ firstName: 'Turtle' });

    return store.dispatch(EditProfile.updateProfile(profile))
      .then(() => { // return of async actions
        expect(store.getActions()).to.deep.equal(expectedActions);
      });
  });

  it('should error on updating user profile', () => {
    const store = mockStore({ firstName: 'turtle', session: { accessToken: '1234' } });
    const expectedActions = [
      { type: EditProfile.UPDATE_PROFILE },
      { type: EditProfile.UPDATE_PROFILE_ERROR, error: { statusCode: 400 } }
    ];

    userProfilePromise.rejects({ statusCode: 400 });

    return store.dispatch(EditProfile.updateProfile(profile))
      .then(() => { // return of async actions
        expect(store.getActions()).to.deep.equal(expectedActions);
      });
  });

  it('should reset state', () => {
    const store = mockStore({});
    const expectedActions = [
      { type: EditProfile.UPDATE_PROFILE_RESET }
    ];

    store.dispatch(EditProfile.resetState());

    expect(store.getActions()).to.deep.equal(expectedActions);
  });
});
