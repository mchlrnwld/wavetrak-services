import { expect } from 'chai';
import * as EditProfile from '../editProfile';

const reducer = EditProfile.default;

describe('Reducers / Account / <EditProfile>', () => {
  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).to.deep.equal({
      editProfileErrorMessage: null,
      editProfileSuccessMessage: null,
      editProfileSubmitting: false,
      editProfileSucceeded: false
    });
  });

  it('should be submitting on UPDATE_PROFILE', () => {
    expect(
      reducer([], {
        type: EditProfile.UPDATE_PROFILE
      })
    ).to.deep.equal({
      editProfileSubmitting: true
    });
  });

  it('should update successfully on UPDATE_PROFILE_SUCCESS', () => {
    const result = {
      firstName: 'Turtle',
      lastName: 'Yoooo',
      email: 'turtle@yoooo.com',
    };

    expect(
      reducer([], {
        type: EditProfile.UPDATE_PROFILE_SUCCESS,
        message: 'Profile Updated Successfully!',
        result: result
      })
    ).to.deep.equal({
      firstName: 'Turtle',
      lastName: 'Yoooo',
      email: 'turtle@yoooo.com',
      editProfileSuccessMessage: 'Profile Updated Successfully!',
      editProfileSubmitting: false,
      editProfileSucceeded: true
    });
  });

  it('should error successfully on UPDATE_PROFILE_ERROR', () => {
    expect(
      reducer([], {
        type: EditProfile.UPDATE_PROFILE_ERROR,
        error: {
          message: 'Something went wrong!'
        }
      })
    ).to.deep.equal({
      editProfileErrorMessage: 'Something went wrong!',
      editProfileSubmitting: false,
    });
  });
});
