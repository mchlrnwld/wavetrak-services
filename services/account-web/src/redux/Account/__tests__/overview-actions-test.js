import { expect } from 'chai';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import sinon from 'sinon';
import { renew } from '../overview';
import * as subscriptionAPI from '../../../common/api/subscription';
import { UPDATE_SUBSCRIPTION_FAILURE } from '../../Funnel/subscription';

describe('Actions (async) / Account / <Overview>', () => {
  let mockStore;

  before(() => {
    mockStore = configureMockStore([ thunk ]);
  });

  it('should handle postSubscription errors', async done => {
    const store = mockStore({
      isRenewing: false,
      isRenewingError: false,
      isRenewingSuccess: false,
      renewalErrorMessage: null,
      session: { accessToken: '56f9eaf946f566a11a5c6148' }
    });

    const fakePostSubscription = () => {
      throw new Error('Something blew up!');
    };

    const subscriptionAPIStub = sinon.stub(
      subscriptionAPI,
      'postSubscription',
      async (subsInfo, userId) => {
        subscriptionAPIStub.restore();
        return fakePostSubscription(subsInfo, userId);
      });

    const expectedActions = [
      { type: 'RENEW_SUBSCRIPTION' },
      { type: UPDATE_SUBSCRIPTION_FAILURE },
      { type: 'RENEW_SUBSCRIPTION_FAILURE', error: 'Something blew up!' }
    ];

    await store.dispatch(renew('sl_premium_monthly'));
    expect(store.getActions()).to.deep.equal(expectedActions);
    done();
  });
});
