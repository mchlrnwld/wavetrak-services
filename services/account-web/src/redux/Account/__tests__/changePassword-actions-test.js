import { expect } from 'chai';
import sinon from 'sinon';
import sinonStubPromise from 'sinon-stub-promise';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import cookie from 'react-cookie';
import * as userAPI from '../../../common/api/user';
import { UPDATE_SESSION } from '../session';
import * as changePassword from '../changePassword';

describe('Actions (async) / Account / <ChangePassword>', () => {
  let mockStore;
  let updatePasswordStub;
  let cookieSaveStub;

  before(() => {
    sinonStubPromise(sinon);
    mockStore = configureMockStore([thunk]);
  });

  beforeEach(() => {
    updatePasswordStub = sinon.stub(userAPI, 'updatePassword').returnsPromise();
    cookieSaveStub = sinon.stub(cookie, 'save');
  });

  afterEach(() => {
    updatePasswordStub.restore();
    cookieSaveStub.restore();
  });

  it('should change passwords', async done => {
    const store = mockStore({
      session: { accessToken: '1234' }
    });
    updatePasswordStub.resolves({
      accessToken: '2345',
      refreshToken: '3456',
      expiresIn: 10000
    });
    const passwordData = { password: 'password' };
    const expectedActions = [
      { type: changePassword.CHANGE_PASSWORD },
      { type: UPDATE_SESSION, accessToken: '2345' },
      {
        type: changePassword.CHANGE_PASSWORD_SUCCESS,
        result: {
          accessToken: '2345',
          refreshToken: '3456',
          expiresIn: 10000
        },
        message: 'Password Updated Successfully'
      }
    ];

    await store.dispatch(changePassword.changePassword(passwordData));

    expect(store.getActions()).to.deep.equal(expectedActions);
    expect(updatePasswordStub.calledOnce).to.be.true;
    expect(updatePasswordStub.calledWithExactly('1234', passwordData)).to.be.true;
    expect(cookieSaveStub.calledTwice).to.be.true;
    return done();
  });

  it('should handle updatePassword errors', async done => {
    const store = mockStore({
      session: { accessToken: '1234' }
    });
    const passwordData = { password: 'password' };
    const error = { message: 'Update password error' };
    updatePasswordStub.rejects(error);
    const expectedActions = [
      { type: changePassword.CHANGE_PASSWORD },
      { type: changePassword.CHANGE_PASSWORD_ERROR, error }
    ];

    await store.dispatch(changePassword.changePassword(passwordData));

    expect(store.getActions()).to.deep.equal(expectedActions);
    expect(updatePasswordStub.calledOnce).to.be.true;
    expect(updatePasswordStub.calledWithExactly('1234', passwordData)).to.be.true;
    expect(cookieSaveStub.called).to.be.false;
    return done();
  });

  it('should reset state', () => {
    const store = mockStore({});
    const expectedActions = [
      { type: changePassword.CHANGE_PASSWORD_RESET }
    ];

    store.dispatch(changePassword.resetState());

    expect(store.getActions()).to.deep.equal(expectedActions);
  });
});
