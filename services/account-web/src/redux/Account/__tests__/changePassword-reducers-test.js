import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import reducer, * as changePassword from '../changePassword';

describe('Reducers / Account / <ChangePassword>', () => {
  const initialState = deepFreeze({
    passwordErrorMessage: null,
    passwordSuccessMessage: null,
    changePasswordSubmitting: false,
    changePasswordSuccess: false,
  });

  it('should return state by default', () => {
    const state = deepFreeze({
      passwordErrorMessage: 'Password error message',
      passwordSuccessMessage: 'Password success message',
      changePasswordSubmitting: true,
      changePasswordSuccess: true
    });

    expect(reducer(state, undefined)).to.deep.equal(state);
  });

  it('should be submitting password change on CHANGE_PASSWORD', () => {
    const action = deepFreeze({ type: changePassword.CHANGE_PASSWORD });

    expect(reducer(initialState, action)).to.deep.equal({
      passwordErrorMessage: null,
      passwordSuccessMessage: null,
      changePasswordSubmitting: true,
      changePasswordSuccess: false
    });
  });

  it('should update state on CHANGE_PASSWORD_SUCCESS', () => {
    const state = deepFreeze({
      ...initialState,
      changePasswordSubmitting: true
    });
    const action = deepFreeze({
      type: changePassword.CHANGE_PASSWORD_SUCCESS,
      result: {
        accessToken: '1234',
        refreshToken: '2345'
      },
      message: 'Password updated successfully.'
    });

    expect(reducer(state, action)).to.deep.equal({
      passwordErrorMessage: null,
      passwordSuccessMessage: 'Password updated successfully.',
      changePasswordSubmitting: false,
      changePasswordSuccess: true,
      accessToken: '1234',
      refreshToken: '2345'
    });
  });

  it('should handle error on CHANGE_PASSWORD_ERROR', () => {
    const state = deepFreeze({
      ...initialState,
      changePasswordSubmitting: true
    });
    const action = deepFreeze({
      type: changePassword.CHANGE_PASSWORD_ERROR,
      error: {
        message: 'Password update errored.'
      }
    });

    expect(reducer(state, action)).to.deep.equal({
      passwordErrorMessage: 'Password update errored.',
      passwordSuccessMessage: null,
      changePasswordSubmitting: false,
      changePasswordSuccess: false
    });
  });

  it('should return initial state on CHANGE_PASSWORD_RESET', () => {
    const state = deepFreeze({
      passwordErrorMessage: 'Password update errored.',
      passwordSuccessMessage: 'Password updated successfully.',
      changePasswordSubmitting: true,
      changePasswordSuccess: true
    });
    const action = deepFreeze({
      type: changePassword.CHANGE_PASSWORD_RESET
    });

    expect(reducer(state, action)).to.deep.equal(initialState);
  });
});
