import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import reducer from '../overview';

const initialState = deepFreeze({
  isRenewing: false,
  isRenewingError: false,
  isRenewingSuccess: false,
  renewalErrorMessage: null
});

describe('Reducers / Account / <Overview>', () => {
  it('should return the state by default', () => {
    const state = deepFreeze({
      isRenewing: true,
      isRenewingError: true,
      isRenewingSuccess: true,
      renewalErrorMessage: 'test error message'
    });

    expect(reducer(state, undefined)).to.deep.equal(state);
  });

  it('should start renewing on RENEW_SUBSCRIPTION', () => {
    const action = deepFreeze({ type: 'RENEW_SUBSCRIPTION' });

    expect(reducer(initialState, action)).to.deep.equal({
      isRenewing: true,
      isRenewingError: false,
      isRenewingSuccess: false,
      renewalErrorMessage: null
    });
  });

  it('should renew on RENEW_SUBSCRIPTION_SUCCESS', () => {
    const state = deepFreeze({
      isRenewing: true,
      isRenewingError: false,
      isRenewingSuccess: false,
      renewalErrorMessage: null
    });
    const action = deepFreeze({ type: 'RENEW_SUBSCRIPTION_SUCCESS' });

    expect(reducer(state, action)).to.deep.equal({
      isRenewing: false,
      isRenewingError: false,
      isRenewingSuccess: true,
      renewalErrorMessage: null
    });
  });

  it('should error on RENEW_SUBSCRIPTION_FAILURE', () => {
    const state = deepFreeze({
      isRenewing: true,
      isRenewingError: false,
      isRenewingSuccess: false,
      renewalErrorMessage: null
    });
    const action = deepFreeze({
      type: 'RENEW_SUBSCRIPTION_FAILURE',
      error: 'Something blew up!'
    });

    expect(reducer(state, action)).to.deep.equal({
      isRenewing: false,
      isRenewingError: true,
      isRenewingSuccess: false,
      renewalErrorMessage: 'Something blew up!'
    });
  });

  it('should reset on RENEW_SUBSCRIPTION_RESET', () => {
    const state = deepFreeze({
      isRenewing: true,
      isRenewingError: true,
      isRenewingSuccess: true,
      renewalErrorMessage: 'Existing error message'
    });
    const action = deepFreeze({ type: 'RENEW_SUBSCRIPTION_RESET' });

    expect(reducer(state, action)).to.deep.equal(initialState);
  });
});
