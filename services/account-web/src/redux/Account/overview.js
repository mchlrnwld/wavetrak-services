import cookie from 'react-cookie';
import { updateSubscription } from '../../common/api/subscription';

export async function renew(subscription) {
  return async (dispatch) => {
    const accessToken = cookie.load('access_token');
    const { subscriptionId, planId } = subscription;
    dispatch({ type: 'RENEW_SUBSCRIPTION' });
    return dispatch(
      await updateSubscription(accessToken, { subscriptionId, planId, renew: true }),
    ).then(
      () => dispatch({ type: 'RENEW_SUBSCRIPTION_SUCCESS' }),
      (error) => dispatch({ type: 'RENEW_SUBSCRIPTION_FAILURE', error: error.message }),
    );
  };
}

const initialState = {
  isRenewing: false,
  isRenewingError: false,
  isRenewingSuccess: false,
  renewalErrorMessage: null,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case 'RENEW_SUBSCRIPTION':
      return {
        ...state,
        isRenewing: true,
      };
    case 'RENEW_SUBSCRIPTION_SUCCESS':
      return {
        ...state,
        isRenewing: false,
        isRenewingSuccess: true,
      };
    case 'RENEW_SUBSCRIPTION_FAILURE':
      return {
        ...state,
        isRenewing: false,
        isRenewingError: true,
        renewalErrorMessage: action.error,
      };
    case 'RENEW_SUBSCRIPTION_RESET': {
      return {
        ...state,
        ...initialState,
      };
    }
    default:
      return state;
  }
  return state;
}
