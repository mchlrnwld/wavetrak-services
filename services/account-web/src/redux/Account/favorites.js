import { deleteFavorite, updateFavorites } from '../../common/api/favorites';

export const FAVORITE_MESSAGE_RESET = 'redux/Account/FAVORITE_MESSAGE_RESET';
export const REMOVE_FAVORITE = 'redux/Account/REMOVE_FAVORITE';
export const REMOVE_FAVORITE_SUCCESS = 'redux/Account/REMOVE_FAVORITE_SUCCESS';
export const REMOVE_FAVORITE_FAILURE = 'redux/Account/REMOVE_FAVORITE_FAILURE';
export const UPDATE_FAVORITES = 'redux/Account/UPDATE_FAVORITES';
export const UPDATE_FAVORITES_SUCCESS = 'redux/Account/UPDATE_FAVORITES_SUCCESS';
export const UPDATE_FAVORITES_FAILURE = 'redux/Account/UPDATE_FAVORITES_FAILURE';

export function resetMessages() {
  return dispatch => dispatch({ type: FAVORITE_MESSAGE_RESET });
}

export function removeFavorite(favorite) {
  return async (dispatch, getState) => {
    dispatch({ type: REMOVE_FAVORITE});
    const { _id, name } = favorite;
    try {
      await deleteFavorite(getState().session.accessToken, _id);
      const updatedFavorites = getState().favorites.userFavorites.filter(fav => fav._id !== _id);
      dispatch({
        type: REMOVE_FAVORITE_SUCCESS,
        updatedFavorites,
        message: `${name} has been removed from your Favorites`
      });
    } catch (error) {
      dispatch({ type: REMOVE_FAVORITE_FAILURE, error, message: `There was an error updating your favorites. Please try again.` });
    }
  };
}

export function updateFavoritesOrder(updatedFavorites) {
  return async (dispatch, getState) => {
    dispatch({ type: UPDATE_FAVORITES});
    try {
      const updatedFavoriteIds = updatedFavorites.map(fav => fav._id);
      await updateFavorites(getState().session.accessToken, updatedFavoriteIds);
      dispatch({
        type: UPDATE_FAVORITES_SUCCESS,
        updatedFavorites,
        message: `Your Favorites order has been updated`
      });
    } catch (error) {
      dispatch({ type: UPDATE_FAVORITES_FAILURE, error, message: `There was an error updating your favorites. Please try again.` });
    }
  };
}

const initialState = {
  userFavorites: [],
  updatingFavorites: false,
  updateFavoritesErrorMessage: null,
  updateFavoritesSuccessMessage: null,
  error: null,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case FAVORITE_MESSAGE_RESET:
      return {
        ...state,
        updateFavoritesErrorMessage: null,
        updateFavoritesSuccessMessage: null,
        error: null,
      };
    case REMOVE_FAVORITE:
      return {
        ...state,
        ...action.result,
        updatingFavorites: true,
      };
    case REMOVE_FAVORITE_SUCCESS:
      return {
        ...state,
        userFavorites: action.updatedFavorites,
        updateFavoritesSuccessMessage: action.message,
        updatingFavorites: false,
      };
    case REMOVE_FAVORITE_FAILURE:
      return {
        ...state,
        updateFavoritesErrorMessage: action.message,
        updatingFavorites: false,
        error: action.error.message,
      };
    case UPDATE_FAVORITES:
      return {
        ...state,
        ...action.result,
        updatingFavorites: true,
      };
    case UPDATE_FAVORITES_SUCCESS:
      return {
        ...state,
        userFavorites: action.updatedFavorites,
        updatingFavorites: false,
        updateFavoritesSuccessMessage: action.message,
      };
    case UPDATE_FAVORITES_FAILURE:
      return {
        ...state,
        updatingFavorites: false,
        updateFavoritesErrorMessage: action.message,
        error: action.error.message,
      };
    default:
      return state;
  }
  return state;
}
