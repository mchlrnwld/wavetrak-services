import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

import { connectRouter, routerMiddleware } from 'connected-react-router';
import { reducer as formReducer } from 'redux-form';

import profileReducer from './editProfile';
import favoritesReducer from './favorites';
import settingsReducer from './editSettings';
import passwordReducer from './changePassword';
import sessionReducer from './session';
import overviewReducer from './overview';
import acctSubscriptionReducer from './subscription';
import cards from '../reducers/cards';
import verifyEmail from '../reducers/verifyEmail';
import warnings from '../reducers/warnings';
import paymentAlert from '../reducers/paymentAlert';
import subscriptionReducer from '../reducers/subscription';

import { stripeCCFormReducer as stripeCCForm } from '../Funnel/subscription';

export function configureStore(initialState, history) {
  const enhancer = composeWithDevTools(applyMiddleware(thunkMiddleware, routerMiddleware(history)));

  const reducer = combineReducers({
    router: connectRouter(history),
    form: formReducer,
    user: profileReducer,
    userSettings: settingsReducer,
    acctSubscription: acctSubscriptionReducer,
    subscription: subscriptionReducer,
    favorites: favoritesReducer,
    overview: overviewReducer,
    password: passwordReducer,
    subscriptionForm: formReducer.plugin({ stripeCCForm }),
    entitlement: (state = {}) => state,
    auth: (state = {}) => state,
    session: sessionReducer,
    referrer: (state = {}) => state,
    cards,
    verifyEmail,
    warnings,
    paymentAlert,
  });

  const store = createStore(reducer, initialState, enhancer);
  return store;
}
