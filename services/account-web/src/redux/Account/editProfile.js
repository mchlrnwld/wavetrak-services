import { updateUser } from '../../common/api/user';
import { resolveError } from '../../utils/errorMessages';

export const UPDATE_PROFILE = 'redux/Account/UPDATE_PROFILE';
export const UPDATE_PROFILE_SUCCESS = 'redux/Account/UPDATE_PROFILE_SUCCESS';
export const UPDATE_PROFILE_ERROR = 'redux/Account/UPDATE_PROFILE_ERROR';
export const UPDATE_PROFILE_RESET = 'redux/Account/UPDATE_PROFILE_RESET';

export function updateProfile(profile) {
  return async (dispatch, getState) => {
    dispatch({ type: UPDATE_PROFILE });
    try {
      const result = await updateUser(getState().session.accessToken, profile);
      dispatch({ type: UPDATE_PROFILE_SUCCESS, result, message: 'Profile Updated Successfully!' });
    } catch (error) {
      dispatch({ type: UPDATE_PROFILE_ERROR, error: resolveError(error) });
    }
  };
}

export function resetState() {
  return dispatch => dispatch({ type: UPDATE_PROFILE_RESET });
}

const initialState = {
  editProfileErrorMessage: null,
  editProfileSuccessMessage: null,
  editProfileSubmitting: false,
  editProfileSucceeded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case UPDATE_PROFILE:
      return {
        ...state,
        editProfileSubmitting: true
      };
    case UPDATE_PROFILE_SUCCESS:
      return {
        ...state,
        ...action.result,
        editProfileSuccessMessage: action.message,
        editProfileSubmitting: false,
        editProfileSucceeded: true,
      };
    case UPDATE_PROFILE_ERROR:
      return {
        ...state,
        editProfileErrorMessage: action.error.message,
        editProfileSubmitting: false,
      };
    case UPDATE_PROFILE_RESET:
      return {
        ...state,
        ...initialState,
      };
    default:
      return state;
  }
  return state;
}
