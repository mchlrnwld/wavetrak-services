/* eslint-disable no-param-reassign */
import { updateUserSettings } from '../../common/api/user';

export const UPDATE_SETTINGS = 'redux/Account/UPDATE_SETTINGS';
export const UPDATE_SETTINGS_SUCCESS = 'redux/Account/UPDATE_SETTINGS_SUCCESS';
export const UPDATE_SETTINGS_ERROR = 'redux/Account/UPDATE_SETTINGS_ERROR';
export const UPDATE_SETTINGS_RESET = 'redux/Account/UPDATE_SETTINGS_RESET';

export const unitLabels = {
  windSpeed: 'Wind Speed',
  swellHeight: 'Swell Height',
  surfHeight: 'Surf Height',
  tideHeight: 'Tide Height',
  temperature: 'Temperature',
};

export const feedLabels = {
  localized: 'Localized Feed',
};

export const dateLabels = {
  format: 'Date Format',
};

export function updateSettings(userSettings) {
  return async (dispatch, getState) => {
    dispatch({ type: UPDATE_SETTINGS });
    try {
      const result = await updateUserSettings(getState().session.accessToken, userSettings);
      dispatch({
        type: UPDATE_SETTINGS_SUCCESS,
        result,
        message: 'Settings Updated Successfully!',
      });
    } catch (error) {
      dispatch({ type: UPDATE_SETTINGS_ERROR, error });
    }
  };
}

export function updateUnit(unitName, unitValue, hawaiianFlag) {
  return async (dispatch, getState) => {
    dispatch({ type: UPDATE_SETTINGS });
    try {
      getState().userSettings.units[unitName] = unitValue;
      if (hawaiianFlag && hawaiianFlag !== 'on')
        getState().userSettings.units.swellHeight = getState().userSettings.units.surfHeight;
      const result = await updateUserSettings(
        getState().session.accessToken,
        getState().userSettings,
      );
      dispatch({
        type: UPDATE_SETTINGS_SUCCESS,
        result,
        message: `${unitLabels[unitName]} Unit Updated Successfully!`,
      });
    } catch (error) {
      dispatch({ type: UPDATE_SETTINGS_ERROR, error });
    }
  };
}

export function updateFeed(field, value) {
  return async (dispatch, getState) => {
    dispatch({ type: UPDATE_SETTINGS });
    try {
      getState().userSettings.feed[field] = value;
      const result = await updateUserSettings(
        getState().session.accessToken,
        getState().userSettings,
      );
      dispatch({
        type: UPDATE_SETTINGS_SUCCESS,
        result,
        message: `${feedLabels[field]} Updated Successfully!`,
      });
    } catch (error) {
      dispatch({ type: UPDATE_SETTINGS_ERROR, error });
    }
  };
}

export function updateModel(field, value) {
  return async (dispatch, getState) => {
    dispatch({ type: UPDATE_SETTINGS });
    try {
      getState().userSettings.model = value;
      const result = await updateUserSettings(
        getState().session.accessToken,
        getState().userSettings,
      );
      dispatch({
        type: UPDATE_SETTINGS_SUCCESS,
        result,
        message: `Model Updated Successfully!`,
      });
    } catch (error) {
      dispatch({ type: UPDATE_SETTINGS_ERROR, error });
    }
  };
}

export function updateDate(field, value) {
  return async (dispatch, getState) => {
    dispatch({ type: UPDATE_SETTINGS });
    try {
      getState().userSettings.date[field] = value;
      const result = await updateUserSettings(
        getState().session.accessToken,
        getState().userSettings,
      );
      dispatch({
        type: UPDATE_SETTINGS_SUCCESS,
        result,
        message: `${dateLabels[field]} Updated Successfully!`,
      });
    } catch (error) {
      dispatch({ type: UPDATE_SETTINGS_ERROR, error });
    }
  };
}

export function resetState() {
  return (dispatch) => dispatch({ type: UPDATE_SETTINGS_RESET });
}

const initialState = {
  editSettingsErrorMessage: null,
  editSettingsSuccessMessage: null,
  editSettingsSubmitting: false,
  editSettingsSucceeded: false,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case UPDATE_SETTINGS:
      return {
        ...state,
        editSettingsSubmitting: true,
      };
    case UPDATE_SETTINGS_SUCCESS:
      return {
        ...state,
        ...action.result,
        editSettingsSuccessMessage: action.message,
        editSettingsSubmitting: false,
        editSettingsSucceeded: true,
      };
    case UPDATE_SETTINGS_ERROR:
      return {
        ...state,
        editSettingsErrorMessage: action.error.message,
        editSettingsSubmitting: false,
      };
    case UPDATE_SETTINGS_RESET:
      return {
        ...state,
        ...initialState,
      };
    default:
      return state;
  }
}
