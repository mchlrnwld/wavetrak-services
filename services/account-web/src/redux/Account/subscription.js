import * as subscription from '../../common/api/subscription';

export const OPEN_CARD_DETAILS = 'redux/acctSubscription/OPEN_CARD_DETAILS';
export const TOGGLE_UPDATE_ACTIONS = 'redux/acctSubscription/TOGGLE_UPDATE_ACTIONS';
export const EXPAND_CARD = 'redux/acctSubscription/EXPAND_CARD';
export const UPDATE_CARD = 'redux/acctSubscription/UPDATE_CARD';
export const UPDATE_CARD_SUCCESS = 'redux/acctSubscription/UPDATE_CARD_SUCCESS';
export const UPDATE_CARD_FAIL = 'redux/acctSubscription/UPDATE_CARD_FAIL';


export function openCardDetail(index) {
  return (dispatch, getState) => {
    const currentOpenState = getState().acctSubscription.cardWithDetailOpen;
    const computedToggleState = (index === currentOpenState) ? -1 : index;
    dispatch({ type: TOGGLE_UPDATE_ACTIONS, result: false });
    dispatch({ type: OPEN_CARD_DETAILS, result: computedToggleState });
  };
}

export function toggleUpdateAction(isShowing) {
  return dispatch => dispatch({ type: TOGGLE_UPDATE_ACTIONS, result: !isShowing});
}

export function updateCard(cardDetails) {
  return async (dispatch, getState) => {
    dispatch({ type: UPDATE_CARD });
    const token = getState().auth.user.accessToken;
    try {
      const result = await subscription.updateCard(token, cardDetails.id, cardDetails);
      const cards = getState().subscription.cards;
      const cardAtIndex = cards.findIndex(card => card.id === result.card.id );
      cards[cardAtIndex].expMonth = result.card.expMonth;
      cards[cardAtIndex].expYear = result.card.expYear;
      return dispatch({ type: UPDATE_CARD_SUCCESS,
        result: cards, message: `The card ending in ${result.card.last4} was successfully updated.` });
    } catch (error) {
      return dispatch({ type: UPDATE_CARD_FAIL, result: 'There was a problem updating the card.' });
    }
  };
}

const initialState = {
  cardWithDetailOpen: -1,
  isUpdateShowing: false,
  cardUpdateSubmitting: false,
  cardUpdateSucceeded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case OPEN_CARD_DETAILS:
      return {
        ...state,
        cardWithDetailOpen: action.result,
        cardUpdateSubmitting: false,
        cardUpdateSucceeded: false,
        cardUpdateErrorMessage: null,
        cardUpdateSuccessMessage: null
      };
    case TOGGLE_UPDATE_ACTIONS:
      return {
        ...state,
        isUpdateShowing: action.result,
        cardUpdateSubmitting: false,
        cardUpdateSucceeded: false,
        cardUpdateErrorMessage: null
      };
    case UPDATE_CARD:
      return {
        ...state,
        cardUpdateSubmitting: true,
        cardUpdateErrorMessage: null,
      };
    case UPDATE_CARD_SUCCESS:
      return {
        ...state,
        subscription: {
          ...state,
          cards: action.result,
        },
        cardUpdateSubmitting: false,
        cardUpdateSucceeded: true,
        cardUpdateErrorMessage: null,
        cardWithDetailOpen: -1,
        cardUpdateSuccessMessage: action.message
      };
    case UPDATE_CARD_FAIL:
      return {
        ...state,
        cardUpdateErrorMessage: action.result,
        cardUpdateSubmitting: false,
        cardUpdateSucceeded: false,
      };
    default:
      return state;
  }
}
