export const UPDATE_SESSION = 'redux/Account/UPDATE_SESSION';

export function updateSession(accessToken) {
  return dispatch => dispatch({ type: UPDATE_SESSION, accessToken });
}

export default function reducer(state = null, action = {}) {
  switch (action.type) {
    case UPDATE_SESSION:
      return {
        ...state,
        accessToken: action.accessToken
      };
    default:
      return state;
  }
  return state;
}
