import cookie from 'react-cookie';
import config from '../../config';
import {
  verifyEmail as verifyEmailAPI,
  resendEmailVerification as resendEmailVerificationAPI
} from '../../common/api/user';
import setRedirectCookie from '../../utils/setRedirectCookie';

export const VERIFY_EMAIL = 'VERIFY_EMAIL';
export const VERIFY_EMAIL_SUCCESS = 'VERIFY_EMAIL_SUCCESS';
export const VERIFY_EMAIL_FAIL = 'VERIFY_EMAIL_FAIL';
export const RESEND_EMAIL_VERIFICATION = 'RESEND_EMAIL_VERIFICATION';
export const RESEND_EMAIL_VERIFICATION_SUCCESS = 'RESEND_EMAIL_VERIFICATION_SUCCESS';
export const RESEND_EMAIL_VERIFICATION_FAIL = 'RESEND_EMAIL_VERIFICATION_FAIL';

export const verifyEmail = hash => async (dispatch) => {
  dispatch({ type: VERIFY_EMAIL });
  try {
    const { company } = config;
    const { message } = await verifyEmailAPI(hash, company);
    return dispatch({ type: VERIFY_EMAIL_SUCCESS, message });
  } catch (error) {
    return dispatch({ type: VERIFY_EMAIL_FAIL, error: error.message });
  }
};

export const resendEmailVerification = () => async (dispatch) => {
  dispatch({ type: RESEND_EMAIL_VERIFICATION });
  try {
    const { company } = config;
    const accessToken = cookie.load('access_token');

    if (!accessToken) {
      setRedirectCookie(`${config.redirectUrl}/account/edit-profile`);
      return window.location.assign('/sign-in');
    }

    await resendEmailVerificationAPI(accessToken, company);
    return dispatch({ type: RESEND_EMAIL_VERIFICATION_SUCCESS });
  } catch (error) {
    return dispatch({ type: RESEND_EMAIL_VERIFICATION_FAIL, error: error.message });
  }
};
