import cookie from 'react-cookie';
import { getPlans } from '../../common/api/subscription';
import config from '../../config';

export const FETCH_PLANS = 'FETCH_PLANS';
export const FETCH_PLANS_SUCCESS = 'FETCH_PLANS_SUCCESS';
export const FETCH_PLANS_FAIL = 'FETCH_PLANS_FAIL';
export const TOGGLE_SELECTED_PLANID = 'TOGGLE_SELECTED_PLANID';
export const SET_DISCOUNTED_PLANS = 'SET_DISCOUNTED_PLANS';

export const fetchPlans = () => async (dispatch) => {
  dispatch({ type: FETCH_PLANS });
  try {
    const { company } = config;
    const accessToken = cookie.load('access_token');
    const { plans } = await getPlans(accessToken, company);
    const { id } = plans.find(plan => plan.id.includes('annual'));
    dispatch({ type: FETCH_PLANS_SUCCESS, plans, selectedPlanId: id });
  } catch (error) {
    dispatch({ type: FETCH_PLANS_FAIL, error });
  }
};

export const fetchFilteredPlans = () => async (dispatch) => {
  dispatch({ type: FETCH_PLANS });
  try {
    const { company: brand } = config;
    const accessToken = cookie.load('access_token');
    const { funnelConfig } = config;
    const filter = funnelConfig.plans || 'all';
    const { plans } = await getPlans(accessToken, brand, filter);
    const { id } = funnelConfig.plans === 'all' ? plans.find(plan => plan.id.includes('annual')) : plans[0];
    dispatch({ type: FETCH_PLANS_SUCCESS, plans, selectedPlanId: id });
  } catch (error) {
    dispatch({ type: FETCH_PLANS_FAIL, error });
  }
};

export const toggleSelectedPlanId = selectedPlanId => async (dispatch) => {
  dispatch({ type: TOGGLE_SELECTED_PLANID, selectedPlanId });
};


export const setDiscountedPlans = plans => async (dispatch) => {
  const { id } = plans.find(plan => plan.id.includes('annual'));
  dispatch({ type: 'SET_DISCOUNTED_PLANS', plans, selectedPlanId: id });
};
