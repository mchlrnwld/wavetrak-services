import cookie from 'react-cookie';
import {
  getCards,
  setDefaultCard,
  addCard as addNewCard,
  deleteCard as deleteCreditDetails,
  updateCard as updateCardDetails,
} from '../../common/api/credit-cards';
import { updateWarning } from './warnings';
import { months, getYears } from '../../utils/dateUtils';

export const FETCH_CARDS = 'FETCH_CARDS';
export const FETCH_CARDS_SUCCESS = 'FETCH_CARDS_SUCCESS';
export const FETCH_CARDS_FAIL = 'FETCH_CARDS_FAIL';
export const TOGGLE_SHOW_CARDS = 'TOGGLE_SHOW_CARDS';
export const TOGGLE_SHOW_NEW_CARD_FORM = 'TOGGLE_SHOW_NEW_CARD_FORM';
export const SET_SHOW_EDIT_CARD_FORM = 'SET_SHOW_EDIT_CARD_FORM';
export const SET_SELECTED_CARD_SOURCE = 'SET_SELECTED_CARD_SOURCE';
export const SET_DEFAULT_CARD_SOURCE = 'SET_DEFAULT_CARD_SOURCE';
export const SET_DEFAULT_CARD_SOURCE_SUCCESS = 'SET_DEFAULT_CARD_SOURCE_SUCCESS';
export const SET_DEFAULT_CARD_SOURCE_FAIL = 'SET_DEFAULT_CARD_SOURCE_FAIL';
export const ADD_CARD = 'ADD_CARD';
export const ADD_CARD_SUCCESS = 'ADD_CARD_SUCCESS';
export const ADD_CARD_FAIL = 'ADD_CARD_FAIL';
export const UPDATE_CARD = 'UPDATE_CARD';
export const UPDATE_CARD_SUCCESS = 'UPDATE_CARD_SUCCESS';
export const UPDATE_CARD_FAIL = 'UPDATE_CARD_FAIL';
export const DELETE_CARD = 'DELETE_CARD';
export const DELETE_CARD_SUCCESS = 'DELETE_CARD_SUCCESS';
export const DELETE_CARD_FAIL = 'DELETE_CARD_FAIL';

export const fetchCards = () => async (dispatch) => {
  try {
    const accessToken = cookie.load('access_token');
    const { cards, defaultSource } = await getCards(accessToken);
    const showCards = !!cards;
    return dispatch({
      type: FETCH_CARDS_SUCCESS,
      cards,
      defaultSource,
      showCards,
      addNewCard,
    });
  } catch (error) {
    const { message } = error;
    return dispatch({ type: FETCH_CARDS_FAIL, error: message });
  }
};

export const toggleShowCards = (showCards) => async (dispatch) => {
  dispatch({ type: TOGGLE_SHOW_CARDS, showCards: !showCards });
};

export const toggleShowNewCardForm = (showNewCardForm) => async (dispatch) => {
  dispatch({ type: TOGGLE_SHOW_NEW_CARD_FORM, showNewCardForm: !showNewCardForm });
};

export const setShowEditCardForm = (cardToEdit) => async (dispatch) => {
  dispatch({ type: SET_SHOW_EDIT_CARD_FORM, cardToEdit });
};

export const setSelectedCardSource = (cardId) => async (dispatch) => {
  dispatch({ type: SET_SELECTED_CARD_SOURCE, selectedCard: cardId });
};

export const setDefaultCardSource = (cardId) => async (dispatch) => {
  try {
    dispatch({ type: SET_DEFAULT_CARD_SOURCE });
    const accessToken = cookie.load('access_token');
    await setDefaultCard(accessToken, cardId);
    return dispatch({ type: SET_DEFAULT_CARD_SOURCE_SUCCESS, defaultSource: cardId });
  } catch (error) {
    const { message } = error;
    return dispatch({ type: SET_DEFAULT_CARD_SOURCE_FAIL, error: message });
  }
};

export const addCard = (source) => async (dispatch) => {
  try {
    dispatch({ type: ADD_CARD });
    const accessToken = cookie.load('access_token');
    const { id: token } = source;
    const { card } = await addNewCard(accessToken, token);
    return dispatch({ type: ADD_CARD_SUCCESS, card });
  } catch (error) {
    const { message } = error;
    return dispatch({ type: ADD_CARD_FAIL, error: message });
  }
};

export const updateCard = (cardDetails) => async (dispatch) => {
  const {
    id: cardId,
    opts: { exp_month: expMonth, exp_year: expYear },
  } = cardDetails;
  dispatch({ type: UPDATE_CARD });
  try {
    const monthValid = months.find((month) => month === expMonth);
    if (!monthValid) return dispatch({ type: UPDATE_CARD_FAIL, error: 'Invalid Month' });
    const yearValid = getYears().find((year) => year === expYear);
    if (!yearValid) return dispatch({ type: UPDATE_CARD_FAIL, error: 'Invalid Year' });
    const accessToken = cookie.load('access_token');
    const { card } = await updateCardDetails(accessToken, cardId, cardDetails);
    dispatch(updateWarning(cardId));
    return dispatch({ type: UPDATE_CARD_SUCCESS, card });
  } catch (error) {
    const { message } = error;
    return dispatch({ type: UPDATE_CARD_FAIL, error: message });
  }
};

export const deleteCard = (cardId) => async (dispatch) => {
  dispatch({ type: DELETE_CARD });
  try {
    const accessToken = cookie.load('access_token');
    await deleteCreditDetails(accessToken, cardId);
    return dispatch({ type: DELETE_CARD_SUCCESS, cardId });
  } catch (error) {
    const { message } = error;
    return dispatch({ type: DELETE_CARD_FAIL, error: message });
  }
};
