import { trackEvent } from '@surfline/web-common';
import { generateToken, authorise } from '../../common/api/auth';
import setLoginCookie from '../../utils/setLoginCookie';
import performRedirect from '../../utils/performRedirect';
import deviceInfo from '../../common/utils/deviceInfo';
import { setAccessToken } from './auth';
import { stopSubmitSpinner, startSubmitSpinner } from './interactions';

export const LOGIN = 'LOGIN';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';

export const login = (values) => async (dispatch) => {
  dispatch(startSubmitSpinner());
  try {
    const { email, password, staySignedIn, location, promotionId } = values;

    const { deviceId, deviceType } = deviceInfo();

    const valuesToEncode = {
      grant_type: 'password',
      username: email,
      password,
      device_id: deviceId,
      device_type: deviceType,
      forced: true,
    };

    const urlEncodedFormValues = [];
    const keysToEncode = Object.keys(valuesToEncode);

    keysToEncode.forEach((key) => {
      const encodedKey = encodeURIComponent(key);
      const encodedValue = encodeURIComponent(valuesToEncode[key]);
      urlEncodedFormValues.push(`${encodedKey}=${encodedValue}`);
    });

    const urlEncodedFormString = urlEncodedFormValues.join('&');

    const {
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: expiresIn,
    } = await generateToken(!staySignedIn, urlEncodedFormString);

    const { id } = await authorise(accessToken);

    if (window && window.analytics) {
      window.analytics.identify(id);
    }

    trackEvent('Signed In', {
      method: 'email',
      email,
      location,
      promotionId,
    });

    if (location !== 'register') {
      trackEvent('Completed Checkout Step', { step: 1, promotionId });
    }

    const maxAge = values.staySignedIn ? expiresIn : 1800;
    const refreshMaxAge = values.staySignedIn ? 31536000 : 1800;

    await setLoginCookie(accessToken, refreshToken, maxAge, refreshMaxAge);
    dispatch(setAccessToken(accessToken));

    if (location === 'register') performRedirect();
    dispatch(stopSubmitSpinner());
    dispatch({ type: LOGIN_SUCCESS });
  } catch (error) {
    const err = { _error: error.error_description };
    dispatch(stopSubmitSpinner());
    dispatch({ type: LOGIN_FAIL, error: error.error_description });
    throw err;
  }
};
