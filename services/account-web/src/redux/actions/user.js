import { trackEvent } from '@surfline/web-common';
import setLoginCookie from '../../utils/setLoginCookie';
import config from '../../config';
import { postUser } from '../../common/api/user';
import { setAccessToken } from './auth';
import { stopSubmitSpinner, startSubmitSpinner } from './interactions';
import performRedirect from '../../utils/performRedirect';
import deviceInfo from '../../common/utils/deviceInfo';

export const CREATE = 'CREATE';
export const CREATE_SUCCESS = 'CREATE_SUCCESS';
export const CREATE_FAIL = 'CREATE_FAIL';

export const register = (values) => async (dispatch) => {
  dispatch(startSubmitSpinner());
  try {
    const {
      first,
      last,
      email,
      password,
      receivePromotions,
      staySignedIn,
      location,
      promotionId,
    } = values;
    const { deviceId, deviceType } = deviceInfo();

    const valuesForRegistration = {
      firstName: first,
      lastName: last,
      email,
      password,
      brand: config.company,
      settings: {
        receivePromotions,
      },
      client_id: config.client_id,
      device_id: deviceId,
      device_type: deviceType,
    };

    const {
      token: { access_token: accessToken, refresh_token: refreshToken, expires_in: expiresIn },
      user: { _id: userId },
    } = await postUser(!staySignedIn, valuesForRegistration);

    if (window) window.analytics.identify(userId);
    if (location !== 'register') {
      trackEvent('Completed Checkout Step', { step: 1, promotionId });
    }

    const maxAge = values.staySignedIn ? expiresIn : 1800;
    const refreshMaxAge = values.staySignedIn ? 31536000 : 1800;

    await setLoginCookie(accessToken, refreshToken, maxAge, refreshMaxAge);
    dispatch(setAccessToken(accessToken));

    if (location === 'register') {
      if (config.company === 'sl') return window.location.assign('/onboarding');
      performRedirect();
    }
    dispatch(stopSubmitSpinner());
    return dispatch({ type: CREATE_SUCCESS });
  } catch (error) {
    const errorDescription =
      error.status === 400
        ? 'Invalid email. Already in use.'
        : 'Sorry, we could not complete this request';
    const err = { _error: errorDescription };
    dispatch({ type: CREATE_FAIL, error: errorDescription });
    dispatch(stopSubmitSpinner());
    throw err;
  }
};

export default register;
