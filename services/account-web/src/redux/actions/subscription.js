import cookie from 'react-cookie';
import { trackEvent } from '@surfline/web-common';
import {
  stopSubmitSpinner,
  startSubmitSpinner,
  redirectToOnboarding,
  redirectToMultiCam,
} from './interactions';
import { upgrade, updateSubscription } from '../../common/api/subscription';
import { showDuplicateCard, clearDuplicateCard } from './duplicateCard';
import config from '../../config';
import en from '../../international/translations/en';

export const SUBMIT_SUBSCRIPTION = 'SUBMIT_SUBSCRIPTION';
export const SUBMIT_SUBSCRIPTION_SUCCESS = 'SUBMIT_SUBSCRIPTION_SUCCESS';
export const SUBMIT_SUBSCRIPTION_FAIL = 'SUBMIT_SUBSCRIPTION_FAIL';
export const RENEW_SUBSCRIPTION = 'RENEW_SUBSCRIPTION';
export const RENEW_SUBSCRIPTION_SUCCESS = 'RENEW_SUBSCRIPTION_SUCCESS';
export const RENEW_SUBSCRIPTION_FAILURE = 'RENEW_SUBSCRIPTION_FAILURE';
export const CHANGE_SUBSCRIPTION_PLAN = 'CHANGE_SUBSCRIPTION_PLAN';
export const CHANGE_SUBSCRIPTION_PLAN_SUCCESS = 'CHANGE_SUBSCRIPTION_PLAN_SUCCESS';
export const CHANGE_SUBSCRIPTION_PLAN_FAILURE = 'CHANGE_SUBSCRIPTION_PLAN_FAILURE';

export const submitSubscription = (values) => async (dispatch) => {
  const {
    plan,
    source,
    selectedCard,
    defaultCardId,
    accessToken,
    coupon,
    trialEligible,
    trialPeriodDays,
    promotionId,
    promoCodesEnabled,
    promoCode,
    digitalWallet,
    shouldRedirectToOnboarding,
  } = values;
  dispatch(startSubmitSpinner());

  // we must have either new CC entered OR existing default CC for submission
  const paymentSource = source || selectedCard;
  if (!plan || !paymentSource) {
    dispatch(stopSubmitSpinner());
    return dispatch({
      type: SUBMIT_SUBSCRIPTION_FAIL,
      message: 'A valid credit card is required.',
    });
  }
  if (promoCodesEnabled && !promoCode) {
    dispatch(stopSubmitSpinner());
    return dispatch({ type: SUBMIT_SUBSCRIPTION_FAIL, message: 'A valid promo code is required.' });
  }

  const freeTrial = trialEligible && !!trialPeriodDays;

  try {
    dispatch({ type: SUBMIT_SUBSCRIPTION });
    const subscriptionApiParams = {
      plan,
      coupon,
      freeTrial,
      source: source ? source.id : null,
      cardId: selectedCard !== defaultCardId ? selectedCard : defaultCardId,
      promotionId,
      brand: config.company,
      promoCode,
    };
    // TODO this API should only return the newly created subscription.
    const response = await upgrade(accessToken, subscriptionApiParams);
    const {
      subscriptionId,
      effectivePrice,
      plan: { currency, name },
    } = response.subscriptions.find(({ planId }) => planId === plan);

    trackEvent('Completed Checkout Step', { step: 2, promotionId });
    trackEvent('Completed Order', {
      orderId: subscriptionId,
      total: 0,
      coupon,
      currency,
      products: [
        {
          product_id: plan,
          sku: plan,
          name,
          price: effectivePrice / 100,
          quantity: 1,
          category: 'Subscription',
        },
      ],
      promotionId,
      paymentSource: digitalWallet || 'credit card',
    });

    if (
      config.company === 'sl' &&
      shouldRedirectToOnboarding &&
      !promotionId &&
      !promoCodesEnabled
    ) {
      return dispatch(redirectToOnboarding());
    }

    // For regular upgrade funnel users that have been onboarded, send them to the multi-cam page.
    if (config.company === 'sl' && !promotionId && !promoCodesEnabled) {
      return dispatch(redirectToMultiCam());
    }

    dispatch({ type: SUBMIT_SUBSCRIPTION_SUCCESS });
    return dispatch(stopSubmitSpinner());
  } catch (error) {
    dispatch(stopSubmitSpinner());
    const { code, message } = error;
    if (code === 1001) return dispatch(showDuplicateCard({ message, source, plan }));
    dispatch(clearDuplicateCard());
    if (code === 1002) {
      const errMessage = en.promotionFeatures.promoCodeError;
      return dispatch({ type: SUBMIT_SUBSCRIPTION_FAIL, message: errMessage });
    }

    return dispatch({ type: SUBMIT_SUBSCRIPTION_FAIL, message });
  }
};

export const renew = (subscription) => async (dispatch) => {
  const { subscriptionId, planId } = subscription;
  dispatch({ type: 'RENEW_SUBSCRIPTION' });
  try {
    const accessToken = cookie.load('access_token');
    await updateSubscription(accessToken, { subscriptionId, planId, resume: true });
    return dispatch({ type: 'RENEW_SUBSCRIPTION_SUCCESS' });
  } catch (error) {
    return dispatch({ type: 'RENEW_SUBSCRIPTION_FAILURE', message: error.message });
  }
};

export const changeSubscriptionPlan = (subscription, newPlanId) => async (dispatch) => {
  const { subscriptionId } = subscription;
  dispatch({ type: 'CHANGE_SUBSCRIPTION_PLAN' });
  try {
    const accessToken = cookie.load('access_token');
    await updateSubscription(accessToken, { subscriptionId, planId: newPlanId, resume: false });
    dispatch({ type: 'CHANGE_SUBSCRIPTION_PLAN_SUCCESS' });
    return window.location.reload();
  } catch (error) {
    return dispatch({ type: 'CHANGE_SUBSCRIPTION_PLAN_FAILURE', mesage: error.message });
  }
};
