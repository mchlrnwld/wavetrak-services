export const SET_ACCESS_TOKEN = 'SET_ACCESS_TOKEN';

export const setAccessToken = accessToken => async (dispatch) => {
  return dispatch({ type: SET_ACCESS_TOKEN, accessToken });
};
