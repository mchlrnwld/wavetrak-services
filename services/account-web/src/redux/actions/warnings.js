
export const UPDATE_WARNING = 'UPDATE_WARNING';

export const updateWarning = cardId => async (dispatch) => {
  dispatch({ type: UPDATE_WARNING, cardId });
};
