export const TOKEN_LIMIT_FAIL = 'TOKEN_LIMIT_FAIL';
export const TOKEN_LIMIT_RESET = 'TOKEN_LIMIT_RESET';

/**
 * A wrapper for catching errors within the login flows related to the token limit.
 * The wrapped action should throw a new Error object with the original error code
 * and description but with a fallback action to dispatch.
 *
 * Example:
 * Error: {
 *  code: 400,
 *  description: 'Some desc',
 *  dispatch: { type: SOME_TYPE, err: originalErr }
 * }
 */
export const tokenLimitWrapper = async (action, dispatch) => {
  try {
    const result = await action();
    return result;
  } catch (err) {
    if (
      err._error_code === 403 &&
      err.description === 'User have exceeded the token limit'
    ) {
      dispatch({ type: TOKEN_LIMIT_FAIL, err });
    } else {
      // Dispatch the fallback
      await dispatch(err.dispatch);
      throw err;
    }
  }
};

export const loadContinueForm = authError => dispatch =>
  dispatch({ type: TOKEN_LIMIT_FAIL, authError });

export const tokenLimitReset = () => dispatch => dispatch({ type: TOKEN_LIMIT_RESET });
