/* eslint no-unused-vars: 0 */
import { validateHash } from '../../common/api/passwordreset';
import config from '../../config';

export const REQUEST = 'REQUEST';
export const REQUEST_SUCCESS = 'REQUEST_SUCCESS';
export const REQUEST_FAIL = 'REQUEST_FAIL';
export const GETUSERID = 'GETUSERID';
export const GETUSERID_SUCCESS = 'GETUSERID_SUCCESS';
export const GETUSERID_FAIL = 'GETUSERID_FAIL';
export const REDIRECT = 'REDIRECT';
export const REDIRECT_SUCCESS = 'REDIRECT_SUCCESS';
export const REDIRECT_FAIL = 'REDIRECT_FAIL';
export const SETPASSWORD = 'SETPASSWORD';
export const SETPASSWORD_SUCESS = 'SETPASSWORD_SUCESS';
export const SETPASSWORD_FAIL = 'SETPASSWORD_FAIL';
export const CLEAR = 'CLEAR';
export const CLEAR_SUCCESS = 'CLEAR_SUCCESS';
export const CLEAR_FAIL = 'CLEAR_FAIL';

export function requestReset(email) {
  return async (dispatch) => {
    dispatch({ type: REQUEST });
    try {
      const result = {
        email,
      };
      return dispatch({ type: REQUEST_SUCCESS, result });
    } catch (err) {
      return dispatch({ type: REQUEST_FAIL, err });
    }
  };
}

export function enterCode(userId, resetCode) {
  return async (dispatch) => {
    dispatch({ type: GETUSERID });
    try {
      const result = { userId, resetCode };
      return dispatch({ type: GETUSERID_SUCCESS, result });
    } catch (err) {
      return dispatch({ type: GETUSERID_FAIL, err });
    }
  };
}

export function setPassword() {
  return async (dispatch) => {
    dispatch({ type: SETPASSWORD });
    try {
      return dispatch({ type: SETPASSWORD_SUCESS });
    } catch (err) {
      return dispatch({ type: SETPASSWORD_FAIL, err });
    }
  };
}

export function isValidated(globalState) {
  return globalState.forgotPassword.isValidated;
}

export function validate(location) {
  return async (dispatch, getState) => {
    dispatch({ type: GETUSERID });
    try {
      const queryString = getState().router.location.search;
      const matches = queryString.match(/hash=([^&]*)/);
      const hash = matches !== null ? matches[1] : '1234';
      const hashBody = { hash, brand: config.company };
      const response = await validateHash(hashBody);
      const result = { userId: response.userId, resetCode: response.resetCode };
      return dispatch({ type: GETUSERID_SUCCESS, result });
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err);
      return dispatch({ type: GETUSERID_FAIL, err });
    }
  };
}

export function clearUser() {
  return (dispatch) => {
    dispatch({ type: CLEAR });
    try {
      return dispatch({ type: CLEAR_SUCCESS, result: null });
    } catch (err) {
      return dispatch({ type: CLEAR_FAIL, err });
    }
  };
}
