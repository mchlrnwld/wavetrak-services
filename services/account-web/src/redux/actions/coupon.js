import cookie from 'react-cookie';
import { validateCoupon } from '../../common/api/subscription';
import config from '../../config';
import { setDiscountedPlans } from './plans';

export const VALIDATE_COUPON = 'VALIDATE_COUPON';
export const VALIDATE_COUPON_SUCCESS = 'VALIDATE_COUPON_SUCCESS';
export const VALIDATE_COUPON_FAIL = 'VALIDATE_COUPON_FAIL';
export const OPEN_COUPON_FORM = 'OPEN_COUPON_FORM';
export const CLOSE_COUPON_FORM = 'CLOSE_COUPON_FORM';
export const TOGGLE_COUPON_FORM = 'TOGGLE_COUPON_FORM';

export const fetchCoupon = coupon => async (dispatch) => {
  dispatch({ type: VALIDATE_COUPON });
  try {
    if (!coupon.length) {
      return dispatch({
        type: VALIDATE_COUPON_FAIL,
        error: {
          message: 'Coupon cannont be blank',
        }
      });
    }
    const { company } = config;
    const accessToken = cookie.load('access_token');
    const { plans } = await validateCoupon(accessToken, company, coupon);
    dispatch(setDiscountedPlans(plans));
    return dispatch({ type: VALIDATE_COUPON_SUCCESS, coupon });
  } catch (error) {
    return dispatch({ type: VALIDATE_COUPON_FAIL, error });
  }
};

export const toggleCouponForm = () => async dispatch => (
  dispatch({ type: TOGGLE_COUPON_FORM })
);
