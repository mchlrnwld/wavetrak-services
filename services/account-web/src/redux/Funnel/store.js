import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { reducer as formReducer } from 'redux-form';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { connectRouter, routerMiddleware } from 'connected-react-router';

import auth from './auth';
import forgotPassword from './forgotPassword';
import onboarding from './onboarding';
import subscription, { stripeCCFormReducer as stripeCCForm } from './subscription';
import geo from './geo';
import user from '../reducers/user';
import login from '../reducers/login';
import tokenLimit from '../reducers/tokenLimit';
import facebook from '../reducers/facebook';

export function configureStore(initialState, history) {
  const enhancer = composeWithDevTools(applyMiddleware(thunkMiddleware, routerMiddleware(history)));

  const reducer = combineReducers({
    router: connectRouter(history),
    auth,
    user,
    login,
    tokenLimit,
    facebook,
    forgotPassword,
    onboarding,
    subscription,
    geo,
    form: formReducer,
    subscriptionForm: formReducer.plugin({ stripeCCForm }),
  });

  const store = createStore(reducer, initialState, enhancer);

  return store;
}
