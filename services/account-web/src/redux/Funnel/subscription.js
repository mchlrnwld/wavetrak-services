import { reset } from 'redux-form';
import cookie from 'react-cookie';
import { trackEvent } from '@surfline/web-common';
import * as stripe from '../../common/api/stripe';
import * as subscription from '../../common/api/subscription';
import { updateUser } from '../../common/api/user';
import filterBrandPlans from '../../common/utils/filterBrandPlans';
import formatSubscriptionData from '../../utils/formatSubscriptionData';
import en from '../../international/translations/en';
import { trackCompletedOrder, trackFailedOrder } from '../../utils/trackSubscription';
import { discountAvailablePlans } from './auth';
import cardTypeFromNumber from '../../utils/cardTypeFromNumber';

export const LOADING = 'redux/subscription/LOADING';

export const TOKEN_SUCCESS = 'redux/subscription/TOKEN_SUCCESS';
export const TOKEN_FAIL = 'redux/subscription/TOKEN_FAIL';

export const ADD_CARD_SUCCESS = 'redux/subscription/ADD_CARD_SUCCESS';
export const ADD_CARD_FAIL = 'redux/subscription/ADD_CARD_FAIL';

export const DEFAULT_CARD_SUCCESS = 'redux/subscription/DEFAULT_SUCCESS';
export const DEFAULT_CARD_FAIL = 'redux/subscription/DEFAULT_DEFAULT';

export const SUBMIT = 'redux/subscription/SUBMIT';
export const SUBMIT_SUCCESS = 'redux/subscription/SUBMIT_SUCCESS';
export const SUBMIT_FAIL = 'redux/subscription/SUBMIT_FAIL';

export const DELETE_CARD_SUCCESS = 'redux/subscription/DELETE_CARD_SUCCESS';
export const DELETE_CARD_FAIL = 'redux/subscription/DELETE_CARD_FAIL';

export const ADD_PAYMENT_CARD_SUCCESS = 'redux/subscription/ADD_PAYMENT_CARD_SUCCESS';
export const ADD_PAYMENT_CARD_FAIL = 'redux/subscription/ADD_PAYMENT_CARD_FAIL';

export const TOGGLE_UPDATE_CARD = 'redux/subscription/UPDATE_CARD_DISPLAY';

export const TOGGLE_ADD_CARD = 'redux/subscription/TOGGLE_ADD_CARD';
export const TOGGLE_DISCOUNT = 'redux/subscription/TOGGLE_DISCOUNT';
export const TOGGLE_DISCOUNT_CLOSED = 'redux/subscription/TOGGLE_DISCOUNT_CLOSED';

export const OPEN_CC_FORM = 'redux/subscription/OPEN_CC_FORM';

export const DISCOUNT = 'redux/subscription/DISCOUNT';
export const DISCOUNT_SUCCESS = 'redux/subscription/DISCOUNT_SUCCESS';
export const DISCOUNT_FAIL = 'redux/subscription/DISCOUNT_FAIL';

export const DISCOUNT_QUERY = 'redux/subscription/DISCOUNT_QUERY';
export const TRIAL_QUERY = 'redux/subscription/TRIAL_QUERY';
export const RESET_TRIAL_QUERY = 'redux/subscription/RESET_TRIAL_QUERY';
export const RESET_DISCOUNT = 'redux/subscription/RESET_DISCOUNT';

export const UPDATE_SUBSCRIPTION = 'redux/subscription/UPDATE_SUBSCRIPTION';
export const UPDATE_SUBSCRIPTION_SUCCESS = 'redux/subscription/UPDATE_SUBSCRIPTION_SUCCESS';
export const UPDATE_SUBSCRIPTION_FAILURE = 'redux/subscription/UPDATE_SUBSCRIPTION_FAILURE';

export const SHOW_TRIAL_SUB_FORM = 'redux/subscription/SHOW_TRAIL_SUB_FORM';
export const SHOW_RENEW_SUB_FORM = 'redux/subscription/SHOW_RENEW_SUB_FORM';

export const SHOW_DUPLICATE_CARD_FORM = 'SHOW_DUPLICATE_CARD_FORM';
export const HIDE_DUPLICATE_CARD_FORM = 'HIDE_DUPLICATE_CARD_FORM';

export const SUBMIT_DUPLICATE_CARD_FORM = 'SUBMIT_DUPLICATE_CARD_FORM';
export const SUBMIT_DUPLICATE_CARD_FORM_SUCCESS = 'SUBMIT_DUPLICATE_CARD_FORM_SUCCESS';
export const SUBMIT_DUPLICATE_CARD_FORM_FAILURE = 'SUBMIT_DUPLICATE_CARD_FORM_FAILURE';

const initialState = {
  loading: false,
  ccOpen: false,
  updateOpen: false,
  clickedIndex: null,
  submitting: false,
  currentDefaultStripeSource: false,
  discount: {
    discountSubmitting: false,
    discountOpen: false,
  },
  trialEligible: true,
  isTrialForm: true,
  duplicateCardForm: {
    showDuplicateCardForm: false,
    card: {},
    selectedPlan: {},
    submitting: false,
  },
};

export function openCCForm() {
  return (dispatch) => dispatch({ type: OPEN_CC_FORM });
}

export const showDuplicateCardForm = (subscriptionInfo, formValues) => (dispatch, getState) => {
  const {
    subscriptionForm,
    subscription: { defaultStripeSource: updatedStripeSource },
    auth: {
      user: {
        subscriptionInfo: { availablePlans, cards, defaultStripeSource },
      },
    },
  } = getState();

  const defaultCard = updatedStripeSource || defaultStripeSource;
  const { stripeCCForm } = subscriptionForm;
  const { source } = subscriptionInfo;
  const plan = subscriptionForm.subscription.plan.value;
  const selectedPlan = availablePlans.find((availablePlan) => availablePlan.id === plan);
  const ccNumber = stripeCCForm.cc.value;

  const card = ccNumber
    ? {
        brand: cardTypeFromNumber(ccNumber).type,
        last4: ccNumber.slice(-4),
        expYear: stripeCCForm.expy.value,
        expMonth: stripeCCForm.expm.value,
      }
    : cards.find((stripeCard) => stripeCard.id === defaultCard);

  return dispatch({
    type: SHOW_DUPLICATE_CARD_FORM,
    selectedPlan,
    card,
    formValues,
    source,
  });
};

export const hideDuplicateCardForm = () => (dispatch) => {
  dispatch({ type: HIDE_DUPLICATE_CARD_FORM });
};

export function setDefaultCard(card) {
  return async (dispatch, getState) => {
    dispatch({ type: LOADING, card });
    const token = getState().auth.user.accessToken;
    try {
      const result = await subscription.setDefaultCard(token, card);
      return dispatch({ type: DEFAULT_CARD_SUCCESS, card, result });
    } catch (error) {
      return dispatch({ type: DEFAULT_CARD_FAIL, error });
    }
  };
}

export function toggleUpdateCard(index) {
  return async (dispatch) => {
    dispatch({ type: TOGGLE_UPDATE_CARD, index });
  };
}

export function doDeleteCard(cardId, index) {
  return async (dispatch, getState) => {
    dispatch({ type: LOADING, cardId });
    const token = getState().auth.user.accessToken;

    try {
      const { cards } = getState().subscription;
      cards.splice(index, 1);

      const result = await subscription.deleteCard(token, cardId);
      return dispatch({ type: DELETE_CARD_SUCCESS, cards, result });
    } catch (error) {
      return dispatch({ type: DELETE_CARD_FAIL, error });
    }
  };
}

const segmentErrorEvent = (stripeError, location) => {
  const formLocation = location || 'account';
  let errorToMap;
  switch (stripeError) {
    case 'exp_year':
    case 'exp_month':
      errorToMap = 'Expiry Date';
      break;
    case 'number':
      errorToMap = 'Card Number';
      break;
    case 'cvc':
      errorToMap = 'Security Code';
      break;
    default:
      errorToMap = 'unknown field';
      break;
  }
  return trackEvent(`Entered Invalid ${errorToMap}`, { location: formLocation });
};

const mapStripeErrors = (stripeError) => {
  const msg = en.payment_form.stripe;
  switch (stripeError.param) {
    case 'exp_year':
      return msg.expy;
    case 'exp_month':
      return msg.expm;
    case 'number':
      return msg.cc;
    case 'cvc':
      return msg.scode;
    default:
      return en.payment_form.fatal_cc_error;
  }
};

export function getStripeToken(formLocation) {
  return async (dispatch, getState) => {
    // determine form location for segment tracking on billing error, default to account section
    const location = formLocation || 'account';
    let { ccOpen } = getState().subscription;
    const { user } = getState().auth;

    // Edge case: Forcing ccOpen to true for expired premium users that don't have a card on file
    let hasCards;
    if (user.subscriptionInfo) {
      const { archivedSubscriptions, stripeCustomerId, cards } = user.subscriptionInfo;
      const hasArchivedSub = archivedSubscriptions.length > 0;
      hasCards = cards && cards.length > 0;
      if (stripeCustomerId && hasArchivedSub && !hasCards) {
        ccOpen = true;
      }
    }

    if (ccOpen || !hasCards) {
      try {
        const ccForm = getState().subscriptionForm.stripeCCForm;
        const { cc, scode, expy, expm, zip } = ccForm;
        const keys = Object.keys({
          cc,
          scode,
          expy,
          expm,
          zip,
        });

        for (let i = 0; i < keys.length; i += 1) {
          const key = keys[i];

          if (!ccForm[key].value) {
            return dispatch({
              type: TOKEN_FAIL,
              error: { message: en.payment_form.stripe[`${key}_req`] },
            });
          }
        }

        const cardName = `${user.firstName} ${user.lastName}`;

        const card = {
          number: cc.value,
          cvc: scode.value,
          exp_year: expy.value,
          exp_month: expm.value,
          address_zip: zip.value,
          name: cardName,
        };

        const source = await stripe.getSource(card);
        if (source.error) {
          segmentErrorEvent(source.error.param, location);
          if (source.error.message) source.error.message = mapStripeErrors(source.error);
          await dispatch({ type: TOKEN_FAIL, error: source.error });
          return false;
        }
        await dispatch({ type: TOKEN_SUCCESS, source });
        return source.id;
      } catch (error) {
        if (error.message) error.message = mapStripeErrors(error);
        await dispatch({ type: TOKEN_FAIL, error });
        return false;
      }
    }
    return false;
  };
}

export function postSubscription(values, location) {
  return async (dispatch, getState) => {
    try {
      dispatch({ type: SUBMIT });
      const formLocation = location || 'account';

      const source = values.source || (await dispatch(getStripeToken(formLocation)));

      const token = getState().auth.user.accessToken || (window ? cookie.load('access_token') : '');
      const { error } = getState().subscription;

      const { discount: coupon, plan, trial: freeTrial } = values;
      const subscriptionInfo = {
        plan,
        coupon,
        freeTrial,
        ...(source && { source }),
      };

      let { ccOpen } = getState().subscription;
      const { subscriptionInfo: subInfo, _id: userId } = getState().auth.user;

      // Edge case: Forcing ccOpen to true for expired premium users that don't have a card on file
      if (subInfo) {
        const hasStripeCustomerId = subInfo.stripeCustomerId;
        const hasArchivedSub = subInfo.archivedSubscriptions.length > 0;
        const hasNoCardOnFile = subInfo.cards && subInfo.cards.length === 0;
        if (hasStripeCustomerId && hasArchivedSub && hasNoCardOnFile) {
          ccOpen = true;
        }
      }

      const hasCards = !!(subInfo && subInfo.cards);
      // update user with zip/postal code
      if (ccOpen || !hasCards) {
        const { zip } = getState().subscriptionForm.stripeCCForm;
        if (zip && zip.value) {
          const postal = zip.value;
          await updateUser(token, { _id: userId, postal });
        }
      }

      if (!error) {
        if (hasCards && ccOpen && source) {
          await subscription.addCard(token, subscriptionInfo);
        }
        try {
          const subsResult = await subscription.postSubscription(subscriptionInfo, token);
          trackCompletedOrder(subsResult, values.plan);
        } catch (err) {
          if (err.name === 'DuplicateCardError' && err.code === 1001) {
            return dispatch(showDuplicateCardForm(subscriptionInfo, values));
          }
          throw err;
        }

        // fire segment event if user has cards on file
        if (hasCards) {
          trackEvent(`${source ? 'Added New' : 'Used Existing'} Credit Debit Card`);
        }

        return dispatch({ type: SUBMIT_SUCCESS });
      }
      trackFailedOrder(error);
      return dispatch({ type: SUBMIT_FAIL, error });
    } catch (error) {
      return dispatch({ type: SUBMIT_FAIL, error });
    }
  };
}

export const submitDuplicateCardForm = () => async (dispatch, getState) => {
  dispatch({ type: SUBMIT_DUPLICATE_CARD_FORM });

  try {
    const { formValues, source } = getState().subscription.duplicateCardForm;
    await dispatch(postSubscription({ ...formValues, trial: false, source }, 'funnel'));
    return dispatch({ type: SUBMIT_DUPLICATE_CARD_FORM_SUCCESS });
  } catch (error) {
    return dispatch({
      type: SUBMIT_DUPLICATE_CARD_FORM_FAILURE,
      error: 'There was a problem creating the subscription.',
    });
  }
};

export function changePlan(planId, brand) {
  return async (dispatch, getState) => {
    try {
      dispatch({ type: UPDATE_SUBSCRIPTION });
      const subsInfo = { plan: planId };
      const userId = getState().auth.user.accessToken;
      const subsInState = getState().subscription;
      let result = await subscription.postSubscription(subsInfo, userId);

      const filterByBrand = (company, itemToFilterBy, element) => {
        let filtered = false;
        if (element[itemToFilterBy]) filtered = element[itemToFilterBy].startsWith(company);
        return filtered;
      };
      const filterByBrandWrapper = (itemToFilter, company, propToFilterBy) =>
        itemToFilter.filter(filterByBrand.bind(this, company, propToFilterBy));
      const brandPlans = filterByBrandWrapper(result.availablePlans, brand, 'id');
      const brandSubs = filterByBrandWrapper(result.subscriptions, brand, 'planId');

      result = {
        ...result,
        subscriptions: {
          ...subsInState.subscriptions,
          [brand]: brandSubs,
        },
        availablePlans: {
          ...subsInState.availablePlans,
          [brand]: brandPlans,
        },
      };

      trackEvent(
        'Clicked Changed Subscription Plan',
        {
          location: 'account-subscription',
        },
        null,
      );
      return dispatch({ type: UPDATE_SUBSCRIPTION_SUCCESS, subscription: result });
    } catch (error) {
      return dispatch({ type: UPDATE_SUBSCRIPTION_FAILURE, error });
    }
  };
}

export function checkSuccess(store) {
  return store && store.subscription && store.subscription.successRoute;
}

export function toggleAddCard() {
  return (dispatch) => {
    dispatch(reset('stripeCCForm'));
    return dispatch({ type: TOGGLE_ADD_CARD });
  };
}

/* opens discount form from link below payment form */
export function discountToggle() {
  trackEvent('Start Premium Trial Clicked Enter Discount Code');

  return (dispatch) => dispatch({ type: TOGGLE_DISCOUNT });
}

/* closes discount form from link in discount query error message */
export function closeDiscountForm() {
  return (dispatch) => dispatch({ type: TOGGLE_DISCOUNT_CLOSED });
}

/* displays discount success on funnel login/create pages before validating coupon */
export function applyDiscountInQuery(discountCode) {
  return (dispatch) => dispatch({ type: DISCOUNT_QUERY, discountCode });
}

/* removes discountQuery from redux state to update ui */
export function resetDiscountQuery() {
  return (dispatch) => dispatch({ type: RESET_DISCOUNT });
}

/* sets trialEligible in redux state based on 'trial' query param */
export function invalidateTrialFromQuery(trial) {
  let freeTrial = true;
  if (trial === 'off') freeTrial = false;
  return (dispatch) => dispatch({ type: TRIAL_QUERY, freeTrial });
}

/* resets trialEligible redux state to true for expired user ui.
expired users will not receive free trial even if set to true */
export function resetTrialInQuery() {
  return (dispatch) => dispatch({ type: RESET_TRIAL_QUERY });
}

/* validate discount code with stripe and update available plans on success */
export function validateDiscount(discountCode, discountInQuery) {
  return async (dispatch, getState) => {
    try {
      await dispatch({ type: DISCOUNT });
      const token = getState().auth.user.accessToken;

      if (discountInQuery) await dispatch(applyDiscountInQuery(discountCode));
      const subResponse = await subscription.getSubscription(token, discountCode);
      const brandSubscription = filterBrandPlans(subResponse);

      await dispatch({ type: DISCOUNT_SUCCESS, discountCode });
      return dispatch(discountAvailablePlans(brandSubscription));
    } catch (error) {
      if (error.statusCode === 400) {
        trackEvent('Start Premium Trial Entered Invalid Discount Code');
      }
      return dispatch({ type: DISCOUNT_FAIL, error });
    }
  };
}

export function addCard() {
  return async (dispatch, getState) => {
    try {
      dispatch({ type: SUBMIT });
      const result = await dispatch(getStripeToken());
      const token = getState().auth.user.accessToken;
      const { error } = getState().subscription;
      let cardInfo = {};

      if (result) {
        cardInfo = {
          source: result,
        };
      }
      if (!error) {
        const newCard = await subscription.addCard(token, cardInfo);
        dispatch(reset('stripeCCForm'));
        return dispatch({ type: ADD_PAYMENT_CARD_SUCCESS, card: newCard.card });
      }
      return dispatch({ type: ADD_PAYMENT_CARD_FAIL, error });
    } catch (error) {
      return dispatch({ type: ADD_PAYMENT_CARD_FAIL, error });
    }
  };
}

export function renewSubscription(subsInfo) {
  return async (dispatch, getState) => {
    try {
      dispatch({ type: UPDATE_SUBSCRIPTION });
      const { accessToken } = getState().session;
      const result = await subscription.postSubscription(subsInfo, accessToken);
      const formattedSubcription = formatSubscriptionData(result);
      return dispatch({ type: UPDATE_SUBSCRIPTION_SUCCESS, subscription: formattedSubcription });
    } catch (error) {
      dispatch({ type: UPDATE_SUBSCRIPTION_FAILURE });
      throw error;
    }
  };
}

export function showTrialForm() {
  return (dispatch) => dispatch({ type: SHOW_TRIAL_SUB_FORM });
}

export function showRenewalForm() {
  return (dispatch) => dispatch({ type: SHOW_RENEW_SUB_FORM });
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOADING:
      return {
        ...state,
        loading: true,
      };
    case DISCOUNT_QUERY:
      return {
        ...state,
        discount: {
          ...state.discount,
          discountQuery: action.discountCode,
        },
      };
    case TRIAL_QUERY:
      return {
        ...state,
        trialEligible: action.freeTrial,
      };
    case RESET_DISCOUNT:
      return {
        ...state,
        trialEligible: true,
        discount: {
          ...state.discount,
          discountQuery: null,
          discountOpen: true,
          discountError: false,
        },
      };
    case RESET_TRIAL_QUERY:
      return {
        ...state,
        trialEligible: true,
      };
    case TOGGLE_ADD_CARD:
      return {
        ...state,
        ccOpen: !state.ccOpen,
      };
    case TOGGLE_UPDATE_CARD:
      return {
        ...state,
        updateOpen: !state.updateOpen,
        clickedIndex: action.index,
      };
    case OPEN_CC_FORM:
      return {
        ...state,
        ccOpen: true,
      };
    case TOGGLE_DISCOUNT:
      return {
        ...state,
        discount: {
          ...state.discount,
          discountOpen: !state.discountOpen,
        },
      };
    case TOGGLE_DISCOUNT_CLOSED:
      return {
        ...state,
        discount: {
          ...state.discount,
          discountOpen: false,
        },
      };
    case DISCOUNT:
      return {
        ...state,
        discount: {
          ...state.discount,
          discountSubmitting: true,
          discountError: false,
        },
      };
    case SUBMIT:
      return {
        ...state,
        error: null,
        submitting: true,
      };

    // async action success
    case DEFAULT_CARD_SUCCESS:
    case ADD_CARD_SUCCESS:
      return {
        ...state,
        loading: false,
        defaultStripeSource: action.card,
      };
    case TOKEN_SUCCESS:
      return {
        ...state,
        subscription,
        error: null,
      };
    case SUBMIT_SUCCESS:
      return {
        ...state,
        submitting: false,
        successRoute: true,
        submitSuccess: true,
      };
    case DISCOUNT_SUCCESS:
      return {
        ...state,
        discount: {
          ...state.discount,
          discountCode: action.discountCode,
          discountSuccess: true,
          discountSubmitting: false,
          discountError: false,
        },
      };
    case DELETE_CARD_SUCCESS:
      return {
        ...state,
        cards: [...action.cards],
        defaultStripeSource: action.cards[0].id,
        cardWithDetailOpen: -1,
      };
    case ADD_PAYMENT_CARD_SUCCESS:
      return {
        ...state,
        ccOpen: false,
        submitting: false,
        cards: [action.card, ...state.cards],
        defaultStripeSource: action.card.id,
        cardWithDetailOpen: -1,
      };

    // async action failure
    case DEFAULT_CARD_FAIL:
    case ADD_CARD_FAIL:
    case DELETE_CARD_FAIL:
    case TOKEN_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    case ADD_PAYMENT_CARD_FAIL:
    case SUBMIT_FAIL:
      return {
        ...state,
        submitting: false,
        error: action.error,
      };
    case DISCOUNT_FAIL:
      return {
        ...state,
        discount: {
          ...state.discount,
          discountError: action.error,
          discountSubmitting: false,
        },
      };
    case UPDATE_SUBSCRIPTION:
      return {
        ...state,
        updating: true,
      };
    case UPDATE_SUBSCRIPTION_SUCCESS:
      return {
        ...state,
        ...action.subscription,
        updating: false,
      };
    case UPDATE_SUBSCRIPTION_FAILURE:
      return {
        ...state,
        error: action.error,
        updating: false,
      };
    case SHOW_TRIAL_SUB_FORM:
      return {
        ...state,
        isTrialForm: true,
      };
    case SHOW_RENEW_SUB_FORM:
      return {
        ...state,
        isTrialForm: false,
      };
    case SHOW_DUPLICATE_CARD_FORM:
      // eslint-disable-next-line no-case-declarations
      const { selectedPlan, card, formValues, source } = action;
      return {
        ...state,
        submitting: false,
        ccOpen: false,
        duplicateCardForm: {
          ...state.duplicateCardForm,
          showDuplicateCardForm: true,
          selectedPlan,
          card,
          formValues,
          source,
        },
      };
    case HIDE_DUPLICATE_CARD_FORM:
      return {
        ...state,
        duplicateCardForm: {
          ...state.duplicateCardForm,
          showDuplicateCardForm: false,
        },
      };
    case SUBMIT_DUPLICATE_CARD_FORM:
      return {
        ...state,
        duplicateCardForm: {
          ...state.duplicateCardForm,
          submitting: true,
          success: false,
        },
      };
    case SUBMIT_DUPLICATE_CARD_FORM_SUCCESS:
      return {
        ...state,
        duplicateCardForm: {
          ...state.duplicateCardForm,
          submitting: false,
          success: true,
        },
      };
    case SUBMIT_DUPLICATE_CARD_FORM_FAILURE:
      return {
        ...state,
        duplicateCardForm: {
          ...state.duplicateCardForm,
          submitting: false,
          success: false,
          error: action.error,
        },
      };
    default:
      return state;
  }
}

/**
 * For the current implementation we can get around needing to change the
 * redux form state. but if we needed to the `stripeCCFormReducer` would do the
 * trick
 *
 * For example we could listen for a `TOKEN_FAIL` action
 *
 *  switch (action.type) {
 *    case TOKEN_FAIL:
 *      const theState = {
 *        ...state,
 *      };
 *      return theState;
 *    default:
 *      return state;
 *  }
 *
 */
export function stripeCCFormReducer(state) {
  return state;
}
