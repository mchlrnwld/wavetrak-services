import cookie from 'react-cookie';
import { trackEvent } from '@surfline/web-common';
import { authorise } from '../../common/api/auth';
import * as userApi from '../../common/api/user';
import * as subscriptionApi from '../../common/api/subscription';
import filterSubObjByBrand from '../../common/utils/filterSubObjByBrand';
import setLoginCookie from '../../utils/setLoginCookie';
import signIn from '../../utils/signIn';

export const DISCOUNTPLANS = 'auth/DISCOUNTPLANS';
export const DISCOUNTPLANS_SUCCESS = 'auth/DISCOUNTPLANS_SUCCESS';
export const DISCOUNTPLANS_FAIL = 'auth/DISCOUNTPLANS_FAIL';
export const LOAD_USER = 'auth/LOAD_USER';
export const LOAD_USER_SUCCESS = 'auth/LOAD_USER_SUCCESS';
export const LOAD_USER_FAILURE = 'auth/LOAD_USER_FAILURE';
export const FUNNELLOGIN = 'auth/FUNNELLOGIN';
export const FUNNELLOGIN_SUCCESS = 'auth/FUNNELLOGIN_SUCCESS';
export const FUNNELLOGIN_FAIL = 'auth/FUNNELLOGIN_FAIL';
export const PLANS = 'auth/PLANS';
export const PLANS_SUCCESS = 'auth/PLANS_SUCCESS';
export const PLANS_FAIL = 'auth/PLANS_FAIL';

const initialState = {
  loaded: false,
  cookies: false,
  tokenAboveLimit: false,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case FUNNELLOGIN:
      return {
        ...state,
        signingIn: true,
        submitSuccess: false,
      };
    case FUNNELLOGIN_SUCCESS:
      return {
        ...state,
        signingIn: false,
        user: {
          ...state.user,
          ...action.result,
        },
        submitSuccess: true,
      };
    case FUNNELLOGIN_FAIL:
      return {
        ...state,
        signingIn: false,
        user: action.result,
        loginError: action.error,
        submitSuccess: false,
      };
    case PLANS:
      return {
        ...state,
        loadingPlans: true,
      };
    case PLANS_SUCCESS:
      return {
        ...state,
        loadingPlans: false,
        user: {
          ...state.user,
          subscriptionInfo: action.result,
        },
      };
    case PLANS_FAIL:
      return {
        ...state,
        loadingPlans: false,
        error: action.error,
      };
    case DISCOUNTPLANS:
      return {
        ...state,
        updatingPlans: true,
      };
    case DISCOUNTPLANS_SUCCESS:
      return {
        ...state,
        updatingPlans: false,
        user: {
          ...state.user,
          subscriptionInfo: {
            ...state.user.subscriptionInfo,
            availablePlans: action.result,
          },
        },
      };
    case DISCOUNTPLANS_FAIL:
      return {
        ...state,
        updatingPlans: false,
        error: action.error,
      };
    case LOAD_USER:
      return {
        ...state,
        loadingUser: true,
      };
    case LOAD_USER_SUCCESS:
      return {
        ...state,
        loadingUser: false,
        cookies: true,
        user: {
          ...state.user,
          ...action.user,
          accessToken: action.token,
        },
      };
    case LOAD_USER_FAILURE:
      return {
        ...state,
        loadingUser: false,
        loadingTokenError: action.error,
      };
    default:
      return state;
  }
}

export const loadUser = () => async (dispatch) => {
  try {
    dispatch({ type: LOAD_USER });
    const accessToken = cookie.load('access_token');
    if (accessToken) {
      const authorized = await authorise(accessToken);
      if (authorized) {
        const user = await userApi.getUser(cookie.load('access_token'));
        return dispatch({ type: LOAD_USER_SUCCESS, token: accessToken, user });
      }
    }
    return dispatch({ type: LOAD_USER_FAILURE, error: 'Invalid Token' });
  } catch (err) {
    return dispatch({ type: LOAD_USER_FAILURE, error: err });
  }
};

export const getSubscriptionInfo = (accessToken) => {
  return async (dispatch) => {
    dispatch({ type: PLANS });

    try {
      const subscriptions = await subscriptionApi.getSubscription(accessToken);
      const subs = filterSubObjByBrand(subscriptions);
      return dispatch({ type: PLANS_SUCCESS, result: subs });
    } catch (err) {
      return dispatch({ type: PLANS_FAIL, error: err });
    }
  };
};

export const funnelLogin = (values) => async (dispatch) => {
  dispatch({ type: FUNNELLOGIN });
  try {
    const response = await signIn(values, 'funnel');
    const { accessToken, refreshToken, canUpdate, redirectAway, expiresIn, userId: _id } = response;

    const { email, firstName } = values;

    const maxAge = values.staySignedIn ? expiresIn : 1800;
    const refreshMaxAge = values.staySignedIn ? 31536000 : 1800;
    await setLoginCookie(accessToken, refreshToken, maxAge, refreshMaxAge);

    trackEvent('Completed Checkout Step', {
      step: 1,
    });

    const result = {
      email,
      accessToken,
      refreshToken,
      firstName,
      canUpdate,
      redirectAway,
      _id,
    };

    await dispatch(getSubscriptionInfo(accessToken));
    await dispatch({ type: FUNNELLOGIN_SUCCESS, result });

    return { redirectAway };
  } catch (err) {
    // eslint-disable-next-line no-underscore-dangle
    err.dispatch = { type: FUNNELLOGIN_FAIL, error: err._error };
    throw err;
  }
};

export function discountAvailablePlans(subscription) {
  return async (dispatch) => {
    dispatch({ type: DISCOUNTPLANS });
    try {
      const result = subscription.availablePlans;
      return dispatch({ type: DISCOUNTPLANS_SUCCESS, result });
    } catch (err) {
      return dispatch({ type: DISCOUNTPLANS_FAIL, err });
    }
  };
}
