import * as passwordreset from '../../common/api/passwordreset';
import config from '../../config';

const CLEAR = 'redux/forgotPassword/CLEAR';
const CLEAR_SUCCESS = 'redux/forgotPassword/CLEAR_SUCCESS';
const CLEAR_FAIL = 'redux/forgotPassword/CLEAR_FAIL';

const REQUEST = 'redux/forgotPassword/REQUEST';
const REQUEST_SUCCESS = 'redux/forgotPassword/REQUEST_SUCCESS';
const REQUEST_FAIL = 'redux/forgotPassword/REQUEST_FAIL';

const GETUSERID = 'redux/forgotPassword/GETUSERID';
const GETUSERID_SUCCESS = 'redux/forgotPassword/GETUSERID_SUCCESS';
const GETUSERID_FAIL = 'redux/forgotPassword/GETUSERID_FAIL';

const REDIRECT = 'redux/forgotPassword/REDIRECT';
const REDIRECT_SUCCESS = 'redux/forgotPassword/REDIRECT_SUCCESS';
const REDIRECT_FAIL = 'redux/forgotPassword/REDIRECT_FAIL';

const SETPASSWORD = 'redux/forgotPassword/SETPASSWORD';
const SETPASSWORD_SUCESS = 'redux/forgotPassword/SETPASSWORD_SUCESS';
const SETPASSWORD_FAIL = 'redux/SETPASSWORD_FAIL';

const initialState = {
  user: null,
  isValidated: false,
  submitting: false,
  submitSuccess: false,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CLEAR:
      return {
        ...state,
        requestingClear: true,
      };
    case CLEAR_SUCCESS:
      return {
        ...state,
        requestingClear: false,
        user: action.result,
        isValidated: false,
      };
    case CLEAR_FAIL:
      return {
        ...state,
        requestingReset: false,
        user: null,
        requestResetError: action.error,
      };
    case REQUEST:
      return {
        ...state,
        submitting: true,
        submitSuccess: true,
      };
    case REQUEST_SUCCESS:
      return {
        ...state,
        submitting: false,
        resetSuccess: true,
        submitSuccess: true,
        user: action.result,
      };
    case REQUEST_FAIL:
      return {
        ...state,
        submitting: false,
        user: null,
        submitSuccess: false,
        requestResetError: action.error,
      };
    case GETUSERID:
      return {
        ...state,
        requestingUserId: true,
      };
    case GETUSERID_SUCCESS:
      return {
        ...state,
        user: action.result,
        isValidated: true,
        requestingUserId: false,
      };
    case GETUSERID_FAIL:
      return {
        ...state,
        requestingUserId: false,
        user: null,
        requestResetError: action.error,
      };
    case REDIRECT:
      return {
        ...state,
        redirecting: true,
      };
    case REDIRECT_SUCCESS:
      return {
        ...state,
        redirecting: false,
        user: action.result,
      };
    case REDIRECT_FAIL:
      return {
        ...state,
        redirecting: false,
        user: null,
        redirectError: action.error,
      };
    case SETPASSWORD:
      return {
        ...state,
        requestingPasswordReset: true,
        passwordSubmitSuccess: true,
      };
    case SETPASSWORD_SUCESS:
      return {
        ...state,
        requestingPasswordReset: false,
        passwordReset: true,
        passwordSubmitSuccess: true,
      };
    case SETPASSWORD_FAIL:
      return {
        ...state,
        requestingPasswordReset: false,
        passwordReset: false,
        passwordSubmitSuccess: false,
      };
    default:
      return state;
  }
}

export function requestReset(email) {
  return async (dispatch) => {
    dispatch({ type: REQUEST });
    try {
      const result = {
        email,
      };
      return dispatch({ type: REQUEST_SUCCESS, result });
    } catch (err) {
      return dispatch({ type: REQUEST_FAIL, err });
    }
  };
}

export function enterCode(userId, resetCode) {
  return async (dispatch) => {
    dispatch({ type: GETUSERID });
    try {
      const result = { userId, resetCode };
      return dispatch({ type: GETUSERID_SUCCESS, result });
    } catch (err) {
      return dispatch({ type: GETUSERID_FAIL, err });
    }
  };
}

export function setPassword() {
  return async (dispatch) => {
    dispatch({ type: SETPASSWORD });
    try {
      return dispatch({ type: SETPASSWORD_SUCESS });
    } catch (err) {
      return dispatch({ type: SETPASSWORD_FAIL, err });
    }
  };
}

export function isValidated(globalState) {
  return globalState.forgotPassword.isValidated;
}

export function validate() {
  return async (dispatch, getState) => {
    dispatch({ type: GETUSERID });
    try {
      const queryString = getState().router.location.search;
      const matches = queryString.match(/hash=([^&]*)/);
      const hash = matches !== null ? matches[1] : '1234';
      const hashBody = { hash, brand: config.company };
      const response = await passwordreset.validateHash(hashBody);
      const result = { userId: response.userId, resetCode: response.resetCode };
      return dispatch({ type: GETUSERID_SUCCESS, result });
    } catch (err) {
      console.log(err);
      return dispatch({ type: GETUSERID_FAIL, err });
    }
  };
}

export function clearUser() {
  return (dispatch) => {
    dispatch({ type: CLEAR });
    try {
      return dispatch({ type: CLEAR_SUCCESS, result: null });
    } catch (err) {
      return dispatch({ type: CLEAR_FAIL, err });
    }
  };
}
