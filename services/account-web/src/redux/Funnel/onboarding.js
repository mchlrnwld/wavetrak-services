import { postOnboarding } from '../../common/api/onboarding';
import { login } from './auth';
import config from '../../config';

export const POST_ONBOARDING = 'redux/onboarding/POST_ONBOARDING';
export const POST_ONBOARDING_SUCCESS = 'redux/onboarding/POST_ONBOARDING_SUCCESS';
export const POST_ONBOARDING_FAIL = 'redux/onboarding/POST_ONBOARDING_FAIL';

const initialState = {
  loading: false,
  error: null
};

export function postOnboardingObj(loginInfo) {
  return async (dispatch) => {
    dispatch({ type: POST_ONBOARDING });
    try {
      // post to onboarding service if local storage > slOnboarding > favorites > spots exist
      if (loginInfo.spots) {
        const onboardingInfo = { spots: loginInfo.spots };
        const onboardingResult = await postOnboarding(loginInfo.accessToken, onboardingInfo);
        await dispatch({ type: POST_ONBOARDING_SUCCESS, onboardingResult });
      }
      // trigger submit success ui
      await dispatch(login(loginInfo.email, loginInfo.accessToken, loginInfo.refreshToken));
      localStorage.removeItem('slOnboarding');
      // redirect to homepage
      window.location.href = config.surflineHome;
    } catch (error) {
      // dispatch error to display on to user
      dispatch({ type: POST_ONBOARDING_FAIL, error });
    }
  };
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case POST_ONBOARDING:
      return {
        ...state,
        loading: true
      };
    case POST_ONBOARDING_SUCCESS:
      return {
        ...state,
        loading: false,
        onboarding: action.result,
      };
    case POST_ONBOARDING_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default:
      return state;
  }
}
