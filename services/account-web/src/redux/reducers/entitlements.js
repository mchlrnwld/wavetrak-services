import createReducer from './createReducer';
import {
  FETCH_ENTITLEMENTS,
  FETCH_ENTITLEMENTS_SUCCESS,
  FETCH_ENTITLEMENTS_FAIL,
  DISABLE_FREE_TRIAL,
  ENABLE_FREE_TRIAL,
} from '../actions/entitlements';

const initialState = {
  isPremium: false,
  trialEligable: false,
  entitlements: [],
  promotions: []
};
const handlers = {};

handlers[FETCH_ENTITLEMENTS] = state => ({
  ...state,
  fetchingEntitlements: true,
});

handlers[FETCH_ENTITLEMENTS_SUCCESS] = (state, { entitlements, promotions }) => ({
  ...state,
  fetchingEntitlements: false,
  entitlements,
  promotions,
});

handlers[FETCH_ENTITLEMENTS_FAIL] = (state, { error }) => ({
  ...state,
  fetchingEntitlements: false,
  entitlements: null,
  promotions: null,
  error
});

handlers[DISABLE_FREE_TRIAL] = (state, { company }) => ({
  ...state,
  promotions: state.promotions.filter(promo => promo !== `${company}_free_trial`)
});

handlers[ENABLE_FREE_TRIAL] = (state, { company }) => ({
  ...state,
  promotions: state.promotions.concat([`${company}_free_trial`])
});

export default createReducer(handlers, initialState);
