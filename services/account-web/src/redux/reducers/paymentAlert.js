import createReducer from './createReducer';

const initialState = {
  alert: false,
  message: 'No payment alerts found.',
};

const handlers = {};

export default createReducer(handlers, initialState);
