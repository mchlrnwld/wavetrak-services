import createReducer from './createReducer';
import {
  LOGIN,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
} from '../actions/login';

const initialState = {
  loginError: null,
};
const handlers = {};

handlers[LOGIN] = state => ({
  ...state,
});

handlers[LOGIN_SUCCESS] = state => ({
  ...state,
});

handlers[LOGIN_FAIL] = (state, { error }) => ({
  ...state,
  loginError: error,
});

export default createReducer(handlers, initialState);
