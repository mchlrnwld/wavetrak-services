import createReducer from './createReducer';
import { SHOW_DUPLICATE_CARD, CLEAR_DUPLICATE_CARD } from '../actions/duplicateCard';

const initialState = {
  showDuplicateCard: false,
  duplicateCardError: null,
  plan: null,
  source: null,
};
const handlers = {};

handlers[SHOW_DUPLICATE_CARD] = (state, { message, plan, source }) => ({
  ...state,
  showDuplicateCard: true,
  duplicateCardMessage: message,
  duplicateCardError: null,
  plan,
  source
});

handlers[CLEAR_DUPLICATE_CARD] = state => ({
  ...state,
  showDuplicateCard: false,
  duplicateCardMessage: null,
  duplicateCardError: null,
  plan: null,
  source: null
});

export default createReducer(handlers, initialState);
