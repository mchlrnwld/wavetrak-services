import createReducer from './createReducer';
import {
  VERIFY_EMAIL,
  VERIFY_EMAIL_SUCCESS,
  VERIFY_EMAIL_FAIL,
  RESEND_EMAIL_VERIFICATION,
  RESEND_EMAIL_VERIFICATION_SUCCESS,
  RESEND_EMAIL_VERIFICATION_FAIL,
} from '../actions/verifyEmail';

const initialState = {
  verifyingEmail: true,
  success: false,
  error: null,
  resendingEmail: false,
  resendSuccess: false,
};

const handlers = {};

handlers[VERIFY_EMAIL] = state => ({
  ...state,
  verifyingEmail: true,
});

handlers[VERIFY_EMAIL_SUCCESS] = state => ({
  ...state,
  verifyingEmail: false,
  success: true,
  error: null,
});

handlers[VERIFY_EMAIL_FAIL] = (state, { error }) => ({
  ...state,
  verifyingEmail: false,
  error,
});

handlers[RESEND_EMAIL_VERIFICATION] = state => ({
  ...state,
  resendingEmail: true,
});

handlers[RESEND_EMAIL_VERIFICATION_SUCCESS] = state => ({
  ...state,
  resendingEmail: false,
  resendSuccess: true,
  error: null,
});

handlers[RESEND_EMAIL_VERIFICATION_FAIL] = (state, { error }) => ({
  ...state,
  error,
  resendingEmail: false,
});

export default createReducer(handlers, initialState);
