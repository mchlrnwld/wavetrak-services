import createReducer from './createReducer';

const initialState = {};
const handlers = {};

export default createReducer(handlers, initialState);
