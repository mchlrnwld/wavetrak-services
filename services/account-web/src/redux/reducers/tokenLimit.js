import createReducer from './createReducer';
import {
  TOKEN_LIMIT_FAIL,
  TOKEN_LIMIT_RESET,
} from '../actions/tokenLimit';

const initialState = {
  tokenAboveLimit: false,
  userData: {}
};
const handlers = {};

handlers[TOKEN_LIMIT_FAIL] = (state, { authError }) => ({
  ...state,
  tokenAboveLimit: true,
  userData: authError
});

handlers[TOKEN_LIMIT_RESET] = state => ({
  ...state,
  tokenAboveLimit: false,
  userData: {}
});

export default createReducer(handlers, initialState);
