import createReducer from './createReducer';
import {
  SUBMIT_SUBSCRIPTION,
  SUBMIT_SUBSCRIPTION_SUCCESS,
  SUBMIT_SUBSCRIPTION_FAIL,
  RENEW_SUBSCRIPTION,
  RENEW_SUBSCRIPTION_SUCCESS,
  RENEW_SUBSCRIPTION_FAILURE,
  CHANGE_SUBSCRIPTION_PLAN,
  CHANGE_SUBSCRIPTION_PLAN_SUCCESS,
  CHANGE_SUBSCRIPTION_PLAN_FAILURE,
} from '../actions/subscription';

const initialState = {
  error: null,
  success: null,
  isRenewing: false,
  isRenewingError: false,
  isRenewingSuccess: false,
  renewalErrorMessage: null,
};
const handlers = {};

handlers[SUBMIT_SUBSCRIPTION] = (state) => ({
  ...state,
  error: null,
});

handlers[SUBMIT_SUBSCRIPTION_SUCCESS] = (state) => ({
  ...state,
  error: null,
  success: true,
});

handlers[SUBMIT_SUBSCRIPTION_FAIL] = (state, { message }) => ({
  ...state,
  error: message,
});

handlers[RENEW_SUBSCRIPTION] = (state) => ({
  ...state,
  error: null,
  isRenewing: true,
});

handlers[RENEW_SUBSCRIPTION_SUCCESS] = (state) => ({
  ...state,
  error: null,
  success: true,
  isRenewing: false,
  isRenewingSuccess: true,
});

handlers[RENEW_SUBSCRIPTION_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  isRenewing: false,
  isRenewingError: true,
  renewalErrorMessage: message,
});

handlers[CHANGE_SUBSCRIPTION_PLAN] = (state) => ({
  ...state,
  error: null,
  isUpdatingPlan: true,
});

handlers[CHANGE_SUBSCRIPTION_PLAN_SUCCESS] = (state) => ({
  ...state,
  error: null,
  success: true,
  isUpdatingPlan: true,
  isUpdatingPlanSuccess: true,
});

handlers[CHANGE_SUBSCRIPTION_PLAN_FAILURE] = (state, { message }) => ({
  ...state,
  error: message,
  isUpdatingPlan: false,
  isUpdatingPlanError: true,
  updateErrorMessage: message,
});

export default createReducer(handlers, initialState);
