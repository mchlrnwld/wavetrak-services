import createReducer from './createReducer';
import {
  FETCH_CARDS,
  FETCH_CARDS_FAIL,
  FETCH_CARDS_SUCCESS,
  TOGGLE_SHOW_CARDS,
  TOGGLE_SHOW_NEW_CARD_FORM,
  SET_SHOW_EDIT_CARD_FORM,
  SET_SELECTED_CARD_SOURCE,
  SET_DEFAULT_CARD_SOURCE,
  SET_DEFAULT_CARD_SOURCE_SUCCESS,
  SET_DEFAULT_CARD_SOURCE_FAIL,
  ADD_CARD,
  ADD_CARD_SUCCESS,
  ADD_CARD_FAIL,
  UPDATE_CARD,
  UPDATE_CARD_SUCCESS,
  UPDATE_CARD_FAIL,
  DELETE_CARD,
  DELETE_CARD_SUCCESS,
  DELETE_CARD_FAIL,
} from '../actions/cards';

const initialState = {
  cards: null,
  defaultSource: null,
  selectedCard: null,
  showCards: null,
  showNewCardForm: null,
  submitting: false,
  cardToEdit: null,
  error: null,
  editCardError: null,
};

const handlers = {};

handlers[FETCH_CARDS] = state => ({
  ...state
});

handlers[FETCH_CARDS_SUCCESS] = (state, { cards, defaultSource, showCards }) => ({
  ...state,
  cards,
  defaultSource,
  selectedCard: defaultSource,
  showCards,
});

handlers[FETCH_CARDS_FAIL] = (state, { error }) => ({
  ...state,
  error,
});

handlers[TOGGLE_SHOW_CARDS] = (state, { showCards }) => ({
  ...state,
  showCards,
});

handlers[TOGGLE_SHOW_NEW_CARD_FORM] = (state, { showNewCardForm }) => ({
  ...state,
  showNewCardForm,
  cardToEdit: null,
});

handlers[SET_SHOW_EDIT_CARD_FORM] = (state, { cardToEdit }) => {
  const { cardToEdit: currentCardToEdit } = state;
  return {
    ...state,
    cardToEdit: cardToEdit !== currentCardToEdit ? cardToEdit : null,
    showNewCardForm: false,
  };
};

handlers[SET_SELECTED_CARD_SOURCE] = (state, { selectedCard }) => ({
  ...state,
  selectedCard
});

handlers[SET_DEFAULT_CARD_SOURCE] = (state, { defaultSource }) => ({
  ...state,
  defaultSource,
  submitting: true,
});

handlers[SET_DEFAULT_CARD_SOURCE_SUCCESS] = (state, { defaultSource }) => ({
  ...state,
  defaultSource,
  cardToEdit: null,
  submitting: false,
  editCardError: null,
});

handlers[SET_DEFAULT_CARD_SOURCE_FAIL] = (state, { error }) => ({
  ...state,
  editCardError: error,
  submitting: false,
});

handlers[ADD_CARD] = state => ({
  ...state,
  submitting: true,
});

handlers[ADD_CARD_SUCCESS] = (state, { card }) => {
  const { cards } = state;
  cards.unshift(card);
  return {
    ...state,
    cards,
    submitting: false,
    showNewCardForm: false,
    defaultSource: card.id,
    error: null,
  };
};

handlers[ADD_CARD_FAIL] = (state, { error }) => ({
  ...state,
  error,
  submitting: false,
});

handlers[UPDATE_CARD] = state => ({
  ...state,
  submitting: true,
});

handlers[UPDATE_CARD_SUCCESS] = (state, { card: updatedCard }) => {
  const { cards } = state;
  const {
    expMonth, expYear, id, addressZip
  } = updatedCard;
  const updateAtIndex = cards.findIndex(card => card.id === id);

  cards[updateAtIndex].expMonth = expMonth;
  cards[updateAtIndex].expYear = expYear;
  cards[updateAtIndex].addressZip = addressZip;

  return {
    ...state,
    cards,
    cardToEdit: null,
    submitting: false,
    editCardError: null,
  };
};

handlers[UPDATE_CARD_FAIL] = (state, { error }) => ({
  ...state,
  editCardError: error,
  submitting: false,
});

handlers[DELETE_CARD] = state => ({
  ...state,
  submitting: true,
});

handlers[DELETE_CARD_SUCCESS] = (state, { cardId }) => {
  const { cards } = state;
  const updatedCards = cards.filter(({ id }) => id !== cardId);
  return {
    ...state,
    cards: updatedCards,
    cardToEdit: null,
    submitting: false,
    editCardError: null,
  };
};

handlers[DELETE_CARD_FAIL] = (state, { error }) => ({
  ...state,
  editCardError: error,
  submitting: false,
});

export default createReducer(handlers, initialState);
