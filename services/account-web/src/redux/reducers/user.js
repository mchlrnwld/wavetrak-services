import createReducer from './createReducer';

import {
  CREATE,
  CREATE_SUCCESS,
  CREATE_FAIL,
} from '../actions/user';

const initialState = {
  createError: null,
  user: null,
};
const handlers = {};

handlers[CREATE] = state => ({
  ...state,
});

handlers[CREATE_SUCCESS] = state => ({
  ...state,
});

handlers[CREATE_FAIL] = (state, { error }) => ({
  ...state,
  createError: error
});

export default createReducer(handlers, initialState);
