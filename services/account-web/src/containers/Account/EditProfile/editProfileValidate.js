import memoize from 'lru-memoize';
import { createValidator, required, email } from 'utils/validation';

const editProfileValidation = createValidator({
  email: [required, email],
  firstName: [required],
  lastName: [required],
});
export default memoize(10)(editProfileValidation);
