import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { reduxForm } from 'redux-form';
import { Button } from '@surfline/quiver-react';
import { trackEvent, trackNavigatedToPage } from '@surfline/web-common';
import { EditProfileForm } from '../../../components';
import en from '../../../international/translations/en';
import config from '../../../config';
import editProfileValidate from './editProfileValidate';
import { updateProfile, resetState } from '../../../redux/Account/editProfile';
import brandToString from '../../../utils/brandToString';
import { resendEmailVerification } from '../../../redux/actions/verifyEmail';

class EditProfile extends Component {
  componentDidMount() {
    trackNavigatedToPage('Account', {}, 'Edit Profile');
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(resetState());
  }

  onSubmit = (data) => {
    const { dispatch } = this.props;
    dispatch(updateProfile(data));
    trackEvent('Clicked Save', { location: 'edit-profile' });
  };

  enableSubmit = () => {
    const { dispatch, editProfileSucceeded } = this.props;
    if (editProfileSucceeded) {
      dispatch(resetState());
    }
  };

  render() {
    const {
      fields,
      handleSubmit,
      editProfileSubmitting,
      editProfileSucceeded,
      editProfileSuccessMessage,
      editProfileErrorMessage,
      user: { email, isEmailVerified },
      doResendEmail,
      resendSuccess,
    } = this.props;

    const {
      edit_profile: { meta_title: metaTitle, title, emailVerificationMessage },
    } = en;
    const { company } = config;
    const brand = brandToString(company);
    return (
      <div className="editProfile">
        <Helmet title={metaTitle[config.company]} />
        <div className="editProfile__heading__container">
          <div className="editProfile__heading">{title}</div>
        </div>
        <div className="editProfile__body__container">
          {company === 'sl' && !isEmailVerified ? (
            <div className="account__card account__card--alt">
              <div className="email-verification__profile">
                <h5 className="email-verification__profile__header">
                  {!resendSuccess ? 'Verify Your Email' : 'Email Sent'}
                </h5>
                <div className="email-verification__profile__message">
                  {emailVerificationMessage(email, brand)}
                </div>
                <div className="email-verification__profile__message">
                  Make sure to check your spam folder.
                </div>
                {!resendSuccess ? (
                  <div className="email-verification__profile__button-container">
                    <div className="email-verification__profile__message">
                      Did not receive an email?
                    </div>
                    <Button onClick={() => doResendEmail()}>Resend Email Verification</Button>
                  </div>
                ) : (
                  <div className="email-verification__profile__message">
                    {'If you are unable to receive this email, please contact '}
                    <a
                      href="mailto:support@surfline.com"
                      className="email-verification__profile__support-link"
                    >
                      support@surfline.com
                    </a>
                  </div>
                )}
              </div>
            </div>
          ) : null}
          <div className="account__card">
            <EditProfileForm
              fields={fields}
              onSubmit={handleSubmit(this.onSubmit)}
              enableSubmit={this.enableSubmit}
              editProfileErrorMessage={editProfileErrorMessage}
              editProfileSuccessMessage={editProfileSuccessMessage}
              editProfileSucceeded={editProfileSucceeded}
              editProfileSubmitting={editProfileSubmitting}
            />
          </div>
        </div>
      </div>
    );
  }
}

EditProfile.propTypes = {
  user: PropTypes.shape({
    email: PropTypes.string,
    isEmailVerified: PropTypes.bool,
  }).isRequired,
  editProfileSucceeded: PropTypes.bool,
  editProfileSubmitting: PropTypes.bool,
  editProfileErrorMessage: PropTypes.string,
  editProfileSuccessMessage: PropTypes.string,
  dispatch: PropTypes.func.isRequired,
  fields: PropTypes.shape({}).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  doResendEmail: PropTypes.func.isRequired,
  resendSuccess: PropTypes.bool,
};

EditProfile.defaultProps = {
  resendSuccess: null,
  editProfileSuccessMessage: '',
  editProfileSubmitting: false,
  editProfileSucceeded: false,
  editProfileErrorMessage: '',
};

export default reduxForm(
  {
    form: 'editProfile',
    fields: ['firstName', 'lastName', 'email', '_id'],
    validate: editProfileValidate,
    desstroyOnUnmount: true,
  },
  (state) => ({
    initialValues: state.user,
    user: state.user,
    editProfileErrorMessage: state.user.editProfileErrorMessage,
    editProfileSuccessMessage: state.user.editProfileSuccessMessage,
    editProfileSubmitting: state.user.editProfileSubmitting,
    editProfileSucceeded: state.user.editProfileSucceeded,
    resendSuccess: state.verifyEmail.resendSuccess,
  }),
  (dispatch) => ({
    doResendEmail: () => dispatch(resendEmailVerification()),
    dispatch,
  }),
)(EditProfile);
