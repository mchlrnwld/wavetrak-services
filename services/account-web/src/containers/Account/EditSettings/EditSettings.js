import PropTypes from 'prop-types';
import React, { useEffect, useMemo, useCallback, memo } from 'react';
import { trackEvent, trackNavigatedToPage } from '@surfline/web-common';
import { useTreatment } from '@surfline/quiver-react';
import queryString from 'query-string';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { EditSettingsForm } from '../../../components';
import en from '../../../international/translations/en';
import config from '../../../config';
import {
  resetState,
  updateUnit,
  updateFeed,
  updateDate,
  updateModel,
} from '../../../redux/Account/editSettings';
import { SL_WEB_HAWAIIAN_WAVE_HEIGHT } from '../../../common/utils/treatments';

const EditSettings = ({
  location,
  dispatch,
  userSettings,
  editSettingsErrorMessage,
  editSettingsSuccessMessage,
  userTimezone,
  accessToken,
}) => {
  const { treatment: hawaiianWaveHeightTreatment } = useTreatment(SL_WEB_HAWAIIAN_WAVE_HEIGHT);

  useEffect(() => {
    trackNavigatedToPage('Account', {}, 'Edit Settings');
    return () => {
      dispatch(resetState());
    };
  }, [dispatch]);

  const onSettingChange = useCallback(
    (fieldName, fieldValue, fieldType = 'units', hawaiianFlag = 'off') => {
      if (fieldType === 'feed') {
        dispatch(updateFeed(fieldName, fieldValue));
        trackEvent(`Saved Unit`, {
          location: 'edit-settings',
          fieldName: fieldType,
          fieldValue,
        });
      } else if (fieldType === 'date') {
        dispatch(updateDate(fieldName, fieldValue));
        trackEvent(`Saved Unit`, {
          location: 'edit-settings',
          fieldName: fieldType,
          fieldValue,
        });
      } else if (fieldType === 'model') {
        dispatch(updateModel(fieldName, fieldValue));
        trackEvent(`Saved Unit`, {
          location: 'edit-settings',
          fieldName,
          fieldValue,
        });
      } else {
        dispatch(updateUnit(fieldName, fieldValue, hawaiianFlag));
        trackEvent(`Saved Unit`, {
          location: 'edit-settings',
          fieldName,
          fieldValue,
        });
      }
    },
    [dispatch],
  );

  const redirectUrl = useMemo(() => {
    const query = queryString.parse(location.search);
    return query.redirectUrl;
  }, [location]);

  return (
    <div className="editSettings">
      <Helmet title={en.edit_settings.meta_title[config.company]} />
      <div className="editSettings__heading__container">
        <div className="editSettings__heading">{en.edit_settings.title}</div>
      </div>
      <div className="editSettings__body__container">
        <div className="account__card">
          <EditSettingsForm
            userSettings={userSettings}
            onSettingChange={onSettingChange}
            editSettingsErrorMessage={editSettingsErrorMessage}
            editSettingsSuccessMessage={editSettingsSuccessMessage}
            redirectUrl={redirectUrl}
            userTimezone={userTimezone}
            accessToken={accessToken}
            hawaiianScaleFlag={hawaiianWaveHeightTreatment}
          />
        </div>
      </div>
    </div>
  );
};

EditSettings.propTypes = {
  location: PropTypes.shape({
    search: PropTypes.string,
  }).isRequired,
  dispatch: PropTypes.func.isRequired,
  userSettings: PropTypes.shape({}).isRequired,
  editSettingsSuccessMessage: PropTypes.string,
  editSettingsErrorMessage: PropTypes.string,
  accessToken: PropTypes.string.isRequired,
  userTimezone: PropTypes.string,
};

EditSettings.defaultProps = {
  editSettingsErrorMessage: '',
  editSettingsSuccessMessage: '',
  userTimezone: '',
};

export default connect((state) => ({
  userSettings: state.userSettings,
  editSettingsErrorMessage: state.userSettings.editSettingsErrorMessage,
  editSettingsSuccessMessage: state.userSettings.editSettingsSuccessMessage,
  userTimezone: state.user.timezone,
  accessToken: state.session.accessToken,
}))(memo(EditSettings));
