import React, { useCallback, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useSelector, useDispatch, shallowEqual } from 'react-redux';
import { trackEvent, trackNavigatedToPage } from '@surfline/web-common';
import {
  UserCard,
  PromoCard,
  StatusError,
  SubscriptionSummaryCard,
  FailedPaymentMessage,
} from '../../../components';
import en from '../../../international/translations/en';
import { renew } from '../../../redux/actions/subscription';

const Overview = () => {
  useEffect(() => {
    trackNavigatedToPage('Account', null, 'Overview');
  }, []);

  const dispatch = useDispatch();
  const subscription = useSelector((state) => state.subscription, shallowEqual);
  const user = useSelector((state) => state.user, shallowEqual);
  const overview = useSelector((state) => state.overview, shallowEqual);
  const paymentAlert = useSelector((state) => state.paymentAlert, shallowEqual);
  const { companyMap } = subscription;
  const { alert, message } = paymentAlert;
  /**
   * Used for one-click renewals of expiring subscriptions.
   */
  const renewSubscriptionHandler = useCallback(
    (sub) => {
      const { planId } = sub;
      dispatch(renew(sub));

      trackEvent('Clicked Renew Subscription', {
        location: 'account-overview',
        plan: planId,
      });
    },
    [dispatch],
  );

  return (
    <div className="overview__container">
      <Helmet title={en.account.overview.meta_title} />
      {subscription.isRenewingError && (
        <StatusError statusErrorMessage={subscription.renewalErrorMessage} />
      )}
      <div>{alert ? <FailedPaymentMessage message={message} /> : null}</div>
      <div className="overview__three__column">
        <UserCard user={user} />
        <SubscriptionSummaryCard subscription={subscription} companyMap={companyMap} />
      </div>

      <div className="overview__nine__column">
        <PromoCard
          overview={overview}
          renewHandler={renewSubscriptionHandler}
          subscription={subscription}
          companyMap={companyMap}
        />
      </div>
    </div>
  );
};

export default Overview;
