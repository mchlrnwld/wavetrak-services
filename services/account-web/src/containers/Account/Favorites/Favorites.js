import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { trackNavigatedToPage } from '@surfline/web-common';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import classNames from 'classnames';
import { arrayMove } from 'react-sortable-hoc';
import { SortableFavoriteList } from '../../../components';
import en from '../../../international/translations/en';
import config from '../../../config';
import userFavoritePropType from '../../../propTypes/userFavoritePropType';
import {
  resetMessages,
  removeFavorite,
  updateFavoritesOrder,
} from '../../../redux/Account/favorites';

class Favorites extends Component {
  componentDidMount() {
    trackNavigatedToPage('Account', 'Favorites');
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(resetMessages());
  }

  onSortEnd = ({ oldIndex, newIndex }) => {
    const { userFavorites, dispatch } = this.props;

    const updatedOrder = arrayMove(userFavorites, oldIndex, newIndex);
    dispatch(updateFavoritesOrder(updatedOrder));
  };

  handleRemove = (evt, favorite) => {
    const { dispatch } = this.props;
    evt.preventDefault();
    dispatch(removeFavorite(favorite));
  };

  render() {
    const {
      user,
      userFavorites,
      updatingFavorites,
      updateFavoritesSuccessMessage,
      updateFavoritesErrorMessage,
    } = this.props;

    const cardClass = classNames({
      favorites__body__container__card: true,
      favorites__body__container__card__no__favs: !userFavorites.length,
    });

    const errorAlertStyle = updateFavoritesErrorMessage
      ? 'favorites__error__alert'
      : 'favorites__error__alert__hidden';

    const successAlertStyle = updateFavoritesSuccessMessage
      ? 'favorites__success__alert'
      : 'favorites__success__alert__hidden';

    const noFavsWelcomeMessage = user
      ? `${en.favorites.no_favs_title} ${user.firstName},`
      : `${en.favorites.no_favs_title},`;

    return (
      <div className="favorites">
        <Helmet title={en.favorites.meta_title[config.company]} />
        <div className="favorites__heading__container">
          <div className="favorites__heading">{en.favorites.title}</div>
        </div>
        <div className="favorites__body__container">
          <div className={updateFavoritesSuccessMessage ? successAlertStyle : errorAlertStyle}>
            {updateFavoritesSuccessMessage || updateFavoritesErrorMessage}
          </div>
          <a className="favorites__body__container__link" href="/setup/favorite-surf-spots/add">
            Add Favorites
          </a>
          <div className={cardClass}>
            {userFavorites.length ? (
              <SortableFavoriteList
                favorites={userFavorites}
                onSortEnd={this.onSortEnd}
                handleRemove={(evt, fav) => this.handleRemove(evt, fav)}
                pressDelay={100}
                helperClass="draggingFavorite"
                updatingFavorites={updatingFavorites}
                useWindowAsScrollContainer
              />
            ) : (
              <div>
                <h2>{noFavsWelcomeMessage}</h2>
                <p>{en.favorites.no_favs_message}</p>
                <a href="/surf-reports-forecasts-cams-map">{en.favorites.no_favs_button}</a>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

Favorites.propTypes = {
  user: PropTypes.shape({
    firstName: PropTypes.string,
  }).isRequired,
  userFavorites: PropTypes.arrayOf({
    userFavoritePropType,
  }).isRequired,
  updatingFavorites: PropTypes.bool,
  updateFavoritesErrorMessage: PropTypes.string,
  updateFavoritesSuccessMessage: PropTypes.string,
  dispatch: PropTypes.func.isRequired,
};

Favorites.defaultProps = {
  updatingFavorites: false,
  updateFavoritesErrorMessage: '',
  updateFavoritesSuccessMessage: '',
};

export default connect((state) => ({
  user: state.user,
  userFavorites: state.favorites.userFavorites,
  updatingFavorites: state.favorites.updatingFavorites,
  updateFavoritesErrorMessage: state.favorites.updateFavoritesErrorMessage,
  updateFavoritesSuccessMessage: state.favorites.updateFavoritesSuccessMessage,
}))(Favorites);
