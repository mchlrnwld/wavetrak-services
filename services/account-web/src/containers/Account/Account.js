import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as queryString from 'query-string';
import { connect } from 'react-redux';
import { MobileNavigation, DesktopNavigation, BrandIcons } from '../../components';
import trackCampaign from '../../utils/trackCampaign';
import config from '../../config';
import en from '../../international/translations/en';
import brandToString from '../../utils/brandToString';

class Account extends Component {
  constructor(props) {
    super(props);
    const { subscription } = props;
    const subscriptionTitle = subscription.hasMultipleSubscriptions
      ? 'SUBSCRIPTIONS'
      : 'SUBSCRIPTION';
    this.accountChildRoutes = [
      { name: 'OVERVIEW', path: '/account/overview' },
      { name: 'EDIT PROFILE', path: '/account/edit-profile' },
      { name: 'CHANGE PASSWORD', path: '/account/change-password' },
      { name: subscriptionTitle, path: '/account/subscription' },
    ];
    if (config.company === 'sl')
      this.accountChildRoutes.splice(2, 0, { name: 'FAVORITES', path: '/account/favorites' });
    if (config.company === 'sl')
      this.accountChildRoutes.splice(3, 0, { name: 'SETTINGS', path: '/account/edit-settings' });
  }

  componentDidMount() {
    const { user, router } = this.props;
    if (window?.analytics) {
      if (user && user._id) {
        window.analytics.identify(user._id);
      }
      const query = queryString.parse(router.location.search);
      if (query) {
        trackCampaign(query);
      }
    }
  }

  render() {
    const { children, router, referrer } = this.props;
    return (
      <div>
        <div className="account-header__container">
          <div className="account-header__sub_container">
            <div className="account-header-go-back">
              <a
                className="account-header-go-back__link"
                href={en.account.header.go_back_link(referrer, brandToString())}
              >
                {en.account.header.go_back_text(brandToString())}
              </a>
            </div>
            <BrandIcons theme="account" />
          </div>
        </div>
        <div className="account-subnavigation__container">
          <div className="account-subnavigation__container__list">
            <MobileNavigation childRoutes={this.accountChildRoutes} router={router} />
            <DesktopNavigation childRoutes={this.accountChildRoutes} />
          </div>
        </div>
        <div className="account-content">{children}</div>
      </div>
    );
  }
}

Account.propTypes = {
  subscription: PropTypes.shape({ hasMultipleSubscriptions: PropTypes.bool }),
  children: PropTypes.node,
  router: PropTypes.shape({
    location: PropTypes.shape({
      search: PropTypes.string,
    }),
  }).isRequired,
  referrer: PropTypes.string,
  user: PropTypes.shape({
    _id: PropTypes.string,
  }),
};

Account.defaultProps = {
  subscription: null,
  children: null,
  referrer: null,
  user: null,
};

export default connect((state) => {
  return {
    user: state.user,
    router: state.router,
    subscription: state.subscription,
    referrer: state.referrer,
  };
})(Account);
