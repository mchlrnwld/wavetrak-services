import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Elements, StripeProvider } from 'react-stripe-elements';
import { trackEvent, trackNavigatedToPage } from '@surfline/web-common';
import en from '../../../international/translations/en';
import config from '../../../config';
import { openCCForm } from '../../../redux/Funnel/subscription';
import { renew, changeSubscriptionPlan } from '../../../redux/actions/subscription';
import {
  addCard,
  toggleShowNewCardForm,
  updateCard,
  deleteCard,
  setDefaultCardSource,
  setShowEditCardForm,
} from '../../../redux/actions/cards';
import { openCardDetail } from '../../../redux/Account/subscription';
import getBillingInterval from '../../../utils/getBillingInterval';
import {
  PremiumSubscriptions,
  NoPremiumSubscriptions,
  SubscriptionCards,
  PaymentWarningsBanner,
} from '../../../components';

class Subscription extends Component {
  constructor(props) {
    super(props);

    const { subscription } = props;
    this.state = {
      addCardSubmitting: subscription.submitting || false,
    };
  }

  componentDidMount() {
    const { failedPaymentAttempts, doOpenCCForm } = this.props;

    trackNavigatedToPage('Account', {}, 'Subscription Details');

    if (failedPaymentAttempts === true) {
      doOpenCCForm();
    }
  }

  componentDidUpdate = (prevProps) => {
    const {
      subscription: { submitting },
    } = this.props;

    const submittingChanged = submitting !== prevProps.subscription.submitting;
    if (submittingChanged) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        addCardSubmitting: submitting || false,
      });
    }
  };

  analyticsCancelClicked = () => {
    trackEvent('Clicked Cancel Your Subscription', { location: 'subscription-details' });
  };

  render() {
    const {
      acctSubscription,
      doAddCard,
      doUpdateCard,
      doDeleteCard,
      doSetDefaultCard,
      doToggleShowNewCardForm,
      doSetCardToEdit,
      cards,
      showCardForm,
      doRenewSubscription,
      subscription,
      doChangePlan,
      doOpenCardDetail,
      paymentAlert,
    } = this.props;

    const { addCardSubmitting } = this.state;

    const {
      subscriptions,
      availablePlans,
      isRenewing,
      isRenewingSuccess,
      isUpdatingPlan,
      subTypeIAPOnly,
      nonStripeSubType,
      companyMap,
      hasActiveSubscription,
      hasMultipleSubscriptions,
      stripeCustomerId,
      error,
    } = subscription;

    const subsciptionTitle = hasMultipleSubscriptions
      ? en.subscription.title.subscriptions
      : en.subscription.title.subscription;

    const { defaultSource } = cards;
    const { alert, message: alertMessage } = paymentAlert;
    return (
      <div className="subscription">
        <Helmet title={en.subscription.meta_title[config.company]} />
        <div className="subscription__heading__container">
          <div className="subscription__heading">{subsciptionTitle}</div>
        </div>
        {alert ? <PaymentWarningsBanner message={alertMessage} /> : null}
        {!hasActiveSubscription ? <NoPremiumSubscriptions /> : null}

        {hasActiveSubscription
          ? companyMap.map((brand) =>
              subscriptions[brand].map((sub) => {
                const periodEnd = sub.periodEnd || '';
                const brandPlans = availablePlans[brand];
                return (
                  <PremiumSubscriptions
                    periodEnd={periodEnd}
                    sub={sub}
                    brand={brand}
                    brandPlans={brandPlans}
                    subscriptions={subscriptions}
                    updating={isRenewing}
                    success={isRenewingSuccess}
                    isUpdatingPlan={isUpdatingPlan}
                    error={error}
                    getBillingInterval={getBillingInterval}
                    key={brand}
                    changePlan={(currentSubscription, planId) =>
                      doChangePlan(currentSubscription, planId)
                    }
                    renewSubscription={doRenewSubscription}
                  />
                );
              }),
            )
          : null}

        <StripeProvider apiKey={config.stripe.publishableKey}>
          <div>
            <Elements>
              <SubscriptionCards
                subscription={subscription}
                subscriptionType={nonStripeSubType}
                subTypeIAPOnly={subTypeIAPOnly}
                stripeCustomerId={stripeCustomerId}
                acctSubscription={acctSubscription}
                openCardDetailHandler={doOpenCardDetail}
                addCardSubmitting={addCardSubmitting}
                doAddCard={doAddCard}
                doUpdateCard={doUpdateCard}
                doDeleteCard={doDeleteCard}
                doSetDefaultCard={doSetDefaultCard}
                doToggleShowNewCardForm={doToggleShowNewCardForm}
                doSetCardToEdit={doSetCardToEdit}
                showCardForm={showCardForm}
                cards={cards}
              />
            </Elements>
          </div>
        </StripeProvider>

        {/* <div className="subscription__cards__wrapper">
          {archivedSubscriptions.length > 0 ? (
            <PreviousSubscriptions
              subscription={this.props.subscription}
              getBillingInterval={getBillingInterval}
            />
          ) :
            null
          }
        </div> */}
      </div>
    );
  }
}

Subscription.propTypes = {
  subscription: PropTypes.shape({
    subscriptions: PropTypes.arrayOf(PropTypes.shape()),
    availablePlans: PropTypes.shape({}),
    isRenewing: PropTypes.bool,
    subTypeIAPOnly: PropTypes.bool,
    nonStripeSubType: PropTypes.bool,
    companyMap: PropTypes.arrayOf(PropTypes.string),
    hasActiveSubscription: PropTypes.bool,
    hasMultipleSubscriptions: PropTypes.bool,
    stripeCustomerId: PropTypes.string,
    error: PropTypes.oneOf([PropTypes.string, PropTypes.bool]),
    isRenewingSuccess: PropTypes.bool,
    isUpdatingPlan: PropTypes.bool,
  }).isRequired,
};

export default connect(
  (state) => ({
    subscription: state.subscription,
    acctSubscription: state.acctSubscription,
    auth: state.auth,
    cards: state.cards,
    warnings: state.warnings,
    paymentAlert: state.paymentAlert,
  }),
  (dispatch) => ({
    doOpenCCForm: () => dispatch(openCCForm()),
    doToggleShowNewCardForm: (showCardForm) => dispatch(toggleShowNewCardForm(showCardForm)),
    doAddCard: (source) => dispatch(addCard(source)),
    doUpdateCard: (cardDetails) => dispatch(updateCard(cardDetails)),
    doDeleteCard: (cardId) => dispatch(deleteCard(cardId)),
    doSetDefaultCard: (cardId) => dispatch(setDefaultCardSource(cardId)),
    doSetCardToEdit: (cardId) => dispatch(setShowEditCardForm(cardId)),
    doOpenCardDetail: (cardId) => dispatch(openCardDetail(cardId)),
    doChangePlan: (subscription, planId) => dispatch(changeSubscriptionPlan(subscription, planId)),
    doRenewSubscription: (subsInfo) => dispatch(renew(subsInfo)),
  }),
)(Subscription);
