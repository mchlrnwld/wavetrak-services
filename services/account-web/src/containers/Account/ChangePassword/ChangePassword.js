import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { reduxForm } from 'redux-form';
import { trackNavigatedToPage, trackEvent } from '@surfline/web-common';
import { ChangePasswordForm } from '../../../components';
import en from '../../../international/translations/en';
import config from '../../../config';
import changePasswordValidate from './changePasswordValidate';
import { changePassword, resetState } from '../../../redux/Account/changePassword';

class ChangePassword extends Component {
  componentDidMount() {
    trackNavigatedToPage('Account', 'Change Password');
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch(resetState());
  }

  onSubmit = (data) => {
    const { dispatch } = this.props;
    trackEvent('Clicked Update Password', {
      location: 'change-password',
    });
    dispatch(changePassword(data));
  };

  enableSubmit = () => {
    const { dispatch, passwordSubmitSucceeded } = this.props;
    if (passwordSubmitSucceeded) {
      dispatch(resetState());
    }
  };

  render() {
    const {
      fields,
      handleSubmit,
      passwordSubmitting,
      passwordSubmitSucceeded,
      passwordErrorMessage,
      passwordSuccessMessage,
    } = this.props;

    return (
      <div className="changePassword">
        <Helmet title={en.change_password.meta_title[config.company]} />
        <div className="changePassword__heading__container">
          <div className="changePassword__heading">{en.change_password.title}</div>
        </div>
        <div className="changePassword__body__container">
          <div className="account__card">
            <ChangePasswordForm
              fields={fields}
              onSubmit={handleSubmit(this.onSubmit)}
              enableSubmit={this.enableSubmit}
              passwordSubmitSucceeded={passwordSubmitSucceeded}
              passwordSubmitting={passwordSubmitting}
              passwordErrorMessage={passwordErrorMessage}
              passwordSuccessMessage={passwordSuccessMessage}
            />
          </div>
        </div>
      </div>
    );
  }
}

ChangePassword.propTypes = {
  passwordSubmitSucceeded: PropTypes.bool.isRequired,
  passwordSubmitting: PropTypes.bool.isRequired,
  passwordErrorMessage: PropTypes.string.isRequired,
  passwordSuccessMessage: PropTypes.string.isRequired,
  dispatch: PropTypes.func.isRequired,
  fields: PropTypes.shape({}).isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm(
  {
    form: 'changePassword',
    fields: ['currentPassword', 'newPassword', 'confirmPassword', '_id'],
    validate: changePasswordValidate,
    destroyOnUnmount: true,
  },
  (state) => ({
    initialValues: state.user,
    passwordSubmitting: state.password.changePasswordSubmitting,
    passwordSubmitSucceeded: state.password.changePasswordSuccess,
    passwordErrorMessage: state.password.passwordErrorMessage,
    passwordSuccessMessage: state.password.passwordSuccessMessage,
  }),
)(ChangePassword);
