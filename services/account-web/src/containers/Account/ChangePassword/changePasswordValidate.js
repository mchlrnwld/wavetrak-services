import memoize from 'lru-memoize';
import { createValidator, required, minLength, maxLength, match } from 'utils/validation';

const MIN_PASSWORD_LENGTH = 6;
const MAX_PASSWORD_LENGTH = 50;

const changePasswordValidation = createValidator({
  currentPassword: [required],
  newPassword: [required, minLength(MIN_PASSWORD_LENGTH), maxLength(MAX_PASSWORD_LENGTH)],
  confirmPassword: [required, match('newPassword')],
});

export default memoize(10)(changePasswordValidation);
