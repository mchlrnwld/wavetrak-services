/* eslint no-unused-vars: 0 */
import PropTypes from 'prop-types';

import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import {BrandIcons} from 'components';
import en from '../../../international/translations/en';


export default class EnterCode extends Component {

  render() {
    const notify = en.forgot_password.enter_code.notify;
    const info = en.forgot_password.enter_code.info;

    return (
      <div className="forgot-password__card">
        <h4 className="enterCode_container_title">{en.forgot_password.enter_code.title}</h4>
        <p className="enterCode_container_notify_message">{notify}</p>
        <p className="enterCode_container_info_message">{info}</p>
        <Helmet title={en.forgot_password.enter_code.meta_title}/>
        <BrandIcons theme="desktop" />
      </div>
    );
  }
}
