import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { initialize } from 'redux-form';
import { trackEvent } from '@surfline/web-common';
import * as forgotPasswordActions from '../../../redux/Funnel/forgotPassword';
import { ResetPasswordForm, BrandIcons } from '../../../components';
import en from '../../../international/translations/en';
import saveCookies from '../../../utils/setLoginCookie';
import performRedirect from '../../../utils/performRedirect';
import { clearUser } from '../../../redux/actions/forgotPassword';
import resetPassword from '../../../utils/resetPassword';
import resetTokenValidationWrapper from '../ResetTokenValidation';

export const submit = (values, dispatch) => {
  return new Promise((resolve, reject) => {
    resetPassword(values, dispatch)
      .then((response) => {
        trackEvent('Signed In', {
          method: 'forgot password flow',
        });

        const accessToken = response.body.access_token;
        const refreshToken = response.body.refresh_token;
        const maxAge = 2592000;
        const refreshMaxAge = 31536000;

        saveCookies(accessToken, refreshToken, maxAge, refreshMaxAge).then(() => {
          dispatch(forgotPasswordActions.setPassword());
          performRedirect();
        });

        resolve();
      })
      .catch((error) => {
        reject(error);
      });
  });
};

class SetPassword extends Component {
  constructor(props) {
    super(props);
    this.submitSuccess = false;
  }

  componentDidUpdate(prevProps) {
    const { submitSuccess } = this.props;
    if (!prevProps.submitSuccess && submitSuccess) {
      this.submitSuccess = submitSuccess;
    }
  }

  componentWillUnmount() {
    this.clearForgotPassword();
  }

  clearForgotPassword = () => {
    const { doClearUser } = this.props;
    doClearUser();
  };

  render() {
    const { user } = this.props;

    return (
      <div className="forgot-password__card">
        <h1 className="h4">{en.forgot_password.set_password.title}</h1>
        <Helmet title={en.forgot_password.set_password.meta_title} />
        <ResetPasswordForm
          onSubmit={this.handleSubmit}
          userId={user && user.userId}
          resetCode={user && user.resetCode}
          submitSuccess={this.submitSuccess}
        />
        <BrandIcons theme="desktop" />
      </div>
    );
  }
}

SetPassword.propTypes = {
  user: PropTypes.shape({ userId: PropTypes.string, resetCode: PropTypes.string }).isRequired,
  submitSuccess: PropTypes.bool.isRequired,
  doClearUser: PropTypes.func.isRequired,
};

export default compose(
  connect(
    (state) => ({
      forgotPassword: state.forgotPassword,
      user: state.forgotPassword.user,
      submitSuccess: state.forgotPassword.passwordSubmitSuccess,
    }),
    (dispatch) => ({
      ...forgotPasswordActions,
      initialize,
      doClearUser: () => dispatch(clearUser()),
    }),
  ),
  resetTokenValidationWrapper,
)(SetPassword);
