import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { isValidated, validate } from '../../redux/actions/forgotPassword';

const resetTokenValidationWrapper = (WrappedComponent) => {
  class ResetTokenValidation extends Component {
    constructor(props) {
      super(props);

      this.state = {
        tokenValidated: false,
        redirect: false,
      };
    }

    componentDidMount() {
      // Validate the user before rendering the route
      this.requireValidation();
    }

    checkValidation = () => {
      const { forgotPassword } = this.props;
      if (!forgotPassword.isValidated) {
        return this.setState({ redirect: '/forgot-password' });
      }
      return this.setState({
        tokenValidated: true,
      });
    };

    requireValidation = async () => {
      const { doValidate } = this.props;
      if (!isValidated(this.props)) {
        await doValidate(this.props);
        this.checkValidation();
      }
      return this.setState({ tokenValidated: true });
    };

    render() {
      const { tokenValidated, redirect } = this.state;
      if (redirect) {
        return <Redirect to={redirect} />;
      }
      return tokenValidated && <WrappedComponent {...this.props} />;
    }
  }

  ResetTokenValidation.propTypes = {
    forgotPassword: PropTypes.shape({
      isValidated: PropTypes.bool,
    }).isRequired,
    doValidate: PropTypes.func.isRequired,
  };

  return connect(
    (state) => ({
      forgotPassword: state.forgotPassword,
      user: state.forgotPassword.user,
      submitSuccess: state.forgotPassword.passwordSubmitSuccess,
    }),
    (dispatch) => ({
      doValidate: (location) => dispatch(validate(location)),
    }),
  )(ResetTokenValidation);
};

export default resetTokenValidationWrapper;
