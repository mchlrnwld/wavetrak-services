import { Route, Switch } from 'react-router';
import React from 'react';
import { EnterCode, EnterEmail, SetPassword } from '../../containers';

export default () => (
  <div>
    <div className="forgot-password__container">
      <Switch>
        <Route path="/forgot-password/set-password" component={SetPassword} />
        <Route path="/forgot-password/email-sent" component={EnterCode} />
        <Route component={EnterEmail} />
      </Switch>
    </div>
  </div>
);
