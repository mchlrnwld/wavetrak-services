import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { initialize } from 'redux-form';
import { Helmet } from 'react-helmet';
import * as forgotPasswordActions from '../../../redux/Funnel/forgotPassword';
import { EmailForm, BrandIcons } from '../../../components';
import en from '../../../international/translations/en';
import config from '../../../config';
import { clearUser, requestReset } from '../../../redux/actions/forgotPassword';
import forgotPassword from '../../../utils/forgotPassword';

class EnterEmail extends Component {
  componentDidMount() {
    this.clearForgotPassword();
  }

  componentDidUpdate(prevProps) {
    const { user, pushState } = this.props;
    if (!prevProps.user && user) {
      pushState('/forgot-password/email-sent');
    } else if (prevProps.user && !user) {
      pushState('/');
    }
  }

  submitHandler = async (values) => {
    const { doRequestReset } = this.props;
    try {
      await forgotPassword(values);
      const { email } = values;
      return doRequestReset(email);
    } catch (error) {
      return error;
    }
  };

  clearForgotPassword = () => {
    const { doClearUser } = this.props;
    doClearUser();
  };

  render() {
    const { user, submitting, submitSuccess } = this.props;
    return (
      <div className="forgot-password__card">
        <h1 className="h4">{en.forgot_password.enter_email.title}</h1>
        <Helmet title={en.forgot_password.enter_email.meta_title} />
        <EmailForm
          submitting={submitting}
          submitHandler={this.submitHandler}
          submitSuccess={submitSuccess}
          user={user}
        />
        <p className="enter_email_container_footer_message">
          <a
            className="enter_email_container_footer_message_link"
            href={config.needHelpUrl}
            target="_blank"
            rel="noopener noreferrer"
          >
            {en.forgot_password.enter_email.need_help_message}
          </a>
        </p>
        <BrandIcons theme="desktop" />
      </div>
    );
  }
}

EnterEmail.propTypes = {
  user: PropTypes.shape(),
  pushState: PropTypes.func.isRequired,
  doRequestReset: PropTypes.func.isRequired,
  doClearUser: PropTypes.func.isRequired,
  submitting: PropTypes.bool,
  submitSuccess: PropTypes.bool,
};

EnterEmail.defaultProps = {
  user: null,
  submitting: false,
  submitSuccess: false,
};

export default connect(
  (state) => ({
    user: state.forgotPassword.user,
    submitting: state.forgotPassword.submitting,
    submitSuccess: state.forgotPassword.submitSuccess,
  }),
  (dispatch) => ({
    ...forgotPasswordActions,
    initialize,
    pushState: (route) => dispatch(push(route)),
    doClearUser: () => dispatch(clearUser()),
    doRequestReset: (email) => dispatch(requestReset(email)),
  }),
)(EnterEmail);
