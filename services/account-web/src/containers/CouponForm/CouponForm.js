import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Alert, Button } from '@surfline/quiver-react';
import { fetchCoupon, toggleCouponForm } from '../../redux/actions/coupon';

class CouponForm extends Component {
  constructor(props) {
    super(props);
    this.state = { coupon: '' };
  }

  handleChange = (event) => {
    this.setState({ coupon: event.target.value });
  };

  handleSubmit = async (event) => {
    const { doFetchCoupon } = this.props;
    event.preventDefault();
    await doFetchCoupon(this.state.coupon);
  };

  cancelCoupon = () => {
    const { doToggleCouponForm } = this.props;
    this.setState({ coupon: '' });
    doToggleCouponForm();
  };

  render() {
    const {
      doToggleCouponForm,
      isFormOpen,
      couponSubmitting,
      couponError,
      couponSuccess
    } = this.props;

    if (couponSuccess) {
      return (
        <div>
          <Alert
            style={{ margin: '16px 0 0 0', textAlign: 'center' }}
            type="success"
          >
            {couponSuccess}
          </Alert>
        </div>
      );
    }

    return (
      <div className="coupon-form">
        {isFormOpen ? (
          <div>
            {couponError ? (
              <Alert
                style={{ marginBottom: '8px', textAlign: 'center' }}
                type="error"
              >
                {couponError}
              </Alert>)
              : null
            }
            <form onSubmit={this.handleSubmit}>
              <input
                type="text"
                name="coupon"
                value={this.state.coupon}
                onChange={this.handleChange}
                placeholder="Discount Code"
              />
              <div className="coupon-form__button-container">
                <Button loading={couponSubmitting}>Apply</Button>
                <Button
                  type="info"
                  onClick={this.cancelCoupon}
                >
                  Cancel
                </Button>
              </div>
            </form>
          </div>
        ) : (
          <p className="coupon-form__message" onClick={() => doToggleCouponForm()}>Have a discount code?</p>
        )}
      </div>
    );
  }
}

export default connect(
  state => ({
    isFormOpen: state.coupon.isFormOpen,
    couponSubmitting: state.coupon.couponSubmitting,
    couponError: state.coupon.couponError,
    couponSuccess: state.coupon.couponSuccess
  }),
  dispatch => ({
    doFetchCoupon: coupon => dispatch(fetchCoupon(coupon)),
    doToggleCouponForm: () => dispatch(toggleCouponForm()),
  }),
)(CouponForm);
