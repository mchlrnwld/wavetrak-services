import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Elements, StripeProvider } from 'react-stripe-elements';
import { CheckoutForm, CouponForm, DuplicateCard } from '..';
import { startSubmitSpinner } from '../../redux/actions/interactions';
import { submitSubscription } from '../../redux/actions/subscription';
import { toggleSelectedPlanId } from '../../redux/actions/plans';
import { setSelectedCardSource, toggleShowCards } from '../../redux/actions/cards';
import { getAccessToken } from '../../selectors/auth';
import { getTrialEligibility } from '../../selectors/entitlements';
import { getSelectedPlanId, getPremiumPlans, getTrialPeriodDays } from '../../selectors/plans';
import { getCreditCards, getDefaultSource, getSelectedCard } from '../../selectors/cards';
import config from '../../config';

const promoCodesEnabled = config.funnelConfig ? config.funnelConfig.promoCodesEnabled : null;
class PaymentWrapper extends Component {
  constructor(props) {
    super(props);
    this.handlePromoCodeChange = this.handlePromoCodeChange.bind(this);
    this.state = { promoCode: null };
  }

  componentDidMount() {
    const { viewedCheckout } = this.props;
    viewedCheckout();
  }

  handlePromoCodeChange = (promoCode) => {
    this.setState({ promoCode });
  };

  submitPaymentForm = async (values) => {
    const {
      doSubmitSubscription,
      defaultCardId,
      trialEligible,
      trialPeriodDays,
      accessToken,
      coupon,
      formLocation,
      shouldRedirectToOnboarding,
    } = this.props;
    const { promoCode } = this.state;
    const promotionId = config.funnelConfig ? config.funnelConfig._id : null;
    const subscriptionValues = {
      ...values,
      trialEligible: coupon ? false : trialEligible,
      trialPeriodDays,
      location: formLocation,
      defaultCardId,
      accessToken,
      coupon,
      promotionId,
      promoCodesEnabled,
      promoCode,
      shouldRedirectToOnboarding,
    };
    await doSubmitSubscription(subscriptionValues);
  };

  render() {
    const {
      plans,
      cards,
      selectedCard,
      showCards,
      submitSuccess,
      submitting,
      selectedPlanId,
      doToggleSelectedPlanId,
      doStartSubmitSpinner,
      trialEligible,
      trialPeriodDays,
      coupon,
      subscriptionError,
      cardError,
      showDuplicateCard,
      doSetSelectedCardSource,
      doToggleShowCards,
      showCouponForm,
      children,
      enableFunnelPlanTest,
    } = this.props;
    const { promoCode } = this.state;
    if (showDuplicateCard) {
      return <DuplicateCard submitting={submitting} submitPayment={this.submitPaymentForm} />;
    }

    return (
      <div>
        {children}
        <StripeProvider apiKey={config.stripe.publishableKey}>
          <div>
            <Elements>
              <CheckoutForm
                submitting={submitting}
                submitSuccess={submitSuccess}
                paymentHandler={this.submitPaymentForm}
                triggerSubmit={doStartSubmitSpinner}
                plans={plans}
                cards={cards}
                selectedCard={selectedCard}
                showCards={showCards}
                trialEligible={trialEligible}
                trialPeriodDays={trialPeriodDays}
                selectedPlanId={selectedPlanId}
                toggleSelectedPlanId={doToggleSelectedPlanId}
                error={subscriptionError || cardError}
                doSetSelectedCardSource={doSetSelectedCardSource}
                doToggleShowCards={doToggleShowCards}
                promoCodesEnabled={promoCodesEnabled}
                promoCode={promoCode}
                handlePromoCodeChange={this.handlePromoCodeChange}
                coupon={coupon}
                enableFunnelPlanTest={enableFunnelPlanTest}
              />
            </Elements>
          </div>
        </StripeProvider>
        {showCouponForm ? <CouponForm /> : null}
      </div>
    );
  }
}

PaymentWrapper.propTypes = {
  viewedCheckout: PropTypes.func.isRequired,
  doSubmitSubscription: PropTypes.func.isRequired,
  defaultCardId: PropTypes.string.isRequired,
  trialEligible: PropTypes.bool,
  trialPeriodDays: PropTypes.number.isRequired,
  accessToken: PropTypes.string,
  coupon: PropTypes.string,
  formLocation: PropTypes.string.isRequired,
  shouldRedirectToOnboarding: PropTypes.bool,
  plans: PropTypes.instanceOf(Array).isRequired,
  cards: PropTypes.instanceOf(Array).isRequired,
  selectedCard: PropTypes.string.isRequired,
  showCards: PropTypes.bool,
  submitSuccess: PropTypes.bool,
  submitting: PropTypes.bool,
  selectedPlanId: PropTypes.string.isRequired,
  doToggleSelectedPlanId: PropTypes.func.isRequired,
  doStartSubmitSpinner: PropTypes.func.isRequired,
  subscriptionError: PropTypes.string,
  cardError: PropTypes.string,
  showDuplicateCard: PropTypes.bool,
  enableFunnelPlanTest: PropTypes.bool,
  doSetSelectedCardSource: PropTypes.func.isRequired,
  doToggleShowCards: PropTypes.func.isRequired,
  showCouponForm: PropTypes.bool,
  children: PropTypes.node,
};

PaymentWrapper.defaultProps = {
  subscriptionError: null,
  cardError: null,
  showDuplicateCard: null,
  enableFunnelPlanTest: null,
  children: null,
  showCards: null,
  submitSuccess: null,
  submitting: null,
  trialEligible: null,
  accessToken: null,
  coupon: null,
  shouldRedirectToOnboarding: null,
  showCouponForm: true,
};

export default connect(
  (state) => ({
    accessToken: getAccessToken(state),
    trialEligible: getTrialEligibility(state),
    trialPeriodDays: getTrialPeriodDays(state),
    selectedPlanId: getSelectedPlanId(state),
    cards: getCreditCards(state),
    plans: getPremiumPlans(state),
    defaultCardId: getDefaultSource(state),
    selectedCard: getSelectedCard(state),
    submitting: state.interactions.submitting,
    submitSuccess: state.user.submitSuccess,
    showCards: state.cards.showCards,
    showDuplicateCard: state.duplicateCard.showDuplicateCard,
    subscriptionError: state.subscription.error,
    cardError: state.cards.error,
    coupon: state.coupon.coupon,
  }),
  (dispatch) => ({
    doToggleSelectedPlanId: (planId) => dispatch(toggleSelectedPlanId(planId)),
    doStartSubmitSpinner: () => dispatch(startSubmitSpinner()),
    doSubmitSubscription: (values) => dispatch(submitSubscription(values)),
    doSetSelectedCardSource: (cardId) => dispatch(setSelectedCardSource(cardId)),
    doToggleShowCards: (showCards) => dispatch(toggleShowCards(showCards)),
  }),
)(PaymentWrapper);
