import React, { Component } from 'react';
import { connect } from 'react-redux';
import { injectStripe, PaymentRequestButtonElement } from 'react-stripe-elements';

class PaymentRequestSubscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      digitalWallet: false,
      paymentRequest: null,
    };
    this.handlePaymentRequestButtonClick = this.handlePaymentRequestButtonClick.bind(this);
  }

  async componentDidMount() {
    await this.setupPaymentRequest();
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.selectedPlanId !== prevProps.selectedPlanId ||
      this.props.coupon !== prevProps.coupon
    ) {
      this.updatePaymentRequest(this.props.selectedPlanId);
    }
  }

  async setupPaymentRequest() {
    const { selectedPlanId, plans } = this.props;
    const { amount, currency } = plans.find(({ id }) => id === selectedPlanId);
    const cost = amount / 100;
    const label = selectedPlanId.includes('annual')
      ? `Billed $${cost} once a year`
      : 'Billed month-to-month';
    const paymentRequestObject = {
      country: 'US',
      currency,
      total: {
        label,
        amount,
      },
    };
    const paymentRequest = this.props.stripe.paymentRequest(paymentRequestObject);
    const digitalWallet = await paymentRequest.canMakePayment();
    this.setState({ digitalWallet, paymentRequest });
  }

  async updatePaymentRequest(selectedPlanId) {
    const { plans } = this.props;
    const { paymentRequest } = this.state;
    const { amount, currency } = plans.find(({ id }) => id === selectedPlanId);
    const cost = amount / 100;
    const label = selectedPlanId.includes('annual')
      ? `Billed $${cost} once a year`
      : 'Billed month-to-month';
    const paymentRequestObject = {
      currency,
      total: {
        label,
        amount,
      },
    };
    await paymentRequest.update(paymentRequestObject);
    const digitalWallet = await paymentRequest.canMakePayment();
    this.setState({ digitalWallet, paymentRequest });
  }

  handlePaymentRequestButtonClick = () => {
    const { paymentRequest, digitalWallet } = this.state;
    const { paymentHandler, selectedPlanId } = this.props;
    paymentRequest.on('token', async (ev) => {
      const { applePay } = digitalWallet;
      const paymentSource = applePay ? 'Apple Pay' : 'other';
      const paymentValues = {
        plan: selectedPlanId,
        source: ev.token,
        selectedCard: null,
        digitalWallet: paymentSource,
      };
      try {
        await paymentHandler(paymentValues);
        ev.complete('success');
      } catch (err) {
        ev.complete('fail');
      }
    });
  };

  render() {
    const { paymentRequest, digitalWallet } = this.state;
    return digitalWallet ? (
      <div>
        <PaymentRequestButtonElement
          paymentRequest={paymentRequest}
          className="checkout-form__container__digital_wallet__button"
          onClick={this.handlePaymentRequestButtonClick}
          style={{
            paymentRequestButton: {
              height: '48px',
            },
          }}
        />
      </div>
    ) : null;
  }
}

export default connect()(injectStripe(PaymentRequestSubscription));
