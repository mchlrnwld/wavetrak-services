import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { trackNavigatedToPage, trackEvent } from '@surfline/web-common';
import { fetchEntitlements } from '../../redux/actions/entitlements';
import { fetchFilteredPlans } from '../../redux/actions/plans';
import { fetchCards } from '../../redux/actions/cards';
import { toggleRegistrationForm } from '../../redux/actions/interactions';
import { getAccessToken } from '../../selectors/auth';
import { getPremiumStatus, getTrialEligibility } from '../../selectors/entitlements';
import { getPremiumPlans, getTrialPeriodDays } from '../../selectors/plans';
import { PaymentWrapper, RegistrationWrapper } from '..';
import {
  PromotionRailCTA,
  PromotionMobileRailCTA,
  LeftRail,
  RightRail,
  PremiumSuccess,
  PromotionHeader,
  RegistrationSubtitle,
  FunnelHelmet,
  ShowPromotionError,
  FacebookLogin,
} from '../../components';
import en from '../../international/translations/en';
import config from '../../config';

class Promotion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      registrationRendered: false,
      checkoutRendered: false,
      promotionId: config.funnelConfig ? config.funnelConfig._id : null,
    };
  }

  componentDidMount() {
    const { promotionId } = this.state;
    trackNavigatedToPage('Funnel', { promotionId }, 'Promotion', { Wootric: false });
  }

  viewedCheckout = () => {
    const { checkoutRendered, promotionId } = this.state;
    if (!checkoutRendered) {
      this.setState({ checkoutRendered: true });
      trackEvent('Viewed Checkout Step', {
        step: 2,
        promotionId,
      });
    }
  };

  viewedRegistration = () => {
    const { registrationRendered, promotionId } = this.state;
    if (!registrationRendered) {
      this.setState({ registrationRendered: true });
      trackEvent('Viewed Checkout Step', {
        step: 1,
        promotionId,
      });
    }
  };

  registrationSuccess = async () => {
    const { doFetchEntitlements, doFetchPlans, doFetchCards } = this.props;
    await doFetchEntitlements();
    await doFetchCards();
    await doFetchPlans();
  };

  renderRegistrationForm = () => {
    const { showLogin, doToggleForm } = this.props;
    return (
      <RegistrationWrapper
        showLogin={showLogin}
        formLocation="upgrade"
        viewedRegistration={this.viewedRegistration}
        successCallback={this.registrationSuccess}
      >
        <FunnelHelmet showLogin={showLogin} />
        <PromotionHeader hideSubtitle>
          <RegistrationSubtitle showLogin={showLogin} doToggleForm={doToggleForm} />
        </PromotionHeader>
        <FacebookLogin formLocation="upgrade" successCallback={this.registrationSuccess} />
      </RegistrationWrapper>
    );
  };

  renderPaymentForm = () => {
    const { isPremium, trialPeriodDays } = this.props;
    if (isPremium) return window.location.assign('/account/subscription');
    return (
      <PaymentWrapper
        formLocation="upgrade"
        viewedCheckout={this.viewedCheckout}
        showCouponForm={false}
      >
        <PromotionHeader isLoggedIn trialPeriodDays={trialPeriodDays} />
      </PaymentWrapper>
    );
  };

  renderPromotionFunnel = (subscriptionSuccess, accessToken, plans) => {
    return (
      <div>
        {subscriptionSuccess ? (
          <PremiumSuccess />
        ) : (
          <div>
            {accessToken && plans ? this.renderPaymentForm() : this.renderRegistrationForm()}
          </div>
        )}
      </div>
    );
  };

  render() {
    const { accessToken, plans, subscriptionSuccess } = this.props;
    const promotionFeatures = { ...en.promotionFeatures };
    const { funnelConfig } = config;
    if (funnelConfig) {
      funnelConfig.promotionFeatures = promotionFeatures;
    }
    return (
      <div className="upgrade-funnel">
        <PromotionMobileRailCTA funnelConfig={funnelConfig} />
        <LeftRail>
          <div className="upgrade-funnel__content">
            {funnelConfig.isActive ? (
              this.renderPromotionFunnel(subscriptionSuccess, accessToken, plans)
            ) : (
              <ShowPromotionError />
            )}
          </div>
        </LeftRail>
        <RightRail>
          <PromotionRailCTA funnelConfig={funnelConfig} />
        </RightRail>
      </div>
    );
  }
}

Promotion.propTypes = {
  doFetchEntitlements: PropTypes.func.isRequired,
  doFetchPlans: PropTypes.func.isRequired,
  doFetchCards: PropTypes.func.isRequired,
  showLogin: PropTypes.bool,
  doToggleForm: PropTypes.func.isRequired,
  accessToken: PropTypes.string,
  plans: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  subscriptionSuccess: PropTypes.bool,
  isPremium: PropTypes.bool,
  trialPeriodDays: PropTypes.number.isRequired,
};

Promotion.defaultProps = {
  showLogin: null,
  accessToken: null,
  subscriptionSuccess: null,
  isPremium: null,
};

export default connect(
  (state) => ({
    accessToken: getAccessToken(state),
    entitlements: state.entitlements,
    isPremium: getPremiumStatus(state),
    trialEligible: getTrialEligibility(state),
    trialPeriodDays: getTrialPeriodDays(state),
    plans: getPremiumPlans(state),
    subscriptionSuccess: state.subscription.success,
    showLogin: state.interactions.showLogin,
  }),
  (dispatch) => ({
    doToggleForm: (showLogin) => dispatch(toggleRegistrationForm(showLogin, 'promotion')),
    doFetchEntitlements: () => dispatch(fetchEntitlements()),
    doFetchPlans: () => dispatch(fetchFilteredPlans()),
    doFetchCards: () => dispatch(fetchCards()),
  }),
)(Promotion);
