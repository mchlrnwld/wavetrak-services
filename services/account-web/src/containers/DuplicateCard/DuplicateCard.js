/* eslint-disable no-nested-ternary */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button } from '@surfline/quiver-react';
import en from '../../international/translations/en';
import CreditCard from '../../components/CreditCard/CreditCard';
import { clearDuplicateCard } from '../../redux/actions/duplicateCard';
import { getPremiumPlans } from '../../selectors/plans';
import { getSelectedCard } from '../../selectors/cards';

class DuplicateCard extends Component {
  amountToFixed = () => {
    const { selectedPlanId, plans } = this.props;
    const { amount } = plans.find(({ id }) => id === selectedPlanId);
    return parseFloat(amount / 100).toFixed(2);
  }

  render() {
    const {
      submitting,
      source,
      error,
      submitPayment,
      doClearDuplicateCard,
      selectedPlanId,
      selectedCard
    } = this.props;

    return (
      <div className="duplicate-card">
        <p className="duplicate-card__description">
          {en.duplicateCard.description(this.amountToFixed())}
        </p>
        {error && <div className="duplicate-card__error">{error}</div>}
        <CreditCard card={source.card} />
        <Button
          loading={submitting}
          onClick={() => submitPayment({ plan: selectedPlanId, source, selectedCard })}
        >
          {en.duplicateCard.goPremium}
        </Button>
        <button
          onClick={doClearDuplicateCard}
        >
          {en.duplicateCard.goBack}
        </button>
      </div>
    );
  }
}

export default connect(
  state => ({
    selectedPlanId: state.duplicateCard.plan,
    selectedCard: getSelectedCard(state),
    plans: getPremiumPlans(state),
    source: state.duplicateCard.source,
  }),
  dispatch => ({
    doClearDuplicateCard: () => dispatch(clearDuplicateCard()),
  }),
)(DuplicateCard);
