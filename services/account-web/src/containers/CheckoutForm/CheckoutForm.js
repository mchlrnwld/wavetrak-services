import React, { Component } from 'react';
import { CardElement, injectStripe } from 'react-stripe-elements';
import { Alert, Button } from '@surfline/quiver-react';
import classnames from 'classnames';
import { PaymentRequestSubscription } from '../../containers';
import {
  Disclaimer,
  CreditCard,
  PromoCodeForm,
  PremiumPlans,
  PremiumAnnualPlan,
  PlanBillingText,
} from '../../components';

class CheckoutForm extends Component {
  getActiveCardClassName = (cardId, selectedCard) =>
    classnames({
      'credit-card': true,
      'credit-card--active': cardId === selectedCard,
    });

  createOptions = (fontSize, padding) => ({
    style: {
      base: {
        fontSize,
        color: '#424770',
        letterSpacing: '0.025em',
        fontFamily: 'Source Sans Pro, Helvetica, sans-serif',
        '::placeholder': {
          color: '#aab7c4',
        },
        padding,
      },
      invalid: {
        color: '#9e2146',
      },
    },
  });

  async submitPayment() {
    const {
      triggerSubmit,
      paymentHandler,
      stripe,
      selectedPlanId,
      selectedCard,
      showCards,
    } = this.props;

    triggerSubmit();
    const { token = null } = !showCards && (await stripe.createToken({ type: 'card' }));
    const paymentValues = {
      plan: selectedPlanId,
      source: token,
      selectedCard: showCards ? selectedCard : null,
    };
    paymentHandler(paymentValues);
  }

  renderCards = () => {
    const { cards, selectedCard, showCards, submitting, doSetSelectedCardSource } = this.props;
    if (!cards || !showCards) {
      return (
        <div>
          <div className="section-label">Enter your card</div>
          <CardElement {...this.createOptions('18px')} disabled={submitting} />
        </div>
      );
    }
    return (
      <div className="credit-card-list">
        <div className="section-label">Select a card</div>
        {cards.map((card, index) => (
          <CreditCard
            card={card}
            selectedCard={selectedCard}
            doSetSelectedCardSource={doSetSelectedCardSource}
            tabIndex={index}
            key={card.id}
          />
        ))}
      </div>
    );
  };

  render() {
    const {
      plans,
      cards,
      submitting,
      toggleSelectedPlanId,
      selectedPlanId,
      trialEligible,
      trialPeriodDays,
      error,
      showCards,
      doToggleShowCards,
      promoCodesEnabled,
      promoCode,
      handlePromoCodeChange,
      paymentHandler,
      coupon,
      enableFunnelPlanTest,
    } = this.props;
    return (
      <div className="checkout-form__container">
        {error ? (
          <Alert style={{ marginBottom: '0px', textAlign: 'center' }} type="error">
            {error}
          </Alert>
        ) : null}
        <div className="section-label">Choose your plan</div>
        {enableFunnelPlanTest ? (
          <PremiumAnnualPlan plan={plans.find(({ id }) => id.includes('annual'))} />
        ) : (
          <PremiumPlans
            plans={plans}
            selectedPlanId={selectedPlanId}
            toggleSelectedPlanId={toggleSelectedPlanId}
            trialEligible={trialEligible}
          />
        )}
        <PlanBillingText
          trialEligible={trialEligible}
          coupon={coupon}
          trialPeriodDays={trialPeriodDays}
        />
        <div className="credit-card-container">
          {this.renderCards()}
          {cards ? (
            <div>
              <p onClick={() => doToggleShowCards(showCards)} className="credit-card__form__toggle">
                {showCards ? '+ Add New Credit Card' : 'Use An Existing Credit card'}
              </p>
            </div>
          ) : null}
        </div>
        {promoCodesEnabled ? (
          <PromoCodeForm promoCode={promoCode} handlePromoCodeChange={handlePromoCodeChange} />
        ) : null}
        <div className="checkout-form__container__payment-button">
          <Button loading={submitting} onClick={() => this.submitPayment()}>
            {!trialEligible || !trialPeriodDays ? 'Go Premium' : 'Start Free Trial'}
          </Button>
        </div>
        <PaymentRequestSubscription
          paymentHandler={paymentHandler}
          selectedPlanId={selectedPlanId}
          plans={plans}
          coupon={coupon}
        />
        <Disclaimer
          referenceText={!trialEligible || !trialPeriodDays ? 'Go Premium' : 'Start Free Trial'}
        />
      </div>
    );
  }
}

export default injectStripe(CheckoutForm);
