import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { initialize } from 'redux-form';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import * as queryString from 'query-string';
import { Alert } from '@surfline/quiver-react';
import { trackNavigatedToPage, trackEvent } from '@surfline/web-common';
import { register } from '../../redux/actions/user';
import { CreateForm, FacebookLogin } from '../../components';
import en from '../../international/translations/en';

class Create extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showFavoritesAlert: false,
    };
  }

  componentDidMount() {
    trackNavigatedToPage('Create Account', {}, 'Authentication');
    const { query } = this.props;
    if (query) {
      const queryParams = queryString.parse(query);
      sessionStorage.setItem('redirectUrl', queryParams.redirectUrl);
      if (queryParams && queryParams.alert === 'noAuthFav') {
        this.setFavoritesAlert();
      }
    }
  }

  // This is just a band-aide.
  setFavoritesAlert() {
    this.setState({
      showFavoritesAlert: true,
    });
  }

  submitForm = async (values) => {
    const { doSubmitRegister } = this.props;
    await doSubmitRegister(values);
  };

  render() {
    const formLocation = 'register';
    const meta = [{ name: 'description', content: en.create.meta_description }];
    const { submitting, submitSuccess, geo } = this.props;
    const { showFavoritesAlert } = this.state;
    return (
      <div className="create__container">
        <div className="create__card">
          {showFavoritesAlert && (
            <Alert style={{ textAlign: 'center', margin: '0 0 20px 0' }}>
              {en.create.onboarding_alert_nofav}
            </Alert>
          )}
          <h1 className="create__title">{en.create.title}</h1>
          <p>
            {'Already have an account? '}
            <Link
              to="/sign-in"
              onClick={() =>
                trackEvent('Create Account Form Clicked Sign In', { location: 'register' })
              }
            >
              {'  Sign In'}
            </Link>
          </p>
          <Helmet title={en.create.meta_title} meta={meta} />
          <FacebookLogin formLocation={formLocation} />
          <CreateForm
            submitting={submitting}
            submitSuccess={submitSuccess}
            formLocation={formLocation}
            submitFunc={this.submitForm}
            geo={geo}
          />
        </div>
      </div>
    );
  }
}

Create.propTypes = {
  query: PropTypes.string,
  submitSuccess: PropTypes.bool.isRequired,
  submitting: PropTypes.bool,
  geo: PropTypes.shape({}).isRequired,
  doSubmitRegister: PropTypes.func.isRequired,
};

Create.defaultProps = {
  query: null,
  submitting: null,
};

export default connect(
  (state) => ({
    query: state.router.location.search,
    submitting: state.interactions.submitting,
    submitSuccess: state.user.submitSuccess,
    geo: state.geo,
  }),
  (dispatch) => ({
    doSubmitRegister: (values) => dispatch(register(values)),
    initialize,
  }),
)(Create);
