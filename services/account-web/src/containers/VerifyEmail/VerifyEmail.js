import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { parse } from 'query-string';
import { Button } from '@surfline/quiver-react';
import config from '../../config';
import { verifyEmail, resendEmailVerification } from '../../redux/actions/verifyEmail';
import brandToString from '../../utils/brandToString';

class VerifyEmail extends Component {
  componentDidMount() {
    const {
      location: { search },
      doVerifyEmail,
    } = this.props;
    const { verification } = parse(search);
    doVerifyEmail(verification);
  }

  redirectOnSuccess = () => {
    setTimeout(() => {
      window.location.assign(config.redirectUrl);
    }, 3000);
  };

  resendOnFailure = (event) => {
    event.preventDefault();
    const { doResendEmail } = this.props;
    doResendEmail();
  };

  renderSuccess = () => {
    this.redirectOnSuccess();
    const { company } = config;
    return (
      <div className="email-verification__success">
        <h4>Success</h4>
        <div className="email-verification__message">
          {`Your email is verified!
            If you are not redirected to ${brandToString(company)}, click the link below.`}
        </div>
        <a className="quiver-button" href={config.redirectUrl}>
          {`Return to ${brandToString(company)}`}
        </a>
      </div>
    );
  };

  renderResendSuccess = () => {
    const { company } = config;
    return (
      <div className="email-verification__success">
        <h4>Email Sent</h4>
        <div className="email-verification__message">
          {`Please check your inbox or spam folders and follow the
            instructions listed in the email.`}
        </div>
        <a className="quiver-button" href={config.redirectUrl}>
          {`Return to ${brandToString(company)}`}
        </a>
      </div>
    );
  };

  renderError = () => (
    <div className="email-verification__failure">
      <h4>Verification Failed</h4>
      <div className="email-verification__message">
        {`Your verification request failed.
          Please click the link below to resend verification email.`}
      </div>
      <a
        className="quiver-button"
        onClick={(event) => this.resendOnFailure(event)}
        href="/account/edit-profile"
      >
        Resend Verification Email
      </a>
    </div>
  );

  renderVerifying = () => {
    const { verifyingEmail } = this.props;
    return (
      <div className="email-verification__in-progress">
        <h4>Stand By</h4>
        <div className="email-verification__message">
          {`We are verifying your email.
             If you continue to see this message, please contact support@surfline.com.`}
        </div>
        <Button type="success" loading={verifyingEmail}>
          Processing
        </Button>
      </div>
    );
  };

  render() {
    const { verifyingEmail, success, resendSuccess, error } = this.props;
    return (
      <div className="email-verification__content">
        {success ? this.renderSuccess() : null}
        {error ? this.renderError() : null}
        {resendSuccess ? this.renderResendSuccess() : null}
        {verifyingEmail ? this.renderVerifying() : null}
      </div>
    );
  }
}

VerifyEmail.defaultProps = {
  verifyingEmail: true,
  success: false,
  resendSuccess: false,
  error: null,
};

VerifyEmail.propTypes = {
  verifyingEmail: PropTypes.bool,
  success: PropTypes.bool,
  resendSuccess: PropTypes.bool,
  error: PropTypes.string,
  doVerifyEmail: PropTypes.func.isRequired,
  doResendEmail: PropTypes.func.isRequired,
  location: PropTypes.shape({
    search: PropTypes.string,
  }).isRequired,
};

export default connect(
  (state) => ({
    verifyingEmail: state.verifyEmail.verifyingEmail,
    success: state.verifyEmail.success,
    resendSuccess: state.verifyEmail.resendSuccess,
    error: state.verifyEmail.error,
    location: state.router?.location,
  }),
  (dispatch) => ({
    doVerifyEmail: (hash) => dispatch(verifyEmail(hash)),
    doResendEmail: () => dispatch(resendEmailVerification()),
  }),
)(VerifyEmail);
