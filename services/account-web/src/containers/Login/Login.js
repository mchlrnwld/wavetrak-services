import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Alert } from '@surfline/quiver-react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { trackNavigatedToPage, trackEvent } from '@surfline/web-common';
import { LoginForm, FacebookLogin } from '../../components';
import en from '../../international/translations/en';
import { login } from '../../redux/actions/login';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showFavoritesAlert: false,
    };
  }

  componentDidMount() {
    trackNavigatedToPage('Sign In', 'Authentication');

    const { query } = this.props;
    if (query) {
      const queryParams = queryString.parse(query);
      sessionStorage.setItem('redirectUrl', queryParams.redirectUrl);
      if (queryParams && queryParams.alert === 'noAuthFav') {
        this.setFavoritesAlert(queryParams.alert);
      }
    }
  }

  // This is just a band-aide.
  setFavoritesAlert() {
    this.setState({
      showFavoritesAlert: true,
    });
  }

  submitForm = async (values) => {
    const { doSubmitLogin } = this.props;
    await doSubmitLogin(values);
  };

  render() {
    const { submitting, submitSuccess } = this.props;
    const { showFavoritesAlert } = this.state;
    const formLocation = 'register';
    const meta = [{ name: 'description', content: en.sign_in.meta_description }];

    return (
      <div className="login__container">
        <div className="login__card">
          {showFavoritesAlert ? (
            <Alert style={{ textAlign: 'center', margin: '0 0 20px 0' }}>
              {en.create.onboarding_alert_nofav}
            </Alert>
          ) : null}
          <h1 className="login__title">{en.sign_in.title}</h1>
          <p>
            {/* eslint-disable-next-line react/jsx-curly-brace-presence */}
            {`Don't have an account?`}
            <Link
              to="/create-account"
              onClick={() => trackEvent('Sign In Form Clicked Create Account', 'register')}
            >
              {' Sign Up'}
            </Link>
          </p>
          <Helmet title={en.sign_in.meta_title} meta={meta} />
          <FacebookLogin formLocation={formLocation} />
          <LoginForm
            submitting={submitting}
            submitSuccess={submitSuccess}
            submitFunc={this.submitForm}
            formLocation={formLocation}
          />
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  submitting: PropTypes.bool,
  submitSuccess: PropTypes.bool.isRequired,
  query: PropTypes.string,
  doSubmitLogin: PropTypes.func.isRequired,
};

Login.defaultProps = {
  query: null,
  submitting: null,
};

export default connect(
  (state) => ({
    submitting: state.interactions.submitting,
    submitSuccess: state.login.submitSuccess,
    loginError: state.login.loginError,
    query: state.router.location.search,
  }),
  (dispatch) => ({
    doSubmitLogin: (values) => dispatch(login(values)),
  }),
)(Login);
