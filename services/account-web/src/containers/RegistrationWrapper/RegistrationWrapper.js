import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { toggleRegistrationForm, startSubmitSpinner } from '../../redux/actions/interactions';
import { login } from '../../redux/actions/login';
import { register } from '../../redux/actions/user';
import { getAccessToken } from '../../selectors/auth';
import { CreateForm, LoginForm } from '../../components';
import config from '../../config';

class RegistrationWrapper extends Component {
  submitRegistrationForm = async (values) => {
    const { showLogin, doSubmitLogin, doSubmitRegister, successCallback } = this.props;
    const submitValues = {
      ...values,
      promotionId: config.funnelConfig ? config.funnelConfig._id : null,
    };
    const submitRegisterFunc = showLogin ? doSubmitLogin : doSubmitRegister;
    await submitRegisterFunc(submitValues);
    if (successCallback) await successCallback();
  };

  render() {
    const formLocation = 'funnel';
    const { submitting, submitSuccess, showLogin, viewedRegistration, children, geo } = this.props;
    viewedRegistration();
    return (
      <div className="registration-wrapper">
        {children}
        {showLogin ? (
          <LoginForm
            onSubmit={this.handleSubmit}
            submitting={submitting}
            submitSuccess={submitSuccess}
            formLocation={formLocation}
            submitFunc={this.submitRegistrationForm}
          />
        ) : (
          <CreateForm
            onSubmit={this.handleSubmit}
            submitting={submitting}
            submitSuccess={submitSuccess}
            formLocation={formLocation}
            submitFunc={this.submitRegistrationForm}
            geo={geo}
          />
        )}
      </div>
    );
  }
}

RegistrationWrapper.propTypes = {
  showLogin: PropTypes.bool,
  doSubmitLogin: PropTypes.func.isRequired,
  doSubmitRegister: PropTypes.func.isRequired,
  successCallback: PropTypes.func.isRequired,
  submitting: PropTypes.bool,
  submitSuccess: PropTypes.bool,
  viewedRegistration: PropTypes.bool,
  children: PropTypes.node,
  geo: PropTypes.shape({}).isRequired,
};

RegistrationWrapper.defaultProps = {
  viewedRegistration: null,
  submitting: null,
  submitSuccess: null,
  showLogin: null,
  children: null,
};

export default connect(
  (state) => ({
    accessToken: getAccessToken(state),
    submitting: state.interactions.submitting,
    submitSuccess: state.user.submitSuccess,
    fbSubmitting: state.facebook.fbSubmitting,
    fbSubmitSuccess: state.facebook.fbSubmitSuccess,
    fbLoginError: state.facebook.fbLoginError,
    geo: state.geo,
  }),
  (dispatch) => ({
    doToggleForm: (showLogin) => dispatch(toggleRegistrationForm(showLogin)),
    doSubmitLogin: (values) => dispatch(login(values)),
    doSubmitRegister: (values) => dispatch(register(values)),
    doStartSubmitSpinner: () => dispatch(startSubmitSpinner()),
  }),
)(RegistrationWrapper);
