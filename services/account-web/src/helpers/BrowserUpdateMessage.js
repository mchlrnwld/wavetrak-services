import PropTypes from 'prop-types';
import React, { Component } from 'react';
import en from '../international/translations/en';

export default class BrowserUpdateMessage extends Component {
  static propTypes = {
    browserUpdate: PropTypes.bool
  };

  render() {
    const {browserUpdate} = this.props;
    const browserUpdateTextBefore = en.index.browserUpdateTextBefore;
    const browserUpdateLink = en.index.browserUpdateLink;
    const browserUpdateTextAfter = en.index.browserUpdateTextAfter;
    const browserUpdateStyleDiv = browserUpdate === true ? 'browserUpdate' : 'browserUpdate__hidden';
    const browserUpdateStyleText = 'browserUpdate__text';
    const browserUpdateStyleLink = 'browserUpdate__text__link';

    if (browserUpdate) {
      return (
        <div className={browserUpdateStyleDiv}>
          <p className={browserUpdateStyleText}>{browserUpdateTextBefore}<a href="/subs/update" className={browserUpdateStyleLink}>{browserUpdateLink}</a>{browserUpdateTextAfter}</p>
        </div>
      );
    }
    return null;
  }
}
