import React from 'react';
import path from 'path';
import fs from 'fs';

const loadManifest = (config, bundle) => {
  const assetPath = path.join(__dirname, `../../build/${config.company}/dist`, 'manifest.json');
  const manifest = fs.readFileSync(assetPath);
  const assets = JSON.parse(manifest);

  const js = assets[`${bundle}.js`];
  const css = assets[`${bundle}.css`];

  return {
    js: js ? <script
      src={js}
      charSet="utf-8"
    /> : null,
    css: css ? <link
      rel="stylesheet"
      type="text/css"
      href={css}
    /> : null,
  };
};


export default loadManifest;
