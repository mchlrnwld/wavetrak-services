/* eslint no-use-before-define: 0 */
/* eslint prefer-const: 0 */
import superagent from 'superagent';

function formatUrl(path) {
  const adjustedPath = path[0] !== '/' ? '/' + path : path;
  // Prepend `/api` to relative URL, to proxy to API server.
  return '/subs/api' + adjustedPath;
}

export default function ApiClient(req) {
  ['get', 'post', 'put', 'patch', 'del'].forEach((method) =>
    this[method] = (path, { params, data } = {}) => new Promise((resolve, reject) => {
      const request = superagent[method](formatUrl(path));

      if (params) {
        request.query(params);
      }

      if (__SERVER__ && req.get('cookie')) {
        request.set('cookie', req.get('cookie'));
      }

      if (data) {
        request.send(data);
      }

      request.end((err, { body } = {}) =>
        err ? reject(body || err) : resolve(body));
    }));
  return this;
}
