import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { SegmentBrowser } from '@surfline/quiver-react';
// import StripeJS from '../common/components/StripeJS';
import BrowserUpdateMessage from './BrowserUpdateMessage';
import NewRelicBrowser from '../common/components/NewRelicBrowser';
import FacebookBrowser from '../common/components/FacebookBrowser';

/**
 * Wrapper component containing HTML metadata tags.
 * Used in server-side code only to wrap the string output of the
 * rendered route component.
 *
 * The only thing this component doesn't (and can't) include is the
 * HTML doctype declaration, which is added to the rendered output
 * by the server.js file.
 */
export default class Html extends Component {
  static propTypes = {
    assets: PropTypes.object,
    config: PropTypes.object,
    component: PropTypes.node,
    store: PropTypes.object,
    favicon: PropTypes.string,
    browserUpdate: PropTypes.bool,
  };

  render() {
    const { assets, favicon, browserUpdate, config, translations, state } = this.props;
    const { newRelic, facebook, segment } = config;

    return (
      <html lang="en-us">
        <head>
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />

          <NewRelicBrowser
            licenseKey={newRelic.licenseKey}
            applicationID={newRelic.applicationID}
          />

          {/* <StripeJS /> */}

          <SegmentBrowser segmentKey={segment} />

          <link rel="shortcut icon" href={favicon} />
          <link
            href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700"
            rel="stylesheet"
            type="text/css"
          />
          <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"
          />
          <link
            rel="stylesheet"
            type="text/css"
            href="https://cloud.typography.com/6101534/7252552/css/fonts.css"
          />
          {assets.css}
        </head>
        <body>
          <BrowserUpdateMessage browserUpdate={browserUpdate} />

          <div id="fb-root" />
          {facebook ? <FacebookBrowser appId={facebook.appId} /> : ''}

          <div id="content" />
          <script
            dangerouslySetInnerHTML={{ __html: `window.__data=${JSON.stringify(state)};` }}
            charSet="UTF-8"
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `var wt={};wt.brand=${JSON.stringify(
                config.company,
              )};wt.funnel_config=${JSON.stringify(config)};wt.funnel_translations=${JSON.stringify(
                translations,
              )};`,
            }}
          />
          {assets.js}
        </body>
      </html>
    );
  }
}
