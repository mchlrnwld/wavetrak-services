/* eslint-disable no-undef */

if (__SERVER__) {
  module.exports = require('./base');
} else {
  const translations = {
    bw: require('./bw/en'),
    sl: require('./sl/en'),
    fs: require('./fs/en')
  };
  module.exports = translations[wt.brand];
}
