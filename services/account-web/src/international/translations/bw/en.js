import base from '../base';
import {
  Buoy,
  Wind,
  SwellLines,
  BuoyweatherLogo,
  ForecastsAlt as PromotionForecasts,
  WeatherAlt as PromotionWeather,
  WindAlt as PromotionWind
} from '../../../components/Icons';
import config from '../../../config';

export default {
  index: {
    ...base.index,
    browserUpdateTextBefore: 'You are using an outdated browser. Buoyweather no longer supports this browser. Please ',
  },

  app: {
    ...base.app,
  },

  upgradeFunnel: {
    upgradeHeaderSubstitle: 'Buoyweather plans start at $6.67/mo'
  },

  duplicateCard: {
    ...base.duplicateCard
  },

  premiumFeatures: {
    features: [
      {
        Icon: SwellLines,
        header: 'Marine Forecasts For Any Point',
        description: 'View accurate 16-day wind, wave and tide forecasts.'
      },
      {
        Icon: Wind,
        header: 'High-Resolution Wind Forecasts',
        description: 'Access a full collection of weather charts and forecasts.'
      },
      {
        Icon: Buoy,
        header: 'Pick Your Weather Window',
        description: 'Long-range forecasts help you stay safe on the water.'
      },
    ]
  },
  promotionFeatures: {
    BrandLogo: BuoyweatherLogo,
    header: 'Get the most important information needed to maximize your time on the water',
    features: [
      {
        Icon: PromotionForecasts,
        header: 'Marine Forecasts',
      },
      {
        Icon: PromotionWind,
        header: 'Wind Forecasts',
      },
      {
        Icon: PromotionWeather,
        header: 'Pick Weather Windows',
      }
    ],
    promoCodeError: 'Invalid Code. To receive this offer you must enter a valid code. If you do not have one, you can still upgrade to BuoyWeather Premium at www.buoyweather.com/upgrade/'
  },
  giftCards: {
    ...base.giftCards,
    backgroundImage: `url(${config.cdnHost}static/gifts/bw/rail_cta_desktop_bg_image.jpg)`,
    layeredCardImage: `url(${config.cdnHost}static/gifts/bw/rail_cta_desktop_cards.png)`,
    brandIcon: 'url(https://wa.cdn-surfline.com/account/static/bw-icon.svg)'
  },
  rail_cta: {
    ...base.rail_cta.bw
  },

  create: {
    ...base.create,
    meta_title: 'Create Account - Buoyweather',
    meta_description: 'Stay safe on the water – create an account with Buoyweather to gain instant access to point-based marine forecasts.',

    form: {
      ...base.create.form,
      disclaimer_text_start: 'By clicking "Create Account" you agree to Buoyweather ',
    }
  },

  create_funnel: {
    ...base.create_funnel,
    meta_title: 'Create Your Premium Account - Buoyweather',
    title_no_trial: 'Join Buoyweather Premium',
  },

  sign_in: {
    ...base.sign_in,
    meta_title: 'Sign In - Buoyweather',
    meta_description: 'Sign in to Buoyweather and get point-based marine forecasts anywhere in the world',

    form: {
      ...base.sign_in.form,
      disclaimer_text_start: 'If you click "Sign In with Facebook" and are not a Buoyweather user, you will be registered and you agree to Buoyweather ',
    }
  },

  sign_in_funnel: {
    ...base.sign_in_funnel,
    title_no_trial: 'Sign in to join<br/>Buoyweather Premium',
    meta_title: 'Sign In to Go Premium - Buoyweather',
    meta_description: 'Get the latest 16-day marine forecasts – sign in with your Buoyweather account to go Premium.',
  },

  payment_form: {
    ...base.payment_form,
    meta_title: 'Start Your Premium Trial - Buoyweather',
    meta_description: 'Stay safe on the water – start your free 15-day Premium trial today!',
    meta_description_resume: 'Stay safe on the water, renew Buoyweather Premium',
    title_resume_premium: 'Stay safe on the water, renew Buoyweather Premium',
    title_no_trial: 'Join Buoyweather Premium',
    radio_plan_select: {
      ...base.payment_form.radio_plan_select,
    },
    stripe: {
      ...base.payment_form.stripe,
    },
    discount: {
      ...base.payment_form.discount,
      success_message_plans: 'Success! Your discount code has been applied.',
      form: {
        ...base.payment_form.discount.form,
      }
    },
  },

  payment_success: {
    subTitle: 'Welcome to Buoyweather Premium!',
    message: 'Thank you for joining the Buoyweather family.  You\'re now on your way to safer seas with the most trusted marine weather information.',
    linkText: 'Return to Buoyweather',
    linkAddress: 'https://buoyweather.com',
  },

  forgot_password: {
    ...base.forgot_password,
    enter_code: {
      ...base.forgot_password.enter_code,
      meta_title: 'Enter Code - Buoyweather',
    },
    enter_email: {
      ...base.forgot_password.enter_email,
      meta_title: 'Forgot Password - Buoyweather',
    },
    set_password: {
      ...base.forgot_password.set_password,
      meta_title: 'Create New Password - Buoyweather',
    },
    email_form: {
      ...base.forgot_password.email_form,
    },
    passcode_form: {
      ...base.forgot_password.passcode_form,
    },
    reset_password_form: {
      ...base.forgot_password.reset_password_form,
    }
  },

  edit_profile: {
    ...base.edit_profile,
    meta_title: {
      ...base.edit_profile.meta_title,
    },
    form: {
      ...base.edit_profile.form,
    }
  },

  edit_settings: {
    ...base.edit_settings,
    meta_title: {
      ...base.edit_settings.meta_title,
    },
    form: {
      ...base.edit_settings.form,
    }
  },

  favorites: {
    ...base.favorites,
    meta_title: {
      ...base.favorites.meta_title,
    },
  },

  change_password: {
    ...base.change_password,
    meta_title: {
      ...base.change_password.meta_title,
    },
    form: {
      ...base.change_password.form,
    }
  },

  subscription: {
    ...base.subscription,
    title: {
      ...base.subscription.title,
    },
    meta_title: {
      ...base.subscription.meta_title,
    },
    availablePlans: {
      ...base.subscription.availablePlans,
    },
    subheading_vip: {
      ...base.subscription.subheading_vip,
    },
    expiring_message: (renewDate, brand) => `Your ${brand} subscription will expire on ${renewDate}. Stay Premium to keep 16-Day Forecasts, High Resolution Wind, and more.`,
    cards: {
      ...base.subscription.cards,
      iap_message: {
        ...base.subscription.cards.iap_message,
      },
      iap_message_link: {
        ...base.subscription.cards.iap_message_link,
      },
      vip: {
        ...base.subscription.cards.vip,
      },
    },
    card: {
      ...base.subscription.card,
      actions: {
        ...base.subscription.card.actions,
      }
    },
    goPremium: {
      ...base.subscription.goPremium,
      sl: {
        ...base.subscription.goPremium.sl,
      },
      bw: {
        ...base.subscription.goPremium.bw,
      },
      fs: {
        ...base.subscription.goPremium.fs,
      },
    },
    previousSubscriptions: {
      ...base.subscription.previousSubscriptions,
      table: {
        ...base.subscription.table,
      }
    },
    plans: {
      ...base.subscription.plans,
    },
    prevSub: {
      ...base.subscription.prevSub,
    },
    availablePlan: {
      ...base.subscription.availablePlan,
    }
  },
  not_found: {
    ...base.not_found,
    button_text: 'Back to Buoyweather.com',
  },

  five_hundred: {
    ...base.five_hundred,
    button_text: 'Back to Buoyweather.com',
  },

  utils: {
    ...base.utils,
  },

  facebook: {
    ...base.facebook,
  },

  or_break: {
    ...base.or_break,
  },
  account: {
    ...base.account,
    overview: {
      ...base.account.overview,
      meta_title: 'Account Overview - Buoyweather',
      benefits_card: {
        ...base.account.overview.benefits_card,
        price: {
          ...base.account.overview.benefits_card.price,
        },
      },
      promo_card: {
        ...base.account.overview.promo_card,
        sl_premium: {
          ...base.account.overview.promo_card.sl_premium,
        },
        free: {
          ...base.account.overview.promo_card.free,
        },
        expired: {
          ...base.account.overview.promo_card.expired,
        },
        expiring: {
          ...base.account.overview.promo_card.expiring,
        }
      },
      info_card: {
        ...base.account.overview.info_card,
        premium: {
          ...base.account.overview.info_card.premium,
          message: {
            ...base.account.overview.info_card.premium.message,
          }
        },
        free: {
          ...base.account.overview.info_card.free,
          message: {
            ...base.account.overview.info_card.free.message,
          }
        },
        expiring: {
          ...base.account.overview.info_card.expiring,
          message: {
            ...base.account.overview.info_card.expiring.message,
          }
        },
        expired: {
          ...base.account.overview.info_card.expired,
          message: {
            ...base.account.overview.info_card.expired.message,
          }
        },
        discount: {
          ...base.account.overview.info_card.discount,
        }
      },
      subs_card: {
        ...base.account.overview.subs_card,
      },
      statusCard: {
        ...base.account.overview.statusCard,
        expired: {
          ...base.account.overview.statusCard.expired,
        },
        expiring: {
          ...base.account.overview.statusCard.expiring,
        },
        delinquent: {
          ...base.account.overview.statusCard.delinquent,
        },
        monthly: {
          ...base.account.overview.statusCard.monthly,
        }
      }
    }
  }
};
