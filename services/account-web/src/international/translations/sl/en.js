/* Surfline text is default as defined in base.js */
import base from '../base';
import {
  Cams,
  Analysis,
  Charts,
  CamRewind,
  SurflineLogo,
  CamsAlt as PromotionCams,
  AnalysisAlt as PromotionAnalysis,
  ChartsAlt as PromotionCharts
} from '../../../components/Icons';
import config from '../../../config';

export default {
  index: {
    ...base.index,
  },

  app: {
    ...base.app,
  },

  upgradeFunnel: {
    upgradeHeaderSubstitle: 'Surfline plans start at $7.99/mo'
  },

  duplicateCard: {
    ...base.duplicateCard
  },

  premiumFeatures: {
    features: [
      {
        Icon: Cams,
        header: 'Ad-Free and Premium Cams',
        description: 'Watch live conditions ad-free -- and access exclusive, high-resolution cams.'
      },
      {
        Icon: Analysis,
        header: 'Trusted Forecast Team',
        description: 'Plan your next trip or session with Surfline’s expert forecast guidance.'
      },
      {
        Icon: Charts,
        header: 'Exclusive Forecast Tools',
        description: 'Access curated condition reports and proprietary surf, wind and buoy data.'
      },
      {
        Icon: CamRewind,
        header: 'Cam Rewind',
        description: 'Relive an epic session by downloading your rides -- then share ‘em with friends.'
      },
    ]
  },

  promotionFeatures: {
    BrandLogo: SurflineLogo,
    header: 'Score better waves more often',
    features: [
      {
        Icon: PromotionCams,
        header: 'Ad-Free and Premium Cams',
      },
      {
        Icon: PromotionAnalysis,
        header: 'Trusted Forecast Team',
      },
      {
        Icon: PromotionCharts,
        header: 'Exclusive Forecast Tools',
      }
    ],
    promoCodeError: 'Invalid Code. To receive this offer you must enter a valid code. If you do not have one, you can still upgrade to Surfline Premium at www.surfline.com/upgrade/'
  },

  giftCards: {
    ...base.giftCards,
    backgroundImage: `url(${config.cdnHost}static/gifts/sl/rail_cta_desktop_bg_image.jpg)`,
    layeredCardImage: `url(${config.cdnHost}static/gifts/sl/rail_cta_desktop_cards.png)`,
    brandIcon: 'url(https://wa.cdn-surfline.com/account/static/surfline-waterdrop.svg)'
  },
  
  rail_cta: {
    ...base.rail_cta.sl
  },

  create: {
    ...base.create,

    form: {
      ...base.create.form,
    }
  },

  create_funnel: {
    ...base.create_funnel,
  },

  sign_in: {
    ...base.sign_in,

    form: {
      ...base.sign_in.form,
    }
  },

  sign_in_funnel: {
    ...base.sign_in_funnel,
  },


  payment_form: {
    ...base.payment_form,
    radio_plan_select: {
      ...base.payment_form.radio_plan_select,
    },
    stripe: {
      ...base.payment_form.stripe,
    },
    discount: {
      ...base.payment_form.discount,
      form: {
        ...base.payment_form.discount.form,
      }
    },
  },

  payment_success: {
    subTitle: 'Welcome to Surfline Premium!',
    message: 'Thank you for joining the Surfline family.  You\'re now on your way to scoring better waves, more often.  See you in the water.',
    linkText: 'Return to Surfline',
    linkAddress: 'https://surfline.com',
  },

  forgot_password: {
    ...base.forgot_password,
    enter_code: {
      ...base.forgot_password.enter_code,
    },
    enter_email: {
      ...base.forgot_password.enter_email,
    },
    set_password: {
      ...base.forgot_password.set_password,
    },
    email_form: {
      ...base.forgot_password.email_form,
    },
    passcode_form: {
      ...base.forgot_password.passcode_form,
    },
    reset_password_form: {
      ...base.forgot_password.reset_password_form,
    }
  },

  edit_profile: {
    ...base.edit_profile,
    meta_title: {
      ...base.edit_profile.meta_title,
    },
    form: {
      ...base.edit_profile.form,
    }
  },

  edit_settings: {
    ...base.edit_settings,
    meta_title: {
      ...base.edit_settings.meta_title,
    },
    form: {
      ...base.edit_settings.form,
    }
  },

  favorites: {
    ...base.favorites,
    meta_title: {
      ...base.favorites.meta_title,
    },
  },

  change_password: {
    ...base.change_password,
    meta_title: {
      ...base.change_password.meta_title,
    },
    form: {
      ...base.change_password.form,
    }
  },

  subscription: {
    ...base.subscription,
    title: {
      ...base.subscription.title,
    },
    meta_title: {
      ...base.subscription.meta_title,
    },
    availablePlans: {
      ...base.subscription.availablePlans,
    },
    subheading_vip: {
      ...base.subscription.subheading_vip,
    },
    expiring_message: (renewDate, brand) => `Your ${brand} subscription will expire on ${renewDate}. Stay Premium to continue to access ad-free cams, 16-day forecasts, Premium cams, and more.`,
    cards: {
      ...base.subscription.cards,
      iap_message: {
        ...base.subscription.cards.iap_message,
      },
      iap_message_link: {
        ...base.subscription.cards.iap_message_link,
      },
      vip: {
        ...base.subscription.cards.vip,
      },
    },
    card: {
      ...base.subscription.card,
      actions: {
        ...base.subscription.card.actions,
      }
    },
    goPremium: {
      ...base.subscription.goPremium,
      sl: {
        ...base.subscription.goPremium.sl,
      },
      bw: {
        ...base.subscription.goPremium.bw,
      },
      fs: {
        ...base.subscription.goPremium.fs,
      },
    },
    previousSubscriptions: {
      ...base.subscription.previousSubscriptions,
      table: {
        ...base.subscription.table,
      }
    },
    plans: {
      ...base.subscription.plans,
    },
    prevSub: {
      ...base.subscription.prevSub,
    },
    availablePlan: {
      ...base.subscription.availablePlan,
    }
  },
  not_found: {
    ...base.not_found,
  },

  five_hundred: {
    ...base.five_hundred,
  },

  utils: {
    ...base.utils,
  },

  facebook: {
    ...base.facebook,
  },

  or_break: {
    ...base.or_break,
  },
  account: {
    ...base.account,
    overview: {
      ...base.account.overview,
      benefits_card: {
        ...base.account.overview.benefits_card,
        price: {
          ...base.account.overview.benefits_card.price,
        },
      },
      promo_card: {
        ...base.account.overview.promo_card,
        sl_premium: {
          ...base.account.overview.promo_card.sl_premium,
        },
        free: {
          ...base.account.overview.promo_card.free,
        },
        expired: {
          ...base.account.overview.promo_card.expired,
        },
        expiring: {
          ...base.account.overview.promo_card.expiring,
        }
      },
      info_card: {
        ...base.account.overview.info_card,
        premium: {
          ...base.account.overview.info_card.premium,
          message: {
            ...base.account.overview.info_card.premium.message,
          }
        },
        free: {
          ...base.account.overview.info_card.free,
          message: {
            ...base.account.overview.info_card.free.message,
          }
        },
        expiring: {
          ...base.account.overview.info_card.expiring,
          message: {
            ...base.account.overview.info_card.expiring.message,
          }
        },
        expired: {
          ...base.account.overview.info_card.expired,
          message: {
            ...base.account.overview.info_card.expired.message,
          }
        },
        discount: {
          ...base.account.overview.info_card.discount,
        }
      },
      subs_card: {
        ...base.account.overview.subs_card,
      },
      statusCard: {
        ...base.account.overview.statusCard,
        expired: {
          ...base.account.overview.statusCard.expired,
        },
        expiring: {
          ...base.account.overview.statusCard.expiring,
        },
        delinquent: {
          ...base.account.overview.statusCard.delinquent,
        },
        monthly: {
          ...base.account.overview.statusCard.monthly,
        }
      }
    }
  }
};
