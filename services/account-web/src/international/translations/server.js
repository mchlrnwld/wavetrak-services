const base = require('./base');

const translations = {
  bw: require('./bw/en'),
  sl: require('./sl/en'),
  fs: require('./fs/en')
};

module.exports = brand => ({ en: Object.assign(base, translations[brand]) });

