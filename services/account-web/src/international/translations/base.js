import format from 'date-fns/format';
import { Wave } from '../../components/Icons';
import brandToString from '../../utils/brandToString';
import config from '../../config';

export default {
  index: {
    browserUpdateTextBefore:
      'You are using an outdated browser. Surfline no longer supports this browser. Please ',
    browserUpdateLink: 'upgrade your browser',
    browserUpdateTextAfter: ' to improve your experience.',
  },

  app: {
    sign_in_link: 'Sign in',
    create_account_link: 'Create account',
    brand_icons: 'One account for everything.',
  },

  duplicateCard: {
    header: 'This card has been used for a free trial before',
    description: (amount) =>
      `This card is not eligible for a free trial. Please use another card to start your trial, or continue to Go Premium. You will be charged $${amount} today.`,
    goPremium: 'GO PREMIUM',
    expirationDate: (date) => `EXPIRES ${date}`,
    goBack: 'GO BACK',
  },

  giftCards: {
    rightRailHeader: `${brandToString(config.company)} Premium Gift Cards`,
    learn_more_link: '',
    select: {
      headerText: `Give the Gift of ${brandToString(config.company)} Premium`,
      subHeaderText: `Stoke out a friend or loved one with a digital ${brandToString(
        config.company,
      )} Premium gift card.`,
    },
    success: {
      headerText: 'Success',
      subHeaderText: (email) =>
        `A receipt and confirmation has been sent to ${email}. Please let the recipient know to check their inbox as well as their Promotion or Spam folders to ensure they receive their gift card.`,
    },
  },
  rail_cta: {
    sl: {
      header: 'Premium Benefits',
      mobileHeader: 'Go Premium',
      mobileSubtitle: 'Learn More About Surfline Premium',
      MobileIcon: Wave,
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
    },
    bw: {
      mobileHeader: 'Go Premium',
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
      header: 'Buoyweather',
    },
    fs: {
      mobileHeader: 'Go Premium',
      sign_up: 'Sign Up',
      sign_in: 'Sign In',
      renew: 'Renew',
      header: 'FishTrack',
    },
  },

  create: {
    title: 'Create account',
    onboarding_title: "You're almost there!",
    onboarding_subtitle:
      'Save your settings by creating an account below. Already have an account?',
    onboarding_alert_nofav: 'Please create an account or sign in to save favorites.',
    meta_title: 'Create Account - Surfline',
    meta_description:
      'Don’t miss another good surf – create an account with Surfline to gain instant access to Surfline.',

    error_email_in_use: 'Email in use, ',
    error_form_error: 'Login failed!',

    form: {
      already_have_account: 'Already have an account?',
      sign_in_link: ' Sign In',
      discount_applied: 'Your discount has been applied.',
      label_first: 'First Name',
      label_last: 'Last Name',
      label_email: 'Email',
      email_error_message: 'Email in use, ',
      email_error_message_link: 'sign in?',
      label_password: 'Password',
      create_account_button: 'Create account',
      staySignedIn_label: 'Keep me signed in',
      receivePromotions_label: (brand) =>
        `I would like to receive product updates and promotional offers from ${brandToString(
          brand,
        )} and its partners`,
      disclaimer_text_start: 'By clicking "Create Account" you agree to Surfline ',
      terms_of_use_link_text: 'Terms of use',
      disclaimer_and: ' and ',
      privacy_policy_link_text: 'Privacy policy',
      password_helper_text: 'Minimum 6 characters',
    },
  },

  create_funnel: {
    meta_title: 'Create Your Premium Account - Surfline',
    title_no_trial: 'Join Surfline Premium',
  },

  sign_in: {
    title: 'Sign in',
    onboarding_title: "You're almost there!",
    onboarding_subtitle: 'Save your settings by signing in below. Need an account?',
    meta_title: 'Sign In - Surfline',
    meta_description:
      'Sign in to Surfline and start surfing more often by customizing your Surfline experience.',

    error_email_does_not_exist: 'No account with this email. ',
    error_form_error: 'Incorrect email / password combination',
    internal_server_error: 'Internal Server Error',

    form: {
      need_account: 'Need an account?',
      create_account_link: ' Sign Up',
      label_email: 'Email',
      email_error_message: 'No account with this email. ',
      email_error_message_link: 'Create account?',
      label_password: 'Password',
      forgot_password: 'Forgot Password?',
      sign_in_button: 'Sign in',
      staySignedIn_label: 'Keep me signed in',
      submit_success: '',
    },
    continueform: {
      title: 'It appears too many people are using your account right now',
      info:
        'Surfline has a limit on the total number of browsers and devices that you may be logged into at once.',
      continue_button: 'CONTINUE',
      go_back_link: 'CANCEL',
      recover_button: 'RECOVER ACCOUNT',
      faq_text: 'Why am I seeing this?',
    },
  },

  sign_in_funnel: {
    title: 'Sign in to start free trial',
    title_no_trial: 'Sign in to join Surfline Premium',
    meta_title: 'Sign In to Go Premium - Surfline',
    meta_description:
      "Don't miss another good surf - sign in with your Surfline account to go Premium.",
  },

  payment_form: {
    meta_title: 'Start Your Premium Trial - Surfline',
    meta_description: "Don't miss another good surf – start your free 15-day Premium trial today!",
    meta_description_resume: "Don't miss out on the next swell, renew Surfline Premium",
    title_resume_premium: "Don't miss out on the next swell, renew Surfline Premium",
    title_no_trial: 'Join Surfline Premium',
    subtitle: () => {
      const billDate = new Date();
      billDate.setDate(billDate.getDate() + 15);
      return `Billing begins after free trial ends on ${format(billDate, 'MM/DD/YY')}`;
    },
    disclaimer_text_start: (button) => `By clicking "${button}" button you agree to our `,
    terms_of_use_link_text: 'Terms of use',
    disclaimer_and: ' and ',
    privacy_policy_link_text: 'Privacy policy',
    free_trial_link: 'Start free trial instead?',
    add_new_card: 'Add new credit/debit card',
    card_on_file: 'Card on file',
    plan_currency_usd: '$',
    plan_interval_usd: '/mo',
    plan_interval_slash: '/',
    plan_billed_usd: ' (Billed $',
    plan_each_usd: '/',
    plan_yr_usd: 'yr',
    plan_mo_usd: 'mo',
    plan_close_usd: ')',
    plan_billed_daily: (amount) => `Billed $${amount} daily`,
    plan_billed_monthly: 'Billed month to month',
    plan_billed_yearly: (amount) => `Billed $${amount} once a year`,
    label_plan: 'Choose Subscription',
    label_cc: 'Card Number',
    label_exp: 'Exp MM/YY',
    label_exp_mm: 'Exp MM',
    label_exp_yy: 'Exp YY',
    label_scode: 'Security Code',
    card_expires: '[Expires:] MM/YY',
    card_expired: '[Expired:] MM/YY',
    submit_button: 'Start free trial',
    submit_button_resume_premium: 'Renew Premium',
    submit_button_no_trial: 'Join Premium',
    submit_success: '',
    fatal_cc_error:
      'The form could not be submitted. Please check your card details and try again.',
    radio_plan_select: {
      annual_title: 'Yearly',
      monthly_title: 'Monthly',
      annual_interval: ' each year',
      default_interval: ' each ',
      annual_plan_billed_usd: 'Billed $',
      monthly_plan_billed_usd: 'Billed month to month',
    },
    stripe: {
      scode: 'There was a problem with the provided security code',
      scode_req: 'Security code is required',
      zip: 'The postal / zip code is invalid',
      zip_req: 'Postal / zip code is required',
      cc: 'There was a problem with the provided credit card number',
      cc_req: 'Credit card number is required',
      exp: 'The expiration number is invalid',
      expm: 'The expiration month is invalid',
      expm_req: 'Expiration month is required',
      expy: 'The expiration year is invalid',
      expy_req: 'Expiration year is required',
    },
    discount: {
      internal_server_error: 'Internal Server Error',
      toggle_link: 'Enter discount code',
      success_message_plans: 'Yewww! Your discount code has been applied.',
      query_error_message: 'Discount code has expired or is no longer valid.',

      form: {
        label_code: 'Discount Code',
        code_error_message: 'Invalid code, try again',
        code_success_message: 'Success!',
        apply_discount_button: 'Apply Discount',
        submit_success: '',
      },
    },
  },

  forgot_password: {
    message: 'Not all those who wander are lost',
    enter_code: {
      title: 'Email Sent',
      meta_title: 'Email Sent - Surfline',
      notify: 'You will have 30 minutes to reset your password.',
      info:
        'If you do not receive an email shortly, check your junk folder or contact us. Once you receive the email please click the link to reset your password.',
      internal_server_error: 'Internal Server Error',
      passcode_error: 'Invalid Pass Code.',
      form_error: 'Forgot Password failed!',
    },
    enter_email: {
      title: 'Forgot Password?',
      meta_title: 'Forgot Password - Surfline',
      internal_server_error: 'Internal Server Error',
      email_error: 'Email not found',
      form_error: 'Login failed!',
      need_help_message: 'Still need help?',
    },
    set_password: {
      title: 'New Password',
      meta_title: 'Create New Password - Surfline',
    },
    email_form: {
      label_email: 'Email Address',
      request_reset_button: 'Request Reset',
    },
    passcode_form: {
      label_passcode: 'Enter 4-Digit Code',
      reset_password_button: 'Submit',
    },
    reset_password_form: {
      label_new_password: 'New Password',
      label_new_password_confirm: 'Confirm New Password',
      set_password_button: 'Set New Password',
      password_helper_text: 'Minimum 6 characters',
    },
  },

  edit_profile: {
    meta_title: {
      sl: 'Edit Profile - Surfline',
      bw: 'Edit Profile - Buoyweather',
      fs: 'Edit Profile - FishTrack',
    },
    title: 'Profile',
    form: {
      save_button: 'Save',
      label_first: 'First Name',
      label_last: 'Last Name',
      label_email: 'Email',
    },
    emailVerificationMessage: (
      email,
      company,
    ) => `Check ${email} for a verification email from ${company},
      and follow the link to verify your account.`,
  },

  edit_settings: {
    meta_title: {
      sl: 'Settings - Surfline',
      bw: 'Settings - Buoyweather',
      fs: 'Settings - FishTrack',
    },
    title: 'Settings',
    form: {
      save_button: 'Save',
      label_windSpeed: 'Wind Speed',
      label_surfHeight: 'Surf Height',
      label_swellHeight: 'Swell Height',
      label_tideHeight: 'Tide Height',
      label_temperature: 'Temperature',
      label_localizedFeed: 'Localized Feed',
      label_dateFormat: 'Date Format',
      label_model: 'Forecast Model',
    },
  },

  favorites: {
    meta_title: {
      sl: 'Favorites - Surfline',
      bw: 'Favorites - Buoyweather',
      fs: 'Favorites - FishTrack',
    },
    title: 'Surfline Favorites',
    message: 'Add some favorites',
    no_favs_title: 'Welcome',
    no_favs_button: 'View Map',
    no_favs_message:
      'Use the map or search bar to find your favorite spots, then press star on the report page to save Cams and Forecasts to your Favorites.',
  },

  change_password: {
    meta_title: {
      sl: 'Change Password - Surfline',
      bw: 'Change Password - Buoyweather',
      fs: 'Change Password - FishTrack',
    },
    title: 'Change Password',
    form: {
      save_button: 'Update Password',
      label_current: 'Current Password',
      label_new: 'New Password',
      label_confirm: 'Confirm Password',
    },
  },

  subscription: {
    meta_title: {
      sl: 'Subscriptions - Surfline',
      bw: 'Subscriptions - Buoyweather',
      fs: 'Subscriptions - FishTrack',
    },
    title: {
      subscription: 'Subscription',
      subscriptions: 'Subscriptions',
    },
    subheading: (period, renewDate, price, brand) =>
      `Your ${brand} subscription is currently being billed ${period} and will auto-renew on ${renewDate}${
        price ? ` for ${price}` : ''
      }.`,
    subheading_trial: (period, renewDate, price, brand) =>
      `You’re currently on a free trial and your ${brand} ${period} subscription at ${price} will begin on ${renewDate}.`,
    subheading_expiring: (renewDate, brand) =>
      `Your ${brand} subscription is set to expire on ${renewDate}.`,
    subheading_vip: {
      Surfline:
        'You are a Surfline VIP Member. For assistance in managing your VIP account please email <a href="mailto:support@surfline.com">support@surfline.com</a>.',
      Buoyweather:
        'You are a Buoyweather VIP Member. For assistance in managing your VIP account please email <a href="mailto:support@buoyweather.com">support@buoyweather.com</a>.',
      Fishtrack:
        'You are a FishTrack VIP Member. For assistance in managing your VIP account please email <a href="mailto:support@fishtrack.com">support@fishtrack.com</a>.',
    },
    subheading_apple:
      'You are currently being billed through Apple, please check iTunes to manage your subscription.',
    subheading_google:
      'You are currently being billed through Google Play, please check Google Play to manage your subscription.',
    no_premium_subs: 'You currently have no Premium subscriptions.',
    add: 'Add Credit/Debit Card',
    add_card_button: 'Add Card',
    cancel: (subscriptionType, brand) =>
      `Cancel ${brand} subscription${subscriptionType === 'apple' ? ' through iTunes' : ''}?`,
    availablePlans: {
      current_subscription: 'Current Subscription',
    },
    cards: {
      title: 'Payment Details',
      iap_message: {
        apple:
          "You're currently subscribed through iTunes. The payment details below are not being used for your active subscription.",
        google:
          "You're currently subscribed through Google Play. The payment details below are not being used for your active subscription.",
      },
      iap_message_link: {
        apple:
          'To manage your payment methods in iTunes please <a href="https://support.apple.com/en-us/HT201266">follow this guide</a>.',
        google:
          'To manage your payment methods in Google Play please <a href="https://support.google.com/googleplay/answer/4646404">follow this guide</a>.',
      },
      vip: {
        message: 'VIP account types will not be charged using the payment details below.',
      },
    },
    card: {
      default: 'Default method',
      actions: {
        default: 'Use As Default',
        delete: 'Delete',
      },
    },
    goPremium: {
      heading: 'Go premium and never miss another set',
      subheading: 'Renew your Premium today',
      join_button: 'Join Premium',
      learn_more: 'Learn More',
      sl: {
        company_premium: 'Surfline Premium',
        upgrade_link: 'https://www.surfline.com',
        desc: 'Ad-free cams, expert forecasts, advanced forecast tools - ',
        benefitsLink: 'http://www.surfline.com/premium-benefits',
      },
      bw: {
        company_premium: 'Buoyweather Premium',
        upgrade_link: 'https://www.buoyweather.com',
        desc: 'Get point-based 16-day wind, wave and tide forecasts - ',
        benefitsLink: 'http://benefits.buoyweather.com/7-day-forecast/',
      },
      fs: {
        company_premium: 'FishTrack Premium',
        upgrade_link: 'https://www.fishtrack.com',
        desc: 'View the latest sea surface temperature and chlorophyll charts - ',
        benefitsLink: 'http://benefits.fishtrack.com/latest-sst-imagery/',
      },
    },
    previousSubscriptions: {
      heading: 'Previous Subscriptions',
      table: {
        heading_plan: 'Plan',
        heading_date: 'Date',
      },
    },
    plans: {
      daily: 'daily',
      weekly: 'weekly',
      monthly: 'monthly',
      yearly: 'yearly',
    },
    prevSub: {
      surfline_premium: 'Surfline Premium',
    },
    availablePlan: {
      default_description: 'Ad-free cams, expert forecasts, advanced forecast tools.',
    },
    annual_plan_switch_confirm_message: (planAmount, symbol) =>
      `By clicking "OK", you will change from monthly to annual billing. You will receive a credit for the reminder of your current monthly subscription which will be applied to today's charge of ${symbol}${parseFloat(
        planAmount / 100,
      ).toFixed(2)} for your annual subscription. To keep monthly billing, click "Cancel".`,
  },
  not_found: {
    title: 'Page Not Found',
    subheading: 'Yo, Johnny! See you in the next life!',
    paragraph_top: "The link you've followed may be broken, or the page may have been removed.",
    paragraph_bottom_before: 'Go ahead... go shred back to the ',
    paragraph_bottom_link: 'homepage',
    paragraph_bottom_after: ' for surf reports, forecasts and surf news.',
    button_text: 'Back to Surfline.com',
  },

  five_hundred: {
    title: 'Internal Server Error',
    subheading: 'Bodhi! This is your wakeup call I am an F... B... I... agent!',
    paragraph_top:
      "Looks like we're having a few issues here, thanks for noticing. Rest assured we've been notified of this problem.",
    paragraph_bottom_before: 'Checkout the ',
    paragraph_bottom_link: 'homepage',
    paragraph_bottom_after: ' for surf reports, forecasts and surf news.',
    button_text: 'Back to Surfline.com',
  },

  utils: {
    email: 'Not a valid email address format',
    required: 'Required',
    min_length_start: 'Minimum ',
    min_length_end: ' characters',
    max_length_start: 'Maximum ',
    max_length_end: ' characters',
    match: 'Do not match',
    integer: 'Must be a number',
    five_hundred_four: 'Server Timed Out. Please Try Again.',
    five_hundred: 'Internal Server Error',
    free_trial_text: 'Start your 15-day free trial of Surfline Premium',
    inactive_promotion: (brandName) =>
      `This promotion has expired, but you can still sign-up for ${brandName} Premium`,
  },

  facebook: {
    facebook_sign_in: 'Sign in with Facebook',
  },

  or_break: {
    or: 'or',
  },
  account: {
    header: {
      go_back_text: (brand) => `< Back to ${brand}`,
      go_back_link: (referrer, brand) => {
        if (
          referrer.includes(brand.toLowerCase()) &&
          !referrer.includes('/setup/favorite-surf-spots')
        ) {
          return referrer;
        }
        return config[`${brand.toLowerCase()}Home`];
      },
    },
    overview: {
      meta_title: 'Account Overview - Surfline',
      benefits_card: {
        message_calc: (days) =>
          `We\'re stoked that you have been a Premium member for ${days} days.`,
        message: "We're stoked to have you as a Premium member.",
      },
      promo_card: {
        sl_premium: {
          title: "You're Premium!",
          message: 'One of the benefits of being Premium is access to discounts on surf gear.',
          link_text: 'See Savings',
        },
        free: {
          title: 'Enjoy ad-free cams',
          message: '',
          link_text: 'Join Premium Now',
        },
        expired: {
          title: 'Enjoy ad-free cams',
          message: '',
          link_text: 'Join Premium Now',
        },
        expiring: {
          title: "Don't miss out",
          message: "Ads get in the way, don't miss out on the next set.",
          link_text: 'Stay Premium',
        },
      },
      info_card: {
        premium: {
          title: "You're Premium!",
          message: {
            sl: "We're stoked to have you as a Surfline Premium member.",
            bw: 'Thanks for being a Buoyweather Premium member.',
            fs: "We're glad to have you as a FishTrack Premium member.",
          },
          link_text: 'Premium Benefits',
          link_text_secondary: 'View More Discounts',
        },
        free: {
          title: 'Upgrade to Premium!',
          message: {
            sl: 'Enjoy ad-free cams, 16-day forecasts, discount codes and much more.',
            bw: 'Get point-based 16-day wind, wave and tide forecasts.',
            fs: 'View the latest sea surface temperature and chlorophyll charts.',
          },
          link_text: 'Add Premium',
          link_text_secondary: '',
        },
        expiring: {
          title: "Don't miss out",
          message: {
            sl: 'Keep your access to ad-free cams, 16-day forecasts and much more.',
            bw: 'Keep your access to point-based 16-day wind, wave and tide forecasts.',
            fs: 'Keep your access to the latest sea surface temperature and chlorophyll charts.',
          },
          link_text: 'Stay Premium',
          link_text_secondary: '',
        },
        expired: {
          title: 'Subscription Expired!',
          message: {
            sl:
              "You've lost access to ad-free HD cams and trusted forecasts created by Surfline's team of experts.",
            bw:
              "You've lost access to 16-day wind and wave forecasts, tides and premium weather charts.",
            fs:
              "You've lost access to the latest satellite fishing charts, premium overlays and marine weather.",
          },
          link_text: 'Add Premium',
          link_text_secondary: '',
        },
        discount: {
          title: 'Save 15% off surf gear',
          message:
            'Surfline Store powered by evo - UP TO 15% OFF.  Use the code below when you check out.',
          link_text: 'Get Code',
          link_text_secondary: 'More Discounts',
        },
        vip: {
          title: "You're a VIP Member!",
          message: {
            sl: 'Please enjoy your Premium-level access to 16-day forecasts and much more.',
            bw: 'Please enjoy your Premium-level access to 16-day wind, wave and tide forecasts.',
            fs:
              'Please enjoy your Premium-level access to the latest sea surface temperature and chlorophyll charts.',
          },
          link_text: 'Premium Benefits',
          link_text_secondary: 'View More Discounts',
        },
      },
      subs_card: {
        message: (days, onTrial) => {
          const delimiter = days > 1 || days === 0 ? 'days' : 'day';
          return `You have ${days} ${delimiter} remaining on your ${
            onTrial ? 'free trial' : 'subscription'
          }.`;
        },
        link_text: 'Manage Subscription',
        add_premium: 'Add Premium',
        renew: 'Renew Premium',
        stripe: 'Premium Member',
        apple: 'Premium Member',
        google: 'Premium Member',
        vip: 'VIP Member',
      },
      statusCard: {
        expired: {
          title: 'Your Premium memebership has expired.',
          message: () =>
            "You've lost access to ad-free HD cams and the world's most trusted forecast created by Surfline's team of experts.",
          link_text: 'Renew Premium',
        },
        expiring: {
          message: (days) =>
            `Your Premium subscription has been canceled and is set to expire in ${days} ${
              days === 1 ? 'day' : 'days'
            }. You are about to lose your edge. Resubscribe to keep the waves coming.`,
          link_text: 'Stay Premium',
        },
        delinquent: {
          message: (days, brand) =>
            `There was a billing error and your ${brand} Premium membership will expire in ${days} ${
              days === 1 ? 'day' : 'days'
            }.`,
          link_text: 'Update Payment Details',
        },
        monthly: {
          message: () => 'Switch to yearly payments and $84.',
          link_text: 'Go Yearly and Save $84',
        },
      },
      failedPaymentMessage: {
        title: 'payment failure',
        subtitle: (brand) => `Update payment details to keep ${brand} Premium`,
      },
    },
  },
};
