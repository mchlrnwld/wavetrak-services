import base from '../base';
import {
  Satellite,
  Layers,
  Waypoint,
  FishtrackLogo,
  CurrentsAlt as PromotionCurrents,
  SatelliteAlt as PromotionSatellite,
  WaypointAlt as PromotionWaypoint
} from '../../../components/Icons';
import config from '../../../config';

export default {
  index: {
    ...base.index,
    browserUpdateTextBefore: 'You are using an outdated browser. FishTrack no longer supports this browser. Please ',
  },

  app: {
    ...base.app,
  },

  upgradeFunnel: {
    upgradeHeaderSubstitle: 'Fishtrack plans start at $7.99/mo'
  },

  duplicateCard: {
    ...base.duplicateCard
  },

  premiumFeatures: {
    features: [
      {
        Icon: Satellite,
        header: 'Latest Satellite Imagery',
        description: 'Locate temperature and color breaks offshore.'
      },
      {
        Icon: Waypoint,
        header: 'Waypoints And Routes',
        description: 'Measure distances and plot routes for trips offshore.'
      },
      {
        Icon: Layers,
        header: 'Currents, Bathymetry And Altimetry',
        description: 'Use premium overlays to pinpoint areas of activity.'
      },
    ]
  },
  promotionFeatures: {
    BrandLogo: FishtrackLogo,
    header: 'Get everything you need to find more fish',
    features: [
      {
        Icon: PromotionSatellite,
        header: 'Satellite Imagery',
      },
      {
        Icon: PromotionWaypoint,
        header: 'Waypoint and Routes',
      },
      {
        Icon: PromotionCurrents,
        header: 'Currents and Bathymetry',
      }
    ],
    promoCodeError: 'Invalid Code. To receive this offer you must enter a valid code. If you do not have one, you can still upgrade to FishTrack Premium at www.fishtrack.com/upgrade/'
  },
  giftCards: {
    ...base.giftCards,
    backgroundImage: `url(${config.cdnHost}static/gifts/fs/rail_cta_desktop_bg_image.jpg)`,
    layeredCardImage: `url(${config.cdnHost}static/gifts/fs/rail_cta_desktop_cards.png)`,
    brandIcon: 'url(https://wa.cdn-surfline.com/account/static/fishtrack-icon.svg)'
  },
  rail_cta: {
    ...base.rail_cta.fs
  },

  create: {
    ...base.create,
    meta_title: 'Create Account - FishTrack',
    meta_description: 'Find more fish – create an account with FishTrack to gain instant access to the latest satellite imagery.',

    form: {
      ...base.create.form,
      disclaimer_text_start: 'By clicking "Create Account" you agree to FishTrack ',
    }
  },

  create_funnel: {
    ...base.create_funnel,
    meta_title: 'Create Your Premium Account - FishTrack',
    meta_description: 'FishTrack Fishing Charts - Create a FishTrack account to view the latest SST, Chlorophyll and True-Color satellite charts.',
    title_no_trial: 'Join FishTrack Premium',
  },

  sign_in: {
    ...base.sign_in,
    meta_title: 'Sign In - FishTrack',
    meta_description: 'Sign in to FishTrack and find fish faster offshore.',

    form: {
      ...base.sign_in.form,
      disclaimer_text_start: 'If you click "Sign In with Facebook" and are not a FishTrack user, you will be registered and you agree to FishTrack ',
    }
  },

  sign_in_funnel: {
    ...base.sign_in_funnel,
    title_no_trial: 'Sign in to join<br/>FishTrack Premium',
    meta_title: 'Sign In to Go Premium - FishTrack',
    meta_description: 'Get the latest satellite imagery - sign in with your FishTrack account to go Premium.',
  },

  payment_form: {
    ...base.payment_form,
    meta_title: 'Start Your Premium Trial - FishTrack',
    meta_description: 'FishTrack Fishing Charts - Create a FishTrack account to view the latest SST, Chlorophyll and True-Color satellite charts.',
    meta_description_resume: 'Renew FishTrack Premium',
    title_resume_premium: 'Don’t miss a hot bite, renew FishTrack Premium',
    title_no_trial: 'Join FishTrack Premium',
    radio_plan_select: {
      ...base.payment_form.radio_plan_select,
    },
    stripe: {
      ...base.payment_form.stripe,
    },
    discount: {
      ...base.payment_form.discount,
      success_message_plans: 'Success! Your discount code has been applied.',
      form: {
        ...base.payment_form.discount.form,
      }
    },
  },

  payment_success: {
    subTitle: 'Welcome to Fistrack Premium!',
    message: 'Thank you for joining the Fistrack family.  You can now access the latest satellite fishing chards and overlays.  See you in the water.',
    linkText: 'Return to Fishtrack',
    linkAddress: 'https://fishtrack.com',
  },

  forgot_password: {
    ...base.forgot_password,
    enter_code: {
      ...base.forgot_password.enter_code,
      meta_title: 'Enter Code - FishTrack',
    },
    enter_email: {
      ...base.forgot_password.enter_email,
      meta_title: 'Forgot Password - FishTrack',
    },
    set_password: {
      ...base.forgot_password.set_password,
      meta_title: 'Create New Password - FishTrack',
    },
    email_form: {
      ...base.forgot_password.email_form,
    },
    passcode_form: {
      ...base.forgot_password.passcode_form,
    },
    reset_password_form: {
      ...base.forgot_password.reset_password_form,
    }
  },

  edit_profile: {
    ...base.edit_profile,
    meta_title: {
      ...base.edit_profile.meta_title,
    },
    form: {
      ...base.edit_profile.form,
    }
  },

  edit_settings: {
    ...base.edit_settings,
    meta_title: {
      ...base.edit_settings.meta_title,
    },
    form: {
      ...base.edit_settings.form,
    }
  },

  favorites: {
    ...base.favorites,
    meta_title: {
      ...base.favorites.meta_title,
    },
  },

  change_password: {
    ...base.change_password,
    meta_title: {
      ...base.change_password.meta_title,
    },
    form: {
      ...base.change_password.form,
    }
  },

  subscription: {
    ...base.subscription,
    title: {
      ...base.subscription.title,
    },
    meta_title: {
      ...base.subscription.meta_title,
    },
    availablePlans: {
      ...base.subscription.availablePlans,
    },
    subheading_vip: {
      ...base.subscription.subheading_vip,
    },
    expiring_message: (renewDate, brand) => `Your ${brand} subscription will expire on ${renewDate}. Stay Premium to keep Satellite imagery, Waypoints, Routes, and more.`,
    cards: {
      ...base.subscription.cards,
      iap_message: {
        ...base.subscription.cards.iap_message,
      },
      iap_message_link: {
        ...base.subscription.cards.iap_message_link,
      },
      vip: {
        ...base.subscription.cards.vip,
      },
    },
    card: {
      ...base.subscription.card,
      actions: {
        ...base.subscription.card.actions,
      }
    },
    goPremium: {
      ...base.subscription.goPremium,
      sl: {
        ...base.subscription.goPremium.sl,
      },
      bw: {
        ...base.subscription.goPremium.bw,
      },
      fs: {
        ...base.subscription.goPremium.fs,
      },
    },
    previousSubscriptions: {
      ...base.subscription.previousSubscriptions,
      table: {
        ...base.subscription.table,
      }
    },
    plans: {
      ...base.subscription.plans,
    },
    prevSub: {
      ...base.subscription.prevSub,
    },
    availablePlan: {
      ...base.subscription.availablePlan,
    }
  },
  not_found: {
    ...base.not_found,
    button_text: 'Back to FishTrack.com',
  },

  five_hundred: {
    ...base.five_hundred,
    button_text: 'Back to FishTrack.com',
  },

  utils: {
    ...base.utils,
  },

  facebook: {
    ...base.facebook,
  },

  or_break: {
    ...base.or_break,
  },
  account: {
    ...base.account,
    overview: {
      ...base.account.overview,
      meta_title: 'Account Overview - FishTrack',
      benefits_card: {
        ...base.account.overview.benefits_card,
        price: {
          ...base.account.overview.benefits_card.price,
        },
      },
      promo_card: {
        ...base.account.overview.promo_card,
        sl_premium: {
          ...base.account.overview.promo_card.sl_premium,
        },
        free: {
          ...base.account.overview.promo_card.free,
        },
        expired: {
          ...base.account.overview.promo_card.expired,
        },
        expiring: {
          ...base.account.overview.promo_card.expiring,
        }
      },
      info_card: {
        ...base.account.overview.info_card,
        premium: {
          ...base.account.overview.info_card.premium,
          message: {
            ...base.account.overview.info_card.premium.message,
          }
        },
        free: {
          ...base.account.overview.info_card.free,
          message: {
            ...base.account.overview.info_card.free.message,
          }
        },
        expiring: {
          ...base.account.overview.info_card.expiring,
          message: {
            ...base.account.overview.info_card.expiring.message,
          }
        },
        expired: {
          ...base.account.overview.info_card.expired,
          message: {
            ...base.account.overview.info_card.expired.message,
          }
        },
        discount: {
          ...base.account.overview.info_card.discount,
        }
      },
      subs_card: {
        ...base.account.overview.subs_card,
      },
      statusCard: {
        ...base.account.overview.statusCard,
        expired: {
          ...base.account.overview.statusCard.expired,
        },
        expiring: {
          ...base.account.overview.statusCard.expiring,
        },
        delinquent: {
          ...base.account.overview.statusCard.delinquent,
        },
        monthly: {
          ...base.account.overview.statusCard.monthly,
        }
      }
    }
  }
};
