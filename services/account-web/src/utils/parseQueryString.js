import urlCheck from './urlCheck';

export default function parseRedirect(splitQuery) {
  function parseQueryString(splitUrlQuery) {
    const paramsObj = {
      redirectLocation: '/',
      funnelredirectLocation: null
    };
    if (!splitUrlQuery || splitUrlQuery === '') {
      return paramsObj;
    }
    const qsObj = {};
    for (let ii = 0; ii < splitUrlQuery.length; ii++) {
      const param = splitUrlQuery[ii].split('=', 2);
      if (param.length > 1) {
        qsObj[param[0]] = decodeURIComponent(param[1].replace(/\+/g, ' '));
      }
    }
    if (qsObj.redirectUrl) {
      if (urlCheck(qsObj.redirectUrl)) {
        paramsObj.redirectLocation = qsObj.redirectUrl;
      }
    }
    return paramsObj;
  }

  return Promise.resolve(parseQueryString(splitQuery));
}
