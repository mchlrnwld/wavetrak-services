import superagent from 'superagent';
import { trackEvent } from '@surfline/web-common';
import en from '../international/translations/en';
import config from '../config';

export default (values) => {
  return new Promise((resolve, reject) => {
    superagent
      .get(`${config.servicesEndpoint}/password-reset`)
      .query({
        email: values.email,
        brand: config.company,
      })
      .end((err, response) => {
        if (err) {
          if (!response) {
            reject({
              passCode: ' ',
              _error: en.utils.five_hundred,
            });
          } else if (response.statusCode === 400) {
            trackEvent('Invalid Email Forgot Password', {
              reason: 'email not associated with account',
              email: values.email,
            });
            reject({
              email: en.forgot_password.enter_email.email_error,
            });
          } else if (response.status === 504) {
            reject({
              password: ' ',
              email: ' ',
              _error: en.utils.five_hundred_four,
            });
          } else {
            reject({
              password: ' ',
              email: ' ',
              _error: en.utils.five_hundred,
            });
          }
        } else {
          resolve(response);
        }
      });
  });
};
