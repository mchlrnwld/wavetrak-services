import readableInterval from './readableInterval';

export default (sub) => {
  const identifier = sub.type === 'stripe' ? sub.plan.interval : sub.planId;
  return readableInterval(identifier);
};
