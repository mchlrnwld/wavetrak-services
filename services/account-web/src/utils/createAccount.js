import * as userApi from '../common/api/user';
import en from '../international/translations/en';
import config from '../config';
import identifyUser from './identifyUser';

export default function createAccount(values) {
  return new Promise(async(resolve, reject) => {
    try {
      const isShortLived = !values.staySignedIn;
      const userInfo = {
        firstName: values.first,
        lastName: values.last,
        email: values.email,
        password: values.password,
        brand: config.company,
        settings: {
          receivePromotions: values.receivePromotions
        }
      };

      const postUserResponse = await userApi.postUser(isShortLived, userInfo);
      identifyUser(postUserResponse.user._id);
      resolve(postUserResponse);
    } catch (err) {
      if (err.status === 400) {
        reject({email: en.create.error_email_in_use});
      } else if (err.status === 504) {
        reject({
          password: ' ',
          email: ' ',
          first: ' ',
          last: ' ',
          _error: en.utils.five_hundred_four
        });
      } else {
        reject({
          password: ' ',
          email: ' ',
          first: ' ',
          last: ' ',
          _error: en.utils.five_hundred
        });
      }
    }
  });
}
