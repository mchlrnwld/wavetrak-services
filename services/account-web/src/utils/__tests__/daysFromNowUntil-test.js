import { expect } from 'chai';
import daysFromNowUntil from '../daysFromNowUntil';

describe('Utils / daysFromNowUntil', () => {
  const fiveDaysFromNow = new Date(Date.now() + (5 * 1000 * 60 * 60 * 24));
  const fiveDaysFromNowString = `${fiveDaysFromNow.getMonth() + 1}/${fiveDaysFromNow.getDate()}/${fiveDaysFromNow.getFullYear()}`;
  const fiveAndHalfDaysFromNow = new Date(fiveDaysFromNow.getTime() + (500 * 60 * 60 * 24));
  const yesterday = new Date(Date.now() - (1000 * 60 * 60 * 24));

  it('should return 0 if endDate is not a valid date', () => {
    expect(daysFromNowUntil('invalid date')).to.equal(0);
  });

  it('should return number of days from now until endDate', () => {
    expect(daysFromNowUntil(fiveDaysFromNow)).to.equal(5);
  });

  it('should parse date strings', () => {
    expect(daysFromNowUntil(fiveDaysFromNowString)).to.equal(5);
  });

  it('should round up partial days', () => {
    expect(daysFromNowUntil(fiveAndHalfDaysFromNow)).to.equal(6);
  });

  it('should return 0 if endDate is in the past', () => {
    expect(daysFromNowUntil(yesterday)).to.equal(0);
  });
});
