import { expect } from 'chai';
import {
  sortBrandSubsByPeriodEnd,
  findOnlyIAPSubType,
  hasSurflinePremiumEntitlement,
  findFailedPaymentAttempts,
  upgradesAvailable
} from '../formatSubscriptionData';

describe('Utils / formatSubscriptionData / sortBrandSubsByPeriodEnd', () => {
  const subObj = {
    'sl': [{periodEnd: '2016-14-09T23:05:48.000Z'}, {periodEnd: '2016-12-09T23:05:48.000Z'}],
    'bw': [{periodEnd: '2016-12-09T23:05:48.000Z'}],
    'fs': [],
  };

  it('should return brand subs sorted by periodEnd', () => {
    expect(sortBrandSubsByPeriodEnd(subObj)).to.deep.equal({
      'sl': [{periodEnd: '2016-14-09T23:05:48.000Z'}, {periodEnd: '2016-12-09T23:05:48.000Z'}],
      'bw': [{periodEnd: '2016-12-09T23:05:48.000Z'}],
      'fs': [],
    });
  });
});

describe('Utils / formatSubscriptionData / findOnlyIAPSubType', () => {
  it('should return true if all subscriptions are iap', () => {
    const subscriptions = [
      {entitlement: 'sl_premium', type: 'google'},
      {entitlement: 'bw_premium', type: 'apple'},
      {entitlement: 'fs_premium', type: 'apple'}
    ];

    expect(findOnlyIAPSubType(subscriptions)).to.equal(true);
  });

  it('should return false if all subscriptions are not iap', () => {
    const subscriptions = [
      {entitlement: 'sl_premium', type: 'google'},
      {entitlement: 'bw_premium', type: 'apple'},
      {entitlement: 'fs_premium', type: 'stripe'}
    ];

    expect(findOnlyIAPSubType(subscriptions)).to.equal(false);
  });
});

describe('Utils / formatSubscriptionData / hasSurflinePremiumEntitlement', () => {
  it('should return true if at least one sub has surfline premium entitlement', () => {
    const subscriptions = [
      {entitlement: 'sl_premium', type: 'google'},
      {entitlement: 'bw_premium', type: 'apple'},
      {entitlement: 'fs_premium', type: 'apple'}
    ];

    expect(hasSurflinePremiumEntitlement(subscriptions)).to.equal(true);
  });

  it('should return false if at least one sub does not have surfline premium entitlement', () => {
    const subscriptions = [
      {entitlement: 'sl_basic', type: 'google'},
      {entitlement: 'bw_premium', type: 'apple'},
      {entitlement: 'fs_premium', type: 'stripe'}
    ];

    expect(hasSurflinePremiumEntitlement(subscriptions)).to.equal(false);
  });
});

describe('Utils / formatSubscriptionData / findFailedPaymentAttempts', () => {
  it('should return true if sub has at least one failedPaymentAttempt', () => {
    const subscriptions = [
      {entitlement: 'sl_premium', type: 'google', failedPaymentAttempts: 1},
      {entitlement: 'bw_premium', type: 'apple', failedPaymentAttempts: 0},
      {entitlement: 'fs_premium', type: 'apple'}
    ];

    expect(findFailedPaymentAttempts(subscriptions)).to.equal(true);
  });

  it('should return false if sub has no failedPaymentAttempts', () => {
    const subscriptions = [
      {entitlement: 'sl_basic', type: 'google', failedPaymentAttempts: 0},
      {entitlement: 'bw_premium', type: 'apple', failedPaymentAttempts: 0},
      {entitlement: 'fs_premium', type: 'stripe'}
    ];

    expect(findFailedPaymentAttempts(subscriptions)).to.equal(false);
  });
});

describe('Utils / formatSubscriptionData / upgradesAvailable', () => {
  it('should return true if at least one brand has no subs', () => {
    const subscriptions = {
      'sl': [{periodEnd: '2016-14-09T23:05:48.000Z'}, {periodEnd: '2016-12-09T23:05:48.000Z'}],
      'bw': [{periodEnd: '2016-12-09T23:05:48.000Z'}],
      'fs': [],
    };

    expect(upgradesAvailable(subscriptions)).to.equal(true);
  });

  it('should return false if all brand have at least one sub', () => {
    const subscriptions = {
      'sl': [{periodEnd: '2016-14-09T23:05:48.000Z'}, {periodEnd: '2016-12-09T23:05:48.000Z'}],
      'bw': [{periodEnd: '2016-12-09T23:05:48.000Z'}],
      'fs': [{periodEnd: '2016-12-09T23:05:48.000Z'}]
    };

    expect(upgradesAvailable(subscriptions)).to.equal(false);
  });
});
