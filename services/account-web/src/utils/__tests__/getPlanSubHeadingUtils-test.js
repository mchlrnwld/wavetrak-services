import { expect } from 'chai';
import { amountToFixed, getSubscriptionPrice, parseBillingDate } from '../getPlanSubHeadingUtils';

describe('Utils / getPlanSubHeadingUtils / amountToFixed', () => {
  it('should return a fixed dollar amount', () => {
    expect(amountToFixed(6999)).to.equal('$69.99');
  });
});

describe('Utils / getPlanSubHeadingUtils / getSubscriptionPrice', () => {
  const subscriptionIAP = {type: 'apple'};
  /* Phantom cannot test Array.prototype.find */
  it('should return null if sub type is not stripe', () => {
    expect(getSubscriptionPrice(subscriptionIAP)).to.equal(null);
  });
});

describe('Utils / getPlanSubHeadingUtils / parseBillingDate', () => {
  const date = '2016-12-09T23:05:48.000Z';

  it('should return a date in the format m-d-yy', () => {
    expect(parseBillingDate(date)).to.equal('12-9-16');
  });
});
