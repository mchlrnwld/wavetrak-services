import en from '../international/translations/en';
import getFreeTrialText from './getFreeTrialText';

const getDefaultHeaderText = (isLoggedIn, trialEligible, showLogin) => {
  let title;
  if (isLoggedIn) {
    title = trialEligible ? getFreeTrialText() : en.payment_form.title_no_trial;
    return title;
  }
  if (showLogin) {
    title = trialEligible ? en.sign_in_funnel.title : en.sign_in_funnel.title_no_trial;
  }
  title = trialEligible ? getFreeTrialText() : en.create_funnel.title_no_trial;
  return title;
};

export default function getHeaderText(isLoggedIn, trialEligible, showLogin, funnelConfig) {
  if (funnelConfig) {
    if (isLoggedIn) {
      return funnelConfig.paymentHeader
        ?
        funnelConfig.paymentHeader :
        getDefaultHeaderText(isLoggedIn, trialEligible, showLogin);
    }
    return funnelConfig.signUpHeader ?
      funnelConfig.signUpHeader : getDefaultHeaderText(isLoggedIn, trialEligible, showLogin);
  }

  return getDefaultHeaderText(isLoggedIn, trialEligible, showLogin);
}
