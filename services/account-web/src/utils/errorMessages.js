export const messages = new Map();

// Add mongo error messages here
messages.set('DuplicateKey', 'Email address already exists');

export const resolveError = error => {
  const newError = Object.assign(error);
  if (error.name === 'MongoError') {
    newError.message = messages.get(error.codeName);
  }
  return newError;
};
