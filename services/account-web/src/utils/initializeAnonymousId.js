import { canUseDOM, getWavetrakIdentity, LOGGED_IN } from '@surfline/web-common';

const canUseAnalytics = !!(canUseDOM && window.analytics && window.analytics.user);

export default function (sourceOrigin = null) {
  const identity = getWavetrakIdentity();
  if (canUseAnalytics) {
    if (identity?.type === LOGGED_IN) {
      window.analytics.identify(identity.userId, { sourceOrigin });
    }
  }
}
