import { trackEvent } from '@surfline/web-common';

export const getPlan = (plans, planId) => plans.find((plan) => plan.id === planId);

export const getSubId = (subs, planId) => {
  const subscription = subs.find((sub) => sub.planId === planId);
  return subscription?.subscriptionId;
};

export const trackAddedProduct = (subscriptionObj, subscriptionPlanId, trialEligible) => {
  const plan = getPlan(subscriptionObj.availablePlans, subscriptionPlanId);
  trackEvent('Added Product', {
    id: plan.id,
    sku: plan.id,
    name: plan.name,
    price: plan.amount / 100,
    category: 'Subscription',
    interval: plan.interval,
    intervalCount: plan.intervalCount,
    trialEligible,
    currency: plan.currency,
    quantity: 1,
  });
};

export const trackCompletedOrder = (subscriptionObj, planId) => {
  const subId = getSubId(subscriptionObj.subscriptions, planId);
  const plan = getPlan(subscriptionObj.availablePlans, planId);

  trackEvent('Completed Checkout Step', {
    step: 2,
  });

  trackEvent('Completed Order', {
    orderId: subId,
    total: 0,
    // discount: '', TODO update once we have discount codes working
    // coupon: '', TODO update once we have coupon codes working
    currency: plan.currency,
    products: [
      {
        id: plan.id,
        sku: plan.id,
        name: plan.name,
        price: plan.amount / 100,
        quantity: 1,
        category: 'Subscription',
      },
    ],
  });
};

export const trackFailedOrder = (error) => {
  if (error) {
    trackEvent('Failed Order', {
      code: error.code,
      reason: error.message,
    });
  }
};
