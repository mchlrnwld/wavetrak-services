import cookie from 'react-cookie';
import urlCheck from './urlCheck';
import config from '../config';

export default () => {
  // Set the fallback redirect as the base homepage
  let redirectLocation = config.redirectUrl;

  const storedRedirect = sessionStorage.getItem('redirectUrl');
  const cookieRedirect = cookie.load('redirectUrl');

  if (storedRedirect && urlCheck(storedRedirect, redirectLocation)) {
    redirectLocation = storedRedirect;
  } else if (cookieRedirect && urlCheck(cookieRedirect, redirectLocation)) {
    redirectLocation = cookieRedirect;
  }
  window.location.assign(redirectLocation);
};
