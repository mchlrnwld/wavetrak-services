import validateEmail from './signInUtils/validateEmail';
import authorizeToken from './signInUtils/authorizeToken';
import getUser from './signInUtils/getUser';
import getSubscriptions from './signInUtils/getSubscriptions';
import identifyUser from './identifyUser';
import trackSignIn from './trackSignIn';

const signIn = async (values, location) => {
  const { facebookId } = values;

  if (!facebookId) {
    await validateEmail(values);
  }

  const authResponse = await authorizeToken(values);
  const authInfo = await getUser(values, authResponse);
  identifyUser(authInfo.userId);
  trackSignIn(values);

  if (location === 'funnel') {
    const subInfo = await getSubscriptions(authInfo);
    return subInfo;
  }

  return authInfo;
};

export default signIn;
