import cookie from 'react-cookie';
import config from '../config';

export default async (accessToken, refreshToken, maxAge, refreshMaxAge = 31536000) => {
  const domainName = config.company === 'fs' ? '.fishtrack.com' : location.hostname;
  cookie.save('access_token', accessToken, {
    path: '/',
    maxAge: maxAge,
    expires: new Date((new Date).getTime() + (maxAge * 1000)),
    domain: domainName,
    secure: false
  });

  cookie.save('refresh_token', refreshToken, {
    path: '/',
    maxAge: refreshMaxAge,
    expires: new Date((new Date).getTime() + (refreshMaxAge * 1000)),
    domain: domainName,
    secure: false
  });

  return { accessToken, refreshToken };
};
