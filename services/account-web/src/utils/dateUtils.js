export const months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

export const getYears = () => {
  const currentYear = new Date().getFullYear();
  const years = [];
  const maxYears = 20;
  let incrementor = 0;
  while (incrementor <= maxYears) {
    const futureYear = currentYear + incrementor;
    years.push(futureYear);
    incrementor += 1;
  }
  return years;
};
