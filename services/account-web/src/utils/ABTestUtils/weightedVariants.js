export function createWeightedSubintervals(weights) {
  const subintervals = new Array(weights.length);
  for (let i = 0; i < subintervals.length; i++) { /* eslint id-length:0 */
    const previousSubinterval = i > 0 ? subintervals[i - 1] : { min: 0, max: 0 };
    const min = previousSubinterval.max;
    const max = min + weights[i];
    subintervals[i] = { min, max };
  }
  return subintervals;
}

export function weightedRandom(weights) {
  const weightedSubintervals = createWeightedSubintervals(weights);
  const pureRandomValue = Math.random();
  const resultIndex = weightedSubintervals.findIndex(subInterval => {
    return subInterval.min <= pureRandomValue && pureRandomValue < subInterval.max;
  });
  return resultIndex / weights.length;
}
