export const sortBrandsByCompanyConfig = () => {
  return ['sl', 'bw', 'fs'];
};

const compareSubDates = (subA, subB) => new Date(subA.periodEnd) - new Date(subB.periodEnd);

export const sortBrandSubsByPeriodEnd = (subObj) => {
  for (const brand in subObj) {
    if (subObj[brand].length) {
      subObj[brand].sort(compareSubDates);
    }
  }
  return subObj;
};

export const filterByBrand = (brand, itemToFilterBy, element) => {
  let result = false;
  if (element[itemToFilterBy]) result = element[itemToFilterBy].startsWith(brand);
  return result;
};

export const filterByBrandWrapper = (itemToFilter, brand, propToFilterBy) => {
  return itemToFilter.filter(filterByBrand.bind(this, brand, propToFilterBy));
};

export const findStripeSubType = (subs) => {
  let result = true;
  subs.forEach((element) => {
    if (element.type === 'stripe') result = false;
  });
  return result;
};

export const hasSurflinePremiumEntitlement = (subs) => {
  let result = false;
  subs.forEach((element) => {
    if (element.entitlement === 'sl_premium' || element.entitlement === 'sl_vip') result = true;
  });
  return result;
};

export const getNonStripeSubType = (subscriptions) => {
  let subType;
  subscriptions.forEach((element) => {
    if (element.type === 'apple' || element.type === 'google' || element.type === 'vip')
      subType = element.type;
  });
  return subType;
};

export const findOnlyIAPSubType = (subs) => {
  let result = true;
  if (subs.length < 1 || !findStripeSubType(subs)) {
    result = false;
  }
  return result;
};

export const findFailedPaymentAttempts = (subs) => {
  let result = false;
  subs.forEach((element) => {
    if (element.failedPaymentAttempts > 0) result = true;
  });
  return result;
};

export const findFailedPaymentAttemptsWrapper = (subs) => {
  let result = false;
  if (findFailedPaymentAttempts(subs)) {
    result = false;
  }
  return result;
};

export const upgradesAvailable = (subs) => {
  let upgradable = false;
  for (const brand in subs) {
    if (subs[brand].length < 1) {
      upgradable = true;
    }
  }
  return upgradable;
};

const formatSubscriptionData = (sub) => {
  let subscription = sub;
  const stripeCustomer = !!subscription.stripeCustomerId;
  const sortedBrands = sortBrandsByCompanyConfig();
  const hasActiveSubscription = subscription.subscriptions.length > 0;
  const hasMultipleSubscriptions = subscription.subscriptions.length > 1;
  const hasSurflinePremium = hasSurflinePremiumEntitlement(subscription.subscriptions);
  const subTypeIAPOnly = findOnlyIAPSubType(subscription.subscriptions);
  const nonStripeSubType = getNonStripeSubType(subscription.subscriptions);
  const failedPaymentAttempts = findFailedPaymentAttemptsWrapper(subscription.subscriptions);
  const filteredPlansByBrand = {};
  const filteredSubsByBrand = {};

  sortedBrands.map((brand) => {
    filteredSubsByBrand[brand] = filterByBrandWrapper(
      subscription.subscriptions,
      brand,
      'entitlement',
    );
  });
  const sortedSubsByBrand = sortBrandSubsByPeriodEnd(filteredSubsByBrand);

  sortedBrands.map((brand) => {
    filteredPlansByBrand[brand] = filterByBrandWrapper(subscription.availablePlans, brand, 'brand');
  });

  const upgradable = upgradesAvailable(filteredSubsByBrand);

  subscription = {
    ...subscription,
    subscriptions: sortedSubsByBrand,
    availablePlans: filteredPlansByBrand,
    companyMap: sortedBrands,
    hasActiveSubscription,
    nonStripeSubType,
    failedPaymentAttempts,
    upgradable,
    hasMultipleSubscriptions,
    hasSurflinePremium,
    stripeCustomer,
    subTypeIAPOnly,
  };

  return subscription;
};

export default formatSubscriptionData;
