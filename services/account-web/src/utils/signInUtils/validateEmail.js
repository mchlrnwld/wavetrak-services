/* eslint-disable no-throw-literal */

import { trackEvent } from '@surfline/web-common';

import * as userApi from '../../common/api/user';
import en from '../../international/translations/en';

const validateEmail = async (values) => {
  try {
    await userApi.validateEmail(values.email);
    return true;
  } catch (err) {
    if (err.status === 400) {
      trackEvent('Sign In Form Invalid Email', {
        reason: 'account doesn’t exist with email',
        email: values.email,
        location: values.location,
      });
      throw {
        email: en.sign_in.error_email_does_not_exist,
      };
    } else if (err.status === 504) {
      throw {
        password: ' ',
        email: ' ',
        _error: en.utils.five_hundred_four,
      };
    } else {
      throw {
        password: ' ',
        email: ' ',
        _error: en.utils.five_hundred,
      };
    }
  }
};

export default validateEmail;
