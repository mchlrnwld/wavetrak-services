import * as userApi from '../../common/api/user';
import en from '../../international/translations/en';

export default function authorizeToken(values, authResponse) {
  return new Promise(async(resolve, reject) => {
    try {
      const userResponse = await userApi.getUser(authResponse.access_token);
      const authInfo = {
        accessToken: authResponse.access_token,
        refreshToken: authResponse.refresh_token,
        expiresIn: authResponse.expires_in,
        userId: userResponse._id,
        first: userResponse.firstName,
        canUpdate: null,
        redirectAway: null
      };
      resolve(authInfo);
    } catch (err) {
      reject({
        password: ' ',
        email: ' ',
        _error: en.sign_in.error_form_error
      });
    }
  });
}
