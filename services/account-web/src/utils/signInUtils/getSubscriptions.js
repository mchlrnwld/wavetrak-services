import * as subscriptionApi from '../../common/api/subscription';
import filterBrandSubs from '../../common/utils/filterBrandSubs';
import en from '../../international/translations/en';

export default async function getSubscriptions(authInfo) {
  try {
    const { accessToken } = authInfo;
    const subResponse = await subscriptionApi.getSubscription(accessToken);
    const brandSubs = filterBrandSubs(subResponse);

    if (brandSubs.length <= 0) {
      return {
        ...authInfo,
        canUpdate: true,
      };
    }

    return {
      ...authInfo,
      brandSubs,
      redirectAway: '/account/overview',
    };
  } catch (err) {
    return {
      password: ' ',
      email: ' ',
      _error: en.utils.five_hundred
    };
  }
}
