import * as authApi from '../../common/api/auth';
import en from '../../international/translations/en';
import config from '../../config';
import deviceInfo from '../../common/utils/deviceInfo';

export default function authorizeToken(values) {
  return new Promise(async (resolve, reject) => {
    try {
      const isShortLived = !values.staySignedIn;
      const { deviceId, deviceType } = deviceInfo();
      const userInfo = {
        grant_type: 'password',
        username: values.email,
        password: values.password,
        facebookId: values.facebookId,
        fbToken: values.accessToken,
        firstName: values.first_name,
        lastName: values.last_name,
        brand: values.brand,
        client_id: values.client_id,
        device_id: deviceId,
        device_type: deviceType,
        forced: values.forced
      };

      let formBody = [];
      for (const property in userInfo) {
        if (property !== null) {
          const encodedKey = encodeURIComponent(property);
          const encodedValue = encodeURIComponent(userInfo[property]);
          formBody.push(`${encodedKey}=${encodedValue}`);
        }
      }
      formBody = formBody.join('&');
      const authResponse = await authApi.generateToken(isShortLived, formBody);
      resolve(authResponse);
    } catch (err) {
      if (err.status === 400) {
        reject({
          password: ' ',
          email: ' ',
          _error: en.sign_in.error_form_error
        });
      } else if (err.status === 504) {
        reject({
          password: ' ',
          email: ' ',
          _error: en.utils.five_hundred_four
        });
      } else if (err.status === 403) {
        reject({
          password: values.password,
          email: values.email,
          _error_code: err.code,
          _error_description: err.error_description
        });
      } else {
        reject({
          password: ' ',
          email: ' ',
          _error: en.utils.five_hundred
        });
      }
    }
  });
}
