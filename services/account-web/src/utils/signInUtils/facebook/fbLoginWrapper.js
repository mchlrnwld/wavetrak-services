export default function fbLoginWrapper() {
  return new Promise(async (resolve, reject) => {
    /* FB available via Facebook SDK loaded into HMTL.js from /src/common/components/FacebookBrowser */
    FB.login((response) => { // eslint-disable-line no-undef
      if (response.authResponse) {
        resolve(response);
      } else {
        reject();
      }
    }, { scope: 'public_profile, email' });
  });
}
