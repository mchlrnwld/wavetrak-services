import config from '../../../config';

export default function fbUserInfoWrapper(loginResponse, formLocation) {
  return new Promise((resolve, reject) => {
    /* FB available via Facebook SDK loaded into HMTL.js from /src/common/components/FacebookBrowser */
    FB.api('/me', {fields: 'name, email, first_name, last_name'}, (userInfo) => { // eslint-disable-line no-undef
      const signInValues = {
        ...loginResponse.authResponse,   // contains facebook access_token
        brand: config.company,           // used for backend tracking
        facebookId: userInfo.id,         // used by backend to determine auth method (oauth vs fb)
        email: userInfo.email,
        first_name: userInfo.first_name,
        last_name: userInfo.last_name,
        staySignedIn: true,             // generates long-lived surfline access token
        location: formLocation          // determines whether to call sub service
      };
      if (signInValues.email) {
        resolve(signInValues);
      } else {
        reject({ status: 400, _error: 'An email address is required' });
      }
    });
  });
}
