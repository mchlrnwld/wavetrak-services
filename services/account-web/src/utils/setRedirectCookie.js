import cookie from 'react-cookie';
import config from '../config';

export default (redirectUrl) => {
  const domainName = config.company === 'fs' ? '.fishtrack.com' : location.hostname;
  return new Promise((resolve) => {
    cookie.save('redirectUrl', redirectUrl, {
      path: '/',
      maxAge: 1800,
      expires: new Date((new Date).getTime() + (1800 * 1000)),
      domain: domainName,
      secure: false
    });

    const redirectCookie = { redirectUrl };
    resolve(redirectCookie);
  });
};
