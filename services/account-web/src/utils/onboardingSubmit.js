import saveCookies from './setLoginCookie';
import * as onboardingActions from 'redux/Funnel/onboarding';

export default (signInOrCreate, values, dispatch) => {
  return new Promise(async(resolve, reject) => {
    try {
      const response = await signInOrCreate(values, 'onboarding', dispatch);
      const accessToken = response.accessToken || response.token.access_token;
      const refreshToken = response.refreshToken || response.token.refresh_token;
      let maxAge = 0;
      let refreshMaxAge = 0;
      if (!values.staySignedIn) {
        maxAge = 1800;
        refreshMaxAge = 1800;
      } else {
        maxAge = response.expiresIn || response.token.expires_in;
        refreshMaxAge = 31536000;
      }
      await saveCookies(accessToken, refreshToken, maxAge, refreshMaxAge);

      const loginInfo = {
        email: values.email,
        accessToken,
        refreshToken
      };
      let slOnboarding;
      if (window && window.localStorage) {
        slOnboarding = JSON.parse(localStorage.getItem('slOnboarding'));
        if (slOnboarding && slOnboarding.favorites &&
          slOnboarding.favorites.favorites && slOnboarding.favorites.favorites.spots) {
          loginInfo.spots = slOnboarding.favorites.favorites.spots;
        }
      }
      await dispatch(onboardingActions.postOnboardingObj(loginInfo));
      resolve();
    } catch (err) {
      reject(err);
    }
  });
};
