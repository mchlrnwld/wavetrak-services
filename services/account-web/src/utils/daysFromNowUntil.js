export default function daysFromNowUntil(endDate) {
  const endDateMilliseconds = (new Date(endDate)).getTime();

  if (isNaN(endDateMilliseconds)) {
    return 0;
  }

  const now = Date.now();
  const millisecondsRemaining = endDateMilliseconds - now;
  const daysRemaining = millisecondsRemaining / 1000 / 60 / 60 / 24;

  return Math.max(0, Math.ceil(daysRemaining));
}
