import { trackEvent } from '@surfline/web-common';

/**
 * Google Analytics and Omniture Tracking
 *
 * GA Tracking requirements: utm_source, utm_medium, and utm_campaign
 * ||Prop           ||URL Query Param||
 * |Campaign Source |utm_source       |
 * |Campaign Medium |utm_medium       |
 * |Campaign Name   |utm_campaign     |
 * |Campaign Term   |utm_term         | * Optional
 * |Campaign Content|utm_content      | * Optional
 *
 * Omniture Tracking requirements: slcid or slcintid
 * ||Prop           ||URL Query Param   ||
 * |Campaign ID     |slcid or slcintid  |
 * |Campaign Source |slcsource          | * Optional
 * |Campaign Medium |slcmedium          | * Optional
 * |Campaign Term   |slcterm            | * Optional
 * |Campaign Content|slccontent         | * Optional
 * |Campaign Name   |slcmpname          | * Optional
 *
 * @param queryStrings
 */
export default function trackCampaign(queryStrings) {
  if (window && window.analytics) {
    if (queryStrings.utm_source && queryStrings.utm_medium && queryStrings.utm_campaign) {
      const gaCampaignTrackEvent = {
        'Campaign Source': queryStrings.utm_source,
        'Campaign Medium': queryStrings.utm_medium,
        'Campaign Name': queryStrings.utm_campaign,
      };
      if (queryStrings.utm_term) {
        gaCampaignTrackEvent['Campaign Term'] = queryStrings.utm_term;
      }
      if (queryStrings.utm_content) {
        gaCampaignTrackEvent['Campaign Content'] = queryStrings.utm_content;
      }
      if (queryStrings.mkt) {
        gaCampaignTrackEvent['Legacy Marketing ID'] = queryStrings.mkt;
      }

      trackEvent('Clicked Promotion', gaCampaignTrackEvent);
    }

    if (queryStrings.slcid || queryStrings.slintcid) {
      const omnitureCampaignTrackEvent = {
        'Campaign ID': queryStrings.slcid || queryStrings.slintcid,
      };
      if (queryStrings.slcsource) {
        omnitureCampaignTrackEvent['Campaign Source'] = queryStrings.slcsource;
      }
      if (queryStrings.slcmedium) {
        omnitureCampaignTrackEvent['Campaign Medium'] = queryStrings.slcmedium;
      }
      if (queryStrings.slcterm) {
        omnitureCampaignTrackEvent['Campaign Term'] = queryStrings.slcterm;
      }
      if (queryStrings.slccontent) {
        omnitureCampaignTrackEvent['Campaign Content'] = queryStrings.slccontent;
      }
      if (queryStrings.slcmpname) {
        omnitureCampaignTrackEvent['Campaign Name'] = queryStrings.slcmpname;
      }
      if (queryStrings.mkt) {
        omnitureCampaignTrackEvent['Legacy Marketing ID'] = queryStrings.mkt;
      }

      trackEvent('Clicked Promotion', omnitureCampaignTrackEvent);
    }
  }
}
