import { push } from 'connected-react-router';
import { trackEvent } from '@surfline/web-common';
import * as authActions from 'redux/Funnel/auth';
import { getSubscriptionInfo } from 'redux/Funnel/auth';
import createAccount from './createAccount';
import saveCookies from './setLoginCookie';
import performRedirect from './performRedirect';
import * as subscriptionApi from '../common/api/subscription';
import filterSubObjByBrand from '../common/utils/filterSubObjByBrand';

/**
 * A utility function to create a `submitFunc` prop for the create account forms. This function
 * is meant to help centralize the code for the submission of create account forms.
 * @param {string} formLocation the location of the form. This is an arbitrary string
 * that can be used to add specific logic in the submit function.
 */
export default (formLocation) => async (values, dispatch) => {
  const response = await createAccount(values, dispatch);
  const {
    token: { expires_in: expiresIn, access_token: accessToken, refresh_token: refreshToken },
    user: { _id: userId },
  } = response;
  const { staySignedIn, email, first } = values;

  const maxAge = staySignedIn ? expiresIn : 1800;
  const refreshMaxAge = staySignedIn ? 31536000 : 1800;

  if (formLocation === 'funnel') {
    trackEvent('Create Account Form Submited Form', {
      location: 'funnel',
      email: values.email,
    });

    trackEvent('Completed Checkout Step', {
      step: 1,
    });
  }

  await saveCookies(accessToken, refreshToken, maxAge, refreshMaxAge);

  // Send the user info to the store
  await dispatch(authActions.create(email, first, accessToken, refreshToken, userId));

  if (formLocation === 'funnel') {
    const subscriptions = await subscriptionApi.getSubscription(accessToken);
    const brandSubscription = filterSubObjByBrand(subscriptions);
    await dispatch(getSubscriptionInfo(brandSubscription));
    return dispatch(push('/upgrade'));
  }

  return performRedirect();
};
