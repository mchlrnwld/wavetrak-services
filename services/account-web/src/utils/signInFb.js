import signIn from './signIn';
import { fbLogin, fbSubmit, displayFbLoginError } from '../redux/actions/facebook';
import saveCookies from './setLoginCookie';
import performRedirect from './performRedirect';
import fbLoginWrapper from './signInUtils/facebook/fbLoginWrapper';
import fbUserInfoWrapper from './signInUtils/facebook/fbUserInfoWrapper';
import * as onboardingActions from 'redux/Funnel/onboarding';

const signInFb = (dispatch, formLocation) => {
  return new Promise(async (resolve, reject) => {
    try {
      const loginResponse = await fbLoginWrapper(); // launch fb popup window
      await dispatch(fbSubmit()); // start submitting ui animation
      const signInValues = await fbUserInfoWrapper(loginResponse, formLocation); // obtain user info from fb
      const signInResponse = await signIn(signInValues, formLocation);
      const accessToken = signInResponse.accessToken;
      const refreshToken = signInResponse.refreshToken;
      let canUpdate;
      let redirectAway;
      if (formLocation === 'funnel') {
        canUpdate = signInResponse.canUpdate;
        redirectAway = signInResponse.redirectAway;
      }

      let maxAge = 0;
      let refreshMaxAge = 0;
      if (!signInValues.staySignedIn) {
        maxAge = 1800;
        refreshMaxAge = 1800;
      } else {
        maxAge = signInResponse.expiresIn;
        refreshMaxAge = 31536000;
      }

      const cookies = await saveCookies(accessToken, refreshToken, maxAge, refreshMaxAge);
      dispatch(fbLogin(signInValues.email, cookies.accessToken, cookies.refreshToken, signInResponse.first, canUpdate, redirectAway));

      if (formLocation === 'onboarding') {
        const loginInfo = {
          email: signInValues.email,
          accessToken,
          refreshToken
        };
        let slOnboarding;
        if (window && window.localStorage) {
          slOnboarding = JSON.parse(localStorage.getItem('slOnboarding'));
          if (slOnboarding && slOnboarding.favorites &&
            slOnboarding.favorites.favorites && slOnboarding.favorites.favorites.spots) {
            loginInfo.spots = slOnboarding.favorites.favorites.spots;
          }
        }
        await dispatch(onboardingActions.postOnboardingObj(loginInfo));

        performRedirect();
      }

      if (formLocation === 'register') performRedirect();
      resolve();
    } catch (err) {
      dispatch(displayFbLoginError(err));
      reject();
    }
  });
};

export default signInFb;
