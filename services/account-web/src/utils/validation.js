import en from '../international/translations/en';

const isEmpty = value => value === undefined || value === null || value === '';
const join = (rules) => (value, data) => rules.map(rule => rule(value, data)).filter(error => !!error)[0 /* first error */ ];

export function email(value) {
  // Let's not start a debate on email regex. This is just for an example app!
  if (!isEmpty(value) && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(value)) {
    return en.utils.email;
  }
}

export function required(value) {
  if (isEmpty(value)) {
    return en.utils.required;
  }
}

export function minLength(min) {
  return value => {
    if (!isEmpty(value) && value.length < min) {
      return `${en.utils.min_length_start} ${min} ${en.utils.min_length_end}`;
    }
  };
}

export function minLengthOne(min) {
  return value => {
    if (!isEmpty(value) && value.length < min) {
      return en.utils.required;
    }
  };
}


export function maxLength(max) {
  return value => {
    if (!isEmpty(value) && value.length > max) {
      return `${en.utils.max_length_start} ${max} ${en.utils.max_length_end}`;
    }
  };
}

export function integer(value) {
  if (!Number.isInteger(Number(value))) {
    return `${en.utils.integer}`;
  }
}

export function oneOf(enumeration) {
  return value => {
    if (!~enumeration.indexOf(value)) {
      return `Must be one of: ${enumeration.join(', ')}`;
    }
  };
}

export function match(field) {
  return (value, data) => {
    if (data) {
      if (value !== data[field]) {
        return en.utils.match;
      }
    }
  };
}

export function createValidator(rules) {
  return (data = {}) => {
    const errors = {};
    Object.keys(rules).forEach((key) => {
      const rule = join([].concat(rules[key])); // concat enables both functions and arrays of functions
      const error = rule(data[key], data);
      if (error) {
        errors[key] = error;
      }
    });
    return errors;
  };
}
