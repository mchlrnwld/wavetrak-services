export default function fiveHundredHandler(req) {
  req.on('response', (res) => {
    if (res.status === 500) {
      window.location.href = '/500';
    }
  });
}
