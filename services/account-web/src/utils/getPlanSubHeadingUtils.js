export const amountToFixed = (amount) => {
  const fixedAmount = (amount / 100).toFixed(2);
  return fixedAmount;
};

export const getSubscriptionPrice = (sub) => {
  if (sub.type !== 'stripe') return null;

  return amountToFixed(sub.effectivePrice);
};

export const getRenewalPrice = (sub) => {
  return `${sub.plan.symbol}${amountToFixed(sub.renewalPrice)}`;
};

export const parseBillingDate = (date) => {
  const billingDate = new Date(date);
  const d = billingDate.getDate(); /* eslint id-length: 0 */
  const m = billingDate.getMonth() + 1; /* eslint id-length: 0 */
  const yyyy = billingDate.getFullYear();
  const yy = yyyy.toString().substring(2);

  return `${m}-${d}-${yy}`; /* eslint id-length: 0 */
};
