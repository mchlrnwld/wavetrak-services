import config from '../config';

export default (company = null) => {
  const brand = company || config.company;
  if (brand === 'sl') return 'Surfline';
  if (brand === 'bw') return 'Buoyweather';
  if (brand === 'fs') return 'FishTrack';
  return 'Wavetrak';
};
