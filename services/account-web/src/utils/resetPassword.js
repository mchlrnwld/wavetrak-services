import superagent from 'superagent';
import en from '../international/translations/en';
import config from '../config';

export default (values) => {
  return new Promise((resolve, reject) => {
    superagent.post(config.servicesEndpoint + '/password-reset')
    .send({
      password: values.password,
      userId: values.user,
      resetCode: values.resetCode,
      brand: config.company
    })
    .end((err, response) => {
      if (err) {
        if (!response) {
          reject({
            passCode: ' ',
            _error: en.utils.five_hundred
          });
        } else if (response.statusCode === 400) {
          reject({
            _error: 'Invalid Password Reset',
          });
        } else if (response.statusCode === 401) {
          reject({
            _error: 'Unauthorized Password Reset'
          });
        } else if (response.status === 504) {
          reject({
            password: ' ',
            confirmPassword: ' ',
            _error: en.utils.five_hundred_four
          });
        } else {
          reject({
            password: ' ',
            confirmPassword: ' ',
            _error: en.utils.five_hundred
          });
        }
      } else {
        resolve(response);
      }
    });
  });
};
