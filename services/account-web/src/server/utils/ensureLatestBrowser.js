/* eslint no-unused-vars: 0 */
import UAParser from 'ua-parser-js';
import { includes } from 'lodash';

export default function ensureLatestBrowser(req, res, next) {
  try {
    const parser = new UAParser();
    const ua = req.headers['user-agent'];
    const browserName = parser.setUA(ua).getBrowser().name;
    const fullBrowserVersion = parser.setUA(ua).getBrowser().version;
    const browserVersion = fullBrowserVersion.split('.', 1).toString();
    const browserVersionNumber = Number(browserVersion);
    const osName = parser.setUA(ua).getOS().name;

    if (browserName === 'IE' && browserVersion <= 10) {
      res.redirect('/subs/update/');
    } else if (browserName === 'Safari' && includes(osName, 'Windows')) {
      res.redirect('/subs/update/');
    } else {
      return next();
    }
  } catch (err) {
    return next();
  }
}
