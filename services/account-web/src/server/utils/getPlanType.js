
export const getPlanType = (identifier) => {
  if (identifier.includes('monthly') || identifier.includes('month')) return 'monthly';
  if (identifier.includes('annual') || identifier.includes('year')) return 'annual';
  if (identifier.includes('gift')) return 'gift';
  return 'free';
};
export default getPlanType;
