import getSubscriptions from './getSubscriptions';
import getConfig from '../../../config/server';

const selectPlanAuthServer = async(req, res, next) => {
  const config = getConfig(req.brand);
  if (req.cookies && req.cookies.access_token) {
    try {
      await getSubscriptions(config, req.cookies.access_token);
      next();
    } catch (redirectUrl) {
      res.redirect(redirectUrl);
    }
  } else {
    res.redirect(config.signInRedirect);
  }
};

export default selectPlanAuthServer;
