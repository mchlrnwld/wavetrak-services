import * as subscriptionApi from '../../../common/api/subscription';
import filterBrandSubs from '../../../common/utils/filterBrandSubs';

const getSubscriptions = (config, token) =>
  new Promise(async(resolve, reject) => {
    try {
      const subResponse = await subscriptionApi.getSubscription(token);
      const brandSubs = filterBrandSubs(subResponse);

      if (brandSubs.length <= 0) {
        resolve();
      } else {
        reject('/account/overview');
      }
    } catch (err) {
      if (err.status === 400) {
        reject(config.signInRedirect);
      } else {
        reject('/500');
      }
    }
  });

export default getSubscriptions;
