/* eslint no-unused-vars: 0 */
import * as userApi from '../../common/api/user';

const getUserByToken = async (req, res, next) => {
  const accessToken = req.cookies.access_token;

  // No access_token - redirect to sign-in
  if (!accessToken) return res.redirect('/sign-in');

  // Try to get User data based on access token.  If it sends back 400 error, delete cookies and redirect to sign-in
  try {
    const user = await userApi.getUser(accessToken);

    // set req.user for use in other routes
    req.user = user;
  } catch (err) {
    console.log(err);
    const errStatus = err.status ? err.status : 500;

    if (errStatus === 401) {
      // user data is invalid, clear out the access_token cookie and remove from request.
      // clear all logged in related cookies to force a fresh login
      // TODO: change all fishtrack domains to match other brands since we no longer use my.fishtrack.com
      // for now, fishtrack uses .fishtrack.com as cookie domain
      const domainName = req.brand === 'fs' ? '.fishtrack.com' : '.' + req.hostname.trim();

      // CF Specific cookies
      res.cookie('USERINFO', '', { domain: domainName, expires: new Date() });
      res.cookie('USER_ID', '', { domain: domainName, expires: new Date() });

      // Token cookies
      res.cookie('access_token', '', { domain: domainName, expires: new Date() });
      res.cookie('refresh_token', '', { domain: domainName, expires: new Date() });

      // delete cookie from the request
      delete req.cookies.access_token;

      // redirect to sign-in after cookie removal
      return res.redirect('/sign-in');
    }
  }
  return next();
};

export default getUserByToken;
