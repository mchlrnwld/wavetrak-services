import React, { Component } from 'react';
import config from '../../config';
import en from '../../international/translations/en';

class PremiumSuccess extends Component {
  constructor(props) {
    super(props);
    this.funnelConfig = config.funnelConfig;
  }
  componentDidMount() {
    if (this.funnelConfig) {
      const { successUrl } = this.funnelConfig;
      if (successUrl) {
        setTimeout(() => {
          window.location.assign(successUrl);
        }, 2000);
      }
    }
  }
  render() {
    let { message } = en.payment_success;
    const { subTitle } = en.payment_success;
    let header = 'Success';
    if (this.funnelConfig) {
      const { successHeader, successMessage } = this.funnelConfig;
      message = successMessage || message;
      header = successHeader || header;
    }
    return (
      <div className="premium-success__container">
        <h1>{header}</h1>
        <div className="premium-success__subtitle">{subTitle}</div>
        <div className="premium-success__message">
          {message}
        </div>
        <a className="quiver-button" type="button" href={config.redirectUrl} >{en.payment_success.linkText}</a>
      </div>
    );
  }
}
export default PremiumSuccess;
