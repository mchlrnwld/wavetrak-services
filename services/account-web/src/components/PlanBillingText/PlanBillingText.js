import React from 'react';
import { addDays, format } from 'date-fns';

const formatTrialEndDate = () => {
  const trialEnd = addDays(new Date(), 15);
  return format(trialEnd, 'MM/DD/YY');
};
const getSubtitleText = (trialEligible, coupon, trialPeriodDays) => {
  if (trialEligible && coupon) return 'Discounted subscriptions are not eligible for free trials. Billing begins today';
  let text = `Billing begins after free trial ends on ${formatTrialEndDate()}.`;
  if (!trialEligible || !trialPeriodDays) text = 'Billing begins today';
  return text;
};

export const PlanBillingText = ({ trialEligible, coupon, trialPeriodDays }) => (
  <div className="billing-text">{getSubtitleText(trialEligible, coupon, trialPeriodDays)}</div>
);

export default PlanBillingText;
