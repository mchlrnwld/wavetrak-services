import React from 'react';

const Chevron = () => (
  <svg xmlns="http://www.w3.org/2000/svg" className="chevron_icon" viewBox="0 0 24 13">
    <path fill="none" fillRule="evenodd" stroke="#22D736" d="M1 12.314L12.314 1l11.313 11.314"/>
  </svg>
);

export default Chevron;
