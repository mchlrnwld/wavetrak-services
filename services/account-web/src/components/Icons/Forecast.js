import React from 'react';

const Forecast = () => (
  <svg
    className="forecast_icon"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 42 44"
  >
    <defs>
      <path id="a" d="M28 0h1.75v6.769H28z" />
      <path id="b" d="M14 0h1.75v6.769H14z" />
    </defs>
    <g fill="none" fillRule="evenodd">
      <path
        stroke="#FFF"
        strokeWidth="1.728"
        d="M.864 4.249v38.565L5.25 41.4l5.25 1.692 5.25-1.692L21 43.092l5.25-1.692 5.246 1.691 5.1-1.691 4.54 1.424V4.249H.864z"
      />
      <path
        stroke="#22D737"
        strokeWidth="1.728"
        d="M10.5 35.538c0-10.744 12.25-21.489 19.25-10.844-7 0-7 8.15 0 9.94"
      />
      <path stroke="#FFF" strokeWidth="1.728" d="M42 15.23H0" />
      <g>
        <use fill="#011D38" />
        <path stroke="#FFF" strokeWidth="1.728" d="M28.864.864h1v5.041h-1z" />
      </g>
      <g>
        <use fill="#011D38" />
        <path stroke="#FFF" strokeWidth="1.728" d="M14.864.864h1v5.041h-1z" />
      </g>
    </g>
  </svg>
);

export default Forecast;
