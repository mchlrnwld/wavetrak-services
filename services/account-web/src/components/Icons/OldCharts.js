import React from 'react';

const OldCharts = () => (
  <svg
    className="old_charts_icon"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 43 30"
  >
    <g fill="none" fillRule="evenodd" transform="translate(0 1)">
      <ellipse
        cx="5.059"
        cy="12.609"
        fill="#FFF"
        stroke="#FFF"
        strokeWidth="1.25"
        rx="2.529"
        ry="2.522"
      />
      <ellipse
        cx="21.5"
        cy="2.522"
        fill="#FFF"
        stroke="#FFF"
        strokeWidth="1.25"
        rx="2.529"
        ry="2.522"
      />
      <path
        stroke="#FFF"
        strokeLinecap="square"
        strokeWidth="1.25"
        d="M10.75 9.457l5.059-3.783"
      />
      <ellipse
        cx="37.941"
        cy="8.826"
        fill="#FFF"
        stroke="#FFF"
        strokeWidth="1.25"
        rx="2.529"
        ry="2.522"
      />
      <path
        fill="#FFF"
        d="M0 17.652h10.118V29H0zM16.441 8.826h10.118V29H16.441zM32.882 12.609H43V29H32.882z"
      />
      <path
        stroke="#FFF"
        strokeLinecap="square"
        strokeWidth="1.25"
        d="M26.559 3.783l6.855 2.693"
      />
    </g>
  </svg>
);

export default OldCharts;
