import React from 'react';

const Analysis = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    className="analysis_icon"
    viewBox="0 0 42 42"
  >
    <g fill="none" fillRule="evenodd" strokeWidth="1.728">
      <path
        stroke="#FFF"
        d="M22.855 27.618a8.89 8.89 0 0 0 7.217-2.643l.213-.217.302-.036c5.988-.708 10.549-5.805 10.549-11.887 0-6.611-5.36-11.971-11.97-11.971h-1.07c-6.612 0-11.971 5.36-11.971 11.97 0 5.11 3.231 9.607 7.97 11.287l.972.344-.51.897a10.872 10.872 0 0 1-1.702 2.256z"
      />
      <path
        stroke="#22D737"
        d="M20.609 14.675c2.251-5.028 8.556-8.4 12.478-3.033-4.84 0-4.84 5.07 0 6.184"
      />
      <g stroke="#FFF">
        <g transform="translate(1 21.391)">
          <path
            strokeLinecap="round"
            d="M0 19.609C0 16.4 5.136 14.26 9.804 14.26c4.67 0 9.805 2.139 9.805 5.348"
          />
          <circle cx="9.804" cy="4.457" r="4.457" />
        </g>
      </g>
    </g>
  </svg>
);

export default Analysis;
