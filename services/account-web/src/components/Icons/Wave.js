import React from 'react';

const Wave = () => (
  <svg xmlns="http://www.w3.org/2000/svg" className="wave_icon" viewBox="0 0 16 12">
    <path fill="none" fillRule="evenodd" stroke="#22D737" strokeWidth="1.244" d="M1 12C1 4.24 9.273-3.52 14 4.168c-4.727 0-4.727 5.885 0 7.179"/>
  </svg>
);

export default Wave;
