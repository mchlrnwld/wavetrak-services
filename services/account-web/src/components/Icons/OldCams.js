import React from 'react';

const OldCams = () => (
  <svg
    className="old_cams_icon"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 44 29"
  >
    <g fill="none" fillRule="evenodd" stroke="#FFF" strokeWidth="1.25">
      <path d="M1.375 1.375h41.008v26.25H1.375z" />
      <path
        fill="#FFF"
        d="M3.17 27C5.098 17.13 19.133.762 33.894 9.152 21.797 16.314 32.474 26.79 42.927 27H3.169zm14.904-16.373l-.12 9.67 7.824-4.931-7.704-4.739z"
      />
    </g>
  </svg>
);

export default OldCams;
