import React from 'react';

const Charts = () => (
  <svg
    className="charts_icon"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 36 28"
  >
    <g fill="none" fillRule="evenodd" strokeWidth="1.728">
      <path stroke="#FFF" d="M.864 13.114h6.843v14.022H.864z" />
      <path stroke="#22D737" d="M14.578.864h6.843v26.272h-6.843z" />
      <path stroke="#FFF" d="M28.293 6.114h6.843v21.022h-6.843z" />
    </g>
  </svg>
);

export default Charts;
