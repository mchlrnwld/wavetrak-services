import React from 'react';

const Chevron = () => (
  <svg
    width="16px"
    height="9px"
    viewBox="0 0 16 9"
    version="1.1"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g
        transform="translate(-435.000000, -272.000000)"
        fillRule="nonzero"
        stroke="#333333"
        strokeWidth="2"
      >
        <g>
          <g transform="translate(61.000000, 221.000000)">
            <g transform="translate(349.000000, 18.000000)">
              <g transform="translate(21.000000, 25.000000)">
                <polyline points="5 9 12 15 19 9" />
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default Chevron;
