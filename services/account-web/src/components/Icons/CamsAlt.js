import React from 'react';

const CamsAlt = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="36" height="41" viewBox="0 0 36 41">
    <g fill="none" fillRule="evenodd">
      <path fill="#FFF" d="M-853-408H587v909H-853z" />
      <g strokeWidth="1.728">
        <path stroke="#22D737" d="M27.429 13.593v8.229l-4.19-2.213v2.487c0 .595-.474 1.078-1.057 1.078H9.628a1.067 1.067 0 0 1-1.057-1.078v-8.54c0-.595.473-1.078 1.057-1.078h12.554c.583 0 1.056.483 1.056 1.079v2.249l4.19-2.213z" />
        <path stroke="#011D38" d="M.864.864v38.805L18 32.032l17.136 7.637V.864H.864z" />
      </g>
    </g>
  </svg>
);

export default CamsAlt;
