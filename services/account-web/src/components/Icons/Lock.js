import React from 'react';

const Lock = () => (
  <svg width="14px" height="17px" viewBox="0 0 14 17" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-24.000000, -233.000000)" fillRule="nonzero">
        <g transform="translate(24.000000, 233.000000)">
          <rect fill="#96A9B1" x="0" y="6" width="14" height="11" rx="1" />
          <rect fill="#F2F7F9" x="6" y="9" width="2" height="5" />
          <path d="M6,0 L8,0 C10.209139,-4.05812251e-16 12,1.790861 12,4 L12,9 L12,9 L9.5,9 L9.5,4 C9.5,3.44771525 9.05228475,3 8.5,3 L5.5,3 C4.94771525,3 4.5,3.44771525 4.5,4 L4.5,9 L4.5,9 L2,9 L2,4 C2,1.790861 3.790861,4.05812251e-16 6,0 Z" id="Rectangle-Copy" fill="#96A9B1" />
        </g>
      </g>
    </g>
  </svg>
);
export default Lock;

