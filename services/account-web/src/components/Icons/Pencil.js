import React from 'react';

const Pencil = () => (
  <svg width="14px" height="14px" viewBox="0 0 14 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
    <g id="Purchase" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Purchase-Payment-1yr" transform="translate(-100.000000, -563.000000)" fill="#42A5FC" fillRule="nonzero">
        <g id="Group-4" transform="translate(60.000000, 219.000000)">
          <g id="Edit" transform="translate(35.000000, 339.000000)">
            <g id="Edit-pencil" transform="translate(5.000000, 5.000000)">
              <path d="M11.8535534,6.14644661 L7.85355339,2.14644661 L9.29289322,0.707106781 C9.68341751,0.316582489 10.3165825,0.316582489 10.7071068,0.707106781 L13.2928932,3.29289322 C13.6834175,3.68341751 13.6834175,4.31658249 13.2928932,4.70710678 L11.8535534,6.14644661 Z M11.1464466,6.85355339 L4,14 L0,14 L0,10 L7.14644661,2.85355339 L11.1464466,6.85355339 Z" id="Combined-Shape" />
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default Pencil;
