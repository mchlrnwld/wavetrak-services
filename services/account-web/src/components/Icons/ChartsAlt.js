import React from 'react';

const ChartsAlt = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="37" height="38" viewBox="0 0 37 38">
    <g fill="none" fillRule="evenodd">
      <path fill="#FFF" d="M-1098-408H342v909h-1440z" />
      <g strokeWidth="1.728">
        <path stroke="#011D38" d="M.864 19.864h7.272v17.272H.864z" />
        <path stroke="#22D737" d="M14.864.864h7.272v36.272h-7.272z" />
        <path stroke="#011D38" d="M28.864 10.864h7.272v26.272h-7.272z" />
      </g>
    </g>
  </svg>

);

export default ChartsAlt;
