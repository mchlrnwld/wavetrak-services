# Component Overview

## ActionLink -

### Description: A link that executes an onClick function

Parameters -

@onClickHandler(func) - the function to execute on click
@theme(str) = css theme to use
