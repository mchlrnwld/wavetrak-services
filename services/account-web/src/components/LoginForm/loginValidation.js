import memoize from 'lru-memoize';
import {createValidator, required, minLength, email} from 'utils/validation';

const loginValidation = createValidator({
  email: [required, email],
  password: [required, minLength(1)],
});
export default memoize(10)(loginValidation);
