import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { reduxForm } from 'redux-form';
import { Button } from '@surfline/quiver-react';
import loginValidation from './loginValidation';
import en from '../../international/translations/en';
import config from '../../config';

class LoginForm extends Component {
  static propTypes = {
    fields: PropTypes.shape({}).isRequired,
    handleSubmit: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    error: PropTypes.string,
    submitSuccess: PropTypes.bool.isRequired,
    initializeForm: PropTypes.func.isRequired,
    submitFunc: PropTypes.func.isRequired,
    formLocation: PropTypes.string.isRequired,
    showDiscountMessage: PropTypes.bool,
  };

  static defaultProps = {
    error: null,
    showDiscountMessage: false,
  }

  constructor(props) {
    super(props);
    this.state = { checkedBox: true };
    this.handleCheckChange = this.handleCheckChange.bind(this);
  }

  componentWillMount() {
    this.props.initializeForm({
      staySignedIn: true,
      location: this.props.formLocation,
      client_id: config.client_id
    });
  }

  handleCheckChange() {
    this.setState({ checkedBox: !this.state.checkedBox });
  }
  render() {
    const {
      fields: {
        email,
        password,
        staySignedIn,
        location
      },
      error,
      handleSubmit,
      submitting,
      submitSuccess,
      submitFunc,
      showDiscountMessage,
      onboardingFormError
    } = this.props;
    let errorAlert = error ? 'loginForm__error__alert' : 'loginForm__error__alert__hidden';
    let errorText;
    let errorAlertText = error;

    if (onboardingFormError) {
      errorAlert = 'loginForm__error__alert';
      errorText = 'There was an error submitting your form. Please try again';
      errorAlertText = errorText;
    }

    let emailError = 'loginForm__error__text__hidden';
    if (email.error && email.touched) {
      if (email.error === ' ') {
        emailError = 'loginForm__error__no__text';
      } else {
        emailError = 'loginForm__error__text';
      }
    }
    let passwordError = 'loginForm__error__text__hidden';
    if (password.error && password.touched) {
      if (password.error === ' ') {
        passwordError = 'loginForm__error__no__text';
      } else {
        passwordError = 'loginForm__error__text';
      }
    }

    let emailErrorInput = '';
    if (email.error && email.touched) {
      emailErrorInput = 'loginForm__input__hasError';
    } else if (email.touched) {
      emailErrorInput = 'loginForm__input__isValid';
    }
    let passwordErrorInput = '';
    if (password.error && password.touched) {
      passwordErrorInput = 'loginForm__input__hasError';
    } else if (password.touched) {
      passwordErrorInput = 'loginForm__input__isValid';
    }
    if (submitSuccess) {
      emailErrorInput = 'loginForm__input__isValid';
      passwordErrorInput = 'loginForm__input__isValid';
    }

    return (
      <div>
        {(() => {
          if (showDiscountMessage) {
            return (
              <div className="loginForm__discount__success">{en.create.form.discount_applied}</div>
            );
          }
        })()}
        <div className={errorAlert}>{errorAlertText}</div>
        <form onSubmit={handleSubmit(submitFunc)} className="loginForm">
          <input type="hidden" id={location.name} {...location} />
          <div>
            <div className="loginForm__input__email">
              <input type="email" id={email.name} {...email} className={emailErrorInput} required />
              <label htmlFor={email.name} className="loginForm__label__floating">{en.sign_in.form.label_email}</label>
              <div className={emailError}>{email.error} {email.error === en.sign_in.form.email_error_message ? <Link to="/create-account" className="loginForm__error__text__link">{en.sign_in.form.email_error_message_link}</Link> : ''}</div>
            </div>
          </div>
          <div>
            <div className="loginForm__input__password">
              <input type="password" id={password.name} {...password} className={passwordErrorInput} required/>
              <label htmlFor={password.name} className="loginForm__label__floating">{en.sign_in.form.label_password}</label>
              <div className={passwordError}>{password.error}</div>
            </div>
          </div>
          <a className="loginForm__forgotPassword" href={`${config.redirectUrl}/forgot-password`}>{en.sign_in.form.forgot_password}</a>
          <Button
            loading={submitting}
            success={submitSuccess}
          >
            Sign In
          </Button>
          <div>
            <div className="loginForm__checkboxContainer">
              <input type="checkbox" id="staySignedIn" className="loginForm__checkboxContainer__checkbox" {...staySignedIn} value={this.state.checkedBox} onClick={this.handleCheckChange} />
              <label className={this.state.checkedBox ? 'loginForm__checkboxContainer__checkbox__checked' : 'loginForm__checkboxContainer__checkbox__unchecked'} htmlFor="staySignedIn">{en.sign_in.form.staySignedIn_label}</label>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'login',
  fields: ['email', 'password', 'staySignedIn', 'location', 'client_id'],
  initialValues: {
    staySignedIn: true,
    client_id: config.client_id
  },
  validate: loginValidation
})(LoginForm);
