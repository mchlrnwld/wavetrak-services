import React from 'react';
import { RailLogo } from 'components';

export const LeftRail = ({ children }) => (
  <div className="left-rail">
    <RailLogo/>
    {children}
  </div>
);

export const RightRail = ({ children }) => (
  <div className="right-rail">
    {children}
  </div>
);

