import React from 'react';
import config from '../../config';
import {
  Favorite,
  Homepage,
  Newsletter
} from '../Icons';

const RegistrationCTA = () => (
  <div className="registration__geo-image">
    {config.company === 'sl' ? (
      <div className="registration__cta">
        <h1 className="registration__cta__header">Account Benefits</h1>
        <div className="registration__cta__item">
          <Favorite />
          <div className="registration__cta__item__text">
            <h4>Favorite Your Spots</h4>
            <p>
              Quickly access cams and forecasts for the breaks you care most
              about.
            </p>
          </div>
        </div>
        <div className="registration__cta__item">
          <Newsletter />
          <div className="registration__cta__item__text">
            <h4>Email Newsletter</h4>
            <p>The best of Surfline delivered straight to your inbox.</p>
          </div>
        </div>
        <div className="registration__cta__item">
          <Homepage />
          <div className="registration__cta__item__text">
            <h4>Personalized Homepage</h4>
            <p>Receive news and forecasts tailored to your local area.</p>
          </div>
        </div>
      </div>
    )
      : null }
  </div>
);

export default RegistrationCTA;
