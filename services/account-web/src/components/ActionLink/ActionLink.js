import PropTypes from 'prop-types';
import React from 'react';

const ActionLink = ({ children, theme, onClickHandler }) => {
  return (
    <button
      type="button"
      className={`actionLink${theme ? `--${theme}` : ''}`}
      onClick={onClickHandler}
    >
      {children}
    </button>
  );
};

ActionLink.propTypes = {
  onClickHandler: PropTypes.func.isRequired,
  theme: PropTypes.string,
  children: PropTypes.element,
};

ActionLink.defaultProps = {
  theme: null,
  children: null,
};

export default ActionLink;
