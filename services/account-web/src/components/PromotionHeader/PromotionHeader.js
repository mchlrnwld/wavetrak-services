import React, { Component } from 'react';
import { format } from 'date-fns';
import brandToString from '../../utils/brandToString';
import config from '../../config';

class PromotionHeader extends Component {
  getTitleText = () => {
    const {
      isLoggedIn,
    } = this.props;
    const {
      funnelConfig: {
        paymentHeader,
        signUpHeader,
      }
    } = config;

    if (paymentHeader || signUpHeader) {
      return isLoggedIn ? paymentHeader : signUpHeader;
    }
    return `Start your 15-day free trial of ${brandToString()} Premium`;
  }

  getSubtitleText = () => {
    const {
      funnelConfig: {
        paymentMessage,
        kind
      }
    } = config;
    if (kind === 'Retention Offer') {
      return <p className="upgrade-header__subtitle">{paymentMessage}</p>;
    }
    const { trialPeriodDays } = this.props;
    const subHeadingText = trialPeriodDays
      ? paymentMessage
      : `Billing will begin today. ${format(new Date(), 'MM/DD/YY')}`;
    return <p className="upgrade-header__subtitle">{subHeadingText}</p>;
  }

  render() {
    const { children, hideSubtitle } = this.props;
    return (
      <div className="upgrade-header">
        <h1 className="upgrade-header__title">
          {this.getTitleText()}
        </h1>
        {hideSubtitle ? children : this.getSubtitleText()}
      </div>
    );
  }
}

export default PromotionHeader;
