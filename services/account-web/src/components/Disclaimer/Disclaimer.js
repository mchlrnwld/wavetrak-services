import React from 'react';
import config from '../../config';

const Disclaimer = () => (
  <p className="disclaimer">
    {'By starting a free trial or subscription, you agree to our '}
    <DisclaimerLink url={config.privacyPolicy} text="Privacy policy" />
    {' and '}
    <DisclaimerLink url={config.termsConditions} text="Terms of Use" />.
  </p>
);

const DisclaimerLink = ({ text, url }) => (
  <a
    className="disclaimer-link"
    href={url}
    target="_blank"
    rel="noopener noreferrer"
  >
    {text}
  </a>
);

export default Disclaimer;
