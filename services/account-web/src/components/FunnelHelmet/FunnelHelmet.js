import React from 'react';
import { Helmet } from 'react-helmet';
import en from '../../international/translations/en';

const meta = [
  { name: 'description', content: en.create.meta_description },
  { property: 'fb:app_id', content: '218714858155230' },
  { property: 'fb:page_id', content: '255110695512' },
  { property: 'og:site_name', content: 'Surfline' },
  {
    name: 'apple-itunes-app',
    content:
      'app-id=393782096,affiliate-data=at=1000lb9Z&ct=surfline-website-smart-banner&pt=261378',
  },
];

const links = [
  { rel: 'manifest', href: '/manifest.json' },
  { rel: 'publisher', href: 'https://plus.google.com/+Surfline' },
  {
    rel: 'apple-touch-icon',
    href: 'https://wa.cdn-surfline.com/quiver/0.4.0/apple/surfline-apple-touch-icon.png',
  },
];

const FunnelHelmet = ({ showLogin }) => (
  <Helmet
    title={showLogin ? en.sign_in_funnel.meta_title : en.create_funnel.meta_title}
    meta={showLogin ? [{ name: 'description', content: en.sign_in_funnel.meta_description }] : meta}
    link={links}
  />
);

export default FunnelHelmet;
