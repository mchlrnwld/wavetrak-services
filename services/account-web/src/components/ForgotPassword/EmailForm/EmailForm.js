import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { trackNavigatedToPage } from '@surfline/web-common';
import emailValidation from './emailValidation';
import en from '../../../international/translations/en';
import spinner from '../../../../static/btn-spinner.svg';

class EmailForm extends Component {
  componentDidMount() {
    trackNavigatedToPage('Authentication', 'Forgot Password');
  }

  render() {
    const {
      fields: { email },
      handleSubmit,
      submitting,
      submitSuccess,
      error,
      submitHandler,
    } = this.props;
    const submitStyle = submitSuccess
      ? 'emailForm__button__submit__success'
      : 'emailForm__button__submit';
    const submitText = submitSuccess
      ? en.sign_in.form.sign_in_button
      : en.forgot_password.email_form.request_reset_button;
    const errorAlert = error ? 'emailForm__error__alert' : 'emailForm__error__alert__hidden';

    let emailError = 'emailForm__error__text__hidden';
    if (email.error && email.touched) {
      if (email.error === ' ') {
        emailError = 'emailForm__error__no__text';
      } else {
        emailError = 'emailForm__error__text';
      }
    }

    let emailErrorInput = '';
    if (email.error && email.touched) {
      emailErrorInput = 'emailForm__input__hasError';
    } else if (email.touched) {
      emailErrorInput = 'emailForm__input__isValid';
    }
    if (submitSuccess) {
      emailErrorInput = 'emailForm__input__isValid';
    }

    return (
      <div>
        <div className={errorAlert}>{error}</div>
        <form onSubmit={handleSubmit(submitHandler)} className="emailForm">
          <div>
            <div className="emailForm__input__email">
              <input type="email" id={email.name} {...email} className={emailErrorInput} required />
              <label htmlFor={email.name} className="emailForm__label__floating">
                {en.forgot_password.email_form.label_email}
              </label>
              <div className={emailError}>{email.error}</div>
            </div>
          </div>

          <div>
            <div>
              <button
                disabled={submitting}
                type="submit"
                className={submitting ? 'emailForm__button__submit__submitting' : submitStyle}
              >
                {submitting ? (
                  <img
                    src={spinner}
                    className="emailForm__button__submit__submitting__img"
                    alt="Spinner"
                  />
                ) : (
                  submitText
                )}
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

EmailForm.propTypes = {
  fields: PropTypes.shape().isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  submitSuccess: PropTypes.bool.isRequired,
  error: PropTypes.string,
  user: PropTypes.shape(),
  submitHandler: PropTypes.func.isRequired,
};

EmailForm.defaultProps = {
  user: null,
  error: null,
};

export default reduxForm({
  form: 'email',
  fields: ['email'],
  validate: emailValidation,
})(EmailForm);
