/* eslint no-nested-ternary: 0 */

import PropTypes from 'prop-types';

import React, { Component } from 'react';
import { reduxForm } from 'redux-form';
import { trackNavigatedToPage } from '@surfline/web-common';
import resetPasswordValidation from './resetPasswordValidation';
import en from '../../../international/translations/en';
import { submit } from '../../../containers/ForgotPassword/SetPassword/SetPassword';
import spinner from '../../../../static/btn-spinner.svg';

class ResetPasswordForm extends Component {
  componentDidMount() {
    const { initializeForm, userId, resetCode } = this.props;
    if (window?.analytics) {
      window.analytics.identify(userId);
    }
    trackNavigatedToPage('Authentication', 'Set New Password');

    initializeForm({
      user: userId,
      resetCode,
    });
  }

  render() {
    const {
      fields: { password, confirmPassword, user, resetCode },
      handleSubmit,
      submitting,
      submitSuccess,
      error,
    } = this.props;
    const submitStyle = submitSuccess
      ? 'resetPasswordForm__button__submit__success'
      : 'resetPasswordForm__button__submit';
    const submitText = submitSuccess
      ? en.sign_in.form.submit_success
      : en.forgot_password.reset_password_form.set_password_button;
    const errorAlert = error
      ? 'resetPasswordForm__error__alert'
      : 'resetPasswordForm__error__alert__hidden';

    let confirmPasswordError = 'resetPasswordForm__error__text__hidden';
    if (confirmPassword.error && confirmPassword.touched) {
      if (password.error === ' ') {
        confirmPasswordError = 'resetPasswordForm__error__no__text';
      } else {
        confirmPasswordError = 'resetPasswordForm__error__text';
      }
    }

    let passwordErrorInput = '';
    if (password.error && password.touched) {
      passwordErrorInput = 'resetPasswordForm__input__hasError';
    } else if (password.touched) {
      passwordErrorInput = 'resetPasswordForm__input__isValid';
    }
    let confirmPasswordErrorInput = '';
    if (confirmPassword.error && confirmPassword.touched) {
      confirmPasswordErrorInput = 'resetPasswordForm__input__hasError';
    } else if (confirmPassword.touched) {
      confirmPasswordErrorInput = 'resetPasswordForm__input__isValid';
    }
    if (submitSuccess) {
      passwordErrorInput = 'resetPasswordForm__input__isValid';
      confirmPasswordErrorInput = 'resetPasswordForm__input__isValid';
    }

    return (
      <div>
        <div className={errorAlert}>{error}</div>
        <form onSubmit={handleSubmit(submit)} className="resetPasswordForm">
          <input type="hidden" id={user.name} {...user} />
          <input type="hidden" id={resetCode.name} {...resetCode} />
          <div>
            <div className="resetPasswordForm__input__password">
              <input
                type="password"
                id={password.name}
                {...password}
                className={passwordErrorInput}
                required
              />
              <label htmlFor={password.name} className="resetPasswordForm__label__floating">
                {en.forgot_password.reset_password_form.label_new_password}
              </label>
              {password.error && password.touched ? (
                <div className="resetPasswordForm__error__text">{password.error}</div>
              ) : (
                <div className="resetPasswordForm__helper__text">
                  {en.forgot_password.reset_password_form.password_helper_text}
                </div>
              )}
            </div>
          </div>
          <div>
            <div className="resetPasswordForm__input__password">
              <input
                type="password"
                id={confirmPassword.name}
                {...confirmPassword}
                className={confirmPasswordErrorInput}
                required
              />
              <label htmlFor={confirmPassword.name} className="resetPasswordForm__label__floating">
                {en.forgot_password.reset_password_form.label_new_password_confirm}
              </label>
              <div className={confirmPasswordError}>{confirmPassword.error}</div>
            </div>
          </div>
          <div>
            <div>
              <button
                disabled={submitting}
                onClick={handleSubmit(submit)}
                type="button"
                className={
                  submitting ? 'resetPasswordForm__button__submit__submitting' : submitStyle
                }
              >
                {submitting ? (
                  <img
                    alt="loading-spinner"
                    src={spinner}
                    className="resetPasswordForm__button__submit__submitting__img"
                  />
                ) : (
                  submitText
                )}
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

ResetPasswordForm.propTypes = {
  fields: PropTypes.shape({
    password: PropTypes.shape({}),
    confirmPassword: PropTypes.shape({}),
    user: PropTypes.shape({}),
    resetCode: PropTypes.shape({}),
  }).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  submitSuccess: PropTypes.bool,
  error: PropTypes.string,
  userId: PropTypes.string,
  resetCode: PropTypes.string,
  initializeForm: PropTypes.func.isRequired,
};

ResetPasswordForm.defaultProps = {
  submitSuccess: false,
  error: null,
  userId: null,
  resetCode: null,
};

export default reduxForm({
  form: 'resetPassword',
  fields: ['password', 'confirmPassword', 'user', 'resetCode'],
  validate: resetPasswordValidation,
  submit,
})(ResetPasswordForm);
