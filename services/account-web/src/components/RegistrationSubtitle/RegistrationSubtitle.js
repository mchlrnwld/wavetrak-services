import React from 'react';

const RegistrationSubtitle = ({ showLogin, doToggleForm }) => (
  showLogin ? (
    <p>
      Need an account?
      <span
        className="register-link__action"
        onClick={() => doToggleForm(showLogin)}
      >
        {' Sign Up'}
      </span>
    </p>
  ) : (
    <p>
      Already have an account?
      <span
        className="register-link__action"
        onClick={() => doToggleForm(showLogin)}
      >
        {' Sign In'}
      </span>
    </p>
  )
);

export default RegistrationSubtitle;
