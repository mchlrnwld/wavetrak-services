import PropTypes from 'prop-types';
import React, { Component } from 'react';
import en from '../../international/translations/en';
import SurflineIcon from '../Icons/SurflineIcon';
import FishtrackIcon from '../Icons/FishtrackIcon';
import BuoyweatherIcon from '../Icons/BuoyweatherIcon';

export default class BrandIcons extends Component {

  static propTypes = {
    theme: PropTypes.string.isRequired
  };

  render() {
    const { theme } = this.props;
    const containerClass = `brand-icons__${theme}`;

    return (
      <div className={containerClass}>
        {(() => {
          if (theme !== 'account') {
            return (
              <p className="brand-icons__p">{en.app.brand_icons}</p>
            );}
        })()}
        <div className="brand-icons__ul">
          <SurflineIcon theme={theme} />
          <FishtrackIcon theme={theme} />
          <BuoyweatherIcon theme={theme} />
        </div>
      </div>
    );
  }
}
