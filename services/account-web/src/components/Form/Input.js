import PropTypes from 'prop-types';
import React, { Component } from 'react';

class Input extends Component {
  render() {
    const {
      formObject,
      inputClassName,
      formLocation,
      label,
      errorClassName,
      formName,
      required,
      type,
      children,
      onClick
    } = this.props;

    return (
      <div>
        <div className={`${formLocation}__input__${formName}`}>
          <input
            type={type}
            ref="input"
            id={formObject.name}
            {...formObject}
            className={inputClassName}
            required={required}
            onClick={onClick}
          />
          <label
            htmlFor={formObject.name}
            className={`${formLocation}__label__floating`}
          >
            {label}
          </label>
          {children || <div className={errorClassName}>{formObject.error}</div>}
        </div>
      </div>
    );
  }
}

Input.propTypes = {
  formObject: PropTypes.shape({
    name: PropTypes.string,
    error: PropTypes.string
  }).isRequired,
  formLocation: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  formName: PropTypes.string.isRequired,
  errorClassName: PropTypes.string,
  inputClassName: PropTypes.string,
  onClick: PropTypes.func,
  required: PropTypes.bool,
  children: PropTypes.node,
  type: PropTypes.string,
};

Input.defaultProps = {
  errorClassName: '',
  inputClassName: '',
  required: false,
  type: 'text',
};

export default Input;
