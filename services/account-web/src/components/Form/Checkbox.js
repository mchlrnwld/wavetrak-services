import PropTypes from 'prop-types';
import React from 'react';

const Checkbox = ({copy, isChecked, onClick, reduxFormProps, formName}) => (
  <div className="createForm__checkboxContainer">
    <input
      type="checkbox"
      id={formName}
      className="createForm__checkboxContainer__checkbox"
      {...reduxFormProps}
      value={isChecked}
      onClick={onClick}
    />
    <label
      className={
        isChecked
          ? 'createForm__checkboxContainer__checkbox__checked'
          : 'createForm__checkboxContainer__checkbox__unchecked'
      }
      htmlFor={formName}
    >
      {copy}
    </label>
  </div>
);

Checkbox.propTypes = {
  copy: PropTypes.string.isRequired,
  isChecked: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  reduxFormProps: PropTypes.object.isRequired,
  formName: PropTypes.string.isRequired,
};
export default Checkbox;
