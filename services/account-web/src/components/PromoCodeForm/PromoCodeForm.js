import React, { Component } from 'react';

class PromoCodeForm extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (event) => {
    this.props.handlePromoCodeChange(event.target.value);
  };

  render() {
    return (
      <div>
        <input
          type="text"
          name="promoCode"
          value={this.props.promoCode}
          onChange={this.handleChange}
          placeholder="PROMO CODE"
          maxLength="15"
        />
      </div>
    );
  }
}

export default PromoCodeForm;
