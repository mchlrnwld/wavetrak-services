import PropTypes from 'prop-types';
import React from 'react';
import { SortableContainer as sortableContainer } from 'react-sortable-hoc';
import { SortableFavoriteItem } from 'components';
import userFavoritePropType from '../../../../propTypes/userFavoritePropType';

const SortableFavoriteList = ({
  favorites,
  handleRemove,
  updatingFavorites,
}) =>
    <ul>
      {favorites.map((favorite, index) => (
        <SortableFavoriteItem
          key={`item-${index}`}
          index={index}
          favorite={favorite}
          handleRemove={handleRemove}
          updatingFavorites={updatingFavorites}
        />
      ))}
    </ul>;

SortableFavoriteList.propTypes = {
  favorites: PropTypes.arrayOf(userFavoritePropType),
  handleRemove: PropTypes.func,
  updatingFavorites: PropTypes.bool,
  styles: PropTypes.object,
};

export default sortableContainer(SortableFavoriteList);
