import PropTypes from 'prop-types';
import React from 'react';
import { SortableElement as sortableElement } from 'react-sortable-hoc';
import Spinner from '../../Spinner/Spinner';
import userFavoritePropType from '../../../../propTypes/userFavoritePropType';

const SortableFavoriteItem = ({ favorite, handleRemove, updatingFavorites }) => (
  <li className="favorites__body__container__card__item">
    {updatingFavorites ? <Spinner /> : favorite.name}
    <div className="favorites__body__container__card__item__controls">
      <button
        className="favorites__body__container__card__item__controls__delete"
        onClick={(evt) => handleRemove(evt, favorite)}
        onTouchEnd={(evt) => handleRemove(evt, favorite)}
        type="button"
      >
        Remove
      </button>
      <div
        role="presentation"
        label="Sort"
        className="favorites__body__container__card__item__controls__move"
      />
    </div>
  </li>
);

SortableFavoriteItem.propTypes = {
  favorite: userFavoritePropType.isRequired,
  handleRemove: PropTypes.func.isRequired,
  updatingFavorites: PropTypes.bool.isRequired,
};

export default sortableElement(SortableFavoriteItem);
