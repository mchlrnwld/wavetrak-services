import React, { Component } from 'react';

export default class PreviousSubscription extends Component {

  constructor(props) {
    super(props);
  }

  formatPrevSubPeriod(start, end) {
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
      'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
    ];
    const startDate = new Date(start);
    const endDate = new Date(end);

    return `${monthNames[startDate.getMonth()]} ${startDate.getFullYear()} - ${monthNames[endDate.getMonth()]} ${endDate.getFullYear()}`;
  }

  findPrevSubBrand(prevSub) {
    let brandDescription;
    if (prevSub.entitlement) {
      switch (prevSub.entitlement) {
        case 'sl_premium':
          brandDescription = 'Surfline Premium';
          break;
        case 'bw_premium':
          brandDescription = 'Buoyweather Premium';
          break;
        case 'fs_premium':
          brandDescription = 'FishTrack Premium';
          break;
        default:
          brandDescription = 'Premium';
      }
    }
    return brandDescription;
  }

  render() {
    const { prevSub, interval } = this.props;

    return (
      <tr className="prevSub__table__row">
        <td className="prevSub__table__col">
          {(() => {
            if (prevSub.type === 'apple') {
              return <i className="fa fa-apple" />;
            } else if (prevSub.type === 'google') {
              return <i className="fa fa-android" />;
            }
          })()}
          {(() => {
            return (
              <div className="prevSub__table__col__description">{this.findPrevSubBrand(prevSub)} - <span className="prevSub__interval">{interval}</span></div>
            );
          })()}
        </td>
        <td className="prevSub__table__col">
          {this.formatPrevSubPeriod(prevSub.periodStart, prevSub.periodEnd)}
        </td>
      </tr>
    );
  }
}
