import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import MonthlyCost from '../MonthlyCost';

describe('Components / Account / <MonthlyCost />', () => {
  let wrapper1;
  let wrapper2;
  let wrapper3;

  before(() => {
    wrapper1 = shallow(<MonthlyCost cost={49.99} />);
    wrapper2 = shallow(<MonthlyCost cost={49} />);
    wrapper3 = shallow(<MonthlyCost cost={0.99} />);
  });

  it('should display dollars and cents separately', () => {
    expect(wrapper1.find('h3').text()).to.equal('49');
    expect(wrapper1.find(`monthlyCost__centsPerMonth`).find('sup').text()).to.equal('99');
    expect(wrapper2.find('h3').text()).to.equal('49');
    expect(wrapper2.find(`monthlyCost__centsPerMonth`).find('sup').text()).to.equal('');
    expect(wrapper3.find('h3').text()).to.equal('0');
    expect(wrapper3.find(`monthlyCost__centsPerMonth`).find('sup').text()).to.equal('99');
  });
});
