import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames';

export default class MonthlyCost extends Component {
  static propTypes = {
    cost: PropTypes.string,
    theme: PropTypes.string,
    brand: PropTypes.string
  };

  parseCost = () => {
    const numberString = this.props.cost.toString();
    const decimalIndex = numberString.indexOf('.');
    const dollars = numberString.substring(0, decimalIndex > -1 ? decimalIndex : numberString.length);
    const cents = decimalIndex > -1 ? numberString.substring(decimalIndex + 1) : '';
    return { dollars, cents };
  };

  render() {
    const { dollars, cents } = this.parseCost();
    const { theme, symbol } = this.props;
    const containerClasses = classNames({
      'monthlyCost__container': true,
      'monthlyCost__container--alt': (theme === 'alt'),
      'monthlyCost__container--overview': (theme === 'overview'),
    });
    const brand = this.props.brand ? this.props.brand : 'default';

    return (
      <div className={`monthlyCost__container--${brand}`}>
        <div className={containerClasses}>
          <sup className="monthlyCost__currency">{symbol}</sup>
          <h3 className="monthlyCost__dollars">{dollars}</h3>
          <span className="monthlyCost__centsPerMonth">
            <sup>
              {cents}
            </sup>
            <sub>
              /mo
            </sub>
          </span>
        </div>
      </div>
    );
  }
}
