import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { trackEvent } from '@surfline/web-common';
import { Button, TrackableLink } from '@surfline/quiver-react';

import { AvailablePlans } from '../..';
import en from '../../../international/translations/en';
import {
  getSubscriptionPrice,
  parseBillingDate,
  getRenewalPrice,
} from '../../../utils/getPlanSubHeadingUtils';
import config from '../../../config';

const EVENT_PROPERTIES = {
  location: 'subscription-details',
};

export default class PremiumSubscriptions extends Component {
  constructor(props) {
    super(props);

    this.state = {
      validTrial: false,
    };
    this.cancelLinkRef = React.createRef();
  }

  componentDidMount() {
    const { sub } = this.props;
    this.validateTrialDate(sub.periodStart);
  }

  getSubHeading = (subscription, availablePlans, periodEnd, brand) => {
    if (subscription.type === 'apple') return en.subscription.subheading_apple;
    if (subscription.type === 'google') return en.subscription.subheading_google;
    if (subscription.type === 'vip') return en.subscription.subheading_vip[brand];
    if (subscription.expiring)
      return en.subscription.expiring_message(parseBillingDate(periodEnd), brand);

    const { validTrial } = this.state;
    const { getBillingInterval } = this.props;

    // check vaildTrial state
    const subHeading =
      subscription.onTrial && validTrial
        ? en.subscription.subheading_trial
        : en.subscription.subheading;
    return subHeading(
      getBillingInterval(subscription),
      parseBillingDate(periodEnd),
      getRenewalPrice(subscription),
      brand,
    );
  };

  calculateCurrentPlanAnnualAmount = (subscription) => {
    const { getBillingInterval } = this.props;
    const amount = getSubscriptionPrice(subscription);
    const interval = getBillingInterval(subscription);
    let annualAmount;

    if (interval === en.subscription.plans.daily) {
      annualAmount = amount * 365;
    } else if (interval === en.subscription.plans.monthly) {
      annualAmount = amount * 12;
    } else {
      annualAmount = amount;
    }
    return annualAmount;
  };

  validateTrialDate = (startDate) => {
    const today = new Date();
    const diff = Math.floor((Date.parse(today) - Date.parse(startDate)) / 86400000);

    if (diff <= 16) {
      this.setState({
        validTrial: true,
      });
    }
  };

  generateCancelHref = (sub, brand) => {
    const brandCancelMap = {
      sl: 'surfline.com/cancel/',
      bw: 'buoyweather.com/cancel/',
      fs: 'fishtrack.com/cancel/',
    };
    let cancelHref;
    if (sub.type === 'stripe') {
      cancelHref = `${config.accountLinkProtocol}${brandCancelMap[brand]}${sub.subscriptionId}`;
    } else if (sub.type === 'google') {
      cancelHref =
        'https://support.google.com/googleplay/answer/7018481?co=GENIE.Platform%3DAndroid&hl=en';
    } else {
      cancelHref = 'https://support.apple.com/en-us/HT202039';
    }
    return cancelHref;
  };

  handleRenewSubscription = (sub) => {
    const { renewSubscription } = this.props;
    const { planId } = sub;
    renewSubscription(sub);
    trackEvent('Clicked Renew Subscription', {
      location: 'account-subscription',
      plan: planId,
    });
    window.location.reload();
  };

  /** If the user is on an annual plan, only return their current plan for that brand to prevent them from downgrading to monthly */
  filterCurrentPlan = (sub, brandPlans) =>
    brandPlans.filter((plan) =>
      sub.plan?.interval.match(/year/g) ? plan.id === sub.planId : plan.id,
    );

  renderCancelButton = (brandNameMap) => {
    const { sub, brand } = this.props;
    const isExpiring = sub && sub.expiring;
    const hasFailedPayments = sub && sub.failedPaymentAttempts > 0;
    const isVip = sub.type === 'vip';
    if ((!isExpiring || hasFailedPayments) && !isVip) {
      return (
        <div className="subscription__cancel">
          <TrackableLink
            ref={this.cancelLinkRef}
            eventName="Clicked Cancel Your Subscription"
            eventProperties={EVENT_PROPERTIES}
          >
            <a ref={this.cancelLinkRef} href={this.generateCancelHref(sub, brand)}>
              {en.subscription.cancel(sub.type, brandNameMap[brand])}
            </a>
          </TrackableLink>
        </div>
      );
    }
    return null;
  };

  render() {
    const {
      updating,
      isUpdatingPlan,
      success,
      error,
      sub,
      periodEnd,
      brandPlans,
      brand,
      changePlan,
    } = this.props;
    const brandNameMap = {
      sl: 'Surfline',
      bw: 'Buoyweather',
      fs: 'FishTrack',
    };

    return (
      <div className={`subscription__card__${brand}`}>
        <div
          className="subscription__subheading"
          dangerouslySetInnerHTML={{
            __html: this.getSubHeading(sub, brandPlans, periodEnd, brandNameMap[brand]),
          }}
        />
        {error && <div className="subscription_error__alert">{error.message}</div>}
        {sub.type === 'stripe' &&
          (!sub.expiring ? (
            <AvailablePlans
              subscription={sub}
              currentPlanAnnualAmount={this.calculateCurrentPlanAnnualAmount(sub)}
              plans={this.filterCurrentPlan(sub, brandPlans)}
              isChanging={isUpdatingPlan}
              onSelectHandler={changePlan}
            />
          ) : (
            <div className="stayPremium__btn">
              <Button
                onClick={() => {
                  this.handleRenewSubscription(sub);
                }}
                loading={updating}
                success={success}
              >
                STAY PREMIUM
              </Button>
            </div>
          ))}
        {this.renderCancelButton(brandNameMap)}
      </div>
    );
  }
}

PremiumSubscriptions.propTypes = {
  subscriptions: PropTypes.shape({}).isRequired,
  updating: PropTypes.bool,
  sub: PropTypes.shape({
    type: PropTypes.string,
    expiring: PropTypes.bool,
    failedPaymentAttempts: PropTypes.number,
    periodStart: PropTypes.string,
    periodEnd: PropTypes.string,
  }).isRequired,
  periodEnd: PropTypes.string.isRequired,
  brandPlans: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  brand: PropTypes.string.isRequired,
  changePlan: PropTypes.func.isRequired,
  getBillingInterval: PropTypes.func.isRequired,
  error: PropTypes.shape({ message: PropTypes.string }),
  renewSubscription: PropTypes.func.isRequired,
  isUpdatingPlan: PropTypes.bool.isRequired,
  success: PropTypes.bool.isRequired,
};

PremiumSubscriptions.defaultProps = {
  updating: false,
  error: null,
};
