/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';
import { createAccessibleOnClick } from '@surfline/quiver-react';

const getCurrentRoute = (props) => {
  const { childRoutes, router } = props;
  const routeMatch = childRoutes.filter((route) => {
    if (route.path === router.location.pathname) {
      return route.name;
    }
    return false;
  });
  if (routeMatch.length) return routeMatch[0].name;
  return null;
};

class MobileNavigation extends Component {
  constructor(props) {
    super(props);
    this.toggleNav = this.toggleNav.bind(this);
    this.activeRoute = getCurrentRoute(props);
    this.state = {
      isNavHidden: true,
    };
  }

  componentDidMount() {
    this.setActiveRoute();
  }

  componentDidUpdate(prevProps) {
    const currentRoute = getCurrentRoute(this.props);
    const pastRoute = getCurrentRoute(prevProps);
    if (currentRoute !== pastRoute) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        activeItemTitle: currentRoute,
        isNavHidden: true,
      });
    }
  }

  setActiveRoute = () => {
    this.setState({
      activeItemTitle: this.activeRoute,
    });
  };

  selectNavItem = (item) => {
    const { isNavHidden } = this.state;
    this.setState({
      activeItemTitle: item.name,
      isNavHidden: !isNavHidden,
    });
  };

  toggleNav() {
    const { isNavHidden } = this.state;
    this.setState({
      isNavHidden: !isNavHidden,
    });
  }

  render() {
    const { isNavHidden, activeItemTitle } = this.state;
    const { childRoutes } = this.props;
    const navHiddenClass = classNames({
      mobileNavigation__link__list__container__hidden: isNavHidden,
    });
    return (
      <div className="mobileNavigation__container">
        <div className="mobileNavigation__active__item" onClick={this.toggleNav}>
          <span className="mobileNavigation__active__item__title">{activeItemTitle}</span>
          {(() => {
            return isNavHidden ? (
              <i className="fa fa-chevron-down" />
            ) : (
              <i className="fa fa-chevron-up" />
            );
          })()}
        </div>
        <div className={navHiddenClass}>
          <ul className="mobileNavigation__link__list">
            {childRoutes.map((route) => {
              return (
                <li onClick={() => this.selectNavItem(route)} key={route.name}>
                  <NavLink
                    className="mobileNavigation__link__list_link"
                    activeClassName="mobileNavigation__link__list_active"
                    to={route.path}
                  >
                    {route.name}
                  </NavLink>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
}

MobileNavigation.propTypes = {
  childRoutes: PropTypes.instanceOf(Array).isRequired,
  router: PropTypes.shape({
    location: PropTypes.shape({
      pathname: PropTypes.string,
    }),
  }).isRequired,
};

export default MobileNavigation;
