import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { trackEvent } from '@surfline/web-common';
import classNames from 'classnames';
import { isPast } from 'date-fns';
import { CardElement, injectStripe } from 'react-stripe-elements';
import { Button, Spinner, Alert } from '@surfline/quiver-react';
import { CreditCardIcon, ActionLink } from '../..';

const createOptions = (fontSize, padding) => ({
  style: {
    base: {
      fontSize,
      color: '#424770',
      letterSpacing: '0.025em',
      fontFamily: 'Source Sans Pro, Helvetica, sans-serif',
      '::placeholder': {
        color: '#aab7c4',
      },
      padding,
    },
    invalid: {
      color: '#9e2146',
    },
  },
});

class SubscriptionCards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expMonth: null,
      expYear: null,
      addressZip: null,
    };
  }

  getCreditCardClassName = (card) => {
    const { id, expMonth, expYear } = card;
    const {
      cards: { cardToEdit },
    } = this.props;
    return classNames({
      'credit-card': true,
      'credit-card--editting': id === cardToEdit,
      'credit-card--expired': this.isCardExpired(expMonth, expYear),
    });
  };

  getCreditCardExpDateClassName = () => {
    return classNames({
      'credit-card__element--expDate': true,
    });
  };

  getCaretClassName = (cardId) => {
    const {
      cards: { cardToEdit },
    } = this.props;
    return classNames({
      'credit-card__caret': true,
      'credit-card__caret--open': cardId === cardToEdit,
      'credit-card__caret--closed': cardId !== cardToEdit,
    });
  };

  isCardExpired = (month, year) => isPast(new Date(year, month));

  formatExpirationDate = (month, year) => {
    const formattedMonth = month.toString().length === 1 ? `0${month.toString()}` : month;
    const parsedYear = year.toString().substring(2);
    return `${formattedMonth}/${parsedYear}`;
  };

  submitAddCard = async () => {
    const { stripe, doAddCard } = this.props;
    const { token = null } = await stripe.createToken({ type: 'card' });

    trackEvent('Clicked Add Credit Debit Card', {
      location: 'subscription-details',
    });
    doAddCard(token);
  };

  submitUpdateCard = async (event) => {
    event.preventDefault();
    const {
      doUpdateCard,
      cards: { cards, cardToEdit },
    } = this.props;
    const { expMonth: expirationMonth, expYear: expirationYear, addressZip: zipCode } = this.state;

    const { expMonth, expYear, addressZip } = cards.find(({ id }) => id === cardToEdit);

    const cardDetails = {
      id: cardToEdit,
      opts: {
        exp_month: expirationMonth || expMonth,
        exp_year: expirationYear || expYear,
        address_zip: zipCode || addressZip,
      },
    };

    await doUpdateCard(cardDetails);
  };

  handleMonthChange = (event) => {
    this.setState({ expMonth: parseInt(event.target.value, 10) });
  };

  handleYearChange = (event) => {
    this.setState({ expYear: parseInt(event.target.value, 10) });
  };

  handleZipChange = (event) => {
    this.setState({ addressZip: event.target.value });
  };

  clearForm = async () => {
    const { doSetCardToEdit } = this.props;
    this.setState({
      expMonth: null,
      expYear: null,
      addressZip: null,
    });
    await doSetCardToEdit();
  };

  render() {
    const {
      stripeCustomerId,
      doToggleShowNewCardForm,
      doSetCardToEdit,
      doSetDefaultCard,
      doDeleteCard,
      cards: { submitting, cards, defaultSource, showNewCardForm, editCardError, error },
    } = this.props;
    if (!stripeCustomerId) {
      return (
        <div className="subscription-payment-disclaimer">
          <h5 className="subscription-payment-header">Payment Details</h5>
          <Alert type="info">
            Looks like your payment details are managed by a third party. Please check either your
            iTunes or Google Play account for payment related information.
          </Alert>
        </div>
      );
    }
    return (
      <section id="payment-details">
        <div className="subscription-payment-details">
          <h5 className="subscription-payment-header">Payment Details</h5>
          {editCardError ? <Alert type="error">{editCardError}</Alert> : null}
          {cards ? (
            <div className="subscription-payment-details__credit-cards">
              {cards.map((card) => (
                <div key={card.id} className={this.getCreditCardClassName(card)}>
                  <div onClick={() => doSetCardToEdit(card.id)} className="credit-card__details">
                    <div className="credit-card__element credit-card__element--card-brand">
                      <CreditCardIcon ccBrand={card.brand} />
                    </div>
                    <div className="credit-card__element credit-card__element--card-number">
                      **** {card.last4}
                      <div className={this.getCreditCardExpDateClassName(card)}>
                        {this.formatExpirationDate(card.expMonth, card.expYear)}
                      </div>
                    </div>
                    <div className="credit-card__element credit-card__element--default-source-indicator">
                      {card.id === defaultSource ? <span>Primary Card</span> : null}
                    </div>
                    <div className={this.getCaretClassName(card.id)} />
                  </div>
                  <div className="credit-card__edit-form__container">
                    {submitting ? (
                      <div className="credit-card__edit-form__submitting">
                        <p className="creidt-card__edit-form__submitting__message">
                          Processing your changes...
                        </p>
                        <Spinner />
                      </div>
                    ) : (
                      <div className="credit-card__edit-form__content">
                        <div className="credit-card__edit-form__content__update">
                          <p>Update Your Payment Details</p>
                          <form
                            className="credit-card__edit_form__form"
                            onSubmit={this.submitUpdateCard}
                          >
                            <div className="credit-card__editform--fields">
                              <div className="credit-card__editform--fields--month">
                                <span>EXP MONTH</span>
                                <input
                                  type="text"
                                  required
                                  maxLength="2"
                                  onChange={this.handleMonthChange}
                                  defaultValue={card.expMonth}
                                />
                              </div>
                              <div className="credit-card__editform--fields--year">
                                <span>EXP YEAR</span>
                                <input
                                  type="text"
                                  required
                                  maxLength="4"
                                  onChange={this.handleYearChange}
                                  defaultValue={card.expYear}
                                />
                              </div>
                              <div className="credit-card__editform--fields--zip">
                                <span>ZIP/POSTAL</span>
                                <input
                                  type="text"
                                  required
                                  maxLength="10"
                                  onChange={this.handleZipChange}
                                  defaultValue={card.addressZip}
                                />
                              </div>
                            </div>
                            <div className="credit-card__edit-form__button-container">
                              <Button loading={submitting}>Save</Button>
                              <Button onClick={this.clearForm} type="info">
                                Cancel
                              </Button>
                            </div>
                          </form>
                        </div>
                        <div>
                          {card.id === defaultSource ? (
                            <div className="credit-card__default-source-disclaimer">
                              Your primary card cannot be removed without first assigning a new card
                              as the primary billing method
                            </div>
                          ) : (
                            <div className="credit-card-form__edit-actions">
                              <ActionLink onClickHandler={() => doSetDefaultCard(card.id)}>
                                Set As Primary Card
                              </ActionLink>
                              <span> | </span>
                              <ActionLink onClickHandler={() => doDeleteCard(card.id)}>
                                Remove Card
                              </ActionLink>
                            </div>
                          )}
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              ))}
            </div>
          ) : null}
          {showNewCardForm ? (
            <div className="subscription-payment-form">
              <h5 className="subscription-payment-header">Add a New Payment Method</h5>
              <div className="subscription-payment-form__subheader">
                <p>These credit and debit cards are accepted.</p>
                <div>
                  <CreditCardIcon ccBrand="visa" />
                  <CreditCardIcon ccBrand="mastercard" />
                  <CreditCardIcon ccBrand="american-express" />
                  <CreditCardIcon ccBrand="discover" />
                  <CreditCardIcon ccBrand="jcb" />
                  <CreditCardIcon ccBrand="unionpay" />
                </div>
              </div>
              {error ? <Alert type="error">{error}</Alert> : null}
              <div className="subscription-payment-form__cc-container">
                <CardElement {...createOptions('18px')} disabled={submitting} />
                <Button loading={submitting} onClick={this.submitAddCard}>
                  Add Card
                </Button>
                <Button onClick={() => doToggleShowNewCardForm(showNewCardForm)} type="info">
                  Cancel
                </Button>
              </div>
            </div>
          ) : (
            <ActionLink
              theme="add-card"
              onClickHandler={() => doToggleShowNewCardForm(showNewCardForm)}
            >
              + Add New Card?
            </ActionLink>
          )}
        </div>
      </section>
    );
  }
}

SubscriptionCards.propTypes = {
  doAddCard: PropTypes.func.isRequired,
  doUpdateCard: PropTypes.func.isRequired,
  doSetDefaultCard: PropTypes.func.isRequired,
  doToggleShowNewCardForm: PropTypes.func.isRequired,
  doSetCardToEdit: PropTypes.func.isRequired,
  doDeleteCard: PropTypes.func.isRequired,
  showNewCardForm: PropTypes.bool,
  stripeCustomerId: PropTypes.string,
  stripe: PropTypes.shape({
    createToken: PropTypes.func,
  }).isRequired,
  cards: PropTypes.shape({
    cardToEdit: PropTypes.string,
    cards: PropTypes.arrayOf(PropTypes.shape({})),
    defaultSource: PropTypes.string,
    submitting: PropTypes.bool,
    editCardError: PropTypes.string,
    error: null,
    showNewCardForm: PropTypes.bool,
  }),
};

SubscriptionCards.defaultProps = {
  cards: null,
  showNewCardForm: false,
  stripeCustomerId: null,
};

export default injectStripe(SubscriptionCards);
