import React from 'react';
import en from '../../../international/translations/en';

const PaymentWarningsBanner = ({ message }) => {
  return (
    <div className="payment__warnings__message">
      <a href="#payment-details">{message}</a>
    </div>
  );
};

export default PaymentWarningsBanner;
