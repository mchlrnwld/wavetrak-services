import memoize from 'lru-memoize';
import {createValidator, required, minLength, match} from 'utils/validation';

const changePasswordValidation = createValidator({
  currentPassword: [required],
  newPassword: [required, minLength(6)],
  confirmPassword: [required, match('newPassword')]
});

export default memoize(10)(changePasswordValidation);
