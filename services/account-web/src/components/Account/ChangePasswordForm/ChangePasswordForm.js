/* eslint no-nested-ternary: 0 */
import PropTypes from 'prop-types';

import React, { Component } from 'react';
import en from '../../../international/translations/en';
import classnames from 'classnames';
import spinner from '../../../../static/btn-spinner.svg';

export default class ChangePasswordForm extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
    enableSubmit: PropTypes.func.isRequired,
    passwordErrorMessage: PropTypes.string.isRequired,
    passwordSuccessMessage: PropTypes.string.isRequired,
    passwordSubmitSucceeded: PropTypes.bool.isRequired,
    passwordSubmitting: PropTypes.bool.isRequired
  };

  setErrorClassName(field) {
    return classnames({
      ['changePasswordForm__error__text']: field.touched && field.error,
      ['changePasswordForm__error__text__hidden']: true
    });
  }

  setInputErrorClassName(field) {
    return classnames({
      ['changePasswordForm__input__hasError']: field.touched && field.error,
      ['changePasswordForm__input__isValid']: field.touched && !field.error || this.props.submitSucceeded
    });
  }

  render() {
    const {
      fields: { currentPassword, newPassword, confirmPassword, _id},
      passwordSubmitSucceeded,
      onSubmit,
      enableSubmit,
      passwordSubmitting,
      passwordErrorMessage,
      passwordSuccessMessage
    } = this.props;

    const {
      label_current, label_new, label_confirm /* eslint camelcase: 0 */
    } = en.change_password.form;

    const submitText = passwordSubmitSucceeded ?
      en.change_password.form.submit_success :
      en.change_password.form.save_button;

    const submitStyle = passwordSubmitSucceeded ?
      'changePasswordForm__button__submit__success' :
      'changePasswordForm__button__submit';

    const errorAlertStyle = passwordErrorMessage ?
      'changePasswordForm__error__alert' :
      'changePasswordForm__error__alert__hidden';

    const successAlertStyle = passwordSuccessMessage ?
      'changePasswordForm__success__alert' :
      'changePasswordForm__success__alert__hidden';

    return (
      <div>
        <div className={passwordSuccessMessage ? successAlertStyle : errorAlertStyle}>
          {passwordSuccessMessage ? passwordSuccessMessage : passwordErrorMessage}
        </div>
        <form onSubmit={onSubmit} className="changePasswordForm">
          <input type="hidden" {..._id} />
          <div>
            <div className="changePasswordForm__input__currentPassword" onClick={enableSubmit}>
              <input id={currentPassword.name} className={this.setInputErrorClassName(currentPassword)} type="password" {...currentPassword} />
              <label htmlFor={currentPassword.name} className="changePasswordForm__label__floating">{label_current}</label>
              <div className={this.setErrorClassName(currentPassword)}>{currentPassword.error}</div>
            </div>
          </div>
          <div>
            <div className="changePasswordForm__input__newPassword" onClick={enableSubmit}>
              <input id={newPassword.name} className={this.setInputErrorClassName(newPassword)} type="password" defaultValue="" {...newPassword} />
              <label htmlFor={newPassword.name} className="changePasswordForm__label__floating">{label_new}</label>
              <div className={this.setErrorClassName(newPassword)}>{newPassword.error}</div>
            </div>
          </div>
          <div>
            <div className="changePasswordForm__input__confirmPassword" onClick={enableSubmit}>
              <input id={confirmPassword.name} className={this.setInputErrorClassName(confirmPassword)} type="password" defaultValue="" {...confirmPassword} />
              <label htmlFor={confirmPassword.name} className="changePasswordForm__label__floating">{label_confirm}</label>
              <div className={this.setErrorClassName(confirmPassword)}>{confirmPassword.error}</div>
            </div>
          </div>
          <div>
            <div>
              <button disabled={passwordSubmitting || passwordSubmitSucceeded} className={(passwordSubmitting ? 'changePasswordForm__button__submit__submitting' : submitStyle)}>
                {passwordSubmitting ? <img src={spinner} className="changePasswordForm__button__submit__submitting__img" /> : submitText}
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
