import PropTypes from 'prop-types';

import React, { memo, useState, useMemo } from 'react';
import { Select, Button, TooltipProvider, QuestionMark } from '@surfline/quiver-react';
import { updateUserTimezone } from '../../../common/api/user';
import en from '../../../international/translations/en';
import zones from './zones.json';
import config from '../../../config';

const EditSettingsForm = ({
  userSettings,
  onSettingChange,
  editSettingsErrorMessage,
  editSettingsSuccessMessage,
  redirectUrl,
  userTimezone,
  accessToken,
  hawaiianScaleFlag,
}) => {
  const isSurfline = config.company === 'sl';

  // Compute the user's zone based off their currently set `userTimezone`
  const userZone = useMemo(() => {
    if (userTimezone) {
      return zones.find((zone) => zone.timezones.find((timezone) => timezone === userTimezone));
    }
    return null;
  }, [userTimezone]);

  const [zone, setZone] = useState(userZone);
  const [timezone, setTimezone] = useState(userTimezone);

  const timezones = useMemo(() => {
    if (zone) {
      return [{ text: 'Select a timezone', value: 'hidden-default' }].concat(
        zone.timezones.map((tz) => ({ text: tz, value: tz })),
      );
    }
    return null;
  }, [zone]);

  const renderTimezoneSelect = isSurfline && userTimezone && zone;
  // setup internationalized text
  /* eslint camelcase: 0 */
  const {
    label_windSpeed,
    label_surfHeight,
    label_swellHeight,
    label_tideHeight,
    label_temperature,
    label_localizedFeed,
    label_dateFormat,
  } = en.edit_settings.form;

  let messageAlertStyle;
  if (editSettingsSuccessMessage || editSettingsErrorMessage) {
    messageAlertStyle = editSettingsSuccessMessage
      ? 'editSettingsForm__success__alert'
      : 'editSettingsForm__error__alert';
  }

  const onChange = (fieldName, fieldType) => (selectEvent) =>
    onSettingChange(fieldName, selectEvent.target.value, fieldType, hawaiianScaleFlag);
  const onZoneChange = (event) => {
    const newZone = zones.find((z) => z.value === event.target.value);
    setZone(newZone);
  };
  const onTimezoneChange = async (event) => {
    const { value } = event.target;
    setTimezone(value);
    await updateUserTimezone(accessToken, value);
  };

  const errorMessage = editSettingsErrorMessage
    ? 'An error occurred.  Please try again later.'
    : '';

  const surfHeightOptions = [
    { text: 'Feet (Face Height)', value: 'FT' },
    { text: 'Meters', value: 'M' },
  ];

  if (hawaiianScaleFlag === 'on')
    surfHeightOptions.splice(1, 0, { text: 'Feet (Traditional Height)', value: 'HI' });

  // Do not expose actual error message for potential security issues.
  return (
    <div className="editSettingsForm">
      <div className={messageAlertStyle}>{editSettingsSuccessMessage || errorMessage}</div>
      <div className="editSettingsForm__select">
        <Select
          id="windSpeed"
          label={label_windSpeed}
          defaultValue="KT"
          select={{
            onChange: onChange('windSpeed', 'units'),
            value: userSettings.units.windSpeed,
          }}
          options={[
            { text: 'Knots', value: 'KTS' },
            { text: 'Miles per hour', value: 'MPH' },
            { text: 'Kilometers per hour', value: 'KPH' },
          ]}
        />
        <div className="editSettingsForm__tooltip" />
      </div>
      <div className="editSettingsForm__select">
        <Select
          id="surfHeight"
          label={hawaiianScaleFlag === 'on' ? label_surfHeight : 'Surf/Swell Height'}
          defaultValue="FT"
          select={{
            onChange: onChange('surfHeight', 'units'),
            value: userSettings.units.surfHeight,
          }}
          options={surfHeightOptions}
        />
        <div className="editSettingsForm__tooltip">
          <TooltipProvider
            renderTooltip={() => (
              <div className="hawaiian-scale-toggle__text">
                Surfline typically uses face height in feet to describe wave size. We also provide
                the option to render surf height in meters, or choose what we call the “Traditional”
                scale, which is popular in Australia and Hawaii. The Traditional scale corresponds
                to approximately one half the height of the wave face.
              </div>
            )}
          >
            <div className="hawaiian-scale-toggle__tooltip">
              {' '}
              <a
                href="https://www.surfline.com/surf-news/surflines-rating-surf-heights-quality/1417"
                target="_blank"
                rel="noreferrer"
              >
                <QuestionMark />
              </a>
            </div>
          </TooltipProvider>
        </div>
      </div>
      {hawaiianScaleFlag === 'on' && (
        <div className="editSettingsForm__select">
          <Select
            id="swellHeight"
            label={label_swellHeight}
            defaultValue="FT"
            select={{
              onChange: onChange('swellHeight', 'units'),
              value: userSettings.units.swellHeight,
            }}
            options={[
              { text: 'Feet', value: 'FT' },
              { text: 'Meters', value: 'M' },
            ]}
          />
          <div className="editSettingsForm__tooltip" />
        </div>
      )}
      <div className="editSettingsForm__select">
        <Select
          id="tideHeight"
          label={label_tideHeight}
          defaultValue="FT"
          select={{
            onChange: onChange('tideHeight', 'units'),
            value: userSettings.units.tideHeight,
          }}
          options={[
            { text: 'Feet', value: 'FT' },
            { text: 'Meters', value: 'M' },
          ]}
        />
        <div className="editSettingsForm__tooltip" />
      </div>
      <div className="editSettingsForm__select">
        <Select
          id="temperature"
          label={label_temperature}
          defaultValue="F"
          select={{
            onChange: onChange('temperature', 'units'),
            value: userSettings.units.temperature,
          }}
          options={[
            { text: 'Celsius', value: 'C' },
            { text: 'Fahrenheit', value: 'F' },
          ]}
        />
        <div className="editSettingsForm__tooltip" />
      </div>
      <div className="editSettingsForm__select">
        <Select
          id="feedLocalized"
          label={label_localizedFeed}
          defaultValue="LOCALIZED"
          select={{
            onChange: onChange('localized', 'feed'),
            value: userSettings.feed.localized,
          }}
          options={[
            { text: 'Localized', value: 'LOCALIZED' },
            { text: 'US', value: 'US' },
            { text: 'AU', value: 'AU' },
            { text: 'NZ', value: 'NZ' },
          ]}
        />
        <div className="editSettingsForm__tooltip" />
      </div>
      {hawaiianScaleFlag === 'on' && !!userSettings.date && (
        <div className="editSettingsForm__select">
          <Select
            id="dateFormat"
            label={label_dateFormat}
            defaultValue="MDY"
            select={{
              onChange: onChange('format', 'date'),
              value: userSettings.date.format,
            }}
            options={[
              { text: 'Month Day Year', value: 'MDY' },
              { text: 'Day Month Year', value: 'DMY' },
            ]}
          />
          <div className="editSettingsForm__tooltip" />
        </div>
      )}
      {renderTimezoneSelect && <br />}
      {renderTimezoneSelect && (
        <div className="editSettingsForm__select">
          <Select
            id="userZone"
            label="Timezone"
            defaultValue={zone.value}
            select={{
              onChange: onZoneChange,
              value: zone.value,
            }}
            options={zones}
          />
          <div className="editSettingsForm__tooltip" />
        </div>
      )}
      {renderTimezoneSelect && (
        <div className="editSettingsForm__select">
          <Select
            id="userTimezone"
            defaultValue={userTimezone}
            select={{
              onChange: onTimezoneChange,
              value: zone.timezones.includes(timezone) ? timezone : 'default',
            }}
            options={timezones}
          />
          <div className="editSettingsForm__tooltip" />
        </div>
      )}
      {redirectUrl && <br />}
      {redirectUrl ? (
        <Button
          onClick={() => {
            window.location.href = redirectUrl;
          }}
        >
          Go Back
        </Button>
      ) : null}
    </div>
  );
};

EditSettingsForm.propTypes = {
  userSettings: PropTypes.shape({
    model: PropTypes.oneOf(['LOLA', 'LOTUS']),
    feed: PropTypes.shape({
      localized: PropTypes.string,
    }),
    units: PropTypes.shape({
      temperature: PropTypes.string,
      tideHeight: PropTypes.string,
      windSpeed: PropTypes.string,
      surfHeight: PropTypes.string,
      swellHeight: PropTypes.string,
    }),
    date: PropTypes.shape({
      format: PropTypes.string,
    }),
  }).isRequired,
  onSettingChange: PropTypes.func.isRequired,
  editSettingsErrorMessage: PropTypes.string,
  editSettingsSuccessMessage: PropTypes.string,
  redirectUrl: PropTypes.string,
  userTimezone: PropTypes.string,
  accessToken: PropTypes.string.isRequired,
  hawaiianScaleFlag: PropTypes.string.isRequired,
};

EditSettingsForm.defaultProps = {
  editSettingsErrorMessage: '',
  editSettingsSuccessMessage: '',
  redirectUrl: '',
  userTimezone: '',
};

export default memo(EditSettingsForm);
