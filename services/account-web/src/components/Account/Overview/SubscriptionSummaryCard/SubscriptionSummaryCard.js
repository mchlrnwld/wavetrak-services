import PropTypes from 'prop-types';
import React, { useRef, useMemo } from 'react';
import { TrackableLink } from '@surfline/quiver-react';
import en from '../../../../international/translations/en';
import config from '../../../../config';
import daysFromNowUntil from '../../../../utils/daysFromNowUntil';
import { parseBillingDate } from '../../../../utils/getPlanSubHeadingUtils';

const getExpiringMessage = (subscription, periodEnd, brand) => {
  if (subscription.type === 'apple') return en.subscription.subheading_apple;
  if (subscription.type === 'google') return en.subscription.subheading_google;
  if (subscription.type === 'vip') return '';
  if (subscription.expiring) {
    return en.subscription.subheading_expiring(parseBillingDate(periodEnd), brand);
  }
  return null;
};

const BRAND_NAME_MAP = { sl: 'Surfline', bw: 'Buoyweather', fs: 'FishTrack' };

const AddPremiumLink = ({ brand }) => {
  const ref = useRef();
  const eventProperties = useMemo(
    () => ({
      location: 'account-overview summary card',
      brandSelected: brand,
    }),
    [brand],
  );

  const copy = en.account.overview.subs_card;

  return (
    <TrackableLink ref={ref} eventName="Clicked Subscribe CTA" eventProperties={eventProperties}>
      <a
        ref={ref}
        className="subscription__card__message__right__link"
        href={`${config.accountLinkProtocol}${BRAND_NAME_MAP[brand]}.com${config.selectPlanRedirect}`}
        target="_blank"
        rel="noreferrer"
      >
        {copy.add_premium}
      </a>
    </TrackableLink>
  );
};

const SubscriptionSummaryCard = ({ subscription, companyMap }) => {
  const copy = en.account.overview.subs_card;

  return (
    <div className="subscription__card">
      {companyMap.map((brand) => {
        const [sub] = subscription.subscriptions[brand];
        return (
          <div key={brand} className="subscription__card__inner">
            {sub &&
            (sub.entitlement === `${brand}_premium` || sub.entitlement === `${brand}_vip`) ? (
              <div className="subscription__card__message">
                <div className="subscription__card__message__left">
                  <h5 className="subscription__card__message__left__title">
                    {BRAND_NAME_MAP[brand]}
                  </h5>
                  <p className="subscription__card__message__left__entitlement">{copy[sub.type]}</p>
                </div>
                <div className="subscription__card__message__right">
                  {sub && (sub.expiring || sub.entitlement === `${brand}_vip`) ? (
                    <p className="subscription__card__message__right__message">
                      {getExpiringMessage(sub, sub.periodEnd, BRAND_NAME_MAP[brand])}
                    </p>
                  ) : (
                    <p className="subscription__card__message__right__message">
                      {copy.message(daysFromNowUntil(sub.periodEnd), sub.onTrial)}
                    </p>
                  )}
                </div>
              </div>
            ) : (
              <div className="subscription__card__message">
                <div className="subscription__card__message__left">
                  <h5 className="subscription__card__message__left__title">
                    {BRAND_NAME_MAP[brand]}
                  </h5>
                </div>
                <div className="subscription__card__message__right">
                  <i
                    className="subscription__card__message__right__icon fa fa-plus"
                    aria-hidden="true"
                  />
                  <AddPremiumLink brand={brand} />
                </div>
              </div>
            )}
          </div>
        );
      })}
    </div>
  );
};

AddPremiumLink.propTypes = {
  brand: PropTypes.oneOf(['sl', 'bw', 'fs']).isRequired,
};

SubscriptionSummaryCard.propTypes = {
  subscription: PropTypes.shape().isRequired,
  companyMap: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default SubscriptionSummaryCard;
