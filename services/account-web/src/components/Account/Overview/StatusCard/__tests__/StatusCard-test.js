import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { StatusMessage } from 'components';
import StatusCard from '../StatusCard';

describe('Components / Account / Overview / <StatusCard />', () => {
  let delinquentStatusMessage;

  before(() => {
    const delinquentStatusCard = shallow(
      <StatusCard
        subscription={{ periodStart: '06/21/2016', failedPaymentAttempts: 2 }}
        brand={'sl'}
      />
    );

    delinquentStatusMessage = delinquentStatusCard.find(StatusMessage);
  });

  it('should render a StatusMessage component with daysUntilCancelDate', () => {
    expect(delinquentStatusMessage.props().daysUntilCancelDate).to.be.above(5);
  });
});
