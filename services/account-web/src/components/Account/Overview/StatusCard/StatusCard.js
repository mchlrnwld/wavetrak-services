import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StatusMessage } from 'components';

/* DEPRECATED - DO NOT REUSE */
export default class StatusCard extends Component {

  static propTypes = {
    subscription: PropTypes.array.isRequired,
    brand: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
  }

  getCancelDate(startDate) {
    const cancelDate = this.addDays(startDate, 7);
    const today = new Date();
    const daysUntilCancelDate = Math.abs(cancelDate - today);
    return Math.round(daysUntilCancelDate / 1000 / 60 / 60 / 24);
  }

  addDays(date, days) {
    const result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }

  render() {
    const { subscription, brand } = this.props;
    const daysUntilCancelDate = (subscription.failedPaymentAttempts > 0) ?
      this.getCancelDate(subscription.periodStart) :
      null;

    return (
      <StatusMessage
        theme={'delinquent'}
        subscription={subscription}
        brand={brand}
        daysUntilCancelDate={daysUntilCancelDate}
      />
    );
  }
}
