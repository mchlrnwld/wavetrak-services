import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { TrackableLink } from '@surfline/quiver-react';
import { OverviewAction } from '../../..';
import en from '../../../../international/translations/en';
import config from '../../../../config';

class InfoCard extends Component {
  constructor(props) {
    super(props);

    const { subscription, brand } = this.props;
    const hasSubscription = subscription.subscriptions[brand].length;
    const { type, entitlement, expiring } = subscription.subscriptions?.[brand]?.[0] || {};

    this.linkRef = React.createRef();
    this.vip = hasSubscription ? type === 'vip' : false;
    this.entitlement = hasSubscription ? entitlement : 'free';
    this.expiring = hasSubscription ? expiring : false;
  }

  componentDidUpdate(prevProps) {
    const { subscription, brand } = this.props;
    if (prevProps.subscription !== subscription) {
      const hasSubscription = subscription.subscriptions[brand]?.length;
      const { entitlement, expiring } = subscription.subscriptions?.[brand]?.[0] || {};

      this.entitlement = hasSubscription ? entitlement : 'free';
      this.expiring = hasSubscription ? expiring : false;
    }
  }

  getHref = () => {
    const { brand, brandName } = this.props;
    let href;
    if (this.entitlement.indexOf('premium') > -1 || this.entitlement.indexOf('vip') > -1) {
      href = en.subscription.goPremium[brand].benefitsLink;
    } else {
      href = `${config.accountLinkProtocol}${brandName[brand]}.com${config.selectPlanRedirect}`;
    }
    return href;
  };

  getTrackProperties = () => {
    const { brand } = this.props;
    return {
      location: 'account-overview brand card',
      brandSelected: brand,
    };
  };

  render() {
    const { subscription, brand, renewHandler } = this.props;
    if (this.vip) {
      this.theme = 'vip';
    } else if (this.expiring) {
      this.theme = 'expiring';
    } else if (this.entitlement.indexOf('premium') > -1) {
      this.theme = 'premium';
    } else {
      this.theme = 'free';
    }

    const text = en.account.overview.info_card[this.theme];
    const href = this.getHref();

    return (
      <div className={`info__card--${brand}`}>
        <div className="info__card">
          <div>
            <h5 className="info__card__title">{text.title}</h5>
          </div>
          <div className="info__card__message__container">
            <p className="info__card__message__container__message">{text.message[brand]}</p>
            <div className="info__card__link">
              {(() => {
                if (
                  this.theme === 'expiring' &&
                  subscription.subscriptions[brand][0].type === 'stripe'
                ) {
                  return (
                    <OverviewAction
                      onClickHandler={() => renewHandler(subscription.subscriptions[brand][0])}
                      theme={this.theme}
                      isSubmitting={subscription.isRenewing}
                      isSubmittingSuccess={subscription.isRenewingSuccess}
                      isSubmittingError={subscription.isRenewingError}
                    >
                      {text.link_text}
                    </OverviewAction>
                  );
                }
                if (this.theme === 'premium' || this.theme === 'vip') {
                  return (
                    <a
                      className="info__card__link__premium"
                      href={href}
                      target="_blank"
                      rel="noreferrer"
                    >
                      {text.link_text}
                    </a>
                  );
                }
                return (
                  <TrackableLink
                    ref={this.linkRef}
                    eventName="Clicked Subscribe CTA"
                    eventProperties={this.getTrackProperties}
                  >
                    <a
                      ref={this.linkRef}
                      className="info__card__link__free"
                      target="_blank"
                      rel="noreferrer"
                      href={href}
                    >
                      {text.link_text}
                    </a>
                  </TrackableLink>
                );
              })()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

InfoCard.propTypes = {
  subscription: PropTypes.shape({
    subscriptions: PropTypes.shape({}),
    isRenewing: PropTypes.bool,
    isRenewingError: PropTypes.bool,
    isRenewingSuccess: PropTypes.bool,
  }).isRequired,
  renewHandler: PropTypes.func.isRequired,
  brand: PropTypes.string.isRequired,
  brandName: PropTypes.shape({
    sl: PropTypes.string,
    bw: PropTypes.string,
    fs: PropTypes.string,
  }),
};

InfoCard.defaultProps = {
  brandName: {
    sl: 'Surfline',
    bw: 'Buoyweather',
    fs: 'FishTrack',
  },
};

export default InfoCard;
