import PropTypes from 'prop-types';
import React, { Component } from 'react';

import { Chart }from 'components';

export default class CountDown extends Component {
  static propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    border: PropTypes.string,
    strokeWidth: PropTypes.number,
    strokeColor: PropTypes.string,
    labelColor: PropTypes.string,
    labelFontSize: PropTypes.string,
    railColor: PropTypes.string,
    fillColor: PropTypes.string,
    padding: PropTypes.number,
    daysRemaining: PropTypes.number,
    percent: PropTypes.number
  }

  static defaultProps = {
    width: 70,
    height: 65,
    border: 'none',
    strokeWidth: 3,
    strokeColor: '#fa5065',
    labelColor: '#333333',
    labelFontSize: '28px',
    railColor: '#e6e6e6',
    fillColor: 'none',
    padding: 0,
  }

  polarToCartesian(centerX, centerY, radius, angleInDegrees) {
    const angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;

    return {
      x: centerX + (radius * Math.cos(angleInRadians)),
      y: centerY + (radius * Math.sin(angleInRadians))
    };
  }

  describeArc(xx, yy, radius, startAngle, endAngle) {
    if (!xx || !yy) console.error('x or y missing to describeArc');

    const start = this.polarToCartesian(xx, yy, radius, endAngle);
    const end = this.polarToCartesian(xx, yy, radius, startAngle);
    const arcSweep = endAngle - startAngle <= 180 ? '0' : '1';

    return [
      'M', start.x, start.y,
      'A', radius, radius, 0, arcSweep, 0, end.x, end.y
    ].join(' ');
  }

  render() {
    const radius = this.props.width / 2 - this.props.strokeWidth / 2 - this.props.padding;
    const center = radius + this.props.strokeWidth / 2 + this.props.padding;
    const startAngle = 0;
    const percent = (this.props.daysRemaining === 0) ? 99.99 : this.props.percent;
    const countdownText = (this.props.daysRemaining === 1) ? 'day' : 'days';
    const endAngle = 3.6 * percent;
    const arc = this.describeArc(center, center, radius, startAngle, endAngle);
    const dx = {x: '34', y: '27'};
    if (this.props.daysRemaining < 10) {
      dx.x = '42';
    } else if (this.props.daysRemaining >= 100) {
      dx.x = '25';
    }
    return (
      <Chart width={this.props.width} height={this.props.height} border={this.props.border} type={'countdown'}>
        <circle
          cx={center}
          cy={center}
          r={radius}
          fill={this.props.fillColor}
          stroke={this.props.railColor}
          strokeWidth={this.props.strokeWidth}/>
        <path
          fill={this.props.fillColor}
          stroke={this.props.strokeColor}
          strokeWidth={this.props.strokeWidth}
          d={arc}/>
        <text
          x={dx.x}
          y={dx.y}
          dx="-.5em"
          dy=".4em"
          fill={this.props.labelColor}
          fontSize={this.props.labelFontSize}>
          {`${this.props.daysRemaining}`}
        </text>
        <text
          x="29"
          y="44"
          dx="-.5em"
          dy=".4em"
          fill={this.props.labelColor}
          fontSize="12px">
          {countdownText}
        </text>
      </Chart>
    );
  }
}
