import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Countdown from '../Countdown';

describe('Components / Account / Overview / <Countdown />', () => {
  const countdown = new Countdown();

  it('should convert polar to cartesian coordinates rotated 90 degrees clockwise', () => {
    const epsilon = 10 ** (-15);

    const conversion1 = countdown.polarToCartesian(0, 0, 0, 0);
    expect(conversion1.x).to.be.closeTo(0, epsilon);
    expect(conversion1.y).to.be.closeTo(0, epsilon);

    const conversion2 = countdown.polarToCartesian(1, 2, 4, 60);
    expect(conversion2.x).to.be.closeTo(1 + (2 * Math.sqrt(3)), epsilon);
    expect(conversion2.y).to.be.closeTo(0, epsilon);

    const conversion3 = countdown.polarToCartesian(-2, -1, -2, -30);
    expect(conversion3.x).to.be.closeTo(-1, epsilon);
    expect(conversion3.y).to.be.closeTo(-1 + Math.sqrt(3), epsilon);
  });

  it('should render path of arc dependent on percent', () => {
    const wrapper = shallow(<Countdown percent={50} daysRemaining={10} />);
    expect(wrapper.find('path').props().d).to.equal('M 35 68.5 A 33.5 33.5 0 0 0 35 1.5');
  });

  it('should render path of arc with 99.99 percent when daysRemaining is 0', () => {
    const wrapper = shallow(<Countdown percent={50} daysRemaining={0} />);
    expect(wrapper.find('path').props().d).to.equal('M 34.97895133060589 1.500006612634735 A 33.5 33.5 0 1 0 35 1.5');
  });

  it('should render text with appropriate x and daysRemaining', () => {
    const wrapper1 = shallow(<Countdown percent={50} daysRemaining={9} />);
    const wrapper2 = shallow(<Countdown percent={50} daysRemaining={10} />);
    const wrapper3 = shallow(<Countdown percent={50} daysRemaining={99} />);
    const wrapper4 = shallow(<Countdown percent={50} daysRemaining={100} />);

    const text1 = wrapper1.find('text').first();
    expect(text1.props().x).to.equal('42');
    expect(text1.text()).to.equal('9');

    const text2 = wrapper2.find('text').first();
    expect(text2.props().x).to.equal('34');
    expect(text2.text()).to.equal('10');

    const text3 = wrapper3.find('text').first();
    expect(text3.props().x).to.equal('34');
    expect(text3.text()).to.equal('99');

    const text4 = wrapper4.find('text').first();
    expect(text4.props().x).to.equal('25');
    expect(text4.text()).to.equal('100');
  });
});
