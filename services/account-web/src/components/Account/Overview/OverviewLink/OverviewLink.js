import PropTypes from 'prop-types';
import React, { useRef } from 'react';
import { Link } from 'react-router-dom';
import { TrackableLink } from '@surfline/quiver-react';

const OverviewLink = ({
  onClickHandler,
  eventName,
  eventProperties,
  children,
  theme,
  href,
  hrefExternal,
  hrefExternalTarget,
}) => {
  const hrefExternalRef = useRef();
  const hrefExternalTargetRef = useRef();

  let linkTag;

  const classname = `overviewLink__button${theme ? `--${theme}` : ''}`;

  if (hrefExternal) {
    linkTag = (
      <TrackableLink ref={hrefExternalRef} eventName={eventName} eventProperties={eventProperties}>
        <a ref={hrefExternalRef} className={classname} href={hrefExternal}>
          {children}
        </a>
      </TrackableLink>
    );
  } else if (hrefExternalTarget) {
    linkTag = (
      <TrackableLink
        ref={hrefExternalTargetRef}
        eventName={eventName}
        eventProperties={eventProperties}
      >
        <a
          ref={hrefExternalTargetRef}
          target="_blank"
          rel="noreferrer"
          className={classname}
          href={hrefExternalTarget}
        >
          {children}
        </a>
      </TrackableLink>
    );
  } else if (href) {
    linkTag = (
      <Link onClick={onClickHandler} className={classname} to={href}>
        {children}
      </Link>
    );
  } else {
    // eslint-disable-next-line jsx-a11y/anchor-is-valid
    linkTag = <a className={classname}>{children}</a>;
  }

  return <div>{linkTag}</div>;
};

OverviewLink.propTypes = {
  onClickHandler: PropTypes.func.isRequired,
  theme: PropTypes.oneOf(['primary', 'secondary']),
  children: PropTypes.node,
  href: PropTypes.string,
  hrefExternal: PropTypes.string,
  hrefExternalTarget: PropTypes.string,
  eventName: PropTypes.oneOf([PropTypes.string, PropTypes.func]),
  eventProperties: PropTypes.oneOf([PropTypes.shape(), PropTypes.func]),
};

OverviewLink.defaultProps = {
  theme: 'primary',
  children: null,
  href: null,
  hrefExternal: null,
  hrefExternalTarget: null,
  eventName: null,
  eventProperties: null,
};

export default OverviewLink;
