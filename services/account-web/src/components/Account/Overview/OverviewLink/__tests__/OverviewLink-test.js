import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import sinon from 'sinon';
import { Link } from 'react-router-dom';
import classNames from 'classnames/bind';
import OverviewLink from '../OverviewLink';
import styles from '../OverviewLink.scss';

describe('Components / Account / Overview / <OverviewLink />', () => {
  const clickHandler = sinon.spy();
  const hrefWrapper = shallow(
    <OverviewLink
      href={'surfline.com'}
      onClickHandler={clickHandler}
      theme={'test_theme'}
    >
    Href Link
    </OverviewLink>
  );
  const hrefExternalWrapper = shallow(
    <OverviewLink
      hrefExternal={'surfline.com'}
      onClickHandler={clickHandler}
      theme={'test_theme'}
    >
    Href External Link
    </OverviewLink>
  );
  const defaultWrapper = shallow(
    <OverviewLink
      onClickHandler={clickHandler}
    >
    Default Link
    </OverviewLink>
  );

  const hrefLink = hrefWrapper.find('div').children();
  const hrefExternalLink = hrefExternalWrapper.find('div').children();
  const defaultLink = defaultWrapper.find('div').children();

  it('should render the appropriate link tag', () => {
    expect(hrefLink.is(Link)).to.be.true;
    expect(hrefExternalLink.is('a')).to.be.true;
    expect(defaultLink.is('a')).to.be.true;
  });

  it('should render the link with the appropriate className', () => {
    const cx = classNames.bind(styles);

    expect(hrefLink.hasClass(cx('overviewLink__button--test_theme'))).to.be.true;
    expect(hrefExternalLink.hasClass(cx('overviewLink__button--test_theme'))).to.be.true;
    expect(defaultLink.hasClass(cx('overviewLink__button'))).to.be.true;
  });

  it('should render children', () => {
    expect(hrefLink.children().text()).to.equal('Href Link');
    expect(hrefExternalLink.children().text()).to.equal('Href External Link');
    expect(defaultLink.children().text()).to.equal('Default Link');
  });

  it('should handle clicks for links with href or hrefExternal', () => {
    hrefLink.simulate('click');
    hrefExternalLink.simulate('click');

    expect(clickHandler.calledTwice).to.be.true;

    defaultLink.simulate('click');

    expect(clickHandler.calledTwice).to.be.true;
  });
});
