import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { InfoCard, PriceIconCard } from 'components';

export default class PromoCard extends Component {
  static propTypes = {
    subscription: PropTypes.object.isRequired,
    overview: PropTypes.object.isRequired,
    renewHandler: PropTypes.func,
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { subscription, companyMap, overview, renewHandler } = this.props;
    const brandName = { sl: 'Surfline', bw: 'Buoyweather', fs: 'FishTrack' };

    return (
      <div>
        {(() => {
          return companyMap.map((brand) => {
            return (
              <div className="promo__card" key={brand}>
                <div className={`promo__card--${brand}`} key={brand}>
                  <InfoCard
                    subscription={subscription}
                    buttonType="primary"
                    brandName={brandName}
                    key={brand}
                    brand={brand}
                    overview={overview}
                    renewHandler={renewHandler}
                  />
                  <PriceIconCard brand={brand} subscription={subscription} />
                </div>
              </div>
            );
          });
        })()}
      </div>
    );
  }
}
