import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import DiscountCard from '../DiscountCard';
import { OverviewLink } from 'components';

describe('Components / Account / Overview / <DiscountCard />', () => {
  const wrapper = shallow(
    <DiscountCard
      entitlement={'Test Entitlement'}
      text={{
        title: 'Test Title',
        message: 'Test Message',
        link_text_secondary: 'Test Link Text Secondary'
      }}
      href={'surfline.com'}
    />);

  let windowAnalyticsTrackStub;

  before(() => {
    window.analytics = {
      track: () => true
    };
  });

  beforeEach(() => {
    windowAnalyticsTrackStub = sinon.stub(window.analytics, 'track');
  });

  afterEach(() => {
    window.analytics.track.restore();
  });

  it('should track clicks on discount link', () => {
    const link = wrapper.find(`discount__card__link`).find('a');
    link.simulate('click');
    expect(windowAnalyticsTrackStub.calledOnce).to.be.true;
    expect(windowAnalyticsTrackStub.calledWithExactly(
      'Clicked Email support@surfline.com to receive your discount code.',
      {
        location: 'account-overview',
        user: {
          type: 'sl_premium'
        }
      }
    )).to.be.true;
  });

  it('should track clicks on OverviewLink', () => {
    const link = wrapper.find(OverviewLink);
    const clickHandler = link.prop('onClickHandler');
    clickHandler();
    expect(windowAnalyticsTrackStub.calledOnce).to.be.true;
    expect(windowAnalyticsTrackStub.calledWithExactly(
      'Clicked Test Link Text Secondary',
      {
        location: 'account-overview',
        user: {
          type: 'sl_premium'
        }
      }
    )).to.be.true;
  });
});
