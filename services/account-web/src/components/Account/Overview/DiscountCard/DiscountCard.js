import PropTypes from 'prop-types';
import React from 'react';
import { TrackableLink } from '@surfline/quiver-react';
import { OverviewLink } from '../../..';

const emailEventName = 'Clicked Email support@surfline.com to receive your discount code.';
const emailEventProperties = {
  location: 'account-overview',
  user: {
    type: 'sl_premium',
  },
};

const overviewLinkProperties = {
  location: 'account-overview',
  user: {
    type: 'sl_premium',
  },
};

const DiscountCard = ({ text, href, theme }) => (
  <div className={`discount__card--${theme}`}>
    <div className="discount__card">
      <div>
        <p className="discount__card__title">{text.title}</p>
      </div>
      <div>
        <p className="discount__card__message">{text.message}</p>
      </div>
      <div>
        <p className="discount__card__message">
          Email{' '}
          <TrackableLink
            className="discount__card__link"
            href="mailto:support@surfline.com"
            eventName={emailEventName}
            eventProperties={emailEventProperties}
          >
            support@surfline.com
          </TrackableLink>{' '}
          to receive your discount code.
        </p>
      </div>
      <div className="discount__card__link__secondary">
        <OverviewLink
          eventName={`Clicked ${text.link_text_secondary}`}
          eventProperties={overviewLinkProperties}
          theme="secondary"
          hrefExternal={href}
        >
          {text.link_text_secondary}
        </OverviewLink>
      </div>
    </div>
  </div>
);

DiscountCard.propTypes = {
  text: PropTypes.shape({
    title: PropTypes.string,
    link_text_secondary: PropTypes.string,
    message: PropTypes.string,
  }).isRequired,
  href: PropTypes.string.isRequired,
  theme: PropTypes.string.isRequired,
};

export default DiscountCard;
