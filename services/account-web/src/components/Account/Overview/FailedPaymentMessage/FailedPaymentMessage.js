import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { trackEvent } from '@surfline/web-common';
import en from '../../../../international/translations/en';

class FailedPaymentMessage extends Component {
  trackClick = () => {
    trackEvent(`Clicked Update Payment Details`, {
      location: 'account-overview',
      user: {
        type: 'failedPayment',
      },
    });
  };

  render() {
    const { message } = this.props;
    return (
      <Link className="failed__payment__link" to="/account/subscription">
        <div className="failed__payment__message">
          <div className="failed__payment__message__title">
            {en.account.overview.failedPaymentMessage.title.toUpperCase()}
          </div>
          <div className="failed__payment__message__subtitle">{message}</div>
        </div>
      </Link>
    );
  }
}

export default FailedPaymentMessage;
