import PropTypes from 'prop-types';
import React, { Component } from 'react';

export default class StatusError extends Component {

  static propTypes = {
    statusErrorMessage: PropTypes.string
  }

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="statusError">{this.props.statusErrorMessage}</div>
    );
  }
}
