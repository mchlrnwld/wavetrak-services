import PropTypes from 'prop-types';
import React, { Component } from 'react';
import convertToMonthlyCost from '../../../../utils/convertToMonthlyCost';
export default class PriceIconCard extends Component {
  static propTypes = {
    brand: PropTypes.string,
    subscription: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.vip = this.props.subscription.subscriptions[this.props.brand].length ?
      this.props.subscription.subscriptions[this.props.brand][0].type === 'vip' :
      false;
    this.entitlement = this.props.subscription.subscriptions[this.props.brand].length ?
      this.props.subscription.subscriptions[this.props.brand][0].entitlement :
      'free';
    this.expiring = this.props.subscription.subscriptions[this.props.brand].length ?
      this.props.subscription.subscriptions[this.props.brand][0].expiring :
      false;

    if (this.vip) {
      this.theme = 'vip';
    } else if (this.expiring || (this.entitlement.indexOf('premium') < 0)) {
      this.theme = 'expiring';
    } else if (this.entitlement.indexOf('premium') > -1) {
      this.theme = 'premium';
    } else {
      this.theme = 'free';
    }
  }

  monthlyCost = () => {
    const annualPlan = this.props.subscription.availablePlans[this.props.brand].find(brandplan => brandplan.interval === 'year');
    const cents = convertToMonthlyCost(annualPlan.amount, annualPlan.interval);
    const cost = parseFloat((cents / 100)).toFixed(2);
    return cost;
  };

  render() {
    const { brand } = this.props;
    return (
      <div className="benefits__card">
        {(() => {
          if (this.theme === 'expiring' || this.theme === 'free') {
            return null;
          }
          return (
            <div className={`benefits__card--${brand}`}>
              <div className="benefits__card__icon__container">
                <div className="benefits__card__icon__container__icon">
                  <div className="benefits__card__icon__container__icon__check" />
                </div>
              </div>
            </div>
          );
        })()}
      </div>
    );
  }
}
