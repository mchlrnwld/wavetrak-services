import PropTypes from 'prop-types';
import React, { useMemo } from 'react';
import { trackEvent } from '@surfline/web-common';
import { Link } from 'react-router-dom';

const trackClick = () => {
  trackEvent('Clicked Edit Profile', { location: 'account-overview' });
};

const UserCard = ({ user }) => {
  const { firstName, lastName } = user;

  const fullName = useMemo(() => `${firstName} ${lastName}`, [firstName, lastName]);

  return (
    <div className="user__card">
      <div className="user__card__info">
        <p className="user__card__info__name">{fullName}</p>
        <Link onClick={trackClick} className="user__card__info__link" to="/account/edit-profile">
          Edit Profile
        </Link>
      </div>
    </div>
  );
};

UserCard.propTypes = {
  user: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }).isRequired,
};

export default UserCard;
