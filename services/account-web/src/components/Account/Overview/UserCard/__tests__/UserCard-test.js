import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import { Link } from 'react-router-dom';
import UserCard from '../UserCard';

describe('Components / Account / Overview / <UserCard />', () => {
  const user = {
    firstName: 'Kelly',
    lastName: 'Slater'
  };
  let defaultUserCard;
  let defaultUserCardInfo;
  let defaultUserCardLink;

  before(() => {
    window.analytics = {
      track: sinon.spy()
    };

    defaultUserCard = shallow(<UserCard user={user} entitlement={''} />);

    defaultUserCardInfo = defaultUserCard.find(`user__card__info`).text();

    defaultUserCardLink = defaultUserCard.find(Link);
  });

  it('should display user\'s full name', () => {
    expect(defaultUserCardInfo).to.have.string('Kelly Slater');
  });

  it('should render Link component', () => {
    expect(defaultUserCardLink).to.have.length(1);
  });

  it('should track clicks on Link component', () => {
    expect(window.analytics.track.called).to.be.false;
    defaultUserCardLink.simulate('click');
    expect(window.analytics.track.called).to.be.true;
    expect(window.analytics.track.calledWithExactly(
      'Clicked Edit Profile',
      {
        location: 'account-overview'
      }
    )).to.be.true;
  });
});
