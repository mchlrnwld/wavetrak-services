import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import OverviewAction from '../OverviewAction';

describe('Components / Account / Overview / <OverviewAction />', () => {
  const defaultWrapper = shallow(<OverviewAction />);
  const submittingWrapper = shallow(<OverviewAction isSubmitting />);
  const submittedWrapper = shallow(<OverviewAction isSubmittingSuccess />);

  it('should handle clicks', () => {
    const onClickHandler = sinon.spy();
    const wrapper = shallow(<OverviewAction onClickHandler={onClickHandler} />);
    wrapper.simulate('click');
    expect(onClickHandler.calledOnce).to.be.true;
  });

  it('should be disabled if submitting or submitting succeeded', () => {
    expect(submittingWrapper.prop('disabled')).to.be.ok;
    expect(submittedWrapper.prop('disabled')).to.be.ok;
    expect(defaultWrapper.prop('disabled')).not.to.be.ok;
  });

  it('should set className based on submitting status', () => {
    expect(submittingWrapper.hasClass('overviewAction__button')).to.be.true;
    expect(submittingWrapper.hasClass('overviewAction__button__isSubmitting')).to.be.true;

    expect(submittedWrapper.hasClass('overviewAction__button')).to.be.true;
    expect(submittedWrapper.hasClass('overviewAction__button__isSubmittingSuccess')).to.be.true;

    expect(defaultWrapper.prop('className')).to.equal('overviewAction__button');
  });

  it('should show spinner when submitting', () => {
    expect(submittingWrapper.find(`overviewAction__button__isSubmitting__img`))
      .to.have.lengthOf(1);
    expect(submittedWrapper.find(`overviewAction__button__isSubmitting__img`))
      .to.have.lengthOf(0);
    expect(submittedWrapper.find(`overviewAction__button__isSubmitting__img`))
      .to.have.lengthOf(0);
  });
});
