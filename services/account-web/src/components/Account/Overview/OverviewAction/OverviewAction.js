import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames';
import spinner from '../../../../../static/btn-spinner.svg';

export default class OverviewAction extends Component {
  static propTypes = {
    onClickHandler: PropTypes.func,
    children: PropTypes.string,
    isSubmitting: PropTypes.bool,
    isSubmittingSuccess: PropTypes.bool,
    isSubmittingError: PropTypes.bool,
    text: PropTypes.string

  };

  constructor(props) {
    super(props);
  }

  render() {
    const {
      onClickHandler,
      isSubmitting,
      isSubmittingSuccess,
      children,
    } = this.props;
    const actionText = isSubmittingSuccess ? null : children;
    const className = classNames({
      'overviewAction__button': !isSubmitting || !isSubmittingSuccess,
      'overviewAction__button__isSubmitting': isSubmitting,
      'overviewAction__button__isSubmittingSuccess': isSubmittingSuccess
    });
    return (
      <button
        onClick={onClickHandler}
        disabled={isSubmitting || isSubmittingSuccess}
        className={className}>
        {isSubmitting ? <img src={spinner} className="overviewAction__button__isSubmitting__img" /> : actionText}
      </button>
    );
  }
}

