import PropTypes from 'prop-types';
import React from 'react';
import { trackEvent } from '@surfline/web-common';
import { OverviewLink } from '../../..';
import en from '../../../../international/translations/en';

const StatusMessage = ({ subscription, brand, theme, daysUntilCancelDate }) => {
  const copy = en.account.overview.statusCard.delinquent;

  const trackClick = () => {
    trackEvent(`Clicked ${copy.link_text}`, {
      location: 'account-overview',
      user: {
        type: subscription,
      },
    });
  };

  const buttonTheme = theme === 'delinquent' ? 'warning' : 'primary';
  const brandName = { sl: 'Surfline', bw: 'Buoyweather', fs: 'FishTrack' };

  return (
    <div className={`status${theme ? `--${theme}` : ''}`}>
      <div className="status__message">
        <span className="status__message__title">{copy.title}</span>
        <p>{copy.message(daysUntilCancelDate, brandName[brand])}</p>
      </div>
      <div className="status__link">
        <OverviewLink onClickHandler={trackClick} theme={buttonTheme} href="/account/subscription">
          {copy.link_text}
        </OverviewLink>
      </div>
    </div>
  );
};

StatusMessage.propTypes = {
  subscription: PropTypes.shape({}).isRequired,
  theme: PropTypes.string.isRequired,
  brand: PropTypes.string.isRequired,
  daysUntilCancelDate: PropTypes.string.isRequired,
};

export default StatusMessage;
