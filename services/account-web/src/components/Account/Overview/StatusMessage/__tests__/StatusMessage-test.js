import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { OverviewLink, Countdown } from 'components';
import en from '../../../../../international/translations/en';
import StatusMessage from '../StatusMessage';

describe('Components / Account / Overview / <StatusMessage />', () => {
  const delinquentMessage = en.account.overview.statusCard.delinquent;

  let delinquentStatusMessage;
  let delinquentOverviewLink;

  before(() => {
    window.analytics = {
      track: sinon.spy()
    };

    delinquentStatusMessage = shallow(
      <StatusMessage
        theme={'delinquent'}
        daysUntilCancelDate={5} />
    );

    delinquentOverviewLink = delinquentStatusMessage.find(`status__link`).children();
  });

  it('should use the appropriate classname', () => {
    expect(delinquentStatusMessage.hasClass('status--delinquent')).to.be.true;
  });

  it('should render Countdown if expiring', () => {
    expect(delinquentStatusMessage.find(Countdown).isEmpty()).to.be.true;
  });

  it('should render appropriate title and message', () => {
    const delinquentActualMessage = delinquentStatusMessage.find(`status__message`).text();
    expect(delinquentActualMessage).to.equal(delinquentMessage.message(5));
  });

  it('should render appropriate action or link component', () => {
    expect(delinquentOverviewLink.is(OverviewLink)).to.be.true;
    expect(delinquentOverviewLink.prop('theme')).to.equal('warning');
    expect(delinquentOverviewLink.prop('href')).to.equal('/account/subscription');
    expect(delinquentOverviewLink.prop('children')).to.equal(delinquentMessage.link_text);
  });

  it('should track clicks unless expiring', () => {
    expect(window.analytics.track.called).to.be.false;
    delinquentOverviewLink.prop('onClickHandler')();
    expect(window.analytics.track.called).to.be.true;
  });
});
