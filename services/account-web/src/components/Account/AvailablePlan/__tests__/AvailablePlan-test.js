import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import { MonthlyCost } from 'components';
import en from '../../../../international/translations/en';
import AvailablePlan from '../AvailablePlan';

describe('Components / Account / <AvailablePlan />', () => {
  let availablePlanWrapper;
  let selectedPlanWrapper;
  let onClickHandler;

  before(() => {
    onClickHandler = sinon.spy();
    availablePlanWrapper = shallow(
      <AvailablePlan
        plan={{ interval: 'month', amount: 5000 }}
        onClickHandler={onClickHandler} />
    );
    selectedPlanWrapper = shallow(
      <AvailablePlan
        plan={{ interval: 'year', amount: 7000 }}
        onClickHandler={onClickHandler}
        disabled
        selected />
    );
  });

  it('should display heading', () => {
    const heading = availablePlanWrapper.find('.availablePlan__heading');
    const selectedHeading = selectedPlanWrapper.find('.availablePlan__heading');
    expect(heading.text()).to.equal(en.subscription.plans.monthly);
    expect(selectedHeading.text()).to.equal(en.subscription.plans.yearly);
  });

  it('should display monthly cost for unselected plans', () => {
    const monthlyCost = availablePlanWrapper.find(MonthlyCost);
    const selectedMonthlyCost = selectedPlanWrapper.find(MonthlyCost);
    expect(monthlyCost.prop('cost')).to.equal('50.00');
    expect(selectedMonthlyCost.prop('cost')).to.equal('5.83');
  });

  it('should display summary', () => {
    const summary = availablePlanWrapper.find('.availablePlan__summary');
    const selectedSummary = selectedPlanWrapper.find('.availablePlan__summary');
    expect(summary.text()).to.equal(`Go ${en.subscription.plans.monthly}`);
    expect(selectedSummary.text()).to.equal(en.subscription.availablePlans.current_subscription);
  });

  it('should display icon for selected plans', () => {
    const icon = availablePlanWrapper.find('.availablePlan__icon__container');
    const selectedIcon = selectedPlanWrapper.find('.availablePlan__icon__container');
    expect(icon).to.have.length(0);
    expect(selectedIcon).to.have.length(1);
  });

  it('should add disabled class for disabled plans', () => {
    const classNames = availablePlanWrapper.prop('className');
    const selectedClassNames = selectedPlanWrapper.prop('className');

    expect(classNames).not.to.contain('availablePlan__container--selected');
    expect(selectedClassNames).to.contain('availablePlan__container--disabled');
  });

  it('should handle clicks', () => {
    expect(onClickHandler.called).to.be.false;

    availablePlanWrapper.simulate('click');
    expect(onClickHandler.calledOnce).to.be.true;

    selectedPlanWrapper.simulate('click');
    expect(onClickHandler.calledTwice).to.be.true;
  });
});
