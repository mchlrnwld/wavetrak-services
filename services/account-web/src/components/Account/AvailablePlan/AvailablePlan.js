import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames';
import { MonthlyCost } from 'components';
import readableInterval from '../../../utils/readableInterval';
import en from '../../../international/translations/en';
import convertToMonthlyCost from '../../../utils/convertToMonthlyCost';

export default class AvailablePlan extends Component {
  static propTypes = {
    plan: PropTypes.shape({}).isRequired,
    currentPlanAnnualAmount: PropTypes.number,
    disabled: PropTypes.bool,
    selected: PropTypes.bool,
    theme: PropTypes.string,
    onClickHandler: PropTypes.func.isRequired,
    discountObj: PropTypes.shape({}),
  };

  setDescription = (plan, currentPlanAnnualAmount, desc) => {
    const { interval, amount, symbol } = plan;
    if (desc.indexOf('${amount}') === -1 && desc.indexOf('${saving}') === -1) {
      return desc;
    }
    let { default_description: description } = en.subscription.availablePlan;
    const planAnnualAmount = this.getPlanAnnualAmount(
      readableInterval(interval),
      amount,
    );
    const savings = currentPlanAnnualAmount - planAnnualAmount;
    const fixedSavings = `${symbol}${savings.toFixed(2)}`;
    const fixedAmount = `${symbol}${this.amountToFixed(amount)}`;

    if (savings > 0) {
      description = desc
        .replace('${amount}', fixedAmount)
        .replace('${saving}', fixedSavings);
    }
    return description;
  };

  getPlanAnnualAmount = (interval, amount) => {
    let annualAmount = amount;
    const { daily, monthly } = en.subscription.plans;
    if (interval === daily) {
      annualAmount = amount * 365;
    }
    if (interval === monthly) {
      annualAmount = amount * 12;
    }
    return this.amountToFixed(annualAmount);
  };

  amountToFixed = amount => parseFloat(amount / 100).toFixed(2);

  monthlyCost = () => {
    const { interval, amount } = this.props.plan;
    const cents = convertToMonthlyCost(amount, interval);
    return this.amountToFixed(cents);
  };

  render() {
    const {
      plan,
      selected,
      disabled,
      onClickHandler,
      theme,
      discountObj,
      currentPlanAnnualAmount,
      trialEligible,
    } = this.props;

    const {
      current_subscription: currentSubText,
    } = en.subscription.availablePlans;
    const {
      default_description: defaultDescriptionText,
    } = en.subscription.availablePlan;
    const { interval, description, trialPeriodDays } = plan;
    const heading = readableInterval(interval);
    const summary = selected ? currentSubText : `Go ${heading}`;

    const containerClasses = classNames({
      availablePlan__container: !theme,
      'availablePlan__container--disabled': !theme && disabled,
      'availablePlan__container--alt': theme === 'alt',
      'availablePlan__container--alt--selected': theme === 'alt' && selected,
      'availablePlan__container--alt--selected--discount':
        theme === 'alt' &&
        selected &&
        discountObj &&
        discountObj.discountSuccess,
    });

    const iconClasses = classNames({
      availablePlan__icon__container__icon: true,
      'availablePlan__icon__container__icon--discount':
        theme === 'alt' && discountObj && discountObj.discountSuccess,
    });

    const freeTrialClassName = classNames({
      'availablePlan__free-trial-label': true,
      'availablePlan__free-trial-label--selected': selected
    });
    return (
      <div>
        <li
          onClick={onClickHandler}
          className={containerClasses}
        >
          <div className="availablePlan__details">
            <div className="availablePlan__heading">{heading}</div>
            {selected ? (
              <div className="availablePlan__icon__container">
                <div className={iconClasses}>
                  <div className="availablePlan__icon__container__icon__check" />
                </div>
              </div>
            ) : null}
            <MonthlyCost cost={this.monthlyCost()} theme={theme} symbol={plan.symbol}/>
            <div className="availablePlan__description">
              {plan.description
                ? this.setDescription(plan, currentPlanAnnualAmount, description)
                : defaultDescriptionText}
            </div>
          </div>
          {!theme ? (
            <div className="availablePlan__summary">{summary}</div>
          ) : null}
        </li>
        {trialPeriodDays && trialEligible ? (
          <div className={freeTrialClassName}>
            {`${trialPeriodDays} Days Free`}
          </div>
        ) : null}
      </div>
    );
  }
}
