import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { AvailablePlan } from 'components';
import en from '../../../international/translations/en';

export default class AvailablePlans extends Component {
  static propTypes = {
    subscription: PropTypes.object,
    plans: PropTypes.array.isRequired,
    onSelectHandler: PropTypes.func.isRequired,
    isChanging: PropTypes.bool,
  };

  onClickHandler = (plan, selected) => {
    if (!selected && !this.props.isChanging) {
      const { subscription } = this.props;
      const {
        plan: { interval },
      } = subscription;
      if (interval.match(/month/g)) {
        const upgradeMessage = en.subscription.annual_plan_switch_confirm_message(
          plan.amount,
          plan.symbol,
        );
        const result = confirm(upgradeMessage);
        if (!result) {
          return false;
        }
      }
      this.props.onSelectHandler(subscription, plan.id);
    }
  };

  isSelectedPlan = (plan) => {
    const { subscription } = this.props;
    return subscription && subscription?.plan.interval === plan.interval;
  };

  render() {
    const { plans, subscription, isChanging, currentPlanAnnualAmount } = this.props;
    const planList = plans
      .filter((plan) => !this.isSelectedPlan(plan) && plan.upgradable)
      .concat([subscription.plan])
      .sort((plan) => plan.interval)
      .map((plan) => {
        const selected = this.isSelectedPlan(plan);
        const amount = (selected && subscription && subscription.effectivePrice) || plan.amount;
        const defaultPlanAmount = plan.amount;
        return (
          <AvailablePlan
            key={plan.id}
            onClickHandler={() => this.onClickHandler(plan, selected)}
            disabled={isChanging || selected}
            selected={selected}
            currentPlanAnnualAmount={currentPlanAnnualAmount}
            defaultPlanAmount={defaultPlanAmount}
            subscription={{ ...subscription }}
            plan={{ ...plan, amount }}
          />
        );
      });

    return <ul className="availablePlans__container">{planList}</ul>;
  }
}
