import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import deepFreeze from 'deep-freeze';
import AvailablePlan from '../../AvailablePlan/AvailablePlan';
import AvailablePlans from '../AvailablePlans';

describe('Components / Account / <AvailablePlans />', () => {
  let availablePlansWrapper;
  let availablePlanWrappers;
  let selectedPlanWrapper;
  let upgradablePlanWrapper;
  const onSelectHandler = sinon.spy();
  const subscription = {
    planId: 'selected_plan',
    effectivePrice: 1500,
  };
  const selectedPlan = deepFreeze({
    id: 'selected_plan',
    amount: 1000,
  });
  const upgradablePlan = deepFreeze({
    id: 'upgradable_plan',
    amount: 2000,
    upgradable: true,
  });
  const otherPlan = deepFreeze({
    id: 'other_plan',
    amount: 3000,
  });
  const plans = [selectedPlan, upgradablePlan, otherPlan];

  before(() => {
    const props = { plans, subscription, onSelectHandler };
    availablePlansWrapper = shallow(<AvailablePlans {...props} />);
    availablePlanWrappers = availablePlansWrapper.find(AvailablePlan);
    selectedPlanWrapper = availablePlanWrappers.findWhere(
      (wrapper) => wrapper.prop('plan').id === subscription.planId,
    );
    upgradablePlanWrapper = availablePlanWrappers.findWhere(
      (wrapper) => wrapper.prop('plan').upgradable,
    );
  });

  it('should list all upgradable or selected plans', () => {
    expect(availablePlanWrappers).to.have.length(2);
    expect(
      availablePlanWrappers.everyWhere((wrapper) => {
        const plan = wrapper.prop('plan');
        return plan.id === subscription.planId || plan.upgradable;
      }),
    ).to.be.true;
  });

  it('should list plans with subscription effectivePrice if selected', () => {
    expect(selectedPlanWrapper.prop('plan').amount).to.equal(1500);
    expect(upgradablePlanWrapper.prop('plan').amount).to.equal(2000);
  });

  it('should default to amount if subscription effectivePrice is falsey', () => {
    const planWrapperWithoutSubscription = shallow(
      <AvailablePlans
        plans={[selectedPlan]}
        subscription={{ planId: 'selected_plan' }}
        onSelectHandler={onSelectHandler}
      />,
    );
    const planWrapper = planWrapperWithoutSubscription.find(AvailablePlan);
    expect(planWrapper.prop('plan').amount).to.equal(1000);
  });

  it('should handle selecting plans', () => {
    expect(onSelectHandler.called).to.be.false;

    upgradablePlanWrapper.prop('onClickHandler')();

    expect(onSelectHandler.calledOnce).to.be.true;
    expect(onSelectHandler.calledWithExactly('upgradable_plan')).to.be.true;

    selectedPlanWrapper.prop('onClickHandler')();
    expect(onSelectHandler.calledOnce).to.be.true;
  });

  it('should mark plan selected if plan matches subscription', () => {
    expect(upgradablePlanWrapper.prop('selected')).to.be.false;
    expect(selectedPlanWrapper.prop('selected')).to.be.true;
  });
});
