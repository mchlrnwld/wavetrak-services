import React, { Component } from 'react';
import en from '../../../international/translations/en';
import { PreviousSubscription } from 'components';

export default class PreviousSubscriptions extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    const { getBillingInterval } = this.props;
    const { archivedSubscriptions } = this.props.subscription;
    const last10ArchivedSubs = archivedSubscriptions.slice(-10);

    return (
      <div className="prevSubs__container">
        <div className="prevSubs__heading__container">
          <div className="prevSubs__heading">{en.subscription.previousSubscriptions.heading}</div>
        </div>
        <div className="prevSubs__table__container">
          <table className="prevSubs__table">
            <thead>
              <tr>
                <th className="prevSubs__table__heading">{en.subscription.previousSubscriptions.table.heading_plan}</th>
                <th className="prevSubs__table__heading">{en.subscription.previousSubscriptions.table.heading_date}</th>
              </tr>
            </thead>
            <tbody>
              {last10ArchivedSubs.map((prevSub, k) =>
                <PreviousSubscription
                  prevSub={prevSub}
                  interval={getBillingInterval(prevSub)}
                  key={k}
                  index={k} /* eslint id-length: 0 */
                />
              )}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
