import React, { Component } from 'react';
import en from '../../../international/translations/en';

export default class NoPremiumSubscriptions extends Component {
  render() {
    return (
      <div className="no__subscription__card">
        <div className="no__subscription__subheading">
          {en.subscription.no_premium_subs}
        </div>
      </div>
    );
  }
}
