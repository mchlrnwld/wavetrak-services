/* eslint no-nested-ternary: 0 */

import PropTypes from 'prop-types';

import React, { Component } from 'react';
import en from '../../../international/translations/en';
import classnames from 'classnames';
import spinner from '../../../../static/btn-spinner.svg';


export default class EditProfileForm extends Component {
  static propTypes = {
    fields: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
    enableSubmit: PropTypes.func.isRequired,
    editProfileMessage: PropTypes.string.isRequired,
    editProfileSucceeded: PropTypes.bool.isRequired,
    editProfileSubmitting: PropTypes.bool.isRequired
  };

  setErrorClassName(field) {
    return classnames({
      'editProfileForm__error__text': field.touched && field.error,
      'editProfileForm__error__text__hidden': true
    });
  }

  setInputErrorClassName(field) {
    return classnames({
      'editProfileForm__input__hasError': field.touched && field.error,
      'editProfileForm__input__isValid': field.touched && !field.error || this.props.submitSucceeded
    });
  }

  render() {
    const {
      fields: {
 firstName, lastName, email, _id
},
      onSubmit,
      enableSubmit,
      editProfileErrorMessage,
      editProfileSuccessMessage,
      editProfileSucceeded,
      editProfileSubmitting
    } = this.props;

    // setup internationalized text
    const {
      label_first, label_last, label_email /* eslint camelcase: 0 */
    } = en.edit_profile.form;

    const submitText = editProfileSucceeded ?
      en.edit_profile.form.submit_success :
      en.edit_profile.form.save_button;

    const submitStyle = editProfileSucceeded ?
      'editProfileForm__button__submit__success' :
      'editProfileForm__button__submit';

    const errorAlertStyle = editProfileErrorMessage ?
      'editProfileForm__error__alert' :
      'editProfileForm__error__alert__hidden';

    const successAlertStyle = editProfileSuccessMessage ?
      'editProfileForm__success__alert' :
      'editProfileForm__success__alert__hidden';

    return (
      <div>
        <div className={editProfileSuccessMessage ? successAlertStyle : errorAlertStyle}>
          {editProfileSuccessMessage || editProfileErrorMessage}
        </div>
        <form onSubmit={onSubmit} className="editProfileForm">
          <input type="hidden" {..._id} />
          <div>
            <div className="editProfileForm__input__firstName" onClick={enableSubmit}>
              <input id="first" className={this.setInputErrorClassName(firstName)} type="text" required {...firstName} />
              <label className="editProfileForm__label__floating">{label_first}</label>
              <div className={this.setErrorClassName(firstName)}>{firstName.error}</div>
            </div>
          </div>
          <div>
            <div className="editProfileForm__input__lastName" onClick={enableSubmit}>
              <input id="last" className={this.setInputErrorClassName(lastName)} type="text" required {...lastName} />
              <label className="editProfileForm__label__floating">{label_last}</label>
              <div className={this.setErrorClassName(lastName)}>{lastName.error}</div>
            </div>
          </div>
          <div>
            <div className="editProfileForm__input__email" onClick={enableSubmit}>
              <input id="email" className={this.setInputErrorClassName(email)} type="email" required {...email} />
              <label className="editProfileForm__label__floating">{label_email}</label>
              <div className={this.setErrorClassName(email)}>{email.error}</div>
            </div>
          </div>
          <div>
            <div>
              <button disabled={editProfileSubmitting || editProfileSucceeded} className={(editProfileSubmitting ? 'editProfileForm__button__submit__submitting' : submitStyle)}>
                {editProfileSubmitting ? <img src={spinner} className="editProfileForm__button__submit__submitting__img" /> : submitText}
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
