import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

export default class DesktopNavigation extends Component {

  static propTypes = {
    childRoutes: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <ul className="desktopNavigation__link__list">
        {this.props.childRoutes.map((route) => {
          return (
            <li key={route.name} className="desktopNavigation__link__list__item">
              <NavLink
                className="desktopNavigation__link__list__link"
                activeClassName="desktopNavigation__link__list__active"
                to={route.path}>
                {route.name}
              </NavLink>
            </li>
          );
        })}
        </ul>
      </div>
    );
  }
}
