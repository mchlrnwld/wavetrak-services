import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button } from '@surfline/quiver-react';
import { OrBreak } from '../../components';
import en from '../../international/translations/en';
import { loginWithFacebook } from '../../redux/actions/facebook';

class FacebookLogin extends Component {
  submitFacebook = async () => {
    const {
      doLoginWithFacebook,
      successCallback,
      formLocation,
    } = this.props;
    const { type } = await doLoginWithFacebook(formLocation);
    if (successCallback && (type === 'FBLOGIN_SUCCESS')) await successCallback();
  };

  render() {
    const {
      fbSubmitting,
      fbSubmitSuccess,
      fbLoginError,
    } = this.props;
    const submitText = fbSubmitSuccess
      ? en.sign_in.form.submit_success
      : en.facebook.facebook_sign_in;
    const errorAlert = fbLoginError
      ? 'button__facebook__error__alert'
      : 'button__facebook__error__alert__hidden';

    return (
      <div className="facebook-login__content">
        <div className={errorAlert}>{fbLoginError}</div>
        <Button
          onClick={() => this.submitFacebook()}
          loading={fbSubmitting}
          success={fbSubmitSuccess}
        >
          {submitText}
        </Button>
        <OrBreak />
      </div>
    );
  }
}

FacebookLogin.propTypes = {
  fbSubmitting: PropTypes.bool,
  fbSubmitSuccess: PropTypes.bool,
  fbLoginError: PropTypes.string,
  formLocation: PropTypes.string,
};

FacebookLogin.defaultProps = {
  fbSubmitting: false,
  fbSubmitSuccess: false,
  fbLoginError: null,
  formLocation: null,
};

export default connect(
  state => ({
    fbSubmitting: state.facebook.fbSubmitting,
    fbSubmitSuccess: state.facebook.fbSubmitSuccess,
    fbLoginError: state.facebook.fbLoginError,
  }),
  dispatch => ({
    doLoginWithFacebook: location => dispatch(loginWithFacebook(location)),
  })
)(FacebookLogin);
