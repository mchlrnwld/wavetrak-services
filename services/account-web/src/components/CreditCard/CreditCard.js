import React, { Component } from 'react';
import classnames from 'classnames';
import { CreditCardIcon } from '../../components';

class CreditCard extends Component {
  getActiveCardClassName = (cardId, selectedCard) => classnames({
    'credit-card': true,
    'credit-card--active': cardId === selectedCard
  });

  setSelectedCardSource = () => {
    const {
      card: { id },
      doSetSelectedCardSource,
    } = this.props;
    if (doSetSelectedCardSource) doSetSelectedCardSource(id);
  }

  formatExpirationDate = (month, year) => {
    const formattedMonth = month.toString().length === 1 ? `0${month.toString()}` : month;
    const parsedYear = year.toString().substring(2);
    return `${formattedMonth}/${parsedYear}`;
  }

  render() {
    const {
      card: {
        exp_month: exMonth,
        exp_year: exYear,
        expMonth,
        expYear,
        id,
        brand,
        last4,
      },
      tabIndex,
      selectedCard,
    } = this.props;
    const expiresYear = expYear || exYear;
    const expiresMonth = expMonth || exMonth;
    return (
      <div
        onClick={() => this.setSelectedCardSource()}
        onKeyPress={() => this.setSelectedCardSource()}
        className={this.getActiveCardClassName(id, selectedCard)}
        role="radio"
        tabIndex={tabIndex}
        aria-checked={id === selectedCard}
      >
        <div className="credit-card__element credit-card__element--card-brand">
          <CreditCardIcon ccBrand={brand} />
        </div>
        <div className="credit-card__element credit-card__element--card-number">
          **** {last4}
        </div>
        <div className="credit-card__element">
          {this.formatExpirationDate(expiresMonth, expiresYear)}
        </div>
      </div>
    );
  }
}

export default CreditCard;
