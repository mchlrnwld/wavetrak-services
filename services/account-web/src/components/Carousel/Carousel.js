import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Slider from 'react-image-gallery';

export default class Carousel extends Component {
  static propTypes = {
    carouselImages: PropTypes.array,
  };

  render() {
    const { carouselImages } = this.props;
    const autoplay = true;
    const bullets = true;

    return (
      <div>
        <Slider
          ref={ii => this._imageGallery = ii}
          items={carouselImages}
          autoPlay={autoplay}
          showBullets={bullets}
          slideInterval={7000} />
      </div>
    );
  }

}
