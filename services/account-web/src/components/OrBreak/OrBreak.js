import React from 'react';
import en from '../../international/translations/en';

const OrBreak = ({ text = en.or_break.or }) => (
  <div className="or">
    <span className="or__span">{text.toUpperCase()}</span>
  </div>
);

export default OrBreak;
