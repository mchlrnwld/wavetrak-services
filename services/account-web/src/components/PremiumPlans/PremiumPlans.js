import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { ChevronAlt } from '../../components/Icons';
import config from '../../config';

class PremiumPlans extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      plans: props.plans.sort((planA, planB) => {
        if (planA.id < planB.id) return -1;
        if (planA.id > planB.id) return 1;
        return 0;
      }),
    };
  }

  getActiveClassName = (id) => {
    const { selectedPlanId } = this.props;
    const { open } = this.state;
    return classNames({
      'premium-plan': true,
      'premium-plan--hidden': !open && selectedPlanId !== id,
    });
  }

  getPlanName = id => (
    id.includes('annual') ? 'Yearly' : 'Monthly'
  );

  getTrialText = (trialPeriodDays, trialEligible) => {
    const { funnelConfig } = config;
    if (!trialPeriodDays) return null;
    if (!trialEligible) return null;
    if (funnelConfig && !funnelConfig.trialLength) return null;
    const trialLength = (funnelConfig && trialPeriodDays)
      ? funnelConfig.trialLength
      : trialPeriodDays;
    return `${trialLength}-Day Free Trial`;
  };

  getProjectedCost = (id, amount) => (
    id.includes('annual')
      ? parseFloat(amount / 100 / 12).toFixed(2)
      : amount / 100
  );

  getActualCost = (id, amount) => {
    const cost = amount / 100;
    return id.includes('annual')
      ? `/month - Billed $${cost} once a year`
      : '/month - Billed month-to-month';
  };

  toggleSelectedPlan = (id) => {
    const { toggleSelectedPlanId, selectedPlanId } = this.props;
    const { open, plans } = this.state;
    if (open && id !== selectedPlanId) {
      this.setState({ plans: plans.reverse() });
      toggleSelectedPlanId(id);
    }
    this.setState({ open: !open });
  }


  render() {
    const { open } = this.state;
    const { plans, trialEligible } = this.props;
    const sortedPlans = plans.sort((planA, planB) => {
      if (planA.id < planB.id) return -1;
      if (planA.id > planB.id) return 1;
      return 0;
    });
    return (
      <div className="premium-plans__container">
        <div className="premium-plans">
          {sortedPlans.map(({ id, amount, trialPeriodDays }) => (
            <div
              className={this.getActiveClassName(id)}
              onClick={() => this.toggleSelectedPlan(id)}
              onKeyPress={() => this.toggleSelectedPlan(id)}
            >
              <div className="premium-plan__details">
                <div className="premium-plan__name">
                  <div className="premium-plan__name--plan">
                    {`${this.getPlanName(id)} Subscription`}
                  </div>
                  <div className="premium-plan__name--trial">
                    {this.getTrialText(trialPeriodDays, trialEligible)}
                  </div>
                </div>
                <div className="premium-plan__price">
                  <div className="premium-plan__price--cost">
                    ${this.getProjectedCost(id, amount)}
                  </div>
                  <div className="premium-plan__price--total">
                    {this.getActualCost(id, amount)}
                  </div>
                </div>
              </div>
              {!open && plans.length > 1 ? (
                <div className="premium-plan__action">
                  <div className="premium-plan__action--text">Select Plan</div>
                  <div className="premiium-plan__action--icon">
                    <ChevronAlt />
                  </div>
                </div>
              ) : null}
            </div>
          ))}
        </div>
      </div>
    );
  }
}

PremiumPlans.propTypes = {
  plans: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
    trialPeriodDays: PropTypes.number,
    ammount: PropTypes.number,
  })),
  selectedPlanId: PropTypes.string.isRequired,
  toggleSelectedPlanId: PropTypes.func.isRequired,
};

PremiumPlans.defaultProps = {
  plans: null,
};

export default PremiumPlans;
