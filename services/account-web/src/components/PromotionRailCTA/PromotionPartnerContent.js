import PropTypes from 'prop-types';
import React, { Component } from 'react';

/**
 * @description The `PromotionPartnerContent` component is used on the right rail.
 * It renders the Partner logo/text and the promotion specific copy beneath it.
 *
 * @param {object} props - The props for the component
 * @param {object} props.funnelConfig - The funnel configuration for the Promotion.
 *
 * @returns {JSX.Element} PromotionPartnerContent JSX
 */

class PromotionPartnerContent extends Component {
  static propTypes = {
    funnelConfig: PropTypes.instanceOf(Object).isRequired
  };

  /**
   * @description Function for creating the PromotionPartnerContent JSX
   * @param {object} funnelConfig - The funnel configuration for the Promotion.
   */
  renderPartnerContent = (funnelConfig) => {
    const {
      logo, logoAltText, detailCopy, promotionFeatures
    } = funnelConfig;
    const { BrandLogo } = promotionFeatures;
    return (
      <div className="promotion-partner-content">
        <div className="promotion-partner-content__logo">
          <BrandLogo />
        </div>
        <span className="promotion-partner-content__crossmark">X</span>
        {
          logo ?
            <div className="promotion-partner-content__partner_logo" style={{ backgroundImage: `url(${logo})` }} />
            :
            <div className="promotion-partner-content__partner_logo_text">{logoAltText}</div>
        }
        <div className="promotion-partner-content__detail_copy">
          {detailCopy}
        </div>
      </div>
    );
  };

  render() {
    const { funnelConfig } = this.props;
    return this.renderPartnerContent(funnelConfig);
  }
}

export default PromotionPartnerContent;
