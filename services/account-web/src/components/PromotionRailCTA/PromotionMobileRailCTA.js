import PropTypes from 'prop-types';
import React from 'react';
import PromotionPartnerContent from './PromotionPartnerContent';
/**
 * @description The PromotionMobileRailCTA only display on mobile view.
 * This component renders a heading
 * for the mobile page that is placed above the form.
 *
 * @param {object} funnelConfig - The funnel configuration for the Promotion.
 */

const PromotionMobileRailCTA = ({ funnelConfig }) => (
  <div className="mobile-rail-cta" style={funnelConfig.bgMobileImgPath ? { backgroundImage: `url(${funnelConfig.bgMobileImgPath})` } : null}>
    <div className="mobile-rail-cta__content">
      <PromotionPartnerContent funnelConfig={funnelConfig} />
    </div>
  </div>
);

PromotionMobileRailCTA.propTypes = {
  funnelConfig: PropTypes.instanceOf(Object).isRequired,
};

export default PromotionMobileRailCTA;
