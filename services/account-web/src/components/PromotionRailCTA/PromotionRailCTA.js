import PropTypes from 'prop-types';
import React, { Component } from 'react';

import PromotionPartnerContent from './PromotionPartnerContent';
import PromotionValueProps from './PromotionValueProps';

/**
 * @description The `PromotionRailCTA` component is used on the right rail. It renders a
 * background image which covers the block and on
 * top of it renders a Promotion Partner Content block.
 * Beneath this block the Value Propositions for the Promotion is rendered in a separate block.
 * The funnelConfig object is being passed down to the sub components.
 *
 * @param {object} funnelConfig - The funnel configuration for the Promotion.
 *
 * @returns {JSX.Element} PromitionRailCTA JSX
 */
class PromotionRailCTA extends Component {
  static propTypes = {
    funnelConfig: PropTypes.instanceOf(Object).isRequired,
  };

renderCTA = (funnelConfig) => {
  const { bgDesktopImgPath } = funnelConfig;
  return (
    <div className="promotion-rail-cta" style={bgDesktopImgPath ? { backgroundImage: `url(${bgDesktopImgPath})` } : null}>
      <div className="promotion-rail-cta-container">
        <div className="promotion-rail-cta-partner-container">
          <PromotionPartnerContent funnelConfig={funnelConfig} />
        </div>
        <PromotionValueProps funnelConfig={funnelConfig} />
      </div>
    </div>
  );
};

render() {
  const { funnelConfig } = this.props;
  return this.renderCTA(funnelConfig);
}
}

export default PromotionRailCTA;
