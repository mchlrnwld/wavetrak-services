import PropTypes from 'prop-types';
import React from 'react';
import { Chevron } from '../Icons';

/**
 * @description - Component for rendering the mobile scroll to top div at
 * the bottom of the screen
 * @param {object} props
 * @param {string} props.formType One of ["sign_up", "sign_in", "renew"] which
 * is used for determining the copy to render in the button
 * @param {object} props.copy The object containing the copy for the component
 */
const MobileFooter = ({ formType, copy }) => (
  <div className="scroll-to-top">
    <a href={`#${formType}`} className="scroll-to-top__content">
      <Chevron />
      <h6 className="footer-text">{copy[formType]}</h6>
    </a>
  </div>
);

MobileFooter.propTypes = {
  copy: PropTypes.object.isRequired,
  formType: PropTypes.oneOf(['sign_up', 'renew', 'sign_in'])
};

MobileFooter.defaultProps = {
  formType: 'sign_up'
};

export default MobileFooter;
