import React, { Component } from 'react';
import brandToString from '../../utils/brandToString';
import en from '../../international/translations/en';

class UpgradeHeader extends Component {
  getTitleText = () => {
    const {
      trialEligible = true,
    } = this.props;
    if (!trialEligible) return 'It looks like you have already taken a free trial or promotional offer.';
    return `Start your ${brandToString()} Premium subscription today!`;
  }

  render() {
    const { children, hideSubtitle } = this.props;
    const { upgradeFunnel: { upgradeHeaderSubstitle } } = en;
    return (
      <div className="upgrade-header">
        <h1 className="upgrade-header__title">
          {this.getTitleText()}
        </h1>
        {hideSubtitle ? (
          <div>
            <p className="upgrade-header__subtitle">
              {upgradeHeaderSubstitle}
            </p>
            {children}
          </div>
        )
          : null}
      </div>
    );
  }
}

export default UpgradeHeader;
