import React from 'react';
import config from '../../config';
import en from '../../international/translations/en';
import brandToString from '../../utils/brandToString';

const ShowPromotionError = () => (
  <div className="promotion-error__container">
    <p>{en.utils.inactive_promotion(brandToString())}</p>
    <a className="quiver-button" type="button" href={config.upgradeRedirectUrl} >START FREE TRIAL</a>
  </div>
);

export default ShowPromotionError;
