import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { AvailablePlan } from '../../components';
import en from '../../international/translations/en';

export default class SelectBox extends Component {
  static propTypes = {
    plans: PropTypes.array.isRequired,
    onClickHandler: PropTypes.func.isRequired,
    discountObj: PropTypes.shape({}),
    treatments: PropTypes.shape({}),
  };

  constructor(props) {
    super(props);
    this.modifyPlanDescription = this.modifyPlanDescription.bind(this);
  }

  modifyPlanDescription = (interval, amount) => {
    let description;

    switch (interval) {
      case 'month':
        description = en.payment_form.plan_billed_monthly;
        break;
      case 'year':
        description = en.payment_form.plan_billed_yearly(amount);
        break;
      default:
        description = en.payment_form.plan_billed_daily(amount);
    }

    return description;
  }

  render() {
    const {
      plans,
      onClickHandler,
      discountObj,
      selectedPlanId,
      trialEligible,
    } = this.props;
    const planList = plans
      .sort((planA, planB) => planA.id > planB.id)
      .map((plan) => {
        const { id, amount, interval } = plan;
        const description = this.modifyPlanDescription(interval, (amount / 100).toFixed(2));
        return (
          <AvailablePlan
            key={id}
            onClickHandler={() => onClickHandler(id)}
            disabled={selectedPlanId === id}
            selected={selectedPlanId === id}
            theme="alt"
            plan={{ ...plan, amount, description }}
            discountObj={discountObj}
            trialEligible={trialEligible}
          />
        );
      });

    return (
      <ul className="selectBox__container">{planList}</ul>
    );
  }
}
