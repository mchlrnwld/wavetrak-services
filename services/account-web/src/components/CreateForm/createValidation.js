import memoize from 'lru-memoize';
import {createValidator, required, minLength, email} from 'utils/validation';

const createValidation = createValidator({
  email: [required, email],
  password: [required, minLength(6)],
  first: [required],
  last: [required],
});
export default memoize(10)(createValidation);
