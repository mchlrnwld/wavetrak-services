import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { reduxForm } from 'redux-form';
import { Button } from '@surfline/quiver-react';
import { Checkbox, Input, Disclaimer } from '../../components';
import createValidation from './createValidation';
import config from '../../config';
import en from '../../international/translations/en';

class CreateForm extends Component {
  constructor(props) {
    super(props);
    const { geo } = this.props;
    this.state = {
      staySignedInChecked: true,
      receivePromotionsChecked: geo && geo.countryCode === 'US'
    };
  }

  componentWillMount() {
    this.props.initializeForm({
      staySignedIn: true,
      receivePromotions: this.state.receivePromotionsChecked,
      location: this.props.formLocation,
    });
  }

  getInputClassNames = ({ error, touched }, submitSuccess) => {
    let errorClass = 'createForm__error__text__hidden';
    let inputClass = '';

    if (error && touched) {
      if (error === ' ') {
        errorClass = 'createForm__error__no__text';
      }
      errorClass = 'createForm__error__text';
      inputClass = 'createForm__input__hasError';
    } else if (touched) {
      inputClass = 'createForm__input__isValid';
    }

    if (submitSuccess) {
      inputClass = 'createForm__input__isValid';
    }

    return { inputClass, errorClass };
  };

  handleStaySignedInCheckChange = () => {
    this.setState({
      staySignedInChecked: !this.state.staySignedInChecked
    });
  }

  handleReceivePromotionsCheckChange = () => {
    this.setState({
      receivePromotionsChecked: !this.state.receivePromotionsChecked
    });
  }

  render() {
    const {
      fields: { first, last, email, password, staySignedIn, receivePromotions },
      handleSubmit,
      submitting,
      submitSuccess,
      error,
      submitFunc,
    } = this.props;
    const errorAlert = error
      ? 'createForm__error__alert'
      : 'createForm__error__alert__hidden';

    const errorAlertText = error;

    const { errorClass: firstError, inputClass: firstInput } =
      this.getInputClassNames(first, submitSuccess);

    const { errorClass: lastError, inputClass: lastInput } =
      this.getInputClassNames(last, submitSuccess);

    const { errorClass: emailError, inputClass: emailInput } =
      this.getInputClassNames(email, submitSuccess);

    const { inputClass: passwordInput } =
      this.getInputClassNames(password, submitSuccess);

    return (
      <div>
        <div className={errorAlert}>{errorAlertText}</div>
        <form onSubmit={handleSubmit(submitFunc)} className="createForm">
          <input type="hidden" id={location.name} {...location} />
          <Input
            formObject={first}
            inputClassName={firstInput}
            formLocation="createForm"
            label={en.create.form.label_first}
            errorClassName={firstError}
            formName="firstName"
            required
          />
          <Input
            formObject={last}
            inputClassName={lastInput}
            formLocation="createForm"
            label={en.create.form.label_last}
            errorClassName={lastError}
            formName="lastName"
            required
          />
          <Input
            formObject={email}
            inputClassName={emailInput}
            formLocation="createForm"
            type="email"
            label={en.create.form.label_email}
            formName="email"
            required
          >
            <div className={emailError}>
              {email.error}{' '}
              {(email.error === en.create.form.email_error_message) && (
                <Link
                  className="createForm__error__text__link"
                  to="/sign-in"
                >
                  {en.create.form.email_error_message_link}
                </Link>
              )}
            </div>
          </Input>
          <Input
            formObject={password}
            inputClassName={passwordInput}
            formLocation="createForm"
            type="password"
            label={en.create.form.label_password}
            formName="password"
            required
          >
            {password.error && password.touched ? (
              <div className="createForm__error__text">{password.error}</div>
            ) : (
              <div className="createForm__helper__text">
                {en.create.form.password_helper_text}
              </div>
            )}
          </Input>
          <div className="createForm__submit">
            <Button
              loading={submitting}
              success={submitSuccess}
            >
              Create Account
            </Button>
          </div>
          <div className="createForm__checkboxes">
            <Checkbox
              reduxFormProps={staySignedIn}
              formName="staySignedIn"
              isChecked={this.state.staySignedInChecked}
              onClick={this.handleStaySignedInCheckChange}
              copy={en.create.form.staySignedIn_label}
            />
            <Checkbox
              reduxFormProps={receivePromotions}
              formName="receivePromotions"
              isChecked={this.state.receivePromotionsChecked}
              onClick={this.handleReceivePromotionsCheckChange}
              copy={en.create.form.receivePromotions_label(config.brand)}
            />
            <Disclaimer referenceText="Create Account" />
          </div>
        </form>
      </div>
    );
  }
}

CreateForm.propTypes = {
  fields: PropTypes.shape().isRequired,
  geo: PropTypes.shape({
    countryCode: PropTypes.string,
    ip: PropTypes.string,
    location: PropTypes.shape({
      latitude: PropTypes.number,
      longitude: PropTypes.number,
    })
  }).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  error: PropTypes.string,
  submitSuccess: PropTypes.bool.isRequired,
  formLocation: PropTypes.string.isRequired,
  initializeForm: PropTypes.func.isRequired,
  submitFunc: PropTypes.func.isRequired,
  showDiscountMessage: PropTypes.bool,
};

CreateForm.defaultProps = {
  showDiscountMessage: false,
  error: null,
}

export default reduxForm({
  form: 'create',
  fields: [
    'first',
    'last',
    'email',
    'password',
    'staySignedIn',
    'receivePromotions',
    'location',
  ],
  validate: createValidation
})(CreateForm);
