const target = process.env.NODE_ENV || 'development';

const config = {
  bw: require(`./bw/environments/${target}`),
  sl: require(`./sl/environments/${target}`),
  fs: require(`./fs/environments/${target}`),
};

export default (brand) => ({
  redirectUrl: '/',
  siteHome: '/',
  benefitsRedirectUrl:
    'https://www.surfline.com/myaccount/?action=premiumbenefits&slintcid=NAV-BC&slcmpname=NAV-BENEFITSCENTER',
  signInRedirect: '/upgrade',
  selectPlanRedirect: '/upgrade',
  upgradeRedirectUrl: '/upgrade',
  giftRedeemUrl: '/redeem',
  relativeUpgradeRedirectUrl: '/upgrade',
  segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
  needHelpUrl: '/company/contact',
  servicesEndpoint: 'https://services.surfline.com',
  termsConditions: '/terms_conditions.cfm',
  privacyPolicy: '/privacy.cfm',
  surflineHome: 'http://surfline.com/home/index.cfm',
  buoyweatherHome: 'http://www.buoyweather.com',
  fishtrackHome: 'http://www.fishtrack.com',
  accountLinkProtocol: 'https://www.',
  deviceLimitFaqLink:
    'https://support.surfline.com/hc/en-us/articles/360000849611-Why-am-I-seeing-a-device-limit-message-',
  facebook: {
    appId: process.env.FB_APP_ID,
  },
  company: brand,
  ...config[brand],
});
