module.exports = {
  cdnHost: 'https://product-cdn.sandbox.surfline.com/web-account/',
  staticHost: 'https://product-cdn.sandbox.surfline.com/web-account/',
  redirectUrl: 'http://sandbox.fishtrack.com',
  benefitsRedirectUrl: 'http://benefits.fishtrack.com/latest-sst-imagery/',
  giftPurchaseHelpUrl: 'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl: 'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: 'https://sandbox.fishtrack.com/upgrade',
  segment: 'wcxCswdPqynOfwHMDhKE4xCVSLF8knV0',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '16905555'
  },
  servicesEndpoint: 'https://services.sandbox.surfline.com',
  stripe: {
    publishableKey: 'pk_test_Meb7MPzMWgbBYwU3t6gmdHGt'
  },
  termsConditions: 'http://www.fishtrack.com/terms-and-conditions/',
  privacyPolicy: 'http://www.fishtrack.com/privacy-policy/',
  needHelpUrl: 'http://www.fishtrack.com/contact-us/',
  client_id: '5c59e4fb2d11e750809d572e',
  client_secret: 'sk_h63qPbkRrqNLXxEZFbXE',
  backPlaneHost: 'http://web-backplane.sandbox.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
};
