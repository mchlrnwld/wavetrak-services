module.exports = {
  cdnHost: 'https://product-cdn.staging.surfline.com/web-account/',
  staticHost: 'https://product-cdn.staging.surfline.com/web-account/',
  redirectUrl: 'http://staging.fishtrack.com',
  benefitsRedirectUrl: 'http://benefits.fishtrack.com/latest-sst-imagery/',
  giftPurchaseHelpUrl: 'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl: 'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: 'https://staging.fishtrack.com/upgrade',
  accountLinkProtocol: 'https://staging.',
  segment: 'wcxCswdPqynOfwHMDhKE4xCVSLF8knV0',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '17259048'
  },
  servicesEndpoint: 'https://services.staging.surfline.com',
  stripe: {
    publishableKey: 'pk_test_xBhwMLUOtBN1ORmA47xmqNGg'
  },
  termsConditions: 'http://www.fishtrack.com/terms-and-conditions/',
  privacyPolicy: 'http://www.fishtrack.com/privacy-policy/',
  needHelpUrl: 'http://www.fishtrack.com/contact-us/',
  client_id: '5c59e4fb2d11e750809d572e',
  client_secret: 'sk_h63qPbkRrqNLXxEZFbXE',
  backPlaneHost: 'http://web-backplane.staging.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || '3prmuinjgigdvi7m5haujp8jtq1un82rc64n',
};
