module.exports = {
  cdnHost: 'https://wa.cdn-surfline.com/web-account/',
  staticHost: 'https://wa.cdn-surfline.com/web-account/',
  redirectUrl: 'http://www.fishtrack.com',
  benefitsRedirectUrl: 'http://benefits.fishtrack.com/latest-sst-imagery/',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: 'http://www.fishtrack.com/upgrade',
  accountLinkProtocol: 'https://www.',
  segment: 'NcHLO1P5K4Dv1XP2hRXvgXAQMgFLJ3Ch',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '16659845',
  },
  servicesEndpoint: 'https://services.surfline.com',
  stripe: {
    publishableKey:
      'pk_live_518MSMSJ4F5N75cpXxyXhiIIfbYxuWIna19yhOzriQDepMQfxMqDvwuNNhQXbKbOV5icocdsHvbp0gizP6L0wMCjn00dPOhEeeW',
  },
  termsConditions: 'http://www.fishtrack.com/terms-and-conditions/',
  privacyPolicy: 'http://www.fishtrack.com/privacy-policy/',
  needHelpUrl: 'http://www.fishtrack.com/contact-us/',
  client_id: '5c59e80cf0b6cb1ad02baf67',
  client_secret: 'sk_PvRkUPFXC39AWZwhK6Da',
  backPlaneHost: 'http://web-backplane.prod.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'in20v2ebf6q1gh97ldcn1bp8ufdbr404e514',
};
