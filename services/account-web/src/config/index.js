if (__SERVER__) {
  const servicesEndpoint = {
    'development': 'https://services.sandbox.surfline.com',
    'sandbox': 'https://services.sandbox.surfline.com',
    'staging': 'https://services.staging.surfline.com',
    'production': 'https://services.surfline.com',
  }[process.env.NODE_ENV || 'development'];
  module.exports = { servicesEndpoint };
} else {
  module.exports = wt.funnel_config; // eslint-disable-line no-undef
}
