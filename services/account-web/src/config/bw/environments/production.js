module.exports = {
  cdnHost: 'https://wa.cdn-surfline.com/web-account/',
  staticHost: 'https://wa.cdn-surfline.com/web-account/',
  redirectUrl: 'http://buoyweather.com',
  benefitsRedirectUrl: 'http://benefits.buoyweather.com/7-day-forecast/',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: 'https://www.buoyweather.com/upgrade',
  accountLinkProtocol: 'https://www.',
  segment: 'CvbvzychXPuprLT31PUJjn2797aKS8Ez',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '16659845',
  },
  servicesEndpoint: 'https://services.surfline.com',
  stripe: {
    publishableKey:
      'pk_live_518MSMSJ4F5N75cpXxyXhiIIfbYxuWIna19yhOzriQDepMQfxMqDvwuNNhQXbKbOV5icocdsHvbp0gizP6L0wMCjn00dPOhEeeW',
  },
  termsConditions: 'http://www.buoyweather.com/info.jsp?id=21738',
  privacyPolicy: 'http://www.buoyweather.com/info.jsp?id=21737',
  needHelpUrl: 'http://www.buoyweather.com/info.jsp?id=21736',
  client_id: '5c59e826f0b6cb1ad02baf68',
  client_secret: 'sk_4Z4sSmWpPrQsme6nsSuG',
  backPlaneHost: 'http://web-backplane.prod.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'in20v2ebf6q1gh97ldcn1bp8ufdbr404e514',
};
