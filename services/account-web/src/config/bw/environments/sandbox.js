module.exports = {
  cdnHost: 'https://product-cdn.sandbox.surfline.com/web-account/',
  staticHost: 'https://product-cdn.sandbox.surfline.com/web-account/',
  redirectUrl: 'http://sandbox.buoyweather.com',
  benefitsRedirectUrl: 'http://benefits.buoyweather.com/7-day-forecast/',
  giftPurchaseHelpUrl: 'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl: 'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: 'https://sandbox.buoyweather.com/upgrade',
  segment: 'wGHvUUloHAZXW8STk3qUxvKD0NswRkHu',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '16905555'
  },
  servicesEndpoint: 'https://services.sandbox.surfline.com',
  stripe: {
    publishableKey: 'pk_test_Meb7MPzMWgbBYwU3t6gmdHGt'
  },
  termsConditions: 'http://www.buoyweather.com/info.jsp?id=21738',
  privacyPolicy: 'http://www.buoyweather.com/info.jsp?id=21737',
  needHelpUrl: 'http://www.buoyweather.com/info.jsp?id=21736',
  client_id: '5c59e53d2d11e750809d572f',
  client_secret: 'sk_dKW7DWJ4XMxxfP5BWDx8',
  backPlaneHost: 'http://web-backplane.sandbox.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
};
