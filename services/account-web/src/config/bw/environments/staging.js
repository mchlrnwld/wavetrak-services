module.exports = {
  cdnHost: 'https://product-cdn.staging.surfline.com/web-account/',
  staticHost: 'https://product-cdn.staging.surfline.com/web-account/',
  redirectUrl: 'http://staging.buoyweather.com',
  benefitsRedirectUrl: 'http://benefits.buoyweather.com/7-day-forecast/',
  giftPurchaseHelpUrl: 'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl: 'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: 'https://staging.buoyweather.com/upgrade',
  accountLinkProtocol: 'https://staging.',
  segment: 'wGHvUUloHAZXW8STk3qUxvKD0NswRkHu',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '17259048'
  },
  servicesEndpoint: 'https://services.staging.surfline.com',
  stripe: {
    publishableKey: 'pk_test_xBhwMLUOtBN1ORmA47xmqNGg'
  },
  termsConditions: 'http://staging.buoyweather.com/info.jsp?id=21738',
  privacyPolicy: 'http://staging.buoyweather.com/info.jsp?id=21737',
  needHelpUrl: 'http://staging.buoyweather.com/info.jsp?id=21736',
  client_id: '5c59e53d2d11e750809d572f',
  client_secret: 'sk_dKW7DWJ4XMxxfP5BWDx8',
  backPlaneHost: 'http://web-backplane.staging.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || '3prmuinjgigdvi7m5haujp8jtq1un82rc64n',
};
