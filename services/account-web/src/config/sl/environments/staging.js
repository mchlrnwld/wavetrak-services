module.exports = {
  cdnHost: 'https://product-cdn.staging.surfline.com/web-account/',
  staticHost: 'https://product-cdn.staging.surfline.com/web-account/',
  redirectUrl: 'https://staging.surfline.com',
  benefitsRedirectUrl: 'https://staging.surfline.com/premium-benefits',
  giftPurchaseHelpUrl: 'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl: 'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  accountLinkProtocol: 'https://staging.',
  segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '17259048'
  },
  servicesEndpoint: 'https://services.staging.surfline.com',
  stripe: {
    publishableKey: 'pk_test_xBhwMLUOtBN1ORmA47xmqNGg'
  },
  surflineHome: '/',
  client_id: '5bff1583ffda1e1eb8d35c4b',
  client_secret: 'sk_Hmxh7RJEJgJnZeaNF3Kx',
  backPlaneHost: 'http://web-backplane.staging.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || '3prmuinjgigdvi7m5haujp8jtq1un82rc64n',
};
