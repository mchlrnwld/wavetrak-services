module.exports = {
  cdnHost: 'https://wa.cdn-surfline.com/web-account/',
  staticHost: 'https://wa.cdn-surfline.com/web-account/',
  redirectUrl: 'https://www.surfline.com',
  benefitsRedirectUrl: 'https://www.surfline.com/premium-benefits',
  giftPurchaseHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl:
    'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  accountLinkProtocol: 'https://www.',
  segment: 'Se6IGuzcvUhLx55hLHL0RiXkS6oUTz8L',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '16659845',
  },
  servicesEndpoint: 'https://services.surfline.com',
  stripe: {
    publishableKey:
      'pk_live_518MSMSJ4F5N75cpXxyXhiIIfbYxuWIna19yhOzriQDepMQfxMqDvwuNNhQXbKbOV5icocdsHvbp0gizP6L0wMCjn00dPOhEeeW',
  },
  surflineHome: '/',
  client_id: '5c59e7c3f0b6cb1ad02baf66',
  client_secret: 'sk_QqXJdn6Ny5sMRu27Amg3',
  backPlaneHost: 'http://web-backplane.prod.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'in20v2ebf6q1gh97ldcn1bp8ufdbr404e514',
};
