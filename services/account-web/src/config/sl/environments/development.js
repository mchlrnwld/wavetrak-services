module.exports = {
  cdnHost: '/',
  staticHost: '/static/',
  redirectUrl: 'https://sandbox.surfline.com',
  benefitsRedirectUrl: 'https://sandbox.surfline.com/premium-benefits',
  giftPurchaseHelpUrl: 'https://support.surfline.com/hc/en-us/articles/360019773771-How-can-I-purchase-a-Surfline-Gift-Card-',
  giftRedeemHelpUrl: 'https://support.surfline.com/hc/en-us/articles/360019501212-How-can-I-redeem-a-Surfline-Gift-Card-',
  upgradeRedirectUrl: '/upgrade',
  accountLinkProtocol: 'https://sandbox.',
  segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
  newRelic: {
    licenseKey: 'ac93b47204',
    applicationID: '17227951',
  },
  servicesEndpoint: 'https://services.sandbox.surfline.com',
  stripe: {
    publishableKey: 'pk_test_Meb7MPzMWgbBYwU3t6gmdHGt'
  },
  surflineHome: '/',
  client_id: '5bff1583ffda1e1eb8d35c4b',
  client_secret: 'sk_Hmxh7RJEJgJnZeaNF3Kx',
  backPlaneHost: 'http://web-backplane.sandbox.surfline.com',
  splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
  facebook: {
    appId: '564041023804928'
  },
};
