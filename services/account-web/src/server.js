/* eslint no-unused-vars: 0 */
import Express from 'express';
import path from 'path';
import session from 'express-session';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import http from 'http';
import compression from 'compression';
import superagent from 'superagent';

import configServer from './configServer';
import Account from './routes/Account/account.server';
import getUserByToken from './server/utils/getUserByToken';
import ensureLatestBrowser from './server/utils/ensureLatestBrowser';
import { authorise } from './common/api/auth';

const app = new Express();
const server = new http.Server(app);
const BRAND = process.env.BRAND || 'sl';

const authorizeToken = async (req, res, next) => {
  const accessToken = req.cookies.access_token;
  if (!accessToken) return next();
  try {
    await authorise(accessToken);
    return next();
  } catch (error) {
    // for now, fishtrack uses .fishtrack.com as cookie domain
    const domainName = req.brand === 'fs' ? '.fishtrack.com' : `.${req.hostname.trim()}`;
    res.clearCookie('USERINFO', { path: '/', domain: domainName });
    res.clearCookie('USER_ID', { path: '/', domain: domainName });
    res.clearCookie('access_token', { path: '/', domain: domainName });
    res.clearCookie('refresh_token', { path: '/', domain: domainName });

    // delete cookie from the request
    delete req.cookies.access_token;
    return next();
  }
};

app.use(compression());
app.enable('trust proxy');

app.get('/Health', (req, res) => res.send('OK'));

app.use('/static', Express.static(path.join(__dirname, '..', 'static')));
app.use(
  `/${BRAND}/dist/`,
  Express.static(path.join(__dirname, '..', '..', 'build', BRAND, 'dist')),
);

app.use(
  session({
    secret: configServer.session_secret,
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 60000 },
  }),
);

app.use(bodyParser.json());
app.use(cookieParser());

app.use((req, res, next) => {
  const brandHeader = req.headers['x-wt-brand'];
  let brand = 'sl';

  switch (brandHeader) {
    case 'bw':
      brand = 'bw';
      break;
    case 'fs':
      brand = 'fs';
      break;
    case 'sl':
      brand = 'sl';
      break;
    default:
      brand = 'sl';
      break;
  }
  req.brand = brand;
  return next();
});

app.get('/subs/geoTargetResource', (req, res) => {
  const ipAddress = req.ip;
  if (req.query.key) {
    const geoLookup =
      `${configServer.geoTargetEndpoint}/assets/resource?` +
      `key=${req.query.key}&ipAddress=${ipAddress}`;
    superagent
      .get(geoLookup)
      .redirects(0)
      .end((err, result) => {
        if (result.status > 404) {
          return res.status(500).send('Internal Server Error');
        }
        if (result.status === 302) {
          const redirectTo = result.header.location;
          if (redirectTo) {
            return res.redirect(302, redirectTo);
          }
          return res.status(404).send('Not Found');
        }
        return res.status(404).send('Not Found');
      });
  }
  return res.status(404).send('Not Found');
});

app.use('/subs/update', (req, res) => {
  res.sendFile(path.join(`${__dirname}/browser/update.html`));
});

app.use('/subs/browser-images', Express.static(`${__dirname}/browser/images`));

app.all('*', ensureLatestBrowser);

app.use('/account', getUserByToken, Account);

if (configServer.listenPort) {
  server.listen(configServer.listenPort, (err) => {
    if (err) {
      console.error(err);
    }
    console.info(
      '==> 💻  Open http://%s:%s in a browser to view the app.',
      configServer.host,
      configServer.listenPort,
    );
  });
} else {
  console.error('==>     ERROR: No PORT environment variable has been specified');
}
