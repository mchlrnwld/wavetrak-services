const environment = {
  development: {
    isProduction: false,
    geoTargetEndpoint: 'https://services.staging.surfline.com/geo-target',
    proxiedHost: 'https://staging.surfline.com',
  },
  production: {
    isProduction: true,
    geoTargetEndpoint: 'https://services.surfline.com/geo-target',
    proxiedHost: 'https://www.surfline.com',
  },
  staging: {
    isProduction: true,
    geoTargetEndpoint: 'https://services.staging.surfline.com/geo-target',
    proxiedHost: 'https://staging.surfline.com',
  },
  qa: {
    isProduction: true,
    geoTargetEndpoint: 'http://services.staging.surfline.com/geo-target',
    proxiedHost: 'https://staging.surfline.com',
  },
  sandbox: {
    isProduction: true,
    geoTargetEndpoint: 'https://services.sandbox.surfline.com/geo-target',
    proxiedHost: 'https://sandbox.surfline.com',
  },
}[process.env.NODE_ENV || 'development'];

module.exports = {
  host: process.env.WEB_HOST || 'localhost',
  exposePort: parseInt(process.env.WEB_EXPOSE_PORT, 10) || 8100,
  listenPort: parseInt(process.env.WEB_LISTEN_PORT, 10) || 8100,
  session_secret: process.env.SESSION_SECRET || 'j2UT1qrz0PdTt2Nn0L0n5dhbsG110iH4',
  app: {
    title: 'Surfline/Wavetrak Account',
    description: 'Unified account funnel for BW/FS/SL',
    head: {
      titleTemplate: 'Surfline: %s',
    },
  },
  ...environment,
};
