# Feed Microservice

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Setup](#setup)
- [Development](#development)
- [Endpoint Authentication](#endpoint-authentication)
  - [Sample Authentication Header](#sample-authentication-header)
- [Endpoint Versioning](#endpoint-versioning)
  - [Version Changelog](#version-changelog)
    - [Version: 1 (Status: Production)](#version-1-status-production)
- [Article Response Object](#article-response-object)
- [Personalized](#personalized)
  - [Request Query Parameters](#request-query-parameters)
  - [Sample Request](#sample-request)
  - [Sample Response](#sample-response)
- [Regional](#regional)
  - [Request Query Parameters](#request-query-parameters-1)
      - [Associated Object](#associated-object)
  - [Sample Request](#sample-request-1)
    - [Without filter](#without-filter)
  - [Sample Response](#sample-response-1)
    - [With Filter](#with-filter)
  - [Sample Response](#sample-response-2)
- [Promobox](#promobox)
  - [Request Query Parameters](#request-query-parameters-2)
  - [Sample Request](#sample-request-2)
  - [Sample Response](#sample-response-3)
- [Curated](#curated)
  - [Request Query Parameters](#request-query-parameters-3)
      - [Associated Object](#associated-object-1)
  - [Sample Request](#sample-request-3)
    - [Without Filter](#without-filter)
  - [Sample Response](#sample-response-4)
    - [With Filter](#with-filter-1)
  - [Sample Response](#sample-response-5)
- [All Active Storm Feed Events](#all-active-storm-feed-events)
  - [Sample Response](#sample-response-6)
- [Storm Feed Event by ID](#storm-feed-event-by-id)
  - [Sample Response](#sample-response-7)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Setup

Use Node v6.9.2

```sh
cp .env.sample .env
npm install
```

## Development

```sh
npm run startlocal
```

## Endpoint Authentication

Feed endpoints are partially authenticated. An premium forecast content body is only returned to premium authenticated users.

### Sample Authentication Header

| Key           | Value                    | Description      |
| ------------- | ------------------------ | ---------------- |
| X-Auth-UserId | 582495992d06cd3b71d66229 | User's Mongo ID. |

## Endpoint Versioning

Endpoints can be versioned by passing an API version specifier in the `Accept`
header. If no version is passed the latest is assumed.

```
Accept: application/vnd.surfline.feed+json; version=1
```

As a best practice consumers of this API should always explicitly indicate
what version they're requesting.

### Version Changelog

#### Version: 1 (Status: Production)

* Initial Implementation


## Article Response Object

Applies to: all api versions

Each of the endpoints returns arrays of feed articles with the following schema:

| Property         | Type   | Description                                                                                                                                                        | Nullable |
| ---------------- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------- |
| id               | string | Wordpress or Contentful ID.                                                                                                                                        | No       |
| contentType      | string | Article source. Possible values: `FORECAST` or `EDITORIAL`.                                                                                                        | No       |
| createdAt        | int    | Unix timestamp of when the article was created.                                                                                                                    | No       |
| updatedAt        | int    | Unix timestamp of when the article was updated.                                                                                                                    | No       |
| premium          | bool   | Premium content is only available to premium users. Non-premium users can see the `title` and `subtitle`.                                                          | No       |
| newWindow          | bool   |  (Editorial type only) open articles in a new window? | No       |
| externalSource        | string   |  (Editorial type only) external link to open when clicked | Yes       |
| externalLink        | string   |  (Editorial type only) whether this is an external link | Yes       |
| content.title    | string | Article title.                                                                                                                                                     | No       |
| content.subtitle | string | Article subtitle. May be `null` if `FORECAST` type.                                                                                                                | Yes      |
| content.body     | string | Article body in HTML. May be `null` if `EDITORIAL` type or if article is `premium` and user is not.                                                                | Yes      |
| permalink        | string | URL to full article. Will be `null` if `FORECAST` type.                                                                                                            | Yes      |
| media            | object | Media content (i.e. image or video). Will be `null` if `FORECAST` type.                                                                                            | Yes      |
| media.type       | string | Media content type. Possible values: `image` or `video`.                                                                                                           | No       |
| media.feed1x     | string | Media content URL at 1x resolution intended to be displayed in the feed for lower resolution displays.                                                             | No       |
| media.feed2x     | string | Media content URL at 2x resolution intended to be displayed in the feed for higher resolution displays.                                                            | No       |
| media.promobox1x | string | Media content URL at 1x resolution intended to be displayed in the promobox for lower resolution displays. Will be `null` unless article is promoted to promobox.  | No       |
| media.promobox2x | string | Media content URL at 2x resolution intended to be displayed in the promobox for higher resolution displays. Will be `null` unless article is promoted to promobox. | No       |
| author           | object | Article author. Will be `null` if `FORECAST` type.                                                                                                                 | Yes      |
| author.iconUrl   | string | Article author's icon image URL.                                                                                                                                   | No       |
| author.name      | string | Article author's full name.                                                                                                                                        | No       |
| nextPageStart    | string | Start position parameter for the next page if you want the next page to begin after this article.                                                                  | No       |
| tags             | array  | Array of tag objects. May be empty.                                                                                                                                | No       |
| tags[i].name     | string | Tag name.                                                                                                                                                          | No       |
| tags[i].url      | string | Tag URL.                                                                                                                                                           | No       |

## Personalized

Applies to: all api versions

Personalized feed endpoint returns articles targeted at user and promoted editor's pick articles. Editorial and forecast articles can both be returned as either targeted or promoted. An authenticated user can receive personalized content targeted toward the user.

### Request Query Parameters

| Parameters    | Type   | Description                                                                                                                                                             | Example                                | Required | Default |
| ------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- | -------- | ------- |
| targetedStart | string | Comma separated sort position to start targeted search results after. Contains milliseconds from 01/01/1970 and article ID. `associated.nextTargetedStart` can be used. | 1491088282685,12CCDDpSuBQDe86u4GmssPPA | No       | null    |
| limit         | int    | Number of articles to return for targeted. Restricted to maximum of 50.                                                                                                 | 8                                      | No       | 50      |
| subregionIds | string | Comma separated list of subregion Ids to filter articles | 58581a836630e24c44879044,58581a836630e24c44878fd6 | No | null
| targetedOnly | bool | Determines whether to return default/curated and promoted data vs only targeted | true | No | false

### Sample Request

```
HTTP/1.1 GET /personalized?promotedStart=1491174682685,12CCDDpSuBQDe86u4GmssPPE&targetedStart=1491088282685,12CCDDpSuBQDe86u4GmssPPA&limit=10
X-Auth-UserId: 582495992d06cd3b71d66229
```

### Sample Response

```JSON
{
  "associated": {
    "nextTargetedStart": "1489194883454,3NvXWZELLcG8YAwgS",
    "feedType": "TARGETED"
  },
  "data": {
    "promoted": [
      {
        "id": "2NvDL0XJXXcG8YAwgS",
        "contentType": "Editorial",
        "createdAt": 1489194883,
        "updatedAt": 1489194883,
        "premium": false,
        "promoted": [],
        "externalSource": null,
        "externalLink": null,
        "newWindow": false,
        "content": {
          "title": "Winter is Coming!",
          "subtitle": "So say we all.",
          "body": null
        },
        "permalink": "http://www.surfline.com/templates/article.cfm?sef=true&id=145781",
        "media": {
          "type": "image",
          "url": "http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg"
        },
        "author": {
          "iconUrl": "http://gravatar.com/avatar/ea1e9a0c570c61d61dec3cf6ea26a85e?d=mm",
          "name": "Shaler Perry"
        },
        "tags": [
          {
            "name": "Waves",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454,2NvDL0XJXXcG8YAwgS"
      },
      {
        "id": "",
        "contentType": "Forecast",
        "createdAt": 1489194883,
        "updatedAt": 1489194883,
        "premium": false,
        "content": {
          "title": "Late Season XL Swell on Track for Hawaii",
          "subtitle": null,
          "body": "An area of low pressure that has been meandering northwest of the islands over the past few days is reenergizing this Monday evening and will set up a solid NW swell for the middle to second half of the week.<br />There are several factors that will help this storm create extra large surf for the islands:1) it's close, with the strongest wind roughly 1500 miles away, 2) it's fairly strong with satellite confirmed wind of 35-40 knots and 3) the storm/fetch will move toward the islands for a period of roughly 24-36 hours."
        },
        "permalink": null,
        "media": null,
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm",
          "name": "Kevin Willis"
        },
        "tags": [
          {
            "name": "Storm Feed",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      },
      {
        "id": "",
        "contentType": "Editorial",
        "createdAt": 1489194883,
        "updatedAt": 1489194883,
        "premium": false,
        "promoted": [],
        "externalSource": null,
        "externalLink": null,
        "newWindow": false,
        "content": {
          "title": "Exclusive Interview: Chris Bertish on Achieving the Impossible",
          "subtitle": "Waterman's 93-day, transatlantic SUP journey more than just a personal milestone",
          "body": null
        },
        "permalink": "http://www.surfline.com/templates/article.cfm?sef=true&id=145975",
        "media": {
          "type": "image",
          "url": "http://i.cdn-surfline.com/surfnews/images/2017/03_mar/bertish-640/full/01-17265940_379106969138051_3659242523362066432_n.jpg"
        },
        "author": {
          "iconUrl": "http://gravatar.com/avatar/732a7c19e81048b6e1f7d9d654c450c7?d=mm",
          "name": "Chris Borg"
        },
        "tags": [
          {
            "name": "Surf News",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      },
      ...
      {
        "id": "",
        "contentType": "Editorial",
        "createdAt": 1489194883,
        "updatedAt": 1489194883,
        "premium": false,
        "promoted": [],
        "externalSource": null,
        "externalLink": null,
        "newWindow": false,        
        "content": {
          "title": "Surfers Take on the Sharks",
          "subtitle": "Brothers behind Rareform recycled bag company featured on ABC's \"Shark Tank\"",
          "body": null
        },
        "permalink": "http://www.surfline.com/templates/article.cfm?sef=true&id=145852",
        "media": {
          "type": "image",
          "url": "http://i.cdn-surfline.com/surfnews/images/2017/03_mar/shark-tank/full/Rareform-1.jpg"
        },
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm",
          "name": "Kevin Wallis"
        },
        "tags": [
          {
            "name": "Sharks",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      }
    ],
    "targeted": [
      {
        "id": "3NvXWZELLcG8YAwgS",
        "contentType": "Forecast",
        "createdAt": 1489194883,
        "updatedAt": 1489194883,
        "premium": false,
        "content": {
          "title": "Spring Is in the Air - Will There Be Waves?",
          "subtitle": null,
          "body": "<p>The leaves are turning and the rains have started for the Pacific Northwest, it&#39;s a tinge cooler in Southern California, and Hawaii is staring down the most substantial swell of the early NPAC season to-date. Look at satellite imagery for the North Pacific today, and the seasonal flux is here. A large, complex low-pressure system is centered over the Aleutians, and another system sliding off northern Japan is slated to rapidly intensify. Further, as we head through the end of the week and weekend, storm activity looks to stretch from the Bearing Sea eastward through the Alaskan Gulf. Stay tuned for more as we breakdown swell implications for Hawaii, the Pacific NW, North/Central and Southern California.</p><p><img src=\"https://images.contentful.com/kl4m5s78bekv/6dH9KaoJDagsYqeE6Ku6CI/495c24f0371f8e7c8d9a26099b725bc2/Screen_Shot_2016-10-05_at_2.45.43_PM.png\" alt=\"NPAC Surface Analysis\"> <em>Image: NOAA OPC</em></p><p><img src=\"https://images.contentful.com/kl4m5s78bekv/1oQ9b9CU2IcAg6Mmmu2qK4/3cf351e12d35f29898b86f96dc797512/rgb-animated.gif\" alt=\"NPAC Satellite\"> <em>Image: NOAA SSD</em></p>"
        },
        "permalink": null,
        "media": null,
        "author": null,
        "tags": [],
        "nextPageStart": "1489194883454,3NvXWZELLcG8YAwgS"
      },
      {
        "id": "",
        "contentType": "Forecast",
        "createdAt": 1489194883,
        "updatedAt": 1489194883,
        "premium": true,
        "content": {
          "title": "Hurricane Force Winter Storm Stella Developing off US East Coast",
          "subtitle": null,
          "body": null
        },
        "permalink": null,
        "media": null,
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm",
          "name": "Kevin Wallis"
        },
        "tags": [
          {
            "name": "Storm Feed",
            "url": "/"
          },
          {
            "name": "Atlantic",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      },
      {
        "id": "",
        "contentType": "Forecast",
        "createdAt": 1489194883,
        "updatedAt": 1489194883,
        "premium": false,
        "content": {
          "title": "Wave of the Day - Brent Dorrington",
          "body": "Watch Brent Dorrington air drop into this double-barrel Snapper Rocks drainer during a recent pulsing swell on the Gold Coast.  That shoulder hopper did not even see him coming."
        },
        "permalink": null,
        "media": null,
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm",
          "name": "Kevin Wallis"
        },
        "tags": [
          {
            "name": "Australia",
            "url": "/"
          },
          {
            "name": "Snapper Rocks",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      },
      ...
      {
        "id": "",
        "contentType": "Editorial",
        "createdAt": 1489194883,
        "updatedAt": 1489194883,
        "premium": false,
        "promoted": [],
        "externalSource": null,
        "externalLink": null,
        "newWindow": false,
        "content": {
          "title": "Is Kelly Slater Building a Left at the Surf Ranch?",
          "subtitle": "Following Kelly Slater on social media is like following a reality TV show. It's jam-packed with cliffhangers, juicy drama, and what-will-he-do-next? entertainment.",
          "body": null
        },
        "permalink": "http://www.surfline.com/templates/article.cfm?sef=true&id=145760",
        "media": {
          "type": "image",
          "url": "http://i.cdn-surfline.com/surfnews/images/2017/03_mar/slater-left/full/01_P007_201605_KSWC_GLASER_Day4_RAW_0398.jpg"
        },
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7ef72bd7b4a1f950432200762aa71b2d?d=mm",
          "name": "John Warren"
        },
        "tags": [
          {
            "name": "Social",
            "url": "/"
          },
          {
            "name": "Surf News",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      }
    ]
  }
}
```

## Regional

Applies to: all api versions

Regional feed endpoint returns editorial and forecast articles targeted at the regional forecast page.

### Request Query Parameters

| Parameters  | Type   | Description                                                                                                                                                     | Example                                | Required | Default |
| ----------- | ------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- | -------- | ------- |
| subregionId | string | Subregion ID to receive articles for.                                                                                                                           | 58581a836630e24c44878fd6               | Yes      |         |
| start       | string | Comma separated sort position to start promoted search results after. Contains milliseconds from 01/01/1970 and article ID. `associated.nextStart` can be used. | 1491174682685,12CCDDpSuBQDe86u4GmssPPE | No       | null    |
| limit       | int    | Number of articles to return for each of promoted and targeted. Restricted to maximum of 50.                                                                    | 8                                      | No       | 50      |
| filter      | string |  Filter name for filtering the results to articles specified in Contentful.  Possible options are realtimeforecast,shorttermforecast,longtermforecast,seasonalforecast | shorttermforecast                                      | No       | null      |
| includeLocalNews      | bool |  Bool determining whether to include "local news" in query  | true       | No      | false


##### Associated Object

```
{
  "associated": {
    "nextStart": "1489194883454",
    "keywords": [ "shorttermforecast", "longtermforecast" ],
}
```
* nextStart - Used for paginating feed-service requests
* keywords - Indicates what keywords exist for the articles that are part of the query result set (not just the ones returned for each page, but all that match the overall query parameters of the search being performed)


### Sample Request

#### Without filter
```
HTTP/1.1 GET /regional?subregionId=58581a836630e24c44878fd6&start=1491174682685,12CCDDpSuBQDe86u4GmssPPE&limit=10
X-Auth-UserId: 582495992d06cd3b71d66229
```

### Sample Response

```JSON
{
  "associated": {
    "nextStart": "1489194883454",
    "keywords": [ "shorttermforecast", "longtermforecast" ],
},
  "data": {
    "regional": [
      {
        "id": "",
        "contentType": "Forecast",
        "createdAt": 1489194883,
        "updatedAt": 1489194883,
        "premium": false,
        "content": {
          "title": "Late Season XL Swell on Track for Hawaii",
          "subtitle": null,
          "body": "An area of low pressure that has been meandering northwest of the islands over the past few days is reenergizing this Monday evening and will set up a solid NW swell for the middle to second half of the week.<br />There are several factors that will help this storm create extra large surf for the islands:1) it's close, with the strongest wind roughly 1500 miles away, 2) it's fairly strong with satellite confirmed wind of 35-40 knots and 3) the storm/fetch will move toward the islands for a period of roughly 24-36 hours."
        },
        "permalink": null,
        "media": null,
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm",
          "name": "Kevin Willis"
        },
        "tags": [
          {
            "name": "Storm Feed",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      },
      {
        "id": "",
        "contentType": "Forecast",
        "createdAt": 1489194883,
        "keywords": [],
        "updatedAt": 1489194883,
        "premium": true,
        "content": {
          "title": "Hurricane Force Winter Storm Stella Developing off US East Coast",
          "subtitle": null,
          "body": null
        },
        "permalink": null,
        "media": null,
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm",
          "name": "Kevin Wallis"
        },
        "tags": [
          {
            "name": "Storm Feed",
            "url": "/"
          },
          {
            "name": "Atlantic",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      },
      ...
      {
        "id": "",
        "contentType": "Editorial",
        "createdAt": 1489194883,
        "keywords": [],
        "updatedAt": 1489194883,
        "premium": false,
        "promoted": [],
        "externalSource": null,
        "externalLink": null,
        "newWindow": false,
        "content": {
          "title": "Wave of the Day - Brent Dorrington",
          "body": "Watch Brent Dorrington air drop into this double-barrel Snapper Rocks drainer during a recent pulsing swell on the Gold Coast.  That shoulder hopper did not even see him coming."
        },
        "permalink": null,
        "media": null,
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm",
          "name": "Kevin Wallis"
        },
        "tags": [
          {
            "name": "Australia",
            "url": "/"
          },
          {
            "name": "Snapper Rocks",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      }
    ]
  }
}
```

#### With Filter
```
HTTP/1.1 GET /regional?subregionId=58581a836630e24c44878fd6&start=1491174682685,12CCDDpSuBQDe86u4GmssPPE&limit=10&filter=shorttermforecast
X-Auth-UserId: 582495992d06cd3b71d66229
```

### Sample Response

```JSON
{
  "associated": {
    "nextStart": "1489194883454",
    "keywords": ["shorttermforecast", "longtermforecast" ]
  },
  "data": {
    "regional": [
      {
        "id": "",
        "contentType": "Forecast",
        "createdAt": 1489194883,
        "keywords": [ "shorttermforecast" ],
        "updatedAt": 1489194883,
        "premium": false,
        "content": {
          "title": "Late Season XL Swell on Track for Hawaii",
          "subtitle": null,
          "body": "An area of low pressure that has been meandering northwest of the islands over the past few days is reenergizing this Monday evening and will set up a solid NW swell for the middle to second half of the week.<br />There are several factors that will help this storm create extra large surf for the islands:1) it's close, with the strongest wind roughly 1500 miles away, 2) it's fairly strong with satellite confirmed wind of 35-40 knots and 3) the storm/fetch will move toward the islands for a period of roughly 24-36 hours."
        },
        "permalink": null,
        "media": null,
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm",
          "name": "Kevin Willis"
        },
        "tags": [
          {
            "name": "Storm Feed",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      },
      {
        "id": "",
        "contentType": "Forecast",
        "createdAt": 1489194883,
        "keywords": [ "shorttermforecast" ],
        "updatedAt": 1489194883,
        "premium": true,
        "content": {
          "title": "Hurricane Force Winter Storm Stella Developing off US East Coast",
          "subtitle": null,
          "body": null
        },
        "permalink": null,
        "media": null,
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm",
          "name": "Kevin Wallis"
        },
        "tags": [
          {
            "name": "Storm Feed",
            "url": "/"
          },
          {
            "name": "Atlantic",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      },
      ...
      {
        "id": "",
        "contentType": "Editorial",
        "createdAt": 1489194883,
        "keywords": [ "shorttermforecast" ],
        "updatedAt": 1489194883,
        "premium": false,
        "promoted": [],
        "externalSource": null,
        "externalLink": null,
        "newWindow": false,
        "content": {
          "title": "Wave of the Day - Brent Dorrington",
          "body": "Watch Brent Dorrington air drop into this double-barrel Snapper Rocks drainer during a recent pulsing swell on the Gold Coast.  That shoulder hopper did not even see him coming."
        },
        "permalink": null,
        "media": null,
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm",
          "name": "Kevin Wallis"
        },
        "tags": [
          {
            "name": "Australia",
            "url": "/"
          },
          {
            "name": "Snapper Rocks",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      }
    ]
  }
}
```

## Promobox

Applies to: all api versions

Promobox feed endpoint returns editorial and forecast articles promoted to promobox.

### Request Query Parameters

| Parameters | Type | Description                                                                                  | Example | Required | Default |
| ---------- | ---- | -------------------------------------------------------------------------------------------- | ------- | -------- | ------- |
| limit      | int  | Number of articles to return for each of promoted and targeted. Restricted to maximum of 50. | 8       | No       | 50      |

### Sample Request

```
HTTP/1.1 GET /promobox?limit=10
X-Auth-UserId: 582495992d06cd3b71d66229
```

### Sample Response

```JSON
{
  "data": {
    "promobox": [
      {
        "id": "",
        "contentType": "Forecast",
        "createdAt": 1489194883,
        "updatedAt": 1489194883,
        "premium": false,
        "content": {
          "title": "Late Season XL Swell on Track for Hawaii",
          "subtitle": null,
          "body": "An area of low pressure that has been meandering northwest of the islands over the past few days is reenergizing this Monday evening and will set up a solid NW swell for the middle to second half of the week.<br />There are several factors that will help this storm create extra large surf for the islands:1) it's close, with the strongest wind roughly 1500 miles away, 2) it's fairly strong with satellite confirmed wind of 35-40 knots and 3) the storm/fetch will move toward the islands for a period of roughly 24-36 hours."
        },
        "permalink": null,
        "media": null,
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm",
          "name": "Kevin Willis"
        },
        "tags": [
          {
            "name": "Storm Feed",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      },
      {
        "id": "",
        "contentType": "Forecast",
        "createdAt": 1489194883,
        "updatedAt": 1489194883,
        "premium": true,
        "content": {
          "title": "Hurricane Force Winter Storm Stella Developing off US East Coast",
          "subtitle": null,
          "body": null
        },
        "permalink": null,
        "media": null,
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm",
          "name": "Kevin Wallis"
        },
        "tags": [
          {
            "name": "Storm Feed",
            "url": "/"
          },
          {
            "name": "Atlantic",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      },
      ...
      {
        "id": "",
        "contentType": "Editorial",
        "createdAt": 1489194883,
        "updatedAt": 1489194883,
        "premium": false,
        "promoted": [],
        "externalSource": null,
        "externalLink": null,
        "newWindow": false,
        "content": {
          "title": "Wave of the Day - Brent Dorrington",
          "body": "Watch Brent Dorrington air drop into this double-barrel Snapper Rocks drainer during a recent pulsing swell on the Gold Coast.  That shoulder hopper did not even see him coming."
        },
        "permalink": null,
        "media": null,
        "author": {
          "iconUrl": "http://gravatar.com/avatar/7b56791847d5971c1dce71f35fba259d?d=mm",
          "name": "Kevin Wallis"
        },
        "tags": [
          {
            "name": "Australia",
            "url": "/"
          },
          {
            "name": "Snapper Rocks",
            "url": "/"
          }
        ],
        "nextPageStart": "1489194883454"
      }
    ]
  }
}
```

## Curated

Applies to: all api versions

Curated feed endpoint returns curated editorial articles, feed large promoted articles, and feed small promoted articles.

### Request Query Parameters

| Parameters  | Type   | Description                                                                                                                                                     | Example                                | Required | Default |
| ----------- | ------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- | -------- | ------- |
| feedLargeStart       | string | Comma separated sort position to start promoted feed large search results after. Contains milliseconds from 01/01/1970 and article ID. `associated.nextFeedLargeStart` can be used. | 1491174682685,12CCDDpSuBQDe86u4GmssPPE | No       | null    |
| feedSmallStart       | string | Comma separated sort position to start promoted feed small search results after. Contains milliseconds from 01/01/1970 and article ID. `associated.nextFeedSmallStart` can be used. | 1491174682685,12CCDDpSuBQDe86u4GmssPPE | No       | null    |
| curatedStart       | string | Comma separated sort position to start curated article search results after. Contains milliseconds from 01/01/1970 and article ID. `associated.curatedStart` can be used. | 1491174682685,12CCDDpSuBQDe86u4GmssPPE | No       | null    |
| feedLargeLimit       | int | Max number of feed large promoted articles to return. | 6 | No       | 3    |
| feedSmallLimit       | int | Max number of feed small promoted articles to return. | 4 | No       | 2    |
| curatedLimit       | int | Max number of curated articles to return. | 24 | No       | 12    |
| tag       | string | Article tag to filter on. | 'Travel' | No       | null    |


##### Associated Object

```
{
  "associated": {
    "nextFeedLargeStart": "1540226485000,1634",
    "nextFeedSmallStart": "1541023836000,1648",
    "nextCuratedStart": "1505315561000,1292"
}
```
* nextFeedLargeStart - Used for paginating feed-service requests for promoted feed large articles
* nextFeedSmallStart - Used for paginating feed-service requests for promoted feed small articles
* nextCuratedStart - Used for paginating feed-service requests for curated articles that are not promoted as feed large or feed small

### Sample Request

#### Without Filter
```
HTTP/1.1 GET /curated
X-Auth-UserId: 582495992d06cd3b71d66229
```

### Sample Response

```JSON
{
  "associated": {
      "nextFeedLargeStart": "1540226485000,1634",
      "nextFeedSmallStart": "1541023836000,1648",
      "nextHomepageCuratedStart": "1505315561000,1292"
  },
  "data": {
    "feedLarge": [
      {
        "id": "1646",
        "contentType": "EDITORIAL",
        "createdAt": 1541023514,
        "updatedAt": 1541023514,
        "premium": false,
        "promoted": [
            "FEED_LARGE"
        ],
        "externalSource": null,
        "externalLink": "Wordpress URL",
        "newWindow": false,
        "content": {
            "title": "Large Promotion Test",
            "subtitle": "Testing Homepage Large",
            "body": "<p>Test</p>\n"
        },
        "permalink": "https://sandbox.surfline.com/surf-news/large-promotion-test/1646",
        "media": {
            "promobox1x": "",
            "promobox2x": "",
            "feed2x": "",
            "feed1x": "",
            "type": "image"
        },
        "author": {
            "name": "Surfline Development",
            "iconUrl": "https://secure.gravatar.com/avatar/5958b357ef71b0033018b5d816049d81?s=96&d=mm&r=g"
        },
        "tags": [
            {
                "name": "Surf News",
                "url": "https://sandbox.surfline.com/category/surf-news"
            }
        ],
        "keywords": [],
        "menuOrder": 1,
        "nextPageStart": "1541023514000,1646"
      },
      ...
    ],
    "feedSmall": [
      {
        "id": "1648",
        "contentType": "EDITORIAL",
        "createdAt": 1541023836,
        "updatedAt": 1541024267,
        "premium": false,
        "promoted": [
            "FEED_SMALL"
        ],
        "externalSource": null,
        "externalLink": "Wordpress URL",
        "newWindow": false,
        "content": {
            "title": "Test Homepage Small",
            "subtitle": "testing small promotion",
            "body": null
        },
        "permalink": "https://sandbox.surfline.com/surf-news/test-homepage-small/1648",
        "media": {
            "promobox1x": "",
            "promobox2x": "",
            "feed2x": "",
            "feed1x": "",
            "type": "image"
        },
        "author": {
            "name": "Surfline Development",
            "iconUrl": "https://secure.gravatar.com/avatar/5958b357ef71b0033018b5d816049d81?s=96&d=mm&r=g"
        },
        "tags": [
            {
                "name": "Surf News",
                "url": "https://sandbox.surfline.com/category/surf-news"
            }
        ],
        "keywords": [],
        "menuOrder": 1,
        "nextPageStart": "1541023836000,1648"
      },
      ...
    ],
    "curated": [
      {
        "id": "1648",
        "contentType": "EDITORIAL",
        "createdAt": 1541023836,
        "updatedAt": 1541024267,
        "premium": false,
        "promoted": [
            "CURATED"
        ],
        "externalSource": null,
        "externalLink": "Wordpress URL",
        "newWindow": false,
        "content": {
            "title": "Test Curated",
            "subtitle": "testing curated promotion",
            "body": null
        },
        "permalink": "https://sandbox.surfline.com/surf-news/test-curated/1649",
        "media": {
            "promobox1x": "",
            "promobox2x": "",
            "feed2x": "",
            "feed1x": "",
            "type": "image"
        },
        "author": {
            "name": "Surfline Development",
            "iconUrl": "https://secure.gravatar.com/avatar/5958b357ef71b0033018b5d816049d81?s=96&d=mm&r=g"
        },
        "tags": [
            {
                "name": "Surf News",
                "url": "https://sandbox.surfline.com/category/surf-news"
            }
        ],
        "keywords": [],
        "menuOrder": 1,
        "nextPageStart": "1541023836000,1649"
      },
      ...
    ]
  }
}
```

#### With Filter
```
HTTP/1.1 GET /curated?feedLargeStart=1,1540226485000,1628&feedSmallStart=1,1540226485000,1629&curatedDefautlStart=1,1540226485000,1636
X-Auth-UserId: 582495992d06cd3b71d66229
```

### Sample Response

```JSON
{
  "associated": {
      "nextFeedLargeStart": "1540226485000,1640",
      "nextFeedSmallStart": "1541023836000,1650",
      "nextCuratedStart": "1489615890000,743"
  },
  "data": {
    "feedLarge": [
      {
        "id": "1646",
        "contentType": "EDITORIAL",
        "createdAt": 1541023514,
        "updatedAt": 1541023514,
        "premium": false,
        "promoted": [
            "FEED_LARGE"
        ],
        "externalSource": null,
        "externalLink": "Wordpress URL",
        "newWindow": false,
        "content": {
            "title": "Large Promotion Test",
            "subtitle": "Testing Homepage Large",
            "body": "<p>Test</p>\n"
        },
        "permalink": "https://sandbox.surfline.com/surf-news/large-promotion-test/1646",
        "media": {
            "promobox1x": "",
            "promobox2x": "",
            "feed2x": "",
            "feed1x": "",
            "type": "image"
        },
        "author": {
            "name": "Surfline Development",
            "iconUrl": "https://secure.gravatar.com/avatar/5958b357ef71b0033018b5d816049d81?s=96&d=mm&r=g"
        },
        "tags": [
            {
                "name": "Surf News",
                "url": "https://sandbox.surfline.com/category/surf-news"
            }
        ],
        "keywords": [],
        "menuOrder": 1,
        "nextPageStart": "1541023514000,1646"
      },
      ...
    ],
    "feedSmall": [
      {
        "id": "1648",
        "contentType": "EDITORIAL",
        "createdAt": 1541023836,
        "updatedAt": 1541024267,
        "premium": false,
        "promoted": [
            "FEED_SMALL"
        ],
        "externalSource": null,
        "externalLink": "Wordpress URL",
        "newWindow": false,
        "content": {
            "title": "Test Homepage Small",
            "subtitle": "testing small promotion",
            "body": null
        },
        "permalink": "https://sandbox.surfline.com/surf-news/test-homepage-small/1648",
        "media": {
            "promobox1x": "",
            "promobox2x": "",
            "feed2x": "",
            "feed1x": "",
            "type": "image"
        },
        "author": {
            "name": "Surfline Development",
            "iconUrl": "https://secure.gravatar.com/avatar/5958b357ef71b0033018b5d816049d81?s=96&d=mm&r=g"
        },
        "tags": [
            {
                "name": "Surf News",
                "url": "https://sandbox.surfline.com/category/surf-news"
            }
        ],
        "keywords": [],
        "menuOrder": 1,
        "nextPageStart": "1541023836000,1648"
      },
      ...
    ],
    "curated": [
      {
        "id": "1238",
        "contentType": "EDITORIAL",
        "createdAt": 1541023836,
        "updatedAt": 1541024267,
        "premium": false,
        "promoted": [
            "CURATED"
        ],
        "externalSource": null,
        "externalLink": "Wordpress URL",
        "newWindow": false,
        "content": {
            "title": "Test Curated",
            "subtitle": "testing curated promotion",
            "body": null
        },
        "permalink": "https://sandbox.surfline.com/surf-news/test-curated/1649",
        "media": {
            "promobox1x": "",
            "promobox2x": "",
            "feed2x": "",
            "feed1x": "",
            "type": "image"
        },
        "author": {
            "name": "Surfline Development",
            "iconUrl": "https://secure.gravatar.com/avatar/5958b357ef71b0033018b5d816049d81?s=96&d=mm&r=g"
        },
        "tags": [
            {
                "name": "Surf News",
                "url": "https://sandbox.surfline.com/category/surf-news"
            }
        ],
        "keywords": [],
        "menuOrder": null,
        "nextPageStart": "1541023836000,1649"
      },
      ...
    ]
  }
}
```


## All Active Storm Feed Events
```
HTTP/1.1 GET /feed/events
```

### Sample Response

```JSON
{
  "events": [
    {
      "id": 2647,
      "permalink": "https://sandbox.surfline.com/surf-news/entries/hurricane-lacey",
      "createdAt": "2019-05-09 16:45:50",
      "updatedAt": "2019-05-09 16:45:50",
      "name": "Hurricane Lacey",
      "summary": "Made up subtitle woohoo!",
      "author": {
          "name": "Surfline Development",
          "iconUrl": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/13123139/mick_container-150x150.jpg"
      },
      "thumbnailUrl": "",
      "basins": [
          "south_atlantic",
          "north_pacific"
      ],
      "dfpKeyword": "dfpkeyword",
      "jwPlayerVideoId": "",
      "videoCaption": "",
      "taxonomies": [
          {
              "id": "58f7ed5fdadb30820bb3987c",
              "href": "https://sandbox.surfline.com/travel/united-states/california/orange-county/huntington-beach-surfing-and-beaches/5358705",
              "name": "Huntington Beach",
              "type": "travel",
              "breadCrumbs": [
                  "United States",
                  "California",
                  "Orange County"
              ]
          },
          {
              "id": "58f7ed5fdadb30820bb3987c",
              "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/orange-county/huntington-beach/5358705",
              "name": "Huntington Beach",
              "type": "geoname",
              "breadCrumbs": [
                  "United States",
                  "California",
                  "Orange County"
              ]
          }
      ],
      "active": true,
    },
    {
      "id": 2635,
      "permalink": "https://sandbox.surfline.com/surf-news/entries/hurricane-herman",
      "createdAt": "2019-05-06 21:04:09",
      "updatedAt": "2019-05-06 21:04:09",
      "name": "Hurricane Herman",
      "summary": "Big storm brewin!",
      "author": {
          "name": "Surfline Development",
          "iconUrl": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/13123139/mick_container-150x150.jpg"
      },
      "thumbnailUrl": "",
      "basins": "north_atlantic",
      "dfpKeyword": "test",
      "jwPlayerVideoId": null,
      "videoCaption": null,
      "taxonomies": [
          {
              "id": "58f7eef9dadb30820bb56276",
              "href": "https://sandbox.surfline.com/travel/new-zealand-surfing-and-beaches/2186224",
              "name": "New Zealand",
              "type": "travel",
              "breadCrumbs": []
          },
          {
              "id": "58f7eef9dadb30820bb56276",
              "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/new-zealand/2186224",
              "name": "New Zealand",
              "type": "geoname",
              "breadCrumbs": []
          },
          {
              "id": "59a88cecdadb30820b474134",
              "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/french-polynesia/vairao/tahiti/4033649",
              "name": "Tahiti",
              "type": "geoname",
              "breadCrumbs": [
                  "French Polynesia",
                  "Vairao"
              ]
          },
          {
              "id": "59a88cecdadb30820b474134",
              "href": "https://sandbox.surfline.com/travel/french-polynesia/vairao/tahiti-surfing-and-beaches/4033649",
              "name": "Tahiti",
              "type": "travel",
              "breadCrumbs": [
                  "French Polynesia",
                  "Vairao"
              ]
          },
          {
              "id": "58f7f03ddadb30820bb6ceba",
              "href": "https://sandbox.surfline.com/surf-forecasts/tahiti-south/58581a836630e24c44878ffc",
              "name": "Tahiti South",
              "type": "subregion",
              "breadCrumbs": null
          }
      ],
      "active": true,
    }
  ]
}
```


## Storm Feed Event by ID
```
HTTP/1.1 GET /feed/events/:id
```

### Sample Response

```JSON
{
  "event": {
    "id": 2635,
    "permalink": "https://sandbox.surfline.com/surf-news/entries/hurricane-herman",
    "createdAt": "2019-05-06 21:04:09",
    "updatedAt": "2019-05-06 21:04:09",
    "name": "Hurricane Herman",
    "summary": "Big storm brewin!",
    "author": {
        "name": "Surfline Development",
        "iconUrl": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/13123139/mick_container-150x150.jpg"
    },
    "thumbnailUrl": "",
    "basins": "north_atlantic",
    "dfpKeyword": "test",
    "jwPlayerVideoId": null,
    "videoCaption": null,
    "taxonomies": [
        {
            "id": "58f7eef9dadb30820bb56276",
            "href": "https://sandbox.surfline.com/travel/new-zealand-surfing-and-beaches/2186224",
            "name": "New Zealand",
            "type": "travel",
            "breadCrumbs": []
        },
        {
            "id": "58f7eef9dadb30820bb56276",
            "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/new-zealand/2186224",
            "name": "New Zealand",
            "type": "geoname",
            "breadCrumbs": []
        },
        {
            "id": "59a88cecdadb30820b474134",
            "href": "https://sandbox.surfline.com/surf-reports-forecasts-cams/french-polynesia/vairao/tahiti/4033649",
            "name": "Tahiti",
            "type": "geoname",
            "breadCrumbs": [
                "French Polynesia",
                "Vairao"
            ]
        },
        {
            "id": "59a88cecdadb30820b474134",
            "href": "https://sandbox.surfline.com/travel/french-polynesia/vairao/tahiti-surfing-and-beaches/4033649",
            "name": "Tahiti",
            "type": "travel",
            "breadCrumbs": [
                "French Polynesia",
                "Vairao"
            ]
        },
        {
            "id": "58f7f03ddadb30820bb6ceba",
            "href": "https://sandbox.surfline.com/surf-forecasts/tahiti-south/58581a836630e24c44878ffc",
            "name": "Tahiti South",
            "type": "subregion",
            "breadCrumbs": null
        }
    ],
    "updates": [
        {
            "id": 2633,
            "premiumContent": false,
            "createdAt": "2019-05-06 20:49:58",
            "updatedAt": "2019-05-06 23:35:03",
            "title": "New Premium Article",
            "content": "<p><a href=\"https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2019/05/06143117/Vissla_Surfline_Mexico__D4_5498.jpg\"><img class=\"alignnone size-thumbnail wp-image-2636\" src=\"https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2019/05/06143117/Vissla_Surfline_Mexico__D4_5498-e1557178321334-150x136.jpg\" alt=\"\" width=\"150\" height=\"136\" /></a></p>\n<p><strong>There are two types of morning sickness</strong>",
            "author": {
                "name": "Surfline Development",
                "iconUrl": "https://d1542niisbwnmf.cloudfront.net/wp-content/uploads/2017/03/13123139/mick_container-150x150.jpg"
            }
        }
    ],
    "active": true,
  }
}
```

## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `feed-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbncwTVRJM05EVTJNdyJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53ME1USTNORFUyTXciLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests on the endpoints detailed in one of the [Sample Requests](#all-active-storm-feed-events) sections. 

**Ex:**

Test `GET` requests against the `/feed/events` endpoint on the `feed` service:

```
curl \
    -X GET \
    -i \
    http://feed-api.prod.surfline.com/feed/curated
```
**Expected response:** See [above](#sample-response-4) for sample response.
