import 'newrelic';
import startApp from './server';
import logger from './common/logger';
import { initElasticsearch } from './model/dbContext';

const log = logger('feed-api:server');

Promise.all([initElasticsearch()])
  .then(startApp(log))
  .catch(err => {
    // eslint-disable-next-line no-console
    console.log(err);
    log.error({
      message: "App Initialization failed. Can't connect to database",
      stack: err,
    });
  });
