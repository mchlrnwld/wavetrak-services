import fetch from 'node-fetch';
import config from '../config';

const getFavoritesForUser = async (userId) => {
  const result = await fetch(`${config.FAVORITES_API}/?type=spots`, {
    headers: { 'x-auth-userid': userId },
  });
  const response = await result.json();

  if (result.status === 200) return response;
  throw response;
};

export { getFavoritesForUser }; // eslint-disable-line import/prefer-default-export
