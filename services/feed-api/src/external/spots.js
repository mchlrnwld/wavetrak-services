import fetch from 'node-fetch';
import config from '../config';

const getTaxonomyIds = async (items) => {
  const result = await fetch(`${config.SPOTS_API}/taxonomy/ids`, {
    method: 'POST',
    body: JSON.stringify(items),
    headers: { 'Content-Type': 'application/json' },
  });

  const response = await result.json();
  if (result.status === 200) return response;
  throw response;
};

const getTaxonomy = async (type, id) => {
  const result = await fetch(`${config.SPOTS_API}/taxonomy?type=${type}&id=${id}`);

  const response = await result.json();
  if (result.status === 200) return response;
  throw response;
};

export { getTaxonomyIds, getTaxonomy };
