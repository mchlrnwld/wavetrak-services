import fetch from 'node-fetch';
import config from '../config';

const getTaxonomiesByIDs = async (taxonomyIDs) => {
  const taxonomyString = taxonomyIDs.join(',');
  const result = await fetch(`${config.SEARCH_API}/taxonomy?ids=${taxonomyString}`);
  const response = await result.json();

  if (result.status === 200) return response;
  throw response;
};

export { getTaxonomiesByIDs }; // eslint-disable-line

