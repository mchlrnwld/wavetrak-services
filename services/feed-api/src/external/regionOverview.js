import fetch from 'node-fetch';
import config from '../config';

const getRegionalOverview = async (subregionId) => {
  const result = await fetch(`${config.FORECASTS_API}/admin/forecasts?subregionId=${subregionId}`);

  const response = await result.json();
  if (result.status === 200) return response;
  throw response;
};

export default getRegionalOverview;
