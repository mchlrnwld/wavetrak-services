import fetch from 'node-fetch';
import config from '../config';

const getEntitlements = async (userId) => {
  const url = `${config.ENTITLEMENTS_SERVICE}/entitlements?objectId=${userId}`;

  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  if (response.status === 400) return { entitlements: [] };
  throw body;
};


export const isPremium = async (userId, apiKey = null) => {
  /* temporarily entitle CW requests */
  if (apiKey && apiKey === config.CW_CLIENT_ID) return true;
  if (!userId) return false;
  const entitlementsResponse = await getEntitlements(userId);
  return entitlementsResponse.entitlements.includes('sl_premium');
};

export default getEntitlements;
