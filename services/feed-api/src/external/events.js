import fetch from 'node-fetch';
import config from '../config';
// eslint-disable-next-line import/no-cycle
import { getLocalFeedArticle } from '../model/feed';

const getWPEvents = async () => {
  const result = await fetch(`${config.WWW_HOST}/wp-json/sl/v1/events`);
  const response = await result.json();

  if (result.status === 200) return response;
  throw response;
};

const getWPEvent = async id => {
  const result = await fetch(`${config.WWW_HOST}/wp-json/sl/v1/events/${id}`);
  const response = await result.json();

  if (result.status === 200) return response;
  throw response;
};

const getWPPost = async (id, premium) => {
  const response = await getLocalFeedArticle(id, premium);
  return response;
};

export { getWPEvents, getWPEvent, getWPPost };
