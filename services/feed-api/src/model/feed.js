import elasticsearch from './elasticsearch';
// eslint-disable-next-line import/no-cycle
import { buildBody } from '../server/feed/helpers';

const keywordFilter = filter => (
  filter ? {
    terms: {
      keywords: filter.split(','),
    },
  }
  : undefined
);

const parseFeedSearchHit = ({ _type, _source, sort }, premium) => ({
  id: _source.articleId.toString(),
  contentType: _type.toUpperCase(),
  createdAt: Math.floor(new Date(_source.createdAt) / 1000),
  updatedAt: Math.floor(new Date(_source.updatedAt) / 1000),
  premium: _source.premium,
  promoted: _source.promoted,
  externalSource: _source.externalSource || null,
  externalLink: _source.externalLink || null,
  newWindow: _source.newWindow || false,
  content: {
    title: _source.content.title,
    displayTitle: _source.content.displayTitle || null,
    subtitle: _source.content.subtitle || null,
    body: buildBody(_source.content.teaser, _source.content.body, _source.premium, premium),
  },
  auContent: {
    title: _source.auContent?.title || null,
    displayTitle: _source.auContent?.displayTitle || null,
    subtitle: _source.auContent?.subtitle || null,
  },
  permalink: _source.permalink || null,
  media: _source.media || null,
  author: _source.author || null,
  tags: _source.tags || [],
  keywords: _source.keywords || [],
  menuOrder: _source.menuOrder || null,
  auMenuOrder: _source.auMenuOrder || null,
  nzMenuOrder: _source.nzMenuOrder || null,
  feedSuperheader: _source.feedSuperheader || null,
  sponsoredArticle: {
    attributionText: _source.sponsoredArticle ? _source.sponsoredArticle.attributionText : null,
    dfpCode: _source.sponsoredArticle ? _source.sponsoredArticle.dfpCode : null,
    showAttributionInFeed: _source.sponsoredArticle ?
      _source.sponsoredArticle.showAttributionInFeed : false,
    sponsorName: _source.sponsoredArticle ?
      _source.sponsoredArticle.sponsorName : null,
  },
  nextPageStart: sort ? sort.join(',') : null,
});

const parseFeedSearchHits = ({ hits }, premium) =>
  hits.map(hit => parseFeedSearchHit(hit, premium));

const search = async (searchPayload, premium) => {
  const client = await elasticsearch.getClient();
  const response = await client.search({
    index: 'feed',
    body: {
      _source: [
        'articleId',
        'createdAt',
        'updatedAt',
        'premium',
        'promoted',
        'externalSource',
        'externalLink',
        'newWindow',
        'auContent.title',
        'auContent.displayTitle',
        'auContent.subtitle',
        'content.title',
        'content.displayTitle',
        'content.subtitle',
        'content.body',
        'content.teaser',
        'permalink',
        'media.type',
        'media.feed1x',
        'media.feed2x',
        'media.promobox1x',
        'media.promobox2x',
        'author.iconUrl',
        'author.name',
        'tags',
        'keywords',
        'feedSuperheader',
        'sponsoredArticle.attributionText',
        'sponsoredArticle.dfpCode',
        'sponsoredArticle.showAttributionInFeed',
        'sponsoredArticle.sponsorName',
        'menuOrder',
        'auMenuOrder',
        'nzMenuOrder',
      ],
      ...searchPayload,
    },
  });
  return parseFeedSearchHits(response.hits, premium);
};

const aggregate = async (searchPayload, type = undefined) => {
  const client = await elasticsearch.getClient();
  const response = await client.search({
    index: 'feed',
    type,
    body: {
      ...searchPayload,
    },
  });
  return response.aggregations ?
    response.aggregations.counts.buckets.map(match => match.key)
    : [];
};

const searchTargetedFeedArticles = (
  targetTaxonomy,
  size,
  searchAfter,
  premium,
  filterRange = null,
) => {
  if (!targetTaxonomy) return [];
  const query = {
    bool: {
      filter: [
        {
          terms: {
            targetTaxonomy,
          },
        },
      ],
      must_not: {
        terms: {
          promoted: ['EDITORS_PICK', 'PROMO_BOX'],
        },
      },
    },
  };
  if (filterRange) {
    query.bool.filter.push({
      range: {
        sortDate: {
          gte: 'now-7d/d',
          lte: 'now',
        },
      },
    });
  }

  return search({
    size,
    search_after: searchAfter || undefined,
    query,
    sort: [
      { sortDate: 'desc' },
      { articleId: 'desc' },
    ],
  }, premium);
};

const searchTrendingFeedArticles = (size, premium) => search({
  size,
  query: {
    bool: {
      filter: {
        term: {
          promoted: 'TRENDING',
        },
      },
    },
  },
  sort: [
    { menuOrder: 'asc' },
    { sortDate: 'desc' },
    { articleId: 'desc' },
  ],
}, premium);

const searchEditorsPickFeedArticles = (size, premium) => search({
  size,
  query: {
    bool: {
      filter: {
        term: {
          promoted: 'EDITORS_PICK',
        },
      },
    },
  },
  sort: [
    { menuOrder: 'asc' },
    { sortDate: 'desc' },
    { articleId: 'desc' },
  ],
}, premium);

const searchRegionalFeedArticles =
  (targetTaxonomy, filter, size, searchAfter, premium) => search({
    size,
    search_after: searchAfter || undefined,
    query: {
      bool: {
        should: [
          {
            range: { sortDate: { gte: 'now-7d/d', lte: 'now' } },
          },
          {
            terms: { keywords: ['seasonalforecast'] },
          },
        ],
        filter: [
          {
            terms: {
              targetTaxonomy,
            },
          },
          { ...keywordFilter(filter) },
          {
            term: {
              promoted: 'REGIONAL',
            },
          },
          {
            range: {
              sortDate: {
                gte: 'now-10d/d',
                lte: 'now',
              },
            },
          },
        ],
      },
    },
    sort: [
      { sortDate: 'desc' },
      { articleId: 'desc' },
    ],
  }, premium);

const searchForecastFeedArticles =
  (targetTaxonomy, filter, size, searchAfter, premium) => search({
    size,
    search_after: searchAfter || undefined,
    query: {
      bool: {
        filter: [
          {
            terms: {
              targetTaxonomy,
            },
          },
          {
            range: {
              sortDate: {
                gte: 'now-7d/d',
                lte: 'now',
              },
            },
          },
        ],
        must_not: {
          term: {
            showInFeed: false,
          },
        },
      },
    },
    sort: [
      { sortDate: 'desc' },
      { articleId: 'desc' },
    ],
  }, premium);

const aggregateRegionalFeedKeywords =
  targetTaxonomy => aggregate({
    size: 0,
    aggs: {
      counts: {
        terms: {
          field: 'keywords',
        },
      },
    },
    query: {
      bool: {
        should: [
          {
            range: { sortDate: { gte: 'now-7d/d', lte: 'now' } },
          },
          {
            terms: { keywords: ['seasonalforecast'] },
          },
        ],
        filter: [
          {
            terms: {
              targetTaxonomy,
            },
          },
          {
            term: {
              promoted: 'REGIONAL',
            },
          },
          {
            range: {
              sortDate: {
                gte: 'now-10d/d',
                lte: 'now',
              },
            },
          },
        ],
      },
    },
  }, 'forecast');

const searchPromoboxFeedArticles = (size, premium, promoted = 'PROMO_BOX') => {
  const promotedMenuOrder = {
    AU_PROMO_BOX: 'auMenuOrder',
    NZ_PROMO_BOX: 'nzMenuOrder',
    PROMO_BOX: 'menuOrder',
  };
const query = {
  bool: {
    filter: [
      {
        bool: {
          must: {
            exists: {
                field: promotedMenuOrder[promoted],
            },
          },
        },
      },
      {
        term: {
          promoted,
        },
      },
    ],
  },
};

  const sortOrder = promotedMenuOrder[promoted];

  return search({
    size,
    query,
    sort: [
      { [sortOrder]: 'asc' },
      { sortDate: 'desc' },
      { articleId: 'desc' },
    ],
  }, premium);
};

const searchPromotedFeedArticles = (
  size,
  searchAfter,
  premium,
  promoted,
  tagFilter,
  promotionsToFilterOut = [],
) => {
  const query = {
    bool: {
      filter: {
        term: {
          promoted,
        },
      },
    },
  };
  if (tagFilter) {
    query.bool.must = {
      nested: {
        path: 'tags',
        query: {
          bool: {
            must: {
              match: { 'tags.name': tagFilter },
            },
          },
        },
      },
    };
  }
  if (promotionsToFilterOut && promotionsToFilterOut.length) {
    query.bool.must_not = {
      terms: {
        promoted: promotionsToFilterOut,
      },
    };
  }

  return search({
    size,
    search_after: searchAfter || undefined,
    query,
    sort: [
      { sortDate: 'desc' },
      { articleId: 'desc' },
    ],
  }, premium);
};

const getLocalFeedArticle = async (articleId, premium, type = 'forecast') => {
  const client = await elasticsearch.getClient();
  const response = await client.get({
    id: articleId,
    index: 'feed',
    type,
  });
  return parseFeedSearchHit(response, premium);
};

export {
  parseFeedSearchHits,
  getLocalFeedArticle,
  searchTrendingFeedArticles,
  searchEditorsPickFeedArticles,
  searchTargetedFeedArticles,
  searchRegionalFeedArticles,
  searchPromoboxFeedArticles,
  searchForecastFeedArticles,
  searchPromotedFeedArticles,
  aggregateRegionalFeedKeywords,
};
