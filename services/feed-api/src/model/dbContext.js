import elasticsearch from './elasticsearch';

// eslint-disable-next-line import/prefer-default-export
export const initElasticsearch = async () => {
  const client = await elasticsearch.getClient();
  await client.ping(); // Check if Elastic Search connection returns 200
  return client;
};
