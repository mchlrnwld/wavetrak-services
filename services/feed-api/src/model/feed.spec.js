import { expect } from 'chai';
import sinon from 'sinon';
import elasticsearch from './elasticsearch';
import feedArticleFixture from './fixtures/feedArticle.json';
import parsedFeedArticleFixture from './fixtures/parsedFeedArticle.json';
import feedArticlesFixture from './fixtures/feedArticles.json';
import {
  parseFeedSearchHits,
  searchTrendingFeedArticles,
  searchEditorsPickFeedArticles,
  searchTargetedFeedArticles,
  searchRegionalFeedArticles,
  searchPromoboxFeedArticles,
  searchPromotedFeedArticles,
  searchForecastFeedArticles,
  getLocalFeedArticle,
} from './feed';

describe('model / feed', () => {
  const client = {
    get: sinon.stub(),
    search: sinon.stub(),
  };

  const sourceFilterFixture = [
    'articleId',
    'createdAt',
    'updatedAt',
    'premium',
    'promoted',
    'externalSource',
    'externalLink',
    'newWindow',
    'auContent.title',
    'auContent.displayTitle',
    'auContent.subtitle',
    'content.title',
    'content.displayTitle',
    'content.subtitle',
    'content.body',
    'content.teaser',
    'permalink',
    'media.type',
    'media.feed1x',
    'media.feed2x',
    'media.promobox1x',
    'media.promobox2x',
    'author.iconUrl',
    'author.name',
    'tags',
    'keywords',
    'feedSuperheader',
    'sponsoredArticle.attributionText',
    'sponsoredArticle.dfpCode',
    'sponsoredArticle.showAttributionInFeed',
    'sponsoredArticle.sponsorName',
    'menuOrder',
    'auMenuOrder',
    'nzMenuOrder',
  ];

  beforeEach(() => {
    sinon.stub(elasticsearch, 'getClient');
    elasticsearch.getClient.resolves(client);
    client.get.resolves(feedArticleFixture);
    client.search.resolves(feedArticlesFixture);
  });

  afterEach(() => {
    client.get.reset();
    client.search.reset();
    elasticsearch.getClient.restore();
  });

  describe('parseFeedSearchHits', () => {
    it('maps feed search results', () => {
      expect(parseFeedSearchHits({
        hits: [
          {
            _type: 'Editorial',
            _source: {
              articleId: '2NvDL0XJXXcG8YAwgS',
              createdAt: new Date(1489194883454),
              updatedAt: new Date(1489194883454),
              premium: false,
              promoted: [],
              externalSource: null,
              externalLink: null,
              menuOrder: null,
              auMenuOrder: null,
              nzMenuOrder: null,
              newWindow: false,
              content: {
                title: 'Winter is Coming!',
                subtitle: 'So say we all.',
                displayTitle: 'Winter Coming',
                body: 'Test body',
              },
              auContent: {
                title: null,
                subtitle: null,
                displayTitle: null,
              },
              permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145781',
              media: {
                type: 'image',
                url: 'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
              },
              author: {
                iconUrl: 'http://gravatar.com/avatar/ea1e9a0c570c61d61dec3cf6ea26a85e?d=mm',
                name: 'Shaler Perry',
              },
              tags: [
                {
                  name: 'Waves',
                  url: '/',
                },
              ],
              feedSuperheader: null,
              sponsoredArticle: {
                attributionText: null,
                dfpCode: null,
                showAttributionInFeed: false,
                sponsorName: null,
              },
            },
            sort: [1489194883454, '2NvDL0XJXXcG8YAwgS'],
          },
        ],
      }, false)).to.deep.equal([
        {
          id: '2NvDL0XJXXcG8YAwgS',
          contentType: 'EDITORIAL',
          createdAt: 1489194883,
          updatedAt: 1489194883,
          premium: false,
          promoted: [],
          externalSource: null,
          externalLink: null,
          menuOrder: null,
          auMenuOrder: null,
          nzMenuOrder: null,
          newWindow: false,
          content: {
            title: 'Winter is Coming!',
            subtitle: 'So say we all.',
            displayTitle: 'Winter Coming',
            body: 'Test body',
          },
          auContent: {
            title: null,
            subtitle: null,
            displayTitle: null,
          },
          permalink: 'http://www.surfline.com/templates/article.cfm?sef=true&id=145781',
          media: {
            type: 'image',
            url: 'http://i.cdn-surfline.com/forecasters/2017/03_MAR/PACoutlook_spring/creek_pacoutlook.jpg',
          },
          author: {
            iconUrl: 'http://gravatar.com/avatar/ea1e9a0c570c61d61dec3cf6ea26a85e?d=mm',
            name: 'Shaler Perry',
          },
          tags: [
            {
              name: 'Waves',
              url: '/',
            },
          ],
          feedSuperheader: null,
          sponsoredArticle: {
            attributionText: null,
            dfpCode: null,
            showAttributionInFeed: false,
            sponsorName: null,
          },
          keywords: [],
          nextPageStart: '1489194883454,2NvDL0XJXXcG8YAwgS',
        },
      ]);
    });

    it('defaults missing fields', () => {
      expect(parseFeedSearchHits({
        hits: [{
          _type: 'Editorial',
          _source: {
            articleId: '2NvDL0XJXXcG8YAwgS',
            createdAt: 1489194883454,
            updatedAt: 1489194883454,
            premium: false,
            promoted: [],
            externalSource: null,
            externalLink: null,
            menuOrder: null,
            auMenuOrder: null,
            nzMenuOrder: null,
            newWindow: false,
            content: {
              title: 'Winter is Coming!',
            },
          },
        }],
      })).to.deep.equal([{
        id: '2NvDL0XJXXcG8YAwgS',
        contentType: 'EDITORIAL',
        createdAt: 1489194883,
        updatedAt: 1489194883,
        premium: false,
        promoted: [],
        externalSource: null,
        externalLink: null,
        menuOrder: null,
        auMenuOrder: null,
        nzMenuOrder: null,
        newWindow: false,
        content: {
          title: 'Winter is Coming!',
          displayTitle: null,
          subtitle: null,
          body: null,
        },
        auContent: {
          title: null,
          subtitle: null,
          displayTitle: null,
        },
        permalink: null,
        media: null,
        author: null,
        tags: [],
        feedSuperheader: null,
        sponsoredArticle: {
          attributionText: null,
          dfpCode: null,
          showAttributionInFeed: false,
          sponsorName: null,
        },
        keywords: [],
        nextPageStart: null,
      }]);
    });

    it('clears content body if source premium is true and premium is false', () => {
      const feedSearchHits = {
        hits: [
          {
            _type: 'Editorial',
            _source: {
              articleId: '12CCDDpSuBQDe86u4GmssPPE',
              createdAt: new Date(1491174682685),
              updatedAt: new Date(1491174682685),
              premium: true,
              promoted: [],
              externalSource: null,
              externalLink: null,
              menuOrder: null,
              auMenuOrder: null,
              nzMenuOrder: null,
              newWindow: false,
              content: {
                title: 'Winter is Coming!',
                subtitle: 'So say we all.',
                teaser: null,
                body: 'Test body',
              },
              auContent: {
                title: null,
                subtitle: null,
                displayTitle: null,
              },
              feedSuperheader: null,
              sponsoredArticle: {
                attributionText: null,
                dfpCode: null,
                showAttributionInFeed: false,
                sponsorName: null,
              },
            },
            sort: [
              1491174682685,
              '12CCDDpSuBQDe86u4GmssPPE',
            ],
          },
        ],
      };
      expect(parseFeedSearchHits(feedSearchHits, false)[0].content.body).to.be.null();
      expect(parseFeedSearchHits(feedSearchHits, true)[0].content.body).to.equal('Test body');
    });


    it('combines teaser and body for premium users', () => {
      const feedSearchHits = {
        hits: [
          {
            _type: 'Editorial',
            _source: {
              articleId: '12CCDDpSuBQDe86u4GmssPPE',
              createdAt: new Date(1491174682685),
              updatedAt: new Date(1491174682685),
              premium: true,
              promoted: [],
              externalSource: null,
              externalLink: null,
              menuOrder: null,
              auMenuOrder: null,
              nzMenuOrder: null,
              newWindow: false,
              content: {
                title: 'Winter is Coming!',
                subtitle: 'So say we all.',
                teaser: 'teaser content',
                body: 'Test body premium',
              },
              feedSuperheader: null,
              sponsoredArticle: {
                attributionText: null,
                dfpCode: null,
                showAttributionInFeed: false,
                sponsorName: null,
              },
            },
            sort: [
              1491174682685,
              '12CCDDpSuBQDe86u4GmssPPE',
            ],
          },
        ],
      };
      expect(parseFeedSearchHits(feedSearchHits, false)[0].content.body).to.be.equal('teaser content');
      expect(parseFeedSearchHits(feedSearchHits, true)[0].content.body).to.equal('teaser contentTest body premium');
    });
  });

  describe('searchTrendingFeedArticles', () => {
    it('searches articles promoted to TRENDING', async () => {
      const promotedFeedArticles = await searchTrendingFeedArticles(15, true);
      expect(promotedFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].index).to.equal('feed');
      expect(client.search.firstCall.args[0].body.size).to.equal(15);
      expect(client.search.firstCall.args[0].body.query).to.deep.equal({
        bool: {
          filter: {
            term: {
              promoted: 'TRENDING',
            },
          },
        },
      });
      expect(client.search.firstCall.args[0].body.sort).to.deep.equal([
        { menuOrder: 'asc' },
        { sortDate: 'desc' },
        { articleId: 'desc' },
      ]);
      expect(client.search.firstCall.args[0].body._source).to.deep.equal(sourceFilterFixture);
    });
  });

  describe('searchEditorsPickFeedArticles', () => {
    it('searches articles promoted to EDITORS_PICK', async () => {
      const promotedFeedArticles = await searchEditorsPickFeedArticles(15, true);
      expect(promotedFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].index).to.equal('feed');
      expect(client.search.firstCall.args[0].body.size).to.equal(15);
      expect(client.search.firstCall.args[0].body.query).to.deep.equal({
        bool: {
          filter: {
            term: {
              promoted: 'EDITORS_PICK',
            },
          },
        },
      });
      expect(client.search.firstCall.args[0].body.sort).to.deep.equal([
        { menuOrder: 'asc' },
        { sortDate: 'desc' },
        { articleId: 'desc' },
      ]);
      expect(client.search.firstCall.args[0].body._source).to.deep.equal(sourceFilterFixture);
    });
  });

  describe('searchTargetedFeedArticles', () => {
    it('searches targeted articles not promoted to EDITORS_PICK nor PROMO_BOX', async () => {
      const searchAfter = [1491174682685, '12CCDDpSuBQDe86u4GmssPPD'];
      const targetTaxonomy = ['5842041f4e65fad6a7708801', '58581a836630e24c44879010'];
      const targetedFeedArticles = await searchTargetedFeedArticles(
        targetTaxonomy,
        15,
        searchAfter,
        true,
      );
      expect(targetedFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].index).to.equal('feed');
      expect(client.search.firstCall.args[0].body.size).to.equal(15);
      expect(client.search.firstCall.args[0].body.search_after).to.deep.equal(searchAfter);
      expect(client.search.firstCall.args[0].body.query).to.deep.equal({
        bool: {
          filter: [
            {
              terms: {
                targetTaxonomy,
              },
            },
          ],
          must_not: {
            terms: {
              promoted: ['EDITORS_PICK', 'PROMO_BOX'],
            },
          },
        },
      });
      expect(client.search.firstCall.args[0].body.sort).to.deep.equal([
        { sortDate: 'desc' },
        { articleId: 'desc' },
      ]);
      expect(client.search.firstCall.args[0].body._source).to.deep.equal(sourceFilterFixture);
    });

    it('uses undefined searchAfter is falsey', async () => {
      const targetedFeedArticles = await searchTargetedFeedArticles(15, null, false);
      expect(targetedFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].body.search_after).to.be.undefined();
    });
  });

  describe('searchRegionalFeedArticles', () => {
    it('searches targeted articles promoted to REGIONAL', async () => {
      const searchAfter = [1491174682685, '12CCDDpSuBQDe86u4GmssPPD'];
      const targetTaxonomy = ['5842041f4e65fad6a7708801', '58581a836630e24c44879010'];
      const filter = 'shorttermforecast';
      const regionalFeedArticles = await searchRegionalFeedArticles(
        targetTaxonomy,
        filter,
        15,
        searchAfter,
        true,
      );
      expect(regionalFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].index).to.equal('feed');
      expect(client.search.firstCall.args[0].body.size).to.equal(15);
      expect(client.search.firstCall.args[0].body.search_after).to.deep.equal(searchAfter);
      expect(client.search.firstCall.args[0].body.query).to.deep.equal({
        bool: {
          should: [
            {
              range: { sortDate: { gte: 'now-7d/d', lte: 'now' } },
            },
            {
              terms: { keywords: ['seasonalforecast'] },
            },
          ],
          filter: [
            {
              terms: {
                targetTaxonomy,
              },
            },
            {
              terms: {
                keywords: ['shorttermforecast'],
              },
            },
            {
              term: {
                promoted: 'REGIONAL',
              },
            },
            {
              range: {
                sortDate: {
                  gte: 'now-10d/d',
                  lte: 'now',
                },
              },
            },
          ],
        },
      });
      expect(client.search.firstCall.args[0].body.sort).to.deep.equal([
        { sortDate: 'desc' },
        { articleId: 'desc' },
      ]);
      expect(client.search.firstCall.args[0].body._source).to.deep.equal(sourceFilterFixture);
    });

    it('uses undefined searchAfter is falsey', async () => {
      const regionalFeedArticles = await searchRegionalFeedArticles(15, null, false);
      expect(regionalFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].body.search_after).to.be.undefined();
    });
  });

  describe('searchForecastFeedArticles', () => {
    it('searches targeted articles of type forecast', async () => {
      const searchAfter = [1491174682685, '12CCDDpSuBQDe86u4GmssPPD'];
      const targetTaxonomy = ['5842041f4e65fad6a7708801', '58581a836630e24c44879010'];
      const regionalFeedArticles = await searchForecastFeedArticles(
        targetTaxonomy,
        null,
        15,
        searchAfter,
        true,
      );
      expect(regionalFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].index).to.equal('feed');
      expect(client.search.firstCall.args[0].body.size).to.equal(15);
      expect(client.search.firstCall.args[0].body.search_after).to.deep.equal(searchAfter);
      expect(client.search.firstCall.args[0].body.query).to.deep.equal({
        bool: {
          filter: [
            {
              terms: {
                targetTaxonomy,
              },
            },
            {
              range: {
                sortDate: {
                  gte: 'now-7d/d',
                  lte: 'now',
                },
              },
            },
          ],
          must_not: {
            term: {
              showInFeed: false,
            },
          },
        },
      });
      expect(client.search.firstCall.args[0].body.sort).to.deep.equal([
        { sortDate: 'desc' },
        { articleId: 'desc' },
      ]);
      expect(client.search.firstCall.args[0].body._source).to.deep.equal(sourceFilterFixture);
    });

    it('uses undefined searchAfter is falsey', async () => {
      const regionalFeedArticles = await searchForecastFeedArticles(15, null, false);
      expect(regionalFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].body.search_after).to.be.undefined();
    });
  });

  describe('searchPromoboxFeedArticles', () => {
    it('searches articles promoted to PROMO_BOX', async () => {
      const promoBox = 'PROMO_BOX';
      const promoboxFeedArticles = await searchPromoboxFeedArticles(15, true);
      expect(promoboxFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].index).to.equal('feed');
      expect(client.search.firstCall.args[0].body.size).to.equal(15);
      expect(client.search.firstCall.args[0].body.query).to.deep.equal({
        bool: {
          filter: [{
            bool: {
              must: {
                exists: {
                  field: 'menuOrder',
                },
              },
            }},
            {term: {
              promoted: promoBox,
            },
          }],
        },
      });
      expect(client.search.firstCall.args[0].body.sort).to.deep.equal([
        { menuOrder: 'asc' },
        { sortDate: 'desc' },
        { articleId: 'desc' },
      ]);
      expect(client.search.firstCall.args[0].body._source).to.deep.equal(sourceFilterFixture);
    });
  });

  describe('searchAUPromoboxFeedArticles', () => {
    it('searches articles promoted to AU_PROMO_BOX', async () => {
      const auPromoBox = 'AU_PROMO_BOX';
      const promoboxFeedArticles = await searchPromoboxFeedArticles(15, true, auPromoBox);
      expect(promoboxFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].index).to.equal('feed');
      expect(client.search.firstCall.args[0].body.size).to.equal(15);
      expect(client.search.firstCall.args[0].body.query).to.deep.equal({
        bool: {
          filter: [{
            bool: {
              must: {
                exists: {
                  field: 'auMenuOrder',
                },
              },
            }},
            {term: {
              promoted: auPromoBox,
            },
          }],
        },
      });
      expect(client.search.firstCall.args[0].body.sort).to.deep.equal([
        { auMenuOrder: 'asc' },
        { sortDate: 'desc' },
        { articleId: 'desc' },
      ]);
      expect(client.search.firstCall.args[0].body._source).to.deep.equal(sourceFilterFixture);
    });
  });

  describe('searchNZPromoboxFeedArticles', () => {
    it('searches articles promoted to NZ_PROMO_BOX', async () => {
      const nzPromoBox = 'NZ_PROMO_BOX';
      const promoboxFeedArticles = await searchPromoboxFeedArticles(15, true, nzPromoBox);
      expect(promoboxFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].index).to.equal('feed');
      expect(client.search.firstCall.args[0].body.size).to.equal(15);
      expect(client.search.firstCall.args[0].body.query).to.deep.equal({
        bool: {
          filter: [{
            bool: {
              must: {
                exists: {
                  field: 'nzMenuOrder',
                },
              },
            }},
            {term: {
              promoted: nzPromoBox,
            },
          }],
        },
      });
      expect(client.search.firstCall.args[0].body.sort).to.deep.equal([
        { nzMenuOrder: 'asc' },
        { sortDate: 'desc' },
        { articleId: 'desc' },
      ]);
      expect(client.search.firstCall.args[0].body._source).to.deep.equal(sourceFilterFixture);
    });
  });

  describe('searchFeedLargeFeedArticles', () => {
    it('searches articles promoted to FEED_LARGE', async () => {
      const searchAfter = [1491174682685, '12CCDDpSuBQDe86u4GmssPPD'];
      const feedLargeFeedArticles = await searchPromotedFeedArticles(3, searchAfter, true, 'FEED_LARGE');
      expect(feedLargeFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].index).to.equal('feed');
      expect(client.search.firstCall.args[0].body.size).to.equal(3);
      expect(client.search.firstCall.args[0].body.search_after).to.deep.equal(searchAfter);
      expect(client.search.firstCall.args[0].body.query).to.deep.equal({
        bool: {
          filter: {
            term: {
              promoted: 'FEED_LARGE',
            },
          },
        },
      });
      expect(client.search.firstCall.args[0].body.sort).to.deep.equal([
        { sortDate: 'desc' },
        { articleId: 'desc' },
      ]);
      expect(client.search.firstCall.args[0].body._source).to.deep.equal(sourceFilterFixture);
    });
  });

  describe('searchFeedSmallFeedArticles', () => {
    it('searches articles promoted to FEED_SMALL', async () => {
      const searchAfter = [1491174682685, '12CCDDpSuBQDe86u4GmssPPD'];
      const feedSmallFeedArticles = await searchPromotedFeedArticles(2, searchAfter, true, 'FEED_SMALL');
      expect(feedSmallFeedArticles).to.have.length(feedArticlesFixture.hits.hits.length);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.search).to.have.been.calledOnce();
      expect(client.search.firstCall.args[0].index).to.equal('feed');
      expect(client.search.firstCall.args[0].body.size).to.equal(2);
      expect(client.search.firstCall.args[0].body.search_after).to.deep.equal(searchAfter);
      expect(client.search.firstCall.args[0].body.query).to.deep.equal({
        bool: {
          filter: {
            term: {
              promoted: 'FEED_SMALL',
            },
          },
        },
      });
      expect(client.search.firstCall.args[0].body.sort).to.deep.equal([
        { sortDate: 'desc' },
        { articleId: 'desc' },
      ]);
      expect(client.search.firstCall.args[0].body._source).to.deep.equal(sourceFilterFixture);
    });
  });

  describe('getLocalFeedArticle', () => {
    it('searches feed articles by articleId', async () => {
      const articleId = '2RKbzlDgmk2sciqUqmeqwA';
      const feedArticle = await getLocalFeedArticle(articleId, true);
      expect(elasticsearch.getClient).to.have.been.calledOnce();
      expect(client.get).to.have.been.calledOnce();
      expect(client.get.firstCall.args[0].index).to.equal('feed');
      expect(client.get.firstCall.args[0]).to.deep.equal({
        id: articleId,
        index: 'feed',
        type: 'forecast',
      });
      expect(feedArticle).to.deep.equal(parsedFeedArticleFixture);
    });
  });
});
