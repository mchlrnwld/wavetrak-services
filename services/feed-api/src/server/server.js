import { setupExpress } from '@surfline/services-common';
import feed from './feed';
import logger from '../common/logger';
import config from '../config';

const log = logger('feed-api');
const setupApp = () =>
  setupExpress({
    log,
    name: 'feed-api',
    port: config.EXPRESS_PORT,
    allowedMethods: ['GET', 'OPTIONS'],
    handlers: [['/feed', feed(log)]],
  });

export default setupApp;
