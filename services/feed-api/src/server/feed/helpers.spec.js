import { expect } from 'chai';
import sinon from 'sinon';
import { personalizedTaxonomy, regionalTaxonomy, buildEvents, buildEvent, buildHeadline } from './helpers';
import * as spotsAPI from '../../external/spots';
import * as searchAPI from '../../external/search';
import * as favoritesAPI from '../../external/favorites';
import * as eventsAPI from '../../external/events';
import headlineFeedData from './fixtures/headlineFeedData.json';

import taxonomyFixture from './fixtures/taxonomy.json';

describe('Helpers', async () => {
  beforeEach(() => {
    sinon.stub(spotsAPI, 'getTaxonomyIds');
  });

  afterEach(() => {
    spotsAPI.getTaxonomyIds.restore();
  });

  describe('personalizedTaxonomy', async () => {
    beforeEach(() => {
      sinon.stub(favoritesAPI, 'getFavoritesForUser');
    });

    afterEach(() => {
      favoritesAPI.getFavoritesForUser.restore();
    });

    it('should fetch taxonomy ids from favorites', async () => {
      const userId = '56fae6fe19b74318a73a7833';
      const userFavorites = {
        favorites: [
          {
            spotId: '5842041f4e65fad6a7708827',
          },
          {
            spotId: '5842041f4e65fad6a770882e',
          },
        ],
      };

      spotsAPI.getTaxonomyIds.resolves(taxonomyFixture);
      favoritesAPI.getFavoritesForUser.resolves(userFavorites);

      const taxonomy = await personalizedTaxonomy(userId);

      expect(favoritesAPI.getFavoritesForUser).to.have.been.calledWith(userId);
      expect(spotsAPI.getTaxonomyIds).to.have.been.calledWith({
        spots: ['5842041f4e65fad6a7708827', '5842041f4e65fad6a770882e'],
      });
      expect(taxonomy).to.equal(taxonomyFixture.taxonomy);
    });
  });

  describe('regionalTaxonomy', async () => {
    beforeEach(() => {
      sinon.stub(spotsAPI, 'getTaxonomy');
    });

    afterEach(() => {
      spotsAPI.getTaxonomy.restore();
    });

    it('should fetch taxonomy IDs for spots that the subregion contains', async () => {
      const subregionId = '58581a836630e24c44878fd6';

      spotsAPI.getTaxonomy.resolves({
        contains: [
          {
            type: 'spot',
            spot: '5842041f4e65fad6a7708827',
          },
          {
            type: 'spot',
            spot: '5842041f4e65fad6a77088ed',
          },
          {
            type: 'for some reason another type is added and we only want spots',
          },
        ],
      });
      spotsAPI.getTaxonomyIds.resolves(taxonomyFixture);

      const taxonomy = await regionalTaxonomy(subregionId);
      expect(spotsAPI.getTaxonomy).to.have.been.calledOnce();
      expect(spotsAPI.getTaxonomy).to.have.been.calledWithExactly('subregion', subregionId);
      expect(spotsAPI.getTaxonomyIds).to.have.been.calledOnce();
      expect(spotsAPI.getTaxonomyIds).to.have.been.calledWithExactly({
        spots: [
          '5842041f4e65fad6a7708827',
          '5842041f4e65fad6a77088ed',
        ],
      });
      expect(taxonomy).to.equal(taxonomyFixture.taxonomy);
    });
  });

  describe('events', async () => {
    beforeEach(() => {
      sinon.stub(eventsAPI, 'getWPEvents');
      sinon.stub(eventsAPI, 'getWPEvent');
      sinon.stub(searchAPI, 'getTaxonomiesByIDs');
      sinon.stub(eventsAPI, 'getWPPost');
    });

    afterEach(() => {
      eventsAPI.getWPEvents.restore();
      eventsAPI.getWPEvent.restore();
      searchAPI.getTaxonomiesByIDs.restore();
      eventsAPI.getWPPost.restore();
    });

    it('should fetch all WP events', async () => {
      eventsAPI.getWPEvents.resolves({
        contains: [
          {
            id: '2635',
            name: 'Hurricane Herman',
          },
        ],
      });

      await buildEvents();
      expect(eventsAPI.getWPEvents).to.have.been.calledOnce();
    });

    it('should fetch single WP event', async () => {
      eventsAPI.getWPEvent.resolves({
        contains: [
          {
            id: '2635',
            name: 'Hurricane Herman',
            updates: [
              {
                id: '2633',
              },
            ],
          },
        ],
      });
      await buildEvent('2635');
      expect(eventsAPI.getWPEvent).to.have.been.calledOnce();
    });
  });

  describe('headline', async () => {
    const geotarget = 'AU';
    it('Changes headline props for Au users in promobox post', async () => {
      let { promobox } = headlineFeedData;
      promobox = buildHeadline(promobox, geotarget);
      expect(promobox[0].content.title).to.equal(headlineFeedData.resolvedPromobox[0].content.title);
      expect(promobox[0].content.subtitle).to.equal(headlineFeedData.resolvedPromobox[0].content.subtitle);
      expect(promobox[0].content.displayTitle).to.equal(headlineFeedData.resolvedPromobox[0].content.displayTitle);
    });

    it('Changes headline props for Au users in curated post', async () => {
      let { curated } = headlineFeedData;
      curated = buildHeadline(curated, geotarget);
      expect(curated[0].content.title).to.equal(headlineFeedData.resolvedCurated[0].content.title);
      expect(curated[0].content.subtitle).to.equal(headlineFeedData.resolvedCurated[0].content.subtitle);
      expect(curated[0].content.displayTitle).to.equal(headlineFeedData.resolvedCurated[0].content.displayTitle);
    });

  });
});
