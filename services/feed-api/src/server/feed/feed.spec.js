import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import { authenticateRequest } from '@surfline/services-common';
import * as entitlements from '../../external/entitlements';
import * as feedModel from '../../model/feed';
import * as helpers from './helpers';
import * as spotsAPI from '../../external/spots';
import * as eventsAPI from '../../external/events';
import * as searchAPI from '../../external/search';
import curatedFeedDataFixture from './fixtures/curatedFeedData.json';
import personalizedFeedDataFixture from './fixtures/personalizedFeedData.json';
import regionalFeedDataFixture from './fixtures/regionalFeedData.json';
import promoboxFeedDataFixture from './fixtures/promoboxFeedData.json';
import parsedFeedArticleFixture from '../../model/fixtures/parsedFeedArticle.json';
import feed from './feed';

describe('/feed', () => {
  let log;
  let request;
  let clock;
  const current = 1489194883;
  const taxonomy = [
    {
      _id: '5842041f4e65fad6a7708806',
    },
    {
      _id: '5842041f4e65fad6a77087f7',
      type: 'subregion',
      subregion: '58581a836630e24c44878fd6',
      name: 'North Orange County',
    },
  ];
  const taxonomyIds = ['5842041f4e65fad6a7708806', '5842041f4e65fad6a77087f7'];
  const filter = null;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(authenticateRequest(), feed(log));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    clock = sinon.useFakeTimers(current * 1000);
    sinon.stub(entitlements, 'isPremium');
  });

  afterEach(() => {
    clock.restore();
    entitlements.isPremium.restore();
  });

  after(() => {
    request.close();
  });

  describe('GET /personalized', () => {
    beforeEach(() => {
      sinon.stub(helpers, 'personalizedTaxonomy');
      sinon.stub(feedModel, 'searchPromotedFeedArticles');
      sinon.stub(feedModel, 'searchTrendingFeedArticles');
      sinon.stub(feedModel, 'searchEditorsPickFeedArticles');
      sinon.stub(feedModel, 'searchTargetedFeedArticles');
      sinon.stub(spotsAPI, 'getTaxonomy');
    });

    afterEach(() => {
      helpers.personalizedTaxonomy.restore();
      feedModel.searchPromotedFeedArticles.restore();
      feedModel.searchTrendingFeedArticles.restore();
      feedModel.searchEditorsPickFeedArticles.restore();
      feedModel.searchTargetedFeedArticles.restore();
      spotsAPI.getTaxonomy.restore();
    });

    it('returns promoted and targeted articles for authenticated user', async () => {
      const userId = '5842041f4e65fad6a7708801';

      entitlements.isPremium.resolves(true);
      helpers.personalizedTaxonomy.resolves(taxonomy);
      feedModel.searchTrendingFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchEditorsPickFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchTargetedFeedArticles.resolves(personalizedFeedDataFixture.targeted);

      const res = await request
        .get('/personalized')
        .set('X-Auth-UserId', userId)
        .send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextTargetedStart: '1489194883454',
          feedType: 'TARGETED',
          subregions: [{ _id: '58581a836630e24c44878fd6', name: 'North Orange County' }],
        },
        data: personalizedFeedDataFixture,
      });
      expect(helpers.personalizedTaxonomy).to.have.been.calledOnce();
      expect(helpers.personalizedTaxonomy).to.have.been.calledWithExactly(userId);
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(userId);
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledWithExactly(5, true);
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledWithExactly(5, true);
      expect(feedModel.searchTargetedFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchTargetedFeedArticles).to.have.been.calledWithExactly(
        taxonomyIds,
        50,
        null,
        true,
      );
      expect(feedModel.searchPromotedFeedArticles).not.to.have.been.called();
    });

    it('returns default feed articles for unauthenticated user', async () => {
      entitlements.isPremium.resolves(false);
      feedModel.searchPromotedFeedArticles.resolves(personalizedFeedDataFixture.targeted);
      feedModel.searchTrendingFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchEditorsPickFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchTargetedFeedArticles.resolves(personalizedFeedDataFixture.targeted);

      const res = await request.get('/personalized').send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextTargetedStart: '1489194883454',
          feedType: 'DEFAULT',
          subregions: [],
        },
        data: personalizedFeedDataFixture,
      });
      expect(helpers.personalizedTaxonomy).not.to.have.been.called();
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(undefined);
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledWithExactly(5, false);
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledWithExactly(5, false);
      expect(feedModel.searchTargetedFeedArticles).not.to.have.been.called();
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWithExactly(
        50,
        null,
        false,
        'CURATED',
        null,
        ['EDITORS_PICK', 'PROMO_BOX', 'TRENDING'],
      );
    });

    it('returns default feed articles for authenticated user with no taxonomy to target', async () => {
      const userId = '5842041f4e65fad6a7708801';

      entitlements.isPremium.resolves(false);
      helpers.personalizedTaxonomy.resolves([]);
      feedModel.searchPromotedFeedArticles.resolves(personalizedFeedDataFixture.targeted);
      feedModel.searchTrendingFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchEditorsPickFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchTargetedFeedArticles.resolves(personalizedFeedDataFixture.targeted);

      const res = await request
        .get('/personalized')
        .set('X-Auth-UserId', userId)
        .send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextTargetedStart: '1489194883454',
          feedType: 'DEFAULT',
          subregions: [],
        },
        data: personalizedFeedDataFixture,
      });
      expect(helpers.personalizedTaxonomy).to.have.been.calledOnce();
      expect(helpers.personalizedTaxonomy).to.have.been.calledWithExactly(userId);
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(userId);
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledWithExactly(5, false);
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledWithExactly(5, false);
      expect(feedModel.searchTargetedFeedArticles).not.to.have.been.called();
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWithExactly(
        50,
        null,
        false,
        'CURATED',
        null,
        ['EDITORS_PICK', 'PROMO_BOX', 'TRENDING'],
      );
    });

    it('returns default feed articles for authenticated user with no articles targeted at them', async () => {
      const userId = '5842041f4e65fad6a7708801';

      entitlements.isPremium.resolves(false);
      helpers.personalizedTaxonomy.resolves(taxonomy);
      feedModel.searchPromotedFeedArticles.resolves(personalizedFeedDataFixture.targeted);
      feedModel.searchTrendingFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchEditorsPickFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchTargetedFeedArticles.resolves([]);

      const res = await request
        .get('/personalized')
        .set('X-Auth-UserId', userId)
        .send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextTargetedStart: '1489194883454',
          feedType: 'DEFAULT',
          subregions: [{ _id: '58581a836630e24c44878fd6', name: 'North Orange County' }],
        },
        data: personalizedFeedDataFixture,
      });
      expect(helpers.personalizedTaxonomy).to.have.been.calledOnce();
      expect(helpers.personalizedTaxonomy).to.have.been.calledWithExactly(userId);
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(userId);
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledWithExactly(5, false);
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledWithExactly(5, false);
      expect(feedModel.searchTargetedFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchTargetedFeedArticles).to.have.been.calledWithExactly(
        taxonomyIds,
        50,
        null,
        false,
      );
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWithExactly(
        50,
        null,
        false,
        'CURATED',
        null,
        ['EDITORS_PICK', 'PROMO_BOX', 'TRENDING'],
      );
    });

    it('returns default feed articles for authenticated user with only stale articles targeted at them', async () => {
      clock.tick(7 * 24 * 60 * 60 * 1000 + 1);
      const userId = '5842041f4e65fad6a7708801';

      entitlements.isPremium.resolves(false);
      helpers.personalizedTaxonomy.resolves(taxonomy);
      feedModel.searchPromotedFeedArticles.resolves(personalizedFeedDataFixture.targeted);
      feedModel.searchTrendingFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchEditorsPickFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchTargetedFeedArticles.resolves(personalizedFeedDataFixture.targeted);

      const res = await request
        .get('/personalized')
        .set('X-Auth-UserId', userId)
        .send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextTargetedStart: '1489194883454',
          feedType: 'DEFAULT',
          subregions: [{ _id: '58581a836630e24c44878fd6', name: 'North Orange County' }],
        },
        data: personalizedFeedDataFixture,
      });
      expect(helpers.personalizedTaxonomy).to.have.been.calledOnce();
      expect(helpers.personalizedTaxonomy).to.have.been.calledWithExactly(userId);
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(userId);
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledWithExactly(5, false);
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledWithExactly(5, false);
      expect(feedModel.searchTargetedFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchTargetedFeedArticles).to.have.been.calledWithExactly(
        taxonomyIds,
        50,
        null,
        false,
      );
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWithExactly(
        50,
        null,
        false,
        'CURATED',
        null,
        ['EDITORS_PICK', 'PROMO_BOX', 'TRENDING'],
      );
    });

    it('allows paginated requests for targeted articles only', async () => {
      const promotedStart = [1491174682685, '12CCDDpSuBQDe86u4GmssPPE'];
      const targetedStart = [1491001882685, '12CCDDpSuBQDe86u4GmssPPP'];
      const limit = 8;

      entitlements.isPremium.resolves(false);
      feedModel.searchPromotedFeedArticles.resolves(personalizedFeedDataFixture.targeted);
      feedModel.searchTrendingFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchEditorsPickFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchTargetedFeedArticles.resolves(personalizedFeedDataFixture.targeted);

      const promotedStartString = promotedStart.join(',');
      const targetedStartString = targetedStart.join(',');

      const res = await request
        .get(
          `/personalized?promotedStart=${promotedStartString}&targetedStart=${targetedStartString}&limit=${limit}`,
        )
        .send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextTargetedStart: '1489194883454',
          feedType: 'DEFAULT',
          subregions: [],
        },
        data: {
          ...personalizedFeedDataFixture,
          promoted: [],
        },
      });
      expect(feedModel.searchTrendingFeedArticles).not.to.have.been.called();
      expect(feedModel.searchEditorsPickFeedArticles).not.to.have.been.called();
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWithExactly(
        limit,
        targetedStart,
        false,
        'CURATED',
        null,
        ['EDITORS_PICK', 'PROMO_BOX', 'TRENDING'],
      );
      expect(feedModel.searchTargetedFeedArticles).not.to.have.been.called();
    });

    it('restricts limit to at most 50 for targeted and 5 for trending and editors picks', async () => {
      entitlements.isPremium.resolves(false);
      feedModel.searchPromotedFeedArticles.resolves(personalizedFeedDataFixture.targeted);
      feedModel.searchTrendingFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchEditorsPickFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchTargetedFeedArticles.resolves(personalizedFeedDataFixture.targeted);

      const res = await request.get('/personalized?limit=51');

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextTargetedStart: '1489194883454',
          feedType: 'DEFAULT',
          subregions: [],
        },
        data: personalizedFeedDataFixture,
      });
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledWithExactly(5, false);
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledWithExactly(5, false);
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWithExactly(
        50,
        null,
        false,
        'CURATED',
        null,
        ['EDITORS_PICK', 'PROMO_BOX', 'TRENDING'],
      );
      expect(feedModel.searchTargetedFeedArticles).not.to.have.been.called();
    });

    it('returns 400 if targeted start parameter is invalid', async () => {
      try {
        const res = await request.get('/personalized?targetedStart=error');
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({
          message: 'Invalid targetedStart parameter',
        });
      }

      expect(feedModel.searchTargetedFeedArticles).not.to.have.been.called();
      expect(feedModel.searchPromotedFeedArticles).not.to.have.been.called();
    });

    it('queries articles by subregion if passed subregionIds', async () => {
      const subregionTaxonomy = {
        _id: '5842041f4e65fad6a77087f7',
        type: 'subregion',
        subregion: '58581a836630e24c44878fd6',
        name: 'North Orange County',
        in: [],
      };
      const userId = '5842041f4e65fad6a7708801';

      entitlements.isPremium.resolves(true);
      spotsAPI.getTaxonomy.resolves(subregionTaxonomy);
      feedModel.searchTrendingFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchEditorsPickFeedArticles.resolves(personalizedFeedDataFixture.promoted);
      feedModel.searchTargetedFeedArticles.resolves(personalizedFeedDataFixture.targeted);

      const res = await request
        .get('/personalized?subregionIds=subId1')
        .set('X-Auth-UserId', userId)
        .send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextTargetedStart: '1489194883454',
          feedType: 'TARGETED',
          subregions: [{ _id: '58581a836630e24c44878fd6', name: 'North Orange County' }],
        },
        data: personalizedFeedDataFixture,
      });
      expect(helpers.personalizedTaxonomy).not.to.have.been.called();
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(userId);
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchTrendingFeedArticles).to.have.been.calledWithExactly(5, true);
      expect(feedModel.searchEditorsPickFeedArticles).to.have.been.calledWithExactly(5, true);
      expect(feedModel.searchTargetedFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchTargetedFeedArticles).to.have.been.calledWithExactly(
        ['5842041f4e65fad6a77087f7'],
        50,
        null,
        true,
      );
      expect(feedModel.searchPromotedFeedArticles).not.to.have.been.called();
    });
  });

  describe('/regional', () => {
    beforeEach(() => {
      sinon.stub(helpers, 'regionalTaxonomy');
      sinon.stub(feedModel, 'searchForecastFeedArticles');
      sinon.stub(feedModel, 'searchRegionalFeedArticles');
      sinon.stub(feedModel, 'aggregateRegionalFeedKeywords');
    });

    afterEach(() => {
      helpers.regionalTaxonomy.restore();
      feedModel.searchForecastFeedArticles.restore();
      feedModel.searchRegionalFeedArticles.restore();
      feedModel.aggregateRegionalFeedKeywords.restore();
    });

    it('returns regional articles targeted at entire subregion taxonomy', async () => {
      const userId = '5842041f4e65fad6a7708801';
      const subregionId = '58581a836630e24c44878fd6';

      entitlements.isPremium.resolves(true);
      helpers.regionalTaxonomy.resolves(taxonomy);
      feedModel.searchRegionalFeedArticles.resolves(regionalFeedDataFixture.regional);
      feedModel.aggregateRegionalFeedKeywords.resolves([]);

      const res = await request
        .get(`/regional?subregionId=${subregionId}`)
        .set('X-Auth-UserId', userId)
        .send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextStart: '1489194883454',
          keywords: [],
        },
        data: regionalFeedDataFixture,
      });
      expect(helpers.regionalTaxonomy).to.have.been.calledOnce();
      expect(helpers.regionalTaxonomy).to.have.been.calledWithExactly(subregionId);
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(userId, undefined);
      expect(feedModel.searchRegionalFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchRegionalFeedArticles).to.have.been.calledWithExactly(
        taxonomyIds,
        filter,
        50,
        null,
        true,
      );
    });

    it('filters premium content from articles if user is not premium', async () => {
      const subregionId = '58581a836630e24c44878fd6';

      entitlements.isPremium.resolves(false);
      helpers.regionalTaxonomy.resolves(taxonomy);
      feedModel.searchRegionalFeedArticles.resolves(regionalFeedDataFixture.regional);
      feedModel.aggregateRegionalFeedKeywords.resolves([]);

      const res = await request.get(`/regional?subregionId=${subregionId}`).send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextStart: '1489194883454',
          keywords: [],
        },
        data: regionalFeedDataFixture,
      });
      expect(helpers.regionalTaxonomy).to.have.been.called();
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(undefined, undefined);
      expect(feedModel.searchRegionalFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchRegionalFeedArticles).to.have.been.calledWithExactly(
        taxonomyIds,
        filter,
        50,
        null,
        false,
      );
    });

    it('allows paginated requests', async () => {
      const subregionId = '58581a836630e24c44878fd6';

      const start = [1491174682685, '12CCDDpSuBQDe86u4GmssPPE'];
      const limit = 8;

      entitlements.isPremium.resolves(false);
      helpers.regionalTaxonomy.resolves(taxonomy);
      feedModel.searchRegionalFeedArticles.resolves(regionalFeedDataFixture.regional);
      feedModel.aggregateRegionalFeedKeywords.resolves([]);

      const startString = start.join(',');

      const res = await request
        .get(`/regional?subregionId=${subregionId}&start=${startString}&limit=${limit}`)
        .send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextStart: '1489194883454',
          keywords: [],
        },
        data: regionalFeedDataFixture,
      });
      expect(feedModel.searchRegionalFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchRegionalFeedArticles).to.have.been.calledWithExactly(
        taxonomyIds,
        filter,
        limit,
        start,
        false,
      );
    });

    it('restricts limit to at most 50', async () => {
      const subregionId = '58581a836630e24c44878fd6';

      entitlements.isPremium.resolves(false);
      helpers.regionalTaxonomy.resolves(taxonomy);
      feedModel.searchRegionalFeedArticles.resolves(regionalFeedDataFixture.regional);
      feedModel.aggregateRegionalFeedKeywords.resolves([]);

      const res = await request.get(`/regional?subregionId=${subregionId}&limit=51`);

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextStart: '1489194883454',
          keywords: [],
        },
        data: regionalFeedDataFixture,
      });
      expect(feedModel.searchRegionalFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchRegionalFeedArticles).to.have.been.calledWithExactly(
        taxonomyIds,
        filter,
        50,
        null,
        false,
      );
    });

    it('returns 400 if subregionId is not provided', async () => {
      try {
        const res = await request.get('/regional');
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({
          message: 'subregionId required',
        });
      }
    });

    it('returns 400 if start parameter is invalid', async () => {
      try {
        const res = await request.get('/regional?subregionId=58581a836630e24c44878fd6&start=error');
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({
          message: 'Invalid start parameter',
        });
      }

      expect(feedModel.searchRegionalFeedArticles).not.to.have.been.called();
      expect(feedModel.searchRegionalFeedArticles).not.to.have.been.called();
    });

    it('returns local news if includeLocalNews parameter is truthy', async () => {
      const subregionId = '58581a836630e24c44878fd6';

      entitlements.isPremium.resolves(false);
      helpers.regionalTaxonomy.resolves(taxonomy);
      feedModel.searchForecastFeedArticles.resolves(regionalFeedDataFixture.regional);
      feedModel.aggregateRegionalFeedKeywords.resolves([]);

      const res = await request.get(`/regional?subregionId=${subregionId}&includeLocalNews=true`);

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextStart: '1489194883454',
          keywords: [],
        },
        data: regionalFeedDataFixture,
      });
      expect(feedModel.searchForecastFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchForecastFeedArticles).to.have.been.calledWithExactly(
        taxonomyIds,
        null,
        50,
        null,
        false,
      );
    });
  });

  describe('/promobox', () => {
    beforeEach(() => {
      sinon.stub(feedModel, 'searchPromoboxFeedArticles');
    });

    afterEach(() => {
      feedModel.searchPromoboxFeedArticles.restore();
    });

    it('returns promobox articles', async () => {
      const userId = '5842041f4e65fad6a7708801';

      entitlements.isPremium.resolves(true);
      feedModel.searchPromoboxFeedArticles.resolves(promoboxFeedDataFixture.promobox);

      const res = await request
        .get('/promobox')
        .set('X-Auth-UserId', userId)
        .send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        data: promoboxFeedDataFixture,
      });
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(userId);
      expect(feedModel.searchPromoboxFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchPromoboxFeedArticles).to.have.been.calledWithExactly(50, true, 'PROMO_BOX');
    });

    it('filters premium content from articles if user is not premium', async () => {
      entitlements.isPremium.resolves(false);
      feedModel.searchPromoboxFeedArticles.resolves(promoboxFeedDataFixture.promobox);

      const res = await request.get('/promobox').send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        data: promoboxFeedDataFixture,
      });
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(undefined);
      expect(feedModel.searchPromoboxFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchPromoboxFeedArticles).to.have.been.calledWithExactly(50, false, 'PROMO_BOX');
    });

    it('allows requesting feed page limit', async () => {
      entitlements.isPremium.resolves(false);
      feedModel.searchPromoboxFeedArticles.resolves(promoboxFeedDataFixture.promobox);

      const res = await request.get('/promobox?limit=8').send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        data: promoboxFeedDataFixture,
      });
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(undefined);
      expect(feedModel.searchPromoboxFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchPromoboxFeedArticles).to.have.been.calledWithExactly(8, false, 'PROMO_BOX');
    });

    it('restricts limit to at most 50', async () => {
      entitlements.isPremium.resolves(false);
      feedModel.searchPromoboxFeedArticles.resolves(promoboxFeedDataFixture.promobox);

      const res = await request.get('/promobox?limit=51');

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        data: promoboxFeedDataFixture,
      });
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(undefined);
      expect(feedModel.searchPromoboxFeedArticles).to.have.been.calledOnce();
      expect(feedModel.searchPromoboxFeedArticles).to.have.been.calledWithExactly(50, false, 'PROMO_BOX');
    });
  });

  describe('/curated', () => {
    beforeEach(() => {
      sinon.stub(feedModel, 'searchPromotedFeedArticles');
    });

    afterEach(() => {
      feedModel.searchPromotedFeedArticles.restore();
    });

    it('returns homepage large, small, and curated articles', async () => {
      const userId = '5842041f4e65fad6a7708801';
      entitlements.isPremium.resolves(true);
      feedModel.searchPromotedFeedArticles
        .onFirstCall()
        .resolves(curatedFeedDataFixture.data.feedLarge);
      feedModel.searchPromotedFeedArticles
        .onSecondCall()
        .resolves(curatedFeedDataFixture.data.feedSmall);
      feedModel.searchPromotedFeedArticles
        .onThirdCall()
        .resolves(curatedFeedDataFixture.data.curated);
      const res = await request
        .get('/curated')
        .set('X-Auth-UserId', userId)
        .send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(curatedFeedDataFixture);
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(userId);
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledThrice();
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWith(
        3,
        null,
        true,
        'FEED_LARGE',
        undefined,
        ['PROMO_BOX'],
      );
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWith(
        2,
        null,
        true,
        'FEED_SMALL',
        undefined,
        ['PROMO_BOX'],
      );
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWith(
        12,
        null,
        true,
        'CURATED',
        undefined,
        ['FEED_LARGE', 'FEED_SMALL', 'PROMO_BOX'],
      );
    });

    it('filters premium content from articles if user is not premium', async () => {
      entitlements.isPremium.resolves(false);
      feedModel.searchPromotedFeedArticles
        .onFirstCall()
        .resolves(curatedFeedDataFixture.data.feedLarge);
      feedModel.searchPromotedFeedArticles
        .onSecondCall()
        .resolves(curatedFeedDataFixture.data.feedSmall);
      feedModel.searchPromotedFeedArticles
        .onThirdCall()
        .resolves(curatedFeedDataFixture.data.curated);

      const res = await request.get('/curated').send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal(curatedFeedDataFixture);
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(undefined);
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledThrice();
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWith(
        3,
        null,
        false,
        'FEED_LARGE',
        undefined,
        ['PROMO_BOX'],
      );
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWith(
        2,
        null,
        false,
        'FEED_SMALL',
        undefined,
        ['PROMO_BOX'],
      );
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWith(
        12,
        null,
        false,
        'CURATED',
        undefined,
        ['FEED_LARGE', 'FEED_SMALL', 'PROMO_BOX'],
      );
    });

    it('allows paginated requests', async () => {
      const feedLargeStart = [1491174682685, '12CCDDpSuBQDe86u4GmssPPE'];
      const feedSmallStart = [1491174682685, '12CCDDpSuBQDe86u4GmssPPE'];
      const curatedStart = [1491174682685, '12CCDDpSuBQDe86u4GmssPPE'];

      const feedLargeStartString = feedLargeStart.join(',');
      const feedSmallStartString = feedSmallStart.join(',');
      const curatedtartString = curatedStart.join(',');

      entitlements.isPremium.resolves(false);
      feedModel.searchPromotedFeedArticles
        .onFirstCall()
        .resolves(curatedFeedDataFixture.data.feedLarge);
      feedModel.searchPromotedFeedArticles
        .onSecondCall()
        .resolves(curatedFeedDataFixture.data.feedSmall);
      feedModel.searchPromotedFeedArticles
        .onThirdCall()
        .resolves(curatedFeedDataFixture.data.curated);

      const res = await request
        .get(
          `/curated?feedLargeStart=${feedLargeStartString}&feedSmallStart=${feedSmallStartString}&curatedStart=${curatedtartString}`,
        )
        .send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        associated: {
          nextFeedLargeStart: '1489194883454',
          nextFeedSmallStart: '1489194883454',
          nextCuratedStart: '1489194883454',
        },
        data: curatedFeedDataFixture.data,
      });
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledThrice();
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWith(
        3,
        feedLargeStart,
        false,
        'FEED_LARGE',
        undefined,
        ['PROMO_BOX'],
      );
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWith(
        2,
        feedSmallStart,
        false,
        'FEED_SMALL',
        undefined,
        ['PROMO_BOX'],
      );
      expect(feedModel.searchPromotedFeedArticles).to.have.been.calledWith(
        12,
        curatedStart,
        false,
        'CURATED',
        undefined,
        ['FEED_LARGE', 'FEED_SMALL', 'PROMO_BOX'],
      );
    });
  });

  describe('/local', () => {
    beforeEach(() => {
      sinon.stub(feedModel, 'getLocalFeedArticle');
    });

    afterEach(() => {
      feedModel.getLocalFeedArticle.restore();
    });

    it('returns feed article', async () => {
      const userId = '5842041f4e65fad6a7708801';

      entitlements.isPremium.resolves(true);
      feedModel.getLocalFeedArticle.resolves(parsedFeedArticleFixture);

      const res = await request
        .get('/local?id=2RKbzlDgmk2sciqUqmeqwA')
        .set('X-Auth-UserId', userId)
        .send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({ data: { article: parsedFeedArticleFixture } });
      expect(entitlements.isPremium).to.have.been.calledOnce();
      expect(entitlements.isPremium).to.have.been.calledWithExactly(userId);
      expect(feedModel.getLocalFeedArticle).to.have.been.calledOnce();
      expect(feedModel.getLocalFeedArticle).to.have.been.calledWithExactly(
        '2RKbzlDgmk2sciqUqmeqwA',
        true,
      );
    });
  });

  describe('/events', () => {
    beforeEach(() => {
      sinon.stub(eventsAPI, 'getWPEvent');
      sinon.stub(eventsAPI, 'getWPEvents');
      sinon.stub(searchAPI, 'getTaxonomiesByIDs');
    });

    afterEach(() => {
      eventsAPI.getWPEvent.restore();
      eventsAPI.getWPEvents.restore();
      searchAPI.getTaxonomiesByIDs.restore();
    });

    it('returns all events', async () => {
      const res = await request.get('/events').send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(eventsAPI.getWPEvents).to.have.been.calledOnce();
    });

    it('returns a single event', async () => {
      const res = await request.get('/events/2635').send();

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(eventsAPI.getWPEvent).to.have.been.calledOnce();
    });
  });
});
