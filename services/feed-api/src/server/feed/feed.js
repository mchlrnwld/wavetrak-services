import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import {
  curatedHandler,
  localHandler,
  personalizedHandler,
  promoboxHandler,
  regionalHandler,
  trackFeedRequests,
  eventsHandler,
  eventHandler,
} from './handlers';

// TODO: Deprecate this once mobile is fixed
const appVersion = (req, res, next) => {
  req.mobile = !!req.get('X-App-Version');
  return next();
};

const feed = log => {
  const api = Router();

  api.use('*', trackFeedRequests(log), appVersion);

  api.get('/personalized', wrapErrors(personalizedHandler));
  api.get('/regional', wrapErrors(regionalHandler));
  api.get('/promobox', wrapErrors(promoboxHandler));
  api.get('/local', wrapErrors(localHandler));
  api.get('/curated', wrapErrors(curatedHandler));
  api.get('/events/:id', wrapErrors(eventHandler));
  api.get('/events', wrapErrors(eventsHandler));

  return api;
};

export default feed;
