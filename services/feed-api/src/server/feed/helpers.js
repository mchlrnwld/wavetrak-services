import { getTaxonomyIds, getTaxonomy } from '../../external/spots';
import { getFavoritesForUser } from '../../external/favorites';
import { getTaxonomiesByIDs } from '../../external/search';
// eslint-disable-next-line import/no-cycle
import { getWPEvents, getWPEvent, getWPPost } from '../../external/events';

const buildBody = (teaser = '', body = '', premiumContent, premiumUser) => {
  let content = '';
  if (!premiumUser && premiumContent) {
    content = teaser || '';
  } else {
    content = (body && teaser && body.indexOf(teaser) < 0) ? (teaser + body) : (body || '');
  }
  return content.length > 0 ? content : null;
};

const personalizedTaxonomy = async (userId) => {
  const favorites = await getFavoritesForUser(userId);
  const spots = favorites.favorites.map(favorite => favorite.spotId);
  const taxonomyIdsResponse = await getTaxonomyIds({ spots });

  return taxonomyIdsResponse.taxonomy;
};

const regionalTaxonomy = async (subregionId) => {
  const subregionTaxonomy = await getTaxonomy('subregion', subregionId);
  const spots = subregionTaxonomy.contains
    .filter(({ type }) => type === 'spot')
    .map(({ spot }) => spot);
  const taxonomyIdsResponse = await getTaxonomyIds({ spots });

  return taxonomyIdsResponse.taxonomy;
};

// TODO: Deprecate this once mobile is fixed
const updateMobilePermalinks = (mobile, articles) => articles.map((article) => {
  let { permalink } = article;
  if (mobile) permalink = article.contentType === 'FORECAST' ? null : `${permalink}?native=true`;
  return { ...article, permalink };
});

// Storm Feed Events

const buildEvents = async () => {
  try {
    const wpEvents = await getWPEvents();

    const alteredEvents = await Promise.all(wpEvents.map(async (wpEvent) => {
      if (wpEvent.taxonomies.length > 0) {
        // Build taxonomies array
        const taxonomies = await getTaxonomiesByIDs(wpEvent.taxonomies);
        const { hits: { hits } } = taxonomies;
        const taxonomyArr = hits.map(result => ({
          id: result._id,
          href: result._source.href,
          name: result._source.name,
          type: result._type, // eslint-disable-line
          breadCrumbs: result._source.breadCrumbs,
        }));

        const sortedTaxonomies = wpEvent.taxonomies.map(taxonomy =>
          taxonomyArr.find(obj => obj.id === taxonomy));
        // Overwrite taxonomies array
        wpEvent.taxonomies = sortedTaxonomies; // eslint-disable-line
      }
      return wpEvent;
    }));
    return alteredEvents;
  } catch (e) {
    return e;
  }
};

const buildEvent = async (id, premium) => {
  try {
    const wpEvent = await getWPEvent(id);

    if (wpEvent.taxonomies.length > 0) {
      // Build taxonomies array
      const taxonomies = await getTaxonomiesByIDs(wpEvent.taxonomies);
      const results = taxonomies.hits.hits;
      const taxonomyArr = results.map(result => ({
        id: result._id,
        href: result._source.href,
        name: result._source.name,
        type: result._type, // eslint-disable-line
        breadCrumbs: result._source.breadCrumbs,
      }));
      const sortedTaxonomies = wpEvent.taxonomies.map(taxonomy =>
        taxonomyArr.find(obj => obj.id === taxonomy));
      // Overwrite taxonomies array
      wpEvent.taxonomies = sortedTaxonomies; // eslint-disable-line
    }
    // Build updates array
    if (wpEvent.updates.length > 0) {
      const updatePosts = await Promise.all(wpEvent.updates.map(async (updateId) => {
        const fetchedPost = await getWPPost(updateId, premium);

        const update = {
          id: fetchedPost.id,
          premiumContent: fetchedPost.premium,
          createdAt: fetchedPost.createdAt,
          updatedAt: fetchedPost.updatedAt,
          title: fetchedPost.content.title,
          content: buildBody(
            fetchedPost.content.teaser,
            fetchedPost.content.body,
            fetchedPost.premium.premium,
            premium),
          author: fetchedPost.author,
          media: fetchedPost.media,
        };
        return update;
      }));
      // Overwrite updates array
      wpEvent.updates = updatePosts; // eslint-disable-line
    }
    return wpEvent;
  } catch (e) {
    return e;
  }
};

const buildHeadline = (articles, geotarget) => {
  const newArticles = [];
  articles.forEach(article => {
    const newArticle = article;
    const content = {};
    const { auContent } = article;
    const { title, subtitle, displayTitle, body } = article.content;
    if (geotarget === 'AU') {
      content.title = auContent?.title || title;
      content.subtitle = auContent?.subtitle || subtitle;
      content.displayTitle = auContent?.displayTitle || auContent?.title || displayTitle
      content.body = body;
      newArticle.content = content;
    }
    delete newArticle.auContent;
    newArticles.push(newArticle);
  });
  return newArticles;
};

export {
  buildBody,
  personalizedTaxonomy,
  regionalTaxonomy,
  updateMobilePermalinks,
  buildEvents,
  buildEvent,
  buildHeadline,
};
