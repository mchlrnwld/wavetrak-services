import newrelic from 'newrelic';
import { isPremium } from '../../external/entitlements';
import getRegionalOverview from '../../external/regionOverview';

import {
  personalizedTaxonomy,
  regionalTaxonomy,
  updateMobilePermalinks,
  buildEvent,
  buildEvents,
  buildHeadline,
} from './helpers';
import {
  searchEditorsPickFeedArticles,
  getLocalFeedArticle,
  searchTrendingFeedArticles,
  searchTargetedFeedArticles,
  searchRegionalFeedArticles,
  searchPromoboxFeedArticles,
  searchForecastFeedArticles,
  searchPromotedFeedArticles,
  aggregateRegionalFeedKeywords,
} from '../../model/feed';
import { getTaxonomy } from '../../external/spots';

const trackFeedRequests = log => (req, res, next) => {
  log.trace({
    action: req.baseUrl,
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const parseSearchAfter = (searchAfter, type) => {
  if (!searchAfter) return null;

  const searchAfterStrings = searchAfter.split(',');
  const searchAfterSortDate = parseInt(searchAfterStrings[0], 10);
  const searchAfterArticleId = searchAfterStrings[1];

  // eslint-disable-next-line no-restricted-globals
  if (isNaN(searchAfterSortDate))
    throw new Error(`Invalid ${type ? `${type}Start` : 'start'} parameter`);

  return [searchAfterSortDate, searchAfterArticleId];
};

const defSortVal = 100000;
const combinePromotedArray = (editorsPick, trending) => {
  const trendingIds = trending.map(t => t.id);
  return [...trending, ...editorsPick.filter(e => !trendingIds.includes(e.id))].sort(
    (a, b) => (a.menuOrder || defSortVal) - (b.menuOrder || defSortVal) || b.updatedAt - a.updatedAt,
  );
};

const curatedHandler = async (req, res) => {
  const premium = await isPremium(req.authenticatedUserId);
  const tagFilter = req.query.tag;
  const { geotarget } = req.query;

  const feedLargeSize = req.query.feedLargeLimit ? parseInt(req.query.feedLargeLimit, 10) : 3;
  const feedSmallSize = req.query.feedSmallLimit ? parseInt(req.query.feedSmallLimit, 10) : 2;
  const curatedSize = req.query.curatedLimit ? parseInt(req.query.curatedLimit, 10) : 12;

  let feedLargeSearchAfter;
  let feedSmallSearchAfter;
  let curatedSearchAfter;
  try {
    feedLargeSearchAfter = parseSearchAfter(req.query.feedLargeStart, 'feedLargeStart');
    feedSmallSearchAfter = parseSearchAfter(req.query.feedSmallStart, 'feedSmallStart');
    curatedSearchAfter = parseSearchAfter(req.query.curatedStart, 'curatedStart');
  } catch (err) {
    return res.status(400).send({ message: err.message });
  }

  let FEED_LARGE = 'FEED_LARGE';
  let FEED_SMALL = 'FEED_SMALL';
  let CURATED = 'CURATED';
  let FILTER_OUT = 'PROMO_BOX';
  if (geotarget === 'AU') {
    FEED_LARGE = 'AU_FEED_LARGE';
    FEED_SMALL = 'AU_FEED_SMALL';
    CURATED = 'AU_CURATED';
    FILTER_OUT = 'AU_PROMO_BOX';
  } else if (geotarget === 'NZ') {
    FEED_LARGE = 'NZ_FEED_LARGE';
    FEED_SMALL = 'NZ_FEED_SMALL';
    CURATED = 'NZ_CURATED';
    FILTER_OUT = 'NZ_PROMO_BOX';
  }

  let [feedLarge, feedSmall, curated] = await Promise.all([
    searchPromotedFeedArticles(
      feedLargeSize,
      feedLargeSearchAfter,
      premium,
      FEED_LARGE,
      tagFilter,
      [FILTER_OUT],
    ),
    searchPromotedFeedArticles(
      feedSmallSize,
      feedSmallSearchAfter,
      premium,
      FEED_SMALL,
      tagFilter,
      [FILTER_OUT],
    ),
    searchPromotedFeedArticles(curatedSize, curatedSearchAfter, premium, CURATED, tagFilter, [
      FEED_LARGE,
      FEED_SMALL,
      FILTER_OUT,
    ]),
  ]);

  feedLarge = buildHeadline(feedLarge, geotarget);
  feedSmall = buildHeadline(feedSmall, geotarget);
  curated = buildHeadline(curated, geotarget);

  const lastFeedLarge = feedLarge[feedLarge.length - 1];
  const lastFeedSmall = feedSmall[feedSmall.length - 1];
  const lastCurated = curated[curated.length - 1];

  return res.send({
    associated: {
      nextFeedLargeStart: lastFeedLarge ? lastFeedLarge.nextPageStart : null,
      nextFeedSmallStart: lastFeedSmall ? lastFeedSmall.nextPageStart : null,
      nextCuratedStart: lastCurated ? lastCurated.nextPageStart : null,
    },
    data: {
      feedLarge: updateMobilePermalinks(req.mobile, feedLarge),
      feedSmall: updateMobilePermalinks(req.mobile, feedSmall),
      curated: updateMobilePermalinks(req.mobile, curated),
    },
  });
};

const personalizedHandler = async (req, res) => {
  const premium = await isPremium(req.authenticatedUserId);
  const searchTargetedOnly = req.query.targetedOnly || null;
  let targetedSearchAfter;
  try {
    targetedSearchAfter = parseSearchAfter(req.query.targetedStart, 'targeted');
  } catch (err) {
    return res.status(400).send({ message: err.message });
  }

  let taxonomy = [];
  if (req.query.subregionIds) {
    const subregionIds = req.query.subregionIds.split(',');
    const results = await Promise.all(subregionIds.map(id => getTaxonomy('subregion', id)));
    results.forEach(result => {
      taxonomy.push({
        _id: result._id,
        name: result.name,
        type: result.type,
        subregion: result.subregion,
      });
      result.in.forEach(item => {
        taxonomy.push({
          _id: item._id,
          name: item.name,
          type: item.type,
        });
      });
    });
  } else if (req.authenticatedUserId) {
    taxonomy = await personalizedTaxonomy(req.authenticatedUserId);
  }

  const taxonomyIds = taxonomy.map(({ _id }) => _id);
  const size = Math.min(req.query.limit || 50, 50);

  let editorsPick;
  let trending;
  let targeted;
  let promoted = [];
  let showDefault = false;

  if (searchTargetedOnly) {
    targeted =
      taxonomyIds.length === 0
        ? []
        : await searchTargetedFeedArticles(taxonomyIds, size, targetedSearchAfter, premium, true);
  } else {
    [editorsPick, trending, targeted] = await Promise.all([
      targetedSearchAfter ? [] : searchEditorsPickFeedArticles(5, premium),
      targetedSearchAfter ? [] : searchTrendingFeedArticles(5, premium),
      taxonomyIds.length === 0
        ? []
        : searchTargetedFeedArticles(taxonomyIds, size, targetedSearchAfter, premium),
    ]);

    promoted = combinePromotedArray(editorsPick, trending);

    const sevenDays = 7 * 24 * 60 * 60;
    showDefault =
      !searchTargetedOnly &&
      targeted.filter(article => Math.floor(new Date() / 1000) - article.updatedAt < sevenDays)
        .length === 0;
  }

  const defaultPromotionsToFilterOut = ['EDITORS_PICK', 'PROMO_BOX', 'TRENDING'];
  const targetedOrDefault = showDefault
    ? await searchPromotedFeedArticles(
        size,
        targetedSearchAfter,
        premium,
        'CURATED',
        null,
        defaultPromotionsToFilterOut,
      )
    : targeted;
  const lastTargeted = targetedOrDefault[targetedOrDefault.length - 1];
  const subregions = taxonomy
    .filter(({ type }) => type === 'subregion')
    .map(({ subregion, name }) => ({ _id: subregion, name }));

  return res.send({
    associated: {
      nextTargetedStart: lastTargeted ? lastTargeted.nextPageStart : null,
      feedType: showDefault ? 'DEFAULT' : 'TARGETED',
      subregions,
    },
    data: {
      promoted: updateMobilePermalinks(req.mobile, promoted),
      targeted: updateMobilePermalinks(req.mobile, targetedOrDefault),
    },
  });
};

const regionalHandler = async (req, res) => {
  const { subregionId, includeLocalNews } = req.query;
  if (!subregionId) return res.status(400).send({ message: 'subregionId required' });

  let searchAfter;

  try {
    searchAfter = parseSearchAfter(req.query.start);
  } catch (err) {
    return res.status(400).send({ message: err.message });
  }

  const [taxonomy, premium] = await Promise.all([
    regionalTaxonomy(subregionId),
    isPremium(req.authenticatedUserId, req.headers['x-api-key']),
  ]);

  const taxonomyIds = taxonomy.map(({ _id }) => _id);
  const filter = req.query.filter || null;
  const size = Math.min(req.query.limit || 50, 50);

  /*
  Below for temporary to support ios version 5.5.1
  Remove after ios 5.5.1 is considered deprecated
  Refer to backlog ticket KBYG-1225
  */
  let bestBetPost;

  if (!searchAfter && filter && filter.indexOf('realtimeforecast') > -1) {
    const bestBetBase = {
      id: 'bestbetsfromforecast',
      contentType: 'FORECAST',
      promoted: ['REGIONAL'],
      createdAt: null,
      updatedAt: null,
      premium: true,
      externalSource: null,
      externalLink: null,
      newWindow: false,
      content: {
        title: 'Best / Worst Bets',
        subtitle: null,
        body: null,
      },
      permalink: null,
      media: null,
      author: {
        name: null,
        iconUrl: null,
      },
      tags: [],
      keywords: ['realtimeforecast'],
      menuOrder: null,
      nextPageStart: null,
    };

    const regionOverview = await getRegionalOverview(subregionId);
    const summary = regionOverview && regionOverview.summary ? regionOverview.summary : null;
    const bets = summary && summary.bets ? summary.bets : null;
    const hasBestBets = bets.best || bets.worst;

    if (hasBestBets) {
      const subregionName = summary.subregionName || 'this region';
      const forecastDate = summary.forecastDate || null;
      const forecaster = summary.forecaster || null;

      const bestBet = bets.best ? `<p><b>Best Bet</b><br />${bets.best}</p>` : null;
      const worstBet = bets.worst ? `<p><b>Worst Bet</b><br />${bets.worst}</p>` : null;
      bestBetPost = bestBetBase;
      bestBetPost.content.title = `Best Bets for ${subregionName}`;
      bestBetPost.content.body = `${bestBet} ${worstBet}`;
      bestBetPost.createdAt = forecastDate;
      bestBetPost.updatedAt = forecastDate;
      bestBetPost.author.name = forecaster.name;
    }
  }
  /*
  Above to support ios version 5.5.1
  ticket KBYG-1225
  */

  const regionalFeedSearchModel = includeLocalNews
    ? searchForecastFeedArticles
    : searchRegionalFeedArticles;

  const regional = await regionalFeedSearchModel(taxonomyIds, filter, size, searchAfter, premium);
  const keywords = await aggregateRegionalFeedKeywords(taxonomyIds);

  const lastRegional = regional[regional.length - 1];

  /*
  ios 5.5.1 support -> KBYG-1225
  */
  if (bestBetPost) {
    regional.unshift(bestBetPost);
  }

  return res.send({
    associated: {
      nextStart: lastRegional ? lastRegional.nextPageStart : null,
      keywords,
    },
    data: {
      regional: updateMobilePermalinks(req.mobile, regional),
    },
  });
};

const promoboxHandler = async (req, res) => {
  const premium = await isPremium(req.authenticatedUserId);
  const { geotarget, limit } = req.query;
  const size = Math.min(limit || 50, 50);
  let promoted = 'PROMO_BOX';
  if (geotarget === 'AU') {
    promoted = 'AU_PROMO_BOX';
  } else if (geotarget === 'NZ') {
    promoted = 'NZ_PROMO_BOX';
  }
  let promobox = await searchPromoboxFeedArticles(size, premium, promoted);
  promobox = buildHeadline(promobox, geotarget);
  return res.send({
    data: {
      promobox: updateMobilePermalinks(req.mobile, promobox),
    },
  });
};

const localHandler = async (req, res) => {
  if (!req.query.id) return res.status(400).send({ message: 'Valid article ID is required' });
  const premium = await isPremium(req.authenticatedUserId);

  let article;
  try {
    article = await getLocalFeedArticle(req.query.id, premium);
  } catch (err) {
    if (err.status === 404)
      return res.status(400).send({ message: 'Valid article ID is required' });
    throw err;
  }

  return res.send({
    data: {
      article,
    },
  });
};

const eventsHandler = async (req, res) => {
  // Always return premium for mobile
  const premium = req.mobile || (await isPremium(req.authenticatedUserId));
  let events = [];
  try {
    events = await buildEvents(premium);
  } catch (error) {
    newrelic.noticeError(error);
  }
  return res.send({
    events,
  });
};

const eventHandler = async (req, res) => {
  // Always return premium for mobile
  const premium = req.mobile || (await isPremium(req.authenticatedUserId));
  const event = await buildEvent(req.params.id, premium);
  return res.send({
    event,
  });
};

export {
  curatedHandler,
  localHandler,
  personalizedHandler,
  promoboxHandler,
  regionalHandler,
  trackFeedRequests,
  eventsHandler,
  eventHandler,
};
