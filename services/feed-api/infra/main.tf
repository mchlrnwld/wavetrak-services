module "feed_api" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = var.company
  application  = var.application
  environment  = var.environment
  service_name = var.application

  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/health"
  service_td_name             = "sl-core-svc-${var.environment}-${var.application}"
  service_td_count            = var.service_td_count
  service_td_container_name   = var.application
  service_port                = 8080
  service_alb_priority        = 240

  alb_listener      = var.alb_listener_arn
  iam_role_arn      = var.iam_role_arn
  load_balancer_arn = var.load_balancer_arn

  dns_name    = var.dns_name
  dns_zone_id = var.dns_zone_id
  default_vpc = var.default_vpc
  ecs_cluster = var.ecs_cluster

  auto_scaling_enabled        = var.auto_scaling_enabled
  auto_scaling_scale_by       = var.auto_scaling_scale_by
  auto_scaling_min_size       = var.auto_scaling_min_size
  auto_scaling_max_size       = var.auto_scaling_max_size
  auto_scaling_target_value   = var.auto_scaling_target_value
  auto_scaling_alb_arn_suffix = "${element(split("/", var.load_balancer_arn), 1)}/${element(split("/", var.load_balancer_arn), 2)}/${element(split("/", var.load_balancer_arn), 3)}"
}
