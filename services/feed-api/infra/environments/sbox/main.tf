provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/feed-api/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

# This data block and the next two resources are needed here because of how
# resources have been created before. They could safely be moved to the main
# module file once executed in all environments.
locals {
  dns_name          = "feed-api.sandbox.surfline.com"
  elasticsearch_arn = "arn:aws:es:us-west-1:665294954271:domain/sl-es-sbox"
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-2-sandbox"
}

resource "aws_iam_policy" "feed_api_es_policy" {
  name        = "sl-feed-api-feed-api-es-policy-sandbox"
  description = "policy to access elasticsearch from feed api"
  policy = templatefile("${path.module}/../../resources/elasticsearch-policy.json", {
    elasticsearch_arn = local.elasticsearch_arn
  })
}

resource "aws_iam_role_policy_attachment" "feed_api_task_arn" {
  role       = "feed_api_task_role_sandbox"
  policy_arn = aws_iam_policy.feed_api_es_policy.arn
}

module "feed-api" {
  source = "../../"

  company     = "sl"
  application = "feed-api"
  environment = "sandbox"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-sandbox"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "path-pattern"
      value = "/feed*"
    },
  ]
  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-2-sandbox/224dfd1fa8567f7e/2270815960a67034"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = data.aws_alb.main_internal.arn
  elasticsearch_arn = local.elasticsearch_arn

  dns_name    = local.dns_name
  dns_zone_id = "Z3DM6R3JR1RYXV"

  auto_scaling_enabled = false
}
