provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/feed-api/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

# This locals, data and the next two resources blocks are needed here because
# of how resources have been created before. They could safely be moved to the
# main module file once executed in all environments.
locals {
  dns_name          = "feed-api.prod.surfline.com"
  elasticsearch_arn = "arn:aws:es:us-west-1:833713747344:domain/sl-es-prod"
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-2-prod"
}

resource "aws_iam_policy" "feed_api_es_policy" {
  name        = "sl-feed-api-feed-api-es-policy-prod"
  description = "policy to access elasticsearch from feed api"
  policy = templatefile("${path.module}/../../resources/elasticsearch-policy.json", {
    elasticsearch_arn = local.elasticsearch_arn
  })
}

resource "aws_iam_role_policy_attachment" "feed_api_task_arn" {
  role       = "feed_api_task_role_prod"
  policy_arn = aws_iam_policy.feed_api_es_policy.arn
}

module "feed-api" {
  source = "../../"

  company     = "sl"
  application = "feed-api"
  environment = "prod"

  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  service_td_count = 10
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "path-pattern"
      value = "/feed*"
    },
  ]
  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-2-prod/99e5fd75fdbf20fd/b668bfe144c94994"
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn = data.aws_alb.main_internal.arn
  elasticsearch_arn = local.elasticsearch_arn

  dns_name    = local.dns_name
  dns_zone_id = "Z3LLOZIY0ZZQDE"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 200
  auto_scaling_min_size     = 2
  auto_scaling_max_size     = 5000
}
