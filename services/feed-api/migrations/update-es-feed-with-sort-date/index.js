/* eslint-disable no-console */
import dotenv from 'dotenv';
import fetch from 'node-fetch';
import AWS from 'aws-sdk';

dotenv.config();

const awsConfig = new AWS.Config({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION,
});

const getCredentials = config => new Promise((resolve, reject) =>
  config.getCredentials((err, credentials) => {
    if (err) return reject(err);
    return resolve(credentials);
  }));

const baseFetchSignedAWSESRequest = async (url, body) => {
  const endpoint = new AWS.Endpoint(url);
  const request = new AWS.HttpRequest(endpoint);
  request.region = awsConfig.region;
  request.headers = {
    Host: endpoint.host,
    'Content-Type': 'application/json',
  };
  request.body = body;
  const signer = new AWS.Signers.V4(request, 'es');
  signer.addAuthorization(await getCredentials(awsConfig), new Date());
  const response = await fetch(request.endpoint.href, {
    method: request.method,
    headers: request.headers,
    body: request.body,
  });
  const responseBody = await response.json();
  // eslint-disable-next-line yoda
  if (200 <= response.status && response.status < 300) return responseBody;
  throw responseBody;
};

const fetchFeedArticlesWithoutSortDate = async (scrollId = null) => {
  const scroll = '5m';
  const body = JSON.stringify(scrollId ? {
    scroll,
    scroll_id: scrollId,
  } : {
    query: {
      bool: {
        must_not: [
          {
            exists: {
              field: 'sortDate',
            },
          },
        ],
      },
    },
    sort: ['_doc'],
    _source: ['createdAt', 'updatedAt'],
  });
  const query = scrollId ? '' : `scroll=${scroll}`;
  const path = scrollId ? '/_search/scroll' : '/feed/_search';
  return baseFetchSignedAWSESRequest(
    `${process.env.ELASTICSEARCH_URL}${path}?${query}`,
    body,
  );
};

const setSortDate = async (_type, _id, sortDate) => baseFetchSignedAWSESRequest(
  `${process.env.ELASTICSEARCH_URL}/feed/${_type}/${_id}/_update`,
  JSON.stringify({
    doc: {
      sortDate,
    },
  }),
);

const main = async () => {
  const initialScrollSearch = await fetchFeedArticlesWithoutSortDate();
  // eslint-disable-next-line no-underscore-dangle
  const scrollId = initialScrollSearch._scroll_id;

  let feedArticles = initialScrollSearch.hits.hits;
  let articlesUpdated = 0;

  /* eslint-disable no-await-in-loop */
  while (feedArticles.length > 0) {
    const updates = await Promise.all(
      feedArticles.map(({ _type, _id, _source: { createdAt, updatedAt } }) =>
        setSortDate(_type, _id, _type === 'forecast' ? updatedAt : createdAt)),
    );
    articlesUpdated += feedArticles.length;
    console.log(updates);
    console.log(articlesUpdated);
    const scrollSearch = await fetchFeedArticlesWithoutSortDate(scrollId);
    feedArticles = scrollSearch.hits.hits;
  }
  /* eslint-enable no-await-in-loop */

  return articlesUpdated;
};

const start = new Date();
main().then((articlesUpdated) => {
  const end = new Date();
  console.log(`${articlesUpdated} updated in ${end - start}ms`);
  process.exit(0);
}).catch((err) => {
  console.error(err);
  process.exit(-1);
});
