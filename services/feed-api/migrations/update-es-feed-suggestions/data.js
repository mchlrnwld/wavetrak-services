/* eslint-disable no-console */
// import fs from 'fs';
import dotenv from 'dotenv';
import fetch from 'node-fetch';

dotenv.config();

const getPostTagData = async () => {
  const postTags = new Map();
  const limit = 100;
  let offset = 0;

  const initialResponse = await fetch(`https://${process.env.FETCH_PREFIX}.surfline.com/wp-json/sl/v1/post-tags-names?date=${new Date().getTime()}`);
  let results = await initialResponse.json();

  /* eslint-disable no-await-in-loop */
  while (results.length > 0) {
    const fetchUrl = `https://${process.env.FETCH_PREFIX}.surfline.com/wp-json/sl/v1/post-tags-names?limit=${limit}&offset=${offset}&date=${new Date().getTime()}`;
    console.log('fetching: ', fetchUrl);
    const response = await fetch(fetchUrl);
    results = await response.json();
    console.log('result length', results.length);
    results.map((res) => {
      if (res.id && res.tags) {
        postTags.set(res.id, res.tags);
      }
      return null;
    });
    console.log('OFFSET', offset);
    offset += 100;
  }
  return postTags;
};

export default getPostTagData;
