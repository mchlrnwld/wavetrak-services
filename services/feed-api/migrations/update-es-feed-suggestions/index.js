/* eslint-disable no-console */
import dotenv from 'dotenv';
import fetch from 'node-fetch';
import AWS from 'aws-sdk';
import getPostTagData from './data';

dotenv.config();

const awsConfig = new AWS.Config({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_DEFAULT_REGION,
});

const getCredentials = config => new Promise((resolve, reject) =>
  config.getCredentials((err, credentials) => {
    if (err) return reject(err);
    return resolve(credentials);
  }));

const baseFetchSignedAWSESRequest = async (url, body) => {
  const endpoint = new AWS.Endpoint(url);
  const request = new AWS.HttpRequest(endpoint);
  request.region = awsConfig.region;
  request.headers = {
    Host: endpoint.host,
    'Content-Type': 'application/json',
  };
  request.body = body;
  const signer = new AWS.Signers.V4(request, 'es');
  signer.addAuthorization(await getCredentials(awsConfig), new Date());
  const response = await fetch(request.endpoint.href, {
    method: request.method,
    headers: request.headers,
    body: request.body,
  });
  const responseBody = await response.json();
  // eslint-disable-next-line yoda
  if (200 <= response.status && response.status < 300) return responseBody;
  throw responseBody;
};

const fetchFeedArticlesWithoutSuggestTags = async (scrollId = null) => {
  const scroll = '5m';
  const body = JSON.stringify(scrollId ? {
    scroll,
    scroll_id: scrollId,
  } : {
    query: {
      bool: {
        must: [
          { term: { _type: 'editorial' } },
        ],
        must_not: [
          {
            exists: {
              field: 'suggestTags',
            },
          },
        ],
      },
    },
    sort: ['_doc'],
    _source: ['articleId'],
  });
  const query = scrollId ? '' : `scroll=${scroll}`;
  const path = scrollId ? '/_search/scroll' : '/feed/_search';
  return baseFetchSignedAWSESRequest(
    `${process.env.ELASTICSEARCH_URL}${path}?${query}`,
    body,
  );
};

const setSuggestTags = async (_type, _id, suggestTags) => baseFetchSignedAWSESRequest(
    `${process.env.ELASTICSEARCH_URL}/feed/${_type}/${_id}/_update`,
    JSON.stringify({
      doc: {
        suggestTags,
      },
    }),
  );

const main = async () => {
  const postTagData = await getPostTagData();
  console.log('size', postTagData.size);

  const initialScrollSearch = await fetchFeedArticlesWithoutSuggestTags();
  // eslint-disable-next-line no-underscore-dangle
  const scrollId = initialScrollSearch._scroll_id;

  let feedArticles = initialScrollSearch.hits.hits;
  let totalArticles = 0;
  let articlesUpdated = 0;
  let articlesSkipped = 0;
  let suggestTags;

  /* eslint-disable no-await-in-loop */
  while (feedArticles.length > 0) {
    console.log('length', feedArticles.length);

    /* eslint-disable-next-line */
    feedArticles.map(({ _id, _type, _source: { articleId } }) => {
      if (articleId && postTagData.has(articleId)) {
        suggestTags = postTagData.get(articleId);
        console.log('**updating doc: ', articleId, suggestTags);
        setSuggestTags(_type, _id, suggestTags);
        articlesUpdated += 1;
      } else {
        console.log('skipping doc: ', articleId);
        articlesSkipped += 1;
      }
    });
    totalArticles += feedArticles.length;
    const scrollSearch = await fetchFeedArticlesWithoutSuggestTags(scrollId);
    feedArticles = scrollSearch.hits.hits;
  }
  console.log(articlesUpdated);
  /* eslint-enable no-await-in-loop */
  return {
    articlesUpdated,
    articlesSkipped,
    totalArticles,
  };
};

const start = new Date();
main().then((articleResults) => {
  const end = new Date();
  const { articlesUpdated, articlesSkipped, totalArticles } = articleResults;
  console.log(`${articlesUpdated}/${totalArticles} articles updated and ${articlesSkipped}/${totalArticles} articles skipped in ${end - start}ms`);
  process.exit(0);
}).catch((err) => {
  console.error(err);
  process.exit(-1);
});
