import sinon, { SinonStub } from 'sinon';
import chai from 'chai';
import 'chai-http';
import * as servicesCommon from '@surfline/services-common';
import server from './server';

const { expect } = chai;

describe('server', () => {
  beforeEach(() => {
    sinon.spy(servicesCommon, 'setupExpress');
  });

  afterEach(() => {
    (servicesCommon.setupExpress as SinonStub).resetHistory();
  });

  it('should setup the server with a health endpoint', async () => {
    const { app } = server();
    expect(app).to.exist();
    const request = chai.request(app);

    const response = await request.get('/health');

    expect(servicesCommon.setupExpress).to.have.been.calledOnce();
    expect(response).to.have.status(200);
    expect(response.body.status).to.equal(200);
    expect(response.body.message).to.equal('OK');
    expect(response.body.version).to.exist();
  });
});
