import nr from 'newrelic';

export default {
  requestDidStart: (): object => ({
    executionDidStart: (rc): void => {
      if (rc.request.operationName) {
        nr.setTransactionName(`operation:${rc.request.operationName}`);
      }
      nr.addCustomAttribute('query', rc.request.query);
      if (rc.request.variables) {
        nr.addCustomAttribute('variables', JSON.stringify(rc.request.variables));
      }
    },
    didEncounterErrors: (rc): void => {
      nr.noticeError(rc.errors[0].message);
    },
  }),
};
