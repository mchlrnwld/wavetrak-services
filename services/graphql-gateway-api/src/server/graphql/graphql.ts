import { ApolloGateway } from '@apollo/gateway';
import { ApolloServer } from 'apollo-server-express';
import newRelicPlugin from './newRelicPlugin';
import WavetrakGraphQLDataSource from './wavetrakGraphQLDataSource';
import config from '../../config';

const gateway = new ApolloGateway({
  serviceList: [
    { name: 'science-data-service', url: `${config.SCIENCE_DATA_SERVICE}/graphql` },
    { name: 'spots-api', url: `${config.SPOTS_API}/graphql` },
  ],
  buildService: ({ url }): WavetrakGraphQLDataSource => new WavetrakGraphQLDataSource({ url }),
});

export default new ApolloServer({
  gateway,
  subscriptions: false,
  plugins: [newRelicPlugin],
  context: ({ req }): object => ({ requestHeaders: req.headers }),
});
