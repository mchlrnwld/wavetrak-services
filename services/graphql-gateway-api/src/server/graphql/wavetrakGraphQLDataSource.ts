import { RemoteGraphQLDataSource } from '@apollo/gateway';

export default class WavetrakGraphQLDataSource extends RemoteGraphQLDataSource {
  willSendRequest({ request, context }): void {
    if (context.requestHeaders && context.requestHeaders['cache-control']) {
      request.http.headers.set('cache-control', context.requestHeaders['cache-control']);
    }
  }
}
