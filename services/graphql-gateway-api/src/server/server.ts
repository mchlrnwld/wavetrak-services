import { Express } from 'express';
import { Server } from 'http';
import { setupExpress } from '@surfline/services-common';
import log from '../common/logger';
import trackRequests from '../common/trackRequests';
import config from '../config';
import graphql from './graphql';

const server = (): { app: Express; server: Server } =>
  setupExpress({
    log,
    port: config.EXPRESS_PORT as number,
    name: 'graphql-gateway',
    allowedMethods: ['GET', 'OPTIONS', 'POST'],
    handlers: [
      ['*', trackRequests],
    ],
    callback: app => {
      graphql.applyMiddleware({ app });
    },
  });

export default server;
