/* istanbul ignore file */
import 'newrelic';
import app from './server';
import log from './common/logger';

const start = async (): Promise<void> => {
  try {
    app();
  } catch (err) {
    log.error({
      message: `App Initialization failed: ${err.message}`,
      stack: err,
    });
  }
};

start();
