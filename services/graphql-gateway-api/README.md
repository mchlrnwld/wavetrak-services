# GraphQL Gateway

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Description](#description)
- [Development](#development)
- [Testing](#testing)
- [Adding Services](#adding-services)
- [Service Validation](#service-validation)
- [More Information](#more-information)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Description

The GraphQL Gateway service distributes GraphQL operations across multiple implementing services.

## Development

```bash
nvm use # honors .nvmrc
cp .env.sample .env
# fill out the missing env variables
npm install
npm run startlocal
```

## Testing

This service requires coverage to stay above 95 percent on all metrics. Functions that have code that cannot be stubbed or tested should be ignored using the `/* istanbul ignore next */` or `/* istanbul ignore file */` comments.

Tests can be run using either of the following commands:

```sh
npm run test
```

```sh
npm run coverage
```

## Adding Services

1. Add an environment variable referencing the service to the following files:

- `.env`
- `.env.sample`
- `.env.test`
- `ci.json`
- `src/config.ts`

2. Add the service to `src/server/graphql/graphql.ts`.

Because `http://service.domain.com/graphql` is a common pattern and we have shared environment variables containing service URLs, the default way of referencing a service is to use the shared service environment variable and append `/graphql` in `graphql.ts`. See `science-data-service` as an example.

If you're referencing a GraphQL service that doesn't follow this pattern, you may define a `SERVICE_NAME_GRAPHQL_ENDPOINT` environment variable that contains the full service URL including the GraphQL endpoint.

## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `graphql-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbncwTXpJMk16STFPVGcifQ==&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53ME16STJNekkxT1RnIiwic2VsZWN0ZWROZXJkbGV0Ijp7Im5lcmRsZXRJZCI6ImFwbS1uZXJkbGV0cy5vdmVydmlldyJ9fQ==&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests that execute queries against the graph.

**Ex:**

Test the following query via a `POST` request against the graph:

```
curl -i \
        -H 'Content-Type: application/json' \
        --data-binary '{"query": "query ($agency: String!, $model: String!) { models(agency: $agency, model: $model) { runs(types: POINT_OF_INTEREST, statuses: ONLINE) { run } } }","variables": { "agency": "Wavetrak", "model": "Lotus-WW3" } }' \
        'https://services.sandbox.surfline.com/graphql?api_key=temporary_and_insecure_wavetrak_api_key'
```

**Expected response:** The above query should return a response that looks similar to:

```
HTTP/2 200
date: Tue, 24 Nov 2020 03:32:06 GMT
content-type: application/json; charset=utf-8
server: openresty/1.13.6.1
vary: Accept-Encoding
x-powered-by: Express
access-control-allow-origin: *
access-control-allow-headers: Origin, X-Requested-With, Content-Type, Accept, Credentials, Authorization
access-control-allow-credentials: true
access-control-allow-methods: GET, OPTIONS, POST
etag: W/"5a-6JPDVaimWlwR1xOJRXkehKjxfio"
content-encoding: gzip

{"data":{"models":[{"runs":[{"run":2020112306},{"run":2020112206},{"run":2020112106}]}]}}
```

## More Information

See [GraphQL at Wavetrak](https://wavetrak.atlassian.net/wiki/spaces/PLA/pages/900940071/GraphQL+at+Wavetrak) for more information.
