FROM node:14 as base
WORKDIR /opt/app
COPY .npmrc package.json package-lock.json ./
RUN npm ci --ignore-scripts --prefer-offline


FROM base AS test
WORKDIR /opt/app
COPY . .
RUN npm run lint && \
    npm run test


FROM test AS build
WORKDIR /opt/app
ARG APP_ENV=sandbox
ARG APP_VERSION=master
ENV NODE_ENV=$APP_ENV
ENV APP_VERSION=$APP_VERSION
RUN npm run dist && \
    npm ci --production --ignore-scripts --prefer-offline


FROM node:14-alpine
ARG APP_VERSION=master
ENV APP_VERSION=$APP_VERSION
WORKDIR /opt/app
COPY --from=build /opt/app/dist dist
COPY --from=build /opt/app/newrelic.js ./dist/newrelic.js
COPY --from=build /opt/app/node_modules /opt/app/node_modules

# execute yarn start equivalent. Prevent extra process calls
# https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md#cmd
CMD ["node", "dist/index.js"]
