variable "company" {
  default     = "wt"
  type        = string
  description = "The brand that this service will be used for. Wavetrak can be used for cross-brand services"
}

variable "application" {
  default     = "graphql-gateway"
  type        = string
  description = "Name of the service"
}

variable "environment" {
  type        = string
  description = "The environment in which the infrastructure is being created"
}

variable "default_vpc" {
  type        = string
  description = "The default VPC for the service"
}

variable "ecs_cluster" {
  type        = string
  description = "The ECS cluster that the service will sit on"
}

variable "alb_listener_arn" {
  type        = string
  description = "ARN for the ALB listener"
}

variable "service_td_count" {
  type        = string
  description = "The number of task definitions for the service"
}

variable "service_lb_rules" {
  type        = list(map(string))
  description = "List of rules for load balancer listener"
}

variable "iam_role_arn" {
  type        = string
  description = "ARN for the IAM role"
}

variable "dns_name" {
  type        = string
  description = "DNS Name for the service"
}

variable "dns_zone_id" {
  type        = string
  description = "DNS zone ID for the service"
}

variable "load_balancer_arn" {
  type        = string
  description = "ARN for the load balancer"
}

variable "auto_scaling_enabled" {
  default     = false
  type        = string
  description = "Boolean determining if auto-scaling is enabled"
}

variable "auto_scaling_scale_by" {
  default     = "alb_request_count"
  type        = string
  description = "Metric which the service should use to trigger scaling"
}

variable "auto_scaling_min_size" {
  default     = ""
  type        = string
  description = "Minimum number of tasks for an auto-scaling service"
}

variable "auto_scaling_max_size" {
  default     = ""
  type        = string
  description = "Maximum number of tasks for an auto-scaling service"
}

variable "auto_scaling_target_value" {
  default     = ""
  type        = string
  description = "The target value for auto-scaling"
}
