variable "company" {
  default = "sl"
}

variable "application" {
  default = "admin-spots-service"
}

variable "environment" {
}

variable "alb_listener_arn" {
}

variable "iam_role_arn" {
}

variable "load_balancer_arn" {
}

variable "default_vpc" {
}

variable "dns_zone" {
}

variable "container_name" {
  default = "spots"
}

variable "service_lb_healthcheck_path" {
  default = "/spots/health"
}

variable "service_lb_priority" {
  default = 240
}

variable "service_port" {
  default = 8080
}

variable "service_td_count" {
  default = 1
}

variable "task_deregistration_delay" {
  default = 60
}
