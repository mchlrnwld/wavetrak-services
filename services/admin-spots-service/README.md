# Admin Spots Service

This module contains _only_ the **infrastructure code** for the `admin-spots-service` formerly configured as part of the `surfline-admin-tools`.

The application code can be found at [surfline-admin/modules/spots].

## Naming Conventions

See [naming-conventions] for AWS resources.

## Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV={env} make plan
ENV={env} make apply
```

Available environments are:

- sandbox
- staging
- prod

[surfline-admin/modules/spots]: https://github.com/Surfline/surfline-admin/tree/master/modules/spots
[naming-conventions]: https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions
