provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/favorites-api/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-4-staging"
}

module "favorites-api" {
  source = "../../"

  company     = "sl"
  application = "favorites-api"
  environment = "staging"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-staging"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "favorites-api.staging.surfline.com"
    },
    {
      field = "path-pattern"
      value = "/admin/favorites*"
    },
    {
      field = "path-pattern"
      value = "/favorites*"
    },
  ]
  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-staging/26ee81426b4723db/a6bb3e305cea13f5"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn = data.aws_alb.main_internal.arn

  dns_name    = "favorites-api.staging.surfline.com"
  dns_zone_id = "Z3JHKQ8ELQG5UE"

  auto_scaling_enabled = false
}
