provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/favorites-api/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-4-prod"
}

module "favorites-api" {
  source = "../../"

  company     = "sl"
  application = "favorites-api"
  environment = "prod"

  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  service_td_count = 10
  service_lb_rules = [
    {
      field = "host-header"
      value = "favorites-api.prod.surfline.com"
    },
    {
      field = "path-pattern"
      value = "/admin/favorites*"
    },
    {
      field = "path-pattern"
      value = "/favorites*"
    },
  ]
  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-4-prod/689f354baf4e976c/d6a5a0222c6ea0c3"
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn = data.aws_alb.main_internal.arn

  dns_name    = "favorites-api.prod.surfline.com"
  dns_zone_id = "Z3LLOZIY0ZZQDE"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 200
  auto_scaling_min_size     = 4
  auto_scaling_max_size     = 5000
}
