import fetch from 'node-fetch';

/* eslint-disable import/prefer-default-export */
export const getSubregionsBySubregionIds = async (subregionIds) => {
  const url = `${process.env.SPOTS_API}/subregions/batch?subregionIds=${subregionIds}`;
  const response = await fetch(url);
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};
