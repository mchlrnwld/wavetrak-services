import fetch from 'node-fetch';

/* eslint-disable import/prefer-default-export */
export const getSpotReportsBySpotIds = async (spotIds) => {
  const url = `${process.env.KBYG_API}/kbyg/spots/batch`;
  const response = await fetch(url, {
    method: 'post',
    body: JSON.stringify({
      spotIds,
    }),
    headers: { 'Content-Type': 'application/json' },
  });
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};
