import { expect } from 'chai';
import SpotModel from './schema';

describe('SpotModel schema', () => {
  it('should be invalid if spotId is empty', () => {
    const newSpot = new SpotModel();
    newSpot.validate(err => (
      expect(err.error.spotId).to.exist
    ));
  });

  it('should be invalid if rank is empty', () => {
    const newSpot = new SpotModel();
    newSpot.validate(err => (
      expect(err.error.rank).to.exist
    ));
  });

  it('should should have kind of "Spot"', () => {
    const newSpot = new SpotModel();
    expect(newSpot.kind).to.equal('Spot');
  });
});
