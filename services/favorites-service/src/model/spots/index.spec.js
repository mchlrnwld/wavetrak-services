import Chai, { expect } from 'chai';
import { APIError } from '@surfline/services-common';
import sinon from 'sinon';
import 'sinon-as-promised';
import chaiAsPromised from 'chai-as-promised';
import spotFixtures from './fixtures';
import SpotModel from './schema';
import * as Spots from './index';

Chai.use(chaiAsPromised);

describe('Spots Model', () => {
  const userId = '56fae6fe19b74318a73a7833';
  const spotIds = ['5842041f4e65fad6a7708955', '5842041f4e65fad6a7708954'];

  describe('getSpots', () => {
    beforeEach(() => {
      sinon.stub(SpotModel, 'find');
    });

    afterEach(() => {
      SpotModel.find.restore();
    });

    it('should return favorited spots for a given userId', async () => {
      SpotModel.find.returns({
        select: sinon.stub().returns({
          sort: sinon.stub().resolves(spotFixtures),
        }),
      });
      const result = await Spots.getSpots(userId);
      expect(result).to.deep.equal({ favorites: spotFixtures });
    });

    it('should return an empty if no spots are found', async () => {
      SpotModel.find.returns({
        select: sinon.stub().returns({
          sort: sinon.stub().resolves([]),
        }),
      });
      const result = await Spots.getSpots(userId);
      expect(result).to.deep.equal({ favorites: [] });
    });

    it('should error if user id is not passed', async () => {
      const err = new APIError('Cannot find spot with invalid userId');
      expect(Spots.getSpots()).to.be.rejectedWith(err);
    });
  });

  describe('addSpot', () => {
    beforeEach(() => {
      sinon.stub(SpotModel, 'findOne');
    });

    afterEach(() => {
      SpotModel.findOne.restore();
    });

    /* it('should add an new spot with the correct rank', async () => {
    });*/

    it('should error if spot id is not passed', async () => {
      const err = new APIError('Cannot add spot with empty spotIds');
      expect(Spots.addSpots()).to.be.rejectedWith(err);
    });
  });

  describe('deleteSpot', () => {
    beforeEach(() => {
      sinon.stub(SpotModel, 'deleteOne');
      sinon.stub(SpotModel, 'find');
    });

    afterEach(() => {
      SpotModel.deleteOne.restore();
      SpotModel.find.restore();
    });

    it('should delete a valid spot', async () => {
      SpotModel.deleteOne.resolves(spotFixtures[1]);
      SpotModel.find.returns({
        select: sinon.stub().returns({
          sort: sinon.stub().resolves([spotFixtures[0]]),
        }),
      });

      const result = await Spots.deleteSpots(userId, spotIds);
      expect(result).to.deep.equal({ favorites: [spotFixtures[0]] });
    });

    it('should error if spot id is not passed', async () => {
      const err = new APIError('Cannot delete spot with empty spotIds');
      expect(Spots.deleteSpots()).to.be.rejectedWith(err);
    });
  });

  describe('updateSpot,', () => {
    beforeEach(() => {
      sinon.stub(SpotModel, 'updateOne');
      sinon.stub(SpotModel, 'find');
    });

    afterEach(() => {
      SpotModel.updateOne.restore();
      SpotModel.find.restore();
    });

    it('should update a list of new spots', async () => {
      const updatedSpots = [
        {
          _id: '589115f5e833d25e9f1933a5',
          spotId: '5842041f4e65fad6a7708954',
          rank: 0,
          kind: 'Spot',
        },
        {
          _id: '587fdc0d3b01e6a94510ca68',
          spotId: '5842041f4e65fad6a7708955',
          rank: 1,
          kind: 'Spot',
        },
      ];
      SpotModel.updateOne.resolves();
      SpotModel.find.returns({
        select: sinon.stub().returns({
          sort: sinon.stub().resolves(updatedSpots),
        }),
      });
      const results = await Spots.updateSpots(userId, spotIds);
      expect(results).to.deep.equal({ favorites: updatedSpots });
    });

    it('should error if spot ids are not is passed', async () => {
      const err = new APIError('Cannot update with invalid spots');
      expect(Spots.updateSpots()).to.be.rejectedWith(err);
    });
  });
});
