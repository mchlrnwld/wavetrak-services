import mongoose from 'mongoose';
import { APIError } from '@surfline/services-common';
import SpotsModel from './schema';
import { track } from '../../common/analytics';

export const getSpots = async (userId) => {
  if (!userId) throw new APIError('Cannot find spot with invalid userId');

  const spots = await SpotsModel
    .find({ user: mongoose.Types.ObjectId(userId) })
    .select('spotId rank kind')
    .sort('rank');

  return { favorites: spots };
};


export const addSpots = async (userId, spotIds, platform) => {
  if (!spotIds.length) throw new APIError('Cannot add spot with empty spotIds');

  const lastFavorited = await SpotsModel
    .findOne({ user: mongoose.Types.ObjectId(userId) })
    .select('rank')
    .sort('-rank');

  let rank = lastFavorited ? lastFavorited.rank + 1 : 0;

  for (const spotId of spotIds) {
    const spot = new SpotsModel({
      user: mongoose.Types.ObjectId(userId),
      spotId: mongoose.Types.ObjectId(spotId),
      rank,
    });
    rank += 1;
    track('Edited Favorites', userId, {
      action: 'add',
      favoriteType: 'spot',
      spotId,
      platform,
    }, 'sl');
    await spot.save();
  }

  const userFavorites = await getSpots(userId);

  return userFavorites;
};


export const deleteSpots = async (userId, spotIds, platform) => {
  if (!spotIds.length) throw new APIError('Cannot delete with invalid spotId');

  for (const spotId of spotIds) {
    track('Edited Favorites', userId, {
      action: 'delete',
      favoriteType: 'spot',
      spotId,
      platform,
    }, 'sl');
    await SpotsModel.deleteOne({
      user: mongoose.Types.ObjectId(userId),
      spotId: mongoose.Types.ObjectId(spotId),
    });
  }

  const userFavorites = await getSpots(userId);

  return userFavorites;
};


export const updateSpots = async (userId, spotIds, platform) => {
  const { favorites } = await getSpots(userId);

  /**
   * Delete spots from a user's favorites if that spot no
   * longer exists in the Spots collection.
   */
  if (favorites.length !== spotIds.length) {
    for (const fav of favorites) {
      if (spotIds.indexOf(fav.spotId.toString()) === -1) {
        await SpotsModel.deleteOne({
          user: mongoose.Types.ObjectId(userId),
          spotId: mongoose.Types.ObjectId(fav.spotId),
        });
      }
    }
  }

  const spotMap = new Map(spotIds.map(i => [spotIds.indexOf(i), i]));

  for (const [rank, spotId] of spotMap) {
    track('Edited Favorites', userId, {
      action: 'modify',
      favoriteType: 'spot',
      spotId,
      platform,
    }, 'sl');
    await SpotsModel.updateOne({ user: userId, spotId }, { $set: { rank } });
  }

  const userFavorites = await getSpots(userId);

  return userFavorites;
};
