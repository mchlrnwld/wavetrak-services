import mongoose from 'mongoose';
import Favorite from '../schema';

const options = { discriminatorKey: 'kind' };

const favoriteSpotSchema = new mongoose.Schema({
  spotId: {
    type: mongoose.Schema.Types.ObjectId,
    required: [true, 'SpotId is required'],
  },
  rank: {
    type: mongoose.Schema.Types.Number,
    required: [true, 'Rank is required'],
    default: 0,
  },
}, options);

favoriteSpotSchema.index({ user: 1, spotId: 1 }, { unique: true });

export default Favorite.discriminator('Spot', favoriteSpotSchema);
