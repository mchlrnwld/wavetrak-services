import { expect } from 'chai';
import Favorite from './schema';

describe('Favorite schema', () => {
  it('should be invalid if userId is empty', () => {
    const newFavorite = new Favorite();
    newFavorite.validate(err => expect(err.error.userId).to.exist);
  });

  it('should be invalid if userId is invalid', () => {
    const newFavorite = new Favorite({ userId: '1234' });
    newFavorite.validate(err => expect(err.error.userId).to.exist);
  });
});
