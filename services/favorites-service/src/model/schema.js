import mongoose from 'mongoose';

const options = {
  collection: process.env.MONGO_FAVORITES_COLLECTION || 'Favorites',
  timestamps: true,
  discriminatorKey: 'kind',
};

const favoriteSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'UserInfo',
    required: [true, 'User is required'],
  },
}, options);

favoriteSchema.index({ user: 1 });

export default mongoose.model('Favorite', favoriteSchema);
