import { expect } from 'chai';
import sinon from 'sinon';
import 'sinon-as-promised';
import tagFixtures from './fixtures';
import TagModel from './schema';
import * as Tags from './index';

describe('Tags Model', () => {
  const userId = '56fae6fe19b74318a73a7833';
//   const tagId = '5842041f4e65fad6a7708954';

  describe('getTags', () => {
    beforeEach(() => {
      sinon.stub(TagModel, 'find');
    });

    afterEach(() => {
      TagModel.find.restore();
    });

    it('should return favorited tags for a given userId', async () => {
      TagModel.find.resolves(tagFixtures);
      const result = await Tags.getTags(userId);
      expect(result).to.deep.equal({ favorites: tagFixtures });
    });

    it('should return an empty if no tags are found', async () => {
      TagModel.find.resolves([]);
      const result = await Tags.getTags(userId);
      expect(result).to.deep.equal({ favorites: [] });
    });
  });

  describe('addTag', () => {
    beforeEach(() => {
      sinon.stub(TagModel, 'findOne');
    });

    afterEach(() => {
      TagModel.findOne.restore();
    });

    /* it('should add an new tag', () => {
      TagModel.findOne.resolves(tagFixtures[0]);
      const result = Tags.addTag(userId, tagId);
      expect(result).to.deep.equal(tagFixtures[1]);
    });*/
  });

  describe('deleteTag', () => {
    beforeEach(() => {
      sinon.stub(TagModel, 'deleteOne');
      sinon.stub(Tags, 'getTags');
    });

    afterEach(() => {
      TagModel.deleteOne.restore();
      Tags.getTags.returns();
    });

    /* it('should delete a valid tag', () => {
      TagModel.deleteOne.resolves(tagFixtures[1]);
      Tags.getTags.resolves({ favorites: [tagFixtures[0]] });
      const result = Tags.deleteTag(userId, tagId);
      console.log(result);
      expect(result).to.deep.equal({ favorites: [tagFixtures[0]] });
    });*/
  });

  describe('updateTag,', () => {
    beforeEach(() => {
      sinon.stub(TagModel, 'find');
    });

    afterEach(() => {
      TagModel.find.restore();
    });
  });
});
