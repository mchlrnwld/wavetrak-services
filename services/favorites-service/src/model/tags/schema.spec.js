import { expect } from 'chai';
import TagModel from './schema';

describe('TagModel schema', () => {
  it('should be invalid if tagId is empty', () => {
    const newTag = new TagModel();
    newTag.validate(err => expect(err.error.tagId).to.exist);
  });

  it('should should have kind of "Tag"', () => {
    const newTag = new TagModel();
    expect(newTag.kind).to.equal('Tag');
  });
});
