import mongoose from 'mongoose';
import Favorite from '../schema';

const options = { discriminatorKey: 'kind' };

const favoriteTagSchema = new mongoose.Schema({
  tagId: {
    type: mongoose.Schema.Types.ObjectId,
    required: [true, 'tagId is required'],
  },
}, options);

favoriteTagSchema.index({ user: 1, tagId: 1 }, { unique: true });

export default Favorite.discriminator('Tag', favoriteTagSchema);
