import mongoose from 'mongoose';
import { APIError } from '@surfline/services-common';
import TagsModel from './schema';

export const getTags = async (userId) => {
  if (!userId) throw new APIError('Cannot find tag with invalid userId');

  const tags = await TagsModel.find(
    { user: mongoose.Types.ObjectId(userId) },
    { tagId: 1, kind: 1 }
  );

  return { favorites: tags };
};


export const addTag = async (userId, tagId) => {
  if (!tagId) throw new APIError('Cannot add tag with invalid tagId');
  const tag = new TagsModel({
    user: mongoose.Types.ObjectId(userId),
    tagId: mongoose.Types.ObjectId(tagId),
  });

  const newTag = await tag.save();
  return newTag;
};


export const deleteTag = async (userId, tagId) => {
  if (!tagId) throw new APIError('Cannot delete with invalid tagId');

  await TagsModel.deleteOne({
    user: mongoose.Types.ObjectId(userId),
    tagId: mongoose.Types.ObjectId(tagId),
  });

  return await getTags(userId);
};


export const updateTags = async (userId, tagIds) => {
  if (!tagIds) throw new APIError('Cannot update with invalid tags');

  const tagMap = new Map(tagIds.map(i => [tagIds.indexOf(i), i]));
  for (const [rank, tagId] of tagMap) {
    await TagsModel.updateOne({ user: userId, tagId }, { $set: { rank } });
  }

  return getTags(userId);
};
