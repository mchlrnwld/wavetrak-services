import { Router } from 'express';
import { json } from 'body-parser';

export const trackRequests = log => (req, res, next) => {
  log.trace({
    action: '/favorites/collections',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const collections = (log) => {
  const api = Router();
  api.use('*', json(), trackRequests(log));

  return api;
};

export default collections;
