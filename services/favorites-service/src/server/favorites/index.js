import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import {
  getHandler,
  postHandler,
  deleteHandler,
  putHandler,
} from '../../handlers';
import collections from './collections';

export const trackRequests = log => (req, _, next) => {
  log.trace({
    action: '/favorites',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const favorites = (log) => {
  const api = Router();

  api.use(trackRequests(log));

  // Collections
  api.use('/collections', collections(log));

  // General favorites
  api.get('/', wrapErrors(getHandler));
  api.post('/', wrapErrors(postHandler));
  api.put('/', wrapErrors(putHandler));
  api.delete('/', wrapErrors(deleteHandler));

  return api;
};

export default favorites;
