import chai, { expect } from 'chai';
import { json } from 'body-parser';
import sinon from 'sinon';
import express from 'express';
import 'sinon-as-promised';
import * as SpotModel from '../../model/spots';
// import * as TagModel from '../../model/tags';
import favorites from './';

describe('/favorites', () => {
  let log;
  let request;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(json(), favorites(log));
    request = chai.request(app);
  });

  describe('Spots', () => {
    describe('GET Spots', () => {
      it('should return spots for a request user', async () => {
        try {
          const modelResponse = { favorites: [{ spotId: '1234', rank: 0 }] };
          sinon.stub(SpotModel, 'getSpots').resolves(modelResponse);
          const res = await request
            .get('/?type=spots')
            .set('x-auth-userId', '1234')
            .send();
          expect(res).to.have.status(200);
        } finally {
          SpotModel.getSpots.restore();
        }
      });
    });

    describe('POST Spot', () => {
      it('should return the new spot for a request user', async () => {
        try {
          const modelResponse = { spotId: '1234' };
          sinon.stub(SpotModel, 'addSpots').resolves(modelResponse);
          const res = await request
            .post('/')
            .set('x-auth-userId', '1234')
            .send({ type: 'spots', spotId: '1234' });
          expect(res).to.have.status(200);
        } finally {
          SpotModel.addSpots.restore();
        }
      });
    });

    describe('DELETE Spot', () => {
      it('should return the new spot for a request user', async () => {
        try {
          const modelResponse = { favorites: [{ spotId: '1234', rank: 0 }] };
          sinon.stub(SpotModel, 'deleteSpots').resolves(modelResponse);
          const res = await request
            .delete('/')
            .set('x-auth-userId', '1234')
            .send({ type: 'spots', spotId: '1234' });
          expect(res).to.have.status(200);
        } finally {
          SpotModel.deleteSpots.restore();
        }
      });
    });

    describe('PUT Spot', () => {
      it('should return the new spot for a request user', async () => {
        try {
          const modelResponse = { favorites: [{ spotId: '1234', rank: 0 }] };
          sinon.stub(SpotModel, 'updateSpots').resolves(modelResponse);
          const res = await request
            .put('/')
            .set('x-auth-userId', '1234')
            .send({ type: 'spots', spotIds: ['1234', '5678'] });
          expect(res).to.have.status(200);
        } finally {
          SpotModel.updateSpots.restore();
        }
      });
    });
  });

  // describe('Tags', () => {
  //   describe('GET Tags', () => {
  //     it('should return tags for a request user', async () => {
  //       try {
  //         const modelResponse = { favorites: [{ tagId: '1234' }] };
  //         sinon.stub(TagModel, 'getTags').resolves(modelResponse);
  //         const res = await request
  //           .get('/?type=tags')
  //           .set('x-auth-userId', '1234')
  //           .send();
  //         expect(res).to.have.status(200);
  //       } finally {
  //         TagModel.getTags.restore();
  //       }
  //     });
  //   });

  //   describe('POST Tag', () => {
  //     it('should return the new tag for a request user', async () => {
  //       try {
  //         const modelResponse = { tagId: '1234' };
  //         sinon.stub(TagModel, 'addTag').resolves(modelResponse);
  //         const res = await request
  //           .post('/')
  //           .set('x-auth-userId', '1234')
  //           .send({ type: 'tags', tagId: '1234' });
  //         expect(res).to.have.status(200);
  //       } finally {
  //         TagModel.addTag.restore();
  //       }
  //     });
  //   });

  //   describe('DELETE Tag', () => {
  //     it('should return the new tag for a request user', async () => {
  //       try {
  //         const modelResponse = { favorites: [{ tagId: '1234' }] };
  //         sinon.stub(TagModel, 'deleteTag').resolves(modelResponse);
  //         const res = await request
  //           .delete('/')
  //           .set('x-auth-userId', '1234')
  //           .send({ type: 'tags', tagId: '1234' });
  //         expect(res).to.have.status(200);
  //       } finally {
  //         TagModel.deleteTag.restore();
  //       }
  //     });
  //   });
  // });
});
