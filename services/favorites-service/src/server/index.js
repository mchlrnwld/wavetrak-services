import { setupExpress } from '@surfline/services-common';
import favorites from './favorites';
import admin from './admin';
import logger from '../common/logger';
import setPlatform from './middleware/setPlatform';

const log = logger('favorites-service');

// This was included in the app prior to upgrading to `setupExpress`
// so i've kept it here for consistency but we should likely remove
// it in the future
// TODO: remove this
const autoResolveOptions = (req, res, next) => {
  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
};

const setupApp = () =>
  setupExpress({
    port: process.env.EXPRESS_PORT || 8082,
    handlers: [
      autoResolveOptions,
      ['/favorites', setPlatform, favorites(log)],
      ['/admin/favorites', admin(log)],
    ],
    name: 'favorites-service',
    log,
    allowedMethods: ['GET', 'OPTIONS', 'DELETE', 'PUT', 'POST'],
  });

export default setupApp;
