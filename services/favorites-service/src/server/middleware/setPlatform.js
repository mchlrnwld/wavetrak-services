import userAgent from 'useragent';
import { wrapErrors } from '@surfline/services-common';

const setPlatform = async (req, _, next) => {
  const agent = userAgent.parse(req.headers['user-agent']);
  const os = agent.os.toString();
  let platform = 'web';

  if (os) {
    if (os.toLowerCase().indexOf('ios') !== -1) {
      platform = 'ios';
    } else if (os.toLowerCase().indexOf('android') !== -1) {
      platform = 'android';
    }
  }

  // eslint-disable-next-line no-param-reassign
  req.platform = platform;

  return next();
};

export default wrapErrors(setPlatform);
