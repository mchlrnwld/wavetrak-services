import { deleteSpots, getSpots } from '../../model/spots';

/**
 * @description Admin endpoint for handling GDPR requests.
 * This endpoint deletes the favorited spots for a user from Favorites collection
 *
*/
const deleteUserFavorites = async (req, res) => {
  const { userId } = req.params;
  if (!userId) {
    return res.status(400).send({ message: 'You need to specify an id' });
  }
  try {
    const { favorites } = await getSpots(userId);
    if (favorites.length === 0) return res.send({ message: 'No user favorites' });
    const spots = favorites.map(({ spotId }) => spotId);
    await deleteSpots(userId, spots);
    return res.send({ message: 'Favorites deleted successfully' });
  } catch (error) {
    return res.status(500).send({
      message: 'Could not delete user favorites',
      error,
    });
  }
};

export default deleteUserFavorites;
