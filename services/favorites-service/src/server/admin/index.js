import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import { getHandler } from '../../handlers';
import deleteUserFavorites from './favorites';

export const trackAdminRequests = log => (req, _, next) => {
  log.trace({
    action: '/admin',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

const admin = (log) => {
  const api = Router();

  api.use('*', trackAdminRequests(log));
  api.get('/', wrapErrors(getHandler));
  api.delete('/favorites/:userId', wrapErrors(deleteUserFavorites));
  return api;
};

export default admin;
