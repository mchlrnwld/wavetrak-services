import { getTags, addTag, deleteTag } from '../model/tags';

export const getTagsHandler = async (req, res) => {
  const tags = await getTags(req.authenticatedUserId);
  return res.status(200).send(tags);
};

export const addTagsHandler = async (req, res) => {
  const newTag = await addTag(req.authenticatedUserId, req.body.tagId);
  return res.status(200).send(newTag);
};

export const deleteTagsHandler = async (req, res) => {
  const tags = await deleteTag(req.authenticatedUserId, req.body.tagId);
  return res.status(200).send(tags);
};
