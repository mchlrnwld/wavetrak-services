import * as spots from './spots';
// import * as tags from './tags';

const mapping = {
  GET: {
    spots: spots.getSpotsHandler,
  },
  POST: {
    spots: spots.addSpotsHandler,
  },
  PUT: {
    spots: spots.updateSpotsHandler,
  },
  DELETE: {
    spots: spots.deleteSpotsHandler,
  },
};

const selectHandler = (action, type) => {
  const handler = mapping[action][type];
  if (handler) return handler;
  return async (req, res) => res.status(400).send({ message: 'Invalid type' });
};

/*
Commenting out tags endpoint from mapping until further notice
The use of the user + tag index conflicts with the user + spotId unique index
causes issues with conflicting indices in Mongo.
TODO: If reimplimenting tags, will require refactor.
Possible solution is to use unique and sparse index TBD

GET
tags: tags.getTagsHandler,

POST
tags: tags.addTagsHandler,

DELETE
tags: tags.deleteTagsHandler,
*/

export const getHandler = (req, res) => selectHandler('GET', req.query.type)(req, res);
export const postHandler = (req, res) => selectHandler('POST', req.body.type)(req, res);
export const putHandler = (req, res) => selectHandler('PUT', req.body.type)(req, res);
export const deleteHandler = (req, res) => selectHandler('DELETE', req.body.type)(req, res);
