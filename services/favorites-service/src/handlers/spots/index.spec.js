import { expect } from 'chai';
import sinon from 'sinon';
import 'sinon-as-promised';
import { mockReq, mockRes } from 'sinon-express-mock';
import * as Spots from '../../model/spots';
import * as SpotsHandlers from './index';
import spotFixtures from '../../model/spots/fixtures';

describe('Spots Handlers', () => {
  const userId = '56fae6fe19b74318a73a7833';
  const spotIds = ['842041f4e65fad6a7708954', '842041f4e65fad6a7708955'];

  describe('Get Spots handler', () => {
    beforeEach(() => {
      sinon.stub(Spots, 'getSpots');
    });

    afterEach(() => {
      Spots.getSpots.restore();
    });

    it('should return spots when passed a valid userId', async () => {
      Spots.getSpots.resolves({ favorites: spotFixtures });
      const req = mockReq({ authenticatedUserId: userId });
      const res = mockRes();
      await SpotsHandlers.getSpotsHandler(req, res);
      expect(res.send).to.be.calledWith({ favorites: spotFixtures });
    });
  });


  describe('Add Spots handler', () => {
    beforeEach(() => {
      sinon.stub(Spots, 'addSpots');
    });

    afterEach(() => {
      Spots.addSpots.restore();
    });

    it('should return a new spot when passed a valid userId', async () => {
      Spots.addSpots.resolves();
      const req = mockReq({
        authenticatedUserId: userId,
        body: { spotIds },
      });
      const res = mockRes();
      await SpotsHandlers.addSpotsHandler(req, res);
      expect(res.send).to.be.calledWith({
        message: 'Spots added successfully.',
      });
    });
  });


  describe('Update Spots handler', () => {
    beforeEach(() => {
      sinon.stub(Spots, 'updateSpots');
    });

    afterEach(() => {
      Spots.updateSpots.restore();
    });

    it('should return a new spot when passed a valid userId', async () => {
      Spots.updateSpots.resolves({ favorites: spotFixtures });
      const req = mockReq({
        authenticatedUserId: userId,
        body: { spotIds },
      });
      const res = mockRes();
      await SpotsHandlers.updateSpotsHandler(req, res);
      expect(res.send).to.be.calledWith({ message: 'Spots updated successfully.' });
    });
  });

  describe('Delete Spots handler', () => {
    beforeEach(() => {
      sinon.stub(Spots, 'deleteSpots');
    });

    afterEach(() => {
      Spots.deleteSpots.restore();
    });

    it('should return a new spot when passed a valid userId', async () => {
      Spots.deleteSpots.resolves({ favorites: [spotFixtures[0]] });
      const req = mockReq({
        authenticatedUserId: userId,
        body: { spotIds },
      });
      const res = mockRes();
      await SpotsHandlers.deleteSpotsHandler(req, res);
      expect(res.send).to.be.calledWith({ message: 'Spots deleted successfully.' });
    });
  });
});
