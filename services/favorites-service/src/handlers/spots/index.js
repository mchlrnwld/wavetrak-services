import { getSpots, addSpots, deleteSpots, updateSpots } from '../../model/spots';

export const getSpotsHandler = async (req, res) => {
  const spots = await getSpots(req.authenticatedUserId);
  return res.send(spots);
};

export const addSpotsHandler = async (req, res) => {
  await addSpots(req.authenticatedUserId, req.body.spotIds, req.platform);
  return res.send({ message: 'Spots added successfully.' });
};

export const deleteSpotsHandler = async (req, res) => {
  await deleteSpots(req.authenticatedUserId, req.body.spotIds, req.platform);
  return res.send({ message: 'Spots deleted successfully.' });
};

export const updateSpotsHandler = async (req, res) => {
  await updateSpots(req.authenticatedUserId, req.body.spotIds, req.platform);
  return res.send({ message: 'Spots updated successfully.' });
};
