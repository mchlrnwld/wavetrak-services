# Surfline User Favorites Micro-service

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


    - [QuickStart](#quickstart)
  - [Development](#development)
    - [Dependencies](#dependencies)
    - [Favoroite-Service Directory](#favoroite-service-directory)
      - [/common](#common)
      - [/error](#error)
      - [/handlers](#handlers)
      - [/model](#model)
      - [/server](#server)
- [API](#api)
      - [Table of Contents](#table-of-contents)
  - [Favorites](#favorites)
    - [Authentication](#authentication)
        - [Headers](#headers)
    - [Required Parameters](#required-parameters)
        - [`type`](#type)
  - [Spots](#spots)
        - [`type: "spots"`](#type-spots)
      - [The spots object](#the-spots-object)
    - [`GET /favorites/`](#get-favorites)
      - [Parameters](#parameters)
      - [Sample Request](#sample-request)
      - [Sample Response](#sample-response)
    - [`POST /favorites/`](#post-favorites)
      - [Parameters](#parameters-1)
      - [Sample Request](#sample-request-1)
      - [Sample Response](#sample-response-1)
    - [`DELETE /favorites/`](#delete-favorites)
      - [Parameters](#parameters-2)
      - [Sample Request](#sample-request-2)
      - [Sample Response](#sample-response-2)
    - [`PUT /favorites/`](#put-favorites)
      - [Parameters](#parameters-3)
      - [Sample Request](#sample-request-3)
      - [Sample Response](#sample-response-3)
    - [API Errors](#api-errors)
- [Service Validation](#service-validation)
  - [Prerequisite: Obtaining an X-Auth-UserID](#prerequisite-obtaining-an-x-auth-userid)
  - [Performing Validations](#performing-validations)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


### QuickStart
 1. `nvm use v6.9.2`
 2. `cat .env.sample >> .env && npm install && npm run startlocal`
 3. [localhost:8082/health](http://localhost:8082/health)

Skip to [API docs](#api)
## Development
### Dependencies
To run the locally, use docker to start your `Mongo` and `Logstash` instances.  Mongo should have at least a `UserDB` database defined.  The `Favorites` collection does not need to exist and does not need to be seeded with data. Although you should probably `POST` to the `/favorites` endpoint before you `GET`.  That will add the collection for you.  I'll add a `make` file to seed data if needed.
### Favoroite-Service Directory
```
 - /src
   | - /common
   | - /error
   | - /handlers
   | - /model
   | - /server
```
#### /common
Files included in the common directory should be generic and reusable throughout our suite of microservices.
#### /error
All errors thrown by the favorites service are defined within individual files named after their constructor type, and exported from `index.js`. **NOTE:** Try to make these errors as genaric as possible.  We may want to resuse this logic within other services.
#### /handlers
Handlers for this service are speficic to the `kind` of favorite being requested.  The root favorites-service handler delegates **REST**ful requests to for a these `kind`s by pulling a `type` value off either the query param or request body (depending on the request).

Each `kind` of handler function is responsible for calling the appropriate model functions and returning `200` level responses with contract defined data attached as json.  **We do not handle errors within handlers**.

File names should represent the `kind` of handlers included.  For example: functions specific to favoriting "Spots", should be defined in `handlers/spots.js`.
#### /model
Models included most of the business logic for this service.  At the root, we define the parent Favorites schema from which all other `kind`s inherit from.

Following a similar pattern found in `/handlers`, model subdirectories are separated by `kind`.  Each subdirectory should include the Mongoose schema, and **CRUD** functions that retrieving, transform and define data returned by this API.
#### /server
The favorites service API is exposed under two routes; `/admin/favorites` and `/favorites`.  Both routes are agnostic to the `kind` of favorite being requested.  Although `/admin` in mounted for internal user only, it requires the same authentication and called the same handlers as the publicly mounted `/favorites` routes.  _These files shouldn't need updating to support addition types of favorites_.


 - TBD - document `/src` directory, linting, and test runners.

# API
#### Table of Contents
 - [Favorites](##favorites)
 - [Spots](##spots)

## Favorites
This service is organized around REST, and provides a flexible yet predictable API that supports multiple type of favorited items.
### Authentication
##### Headers
Each request made to the favorites service is authenticated and requires a valid `X-Auth-AccessToken` or `X-Auth-UserId` to exist in the **request header**.  Unauthorized requests will respond with the following response.

```
// HTTP GET `/favorites`
// Response statusCode: 401 Unauthorized
{
  "message": "You are unauthorized to view this resource"
}
```
### Required Parameters
Since this API is build to handle many types of Favorites from a single endpoint, each request must specify which type of data it expects the response to return.
##### `type`
```
// Valid Request: GET
$ curl -X GET 'https://services.surfline.com/favorites?type=spots'

// Valid Request: POST, PUT, DELETE
$ curl -X POST 'https://services.surfline.com/favorites' \
  -d type=spots \
  **additional params**
```
```
// Error
$ curl -X POST 'https://services.surfline.com/favorites'

/* Response
HTTP 500 Internal Server Error
  {
	message: "Internal Server Error"
  }
*/

```

## Spots
##### `type: "spots"`
Spots allow you to store a user's favorited spots along with the order in which they've been ranked.
#### The spots object
| Attribute     | Type             | Description
| ------------- |----------------- |-----------
| _id           | String (ObjectId)| Unique Favorites identifier
| userId        | String (ObjectId)| User who created this favorite
| spotId        | String (ObjectId)| Representative KBYG Spot
| rank          | Number/Integer   | Order in which this spot is displayed
| kind          | String           | Type of favorite


### `GET /favorites/`
#### Parameters
| Parameter    | Required | Description
| ------------- |---------|-----------
| type          | Yes     | Type of favorites to return
#### Sample Request
```
$ curl -H 'X-Auth-AccessToken: 123456' \
	-X GET 'https://services.surfline.com/favorites/?type=spots'
```

#### Sample Response
```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
{
  "favorites": [
    {
      "_id": "5882591dc5eb5a6ec4511ab9",
      "spotId": "5842041f4e65fad6a7708954",
      "rank": 0,
      "kind": "Spot"
    },
    {
      "_id": "587fdc0d3b01e6a94510ca68",
      "spotId": "5842041f4e65fad6a7708955",
      "rank": 1,
      "kind": "Spot"
    }
  ]
}
```

### `POST /favorites/`
#### Parameters
| Parameter    | Required| Description
| -------------|---------|-----------
| type         | Yes     | Type of favorites to return
| spotIds      | Yes     | Array of spotIds to add.

**NOTE**: the new spot's `rank` will be assigned by the API.
#### Sample Request
```
$ curl -H 'X-Auth-AccessToken: 123456' \
	-X POST 'https://services.surfline.com/favorites/' \
	-d type=spots
	-d spotIds=['5842041f4e65fad6a7708954']
```

#### Sample Response
```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
{
  message: 'Spots added successfully.'
}
```
### `DELETE /favorites/`
#### Parameters
| Parameter    | Required| Description
| -------------|---------|-----------
| type         | Yes     | Type of favorites to return
| spotIds       | Yes     | Array of spotIds to remove.

**NOTE**: spot `rank` values will not be reassigned.
#### Sample Request
```
$ curl -H 'X-Auth-AccessToken: 123456' \
	-X DELETE 'https://services.surfline.com/favorites/' \
	-d type=spots
	-d spotIds=['5842041f4e65fad6a7708954']
```

#### Sample Response
```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
{
  message: 'Spots deleted successfully.'
}
```
### `PUT /favorites/`
#### Parameters
| Parameter    | Required| Description
| -------------|---------|-----------
| type         | Yes     | Type of favorites to return
| spotIds      | Yes     | Array of spots in the order to rank them in.

**NOTE**: spot `rank` values will be reassigned by the API, starting from 0 and incrementing by 1 for total of the spots included.
#### Sample Request
```
$ curl -H 'X-Auth-AccessToken: 123456' \
	-X PUT 'https://services.surfline.com/favorites/' \
	-d type=spots
	-d spotIds=['5842041f4e65fad6a7708955','5842041f4e65fad6a7708954']
```

#### Sample Response
```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
{
   message: 'Spots updated successfully.'
}
```
### API Errors
| StatusCode| Message | Triggered by
| ----------|---------|-----------
| 401 | "You are unauthorized to view this resource"|Invalid request headers
| 400 | "Invalid Parameters"| Ommitting `spotId` or other invalid, missing, or malformed parmeters.
| 500 | "Internal Server Error" | We messed up...

# Service Validation

## Prerequisite: Obtaining an X-Auth-UserID

An `X-Auth-UserId` must be passed in the header to perform the validation `curl` requests. This value corresponds to the `_id` field in the Mongo `UserDB.UserInfo` collection. For information on how to obtain this, continue reading this section.

You should create a registered account in the respective test environment if you don't already have one. You can do that by navigating to the Surfline homepage in the appropriate environment, and clicking `Sign In` > `Sign Up`.

Once you have a registered account, you should query the `UserDB.UserInfo` collection for the email that you used to register. Find the Mongo document, and copy the `_id` value. This will be the `X-Auth-UserId` that you use to perform the following verifications.


## Performing Validations

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `favorites-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd6TnpnMk5UYzNPQSJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53ek56ZzJOVGMzT0EiLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests on the endpoints detailed in the [API](#api) section.

**Ex:**

Test `GET` requests against the `favorites` endpoint:

```
curl \
    -X GET \
    -i \
    -H 'X-Auth-UserId: 5cb8c278a9680b000ffc68be' \
    http://favorites-api.sandbox.surfline.com/favorites?type=spots
```
**Expected response:** See [above](#sample-response) for sample response.