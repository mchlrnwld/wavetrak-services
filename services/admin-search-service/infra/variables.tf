variable "company" {
  default = "sl"
}

variable "application" {
  default = "admin-search-service"
}

variable "environment" {
}

variable "alb_listener_arn" {
}

variable "iam_role_arn" {
}

variable "load_balancer_arn" {
}

variable "target_group_name" {
  default = ""
}

variable "default_vpc" {
}

variable "dns_zone" {
}

variable "container_name" {
  default = "search"
}

variable "service_lb_healthcheck_path" {
  default = "/search/health"
}

variable "service_lb_priority" {
  default = 210
}

variable "service_port" {
  default = 8080
}

variable "service_td_count" {
  default = 1
}

variable "task_deregistration_delay" {
  default = 60
}
