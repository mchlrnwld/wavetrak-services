data "aws_route53_zone" "dns_zone" {
  name = var.dns_zone
}

module "ecs-service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company     = var.company
  application = var.application
  environment = var.environment

  default_vpc = var.default_vpc
  dns_zone_id = data.aws_route53_zone.dns_zone.zone_id
  dns_name    = "${var.application}.${var.dns_zone}"

  service_name = var.application
  service_port = var.service_port

  ecs_cluster               = "${var.company}-core-svc-${var.environment}"
  service_td_name           = "${var.company}-admin-${var.container_name}-service-${var.environment}"
  service_td_container_name = var.container_name
  service_td_count          = var.service_td_count

  service_alb_priority        = var.service_lb_priority
  service_lb_healthcheck_path = var.service_lb_healthcheck_path
  service_lb_rules = [
    {
      field = "host-header"
      value = "${var.application}.${var.dns_zone}"
    },
  ]

  alb_listener      = var.alb_listener_arn
  iam_role_arn      = var.iam_role_arn
  load_balancer_arn = var.load_balancer_arn

  auto_scaling_enabled        = false
  auto_scaling_scale_by       = "alb_request_count"
  auto_scaling_min_size       = var.service_td_count
  auto_scaling_max_size       = var.service_td_count
  auto_scaling_target_value   = ""
  auto_scaling_alb_arn_suffix = ""
}
