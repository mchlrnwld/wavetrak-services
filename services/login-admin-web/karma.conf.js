const webpack = require('webpack');

module.exports = (config) => {
  config.set({
    basePath: '',
    client: {
      mocha: {
        timeout: 60000, // 6 seconds - upped from 2 seconds
      },
    },
    browsers: ['ChromeCustom'],
    frameworks: ['mocha'],
    singleRun: true,
    files: ['tests.webpack.js'],
    reporters: ['mocha', 'coverage'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    concurrency: Infinity,
    browserNoActivityTimeout: 30000,
    customLaunchers: {
      ChromeCustom: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox'],
      },
    },
    preprocessors: {
      'tests.webpack.js': ['webpack', 'sourcemap'],
    },
    plugins: [
      'karma-chrome-launcher',
      'karma-webpack',
      'karma-mocha',
      'karma-mocha-reporter',
      'karma-sourcemap-loader',
      'karma-sourcemap-writer',
      'karma-coverage',
    ],
    webpack: {
      mode: 'development',
      resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
          sinon: 'sinon/pkg/sinon',
          react: require.resolve('react'),
        },
      },
      externals: [
        'react/addons',
        'react/lib/ExecutionEnvironment',
        'react/lib/ReactContext',
        'react-hot-loader/root',
      ],
      plugins: [
        new webpack.DefinePlugin({
          'process.env.NODE_ENV': JSON.stringify('development'),
          'process.env.APP_ENV': JSON.stringify('sandbox')
        }),
      ],
      devtool: 'inline-source-map',
      module: {
        rules: [
          {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: [
                  [
                    '@surfline/babel-preset-web/base',
                    {
                      // Webpack 4 and Babel 7 has changed the way modules are
                      // compiled which breaks sinon using CJS modules allows sinon to work properly
                      modules: 'cjs',
                    },
                  ],
                ],
                plugins: ['istanbul'],
              },
            },
          },
          {
            test: /\.css$/,
            use: ['style-loader', 'css-loader'],
          },
          {
            test: /.(png|woff(2)?|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
            use: 'url-loader',
          },
        ],
      },
    },
  });
};
