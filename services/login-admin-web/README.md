# Admin Tools - Login

Implements OAuth 2.0 Client Grant pattern. See `services/auth-service` for
additional details.

For information detailing active roles [https://wavetrak.atlassian.net/wiki/display/IE/OneLogin+Groups+and+Roles](https://wavetrak.atlassian.net/wiki/display/IE/OneLogin+Groups+and+Roles).

## Configuration

There is a `.env.sample` detailing the expected environment variables. Duplicate, rename to `.env` and set appropriate values before running.

## Endpoints

### `GET /login`

Will redirect request to OneLogin in order to trigger SAML assertion postback to
`/saml/consume`.

### `POST /authorize`

Will take a SAML assertion and return authentication details

### `POST /saml/consume`

Receives SAML assertion and exchanges assertion for a scoped access token.

# Admin Login Service

This module contains the **infrastructure code** for the `admin-login-service` formerly configured as part of the `surfline-admin-tools`.

The application code can be found at [surfline-admin/modules/login].

## Naming Conventions

See [naming-conventions] for AWS resources.

## Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV={env} make plan
ENV={env} make apply
```

Available environments are:

- sandbox
- staging
- prod

[surfline-admin/modules/login]: https://github.com/Surfline/surfline-admin/tree/master/modules/login
[naming-conventions]: https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions