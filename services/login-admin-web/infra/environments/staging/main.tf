provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "admin-tools/admin-login-service.tfstate"
    region = "us-west-1"
  }
}

module "service" {
  source = "../../"

  environment = "staging"
  default_vpc = "vpc-981887fd"
  dns_zone    = "staging.surfline.com"

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-3-staging/bca6896a1b354474/e10b8d509912ed71"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-3-staging/bca6896a1b354474"

  service_td_count = 2
}
