provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "admin-tools/admin-login-service.tfstate"
    region = "us-west-1"
  }
}

module "service" {
  source = "../../"

  environment = "sandbox"
  default_vpc = "vpc-981887fd"
  dns_zone    = "sandbox.surfline.com"

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-3-sandbox/9ff25c35a340093b/66fd46bbe4d5aa9e"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-3-sandbox/9ff25c35a340093b"

  service_td_count = 1
}
