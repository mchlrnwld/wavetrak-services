const chai = require('chai');
const dirtyChai = require('dirty-chai');
const sinonChai = require('sinon-chai');

chai.use(dirtyChai);
chai.use(sinonChai);

const testsContext = require.context('./src', true, /\.spec\.js$/);
testsContext.keys().forEach(testsContext);
