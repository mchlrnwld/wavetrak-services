/* eslint no-console: 0 */
const path = require('path');
const express = require('express');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const config = require('./webpack.config');
const login = require('./src/routes/login');

const DEVELOPING = process.env.NODE_ENV === 'development' || !process.env.NODE_ENV;
const isDeveloping = DEVELOPING;
const port = process.env.EXPRESS_PORT || 3000;
const app = express();
app.use(cookieParser()); // used for securityHandler()
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/login', login());

if (isDeveloping) {
  const compiler = webpack(config);
  const middleware = webpackMiddleware(compiler, {
    publicPath: config.output.publicPath,
    contentBase: 'src',
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false,
    },
  });

  app.use(middleware);
  app.use(webpackHotMiddleware(compiler));
  app.get('/login/*', (req, res) => {
    res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist', 'index.html')));
    res.end();
  });
} else {
  app.use('/public/', express.static(path.join(__dirname, '/dist')));
  app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  });
}

app.listen(port, '0.0.0.0', (err) => {
  if (err) {
    console.log(err);
  }
  console.info(
    `==> (developing ${isDeveloping}) Listening on port ${port}. ` +
      `http://0.0.0.0:${port}/login/ in your browser.`,
  );
});
