const fetch = require('isomorphic-fetch');

const { HOSTNAME } = process.env;

const fetchToken = (id, scope, email, name, title) => {
  const body = JSON.stringify({
    grant_type: 'client_credentials',
    scope,
    id,
    email,
    name,
    title,
  });
  const authCredentials = Buffer.from(
    `${process.env.ADMIN_LOGIN_CLIENT_ID}:${process.env.ADMIN_LOGIN_CLIENT_SECRET}`,
  ).toString('base64');
  return new Promise((resolve, reject) => {
    fetch(`https://${HOSTNAME}/api/auth/token`, {
      method: 'POST',
      headers: {
        Authorization: `Basic ${authCredentials}`,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body,
    }).then((resp) =>
      resp
        .json()
        .then((jsonResp) => resolve(jsonResp))
        .catch((err) => reject(err)),
    );
  });
};

const fetchUser = (token) =>
  new Promise((resolve, reject) => {
    fetch(`htts://${HOSTNAME}/api/auth/authorize`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    }).then((resp) =>
      resp
        .json()
        .then((jsonResp) => resolve(jsonResp))
        .catch((err) => reject(err)),
    );
  });

module.exports = { fetchToken, fetchUser };
