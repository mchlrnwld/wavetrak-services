import React, { Component } from 'react';
import { render } from 'react-dom';
import { Router, Route } from 'react-router';
import { AppContainer } from '@surfline/admin-common';
import { withErrorBoundary } from './containers/ErrorBoundary';
import history from './history';
import './style.scss';

export class Index extends Component {
  constructor(props) {
    super(props);
    this.hour = new Date().getHours();
  }

  componentDidMount() {
    document.body.style.backgroundImage = `url(/public/images/${this.getBackground()}.jpg)`;
  }

  getGreeting() {
    if (this.hour >= 0 && this.hour <= 4) return 'Welcome';
    if (this.hour >= 5 && this.hour <= 11) return 'Good Morning';
    if (this.hour >= 12 && this.hour <= 17) return 'Good Afternoon';
    return 'Good Evening';
  }

  getBackground() {
    if (this.hour >= 0 && this.hour <= 11) return 'morning';
    if (this.hour >= 12 && this.hour <= 17) return 'afternoon';
    return 'evening';
  }

  render() {
    return (
      <div style={{ transform: 'translateY(100%)' }}>
        <h2 style={{ color: 'white' }}>{this.getGreeting()}</h2>
      </div>
    );
  }
}

const App = withErrorBoundary(() => (
  <AppContainer>
    <Route path="*" component={Index} />
  </AppContainer>
));

render(
  <Router history={history}>
    <Route path="/" component={App} />
  </Router>,
  document.getElementById('root'),
);
