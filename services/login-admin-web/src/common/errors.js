/**
 * Wraps route definitions and automatically catch errors and sends them
 * downstream.
 * @param fn
 */
const wrapErrors = (fn) => (...args) => fn(...args).catch(args[2]);

/**
 * Express middleware to handle downstream errors.
 *
 * @returns {function()}
 */
const errorHandlerMiddleware = () => async (err, req, res) => {
  // eslint-disable-line no-unused-vars
  console.log(err.stack);
  return res.status(500).send({ message: 'Error encountered' });
};

module.exports = { errorHandlerMiddleware, wrapErrors };
