const session = require('express-session');
const { Strategy: SamlStrategy } = require('passport-saml');
const passport = require('passport');
const { Router } = require('express');
const { wrapErrors } = require('../common/errors');
const { fetchToken } = require('../models/users');
const logger = require('../common/logger');

const REDIRECT_SUCCESS_PATH = '/';

const log = logger('admin-login');

const { OL_COMPANY_ID, OL_COMPANY, HOSTNAME } = process.env;

const samlConfig = {
  issuer: `https://app.onelogin.com/saml/metadata/${OL_COMPANY_ID}`,
  acceptedClockSkewMs: -1, // TODO turn this back on
  callbackUrl: `https://${HOSTNAME}/saml/consume`,
  entryPoint: `https://${OL_COMPANY}.onelogin.com/trust/saml2/http-post/sso/${OL_COMPANY_ID}`,
};

module.exports = () => {
  const api = new Router();
  const RedisStore = require('connect-redis')(session); // eslint-disable-line
  const ERROR_PATH = '/error';

  api.use(
    session({
      resave: false,
      saveUninitialized: true,
      cookie: { secure: true },
      secret: 'surfs up',
    }),
  );

  passport.serializeUser((user, done) => {
    done(null, user);
  });

  passport.deserializeUser((user, done) => {
    done(null, user);
  });

  api.use(passport.initialize());
  api.use(passport.session());

  passport.use(new SamlStrategy(samlConfig, (profile, done) => done(null, { profile })));

  api.get('/error', (res) => res.status(500).send('Something went wrong'));

  const devAuth = (req, res, next) => {
    if (process.env.NODE_ENV === 'development') {
      res.cookie('access_token', 'dev_no_token');
      return res.redirect(REDIRECT_SUCCESS_PATH);
    }
    return next();
  };

  api.get(
    '/',
    devAuth,
    passport.authenticate('saml', {
      failureRedirect: ERROR_PATH,
      failureFlash: true,
    }),
  );

  api.post(
    '/saml/consume',
    passport.authenticate('saml', { failureRedirect: ERROR_PATH, failureFlash: true }),
    wrapErrors(async (req, res, next) => {
      if (req.user) {
        try {
          const { profile } = req.user;
          const token = await fetchToken(
            profile.PersonImmutableID,
            profile['User.Roles'].split(';').join(','),
            profile['User.email'],
            `${profile['User.FirstName']} ${profile['User.LastName']}`,
            profile['User.Title'],
          );
          res.cookie('access_token', token.access_token);
          log.info({ ...req.user, token, req });
        } catch (err) {
          return next(err);
        }
      }
      return res.redirect(REDIRECT_SUCCESS_PATH);
    }),
  );

  api.get('/health', (req, res) =>
    res.send({
      status: 200,
      message: 'OK',
      version: process.env.APP_VERSION || 'unknown',
    }),
  );

  return api;
};
