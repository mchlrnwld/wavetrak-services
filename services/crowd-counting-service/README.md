# Crowd Counting Service

This service implements the logic to do crowd-counting for the ML smart-highlights/crowd-counting pipeline. Please refer to the
ML pipeline architecture [document](https://github.com/Surfline/wavetrak-services/pull/4106) for more information regarding the pipeline
as a whole. In summary, this service:
1. Listens to an SQS queue, waiting for messages that are triggered from ObjectCreate events in `sl-live-cam-archive-{ENV}`
2. Burns down the queue when messages arrive. The messages contain paths to frame objects in S3.
3. The service will then get detection counts from the frame as well as annotate it.
4. The annotated frame will be put back in S3, and the detection counts will be stored in Mongo.

## Development

This service requires a hefty amount of hard-drive/RAM space to build and run. Hence, instead of developing locally,
it is often easier to spin up a EC2 instance, and develop remotely. The below commands show you how to spin up a `t3.large`
instance (16 GB of RAM with 125 GB of hard-drive space). Before running these commands, make sure to download the `.pem` file
from `1Password` (which `.pem` file depends on the environment you are choosing, i.e. `surfline-dev|prod|legacy`) and export
your environment variables (described in [this](https://wavetrak.atlassian.net/wiki/spaces/TECH/pages/1107951999/Create+a+Temporary+EC2+Instance) document).

```
# Enter into the remote environment
make enter_remote_environment

# Copy your local work to the remote environment
make copy_dir_to_remote_environment

# Shutdown the remote environment
make shutdown_remote_environment
```

Make sure you are connected to the corresponding VPN.

## Development Commands

To run the service, use the below commands:

```
# Copy sample environment file, make sure to populate yours
cp .env.sample .env

# Builds the container
make build

# Enter into the container (if not using GPU)
make enter_local

# Enter into the container (if using GPU)
make enter_gpu
```

## Testing

Once inside the container, you can run an integration test with the command:
```
# Integration test
python3 /usr/src/app/entrypoint.py --saveoutput true --deletemessages true --downloadmodel true --test true
```

This will run the entrypoint to ensure the code is working properly. If there is an error, you will see the stack trace.
The above command is exactly what a production command would look like.

As an added layer of assurance, the test will output an annotated frame as a result of your test. The frame
will be placed in the current directory and will be called `test_result.jpg`. You can view this file to ensure you
are annotating stills properly. Also, the detection counts will be logged, so you can ensure they are what you are
expecting for the frame in question.

Any calls that modify data in S3 will be stubbed in the test, hence, don't worry about setting the `--saveoutput` and
`--deletemessages` flags to `true`.

## Deployment

FINISH README WITH DEPLOYMENT INSTRUCTIONS ONCE TERRAFORM HAS BEEN SETUP.

## Infrastructure

![Infrastructure Image](../../architecture/images/camera-system/crowd-counting-service.png)



