#!/usr/bin/env bash
rm -rf models
aws s3 cp s3://wt-ml-artifacts/models/Segmentation/TorchDeepLabV3/v20220105/model.tar.gz models/Segmentation.tar.gz
aws s3 cp s3://wt-ml-artifacts/models/ObjectDetection/TorchRetinaNet/Optimized/g4dn/v20220113/model.tar.gz models/ObjectDetection.tar.gz
mkdir -p models/Segmentation
tar -zxf models/Segmentation.tar.gz -C models/Segmentation/
mkdir -p models/ObjectDetection
tar -zxf models/ObjectDetection.tar.gz -C models/ObjectDetection/

rm models/*.tar.gz
