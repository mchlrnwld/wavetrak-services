# We're compiling some NVIDIA plugins for TensorRT infernence in here.
import os
import glob

from setuptools import find_packages, setup
from torch.utils.cpp_extension import BuildExtension, CUDAExtension


def get_extension_sources():
    this_dir = os.path.dirname(os.path.abspath(__file__))
    if not this_dir.split('/')[-1].endswith('sl_camera_sensor'):
        this_dir = os.path.join(this_dir, 'sl_camera_sensor')
    extensions_dir = os.path.join(this_dir, "nets", "detection", "csrc")
    cpp_sources = glob.glob(os.path.join(extensions_dir, "*.cpp"))
    cu_sources = glob.glob(os.path.join(extensions_dir, "cuda", "*.cu"))
    return cpp_sources + cu_sources
    
setup(
    name='sl-camera-sensor',
    version='0.2.0',
    description='DNN inference and post-processing for Surfline cameras',
    author='Surfline Labs',
    url='https://github.com/Surfline/surfline-labs',
    packages=find_packages(),
    ext_modules=[
        CUDAExtension(
            'nets.detection._C',
            get_extension_sources(),
            extra_compile_args={
                          'cxx': ['-std=c++14', '-O2', '-Wall'],
                          'nvcc': [
                              '-std=c++14', '--expt-extended-lambda', '--use_fast_math', '-Xcompiler', '-Wall,-fno-gnu-unique',
                              '-gencode=arch=compute_60,code=sm_60', '-gencode=arch=compute_61,code=sm_61',
                              '-gencode=arch=compute_70,code=sm_70', '-gencode=arch=compute_72,code=sm_72',
                              '-gencode=arch=compute_75,code=sm_75', '-gencode=arch=compute_80,code=sm_80',
                              '-gencode=arch=compute_86,code=sm_86', '-gencode=arch=compute_86,code=compute_86'
                          ],
                      },
                      libraries=['nvinfer', 'nvinfer_plugin', 'nvonnxparser', 'opencv_core', 'opencv_imgproc', 'opencv_highgui', 'opencv_imgcodecs'])
    ],
    cmdclass={'build_ext': BuildExtension.with_options(no_python_abi_suffix=True)},
    install_requires=[
        'torch>=1.0.0a0',
        'torchvision',
        'pillow',
        'requests',
    ],
)
