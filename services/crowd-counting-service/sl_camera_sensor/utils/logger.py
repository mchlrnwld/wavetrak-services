# -*- coding: utf-8 -*-
import logging
import os

logging.basicConfig(
    format="%(asctime)s %(levelname)-8s %(message)s",
    level=os.environ.get("LOG_LEVEL", 'INFO'),
    datefmt="%Y-%m-%d %H:%M:%S",
)
logger = logging.getLogger()


def get_logger():
    return logger
