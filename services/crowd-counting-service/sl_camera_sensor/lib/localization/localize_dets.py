# Localize the detection classes to the correct frame regions, using
# segmentation masks. This fixes false SUP/surfer detections on the sand.
# This is also used in the wave tracking to find out if surfers are
# surfing on any given wave. In addition we're adding the percents
# of each mask attribute contained within each surfersurfing bounding box
# in the surfer tracking.
#
from typing import List, Optional, Union

import numpy as np
from PIL import Image
from scipy.ndimage.measurements import label as label_regions
from scipy.ndimage.morphology import binary_dilation
from scipy.spatial.distance import cdist

from sl_camera_sensor.nets.detection.detection_model import DetectionModel
from sl_camera_sensor.nets.segmentation.segmentation_model import (
    SegmentationModel,
)
from sl_camera_sensor.utils.logger import get_logger

log = get_logger()


class LocalizeDets:
    def __init__(
        self,
        dets_label_map: Optional[dict] = None,
        mask_label_map: Optional[dict] = None,
        target_size: Optional[tuple] = (720, 1280),
    ) -> None:
        """We will need to store each of the label maps, either now or
        when we call this with masks + dets.
        :param dets_label_map: dict of class string integer mappings for detection model
        :param mask_label_map: " " " for segmentation model.
        """
        self.dets_label_map = dets_label_map or DetectionModel.get_label_map()
        self.mask_label_map = (
            mask_label_map or SegmentationModel.get_label_map()
        )
        self.target_size = target_size
        self.height, self.width = target_size
        if target_size[0] > target_size[1]:
            self.height, self.width = self.width, self.height

    @property
    def dets_label_map(self):
        return self._dets_label_map

    @dets_label_map.setter
    def dets_label_map(self, inp):
        self._dets_label_map = self.__check_label_map(inp)
        self._dets_inv = self._invert_map(self._dets_label_map)

    @property
    def mask_label_map(self):
        return self._mask_label_map

    @mask_label_map.setter
    def mask_label_map(self, inp):
        self._mask_label_map = self.__check_label_map(inp)
        self._mask_inv = self._invert_map(self._mask_label_map)

    def __call__(
        self,
        dets: Union[List[dict], dict],
        mask: np.ndarray,
        dets_label_map: Optional[dict] = None,
        mask_label_map: Optional[dict] = None,
        localize_shore: Optional[bool] = True,
        localize_surfing: Optional[bool] = False,
        localize_mask_inside_surfer: Optional[bool] = False,
        binary_mask: Optional[bool] = False,
    ) -> Union[List[dict], dict]:
        """Feed a dict or list of dicts of detection model outputs,
        along with a single mask or multiple masks, and modify each detection
        dict classes, if the water objects are outside the water mask regions.
        :param dets: detection dicts
        :param masks: single segmentation mask as logits or argmax(logits)
        :param dets_label_map: dict of class string integer mappings for detection model
        :param mask_label_map: " " " for segmentation model.
        :param localize_shore: whether or not to check for water people on the shore.
        :param localize_surfing: whether or not to check for someone surfing a wave.
        :param localize_mask_inside_surfer: get back mask attribute percents
        contained inside a SurferSurfing bounding box.
        :param binary_mask: Ignore class ints in the mask and just look for 1's overlap.
        This is useful with wave tracking, since my wave contours are class
        attribute unions.
        :returns: modified detection dicts. surfer classes on the shore will be
        converted to BeachPeople class.
        """
        if dets_label_map is not None:
            self.dets_label_map = dets_label_map
        elif self.dets_label_map is None:
            raise ValueError("Need detection label map to localize detections")
        if mask_label_map is not None:
            self.mask_label_map = mask_label_map
        elif self.mask_label_map is None:
            raise ValueError("Need mask label map to localize detections")
        if not isinstance(mask, np.ndarray):
            raise ValueError(
                f"Input segmentation mask as numpy array, not {type(mask)}"
            )
        if mask.ndim == 4:
            mask = np.squeeze(np.argmax(mask, axis=1))
        elif mask.ndim == 3:
            mask = np.argmax(mask, axis=0)
        elif mask.ndim != 2:
            raise ValueError(
                f"Mask input dimension not understood: {mask.ndim}"
            )
        if mask.shape != self.target_size:
            mask = np.asarray(
                Image.fromarray(mask.astype(np.uint8)).resize(
                    self.target_size[::-1], Image.NEAREST
                )  # Don't use anything other than NEAREST here!
            )
        if not isinstance(dets, list):
            if isinstance(dets, dict):
                return self._localize(
                    dets,
                    mask,
                    localize_shore,
                    localize_surfing,
                    localize_mask_inside_surfer,
                    binary_mask,
                )
            else:
                raise ValueError(
                    "Input single det dict or list of dicts to localizer"
                )

        else:
            if not isinstance(dets, list):
                dets = [dets]
            newdets = []
            for det in dets:
                newdets.append(
                    self._localize(
                        det,
                        mask,
                        localize_shore,
                        localize_surfing,
                        localize_mask_inside_surfer,
                        binary_mask,
                    )
                )
            return newdets

    def _localize(
        self,
        det,
        mask,
        localize_shore,
        localize_surfing,
        localize_mask_inside_surfer,
        binary_mask,
    ) -> List[dict]:
        if "detection_classes" not in det or "detection_boxes" not in det:
            raise ValueError(
                "'detection_classes' or '_boxes' key not present in detection dict"
            )
        ylocs = (det["detection_boxes"][:, 0:4:2] * self.height).astype(int)
        xlocs = (det["detection_boxes"][:, 1:4:2] * self.width).astype(int)
        xlocs[xlocs >= self.width] = self.width - 1
        ylocs[ylocs >= self.height] = self.height - 1
        if len(det["detection_classes"]) == 0:
            return det
        det_classes = np.vectorize(self.dets_label_map.get)(
            det["detection_classes"]
        )
        if localize_shore and not (
            localize_surfing or localize_mask_inside_surfer
        ):
            return self._localize_shore(det, mask, xlocs, ylocs, det_classes)
        elif localize_surfing and not (
            localize_shore or localize_mask_inside_surfer
        ):
            return self._localize_surfing(
                det, mask, xlocs, ylocs, det_classes, binary_mask
            )
        elif localize_mask_inside_surfer and not (
            localize_shore or localize_surfing
        ):
            return self._localize_mask_inside_surfer(det, mask, xlocs, ylocs)
        else:
            raise NotImplementedError("Mulit-localization not implemented yet")

    @staticmethod
    def _preprocess_locs(
        pos_tuples: Union[np.ndarray, List[tuple]], mask: np.ndarray
    ) -> np.ndarray:
        """unnormalize detection positional tuples, [(y0, x0, y1, x1), ...]
        to be integer values between the same range as the input msk shape.
        """
        if not isinstance(pos_tuples, np.ndarray):
            pos_tuples = np.asarray(pos_tuples)
        if np.max(pos_tuples) <= 1:
            pos_tuples[:, 0:4:2] *= mask.shape[0]
            pos_tuples[:, 1:4:2] *= mask.shape[1]
            pos_tuples = pos_tuples.astype(int)
        else:
            pos_tuples = pos_tuples.astype(np.float16)
            pos_tuples[:, 0:4:2] /= mask.shape[0]
            pos_tuples[:, 1:4:2] /= mask.shape[1]
        return pos_tuples

    @staticmethod
    def unnormalize(locs: np.ndarray, target_size: tuple) -> np.ndarray:
        """Input an array of [(x, y), ...] normalized tuple locations and
        a target (y, x) size, and get back unnormalized locations
        """
        if len(locs) < 1 or np.max(locs) >= 1:
            return locs
        if isinstance(locs, list):
            locs = np.array(locs)
        if locs.ndim == 1:
            locs = locs[np.newaxis, :]
        locs[:, 0] *= target_size[1]
        locs[:, 1] *= target_size[0]
        return locs.astype(int)

    @staticmethod
    def normalize(locs: np.ndarray, target_size: tuple) -> np.ndarray:
        """integer unnormalized locations to normalized float16 positions"""
        if len(locs) < 1 or np.max(locs) <= 1:
            return locs
        if isinstance(locs, list):
            locs = np.array(locs)
        if locs.ndim == 1:
            locs = locs[np.newaxis, :]
        locs = locs.astype(np.float16)
        locs[:, 0] /= target_size[1]
        locs[:, 1] /= target_size[0]
        return locs

    @staticmethod
    def localize_mask_inside_surfer(
        pos_tuples: List[tuple],
        mask: np.ndarray,
        map_label: dict,
        store_crops: Optional[bool] = False,
    ) -> List[dict]:
        """Pass list of [(y0, x0, y1, x1), ...] tuples + single mask
        and get back percents of each mask class inside bbox tuple
        :param map_label: inverse of label_map == {string: int, ...},
        so a dict of {int: string, ...}
        """
        if len(pos_tuples) == 0:
            return None
        pos_tuples = LocalizeDets._preprocess_locs(pos_tuples, mask)
        pcts = [{k: 0 for k in map_label.values()}] * len(pos_tuples)
        for i, pos_tuple in enumerate(pos_tuples):
            y0, x0, y1, x1 = pos_tuple
            crop = mask[y0:y1, x0:x1]
            if store_crops:
                # Store the crop along with pcts in the dict!
                pcts[i]["crop"] = crop
            for wave_cls_int in np.unique(crop):
                n_cls = np.count_nonzero(crop == wave_cls_int)
                pcts[i][map_label[wave_cls_int]] = np.float16(
                    n_cls / crop.size
                )
        return pcts

    def _get_dilated_binary_mask(
        self,
        mask: np.ndarray,
        cls_int: int,
        kernel: Optional[np.ndarray] = np.ones((3, 3)),
        pad_with_water=False,
    ) -> np.ndarray:
        """Input a mask, class integer you're interested in, and a dilation kernel,
        and get back binary mask of that class, slightly dilated (depending on kernel).
        This is useful when you're looking for the region where boundaries of
        different classes meet - sum two or more dilated masks and look for where > 1...

        :param mask: 2D class integer mask
        :param cls_int: class integer you want binary mask of
        :param kernel: 2D matrix of kernel to dilate with
        :param pad_with_water: dilate the water minus wave mask and hit
        the binary mask with that, so as to remove some of the noise at the
        edges when waves are breaking.
        :returns: dilated binary mask.
        """
        bin_mask = np.zeros(mask.shape)
        bin_mask[mask == cls_int] = 1
        return binary_dilation(bin_mask, kernel).astype(int)

    def localize_pockets(
        self,
        mask: np.ndarray,
        unnormalize: Optional[tuple] = False,
    ) -> np.ndarray:
        """Given a single input mask, get back an array of normalized (x, y)
        tuples of wave pocket positions. This size of this list should
        range between 1 and 2, in theory.

        :param mask: 2D np array of class integer mask. If you want to do this
        on a per-wave basis, you should pass a masked-mask, where only the
        wave in question is unmasked.
        :param unnormalize: (y, x) size of matrix to unnormalize against.
        :returns: np array shaped (n_pocket, 2)
        """
        binMasks = {
            cls: self._get_dilated_binary_mask(mask, self._mask_inv[cls])
            for cls in ["WhiteWater", "Lip", "Wave"]
        }
        # Ignore whitewater for now...
        # binMaskSum = sum([binMasks["Lip"], binMasks["Wave"]])
        binMaskSum = sum(
            [binMasks["Lip"], binMasks["WhiteWater"], binMasks['Wave']]
        )
        pockets = np.where(binMaskSum > 1)
        # If there's no lip, try again with whitewater added. I think those big
        # pitching waves will almost always have a lip component detected.

        # if len(pockets[0]) == 0:  # Most aggressive way here
        # binMaskSum += binMasks['WhiteWater']
        pockets = np.where(binMaskSum > 1)

        binMaskSum *= 0
        binMaskSum[pockets] = 1
        # Lastly we need to separate them as there could be 2 pockets per wave.
        labeled_pockets, npockets = label_regions(binMaskSum, np.ones((3, 3)))
        pocket_locs = []
        sy, sx = mask.shape
        for npocket in range(1, npockets + 1):
            y, x = np.where(labeled_pockets == npocket)
            # idx = np.max([0, np.argmin(y) - 2])  # This will shift the pocket away
            # towards the open face, but fails in bad lighting
            idx = np.max([0, np.argmax(y)])  # Look at bottom - instead of top
            pocket_locs.append(
                (np.float16(x[idx] / sx), np.float16(y[idx] / sy))
            )
        pocket_locs = np.array(pocket_locs)
        if unnormalize:
            pocket_locs = self.unnormalize(pocket_locs, self.target_size)
        return pocket_locs

    @staticmethod
    def get_dist_to_pocket(
        pos_tuples: List[tuple], pocket_locs: Optional[List[tuple]] = None
    ) -> List[dict]:
        """
        list of (x,y) pocket positions, and list of surfer locations ->
        get back distance to closest pocket position. Will this be noisy?
        Ie do we need some filtering or thresholding here?

        All inputs should be in normalized coordinates.
        """
        # Get mean of pos_tuples. y0, x0, y1, x1 -> x, y
        ymeans = pos_tuples[:, 0:4:2].mean(axis=1)[np.newaxis, :]
        xmeans = pos_tuples[:, 1:4:2].mean(axis=1)[np.newaxis, :]
        pos_tuples2 = np.hstack((xmeans.T, ymeans.T))
        if pos_tuples2.ndim > 2:
            pos_tuples2 = np.squeeze(pos_tuples2)
        dists = cdist(pos_tuples2, pocket_locs).astype(np.float16)
        return [{"dPocket": d.min()} for d in dists]

    @staticmethod
    def localize_lip_trough_height(
        pos_tuples: List[tuple], mask: np.ndarray
    ) -> List[dict]:
        """Given a tuple of OD positions and a binary mask comprised
        of all the wave attributes, figure out how far from the top and
        bottom of the wave the surfer is.

        :param pos_tuples: Normalized or unnormalized list of postiional tuples
        as [(y0, x0, y1, x1), ...]
        :returns: list of dicts for each pos tuple, with keys for distances from
        various wave locations (lip, trough) and ratios of wave height
        to box height.

        TODO perhaps try using averages instead of size-one slices.
        """
        if len(pos_tuples) == 0:
            return None
        # keys = [str(pt) for pt in pos_tuples.copy()]
        pos_tuples = LocalizeDets._preprocess_locs(pos_tuples, mask)
        # Pass back a list of dicts, same as localize_mask_inside_surfer
        info = [{}] * len(pos_tuples)
        # for key, pos_tuple in zip(keys, pos_tuples):
        for i, pos_tuple in enumerate(pos_tuples):
            # Take the position of the surfbort, not mean y.
            # y0 = pos_tuple[2]  # Use mean instead
            # Use mean of y0, y1 in an effort to reduce noise.
            y0 = pos_tuple[[0, 2]].mean().astype(int)
            x = pos_tuple[[1, 3]].mean().astype(int)
            wave_slice = np.where(mask[:, x])[
                0
            ]  # vertical slice of binary wave
            if len(wave_slice) == 0:
                # We're in here if the mask is all zeros. This
                # is pretty rare but happens sometimes. Need to
                # investigate wave tracker blobber in this case.
                log.critical(
                    "binary mask is all zeros in lip/trough localization,"
                    " so you're getting back NaNs"
                )
                info[i] = {
                    "dLip": np.nan,
                    "dTrough": np.nan,
                    "blob_bbox_ratio": np.nan,
                }
            else:
                trough = wave_slice.max()  # y0 is at the top
                lip = wave_slice.min()
                # These will be negative when the surfer is out of wave bounds.
                info[i]["dLip"] = y0 - lip
                info[i]["dTrough"] = trough - y0
                info[i]["blob_bbox_ratio"] = np.float16(
                    (trough - lip) * 1.0 / (pos_tuple[2] - pos_tuple[0])
                )
        return info

    def _localize_surfing(
        self, det, mask, xlocs, ylocs, det_classes, binary_mask
    ):
        """This should be called for each wave separately for the most
        simplicity. For now it returns positional tuples, so the
        __call__ method will return varying outputs depending on what you
        want to localize. This is called from the wave tracker to get the
        normalized gloabl positions of all the surfers surfing on any given wave.

        TODO I know there is a vectorized way to get multiple crops from a
        single image. However I'm not sure how to do that vectorizing with
        varying crop sizes. This is pretty fast as-is, but if it's too slow at some
        point we can look into vectorizing more.

        :returns: tuples of (y0, x0, y1, x1) normalized bounding box coords.
        """
        if binary_mask:
            mask_classes_to_match = 1
        else:
            mask_classes_to_match = [
                self._mask_inv[cls] for cls in ["Wave", "Lip", "WhiteWater"]
            ]
        surfing_locs = []
        for i in range(len(det_classes)):
            if det_classes[i] == "SurferSurfing":
                y, x = ylocs[i], xlocs[i]
                crop = mask[y[0] : y[1], x[0] : x[1]]  # noqa: E203
                if mask_classes_to_match in crop:
                    # ilocs = (int(np.mean(xlocs)), int(np.mean(ylocs[i])))
                    norm_tuple = (
                        np.float16(y[0]) / self.height,
                        np.float16(x[0]) / self.width,
                        np.float16(y[1]) / self.height,
                        np.float16(x[1]) / self.width,
                    )
                    if norm_tuple not in surfing_locs:
                        surfing_locs.append(norm_tuple)
        return surfing_locs

    def _localize_shore(
        self, det: dict, mask: np.ndarray, xlocs, ylocs, det_classes: List[str]
    ) -> dict:
        """
        Check for 'SUP' and 'Surfer'* classes on the shore. I've
        never seen a beachperson in the water, but when/if we do, we can
        look into adding that (although it's a one->many problem not many->one).
        This just looks for a 'Shore' mask integer within a SUP and Surfer*
        class det crop, which is rotationally invariant (ie not looking at ymaxs).
        """
        bad_cnt = 0
        for i in range(len(det_classes)):
            if "Surfer" in det_classes[i] or "SUP" in det_classes[i]:
                y, x = ylocs[i], xlocs[i]
                crop = mask[y[0] : y[1], x[0] : x[1]]  # noqa: E203
                if self._mask_inv["Shore"] in np.unique(crop):
                    # Assign this from a water person to a beach person
                    det["detection_classes"][i] = self._dets_inv["BeachPerson"]
                    bad_cnt += 1
        return det

    def localize_sitting(
        self,
        dets: dict,
        mask: np.ndarray,
        with_beachpeople=True,
        shoreline=None,
        max_shore_dist_pix=30,
    ) -> dict:
        """
        Remove any sitting detections that occur on a wave, by
        looking at bbox regions on seg mask. Trying
        to figure out if we can do statistical homography estimation from
        people sitting, and obv vertical modulations for stationary
        detections are unwanted

        :param dets: single detection dict.
        :param mask: corresponding mask for dets.
        :param with_beachpeople: add beachpeople to the returned locs.
        :param shoreline: (x, y) vector array of shoreline, used
        to omit beachpeople that are too far from the shore.
        :param max_shore_dist_pix: max distance in pixels that a bbox can be from
        the nearest shoreline location.
        :returns: detection dict with those sitting on water only.
        All the other keys are removed as well.
        """
        det_classes = np.vectorize(self.dets_label_map.get)(
            dets["detection_classes"]
        )
        idxs = np.where(det_classes == 'SurferSitting')[0]
        sitting_locs = []
        if len(idxs) > 0:
            locs = LocalizeDets._preprocess_locs(
                dets['detection_boxes'][idxs], mask
            )
            for loc in locs:
                crop = mask[loc[0] : loc[2], loc[1] : loc[3]]  # noqa: E203
                if np.all(crop == self._mask_inv['Water']):
                    sitting_locs.append(loc)
            # Put them back in normalized coords
            if len(sitting_locs) > 0:
                sitting_locs = LocalizeDets._preprocess_locs(
                    sitting_locs, mask
                )
            # Add BeachPeople?
            if with_beachpeople:
                if shoreline is None:
                    raise ValueError(
                        'if you want to include beachpeople in the homography'
                        ' locs you need to supply a shoreline vector'
                    )
                if shoreline[:, 0].max() != mask.shape[1] - 1:
                    raise ValueError(
                        'Shoreline coords were computed on the wrong'
                        ' size image.'
                    )
                idxs = np.where(det_classes == 'BeachPerson')[0]
                if len(idxs) > 0:
                    norm_locs = dets['detection_boxes'][idxs]
                    y = norm_locs[:, 2][np.newaxis, :]
                    # Take the bottom position. Assumes beach is below water...
                    x = norm_locs[:, 1:4:2].mean(axis=1)[np.newaxis, :]
                    locs = LocalizeDets.unnormalize(
                        np.vstack((x, y)).T, mask.shape[:2]
                    )  # This assumes the mask localizer that made this
                    # shoreline has the same target size as the current mask lul.
                    if locs.ndim != 2 or shoreline.ndim != 2:
                        log.critical(
                            'Shape error in beach people x shoreline dist'
                        )
                        return sitting_locs

                    dr = cdist(locs, shoreline).min(axis=1)
                    close = np.where(dr < max_shore_dist_pix)[0]
                    if len(close) > 0:
                        if len(sitting_locs) > 0:
                            sitting_locs = np.vstack(
                                (sitting_locs, norm_locs[close])
                            )
                        else:
                            sitting_locs = norm_locs[close]
        return sitting_locs

    def _invert_map(self, label_map):
        return {v: k for k, v in label_map.items()}

    def __check_label_map(self, label_map):
        """
        In this class we always want {int: str} label maps. Flip them
        if they're the other way around
        """
        if label_map is None:
            raise ValueError("Localizer label maps cannot be None")
        keys = list(label_map.keys())
        if isinstance(keys[0], str):
            label_map = self._invert_map(label_map)
        elif not str(keys[0]).isnumeric():
            raise ValueError(
                "Only strings and integers should be in label map."
                f"Got {type(keys[0])}"
            )
        return label_map
