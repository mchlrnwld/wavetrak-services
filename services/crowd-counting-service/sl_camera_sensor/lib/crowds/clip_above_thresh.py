# This is not exclusively related to crowd counting, but this removes
# low detections below some threshold. It's better practice to not pass
# around dets with a bunch of false positives.
#
from typing import List, Optional, Union

import numpy as np


def __clip_det(det, thresh):
    keep = np.where(det['detection_scores'] >= thresh)[0]
    for key in ['detection_classes', 'detection_boxes', 'detection_scores']:
        det[key] = det[key][keep]
    det['num_detections'] = len(keep)
    return det


def clip_above_thresh(dets: Union[List, dict], thresh: Optional[float] = 0.25):
    """Remove all the low confidence detections from a dection dict
    or list of detection dicts
    :parm dets: list or dict of detections
    :parm thresh: inclusive threshold to clip at.
    :returns: clipped dets.
    """
    if isinstance(dets, dict):
        return __clip_det(dets, thresh)
    elif isinstance(dets, list):
        return [__clip_det(det, thresh) for det in dets]
    else:
        raise ValueError('Pass dict or list of dicts for confidence clipping.')
