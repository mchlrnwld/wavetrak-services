# Crowd counting for high-hz batch inference detections.
#
# TODO this needs to be updated with sigma clipping for water people.
from collections import defaultdict

import numpy as np

from common import logger
from sl_camera_sensor.nets.detection.detection_model import DetectionModel
from sl_camera_sensor.lib.crowds.clip_above_sigma import clip_above_sigma
log = logger.get_logger()


class CrowdCounter:
    def __init__(self):
        self._label_map = DetectionModel.get_label_map()
        lm_items = self._label_map.items()
        self.sand_classes = [v for k, v in lm_items if 'Beach' in k]
        self.surfer_classes = [
            v for k, v in lm_items if 'Surfer' in k or 'SUP' in k
        ]

        # 'timestamps', 'sand', and 'surf' keys. Update while streaming
        self.counts = defaultdict(list)
        self._clear = False

    def get_count(self, det, thresh):
        # Get the raw counts from class ints
        raw_sand_cnts = self._dets2raw(det, thresh, self.sand_classes)
        raw_surf_cnts = self._dets2raw(det, thresh, self.surfer_classes)

        return int(raw_sand_cnts), int(raw_surf_cnts)

    # TODO move this logic to the counts api?
    # in theory this logic could be applied to any timeframe and we store the raw counts?
    def get_counts(self, dets, thresh, update=True, **kwargs):
        """Update the counts dict and return the latest counts.

        :param dets: list of detection dictionaries
        :param thresh: threshold in range (0, 1), below which detections
        will be ignored.
        :param update: I don't remember
        :param kwargs: optional key, val pairs to add to self.counts,
        eg {'GlareModel': 0.54, 'FogModel':0.04} etc.
        """

        # Get the raw counts from class ints
        raw_sand_cnts = [self._dets2raw(d, thresh, self.sand_classes) for d in dets]
        raw_surf_cnts = [self._dets2raw(d, thresh, self.surfer_classes) for d in dets]

        # TODO I think taking the max for the sand counts might be wrong - if there
        # is a lot of flux in the scene it will bias it higher. What do we care
        # most about? probably the mean.
        sand_cnt = int(np.mean(raw_sand_cnts))
        surf_cnt = int(clip_above_sigma(raw_surf_cnts))

        if update:
            if self._clear:
                self.counts.clear()
                self._clear = False
            try:
                timestamps = [d["timestamp"] for d in dets]
                self.counts["timestamp"].append(int(np.mean(timestamps)))
            except (IndexError, KeyError):
                raise ValueError("Detection dict doesn't have 'timestamp' key")

            self.counts["surf"].append(surf_cnt)
            self.counts["sand"].append(sand_cnt)

            for k, v in kwargs.items():
                if not isinstance(v[0], str):
                    self.counts[k] = np.mean(v)
                else:
                    # It's a string, so just report the most often occuring string
                    unq, cnt = np.unique(v, return_counts=True)
                    self.counts[k] = unq[np.argmax(cnt)]

        return sand_cnt, surf_cnt

    def get_history(self):
        """Return all the coutns (serializable to json)
        and reset self.counts. Use this to write out to disk
        at some regular intervals.
        """
        self._clear = True
        return self.counts

    def _dets2raw(self, d, thresh, class_list):
        """
        Count the number of instances, in a list of detections,
        that are above some confidence threshold and belonging
        to any number of classes.

        :param detections: list of detection dicts
        :param thresh: detection threshold (inclusive)
        :param class_list: list of class ints to keep.
        :return: list of integer counts for each dict, same
        length as input detections list.
        """
        if isinstance(class_list[0], str):
            class_list = [self._label_map[c] for c in class_list]
        return np.sum(
                np.logical_and(
                    (d["detection_scores"] >= thresh),
                    np.isin(d["detection_classes"], class_list),
                )
            )
