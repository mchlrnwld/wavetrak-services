# 2D angular power spectrum with flat plane approximation.
# TODO add general prime factorization to speed up FFTs?
#
import numpy as np
import PIL
from numpy.fft import fft2
from PIL import Image
from skimage.transform import resize


class Spec2D:
    """Two dimensional spectral decompositions and binning
    for power spectrum density measurements. Currently power spectra
    are input features for visibility models.
    """

    def __init__(self, input_image=None, nbins=10, center=True):
        """Set the bins and ell modes based on the image size.
        :param input_image: string or np array of 2d or 3d image
        :param nbins: number of bins for power spectra computation
        :param center: bool whether or not to center the fft.
        """
        self.nbins = nbins
        self.nx = 0  # Frame sizes
        self.ny = 0
        self.center = center  # whether or not to center the FFTs
        self.target_nx = 1280
        self.target_ny = 720

        self.ell = None  # ell modes per frame size
        self._minl = None
        self._maxl = None
        self._bin_edges = None

        if input_image is not None:
            self._load_image(input_image)
        self.im = None

    def get_power_spec(
        self, im, nbins=None, rgb=True, rm_monopole=False, normalize=False
    ):
        """Inupt an RGB frame and get back a normalized
        power spectrum (ell, ell**2*Cl)

        :param im: 2d grayscale image or string to image path
        :param nbins: number of bins for spectrum. Should be 10 for 720p frames
        :returns: (ell, norm(ell**2 * Cl)) tuple.
        """
        nbins = self.nbins if nbins is not None else nbins
        frame = self._load_image(im, nbins=nbins, rgb=rgb)
        fspec = self._spec2d(frame, center=True, rm_monopole=rm_monopole)
        ell, Cl = self._powerspec(np.real(fspec), nbins=nbins, center=True)
        # pspec = ell**2 * Cl
        pspec = np.log(Cl + 1e-6)
        # return (ell, pspec / np.max(pspec)) if np.max(pspec) > 0 else (ell, pspec)
        return ell, pspec

    def _spec2d(self, im, im2=None, rm_monopole=False, center=True):
        """2D Spectral decomposition.
        You can look at this image by doing:
        >>> plt.imshow(np.log(spec2d(grayscale_im)))

        :param im: input 2d image
        :param im2: second 2d image for cross-correlation. If None,
        we do the auto-correlation.
        :param rm_monopole: boolean to remove the monopole (mean)
        from the image. This reduces noise/fft offsets, but affects
        your absolute units.
        :param center: bool whether or not to roll the low-frequency modes
        : to the center of the image (small scales in real space moved
        to the outside edges of spec2d frame).
        :returns: 2D image in frequency space, with smallest wavenumbers
        in the middle, increasing radially outward (if center=True).
        """
        # im = self._load_image(im)
        if rm_monopole:
            im = self._rm_mono(im)
        ft1 = fft2(im)

        # Cross-correlation?
        if im2:
            # im2 = self._load_image(im2)
            if im.shape != im2.shape:
                raise ValueError(
                    "Cannot compute cross-correlation of "
                    "mismatched image shapes."
                )
            if rm_monopole:
                im2 = self._rm_mono(im2)
            ft2 = fft2(im2)
        else:
            ft2 = ft1  # Auto-correlation

        ft = ft1 * np.conj(ft2)
        if center:
            ft = self._fftroll(ft)
        return ft

    def _load_image(self, im, nbins=None, rgb=True, norm255=False):
        """If you input a floating point array we assume it's normalized.
        if you want to normalize it like we do with RGB uint8 frames,
        set norm255=True to get array /= 255.
        """
        if (type(im) == PIL.Image.Image) or hasattr(im, "alpha_composite"):
            im = im.resize((self.target_nx, self.target_ny), Image.BILINEAR)
            im = np.asarray(im)

        if isinstance(im, str):
            frame = Image.open(im)
            if rgb:
                frame = np.array(frame) / 255.0
            else:
                frame = frame.convert("L") / 255.0

        elif isinstance(im, np.ndarray):
            if rgb and im.shape[-1] != 3:
                raise ValueError("Requested RGB spectra but input a 2D frame")

            if im.dtype == "uint8":  # Assume it's RGB 0 <= im <= 255
                if rgb:
                    frame = np.array(im) / 255.0
                else:
                    frame = np.array(Image.fromarray(im).convert("L")) / 255.0

            else:  # Given a float array...
                if ~rgb:
                    frame = np.array(Image.fromarray(im).convert("L"))
                if norm255:
                    frame /= 255.0
        else:
            raise TypeError(f"Input type not supported: {type(im)}")

        # These need to be run on 720p frames! Our ell modes need to
        # be conistent between cams!!!
        if frame.shape[:2] != (self.target_ny, self.target_nx):
            frame = resize(frame, (self.target_ny, self.target_nx))
            set_trace()

        if np.mean(frame) == 0.0:
            frame += 1e-6

        # Set the PS params based on image size. Should be 720p
        # if you're doing fog stuff!
        if (
            ((self.ny, self.nx) != frame.shape)
            or (self.ell is None)
            or (nbins != None and nbins != self.nbins)
        ):
            self.nbins = nbins if nbins is not None else self.nbins
            self.ny, self.nx = frame.shape[:2]
            self.ell = self._get_ell(self.nx, self.ny, center=self.center)
            self._minl = min(self.ell[self.ell > 0])
            self._maxl = max(self.ell[self.ell > 0])
            self._bin_edges = np.logspace(
                np.log10(self._minl), np.log10(self._maxl), self.nbins + 1
            )
            self._insides = [
                np.where(
                    (self.ell >= self._bin_edges[i])
                    & (self.ell <= self._bin_edges[i + 1])
                )
                for i in range(self.nbins)
            ]
            self._l = [
                np.mean([self._bin_edges[i], self._bin_edges[i + 1]])
                for i in range(self.nbins)
            ]
        return frame

    def _fftroll(self, ft):
        """Center the FFT so the large-scale modes are shifted
        to the middle of the frame (or vice versa). 2D fft will put them in
        the corners.

        :param ft: 2d FFT (real or real and complex)
        """
        return np.roll(
            np.roll(ft, ft.shape[0] // 2, axis=0), ft.shape[1] // 2, axis=1
        )

    def _rm_mono(self, im):
        """Removes monopole from array.
        If you don't do this before raw angular spec there
        will probably be some ringing in the fft from
        the sharp cut-off to zero at the edges and mask.
        """
        z = np.where(im == 0)
        im -= np.mean(im[im != 0])
        im[z] = 0.0
        return im

    def _rescale(self, arr, high=1, scale=100):
        """ return array between -scale & scale"""
        low = 0.0  # It's always zero.
        return exposure.rescale_intensity(
            arr, out_range=(-high * scale, high * scale)
        )

    def _get_ell(self, nx, ny, center=True):
        """Get multipole moments given the shape of the input image.
        Nyquist frequencies are considered, and large-scales centered
        in freq space are assumed.

        :param nx: width
        :param ny: height
        returns: multipole moment ell, for each pixel.
        """
        lx = np.fft.fftfreq(nx, d=0.5)
        ly = np.fft.fftfreq(ny, d=0.5)

        if center:
            lx = np.roll(lx, nx // 2)
            ly = np.roll(ly, ny // 2)

        xgrid, ygrid = np.meshgrid(lx, ly)
        return np.sqrt(xgrid ** 2 + ygrid ** 2)

    def _powerspec(self, ft2d, nbins=10, center=True):
        """ Input a 2D fft and just take means along annuli """
        cl = np.array(
            [np.mean(ft2d[inside], axis=0) for inside in self._insides]
        )
        if ft2d.shape[-1] == 3:
            cl = cl.T  # One powerspec for each channel, shaped as (nbins, 3)
        l = np.array(self._l)
        return l, cl
