# Combined 4-class model:
# clear, glare, fog, rain
#
import ast
import os
import pickle
import sys

import numpy as np
import PIL
import xgboost as xgb
from PIL import Image
from skimage import filters

from common import logger
from visibility.spec2d import Spec2D

log = logger.get_logger()

# So this stuff is pickle-able :(
_spec = Spec2D(nbins=10)


def _get_features(frame):
    """Input a RGB frame or list of frames and get back
    visibility features, which are fourier statistics and basic
    RGB, HSV and edge statstics

    :param frame: RGB frame (NOT BGR!!)
    :returns: numpy array of features. nbins is defined in ctor
    for power spectra computation (7 is good). Should be the
    same as model was trained with.
    """
    if isinstance(frame, str):
        try:
            rgb = Image.open(frame)
        except IOError:
            raise IOError(f"Input frame path doesn't exist: {frame}")

    elif isinstance(frame, np.ndarray):
        if frame.shape[-1] != 3:
            raise ValueError(
                "Input a RGB tensor; incorrect shape: "
                f"{frame.shape} (or call with feature_batch="
                " if calling from .predict)"
            )
        rgb = Image.fromarray(frame)

    elif isinstance(frame, list):
        return [_get_features(f) for f in frame]

    elif isinstance(frame, PIL.Image.Image) or hasattr(
        frame, "alpha_composite"
    ):
        rgb = frame

    else:
        raise ValueError(
            "Input a np array or string " f"path, not {type(frame)}"
        )

    # Compute features
    _, Cl = _spec.get_power_spec(rgb, rm_monopole=False, rgb=True)
    nprgb = np.array(rgb)
    hists = (
        np.hstack([np.histogram(nprgb[..., i].flatten())[0] for i in range(3)])
        / nprgb.size
        * 100.0
    )

    feature = np.hstack(
        (Cl.flatten(), hists, filters.laplace(rgb).var() * 10)  # blur
    ).astype(np.float16)

    return feature[np.newaxis]


class VisibilityModel:
    def __init__(self, model_path=None, label_map_path=None, nbins=9):
        """
        XGB BUG ANNOUNCEMENT: xgb will raise a FileNotFound error
        if you try to load a model from an absolute path. This
        only happens inside a docker container and seems to be
        a deep-rooted issue with the C code, from what I gather
        here: https://github.com/dmlc/xgboost/issues/3649

        I spent two days on this!
        """
        cwd = os.path.dirname(os.path.realpath(__file__))
        if model_path is None:
            model_path = os.path.join(cwd, "xgb_clf_crossval0.model")

        # If you feed this thing a model_path, make it relative, not absolute!
        if not os.path.isfile(model_path):
            raise IOError(f"No such model file: {model_path}")

        if model_path.endswith(".pkl"):
            # Pickles are python version dependent, so load the right one.
            # It's literally impossible to load from a .json or .model
            # with the XGB api inside a docker container.
            pyversion = "".join(sys.version.split(" ")[0].split("."))[:-1]
            model_path = model_path.replace(".pkl", f".py{pyversion}.pkl")
            if not os.path.isfile(model_path):
                raise ValueError(
                    "No model pkl exists for this python "
                    f"version: {model_path}"
                )
            with open(model_path, "rb") as f:
                self.model = pickle.load(f)
        elif model_path.endswith((".model", ".json")):
            self.model = xgb.Booster()
            self.model.load_model(model_path)
        else:
            raise ValueError(f"Input model name not supported: {model_path}")

        label_path = (
            label_map_path
            if not None
            else os.path.join(cwd, "label_map_vis.pbtxt")
        )
        self.label_map = self._load_label_map(label_path)
        self._label_hash = {v: k for k, v in self.label_map.items()}
        log.info(f"Loaded visibility model: {model_path} and local label map.")
        self._spec = Spec2D(nbins=nbins)
        self.batch_size = 5

    def __repr__(self):
        return self.__class__.__name__

    def __str__(self):
        return self.__repr__()

    def get_features(self, frame):
        return _get_features(frame)

    def predict(
        self,
        frame_batch=None,
        feature_batch=None,
        batch_size=None,
        encoding="probs",
    ):
        """Input a list or tensor of frames and features will be pre-computed
        prior to neural network forward pass. Pass in a list or tensor
        of features and this will just do the forward pass.

        :param frame_batch: frames list or np array
        :param feature_batch: feautre list or np array
        :param encoding: 'str', 'int', 'probs' - how you want the neural
        network outputs encoded. If you set 'str' it will output the argmax class.
        'probs' is the correct way, since in general you will have a mix of
        'glare' and 'clear' for large parts of the day. with a 'probs' output
        encoding you'll get back a list of 4 "probabilities" for each class
        ordered as ['Clear', 'Glare', 'Fog', 'Blur']
        :returns: list of model predictions the same length as input
        """
        if feature_batch is None and frame_batch is None:
            raise ValueError("Input a frame_batch or feature_batch")

        if feature_batch is None:
            if not isinstance(frame_batch, list):
                if isinstance(frame_batch, np.ndarray):
                    if frame_batch.ndim < 3:
                        raise TypeError(
                            "Input a list of np array, "
                            f"not {type(frame_batch)}"
                        )
                    if frame_batch.ndim == 3:
                        frame_batch = [frame_batch]
                else:
                    frame_batch = [frame_batch]

            # maybe pool this - 0.15s per frame
            # feature_batch = np.vstack(self._pool.map(self.get_features, frame_batch))
            feature_batch = np.vstack(
                [self.get_features(frame) for frame in frame_batch]
            )

        else:  # inputted feature_batch
            if isinstance(feature_batch, list):
                feature_batch = np.vstack(feature_batch)

        # TODO these preds will be probabilities. We might
        # need to check instances with low probabilities...
        preds = self.model.predict(xgb.DMatrix(feature_batch))
        assert len(preds) == len(feature_batch)
        if encoding != "probs":
            # If the prediction is low and the encoding is str,
            # give back the probs. Just return a single prediction here.
            opreds = preds.copy()
            maxes = np.max(opreds, axis=1)
            lowidx = np.where(maxes < 0.75)[0]

            # If there're no low predictions, just return the class normally.
            if len(lowidx) == 0:
                ip = np.argmax(preds, axis=1)[0]
                try:
                    plab = self.label_map[ip]
                except KeyError:
                    plab = self._label_hash[ip]
            else:  # uncertain pred
                lowpreds = opreds[lowidx[0]]
                plab = "uncertain: " + ", ".join(
                    [str(f"{p:.2f}") for p in lowpreds]
                )

            return [plab]

        return preds.astype(np.float16)

    def _load_label_map(self, label_map_path):
        """It's either this or a wacky protobuf circle to get this
        into a dict, or a tensorflow object detection API dependency in here.
        """
        if not isinstance(label_map_path, str):
            raise ValueError("Input label_map should be a string path.")
        if not os.path.isfile(label_map_path):
            raise IOError(f"File: {label_map_path} doesn't exist.")
        if not label_map_path.endswith(".pbtxt"):
            raise ValueError(f"Input label map path should be a .pbtxt file.")
        try:
            with open(label_map_path) as f:
                ltxt = f.read()
        except IOError:
            raise IOError(f"Cannot read {label_map_path}.")
        label_map = {}

        for txt in ltxt.split("item")[1:]:
            # pert = txt.replace('\n','').replace(
            #    '{','').replace('}','').replace("'",'').strip().split()
            # label_map[pert[3]] = int(pert[1])
            pert = (
                txt.replace("\n", "")
                .replace("  id:", "")
                .replace("  name:", ":")
                .replace(" {", "{")
            )
            try:
                lm = ast.literal_eval(pert)
            except:
                raise ValueError(f"Unable to evaluate {pert} to literal dict")

            k, v = zip(*lm.items())
            label_map[k[0]] = v[0]
        return label_map
