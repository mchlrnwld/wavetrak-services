import json
import os

import boto3  # type: ignore
import pytest  # type: ignore
import responses  # type: ignore
from moto import mock_s3  # type: ignore

from common import config
from lib.clip import Clip

RECORDING_ID = '58349c1fe411dc743a5d52ad'
CAMERA_ID = '583499c4e411dc743a5d5296'
RECORDING_PATH = '/cameras/recording/'
CROWDS_URL = f'{config.CAMERAS_API}/crowds'
RECORDING_GET_URL = (
    f'{config.CAMERAS_API}/cameras/recording/recordingId/{RECORDING_ID}'
)
CLIP_NAME = 'wc-hbpierns.trim.20211026T165841784.mp4'
RECORDING_BASE_URL = 'fixtures/'


def get_all_keys_from_bucket(bucket: str):
    s3 = boto3.resource('s3')
    bucket_client = s3.Bucket(bucket)
    return list(map(lambda obj: obj.key, bucket_client.objects.all()))


def get_requests(path, calls):
    return [
        call.request for call in calls if call.request.url.startswith(path)
    ]


@pytest.fixture
def create_s3_bucket():
    with mock_s3():
        s3_client = boto3.resource('s3')
        s3_client.create_bucket(Bucket=config.CROWD_FRAME_BUCKET)
        yield s3_client


@pytest.fixture
def intercept_requests():
    with responses.RequestsMock() as rsps:
        rsps.add(responses.POST, CROWDS_URL, body='{"success": true}')
        yield rsps


def test_normal_case(create_s3_bucket, intercept_requests):
    intercept_requests.add(
        responses.GET,
        RECORDING_GET_URL,
        body=json.dumps(
            {
                "_id": RECORDING_ID,
                "cameraId": CAMERA_ID,
                "alias": "a-test",
                "startDate": "2021-02-08T20:21:24.080Z",
                "endDate": "2021-02-08T20:21:54.080Z",
                "recordingUrl": RECORDING_BASE_URL + CLIP_NAME,
                "resolution": "1280x720",
            }
        ),
    )

    clip = Clip.from_recording_id(RECORDING_ID)
    clip.populate_one_minute_frames()
    clip.get_detections()

    crowds_thumbs = get_all_keys_from_bucket(config.CROWD_FRAME_BUCKET)

    still_object_key = (
        f"{CAMERA_ID}/2021/02/08/{CAMERA_ID}.20210208T202124.jpg"
    )
    annotated_object_key = still_object_key.replace('.jpg', '_annotated.jpg')

    expected_keys = [annotated_object_key, still_object_key]

    for expected_key in expected_keys:
        assert expected_key in crowds_thumbs

    # download the actual clip
    # (by now the temp dir it gets stored in will have been destroyed)
    if not os.path.isdir('results'):
        os.mkdir('results')
    s3_client = boto3.resource('s3')
    s3_client.Bucket(config.CROWD_FRAME_BUCKET).download_file(
        annotated_object_key, 'results/test_normal_case_annotated.jpg'
    )

    calls = intercept_requests.calls

    crowds_requests = get_requests(CROWDS_URL, calls)
    assert len(crowds_requests) == 1
    assert json.loads(crowds_requests[0].body) == [
        {
            'timestamp': 1612815684.0,
            'stillObjectKey': still_object_key,
            'cameraId': CAMERA_ID,
            'recordingId': RECORDING_ID,
            'waterCount': clip.frames[0]._water_count,
            'beachCount': clip.frames[0]._beach_count,
            'annotatedObjectKey': annotated_object_key,
            'status': 'CROWD_AVAILABLE',
        }
    ]
