import calendar
import os
import subprocess
import time
from datetime import datetime
from typing import Dict, List, Union
from urllib.parse import urljoin

import requests

from common import logger

_logger = logger.get_logger()


def run_subprocess(cmd: List[str]) -> bytes:
    """
    Given a list of subprocess commands and flags,
    a success message and an error message, run a
    subprocess thread for that cmd.
    """
    joined_command = ' '.join(cmd)

    _logger.info(f'Running subprocess: {joined_command}')
    with open(os.devnull) as devnull:
        try:
            output = subprocess.check_output(
                cmd, stdin=devnull, stderr=subprocess.STDOUT
            )
        except subprocess.CalledProcessError as e:
            _logger.error(e.output)
            raise e
    _logger.debug(f'Command {joined_command}: success.')
    return output


def timestamp_to_epoch(timestamp: str) -> float:
    """
    Given a timestamp and (optionally) a timestamp format,
    return the unix time in seconds.
    """
    date = time.strptime(timestamp, '%Y-%m-%dT%H:%M:%S.%fZ')
    unix_sec = calendar.timegm(date)

    _logger.debug(
        f'Timestamp {timestamp} converted to epoch in ms: {unix_sec}.'
    )

    return unix_sec


def epoch_to_timestamp(
    epoch: Union[int, float], timestamp_format: str = '%Y%m%dT%H%M%S'
) -> str:
    """
    Given a unix epoch time in seconds and (optionally)
    a timestamp format, return a UTC timestamp.
    """
    timestamp = datetime.utcfromtimestamp(epoch).strftime(timestamp_format)
    _logger.debug(
        'Epoch {0} converted to timestamp {1}.'.format(epoch, timestamp)
    )

    return timestamp


def post_request(
    domain: str, endpoint: str, data: Union[Dict, List, str]
) -> Dict:
    """
    Given a domain and endpoint strings, and a dict of data,
    send a HTTP POST request, returning a JSON response.
    """
    url = urljoin(domain, endpoint)
    r = requests.post(url, json=data)
    _logger.info(f'POST {url} with {data}: {r.status_code}')
    r.raise_for_status()

    return r.json()
