import newrelic.agent  # type: ignore

application = newrelic.agent.register_application(timeout=100.0)

import boto3  # type: ignore # noqa

from common import config, logger  # noqa

_logger = logger.get_logger()

sqs = boto3.client("sqs", 'us-west-1')


@newrelic.agent.background_task(application)
def get_next_recording():
    response = sqs.receive_message(
        QueueUrl=config.QUEUE_URL,
        AttributeNames=["SentTimestamp"],
        MaxNumberOfMessages=1,
        MessageAttributeNames=["All"],
        VisibilityTimeout=30,
        WaitTimeSeconds=5,  # long polling, helps w/ empty polling
    )

    if "Messages" in response and len(response["Messages"]) != 0:
        message = response["Messages"][0]
        newrelic.agent.add_custom_parameter(
            "ReceiptHandle", message.get("ReceiptHandle", None)
        )
        sqs.delete_message(
            QueueUrl=config.QUEUE_URL,
            ReceiptHandle=message.get("ReceiptHandle", None),
        )
        return message
    else:
        return None


def poll_for_messages():
    # while loop that runs perpetually for message checking
    while True:
        try:
            next_clip = get_next_recording()
        except Exception as e:
            _logger.exception(e)
            _logger.critical(f'Failed to retrieve next recording')
            continue
        if next_clip:
            yield next_clip
