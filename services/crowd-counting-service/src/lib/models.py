import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from common import config  # noqa E402
from sl_camera_sensor.lib.localization.localize_dets import (  # type: ignore # noqa E402
    LocalizeDets,
)
from sl_camera_sensor.nets.detection.detection_model import (  # type: ignore  # noqa E402
    DetectionModel,
)
from sl_camera_sensor.nets.segmentation.segmentation_model import (  # type: ignore  # noqa E402
    SegmentationModel,
)

detection_bot = DetectionModel(model_name=config.DETECTION_MODEL_NAME)

# Initialize torch segmentation model
segmentation_bot = SegmentationModel()

# Localizer instance to fix bad detections using segmentation
localizer = LocalizeDets(
    dets_label_map=detection_bot.label_map,
    mask_label_map=segmentation_bot.label_map,
    target_size=config.TARGET_FRAME_SIZE[::-1],
)
