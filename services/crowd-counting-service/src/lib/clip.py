import math
from dataclasses import dataclass
from typing import Dict, List

import newrelic.agent  # type: ignore
import requests

from common import config, logger
from lib import utils
from lib.ffmpeg_wrapper import FfmpegWrapper
from lib.frame import Frame
from lib.models import detection_bot, localizer, segmentation_bot
from sl_camera_sensor.lib.crowds.clip_above_thresh import (  # type: ignore
    clip_above_thresh,
)

_logger = logger.get_logger()


@dataclass
class Clip:
    """
    Class that represents everything regarding a clip
    Arguments:
        event: an sns event that is used to initialise the Clip
    Attributes:
        bucket: The name of the S3 bucket where the clip is held
        object_key: The object key of the clip in S3
        camera: The camera that captured the clip
        start_timestamp: The start timestamp of the clip
        width: The width of the clip
        height: The height of the clip
        duration_seconds: The duration of the clip in seconds
    """

    camera_id: str
    alias: str
    start_timestamp: int
    start_date_string: str
    end_timestamp: int
    duration_seconds: int
    recording_url: str
    resolution: str
    recording_id: str

    def __post_init__(self) -> None:
        self.ffmpeg = FfmpegWrapper()
        self.failed_frames: List[Dict] = []
        self.frames: List[Frame] = []

    @staticmethod
    def fetch_recording(recording_id):
        url = requests.compat.urljoin(
            config.CAMERAS_API,
            f'cameras/recording/recordingId/{recording_id}',
        )
        r = requests.get(url)
        _logger.info(f'GET {url}: {r.status_code}')
        r.raise_for_status()

        return r.json()

    @classmethod
    def from_recording_id(cls, recording_id):
        recording = cls.fetch_recording(recording_id)
        start_timestamp = utils.timestamp_to_epoch(recording['startDate'])
        end_timestamp = utils.timestamp_to_epoch(recording['endDate'])
        duration_seconds = end_timestamp - start_timestamp
        return cls(
            recording_id=recording_id,
            camera_id=recording['cameraId'],
            alias=recording['alias'],
            start_timestamp=start_timestamp,
            start_date_string=recording['startDate'],
            end_timestamp=end_timestamp,
            duration_seconds=duration_seconds,
            recording_url=recording['recordingUrl'],
            resolution=recording['resolution'],
        )

    def populate_one_minute_frames(self):
        """
        Creates one thumbnail
        image per minute, adding their paths
        to a list.
        """
        num_thumbs = math.ceil(float(self.duration_seconds) / 60)

        for clip_time in range(num_thumbs):
            timestamp = self.start_timestamp + (60 * clip_time)

            try:
                frame_file = self.ffmpeg.create_thumbnail(
                    self.recording_url, clip_time
                )
            except Exception as e:
                _logger.exception(e)
                _logger.error(f'Frame extraction failed for {timestamp}')
                newrelic.agent.notice_error(
                    attributes={"timestamp": timestamp}
                )
                self.failed_frames.append(
                    {
                        'timestamp': timestamp,
                        'cameraId': self.camera_id,
                        'status': 'FATAL_ERROR',
                    }
                )
                continue

            frame = Frame(
                filename=frame_file,
                camera_id=self.camera_id,
                alias=self.alias,
                timestamp=timestamp,
                recording_id=self.recording_id,
            )

            frame.try_upload_to_s3()

            try:
                frame.setup()
            except Exception as e:
                _logger.exception(e)
                _logger.error('frame setup failed')
                newrelic.agent.notice_error(
                    attributes={
                        "timestamp": timestamp,
                        's3_key': frame._s3_key,
                    },
                )
                self.failed_frames.append(
                    {
                        'timestamp': timestamp / 1000,
                        'cameraId': self.camera_id,
                        'stillObjectKey': frame._s3_key,
                        'status': 'FATAL_ERROR',
                    }
                )
                continue

            self.frames.append(frame)

    def get_detections(self):
        if len(self.frames) == 0:
            self.post_to_crowds_api()
        frame_data = [fr.array for fr in self.frames]

        masks = segmentation_bot.predict(frame_data[0])
        detections = detection_bot.predict(frame_data)
        # Filter/fix low-conf/false-positive detections
        filtered_detections = clip_above_thresh(detections, thresh=0.2)
        localized_detections = localizer(filtered_detections, masks[0])

        self.count_detections(localized_detections)
        self.annotate_frames()
        self.post_to_crowds_api()

        if all([frame.status == 'FATAL_ERROR' for frame in self.frames]):
            raise Exception('All frames in clip failed to process')

        return self.get_average_counts()

    def count_detections(self, detections):
        for index, frame in enumerate(self.frames):
            # Convert numpy types to regular python types, we don't need the
            # additional precision numpy.float16 provides at this point.
            frame._detections = detections[index]
            frame.try_count_detections()

    def annotate_frames(self):
        for frame in self.frames:
            frame.try_create_annotated_frame()

    def post_to_crowds_api(self):
        crowds = self.failed_frames + [
            frame.get_crowd_dict() for frame in self.frames
        ]
        utils.post_request(config.CAMERAS_API, '/crowds', crowds)

    def get_average_counts(self):
        water_count_sum = sum([frame._water_count for frame in self.frames])
        beach_count_sum = sum([frame._beach_count for frame in self.frames])

        water_count = water_count_sum / len(self.frames)
        beach_count = beach_count_sum / len(self.frames)

        return (water_count, beach_count)
