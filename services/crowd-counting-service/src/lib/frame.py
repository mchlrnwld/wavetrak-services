# -*- coding: utf-8 -*-
"""
  Class makes it easier to work with rewind files.
"""
import io  # type: ignore
from dataclasses import dataclass

import boto3  # type: ignore
import newrelic.agent  # type: ignore
import numpy as np  # type: ignore
import PIL.ImageColor as ImageColor  # type: ignore
import PIL.ImageDraw as ImageDraw  # type: ignore
import PIL.ImageFont as ImageFont  # type: ignore
from PIL import Image  # type: ignore
from PIL import ImageFile  # type: ignore

from common import config, logger
from lib import utils
from sl_camera_sensor.lib.crowds.crowd_counter import (  # type: ignore
    CrowdCounter,
)

_logger = logger.get_logger()

# Tell PILL to be tolerant of files that are truncated
# i.e. bad JPEG image that's only partially complete.
ImageFile.LOAD_TRUNCATED_IMAGES = True


@dataclass
class Frame:
    def __init__(
        self, filename, camera_id, alias, timestamp, recording_id
    ) -> None:
        self._filename = filename
        self._camera_id = camera_id
        self._alias = alias
        self._timestamp = timestamp
        self.recording_id = recording_id
        self._s3_key = None
        self._s3_key_annotated = None

    def setup(self):
        self._image = Image.open(self._filename)
        self._resize_frame()
        self.array = self.get_image_as_array()

    """
    === PUBLIC INTERFACE ===
    """

    def get_image_as_array(self):
        return np.array(self._image)

    def try_count_detections(self):
        try:
            cc = CrowdCounter()

            # Class requires a timestamp key for internal storage.
            dets = self._detections
            dets["timestamp"] = self._timestamp

            sand_cnt, surf_cnt = cc.get_count(dets, thresh=0.25)

            self._beach_count = sand_cnt
            self._water_count = surf_cnt

            _logger.info(f'Water count: {self._water_count}')
            _logger.info(f'Beach count: {self._beach_count}')

            self.status = 'CROWD_AVAILABLE'
        except Exception as e:
            _logger.exception(e)
            newrelic.agent.notice_error(attributes={'s3_key': self._s3_key})
            self._beach_count = None
            self._water_count = None
            self.status = 'FATAL_ERROR'

    def try_create_annotated_frame(
        self,
    ):
        if self.status == 'FATAL_ERROR':
            return
        try:
            frame = self._image.convert("RGBA")

            for cls in config.CLASSES_TO_ANNOTATE:
                # Make color with alpha channel!!
                color = ImageColor.getrgb(config.COLORS_TO_USE[cls])

                # no threshold at this stage as the detections have
                # already been filtered below a threshold and we
                # want to see all the detections that were
                # reflected in the count
                cls_idxs = np.where(
                    (self._detections["detection_classes"] == cls)
                )[0]
                boxes = self._detections["detection_boxes"][cls_idxs]
                scores = self._detections["detection_scores"][cls_idxs]

                strs = ["{:.0f}%".format(score * 100) for score in scores]

                self._draw_bounding_boxes_on_image(
                    frame,
                    boxes,
                    color=color,
                    thickness=2,
                    display_str_list=strs,
                )

            self._annotated_frame = frame.convert("RGB")

            self._upload_annotated_frame()
        except Exception as e:
            _logger.exception(e)
            _logger.error('frame annotation failed')

    def try_upload_to_s3(self):
        """
        Upload thumbnail to the crowds S3 bucket.
        """
        s3_key = self._generate_s3_key()

        s3 = boto3.client('s3')
        try:
            s3.upload_file(
                self._filename,
                config.CROWD_FRAME_BUCKET,
                s3_key,
                ExtraArgs={'ContentType': 'image'},
            )
        except Exception as e:
            # non blocking, the frames are only for debugging in s3
            _logger.exception(e)
            _logger.error(f'{self._filename} failed to save at {s3_key}')
            return

        self._s3_key = s3_key

        _logger.info(f'{self._filename} saved at {self._s3_key}')

    def get_crowd_dict(self):
        return {
            'timestamp': self._timestamp,
            'stillObjectKey': self._s3_key,
            'cameraId': self._camera_id,
            'recordingId': self.recording_id,
            'waterCount': self._water_count,
            'beachCount': self._beach_count,
            'annotatedObjectKey': self._s3_key_annotated,
            'status': self.status,
        }

    """
    === PRIVATE INTERFACE ===
    """

    def _upload_annotated_frame(self):
        """
        Puts annotated frame in S3.
        """

        # Create encoded jpeg in memory
        with io.BytesIO() as byte_io:
            self._annotated_frame.save(
                byte_io, "JPEG", quality=90, progressive=True
            )
            byte_io.seek(0)

            self._s3_key_annotated = self._generate_s3_key(annotated=True)

            boto3.client("s3").put_object(
                Body=byte_io,
                Bucket=config.CROWD_FRAME_BUCKET,
                Key=self._s3_key_annotated,
                ACL="public-read",
                ContentType="image/jpeg",
            )

            _logger.info(
                "Uploaded "
                f"s3://{config.CROWD_FRAME_BUCKET}/{self._s3_key_annotated}"
            )

    def _generate_s3_key(self, annotated=False):
        date = utils.epoch_to_timestamp(
            self._timestamp, timestamp_format='%Y/%m/%d'
        )
        start_timestamp = utils.epoch_to_timestamp(self._timestamp)
        file_name = (
            f"{self._camera_id}.{start_timestamp}"
            f"{'_annotated' if annotated else ''}.jpg"
        )
        return f"{self._camera_id}/{date}/{file_name}"

    def _resize_frame(self):
        resize_width, resize_height = config.TARGET_FRAME_SIZE
        source_width, source_height = self._image.size

        if (
            source_width is not resize_width
            and source_height is not resize_height
        ):
            self._image = self._image.resize(
                (resize_width, resize_height), Image.LANCZOS
            )

    def _draw_bounding_boxes_on_image(
        self, image, boxes, color=(255, 0, 0), thickness=5, display_str_list=()
    ):
        """Draws bounding boxes on image.

        Args:
            image (PIL.Image): PIL.Image object
            boxes (np.array): a 2 dimensional numpy array
                of [N, 4]: (ymin, xmin, ymax, xmax)
                The coordinates are in normalized format between [0, 1]
            color (int, int, int): RGB tuple describing color of bounding box
            thickness (int): bounding box line thickness
            display_str_list [str]: list of strings.
                Contains one string for each bounding box.
        Raises:
            ValueError: if boxes is not a [N, 4] array
        """
        boxes_shape = boxes.shape
        if not boxes_shape:
            return
        if len(boxes_shape) != 2 or boxes_shape[1] != 4:
            raise ValueError("boxes must be of size [N, 4]")
        for i in range(boxes_shape[0]):
            self._draw_bounding_box_on_image(
                image,
                boxes[i, 0],
                boxes[i, 1],
                boxes[i, 2],
                boxes[i, 3],
                color,
                thickness,
                display_str_list[i],
            )

    def _draw_bounding_box_on_image(
        self,
        image,
        ymin,
        xmin,
        ymax,
        xmax,
        color=(255, 0, 0),
        thickness=4,
        display_str="",
        use_normalized_coordinates=True,
    ):
        """Adds a bounding box to an image.

        Bounding box coordinates can be specified in either absolute (pixel) or
        normalized coordinates by setting the use_normalized_coordinates
        argument.

        The string passed in display_str is displayed above the
        bounding box in black text on a rectangle filled with
        the input 'color'.
        If the top of the bounding box extends to the edge of the image,
        the string is displayed below the bounding box.

        Args:
            image (PIL.Image): PIL.Image object
            ymin (float): ymin of bounding box
            xmin (float): xmin of bounding box
            ymax (float): ymax of bounding box
            xmax (float): xmax of bounding box
            color (int, int, int): RGB tuple describing color of bounding box
            thickness (int): line thickness
            display_str (str): string to display in box
            use_normalized_coordinates (bool): If True, treat coordinates
                ymin, xmin, ymax, xmax as relative to the image.
                Otherwise treat coordinates as absolute
        """
        fill = tuple(color) if not isinstance(color, str) else color
        draw = ImageDraw.Draw(image)
        im_width, im_height = image.size
        if use_normalized_coordinates:
            (left, right, top, bottom) = (
                xmin * im_width,
                xmax * im_width,
                ymin * im_height,
                ymax * im_height,
            )
        else:
            (left, right, top, bottom) = (xmin, xmax, ymin, ymax)
        draw.line(
            [
                (left, top),
                (left, bottom),
                (right, bottom),
                (right, top),
                (left, top),
            ],
            width=thickness,
            fill=fill,
        )

        if display_str == "":
            return
        try:
            font = ImageFont.truetype("arial.ttf", 24)
        except IOError:
            font = ImageFont.load_default()

        # If the total height of the display string added to the top of the
        # bounding box exceeds the top of the image, move the string below
        # the bounding box instead of above
        display_str_height = font.getsize(display_str)[1]
        # Each display_str has a top and bottom margin of 0.05x
        total_display_str_height = (1 + 2 * 0.05) * display_str_height

        if top > total_display_str_height:
            text_bottom = top
        else:
            text_bottom = bottom + total_display_str_height

        text_width, text_height = font.getsize(display_str)
        margin = np.ceil(0.05 * text_height)
        draw.rectangle(
            [
                (left, text_bottom - text_height - 2 * margin),
                (left + text_width, text_bottom),
            ],
            fill=fill,
        )

        draw.text(
            (left + margin, text_bottom - text_height - margin),
            display_str,
            fill="black",
            font=font,
        )
        text_bottom -= text_height - 2 * margin
