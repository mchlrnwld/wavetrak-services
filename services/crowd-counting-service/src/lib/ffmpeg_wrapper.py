# -*- coding: utf-8 -*-
""" Ffmpeg API wrapper methods module """

import tempfile

import newrelic.agent  # type: ignore

from common import logger
from lib import utils

application = newrelic.agent.application()
_logger = logger.get_logger()


class FfmpegWrapper:
    def __init__(self):
        # ref so folder is only deleted when class obj is destroyed
        self.temp_dir_obj = tempfile.TemporaryDirectory()
        self.temp_dir = self.temp_dir_obj.name

    def create_thumbnail(self, recording_file: str, clip_time: int = 0) -> str:
        """
        Given a clip filename and path creates a thumbnail
        image in several resolutions, adding their paths
        to a list.
        """
        img_out_path = f'{self.temp_dir}/{clip_time}.jpg'
        _logger.info(f'Creating thumbnail for {img_out_path}')
        minutes = str(clip_time).zfill(2)

        cmd = [
            'ffmpeg',
            '-loglevel',
            'error',
            '-nostats',
        ]

        if clip_time > 0:
            # fast seeking fails when clip is less than 1 min long
            cmd += [
                '-ss',
                f'00:{minutes}:00',
            ]
        cmd += [
            '-i',
            recording_file,
            '-vframes',
            '1',
            '-abort_on',
            'empty_output',
            '-y',
            img_out_path,
        ]

        utils.run_subprocess(cmd)

        return img_out_path
