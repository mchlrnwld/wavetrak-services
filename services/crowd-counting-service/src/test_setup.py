import os

os.environ['CAMERAS_API'] = 'http://test-cameras.com'
os.environ['CROWD_FRAME_BUCKET'] = 'crowd_thumbs'
os.environ['CROWD_COUNTING_QUEUE_URL'] = 'test_queue'
