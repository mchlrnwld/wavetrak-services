#!/usr/bin/python
# -*- coding: utf-8 -*-

import newrelic.agent  # type: ignore

from common import config

newrelic.agent.initialize('./newrelic.ini', config.ENV)
application = newrelic.agent.register_application(timeout=100.0)

import json  # noqa E402
import time  # noqa E402

from common import logger  # noqa E402
from lib.clip import Clip  # noqa E402
from lib.queue_ops import poll_for_messages  # noqa E402

_logger = logger.get_logger()


@newrelic.agent.background_task(application)
def process_recording(message):
    message_body = json.loads(message["Body"])
    recording_id = message_body['recordingId']
    newrelic.agent.add_custom_parameter('recordingId', recording_id)

    start_time = time.time()

    clip = Clip.from_recording_id(recording_id)

    newrelic.agent.add_custom_parameter(
        'queueTime',
        int(start_time - clip.end_timestamp),
    )
    newrelic.agent.add_custom_parameter('alias', clip.alias)
    newrelic.agent.add_custom_parameter('recording_url', clip.recording_url)

    clip.populate_one_minute_frames()
    newrelic.agent.add_custom_parameter('frame_count', len(clip.frames))

    water_count, beach_count = clip.get_detections()

    newrelic.agent.add_custom_parameter('waterCount', water_count)
    newrelic.agent.add_custom_parameter('beachCount', beach_count)


def main():
    for recording_id in poll_for_messages():
        # by putting the catch outside the transaction we can see failures
        # in the standard way in newrelic
        try:
            process_recording(recording_id)
        except Exception as e:
            _logger.exception(e)
            _logger.critical(f'Crowd Counting Failed for {recording_id}')


if __name__ == "__main__":
    main()
