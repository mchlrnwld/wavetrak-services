import os

# setting these in here so they are static across testing/deployed envs
# can't just use config as these are referenced in `sl-camera-sensor` too
os.environ['GRAPHS_DIR'] = '/opt/ml/models'
os.environ['SEGMENTATION_MODEL_LOCATION'] = 'Segmentation/'
os.environ['SEGMENTATION_MODEL_NAME'] = 'deeplabv3-resnet50-720p-tdv7.pt'

os.environ['DETECTION_MODEL_LOCATION'] = 'ObjectDetection/'
os.environ['DETECTION_MODEL_NAME'] = 'retinanet-resnet50-1080p-bs2-tdv5.plan'

CAMERAS_API = os.environ['CAMERAS_API']
COLORS_TO_USE = [
    "AliceBlue",
    "DodgerBlue",
    "SpringGreen",
    "SpringGreen",
    "SpringGreen",
    "SpringGreen",
]
CLASSES_TO_ANNOTATE = [1, 2, 3, 4, 5]
CROWD_FRAME_BUCKET = os.environ['CROWD_FRAME_BUCKET']
DETECTION_MODEL_NAME = os.environ['DETECTION_MODEL_NAME']
ENV = os.environ['APP_ENV']
FRAMES_PER_MINUTE = 1
QUEUE_URL = os.environ['CROWD_COUNTING_QUEUE_URL']
TARGET_FRAME_SIZE = (1920, 1080)
