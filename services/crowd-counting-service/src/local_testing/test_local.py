import json
import os

import boto3  # type: ignore
import pytest  # type: ignore
import responses  # type: ignore
from moto import mock_s3  # type: ignore

from common import config
from lib.clip import Clip

RECORDING_ID = '58349c1fe411dc743a5d52ad'
CAMERA_ID = '583499c4e411dc743a5d5296'
RECORDING_PATH = '/cameras/recording/'
CROWDS_URL = f'{config.CAMERAS_API}/crowds'
RECORDING_GET_URL = (
    f'{config.CAMERAS_API}/cameras/recording/recordingId/{RECORDING_ID}'
)
CLIP_NAME = 'wc-hbpierns.trim.20211026T165841784.mp4'
RECORDING_BASE_URL = 'local_testing/test_clips/'


@pytest.fixture
def create_s3_bucket():
    with mock_s3():
        s3_client = boto3.resource('s3')
        s3_client.create_bucket(Bucket=config.CROWD_FRAME_BUCKET)
        yield s3_client


@pytest.fixture
def intercept_requests():
    with responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
        rsps.add(responses.POST, CROWDS_URL, body='{"success": true}')
        yield rsps


def test_all(create_s3_bucket, intercept_requests):
    if not os.path.isdir(RECORDING_BASE_URL):
        return
    for file in os.listdir(RECORDING_BASE_URL):
        if file.endswith(".mp4"):
            intercept_requests.add(
                responses.GET,
                RECORDING_GET_URL,
                body=json.dumps(
                    {
                        "_id": RECORDING_ID,
                        "cameraId": CAMERA_ID,
                        "alias": "a-test",
                        "startDate": "2021-02-08T20:21:24.080Z",
                        "endDate": "2021-02-08T20:31:24.080Z",
                        "recordingUrl": RECORDING_BASE_URL + file,
                        "resolution": "1280x720",
                    }
                ),
            )

            clip = Clip.from_recording_id(RECORDING_ID)
            clip.populate_one_minute_frames()
            water_count, beach_count = clip.get_detections()

            print(f'\n average water count: {water_count}')
            print(f'\n average beach count: {beach_count}')
            for i, frame in enumerate(clip.frames):
                print(f'FRAME: {i}')
                print(f'\n water count: {frame._water_count}')
                print(f'\n beach count: {frame._beach_count}')

            print(f'Frame count: {len(clip.frames)}')

            # download the actual clip
            # (by now the temp dir it gets stored in will have been destroyed)
            if not os.path.isdir('results'):
                os.mkdir('results')
            s3_client = boto3.resource('s3')
            bucket = s3_client.Bucket(config.CROWD_FRAME_BUCKET)
            i = 1
            for obj in bucket.objects.all():
                if 'annotated' in obj.key:
                    bucket.download_file(
                        obj.key,
                        (
                            f'results/{file.replace(".mp4", "")}'
                            f'_{i}_annotated.jpg'
                        ),
                    )
                    i += 1
