[
  {
    "name": "${name}",
    "image": "${image}:${version}",
    "memory": ${memory},
    "cpu": ${cpu},
    "resourceRequirements": [
         {
           "type":"GPU",
           "value": "${gpu}"
         }
      ],
    "entryPoint": [
      "python",
      "/usr/src/app/entrypoint.py"
    ],
    "portMappings": [
      {
        "hostPort": 0,
        "containerPort": ${port},
        "protocol": "tcp"
      }
    ],
    "environment": [
      {
        "name": "APP_VERSION",
        "value": "${version}"
      }
    ]
  }
]
