# Crowd Counting Service

non http service to generate crowd counts from rewinds. It burns down an sqs queue with a message for every rewind from cameras that are compatible and have sufficient daylight for the model to work