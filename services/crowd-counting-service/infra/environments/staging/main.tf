provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "wavetrak-core-gpu/staging/crowd-counting-service.tfstate"
    region = "us-west-1"
  }
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/staging/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/staging/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

module "service" {
  source = "../../"

  company     = "wt"
  application = "crowd-counting-service"
  environment = "staging"

  vpc_id      = "vpc-981887fd"
  dns_name    = "crowd-counting-service.staging.surfline.com"
  dns_zone_id = "Z3JHKQ8ELQG5UE"

  ecs_cluster   = "wt-core-gpu-staging"
  iam_role      = "wt-core-gpu-iam-role-ecs-service-staging"
  load_balancer = "wt-core-gpu-alb-staging"
}
