provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "wavetrak-core-gpu/sandbox/crowd-counting-service.tfstate"
    region = "us-west-1"
  }
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/sandbox/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/sandbox/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

module "service" {
  source = "../../"

  company     = "wt"
  application = "crowd-counting-service"
  environment = "sandbox"

  vpc_id      = "vpc-981887fd"
  dns_name    = "crowd-counting-service.sandbox.surfline.com"
  dns_zone_id = "Z3DM6R3JR1RYXV"

  ecs_cluster   = "wt-core-gpu-sandbox"
  iam_role      = "wt-core-gpu-iam-role-ecs-service-sandbox"
  load_balancer = "wt-core-gpu-alb-sandbox"
}
