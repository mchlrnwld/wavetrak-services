provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "wavetrak-core-gpu/prod/crowd-counting-service.tfstate"
    region = "us-west-1"
  }
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/prod/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/prod/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

module "service" {
  source = "../../"

  company     = "wt"
  application = "crowd-counting-service"
  environment = "prod"

  vpc_id      = "vpc-116fdb74"
  dns_name    = "crowd-counting-service.prod.surfline.com"
  dns_zone_id = "Z3LLOZIY0ZZQDE"

  ecs_cluster   = "wt-core-gpu-prod"
  iam_role      = "wt-core-gpu-iam-role-ecs-service-prod"
  load_balancer = "wt-core-gpu-alb-prod"
}
