provider "aws" {
  region = "us-west-1"
}

data "aws_alb" "main" {
  name = var.load_balancer
}

data "aws_alb_listener" "main" {
  load_balancer_arn = data.aws_alb.main.arn
  port              = 80
}

data "aws_iam_role" "main" {
  name = var.iam_role
}

locals {
  region = "us-west-1"

  service = {
    name        = var.application
    image       = "833713747344.dkr.ecr.us-west-1.amazonaws.com/services/crowd-counting-service"
    tag         = "latest"
    port        = 8080
    count       = 1
    cpu         = 4
    gpu         = 1
    memory      = 15000
    service_alb_priority    = 102
    healthcheck = "/health"
  }

  bucket = {
    name = "${var.application}-frames-${var.environment}"
  }

  queue = {
    timeout = 600
  }

  tags = {
    Company     = var.company
    Application = var.application
    Environment = var.environment
    Service     = "ecs"
    Terraform   = "true"
    Source      = "Surfline/wavetrak-services/services/crowd-counting-service"
  }
}

resource "aws_ecs_task_definition" "main" {
  family = "${var.company}-core-gpu-${var.environment}-${var.application}"

  container_definitions = templatefile("${path.module}/resources/container-definitions.tpl", {
    cpu     = local.service.cpu
    gpu     = local.service.gpu
    image   = local.service.image
    name    = local.service.name
    memory  = local.service.memory
    port    = local.service.port
    version = local.service.tag
  })

  tags = local.tags
}

module "crowd_counting_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company     = var.company
  application = var.application
  environment = var.environment

  service_name = local.service.name
  service_port = local.service.port

  default_vpc = var.vpc_id
  dns_name    = var.dns_name
  dns_zone_id = var.dns_zone_id

  ecs_cluster               = var.ecs_cluster
  service_td_name           = aws_ecs_task_definition.main.family
  service_td_container_name = local.service.name
  service_td_count          = local.service.count

  service_alb_priority        = local.service.service_alb_priority
  service_lb_healthcheck_path = local.service.healthcheck
  service_lb_rules = [
    {
      field = "host-header"
      value = var.dns_name
    },
  ]

  load_balancer_arn = data.aws_alb.main.arn
  alb_listener      = data.aws_alb_listener.main.arn
  iam_role_arn      = data.aws_iam_role.main.arn

  is_http_service      = false
  auto_scaling_enabled = false
}

resource "null_resource" "deploy_default_service" {
  provisioner "local-exec" {
    command = <<EOT
      taskDefinition=$(
        aws ecs list-task-definitions \
          --region "${local.region}" \
          --family-prefix "${aws_ecs_task_definition.main.family}" | \
        jq -r '.taskDefinitionArns[-1]'
      )
      echo $taskDefinition
      aws ecs update-service \
        --cluster "${var.ecs_cluster}" \
        --service "${var.company}-${local.service.name}-${var.environment}" \
        --region "${local.region}" \
        --task-definition "$taskDefinition"
    EOT
  }

  depends_on = [
    aws_ecs_task_definition.main,
    module.crowd_counting_service
  ]
}

# crowd counting queue

module "crowd_counting_queue" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/sqs"

  environment                = var.environment
  queue_name                 = "${var.company}-${var.application}-sqs"
  visibility_timeout_seconds = local.queue.timeout
}

resource "aws_ssm_parameter" "crowd_counting_queue_url" {
  name = "/${var.environment}/services/crowd-counting-service/CROWD_COUNTING_QUEUE_URL"
  value = module.crowd_counting_queue.queue_url
  type = "String"
}

resource "aws_iam_policy" "crowds_queue_mgmt_policy" {
  name        = "${var.company}-${var.application}-highlights-sqs-mgmt-policy-${var.environment}"
  description = "policy to allow recieve send messages for sqs"
  policy = templatefile("${path.module}/resources/sqs-policy.json", {
    sqs_queue = module.crowd_counting_queue.queue_arn
  })
}

resource "aws_iam_role_policy_attachment" "crowd_counting_service_rewind_clips_sqs" {
  role       = module.crowd_counting_service.task_role_name
  policy_arn = aws_iam_policy.crowds_queue_mgmt_policy.arn
}
# crowd frames bucket

resource "aws_s3_bucket" "crowds_bucket" {
  bucket = local.bucket.name
  acl    = "private"
  tags   = local.tags
}

resource "aws_ssm_parameter" "crowds_bucket" {
  name = "/${var.environment}/services/crowd-counting-service/CROWD_FRAME_BUCKET"
  value = local.bucket.name
  type = "String"
}

resource "aws_iam_policy" "crowds_bucket_mgmt_policy" {
  name        = "${var.company}-${var.application}-highlights-bucket-mgmt-policy-${var.environment}"
  description = "policy to manage objects in an s3 bucket"
  policy = templatefile("${path.module}/resources/s3-access-policy.json", {
    bucket_name = local.bucket.name
  })
}

resource "aws_iam_role_policy_attachment" "highlights_bucket_mgmt_policy_attachement" {
  role       = module.crowd_counting_service.task_role_name
  policy_arn = aws_iam_policy.crowds_bucket_mgmt_policy.arn
}
