variable "company" {
}

variable "application" {
}

variable "environment" {
}

variable "default_vpc" {
}

variable "ecs_cluster" {
}

variable "alb_listener_arn" {
}

variable "service_td_count" {
}

variable "service_lb_rules" {
  type = list(map(string))
}

variable "iam_role_arn" {
}

variable "dns_name" {
}

variable "dns_zone_id" {
}

variable "load_balancer_arn" {
}

variable "auto_scaling_enabled" {
  default = false
}

variable "auto_scaling_scale_by" {
  default = "alb_response_latency"
}

variable "auto_scaling_min_size" {
  default = ""
}

variable "auto_scaling_max_size" {
  default = ""
}

variable "auto_scaling_target_value" {
}
