provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "promotions-service/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  dns_name = "promotions-service.sandbox.surfline.com"
}

module "promotions_service" {
  source = "../../"

  company     = "sl"
  application = "promotions-service"
  environment = "sandbox"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-sandbox"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-3-sandbox/9ff25c35a340093b/66fd46bbe4d5aa9e"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-3-sandbox/9ff25c35a340093b"

  dns_name    = local.dns_name
  dns_zone_id = "Z3DM6R3JR1RYXV"

  auto_scaling_enabled      = false
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = ""
}
