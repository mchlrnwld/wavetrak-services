provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "promotions-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-3-prod"
}

data "aws_route53_zone" "dns_zone" {
  name         = "prod.surfline.com."
  private_zone = false
}

data "aws_route53_zone" "legacy_dns_zone" {
  name         = "surfline.com."
  private_zone = false
}

locals {
  dns_name        = "promotions-service.prod.surfline.com"
  legacy_dns_name = "promotions-service.surfline.com"
}

resource "aws_route53_record" "promotions_legacy_dns" {
  zone_id = data.aws_route53_zone.legacy_dns_zone.zone_id
  name    = local.legacy_dns_name
  type    = "A"
  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = false
  }
}

module "promotions_service" {
  source = "../../"

  company     = "sl"
  application = "promotions-service"
  environment = "prod"

  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "host-header"
      value = local.legacy_dns_name
    },
  ]

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b/4796722fa18acc2f"
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b"

  dns_name    = local.dns_name
  dns_zone_id = data.aws_route53_zone.dns_zone.zone_id

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 2500
  auto_scaling_min_size     = 4
  auto_scaling_max_size     = 5000
}
