provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "promotions-service/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  dns_name = "promotions-service.staging.surfline.com"
}

module "promotions_service" {
  source = "../../"

  company     = "sl"
  application = "promotions-service"
  environment = "staging"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-staging"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-3-staging/bca6896a1b354474/e10b8d509912ed71"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-3-staging/bca6896a1b354474"

  dns_name    = local.dns_name
  dns_zone_id = "Z3JHKQ8ELQG5UE"

  auto_scaling_enabled      = false
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = ""
}
