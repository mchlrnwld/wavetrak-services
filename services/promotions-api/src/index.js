import 'babel-polyfill';
import 'newrelic';
import { initMongoDB, initCache } from './models/dbContext';
import app from './server';
import logger from './common/logger';

const log = logger('promotions-service');

const start = async () => {
  try {
    await Promise.all([initCache(), initMongoDB()]);
    app(log);
  } catch (err) {
    log.error({
      message: "App Initialization failed. Can't connect to database",
      stack: err,
    });
  }
};

start();
