import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import {
  createPromotionHandler,
  updatePromotionHandler,
  deletePromotionHandler,
  getPromotionHandler,
  getPromotionsHandler,
  getPerksHeroHandler,
  updatePerksHeroHandler,
  deletePerksHeroHandler,
} from './promotions';
import {
  getPromotionCodesHandler,
  createPromotionCodesHandler,
  insertPromotionCodesHandler,
} from './promotionCodes';
import trackRequests from '../../common/trackRequests';

const promotions = (log) => {
  const api = Router();
  api.use('*', trackRequests(log));
  api.get('/promotions/perks-hero', wrapErrors(getPerksHeroHandler));
  api.post('/promotions/perks-hero', wrapErrors(updatePerksHeroHandler));
  api.delete('/promotions/perks-hero', wrapErrors(deletePerksHeroHandler));
  api.get('/promotions', wrapErrors(getPromotionsHandler));
  api.get('/promotions/:id', wrapErrors(getPromotionHandler));
  api.post('/promotions', wrapErrors(createPromotionHandler));
  api.put('/promotions/:id', wrapErrors(updatePromotionHandler));
  api.delete('/promotions/:id', wrapErrors(deletePromotionHandler));
  api.get('/promotions/:id/codes', wrapErrors(getPromotionCodesHandler));
  api.post('/promotions/:id/internal-codes', wrapErrors(createPromotionCodesHandler));
  api.post('/promotions/:id/partner-codes', wrapErrors(insertPromotionCodesHandler));

  return api;
};

export default promotions;
