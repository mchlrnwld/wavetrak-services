import {
  getPromotionCodes,
  createInternalPromotionCodes,
  insertPartnerPromotionCodes,
} from '../../models/promotionCode';
import { incrementPromotionMaxRedemption } from '../../models/promotion';

export async function getPromotionCodesHandler(req, res) {
  try {
    const { params: { id } } = req;
    const promotionCodes = await getPromotionCodes({
      promotion: id,
    });
    return res.send({ promotionCodes });
  } catch (error) {
    return res.status(500).json({ message: error });
  }
}

export async function createPromotionCodesHandler(req, res) {
  try {
    const {
      body: { promotionCodePrefix, amount },
      params: { id },
    } = req;
    const promotionCodes = await createInternalPromotionCodes(id, promotionCodePrefix, amount);
    const numCodes = promotionCodes.length;
    return res.send({ codes: promotionCodes, message: `Successfully generated ${numCodes} promotion codes` });
  } catch (error) {
    return res.status(500).json({ message: error });
  }
}

export async function insertPromotionCodesHandler(req, res) {
  try {
    const {
      body: { codes },
      params: { id },
    } = req;
    const promotionCodes = await insertPartnerPromotionCodes(id, codes);
    const numCodes = promotionCodes.length;
    await incrementPromotionMaxRedemption(id, { amount: numCodes });
    return res.send({ message: `Successfully uploaded ${numCodes} promotion codes` });
  } catch (error) {
    return res.status(500).json({ message: error });
  }
}
