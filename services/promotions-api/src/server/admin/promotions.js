import logger from '../../common/logger';
import {
  createDiscriminatedPromotion,
  updatePromotion,
  deletePromotion,
  getPromotionById,
  getPromotions,
} from '../../models/promotion';
import { redisPromise } from '../../models/dbContext';

const log = logger('promotions-service');

function performValidation(body) {
  const { kind } = body;
  const requiredFields = kind === 'Perk' ? ['name', 'brand', 'kind'] : ['name', 'urlKey', 'brand', 'kind'];
  const result = requiredFields.every(field => (
    Object.prototype.hasOwnProperty.call(body, field)));
  return result;
}


export async function getPromotionsHandler(req, res) {
  try {
    const promotions = await getPromotions();
    return res.send(promotions);
  } catch (err) {
    return res.status(500).send({ message: err });
  }
}


export async function getPromotionHandler(req, res) {
  try {
    const { params: { id } } = req;
    const promotion = await getPromotionById(id);
    if (!promotion) {
      return res.status(400).json({ message: 'Promotion not found.' });
    }
    return res.send({ promotion });
  } catch (err) {
    return res.status(500).send({ message: err });
  }
}


export async function createPromotionHandler(req, res) {
  try {
    const { body } = req;
    const isValid = performValidation(body);
    if (!isValid) {
      log.warn('createPromotionHandler called without required fields.');
      return res.status(400).send({
        message: 'You need to specify name, urlKey, brand, and kind',
      });
    }
    const promotion = await createDiscriminatedPromotion(body);
    return res.send(promotion);
  } catch (err) {
    const { name } = err;
    if(name && name === 'ValidationError') return res.status(400).send(err);
    return res.status(500).send(err);
  }
}


export async function updatePromotionHandler(req, res) {
  try {
    const { body, params: { id } } = req;
    const promotion = await updatePromotion(id, body);
    return res.send(promotion);
  } catch (err) {
    const { name } = err;
    if(name && name === 'ValidationError') return res.status(400).send(err);
    return res.status(500).send(err);
  }
}


export async function deletePromotionHandler(req, res) {
  try {
    const { params: { id } } = req;
    await deletePromotion(id);
    return res.status(200).json({
      message: `Promotion deleted : ${id}`,
    });
  } catch (err) {
    return res.status(500).send(err);
  }
}

export async function getPerksHeroHandler(req, res) {
  try {
    const brands = ['sl', 'fs', 'bw'];
    const pendingPromises = brands.map(async (brand) => {
      const key = `cms:perkhero:${brand}`;
      const result = await redisPromise().hgetall(key);
      return result;
    });
    const perksHero = await Promise.all(pendingPromises);
    return res.send(perksHero);
  } catch (err) {
    return res.status(500).send(err);
  }
}

export async function updatePerksHeroHandler(req, res) {
  try {
    const { body: { brand, productName, productImgPath } } = req;
    const key = `cms:perkhero:${brand}`;
    const updateFields = [
      'brand',
      brand,
      'productName',
      productName,
      'productImgPath',
      productImgPath,
    ];
    await redisPromise().hmset(key, updateFields);
    const productoftheMonth = await redisPromise().hgetall(key);
    return res.send(productoftheMonth);
  } catch (err) {
    return res.status(500).send(err);
  }
}

export async function deletePerksHeroHandler(req, res) {
  try {
    const { query: { brand } } = req;
    const key = `cms:perkhero:${brand}`;
    await redisPromise().del(key);
    return res.send({ status: 200 });
  } catch (err) {
    return res.status(500).send(err);
  }
}
