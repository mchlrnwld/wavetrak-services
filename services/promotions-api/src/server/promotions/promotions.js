import addDays from 'date-fns/addDays';
import compareAsc from 'date-fns/compareAsc';
import differenceInSeconds from 'date-fns/differenceInSeconds';
import shuffle from 'lodash.shuffle';
import { getPlatform } from '@surfline/services-common';
import * as analytics from '../../common/analytics';
import { PromotionModel } from '../../models/schema';
import {
  getPromotionByUrlKey,
  updatePromotion,
  getPerksByBrand,
  getPromotionById,
} from '../../models/promotion';
import {
  getPromotionCode,
  verifyPromotionCode,
  updatePromotionCode,
} from '../../models/promotionCode';
import { checkCommonPerkEligibility } from '../../external/subscription';
import { redisPromise } from '../../models/dbContext';

export const getPromotionHandler = async (req, res) => {
  if (!req.params.id) {
    return res.status(400).send({ message: 'You need to specify a Promotion ID.' });
  }

  try {
    const promotion = await PromotionModel.findOne({ _id: req.params.id, isDeleted: false });
    if (!promotion) {
      return res.status(404).send({ message: 'Requested resource not found' });
    }
    return res.send(promotion);
  } catch (err) {
    return res.status(500).send({ message: err });
  }
};

export const getPromotionByUrlKeyHandler = async (req, res) => {
  const { urlKey, brand } = req.query;

  if (!urlKey || !brand) {
    return res.status(400).send({ message: 'Invalid params' });
  }

  try {
    const promotion = await getPromotionByUrlKey(urlKey, brand);
    if (!promotion) {
      return res.status(400).send({ message: 'Requested promotion not found' });
    }
    const response = { _id: promotion._id, kind: promotion.kind, isActive: promotion.isActive };
    return res.send(response);
  } catch (err) {
    return res.status(500).send({ message: err });
  }
};

export const incrementPromotionCount = async (req, res) => {
  const { id: promotionId } = req.params;
  const { userId, brand, promoCode } = req.body;

  if (!promotionId || !userId || !brand) {
    return res.status(400).send({ message: 'Invalid request body and/or params.' });
  }

  try {
    const promotion = await PromotionModel.findOne({
      _id: promotionId,
      isDeleted: false,
      isActive: true,
    });
    let isActive = true;
    const { totalRedemption, maxRedemption, name, kind } = promotion;
    const incrementedTotal = totalRedemption + 1;
    if (incrementedTotal >= maxRedemption) {
      isActive = false;
    }

    if (promoCode || kind === 'Direct To Pay') {
      const code = await getPromotionCode(promotionId, promoCode);
      if (!code && kind !== 'Direct To Pay') {
        return res.status(404).send({ message: 'Requested resource not found' });
      }
      if (code) {
        const { code: promotionCode, _id: codeId } = code;
        await updatePromotionCode(codeId, {
          redeemed: true,
          redeemedAt: Date.now(),
          user: userId,
        });

        if (code && kind === 'Direct To Pay') {
          const trackData = {
            userId,
            event: 'Discount Code Dispatched',
            properties: {
              promotionId,
              promotionName: name,
              promotionCode,
              platform: getPlatform(req.headers['user-agent']),
            },
          };
          analytics.track(brand, trackData);
        }
      }
    }

    const updatedPromotion = await updatePromotion(promotionId, {
      totalRedemption: incrementedTotal,
      isActive,
      kind,
    });

    return res.send(updatedPromotion);
  } catch (err) {
    return res.status(500).send({ message: err });
  }
};

export const verifyPromoCode = async (req, res) => {
  const { id: promotionId } = req.params;
  const { promoCode } = req.body;
  try {
    const code = await verifyPromotionCode(promotionId, promoCode);
    if (!code) {
      return res.status(400).send({
        statusCode: 400,
        code: 1002,
        message: `${promoCode} is not a valid code`,
      });
    }
    return res.send(code);
  } catch (err) {
    return res.status(500).send({ message: err });
  }
};

const checkPerkCodeEligibility = async (perkId, authenticatedUserId) => {
  const { codesPerUser, frequency } = await getPromotionById(perkId);
  const promotionCode = await getPromotionCode(perkId);
  const isCodeAvailable = !!promotionCode;
  const perkKey = `perk:${authenticatedUserId}:${perkId}`;
  const userPerk = await redisPromise().hgetall(perkKey);
  if (userPerk) {
    const { currentCount, code } = userPerk;
    const { frequency: frequencyRedis, dateCreated } = userPerk;
    const dtCreated = new Date(dateCreated);
    const keyExpiry = addDays(dtCreated, frequency);
    const today = new Date();
    let isPerkEligible = currentCount < codesPerUser;
    let lastRequestedCode = code;
    // If frequency is changed in CMS then do additional validations
    if (frequency.toString() !== frequencyRedis.toString()) {
      if (compareAsc(today, keyExpiry) > 0) {
        /*
        If the current date is after key expiry date, then user is eligible.
        Also set the lastRequestedCode to null so that the user can request the code again.
        */
        isPerkEligible = true;
        lastRequestedCode = null;
      }
    }
    return {
      isPerkEligible,
      isCodeAvailable,
      code: lastRequestedCode,
    };
  }
  return {
    isPerkEligible: true,
    isCodeAvailable,
  };
};

export const getPerks = async (req, res) => {
  const { authenticatedUserId } = req;
  const { brand } = req.query;
  const eligibleCount = 3; // TODO: Move this field to perk Mongo document
  let renewalCount = 0;
  let isCommonEligible = false;
  if (!brand) {
    return res.status(400).send({ message: 'Invalid request body and/or params.' });
  }
  if (authenticatedUserId) {
    const {
      commonEligibility,
      renewalCount: renewalCountApi = 0,
    } = await checkCommonPerkEligibility(authenticatedUserId, brand, eligibleCount);
    isCommonEligible = commonEligibility;
    renewalCount = renewalCountApi;
  }
  const perks = await getPerksByBrand(brand);
  const perkPromises = perks.map(async perk => {
    const { isPerkEligible, isCodeAvailable, code } = await checkPerkCodeEligibility(
      perk._id,
      authenticatedUserId
    );
    return {
      ...perk.toObject(),
      eligible: isCommonEligible && isPerkEligible,
      isCodeAvailable,
      lastRequestedCode: code,
      discountText: isCommonEligible ? perk.discountText : null,
      description: isCommonEligible ? perk.description : null,
    };
  });
  const perksResult = await Promise.all(perkPromises);

  // Fetch Perk Hero
  const heroKey = `cms:perkhero:${brand}`;
  const heroConfig = await redisPromise().hgetall(heroKey);
  const shuffledPerks = shuffle(perksResult);
  return res.send({
    eligibleCount,
    renewalCount,
    isCommonEligible,
    heroConfig,
    perks: shuffledPerks,
  });
};

export const generatePerkCode = async (req, res) => {
  const { authenticatedUserId } = req;
  const { id: perkId } = req.params;
  const { brand } = req.query;
  if (!perkId || !authenticatedUserId || !brand) {
    return res.status(400).send({ message: 'Invalid request body and/or params.' });
  }
  try {
    const eligibleCount = 3; // TODO: Move this field to perk Mongo document
    const { commonEligibility } = await checkCommonPerkEligibility(
      authenticatedUserId,
      brand,
      eligibleCount
    );
    if (!commonEligibility) {
      return res.status(400).send({ message: 'User not eligible to request codes' });
    }
    const promotion = await getPromotionById(perkId);
    const { codesPerUser, frequency, kind, totalRedemption } = promotion;
    if (kind !== 'Perk') {
      return res.status(400).send({ message: 'Invalid Promotion Type' });
    }
    const perkCode = await getPromotionCode(perkId);
    if (!perkCode) {
      return res.status(400).send({ message: 'No codes available' });
    }
    const { code } = perkCode;
    const perkKey = `perk:${authenticatedUserId}:${perkId}`;
    const userPerk = await redisPromise().hgetall(perkKey);
    if (userPerk) {
      const { currentCount, frequency: frequencyRedis, dateCreated } = userPerk;
      let currentCountIncremented = parseInt(currentCount, 10) + 1;
      const dtCreated = new Date(dateCreated);
      const keyExpiry = addDays(dtCreated, frequency);
      const today = new Date();
      /*
      If frequency is changed in CMS then do additional validations
      */
      if (frequency.toString() !== frequencyRedis.toString()) {
        if (compareAsc(today, keyExpiry) > 0) {
          /*
          If the current date is after key expiry date,
          then we start again and set currentCount to 1
          */
          currentCountIncremented = 1;
        }
      }
      if (currentCountIncremented > codesPerUser) {
        return res.status(400).send({ message: 'User exceeded code limit' });
      }
      perkCode.redeemed = true;
      perkCode.redeemedAt = Date.now();
      perkCode.user = authenticatedUserId;
      await perkCode.save();
      promotion.totalRedemption = totalRedemption + 1;
      await promotion.save();
      const updateFields = [
        'currentCount',
        currentCountIncremented,
        'dateLastRequested',
        today,
        'codesPerUser',
        codesPerUser,
        'code',
        code,
      ];
      await redisPromise().hmset(perkKey, updateFields);
      // If frequency is changed in CMS then change the expiry of the key accordingly
      if (frequency.toString() !== frequencyRedis.toString()) {
        const ttl = differenceInSeconds(keyExpiry, today);
        await redisPromise().hmset(perkKey, ['keyExpiry', keyExpiry, 'frequency', frequency]);
        await redisPromise().expire(perkKey, ttl);
      }
      const perk = await redisPromise().hgetall(perkKey);
      const trackData = {
        event: 'Issued Perk Code',
        userId: authenticatedUserId,
        properties: {
          perk: perkId,
          platform: getPlatform(req.headers['user-agent']),
        },
      };
      analytics.track(brand, trackData);
      return res.send({ ...perk, code });
    }

    const today = new Date();
    const keyExpiry = addDays(today, frequency);
    const ttl = differenceInSeconds(keyExpiry, today);
    perkCode.redeemed = true;
    perkCode.redeemedAt = Date.now();
    perkCode.user = authenticatedUserId;
    await perkCode.save();
    promotion.totalRedemption = totalRedemption + 1;
    await promotion.save();
    const updateFields = [
      'currentCount',
      1,
      'dateLastRequested',
      today,
      'codesPerUser',
      codesPerUser,
      'frequency',
      frequency,
      'keyExpiry',
      keyExpiry,
      'dateCreated',
      today,
      'code',
      code,
    ];
    await redisPromise().hmset(perkKey, updateFields);
    await redisPromise().expire(perkKey, ttl);
    const perk = await redisPromise().hgetall(perkKey);
    const trackData = {
      event: 'Issued Perk Code',
      userId: authenticatedUserId,
      properties: {
        perk: perkId,
        platform: getPlatform(req.headers['user-agent']),
      },
    };
    analytics.track(brand, trackData);
    return res.send({ ...perk, code });
  } catch (err) {
    return res.send({ err });
  }
};
