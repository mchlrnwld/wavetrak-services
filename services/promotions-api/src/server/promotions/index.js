import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import {
  getPromotionHandler,
  getPromotionByUrlKeyHandler,
  incrementPromotionCount,
  verifyPromoCode,
  getPerks,
  generatePerkCode,
} from './promotions';
import trackRequests from '../../common/trackRequests';

const promotions = (log) => {
  const api = Router();

  api.use('*', trackRequests(log));
  api.get('/perks', wrapErrors(getPerks));
  api.post('/:id/perk-code', wrapErrors(generatePerkCode));
  api.get('/validate', wrapErrors(getPromotionByUrlKeyHandler));
  api.get('/:id', wrapErrors(getPromotionHandler));
  api.post('/:id/increment', wrapErrors(incrementPromotionCount));
  api.post('/:id/verify-promocode', wrapErrors(verifyPromoCode));
  return api;
};

export default promotions;
