import { setupExpress } from '@surfline/services-common';
import trackRequests from '../common/trackRequests';
import promotions from './promotions';
import admin from './admin';
import config from '../config';

const server = log => setupExpress({
  log,
  port: config.EXPRESS_PORT || 8080,
  name: 'promotions-service',
  allowedMethods: ['GET', 'POST', 'PUT', 'DELETE'],
  handlers: [
    ['*', trackRequests(log)],
    ['/', promotions(log)],
    ['/admin', admin(log)],
  ],
});

export default server;
