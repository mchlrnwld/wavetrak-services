import fetch from 'isomorphic-fetch';

const parseRequest = async resp => {
  const contentType = resp.headers.get('Content-Type');
  const isJson = contentType.indexOf('json') > -1;
  const body = isJson ? await resp.json() : await resp.text();
  if (resp.status > 200) {
    throw body;
  }
  return body;
};

export const checkCommonPerkEligibility = (userId, brand, count) =>
  fetch(
    `${process.env.SUBSCRIPTION_SERVICE_URL}/subscriptions/promotions/perk-eligibility?brand=${brand}&count=${count}`,
    {
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
        'x-auth-userid': userId,
      },
    }
  ).then(parseRequest);

export default checkCommonPerkEligibility;
