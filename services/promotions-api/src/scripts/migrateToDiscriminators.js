import { initMongoDB } from '../models/dbContext';
import PromotionModel from '../models/schema';

function getKindFromType(type) {
  const kindMap = {
    extendTrial: 'Extended Free Trial',
    giftRedeem: 'Gift Redeem',
    directToPay: 'Direct To Pay',
    retentionOffer: 'Retention Offer',
  };
  return kindMap[type];
}

async function updateDocumentsToDiscriminatorPattern() {
  try {
    await initMongoDB();
    const docsToDiscriminate = await PromotionModel.find().lean();
    docsToDiscriminate.map(async (doc) => {
      if (doc.partnerDetails) {
        const {
          _id,
          type,
          partnerDetails: {
            logo,
            logoAltText,
            detailCopy,
            signup: { headingText: signUpHeader },
            payment: { headingText: paymentHeader },
            success: { headingText: successHeader, successMessage, successUrl },
          },
        } = doc;
        const docToUpdate = await PromotionModel.findOne({ _id });
        docToUpdate.logo = logo;
        docToUpdate.logoAltText = logoAltText;
        docToUpdate.detailCopy = detailCopy;
        docToUpdate.signUpHeader = signUpHeader;
        docToUpdate.paymentHeader = paymentHeader;
        docToUpdate.successHeader = successHeader;
        docToUpdate.successMessage = successMessage;
        docToUpdate.successUrl = successUrl;
        docToUpdate.kind = getKindFromType(type);
        await docToUpdate.save();
      }
    });
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log(error);
  }
}

updateDocumentsToDiscriminatorPattern();
