/* eslint-disable no-console */
import mongoose from 'mongoose';
import isPast from 'date-fns/isPast';
import { initMongoDB, initCache, redisPromise } from '../models/dbContext';
import PromotionCodeModel from '../models/PromotionCodeModel';

/*
  This cleanup script cleans up the perk keys in redis that were having incorrect TTL set to them.
  Because of this issue users were not able to access the "GET CODE" button even if the frequency time (in days) set in CMS for the perk is over.
  As a fix I am immidietly expiring the perk keys assosciated to a Perk in Redis that are having a keyExpiry in the past.
*/
async function fixPerkKeyExpiry() {
  try {
    await Promise.all([initCache(), initMongoDB()]);
    const promotionId = '5dc46da4628aaa008e105154';
    const condition = {
      "redeemed": true,
      "promotion":{$in:[ mongoose.Types.ObjectId(promotionId)]},
      // "user": "5db776e2077cb6008ed35190"
    }
    const docs = await PromotionCodeModel.find(condition);
    // .limit(100);
    docs.map(async(doc)=>{
      const { user } = doc;
      const perkKey = `perk:${user}:${promotionId}`;
      const keyExpiry = await redisPromise().hget(perkKey, ['keyExpiry']);
      console.log('perkKey', perkKey);
      console.log('keyExpiry', keyExpiry);
      const result = isPast(new Date(keyExpiry));
      console.log('result', result);
      if(keyExpiry && result) {
        // keyExpiry is in the past. Expire now.
        await redisPromise().expire(perkKey, 0);
      }
    })
  } catch (error) {
    console.log('error', error);
  }
}

fixPerkKeyExpiry();

