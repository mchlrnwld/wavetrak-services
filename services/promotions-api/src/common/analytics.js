import Analytics from 'analytics-node';

const bwAnalytics = new Analytics(process.env.BW_SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });
const fsAnalytics = new Analytics(process.env.FS_SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });
const slAnalytics = new Analytics(process.env.SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });

const analytics = {
  bw: bwAnalytics,
  sl: slAnalytics,
  fs: fsAnalytics,
};

export const track = (brand, params, callback = null) => {
  analytics[brand].track(params, callback);
};

export default track;
