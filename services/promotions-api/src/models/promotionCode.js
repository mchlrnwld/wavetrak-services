import { ServerError } from '@surfline/services-common';
import difference from 'lodash.difference';
import PromotionCodeModel from './PromotionCodeModel';
import logger from '../common/logger';

const log = logger('promotion-model');

const buildRedeemCodeString = () => {
  const length = 7;
  const chars = '23456789ABCDEFGHJLMNPQRSTUVWXYZ';
  let result = '';
  for (let i = 0; i < length; i += 1) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
};

export async function getPromotionCodes(filter) {
  try {
    const codes = await PromotionCodeModel.find(filter);
    return codes;
  } catch (error) {
    log.error({
      message: 'Error returned from getPromotionCodes',
      stack: error.stack,
      filter,
    });
    throw new ServerError(error);
  }
}

export async function getPromotionCode(promotion, code = null) {
  try {
    const params = code ? { promotion, code } : { promotion };
    const promotionCode = await PromotionCodeModel.findOne({
      ...params,
      redeemed: false,
    });

    return promotionCode;
  } catch (error) {
    log.error({
      message: 'Error returned from getPromotionCode',
      stack: error.stack,
      params: { promotion, code },
    });
    throw new ServerError(error);
  }
}

export async function verifyPromotionCode(promotion, code) {
  try {
    const promotionCode = await PromotionCodeModel.findOne({
      promotion,
      code,
      redeemed: false,
    });
    return promotionCode;
  } catch (error) {
    log.error({
      message: 'Error returned from verifyPromotionCode',
      stack: error.stack,
      params: { promotion, code },
    });
    throw new ServerError(error);
  }
}

export async function pruneCodesForCreation(promotionId, codes) {
  try {
    const existingPromoCodesObjects = await PromotionCodeModel.find({
      promotion: promotionId,
    });
    const existingPromoCodeValues = existingPromoCodesObjects.map(({ code }) => code);
    const newPromotionCodesToCreate = difference(codes, existingPromoCodeValues);
    return newPromotionCodesToCreate;
  } catch (error) {
    log.error({
      message: 'Error returned from pruneCodesForCreation',
      stack: error.stack,
    });
    throw new ServerError(error);
  }
}

export function generateInternalPromotionCodes(promoCodePrefix, amountToCreate) {
  const promoCodesArray = [];
  let codesGenerated = 0;
  while (codesGenerated < amountToCreate) {
    const code = buildRedeemCodeString();
    const promoCode = `${promoCodePrefix}${code}`;
    promoCodesArray.push(promoCode);
    codesGenerated += 1;
  }
  return promoCodesArray;
}

export async function savePromotionCode(promotion, code) {
  try {
    const newPromotionCode = new PromotionCodeModel({ promotion, code });
    await newPromotionCode.save();
    return newPromotionCode.toObject();
  } catch (error) {
    log.error({
      message: 'Error returned from savePromotionCode',
      stack: error.stack,
    });
    throw new ServerError(error);
  }
}

export async function createInternalPromotionCodes(promotionId, promotionCodePrefix, amount) {
  try {
    const generatedCodes = generateInternalPromotionCodes(promotionCodePrefix, amount);
    const queuedPromisesToResolve = generatedCodes
      .map(code => savePromotionCode(promotionId, code));
    await Promise.all(queuedPromisesToResolve);
    return generatedCodes;
  } catch (error) {
    log.error({
      message: 'Error returned from createInternalPromotionCodes',
      stack: error.stack,
      promotion: promotionId,
    });
    throw new ServerError(error);
  }
}

export async function insertPartnerPromotionCodes(promotionId, codes) {
  try {
    const partnerCodesToInsert = await pruneCodesForCreation(promotionId, codes);
    const queuedPromisesToResolve = partnerCodesToInsert
      .map(code => savePromotionCode(promotionId, code));
    await Promise.all(queuedPromisesToResolve);
    return partnerCodesToInsert;
  } catch (error) {
    log.error({
      message: 'Error returned from insertPartnerPromtionCodes',
      stack: error.stack,
      promotion: promotionId,
    });
    throw new ServerError(error);
  }
}

export async function updatePromotionCode(id, data) {
  try {
    const updatedCode = await PromotionCodeModel.findOneAndUpdate(
      { _id: id },
      { $set: data },
      { new: true, runValidators: true },
    );
    return updatedCode.toObject();
  } catch (error) {
    log.error({
      message: 'Error returned from updatePromotionCode',
      stack: error.stack,
      codeId: id,
    });
    throw new ServerError(error);
  }
}
