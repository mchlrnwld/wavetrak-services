import mongoose from 'mongoose';
import redis from 'redis';
import { promisify } from 'util';
import logger from '../common/logger';
import config from '../config';

const log = logger('promotions-service:dbContext');

let redisClient = {};

// eslint-disable-next-line
export const initMongoDB = () => {
  const connectionString = config.MONGO_CONNECTION_STRING;
  const mongoDbConfig = {
    poolSize: 50,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  };
  return new Promise((resolve, reject) => {
    try {
      log.debug(`Mongo ConnectionString: ${connectionString} `);

      mongoose.connect(connectionString, mongoDbConfig);
      mongoose.connection.once('open', () => {
        log.info(`MongoDB connected on ${connectionString}`);
        resolve();
      });
      mongoose.connection.on('error', (error) => {
        log.error({
          action: 'MongoDB:ConnectionError',
          error,
        });
        reject(error);
      });
    } catch (error) {
      log.error({
        message: 'MongoDB:ConnectionError',
        stack: error,
      });
      reject(error);
    }
  });
};

export function initCache() {
  const connectionString = `redis://${process.env.REDIS_IP}:${process.env.REDIS_PORT}`;
  return new Promise((resolve, reject) => {
    try {
      log.trace(`Redis ConnectionString: ${connectionString} `);
      redisClient = redis.createClient(connectionString);
      redisClient.once('ready',
        () => {
          log.info(`Redis connected on ${connectionString} and ready to accept commands`);
          resolve();
        });
      redisClient.on('error',
        (error) => {
          log.error({
            action: 'Redis:ConnectionError',
            error,
          });
          reject(error);
        });
    } catch (error) {
      log.error({
        action: 'Redis:ConnectionError',
        error,
      });
      reject(error);
    }
  });
}

export function cacheClient() {
  return redisClient;
}

let promisifiedRedis = null;
export const redisPromise = () => {
  if (!promisifiedRedis) {
    promisifiedRedis = {
      hgetall: promisify(cacheClient().hgetall).bind(cacheClient()),
      hmset: promisify(cacheClient().hmset).bind(cacheClient()),
      expire: promisify(cacheClient().expire).bind(cacheClient()),
      del: promisify(cacheClient().del).bind(cacheClient()),
      hget: promisify(cacheClient().hget).bind(cacheClient()),
    };
  }
  return promisifiedRedis;
};
