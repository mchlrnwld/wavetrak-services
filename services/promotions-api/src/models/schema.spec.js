import { expect } from 'chai';
import { PromotionModel, getDiscriminatorModel } from './schema';

describe('Promotion Discriminator schema', () => {
  it('should include the base promotion fields with defaults', () => {
    const {
      name,
      brand,
      isActive,
    } = new PromotionModel({
      name: 'Test',
      urlKey: 'test',
      brand: 'sl',
    });
    expect(name).to.equal('Test');
    expect(brand).to.equal('sl');
    expect(isActive).to.equal(false);
  });

  it('should discriminate for Extended Free Trial promotions', () => {
    const ExtendedFreeTrialModel = getDiscriminatorModel('Extended Free Trial');
    const {
      name,
      brand,
      trialLength,
      promoCodesEnabled,
      logo,
      kind,
      plans,
      maxRedemption,
      currency
    } = new ExtendedFreeTrialModel({
      name: 'Test',
      urlKey: 'test',
      brand: 'bw',
      trialLength: 30,
    });
    expect(kind).to.equal('Extended Free Trial');
    expect(name).to.equal('Test');
    expect(brand).to.equal('bw');
    expect(trialLength).to.equal(30);
    expect(plans).to.equal('annual');
    expect(promoCodesEnabled).to.equal(false);
    expect(logo).to.be.null();
    expect(maxRedemption).to.equal(0);
    expect(currency).to.equal('ANY');
  });

  it('should discriminate for Direct To Pay promotions', () => {
    const DirectToPayModel = getDiscriminatorModel('Direct To Pay');
    const {
      name,
      brand,
      plans,
      logo,
      kind,
      maxRedemption,
      currency
    } = new DirectToPayModel({
      name: 'Direct To Pay',
      urlKey: 'direct-to-pay',
      brand: 'sl',
      plans: 'annual',
      maxRedemption: 10,
      currency: 'USD'
    });
    expect(kind).to.equal('Direct To Pay');
    expect(name).to.equal('Direct To Pay');
    expect(brand).to.equal('sl');
    expect(plans).to.equal('annual');
    expect(logo).to.be.null();
    expect(maxRedemption).to.equal(10);
    expect(currency).to.equal('USD')
  });

  it('should discriminate for Gift Redeem promotions', () => {
    const GiftRedeemModel = getDiscriminatorModel('Gift Redeem');
    const {
      name,
      brand,
      layeredCardImgPath,
      kind,
    } = new GiftRedeemModel({
      name: 'Test',
      urlKey: 'test',
      brand: 'fs',
    });
    expect(kind).to.equal('Gift Redeem');
    expect(name).to.equal('Test');
    expect(brand).to.equal('fs');
    expect(layeredCardImgPath).to.be.null();
  });

  it('should discriminate for Retention Offer promptions', () => {
    const RetentionOffer = getDiscriminatorModel('Retention Offer');
    const {
      name,
      brand,
      stripeCouponCode,
      eligibilityTarget,
      eligibleStatus,
      offeredPlan,
      eligiblePlan,
      kind,
    } = new RetentionOffer({
      name: 'Test',
      urlKey: 'test',
      brand: 'fs',
      stripeCouponCode: 'test',
      eligibilityTarget: 1,
      offeredPlan: 'fs_annual_v2',
      eligiblePlan: 'fs_monthly_v2',
    });
    expect(kind).to.equal('Retention Offer');
    expect(name).to.equal('Test');
    expect(brand).to.equal('fs');
    expect(stripeCouponCode).to.equal('test');
    expect(eligibilityTarget).to.equal(1);
    expect(eligibleStatus).to.equal('active');
    expect(offeredPlan).to.equal('fs_annual_v2');
    expect(eligiblePlan).to.equal('fs_monthly_v2');
  });

  it('should discriminate for Perk promotions', () => {
    const RetentionOffer = getDiscriminatorModel('Perk');
    const {
      name,
      brand,
      codesPerUser,
      frequency,
      isPremium,
      kind,
    } = new RetentionOffer({
      name: 'Perk Promotion',
      urlKey: 'perk_urlKey',
      brand: 'sl',
      codesPerUser: 5,
      frequency: 30,
      isPremium: true,
    });
    expect(kind).to.equal('Perk');
    expect(name).to.equal('Perk Promotion');
    expect(brand).to.equal('sl');
    expect(codesPerUser).to.equal(5);
    expect(frequency).to.equal(30);
    expect(isPremium).to.equal(true);
  });
});
