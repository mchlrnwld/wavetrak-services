import mongoose from 'mongoose';

const options = {
  discriminatorKey: 'kind',
};

const baseSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: [true, 'Name is required'],
      default: '',
      maxlength: [50, 'Name must not have more than 50 characters.'],
    },
    brand: {
      type: String,
      required: [true, 'Brand is required'],
      enum: ['sl', 'bw', 'fs'],
      trim: true,
    },
    isActive: {
      type: Boolean,
      default: false,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
    totalRedemption: {
      type: Number,
      default: 0,
    },
  },
  { ...options, collection: 'Promotions', timestamps: true },
);

const funnelAttributes = {
  urlKey: {
    type: String,
    trim: true,
    required: [true, 'URL Key is required'],
    maxlength: [50, 'Name must not have more than 50 characters.'],
    validate: {
      validator: v => /^[a-zA-Z0-9\-_]{0,50}$/.test(v),
      message: 'Url key must not have special characters or spaces.',
    },
    // unique: true,
  },
  signUpHeader: {
    type: String,
    default: null,
    maxlength: [60, 'Heading Text must not have more than 60 characters.'],
  },
  successHeader: {
    type: String,
    default: null,
    maxlength: [100, 'Heading Text must not have more than 100 characters.'],
  },
  successMessage: {
    type: String,
    default: null,
    maxlength: [
      300,
      'Success Message must not have more than 300 characters.',
    ],
  },
  successUrl: {
    type: String,
    default: null,
    maxlength: [100, 'Success Url must not have more than 100 characters.'],
  },
};

const partnerAttributes = {
  logo: {
    type: String,
    default: null,
    maxlength: [100, 'Logo Image path must not have more than 100 characters.'],
  },
  logoAltText: {
    type: String,
    default: null,
    maxlength: [24, 'Heading Text must not have more than 24 characters.'],
  },
  detailCopy: {
    type: String,
    default: null,
    maxlength: [140, 'Detail Copy must not have more than 140 characters.'],
  },
  bgDesktopImgPath: {
    type: String,
    trim: true,
    default: null,
    maxlength: [
      100,
      'bgDesktopImgPath must not have more than 100 characters.',
    ],
  },
  bgMobileImgPath: {
    type: String,
    trim: true,
    default: null,
    maxlength: [100, 'bgMobileImgPath must not have more than 100 characters.'],
  },
  maxRedemption: {
    type: Number,
    default: 0,
  },
};

const locationAttributes = {
  currency: {
    type: String,
    default: 'ANY',
    enum: ['USD', 'ILS', 'AUD', 'GBP', 'NZD', 'EUR', 'ANY']
  }
}

const extendedFreeTrialSchema = new mongoose.Schema(
  {
    trialLength: {
      type: Number,
      required: [true, 'trial length is required'],
    },
    paymentHeader: {
      type: String,
      default: null,
      maxlength: [60, 'Heading Text must not have more than 60 characters.'],
      // required: [true, 'payment header is required'],
    },
    promoCodesEnabled: {
      type: Boolean,
      default: false,
    },
    promoCodePrefix: {
      type: String,
      trim: true,
      default: null,
      maxlength: [5, 'promoCodePrefix must not have more than 5 characters.'],
    },
    plans: {
      type: String,
      default: 'annual',
      enum: ['all', 'annual', 'monthly'],
      trim: true,
    },
    ...funnelAttributes,
    ...partnerAttributes,
    ...locationAttributes
  },
  options,
);

const directToPaySchema = new mongoose.Schema(
  {
    paymentHeader: {
      type: String,
      default: null,
      maxlength: [60, 'Heading Text must not have more than 60 characters.'],
      // required: [true, 'payment header is required'],
    },
    plans: {
      type: String,
      default: 'all',
      enum: ['all', 'annual', 'monthly'],
      trim: true,
    },
    buttonText: {
      type: String,
      default: null,
      maxlength: [60, 'Button Text must not have more than 60 characters.'],
    },
    ...funnelAttributes,
    ...partnerAttributes,
    ...locationAttributes
  },
  options,
);

const giftRedeemSchema = new mongoose.Schema(
  {
    redeemHeader: {
      type: String,
      default: null,
      maxlength: [100, 'Heading Text must not have more than 100 characters.'],
    },
    layeredCardImgPath: {
      type: String,
      trim: true,
      default: null,
      maxlength: [
        100,
        'layeredCardImgPath must not have more than 100 characters.',
      ],
    },
    bgDesktopImgPath: {
      type: String,
      trim: true,
      default: null,
      maxlength: [
        100,
        'bgDesktopImgPath must not have more than 100 characters.',
      ],
    },
    bgMobileImgPath: {
      type: String,
      trim: true,
      default: null,
      maxlength: [
        100,
        'bgMobileImgPath must not have more than 100 characters.',
      ],
    },
    ...funnelAttributes,
  },
  options,
);

const retentionOfferSchema = new mongoose.Schema(
  {
    paymentHeader: {
      type: String,
      default: null,
      maxlength: [60, 'Heading Text must not have more than 60 characters.'],
    },
    paymentMessage: {
      type: String,
      default: null,
      maxlength: [
        300,
        'Payment Message must not have more than 300 characters.',
      ],
    },
    buttonText: {
      type: String,
      default: null,
      maxlength: [60, 'Button Text must not have more than 60 characters.'],
    },
    stripeCouponCode: {
      type: String,
      required: [true, 'A coupon code is required'],
      trim: true,
    },
    eligibilityType: {
      type: String,
      default: 'renewals',
      enum: ['renewals', 'duration'],
      trim: true,
    },
    eligibilityTarget: {
      type: Number,
      required: [true, 'A target number must be specified'],
    },
    eligiblePlan: {
      type: String,
      required: [true, 'An eligible plan is required.'],
      trim: true,
    },
    eligibleStatus: {
      type: String,
      default: 'active',
      enum: ['active', 'expiring', 'expired'],
      trim: true,
    },
    offeredPlan: {
      type: String,
      required: [true, 'An offered plan is required'],
      trim: true,
    },
    ...funnelAttributes,
  },
  options,
);

const perkSchema = new mongoose.Schema(
  {
    description: {
      type: String,
      default: null,
      maxlength: [300, 'Description must not have more than 300 characters.'],
    },
    freeDescription: {
      type: String,
      default: null,
      maxlength: [300, 'Description must not have more than 300 characters.'],
    },
    bgDesktopImgPath: {
      type: String,
      trim: true,
      default: null,
      maxlength: [
        100,
        'bgDesktopImgPath must not have more than 100 characters.',
      ],
    },
    bgMobileImgPath: {
      type: String,
      trim: true,
      default: null,
      maxlength: [
        100,
        'bgMobileImgPath must not have more than 100 characters.',
      ],
    },
    logoImgPath: {
      type: String,
      trim: true,
      default: null,
      maxlength: [
        100,
        'logoImgPath must not have more than 100 characters.',
      ],
    },
    maxRedemption: {
      type: Number,
      default: 0,
    },
    discountText: {
      type: String,
      default: null,
      maxlength: [100, 'discountText must not have more than 100 characters.'],
    },
    codesPerUser: {
      type: Number,
      required: [true, 'Codes per user is required'],
    },
    frequency: {
      type: Number,
      required: [true, 'Frequency is required'],
    },
    isPremium: {
      type: Boolean,
      default: true,
    },
    partnerUrl: {
      type: String,
      trim: true,
      default: null,
      maxlength: [
        300,
        'partnerUrl must not have more than 300 characters.',
      ],
    },
  },
  options,
);

export const PromotionModel = mongoose.model('Promotion', baseSchema);
const ExtendedFreeTrialModel = PromotionModel.discriminator('Extended Free Trial', extendedFreeTrialSchema);
const DirectToPayModel = PromotionModel.discriminator('Direct To Pay', directToPaySchema);
const GiftRedeemModel = PromotionModel.discriminator('Gift Redeem', giftRedeemSchema);
const RetentionOfferModel = PromotionModel.discriminator('Retention Offer', retentionOfferSchema);
const PerkModel = PromotionModel.discriminator('Perk', perkSchema);

export const getDiscriminatorModel = (kind) => {
  const modelMap = {
    'Extended Free Trial': ExtendedFreeTrialModel,
    'Direct To Pay': DirectToPayModel,
    'Gift Redeem': GiftRedeemModel,
    'Retention Offer': RetentionOfferModel,
    Perk: PerkModel,
  };
  return modelMap[kind];
};
