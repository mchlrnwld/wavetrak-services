import mongoose from 'mongoose';

const PromotionCodeSchema = mongoose.Schema(
  {
    promotion: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Promotions',
    },
    code: {
      type: String,
      required: [true, 'code is required'],
      trim: true,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
    redeemed: {
      type: Boolean,
      default: false,
    },
    redeemedAt: {
      type: Date,
      default: null,
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  { collection: 'PromotionCodes' }
);

PromotionCodeSchema.index({ promotion: 1, redeemed: 1 });
PromotionCodeSchema.index(
  {
    promotion: 1,
    code: 1,
  },
  {
    unique: true,
  }
);

const PromotionCodeModel = mongoose.model('PromotionCodes', PromotionCodeSchema);

export default PromotionCodeModel;
