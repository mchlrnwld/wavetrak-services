import { ServerError } from '@surfline/services-common';
import logger from '../common/logger';
import { PromotionModel, getDiscriminatorModel } from './schema';

const log = logger('promotion-model');

export async function getPromotions() {
  try {
    const promotions = await PromotionModel.find({ isDeleted: false });
    return promotions;
  } catch (error) {
    log.error({
      message: 'Error thrown in getPromotions',
      stack: error,
    });
    throw new ServerError(error);
  }
}

export async function getPromotionById(id) {
  try {
    const promotion = await PromotionModel.findOne({ _id: id });
    return promotion;
  } catch (error) {
    log.error({
      message: 'Error thrown in getPromotionById',
      stack: error,
    });
    throw new ServerError(error);
  }
}

export async function getPromotionByUrlKey(urlKey, brand) {
  try {
    const promotion = await PromotionModel
      .findOne({ urlKey, brand, isDeleted: false });
    return promotion;
  } catch (error) {
    log.error({
      message: 'Error thrown in getPromotionByUrlKey',
      stack: error,
    });
    throw new ServerError(error);
  }
}

export async function createDiscriminatedPromotion(data) {
  try {
    const promotion = await PromotionModel.create(data);
    return promotion;
  } catch (error) {
    log.error({
      message: 'Error thrown in createDiscriminatedPromotion',
      stack: error,
    });
    throw error;
  }
}

export async function updatePromotion(promotionId, data) {
  try {
    const { kind } = data;
    const DiscriminatorModel = getDiscriminatorModel(kind);
    const promotion = await DiscriminatorModel.findOneAndUpdate(
      { _id: promotionId },
      { ...data },
      { new: true, runValidators: true },
    );
    return promotion;
  } catch (error) {
    log.error({
      message: 'Error thrown in updatePromotion',
      stack: error,
    });
    throw error;
  }
}

export async function incrementPromotionMaxRedemption(promotionId, opts) {
  try {
    const { amount } = opts;
    const { kind } = await getPromotionById(promotionId);
    const DiscriminatorModel = getDiscriminatorModel(kind);
    const promotion = DiscriminatorModel.findOneAndUpdate(
      { _id: promotionId },
      { $inc: { maxRedemption: amount } },
      { new: true },
    );
    return promotion;
  } catch (error) {
    log.error({
      message: 'Error thrown in incrementPromotionMaxRedemption',
      stack: error,
    });
    throw new ServerError(error);
  }
}

export async function deletePromotion(id) {
  try {
    await PromotionModel.findOneAndUpdate(
      { _id: id },
      { $set: { isDeleted: true, updatedAt: new Date() } },
      { new: true },
    );
  } catch (error) {
    log.error({
      message: 'Error thrown in deletePromotion',
      stack: error,
    });
    throw new ServerError(error);
  }
}

export async function getPerksByBrand(brand) {
  try {
    const perks = await PromotionModel
      .find({
        kind: 'Perk', brand, isActive: true, isDeleted: false,
      });
    return perks;
  } catch (error) {
    log.error({
      message: 'Error thrown in getPerksByBrand',
      stack: error,
    });
    throw new ServerError(error);
  }
}
