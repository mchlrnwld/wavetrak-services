export default {
  MONGO_CONNECTION_STRING: process.env.MONGO_CONNECTION_STRING,
  EXPRESS_PORT: process.env.EXPRESS_PORT || 8083,
  LOGSENE_KEY: process.env.LOGSENE_KEY || '36621d1a-eee6-4cec-89b6-4b7f734d0d63',
  CONSOLE_LOG_LEVEL: process.env.CONSOLE_LOG_LEVEL || 'debug',
  LOGSENE_LEVEL: process.env.LOGSENE_LEVEL || 'debug',
};
