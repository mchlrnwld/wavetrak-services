# Promotions Service

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Developing](#developing)
- [Promotion API Admin Endpoints](#promotion-api-admin-endpoints)
  - [Get all Promotions](#get-all-promotions)
  - [Get details of a Promotion](#get-details-of-a-promotion)
  - [Delete a Promotion](#delete-a-promotion)
  - [Update a Promotion](#update-a-promotion)
  - [Create a Promotion](#create-a-promotion)
- [Promotion API Public Endpoints](#promotion-api-public-endpoints)
  - [Validate a Promotion based on urlKey and brand](#validate-a-promotion-based-on-urlkey-and-brand)
  - [Fetch Promotion Details based on PromotionID](#fetch-promotion-details-based-on-promotionid)
  - [Update totalRedemption for a Promotion](#update-totalredemption-for-a-promotion)
  - [Verify a promocode is redeemed or not](#verify-a-promocode-is-redeemed-or-not)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Developing

To start developing

* `nvm install v10.7.0`
* `npm install`
* copy `.env.sample` to `.env` and adjust environment variables to your local computer
* run `npm run startlocal` to start the development server
* `npm run test` will run unit tests
* `npm run lint` will run lint tests

## Promotion API Admin Endpoints

All admin endpoints are authenticated and should have a valid `X-Auth-UserId` in the header.

Promotions Schema (UserDB/Promotions collection):

```
{ 
    "_id" : ObjectId("5b6cae80205bd4773f014bf7"), 
    "partnerDetails" : {
        "signup" : {
            "headingText" : "Sign Up Screen for 30 days Free Trial"
        }, 
        "payment" : {
            "headingText" : "Payment Screen for 30 days Free Trial"
        }, 
        "success" : {
            "headingText" : "Thank You Screen for 30 days Free Trial", 
            "successMessage" : "Thank You for subscribing 30 days Free Trial"
        }, 
        "logo" : "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png", 
        "logoAltText" : "Kelly Slater Alt Text", 
        "detailCopy" : "30 days Free Trial promotion by Kelly Slater"
    }, 
    "name" : "Kelly Slater", 
    "status" : true, 
    "trialLength" : NumberInt(30), 
    "isDeleted" : false, 
    "isPremium" : false, 
    "maxRedemption" : NumberInt(100), 
    "totalRedemption" : NumberInt(0), 
    "bgDesktopImgPath" : "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png", 
    "bgMobileImgPath" : "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png", 
    "valuePropStyle" : "minimized", 
    "urlKey" : "kellyslater", 
    "brand" : "sl", 
    "type" : "extendTrial", 
    "createdAt" : ISODate("2018-08-09T21:13:36.560+0000"), 
    "updatedAt" : ISODate("2018-08-09T21:13:36.560+0000"), 
    "__v" : NumberInt(0)
}
```
----

### Get all Promotions
```
GET /admin/promotions/ HTTP/1.1
Host: server.example.com
X-Auth-UserId: <Mongo UserId>
```
Response:
An array of promotion objects.

```
[
    {
        "partnerDetails": {
            "signup": {
                "headingText": "Sign Up Screen for 30 days Free Trial"
            },
            "payment": {
                "headingText": "Payment Screen for 30 days Free Trial"
            },
            "success": {
                "headingText": "Thank You Screen for 30 days Free Trial",
                "successMessage": "Thank You for subscribing 30 days Free Trial"
            },
            "logo": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
            "logoAltText": "Kelly Slater Alt Text",
            "detailCopy": "30 days Free Trial promotion by Kelly Slater"
        },
        "name": "Kelly Slater",
        "status": true,
        "trialLength": 30,
        "isDeleted": false,
        "isPremium": false,
        "maxRedemption": 100,
        "totalRedemption": 0,
        "bgDesktopImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
        "bgMobileImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
        "valuePropStyle": "minimized",
        "_id": "5b6cae80205bd4773f014bf7",
        "urlKey": "kellyslater",
        "brand": "sl",
        "type": "extendTrial",
        "createdAt": "2018-08-09T21:13:36.560Z",
        "updatedAt": "2018-08-09T21:13:36.560Z",
        "__v": 0
    },
    {
        ..Promotion..
    }
]
```

### Get details of a Promotion
```
GET /admin/promotions/5b6cae83205bd4773f014bfd HTTP/1.1
Host: server.example.com
X-Auth-UserId: <Mongo UserId>
```
Response: Promotion object

```
{
    "partnerDetails": {
        "signup": {
            "headingText": "Payment Screen for 30 days Free Trial"
        },
        "payment": {
            "headingText": "Payment Screen for 30 days Free Trial"
        },
        "success": {
            "headingText": "Thank You Screen for 30 days Free Trial",
            "successMessage": "Thank You for subscribing 30 days Free Trial"
        },
        "logo": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
        "logoAltText": "Test Aj 10",
        "detailCopy": "30 days Free Trial promotion by Kelly Slater"
    },
    "name": "Test Ajith 10",
    "status": true,
    "trialLength": 30,
    "isDeleted": true,
    "isPremium": false,
    "maxRedemption": 100,
    "totalRedemption": 0,
    "bgDesktopImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
    "bgMobileImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
    "valuePropStyle": "minimized",
    "_id": "5b6cae83205bd4773f014bfd",
    "urlKey": "kellyslater",
    "brand": "sl",
    "type": "extendTrial",
    "createdAt": "2018-08-09T21:13:39.781Z",
    "updatedAt": "2018-08-09T22:04:52.272Z",
    "__v": 0
}
```

### Delete a Promotion
```
DELETE /admin/promotions/5b6cae83205bd4773f014bfd HTTP/1.1
Host: server.example.com
X-Auth-UserId: <Mongo UserId>
```
Response:
```
{
    "message": "Promotion deleted : 5b6cae83205bd4773f014bfd"
}
```

### Update a Promotion
```
PUT /admin/promotions/5b6cae83205bd4773f014bfd HTTP/1.1
Host: server.example.com
X-Auth-UserId: <Mongo UserId>
Content-Type: application/json
```
```
{
	"name": "Updated Name",
	"partnerDetails": {
		"logoAltText": "Updated Text",
		"signup": {
			"headingText": "Updated Text"
		}
	}
}
```

Response: Promotion object
```
{
    "partnerDetails": {
        "signup": {
            "headingText": "Updated Text"
        },
        "payment": {
            "headingText": "Payment Screen for 30 days Free Trial"
        },
        "success": {
            "headingText": "Thank You Screen for 30 days Free Trial",
            "successMessage": "Thank You for subscribing 30 days Free Trial"
        },
        "logo": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
        "logoAltText": "Updated Text",
        "detailCopy": "30 days Free Trial promotion by Kelly Slater"
    },
    "name": "Updated Name",
    "status": true,
    "trialLength": 30,
    "isDeleted": true,
    "isPremium": false,
    "maxRedemption": 100,
    "totalRedemption": 0,
    "bgDesktopImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
    "bgMobileImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
    "valuePropStyle": "minimized",
    "_id": "5b6cae83205bd4773f014bfd",
    "urlKey": "kellyslater",
    "brand": "sl",
    "type": "extendTrial",
    "createdAt": "2018-08-09T21:13:39.781Z",
    "updatedAt": "2018-08-09T22:04:52.272Z",
    "__v": 0
}
```

### Create a Promotion

The following fields are required to create a Promotion:
```name, urlKey, brand, type, trialLength```
```
POST /admin/promotions HTTP/1.1
Host: server.example.com
X-Auth-UserId: <Mongo UserId>
Content-Type: application/json
```
Sample Payload:
```
{
	"name": "Kelly Slater",
	"urlKey": "kellyslater",
	"trialLength": 30,
	"status": true,
	"brand": "sl",
	"type": "extendTrial",
	"maxRedemption": 100,
	"bgDesktopImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
	"bgMobileImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
	"partnerDetails": {
		"logo": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
		"logoAltText": "Kelly Slater Alt Text",
		"detailCopy": "30 days Free Trial promotion by Kelly Slater",
		"signup": {
			"headingText": "Sign Up Screen for 30 days Free Trial"
		},
		"payment": {
			"headingText": "Payment Screen for 30 days Free Trial"
		},
		"success": {
			"headingText": "Thank You Screen for 30 days Free Trial",
			"successMessage": "Thank You for subscribing 30 days Free Trial"
		}
	}
}
```

Response: Promotion Object

```
{
    "partnerDetails": {
        "signup": {
            "headingText": "Sign Up Screen for 30 days Free Trial"
        },
        "payment": {
            "headingText": "Payment Screen for 30 days Free Trial"
        },
        "success": {
            "headingText": "Thank You Screen for 30 days Free Trial",
            "successMessage": "Thank You for subscribing 30 days Free Trial"
        },
        "logo": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
        "logoAltText": "Kelly Slater Alt Text",
        "detailCopy": "30 days Free Trial promotion by Kelly Slater"
    },
    "name": "Kelly Slater",
    "status": true,
    "trialLength": 30,
    "isDeleted": false,
    "isPremium": false,
    "maxRedemption": 100,
    "totalRedemption": 0,
    "bgDesktopImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
    "bgMobileImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
    "valuePropStyle": "minimized",
    "_id": "5b6cba7ff4a9267eeff27fde",
    "urlKey": "kellyslater",
    "brand": "sl",
    "type": "extendTrial",
    "createdAt": "2018-08-09T22:04:47.287Z",
    "updatedAt": "2018-08-09T22:04:47.287Z",
    "__v": 0
}
```

## Promotion API Public Endpoints

The public endpoints are not authenticated.

### Validate a Promotion based on urlKey and brand
urlKey and brand are required parameters.

```
GET /validate?urlKey=kellyslater&brand=sl HTTP/1.1
Host: server.example.com
```
Response: Promotion object

```
{
    "_id": <Mongo Promotion _id>
}
```

### Fetch Promotion Details based on PromotionID
Promotion_ID is a required parameter.

This endpoint fetches data for promotions that are active and not deleted.

```
GET /promotions/5b7f1e0db577ca06cb7afd37/ HTTP/1.1
Host: server.example.com
```
Response: Promotion Mongo object

```
{
    "partnerDetails": {
        "signup": {
            "headingText": "Sign Up Screen for 30 days Free Trial"
        },
        "payment": {
            "headingText": "Payment Screen for 30 days Free Trial"
        },
        "success": {
            "headingText": "Thank You, Screen for 30 days Free Trial",
            "successMessage": "Thank You for subscribing 30 days Free Trial test 2"
        },
        "logo": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
        "logoAltText": "Kelly Slater Alt Text",
        "detailCopy": "30 days Free Trial promotion by Kelly Slater"
    },
    "name": "Kelly Slater",
    "isActive": true,
    "isDeleted": false,
    "isPremium": true,
    "totalRedemption": 0,
    "bgDesktopImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
    "bgMobileImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
    "valuePropStyle": "minimized",
    "_id": "5b7f1e0db577ca06cb7afd37",
    "urlKey": "kellyslater",
    "trialLength": 30,
    "brand": "sl",
    "type": "extendTrial",
    "maxRedemption": 100,
    "createdAt": "2018-08-23T20:50:21.291Z",
    "updatedAt": "2018-08-28T23:17:09.483Z",
    "__v": 0
}
```
### Update totalRedemption for a Promotion

This endpoint increments the `totalRedemption` and updates the `isActive` fields for a Promotion. 
This endpoint would be called from the `subscription-service` when a new subscription is added.

If the `totalRedemption` is GTE to `maxRedemption` then we will set the `isActive` flag to false. This would make the Promotion inactive.

```
POST /5ba4061b7248bf000e9b8969/increment HTTP/1.1
{
	"userId": "58b7727824aec05aa591fa60",
	"brand": "sl"
}
```
Response: Updated Promotion Mongo object

```
{
    "partnerDetails": {
        "signup": {
            "headingText": "Sign Up Screen for 30 days Free Trial"
        },
        "payment": {
            "headingText": "Payment Screen for 30 days Free Trial"
        },
        "success": {
            "headingText": "Thank You, Screen for 30 days Free Trial",
            "successMessage": "Thank You for subscribing 30 days Free Trial test 2"
        },
        "logo": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
        "logoAltText": "Kelly Slater Alt Text",
        "detailCopy": "30 days Free Trial promotion by Kelly Slater"
    },
    "name": "Kelly Slater",
    "isActive": true,
    "isDeleted": false,
    "isPremium": true,
    "totalRedemption": 0,
    "bgDesktopImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
    "bgMobileImgPath": "https://wa.cdn-surfline.com/quiver/0.11.0/clienticons/beachlive-icon.png",
    "valuePropStyle": "minimized",
    "_id": "5b7f1e0db577ca06cb7afd37",
    "urlKey": "kellyslater",
    "trialLength": 30,
    "brand": "sl",
    "type": "extendTrial",
    "maxRedemption": 100,
    "createdAt": "2018-08-23T20:50:21.291Z",
    "updatedAt": "2018-08-28T23:17:09.483Z",
    "__v": 0
}
```

### Verify a promocode is redeemed or not

This endpoint verifies if the promoCode exists for the corresponding promotionId and is not redeemed yet. The endpoint would return the code if it is not yet redeemed else would return a 400.

```
POST /5ba4061b7248bf000e9b8969/verify-promocode HTTP/1.1
{
	"promoCode":"ABC-106"
}
```
Response - 200
```
{
    "redeemed": false,
    "redeemedAt": null,
    "_id": "5c3f861e99eabd000f0a0c2c",
    "promotion": "5c3d235e1dd5cfd56c54f647",
    "code": "ABC-106",
    "createdAt": "2019-01-16T19:29:34.756Z",
    "__v": 0
}
```
Response - 400
```
{
    "message": "DISC-341417158 is not a valid code",
    "code": 1002
}
```

## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `promotions-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd4TWpVeE9UTXdOVFkifQ==&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eE1qVXhPVE13TlRZIiwic2VsZWN0ZWROZXJkbGV0Ijp7Im5lcmRsZXRJZCI6ImFwbS1uZXJkbGV0cy5vdmVydmlldyJ9fQ==&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the endpoints detailed in the [Promotion API Admin Endpoints](#promotion-api-admin-endpoints) and/or [Promotion API Public Endpoints](#promotion-api-public-endpoints) sections. 

**Ex:**

Test `GET` requests to the `/admin/promotions` endpoint:

```
curl \
    -X GET \
    -i \
    http://promotions-service.sandbox.surfline.com/admin/promotions
```
**Expected response:** See [above](https://github.com/Surfline/wavetrak-services/tree/master/services/promotions-service#get-all-promotions) for sample response.