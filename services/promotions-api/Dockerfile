FROM node:12 as builder
WORKDIR /opt/app

ARG NPMRC=.npmrc

COPY $NPMRC .
COPY package.json .
RUN npm install

COPY . .

RUN npm run lint && npm run test && npm run dist && npm install --production

FROM node:12-alpine as app
WORKDIR /opt/app

ARG APP_ENV=sandbox
ARG APP_VERSION=master
ENV NODE_ENV=$APP_ENV
ENV APP_VERSION=$APP_VERSION

COPY --from=builder /opt/app/package.json .
COPY --from=builder /opt/app/node_modules node_modules
COPY --from=builder /opt/app/dist dist
COPY --from=builder /opt/app/newrelic.js .

CMD npm start
