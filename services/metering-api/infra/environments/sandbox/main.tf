provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "metering-service/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company           = "wt"
  application       = "metering-service"
  environment       = "sandbox"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac"
  dns_name          = "${local.application}.${local.environment}.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "metering_service" {
  source = "../../"

  company     = local.company
  environment = local.environment
  application = local.application

  default_vpc = "vpc-981887fd"
  dns_name    = local.dns_name
  dns_zone_id = "Z3DM6R3JR1RYXV"
  ecs_cluster = "sl-core-svc-${local.environment}"

  service_lb_rules = local.service_lb_rules
  service_td_count = 1

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-sandbox/c3d74f733d972eac/b3d92f844160d459"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = local.load_balancer_arn

  auto_scaling_enabled = false
}
