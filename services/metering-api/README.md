# Metering API

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Description](#description)
  - [Development](#development)
    - [Testing](#testing)
- [Endpoints](#endpoints)
  - [GET /meter - Endpoint to retrieve a user's meter document](#get-meter---endpoint-to-retrieve-a-users-meter-document)
    - [Description](#description-1)
    - [Request Headers](#request-headers)
    - [Sample Request](#sample-request)
    - [Sample Response](#sample-response)
  - [POST /meter - Endpoint to create a user's meter document](#post-meter---endpoint-to-create-a-users-meter-document)
    - [Description](#description-2)
    - [Request Headers](#request-headers-1)
    - [Request Body](#request-body)
    - [Sample Request](#sample-request-1)
    - [Sample Response](#sample-response-1)
    - [Errors](#errors)
      - [Duplicate Anonymous ID Error](#duplicate-anonymous-id-error)
  - [PATCH / - Endpoint to make whitelisted updates to a meter document.](#patch----endpoint-to-make-whitelisted-updates-to-a-meter-document)
    - [Description](#description-3)
    - [Request Headers](#request-headers-2)
    - [Request Body](#request-body-1)
    - [Sample Request](#sample-request-2)
    - [Sample Response](#sample-response-2)
  - [PATCH /surf-check - Endpoint to consume a surf check or extend a surf check for a given user.](#patch-surf-check---endpoint-to-consume-a-surf-check-or-extend-a-surf-check-for-a-given-user)
    - [Description](#description-4)
    - [Request Headers](#request-headers-3)
    - [Sample Request](#sample-request-3)
    - [Sample Responses](#sample-responses)
  - [GET /surf-check - Endpoint to check if the user has an active surf check or not.](#get-surf-check---endpoint-to-check-if-the-user-has-an-active-surf-check-or-not)
    - [Description](#description-5)
    - [Response](#response)
  - [POST /anonymous - Endpoint to create a surf check in Redis for an anonymous session](#post-anonymous---endpoint-to-create-a-surf-check-in-redis-for-an-anonymous-session)
    - [Description](#description-6)
    - [Request Body](#request-body-2)
    - [Response](#response-1)
  - [GET /anonymous/:anonymousId - Endpoint to check if a surf check exists for a anonymous session in Redis](#get-anonymousanonymousid---endpoint-to-check-if-a-surf-check-exists-for-a-anonymous-session-in-redis)
    - [Description](#description-7)
    - [Request Headers](#request-headers-4)
    - [Response](#response-2)
  - [PATCH /anonymous/:anonymousId - Endpoint to decrement the surf check (meterRemaining) value for an anonymous session in Redis](#patch-anonymousanonymousid---endpoint-to-decrement-the-surf-check-meterremaining-value-for-an-anonymous-session-in-redis)
    - [Description](#description-8)
    - [Request Headers](#request-headers-5)
    - [Response](#response-3)
    - [Development Errors/Issues](#development-errorsissues)
      - [`@types/mongoose` Type Check Errors](#typesmongoose-type-check-errors)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Description

The metering api is a service which will be used to meter non-premium users and limit the amount of surf checks they are able to perform with a given time frame

### Development

```bash
cp .env.sample .env
# fill out the missing env variables

npm install
npm run startlocal
```

#### Testing

This api requires coverage to stay above 95 percent on all metrics. Functions that have code that cannot be stubbed or tested should be ignored using the `/* istanbul ignore next */` or `/* istanbul ignore file */` comments.

Tests can be run using either of the following commands:

```sh
npm run test
```

```sh
npm run coverage
```

## Endpoints

### GET /meter - Endpoint to retrieve a user's meter document

#### Description

This endpoint is used to retrieve metering information for a user based off the userId (access token through the services proxy) and anonymousId passed to the endpoint.

Applications should not be calling this endpoint unless the `sl_metered` entitlement is included in the response from the Entitlements API, at which point the application should recognize
the user as a metered user and request information on their metering.

#### Request Headers

| Header          | Proxy | Service | Notes                                                                                                                                                                |
| --------------- | ----- | ------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `x-auth-userid` | 🚫    | ✅      | This header will be automatically added through the proxy. For information on the proxy see its [docs](https://github.com/Surfline/wavetrak-services#authentication) |

#### Sample Request

Through the services proxy:

```
curl --location --request GET 'https://services.prod.surfline.com/meter?accesstoken=ACCESS_TOKEN'
```

Hitting the api directly:

```
curl --location --request GET 'http://metering-service.prod.surfline.com/' \
  --header 'x-geo-country-iso: <ISO_COUNTRY_CODE>' \
  --header 'x-auth-userId: <USER_ID>'
```

#### Sample Response

```json
{
  "_id": "5ea1f1a9c783518d6acd2824",
  "entitlement": "sl_metered",
  "active": true,
  "anonymousId": "70d83d27-ffc2-4af2-80b9-2c0646fde58b",
  "user": "5aac31f7ffd66500117e5b62",
  "countryCode": "US",
  "treatmentName": "sl_rate_limiting_v3",
  "treatmentVariant": "on",
  "meterLimit": 3,
  "treatmentState": "on",
  "meterRemaining": 2,
  "createdAt": "2020-04-23T19:51:05.503Z",
  "updatedAt": "2020-04-23T19:57:03.942Z",
  "__v": 0
}
```

### POST /meter - Endpoint to create a user's meter document

#### Description

This endpoint is used to create a metering document for a user using both their `userId` and `anonymousId`.
The `meterLimit` for a user would be controlled by Split. See the treatment [sl_rate_limiting_user_v1](https://app.split.io/org/67a2bcf0-5000-11e7-ab38-12a980fb7f4a/ws/3e83a030-1f27-11e9-9a2e-069aee18f4aa/splits/f2687420-b405-11eb-b11d-0e264c9979ad/env/67f28aa0-5000-11e7-ab38-12a980fb7f4a/definition) for details on how it is configured in Split.

#### Request Headers

| Header              | Proxy | Service | Notes                                                                                                                                                                |
| ------------------- | ----- | ------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `x-auth-userid`     | 🚫    | ✅      | This header will be automatically added through the proxy. For information on the proxy see its [docs](https://github.com/Surfline/wavetrak-services#authentication) |
| `x-geo-country-iso` | 🚫    | ✅      | This header will automatically be added through the proxy based on the geo-location of the request. It is only required for direct requests to the service.          |

#### Request Body

| Property    | Required | Notes                                                                                                                                                                                                                                                                                                |
| ----------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| anonymousId | ✅       | Anonymous ID used to create the document. Whatever `anonymousId` is used to create the document should be persisted and should not change. The Anonymous ID must be unique, if not a specific error will be thrown. See [duplicate anonymous ID error](#duplicate-anonymous-id-error) section below. |
| countryCode | ✅       | The country code of the user that the meter is being created for.                                                                                                                                                                                                                                    |
| timezone    | 🚫       | The timezone that the user is in. Defaults to UTC if not passed. Timezone must match the format of [TZ Database (TZ Database name column)](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)                                                                                             |

#### Sample Request

Through the services proxy:

```
curl \
  --location --request POST 'https://services.prod.surfline.com/meter?accesstoken=ACCESS_TOKEN' \
  --header 'Content-Type: application/json' \
  --data-raw '{"anonymousId": "70d83d27-ffc2-4af2-80b9-2c0646fde58c", "timezone": "America/Los_Angeles"}'
```

Hitting the api directly:

```
curl \
  --location --request POST 'http://metering-service.prod.surfline.com/' \
  --header 'x-geo-country-iso: <ISO_COUNTRY_CODE>' \
  --header 'Content-Type: application/json' \
  --header 'x-auth-userId: <USER_ID>' \
  --data-raw '{"anonymousId": "70d83d27-ffc2-4af2-80b9-2c0646fde58c", "timezone": "Etc/GMT-9"}'
```

#### Sample Response

```json
{
  "id": "5ea33cf01cf52009c80f5569",
  "anonymousId": "70d83d27-ffc2-4af2-80b9-2c0646fde58c",
  "countryCode": "US",
  "treatmentState": "on",
  "treatmentVariant": "on",
  "entitlement": "sl_metered",
  "meterLimit": 3,
  "meterRemaining": 3,
  "message": "Meter successfully created."
}
```

#### Errors

##### Duplicate Anonymous ID Error

In the case that an anonymous ID has already been assigned for a meter document, an error with a unique
code will be returned. This error is a generic 409 status code error, but the body contains a `code`
property with the value `1019` denoting a conflicting anonymous ID.
The consuming app should re-generate a new anonymous ID and assign it for that user and then retry
the request with the new anonymous ID.

```json
{
  "message": "Invalid Parameters: Anonymous ID already exists on another meter document",
  "errorHandler": "APIError",
  "name": "APIError",
  "code": 1019
}
```

##### Premium User Error

If the user is Premium we won't proceed with the meter creation. Instead it will throw a 400 error with code 1021. Below is the response:

```json
{
  "message": "Invalid Parameters: Not supported for Premium users",
  "errorHandler": "APIError",
  "name": "APIError",
  "code": 1021
}
```

##### Duplicate User ID Error

In the case that a user ID has already been assigned for a meter document, an error with a unique
code will be returned. This error is a generic 409 status code error, but the body contains a `code`
property with the value `1020` denoting a conflicting user ID.
This signals an issue with application originating the request, since we should not be calling this
endpoint if a user already has a meter document. This could happen for a variety of reasons, one of
which could be:

- The entitlements request failed or was not made (users with a meter document will have an `sl_metered`
  entitlement)

If an application sees this error code, we should stop making this request and fetch the user's meter
document via the `GET` endpoint. It would also be helpful to flag this transaction to try to triangulate
what led to this erroneous request being made.

```json
{
  "message": "Invalid Parameters: User ID already exists on another meter document",
  "errorHandler": "APIError",
  "name": "APIError",
  "code": 1020
}
```

##### User not in Split treatment Error

If the user is not in the Split treatment then the endpoint returns a `400` with the below response. Note that we won't create any meter document in Mongo since the user is out of Rate limiting.

```json
{
  "message": "Invalid Parameters: User not in Split treatment",
  "errorHandler": "APIError",
  "name": "APIError",
  "code": 1022
}
```

##### Split not configured correctly Error

If the Split returns `meterLimit` as `undefined` or `null` for a valid treatment we should not proceed with Meter creation and throw a server error (status code `500`). This could happen when someone updates Split with incorrect or no config for a valid treatment.

```json
{
  "message": "Invalid Parameters: Split configured incorrectly. Not proceeding with Meter creation",
  "errorHandler": "APIError",
  "name": "APIError",
  "code": 1023
}
```

### PATCH / - Endpoint to make whitelisted updates to a meter document.

#### Description

This endpoint can be used to make specific whitelisted updates to a meter document. The following updates are currently
supported:

- Update the meter document after a user has validated their email.

#### Request Headers

| Header          | Proxy | Service | Notes                                                                                                                                                                |
| --------------- | ----- | ------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `x-auth-userid` | 🚫    | ✅      | This header will be automatically added through the proxy. For information on the proxy see its [docs](https://github.com/Surfline/wavetrak-services#authentication) |

#### Request Body

| Property   | Required | Type   | Notes                                                                        |
| ---------- | -------- | ------ | ---------------------------------------------------------------------------- |
| `timezone` | 🚫       | string | Updates the user's timezone to correct value if the current timezone is UTC. |

#### Sample Request

Through the services proxy:

```
curl \
  --location --request PATCH 'https://services.prod.surfline.com/meter?accesstoken=ACCESS_TOKEN' \
  --header 'Content-Type: application/json' \
  --data-raw '{
      "timezone": "America/Los_Angeles"
  }'
```

Hitting the api directly:

```
curl \
  --location --request PATCH 'http://metering-service.prod.surfline.com/' \
  --header 'x-auth-userId: <USER_ID>'
  --header 'Content-Type: application/json' \
  --data-raw '{
      "timezone": "America/Los_Angeles"
  }'
```

#### Sample Response

```json
{
  "message": "Meter successfully updated.",
  "success": true
}
```

If the timezone stored in UserInfo is already a non UTC value then we won't update it again. Instead the below validation error is sent:

```json
{
  "message": "Invalid Parameters: User timezone is in non UTC",
  "errorHandler": "APIError",
  "name": "APIError"
}
```

### PATCH /surf-check - Endpoint to consume a surf check or extend a surf check for a given user.

#### Description

This endpoint is used to register a surf check for a user, or to extend an active surf check for a user. An active surf check denotes a surf check that has registered a whitelisted activity in the past 30 minutes.

An example of an active surf check would be that I load the mobile app and check the "HB Pier Southside" forecast page. This action would start a new surf check. If I close the app and don't return for 30 minutes,
my surf check now becomes _inactive_. However, if I close the app for 10 minutes and the re-open it and check the same spot (or a different one) then the surf check has a new activity within the 30 minute window and
will now be extended 30 more minutes.

Every time the user loads a page, or performs a valid action that should extend their surf check the application should make a call to this endpoint as follows:

```
curl \
  --location --request PATCH 'https://services.prod.surfline.com/meter/surf-check?accesstoken=ACCESS_TOKEN'
```

The endpoint will then do one of the following:

- If a currently active surf check exists in redis, the endpoint will extend it and return a response like so:
  ```json
  {
    "message": "Successfully extended surf check",
    "meterRemaining": 4,
    "extended": true
  }
  ```
- If no currently active surf check exists in redis, the endpoint will consume one surf check from the `meterRemaining` on the mongo document, and then create a surf
  check in Redis with a 30 min expiration on the key.

Each user can have only _ONE_ active surf check at a time. This means that surf checks are persisted across devices. A user can start a surf check on the web, and continue it from their mobile app so long as they
have not exceeded a thirty minute inactivity window. The endpoint makes [atomic](https://docs.mongodb.com/manual/reference/operator/update/inc/#behavior) updates to both Redis and Mongo, therefore it is safe
to call this endpoint multiple times in quick succesion.

#### Request Headers

| Header          | Proxy | Service | Notes                                                                                                                                                                |
| --------------- | ----- | ------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `x-auth-userid` | 🚫    | ✅      | This header will be automatically added through the proxy. For information on the proxy see its [docs](https://github.com/Surfline/wavetrak-services#authentication) |

#### Sample Request

Through the services proxy:

```
curl \
  --location --request PATCH 'https://services.prod.surfline.com/meter/surf-check?accesstoken=ACCESS_TOKEN'
```

Hitting the api directly:

```
curl \
  --location --request PATCH 'http://metering-service.prod.surfline.com/surf-check' \
  --header 'x-auth-userId: <USER_ID>'
```

#### Sample Responses

```json
{
  "message": "Successfully started surf check",
  "meterRemaining": 4
}
```

```json
{
  "message": "Successfully extended surf check",
  "meterRemaining": 4,
  "extended": true
}
```

### GET /surf-check - Endpoint to check if the user has an active surf check or not.

#### Description

This endpoint will check if a user has an ongoing surf check or not. If the user has an active surf check then he is eligible for Premium data.
Make sure to send `X-Auth-UserId` in the header.

#### Response

```json
{
  "hasActiveSurfCheck": true
}
```

### PUT /anonymous/increment/:anonymousId - Endpoint to create a spot check in Redis for an anonymous session

#### Description

If eligible, this endpoint gets called on every page load/client side route change for an anonymous user which increments the spot-check key in Redis by 1. When the anonymous user have used up all the spots checks the final value stored in Redis would be `ANONYMOUS_METER_LIMIT + 1`. Expiry of the key is set to 1 week. Currently the `ANONYMOUS_METER_LIMIT` value is set to 3.

Key: `meter:anonymous:spot-check:<anonymousId>`

#### Request Headers

We don't send any header for this endpoint. The anonymousId is specified in the url.

#### Response

```json
{
  "meterRemaining": 2
}
```

### GET /anonymous/:anonymousId - Endpoint to check if a spot check exists for a anonymous session in Redis

#### Description

This endpoint is used to retrieve the value assosciated to a anonymous spot check from Redis.
The return value denotes the number of spot checks remaining (Similar to meterRemaining).
Key: `meter:anonymous:spot-check:<anonymousId>`

#### Request Headers

We don't send any header for this endpoint. The anonymousId is specified in the url.

#### Response

If surf check exists in Redis for the anonymous session then send a 200 OK response:

```json
{
  "meterRemaining": 1
}
```

If it doesn't exists then send back meterRemaining as null with a status of 404.

```json
{
  "meterRemaining": null
}
```

If there are no spot checks remaining we send meterRemaining as -1.

```json
{
  "meterRemaining": -1
}
```

### GET /anonymous/verify/:anonymousId - Endpoint to verify if the anonymous user is eligible for premium data or not

#### Description

This endpoint would be called from the services-proxy to verify if the anonymous user is eligible for premium data or not based on the number of spot checks left.

#### Request Headers

We don't send any header for this endpoint. The anonymousId is specified in the url.

#### Response

If eligible for premium data we send the below response:

```json
{
  "premiumEligible": true
}
```

#### Development Errors/Issues

Below is a list of potential errors you could run into related to this api (Typescript, NPM, Node, ESLint, Prettier, etc...).
Ideally we could maintain this list as a team and add to it when we run into obscure issues.

##### `@types/mongoose` Type Check Errors

I noticed that when linking `@surfline/services-common` to this service there were typing and compilation errors
related to `@types/mongoose`. This is a red flag as external types shouldn't break compilation.

The ultimate cause was that linking to the services common library resulted in two different versions of `@types/mongoose`
existing and conflicting.

Quick fix

```sh
npm link ../path/to/services-common/node_modules/@types/mongoose
```

## API Validation

To validate that the api is functioning as expected (for example, after a deployment), start with the following:

1. Check the `metering-api` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbncwTWpBd056TXpNREkifQ==&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53ME1qQXdOek16TURJIiwic2VsZWN0ZWROZXJkbGV0Ijp7Im5lcmRsZXRJZCI6ImFwbS1uZXJkbGV0cy5vdmVydmlldyJ9fQ==&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the endpoints detailed in the [Endpoints](#endpoints) section.

**Ex:**

Test `GET` requests against the `/anonymous` endpoint:

```
curl \
    -X GET \
    -i \
    http://metering-service.sandbox.surfline.com/anonymous/456
```

**Expected response:** See [above](#response-2) for sample response.
