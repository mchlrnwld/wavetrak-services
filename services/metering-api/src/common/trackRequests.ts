/* istanbul ignore file */

import { Request, Response, NextFunction } from 'express';
import log from './logger';

const trackRequests = (req: Request, _res: Response, next: NextFunction): void => {
  log.trace({
    action: req.path,
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export default trackRequests;
