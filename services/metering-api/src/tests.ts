/* eslint-disable import/no-extraneous-dependencies */
import fs from 'fs';
import path from 'path';
import Mocha from 'mocha';
import chai from 'chai';
import chaiHttp from 'chai-http';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import sinon from 'sinon';
import * as servicesCommon from '@surfline/services-common';
import type { SurflineLogger } from '@surfline/services-common';
import { setupPactServers, teardownPactServers } from './pact/providers';

chai.use(chaiHttp);
chai.use(dirtyChai);
chai.use(sinonChai);

sinon.stub(servicesCommon, 'setupLogsene');
sinon.stub(servicesCommon, 'createLogger').returns({
  trace: (() => null) as SurflineLogger['trace'],
  debug: (() => null) as SurflineLogger['debug'],
  info: (() => null) as SurflineLogger['info'],
  error: (() => null) as SurflineLogger['error'],
  warn: (() => null) as SurflineLogger['warn'],
  setMaxListeners: (() => null) as SurflineLogger['setMaxListeners'],
} as SurflineLogger);

const findTestFiles = (dir: string): string[] => {
  const contents = fs.readdirSync(dir);

  const filePaths: string[] = [];

  contents.forEach((element) => {
    const elementPath = path.join(dir, element);
    const stats = fs.statSync(elementPath);

    if (stats.isDirectory()) {
      Array.prototype.push.apply(filePaths, findTestFiles(elementPath));
    } else if (element.substr(-8) === '.spec.ts') {
      filePaths.push(elementPath);
    }
  });

  return filePaths;
};

const runMocha = (dir: string): Promise<unknown> =>
  new Promise((resolve, reject) => {
    try {
      const mocha = new Mocha();
      const files = findTestFiles(dir);
      files.forEach((file) => {
        mocha.addFile(file);
      });
      mocha.run(resolve);
    } catch (err) {
      reject(err);
    }
  });

const tests = async (): Promise<void> => {
  const providers = await setupPactServers();
  const failures = await runMocha(__dirname);
  if (failures) process.exit(failures ? 1 : 0);
  await teardownPactServers(providers);
};

tests()
  .then(() => process.exit(0))
  .catch((err) => {
    // eslint-disable-next-line no-console
    console.error(err);
    process.exit(1);
  });
