import { Express } from 'express';
import { Server } from 'http';
import { setupExpress } from '@surfline/services-common';
import log from '../common/logger';
import trackRequests from '../common/trackRequests';
import config from '../config';
import meter from './meter';

const server = (): { app: Express; server: Server } =>
  setupExpress({
    log,
    port: config.EXPRESS_PORT as number,
    name: 'metering-service',
    allowedMethods: ['GET', 'OPTIONS', 'POST', 'PUT', 'PATCH'],
    handlers: [
      ['*', trackRequests],
      ['/', meter],
    ],
  });

export default server;
