import type { Response } from 'express';
import mongoose from 'mongoose';
import {
  SurflineRequest,
  NotFound,
  trackEvent,
  identify,
  APIError,
  SL_PREMIUM,
  getPlatform,
  IOS,
} from '@surfline/services-common';
import Meter, {
  getMeterByUser,
  createMeter,
  decrementMeter,
  checkIfUserExists,
} from '../../models/Meter';
import { getUser, setUserTimezone } from '../../external/user';
import { getUserEntitlements } from '../../external/entitlement';
import {
  METER_LIMIT,
  INITIAL_METER_REMAINING,
  ON,
  OFF,
  ANONYMOUS_METER_LIMIT,
  getRateLimitingTreatmentWithConfig,
  checkIfABTestSupported,
} from '../../utils/meter';
import {
  getSurfCheckMeterTTL,
  resetSurfCheckMeterTTL,
  createSurfCheckMeterKey,
  getSpotCheckAnonymousMeterKey,
  incrementSpotCheckAnonymousMeterKey,
} from '../../external/redis';
import log from '../../common/logger';

/**
 * @description Handler for a retrieving a users metering document
 */
export const getMeterHandler = async (req: SurflineRequest, res: Response): Promise<void> => {
  const { authenticatedUserId: userId } = req;

  const meter = await getMeterByUser(userId);

  if (!meter) {
    throw new NotFound(`Metering document not found for user: ${userId}`);
  }

  const userInfo = await getUser(userId);
  const timezone = userInfo?.timezone;

  res.status(200).send({ ...meter, timezone, treatmentVariant: ON, treatmentState: ON });
};

/**
 * @description Handler used to create a new Metering Document
 */
export const postMeterHandler = async (req: SurflineRequest, res: Response): Promise<Response> => {
  const { authenticatedUserId: userId } = req;
  const { anonymousId, countryCode } = req.body;
  const timezone = req.body.timezone || 'UTC';

  const userAgent = req.get('user-agent');
  const unsupportedVersionRegex = new RegExp(/Surfline\/([0-6]){1}/);
  const isUnsupportedIOSVersion = unsupportedVersionRegex.test(userAgent);

  const platform = getPlatform(userAgent);
  /* istanbul ignore next */
  if (isUnsupportedIOSVersion && platform === IOS) {
    return res.send({
      id: mongoose.Types.ObjectId(),
      anonymousId,
      countryCode,
      treatmentState: 'off',
      treatmentVariant: 'off',
      entitlement: 'sl_metered',
      meterLimit: 0,
      meterRemaining: 0,
      message: 'Meter not created. Version unsupported.',
    });
  }

  if (!anonymousId) {
    throw new APIError('anonymousId is required in the body.');
  }

  const { entitlements } = await getUserEntitlements(userId);
  const isPremium = entitlements.includes(SL_PREMIUM);
  // Rate limiting is not supported for Surfline Premium users.
  if (isPremium) throw new APIError('Not supported for Premium users', 400, 1021);

  // Check if the User ID already exists, and send back a specific error code if it does
  const userExists = await checkIfUserExists(userId);

  if (userExists) {
    const logData = {
      userId,
      anonymousId,
      action: 'Duplicate User ID. Cannot create Meter',
      referrer: req.get('Referrer'),
    };
    log.error(`RateLimit User Lifecycle ${logData}`);
    throw new APIError('User ID already exists on another meter document', 409, 1020);
  }
  // Verify if the user is in A/B Test
  let initialMeterLimit = METER_LIMIT;
  let initialMeterRemaining = INITIAL_METER_REMAINING;
  const appVersion = req.get('x-app-version');
  const isABTestSupported = checkIfABTestSupported(platform, appVersion);
  if (isABTestSupported) {
    const identity = {
      id: userId,
      anonymousId,
      userId,
      isoCountryCode: countryCode,
    };
    const result = getRateLimitingTreatmentWithConfig(userId, identity);
    if (result?.treatmentState === OFF) {
      throw new APIError('User not in Split treatment', 400, 1022);
    }
    if (!result?.meterLimit) {
      throw new APIError(
        'Split configured incorrectly. Not proceeding with Meter creation',
        500,
        1023,
      );
    }
    initialMeterLimit = result?.meterLimit;
    initialMeterRemaining = result?.meterLimit;
  }

  await setUserTimezone(userId, timezone);

  const meter = await createMeter({
    ...req.body,
    user: userId,
    countryCode,
    meterLimit: initialMeterLimit,
    meterRemaining: initialMeterRemaining,
  });

  const { _id, entitlement, meterRemaining, meterLimit } = meter;

  const logData = {
    userId,
    action: 'Meter created',
    meter: JSON.stringify(meter),
    referrer: req.get('Referrer'),
  };

  log.info(logData, `RateLimit User Lifecycle`);

  return res.send({
    id: _id,
    anonymousId,
    countryCode,
    treatmentState: ON,
    treatmentVariant: ON,
    entitlement,
    meterLimit,
    meterRemaining,
    message: 'Meter successfully created.',
  });
};

/**
 * @description
 * PATCH endpoint that can be used to update various values
 * on a metering document. Currently this endpoint supports
 * the following:
 *  - Updates the user's timezone to correct value if the current timezone is UTC. Called from the App.
 */
export const patchMeterHandler = async (req: SurflineRequest, res: Response): Promise<Response> => {
  const { authenticatedUserId: userId } = req;
  const { timezone } = req.body;
  if (!timezone) {
    throw new APIError('timezone is required in the body.');
  }
  const userInfo = await getUser(userId);
  const timezoneStored = userInfo?.timezone;
  if (timezoneStored !== 'UTC') throw new APIError('User timezone is in non UTC');
  setUserTimezone(userId, timezone);
  return res.send({ success: true, message: 'Meter successfully updated.' });
};

/**
 * @desription When a user performs a surf check and their inactivity we want to
 * decrement the amount of remaining surf checks they have. This handler makes a
 * call to Mongo using the `$inc` operator to atomically decrement the documents
 * `meterRemaining` value
 */
export const surfCheckHandler = async (req: SurflineRequest, res: Response): Promise<Response> => {
  const { authenticatedUserId: userId } = req;

  const meter = await getMeterByUser(userId);

  if (!meter) {
    throw new NotFound(`No meter found for user: ${userId}`);
  }

  const { anonymousId, meterRemaining } = meter;

  const surfCheckMeterTTL = await getSurfCheckMeterTTL(userId);
  const hasActiveSurfCheck = surfCheckMeterTTL > 0;

  // If user has a currently ongoing surf check and they have performed a surf
  // check action, then we will extend their surf-check another 30 minutes
  if (hasActiveSurfCheck) {
    await resetSurfCheckMeterTTL(userId);
    const logData = {
      userId,
      action: 'Surf Check Extended',
      meter: JSON.stringify(meter),
      referrer: req.get('Referrer'),
    };
    log.info(logData, `RateLimit User Lifecycle`);
    return res.send({
      message: 'Successfully extended surf check',
      meterRemaining,
      extended: true,
    });
  }

  // If the user does not have an active surf-check, then they are consuming
  // a new one, so we should create a 30 minute surf check in redis, decrement
  // the meter, and return the new number of remaining checks available
  await createSurfCheckMeterKey(userId);
  const updatedMeter = await decrementMeter(userId);

  if (updatedMeter?.meterRemaining > -1) {
    Meter.updateOne({ user: userId }, { $push: { metersConsumed: Date.now() } }).exec();
  }

  if (updatedMeter?.meterRemaining < 0) {
    identify('sl', userId, { zeroChecksLeft: true }, null, null);
  }
  const platform = getPlatform(req.headers['user-agent']);
  trackEvent(
    'sl',
    'Used Surf Check',
    { userId, anonymousId },
    { checksAvailable: updatedMeter.meterRemaining, platform },
  );

  const logData = {
    userId,
    action: 'Surf Check Started',
    meter: JSON.stringify(updatedMeter),
    referrer: req.get('Referrer'),
  };

  log.info(logData, `RateLimit User Lifecycle`);
  return res.send({
    message: 'Successfully started surf check',
    meterRemaining: updatedMeter.meterRemaining,
  });
};

/**
 * @desription This endpoint checks if the user has an active surf check or not.
 * User with active surf check is eligible for Premium data.
 */
export const hasActiveSurfCheck = async (
  req: SurflineRequest,
  res: Response,
): Promise<Response> => {
  const { authenticatedUserId: userId } = req;
  const surfCheckMeterTTL = await getSurfCheckMeterTTL(userId);
  const activeSurfCheckExists = surfCheckMeterTTL > 0;
  return res.send({
    hasActiveSurfCheck: activeSurfCheckExists,
  });
};

/**
 * @description Handler for a retrieving the spot checks for anonymous session
 */
export const getSpotCheckAnonymousMeterHandler = async (
  req: SurflineRequest,
  res: Response,
): Promise<Response> => {
  const { anonymousId } = req.params;
  if (!anonymousId) {
    throw new APIError('anonymousId is required in the url.');
  }
  const meterLimit = Number(ANONYMOUS_METER_LIMIT) + 1;
  const checksUsed = await getSpotCheckAnonymousMeterKey(anonymousId);
  const logData = {
    anonymousId,
    checksUsed,
  };
  if (!checksUsed) {
    log.info(logData, 'Anonymous Spot Check Key does not exist');
    return res.status(404).send({
      meterRemaining: null,
    });
  }
  if (Number(checksUsed) >= meterLimit) {
    log.info(logData, 'Anonymous Spot Checks used up');
    return res.send({
      meterRemaining: -1,
    });
  }
  const meterRemaining = meterLimit - Number(checksUsed);
  log.info({ ...logData, meterRemaining }, 'Get Anonymous Spot Check');
  return res.send({
    meterRemaining,
  });
};

/**
 * @description Handler to increment a new spot check for anonymous session in Redis
 */
export const incrementSpotCheckAnonymousMeterHandler = async (
  req: SurflineRequest,
  res: Response,
): Promise<Response> => {
  const { anonymousId } = req.params;
  if (!anonymousId) {
    throw new APIError('anonymousId is required in the url.');
  }
  const checksUsed = await getSpotCheckAnonymousMeterKey(anonymousId);
  const meterLimit = Number(ANONYMOUS_METER_LIMIT) + 1;
  if (Number(checksUsed) >= meterLimit) {
    const logData = {
      anonymousId,
      checksUsed,
    };
    log.info(logData, 'No Remaining Anonymous Spot Checks');
    return res.send({
      message: 'There are no remaining surf checks for this anonymous session',
      meterRemaining: -1,
    });
  }
  const checksUsedUpdated = await incrementSpotCheckAnonymousMeterKey(anonymousId);
  const meterRemaining = meterLimit - Number(checksUsedUpdated);
  const logData = {
    anonymousId,
    checksUsed,
    checksUsedUpdated,
    meterRemaining,
  };
  log.info(logData, 'Anonymous Spot Check Incremented');
  return res.send({
    meterRemaining,
  });
};

/**
 * @description Handler to verify if an anonymous session have spot checks left and is eligible for premium data
 */
export const verifySpotCheckAnonymousMeterHandler = async (
  req: SurflineRequest,
  res: Response,
): Promise<Response> => {
  const { anonymousId } = req.params;
  if (!anonymousId) {
    throw new APIError('anonymousId is required in the url.');
  }
  const checksUsed = await getSpotCheckAnonymousMeterKey(anonymousId);
  const meterLimit = Number(ANONYMOUS_METER_LIMIT) + 1;
  if (!checksUsed || Number(checksUsed) > meterLimit) {
    return res.send({
      premiumEligible: false,
    });
  }
  return res.send({
    premiumEligible: true,
  });
};
