import chai from 'chai';
import mongoose from 'mongoose';
import express from 'express';
import 'chai-http';
import sinon from 'sinon';
import { ImportMock } from 'ts-mock-imports';
import * as servicesCommon from '@surfline/services-common';

import meter from '.';
import Meter, * as meterModel from '../../models/Meter';
import meterFixture from '../../fixtures/meter';
import mockExpressRequests from '../../../mockExpressRequests.spec';
import * as userExternal from '../../external/user';
import * as entitlementExternal from '../../external/entitlement';
import * as redisExternal from '../../external/redis';

const { expect } = chai;

describe('/', () => {
  const userId = mongoose.Types.ObjectId().toString();
  const meterDoc = meterFixture(userId);

  let trackEventStub: sinon.SinonStub;
  let getTreatmentStub: sinon.SinonStub;
  let request: ChaiHttp.Agent;

  let currentTimeStub: sinon.SinonStub;
  const currentTimestamp = Date.now();

  before(() => {
    trackEventStub = sinon.stub(servicesCommon, 'trackEvent');
    getTreatmentStub = sinon.stub(servicesCommon, 'getTreatment');
    const setupMeter = (app: express.Express): void => {
      const consoleStub: Partial<Console> = {
        log: () => null,
        info: () => null,
        error: () => null,
      };
      app.use(servicesCommon.geoCountryIso);
      app.use('/', meter);
      app.use(servicesCommon.errorHandlerMiddleware(consoleStub as Console));
    };
    request = mockExpressRequests(setupMeter);
  });

  beforeEach(() => {
    currentTimeStub = sinon.stub(Date, 'now').returns(currentTimestamp);
  });

  after(() => {
    trackEventStub.restore();
    getTreatmentStub.restore();
    request.close();
  });

  afterEach(() => {
    trackEventStub.resetHistory();
    currentTimeStub.restore();
  });

  describe('GET /', () => {
    let getMeterByUserStub: sinon.SinonStub;
    let getUserInfoStub: sinon.SinonStub;

    beforeEach(() => {
      getMeterByUserStub = ImportMock.mockFunction(meterModel, 'getMeterByUser');
      getUserInfoStub = ImportMock.mockFunction(userExternal, 'getUser');
    });

    afterEach(() => {
      getMeterByUserStub.restore();
      getUserInfoStub.restore();
    });

    it('should return a meter document if it is exists', async () => {
      getMeterByUserStub.resolves(meterDoc);
      getTreatmentStub.returns('on');
      getUserInfoStub.resolves({ timezone: 'America/Los_Angeles' });

      const response = await request.get('/').set('x-auth-userid', userId);
      expect(response).to.exist();
      expect(response).to.have.status(200);
      expect(response.error).to.be.false();
      expect(response.body).to.deep.equal({
        ...meterDoc,
        timezone: 'America/Los_Angeles',
        treatmentVariant: 'on',
        treatmentState: 'on',
      });
      expect(getMeterByUserStub).to.have.been.calledOnceWithExactly(userId);
    });

    it('should return a 404 if a meter does not exist for the user', async () => {
      getMeterByUserStub.resolves(null);

      const response = await request.get('/').set('x-auth-userid', userId);

      expect(response).to.exist();
      expect(response).to.have.status(404);
      expect(response.error).to.exist();
      expect(response.body).to.deep.equal({
        message: `Metering document not found for user: ${userId}`,
      });
    });

    it('should handle an unexpected error', async () => {
      getMeterByUserStub.throws();

      const response = await request.get('/').set('x-auth-userid', userId);

      expect(response).to.exist();
      expect(response).to.have.status(500);
      expect(response.error).to.exist();
      expect(response.body).to.deep.equal({
        message: 'Error encountered',
      });
    });
  });

  describe('POST /', () => {
    let createMeterStub: sinon.SinonStub;
    let checkIfUserExistsStub: sinon.SinonStub;
    let setUserTimezoneStub: sinon.SinonStub;
    let getUserEntitlementsStub: sinon.SinonStub;
    let getTreatmentWithConfigStub: sinon.SinonStub;
    const timezone = 'America/Los_Angeles';

    beforeEach(() => {
      createMeterStub = ImportMock.mockFunction(meterModel, 'createMeter');
      setUserTimezoneStub = ImportMock.mockFunction(userExternal, 'setUserTimezone');
      checkIfUserExistsStub = ImportMock.mockFunction(meterModel, 'checkIfUserExists');
      getUserEntitlementsStub = ImportMock.mockFunction(entitlementExternal, 'getUserEntitlements');
      getTreatmentWithConfigStub = sinon.stub(servicesCommon, 'getTreatmentWithConfig');
    });

    afterEach(() => {
      createMeterStub.restore();
      setUserTimezoneStub.restore();
      checkIfUserExistsStub.restore();
      getUserEntitlementsStub.restore();
      getTreatmentWithConfigStub.restore();
    });

    it('should create a meter document for users', async () => {
      const customMeterDoc = meterFixture(userId, 5, 5);
      createMeterStub.resolves(customMeterDoc);
      setUserTimezoneStub.resolves();
      getUserEntitlementsStub.resolves({ entitlements: [] });

      const anonymousId = '123';
      const body = { anonymousId, timezone, countryCode: 'US' };
      getTreatmentWithConfigStub.returns({
        treatment: 'surf_check_weekly_2',
        config: '{"meterLimit":5}',
      });
      const response = await request.post('/').set('x-auth-userid', userId).send(body);

      expect(response).to.exist();
      expect(response).to.have.status(200);
      expect(response.error).to.be.false();
      expect(response.body).to.deep.equal({
        id: customMeterDoc._id,
        anonymousId,
        entitlement: 'sl_metered',
        meterLimit: 5,
        countryCode: 'US',
        meterRemaining: 5,
        treatmentState: 'on',
        treatmentVariant: 'on',
        message: 'Meter successfully created.',
      });
      expect(setUserTimezoneStub).to.have.been.calledOnceWithExactly(userId, timezone);
      expect(createMeterStub).to.have.been.calledOnceWithExactly({
        ...body,
        user: userId,
        countryCode: 'US',
        meterLimit: 5,
        meterRemaining: 5,
      });
    });

    it('should default the timezone to UTC if it is not sent', async () => {
      const customMeterDoc = meterFixture(userId, 3, 3);
      createMeterStub.resolves(customMeterDoc);
      setUserTimezoneStub.resolves();
      getUserEntitlementsStub.resolves({ entitlements: [] });

      const anonymousId = '123';
      const body = { anonymousId, countryCode: 'US' };
      getTreatmentWithConfigStub.returns({
        treatment: 'surf_check_weekly_2',
        config: '{"meterLimit":3}',
      });
      const response = await request
        .post('/')
        .set('x-auth-userid', userId)
        .set('x-geo-country-iso', 'US')
        .send(body);

      expect(response).to.exist();
      expect(response).to.have.status(200);
      expect(response.error).to.be.false();
      expect(response.body).to.deep.equal({
        id: customMeterDoc._id,
        anonymousId,
        entitlement: 'sl_metered',
        meterLimit: 3,
        countryCode: 'US',
        meterRemaining: 3,
        treatmentState: 'on',
        treatmentVariant: 'on',
        message: 'Meter successfully created.',
      });
      expect(setUserTimezoneStub).to.have.been.calledOnceWithExactly(userId, 'UTC');
      expect(createMeterStub).to.have.been.calledOnceWithExactly({
        ...body,
        user: userId,
        countryCode: 'US',
        meterLimit: 3,
        meterRemaining: 3,
      });
    });

    it('should handle an unexpected error', async () => {
      createMeterStub.throws();
      setUserTimezoneStub.resolves();

      const body = { anonymousId: '123', countryCode: 'US' };
      getTreatmentWithConfigStub.returns({
        treatment: 'surf_check_weekly_2',
        config: '{"meterLimit":3}',
      });
      const response = await request.post('/').set('x-auth-userid', userId).send(body);

      expect(response).to.exist();
      expect(response).to.have.status(500);
      expect(response.error).to.exist();
      expect(response.body).to.deep.equal({
        message: 'Error encountered',
      });
    });

    it('should not create a meter if setting the users timezone fails', async () => {
      const customMeterDoc = meterFixture(userId, 3, 3);
      createMeterStub.resolves(customMeterDoc);
      getTreatmentStub.returns('variant');
      setUserTimezoneStub.throws();
      getUserEntitlementsStub.resolves({ entitlements: [] });

      const anonymousId = '123';
      const body = { anonymousId };
      getTreatmentWithConfigStub.returns({
        treatment: 'surf_check_weekly_2',
        config: '{"meterLimit":3}',
      });
      const response = await request
        .post('/')
        .set('x-auth-userid', userId)
        .set('x-geo-country-iso', 'US')
        .send(body);

      expect(response).to.exist();
      expect(response).to.have.status(500);
      expect(response.error).to.exist();
      expect(response.body).to.deep.equal({
        message: 'Error encountered',
      });
      expect(setUserTimezoneStub).to.have.been.calledOnceWithExactly(userId, 'UTC');
      expect(createMeterStub).to.have.not.been.called();
    });
  });

  describe('PATCH /surf-check', () => {
    let decrementMeterStub: sinon.SinonStub;
    let getMeterByUserStub: sinon.SinonStub;
    let getSurfCheckMeterTTLStub: sinon.SinonStub;
    let resetSurfCheckMeterTTLStub: sinon.SinonStub;
    let createSurfCheckMeterKeyStub: sinon.SinonStub;
    let updateOneStub: sinon.SinonStub;

    beforeEach(() => {
      getMeterByUserStub = sinon.stub(meterModel, 'getMeterByUser');
      decrementMeterStub = ImportMock.mockFunction(meterModel, 'decrementMeter');
      getSurfCheckMeterTTLStub = ImportMock.mockFunction(redisExternal, 'getSurfCheckMeterTTL');
      resetSurfCheckMeterTTLStub = ImportMock.mockFunction(redisExternal, 'resetSurfCheckMeterTTL');
      createSurfCheckMeterKeyStub = ImportMock.mockFunction(
        redisExternal,
        'createSurfCheckMeterKey',
      );
      updateOneStub = sinon.stub(Meter, 'updateOne');
    });

    afterEach(() => {
      decrementMeterStub.restore();
      getMeterByUserStub.restore();
      getSurfCheckMeterTTLStub.restore();
      resetSurfCheckMeterTTLStub.restore();
      createSurfCheckMeterKeyStub.restore();
      updateOneStub.restore();
    });

    it('should register a new surf check and decrement the meter document', async () => {
      decrementMeterStub.resolves({ ...meterDoc, meterRemaining: 0 });
      getMeterByUserStub.resolves(meterDoc);
      getSurfCheckMeterTTLStub.resolves(-2);
      createSurfCheckMeterKeyStub.resolves(1);
      updateOneStub.returns({ exec: () => meterDoc });

      const response = await request.patch('/surf-check').set('x-auth-userid', userId);

      expect(response).to.exist();
      expect(response).to.have.status(200);
      expect(response.error).to.be.false();
      expect(response.body).to.deep.equal({
        meterRemaining: 0,
        message: 'Successfully started surf check',
      });
      expect(getSurfCheckMeterTTLStub).to.have.been.calledOnceWithExactly(userId);
      expect(createSurfCheckMeterKeyStub).to.have.been.calledOnceWithExactly(userId);
      expect(decrementMeterStub).to.have.been.calledOnceWithExactly(userId);
      expect(updateOneStub).to.have.been.calledOnceWithExactly(
        { user: userId },
        { $push: { metersConsumed: Date.now() } },
      );
    });

    it('should extend an existing surf check', async () => {
      getMeterByUserStub.resolves(meterDoc);
      getSurfCheckMeterTTLStub.resolves(100);
      resetSurfCheckMeterTTLStub.resolves(1);

      const response = await request.patch('/surf-check').set('x-auth-userid', userId);

      expect(response).to.exist();
      expect(response).to.have.status(200);
      expect(response.error).to.be.false();
      expect(response.body).to.deep.equal({
        meterRemaining: 3,
        extended: true,
        message: 'Successfully extended surf check',
      });
      expect(getSurfCheckMeterTTLStub).to.have.been.calledOnceWithExactly(userId);
      expect(resetSurfCheckMeterTTLStub).to.have.been.calledOnceWithExactly(userId);
      expect(createSurfCheckMeterKeyStub).to.not.have.been.called();
      expect(decrementMeterStub).to.not.have.been.called();
      expect(trackEventStub).to.not.have.been.called();
    });

    it('should send a 404 if no matching meter document is found', async () => {
      getMeterByUserStub.resolves(null);

      const response = await request.patch('/surf-check').set('x-auth-userid', userId);

      expect(response).to.exist();
      expect(response).to.have.status(404);
      expect(response.error).to.exist();
      expect(response.body).to.deep.equal({
        message: `No meter found for user: ${userId}`,
      });
      expect(getMeterByUserStub).to.have.been.calledOnceWithExactly(userId);
    });

    it('should handle an unexpected error', async () => {
      getMeterByUserStub.throws();

      const response = await request.patch('/surf-check').set('x-auth-userid', userId);

      expect(response).to.exist();
      expect(response).to.have.status(500);
      expect(response.error).to.exist();
      expect(response.body).to.deep.equal({
        message: 'Error encountered',
      });
    });
  });

  describe('GET /anonymous/:anonymousId', () => {
    let getSpotCheckAnonymousMeterKeyStub: sinon.SinonStub;
    beforeEach(() => {
      getSpotCheckAnonymousMeterKeyStub = ImportMock.mockFunction(
        redisExternal,
        'getSpotCheckAnonymousMeterKey',
      );
    });
    afterEach(() => {
      getSpotCheckAnonymousMeterKeyStub.restore();
    });

    it('should get the surf check value for an anonymousId', async () => {
      getSpotCheckAnonymousMeterKeyStub.resolves('2');
      const response = await request.get('/anonymous/test_anonymousId');
      expect(response).to.exist();
      expect(response).to.have.status(200);
      expect(response.error).to.be.false();
      expect(response.body).to.deep.equal({
        meterRemaining: 2,
      });
      expect(getSpotCheckAnonymousMeterKeyStub).to.have.been.calledOnceWithExactly(
        'test_anonymousId',
      );
    });

    it('should return the surf check value as -1 for an anonymousId with no checks left', async () => {
      getSpotCheckAnonymousMeterKeyStub.resolves('4');
      const response = await request.get('/anonymous/test_anonymousId');
      expect(response).to.exist();
      expect(response).to.have.status(200);
      expect(response.error).to.be.false();
      expect(response.body).to.deep.equal({
        meterRemaining: -1,
      });
      expect(getSpotCheckAnonymousMeterKeyStub).to.have.been.calledOnceWithExactly(
        'test_anonymousId',
      );
    });

    it('should return the surf check value as null if anonymousId key does not exist', async () => {
      getSpotCheckAnonymousMeterKeyStub.resolves(null);
      const response = await request.get('/anonymous/test_anonymousId');
      expect(response).to.exist();
      expect(response).to.have.status(404);
      expect(response.error).to.exist();
      expect(response.body).to.deep.equal({
        meterRemaining: null,
      });
      expect(getSpotCheckAnonymousMeterKeyStub).to.have.been.calledOnceWithExactly(
        'test_anonymousId',
      );
    });
  });

  describe('GET /surf-check', () => {
    let getSurfCheckMeterTTLStub: sinon.SinonStub;
    beforeEach(() => {
      getSurfCheckMeterTTLStub = ImportMock.mockFunction(redisExternal, 'getSurfCheckMeterTTL');
    });
    afterEach(() => {
      getSurfCheckMeterTTLStub.restore();
    });

    it('should return true if there is an active surf check', async () => {
      getSurfCheckMeterTTLStub.resolves(100);
      const response = await request.get('/surf-check').set('x-auth-userid', '1234');
      expect(response).to.exist();
      expect(response).to.have.status(200);
      expect(response.error).to.be.false();
      expect(response.body).to.deep.equal({
        hasActiveSurfCheck: true,
      });
    });
    it('should return false if there is no active surf check', async () => {
      getSurfCheckMeterTTLStub.resolves(-1);
      const response = await request.get('/surf-check').set('x-auth-userid', '1234');
      expect(response).to.exist();
      expect(response).to.have.status(200);
      expect(response.error).to.be.false();
      expect(response.body).to.deep.equal({
        hasActiveSurfCheck: false,
      });
    });
  });
});
