import chai from 'chai';
import mongoose from 'mongoose';
import express from 'express';
import 'chai-http';
import sinon from 'sinon';
import * as servicesCommon from '@surfline/services-common';

import meterFixture from '../../../fixtures/meter';
import admin from '.';
import mockExpressRequests from '../../../../mockExpressRequests.spec';
import Meter from '../../../models/Meter';
import * as userExternal from '../../../external/user';
import * as meterUtils from '../../../utils/meter';

const { expect } = chai;

describe('/admin', () => {
  const userId = mongoose.Types.ObjectId().toString();
  let aggregateStub: sinon.SinonStub;
  let findOneStub: sinon.SinonStub;
  let updateOneStub: sinon.SinonStub;
  let trackEventStub: sinon.SinonStub;
  let identifyStub: sinon.SinonStub;
  let request: ChaiHttp.Agent;
  let currentTimeStub: sinon.SinonStub;
  let getTreatmentWithConfigStub: sinon.SinonStub;
  let getUserStub: sinon.SinonStub;
  let getRateLimitingTreatmentWithConfigSpy: sinon.SinonSpy;
  const currentTimestamp = Date.now();

  const allowDiskUseStub = sinon.stub();

  before(() => {
    trackEventStub = sinon.stub(servicesCommon, 'trackEvent');
    identifyStub = sinon.stub(servicesCommon, 'identify');
    const setupMeter = (app: express.Express): void => {
      const consoleStub: Partial<Console> = {
        log: () => null,
        info: () => null,
        error: () => null,
      };
      app.use(servicesCommon.geoCountryIso);
      app.use('/admin', admin);
      app.use(servicesCommon.errorHandlerMiddleware(consoleStub as Console));
    };
    request = mockExpressRequests(setupMeter);
  });

  beforeEach(() => {
    aggregateStub = sinon.stub(Meter, 'aggregate');
    findOneStub = sinon.stub(Meter, 'findOne');
    updateOneStub = sinon.stub(Meter, 'updateOne');
    currentTimeStub = sinon.stub(Date, 'now').returns(currentTimestamp);
    getTreatmentWithConfigStub = sinon.stub(servicesCommon, 'getTreatmentWithConfig');
    getUserStub = sinon.stub(userExternal, 'getUser');
    getRateLimitingTreatmentWithConfigSpy = sinon.spy(
      meterUtils,
      'getRateLimitingTreatmentWithConfig',
    );
  });

  after(() => {
    trackEventStub.restore();
    identifyStub.restore();
    request.close();
  });

  afterEach(() => {
    trackEventStub.resetHistory();
    identifyStub.resetHistory();
    aggregateStub.restore();
    allowDiskUseStub.resetHistory();
    findOneStub.restore();
    updateOneStub.restore();
    currentTimeStub.restore();
    getTreatmentWithConfigStub.restore();
    getUserStub.restore();
    getRateLimitingTreatmentWithConfigSpy.restore();
  });

  describe('DELETE /admin/meters', () => {
    it('should delete a meter for a user', async () => {
      const meter = {
        ...meterFixture(),
        delete: () => null,
      };
      const meterDeleteStub = sinon.stub(meter, 'delete');
      findOneStub.resolves(meter);

      const result = await request.delete('/admin/meters').send({
        user: meter.user,
      });
      expect(result.status).to.equal(200);
      expect(result.body).to.deep.equal({
        message: `Successfully deleted meter ${meter._id} for user: ${meter.user}`,
      });
      expect(findOneStub).to.have.been.called.calledOnceWithExactly({ user: meter.user });
      expect(meterDeleteStub).to.have.been.called.calledOnce();
    });

    it('should return 404 if no meter document is found', async () => {
      const meter = {
        ...meterFixture(),
        delete: () => null,
      };
      const meterDeleteStub = sinon.stub(meter, 'delete');

      const result = await request.delete('/admin/meters').send({
        user: meter.user,
      });
      expect(result.status).to.equal(404);
      expect(result.body).to.deep.equal({ message: `No meter found for user ${meter.user}` });
      expect(findOneStub).to.have.been.called.calledOnceWithExactly({ user: meter.user });
      expect(meterDeleteStub).to.not.have.been.called();
    });

    it('should return 400 if no user is passed', async () => {
      const result = await request.delete('/admin/meters');
      expect(result.status).to.equal(400);
      expect(result.body).to.deep.equal({ message: 'Missing user parameter' });
      expect(findOneStub).to.not.have.been.called();
    });

    it('should return 500 if an error occurs', async () => {
      findOneStub.throwsException('error');

      const result = await request.delete('/admin/meters').send({
        user: userId,
      });
      expect(result.status).to.equal(500);
      expect(result.body).to.deep.equal({
        message: 'Error deleting meter',
        error: { name: 'error' },
      });
    });
  });

  describe('GET /admin/timezones', () => {
    it('should return the unique timezones for metered documents', async () => {
      allowDiskUseStub.resolves([
        { timezones: ['America/Los_Angeles', 'America/New_York', 'Australia/Brisbane'] },
      ]);
      aggregateStub.returns({ allowDiskUse: allowDiskUseStub });

      const result = await request.get('/admin/timezones');
      expect(result.body).to.deep.equal({
        timezones: ['America/Los_Angeles', 'America/New_York', 'Australia/Brisbane'],
      });
      expect(allowDiskUseStub).to.have.been.called.calledOnceWithExactly(true);
    });

    it('should return an empty array if no timezones are found', async () => {
      allowDiskUseStub.resolves([]);
      aggregateStub.returns({ allowDiskUse: allowDiskUseStub });

      const result = await request.get('/admin/timezones');
      expect(result.body).to.deep.equal({
        timezones: [],
      });
      expect(allowDiskUseStub).to.have.been.called.calledOnceWithExactly(true);
    });
  });

  describe('GET /admin/meters', () => {
    it('should return the meters matching the query', async () => {
      const meterFixture1 = meterFixture();
      const meterFixture2 = meterFixture();

      const meter1 = {
        ...meterFixture1,
        user: { timezone: 'America/New_York', isEmailVerified: true, _id: meterFixture1.user },
      };
      const meter2 = {
        ...meterFixture2,
        user: { timezone: 'America/New_York', isEmailVerified: true, _id: meterFixture2.user },
      };

      allowDiskUseStub.resolves([meter1, meter2]);
      aggregateStub.returns({ allowDiskUse: allowDiskUseStub });

      const result = await request.get('/admin/meters').send({
        meter: {
          active: true,
        },
        user: {
          isEmailVerified: true,
          timezone: 'America/New_York',
        },
      });

      const aggregateStubCall = aggregateStub.getCall(0).args[0];
      const lookupStage = aggregateStubCall.find((stage) => !!stage.$lookup);
      const matchStage = aggregateStubCall.find((stage) => !!stage.$match);
      const projectionStage = aggregateStubCall.find((stage) => !!stage.$project);

      expect(result.body).to.deep.equal({
        meters: [meter1, meter2],
      });
      expect(matchStage.$match).to.deep.equal({ active: true });
      expect(lookupStage.$lookup.pipeline[0].$match).to.deep.equal({
        $expr: {
          $eq: ['$_id', '$$userId'],
        },
        isEmailVerified: true,
        timezone: 'America/New_York',
      });
      expect(projectionStage).to.not.exist();
      expect(allowDiskUseStub).to.have.been.called.calledOnceWithExactly(true);
    });

    it('should add a projection to the aggregation', async () => {
      const meterFixture1 = meterFixture();
      const meterFixture2 = meterFixture();

      const meter1 = {
        ...meterFixture1,
        user: { timezone: 'America/New_York', isEmailVerified: true, _id: meterFixture1.user },
      };
      const meter2 = {
        ...meterFixture2,
        user: { timezone: 'America/New_York', isEmailVerified: true, _id: meterFixture2.user },
      };

      allowDiskUseStub.resolves([meter1, meter2]);
      aggregateStub.returns({ allowDiskUse: allowDiskUseStub });

      const result = await request.get('/admin/meters').send({
        meter: {
          active: true,
        },
        user: {
          isEmailVerified: true,
          timezone: 'America/New_York',
        },
        projection: {
          'user.isEmailVerified': 1,
          active: true,
          'user.timezone': 1,
        },
      });

      const aggregateStubCall = aggregateStub.getCall(0).args[0];

      expect(result.body).to.deep.equal({
        meters: [meter1, meter2],
      });
      expect(aggregateStubCall.find((stage) => !!stage.$project)).to.deep.equal({
        $project: {
          'user.isEmailVerified': 1,
          active: true,
          'user.timezone': 1,
        },
      });
      expect(allowDiskUseStub).to.have.been.called.calledOnceWithExactly(true);
    });

    it('should add limit and skip on the aggregation', async () => {
      const meterFixture1 = meterFixture();

      const meter = {
        ...meterFixture1,
        user: { timezone: 'America/New_York', isEmailVerified: true, _id: meterFixture1.user },
      };

      allowDiskUseStub.resolves([meter]);
      aggregateStub.returns({ allowDiskUse: allowDiskUseStub });

      const result = await request.get('/admin/meters').send({
        meter: {
          active: true,
        },
        user: {
          isEmailVerified: true,
          timezone: 'America/New_York',
        },
        projection: {
          'user.isEmailVerified': 1,
          active: true,
          'user.timezone': 1,
        },
        skip: 1,
        limit: 1,
      });

      const aggregateStubCall = aggregateStub.getCall(0).args[0];

      expect(result.body).to.deep.equal({
        meters: [meter],
      });
      expect(aggregateStubCall.find((stage) => !!stage.$limit)).to.deep.equal({ $limit: 1 });
      expect(aggregateStubCall.find((stage) => !!stage.$skip)).to.deep.equal({ $skip: 1 });
      expect(allowDiskUseStub).to.have.been.called.calledOnceWithExactly(true);
    });
  });

  describe('PATCH /admin/refresh/:userId', () => {
    it('should succeed in refreshing a meter', async () => {
      const meter = meterFixture(userId, 3, 0);

      findOneStub.returns({ lean: () => meter });
      updateOneStub.resolves();
      getUserStub.returns({
        isEmailVerified: false,
        locale: 'US',
        _id: '60a457261311df0013f5544d',
        createdAt: '2021-05-19T00:09:10.551Z',
        timezone: 'America/Denver',
      });
      getTreatmentWithConfigStub.returns({
        treatment: 'surf_check_weekly_2',
        config: '{"meterLimit":5}',
      });
      const result = await request.patch(`/admin/refresh/${meter._id}`);

      expect(result.body).to.deep.equal({
        success: true,
        message: 'Successfully updated meter.',
      });
      expect(findOneStub).to.have.been.calledOnceWithExactly({ _id: meter._id });
      expect(getRateLimitingTreatmentWithConfigSpy).to.have.been.calledOnceWithExactly(userId, {
        id: userId,
        anonymousId: 'anonymousId',
        userId,
        isoCountryCode: 'US',
      });
      expect(updateOneStub).to.have.been.calledOnceWithExactly(
        { _id: meter._id },
        {
          meterRemaining: 5,
          meterLimit: 5,
          latestReset: Date.now(),
          metersConsumed: [],
        },
      );
      expect(trackEventStub).to.have.been.calledOnceWithExactly(
        'sl',
        'Surf Checks Reset',
        { userId: meter.user, anonymousId: meter.anonymousId },
        { checksAvailable: 5, meterId: meter._id, platform: 'web' },
      );
      expect(identifyStub).to.have.been.calledOnceWithExactly(
        'sl',
        meter.user,
        { zeroChecksLeft: false },
        null,
        null,
      );
    });

    it('should succeed in refreshing a meter with default meterLimit values - AU users', async () => {
      const meter = meterFixture(userId, 3, 0);

      findOneStub.returns({ lean: () => meter });
      updateOneStub.resolves();
      getUserStub.returns({
        isEmailVerified: false,
        locale: 'AU',
        _id: '60a457261311df0013f5544d',
        createdAt: '2021-05-19T00:09:10.551Z',
        timezone: 'Australia/Perth',
      });
      const result = await request.patch(`/admin/refresh/${meter._id}`);
      expect(result.body).to.deep.equal({
        success: true,
        message: 'Successfully updated meter.',
      });
      expect(findOneStub).to.have.been.calledOnceWithExactly({ _id: meter._id });
      expect(getRateLimitingTreatmentWithConfigSpy).to.not.have.been.called();
      expect(updateOneStub).to.have.been.calledOnceWithExactly(
        { _id: meter._id },
        {
          meterRemaining: meterUtils.METER_LIMIT,
          meterLimit: meterUtils.METER_LIMIT,
          latestReset: Date.now(),
          metersConsumed: [],
        },
      );
      expect(trackEventStub).to.have.been.calledOnceWithExactly(
        'sl',
        'Surf Checks Reset',
        { userId: meter.user, anonymousId: meter.anonymousId },
        { checksAvailable: meterUtils.METER_LIMIT, meterId: meter._id, platform: 'web' },
      );
      expect(identifyStub).to.have.been.calledOnceWithExactly(
        'sl',
        meter.user,
        { zeroChecksLeft: false },
        null,
        null,
      );
    });

    it('should throw an error if the user is not in A/B test', async () => {
      const meter = meterFixture(userId, 3, 0);

      findOneStub.returns({ lean: () => meter });
      getUserStub.returns({
        isEmailVerified: false,
        locale: 'US',
        _id: '60a457261311df0013f5544d',
        createdAt: '2021-05-19T00:09:10.551Z',
        timezone: 'America/Denver',
      });
      getTreatmentWithConfigStub.returns({ treatment: 'off', config: null });
      const result = await request.patch(`/admin/refresh/${meter._id}`);

      expect(result.body).to.deep.equal({
        message: 'Invalid Parameters: User not in Split treatment',
        errorHandler: 'APIError',
        name: 'APIError',
        code: 1022,
      });
      expect(getRateLimitingTreatmentWithConfigSpy).to.have.been.calledOnceWithExactly(userId, {
        id: userId,
        anonymousId: 'anonymousId',
        userId,
        isoCountryCode: 'US',
      });
      expect(result).to.have.status(400);
      expect(result.error).to.exist();
    });

    it('should throw an error if the A/B test is configured incorrectly', async () => {
      const meter = meterFixture(userId, 3, 0);

      findOneStub.returns({ lean: () => meter });
      getUserStub.returns({
        isEmailVerified: false,
        locale: 'US',
        _id: '60a457261311df0013f5544d',
        createdAt: '2021-05-19T00:09:10.551Z',
        timezone: 'America/Denver',
      });
      getTreatmentWithConfigStub.returns({
        treatment: 'surf_check_weekly_2',
        config: null,
      });
      const result = await request.patch(`/admin/refresh/${meter._id}`);

      expect(result.body).to.deep.equal({
        message:
          'Invalid Parameters: Split configured incorrectly. Not proceeding with Meter creation',
        errorHandler: 'APIError',
        name: 'APIError',
        code: 1023,
      });
      expect(getRateLimitingTreatmentWithConfigSpy).to.have.been.calledOnceWithExactly(userId, {
        id: userId,
        anonymousId: 'anonymousId',
        userId,
        isoCountryCode: 'US',
      });
      expect(result).to.have.status(500);
      expect(result.error).to.exist();
    });

    it('should throw an error if a meter is not found', async () => {
      const meter = meterFixture(userId, 3, 0);
      findOneStub.returns({ lean: () => null });
      const result = await request.patch(`/admin/refresh/${meter._id}`);

      expect(result.body).to.deep.equal({
        message: `No meter found with ID: ${meter._id}`,
      });
      expect(result).to.have.status(404);
      expect(result.error).to.exist();
    });
  });

  describe('PATCH /admin/:userId', () => {
    it('should succeed in patching a meter', async () => {
      const meter = meterFixture(userId, 3, 0);

      findOneStub.returns({ lean: () => meter });
      updateOneStub.resolves();
      const result = await request.patch(`/admin/${meter._id}`).send({ meterRemaining: 2 });

      expect(result.body).to.deep.equal({
        success: true,
        message: 'Successfully updated meter.',
      });
      expect(findOneStub).to.have.been.calledOnceWithExactly({ _id: meter._id });
      expect(updateOneStub).to.have.been.calledOnceWithExactly(
        { _id: meter._id },
        { meterRemaining: 2 },
      );
    });

    it('should strip non-whitelisted body fields', async () => {
      const meter = meterFixture(userId, 3, 0);

      findOneStub.returns({ lean: () => meter });
      updateOneStub.resolves();
      const result = await request
        .patch(`/admin/${meter._id}`)
        .send({ meterRemaining: 0, notWhitelisted: true });

      expect(result.body).to.deep.equal({
        success: true,
        message: 'Successfully updated meter.',
      });
      expect(findOneStub).to.have.been.calledOnceWithExactly({ _id: meter._id });
      expect(updateOneStub).to.have.been.calledOnceWithExactly(
        { _id: meter._id },
        { meterRemaining: 0 },
      );
    });

    it('should strip empty fields from the query', async () => {
      const meter = meterFixture(userId, 3, 0);

      findOneStub.returns({ lean: () => meter });
      updateOneStub.resolves();
      const result = await request.patch(`/admin/${meter._id}`).send({});

      expect(result.body).to.deep.equal({
        errorHandler: 'APIError',
        message:
          'Invalid Parameters: Cannot process update without valid whitelisted fields in the body',
        name: 'APIError',
      });
      expect(result).to.have.status(400);
      expect(result.error).to.exist();

      expect(findOneStub).to.have.been.calledOnceWithExactly({ _id: meter._id });
      expect(updateOneStub).to.have.not.been.called();
    });

    it('should throw an error if a meter is not found', async () => {
      const meter = meterFixture(userId, 3, 0);
      findOneStub.returns({ lean: () => null });
      const result = await request.patch(`/admin/${meter._id}`);

      expect(result.body).to.deep.equal({
        message: `No meter found with ID: ${meter._id}`,
      });
      expect(result).to.have.status(404);
      expect(result.error).to.exist();
    });

    it('should throw an error if meterRemaining is larger than 3', async () => {
      const meter = meterFixture(userId, 3, 0);
      findOneStub.returns({ lean: () => meter });
      const result = await request.patch(`/admin/${meter._id}`).send({ meterRemaining: 100 });

      expect(result.body).to.deep.equal({
        errorHandler: 'APIError',
        message: 'Invalid Parameters: Cannot make `meterRemaining` larger than 3.',
        name: 'APIError',
      });
      expect(result).to.have.status(400);
      expect(result.error).to.exist();
    });
  });
});
