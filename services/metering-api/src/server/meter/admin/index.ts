import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import {
  adminPatchMeterHandler,
  deleteMetersHandler,
  getTimezonesHandler,
  getMetersHandler,
  refreshMeterHandler,
} from './admin';

const admin = Router();

admin.delete('/meters', wrapErrors(deleteMetersHandler));
admin.get('/timezones', wrapErrors(getTimezonesHandler));
admin.get('/meters', wrapErrors(getMetersHandler));
admin.patch('/refresh/:meterId', wrapErrors(refreshMeterHandler));
admin.patch('/:meterId', wrapErrors(adminPatchMeterHandler));

export default admin;
