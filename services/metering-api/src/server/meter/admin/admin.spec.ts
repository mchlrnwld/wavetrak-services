import { expect } from 'chai';
import {
  adminPatchMeterHandler,
  deleteMetersHandler,
  getMetersHandler,
  getTimezonesHandler,
  refreshMeterHandler,
} from './admin';

describe('Admin handlers', () => {
  // For now all the handler logic is covered in the express tests (index.spec.ts)
  // so this is just a scaffold to make creating tests easier later

  it('should have all the handlers', () => {
    expect(deleteMetersHandler).to.exist();
    expect(getMetersHandler).to.exist();
    expect(getTimezonesHandler).to.exist();
    expect(refreshMeterHandler).to.exist();
    expect(adminPatchMeterHandler).to.exist();
  });
});
