import mongoose from 'mongoose';
import {
  trackEvent,
  identify,
  SurflineRequest,
  NotFound,
  APIError,
  getPlatform,
} from '@surfline/services-common';
import {
  pick as _pick,
  pickBy as _pickBy,
  isNull as _isNull,
  isUndefined as _isUndefined,
} from 'lodash';
import { Response } from 'express';

import log from '../../../common/logger';
import Meter from '../../../models/Meter';
import { OFF, getRateLimitingTreatmentWithConfig, METER_LIMIT } from '../../../utils/meter';
import { getUser } from '../../../external/user';

/**
 * Endpoint for permanently deleting meters to satisfy Apple and GDPR requirements
 */
export const deleteMetersHandler = async (
  req: SurflineRequest,
  res: Response,
): Promise<Response> => {
  const { user } = req.body;

  if (!user) return res.status(400).send({ message: 'Missing user parameter' });

  try {
    const meter = await Meter.findOne({ user });

    if (!meter) return res.status(404).send({ message: `No meter found for user ${user}` });

    meter.delete();
    return res.send({ message: `Successfully deleted meter ${meter._id} for user: ${user}` });
  } catch (error) {
    return res.status(500).send({
      message: 'Error deleting meter',
      error,
    });
  }
};

export const getTimezonesHandler = async (_: SurflineRequest, res: Response): Promise<Response> => {
  const aggregation = await Meter.aggregate<{ timezones: string[] }>([
    {
      $match: {
        active: true,
      },
    },
    {
      $lookup: {
        from: 'UserInfo',
        localField: 'user',
        foreignField: '_id',
        as: 'user',
      },
    },
    {
      $unwind: {
        path: '$user',
      },
    },
    {
      $group: {
        _id: null,
        timezones: {
          $addToSet: '$user.timezone',
        },
      },
    },
  ]).allowDiskUse(true);

  return res.send({ timezones: aggregation[0]?.timezones || [] });
};

export const getMetersHandler = async (req: SurflineRequest, res: Response): Promise<Response> => {
  const { meter, user, projection, limit, skip } = req.body;

  const aggregation = await Meter.aggregate<{ _id: mongoose.Types.ObjectId }>(
    [
      {
        $match: {
          ...meter,
        },
      },
      {
        $lookup: {
          from: 'UserInfo',
          let: {
            userId: '$user',
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ['$_id', '$$userId'],
                },
                ...user,
              },
            },
          ],
          as: 'user',
        },
      },
      {
        $unwind: {
          path: '$user',
        },
      },
      projection && { $project: projection },
      {
        $sort: { _id: 1 },
      },
      skip && { $skip: skip },
      limit && { $limit: limit },
    ].filter(Boolean),
  ).allowDiskUse(true);

  return res.send({ meters: aggregation });
};

export const refreshMeterHandler = async (
  req: SurflineRequest,
  res: Response,
): Promise<Response> => {
  const { meterId } = req.params;
  const meter = await Meter.findOne({ _id: meterId }).lean();
  if (!meter) {
    throw new NotFound(`No meter found with ID: ${meterId}`);
  }
  const userId = meter?.user.toString();
  const { locale: isoCountryCode } = await getUser(userId);
  const identity = {
    id: userId,
    anonymousId: meter?.anonymousId,
    userId,
    isoCountryCode,
  };

  /*
    For AU users who already have a Meter document set meterLimit to 3.
    For all non AU users call Split and get the meterLimit.
  */
  let meterLimit = METER_LIMIT;
  if (isoCountryCode && isoCountryCode !== 'AU') {
    const result = getRateLimitingTreatmentWithConfig(userId, identity);
    if (result?.treatmentState === OFF) {
      throw new APIError('User not in Split treatment', 400, 1022);
    }
    if (!result?.meterLimit) {
      throw new APIError(
        'Split configured incorrectly. Not proceeding with Meter creation',
        500,
        1023,
      );
    }
    meterLimit = result?.meterLimit;
  }
  await Meter.updateOne(
    { _id: meterId },
    { meterRemaining: meterLimit, meterLimit, latestReset: Date.now(), metersConsumed: [] },
  );

  identify('sl', meter?.user?.toString(), { zeroChecksLeft: false }, null, null);
  const platform = getPlatform(req.headers['user-agent']);
  trackEvent(
    'sl',
    'Surf Checks Reset',
    { userId: meter.user.toString(), anonymousId: meter.anonymousId },
    { checksAvailable: meterLimit, meterId: meter._id.toString(), platform },
  );
  const logData = {
    userId: meter.user.toString(),
    anonymousId: meter.anonymousId,
    action: 'Surf Checks Reset',
    meterRemaining: meterLimit,
  };
  log.info({ logData }, `RateLimit User Lifecycle`);
  return res.send({ success: true, message: 'Successfully updated meter.' });
};

export const adminPatchMeterHandler = async (
  req: SurflineRequest,
  res: Response,
): Promise<Response<{ success: boolean; message: string }>> => {
  const { meterId } = req.params;

  const body = _pick<{ meterRemaining?: number }>(req.body, ['meterRemaining']);
  const { meterRemaining } = body;

  const meter = await Meter.findOne({ _id: meterId }).lean();

  if (!meter) {
    throw new NotFound(`No meter found with ID: ${meterId}`);
  }

  const maxMeterRemainingUpdate = meter.meterLimit || 3;
  if (meterRemaining > maxMeterRemainingUpdate) {
    throw new APIError('Cannot make `meterRemaining` larger than 3.', 400);
  }

  const updateQuery = _pickBy({ meterRemaining }, (val) => !_isNull(val) && !_isUndefined(val));

  if (!updateQuery || Object.keys(updateQuery).length === 0) {
    throw new APIError('Cannot process update without valid whitelisted fields in the body');
  }

  await Meter.updateOne({ _id: meterId }, updateQuery);

  const zeroChecksLeft = !(meterRemaining > -1);
  identify('sl', meter?.user?.toString(), { zeroChecksLeft }, null, null);

  const logData = {
    userId: meter.user.toString(),
    action: 'Meter updated via Admin',
    meterRemaining,
    referrer: req.get('Referrer'),
  };
  log.info({ logData }, `RateLimit User Lifecycle`);

  return res.send({ success: true, message: 'Successfully updated meter.' });
};
