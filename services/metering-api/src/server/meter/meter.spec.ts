import sinon from 'sinon';
import newrelic from 'newrelic';
import { expect } from 'chai';
import mongoose from 'mongoose';
import * as servicesCommon from '@surfline/services-common';
import { ImportMock } from 'ts-mock-imports';
import { SurflineRequest } from '@surfline/services-common';
import { Response } from 'express';
import Meter, * as meterModel from '../../models/Meter';
import meterFixture from '../../fixtures/meter';
import {
  getMeterHandler,
  postMeterHandler,
  surfCheckHandler,
  patchMeterHandler,
  getSpotCheckAnonymousMeterHandler,
  hasActiveSurfCheck,
  incrementSpotCheckAnonymousMeterHandler,
  verifySpotCheckAnonymousMeterHandler,
} from './meter';
import * as userExternal from '../../external/user';
import * as redisExternal from '../../external/redis';
import * as entitlementExternal from '../../external/entitlement';
import * as meterUtils from '../../utils/meter';

describe('Metering Handlers', () => {
  const userId = mongoose.Types.ObjectId().toString();
  let trackEventStub: sinon.SinonStub;
  let getUserStub: sinon.SinonStub;
  let getUserEntitlementsStub: sinon.SinonStub;
  let setUserTimezoneStub: sinon.SinonStub;
  let recordCustomEventStub: sinon.SinonStub;
  let addCustomAttributeStub: sinon.SinonStub;
  let getTreatmentWithConfigStub: sinon.SinonStub;
  let checkIfABTestSupportedSpy: sinon.SinonSpy;
  let getRateLimitingTreatmentWithConfigSpy: sinon.SinonSpy;

  beforeEach(() => {
    trackEventStub = sinon.stub(servicesCommon, 'trackEvent');
    getUserStub = ImportMock.mockFunction(userExternal, 'getUser');
    getUserEntitlementsStub = ImportMock.mockFunction(entitlementExternal, 'getUserEntitlements');
    setUserTimezoneStub = ImportMock.mockFunction(userExternal, 'setUserTimezone');
    recordCustomEventStub = sinon.stub(newrelic, 'recordCustomEvent');
    addCustomAttributeStub = sinon.stub(newrelic, 'addCustomAttribute');
    getTreatmentWithConfigStub = sinon.stub(servicesCommon, 'getTreatmentWithConfig');
    checkIfABTestSupportedSpy = sinon.spy(meterUtils, 'checkIfABTestSupported');
    getRateLimitingTreatmentWithConfigSpy = sinon.spy(
      meterUtils,
      'getRateLimitingTreatmentWithConfig',
    );
  });

  afterEach(() => {
    trackEventStub.restore();
    getUserStub.restore();
    getUserEntitlementsStub.restore();
    setUserTimezoneStub.restore();
    recordCustomEventStub.restore();
    addCustomAttributeStub.restore();
    getTreatmentWithConfigStub.restore();
    checkIfABTestSupportedSpy.restore();
    getRateLimitingTreatmentWithConfigSpy.restore();
  });

  describe('getMeterHandler', () => {
    let getMeterByUserStub: sinon.SinonStub;

    beforeEach(() => {
      getMeterByUserStub = ImportMock.mockFunction(meterModel, 'getMeterByUser');
    });

    afterEach(() => {
      getMeterByUserStub.restore();
    });

    it('should return a metered user document', async () => {
      const req: Partial<SurflineRequest> = {
        authenticatedUserId: userId,
      };
      const sendStub = sinon.stub();
      const res: Partial<Response> = {
        status: sinon.stub().returns({ send: sendStub }),
      };

      const meter = meterFixture(userId);
      getMeterByUserStub.resolves(meter);
      getUserStub.resolves({ timezone: 'America/Los_Angeles' });

      await getMeterHandler(req as SurflineRequest, res as Response);

      expect(getMeterByUserStub).to.have.been.calledOnceWithExactly(userId);
      expect(res.status).to.have.been.calledOnceWithExactly(200);
      expect(sendStub).to.have.been.calledOnceWithExactly({
        ...meter,
        timezone: 'America/Los_Angeles',
        treatmentState: 'on',
        treatmentVariant: 'on',
      });
    });

    it('should throw an error if a metering document is not found', async () => {
      const req: Partial<SurflineRequest> = {
        authenticatedUserId: userId,
      };

      getMeterByUserStub.resolves(null);

      const res: Partial<Response> = {};
      let err: Error;
      try {
        await getMeterHandler(req as SurflineRequest, res as Response);
      } catch (error) {
        err = error;
        expect(error).to.exist();
        expect(error.name).to.be.equal('NotFound');
        expect(error.message).to.be.equal(`Metering document not found for user: ${userId}`);
      }
      expect(err).to.exist();
    });
  });

  describe('postMeterHandler', () => {
    const anonymousId = 'anonymousId';
    let createMeterStub: sinon.SinonStub;
    let existsStub: sinon.SinonStub;

    const sendStub = sinon.stub();
    const req: Partial<SurflineRequest> = {
      authenticatedUserId: userId,
      body: {
        anonymousId,
        countryCode: 'US',
      },
      get: () => null,
    };
    const res: Partial<Response> = {
      send: sendStub,
    };

    beforeEach(() => {
      createMeterStub = ImportMock.mockFunction(meterModel, 'createMeter');
      existsStub = ImportMock.mockFunction(meterModel.default, 'exists');
    });

    afterEach(() => {
      sendStub.resetHistory();
      createMeterStub.restore();
      existsStub.restore();
    });

    it('should create a metering document for users in A/B test - Web requests', async () => {
      const updatedReq: Partial<SurflineRequest> = {
        authenticatedUserId: userId,
        body: {
          anonymousId,
          countryCode: 'US',
        },
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        get: (name: string): any => {
          if (name === 'user-agent')
            return 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36';
          if (name === 'x-app-version') return null;
          return null;
        },
      };
      const meter = meterFixture(userId);
      createMeterStub.resolves({
        ...meter,
        meterLimit: 5,
        meterRemaining: 5,
      });
      existsStub.resolves(false);
      getUserStub.returns({ isEmailVerified: true });
      getUserEntitlementsStub.resolves({ entitlements: [] });
      getTreatmentWithConfigStub.returns({
        treatment: 'surf_check_weekly_2',
        config: '{"meterLimit":5}',
      });

      await postMeterHandler(updatedReq as SurflineRequest, res as Response);
      expect(checkIfABTestSupportedSpy).to.have.been.calledOnceWithExactly('web', null);
      expect(getRateLimitingTreatmentWithConfigSpy).to.have.been.calledOnceWithExactly(userId, {
        id: userId,
        anonymousId: 'anonymousId',
        userId,
        isoCountryCode: 'US',
      });

      expect(createMeterStub).to.have.been.calledOnceWithExactly({
        user: userId,
        anonymousId: 'anonymousId',
        countryCode: 'US',
        meterLimit: 5,
        meterRemaining: 5,
      });

      expect(res.send).to.have.been.calledOnceWithExactly({
        id: meter._id,
        anonymousId,
        entitlement: 'sl_metered',
        meterLimit: 5,
        countryCode: 'US',
        meterRemaining: 5,
        treatmentState: 'on',
        treatmentVariant: 'on',
        message: 'Meter successfully created.',
      });
    });

    it('should create a metering document for users in A/B test - Android version above 4.1.2', async () => {
      const updatedReq: Partial<SurflineRequest> = {
        authenticatedUserId: userId,
        body: {
          anonymousId,
          countryCode: 'US',
        },
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        get: (name: string): any => {
          if (name === 'user-agent')
            return 'Surfline/4.1.1 (com.surfline.android; build:1; Android 10) okhttp/4.9.1';
          if (name === 'x-app-version') return '4.1.2';
          return null;
        },
      };
      const meter = meterFixture(userId);
      createMeterStub.resolves({
        ...meter,
        meterLimit: 5,
        meterRemaining: 5,
      });
      existsStub.resolves(false);
      getUserStub.returns({ isEmailVerified: true });
      getUserEntitlementsStub.resolves({ entitlements: [] });
      getTreatmentWithConfigStub.returns({
        treatment: 'surf_check_weekly_2',
        config: '{"meterLimit":5}',
      });

      await postMeterHandler(updatedReq as SurflineRequest, res as Response);
      expect(checkIfABTestSupportedSpy).to.have.been.calledOnceWithExactly('android', '4.1.2');
      expect(getRateLimitingTreatmentWithConfigSpy).to.have.been.calledOnceWithExactly(userId, {
        id: userId,
        anonymousId: 'anonymousId',
        userId,
        isoCountryCode: 'US',
      });

      expect(createMeterStub).to.have.been.calledOnceWithExactly({
        user: userId,
        anonymousId: 'anonymousId',
        countryCode: 'US',
        meterLimit: 5,
        meterRemaining: 5,
      });

      expect(res.send).to.have.been.calledOnceWithExactly({
        id: meter._id,
        anonymousId,
        entitlement: 'sl_metered',
        meterLimit: 5,
        countryCode: 'US',
        meterRemaining: 5,
        treatmentState: 'on',
        treatmentVariant: 'on',
        message: 'Meter successfully created.',
      });
    });

    it('should create a metering document for users in A/B test - iOS version above 7.4.0', async () => {
      const updatedReq: Partial<SurflineRequest> = {
        authenticatedUserId: userId,
        body: {
          anonymousId,
          countryCode: 'US',
        },
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        get: (name: string): any => {
          if (name === 'user-agent')
            return 'Surfline/7.1.1 (com.surfline.surfapp; build:0; iOS 14.4.2) Alamofire/4.9.1';
          if (name === 'x-app-version') return '7.4.0';
          return null;
        },
      };
      const meter = meterFixture(userId);
      createMeterStub.resolves({
        ...meter,
        meterLimit: 5,
        meterRemaining: 5,
      });
      existsStub.resolves(false);
      getUserStub.returns({ isEmailVerified: true });
      getUserEntitlementsStub.resolves({ entitlements: [] });
      getTreatmentWithConfigStub.returns({
        treatment: 'surf_check_weekly_2',
        config: '{"meterLimit":5}',
      });

      await postMeterHandler(updatedReq as SurflineRequest, res as Response);
      expect(checkIfABTestSupportedSpy).to.have.been.calledOnceWithExactly('ios', '7.4.0');
      expect(getRateLimitingTreatmentWithConfigSpy).to.have.been.calledOnceWithExactly(userId, {
        id: userId,
        anonymousId: 'anonymousId',
        userId,
        isoCountryCode: 'US',
      });

      expect(createMeterStub).to.have.been.calledOnceWithExactly({
        user: userId,
        anonymousId: 'anonymousId',
        countryCode: 'US',
        meterLimit: 5,
        meterRemaining: 5,
      });

      expect(res.send).to.have.been.calledOnceWithExactly({
        id: meter._id,
        anonymousId,
        entitlement: 'sl_metered',
        meterLimit: 5,
        countryCode: 'US',
        meterRemaining: 5,
        treatmentState: 'on',
        treatmentVariant: 'on',
        message: 'Meter successfully created.',
      });
    });

    it('should create a metering document for a user (legacy RL implementation) - iOS version below 7.4.0', async () => {
      // For now iOS requests will get the legacy functionality.
      const updatedReq: Partial<SurflineRequest> = {
        authenticatedUserId: userId,
        body: {
          anonymousId,
          countryCode: 'US',
        },
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        get: (name: string): any => {
          if (name === 'user-agent')
            return 'Surfline/7.1.1 (com.surfline.surfapp; build:0; iOS 14.4.2) Alamofire/4.9.1';
          if (name === 'x-app-version') return '7.2.3';
          return null;
        },
      };
      const meter = meterFixture(userId);
      createMeterStub.resolves(meter);
      existsStub.resolves(false);
      getUserStub.returns({ isEmailVerified: true });
      getUserEntitlementsStub.resolves({ entitlements: [] });

      await postMeterHandler(updatedReq as SurflineRequest, res as Response);
      expect(checkIfABTestSupportedSpy).to.have.been.calledOnceWithExactly('ios', '7.2.3');
      expect(getRateLimitingTreatmentWithConfigSpy.called).to.be.false();

      expect(createMeterStub).to.have.been.calledOnceWithExactly({
        user: userId,
        anonymousId: 'anonymousId',
        countryCode: 'US',
        meterLimit: 3,
        meterRemaining: 3,
      });

      expect(res.send).to.have.been.calledOnceWithExactly({
        id: meter._id,
        anonymousId,
        entitlement: 'sl_metered',
        meterLimit: 3,
        countryCode: 'US',
        meterRemaining: 3,
        treatmentState: 'on',
        treatmentVariant: 'on',
        message: 'Meter successfully created.',
      });
    });

    it('should throw an error if the user is not in A/B test', async () => {
      const updatedReq: Partial<SurflineRequest> = {
        authenticatedUserId: userId,
        body: {
          anonymousId,
          countryCode: 'US',
        },
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        get: (name: string): any => {
          if (name === 'user-agent')
            return 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36';
          if (name === 'x-app-version') return null;
          return null;
        },
      };
      const meter = meterFixture(userId);
      createMeterStub.resolves(meter);
      existsStub.resolves(false);
      getUserStub.returns({ isEmailVerified: true });
      getUserEntitlementsStub.resolves({ entitlements: [] });
      getTreatmentWithConfigStub.returns({ treatment: 'off', config: null });
      let err: Error;
      try {
        await postMeterHandler(updatedReq as SurflineRequest, res as Response);
      } catch (error) {
        expect(checkIfABTestSupportedSpy).to.have.been.calledOnceWithExactly('web', null);
        expect(getRateLimitingTreatmentWithConfigSpy).to.have.been.calledOnceWithExactly(userId, {
          id: userId,
          anonymousId: 'anonymousId',
          userId,
          isoCountryCode: 'US',
        });
        expect(createMeterStub.called).to.be.false();
        err = error;
        expect(error).to.exist();
        expect(error.statusCode).to.equal(400);
        expect(error.name).to.be.equal('APIError');
        expect(error.message).to.be.equal('Invalid Parameters: User not in Split treatment');
        expect(error.wavetrakCode).to.be.equal(1022);
      }
      expect(err).to.exist();
    });

    it('should throw an error if the A/B test is configured incorrectly', async () => {
      const updatedReq: Partial<SurflineRequest> = {
        authenticatedUserId: userId,
        body: {
          anonymousId,
          countryCode: 'US',
        },
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        get: (name: string): any => {
          if (name === 'user-agent')
            return 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36';
          if (name === 'x-app-version') return null;
          return null;
        },
      };
      const meter = meterFixture(userId);
      createMeterStub.resolves(meter);
      existsStub.resolves(false);
      getUserStub.returns({ isEmailVerified: true });
      getUserEntitlementsStub.resolves({ entitlements: [] });
      getTreatmentWithConfigStub.returns({
        treatment: 'surf_check_weekly_2',
        config: null,
      });
      let err: Error;
      try {
        await postMeterHandler(updatedReq as SurflineRequest, res as Response);
      } catch (error) {
        expect(checkIfABTestSupportedSpy).to.have.been.calledOnceWithExactly('web', null);
        expect(getRateLimitingTreatmentWithConfigSpy).to.have.been.calledOnceWithExactly(userId, {
          id: userId,
          anonymousId: 'anonymousId',
          userId,
          isoCountryCode: 'US',
        });
        expect(createMeterStub.called).to.be.false();
        err = error;
        expect(error).to.exist();
        expect(error.statusCode).to.equal(500);
        expect(error.name).to.be.equal('APIError');
        expect(error.message).to.be.equal(
          'Invalid Parameters: Split configured incorrectly. Not proceeding with Meter creation',
        );
        expect(error.wavetrakCode).to.be.equal(1023);
      }
      expect(err).to.exist();
    });

    it('should give a user 3 refreshing surf checks', async () => {
      const meter = meterFixture(userId);
      createMeterStub.resolves(meter);
      existsStub.resolves(false);
      getUserStub.returns({ createdAt: '2020-08-27T00:00:00.000Z', isEmailVerified: true });
      getUserEntitlementsStub.resolves({ entitlements: [] });
      getTreatmentWithConfigStub.returns({ config: '{"meterLimit":3}' });

      await postMeterHandler(req as SurflineRequest, res as Response);

      expect(createMeterStub).to.have.been.calledOnceWithExactly({
        user: userId,
        anonymousId: 'anonymousId',
        countryCode: 'US',
        meterLimit: 3,
        meterRemaining: 3,
      });
    });

    it('should throw an error if the anonymousId is missing', async () => {
      const customReq: Partial<SurflineRequest> = {
        ...req,
        body: {},
      };
      const newRes: Partial<Response> = {};
      let err: Error;
      try {
        await postMeterHandler(customReq as SurflineRequest, newRes as Response);
      } catch (error) {
        err = error;
        expect(error).to.exist();
        expect(error.name).to.be.equal('APIError');
        expect(error.message).to.be.equal(
          'Invalid Parameters: anonymousId is required in the body.',
        );
      }
      expect(err).to.exist();
    });

    it('should throw an error if the user is Premium', async () => {
      getUserEntitlementsStub.resolves({ entitlements: ['sl_premium'] });
      const newRes: Partial<Response> = {};
      let err: Error;
      try {
        await postMeterHandler(req as SurflineRequest, newRes as Response);
      } catch (error) {
        err = error;
        expect(error).to.exist();
        expect(error.statusCode).to.equal(400);
        expect(error.name).to.be.equal('APIError');
        expect(error.message).to.be.equal('Invalid Parameters: Not supported for Premium users');
        expect(error.wavetrakCode).to.be.equal(1021);
      }
      expect(err).to.exist();
    });

    it('should throw an error if the userId already exists', async () => {
      getUserEntitlementsStub.resolves({ entitlements: [] });
      const newRes: Partial<Response> = {};
      existsStub.withArgs({ user: userId }).resolves(true);
      existsStub.resolves(false);
      let err: Error;
      try {
        await postMeterHandler(req as SurflineRequest, newRes as Response);
      } catch (error) {
        err = error;
        expect(error).to.exist();
        expect(error.statusCode).to.equal(409);
        expect(error.name).to.be.equal('APIError');
        expect(error.message).to.be.equal(
          'Invalid Parameters: User ID already exists on another meter document',
        );
        expect(error.wavetrakCode).to.be.equal(1020);
      }
      expect(err).to.exist();
    });
  });

  describe('surfCheckHandler', () => {
    let decrementMeterStub: sinon.SinonStub;
    let getMeterByUserStub: sinon.SinonStub;
    let getSurfCheckMeterTTLStub: sinon.SinonStub;
    let resetSurfCheckMeterTTLStub: sinon.SinonStub;
    let createSurfCheckMeterKeyStub: sinon.SinonStub;
    let updateOneStub: sinon.SinonStub;

    beforeEach(() => {
      getMeterByUserStub = sinon.stub(meterModel, 'getMeterByUser');
      decrementMeterStub = ImportMock.mockFunction(meterModel, 'decrementMeter');
      getSurfCheckMeterTTLStub = ImportMock.mockFunction(redisExternal, 'getSurfCheckMeterTTL');
      resetSurfCheckMeterTTLStub = ImportMock.mockFunction(redisExternal, 'resetSurfCheckMeterTTL');
      createSurfCheckMeterKeyStub = ImportMock.mockFunction(
        redisExternal,
        'createSurfCheckMeterKey',
      );
      updateOneStub = sinon.stub(Meter, 'updateOne');
    });

    afterEach(() => {
      decrementMeterStub.restore();
      getMeterByUserStub.restore();
      getSurfCheckMeterTTLStub.restore();
      resetSurfCheckMeterTTLStub.restore();
      createSurfCheckMeterKeyStub.restore();
      updateOneStub.restore();
    });

    it('should handle consuming a surf check if the user does not have an active one', async () => {
      const req: Partial<SurflineRequest> = {
        authenticatedUserId: userId,
        get: () => null,
        headers: {
          'user-agent':
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36',
        },
      };
      const res: Partial<Response> = {
        send: sinon.stub(),
      };

      const meter = meterFixture(userId);
      getSurfCheckMeterTTLStub.resolves(-2);
      createSurfCheckMeterKeyStub.resolves(1);
      getMeterByUserStub.resolves(meter);
      decrementMeterStub.resolves({ ...meter, meterRemaining: 0 });
      updateOneStub.returns({ exec: () => meter });

      await surfCheckHandler(req as SurflineRequest, res as Response);

      expect(decrementMeterStub).to.have.been.calledOnceWithExactly(userId);
      expect(getSurfCheckMeterTTLStub).to.have.been.calledOnceWithExactly(userId);
      expect(createSurfCheckMeterKeyStub).to.have.been.calledOnceWithExactly(userId);
      expect(res.send).to.have.been.calledOnceWithExactly({
        meterRemaining: 0,
        message: 'Successfully started surf check',
      });
    });

    it('should send a segment event when registering a surf check', async () => {
      const req: Partial<SurflineRequest> = {
        authenticatedUserId: userId,
        get: () => null,
        headers: {
          'user-agent':
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36',
        },
      };
      const res: Partial<Response> = {
        send: sinon.stub(),
      };

      const meter = meterFixture(userId);
      getMeterByUserStub.resolves(meter);
      getSurfCheckMeterTTLStub.resolves(-2);
      createSurfCheckMeterKeyStub.resolves(1);
      decrementMeterStub.resolves({ ...meter, meterRemaining: 0 });
      updateOneStub.returns({ exec: () => meter });

      await surfCheckHandler(req as SurflineRequest, res as Response);

      expect(trackEventStub).to.have.been.called.calledOnceWithExactly(
        'sl',
        'Used Surf Check',
        { userId, anonymousId: meter.anonymousId },
        { checksAvailable: 0, platform: 'web' },
      );
    });

    it('should extend an already existing surf-check', async () => {
      const req: Partial<SurflineRequest> = {
        authenticatedUserId: userId,
        get: () => null,
      };
      const res: Partial<Response> = {
        send: sinon.stub(),
      };

      const meter = meterFixture(userId);
      getMeterByUserStub.resolves(meter);
      getSurfCheckMeterTTLStub.resolves(100);
      resetSurfCheckMeterTTLStub.resolves(1);

      await surfCheckHandler(req as SurflineRequest, res as Response);

      expect(decrementMeterStub).to.not.have.been.called();
      expect(trackEventStub).to.not.have.been.called();
      expect(getSurfCheckMeterTTLStub).to.have.been.calledOnceWithExactly(userId);
      expect(resetSurfCheckMeterTTLStub).to.have.been.calledOnceWithExactly(userId);
      expect(res.send).to.have.been.calledOnceWithExactly({
        message: 'Successfully extended surf check',
        meterRemaining: meter.meterRemaining,
        extended: true,
      });
    });

    it('should throw a not found error if a metering document is not found', async () => {
      const req: Partial<SurflineRequest> = {
        authenticatedUserId: userId,
      };
      const res: Partial<Response> = {
        send: sinon.stub(),
      };

      getMeterByUserStub.resolves(null);
      let err: Error;
      try {
        await surfCheckHandler(req as SurflineRequest, res as Response);
      } catch (error) {
        err = error;
        expect(error).to.exist();
        expect(error.name).to.equal('NotFound');
        expect(error.message).to.equal(`No meter found for user: ${userId}`);
      }
      expect(err).to.exist();
    });
  });

  describe('patchMeterHandler', () => {
    let findOneAndUpdateStub: sinon.SinonStub;

    const sendStub = sinon.stub();
    const req: Partial<SurflineRequest> = {
      authenticatedUserId: userId,
      body: {},
      get: () => null,
    };
    const res: Partial<Response> = {
      send: sendStub,
    };

    beforeEach(() => {
      findOneAndUpdateStub = sinon.stub(meterModel.default, 'findOneAndUpdate');
    });

    afterEach(() => {
      findOneAndUpdateStub.restore();
      sendStub.resetHistory();
    });

    it('should handle updating the timezone for a user if they have the incorrect UTC timezone', async () => {
      getUserStub.resolves({ timezone: 'UTC' });
      await patchMeterHandler(
        { ...req, body: { timezone: 'America/Los_Angeles' } } as SurflineRequest,
        res as Response,
      );

      expect(getUserStub).to.have.been.calledOnceWithExactly(userId);
      expect(setUserTimezoneStub).to.have.been.calledOnceWithExactly(userId, 'America/Los_Angeles');
      expect(sendStub).to.have.been.calledOnceWithExactly({
        success: true,
        message: 'Meter successfully updated.',
      });
    });

    it('should not call setUserTimezone if the existing timezone stored is not UTC', async () => {
      getUserStub.resolves({ timezone: 'America/Los_Angeles' });
      try {
        await patchMeterHandler(
          { ...req, body: { timezone: 'America/Los_Angeles' } } as SurflineRequest,
          res as Response,
        );
        expect(getUserStub).to.have.been.calledOnceWithExactly(userId);
        expect(setUserTimezoneStub).to.not.have.been.called();
      } catch (err) {
        expect(err.message).to.contain('User timezone is in non UTC');
      }
    });
  });

  describe('getSpotCheckAnonymousMeterHandler', () => {
    let getSpotCheckAnonymousMeterKeyStub: sinon.SinonStub;

    const sendStub = sinon.stub().resolves({
      meterRemaining: '1',
    });

    const req: Partial<SurflineRequest> = {};

    req.params = {
      anonymousId: 'test_anonymousId',
    };

    const res: Partial<Response> = {
      status: sinon.stub().returns({ send: sendStub }),
      send: sendStub,
    };

    beforeEach(() => {
      getSpotCheckAnonymousMeterKeyStub = ImportMock.mockFunction(
        redisExternal,
        'getSpotCheckAnonymousMeterKey',
      );
    });
    afterEach(() => {
      sendStub.resetHistory();
      (res.status as sinon.SinonStub).resetHistory();
      getSpotCheckAnonymousMeterKeyStub.restore();
    });

    it('should return the number of surf checks left for an anonymous user', async () => {
      getSpotCheckAnonymousMeterKeyStub.resolves('2');
      await getSpotCheckAnonymousMeterHandler(req as SurflineRequest, res as Response);
      expect(getSpotCheckAnonymousMeterKeyStub).to.have.been.calledOnceWithExactly(
        'test_anonymousId',
      );
      expect(sendStub).to.have.been.calledOnceWithExactly({ meterRemaining: 2 });
    });

    it('should return meterRemaining as null if anonymousId key does not exist', async () => {
      getSpotCheckAnonymousMeterKeyStub.resolves(null);
      await getSpotCheckAnonymousMeterHandler(req as SurflineRequest, res as Response);
      expect(getSpotCheckAnonymousMeterKeyStub).to.have.been.calledOnceWithExactly(
        'test_anonymousId',
      );
      expect(res.status).to.have.been.calledOnceWithExactly(404);
      expect(sendStub).to.have.been.calledOnceWithExactly({ meterRemaining: null });
    });

    it('should return meterRemaining as -1 if there are no surf checks left', async () => {
      getSpotCheckAnonymousMeterKeyStub.resolves(4);
      await getSpotCheckAnonymousMeterHandler(req as SurflineRequest, res as Response);
      expect(getSpotCheckAnonymousMeterKeyStub).to.have.been.calledOnceWithExactly(
        'test_anonymousId',
      );
      expect(sendStub).to.have.been.calledOnceWithExactly({ meterRemaining: -1 });
    });

    it('should throw an error if required fields are not present', async () => {
      let err: Error;
      req.params = {
        anonymousId: null,
      };
      try {
        await getSpotCheckAnonymousMeterHandler(req as SurflineRequest, res as Response);
      } catch (error) {
        err = error;
        expect(error).to.exist();
        expect(error.name).to.be.equal('APIError');
        expect(error.message).to.be.equal(
          'Invalid Parameters: anonymousId is required in the url.',
        );
      }
      expect(err).to.exist();
    });
  });

  describe('hasActiveSurfCheck', () => {
    let getSurfCheckMeterTTLStub: sinon.SinonStub;
    const req: Partial<SurflineRequest> = {
      authenticatedUserId: userId,
    };
    beforeEach(() => {
      getSurfCheckMeterTTLStub = ImportMock.mockFunction(redisExternal, 'getSurfCheckMeterTTL');
    });
    afterEach(() => {
      getSurfCheckMeterTTLStub.restore();
    });
    it('should return true if there is an active surf check', async () => {
      const res: Partial<Response> = {
        send: sinon.stub(),
      };
      getSurfCheckMeterTTLStub.resolves(100);
      await hasActiveSurfCheck(req as SurflineRequest, res as Response);
      expect(res.send).to.have.been.calledOnceWithExactly({
        hasActiveSurfCheck: true,
      });
    });

    it('should return false if there is no active surf check', async () => {
      const res: Partial<Response> = {
        send: sinon.stub(),
      };
      getSurfCheckMeterTTLStub.resolves(-1);
      await hasActiveSurfCheck(req as SurflineRequest, res as Response);
      expect(res.send).to.have.been.calledOnceWithExactly({
        hasActiveSurfCheck: false,
      });
    });
  });

  describe('incrementSpotCheckAnonymousMeterHandler', () => {
    let incrementSpotCheckAnonymousMeterKeyStub: sinon.SinonStub;
    let getSpotCheckAnonymousMeterKeyStub: sinon.SinonStub;
    const sendStub = sinon.stub();
    const req: Partial<SurflineRequest> = {};
    req.params = {
      anonymousId: 'test_anonymousId',
    };
    const res: Partial<Response> = {
      send: sendStub,
    };
    beforeEach(() => {
      incrementSpotCheckAnonymousMeterKeyStub = ImportMock.mockFunction(
        redisExternal,
        'incrementSpotCheckAnonymousMeterKey',
      );
      getSpotCheckAnonymousMeterKeyStub = ImportMock.mockFunction(
        redisExternal,
        'getSpotCheckAnonymousMeterKey',
      );
    });
    afterEach(() => {
      sendStub.resetHistory();
      incrementSpotCheckAnonymousMeterKeyStub.restore();
      getSpotCheckAnonymousMeterKeyStub.restore();
    });

    it('should increment a new surf check for anonymous session', async () => {
      incrementSpotCheckAnonymousMeterKeyStub.resolves('1');
      getSpotCheckAnonymousMeterKeyStub.resolves(null);
      await incrementSpotCheckAnonymousMeterHandler(req as SurflineRequest, res as Response);
      expect(incrementSpotCheckAnonymousMeterKeyStub).to.have.been.calledOnceWithExactly(
        'test_anonymousId',
      );
      expect(getSpotCheckAnonymousMeterKeyStub).to.have.been.calledOnceWithExactly(
        'test_anonymousId',
      );
      expect(res.send).to.have.been.calledOnceWithExactly({
        meterRemaining: 3,
      });
    });
    it('should increment an existing anonymous surf check key if its eligible', async () => {
      incrementSpotCheckAnonymousMeterKeyStub.resolves('3');
      getSpotCheckAnonymousMeterKeyStub.resolves(2);
      await incrementSpotCheckAnonymousMeterHandler(req as SurflineRequest, res as Response);
      expect(incrementSpotCheckAnonymousMeterKeyStub).to.have.been.calledOnceWithExactly(
        'test_anonymousId',
      );
      expect(getSpotCheckAnonymousMeterKeyStub).to.have.been.calledOnceWithExactly(
        'test_anonymousId',
      );
      expect(res.send).to.have.been.calledOnceWithExactly({
        meterRemaining: 1,
      });
    });
    it('should not increment an anonymous surf check key if there are no surf checks left', async () => {
      getSpotCheckAnonymousMeterKeyStub.resolves(4);
      await incrementSpotCheckAnonymousMeterHandler(req as SurflineRequest, res as Response);
      expect(incrementSpotCheckAnonymousMeterKeyStub).to.not.have.been.called();
      expect(getSpotCheckAnonymousMeterKeyStub).to.have.been.calledOnceWithExactly(
        'test_anonymousId',
      );
      expect(res.send).to.have.been.calledOnceWithExactly({
        message: 'There are no remaining surf checks for this anonymous session',
        meterRemaining: -1,
      });
    });
    it('should throw an error if required fields are not present', async () => {
      let err: Error;
      req.params = {
        anonymousId: null,
      };
      try {
        await incrementSpotCheckAnonymousMeterHandler(req as SurflineRequest, res as Response);
      } catch (error) {
        err = error;
        expect(error).to.exist();
        expect(error.name).to.be.equal('APIError');
        expect(error.message).to.be.equal(
          'Invalid Parameters: anonymousId is required in the url.',
        );
      }
      expect(err).to.exist();
    });
  });

  describe('verifySpotCheckAnonymousMeterHandler', () => {
    let getSpotCheckAnonymousMeterKeyStub: sinon.SinonStub;
    const sendStub = sinon.stub();
    const req: Partial<SurflineRequest> = {};
    req.params = {
      anonymousId: 'test_anonymousId',
    };
    const res: Partial<Response> = {
      send: sendStub,
    };
    beforeEach(() => {
      getSpotCheckAnonymousMeterKeyStub = ImportMock.mockFunction(
        redisExternal,
        'getSpotCheckAnonymousMeterKey',
      );
    });
    afterEach(() => {
      sendStub.resetHistory();
      getSpotCheckAnonymousMeterKeyStub.restore();
    });

    it('should return premiumEligible=true if an anonymous session has surf checks left', async () => {
      getSpotCheckAnonymousMeterKeyStub.resolves(1);
      await verifySpotCheckAnonymousMeterHandler(req as SurflineRequest, res as Response);
      expect(getSpotCheckAnonymousMeterKeyStub).to.have.been.calledOnceWithExactly(
        'test_anonymousId',
      );
      expect(res.send).to.have.been.calledOnceWithExactly({
        premiumEligible: true,
      });
    });
    it('should return premiumEligible=false if an anonymous session has no surf checks left', async () => {
      getSpotCheckAnonymousMeterKeyStub.resolves(null);
      await verifySpotCheckAnonymousMeterHandler(req as SurflineRequest, res as Response);
      expect(getSpotCheckAnonymousMeterKeyStub).to.have.been.calledOnceWithExactly(
        'test_anonymousId',
      );
      expect(res.send).to.have.been.calledOnceWithExactly({
        premiumEligible: false,
      });
    });
    it('should throw an error if required fields are not present', async () => {
      let err: Error;
      req.params = {
        anonymousId: null,
      };
      try {
        await verifySpotCheckAnonymousMeterHandler(req as SurflineRequest, res as Response);
      } catch (error) {
        err = error;
        expect(error).to.exist();
        expect(error.name).to.be.equal('APIError');
        expect(error.message).to.be.equal(
          'Invalid Parameters: anonymousId is required in the url.',
        );
      }
      expect(err).to.exist();
    });
  });
});
