import { Router } from 'express';
import { authenticateRequest, wrapErrors } from '@surfline/services-common';
import {
  getMeterHandler,
  hasActiveSurfCheck,
  postMeterHandler,
  surfCheckHandler,
  patchMeterHandler,
  getSpotCheckAnonymousMeterHandler,
  incrementSpotCheckAnonymousMeterHandler,
  verifySpotCheckAnonymousMeterHandler,
} from './meter';
import admin from './admin';

const meter = Router();

meter.use('/admin', admin);
meter.get('/', authenticateRequest({ userIdRequired: true }), wrapErrors(getMeterHandler));
meter.post('/', authenticateRequest({ userIdRequired: true }), wrapErrors(postMeterHandler));
meter.patch('/', authenticateRequest({ userIdRequired: true }), wrapErrors(patchMeterHandler));
meter.get(
  '/surf-check',
  authenticateRequest({ userIdRequired: true }),
  wrapErrors(hasActiveSurfCheck),
);
meter.patch(
  '/surf-check',
  authenticateRequest({ userIdRequired: true }),
  wrapErrors(surfCheckHandler),
);
meter.get('/anonymous/:anonymousId', wrapErrors(getSpotCheckAnonymousMeterHandler));
meter.put('/anonymous/increment/:anonymousId', wrapErrors(incrementSpotCheckAnonymousMeterHandler));
meter.get('/anonymous/verify/:anonymousId', wrapErrors(verifySpotCheckAnonymousMeterHandler));

export default meter;
