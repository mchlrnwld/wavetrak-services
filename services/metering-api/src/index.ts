/* istanbul ignore file */
import 'newrelic';
import mongoose from 'mongoose';
import {
  initMongo,
  initRedis,
  setupFeatureFramework,
  setupSurflineAnalytics,
} from '@surfline/services-common';
import config from './config';
import app from './server';
import log, { createLogger } from './common/logger';
import { createPromisifiedRedisInstance } from './external/redis';

const start = async (): Promise<void> => {
  try {
    await Promise.all([
      initRedis(config.REDIS_IP, config.REDIS_PORT, 'metering-service'),
      initMongo(mongoose, config.MONGO_CONNECTION_STRING, 'metering-service'),
      setupFeatureFramework(config.SPLIT_AUTHORIZATION_KEY, log),
      setupSurflineAnalytics(
        config.SL_SEGMENT_WRITE_KEY,
        createLogger('metering-service:sl-analytics'),
      ),
    ]);
    createPromisifiedRedisInstance();
    app();
  } catch (err) {
    log.error({
      message: `App Initialization failed: ${err.message}`,
      stack: err,
    });
  }
};

start();
