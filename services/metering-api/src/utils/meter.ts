import semver from 'semver';
import { getTreatmentWithConfig, ANDROID, IOS } from '@surfline/services-common';

const RATE_LIMIT_TREATMENT_NAME = 'sl_rate_limiting_user_v1';
const LOGGED_IN = 'logged_in';
const CONTROL = 'control';
const IOS_SUPPORTED_VERSION = '7.4.0';
const ANDROID_SUPPORTED_VERSION = '4.1.2';

export const OFF = 'off';
export const ON = 'on';

/**
 * @description Sets the meter limit for registered users
 */

export const METER_LIMIT = 3;

/**
 * @description Sets the initial meter remaining for registered users
 */

export const INITIAL_METER_REMAINING = 3;

/**
 * @description Sets the meter limit for anonymous users
 */
export const ANONYMOUS_METER_LIMIT = 3;

type Identity = {
  id: string;
  anonymousId: string;
  userId: string;
  isoCountryCode?: string;
  type?: string;
};
/**
 * @function getRateLimitingTreatmentWithConfig
 * @description Use the given user information to retrieve the split treatment information
 */
export const getRateLimitingTreatmentWithConfig = (
  userId: string,
  identity: Identity,
): {
  treatmentState: 'on' | 'off';
  meterLimit: number;
} => {
  const treatmentName = RATE_LIMIT_TREATMENT_NAME;
  const result = getTreatmentWithConfig(userId, treatmentName, {
    ...identity,
    type: LOGGED_IN,
  });
  const config = JSON.parse(result?.config);
  const meterLimit = config?.meterLimit;
  const treatmentVariant = result?.treatment;
  const treatmentState = treatmentVariant !== CONTROL && treatmentVariant !== OFF ? ON : OFF;
  return { meterLimit, treatmentState };
};

/**
 * @function checkIfABTestSupported
 * @description Check if the Test is supported in the current version
 */
export const checkIfABTestSupported = (
  platform: string,
  appVersion: string | string[],
): boolean => {
  if (platform === IOS) return semver.gte(appVersion, IOS_SUPPORTED_VERSION);
  if (platform === ANDROID) return semver.gte(appVersion, ANDROID_SUPPORTED_VERSION);
  return true;
};
