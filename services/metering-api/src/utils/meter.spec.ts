import { expect } from 'chai';
import sinon from 'sinon';
import * as servicesCommon from '@surfline/services-common';
import { getRateLimitingTreatmentWithConfig } from './meter';

describe('utils / meter', () => {
  describe('utils/ getRateLimitingTreatmentWithConfig', () => {
    let getTreatmentWithConfigStub: sinon.SinonStub;
    beforeEach(() => {
      getTreatmentWithConfigStub = sinon.stub(servicesCommon, 'getTreatmentWithConfig');
    });

    afterEach(() => {
      getTreatmentWithConfigStub.restore();
    });

    it('should return treatment "off" when getTreatment returns "control"', () => {
      getTreatmentWithConfigStub.returns({ treatment: 'control', config: null });
      const result = getRateLimitingTreatmentWithConfig('userId', {
        id: 'userId',
        anonymousId: 'anonymousId',
        userId: 'userId',
        isoCountryCode: 'US',
      });
      expect(result).to.deep.equal({
        treatmentState: 'off',
        meterLimit: undefined,
      });
    });

    it('should return treatment "off" when getTreatment returns "off"', () => {
      getTreatmentWithConfigStub.returns({ treatment: 'off', config: null });

      const result = getRateLimitingTreatmentWithConfig('userId', {
        id: 'userId',
        anonymousId: 'anonymousId',
        userId: 'userId',
        isoCountryCode: 'US',
      });
      expect(result).to.deep.equal({
        treatmentState: 'off',
        meterLimit: undefined,
      });
    });

    it('should return treatment "on" when getTreatment returns a valid treatment', () => {
      getTreatmentWithConfigStub.returns({
        treatment: 'surf_check_weekly_2',
        config: '{"meterLimit":5}',
      });

      const result = getRateLimitingTreatmentWithConfig('userId', {
        id: 'userId',
        anonymousId: 'anonymousId',
        userId: 'userId',
        isoCountryCode: 'US',
      });
      expect(result).to.deep.equal({
        treatmentState: 'on',
        meterLimit: 5,
      });
    });
  });
});
