import mongoose from 'mongoose';
import sinon from 'sinon';
import { expect } from 'chai';
import Meter, { getMeterByUser, createMeter, decrementMeter, TMeter as MeterType } from './Meter';
import meterFixtureGenerator from '../fixtures/meter';

describe('Meter model', () => {
  const userId = mongoose.Types.ObjectId().toString();
  const meterFixture = meterFixtureGenerator(userId);
  let currentTimeStub: sinon.SinonStub;
  const currentTimestamp = Date.now();

  beforeEach(() => {
    currentTimeStub = sinon.stub(Date, 'now').returns(currentTimestamp);
  });

  afterEach(() => {
    currentTimeStub.restore();
  });

  it('Should throw validation errors', async () => {
    const meter = new Meter();

    let error: Error;
    try {
      await meter.validate();
    } catch (err) {
      error = err;
      expect(err).to.exist();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors.user.message).to.equal('user is required');
      expect(err.errors.anonymousId.message).to.equal('anonymousId is required');
      expect(err.errors.meterLimit.message).to.equal('meterLimit is required');
      expect(err.errors.countryCode.message).to.equal('countryCode is required');
    }
    expect(error).to.exist();
  });

  it('should accept a valid Meter', async () => {
    const meter = new Meter({
      user: mongoose.Types.ObjectId(),
      anonymousId: 'anonymouseId',
      meterLimit: 1,
      entitlement: 'sl_metered',
      countryCode: 'CA',
    });

    await meter.validate();
    expect(meter.meterRemaining).to.be.equal(1);
    expect(meter.active).to.be.equal(true);
    expect(meter.latestReset).to.be.equal(Date.now());
    expect(meter.metersConsumed).to.be.deep.equal([]);
  });

  describe('getMeterByUser', () => {
    let findOneStub: sinon.SinonStub;

    beforeEach(() => {
      findOneStub = sinon.stub(Meter, 'findOne');
    });

    afterEach(() => {
      findOneStub.restore();
    });

    it('should return a metering document if it exists', async () => {
      findOneStub.returns({ lean: sinon.stub().returns({ exec: () => meterFixture }) });

      const result = await getMeterByUser(userId);

      expect(result).to.deep.equal(meterFixture);
      expect(findOneStub).to.have.been.calledOnceWithExactly({ user: userId, active: true });
    });

    it('should throw an error if no userId is passed', async () => {
      let err: Error;
      try {
        await getMeterByUser('');
      } catch (error) {
        err = error;
        expect(error.name).to.be.equal('APIError');
        expect(error.message).to.be.equal('Invalid Parameters: User ID is required.');
        expect(findOneStub).to.have.not.been.called();
      }
      expect(err).to.exist();
    });

    it('should throw an error if no meter is found', async () => {
      let err: Error;
      findOneStub.returns({ lean: sinon.stub().returns({ exec: () => null }) });
      try {
        await getMeterByUser(userId);
      } catch (error) {
        err = error;
        expect(error.name).to.be.equal('NotFound');
        expect(error.message).to.be.equal(`No metering document found for user: ${userId}`);
        expect(findOneStub).to.have.been.calledOnceWithExactly({ user: userId, active: true });
      }
      expect(err).to.exist();
    });
  });

  describe('createMeter', () => {
    let createStub: sinon.SinonStub;

    beforeEach(() => {
      createStub = sinon.stub(Meter, 'create');
    });

    afterEach(() => {
      createStub.restore();
    });

    it('should return a metering document if it exists', async () => {
      createStub.resolves(meterFixture);

      const result = await createMeter(meterFixture);

      expect(result).to.deep.equal(meterFixture);
      expect(createStub).to.have.been.calledOnceWithExactly(meterFixture);
    });
  });

  describe('decrementMeter', () => {
    let findOneAndUpdateStub: sinon.SinonStub;

    beforeEach(() => {
      findOneAndUpdateStub = sinon.stub(Meter, 'findOneAndUpdate');
    });

    afterEach(() => {
      findOneAndUpdateStub.restore();
    });

    it('should return a decremented metering document', async () => {
      findOneAndUpdateStub.returns({
        lean: () => ({ exec: (): MeterType => ({ ...meterFixture, meterRemaining: 0 }) }),
      });

      const result = await decrementMeter(userId);

      expect(result).to.deep.equal({ ...meterFixture, meterRemaining: 0 });
      expect(findOneAndUpdateStub).to.have.been.calledOnceWithExactly(
        { user: userId },
        { $inc: { meterRemaining: -1 } },
        { new: true },
      );
    });

    it('should throw an error if no userId is passed', async () => {
      let err: Error;
      try {
        await decrementMeter('');
      } catch (error) {
        err = error;
        expect(error.name).to.be.equal('APIError');
        expect(error.message).to.be.equal('Invalid Parameters: User ID is required.');
        expect(findOneAndUpdateStub).to.have.not.been.called();
      }
      expect(err).to.exist();
    });

    it('should throw an error if no meter is found', async () => {
      let err: Error;
      findOneAndUpdateStub.returns({
        lean: () => ({ exec: (): MeterType => null }),
      });
      try {
        await decrementMeter(userId);
      } catch (error) {
        err = error;
        expect(error.name).to.be.equal('NotFound');
        expect(error.message).to.be.equal(`No metering document found for user: ${userId}`);
        expect(findOneAndUpdateStub).to.have.been.calledOnceWithExactly(
          { user: userId },
          { $inc: { meterRemaining: -1 } },
          { new: true },
        );
      }
      expect(err).to.exist();
    });
  });
});
