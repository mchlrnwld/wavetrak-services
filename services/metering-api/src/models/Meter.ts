import mongoose, { Schema, Document, Model } from 'mongoose';
import { APIError, NotFound } from '@surfline/services-common';

export type TMeter = {
  _id?: mongoose.Types.ObjectId | string;
  user: mongoose.Types.ObjectId | string;
  anonymousId: string;
  meterLimit: number;
  meterRemaining?: number;
  entitlement: 'sl_metered';
  countryCode: string;
  active?: boolean;
  latestReset: number;
  metersConsumed?: Array<number>;
};

export type MeterDocument = Document & TMeter;

const meterSchema: Schema = new Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'UserId',
      required: [true, 'user is required'],
      unique: true,
    },
    anonymousId: { type: String, required: [true, 'anonymousId is required'], unique: true },
    meterLimit: { type: Number, required: [true, 'meterLimit is required'] },
    meterRemaining: {
      type: Number,
      default(): number {
        return this.meterLimit;
      },
      min: 0,
    },
    entitlement: {
      type: String,
      enum: ['sl_metered'],
      default: 'sl_metered',
    },
    countryCode: {
      type: String,
      required: [true, 'countryCode is required'],
    },
    active: {
      type: Boolean,
      default: true,
    },
    latestReset: {
      type: Number,
      default(): number {
        return Date.now();
      },
    },
    metersConsumed: {
      type: [Number],
      default: [],
    },
  },
  {
    timestamps: true,
    _id: true,
  },
);

meterSchema.index({ user: 1 }, { unique: true });
meterSchema.index({ anonymousId: 1 }, { unique: true });
meterSchema.index({ active: 1 });

const Meter: Model<MeterDocument> = mongoose.model<MeterDocument>('Meter', meterSchema, 'Meters');

export const getMeterByUser = (userId: string): Promise<MeterDocument> => {
  if (!userId) {
    throw new APIError('User ID is required.');
  }

  const meter = Meter.findOne({ user: userId, active: true }).lean<MeterDocument>().exec();

  if (!meter) throw new NotFound(`No metering document found for user: ${userId}`);

  return meter;
};

export const createMeter = (meter: TMeter): Promise<MeterDocument> => Meter.create(meter);

export const decrementMeter = (userId: string): Promise<MeterDocument> => {
  if (!userId) {
    throw new APIError('User ID is required.');
  }

  const meter = Meter.findOneAndUpdate(
    { user: userId },
    { $inc: { meterRemaining: -1 } },
    { new: true },
  )
    .lean<MeterDocument>()
    .exec();

  if (!meter) {
    throw new NotFound(`No metering document found for user: ${userId}`);
  }

  return meter;
};

export const checkIfUserExists = async (user: string): Promise<boolean> => Meter.exists({ user });

export default Meter;
