import { expect } from 'chai';
import newrelic from 'newrelic';
import mongoose from 'mongoose';
import sinon from 'sinon';
import axios from 'axios';

import { getUser, setUserTimezone } from './user';

describe('external / user', () => {
  let newrelicNoticeStub: sinon.SinonStub;

  beforeEach(() => {
    newrelicNoticeStub = sinon.stub(newrelic, 'noticeError');
  });

  afterEach(() => {
    newrelicNoticeStub.restore();
  });

  describe('getUser', () => {
    const userId = mongoose.Types.ObjectId().toString();
    let axiosGetStub: sinon.SinonStub;
    beforeEach(() => {
      axiosGetStub = sinon.stub(axios, 'get');
    });
    afterEach(() => {
      axiosGetStub.restore();
    });

    it('should return a user', async () => {
      const res = {
        data: {
          createdAt: '2020-04-27T21:59:51.558Z',
          _id: userId,
          isEmailVerified: true,
        },
      };
      axiosGetStub.resolves(res);

      const response = await getUser(userId);

      expect(response).to.deep.equal(res.data);
      expect(axiosGetStub).to.have.been.calledOnceWithExactly(
        `user-service.test/user?id=${userId}`,
      );
    });

    it('should thow a server error if the user Id is not passed', async () => {
      let error: Error;
      try {
        await getUser('');
      } catch (err) {
        error = err;
        expect(err.name).to.equal('ServerError');
        expect(err.message).to.equal('User ID is required to make a request to the user-service.');
        expect(axiosGetStub).to.not.have.been.called();
      }
      expect(error).to.exist();
    });

    it('should handle an api error response', async () => {
      let error: Error;
      axiosGetStub.throws({ message: 'test message' });
      try {
        await getUser(userId);
      } catch (err) {
        error = err;
        expect(err.name).to.equal('ServerError');
        expect(err.message).to.equal('Something went wrong fetching the user.');
        expect(newrelicNoticeStub).to.have.been.calledOnceWithExactly('test message');
      }
      expect(error).to.exist();
    });
  });

  describe('setUserTimezone', () => {
    const userId = mongoose.Types.ObjectId().toString();
    let axiosPatchStub: sinon.SinonStub;
    beforeEach(() => {
      axiosPatchStub = sinon.stub(axios, 'patch');
    });
    afterEach(() => {
      axiosPatchStub.restore();
    });

    it("should update the user's timezone", async () => {
      const res = {
        data: {
          message: 'User updated successfully',
        },
      };
      axiosPatchStub.resolves(res);

      const response = await setUserTimezone(userId, 'America/Los_Angeles');

      expect(response).to.deep.equal(res.data);
      expect(axiosPatchStub).to.have.been.calledOnceWithExactly(
        'user-service.test/user',
        { timezone: 'America/Los_Angeles' },
        { headers: { 'x-auth-userid': userId } },
      );
    });

    it('should handle an api error response', async () => {
      let error: Error;
      axiosPatchStub.throws({ message: 'test message' });
      try {
        await setUserTimezone(userId, 'America/Los_Angeles');
      } catch (err) {
        error = err;
        expect(err.name).to.equal('ServerError');
        expect(err.message).to.equal("Something went wrong setting the user's timezone");
        expect(newrelicNoticeStub).to.have.been.calledOnceWithExactly({ message: 'test message' });
      }
      expect(error).to.exist();
    });
  });
});
