import { expect } from 'chai';
import sinon from 'sinon';
import axios from 'axios';

import { getUserEntitlements } from './entitlement';

describe('external / entitlement', () => {
  describe('getUserEntitlements', () => {
    const userId = '60b91dd4c515fdea7d566152';
    let axiosGetStub: sinon.SinonStub;
    beforeEach(() => {
      axiosGetStub = sinon.stub(axios, 'get');
    });
    afterEach(() => {
      axiosGetStub.restore();
    });

    it('should return a users entitlement', async () => {
      const res = {
        data: {
          entitlements: ['sl_premium'],
          promotions: ['bw_free_trial', 'fs_free_trial'],
          hasNewAccount: true,
        },
      };
      axiosGetStub.resolves(res);

      const response = await getUserEntitlements(userId);

      expect(response).to.deep.equal(res.data);
      expect(axiosGetStub).to.have.been.calledOnceWithExactly(
        `entitlements-service.test/entitlements`,
        {
          headers: { 'x-auth-userid': '60b91dd4c515fdea7d566152' },
        },
      );
    });
  });
});
