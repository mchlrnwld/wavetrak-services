import axios from 'axios';
import newrelic from 'newrelic';
import { ServerError } from '@surfline/services-common';
import config from '../config';

interface GetUserEntitlementsResponse {
  entitlements: string[];
}

export const getUserEntitlements = async (userId: string): Promise<GetUserEntitlementsResponse> => {
  if (!userId) {
    throw new ServerError({
      message: 'User ID is required to make a request to the entitlements-service.',
    });
  }
  const url = `${config.ENTITLEMENTS_SERVICE}/entitlements`;

  try {
    const response = await axios.get(url, {
      headers: {
        'x-auth-userid': userId,
      },
    });

    return response.data;
  } catch (err) {
    newrelic.noticeError(err.message);
    throw new ServerError({ message: 'Something went wrong fetching the entitlements.' });
  }
};
