import axios from 'axios';
import newrelic from 'newrelic';
import { ServerError } from '@surfline/services-common';
import config from '../config';

interface GetUserResponse {
  createdAt?: string;
  _id: string;
  isEmailVerified: boolean;
  timezone?: string;
  locale?: string;
}

export const getUser = async (userId: string): Promise<GetUserResponse> => {
  if (!userId) {
    throw new ServerError({
      message: 'User ID is required to make a request to the user-service.',
    });
  }
  const url = `${config.USER_SERVICE}/user?id=${userId}`;

  try {
    const response = await axios.get<GetUserResponse>(url);

    return response.data;
  } catch (err) {
    newrelic.noticeError(err.message);
    throw new ServerError({ message: 'Something went wrong fetching the user.' });
  }
};

export const setUserTimezone = async (
  userId: string,
  timezone: string,
): Promise<{ message: string }> => {
  const url = `${config.USER_SERVICE}/user`;

  try {
    const response = await axios.patch<{ message: string }>(
      url,
      {
        timezone,
      },
      {
        headers: {
          'x-auth-userid': userId,
        },
      },
    );

    return response.data;
  } catch (error) {
    newrelic.noticeError(error);
    throw new ServerError({ message: "Something went wrong setting the user's timezone" });
  }
};
