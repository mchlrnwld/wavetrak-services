import { getPromisifiedRedis, PromisifiedRedis } from '@surfline/services-common';

let redis: PromisifiedRedis;

export const createPromisifiedRedisInstance = (): void => {
  redis = getPromisifiedRedis();
};

const SURF_CHECK_TTL = 1800; // 30 minutes in seconds
const ANONYMOUS_SPOT_CHECK_TTL = 2592000; // 30 days in seconds

export const getMeterKey = (userId: string): string => `meter:surf-check:${userId}`;

export const getAnonymousMeterKey = (anonymousId: string): string =>
  `meter:anonymous:spot-check:${anonymousId}`;

/**
 * @description
 * Retrieves the TTL of a surf check meter. As per the Redis documentation this command
 * will return a negative number if the key does not exist or does not have an expiration.
 * If this function returns a negative number, we can assume that the user does not have
 * an active surf check
 * https://redis.io/commands/ttl
 */
export const getSurfCheckMeterTTL = (userId: string): Promise<number> =>
  redis.ttl(getMeterKey(userId));

/**
 * @description
 * Resets the TTL of the surf-check key back to it's default. This function
 * is used to extend a surf-check's active state when a user performs a valid
 * activity
 */
export const resetSurfCheckMeterTTL = (userId: string): Promise<number> =>
  redis.expire(getMeterKey(userId), SURF_CHECK_TTL);

/**
 * @description
 * When a user starts an entirely new surf check, we need to create a key
 * in redis and then set it to expire after 30 minutes of inactivity.
 */
export const createSurfCheckMeterKey = async (userId: string): Promise<number> => {
  await redis.set(getMeterKey(userId), '1');
  return resetSurfCheckMeterTTL(userId);
};

/**
 * @description
 * Retrieves the value assosciated to a anonymous meter.
 * The value denotes the number of spot checks used by the anonymous session.
 */
export const getSpotCheckAnonymousMeterKey = async (anonymousId: string): Promise<string> =>
  redis.get(getAnonymousMeterKey(anonymousId));

/**
 * @description
 * Increments the anonymous spot check key by 1. Denotes the number of spot checks the anonymous user has used up so far.
 */
export const incrementSpotCheckAnonymousMeterKey = async (anonymousId: string): Promise<number> => {
  const meterKey = getAnonymousMeterKey(anonymousId);
  const count = await redis.inc(meterKey);
  if (count <= 1) redis.expire(meterKey, ANONYMOUS_SPOT_CHECK_TTL);
  return count;
};
