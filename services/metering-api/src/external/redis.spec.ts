import { expect } from 'chai';
import sinon from 'sinon';
import { ImportMock } from 'ts-mock-imports';
import * as servicesCommon from '@surfline/services-common';
import {
  createPromisifiedRedisInstance,
  getSurfCheckMeterTTL,
  resetSurfCheckMeterTTL,
  createSurfCheckMeterKey,
  getSpotCheckAnonymousMeterKey,
  incrementSpotCheckAnonymousMeterKey,
} from './redis';

describe('external / redis', () => {
  const getPromisifiedRedisStub = ImportMock.mockFunction(servicesCommon, 'getPromisifiedRedis');
  const ttlStub = sinon.stub();
  const expireStub = sinon.stub();
  const setStub = sinon.stub();
  const getStub = sinon.stub();
  const decrStub = sinon.stub();
  const existsStub = sinon.stub();
  const incStub = sinon.stub();

  getPromisifiedRedisStub.returns({
    ttl: ttlStub,
    expire: expireStub,
    set: setStub,
    get: getStub,
    decr: decrStub,
    exists: existsStub,
    inc: incStub,
  });

  before(() => {
    createPromisifiedRedisInstance();
  });

  afterEach(() => {
    ttlStub.resetHistory();
    expireStub.resetHistory();
    setStub.resetHistory();
    getStub.resetHistory();
    decrStub.resetHistory();
    existsStub.resetHistory();
    incStub.resetHistory();
  });

  describe('getSurfCheckMeterTTL', () => {
    it('should return the TTL for the correct key', async () => {
      ttlStub.resolves(1);
      const result = await getSurfCheckMeterTTL('123');
      expect(result).to.be.equal(1);
      expect(ttlStub).to.be.have.been.calledOnceWithExactly('meter:surf-check:123');
    });
  });

  describe('resetSurfCheckMeterTTL', async () => {
    it('should reset the TTL for the correct key', async () => {
      expireStub.resolves(1);
      const result = await resetSurfCheckMeterTTL('123');
      expect(result).to.be.equal(1);
      expect(expireStub).to.be.have.been.calledOnceWithExactly('meter:surf-check:123', 1800);
    });
  });

  describe('createSurfCheckMeterKey', async () => {
    it('should create the correct key', async () => {
      ttlStub.resolves(1);
      setStub.resolves('1');
      const result = await createSurfCheckMeterKey('123');
      expect(result).to.be.equal(1);
      expect(setStub).to.be.have.been.calledOnceWithExactly('meter:surf-check:123', '1');
      expect(expireStub).to.be.have.been.calledOnceWithExactly('meter:surf-check:123', 1800);
    });
  });

  describe('getSpotCheckAnonymousMeterKey', () => {
    it('should return the value for the correct key', async () => {
      getStub.resolves('1');
      const result = await getSpotCheckAnonymousMeterKey('test_anonymousId');
      expect(result).to.be.equal('1');
      expect(getStub).to.be.have.been.calledOnceWithExactly(
        'meter:anonymous:spot-check:test_anonymousId',
      );
    });
  });

  describe('incrementSpotCheckAnonymousMeterKey', async () => {
    it('should increment a spot check and set the expiry in redis for a new anonymous session', async () => {
      expireStub.resolves(1);
      incStub.resolves(1);
      const anonymousId = 'test_anonymousId';
      const result = await incrementSpotCheckAnonymousMeterKey(anonymousId);
      expect(result).to.be.equal(1);
      expect(incStub).to.be.have.been.calledOnceWithExactly(
        'meter:anonymous:spot-check:test_anonymousId',
      );
      expect(expireStub).to.be.have.been.calledOnceWithExactly(
        'meter:anonymous:spot-check:test_anonymousId',
        2592000,
      );
    });
    it('should increment a spot check in redis for anonymous session', async () => {
      incStub.resolves(2);
      const anonymousId = 'test_anonymousId';
      const result = await incrementSpotCheckAnonymousMeterKey(anonymousId);
      expect(result).to.be.equal(2);
      expect(incStub).to.be.have.been.calledOnceWithExactly(
        'meter:anonymous:spot-check:test_anonymousId',
      );
      expect(expireStub).to.not.have.been.called();
    });
  });
});
