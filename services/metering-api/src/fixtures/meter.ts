import mongoose from 'mongoose';
import type { TMeter } from '../models/Meter';

export default (
  userId?: string | mongoose.Types.ObjectId,
  meterLimit?: number,
  meterRemaining?: number,
): TMeter => ({
  _id: mongoose.Types.ObjectId().toString(),
  user: userId || mongoose.Types.ObjectId().toString(),
  anonymousId: 'anonymousId',
  meterLimit: meterLimit ?? 3,
  meterRemaining: meterRemaining ?? 3,
  entitlement: 'sl_metered',
  countryCode: 'US',
  active: true,
  latestReset: Date.now(),
  metersConsumed: [],
});
