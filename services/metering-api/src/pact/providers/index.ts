/* eslint-disable import/no-extraneous-dependencies */
import { Pact } from '@pact-foundation/pact';

export const setupPactServers = (): Promise<Pact[]> => Promise.all([]);

export const teardownPactServers = (providers: Pact[]): Promise<void[]> =>
  Promise.all(
    providers.map(async ({ verify, finalize }) => {
      await verify();
      await finalize();
    }),
  );
