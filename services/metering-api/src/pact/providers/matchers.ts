/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable import/no-extraneous-dependencies */
import { Matchers } from '@pact-foundation/pact';

const { term } = Matchers;

export const applicationJSON = term({
  matcher: 'application/json',
  generate: 'application/json',
});

export const dateTimeISO = (generate: string) =>
  term({
    matcher: '^\\d{4}\\-\\d{2}\\-\\d{2}T\\d{2}\\:\\d{2}\\:\\d{2}\\.\\d{3}Z$',
    generate,
  });

export const objectId = (generate: string) =>
  term({
    matcher: '^[a-zA-Z0-9]{24}$',
    generate,
  });

export const numberRange = (generate) =>
  term({
    matcher: '^\\d+\\-\\d+$',
    generate,
  });

export const gitSha = (generate: string) =>
  term({
    matcher: '^([0-9a-f]{5}|[0-9a-f]{40}|unknown)$',
    generate,
  });
