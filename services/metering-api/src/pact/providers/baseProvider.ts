/* eslint-disable import/no-extraneous-dependencies */
import path from 'path';
import { Pact } from '@pact-foundation/pact';

const baseProvider = (options: { provider: string; port: number }): Pact =>
  new Pact({
    dir: path.resolve(process.cwd(), 'pacts'),
    spec: 2,
    consumer: 'metering-service',
    ...options,
  });

export default baseProvider;
