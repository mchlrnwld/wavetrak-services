import chai from 'chai';
import 'chai-http';
import express from 'express';
import { json } from 'body-parser';

const mockExpressRequests = (setupMiddleware: (app: express.Express) => void): ChaiHttp.Agent => {
  const app = express();
  app.use(json());
  setupMiddleware(app);

  const request = chai.request(app).keepOpen();
  return request;
};

export default mockExpressRequests;
