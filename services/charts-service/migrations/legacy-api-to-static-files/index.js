import fs from 'fs';
import {
  fetchLegacyChartImageData,
  fetchLegacyNearshoreCharts,
  fetchLegacyHiResCharts,
} from './utils/external';
import { formatChartImageData, formatChartTypeData, formatNearshoreMap } from './utils/formatData';
import { loadLegacyChartArray, loadCurrentData } from './utils/loadData';
import wait from './utils/wait';

const main = async () => {
  const [legacyChartDataArray, currentChartImages, currentChartTypes] = await Promise.all([
    loadLegacyChartArray('./data/chartDataArray.json'),
    loadCurrentData('./data/chartImages.json'),
    loadCurrentData('./data/chartTypes.json'),
  ]);
  console.log('Charts to add:', legacyChartDataArray.length);
  console.log('Current chart images count:', Object.entries(currentChartImages).length);
  console.log('Current chart types count:', Object.entries(currentChartTypes).length);

  const imageResults = [];
  const chartTypesResults = [];

  for (const subregion of legacyChartDataArray) {
    // fetch legacy images
    if (
      !currentChartImages[subregion.id] &&
      !imageResults.some((result) => result.id === subregion.id)
    ) {
      await wait();
      const chartImageData = await fetchLegacyChartImageData(subregion.id);
      imageResults.push(chartImageData);
    }

    // fetch legacy nearshore
    await wait();
    const nearshoreData = await fetchLegacyNearshoreCharts(subregion.id);
    const nearshoreMap = formatNearshoreMap(nearshoreData, subregion.id);
    subregion.charts.nearshore = nearshoreMap;

    // fetch legacy hiRes
    if (subregion.charts && subregion.charts.wrf) {
      for (const wrfChart of subregion.charts.wrf) {
        await wait();
        const wrfData = await fetchLegacyHiResCharts(wrfChart.id);
        const wrfTitle = wrfData.articles[wrfChart.id].Title || null;
        if (wrfTitle) wrfChart.name = wrfTitle;
      }
    }
    chartTypesResults.push({
      ...subregion,
      charts: subregion.charts,
    });
  }

  // write ChartImages file
  const imageResultData = formatChartImageData(imageResults);
  const appendedImageData = { ...currentChartImages, ...imageResultData };
  const jsonImageData = JSON.stringify(appendedImageData, null, 2);
  fs.writeFileSync('./data/chartImages.json', jsonImageData);

  // write ChartTypes file
  const chartTypeData = formatChartTypeData(chartTypesResults);
  const appendedChartTypes = { ...currentChartTypes, ...chartTypeData };
  const jsonTypesData = JSON.stringify(appendedChartTypes, null, 2);
  fs.writeFileSync('./data/chartTypes.json', jsonTypesData);

  console.log('main: done');
};

main()
  .then(() => {
    console.log('Done!');
    process.exit(0);
  })
  .catch((err) => {
    console.error(err);
    process.exit(-1);
  });
