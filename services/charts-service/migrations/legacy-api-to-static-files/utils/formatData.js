export const formatChartImageData = (imageResults) =>
  imageResults.reduce((accum, subregion) => {
    for (const key in subregion.Chart) {
      if (subregion.Chart.hasOwnProperty(key)) {
        subregion.Chart[key] = {
          baseUrl: subregion.Chart[key].base2,
          increment: subregion.Chart[key].increment,
          start: subregion.Chart[key].start,
          end: subregion.Chart[key].end,
        };
      }
    }
    return {
      ...accum,
      [subregion.id]: {
        timezoneString: subregion.timeZoneString,
        charts: subregion.Chart,
      },
    };
  }, {});

export const formatChartTypeData = (chartTypesResults) =>
  chartTypesResults.reduce(
    (accum, subregion) => ({
      ...accum,
      [subregion.subregionId]: { ...subregion },
    }),
    {},
  );

export const formatNearshoreMap = (nearshoreData, subregionId) => {
  let nearshoreMap = [];
  const nearshoreList = nearshoreData.articles[subregionId]['Pretty Nearshore Model List'] || null;
  if (nearshoreList) {
    const nearshoreArray = nearshoreList.split(/\r?\n|\r/g);
    nearshoreMap = nearshoreArray.map((nearshore) => {
      const splitArray = nearshore.split('|');
      return { name: splitArray[0], id: splitArray[1] };
    });
  }
  return nearshoreMap;
};
