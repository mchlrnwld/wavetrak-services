import fetch from 'node-fetch';

const { LEGACY_API } = process.env;

export const fetchLegacyChartImageData = async (legacyId) => {
  console.log('fetching image data for: ', legacyId);
  const url = `${LEGACY_API}/forecasts/${legacyId}?resources=charts`;
  const response = await fetch(url);
  const body = await response.json();
  return body;
};

export const fetchLegacyNearshoreCharts = async (legacyId) => {
  console.log('fetching nearshore data for: ', legacyId);
  const url = `${LEGACY_API}/cmsarticles/${legacyId}?fields=Pretty%20Nearshore%20Model%20List`;
  const response = await fetch(url);
  const body = await response.json();
  return body;
};

export const fetchLegacyHiResCharts = async (legacyId) => {
  console.log('fetching hiRes data for: ', legacyId);
  const url = `${LEGACY_API}/cmsarticles/${legacyId}?fields=Title`;
  const response = await fetch(url);
  const body = await response.json();
  return body;
};
