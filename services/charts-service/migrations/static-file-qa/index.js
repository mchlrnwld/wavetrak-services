import fs from 'fs';
import { isEqual as _isEqual } from 'lodash';
import { fetchChartImages, fetchChartTypes } from './utils/external';
import { loadLegacyChartArray, loadCurrentData } from './utils/loadData';
import wait from './utils/wait';

const {
  CHARTS_SERVICE_API_PROD,
  CHARTS_SERVICE_API_STAGING,
  ACCESS_TOKEN_PROD,
  ACCESS_TOKEN_STAGING,
} = process.env;

const main = async () => {
  const [
    legacyChartDataArray,
    currentChartImagesStaging,
    currentChartImagesProd,
    currentChartImagesMatch,
    currentChartImagesMismatch,
    currentChartTypesStaging,
    currentChartTypesProd,
    currentChartTypesMatch,
    currentChartTypesMismatch,
    currentChartErrors,
  ] = await Promise.all([
    loadLegacyChartArray('./data/chartDataArray.json'),
    loadCurrentData('./data/chartImagesStaging.json'),
    loadCurrentData('./data/chartImagesProd.json'),
    loadCurrentData('./data/chartImagesMatch.json'),
    loadCurrentData('./data/chartImagesMismatch.json'),
    loadCurrentData('./data/chartTypesStaging.json'),
    loadCurrentData('./data/chartTypesProd.json'),
    loadCurrentData('./data/chartTypesMatch.json'),
    loadCurrentData('./data/chartTypesMismatch.json'),
    loadCurrentData('./data/chartErrors.json'),
  ]);
  console.log('Charts to add:', legacyChartDataArray.length);
  console.log('Current staging chart images count:', currentChartImagesStaging.length);
  console.log('Current prod chart images count:', currentChartImagesProd.length);
  console.log('Current staging chart types count:', currentChartTypesStaging.length);
  console.log('Current prod chart types count:', currentChartTypesProd.length);
  console.log('Current types match count:', currentChartTypesMatch.length);
  console.log('Current types mismatch count:', currentChartTypesMismatch.length);
  console.log('Current images match count:', currentChartImagesMatch.length);
  console.log('Current images mismatch count:', currentChartImagesMismatch.length);
  console.log('Current error count:', currentChartErrors.length);

  const chartImagesStaging = [];
  const chartImagesProd = [];
  const chartImagesMatches = [];
  const chartImagesMismatches = [];
  const chartTypesStaging = [];
  const chartTypesProd = [];
  const chartTypesMatches = [];
  const chartTypesMismatches = [];
  const chartErrors = [];

  for (const currSubregion of legacyChartDataArray) {
    // fetch staging types
    try {
      const stagingTypesResponse = await fetchChartTypes(
        CHARTS_SERVICE_API_STAGING,
        currSubregion.subregionId,
      );
      chartTypesStaging.push({ [currSubregion.subregionId]: stagingTypesResponse });

      // fetch prod types
      await wait();
      const prodTypesResponse = await fetchChartTypes(
        CHARTS_SERVICE_API_PROD,
        currSubregion.subregionId,
      );
      chartTypesProd.push({ [currSubregion.subregionId]: prodTypesResponse });

      // compare env results
      const typesAreEqual = _isEqual(stagingTypesResponse, prodTypesResponse);
      if (typesAreEqual) {
        console.log(`***Chart types for ${currSubregion.subregionId} are equal`);
        chartTypesMatches.push(currSubregion.subregionId);
      } else {
        console.log(`*********Chart types for ${currSubregion.subregionId} are NOT equal`);
        chartTypesMismatches.push(currSubregion.subregionId);
      }

      // iterate subregion types and fetch images or each subregion type
      const currentStagingSubregionImages = [];
      const currentProdSubregionImages = [];
      const currentSubregionImagesMatches = [];
      const currentSubregionImagesMismatches = [];
      if (prodTypesResponse && prodTypesResponse.data) {
        const { nearshore, subregion, region, basins, global } = prodTypesResponse.data.types;
        const basinsArr = basins && basins.length ? basins : [];
        const flattenedTypes = [...nearshore, ...subregion, ...region, ...basinsArr, ...global];
        for (const type of flattenedTypes) {
          // fetch staging images
          await wait();
          const stagingImagesResponse = await fetchChartImages(
            CHARTS_SERVICE_API_STAGING,
            currSubregion.subregionId,
            currSubregion.id,
            type.type,
            type.name,
            ACCESS_TOKEN_STAGING,
          );
          currentStagingSubregionImages.push({ [type.type]: stagingImagesResponse });

          // fetch prod images
          await wait();
          const prodImagesResponse = await fetchChartImages(
            CHARTS_SERVICE_API_PROD,
            currSubregion.subregionId,
            currSubregion.id,
            type.type,
            type.name,
            ACCESS_TOKEN_PROD,
          );
          currentProdSubregionImages.push({ [type.type]: prodImagesResponse });

          // compare env results
          const imagesAreEqual = _isEqual(stagingImagesResponse, prodImagesResponse);
          if (imagesAreEqual) {
            console.log(`***${type.type} images for ${currSubregion.subregionId} are equal`);
            currentSubregionImagesMatches.push(type.type);
          } else {
            console.log(
              `*********${type.type} images for ${currSubregion.subregionId} are NOT equal`,
            );
            currentSubregionImagesMismatches.push(type.type);
          }
        }
        chartImagesStaging.push({ [currSubregion.subregionId]: currentStagingSubregionImages });
        chartImagesProd.push({ [currSubregion.subregionId]: currentProdSubregionImages });
        if (currentSubregionImagesMatches.length) {
          chartImagesMatches.push({ [currSubregion.subregionId]: currentSubregionImagesMatches });
        }
        if (currentSubregionImagesMismatches.length) {
          chartImagesMismatches.push({
            [currSubregion.subregionId]: currentSubregionImagesMismatches,
          });
        }
      }
    } catch (err) {
      console.log('*********err', err);
      chartErrors.push({ [currSubregion.subregionId]: err });
    }
  }

  // write staging images file
  const appendedImageDataStaging = [...currentChartImagesStaging, ...chartImagesStaging];
  const jsonImagesDataStaging = JSON.stringify(appendedImageDataStaging, null, 2);
  fs.writeFileSync('./data/chartImagesStaging.json', jsonImagesDataStaging);

  // write prod images file
  const appendedImageDataProd = [...currentChartImagesProd, ...chartImagesProd];
  const jsonImagesDataProd = JSON.stringify(appendedImageDataProd, null, 2);
  fs.writeFileSync('./data/chartImagesProd.json', jsonImagesDataProd);

  // write images match file
  const appendedImagesMatches = [...currentChartImagesMatch, ...chartImagesMatches];
  const jsonImagesMatches = JSON.stringify(appendedImagesMatches, null, 2);
  fs.writeFileSync('./data/chartImagesMatch.json', jsonImagesMatches);

  // write images mismatch file
  const appendedImagesMismatches = [...currentChartImagesMismatch, ...chartImagesMismatches];
  const jsonImagesMismatches = JSON.stringify(appendedImagesMismatches, null, 2);
  fs.writeFileSync('./data/chartImagesMismatch.json', jsonImagesMismatches);

  // write staging types file
  const appendedTypesDataStaging = [...currentChartTypesStaging, ...chartTypesStaging];
  const jsonTypesDataStaging = JSON.stringify(appendedTypesDataStaging, null, 2);
  fs.writeFileSync('./data/chartTypesStaging.json', jsonTypesDataStaging);

  // write prod types file
  const appendedTypesDataProd = [...currentChartTypesProd, ...chartTypesProd];
  const jsonTypesDataProd = JSON.stringify(appendedTypesDataProd, null, 2);
  fs.writeFileSync('./data/chartTypesProd.json', jsonTypesDataProd);

  // write types match file
  const appendedTypesMatches = [...currentChartTypesMatch, ...chartTypesMatches];
  const jsonTypesMatches = JSON.stringify(appendedTypesMatches, null, 2);
  fs.writeFileSync('./data/chartTypesMatch.json', jsonTypesMatches);

  // write types mismatch file
  const appendedTypesMismatches = [...currentChartTypesMismatch, ...chartTypesMismatches];
  const jsonTypesMismatches = JSON.stringify(appendedTypesMismatches, null, 2);
  fs.writeFileSync('./data/chartTypesMismatch.json', jsonTypesMismatches);

  // write errors file
  const appendedErrors = [...currentChartErrors, ...chartErrors];
  const jsonErrors = JSON.stringify(appendedErrors, null, 2);
  fs.writeFileSync('./data/chartErrors.json', jsonErrors);

  console.log('main: done');
};

main()
  .then(() => {
    console.log('Done!');
    process.exit(0);
  })
  .catch((err) => {
    console.error(err);
    process.exit(-1);
  });
