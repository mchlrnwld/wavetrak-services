const { WAIT_TIME } = process.env;

const waitTime = Number.parseInt(WAIT_TIME, 10) || 500;
const wait = () => new Promise((resolve) => setTimeout(resolve, waitTime));

export default wait;
