import fetch from 'node-fetch';

const createParamString = (params) =>
  Object.keys(params)
    .filter((key) => params[key] !== undefined)
    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join('&');

export const fetchChartTypes = async (servicesUrl, subregionId) => {
  try {
    console.log('*fetching types for:', subregionId);
    const url = `${servicesUrl}/types?${createParamString({ subregionId })}`;
    const response = await fetch(url);
    const body = await response.json();
    return body;
  } catch (err) {
    throw err;
  }
};

export const fetchChartImages = async (
  servicesUrl,
  subregionId,
  legacyId,
  type,
  nearshoreModelName,
  accesstoken,
) => {
  try {
    console.log(`fetching ${subregionId} images type ${type}`);
    const url = `${servicesUrl}/images?${createParamString({
      legacyId,
      type,
      nearshoreModelName,
      accesstoken,
    })}`;
    const response = await fetch(url);
    const body = await response.json();
    return body;
  } catch (err) {
    throw err;
  }
};
