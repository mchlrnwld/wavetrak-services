import fs from 'fs';

const { BATCH_START, BATCH_SIZE } = process.env;

const readFile = (path) =>
  new Promise((resolve, reject) =>
    fs.readFile(path, (err, data) => {
      if (err) return reject(err);
      return resolve(data);
    }),
  );

export const loadLegacyChartArray = async (filePath) => {
  const jsonChartData = await readFile(filePath);
  const data = JSON.parse(jsonChartData);
  const start = Number.parseInt(BATCH_START, 10);
  const size = Number.parseInt(BATCH_SIZE, 10);
  const batch = data.slice(start, start + size);
  return batch;
};

export const loadCurrentData = async (filePath) => {
  const jsonData = await readFile(filePath);
  const data = await JSON.parse(jsonData);
  return data;
};
