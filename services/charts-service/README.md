# Charts Microservice

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Setup](#setup)
- [Development](#development)
- [Deployment](#deployment)
- [Endpoints](#endpoints)
  - [Legacy](#legacy)
    - [Images](#images)
    - [Sample Request](#sample-request)
    - [Sample Response](#sample-response)
    - [Sample Nearshore Type Request](#sample-nearshore-type-request)
    - [Sample Response](#sample-response-1)
- [Service Validation](#service-validation)
- [Fixing broken subregion chart pages](#fixing-broken-subregion-chart-pages)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Setup

Use Node v8

```sh
cp .env.sample .env
npm install
```

## Development

```sh
npm run startlocal
```

## Deployment

To deploy this application, run the Services Pipeline in Github in the desired environment (e.g. prod, sandbox or staging).

Use the following input parameters:

- Use workflow from: master
- The name of the function to be deployed: charts-service
- The environment where to deploy: sandbox | staging | prod
- The branch name, tag, or commit sha to deploy.

## Endpoints

### Legacy

#### Images

Returns an array of legacy chart image urls and timestamps.

| Parameter           | Type   | Description              | Example                  |
| ------------------- | ------ | ------------------------ | ------------------------ |
| legacyId            | string | Legacy SubregionId       | 2143                     |
| type                | string | Chart type               | localswell               |
| nearshoreModelName  | string | Name of nearshore model  | Seal Beach to HB cliffs  |

#### Sample Request

```
HTTP/1.1 GET /legacy/images?legacyId=2143&type=localswell
```

#### Sample Response

```JSON
{
  "data": {
    "images": [
      {
        "url": "http://slcharts01.cdn-surfline.com/charts/socal/norange/nearshore/norange_large_1.png",
        "timestamp": 1529496000
      },
      {
        "url": "http://slcharts01.cdn-surfline.com/charts/socal/norange/nearshore/norange_large_2.png",
        "timestamp": 1529517600
      },
      ...
      {
        "url": "http://slcharts01.cdn-surfline.com/charts/socal/norange/nearshore/norange_large_30.png",
        "timestamp": 1530122400
      }
    ]
  }
}
```

#### Sample Nearshore Type Request
For `type: nearshore` requests, param `nearshoreModelName` is required. `timestamp` will be returned as `null`.

```
HTTP/1.1 GET /legacy/images?legacyId=2143&type=nearshore&nearshoreModelName=Seal%20Beach%20to%20HB%20Cliffs
```

#### Sample Response

```JSON
{
  "data": {
    "images": [
      {
        "url": "https://charts.cdn-surfline.com/nearshores/sealb_01.png",
        "timestamp": null
      },
      {
        "url": "https://charts.cdn-surfline.com/nearshores/sealb_02.png",
        "timestamp": null
      },
      ...
      {
        "url": "https://charts.cdn-surfline.com/nearshores/sealb_14.png",
        "timestamp": null
      }
    ]
  }
}
```

## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `charts-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd4T1Rnek1qQTVNVEEifQ==&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eE9UZ3pNakE1TVRBIiwic2VsZWN0ZWROZXJkbGV0Ijp7Im5lcmRsZXRJZCI6ImFwbS1uZXJkbGV0cy5vdmVydmlldyJ9fQ==&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests on the endpoints detailed in the [Endpoints](#endpoints) section.

**Ex:**

Test `GET` requests against the `legacy/images` endpoint:

```
curl \
    -X GET \
    -i \
    http://charts-service.sandbox.surfline.com/legacy/images?legacyId=2143&type=localswell
```
**Expected response:** See [above](#sample-response) for sample response.


## Fixing broken subregion chart pages

1)	Get the list of subregions to fix. This should come from Ryan Starr or Dan Hunter. The list should include the ID of the subregion to add/fix as well as the ID of another subregion to copy the chartTypes configuration from.

Tip:	Put the list in a spreadsheet and add extra columns below to track each step. Example:
| Subregion ID              | Subregion Name  | Subregion ID to copy from   | Legacy ID       | ChartTypes Updated  | Mongo Document Updated |
| -------------             | -------------   | -------------               | -------------   | -------------       | -------------          |
| 595573ba31d48a00128db925  | Nias		        | 58581a836630e24c44878fee	  | 2168            | Yes                 | Yes

- Subregion Name: you might have to lookup the name of the subregion in the database under the subregions collection using the ID
- Legacy ID: you will get this from chartTypes.json from the 'id' property.


2) Update wavetrak-services/sevices/charts-service/chartTypes.json. For each new subregion, you will need to copy the chart configuration from an existing subregion.


a) Use the 'Subregion ID to copy from' and search chartTypes.json to find it. Copy the entire JSON object and paste below.
Here is an example that matches the table in #1 above.

```json
"58581a836630e24c44878fee": {
  "subregionId": "58581a836630e24c44878fee",
  "id": "2168",
  "name": "Mentawais",
  "url": "/surf-charts/indian-ocean/indonesia/mentawais/2168",
  "spotIds": [
    72188,
    116182,
    116187,
    116186,
    116185,
    116184,
    116188,
    116189,
    11969,
    7203,
    7202,
    7201,
    7200,
    116193,
    7199
  ],
  "charts": {
    "wrf": [
      {
        "id": 138953,
        "name": "Mentawais Regional",
        "match": true
      },
      {
        "id": 128249,
        "name": "Pulau Pagai-utara",
        "match": true
      },
      {
        "id": 128252,
        "name": "Pulau Sipura",
        "match": true
      },
      {
        "id": 128253,
        "name": "Pulau Siberut",
        "match": true
      },
      {
        "id": 128254,
        "name": "Pulau Tanahmasa",
        "match": true
      }
    ],
    "nam": null,
    "nearshore": []
  },
  "region": "Indonesia"
},
```


b) Change the ID to be the new 'Subregion ID' and also change the Name. Here is an example that matches the table in #1 above.

- There are 2 spots where the subregionID is listed and both need to be updated. "58581a836630e24c44878fee" gets changed to "595573ba31d48a00128db925" in the example below.
- Name gets changed to "Nias"
- "2168" is the legacy ID which will be used to update the MongoDB Subregion document in Step 5 below to set "imported.subregionId". We'll just note this in our spreadsheet in preparation for Step 5.
- All other fields remain unedited.

```json
"595573ba31d48a00128db925": {
  "subregionId": "595573ba31d48a00128db925",
  "id": "2168",
  "name": "Nias",
  "url": "/surf-charts/indian-ocean/indonesia/mentawais/2168",
  "spotIds": [
    72188,
    116182,
    116187,
    116186,
    116185,
    116184,
    116188,
    116189,
    11969,
    7203,
    7202,
    7201,
    7200,
    116193,
    7199
  ],
  "charts": {
    "wrf": [
      {
        "id": 138953,
        "name": "Mentawais Regional",
        "match": true
      },
      {
        "id": 128249,
        "name": "Pulau Pagai-utara",
        "match": true
      },
      {
        "id": 128252,
        "name": "Pulau Sipura",
        "match": true
      },
      {
        "id": 128253,
        "name": "Pulau Siberut",
        "match": true
      },
      {
        "id": 128254,
        "name": "Pulau Tanahmasa",
        "match": true
      }
    ],
    "nam": null,
    "nearshore": []
  },
  "region": "Indonesia"
},
```



4)	Launch:

- Launch your branch to sandbox/ staging and make sure charts are working in general to ensure there wasn’t invalid JSON
- Submit a PR & Merge to master
- Launch to prod.



5) While prod is deploying, update the new MongoDB subregion documents and set ‘imported.subregionId’ if the new subregion does not yet have a legacy id. Most won't. Use the 'id' property from the JSON object. Ex. "2168".

Using Studio 3T:
- Find the subregion by ID using Query:
`{_id: ObjectId("58581a836630e24c44878fee")}`
- Right click the document and choose Document -> Edit Document (JSON)
- Add 'imported.subregionId` as a NumberInt(2168) type and click Update.

Here is an example of the Nias subregion.
```json
{
    "_id" : ObjectId("595573ba31d48a00128db925"),
    "name" : "Nias",
    "region" : ObjectId("5908e66d380d980012ba8a80"),
    "primarySpot" : ObjectId("598a2a8ea750500013cf37d5"),
    "__v" : NumberInt(0),
    "status" : "PUBLISHED",
    "geoname" : NumberInt(1643084),
    "offshoreSwellLocation" : {
        "coordinates" : [
            NumberInt(97),
            NumberInt(1)
        ],
        "type" : "Point"
    },
    "imported" : {
        "basins" : [
            "io"
        ],
        "subregionId" : NumberInt(2168)
    }
}
```


Note: No updates to chartImages.json necessary, as those configurations are linked to the legacy id.

6) QA: Go to surfline.com and navigate to the subregions affected. Check the 'Charts' page for the subregion and ensure it loads without error.

You can also view the [JIRA ticket] (https://wavetrak.atlassian.net/browse/FE-946) for a previously completed update and the [corresponding PR here](https://github.com/Surfline/wavetrak-services/pull/6007).
