import bunyan from 'bunyan';
import Logsene from '@surfline/bunyan-logsene';
import PrettyStream from 'bunyan-prettystream';
import config from '../config';

const logseneStream = new Logsene({
  token: config.LOGSENE_KEY, // default to sandbox
});

// Using hadfieldn's fork of node-bunyan-prettystream
// to handle pretty printing with circular reference safety
const prettyStdOut = new PrettyStream();
prettyStdOut.pipe(process.stdout);


/**
 * Use example:
 *
 * // In ES6 using "import { x } from 'y'" is analogous to overwriting module.exports with a
 * // function in ES5. * as logger makes writing unit tests easier.
 * import * as logger from '../common/logger';
 *
 * export function MyController() {
 *    const log = logger.logger('user-service');
 *    log.trace('MyController created');
 * }
 *
 *
 * Test example:
 *
 * import sinon from 'sinon';
 * import * as logger from '../common/logger';
 * import * as MyController from './MyController';
 *
 * sinon.stub(logger, 'logger').returns({});
 *
 * @param name of service
 */
const logger = (name) => {
  const log = bunyan.createLogger({
    name: name || 'default-js-logger',
    serializers: bunyan.stdSerializers,
    streams: [
      {
        level: config.CONSOLE_LOG_LEVEL,
        type: 'raw',
        stream: prettyStdOut,
      },
      {
        level: config.LOGSENE_LEVEL,
        stream: logseneStream,
        type: 'raw',
        reemitErrorEvents: true,
      },
    ],
  });

  // TODO: Do we need these err and stream parameters for something? Does changing the signature
  //       of the function we pass in affect the behavior of this stream?
  // eslint-disable-next-line no-unused-vars
  log.on('error', (err, stream) => {
    console.error('Error logging.');
  });
  log.trace(`${name} logging started.`);
  return log;
};

export default logger;
