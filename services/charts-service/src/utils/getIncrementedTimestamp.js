import { utc } from 'moment';
import moment from 'moment-timezone';

import parseModelTime from './parseModelTime';

const secondsPerHour = 60 * 60;

const getIncrementedTimestamp = (chartData, zone, index) => {
  if (!chartData || !chartData.UTCTIME || chartData.UTCTIME === 'UNDEF') {
    return null;
  }

  const modelTimeUTC = parseModelTime(chartData.UTCTIME);

  const utcTime = utc(moment.tz(modelTimeUTC, zone)) / 1000;
  const timeToAdd = secondsPerHour * chartData.INCREMENT * index;

  return utcTime + timeToAdd;
};

export default getIncrementedTimestamp;
