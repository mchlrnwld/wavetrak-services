import { isBasinChart } from "./getChartTypes";
import { getLegacyCustomChartTime } from '../external/legacy';

const chartTimes = {
  regionalswell: {
    hasTime: true,
    usePrefix: true,
  },
  regionalperiod: {
    hasTime: true,
    usePrefix: true,
  },
  regionalsst: {
    hasTime: false,
    usePrefix: false,
  },
  regionalwind: {
    hasTime: true,
    usePrefix: true,
  },
  localswell: {
    hasTime: true,
    usePrefix: true,
  },
  localperiod: {
    hasTime: true,
    usePrefix: true,
  },
  nearshoresst: {
    hasTime: false,
    usePrefix: false,
  },
  localwind: {
    hasTime: true,
    usePrefix: true,
  },
  hireswind: {
    hasTime: true,
    usePrefix: true,
  },
  custom: {
    hasTime: true,
    usePrefix: false,
  },
  basins: {
    hasTime: true,
    usePrefix: false,
  },
  globalwave: {
    hasTime: true,
    usePrefix: true,
    prefix: 'wht_',
  },
  globalwind: {
    hasTime: true,
    usePrefix: true,
    prefix: 'wind_',
  },
  globalperiod: {
    hasTime: true,
    usePrefix: true,
    prefix: 'pper_',
  },
  localsst: {
    hasTime: false,
    usePrefix: false,
  },
  japansst: {
    hasTime: false,
    usePrefix: false,
  },
  japanwave: {
    hasTime: true,
    usePrefix: true,
    prefix: 'japan_wave_',
  },
  japanwave_tn: {
    hasTime: true,
    usePrefix: true,
    prefix: 'japan_wave_',
  },
  japanwind: {
    hasTime: true,
    usePrefix: true,
    prefix: 'japan_wind_',
  },
  japanwind_tn: {
    hasTime: true,
    usePrefix: true,
    prefix: 'japan_wind_',
  },
  japanperiod: {
    hasTime: true,
    usePrefix: true,
    prefix: 'japan_period_',
  },
  japanperiod_tn: {
    hasTime: true,
    usePrefix: true,
    prefix: 'japan_period_',
  },
};

const getChartTimePrefix = (url) => {
  const urlArrayRaw = url.split('/');
  // return second to last value of modified url base
  const urlArray = urlArrayRaw.filter((node, index) => index === urlArrayRaw.length - 2);
  const chartPrefix = `${urlArray.join('')}_`;
  return chartPrefix;
};

const getBaseChartTimeUrl = (url) => {
  const urlArrayRaw = url.split('/');
  // strip off partial image url and return base for chart time url
  const urlArray = urlArrayRaw.filter((node, index) => index < urlArrayRaw.length - 1);
  return urlArray.join('/');
};

const getChartTimeData = async (type, imgUrl) => {
  const baseUrl = getBaseChartTimeUrl(imgUrl);
  let chartType = type.toString();
  if (isBasinChart(type)) {
    chartType = 'basins';
  } else if (!chartTimes[chartType]) {
    chartType = 'custom';
  }

  if (!chartTimes[chartType].hasTime) return null;
  const { usePrefix, prefix } = chartTimes[chartType];
  let chartTimeFile = '/chart_time.js';
  if (prefix) {
    chartTimeFile = `/${chartTimes[chartType].prefix}chart_time.js`;
  } else if (usePrefix) {
    const chartTimePrefix = getChartTimePrefix(baseUrl);
    chartTimeFile = `/${chartTimePrefix}chart_time.js`;
  }
  const chartTimeUrl = `${baseUrl}${chartTimeFile}`;
  const chartTimeData = await getLegacyCustomChartTime(chartTimeUrl);
  return chartTimeData;
};

export default getChartTimeData;
