import { expect } from 'chai';
import sinon from 'sinon';
import getChartImages from './getChartImages';
import * as legacyAPI from '../external/legacy';
import chartImagesFixture from './fixtures/chartImagesFixture';
import chartImagesPremiumFixture from './fixtures/chartImagesPremiumFixture';
import chartImagesNamiaruFixture from './fixtures/chartImagesNamiaruFixture';
import chartImagesThumbnailFixture from './fixtures/chartImagesThumbnailFixture';
import chartTimeFixture from './fixtures/chartTimeFixture.json';
import * as parseModelTime from './parseModelTime';

const getChartImagesSpec = () => {
  describe('/getChartImages', () => {
    const legacyId = '2143';
    const namiaruLegacyId = '91255';
    const modelTimeUTC = '2021-05-19T06:00:00.000Z';
    const basinType = 'blkwind';
    const globalType = 'globalwind';
    const hiResType = 'hireswind';
    const localWindType = 'localwind';
    const namiaruType = 'japanwave';
    const namiaruThumbnail = 'japanwave_tn';
    const nearshoreSstType = 'nearshoresst';
    const premium = true;
    const tempUnit = 'C';

    /** @type {sinon.SinonStub} */
    let parseModelTimeStub;

    beforeEach(() => {
      parseModelTimeStub = sinon.stub(parseModelTime, 'default');
    });

    afterEach(() => {
      parseModelTimeStub.restore();
    });

    it('should throw APIError if invalid legacyId param provided', async () => {
      try {
        await getChartImages('invalid legacyId', localWindType, premium);
      } catch (err) {
        expect(err).to.exist();
        expect(err.statusCode).to.equal(400);
        expect(err.name).to.equal('APIError');
        expect(err.message).to.equal('Valid legacyId required');
      }
    });

    it('should throw APIError if requested type does not exist for subregion', async () => {
      try {
        await getChartImages(legacyId, 'invalidChartType');
      } catch (err) {
        expect(err).to.exist();
        expect(err.statusCode).to.equal(400);
        expect(err.name).to.equal('APIError');
        expect(err.message).to.equal('No invalidChartType chart found for subregion 2143');
      }
    });

    it('should replace http with https in URL string', async () => {
      parseModelTimeStub.returns(modelTimeUTC);
      legacyAPI.getLegacyCustomChartTime.resolves(chartTimeFixture);
      const { images } = await getChartImages(legacyId, localWindType);
      expect(images[0].url).to.equal(
        'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_1.png',
      );
    });

    it('should format image URL and timestamp', async () => {
      parseModelTimeStub.returns(modelTimeUTC);
      legacyAPI.getLegacyCustomChartTime.resolves(chartTimeFixture);
      const { images } = await getChartImages(legacyId, localWindType);
      expect(images).to.deep.equal(chartImagesFixture.images);
    });

    it('should update nearshoresst URL with provided tempUnit', async () => {
      parseModelTimeStub.returns(modelTimeUTC);
      legacyAPI.getLegacyCustomChartTime.resolves(chartTimeFixture);
      const defaultImages = await getChartImages(legacyId, nearshoreSstType, premium);
      const tempCImages = await getChartImages(legacyId, nearshoreSstType, premium, tempUnit);
      expect(defaultImages.images[0].url).to.equal(
        'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshoresst/norange_large_1_F.png',
      );
      expect(tempCImages.images[0].url).to.equal(
        `https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshoresst/norange_large_1_${tempUnit}.png`,
      );
    });

    it('should update global charts URLs with gif file type', async () => {
      parseModelTimeStub.returns(modelTimeUTC);
      legacyAPI.getLegacyCustomChartTime.resolves(chartTimeFixture);
      const { images } = await getChartImages(legacyId, globalType, premium);
      expect(images[0].url).to.equal(
        'https://slcharts01.cdn-surfline.com/charts/global/global/globalwind_large_1.gif',
      );
    });

    it('should update hiRes charts URLs with _vect extension', async () => {
      parseModelTimeStub.returns(modelTimeUTC);
      legacyAPI.getLegacyCustomChartTime.resolves(chartTimeFixture);
      const { images } = await getChartImages(legacyId, hiResType, premium);
      expect(images[0].url).to.equal(
        'https://slcharts02.cdn-surfline.com/charts/socal/norange/hireswind/norange_large_1_vect.png',
      );
    });

    it('should return null timestamp if UTCTIME is undefined', async () => {
      parseModelTimeStub.returns(modelTimeUTC);
      legacyAPI.getLegacyCustomChartTime.resolves(null);
      const { images } = await getChartImages(legacyId, hiResType, premium);
      expect(images[0].timestamp).to.equal(null);
    });

    it('should format timezone', async () => {
      parseModelTimeStub.returns(modelTimeUTC);
      legacyAPI.getLegacyCustomChartTime.resolves(chartTimeFixture);
      const { timezone } = await getChartImages(legacyId, localWindType);
      expect(timezone).to.deep.equal(chartImagesFixture.timezone);
    });

    it('should return URLs for allowed timesteps for anonymous user', async () => {
      parseModelTimeStub.returns(modelTimeUTC);
      legacyAPI.getLegacyCustomChartTime.resolves(chartTimeFixture);
      const { timezone } = await getChartImages(legacyId, localWindType);
      expect(timezone).to.deep.equal(chartImagesFixture.timezone);
    });

    it('should update timesteps for basin types', async () => {
      parseModelTimeStub.returns(modelTimeUTC);
      legacyAPI.getLegacyCustomChartTime.resolves(chartTimeFixture);
      const defaultTimesteps = await getChartImages(legacyId, localWindType);
      const basinTimesteps = await getChartImages(legacyId, basinType);
      expect(defaultTimesteps.images).to.have.length(30);
      expect(basinTimesteps.images).to.have.length(61);
    });

    it('should return all image URLs for premium user', async () => {
      parseModelTimeStub.returns(modelTimeUTC);
      legacyAPI.getLegacyCustomChartTime.resolves(chartTimeFixture);
      const chartImages = await getChartImages(legacyId, localWindType, premium);
      expect(chartImages).to.deep.equal(chartImagesPremiumFixture);
    });

    it('should return all image URLs for third party user', async () => {
      parseModelTimeStub.returns(modelTimeUTC);
      legacyAPI.getLegacyCustomChartTime.resolves(chartTimeFixture);
      const chartImages = await getChartImages(namiaruLegacyId, namiaruType, premium);
      expect(chartImages).to.deep.equal(chartImagesNamiaruFixture);
    });

    it('should update image URL for thumbnail types', async () => {
      parseModelTimeStub.returns(modelTimeUTC);
      legacyAPI.getLegacyCustomChartTime.resolves(chartTimeFixture);
      const chartImages = await getChartImages(namiaruLegacyId, namiaruThumbnail, premium);
      expect(chartImages).to.deep.equal(chartImagesThumbnailFixture);
    });
  });
};

export default getChartImagesSpec;
