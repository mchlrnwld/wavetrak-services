import moment from 'moment-timezone';
import { APIError } from '@surfline/services-common';
import getChartTimeData from './getChartTimes';
import getIncrementedTimestamp from './getIncrementedTimestamp';
import allowedTimesteps from './allowedTimesteps';
import { isBasinChart } from './getChartTypes';
import chartImages from '../model/chartImages.json';

const formatImageUrl = (type, baseUrl, index, tempUnit) => {
  let fileExt = '.png';
  let url = `${baseUrl.replace('http://', 'https://')}${index + 1}`;
  if (type === 'nearshoresst' || type === 'regionalsst' || type === 'japansst' || type === 'localsst') {
    url += `_${tempUnit.toUpperCase()}`;
  }
  if (type.indexOf('_tn') > -1) url += '_tn';
  else if (type === 'hireswind' || !Number.isNaN(parseFloat(type))) url += '_vect';
  else if (type.indexOf('global') > -1 || isBasinChart(type)) fileExt = '.gif';
  return `${url}${fileExt}`;
};

const getChartBaseUrl = (type, baseUrl) => {
  if (type === 'hireswind' || !Number.isNaN(parseFloat(type))) {
    return `${baseUrl.replace('slcharts01', 'slcharts02')}`;
  }
  return baseUrl;
};

const getImageCount = (chart, type) => {
  if (type.indexOf('global') > -1 || isBasinChart(type)) return parseInt(chart.end, 10);
  return chart.end / chart.increment;
};

const getChartImages = async (legacyId, type, premium, tempUnit = 'F', thirdParty = null) => {
  const subregionChartImages = chartImages[legacyId];
  if (!subregionChartImages) throw new APIError('Valid legacyId required', 400);
  const { charts, timezoneString } = subregionChartImages;
  const chart = charts[type];
  if (!chart) throw new APIError(`No ${type} chart found for subregion ${legacyId}`, 400);
  const chartBaseUrl = getChartBaseUrl(type, chart.baseUrl);
  const chartTimeData = await getChartTimeData(type, chartBaseUrl);
  const images = [...Array(getImageCount(chart, type))]
    .map((_, index) => ({
      url: (premium || thirdParty) || index < allowedTimesteps(type)
        ? formatImageUrl(type, chartBaseUrl, index, tempUnit)
        : null,
      timestamp: getIncrementedTimestamp(chartTimeData, timezoneString, index),
    }));
  const timezone = moment.tz(timezoneString).format('z');
  return { images, timezone };
};

export default getChartImages;
