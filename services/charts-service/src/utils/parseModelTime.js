const months = {
  JAN: '01',
  FEB: '02',
  MAR: '03',
  APR: '04',
  MAY: '05',
  JUN: '06',
  JUL: '07',
  AUG: '08',
  SEP: '09',
  OCT: '10',
  NOV: '11',
  DEC: '12',
};

// use modelTime string instead of localtime from chart to get utc ts
const parseModelTime = (modelTime) => {
  const currentYear = new Date().getFullYear();
  let yearString = currentYear.toString();
  let utcDateString = null;
  if (modelTime.indexOf('Z') > -1) {
    const hourDate = modelTime.split('Z');
    const hourString = hourDate[0];
    const dateString = hourDate[1];
    const dayString = dateString.substring(0, 2);
    const monthString = dateString.substring(2, 5);
    yearString = dateString.substring(5, 9);
    utcDateString = `${yearString}-${months[monthString]}-${dayString}T${hourString}:00:00.000Z`;
  } else {
    yearString = modelTime.substring(0, 4);
    const monthString = modelTime.substring(4, 7);
    const dayString = modelTime.substring(7, 9);
    const hourString = modelTime.substring(9, 11);
    utcDateString = `${yearString}-${months[monthString]}-${dayString}T${hourString}:00:00.000Z`;
  }
  if (parseInt(yearString, 10) < currentYear) {
    return null;
  }
  return utcDateString;
};

export default parseModelTime;
