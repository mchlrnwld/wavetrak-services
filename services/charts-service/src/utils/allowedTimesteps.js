const allowedTimesteps = (type) => {
  if (!Number.isNaN(parseFloat(type))) {
    return 0;
  }
  switch (type) {
    case 'nearshore':
      return 0;
    case 'hireswind':
      return 0;
    default:
      return 9;
  }
};

export default allowedTimesteps;
