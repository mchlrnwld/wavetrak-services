const userTypes = {
  ANONYMOUS: 'anonymous',
  REGISTERED: 'registered',
};
export default userTypes;
