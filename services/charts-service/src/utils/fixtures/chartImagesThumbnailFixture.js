const chartImagesThumbnailFixutre = {
  images: [
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_1_tn.png',
      timestamp: 1621404000,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_2_tn.png',
      timestamp: 1621425600,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_3_tn.png',
      timestamp: 1621447200,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_4_tn.png',
      timestamp: 1621468800,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_5_tn.png',
      timestamp: 1621490400,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_6_tn.png',
      timestamp: 1621512000,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_7_tn.png',
      timestamp: 1621533600,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_8_tn.png',
      timestamp: 1621555200,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_9_tn.png',
      timestamp: 1621576800,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_10_tn.png',
      timestamp: 1621598400,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_11_tn.png',
      timestamp: 1621620000,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_12_tn.png',
      timestamp: 1621641600,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_13_tn.png',
      timestamp: 1621663200,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_14_tn.png',
      timestamp: 1621684800,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_15_tn.png',
      timestamp: 1621706400,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_16_tn.png',
      timestamp: 1621728000,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_17_tn.png',
      timestamp: 1621749600,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_18_tn.png',
      timestamp: 1621771200,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_19_tn.png',
      timestamp: 1621792800,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_20_tn.png',
      timestamp: 1621814400,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_21_tn.png',
      timestamp: 1621836000,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_22_tn.png',
      timestamp: 1621857600,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_23_tn.png',
      timestamp: 1621879200,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_24_tn.png',
      timestamp: 1621900800,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_25_tn.png',
      timestamp: 1621922400,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_26_tn.png',
      timestamp: 1621944000,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_27_tn.png',
      timestamp: 1621965600,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_28_tn.png',
      timestamp: 1621987200,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_29_tn.png',
      timestamp: 1622008800,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/japan/regional/japan_wave_30_tn.png',
      timestamp: 1622030400,
    },
  ],
  timezone: 'JST',
};
export default chartImagesThumbnailFixutre;
