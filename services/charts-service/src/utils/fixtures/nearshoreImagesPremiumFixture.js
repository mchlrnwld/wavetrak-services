const nearshoreImagesPremiumFixture = [
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_01.png',
    timestamp: null,
  },
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_02.png',
    timestamp: null,
  },
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_03.png',
    timestamp: null,
  },
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_04.png',
    timestamp: null,
  },
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_05.png',
    timestamp: null,
  },
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_06.png',
    timestamp: null,
  },
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_07.png',
    timestamp: null,
  },
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_08.png',
    timestamp: null,
  },
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_09.png',
    timestamp: null,
  },
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_10.png',
    timestamp: null,
  },
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_11.png',
    timestamp: null,
  },
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_12.png',
    timestamp: null,
  },
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_13.png',
    timestamp: null,
  },
  {
    url: 'https://charts.cdn-surfline.com/nearshores/sealb_14.png',
    timestamp: null,
  },
];

export default nearshoreImagesPremiumFixture;
