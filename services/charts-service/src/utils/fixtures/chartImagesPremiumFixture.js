const chartImagesPremiumFixutre = {
  images: [
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_1.png',
      timestamp: 1621404000,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_2.png',
      timestamp: 1621425600,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_3.png',
      timestamp: 1621447200,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_4.png',
      timestamp: 1621468800,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_5.png',
      timestamp: 1621490400,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_6.png',
      timestamp: 1621512000,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_7.png',
      timestamp: 1621533600,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_8.png',
      timestamp: 1621555200,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_9.png',
      timestamp: 1621576800,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_10.png',
      timestamp: 1621598400,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_11.png',
      timestamp: 1621620000,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_12.png',
      timestamp: 1621641600,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_13.png',
      timestamp: 1621663200,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_14.png',
      timestamp: 1621684800,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_15.png',
      timestamp: 1621706400,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_16.png',
      timestamp: 1621728000,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_17.png',
      timestamp: 1621749600,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_18.png',
      timestamp: 1621771200,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_19.png',
      timestamp: 1621792800,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_20.png',
      timestamp: 1621814400,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_21.png',
      timestamp: 1621836000,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_22.png',
      timestamp: 1621857600,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_23.png',
      timestamp: 1621879200,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_24.png',
      timestamp: 1621900800,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_25.png',
      timestamp: 1621922400,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_26.png',
      timestamp: 1621944000,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_27.png',
      timestamp: 1621965600,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_28.png',
      timestamp: 1621987200,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_29.png',
      timestamp: 1622008800,
    },
    {
      url: 'https://slcharts01.cdn-surfline.com/charts/socal/norange/nearshorewinds/norange_large_30.png',
      timestamp: 1622030400,
    },
  ],
  timezone: 'PST',
};
export default chartImagesPremiumFixutre;
