import { slugify } from '@surfline/web-common';
import namiaruChartTypes from '../model/namiaruChartTypes';

export const getLocationTypes = location => ([
  {
    type: `${location}swell`,
    name: 'Wave Height',
    slug: location === 'local' ? 'wave-height' : 'regional-wave-height',
  },
  {
    type: `${location}period`,
    name: 'Dominant Wave Period',
    slug: location === 'local' ? 'local-dominant-wave-period' : 'regional-dominant-wave-period',
  },
  {
    type: `${location === 'local' ? 'nearshore' : location}sst`,
    name: 'Water Temperature',
    slug: location === 'local' ? 'water-temperature' : 'regional-water-temperature',
  },
  {
    type: `${location}wind`,
    name: 'Wind (GFS)',
    slug: location === 'local' ? 'wind-gfs' : 'regional-wind-gfs',
  },
]);

export const basinEnum = {
  npac: {
    name: 'North Pacific',
    periodCharts: true,
  },
  spac: {
    name: 'South Pacific',
    periodCharts: true,
  },
  natl: {
    name: 'North Atlantic',
    periodCharts: true,
  },
  satl: {
    name: 'South Atlantic',
    periodCharts: true,
  },
  io: {
    name: 'Indian Ocean',
    periodCharts: true,
  },
  soc: {
    name: 'Sea of Cortez',
    periodCharts: false,
  },
  nrt: {
    name: 'North Sea',
    periodCharts: false,
  },
  med: {
    name: 'Mediterranean Sea',
    periodCharts: false,
  },
  glk: {
    name: 'Great Lakes',
    periodCharts: false,
  },
  blt: {
    name: 'Baltic Sea',
    periodCharts: false,
  },
  blk: {
    name: 'Black Sea',
    periodCharts: false,
  },
  red: {
    name: 'Red Sea',
    periodCharts: false,
  },
};

export const isBasinChart = (type) => {
  const first2 = type.substring(0, 2);
  const first3 = type.substring(0, 3);
  const first4 = type.substring(0, 4);
  return (!!basinEnum[first2] && first2 === 'io') || !!basinEnum[first3] || !!basinEnum[first4];
};

export const getBasinStandardByPrefix = basinPrefix => ([
  {
    type: `${basinPrefix}wave`,
    name: `${basinEnum[basinPrefix].name} Wave Height`,
    slug: `${slugify(basinEnum[basinPrefix].name)}-wave-height`,
  },
  {
    type: `${basinPrefix}period`,
    name: `${basinEnum[basinPrefix].name} Dominant Wave Period`,
    slug: `${slugify(basinEnum[basinPrefix].name)}-dominant-wave-period`,
  },
  {
    type: `${basinPrefix}wind`,
    name: `${basinEnum[basinPrefix].name} Wind (GFS)`,
    slug: `${slugify(basinEnum[basinPrefix].name)}-wind-gfs`,
  },
]);

export const getBasinTSwellByPrefix = (basinPrefix) => {
  const basinTSwell = basinEnum[basinPrefix].periodCharts ? [
    {
      type: `${basinPrefix}eband10`,
      name: `${basinEnum[basinPrefix].name} Swell T>10s`,
      slug: `${slugify(basinEnum[basinPrefix].name)}-swell-10-seconds`,
    },
    {
      type: `${basinPrefix}eband13`,
      name: `${basinEnum[basinPrefix].name} Swell T>13s`,
      slug: `${slugify(basinEnum[basinPrefix].name)}-swell-13-seconds`,
    },
    {
      type: `${basinPrefix}eband16`,
      name: `${basinEnum[basinPrefix].name} Swell T>16s`,
      slug: `${slugify(basinEnum[basinPrefix].name)}-swell-16-seconds`,
    },
    {
      type: `${basinPrefix}eband20`,
      name: `${basinEnum[basinPrefix].name} Swell T>20s`,
      slug: `${slugify(basinEnum[basinPrefix].name)}-swell-20-seconds`,
    },
  ] : undefined;
  return basinTSwell;
};

export const globalTypes = [
  {
    type: 'globalwave',
    name: 'Wave Height',
    slug: 'global-wave-height',
  },
  {
    type: 'globalperiod',
    name: 'Dominant Wave Period',
    slug: 'global-dominant-wave-period',
  },
  {
    type: 'globalwind',
    name: 'Wind (GFS)',
    slug: 'global-wind-gfs',
  },
  {
    type: 'globaleband10',
    name: 'Swell T>10s',
    slug: 'global-swell-10-seconds',
  },
  {
    type: 'globaleband13',
    name: 'Swell T>13s',
    slug: 'global-swell-13-seconds',
  },
  {
    type: 'globaleband16',
    name: 'Swell T>16s',
    slug: 'global-swell-16-seconds',
  },
  {
    type: 'globaleband20',
    name: 'Swell T>20s',
    slug: 'global-swell-20-seconds',
  },
];

const getHiResWindData = (subregionName, subregionCharts) => {
  const wrfArray = subregionCharts.wrf;
  const namHiResChart = {
    type: 'hireswind',
    name: 'High Res Wind (3 hr)',
    slug: 'local-wind-high-res',
  };

  if (!wrfArray && subregionCharts.nam) return [namHiResChart];
  if (!wrfArray || !wrfArray.length) return null;

  const hiResArrayFiltered = wrfArray.length > 1
    ? wrfArray.filter(wrf => wrf.name === subregionName || (wrf.match && wrf.match === true))
    : wrfArray;

  const hiResArray = hiResArrayFiltered.map(hiResChart => ({
    type: hiResChart.id.toString(),
    name: `${hiResChart.name} High Res Wind`,
    slug: `${slugify(hiResChart.name)}-wind-high-res`,
  }));

  if (subregionCharts.nam) {
    hiResArray.unshift(namHiResChart);
  }

  const hisResObject = hiResArray.length ? hiResArray : null;
  return hisResObject;
};

export const getBasinTypes = async (subregion) => {
  const basinPrefixArray = subregion.basins ? subregion.basins : null;
  if (!basinPrefixArray.length) {
    return null;
  }
  const basinsArray = [];
  basinPrefixArray.forEach((basinPrefix) => {
    const basinsByPrefix = getBasinStandardByPrefix(basinPrefix);
    const basinsTByPrefix = getBasinTSwellByPrefix(basinPrefix);
    basinsByPrefix.forEach((basinChart) => {
      if (basinChart) {
        basinsArray.push(basinChart);
      }
    });

    if (basinsTByPrefix) {
      basinsTByPrefix.forEach((basinChartT) => {
        if (basinChartT) {
          basinsArray.push(basinChartT);
        }
      });
    }
  });

  return basinsArray;
};

export const getSubregionTypes = (subregionChart) => {
  const hiResChartType = getHiResWindData(subregionChart.name, subregionChart.charts);
  return hiResChartType ?
    [...getLocationTypes('local'), ...hiResChartType]
    : getLocationTypes('local');
};

export const getRegionTypes = (namiaru, thirdParty) => {
  let regionTypes = getLocationTypes('regional');
  if (namiaru && thirdParty) regionTypes = [...regionTypes, ...namiaruChartTypes];
  return regionTypes;
};

export const getNearshoreNames = nearshore => nearshore.map(chart => ({
  type: 'nearshore',
  name: chart.name,
  slug: `${slugify(chart.name)}-nearshore-wave-models`,
}));

export const getNearshoreCustomName = (nearshoreCustom) => {
  if (nearshoreCustom) {
    return {
      type: 'nearshore',
      name: `${nearshoreCustom.id}`,
      slug: `${slugify(nearshoreCustom.subregion)}-nearshore-wave-models`,
      displayName: nearshoreCustom.displayName,
    };
  }
  return null;
};
