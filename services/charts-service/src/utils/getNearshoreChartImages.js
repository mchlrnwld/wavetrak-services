import { find as _find, get as _get } from 'lodash';
import { APIError } from '@surfline/services-common';
import chartTypes from '../model/chartTypes.json';
import config from '../config';
import allowedTimesteps from './allowedTimesteps';

const NEARSHORE_IMAGES_LENGTH = 14;
const NEARSHORE_CUSTOM_IMAGES_LENGTH = 58;

const formatNearshoreImages = (chart, premium) => [...Array(NEARSHORE_IMAGES_LENGTH)]
  .map((_, index) => ({
    url: premium || index < allowedTimesteps('nearshore') ?
      `${config.CHART_CDN}/nearshores/${chart.id}_${index < 9 ? `0${index + 1}` : index + 1}.png`
      : null,
    timestamp: null,
  }));

const formatNearshoreCustomImages = (chart, premium) => {
  const urlPrefix = `${config.CHART_CDN_2}/charts/${chart.region}/${chart.subregion}/nearshoremodel/`;
  return [...Array(NEARSHORE_CUSTOM_IMAGES_LENGTH)].map((_, index) => ({
    url: premium ?
      `${urlPrefix}${chart.subregion}_${index + 1}.png`
      : null,
    timestamp: null,
  }));
};

const getNearshoreChartImages = (legacyId, nearshoreModelName, premium) => {
  if (!nearshoreModelName) {
    throw new APIError('Param nearshoreModelName is required for type nearshore charts', 400);
  }

  const subregionChart = _find(Object.values(chartTypes), ['id', legacyId]);
  if (!subregionChart) throw new APIError('Valid legacyId required', 400);

  const isNearshoreCustomType = !Number.isNaN(Number(nearshoreModelName));
  if (isNearshoreCustomType) {
    const nearshoreCustomMap = _get(subregionChart, 'charts.nearshoreCustom', {});
    const nearshoreCustomChart = nearshoreCustomMap[nearshoreModelName];
    if (!nearshoreCustomChart) {
      throw new APIError(`Nearshore model chart not found for subregion ${legacyId}`, 400);
    }
    return formatNearshoreCustomImages(nearshoreCustomChart, premium);
  }

  const nearshoreList = _get(subregionChart, 'charts.nearshore', []);
  const nearshoreChart = nearshoreList.find(chart => chart.name === nearshoreModelName);
  if (!nearshoreChart) {
    throw new APIError(`Nearshore model chart not found for subregion ${legacyId}`, 400);
  }
  return formatNearshoreImages(nearshoreChart, premium);
};

export default getNearshoreChartImages;
