import { expect } from 'chai';
import getNearshoreChartImages from './getNearshoreChartImages';
import nearshoreImagesNonPremiumFixture from './fixtures/nearshoreImagesNonPremiumFixture';
import nearshoreImagesPremiumFixture from './fixtures/nearshoreImagesPremiumFixture';
import nearshoreCustomNonPremiumFixture from './fixtures/nearshoreCustomNonPremiumFixture';
import nearshoreCustomPremiumFixture from './fixtures/nearshoreCustomPremiumFixture';

const getNearshoreChartImagesSpec = () => {
  describe('/getNearshoreChartImages', () => {
    const legacyId = '2143';
    const nearshoreModelName = 'Seal Beach to HB Cliffs';
    const customLegacyId = '2156';
    const customNearshoreModelName = '121952';
    const premium = true;

    it('should throw APIError if invalid legacyId param provided', () => {
      try {
        getNearshoreChartImages('invalid legacyId', nearshoreModelName, premium);
      } catch (err) {
        expect(err).to.exist();
        expect(err.statusCode).to.equal(400);
        expect(err.name).to.equal('APIError');
        expect(err.message).to.equal('Valid legacyId required');
      }
    });

    it('should throw APIError if nearshoreModelName param not provided', () => {
      try {
        getNearshoreChartImages(legacyId);
      } catch (err) {
        expect(err).to.exist();
        expect(err.statusCode).to.equal(400);
        expect(err.name).to.equal('APIError');
        expect(err.message).to.equal(
          'Param nearshoreModelName is required for type nearshore charts',
        );
      }
    });

    it('should throw APIError if nearshoreModelName does not exist for subregion', () => {
      try {
        getNearshoreChartImages(legacyId, 'invalid chart name');
      } catch (err) {
        expect(err).to.exist();
        expect(err.statusCode).to.equal(400);
        expect(err.name).to.equal('APIError');
        expect(err.message).to.equal('Nearshore model chart not found for subregion 2143');
      }
    });

    it('should throw APIError if custom nearshoreModelName does not exist for subregion', () => {
      try {
        getNearshoreChartImages(customLegacyId, 123);
      } catch (err) {
        expect(err).to.exist();
        expect(err.statusCode).to.equal(400);
        expect(err.name).to.equal('APIError');
        expect(err.message).to.equal('Nearshore model chart not found for subregion 2156');
      }
    });

    it('should return null nearshore images for non-premium user', () => {
      const nearshoreImages = getNearshoreChartImages(legacyId, nearshoreModelName, false);
      expect(nearshoreImages).to.deep.equal(nearshoreImagesNonPremiumFixture);
    });

    it('should return nearshore images URLs for premium user', () => {
      const nearshoreImages = getNearshoreChartImages(legacyId, nearshoreModelName, premium);
      expect(nearshoreImages).to.deep.equal(nearshoreImagesPremiumFixture);
    });

    it('should return null nearshore custom images for non-premium user', () => {
      const nearshoreImages = getNearshoreChartImages(
        customLegacyId,
        customNearshoreModelName,
        false,
      );
      expect(nearshoreImages).to.deep.equal(nearshoreCustomNonPremiumFixture);
    });

    it('should return nearshore custom images for premium user', () => {
      const nearshoreImages = getNearshoreChartImages(
        customLegacyId,
        customNearshoreModelName,
        premium,
      );
      expect(nearshoreImages).to.deep.equal(nearshoreCustomPremiumFixture);
    });
  });
};

export default getNearshoreChartImagesSpec;
