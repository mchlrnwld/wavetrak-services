import 'newrelic';
import app from './server';
import logger from './common/logger';
import * as dbContext from './model/dbContext';

const log = logger('charts-service');

Promise.all([dbContext.initMongoDB()])
  .then(app)
  .catch((err) => {
    console.log('error', err);
    log.error({
      message: "App Initialization failed. Can't connect to database",
      stack: err,
    });
  });
