import { getEntitlements } from '../../external/entitlements';
import userTypes from '../../utils/userTypes';

const authenticateRequest = async (req, _, next) => {
  /* eslint-disable no-param-reassign */
  req.user = { premium: false };
  // req.authenticatedUserId is set on the auth middleware
  // created by `setupExpress` (see @surfline/services-common)
  if (req.authenticatedUserId) {
    const userEntitlements = await getEntitlements(req.authenticatedUserId);
    const premium = userEntitlements.entitlements.includes('sl_premium');
    req.user.premium = premium;
    const metered = userEntitlements.entitlements.includes('sl_metered');
    if (metered && !premium) {
      // For granting Premium access to Metered non premium users
      const anonymousId = req.get('x-auth-anonymousid');
      const { scopes } = req;
      const premiumOverride = scopes && scopes.includes('premium:read:registered');
      if (anonymousId && premiumOverride) {
        req.user = {
          id: req.authenticatedUserId,
          premium: true,
          entitlements: ['sl_metered'],
          type: userTypes.REGISTERED,
        };
      }
    }
  }
  // For granting Premium access to Metered Anonymous Sessions
  const anonymousId = req.get('x-auth-anonymousid');
  const { scopes } = req;
  const premiumOverride = scopes && scopes.includes('premium:read:anonymous');
  if (anonymousId && premiumOverride) {
    req.user = {
      id: anonymousId,
      premium: true,
      entitlements: ['sl_metered'],
      type: userTypes.ANONYMOUS,
    };
  }
  next();
  /* eslint-enable no-param-reassign */
};

export default authenticateRequest;
