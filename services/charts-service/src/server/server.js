import { setupExpress } from '@surfline/services-common';
import authenticateRequest from './middleware/authenticateRequest';
import legacy from './legacy';
import logger from '../common/logger';
import config from '../config';

const log = logger('charts-service');

const setupApp = () =>
  setupExpress({
    port: config.EXPRESS_PORT,
    allowedMethods: ['GET', 'OPTIONS'],
    handlers: [
      authenticateRequest,
      ['/legacy', legacy(log)],
    ],
    log,
    name: 'Charts Service',
  });

export default setupApp;
