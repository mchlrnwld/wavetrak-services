const trackLegacyImagesRequests = log => (req, res, next) => {
  log.trace({
    action: '/charts/legacy/images',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export default trackLegacyImagesRequests;
