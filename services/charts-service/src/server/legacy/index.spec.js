import getLegacyImagesHandlerSpec from './getLegacyImagesHandler.spec';
import getLegacyTypesHandlerSpec from './getLegacyTypesHandler.spec';

describe('/legacy', () => {
  getLegacyImagesHandlerSpec();
  getLegacyTypesHandlerSpec();
});
