import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import legacy from '.';

import * as getChartTypes from '../../utils/getChartTypes';
import chartTypes from '../../model/chartTypes.json';
import nearshoreNamesFixture from './fixtures/tests/nearshoreNames.json';
import subregionTypesFixture from './fixtures/tests/subregionTypes.json';
import regionTypesFixture from './fixtures/tests/regionTypes.json';
import basinTypesFixture from './fixtures/tests/basinTypes.json';

const subregion = {
  _id: '',
  basins: ['npac'],
};

const subregionChart = chartTypes['58581a836630e24c44878fd6'];
const subregionNamChart = chartTypes['58581a836630e24c4487917e'];

const getLegacyTypesHandlerSpec = () => {
  describe('/types', () => {
    let log;

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(legacy(log));
      chai.request(app).keepOpen();
    });

    beforeEach(() => {
      sinon.stub(getChartTypes, 'getSubregionTypes');
    });

    afterEach(() => {
      getChartTypes.getSubregionTypes.restore();
    });

    it('should return getNearshoreNames for nearshore chart types', () => {
      const nearshoreNames = getChartTypes.getNearshoreNames(subregionChart.charts.nearshore);
      expect(nearshoreNames).to.be.ok();
      expect(nearshoreNames).to.deep.equal(nearshoreNamesFixture);
    });

    it('should return getSubregionTypes chart types for subregion', async () => {
      getChartTypes.getSubregionTypes.resolves(subregionTypesFixture);
      const subregionTypes = await getChartTypes.getSubregionTypes(subregionChart);
      expect(subregionTypes).to.be.ok();
      expect(subregionTypes).to.deep.equal(subregionTypesFixture);
    });

    it('should return getSubregionTypes nam chart when no wrf and nam is true', async () => {
      const namDefaultFixture = {
        type: 'hireswind',
        name: 'High Res Wind (3 hr)',
        slug: 'local-wind-high-res',
      };
      getChartTypes.getSubregionTypes.resolves(namDefaultFixture);
      const subregionTypes = await getChartTypes.getSubregionTypes(subregionNamChart);
      expect(subregionTypes).to.be.ok();
      expect(subregionTypes).to.deep.equal(namDefaultFixture);
    });

    it('should return getRegionTypes chart types for subregion', () => {
      const regionTypes = getChartTypes.getRegionTypes(subregionChart.charts.nam);
      expect(regionTypes).to.be.ok();
      expect(regionTypes).to.deep.equal(regionTypesFixture);
    });

    it('should return getBasinTypes chart types for subregion', async () => {
      const basinTypes = await getChartTypes.getBasinTypes(subregion);
      expect(basinTypes).to.be.ok();
      expect(basinTypes).to.deep.equal(basinTypesFixture);
    });
  });
};

export default getLegacyTypesHandlerSpec;
