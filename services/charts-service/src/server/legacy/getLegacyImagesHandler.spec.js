import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import legacy from '.';
import * as legacyAPI from '../../external/legacy';
import createParamString from '../../external/createParamString';
import * as getChartImages from '../../utils/getChartImages';
import * as getNearshoreChartImages from '../../utils/getNearshoreChartImages';
import getChartImagesSpec from '../../utils/getChartImages.spec';
import getNearshoreChartImagesSpec from '../../utils/getNearshoreChartImages.spec';
import nearshoreImagesFreeUserFixutre from './fixtures/nearshoreImagesFreeUser.json';

const getLegacyImagesHandlerSpec = () => {
  describe('/images', () => {
    let log;
    let request;
    let nearshoreSpy;
    let imagesSpy;

    const legacyId = '2143';
    const subregionId = '58581a836630e24c44878fd6';
    const nearshoreType = 'nearshore';
    const localWindType = 'localwind';
    const nearshoreModelName = 'Seal Beach to HB Cliffs';
    const tempUnit = 'F';

    before(() => {
      log = { trace: sinon.spy() };
      const app = express();
      app.use(legacy(log));
      request = chai.request(app).keepOpen();
      nearshoreSpy = sinon.spy(getNearshoreChartImages, 'default');
      imagesSpy = sinon.spy(getChartImages, 'default');
    });

    beforeEach(() => {
      sinon.stub(legacyAPI, 'getLegacyCustomChartTime');
    });

    afterEach(() => {
      nearshoreSpy.resetHistory();
      imagesSpy.resetHistory();
      legacyAPI.getLegacyCustomChartTime.restore();
    });

    it('should call getNearshoreChartImages if type nearshore', async () => {
      const params = { legacyId, type: nearshoreType, nearshoreModelName };
      await request.get(`/images?${createParamString(params)}`).send();
      expect(nearshoreSpy).to.have.been.calledOnce();
      expect(nearshoreSpy).to.have.been.calledWithExactly(
        '2143',
        'Seal Beach to HB Cliffs',
        undefined,
      );
      expect(imagesSpy).not.to.have.been.called();
    });

    it('should call getChartImages if not type nearshore', async () => {
      const params = { legacyId, type: localWindType, tempUnit };
      await request.get(`/images?${createParamString(params)}`).send();
      expect(imagesSpy).to.have.been.calledOnce();
      expect(imagesSpy).to.have.been.calledWithExactly(
        '2143',
        'localwind',
        undefined,
        'F',
        undefined,
      );
      expect(nearshoreSpy).not.to.have.been.called();
    });

    it('should lookup legacyId from subregionId', async () => {
      const params = { subregionId, type: localWindType, tempUnit };
      await request.get(`/images?${createParamString(params)}`).send();
      expect(imagesSpy).to.have.been.calledOnce();
      expect(imagesSpy).to.have.been.calledWithExactly(
        '2143',
        'localwind',
        undefined,
        'F',
        undefined,
      );
      expect(nearshoreSpy).not.to.have.been.called();
    });

    it('should format response with data and associated properties', async () => {
      const params = { legacyId, type: nearshoreType, nearshoreModelName };
      const res = await request.get(`/images?${createParamString(params)}`).send();
      expect(res.body).to.deep.equal(nearshoreImagesFreeUserFixutre);
    });

    it('should throw 400 if legacyId and type params not provided', async () => {
      try {
        legacyAPI.getLegacyCustomChartTime.resolves(null);
        const res = await request.get('/images').send();
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Valid legacyId and type required' });
      }
    });

    getChartImagesSpec();
    getNearshoreChartImagesSpec();
  });
};

export default getLegacyImagesHandlerSpec;
