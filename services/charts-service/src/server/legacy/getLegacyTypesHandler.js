import chartTypes from '../../model/chartTypes.json';
import SubregionModel from '../../model/SubregionModel';

import {
  getNearshoreNames,
  getNearshoreCustomName,
  getSubregionTypes,
  getRegionTypes,
  globalTypes,
  getBasinTypes,
} from '../../utils/getChartTypes';

const getLegacyTypesHandler = async (req, res) => {
  const { subregionId, thirdParty } = req.query;
  const subregionChart = chartTypes[subregionId];
  const subregion = await SubregionModel.findOne({ _id: subregionId }) || null;
  if (!subregionChart) return res.status(400).send({ message: 'Valid subregionId required' });
  const nearshore = getNearshoreNames(subregionChart.charts.nearshore);
  if (subregionChart.charts.nearshoreCustom) {
    // get custom nearshore items into an array and push into nearshore array
    const customValues = Object.values(subregionChart.charts.nearshoreCustom);
    customValues.forEach((nearshoreData) => {
      nearshore.push(getNearshoreCustomName(nearshoreData));
    });
  }

  const types = {
    nearshore,
    region: getRegionTypes(subregionChart.charts.namiaru, thirdParty),
    subregion: await getSubregionTypes(subregionChart),
    basins: await getBasinTypes(subregion),
    global: globalTypes,
  };
  const data = { types };
  const associated = { region: subregionChart.region };
  return res.send({ data, associated });
};

export default getLegacyTypesHandler;
