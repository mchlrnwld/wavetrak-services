import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import getLegacyImagesHandler from './getLegacyImagesHandler';
import getLegacyTypesHandler from './getLegacyTypesHandler';
import trackLegacyImagesRequests from './trackLegacyImagesRequests';

const swells = (log) => {
  const api = Router();

  api.use('*', trackLegacyImagesRequests(log));
  api.get('/images', wrapErrors(getLegacyImagesHandler));
  api.get('/types', wrapErrors(getLegacyTypesHandler));

  return api;
};

export default swells;
