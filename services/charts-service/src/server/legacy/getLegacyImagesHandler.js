import chartTypes from '../../model/chartTypes.json';
import getChartImages from '../../utils/getChartImages';
import getNearshoreChartImages from '../../utils/getNearshoreChartImages';

const getLegacyImagesHandler = async (req, res) => {
  const premium = req.user && req.user.premium;
  const {
    nearshoreModelName,
    subregionId,
    tempUnit,
    thirdParty,
    type,
  } = req.query;
  let images;
  let timezone;
  let { legacyId } = req.query;
  if (!legacyId && subregionId && chartTypes[subregionId]) {
    legacyId = chartTypes[subregionId].id;
  }

  if (!legacyId || !type) {
    return res.status(400).send({
      message: 'Valid legacyId and type required',
    });
  }

  if (type === 'nearshore') images = getNearshoreChartImages(legacyId, nearshoreModelName, premium);
  else ({ images, timezone } = await getChartImages(legacyId, type, premium, tempUnit, thirdParty));
  return res.send({
    data: { images },
    associated: { timezone },
  });
};

export default getLegacyImagesHandler;
