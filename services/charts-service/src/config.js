
export default {
  EXPRESS_PORT: process.env.EXPRESS_PORT || 8081,
  LOGSENE_KEY: process.env.LOGSENE_KEY || '36621d1a-eee6-4cec-89b6-4b7f734d0d63',
  CONSOLE_LOG_LEVEL: process.env.CONSOLE_LOG_LEVEL || 'debug',
  LOGSENE_LEVEL: process.env.LOGSENE_LEVEL || 'debug',
  LEGACY_API: 'https://api.surfline.com/v1',
  CHART_CDN: 'https://charts.cdn-surfline.com',
  CHART_CDN_2: 'https://slcharts02.cdn-surfline.com',
  ENTITLEMENTS_SERVICE: process.env.ENTITLEMENTS_SERVICE,
};
