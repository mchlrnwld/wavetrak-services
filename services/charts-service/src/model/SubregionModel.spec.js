import { expect } from 'chai';
import mongoose from 'mongoose';
import Subregion from './SubregionModel';

describe('Subregion Model', () => {
  it('should require name field', (done) => {
    const subregion = new Subregion({
      spots: [{ _id: new mongoose.Types.ObjectId() }],
      regionId: new mongoose.Types.ObjectId(),
    });
    subregion.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.name).to.be.ok();
      expect(err.errors.name.message).to.equal('Name is required');
      return done();
    });
  });

  it('should require region field', (done) => {
    const subregion = new Subregion({
      name: 'Test Subregion',
      spots: [{ _id: new mongoose.Types.ObjectId() }],
    });
    subregion.save((err) => {
      expect(err).to.be.ok();
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.region).to.be.ok();
      expect(err.errors.region.message).to.equal('Region is required');
      return done();
    });
  });
});
