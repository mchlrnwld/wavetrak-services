import mongoose from 'mongoose';
import 'mongoose-geojson-schema';

const subregionSchema = new mongoose.Schema(
  {
    name: { type: String, required: [true, 'Name is required'] },
    geoname: { type: Number, required: [true, 'Geoname is required'] },
    region: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Region',
      required: [true, 'Region is required'],
    },
    primarySpot: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Spot',
    },
    imported: {
      subregionId: { type: Number, select: true },
      basins: { type: Array, select: true },
    },
    offshoreSwellLocation: {
      type: { type: String },
      coordinates: [Number],
    },
  },
  {
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
    collection: 'Subregions',
  },
);

subregionSchema.index({ offshoreSwellLocation: '2dsphere' });

subregionSchema.virtual('legacyId').get(function setLegacyId() {
  return this.imported.subregionId ? this.imported.subregionId : null;
});

subregionSchema.virtual('basins').get(function setBasins() {
  return this.imported.basins ? this.imported.basins : null;
});

subregionSchema.set('toJSON', {
  transform: (doc, ret) => {
    delete ret.imported; // eslint-disable-line no-param-reassign
    return ret;
  },
});

const SubregionModel = mongoose.model('Subregion', subregionSchema);

export default SubregionModel;
