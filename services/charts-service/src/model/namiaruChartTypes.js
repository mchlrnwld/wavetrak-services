const namiaruChartTypes = [
  {
    type: 'japanwave',
    name: 'Japan Wave Height',
  },
  {
    type: 'japanwave_tn',
    name: 'Japan Wave Height Thumbnail',
  },
  {
    type: 'japanwind',
    name: 'Japan Wind',
  },
  {
    type: 'japanwind_tn',
    name: 'Japan Wind Thumbnail',
  },
  {
    type: 'japanperiod',
    name: 'Japan Dominant Wave Period',
  },
  {
    type: 'japanperiod_tn',
    name: 'Japan Dominant Wave Period Thumbnail',
  },
  {
    type: 'japansst',
    name: 'Japan Water Temperature',
  },
  {
    type: 'localsst',
    name: 'Local Water Temperature',
  },
];

export default namiaruChartTypes;
