import fetch from 'node-fetch';
import { APIError } from '@surfline/services-common';

// eslint-disable-next-line import/prefer-default-export
export const getLegacyCustomChartTime = async (chartTimeUrl) => {
  const response = await fetch(chartTimeUrl);
  const body = await response.json();
  if (response.status === 200) return body;
  throw new APIError(`Error making request to ${chartTimeUrl}: ${await response.text()}`);
};
