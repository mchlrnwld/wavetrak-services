provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "charts-service/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "charts_service" {
  source = "../../"

  company     = "wt"
  application = "charts-service"
  environment = "staging"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-staging"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678/1b066cae0efbb0a4"
  service_td_count = "1"
  service_lb_rules = [
    {
      field = "host-header"
      value = "charts-service.staging.surfline.com"
    },
  ]
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678"

  auto_scaling_enabled = false

  dns_name    = "charts-service.staging.surfline.com"
  dns_zone_id = "Z3JHKQ8ELQG5UE"
}
