provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "charts-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_route53_zone" "legacy_dns_zone" {
  name         = "surfline.com."
  private_zone = false
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-1-prod"
}

data "aws_route53_zone" "dns_zone" {
  name         = "prod.surfline.com."
  private_zone = false
}

resource "aws_route53_record" "charts_legacy_dns" {
  zone_id = data.aws_route53_zone.legacy_dns_zone.zone_id
  name    = "charts-service.surfline.com"
  type    = "A"

  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = false
  }
}

module "charts_service" {
  source = "../../"

  company     = "wt"
  application = "charts-service"
  environment = "prod"

  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-1-prod/9fed27702f8f890f/05e6f65280962f71"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "charts-service.prod.surfline.com"
    },
    {
      field = "host-header"
      value = "charts-service.surfline.com"
    },
  ]
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-1-prod/9fed27702f8f890f"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 100
  auto_scaling_min_size     = 2
  auto_scaling_max_size     = 5000

  dns_name    = "charts-service.prod.surfline.com"
  dns_zone_id = data.aws_route53_zone.dns_zone.zone_id
}
