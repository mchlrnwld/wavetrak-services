module.exports = {
  extends: '@surfline/eslint-config-services',
  rules: {
    'import/no-extraneous-dependencies': [
      2,
      {
        devDependencies: [
          '**/babel-index.js',
          '**/babel-tests.js',
          '**/tests.js',
          '**/*.spec.js',
          '**/pact/**/*.js',
        ],
      },
    ],
    'prettier/prettier': 'off',
  },
};
