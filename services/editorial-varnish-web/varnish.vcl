vcl 4.0;

acl purge_ban_refresh_acl {
  "localhost";
  "10.0.0.0"/8;
  "172.18.0.0"/16;
}

sub vcl_recv {
  # Setting http headers for backend
  if (req.restarts == 0) {
    if (req.http.X-Forwarded-For) {
      set req.http.X-Forwarded-For =
        req.http.X-Forwarded-For + ", " + client.ip;
    } else {
      set req.http.X-Forwarded-For = client.ip;
    }

    if (req.http.X-Forwarded-Proto) {
      set req.http.X-Forwarded-Proto = req.http.X-Forwarded-Proto;
    } else {
      set req.http.X-Forwarded-Proto = "https";
    }

    # Since we're forced to use HTTP on the WP backend and ELB overwrites
    # the X-Forwarded-Proto header, let's set another header to contain
    # this information. This is a hacky workaround.
    set req.http.X-Forwarded-Proto-Bypass-ELB = req.http.X-Forwarded-Proto;
  }

  # Unset headers that might cause us to cache duplicate infos
  unset req.http.Accept-Language;
  unset req.http.User-Agent;

  # Healthcheck
  if (req.url ~ "\/varnish\/health\/?") {
    return (synth(751, "OK"));
  }

  # If not healthcheck, remove `/varnish` from the url
  # `/varnish` exists in the url to allow for proper routing within ECS.
  # The nginx proxy prepends `/varnish` to the URL, ECS routes accordingly,
  # and Varnish removes `/varnish` from the URL before sending to the backend.
  set req.url = regsub(req.url, "/varnish", "");

  if (req.method == "PURGE") {
    if (!client.ip ~ purge_ban_refresh_acl) {
      return(synth(405, "Not allowed."));
    }
    return(purge);
  }

  # This block allows BAN requests to be applied to Varnish
  # ex. curl -i -X BAN -H 'X-Ban-Url: .*1234.*' http://localhost:8082/
  # ex. curl -i -X BAN -H 'X-Ban-Url: .*1234.*' http://localhost:8082/
  if (req.method == "BAN") {
    if (!client.ip ~ purge_ban_refresh_acl) {
      return(synth(405, "Not allowed."));
    }
    if (req.http.x-ban-url == "/") {
      ban("obj.http.url == /");
    } else {
      ban("obj.http.url ~ " + req.http.x-ban-url);
    }
    return(synth(200, "Banned"));
  }

  # drop cookies and params from static assets
  if (req.url ~ "\.(gif|jpg|jpeg|swf|ttf|css|js|flv|mp3|mp4|pdf|ico|png)(\?.*|)$") {
    unset req.http.cookie;
    set req.url = regsub(req.url, "\?.*$", "");
  }

  # drop tracking params
  if (req.url ~ "\?(utm_(campaign|medium|source|term)|adParams|client|cx|eid|fbid|feed|ref(id|src)?|v(er|iew))=") {
    set req.url = regsub(req.url, "\?.*$", "");
  }

  # pass wp-admin urls
  if (req.url ~ "(wp-login|wp-admin)" || req.url ~ "preview=true" || req.url ~ "xmlrpc.php") {
    return (pass);
  }

  # pass wp contest submit url
  if (req.url ~ "/wp-json/sl-contests/v1/(submit|entries.*)") {
    return (pass);
  }

  # don't worry about wp-admin cookies, we're not routing admin through varnish
  if (req.http.cookie) {
    unset req.http.cookie;
  }

  return (hash);
}

sub vcl_backend_response {
  # Set url for BAN matching
  set beresp.http.url = bereq.url;

  # retry a few times if backend is down
  if (beresp.status == 503 && bereq.retries < 3 ) {
    return (retry);
  }

  if (beresp.ttl <= 0s) {
    # Varnish determined the object was not cacheable
    set beresp.http.X-Cacheable = "NO:Not Cacheable";

  } elsif (beresp.http.Cache-Control ~ "private") {
    # You are respecting the Cache-Control=private header from the backend
    set beresp.http.X-Cacheable = "NO:Cache-Control=private";
    set beresp.uncacheable = true;
    return (deliver);

  } else {
    # Varnish determined the object was cacheable
    set beresp.http.X-Cacheable = "YES";

    # Remove Expires from backend, it's not long enough
    unset beresp.http.expires;

    # Set the clients TTL on this object
    set beresp.http.cache-control = "max-age=900";

    # Set how long Varnish will keep it
    set beresp.ttl = 1h;

    # marker for vcl_deliver to reset Age:
    set beresp.http.magicmarker = "1";
  }

  # unset cookies from backendresponse
  if (!(bereq.url ~ "(wp-login|wp-admin)"))  {
    # set beresp.http.X-UnsetCookies = "TRUE";
    unset beresp.http.set-cookie;
    set beresp.ttl = 1h;
  }

  # long ttl for assets
  if (bereq.url ~ "\.(gif|jpg|jpeg|swf|ttf|css|js|flv|mp3|mp4|pdf|ico|png)(\?.*|)$") {
    set beresp.ttl = 1d;
  }

  set beresp.grace = 1w;
}

sub vcl_hash {
 if ( req.http.X-Forwarded-Proto ) {
   hash_data( req.http.X-Forwarded-Proto );
  }
}

sub vcl_synth {
  # redirect for http
  if (resp.status == 750) {
    set resp.status = 301;
    set resp.http.Location = req.http.x-redir;
    return(deliver);
  } else if (resp.status == 751) {
    # Healthcheck
    set resp.status = 200;
    set resp.http.Content-Type = "plain/text";
    # This should return json, having problems with synthetic
    synthetic({"OK"});
    return(deliver);
  }
}

sub vcl_deliver {
  unset resp.http.url;

  if (resp.status == 503) {
    return(restart);
  }

  if (resp.http.magicmarker) {
    # Remove the magic marker
    unset resp.http.magicmarker;

    # By definition we have a fresh object
    set resp.http.age = "0";
  }

  if (obj.hits > 0) {
    set resp.http.X-Cache = "HIT";
  } else {
    set resp.http.X-Cache = "MISS";
  }
}

sub vcl_hit {
  if (req.method == "PURGE") {
    return(synth(200, "OK"));
  }
}

sub vcl_miss {
  if (req.method == "PURGE") {
    return(synth(404, "Not cached"));
  }
}
