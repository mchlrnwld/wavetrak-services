# Editorial Varnish Web

### File Structure

```bash
├── infra
├── conf
│   └── varnish.vcl
├── infra
│   ├── main.tf
│   ├── variables.tf
│   ├── versions.tf
│   ├── README.MD
│   ├── .terraform-version
│   ├── Makefile
        ├── environments
        ├── prod
        ├── sbox
        ├── staging
```
