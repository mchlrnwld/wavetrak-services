data "aws_route53_zone" "dns_zone" {
  name = "${var.environment}.surfline.com."
}

resource "aws_alb_listener_rule" "varnish_rule" {
  listener_arn = var.alb_listener_arn_https
  priority     = 100

  action {
    target_group_arn = module.editorial_varnish_service.target_group
    type             = "forward"
  }

  condition {
    path_pattern {
      values = ["/varnish/*"]
    }
  }
}

module "editorial_varnish_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = var.company
  application  = var.application
  environment  = var.environment
  service_name = var.application

  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/varnish/health/"

  service_td_name           = "${var.company}-cms-${var.environment}-${var.application}"
  service_td_container_name = "varnish"
  service_td_count          = var.service_td_count
  service_port              = 6081
  service_alb_priority      = 410

  default_vpc = var.default_vpc
  dns_name    = var.dns_name
  dns_zone_id = data.aws_route53_zone.dns_zone.zone_id
  ecs_cluster = "sl-core-svc-${var.environment}"

  alb_listener      = var.alb_listener_arn_https
  iam_role_arn      = var.iam_role_arn
  load_balancer_arn = var.load_balancer_arn

  auto_scaling_enabled        = false
  auto_scaling_scale_by       = "alb_request_count"
  auto_scaling_target_value   = ""
  auto_scaling_min_size       = ""
  auto_scaling_max_size       = ""
  auto_scaling_alb_arn_suffix = ""
}
