provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "surfline-editorial-varnish/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "sl"
  environment = "staging"
  application = "editorial-varnish"

  certificate_arn        = "arn:aws:acm:us-west-1:665294954271:certificate/a926595c-f282-4435-be22-3cfaf907b459"
  iam_role_arn           = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn      = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-3-staging/bca6896a1b354474"
  alb_listener_arn_https = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-3-staging/bca6896a1b354474/a39da1ea44320d7c"

  default_vpc      = "vpc-981887fd"
  dns_name         = "editorial-varnish.staging.surfline.com"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "editorial_varnish_service" {
  source = "../../"

  company      = local.company
  application  = local.application
  environment  = local.environment
  service_name = local.application

  default_vpc      = local.default_vpc
  dns_name         = local.dns_name
  service_lb_rules = local.service_lb_rules
  service_td_count = local.service_td_count

  certificate_arn        = local.certificate_arn
  iam_role_arn           = local.iam_role_arn
  load_balancer_arn      = local.load_balancer_arn
  alb_listener_arn_https = local.alb_listener_arn_https
}
