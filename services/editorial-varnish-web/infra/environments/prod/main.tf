provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surfline-editorial-varnish/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "sl"
  environment = "prod"
  application = "editorial-varnish"

  certificate_arn        = "arn:aws:acm:us-west-1:833713747344:certificate/4c4df1b0-55b5-46e7-8cb6-9ac5d5e1bd2c"
  iam_role_arn           = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn      = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b"
  alb_listener_arn_https = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-3-prod/8dd5625ed91a702b/7d4f5548c2239855"

  default_vpc      = "vpc-116fdb74"
  dns_name         = "editorial-varnish.prod.surfline.com"
  service_td_count = 2
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "editorial_varnish_service" {
  source = "../../"

  company      = local.company
  application  = local.application
  environment  = local.environment
  service_name = local.application

  default_vpc      = local.default_vpc
  dns_name         = local.dns_name
  service_lb_rules = local.service_lb_rules
  service_td_count = local.service_td_count

  certificate_arn        = local.certificate_arn
  iam_role_arn           = local.iam_role_arn
  load_balancer_arn      = local.load_balancer_arn
  alb_listener_arn_https = local.alb_listener_arn_https
}
