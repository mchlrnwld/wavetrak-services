provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "surfline-editorial-varnish/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  company     = "sl"
  environment = "sandbox"
  application = "editorial-varnish"

  certificate_arn        = "arn:aws:acm:us-west-1:665294954271:certificate/b5d003ab-5256-4737-9333-c07e7f817a98"
  iam_role_arn           = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn      = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-3-sandbox/9ff25c35a340093b"
  alb_listener_arn_https = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-3-sandbox/9ff25c35a340093b/e78a34116d637d96"

  default_vpc      = "vpc-981887fd"
  dns_name         = "editorial-varnish.sandbox.surfline.com"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "editorial_varnish_service" {
  source = "../../"

  company      = local.company
  application  = local.application
  environment  = local.environment
  service_name = local.application

  default_vpc      = local.default_vpc
  dns_name         = local.dns_name
  service_lb_rules = local.service_lb_rules
  service_td_count = local.service_td_count

  certificate_arn        = local.certificate_arn
  iam_role_arn           = local.iam_role_arn
  load_balancer_arn      = local.load_balancer_arn
  alb_listener_arn_https = local.alb_listener_arn_https
}
