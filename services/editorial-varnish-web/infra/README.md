# Editorial Varnish service

This module contains _only_ the infrastructure code for the `editorial-varnish` service. The application code can be found at https://github.com/Surfline/surfline-web-editorial.

Instructions for running the infrastructure code can be found within the enclosed `infra` folder here.

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=sbox make plan
ENV=sbox make apply
```

Valid environments currently only include `sbox`, `staging` and `prod`.
