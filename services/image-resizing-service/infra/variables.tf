variable "company" {
  type = string
}

variable "environment" {
  type = string
}

variable "application" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "subnet_ids" {
  type = list(string)
}

variable "s3_bucket" {
  type = string
}

variable "route_zone" {
  type = string
}

variable "route_name" {
  type = string
}

variable "artifacts_bucket" {
  type = string
}
