# image-service Infrastructure

## Summary

Provisions an Application Load Balancer, routing requests to a Lambda function, and grants the Lambda function read access to an S3 bucket.

Additionally, provisions an DNS alias record pointing to the ALB.

## Terraform

_Uses terraform version `0.12.28`._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement it's respective module. See https://www.terraform.io/docs/modules/usage.html for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=msw make plan
ENV=msw make apply
```

Only valid environment is `msw`.
