provider "aws" {
  region = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "msw-tf-state-msw"
    key    = "image-resizing-service/msw/terraform.tfstate"
    region = "us-east-1"
  }
}

data "aws_vpc" "resizer" {
  tags = {
    Name = "msw-imageloader"
  }
}

data "aws_subnet_ids" "resizer" {
  vpc_id = data.aws_vpc.resizer.id
}

module "image_service" {
  source = "../../"

  company     = "msw"
  application = "image-resize-service"
  environment = "msw"

  vpc_id           = data.aws_vpc.resizer.id
  subnet_ids       = data.aws_subnet_ids.resizer.ids
  s3_bucket        = "msw-contributions"
  artifacts_bucket = "msw-artifacts"
  route_zone       = "magicseaweed.com."
  route_name       = "msw-image-service.magicseaweed.com"
}
