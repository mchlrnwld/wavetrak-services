resource "aws_iam_role" "fn_role" {
  name               = "${var.company}-lambda-role-${var.application}-${var.environment}"
  assume_role_policy = file("${path.module}/resources/lambda-assume-role-policy.json")
}

resource "aws_iam_role_policy" "allow_s3_access" {
  name = "${var.company}-s3-access-${var.application}-${var.environment}"
  role = aws_iam_role.fn_role.id
  policy = templatefile("${path.module}/resources/lambda-role-policy.json", {
    bucket_name = var.s3_bucket
  })
}

resource "aws_lambda_function" "stub_fn" {
  function_name = "${var.company}-${var.application}-resize-fn-${var.environment}"
  s3_bucket     = var.artifacts_bucket
  s3_key        = "lambda-functions/${var.application}-resize-fn/${var.company}-${var.application}-resize-fn.zip"
  handler       = "src/handler.handler"
  runtime       = "nodejs10.x"
  role          = aws_iam_role.fn_role.arn
  memory_size   = 256
  timeout       = 15

  environment {
    variables = {
      s3_bucket = var.s3_bucket
    }
  }
}

resource "aws_lb_target_group" "tg" {
  name        = "${var.company}-tg-${var.application}-${var.environment}"
  target_type = "lambda"
}

resource "aws_lambda_permission" "allow_alb" {
  statement_id  = "AllowExecutionFromlb"
  action        = "lambda:InvokeFunction"
  principal     = "elasticloadbalancing.amazonaws.com"
  function_name = aws_lambda_function.stub_fn.arn
  source_arn    = aws_lb_target_group.tg.arn
}

resource "aws_lb_target_group_attachment" "link_fn_group" {
  target_group_arn = aws_lb_target_group.tg.arn
  target_id        = aws_lambda_function.stub_fn.arn
  depends_on       = [aws_lambda_permission.allow_alb]
}

resource "aws_security_group" "allow_http" {
  name        = "${var.company}-sg-${var.application}-${var.environment}"
  description = "Allow HTTP (80) inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "lb" {
  name               = "${var.company}-alb-${var.application}-${var.environment}"
  load_balancer_type = "application"
  security_groups    = [aws_security_group.allow_http.id]
  subnets            = var.subnet_ids
  internal           = false
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.lb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tg.arn
  }
}

data "aws_route53_zone" "selected" {
  name = var.route_zone
}

resource "aws_route53_record" "route" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.route_name
  type    = "A"

  alias {
    name                   = aws_lb.lb.dns_name
    zone_id                = aws_lb.lb.zone_id
    evaluate_target_health = true
  }
}
