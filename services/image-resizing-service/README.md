# image-resizing-service

Serves images from an S3 bucket, resized as requested.

## Development

Service is an [Express](https://expressjs.com/) app. There are three ways in which it can be run:

- Locally in the usual way;
- deployed to AWS lambda, utilising [aws-serverless-express](https://github.com/awslabs/aws-serverless-express); or
- via integration tests, utilising [supertest](https://github.com/visionmedia/supertest) and [Jasmine](https://jasmine.github.io/).

To run service locally:

```
s3_bucket=BUCKET_NAME npm run start
```

Rather than specifying on the command line, you can create a `.env` file containing `s3_bucket=BUCKET_NAME`.

Run integration tests with:

```
npm run test
```

This requires access to the test bucket `msw-contributions-test`.

## Files

```
.
├── infra/          // Terraform infrastructure configuration
├── package.json
├── package-lock.json
├── README.md
├── spec/           // Integration test runner, specs, and test fixtures
│   ├── run.js      // Entry point for integration tests
│   └── ...
└── src
    ├── config.js   // Configuration regarding re-writing legacy MSW image URLs
    ├── handler.js  // Entry point for AWS Lambda function
    ├── index.js    // Entry point for local development
    ├── middleware
    │   ├── bounceCacheValidation.js
    │   ├── imagePhp
    │   │   ├── rewriteResizeType.js
    │   │   └── rewriteType.js
    │   ├── proxyImage.js
    │   └── rewriteLegacyURLs.js
    └── router.js
```
