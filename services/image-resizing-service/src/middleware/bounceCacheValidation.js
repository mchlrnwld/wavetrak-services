/**
 * Expressjs middleware to check if the request has cache headers indicating the client is looking
 * for an update. Images are immutable, so 304 if so.
 *
 * @see https://expressjs.com/en/guide/writing-middleware.html
 */
module.exports = function (req, res, next) {
  if (req.get('If-None-Match') || req.get('If-Modified-Since')) {
    // Responses never change, so if either header is present we're done.
    res.status(304).send();
  } else {
    next();
  }
};
