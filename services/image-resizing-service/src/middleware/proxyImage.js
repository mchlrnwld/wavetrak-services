const aws = require('aws-sdk'),
  s3 = new aws.S3(),
  sharp = require('sharp'),
  tryInt = x => parseInt(x) || null,
  tryBool = b => typeof b === 'boolean' ? b : b === 'true';

/**
 * Middleware to serve resized images. Proxies the reqest path to the upstream source,
 * then resizes and sends the return image on to the client.
 *
 * @see https://expressjs.com/en/guide/writing-middleware.html
 */
module.exports = async function proxyImage (req, res) {
  const accept = req.get('accept') || '',
    format = accept.match(/image\/webp/) ? 'webp' : 'jpeg',
    path = req.path.replace(/^\//, ''),
    w = tryInt(req.query.width),
    h = tryInt(req.query.height),
    stretch = tryBool(req.query.resizeUp),
    q = tryInt(req.query.quality) || 85;

  if (!path) {
    return res.status(400).send('Path required');
  } else if (!w && !h) {
    return res.status(400).send('At least one of width or height required');
  }

  var image;

  try {
    image = await s3.getObject({
      Bucket: process.env.s3_bucket,
      Key: path
    }).promise();

    image = await resize(image.Body, format, w, h, stretch, q);
  } catch (err) {
    if (err.statusCode === 404) {
      // Not found in S3
      return res.status(404).send('Image not found upstream');
    }

    return res.status(500).send(err.message);
  }

  res.status(200)
    .type('image/'+format)
    .set({
      // Let caching proxies know we vary the response based on the accept header
      'Vary': 'Accept',
      // public, cache for a year, never changes
      'Cache-Control': 'public, max-age=31536000, immutable'
    });

  res.send(image);
};

function resize (buffer, format, width, height, resizeUp, quality) {
  const image = sharp(buffer);

  // resize will preseve aspect ratio if one or other of height or which is null, and will
  // shrink and crop if both are provided.
  return image
    .resize(width, height, {
      withoutEnlargement: resizeUp !== true
    })
    .toFormat(format, {
      quality: quality
    }).toBuffer();
}
