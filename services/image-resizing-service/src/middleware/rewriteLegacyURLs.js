const router = require('express').Router(),
  config = require('../config.js'),
  legacyUpstream = config.legacyUpstream.endsWith('/') ? config.legacyUpstream.substring(0, config.legacyUpstream.length - 1) : config.legacyUpstream,
  rewriteType = require('./imagePhp/rewriteType.js'),
  rewriteResizeType = require('./imagePhp/rewriteResizeType.js');

// These attempt to rewrite URLs such as
// `/md/image.php?type=PHOTOLAB&id=8&resize_type=FEATURED_THUMB`
router.get('/md/image.php', [
  // Convert, e.g., `/md/image.php?type=PHOTOLAB&id=8` to `/photos/8.jpg`
  rewriteType,
  // Convert, e.g., `?resize_type=FEATURED_THUMB` to `?width=240&height=140`
  rewriteResizeType
]);

// Any lingering `/md/` based URLs could be _anything_. Redirect back to MSW.
router.use('/md/', (req, res) => res.redirect(302, legacyUpstream+req.baseUrl+req.url));

module.exports = router;
