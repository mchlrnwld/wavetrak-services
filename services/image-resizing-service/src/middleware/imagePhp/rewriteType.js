const config = require('../../config.js'),
  legacyUpstream = config.legacyUpstream.endsWith('/') ? config.legacyUpstream.substring(0, config.legacyUpstream.length - 1) : config.legacyUpstream,
  moment = require('moment');

/**
 * Checks the `type` query parameter, and redirects to the legacy
 * image loaders if we can't handle it here, otherwise rewitres
 * the path to a cononical form.
 */
module.exports = function redirectUnknownTypes (req, res, next) {
  const type = req.query.type;

  if (type && !config.supportedTypes[type.toLowerCase()]) {
    // If type is defined, but we don't know about it, redirect to the legacy imageloader
    return res.redirect(302, legacyUpstream+req.url);
  }

  var path;

  // Try to conver the /md/image.php?PARAMS to a proper path
  try {
    path = getPath(req.query);
  } catch (err) {
    return res.status(400).send(err.message);
  }

  req.url = path;

  next();
};

/**
 * Convert query paramteters to an image path
 *
 * @param {Object} query  The parameters
 *
 * @return {String}     The image path
 */
function getPath (query) {
  var type = query.type && query.type.toLowerCase(),
    id = query.id  || query.key,
    basePath = config.supportedTypes[type];

  if (typeof id !== 'string' || typeof basePath !== 'string') {
    throw new Error('Bad `type` and/or `id` or `key` fields');
  }

  if (type === 'netcam_archive' || type === 'netcam_panoramic') {
    if (typeof query.netcam_id === 'undefined') {
      throw new Error('`netcam_id` required for netcam type images');
    }

    var dt = moment.unix(+id);

    basePath += '/'+query.netcam_id+dt.format('/YYYY-MM-DD');

    if (type === 'netcam_panoramic') {
      id += '-panoramic';
    }
  }

  return '/'+basePath+'/'+id+'.jpg';
}
