const config = require('../../config.js');

/**
 * Re-writes the legacy `resize_type` query param to cannoical values
 */
module.exports = function rewriteUrl (req, res, next) {
  // Add the resize params to the query
  const params = req.query.resize_type && config.resizeTypes[req.query.resize_type];

  if (typeof params === 'undefined') {
    res.status(400).send('Unrecognised resize_type');

    return;
  }

  Object.entries(params).forEach(([key, val]) => req.query[key] = val);

  // Having converted resize_type to cononical params, remove it. (Not strictly required
  // to remove, but enforces that later middleware does not peek at this value.)
  delete req.query.resize_type;

  next();
};

