module.exports = {
  legacyUpstream: 'http://images.magicseaweed.com/',
  supportedTypes: {
    photolab: 'photos',
    netcam_archive: 'netcam-stills',
    netcam_panoramic: 'netcam-stills'
  },
  resizeTypes: {
    AVATAR_TINY_FLUID: {
      width: 140,
      resizeUp: true
    },
    AVATAR_SMALL_FLUID: {
      width: 320,
      resizeUp: true
    },
    AVATAR_LARGE_FLUID: {
      width: 640
    },
    AVATAR_TINY: {
      width: 140,
      height: 140,
      resizeUp: true
    },
    AVATAR_SMALL: {
      width: 320,
      height: 320,
      resizeUp: true
    },
    AVATAR_LARGE: {
      width: 640,
      height: 640,
      resizeUp: true
    },
    CHANNEL_TEASER: {
      width: 300,
      height: 175,
      resizeUp: true
    },
    SQUARE_THUMB_CROPPED: {
      width: 70,
      height: 70
    },
    SQUARE_175_CROPPED: {
      width: 175,
      height: 175
    },
    POTD_THUMB: {
      width: 320,
      height: 160
    },
    SQUARE_FEATURED_CROPPED_RETINA: {
      width: 620,
      height: 230
    },
    SQUARE_FEATURED_CROPPED: {
      width: 310,
      height: 115
    },
    FEATURED_THUMB: {
      width: 240,
      height: 140
    },
    GRID_7: {
      width: 400,
      height: 225
    },
    SIZE_SMALL_THUMB_CROPPED: {
      width: 40,
      height: 25
    },
    STREAM_SMALL: {
      width: 140,
      height: 140
    },
    STREAM_MEDIUM: {
      width: 640
    },
    STREAM_MEDIUM_SQUARE: {
      width: 640,
      height: 640,
      resizeUp: true
    },
    STREAM_LARGE: {
      width: 1280
    },
    STREAM_FULL: {
      width: 1920
    },
    STREAM_CAROUSEL: {
      width: 418,
      height: 210,
      resizeUp: true
    },
    COVER: {
      width: 2320,
      height: 1222,
      resizeUp: true
    },
    COVER_MEDIUM: {
      width: 1400,
      height: 750,
      resizeUp: true
    },
    COVER_MOBILE: {
      width: 640,
      height: 337,
      resizeUp: true
    },
    LISTING_SMALL: {
      width: 200,
      height: 200,
      resizeUp: true
    },
    LISTING_MEDIUM: {
      width: 550,
      height: 550,
      resizeUp: true
    },
    LISTING_FULL: {
      width: 550,
      resizeUp: true
    },
    STORM_WARNING_SMALL: {
      width: 200,
      height: 200,
      resizeUp: true
    },
    STORM_WARNING_MEDIUM: {
      width: 550,
      height: 550,
      resizeUp: true
    },
    STORM_WARNING_FULL: {
      width: 550,
      resizeUp: true
    },
    STORM_WARNING_TEASER: {
      width: 500,
      height: 292,
      resizeUp: true
    },
    PHOTO_LARGE: {
      width: 1280,
      resizeUp: true
    },
    PHOTO_FULL: {
      width: 1920,
      resizeUp: true
    },
    PHOTOLAB_FULL: {
      width: 1920
    },
    PANORAMIC_LARGE: {
      height: 500
    },
    PANORAMIC_MEDIUM: {
      height: 380
    },
    PANORAMIC_SMALL: {
      height: 260
    },
    CAMPAIGN_SMALL: {
      width: 200,
      height: 200,
      resizeUp: true
    },
    CAMPAIGN_MEDIUM: {
      width: 550,
      height: 550,
      resizeUp: true
    },
    CAMPAIGN_FULL: {
      width: 550,
      resizeUp: true
    },
    CAMPAIGN_TEASER: {
      width: 500,
      height: 292,
      resizeUp: true
    },
    LEGACY_FULL: {
      width: 770,
      resizeUp: true
    },
    LEGACY_FULL_WIDTH: {
      width: 990,
      resizeUp: true
    },
    LEGACY_THUMB: {
      width: 150,
      height: 100
    }
  }
};
