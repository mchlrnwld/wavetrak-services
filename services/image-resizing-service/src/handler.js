/**
 * AWS Lambda handler to run Express app
 */

if (typeof process.env.s3_bucket !== 'string') {
  throw new Error('s3_bucket env variable must be set');
}

const express = require('express'),
  morgan = require('morgan'),
  router = require('./router.js'),
  app = express();

app.use(morgan('combined'));
app.use(router);

const awsServerlessExpress = require('aws-serverless-express'),
  server = awsServerlessExpress.createServer(app, null, ['image/jpeg', 'image/webp']);

exports.handler = (event, context) => {
  awsServerlessExpress.proxy(server, event, context);
};

