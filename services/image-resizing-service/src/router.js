const express = require('express'),
  bounceCacheValidation = require('./middleware/bounceCacheValidation.js'),
  rewriteLegacyURLs = require('./middleware/rewriteLegacyURLs.js'),
  proxyImage = require('./middleware/proxyImage.js'),
  router = express.Router();

// Check the cache headers and 304 if looking for an update. Everything
// we're serving here is immutable.
router.use(bounceCacheValidation);

// Re-write legacy MSW image URLs into a cannoncal form, (or redirect to
// MSW for anything we can't handle)
router.use(rewriteLegacyURLs);

// Proxy image from the upstream location, resizing as requested
router.use(proxyImage);

module.exports = router;
