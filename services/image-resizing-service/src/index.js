/**
 * Run Express app, for local development puroposes
 */

require('dotenv').config();

if (typeof process.env.s3_bucket !== 'string') {
  throw new Error('s3_bucket env variable must be set');
}

var express = require('express'),
  morgan = require('morgan'),
  router = require('./router.js'),
  app = express();

app.use(morgan('dev'));
app.use(router);

// Listen for connections
app.listen(3001, () => console.log('listening on port 3001'));
