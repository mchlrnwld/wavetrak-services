const Jasmine = require('jasmine'),
  jasmine = new Jasmine(),
  aws = require('aws-sdk'),
  s3 = new aws.S3(),
  fs = require('fs');

if (typeof process.env.s3_bucket !== 'string' || !/test/.test(process.env.s3_bucket)) {
  console.error('s3_bucket env variable should be set to bucket to which you have access, suitable for testing');
  process.exit(1);
}

// Ensure all test assets are in S3 before staring
return Promise.all([
  'photos/8.jpg',
  'netcam-stills/99/2017-01-31/1485885600-panoramic.jpg',
  'netcam-stills/99/2017-01-31/1485889200.jpg'
].map((key) => {
  try {
    return s3.putObject({
      Body: Buffer.from(fs.readFileSync('./spec/fixtures/'+key)),
      Bucket: process.env.s3_bucket,
      Key: key,
    }).promise();
  } catch (e) {
    // readFileSync may throw an error
    return Promise.reject(e);
  }
})).catch((e) => {
  // on any errors, bail
  console.error('Unable to write test assets to S3 bucket `'+process.env.s3_bucket+'`');
  console.error('Error:', e);
  process.exit(1);
}).then(() => {
  // All good, proceed
  jasmine.loadConfigFile('spec/support/jasmine.json');
  jasmine.execute();
});

