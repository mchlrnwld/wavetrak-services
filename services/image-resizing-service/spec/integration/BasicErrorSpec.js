var request = require('supertest'),
  app = require('express')();

app.use(require('../../src/router.js'));

describe('basic bad requests', () => {
  it('should give a 404 response if image not found upstream', () =>
    request(app)
      .get('/photos/99999099999.jpg?width=100')
      .expect(404)
  );

  it('should give a 400 response if neither `width` nor `height` is supplied', () =>
    request(app)
      .get('/photos/8.jpg')
      .expect(400)
  );
});
