var request = require('supertest'),
  app = require('express')(),
  sharp = require('sharp'),
  config = require('../../src/config.js');

app.use(require('../../src/router.js'));

describe('MSW legacy "happy case" request', () => {
  it('should give a 200 response', () => {
    return request(app)
      .get('/md/image.php?type=PHOTOLAB&id=8&resize_type=FEATURED_THUMB')
      .expect(200);
  });

  it('should set the assorted http headers', () => {
    return request(app)
      .get('/md/image.php?type=PHOTOLAB&id=8&resize_type=FEATURED_THUMB')
      .expect('content-type', 'image/jpeg')
      .expect('cache-control', 'public, max-age=31536000, immutable')
      .expect('vary', 'Accept');
  });

  it('should return a jpeg of correct size', () => {
    // Attempt to process the request body as an image. toBuffer returns a promise which will
    // reject (causing the test to fail) if the body is not an image.
    return request(app)
      .get('/md/image.php?type=PHOTOLAB&id=8&resize_type=FEATURED_THUMB')
      .then(res => sharp(res.body).toBuffer({resolveWithObject: true}))
      .then(({info}) => {
        expect(info.format).toEqual('jpeg');
        expect(info.height).toEqual(config.resizeTypes.FEATURED_THUMB.height);
        expect(info.width).toEqual(config.resizeTypes.FEATURED_THUMB.width);
      });
  });

  it('should not scale up images if resize_type does not have `resizeUp: true`', () => {
    return request(app)
      .get('/md/image.php?type=PHOTOLAB&id=8&resize_type=PHOTOLAB_FULL')
      .then(res => sharp(res.body).toBuffer({resolveWithObject: true}))
      .then(({info}) => {
        // 990 is the original image size, PHOTOLAB_FULL has width 1920, but should not
        // resize up.
        expect(info.width).toEqual(990);
      });
  });

  it('with type=NETCAM_ARCHIVE should find an image', () =>
    request(app)
      .get('/md/image.php?type=NETCAM_ARCHIVE&id=1485889200&netcam_id=99&resize_type=GRID_7')
      .expect(200)
      .then(({body}) => sharp(body).toBuffer({resolveWithObject: true}) )
      .then(({info}) => {
        expect(info.height).toEqual(config.resizeTypes.GRID_7.height);
        expect(info.width).toEqual(config.resizeTypes.GRID_7.width);
      })
  );

  it('with type=NETCAM_PANORAMIC should find an image', () =>
    request(app)
      .get('/md/image.php?type=NETCAM_PANORAMIC&id=1485885600&netcam_id=99&resize_type=PANORAMIC_LARGE')
      .expect(200)
      .then(({body}) => sharp(body).toBuffer({resolveWithObject: true}) )
  );
});
