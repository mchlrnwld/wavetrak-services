var request = require('supertest'),
  app = require('express')(),
  config = require('../../src/config.js'),
  legacyUpstream = config.legacyUpstream;

app.use(require('../../src/router.js'));

if (legacyUpstream.endsWith('/')) {
  legacyUpstream = legacyUpstream.substring(0, legacyUpstream.length - 1);
}

describe('request to /md/image.php', () => {
  it('fails with no query params', () => {
    return request(app)
      .get('/md/image.php')
      .expect(400, 'Bad `type` and/or `id` or `key` fields');
  });

  it('fails with type=PHOTOLAB and no id', () => {
    return request(app)
      .get('/md/image.php?type=PHOTOLAB&resize_type=FEATURED_THUMB')
      .expect(400, 'Bad `type` and/or `id` or `key` fields');
  });

  it('fails with type=NETCAM_ARCHIVE and no netcam_id', () => {
    return request(app)
      .get('/md/image.php?type=NETCAM_ARCHIVE&id=1485889200&resize_type=GRID_7')
      .expect(400, '`netcam_id` required for netcam type images');
  });

  it('redirects for types it can\'t handle', () => {
    return request(app)
      .get('/md/image.php?type=FOOBAR&id=1485889200&resize_type=GRID_7')
      .expect(302)
      .expect('location', legacyUpstream+'/md/image.php?type=FOOBAR&id=1485889200&resize_type=GRID_7');
  });

  it('should give a 400 response for bad resize_type', () =>
    request(app)
      .get('/md/image.php?type=PHOTOLAB&resize_type=REALLY_REALL_VERY_SMALL')
      .expect(400)
  );
});
