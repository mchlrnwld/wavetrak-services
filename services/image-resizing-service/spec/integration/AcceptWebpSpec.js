var request = require('supertest'),
  app = require('express')(),
  sharp = require('sharp');

app.use(require('../../src/router.js'));

describe('request with \'Accept: webp\'', () => {
  it('should give a 200 response', () => {
    return request(app)
      .get('/photos/8.jpg?width=240&height=140')
      .set('accept', 'image/*,image/webp,*.*')
      .expect(200);
  });

  it('should set the assorted http headers', () => {
    return request(app)
      .get('/photos/8.jpg?width=240&height=140')
      .set('accept', 'image/*,image/webp,*.*')
      .expect('content-type', 'image/webp')
      .expect('cache-control', 'public, max-age=31536000, immutable')
      .expect('vary', 'Accept');
  });

  it('should return a webp of correct size', () => {
    // Attempt to process the request body as an image. toBuffer returns a promise which will
    // reject (causing the test to fail) if the body is not an image.
    return request(app)
      .get('/photos/8.jpg?width=240&height=140')
      .set('accept', 'image/*,image/webp,*.*')
      .then(res => sharp(res.body).toBuffer({resolveWithObject: true}))
      .then(({info}) => {
        expect(info.format).toEqual('webp');
        expect(info.height).toEqual(140);
        expect(info.width).toEqual(240);
      });
  });
});
