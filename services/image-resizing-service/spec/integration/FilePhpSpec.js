var request = require('supertest'),
  app = require('express')(),
  config = require('../../src/config.js'),
  legacyUpstream = config.legacyUpstream;

app.use(require('../../src/router.js'));

if (legacyUpstream.endsWith('/')) {
  legacyUpstream = legacyUpstream.substring(0, legacyUpstream.length - 1);
}

describe('request to /md/file.php', () => {
  it('302 redirects', () =>
    request(app)
      .get('/md/file.php?key=abcd&type=CAMPAIGN')
      .expect(302)
      .expect('location', legacyUpstream+'/md/file.php?key=abcd&type=CAMPAIGN')
  );
});
