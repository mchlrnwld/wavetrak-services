var request = require('supertest'),
  app = require('express')(),
  sharp = require('sharp');

app.use(require('../../src/router.js'));

describe('"happy case" request', () => {
  it('should give a 200 response', () => {
    return request(app)
      .get('/photos/8.jpg?width=240&height=140')
      .expect(200);
  });

  it('should set the assorted http headers', () => {
    return request(app)
      .get('/photos/8.jpg?width=240&height=140')
      .expect('content-type', 'image/jpeg')
      .expect('cache-control', 'public, max-age=31536000, immutable')
      .expect('vary', 'Accept');
  });

  it('should return a jpeg of correct size', () => {
    // Attempt to process the request body as an image. toBuffer returns a promise which will
    // reject (causing the test to fail) if the body is not an image.
    return request(app)
      .get('/photos/8.jpg?width=240&height=140')
      .then(res => sharp(res.body).toBuffer({resolveWithObject: true}))
      .then(({info}) => {
        expect(info.format).toEqual('jpeg');
        expect(info.height).toEqual(140);
        expect(info.width).toEqual(240);
      });
  });

  it('should not scale up images if `resizeUp` not specified', () => {
    return request(app)
      .get('/photos/8.jpg?width=1920')
      .then(res => sharp(res.body).toBuffer({resolveWithObject: true}))
      .then(({info}) => {
        // 990 is the original image size
        expect(info.width).toEqual(990);
      });
  });

  it('should scale up images if `resizeUp` set true', () => {
    return request(app)
      .get('/photos/8.jpg?width=1920&resizeUp=true')
      .then(res => sharp(res.body).toBuffer({resolveWithObject: true}))
      .then(({info}) => {
        expect(info.width).toEqual(1920);
      });
  });
});
