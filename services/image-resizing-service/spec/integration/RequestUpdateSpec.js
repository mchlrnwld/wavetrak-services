var request = require('supertest'),
  app = require('express')();

app.use(require('../../src/router.js'));

describe('request for updated asset', () => {
  it('with if-modified-since should 304', () =>
    request(app)
      .get('/photos/8.jpg?width=240')
      .set('If-Modified-Since', 'Wed, 21 Oct 2015 07:28:00 GMT')
      .expect(304)
  );

  it('with if-none-match should 304', () =>
    request(app)
      .get('/photos/8.jpg?width=240')
      .set('If-None-Match', 'bfc13a64729c4290ef5b2c2730249c88ca92d82d')
      .expect(304)
  );
});
