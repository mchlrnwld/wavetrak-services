variable "application" {
  type    = string
  default = "surf-ratings-api"
}

variable "environment" {
  type        = string
  description = "Environment name (sandbox or prod)."
}

variable "company" {
  type        = string
  default     = "wt"
  description = "Company name (ex: wt)"
}

variable "default_vpc" {
  type = string
}

variable "ecs_cluster" {
  type        = string
  description = "ECS cluster to create application service."
}

variable "alb_listener_arn" {
  type        = string
  description = "Application load balancer listener ARN to load balance requests to service."
}

variable "service_lb_rules" {
  type        = list(map(string))
  description = "List of load balancer rules."
}

variable "iam_role_arn" {
  type        = string
  description = "ECS service IAM role ARN."
}

variable "dns_name" {
  type        = string
  description = "DNS name for service."
}

variable "dns_zone_id" {
  type        = string
  description = "DNS zone to create DNS record in."
}

variable "load_balancer_arn" {
  type        = string
  description = "Application load balancer ARN to load balance requests to service."
}

variable "auto_scaling_min_size" {
  type        = number
  description = "Minimum instance count."
}

# New Relic apdex alert variables
variable "newrelic_apdex_create_alert" {
  description = "Create New Relic apdex alert"
}

# New Relic error rate alert variables
variable "newrelic_error_rate_create_alert" {
  description = "Create New Relic error rate alert"
}
