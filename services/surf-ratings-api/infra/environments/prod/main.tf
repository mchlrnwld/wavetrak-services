provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/prod/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/prod/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "surf-ratings-api/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  dns_name = "surf-ratings-api.prod.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "surf_ratings_api" {
  source = "../../"

  environment      = "prod"
  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-4-prod/689f354baf4e976c/d6a5a0222c6ea0c3"
  service_lb_rules = local.service_lb_rules
  iam_role_arn     = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"

  dns_name          = local.dns_name
  dns_zone_id       = "Z3LLOZIY0ZZQDE"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-4-prod/689f354baf4e976c"

  auto_scaling_min_size = 4

  newrelic_apdex_create_alert      = true
  newrelic_error_rate_create_alert = true
}
