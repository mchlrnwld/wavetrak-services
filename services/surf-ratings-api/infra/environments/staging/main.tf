provider "aws" {
  region = "us-west-1"
}

data "aws_ssm_parameter" "newrelic_account_id" {
  name = "/staging/common/NEW_RELIC_ACCOUNT_ID"
}

data "aws_ssm_parameter" "newrelic_api_key" {
  name = "/staging/common/NEW_RELIC_API_KEY"
}

provider "newrelic" {
  region     = "US"
  account_id = data.aws_ssm_parameter.newrelic_account_id.value
  api_key    = data.aws_ssm_parameter.newrelic_api_key.value
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "surf-ratings-api/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

locals {
  dns_name = "surf-ratings-api.staging.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
  ]
}

module "surf_ratings_api" {
  source = "../../"

  environment      = "staging"
  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-staging"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-staging/26ee81426b4723db/a6bb3e305cea13f5"
  service_lb_rules = local.service_lb_rules
  iam_role_arn     = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"

  dns_name          = local.dns_name
  dns_zone_id       = "Z3JHKQ8ELQG5UE"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-4-staging/26ee81426b4723db"

  newrelic_apdex_create_alert      = true
  newrelic_error_rate_create_alert = true
}
