locals {
  newrelic_app_name    = "Wavetrak - Surf Ratings API"
  newrelic_policy_name = "surf-ratings-api-${var.environment}"
}

module "surf_ratings_api" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = "wt"
  application  = var.application
  environment  = var.environment
  service_name = var.application

  #  NOTE: TorchServe provides /ping endpoint for health check, so we are breaking the standard /health pattern here
  #  https://pytorch.org/serve/inference_api.html#health-check-api
  service_lb_rules                  = var.service_lb_rules
  service_lb_healthcheck_path       = "/ping"
  service_td_name                   = "sl-core-svc-${var.environment}-${var.application}"
  service_td_count                  = 1
  service_td_container_name         = var.application
  health_check_grace_period_seconds = 60

  service_alb_priority = 451
  service_port         = 8080

  default_vpc = var.default_vpc
  dns_name    = var.dns_name
  dns_zone_id = var.dns_zone_id
  ecs_cluster = var.ecs_cluster

  alb_listener      = var.alb_listener_arn
  iam_role_arn      = var.iam_role_arn
  load_balancer_arn = var.load_balancer_arn

  # TODO: tune the autoscaling for this service
  auto_scaling_enabled        = true
  auto_scaling_scale_by       = "alb_request_count"
  auto_scaling_target_value   = 4000
  auto_scaling_min_size       = var.auto_scaling_min_size
  auto_scaling_max_size       = 5000
  auto_scaling_alb_arn_suffix = "${element(split("/", var.load_balancer_arn), 1)}/${element(split("/", var.load_balancer_arn), 2)}/${element(split("/", var.load_balancer_arn), 3)}"

  newrelic_app_name                = local.newrelic_app_name
  newrelic_policy_name             = local.newrelic_policy_name
  newrelic_apdex_create_alert      = var.newrelic_apdex_create_alert
  newrelic_error_rate_create_alert = var.newrelic_error_rate_create_alert
}

resource "newrelic_alert_policy" "newrelic_surf_ratings_api_policy" {
  name = local.newrelic_policy_name
}

data "aws_ssm_parameter" "newrelic_integration_key" {
  name = "/${var.environment}/pagerduty/surf-ratings-api/newrelic-integration-key"
}

resource "newrelic_alert_channel" "pagerduty_channel" {
  name = "pagerduty-surf-ratings-api"
  type = "pagerduty"

  config {
    service_key = data.aws_ssm_parameter.newrelic_integration_key.value
  }
}

resource "newrelic_alert_policy_channel" "newrelic_surf_ratings_api_channel" {
  policy_id = newrelic_alert_policy.newrelic_surf_ratings_api_policy.id
  channel_ids = [
    newrelic_alert_channel.pagerduty_channel.id
  ]
}
