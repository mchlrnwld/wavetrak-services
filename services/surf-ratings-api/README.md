# Surf Ratings API 

## Files
  - `ci.json` - configuration of CI/CD pipeline
  - `config.properties` - configuration of the surf ratings API
  - `docker-compose.integration.yml` - compose file to run integration test container
  - `docker-compose.test.yml` - compose file to run application for integration testing
  - `Dockerfile` - docker image blueprint of the surf ratings API
  - `environment.devenv.yml` - Conda environment for creating PyTorch model archives or running tests locally
  
## Running
The surf ratings API uses [TorchServe](https://pytorch.org/serve/) to manage and run PyTorch machine learning models related to surf ratings. 

1. Create docker image:
    ```sh
    $ docker build -t my_surf_ratings_api .
    ```
2. [Run a TorchServe docker container](https://github.com/pytorch/serve/tree/master/docker#running-torchserve-in-a-production-docker-environment):
    ```sh
    $ docker run -d --rm -p8080:8080 -p8081:8081 -p8082:8082 my_surf_ratings_api
    ```
3. [Check the health](https://pytorch.org/serve/inference_api.html#health-check-api) of the service and initially loaded workers:
    ```sh
    $ curl "http://localhost:8080/ping"
    ```
   
## Packaging models for registration in the API
The [torch-model-archiver](https://github.com/pytorch/serve/tree/master/model-archiver#torch-model-archiver-for-torchserve) is used to package models for TorchServe.

1. Minimum files:
- `my_model.pth` - Required PyTorch model file
- `my_nn_module.py` - PyTorch [neural network module](https://pytorch.org/tutorials/beginner/basics/buildmodel_tutorial.html#define-the-class) (required for eager mode)
- `requirements.txt` - optional custom Python dependencies for the model [handler](https://pytorch.org/serve/custom_service.html)
- `my_model_handler.py` - model [handler](https://pytorch.org/serve/custom_service.html) (optional custom request handling, preprocessing, postprocessing for this model)
   
2. [Run the torch-model-archiver](https://github.com/pytorch/serve/tree/master/model-archiver#torch-model-archiver-for-torchserve) to create the `.mar` archive file, for example:
    ```sh
    $ torch-model-archiver \
        --model-name my_model \
        --version 1.0 \
        --serialized-file my_model.pth \
        --handler my_model_handler \
        --model-file my_neural_network_module.py \
        --extra-files "utils.py" \
        --requirements-file "requirements.txt"
    ```

## Adding new models to the API
1. Add your new `.mar` file name (or remote location) to the `load_models` list in the `config.properties` file so it will be [loaded on startup](https://pytorch.org/serve/configuration.html#load-models-at-startup).
2. [Add your new model configuration](https://pytorch.org/serve/configuration.html#config-model):
    ```sh
    models={\
      ...
      "my_new_model_name": {\
        "1.0": {\
          "defaultVersion": true,\
          "marName": "https://surfline-science-labs.s3.us-west-1.amazonaws.com/models/my_new_model_name.mar",\
          "minWorkers": 2,\
          "maxWorkers": 4,\
          "batchSize": 4,\
          "maxBatchDelay": 100,\
          "responseTimeout": 120\
        }\
      }\      
    }
    ```
   
## Monitoring
[Install New Relic Java Agent using Docker](https://docs.newrelic.com/docs/apm/agents/java-agent/additional-installation/install-new-relic-java-agent-docker/)

## Testing
...
