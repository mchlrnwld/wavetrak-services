import logging
import os
import backoff
import requests

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())
logging.getLogger('backoff').addHandler(logging.StreamHandler())


@backoff.on_exception(
    backoff.constant,
    requests.exceptions.RequestException,
    interval=1,
    max_time=60,
    jitter=None
)
def wait_for_torchserve_initialization():
    requests.get(f"{os.environ['SURF_RATINGS_API']}/ping")
