# PyTorch Model Testing
Integration testing of the PyTorch models deployed in `surf-ratings-api` ([TorchServe](https://pytorch.org/serve/))

## Files
- `.env.sample` - example environment variables for running the tests
- `Dockerfile` - docker image definition for running surf ratings API integration tests
- `environment.devenv.yml` - Conda environment for running integration tests
- `fixtures.py` - module to create test fixture data
- `main.py` - module to run integration tests
- `util.py` - utility functions that are common across integration tests

## Running
Tests are broken in to two steps: fixture creation and test execution. This way, we can create fixtures which allow testing in isolation.

1. Copy `.env.sample` to `.env` and fill in the necessary variables.
2. Run tests locally (for development):
     ```sh
     $ env $(xargs < .env) python main.py
     ```
3. Build and run docker image to run tests (used in CI/CD):
     ```sh
     $ docker build -t test_surf_ratings_api .
     $ docker run --env-file=.env --rm test_surf_ratings_api
     ```

## Adding a new test

