import logging

from RatingsSpectraRefMatrix.test import test as test_ratings_spectra_ref_matrix
from RatingsMinimal.test import test as test_ratings_minimal
from util import wait_for_torchserve_initialization

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


def main():
    wait_for_torchserve_initialization()

    """
    RatingsSpectraRefMatrix
    """
    test_ratings_spectra_ref_matrix()

    """
    RatingsMinimal
    """
    test_ratings_minimal()


if __name__ == '__main__':
    main()
