import json
import logging
import os
from timeit import default_timer

import requests

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


def test():
    with open('RatingsMinimal/request.json', 'r') as f:
        s = f.read()
        request_json = json.loads(s)

    start = default_timer()
    response = requests.post(
        f"{os.environ['SURF_RATINGS_API']}/predictions/ratings-minimal",
        json=request_json,
    )
    elapsed = default_timer() - start
    logger.info(f'Completed request in {elapsed}s.')
    result = response.json()
    logger.info(f'Response: {result}')
    logger.info(f'Response length: {len(result)}')
    assert len(result) == len(request_json['forecast']['surf']['data'])
