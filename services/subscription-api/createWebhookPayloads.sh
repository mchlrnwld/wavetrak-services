## STANDUP WEBHOOK HANDLER
docker-compose -f ./createWebhookPayloads/docker-compose.webhooks.yml build --compress
docker-compose -f ./createWebhookPayloads/docker-compose.webhooks.yml up -d

## LOCAL TUNNEL TO PORT 9005 FOR WEBHOOK ENDPOINT
# Command has to be run in the background
lt --port 9005 --subdomain surflinewebhooks >/dev/null 2>&1 &

read -p "Once you have updated the webhook version to desired one press Y: " y
case $y in
		[Yy]* ) echo "Continuing";;
		[Nn]* ) exit;;
		* ) echo "Please answer yes or no.";;
esac

# Replace docker urls with localhost URL's
sed -i -e 's/\/\/mongo/\/\/127.0.0.1/g' ./.env.test
sed -i -e 's/redis/127.0.0.1/g' ./.env.test

# Start up mongo and redis containers
# Note: this command will fail if containers are already running, tests will continue
docker-compose -f ./integration-tests/docker-compose.test.yml up -d

# Run mocha integration tests
npm run test:integration

# Replace localhost urls back to docker url's
sed -i -e 's/\/\/127.0.0.1/\/\/mongo/g' ./.env.test
sed -i -e 's/127.0.0.1/redis/g' ./.env.test

rm .env.test-e

## Kill local tunnel
kill $(ps aux | grep 'lt --port 9005' | awk '{print $2}')