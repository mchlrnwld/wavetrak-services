#!/usr/bin/env bash

curl https://api.stripe.com/v1/tokens -s \
   -u sk_test_2qVFch0zD31N8dYZCagyY7Vx: \
   -d card[number]=4242424242424242 \
   -d card[exp_month]=12 \
   -d card[exp_year]=2017 \
   -d card[cvc]=123 \
| jq .id -r
