import csvtojson from 'csvtojson';
import { connectMongo } from '../src/common/dbContext';
import UserAccountModel from '../src/models/v1/accounts/UserAccountModel';
import { processAppleReceipt } from '../src/models/subscription-purchases/itunes';
import { getSubscriptionDataFromItunesReceipt } from '../src/utils/itunes';
import { updateOrCreateItunesSubscription } from '../src/models/subscriptions/itunes';

const populateGooglePurchases = async () => {
  try {
    await connectMongo();
    csvtojson()
      .fromFile('tools/apple_account_ids.csv')
      .subscribe(
        async data => {
          const { _id } = data;
          const { receipts, subscriptions, user } = await UserAccountModel.findOne({ _id });
          const appleSubscriptionIds = subscriptions
            .filter(({ type }) => type === 'apple')
            .map(({ subscriptionId }) => {
              return subscriptionId;
            });
          for (const subscriptionId of appleSubscriptionIds) {
            const receipt = receipts.find(({ purchases }) => {
              const purchase = purchases.find(({ transactionId }) => {
                return transactionId === subscriptionId;
              });
              if (purchase) return true;
            });
            if (receipt) {
              try {
                const { receiptData } = receipt;
                const newOrUpdatedReceipt = await processAppleReceipt(user, receiptData);
                const subscriptionData = getSubscriptionDataFromItunesReceipt(newOrUpdatedReceipt);
                await updateOrCreateItunesSubscription(subscriptionData);
              } catch (error) {
                console.log(error);
              }
            }
          }
        },
        error => {
          console.log(error);
          process.exit(1);
        },
        () => {
          console.log('Done');
          process.exit(0);
        },
      );
  } catch (error) {
    console.log('fatal error', error);
    process.exit(1);
  }
};

populateGooglePurchases();
