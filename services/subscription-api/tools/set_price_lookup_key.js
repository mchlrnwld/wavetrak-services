import { getPrices, updatePrice } from '../src/external/stripe';
import getConfig from '../src/config';

const getKeyName = priceId => {
  const { priceIdMap } = getConfig();
  return Object.keys(priceIdMap).find(key => priceIdMap[key] === priceId);
};

const updateStripePrices = async () => {
  const { data: prices } = await getPrices();
  for (const price of prices) {
    const { id: priceId, unit_amount: amount } = price;
    const lookupKey = getKeyName(price.id);
    if (lookupKey) {
      await updatePrice(priceId, { lookup_key: `${lookupKey}_${amount}` });
    }
  }
};

updateStripePrices();
