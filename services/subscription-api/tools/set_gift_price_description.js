import { getPrices, updatePrice } from '../src/external/stripe';
import getConfig from '../src/config';
import toBrandName from '../src/utils/toBrandName';

const getKeyName = priceId => {
  const { priceIdMap } = getConfig();
  return Object.keys(priceIdMap).find(key => priceIdMap[key] === priceId);
};

const computeDescription = str => {
  const descArray = str.split('_');
  const brand = descArray[0].toLowerCase();
  const length = parseInt(descArray[1], 10);

  if (length === 12) {
    return `1 Year of ${toBrandName(brand)} Premium`;
  }
  if (length === 1) {
    return `1 Month of ${toBrandName(brand)} Premium`;
  }
  return `${length} Months of ${toBrandName(brand)} Premium`;
};

const updateStripePrices = async () => {
  const { data: prices } = await getPrices({ type: 'one_time' });
  for (const price of prices) {
    const { id: priceId } = price;
    const keyName = getKeyName(price.id);
    if (keyName) {
      const description = computeDescription(keyName);
      await updatePrice(priceId, { metadata: { description } });
    }
  }
};

updateStripePrices();
