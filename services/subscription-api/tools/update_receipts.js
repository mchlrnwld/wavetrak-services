import csvToJson from 'csvtojson';
import { getAccount } from '../src/models/accounts';
import { connectMongo } from '../src/common/dbContext';
import { updateUserAccountReceipt } from '../src/models/iap';

const updateAccounts = async () => {
  try {
    await connectMongo();
    csvToJson()
      .fromFile('./ExpEnt/sl.csv')
      .on('json', async (user) => {
        const account = await getAccount(user.userId);
        const deviceType = 'apple';
        if ((account.receipts[0].type === 'apple') && (account.entitlements.length === 0)) {
          await updateUserAccountReceipt(
            deviceType,
            account.receipts[0].receiptData,
            account.user.toString(),
          );
        }
      })
      .on('done', error => {
        if (error) { console.log(error); }
      });
  } catch (error) {
    console.log(error);
  }
};


updateAccounts();
