import { connectMongo } from '../src/common/dbContext';
import SubscriptionUsersModel from '../src/models/subscription-users/SubscriptionUsersModel';
import { SubscriptionModel } from '../src/models/subscriptions/SubscriptionModel';

const updateSubUsers = async () => {
  try {
    await connectMongo();
    const subUsers = await SubscriptionUsersModel.find({ subscriptionId: null });

    for (const subUser of subUsers) {
      const { primaryUser } = subUser;
      const subscriptions = await SubscriptionModel.find({ user: primaryUser, active: true });
      if (subscriptions.length === 1) {
        const [{ type, subscriptionId }] = subscriptions;
        subUser.type = type;
        subUser.subscriptionId = subscriptionId;
        await subUser.save();
      }
    }
  } catch (error) {
    console.log(error);
  }
};

updateSubUsers();
