require('dotenv').config({ silent: true });
require('@babel/register');
const stripe = require('stripe')('sk_test_XbDRO42EABPlUJfsZgAEVMc4');
const fs = require('fs');
const path = require('path');
const app = require('express')();

// Retrieve the raw body as a buffer and match all content types
app.use(require('body-parser').raw({ type: '*/*' }));

app.post('/webhook/integration', (req, res) => {
  const sig = req.headers['stripe-signature'];
  const endpointSecret = 'whsec_gydMXUM4rEm1GZOXiBRbvAXHyfH3DhHs';

  try {
    const event = stripe.webhooks.constructEvent(req.body, sig, endpointSecret);
    const type = event.type;
    fs.writeFileSync(
      path.resolve(process.cwd(), `./payloads/${type}.json`),
      JSON.stringify({ event })
    );
  } catch (err) {
    console.log(err);
    return res.status(400).end();
  }

  return res.status(200).json({ received: true }).end();
});

app.listen(9005, () => console.log('Running on port 9005'));
