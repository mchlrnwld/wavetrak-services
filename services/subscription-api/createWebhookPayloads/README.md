### Purpose

The createWebhookPayloads is a mini node app that is stood up using docker to consume webhooks from the Integration test environment.

This node app consumes any webhooks from the environment a writes the output to a file on the users local. This allows us to quickly get and stub webhook payloads. With this we can easily write a test and then stub a webhook specifically for that test.

The default output for the webhook is `/payloads/webhook-type.json`. For example `/payloads/invoice.created.json`

### Prerequisites to run

- Globally installed `localtunnel`: `npm install -g localtunnel`
- Node
- Docker


### Instructions

- Enable webhooks in the integration test stripe dashboard
- Run ` sh createWebhookPayloads.sh`
- When prompted with `Once you have updated the webhook version to desired one press Y:` ensure you have updated the webhook version to your desired one in the Stripe Dashboard
- Sit back and relax...