import { expect } from 'chai';
import request from 'superagent';
import { genTestToken } from '../../../src/external/stripeutils';
import { SubscriptionModel } from '../../../src/models/subscriptions/SubscriptionModel';
import { deleteStripeSubscriptions } from '../../../src/models/subscriptions/stripe';
import * as stripe from '../../../src/external/stripe';
import { createStripeCustomer, removeStripeCustomer } from '../../../src/models/customers/stripe';

const cardsTests = theUserId => {
  const servicesUri = 'http://localhost:3000';

  const cardsApi = `${servicesUri}/credit-cards`;

  const getCardsApi = () => {
    return new Promise((resolve, reject) => {
      request
        .get(cardsApi)
        .set('x-auth-userid', theUserId)
        .end((err, result) => {
          if (err) {
            reject(result.body);
          }
          resolve(result.body);
        });
    });
  };

  const createCardsApi = tokenId => {
    return new Promise((resolve, reject) => {
      request
        .post(cardsApi)
        .set('x-auth-userid', theUserId)
        .send({ source: tokenId })
        .end((err, result) => {
          if (err) {
            reject(result.body);
          }
          resolve(result.body);
        });
    });
  };

  const updateCardsApi = (cardId, opts) => {
    return new Promise((resolve, reject) => {
      request
        .put(`${cardsApi}/${cardId}`)
        .set('x-auth-userid', theUserId)
        .send({ opts })
        .end((err, result) => {
          if (err) {
            reject(result.body);
          }
          resolve(result.body);
        });
    });
  };

  const deleteCardsApi = cardId => {
    return new Promise((resolve, reject) => {
      request
        .delete(`${cardsApi}/${cardId}`)
        .set('x-auth-userid', theUserId)
        .end((err, result) => {
          if (err) {
            reject(result.body);
          }
          resolve(result.body);
        });
    });
  };

  const setDefaultCardApi = cardId => {
    return new Promise((resolve, reject) => {
      request
        .put(`${cardsApi}/default/${cardId}`)
        .set('x-auth-userid', theUserId)
        .end((err, result) => {
          if (err) {
            reject(result.body);
          }
          resolve(result.body);
        });
    });
  };

  const stubbedUser = {
    _id: theUserId.toString(),
    firstName: 'Pam',
    lastName: 'Beesly',
    email: 'pambeesly@example.com',
    usID: 783514,
  };

  const createTestStripeCustomer = async tokenId => {
    const { email } = stubbedUser;
    const res = await stripe.createCustomer({
      source: tokenId,
      email,
    });
    await createStripeCustomer(theUserId, res.id);
    return res;
  };

  const seedSubscription = async (brand, type, subscriptionId, opts = {}) => {
    const subscription = await SubscriptionModel.create({
      type,
      subscriptionId,
      entitlement: `${brand}_premium`,
      planId: `${brand}_monthly`,
      user: theUserId,
      start: 1579201980,
      end: 1894821181,
      active: true,
      ...opts,
    });
    return subscription;
  };

  describe('Cards Tests [Subscription API V2]', () => {
    afterEach(async () => {
      await deleteStripeSubscriptions(theUserId);
      await removeStripeCustomer(theUserId);
    });

    describe('Get Credit Card API', () => {
      it('can get a card', async () => {
        const {
          id: tokenId,
          card: { id: cardId },
        } = await genTestToken();
        const { id: stripeCustomerId } = await createTestStripeCustomer(tokenId);
        await seedSubscription('sl', 'stripe', 'sub_123', {
          stripeCustomerId,
          failedPaymentAttempts: 0,
        });
        const { cards, defaultSource } = await getCardsApi();
        expect(cards.length).to.equal(1);
        expect(cards[0].id).to.equal(cardId);
        expect(defaultSource).to.equal(cardId);

        await stripe.deleteCustomer(stripeCustomerId);
      });
    });

    describe('Create Credit Card API', () => {
      it('can add new card', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        await seedSubscription('sl', 'stripe', 'sub_123', { stripeCustomerId });
        const {
          id: tokenId,
          card: { id: cardId },
        } = await genTestToken();
        const { card } = await createCardsApi(tokenId);

        expect(card.id).to.equal(cardId);

        await stripe.deleteCustomer(stripeCustomerId);
      });
      it('cannot add duplicate cards', async () => {
        const { id: original } = await genTestToken('371449635398431');

        const { id: stripeCustomerId } = await createTestStripeCustomer(original);
        await seedSubscription('sl', 'stripe', 'sub_123', { stripeCustomerId });

        const { id: dupe } = await genTestToken('371449635398431');

        try {
          await createCardsApi(dupe);
          expect(false);
        } catch (err) {
          expect(err).to.exist;
        }
      });
    });

    describe('Update Credit Card API', () => {
      it('can update a card', async () => {
        const {
          id: tokenId,
          card: { id: cardId },
        } = await genTestToken();
        const { id: stripeCustomerId } = await createTestStripeCustomer(tokenId);
        await seedSubscription('sl', 'stripe', 'sub_123', { stripeCustomerId });

        const { card } = await updateCardsApi(cardId, { address_zip: 90210 });

        expect(card.address_zip).to.equal('90210');

        // check stripe
        const stripeCust = await stripe.getCustomer(stripeCustomerId, ['sources']);
        expect(stripeCust.sources.data[0].exp_month).to.equal(card.expMonth);
        expect(stripeCust.sources.data[0].exp_year).to.equal(card.expYear);

        // submit an invalid exp month
        try {
          await updateCardsApi(cardId, { expMonth: 'ab' });
        } catch (err) {
          expect(err).to.exist;
        }

        // submit an invalid exp year
        try {
          await updateCardsApi(cardId, { expYear: 'defg' });
        } catch (err) {
          expect(err).to.exist;
        }

        // submit an invalid card
        try {
          await updateCardsApi('abcdefg', { blah: 'blah' });
        } catch (err) {
          expect(err).to.exist;
        }

        await stripe.deleteCustomer(stripeCustomerId);
      });
    });

    describe('Delete Credit Card API', () => {
      it('can delete a card', async () => {
        const { id: tokenId } = await genTestToken();
        const { id: stripeCustomerId } = await createTestStripeCustomer(tokenId);
        await seedSubscription('sl', 'stripe', 'sub_123', { stripeCustomerId });

        const {
          id: newTokenId,
          card: { id: newCardId },
        } = await genTestToken('371449635398431');
        await stripe.addCard(stripeCustomerId, newTokenId);

        const { message } = await deleteCardsApi(newCardId);

        expect(message).to.equal('The card was deleted');

        await stripe.deleteCustomer(stripeCustomerId);
      });

      it('cannot delete a default card', async () => {
        const {
          id: tokenId,
          card: { id: cardId },
        } = await genTestToken();
        const { id: stripeCustomerId } = await createTestStripeCustomer(tokenId);
        await seedSubscription('sl', 'stripe', 'sub_123', { stripeCustomerId });

        try {
          await deleteCardsApi(cardId);
        } catch (err) {
          expect(err).to.exist;
        }

        await stripe.deleteCustomer(stripeCustomerId);
      });
    });

    describe('Set Default Credit Card API', () => {
      it('can change a default credit card', async () => {
        const { id: tokenId } = await genTestToken();
        const { id: stripeCustomerId } = await createTestStripeCustomer(tokenId);
        await seedSubscription('sl', 'stripe', 'sub_123', { stripeCustomerId });

        const {
          id: newTokenId,
          card: { id: newCardId },
        } = await genTestToken('371449635398431');
        await stripe.addCard(stripeCustomerId, newTokenId);

        const { message } = await setDefaultCardApi(newCardId);

        expect(message).to.equal('Subscription updated');

        const { default_source } = await stripe.getCustomer(stripeCustomerId);

        expect(default_source).to.equal(newCardId);

        await stripe.deleteCustomer(stripeCustomerId);
      });
    });
  });
};

export default cardsTests;
