import sinon from 'sinon';
import { expect } from 'chai';
import request from 'superagent';
import * as getUser from '../../../src/external/user';
import * as stripe from '../../../src/external/stripe';
import simulateStripeEvent from '../../helpers/simulateStripeEvent';
import CreditCardModel from '../../../src/models/v1/credit-cards/CreditCardModel';
import {
  deleteSubscriptions,
  getActiveSubscriptionByUserIdAndEntitlement,
} from '../../../src/models/subscriptions';
import { SubscriptionPurchaseModel } from '../../../src/models/subscription-purchases/SubscriptionPurchaseModel';
import * as itunesApi from '../../../src/external/itunes';
import {
  simulatePostBilling,
  simulateGetBilling,
  verfifyItunesReceipt,
  verifyGooglePurchase,
  simulateItunesNotification,
  simulateGoogleNotification,
} from '../../helpers/simulateSubscription';
import iTunesReceiptData from '../../../src/__stubs__/itunes/BASE_RECEIPT';
import { keysToCamel } from '../../../src/utils/toCamelCase';
import { removeStripeCustomer } from '../../../src/models/customers/stripe';
import googlePurchaseData from '../../../src/__stubs__/google/BASE_PURCHASE_DATA';
import googleSignature from '../../../src/__stubs__/google/BASE_SIGNATUTRE';
import * as googlePlayAPI from '../../../src/external/google';

export default function paymentAlertsIntegrationTests(theUserId) {
  const servicesURI = 'http://localhost:3000';
  const paymentAlertsAPI = `${servicesURI}/payment/alerts`;
  let theStripeCustomerId;
  let getUserStub;
  let getInvoiceFromChargeStub;
  let getReceiptDataStub;
  let getPurchaseDataStub;

  const stubbedUser = {
    _id: theUserId.toString(),
    firstName: 'Joel',
    lastName: 'Parkinson',
    email: 'joel@example.com',
    usID: 783514,
  };

  const defaultItunesReceiptDataOpts = {
    productId: 'surfline.iosapp.1month.subscription.notrial',
    isTrialPeriod: 'false',
    transactionId: '201910100000',
    originalTransactionId: '201910100000',
  };

  const simulateGetPaymentAlert = async product => {
    return new Promise((resolve, reject) => {
      request
        .get(paymentAlertsAPI)
        .set('x-auth-userid', theUserId)
        .set('accept', 'json')
        .query({ product })
        .end((err, resp) => {
          if (err) {
            reject(resp);
          }
          resolve(resp.body);
        });
    });
  };

  const teardownSubscription = async () => {
    await deleteSubscriptions(theUserId);
    if (theStripeCustomerId) {
      await stripe.deleteCustomer(theStripeCustomerId);
      await removeStripeCustomer(theUserId);
    }
  };

  const tearDownTestSubscriptionPurchase = async () =>
    SubscriptionPurchaseModel.deleteOne({ user: theUserId });

  describe('Payment Alerts Tests [Subscription API v2]', () => {
    beforeEach(async () => {
      getUserStub = sinon.stub(getUser, 'getUserDetails');
      getReceiptDataStub = sinon.stub(itunesApi, 'default');
      getUserStub.returns(stubbedUser);
      getInvoiceFromChargeStub = sinon.stub(stripe, 'getInvoiceFromCharge');
      getPurchaseDataStub = sinon.stub(googlePlayAPI, 'default');
    });
    afterEach(async () => {
      getUserStub.restore();
      await teardownSubscription();
      await tearDownTestSubscriptionPurchase();
      await CreditCardModel.deleteOne({});
      theStripeCustomerId = null;
      getInvoiceFromChargeStub.restore();
      getReceiptDataStub.restore();
      getPurchaseDataStub.restore();
    });
    describe('GET Payment Alerts API', () => {
      it('should not receive a payment alert if a product query is not passed', async () => {
        await simulatePostBilling(theUserId);
        const { stripeCustomerId } = await simulateGetBilling(theUserId, 'sl');
        theStripeCustomerId = stripeCustomerId;

        const { message, alert } = await simulateGetPaymentAlert();
        expect(message).to.equal('No payment alerts found.');
        expect(alert).to.equal(false);
      });
      it('should recieve a payment alert for the failed payment of a stripe subscription', async () => {
        await simulatePostBilling(theUserId);
        const { stripeCustomerId, subscriptionId, end, start } = await simulateGetBilling(
          theUserId,
          'sl',
        );
        theStripeCustomerId = stripeCustomerId;

        const { message, alert } = await simulateGetPaymentAlert('sl');
        expect(message).to.equal('No payment alerts found.');
        expect(alert).to.equal(false);

        await simulateStripeEvent(
          'invoice.payment_failed',
          { stripeCustomerId, subscriptionId },
          {},
        );
        const {
          message: alertMessage,
          alert: updatedAlertBool,
          type,
        } = await simulateGetPaymentAlert('sl');
        expect(alertMessage).to.equal(
          'Payment Failed.  Your credit card could not be charged.  Please update your payment details to continue receiving your premium benefits.',
        );
        expect(updatedAlertBool).to.equal(true);
        expect(type).to.equal('web');

        await simulateStripeEvent(
          'invoice.payment_succeeded',
          {
            stripeCustomerId,
            subscriptionId,
          },
          { periodEnd: end, periodStart: start },
        );

        const {
          message: updatedAlertMessage,
          alert: revertedAlertBool,
        } = await simulateGetPaymentAlert('sl');

        expect(updatedAlertMessage).to.equal('No payment alerts found.');
        expect(revertedAlertBool).to.equal(false);
      });
      it('should recieve a payment alert for the failed payment of an itunes subscription', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt(theUserId, '123ABC');
        const {
          active,
          alertPayment,
          planId,
          transactionId,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(active).to.equal(true);
        expect(alertPayment).to.equal(false);

        const { message, alert } = await simulateGetPaymentAlert('sl');
        expect(message).to.equal('No payment alerts found.');
        expect(alert).to.equal(false);

        await simulateItunesNotification('DID_FAIL_TO_RENEW', {
          planId,
          transactionId,
          isInBillingRetryPeriod: '1',
        });

        const {
          active: updatedActive,
          alertPayment: updatedAlertPayment,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(updatedActive).to.equal(true);
        expect(updatedAlertPayment).to.equal(true);

        const {
          message: alertMessage,
          alert: updatedAlertBool,
          type,
        } = await simulateGetPaymentAlert('sl');
        expect(alertMessage).to.equal(
          'Payment Failed.  Please visit your iTunes account to resolve this issue.',
        );
        expect(updatedAlertBool).to.equal(true);
        expect(type).to.equal('apple');
      });
      it('should recieve a payment alert for the failed payment of an itunes subscription', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const { active, alertPayment } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );
        expect(active).to.equal(true);
        expect(alertPayment).to.equal(false);

        const { message, alert } = await simulateGetPaymentAlert('sl');
        expect(message).to.equal('No payment alerts found.');
        expect(alert).to.equal(false);

        const {
          receipt: {
            data: { purchaseToken },
          },
        } = purchaseSignature;
        const onHoldPurchaseData = googlePurchaseData({ onHold: true });
        getPurchaseDataStub.returns(onHoldPurchaseData);
        await simulateGoogleNotification('05_SUBSCRIPTION_ON_HOLD', { purchaseToken });

        const {
          active: updatedActive,
          alertPayment: updatedAlertPayment,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(updatedActive).to.equal(true);
        expect(updatedAlertPayment).to.equal(true);

        const {
          message: alertMessage,
          alert: updatedAlertBool,
          type,
        } = await simulateGetPaymentAlert('sl');
        expect(alertMessage).to.equal(
          'Payment Failed.  Please visit your Google-Play account to resolve this issue.',
        );
        expect(updatedAlertBool).to.equal(true);
        expect(type).to.equal('google');
      });
    });
  });
}
