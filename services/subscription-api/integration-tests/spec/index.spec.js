import express from 'express';
import http from 'http';
import chai from 'chai';
import chaihttp from 'chai-http';
import dirtyChai from 'dirty-chai';
import chaiString from 'chai-string';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import mongoose from 'mongoose';
import Chance from 'chance';
import { errorHandlerMiddleware } from '@surfline/services-common';
import subscriptions from '../../src/server/v1/subscriptions';
import cards from '../../src/server/cards';
import itunes from '../../src/server/subscriptions/itunes';
import google from '../../src/server/subscriptions/google';
import billing from '../../src/server/subscriptions/stripe';
import checkout from '../../src/server/subscriptions/stripe/checkout';
import promotion from '../../src/server/subscriptions/promotion';
import offer from '../../src/server/subscriptions/offer';
import payment from '../../src/server/gifts/payment';
import redeem from '../../src/server/gifts/redeem';
import giftsVerify from '../../src/server/gifts/verify';
import alerts from '../../src/server/alerts';
import invoices from '../../src/server/invoices';
import pricingPlans from '../../src/server/pricing-plans';
import stripeIntegrations from '../../src/server/integrations/stripe';
import iTunesIntegrations from '../../src/server/integrations/itunes';
import googleIntegrations from '../../src/server/integrations/google';
import events from '../../src/server/integrations/events';
import adminCoupons from '../../src/server/admin/coupons';
import adminSubscriptionsGifts from '../../src/server/admin/subscriptions/gifts';
import adminSubscriptions from '../../src/server/subscriptions/wavetrak/admin';
import adminItunesPurchases from '../../src/server/subscription-purchases/itunes/admin';
import adminGooglePurchases from '../../src/server/subscription-purchases/google/admin';
import adminSubscriptionUsers from '../../src/server/subscription-users/admin';

import v1Admin from '../../src/server/v1/admin';
import stripeV1Integrations from '../../src/server/v1/integrations/stripe';
import inAppPurchaseIOS from '../../src/server/v1/iap/inAppPurchaseIOS';
import inAppPurchaseGoogle from '../../src/server/v1/iap/inAppPurchaseGoogle';

import cardTests from './cards/cards.spec';
import iTunesNotificationsTests from './integrations/itunes.spec';
import googleNotificationsTests from './integrations/google.spec';
import stripeNotificationsTests from './integrations/stripe.spec';
import triggerSubscriptionEventTests from './integrations/events.spec';
import giftPaymentTests from './gifts/payment.spec';
import giftRedeemTests from './gifts/redeem.spec';
import giftVerifyTests from './gifts/verify.spec';
import giftCheckoutTests from './gifts/checkout.spec';
import invoiceTests from './invoices/invoices.spec';
import pricingPlanTests from './pricing-plans/pricing-plans.spec';
import stripeSubscriptionTests from './subscriptions/stripe.spec';
import promotionSubscriptionTests from './subscriptions/promotion.spec';
// import offerSubscriptionTests from './subscriptions/offer.spec';
import googleSubscriptionTests from './subscriptions/google.spec';
import itunesSubscriptionTests from './subscriptions/itunes.spec';
import subscriptionUsersTests from './subscription-users/subscription-users.spec';
import paymentAlertsTests from './alerts/index.spec';
import adminCouponTests from './admin/coupons.spec';
import adminGiftTests from './admin/subscriptions/gifts.spec';
import adminSubscriptionTests from './subscriptions/wavetrak/admin.spec';
import adminItunesPurchaseTests from './subscription-purchases/itunes/admin.spec';
import adminGooglePurchaseTests from './subscription-purchases/google/admin.spec';
import adminSubscriptionUsersTests from './subscription-users/admin.spec';
import { initCache, connectMongo } from '../../src/common/dbContext';

import * as logger from '../../src/common/logger';
import * as analytics from '../../src/common/analytics';
import * as customerio from '../../src/common/customerio';

import * as userService from '../../src/external/user';
import * as UserInfo from '../../src/models/v1/accounts/info';
import UserInfoModel from '../../src/models/v1/accounts/UserInfoModel';
import CreditCardModel from '../../src/models/v1/credit-cards/CreditCardModel';
import giftCheckout from '../../src/server/gifts/checkout';

const theUserId = mongoose.Types.ObjectId();

chai.use(chaiString);
chai.use(chaihttp);
chai.use(sinonChai);
chai.use(dirtyChai);

describe('Integration Tests', () => {
  let userServiceDefault;
  let userInfoDefault;
  before(async () => {
    if (process.env.NODE_ENV !== 'test') {
      console.log('You need to set your NODE_ENV to test');
      process.exit(1);
    }
    sinon.stub(customerio, 'default').returns(true);
    sinon.stub(analytics, 'track').returns(true);
    sinon.stub(analytics, 'identify').returns(true);
    sinon.stub(analytics, 'identifyAll').returns(true);

    const userStub = new Chance();
    const firstName = userStub.first({ nationality: 'en' }).toLowerCase();
    const lastName = userStub.last({ nationality: 'en' }).toLowerCase();
    const email = `${firstName}_${lastName}@example.com`;

    userServiceDefault = sinon.stub(userService, 'default').callsFake(userId => ({
      _id: userId.toString(),
      firstName,
      lastName,
      email,
      usID: 783514,
      __v: 0,
      locale: 'en-US',
      brand: 'sl',
      modifiedDate: '2016-05-31T22:45:32.092Z',
      isActive: true,
    }));

    userInfoDefault = sinon.stub(UserInfo, 'default').callsFake(userId => ({
      _id: userId.toString(),
      firstName,
      lastName,
      email,
      usID: 783514,
      __v: 0,
      locale: 'en-US',
      brand: 'sl',
      modifiedDate: '2016-05-31T22:45:32.092Z',
      isActive: true,
    }));

    try {
      await initCache();
      await connectMongo();
    } catch (error) {
      console.log('Connecting to DB failed');
      throw error;
    }

    const userInfo = new UserInfoModel({
      _id: theUserId.toString(),
      firstName,
      lastName,
      email,
      usID: 783514,
      __v: 0,
      locale: 'en-US',
      brand: 'sl',
      modifiedDate: '2016-05-31T22:45:32.092Z',
      isActive: true,
    });
    userInfo.save();

    const app = express();
    app.server = http.createServer(app);
    app.server.listen(3000);

    app.use('/credit-cards', cards());
    app.use('/subscriptions/itunes', itunes());
    app.use('/subscriptions/google', google());
    app.use('/subscriptions/billing/checkout', checkout());
    app.use('/subscriptions/billing', billing());
    app.use('/subscriptions/promotion', promotion());
    app.use('/subscriptions/offers', offer());

    app.use('/integrations/stripe', stripeIntegrations());
    app.use('/integrations/itunes', iTunesIntegrations());
    app.use('/integrations/google', googleIntegrations());
    app.use('/integrations/subscriptions', events());

    app.use('/gift/payment', payment());
    app.use('/gift/redeem', redeem());
    app.use('/gifts/verify', giftsVerify());
    app.use('/gift/checkout', giftCheckout());

    app.use('/payment/alerts', alerts());
    app.use('/admin/coupons', adminCoupons());
    app.use('/admin/subscriptions/wavetrak', adminSubscriptions());
    app.use('/admin/subscriptions/gifts', adminSubscriptionsGifts());
    app.use('/admin/purchases/itunes', adminItunesPurchases());
    app.use('/admin/purchases/google', adminGooglePurchases());
    app.use('/admin/subscription-users', adminSubscriptionUsers());
    app.use('/invoices', invoices());
    app.use('/pricing-plans', pricingPlans());

    app.use('/ios', inAppPurchaseIOS());
    app.use('/google', inAppPurchaseGoogle());

    app.use('/admin', v1Admin());
    app.use('/subscriptions', subscriptions());
    app.use('/integrations/v1/stripe', stripeV1Integrations());
    app.use('/iap/ios', inAppPurchaseIOS());
    app.use('/iap/google', inAppPurchaseGoogle());
    app.use(errorHandlerMiddleware({ error: () => {} }));
  });
  try {
    cardTests(theUserId);
    iTunesNotificationsTests(theUserId);
    googleNotificationsTests(theUserId);
    stripeNotificationsTests(theUserId);
    invoiceTests(theUserId);
    pricingPlanTests(theUserId);
    giftPaymentTests(theUserId);
    giftRedeemTests(theUserId);
    giftVerifyTests(theUserId);
    giftCheckoutTests(theUserId);
    stripeSubscriptionTests(theUserId);
    promotionSubscriptionTests(theUserId);
    // offerSubscriptionTests(theUserId);
    googleSubscriptionTests(theUserId);
    itunesSubscriptionTests(theUserId);
    subscriptionUsersTests(theUserId);
    paymentAlertsTests(theUserId);
    adminCouponTests(theUserId);
    adminGiftTests(theUserId);
    adminSubscriptionTests(theUserId);
    adminItunesPurchaseTests(theUserId);
    adminGooglePurchaseTests(theUserId);
    adminSubscriptionUsersTests(theUserId);
    triggerSubscriptionEventTests();
  } catch (error) {
    console.log(error);
  }

  after(async () => {
    logger.default.restore();
    userServiceDefault.restore();
    userInfoDefault.restore();
    customerio.default.restore();
    analytics.track.restore();
    analytics.identify.restore();
    analytics.identifyAll.restore();
    await CreditCardModel.deleteOne({});
  });
});
