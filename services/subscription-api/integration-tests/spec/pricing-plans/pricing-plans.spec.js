import request from 'superagent';
import { expect } from 'chai';
import sinon from 'sinon';
import * as getUser from '../../../src/external/user';
import {
  simulateGetBilling,
  simulatePostBilling,
  simulateDeleteBilling,
} from '../../helpers/simulateSubscription';
import simulateStripeEvent from '../../helpers/simulateStripeEvent';
import { removeStripeCustomer } from '../../../src/models/customers/stripe';

export default function pricingPlansIntegrationTests(theUserId) {
  const servicesURI = 'http://localhost:3000';
  const pricingPlansAPI = `${servicesURI}/pricing-plans`;
  const giftsPricingPlansAPI = `${servicesURI}/pricing-plans/gifts`;
  const theSource = '4242424242424242';
  let getUserStub;
  const stubbedUser = {
    _id: theUserId.toString(),
    firstName: 'Joel',
    lastName: 'Parkinson',
    email: 'joel@example.com',
    usID: 783514,
  };

  const simulateGetPricingPlans = queryOpts =>
    new Promise((resolve, reject) => {
      request
        .get(pricingPlansAPI)
        .set('accept', 'json')
        .query(queryOpts)
        .end((err, resp) => {
          if (err) {
            reject(err);
          }
          resolve(resp.body);
        });
    });
  const simulateGetPricingPlansWithAuth = queryOpts =>
    new Promise((resolve, reject) => {
      request
        .get(pricingPlansAPI)
        .set('accept', 'json')
        .set('x-auth-userid', theUserId)
        .query(queryOpts)
        .end((err, resp) => {
          if (err) {
            reject(err);
          }
          resolve(resp.body);
        });
    });
  const simulateGetGiftPricingPlans = queryOpts =>
    new Promise((resolve, reject) => {
      request
        .get(giftsPricingPlansAPI)
        .set('accept', 'json')
        .query(queryOpts)
        .end((err, resp) => {
          if (err) {
            reject(err);
          }
          resolve(resp.body);
        });
    });
  const simulateGetGiftPricingPlansWithAuth = queryOpts =>
    new Promise((resolve, reject) => {
      request
        .get(giftsPricingPlansAPI)
        .set('accept', 'json')
        .set('x-auth-userid', theUserId)
        .query(queryOpts)
        .end((err, resp) => {
          if (err) {
            reject(err);
          }
          resolve(resp.body);
        });
    });

  describe('Pricing-Plans API Tests [Subscription API V2]', () => {
    beforeEach(() => {
      getUserStub = sinon.stub(getUser, 'getUserDetails');
    });
    afterEach(async () => {
      getUserStub.restore();
      await removeStripeCustomer(theUserId);
    });

    describe('GET /pricing-plans', () => {
      it('should only return recurring pricing plans', async () => {
        const { plans } = await simulateGetPricingPlans({ brand: 'sl' });
        plans.forEach(plan => {
          expect(plan.lookupKey).to.not.include('gift');
        });
      });

      it('should return the correct pricing-plans per brand', async () => {
        const { plans: slPlans } = await simulateGetPricingPlans({ brand: 'sl' });
        slPlans.forEach(({ brand }) => {
          expect(brand).to.equal('sl');
        });
        const { plans: bwPlans } = await simulateGetPricingPlans({ brand: 'bw' });
        bwPlans.forEach(({ brand }) => {
          expect(brand).to.equal('bw');
        });
        const { plans: fsPlans } = await simulateGetPricingPlans({ brand: 'fs' });
        fsPlans.forEach(({ brand }) => {
          expect(brand).to.equal('fs');
        });
      });

      it('should return the correct pricing-plans per country', async () => {
        const { plans: usdPlans } = await simulateGetPricingPlans({ brand: 'sl' });
        usdPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('usd');
          expect(symbol).to.equal('$');
        });
        const { plans: gbpPlans } = await simulateGetPricingPlans({
          brand: 'sl',
          countryISO: 'GB',
        });
        gbpPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('gbp');
          expect(symbol).to.equal('£');
        });
        const { plans: eurPlans } = await simulateGetPricingPlans({
          brand: 'sl',
          countryISO: 'FR',
        });
        eurPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('eur');
          expect(symbol).to.equal('€');
        });
        const { plans: audPlans } = await simulateGetPricingPlans({
          brand: 'sl',
          countryISO: 'AU',
        });
        audPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('aud');
          expect(symbol).to.equal('$');
        });
        const { plans: nzdPlans } = await simulateGetPricingPlans({
          brand: 'sl',
          countryISO: 'NZ',
        });
        nzdPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('nzd');
          expect(symbol).to.equal('$');
        });
        const { plans: ilsPlans } = await simulateGetPricingPlans({
          brand: 'sl',
          countryISO: 'IL',
        });
        ilsPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('ils');
          expect(symbol).to.equal('₪');
        });
        const { plans: zaPlans } = await simulateGetPricingPlans({ brand: 'sl', countryISO: 'ZA' });
        zaPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('usd');
          expect(symbol).to.equal('$');
        });
      });

      it('should return the discounted pricing-plans for valid coupon codes', async () => {
        const { plans: discountedPlans } = await simulateGetPricingPlans({
          brand: 'sl',
          couponcode: '50-off-applies-to',
        });
        discountedPlans.forEach(({ amount, id }) => {
          if (id === 'sl_annual_v2') {
            expect(amount).to.equal(4794);
          }
        });
      });

      it('should return the correct pricing-plan currency per customer', async () => {
        getUserStub.returns({ ...stubbedUser });
        const { message } = await simulatePostBilling(theUserId, {
          plan: 'sl_annual_aud',
          source: theSource,
        });
        expect(message).to.equal('Successfully processed the Subscription');
        const { subscriptionId, stripeCustomerId, planId } = await simulateGetBilling(
          theUserId,
          'sl',
        );
        await simulateDeleteBilling(theUserId, subscriptionId);
        await simulateStripeEvent('customer.subscription.deleted', {
          stripeCustomerId,
          subscriptionId,
          planId,
        });
        getUserStub.returns({ ...stubbedUser });
        const { plans: audPlans } = await simulateGetPricingPlansWithAuth({ brand: 'sl' });
        audPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('aud');
          expect(symbol).to.equal('$');
        });
      });
    });
    describe('GET /pricing-plans/gifts', () => {
      it('should only return gift pricing plans', async () => {
        const { plans: giftPlans } = await simulateGetGiftPricingPlans({ brand: 'sl' });
        giftPlans.forEach(plan => {
          expect(plan.lookupKey).to.include('gift');
        });
      });

      it('should return the correct gift pricing-plans per brand', async () => {
        const { plans: slPlans } = await simulateGetGiftPricingPlans({ brand: 'sl' });
        slPlans.forEach(({ brand }) => {
          expect(brand).to.equal('sl');
        });
        const { plans: bwPlans } = await simulateGetGiftPricingPlans({ brand: 'bw' });
        bwPlans.forEach(({ brand }) => {
          expect(brand).to.equal('bw');
        });
        const { plans: fsPlans } = await simulateGetGiftPricingPlans({ brand: 'fs' });
        fsPlans.forEach(({ brand }) => {
          expect(brand).to.equal('fs');
        });
      });

      it('should return the correct gift pricing-plans per country', async () => {
        const { plans: usdPlans } = await simulateGetGiftPricingPlans({ brand: 'sl' });
        usdPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('usd');
          expect(symbol).to.equal('$');
        });
        const { plans: gbpPlans } = await simulateGetGiftPricingPlans({
          brand: 'sl',
          countryISO: 'GB',
        });
        gbpPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('gbp');
          expect(symbol).to.equal('£');
        });
        const { plans: eurPlans } = await simulateGetGiftPricingPlans({
          brand: 'sl',
          countryISO: 'FR',
        });
        eurPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('eur');
          expect(symbol).to.equal('€');
        });
        const { plans: audPlans } = await simulateGetGiftPricingPlans({
          brand: 'sl',
          countryISO: 'AU',
        });
        audPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('aud');
          expect(symbol).to.equal('$');
        });
        const { plans: nzdPlans } = await simulateGetGiftPricingPlans({
          brand: 'sl',
          countryISO: 'NZ',
        });
        nzdPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('nzd');
          expect(symbol).to.equal('$');
        });
        const { plans: ilsPlans } = await simulateGetGiftPricingPlans({
          brand: 'sl',
          countryISO: 'IL',
        });
        ilsPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('ils');
          expect(symbol).to.equal('₪');
        });
        const { plans: zaPlans } = await simulateGetGiftPricingPlans({
          brand: 'sl',
          countryISO: 'ZA',
        });
        zaPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('usd');
          expect(symbol).to.equal('$');
        });
      });

      it('should return the discounted pricing-plans for valid coupon codes', async () => {
        const { plans: discountedPlans } = await simulateGetGiftPricingPlans({
          brand: 'sl',
          couponcode: 'gift-50-off',
        });
        discountedPlans.forEach(({ renewalPrice, amount }) => {
          if (renewalPrice === 9588) {
            expect(amount).to.equal(4794);
          }
        });
      });

      it('should return the correct pricing-plan currency per customer', async () => {
        getUserStub.returns({ ...stubbedUser });
        const { message } = await simulatePostBilling(theUserId, {
          plan: 'sl_annual_aud',
          source: theSource,
        });
        expect(message).to.equal('Successfully processed the Subscription');
        const { subscriptionId, stripeCustomerId, planId } = await simulateGetBilling(
          theUserId,
          'sl',
        );
        await simulateDeleteBilling(theUserId, subscriptionId);
        await simulateStripeEvent('customer.subscription.deleted', {
          stripeCustomerId,
          subscriptionId,
          planId,
        });
        getUserStub.returns({ ...stubbedUser });
        const { plans: audPlans } = await simulateGetGiftPricingPlansWithAuth({ brand: 'sl' });
        audPlans.forEach(({ currency, symbol }) => {
          expect(currency).to.equal('aud');
          expect(symbol).to.equal('$');
        });
      });
    });
  });
}
