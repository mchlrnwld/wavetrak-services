import { expect } from 'chai';
import request from 'superagent';

export default function adminCouponTests(theUserId) {
  const servicesURI = 'http://localhost:3000';
  const adminCouponEndpoint = `${servicesURI}/admin/coupons`;

  const adminGetCoupon = couponId =>
    new Promise((resolve, reject) => {
      request
        .get(adminCouponEndpoint)
        .query({ id: couponId })
        .end((err, res) => {
          if (err) return reject(err);
          const { body } = res;
          return resolve(body);
        });
    });

  const adminCreateCoupon = params =>
    new Promise((resolve, reject) => {
      request
        .post(adminCouponEndpoint)
        .send({ ...params })
        .end((err, res) => {
          if (err) return reject(err);
          const { body } = res;
          return resolve(body);
        });
    });

  const adminDeleteCoupon = id =>
    new Promise((resolve, reject) => {
      request
        .delete(adminCouponEndpoint)
        .send({ id })
        .end((err, res) => {
          const { body } = res;
          if (err) return reject(body);
          return resolve(body);
        });
    });

  describe('Admin Coupon Tests', () => {
    const testCouponParams = {
      name: 'test',
      percentOff: 50,
      duration: 'once',
      maxRedemptions: 1,
      userId: theUserId,
    };

    it('should create an admin generated coupon', async () => {
      const { coupon } = await adminCreateCoupon({ ...testCouponParams });
      const {
        id,
        name,
        percent_off: percentOff,
        duration,
        metadata,
        max_redemptions: maxRedemptions,
      } = coupon;
      expect(id).to.equal(name);
      expect(name).to.equal('test');
      expect(percentOff).to.equal(50);
      expect(duration).to.equal('once');
      expect(maxRedemptions).to.equal(1);
      expect(metadata.origin).to.equal('admin');
      expect(metadata.userId).to.equal(theUserId.toString());

      await adminDeleteCoupon(id);
    });
    it('should retrieve coupon details', async () => {
      const {
        coupon: { id: newCouponId },
      } = await adminCreateCoupon({ ...testCouponParams });

      const { coupon } = await adminGetCoupon(newCouponId);
      const {
        id,
        name,
        percent_off: percentOff,
        duration,
        metadata,
        max_redemptions: maxRedemptions,
      } = coupon;
      expect(id).to.equal(name);
      expect(name).to.equal('test');
      expect(percentOff).to.equal(50);
      expect(duration).to.equal('once');
      expect(maxRedemptions).to.equal(1);
      expect(metadata.origin).to.equal('admin');
      expect(metadata.userId).to.equal(theUserId.toString());

      await adminDeleteCoupon(id);
    });
    it('should delete a coupon', async () => {
      const {
        coupon: { id },
      } = await adminCreateCoupon({ ...testCouponParams });
      const { message } = await adminDeleteCoupon(id);
      expect(message).to.equal(`Successfully deleted coupon: ${id}`);

      try {
        await adminGetCoupon(id);
      } catch (error) {
        expect(error.status).to.equal(500);
      }
    });
  });
}
