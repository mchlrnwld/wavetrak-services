import { expect } from 'chai';
import uuidV1 from 'uuid';
import request from 'superagent';
import { SubscriptionModel } from '../../../../src/models/subscriptions/SubscriptionModel';
import {
  getActiveSubscriptionsByUserId,
  deleteSubscriptions,
} from '../../../../src/models/subscriptions';

export default function adminGiftSubscriptionTests(theUserId) {
  const servicesURI = 'http://localhost:3000';
  const url = `${servicesURI}/admin/subscriptions/gifts`;
  const slMonthlyPriceId = 'price_1KMDRPLwFGcBvXawEz531QXD';

  const adminExpireGiftSubscription = subscriptionId =>
    new Promise((resolve, reject) => {
      request
        .delete(url)
        .send({ subscriptionId })
        .end((err, res) => {
          if (res?.status >= 500) return reject(err);
          const { body } = res;
          return resolve(body);
        });
    });

  const seedSubscription = async (brand, type, opts = {}) => {
    const subscription = await SubscriptionModel.create({
      type,
      subscriptionId: uuidV1(),
      entitlement: `${brand}_premium`,
      planId: slMonthlyPriceId,
      user: theUserId,
      start: 1579201980,
      end: new Date() / 1000,
      active: true,
      ...opts,
    });
    return subscription;
  };

  describe('Admin Gift Subscription Tests [Subscription API v2]', () => {
    afterEach(async () => {
      await deleteSubscriptions(theUserId);
    });

    it('should expire a gift subscription for a single user', async () => {
      const { subscriptionId } = await seedSubscription('sl', 'gift', {
        stripeCustomerId: 'cus_1234',
      });

      let subscriptions = await getActiveSubscriptionsByUserId(theUserId);
      expect(subscriptions.length).to.equal(1);

      const { message } = await adminExpireGiftSubscription(subscriptionId);
      expect(message).is.not.null();

      subscriptions = await getActiveSubscriptionsByUserId(theUserId);
      expect(subscriptions.length).to.equal(0);
    });

    it('should not expire a gift subscription that has not ended', async () => {
      const { subscriptionId } = await seedSubscription('sl', 'gift', {
        stripeCustomerId: 'cus_1234',
        end: 1894821181,
      });

      let subscriptions = await getActiveSubscriptionsByUserId(theUserId);
      expect(subscriptions.length).to.equal(1);

      const { message } = await adminExpireGiftSubscription(subscriptionId);
      expect(message).to.equal('no valid record found');

      subscriptions = await getActiveSubscriptionsByUserId(theUserId);
      expect(subscriptions.length).to.equal(1);
    });
  });
}
