/* eslint-disable global-require */

import request from 'superagent';
import sinon from 'sinon';
import { expect } from 'chai';
import { verifyGooglePurchase } from '../../helpers/simulateSubscription';
import {
  deleteSubscriptions,
  getActiveSubscriptionByUserIdAndEntitlement,
} from '../../../src/models/subscriptions';
import { SubscriptionPurchaseModel } from '../../../src/models/subscription-purchases/SubscriptionPurchaseModel';
import { getGooglePurchaseByPurchaseToken } from '../../../src/models/subscription-purchases/google';
import { getSubscriptionByGooglePackageNameAndUser } from '../../../src/models/subscriptions/google';
import * as googlePlayAPI from '../../../src/external/google';
import googlePurchaseData from '../../../src/__stubs__/google/BASE_PURCHASE_DATA';
import googleSignature from '../../../src/__stubs__/google/BASE_SIGNATUTRE';
import * as trackCancelledSubscription from '../../../src/tracking/cancelledSubscription';
import * as trackExpiredSubscription from '../../../src/tracking/expiredSubscription';
import * as orderCompleted from '../../../src/tracking/orderCompleted';

export default function googleNotificationTests(theUserId) {
  const servicesURI = 'http://localhost:3000';
  const googleNotificationHandler = `${servicesURI}/integrations/google/handler`;

  /**
   * This function removes all Subscription documents related to a single
   * user.  This should be run after each successful test block to
   * guarentee a clean test evnironment for each test case.
   */
  const tearDownTestSubscription = async () => deleteSubscriptions(theUserId);

  /**
   * This function removes all Subscription Purchase documents related
   * to a single user.  This should be run after each successful
   * test block to guarentee a clean test evnironment for each test case.
   */
  const tearDownTestSubscriptionPurchase = async user =>
    SubscriptionPurchaseModel.deleteOne({ user });

  /**
   * This function should be used to modify a Subscription document. For
   * example, if a test requires a subscription in an expiring state, the
   * test should create a new auto-renewing subscription and simulate the
   * webhook indicative of a user changing the auto-renewal status.  The
   * model functions should not be directly used to modify subscription
   * state.
   *
   * @param {String} notification - the desired Google notification (i.e. 'SUBSCRIPTION_RENEWAL').
   * @param {String} plan - the planId or productId associated defined on the Subscription.
   * @param {String} purchaseToken - the unique identifier used to fetch the Subscription.
   *
   * @returns {Object} - the API response object.
   *
   */
  const simulateGoogleNotification = async (notification, opts = {}) => {
    // eslint-disable-next-line import/no-dynamic-require
    const notificationStub = require(`../../../src/__stubs__/google/${notification}`);
    const notificationObject = notificationStub.default(opts);
    const encodedNotification = Buffer.from(JSON.stringify(notificationObject)).toString('base64');
    const requestPayload = {
      message: {
        data: encodedNotification,
      },
    };
    return new Promise((resolve, reject) => {
      request
        .post(googleNotificationHandler)
        .send(requestPayload)
        .end((err, response) => {
          if (err) reject(response);
          resolve(response.body);
        });
    });
  };

  const slGoogleMonthly = 'surfline.android.monthly.2022';

  describe('Google Purchase Notification Tests [Subscription API v2]', () => {
    let getPurchaseDataStub;
    let trackOrderCompletedSpy;

    beforeEach(() => {
      getPurchaseDataStub = sinon.stub(googlePlayAPI, 'default');
      trackOrderCompletedSpy = sinon.spy(orderCompleted, 'trackOrderCompleted');
    });

    afterEach(async () => {
      await tearDownTestSubscription();
      await tearDownTestSubscriptionPurchase(theUserId);
      getPurchaseDataStub.restore();
      trackOrderCompletedSpy.restore();
    });

    describe('Google SUBSCRIPTION_RECOVERED (1) notification tests', () => {
      it('should release the subscription from an alerted payment status', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);

        const {
          planId,
          entitlement,
          active: originalActive,
          expiring: originalExpiring,
          baseReceipt,
          alertPayment,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(originalActive).to.equal(true);
        expect(originalExpiring).to.equal(false);
        expect(alertPayment).to.equal(false);

        const {
          receipt: {
            data: { purchaseToken },
          },
        } = purchaseSignature;
        const {
          productId,
          purchaseToken: originalPurchaseToken,
        } = await getGooglePurchaseByPurchaseToken(purchaseToken);

        expect(productId).to.equal(planId);

        const onHoldPurchaseData = googlePurchaseData({ onHold: true });

        getPurchaseDataStub.returns(onHoldPurchaseData);
        await simulateGoogleNotification('05_SUBSCRIPTION_ON_HOLD', { purchaseToken });

        const {
          packageName,
          purchaseToken: purchaseTokenAfterUpdate,
          data: { paymentState, autoRenewing },
        } = await SubscriptionPurchaseModel.findOne({
          purchaseToken,
        });

        expect(purchaseTokenAfterUpdate).to.equal(originalPurchaseToken);
        expect(paymentState).to.equal(0);
        expect(autoRenewing).to.equal(true);

        const {
          expiring,
          active,
          alertPayment: updatedAlertPayment,
        } = await getSubscriptionByGooglePackageNameAndUser(packageName, theUserId, baseReceipt);

        expect(expiring).to.equal(false);
        expect(active).to.equal(true);
        expect(updatedAlertPayment).to.equal(true);

        const recoveredPurchaseData = googlePurchaseData({ recovered: true });

        getPurchaseDataStub.returns(recoveredPurchaseData);
        await simulateGoogleNotification('01_SUBSCRIPTION_RECOVERED', { purchaseToken });

        const {
          packageName: recoveredPackageName,
          purchaseToken: purchaseTokenAfterRecovered,
          data: { paymentState: recoveredPaymentState, autoRenewing: recoveredAutoRenewing },
        } = await SubscriptionPurchaseModel.findOne({
          purchaseToken,
        });

        expect(purchaseTokenAfterRecovered).to.equal(originalPurchaseToken);
        expect(recoveredPaymentState).to.equal(1);
        expect(recoveredAutoRenewing).to.equal(true);

        const {
          expiring: recoveredExpiring,
          active: recoveredActive,
          alertPayment: recoveredAlertPayment,
        } = await getSubscriptionByGooglePackageNameAndUser(
          recoveredPackageName,
          theUserId,
          baseReceipt,
        );

        expect(recoveredExpiring).to.equal(false);
        expect(recoveredActive).to.equal(true);
        expect(recoveredAlertPayment).to.equal(false);
      });
    });
    describe('Google SUBSCRIPTION_RENEWED (2) notification tests', () => {
      it('should update the subscription and purchase start and end time', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);

        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          planId,
          entitlement,
          active,
          expiring,
          end: originalEnd,
          baseReceipt,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(active).to.equal(true);
        expect(expiring).to.equal(false);

        const {
          receipt: {
            data: { purchaseToken },
          },
        } = purchaseSignature;
        const {
          productId,
          purchaseToken: originalPurchaseToken,
        } = await getGooglePurchaseByPurchaseToken(purchaseToken);

        expect(productId).to.equal(planId);

        const renewalPurchaseData = googlePurchaseData({
          renewal: true,
          linkedPurchaseToken: purchaseToken,
        });

        getPurchaseDataStub.returns(renewalPurchaseData);
        await simulateGoogleNotification('02_SUBSCRIPTION_RENEWED');

        const {
          packageName,
          data: { linkedPurchaseToken },
        } = await SubscriptionPurchaseModel.findOne({
          'data.linkedPurchaseToken': purchaseToken,
        });

        expect(linkedPurchaseToken).to.equal(originalPurchaseToken);

        const {
          start: renewalStart,
          planId: renewalPlanId,
        } = await getSubscriptionByGooglePackageNameAndUser(packageName, theUserId, baseReceipt);

        expect(Math.floor(originalEnd)).to.equal(Math.floor(renewalStart));
        expect(planId).to.equal(renewalPlanId);
      });
    });
    describe('Google SUBSCRIPTION_CANCELED (3) notificiation tests', () => {
      it('should update the purchase data and subscription expiring status', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();
        const trackSubscriptionSpy = sinon.spy(
          trackCancelledSubscription,
          'trackCancelledSubscription',
        );

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          planId,
          entitlement,
          active: originalActive,
          expiring: originalExpiring,
          start: originalStart,
          end: originalEnd,
          baseReceipt,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(originalActive).to.equal(true);
        expect(originalExpiring).to.equal(false);

        const {
          receipt: {
            data: { purchaseToken },
          },
        } = purchaseSignature;
        const {
          productId,
          purchaseToken: originalPurchaseToken,
        } = await getGooglePurchaseByPurchaseToken(purchaseToken);

        expect(productId).to.equal(planId);

        const cancellationPurchaseData = googlePurchaseData({
          cancellation: true,
        });

        getPurchaseDataStub.returns(cancellationPurchaseData);
        await simulateGoogleNotification('03_SUBSCRIPTION_CANCELED', { purchaseToken });
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        const {
          packageName,
          purchaseToken: purchaseTokenAfterUpdate,
          data: { cancelReason },
        } = await SubscriptionPurchaseModel.findOne({
          purchaseToken,
        });

        expect(purchaseTokenAfterUpdate).to.equal(originalPurchaseToken);

        const {
          start,
          end,
          expiring,
          active,
          cancellationReason,
        } = await getSubscriptionByGooglePackageNameAndUser(packageName, theUserId, baseReceipt);

        expect(start).to.equal(originalStart);
        expect(end).to.equal(originalEnd);
        expect(expiring).to.equal(true);
        expect(active).to.equal(true);
        expect(cancellationReason).to.equal(cancelReason);
        trackSubscriptionSpy.restore();
      });
    });
    describe('Google SUBSCRIPTION_PURCHASED (4) notification tests', () => {
      it('should track an Order Completed event', async () => {
        const purchaseSignature = googleSignature();
        const {
          receipt: { data },
        } = purchaseSignature;
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        await simulateGoogleNotification('04_SUBSCRIPTION_PURCHASED', data);

        expect(trackOrderCompletedSpy).to.have.been.calledOnce();
        expect(trackOrderCompletedSpy).to.have.been.calledWithMatch({
          data: {
            kind: 'androidpublisher#subscriptionPurchase',
            orderId: 'GPA.3327-5391-7183-13013..0',
          },
          productId: 'surfline.android.monthly.2022',
          type: 'google-play',
        });
      });
    });
    describe('Google SUBSCRIPTION_ON_HOLD (5) notification tests', () => {
      it('should update the purchase data and subscription alertPayment status', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          planId,
          entitlement,
          active: originalActive,
          expiring: originalExpiring,
          baseReceipt,
          alertPayment,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(originalActive).to.equal(true);
        expect(originalExpiring).to.equal(false);
        expect(alertPayment).to.equal(false);

        const {
          receipt: {
            data: { purchaseToken },
          },
        } = purchaseSignature;
        const {
          productId,
          purchaseToken: originalPurchaseToken,
        } = await getGooglePurchaseByPurchaseToken(purchaseToken);

        expect(productId).to.equal(planId);

        const onHoldPurchaseData = googlePurchaseData({ onHold: true });

        getPurchaseDataStub.returns(onHoldPurchaseData);
        await simulateGoogleNotification('05_SUBSCRIPTION_ON_HOLD', { purchaseToken });

        const {
          packageName,
          purchaseToken: purchaseTokenAfterUpdate,
          data: { paymentState, autoRenewing },
        } = await SubscriptionPurchaseModel.findOne({
          purchaseToken,
        });

        expect(purchaseTokenAfterUpdate).to.equal(originalPurchaseToken);
        expect(paymentState).to.equal(0);
        expect(autoRenewing).to.equal(true);

        const {
          expiring,
          active,
          alertPayment: updatedAlertPayment,
        } = await getSubscriptionByGooglePackageNameAndUser(packageName, theUserId, baseReceipt);

        expect(expiring).to.equal(false);
        expect(active).to.equal(true);
        expect(updatedAlertPayment).to.equal(true);
      });
    });
    describe('Google SUBSCRIPTION_DEFERRED', () => {
      it('should update the subscription and purchase end time', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);

        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          planId,
          entitlement,
          active,
          expiring,
          start: originalStart,
          end: originalEnd,
          baseReceipt,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(active).to.equal(true);
        expect(expiring).to.equal(false);

        const {
          receipt: {
            data: { purchaseToken },
          },
        } = purchaseSignature;
        const {
          productId,
          purchaseToken: originalPurchaseToken,
        } = await getGooglePurchaseByPurchaseToken(purchaseToken);

        expect(productId).to.equal(planId);

        const deferredPurchaseData = googlePurchaseData({
          deferred: true,
        });

        getPurchaseDataStub.returns(deferredPurchaseData);
        await simulateGoogleNotification('09_SUBSCRIPTION_DEFERRED', { purchaseToken });

        const {
          packageName,
          purchaseToken: samePurchaseToken,
        } = await SubscriptionPurchaseModel.findOne({
          purchaseToken,
        });

        expect(samePurchaseToken).to.equal(originalPurchaseToken);

        const {
          start: deferredStart,
          end: deferredEnd,
          planId: deferredPlanId,
        } = await getSubscriptionByGooglePackageNameAndUser(packageName, theUserId, baseReceipt);

        expect(Math.floor(originalStart)).to.equal(Math.floor(deferredStart));
        expect(planId).to.equal(deferredPlanId);
        expect(originalEnd).to.be.lessThan(deferredEnd);
      });
    });
    describe('Google SUBSCRIPTION_EXPIRED (13) notification tests', () => {
      it('should update the purchase data and subscription active status', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();
        const trackSubscriptionSpy = sinon.spy(
          trackExpiredSubscription,
          'trackExpiredSubscription',
        );

        getPurchaseDataStub.returns(purchaseData);

        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          planId,
          entitlement,
          active: originalActive,
          expiring: originalExpiring,
          start: originalStart,
          baseReceipt,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(originalActive).to.equal(true);
        expect(originalExpiring).to.equal(false);

        const {
          receipt: {
            data: { purchaseToken },
          },
        } = purchaseSignature;
        const {
          productId,
          purchaseToken: originalPurchaseToken,
        } = await getGooglePurchaseByPurchaseToken(purchaseToken);

        expect(productId).to.equal(planId);

        const expirationPurchaseData = googlePurchaseData({
          expiration: true,
        });

        getPurchaseDataStub.returns(expirationPurchaseData);
        await simulateGoogleNotification('13_SUBSCRIPTION_EXPIRED', { purchaseToken });
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        const {
          packageName,
          purchaseToken: purchaseTokenAfterExpriation,
          data: { cancelReason },
        } = await SubscriptionPurchaseModel.findOne({
          purchaseToken,
        });

        expect(purchaseTokenAfterExpriation).to.equal(originalPurchaseToken);

        const {
          start,
          expiring,
          active,
          cancellationReason,
        } = await getSubscriptionByGooglePackageNameAndUser(packageName, theUserId, baseReceipt);

        expect(start).to.equal(originalStart);
        expect(expiring).to.equal(true);
        expect(active).to.equal(false);
        expect(cancellationReason).to.equal(cancelReason);
        trackSubscriptionSpy.restore();
      });
    });
    describe('Google SUBSCRIPTION_REVOKED (12) notification tests', () => {
      it('should update the purchase data and subscription active status', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();
        const trackSubscriptionSpy = sinon.spy(
          trackExpiredSubscription,
          'trackExpiredSubscription',
        );

        getPurchaseDataStub.returns(purchaseData);

        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          planId,
          entitlement,
          active: originalActive,
          expiring: originalExpiring,
          start: originalStart,
          end: originalEnd,
          baseReceipt,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(originalActive).to.equal(true);
        expect(originalExpiring).to.equal(false);

        const {
          receipt: {
            data: { purchaseToken },
          },
        } = purchaseSignature;
        const {
          productId,
          purchaseToken: originalPurchaseToken,
        } = await getGooglePurchaseByPurchaseToken(purchaseToken);

        expect(productId).to.equal(planId);

        const revokedPurchaseData = googlePurchaseData({
          revoke: true,
        });

        getPurchaseDataStub.returns(revokedPurchaseData);
        await simulateGoogleNotification('12_SUBSCRIPTION_REVOKED', { purchaseToken });
        expect(trackSubscriptionSpy.callCount).to.equal(1);

        const {
          packageName,
          purchaseToken: purchaseTokenAfterExpriation,
          data: { cancelReason },
        } = await SubscriptionPurchaseModel.findOne({
          purchaseToken,
        });

        expect(purchaseTokenAfterExpriation).to.equal(originalPurchaseToken);

        const {
          start,
          end,
          expiring,
          active,
          cancellationReason,
        } = await getSubscriptionByGooglePackageNameAndUser(packageName, theUserId, baseReceipt);

        expect(start).to.equal(originalStart);
        expect(expiring).to.equal(true);
        expect(active).to.equal(false);
        expect(cancellationReason).to.equal(cancelReason);
        expect(end).to.not.equal(originalEnd);
        trackSubscriptionSpy.restore();
      });
    });
  });
}
