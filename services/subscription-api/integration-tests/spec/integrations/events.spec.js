/* eslint-disable prefer-promise-reject-errors */
import request from 'superagent';
import sinon from 'sinon';
import { expect } from 'chai';
import * as subscriptionModels from '../../../src/models/subscriptions';
import * as stripe from '../../../src/external/stripe';
import { stripeSubscriptionFixture } from '../../../src/tracking/tests/fixtures';
import * as trackUpcomingSubscriptionRenewal from '../../../src/tracking/upcomingSubscriptionRenewal';

export default function triggerSubscriptionEventTests() {
  const servicesURI = 'http://localhost:3000';
  const triggerSubscriptionEventAPI = `${servicesURI}/integrations/subscriptions/event`;
  let getSubscriptionFromCustomParamsStub;
  let getUpcomingInvoiceStub;
  let trackUpcomingSubscriptionRenewalSpy;

  /**
   * Simulates the /integration/subscription/event endpoint
   *
   * @param {String} eventName - the name of the event to be triggered
   * @param {Object} params - other request body parameters as needed depending on the event
   *
   * @returns {String} - message indicating success or an error
   */
  const simulateTriggerSubscriptionEvent = (eventName, params) => {
    return new Promise((resolve, reject) => {
      request
        .post(triggerSubscriptionEventAPI)
        .set('accept', 'json')
        .send({ eventName, ...params })
        .end(async (err, result) => {
          if (err) {
            const { statusCode, text } = result;
            reject({ statusCode, text });
          }
          resolve(result);
        });
    });
  };

  beforeEach(() => {
    getSubscriptionFromCustomParamsStub = sinon.stub(
      subscriptionModels,
      'getSubscriptionFromCustomParams',
    );
    getUpcomingInvoiceStub = sinon.stub(stripe, 'getUpcomingInvoice');
    trackUpcomingSubscriptionRenewalSpy = sinon.spy(
      trackUpcomingSubscriptionRenewal,
      'trackUpcomingSubscriptionRenewal',
    );
  });

  afterEach(async () => {
    getSubscriptionFromCustomParamsStub.restore();
    getUpcomingInvoiceStub.restore();
    trackUpcomingSubscriptionRenewalSpy.restore();
  });

  describe('Trigger Upcoming Subscription Renewal event', () => {
    it('should trigger the event with correct request body', async () => {
      const upcomingInvoice = {
        lines: { data: [{ price: { unit_amount: 9588 }, period: { start: 1643304062 } }] },
      };
      getSubscriptionFromCustomParamsStub.returns({
        ...stripeSubscriptionFixture,
        end: 1643304062,
      });
      getUpcomingInvoiceStub.returns(upcomingInvoice);
      const response = await simulateTriggerSubscriptionEvent('Upcoming Subscription Renewal', {
        subscriptionId: stripeSubscriptionFixture.subscriptionId,
      });
      expect(trackUpcomingSubscriptionRenewalSpy).to.have.been.calledWith(
        { ...stripeSubscriptionFixture, end: 1643304062 },
        {
          entitlement: stripeSubscriptionFixture.entitlement,
          invoiceRenewalDate: 1643304062,
          newInvoiceAmount: 95.88,
          platform: 'web',
        },
      );
      expect(response.statusCode).to.equal(200);
    });

    it('should throw an error if event name is not provided in request body', async () => {
      try {
        await simulateTriggerSubscriptionEvent();
      } catch (err) {
        expect(err.statusCode).to.equal(400);
        expect(err.text).to.equal('Event name required in request body');
        expect(trackUpcomingSubscriptionRenewalSpy).to.not.have.been.called();
      }
    });

    it('should throw an error if event name is incorrect', async () => {
      try {
        await simulateTriggerSubscriptionEvent('Non-existing event name', {
          subscriptionId: 'sub_12345',
        });
      } catch (err) {
        expect(err.statusCode).to.equal(400);
        expect(err.text).to.equal('Handler not found for event');
        expect(trackUpcomingSubscriptionRenewalSpy).to.not.have.been.called();
      }
    });

    it('should thow an error if no active subscription is found for the provided subscriptionId', async () => {
      getSubscriptionFromCustomParamsStub.rejects();
      try {
        await simulateTriggerSubscriptionEvent('Upcoming Subscription Renewal', {
          subscriptionId: 'sub_12345',
        });
      } catch (err) {
        expect(err.statusCode).to.equal(500);
        expect(err.text).to.equal('There was an error processing the request.');
      }
    });
  });
}
