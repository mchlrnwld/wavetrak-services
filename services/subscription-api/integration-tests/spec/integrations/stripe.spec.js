import request from 'superagent';
import sinon from 'sinon';
import { expect } from 'chai';
import simulateStripeEvent from '../../helpers/simulateStripeEvent';
import CreditCardModel from '../../../src/models/v1/credit-cards/CreditCardModel';
import { RedeemCodeModel } from '../../../src/models/gifts/RedeemCodeModel';
import {
  deleteStripeSubscriptions,
  getStripeSubscription,
} from '../../../src/models/subscriptions/stripe';

import * as trackStartedSubscription from '../../../src/tracking/startedSubscription';
import * as trackStartedFreeTrial from '../../../src/tracking/startedFreeTrial';
import * as trackChangedSubscriptionPlan from '../../../src/tracking/changedSubscriptionPlan';
import * as trackPaymentFailed from '../../../src/tracking/paymentFailed';
import * as trackPaymentSucceeded from '../../../src/tracking/paymentSucceeded';
import * as trackCheckoutSessionCompleted from '../../../src/tracking/checkoutSessionCompleted';
import * as trackCheckoutSessionExpired from '../../../src/tracking/checkoutSessionExpired';
import * as trackPurchasedGift from '../../../src/tracking/purchasedGift';
import * as orderCompleted from '../../../src/tracking/orderCompleted';
import * as getUser from '../../../src/external/user';
import * as stripeutils from '../../../src/external/stripeutils';
import * as stripe from '../../../src/external/stripe';
import * as stripeCustomers from '../../../src/models/customers/stripe';
import * as promotion from '../../../src/external/promotion';

export default function stripeIntegrationsTests(theUserId) {
  const servicesURI = 'http://localhost:3000';
  const stripeSubscriptionAPI = `${servicesURI}/subscriptions/billing`;
  const slAnnualPriceId = 'price_1KMDTZLwFGcBvXawjCayM4YC';
  const slMonthlyPriceId = 'price_1KMDRPLwFGcBvXawEz531QXD';
  const slGiftPrice = 'price_1KLXYXLwFGcBvXaw01JTOx0f';

  /**
   * This function removes all Subscription documents related to a single
   * user.  This should be run after each successful test block to
   * guarentee a clean test evnironment for each test case.
   */
  const teardownSubscription = async () => deleteStripeSubscriptions(theUserId);
  const teardownStripeCustomer = async () => stripeCustomers.removeStripeCustomer(theUserId);
  const teardownRedeemCodes = async () => RedeemCodeModel.deleteMany({});

  const simulateSubscriber = async (
    { plan, source } = { plan: slAnnualPriceId, source: '4242424242424242' },
  ) => {
    const { id: testToken } = await stripeutils.genTestToken(source);
    return new Promise((resolve, reject) => {
      request
        .post(stripeSubscriptionAPI)
        .type('json')
        .send({ planId: plan, source: testToken })
        .set('x-auth-userId', theUserId)
        .end((err, result) => {
          if (err) {
            reject(err);
          }
          resolve(result.body);
        });
    });
  };

  const simulateUpdateSubscription = async (
    subscriptionId,
    { planId, resume } = { planId: slMonthlyPriceId, resume: false },
  ) => {
    return new Promise((resolve, reject) => {
      request
        .put(stripeSubscriptionAPI)
        .type('json')
        .send({ subscriptionId, planId, resume })
        .set('x-auth-userId', theUserId)
        .end((err, result) => {
          if (err) {
            reject(err);
          }
          resolve(result.body);
        });
    });
  };

  const simulateCancelSubscription = async subscriptionId => {
    return new Promise((resolve, reject) => {
      request
        .delete(stripeSubscriptionAPI)
        .type('json')
        .send({ subscriptionId })
        .set('x-auth-userId', theUserId)
        .end((err, result) => {
          if (err) {
            reject(err);
          }
          resolve(result.body);
        });
    });
  };

  const requestActiveSubscription = async (product = 'sl') =>
    new Promise((resolve, reject) => {
      request
        .get(stripeSubscriptionAPI)
        .query({ product })
        .set('x-auth-userId', theUserId)
        .end((err, result) => {
          if (err) {
            reject(err);
          }
          resolve(result.body);
        });
    });

  describe('Stripe Event Tests [Subscription API v2]', () => {
    let getUserStub;
    let updateStripeCustomerStub;
    let trackOrderCompletedSpy;
    const stubbedUser = {
      _id: theUserId.toString(),
      firstName: 'Joel',
      lastName: 'Parkinson',
      email: 'joel@example.com',
      usID: 783514,
      __v: 0,
      locale: 'en-US',
      brand: 'sl',
      modifiedDate: '2016-05-31T22:45:32.092Z',
      isActive: true,
    };

    beforeEach(() => {
      getUserStub = sinon.stub(getUser, 'getUserDetails');
      updateStripeCustomerStub = sinon.stub(stripeCustomers, 'updateStripeCustomer');
      trackOrderCompletedSpy = sinon.spy(orderCompleted, 'trackOrderCompleted');
    });

    afterEach(async () => {
      getUserStub.restore();
      updateStripeCustomerStub.restore();
      trackOrderCompletedSpy.restore();
      await teardownSubscription();
      await teardownStripeCustomer();
      await teardownRedeemCodes();
      await CreditCardModel.deleteOne({});
    });

    describe('Stripe customer events', () => {
      it('should make a request to update the StripeCustomers', async () => {
        updateStripeCustomerStub.returns(true);
        getUserStub.returns(stubbedUser);

        await simulateSubscriber();
        const { stripeCustomerId } = await requestActiveSubscription();
        await simulateStripeEvent('customer.created', { stripeCustomerId }, { userId: theUserId });

        const [userId, customerId] = updateStripeCustomerStub.firstCall.args;
        expect(updateStripeCustomerStub.callCount).to.equal(1);
        expect(userId).to.equal(theUserId.toString());
        expect(customerId).to.equal(stripeCustomerId);
      });
    });

    describe('Stripe customer.subscription events', () => {
      it('subscription.created should create the necessary documents for the user', async () => {
        const trackSubscriptionSpy = sinon.spy(trackStartedFreeTrial, 'trackStartedFreeTrial');
        const getCustomerFromStripe = sinon.stub(stripe, 'getCustomer');
        const incrementRedemptionCountStub = sinon.spy(promotion, 'incrementRedemptionCount');
        const { _id: userId } = stubbedUser;
        getCustomerFromStripe.returns({
          metadata: {
            userId,
          },
        });

        const stripeCustomerId = 'cus_123456';
        const subscriptionId = 'sub_123456';
        await simulateStripeEvent(
          'customer.subscription.created',
          {
            stripeCustomerId,
            subscriptionId,
            price: {
              id: slAnnualPriceId,
            },
          },
          { status: 'trialing' },
        );

        const subscription = await requestActiveSubscription();
        expect(subscription.user).to.equal(userId);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(incrementRedemptionCountStub.callCount).to.equal(0);
        trackSubscriptionSpy.restore();
        getCustomerFromStripe.restore();
        incrementRedemptionCountStub.restore();
      });
      it('subscription.created should send free trial details to analytics', async () => {
        const trackSubscriptionSpy = sinon.spy(trackStartedFreeTrial, 'trackStartedFreeTrial');
        getUserStub.returns(stubbedUser);
        const getCustomerFromStripe = sinon.stub(stripe, 'getCustomer');
        const { _id: userId } = stubbedUser;
        getCustomerFromStripe.returns({
          metadata: {
            userId,
          },
        });

        const stripeCustomerId = 'cus_123456';
        const subscriptionId = 'sub_123456';
        await simulateStripeEvent(
          'customer.subscription.created',
          {
            stripeCustomerId,
            subscriptionId,
            price: { id: slAnnualPriceId },
          },
          { status: 'trialing' },
        );

        const [{ planId: argPlanId, user, trialing }] = trackSubscriptionSpy.firstCall.args;

        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(trackOrderCompletedSpy.callCount).to.equal(1);
        expect(user.toString()).to.equal(theUserId.toString());
        expect(slAnnualPriceId).to.equal(argPlanId);
        expect(trialing).to.equal(true);

        expect(trackSubscriptionSpy.callCount).to.equal(1);
        trackSubscriptionSpy.restore();
        getCustomerFromStripe.restore();
      });
      it('subscription.created should send subscription details to analytics', async () => {
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );
        getUserStub.returns(stubbedUser);
        const getCustomerFromStripe = sinon.stub(stripe, 'getCustomer');
        const { _id: userId } = stubbedUser;
        getCustomerFromStripe.returns({
          metadata: {
            userId,
          },
        });

        const stripeCustomerId = 'cus_123456';
        const subscriptionId = 'sub_123456';
        await simulateStripeEvent('customer.subscription.created', {
          stripeCustomerId,
          subscriptionId,
          price: { id: slMonthlyPriceId },
        });

        const [{ planId: argPlanId, user, trialing }] = trackSubscriptionSpy.firstCall.args;

        // expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(user.toString()).to.equal(theUserId.toString());
        expect(slMonthlyPriceId).to.equal(argPlanId);
        expect(trialing).to.equal(false);

        expect(trackSubscriptionSpy.callCount).to.equal(1);
        trackSubscriptionSpy.restore();
        getCustomerFromStripe.restore();
      });

      it('subscription.created should not attempt to create a duplicate StripeCustomer document', async () => {
        const getCustomerFromStripe = sinon.stub(stripe, 'getCustomer');
        const getStripeCustomerStub = sinon.stub(stripeCustomers, 'getStripeCustomerId');
        const createStripeCustomerSpy = sinon.spy(stripeCustomers, 'createStripeCustomer');
        const { _id: userId } = stubbedUser;
        getCustomerFromStripe.returns({
          metadata: {
            userId,
          },
        });
        getStripeCustomerStub.returns('cus_123456');

        const stripeCustomerId = 'cus_123456';
        const subscriptionId = 'sub_123456';
        await simulateStripeEvent('customer.subscription.created', {
          stripeCustomerId,
          subscriptionId,
          price: {
            id: slMonthlyPriceId,
          },
        });

        expect(createStripeCustomerSpy.callCount).to.be.equal(0);

        const subscription = await requestActiveSubscription();
        expect(subscription.user).to.equal(userId);

        getCustomerFromStripe.restore();
        getStripeCustomerStub.restore();
        createStripeCustomerSpy.restore();
      });

      it('subscription.created should error when attempting to create documents for user with an active subscription', async () => {
        getUserStub.returns(stubbedUser);
        await simulateSubscriber();
        const { stripeCustomerId, subscriptionId } = await requestActiveSubscription();
        try {
          await simulateStripeEvent('customer.subscription.created', {
            stripeCustomerId,
            subscriptionId,
            price: {
              id: slMonthlyPriceId,
            },
          });
        } catch (err) {
          expect(err.status).to.be.equal(422);
        }
      });

      it('subscription.created should call incrementRedemptionCount if there is a promotionId in the subscription metadata', async () => {
        const trackSubscriptionSpy = sinon.spy(trackStartedFreeTrial, 'trackStartedFreeTrial');
        const getCustomerFromStripe = sinon.stub(stripe, 'getCustomer');
        const incrementRedemptionCountStub = sinon.stub(promotion, 'incrementRedemptionCount');
        const { _id: userId } = stubbedUser;
        getCustomerFromStripe.returns({
          metadata: {
            userId,
          },
        });
        incrementRedemptionCountStub.returns({
          _id: '5b85b55b7d04491c0854e832',
          totalRedemption: 3,
        });
        const stripeCustomerId = 'cus_123456';
        const subscriptionId = 'sub_123456';
        await simulateStripeEvent(
          'customer.subscription.created',
          {
            stripeCustomerId,
            subscriptionId,
            price: {
              id: slAnnualPriceId,
            },
          },
          {
            promotionId: '5b85b55b7d04491c0854e832',
          },
        );

        expect(incrementRedemptionCountStub.callCount).to.equal(1);
        trackSubscriptionSpy.restore();
        getCustomerFromStripe.restore();
        incrementRedemptionCountStub.restore();
      });

      it('subscription.updated should change the billing plan of the subscription', async () => {
        const trackSubscriptionSpy = sinon.spy(
          trackChangedSubscriptionPlan,
          'trackChangedSubscriptionPlan',
        );
        getUserStub.returns(stubbedUser);

        await simulateSubscriber();
        const { stripeCustomerId, subscriptionId, planId } = await requestActiveSubscription();
        await simulateUpdateSubscription(subscriptionId);
        await simulateStripeEvent('customer.subscription.updated', {
          stripeCustomerId,
          subscriptionId,
        });

        const [{ planId: argPlanId, user }] = trackSubscriptionSpy.firstCall.args;

        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(user.toString()).to.equal(theUserId.toString());

        const { planId: updatedPlanId } = await requestActiveSubscription();

        expect(argPlanId).to.equal(updatedPlanId);
        expect(updatedPlanId).to.not.equal(planId);

        trackSubscriptionSpy.restore();
      });
      it('subscription.updated should resume the billing of a canceled subscription', async () => {
        getUserStub.returns(stubbedUser);
        await simulateSubscriber();

        const {
          stripeCustomerId,
          subscriptionId: trialingSubscriptionId,
          planId,
        } = await requestActiveSubscription();
        await simulateCancelSubscription(trialingSubscriptionId);
        await simulateStripeEvent('customer.subscription.deleted', {
          stripeCustomerId,
          subscriptionId: trialingSubscriptionId,
          price: { id: planId },
        });

        getUserStub.returns({ ...stubbedUser, stripeCustomerId });
        await simulateSubscriber();

        const {
          trialing,
          active,
          subscriptionId: nonTrialingSubscriptionId,
        } = await requestActiveSubscription();

        expect(trialingSubscriptionId).to.not.equal(nonTrialingSubscriptionId);
        expect(trialing).to.equal(false);
        expect(active).to.equal(true);

        await simulateCancelSubscription(nonTrialingSubscriptionId);
        await simulateStripeEvent(
          'customer.subscription.updated',
          {
            stripeCustomerId,
            subscriptionId: nonTrialingSubscriptionId,
            planId,
          },
          {
            planId,
            cancelAtPeriodEnd: true,
          },
        );

        const {
          subscriptionId: canceledSubscriptionId,
          active: canceledActiveState,
          expiring: canceledExpiringState,
        } = await requestActiveSubscription();
        expect(canceledExpiringState).to.equal(true);
        expect(canceledActiveState).to.equal(true);

        await simulateUpdateSubscription(canceledSubscriptionId, { planId, resume: true });
        await simulateStripeEvent(
          'customer.subscription.updated',
          {
            stripeCustomerId,
            subscriptionId: nonTrialingSubscriptionId,
            planId,
          },
          {
            planId,
            cancelAtPeriodEnd: false,
          },
        );

        const {
          subscriptionId: renewedSubscriptionId,
          active: renewedActiveState,
          expiring: renewedExpiringState,
        } = await requestActiveSubscription();

        expect(renewedSubscriptionId).to.equal(canceledSubscriptionId);
        expect(renewedActiveState).to.equal(true);
        expect(renewedExpiringState).to.equal(false);

        getUserStub.restore();
      });
      it('subscription.deleted should inactivate a subscription', async () => {
        getUserStub.returns(stubbedUser);
        await simulateSubscriber();

        const {
          stripeCustomerId,
          subscriptionId,
          planId,
          active,
        } = await requestActiveSubscription();

        expect(active).to.equal(true);

        await simulateCancelSubscription(subscriptionId);
        await simulateStripeEvent('customer.subscription.deleted', {
          stripeCustomerId,
          subscriptionId,
          price: { id: planId },
        });

        const { active: expiredActiveState } = await getStripeSubscription(subscriptionId);
        expect(expiredActiveState).to.equal(false);
        getUserStub.restore();
      });
    });

    describe('Stripe invoice events', () => {
      it('invoice.payment_failed should append payment warning to a subscription', async () => {
        const trackInvoiceSpy = sinon.spy(trackPaymentFailed, 'trackPaymentFailed');
        getUserStub.returns(stubbedUser);
        await simulateSubscriber();

        const {
          stripeCustomerId,
          subscriptionId,
          expiring,
          user,
          paymentAlertType,
          paymentAlertStatus,
          failedPaymentAttempts,
          alertPayment,
        } = await requestActiveSubscription();
        expect(alertPayment).to.equal(false);
        expect(expiring).to.equal(false);
        expect(paymentAlertType).to.equal(undefined);
        expect(paymentAlertStatus).to.equal(undefined);
        expect(failedPaymentAttempts).to.equal(0);

        await simulateStripeEvent(
          'invoice.payment_failed',
          { stripeCustomerId, subscriptionId },
          {},
        );
        const {
          expiring: failedExpiring,
          paymentAlertType: failedPaymentWarningType,
          paymentAlertStatus: failedPaymentWarningStatus,
          failedPaymentAttempts: failedFailedPaymentAttempts,
          alertPayment: failedAlertPayment,
        } = await requestActiveSubscription();

        expect(failedAlertPayment).to.equal(true);
        expect(failedExpiring).to.equal(true);
        expect(failedPaymentWarningType).to.equal('failedPayment');
        expect(failedPaymentWarningStatus).to.equal('open');
        expect(failedFailedPaymentAttempts).to.equal(1);

        const [
          { subscriptionId: argSubscriptionId, user: argUser },
        ] = trackInvoiceSpy.firstCall.args;
        expect(trackInvoiceSpy.callCount).to.equal(1);
        expect(argUser.toString()).to.equal(user.toString());
        expect(argSubscriptionId.toString()).to.equal(subscriptionId);

        trackInvoiceSpy.restore();
      });
      it('invoice.payment_succeeded should update the billing dates', async () => {
        const trackInvoiceSpy = sinon.spy(trackPaymentSucceeded, 'trackPaymentSucceeded');
        getUserStub.returns(stubbedUser);
        await simulateSubscriber({ plan: slMonthlyPriceId, source: '4242424242424242' });

        const { stripeCustomerId, subscriptionId, user, end } = await requestActiveSubscription();

        await simulateStripeEvent(
          'invoice.payment_succeeded',
          {
            stripeCustomerId,
            subscriptionId,
          },
          { periodStart: end },
        );
        const { start: nextStart, renewalCount } = await requestActiveSubscription();

        expect(end).to.equal(nextStart);
        expect(renewalCount).to.equal(1);

        const [
          { subscriptionId: argSubscriptionId, user: argUser },
        ] = trackInvoiceSpy.firstCall.args;
        expect(trackInvoiceSpy.callCount).to.equal(1);
        expect(argUser.toString()).to.equal(user.toString());
        expect(argSubscriptionId).to.equal(subscriptionId);

        trackInvoiceSpy.restore();
      });
      it('invoice.payment_succeeded resolve outstanding payment warnings', async () => {
        getUserStub.returns(stubbedUser);
        await simulateSubscriber();

        const {
          stripeCustomerId,
          subscriptionId,
          expiring,
          paymentAlertType,
          paymentAlertStatus,
          failedPaymentAttempts,
          alertPayment,
          end,
        } = await requestActiveSubscription();
        expect(alertPayment).to.equal(false);
        expect(expiring).to.equal(false);
        expect(paymentAlertType).to.equal(undefined);
        expect(paymentAlertStatus).to.equal(undefined);
        expect(failedPaymentAttempts).to.equal(0);

        await simulateStripeEvent(
          'invoice.payment_failed',
          { stripeCustomerId, subscriptionId },
          {},
        );
        const {
          expiring: failedExpiring,
          paymentAlertType: failedPaymentWarningType,
          paymentAlertStatus: failedPaymentWarningStatus,
          failedPaymentAttempts: failedFailedPaymentAttempts,
          alertPayment: failedAlertPayment,
        } = await requestActiveSubscription();

        expect(failedAlertPayment).to.equal(true);
        expect(failedExpiring).to.equal(true);
        expect(failedPaymentWarningType).to.equal('failedPayment');
        expect(failedPaymentWarningStatus).to.equal('open');
        expect(failedFailedPaymentAttempts).to.equal(1);

        await simulateStripeEvent(
          'invoice.payment_succeeded',
          {
            stripeCustomerId,
            subscriptionId,
          },
          { periodStart: end },
        );
        const {
          expiring: resolvedExpiring,
          paymentAlertStatus: resolvedPaymentWarningStatus,
          failedPaymentAttempts: resolvedFailedPaymentAttempts,
          renewalCount,
          alertPayment: resolvedAlertPayment,
        } = await requestActiveSubscription();

        expect(resolvedAlertPayment).to.equal(false);
        expect(resolvedExpiring).to.equal(false);
        expect(resolvedPaymentWarningStatus).to.equal('resolved');
        expect(resolvedFailedPaymentAttempts).to.equal(0);
        expect(renewalCount).to.equal(1);
      });
      it('invoice.payment_succeeded should have no effect when fired after subscription expires', async () => {
        const trackInvoiceSpy = sinon.spy(trackPaymentSucceeded, 'trackPaymentSucceeded');
        getUserStub.returns(stubbedUser);
        await simulateSubscriber({ plan: slMonthlyPriceId, source: '4242424242424242' });

        const { stripeCustomerId, subscriptionId, end, planId } = await requestActiveSubscription();

        await simulateCancelSubscription(subscriptionId);
        await simulateStripeEvent('customer.subscription.deleted', {
          stripeCustomerId,
          subscriptionId,
          price: { id: planId },
        });

        const { active: expiredActiveState } = await getStripeSubscription(subscriptionId);
        expect(expiredActiveState).to.equal(false);

        await simulateStripeEvent(
          'invoice.payment_succeeded',
          {
            stripeCustomerId,
            subscriptionId: null,
          },
          { periodStart: end },
        );
        const { active, renewalCount } = await getStripeSubscription(subscriptionId);

        expect(renewalCount).to.equal(0);
        expect(active).to.equal(false);
        expect(trackInvoiceSpy.callCount).to.equal(0);
        trackInvoiceSpy.restore();
      });
      it('invoice.payment_failed shoud have no effect when fired for non-subscription invoice items', async () => {
        getUserStub.returns(stubbedUser);
        await simulateSubscriber();

        const {
          stripeCustomerId,
          expiring,
          paymentAlertType,
          paymentAlertStatus,
          failedPaymentAttempts,
          alertPayment,
        } = await requestActiveSubscription();
        expect(alertPayment).to.equal(false);
        expect(expiring).to.equal(false);
        expect(paymentAlertType).to.equal(undefined);
        expect(paymentAlertStatus).to.equal(undefined);
        expect(failedPaymentAttempts).to.equal(0);

        await simulateStripeEvent(
          'invoice.payment_failed',
          {
            stripeCustomerId,
          },
          { invoiceItemType: 'invoiceItem' },
        );
        const {
          failedPaymentAttempts: noFailedPaymentAttempts,
        } = await requestActiveSubscription();
        expect(noFailedPaymentAttempts).to.equal(0);
      });
      it('invoice.payment_succeeded shoud have no effect when fired for non-subscription invoice items', async () => {
        getUserStub.returns(stubbedUser);
        await simulateSubscriber();

        const {
          stripeCustomerId,
          expiring,
          paymentAlertType,
          paymentAlertStatus,
          failedPaymentAttempts,
          alertPayment,
          start,
          end,
        } = await requestActiveSubscription();
        expect(alertPayment).to.equal(false);
        expect(expiring).to.equal(false);
        expect(paymentAlertType).to.equal(undefined);
        expect(paymentAlertStatus).to.equal(undefined);
        expect(failedPaymentAttempts).to.equal(0);

        await simulateStripeEvent(
          'invoice.payment_succeeded',
          {
            stripeCustomerId,
          },
          { invoiceItemType: 'invoiceItem' },
        );
        const { start: sameStart, end: sameEnd } = await requestActiveSubscription();
        expect(sameStart).to.equal(start);
        expect(sameEnd).to.equal(end);
      });
    });

    describe('Stripe charge.dispute events', () => {
      let getInvoiceFromChargeStub;
      beforeEach(() => {
        getInvoiceFromChargeStub = sinon.stub(stripe, 'getInvoiceFromCharge');
      });
      afterEach(() => {
        getInvoiceFromChargeStub.restore();
      });

      it('charge.dispute.created should append dispute attributes to the subscription', async () => {
        getUserStub.returns(stubbedUser);

        await simulateSubscriber({ plan: slMonthlyPriceId, source: '4242424242424242' });
        const { stripeCustomerId, subscriptionId } = await requestActiveSubscription();
        getInvoiceFromChargeStub.returns({
          customer: stripeCustomerId,
          subscription: subscriptionId,
        });

        await simulateStripeEvent('charge.dispute.created', { stripeCustomerId, subscriptionId });
        const {
          paymentAlertType,
          paymentAlertStatus,
          paymentAlertReason,
          alertPayment,
        } = await requestActiveSubscription();
        expect(alertPayment).to.equal(true);
        expect(paymentAlertType).to.equal('dispute');
        expect(paymentAlertStatus).to.equal('open');
        expect(paymentAlertReason).to.equal('fraudulent');
      });
      it('charge.dispute.closed should cancel the subscription immediate if lost', async () => {
        getUserStub.returns(stubbedUser);

        await simulateSubscriber({ plan: slMonthlyPriceId, source: '4242424242424242' });
        const { stripeCustomerId, subscriptionId } = await requestActiveSubscription();
        getInvoiceFromChargeStub.returns({
          customer: stripeCustomerId,
          subscription: subscriptionId,
        });
        await simulateStripeEvent('charge.dispute.created', { stripeCustomerId, subscriptionId });
        await simulateStripeEvent(
          'charge.dispute.closed',
          { stripeCustomerId, subscriptionId },
          { status: 'lost' },
        );

        const {
          paymentAlertType,
          paymentAlertStatus,
          paymentAlertReason,
          alertPayment,
        } = await requestActiveSubscription();
        expect(alertPayment).to.equal(false);
        expect(paymentAlertType).to.equal('dispute');
        expect(paymentAlertStatus).to.equal('lost');
        expect(paymentAlertReason).to.equal('fraudulent');
      });
      it('charge.dispute.closed should update the dispute attributes of the subscription if won', async () => {
        getUserStub.returns(stubbedUser);

        await simulateSubscriber({ plan: slMonthlyPriceId, source: '4242424242424242' });
        const { stripeCustomerId, subscriptionId } = await requestActiveSubscription();
        getInvoiceFromChargeStub.returns({
          customer: stripeCustomerId,
          subscription: subscriptionId,
        });
        await simulateStripeEvent('charge.dispute.created', { stripeCustomerId, subscriptionId });
        await simulateStripeEvent(
          'charge.dispute.closed',
          { stripeCustomerId, subscriptionId },
          { status: 'won' },
        );

        const {
          paymentAlertType,
          paymentAlertStatus,
          paymentAlertReason,
          alertPayment,
        } = await requestActiveSubscription();
        expect(alertPayment).to.equal(false);
        expect(paymentAlertType).to.equal('dispute');
        expect(paymentAlertStatus).to.equal('won');
        expect(paymentAlertReason).to.equal('fraudulent');
      });
    });

    describe('Stripe checkout events', () => {
      it('checkout.session.completed should trigger the Segment tracking call', async () => {
        const trackCheckoutSessionCompletedSpy = sinon.spy(
          trackCheckoutSessionCompleted,
          'trackCheckoutSessionCompleted',
        );
        getUserStub.returns(stubbedUser);
        await simulateSubscriber({ plan: slMonthlyPriceId, source: '4242424242424242' });
        const { stripeCustomerId, subscriptionId } = await requestActiveSubscription();
        await simulateStripeEvent('checkout.session.completed', {
          stripeCustomerId,
          subscriptionId,
        });
        expect(trackCheckoutSessionCompletedSpy.callCount).to.equal(1);
        const { customer: argCustomerId } = trackCheckoutSessionCompletedSpy.firstCall.args[1];
        expect(argCustomerId).to.equal(stripeCustomerId);
        trackCheckoutSessionCompletedSpy.restore();
      });

      it('checkout.session.expired should trigger the Segment tracking call', async () => {
        const trackCheckoutSessionExpiredSpy = sinon.spy(
          trackCheckoutSessionExpired,
          'trackCheckoutSessionExpired',
        );
        await simulateStripeEvent('checkout.session.expired', {
          stripeCustomerId: undefined,
          subscriptionId: undefined,
        });
        expect(trackCheckoutSessionExpiredSpy.callCount).to.equal(1);
        trackCheckoutSessionExpiredSpy.restore();
      });
    });

    describe('Stripe payment_intent events', () => {
      it('payment_intent.succeeded should create a gift code for gift-product related payment', async () => {
        const trackPurchasedGiftSpy = sinon.spy(trackPurchasedGift, 'trackPurchasedGift');
        await simulateStripeEvent(
          'payment_intent.succeeded',
          { customerId: null, subscriptionId: null, price: slGiftPrice },
          {},
        );
        const [
          { type, priceId, paymentIntentId, redeemCode, redeemed },
        ] = await RedeemCodeModel.find({});
        expect(type).to.equal('prices');
        expect(priceId).to.equal(slGiftPrice);
        expect(redeemed).to.equal(false);
        expect(redeemCode).to.exist(true);
        expect(paymentIntentId).to.exist(true);

        expect(trackPurchasedGiftSpy).to.have.been.calledOnce();

        trackPurchasedGiftSpy.restore();
      });
      it('payment_intent.succeeded should ignore non-gift-product related payment intents', async () => {
        const trackPurchasedGiftSpy = sinon.spy(trackPurchasedGift, 'trackPurchasedGift');
        await simulateStripeEvent(
          'payment_intent.succeeded',
          { customerId: null, subscriptionId: null },
          { invoiceId: 'in_abc1234' },
        );
        const redeemCodes = await RedeemCodeModel.find({});
        expect(redeemCodes.length).to.equal(0);

        expect(trackPurchasedGiftSpy).to.have.not.been.called();

        trackPurchasedGiftSpy.restore();
      });
      it('payment_intent.payment_failed should be ignored for non-gift-product related payment intents', async () => {
        const res = await simulateStripeEvent(
          'payment_intent.payment_failed',
          {
            customerId: null,
            subscriptionId: null,
          },
          { purchaseType: 'not-applicable' },
        );
        expect(res).to.deep.equal({});
      });
      it('payment_intent.payment_failed should return an error for gift-product related payment intents', async () => {
        try {
          await simulateStripeEvent(
            'payment_intent.payment_failed',
            {
              customerId: null,
              subscriptionId: null,
              price: slGiftPrice,
            },
            {},
          );
        } catch (error) {
          expect(error.status).to.equal(422);
        }
      });
      it('payment_intent.payment_failed should not if the gift-product code was created', async () => {
        try {
          await simulateStripeEvent(
            'payment_intent.payment_failed',
            {
              customerId: null,
              subscriptionId: null,
              price: slGiftPrice,
            },
            {},
          );
        } catch (error) {
          expect(error.status).to.equal(422);
        }
        await simulateStripeEvent(
          'payment_intent.succeeded',
          { customerId: null, subscriptionId: null, price: slGiftPrice },
          {},
        );
        const res = await simulateStripeEvent(
          'payment_intent.payment_failed',
          {
            customerId: null,
            subscriptionId: null,
            price: slGiftPrice,
          },
          {},
        );
        expect(res).to.deep.equal({});
      });
    });
  });
}
