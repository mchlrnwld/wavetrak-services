import request from 'superagent';
import sinon from 'sinon';
import { expect } from 'chai';
import {
  deleteSubscriptions,
  getActiveSubscriptionByUserIdAndEntitlement,
} from '../../../src/models/subscriptions';
import { SubscriptionPurchaseModel } from '../../../src/models/subscription-purchases/SubscriptionPurchaseModel';
import { getSubscriptionByItunesTransactionId } from '../../../src/models/subscriptions/itunes';
import * as itunesApi from '../../../src/external/itunes';
import iTunesReceiptData from '../../../src/__stubs__/itunes/BASE_RECEIPT';
import { keysToCamel } from '../../../src/utils/toCamelCase';
import * as trackCancelledSubscription from '../../../src/tracking/cancelledSubscription';
import * as trackCancelledFreeTrial from '../../../src/tracking/cancelledFreeTrial';
import * as trackExpiredSubscription from '../../../src/tracking/expiredSubscription';
import * as trackChangedSubscriptionPlan from '../../../src/tracking/changedSubscriptionPlan';
import * as orderCompleted from '../../../src/tracking/orderCompleted';

export default function iTunesNotificationTests(theUserId) {
  const servicesURI = 'http://localhost:3000';
  const iTunesNotificationHandler = `${servicesURI}/integrations/itunes/handler`;
  const iTunesSubscriptionHandler = `${servicesURI}/subscriptions/itunes`;

  /**
   * Verify iTunes Receipt
   *
   * This function should be used to create or update the subscription through
   * iTunes receipt validation.  This function mocks the response from the iTunes
   * store by stubbing the {getPurchaseData} request in the {processReceipt}
   * subscription-purchase model function.
   *
   *
   * @returns {Object} - the API response object
   */
  const verfifyItunesReceipt = async () =>
    new Promise((resolve, reject) => {
      request
        .post(iTunesSubscriptionHandler)
        .type('json')
        .send({ receipt: 'ABC123' })
        .set('x-auth-userid', theUserId)
        .end((err, res) => {
          if (err) return reject(err);
          return resolve(res.body);
        });
    });

  /**
   * This function removes all Subscription documents related to a single
   * user.  This should be run after each successful test block to
   * guarentee a clean test evnironment for each test case.
   */
  const tearDownTestSubscription = async () => deleteSubscriptions(theUserId);

  /**
   * This function removes all Subscription Purchase documents related
   * to a single user.  This should be run after each successful
   * test block to guarentee a clean test evnironment for each test case.
   */
  const tearDownTestSubscriptionPurchase = async user =>
    SubscriptionPurchaseModel.deleteOne({ user });

  /**
   * This function should be used to modify a Subscription document. For
   * example, if a test requires a subscription in an expiring state, the
   * test should create a new auto-renewing subscription and simulate the
   * webhook indicative of a user changing the auto-renewal status.  The
   * model functions should not be directly used to modify subscription
   * state.
   *
   * @param {String} notification - the desired iTunes notification (i.e. 'CANCEL').
   * @param {String} plan - the planId or productId associated defined on the Subscription.
   * @param {String} transactionId - the unique identifier used to fetch the Subscription.
   *
   * @returns {Object} - the API response object.
   *
   */
  const simulateItunesNotification = async (notification, opts) => {
    // eslint-disable-next-line max-len
    const notificationStub = require(`../../../src/__stubs__/itunes/${notification}`);
    const notificationObject = notificationStub.default(opts);

    return new Promise((resolve, reject) => {
      request
        .post(iTunesNotificationHandler)
        .send(notificationObject)
        .end((err, response) => {
          if (err) reject(err);
          resolve(response.body);
        });
    });
  };

  const slItunesMonthly = 'surfline.iosapp.1month.subscription.notrial';
  const slItunesYearly = 'surfline.iosapp.1year.subscription.notrial';
  const defaultItunesReceiptDataOpts = {
    productId: 'surfline.iosapp.1month.subscription.notrial',
    isTrialPeriod: 'false',
    transactionId: '201910100000',
    originalTransactionId: '201910100000',
  };

  describe('iTunes Notifications Tests [Subscription API v2]', () => {
    let getReceiptDataStub;
    let trackOrderCompletedSpy;

    beforeEach(() => {
      getReceiptDataStub = sinon.stub(itunesApi, 'default');
      trackOrderCompletedSpy = sinon.spy(orderCompleted, 'trackOrderCompleted');
    });

    afterEach(async () => {
      await tearDownTestSubscription();
      await tearDownTestSubscriptionPurchase(theUserId);
      getReceiptDataStub.restore();
      trackOrderCompletedSpy.restore();
    });

    describe('iTunes CANCEL notification tests', () => {
      it('should inactivate an active iTunes subscription', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));
        const trackSubscriptionSpy = sinon.spy(
          trackExpiredSubscription,
          'trackExpiredSubscription',
        );

        await verfifyItunesReceipt();
        const { transactionId, active, planId } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );
        expect(active).to.equal(true);

        await simulateItunesNotification('CANCEL', { planId, transactionId });
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        const updatedSubscription = await getSubscriptionByItunesTransactionId(transactionId);

        expect(updatedSubscription.transactionId).to.equal(transactionId);
        expect(updatedSubscription.active).to.equal(false);
        expect(updatedSubscription.cancellationReason).to.equal('0');
        expect(updatedSubscription.latestExpiredReceipt).to.not.equal(null);
        trackSubscriptionSpy.restore();
      });
    });

    describe('iTunes DID_CHANGE_RENEWAL_PREF notification tests', () => {
      it('should add the autoRenewProductId to an active iTunes subscription', async () => {
        const currentPlan = slItunesMonthly;
        const autoRenewPlan = slItunesYearly;
        const trackSubscriptionSpy = sinon.spy(
          trackChangedSubscriptionPlan,
          'trackChangedSubscriptionPlan',
        );
        const receiptData = iTunesReceiptData({
          ...defaultItunesReceiptDataOpts,
          productId: currentPlan,
        });
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt();
        const { transactionId, active, planId } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );

        expect(active).to.equal(true);
        expect(planId).to.equal(currentPlan);

        await simulateItunesNotification('DID_CHANGE_RENEWAL_PREF', {
          planId,
          autoRenewPlan,
          transactionId,
        });
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        const subscription = await getSubscriptionByItunesTransactionId(transactionId);

        expect(subscription.autoRenewProductId).to.equal(autoRenewPlan);
        expect(subscription.planId).to.equal(currentPlan);
        expect(subscription.latestReceipt).to.not.equal(null);
        expect(subscription.active).to.equal(true);
        trackSubscriptionSpy.restore();
      });
      it('should update the autoRenewProductId of an active iTunes subscription', async () => {
        const currentPlan = slItunesMonthly;
        const autoRenewPlan = slItunesYearly;
        const updatedAutoRenewPlan = slItunesMonthly;
        const trackSubscriptionSpy = sinon.spy(
          trackChangedSubscriptionPlan,
          'trackChangedSubscriptionPlan',
        );

        const receiptData = iTunesReceiptData({
          ...defaultItunesReceiptDataOpts,
          productId: currentPlan,
        });
        getReceiptDataStub.returns(keysToCamel(receiptData));
        await verfifyItunesReceipt();
        const { transactionId, active, planId } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );

        expect(active).to.equal(true);
        expect(planId).to.equal(currentPlan);

        await simulateItunesNotification('DID_CHANGE_RENEWAL_PREF', {
          planId,
          autoRenewPlan,
          transactionId,
        });
        const subscription = await getSubscriptionByItunesTransactionId(transactionId);

        expect(subscription.autoRenewProductId).to.equal(autoRenewPlan);
        expect(subscription.planId).to.equal(currentPlan);
        expect(subscription.latestReceipt).to.not.equal(null);

        await simulateItunesNotification('DID_CHANGE_RENEWAL_PREF', {
          planId,
          autoRenewPlan: updatedAutoRenewPlan,
          transactionId,
        });
        expect(trackSubscriptionSpy.callCount).to.equal(2);
        const updatedSubscription = await getSubscriptionByItunesTransactionId(transactionId);

        expect(updatedSubscription.autoRenewProductId).to.equal(updatedAutoRenewPlan);
        expect(updatedSubscription.planId).to.equal(currentPlan);
        expect(updatedSubscription.latestReceipt).to.not.equal(null);
        trackSubscriptionSpy.restore();
      });
    });

    describe('iTunes DID_CHANGE_RENWAL_STATUS notification tests', () => {
      it('should update the expiring status for non-auto-renewing iTunes subscriptions', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));
        const trackSubscriptionSpy = sinon.spy(
          trackCancelledSubscription,
          'trackCancelledSubscription',
        );

        await verfifyItunesReceipt();
        const {
          transactionId,
          active,
          planId,
          expiring,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(active).to.equal(true);
        expect(expiring).to.equal(false);

        await simulateItunesNotification('DID_CHANGE_RENEWAL_STATUS', {
          planId,
          transactionId,
          autoRenewStatus: 'false',
        });
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        const subscription = await getSubscriptionByItunesTransactionId(transactionId);

        expect(subscription.active).to.equal(true);
        expect(subscription.expiring).to.equal(true);
        expect(subscription.latestReceipt).to.not.equal(null);
        trackSubscriptionSpy.restore();
      });
      it('should update the expiring status for trialing iTunes subscriptions', async () => {
        const receiptData = iTunesReceiptData({
          ...defaultItunesReceiptDataOpts,
          isTrialPeriod: 'true',
        });
        getReceiptDataStub.returns(keysToCamel(receiptData));
        const trackSubscriptionSpy = sinon.spy(trackCancelledFreeTrial, 'trackCancelledFreeTrial');

        await verfifyItunesReceipt();
        const {
          transactionId,
          active,
          planId,
          expiring,
          trialing,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(active).to.equal(true);
        expect(expiring).to.equal(false);
        expect(trialing).to.equal(true);

        await simulateItunesNotification('DID_CHANGE_RENEWAL_STATUS', {
          planId,
          transactionId,
          autoRenewStatus: 'false',
        });
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        const subscription = await getSubscriptionByItunesTransactionId(transactionId);

        expect(subscription.active).to.equal(true);
        expect(subscription.expiring).to.equal(true);
        expect(subscription.trialing).to.equal(true);
        expect(subscription.latestReceipt).to.not.equal(null);
        trackSubscriptionSpy.restore();
      });
      it('should track an Order Completed event', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt();
        const { transactionId, planId } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );
        await simulateItunesNotification('INITIAL_BUY', {
          planId,
          transactionId,
        });

        expect(trackOrderCompletedSpy).to.have.been.calledOnce();
        expect(trackOrderCompletedSpy).to.have.been.calledWithMatch({
          original_transaction_id: '110000840906409',
          product_id: 'surfline.iosapp.1month.subscription.notrial',
        });
      });
      it('should update the expiring status for renewable iTunes subscriptions', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));
        const trackSubscriptionSpy = sinon.spy(
          trackCancelledSubscription,
          'trackCancelledSubscription',
        );

        await verfifyItunesReceipt();
        const {
          transactionId,
          active,
          planId,
          expiring,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(active).to.equal(true);
        expect(expiring).to.equal(false);

        await simulateItunesNotification('DID_CHANGE_RENEWAL_STATUS', {
          planId,
          transactionId,
          autoRenewStatus: 'false',
        });
        expect(trackSubscriptionSpy.callCount).to.equal(1);

        const expiringSubscription = await getSubscriptionByItunesTransactionId(transactionId);

        expect(expiringSubscription.active).to.equal(true);
        expect(expiringSubscription.expiring).to.equal(true);
        expect(expiringSubscription.latestReceipt).to.not.equal(null);

        await simulateItunesNotification('DID_CHANGE_RENEWAL_STATUS', {
          planId,
          transactionId,
          autoRenewStatus: 'true',
        });

        const subscription = await getSubscriptionByItunesTransactionId(transactionId);

        expect(subscription.active).to.equal(true);
        expect(subscription.expiring).to.equal(false);
        expect(subscription.latestReceipt).to.not.equal(null);
        trackSubscriptionSpy.restore();
      });
    });

    describe('iTunes INITIAL_BUY notification tests', () => {
      it('should add the latest receipt to an active iTunes subscription', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt();
        const { transactionId, active, planId } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );
        expect(active).to.equal(true);

        await simulateItunesNotification('INITIAL_BUY', {
          planId,
          transactionId,
        });

        const subscription = await getSubscriptionByItunesTransactionId(transactionId);

        expect(subscription.latestReceipt).to.not.equal(null);
      });
    });

    describe('iTunes INTERACTIVE_RENEWAL notificiation tests', () => {
      it('should add the latest receipt to an active iTunes subscription', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt();
        const { transactionId, active, planId } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );
        expect(active).to.equal(true);

        await simulateItunesNotification('INTERACTIVE_RENEWAL', {
          planId,
          transactionId,
        });

        const subscription = await getSubscriptionByItunesTransactionId(transactionId);

        expect(subscription.latestReceipt).to.not.equal(null);
        expect(subscription.active).to.equal(true);
      });
      it('should create a new subscription if an active itunes subscription does not exist', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt();
        const {
          active,
          planId,
          transactionId,
          originalTransactionId,
          entitlement,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(active).to.equal(true);

        await simulateItunesNotification('CANCEL', { planId, transactionId });

        const cancelledSubscription = await getSubscriptionByItunesTransactionId(transactionId);
        expect(cancelledSubscription.active).to.equal(false);

        const interactiveRenewalTransactionId = '20191014000';
        await simulateItunesNotification('INTERACTIVE_RENEWAL', {
          planId,
          transactionId: interactiveRenewalTransactionId,
          originalTransactionId,
        });

        const subscription = await getSubscriptionByItunesTransactionId(
          interactiveRenewalTransactionId,
        );
        expect(subscription.active).to.equal(true);
        expect(subscription.planId).to.equal(planId);
        expect(subscription.entitlement).to.equal(entitlement);
        expect(subscription.originalTransactionId).to.equal(originalTransactionId);
        expect(subscription.latestReceipt).to.not.equal(null);
        expect(subscription.trialing).to.equal(false);
      });
    });

    describe('iTunes DID_RECOVER notification tests', () => {
      it('should create or update a subscription with the same original transactionId', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt();
        const {
          active,
          planId,
          transactionId,
          originalTransactionId,
          end,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(active).to.equal(true);

        await simulateItunesNotification('DID_FAIL_TO_RENEW', {
          planId,
          transactionId,
          isInBillingRetryPeriod: '1',
        });

        const failedRenewalSubscription = await getSubscriptionByItunesTransactionId(transactionId);
        const { alertPayment, expiring, active: isStillActive } = failedRenewalSubscription;

        expect(alertPayment).to.equal(true);
        expect(expiring).to.equal(true);
        expect(isStillActive).to.equal(true);

        const recoveredTranasctionId = `${transactionId}001`;
        await simulateItunesNotification('DID_RECOVER', {
          planId,
          transactionId: recoveredTranasctionId,
          originalTransactionId,
        });

        const {
          active: recoveredActive,
          planId: recoveredPlanId,
          transactionId: recoveredTranasctionIdOnSub,
          originalTransactionId: recoveredOriginalTransactionId,
          end: recoveredEnd,
          alertPayment: recoveredAlertPayment,
        } = await getSubscriptionByItunesTransactionId(recoveredTranasctionId);

        expect(recoveredActive).to.equal(true);
        expect(recoveredOriginalTransactionId).to.equal(originalTransactionId);
        expect(recoveredPlanId).to.equal(planId);
        expect(recoveredEnd).to.not.equal(end);
        expect(recoveredTranasctionIdOnSub).to.not.equal(transactionId);
        expect(recoveredAlertPayment).to.equal(false);
      });
    });

    describe('iTunes DID_FAIL_TO_RENEW notification tests', () => {
      it('should should trigger a payment alert for an active iTunes subscription', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt();
        const {
          active,
          planId,
          transactionId,
          entitlement,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(active).to.equal(true);
        expect(entitlement).to.equal('sl_premium');

        await simulateItunesNotification('DID_FAIL_TO_RENEW', {
          planId,
          transactionId,
          isInBillingRetryPeriod: '1',
        });

        const failedRenewalSubscription = await getSubscriptionByItunesTransactionId(transactionId);
        const { alertPayment, expiring, active: isStillActive } = failedRenewalSubscription;

        expect(alertPayment).to.equal(true);
        expect(expiring).to.equal(true);
        expect(isStillActive).to.equal(true);
      });
      it('should should deactivate a subscription after all retries have failed', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt();
        const {
          active,
          planId,
          transactionId,
          entitlement,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(active).to.equal(true);
        expect(entitlement).to.equal('sl_premium');

        await simulateItunesNotification('DID_FAIL_TO_RENEW', {
          planId,
          transactionId,
          isInBillingRetryPeriod: '1',
        });

        const failedRenewalSubscription = await getSubscriptionByItunesTransactionId(transactionId);
        const { alertPayment, expiring, active: isStillActive } = failedRenewalSubscription;

        expect(alertPayment).to.equal(true);
        expect(expiring).to.equal(true);
        expect(isStillActive).to.equal(true);

        await simulateItunesNotification('DID_FAIL_TO_RENEW', {
          planId,
          transactionId,
          isInBillingRetryPeriod: '0',
        });

        const finalFailedRenewalSubscription = await getSubscriptionByItunesTransactionId(
          transactionId,
        );
        const {
          alertPayment: finalAlertPayment,
          expiring: finalExpriring,
          active: isActive,
        } = finalFailedRenewalSubscription;

        expect(finalAlertPayment).to.equal(false);
        expect(finalExpriring).to.equal(true);
        expect(isActive).to.equal(false);
      });
    });

    describe('iTunes DID_RENEW notification tests', () => {
      it('should update the transactionId, start, and end properties', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt();
        const {
          active,
          planId,
          transactionId,
          originalTransactionId,
          entitlement,
          start,
          end,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(active).to.equal(true);
        expect(entitlement).to.equal('sl_premium');

        await simulateItunesNotification('DID_RENEW', {
          plan: planId,
          transactionId,
        });

        const {
          active: renewedActive,
          planId: renewedPlanId,
          transactionId: renewedTransactionId,
          originalTransactionId: renewedOriginalTransactionId,
          entitlement: renewedEntitlement,
          start: renewedStart,
          end: renewedEnd,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        expect(renewedActive).to.equal(active);
        expect(renewedPlanId).to.equal(planId);
        expect(renewedTransactionId).to.not.equal(transactionId);
        expect(renewedOriginalTransactionId).to.equal(originalTransactionId);
        expect(renewedEntitlement).to.equal(entitlement);
        expect(renewedStart).to.not.equal(start);
        expect(renewedEnd).to.not.equal(end);
      });
    });
  });
}
