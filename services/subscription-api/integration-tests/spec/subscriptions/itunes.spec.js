import sinon from 'sinon';
import { expect } from 'chai';
import mongoose from 'mongoose';
import {
  simulateITunesPremiumPending,
  verfifyItunesReceipt,
  verifyDeprecatedV1ItunesReceipt,
} from '../../helpers/simulateSubscription';
import * as itunesApi from '../../../src/external/itunes';
import { keysToCamel } from '../../../src/utils/toCamelCase';
import {
  deleteSubscriptions,
  getActiveSubscriptionByUserIdAndEntitlement,
  getSubscriptionFromCustomParams,
} from '../../../src/models/subscriptions';
import iTunesReceiptData from '../../../src/__stubs__/itunes/BASE_RECEIPT';
import iTunesReceiptError from '../../../src/__stubs__/itunes/BASE_RECEIPT_ERROR';
import { createSubscriptionDate as createDate } from '../../helpers/testUtils';
import { getPendingSubscription } from '../../../src/models/subscriptions/pending';
import { SubscriptionPurchaseModel } from '../../../src/models/subscription-purchases/SubscriptionPurchaseModel';
import * as trackStartedSubscription from '../../../src/tracking/startedSubscription';
import * as trackStartedFreeTrial from '../../../src/tracking/startedFreeTrial';
import * as trackCancelledSubscription from '../../../src/tracking/cancelledSubscription';
import * as trackExpiredSubscription from '../../../src/tracking/expiredSubscription';
import * as trackChangedSubscriptionPlan from '../../../src/tracking/changedSubscriptionPlan';
import * as identifySubscription from '../../../src/tracking/identifySubscription';

export default function iTunesSubscriptionIntegrationTests(theUserId) {
  let getReceiptDataStub;
  let identifySubscriptionSpy;
  const slItunesMonthly = 'surfline.iosapp.1month.subscription.notrial';
  const slItunesYearly = 'surfline.iosapp.1year.subscription.notrial';
  const testReceiptString = 'ENCRYPTEDSTUBABC123';

  const defaultItunesReceiptDataOpts = {
    productId: 'surfline.iosapp.1month.subscription.notrial',
    isTrialPeriod: 'false',
    transactionId: '201910100000',
    originalTransactionId: '201910100000',
  };

  const teardownSubscription = async () => {
    await deleteSubscriptions(theUserId);
  };

  const tearDownTestSubscriptionPurchase = async () =>
    SubscriptionPurchaseModel.deleteOne({ user: theUserId });

  describe('iTunes Subscription Tests [Subscription API V2]', () => {
    beforeEach(async () => {
      getReceiptDataStub = sinon.stub(itunesApi, 'default');
      identifySubscriptionSpy = sinon.spy(identifySubscription, 'identifySubscription');
    });
    afterEach(async () => {
      getReceiptDataStub.restore();
      identifySubscriptionSpy.restore();
      await teardownSubscription();
      await tearDownTestSubscriptionPurchase();
    });
    describe('POST iTunes Premium Pending API', () => {
      it('should create a premium pending subscription status', async () => {
        await simulateITunesPremiumPending(theUserId, 'sl');
        const subscription = await getPendingSubscription({ user: theUserId, brand: 'sl' });
        const { type, planId, user, platform, entitlement } = subscription;
        expect(type).to.equal('pending');
        expect(planId).to.equal('sl_pending');
        expect(user.toString()).to.equal(theUserId.toString());
        expect(platform).to.equal('apple');
        expect(entitlement).to.equal('sl_pending');
      });
      it('should throw an error for duplicate pending subscription requests', async () => {
        await simulateITunesPremiumPending(theUserId, 'sl');
        try {
          await simulateITunesPremiumPending(theUserId, 'sl');
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;

          expect(statusCode).to.equal(400);
          expect(message).to.equal('There is an existing apple subscription pending.');
        }
      });
      it('should clear the pending premium subscription status', async () => {
        await simulateITunesPremiumPending(theUserId, 'sl');
        const subscription = await getPendingSubscription({ user: theUserId, brand: 'sl' });
        const { type, planId, user, platform, entitlement } = subscription;

        expect(type).to.equal('pending');
        expect(planId).to.equal('sl_pending');
        expect(user.toString()).to.equal(theUserId.toString());
        expect(platform).to.equal('apple');
        expect(entitlement).to.equal('sl_pending');

        await simulateITunesPremiumPending(theUserId, 'sl', true);
        const shouldEqualNull = await getPendingSubscription({ user: theUserId, brand: 'sl' });
        expect(shouldEqualNull).to.equal(null);
      });
    });
    describe('POST iTunes Subscription API', () => {
      it('should create a valid iTunes subscription', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );

        await verfifyItunesReceipt(theUserId, testReceiptString);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        const {
          type,
          active,
          planId,
          expiring,
          trialing,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(active).to.equal(true);
        expect(type).to.equal('apple');
        expect(expiring).to.equal(false);
        expect(planId).to.equal(slItunesMonthly);
        expect(trialing).to.equal(false);
        trackSubscriptionSpy.restore();
      });
      it('should create a valid trialing iTunes subscription', async () => {
        const receiptData = iTunesReceiptData({
          ...defaultItunesReceiptDataOpts,
          isTrialPeriod: true,
        });
        getReceiptDataStub.returns(keysToCamel(receiptData));
        const trackSubscriptionSpy = sinon.spy(trackStartedFreeTrial, 'trackStartedFreeTrial');

        await verfifyItunesReceipt(theUserId, testReceiptString);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        const {
          type,
          active,
          planId,
          expiring,
          trialing,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(active).to.equal(true);
        expect(type).to.equal('apple');
        expect(expiring).to.equal(false);
        expect(planId).to.equal(slItunesMonthly);
        expect(trialing).to.equal(true);
        trackSubscriptionSpy.restore();
      });
      it('should return an error for invalid itunes subscription requests', async () => {
        try {
          await verfifyItunesReceipt(theUserId);
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;

          expect(statusCode).to.equal(400);
          expect(message).to.equal('Cannot proccess subscription with a valid itunes receipt.');
        }
      });
      it('should return an error for duplicate receipt requests', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt(theUserId, testReceiptString);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        const {
          type,
          active,
          planId,
          expiring,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(active).to.equal(true);
        expect(type).to.equal('apple');
        expect(expiring).to.equal(false);
        expect(planId).to.equal(slItunesMonthly);
        try {
          const altUserId = mongoose.Types.ObjectId();
          await verfifyItunesReceipt(altUserId, testReceiptString);
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;

          expect(statusCode).to.equal(400);
          expect(message).to.equal('This receipt is subscribed to a different account');
        }
      });
      it('should clear the pending subscription for valid itunes subscription requests', async () => {
        await simulateITunesPremiumPending(theUserId, 'sl');
        const subscription = await getPendingSubscription({ user: theUserId, brand: 'sl' });
        const { type: pendingType, platform } = subscription;
        expect(pendingType).to.equal('pending');
        expect(platform).to.equal('apple');

        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt(theUserId, testReceiptString);
        const {
          type,
          active,
          planId,
          expiring,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(active).to.equal(true);
        expect(type).to.equal('apple');
        expect(expiring).to.equal(false);
        expect(planId).to.equal(slItunesMonthly);

        const shouldEqualNull = await getPendingSubscription({ user: theUserId, brand: 'sl' });
        expect(shouldEqualNull).to.equal(null);
      });
      it('should cancel the itunes subscription', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));
        const trackSubscriptionSpy = sinon.spy(
          trackCancelledSubscription,
          'trackCancelledSubscription',
        );

        await verfifyItunesReceipt(theUserId, testReceiptString);
        const { type, active, expiring } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );
        expect(active).to.equal(true);
        expect(type).to.equal('apple');
        expect(expiring).to.equal(false);

        const updatedReceiptData = iTunesReceiptData({
          ...defaultItunesReceiptDataOpts,
          autoRenewStatus: '0',
        });
        getReceiptDataStub.returns(keysToCamel(updatedReceiptData));

        await verfifyItunesReceipt(theUserId, testReceiptString);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(2);
        const {
          type: sameType,
          active: isActive,
          expiring: isExpiring,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(sameType).to.equal('apple');
        expect(isActive).to.equal(true);
        expect(isExpiring).to.equal(true);
        trackSubscriptionSpy.restore();
      });
      it('should cancel the itunes subscription if the payment is refunded', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));
        const trackSubscriptionSpy = sinon.spy(
          trackExpiredSubscription,
          'trackExpiredSubscription',
        );

        await verfifyItunesReceipt(theUserId, testReceiptString);
        const { type, active, expiring } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );
        expect(active).to.equal(true);
        expect(type).to.equal('apple');
        expect(expiring).to.equal(false);
        const updatedReceiptData = iTunesReceiptData({
          ...defaultItunesReceiptDataOpts,
          autoRenewStatus: '0',
          cancelledByApple: true,
        });
        getReceiptDataStub.returns(keysToCamel(updatedReceiptData));

        await verfifyItunesReceipt(theUserId, testReceiptString);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(2);
        const {
          type: sameType,
          active: isActive,
          expiring: isExpiring,
        } = await getSubscriptionFromCustomParams({
          user: theUserId,
          entitlement: 'sl_premium',
          active: false,
        });
        expect(sameType).to.equal('apple');
        expect(isActive).to.equal(false);
        expect(isExpiring).to.equal(true);
        trackSubscriptionSpy.restore();
      });
      it('should update the itunes subscription if the billing changes', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));
        const trackSubscriptionSpy = sinon.spy(
          trackChangedSubscriptionPlan,
          'trackChangedSubscriptionPlan',
        );

        await verfifyItunesReceipt(theUserId, testReceiptString);
        const { type, active, expiring } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );
        expect(active).to.equal(true);
        expect(type).to.equal('apple');
        expect(expiring).to.equal(false);

        const updatedReceiptData = iTunesReceiptData({
          ...defaultItunesReceiptDataOpts,
          productId: slItunesYearly,
        });
        getReceiptDataStub.returns(keysToCamel(updatedReceiptData));
        await verfifyItunesReceipt(theUserId, testReceiptString);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(2);
        const {
          type: sameType,
          active: isActive,
          expiring: isExpiring,
          planId: newPlanId,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(sameType).to.equal('apple');
        expect(isActive).to.equal(true);
        expect(isExpiring).to.equal(false);
        expect(newPlanId).to.equal(slItunesYearly);
        trackSubscriptionSpy.restore();
      });
      it('should expire the itunes subscription appropriately', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));
        const trackSubscriptionSpy = sinon.spy(
          trackExpiredSubscription,
          'trackExpiredSubscription',
        );

        await verfifyItunesReceipt(theUserId, testReceiptString);
        const { type, active, expiring } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );
        expect(active).to.equal(true);
        expect(type).to.equal('apple');
        expect(expiring).to.equal(false);

        const updatedReceiptData = iTunesReceiptData({
          ...defaultItunesReceiptDataOpts,
          expiresDateMs: createDate({ sub: 10 }),
          autoRenewStatus: '0',
        });
        getReceiptDataStub.returns(keysToCamel(updatedReceiptData));

        await verfifyItunesReceipt(theUserId, testReceiptString);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(2);
        const {
          type: sameType,
          active: isActive,
          expiring: isExpiring,
        } = await getSubscriptionFromCustomParams({
          user: theUserId,
          entitlement: 'sl_premium',
          active: false,
        });
        expect(sameType).to.equal('apple');
        expect(isExpiring).to.equal(true);
        expect(isActive).to.equal(false);
        trackSubscriptionSpy.restore();
      });
      it('should return an error is the receipt is not valid and no purchase record exists', async () => {
        const receiptData = iTunesReceiptError();
        getReceiptDataStub.returns(keysToCamel(receiptData));
        try {
          await verfifyItunesReceipt(theUserId, testReceiptString);
        } catch (error) {
          expect(error.status).to.equal(400);
        }
      });
      it('should expire the purchase and subscription if the receipt is not valid', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));
        const trackSubscriptionSpy = sinon.spy(
          trackExpiredSubscription,
          'trackExpiredSubscription',
        );

        await verfifyItunesReceipt(theUserId, testReceiptString);
        const {
          type,
          active,
          planId,
          expiring,
          originalTransactionId,
          latestReceipt,
          user,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(active).to.equal(true);
        expect(type).to.equal('apple');
        expect(expiring).to.equal(false);
        expect(planId).to.equal(slItunesMonthly);

        const receiptDataError = iTunesReceiptError();
        getReceiptDataStub.returns(keysToCamel(receiptDataError));

        await verfifyItunesReceipt(theUserId, latestReceipt);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(2);

        const expiredSubscription = await getSubscriptionFromCustomParams({
          user,
          originalTransactionId,
        });
        expect(expiredSubscription.active).to.equal(false);
        expect(expiredSubscription.expiring).to.equal(false);
        trackSubscriptionSpy.restore();
      });
    });
    describe('POST Deprecated v1 iTunes Subscription API', () => {
      it('should transform an active v2 Subscription to a v1 UserAccount', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        const account = await verifyDeprecatedV1ItunesReceipt(theUserId, testReceiptString);

        expect(account.subscriptions.length).to.equal(1);
        expect(account.subscriptions[0].entitlement).to.equal('sl_premium');
        expect(account.receipts.length).to.equal(1);

        const {
          type,
          active,
          planId,
          expiring,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(active).to.equal(true);
        expect(type).to.equal('apple');
        expect(expiring).to.equal(false);
        expect(planId).to.equal(slItunesMonthly);
      });
      it('should transform an inactive v2 Subscriptuon to a v1 UserAccount', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        const activeAccount = await verifyDeprecatedV1ItunesReceipt(theUserId, testReceiptString);
        expect(activeAccount.subscriptions.length).to.equal(1);
        expect(activeAccount.subscriptions[0].entitlement).to.equal('sl_premium');
        expect(activeAccount.receipts.length).to.equal(1);

        const { type, active, expiring } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );
        expect(active).to.equal(true);
        expect(type).to.equal('apple');
        expect(expiring).to.equal(false);

        const updatedReceiptData = iTunesReceiptData({
          ...defaultItunesReceiptDataOpts,
          expiresDateMs: createDate({ sub: 10 }),
          autoRenewStatus: '0',
        });
        getReceiptDataStub.returns(keysToCamel(updatedReceiptData));

        const inactiveAccount = await verifyDeprecatedV1ItunesReceipt(theUserId, testReceiptString);
        expect(inactiveAccount.subscriptions.length).to.equal(0);
        expect(inactiveAccount.archivedSubscriptions.length).to.equal(1);
        expect(inactiveAccount.archivedSubscriptions[0].entitlement).to.equal('sl_premium');
        expect(inactiveAccount.receipts.length).to.equal(1);
        const {
          type: sameType,
          active: isActive,
          expiring: isExpiring,
        } = await getSubscriptionFromCustomParams({
          user: theUserId,
          entitlement: 'sl_premium',
          active: false,
        });
        expect(sameType).to.equal('apple');
        expect(isExpiring).to.equal(true);
        expect(isActive).to.equal(false);
      });
    });
  });
}
