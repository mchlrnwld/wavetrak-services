import sinon from 'sinon';
import { expect } from 'chai';
import mongoose from 'mongoose';
import * as getUser from '../../../src/external/user';
import { genTestToken } from '../../../src/external/stripeutils';
import {
  simulatePostBilling,
  simulateGetBilling,
  simulateGetOfferSubscription,
  simulatePostOfferSubscription,
} from '../../helpers/simulateSubscription';
import simulateStripeEvent from '../../helpers/simulateStripeEvent';
import CreditCardModel from '../../../src/models/v1/credit-cards/CreditCardModel';
import { SubscriptionModel } from '../../../src/models/subscriptions/SubscriptionModel';
import * as stripe from '../../../src/external/stripe';
import * as getPromotion from '../../../src/external/promotion';
import { deleteStripeSubscriptions } from '../../../src/models/subscriptions/stripe';
import * as stripeCustomer from '../../../src/models/customers/stripe';
import * as trackStartedSubscription from '../../../src/tracking/startedSubscription';
import * as trackChangedSubscriptionPlan from '../../../src/tracking/changedSubscriptionPlan';

export default function offerSubscriptionIntegrationTests(theUserId) {
  let getUserStub;
  let getStripeCustomerIdStub;
  let getOfferStub;
  let getCouponStub;
  let incrementRedemptionCountStub;
  let theStripeCustomerId;
  const theOfferId = mongoose.Types.ObjectId().toString();

  const stubbedUser = {
    _id: theUserId.toString(),
    firstName: 'Joel',
    lastName: 'Parkinson',
    email: 'joel@example.com',
    usID: 783514,
  };

  const stubbedOffer = {
    _id: theOfferId,
    name: 'Stubbed Test Offer',
    brand: 'sl',
    isActive: true,
  };

  const stubbedCoupon = {
    id: 'month_two',
    amount_off: 2000,
    metadata: {
      promotion: theOfferId,
      savings: '20',
    },
  };

  const createTestStripeCustomer = async () => {
    const { email } = stubbedUser;
    const { id: tokenId } = await genTestToken();
    const res = await stripe.createCustomer({
      source: tokenId,
      email,
    });
    await stripeCustomer.createStripeCustomer(theUserId, res.id);
    return res;
  };

  const teardownSubscription = async () => {
    await deleteStripeSubscriptions(theUserId);
    if (theStripeCustomerId) {
      await stripe.deleteCustomer(theStripeCustomerId);
    }
  };

  const seedSubscription = async (brand, type, opts = {}) => {
    const subscription = await SubscriptionModel.create({
      type,
      subscriptionId: 'test123',
      entitlement: `${brand}_premium`,
      planId: `${brand}_monthly`,
      user: theUserId,
      start: 1579201980,
      end: 1894821181,
      active: true,
      ...opts,
    });
    return subscription;
  };

  describe('Offer Subscription Tests [Subscription API V2]', () => {
    beforeEach(async () => {
      getUserStub = sinon.stub(getUser, 'getUserDetails');
      getOfferStub = sinon.stub(getPromotion, 'getPromotion');
      getCouponStub = sinon.stub(stripe, 'getCoupon');
      incrementRedemptionCountStub = sinon.stub(getPromotion, 'incrementRedemptionCount');
    });
    afterEach(async () => {
      getCouponStub.restore();
      getUserStub.restore();
      getOfferStub.restore();
      incrementRedemptionCountStub.restore();
      Promise.all([
        CreditCardModel.deleteOne(),
        teardownSubscription(),
        stripeCustomer.removeStripeCustomer(theUserId),
      ]);
      theStripeCustomerId = null;
    });
    describe('GET Offer Subscription API', () => {
      it('should throw a 1001 error code if the offer does not exist', async () => {
        getUserStub.returns({ ...stubbedUser });
        try {
          getOfferStub.returns(null);
          await simulateGetOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(code).to.equal(1001);
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Invalid user request');
        }
      });
      it('should throw a 1001 error code if the user does not exist', async () => {
        getUserStub.returns(null);
        getOfferStub.returns(stubbedOffer);
        try {
          await simulateGetOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Invalid user request');
          expect(code).to.equal(1001);
        }
      });
      it('should throw a 1002 error code if the eligibility status is invalid', async () => {
        getOfferStub.returns({ ...stubbedOffer, eligibleStatus: 'expired' });
        getUserStub.returns(stubbedUser);
        try {
          await simulatePostBilling(theUserId);
          await simulateGetOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Invalid offer request');
          expect(code).to.equal(1002);
        }
      });
      it('should throw a 1003 error code if a subscription does not match the eligible status params', async () => {
        getUserStub.returns(stubbedUser);
        getOfferStub.returns({
          ...stubbedOffer,
          eligiblePlan: 'sl_annual_v2',
          eligibleStatus: 'expiring',
        });
        try {
          await simulatePostBilling(theUserId);
          await simulateGetOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Ineligible offer request');
          expect(code).to.equal(1003);
        }
      });
      it('should throw a 1003 error code if a subscription does not match the eligible plan params', async () => {
        getUserStub.returns(stubbedUser);
        getOfferStub.returns({
          ...stubbedOffer,
          eligiblePlan: 'sl_monthly_v2',
          eligibleStatus: 'active',
        });
        try {
          await simulatePostBilling(theUserId);
          await simulateGetOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Ineligible offer request');
          expect(code).to.equal(1003);
        }
      });
      it('should throw a 1003 error code if a subscription does not match the eligible product params', async () => {
        getUserStub.returns(stubbedUser);
        getOfferStub.returns({
          ...stubbedOffer,
          eligiblePlan: 'bw_monthly_v2',
          eligibleStatus: 'active',
          brand: 'bw',
        });
        try {
          await simulatePostBilling(theUserId);
          await simulateGetOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Ineligible offer request');
          expect(code).to.equal(1003);
        }
      });
      it('should throw a 1003 error code if a subscription does not match the eligible target params', async () => {
        getUserStub.returns(stubbedUser);
        getOfferStub.returns({
          ...stubbedOffer,
          eligibleStatus: 'active',
          eligiblePlan: 'sl_monthly_v2',
          eligibilityTarget: 2,
          eligibilityType: 'renewals',
        });
        try {
          await simulatePostBilling(theUserId, {
            plan: 'sl_monthly_v2',
            source: '4242424242424242',
          });
          await simulateGetOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Ineligible offer request');
          expect(code).to.equal(1003);
        }
      });
      it('should throw a 1004 error code if the offer coupon in Stripe is not eligible', async () => {
        getUserStub.returns(stubbedUser);
        getOfferStub.returns({
          ...stubbedOffer,
          eligibleStatus: 'active',
          eligiblePlan: 'sl_annual_v2',
          eligibilityTarget: 0,
        });
        getCouponStub.returns({ ...stubbedCoupon, metadata: { promotion: 'invalid_id' } });
        try {
          await simulatePostBilling(theUserId, {
            plan: 'sl_annual_v2',
            source: '4242424242424242',
          });
          await simulateGetOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Ineligible offer request');
          expect(code).to.equal(1004);
        }
      });
      it('should return the pricing details for an active subscription offer', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        getUserStub.returns({ ...stubbedUser });
        getOfferStub.returns({
          ...stubbedOffer,
          eligibleStatus: 'active',
          eligiblePlan: 'sl_monthly_v2',
          eligibilityTarget: 0,
          offeredPlan: 'sl_annual_v2',
          stripeCouponCode: 'month_two',
        });
        getCouponStub.returns({
          ...stubbedCoupon,
          metadata: { promotion: theOfferId, savings: '45' },
        });
        await simulatePostBilling(theUserId, {
          plan: 'sl_monthly_v2',
          source: '4242424242424242',
        });
        const {
          currentAnnualCost,
          currentPlan,
          offeredPlan,
          totalCredits,
          renewalRate,
          offeredSavings,
          totalCost,
        } = await simulateGetOfferSubscription(theUserId, theOfferId);

        expect(currentAnnualCost).to.equal(11988);
        expect(currentPlan).to.equal('sl_monthly_v2');
        expect(offeredPlan).to.equal('sl_annual_v2');
        expect(totalCredits).to.equal(999);
        expect(renewalRate).to.equal(9588);
        expect(offeredSavings).to.equal(4500);
        const expectedTotalCost = totalCost <= 9588 - 2000;
        expect(expectedTotalCost).to.equal(true);
      });
      it('should return the pricing details for an expired Subscription Offer', async () => {
        getUserStub.returns({ ...stubbedUser });
        getOfferStub.returns({
          ...stubbedOffer,
          eligibleStatus: 'expired',
          eligiblePlan: 'surfline.iosapp.1month.subscription',
          eligibilityTarget: 0,
          offeredPlan: 'sl_annual_v2',
          stripeCouponCode: 'month_two',
        });
        getCouponStub.returns({
          ...stubbedCoupon,
          metadata: { promotion: theOfferId, savings: '45' },
        });
        await seedSubscription('sl', 'apple', {
          planId: 'surfline.iosapp.1month.subscription',
          active: false,
          expried: true,
          originalTransactionId: '1234',
          transactionId: '1234',
        });

        const {
          currentAnnualCost,
          currentPlan,
          offeredPlan,
          totalCredits,
          renewalRate,
          offeredSavings,
          totalCost,
        } = await simulateGetOfferSubscription(theUserId, theOfferId);

        expect(currentAnnualCost).to.equal(11988);
        expect(currentPlan).to.equal('surfline.iosapp.1month.subscription');
        expect(offeredPlan).to.equal('sl_annual_v2');
        expect(totalCredits).to.equal(0);
        expect(renewalRate).to.equal(9588);
        expect(offeredSavings).to.equal(4500);
        const expectedTotalCost = totalCost <= 9588 - 2000;
        expect(expectedTotalCost).to.equal(true);
      });
    });
    describe('POST Offer Subscription API', () => {
      it('should throw a 1001 error code if the user does not exist', async () => {
        getUserStub.returns({ ...stubbedUser });
        try {
          getOfferStub.returns(null);
          await simulatePostOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Invalid user request');
          expect(code).to.equal(1001);
        }
      });
      it('should throw a 1002 error code if the eligibility status is invalid', async () => {
        getOfferStub.returns({ ...stubbedOffer, eligibleStatus: 'expired' });
        getUserStub.returns(stubbedUser);
        try {
          await simulatePostBilling(theUserId);
          await simulatePostOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Invalid offer request');
          expect(code).to.equal(1002);
        }
      });
      it('should throw a 1003 error code if a subscription does not match the eligible status params', async () => {
        getUserStub.returns(stubbedUser);
        getOfferStub.returns({
          ...stubbedOffer,
          eligiblePlan: 'sl_annual_v2',
          eligibleStatus: 'expiring',
        });
        try {
          await simulatePostBilling(theUserId);
          await simulatePostOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Ineligible offer request');
          expect(code).to.equal(1003);
        }
      });
      it('should throw a 1003 error code if a subscription does not match the eligible plan params', async () => {
        getUserStub.returns(stubbedUser);
        getOfferStub.returns({
          ...stubbedOffer,
          eligiblePlan: 'sl_monthly_v2',
          eligibleStatus: 'active',
        });
        try {
          await simulatePostBilling(theUserId);
          await simulatePostOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Ineligible offer request');
          expect(code).to.equal(1003);
        }
      });
      it('should throw a 1003 error code if a subscription does not match the eligible product params', async () => {
        getUserStub.returns(stubbedUser);
        getOfferStub.returns({
          ...stubbedOffer,
          eligiblePlan: 'bw_monthly_v2',
          eligibleStatus: 'active',
          brand: 'bw',
        });
        try {
          await simulatePostBilling(theUserId);
          await simulatePostOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Ineligible offer request');
          expect(code).to.equal(1003);
        }
      });
      it('should throw a 1003 error code if a subscription does not match the eligible target params', async () => {
        getUserStub.returns(stubbedUser);
        getOfferStub.returns({
          ...stubbedOffer,
          eligibleStatus: 'active',
          eligiblePlan: 'sl_monthly_v2',
          eligibilityTarget: 2,
          eligibilityType: 'renewals',
        });
        try {
          await simulatePostBilling(theUserId, {
            plan: 'sl_monthly_v2',
            source: '4242424242424242',
          });
          await simulatePostOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Ineligible offer request');
          expect(code).to.equal(1003);
        }
      });
      it('should throw a 1004 error code if the offer coupon in Stripe is not eligible', async () => {
        getUserStub.returns(stubbedUser);
        getOfferStub.returns({
          ...stubbedOffer,
          eligibleStatus: 'active',
          eligiblePlan: 'sl_annual_v2',
          eligibilityTarget: 0,
        });
        getCouponStub.returns({ ...stubbedCoupon, metadata: { promotion: 'invalid_id' } });
        try {
          await simulatePostBilling(theUserId, {
            plan: 'sl_annual_v2',
            source: '4242424242424242',
          });
          await simulatePostOfferSubscription(theUserId, theOfferId);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Ineligible offer request');
          expect(code).to.equal(1004);
        }
      });
      it('should create a new customer and subscription for the offered plan and price', async () => {
        getUserStub.returns(stubbedUser);
        getOfferStub.returns({
          ...stubbedOffer,
          eligibleStatus: 'expired',
          eligiblePlan: 'surfline.iosapp.1month.subscription',
          eligibilityTarget: 0,
          offeredPlan: 'sl_annual_v2',
          stripeCouponCode: 'month_two',
        });
        getCouponStub.returns({
          ...stubbedCoupon,
          metadata: { promotion: theOfferId, savings: '45' },
        });
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );
        await seedSubscription('sl', 'apple', {
          planId: 'surfline.iosapp.1month.subscription',
          active: false,
          expried: true,
          originalTransactionId: '1234',
          transactionId: '1234',
        });

        await simulatePostOfferSubscription(theUserId, theOfferId, { source: '4242424242424242' });
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        const { stripeCustomerId } = await simulateGetBilling(theUserId, 'sl');
        const {
          data: [
            {
              discount: {
                coupon: { id: couponId },
              },
              amount_paid: amountPaid,
            },
          ],
        } = await stripe.getInvoices(stripeCustomerId);
        expect(amountPaid).to.equal(7588);
        expect(couponId).to.equal('month_two');
        trackSubscriptionSpy.restore();
      });
      it('should create a new subscription for the expired user at the offered plan and price', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        await stripe.updateCustomer(stripeCustomerId, { balance: -2000 });
        theStripeCustomerId = stripeCustomerId;
        getUserStub.returns({ ...stubbedUser });
        getOfferStub.returns({
          ...stubbedOffer,
          eligibleStatus: 'expired',
          eligiblePlan: 'sl_monthly_v2',
          eligibilityTarget: 2,
          eligibilityType: 'renewals',
          offeredPlan: 'sl_annual_v2',
          stripeCouponCode: 'month_two',
        });
        getCouponStub.returns({
          ...stubbedCoupon,
          metadata: { promotion: theOfferId, savings: '45' },
        });
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );
        await seedSubscription('sl', 'stripe', {
          planId: 'sl_monthly_v2',
          active: false,
          expried: true,
          subscriptionId: 'test_1234',
          stripeCustomerId,
          renewalCount: 2,
        });

        await simulatePostOfferSubscription(theUserId, theOfferId, { source: '4242424242424242' });
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        const { planId, entitlement, trialing } = await simulateGetBilling(theUserId, 'sl');
        const {
          data: [
            {
              discount: {
                coupon: { id: couponId },
              },
              amount_paid: amountPaid,
            },
          ],
        } = await stripe.getInvoices(stripeCustomerId);
        expect(planId).to.equal('sl_annual_v2');
        expect(trialing).to.equal(false);
        expect(entitlement).to.equal('sl_premium');
        expect(amountPaid).to.equal(5588);
        expect(couponId).to.equal('month_two');
        trackSubscriptionSpy.restore();
      });
      it('should update the subscription for an active user to the offered plan and price', async () => {
        getUserStub.returns(stubbedUser);
        getOfferStub.returns({
          ...stubbedOffer,
          eligibleStatus: 'active',
          eligiblePlan: 'sl_monthly_v2',
          eligibilityTarget: 2,
          eligibilityType: 'renewals',
          offeredPlan: 'sl_annual_v2',
          stripeCouponCode: 'month_two',
        });
        getCouponStub.returns({
          ...stubbedCoupon,
          metadata: { promotion: theOfferId, savings: '45' },
        });
        const trackSubscriptionSpy = sinon.spy(
          trackChangedSubscriptionPlan,
          'trackChangedSubscriptionPlan',
        );

        await simulatePostBilling(theUserId, {
          plan: 'sl_monthly_v2',
          source: '4242424242424242',
        });
        const {
          stripeCustomerId,
          subscriptionId,
          planId,
          type,
          active,
          expiring,
          trialing,
        } = await simulateGetBilling(theUserId, 'sl');

        expect(planId).to.equal('sl_monthly_v2');
        expect(type).to.equal('stripe');
        expect(active).to.equal(true);
        expect(expiring).to.equal(false);
        expect(trialing).to.equal(false);

        await simulateStripeEvent(
          'invoice.payment_succeeded',
          { stripeCustomerId, subscriptionId },
          { periodStart: new Date() / 1000 },
        );
        await simulateStripeEvent(
          'invoice.payment_succeeded',
          { stripeCustomerId, subscriptionId },
          { periodStart: new Date() / 1000 },
        );

        const { renewalCount } = await simulateGetBilling(theUserId, 'sl');
        expect(renewalCount).to.equal(2);

        await simulatePostOfferSubscription(theUserId, theOfferId);
        await simulateStripeEvent(
          'customer.subscription.updated',
          {
            stripeCustomerId,
            subscriptionId,
          },
          {
            planId: 'sl_annual_v2',
            cancelAtPeriodEnd: false,
          },
        );
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        const {
          planId: updatedPlanId,
          active: updatedActive,
          expiring: updatedExpiring,
          trialing: updatedTrialing,
        } = await simulateGetBilling(theUserId, 'sl');

        expect(updatedPlanId).to.equal('sl_annual_v2');
        expect(updatedActive).to.equal(active);
        expect(updatedExpiring).to.equal(expiring);
        expect(updatedTrialing).to.equal(trialing);
        const {
          data: [
            {
              discount: {
                coupon: { id: couponId },
              },
              amount_paid: offerAmountPaid,
            },
            { discount, amount_paid: amountPaid },
          ],
        } = await stripe.getInvoices(stripeCustomerId);
        expect(discount).to.equal(null);
        expect(amountPaid).to.equal(999);
        expect(offerAmountPaid).to.equal(6589);
        expect(couponId).to.equal('month_two');
        trackSubscriptionSpy.restore();
      });
    });
  });
}
