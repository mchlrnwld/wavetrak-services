import sinon from 'sinon';
import { expect } from 'chai';
import mongoose from 'mongoose';
import * as getUser from '../../../src/external/user';
import { genTestToken } from '../../../src/external/stripeutils';
import {
  simulatePostBilling,
  simulateGetPromotionSubscription,
  simulatePostPromotionSubscription,
} from '../../helpers/simulateSubscription';
import CreditCardModel from '../../../src/models/v1/credit-cards/CreditCardModel';
import { SubscriptionModel } from '../../../src/models/subscriptions/SubscriptionModel';
import {
  createCustomer,
  deleteCustomer,
  getCustomer,
  getSubscription,
} from '../../../src/external/stripe';
import * as getPromotion from '../../../src/external/promotion';
import { deleteStripeSubscriptions } from '../../../src/models/subscriptions/stripe';
import { removeStripeCustomer } from '../../../src/models/customers/stripe';
import * as trackStartedSubscription from '../../../src/tracking/startedSubscription';
import * as trackStartedFreeTrial from '../../../src/tracking/startedFreeTrial';
import * as identifySubscription from '../../../src/tracking/identifySubscription';

export default function promotionSubscriptionIntegrationTests(theUserId) {
  let getUserStub;
  let getPromotionStub;
  let verfiyPromoCodeStub;
  let incrementRedemptionCountStub;
  let identifySubscriptionSpy;

  let theStripeCustomerId;
  const thePromotionId = mongoose.Types.ObjectId().toString();

  const stubbedUser = {
    _id: theUserId.toString(),
    firstName: 'Joel',
    lastName: 'Parkinson',
    email: 'joel@example.com',
    usID: 783514,
  };

  const stubbedPromotion = {
    _id: thePromotionId,
    name: 'Stubbed Test Promotion',
    brand: 'sl',
    isActive: true,
    plans: 'all',
  };

  const createTestStripeCustomer = async () => {
    const { email } = stubbedUser;
    const { id: tokenId } = await genTestToken();
    return createCustomer({
      source: tokenId,
      email,
    });
  };

  const teardownSubscription = async () => {
    await deleteStripeSubscriptions(theUserId);
    if (theStripeCustomerId) {
      await deleteCustomer(theStripeCustomerId);
    }
    await removeStripeCustomer(theUserId);
  };

  const seedSubscription = async (brand, type, opts = {}) => {
    const subscription = await SubscriptionModel.create({
      type,
      subscriptionId: 'test123',
      entitlement: `${brand}_premium`,
      planId: `${brand}_monthly_v2`,
      user: theUserId,
      start: 1579201980,
      end: 1894821181,
      active: true,
      ...opts,
    });
    return subscription;
  };

  describe('Promotion Subscription Tests [Subscription API v2]', () => {
    beforeEach(async () => {
      getUserStub = sinon.stub(getUser, 'getUserDetails');
      getPromotionStub = sinon.stub(getPromotion, 'getPromotion');
      verfiyPromoCodeStub = sinon.stub(getPromotion, 'verifyPromoCode');
      incrementRedemptionCountStub = sinon.stub(getPromotion, 'incrementRedemptionCount');
      identifySubscriptionSpy = sinon.spy(identifySubscription, 'identifySubscription');
    });
    afterEach(async () => {
      getUserStub.restore();
      getPromotionStub.restore();
      verfiyPromoCodeStub.restore();
      incrementRedemptionCountStub.restore();
      await teardownSubscription();
      await CreditCardModel.deleteOne({});
      theStripeCustomerId = null;
      identifySubscriptionSpy.restore();
    });

    describe('GET Promotion Subscription API', () => {
      it('should return null if a user has not subscribed to the requested promotion', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;

        const { subscription } = await simulateGetPromotionSubscription(theUserId, thePromotionId);
        expect(subscription).to.equal(null);
      });
      it('should return the active subscription if a user has subscribed to the request promotion', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        await seedSubscription('sl', 'stripe', {
          stripeCustomerId: theStripeCustomerId,
          expiring: false,
          promotionId: thePromotionId,
        });
        const {
          subscription: { active, promotionId },
        } = await simulateGetPromotionSubscription(theUserId, thePromotionId);
        expect(active).to.equal(true);
        expect(promotionId).to.equal(thePromotionId);
      });
      it('should return the inactive subscription if a user has subscribed to the request promotion', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        await seedSubscription('sl', 'stripe', {
          stripeCustomerId: theStripeCustomerId,
          expiring: true,
          promotionId: thePromotionId,
          active: false,
        });
        const {
          subscription: { active, promotionId },
        } = await simulateGetPromotionSubscription(theUserId, thePromotionId);
        expect(active).to.equal(false);
        expect(promotionId).to.equal(thePromotionId);
      });
    });
    describe('POST Promotion Subscription API', () => {
      it('should throw an error if the brand does not match the promotional brand', async () => {
        getUserStub.returns({ ...stubbedUser });
        getPromotionStub.returns({ ...stubbedPromotion, brand: 'fs' });
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );
        try {
          await simulatePostPromotionSubscription(theUserId, thePromotionId);
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;
          expect(trackSubscriptionSpy.callCount).to.equal(0);
          expect(identifySubscriptionSpy.callCount).to.equal(0);
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Cannot access promotion');
          trackSubscriptionSpy.restore();
        }
      });
      it('should throw an error if the plan interval is not specified for the promotion', async () => {
        getUserStub.returns({ ...stubbedUser });
        getPromotionStub.returns({ ...stubbedPromotion, plans: 'monthly' });
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );

        try {
          await simulatePostPromotionSubscription(theUserId, thePromotionId);
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;
          expect(trackSubscriptionSpy.callCount).to.equal(0);
          expect(identifySubscriptionSpy.callCount).to.equal(0);
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Cannot access promotion');
          trackSubscriptionSpy.restore();
        }
      });
      it('should throw an error if the user has already redeemed the promotion', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        getUserStub.returns({ ...stubbedUser, stripeCustomerId });
        getPromotionStub.returns({ ...stubbedPromotion });
        incrementRedemptionCountStub.returns(true);
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );

        await simulatePostPromotionSubscription(theUserId, thePromotionId);
        const {
          subscription: { promotionId },
        } = await simulateGetPromotionSubscription(theUserId, thePromotionId);
        expect(promotionId).to.equal(thePromotionId);
        try {
          await simulatePostPromotionSubscription(theUserId, thePromotionId);
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;
          expect(trackSubscriptionSpy.callCount).to.equal(1);
          expect(identifySubscriptionSpy.callCount).to.equal(1);
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Cannot access promotion');
          trackSubscriptionSpy.restore();
        }
      });
      it('should throw an error if the user is actively subscribed with the same entitlement', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        getUserStub.returns({ ...stubbedUser, stripeCustomerId });
        getPromotionStub.returns({ ...stubbedPromotion });
        incrementRedemptionCountStub.returns(true);
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );

        await simulatePostBilling(theUserId);
        try {
          await simulatePostPromotionSubscription(theUserId, thePromotionId);
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;
          expect(trackSubscriptionSpy.callCount).to.equal(0);
          expect(identifySubscriptionSpy.callCount).to.equal(1);
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Cannot access promotion');
          trackSubscriptionSpy.restore();
        }
      });
      it('should create a trialing promotional subscription', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        getUserStub.returns({ ...stubbedUser, stripeCustomerId });
        getPromotionStub.returns({ ...stubbedPromotion, trialLength: 20 });
        incrementRedemptionCountStub.returns(true);
        const trackSubscriptionSpy = sinon.spy(trackStartedFreeTrial, 'trackStartedFreeTrial');

        await simulatePostPromotionSubscription(theUserId, thePromotionId);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        const {
          subscription: { promotionId, trialing, active },
        } = await simulateGetPromotionSubscription(theUserId, thePromotionId);

        expect(promotionId).to.equal(promotionId);
        expect(trialing).to.equal(true);
        expect(active).to.equal(true);

        trackSubscriptionSpy.restore();
      });
      it('should create a new stripe customer and promotional subscription', async () => {
        getUserStub.returns({ ...stubbedUser });
        getPromotionStub.returns({ ...stubbedPromotion, trialLength: 20 });
        incrementRedemptionCountStub.returns(true);
        const trackSubscriptionSpy = sinon.spy(trackStartedFreeTrial, 'trackStartedFreeTrial');

        await simulatePostPromotionSubscription(theUserId, thePromotionId);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        const {
          subscription: { promotionId, trialing, active, stripeCustomerId },
        } = await simulateGetPromotionSubscription(theUserId, thePromotionId);

        expect(promotionId).to.equal(promotionId);
        expect(trialing).to.equal(true);
        expect(active).to.equal(true);

        const { id, email } = await getCustomer(stripeCustomerId);
        expect(id).to.equal(stripeCustomerId);
        expect(email).to.equal(stubbedUser.email);
        theStripeCustomerId = stripeCustomerId;

        trackSubscriptionSpy.restore();
      });
      it('should create a new trialing subscription according to the promotional trial length', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        getUserStub.returns({ ...stubbedUser, stripeCustomerId });
        getPromotionStub.returns({ ...stubbedPromotion, trialLength: 30 });
        incrementRedemptionCountStub.returns(true);
        const trackSubscriptionSpy = sinon.spy(trackStartedFreeTrial, 'trackStartedFreeTrial');

        await simulatePostPromotionSubscription(theUserId, thePromotionId, {
          plan: 'price_1KMDRPLwFGcBvXawEz531QXD',
          source: '4242424242424242',
          promotionId: thePromotionId,
          promoCode: null,
        });
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        const {
          subscription: { promotionId, trialing, active, planId, end },
        } = await simulateGetPromotionSubscription(theUserId, thePromotionId);

        expect(promotionId).to.equal(promotionId);
        expect(trialing).to.equal(true);
        expect(active).to.equal(true);
        expect(planId).to.equal('price_1KMDRPLwFGcBvXawEz531QXD');
        const lengthOfTrialInSeconds = end - new Date() / 1000;
        const isApproxThirtyDays =
          lengthOfTrialInSeconds > 2591000 && lengthOfTrialInSeconds < 2593000;
        expect(isApproxThirtyDays).to.equal(true);

        trackSubscriptionSpy.restore();
      });
      it('should should apply the apporopriate tax rates to a promotional subscription', async () => {
        getUserStub.returns({ ...stubbedUser });
        getPromotionStub.returns({ ...stubbedPromotion, trialLength: 30 });
        incrementRedemptionCountStub.returns(true);

        await simulatePostPromotionSubscription(theUserId, thePromotionId, {
          plan: 'sl_monthly_aud',
          source: '4242424242424242',
          promotionId: thePromotionId,
          promoCode: null,
        });
        const {
          subscription: { promotionId, trialing, active, planId, stripeCustomerId, subscriptionId },
        } = await simulateGetPromotionSubscription(theUserId, thePromotionId);

        expect(identifySubscriptionSpy.callCount).to.equal(1);
        expect(promotionId).to.equal(promotionId);
        expect(trialing).to.equal(true);
        expect(active).to.equal(true);
        expect(planId).to.equal('sl_monthly_aud');

        const {
          email,
          currency,
          metadata: { firstName, lastName, userId },
        } = await getCustomer(stripeCustomerId);
        expect(currency.toUpperCase()).to.equal('AUD');
        expect(userId).to.equal(theUserId.toString());
        expect(email).to.equal(stubbedUser.email);
        expect(firstName).to.equal(stubbedUser.firstName);
        expect(lastName).to.equal(stubbedUser.lastName);

        const { default_tax_rates: defaultTaxRates } = await getSubscription(subscriptionId);
        const taxedPlans = defaultTaxRates.reduce((results, taxRate) => {
          if (taxRate.metadata.plans) {
            results.push(taxRate.metadata.plans.split(','));
          }
          return results;
        }, []);
        const plans = taxedPlans.flat();
        const hasAppropriateTaxRate = plans.includes('sl_annual_aud');
        expect(hasAppropriateTaxRate).to.equal(true);
      });
      it('should create a non-trialing subscription for direct to pay promotions', async () => {
        getUserStub.returns({ ...stubbedUser });
        getPromotionStub.returns({ ...stubbedPromotion });
        incrementRedemptionCountStub.returns(true);
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );

        await simulatePostPromotionSubscription(theUserId, thePromotionId);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        const {
          subscription: { promotionId, trialing, active, stripeCustomerId },
        } = await simulateGetPromotionSubscription(theUserId, thePromotionId);

        expect(promotionId).to.equal(promotionId);
        expect(trialing).to.equal(false);
        expect(active).to.equal(true);

        const { id, email } = await getCustomer(stripeCustomerId);
        expect(id).to.equal(stripeCustomerId);
        expect(email).to.equal(stubbedUser.email);
        theStripeCustomerId = stripeCustomerId;

        trackSubscriptionSpy.restore();
      });
      it('should include the valid promoCode to the subscription for eligible promotions', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        getUserStub.returns({ ...stubbedUser, stripeCustomerId });
        getPromotionStub.returns({ ...stubbedPromotion, promoCodesEnabled: true });
        verfiyPromoCodeStub.returns(true);
        incrementRedemptionCountStub.returns(true);

        await simulatePostPromotionSubscription(theUserId, thePromotionId, {
          plan: 'price_1KMDRPLwFGcBvXawEz531QXD',
          source: '4242424242424242',
          promotionId: thePromotionId,
          promoCode: '1234',
        });

        expect(identifySubscriptionSpy.callCount).to.equal(1);

        const {
          subscription: { promotionId, trialing, active, planId, redeemCodes },
        } = await simulateGetPromotionSubscription(theUserId, thePromotionId);
        expect(promotionId).to.equal(promotionId);
        expect(trialing).to.equal(false);
        expect(active).to.equal(true);
        expect(planId).to.equal('price_1KMDRPLwFGcBvXawEz531QXD');
        const includesPromoCode = redeemCodes.includes('1234');
        expect(includesPromoCode).to.equal(true);
      });
    });
  });
}
