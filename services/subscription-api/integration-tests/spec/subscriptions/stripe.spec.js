import sinon from 'sinon';
import { expect } from 'chai';
import mongoose from 'mongoose';
import * as getUser from '../../../src/external/user';
import { genTestToken } from '../../../src/external/stripeutils';
import simulateStripeEvent from '../../helpers/simulateStripeEvent';
import CreditCardModel from '../../../src/models/v1/credit-cards/CreditCardModel';
import { SubscriptionModel } from '../../../src/models/subscriptions/SubscriptionModel';
import {
  createCustomer,
  deleteCustomer,
  getCustomer,
  getSubscription,
} from '../../../src/external/stripe';
import * as stripe from '../../../src/external/stripe';
import * as promotion from '../../../src/external/promotion';
import {
  deleteStripeSubscriptions,
  getStripeSubscription,
} from '../../../src/models/subscriptions/stripe';
import * as subscriptionModels from '../../../src/models/subscriptions';
import {
  simulateGetBilling,
  simulatePostBilling,
  simulatePutBilling,
  simulateDeleteBilling,
  simulatePostVerifyBilling,
  simulateStripeCheckout,
  simulateSyncUserDetails,
} from '../../helpers/simulateSubscription';
import { createStripeCustomer, removeStripeCustomer } from '../../../src/models/customers/stripe';
import getStripePriceId from '../../../src/utils/getStripePriceId';
import stripeSubscriptionsByCustomerIdData from '../../../src/__stubs__/stripe/subscriptions.list';
import * as trackStartedSubscription from '../../../src/tracking/startedSubscription';
import * as trackStartedFreeTrial from '../../../src/tracking/startedFreeTrial';
import * as trackCancelledFreeTrial from '../../../src/tracking/cancelledFreeTrial';
import * as trackCancelledSubscription from '../../../src/tracking/cancelledSubscription';
import * as trackExpiredSubscription from '../../../src/tracking/expiredSubscription';
import * as trackChangedSubscriptionPlan from '../../../src/tracking/changedSubscriptionPlan';
import * as identifySubscription from '../../../src/tracking/identifySubscription';

export default function stripeSubscriptionIntegrationTests(theUserId) {
  let getUserStub;
  let identifySubscriptionSpy;
  let theStripeCustomerId;
  let getActiveSubscriptionByUserIdAndEntitlementSpy;
  let getSubscriptionsByCustomerIdStub;
  const theSource = '4242424242424242';
  const annualPriceId = getStripePriceId('sl', 'usd', 'year');
  const monthlyPriceId = getStripePriceId('sl', 'usd', 'month');

  const stubbedUser = {
    _id: theUserId.toString(),
    firstName: 'Joel',
    lastName: 'Parkinson',
    email: 'joel@example.com',
    usID: 783514,
  };

  const createTestStripeCustomer = async () => {
    const { email } = stubbedUser;
    const { id: tokenId } = await genTestToken();
    const res = await createCustomer({
      source: tokenId,
      email,
    });
    await createStripeCustomer(theUserId, res.id);
    return res;
  };

  const teardownSubscription = async () => {
    try {
      await deleteStripeSubscriptions(theUserId);
      await deleteCustomer(theStripeCustomerId);
      await removeStripeCustomer(theUserId);
    } catch (err) {
      // don't crash if this errors
    }
  };

  const seedSubscription = async (brand, type, opts = {}) => {
    const subscription = await SubscriptionModel.create({
      type,
      subscriptionId: 'test123',
      entitlement: `${brand}_premium`,
      planId: `${brand}_monthly_v2`,
      user: theUserId,
      start: 1579201980,
      end: 1894821181,
      active: true,
      ...opts,
    });
    return subscription;
  };

  describe('Stripe Subscription Tests [Subscription API v2]', () => {
    beforeEach(async () => {
      getUserStub = sinon.stub(getUser, 'getUserDetails');
      identifySubscriptionSpy = sinon.spy(identifySubscription, 'identifySubscription');
      getActiveSubscriptionByUserIdAndEntitlementSpy = sinon.spy(
        subscriptionModels,
        'getActiveSubscriptionByUserIdAndEntitlement',
      );
      getSubscriptionsByCustomerIdStub = sinon.stub(stripe, 'getSubscriptionsByCustomerId');
    });
    afterEach(async () => {
      getUserStub.restore();
      identifySubscriptionSpy.restore();
      getActiveSubscriptionByUserIdAndEntitlementSpy.restore();
      getSubscriptionsByCustomerIdStub.restore();
      await teardownSubscription();
      await CreditCardModel.deleteOne({});
    });

    describe('GET Stripe Subscription API', () => {
      it('should return an active subscription per requested product', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;

        await seedSubscription('sl', 'stripe', { stripeCustomerId, expiring: false });
        const { active, type, entitlement, expiring } = await simulateGetBilling(theUserId, 'sl');

        expect(active).to.equal(true);
        expect(type).to.equal('stripe');
        expect(entitlement).to.equal('sl_premium');
        expect(expiring).to.equal(false);
      });
      it('should return an error if an active subscription is not found', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        await seedSubscription('sl', 'stripe', { active: false, expiring: true, stripeCustomerId });
        try {
          await simulateGetBilling(theUserId, 'sl');
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;

          expect(statusCode).to.equal(400);
          expect(message).to.equal('Cannot find active subscription for user and product');
        }
      });
    });
    describe('POST Stripe Subscription API', () => {
      it('should create a customer account and subscription for new subscribers', async () => {
        getUserStub.returns({ ...stubbedUser });
        const trackSubscriptionSpy = sinon.spy(trackStartedFreeTrial, 'trackStartedFreeTrial');
        const { message } = await simulatePostBilling(theUserId);

        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        expect(message).to.equal('Successfully processed the Subscription');

        const {
          active,
          type,
          entitlement,
          expiring,
          trialing,
          stripeCustomerId,
        } = await simulateGetBilling(theUserId, 'sl');
        theStripeCustomerId = stripeCustomerId;

        expect(active).to.equal(true);
        expect(type).to.equal('stripe');
        expect(entitlement).to.equal('sl_premium');
        expect(expiring).to.equal(false);
        expect(trialing).to.equal(true);

        const {
          email,
          metadata: { firstName, lastName, userId },
        } = await getCustomer(stripeCustomerId);
        expect(userId).to.equal(theUserId.toString());
        expect(email).to.equal(stubbedUser.email);
        expect(firstName).to.equal(stubbedUser.firstName);
        expect(lastName).to.equal(stubbedUser.lastName);

        trackSubscriptionSpy.restore();
      });
      it('should create a trialing annual subscription', async () => {
        getUserStub.returns({ ...stubbedUser });
        const trackSubscriptionSpy = sinon.spy(trackStartedFreeTrial, 'trackStartedFreeTrial');
        const { message } = await simulatePostBilling(theUserId);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        expect(message).to.equal('Successfully processed the Subscription');

        const {
          active,
          type,
          entitlement,
          expiring,
          trialing,
          stripeCustomerId,
        } = await simulateGetBilling(theUserId, 'sl');
        theStripeCustomerId = stripeCustomerId;

        expect(active).to.equal(true);
        expect(type).to.equal('stripe');
        expect(entitlement).to.equal('sl_premium');
        expect(expiring).to.equal(false);
        expect(trialing).to.equal(true);

        trackSubscriptionSpy.restore();
      });
      it('should apply the apporopriate tax rates to a subscription', async () => {
        getUserStub.returns({ ...stubbedUser });
        const trackSubscriptionSpy = sinon.spy(trackStartedFreeTrial, 'trackStartedFreeTrial');
        const { message } = await simulatePostBilling(theUserId, {
          plan: 'sl_annual_aud',
          source: theSource,
          trialEligible: true,
        });
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        expect(message).to.equal('Successfully processed the Subscription');

        const {
          active,
          type,
          entitlement,
          expiring,
          trialing,
          stripeCustomerId,
          subscriptionId,
        } = await simulateGetBilling(theUserId, 'sl');
        theStripeCustomerId = stripeCustomerId;

        expect(active).to.equal(true);
        expect(type).to.equal('stripe');
        expect(entitlement).to.equal('sl_premium');
        expect(expiring).to.equal(false);
        expect(trialing).to.equal(true);

        const {
          email,
          currency,
          metadata: { firstName, lastName, userId },
        } = await getCustomer(stripeCustomerId);
        expect(currency.toUpperCase()).to.equal('AUD');
        expect(userId).to.equal(theUserId.toString());
        expect(email).to.equal(stubbedUser.email);
        expect(firstName).to.equal(stubbedUser.firstName);
        expect(lastName).to.equal(stubbedUser.lastName);

        const { default_tax_rates: defaultTaxRates } = await getSubscription(subscriptionId);
        const taxedPlans = defaultTaxRates.reduce((results, taxRate) => {
          if (taxRate.metadata.plans) {
            results.push(taxRate.metadata.plans.split(','));
          }
          return results;
        }, []);
        const plans = taxedPlans.flat();
        const hasAppropriateTaxRate = plans.includes('sl_annual_aud');
        expect(hasAppropriateTaxRate).to.equal(true);

        trackSubscriptionSpy.restore();
      });
      it('should not allow free trials for monthly subscriptions', async () => {
        getUserStub.returns({ ...stubbedUser });
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );
        const { message } = await simulatePostBilling(theUserId, {
          plan: monthlyPriceId,
          source: theSource,
        });
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        expect(message).to.equal('Successfully processed the Subscription');

        const {
          active,
          type,
          entitlement,
          expiring,
          trialing,
          stripeCustomerId,
        } = await simulateGetBilling(theUserId, 'sl');
        theStripeCustomerId = stripeCustomerId;

        expect(active).to.equal(true);
        expect(type).to.equal('stripe');
        expect(entitlement).to.equal('sl_premium');
        expect(expiring).to.equal(false);
        expect(trialing).to.equal(false);

        trackSubscriptionSpy.restore();
      });
      it('should not allow free trial for ineligible customers', async () => {
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        getUserStub.returns({ ...stubbedUser, stripeCustomerId: theStripeCustomerId });

        await seedSubscription('sl', 'stripe', {
          active: false,
          stripeCustomerId,
          expiring: true,
          planId: annualPriceId,
        });

        const { message } = await simulatePostBilling(theUserId);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        expect(message).to.equal('Successfully processed the Subscription');

        const { active, type, entitlement, expiring, trialing } = await simulateGetBilling(
          theUserId,
          'sl',
        );

        expect(active).to.equal(true);
        expect(type).to.equal('stripe');
        expect(entitlement).to.equal('sl_premium');
        expect(expiring).to.equal(false);
        expect(trialing).to.equal(false);

        trackSubscriptionSpy.restore();
      });
      it('should not allow a user to subscribe while actively subscribed', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );
        theStripeCustomerId = stripeCustomerId;
        getUserStub.returns({ ...stubbedUser, stripeCustomerId: theStripeCustomerId });

        await seedSubscription('sl', 'stripe', {
          active: true,
          stripeCustomerId,
          expiring: true,
          planId: annualPriceId,
        });

        try {
          await simulatePostBilling(theUserId);
          expect(trackSubscriptionSpy.callCount).to.equal(0);
          expect(identifySubscriptionSpy.callCount).to.equal(0);
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;

          expect(statusCode).to.equal(400);
          expect(message).to.equal('Cannot process request with existing active subscription.');

          trackSubscriptionSpy.restore();
        }
      });
      it('should allow a user to subscribe with a valid coupon code', async () => {
        getUserStub.returns({ ...stubbedUser });
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );
        const { message } = await simulatePostBilling(theUserId, {
          plan: annualPriceId,
          source: theSource,
          trialEligible: true,
          coupon: '50-off-applies-to',
        });
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        expect(message).to.equal('Successfully processed the Subscription');

        const {
          active,
          type,
          entitlement,
          expiring,
          planId,
          trialing,
          stripeCustomerId,
        } = await simulateGetBilling(theUserId, 'sl');
        theStripeCustomerId = stripeCustomerId;

        expect(active).to.equal(true);
        expect(type).to.equal('stripe');
        expect(entitlement).to.equal('sl_premium');
        expect(expiring).to.equal(false);
        expect(planId).to.equal(annualPriceId);
        expect(trialing).to.equal(false);

        trackSubscriptionSpy.restore();
      });
    });
    describe('DELETE Stripe Subscription API', () => {
      it('should inactivate a trialing subscription', async () => {
        getUserStub.returns({ ...stubbedUser });
        const trackSubscriptionSpy = sinon.spy(trackCancelledFreeTrial, 'trackCancelledFreeTrial');
        const trackExpiredSubscriptionSpy = sinon.spy(
          trackExpiredSubscription,
          'trackExpiredSubscription',
        );

        const { message } = await simulatePostBilling(theUserId, {
          plan: annualPriceId,
          source: theSource,
          trialEligible: true,
        });
        expect(message).to.equal('Successfully processed the Subscription');

        const {
          subscriptionId,
          active,
          trialing,
          stripeCustomerId,
          planId,
        } = await simulateGetBilling(theUserId, 'sl');
        theStripeCustomerId = stripeCustomerId;
        expect(active).to.equal(true);
        expect(trialing).to.equal(true);

        await simulateDeleteBilling(theUserId, subscriptionId);
        await simulateStripeEvent('customer.subscription.deleted', {
          stripeCustomerId,
          subscriptionId,
          planId,
        });
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(trackExpiredSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(3);
        try {
          await simulateGetBilling(theUserId, 'sl');
        } catch (error) {
          const {
            statusCode,
            body: { message: cancellationMessage },
          } = error;
          expect(statusCode).to.equal(400);
          expect(cancellationMessage).to.equal(
            'Cannot find active subscription for user and product',
          );
        }

        const { active: isStillActive } = await getStripeSubscription(subscriptionId);
        expect(isStillActive).to.equal(false);

        trackSubscriptionSpy.restore();
        trackExpiredSubscriptionSpy.restore();
      });
      it('should set an active subscription to expire at the end date', async () => {
        getUserStub.returns({ ...stubbedUser });
        const trackSubscriptionSpy = sinon.spy(
          trackCancelledSubscription,
          'trackCancelledSubscription',
        );
        const trackExpiredSubscriptionSpy = sinon.spy(
          trackExpiredSubscription,
          'trackExpiredSubscription',
        );
        const { message } = await simulatePostBilling(theUserId, {
          plan: monthlyPriceId,
          source: theSource,
        });
        expect(message).to.equal('Successfully processed the Subscription');

        const {
          subscriptionId,
          active,
          trialing,
          stripeCustomerId,
          planId,
        } = await simulateGetBilling(theUserId, 'sl');
        theStripeCustomerId = stripeCustomerId;
        expect(active).to.equal(true);
        expect(trialing).to.equal(false);

        const { message: cancellationMessage } = await simulateDeleteBilling(
          theUserId,
          subscriptionId,
        );
        expect(cancellationMessage).to.equal('Successfully cancelled the Subscription');

        await simulateStripeEvent(
          'customer.subscription.updated',
          {
            stripeCustomerId,
            subscriptionId,
            planId,
          },
          {
            planId,
            cancelAtPeriodEnd: true,
          },
        );

        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(trackExpiredSubscriptionSpy.callCount).to.equal(0);
        expect(identifySubscriptionSpy.callCount).to.equal(2);

        const { active: isStillActive, expiring } = await simulateGetBilling(theUserId, 'sl');
        expect(isStillActive).to.equal(true);
        expect(expiring).to.equal(true);

        trackSubscriptionSpy.restore();
        trackExpiredSubscriptionSpy.restore();
      });
    });
    describe('PUT Stripe Subscription API', () => {
      it('should resume an expiring/cancelled active subscription', async () => {
        getUserStub.returns({ ...stubbedUser });

        const { message } = await simulatePostBilling(theUserId, {
          plan: monthlyPriceId,
          source: theSource,
        });
        expect(message).to.equal('Successfully processed the Subscription');

        const {
          subscriptionId,
          active,
          trialing,
          stripeCustomerId,
          planId,
        } = await simulateGetBilling(theUserId, 'sl');
        theStripeCustomerId = stripeCustomerId;
        expect(active).to.equal(true);
        expect(trialing).to.equal(false);

        const { message: cancellationMessage } = await simulateDeleteBilling(
          theUserId,
          subscriptionId,
        );
        expect(cancellationMessage).to.equal('Successfully cancelled the Subscription');

        await simulateStripeEvent(
          'customer.subscription.updated',
          {
            stripeCustomerId,
            subscriptionId,
            planId,
          },
          {
            planId,
            cancelAtPeriodEnd: true,
          },
        );
        const { active: isStillActive, expiring } = await simulateGetBilling(theUserId, 'sl');
        expect(isStillActive).to.equal(true);
        expect(expiring).to.equal(true);

        const { message: reactivationMessage } = await simulatePutBilling(theUserId, {
          subscriptionId,
          planId,
          resume: true,
        });
        expect(reactivationMessage).to.equal('Successfully processed the Subscription');
        await simulateStripeEvent(
          'customer.subscription.updated',
          {
            stripeCustomerId,
            subscriptionId,
            planId,
          },
          {
            planId,
            cancelAtPeriodEnd: false,
          },
        );
        const {
          active: hasRemainedActive,
          expiring: isStillExpring,
          planId: updatedPlanId,
          trialing: isTrialing,
        } = await simulateGetBilling(theUserId, 'sl');
        expect(hasRemainedActive).to.equal(true);
        expect(isStillExpring).to.equal(false);
        expect(updatedPlanId).to.equal(planId);
        expect(isTrialing).to.equal(false);
      });
      it('should update the subscription from monthly to annual billing', async () => {
        getUserStub.returns({ ...stubbedUser });
        const trackSubscriptionSpy = sinon.spy(
          trackChangedSubscriptionPlan,
          'trackChangedSubscriptionPlan',
        );
        const { message } = await simulatePostBilling(theUserId, {
          plan: monthlyPriceId,
          source: theSource,
        });
        expect(message).to.equal('Successfully processed the Subscription');

        const {
          subscriptionId,
          active,
          trialing,
          stripeCustomerId,
          planId,
        } = await simulateGetBilling(theUserId, 'sl');
        theStripeCustomerId = stripeCustomerId;
        expect(active).to.equal(true);
        expect(trialing).to.equal(false);
        expect(planId).to.equal(monthlyPriceId);

        const { message: reactivationMessage } = await simulatePutBilling(theUserId, {
          subscriptionId,
          planId: annualPriceId,
          resume: null,
        });
        expect(reactivationMessage).to.equal('Successfully processed the Subscription');

        await simulateStripeEvent(
          'customer.subscription.updated',
          {
            stripeCustomerId,
            subscriptionId,
          },
          { planId: annualPriceId, cancelAtPeriodEnd: false },
        );
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(2);
        const {
          active: isStillActive,
          trialing: isTrialing,
          planId: updatedPlanId,
          expiring: isExpiring,
        } = await simulateGetBilling(theUserId, 'sl');
        theStripeCustomerId = stripeCustomerId;
        expect(isStillActive).to.equal(true);
        expect(isTrialing).to.equal(false);
        expect(updatedPlanId).to.equal(annualPriceId);
        expect(isExpiring).to.equal(false);

        trackSubscriptionSpy.restore();
      });
      it('should return an error if the subscription is not found or is inactive', async () => {
        getUserStub.returns({ ...stubbedUser });
        const { message } = await simulatePostBilling(theUserId, {
          plan: annualPriceId,
          source: theSource,
          trialEligible: true,
        });
        expect(message).to.equal('Successfully processed the Subscription');

        const {
          subscriptionId,
          active,
          trialing,
          stripeCustomerId,
          planId,
        } = await simulateGetBilling(theUserId, 'sl');
        theStripeCustomerId = stripeCustomerId;
        expect(active).to.equal(true);
        expect(trialing).to.equal(true);

        await simulateDeleteBilling(theUserId, subscriptionId);
        await simulateStripeEvent('customer.subscription.deleted', {
          stripeCustomerId,
          subscriptionId,
          planId,
        });

        try {
          await simulatePutBilling(theUserId, {
            subscriptionId,
            planId: annualPriceId,
            resume: true,
          });
        } catch (error) {
          const {
            statusCode,
            body: { message: cancellationMessage },
          } = error;
          expect(statusCode).to.equal(400);
          expect(cancellationMessage).to.equal(
            'Cannot find active subscription with subscription id',
          );
        }
      });
    });
    describe('PUT /syncUserDetails', async () => {
      const updatedUser = {
        firstName: 'Ben',
        lastName: 'Moon',
        email: 'ben@gmail.com',
      };
      it('should handle users without stripeCustomerIds', async () => {
        const { message } = await simulateSyncUserDetails(theUserId, updatedUser);
        expect(message).to.equal('No stripe account associated with the user');
      });
      it('should update user details in Stripe for users with a valid stripeCustomerId', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        try {
          const { message } = await simulateSyncUserDetails(theUserId, updatedUser);
          const {
            email,
            metadata: { firstName, lastName },
          } = await getCustomer(stripeCustomerId);
          expect(message).to.equal('User settings were updated');
          expect(firstName).to.equal(updatedUser.firstName);
          expect(lastName).to.equal(updatedUser.lastName);
          expect(email).to.equal(updatedUser.email);
        } finally {
          await teardownSubscription();
        }
      });
    });
    describe('POST Stripe Verify Subscription API', () => {
      let incrementRedemptionCountStub;

      beforeEach(async () => {
        incrementRedemptionCountStub = sinon.stub(promotion, 'incrementRedemptionCount');
      });

      afterEach(async () => {
        incrementRedemptionCountStub.restore();
      });
      it('should succesfully verify if the user has an active subscription', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        await seedSubscription('sl', 'stripe', { stripeCustomerId, expiring: false });
        const response = await simulatePostVerifyBilling(theUserId, 'sl');
        const { status, message, subscription } = response;

        expect(status).to.equal('success');
        expect(message).to.equal('Subscription already exists');
        expect(subscription.entitlement).to.equal('sl_premium');
      });

      it('should succesfully create a subscription if the user has an subscription in Stripe', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        getSubscriptionsByCustomerIdStub.returns(
          stripeSubscriptionsByCustomerIdData(theStripeCustomerId, { planId: monthlyPriceId }),
        );
        const response = await simulatePostVerifyBilling(theUserId, 'sl');
        const { status, message, subscription } = response;

        expect(status).to.equal('success');
        expect(message).to.equal('Successfully created the Subscription');
        expect(subscription.entitlement).to.equal('sl_premium');
      });

      it('should succesfully create a subscription after 2nd try if the user has an subscription in Stripe', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        getSubscriptionsByCustomerIdStub
          .withArgs(theStripeCustomerId)
          .onFirstCall()
          .returns([])
          .onSecondCall()
          .returns(
            stripeSubscriptionsByCustomerIdData(theStripeCustomerId, { planId: monthlyPriceId }),
          );
        const response = await simulatePostVerifyBilling(theUserId, 'sl');
        const { status, message, subscription } = response;

        expect(incrementRedemptionCountStub).to.not.have.been.called();
        expect(status).to.equal('success');
        expect(message).to.equal('Successfully created the Subscription');
        expect(subscription.entitlement).to.equal('sl_premium');
      });

      it('should return with a fail status if no Stripe subscription is available and the maximum number of retries has been met', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        getSubscriptionsByCustomerIdStub.returns([]);
        const response = await simulatePostVerifyBilling(theUserId, 'sl');
        const { status, message } = response;

        expect(status).to.equal('fail');
        expect(message).to.equal('No active subscription found');
      });

      it('should call incrementRedemptionCount if a promotionId is sent in the request body', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        theStripeCustomerId = stripeCustomerId;
        const promotionId = mongoose.Types.ObjectId();
        getSubscriptionsByCustomerIdStub.returns(
          stripeSubscriptionsByCustomerIdData(theStripeCustomerId, {
            planId: monthlyPriceId,
            promotionId,
          }),
        );
        incrementRedemptionCountStub.returns({ _id: 'abcd', totalRedemption: 3 });
        const response = await simulatePostVerifyBilling(theUserId, 'sl', promotionId);
        const { status, message, subscription } = response;

        expect(incrementRedemptionCountStub).to.have.been.called();
        expect(status).to.equal('success');
        expect(message).to.equal('Successfully created the Subscription');
        expect(subscription.entitlement).to.equal('sl_premium');
      });
    });
  });

  describe('POST Stripe Checkout API', () => {
    let startCheckoutSessionSpy;
    let getPromotionStub;

    beforeEach(async () => {
      startCheckoutSessionSpy = sinon.spy(stripe, 'startCheckoutSession');
      getPromotionStub = sinon.stub(promotion, 'getPromotion');
      getUserStub = sinon.stub(getUser, 'getUserDetails');
      const { id } = await createTestStripeCustomer();
      theStripeCustomerId = id;
    });

    afterEach(async () => {
      startCheckoutSessionSpy.restore();
      getPromotionStub.restore();
      getUserStub.restore();
      await removeStripeCustomer(theUserId);
    });

    it('should create a Stripe Checkout session', async () => {
      const priceId = getStripePriceId('bw', 'USD', 'month');
      const res = await simulateStripeCheckout(theUserId, {
        brand: 'bw',
        country: 'US',
        currency: 'USD',
        interval: 'month',
        successUrl: 'https://www.surfline.com',
        cancelUrl: 'https://www.surfline.com',
      });
      expect(res.status).to.equal(200);
      expect(startCheckoutSessionSpy).to.have.been.calledWith({
        cancelUrl: 'https://www.surfline.com',
        clientReferenceId: theUserId.toString(),
        coupon: undefined,
        metadata: { brand: 'bw', priceId },
        priceId,
        mode: 'subscription',
        stripeCustomerId: theStripeCustomerId,
        successUrl: 'https://www.surfline.com',
        subscriptionData: {
          trial_period_days: undefined,
          default_tax_rates: [],
          metadata: {
            brand: 'bw',
            interval: 'month',
            promotionId: undefined,
          },
        },
      });
      expect(res.body.url).to.startWith('https://checkout.stripe.com');
    });

    it('should not return an error if the promotionId is not eligible', async () => {
      const priceId = getStripePriceId('sl', 'USD', 'month');
      const res = await simulateStripeCheckout(theUserId, {
        brand: 'sl',
        country: 'US',
        currency: 'USD',
        interval: 'month',
        promotionId: 'fake',
        successUrl: 'https://www.surfline.com',
        cancelUrl: 'https://www.surfline.com',
      });
      expect(res.status).to.equal(200);
      expect(getPromotionStub).to.have.been.called();
      expect(startCheckoutSessionSpy).to.have.been.calledWith({
        cancelUrl: 'https://www.surfline.com',
        clientReferenceId: theUserId.toString(),
        coupon: undefined,
        metadata: { brand: 'sl', priceId },
        priceId,
        mode: 'subscription',
        stripeCustomerId: theStripeCustomerId,
        successUrl: 'https://www.surfline.com',
        subscriptionData: {
          trial_period_days: undefined,
          default_tax_rates: [],
          metadata: {
            brand: 'sl',
            interval: 'month',
            promotionId: undefined,
          },
        },
      });
      expect(res.body.url).to.startWith('https://checkout.stripe.com');
    });

    it('should return an error if it cannot find an associated price id', async () => {
      const res = await simulateStripeCheckout(theUserId, {
        brand: 'sl',
        country: 'US',
        currency: 'USD',
        interval: 'infinite',
        successUrl: 'https://www.surfline.com',
        cancelUrl: 'https://www.surfline.com',
      });
      expect(res.status).to.equal(400);
      expect(getPromotionStub).to.not.have.been.called();
      expect(startCheckoutSessionSpy).to.not.have.been.called();
      expect(res.body.message).to.equal(
        'Must provide a priceId or a valid brand, currency, and interval',
      );
    });

    it('should return an error if the urls are invalid', async () => {
      const res = await simulateStripeCheckout(theUserId, {
        brand: 'sl',
        country: 'US',
        currency: 'USD',
        interval: 'year',
        successUrl: 'https://www.google.com',
        cancelUrl: 'https://www.xsrf.com',
      });
      expect(res.status).to.equal(400);
      expect(getPromotionStub).to.not.have.been.called();
      expect(startCheckoutSessionSpy).to.not.have.been.called();
      expect(res.body.message).to.equal('The success and cancel URLs need to be valid');
    });

    it('should return an error if the user has an active subscription', async () => {
      try {
        await seedSubscription('sl', 'stripe', {
          active: true,
          expiring: false,
          stripeCustomerId: theStripeCustomerId,
        });
        const res = await simulateStripeCheckout(theUserId, {
          brand: 'sl',
          country: 'US',
          currency: 'USD',
          interval: 'month',
          successUrl: 'https://www.surfline.com',
          cancelUrl: 'https://www.surfline.com',
        });
        expect(res.status).to.equal(400);
        expect(getPromotionStub).to.not.have.been.called();
        expect(startCheckoutSessionSpy).to.not.have.been.called();
        expect(res.body.message).to.equal(
          'Cannot process request with existing active subscription.',
        );
      } finally {
        await teardownSubscription();
      }
    });

    it('should attach a trial to first time users', async () => {
      const priceId = getStripePriceId('sl', 'USD', 'year');
      const res = await simulateStripeCheckout(theUserId, {
        brand: 'sl',
        country: 'US',
        currency: 'USD',
        interval: 'year',
        successUrl: 'https://www.surfline.com',
        cancelUrl: 'https://www.surfline.com',
      });
      expect(res.status).to.equal(200);
      expect(startCheckoutSessionSpy).to.have.been.calledWith({
        cancelUrl: 'https://www.surfline.com',
        clientReferenceId: theUserId.toString(),
        coupon: undefined,
        metadata: { brand: 'sl', priceId },
        priceId,
        mode: 'subscription',
        stripeCustomerId: theStripeCustomerId,
        successUrl: 'https://www.surfline.com',
        subscriptionData: {
          trial_period_days: 15,
          default_tax_rates: [],
          metadata: {
            brand: 'sl',
            interval: 'year',
            promotionId: undefined,
          },
        },
      });
      expect(res.body.url).to.startWith('https://checkout.stripe.com');
    });

    it('should not allow a free trial if the user had been premium previously', async () => {
      try {
        const priceId = getStripePriceId('sl', 'USD', 'year');
        await seedSubscription('sl', 'stripe', {
          active: false,
          expiring: false,
          stripeCustomerId: theStripeCustomerId,
          planId: priceId,
        });
        const res = await simulateStripeCheckout(theUserId, {
          brand: 'sl',
          country: 'US',
          currency: 'USD',
          interval: 'year',
          successUrl: 'https://www.surfline.com',
          cancelUrl: 'https://www.surfline.com',
        });
        expect(res.status).to.equal(200);
        expect(startCheckoutSessionSpy).to.have.been.calledWith({
          cancelUrl: 'https://www.surfline.com',
          clientReferenceId: theUserId.toString(),
          coupon: undefined,
          metadata: { brand: 'sl', priceId },
          priceId,
          mode: 'subscription',
          stripeCustomerId: theStripeCustomerId,
          successUrl: 'https://www.surfline.com',
          subscriptionData: {
            trial_period_days: undefined,
            default_tax_rates: [],
            metadata: {
              brand: 'sl',
              interval: 'year',
              promotionId: undefined,
            },
          },
        });
        expect(res.body.url).to.startWith('https://checkout.stripe.com');
      } finally {
        await teardownSubscription();
      }
    });

    it('should use the promotion free trial length', async () => {
      getPromotionStub.returns({
        paymentHeader: 'Claim your free 3 months of Surfline Premium.',
        promoCodesEnabled: false,
        promoCodePrefix: null,
        plans: 'annual',
        signUpHeader: 'Claim your free 3 months of Surfline Premium.',
        successUrl: 'https://surfline.com/onboarding/about-you',
        maxRedemption: 10000,
        currency: 'AUD',
        name: 'Quiksilver 3-Month Free Trial',
        isActive: true,
        isDeleted: false,
        totalRedemption: 68,
        kind: 'Extended Free Trial',
        brand: 'sl',
        trialLength: 90,
        urlKey: 'quiksilver-aus',
        _id: '616777a4e5a5766393154165',
      });
      const priceId = getStripePriceId('sl', 'USD', 'year');
      const res = await simulateStripeCheckout(theUserId, {
        brand: 'sl',
        country: 'US',
        currency: 'USD',
        interval: 'year',
        promotionId: '616777a4e5a5766393154165',
        successUrl: 'https://www.surfline.com',
        cancelUrl: 'https://www.surfline.com',
      });
      expect(res.status).to.equal(200);
      expect(getPromotionStub).to.have.been.calledOnce();
      expect(startCheckoutSessionSpy).to.have.been.calledWith({
        cancelUrl: 'https://www.surfline.com',
        clientReferenceId: theUserId.toString(),
        coupon: undefined,
        metadata: { brand: 'sl', priceId },
        priceId,
        mode: 'subscription',
        stripeCustomerId: theStripeCustomerId,
        successUrl: 'https://www.surfline.com',
        subscriptionData: {
          trial_period_days: 90,
          default_tax_rates: [],
          metadata: {
            brand: 'sl',
            interval: 'year',
            promotionId: '616777a4e5a5766393154165',
          },
        },
      });
      expect(res.body.url).to.startWith('https://checkout.stripe.com');
      getPromotionStub.restore();
    });

    it('should not use the coupon when there is a promotion', async () => {
      getPromotionStub.returns({
        paymentHeader: 'Claim your free 3 months of Surfline Premium.',
        promoCodesEnabled: false,
        promoCodePrefix: null,
        plans: 'annual',
        signUpHeader: 'Claim your free 3 months of Surfline Premium.',
        successUrl: 'https://surfline.com/onboarding/about-you',
        maxRedemption: 10000,
        currency: 'AUD',
        name: 'Quiksilver 3-Month Free Trial',
        isActive: true,
        isDeleted: false,
        totalRedemption: 68,
        kind: 'Extended Free Trial',
        brand: 'sl',
        trialLength: 90,
        urlKey: 'quiksilver-aus',
        _id: '616777a4e5a5766393154165',
      });
      const priceId = getStripePriceId('sl', 'USD', 'year');
      const res = await simulateStripeCheckout(theUserId, {
        brand: 'sl',
        country: 'US',
        currency: 'USD',
        coupon: '50forever',
        interval: 'year',
        promotionId: '616777a4e5a5766393154165',
        successUrl: 'https://www.surfline.com',
        cancelUrl: 'https://www.surfline.com',
      });
      expect(res.status).to.equal(200);
      expect(getPromotionStub).to.have.been.calledOnce();
      expect(startCheckoutSessionSpy).to.have.been.calledWith({
        cancelUrl: 'https://www.surfline.com',
        clientReferenceId: theUserId.toString(),
        coupon: undefined,
        metadata: { brand: 'sl', priceId: annualPriceId },
        priceId,
        mode: 'subscription',
        stripeCustomerId: theStripeCustomerId,
        successUrl: 'https://www.surfline.com',
        subscriptionData: {
          trial_period_days: 90,
          default_tax_rates: [],
          metadata: {
            brand: 'sl',
            interval: 'year',
            promotionId: '616777a4e5a5766393154165',
          },
        },
      });
      expect(res.body.url).to.startWith('https://checkout.stripe.com');
      getPromotionStub.restore();
    });

    it('should apply a coupon when supplied', async () => {
      const priceId = getStripePriceId('sl', 'USD', 'year');
      const res = await simulateStripeCheckout(theUserId, {
        brand: 'sl',
        country: 'US',
        currency: 'USD',
        coupon: '50forever',
        interval: 'year',
        successUrl: 'https://www.surfline.com',
        cancelUrl: 'https://www.surfline.com',
      });
      expect(res.status).to.equal(200);
      expect(startCheckoutSessionSpy).to.have.been.calledWith({
        cancelUrl: 'https://www.surfline.com',
        clientReferenceId: theUserId.toString(),
        coupon: '50forever',
        metadata: { brand: 'sl', priceId: annualPriceId },
        priceId,
        mode: 'subscription',
        stripeCustomerId: theStripeCustomerId,
        successUrl: 'https://www.surfline.com',
        subscriptionData: {
          trial_period_days: undefined,
          default_tax_rates: [],
          metadata: {
            brand: 'sl',
            interval: 'year',
            promotionId: undefined,
          },
        },
      });
      expect(res.body.url).to.startWith('https://checkout.stripe.com');
    });
  });
}
