import sinon from 'sinon';
import { expect } from 'chai';
import mongoose from 'mongoose';
import * as getUser from '../../../src/external/user';
import {
  simulateGooglePremiumPending,
  verifyGooglePurchase,
  verifyDeprecatedV1GoogleReceipt,
} from '../../helpers/simulateSubscription';
import {
  deleteSubscriptions,
  getActiveSubscriptionByUserIdAndEntitlement,
  getSubscriptionFromCustomParams,
} from '../../../src/models/subscriptions';
import { getPendingSubscription } from '../../../src/models/subscriptions/pending';
import * as googlePlayAPI from '../../../src/external/google';
import googlePurchaseData from '../../../src/__stubs__/google/BASE_PURCHASE_DATA';
import googleSignature from '../../../src/__stubs__/google/BASE_SIGNATUTRE';
import { SubscriptionPurchaseModel } from '../../../src/models/subscription-purchases/SubscriptionPurchaseModel';
import * as trackStartedSubscription from '../../../src/tracking/startedSubscription';
import * as trackCancelledSubscription from '../../../src/tracking/cancelledSubscription';
import * as trackExpiredSubscription from '../../../src/tracking/expiredSubscription';
import * as trackChangedSubscriptionPlan from '../../../src/tracking/changedSubscriptionPlan';
import * as identifySubscription from '../../../src/tracking/identifySubscription';

export default function googleSubscriptionIntegrationTests(theUserId) {
  let getUserStub;
  let getPurchaseDataStub;
  let identifySubscriptionSpy;
  const slGoogleMonthly = 'surfline.android.monthly.2022';
  const slGoogleYearly = 'surfline.android.yearly.2022';

  const teardownSubscription = async () => {
    await deleteSubscriptions(theUserId);
  };

  const tearDownTestSubscriptionPurchase = async () =>
    SubscriptionPurchaseModel.deleteOne({ user: theUserId });

  describe('Google Subscription Tests [Subscription API V2', () => {
    before(async () => {
      await teardownSubscription();
      await tearDownTestSubscriptionPurchase();
    });
    beforeEach(async () => {
      getUserStub = sinon.stub(getUser, 'getUserDetails');
      getPurchaseDataStub = sinon.stub(googlePlayAPI, 'default');
      identifySubscriptionSpy = sinon.spy(identifySubscription, 'identifySubscription');
    });
    afterEach(async () => {
      getUserStub.restore();
      getPurchaseDataStub.restore();
      await teardownSubscription();
      await tearDownTestSubscriptionPurchase();
      identifySubscriptionSpy.restore();
    });
    describe('POST Google Premium Pending API', () => {
      it('should create a premium pending subscription status', async () => {
        await simulateGooglePremiumPending(theUserId, 'sl');
        const subscription = await getPendingSubscription({ user: theUserId, brand: 'sl' });
        const { type, planId, user, platform, entitlement } = subscription;
        expect(type).to.equal('pending');
        expect(planId).to.equal('sl_pending');
        expect(user.toString()).to.equal(theUserId.toString());
        expect(platform).to.equal('google');
        expect(entitlement).to.equal('sl_pending');
      });
      it('should not error if a pending subscription is not found', async () => {
        await simulateGooglePremiumPending(theUserId, 'sl', true);
        const shouldEqualNull = await getPendingSubscription({ user: theUserId, brand: 'sl' });
        expect(shouldEqualNull).to.equal(null);
      });
      it('should throw an error for duplicate pending subscription requests', async () => {
        await simulateGooglePremiumPending(theUserId, 'sl');
        try {
          await simulateGooglePremiumPending(theUserId, 'sl');
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;

          expect(statusCode).to.equal(400);
          expect(message).to.equal('There is an existing google subscription pending.');
        }
      });
      it('should clear the pending premium subscription status', async () => {
        await simulateGooglePremiumPending(theUserId, 'sl');
        const subscription = await getPendingSubscription({ user: theUserId, brand: 'sl' });
        const { type, planId, user, platform, entitlement } = subscription;

        expect(type).to.equal('pending');
        expect(planId).to.equal('sl_pending');
        expect(user.toString()).to.equal(theUserId.toString());
        expect(platform).to.equal('google');
        expect(entitlement).to.equal('sl_pending');

        await simulateGooglePremiumPending(theUserId, 'sl', true);
        const shouldEqualNull = await getPendingSubscription({ user: theUserId, brand: 'sl' });
        expect(shouldEqualNull).to.equal(null);
      });
    });
    describe('POST Google Subscription API', () => {
      it('should create a valid google subscription', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();
        const trackSubscriptionSpy = sinon.spy(
          trackStartedSubscription,
          'trackStartedSubscription',
        );

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(1);
        const {
          type,
          planId,
          entitlement,
          active,
          expiring,
          trialing,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(type).to.equal('google');
        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(active).to.equal(true);
        expect(expiring).to.equal(false);
        expect(trialing).to.equal(false);
        trackSubscriptionSpy.restore();
      });
      it('should return an error for invalid google subscription requests', async () => {
        try {
          await verifyGooglePurchase(theUserId, { purchase: {} });
          expect(identifySubscriptionSpy.callCount).to.equal(0);
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;

          expect(statusCode).to.equal(400);
          expect(message).to.equal('Cannot process subscription without a valid token');
        }
      });
      it('should return an error for duplicate receipt requests', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          type,
          planId,
          entitlement,
          active,
          expiring,
          trialing,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(type).to.equal('google');
        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(active).to.equal(true);
        expect(expiring).to.equal(false);
        expect(trialing).to.equal(false);

        try {
          getPurchaseDataStub.returns(purchaseData);
          const altUserId = mongoose.Types.ObjectId();
          await verifyGooglePurchase(altUserId, purchaseSignature);
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;

          expect(identifySubscriptionSpy.callCount).to.equal(1);
          expect(statusCode).to.equal(400);
          expect(message).to.equal('This receipt is subscribed to a different account');
        }
      });
      it('should clear the pending subscription for valid google subscription requests', async () => {
        await simulateGooglePremiumPending(theUserId, 'sl');
        const subscription = await getPendingSubscription({ user: theUserId, brand: 'sl' });
        const { type: pendingType, platform } = subscription;
        expect(pendingType).to.equal('pending');
        expect(platform).to.equal('google');

        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);

        await verifyGooglePurchase(theUserId, purchaseSignature);
        const { type, planId, entitlement } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );
        expect(type).to.equal('google');
        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');

        const shouldEqualNull = await getPendingSubscription({ user: theUserId, brand: 'sl' });
        expect(shouldEqualNull).to.equal(null);
      });
      it('should cancel the google subscription', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();
        const trackSubscriptionSpy = sinon.spy(
          trackCancelledSubscription,
          'trackCancelledSubscription',
        );

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          type,
          planId,
          entitlement,
          expiring,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(type).to.equal('google');
        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(expiring).to.equal(false);

        const cancellationPurchaseData = googlePurchaseData({ cancellation: true });

        getPurchaseDataStub.returns(cancellationPurchaseData);

        await verifyGooglePurchase(theUserId, purchaseSignature);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(2);
        const { expiring: isExpiring } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );
        expect(isExpiring).to.equal(true);
        trackSubscriptionSpy.restore();
      });
      it('should update the google subscription if the billing changes', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();
        const trackSubscriptionSpy = sinon.spy(
          trackChangedSubscriptionPlan,
          'trackChangedSubscriptionPlan',
        );

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          type,
          planId,
          entitlement,
          expiring,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(type).to.equal('google');
        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(expiring).to.equal(false);

        const updatedPurchaseSignature = googleSignature({ productId: slGoogleYearly });
        await verifyGooglePurchase(theUserId, updatedPurchaseSignature);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(2);
        const { planId: updatedPlanId } = await getActiveSubscriptionByUserIdAndEntitlement(
          theUserId,
          'sl',
        );
        expect(updatedPlanId).to.equal(slGoogleYearly);
        trackSubscriptionSpy.restore();
      });
      it('should expire the google subscription appropriately', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();
        const trackSubscriptionSpy = sinon.spy(
          trackExpiredSubscription,
          'trackExpiredSubscription',
        );

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          type,
          planId,
          entitlement,
          expiring,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(type).to.equal('google');
        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(expiring).to.equal(false);

        const expirationPurchaseData = googlePurchaseData({ expiration: true });

        getPurchaseDataStub.returns(expirationPurchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        expect(trackSubscriptionSpy.callCount).to.equal(1);
        expect(identifySubscriptionSpy.callCount).to.equal(2);
        const { cancellationReason, expired, active } = await getSubscriptionFromCustomParams({
          user: theUserId,
          entitlement,
          type: 'google',
          active: false,
        });
        expect(cancellationReason).to.equal(0);
        expect(expired).to.not.equal(null);
        expect(active).to.equal(false);
        trackSubscriptionSpy.restore();
      });
    });
    describe('POST Deprecated v1 Google Subscription API', () => {
      it('should tranform an active v2 Subscription to a v1 UserAccount', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);

        const account = await verifyDeprecatedV1GoogleReceipt(theUserId, purchaseSignature, 'sl');
        expect(account.subscriptions.length).to.equals(1);
        expect(account.subscriptions[0].entitlement).to.equal('sl_premium');
        expect(account.receipts.length).to.equals(1);
        expect(typeof account._id).to.equal('string');
        expect(typeof account.createdAt).to.equal('string');
        expect(typeof account.updatedAt).to.equal('string');

        const {
          type,
          planId,
          entitlement,
          active,
          expiring,
          trialing,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(type).to.equal('google');
        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(active).to.equal(true);
        expect(expiring).to.equal(false);
        expect(trialing).to.equal(false);
      });
      it('should transform an inactive v2 Subscription to a v1 UserAccount', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);
        const account = await verifyDeprecatedV1GoogleReceipt(theUserId, purchaseSignature, 'sl');
        expect(account.subscriptions.length).to.equals(1);
        expect(account.subscriptions[0].entitlement).to.equal('sl_premium');
        expect(account.receipts.length).to.equals(1);

        const {
          type,
          planId,
          entitlement,
          expiring,
        } = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');
        expect(type).to.equal('google');
        expect(planId).to.equal(slGoogleMonthly);
        expect(entitlement).to.equal('sl_premium');
        expect(expiring).to.equal(false);

        const expirationPurchaseData = googlePurchaseData({ expiration: true });
        getPurchaseDataStub.returns(expirationPurchaseData);

        const inactiveAccount = await verifyDeprecatedV1GoogleReceipt(
          theUserId,
          purchaseSignature,
          'sl',
        );
        expect(inactiveAccount.subscriptions.length).to.equal(0);
        expect(inactiveAccount.archivedSubscriptions.length).to.equal(1);
        expect(inactiveAccount.archivedSubscriptions[0].entitlement).to.equal('sl_premium');
        expect(inactiveAccount.receipts.length).to.equal(1);
        expect(typeof inactiveAccount._id).to.equal('string');
        expect(typeof inactiveAccount.createdAt).to.equal('string');
        expect(typeof inactiveAccount.updatedAt).to.equal('string');

        const { cancellationReason, expired, active } = await getSubscriptionFromCustomParams({
          user: theUserId,
          entitlement,
          type: 'google',
          active: false,
        });
        expect(cancellationReason).to.equal(0);
        expect(expired).to.not.equal(null);
        expect(active).to.equal(false);
      });
    });
  });
}
