import { expect } from 'chai';
import uuidV1 from 'uuid';
import request from 'superagent';
import { SubscriptionModel } from '../../../../src/models/subscriptions/SubscriptionModel';
import { deleteSubscriptions } from '../../../../src/models/subscriptions';

export default function adminSubscriptionTests(theUserId) {
  const servicesURI = 'http://localhost:3000';
  const adminSubscriptionEndpoint = `${servicesURI}/admin/subscriptions/wavetrak`;

  const adminGetSubscriptions = userId =>
    new Promise((resolve, reject) => {
      request
        .get(adminSubscriptionEndpoint)
        .query({ userId })
        .end((err, res) => {
          if (err) return reject(err);
          const { body } = res;
          return resolve(body);
        });
    });

  const teardownSubscription = async () => {
    await deleteSubscriptions(theUserId);
  };

  const seedSubscription = async (brand, type, opts = {}) => {
    const subscription = await SubscriptionModel.create({
      type,
      subscriptionId: uuidV1(),
      entitlement: `${brand}_premium`,
      planId: `${brand}_monthly`,
      user: theUserId,
      start: 1579201980,
      end: 1894821181,
      active: true,
      ...opts,
    });
    return subscription;
  };

  describe('Admin Subscription Tests [Subscription API v2]', () => {
    afterEach(async () => {
      await teardownSubscription();
    });

    describe('GET Admin Subscritpions', () => {
      it('should return all subscriptions for a single user', async () => {
        await seedSubscription('bw', 'stripe', { stripeCustomerId: 'cus_1234', expiring: false });
        await seedSubscription('sl', 'stripe', { stripeCustomerId: 'cus_1234', active: false });

        const { subscriptions } = await adminGetSubscriptions(theUserId.toString());
        expect(subscriptions.length).to.equal(2);

        const [
          { user: sub1User, planId: planId1 },
          { user: sub2User, planId: planId2 },
        ] = subscriptions;
        expect(sub1User).to.equal(theUserId.toString());
        expect(sub1User).to.equal(sub2User);
        expect(planId2).to.equal('sl_monthly');
        expect(planId1).to.equal('bw_monthly');
      });
    });
  });
}
