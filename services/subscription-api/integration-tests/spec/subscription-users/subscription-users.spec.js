import sinon from 'sinon';
import { expect } from 'chai';
import mongoose from 'mongoose';
import { verfifyItunesReceipt, verifyGooglePurchase } from '../../helpers/simulateSubscription';
import {
  deleteSubscriptionUsersBySubscriptionId,
  getSubscriptionUsersBySubscriptionId,
} from '../../../src/models/subscription-users';
import iTunesReceiptData from '../../../src/__stubs__/itunes/BASE_RECEIPT';
import { keysToCamel } from '../../../src/utils/toCamelCase';
import { createSubscriptionDate as createDate } from '../../helpers/testUtils';
import * as itunesApi from '../../../src/external/itunes';
import * as googlePlayAPI from '../../../src/external/google';
import googlePurchaseData from '../../../src/__stubs__/google/BASE_PURCHASE_DATA';
import googleSignature from '../../../src/__stubs__/google/BASE_SIGNATUTRE';

import {
  deleteSubscriptions,
  getActiveSubscriptionByUserIdAndEntitlement,
} from '../../../src/models/subscriptions';
import SubscriptionUsersModel from '../../../src/models/subscription-users/SubscriptionUsersModel';
import { SubscriptionPurchaseModel } from '../../../src/models/subscription-purchases/SubscriptionPurchaseModel';

export default function subscriptionUsersIntegrationTests(theUserId) {
  describe('Subscription Users Tests [Subscription API V2]', () => {
    const teardownSubscriptionUsers = async subscriptionId => {
      await deleteSubscriptionUsersBySubscriptionId(subscriptionId);
    };

    const teardownSubscription = async () => {
      await deleteSubscriptions(theUserId);
    };

    const tearDownTestSubscriptionPurchase = async () =>
      SubscriptionPurchaseModel.deleteOne({ user: theUserId });

    const seedSubscriptionUsers = async subscription => {
      const { type, subscriptionId, entitlement, userId } = subscription;
      for (let i = 0; i < 3; i += 1) {
        await SubscriptionUsersModel.create({
          type,
          subscriptionId,
          entitlement,
          user: i === 0 ? userId : mongoose.Types.ObjectId(),
        });
      }
      return true;
    };

    afterEach(async () => {
      await teardownSubscription();
      await tearDownTestSubscriptionPurchase();
    });
    describe('Subscription Users iTunes Tests', () => {
      let getReceiptDataStub;
      const testReceiptString = 'ENCRYPTEDSTUBABC123';

      const defaultItunesReceiptDataOpts = {
        productId: 'surfline.iosapp.1month.subscription.notrial',
        isTrialPeriod: 'false',
        transactionId: '201910100000',
        originalTransactionId: '201910100000',
      };

      beforeEach(async () => {
        getReceiptDataStub = sinon.stub(itunesApi, 'default');
      });
      afterEach(async () => {
        getReceiptDataStub.restore();
      });
      it('should delete the Subscription Users when an itunes subscription expires', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt(theUserId, testReceiptString);
        const subscription = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        await seedSubscriptionUsers(subscription);
        const { subscriptionId } = subscription;
        const subscriptionUsers = await getSubscriptionUsersBySubscriptionId(subscriptionId);

        expect(subscriptionUsers.length).to.equal(3);

        const updatedReceiptData = iTunesReceiptData({
          ...defaultItunesReceiptDataOpts,
          expiresDateMs: createDate({ sub: 10 }),
          autoRenewStatus: '0',
        });
        getReceiptDataStub.returns(keysToCamel(updatedReceiptData));

        await verfifyItunesReceipt(theUserId, testReceiptString);

        const subscriptionUsersDeleted = await getSubscriptionUsersBySubscriptionId(subscriptionId);
        expect(subscriptionUsersDeleted.length).to.equal(0);

        await teardownSubscriptionUsers(subscriptionId);
      });
    });
    describe('Subscription Users Google Tests', () => {
      let getPurchaseDataStub;

      beforeEach(async () => {
        getPurchaseDataStub = sinon.stub(googlePlayAPI, 'default');
      });
      afterEach(async () => {
        getPurchaseDataStub.restore();
      });

      it('should delete the Subscription Users when a google subscription expires', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const subscription = await getActiveSubscriptionByUserIdAndEntitlement(theUserId, 'sl');

        await seedSubscriptionUsers(subscription);
        const { subscriptionId } = subscription;
        const subscriptionUsers = await getSubscriptionUsersBySubscriptionId(subscriptionId);

        expect(subscriptionUsers.length).to.equal(3);

        const [subUser] = subscriptionUsers;
        expect(subUser.subscriptionId).to.equal(subscriptionId);
        expect(subUser.type).to.equal('google');

        const expirationPurchaseData = googlePurchaseData({ expiration: true });

        getPurchaseDataStub.returns(expirationPurchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);

        const subscriptionUsersDeleted = await getSubscriptionUsersBySubscriptionId(subscriptionId);
        expect(subscriptionUsersDeleted.length).to.equal(0);

        await teardownSubscriptionUsers(subscriptionId);
      });
    });
  });
}
