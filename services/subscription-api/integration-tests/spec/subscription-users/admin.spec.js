import { expect } from 'chai';
import request from 'superagent';
import sinon from 'sinon';
import SubscriptionUsersModel from '../../../src/models/subscription-users/SubscriptionUsersModel';
import { SubscriptionPurchaseModel } from '../../../src/models/subscription-purchases/SubscriptionPurchaseModel';
import { deleteSubscriptions } from '../../../src/models/subscriptions';

import iTunesReceiptData from '../../../src/__stubs__/itunes/BASE_RECEIPT';
import { keysToCamel } from '../../../src/utils/toCamelCase';
import * as itunesApi from '../../../src/external/itunes';
import { verfifyItunesReceipt } from '../../helpers/simulateSubscription';
import UserInfoModel from '../../../src/models/v1/accounts/UserInfoModel';

export default function adminSubscriptionUsersTests(theUserId) {
  let getReceiptDataStub;
  const servicesURI = 'http://localhost:3000';
  const adminSubscriptionUsersEndpoint = `${servicesURI}/admin/subscription-users`;
  const subscription = '230000937945723';
  const testReceiptString = 'ENCRYPTEDSTUBABC123';
  const theAltUserId = '5ad73a077c1a3a00138a446e';

  const defaultItunesReceiptDataOpts = {
    productId: 'surfline.iosapp.1month.subscription.notrial',
    isTrialPeriod: 'false',
    transactionId: '201910100000',
    originalTransactionId: '201910100000',
  };

  const adminGetSubscriptionUsers = userId =>
    new Promise((resolve, reject) => {
      request.get(`${adminSubscriptionUsersEndpoint}/${userId}`).end((err, res) => {
        if (err) return reject(err);
        const { body } = res;
        return resolve(body);
      });
    });

  const adminPatchSubscriptionUsers = (oldPrimaryUser, newPrimaryUser) =>
    new Promise((resolve, reject) => {
      request
        .patch(adminSubscriptionUsersEndpoint)
        .send({ oldPrimaryUser, newPrimaryUser })
        .end((err, res) => {
          if (err) return reject(err);
          const { body } = res;
          return resolve(body);
        });
    });

  const adminDeleteSubscriptionUsers = userId =>
    new Promise((resolve, reject) => {
      request
        .delete(adminSubscriptionUsersEndpoint)
        .send({ userId })
        .end((err, res) => {
          if (err) return reject(err);
          return resolve(res);
        });
    });

  const teardownSubscriptionUsers = async () => {
    await SubscriptionUsersModel.deleteMany();
  };
  const teardownSubscriptionPurchases = () => SubscriptionPurchaseModel.deleteMany();
  const teardownUser = _id => UserInfoModel.deleteOne({ _id });

  const seedAltUserInfo = userId =>
    UserInfoModel.create({
      _id: userId,
      email: `${userId.toString()}@test.com`,
    });

  const seedSubscriptionUsers = async (
    user,
    primaryUser,
    isPrimaryUser,
    subscriptionId,
    opts = {},
  ) => {
    const result = await SubscriptionUsersModel.create({
      subscriptionId,
      type: 'apple',
      entitlement: 'sl_premium',
      user,
      primaryUser,
      isPrimaryUser,
      active: true,
      createdAt: '2021-05-04T03:05:26.301+0000',
      updatedAt: '2021-05-04T03:05:26.301+0000',
      ...opts,
    });
    return result;
  };

  const teardownSubscription = async user => {
    await deleteSubscriptions(user);
  };

  describe('Admin Subscription Users Tests [Subscription API v2]', () => {
    beforeEach(async () => {
      getReceiptDataStub = sinon.stub(itunesApi, 'default');
      await teardownSubscriptionUsers();
      await teardownSubscription();
    });
    afterEach(async () => {
      getReceiptDataStub.restore();
      await teardownSubscriptionUsers();
      await teardownSubscription();
      await teardownSubscriptionPurchases();
      await teardownUser(theAltUserId);
    });

    describe('GET Admin Subscription Users', () => {
      it('should return all SubscriptionUsers assosciated to a user', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));
        await verfifyItunesReceipt(theUserId, testReceiptString);
        await seedSubscriptionUsers(theUserId, theUserId, true, subscription);
        try {
          await seedAltUserInfo(theAltUserId);
          await verfifyItunesReceipt(theAltUserId, testReceiptString);
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('This receipt is subscribed to a different account');
        }

        const { subscriptionUsers } = await adminGetSubscriptionUsers(theUserId);

        expect(subscriptionUsers.length).to.equal(2);
        const {
          primaryUser,
          user: { _id },
          isEntitled,
        } = subscriptionUsers.find(({ isPrimaryUser }) => isPrimaryUser === true);

        expect(isEntitled).to.not.equal(false);
        expect(primaryUser.toString()).to.equal(theUserId.toString());
        expect(_id.toString()).to.equal(theUserId.toString());

        const { isEntitled: isEntitledFalse } = subscriptionUsers.find(
          ({ user }) => user._id === theAltUserId,
        );
        expect(isEntitledFalse).to.equal(false);
      });
      it('should return a 404 if there is no SubscriptionUsers assosciated to a user', async () => {
        await seedSubscriptionUsers(theAltUserId, theUserId, true, subscription);
        await seedSubscriptionUsers('572b480ea00bf0ce6d415976', theUserId, false, subscription);
        await seedSubscriptionUsers('60edd770f21312001393a9a4', theUserId, false, subscription);
        try {
          await adminGetSubscriptionUsers('60edd770f21312001393a9a1');
        } catch (error) {
          expect(error).to.exist();
          expect(error.status).to.equal(404);
        }
      });
    });
    describe('PATCH Admin Subscription Users', () => {
      it('should update all SubscriptionUsers assosciated to a user', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));
        await verfifyItunesReceipt(theUserId, testReceiptString);
        await seedSubscriptionUsers(theUserId, theUserId, true, subscription);

        try {
          await seedAltUserInfo(theAltUserId);
          await verfifyItunesReceipt(theAltUserId, testReceiptString);
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('This receipt is subscribed to a different account');
        }

        const { subscriptionUsers } = await adminGetSubscriptionUsers(theUserId);
        const {
          primaryUser,
          user: { _id },
          isEntitled,
        } = subscriptionUsers.find(({ isPrimaryUser }) => isPrimaryUser === true);

        expect(isEntitled).to.not.equal(false);
        expect(primaryUser.toString()).to.equal(theUserId.toString());
        expect(_id.toString()).to.equal(theUserId.toString());

        const { isEntitled: isEntitledFalse } = subscriptionUsers.find(
          ({ user }) => user._id === theAltUserId,
        );
        expect(isEntitledFalse).to.equal(false);

        const response = await adminPatchSubscriptionUsers(theUserId, theAltUserId);
        expect(response.message).to.equal('success');

        const { subscriptionUsers: patchedSubscriptionUsers } = await adminGetSubscriptionUsers(
          theUserId,
        );
        expect(patchedSubscriptionUsers.length).to.equal(2);

        const patchedAltUser = patchedSubscriptionUsers.find(
          ({ user }) => user._id === theAltUserId,
        );
        expect(patchedAltUser.isEntitled).to.equal(true);
      });
      it('should return a 400 error if required fields are missing', async () => {
        try {
          await adminPatchSubscriptionUsers();
        } catch (error) {
          expect(error).to.exist();
          expect(error.status).to.equal(400);
        }
      });
      it('should return a 404 error if there are no SubscriptionUsers for a user', async () => {
        try {
          await adminPatchSubscriptionUsers(theAltUserId, theAltUserId);
        } catch (error) {
          expect(error).to.exist();
          expect(error.status).to.equal(404);
        }
      });
    });
    describe('DELETE Admin Subscription Users', () => {
      it('should return 200 if a Subscription User does not exist', async () => {
        const res = await adminDeleteSubscriptionUsers('60edd770f21312001393a9a1');
        expect(res.status).to.equal(200);
      });
      it('should return 400 if a Subscription User is the primary user', async () => {
        try {
          await seedSubscriptionUsers(theAltUserId, theAltUserId, true, subscription);
          await await adminDeleteSubscriptionUsers(theAltUserId);
        } catch (error) {
          expect(error).to.exist();
          expect(error.status).to.equal(400);
        }
      });
      it('should return 200 if a Subscription User is the primary user but the subscription is not active', async () => {
        try {
          await seedSubscriptionUsers(theAltUserId, theAltUserId, false, subscription);
          await await adminDeleteSubscriptionUsers(theAltUserId);
        } catch (error) {
          expect(error).to.exist();
          expect(error.status).to.equal(400);
        }
      });
      it('should return 200 if a Subscription User is successfully deleted', async () => {
        await seedSubscriptionUsers(theAltUserId, theUserId, false, subscription);
        const res = await await adminDeleteSubscriptionUsers(theAltUserId);
        expect(res.status).to.equal(200);
      });
    });
  });
}
