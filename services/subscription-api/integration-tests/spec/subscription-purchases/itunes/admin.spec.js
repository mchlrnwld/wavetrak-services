import sinon from 'sinon';
import { expect } from 'chai';
import {
  verfifyItunesReceipt,
  simulateAdminGetItunesReceipts,
  simulateAdminPostItunesReceipts,
} from '../../../helpers/simulateSubscription';
import * as itunesApi from '../../../../src/external/itunes';
import iTunesReceiptData from '../../../../src/__stubs__/itunes/BASE_RECEIPT';
import { keysToCamel } from '../../../../src/utils/toCamelCase';
import { deleteSubscriptions } from '../../../../src/models/subscriptions';
import { SubscriptionPurchaseModel } from '../../../../src/models/subscription-purchases/SubscriptionPurchaseModel';

export default function iTunesReceiptAdminIntegrationTests(theUserId) {
  let getReceiptDataStub;
  const testReceiptString = 'ENCRYPTEDSTUBABC123';

  const defaultItunesReceiptDataOpts = {
    productId: 'surfline.iosapp.1month.subscription.notrial',
    isTrialPeriod: 'false',
    transactionId: '201910100000',
    originalTransactionId: '201910100000',
  };

  const teardownSubscription = async () => {
    await deleteSubscriptions(theUserId);
  };

  const tearDownTestSubscriptionPurchase = async () =>
    SubscriptionPurchaseModel.deleteOne({ user: theUserId });

  describe('iTunes Receipt Admin Tests [Purchases API V2]', () => {
    beforeEach(async () => {
      getReceiptDataStub = sinon.stub(itunesApi, 'default');
    });
    afterEach(async () => {
      getReceiptDataStub.restore();
      await teardownSubscription();
      await tearDownTestSubscriptionPurchase();
    });
    describe('GET iTunes Receipts', () => {
      it('should return all iTunes Subscription Purchases for a single user', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt(theUserId, testReceiptString);
        const {
          purchases: [{ type, user, latestReceipt }],
        } = await simulateAdminGetItunesReceipts(theUserId);
        expect(type).to.equal('itunes');
        expect(user.toString()).to.equal(theUserId.toString());
        expect(latestReceipt).to.equal(receiptData.latest_receipt);
      });
    });
    describe('POST iTunes Receipts', () => {
      it('should return the updated itunes subscription purchase', async () => {
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt(theUserId, testReceiptString);
        const {
          purchases: [{ _id: purchaseId, type, user, latestReceipt }],
        } = await simulateAdminGetItunesReceipts(theUserId);
        expect(type).to.equal('itunes');
        expect(user.toString()).to.equal(theUserId.toString());
        expect(latestReceipt).to.equal(receiptData.latest_receipt);

        const { purchase } = await simulateAdminPostItunesReceipts(purchaseId);
        expect(purchase._id.toString()).to.equal(purchaseId.toString());
        expect(purchase.user.toString()).to.equal(theUserId.toString());
        expect(purchase.type).to.equal(type);
      });
    });
  });
}
