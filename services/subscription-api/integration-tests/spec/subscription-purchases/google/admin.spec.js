import sinon from 'sinon';
import { expect } from 'chai';
import * as googlePlayAPI from '../../../../src/external/google';
import * as getUser from '../../../../src/external/user';
import { deleteSubscriptions } from '../../../../src/models/subscriptions';
import googlePurchaseData from '../../../../src/__stubs__/google/BASE_PURCHASE_DATA';
import googleSignature from '../../../../src/__stubs__/google/BASE_SIGNATUTRE';
import { SubscriptionPurchaseModel } from '../../../../src/models/subscription-purchases/SubscriptionPurchaseModel';
import {
  verifyGooglePurchase,
  simulateAdminGetGooglePurchases,
  simulateAdminPostGooglePurchase,
  simulateAdminPutGooglePurchase,
  simulateAdminDeferGooglePurchase,
} from '../../../helpers/simulateSubscription';

export default function googlePurchaseAdminIntegrationTests(theUserId) {
  let getUserStub;
  let getPurchaseDataStub;
  let postPurchaseDataStub;

  const teardownSubscription = async () => {
    await deleteSubscriptions(theUserId);
  };

  const tearDownTestSubscriptionPurchase = async () =>
    SubscriptionPurchaseModel.deleteOne({ user: theUserId });

  describe('Google Purchase Admin Tests [Purchases API V2]', () => {
    before(async () => {
      await teardownSubscription();
      await tearDownTestSubscriptionPurchase();
    });
    beforeEach(async () => {
      getUserStub = sinon.stub(getUser, 'getUserDetails');
      getPurchaseDataStub = sinon.stub(googlePlayAPI, 'default');
      postPurchaseDataStub = sinon.stub(googlePlayAPI, 'postPurchaseData');
    });
    afterEach(async () => {
      getUserStub.restore();
      getPurchaseDataStub.restore();
      postPurchaseDataStub.restore();
      await teardownSubscription();
      await tearDownTestSubscriptionPurchase();
    });
    describe('GET Google Purchases', () => {
      it('should return all google Subscription Purchases for a single user', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          receipt: { data },
        } = purchaseSignature;
        const {
          purchases: [{ type, user, purchaseToken }],
        } = await simulateAdminGetGooglePurchases(theUserId);
        expect(type).to.equal('google-play');
        expect(user.toString()).to.equal(theUserId.toString());
        expect(purchaseToken).to.equal(data.purchaseToken);
      });
    });
    describe('POST Google Purchases', () => {
      it('should return the updated google subscription purchase', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          receipt: { data },
        } = purchaseSignature;
        const {
          purchases: [{ _id: purchaseId, type, user, purchaseToken }],
        } = await simulateAdminGetGooglePurchases(theUserId);
        expect(type).to.equal('google-play');
        expect(user.toString()).to.equal(theUserId.toString());
        expect(purchaseToken).to.equal(data.purchaseToken);

        const { purchase } = await simulateAdminPostGooglePurchase(purchaseId);
        expect(purchase._id.toString()).to.equal(purchaseId.toString());
        expect(purchase.user.toString()).to.equal(theUserId.toString());
        expect(purchase.type).to.equal(type);
      });
    });
    describe('PUT Google Purchases', () => {
      it('should return the updated google subscription purchase from a "cancel" command', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          receipt: { data },
        } = purchaseSignature;
        const {
          purchases: [{ _id: purchaseId, type, user, purchaseToken }],
        } = await simulateAdminGetGooglePurchases(theUserId);
        expect(type).to.equal('google-play');
        expect(user.toString()).to.equal(theUserId.toString());
        expect(purchaseToken).to.equal(data.purchaseToken);

        postPurchaseDataStub.returns({});
        const { purchase } = await simulateAdminPutGooglePurchase(purchaseId, 'cancel');
        expect(purchase._id.toString()).to.equal(purchaseId.toString());
        expect(purchase.user.toString()).to.equal(theUserId.toString());
        expect(purchase.type).to.equal(type);
      });
      it('should return the updated google subscription purchase from a "refund" command', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          receipt: { data },
        } = purchaseSignature;
        const {
          purchases: [{ _id: purchaseId, type, user, purchaseToken }],
        } = await simulateAdminGetGooglePurchases(theUserId);
        expect(type).to.equal('google-play');
        expect(user.toString()).to.equal(theUserId.toString());
        expect(purchaseToken).to.equal(data.purchaseToken);

        postPurchaseDataStub.returns({});
        const { purchase } = await simulateAdminPutGooglePurchase(purchaseId, 'refund');
        expect(purchase._id.toString()).to.equal(purchaseId.toString());
        expect(purchase.user.toString()).to.equal(theUserId.toString());
        expect(purchase.type).to.equal(type);
      });
      it('should return the updated google subscription purchase from a "revoke" command', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          receipt: { data },
        } = purchaseSignature;
        const {
          purchases: [{ _id: purchaseId, type, user, purchaseToken }],
        } = await simulateAdminGetGooglePurchases(theUserId);
        expect(type).to.equal('google-play');
        expect(user.toString()).to.equal(theUserId.toString());
        expect(purchaseToken).to.equal(data.purchaseToken);

        postPurchaseDataStub.returns({});
        const { purchase } = await simulateAdminPutGooglePurchase(purchaseId, 'revoke');
        expect(purchase._id.toString()).to.equal(purchaseId.toString());
        expect(purchase.user.toString()).to.equal(theUserId.toString());
        expect(purchase.type).to.equal(type);
      });
    });
    describe('Defer [POST] Google Purchases', () => {
      it('should return the updated google subscription purchase from a "cancel" command', async () => {
        const purchaseSignature = googleSignature();
        const purchaseData = googlePurchaseData();

        getPurchaseDataStub.returns(purchaseData);
        await verifyGooglePurchase(theUserId, purchaseSignature);
        const {
          receipt: { data },
        } = purchaseSignature;
        const {
          purchases: [{ _id: purchaseId, type, user, purchaseToken }],
        } = await simulateAdminGetGooglePurchases(theUserId);
        expect(type).to.equal('google-play');
        expect(user.toString()).to.equal(theUserId.toString());
        expect(purchaseToken).to.equal(data.purchaseToken);

        postPurchaseDataStub.returns({});
        const { purchase } = await simulateAdminDeferGooglePurchase(purchaseId, 2629800000);
        expect(purchase._id.toString()).to.equal(purchaseId.toString());
        expect(purchase.user.toString()).to.equal(theUserId.toString());
        expect(purchase.type).to.equal(type);
      });
    });
  });
}
