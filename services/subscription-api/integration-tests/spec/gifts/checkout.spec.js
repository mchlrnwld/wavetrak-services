import sinon from 'sinon';
import { expect } from 'chai';
import * as stripe from '../../../src/external/stripe';
import { simulateGiftCheckout } from '../../helpers/simulateSubscription';

export default function giftCheckoutIntegrationTests(theUserId) {
  let startCheckoutSessionSpy;

  const giftCheckoutParams = {
    brand: 'sl',
    country: 'us',
    currency: 'usd',
    interval: 'month',
    intervalCount: 1,
    successUrl: 'https://www.surfline.com/gift-cards/success',
    cancelUrl: 'https://www.surfline.com/gift-cards',
    giftMessage: 'Happy Birthday!',
    purchaserEmail: 'gift-purchase@test.com',
    purchaserName: 'Jane Doe',
    recipientEmail: 'recipient@test.com',
    recipientName: 'John Doe',
  };
  describe('Gift Checkout Tests', () => {
    beforeEach(async () => {
      startCheckoutSessionSpy = sinon.spy(stripe, 'startCheckoutSession');
    });
    afterEach(async () => {
      startCheckoutSessionSpy.restore();
    });

    it('should create a Stripe Checkout session', async () => {
      const res = await simulateGiftCheckout(theUserId, { ...giftCheckoutParams });
      expect(res.status).to.equal(200);
      const [
        { cancelUrl, successUrl, mode, priceId, metadata, paymentIntentData },
      ] = startCheckoutSessionSpy.firstCall.args;

      expect(cancelUrl).to.equal('https://www.surfline.com/gift-cards');
      expect(successUrl).to.equal('https://www.surfline.com/gift-cards/success');
      expect(mode).to.equal('payment');
      expect(priceId).to.equal('price_1KJLjTLwFGcBvXawASsUqska');
      expect(res.body.url).to.startWith('https://checkout.stripe.com');
      expect(res.body.sessionId).to.exist(true);
      expect(metadata.priceId).to.equal(priceId);
      expect(paymentIntentData).to.deep.equal({
        metadata: {
          type: 'gift_purchase',
          giftMessage: 'Happy Birthday!',
          purchaserEmail: 'gift-purchase@test.com',
          purchaserName: 'Jane Doe',
          recipientEmail: 'recipient@test.com',
          recipientName: 'John Doe',
          priceId,
          brand: 'sl',
          userId: theUserId.toString(),
        },
      });
    });

    it('should return an error if it cannot find an associated price id', async () => {
      try {
        await simulateGiftCheckout(theUserId, { ...giftCheckoutParams, intervalCount: 24 });
      } catch (error) {
        const { status, response } = error;
        expect(status).to.equal(400);
        const {
          body: { message },
        } = response;
        expect(message).to.equal(
          'Must provide a priceId or a valid brand, currency, and interval.',
        );
      }
    });

    it('should return an error if the urls are invalid', async () => {
      try {
        await simulateGiftCheckout(theUserId, {
          ...giftCheckoutParams,
          successUrl: 'https:google.com',
        });
      } catch (error) {
        const { status, response } = error;
        expect(status).to.equal(400);
        const {
          body: { message },
        } = response;
        expect(message).to.equal('The success and cancel URLs need to be valid');
      }
    });
  });
}
