import sinon from 'sinon';
import { expect } from 'chai';
import * as getUser from '../../../src/external/user';
import { createCharge, getCharge, createCustomer } from '../../../src/external/stripe';
import { genTestToken } from '../../../src/external/stripeutils';
import ProductModel from '../../../src/models/gifts/ProductModel';
import { RedeemCodeModel } from '../../../src/models/gifts/RedeemCodeModel';
import { SubscriptionModel } from '../../../src/models/subscriptions/SubscriptionModel';
import { SubscriptionPurchaseModel } from '../../../src/models/subscription-purchases/SubscriptionPurchaseModel';
import UserAccountModel from '../../../src/models/v1/accounts/UserAccountModel';
import {
  simulateGetRedeem,
  simulatePostRedeem,
  simulatePostBilling,
  simulateGetBilling,
  verfifyItunesReceipt,
} from '../../helpers/simulateSubscription';
import { deleteStripeSubscriptions } from '../../../src/models/subscriptions/stripe';
import { removeStripeCustomer } from '../../../src/models/customers/stripe';
import { keysToCamel } from '../../../src/utils/toCamelCase';
import iTunesReceiptData from '../../../src/__stubs__/itunes/BASE_RECEIPT';
import * as itunesApi from '../../../src/external/itunes';
import { testItunesReceiptString, defaultItunesReceiptDataOpts } from '../../helpers/testUtils';
import * as stripe from '../../../src/external/stripe';
import simulateStripeEvent from '../../helpers/simulateStripeEvent';
import * as trackRedeemedGiftCard from '../../../src/tracking/redeemedGiftCard';

export default function giftRedeemIntegrationTests(theUserId) {
  let theRedeemCode;
  let theGiftProduct;
  let theChargeId;
  let getUserStub;
  let getReceiptDataStub;
  let getInvoiceFromChargeStub;
  let trackRedeemedGiftCardSpy;
  const slAnnualPriceId = 'price_1KMDTZLwFGcBvXawjCayM4YC';

  const stubbedUser = {
    _id: theUserId.toString(),
    firstName: 'Joel',
    lastName: 'Parkinson',
    email: 'joel@example.com',
    usID: 783514,
  };

  const createTestStripeCustomer = async () => {
    const { email } = stubbedUser;
    const { id: tokenId } = await genTestToken();
    return createCustomer({
      source: tokenId,
      email,
    });
  };

  const seedGiftProduct = async brand => {
    const giftProduct = await ProductModel.create({
      brand,
      name: '1-Month',
      duration: 1,
      price: 999,
      isActive: true,
      imagePath: `https://creatives.cdn-surfline.com/giftcards/cards/${brand}/1month.png`,
      bestValue: false,
      description: `1 Month of ${brand} Premium`,
    });
    theGiftProduct = giftProduct;
  };

  const seedCharge = async (giftProduct, ccNum = '4242424242424242') => {
    const { id: tokenId } = await genTestToken(ccNum);
    const { email } = stubbedUser;
    const { id: chargeId } = await createCharge(giftProduct, tokenId, email, 'test');
    theChargeId = chargeId;
  };

  const seedRedeemCode = async (giftProduct, chargeId = null, code = 'ABC123') => {
    const { _id: productId } = giftProduct;
    const { redeemCode } = await RedeemCodeModel.create({
      type: 'legacy',
      giftProduct: productId,
      redeemCode: code,
      purchaserEmail: 'purchaser@test.com',
      chargeId,
    });
    theRedeemCode = redeemCode;
  };

  describe('Gift Redeem API Tests [Subscription API v2]', () => {
    beforeEach(async () => {
      await seedGiftProduct('sl');
      await seedCharge(theGiftProduct);
      await seedRedeemCode(theGiftProduct, theChargeId);
      getReceiptDataStub = sinon.stub(itunesApi, 'default');
      getUserStub = sinon.stub(getUser, 'getUserDetails');
      getInvoiceFromChargeStub = sinon.stub(stripe, 'getInvoiceFromCharge');
      trackRedeemedGiftCardSpy = sinon.spy(trackRedeemedGiftCard, 'trackRedeemedGiftCard');
    });
    afterEach(async () => {
      await ProductModel.deleteMany({});
      await RedeemCodeModel.deleteMany({});
      await SubscriptionModel.deleteMany({});
      await UserAccountModel.deleteMany({});
      await SubscriptionPurchaseModel.deleteOne({ user: theUserId });
      await deleteStripeSubscriptions(theUserId);
      await removeStripeCustomer(theUserId);
      getUserStub.restore();
      getReceiptDataStub.restore();
      getInvoiceFromChargeStub.restore();
      trackRedeemedGiftCardSpy.restore();
    });

    describe('GET Gift Redeem API', () => {
      it('should return the gift product details for a valid redeem code', async () => {
        getUserStub.returns({ ...stubbedUser });
        const { subscription, giftProduct } = await simulateGetRedeem(theUserId, theRedeemCode);
        expect(subscription).to.equal(null);

        const { price, duration } = giftProduct;
        expect(price).to.equal(999);
        expect(duration).to.equal(1);
      });
      it('should return the gift prodcut details and subscription for a premium user', async () => {
        getUserStub.returns({ ...stubbedUser });
        const { message } = await simulatePostBilling(theUserId);
        expect(message).to.equal('Successfully processed the Subscription');

        const { subscription, giftProduct } = await simulateGetRedeem(theUserId, theRedeemCode);
        const { price, duration, brand } = giftProduct;
        const { entitlement } = subscription;

        expect(entitlement.includes(brand)).to.equal(true);
        expect(price).to.equal(999);
        expect(duration).to.equal(1);
      });
      it('should throw an API Error for ineligible active subscription types', async () => {
        getUserStub.returns({ ...stubbedUser });
        const receiptData = iTunesReceiptData(defaultItunesReceiptDataOpts);
        getReceiptDataStub.returns(keysToCamel(receiptData));

        await verfifyItunesReceipt(theUserId, testItunesReceiptString);

        try {
          await simulateGetRedeem(theUserId, theRedeemCode);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;

          expect(statusCode).to.equal(400);
          expect(code).to.equal(1002);
          expect(message).to.equal('VIP and In-App Subscriptions are not eligible.');
        }
      });
      it('should throw an API Error for ineligible currencies', async () => {
        getUserStub.returns({ ...stubbedUser });
        const { message } = await simulatePostBilling(theUserId, {
          plan: 'sl_annual_aud',
          source: '4242424242424242',
          trialEligible: false,
          coupon: null,
        });
        expect(message).to.equal('Successfully processed the Subscription');

        try {
          await simulateGetRedeem(theUserId, theRedeemCode);
        } catch (error) {
          const {
            statusCode,
            body: { message: errMsg, code },
          } = error;

          expect(statusCode).to.equal(400);
          expect(code).to.equal(1004);
          expect(errMsg).to.equal(
            'This gift card was purchased in USD and will not support credit toward your subscription currency at this time.',
          );
        }
      });
      it('should throw an API Error for subscriptions with open disputes', async () => {
        getUserStub.returns({ ...stubbedUser });
        const { message } = await simulatePostBilling(theUserId, {
          plan: slAnnualPriceId,
          source: '4242424242424242',
          trialEligible: false,
          coupon: null,
        });
        expect(message).to.equal('Successfully processed the Subscription');

        const { stripeCustomerId, subscriptionId } = await simulateGetBilling(theUserId, 'sl');
        getInvoiceFromChargeStub.returns({
          customer: stripeCustomerId,
          subscription: subscriptionId,
        });

        await simulateStripeEvent('charge.dispute.created', { stripeCustomerId, subscriptionId });

        try {
          await simulateGetRedeem(theUserId, theRedeemCode);
        } catch (error) {
          const {
            statusCode,
            body: { message: errMsg, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(code).to.equal(1003);
          expect(errMsg).to.equal(
            'Your billing has been disputed. Please contact support@surfline.com.',
          );
        }
      });
      it('should throw an API Error for a disputed charge of an eligible redeemCode', async () => {
        const disputeTestCCNum = '4000000000000259';
        await seedCharge(theGiftProduct, disputeTestCCNum);
        await seedRedeemCode(theGiftProduct, theChargeId, 'DEF456');
        try {
          await simulateGetRedeem(theUserId, theRedeemCode);
        } catch (error) {
          const {
            statusCode,
            body: { message, code },
          } = error;
          expect(statusCode).to.equal(400);
          expect(code).to.equal(1001);
          expect(message).to.equal('Purchase charge disputed');
        }
      });
    });
    describe('POST Gift Redeem API', () => {
      it('should create a gift subscription for a non-premium user', async () => {
        getUserStub.returns({ ...stubbedUser });

        const { subscription, accountBalance } = await simulatePostRedeem(theUserId, theRedeemCode);
        expect(accountBalance).to.equal(null);

        const { active, type, chargeId, entitlement, start, end } = subscription;
        expect(active).to.equal(true);
        expect(type).to.equal('gift');
        expect(entitlement).to.equal('sl_premium');
        expect(chargeId).to.equal(theChargeId);

        const charge = await getCharge(theChargeId);
        const {
          metadata: { redeemedBy, creditApplied },
        } = charge;
        expect(redeemedBy).to.equal(theUserId.toString());
        expect(creditApplied).to.equal('false');
        expect(end - start).to.equal(2628000);
        expect(trackRedeemedGiftCardSpy.callCount).to.equal(1);
      });
      it('should extend the end date for a user with an active gift subscription', async () => {
        getUserStub.returns({ ...stubbedUser });
        await seedRedeemCode(theGiftProduct, theChargeId, 'DEF321');
        const { subscription } = await simulatePostRedeem(theUserId, 'DEF321');
        const { end } = subscription;
        const { subscription: updatedSubscription, accountBalance } = await simulatePostRedeem(
          theUserId,
          'ABC123',
        );
        expect(accountBalance).to.equal(null);

        const charge = await getCharge(theChargeId);
        const {
          metadata: { redeemedBy, creditApplied },
        } = charge;
        expect(redeemedBy).to.equal(theUserId.toString());
        expect(creditApplied).to.equal('false');

        const { active, type, chargeId, entitlement, end: extendedEnd } = updatedSubscription;
        expect(active).to.equal(true);
        expect(type).to.equal('gift');
        expect(entitlement).to.equal('sl_premium');
        expect(chargeId).to.equal(theChargeId);
        expect(extendedEnd).to.equal(end + 2628000);
        expect(trackRedeemedGiftCardSpy.callCount).to.equal(2);
      });
      it('should apply a credit to the account of an active stripe subscriber', async () => {
        try {
          const { id: stripeCustomerId } = await createTestStripeCustomer();
          getUserStub.returns({ ...stubbedUser, stripeCustomerId });
          const { message } = await simulatePostBilling(theUserId, {
            plan: slAnnualPriceId,
            source: '4242424242424242',
            trialEligible: false,
            coupon: null,
          });
          expect(message).to.equal('Successfully processed the Subscription');
          const { end } = await simulateGetBilling(theUserId, 'sl');

          const { subscription, accountBalance } = await simulatePostRedeem(
            theUserId,
            theRedeemCode,
          );
          expect(accountBalance).to.equal(-999);

          const { active, type, entitlement, end: unmodifedEnd } = subscription;
          expect(active).to.equal(true);
          expect(type).to.equal('stripe');
          expect(entitlement).to.equal('sl_premium');
          expect(unmodifedEnd).to.equal(end);
          expect(trackRedeemedGiftCardSpy.callCount).to.equal(1);
        } catch (error) {}
      });
    });
  });
}
