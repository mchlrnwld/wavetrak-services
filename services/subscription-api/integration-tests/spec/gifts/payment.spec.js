import request from 'superagent';
import sinon from 'sinon';
import { expect } from 'chai';
import * as getUser from '../../../src/external/user';
import ProductModel from '../../../src/models/gifts/ProductModel';
import { RedeemCodeModel } from '../../../src/models/gifts/RedeemCodeModel';
import { genTestToken } from '../../../src/external/stripeutils';
import { createCustomer, deleteCustomer } from '../../../src/external/stripe';
import { createStripeCustomer, removeStripeCustomer } from '../../../src/models/customers/stripe';
import * as trackPurchasedGift from '../../../src/tracking/purchasedGift';

export default function giftPaymentIntegrationTests(theUserId) {
  const servicesURI = 'http://localhost:3000';
  const giftPaymentAPI = `${servicesURI}/gift/payment`;

  const stubbedUser = {
    _id: theUserId.toString(),
    firstName: 'Joel',
    lastName: 'Parkinson',
    email: 'joel@example.com',
    usID: 783514,
  };

  // Set Up TODOs
  const seedProduct = async brand => {
    await ProductModel.create({
      brand,
      name: '1-Month',
      duration: 1,
      price: 999,
      isActive: true,
      imagePath: `https://creatives.cdn-surfline.com/giftcards/cards/${brand}/1month.png`,
      bestValue: false,
      description: `1 Month of ${brand} Premium`,
    });
  };

  const createTestStripeCustomer = async () => {
    const { email } = stubbedUser;
    const { id: tokenId } = await genTestToken();
    const res = await createCustomer({
      source: tokenId,
      email,
    });
    await createStripeCustomer(theUserId, res.id);
    return res;
  };

  const simulateGetProductsWithAuth = ({ brand }) => {
    return new Promise((resolve, reject) => {
      request
        .get(giftPaymentAPI)
        .query({ product: brand })
        .set('x-auth-userid', theUserId)
        .end((err, result) => {
          if (err) {
            reject(result);
          }
          resolve(result.body);
        });
    });
  };
  const simulateGetProductsNoAuth = ({ brand }) => {
    return new Promise((resolve, reject) => {
      request
        .get(giftPaymentAPI)
        .query({ product: brand })
        .end((err, result) => {
          if (err) {
            reject(result);
          }
          resolve(result.body);
        });
    });
  };

  const simulatePostProductsWithAuth = data => {
    return new Promise((resolve, reject) => {
      request
        .post(giftPaymentAPI)
        .set('accept', 'json')
        .send(data)
        .set('x-auth-userid', theUserId)
        .end((err, result) => {
          if (err) {
            reject(result);
          }
          resolve(result.body);
        });
    });
  };

  describe('Gift Payment API Tests [Subscription API V2]', () => {
    before(async () => {
      await seedProduct('sl');
      await seedProduct('bw');
    });
    after(async () => {
      await ProductModel.deleteMany({});
      await RedeemCodeModel.deleteMany({});
    });

    let getUserStub;
    beforeEach(() => {
      getUserStub = sinon.stub(getUser, 'getUserDetails');
    });

    afterEach(async () => {
      getUserStub.restore();
      await removeStripeCustomer(theUserId);
    });

    describe('GET Gift Payment API', () => {
      it('should return gift products for unauthenticated requests', async () => {
        const { products, email, card } = await simulateGetProductsNoAuth({ brand: 'sl' });
        expect(email).to.equal(null);
        expect(card).to.equal(null);

        const otherBrands = products.filter(({ brand }) => brand !== 'sl');
        expect(otherBrands.length).to.equal(0);

        const targetedBrand = products.filter(({ brand }) => brand === 'sl');
        expect(targetedBrand).to.not.equal(0);
      });
      it('should return gift products for authenticated requests', async () => {
        getUserStub.returns({ ...stubbedUser });

        const { products, email, card } = await simulateGetProductsWithAuth({ brand: 'sl' });
        expect(email).to.equal(stubbedUser.email);
        expect(card).to.equal(null);

        const otherBrands = products.filter(({ brand }) => brand !== 'sl');
        expect(otherBrands.length).to.equal(0);

        const targetedBrand = products.filter(({ brand }) => brand === 'sl');
        expect(targetedBrand).to.not.equal(0);
      });
      it('should return the default credit card of authenticated user if available', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        getUserStub.returns({ ...stubbedUser });

        const { products, email, card } = await simulateGetProductsWithAuth({ brand: 'sl' });
        expect(email).to.equal(stubbedUser.email);
        expect(card.id).to.not.equal(null);

        const otherBrands = products.filter(({ brand }) => brand !== 'sl');
        expect(otherBrands.length).to.equal(0);

        const targetedBrand = products.filter(({ brand }) => brand === 'sl');
        expect(targetedBrand).to.not.equal(0);
        await deleteCustomer(stripeCustomerId);
      });
      it('should throw an API error if product param is not included in the request', async () => {
        try {
          await simulateGetProductsNoAuth({ brand: null });
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('Cannot find products without a valid brand');
        }
      });
    });
    describe('POST Gift Payment API', () => {
      it('should throw an API Error if the required fields are not sent in the request', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        getUserStub.returns({ ...stubbedUser });

        const { products } = await simulateGetProductsWithAuth({ brand: 'sl' });
        const { _id: giftProductId } = products.find(({ brand }) => brand === 'sl');

        try {
          await simulatePostProductsWithAuth({ giftProductId });
        } catch (error) {
          const {
            statusCode,
            body: { message },
          } = error;
          expect(statusCode).to.equal(400);
          expect(message).to.equal('missing or malformed gift purchase params');
        }
        await deleteCustomer(stripeCustomerId);
      });
      it('should create a redeem code from a successful gift purchase', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        getUserStub.returns({ ...stubbedUser });

        const {
          products,
          email,
          card: { id: source },
        } = await simulateGetProductsWithAuth({ brand: 'sl' });
        const { _id: giftProductId } = products.find(({ brand }) => brand === 'sl');

        await simulatePostProductsWithAuth({
          giftProductId,
          source,
          purchaserEmail: email,
          purchaserName: 'Joel',
          recipientEmail: 'payment@test.com',
          recipientName: 'Test User',
          message: 'test',
        });

        const { giftProduct: productId, purchaserEmail, chargeId } = await RedeemCodeModel.findOne({
          purchaserEmail: email,
        });

        expect(productId.toString()).to.equal(giftProductId.toString());
        expect(email).to.equal(purchaserEmail);
        expect(chargeId).to.not.equal(null);

        await deleteCustomer(stripeCustomerId);
      });
      it('should track the gift purchase', async () => {
        const trackPaymentSpy = sinon.spy(trackPurchasedGift, 'trackPurchasedGift');
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        getUserStub.returns({ ...stubbedUser });

        const {
          products,
          email,
          card: { id: source },
        } = await simulateGetProductsWithAuth({ brand: 'sl' });
        const { _id: giftProductId, price } = products.find(({ brand }) => brand === 'sl');

        await simulatePostProductsWithAuth({
          giftProductId,
          source,
          purchaserEmail: email,
          purchaserName: 'Joel',
          recipientEmail: 'payment@test.com',
          recipientName: 'Test User',
          message: 'test',
        });

        const [
          { purchaserEmail, recipientEmail, amount },
          { brand },
        ] = trackPaymentSpy.firstCall.args;

        expect(trackPaymentSpy.callCount).to.equal(1);
        expect(purchaserEmail).to.equal(email);
        expect(recipientEmail).to.equal('payment@test.com');
        expect(amount).to.equal(price);
        expect(brand).to.equal('sl');

        await deleteCustomer(stripeCustomerId);
      });
    });
  });
}
