import sinon from 'sinon';
import { expect } from 'chai';
import ProductModel from '../../../src/models/gifts/ProductModel';
import { RedeemCodeModel } from '../../../src/models/gifts/RedeemCodeModel';
import { simulatePostGiftsVerify } from '../../helpers/simulateSubscription';
import * as stripe from '../../../src/external/stripe';

export const giftVerifyIntegrationTests = () => {
  // We need to stub this since we cannot simulate a successful Stripe Checkout without a major effort
  let getCheckoutSessionStub;

  const checkoutSessionId = 'cs_test_a1mKL4zLPXTCSWi0GmmTuLZJIxBZHxmxy5abkVaXzDcCj5E4BRPxtNPbAH';

  // Note this is not a full response with all fields provided.
  const checkoutSessionFixture = {
    id: 'cs_test_a1mKL4zLPXTCSWi0GmmTuLZJIxBZHxmxy5abkVaXzDcCj5E4BRPxtNPbAH',
    object: 'checkout.session',
    amount_subtotal: 2797,
    amount_total: 2797,
    line_items: {
      data: [
        {
          id: 'li_1KLQMoCUgh9bySt6asG8szJp',
          amount_subtotal: 2797,
          amount_total: 2797,
          currency: 'usd',
          description: 'Surfline Premium (USD)',
          price: {
            id: 'price_1KKSl6CUgh9bySt6ciI0Uv5U',
            currency: 'usd',
            metadata: {
              brand: 'sl',
              interval: 'month',
              intervalCount: '3',
            },
            nickname: 'Surfline 3 Month Gift Card',
            product: 'prod_KwnBPoyWnjYN2F',
            unit_amount: 2797,
            unit_amount_decimal: '2797',
          },
        },
      ],
    },
    payment_intent: {
      id: 'pi_3KLQMoCUgh9bySt61YBNfTZe',
      object: 'payment_intent',
      amount: 2797,
      currency: 'usd',
      metadata: {
        giftMessage: 'message',
        purchaserEmail: 'anemail@email.com',
        purchaserName: 'CJ',
        recipientEmail: 'anemail@email.com',
        recipientName: 'Beau',
      },
      status: 'succeeded',
    },
    status: 'expired',
  };

  describe('Gift Verify API Tests [Subscription API v2]', () => {
    beforeEach(async () => {
      getCheckoutSessionStub = sinon.stub(stripe, 'getCheckoutSession');
    });

    afterEach(async () => {
      await ProductModel.deleteMany({});
      await RedeemCodeModel.deleteMany({});
      getCheckoutSessionStub.restore();
    });

    describe('POST /gifts/verify API', () => {
      it('should return the a verification result if the payment succeeded', async () => {
        getCheckoutSessionStub.returns(checkoutSessionFixture);
        const resp = await simulatePostGiftsVerify(checkoutSessionId);

        const {
          body: {
            data: {
              amount,
              brand,
              currency,
              giftMessage,
              interval,
              intervalCount,
              nickname,
              purchaserEmail,
              purchaserName,
              recipientEmail,
              recipientName,
            },
            status,
          },
          statusCode,
        } = resp;

        expect(statusCode).to.equal(200);
        expect(status).to.equal('success');

        // price information
        const [lineItem] = checkoutSessionFixture.line_items.data;
        expect(brand).to.equal(lineItem.price.metadata.brand);
        expect(interval).to.equal(lineItem.price.metadata.interval);
        expect(intervalCount).to.equal(lineItem.price.metadata.intervalCount);
        expect(nickname).to.equal(lineItem.nickname);

        // gift purchase information
        const { payment_intent: paymentIntent } = checkoutSessionFixture;
        expect(amount).to.equal(paymentIntent.amount);
        expect(currency).to.equal(paymentIntent.currency);
        expect(giftMessage).to.equal(paymentIntent.metadata.giftMessage);
        expect(purchaserEmail).to.equal(paymentIntent.metadata.purchaserEmail);
        expect(purchaserName).to.equal(paymentIntent.metadata.purchaserName);
        expect(recipientEmail).to.equal(paymentIntent.metadata.recipientEmail);
        expect(recipientName).to.equal(paymentIntent.metadata.recipientName);
      });

      it('should require a valid checkout session id', async () => {
        const resp = await simulatePostGiftsVerify(null);
        expect(resp.statusCode).to.equal(400);
        expect(getCheckoutSessionStub).to.not.have.been.called();
      });

      it('should return a 400 error for a non-success status', async () => {
        getCheckoutSessionStub.returns({
          ...checkoutSessionFixture,
          payment_intent: {
            ...checkoutSessionFixture.payment_intent,
            status: 'canceled',
          },
        });
        const { statusCode } = await simulatePostGiftsVerify(checkoutSessionId);
        expect(statusCode).to.equal(400);
        expect(getCheckoutSessionStub).to.have.been.calledOnce();
      });
    });
  });
};

export default giftVerifyIntegrationTests;
