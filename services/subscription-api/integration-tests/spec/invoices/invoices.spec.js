import request from 'superagent';
import { expect } from 'chai';
import {
  createCustomer,
  updateCustomer,
  finalizeInvoice,
  getInvoices,
} from '../../../src/external/stripe';
import { deleteStripeSubscriptions } from '../../../src/models/subscriptions/stripe';
import { genTestToken } from '../../../src/external/stripeutils';
import { SubscriptionModel } from '../../../src/models/subscriptions/SubscriptionModel';

export default function invoiceIntegrationTests(theUserId) {
  const servicesURI = 'http://localhost:3000';
  const invoicePayAPI = `${servicesURI}/invoices/pay`;
  const stripeAPI = require('stripe')(process.env.STRIPE_SECRET_KEY);
  const stubbedUser = {
    _id: theUserId.toString(),
    firstName: 'Jim',
    lastName: 'Halpert',
    email: 'jimhalpert@example.com',
    usID: 783514,
  };

  const createTestStripeCustomer = async () => {
    const { email } = stubbedUser;
    const { id: tokenId } = await genTestToken('4000000000000341');
    return createCustomer({
      source: tokenId,
      email,
    });
  };

  const createTestStripeSubscription = stripeCustomerId =>
    new Promise((resolve, reject) => {
      const opts = {
        collection_method: 'send_invoice',
        days_until_due: 1,
      };
      stripeAPI.subscriptions.create(
        {
          customer: stripeCustomerId,
          plan: 'sl_annual_v2',
          ...opts,
        },
        (err, subscription) => {
          if (err) return reject(err);
          return resolve(subscription);
        },
      );
    });

  const seedSubscription = async (brand, type, subscriptionId, opts = {}) => {
    const subscription = await SubscriptionModel.create({
      type,
      subscriptionId,
      entitlement: `${brand}_premium`,
      planId: `${brand}_monthly`,
      user: theUserId,
      start: 1579201980,
      end: 1894821181,
      active: true,
      ...opts,
    });
    return subscription;
  };

  const simulatePayInvoice = () =>
    new Promise((resolve, reject) => {
      request
        .post(invoicePayAPI)
        .type('json')
        .set('x-auth-userid', theUserId)
        .end((err, result) => {
          if (err) reject(err);
          return resolve(result.body);
        });
    });

  describe('Invoice API Tests [Subscription API V2]', () => {
    afterEach(async () => {
      await deleteStripeSubscriptions(theUserId);
    });
    describe('Pay Invoice API', () => {
      it('should call stripe to pay invoices with failedPaymentAttempts > 0', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        const { id: subscriptionId } = await createTestStripeSubscription(stripeCustomerId);
        const { id: tokenId } = await genTestToken();

        const {
          data: [{ id: failedInvoiceId }],
        } = await getInvoices(stripeCustomerId);
        await finalizeInvoice(failedInvoiceId);

        updateCustomer(stripeCustomerId, { source: tokenId });
        await seedSubscription('sl', 'stripe', subscriptionId, {
          stripeCustomerId,
          failedPaymentAttempts: 2,
        });
        const invoicesPaid = await simulatePayInvoice();

        expect(invoicesPaid.length).to.equal(1);
        expect(invoicesPaid[0]).startsWith('in_');
        expect(invoicesPaid[0]).to.equal(failedInvoiceId);
      });

      it('should not call stripe if failedPaymentAttempts = 0', async () => {
        const { id: stripeCustomerId } = await createTestStripeCustomer();
        const { id: subscriptionId } = await createTestStripeSubscription(stripeCustomerId);
        const { id: tokenId } = await genTestToken();

        updateCustomer(stripeCustomerId, { source: tokenId });
        await seedSubscription('sl', 'stripe', subscriptionId, {
          stripeCustomerId,
          failedPaymentAttempts: 0,
        });
        const invoicesPaid = await simulatePayInvoice();

        expect(invoicesPaid.length).to.equal(0);
      });
    });
  });
}
