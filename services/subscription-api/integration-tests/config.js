/* eslint-disable import/no-extraneous-dependencies */

import fs from 'fs';
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import sinon from 'sinon';
import * as logger from '../src/common/logger';

console.log('Setting up the environment variables for the tests');

const envConfig = dotenv.parse(fs.readFileSync('.env.test'));

// eslint-disable-next-line
for (const envVar in envConfig) {
  if (envVar) {
    process.env[envVar] = envConfig[envVar];
  }
}

mongoose.Promise = global.Promise;

sinon.stub(logger, 'default').returns({
  trace: () => null,
  debug: () => null,
  info: () => null,
  error: () => null,
  warn: () => null,
});
