// eslint-disable-next-line
import request from 'superagent';
import { genTestToken } from '../../src/external/stripeutils';

const servicesURI = 'http://localhost:3000';
const subscriptionBillingAPI = `${servicesURI}/subscriptions/billing`;
const googleSubscriptionHandler = `${servicesURI}/subscriptions/google`;
const iTunesSubscriptionHandler = `${servicesURI}/subscriptions/itunes`;
const subscriptionPromotionAPI = `${servicesURI}/subscriptions/promotion`;
const subscriptionOfferAPI = `${servicesURI}/subscriptions/offers`;
const adminItunesPurchasesAPI = `${servicesURI}/admin/purchases/itunes`;
const adminGooglePurchasesAPI = `${servicesURI}/admin/purchases/google`;
const iTunesNotificationHandler = `${servicesURI}/integrations/itunes/handler`;
const googleNotificationHandler = `${servicesURI}/integrations/google/handler`;
const deprecatedITunesSubscriptionHandler = `${servicesURI}/iap/ios/verifyReceipt`;
const deprecatedGoogleSubscriptionHandler = `${servicesURI}/iap/google/verifyReceipt`;
const giftRedeemAPI = `${servicesURI}/gift/redeem`;
const giftsVerifyAPI = `${servicesURI}/gifts/verify`;
const stripeCheckoutApi = `${servicesURI}/subscriptions/billing/checkout`;
const giftCheckoutApi = `${servicesURI}/gift/checkout`;

/**
 * Retrieves a Stripe Managed Subscription
 *
 * @param {String} userId - the global id for all integration tests
 * @param {String} product - can be one of 'sl', 'fs', or 'bw'
 *
 * @returns {Object} the Stripe Subscription Document object.
 */
export const simulateGetBilling = (theUserId, product) => {
  return new Promise((resolve, reject) => {
    request
      .get(subscriptionBillingAPI)
      .set('x-auth-userid', theUserId)
      .set('accept', 'json')
      .query({ product })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * Creates a Stripe Managed Subscription
 *
 * This function should be used to create a subscription through
 * the Promotion, Billing, and Offer APS.
 *
 * @param {String} theUserId - the global id for all integration tests
 * @typedef {Object} subArgs - contains subscrition modifiers
 * @property {String} planId - the Stripe product plan
 * @property {String} source - the Stripe test credit card number
 *
 * @return {String} - A successfull response message
 */
export const simulatePostBilling = async (
  theUserId,
  { plan, source, trialEligible, coupon } = {
    plan: 'price_1KMDTZLwFGcBvXawjCayM4YC',
    source: '4242424242424242',
    trialEligible: true,
    coupon: null,
  },
) => {
  const { id: testToken } = await genTestToken(source);
  return new Promise((resolve, reject) => {
    request
      .post(subscriptionBillingAPI)
      .set('x-auth-userid', theUserId)
      .set('accept', 'json')
      .send({ planId: plan, source: testToken, trialEligible, coupon })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * Updates a Stripe Managed Subscription
 *
 * This function should be used to update a subscription through
 * the Stripe Billing integration tests.
 * @param {String} theUserId - the global id for all integration tests
 * @typedef {Object} subAttrs
 * @property {String} subscriptionId - the Stripe subscription id of the active subscription
 * @property {String} planId - the current or desired Stripe product plan
 * @property {Boolean} resume - an optional param that reactivates an active expiring subscription
 *
 * @returns {String} A successfull response message
 */
export const simulatePutBilling = (theUserId, { subscriptionId, planId, resume }) => {
  return new Promise((resolve, reject) => {
    request
      .put(subscriptionBillingAPI)
      .set('x-auth-userid', theUserId)
      .set('accept', 'json')
      .send({ subscriptionId, planId, resume })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * Cancel a Stripe Managed Subscription
 *
 * This function should be used to cancel an active or trialing subscription
 * through the Promotion, Billing, or Offer integration tests.
 *
 * @param {String} theUserId - the global id for all integration tests
 * @param {String} subscriptionId - the Stripe subscriptionId of the active subscrition
 *
 * @return {String} A successfull response message
 */
export const simulateDeleteBilling = (theUserId, subscriptionId) => {
  return new Promise((resolve, reject) => {
    request
      .delete(subscriptionBillingAPI)
      .set('x-auth-userid', theUserId)
      .set('accept', 'json')
      .send({ subscriptionId })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * This function would be used to verify a Tripe subscription through
 * the billing/verify endpoint
 *
 * @param {String} theUserId - the global id for all integration tests
 * @property {String} brand - brand
 *
 * @return {String} - A successfull response message
 */
export const simulatePostVerifyBilling = async (theUserId, brand, promotionId) => {
  return new Promise((resolve, reject) => {
    request
      .post(`${subscriptionBillingAPI}/verify`)
      .set('x-auth-userid', theUserId)
      .set('accept', 'json')
      .send({ brand, promotionId })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * Syncs user details to Stripe
 *
 * @param {String} theUserId - the global id for all integration tests
 * @property {String} firstName
 * @property {String} lastName
 * @property {String} email
 *
 * @return {String} - A successful response message
 */
export const simulateSyncUserDetails = async (theUserId, { firstName, lastName, email }) => {
  return new Promise((resolve, reject) => {
    request
      .put(`${subscriptionBillingAPI}/sync`)
      .set('x-auth-userid', theUserId)
      .set('accept', 'json')
      .send({ firstName, lastName, email })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * Retrieves a Promotion-Specific Subscription
 *
 * @param {String} userId - the global id for all integration tests
 * @param {String} promotionId - the unique promotion identifier
 *
 * @returns {Object} the Promotional Subscription Document object.
 */
export const simulateGetPromotionSubscription = (theUserId, promotionId) => {
  return new Promise((resolve, reject) => {
    request
      .get(subscriptionPromotionAPI)
      .set('x-auth-userid', theUserId)
      .set('accept', 'json')
      .query({ promotionId })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * Creates a Promotion-Specific Subscription
 *
 * This function should be used to create a subscription through
 * the Promotion API.
 *
 * @param {String} theUserId - the global id for all integration tests
 * @param {String} thePromotionId - the promotion id
 * @typedef {Object} subArgs - contains subscrition modifiers
 * @property {String} source - the Stripe test credit card number
 * @property {String} plan - the Stripe planId to subscribe to
 * @property {String} promotionId - the promotion id
 * @property {String|Null} promoCode - the redeem code associated with the subscription
 *
 * @return {String} - A successfull response message
 */
export const simulatePostPromotionSubscription = async (
  theUserId,
  thePromotionId,
  { plan, source, promotionId, promoCode } = {
    plan: 'price_1KMDTZLwFGcBvXawjCayM4YC',
    source: '4242424242424242',
    promotionId: thePromotionId,
    promoCode: null,
  },
) => {
  const { id: testToken } = await genTestToken(source);
  return new Promise((resolve, reject) => {
    request
      .post(subscriptionPromotionAPI)
      .set('x-auth-userid', theUserId)
      .set('accept', 'json')
      .send({ planId: plan, source: testToken, promotionId, promoCode })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * Retrieves a Offer-Specific Subscription
 *
 * @param {String} userId - the global id for all integration tests
 * @param {String} offerId - the unique promotion identifier
 *
 * @returns {Object} the Promotional Subscription Document object.
 */
export const simulateGetOfferSubscription = (theUserId, offerId) => {
  return new Promise((resolve, reject) => {
    request
      .get(subscriptionOfferAPI)
      .set('x-auth-userid', theUserId)
      .set('accept', 'json')
      .query({ promotionId: offerId })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * Creates a Offer-Specific Subscription
 *
 * This function should be used to create a subscription through
 * the Offer API.
 *
 * @param {String} theUserId - the global id for all integration tests
 * @param {String} theOfferId - the promotional offer id
 * @typedef {Object} subArgs - contains subscrition modifiers
 * @property {String} source - the Stripe test credit card number
 *
 * @return {String} - A successfull response message
 */
export const simulatePostOfferSubscription = async (
  theUserId,
  offerId,
  { source } = { source: null, cardId: null },
) => {
  const { id: testToken } = source ? await genTestToken(source) : { id: null };
  return new Promise((resolve, reject) => {
    request
      .post(subscriptionOfferAPI)
      .set('x-auth-userid', theUserId)
      .set('accept', 'json')
      .send({ source: testToken, promotionId: offerId })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 *
 */
export const simulateStripeCheckout = async (
  theUserId,
  { brand, country, currency, coupon, interval, promotionId, successUrl, cancelUrl },
) => {
  return new Promise((resolve, reject) => {
    request
      .post(stripeCheckoutApi)
      .set('x-auth-userid', theUserId)
      .set('accept', 'json')
      .send({
        brand,
        cancelUrl,
        country,
        coupon,
        currency,
        interval,
        promotionId,
        successUrl,
      })
      .end(async (err, resp) => {
        resolve(resp);
      });
  });
};

/**
 * Verify Google Purchase
 *
 * This function should be used to create or update the subscription through
 * google purchase validation.  This function mocks the response from the
 * Google Play store by stubbing the {getPurchaseData} request in the
 * {processReceipt} subscription-purchase model function.
 *
 * @param {String} theUserId - the global id for all integration tests
 * @typedef {Object} purchaseSignature - The Google Play purchase payload received from successful
 *  receipt verification.  See https://developers.google.com/android-publisher/api-ref/purchases/subscriptions
 *
 * @returns {Object} - the API response object
 */
export const verifyGooglePurchase = async (theUserId, purchaseSignature) =>
  new Promise((resolve, reject) => {
    request
      .post(googleSubscriptionHandler)
      .type('json')
      .send(JSON.stringify(purchaseSignature))
      .set('x-auth-userid', theUserId)
      .end((err, resp) => {
        if (err) return reject(resp);
        return resolve(resp.body);
      });
  });

/**
 * Verify Google Purchase to DEPRECATED v1 Google API
 *
 * This function is used to create or update the subscription through
 * DEPRECATED request to the v1 Google receipt validation API.
 *
 * @param {String} theUserId - the global id for all integration tests
 * @typedef {Object} purchaseSignature - The Google Play purchase payload received from successful
 *  receipt verification.  See https://developers.google.com/android-publisher/api-ref/purchases/subscriptions
 *
 * @returns {Object} - the API response object
 */
export const verifyDeprecatedV1GoogleReceipt = async (userId, receipt) =>
  new Promise((resolve, reject) => {
    request
      .post(deprecatedGoogleSubscriptionHandler)
      .type('json')
      .send(JSON.stringify(receipt))
      .set('x-auth-userid', userId)
      .end((err, resp) => {
        if (err) reject(err);
        resolve(resp.body);
      });
  });

/** Creates a Pending Google Subscription
 *
 * This function leverages the premium pending API for IAP subscription
 * requests, and creates a pending google subscription to prevent double
 * billing through the IAP platforms.
 *
 * @param {String} userId - the authenticated user
 * @param {String} brand - the product (bw/fs/sl) making the request
 * @param {Boolean} cancel - identifies if the pending subscription should be remeoved
 *
 * @reurns {Object} - the success message and the subscription if created
 */
export const simulateGooglePremiumPending = (theUserId, brand, cancel = false) =>
  new Promise((resolve, reject) => {
    request
      .post(`${googleSubscriptionHandler}/premium-pending`)
      .type('json')
      .send({ brand, cancel })
      .set('x-auth-userId', theUserId)
      .end((err, resp) => {
        if (err) return reject(resp);
        return resolve(resp.body);
      });
  });

/**
 * This function should be used to modify a Subscription document. For
 * example, if a test requires a subscription in an expiring state, the
 * test should create a new auto-renewing subscription and simulate the
 * webhook indicative of a user changing the auto-renewal status.  The
 * model functions should not be directly used to modify subscription
 * state.
 *
 * @param {String} notification - the desired Google notification (i.e. 'SUBSCRIPTION_RENEWAL').
 * @param {String} plan - the planId or productId associated defined on the Subscription.
 * @param {String} purchaseToken - the unique identifier used to fetch the Subscription.
 *
 * @returns {Object} - the API response object.
 *
 */
export const simulateGoogleNotification = async (notification, opts = {}) => {
  // eslint-disable-next-line import/no-dynamic-require
  const notificationStub = require(`../../src/__stubs__/google/${notification}`);
  const notificationObject = notificationStub.default(opts);
  const encodedNotification = Buffer.from(JSON.stringify(notificationObject)).toString('base64');
  const requestPayload = {
    message: {
      data: encodedNotification,
    },
  };
  return new Promise((resolve, reject) => {
    request
      .post(googleNotificationHandler)
      .send(requestPayload)
      .end((err, response) => {
        if (err) reject(response);
        resolve(response.body);
      });
  });
};

/**
 * Verify iTunes Receipt
 *
 * This function should be used to create or update the subscription through
 * iTunes receipt validation.  This function mocks the response from the iTunes
 * store by stubbing the {getPurchaseData} request in the {processReceipt}
 * subscription-purchase model function.
 *
 * @param {String} theUserId - the global id for all integration tests
 * @property {object} receipt - The google purchase token.  Use for v1 API compatibility.
 *
 * @returns {Object} - the API response object
 */
export const verfifyItunesReceipt = async (theUserId, receipt) =>
  new Promise((resolve, reject) => {
    request
      .post(iTunesSubscriptionHandler)
      .type('json')
      .send({ receipt })
      .set('x-auth-userid', theUserId)
      .end((err, res) => {
        if (err) return reject(res);
        return resolve(res.body);
      });
  });

/**
 * Verify iTunes Receipt to DEPRECATED v1 API
 *
 * This function is used to create or update the subscription through
 * DEPRECATED request to the v1 iTunes receipt validation API.
 *
 * @param {String} theUserId - the global id for all integration tests
 * @property {object} receipt - The google purchase token.  Use for v1 API compatibility.
 *
 * @returns {Object} - the API response object
 */
export const verifyDeprecatedV1ItunesReceipt = async (theUserId, receipt) =>
  new Promise((resolve, reject) => {
    request
      .post(deprecatedITunesSubscriptionHandler)
      .type('json')
      .send({ receipt })
      .set('x-auth-userid', theUserId)
      .end((err, res) => {
        if (err) return reject(res);
        return resolve(res.body);
      });
  });

/** Creates a Pending iTunes Subscription
 *
 * This function leverages the premium pending API for IAP subscription
 * requests, and creates a pending google subscription to prevent double
 * billing through the IAP platforms.
 *
 * @param {String} userId - the authenticated user
 * @param {String} brand - the product (bw/fs/sl) making the request
 * @param {Boolean} cancel - identifies if the pending subscription should be remeoved
 *
 * @reurns {Object} - the success message and the subscription if created
 */
export const simulateITunesPremiumPending = (theUserId, brand, cancel = false) =>
  new Promise((resolve, reject) => {
    request
      .post(`${iTunesSubscriptionHandler}/premium-pending`)
      .type('json')
      .send({ brand, cancel })
      .set('x-auth-userId', theUserId)
      .end((err, resp) => {
        if (err) return reject(resp);
        return resolve(resp.body);
      });
  });

/**
 * This function should be used to modify a Subscription document. For
 * example, if a test requires a subscription in an expiring state, the
 * test should create a new auto-renewing subscription and simulate the
 * webhook indicative of a user changing the auto-renewal status.  The
 * model functions should not be directly used to modify subscription
 * state.
 *
 * @param {String} notification - the desired iTunes notification (i.e. 'CANCEL').
 * @param {String} plan - the planId or productId associated defined on the Subscription.
 * @param {String} transactionId - the unique identifier used to fetch the Subscription.
 *
 * @returns {Object} - the API response object.
 *
 */
export const simulateItunesNotification = async (notification, opts) => {
  // eslint-disable-next-line max-len
  const notificationStub = require(`../../src/__stubs__/itunes/${notification}`);
  const notificationObject = notificationStub.default(opts);

  return new Promise((resolve, reject) => {
    request
      .post(iTunesNotificationHandler)
      .send(notificationObject)
      .end((err, response) => {
        if (err) reject(err);
        resolve(response.body);
      });
  });
};

/**
 * Retrieves iTunes Managed Subscription-Purchase
 *
 * @param {String} userId - the global id for all integration tests
 *
 * @returns {Object} the iTunes Subscription-Pruchase Document object.
 */
export const simulateAdminGetItunesReceipts = userId => {
  return new Promise((resolve, reject) => {
    request
      .get(adminItunesPurchasesAPI)
      .set('accept', 'json')
      .query({ userId: userId.toString() })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * Updates a iTunes Managed Subscription-Purchase
 *
 * @param {String} purchaseId - the itunes purchase identifier
 *
 * @returns {Object} the iTunes Subscription-Pruchase Document object.
 */
export const simulateAdminPostItunesReceipts = purchaseId => {
  return new Promise((resolve, reject) => {
    request
      .post(adminItunesPurchasesAPI)
      .set('accept', 'json')
      .send({ purchaseId: purchaseId.toString() })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * Retrieves Google-Play Managed Subscription-Purchase
 *
 * @param {String} userId - the global id for all integration tests
 *
 * @returns {Object} the Google-Play Subscription-Pruchase Document object.
 */
export const simulateAdminGetGooglePurchases = userId => {
  return new Promise((resolve, reject) => {
    request
      .get(adminGooglePurchasesAPI)
      .set('accept', 'json')
      .query({ userId: userId.toString() })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * Updates a Google-Play Managed Subscription-Purchase
 *
 * @param {String} purchaseId - the google purchase idendifier
 *
 * @returns {Object} the Google-Play Subscription-Pruchase Document object.
 */
export const simulateAdminPostGooglePurchase = purchaseId => {
  return new Promise((resolve, reject) => {
    request
      .post(adminGooglePurchasesAPI)
      .set('accept', 'json')
      .send({ purchaseId: purchaseId.toString() })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * Cancels, Refunds, or Revokes a Google-Play Managed Subscription-Purchase
 *
 * @param {String} purchaseId - the google purchase idendifier
 * @param {String} command - the method for which to process the purchase
 *
 * @returns {Object} the Google-Play Subscription-Pruchase Document object.
 */
export const simulateAdminPutGooglePurchase = (purchaseId, command) => {
  return new Promise((resolve, reject) => {
    request
      .put(adminGooglePurchasesAPI)
      .set('accept', 'json')
      .send({ purchaseId: purchaseId.toString(), command })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 * Defers the billing of a Google-Play Managed Subscription-Purchase
 *
 * @param {String} purchaseId - the google purchase idendifier
 *
 * @returns {Object} the Google-Play Subscription-Pruchase Document object.
 */
export const simulateAdminDeferGooglePurchase = (purchaseId, deferMillis) => {
  return new Promise((resolve, reject) => {
    request
      .post(`${adminGooglePurchasesAPI}/defer`)
      .set('accept', 'json')
      .send({ purchaseId: purchaseId.toString(), deferMillis })
      .end((err, resp) => {
        if (err) {
          reject(resp);
        }
        resolve(resp.body);
      });
  });
};

/**
 *
 *
 * @param {*} [code=null]
 * @return {*}
 */
export const simulateGetRedeem = (theUserId, code = null) => {
  return new Promise((resolve, reject) => {
    request
      .get(giftRedeemAPI)
      .set('accept', 'json')
      .set('x-auth-userid', theUserId)
      .query({ brand: 'sl', code })
      .end(async (err, result) => {
        if (err) {
          reject(result);
        }
        resolve(result.body);
      });
  });
};

/**
 *
 *
 * @param {*} [code=null]
 * @return {*}
 */
export const simulatePostRedeem = (theUserId, code = null) => {
  return new Promise((resolve, reject) => {
    request
      .post(giftRedeemAPI)
      .set('accept', 'json')
      .set('x-auth-userid', theUserId)
      .send({ brand: 'sl', code })
      .end(async (err, result) => {
        if (err) {
          reject(result);
        }
        resolve(result.body);
      });
  });
};

/**
 * Simulate a /gifts/verify POST request
 */
export const simulatePostGiftsVerify = checkoutSessionId => {
  return new Promise(resolve => {
    request
      .post(giftsVerifyAPI)
      .set('accept', 'json')
      .send({ checkoutSessionId })
      .end(async (_, result) => {
        resolve(result);
      });
  });
};

/**
 * Gift Checkout API Requestion Handler for tests
 */
export const simulateGiftCheckout = async (theUserId, giftParams) => {
  return new Promise((resolve, reject) => {
    request
      .post(giftCheckoutApi)
      .set('x-auth-userid', theUserId)
      .set('accept', 'json')
      .send({ ...giftParams })
      .end(async (err, resp) => {
        if (err) reject(err);
        resolve(resp);
      });
  });
};
