import fetch from 'isomorphic-fetch';

export class SubscriptionAPI {
  constructor(hostname, userId) {
    this.hostname = hostname;
    this.userId = userId;
  }

  parseRequest = (resp) => new Promise(async (resolve, reject) => {
    const contentType = resp.headers.get('Content-Type');
    const body = (contentType.indexOf('json') > -1) ?
      await resp.json() : await resp.text();

    if (resp.status > 200) {
      return reject(body);
    }
    return resolve(body);
  });

  async fetchPath(path, opts) {
    const formattedPath = path.startsWith('/') ? path.substr(1, path.length) : path;
    return fetch(`${this.hostname}/${formattedPath}`, Object.assign({
      headers: {
        accept: 'application/json',
        'content-type': 'application/json',
        'x-auth-userid': `${this.userId}`
      }
    }, opts)).then(this.parseRequest);
  }

  getSubscription = () => this.fetchPath('subscriptions');

  deleteSubscription = (subscriptionId) => this.fetchPath(`subscriptions/${subscriptionId}`, {
    method: 'DELETE'
  });

  updateSubscription = (details) => this.fetchPath('subscriptions/defaultcard', {
    method: 'PUT',
    body: JSON.stringify(details)
  });

  syncUserDetails = (details) => this.fetchPath('subscriptions/syncUserDetails', {
    method: 'PUT',
    body: JSON.stringify(details)
  });

  deleteCard = (cardId) => this.fetchPath(`subscriptions/cards/${cardId}`, {
    method: 'DELETE'
  });

  addCard = (source) => this.fetchPath('subscriptions/cards', {
    method: 'POST',
    body: JSON.stringify({ source })
  });

  updateCard = (cardId, opts) => this.fetchPath(`subscriptions/cards/${cardId}`, {
    method: 'PUT',
    body: JSON.stringify({ opts })
  });

  setDefaultCard = (cardId) => this.updateSubscription({
    defaultStripeSource: cardId
  });

  invoicesPay = () => this.fetchPath('subscriptions/invoices/pay', {
    method: 'POST'
  });

  createSubscription = (plan, source, coupon) => this.fetchPath('subscriptions', {
    method: 'POST',
    body: JSON.stringify({ source, plan, coupon })
  });

  updatePlan = ({ planId, subscription }) => this.fetchPath('subscriptions', {
    method: 'PUT',
    body: JSON.stringify({ planId, subscription })
  })

  upgrade = ({ plan, coupon, freeTrial, source, cardId, promotionId, brand }) =>
    this.fetchPath('subscriptions/upgrade', {
      method: 'POST',
      body: JSON.stringify({
        plan,
        coupon,
        freeTrial,
        source,
        cardId,
        promotionId,
        brand
      })
    });

  reactivate = ({ subscriptionId, planId }) => this.fetchPath('subscriptions/reactivate', {
    method: 'POST',
    body: JSON.stringify({ subscriptionId, planId })
  });

  validateCoupon = ({ coupon, brand }) =>
    this.fetchPath(`subscriptions/coupons?coupon=${coupon}&brand=${brand}`);

  getPlans = ({ brand }) => this.fetchPath(`subscriptions/plans?brand=${brand}`);

  getOffer = ({
    brand,
    promotionId
  }) => this.fetchPath(`subscriptions/offer?brand=${brand}&promotionId=${promotionId}`);

  acceptOffer = ({
    brand,
    promotionId
  }) => this.fetchPath(`subscriptions/offer?promotionId=${promotionId}`, {
    method: 'POST',
    body: JSON.stringify({
      brand
    })
  });
}
