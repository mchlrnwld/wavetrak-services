/* eslint-disable max-len */

import moment from 'moment-timezone';

/**
 * @description Helper function for formatting moments into the proper format
 *
 * @param {Moment} aMoment - the moment
 * @param {string} timeZone - the timeZone (either 'GMT', 'PST', or null for milliseconds)
 */
export const formatMoment = (aMoment, timeZone = null) => {
  if (timeZone === 'GMT') {
    return aMoment.format('YYYY-MM-DD HH:mm:ss [Etc/GMT]');
  }
  if (timeZone === 'PST') {
    return aMoment.format('YYYY-MM-DD HH:mm:ss [America/Los_Angeles]');
  }
  return aMoment.valueOf();
};

/**
 * @description Helper function for adjusting moments to timezones
 * @param {*} moment - the moment
 * @param {string} timeZone - Timezone to use (either 'GMT', 'PST', or null)
 *
 * @returns {Array} - an array of parameters to be passed into the format function
 */
export const adjustMomentTimeZone = (aMoment, timeZone = null) => {
  if (timeZone === 'GMT') {
    return [moment.tz(aMoment, 'GMT'), 'GMT'];
  }
  if (timeZone === 'PST') {
    return [moment.tz(aMoment, 'America/Los_Angeles'), 'PST'];
  }
  return [aMoment];
};

/**
 * @description - Function used to create a moment in time. Pass in either add/sub as
 * true to create a moment in the future or past. If both are false, it will create a
 * present moment
 *
 * @param {object} params - Object with either add or subtract as true/false
 * @param {number} params.add - Number of days you want to add
 * @param {number} params.sub - Number of days you want to subtract
 *
 * @returns {moment} aMoment
 */
export const createMoment = ({ add, sub } = { add: 0, sub: 0 }) => {
  // Perform some basic validation just in case.
  if (add > 0 && sub > 0) {
    throw new Error("You can't add AND subtract days...");
  }

  if (add > 0) {
    return moment().add(add, 'days');
  }
  if (sub > 0) {
    return moment().subtract(sub, 'days');
  }
  return moment();
};

/**
 * @description - Wrapper function for the functional approach to creating a date
 * @param {object} params - The parameters to pass, this uses an object destructuring for assigning default parameters
 * @param {number} params.add - Number of days you want to add
 * @param {number} params.sub - Number of days you want to subtract
 * @param {string} params.timeZone - One of: 'GMT', 'PST', or null (which will returns millisecond time)
 */
export const createSubscriptionDate = (
  { add, sub, timeZone } = { add: 0, sub: 0, timeZone: null },
) => formatMoment(...adjustMomentTimeZone(createMoment({ sub, add }), timeZone));

export const appleReceipt =
  'ewoJInNpZ25hdHVyZSIgPSAiQXBNVUJDODZBbHpOaWtWNVl0clpBTWlKUWJLOEVkZVhrNjNrV0JBWHpsQzhkWEd1anE0N1puSVlLb0ZFMW9OL0ZTOGNYbEZmcDlZWHQ5aU1CZEwyNTBsUlJtaU5HYnloaXRyeVlWQVFvcmkzMlc5YVIwVDhML2FZVkJkZlcrT3kvUXlQWkVtb05LeGhudDJXTlNVRG9VaFo4Wis0cFA3MHBlNWtVUWxiZElWaEFBQURWekNDQTFNd2dnSTdvQU1DQVFJQ0NHVVVrVTNaV0FTMU1BMEdDU3FHU0liM0RRRUJCUVVBTUg4eEN6QUpCZ05WQkFZVEFsVlRNUk13RVFZRFZRUUtEQXBCY0hCc1pTQkpibU11TVNZd0pBWURWUVFMREIxQmNIQnNaU0JEWlhKMGFXWnBZMkYwYVc5dUlFRjFkR2h2Y21sMGVURXpNREVHQTFVRUF3d3FRWEJ3YkdVZ2FWUjFibVZ6SUZOMGIzSmxJRU5sY25ScFptbGpZWFJwYjI0Z1FYVjBhRzl5YVhSNU1CNFhEVEE1TURZeE5USXlNRFUxTmxvWERURTBNRFl4TkRJeU1EVTFObG93WkRFak1DRUdBMVVFQXd3YVVIVnlZMmhoYzJWU1pXTmxhWEIwUTJWeWRHbG1hV05oZEdVeEd6QVpCZ05WQkFzTUVrRndjR3hsSUdsVWRXNWxjeUJUZEc5eVpURVRNQkVHQTFVRUNnd0tRWEJ3YkdVZ1NXNWpMakVMTUFrR0ExVUVCaE1DVlZNd2daOHdEUVlKS29aSWh2Y05BUUVCQlFBRGdZMEFNSUdKQW9HQkFNclJqRjJjdDRJclNkaVRDaGFJMGc4cHd2L2NtSHM4cC9Sd1YvcnQvOTFYS1ZoTmw0WElCaW1LalFRTmZnSHNEczZ5anUrK0RyS0pFN3VLc3BoTWRkS1lmRkU1ckdYc0FkQkVqQndSSXhleFRldngzSExFRkdBdDFtb0t4NTA5ZGh4dGlJZERnSnYyWWFWczQ5QjB1SnZOZHk2U01xTk5MSHNETHpEUzlvWkhBZ01CQUFHamNqQndNQXdHQTFVZEV3RUIvd1FDTUFBd0h3WURWUjBqQkJnd0ZvQVVOaDNvNHAyQzBnRVl0VEpyRHRkREM1RllRem93RGdZRFZSMFBBUUgvQkFRREFnZUFNQjBHQTFVZERnUVdCQlNwZzRQeUdVakZQaEpYQ0JUTXphTittVjhrOVRBUUJnb3Foa2lHOTJOa0JnVUJCQUlGQURBTkJna3Foa2lHOXcwQkFRVUZBQU9DQVFFQUVhU2JQanRtTjRDL0lCM1FFcEszMlJ4YWNDRFhkVlhBZVZSZVM1RmFaeGMrdDg4cFFQOTNCaUF4dmRXLzNlVFNNR1k1RmJlQVlMM2V0cVA1Z204d3JGb2pYMGlreVZSU3RRKy9BUTBLRWp0cUIwN2tMczlRVWU4Y3pSOFVHZmRNMUV1bVYvVWd2RGQ0TndOWXhMUU1nNFdUUWZna1FRVnk4R1had1ZIZ2JFL1VDNlk3MDUzcEdYQms1MU5QTTN3b3hoZDNnU1JMdlhqK2xvSHNTdGNURXFlOXBCRHBtRzUrc2s0dHcrR0szR01lRU41LytlMVFUOW5wL0tsMW5qK2FCdzdDMHhzeTBiRm5hQWQxY1NTNnhkb3J5L0NVdk02Z3RLc21uT09kcVRlc2JwMGJzOHNuNldxczBDOWRnY3hSSHVPTVoydG04bnBMVW03YXJnT1N6UT09IjsKCSJwdXJjaGFzZS1pbmZvIiA9ICJld29KSW05eWFXZHBibUZzTFhCMWNtTm9ZWE5sTFdSaGRHVXRjSE4wSWlBOUlDSXlNREV5TFRBMExUTXdJREE0T2pBMU9qVTFJRUZ0WlhKcFkyRXZURzl6WDBGdVoyVnNaWE1pT3dvSkltOXlhV2RwYm1Gc0xYUnlZVzV6WVdOMGFXOXVMV2xrSWlBOUlDSXhNREF3TURBd01EUTJNVGM0T0RFM0lqc0tDU0ppZG5KeklpQTlJQ0l5TURFeU1EUXlOeUk3Q2draWRISmhibk5oWTNScGIyNHRhV1FpSUQwZ0lqRXdNREF3TURBd05EWXhOemc0TVRjaU93b0pJbkYxWVc1MGFYUjVJaUE5SUNJeElqc0tDU0p2Y21sbmFXNWhiQzF3ZFhKamFHRnpaUzFrWVhSbExXMXpJaUE5SUNJeE16TTFOems0TXpVMU9EWTRJanNLQ1NKd2NtOWtkV04wTFdsa0lpQTlJQ0pqYjIwdWJXbHVaRzF2WW1Gd2NDNWtiM2R1Ykc5aFpDSTdDZ2tpYVhSbGJTMXBaQ0lnUFNBaU5USXhNVEk1T0RFeUlqc0tDU0ppYVdRaUlEMGdJbU52YlM1dGFXNWtiVzlpWVhCd0xrMXBibVJOYjJJaU93b0pJbkIxY21Ob1lYTmxMV1JoZEdVdGJYTWlJRDBnSWpFek16VTNPVGd6TlRVNE5qZ2lPd29KSW5CMWNtTm9ZWE5sTFdSaGRHVWlJRDBnSWpJd01USXRNRFF0TXpBZ01UVTZNRFU2TlRVZ1JYUmpMMGROVkNJN0Nna2ljSFZ5WTJoaGMyVXRaR0YwWlMxd2MzUWlJRDBnSWpJd01USXRNRFF0TXpBZ01EZzZNRFU2TlRVZ1FXMWxjbWxqWVM5TWIzTmZRVzVuWld4bGN5STdDZ2tpYjNKcFoybHVZV3d0Y0hWeVkyaGhjMlV0WkdGMFpTSWdQU0FpTWpBeE1pMHdOQzB6TUNBeE5Ub3dOVG8xTlNCRmRHTXZSMDFVSWpzS2ZRPT0iOwoJImVudmlyb25tZW50IiA9ICJTYW5kYm94IjsKCSJwb2QiID0gIjEwMCI7Cgkic2lnbmluZy1zdGF0dXMiID0gIjAiOwp9';

export const surflineAppleReceipt =
  'MIIkrgYJKoZIhvcNAQcCoIIknzCCJJsCAQExCzAJBgUrDgMCGgUAMIIUTwYJKoZIhvcNAQcBoIIUQASCFDwxghQ4MAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgEDAgEBBAMMATAwCwIBCwIBAQQDAgEAMAsCAQ4CAQEEAwIBTzALAgEPAgEBBAMCAQAwCwIBEAIBAQQDAgEAMAsCARkCAQEEAwIBAzAMAgEKAgEBBAQWAjQrMA0CAQ0CAQEEBQIDAWBZMA0CARMCAQEEBQwDMS4wMA4CAQkCAQEEBgIEUDI0NzAYAgEEAgECBBC6ts2yviea9UOwee/O9G+QMBsCAQACAQEEEwwRUHJvZHVjdGlvblNhbmRib3gwHAIBBQIBAQQUYFLh8ksmgq/0EM7o1kcEa2o0lckwHgIBAgIBAQQWDBRjb20uc3VyZmxpbmUuc3VyZmFwcDAeAgEMAgEBBBYWFDIwMTYtMDYtMjdUMjE6NDM6NDdaMB4CARICAQEEFhYUMjAxMy0wOC0wMVQwNzowMDowMFowRAIBBgIBAQQ8SKv188VdTk/7sCExdSOjjr34KEXKcJMIB40wmJSr2U6ieT33L19GxauWkF0rJyMFxW48J0TdI4fb2BwtMEoCAQcCAQEEQoMLrDYhRBH1xl28JYjpl1nm30XIMJSxhc6s0fVZTsEdF6xB4VnWtKD6/5BkdZU1HR+O/OFTr7fFt8yhn+yeNYEczDCCAYECARECAQEEggF3MYIBczALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6mnazBMBsCAganAgEBBBIMEDEwMDAwMDAxNzk1Mzc2NzQwGwICBqkCAQEEEgwQMTAwMDAwMDE3OTUzNzY3NDAfAgIGqAIBAQQWFhQyMDE1LTExLTEwVDE3OjUwOjI3WjAfAgIGqgIBAQQWFhQyMDE1LTExLTEwVDE3OjUwOjI4WjAfAgIGrAIBAQQWFhQyMDE1LTExLTEwVDE4OjUwOjI3WjAtAgIGpgIBAQQkDCJzdXJmbGluZS5pb3NhcHAuMXllYXIuc3Vic2NyaXB0aW9uMIIBgQIBEQIBAQSCAXcxggFzMAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMBICAgavAgEBBAkCBwONfqadrMIwGwICBqcCAQEEEgwQMTAwMDAwMDE3OTU0MjY2NTAbAgIGqQIBAQQSDBAxMDAwMDAwMTc5NTM3Njc0MB8CAgaoAgEBBBYWFDIwMTUtMTEtMTBUMTg6NTA6MjdaMB8CAgaqAgEBBBYWFDIwMTUtMTEtMTBUMTg6Mzg6MjhaMB8CAgasAgEBBBYWFDIwMTUtMTEtMTBUMTk6NTA6MjdaMC0CAgamAgEBBCQMInN1cmZsaW5lLmlvc2FwcC4xeWVhci5zdWJzY3JpcHRpb24wggGBAgERAgEBBIIBdzGCAXMwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwEgICBq8CAQEECQIHA41+pp2uUDAbAgIGpwIBAQQSDBAxMDAwMDAwMTc5NTUxMTAwMBsCAgapAgEBBBIMEDEwMDAwMDAxNzk1Mzc2NzQwHwICBqgCAQEEFhYUMjAxNS0xMS0xMFQxOTo1MDoyN1owHwICBqoCAQEEFhYUMjAxNS0xMS0xMFQxOTozODozMlowHwICBqwCAQEEFhYUMjAxNS0xMS0xMFQyMDo1MDoyN1owLQICBqYCAQEEJAwic3VyZmxpbmUuaW9zYXBwLjF5ZWFyLnN1YnNjcmlwdGlvbjCCAYECARECAQEEggF3MYIBczALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6mnbCTMBsCAganAgEBBBIMEDEwMDAwMDAxNzk1NTU4MTkwGwICBqkCAQEEEgwQMTAwMDAwMDE3OTUzNzY3NDAfAgIGqAIBAQQWFhQyMDE1LTExLTEwVDIwOjUwOjI3WjAfAgIGqgIBAQQWFhQyMDE1LTExLTEwVDIwOjM4OjMyWjAfAgIGrAIBAQQWFhQyMDE1LTExLTEwVDIxOjUwOjI3WjAtAgIGpgIBAQQkDCJzdXJmbGluZS5pb3NhcHAuMXllYXIuc3Vic2NyaXB0aW9uMIIBgQIBEQIBAQSCAXcxggFzMAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMBICAgavAgEBBAkCBwONfqadsZIwGwICBqcCAQEEEgwQMTAwMDAwMDE3OTU2MDQ0OTAbAgIGqQIBAQQSDBAxMDAwMDAwMTc5NTM3Njc0MB8CAgaoAgEBBBYWFDIwMTUtMTEtMTBUMjE6NTA6MjdaMB8CAgaqAgEBBBYWFDIwMTUtMTEtMTBUMjE6Mzg6NDJaMB8CAgasAgEBBBYWFDIwMTUtMTEtMTBUMjI6NTA6MjdaMC0CAgamAgEBBCQMInN1cmZsaW5lLmlvc2FwcC4xeWVhci5zdWJzY3JpcHRpb24wggGBAgERAgEBBIIBdzGCAXMwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwEgICBq8CAQEECQIHA41+pp2y3DAbAgIGpwIBAQQSDBAxMDAwMDAwMTc5NTY0OTg5MBsCAgapAgEBBBIMEDEwMDAwMDAxNzk1Mzc2NzQwHwICBqgCAQEEFhYUMjAxNS0xMS0xMFQyMjo1MDoyN1owHwICBqoCAQEEFhYUMjAxNS0xMS0xMFQyMjozODozM1owHwICBqwCAQEEFhYUMjAxNS0xMS0xMFQyMzo1MDoyN1owLQICBqYCAQEEJAwic3VyZmxpbmUuaW9zYXBwLjF5ZWFyLnN1YnNjcmlwdGlvbjCCAYICARECAQEEggF4MYIBdDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6mnbT7MBsCAganAgEBBBIMEDEwMDAwMDAyMjAwNDAyNDEwGwICBqkCAQEEEgwQMTAwMDAwMDE3OTUzNzY3NDAfAgIGqAIBAQQWFhQyMDE2LTA2LTI3VDIxOjE4OjI5WjAfAgIGqgIBAQQWFhQyMDE2LTA2LTI3VDIxOjE4OjMwWjAfAgIGrAIBAQQWFhQyMDE2LTA2LTI3VDIxOjIzOjI5WjAuAgIGpgIBAQQlDCNzdXJmbGluZS5pb3NhcHAuMW1vbnRoLnN1YnNjcmlwdGlvbjCCAYICARECAQEEggF4MYIBdDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6musTVMBsCAganAgEBBBIMEDEwMDAwMDAyMjAwNDA1ODAwGwICBqkCAQEEEgwQMTAwMDAwMDE3OTUzNzY3NDAfAgIGqAIBAQQWFhQyMDE2LTA2LTI3VDIxOjIzOjI5WjAfAgIGqgIBAQQWFhQyMDE2LTA2LTI3VDIxOjIyOjQyWjAfAgIGrAIBAQQWFhQyMDE2LTA2LTI3VDIxOjI4OjI5WjAuAgIGpgIBAQQlDCNzdXJmbGluZS5pb3NhcHAuMW1vbnRoLnN1YnNjcmlwdGlvbjCCAYICARECAQEEggF4MYIBdDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6musTlMBsCAganAgEBBBIMEDEwMDAwMDAyMjAwNDEwMzUwGwICBqkCAQEEEgwQMTAwMDAwMDE3OTUzNzY3NDAfAgIGqAIBAQQWFhQyMDE2LTA2LTI3VDIxOjI4OjI5WjAfAgIGqgIBAQQWFhQyMDE2LTA2LTI3VDIxOjI2OjM1WjAfAgIGrAIBAQQWFhQyMDE2LTA2LTI3VDIxOjMzOjI5WjAuAgIGpgIBAQQlDCNzdXJmbGluZS5pb3NhcHAuMW1vbnRoLnN1YnNjcmlwdGlvbjCCAYICARECAQEEggF4MYIBdDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6musUHMBsCAganAgEBBBIMEDEwMDAwMDAyMjAwNDEzNTAwGwICBqkCAQEEEgwQMTAwMDAwMDE3OTUzNzY3NDAfAgIGqAIBAQQWFhQyMDE2LTA2LTI3VDIxOjMzOjI5WjAfAgIGqgIBAQQWFhQyMDE2LTA2LTI3VDIxOjMxOjQxWjAfAgIGrAIBAQQWFhQyMDE2LTA2LTI3VDIxOjM4OjI5WjAuAgIGpgIBAQQlDCNzdXJmbGluZS5pb3NhcHAuMW1vbnRoLnN1YnNjcmlwdGlvbjCCAYICARECAQEEggF4MYIBdDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6musUfMBsCAganAgEBBBIMEDEwMDAwMDAyMjAwNDIyMzUwGwICBqkCAQEEEgwQMTAwMDAwMDE3OTUzNzY3NDAfAgIGqAIBAQQWFhQyMDE2LTA2LTI3VDIxOjM4OjI5WjAfAgIGqgIBAQQWFhQyMDE2LTA2LTI3VDIxOjM2OjQzWjAfAgIGrAIBAQQWFhQyMDE2LTA2LTI3VDIxOjQzOjI5WjAuAgIGpgIBAQQlDCNzdXJmbGluZS5pb3NhcHAuMW1vbnRoLnN1YnNjcmlwdGlvbjCCAYICARECAQEEggF4MYIBdDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6musU5MBsCAganAgEBBBIMEDEwMDAwMDAyMjAwNDIzNzYwGwICBqkCAQEEEgwQMTAwMDAwMDE3OTUzNzY3NDAfAgIGqAIBAQQWFhQyMDE2LTA2LTI3VDIxOjQzOjI5WjAfAgIGqgIBAQQWFhQyMDE2LTA2LTI3VDIxOjQxOjM2WjAfAgIGrAIBAQQWFhQyMDE2LTA2LTI3VDIxOjQ4OjI5WjAuAgIGpgIBAQQlDCNzdXJmbGluZS5pb3NhcHAuMW1vbnRoLnN1YnNjcmlwdGlvbqCCDmUwggV8MIIEZKADAgECAggO61eH554JjTANBgkqhkiG9w0BAQUFADCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0xNTExMTMwMjE1MDlaFw0yMzAyMDcyMTQ4NDdaMIGJMTcwNQYDVQQDDC5NYWMgQXBwIFN0b3JlIGFuZCBpVHVuZXMgU3RvcmUgUmVjZWlwdCBTaWduaW5nMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQClz4H9JaKBW9aH7SPaMxyO4iPApcQmyz3Gn+xKDVWG/6QC15fKOVRtfX+yVBidxCxScY5ke4LOibpJ1gjltIhxzz9bRi7GxB24A6lYogQ+IXjV27fQjhKNg0xbKmg3k8LyvR7E0qEMSlhSqxLj7d0fmBWQNS3CzBLKjUiB91h4VGvojDE2H0oGDEdU8zeQuLKSiX1fpIVK4cCc4Lqku4KXY/Qrk8H9Pm/KwfU8qY9SGsAlCnYO3v6Z/v/Ca/VbXqxzUUkIVonMQ5DMjoEC0KCXtlyxoWlph5AQaCYmObgdEHOwCl3Fc9DfdjvYLdmIHuPsB8/ijtDT+iZVge/iA0kjAgMBAAGjggHXMIIB0zA/BggrBgEFBQcBAQQzMDEwLwYIKwYBBQUHMAGGI2h0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDMtd3dkcjA0MB0GA1UdDgQWBBSRpJz8xHa3n6CK9E31jzZd7SsEhTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3MIIBHgYDVR0gBIIBFTCCAREwggENBgoqhkiG92NkBQYBMIH+MIHDBggrBgEFBQcCAjCBtgyBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMDYGCCsGAQUFBwIBFipodHRwOi8vd3d3LmFwcGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhvcml0eS8wDgYDVR0PAQH/BAQDAgeAMBAGCiqGSIb3Y2QGCwEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQANphvTLj3jWysHbkKWbNPojEMwgl/gXNGNvr0PvRr8JZLbjIXDgFnf4+LXLgUUrA3btrj+/DUufMutF2uOfx/kd7mxZ5W0E16mGYZ2+FogledjjA9z/Ojtxh+umfhlSFyg4Cg6wBA3LbmgBDkfc7nIBf3y3n8aKipuKwH8oCBc2et9J6Yz+PWY4L5E27FMZ/xuCk/J4gao0pfzp45rUaJahHVl0RYEYuPBX/UIqc9o2ZIAycGMs/iNAGS6WGDAfK+PdcppuVsq1h1obphC9UynNxmbzDscehlD86Ntv0hgBgw2kivs3hi1EdotI9CO/KBpnBcbnoB7OUdFMGEvxxOoMIIEIjCCAwqgAwIBAgIIAd68xDltoBAwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTEzMDIwNzIxNDg0N1oXDTIzMDIwNzIxNDg0N1owgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDKOFSmy1aqyCQ5SOmM7uxfuH8mkbw0U3rOfGOAYXdkXqUHI7Y5/lAtFVZYcC1+xG7BSoU+L/DehBqhV8mvexj/avoVEkkVCBmsqtsqMu2WY2hSFT2Miuy/axiV4AOsAX2XBWfODoWVN2rtCbauZ81RZJ/GXNG8V25nNYB2NqSHgW44j9grFU57Jdhav06DwY3Sk9UacbVgnJ0zTlX5ElgMhrgWDcHld0WNUEi6Ky3klIXh6MSdxmilsKP8Z35wugJZS3dCkTm59c3hTO/AO0iMpuUhXf1qarunFjVg0uat80YpyejDi+l5wGphZxWy8P3laLxiX27Pmd3vG2P+kmWrAgMBAAGjgaYwgaMwHQYDVR0OBBYEFIgnFwmpthhgi+zruvZHWcVSVKO3MA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wLgYDVR0fBCcwJTAjoCGgH4YdaHR0cDovL2NybC5hcHBsZS5jb20vcm9vdC5jcmwwDgYDVR0PAQH/BAQDAgGGMBAGCiqGSIb3Y2QGAgEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQBPz+9Zviz1smwvj+4ThzLoBTWobot9yWkMudkXvHcs1Gfi/ZptOllc34MBvbKuKmFysa/Nw0Uwj6ODDc4dR7Txk4qjdJukw5hyhzs+r0ULklS5MruQGFNrCk4QttkdUGwhgAqJTleMa1s8Pab93vcNIx0LSiaHP7qRkkykGRIZbVf1eliHe2iK5IaMSuviSRSqpd1VAKmuu0swruGgsbwpgOYJd+W+NKIByn/c4grmO7i77LpilfMFY0GCzQ87HUyVpNur+cmV6U/kTecmmYHpvPm0KdIBembhLoz2IYrF+Hjhga6/05Cdqa3zr/04GpZnMBxRpVzscYqCtGwPDBUfMIIEuzCCA6OgAwIBAgIBAjANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMDYwNDI1MjE0MDM2WhcNMzUwMjA5MjE0MDM2WjBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDkkakJH5HbHkdQ6wXtXnmELes2oldMVeyLGYne+Uts9QerIjAC6Bg++FAJ039BqJj50cpmnCRrEdCju+QbKsMflZ56DKRHi1vUFjczy8QPTc4UadHJGXL1XQ7Vf1+b8iUDulWPTV0N8WQ1IxVLFVkds5T39pyez1C6wVhQZ48ItCD3y6wsIG9wtj8BMIy3Q88PnT3zK0koGsj+zrW5DtleHNbLPbU6rfQPDgCSC7EhFi501TwN22IWq6NxkkdTVcGvL0Gz+PvjcM3mo0xFfh9Ma1CWQYnEdGILEINBhzOKgbEwWOxaBDKMaLOPHd5lc/9nXmW8Sdh2nzMUZaF3lMktAgMBAAGjggF6MIIBdjAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUK9BpR5R2Cf70a40uQKb3R01/CF4wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wggERBgNVHSAEggEIMIIBBDCCAQAGCSqGSIb3Y2QFATCB8jAqBggrBgEFBQcCARYeaHR0cHM6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2EvMIHDBggrBgEFBQcCAjCBthqBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMA0GCSqGSIb3DQEBBQUAA4IBAQBcNplMLXi37Yyb3PN3m/J20ncwT8EfhYOFG5k9RzfyqZtAjizUsZAS2L70c5vu0mQPy3lPNNiiPvl4/2vIB+x9OYOLUyDTOMSxv5pPCmv/K/xZpwUJfBdAVhEedNO3iyM7R6PVbyTi69G3cN8PReEnyvFteO3ntRcXqNx+IjXKJdXZD9Zr1KIkIxH3oayPc4FgxhtbCS+SsvhESPBgOJ4V9T0mZyCKM2r3DYLP3uujL/lTaltkwGMzd/c6ByxW69oPIQ7aunMZT7XZNn/Bh1XZp5m5MkL72NVxnn6hUrcbvZNCJBIqxw8dtk2cXmPIS4AXUKqK1drk/NAJBzewdXUhMYIByzCCAccCAQEwgaMwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkCCA7rV4fnngmNMAkGBSsOAwIaBQAwDQYJKoZIhvcNAQEBBQAEggEATkHN/72o7XaA2coHtBLR05uJMBEi5KMuGti7C1Dcf2xnrcJWKjzqcI8SOkXJBmJz1EuJ/q/m2L1HrDl5Z7waHFWAQu8tx02STHf8684liprvhNbSvC6c0ywHoUv4p8BUfkLX8zELCDPp2GenjuLytEns+4LAQ7McUTZHHotS5F5StIbJPbYYo6Hscak7Kcbt33aNT5vL2j6fPRBq1eEjrWAJJ8lJgs1VoTney51fMztl5Jx3syv13yXSwh7i2kaQg5jm5yPF+DaPBx5CLGgN18s75gkAxIDYUFeyBSbf1tpJ1pfCyv5Of0YxTzTN7l6Xlt9P8/ickTJrKbtoqOmq0w==';

export const googleReceipt = {
  data: {
    packageName: 'com.topdox.android.trivialdrivesample2',
    productId: 'topdox_android_monthly_subscription',
    purchaseTime: 1456139019030,
    purchaseState: 0,
    purchaseToken:
      'edgcacfhmkpekcilnihgdjkb.AO-J1OxnZr_-c4xGioV-wbb9YI4w7gtRzY87CRLsa6CrHuP_nF97WNzHaBjbqCyZeYYf_sZByLD1DKxkMOFlpIsiOJnSeHxu5XIwa303DbJwFQ7Lo-sM6dgY4-4DCEqk61C9qgUx0GsLaOMZJF0zMC0mRS9K8Z2P3-uSDQpUv0qorTGt7xQC42s',
    autoRenewing: true,
  },
  signature:
    'DefUPOQ2/c3LwySfk+fdczZefijWQ+eZKzOOM3bIH2+Dz+XgNUtoUe4A4logwZKKkduIJthAxuKbf5JeCspTQI8yLCBYRU0LBv4vjINNRpjY/vCeXUaFeQd5Sd1iw186pw7vvsUoSdrIdVlf2BaARJ5M2hO8SmeZRBFxaZOlN5Ud8rRNxFQOkMXxDtwY+6ihYViLDKjY4Ej1wi7pFTPPRz9R7I9APGT9UJQ/M47DWqd3bZMlZ84TPSntRXb/Qf0QUswS9fV36pQCFKwFfIXEmnF1hQfIxMTRyyMKOw7SqPT8xazexDGy9mcxaOzskeC0OZL2E4jfTnQSoMY/woCIKg==',
};

export const surflineGoogleReceipt = {
  type: 'google',
  receipt: {
    data: {
      orderId: 'GPA.1363-3874-8831-70640',
      packageName: 'com.surfline.android',
      productId: 'surfline.android.1month.subscription.notrial',
      purchaseTime: createSubscriptionDate({ sub: 27 }),
      purchaseState: 0,
      developerPayload: 'NzY5MDc4\n',
      purchaseToken:
        'gpoconfagfkppknjheoedpgk.AO-J1OyxNmYBHt5ExmbsaJ5HV2ILhVMUtO4g5turHsoNKFD3-8YYym3mln6eZJskZhmzYV0TsL7lmZejTMOnVw5ZeTpT6y9tMyAMGake-eEfL5Sa-uZW7hcpfS-YvqCLihXjPUsWl02ZwRm23sYW_eP7_-mtuSyh_TmMP2Nd7ZoBtog0hpXZZsc',
      autoRenewing: true,
    },
    signature:
      'Deq++8jMuoupB3lHaAXP08f4GRSs53HlIfi0nU/Zn1COAnqtwZY9RHQbh+ayfe74Jc5UIDUlPW7tJ+hH4o2bduECwOYAHJR8GsAhQvIhtYea8PQJ2W1pta6Ac7qY6PTiBx0hpKX+qwn33LYgAQKzc3Le7bZzwHqiq1m/MAygf8rsl76zjDEuEfwRdmJmOJNpp9y8rWTf8xp52ICx1C8VaIAu67ErqQeUD+YoeoHpoBMFNL3IkFvdL02GdId/6l9tifQYn0CRaamauNhkGCzoYSnBc+613yB1b6NndCIM/6UJrezSP9/3KXLwTpZuZacUu+ST/Bq+cFniZ44F1d2VVA==',
  },
};

export const appleValidPurchaseData = {
  purchases: [
    {
      quantity: 1,
      productId: 'surfline.iosapp.1month.subscription',
      transactionId: '1',
      originalTransactionId: '1',
      purchaseDate: createSubscriptionDate({ sub: 1, timeZone: 'GMT' }),
      purchaseDateMs: createSubscriptionDate({ sub: 1 }),
      purchaseDatePst: createSubscriptionDate({ sub: 1, timeZone: 'PST' }),
      originalPurchaseDate: createSubscriptionDate({
        sub: 28,
        timeZone: 'GMT',
      }),
      originalPurchaseDateMs: createSubscriptionDate({ sub: 28 }),
      originalPurchaseDatePst: createSubscriptionDate({
        sub: 28,
        timeZone: 'PST',
      }),
      expiresDate: createSubscriptionDate({ add: 30, timeZone: 'GMT' }),
      expiresDateMs: createSubscriptionDate({ add: 30 }),
      expiresDatePst: createSubscriptionDate({ add: 30, timeZone: 'PST' }),
      isTrialPeriod: 'false',
      isInIntroOfferPeriod: 'false',
      expirationDate: createSubscriptionDate({ add: 30 }),
    },
  ],
  type: 'apple',
  creationDate: createSubscriptionDate({ sub: 28 }),
  receipt: {
    receipt_type: 'IntegrationTesting',
    adam_id: 0,
    app_item_id: 0,
    bundle_id: 'com.pointabout.235',
    application_version: '0',
    download_id: 0,
    version_external_identifier: 0,
    receipt_creation_date: createSubscriptionDate({ sub: 28, timeZone: 'GMT' }),
    receipt_creation_date_ms: `${createSubscriptionDate({ sub: 28 })}`,
    receipt_creation_date_pst: createSubscriptionDate({
      sub: 28,
      timeZone: 'PST',
    }),
    request_date: `${createSubscriptionDate({ timeZone: 'GMT' })}`,
    request_date_ms: `${createSubscriptionDate()}`,
    request_date_pst: `${createSubscriptionDate({ timeZone: 'PST' })}`,
    original_purchase_date: createSubscriptionDate({
      sub: 28,
      timeZone: 'GMT',
    }),
    original_purchase_date_ms: `${createSubscriptionDate({ sub: 28 })}`,
    original_purchase_date_pst: createSubscriptionDate({
      sub: 28,
      timeZone: 'PST',
    }),
    original_application_version: '1.0',
    in_app: [
      {
        quantity: '1',
        product_id: 'surfline.iosapp.1month.subscription',
        transaction_id: '1',
        original_transaction_id: '1',
        purchase_date: createSubscriptionDate({ sub: 1, timeZone: 'GMT' }),
        purchase_date_ms: `${createSubscriptionDate({ sub: 1 })}`,
        purchase_date_pst: createSubscriptionDate({ sub: 1, timeZone: 'PST' }),
        original_purchase_date: createSubscriptionDate({
          sub: 28,
          timeZone: 'GMT',
        }),
        original_purchase_date_ms: `${createSubscriptionDate({ sub: 28 })}`,
        original_purchase_date_pst: createSubscriptionDate({
          sub: 28,
          timeZone: 'PST',
        }),
        expires_date: createSubscriptionDate({ add: 30, timeZone: 'GMT' }),
        expires_date_ms: `${createSubscriptionDate({ add: 30 })}`,
        expires_date_pst: createSubscriptionDate({ add: 30, timeZone: 'PST' }),
        is_trial_period: 'false',
      },
    ],
  },
};

export const appleValidNonRenewingPurchaseData = {
  purchases: [
    {
      quantity: 1,
      productId: 'FS365APP',
      transactionId: '1',
      originalTransactionId: '1',
      purchaseDate: createSubscriptionDate({ sub: 1, timeZone: 'GMT' }),
      purchaseDateMs: createSubscriptionDate({ sub: 1 }),
      purchaseDatePst: createSubscriptionDate({ sub: 1, timeZone: 'PST' }),
      originalPurchaseDate: createSubscriptionDate({
        sub: 1,
        timeZone: 'GMT',
      }),
      originalPurchaseDateMs: createSubscriptionDate({ sub: 1 }),
      originalPurchaseDatePst: createSubscriptionDate({
        sub: 1,
        timeZone: 'PST',
      }),
      isTrialPeriod: 'false',
      isInIntroOfferPeriod: 'false',
      expirationDate: 0,
    },
  ],
  type: 'apple',
  creationDate: createSubscriptionDate({ sub: 1 }),
  receipt: {
    receipt_type: 'IntegrationTesting',
    adam_id: 0,
    app_item_id: 0,
    bundle_id: 'com.pointabout.235',
    application_version: '0',
    download_id: 0,
    version_external_identifier: 0,
    receipt_creation_date: createSubscriptionDate({ sub: 1, timeZone: 'GMT' }),
    receipt_creation_date_ms: `${createSubscriptionDate({ sub: 1 })}`,
    receipt_creation_date_pst: createSubscriptionDate({
      sub: 1,
      timeZone: 'PST',
    }),
    request_date: `${createSubscriptionDate({ timeZone: 'GMT' })}`,
    request_date_ms: `${createSubscriptionDate()}`,
    request_date_pst: `${createSubscriptionDate({ timeZone: 'PST' })}`,
    original_purchase_date: createSubscriptionDate({
      sub: 1,
      timeZone: 'GMT',
    }),
    original_purchase_date_ms: `${createSubscriptionDate({ sub: 1 })}`,
    original_purchase_date_pst: createSubscriptionDate({
      sub: 1,
      timeZone: 'PST',
    }),
    original_application_version: '1.0',
    in_app: [
      {
        quantity: '1',
        product_id: 'FS365APP',
        transaction_id: '1',
        original_transaction_id: '1',
        purchase_date: createSubscriptionDate({ sub: 1, timeZone: 'GMT' }),
        purchase_date_ms: `${createSubscriptionDate({ sub: 1 })}`,
        purchase_date_pst: createSubscriptionDate({ sub: 1, timeZone: 'PST' }),
        original_purchase_date: createSubscriptionDate({
          sub: 1,
          timeZone: 'GMT',
        }),
        original_purchase_date_ms: `${createSubscriptionDate({ sub: 1 })}`,
        original_purchase_date_pst: createSubscriptionDate({
          sub: 1,
          timeZone: 'PST',
        }),
        is_trial_period: 'false',
      },
    ],
  },
};

export const googleValidPurchaseData = {
  purchases: [
    {
      orderId: 'GPA.1363-3874-8831-70640',
      packageName: 'com.surfline.android',
      productId: 'surfline.android.1month.subscription.notrial',
      purchaseTime: createSubscriptionDate({ sub: 27 }),
      developerPayload: 'NzY5MDc4\n',
      purchaseToken:
        'gpoconfagfkppknjheoedpgk.AO-J1OyxNmYBHt5ExmbsaJ5HV2ILhVMUtO4g5turHsoNKFD3-8YYym3mln6eZJskZhmzYV0TsL7lmZejTMOnVw5ZeTpT6y9tMyAMGake-eEfL5Sa-uZW7hcpfS-YvqCLihXjPUsWl02ZwRm23sYW_eP7_-mtuSyh_TmMP2Nd7ZoBtog0hpXZZsc',
      autoRenewing: false,
      status: 0,
      kind: 'androidpublisher#subscriptionPurchase',
      startTimeMillis: createSubscriptionDate({ sub: 27 }),
      expiryTimeMillis: createSubscriptionDate({ add: 1 }),
      priceCurrencyCode: 'USD',
      priceAmountMicros: 12990000,
      countryCode: 'US',
      cancelReason: null,
      service: 'google',
      expirationTime: createSubscriptionDate({ add: 1 }),
      transactionId:
        'gpoconfagfkppknjheoedpgk.AO-J1OyxNmYBHt5ExmbsaJ5HV2ILhVMUtO4g5turHsoNKFD3-8YYym3mln6eZJskZhmzYV0TsL7lmZejTMOnVw5ZeTpT6y9tMyAMGake-eEfL5Sa-uZW7hcpfS-YvqCLihXjPUsWl02ZwRm23sYW_eP7_-mtuSyh_TmMP2Nd7ZoBtog0hpXZZsc',
      purchaseDate: createSubscriptionDate({ sub: 27 }),
      quantity: 1,
      expirationDate: `${createSubscriptionDate({ add: 1 })}`,
    },
  ],
  type: 'google',
  creationDate: createSubscriptionDate({ sub: 27 }),
  receipt: {
    orderId: 'GPA.1363-3874-8831-70640',
    packageName: 'com.surfline.android',
    productId: 'surfline.android.1month.subscription.notrial',
    purchaseTime: createSubscriptionDate({ sub: 27 }),
    developerPayload: 'NzY5MDc4\n',
    purchaseToken:
      'gpoconfagfkppknjheoedpgk.AO-J1OyxNmYBHt5ExmbsaJ5HV2ILhVMUtO4g5turHsoNKFD3-8YYym3mln6eZJskZhmzYV0TsL7lmZejTMOnVw5ZeTpT6y9tMyAMGake-eEfL5Sa-uZW7hcpfS-YvqCLihXjPUsWl02ZwRm23sYW_eP7_-mtuSyh_TmMP2Nd7ZoBtog0hpXZZsc',
    autoRenewing: false,
    status: 0,
    kind: 'androidpublisher#subscriptionPurchase',
    startTimeMillis: `${createSubscriptionDate({ sub: 27 })}`,
    expiryTimeMillis: `${createSubscriptionDate({ add: 1 })}`,
    priceCurrencyCode: 'USD',
    priceAmountMicros: '12990000',
    countryCode: 'US',
    cancelReason: null,
    service: 'google',
    expirationTime: `${createSubscriptionDate({ add: 1 })}}`,
  },
};

export const appleExpiredPurchaseData = {
  purchases: [
    {
      quantity: 1,
      productId: 'surfline.iosapp.1month.subscription',
      transactionId: '1',
      originalTransactionId: '1',
      purchaseDate: createSubscriptionDate({ sub: 28, timeZone: 'GMT' }),
      purchaseDateMs: createSubscriptionDate({ sub: 28 }),
      purchaseDatePst: createSubscriptionDate({ sub: 28, timeZone: 'PST' }),
      originalPurchaseDate: createSubscriptionDate({
        sub: 28,
        timeZone: 'GMT',
      }),
      originalPurchaseDateMs: createSubscriptionDate({ sub: 28 }),
      originalPurchaseDatePst: createSubscriptionDate({
        sub: 28,
        timeZone: 'PST',
      }),
      expiresDate: createSubscriptionDate({ timeZone: 'GMT' }),
      expiresDateMs: createSubscriptionDate(),
      expiresDatePst: createSubscriptionDate({ timeZone: 'PST' }),
      isTrialPeriod: 'false',
      isInIntroOfferPeriod: 'false',
      expirationDate: createSubscriptionDate(),
    },
  ],
  type: 'apple',
  creationDate: createSubscriptionDate({ sub: 28 }),
  receipt: {
    receipt_type: 'IntegrationTesting',
    adam_id: 0,
    app_item_id: 0,
    bundle_id: 'com.pointabout.235',
    application_version: '0',
    download_id: 0,
    version_external_identifier: 0,
    receipt_creation_date: createSubscriptionDate({ sub: 28, timeZone: 'GMT' }),
    receipt_creation_date_ms: `${createSubscriptionDate({ sub: 28 })}`,
    receipt_creation_date_pst: createSubscriptionDate({
      sub: 28,
      timeZone: 'PST',
    }),
    request_date: `${createSubscriptionDate({ timeZone: 'GMT' })}`,
    request_date_ms: `${createSubscriptionDate()}`,
    request_date_pst: `${createSubscriptionDate({ timeZone: 'PST' })}`,
    original_purchase_date: createSubscriptionDate({
      sub: 28,
      timeZone: 'GMT',
    }),
    original_purchase_date_ms: `${createSubscriptionDate({ sub: 28 })}`,
    original_purchase_date_pst: createSubscriptionDate({
      sub: 28,
      timeZone: 'PST',
    }),
    original_application_version: '1.0',
    in_app: [
      {
        quantity: '1',
        product_id: 'surfline.iosapp.1month.subscription',
        transaction_id: '1',
        original_transaction_id: '1',
        purchase_date: createSubscriptionDate({ sub: 28, timeZone: 'GMT' }),
        purchase_date_ms: `${createSubscriptionDate({ sub: 28 })}`,
        purchase_date_pst: createSubscriptionDate({ sub: 28, timeZone: 'PST' }),
        original_purchase_date: createSubscriptionDate({
          sub: 28,
          timeZone: 'GMT',
        }),
        original_purchase_date_ms: `${createSubscriptionDate({ sub: 28 })}`,
        original_purchase_date_pst: createSubscriptionDate({
          sub: 28,
          timeZone: 'PST',
        }),
        expires_date: createSubscriptionDate({ timeZone: 'GMT' }),
        expires_date_ms: `${createSubscriptionDate()}`,
        expires_date_pst: createSubscriptionDate({ timeZone: 'PST' }),
        is_trial_period: 'false',
      },
    ],
  },
};

export const googleExpiredPurchaseData = {
  purchases: [
    {
      orderId: 'GPA.1363-3874-8831-70640',
      packageName: 'com.surfline.android',
      productId: 'surfline.android.1month.subscription.notrial',
      purchaseTime: createSubscriptionDate({ sub: 60 }),
      developerPayload: 'NzY5MDc4\n',
      purchaseToken:
        'gpoconfagfkppknjheoedpgk.AO-J1OyxNmYBHt5ExmbsaJ5HV2ILhVMUtO4g5turHsoNKFD3-8YYym3mln6eZJskZhmzYV0TsL7lmZejTMOnVw5ZeTpT6y9tMyAMGake-eEfL5Sa-uZW7hcpfS-YvqCLihXjPUsWl02ZwRm23sYW_eP7_-mtuSyh_TmMP2Nd7ZoBtog0hpXZZsc',
      autoRenewing: false,
      status: 0,
      kind: 'androidpublisher#subscriptionPurchase',
      startTimeMillis: createSubscriptionDate({ sub: 60 }),
      expiryTimeMillis: createSubscriptionDate({ sub: 30 }),
      priceCurrencyCode: 'USD',
      priceAmountMicros: 12990000,
      countryCode: 'US',
      cancelReason: null,
      service: 'google',
      expirationTime: createSubscriptionDate({ sub: 30 }),
      transactionId:
        'gpoconfagfkppknjheoedpgk.AO-J1OyxNmYBHt5ExmbsaJ5HV2ILhVMUtO4g5turHsoNKFD3-8YYym3mln6eZJskZhmzYV0TsL7lmZejTMOnVw5ZeTpT6y9tMyAMGake-eEfL5Sa-uZW7hcpfS-YvqCLihXjPUsWl02ZwRm23sYW_eP7_-mtuSyh_TmMP2Nd7ZoBtog0hpXZZsc',
      purchaseDate: createSubscriptionDate({ sub: 60 }),
      quantity: 1,
      expirationDate: `${createSubscriptionDate({ sub: 30 })}`,
    },
  ],
  type: 'google',
  creationDate: createSubscriptionDate({ sub: 60 }),
  receipt: {
    orderId: 'GPA.1363-3874-8831-70640',
    packageName: 'com.surfline.android',
    productId: 'surfline.android.1month.subscription.notrial',
    purchaseTime: createSubscriptionDate({ sub: 60 }),
    developerPayload: 'NzY5MDc4\n',
    purchaseToken:
      'gpoconfagfkppknjheoedpgk.AO-J1OyxNmYBHt5ExmbsaJ5HV2ILhVMUtO4g5turHsoNKFD3-8YYym3mln6eZJskZhmzYV0TsL7lmZejTMOnVw5ZeTpT6y9tMyAMGake-eEfL5Sa-uZW7hcpfS-YvqCLihXjPUsWl02ZwRm23sYW_eP7_-mtuSyh_TmMP2Nd7ZoBtog0hpXZZsc',
    autoRenewing: false,
    status: 0,
    kind: 'androidpublisher#subscriptionPurchase',
    startTimeMillis: `${createSubscriptionDate({ sub: 60 })}`,
    expiryTimeMillis: `${createSubscriptionDate({ sub: 30 })}`,
    priceCurrencyCode: 'USD',
    priceAmountMicros: '12990000',
    countryCode: 'US',
    cancelReason: null,
    service: 'google',
    expirationTime: `${createSubscriptionDate({ sub: 30 })}}`,
  },
};

export const appleAutoRenewingPurchaseData = {
  purchases: [
    {
      quantity: 1,
      productId: 'surfline.iosapp.1month.subscription',
      transactionId: '1',
      originalTransactionId: '1',
      purchaseDate: createSubscriptionDate({ sub: 28, timeZone: 'GMT' }),
      purchaseDateMs: createSubscriptionDate({ sub: 28 }),
      purchaseDatePst: createSubscriptionDate({ sub: 28, timeZone: 'PST' }),
      originalPurchaseDate: createSubscriptionDate({
        sub: 28,
        timeZone: 'GMT',
      }),
      originalPurchaseDateMs: createSubscriptionDate({ sub: 28 }),
      originalPurchaseDatePst: createSubscriptionDate({
        sub: 28,
        timeZone: 'PST',
      }),
      expiresDate: createSubscriptionDate({ timeZone: 'GMT' }),
      expiresDateMs: createSubscriptionDate(),
      expiresDatePst: createSubscriptionDate({ timeZone: 'PST' }),
      isTrialPeriod: 'false',
      isInIntroOfferPeriod: 'false',
      expirationDate: createSubscriptionDate(),
    },
    {
      quantity: 1,
      productId: 'surfline.iosapp.1month.subscription',
      transactionId: '2',
      originalTransactionId: '1',
      purchaseDate: createSubscriptionDate({ timeZone: 'GMT' }),
      purchaseDateMs: createSubscriptionDate(),
      purchaseDatePst: createSubscriptionDate({ timeZone: 'PST' }),
      originalPurchaseDate: createSubscriptionDate({
        sub: 28,
        timeZone: 'GMT',
      }),
      originalPurchaseDateMs: createSubscriptionDate({ sub: 28 }),
      originalPurchaseDatePst: createSubscriptionDate({
        sub: 28,
        timeZone: 'PST',
      }),
      expiresDate: createSubscriptionDate({ add: 35, timeZone: 'GMT' }),
      expiresDateMs: createSubscriptionDate({ add: 35 }),
      expiresDatePst: createSubscriptionDate({ add: 35, timeZone: 'PST' }),
      isTrialPeriod: 'false',
      isInIntroOfferPeriod: 'false',
      expirationDate: createSubscriptionDate({ add: 30 }),
    },
  ],
  type: 'apple',
  creationDate: createSubscriptionDate({ sub: 28 }),
  receipt: {
    receipt_type: 'IntegrationTesting',
    adam_id: 0,
    app_item_id: 0,
    bundle_id: 'com.pointabout.235',
    application_version: '0',
    download_id: 0,
    version_external_identifier: 0,
    receipt_creation_date: createSubscriptionDate({ sub: 28, timeZone: 'GMT' }),
    receipt_creation_date_ms: `${createSubscriptionDate({ sub: 28 })}`,
    receipt_creation_date_pst: createSubscriptionDate({
      sub: 28,
      timeZone: 'PST',
    }),
    request_date: `${createSubscriptionDate({ timeZone: 'GMT' })}`,
    request_date_ms: `${createSubscriptionDate()}`,
    request_date_pst: `${createSubscriptionDate({ timeZone: 'PST' })}`,
    original_purchase_date: createSubscriptionDate({
      sub: 28,
      timeZone: 'GMT',
    }),
    original_purchase_date_ms: `${createSubscriptionDate({ sub: 28 })}`,
    original_purchase_date_pst: createSubscriptionDate({
      sub: 28,
      timeZone: 'PST',
    }),
    original_application_version: '1.0',
    in_app: [
      {
        quantity: '1',
        product_id: 'surfline.iosapp.1month.subscription',
        transaction_id: '1',
        original_transaction_id: '1',
        purchase_date: createSubscriptionDate({ sub: 28, timeZone: 'GMT' }),
        purchase_date_ms: `${createSubscriptionDate({ sub: 28 })}`,
        purchase_date_pst: createSubscriptionDate({ sub: 28, timeZone: 'PST' }),
        original_purchase_date: createSubscriptionDate({
          sub: 28,
          timeZone: 'GMT',
        }),
        original_purchase_date_ms: `${createSubscriptionDate({ sub: 28 })}`,
        original_purchase_date_pst: createSubscriptionDate({
          sub: 28,
          timeZone: 'PST',
        }),
        expires_date: createSubscriptionDate({ timeZone: 'GMT' }),
        expires_date_ms: `${createSubscriptionDate()}`,
        expires_date_pst: createSubscriptionDate({ timeZone: 'PST' }),
        web_order_line_item_id: '1000000033744561',
        is_trial_period: 'false',
      },
      {
        quantity: '1',
        product_id: 'surfline.iosapp.1month.subscription',
        transaction_id: '2',
        original_transaction_id: '1',
        purchase_date: createSubscriptionDate({ timeZone: 'GMT' }),
        purchase_date_ms: `${createSubscriptionDate()}`,
        purchase_date_pst: createSubscriptionDate({ timeZone: 'PST' }),
        original_purchase_date: createSubscriptionDate({
          sub: 28,
          timeZone: 'GMT',
        }),
        original_purchase_date_ms: `${createSubscriptionDate({ sub: 28 })}`,
        original_purchase_date_pst: createSubscriptionDate({
          sub: 28,
          timeZone: 'PST',
        }),
        expires_date: createSubscriptionDate({ add: 35, timeZone: 'GMT' }),
        expires_date_ms: `${createSubscriptionDate({ add: 35 })}`,
        expires_date_pst: createSubscriptionDate({ add: 35, timeZone: 'PST' }),
        web_order_line_item_id: '1000000033744561',
        is_trial_period: 'false',
      },
    ],
  },
};

export const googleAutoRenewingPurchaseData = {
  purchases: [
    {
      orderId: 'GPA.1363-3874-8831-70640',
      packageName: 'com.surfline.android',
      productId: 'surfline.android.1month.subscription.notrial',
      purchaseTime: createSubscriptionDate({ sub: 27 }),
      developerPayload: 'NzY5MDc4\n',
      purchaseToken:
        'gpoconfagfkppknjheoedpgk.AO-J1OyxNmYBHt5ExmbsaJ5HV2ILhVMUtO4g5turHsoNKFD3-8YYym3mln6eZJskZhmzYV0TsL7lmZejTMOnVw5ZeTpT6y9tMyAMGake-eEfL5Sa-uZW7hcpfS-YvqCLihXjPUsWl02ZwRm23sYW_eP7_-mtuSyh_TmMP2Nd7ZoBtog0hpXZZsc',
      autoRenewing: true,
      status: 0,
      kind: 'androidpublisher#subscriptionPurchase',
      startTimeMillis: createSubscriptionDate({ sub: 27 }),
      expiryTimeMillis: createSubscriptionDate({ add: 1 }),
      priceCurrencyCode: 'USD',
      priceAmountMicros: 12990000,
      countryCode: 'US',
      cancelReason: 0,
      userCancellationTimeMillis: createSubscriptionDate({ sub: 2 }),
      service: 'google',
      expirationTime: createSubscriptionDate({ add: 1 }),
      transactionId:
        'gpoconfagfkppknjheoedpgk.AO-J1OyxNmYBHt5ExmbsaJ5HV2ILhVMUtO4g5turHsoNKFD3-8YYym3mln6eZJskZhmzYV0TsL7lmZejTMOnVw5ZeTpT6y9tMyAMGake-eEfL5Sa-uZW7hcpfS-YvqCLihXjPUsWl02ZwRm23sYW_eP7_-mtuSyh_TmMP2Nd7ZoBtog0hpXZZsc',
      purchaseDate: createSubscriptionDate({ sub: 27 }),
      quantity: 1,
      expirationDate: `${createSubscriptionDate({ add: 1 })}`,
      cancellationDate: createSubscriptionDate({ sub: 2 }),
    },
    {
      orderId: 'GPA.1363-3874-8831-70640',
      packageName: 'com.surfline.android',
      productId: 'surfline.android.1month.subscription.notrial',
      purchaseTime: createSubscriptionDate(),
      developerPayload: 'NzY5MDc4\n',
      purchaseToken:
        'gpoconfagfkppknjheoedpgk.AO-J1OyxNmYBHt5ExmbsaJ5HV2ILhVMUtO4g5turHsoNKFD3-8YYym3mln6eZJskZhmzYV0TsL7lmZejTMOnVw5ZeTpT6y9tMyAMGake-eEfL5Sa-uZW7hcpfS-YvqCLihXjPUsWl02ZwRm23sYW_eP7_-mtuSyh_TmMP2Nd7ZoBtog0hpXZZsc',
      autoRenewing: true,
      status: 0,
      kind: 'androidpublisher#subscriptionPurchase',
      startTimeMillis: createSubscriptionDate(),
      expiryTimeMillis: createSubscriptionDate({ add: 30 }),
      priceCurrencyCode: 'USD',
      priceAmountMicros: 12990000,
      countryCode: 'US',
      service: 'google',
      expirationTime: createSubscriptionDate({ add: 30 }),
      transactionId:
        'gpoconfagfkppknjheoedpgk.AO-J1OyxNmYBHt5ExmbsaJ5HV2ILhVMUtO4g5turHsoNKFD3-8YYym3mln6eZJskZhmzYV0TsL7lmZejTMOnVw5ZeTpT6y9tMyAMGake-eEfL5Sa-uZW7hcpfS-YvqCLihXjPUsWl02ZwRm23sYW_eP7_-mtuSyh_TmMP2Nd7ZoBtog0hpXZZsc',
      purchaseDate: createSubscriptionDate(),
      quantity: 1,
      expirationDate: `${createSubscriptionDate({ add: 30 })}`,
    },
  ],
  type: 'google',
  creationDate: createSubscriptionDate({ sub: 27 }),
  receipt: {
    orderId: 'GPA.1363-3874-8831-70640',
    packageName: 'com.surfline.android',
    productId: 'surfline.android.1month.subscription.notrial',
    purchaseTime: createSubscriptionDate(),
    developerPayload: 'NzY5MDc4\n',
    purchaseToken:
      'gpoconfagfkppknjheoedpgk.AO-J1OyxNmYBHt5ExmbsaJ5HV2ILhVMUtO4g5turHsoNKFD3-8YYym3mln6eZJskZhmzYV0TsL7lmZejTMOnVw5ZeTpT6y9tMyAMGake-eEfL5Sa-uZW7hcpfS-YvqCLihXjPUsWl02ZwRm23sYW_eP7_-mtuSyh_TmMP2Nd7ZoBtog0hpXZZsc',
    autoRenewing: false,
    status: 0,
    kind: 'androidpublisher#subscriptionPurchase',
    startTimeMillis: `${createSubscriptionDate()}`,
    expiryTimeMillis: `${createSubscriptionDate({ add: 30 })}`,
    priceCurrencyCode: 'USD',
    priceAmountMicros: '12990000',
    countryCode: 'US',
    service: 'google',
    expirationTime: `${createSubscriptionDate({ add: 30 })}}`,
  },
};

export const appleOutDatedSubPurchaseData = {
  purchases: [
    {
      quantity: 1,
      productId: 'surfline.iosapp.1month.subscription',
      transactionId: '1',
      originalTransactionId: '1',
      purchaseDate: createSubscriptionDate({ sub: 90, timeZone: 'GMT' }),
      purchaseDateMs: createSubscriptionDate({ sub: 90 }),
      purchaseDatePst: createSubscriptionDate({ sub: 90, timeZone: 'PST' }),
      originalPurchaseDate: createSubscriptionDate({
        sub: 90,
        timeZone: 'GMT',
      }),
      originalPurchaseDateMs: createSubscriptionDate({ sub: 90 }),
      originalPurchaseDatePst: createSubscriptionDate({
        sub: 90,
        timeZone: 'PST',
      }),
      expiresDate: createSubscriptionDate({ sub: 60, timeZone: 'GMT' }),
      expiresDateMs: createSubscriptionDate({ sub: 60 }),
      expiresDatePst: createSubscriptionDate({ sub: 60, timeZone: 'PST' }),
      expirationDate: createSubscriptionDate({ sub: 60 }),
      isTrialPeriod: 'false',
      isInIntroOfferPeriod: 'false',
    },
    {
      quantity: 1,
      productId: 'surfline.iosapp.1month.subscription',
      transactionId: '2',
      originalTransactionId: '1',
      purchaseDate: createSubscriptionDate({ sub: 60, timeZone: 'GMT' }),
      purchaseDateMs: createSubscriptionDate({ sub: 60 }),
      purchaseDatePst: createSubscriptionDate({ sub: 60, timeZone: 'PST' }),
      originalPurchaseDate: createSubscriptionDate({
        sub: 90,
        timeZone: 'GMT',
      }),
      originalPurchaseDateMs: createSubscriptionDate({ sub: 90 }),
      originalPurchaseDatePst: createSubscriptionDate({
        sub: 90,
        timeZone: 'PST',
      }),
      expiresDate: createSubscriptionDate({ sub: 30, timeZone: 'GMT' }),
      expiresDateMs: createSubscriptionDate({ sub: 30 }),
      expiresDatePst: createSubscriptionDate({ sub: 30, timeZone: 'PST' }),
      expirationDate: createSubscriptionDate({ sub: 30 }),
      isTrialPeriod: 'false',
      isInIntroOfferPeriod: 'false',
    },
  ],
  type: 'apple',
  creationDate: createSubscriptionDate({ sub: 90 }),
  receipt: {
    receipt_type: 'EndedSubscription',
    adam_id: 0,
    app_item_id: 0,
    bundle_id: 'com.pointabout.235',
    application_version: '0',
    download_id: 0,
    version_external_identifier: 0,
    receipt_creation_date: createSubscriptionDate({ sub: 90, timeZone: 'GMT' }),
    receipt_creation_date_ms: `${createSubscriptionDate({ sub: 90 })}`,
    receipt_creation_date_pst: createSubscriptionDate({
      sub: 90,
      timeZone: 'PST',
    }),
    request_date: `${createSubscriptionDate({ timeZone: 'GMT' })}`,
    request_date_ms: `${createSubscriptionDate()}`,
    request_date_pst: `${createSubscriptionDate({ timeZone: 'PST' })}`,
    original_purchase_date: createSubscriptionDate({
      sub: 90,
      timeZone: 'GMT',
    }),
    original_purchase_date_ms: `${createSubscriptionDate({ sub: 90 })}`,
    original_purchase_date_pst: createSubscriptionDate({
      sub: 90,
      timeZone: 'PST',
    }),
    original_application_version: '1.0',
    in_app: [
      {
        quantity: '1',
        product_id: 'surfline.iosapp.1month.subscription',
        transaction_id: '1',
        original_transaction_id: '1',
        purchase_date: createSubscriptionDate({ sub: 90, timeZone: 'GMT' }),
        purchase_date_ms: `${createSubscriptionDate({ sub: 90 })}`,
        purchase_date_pst: createSubscriptionDate({ sub: 90, timeZone: 'PST' }),
        original_purchase_date: createSubscriptionDate({
          sub: 90,
          timeZone: 'GMT',
        }),
        original_purchase_date_ms: `${createSubscriptionDate({ sub: 90 })}`,
        original_purchase_date_pst: createSubscriptionDate({
          sub: 90,
          timeZone: 'PST',
        }),
        expires_date: createSubscriptionDate({ sub: 60, timeZone: 'GMT' }),
        expires_date_ms: `${createSubscriptionDate({ sub: 60 })}`,
        expires_date_pst: createSubscriptionDate({ sub: 60, timeZone: 'PST' }),
        is_trial_period: 'false',
      },
      {
        quantity: '1',
        product_id: 'surfline.iosapp.1month.subscription',
        transaction_id: '2',
        original_transaction_id: '1',
        purchase_date: createSubscriptionDate({ sub: 60, timeZone: 'GMT' }),
        purchase_date_ms: `${createSubscriptionDate({ sub: 60 })}`,
        purchase_date_pst: createSubscriptionDate({ sub: 60, timeZone: 'PST' }),
        original_purchase_date: createSubscriptionDate({
          sub: 90,
          timeZone: 'GMT',
        }),
        original_purchase_date_ms: `${createSubscriptionDate({ sub: 90 })}`,
        original_purchase_date_pst: createSubscriptionDate({
          sub: 90,
          timeZone: 'PST',
        }),
        expires_date: createSubscriptionDate({ sub: 30, timeZone: 'GMT' }),
        expires_date_ms: `${createSubscriptionDate({ sub: 30 })}`,
        expires_date_pst: createSubscriptionDate({ sub: 30, timeZone: 'PST' }),
        is_trial_period: 'false',
      },
    ],
  },
};

export const appleOutdatedSubscription = {
  type: 'apple',
  subscriptionId: '2',
  periodStart: new Date(createSubscriptionDate({ sub: 60 })),
  periodEnd: new Date(createSubscriptionDate({ sub: 30 })),
  entitlement: 'sl_premium',
  applePlanId: 'surfline.iosapp.1month.subscription',
  createdAt: new Date(createSubscriptionDate({ sub: 60 })),
  expiring: false,
  failedPaymentAttempts: 0,
};

export const appleArchivedSubs = [
  {
    type: 'apple',
    subscriptionId: '2',
    periodStart: new Date(createSubscriptionDate({ sub: 90 })),
    periodEnd: new Date(createSubscriptionDate({ sub: 60 })),
    entitlement: 'sl_premium',
    applePlanId: 'surfline.iosapp.1month.subscription',
    createdAt: new Date(createSubscriptionDate({ sub: 90 })),
    expiring: true,
    failedPaymentAttempts: 0,
  },
  {
    type: 'apple',
    subscriptionId: '1000',
    periodStart: new Date(createSubscriptionDate({ sub: 1000 })),
    periodEnd: new Date(createSubscriptionDate({ sub: 900 })),
    entitlement: 'sl_premium',
    applePlanId: 'surfline.iosapp.1month.subscription',
    createdAt: new Date(createSubscriptionDate({ sub: 1000 })),
    expiring: true,
    failedPaymentAttempts: 0,
  },
];

export const testItunesReceiptString = 'ENCRYPTEDSTUBABC123';

export const defaultItunesReceiptDataOpts = {
  productId: 'surfline.iosapp.1month.subscription.notrial',
  isTrialPeriod: 'false',
  transactionId: '201910100000',
  originalTransactionId: '201910100000',
};
