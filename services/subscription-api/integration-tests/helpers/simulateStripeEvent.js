// eslint-disable-next-line
import request from 'superagent';

const servicesURI = 'http://localhost:3000';
const stripeIntegrationsAPI = `${servicesURI}/integrations/stripe/handler`;

export default async (event, subscriptionAttrs, optionalAttrs) => {
  const stub = await import(`../../src/__stubs__/stripe/${event}`);
  const { stripeCustomerId, subscriptionId, price } = subscriptionAttrs;
  const mockEventPayload = stub.default(
    {
      customerId: stripeCustomerId,
      subscriptionId,
      price,
    },
    optionalAttrs,
  );

  return new Promise((resolve, reject) => {
    request
      .post(stripeIntegrationsAPI)
      .send(mockEventPayload)
      .end((err, resp) => {
        if (err) reject(err);
        resolve(resp.body);
      });
  });
};
