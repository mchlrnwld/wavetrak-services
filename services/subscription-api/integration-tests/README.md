#### Subscription Backend Integration Tests:

The subscription backend integration tests run using mocha/chai. Other services run the entire app in a separate docker container and then make requests against that container, however for these we need to be able to stub webhooks and calls to `getPurchaseData` so we have to run the app and tests from the same container. Besides that the approach is pretty much the same as in the other servies. We create mongo and redis containers, populate them with necessary collections/data, stub functions that require stubbing, and then run tests against the endpoints.

The stripe test account we use for these tests is called `Surfline Integration Tests`.

#### Run the tests locally

- `npm run test:integration:local`, this command runs a bash script that will edit the env file and run the tests for you :)

#### Troubleshooting

- if test cases fail due to duplicate records in mongo (e.g `MongoError: E11000 duplicate key error collection: UserDB.StripeCustomers`), try clearing the UserDB collection in your dockerized mongo instance



