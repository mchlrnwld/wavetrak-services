#!/usr/bin/env bash

# receipt-job-build-task.sh
#   1) Builds Docker image for receipt job
#   2) Pushes Docker image to ECR
#   3) Registers task definition for receipt job container
#
# 'docker run ${IMAGE_NAME}' to run subscription job
# Subscription Jobs are in ./src/jobs
# Configure application specific environment variables in a Jenkins config file
#   Include `COMMIT_RECEIPT_JOB_CHANGES=true` if you're ready to save UserAccount changes
# Configure build specific environment variables in Jenkins job (CMD+F in this file for usages):
#   ENVIRONMENT='staging'
#   VERSION='latest-staging'
#   CLUSTER='sl-admin-tools-staging'

# Fixed environment variables
if [ "$ENVIRONMENT" = "prod" ]; then
    export REGISTRY_HOST="833713747344.dkr.ecr.us-west-1.amazonaws.com"
else
    export REGISTRY_HOST="665294954271.dkr.ecr.us-west-1.amazonaws.com"
fi
FAMILY=sl-jobs
IMAGE_NAME=sl-receipt-jobs

cp ../../env.${ENVIRONMENT} .env

# build and execute the job
docker build -t ${IMAGE_NAME} -f Dockerfile.receipt-job .
docker run --rm --entrypoint /bin/bash ${IMAGE_NAME} -c date

# tag & publish image
$(aws --profile ${ENVIRONMENT} ecr get-login --no-include-email --region us-west-1)
docker tag ${IMAGE_NAME} ${REGISTRY_HOST}/${IMAGE_NAME}:${VERSION}
docker push "${REGISTRY_HOST}/${IMAGE_NAME}:${VERSION}"

# Register task definition and run container task
aws ecs register-task-definition --profile ${ENVIRONMENT} --cli-input-json file://src/jobs/${IMAGE_NAME}-${ENVIRONMENT}-task-def.json --region us-west-1
echo $(aws ecs list-task-definitions --profile ${ENVIRONMENT} --family-prefix ${FAMILY} | jq -r '.taskDefinitionArns[-1]')
