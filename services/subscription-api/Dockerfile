###########################################################
# BASE
###########################################################
FROM node:12 AS base

WORKDIR /opt/app

COPY .npmrc .
COPY package.json .
COPY package-lock.json .

###########################################################
# RELEASE-PACKAGES
###########################################################
FROM base AS release-packages

RUN npm ci --production --prefer-offline \
 && touch .env

###########################################################
# ALL-PACKAGES
###########################################################
FROM release-packages AS all-packages

RUN npm ci --prefer-offline

###########################################################
# TEST
###########################################################
FROM all-packages AS test

COPY . .

RUN npm run lint
RUN npm run test

###########################################################
# BUILD
###########################################################
FROM test AS build

ARG APP_ENV=sandbox
ARG APP_VERSION=master

ENV NODE_ENV=$APP_ENV
ENV APP_VERSION=$APP_VERSION

RUN npm run dist

###########################################################
# RELEASE
###########################################################
FROM node:12 AS release

WORKDIR /opt/app

COPY --from=build /opt/app/dist dist
COPY --from=build /opt/app/newrelic.js dist/newrelic.js
COPY --from=build /opt/app/package.json .
COPY --from=release-packages /opt/app/node_modules node_modules

CMD npm start
