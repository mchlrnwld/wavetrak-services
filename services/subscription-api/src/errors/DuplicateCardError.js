import { BaseError } from '@surfline/services-common';

class DuplicateCardError extends BaseError {
  constructor(wavetrakCode = null, statusCode = 400) {
    super('DuplicateCardError', 'This card has already been used for a free trial on this brand.', {
      statusCode,
      wavetrakCode,
    });
  }
}

export default DuplicateCardError;
