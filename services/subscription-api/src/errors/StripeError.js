import { BaseError } from '@surfline/services-common';

export default class StripeError extends BaseError {
  constructor(err) {
    const message = err.message || 'There was a problem processing the payment';
    super('StripeError', message, { statusCode: StripeError.getStatusCode(err) });
  }

  static getStatusCode(err) {
    switch (err.type) {
      case 'StripeCardError':
        // "Your card's expiration year is invalid."
        return 400;
      case 'StripeRateLimitError':
        // Too many requests made to the API too quickly
        return 400;
      case 'StripeInvalidRequestError':
        // Invalid parameters were supplied to Stripe's API
        return 400;
      case 'StripeAPIError':
        // An error occurred internally with Stripe's API
        return 400;
      case 'StripeConnectionError':
        // Some kind of error occurred during the HTTPS communication
        return 500;
      case 'StripeAuthenticationError':
        // You probably used an incorrect API key
        return 401;
      default:
        return 500;
    }
  }
}
