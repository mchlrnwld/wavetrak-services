import Analytics from 'analytics-node';

const bwAnalytics = new Analytics(process.env.BW_SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });
const fsAnalytics = new Analytics(process.env.FS_SEGMENT_WRITE_KEY || 'NoWrite', { flushAt: 1 });
export const slAnalytics = new Analytics(process.env.SEGMENT_WRITE_KEY || 'NoWrite', {
  flushAt: 1,
});

const getAnalytics = entitlement => {
  if (entitlement.includes('sl', 0)) return slAnalytics;
  if (entitlement.includes('bw', 0)) return bwAnalytics;
  if (entitlement.includes('fs', 0)) return fsAnalytics;
  return null;
};

export const track = params => {
  const { entitlement } = params.properties;
  const wtAnalytics = getAnalytics(entitlement);
  wtAnalytics.track(params);
};

export const trackAll = params => {
  bwAnalytics.track(params);
  fsAnalytics.track(params);
  slAnalytics.track(params);
};

export const identifyAll = params => {
  bwAnalytics.identify(params);
  fsAnalytics.identify(params);
  slAnalytics.identify(params);
};

export const identify = params => {
  const entitlement = params.traits['subscription.entitlement'];
  const wtAnalytics = getAnalytics(entitlement);
  wtAnalytics.identify(params);
};
