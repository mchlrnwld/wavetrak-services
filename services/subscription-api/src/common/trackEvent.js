import uuidv4 from 'uuid/v4';
import * as analytics from './analytics';
import customerio from './customerio';

/**
 *  Track card fingerprint
 */
export async function trackCreditCardFingerprint(fingerprint, userInfoId, entitlement) {
  if (fingerprint && userInfoId) {
    analytics.track({
      userId: userInfoId,
      event: 'Added Card',
      properties: {
        fingerprint,
        entitlement,
        platform: 'web', // Invoked when adding cards via Stripe
      },
    });
  }
}

/**
 * Track subscription grandfathered
 *
 * This event tracks old premium annual users who have renewed their grandfathered
 * billing cycle, and have been converted to the new sl_annual_v2 pricing plan.
 */
export function trackSubscriptionGrandfathered(userInfoId) {
  analytics.track({
    userId: userInfoId,
    event: 'Subscription Grandfathered',
    properties: {
      planId: 'sl_annual_v2',
      newPlan: 'sl_annual_v2',
      oldPlan: 'sl_annual',
      platform: 'web', // Invoked from invoice.payment_succeeded Stripe webhook
    },
  });
}

/**
 * Track Gift Code Purchase
 *
 * This event tracks a Gift Code purchase.
 * It will be fired after a successfull stripe charge
 * and once the Gift Code is generated and saved in database.
 */
export function trackGiftCodePurchase(data, opts) {
  const anonymousId = !data.userId ? uuidv4() : null;
  analytics.track({
    event: 'Purchased Gift Card',
    anonymousId,
    properties: {
      ...data,
    },
  });
  customerio({
    name: 'Purchased Gift Card',
    data: {
      ...data,
      amount: `$${data.amount / 100}`,
      recipient: data.purchaserEmail,
      recipientEmail: data.recipientEmail || data.purchaserEmail,
      recipientName: data.recipientName || data.purchaserName,
    },
    brand: opts.brand,
  });
  customerio({
    name: 'Send Gift Code',
    data: {
      ...data,
      recipient: data.recipientEmail || data.purchaserEmail,
      recipientEmail: data.recipientEmail || data.purchaserEmail,
      recipientName: data.recipientName || data.purchaserName,
    },
    brand: opts.brand,
  });
}

/**
 * Track Gift Code Generation (Internal use only)
 *
 * This event tracks a Gift Code generated internally.
 * It is fired for every code generated for internal distribution
 */
export function trackGiftCodeGenerated(data) {
  analytics.track({
    event: 'Generated Gift Card',
    userId: data.userId,
    properties: {
      ...data,
    },
  });
}

/**
 * Track Gift Code Redemption
 *
 * This even tracks a Gift Code Redemptions
 *
 *
 */
export function trackGiftCodeRedeemed(data) {
  const { subscription, code } = data;
  const userId = data.userId || data.user;
  analytics.track({
    event: 'Redeemed Gift Card',
    userId,
    properties: {
      planId: subscription.planId || subscription.stripePlanId,
      redeemCode: code,
      platform: 'web', // Gift Subs are Stripe managed
    },
  });

  if (subscription.type === 'gift') {
    const { periodEnd, entitlement } = subscription;
    const identifyTraits = {
      'subscription.entitlement': entitlement,
      'subscription.expiration': periodEnd,
      'subscription.plan_id': subscription.planId || subscription.stripePlanId,
    };
    analytics.identify({
      userId: userId.toString(),
      traits: identifyTraits,
    });
  }
}

/**
 * Track Accepted Subscription Offer
 *
 * This even tracks a redemptions of RetentionOffer Promotion types
 * @params {data}
 *  data.userId || String
 *  data.promotionId || String
 *  data.promotionName || String
 *  data.planId || String
 */
export function trackAcceptedSubscriptionOffer(data) {
  const { userId, promotionId, promotionName, planId } = data;
  analytics.track({
    event: 'Accepted Subscription Offer',
    userId,
    properties: {
      promotionId,
      promotionName,
      planId,
      platform: 'web', // This track event is invoked for Stripe subscriptions only
    },
  });
}

/**
 * Track Days to convert to trial
 *
 * This even tracks a sub renewal 7 days before a user's trial turns into a subscription
 * Triggers email notifying user of their upcoming upgrade
 * @params {data}
 *  data.event || Object
 *  data.userId || String || ObjectId
 *  data.cardType || String
 */
export const trackTrialEnding = (event, userId, cardType = 'visa') => {
  const { lines } = event;
  const {
    plan: { id },
  } = lines.data[0];
  analytics.track({
    event: 'Days to Convert from Trial',
    userId: userId.toString(),
    properties: {
      planId: id,
      daysToPremium: 7,
      cardType,
      platform: 'web', // Invoked from invoice.upcoming Stripe webhook
    },
  });
};

export const trackStripeCustomerDissociated = (userId, customerId) => {
  analytics.trackAll({
    event: 'Dissociated Stripe Customer',
    userId: userId.toString(),
    properties: {
      previousStripeCustomerId: customerId,
      platform: 'web',
    },
  });
};
