import CIO from 'customerio-node';

const bwCIO = new CIO(process.env.BW_CIO_SITEID, process.env.BW_CIO_APIKEY);
const fsCIO = new CIO(process.env.FS_CIO_SITEID, process.env.FS_CIO_APIKEY);
const slCIO = new CIO(process.env.SL_CIO_SITEID, process.env.SL_CIO_APIKEY);

const cioClient = {
  bw: bwCIO,
  fs: fsCIO,
  sl: slCIO,
};

export default ({ name, data, brand }) => {
  const cioApi = cioClient[brand];
  cioApi.trackAnonymous({
    name,
    data,
  });
};
