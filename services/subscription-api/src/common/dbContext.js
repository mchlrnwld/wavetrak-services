import mongoose from 'mongoose';
import redis from 'redis';
import logger from './logger';

// Assign global promise as the default for mongo
mongoose.Promise = global.Promise;

const log = logger('subscription-service:common:dbContext');

let redisClient = {};

export function initCache() {
  const connectionString = `redis://${process.env.REDIS_IP}:${process.env.REDIS_PORT}`;
  return new Promise((resolve, reject) => {
    try {
      log.trace(`Redis ConnectionString: ${connectionString} `);
      redisClient = redis.createClient(connectionString);
      redisClient.once('ready', () => {
        log.info(`Redis connected on ${connectionString} and ready to accept commands`);
        resolve();
      });
      redisClient.on('error', error => {
        log.error({
          action: 'Redis:ConnectionError',
          error,
        });
        reject(error);
      });
    } catch (error) {
      log.error({
        action: 'Redis:ConnectionError',
        error,
      });
      reject(error);
    }
  });
}

export function connectMongo() {
  const connectionString = process.env.MONGO_CONNECTION_STRING;
  const mongoDbConfig = {
    poolSize: 50,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  };
  return new Promise((resolve, reject) => {
    try {
      log.trace(`Mongo ConnectionString: ${connectionString} `);
      mongoose.connect(connectionString, mongoDbConfig);
      mongoose.connection.once('open', () => {
        log.info(`MongoDB connected on ${connectionString}`);
        resolve();
      });
      mongoose.connection.on('error', error => {
        log.error({
          action: 'MongoDB:ConnectionError',
          error,
        });
        reject(error);
      });
    } catch (error) {
      log.error({
        message: 'MongoDB:ConnectionError',
        stack: error,
      });
      reject(error);
    }
  });
}

export function cacheClient() {
  return redisClient;
}
