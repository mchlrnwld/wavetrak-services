/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */

let configFileName = process.env.NODE_ENV || 'sandbox';
if (process.env.NODE_ENV === 'development') {
  configFileName = 'sandbox';
}
const config = require(`./environments/${configFileName}`);

module.exports = () => ({
  ...config,
});
