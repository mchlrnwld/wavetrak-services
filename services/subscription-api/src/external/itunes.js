import fetch from 'isomorphic-fetch';
import { keysToCamel } from '../utils/toCamelCase';

const parseRequest = resp =>
  new Promise(async (resolve, reject) => {
    const body = await resp.json();
    if (resp.status > 200) {
      return reject(body);
    }
    return resolve(body);
  });

const fetchReceipt = receipt =>
  fetch(`${process.env.ITUNES_API}`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
    body: JSON.stringify({
      'receipt-data': receipt,
      password: process.env.APPLE_IAP_PASSWORD,
    }),
  }).then(parseRequest);

export default async receipt => {
  try {
    if (!receipt) {
      throw new Error('Cannot validate purchase without receipt');
    }
    const receiptData = await fetchReceipt(receipt);

    return keysToCamel(receiptData);
  } catch (error) {
    const { status } = error;
    throw new Error(`Receipt Validation Error: ${status}`);
  }
};
