const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

const buildSubscriptionObject = (subscription, stripePlanId) => ({
  type: 'stripe',
  subscriptionId: subscription.id,
  periodStart: new Date(subscription.current_period_start * 1000),
  periodEnd: new Date(subscription.current_period_end * 1000),
  entitlement: subscription.plan.metadata.entitlement || 'sl_default',
  stripePlanId,
  createdAt: new Date(),
  expiring: false,
  onTrial: subscription.status === 'trialing',
  failedPaymentAttempts: 0,
});

/**
 * TODO document the function
 *
 * @param cust
 * @returns {Promise}
 */
export const createCustomer = cust =>
  new Promise((resolve, reject) => {
    stripe.customers.create(
      {
        source: cust.source, // obtained with Stripe.js
        plan: cust.plan,
        email: cust.email,
        metadata: {
          name: `${cust.firstName} ${cust.lastName}`,
          firstName: cust.firstName,
          lastName: cust.lastName,
          userId: cust.userId,
        },
      },
      (err, customer) => {
        if (err) return reject(err);
        return resolve(customer);
      },
    );
  });

/**
 * Retrieves a user's expanced stripe customer account given a stripe customer id.
 * The expanded attributes will return the object value associated with the instead of the
 * default string value.
 *
 * @param customerId | string
 * @param expandedAttrs | array of strings
 * @returns {Promise} stripe customer object
 */
export const getCustomer = (customerId, expandAttrs = []) =>
  new Promise((resolve, reject) => {
    stripe.customers.retrieve(customerId, { expand: expandAttrs }, (err, customer) => {
      if (err) return reject(err);
      return resolve(customer);
    });
  });

/**
 * Retrieves a user's stripe customer account given a user's email.
 *
 * @param email stripe customer id.
 * @returns {Promise} stripe customer object.
 */
export const getCustomerByEmail = email =>
  new Promise((resolve, reject) => {
    stripe.customers.list({ limit: 1, email }, (err, { data }) => {
      if (err) return reject(err);
      return resolve(data[0]);
    });
  });

export const getSubscription = subscriptionId =>
  new Promise((resolve, reject) => {
    stripe.subscriptions.retrieve(subscriptionId, (err, subscription) => {
      if (err) return reject(err);
      return resolve(subscription);
    });
  });

/**
 * Returns a list of subscriptions that have not been canceled for a given customer.
 *
 * @param email stripe customer id.
 * @returns {Promise} Array of Stripe subscription objects.
 */

export const getSubscriptionsByCustomerId = customer =>
  new Promise((resolve, reject) => {
    stripe.subscriptions.list({ customer }, (err, { data }) => {
      if (err) return reject(err);
      return resolve(data);
    });
  });

/**
 * Retrieves a stripe coupon given a valid stripe coupon id
 * @param couponId stripe coupon id
 * @param expandAttrs an array of expanded object attributes. https://stripe.com/docs/api/expanding_objects?lang=node
 * @returns {Promise} stripe coupon object
 */
export const getCoupon = (couponId, expandAttrs = []) =>
  new Promise((resolve, reject) => {
    stripe.coupons.retrieve(couponId, { expand: expandAttrs }, (err, coupon) => {
      if (err) return reject(err);
      return resolve(coupon);
    });
  });

/**
 * Creates a stripe coupon given a valid stripe coupon id
 * @param name stripe coupon id and name
 * @param duration Specifies how long the discount will be in effect.
 * @param percent_off A positive float larger than 0, and smaller or equal to 100, that represents the discount the coupon will apply.
 * @param metadata Set of key-value pairs that you can attach to an object.
 * @param max_redemptions Maximum number of times this coupon can be redeemed, in total, across all customers, before it is no longer valid.
 * @returns {Promise} stripe coupon object
 */
export const createCoupon = ({ name, duration, percentOff, metadata, maxRedemptions }) =>
  new Promise((resolve, reject) => {
    stripe.coupons.create(
      {
        id: name,
        name,
        duration,
        percent_off: percentOff,
        metadata,
        max_redemptions: maxRedemptions,
      },
      (err, coupon) => {
        if (err) return reject(err);
        return resolve(coupon);
      },
    );
  });

/**
 * Removes a stripe coupon given a valid stripe coupon id
 * @param couponId stripe coupon id
 * @returns {Promise} stripe coupon object
 */
export const deleteCoupon = couponId =>
  new Promise((resolve, reject) => {
    stripe.coupons.del(couponId, (err, coupon) => {
      if (err) return reject(err);
      return resolve(coupon);
    });
  });

/**
 * Updates a stripe customer
 *
 * @param customerId
 * @param props
 * @returns {Promise}
 */
export const updateCustomer = (customerId, props) =>
  new Promise((resolve, reject) => {
    stripe.customers.update(customerId, props, (err, customer) => {
      if (err) return reject(err);
      return resolve(customer);
    });
  });

/**
 * Cancels a stripe subscription
 *
 * @param subscriptionId
 * @param options {object} | cancelAtPeriodEnd
 * @returns subscription
 */
export const cancelSubscription = (
  subscriptionId,
  { cancelAtPeriodEnd, metadata } = { cancelAtPeriodEnd: true, metadata: null },
) =>
  new Promise((resolve, reject) => {
    stripe.subscriptions.update(
      subscriptionId,
      { cancel_at_period_end: cancelAtPeriodEnd, metadata },
      (err, subscription) => {
        if (err) reject(err);
        resolve(subscription);
      },
    );
  });

/**
 * Updates a customer's Stripe subscription
 * @param  subscriptionId
 * @param  plan
 * @param {object} options An options object (cancelAtPeriodEnd, trialEndDate)
 * @return {Promise} Resolves to a subscription object that can be added
 *                   to a user's subscriptions []
 */
export const updateSubscription = (
  subscriptionId,
  plan,
  // eslint-disable-next-line camelcase
  { trialEndDate, cancelAtPeriodEnd, proration_behavior, coupon } = {
    trialEndDate: null,
    cancelAtPeriodEnd: null,
    proration_behavior: 'always_invoice',
    coupon: null,
    payment_behavior: 'error_if_incomplete',
  },
) =>
  new Promise((resolve, reject) => {
    const options = {
      plan,
      trial_end: trialEndDate,
      proration_behavior,
      coupon,
    };

    // Only add cancel at period end if it was defined, otherwise stripe will throw an error
    if (cancelAtPeriodEnd !== null) {
      options.cancel_at_period_end = cancelAtPeriodEnd;
    }

    stripe.subscriptions.update(subscriptionId, options, (err, updatedSub) => {
      if (err) return reject(err);
      return resolve(buildSubscriptionObject(updatedSub, plan));
    });
  });

/**
 * Creates a subscription in Stripe
 * https://stripe.com/docs/api/subscriptions/create
 *
 * @typedef {Object} data
 * @property {String} customer
 * @property {String} plan
 * @property {Boolean} trialEligible
 * @property {enum} payment_behavior
 *
 * The payment_behavior options attribute is required so the Subscription is not
 * created in an 'incomplete' state, which is the default.
 *
 * @returns {Promise} Resolves to a Stripe subscription object
 *
 */
export const createSubscription = ({ customer, plan, trialEligible, defaultTaxRates, coupon }) =>
  new Promise((resolve, reject) => {
    const opts = trialEligible ? { trial_period_days: 15 } : { trial_end: 'now' };
    opts.payment_behavior = 'error_if_incomplete';
    opts.default_tax_rates = defaultTaxRates || [];
    opts.coupon = coupon;
    opts.metadata = { zip_check_eligible: '2021-07-20' };
    stripe.subscriptions.create({ customer, plan, ...opts }, (err, subscription) => {
      if (err) return reject(err);
      return resolve(subscription);
    });
  });

/**
 * Creates a subscription in Stripe with Promotion details
 * https://stripe.com/docs/api/subscriptions/create
 *
 * @typedef {Object} data
 * @property {String} customer
 * @property {String} plan
 * @property {Boolean} trialEligible
 * @property {Integer} trialLength
 * @property {String} promotionId
 * @property {String} promotionName
 *
 * @returns {Promise} Resolves to a Stripe subscription object
 *
 */
export const createSubscriptionForPromotion = (
  {
    customer,
    plan,
    trialEligible,
    trialLength,
    promotionId,
    promotionName,
    redeemCode,
    defaultTaxRates,
  },
  coupon = null,
) =>
  new Promise((resolve, reject) => {
    const opts = trialEligible
      ? { trial_period_days: parseInt(trialLength, 10) }
      : { trial_end: 'now' };
    opts.metadata = { promotionId, promotionName, redeemCode, zip_check_eligible: '2021-07-20' };
    opts.coupon = coupon;
    opts.payment_behavior = 'error_if_incomplete';
    opts.default_tax_rates = defaultTaxRates || [];
    stripe.subscriptions.create({ customer, plan, ...opts }, (err, subscription) => {
      if (err) return reject(err);
      return resolve(subscription);
    });
  });

/**
 * ##### DEPRECATED ####
 * Creates a subscription in Stripe for a customer
 *
 * @param customerId
 * @param plan
 * @param freeTrial
 * @param coupon
 * @returns {Promise} Resolves to a subscription object that can be added
 *                    to a user's subscriptions []
 */
export const addSubscription = (
  customerId,
  plan,
  freeTrial = true,
  coupon = null,
  freeTrialLength = null,
  promotionId = null,
  defaultTaxRates = [],
) =>
  new Promise((resolve, reject) => {
    const opts = {
      customer: customerId,
      plan,
      metadata: { zip_check_eligible: '2021-07-20' },
      payment_behavior: 'error_if_incomplete',
      default_tax_rates: defaultTaxRates,
    };
    if (coupon) opts.coupon = coupon;
    if (!freeTrial) opts.trial_end = 'now';
    if (freeTrial && freeTrialLength) opts.trial_period_days = parseInt(freeTrialLength, 10);
    if (promotionId) opts.metadata.promotionId = promotionId;
    stripe.subscriptions.create(opts, (err, subscription) => {
      if (err) return reject(err);
      return resolve(buildSubscriptionObject(subscription, plan));
    });
  });

/**
 * Removes a customer from Stripe's database.
 *
 * @param customerId
 * @returns {Promise}
 */
export const deleteCustomer = customerId =>
  new Promise((resolve, reject) => {
    stripe.customers.del(customerId, (err, confirm) => {
      if (err) return reject(err);
      return resolve(confirm);
    });
  });

/**
 * Removes a subscription given a Stripe customer
 *
 * @param subscriptionId
 * @returns {Promise}
 */
export const removeSubscription = subscriptionId =>
  new Promise((resolve, reject) => {
    stripe.subscriptions.del(subscriptionId, (err, confirmation) => {
      if (err) return reject(err);
      return resolve(confirmation);
    });
  });

/**
 * Returns a list of plans from the Stripe database
 *
 * @returns {Promise}
 */
export const getPlans = () =>
  new Promise((resolve, reject) => {
    stripe.plans.list(
      {
        limit: 100,
      },
      (err, plans) => {
        if (err) return reject(err);
        return resolve(plans);
      },
    );
  });

export const getPlan = planId =>
  new Promise((resolve, reject) => {
    stripe.plans.retrieve(planId, (err, plan) => {
      if (err) return reject(err);
      return resolve(plan);
    });
  });

/**
 * Returns a list of prices from the Stripe database
 *
 * @returns {Promise}
 */
export const getPrices = (opts = {}) =>
  new Promise((resolve, reject) => {
    stripe.prices.list(
      {
        limit: 100,
        ...opts,
      },
      (err, prices) => {
        if (err) return reject(err);
        return resolve(prices);
      },
    );
  });

export const getPrice = priceId =>
  new Promise((resolve, reject) => {
    stripe.prices.retrieve(priceId, (err, price) => {
      if (err) return reject(err);
      return resolve(price);
    });
  });

export const updatePrice = (priceId, opts) =>
  new Promise((resolve, reject) => {
    stripe.prices.update(priceId, { ...opts }, (err, price) => {
      if (err) return reject(err);
      return resolve(price);
    });
  });

/**
 * Retrieve a specific event from stripe.
 *
 * Used for verifying that an event is being sent by Stripe.
 *
 * @param eventId
 * @returns {Promise}
 */
export const retrieveEvent = eventId =>
  new Promise((resolve, reject) => {
    stripe.events.retrieve(eventId, (err, event) => {
      if (err) return reject(err);
      return resolve(event);
    });
  });

/**
 * Adds a new card to a provided Stripe account
 *
 * @param customer the customerId associated with a Stripe account
 * @param source the generated card token to attach to the user account
 * @returns {Promise}
 */
export const addCard = (customer, source) =>
  new Promise((resolve, reject) => {
    stripe.customers.createSource(customer, { source }, (err, card) => {
      if (err) return reject(err);
      return resolve(card);
    });
  });

/**
 * Removes a card from a provided stripe customer
 *
 * @param customer the customer to remove the card from
 * @param source the card to remove from the customer.
 * @returns {Promise}
 */
export const deleteCard = (customer, source) =>
  new Promise((resolve, reject) => {
    stripe.customers.deleteSource(customer, source, (err, confirmation) => {
      if (err) return reject(err);
      return resolve(confirmation);
    });
  });

/**
 * Updates a card from a provided stripe customer
 *
 * @param customer the customer to update the card for
 * @param source the card to update for the customer.
 * @returns {Promise}
 */
export const updateCard = (customer, source, opts) =>
  new Promise((resolve, reject) => {
    stripe.customers.updateSource(customer, source, opts, (err, confirmation) => {
      if (err) return reject(err);
      return resolve(confirmation);
    });
  });

/**
 * Returns a list of invoices for a particular user
 * @param customer
 * @param limit (default set to 10)
 */
export const getInvoices = (customer, limit = 10) =>
  new Promise((resolve, reject) => {
    stripe.invoices.list({ customer, limit }, (err, invoices) => {
      if (err) return reject(err);
      return resolve(invoices);
    });
  });

/**
 * Retrieves the details of a invoice from the Stripe database
 * @param charge
 * @returns {Promise}
 */
export const getInvoiceById = invoice =>
  new Promise((resolve, reject) => {
    stripe.invoices.retrieve(invoice, (err, obj) => {
      if (err) return reject(err);
      return resolve(obj);
    });
  });

/**
 * Retrieves up to 3 invoices for a specific customer and subscription.
 * Excludes $0 invoices for Free Trials.
 * @param customer
 * @param subscription
 *
 * @returns {Promise}
 */
export const countPaidInvoicesBySubscription = ({ customer, subscription }) =>
  new Promise((resolve, reject) => {
    stripe.invoices.list({ customer, subscription }, (err, invoices) => {
      if (err) return reject(err);
      const paidInvoices = invoices.data.filter(
        ({ paid, total, amount_due: amountDue }) => paid === true && total !== 0 && amountDue !== 0,
      );
      return resolve(paidInvoices.length);
    });
  });

/**
 * Returns the upcoming invoice for a particular user
 * @param customer
 * @param subscriptionId
 */
export const getUpcomingInvoice = (customer, subscriptionId, opts) =>
  new Promise((resolve, reject) => {
    stripe.invoices.retrieveUpcoming(
      { customer, subscription: subscriptionId, ...opts },
      (err, invoice) => {
        if (err) return reject(err);
        return resolve(invoice);
      },
    );
  });

/**
 * Iterates over a list of customer invoices and attempts to retry payment
 * on unpaid invoices for active subscriptions.
 *
 * TODO implement invoiceQuery
 *
 * @param customer
 * @param subscriptionsToPay an array of past due subscription ids
 * @param invoiceQuery may be useful for users with many invoices.
 */
export const payInvoices = async (customer, subscriptionsToPay) => {
  const invoices = await getInvoices(customer);

  const doPay = invoiceId =>
    new Promise((resolve, reject) => {
      stripe.invoices.pay(invoiceId, (err, invoice) => {
        if (err) reject(err);
        return resolve(invoice);
      });
    });

  const paidInvoices = [];

  for (const invoice of invoices.data) {
    if (
      !invoice.paid &&
      invoice.status === 'open' &&
      subscriptionsToPay.includes(invoice.subscription)
    ) {
      const result = await doPay(invoice.id);
      paidInvoices.push(result.id);
    }
  }

  return paidInvoices;
};

/**
 * @private
 * Closes an invoice given an invoice ID
 *
 * @param invoiceId the invoice to close.
 */
const closeInvoice = async invoiceId =>
  new Promise((resolve, reject) => {
    stripe.invoices.markUncollectible(invoiceId, (err, invoice) => {
      if (err) return reject(err);
      return resolve(invoice);
    });
  });

/**
 * Closes all open invoices for a matching subscriptionId
 *
 * @param subscriptionId
 * @param customerId
 * @return Array a list of invoices that were closed
 */
export const closeInvoiceBySubscription = async (customerId, subscriptionId) => {
  let invoices = await getInvoices(customerId);
  invoices = invoices.data.filter(
    invoice => invoice.subscription === subscriptionId && invoice.paid === false,
  );

  const closedInvoices = [];
  for (const invoice of invoices) {
    const closed = await closeInvoice(invoice.id);
    closedInvoices.push(closed.id);
  }

  return closedInvoices;
};

/**
 * Finalized a draft invoice given an invoice ID
 *
 * @param invoiceId the invoice to close.
 */
export const finalizeInvoice = async invoiceId =>
  new Promise((resolve, reject) => {
    stripe.invoices.finalizeInvoice(invoiceId, (err, invoice) => {
      if (err) return reject(err);
      return resolve(invoice);
    });
  });

/**
 * Given a source id, will return information about that token.
 *
 * @param source the token information to lookup
 */
export const getToken = source =>
  new Promise((resolve, reject) => {
    stripe.tokens.retrieve(source, (err, obj) => {
      if (err) return reject(err);
      return resolve(obj);
    });
  });

/**
 * Retrives a customer's card. Can be used to validate a card id.
 *
 * @param customer
 * @param source
 * @returns {Promise}
 */
export const getCard = (customer, source) =>
  new Promise((resolve, reject) => {
    stripe.customers.retrieveSource(customer, source, (err, card) => {
      if (err) return reject(err);
      return resolve(card);
    });
  });

/**
 * All stripe dates are returned in unix time in milliseconds. This function
 * serializes a date from stripe into a javascript date suitable for insertion
 * into a mongo document.
 *
 * More information on stripe dates...
 * https://support.stripe.com/questions/what-timezone-does-the-dashboard-and-api-use
 *
 * @param stripeDate date returned from stripe api
 * @returns javascript date object
 */
export const stripeDateToMongo = stripeDate => new Date(stripeDate * 1000);

/**
 * Retrieves the details of a charge from the Stripe database
 * @param charge
 * @returns {Promise}
 */
export const getCharge = charge =>
  new Promise((resolve, reject) => {
    stripe.charges.retrieve(charge, (err, obj) => {
      if (err) return reject(err);
      return resolve(obj);
    });
  });

/**
 * Creates a new charge
 * @param giftProduct
 * @param source - Tokenized credit card
 * @param purchaser - Purchaser Email
 * @param statementDescriptor - Dynamic statement descriptor
 * @returns {Promise}
 */
export const createCharge = (giftProduct, source, purchaser, statementDescriptor, customer) =>
  new Promise((resolve, reject) => {
    const { _id: giftProductId, price, description } = giftProduct;
    stripe.charges.create(
      {
        amount: price,
        currency: 'usd',
        description,
        source,
        statement_descriptor: statementDescriptor,
        customer,
        metadata: {
          giftProductId: giftProductId.toString(),
          purchaser,
        },
      },
      (err, chargeObj) => {
        if (err) return reject(err);
        return resolve(chargeObj);
      },
    );
  });

/**
 * Updates a Stripe Charge
 * @param chargeId
 * @param metadata
 *
 * @return {Promise}
 */
export const updateCharge = (chargeId, metadata) =>
  new Promise((resolve, reject) => {
    stripe.charges.update(
      chargeId,
      {
        metadata: {
          ...metadata,
        },
      },
      (err, chargeObj) => {
        if (err) return reject(err);
        return resolve(chargeObj);
      },
    );
  });

/**
 * Retrieves the invoice detail associated with a single
 * charge.
 *
 * @param charge
 * @returns invoice
 */
export const getInvoiceFromCharge = async charge => {
  const { invoice } = await getCharge(charge);
  return getInvoiceById(invoice);
};

/**
 * Returns a list of your tax rates. Tax rates are returned sorted
 * by creation date, with the most recently created tax rates appearing first.
 *
 * @param limit - optional
 * @returns array of tax rate objects
 *
 */
export const listTaxRates = limit =>
  new Promise((resolve, reject) => {
    stripe.taxRates.list({ limit }, (err, taxRates) => {
      if (err) reject(err);
      const { data } = taxRates;
      resolve(data);
    });
  });

/**
 * Attempts to intiate a Stripe checkout session
 * https://stripe.com/docs/api/checkout/sessions/create
 *
 * @property {string} clientReferenceId - user Id
 * @property {string} cancelUrl - required
 * @property {string} priceId - required
 * @property {string} coupon - optional
 * @property {string} stripeCustomerId - required
 * @property {string} successUrl - required
 * @property {object} metadata - optional
 * @property {object} subscriptionData - optional
 * @property {object} paymentIntentData - optional
 */
export const startCheckoutSession = async ({
  cancelUrl,
  clientReferenceId,
  coupon,
  priceId,
  stripeCustomerId,
  successUrl,
  metadata,
  mode,
  subscriptionData,
  paymentIntentData,
}) => {
  return stripe.checkout.sessions.create({
    // required
    cancel_url: cancelUrl,
    line_items: [
      {
        price: priceId,
        quantity: 1,
      },
    ],
    mode,
    payment_method_types: ['card'],
    success_url: successUrl,
    // optional
    customer: stripeCustomerId,
    client_reference_id: clientReferenceId,
    discounts: [
      {
        coupon: coupon || undefined,
      },
    ],
    subscription_data: subscriptionData ?? undefined,
    payment_intent_data: paymentIntentData ?? undefined,
    metadata,
  });
};

/**
 * Retrieves a checkout session object.
 *
 * @param checkoutSessionId - Checkout session id
 * @param expand - Expansion properties
 */
export const getCheckoutSession = async (checkoutSessionId, expand) =>
  stripe.checkout.sessions.retrieve(checkoutSessionId, { expand });
