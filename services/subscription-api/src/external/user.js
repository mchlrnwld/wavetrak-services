import fetch from 'isomorphic-fetch';

const parseRequest = resp =>
  new Promise(async (resolve, reject) => {
    const contentType = resp.headers.get('Content-Type');
    const body = contentType.indexOf('json') > -1 ? await resp.json() : await resp.text();
    if (resp.status > 200) {
      return reject(body);
    }
    return resolve(body);
  });

export default userId =>
  fetch(`${process.env.USER_SERVICE}/user?id=${userId}`, {
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
    body: JSON.stringify({ id: userId }),
  }).then(parseRequest);

export const updateUser = (userId, stripeCustomerId) =>
  fetch(`${process.env.USER_SERVICE}/user?id=${userId}`, {
    method: 'PUT',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
    body: JSON.stringify({ id: userId, stripeCustomerId }),
  }).then(parseRequest);

/**
 * Until we can refactor the stubbed promises in the Integration test index
 * file, the v2 stipe API will use this function to get user details
 * from the user service.
 *
 * */
export const getUserDetails = userId =>
  fetch(`${process.env.USER_SERVICE}/user?id=${userId}`, {
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
    body: JSON.stringify({ id: userId }),
  }).then(parseRequest);
