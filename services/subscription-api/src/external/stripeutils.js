import { cacheClient } from '../common/dbContext';

const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

export const genTestToken = (number = '4242424242424242') =>
  new Promise((resolve, reject) => {
    stripe.tokens.create(
      {
        card: {
          number,
          exp_month: 12,
          exp_year: 2030,
          cvc: '123',
          address_zip: 92614,
        },
      },
      (err, token) => {
        if (err) return reject(err);
        return resolve(token);
      },
    );
  });

export const cleanupAccount = accountId =>
  new Promise((resolve, reject) => {
    stripe.customers.del(accountId, (err, confirmation) => {
      if (err) return reject(err);
      return resolve(confirmation);
    });
  });

/**
 * Checks (and sets) that a given request has run. This ensures webhook requests
 * can be made idempotent.
 *
 * @param requestId
 * @param run
 */
export const processRequest = requestId =>
  new Promise((resolve, reject) => {
    cacheClient().set(`stripe-event:${requestId}`, true, (err, reply) => {
      cacheClient().expire(`stripe-event:${requestId}`, 604800); // 1 week
      if (err) return reject(err);
      return resolve(reply);
    });
  });

/**
 *
 * @param requestId
 * @returns {Promise}
 */
export const requestDidRun = requestId =>
  new Promise((resolve, reject) => {
    cacheClient().get(`stripe-event:${requestId}`, (err, reply) => {
      if (err) return reject(err);
      return resolve(reply);
    });
  });

/**
 * Checks (and sets) the number of CC validation requests per user per day.  This prevents
 * abuse of the POST /cards endpoint.  Limits request to 10 per user for a 24 hour period.
 *
 * @param userId
 */
export const shouldProcessCardSource = userId =>
  new Promise((resolve, reject) => {
    const rateLimit = parseInt(process.env.STRIPE_SOURCE_RATE_LIMIT, 10);
    const expires = parseInt(process.env.STRIPE_SOURCE_REDIS_TTL, 10);

    cacheClient().get(`sources:${userId}`, (err, reply) => {
      if (err) return reject(err);
      if (!reply) {
        cacheClient().set(`sources:${userId}`, 1, 'EX', expires);
        return resolve();
      }
      if (reply && reply < rateLimit) {
        cacheClient().incr(`sources:${userId}`);
        resolve(reply);
      }
      return reject(new Error({ message: 'Card requests exceeded.', statusCode: 403 }));
    });
  });
