import fetch from 'isomorphic-fetch';

const formatPurchaseApi = ({ packageName, productId, purchaseToken, command }) => {
  if (command) {
    return `https://www.googleapis.com/androidpublisher/v3/applications/${packageName}/purchases/subscriptions/${productId}/tokens/${purchaseToken}:${command}`;
  }
  // eslint-disable-next-line max-len
  return `https://www.googleapis.com/androidpublisher/v3/applications/${packageName}/purchases/subscriptions/${productId}/tokens/${purchaseToken}`;
};

const parseRequest = resp =>
  new Promise(async (resolve, reject) => {
    const body = await resp.json();
    if (resp.status > 200) {
      return reject(body);
    }
    return resolve(body);
  });

const getGoogleAccessToken = () =>
  fetch('https://accounts.google.com/o/oauth2/token', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
    body: JSON.stringify({
      grant_type: 'refresh_token',
      client_id: process.env.PLAY_STORE_API_CLIENT_ID,
      client_secret: process.env.PLAY_STORE_API_CLIENT_SECRET,
      refresh_token: process.env.PLAY_STORE_API_REFRESH_TOKEN,
    }),
  }).then(parseRequest);

const getGoogleSubscriptionData = opts => {
  const computedPurchaseApi = formatPurchaseApi(opts);
  return fetch(computedPurchaseApi, {
    method: 'GET',
    headers: {
      authorization: `Bearer ${opts.accessToken}`,
    },
  }).then(parseRequest);
};

const postGoogleSubscriptionData = opts => {
  const computedPurchaseApi = formatPurchaseApi(opts);

  const { deferralInfo } = opts;
  const bodyParams = opts.deferralInfo ? { deferralInfo } : {};

  return fetch(computedPurchaseApi, {
    method: 'POST',
    headers: {
      authorization: `Bearer ${opts.accessToken}`,
    },
    body: JSON.stringify(bodyParams),
  }).then(parseRequest);
};

const retryGetGoogleSubscriptionData = async (opts, callback) => {
  const { access_token: accessToken } = await getGoogleAccessToken();
  process.env.PLAY_STORE_API_ACCESS_TOKEN = accessToken;

  try {
    const purchaseData = await callback({
      ...opts,
      accessToken: process.env.PLAY_STORE_API_ACCESS_TOKEN,
    });
    return purchaseData;
  } catch (error) {
    if (error.error?.code === 410) {
      const { purchaseToken } = opts;
      return { purchaseToken, expired: true };
    }
    throw error;
  }
};

export default async ({ packageName, purchaseToken, productId }) => {
  const opts = {
    packageName,
    productId,
    purchaseToken,
    accessToken: process.env.PLAY_STORE_API_ACCESS_TOKEN,
  };
  try {
    if (!opts.accessToken) {
      const { access_token: accessToken } = await getGoogleAccessToken();
      process.env.PLAY_STORE_API_ACCESS_TOKEN = accessToken;
      opts.accessToken = accessToken;
    }
    return await getGoogleSubscriptionData(opts);
  } catch (err) {
    const {
      error: { code, message },
    } = err;
    if (code === 401) {
      return retryGetGoogleSubscriptionData(opts, getGoogleSubscriptionData);
    }
    if (code === 410) {
      return { purchaseToken, expired: true };
    }
    throw new Error(`Cannot get purchase data from google play store: ${message}`);
  }
};

export const postPurchaseData = async ({
  packageName,
  purchaseToken,
  productId,
  command,
  deferralInfo = null,
}) => {
  const opts = {
    packageName,
    productId,
    purchaseToken,
    command,
    deferralInfo,
    accessToken: process.env.PLAY_STORE_API_ACCESS_TOKEN,
  };
  try {
    if (!opts.accessToken) {
      const { access_token: accessToken } = await getGoogleAccessToken();
      process.env.PLAY_STORE_API_ACCESS_TOKEN = accessToken;
      opts.accessToken = accessToken;
    }
    return await postGoogleSubscriptionData(opts);
  } catch (err) {
    const {
      error: { code, message },
    } = err;
    if (code === 401) {
      return retryGetGoogleSubscriptionData(opts, postGoogleSubscriptionData);
    }
    if (code === 410) {
      return { purchaseToken, expired: true };
    }
    throw new Error(`Cannot modify purchase data from google play store: ${message}`);
  }
};
