import fetch from 'isomorphic-fetch';

const parseRequest = resp =>
  new Promise(async (resolve, reject) => {
    const contentType = resp.headers.get('Content-Type');
    const body = contentType.indexOf('json') > -1 ? await resp.json() : await resp.text();
    if (resp.status > 200) {
      return reject(body);
    }
    return resolve(body);
  });

export const getPromotion = promotionId =>
  fetch(`${process.env.PROMOTIONS_SERVICE}/${promotionId}`, {
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
  }).then(parseRequest);

export const incrementRedemptionCount = (promotionId, body) =>
  fetch(`${process.env.PROMOTIONS_SERVICE}/${promotionId}/increment`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
    body: JSON.stringify(body),
  }).then(parseRequest);

export const verifyPromoCode = (promotionId, body) =>
  fetch(`${process.env.PROMOTIONS_SERVICE}/${promotionId}/verify-promocode`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
    body: JSON.stringify(body),
  }).then(parseRequest);
