import getSubscriptionTrackingEvent from './utils/getSubscriptionTrackingEvent';
import { identifySubscription } from './identifySubscription';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Checkout Session Completed' event
 * @param {object} subscription The subscription to track
 * @param {object} eventData Event data assoscated with the Checkout event
 */
export const trackCheckoutSessionCompleted = (subscription, eventData) => {
  const { entitlement } = getSubscriptionTrackingEvent(subscription);
  const userId = subscription.user.toString();

  const {
    after_expiration: afterExpiration,
    allow_promotion_codes: allowPromotionCodes,
    amount_subtotal: amountSubtotal,
    amount_total: amountTotal,
    currency,
    customer,
    customer_details: { email: customerEmail },
    locale,
    mode,
    payment_intent: paymentIntent,
    payment_status: paymentStatus,
    recovered_from: recoveredFrom,
    setup_intent: setupIntent,
    success_url: successUrl,
    subscription: subscriptionId,
  } = eventData;
  const properties = {
    afterExpiration,
    allowPromotionCodes,
    amountSubtotal,
    amountTotal,
    currency: currency.toUpperCase(),
    customer,
    customerEmail,
    locale,
    mode,
    paymentIntent,
    paymentStatus,
    recoveredFrom,
    setupIntent,
    successUrl,
    subscription: subscriptionId,
  };

  if (entitlement.includes('sl', 0)) {
    typewriter.checkoutSessionCompleted({
      userId,
      properties,
    });
  } else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Checkout Session Completed',
      userId,
      properties: {
        ...properties,
        entitlement,
      },
    });
  }
  identifySubscription(subscription);
};

export default trackCheckoutSessionCompleted;
