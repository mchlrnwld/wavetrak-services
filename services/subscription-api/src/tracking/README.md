# Subscription Identity and Event Tracking

## Overview
This folder will contain the contracts and functions for each of our Subscription Tracking Plans in Segment.  Use these APIs for broadcasting Subscription lifecycle changes.

Our base v2 Subscription documents schema is defined with consistent naming conventions for most of the properties we are being asked to supply in the Segment Event properties, regardless of the billing platform.  Leveraging the Subscription document in MongoDB as the SOT for these event properties promotes increased consistency between MongoDB and its dependencies and Segment and its dependencies.  Therefore, the new subscription events will be built to accept a MongoDB Subscription object and should be triggered after the Subscription document has been successfully created or modified.

## Tracking Directory
Each tracking event function will exist in it's own file under the `src/tracking` folder.  File naming should represent the name of the event triggered by the default function.  For example:
```
Example:

src
├── tracking
│   └── cancelledFreeTrial.js
│   └── cancelledSubscription.js
│   └── convertedFreeTrial.js
│   └── identify.js
│   └── plans
│     └── sl
│       └── index.js (auto-generated tracking plan)
|   └── startedFreeTrial.js
│   └── startedSubscription.js
│   └── tests
│     └── cancelledFreeTrial.spec.js
│     └── cancelledSubscription.spec.js
│     └── convertedFreeTrial.spec.js
│     └── identity.spec.js
│     └── startedFreeTrial.spec.js
│     └── startedSubscription.spec.js
```
### Identity
Each file/function is responsible for updating the respective properties on a customer's Segment Identity.  The traits included in a Segement Idenity are critical for maintaining consistency across multiple up and downstream integrations and should be tested throughouly.


## Tracking Plan Validation
For this Subscription Event architecture we'll be evaluating Segment's [Typewriter NPM package](https://segment.com/docs/protocols/apis-and-extensions/typewriter/#nodejs-quickstart) for type checking and validation of event traits.

`Typewriter` is a tool for generating strongly-typed Segment analytics libraries based on your pre-defined Tracking Plan spec.

### Typewriter Usage
We use `npx typewriter init` to generate a tracking plan library against the `surfline-web` tracking plan, which is currently located in `/tracking/plans`. To update the plan, you can use the command `npx typewriter`
The API key used for this is found in the parameter: [SEGMENT_TYPEWRITER_KEY](https://us-west-1.console.aws.amazon.com/systems-manager/parameters/prod/common/SEGMENT_TYPEWRITER_KEY/description?region=us-west-1&tab=Table#list_parameter_filters=Name:Contains:SEGMENT)

In addition, `Typewriter` also does runtime validation. If there is a violation during runtime, we can either throw an error or log a warning.

`Typewriter` should be configured with a few relevant options:

```
  typewriter.setTypewriterOptions({
    analytics: slAnalytics, // required - the base analytics
    onViolation: violationHandler // optional - error handling
  });
```

### Caveats
- `typewriter` [only supports track calls.](https://segment.com/docs/protocols/apis-and-extensions/typewriter/#tracking-plan-violation-handling)
- Buoyweather and Fishtrack do not have tracking plans yet, so we should fall back to the base analytics.
- `surfline-web` and `surfline-web-qa` share the same tracking plan so we do not have a "QA" tracking plan available.
- Currently there is no versioning in the tracking plan.

## Example Events

### Stated Free Trail

#### Tracking Plan
| Property Name   | Property Description                                                                      | Data Type | Required |
|-----------------|-------------------------------------------------------------------------------------------|-----------|----------|
| platform        | The platform the event was process for.  values: `web\|ios\|android`                      | string    | required |
| billingPlatform | The billing platform the event was proccessed for.  values: `itunes\|stripe\|google-play` | string    | required |
| planId          | The id of the billing plan                                                                | string    | required |
| trialLength     | Length of the trial in days                                                               | integer   | required |
| subscriptionId  | The unique ID of the subscription used to track the lifecycle from free-trial to paid     | string    | required |
| country         | The country the trialing subscription in associated with                                  | string    | optional |
| trialStartDate  | Start date of the free trial                                                              | date      | required |
| trialEndDate    | End date of the free trial                                                                | date      | required |
| interval        | The billing interval of the subscription plan  values `month\|year`                       | string    | required |

#### Function
```
** Psuedocode **

import { track } from '../common/analytics';
import { parsePlatform, parseBillingPlatform, computeLength, parseInterval } from './utils';
import identify from './identify';

const typewriter = require('./plans/sl');

export default function startedFreeTrial (subscription) => {
  const {
    type,
    user,
    subscriptionId,
    planId,
    trialing,
    start,
    end,
  } = subscription;

  const trackEvent = {
    userId: user.toString(),
    event: 'Started Free Trial',
    properties: {
      platform: parsePlatform(planId),
      billingPlatform: parseBillingPlatform(type),
      planId,
      subscriptionId,
      trailLength: computeLength(start, end),
      trialStartDate: start,
      trialEndDate: end,
      interval: parseInterval(planId)
    }
  }

  if (planId.includes('sl_', 0) || planId.includes('surfline.', 0)) {
    typewriter.startedFreeTrial(trackEvent);
  }
  else {
    track(trackEvent);
  } 
}
```
