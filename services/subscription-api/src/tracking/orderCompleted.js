import { getOrderCompletedTrackingEvent } from './utils/getOrderCompletedTrackingEvent';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Order Completed' event from a receipt
 *
 * @param {object} receipt The receipt from the external third party. This event corresponds to
 * one of these objects depending on the third party:
 * Stripe: https://stripe.com/docs/api/subscriptions/create
 * Apple: https://developer.apple.com/documentation/appstorereceipts/responsebody/latest_receipt_info
 * Google: https://developers.google.com/android-publisher/api-ref/rest/v3/purchases.subscriptions#SubscriptionPurchase
 * @param {object} platform The platform we received the receipt from
 * @param {object} userId userId
 */
export const trackOrderCompleted = (receipt, platform, userId) => {
  const orderCompletedEventProps = getOrderCompletedTrackingEvent(receipt, platform);
  const brand = orderCompletedEventProps?.products[0]?.brand;
  if (brand === 'sl')
    typewriter.orderCompleted({
      userId,
      properties: orderCompletedEventProps,
    });
  else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Order Completed',
      userId,
      properties: { ...orderCompletedEventProps, entitlement: `${brand}_premium` },
    });
  }
};

export default trackOrderCompleted;
