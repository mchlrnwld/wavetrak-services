const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Checkout Session Expired' event
 * @param {object} eventData Event data assoscated with the Checkout event
 */
export const trackCheckoutSessionExpired = eventData => {
  const {
    after_expiration: afterExpiration,
    allow_promotion_codes: allowPromotionCodes,
    amount_subtotal: amountSubtotal,
    amount_total: amountTotal,
    currency,
    customer,
    customer_details: { email: customerEmail },
    locale,
    mode,
    payment_intent: paymentIntent,
    payment_status: paymentStatus,
    recovered_from: recoveredFrom,
    setup_intent: setupIntent,
    success_url: successUrl,
    cancel_url: cancelUrl,
    client_reference_id: clientReferenceId,
    metadata,
  } = eventData;
  const properties = {
    afterExpiration,
    allowPromotionCodes,
    amountSubtotal,
    amountTotal,
    currency: currency.toUpperCase(),
    customer,
    customerEmail,
    locale,
    mode,
    paymentIntent,
    paymentStatus,
    recoveredFrom,
    setupIntent,
    successUrl,
    cancelUrl,
    clientReferenceId,
    metadata,
  };
  const { brand } = metadata;
  if (brand.includes('sl', 0)) {
    typewriter.checkoutSessionExpired({
      userId: clientReferenceId,
      properties,
    });
  } else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Checkout Session Expired',
      userId: clientReferenceId,
      properties: {
        ...properties,
        entitlement: brand,
      },
    });
  }
};

export default trackCheckoutSessionExpired;
