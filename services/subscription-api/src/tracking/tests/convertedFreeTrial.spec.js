import sinon from 'sinon';
import { expect } from 'chai';
import { trackConvertedFreeTrial } from '../convertedFreeTrial';
import * as fixtures from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('convertedFreeTrial', () => {
  beforeEach(() => {
    typewriter.convertedFreeTrial = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.convertedFreeTrial.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send a Converted Free Trial Event', () => {
    trackConvertedFreeTrial(fixtures.googleSubscriptionFixture);
    trackConvertedFreeTrial(fixtures.appleSubscriptionFixture);
    trackConvertedFreeTrial(fixtures.stripeSubscriptionFixture);
    expect(typewriter.convertedFreeTrial).to.have.been.calledThrice();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.convertedFreeTrial).to.have.been.calledWithMatch({
      userId: fixtures.googleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'android',
        country: fixtures.googleSubscriptionFixture.countryCode,
        platform: 'android',
      },
    });
    expect(typewriter.convertedFreeTrial).to.have.been.calledWithMatch({
      userId: fixtures.appleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'ios',
        platform: 'ios',
      },
    });
    expect(typewriter.convertedFreeTrial).to.have.been.calledWithMatch({
      userId: fixtures.stripeSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'stripe',
        platform: 'web',
      },
    });
  });

  it('should use analytics to send a Converted Free Trial event for FS/BW', () => {
    trackConvertedFreeTrial(fixtures.fsSubscriptionFixture);
    trackConvertedFreeTrial(fixtures.bwSubscriptionFixture);
    expect(typewriter.convertedFreeTrial).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledTwice();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Converted Free Trial',
      properties: {
        planId: fixtures.fsSubscriptionFixture.planId,
      },
    });
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Converted Free Trial',
      properties: {
        planId: fixtures.bwSubscriptionFixture.planId,
      },
    });
  });
});
