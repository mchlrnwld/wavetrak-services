import sinon from 'sinon';
import { expect } from 'chai';
import { trackRedeemedGiftCard } from '../redeemedGiftCard';
import * as fixtures from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('redeemedGiftCard', () => {
  beforeEach(() => {
    typewriter.redeemedGiftCard = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.redeemedGiftCard.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send a Redeemed Gift Card Event', () => {
    trackRedeemedGiftCard({ ...fixtures.giftSubscriptionFixture }, 'ABC123');
    expect(typewriter.redeemedGiftCard).to.have.been.calledOnce();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.redeemedGiftCard).to.have.been.calledWithMatch({
      userId: fixtures.giftSubscriptionFixture.user.toHexString(),
      properties: {
        billingPlatform: 'gift',
        platform: 'web',
        planId: 'sl_gift',
        redeemCode: 'ABC123',
      },
    });
  });

  it('should use analytics to send a Redeemed Gift Card event for FS/BW', () => {
    trackRedeemedGiftCard(
      { ...fixtures.giftSubscriptionFixture, planId: 'fs_gift', entitlement: 'fs_premium' },
      'ABC123',
    );
    expect(typewriter.redeemedGiftCard).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledOnce();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Redeemed Gift Card',
      properties: {
        planId: 'fs_gift',
        billingPlatform: 'gift',
        redeemCode: 'ABC123',
      },
    });
  });
  it('should use analytics to send a Redeemed Gift Card event for FS/BW', () => {
    trackRedeemedGiftCard(
      { ...fixtures.giftSubscriptionFixture, planId: 'bw_gift', entitlement: 'bw_premium' },
      'ABC123',
    );
    expect(typewriter.redeemedGiftCard).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledOnce();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Redeemed Gift Card',
      properties: {
        planId: 'bw_gift',
        billingPlatform: 'gift',
        redeemCode: 'ABC123',
      },
    });
  });
});
