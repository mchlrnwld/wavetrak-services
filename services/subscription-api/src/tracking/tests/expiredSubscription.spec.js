import sinon from 'sinon';
import { expect } from 'chai';
import { trackExpiredSubscription } from '../expiredSubscription';
import * as fixtures from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('expiredSubscription', () => {
  beforeEach(() => {
    typewriter.expiredSubscription = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.expiredSubscription.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send a Expired Subscription Event', () => {
    trackExpiredSubscription({ ...fixtures.googleSubscriptionFixture, trialing: false });
    trackExpiredSubscription({ ...fixtures.appleSubscriptionFixture, trialing: false });
    trackExpiredSubscription({ ...fixtures.stripeSubscriptionFixture, trialing: false });
    expect(typewriter.expiredSubscription).to.have.been.calledThrice();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.expiredSubscription).to.have.been.calledWithMatch({
      userId: fixtures.googleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'android',
        country: fixtures.googleSubscriptionFixture.countryCode,
        platform: 'android',
      },
    });
    expect(typewriter.expiredSubscription).to.have.been.calledWithMatch({
      userId: fixtures.googleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'ios',
        platform: 'ios',
      },
    });
    expect(typewriter.expiredSubscription).to.have.been.calledWithMatch({
      userId: fixtures.googleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'stripe',
        platform: 'web',
      },
    });
  });

  it('should use analytics to send a Expired Subscription event for FS/BW', () => {
    trackExpiredSubscription({ ...fixtures.fsSubscriptionFixture, trialing: false });
    trackExpiredSubscription({ ...fixtures.bwSubscriptionFixture, trialing: false });
    expect(typewriter.expiredSubscription).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledTwice();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Expired Subscription',
      properties: {
        planId: fixtures.fsSubscriptionFixture.planId,
      },
    });
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Expired Subscription',
      properties: {
        planId: fixtures.bwSubscriptionFixture.planId,
      },
    });
  });
});
