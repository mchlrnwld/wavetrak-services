import sinon from 'sinon';
import { expect } from 'chai';
import { trackCancelledFreeTrial } from '../cancelledFreeTrial';
import * as fixtures from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('cancelledFreeTrial', () => {
  beforeEach(() => {
    typewriter.cancelledFreeTrial = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.cancelledFreeTrial.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send a Cancelled Free Trial Event', () => {
    trackCancelledFreeTrial(fixtures.googleSubscriptionFixture);
    trackCancelledFreeTrial(fixtures.appleSubscriptionFixture);
    trackCancelledFreeTrial(fixtures.stripeSubscriptionFixture);
    expect(typewriter.cancelledFreeTrial).to.have.been.calledThrice();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.cancelledFreeTrial).to.have.been.calledWithMatch({
      userId: fixtures.googleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'android',
        country: fixtures.googleSubscriptionFixture.countryCode,
        platform: 'android',
      },
    });
    expect(typewriter.cancelledFreeTrial).to.have.been.calledWithMatch({
      userId: fixtures.appleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'ios',
        platform: 'ios',
      },
    });
    expect(typewriter.cancelledFreeTrial).to.have.been.calledWithMatch({
      userId: fixtures.stripeSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'stripe',
        platform: 'web',
      },
    });
  });

  it('should use analytics to send a Cancelled Free Trial event for FS/BW', () => {
    trackCancelledFreeTrial(fixtures.fsSubscriptionFixture);
    trackCancelledFreeTrial(fixtures.bwSubscriptionFixture);
    expect(typewriter.cancelledFreeTrial).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledTwice();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Cancelled Free Trial',
      properties: {
        planId: fixtures.fsSubscriptionFixture.planId,
      },
    });
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Cancelled Free Trial',
      properties: {
        planId: fixtures.bwSubscriptionFixture.planId,
      },
    });
  });
});
