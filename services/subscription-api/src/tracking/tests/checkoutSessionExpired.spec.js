import sinon from 'sinon';
import { expect } from 'chai';
import { trackCheckoutSessionExpired } from '../checkoutSessionExpired';
import * as fixtures from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('checkoutSessionExpired', () => {
  const trackProperties = {
    afterExpiration: null,
    allowPromotionCodes: null,
    amountSubtotal: 0,
    amountTotal: 0,
    currency: 'USD',
    customer: undefined,
    customerEmail: 'ajmn-test-checkout@200.com',
    locale: null,
    mode: 'subscription',
    paymentIntent: null,
    paymentStatus: 'unpaid',
    recoveredFrom: null,
    setupIntent: null,
    successUrl: 'https://sandbox.surfline.com/upgrade',
    cancelUrl: 'https://sandbox.surfline.com/upgrade?selectedInterval=year',
    clientReferenceId: '607f078cd09cdd0013748001',
    metadata: { brand: 'sl', priceId: 'sl_monthly_v2' },
  };
  beforeEach(() => {
    typewriter.checkoutSessionExpired = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.checkoutSessionExpired.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send a Checkout Session Expired Event', () => {
    const userId = fixtures.checkoutSessionExpiredFixture.client_reference_id;
    trackCheckoutSessionExpired(fixtures.checkoutSessionExpiredFixture);
    expect(typewriter.checkoutSessionExpired).to.have.been.calledOnce();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.checkoutSessionExpired).to.have.been.calledWithMatch({
      userId,
      properties: trackProperties,
    });
  });

  it('should use analytics to send a Checkout Session Expired event for FS', () => {
    trackCheckoutSessionExpired({
      ...fixtures.checkoutSessionExpiredFixture,
      metadata: {
        brand: 'fs',
        priceId: 'fs_monthly_v2',
      },
    });
    const userId = fixtures.checkoutSessionExpiredFixture.client_reference_id;
    expect(typewriter.checkoutSessionExpired).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledOnce();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Checkout Session Expired',
      properties: {
        ...trackProperties,
        metadata: {
          brand: 'fs',
          priceId: 'fs_monthly_v2',
        },
        entitlement: 'fs',
      },
      userId,
    });
  });
  it('should use analytics to send a Checkout Session Expired event for BW', () => {
    trackCheckoutSessionExpired({
      ...fixtures.checkoutSessionExpiredFixture,
      metadata: {
        brand: 'bw',
        priceId: 'bw_monthly_v2',
      },
    });
    const userId = fixtures.checkoutSessionExpiredFixture.client_reference_id;
    expect(typewriter.checkoutSessionExpired).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledOnce();
    const [{ event }] = analyticsStub.firstCall.args;
    expect(event).to.equal('Checkout Session Expired');
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Checkout Session Expired',
      properties: {
        ...trackProperties,
        metadata: {
          brand: 'bw',
          priceId: 'bw_monthly_v2',
        },
        entitlement: 'bw',
      },
      userId,
    });
  });
});
