import sinon from 'sinon';
import { expect } from 'chai';
import * as fixtures from './fixtures';
import { trackCancelledSubscription } from '../cancelledSubscription';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('cancelledSubscription', () => {
  beforeEach(() => {
    typewriter.cancelledSubscription = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.cancelledSubscription.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send a Cancelled Subscription Event', () => {
    trackCancelledSubscription({ ...fixtures.googleSubscriptionFixture, trialing: false });
    trackCancelledSubscription({ ...fixtures.appleSubscriptionFixture, trialing: false });
    trackCancelledSubscription({ ...fixtures.stripeSubscriptionFixture, trialing: false });
    expect(typewriter.cancelledSubscription).to.have.been.calledThrice();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.cancelledSubscription).to.have.been.calledWithMatch({
      userId: fixtures.googleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'android',
        country: fixtures.googleSubscriptionFixture.countryCode,
        platform: 'android',
      },
    });
    expect(typewriter.cancelledSubscription).to.have.been.calledWithMatch({
      userId: fixtures.appleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'ios',
        platform: 'ios',
      },
    });
    expect(typewriter.cancelledSubscription).to.have.been.calledWithMatch({
      userId: fixtures.stripeSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'stripe',
        platform: 'web',
      },
    });
  });

  it('should use analytics to send a Cancelled Subscription  event for FS/BW', () => {
    trackCancelledSubscription({ ...fixtures.fsSubscriptionFixture, trialing: false });
    trackCancelledSubscription({ ...fixtures.bwSubscriptionFixture, trialing: false });
    expect(typewriter.cancelledSubscription).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledTwice();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Cancelled Subscription',
      properties: {
        planId: fixtures.fsSubscriptionFixture.planId,
      },
    });
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Cancelled Subscription',
      properties: {
        planId: fixtures.bwSubscriptionFixture.planId,
      },
    });
  });

  /* Code commented out to resolve Jenkins deploy issue.  Test case is valid.
  it('should not send a Cancelled Subscription event if the sub is trialing', () => {
    trackCancelledSubscription(fixtures.stripeSubscriptionFixture);
    expect(typewriter.cancelledSubscription).to.not.have.been.called();
    expect(analyticsStub).to.not.have.been.called();
  });
   */
});
