import mongoose from 'mongoose';

export const baseSubscriptionFixture = {
  active: true,
  entitlement: 'sl_premium',
  expiring: false,
  _id: mongoose.Types.ObjectId('5f208b39a31c6f0090e7971b'),
  subscriptionId: 'abcdef123456',
  start: 1530115742,
  end: new Date() / 1000 + 999,
  planId: 'surfline.android.1year.subscription.notrial',
  trialing: true,
  type: 'google',
  user: mongoose.Types.ObjectId('5f208b39a31c6f0090e7971b'),
};

export const nonTrialSubscriptionFixture = {
  ...baseSubscriptionFixture,
  trialing: false,
};

export const expiringSubscriptionFixture = {
  ...baseSubscriptionFixture,
  expiring: true,
};

export const stripeSubscriptionFixture = {
  ...baseSubscriptionFixture,
  stripeCustomerId: 'cus_8gz1QHB6jBcmlz',
  type: 'stripe',
  planId: 'sl_annual_v2',
};

export const googleSubscriptionFixture = {
  ...baseSubscriptionFixture,
  baseReceipt: mongoose.Types.ObjectId('5e8240284a33cc2c133cc2ae'),
  countryCode: 'US',
  orderId: 'GPA.3346-2677-0259-05939..18',
  packageName: 'com.surfline.android',
  planId: 'surfline.android.1year.subscription.notrial',
  type: 'google',
};

export const appleSubscriptionFixture = {
  ...baseSubscriptionFixture,
  originalTransactionId: '150000857230256',
  transactionId: '150000857230257',
  baseReceipt: mongoose.Types.ObjectId('5e8240284a33cc2c133cc2ae'),
  planId: 'surfline.iosapp.1year.subscription.notrial',
  type: 'apple',
};

export const giftSubscriptionFixture = {
  ...baseSubscriptionFixture,
  type: 'gift',
  planId: 'sl_gift',
  billingPlatform: 'gift',
};

export const fsSubscriptionFixture = {
  ...baseSubscriptionFixture,
  entitlement: 'fs_premium',
  planId: 'fishtrack.android.1month.subscription',
};

export const bwSubscriptionFixture = {
  ...baseSubscriptionFixture,
  entitlement: 'bw_premium',
  planId: 'buoyweather.android.1month.subscription',
};

export const stripeInvoiceRenewalFixture = {
  planId: 'sl_annual_v2',
  invoiceRenewalDate: 1630958978,
  newInvoiceAmount: 95.88,
  platform: 'web',
  entitlement: 'sl_premium',
};

export const fsStripeInvoiceRenewalFixture = {
  planId: 'fs_annual_v2',
  invoiceRenewalDate: 1630958978,
  newInvoiceAmount: 95.88,
  platform: 'web',
  entitlement: 'fs_premium',
};

export const bwStripeInvoiceRenewalFixture = {
  planId: 'bw_annual_v2',
  invoiceRenewalDate: 1630958978,
  newInvoiceAmount: 95.88,
  platform: 'web',
  entitlement: 'bw_premium',
};

export const giftPurchaseFixture = {
  amount: 999,
  currency: 'usd',
  purchaserEmail: 'purchaser@wavetrak.com',
  purchaserName: 'Purchaser',
  recipientEmail: 'recipient@wavetrak.com',
  recipientName: 'Recipient',
};

export const giftCodeFixture = {
  platform: 'web',
  userId: mongoose.Types.ObjectId('5f208b39a31c6f0090e7971b'),
  purchaserEmail: 'purchaser@wavetrak.com',
  purchaserName: 'Purchaser',
  purchaseDate: 'Fri Sep 04 2021 09:33:57 GMT-0500 (Central Daylight Time)',
  redeemCode: 'ABC123',
  duration: 30,
  department: 'Engineering',
  campaign: 'Testing',
};

export const daysToConvertFromTrialFixture = {
  planId: 'sl_annual_v2',
  daysToPremium: 7,
  cardType: 'visa',
  platform: 'web',
  user: mongoose.Types.ObjectId('5f208b39a31c6f0090e7971b'),
  entitlement: 'sl_premium',
};

export const checkoutSessionCompletedFixture = {
  id: 'cs_test_a1ti6jp2OpXDy6ozeAWDLBCspbB9VRGGz2Th9UmaUky88AY5qr3uYhd780',
  object: 'checkout.session',
  after_expiration: null,
  allow_promotion_codes: null,
  amount_subtotal: 0,
  amount_total: 0,
  automatic_tax: {
    enabled: false,
    status: null,
  },
  billing_address_collection: null,
  cancel_url: 'https://sandbox.surfline.com/upgrade?selectedInterval=year',
  client_reference_id: '61784c22fc0e910012380dcb',
  consent: null,
  consent_collection: null,
  currency: 'usd',
  customer: 'cus_KTsMR9qbEpElXU',
  customer_details: {
    email: 'ajmn-test-checkout@200.com',
    phone: null,
    tax_exempt: 'none',
    tax_ids: [],
  },
  customer_email: null,
  expires_at: 1635360166,
  livemode: false,
  locale: null,
  metadata: {},
  mode: 'subscription',
  payment_intent: null,
  payment_method_options: {},
  payment_method_types: ['card'],
  payment_status: 'paid',
  phone_number_collection: {
    enabled: false,
  },
  recovered_from: null,
  setup_intent: 'seti_1JoucHCUgh9bySt68FMTSKg7',
  shipping: null,
  shipping_address_collection: null,
  submit_type: null,
  subscription: 'sub_1JoucKCUgh9bySt6JJX6TNC0',
  success_url: 'https://sandbox.surfline.com/onboarding',
  total_details: {
    amount_discount: 0,
    amount_shipping: 0,
    amount_tax: 0,
  },
  url: null,
};

export const checkoutSessionExpiredFixture = {
  id: 'cs_test_a1M4PwLAYXiNm4sVt4PkGd3b8zjcuvwNxmh7uaDZrc0Nc67AwF2aqIBGY9',
  object: 'checkout.session',
  after_expiration: null,
  allow_promotion_codes: null,
  amount_subtotal: 0,
  amount_total: 0,
  automatic_tax: {
    enabled: false,
    status: null,
  },
  billing_address_collection: null,
  cancel_url: 'https://sandbox.surfline.com/upgrade?selectedInterval=year',
  client_reference_id: '607f078cd09cdd0013748001',
  consent: null,
  consent_collection: null,
  currency: 'usd',
  customer_details: {
    email: 'ajmn-test-checkout@200.com',
    phone: null,
    tax_exempt: null,
    tax_ids: null,
  },
  customer_email: null,
  expires_at: 1635545341,
  livemode: false,
  locale: null,
  metadata: {
    brand: 'sl',
    priceId: 'sl_monthly_v2',
  },
  mode: 'subscription',
  payment_intent: null,
  payment_method_options: null,
  payment_method_types: ['card'],
  payment_status: 'unpaid',
  phone_number_collection: {
    enabled: false,
  },
  recovered_from: null,
  setup_intent: null,
  shipping: null,
  shipping_address_collection: null,
  submit_type: null,
  subscription: null,
  success_url: 'https://sandbox.surfline.com/upgrade',
  total_details: {
    amount_discount: 0,
    amount_shipping: 0,
    amount_tax: 0,
  },
  url: null,
};
