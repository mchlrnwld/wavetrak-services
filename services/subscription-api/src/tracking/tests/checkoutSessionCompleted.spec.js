import sinon from 'sinon';
import { expect } from 'chai';
import { trackCheckoutSessionCompleted } from '../checkoutSessionCompleted';
import * as fixtures from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('checkoutSessionCompleted', () => {
  const trackProperties = {
    afterExpiration: null,
    allowPromotionCodes: null,
    amountSubtotal: 0,
    amountTotal: 0,
    currency: 'USD',
    customer: 'cus_KTsMR9qbEpElXU',
    customerEmail: 'ajmn-test-checkout@200.com',
    locale: null,
    mode: 'subscription',
    paymentIntent: null,
    paymentStatus: 'paid',
    recoveredFrom: null,
    setupIntent: 'seti_1JoucHCUgh9bySt68FMTSKg7',
    successUrl: 'https://sandbox.surfline.com/onboarding',
    subscription: 'sub_1JoucKCUgh9bySt6JJX6TNC0',
  };
  beforeEach(() => {
    typewriter.checkoutSessionCompleted = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.checkoutSessionCompleted.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send a Checkout Session Completed Event', () => {
    trackCheckoutSessionCompleted(
      fixtures.stripeSubscriptionFixture,
      fixtures.checkoutSessionCompletedFixture,
    );
    expect(typewriter.checkoutSessionCompleted).to.have.been.calledOnce();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.checkoutSessionCompleted).to.have.been.calledWithMatch({
      userId: fixtures.stripeSubscriptionFixture.user.toHexString(),
      properties: trackProperties,
    });
  });

  it('should use analytics to send a Checkout Session Completed event for FS', () => {
    trackCheckoutSessionCompleted(
      fixtures.fsSubscriptionFixture,
      fixtures.checkoutSessionCompletedFixture,
    );
    expect(typewriter.checkoutSessionCompleted).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledOnce();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Checkout Session Completed',
      properties: {
        ...trackProperties,
        entitlement: 'fs_premium',
      },
    });
  });
  it('should use analytics to send a Checkout Session Completed event for BW', () => {
    trackCheckoutSessionCompleted(
      fixtures.bwSubscriptionFixture,
      fixtures.checkoutSessionCompletedFixture,
    );
    expect(typewriter.checkoutSessionCompleted).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledOnce();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Checkout Session Completed',
      properties: {
        ...trackProperties,
        entitlement: 'bw_premium',
      },
    });
  });
});
