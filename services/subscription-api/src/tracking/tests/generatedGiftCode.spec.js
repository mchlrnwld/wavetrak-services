import sinon from 'sinon';
import { expect } from 'chai';
import { trackGeneratedGiftCode } from '../generatedGiftCode';
import * as fixtures from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('generatedGiftCode', () => {
  beforeEach(() => {
    typewriter.generatedGiftCard = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.generatedGiftCard.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send a Purchased Gift Code Event', () => {
    trackGeneratedGiftCode({ ...fixtures.giftCodeFixture, planId: 'sl_gift', brand: 'sl' });
    expect(typewriter.generatedGiftCard).to.have.been.calledOnce();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.generatedGiftCard).to.have.been.calledWith({
      userId: fixtures.giftCodeFixture.userId.toString(),
      anonymousId: null,
      properties: {
        brand: 'sl',
        planId: 'sl_gift',
        platform: 'web',
        purchaserEmail: 'purchaser@wavetrak.com',
        purchaserName: 'Purchaser',
        purchaseDate: 'Fri Sep 04 2021 09:33:57 GMT-0500 (Central Daylight Time)',
        redeemCode: 'ABC123',
        duration: 30,
        department: 'Engineering',
        campaign: 'Testing',
        userId: fixtures.giftCodeFixture.userId.toString(),
      },
    });
  });

  it('should use analytics to send a Purchased Gift Code event for FS/BW', () => {
    trackGeneratedGiftCode({ ...fixtures.giftCodeFixture, planId: 'fs_gift', brand: 'fs' });
    trackGeneratedGiftCode({ ...fixtures.giftCodeFixture, planId: 'bw_gift', brand: 'bw' });
    expect(typewriter.generatedGiftCard).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledTwice();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Generated Gift Card',
      properties: {
        planId: 'fs_gift',
        platform: 'web',
      },
    });
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Generated Gift Card',
      properties: {
        planId: 'bw_gift',
        platform: 'web',
      },
    });
  });
});
