import sinon from 'sinon';
import { expect } from 'chai';
import { trackDaysToConvertFromTrial } from '../daysToConvertFromTrial';
import { daysToConvertFromTrialFixture } from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('daysToConvertFromTrial', () => {
  beforeEach(() => {
    typewriter.daysToConvertFromTrial = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.daysToConvertFromTrial.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send an Days To Convert From Trial Event', () => {
    trackDaysToConvertFromTrial({
      ...daysToConvertFromTrialFixture,
    });
    expect(typewriter.daysToConvertFromTrial).to.have.been.calledOnce();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.daysToConvertFromTrial).to.have.been.calledWithMatch({
      userId: daysToConvertFromTrialFixture.user.toString(),
      properties: {
        planId: 'sl_annual_v2',
        daysToPremium: 7,
        platform: 'web',
      },
    });
  });

  it('should use analytics to send a Days To Convert From Trial event for FS/BW', () => {
    trackDaysToConvertFromTrial({
      ...daysToConvertFromTrialFixture,
      planId: 'fs_annual_v2',
      entitlement: 'fs_premium',
    });
    trackDaysToConvertFromTrial({
      ...daysToConvertFromTrialFixture,
      planId: 'bw_annual_v2',
      entitlement: 'bw_premium',
    });
    expect(typewriter.daysToConvertFromTrial).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledTwice();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Days To Convert From Trial',
      properties: {
        planId: 'fs_annual_v2',
      },
    });
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Days To Convert From Trial',
      properties: {
        planId: 'bw_annual_v2',
      },
    });
  });
});
