import sinon from 'sinon';
import { expect } from 'chai';
import { trackChangedSubscriptionPlan } from '../changedSubscriptionPlan';
import * as fixtures from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('changedSubscriptionPlan', () => {
  beforeEach(() => {
    typewriter.changedSubscriptionPlan = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.changedSubscriptionPlan.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send a Changed Subscription Plan Event', () => {
    trackChangedSubscriptionPlan({ ...fixtures.googleSubscriptionFixture, trialing: false });
    trackChangedSubscriptionPlan({ ...fixtures.appleSubscriptionFixture, trialing: false });
    trackChangedSubscriptionPlan({ ...fixtures.stripeSubscriptionFixture, trialing: false });
    expect(typewriter.changedSubscriptionPlan).to.have.been.calledThrice();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.changedSubscriptionPlan).to.have.been.calledWithMatch({
      userId: fixtures.googleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'android',
        country: fixtures.googleSubscriptionFixture.countryCode,
        platform: 'android',
      },
    });
    expect(typewriter.changedSubscriptionPlan).to.have.been.calledWithMatch({
      userId: fixtures.appleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'ios',
        platform: 'ios',
      },
    });
    expect(typewriter.changedSubscriptionPlan).to.have.been.calledWithMatch({
      userId: fixtures.stripeSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'stripe',
        platform: 'web',
      },
    });
  });

  it('should use analytics to send a Changed Subscription Plan event for FS', () => {
    trackChangedSubscriptionPlan({ ...fixtures.fsSubscriptionFixture, trialing: false });
    expect(typewriter.changedSubscriptionPlan).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledOnce();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Changed Subscription Plan',
      properties: {
        entitlement: fixtures.fsSubscriptionFixture.entitlement,
      },
    });
  });
  it('should use analytics to send a Changed Subscription Plan event for BW', () => {
    trackChangedSubscriptionPlan({ ...fixtures.bwSubscriptionFixture, trialing: false });
    expect(typewriter.changedSubscriptionPlan).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledOnce();
    const [
      {
        properties: { entitlement },
        event,
      },
    ] = analyticsStub.firstCall.args;
    expect(event).to.equal('Changed Subscription Plan');
    expect(entitlement).to.equal('bw_premium');
  });
});
