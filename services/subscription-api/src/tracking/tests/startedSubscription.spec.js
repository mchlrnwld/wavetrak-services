import sinon from 'sinon';
import { expect } from 'chai';
import { trackStartedSubscription } from '../startedSubscription';
import * as fixtures from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('startedSubscription', () => {
  beforeEach(() => {
    typewriter.startedSubscription = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.startedSubscription.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send a Started Subscription Event', () => {
    trackStartedSubscription({ ...fixtures.googleSubscriptionFixture, trialing: false });
    trackStartedSubscription({ ...fixtures.appleSubscriptionFixture, trialing: false });
    trackStartedSubscription({ ...fixtures.stripeSubscriptionFixture, trialing: false });
    expect(typewriter.startedSubscription).to.have.been.calledThrice();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.startedSubscription).to.have.been.calledWithMatch({
      userId: fixtures.googleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'android',
        country: fixtures.googleSubscriptionFixture.countryCode,
        platform: 'android',
      },
    });
    expect(typewriter.startedSubscription).to.have.been.calledWithMatch({
      userId: fixtures.appleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'ios',
        platform: 'ios',
      },
    });
    expect(typewriter.startedSubscription).to.have.been.calledWithMatch({
      userId: fixtures.stripeSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'stripe',
        platform: 'web',
      },
    });
  });

  it('should use analytics to send a Started Direct Subscription event for FS/BW', () => {
    trackStartedSubscription({ ...fixtures.fsSubscriptionFixture, trialing: false });
    trackStartedSubscription({ ...fixtures.bwSubscriptionFixture, trialing: false });
    expect(typewriter.startedSubscription).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledTwice();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Started Subscription',
      properties: {
        planId: fixtures.fsSubscriptionFixture.planId,
      },
    });
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Started Subscription',
      properties: {
        planId: fixtures.bwSubscriptionFixture.planId,
      },
    });
  });
});
