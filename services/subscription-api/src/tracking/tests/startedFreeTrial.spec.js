import sinon from 'sinon';
import { expect } from 'chai';
import { trackStartedFreeTrial } from '../startedFreeTrial';
import * as fixtures from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let typewriterStub;
let analyticsStub;

describe('startedFreeTrial', () => {
  beforeEach(() => {
    typewriterStub = sinon.stub(typewriter, 'startedFreeTrial');
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriterStub.restore();
    analyticsStub.restore();
  });

  it('should use typewriter to send a Started Free Trial Event', () => {
    trackStartedFreeTrial(fixtures.googleSubscriptionFixture);
    trackStartedFreeTrial(fixtures.appleSubscriptionFixture);
    trackStartedFreeTrial(fixtures.stripeSubscriptionFixture);
    expect(typewriterStub).to.have.been.calledThrice();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriterStub).to.have.been.calledWithMatch({
      userId: fixtures.googleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'android',
        country: fixtures.googleSubscriptionFixture.countryCode,
        platform: 'android',
      },
    });
    expect(typewriterStub).to.have.been.calledWithMatch({
      userId: fixtures.appleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'ios',
        platform: 'ios',
      },
    });
    expect(typewriterStub).to.have.been.calledWithMatch({
      userId: fixtures.stripeSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'stripe',
        platform: 'web',
      },
    });
  });

  it('should use analytics to send a Started Free Trial event for FS/BW', () => {
    trackStartedFreeTrial(fixtures.fsSubscriptionFixture);
    trackStartedFreeTrial(fixtures.bwSubscriptionFixture);
    expect(typewriterStub).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledTwice();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Started Free Trial',
      properties: {
        planId: fixtures.fsSubscriptionFixture.planId,
      },
    });
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Started Free Trial',
      properties: {
        planId: fixtures.bwSubscriptionFixture.planId,
      },
    });
  });
});
