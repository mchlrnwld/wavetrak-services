import sinon from 'sinon';
import { expect } from 'chai';
import { trackPaymentFailed } from '../paymentFailed';
import * as fixtures from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('paymentFailed', () => {
  beforeEach(() => {
    typewriter.failedPayment = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.failedPayment.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send a Payment Failed Event', () => {
    trackPaymentFailed({ ...fixtures.googleSubscriptionFixture, trialing: false });
    trackPaymentFailed({ ...fixtures.appleSubscriptionFixture, trialing: false });
    trackPaymentFailed({ ...fixtures.stripeSubscriptionFixture, trialing: false });
    expect(typewriter.failedPayment).to.have.been.calledThrice();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.failedPayment).to.have.been.calledWithMatch({
      userId: fixtures.googleSubscriptionFixture.user.toHexString(),
      properties: {
        billingPlatform: 'android',
        country: fixtures.googleSubscriptionFixture.countryCode,
        platform: 'android',
      },
    });
    expect(typewriter.failedPayment).to.have.been.calledWithMatch({
      userId: fixtures.appleSubscriptionFixture.user.toHexString(),
      properties: {
        billingPlatform: 'ios',
        platform: 'ios',
      },
    });
    expect(typewriter.failedPayment).to.have.been.calledWithMatch({
      userId: fixtures.googleSubscriptionFixture.user.toHexString(),
      properties: {
        billingPlatform: 'stripe',
        platform: 'web',
      },
    });
  });

  it('should use analytics to send a Payment Failed event for FS/BW', () => {
    trackPaymentFailed({ ...fixtures.fsSubscriptionFixture, trialing: false });
    trackPaymentFailed({ ...fixtures.bwSubscriptionFixture, trialing: false });
    expect(typewriter.failedPayment).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledTwice();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Failed Payment',
      properties: {
        planId: fixtures.fsSubscriptionFixture.planId,
      },
    });
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Failed Payment',
      properties: {
        planId: fixtures.bwSubscriptionFixture.planId,
      },
    });
  });
});
