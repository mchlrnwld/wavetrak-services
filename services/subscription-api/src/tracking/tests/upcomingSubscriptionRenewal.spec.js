import sinon from 'sinon';
import { expect } from 'chai';
import { trackUpcomingSubscriptionRenewal } from '../upcomingSubscriptionRenewal';
import * as fixtures from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('upcomingSubscriptionRenewal', () => {
  beforeEach(() => {
    typewriter.upcomingSubscriptionRenewal = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.upcomingSubscriptionRenewal.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send an Upcoming Subscription Renewal Event', () => {
    trackUpcomingSubscriptionRenewal(
      { ...fixtures.stripeSubscriptionFixture },
      { ...fixtures.stripeInvoiceRenewalFixture },
    );
    expect(typewriter.upcomingSubscriptionRenewal).to.have.been.calledOnce();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.upcomingSubscriptionRenewal).to.have.been.calledWithMatch({
      userId: fixtures.stripeSubscriptionFixture.user.toString(),
      properties: {
        planId: 'sl_annual_v2',
        platform: 'web',
      },
    });
  });

  it('should use analytics to send a Upcoming Subscription Renewal event for FS/BW', () => {
    trackUpcomingSubscriptionRenewal(
      { ...fixtures.fsSubscriptionFixture },
      { ...fixtures.fsStripeInvoiceRenewalFixture },
    );
    trackUpcomingSubscriptionRenewal(
      { ...fixtures.bwSubscriptionFixture },
      { ...fixtures.bwStripeInvoiceRenewalFixture },
    );
    expect(typewriter.upcomingSubscriptionRenewal).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledTwice();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Upcoming Subscription Renewal',
      properties: {
        planId: fixtures.fsStripeInvoiceRenewalFixture.planId,
      },
    });
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Upcoming Subscription Renewal',
      properties: {
        planId: fixtures.fsStripeInvoiceRenewalFixture.planId,
      },
    });
  });
});
