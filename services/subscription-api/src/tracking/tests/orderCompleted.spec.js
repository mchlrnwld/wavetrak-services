import sinon from 'sinon';
import { expect } from 'chai';
import { getOrderCompletedTrackingEvent } from '../utils/getOrderCompletedTrackingEvent';
import { trackOrderCompleted } from '../orderCompleted';

import itunesReceiptFixture from '../../__stubs__/itunes/INITIAL_BUY';
import googleReceiptFixture from '../../__stubs__/google/BASE_PURCHASE_DATA';
import stripeReceiptFixture from '../../__stubs__/stripe/customer.subscription.created';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;
let orderCompletedStub;
const userId = '5a208b39a31c6f0090e7971b';

describe('orderCompleted', () => {
  beforeEach(() => {
    orderCompletedStub = sinon.stub(typewriter, 'orderCompleted');
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    orderCompletedStub.restore();
    analyticsStub.restore();
  });

  describe('should use typewriter to send an Order Completed Event for SL', () => {
    it('for Apple', () => {
      const {
        unified_receipt: { latest_receipt_info: latestReceiptInfo },
      } = itunesReceiptFixture({
        planId: 'sl_plan123.annual',
        transactionId: 'transactionId',
      });
      const receiptFixture = latestReceiptInfo[0];
      trackOrderCompleted(receiptFixture, 'apple', userId);
      expect(orderCompletedStub).to.have.been.calledOnce();
      expect(analyticsStub).to.not.have.been.called();
      expect(orderCompletedStub).to.have.been.calledWithMatch({
        userId,
        properties: getOrderCompletedTrackingEvent(receiptFixture, 'apple'),
      });
    });
    it('for Google', () => {
      const receiptFixture = {
        productId: 'sl_plan123.annual',
        data: googleReceiptFixture(),
      };
      trackOrderCompleted(receiptFixture, 'google', userId);
      expect(orderCompletedStub).to.have.been.calledOnce();
      expect(analyticsStub).to.not.have.been.called();
      expect(orderCompletedStub).to.have.been.calledWithMatch({
        userId,
        properties: getOrderCompletedTrackingEvent(receiptFixture, 'google'),
      });
    });
    it('for Stripe', () => {
      const {
        data: { object: receiptFixture },
      } = stripeReceiptFixture({ customerId: 'customerId', subscriptionId: 'id123' });
      trackOrderCompleted(receiptFixture, 'stripe', userId);
      expect(orderCompletedStub).to.have.been.calledOnce();
      expect(analyticsStub).to.not.have.been.called();
      expect(orderCompletedStub).to.have.been.calledWithMatch({
        userId,
        properties: getOrderCompletedTrackingEvent(receiptFixture, 'stripe'),
      });
    });
  });

  describe('should use analytics to send an Order Completed event for FS/BW', () => {
    it('for Apple', () => {
      const {
        unified_receipt: { latest_receipt_info: latestReceiptInfo },
      } = itunesReceiptFixture({
        planId: 'bw_plan123.annual',
        transactionId: 'transactionId',
      });
      const receiptFixture = latestReceiptInfo[0];
      trackOrderCompleted(receiptFixture, 'apple', userId);
      expect(orderCompletedStub).to.not.have.been.called();
      expect(analyticsStub).to.have.been.calledOnce();
      expect(analyticsStub).to.have.been.calledWithMatch({
        userId,
        properties: getOrderCompletedTrackingEvent(receiptFixture, 'apple'),
      });
    });
    it('for Google', () => {
      const receiptFixture = {
        productId: 'ft_plan123.annual',
        data: googleReceiptFixture(),
      };
      trackOrderCompleted(receiptFixture, 'google', userId);
      expect(orderCompletedStub).to.not.have.been.called();
      expect(analyticsStub).to.have.been.calledOnce();
      expect(analyticsStub).to.have.been.calledWithMatch({
        userId,
        properties: getOrderCompletedTrackingEvent(receiptFixture, 'google'),
      });
    });
    it('for Stripe', () => {
      const {
        data: { object: baseReceiptFixture },
      } = stripeReceiptFixture({ customerId: 'customerId', subscriptionId: 'id123' });

      baseReceiptFixture.items.data[0].price.id = 'bw_annual_v2';

      trackOrderCompleted(baseReceiptFixture, 'stripe', userId);
      expect(orderCompletedStub).to.not.have.been.called();
      expect(analyticsStub).to.have.been.calledOnce();
      expect(analyticsStub).to.have.been.calledWithMatch({
        userId,
        properties: getOrderCompletedTrackingEvent(baseReceiptFixture, 'stripe'),
      });
    });
  });
});
