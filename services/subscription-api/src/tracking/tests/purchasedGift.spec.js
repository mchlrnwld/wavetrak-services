import sinon from 'sinon';
import { expect } from 'chai';
import { trackPurchasedGift } from '../purchasedGift';
import * as fixtures from './fixtures';
import * as customerio from '../../common/customerio';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;
let customerioStub;

describe('purchasedGift', () => {
  beforeEach(() => {
    typewriter.purchasedGiftCard = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
    customerioStub = sinon.stub(customerio, 'default');
  });

  afterEach(() => {
    typewriter.purchasedGiftCard.reset();
    analyticsStub.restore();
    customerioStub.restore();
  });

  it('should use typewriter to send a Purchased Gift Code Event', () => {
    trackPurchasedGift(fixtures.giftPurchaseFixture, { brand: 'sl' });
    expect(typewriter.purchasedGiftCard).to.have.been.calledOnce();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.purchasedGiftCard).to.have.been.calledWithMatch({
      properties: {
        amount: 999,
        purchaserEmail: 'purchaser@wavetrak.com',
        purchaserName: 'Purchaser',
        recipientEmail: 'recipient@wavetrak.com',
        recipientName: 'Recipient',
      },
    });
  });

  it('should use analytics to send a Purchased Gift Code event for FS/BW', () => {
    trackPurchasedGift(fixtures.giftPurchaseFixture, { brand: 'fs' });
    trackPurchasedGift(fixtures.giftPurchaseFixture, { brand: 'bw' });
    expect(typewriter.purchasedGiftCard).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledTwice();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Purchased Gift Card',
      properties: {
        amount: 999,
        purchaserEmail: 'purchaser@wavetrak.com',
        purchaserName: 'Purchaser',
        recipientEmail: 'recipient@wavetrak.com',
        recipientName: 'Recipient',
      },
    });
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Purchased Gift Card',
      properties: {
        amount: 999,
        purchaserEmail: 'purchaser@wavetrak.com',
        purchaserName: 'Purchaser',
        recipientEmail: 'recipient@wavetrak.com',
        recipientName: 'Recipient',
      },
    });
  });
});
