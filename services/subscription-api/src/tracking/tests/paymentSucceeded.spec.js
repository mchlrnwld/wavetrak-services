import sinon from 'sinon';
import { expect } from 'chai';
import { trackPaymentSucceeded } from '../paymentSucceeded';
import * as fixtures from './fixtures';

const typewriter = require('../plans/sl');
const analytics = require('../../common/analytics');

let analyticsStub;

describe('paymentSucceeded', () => {
  beforeEach(() => {
    typewriter.succeededPayment = sinon.stub();
    analyticsStub = sinon.stub(analytics, 'track');
  });

  afterEach(() => {
    typewriter.succeededPayment.reset();
    analyticsStub.restore();
  });

  it('should use typewriter to send a Payment Succeeded Event', () => {
    trackPaymentSucceeded({ ...fixtures.googleSubscriptionFixture, trialing: false });
    trackPaymentSucceeded({ ...fixtures.appleSubscriptionFixture, trialing: false });
    trackPaymentSucceeded({ ...fixtures.stripeSubscriptionFixture, trialing: false });
    expect(typewriter.succeededPayment).to.have.been.calledThrice();
    expect(analyticsStub).to.not.have.been.called();
    expect(typewriter.succeededPayment).to.have.been.calledWithMatch({
      userId: fixtures.googleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'android',
        country: fixtures.googleSubscriptionFixture.countryCode,
        platform: 'android',
      },
    });
    expect(typewriter.succeededPayment).to.have.been.calledWithMatch({
      userId: fixtures.appleSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'ios',
        platform: 'ios',
      },
    });
    expect(typewriter.succeededPayment).to.have.been.calledWithMatch({
      userId: fixtures.stripeSubscriptionFixture.user.toString(),
      properties: {
        billingPlatform: 'stripe',
        platform: 'web',
      },
    });
  });

  it('should use analytics to send a Payment Succeeded event for FS/BW', () => {
    trackPaymentSucceeded({ ...fixtures.fsSubscriptionFixture, trialing: false });
    trackPaymentSucceeded({ ...fixtures.bwSubscriptionFixture, trialing: false });
    expect(typewriter.succeededPayment).to.not.have.been.called();
    expect(analyticsStub).to.have.been.calledTwice();
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Succeeded Payment',
      properties: {
        planId: fixtures.fsSubscriptionFixture.planId,
      },
    });
    expect(analyticsStub).to.have.been.calledWithMatch({
      event: 'Succeeded Payment',
      properties: {
        planId: fixtures.bwSubscriptionFixture.planId,
      },
    });
  });
});
