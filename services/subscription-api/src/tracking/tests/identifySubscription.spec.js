import sinon from 'sinon';
import { expect } from 'chai';
import { identifySubscription } from '../identifySubscription';
import * as fixtures from './fixtures';

const analytics = require('../../common/analytics');

describe('identifySubscription', () => {
  let analyticsStub;

  beforeEach(() => {
    analyticsStub = sinon.stub(analytics, 'identify');
  });

  afterEach(() => {
    analyticsStub.restore();
  });

  it('calls identify with relevant subscription properties', () => {
    identifySubscription(fixtures.stripeSubscriptionFixture);
    expect(analyticsStub).to.have.been.called.calledOnceWithExactly({
      userId: fixtures.stripeSubscriptionFixture.user.toString(),
      traits: {
        'subscription.entitlement': fixtures.stripeSubscriptionFixture.entitlement,
        'subscription.in_trial': fixtures.stripeSubscriptionFixture.trialing,
        'subscription.interval': 'year',
        'subscription.plan_id': fixtures.stripeSubscriptionFixture.planId,
      },
    });
  });
});
