import getSubscriptionTrackingEvent from './utils/getSubscriptionTrackingEvent';
import { identifySubscription } from './identifySubscription';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Started Subscription' event
 * @param {object} subscription The new subscription to track
 */
export const trackStartedSubscription = (subscription, event) => {
  const subscriptionTrackEvent = getSubscriptionTrackingEvent(subscription, event);

  const { entitlement } = subscriptionTrackEvent;
  const userId = subscription.user.toString();

  if (entitlement.includes('sl', 0))
    typewriter.startedSubscription({
      userId,
      properties: { ...subscriptionTrackEvent, active: true },
    });
  else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Started Subscription',
      userId,
      properties: subscriptionTrackEvent,
    });
  }
  identifySubscription(subscription);
};

export default trackStartedSubscription;
