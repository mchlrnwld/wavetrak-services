import getSubscriptionTrackingEvent from './utils/getSubscriptionTrackingEvent';
import { identifySubscription } from './identifySubscription';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a segment 'Started Free Trial' event
 * @param {object} subscription - The trialing subscription to track
 */
export const trackStartedFreeTrial = subscription => {
  const subscriptionTrackEvent = getSubscriptionTrackingEvent(subscription);

  const userId = subscription.user.toString();
  const { entitlement } = subscriptionTrackEvent;

  if (entitlement.includes('sl', 0))
    typewriter.startedFreeTrial({
      userId,
      properties: { ...subscriptionTrackEvent, active: true },
    });
  else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Started Free Trial',
      userId,
      properties: subscriptionTrackEvent,
    });
  }
  identifySubscription(subscription);
};

export default trackStartedFreeTrial;
