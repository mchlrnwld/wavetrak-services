import { slAnalytics } from '../common/analytics';
// eslint-disable-next-line global-require
export const typewriter = require('./plans/sl');

export { trackStartedFreeTrial } from './startedFreeTrial';
export { trackCancelledFreeTrial } from './cancelledFreeTrial';
export { trackConvertedFreeTrial } from './convertedFreeTrial';

export { trackStartedSubscription } from './startedSubscription';
export { trackExpiredSubscription } from './expiredSubscription';
export { trackCancelledSubscription } from './cancelledSubscription';

export { trackChangedSubscriptionPlan } from './changedSubscriptionPlan';
export { trackPaymentSucceeded } from './paymentSucceeded';
export { trackPaymentFailed } from './paymentFailed';
export { trackUpcomingSubscriptionRenewal } from './upcomingSubscriptionRenewal';
export { trackDaysToConvertFromTrial } from './daysToConvertFromTrial';

export { trackGeneratedGiftCode } from './generatedGiftCode';
export { trackPurchasedGift } from './purchasedGift';
export { trackRedeemedGiftCard } from './redeemedGiftCard';
export { identifySubscription } from './identifySubscription';

export { trackOrderCompleted } from './orderCompleted';

export { trackCheckoutSessionCompleted } from './checkoutSessionCompleted';
export { trackCheckoutSessionExpired } from './checkoutSessionExpired';

typewriter.setTypewriterOptions({
  analytics: slAnalytics,
});
