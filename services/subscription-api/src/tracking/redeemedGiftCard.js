import getSubscriptionTrackingEvent from './utils/getSubscriptionTrackingEvent';
import { identifySubscription } from './identifySubscription';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Redeemed Gift Card' event
 * @param {object} subscription The new subscription to track
 */
export const trackRedeemedGiftCard = (subscription, redeemCode) => {
  const subscriptionTrackEvent = getSubscriptionTrackingEvent(subscription);

  const { planId, billingPlatform, platform, entitlement } = subscriptionTrackEvent;

  const userId = subscription.user.toString();
  const trackingProperties = {
    planId,
    redeemCode,
    billingPlatform,
    platform,
    entitlement,
  };
  if (entitlement.includes('sl', 0))
    typewriter.redeemedGiftCard({
      userId,
      properties: { ...trackingProperties },
    });
  else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Redeemed Gift Card',
      userId,
      properties: {
        ...trackingProperties,
      },
    });
  }
  identifySubscription(subscription);
};

export default trackRedeemedGiftCard;
