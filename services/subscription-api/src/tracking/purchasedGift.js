import uuidv4 from 'uuid/v4';
import customerio from '../common/customerio';
import { currencySymbolMap } from '../utils/getCurrencyCode';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Gift Code Purchased' event
 * @param {object} purchaseData The details of the gift purchase
 * @param {object} opts Additional data relevant to the tracking environment
 */
export const trackPurchasedGift = (purchaseData, opts) => {
  const {
    amount,
    currency,
    purchaserEmail,
    purchaserName,
    recipientEmail,
    recipientName,
    message,
    priceId,
    planId,
    description,
    redeemCode,
    userId,
  } = purchaseData;

  const trackingProperties = {
    amount,
    currency,
    purchaserEmail,
    purchaserName,
    recipientEmail,
    recipientName,
    message,
    priceId,
    planId,
    description,
    redeemCode,
    platform: 'web',
    purchaseDate: Date.now(),
  };

  const anonymousId = !userId ? uuidv4() : null;
  const currencySymbol = currencySymbolMap[currency.toUpperCase()];
  const { brand } = opts;
  customerio({
    name: 'Purchased Gift Card',
    data: {
      ...purchaseData,
      amount: `${currencySymbol}${amount / 100}`,
      recipient: purchaserEmail,
      recipientEmail: recipientEmail || purchaserEmail,
      recipientName: recipientName || purchaserName,
    },
    brand,
  });
  customerio({
    name: 'Send Gift Code',
    data: {
      ...purchaseData,
      recipient: recipientEmail || purchaserEmail,
      recipientEmail: recipientEmail || purchaserEmail,
      recipientName: recipientName || purchaserName,
    },
    brand,
  });
  if (brand === 'sl') {
    typewriter.purchasedGiftCard({
      anonymousId,
      userId,
      properties: trackingProperties,
    });
  } else {
    analytics.track({
      event: 'Purchased Gift Card',
      anonymousId,
      userId,
      properties: trackingProperties,
    });
  }
};

export default trackPurchasedGift;
