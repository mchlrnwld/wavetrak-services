import getSubscriptionTrackingEvent from './utils/getSubscriptionTrackingEvent';
import { identifySubscription } from './identifySubscription';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Cancelled Free Trial' event
 * @param {object} subscription The cancelled subscription to track
 */
export const trackCancelledFreeTrial = subscription => {
  const subscriptionTrackEvent = getSubscriptionTrackingEvent(subscription);

  const { entitlement, billingPlatform } = subscriptionTrackEvent;
  const userId = subscription.user.toString();

  if (entitlement.includes('sl', 0))
    typewriter.cancelledFreeTrial({
      userId,
      properties: { ...subscriptionTrackEvent },
    });
  else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Cancelled Free Trial',
      userId,
      properties: subscriptionTrackEvent,
    });
  }
  // eslint-disable-next-line no-param-reassign
  subscription.trialing = billingPlatform !== 'stripe';
  identifySubscription(subscription);
};

export default trackCancelledFreeTrial;
