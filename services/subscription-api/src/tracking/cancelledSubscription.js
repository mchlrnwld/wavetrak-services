import getSubscriptionTrackingEvent from './utils/getSubscriptionTrackingEvent';
import { identifySubscription } from './identifySubscription';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Cancelled Subscription' event
 * @param {object} subscription The cancelled subscription to track
 */
export const trackCancelledSubscription = subscription => {
  const subscriptionTrackEvent = getSubscriptionTrackingEvent(subscription);
  const { entitlement, inTrial } = subscriptionTrackEvent;
  if (inTrial) return;
  if (entitlement.includes('sl', 0))
    typewriter.cancelledSubscription({
      userId: subscription.user.toString(),
      properties: {
        ...subscriptionTrackEvent,
      },
    });
  else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Cancelled Subscription',
      userId: subscription.user.toString(),
      properties: subscriptionTrackEvent,
    });
  }
  identifySubscription(subscription);
};

export default trackCancelledSubscription;
