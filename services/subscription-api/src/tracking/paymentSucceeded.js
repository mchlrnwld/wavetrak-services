import getSubscriptionTrackingEvent from './utils/getSubscriptionTrackingEvent';
import { identifySubscription } from './identifySubscription';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Payment Succeeded' event
 * @param {object} subscription The updated subscription to track
 */
export const trackPaymentSucceeded = subscription => {
  const { currency, revenue } = subscription;
  const subscriptionTrackEvent = getSubscriptionTrackingEvent(subscription);

  const { planId, entitlement, interval, intervalCount } = subscriptionTrackEvent;
  const userId = subscription.user.toString();

  const modifiedCurrency = currency ? currency.toUpperCase() : currency;

  if (entitlement.includes('sl', 0))
    typewriter.succeededPayment({
      userId,
      properties: { ...subscriptionTrackEvent, currency: modifiedCurrency, revenue },
    });
  else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Succeeded Payment',
      userId,
      properties: { planId, entitlement, interval, intervalCount },
    });
  }
  identifySubscription(subscription);
};

export default trackPaymentSucceeded;
