import * as analytics from '../common/analytics';
import getPriceKey from '../utils/getPriceKey';

/**
 * This function updates the segment identity subscription properties.
 * @param {object} subscription The v2 subscription to identify
 */
export const identifySubscription = subscription => {
  const { trialing, entitlement, planId, user, active, type } = subscription;
  const priceKey = type === 'stripe' ? getPriceKey(planId) : planId;
  const brand = entitlement.substring(0, 2);

  const subscriptionTraits = {
    'subscription.entitlement': active ? entitlement : `${brand}_default`,
    'subscription.in_trial': trialing,
    'subscription.interval': priceKey?.toLowerCase().includes('month') ? 'month' : 'year',
    'subscription.plan_id': planId,
  };
  analytics.identify({
    userId: user.toString(),
    traits: subscriptionTraits,
  });
};

export default identifySubscription;
