import getSubscriptionTrackingEvent from './utils/getSubscriptionTrackingEvent';
import { identifySubscription } from './identifySubscription';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Expired Subscription' event
 * @param {object} subscription The expired subscription to track
 */
export const trackExpiredSubscription = subscription => {
  const subscriptionTrackEvent = getSubscriptionTrackingEvent(subscription);

  const { entitlement } = subscriptionTrackEvent;
  const userId = subscription.user.toString();

  if (entitlement.includes('sl', 0))
    typewriter.expiredSubscription({
      userId,
      properties: { ...subscriptionTrackEvent },
    });
  else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Expired Subscription',
      userId,
      properties: subscriptionTrackEvent,
    });
  }
  // eslint-disable-next-line no-param-reassign
  subscription.trialing = false;
  identifySubscription(subscription);
};

export default trackExpiredSubscription;
