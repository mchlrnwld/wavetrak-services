import getSubscriptionTrackingEvent from './utils/getSubscriptionTrackingEvent';
import { identifySubscription } from './identifySubscription';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Upcoming Subscription Renewal' event
 * @param {object} subscription The updated subscription to track
 * @param {object} renewalData Invoice data pertaining to the renewal
 */
export const trackUpcomingSubscriptionRenewal = (subscription, renewalData) => {
  const subscriptionTrackEvent = getSubscriptionTrackingEvent(subscription);

  const { entitlement } = subscriptionTrackEvent;
  const userId = subscription.user.toString();

  if (entitlement.includes('sl', 0))
    typewriter.upcomingSubscriptionRenewal({
      userId,
      properties: { ...subscriptionTrackEvent, ...renewalData },
    });
  else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Upcoming Subscription Renewal',
      userId,
      properties: renewalData,
    });
  }
  identifySubscription(subscription);
};

export default trackUpcomingSubscriptionRenewal;
