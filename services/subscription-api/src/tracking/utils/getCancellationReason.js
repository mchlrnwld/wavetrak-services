// https://developers.google.com/android-publisher/api-ref/rest/v3/purchases.subscriptions
const googleCancellationReasons = {
  0: 'Other',
  1: 'I do not use this service enough',
  2: 'Technical issues',
  3: 'Cost-related reasons',
  4: 'I found a better app',
};

const appleCancellationReasons = {
  '0': 'uknown',
  '1': 'customer canceled their transaction due to an actual or perceived issue within your app.',
};

const getCancellationReason = subscription => {
  const { type, cancellationReason } = subscription;
  if (type === 'google') {
    return googleCancellationReasons.cancellationReason;
  }
  if (type === 'apple') {
    return appleCancellationReasons[cancellationReason];
  }
  return null;
};

export default getCancellationReason;
