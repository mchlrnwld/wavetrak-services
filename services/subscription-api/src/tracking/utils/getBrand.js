/**
 * Returns the correct Brand (SL, FS, BW) by parsing the plan
 * @param {*} plan
 */
export const getBrand = planId => {
  const plan = planId.toLowerCase();
  if (plan.includes('sl_', 0) || plan.includes('surfline.', 0)) {
    return 'sl';
  }
  if (
    plan.includes('bw_', 0) ||
    plan.includes('bw.', 0) ||
    plan.includes('BW', 0) ||
    plan.includes('buoyweather.', 0)
  ) {
    return 'bw';
  }

  if (
    plan.includes('fs_', 0) ||
    plan.includes('fs.', 0) ||
    plan.includes('FS', 0) ||
    plan.includes('fishtrack.', 0)
  ) {
    return 'fs';
  }
  return null;
};

export default getBrand;
