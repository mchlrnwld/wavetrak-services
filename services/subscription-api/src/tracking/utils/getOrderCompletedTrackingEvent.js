import getBillingPlatform from './getBillingPlatform';
import { getBrand } from './getBrand';
import getPlatform from './getPlatform';
import getPriceKey from '../../utils/getPriceKey';

/**
 * Gets the Order Completion event props relevant to the given subscription type
 * @param {*} receipt
 * @param {*} type
 * @returns
 */
const getOrderCompletedMappedProps = (receipt, type) => {
  switch (type) {
    case 'apple':
      return {
        coupon: receipt.promotional_offer_id,
        name: null,
        orderId: receipt.transaction_id,
        productId: receipt.product_id,
        trial: !!receipt.is_trial_period,
      };
    case 'google':
      return {
        coupon: receipt.data.promotionCode,
        name: null,
        orderId: receipt.data.orderId,
        productId: receipt.productId,
        trial: receipt.data.paymentState === 2,
      };
    case 'stripe':
      return {
        coupon: receipt.discount?.coupon?.id,
        name: receipt.items?.data[0]?.price?.nickname,
        orderId: receipt.id,
        productId: receipt.items?.data[0]?.price?.id,
        trial: receipt.status === 'trialing',
      };
    default:
      return null;
  }
};

/**
 * Builds a Segment-friendly object to be submitted as event properties, based on the receipt
 * data for the given subscription type.
 * @param {*} receipt
 * @param {*} type
 * @returns
 */
export const getOrderCompletedTrackingEvent = (receipt, type) => {
  const { coupon, name, orderId, productId, trial } = getOrderCompletedMappedProps(receipt, type);
  const priceMapKey = (getPriceKey(productId) || productId).toLowerCase();

  const orderCompletionProps = {
    billing_platform: getBillingPlatform(type),
    coupon,
    interval: priceMapKey.includes('month') ? 'month' : 'year',
    order_id: orderId,
    platform: getPlatform(type),
    trial,
  };

  if (!coupon) delete orderCompletionProps.coupon;

  const productProps = {
    brand: getBrand(priceMapKey),
    category: 'Subscription',
    name,
    quantity: 1,
    product_id: productId,
    sku: productId,
  };

  if (!name) delete productProps.name;

  return {
    ...orderCompletionProps,
    products: [
      {
        ...productProps,
      },
    ],
  };
};

export default getOrderCompletedTrackingEvent;
