import { expect } from 'chai';
import { getOrderCompletedTrackingEvent } from './getOrderCompletedTrackingEvent';
import itunesReceiptFixture from '../../__stubs__/itunes/INITIAL_BUY';
import googleReceiptFixture from '../../__stubs__/google/BASE_PURCHASE_DATA';
import stripeReceiptFixture from '../../__stubs__/stripe/customer.subscription.created';

describe('getOrderCompletedTrackingEvent', () => {
  describe('should return a trackingEvent based on receipt properties', () => {
    it('for Apple', () => {
      const {
        unified_receipt: { latest_receipt_info: receiptFixture },
      } = itunesReceiptFixture({
        planId: 'sl_plan123.annual',
        transactionId: 'transactionId',
      });
      const orderCompletedTrackingEvent = getOrderCompletedTrackingEvent(
        receiptFixture[0],
        'apple',
      );
      expect(orderCompletedTrackingEvent).to.deep.equal({
        billing_platform: 'ios',
        interval: 'year',
        order_id: receiptFixture[0].transaction_id,
        platform: 'ios',
        products: [
          {
            brand: 'sl',
            category: 'Subscription',
            product_id: 'sl_plan123.annual',
            quantity: 1,
            sku: 'sl_plan123.annual',
          },
        ],
        trial: true,
      });
    });

    it('for Google', () => {
      const receiptFixture = {
        productId: 'sl_plan123.annual',
        data: googleReceiptFixture(),
      };
      const orderCompletedTrackingEvent = getOrderCompletedTrackingEvent(receiptFixture, 'google');
      expect(orderCompletedTrackingEvent).to.deep.equal({
        billing_platform: 'android',
        interval: 'year',
        order_id: receiptFixture.data.orderId,
        platform: 'android',
        products: [
          {
            brand: 'sl',
            category: 'Subscription',
            product_id: 'sl_plan123.annual',
            quantity: 1,
            sku: 'sl_plan123.annual',
          },
        ],
        trial: false,
      });
    });

    it('for Stripe', () => {
      const {
        data: { object: receiptFixture },
      } = stripeReceiptFixture(
        { customerId: 'customerId', subscriptionId: 'id123' },
        { status: 'trialing' },
      );
      const orderCompletedTrackingEvent = getOrderCompletedTrackingEvent(receiptFixture, 'stripe');
      expect(orderCompletedTrackingEvent).to.deep.equal({
        billing_platform: 'stripe',
        interval: 'year',
        order_id: 'id123',
        platform: 'web',
        products: [
          {
            brand: 'sl',
            category: 'Subscription',
            name: 'Surfline Premium Annual',
            product_id: 'price_1KMDTZLwFGcBvXawjCayM4YC',
            quantity: 1,
            sku: 'price_1KMDTZLwFGcBvXawjCayM4YC',
          },
        ],
        trial: true,
      });
    });
  });
});
