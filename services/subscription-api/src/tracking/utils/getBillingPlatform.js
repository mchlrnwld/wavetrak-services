/**
 * Returns the billing platform based on subscription type
 * @param {string} type Subscription type
 * @returns
 */
const getBillingPlatform = type => {
  switch (type) {
    case 'apple':
      return 'ios';
    case 'google':
      return 'android';
    case 'gift':
      return 'gift';
    default:
      return 'stripe';
  }
};

export default getBillingPlatform;
