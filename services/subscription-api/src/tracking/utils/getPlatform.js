/**
 * Returns the platform of the event based on subscription type
 * @param {string} type Subscription type
 * @returns
 */
const getPlatform = type => {
  switch (type) {
    case 'apple':
      return 'ios';
    case 'google':
      return 'android';
    default:
      return 'web';
  }
};

export default getPlatform;
