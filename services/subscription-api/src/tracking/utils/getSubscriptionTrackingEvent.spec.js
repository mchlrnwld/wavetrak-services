import { expect } from 'chai';
import mongoose from 'mongoose';
import getSubscriptionTrackingEvent from './getSubscriptionTrackingEvent';

describe('getSubscriptionTrackingEvent', () => {
  const start = Math.floor(new Date() / 1000);
  const end = Math.floor(new Date() / 1000) + 86400;

  const baseSubscriptionFixture = {
    active: true,
    entitlement: 'sl_premium',
    _id: mongoose.Types.ObjectId('5f208b39a31c6f0090e7971b'),
    start,
    end,
    planId: 'surfline.android.1year.subscription.notrial',
    trialing: false,
    type: 'google',
    user: mongoose.Types.ObjectId('5f208b39a31c6f0090e7971b'),
    subscriptionId: '5f208b39a31c6f0090e7971b',
  };

  const trialingSubscriptionFixture = {
    active: true,
    entitlement: 'sl_premium',
    _id: mongoose.Types.ObjectId('5f208b39a31c6f0090e7971a'),
    start,
    end,
    planId: 'surfline.android.1year.subscription.notrial',
    trialing: true,
    type: 'google',
    user: mongoose.Types.ObjectId('5f208b39a31c6f0090e7971b'),
    subscriptionId: '5f208b39a31c6f0090e7971b',
  };

  it('should return a trackingEvent based on subscription properties', () => {
    const subscriptionTrackingEvent = getSubscriptionTrackingEvent(baseSubscriptionFixture);
    expect(subscriptionTrackingEvent).to.deep.equal({
      active: true,
      billingPlatform: 'android',
      country: undefined,
      currentPeriodEnd: new Date(trialingSubscriptionFixture.end * 1000).toISOString(),
      currentPeriodStart: new Date(trialingSubscriptionFixture.start * 1000).toISOString(),
      daysUntilRenew: 0,
      entitlement: 'sl_premium',
      expiration: new Date(baseSubscriptionFixture.end * 1000).toISOString(),
      interval: 'year',
      intervalCount: 1,
      inTrial: false,
      planId: 'surfline.android.1year.subscription.notrial',
      platform: 'android',
      promotionId: undefined,

      subscriptionId: '5f208b39a31c6f0090e7971b',
      trialEligible: false,
    });
  });

  it('should add trialing props if the subscription is trialing', () => {
    const subscriptionTrackingEvent = getSubscriptionTrackingEvent(trialingSubscriptionFixture);
    expect(subscriptionTrackingEvent).to.deep.equal({
      active: true,
      billingPlatform: 'android',
      country: undefined,
      currentPeriodEnd: new Date(trialingSubscriptionFixture.end * 1000).toISOString(),
      currentPeriodStart: new Date(trialingSubscriptionFixture.start * 1000).toISOString(),
      daysUntilRenew: 0,
      entitlement: 'sl_premium',
      expiration: new Date(trialingSubscriptionFixture.end * 1000).toISOString(),
      interval: 'year',
      intervalCount: 1,
      inTrial: true,
      planId: 'surfline.android.1year.subscription.notrial',
      platform: 'android',
      promotionId: undefined,
      secondsRemaining: subscriptionTrackingEvent.secondsRemaining,
      subscriptionId: '5f208b39a31c6f0090e7971b',
      trialEligible: false,
      trialEndDate: new Date(trialingSubscriptionFixture.end * 1000).toISOString(),
      trialLength: 1,
      trialStartDate: new Date(trialingSubscriptionFixture.start * 1000).toISOString(),
    });
    expect(end - start).to.be.closeTo(subscriptionTrackingEvent.secondsRemaining, 20);
  });
});
