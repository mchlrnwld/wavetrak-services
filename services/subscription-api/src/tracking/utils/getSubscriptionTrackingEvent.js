import getBillingPlatform from './getBillingPlatform';
import getPlatform from './getPlatform';
import getCancellationReason from './getCancellationReason';
import getPriceKey from '../../utils/getPriceKey';

/**
 * This function will extract data from the parameters and map it to a Segment-friendly object.
 * @param {object} subscription - The v2 Subscription object to track.
 * @param {object} event - An optional associated event with additional parameters not included in
 * the subscription.
 * @returns {object} an object with designated subscription tracking properties relevant to segment
 */
const getSubscriptionTrackingEvent = (subscription, event = null) => {
  const {
    active,
    countryCode: country,
    cancellationReason,
    end,
    planId,
    stripePlanId,
    promotionId,
    start,
    subscriptionId,
    trialing,
    type,
  } = subscription;

  let { entitlement } = subscription;
  if (end < new Date() / 1000) {
    const brand = entitlement.substring(0, 2);
    entitlement = `${brand}_default`;
  }

  const plan = planId || stripePlanId;
  const priceKey = getPriceKey(planId);
  const monthly = planId.includes('month') || priceKey?.toLowerCase().includes('month');

  const subscriptionProps = {
    active,
    billingPlatform: getBillingPlatform(type),
    country, // Android specific
    currentPeriodStart: new Date(start * 1000).toISOString(),
    currentPeriodEnd: new Date(end * 1000).toISOString(),
    daysUntilRenew: Math.floor((end - new Date() / 1000) / 86400),
    entitlement,
    expiration: new Date(end * 1000).toISOString(),
    interval: monthly ? 'month' : 'year',
    intervalCount: 1,
    inTrial: trialing || false,
    planId: plan,
    platform: getPlatform(type),
    promotionId: promotionId ? promotionId.toString() : promotionId,
    subscriptionId,
    trialEligible: false,
  };

  const cancelProps = cancellationReason ? { reason: getCancellationReason(subscription) } : {};

  const trialProps = trialing && {
    secondsRemaining: Math.floor(end - new Date() / 1000),
    trialEndDate: new Date(end * 1000).toISOString(),
    trialLength: Math.floor((end - start) / 86400), // # of Days
    trialStartDate: new Date(start * 1000).toISOString(),
  };

  const extraProps = event && {
    discount: event?.discount?.coupon?.id, // Stripe specific
  };

  return {
    ...extraProps,
    ...subscriptionProps,
    ...trialProps,
    ...cancelProps,
  };
};

export default getSubscriptionTrackingEvent;
