import getSubscriptionTrackingEvent from './utils/getSubscriptionTrackingEvent';
import { identifySubscription } from './identifySubscription';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Payment Failed' event
 * @param {object} subscription The updated subscription to track
 */
export const trackPaymentFailed = subscription => {
  const subscriptionTrackEvent = getSubscriptionTrackingEvent(subscription);

  const { planId, entitlement, interval, intervalCount } = subscriptionTrackEvent;
  const userId = subscription.user.toString();

  if (entitlement.includes('sl', 0))
    typewriter.failedPayment({
      userId,
      properties: { ...subscriptionTrackEvent },
    });
  else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Failed Payment',
      userId,
      properties: { planId, entitlement, interval, intervalCount },
    });
  }
  identifySubscription(subscription);
};

export default trackPaymentFailed;
