import getSubscriptionTrackingEvent from './utils/getSubscriptionTrackingEvent';
import { identifySubscription } from './identifySubscription';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Changed Subscription Plan' event
 * @param {object} subscription The updated subscription to track
 */
export const trackChangedSubscriptionPlan = subscription => {
  const subscriptionTrackEvent = getSubscriptionTrackingEvent(subscription);

  const { entitlement } = subscriptionTrackEvent;

  const userId = subscription.user.toString();

  if (entitlement.includes('sl', 0))
    typewriter.changedSubscriptionPlan({
      userId,
      properties: { ...subscriptionTrackEvent },
    });
  else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Changed Subscription Plan',
      userId,
      properties: subscriptionTrackEvent,
    });
  }
  identifySubscription(subscription);
};

export default trackChangedSubscriptionPlan;
