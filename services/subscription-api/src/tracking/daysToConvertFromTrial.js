const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Days To Convert From Trial' event
 * @param {object} eventData The properties to track
 */
export const trackDaysToConvertFromTrial = eventData => {
  const { daysToPremium, planId, platform, user, entitlement } = eventData;
  const userId = user.toString();

  if (entitlement.includes('sl', 0)) {
    typewriter.daysToConvertFromTrial({
      userId,
      properties: { daysToPremium, planId, platform },
    });
  } else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Days To Convert From Trial',
      userId,
      properties: eventData,
    });
  }
};

export default trackDaysToConvertFromTrial;
