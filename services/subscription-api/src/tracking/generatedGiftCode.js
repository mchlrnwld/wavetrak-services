import uuidv4 from 'uuid/v4';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Gift Code Generated' event
 * @param {object} purchaseData The details of the gift purchase
 * @param {object} opts Additional data relevant to the tracking environment
 */
export const trackGeneratedGiftCode = giftCodeData => {
  const { brand, userId: user } = giftCodeData;
  const anonymousId = !user ? uuidv4() : null;
  const userId = user?.toString();

  if (brand.includes('sl')) {
    typewriter.generatedGiftCard({
      userId,
      anonymousId,
      properties: { ...giftCodeData, userId },
    });
  } else {
    analytics.track({
      event: 'Generated Gift Card',
      userId,
      properties: {
        ...giftCodeData,
        userId,
      },
    });
  }
};

export default trackGeneratedGiftCode;
