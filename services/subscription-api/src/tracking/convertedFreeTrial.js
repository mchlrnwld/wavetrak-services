import getSubscriptionTrackingEvent from './utils/getSubscriptionTrackingEvent';
import { identifySubscription } from './identifySubscription';

const typewriter = require('./plans/sl');
const analytics = require('../common/analytics');

/**
 * Tracks a Segment 'Converted Free Trial' event
 * @param {object} subscription The converted subscription to track
 */
export const trackConvertedFreeTrial = subscription => {
  const subscriptionTrackEvent = getSubscriptionTrackingEvent(subscription);

  const { entitlement } = subscriptionTrackEvent;
  const userId = subscription.user.toString();

  if (entitlement.includes('sl', 0))
    typewriter.convertedFreeTrial({
      userId,
      properties: { ...subscriptionTrackEvent },
    });
  else {
    // BW and FS currently do not have tracking plans so we bypass typewriter
    analytics.track({
      event: 'Converted Free Trial',
      userId,
      properties: subscriptionTrackEvent,
    });
  }
  // eslint-disable-next-line no-param-reassign
  subscription.trialing = false;
  identifySubscription(subscription);
};

export default trackConvertedFreeTrial;
