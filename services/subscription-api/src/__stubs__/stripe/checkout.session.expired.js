export default () => {
  return {
    id: 'evt_1Jq3FyCUgh9bySt6STbZ5sGd',
    object: 'event',
    api_version: '2019-12-03',
    created: 1635545342,
    data: {
      object: {
        id: 'cs_test_a1M4PwLAYXiNm4sVt4PkGd3b8zjcuvwNxmh7uaDZrc0Nc67AwF2aqIBGY9',
        object: 'checkout.session',
        after_expiration: 'null',
        allow_promotion_codes: false,
        amount_subtotal: 0,
        amount_total: 0,
        automatic_tax: {
          enabled: false,
          status: null,
        },
        billing_address_collection: null,
        cancel_url: 'https://sandbox.surfline.com/upgrade?selectedInterval=year',
        client_reference_id: '607f078cd09cdd0013748001',
        consent: null,
        consent_collection: null,
        currency: 'usd',
        customer: 'cus_KUg8DgoceUWEuj',
        customer_details: {
          email: 'eric@test.com',
          phone: null,
          tax_exempt: null,
          tax_ids: null,
        },
        customer_email: null,
        expires_at: 1635545341,
        livemode: false,
        locale: 'en',
        metadata: {
          brand: 'sl',
          priceId: 'sl_monthly_v2',
        },
        mode: 'subscription',
        payment_intent: 'null',
        payment_method_options: null,
        payment_method_types: ['card'],
        payment_status: 'unpaid',
        phone_number_collection: {
          enabled: false,
        },
        recovered_from: 'null',
        setup_intent: 'null',
        shipping: null,
        shipping_address_collection: null,
        submit_type: null,
        subscription: null,
        success_url: 'https://sandbox.surfline.com/upgrade',
        total_details: {
          amount_discount: 0,
          amount_shipping: 0,
          amount_tax: 0,
        },
        url: null,
      },
    },
    livemode: false,
    pending_webhooks: 1,
    request: {
      id: null,
      idempotency_key: null,
    },
    type: 'checkout.session.expired',
  };
};
