export default ({ customerId, subscriptionId }, { invoiceItemType }) => {
  return {
    id: 'evt_1FwIacJle83KVJVkDjLUaLsz',
    object: 'event',
    api_version: '2018-11-08',
    created: 1577928906,
    data: {
      object: {
        object: 'invoice',
        account_country: 'US',
        account_name: 'Surfline/Wavetrak Inc',
        amount_due: 9588,
        amount_paid: 0,
        amount_remaining: 9588,
        application_fee: null,
        attempt_count: 0,
        attempted: false,
        billing: 'charge_automatically',
        billing_reason: 'upcoming',
        charge: null,
        collection_method: 'charge_automatically',
        created: 1578188094,
        currency: 'usd',
        custom_fields: null,
        customer: customerId,
        customer_address: null,
        customer_email: 'test1482283398230@delete.testingaccount',
        customer_name: null,
        customer_phone: null,
        customer_shipping: null,
        customer_tax_exempt: 'none',
        customer_tax_ids: [],
        date: 1578188094,
        default_payment_method: null,
        default_source: null,
        default_tax_rates: [],
        description: null,
        discount: null,
        due_date: null,
        ending_balance: 0,
        finalized_at: null,
        footer: null,
        lines: {
          object: 'list',
          data: [
            {
              id: 'sli_b212dea75d23d6',
              object: 'line_item',
              amount: 9588,
              currency: 'usd',
              description: '1 × Surfline Premium Annual (at $95.88 / year)',
              discountable: true,
              livemode: false,
              metadata: {},
              period: {
                end: 1609810494,
                start: 1578188094,
              },
              plan: {
                id: 'sl_annual_v2',
                object: 'plan',
                active: true,
                aggregate_usage: null,
                amount: 9588,
                amount_decimal: '9588',
                billing_scheme: 'per_unit',
                created: 1529257914,
                currency: 'usd',
                interval: 'year',
                interval_count: 1,
                livemode: false,
                metadata: {
                  // eslint-disable-next-line no-template-curly-in-string
                  description: 'Billed once a year at ${amount}. Saving ${saving} per year.',
                  entitlement: 'sl_premium',
                  query_set: 'sl',
                  upgradable: '1',
                  status: 'active',
                },
                nickname: 'Surfline Premium Annual v2',
                product: 'prod_D4DRBnJiVZV6dF',
                tiers: null,
                tiers_mode: null,
                transform_usage: null,
                trial_period_days: 15,
                usage_type: 'licensed',
              },
              price: {
                id: 'sl_annual_v2',
                object: 'price',
                active: true,
                billing_scheme: 'per_unit',
                created: 1546907200,
                currency: 'usd',
                livemode: true,
                lookup_key: 'SL_YEAR_USD',
                metadata: {
                  description: 'Billed once a year at $95.88',
                  entitlement: 'sl_premium',
                  query_set: 'sl',
                  upgradable: '1',
                  status: 'active',
                  features: 'sl_premium',
                },
                nickname: 'Surfline Premium Annual v2',
                product: 'prod_D51bxaAfF9zdFu',
                recurring: {
                  aggregate_usage: null,
                  interval: 'year',
                  interval_count: 1,
                  trial_period_days: 15,
                  usage_type: 'licensed',
                },
                tax_behavior: 'unspecified',
                tiers_mode: null,
                transform_quantity: null,
                type: 'recurring',
                unit_amount: 9588,
                unit_amount_decimal: '9588',
              },
              proration: false,
              quantity: 1,
              subscription: 'sub_9mY6UNpSd3oJK8',
              subscription_item: 'si_19Sz0MJle83KVJVkpbUQXnZ8',
              tax_amounts: [],
              tax_rates: [],
              type: invoiceItemType || 'subscription',
              unique_id: 'il_tmp_b212dea75d23d6',
            },
          ],
          has_more: false,
          total_count: 1,
          url:
            '/v1/invoices/upcoming/lines?customer=cus_9mY6ZnjHxmtgCc&subscription=sub_9mY6UNpSd3oJK8',
        },
        livemode: false,
        metadata: {},
        next_payment_attempt: 1578191694,
        number: 'CEA80DB-0003',
        paid: false,
        payment_intent: null,
        period_end: 1578188094,
        period_start: 1546652094,
        post_payment_credit_notes_amount: 0,
        pre_payment_credit_notes_amount: 0,
        receipt_number: null,
        starting_balance: 0,
        statement_descriptor: null,
        status: 'draft',
        status_transitions: {
          finalized_at: null,
          marked_uncollectible_at: null,
          paid_at: null,
          voided_at: null,
        },
        subscription: subscriptionId,
        subtotal: 9588,
        tax: null,
        tax_percent: null,
        total: 9588,
        total_tax_amounts: [],
        webhooks_delivered_at: null,
      },
    },
    livemode: false,
    pending_webhooks: 1,
    request: {
      id: null,
      idempotency_key: null,
    },
    type: 'invoice.upcoming',
  };
};
