export default ({ customerId, subscriptionId }, { invoiceItemType }) => {
  return {
    id: 'evt_1FwZTsLwFGcBvXawQot8euwQ',
    object: 'event',
    api_version: '2018-11-08',
    created: 1577993836,
    data: {
      object: {
        id: 'in_1FwZTsLwFGcBvXawTTLrHTOT',
        object: 'invoice',
        account_country: 'US',
        account_name: 'Surfline Integration Tests',
        amount_due: 0,
        amount_paid: 0,
        amount_remaining: 0,
        application_fee: null,
        attempt_count: 0,
        attempted: true,
        auto_advance: false,
        billing: 'charge_automatically',
        billing_reason: 'subscription_create',
        charge: null,
        collection_method: 'charge_automatically',
        created: 1577993836,
        currency: 'usd',
        custom_fields: null,
        customer: customerId,
        customer_address: null,
        customer_email: 'joel@example.com',
        customer_name: null,
        customer_phone: null,
        customer_shipping: null,
        customer_tax_exempt: 'none',
        customer_tax_ids: [],
        date: 1577993836,
        default_payment_method: null,
        default_source: null,
        default_tax_rates: [],
        description: null,
        discount: {
          object: 'discount',
          coupon: {
            id: '50forever_00000000000000',
            object: 'coupon',
            amount_off: null,
            created: 1524698433,
            currency: null,
            duration: 'forever',
            duration_in_months: null,
            livemode: false,
            max_redemptions: null,
            metadata: {
              plans: 'sl_annual_v2, sl_monthly_v2',
            },
            name: null,
            percent_off: 50,
            redeem_by: null,
            times_redeemed: 141,
            valid: true,
          },
          customer: customerId,
          end: null,
          start: 1524701318,
          subscription: 'sub_00000000000000',
        },
        due_date: 1580585836,
        ending_balance: 0,
        finalized_at: 1577993836,
        footer: null,
        hosted_invoice_url: 'https://pay.stripe.com/invoice/invst_48UjtgAsPQwHevE31K4ajfhTuB',
        invoice_pdf: 'https://pay.stripe.com/invoice/invst_48UjtgAsPQwHevE31K4ajfhTuB/pdf',
        lines: {
          object: 'list',
          data: [
            {
              id: 'sli_de0bde74863ae8',
              object: 'line_item',
              amount: 0,
              currency: 'usd',
              description: 'Trial period for Surfline Premium Monthly',
              discountable: true,
              livemode: false,
              metadata: {},
              period: {
                end: 1579289836,
                start: 1577993836,
              },
              plan: {
                id: 'sl_monthly_v2',
                object: 'plan',
                active: true,
                aggregate_usage: null,
                amount: 999,
                amount_decimal: '999',
                billing_scheme: 'per_unit',
                created: 1529353862,
                currency: 'usd',
                interval: 'month',
                interval_count: 1,
                livemode: false,
                metadata: {
                  description: 'Billed once a month at $9.99 Billed once a month at $9.99',
                  upgradable: '1',
                  query_set: 'sl',
                  entitlement: 'sl_premium',
                  status: 'active',
                  updated: 'updatetest',
                },
                nickname: 'Surfline Premium Monthly v2',
                product: 'prod_D4dGGqAr9hUgvP',
                tiers: null,
                tiers_mode: null,
                transform_usage: null,
                trial_period_days: 15,
                usage_type: 'licensed',
              },
              proration: false,
              quantity: 1,
              subscription: 'sub_GTWWasMd4OoU7j',
              subscription_item: 'si_GTWWNfNPbAeGrl',
              tax_amounts: [],
              tax_rates: [],
              type: invoiceItemType || 'subscription',
              unique_id: 'il_1FwZTsLwFGcBvXaw54fARVxp',
            },
          ],
          has_more: false,
          total_count: 1,
          url: '/v1/invoices/in_1FwZTsLwFGcBvXawTTLrHTOT/lines',
        },
        livemode: false,
        metadata: {},
        next_payment_attempt: null,
        number: 'C9395143-0001',
        paid: true,
        payment_intent: null,
        period_end: 1577993836,
        period_start: 1577993836,
        post_payment_credit_notes_amount: 0,
        pre_payment_credit_notes_amount: 0,
        receipt_number: null,
        starting_balance: 0,
        statement_descriptor: null,
        status: 'paid',
        status_transitions: {
          finalized_at: 1577993836,
          marked_uncollectible_at: null,
          paid_at: 1577993836,
          voided_at: null,
        },
        subscription: subscriptionId,
        subtotal: 0,
        tax: null,
        tax_percent: null,
        total: 0,
        total_tax_amounts: [],
        webhooks_delivered_at: null,
      },
    },
    livemode: false,
    pending_webhooks: 1,
    request: {
      id: 'req_cGVOs9KEDgakCp',
      idempotency_key: null,
    },
    type: 'invoice.created',
  };
};
