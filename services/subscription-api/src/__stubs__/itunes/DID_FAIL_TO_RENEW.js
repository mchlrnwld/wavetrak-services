export default opts => {
  return {
    unified_receipt: {
      latest_receipt:
        'MIIUMwYJKoZIhvcNAQcCoIIUJDCCFCACAQExCzAJBgUrDgMCGgUAMIID1AYJKoZIhvcNAQcBoIIDxQSCA8ExggO9MAoCARMCAQEEAgwAMAoCARQCAQEEAgwAMAsCARkCAQEEAwIBAzAMAgEOAgEBBAQCAgCJMA0CAQsCAQEEBQIDA/0CMA0CAQ0CAQEEBQIDAiLhMA4CAQECAQEEBgIEF3ijUDAOAgEJAgEBBAYCBFAyNTYwDgIBCgIBAQQGFgRub25lMA4CARACAQEEBgIEMfCtvDAUAgEAAgEBBAwMClByb2R1Y3Rpb24wFgIBAwIBAQQODAwyMDIwMDkxNjIyMDUwGAIBBAIBAgQQlt45c/7FLKjBclLrlrK8PDAcAgEFAgEBBBTV7jhwiNugOWiQMR9EY3PbU40OoDAeAgECAgEBBBYMFGNvbS5zdXJmbGluZS5zdXJmYXBwMB4CAQwCAQEEFhYUMjAyMC0xMC0wOFQyMDoyNDowNVowHgIBEgIBAQQWFhQyMDIwLTEwLTAxVDIwOjIzOjMxWjAiAgEIAgEBBBoWGDIwMjAtMTAtMDhUMjA6MjQ6MDUuOTE2WjBHAgEHAgEBBD+WMuBJZfuWlrrtAbEF7XXbkbQYjdWoIjzCjL63FIKqYPputITNXAr94OtuA1Mren7+xIzs5nJDf4h5C2I0F90wWgIBBgIBAQRSSWfd6Qam/0+7cEGMWxNwBQhneyxQQKXvciF0v3inBGqoc49wjJBIbaiLjz4lO1lu+L1EizUOOM2cJgs5YlugFJa/Bk7zSjNoHNILuLhjOzNteTCCAZkCARECAQEEggGPMYIBizALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgaxAgEBBAMCAQEwDAICBrcCAQEEAwIBADAPAgIGrgIBAQQGAgRD1jKyMBICAgavAgEBBAkCBwH0OOo33jAwGgICBqcCAQEEEQwPNTUwMDAwNjgwNTM3OTY4MBoCAgapAgEBBBEMDzU1MDAwMDY4MDUzNzk2ODAfAgIGqAIBAQQWFhQyMDIwLTEwLTAxVDIwOjIzOjMxWjAfAgIGqgIBAQQWFhQyMDIwLTEwLTAxVDIwOjIzOjMxWjAfAgIGrAIBAQQWFhQyMDIwLTEwLTA4VDIwOjIzOjMxWjA2AgIGpgIBAQQtDCtzdXJmbGluZS5pb3NhcHAuMW1vbnRoLnN1YnNjcmlwdGlvbi5ub3RyaWFsoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQCGmpsxxHv7A0wgTg8oWSrdObbEcOMttPL/36W5PPG0+U8ePvaJ1qXzg7Kg5/2+sGWctSZDmdK5spXW8t/qU/O98ESXhyNnVWLir+kFfLJb4u5ey+2m2mES1vXG2YYh5lmdQGnyy2MjMtwBjBZRAfcTOpJneppo6YshlafVW7pgpDKafl4GeoSGdH+LFIJfa5mS5nMcCuRGCewyMBUC59hqoJQjtm7rAKn4oFe0hmCfh95rPjYuUfOiho0FD25Ho8MJ7oGSHuA46chSihLkvbGQCrdsCcCGAB/ElAMXKCkdsyvkm67GQDGJAbksCUYJ2P4mCaa2Vf/N6O3WyY+VzTDQ',
      pending_renewal_info: [
        {
          is_in_billing_retry_period: opts.isInBillingRetryPeriod,
          auto_renew_status: '1',
          original_transaction_id: opts.transactionId,
          product_id: opts.planId,
          expiration_intent: '2',
          auto_renew_product_id: opts.planId,
        },
      ],
      environment: 'Production',
      status: 0,
      latest_receipt_info: [
        {
          expires_date_pst: '2020-10-08 13:23:31 America/Los_Angeles',
          purchase_date: '2020-10-01 20:23:31 Etc/GMT',
          purchase_date_ms: '1601583811000',
          original_purchase_date_ms: '1601583811000',
          transaction_id: opts.transactionId,
          original_transaction_id: opts.transactionId,
          quantity: '1',
          expires_date_ms: new Date() * 1000 + 31556952000,
          original_purchase_date_pst: '2020-10-01 13:23:31 America/Los_Angeles',
          product_id: opts.planId,
          subscription_group_identifier: '20337868',
          web_order_line_item_id: '550000261586480',
          expires_date: '2020-10-08 20:23:31 Etc/GMT',
          is_in_intro_offer_period: 'false',
          original_purchase_date: '2020-10-01 20:23:31 Etc/GMT',
          purchase_date_pst: '2020-10-01 13:23:31 America/Los_Angeles',
          is_trial_period: 'true',
        },
      ],
    },
    environment: 'PROD',
    auto_renew_status: 'true',
    bvrs: '202009162205',
    latest_expired_receipt:
      'ewoJInNpZ25hdHVyZSIgPSAiQXpyYTVnUm9obG9yTHpoc1NieTYwdWM2VG5WZmxBc0xpbHNvSEM3N2JxUDg4TTAxMzZoV3JwVXMyb3FyZFFaczdJQm5HcTBwcndRRUhJNG01NkNUdjg2NU9iK0Q5SzIzUzJobkdGZ1oxY0NYY0tRSVRxSGZ6VHo3bi9hdEhQT29xSVVkRVlVSGtob1R3SVNaeDZ4UzdGaExMbTd6Y1JJV1psMTRjVkFmUUxCYktnMWlORmZJUHphTWVOd3Q3Y21wMldOTy9qUEVHOUh3dHE2V092VkhPUUxpRzRLejVJekRXcU42aUtmUEduemY4SlZ0WFY5dUFJdm5JbEF4QXdoaER0ZGkvOVBVaGltOXJZWkNtdTAvYnJlaWdQVGl5QWxUMGV0b1RJdk5Nc3YvQStqeURxaFBndFlxQWRqR2NBb1VIeFBTcjR3TVBSWi83ZTVOdzRBUThNQUFBQVdBTUlJRmZEQ0NCR1NnQXdJQkFnSUlEdXRYaCtlZUNZMHdEUVlKS29aSWh2Y05BUUVGQlFBd2daWXhDekFKQmdOVkJBWVRBbFZUTVJNd0VRWURWUVFLREFwQmNIQnNaU0JKYm1NdU1Td3dLZ1lEVlFRTERDTkJjSEJzWlNCWGIzSnNaSGRwWkdVZ1JHVjJaV3h2Y0dWeUlGSmxiR0YwYVc5dWN6RkVNRUlHQTFVRUF3dzdRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTWdRMlZ5ZEdsbWFXTmhkR2x2YmlCQmRYUm9iM0pwZEhrd0hoY05NVFV4TVRFek1ESXhOVEE1V2hjTk1qTXdNakEzTWpFME9EUTNXakNCaVRFM01EVUdBMVVFQXd3dVRXRmpJRUZ3Y0NCVGRHOXlaU0JoYm1RZ2FWUjFibVZ6SUZOMGIzSmxJRkpsWTJWcGNIUWdVMmxuYm1sdVp6RXNNQ29HQTFVRUN3d2pRWEJ3YkdVZ1YyOXliR1IzYVdSbElFUmxkbVZzYjNCbGNpQlNaV3hoZEdsdmJuTXhFekFSQmdOVkJBb01Da0Z3Y0d4bElFbHVZeTR4Q3pBSkJnTlZCQVlUQWxWVE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBcGMrQi9TV2lnVnZXaCswajJqTWNqdUlqd0tYRUpzczl4cC9zU2cxVmh2K2tBdGVYeWpsVWJYMS9zbFFZbmNRc1VuR09aSHVDem9tNlNkWUk1YlNJY2M4L1cwWXV4c1FkdUFPcFdLSUVQaUY0MWR1MzBJNFNqWU5NV3lwb041UEM4cjBleE5LaERFcFlVcXNTNCszZEg1Z1ZrRFV0d3N3U3lvMUlnZmRZZUZScjZJd3hOaDlLQmd4SFZQTTNrTGl5a29sOVg2U0ZTdUhBbk9DNnBMdUNsMlAwSzVQQi9UNXZ5c0gxUEttUFVockFKUXAyRHQ3K21mNy93bXYxVzE2c2MxRkpDRmFKekVPUXpJNkJBdENnbDdaY3NhRnBhWWVRRUdnbUpqbTRIUkJ6c0FwZHhYUFEzM1k3MkMzWmlCN2o3QWZQNG83UTAvb21WWUh2NGdOSkl3SURBUUFCbzRJQjF6Q0NBZE13UHdZSUt3WUJCUVVIQVFFRU16QXhNQzhHQ0NzR0FRVUZCekFCaGlOb2RIUndPaTh2YjJOemNDNWhjSEJzWlM1amIyMHZiMk56Y0RBekxYZDNaSEl3TkRBZEJnTlZIUTRFRmdRVWthU2MvTVIydDUrZ2l2Uk45WTgyWGUwckJJVXdEQVlEVlIwVEFRSC9CQUl3QURBZkJnTlZIU01FR0RBV2dCU0lKeGNKcWJZWVlJdnM2N3IyUjFuRlVsU2p0ekNDQVI0R0ExVWRJQVNDQVJVd2dnRVJNSUlCRFFZS0tvWklodmRqWkFVR0FUQ0IvakNCd3dZSUt3WUJCUVVIQWdJd2diWU1nYk5TWld4cFlXNWpaU0J2YmlCMGFHbHpJR05sY25ScFptbGpZWFJsSUdKNUlHRnVlU0J3WVhKMGVTQmhjM04xYldWeklHRmpZMlZ3ZEdGdVkyVWdiMllnZEdobElIUm9aVzRnWVhCd2JHbGpZV0pzWlNCemRHRnVaR0Z5WkNCMFpYSnRjeUJoYm1RZ1kyOXVaR2wwYVc5dWN5QnZaaUIxYzJVc0lHTmxjblJwWm1sallYUmxJSEJ2YkdsamVTQmhibVFnWTJWeWRHbG1hV05oZEdsdmJpQndjbUZqZEdsalpTQnpkR0YwWlcxbGJuUnpMakEyQmdnckJnRUZCUWNDQVJZcWFIUjBjRG92TDNkM2R5NWhjSEJzWlM1amIyMHZZMlZ5ZEdsbWFXTmhkR1ZoZFhSb2IzSnBkSGt2TUE0R0ExVWREd0VCL3dRRUF3SUhnREFRQmdvcWhraUc5Mk5rQmdzQkJBSUZBREFOQmdrcWhraUc5dzBCQVFVRkFBT0NBUUVBRGFZYjB5NDk0MXNyQjI1Q2xtelQ2SXhETUlKZjRGelJqYjY5RDcwYS9DV1MyNHlGdzRCWjMrUGkxeTRGRkt3TjI3YTQvdncxTG56THJSZHJqbjhmNUhlNXNXZVZ0Qk5lcGhtR2R2aGFJSlhuWTR3UGMvem83Y1lmcnBuNFpVaGNvT0FvT3NBUU55MjVvQVE1SDNPNXlBWDk4dDUvR2lvcWJpc0IvS0FnWE5ucmZTZW1NL2oxbU9DK1JOdXhUR2Y4YmdwUHllSUdxTktYODZlT2ExR2lXb1IxWmRFV0JHTGp3Vi8xQ0tuUGFObVNBTW5CakxQNGpRQmt1bGhnd0h5dmozWEthYmxiS3RZZGFHNllRdlZNcHpjWm04dzdISG9aUS9PamJiOUlZQVlNTnBJcjdONFl0UkhhTFNQUWp2eWdhWndYRzU2QWV6bEhSVEJoTDhjVHFBPT0iOwoJInB1cmNoYXNlLWluZm8iID0gImV3b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVdGNITjBJaUE5SUNJeU1ESXdMVEV3TFRBeElERXpPakl6T2pNeElFRnRaWEpwWTJFdlRHOXpYMEZ1WjJWc1pYTWlPd29KSW5GMVlXNTBhWFI1SWlBOUlDSXhJanNLQ1NKemRXSnpZM0pwY0hScGIyNHRaM0p2ZFhBdGFXUmxiblJwWm1sbGNpSWdQU0FpTWpBek16YzROamdpT3dvSkluVnVhWEYxWlMxMlpXNWtiM0l0YVdSbGJuUnBabWxsY2lJZ1BTQWlPVGt5UWpNM016Y3RSa0pDUWkwMFFUVTBMVUU1UWpVdE56WXhPVEkzUXpGRlJqY3dJanNLQ1NKdmNtbG5hVzVoYkMxd2RYSmphR0Z6WlMxa1lYUmxMVzF6SWlBOUlDSXhOakF4TlRnek9ERXhNREF3SWpzS0NTSmxlSEJwY21WekxXUmhkR1V0Wm05eWJXRjBkR1ZrSWlBOUlDSXlNREl3TFRFd0xUQTRJREl3T2pJek9qTXhJRVYwWXk5SFRWUWlPd29KSW1sekxXbHVMV2x1ZEhKdkxXOW1abVZ5TFhCbGNtbHZaQ0lnUFNBaVptRnNjMlVpT3dvSkluQjFjbU5vWVhObExXUmhkR1V0YlhNaUlEMGdJakUyTURFMU9ETTRNVEV3TURBaU93b0pJbVY0Y0dseVpYTXRaR0YwWlMxbWIzSnRZWFIwWldRdGNITjBJaUE5SUNJeU1ESXdMVEV3TFRBNElERXpPakl6T2pNeElFRnRaWEpwWTJFdlRHOXpYMEZ1WjJWc1pYTWlPd29KSW1sekxYUnlhV0ZzTFhCbGNtbHZaQ0lnUFNBaWRISjFaU0k3Q2draWFYUmxiUzFwWkNJZ1BTQWlNVEV6T0RFeE1URTFOQ0k3Q2draWRXNXBjWFZsTFdsa1pXNTBhV1pwWlhJaUlEMGdJall5TTJZMFptSTRPV1l3TkRRd1pqTTRPV0kzWlRKbE9UaGlaV1l6WmpFd1lqazBOMlF3TURFaU93b0pJbTl5YVdkcGJtRnNMWFJ5WVc1ellXTjBhVzl1TFdsa0lpQTlJQ0kxTlRBd01EQTJPREExTXpjNU5qZ2lPd29KSW1WNGNHbHlaWE10WkdGMFpTSWdQU0FpTVRZd01qRTRPRFl4TVRBd01DSTdDZ2tpWVhCd0xXbDBaVzB0YVdRaUlEMGdJak01TXpjNE1qQTVOaUk3Q2draWRISmhibk5oWTNScGIyNHRhV1FpSUQwZ0lqVTFNREF3TURZNE1EVXpOemsyT0NJN0Nna2lZblp5Y3lJZ1BTQWlNakF5TURBNU1UWXlNakExSWpzS0NTSjNaV0l0YjNKa1pYSXRiR2x1WlMxcGRHVnRMV2xrSWlBOUlDSTFOVEF3TURBeU5qRTFPRFkwT0RBaU93b0pJblpsY25OcGIyNHRaWGgwWlhKdVlXd3RhV1JsYm5ScFptbGxjaUlnUFNBaU9ETTNPRFUyTnpBd0lqc0tDU0ppYVdRaUlEMGdJbU52YlM1emRYSm1iR2x1WlM1emRYSm1ZWEJ3SWpzS0NTSndjbTlrZFdOMExXbGtJaUE5SUNKemRYSm1iR2x1WlM1cGIzTmhjSEF1TVcxdmJuUm9Mbk4xWW5OamNtbHdkR2x2Ymk1dWIzUnlhV0ZzSWpzS0NTSndkWEpqYUdGelpTMWtZWFJsSWlBOUlDSXlNREl3TFRFd0xUQXhJREl3T2pJek9qTXhJRVYwWXk5SFRWUWlPd29KSW5CMWNtTm9ZWE5sTFdSaGRHVXRjSE4wSWlBOUlDSXlNREl3TFRFd0xUQXhJREV6T2pJek9qTXhJRUZ0WlhKcFkyRXZURzl6WDBGdVoyVnNaWE1pT3dvSkltOXlhV2RwYm1Gc0xYQjFjbU5vWVhObExXUmhkR1VpSUQwZ0lqSXdNakF0TVRBdE1ERWdNakE2TWpNNk16RWdSWFJqTDBkTlZDSTdDbjA9IjsKCSJwb2QiID0gIjU1IjsKCSJzaWduaW5nLXN0YXR1cyIgPSAiMCI7Cn0=',
    latest_expired_receipt_info: {
      original_purchase_date_pst: '2020-10-01 13:23:31 America/Los_Angeles',
      quantity: '1',
      subscription_group_identifier: '20337868',
      unique_vendor_identifier: '992B3737-FBBB-4A54-A9B5-761927C1EF70',
      original_purchase_date_ms: '1601583811000',
      expires_date_formatted: '2020-10-08 20:23:31 Etc/GMT',
      is_in_intro_offer_period: 'false',
      purchase_date_ms: '1601583811000',
      expires_date_formatted_pst: '2020-10-08 13:23:31 America/Los_Angeles',
      is_trial_period: 'true',
      item_id: '1138111154',
      unique_identifier: '623f4fb89f0440f389b7e2e98bef3f10b947d001',
      original_transaction_id: '550000680537968',
      expires_date: '1602188611000',
      app_item_id: '393782096',
      transaction_id: '550000680537968',
      bvrs: '202009162205',
      web_order_line_item_id: '550000261586480',
      version_external_identifier: '837856700',
      bid: 'com.surfline.surfapp',
      product_id: opts.planId,
      purchase_date: '2020-10-01 20:23:31 Etc/GMT',
      purchase_date_pst: '2020-10-01 13:23:31 America/Los_Angeles',
      original_purchase_date: '2020-10-01 20:23:31 Etc/GMT',
    },
    bid: 'com.surfline.surfapp',
    password: process.env.APPLE_IAP_PASSWORD,
    auto_renew_product_id: opts.planId,
    notification_type: 'DID_FAIL_TO_RENEW',
  };
};
