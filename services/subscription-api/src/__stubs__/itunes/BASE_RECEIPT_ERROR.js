export default (opts = {}) => {
  return {
    environment: 'Production',
    status: opts.status || 21010,
    is_retryable: opts.isRetryable || false,
  };
};
