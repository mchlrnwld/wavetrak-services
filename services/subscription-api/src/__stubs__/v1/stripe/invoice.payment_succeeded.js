export default opts => {
  return {
    created: 1326853478,
    livemode: false,
    id: 'invoice.payment_00000000000000',
    type: 'invoice.payment_succeeded',
    object: 'event',
    request: null,
    pending_webhooks: 1,
    api_version: '2018-11-08',
    data: {
      object: {
        id: 'in_00000000000000',
        object: 'invoice',
        amount_due: 1,
        amount_paid: 0,
        amount_remaining: 0,
        application_fee: null,
        attempt_count: 0,
        attempted: true,
        auto_advance: false,
        billing: 'charge_automatically',
        billing_reason: null,
        charge: '_00000000000000',
        currency: 'usd',
        customer: opts.customerId,
        date: 1524701318,
        default_source: null,
        description: null,
        discount: opts.discount,
        due_date: null,
        ending_balance: 0,
        finalized_at: 1524701318,
        hosted_invoice_url: 'https://pay.stripe.com/invoice/invst_gQUOY5mecUjqhueojerKR1Zwga',
        invoice_pdf: 'https://pay.stripe.com/invoice/invst_gQUOY5mecUjqhueojerKR1Zwga/pdf',
        lines: {
          data: [
            {
              id: 'sli_00000000000000',
              object: 'line_item',
              amount: 999,
              currency: 'usd',
              description: '1 × Surfline Premium Monthly (at $9.99 / month)',
              discountable: true,
              livemode: false,
              metadata: {},
              period: {
                end: opts.periodEnd,
                start: opts.periodStart,
              },
              plan: {
                id: opts.planId || opts.stripePlanId || 'sl_premium_daily',
                object: 'plan',
                active: true,
                aggregate_usage: null,
                amount: 999,
                billing_scheme: 'per_unit',
                created: 1524697307,
                currency: 'usd',
                interval: 'month',
                interval_count: 1,
                livemode: false,
                metadata: {
                  entitlement: 'sl_premium',
                  features: 'sl_premium',
                  query_set: 'sl',
                  rank: '2',
                  upgradable: '1',
                  description: 'Shaka bradah!',
                  status: 'active',
                },
                nickname: 'Surfline Premium Monthly',
                product: 'prod_00000000000000',
                tiers: null,
                tiers_mode: null,
                transform_usage: null,
                trial_period_days: 3,
                usage_type: 'licensed',
              },
              proration: false,
              quantity: 1,
              subscription: opts.invoiceSubscriptionId === null ? null : opts.subscriptionId,
              subscription_item: 'si_00000000000000',
              type: 'subscription',
            },
          ],
          has_more: false,
          object: 'list',
          url: '/v1/invoices/in_1CKxf8LwFGcBvXawvwCTwflb/lines',
        },
        livemode: false,
        metadata: {},
        next_payment_attempt: null,
        number: '110ADDB-0001',
        paid: true,
        period_end: 1462401579,
        period_start: 1462401579,
        receipt_number: null,
        starting_balance: 0,
        statement_descriptor: null,
        status: 'paid',
        subscription: opts.subscriptionId,
        subtotal: 0,
        tax: null,
        tax_percent: null,
        total: 1,
        webhooks_delivered_at: 1524701318,
        closed: true,
      },
    },
  };
};
