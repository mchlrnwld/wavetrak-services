import { createSubscriptionDate as createDate } from '../../../integration-tests/helpers/testUtils';

export default (opts = {}) => {
  const baseProperties = {
    kind: 'androidpublisher#subscriptionPurchase',
    startTimeMillis: createDate({ sub: 15 }),
    expiryTimeMillis: createDate({ add: 15 }),
    autoRenewing: true,
    priceCurrencyCode: 'USD',
    priceAmountMicros: '9990000',
    countryCode: 'US',
    developerPayload: 'NTZmYWU5NGExOWI3NDMxOGE3M2JjNTQ0\n',
    orderId: 'GPA.3327-5391-7183-13013..0',
    acknowledgementState: 1,
    paymentState: 1,
  };

  const cancellationProperties = {
    autoRenewing: false,
    cancelReason: 0,
    userCancellationTimeMillis: createDate(),
    cancelSurveyResult: {
      cancelSurveyReason: 1,
    },
  };

  if (opts.renewal) {
    return {
      ...baseProperties,
      autoRenewing: true,
      linkedPurchaseToken: opts.linkedPurchaseToken,
      startTimeMillis: createDate({ add: 15 }),
      expiryTimeMillis: createDate({ add: 45 }),
    };
  }

  if (opts.cancellation) {
    return {
      ...baseProperties,
      ...cancellationProperties,
    };
  }

  if (opts.expiration || opts.revoke) {
    return {
      ...baseProperties,
      ...cancellationProperties,
      expiryTimeMillis: createDate({ sub: 1 }),
    };
  }

  if (opts.onHold) {
    return {
      ...baseProperties,
      autoRenewing: true,
      paymentState: 0,
      expiryTimeMillis: createDate({ sub: 10 }),
    };
  }

  if (opts.recovered) {
    return {
      ...baseProperties,
      autoRenewing: true,
      paymentState: 1,
      expiryTimeMillis: createDate({ add: 30 }),
    };
  }

  if (opts.deferred) {
    return {
      ...baseProperties,
      autoRenewing: true,
      paymentState: 0,
      expiryTimeMillis: createDate({ add: 40 }),
    };
  }
  return baseProperties;
};
