export default () => {
  return {
    message: {
      data:
        // eslint-disable-next-line max-len
        'eyJ2ZXJzaW9uIjoiMS4wIiwicGFja2FnZU5hbWUiOiJjb20uc3VyZmxpbmUuYW5kcm9pZCIsImV2ZW50VGltZU1pbGxpcyI6IjE1NzM1OTA0Mzg1NjYiLCJ0ZXN0Tm90aWZpY2F0aW9uIjp7InZlcnNpb24iOiIxLjAifX0=',
      messageId: '835457810297778',
      message_id: '835457810297778',
      publishTime: '2019-11-12T20:27:18.698Z',
      publish_time: '2019-11-12T20:27:18.698Z',
    },
    subscription:
      'projects/surfline-android-1485812445501/subscriptions/sl-subscription-notifications',
  };
};
