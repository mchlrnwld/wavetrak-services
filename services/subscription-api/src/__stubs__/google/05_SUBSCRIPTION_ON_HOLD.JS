import { createSubscriptionDate as createDate } from '../../../integration-tests/helpers/testUtils';

export default (opts = {}) => {
  return {
    version: '1.0',
    packageName: opts.packageName || 'com.surfline.android',
    eventTimeMillis: createDate(),
    subscriptionNotification: {
      version: '1.0',
      notificationType: 5,
      purchaseToken: opts.purchaseToken || 'DEF456',
      subscriptionId: opts.productId || 'surfline.android.1month.subscription.notrial',
    },
  };
};
