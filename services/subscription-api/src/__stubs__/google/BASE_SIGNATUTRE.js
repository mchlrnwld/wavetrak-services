export default (opts = {}) => {
  return {
    receipt: {
      data: {
        orderId: 'GPA.3365-6119-6364-60363',
        packageName: opts.packageName || 'com.surfline.android',
        productId: opts.productId || 'surfline.android.monthly.2022',
        purchaseToken: opts.purchaseToken || 'ABC123',
      },
      signature: 'ABCDEFGHIJKLMNOP123456789',
    },
  };
};
