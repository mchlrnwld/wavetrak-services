import logger from '../common/logger';
import * as dbContext from '../common/dbContext';
import { verifyUserAccountReceipts } from '../models/v1/iap';
import { getIapUserInfoIds } from '../models/v1/accounts';

/*
 * Verifies all of a user's receipts and update subscriptions accordingly
 *
 * Run from a scheduled task to update ios iap receipts
 *
 * Listen to call from scheduled lambda, trigger at midnight every night
 *  1. Query indexed attribute on UserAccounts (put this in ../models/accounts)
 *  2. for userAccountId in iapUserAccountIds -> verifyUserAccountReceipts(userAccountId)
 *
 * Environment variables used:
 *  1. MONGO_CONNECTION_STRING
 *  2. Google IAP keys
 *        SURFLINE_GOOGLE_IAP_PUBLICKEY_LIVE
 *        BUOYWEATHER_GOOGLE_IAP_PUBLICKEY_LIVE=
 *        FISHTRACK_GOOGLE_IAP_PUBLICKEY_LIVE=
 *        PLAY_STORE_API_ACCESS_TOKEN=
 *        PLAY_STORE_API_REFRESH_TOKEN=
 *        PLAY_STORE_API_CLIENT_ID=
 *        PLAY_STORE_API_CLIENT_SECRET=
 *  3. APPLE_IAP_PASSWORD
 */

const log = logger('iap-validation-job');
log.info('Receipt validation job initializing...');

Promise.all([dbContext.connectMongo()])
  .then(async () => {
    if (!process.env.APPLE_IAP_PASSWORD) {
      log.error('Configure APPLE_IAP_PASSWORD');
      process.exit(1);
    }
    const commitChanges = process.env.COMMIT_RECEIPT_JOB_CHANGES === 'true';
    log.info({
      message: `Commit Changes flag set to: ${commitChanges}`,
    });

    let iapSubscribers = [];

    try {
      iapSubscribers = await getIapUserInfoIds();
    } catch (e) {
      log.error({
        message: 'Receipt validation job: Error retrieving user info ids',
        e,
      });
      process.exit(1);
    }

    if (!iapSubscribers || iapSubscribers.length === 0) {
      log.info({
        message: 'No IAP subscribers in this environment',
      });
      process.exit(0);
    }

    const failedUserIds = [];
    const processedUserIds = [];

    const subscriberCount = iapSubscribers.length;
    let currentSubscriber = 1;

    log.info({
      message: 'Receipt validation job started',
      count: subscriberCount,
    });

    for (const userInfoId of iapSubscribers) {
      try {
        log.trace(`Verifying user ${userInfoId}. ${currentSubscriber} of ${subscriberCount}`);
        if (userInfoId) {
          await verifyUserAccountReceipts(userInfoId, commitChanges);
          log.trace(`verified: ${userInfoId}. ${currentSubscriber} of ${subscriberCount}`);
          processedUserIds.push(userInfoId.toString());
        }
      } catch (error) {
        log.error(
          {
            message: `Problem verifying ${userInfoId}'s receipts.
              ${currentSubscriber} of ${subscriberCount}`,
            userId: userInfoId.toString(),
          },
          error,
        );
        failedUserIds.push(userInfoId.toString());
      }
      currentSubscriber += 1;
    }

    log.info({
      message: 'Receipt validation job done',
      count: processedUserIds.length,
      failedUserIds,
    });
    process.exit(0);
  })
  .catch(err => {
    log.error('Receipt validation job initialization failed. Cannot connect to a data source', err);
    process.exit(1);
  });
