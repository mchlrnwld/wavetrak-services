import {} from 'newrelic';
import http from 'http';
import express from 'express';
import logger from '../common/logger';

const log = logger('iap-status-service');
const app = express();

app.server = http.createServer(app);
app.get('/health', (req, res) => res.send('OK'));

app.server.listen(process.env.EXPRESS_PORT || 8004);
log.debug(`Started on port ${app.server.address().port}`);

export default app;
