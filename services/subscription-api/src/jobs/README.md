# In-app purchase receipt validation job

Configure global environment variables:
   1. MONGO_CONNECTION_STRING
   2. Google IAP keys: SURFLINE_GOOGLE_IAP_PUBLICKEY_LIVE, BUOYWEATHER_GOOGLE_IAP_PUBLICKEY_LIVE, FISHTRACK_GOOGLE_IAP_PUBLICKEY_LIVE, PLAY_STORE_API_ACCESS_TOKEN, PLAY_STORE_API_REFRESH_TOKEN, PLAY_STORE_API_CLIENT_ID, PLAY_STORE_API_CLIENT_SECRET
   3. APPLE_IAP_PASSWORD

Configuring the project
`npm install`

Testing the project
`npm run tools:receipt_validation_job`

Running the Docker container

```
cd subscription-service
docker build -t subscription-jobs .
docker run -p 8080:8080 subscription-jobs
```

## Background

In-app purchases from Apple and Google need to be verified periodically. We are only notified of new receipts by the clients. Scenarios like refunds could happen where we need to poll Apple to re-validate entitlements.
