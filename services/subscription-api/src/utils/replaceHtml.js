/**
 * Html encodes < and > to \&lt; and \&gt; to prevent tags from rendering as HTML. This does not
 * strip or encode anything else.
 * @param {*} inputStr
 * @returns
 */
export const replaceHtml = inputStr => inputStr.replace(/</g, '&lt;').replace(/>/g, '&gt;');

export default replaceHtml;
