const isArray = data => Array.isArray(data);

const isObject = data => data === Object(data) && !isArray(data) && typeof data !== 'function';

export const toCamel = str =>
  str.replace(/([-_][a-z])/gi, $1 =>
    $1
      .toUpperCase()
      .replace('-', '')
      .replace('_', ''),
  );

export const keysToCamel = data => {
  if (isObject(data)) {
    const newObj = {};
    Object.keys(data).forEach(key => {
      newObj[toCamel(key)] = keysToCamel(data[key]);
    });
    return newObj;
  }
  if (isArray(data)) {
    return data.map(element => keysToCamel(element));
  }
  return data;
};
