export const googleProductMap = {
  'surfline.android.1month.subscription.notrial': 'sl_premium',
  'surfline.android.1year.subscription.notrial': 'sl_premium',
  'surfline.android.monthly.subscription.notrial': 'sl_premium',
  'surfline.android.yearly.subscription.notrial': 'sl_premium',
  'surfline.android.annual.subscription.notrial': 'sl_premium',
  'surfline.android.yearly.2022': 'sl_premium',
  'surfline.android.monthly.2022': 'sl_premium',
  'buoyweather.android.1month.subscription': 'bw_premium',
  'buoyweather.android.1year.subscription': 'bw_premium',
  'fishtrack.android.1month.subscription': 'fs_premium',
  'fishtrack.android.1year.subscription': 'fs_premium',
  'fishtrack.android.yearly.subscription.notrial': 'fs_premium',
  FS30APP: 'fs_premium',
  FS365APP: 'fs_premium',
  BW30APP: 'bw_premium',
  BW365APP: 'bw_premium',
};

export const isActive = ({ expiryTimeMillis, paymentState, autoRenewing }) => {
  if (paymentState === 0 && autoRenewing) return true;
  const expiresDateSeconds = expiryTimeMillis / 1000;
  const dateSeconds = new Date() / 1000;
  return expiresDateSeconds > dateSeconds;
};

export const getSubscriptionDataFromGooglePurchase = googlePurchase => {
  const {
    _id: purchaseId,
    user,
    productId,
    packageName,
    data: {
      startTimeMillis,
      expiryTimeMillis,
      autoRenewing,
      paymentState,
      orderId,
      cancelReason,
      countryCode,
    },
  } = googlePurchase;

  return {
    type: 'google',
    user,
    orderId,
    subscriptionId: orderId,
    active: isActive({ expiryTimeMillis, paymentState, autoRenewing }),
    baseReceipt: purchaseId,
    productId,
    packageName,
    start: Math.trunc(startTimeMillis / 1000),
    end: Math.trunc(expiryTimeMillis / 1000),
    trialing: paymentState === 2,
    expiring: !autoRenewing,
    cancellationReason: cancelReason,
    countryCode,
    alertPayment: paymentState === 0,
    entitlement: googleProductMap[productId],
  };
};
