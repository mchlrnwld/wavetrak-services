import mongoose from 'mongoose';

export default (subscription, signature = null) => {
  const { trialing, start, end, active, user, type } = subscription;

  const userAccount = {
    user,
    archivedSubscriptions: [],
    subscriptions: [],
    entitlements: [],
    premiumPending: [],
    winbacks: [],
    receipts: [],
    createdAt: new Date(),
    updatedAt: new Date(),
    _id: mongoose.Types.ObjectId(),
  };

  /* eslint-disable no-param-reassign */
  subscription.onTrial = trialing;
  subscription.periodStart = new Date(start * 1000).toISOString();
  subscription.periodEnd = new Date(end * 1000).toISOString();
  /* eslint-enable no-param-reassign */
  if (active) {
    userAccount.subscriptions.push(subscription);
  } else {
    userAccount.archivedSubscriptions.push(subscription);
  }

  if (type === 'apple') {
    userAccount.receipts.push({ type: 'apple', receiptData: subscription.latestReceipt });
  }
  if (type === 'google') {
    userAccount.receipts.push({ type: 'google', receiptData: { data: {}, signature } });
  }

  return userAccount;
};
