/**
 * the 'notrial' suffix is a legacy hack to address a problem in the
 * first release of IAP in iOS. Don't reuse. These will be deprecated at a later date.
 */
export const iTunesProductMap = {
  'surfline.iosapp.1month.subscription.notrial': 'sl_premium',
  'surfline.iosapp.1year.subscription.notrial': 'sl_premium',
  'surfline.iosapp.1month.subscription': 'sl_premium',
  'surfline.iosapp.1year.subscription': 'sl_premium',
  'buoyweather.iosapp.1month.subscription': 'bw_premium',
  'buoyweather.iosapp.1year.subscription': 'bw_premium',
  'fishtrack.iosapp.1month.subscription': 'fs_premium',
  'fishtrack.iosapp.1year.subscription': 'fs_premium',
  'fs.iosapp.1month.subscription': 'fs_premium',
  'fs.iosapp.1year.subscription': 'fs_premium',
};

export const nonRetryableStatusCodes = [21003, 21007, 21010];

export const isActive = (expiresDateMs, receiptExpired, cancellationDateMs) => {
  if (receiptExpired || cancellationDateMs) return false;
  const expiresDateSeconds = parseInt(expiresDateMs, 10) / 1000;
  const dateSeconds = new Date() / 1000;
  return expiresDateSeconds > dateSeconds;
};

export const sortLatestReceiptInfo = latestReceiptInfo => {
  if (latestReceiptInfo.length === 1) return latestReceiptInfo;
  const sortableLatestReceiptInfo = latestReceiptInfo.filter(
    ({ purchaseDateMs }) => purchaseDateMs,
  );
  return sortableLatestReceiptInfo.sort((prev, next) => {
    const prevExp = parseInt(prev.purchaseDateMs, 10);
    const nextExp = parseInt(next.purchaseDateMs, 10);
    return nextExp - prevExp;
  });
};

export const getSubscriptionDataFromItunesReceipt = receipt => {
  const {
    _id: receiptId,
    user,
    originalTransactionId,
    latestReceipt,
    latestReceiptInfo,
    pendingRenewalInfo,
    expired: receiptExpired,
  } = receipt;

  const {
    expirationIntent,
    autoRenewProductId,
    autoRenewStatus,
    isInBillingRetryPeriod,
  } = pendingRenewalInfo.find(
    ({ originalTransactionId: pendingOriginalTransactionId }) =>
      pendingOriginalTransactionId === originalTransactionId,
  );

  const [
    {
      productId,
      transactionId,
      purchaseDateMs,
      expiresDateMs,
      cancellationDateMs,
      isTrialPeriod,
      cancellationReason,
    },
  ] = sortLatestReceiptInfo(latestReceiptInfo);

  const expiredProps = isActive(expiresDateMs, receiptExpired)
    ? {}
    : { expired: parseInt(expiresDateMs / 1000, 10) };

  const cancellationProps = !cancellationDateMs
    ? {}
    : { expired: cancellationDateMs / 1000, cancellationReason };

  return {
    type: 'apple',
    subscriptionId: transactionId,
    user,
    active: isActive(expiresDateMs, receiptExpired, cancellationDateMs),
    productId,
    entitlement: iTunesProductMap[productId],
    start: parseInt(purchaseDateMs, 10) / 1000,
    end: parseInt(expiresDateMs, 10) / 1000,
    expiring: autoRenewStatus === '0',
    trialing: isTrialPeriod === 'true',
    originalTransactionId,
    transactionId,
    baseReceipt: receiptId,
    latestReceipt,
    autoRenewProductId,
    expirationIntent,
    alertPayment: !!isInBillingRetryPeriod,
    ...expiredProps,
    ...cancellationProps,
  };
};

export const getSubscriptionDataFromItunesNotification = notification => {
  const {
    unifiedReceipt: { latestReceipt, latestReceiptInfo, pendingRenewalInfo },
  } = notification;

  const [
    {
      cancellationDateMs,
      cancellationReason,
      transactionId,
      originalTransactionId,
      purchaseDateMs,
      expiresDateMs,
      isTrialPeriod,
      productId,
    },
  ] = sortLatestReceiptInfo(latestReceiptInfo);

  const {
    autoRenewProductId,
    autoRenewStatus,
    autoRenewStatusChangeDateMs,
    isInBillingRetryPeriod,
  } = pendingRenewalInfo.find(
    ({ originalTransactionId: pendingOriginalTransactionId }) =>
      pendingOriginalTransactionId === originalTransactionId,
  );

  const expiredProps = isActive(expiresDateMs)
    ? {}
    : { expired: parseInt(expiresDateMs / 1000, 10) };

  const cancellationProps = !cancellationDateMs
    ? {}
    : { expired: cancellationDateMs / 1000, cancellationReason };

  return {
    type: 'apple',
    subscriptionId: transactionId,
    active: isActive(expiresDateMs),
    productId,
    entitlement: iTunesProductMap[productId],
    start: parseInt(purchaseDateMs, 10) / 1000,
    end: parseInt(expiresDateMs, 10) / 1000,
    expiring: autoRenewStatus === '0',
    trialing: isTrialPeriod === 'true',
    originalTransactionId,
    transactionId,
    latestReceipt,
    autoRenewProductId,
    autoRenewStatusChangeDate: parseInt(autoRenewStatusChangeDateMs, 10) / 1000,
    alertPayment: !!isInBillingRetryPeriod,
    ...expiredProps,
    ...cancellationProps,
  };
};
