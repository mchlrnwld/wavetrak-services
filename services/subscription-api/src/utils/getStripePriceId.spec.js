import { expect } from 'chai';
import getStripePriceId from './getStripePriceId';

describe('getStripePriceId', () => {
  it('should return matching Stripe Price Ids for SL/BW/FS', () => {
    expect(getStripePriceId('sl', 'usd', 'year')).to.equal('price_1KMDTZLwFGcBvXawjCayM4YC');
    expect(getStripePriceId('SL', 'USD', 'MONTH')).to.equal('price_1KMDRPLwFGcBvXawEz531QXD');
    expect(getStripePriceId('sl', 'aud', 'YEAR')).to.equal('sl_annual_aud');
    expect(getStripePriceId('sl', 'eur', 'YEAR')).to.equal('sl_annual_eur');

    expect(getStripePriceId('BW', 'USD', 'YEAR')).to.equal('bw_annual_v2');
    expect(getStripePriceId('bw', 'usd', 'month')).to.equal('bw_monthly');
    expect(getStripePriceId('bw', 'aud', 'YEAR')).to.equal('bw_annual_aud');
    expect(getStripePriceId('bw', 'eur', 'YEAR')).to.equal('bw_annual_eur');

    expect(getStripePriceId('fs', 'USD', 'year')).to.equal('fs_annual_v2');
    expect(getStripePriceId('FS', 'USD', 'month')).to.equal('fs_monthly');
    expect(getStripePriceId('fs', 'aud', 'YEAR')).to.equal('fs_annual_aud');
    expect(getStripePriceId('fs', 'eur', 'YEAR')).to.equal('fs_annual_eur');
  });

  it('should return falsey with invalid parameters', () => {
    expect(!!getStripePriceId('foo', 'bar', '')).to.equal(false);
    expect(!!getStripePriceId(null, null, null)).to.equal(false);
  });
});
