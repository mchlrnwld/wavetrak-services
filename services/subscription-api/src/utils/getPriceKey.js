import getConfig from '../config';

/**
 * Looks up and returns a Stripe Price Key mapping. Price IDs in stripe are defined
 * unconventionally from what the Subscription Ids expect.  This function returns the
 * more convential Price Mapping Key for Stripe Prices.
 * [Stripe Prices API.](https://stripe.com/docs/api/prices/list)
 * @param priceId A stripe plice id or subscription planId
 * @returns
 */
const getPriceKey = priceId => {
  const { priceIdMap } = getConfig();
  const priceKey = Object.keys(priceIdMap).find(key => priceIdMap[key] === priceId) || priceId;
  return priceKey;
};

export default getPriceKey;
