export default brandInitials => {
  const brandInitialMap = {
    sl: 'Surfline',
    bw: 'Buoyweather',
    fs: 'Fishtrack',
    msw: 'Magicseaweed',
    cw: 'CostalWatch',
  };
  return brandInitialMap[brandInitials];
};
