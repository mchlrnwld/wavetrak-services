import { expect } from 'chai';
import { replaceHtml } from './replaceHtml';

describe('replaceHtml', () => {
  it('should detect and replace "<" and ">"', () => {
    expect(replaceHtml('<h1>test')).to.equal('&lt;h1&gt;test');
    expect(replaceHtml('<h1>test</h1>')).to.equal('&lt;h1&gt;test&lt;/h1&gt;');
    expect(replaceHtml('<h1 data="true">test')).to.equal('&lt;h1 data="true"&gt;test');
    expect(replaceHtml('<3 feet and >25 feet')).to.equal('&lt;3 feet and &gt;25 feet');
  });
});
