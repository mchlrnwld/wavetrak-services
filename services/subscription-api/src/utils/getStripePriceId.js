import getConfig from '../config';

/**
 * Looks up and returns a Stripe PriceId mapping.
 * We have made the decision to hardcode these since they are rarely changed, but they should
 * correspond to the values we receive from the
 * [Stripe Prices API.](https://stripe.com/docs/api/prices/list)
 *
 * @param brand - The brand (sl, fs, or bw)
 * @param currency - The currency of the price
 * @param interval - YEAR or MONTH
 * @param intervalCount - Only applies to gifts, e.g. 1, 3, or 12 months
 * @returns
 */
const getStripePriceId = (brand, currency, interval, intervalCount) => {
  const { priceIdMap } = getConfig();

  const key = intervalCount
    ? `${brand?.toUpperCase()}_GIFT_${intervalCount}_${interval?.toUpperCase()}_${currency?.toUpperCase()}`
    : `${brand?.toUpperCase()}_${interval?.toUpperCase()}_${currency?.toUpperCase()}`;

  return priceIdMap[key];
};

export default getStripePriceId;
