export default v1StripeSubscription => {
  const {
    user,
    stripeCustomerId,
    stripePlanId,
    onTrial,
    periodStart,
    periodEnd,
  } = v1StripeSubscription;

  const start = new Date(periodStart).getTime() / 1000;
  const end = new Date(periodEnd).getTime() / 1000;
  return {
    ...v1StripeSubscription,
    user,
    stripeCustomerId,
    planId: stripePlanId,
    trialing: onTrial,
    start,
    end,
  };
};
