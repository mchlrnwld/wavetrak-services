import getPriceKey from './getPriceKey';

export const stripeProductMap = {
  sl: 'sl_premium',
  fs: 'fs_premium',
  bw: 'bw_premium',
};

// eslint-disable-next-line import/prefer-default-export
export const getEntitlementFromStripePlanId = planId => {
  const priceKey = getPriceKey(planId);
  const deconstructedPlanId = priceKey.toLowerCase().split('_');
  return stripeProductMap[deconstructedPlanId[0]];
};
