import { APIError } from '@surfline/services-common';

/**
 * @description Gift checkout request validation middleware.
 *
 * @export
 * @param {*} req
 * @param {*} res
 * @param {*} next
 *
 * @typedef {*} req
 * @property {object} body
 *
 * @typedef {object} body
 * @property {string} brand - The product or brand (sl|fs|bw) representing the purchased product
 * @property {string} interval - The period (month|year) representing the purchased product
 * @property {integer} invervalCount - The period amount representing the purchased product
 * @property {integer} country - The country code representing the purchased product
 * @property {string} purchaserEmail - A registered or anonymous user's email (the "from" email)
 * @property {string} purchaserName - A registered or anonymous user's name (the "from" name)
 * @property {string} recipientEmail - The email passed downstream to the email delivery service
 * @property {string} recipientName - A name included in the Gift Code delivery email
 *
 * @returns {error|next} - 400 API Error if validation fails
 */
export default (req, res, next) => {
  const { body } = req;
  const requiredFields = [
    'brand',
    'interval',
    'intervalCount',
    'country',
    'purchaserEmail',
    'purchaserName',
    'recipientEmail',
    'recipientName',
  ];
  const result = requiredFields.every(field => Object.prototype.hasOwnProperty.call(body, field));

  if (!result) {
    return next(new APIError('missing or malformed gift purchase params', { statusCode: 400 }));
  }
  return next();
};
