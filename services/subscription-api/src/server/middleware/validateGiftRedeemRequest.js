import { APIError } from '@surfline/services-common';
import { getActiveSubscriptionByUserIdAndEntitlement } from '../../models/subscriptions';
import getStripePriceId from '../../utils/getStripePriceId';

/**
 * @description Authenicates Gift Subscription Eligibility
 *
 * The middleware will return an error if the following exceptions
 * are met:
 *
 * 1. A subscription is an unsupported IAP or VIP (deprecated) type.
 * 2. The Gift Purchase charge has an open dispute.
 *
 * If an eligible subscription exists, the eligible subscription is defined onto
 * the request object.
 *
 * @export
 * @param {*} req
 * @param {*} res
 * @param {*} next
 *
 * @typedef {*} req
 * @property {object} body
 * @property {string} authenticatedUserId - The authorized user id added to this request.
 *
 * @typedef {object} body
 * @property {object} productDetails - the detaild of the gift purchase
 *
 * @returns {error|next} - 400 API Error
 *  - 1002: For ineligible in-app purchases
 *  - 1003: For purchases with open disputes
 */
export default async (req, res, next) => {
  const {
    authenticatedUserId,
    productDetails: { brand },
  } = req;
  const subscription = await getActiveSubscriptionByUserIdAndEntitlement(
    authenticatedUserId,
    brand,
  );
  if (!subscription) return next();
  const { type, dispute, planId } = subscription;

  if (type !== 'stripe' && type !== 'gift') {
    return next(
      new APIError('VIP and In-App Subscriptions are not eligible.', {
        statusCode: 400,
        wavetrakCode: 1002,
      }),
    );
  }

  const plan = planId;

  if (
    type === 'stripe' &&
    plan !== getStripePriceId(brand, 'USD', 'YEAR') &&
    plan !== getStripePriceId(brand, 'USD', 'MONTH') &&
    plan !== `${brand.toLowerCase()}_annual_v2` &&
    plan !== `${brand.toLowerCase()}_monthly_v2` &&
    plan !== `${brand.toLowerCase()}_annual` &&
    plan !== `${brand.toLowerCase()}_monthly`
  ) {
    return next(
      new APIError(
        'This gift card was purchased in USD and will not support credit toward your subscription currency at this time.',
        { statusCode: 400, wavetrakCode: 1004 },
      ),
    );
  }

  if (dispute && dispute !== 'won') {
    return next(
      new APIError('Your billing has been disputed. Please contact support@surfline.com.', {
        statusCode: 400,
        wavetrakCode: 1003,
      }),
    );
  }
  req.subscription = subscription;
  return next();
};
