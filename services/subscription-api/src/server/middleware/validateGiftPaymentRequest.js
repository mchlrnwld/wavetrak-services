import { APIError } from '@surfline/services-common';

/**
 * @description Gift payment request validation middleware.
 *
 * @export
 * @param {*} req
 * @param {*} res
 * @param {*} next
 *
 * @typedef {*} req
 * @property {object} body
 *
 * @typedef {object} body
 * @property {string} giftProductId - The document id representing the purchased product
 * @property {string} purchaserEmail - A registered or anonymous user's email (the "from" email)
 * @property {string} purchaserName - A registered or anonymous user's name (the "from" name)
 * @property {string} recipientEmail - The email passed downstream to the email delivery service
 * @property {string} recipientName - A name included in the Gift Code delivery email
 *
 * @returns {error|next} - 400 API Error if validation fails
 */
export default (req, res, next) => {
  const { body } = req;
  const requiredFields = [
    'giftProductId',
    'source',
    'purchaserEmail',
    'purchaserName',
    'recipientEmail',
    'recipientName',
  ];
  const result = requiredFields.every(field => Object.prototype.hasOwnProperty.call(body, field));
  if (!result) {
    return next(new APIError('missing or malformed gift purchase params'));
  }
  return next();
};
