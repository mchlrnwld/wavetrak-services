import { APIError } from '@surfline/services-common';
import { getCharge } from '../../external/stripe';
import { getRedeemCode } from '../../models/gifts/redeem';
import { getProductById } from '../../models/gifts/payment';

/**
 * @description Authenticates RedeemCode eligibility
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 *
 * @typedef {*} req
 * @property {object} body
 * @priperty {object} query
 *
 * @typedef {object} body
 * @property body {Object}
 * @property {string} code - the code to be redeemed
 * @property {string} brand - the product initials (sl, bw, fs)
 *
 * @typedef {object} query
 * @property {string} code - the code to be redeemed
 * @property {string} brand - the product initials (sl, bw, fs)
 *
 * @returns {error|next}
 * This middleware will return an error if the following exceptions
 * are met:
 *
 * 1. The code has been redeemed
 * 2. The requested product not match the gift product
 * 3. The gift purchase charge has an open dispute
 *
 * If no conditions are met the Gift Product is defined onto the req object.
 */
export default async (req, res, next) => {
  const { body, query } = req;
  const code = query.code || body.code;
  const brand = query.brand || body.brand;
  const redeemCode = await getRedeemCode(code);

  if (!redeemCode || redeemCode.redeemed) return next(new APIError('Invalid redeem code'));
  const { type } = redeemCode;

  if (type !== 'prices') {
    const { giftProduct: giftProductId } = redeemCode;
    const giftProduct = await getProductById(giftProductId);
    const { brand: product, price, duration, name, description } = giftProduct;
    if (brand !== product) return next(new APIError('Invalid redeem code product request'));

    const { chargeId } = redeemCode;
    if (chargeId) {
      const { dispute } = await getCharge(chargeId);
      if (dispute)
        return next(
          new APIError('Purchase charge disputed', { statusCode: 400, wavetrakCode: 1001 }),
        );
    }
    req.productDetails = { brand, price, duration, name, description };
    req.redeemCode = redeemCode;
    return next();
  }
  // TODO: Insert logic for "prices" based Gift codes
  return next();
};
