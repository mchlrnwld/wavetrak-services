const setQueryParams = obj => (req, res, next) => {
  req.query = { ...req.query, ...obj };
  return next();
};

export default setQueryParams;
