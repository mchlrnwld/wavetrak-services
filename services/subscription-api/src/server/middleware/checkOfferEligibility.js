import { APIError } from '@surfline/services-common';
import { getUserDetails } from '../../external/user';
import { getPromotion } from '../../external/promotion';
import {
  getActiveSubscriptionByUserIdAndEntitlement,
  getSubscriptionFromCustomParams,
} from '../../models/subscriptions';
import { getCoupon } from '../../external/stripe';

/**
 * @description This function validates eligiblity for GET and POST requests sent to the
 *  Promotional Offer API.
 *
 * @export
 * @param {*} req
 * @param {*} res
 * @param {*} next
 *
 * @typedef {*} req
 * @property {object} body
 * @property {object} query
 * @property {string} authenticatedUserId - The authorized user id added to this request.
 *
 * @typedef {object} body
 * @property {string} promotionId - The unique identifier for the Promotion Document
 *
 * @typedef {object} query
 * @property {string} promotionId - The unique identifier for the Promotion Document
 *
 * @returns {error|next}- 400 API Error if validation fails
 */
export default async (req, res, next) => {
  const { authenticatedUserId: userId, query, body } = req;
  const promotionId = query.promotionId || body.promotionId;

  const user = await getUserDetails(userId);
  const offer = await getPromotion(promotionId);

  if (!user || !offer) {
    return next(new APIError('Invalid user request', { statusCode: 400, wavetrakCode: 1001 }));
  }

  const {
    brand,
    eligibilityType,
    eligibilityTarget,
    eligiblePlan,
    eligibleStatus,
    stripeCouponCode,
  } = offer;

  const activeSubscription = await getActiveSubscriptionByUserIdAndEntitlement(user, brand);
  if (activeSubscription && eligibleStatus === 'expired') {
    return next(new APIError('Invalid offer request', { statusCode: 400, wavetrakCode: 1002 }));
  }

  const subscriptionSearchParams = {
    user: userId,
    active: eligibleStatus !== 'expired',
    expiring: eligibleStatus === 'expiring',
    entitlement: `${brand}_premium`,
    planId: eligiblePlan,
  };
  const subscription = await getSubscriptionFromCustomParams(subscriptionSearchParams);
  if (!subscription) {
    return next(new APIError('Ineligible offer request', { statusCode: 400, wavetrakCode: 1003 }));
  }

  const shouldAllowOffer = eligibilityTarget === 0;
  const { renewalCount } = subscription;
  if (eligibilityType === 'renewals' && renewalCount !== eligibilityTarget && !shouldAllowOffer) {
    return next(new APIError('Ineligible offer request', { statusCode: 400, wavetrakCode: 1003 }));
  }

  const { metadata } = await getCoupon(stripeCouponCode);
  if (metadata.promotion !== promotionId) {
    return next(new APIError('Ineligible offer request', { statusCode: 400, wavetrakCode: 1004 }));
  }

  req.user = user;
  req.offer = offer;
  req.subscription = subscription;
  return next();
};
