import { APIError } from '@surfline/services-common';
import { getPromotion, verifyPromoCode } from '../../external/promotion';
import { getActiveSubscriptionByUserIdAndEntitlement } from '../../models/subscriptions';
import { getStripeSubscriptionByPromotionIdAndUser } from '../../models/subscriptions/stripe';
import getStripePriceId from '../../utils/getStripePriceId';
import getPriceKey from '../../utils/getPriceKey';

/**
 * @description Validates a Promotional Subscription request
 *
 * @export
 * @param {*} req
 * @param {*} res
 * @param {*} next
 *
 * @typedef {*} req
 * @property {object} body
 * @property {string} authenticatedUserId - The authorized user id added to this request.
 *
 * @typedef {object} body
 * @property {string} promotionId - The unique identifer for the requested promotion
 * @property {string} planId - The Stripe plan id assigned to the promotional subscription transaction.
 * @property {string} promoCode - The promotional redeem code if applicable.
 *
 * @returns {error|next} - 400 API Error if the following conidtion is met.
 *  - The promotion is not active
 *  - The plan does not exist
 *  - The plan does not apply to the requested promotion
 *  - An active subscription exists
 *  - An active subscription representing the request promotion exists
 */
const validatePromotionEligibility = ({ promotionRequired = true }) => async (req, res, next) => {
  const {
    body: { brand, currency, interval, promotionId, planId, promoCode },
    authenticatedUserId: userId,
  } = req;
  const promotion = promotionId ? await getPromotion(promotionId) : null;
  let priceId;
  if (brand && currency && interval) {
    priceId = getStripePriceId(brand, currency, interval) || planId;
  } else {
    priceId = planId;
  }

  if (promotion && priceId) {
    if (promotion.plans === 'annual') {
      promotion.plans = 'year';
    } else if (promotion.plans === 'monthly') {
      promotion.plans = 'month';
    }
    const { isActive, brand: promoBrand, plans, promoCodesEnabled } = promotion;
    if (promoCode && promoCodesEnabled) {
      await verifyPromoCode(promotionId, { promoCode });
    }
    const priceKey = getPriceKey(priceId).toLowerCase();
    const isValidPlan = priceKey?.includes(promoBrand);
    const isEligiblePlan =
      plans.includes('all') ||
      interval?.toLowerCase() === promotion.plans ||
      priceKey?.includes(plans);
    const subscription = await getStripeSubscriptionByPromotionIdAndUser(userId, promotionId);
    const activeSubscription = await getActiveSubscriptionByUserIdAndEntitlement(
      userId,
      promoBrand,
    );

    if (isActive && isValidPlan && isEligiblePlan && !subscription && !activeSubscription) {
      // TODO determine if expired annual subscribers are eligible for the freeTrial promotion
      req.promotion = promotion;
    }
  }
  if (promotionRequired && !req.promotion)
    return next(new APIError('Cannot access promotion', 400));

  return next();
};

export default validatePromotionEligibility;
