export default (req, res, log, name, next) => {
  log.trace({
    action: name,
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  next();
};
