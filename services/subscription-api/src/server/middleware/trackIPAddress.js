import newrelic from 'newrelic';
import { APIError } from '@surfline/services-common';
import { cacheClient } from '../../common/dbContext';

/**
 * Checks (and sets) the number of requests per ipAddress per day.  This prevents
 * abuse of the POST /upgrade endpoint.  Limits request to 5 per user for a 24 hour period.
 */
export default (req, res, next) => {
  const nodeEnv = process.env.NODE_ENV;
  if (nodeEnv === 'sandbox' || nodeEnv === 'test') {
    return next();
  }

  const rateLimit = parseInt(process.env.SUBS_IP_RATE_LIMIT, 10);
  const expires = parseInt(process.env.SUBS_IP_RATE_LIMIT_TTL, 10);

  const xforwardedFor = req.headers['x-forwarded-for'];
  const ipAddresses = xforwardedFor.split(',');
  const ipAddress = ipAddresses[0];
  newrelic.addCustomAttribute('IP Address', ipAddress);

  return cacheClient().get(`subsrequest:${ipAddress}`, (err, reply) => {
    if (err) {
      newrelic.noticeError(err);
      return next(new APIError('Cannot process request', 429));
    }
    if (!reply) {
      cacheClient().set(`subsrequest:${ipAddress}`, 1, 'EX', expires);
      return next();
    }
    if (reply && reply < rateLimit) {
      cacheClient().incr(`subsrequest:${ipAddress}`);
      return next();
    }
    return next(new APIError('Cannot process request', 429));
  });
};
