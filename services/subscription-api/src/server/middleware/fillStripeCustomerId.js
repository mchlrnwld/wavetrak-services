import { getStripeCustomerId } from '../../models/customers/stripe';

const fillStripeCustomerId = async (req, res, next) => {
  req.stripeCustomerId = req.authenticatedUserId
    ? await getStripeCustomerId(req.authenticatedUserId)
    : null;
  return next();
};

export default fillStripeCustomerId;
