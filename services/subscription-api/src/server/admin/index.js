import { Router } from 'express';
import * as bodyParser from 'body-parser';
import coupons from './coupons';
import subscriptions from './subscriptions';
import subscriptionUsers from '../subscription-users/admin';

const jsonBodyParser = bodyParser.json();
/**
 * NOTE: This file will be used once the v2 Admin APIs are complete and the v1
 * Admin APIs have been deprecated.
 *
 */
export default () => {
  const api = Router();

  api.use('*', jsonBodyParser);
  api.use('/coupons', coupons());
  api.use('/subscriptions', subscriptions());
  api.use('/subscription-users', subscriptionUsers());

  return api;
};
