import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import * as bodyParser from 'body-parser';
import trackRequest from '../middleware/trackRequest';
import {
  createCoupon as createStripeCoupon,
  getCoupon as getStripeCoupon,
  deleteCoupon as deleteStripeCoupon,
} from '../../external/stripe';

export default () => {
  const api = Router();
  const jsonBodyParser = bodyParser.json();

  /**
   * @description - Admin API for creating Coupon objects in Stripe
   *
   * @param {String} name - stripe coupon id and name
   * @param {String} durantion - Specifies how long the discount will be in effect.
   * @param {Number} percentOff - A positive float larger than 0, and smaller or equal to 100, that represents the discount the coupon will apply.
   * @param {Number} maxRedemptions - Maximum number of times this coupon can be redeemed, in total, across all customers, before it is no longer valid.
   * @param {String} userId - A wavetrak user id.
   *
   * @returns {coupon} - Stripe Coupon object
   */
  const createCoupon = async (req, res) => {
    const {
      body: { name, percentOff, duration, maxRedemptions, userId },
    } = req;
    const coupon = await createStripeCoupon({
      name,
      percentOff,
      duration,
      metadata: { userId: userId.toString(), origin: 'admin' },
      maxRedemptions,
    });
    return res.send({ coupon });
  };

  /**
   * @description - Admin API for retrieving a Coupon object in Stripe
   *
   * @query {String} id - stripe coupon id to retrieve
   * @returns {coupon} - Stripe Coupon object
   */
  const getCoupon = async (req, res) => {
    const {
      query: { id },
    } = req;

    const coupon = await getStripeCoupon(id);
    return res.send({ coupon });
  };

  /**
   * @description - Admin API for deleting a Coupon object in Stripe
   *
   * @params {String} id - stripe coupon id to delete
   * @returns {object} - Success message
   */
  const deleteCoupon = async (req, res) => {
    const {
      body: { id },
    } = req;
    await deleteStripeCoupon(id);

    return res.send({ message: `Successfully deleted coupon: ${id}` });
  };

  api.use('*', jsonBodyParser);
  api.post(
    '/',
    trackRequest,
    wrapErrors((req, res) => createCoupon(req, res)),
  );
  api.get(
    '/',
    trackRequest,
    wrapErrors((req, res) => getCoupon(req, res)),
  );
  api.delete(
    '/',
    trackRequest,
    wrapErrors((req, res) => deleteCoupon(req, res)),
  );

  return api;
};
