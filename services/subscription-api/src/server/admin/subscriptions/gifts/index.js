import { wrapErrors } from '@surfline/services-common';
import * as bodyParser from 'body-parser';
import { Router } from 'express';
import trackRequest from '../../../middleware/trackRequest';
import { expireGiftSubscription } from '../../../../models/subscriptions/gift';
import { trackExpiredSubscription } from '../../../../tracking/expiredSubscription';

const jsonBodyParser = bodyParser.json();

const giftRouter = () => {
  const api = Router();

  const expireGiftSubscriptionReq = async (req, res) => {
    const { subscriptionId, date } = req.body;

    if (!subscriptionId) return res.status(400).json({ message: 'subscriptionId is required' });
    const expireBefore = date ?? new Date() / 1000;
    const result = await expireGiftSubscription(subscriptionId, expireBefore);

    if (result) {
      trackExpiredSubscription(result);
      return res.json({ message: 'Successfully expired gift subscription' });
    }
    return res.status(404).json({ message: 'no valid record found' });
  };

  api.use('*', jsonBodyParser);
  api.delete(
    '/',
    trackRequest,
    wrapErrors((req, res) => expireGiftSubscriptionReq(req, res)),
  );
  return api;
};

export default giftRouter;
