import { setupExpress } from '@surfline/services-common';
import logger from '../common/logger';

import v1admin from './v1/admin';
import inAppPurchaseIOS from './v1/iap/inAppPurchaseIOS';
import inAppPurchaseGoogle from './v1/iap/inAppPurchaseGoogle';
import subscriptions from './v1/subscriptions';
import stripeV1Integrations from './v1/integrations/stripe';

import cards from './cards';
import itunes from './subscriptions/itunes';
import google from './subscriptions/google';
import billing from './subscriptions/stripe';
import promotion from './subscriptions/promotion';
import offer from './subscriptions/offer';
import checkout from './subscriptions/stripe/checkout';

import itunesIntegrations from './integrations/itunes';
import googleIntegrations from './integrations/google';
import stripeIntegrations from './integrations/stripe';
import subscriptionEvents from './integrations/events';

import payment from './gifts/payment';
import redeem from './gifts/redeem';
import giftsVerify from './gifts/verify';
import giftCheckout from './gifts/checkout';

import alerts from './alerts';
import coupons from './admin/coupons';
import invoices from './invoices';
import pricingPlans from './pricing-plans';

import adminSubscriptionsGifts from './admin/subscriptions/gifts';
import adminSubscriptionUsers from './subscription-users/admin';
import adminSubscriptions from './subscriptions/wavetrak/admin';
import adminGooglePurchases from './subscription-purchases/google/admin';
import adminItunesPurchases from './subscription-purchases/itunes/admin';

import * as dbContext from '../common/dbContext';

const log = logger('subscription-service');

/**
 * @description Custom error handler that sits above the generic surfline error
 * handler and catches specific errors to the service in order to send custom
 * messages back to the user.
 */
const subscriptionErrorHandler = (err, _, res, next) => {
  const recognizedErrors = ['StripeError', 'DuplicateCardError'];
  if (recognizedErrors.indexOf(err.name) === -1) {
    return next(err);
  }
  return res.send(err.statusCode).send({
    message: err.message,
    ...(err.wavetrakCode && { code: err.wavetrakCode }),
  });
};

const setupApp = () =>
  setupExpress({
    log,
    port: process.env.EXPRESS_PORT || 8004,
    name: 'subscription-service',
    handlers: [
      /* V2 APIs */
      ['/credit-cards', cards()],
      ['/subscriptions/itunes', itunes()],
      ['/subscriptions/google', google()],
      ['/subscriptions/billing/checkout', checkout()],
      ['/subscriptions/billing', billing()],
      ['/subscriptions/promotion', promotion()],
      ['/subscriptions/retention-offer', offer()],

      ['/integrations/google', googleIntegrations()],
      ['/integrations/itunes', itunesIntegrations()],
      ['/integrations/stripe', stripeIntegrations()],
      ['/integrations/subscriptions', subscriptionEvents()],

      ['/gift/payment', payment()],
      ['/gift/redeem', redeem()],
      ['/gifts/verify', giftsVerify()],
      ['/gift/checkout', giftCheckout()],

      ['/payment/alerts', alerts()],
      ['/invoices', invoices()],
      ['/pricing-plans', pricingPlans()],

      ['/admin/coupons', coupons()],
      ['/admin/subscriptions/wavetrak', adminSubscriptions()],
      ['/admin/purchases/google', adminGooglePurchases()],
      ['/admin/purchases/itunes', adminItunesPurchases()],
      ['/admin/subscriptions/gifts', adminSubscriptionsGifts()],
      ['/admin/subscription-users', adminSubscriptionUsers()],

      /* V1 APIs */
      ['/subscriptions', subscriptions()],
      ['/iap/ios', inAppPurchaseIOS()],
      ['/iap/google', inAppPurchaseGoogle()],
      ['/admin', v1admin()],
      ['/integrations/v1/stripe', stripeV1Integrations()],
      subscriptionErrorHandler,
    ],
  });

Promise.all([dbContext.initCache(), dbContext.connectMongo()])
  .then(setupApp)
  .catch(err => {
    log.error('App Initialization failed. Cannot connect to a data source', err);
    process.exit(1);
  });

export default setupApp;
