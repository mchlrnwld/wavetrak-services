import { Router } from 'express';
import * as bodyParser from 'body-parser';
import { wrapErrors, authenticateRequest } from '@surfline/services-common';
import toBrandName from '../../../utils/toBrandName';
import { getUserDetails } from '../../../external/user';
import trackRequest from '../../middleware/trackRequest';
import { createPurchasedRedeemCode } from '../../../models/gifts/redeem';
import { getCustomer, createCharge } from '../../../external/stripe';
import validatePaymentRequest from '../../middleware/validateGiftPaymentRequest';
import { getActiveProductsByBrand, getProductById } from '../../../models/gifts/payment';
import { getStripeCustomerId } from '../../../models/customers/stripe';
import fillStripeCustomerId from '../../middleware/fillStripeCustomerId';

const jsonBodyParser = bodyParser.json();

export default function giftPayments() {
  const api = Router();

  /**
   * @description Retrieves a list of active Gift Products specific to the brand
   * inlcuded in the request.
   *
   * @param {*} req
   * @param {*} res
   *
   * @typedef {*} req
   * @property {String} authenticatedUserId - The authorized user id added to this request.
   * @property {Object} query - Request query params
   *
   * @typedef {Object} query
   * @property {String} product - The application or "brand" making the request.
   *
   * @typedef {*} res
   * @property {Array} products - The gift products available for purchase
   * @property {String} email - The email value of a authorized user making the request if it exists
   * @property {Object} card - The default credit card details if they exist (https://stripe.com/docs/api/cards/object)
   *
   * @returns {res}
   * @example
   *    {
   *      products: [{<GiftProduct>}, {<GiftProduct>}],
   *      email: 'mmalchak@surfline.com' || null
   *      card: {<Stripe Card Object>} || null
   *    }
   */
  const get = async (req, res) => {
    const {
      authenticatedUserId: userId,
      query: { product: brand },
    } = req;

    const products = await getActiveProductsByBrand(brand);

    const user = userId ? await getUserDetails(userId) : {};
    const { email } = user;
    const { default_source: card } = req.stripeCustomerId
      ? await getCustomer(req.stripeCustomerId, ['default_source'])
      : {};

    return res.status(200).send({
      products,
      email: email || null,
      card: card || null,
    });
  };

  /**
   * @description Initiates a Stripe Charge for a Gift Product purchase
   *
   * @param {*} req
   * @param {*} res
   *
   * @typedef {*} req
   * @property {String} authenticatedUserId - The authorized user id added to this request.
   *
   * @typedef {object} body
   * @property {string} giftProductId - The document id representing the purchased product
   * @property {string} source - The Stripe Card id or the Stripe Token key to charge.
   * @property {string} purchaserEmail - A registered or anonymous user's email (the "from" email)
   * @property {string} purchaserName - A registered or anonymous user's name (the "from" name)
   * @property {string} recipientEmail - The email passed downstream to the email delivery service
   * @property {string} recipientName - A name included in the Gift Code delivery email
   * @property {string} message - A message included in the Gift Code delivery email
   *
   * @typedef {*} res
   * @property {string} message
   *
   * @returns res
   * @example
   *    {
   *      message: 'Successfully processed the Surfline Gift purchase'.
   *    }
   */
  const create = async (req, res) => {
    const {
      body: {
        giftProductId,
        source,
        purchaserEmail,
        purchaserName,
        recipientEmail,
        recipientName,
        message,
      },
      authenticatedUserId: userId,
    } = req;
    const product = await getProductById(giftProductId);
    const { brand, name, _id: id, description, price } = product;
    const statementDescriptor = `${toBrandName(brand)} Gift Card`;
    const stripeCustomerId = userId ? await getStripeCustomerId(userId) : {};

    const { id: chargeId } = await createCharge(
      product,
      source,
      purchaserEmail,
      statementDescriptor,
      stripeCustomerId,
    );

    await createPurchasedRedeemCode({
      type: 'legacy',
      productId: id,
      purchaserEmail,
      chargeId,
      stripeCustomerId,
      amount: price,
      currency: 'usd',
      purchaserName,
      recipientEmail,
      recipientName,
      message,
      userId,
      brand,
      description,
    });

    return res.send({ message: `Successfully processed the ${name} purchase.` });
  };

  api.use('*', jsonBodyParser);
  api.get(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: false, clientIdRequired: false }),
    fillStripeCustomerId,
    wrapErrors((req, res) => get(req, res)),
  );
  api.post(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: false }),
    validatePaymentRequest,
    wrapErrors((req, res) => create(req, res)),
  );

  return api;
}
