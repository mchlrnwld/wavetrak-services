import { Router } from 'express';
import * as bodyParser from 'body-parser';
import { wrapErrors, authenticateRequest } from '@surfline/services-common';
import trackRequest from '../../middleware/trackRequest';
import { updateRedeemCode } from '../../../models/gifts/redeem';
import authenticateRedeemCode from '../../middleware/authenticateRedeemCode';
import { updateStripeSubscription } from '../../../models/subscriptions/stripe';
import validateGiftRedeemRequest from '../../middleware/validateGiftRedeemRequest';
import { updateCharge, getCustomer, updateCustomer } from '../../../external/stripe';
import { createGiftSubscription, updateGiftSubscription } from '../../../models/subscriptions/gift';

const jsonBodyParser = bodyParser.json();

export default function giftPayments() {
  const api = Router();

  /**
   * @description Retrieves the active Gift Product details associated with a redeemable
   * gift code.  If the authorized user has an active subscription, the subscription
   * detail will also be returned in the request.
   *
   * @param {*} req
   * @param {*} res
   *
   * @typedef {*} req
   * @property {object} giftProduct - The Gift Product document properties
   * @property {object|null} subscription - The Subscription document properties, or null.
   *
   * @typedef {*} res
   * @property {object} giftProduct - The Gift Product document properties
   * @property {object|null} subscription - The Subscription document properties, or null.
   *
   * @returns res
   * @example
   *    {
   *      "giftProduct": {
   *         "_id": "5ba96eb86659ef56051f88ca",
   *         "brand": "sl",
   *         "name": "1-Month",
   *         "duration": 1,
   *         "price": 999,
   *         "isActive": true,
   *         "imagePath": "https://creatives.cdn-surfline.com/giftcards/cards/sl/1month.png",
   *         "bestValue": false,
   *         "description": "1 Month of Surfline Premium",
   *         "__v": 0
   *      },
   *      "subscription": null
   *    }
   */
  const get = async (req, res) => {
    const { subscription = null, productDetails } = req;
    return res.status(200).send({ giftProduct: productDetails, subscription });
  };

  /**
   * @description Processes the gift code redemption
   *
   * @param req
   * @param res
   *
   * @typedef {*} req
   * @property {string} authenticatedUserId - The authorized user id added to this request.
   * @property {object} giftproduct - The Gift Product document properties
   * @property {object} redeemCode - The Gift Code document properties
   * @property {object} body
   *
   * @typedef {object} body
   * @property {string} code - The redeem code associated with Gift Code document
   *
   * @typedef {*} res
   * @property {object} subscription - The new or updated subscription
   * @property {integer|null} accountBalance - The credit applied to a Stripe Customer account if a gift
   * code is redeemed with an active Stripe Subscription.
   *
   * @returns res
   * @example
   * {
   *    "subscription": {
   *      "redeemCodes": [
   *        null
   *      ],
   *      "_id": "5e5eaf7e437a6f1a9f11fd28",
   *      "type": "gift",
   *      "trialing": false,
   *      "expiring": true,
   *      "renewalCount": 0,
   *      "alertPayment": false,
   *      "subscriptionId": "gift_undefined",
   *      "user": "59cd2ca337380116cb292b2e",
   *      "active": true,
   *      "planId": "sl_gift",
   *      "entitlement": "sl_premium",
   *      "start": 1583263614,
   *      "end": 1585855614,
   *      "chargeId": "ch_1DHc2jCUgh9bySt61C9f8zI9",
   *      "createdAt": "2020-03-03T19:26:54.541Z",
   *      "updatedAt": "2020-03-03T19:26:54.541Z",
   *      "__v": 0
   *    },
   *    accountBalance": null
   * }
   *
   *
   */
  const create = async (req, res) => {
    const {
      authenticatedUserId,
      productDetails,
      redeemCode,
      body: { code },
    } = req;
    const { subscription: existingSubscription } = req;
    let subscription;
    let newOrModifiedAccountBalance = null;
    const { chargeId } = redeemCode;

    if (!existingSubscription) {
      subscription = await createGiftSubscription(authenticatedUserId, {
        ...productDetails,
        code,
        chargeId,
      });
    }
    const { type } = existingSubscription || {};
    if (type === 'gift') {
      subscription = await updateGiftSubscription(existingSubscription, {
        ...productDetails,
        code,
        chargeId,
      });
    }

    if (type === 'stripe') {
      const { stripeCustomerId } = existingSubscription;
      const { price } = productDetails;
      const { balance: accountBalance } = await getCustomer(stripeCustomerId);

      const giftCreditAccountBalance = accountBalance - price;
      const { balance: newAccountBalance } = await updateCustomer(stripeCustomerId, {
        balance: giftCreditAccountBalance,
      });
      newOrModifiedAccountBalance = newAccountBalance;
      subscription = await updateStripeSubscription({
        ...existingSubscription,
        redeemCodes: [...existingSubscription.redeemCodes, code],
      });
    }
    if (chargeId) {
      await updateCharge(chargeId, {
        redeemedBy: authenticatedUserId.toString(),
        redeemDate: new Date(),
        creditApplied: !!newOrModifiedAccountBalance,
      });
    }

    await updateRedeemCode(
      {
        ...redeemCode,
        user: authenticatedUserId.toString(),
        redeemed: true,
        redeemDate: new Date(),
      },
      subscription,
    );

    return res.status(200).send({
      subscription,
      accountBalance: newOrModifiedAccountBalance,
    });
  };

  api.use('*', jsonBodyParser);
  api.get(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    authenticateRedeemCode,
    validateGiftRedeemRequest,
    wrapErrors((req, res) => get(req, res)),
  );
  api.post(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    authenticateRedeemCode,
    validateGiftRedeemRequest,
    wrapErrors((req, res) => create(req, res)),
  );

  return api;
}
