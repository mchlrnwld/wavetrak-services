import { Router } from 'express';
import * as bodyParser from 'body-parser';
import { wrapErrors, authenticateRequest } from '@surfline/services-common';
import trackRequest from '../../middleware/trackRequest';
import { getCheckoutSession } from '../../../external/stripe';

const jsonBodyParser = bodyParser.json();
const EXPAND = ['line_items', 'payment_intent'];

export const giftsVerifyHandlers = () => {
  const api = Router();
  /**
   * Verifies if the Gift purchase is confirmed in Stripe Checkout.
   *  - Get the checkout session from Stripe
   *  - If the checkout session has a successful payment, return success and
   *    the metadata details of the purchase.
   * @param body.checkoutSessionId | String
   *
   * @returns res
   * @example
   *    {
   *      status: 'success',
          message: 'gift purchase verified`
   *    }
   */
  const verifyHandler = async (req, res) => {
    const {
      body: { checkoutSessionId },
    } = req;

    if (!checkoutSessionId)
      return res.status(400).send({
        status: 'fail',
        message: 'A checkoutSessionId is required',
      });

    const checkoutSession = await getCheckoutSession(checkoutSessionId, EXPAND);

    if (
      !checkoutSession ||
      !checkoutSession.payment_intent ||
      checkoutSession.payment_intent.status !== 'succeeded'
    ) {
      return res.status(400).send({
        status: 'fail',
        message: 'The checkoutSessionId is invalid',
      });
    }

    const {
      payment_intent: { amount, currency, metadata: paymentIntentMetadata },
      line_items: { data: lineItems },
    } = checkoutSession;

    const [
      {
        nickname,
        price: { metadata: priceMetadata },
      },
    ] = lineItems;

    return res.status(200).send({
      status: 'success',
      message: 'gift purchase verified',
      data: {
        ...priceMetadata,
        ...paymentIntentMetadata,
        amount,
        currency,
        nickname,
      },
    });
  };

  api.use('*', jsonBodyParser);
  api.post(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: false }),
    wrapErrors((req, res) => verifyHandler(req, res)),
  );

  return api;
};

export default giftsVerifyHandlers;
