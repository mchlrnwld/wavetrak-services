import { Router } from 'express';
import { json as jsonParser } from 'body-parser';
import { wrapErrors, authenticateRequest } from '@surfline/services-common';
import validateCheckoutRequest from '../../middleware/validateGiftCheckoutRequest';
import trackRequest from '../../middleware/trackRequest';
import { startCheckoutSession } from '../../../external/stripe';
import getStripePriceId from '../../../utils/getStripePriceId';

const REGEX_WAVETRAK_DOMAIN = /^https?:\/\/((staging|sandbox|www)\.)?(surfline|buoyweather|fishtrack)\.com/i;

const jsonBodyParser = jsonParser();

export default function giftCheckout() {
  const api = Router();

  /**
   *@description Initiates a Stripe Checkout session for a Gift purchase of Surfline,
   * FishTrack, or Buoyweather gift codes.
   *
   * @param {*} req
   * @param {*} res
   *
   * @typedef {*} req
   * @property {String} authenticatedUserId - The authorized user id added to this request (if available)
   *
   * @typedef {object} body
   *
   * @property {string} brand - The Wavetrak representing the purchase (sl|fs|bw)
   * @property {string} interval - A value representing length of pruchase (month)
   * @property {string} intervalCount - A value representing quantity of pruchase (1|3|12)
   * @property {string} country - The purchase origin used to compute gift price id
   * @property {string} successUrl - A redirect URL for valid gift purchases
   * @property {string} cancelUrl - A fallback URL for invalid gift purchases
   * @property {string} giftProductId - The document id representing the purchased product
   * @property {string} purchaserEmail - A registered or anonymous user's email (the "from" email)
   * @property {string} purchaserName - A registered or anonymous user's name (the "from" name)
   * @property {string} recipientEmail - The email passed downstream to the email delivery service
   * @property {string} recipientName - A name included in the Gift Code delivery email
   * @property {string} giftMessage - A message included in the Gift Code delivery email
   *
   * @typedef {*} res
   * @property {string} url
   *
   * @returns res
   * @example
   *    {
   *      "url": "https://checkout.stripe.com/pay/cs_test_a1izWBcC7g4fPGcInpEzPEV8oMIDLd5GGeVHw9h9Zek7vL7EAGdyiSwMuV#fidkdWxOYHwnPyd1blpxYHZxWkhgZzJIVX9IUmJnR1xyUDZxM2JoYU1CcScpJ2N3amhWYHdzYHcnP3F3cGApJ2lkfGpwcVF8dWAnPyd2bGtiaWBabHFgaCcpJ2BrZGdpYFVpZGZgbWppYWB3dic%2FcXdwYHgl"
   *      "sessionId": "cs_test_a1DPS5rSijCDH4xdSaSGMlpvGPJ4qesDJEu6geDWdsTahVL8Poj13qmSGX"
   *    }
   */
  const checkout = async (req, res) => {
    const {
      body: {
        brand,
        interval,
        intervalCount,
        currency,
        successUrl,
        cancelUrl,
        giftMessage,
        purchaserEmail,
        purchaserName,
        recipientEmail,
        recipientName,
      },
      authenticatedUserId: userId,
    } = req;

    const priceId = getStripePriceId(brand, currency, interval, intervalCount);

    if (!priceId) {
      return res.status(400).send({
        message: 'Must provide a priceId or a valid brand, currency, and interval.',
      });
    }

    const validUrls =
      REGEX_WAVETRAK_DOMAIN.test(successUrl) && REGEX_WAVETRAK_DOMAIN.test(cancelUrl);
    if (!validUrls && !['development', 'sandbox'].includes(process.env.NODE_ENV)) {
      return res.status(400).send({
        message: 'The success and cancel URLs need to be valid',
      });
    }

    const { url, id } = await startCheckoutSession({
      priceId,
      cancelUrl,
      successUrl,
      mode: 'payment',
      metadata: {
        brand,
        priceId,
      },
      paymentIntentData: {
        metadata: {
          type: 'gift_purchase',
          giftMessage,
          purchaserEmail,
          purchaserName,
          recipientEmail,
          recipientName,
          priceId,
          userId,
          brand,
        },
      },
    });
    return res.send({ url, sessionId: id });
  };

  api.use('*', jsonBodyParser);
  api.post(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: false }),
    validateCheckoutRequest,
    wrapErrors((req, res) => checkout(req, res)),
  );

  return api;
}
