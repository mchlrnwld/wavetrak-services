import { authenticateRequest, wrapErrors } from '@surfline/services-common';
import { Router } from 'express';
import * as bodyParser from 'body-parser';
import trackRequest from '../middleware/trackRequest';
import { getStripeSubscriptionsByUser } from '../../models/subscriptions/stripe';
import { payInvoices } from '../../external/stripe';

const jsonBodyParser = bodyParser.json();

const invoiceRouter = () => {
  const payInvoice = async (req, res) => {
    const { authenticatedUserId: userId } = req;
    const subscriptions = await getStripeSubscriptionsByUser(userId);
    const subscriptionsToPay = subscriptions
      .filter(({ failedPaymentAttempts }) => failedPaymentAttempts > 0)
      .map(({ subscriptionId }) => subscriptionId);

    if (subscriptionsToPay.length > 0) {
      const { stripeCustomerId } = subscriptions[0];
      const paidInvoices = await payInvoices(stripeCustomerId, subscriptionsToPay);
      return res.send(paidInvoices);
    }

    return res.send([]);
  };
  const api = Router();

  api.use('*', jsonBodyParser);
  api.post(
    '/pay',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    wrapErrors((req, res) => payInvoice(req, res)),
  );

  return api;
};

export default invoiceRouter;
