import { Router } from 'express';
import * as bodyParser from 'body-parser';
import { wrapErrors, authenticateRequest } from '@surfline/services-common';
import trackRequest from '../middleware/trackRequest';
import { getActiveSubscriptionByUserIdAndEntitlement } from '../../models/subscriptions';

export default function paymentAlerts() {
  const api = Router();
  const jsonBodyParser = bodyParser.json();

  /**
   * @description - Helper function that computes the payment alert response by parsing the
   * subscription type, and dispute or failed payment attributes.
   *
   * @param {Object} subscription - An active subscription object.
   * @returns {Object} - Includes the message, alert, and subscription type values.
   */
  const getPaymentAlert = async subscription => {
    const { type, alertPayment } = subscription;
    const alertResponse = {
      alert: alertPayment,
      message: 'Payment Failed.  Please contact support.',
      type,
    };
    if (type === 'apple') {
      alertResponse.message =
        'Payment Failed.  Please visit your iTunes account to resolve this issue.';
    }
    if (type === 'google') {
      alertResponse.message =
        'Payment Failed.  Please visit your Google-Play account to resolve this issue.';
    }
    if (type === 'stripe' || type === 'gift') {
      const { paymentAlertType } = subscription;
      const message =
        paymentAlertType === 'dispute'
          ? 'Your payment was disputed.  Please contact support.'
          : 'Payment Failed.  Your credit card could not be charged.  Please update your payment details to continue receiving your premium benefits.';
      alertResponse.message = message;
      alertResponse.type = 'web';
    }
    return alertResponse;
  };

  /**
   * @description - The Alerts API supports credit card dunning and payment disputes
   * specific to an active subscription with an outstanding invoice payment.
   *
   * @param {*} req
   * @param {*} res
   *
   * @typedef {Object} req
   * @property {string} authenticatedUserId - The authorized user id added to this request.
   * @property {Object} query - The query param attributes accompanying this request.
   *
   * @typedef {Object} query
   * @property {String} product - The application or "brand" making the request and parameter used to filter
   * the payment alerts returned by this API.
   *
   * @returns {Object}
   * @property {String} message - A string describing the reason a payment failed.
   * @property {Boolean} alert - Indicates if the alert should notifify the end user.
   * @property {String} type - Represents the subscription type; 'stripe', 'apple', 'google;
   *
   * @example
   *    { message: 'Your payment was disputed', alert: true, type: 'stripe' }
   *
   */
  const get = async (req, res) => {
    const {
      authenticatedUserId: userId,
      query: { product },
    } = req;
    let alertResponse = { message: 'No payment alerts found.', alert: false };
    const subscription = await getActiveSubscriptionByUserIdAndEntitlement(userId, product);
    if (!subscription) {
      return res.status(200).send(alertResponse);
    }

    const { type, alertPayment } = subscription;
    if (!alertPayment) {
      return res.status(200).send({ ...alertResponse, type });
    }

    alertResponse = await getPaymentAlert(subscription);
    return res.status(200).send(alertResponse);
  };

  api.use('*', jsonBodyParser);
  api.get(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    wrapErrors((req, res) => get(req, res)),
  );

  return api;
}
