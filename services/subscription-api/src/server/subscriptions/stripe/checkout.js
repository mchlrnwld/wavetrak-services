import { Router } from 'express';
import { json as jsonBodyParser } from 'body-parser';
import { authenticateRequest, wrapErrorsToNewRelic } from '@surfline/services-common';
import { createCustomer, listTaxRates, startCheckoutSession } from '../../../external/stripe';
import { getUserDetails } from '../../../external/user';
import fillStripeCustomerId from '../../middleware/fillStripeCustomerId';
import trackRequest from '../../middleware/trackRequest';
import validatePromotionEligibility from '../../middleware/validatePromotionEligibility';
import { createStripeCustomer } from '../../../models/customers/stripe';
import getStripePriceId from '../../../utils/getStripePriceId';
import { getEntitlementFromStripePlanId } from '../../../utils/stripe';
import {
  getActiveSubscriptionByUserIdAndEntitlement,
  getAllSubscriptionsByUserId,
} from '../../../models/subscriptions';

const REGEX_WAVETRAK_DOMAIN = /^https?:\/\/((staging|sandbox|www)\.)?(surfline|buoyweather|fishtrack)\.com/i;

/**
 * @description Attempts to initiates a Stripe Checkout session for a given Stripe customer.
 *
 * @param {*} req
 * @param {*} res
 *
 * @typedef {*} req
 * @property {String} authenticatedUserId - The authorized user id added to this request.
 * @property {String} stripeCustomerId - The associated Stripe Customer Id.
 * @property {object} offer - The associated and validated Stripe offer.
 *
 * @typedef {object} body
 * @property {string} brand - The associated brand identifier
 * @property {string} cancelUrl - URL to return the user to when they cancel Stripe Checkout
 * @property {string} currency - The user's currency based on an already established billing currency or their location.
 * @property {string} coupon - The designated coupon.
 * @property {string} interval - The interval of the subscription selected (month or year)
 * @property {string} planId - Optional field for passing the Stripe Plan Id directly.
 * @property {string} successUrl - URL to return the user to when they successfully complete Stripe Checkout
 *
 * @typedef {*} res
 * @property {string} message
 *
 * @returns res
 * @example
 *  {
 *     "url": "https://checkout.stripe.com/pay/.."
 *  }
 *
 */
const stripeCheckoutHandler = async (req, res) => {
  const {
    body: { brand, cancelUrl, coupon, currency, interval, planId, successUrl },
    authenticatedUserId,
    promotion,
  } = req;
  let { stripeCustomerId } = req;

  const priceId = getStripePriceId(brand, currency, interval) ?? planId;

  if (!priceId) {
    return res.status(400).send({
      message: 'Must provide a priceId or a valid brand, currency, and interval',
    });
  }

  const isActive = await getActiveSubscriptionByUserIdAndEntitlement(authenticatedUserId, brand);

  if (isActive) {
    return res.status(400).send({
      message: 'Cannot process request with existing active subscription.',
    });
  }

  const validUrls = REGEX_WAVETRAK_DOMAIN.test(successUrl) && REGEX_WAVETRAK_DOMAIN.test(cancelUrl);

  if (!validUrls && !['development', 'sandbox'].includes(process.env.NODE_ENV)) {
    return res.status(400).send({
      message: 'The success and cancel URLs need to be valid',
    });
  }

  if (!stripeCustomerId) {
    const { email, firstName, lastName } = await getUserDetails(authenticatedUserId);
    const { id } = await createCustomer({
      email,
      firstName,
      lastName,
      userId: authenticatedUserId,
    });
    await createStripeCustomer(authenticatedUserId, id);
    stripeCustomerId = id;
  }

  let trialPeriodDays;

  if (interval?.toLowerCase() === 'year') {
    const subscriptions = await getAllSubscriptionsByUserId(authenticatedUserId);

    const { entitlement: wasPremium } =
      subscriptions.find(
        ({ entitlement }) => entitlement === getEntitlementFromStripePlanId(priceId),
      ) || {};

    if (!wasPremium) {
      if (promotion && promotion.trialLength) trialPeriodDays = promotion.trialLength;
      else if (!coupon) trialPeriodDays = 15;
    }
  }

  const taxRates = await listTaxRates();
  const defaultTaxRates = taxRates.reduce((results, taxRate) => {
    if (taxRate.metadata.plans?.includes(priceId)) {
      results.push(taxRate.id);
    }
    return results;
  }, []);

  const { url } = await startCheckoutSession({
    cancelUrl,
    clientReferenceId: authenticatedUserId,
    coupon: promotion ? promotion.stripeCouponCode : coupon,
    priceId,
    stripeCustomerId,
    successUrl,
    metadata: {
      brand,
      priceId,
    },
    mode: 'subscription',
    subscriptionData: {
      default_tax_rates: defaultTaxRates,
      trial_period_days: trialPeriodDays ?? undefined,
      metadata: {
        brand,
        interval,
        promotionId: promotion?._id,
      },
    },
  });
  return res.send({ url });
};

export const stripeCheckout = () => {
  const api = Router();

  api.use('*', jsonBodyParser());
  api.post(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    validatePromotionEligibility({ promotionRequired: false }),
    fillStripeCustomerId,
    wrapErrorsToNewRelic((req, res) => stripeCheckoutHandler(req, res)),
  );

  return api;
};

export default stripeCheckout;
