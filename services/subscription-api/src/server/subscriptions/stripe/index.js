import { Router } from 'express';
import newrelic from 'newrelic';
import * as bodyParser from 'body-parser';
import { wrapErrors, authenticateRequest, APIError } from '@surfline/services-common';
import { getUserDetails } from '../../../external/user';
import trackRequest from '../../middleware/trackRequest';
import { validateCard } from '../../../models/v1/credit-cards';
import { shouldProcessCardSource } from '../../../external/stripeutils';
import { trackCreditCardFingerprint } from '../../../common/trackEvent';
import {
  getAllSubscriptionsByUserId,
  getActiveSubscriptionByUserIdAndEntitlement,
} from '../../../models/subscriptions';
import {
  createCustomer,
  updateCustomer,
  getToken,
  addCard,
  createSubscription,
  updateSubscription,
  cancelSubscription,
  removeSubscription,
  listTaxRates,
  getSubscription,
  getSubscriptionsByCustomerId,
  getPrice,
} from '../../../external/stripe';
import {
  getActiveStripeSubscription,
  getActiveStripeSubscriptionByUser,
  createStripeSubscription,
} from '../../../models/subscriptions/stripe';
import { createStripeCustomer, getStripeCustomerId } from '../../../models/customers/stripe';
import fillStripeCustomerId from '../../middleware/fillStripeCustomerId';
import trackIPAddress from '../../middleware/trackIPAddress';
import { incrementRedemptionCount } from '../../../external/promotion';
import getPriceKey from '../../../utils/getPriceKey';

export default function stripePayments() {
  const api = Router();
  const jsonBodyParser = bodyParser.json();

  /**
   * Retrieves an active subscription originated from the
   * Wavetrack Upgrade Funnels of Buoyweather, Fishtrack, and Surfline.
   *
   * @param query.product - the product requesting the subscription (sl | bw | fs)
   *
   * @returns {Object} | the latest active subscription for the product
   * requested.
   */
  const get = async (req, res) => {
    const {
      authenticatedUserId: userId,
      query: { product },
    } = req;
    const activeSubscription = await getActiveStripeSubscriptionByUser(userId, product);
    return res.status(200).send(activeSubscription);
  };

  /**
   * Creates a subscription for Stripe Payments originating from
   * the Wavetrak Upgrade Funnels of Buoyweather, Fishtrack, and Surfline.
   *
   * @param body.planId - the plan purchased
   * @param body.source - the new payment used in the purchase
   * @param body.cardId - the existing or alternate payment used in the purchase
   * @param req.authenticatedUserId - the user associated to the subscription
   *
   * @return 200 response
   */
  const create = async (req, res) => {
    const {
      authenticatedUserId: userId,
      body: { planId, source, cardId, trialEligible, coupon },
    } = req;

    try {
      const token = source ? await getToken(source) : {};

      const user = await getUserDetails(userId);
      const { firstName, lastName, email } = user;
      let { stripeCustomerId } = req;
      const {
        metadata: { query_set: brand, entitlement },
        recurring: { interval },
      } = await getPrice(planId);

      // has this user subscribed previously?
      const subscriptions = await getAllSubscriptionsByUserId(userId);

      // is the user actively subscribed?
      const isActive = subscriptions.some(
        sub => sub.entitlement === entitlement && sub.active === true,
      );

      if (isActive) {
        return res.status(400).send({
          message: 'Cannot process request with existing active subscription.',
        });
      }

      // do they have an account in Stripe?
      if (!stripeCustomerId) {
        const { id } = await createCustomer({
          email,
          firstName,
          lastName,
          userId,
        });
        stripeCustomerId = id;
        await createStripeCustomer(userId, stripeCustomerId);
      }

      // are they eligible for a free trial?
      const { entitlement: wasPremium } =
        subscriptions.find(sub => sub.entitlement === entitlement) || {};

      // validate card is eligible for a free trial
      if (source && !wasPremium && trialEligible) {
        const {
          card: { fingerprint, id },
        } = token;
        await validateCard({
          userId,
          fingerprint,
          brand,
          cardId: id,
        });
      }

      if (source) {
        await shouldProcessCardSource(userId);
        const { id, fingerprint } = await addCard(stripeCustomerId, source);
        await updateCustomer(stripeCustomerId, { default_source: id });
        trackCreditCardFingerprint(fingerprint, userId, entitlement);
      }

      if (cardId) {
        await updateCustomer(stripeCustomerId, { default_source: cardId });
      }

      const taxRates = await listTaxRates();
      const defaultTaxRates = taxRates.reduce((results, taxRate) => {
        if (taxRate.metadata.plans?.includes(planId)) {
          results.push(taxRate.id);
        }
        return results;
      }, []);

      const shouldAllowFreeTrial =
        trialEligible && !wasPremium && !interval.includes('month') && !coupon;
      const stripeSubscriptionData = await createSubscription({
        customer: stripeCustomerId,
        plan: planId,
        trialEligible: shouldAllowFreeTrial,
        defaultTaxRates,
        coupon: shouldAllowFreeTrial ? null : coupon,
      });
      const { subscriptionId } = await createStripeSubscription(userId, stripeSubscriptionData);
      return res
        .status(200)
        .send({ message: 'Successfully processed the Subscription', subscriptionId });
    } catch (error) {
      if (error.wavetrakCode === 1001 || error.wavetrakCode === 1002) {
        return res.status(error.statusCode).send({
          message: error.message,
          code: error.wavetrakCode,
        });
      }
      const { message, statusCode } = error;
      newrelic.addCustomAttributes({
        message,
        statusCode,
        stack: error.stack,
      });
      return res.status(statusCode).send({ message });
    }
  };

  /**
   * Update the billing plan or resumes a canceled subscription
   * managed through Stripe.
   *
   * @param body.subscriptionId | String
   * @param body.planId | String
   * @param body.resume | Boolean
   *
   * @returns subscription
   */
  const update = async (req, res) => {
    const {
      body: { subscriptionId, planId, resume },
    } = req;
    const subscription = await getActiveStripeSubscription(subscriptionId);

    // if resume is passed, reactivate the subscription
    const opts = resume ? { cancelAtPeriodEnd: false } : {};

    // if the subscription is trailing, do not reset the end date
    const { trialing, end } = subscription;
    if (trialing) opts.trialEndDate = end;

    await updateSubscription(subscriptionId, planId, opts);
    return res.status(200).send({ message: 'Successfully processed the Subscription' });
  };

  /**
   * Cancels the recurring billing of a subscription
   * managed through Stripe.  Subscriptions in a free trial
   * period are canceled immediately.  All other subscriptions
   * will expired at the end of the current billing period.
   *
   * @param body.subscriptionId | String
   */
  const cancel = async (req, res) => {
    const {
      body: { subscriptionId },
    } = req;
    const subscription = await getActiveStripeSubscription(subscriptionId);

    const {
      metadata: { migrated },
    } = await getSubscription(subscriptionId);
    // if the subscription is trailing, cancel immediately.
    const { trialing, failedPaymentAttempts } = subscription;
    if ((trialing && !migrated) || failedPaymentAttempts > 0) {
      await removeSubscription(subscriptionId);
      return res.status(200).send({ message: 'Successfully cancelled the Subscription' });
    }

    await cancelSubscription(subscriptionId);
    return res.status(200).send({ message: 'Successfully cancelled the Subscription' });
  };

  /**
   * Verifies if the subscription is created successfully from Stripe Checkout.
   *  - If the user has a subscription in the system, returns success.
   *  - If user doesn’t have a subscription but Stripe API returns an active subscription for the user,
   *    then proceed with the subscription creation and return success.
   *  - If user doesn’t have a subscription and Stripe API returns no active subscription for the user,
   *    then retry the process until max retry count is reached.  If the retry count is reached, return fail status.
   *
   * @param body.brand | String
   *
   * @returns res
   * @example
   *    {
   *      status: 'success',
          message: 'Successfully created the Subscription',
   *    }
   */
  const verify = async (req, res) => {
    const {
      authenticatedUserId: userId,
      body: { brand },
    } = req;

    if (!brand) {
      return res.status(400).send({
        message: 'Must provide a valid brand',
      });
    }
    const stripeCustomerId = await getStripeCustomerId(userId);
    if (!stripeCustomerId) throw new APIError('Cannot find Stripe Customer for user');
    let activeSubscription = await getActiveSubscriptionByUserIdAndEntitlement(userId, brand);
    let priceKey;
    if (activeSubscription) {
      priceKey = getPriceKey(activeSubscription.planId);
      return res.status(200).send({
        status: 'success',
        message: 'Subscription already exists',
        subscription: { ...activeSubscription, priceKey },
      });
    }

    const maxRetries = process.env.STRIPE_VERIFY_MAX_RETRIES || 3;
    const retryInterval = 1000; // 1 second
    let retryCount = 0;
    while (retryCount < maxRetries) {
      // Don't wait for the first try
      if (retryCount > 0) {
        await new Promise(resolve => setTimeout(resolve, retryInterval));
      }
      retryCount += 1;
      const stripeSubscriptions = await getSubscriptionsByCustomerId(stripeCustomerId);
      const stripeSubscription = stripeSubscriptions.find(
        ({
          status,
          items: {
            data: [{ price }],
          },
        }) => {
          return (
            (status === 'active' || status === 'trialing') &&
            price?.lookup_key.toLowerCase().startsWith(`${brand}_`)
          );
        },
      );
      if (stripeSubscription) {
        activeSubscription = await createStripeSubscription(userId, stripeSubscription);
        priceKey = getPriceKey(activeSubscription.planId);
        activeSubscription.priceKey = priceKey;
        const {
          metadata: { promotionId },
        } = stripeSubscription;
        if (promotionId) {
          await incrementRedemptionCount(promotionId, {
            userId,
            brand,
          });
        }
        return res.status(200).send({
          status: 'success',
          message: 'Successfully created the Subscription',
          subscription: activeSubscription,
        });
      }
    }
    return res.status(200).send({ status: 'fail', message: 'No active subscription found' });
  };

  const syncUserDetails = async (req, res) => {
    const {
      stripeCustomerId,
      body: { firstName, lastName, email },
    } = req;
    const name = `${firstName} ${lastName}`;

    if (stripeCustomerId) {
      await updateCustomer(stripeCustomerId, {
        email,
        metadata: { name, firstName, lastName },
      });
      return res.send({ message: 'User settings were updated' });
    }

    return res.send({
      message: 'No stripe account associated with the user',
    });
  };

  api.use('*', jsonBodyParser);
  api.get(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    wrapErrors((req, res) => get(req, res)),
  );
  api.post(
    '/',
    trackRequest,
    trackIPAddress,
    authenticateRequest({ userIdRequired: true }),
    fillStripeCustomerId,
    wrapErrors((req, res) => create(req, res)),
  );
  api.put(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    wrapErrors((req, res) => update(req, res)),
  );
  api.delete(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    wrapErrors((req, res) => cancel(req, res)),
  );
  api.post(
    '/verify',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    wrapErrors((req, res) => verify(req, res)),
  );
  api.put(
    '/sync',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    fillStripeCustomerId,
    wrapErrors((req, res) => syncUserDetails(req, res)),
  );

  return api;
}
