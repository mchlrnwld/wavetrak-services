import { Router } from 'express';
import * as bodyParser from 'body-parser';
import { wrapErrors, authenticateRequest } from '@surfline/services-common';
import moment from 'moment';
import trackRequest from '../../middleware/trackRequest';
import validatePromotionEligibility from '../../middleware/validatePromotionEligibility';
import { getUserDetails } from '../../../external/user';
import { trackCreditCardFingerprint } from '../../../common/trackEvent';
import { shouldProcessCardSource } from '../../../external/stripeutils';
import {
  createCustomer,
  updateCustomer,
  createSubscriptionForPromotion,
  addCard,
  listTaxRates,
  getPrice,
} from '../../../external/stripe';
import { incrementRedemptionCount } from '../../../external/promotion';
import {
  getStripeSubscriptionByPromotionIdAndUser,
  createStripeSubscription,
} from '../../../models/subscriptions/stripe';
import { createStripeCustomer } from '../../../models/customers/stripe';
import fillStripeCustomerId from '../../middleware/fillStripeCustomerId';
import { getActiveSubscriptionsByUserId } from '../../../models/subscriptions';
import getPriceKey from '../../../utils/getPriceKey';
import { getGiftRedemptionCode } from '../../../models/v1/gifts/giftRedemptionCode';

export default function promotionPayments() {
  const api = Router();
  const jsonBodyParser = bodyParser.json();

  /**
   * @description This function handles requests for finding subscriptions specific to a
   *  user and promotion.
   *
   * @param {*} req
   * @param {*} res
   *
   * @typedef {*} req
   * @property {object} query
   * @property {string} authenticatedUserId - The authorized user id added to this request.
   *
   * @typedef {object} query
   * @property {string} promotionId - The unique identifier for a Promotion document
   *
   * @typedef {*} res
   * @property {object|null} subscription - The active or inactive subscription created through the related promotion
   *
   * @returns res
   * @example
   * {
   *    subscription: {
   *        failedPaymentAttempts: 0,
   *        redeemCodes: [],
   *        type: 'stripe',
   *        trialing: false,
   *        expiring: false,
   *        renewalCount: 0,
   *        alertPayment: false,
   *        _id: '5e601cc4efd6034d0a74055e',
   *        subscriptionId: 'test123',
   *        entitlement: 'sl_premium',
   *        planId: 'sl_monthly',
   *        user: '5e601cc1efd6034d0a74055a',
   *        start: 1579201980,
   *        end: 1894821181,
   *        active: true,
   *        stripeCustomerId: 'cus_GqmHsFK8dBQ2gz',
   *        promotionId: '5e601cc2efd6034d0a74055b',
   *        createdAt: '2020-03-04T21:25:24.281Z',
   *        updatedAt: '2020-03-04T21:25:24.281Z',
   *    }
   *  }
   */
  const get = async (req, res) => {
    const {
      query: { promotionId },
      authenticatedUserId: userId,
    } = req;
    const subscription = await getStripeSubscriptionByPromotionIdAndUser(userId, promotionId);
    res.status(200).send({ subscription });
  };

  /**
   * @description This function handles request that process an promotional subscription transaction.
   *
   * @param {*} req
   * @param {*} res
   *
   * @typedef {*} req
   * @property {object} body
   * @property {string} authenticatedUserId - The authorized user id added to this request.
   * @property {object} promotion - The promotion document
   *
   * @typedef {object} body
   * @property {string} source - The Stripe Token key for a new credit card
   * @property {string} planId - The Stripe plan id assigned to the promotional subscription transaction.
   * @property {string} promoCode - The promotional redeem code if applicable.
   *
   * @typedef {*} res
   * @property {string} message - The message representing a successful subscription transaction.
   *
   * @returns {object} res
   * @example
   *    { message: "Successfully processed the Subscription"}
   */
  const create = async (req, res) => {
    const {
      promotion,
      authenticatedUserId: userId,
      body: { planId, source, promoCode },
    } = req;

    const user = await getUserDetails(userId);
    const { firstName, lastName, email } = user;
    let { stripeCustomerId } = req;

    if (!stripeCustomerId) {
      const { id } = await createCustomer({
        email,
        firstName,
        lastName,
        userId,
      });
      stripeCustomerId = id;
      await createStripeCustomer(userId, stripeCustomerId);
    }

    const {
      metadata: { entitlement },
    } = await getPrice(planId);

    if (source) {
      await shouldProcessCardSource(userId);

      const { id, fingerprint } = await addCard(stripeCustomerId, source);
      await updateCustomer(stripeCustomerId, { default_source: id });
      trackCreditCardFingerprint(fingerprint, userId, entitlement);
    }

    const { _id: promotionId, name: promotionName, trialLength, brand } = promotion;

    const taxRates = await listTaxRates();
    const defaultTaxRates = taxRates.reduce((results, taxRate) => {
      if (taxRate.metadata.plans?.includes(planId)) {
        results.push(taxRate.id);
      }
      return results;
    }, []);

    const stripeSubscriptionData = await createSubscriptionForPromotion({
      customer: stripeCustomerId,
      plan: planId,
      trialEligible: !!trialLength,
      trialLength,
      promotionId,
      promotionName,
      redeemCode: promoCode || null,
      defaultTaxRates,
    });

    incrementRedemptionCount(promotionId, {
      userId,
      brand,
      promoCode,
    });

    await createStripeSubscription(userId, stripeSubscriptionData);
    return res.status(200).send({ message: 'Successfully processed the Subscription' });
  };

  const getPerkEligibility = async (req, res) => {
    try {
      const { brand, count = 3 } = req.query;
      if (!brand) return res.status(400).send({ message: 'Invalid request body and/or params.' });

      const subscriptions = await getActiveSubscriptionsByUserId(req.authenticatedUserId);

      if (!subscriptions.length) {
        return res.status(200).send({ commonEligibility: false });
      }

      const subscription = subscriptions.find(sub => sub.entitlement.includes(`${brand}_premium`));

      if (subscription) {
        const { type, planId, trialing, renewalCount, subscriptionId } = subscription;

        if (type === 'stripe') {
          const priceKey = getPriceKey(planId);
          const monthly = planId.includes('month') || priceKey?.toLowerCase().includes('month');
          const commonEligibility = monthly ? !trialing && renewalCount >= count : !trialing;
          return res.status(200).send({ commonEligibility, renewalCount });
        }
        if (type === 'gift') {
          const giftCode = subscriptionId.substring(5); // Get code after gift_
          const formattedCode = giftCode.toUpperCase().trim();
          const {
            chargeId,
            giftProduct: { name },
          } = await getGiftRedemptionCode(formattedCode);
          if (planId.includes('gift') && !chargeId) {
            // VIP users are eligible
            return res.status(200).send({ commonEligibility: true });
          }
          const commonEligibility =
            !!(planId.includes('gift') && name.toLowerCase().includes('year')) && !!chargeId;
          return res.status(200).send({ commonEligibility });
        }
        if (type === 'apple' || type === 'google') {
          const commonEligibility = planId.includes('year') || planId.includes('annual');
          // If annual subscriber return immidietly
          if (commonEligibility) return res.status(200).send({ commonEligibility });
          const { createdAt } = subscription;
          const perksEligibleDate = moment(createdAt)
            .add(count, 'months')
            .toDate();
          const currentDate = moment().toDate();
          if (currentDate >= perksEligibleDate) {
            return res.status(200).send({ commonEligibility: true });
          }
        }
      }
      return res.status(200).send({ commonEligibility: false });
    } catch (err) {
      return res.status(err.code).send(err.message);
    }
  };

  api.use('*', jsonBodyParser);
  api.get(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    wrapErrors((req, res) => get(req, res)),
  );
  api.get(
    '/perk-eligibility',
    authenticateRequest({ userIdRequired: true }),
    wrapErrors((req, res) => getPerkEligibility(req, res)),
  );
  api.post(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    fillStripeCustomerId,
    validatePromotionEligibility({ promotionRequired: true }),
    wrapErrors((req, res) => create(req, res)),
  );

  return api;
}
