import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import * as bodyParser from 'body-parser';
import trackRequest from '../../middleware/trackRequest';
import { getAllSubscriptionsByUserId } from '../../../models/subscriptions';
import { getStripeCustomerId } from '../../../models/customers/stripe';

export default () => {
  const api = Router();
  const jsonBodyParser = bodyParser.json();

  /**
   * @description - Admin API for retrieving all subscriptions for a single customer.
   *
   * @query {String} userId - The wavetrak user id.
   * @returns {subscriptions}  - An array of v2 subscription objects
   */
  const getSubscriptions = async (req, res) => {
    const {
      query: { userId },
    } = req;
    try {
      const subscriptions = await getAllSubscriptionsByUserId(userId);
      const stripeCustomerId = await getStripeCustomerId(userId);
      return res.send({ subscriptions, stripeCustomerId });
    } catch (error) {
      return res
        .status(500)
        .send({ message: 'There was an issue finding the subscriptions for this customer ' });
    }
  };

  api.use('*', jsonBodyParser);
  api.get(
    '/',
    trackRequest,
    wrapErrors((req, res) => getSubscriptions(req, res)),
  );
  return api;
};
