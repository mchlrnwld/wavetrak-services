import { Router } from 'express';
import * as bodyParser from 'body-parser';
import { wrapErrors, authenticateRequest } from '@surfline/services-common';
import trackRequest from '../../middleware/trackRequest';
import { getSubscriptionDataFromItunesReceipt } from '../../../utils/itunes';
import { processAppleReceipt } from '../../../models/subscription-purchases/itunes';
import { updateOrCreateItunesSubscription } from '../../../models/subscriptions/itunes';
import { upsertSubscriptionUser } from '../../../models/subscription-users';
import {
  createPendingSubscription,
  deletePendingSubscription,
} from '../../../models/subscriptions/pending';
import logger from '../../../common/logger';

const jsonBodyParser = bodyParser.json();

export default function inAppPurchaseIOS() {
  const api = Router();
  const log = logger('subscription-api-itunes');

  /**
   * @description Endpoint for indicating the IAP subscription request.
   * A premiumPending status is implied when a user initiates a purchase
   * request within a native application.  If any step in the native purchase
   * reuqest chain is interupted, if this indicator exists, the Apps will
   * prompt a user to retry (restore) their subscription purchase.
   * @param {object} req.body request body
   * @param {string} req.body.brand Brand the purchase was made for.
   * @param {boolean} req.body.cancel Toggles the pendingPremium attr off
   *
   * @return {object} message - the success message
   */
  const premiumPending = async (req, res) => {
    const { authenticatedUserId: user } = req;
    const { brand, cancel } = req.body;
    if (!brand) return res.status(500).send({ message: 'Invalid request params.' });
    const data = { user, brand, platform: 'apple', cancel };
    if (cancel) {
      await deletePendingSubscription(data);
    } else {
      await createPendingSubscription(data);
    }
    return res.json({ message: 'Successfully processed the premium pending status' });
  };

  /**
   * @description Endpoint for CRUD specific modifcation to a iTunes
   * managed auto-renewing subscription.
   *
   * @typedef {Object} req.body - the request body
   * @property {object} receipt - The itunes receipt token.  Use for v1 API compatibility.
   *
   * @returns {object} subscription - the Apple Subscription object
   */
  const validateReceipt = async (req, res) => {
    const {
      body: { purchase, receipt },
      authenticatedUserId: userId,
    } = req;

    const purchaseData = purchase || receipt;

    if (!purchaseData) {
      return res
        .status(400)
        .send({ message: 'Cannot proccess subscription with a valid itunes receipt.' });
    }

    const newOrUpdatedReceipt = await processAppleReceipt(userId, purchaseData);
    if (!newOrUpdatedReceipt) {
      return res.status(400).send({ message: 'Apple has indicated an error with this receipt' });
    }
    const subscriptionData = getSubscriptionDataFromItunesReceipt(newOrUpdatedReceipt);
    const { subscriptionId, user: primaryUser } = subscriptionData;

    if (primaryUser.toString() !== userId.toString()) {
      log.error({
        message: 'Duplicate itunes purchase attempted.',
        userId,
        subscriptionId,
      });
      await upsertSubscriptionUser(primaryUser, subscriptionData);
      await upsertSubscriptionUser(userId, subscriptionData);
      return res.status(400).send({ message: 'This receipt is subscribed to a different account' });
    }

    await updateOrCreateItunesSubscription(subscriptionData);
    return res.status(200).send({ message: 'Successfully processed the Subscription' });
  };

  api.use('*', jsonBodyParser);
  api.post(
    '/premium-pending',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    wrapErrors((req, res) => premiumPending(req, res)),
  );
  api.post(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    wrapErrors((req, res) => validateReceipt(req, res)),
  );

  return api;
}
