import { Router } from 'express';
import * as bodyParser from 'body-parser';
import { wrapErrors, authenticateRequest } from '@surfline/services-common';
import trackRequest from '../../middleware/trackRequest';
import { getSubscriptionDataFromGooglePurchase } from '../../../utils/google';
import { processGooglePurchase } from '../../../models/subscription-purchases/google';
import { updateOrCreateGoogleSubscription } from '../../../models/subscriptions/google';
import { upsertSubscriptionUser } from '../../../models/subscription-users';
import {
  createPendingSubscription,
  deletePendingSubscription,
} from '../../../models/subscriptions/pending';
import logger from '../../../common/logger';

const jsonBodyParser = bodyParser.json();

export default function inAppPurchaseGoogle() {
  const api = Router();
  const log = logger('subscription-api-google');
  /**
   * @description Endpoint for indicating the IAP subscription request.
   * A premiumPending status is implied when a user initiates a purchase
   * request within a native application.  If any step in the native purchase
   * reuqest chain is interupted, if this indicator exists, the Apps will
   * prompt a user to retry (restore) their subscription purchase.
   *
   * @typedef {object} req.body request body
   * @property {string} req.body.brand Brand the purchase was made for.
   * @property {boolean} req.body.cancel Toggles the pendingPremium attr off
   *
   * @return {object} message - the success message
   */
  const premiumPending = async (req, res) => {
    const { authenticatedUserId: user } = req;
    const { brand, cancel } = req.body;
    if (!brand) return res.status(500).send({ message: 'Invalid request params.' });
    const data = { user, brand, platform: 'google', cancel };
    if (cancel) {
      await deletePendingSubscription(data);
    } else {
      await createPendingSubscription(data);
    }
    return res.json({ message: 'Successfully processed the premium pending status' });
  };

  /**
   * @description Endpoint for CRUD specific modifcation to a Google Play
   * managed auto-renewing subscription.
   *
   * @typedef {Object} req.body - the request body
   * @property {object} receipt - The google purchase token.  Use for v1 API compatibility.
   * @property {object} purchase - Also the google purchase token.  Use for the v2 API.
   *
   * @returns {object} subscription - the Google Subscription object
   */
  const verifyPurchase = async (req, res) => {
    const {
      body: { receipt, purchase },
      authenticatedUserId: userId,
    } = req;

    // To help with backward compatibility
    const purchaseData = purchase || receipt;

    if (!purchaseData) {
      return res
        .status(400)
        .send({ message: 'Attach purchaseData as a "receipt || purchase" property in JSON body.' });
    }
    const newOrUpdatedPurchase = await processGooglePurchase(userId, purchaseData);
    const subscriptionData = getSubscriptionDataFromGooglePurchase(newOrUpdatedPurchase);

    const { subscriptionId, user: primaryUser } = subscriptionData;

    if (primaryUser.toString() !== userId.toString()) {
      log.error({
        message: 'Duplicate itunes purchase attempted.',
        userId,
        subscriptionId,
      });
      await upsertSubscriptionUser(primaryUser, subscriptionData);
      await upsertSubscriptionUser(userId, subscriptionData);
      return res.status(400).send({ message: 'This receipt is subscribed to a different account' });
    }

    await updateOrCreateGoogleSubscription(subscriptionData);
    return res.status(200).send({ message: 'Successfully processed the Subscription' });
  };

  api.use('*', jsonBodyParser);
  api.post(
    '/premium-pending',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    wrapErrors((req, res) => premiumPending(req, res)),
  );
  api.post(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    wrapErrors((req, res) => verifyPurchase(req, res)),
  );
  return api;
}
