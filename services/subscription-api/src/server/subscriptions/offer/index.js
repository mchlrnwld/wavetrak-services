import { Router } from 'express';
import * as bodyParser from 'body-parser';
import { wrapErrors, authenticateRequest } from '@surfline/services-common';
import trackRequest from '../../middleware/trackRequest';
import fillStripeCustomerId from '../../middleware/fillStripeCustomerId';
import checkOfferEligibility from '../../middleware/checkOfferEligibility';
import { getPlanAmountAndInterval } from '../../../utils/offer';
import { shouldProcessCardSource } from '../../../external/stripeutils';
import { createStripeSubscription } from '../../../models/subscriptions/stripe';
import { createStripeCustomer } from '../../../models/customers/stripe';
import { incrementRedemptionCount } from '../../../external/promotion';
import {
  getCoupon,
  getSubscription,
  updateSubscription,
  createSubscriptionForPromotion,
  getUpcomingInvoice,
  getPrice,
  getCustomer,
  createCustomer,
  updateCustomer,
  addCard,
} from '../../../external/stripe';
import {
  trackCreditCardFingerprint,
  trackAcceptedSubscriptionOffer,
} from '../../../common/trackEvent';

export default function offerPayments() {
  const api = Router();
  const jsonBodyParser = bodyParser.json();

  const computeActiveStripeOffer = async (stripeCustomerId, offer, subscription) => {
    const { subscriptionId, planId } = subscription;
    const { eligiblePlan, offeredPlan, stripeCouponCode } = offer;

    const {
      items: { data },
    } = await getSubscription(subscriptionId);
    const prorationDate = Math.floor(Date.now() / 1000);
    const prorationItems = [
      {
        id: data[0].id,
        plan: offeredPlan,
      },
    ];
    const { lines } = await getUpcomingInvoice(stripeCustomerId, subscriptionId, {
      subscription_items: prorationItems,
      subscription_proration_date: prorationDate,
    });

    let totalCredits = 0;
    for (let i = 0; i < lines.data.length; i += 1) {
      const invoiceItem = lines.data[i];
      if (invoiceItem.plan.id === eligiblePlan) {
        totalCredits += invoiceItem.amount;
      }
    }

    const { amount_off: amountOff, metadata } = await getCoupon(stripeCouponCode);
    const { unit_amount: offeredPlanAmount } = await getPrice(offeredPlan);
    const {
      unit_amount: currentPlanAmount,
      recurring: { interval: currentPlanInterval },
    } = await getPrice(planId);

    const currentAnnualCost =
      currentPlanInterval === 'month' ? currentPlanAmount * 12 : currentPlanAmount;

    const costResponse = {
      currentAnnualCost,
      currentPlan: planId,
      offeredSavings: parseInt(metadata.savings, 10) * 100,
      offeredPlan,
      renewalRate: offeredPlanAmount,
      totalCredits: Math.abs(totalCredits),
      totalCost: offeredPlanAmount + totalCredits - amountOff,
    };
    return costResponse;
  };

  const computeExpiredOffer = async (stripeCustomerId, offer, subscription) => {
    const { offeredPlan, stripeCouponCode, eligiblePlan } = offer;
    const { planId, type } = subscription;

    const { balance } = stripeCustomerId ? await getCustomer(stripeCustomerId) : { balance: 0 };

    const { amount_off: amountOff, metadata } = await getCoupon(stripeCouponCode);
    const { unit_amount: offeredPlanAmount } = await getPrice(offeredPlan);
    const {
      unit_amount: currentPlanAmount,
      recurring: { interval: currentPlanInterval },
    } = type === 'stripe' ? await getPrice(eligiblePlan) : getPlanAmountAndInterval(planId);

    const currentAnnualCost =
      currentPlanInterval === 'month' ? currentPlanAmount * 12 : currentPlanAmount;

    const costResponse = {
      currentAnnualCost,
      currentPlan: eligiblePlan,
      offeredSavings: parseInt(metadata.savings, 10) * 100,
      offeredPlan,
      renewalRate: offeredPlanAmount,
      totalCredits: Math.abs(balance),
      totalCost: offeredPlanAmount + balance - amountOff,
    };

    return costResponse;
  };

  /**
   * @description This function handles GET requests to the Subscription Offer APIs
   *
   * @param {*} req
   * @param {*} res
   *
   * @typedef {*} req
   * @property {object} user - The UserInfo document of the authorized user
   * @property {object} offer - The promotion offer document
   * @property {object} subscription - The active or inactive subscription that is eligible for this offer
   *
   * @typedef {*} res
   * @property {number} currentAnnualCost - The value, in cents, of the total annual cost of the user's current or most recent plan
   * @property {string} currentPlan - The name of the user's most recent subscription billing plan
   * @property {number} offeredSavings - The value, in cetns, of savings defined in the promotional offer document
   * @property {string} offeredPlan - The eligible billing plan defined by the promotional offer document
   * @property {number} renewalRate - The value, in cents, of the offered Plan
   * @property {number} totalCredits - The total value, in cents, of the existing credits and prorations for a Stripe Customer.
   * @property {number} totalCost - The total value, in cents, the user should expect to be billing of they accept the offer.
   *
   * @returns {object} res
   * @example
   *    {
   *      currentAnnualCost: 11988,
   *      currentPlan: 'surfline.iosapp.1month.subscription',
   *      offeredSavings: 4500,
   *      offeredPlan: 'sl_annual_v2',
   *      renewalRate: 9588,
   *      totalCredits: 0,
   *      totalCost: 7588
   *    }
   */
  const get = async (req, res) => {
    const { offer, stripeCustomerId, subscription } = req;
    const { eligibleStatus } = offer;
    const costResponse =
      eligibleStatus === 'expired'
        ? await computeExpiredOffer(stripeCustomerId, offer, subscription)
        : await computeActiveStripeOffer(stripeCustomerId, offer, subscription);
    return res.status(200).send({ ...costResponse });
  };

  /**
   * @description This function handles request that process an excepted offer transaction.
   *
   * @param {*} req
   * @param {*} res
   *
   * @typedef {*} req
   * @property {object} body
   * @property {object} user - The UserInfo document of the authorized user
   * @property {object} offer - The promotion offer document
   * @property {object} subscription - The active or inactive subscription that is eligible for this offer
   *
   * @typedef {object} body
   * @property {string} source - The Stripe Token key for a new credit card
   * @property {string} cardId - The id of an exsiting customer credit card
   *
   * @typedef {*} res
   * @property {string} message - The message representing a successful offer transaction.
   *
   * @returns {object} res
   * @example
   *    { message: "Successfully processed the Subscription"}
   *
   */
  const create = async (req, res) => {
    const { user, offer, subscription, body } = req;
    const { source: newCreditCardSource, cardId: existingCreditCardId } = body;
    const { _id: offerId, offeredPlan, stripeCouponCode, name, eligibleStatus, brand } = offer;
    const { _id: userId, firstName, lastName, email } = user;
    let { stripeCustomerId } = req;
    const { subscriptionId } = subscription;
    if (!stripeCustomerId) {
      const { id } = await createCustomer({
        email,
        firstName,
        lastName,
        userId,
      });
      stripeCustomerId = id;
      await createStripeCustomer(userId, stripeCustomerId);
    }
    if (newCreditCardSource) {
      await shouldProcessCardSource(userId.toString());
      const { id: newCardId, fingerprint } = await addCard(stripeCustomerId, newCreditCardSource);
      await updateCustomer(stripeCustomerId, { default_source: newCardId });
      trackCreditCardFingerprint(fingerprint, userId, offeredPlan);
    }

    if (existingCreditCardId && !newCreditCardSource) {
      await updateCustomer(stripeCustomerId, { default_source: existingCreditCardId });
    }

    if (eligibleStatus === 'expired') {
      const stripeSubscriptionData = await createSubscriptionForPromotion(
        {
          customer: stripeCustomerId,
          plan: offeredPlan,
          trialEligible: false,
          promotionId: offerId,
          promotionName: name,
        },
        stripeCouponCode,
      );
      await createStripeSubscription(userId, stripeSubscriptionData);
    }

    if (eligibleStatus !== 'expired') {
      await updateSubscription(subscriptionId, offeredPlan, {
        trialEndDate: 'now',
        coupon: stripeCouponCode,
      });
    }

    trackAcceptedSubscriptionOffer({
      userId: userId.toString(),
      promotionId: offerId.toString(),
      promotionName: name,
      planId: offeredPlan,
    });

    incrementRedemptionCount(offerId, {
      userId,
      brand,
    });

    return res.status(200).send({ message: 'Successfully processed the Subscription' });
  };

  api.use('*', jsonBodyParser);
  api.get(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    checkOfferEligibility,
    fillStripeCustomerId,
    wrapErrors((req, res) => get(req, res)),
  );
  api.post(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    checkOfferEligibility,
    fillStripeCustomerId,
    wrapErrors((req, res) => create(req, res)),
  );
  return api;
}
