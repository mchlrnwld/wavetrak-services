import { Router } from 'express';
import _ from 'lodash';
import mongoose from 'mongoose';
import newrelic from 'newrelic';
import * as bodyParser from 'body-parser';
import { authenticateRequest, wrapErrors } from '@surfline/services-common';
import trackRequest from '../middleware/trackRequest';
import logger from '../../common/logger';
import * as stripe from '../../external/stripe';
import { shouldProcessCardSource } from '../../external/stripeutils';
import {
  getStripeSubscriptionsByUser,
  updateStripeSubscription,
} from '../../models/subscriptions/stripe';
import fillStripeCustomerId from '../middleware/fillStripeCustomerId';

const cardsApi = () => {
  const log = logger('subscription-service');

  const getCards = async (req, res) => {
    const { authenticatedUserId: userId } = req;

    try {
      if (!req.stripeCustomerId) {
        return res.send({ cards: null, defaultSource: null });
      }

      const {
        sources: { data },
        default_source: defaultSource,
      } = await stripe.getCustomer(req.stripeCustomerId, ['sources']);
      const cards = data
        .filter(({ object }) => object === 'card')
        // eslint-disable-next-line camelcase
        .map(({ last4, id, exp_month, exp_year, addressZip, brand }) => ({
          brand,
          last4,
          id,
          exp_month,
          exp_year,
          expMonth: exp_month,
          expYear: exp_year,
          addressZip,
        }));

      const responseObject = {
        cards,
        defaultSource,
      };

      return res.send(responseObject);
    } catch (err) {
      log.error({
        userId,
        err,
        message: 'There was a problem fetching cards.',
      });
      newrelic.noticeError(err);
      return res.status(500).send({
        message: 'There was a problem fetching cards.',
      });
    }
  };

  const createCard = async (req, res) => {
    const { authenticatedUserId: userId } = req;

    const { source } = req.body;
    const { stripeCustomerId } = req;

    try {
      await shouldProcessCardSource(userId);
    } catch (err) {
      log.error({
        userId,
        err,
        message: `User ${userId} exceeded create card requests.`,
      });
      return res.status(403).send({
        message: 'Cannot process request.',
      });
    }

    const stripeCustomer = await stripe.getCustomer(stripeCustomerId, ['sources']);
    const tokenInfo = await stripe.getToken(source);
    const foundFingerprint = stripeCustomer.sources.data.reduce((found, oldCard) => {
      if (tokenInfo.card.fingerprint === oldCard.fingerprint) {
        return true;
      }
      return found;
    }, false);

    if (foundFingerprint) {
      return res.status(422).send({
        message: 'Card already exists.',
      });
    }

    const card = await stripe.addCard(stripeCustomerId, req.body.source);

    try {
      await stripe.updateCustomer(stripeCustomerId, {
        default_source: card.id,
      });
    } catch (err) {
      newrelic.noticeError(err);
      return res.status(500).send({
        message: 'There was a problem setting the card as default.',
      });
    }

    try {
      const subscriptions = await getStripeSubscriptionsByUser(mongoose.Types.ObjectId(userId));
      const subscriptionsToPay = subscriptions
        .filter(({ failedPaymentAttempts }) => failedPaymentAttempts > 0)
        .map(({ subscriptionId }) => subscriptionId);

      if (subscriptionsToPay.length > 0) {
        await stripe.payInvoices(stripeCustomerId, subscriptionsToPay);
      }
    } catch (err) {
      log.error({
        userId,
        err,
        message: 'There was a problem retrying invoice payments',
      });
      newrelic.noticeError(err);
      return res.status(500).send({
        message: 'There was a problem retrying invoice payments',
      });
    }
    return res.send({
      card: {
        addressZip: card.address_zip,
        brand: card.brand,
        expMonth: card.exp_month,
        expYear: card.exp_year,
        id: card.id,
        last4: card.last4,
      },
    });
  };

  const deleteCard = async (req, res) => {
    const { authenticatedUserId: userId } = req;
    const { cardId } = req.params;

    const { stripeCustomerId } = req;
    const subObj = await stripe.getCustomer(stripeCustomerId, ['sources']);

    if (subObj.default_source === cardId) {
      return res.status(400).send({
        message: 'The default card cannot be deleted.',
      });
    }

    const foundCard = _.find(subObj.sources.data, card => card.id === cardId);
    if (foundCard) {
      try {
        await stripe.deleteCard(stripeCustomerId, cardId);

        const subscriptions = await getStripeSubscriptionsByUser(mongoose.Types.ObjectId(userId));

        subscriptions
          .filter(item => item.cardId === cardId && item.status === 'open')
          .forEach(subscription => {
            updateStripeSubscription({
              subscriptionId: subscription.id,
              paymentAlertStatus: 'resolved',
            });
          });
      } catch (err) {
        log.error({
          userId,
          err,
          message: 'There was a problem with Stripe removing the card',
        });
        newrelic.noticeError(err);
        return res.status(500).send({
          message: 'There was a problem with Stripe removing the card',
        });
      }
      return res.send({ message: 'The card was deleted' });
    }

    return res.status(404).send({
      message: 'The card could not be found',
    });
  };

  const updateCard = async (req, res) => {
    const { authenticatedUserId: userId } = req;
    const { cardId } = req.params;
    const { opts } = req.body;
    const { stripeCustomerId } = req;

    const { sources } = await stripe.getCustomer(stripeCustomerId, ['sources']);

    const card = sources.data.find(({ object, id }) => object === 'card' && id === cardId);

    if (card) {
      try {
        const cardResult = await stripe.updateCard(stripeCustomerId, cardId, opts);
        const subscriptions = await getStripeSubscriptionsByUser(mongoose.Types.ObjectId(userId));

        subscriptions
          .filter(
            ({ cardId: id, paymentAlertType, paymentAlertStatus }) =>
              id === cardId &&
              paymentAlertType === 'invalidZip' &&
              paymentAlertStatus !== 'resolved',
          )
          .forEach(({ id }) => {
            updateStripeSubscription({
              subscriptionId: id,
              paymentAlertStatus: 'resolved',
            });
          });

        const subscriptionsToPay = subscriptions.filter(
          ({ failedPaymentAttempts }) => failedPaymentAttempts > 0,
        );

        await stripe.payInvoices(stripeCustomerId, subscriptionsToPay);

        return res.send({
          card: {
            ...cardResult,
            expMonth: cardResult.exp_month,
            expYear: cardResult.exp_year,
            addressZip: cardResult.address_zip,
          },
        });
      } catch (err) {
        let message = 'There was a problem with Stripe updating the card';
        let statusCode = 500;
        if (err.type && err.type.includes('Stripe')) {
          message = err.message;
          statusCode = err.statusCode;
        }
        log.error({
          userId,
          err,
          message: 'There was a problem with Stripe updating the card',
        });
        newrelic.addCustomAttributes({
          statusCode,
          message,
          stack: err.stack,
        });
        return res.status(statusCode).send({
          message,
        });
      }
    }

    return res.status(404).send({
      message: 'The card could not be found',
    });
  };

  const setDefaultCard = async (req, res) => {
    const { authenticatedUserId: userId } = req;
    const { cardId } = req.params;
    const { stripeCustomerId } = req;

    try {
      if (cardId) {
        const card = await stripe.getCard(stripeCustomerId, cardId);
        if (card) {
          await stripe.updateCustomer(stripeCustomerId, {
            default_source: cardId,
          });
        }
      }
    } catch (err) {
      log.error({
        userId,
        err,
        message: 'The card provided is invalid or could not be saved',
      });
      return res.status(500).send({
        message: 'The card provided is invalid or could not be saved',
      });
    }
    return res.send({ message: 'Subscription updated' });
  };

  const api = Router();

  const jsonBodyParser = bodyParser.json();
  api.use('*', jsonBodyParser);

  api.get(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    fillStripeCustomerId,
    wrapErrors((req, res) => getCards(req, res)),
  );
  api.post(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    fillStripeCustomerId,
    wrapErrors((req, res) => createCard(req, res)),
  );
  api.delete(
    '/:cardId',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    fillStripeCustomerId,
    wrapErrors((req, res) => deleteCard(req, res)),
  );
  api.put(
    '/:cardId',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    fillStripeCustomerId,
    wrapErrors((req, res) => updateCard(req, res)),
  );
  api.put(
    '/default/:cardId',
    trackRequest,
    authenticateRequest({ userIdRequired: true }),
    fillStripeCustomerId,
    wrapErrors((req, res) => setDefaultCard(req, res)),
  );
  return api;
};

export default cardsApi;
