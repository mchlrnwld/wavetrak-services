### In-app purchase receipt validation for (dis)entitling users

 * Service endpoint to save IAP receipt
 * Daily job to re-verify all IAP receipts
 * Whenever we are interacting with an IAP receipt, we are also updating subscription information

#### Documentation
 * https://developer.apple.com/library/ios/releasenotes/General/ValidateAppStoreReceipt/Chapters/ValidateLocally.html#//apple_ref/doc/uid/TP40010573-CH1-SW9
 * https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/StoreKitGuide/Introduction.html#//apple_ref/doc/uid/TP40008267-CH1-SW1
 * https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/StoreKitGuide/Chapters/Subscriptions.html#//apple_ref/doc/uid/TP40008267-CH7-SW6
 * http://stdout.in/en/post/processing_app_store_auto_renewable_subscriptions_in_php
 * http://sohail.io/2015/10/19/verifying-the-status-of-an-auto-renewable-subscription/
 * https://developer.android.com/google/play/billing/billing_subscriptions.html
 * https://wavetrak.atlassian.net/browse/MOB-608

#### Code tasks
 * Service module triggered from scheduled daily Airflow job to re-validate receipts
    * IAP users would not be able to have their renewal detected until they open the app and let it send the receipt to us
    * We will have to save the receipt and periodically re-validate receipt
 * Authenticated /iap/ios/verifyReceipt endpoint for iOS receipt validation
 * Authenticated /iap/google/verifyReceipt endpoint for Google Play Store receipt validation
 * Shared IapReceipt module that acts on deviceType, receipt, userAccountId, and brand
    * Save base64 receipt on UserAccounts
    * Validate receipt with iap node module
    * Meaningfully map error states with receipt
    * Modify subscription states for users based on receipt
    * Verify segment events are fired for subscription state changes

##### Execution Costs
 * Daily job queries indexed MongoDB UserAccounts for all user account ids with IAP receipts
    * For each of those IDs, verify receipt with provider and update subscription info as needed
 * Devices send receipts to our service endpoint, we add receipt to user account, update subscriptions, and save updated user account

##### Errors
```
Hard error, ask client to send again, don't save receipt
  21000: 'The App Store could not read the JSON object you provided.',
  21002: 'The data in the receipt-data property was malformed.',
Soft error, save and we'll try again later when the next job runs
  21004: 'The shared secret you provided does not match the shared secret on file for your account.',
  21005: 'The receipt server is not currently available.',
Hard error, warn for potential abuse
  21003: 'The receipt could not be authenticated.',
  21007: 'This receipt is a sandbox receipt, but it was sent to the production service for verification.',
  21008: 'This receipt is a production receipt, but it was sent to the sandbox service for verification.',
  2: 'The receipt is valid, but purchased nothing.'
No error, just expired
  21006: 'This receipt is valid but the subscription has expired. When this status code is returned to your server, the receipt data is also decoded and returned as part of the response.',
```
When clients send receipts for validation, if the receipt successfully validates, we'll overwrite the previous receipt.

##### Subscription mapping
```
  UserAccounts -> "subscriptions" : [{
       "type" : "ios",
       "subscriptionId" : receipt_info.transaction_id,
       "periodStart" : receipt_info.original_purchase_date,
       "periodEnd" : receipt_info.expires_date,
       "entitlement" : "sl_premium" from product id,
       "createdAt" : new Date() if no createdAt,
       "expiring" : not being renewed,
       "failedPaymentAttempts" : N/A
   }]
```
#### Use cases
 * User signs-up/upgrades with IAP, need to entitle user
 * User subscription lapses (by cancel, or payment issue), need to disentitle user
 * User restores transactions (phone reset), we can replay the transactions to verify all entitlements

#### Questions
 * Watch out for: when reading receipts, do we only care about the latest item, or do we have to parse every line item?

#### Cancellation / Refunds
 * Apple IAP users will have to [cancel their subscription through apple](https://zoosk.zendesk.com/hc/en-us/articles/206747656-How-do-I-cancel-my-subscription-purchased-using-an-Apple-iPhone-iPod-iPad-or-through-iTunes-)
    * Uninstalling the app will not automatically stop your subscription
    * We will have to change upgrade/renew CTAs on website to point to [iTunes subscription management](https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/manageSubscriptions) or iOS app if they have an active Apple subscription
 * Google IAP users will have to cancel their subscription through us
    * **Important:** You cannot use the API to issue refunds or cancel In-app Billing transactions. You must do this manually through your Google payments merchant account. However, you can use the API to retrieve order information.
 * Implementation options
    * Job to check for cancellations
        * Google subs @ end of subscription
        * Apple subs every day
    * Admin interface
        * Expose cancellation in admin interface
        * Right now it's just for entitlements. I'll have to add a button to the user detail in the admin cms to archive subscription

#### Renewals
 * Apple
    * After a subscription is successfully renewed, Store Kit adds a transaction for the renewal to the transaction queue.
    * Your app checks the transaction queue on launch and handles the renewal the same way as any other transaction.
    * Note that if your app is already running when the subscription renews, the transaction observer is not called; your app finds out about the renewal the next time it’s launched.
 * Google
    * To help you track transactions relating to a given subscription, Google payments provides a base Merchant Order Number for all recurrences of the subscription and denotes each recurring transaction by appending an integer

#### Future
 * Authorize micro-service resources based on entitlements -- protects against app-cracking (all they could potentially do is remove ads)
