import { Router } from 'express';
import * as bodyParser from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import newrelic from 'newrelic';
import logger from '../../../common/logger';
import { getSubscriptionDataFromItunesReceipt } from '../../../utils/itunes';
import { processAppleReceipt } from '../../../models/subscription-purchases/itunes';
import { updateOrCreateItunesSubscription } from '../../../models/subscriptions/itunes';
import transformToV1Contract from '../../../utils/toV1UserAccount';
import { upsertSubscriptionUser } from '../../../models/subscription-users';

const jsonBodyParser = bodyParser.json();

export default function inAppPurchaseIOS() {
  const api = Router();
  const log = logger('iap-route');

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/iap/ios',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  const authenticateRequest = (req, res, next) => {
    if (req.headers['x-auth-userid']) {
      req.authenticatedUserId = req.headers['x-auth-userid'];
    }
    const id = req.authenticatedUserId || null;
    newrelic.addCustomAttribute('userId', id);
    if (!id) {
      return res.status(400).json({ message: 'You must be an authenticated user.' });
    }
    return next();
  };

  /**
   * JSON body format:
   * {
   *    receipt: 'receipt...'
   * }
   *
   * Error responses:
   * 500: {message: 'Error encountered'}
   *      Possibilities: User retrieval or receipt parsing failed
   *
   * 400: {message: 'Attach receipt as a "receipt" and "type" property in JSON body.'}
   *
   * @param req JSON encoded body with 'receipt' property as the base64 encoded receipt
   * @param res
   */
  const updateUserReceipt = async (req, res) => {
    const {
      body: { receipt },
      authenticatedUserId: userId,
    } = req;

    if (!receipt) {
      return res.status(400).json({
        message: 'Attach receipt as a "receipt" property in JSON body.',
      });
    }
    try {
      const newOrUpdatedPurchase = await processAppleReceipt(userId, receipt);
      const subscriptionData = getSubscriptionDataFromItunesReceipt(newOrUpdatedPurchase);
      const { user: primaryUser } = subscriptionData;

      if (primaryUser.toString() !== userId.toString()) {
        await upsertSubscriptionUser(primaryUser, subscriptionData);
        await upsertSubscriptionUser(userId, subscriptionData);
        return res
          .status(400)
          .send({ message: 'This receipt is subscribed to a different account' });
      }

      const subscription = await updateOrCreateItunesSubscription(subscriptionData);
      const result = transformToV1Contract(subscription);
      return res.status(200).send(result);
    } catch (error) {
      log.error({
        message: error.message,
        stack: error.stack,
        userId,
      });
      return res.status(500).send({ message: error.message });
    }
  };

  /**
   * @description Endpoint for indicating the IAP subscription request.
   * A premiumPending status is implied when a user initiates a purchase
   * request within a native application.  If any step in the native purchase
   * reuqest chain is interupted, if this indicator exists, the Apps will
   * prompt a user to retry (restore) their subscription purchase.
   * @param {object} req.body request body
   * @param {string} req.body.brand Brand the purchase was made for.
   * @param {boolean} req.body.cancel Toggles the pendingPremium attr off
   * @param {*} res response object
   */
  const premiumPending = async (req, res) => {
    return res
      .status(410)
      .send({ message: 'The premium pending API has been permanently deprecated.' });
  };

  api.use('*', jsonBodyParser);

  api.post(
    '/verifyReceipt',
    trackRequest,
    authenticateRequest,
    wrapErrors((req, res) => updateUserReceipt(req, res)),
  );
  api.post(
    '/premium-pending',
    trackRequest,
    authenticateRequest,
    wrapErrors((req, res) => premiumPending(req, res)),
  );

  return api;
}
