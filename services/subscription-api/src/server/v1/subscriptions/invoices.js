import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import * as stripe from '../../../external/stripe';
import { getFailedPaymentSubscriptionIds } from './helpers';

export default () => {
  const api = Router();

  /**
   * Returns an array of invoices that were updated
   */
  const payInvoice = async (req, res) => {
    const userAccount = req.user;
    const { stripeCustomerId, subscriptions } = userAccount;
    const subscriptionsToPay = getFailedPaymentSubscriptionIds(subscriptions);
    const paidInvoices = await stripe.payInvoices(stripeCustomerId, subscriptionsToPay);

    return res.send(paidInvoices);
  };

  api.post(
    '/pay',
    wrapErrors((req, res) => payInvoice(req, res)),
  );

  return api;
};
