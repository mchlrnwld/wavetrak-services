import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import newrelic from 'newrelic';
import logger from '../../../common/logger';
import { plansForUser, validateCoupon as validateCouponHelper } from './helpers';
import * as stripe from '../../../external/stripe';
/**
 * Validates the coupon code and returns the adjusted brand specific plans available to the user.
 *
 * @param {String} coupon
 * @param {String} brand
 * @param {String } countryISO
 *
 * @returns {
 *   plans: (*|Array)
 * }
 */
export default () => {
  const api = Router();
  const log = logger('subscription-service');

  const validateCoupon = async (req, res) => {
    const { coupon, brand, countryISO } = req.query;
    const { user: userAccount } = req;
    try {
      let validCoupon;
      try {
        validCoupon = await validateCouponHelper(coupon, brand);
      } catch (error) {
        return res.status(400).send({
          message: `${coupon} is not a valid coupon`,
        });
      }
      const { stripeCustomerId } = userAccount || {};
      const { currency } = stripeCustomerId ? await stripe.getCustomer(stripeCustomerId) : {};

      const plans = await plansForUser(validCoupon, countryISO, currency);
      const filteredPlans = plans.filter(plan => plan.entitlementKey.includes(`${brand}_`));
      return res.status(200).send({ plans: filteredPlans });
    } catch (err) {
      log.error({
        err,
        message: 'There was problem validating the coupon',
      });
      newrelic.noticeError(err);
      return res.status(500).send({
        message: 'There a problem validating the coupon',
      });
    }
  };

  api.get(
    '/',
    wrapErrors((req, res) => validateCoupon(req, res)),
  );
  return api;
};
