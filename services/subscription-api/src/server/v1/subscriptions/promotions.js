import { Router } from 'express';
import moment from 'moment';
import { wrapErrors } from '@surfline/services-common';
import { getGiftRedemptionCode } from '../../../models/v1/gifts/giftRedemptionCode';
import { getActiveSubscriptionsByUserId } from '../../../models/subscriptions/index';
import getPriceKey from '../../../utils/getPriceKey';

export default () => {
  const api = Router();
  /**
   * /perk-eligibility - Check if a user's subscription is eligible for Premium Perks.
   * Returns Boolean commonEligibility to indicate if the user is eligible.
   * Returns Integer renewalCount to indicate how many times the subscription has been renewed.
   * @param req.authenticatedUserId (required MongoId|String)
   * @param brand
   * @param count (number of times user has renewed the subscription)
   * @returns {
   *   commonEligibility: (*|Boolean)
   *   renewalCount: (*|Integer)
   * }
   */

  const getPerkEligibility = async (req, res) => {
    try {
      const { brand, count = 3 } = req.query;
      if (!brand) return res.status(400).send({ message: 'Invalid request body and/or params.' });

      const subscriptions = await getActiveSubscriptionsByUserId(req.authenticatedUserId);

      if (!subscriptions.length) {
        return res.status(200).send({ commonEligibility: false });
      }

      const subscription = subscriptions.find(sub => sub.entitlement.includes(`${brand}_premium`));

      if (subscription) {
        const { type, planId, trialing, renewalCount, subscriptionId } = subscription;

        if (type === 'stripe') {
          const priceKey = getPriceKey(planId);
          const monthly = planId.includes('month') || priceKey?.toLowerCase().includes('month');
          const commonEligibility = monthly ? !trialing && renewalCount >= count : !trialing;
          return res.status(200).send({ commonEligibility, renewalCount });
        }
        if (type === 'gift') {
          const giftCode = subscriptionId.substring(5); // Get code after gift_
          const formattedCode = giftCode.toUpperCase().trim();
          const {
            chargeId,
            giftProduct: { name },
          } = await getGiftRedemptionCode(formattedCode);
          if (planId.includes('gift') && !chargeId) {
            // VIP users are eligible
            return res.status(200).send({ commonEligibility: true });
          }
          const commonEligibility =
            !!(planId.includes('gift') && name.toLowerCase().includes('year')) && !!chargeId;
          return res.status(200).send({ commonEligibility });
        }
        if (type === 'apple' || type === 'google') {
          const commonEligibility = planId.includes('year') || planId.includes('annual');
          // If annual subscriber return immidietly
          if (commonEligibility) return res.status(200).send({ commonEligibility });
          const { createdAt } = subscription;
          const perksEligibleDate = moment(createdAt)
            .add(count, 'months')
            .toDate();
          const currentDate = moment().toDate();
          if (currentDate >= perksEligibleDate) {
            return res.status(200).send({ commonEligibility: true });
          }
        }
      }
      return res.status(200).send({ commonEligibility: false });
    } catch (err) {
      return res.status(500).send(err.message);
    }
  };

  api.get(
    '/perk-eligibility',
    wrapErrors((req, res) => getPerkEligibility(req, res)),
  );
  return api;
};
