import { Router } from 'express';
import newrelic from 'newrelic';
import { wrapErrors } from '@surfline/services-common';
import logger from '../../../common/logger';
import { getCurrencyForCountry } from '../../../utils/getCurrencyCode';
import { plansForUser } from './helpers';

/**
 * Returns location specific information regarding a user's plan currency
 *
 * @param brand
 * @param filter (opt) - Provides filter option to filter plans based on annual/monthly

 * @returns {
 *   plans: (*|Array)
 * }
 */
export default () => {
  const api = Router();
  const log = logger('subscription-service:currency');

  const getCurrencyInfo = async (req, res) => {
    const { countryISO = 'US', brand = 'sl' } = req.query;
    try {
      const allPlans = await plansForUser(null, countryISO);
      const brandSpecificPlans = allPlans.filter(({ brand: priceBrand }) => priceBrand === brand);
      const { amount: monthlyPrice } = brandSpecificPlans.find(plan => plan.interval === 'month');
      const { amount: annualPrice } = brandSpecificPlans.find(plan => plan.interval === 'year');
      const { iso, currency, country, symbol } = getCurrencyForCountry(countryISO);
      return res.status(200).send({
        iso,
        currency,
        country,
        symbol,
        monthly: (monthlyPrice / 100).toFixed(2),
        annual: (annualPrice / 100).toFixed(2),
      });
    } catch (err) {
      log.error({
        err,
        message: `There was a problem fetching currency data for ${countryISO}`,
      });
      newrelic.noticeError(err);
      return res.status(500).send({
        message: `There was a problem fetching currency data for ${countryISO}`,
      });
    }
  };

  api.get(
    '/',
    wrapErrors((req, res) => getCurrencyInfo(req, res)),
  );
  return api;
};
