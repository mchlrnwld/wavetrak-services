import { Router } from 'express';
import _ from 'lodash';
import newrelic from 'newrelic';
import { wrapErrors } from '@surfline/services-common';
import logger from '../../../common/logger';
import * as stripe from '../../../external/stripe';
import { shouldProcessCardSource } from '../../../external/stripeutils';
import * as account from '../../../models/v1/accounts';
import { buildSubscriptionObject, getFailedPaymentSubscriptionIds } from './helpers';

export default () => {
  const api = Router();
  const log = logger('subscription-service');

  const getCards = async (req, res) => {
    const { user } = req;
    if (!user || !user.stripeCustomerId) {
      return res.status(200).send({ cards: null, defaultSource: null });
    }
    try {
      const {
        sources: { data },
        default_source: defaultSource,
      } = await stripe.getCustomer(user.stripeCustomerId, ['sources']);
      const cards = data
        .filter(({ object }) => object === 'card')
        // eslint-disable-next-line camelcase
        .map(({ last4, id, exp_month, exp_year, addressZip, brand }) => ({
          brand,
          last4,
          id,
          exp_month,
          exp_year,
          expMonth: exp_month,
          expYear: exp_year,
          addressZip,
        }));

      const responseObject = {
        cards,
        defaultSource,
      };

      return res.status(200).send(responseObject);
    } catch (err) {
      log.error({
        userId: user ? user.user.toString() : null,
        err,
        message: 'There was a problem fetching cards.',
      });
      newrelic.noticeError(err);
      return res.status(500).send({
        message: 'There was a problem fetching cards.',
      });
    }
  };

  const createCard = async (req, res) => {
    let userAccount = req.user;
    const { user, subscriptions, stripeCustomerId } = userAccount;
    const { source } = req.body;

    try {
      await shouldProcessCardSource(userAccount.user);
    } catch (err) {
      const userId = userAccount.user.toString();
      log.error({
        userId,
        err,
        message: `User ${userId} exceeded create card requests.`,
      });
      return res.status(403).send({
        message: 'Cannot process request.',
      });
    }

    let card;
    const stripeCustomer = await stripe.getCustomer(stripeCustomerId, ['sources']);
    const tokenInfo = await stripe.getToken(source);
    const foundFingerprint = stripeCustomer.sources.data.reduce((found, oldCard) => {
      if (tokenInfo.card.fingerprint === oldCard.fingerprint) {
        return true;
      }
      return found;
    }, false);

    if (foundFingerprint) {
      return res.status(422).send({
        message: 'Card already exists.',
      });
    }

    const { id: newCardId } = await stripe.addCard(stripeCustomerId, req.body.source);

    try {
      await account.updateDefaultStripeSource(user, newCardId);
    } catch (err) {
      newrelic.noticeError(err);
      return res.status(500).send({
        message: 'There was a problem setting the card as default.',
      });
    }

    try {
      userAccount = await account.getAccount(req.authenticatedUserId);
      const subObj = await buildSubscriptionObject(userAccount);
      card = subObj.cards.reduce((found, current) => {
        if (current.id === newCardId) {
          return current;
        }
        return found;
      });
    } catch (err) {
      log.error({
        userId: userAccount.user.toString(),
        err,
        message: 'There was a problem creating card object.',
      });
      newrelic.noticeError(err);
      return res.status(500).send({
        message: 'There was a problem creating card object.',
      });
    }

    try {
      const subscriptionsToPay = getFailedPaymentSubscriptionIds(subscriptions);
      await stripe.payInvoices(stripeCustomerId, subscriptionsToPay);
    } catch (err) {
      log.error({
        userId: user.toString(),
        err,
        message: 'There was a problem retrying invoice payments',
      });
      newrelic.noticeError(err);
      return res.status(500).send({
        message: 'There was a problem retrying invoice payments',
      });
    }
    return res.send({ card });
  };

  const deleteCard = async (req, res) => {
    const userAccount = req.user;
    const { cardId } = req.params;
    const subObj = await stripe.getCustomer(userAccount.stripeCustomerId, ['sources']);

    if (subObj.default_source === cardId) {
      return res.status(400).send({
        message: 'The default card cannot be deleted.',
      });
    }

    const foundCard = _.find(subObj.sources.data, card => card.id === cardId);
    if (foundCard) {
      try {
        await stripe.deleteCard(userAccount.stripeCustomerId, cardId);
        const { paymentWarnings } = userAccount;
        if (paymentWarnings) {
          const existingWarnings = paymentWarnings.filter(
            item => item.cardId === cardId && item.status === 'open',
          );
          existingWarnings.forEach(item => {
            const statusToUpdate = 'unresolved';
            account.updatePaymentWarning(userAccount, item, statusToUpdate);
          });
          if (existingWarnings.length) {
            userAccount.markModified('paymentWarnings');
            await userAccount.save();
          }
        }
      } catch (err) {
        log.error({
          userId: req.authenticatedUserId,
          err,
          message: 'There was a problem with Stripe removing the card',
        });
        newrelic.noticeError(err);
        return res.status(500).send({
          message: 'There was a problem with Stripe removing the card',
        });
      }
      return res.send({ message: 'The card was deleted' });
    }

    return res.status(404).send({
      message: 'The card could not be found',
    });
  };

  const updateCard = async (req, res) => {
    let userAccount = req.user;
    const { stripeCustomerId, subscriptions } = userAccount;
    const { cardId } = req.params;
    const { opts } = req.body;

    const subObj = await stripe.getCustomer(stripeCustomerId, ['sources']);

    const foundCard = _.find(subObj.sources.data, card => card.id === cardId);
    if (foundCard) {
      try {
        const { paymentWarnings } = userAccount;
        if (paymentWarnings) {
          const existingWarning = paymentWarnings.find(
            item => item.cardId === cardId && item.type === 'invalidZip',
          );
          if (existingWarning) {
            const statusToUpdate = 'resolved';
            account.updatePaymentWarning(userAccount, existingWarning, statusToUpdate);
            userAccount.markModified('paymentWarnings');
            await userAccount.save();
          }
        }
        const cardResult = await stripe.updateCard(stripeCustomerId, cardId, opts);
        const subscriptionsToPay = getFailedPaymentSubscriptionIds(subscriptions);
        await stripe.payInvoices(stripeCustomerId, subscriptionsToPay);
        userAccount = await account.getAccount(req.authenticatedUserId);
        const buildSubObj = await buildSubscriptionObject(userAccount);

        const card = buildSubObj.cards.reduce((found, current) => {
          if (current.id === cardResult.id) {
            return current;
          }
          return found;
        });

        return res.send({ card });
      } catch (err) {
        let message = 'There was a problem with Stripe updating the card';
        let statusCode = 500;
        if (err.type && err.type.includes('Stripe')) {
          message = err.message;
          statusCode = err.statusCode;
        }
        log.error({
          userId: req.authenticatedUserId,
          err,
          message: 'There was a problem with Stripe updating the card',
        });
        newrelic.addCustomAttributes({
          statusCode,
          message,
          stack: err.stack,
        });
        return res.status(statusCode).send({
          message,
        });
      }
    }

    return res.status(404).send({
      message: 'The card could not be found',
    });
  };

  api.get(
    '/',
    wrapErrors((req, res) => getCards(req, res)),
  );
  api.post(
    '/',
    wrapErrors((req, res) => createCard(req, res)),
  );
  api.delete(
    '/:cardId',
    wrapErrors((req, res) => deleteCard(req, res)),
  );
  api.put(
    '/:cardId',
    wrapErrors((req, res) => updateCard(req, res)),
  );
  return api;
};
