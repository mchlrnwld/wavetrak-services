import zipcodes from 'zipcodes';
import postcode from 'postcode';

import * as stripe from '../../../external/stripe';
import { getAccount } from '../../../models/v1/accounts';
import { getCurrencyForCountry, currencySymbolMap } from '../../../utils/getCurrencyCode';

/* Calculates the discounted price if a coupon is applied,
 * otherwise it returns the normal price of the plan
 *
 * @param(planId) The id of the plan
 * @param(price) the default price of the plan
 * @param(discount) the discount object
 *
 * @returns discountedPrice
 */
const calculateDiscountedPrice = (planId, price, discount) => {
  let discountedPrice = price;

  if (discount) {
    // eslint-disable-next-line camelcase
    // const includesMonthly = discount?.metadata?.includes_monthly;
    if (discount.amount_off) {
      discountedPrice = price - discount.amount_off;
    }
    if (discount.percent_off) {
      discountedPrice = Math.round(price - price * (discount.percent_off / 100));
    }
  }

  return discountedPrice;
};

export const plansForUser = async (
  discount,
  countryISO = 'US',
  customerCurrency = null,
  type = 'recurring',
) => {
  let { data } = await stripe.getPrices();

  const currencyInfo = getCurrencyForCountry(countryISO);
  const { country } = currencyInfo;
  let { iso, currency, symbol } = currencyInfo;

  if (customerCurrency) {
    currency = customerCurrency;
    symbol = currencySymbolMap[currency.toUpperCase()];
    iso = customerCurrency;
  }

  // eslint-disable-next-line camelcase
  const applicableProductIds = discount?.applies_to?.products;
  if (applicableProductIds?.length) {
    data = data.filter(price => applicableProductIds.includes(price.product));
  }

  return data
    .filter(
      price =>
        price.currency === currency.toLowerCase() &&
        price.type === type.toLowerCase() &&
        price.active,
    )
    .map(price => ({
      id: price.id,
      active: price.active,
      amount: calculateDiscountedPrice(price.id, price.unit_amount, discount),
      product: price.product,
      entitlementKey: price.metadata.entitlement,
      entitlement: price.metadata.entitlement,
      upgradable: price.metadata.upgradable,
      description: price.metadata.description,
      interval: price.recurring?.interval,
      intervalCount: price.recurring?.interval_count,
      lookupKey: price.lookup_key?.toLowerCase(),
      currency: price.currency,
      name: price.name,
      trialPeriodDays: price.recurring?.trial_period_days,
      iso: price.currency === customerCurrency ? iso : currencyInfo.iso,
      symbol: price.currency === customerCurrency ? symbol : currencyInfo.symbol,
      country,
      renewalPrice: price.unit_amount,
      brand: price.metadata.query_set || price.metadata.brand,
    }));
};

export const translateV2Subscriptions = v2Subscriptions => {
  if (!v2Subscriptions.length) {
    return [];
  }
  const translatedV2Subscriptions = v2Subscriptions.map(sub => {
    const v1Params = {
      onTrial: sub.trialing,
      periodStart: new Date(sub.start * 1000).toISOString(),
      periodEnd: new Date(sub.end * 1000).toISOString(),
    };
    const stripeParams = sub.type === 'stripe' ? { stripePlanId: sub.planId } : {};
    return { ...sub, ...v1Params, ...stripeParams };
  });
  return translatedV2Subscriptions;
};

/**
 * Builds a user object suitable for return for a given user
 *
 * @param userAccount
 * @param querySet plan query_set to filter on
 *
 * @returns {
 *   availablePlans: *,
 *   subscriptions: (*|Array),
 *   archivedSubscriptions: (*|Array
 * }
 */
export const buildSubscriptionObject = async (userAccount, coupon, v2Subscriptions) => {
  const translatedV2Subscriptions = v2Subscriptions
    ? translateV2Subscriptions(v2Subscriptions)
    : [];
  if (userAccount) {
    // const v1Subscriptions = userAccount.subscriptions.filter(({ type }) => type !== 'gift');
    // eslint-disable-next-line no-param-reassign
    userAccount.subscriptions = translatedV2Subscriptions;
  }
  // no matter the user state, we always want to return a list of plans
  // that a user can subscribe to.
  const subObj = {
    availablePlans: await plansForUser(coupon),
    subscriptions: [],
    archivedSubscriptions: [],
    updatedAt: null,
    createdAt: null,
  };

  if (!userAccount && !translatedV2Subscriptions.length) {
    return subObj;
  }

  if ((!userAccount || !userAccount.stripeCustomerId) && translatedV2Subscriptions?.length) {
    const { user, stripeCustomerId } =
      translatedV2Subscriptions.find(({ stripeCustomerId: custId }) => custId) ||
      translatedV2Subscriptions[0];
    // eslint-disable-next-line no-param-reassign
    userAccount = {
      user,
      stripeCustomerId,
      subscriptions: translatedV2Subscriptions,
      archivedSubscriptions: [],
    };
  }

  const {
    user,
    stripeCustomerId,
    subscriptions,
    archivedSubscriptions,
    updatedAt,
    createdAt,
  } = userAccount;

  const { currency: customerCurrency } = stripeCustomerId
    ? await stripe.getCustomer(stripeCustomerId)
    : {};

  if (customerCurrency) {
    subObj.availablePlans = await plansForUser(coupon, undefined, customerCurrency);
  }

  subObj.subscriptions = subscriptions;

  for (const sub of subObj.subscriptions) {
    sub.effectivePrice = null;
    sub.renewalPrice = null;
    if (sub.type === 'stripe' && sub.subscriptionId && stripeCustomerId) {
      try {
        const planId = sub.stripePlanId || sub.planId;
        const {
          recurring: {
            interval_count: intervalCount,
            interval,
            trial_period_days: trialPeriodDays,
          },
          unit_amount: amount,
          currency,
          id,
          nickname: name,
          metadata: { description, upgradable, entitlement, query_set: querySet, brand },
        } = await stripe.getPrice(planId);

        // Add the stripe plan info to the sub object for backwards compatibility
        sub.plan = {
          intervalCount,
          interval,
          amount,
          currency,
          id,
          name,
          entitlementKey: entitlement,
          entitlement,
          trialPeriodDays,
          description,
          upgradable,
          brand: querySet || brand,
        };

        const { discount, amount_due: amountDue } = await stripe.getUpcomingInvoice(
          stripeCustomerId,
          sub.subscriptionId,
        );
        const upcomingCoupon = discount ? discount.coupon : null;
        sub.renewalPrice = amountDue;
        sub.plan.symbol = currencySymbolMap[currency.toUpperCase()];
        /**
         * Whilst a subscription is trialing, all plans should reflect a discounted
         * price if applicable.
         */
        if (sub.onTrial) {
          subObj.availablePlans = await plansForUser(upcomingCoupon, null, customerCurrency);
          sub.effectivePrice = amountDue;
        } else {
          sub.effectivePrice = amountDue;

          const currentPlan = subObj.availablePlans.filter(plan => sub.stripePlanId === plan.id);
          if (currentPlan && currentPlan[0]) {
            sub.effectivePrice = currentPlan[0].amount;
          }
        }
      } catch (err) {
        sub.effectivePrice = null;
      }
    }
  }
  subObj.archivedSubscriptions = archivedSubscriptions;
  subObj.updatedAt = updatedAt;
  subObj.createdAt = createdAt;

  if (stripeCustomerId) {
    const { sources, default_source: defaultSource } = await stripe.getCustomer(stripeCustomerId, [
      'sources',
    ]);

    // get 10 most recent cards attached to stripe customer account
    subObj.cards = sources.data
      .filter(card => card.object === 'card')
      .map(card => ({
        brand: card.brand,
        last4: card.last4,
        id: card.id,
        expMonth: card.exp_month,
        expYear: card.exp_year,
        addressZip: card.address_zip,
      }));

    // eslint-disable-next-line camelcase
    if (defaultSource) {
      // eslint-disable-next-line camelcase
      subObj.defaultStripeSource = defaultSource;
    }

    subObj.stripeCustomerId = stripeCustomerId;
  }

  subObj.user = user;
  return subObj;
};

/**
 * Builds an object that contains all the Offered Plans suitable for a given user
 *
 * @param userAccount - Object
 * @param brand - String
 *
 * @returns {
 *   offeredPlans: *,
 *   subscriptions: (*|Array),
 *   user,
 *   stripeCustomerId
 * }
 */
export const buildOfferedPlansObject = async (userAccount, brand) => {
  const { user, stripeCustomerId, subscriptions } = userAccount;
  // A user can only have 1 active stripe subscription for a brand
  const currentSubscriptionForBrand = subscriptions.find(
    sub => sub.entitlement.startsWith(brand) && sub.type === 'stripe',
  );

  const { currency } = await stripe.getCustomer(stripeCustomerId);
  const availablePlans = await plansForUser(null, null, currency);

  const offeredPlans = availablePlans.filter(
    plan =>
      plan.entitlement.startsWith(brand) && plan.id !== currentSubscriptionForBrand.stripePlanId,
  );

  return {
    offeredPlans,
    currentSubscriptionForBrand,
    user,
    stripeCustomerId,
  };
};

/**
 * This function covers the new premium logic for checking a users current plan.
 * If the plan matches one of the old plans the function will return its new premium
 * equivalent plan.
 * @param {string} currentPlan The id of the users current stripe plan
 */
export const checkNewPremiumPlanEligible = currentPlan => {
  /* eslint-disable camelcase */
  let planId = currentPlan;
  let proration_behavior = 'always_invoice';

  // If the user is resuming a subscription on the old plan, force them to the new plan
  if (currentPlan === 'sl_monthly') {
    planId = 'sl_monthly_v2';
    proration_behavior = 'none';
  } else if (currentPlan === 'sl_annual') {
    planId = 'sl_annual_v2';
    proration_behavior = 'none';
  }
  return { planId, proration_behavior };
  /* eslint-enable camelcase */
};

export const validateCoupon = coupon =>
  new Promise(async (resolve, reject) => {
    try {
      const validCoupon = await stripe.getCoupon(coupon, ['applies_to']);
      return resolve(validCoupon);
    } catch (error) {
      return reject(error);
    }
  });

export const loadUser = async (req, res, next) => {
  try {
    // eslint-disable-next-line no-param-reassign
    req.user = await getAccount(req.authenticatedUserId);
  } catch (err) {
    next(err);
  }
  next();
};

export const getFailedPaymentSubscriptionIds = subscriptions =>
  subscriptions.map(({ subscriptionId, failedPaymentAttempts }) => {
    if (failedPaymentAttempts > 0) return subscriptionId;
    return null;
  });

export const isZipValid = (countryCode, zip) => {
  let isValid = true;
  if (countryCode === 'US') {
    isValid = zipcodes.lookup(zip) || false;
  }
  if (countryCode === 'UK') {
    const pcode = new postcode(zip);
    isValid = pcode.valid();
  }
  return isValid;
};
