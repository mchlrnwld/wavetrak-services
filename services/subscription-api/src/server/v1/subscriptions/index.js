import { Router } from 'express';
import newrelic from 'newrelic';
import * as bodyParser from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import logger from '../../../common/logger';
import * as account from '../../../models/v1/accounts';
import { getActiveSubscriptionsByUserId } from '../../../models/subscriptions';
import * as stripe from '../../../external/stripe';
import invoices from './invoices';
import coupons from './coupons';
import currency from './currency';
import promotions from './promotions';
import { buildSubscriptionObject, buildOfferedPlansObject, loadUser } from './helpers';

const jsonBodyParser = bodyParser.json();

const getReqSignature = req =>
  Object({
    userId: req.authenticatedUserId || -1,
  });

export default function subscriptions() {
  const api = Router();
  const log = logger('subscription-service');

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/subscriptions',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  const authenticateRequest = (req, res, next) => {
    if (req.headers['x-auth-userid']) {
      // eslint-disable-next-line no-param-reassign
      req.authenticatedUserId = req.headers['x-auth-userid'];
      const id = req.authenticatedUserId || null;
      newrelic.addCustomAttribute('userId', id);
    }
    next();
  };

  /**
   * Returns subscription details for a given user.
   *
   * TODO implement discount code lookup
   *
   * GET /subscription
   */
  const getSubscription = async (req, res) => {
    const userAccount = await account.getAccount(req.authenticatedUserId);
    const v2Subscriptions = req.authenticatedUserId
      ? await getActiveSubscriptionsByUserId(req.authenticatedUserId)
      : null;
    const {
      query: { coupon },
    } = req;

    if (coupon) {
      // check mongo
      if (userAccount && userAccount.winbacks) {
        const userWinbacks = userAccount.winbacks;
        let percentOff;
        let couponRef;

        userWinbacks.forEach(winback => {
          if (winback.code === coupon) {
            percentOff = winback.percent_off;
            couponRef = winback.ref;
          }
        });
        // post to coupon to stripe and respond as valid
        if (percentOff && couponRef) {
          try {
            const stripeCoupon = await stripe.getCoupon(couponRef);
            const subObj = await buildSubscriptionObject(
              userAccount,
              stripeCoupon,
              v2Subscriptions,
            );
            return res.send(subObj);
          } catch (err) {
            return res.status('400').send(err);
          }
        }
      }
      // check stripe
      try {
        const stripeCoupon = await stripe.getCoupon(coupon);
        const subObj = await buildSubscriptionObject(userAccount, stripeCoupon, v2Subscriptions);
        return res.send(subObj);
      } catch (err) {
        return res.status('400').send(err);
      }
    }

    const subObj = await buildSubscriptionObject(userAccount, null, v2Subscriptions);
    return res.send(subObj);
  };

  const getOfferedPlans = async (req, res) => {
    const {
      query: { brand },
    } = req;
    // ensure the correct parameters are passed to the endpoint
    if (!brand) {
      log.warn({
        ...getReqSignature(req),
        message: 'Tried to call getEligiblePlans with incorrect parameters',
      });
      return res.status(422).send({
        message: 'A brand is required.',
      });
    }
    const userAccount = await account.getAccount(req.authenticatedUserId);
    if (!userAccount) {
      return res.send({
        message: 'No User account associated with the user',
      });
    }
    const offeredPlansObj = await buildOfferedPlansObject(userAccount, brand);
    return res.send(offeredPlansObj);
  };

  api.use('*', jsonBodyParser);
  api.use('/currency', jsonBodyParser, currency());
  api.use('/invoices', jsonBodyParser, authenticateRequest, loadUser, invoices());
  api.use('/coupons', jsonBodyParser, authenticateRequest, loadUser, coupons());
  api.use('/promotions', jsonBodyParser, authenticateRequest, promotions());
  api.get(
    '/',
    trackRequest,
    authenticateRequest,
    wrapErrors((req, res) => getSubscription(req, res)),
  );
  api.get(
    '/cancellation-offer',
    trackRequest,
    authenticateRequest,
    wrapErrors((req, res) => getOfferedPlans(req, res)),
  );

  return api;
}
