import * as stripe from '../../../../../external/stripe';
import * as account from '../../../../../models/v1/accounts';
import logger from '../../../../../common/logger';

const log = logger('invoice-integration-stripe');
/**
 * Gets the invoice details from Stripe
 * @param charge
 * @returns {*}
 */
async function getInvoiceDetailsFromStripe(charge) {
  const chargeDetails = await stripe.getCharge(charge);
  const { invoice } = chargeDetails;
  const invoiceDetails = await stripe.getInvoiceById(invoice);
  return invoiceDetails;
}

/**
 * Charge.dispute.created fires when a dispute is created
 * Adds a dispute property to the disputed UserAccount.subscriptions object  with value "opened"
 * @param disputeObject
 * @returns {*}
 */
export const created = async disputeObject => {
  const { charge } = disputeObject;
  let userAccount;
  let invoiceDetails;
  try {
    invoiceDetails = await getInvoiceDetailsFromStripe(charge);
    userAccount = await account.getAccountFromSubscription(
      invoiceDetails.subscription,
      invoiceDetails.customer,
    );
  } catch (err) {
    log.warn({
      message: `Error processing for Charge: ${charge}`,
      webhookHandler: 'createDispute',
      error: err.stack,
    });
    return true;
  }

  const subscription = userAccount.subscriptions.find(
    elem => elem.subscriptionId === invoiceDetails.subscription,
  );
  subscription.dispute = 'opened';
  userAccount.markModified('subscriptions');

  const { stripePlanId, subscriptionId } = subscription;

  // cancel at period end when the dispute is created
  await stripe.updateSubscription(subscriptionId, stripePlanId, { cancelAtPeriodEnd: true });
  await userAccount.save();
  return true;
};

/**
 * Charge.dispute.closed fires when a dispute is closed
 * Updates the dispute property in UserAccount.subscriptions object
 *    with the outcome of the dispute - i.e either won or lost
 * If dispute is lost cancels the Subscription immediately
 * @param disputeObject
 * @returns {*}
 */
export const closed = async disputeObject => {
  const { charge, status } = disputeObject;
  let userAccount;
  let invoiceDetails;
  try {
    invoiceDetails = await getInvoiceDetailsFromStripe(charge);
    userAccount = await account.getAccountFromSubscription(
      invoiceDetails.subscription,
      invoiceDetails.customer,
    );
  } catch (err) {
    log.warn({
      message: `Error processing for Charge: ${charge}`,
      webhookHandler: 'closeDispute',
      error: err.stack,
    });
    return true;
  }
  const subscription = userAccount.subscriptions.find(
    elem => elem.subscriptionId === invoiceDetails.subscription,
  );

  if (status === 'won' || status === 'warning_closed') {
    subscription.dispute = 'won';
    userAccount.markModified('subscriptions');

    await userAccount.save();
    return true;
  }
  if (status === 'lost') {
    subscription.dispute = 'lost';
    userAccount.markModified('subscriptions');

    await userAccount.save();

    // Cancels subscription immediately
    await stripe.removeSubscription(subscription.subscriptionId);

    return true;
  }
  // Do nothing for other statuses
  return true;
};
