import _ from 'lodash';
import logger from '../../../../../common/logger';
import * as account from '../../../../../models/v1/accounts';

const log = logger('subscriptions-v1-integration-stripe');

/**
 * Occurs whenever a customer ends their subscription.
 *
 * @param evt
 * @returns {*}
 */
export const deleted = async evt => {
  const { id, customer } = evt;
  let userAccount;
  try {
    userAccount = await account.getAccountFromSubscription(id, customer);
  } catch (err) {
    // if an active subscription can't be found on the subscription.deleted
    // it's safe to assume that the subscription has already been deleted, and
    // we can return a successful response indicator.
    log.warn({
      err,
      message: 'Tried to delete a subscription that does not exist',
      stripeCustomerId: customer,
      subscriptionId: id,
      stripeEventType: 'customer.subscription.deleted',
      webhookHandler: 'subscriptionDeleted',
    });
    return true;
  }

  const { stripeCustomerId, paymentWarnings, subscriptions } = userAccount;
  const sub = _.remove(subscriptions, elem => elem.subscriptionId === id);

  if (sub.length > 0) {
    const [subscription] = sub;

    const hasActiveStripeSubscriptions = subscriptions.some(({ type }) => type === 'stripe');
    const { dissociateAtPeriodEnd } = subscription;
    if (!hasActiveStripeSubscriptions && dissociateAtPeriodEnd) {
      userAccount.stripeCustomerId = undefined;
    }

    // archived subscription
    userAccount.archivedSubscriptions.push({
      ...subscription,
      archivedDate: new Date(),
      stripeCustomerId,
    });

    // update payment warnings
    if (paymentWarnings) {
      const existingWarnings = paymentWarnings.filter(
        item => item.subscriptionId === id && item.status === 'open',
      );
      existingWarnings.forEach(item => {
        const statusToUpdate = 'unresolved';
        account.updatePaymentWarning(userAccount, item, statusToUpdate);
      });
      if (existingWarnings.length) {
        userAccount.markModified('paymentWarnings');
      }
    }

    userAccount.markModified('subscriptions');
    userAccount.markModified('archivedSubscriptions');
    await userAccount.save();

    return true;
  }

  return false;
};

/**
 * Updates UserAccount.subscriptions and Segment.io's user identity based on webhook events.
 *
 * Attribute we care about
 *  - canceled_at (a user cancelled)
 *  - status (a user has cancelled or resumed their plan)
 *  - plan.id (a user changed plans)
 *
 * @param evt           Current stripe event
 * @param previousData  Previous stripe event
 * @returns {*}
 */
export const updated = async (evt, previousData) => {
  const {
    id,
    customer,
    canceled_at: canceledAt,
    plan,
    current_period_start: currentPeriodStart,
    current_period_end: currentPeriodEnd,
    metadata,
  } = evt;
  let userAccount;

  try {
    userAccount = await account.getAccountFromSubscription(id, customer);
  } catch (err) {
    log.warn({
      message: `Customer ${customer} subscription not found`,
      webhookHandler: 'subscriptionUpdated',
      error: err.stack,
    });
    return false;
  }

  // a user has transitioned from trialing
  if (evt.status !== 'trialing' && previousData && previousData.status === 'trialing') {
    const subscription = userAccount.subscriptions.find(elem => elem.subscriptionId === id);
    subscription.onTrial = false;
    userAccount.markModified('subscriptions');
  }

  // a user has canceled their subscription
  if (canceledAt) {
    const subscription = userAccount.subscriptions.find(elem => elem.subscriptionId === id);
    subscription.expiring = true;
    userAccount.markModified('subscriptions');

    // a user has renewed their unexpired subscription
  } else if (previousData && previousData.canceled_at && !canceledAt) {
    const subscription = userAccount.subscriptions.find(elem => elem.subscriptionId === id);
    subscription.expiring = false;
    userAccount.markModified('subscriptions');
  }

  // a users plan has been changed
  if (plan && previousData && previousData.plan && plan.id !== previousData.plan.id) {
    const subscription = userAccount.subscriptions.find(elem => elem.subscriptionId === id);
    subscription.stripePlanId = plan.id;
    subscription.periodStart = new Date(currentPeriodStart * 1000);
    subscription.periodEnd = new Date(currentPeriodEnd * 1000);
    userAccount.markModified('subscriptions');
  }

  // A user has requested to change their default currency when their subscription expires
  const { dissociate } = metadata;
  if (dissociate) {
    const subscription = userAccount.subscriptions.find(elem => elem.subscriptionId === id);
    subscription.dissociateAtPeriodEnd = !!dissociate;
    subscription.stripeCustomerId = customer;
  }

  await userAccount.save();
  return true;
};
