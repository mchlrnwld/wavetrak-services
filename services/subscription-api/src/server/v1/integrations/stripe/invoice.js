import logger from '../../../../common/logger';
import * as stripe from '../../../../external/stripe';
import * as account from '../../../../models/v1/accounts';
import { trackSubscriptionGrandfathered, trackTrialEnding } from '../../../../common/trackEvent';

const log = logger('invoice-integration-stripe');
/**
 *
 * This hook may be useful if we need to compare things like
 * invoices paid vs invoices created.
 *
 * Occurs whenever a new invoice is created. If you are using webhooks, Stripe
 * will wait one hour after they have all succeeded to attempt to pay the
 * invoice; the only exception here is on the first invoice, which gets created
 * and paid immediately when you subscribe a customer to a plan. If your
 * webhooks do not all respond successfully, Stripe will continue* retrying
 * the webhooks every* hour and will not attempt to pay the invoice. After 3
 * days, Stripe will attempt to pay the invoice regardless of whether or not
 * your webhooks have succeeded. See how to respond to a webhook.
 */

/**
 * Primairly used for listening to when users signup for subscriptions with
 * a discount code.
 *
 * `invoice.created fires whenever a new invoice is created.
 *
 * Since most coupons in Stripe are applied "ONCE", the discount
 * object is only applied to the first invoice created for that subscription.
 * NOTE: Coupons of this nature are not added to the Subscription discount object.
 */
export const created = async evt => {
  const { customer } = evt;
  try {
    await account.getAccountFromStripe(customer);
  } catch (err) {
    log.warn({
      message: `Customer ${customer} not found`,
      webhookHandler: 'invoice.created',
      error: err.stack,
    });
    return true;
  }

  return true;
};

/**
 * Updates `UserAccount.subscriptions` if
 * necessary.
 *
 * 'invoice.payment_failed' fires whenever an invoice payment fails.
 * This can occur due to a declined payment or because the customer has no active card.
 * A particular case of note is that if a customer with no active
 * card reaches the end of its free trial, an invoice.payment_failed notification will occur.
 */
export const paymentFailed = async evt => {
  const { subscription, customer } = evt;
  let userAccount;
  try {
    userAccount = await account.getAccountFromStripe(customer);
  } catch (err) {
    log.warn({
      message: `Customer ${customer} subscription not found`,
      webhookHandler: 'paymentFailed',
      error: err.stack,
    });
    return true;
  }
  let subscriptionAlreadyArchived = false;
  let sub = userAccount.subscriptions.find(elem => elem.subscriptionId === subscription);
  if (!sub) {
    sub = userAccount.archivedSubscriptions.find(elem => elem.subscriptionId === subscription);
    if (sub) {
      subscriptionAlreadyArchived = true;
    } else {
      throw new Error(
        `Payment failed for unknown subscription(${subscription}) on customer(${customer})`,
      );
    }
  }

  if (sub.failedPaymentAttempts && Number.isInteger(sub.failedPaymentAttempts)) {
    sub.failedPaymentAttempts += 1;
  } else {
    sub.failedPaymentAttempts = 1;
  }
  sub.expiring = true;
  let existingWarning;
  const { paymentWarnings } = userAccount;
  const { default_source: chargedCard } = await stripe.getCustomer(customer);
  const paymentWarning = {
    subscriptionId: subscription,
    cardId: chargedCard,
    planId: sub.stripePlanId,
    type: 'failedPayment',
    status: 'open',
  };
  if (paymentWarnings) {
    existingWarning = paymentWarnings.find(
      warning =>
        warning.subscriptionId === subscription &&
        warning.type === 'failedPayment' &&
        warning.status === 'open',
    );
    if (existingWarning && existingWarning.cardId !== chargedCard) {
      /*
      If a warning exists with a different card then mark the existing warning
      as unresolved and insert a new warning with the new card.
      */
      const statusToUpdate = 'unresolved';
      account.updatePaymentWarning(userAccount, existingWarning, statusToUpdate);
      // insert warning
      userAccount.paymentWarnings.push({ ...paymentWarning });
      userAccount.markModified('paymentWarnings');
    }
  }
  if (!existingWarning && !subscriptionAlreadyArchived && sub.failedPaymentAttempts <= 5) {
    userAccount.paymentWarnings.push({ ...paymentWarning });
    userAccount.markModified('paymentWarnings');
  }

  const existingEntitlement = sub.entitlement;
  const archivedSubs = userAccount.archivedSubscriptions.find(
    ({ entitlement }) => entitlement === existingEntitlement,
  );
  if (!archivedSubs && sub.onTrial) await stripe.removeSubscription(subscription);

  userAccount.markModified('archivedSubscriptions');
  userAccount.markModified('subscriptions');

  await userAccount.save();
  return true;
};

/**
 * Updates `UserAccount.subscriptions` if
 * necessary.
 *
 * Occurs whenever an invoice attempts to be paid, and the payment succeeds.
 *
 * Attributes used
 *  - period_start
 *  - period_end
 */
export const paymentSucceeded = async evt => {
  const { subscription: subscriptionId, customer, lines, discount } = evt;
  const {
    plan,
    period: { start, end },
  } = lines.data.find(
    ({ subscription, type }) => subscription === subscriptionId && type === 'subscription',
  ) || { period: {} };
  /**
   * Prevent failed webhook events for $0 invoices of expired subscriptions
   * For example: https://dashboard.stripe.com/events/evt_1HPDFLJ4F5N75cpX4Xj88VUr
   */
  if (!plan) {
    return true;
  }

  let userAccount;
  try {
    userAccount = await account.getAccountFromSubscription(subscriptionId, customer);
  } catch (err) {
    log.warn({
      message: `Customer ${customer} subscription not found`,
      webhookHandler: 'paymentSucceeded',
      error: err.stack,
    });
    return false;
  }

  const { stripeCustomerId, subscriptions, user, paymentWarnings } = userAccount;

  const sub = subscriptions.find(elem => elem.subscriptionId === subscriptionId);

  sub.periodStart = stripe.stripeDateToMongo(start);
  sub.periodEnd = stripe.stripeDateToMongo(end);
  sub.failedPaymentAttempts = 0;
  sub.expiring = false;

  if (discount && discount.coupon) {
    sub.coupon = discount.coupon;
  }

  sub.renewalCount =
    sub.renewalCount && !sub.onTrial
      ? sub.renewalCount + 1
      : await stripe.countPaidInvoicesBySubscription({
          customer: stripeCustomerId,
          subscription: sub.subscriptionId,
        });

  const trialEndDate = 'now';
  if (plan.id === 'sl_annual') {
    await stripe.updateSubscription(subscriptionId, 'sl_annual_v2', {
      proration_behavior: 'none',
      trialEndDate,
    });
    trackSubscriptionGrandfathered(user.toString());
  } else if (plan.id === 'sl_monthly') {
    await stripe.updateSubscription(subscriptionId, 'sl_monthly_v2', {
      proration_behavior: 'none',
      trialEndDate,
    });
  }

  if (paymentWarnings) {
    const existingWarnings = paymentWarnings.filter(
      warning =>
        warning.subscriptionId === subscriptionId &&
        warning.type === 'failedPayment' &&
        warning.status === 'open',
    );
    existingWarnings.forEach(item => {
      const statusToUpdate = 'resolved';
      account.updatePaymentWarning(userAccount, item, statusToUpdate);
    });
    if (existingWarnings.length) {
      userAccount.markModified('paymentWarnings');
    }
  }
  userAccount.markModified('subscriptions');
  await userAccount.save();
  return true;
};

/**
 * Primarily used to trigger sucbscription billing related segment events.
 *
 * `invoice.upcoming` Occurs X number of days before a subscription
 * is scheduled to create an invoice that is automatically charged—where
 * X is determined by your subscriptions settings.
 * Note: The received Invoice object will not have an invoice ID.
 */
export const upcoming = async evt => {
  const { customer, subscription } = evt;
  try {
    const userAccount = await account.getAccountFromSubscription(subscription, customer);
    const { user, subscriptions } = userAccount;

    const { onTrial } = subscriptions.find(
      ({ subscriptionId }) => subscriptionId === subscription.toString(),
    );
    if (onTrial) {
      const { default_source: defaultSource } = await stripe.getCustomer(customer, [
        'default_source',
      ]);
      const ccBrand = defaultSource?.brand;
      trackTrialEnding(evt, user, ccBrand);
    }

    return true;
  } catch (err) {
    log.warn({
      message: `Customer ${customer} not found`,
      webhookHandler: 'paymentSucceeded',
      error: err.stack,
    });
    return false;
  }
};
