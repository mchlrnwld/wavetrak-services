import { Router } from 'express';
import newrelic from 'newrelic';
import bodyparser from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import logger from '../../../../common/logger';

import * as account from '../../../../models/v1/accounts';

import * as stripe from '../../../../external/stripe';
import * as stripeutils from '../../../../external/stripeutils';

// stripe webhook functions
import * as customerSubscription from './customer/subscription';
import * as invoice from './invoice';
import * as chargeDispute from './charge/dispute';

/**
 * Hash containing key lookups matching functions to process Stripe webhook
 * events.
 */
export const mappings = {
  'invoice.created': invoice.created,
  'invoice.payment_succeeded': invoice.paymentSucceeded,
  'invoice.payment_failed': invoice.paymentFailed,
  'invoice.upcoming': invoice.upcoming,
  'customer.subscription.deleted': customerSubscription.deleted,
  'customer.subscription.updated': customerSubscription.updated,
  'customer.subscription.created': customerSubscription.created,
  'charge.dispute.created': chargeDispute.created,
  'charge.dispute.closed': chargeDispute.closed,
};

export default () => {
  const api = Router();
  const log = logger('subscriptions-integration-stripe');

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/integrations/v1/stripe/handler',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  /**
   * Return a stripe event signature suitable for log messages.
   *
   * @param evt to derive signature from
   */
  const getStripeEventSignature = async evt => {
    const stripeCustomerId = evt.data && evt.data.object ? evt.data.object.customer : null;
    let userId = null;
    if (stripeCustomerId) {
      try {
        const userAccount = await account.getAccountFromStripe(stripeCustomerId);
        userId = `${userAccount.user}`;
      } catch (err) {
        log.warn({
          err,
          eventId: evt.id,
          message: 'Could not find a UserAccount for the webhook',
        });
      }
    }

    return {
      stripeEventType: evt.type,
      stripeEventId: evt.id,
      stripeCustomerId,
      userId,
    };
  };

  /**
   * Verifies that the webhook currently being processed is valid. They redis
   * key is in the format of `stripe-event:eventId`. The value of the key is the
   * data that it was set, and the TTL on the key is one month from that date.
   *
   * Note: development and sandbox environments will allow a Stripe webhook to
   * run again if the event has already been processed.
   *
   * A valid webhook...
   *  - is not in redis
   *  - has a valid event id (`req.body.id`)
   */
  const verifyWebhook = async (req, res, next) => {
    const signature = await getStripeEventSignature(req.body);
    log.info({
      ...signature,
      message: 'Received Stripe event',
    });

    if (process.env.VERIFY_WEBHOOKS === 'false') {
      newrelic.addCustomAttribute('verified', true);
      return next();
    }

    try {
      const confirmation = await stripe.retrieveEvent(req.body.id);
      const hasRun = false; // await stripeutils.requestDidRun(req.body.id);
      if (confirmation && !hasRun) {
        log.info({
          ...signature,
          message: 'Verified Stripe event',
        });
        newrelic.addCustomAttribute('verified', true);
        return next();
      }
      const signatureKeys = Object.keys(signature);
      signatureKeys.map(key => newrelic.addCustomAttribute(key, signature[key]));

      if (hasRun) {
        log.warn({
          ...signature,
          message: 'This webhook has already been processed',
        });
        newrelic.addCustomAttribute('verified', true);
        return res.status(422).send({
          message: 'The webhook has already been processed',
        });
      }
      log.warn({
        ...signature,
        message: 'This webhook could not be verified by Stripe',
      });
    } catch (err) {
      log.error({
        ...signature,
        errorMessage: err.toString(),
        err,
        message: 'There was a problem processing the event',
      });
      return res.status(500).send('There was a problem processing the event.');
    }

    newrelic.addCustomAttribute('verified', false);
    return res.status(401).send('There was a problem processing the event.');
  };

  /**
   * Primary entry point for processing Stripe webhook requests.
   */
  const eventHandler = async (req, res) => {
    const handler = mappings[req.body.type];
    const signature = await getStripeEventSignature(req.body);
    const requestId = req.body.id;

    const signatureKeys = Object.keys(signature);
    signatureKeys.map(key => newrelic.addCustomAttribute(key, signature[key]));
    if (handler && requestId) {
      try {
        const result = await handler(req.body.data.object, req.body.data.previous_attributes);
        if (result) {
          await stripeutils.processRequest(requestId);
          log.info({
            ...signature,
            message: 'Successfuly processed Stripe event',
          });
          return res.status(200).send('Ok');
        }
      } catch (err) {
        newrelic.noticeError(err);
        log.error({
          ...signature,
          errorMessage: err.toString(),
          err,
          message: 'There was a fatal problem processing a Stripe Event',
        });
        return res.status(500).send('There was an error processing the request.');
      }
    }
    log.error({
      ...signature,
      message: 'Problem processing a Stripe Event',
    });
    return res.status(422).send('Could not process the request');
  };

  api.use('*', bodyparser.json());
  api.post('/firehose', async (req, res) => res.send('Ok'));
  api.post(
    '/handler',
    trackRequest,
    verifyWebhook,
    wrapErrors((req, res) => eventHandler(req, res)),
  );

  return api;
};
