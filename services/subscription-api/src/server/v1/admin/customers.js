import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import trackRequest from '../../middleware/trackRequest';
import { getAccountFromStripe, updatePaymentWarning } from '../../../models/v1/accounts';
import { getCustomer, updateCustomer } from '../../../external/stripe';
import { trackStripeCustomerDissociated } from '../../../common/trackEvent';
import {
  getUserIdFromStripeCustomerId,
  removeStripeCustomer,
} from '../../../models/customers/stripe';
import { getActiveSubscriptionsByUserId } from '../../../models/subscriptions';

export default () => {
  const api = Router();

  /**
   * Get Stripe Customer Details
   * @desc An admin API to retrieve information about a Wavetrak User's Stripe Customer Record.
   *
   * @query {string} customerId - The uniquie customer identifier used to link a Stripe Customer with a Wavetrak User.
   *
   * @returns {object} - The Stripe Customer object with an expanded default_source property.
   * https://stripe.com/docs/api/customers/object
   *
   */
  const getCustomerDetails = async (req, res) => {
    const {
      query: { customerId },
    } = req;
    const userAccount = await getAccountFromStripe(customerId);
    if (!userAccount) {
      return res
        .status(401)
        .send({ message: 'This Stripe Customer is not associated with a Wavetrak User.' });
    }

    const customer = await getCustomer(customerId, ['default_source']);
    return res.send({ customer });
  };

  /**
   * Disassociate a Wavetrak User from a Stripe Customer account.
   * @desc An admin API to remove the direct reference between a UserAccount document and a Stripe Customer account.
   * Additionally, and indirect relations is established between any archived Stripe subscriptions on the User and
   * the disassociated Stripe Customer account.  This is done to maintain historical data integrity.
   *
   * @param {string} customerId - The uniquie customer identifier used to link a Stripe Customer with a Wavetrak User.
   *
   * @returns {object} - A success message
   *
   */
  const dissociateCustomer = async (req, res) => {
    const {
      body: { customerId },
    } = req;

    const user = await getUserIdFromStripeCustomerId(customerId);
    if (user) {
      const activeSubscriptions = await getActiveSubscriptionsByUserId(user);
      const hasActiveStripeSubscription = activeSubscriptions.some(({ type }) => type === 'stripe');
      if (hasActiveStripeSubscription) {
        return res.status(400).send({
          message:
            'Cannot disassociate a Customer with an Active Stripe Subscription.  Please cancel the subscription first.',
        });
      }
      await removeStripeCustomer(user);
      await updateCustomer(customerId, {
        description: 'Dissociated Customer',
        metadata: { oldCustomer: 'true' },
      });
      trackStripeCustomerDissociated(user, customerId);
    }

    const userAccount = await getAccountFromStripe(customerId);
    if (userAccount) {
      const { archivedSubscriptions, stripeCustomerId, paymentWarnings } = userAccount;

      // Maintain a reference to the disassociated Stripe account for past subscriptions
      const updatedArchive = archivedSubscriptions.map(subscription => {
        if (subscription.type === 'stripe' && !subscription.stripeCustomerId) {
          return { ...subscription, stripeCustomerId };
        }
        return subscription;
      });

      userAccount.stripeCustomerId = undefined;
      userAccount.archivedSubscriptions = updatedArchive;

      paymentWarnings.forEach(item => {
        if (item.type === 'invalidZip' && item.status === 'open') {
          updatePaymentWarning(userAccount, item, 'resolved');
        }
      });

      userAccount.markModified('stripeCustomerId');
      userAccount.markModified('archivedSubscriptions');
      userAccount.markModified('paymentWarnings');
      await userAccount.save();
    }

    return res.status(200).send({
      message: 'Successfully disassociated this User from their Stripe Customer account.',
    });
  };

  api.get(
    '/',
    trackRequest,
    wrapErrors((req, res) => getCustomerDetails(req, res)),
  );
  api.delete(
    '/',
    trackRequest,
    wrapErrors((req, res) => dissociateCustomer(req, res)),
  );

  return api;
};
