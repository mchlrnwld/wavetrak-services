import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import logger from '../../../common/logger';
import { getAccount, getAccountVerbose, createAccount } from '../../../models/v1/accounts';
import getUser from '../../../models/v1/accounts/info';
import { buildSubscriptionObject } from '../subscriptions/helpers';
import iap from './iap';
import { cancelSubscription, getSubscription } from '../../../external/stripe';
import UserAccountModel from '../../../models/v1/accounts/UserAccountModel';

export default () => {
  const api = Router();
  const log = logger('admin/subscriptions');

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/subscriptions',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  const createUserAccount = async (req, res) => {
    const { userId } = req.body;
    try {
      const user = await getUser(userId);
      if (!user) return res.status(404).send('User not found');

      let userAccount = await getAccount(userId);
      if (userAccount) {
        return res.status(409).send('User already has an account');
      }

      userAccount = await createAccount(userId);
      return res.send(await buildSubscriptionObject(userAccount));
    } catch (err) {
      const message = 'Unable to create user account';
      log.error({ err, message });
      return res.status(500).send(message);
    }
  };

  /**
   *
   * Admin Delete Subscription Handler
   *
   * @description This Stripe specific API should be used to cancel an active subscription and
   * dissociate a UserAccount from their corresponding Stripe Customer if the necessary params
   * are passed to this API.
   *
   * @param {string} subscriptionId - Stripe subscription id
   * @param {boolean} dissociate - Optional parameter indicating Stripe Customer dissociation upon
   *  subscription expiration.  If passed, this value will be included in the stripe subscription metadata.
   *
   * @returns {res} - Success message
   */
  const deleteSubscription = async (req, res) => {
    const {
      body: {
        subscriptionId,
        options: { dissociate },
      },
    } = req;
    if (!subscriptionId) {
      return res.status(400).send({ message: 'Cannot delete a subscription without a valid id.' });
    }

    try {
      const metadata = dissociate ? { dissociate } : null;
      await cancelSubscription(subscriptionId, { cancelAtPeriodEnd: true, metadata });
      return res.status(200).send({ message: 'OK' });
    } catch (err) {
      const message = 'There was a problem canceling the subscription.';
      log.error({ err, message });
      return res.status(500).send({ message });
    }
  };

  /**
   *
   * Admin Update Subscription
   *
   * @description This Stripe specific API should be used to update an active or expiring
   *  subscription.
   * @param {string} subscriptionId - Stripe subscription id
   * @param {string} planId - Optional parameter indiciating the new subscription plan and billing cycle.
   * @param {boolean} resume - Optional paramter to enable auto-renewal of an expiring subscription.
   *
   * @returns {res} - Success message
   *
   */
  const updateSubscription = async (req, res) => {
    const {
      body: { subscripionId, planId, resume },
    } = req;

    try {
      const {
        status,
        trial_end: trialEndDate,
        plan: { currency },
      } = await getSubscription(subscripionId);

      if (status === 'canceled') {
        return res.status(400).send({ message: 'Cannot update a canceled subcription.' });
      }

      if (planId && !planId.includes(currency)) {
        return res
          .status(400)
          .send({ message: 'Cannot update the subscription: currency mismatch' });
      }

      const opts = {
        trialEndDate: status === 'trialing' ? trialEndDate : null,
        cancelAtPeriodEnd: resume || null,
      };
      await updateSubscription(subscripionId, planId, opts);
      return res.status(200).send({ message: 'OK' });
    } catch (error) {
      return res.status(500).send({ message: 'There was a problem updating the subscription.' });
    }
  };

  /**
   * Admin Get Subscription
   *
   * @description This User specific API should be used to retrieve subscription details or a single user.
   *
   * @query {string} userId - A valid Surfline/Buoyweather/FishTrack user id.
   * @returns {res} - The UserAccount Doucment
   *
   */
  const getUserAccount = async (req, res) => {
    const {
      query: { userId },
    } = req;
    if (!userId) return res.sendStatus(400);
    try {
      const result = await getAccountVerbose(userId);
      return res.json(result);
    } catch (err) {
      const message = 'The was a problem fetching the userAccount';
      log.error({ err, message });
      return res.status(500).send({ message });
    }
  };

  /**
   * @description This endpoint is for handling GDPR requests from Admin CMS.
   *
   * For a valid user this endpoint deletes the entry from `UserAccounts` Mongo collection.
   * If there is an active subscription then the endpoint won't delete the document.
   */
  const deleteUserAccount = async (req, res) => {
    const { userId } = req.body;
    if (!userId) {
      return res.status(400).send({ message: 'You need to specify an id' });
    }
    try {
      const { subscriptions } = await getAccountVerbose(userId);
      if (subscriptions.length)
        return res.status(400).send({ message: 'Cannot delete. User has active subscription' });
      await UserAccountModel.findOneAndDelete({ user: userId });
      return res.status(200).send({ result: 'success' });
    } catch (error) {
      return res.status(500).send({
        message: 'Could not delete useraccount',
        error,
      });
    }
  };

  api.post(
    '/',
    trackRequest,
    wrapErrors((req, res) => createUserAccount(req, res)),
  );
  api.put(
    '/',
    trackRequest,
    wrapErrors((req, res) => updateSubscription(req, res)),
  );
  api.delete(
    '/',
    trackRequest,
    wrapErrors((req, res) => deleteSubscription(req, res)),
  );
  api.get(
    '/',
    trackRequest,
    wrapErrors((req, res) => getUserAccount(req, res)),
  );
  api.delete(
    '/account',
    trackRequest,
    wrapErrors((req, res) => deleteUserAccount(req, res)),
  );
  api.use('/iap', iap());

  return api;
};
