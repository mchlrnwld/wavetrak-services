import { Router } from 'express';
import * as bodyParser from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import logger from '../../../common/logger';
import { getAccount, getAccountVerbose } from '../../../models/v1/accounts';
import { updateUserAccountReceipt, getPurchaseData } from '../../../models/v1/iap';
import { getBrand } from '../../../models/v1/iap/utils';

const jsonBodyParser = bodyParser.json();

/**
 * @description An async function used to wrap the for...of loop so that we can
 * synchronously run the updateUserAccountReceipt function for each receipt
 * in the user object
 * @param {array} receipts
 * @param {string} userId
 */
const processReceipts = async (receipts, userId) => {
  for (const receipt of receipts) {
    const brandForGoogle = getBrand(receipt);

    await updateUserAccountReceipt(receipt.type, receipt.receiptData, userId, brandForGoogle);
  }
};

export default () => {
  const api = Router();
  const log = logger('admin/subscriptions/iap');

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/subscriptions',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  /**
   * @description Endpoint for validating a users receipt and syncing them with our backend.
   * This endpoint will be used to restore a user's receipts in case they have purchased a
   * subscriptions but not received their entitlements.
   * @param {object} req request object
   * @param {object} req.params request parameters
   * @param {string} req.params.userId User ID to perform operations for
   * @param {*} res response object
   */
  const updateReceipts = async (req, res) => {
    const { userId } = req.params;
    const userAccount = await getAccount(userId);

    if (!userAccount.receipts || userAccount.receipts.length === 0) {
      return res.status(400).json({
        message: 'No receipts found',
      });
    }

    try {
      await processReceipts(userAccount.receipts, userId);
      const updatedUserAccount = await getAccountVerbose(userId);

      return res.status(200).json(updatedUserAccount);
    } catch (err) {
      const message = 'There was a problem verifying the receipt';
      log.error({ err, message });
      return res.status(500).send({ message });
    }
  };

  /**
   * @description Endpoint for getting information about an Apple or Google receipt.
   * As of now this endpoint will return the 3 latest in app purchases for a receipt.
   * From here the user on the admin side can decide to restore the receipt to give
   * the user the entitlments they have paid for but not yet received.
   * @param {object} req request object
   * @param {object} req.body request body
   * @param {string} req.body._id User ID to fetch receipts for
   * @param {string} req.body.deviceType Device type of the receipt (apple/google/etc...)
   * @param {string} req.body.brand Brand the purchase was made for (Only needed for google)
   * @param {string} req.body.receiptData Receipt data to use for pulling information
   * @param {*} res response object
   */
  const verifyReceipt = async (req, res) => {
    const { userId, deviceType, brand, receiptData } = req.body;
    const userAccount = await getAccount(userId);

    if (!userAccount.receipts) {
      return res.status(400).json({
        message: 'No receipt found',
      });
    }

    try {
      const verifiedReceiptData = await getPurchaseData(deviceType, receiptData, brand, log);

      // Get the latest 3 purchases from the in_app array
      const { purchases } = verifiedReceiptData;
      const type = verifiedReceiptData.receipt.receipt_type || verifiedReceiptData.receipt.service;
      let creationDate;
      if (deviceType === 'apple') {
        creationDate = parseInt(verifiedReceiptData.receipt.receipt_creation_date_ms, 10);
      } else {
        creationDate = verifiedReceiptData.receipt.purchaseTime;
      }

      return res.status(200).send({
        purchases,
        type,
        creationDate,
        receipt: verifiedReceiptData.receipt,
      });
    } catch (err) {
      const message = 'There was a problem getting the receipt';
      log.error({ err, message });
      return res.status(500).send({ message });
    }
  };

  api.use('*', jsonBodyParser);
  api.put(
    '/update-receipts/:userId',
    trackRequest,
    wrapErrors((req, res) => updateReceipts(req, res)),
  );
  api.post(
    '/verify-receipt',
    trackRequest,
    wrapErrors((req, res) => verifyReceipt(req, res)),
  );
  return api;
};
