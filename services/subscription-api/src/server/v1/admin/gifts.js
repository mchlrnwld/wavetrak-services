import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import logger from '../../../common/logger';
import {
  getGiftProducts,
  createGiftProduct,
  updateGiftProduct,
  getGiftProduct,
  getGiftProductByParams,
} from '../../../models/v1/gifts/giftProducts';
import { createInternalRedeemCode } from '../../../models/gifts/redeem';

export default () => {
  const api = Router();
  const log = logger('admin/gifts');

  const trackRequest = (req, res, next) => {
    const { headers, params, query, body } = req;
    log.trace({
      action: '/admin/gifts',
      request: {
        headers,
        params,
        query,
        body,
      },
    });
    next();
  };

  const getGifts = async (req, res) => {
    try {
      const giftProducts = await getGiftProducts();
      return res.send({ giftProducts });
    } catch (error) {
      log.error({ error, message: error.message });
      return res.status(400).send({ message: error.message });
    }
  };

  const createGift = async (req, res) => {
    const { body: giftParams } = req;
    if (!giftParams) {
      return res.status(400).send({ message: 'You must specify the required params' });
    }
    try {
      const giftProduct = await createGiftProduct(giftParams);
      return res.send({ giftProduct });
    } catch (error) {
      log.error({ error, message: error.message });
      return res.status(400).send({ message: error.message });
    }
  };

  const updateGift = async (req, res) => {
    const { body: giftParams } = req;
    if (!giftParams) {
      return res.status(400).send({ message: 'You must specify the required params' });
    }
    try {
      const giftProduct = await updateGiftProduct(giftParams.id, giftParams);
      return res.send({ giftProduct });
    } catch (error) {
      log.error({ error, message: error.message });
      return res.status(400).send({ message: error.message });
    }
  };

  const getGift = async (req, res) => {
    const { id } = req.params;
    if (!id) {
      return res.send(400).send({ message: 'You must supply a valid product id' });
    }
    try {
      const giftProduct = await getGiftProduct(id);
      return res.send({ giftProduct });
    } catch (error) {
      log.error({ error, message: error.message });
      return res.status(400).send({ message: error.message });
    }
  };

  const generateRedeemCodes = async (req, res) => {
    const email = req.headers['x-auth-employeeemail'];
    const { body } = req;
    const { department, campaign, prefix, total, brand, product: duration, sponsor } = body;

    const { _id: productId, name } = await getGiftProductByParams({ brand, duration });
    if (!email || !productId || !department || !campaign || !prefix) {
      return res.status(400).send({ message: 'Unable to generate codes: Invalid Parameters' });
    }

    if (total && parseInt(total, 10) > 1000) {
      return res.status(400).send({ message: 'Unable to generate code: Contact Engineering' });
    }

    try {
      const generatedCodes = [];
      let codesGenerated = 0;
      while (codesGenerated < total) {
        const redeemCodeDoc = await createInternalRedeemCode({
          department,
          purchaserEmail: sponsor,
          productId,
          prefix,
          brand,
        });
        generatedCodes.push({
          brand,
          department: redeemCodeDoc.department,
          campaign,
          sponsor: redeemCodeDoc.purchaserEmail,
          product: name,
          redeemCode: redeemCodeDoc.redeemCode,
          processedBy: email,
          processedOn: redeemCodeDoc.createdAt,
          id: redeemCodeDoc._id,
        });
        codesGenerated += 1;
      }
      return res.json(generatedCodes);
    } catch (error) {
      log.error({ error, message: error });
      return res.status(500).send({ message: error.message });
    }
  };

  api.get(
    '/',
    trackRequest,
    wrapErrors((req, res) => getGifts(req, res)),
  );
  api.get(
    '/:id',
    trackRequest,
    wrapErrors((req, res) => getGift(req, res)),
  );
  api.post(
    '/',
    trackRequest,
    wrapErrors((req, res) => createGift(req, res)),
  );
  api.put(
    '/',
    trackRequest,
    wrapErrors((req, res) => updateGift(req, res)),
  );
  api.post(
    '/generate-codes',
    trackRequest,
    wrapErrors((req, res) => generateRedeemCodes(req, res)),
  );
  return api;
};
