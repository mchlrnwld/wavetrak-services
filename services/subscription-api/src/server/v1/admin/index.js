import { Router } from 'express';
import * as bodyParser from 'body-parser';
import subscriptions from './subscriptions';
import customers from './customers';
import gifts from './gifts';

const jsonBodyParser = bodyParser.json();

export default () => {
  const api = Router();

  api.use('*', jsonBodyParser);
  api.use('/subscriptions', subscriptions());
  api.use('/customers', customers());
  api.use('/gifts', gifts());

  return api;
};
