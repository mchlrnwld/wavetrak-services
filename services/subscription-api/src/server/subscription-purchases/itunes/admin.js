import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import * as bodyParser from 'body-parser';
import trackRequest from '../../middleware/trackRequest';
import {
  getApplePurchasesByUser,
  getAppleReceiptByPurchaseId,
  processAppleReceipt,
} from '../../../models/subscription-purchases/itunes';

export default () => {
  const api = Router();
  const jsonBodyParser = bodyParser.json();

  /**
   * @description - Admin API for retrieving all itunes purchases for a single customer.
   *
   * @query {String} userId - The wavetrak user id.
   * @returns {purchases}  - An array of v2 subscription-purchase objects
   */
  const getPurchases = async (req, res) => {
    const {
      query: { userId },
    } = req;
    try {
      const purchases = await getApplePurchasesByUser(userId);

      return res.send({ purchases });
    } catch (error) {
      const { message } = error;
      return res.status(500).send({
        message: message || 'There was an issue finding the purchases for this customer ',
      });
    }
  };

  /**
   * @description - Admin API for updating a singleitunes purchases against the itunes receipt API.
   *
   * @query {String} purchaseId - The purchase id.
   * @returns {purchase}  - The idempotentenly updated subscription purchase
   */
  const updatePurchase = async (req, res) => {
    const {
      body: { purchaseId },
    } = req;
    try {
      const { user, latestReceipt } = await getAppleReceiptByPurchaseId(purchaseId);
      const purchase = await processAppleReceipt(user, latestReceipt);

      return res.send({ purchase });
    } catch (error) {
      const { message } = error;
      return res.status(500).send({
        message: message || 'There was an issue updating the purchase for this customer ',
      });
    }
  };

  api.use('*', jsonBodyParser);
  api.get(
    '/',
    trackRequest,
    wrapErrors((req, res) => getPurchases(req, res)),
  );
  api.post(
    '/',
    trackRequest,
    wrapErrors((req, res) => updatePurchase(req, res)),
  );
  return api;
};
