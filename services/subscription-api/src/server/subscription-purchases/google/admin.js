import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import * as bodyParser from 'body-parser';
import trackRequest from '../../middleware/trackRequest';
import {
  getGooglePurchasesByUser,
  getGooglePurchaseByPurchaseId,
  processGooglePurchase,
  modifyGooglePurchase,
} from '../../../models/subscription-purchases/google';
import { updateGoogleSubscription } from '../../../models/subscriptions/google';
import { getSubscriptionDataFromGooglePurchase } from '../../../utils/google';

export default () => {
  const api = Router();
  const jsonBodyParser = bodyParser.json();

  /**
   * @description - Admin API for retrieving the most recent google purchase for a single customer.
   *
   * @query {String} userId - The wavetrak user id.
   * @returns {Array} purchases  - An array of v2 subscription-purchase objects
   */
  const getPurchases = async (req, res) => {
    const {
      query: { userId },
    } = req;
    try {
      const purchases = await getGooglePurchasesByUser(userId);

      return res.send({ purchases });
    } catch (error) {
      return res
        .status(500)
        .send({ message: 'There was an issue finding the purchases for this customer ' });
    }
  };

  /**
   * @description - Admin API for updating a singleitunes purchases against the google purchase API.
   *
   * @param {String} purchaseId - The purchase id.
   * @returns {Object} purchase  - The idempotentenly updated subscription purchase
   *
   * https://developers.google.com/android-publisher/api-ref/rest/v3/purchases.subscriptions/get
   */
  const updatePurchase = async (req, res) => {
    const {
      body: { purchaseId },
    } = req;
    try {
      const { user, packageName, productId, purchaseToken } = await getGooglePurchaseByPurchaseId(
        purchaseId,
      );
      const purchase = await processGooglePurchase(user, {
        data: { packageName, productId, purchaseToken },
      });
      const subscriptionData = getSubscriptionDataFromGooglePurchase(purchase);
      await updateGoogleSubscription(subscriptionData);

      return res.send({ purchase });
    } catch (error) {
      const { message } = error;
      return res.status(500).send({
        message: message || 'There was an issue updating the purchase for this customer ',
      });
    }
  };

  /**
   * @description - Cancels, Refunds, or Revokes a user's subscription purchase..
   *
   * @param {String} purchaseId - The purchase id.
   * @param {String} command - The type of modifcation.  Allowable values are; "cancel", "refund", or "revoke".
   * @returns {Object} purchase  - The idempotentenly updated subscription purchase
   *
   * https://developers.google.com/android-publisher/api-ref/rest/v3/purchases.subscriptions#methods
   */
  const cancelPurchase = async (req, res) => {
    const {
      body: { purchaseId, command },
    } = req;

    try {
      const { user, packageName, productId, purchaseToken } = await getGooglePurchaseByPurchaseId(
        purchaseId,
      );
      const purchase = await modifyGooglePurchase(user, {
        data: { packageName, productId, purchaseToken, command },
      });

      return res.send({ purchase });
    } catch (error) {
      const { message } = error;
      return res.status(500).send({
        message: message || 'There was an issue canceling the purchase for this customer ',
      });
    }
  };

  /**
   * @description - Defers a user's subscription purchase until a specified future expiration time.
   *
   * @param {String} purchaseId - The purchase id.
   * @param {Number} deferMillis - The the numner of milliseconds to defer the renewal.
   * @returns {object} purchase  - The idempotentenly updated subscription purchase
   *
   * https://developers.google.com/android-publisher/api-ref/rest/v3/purchases.subscriptions/defer
   */
  const deferPurchase = async (req, res) => {
    const {
      body: { purchaseId, deferMillis },
    } = req;

    try {
      const {
        user,
        packageName,
        productId,
        purchaseToken,
        data: { expiryTimeMillis },
      } = await getGooglePurchaseByPurchaseId(purchaseId);

      const deferralInfo = {
        expiryTimeMillis,
        desiredExpiryTimeMillis: expiryTimeMillis + deferMillis,
      };
      const purchase = await modifyGooglePurchase(user, {
        data: { packageName, productId, purchaseToken, command: 'defer', deferralInfo },
      });

      return res.send({ purchase });
    } catch (error) {
      const { message } = error;
      return res.status(500).send({
        message: message || 'There was an issue defering the purchase renewal for this customer ',
      });
    }
  };

  api.use('*', jsonBodyParser);
  api.get(
    '/',
    trackRequest,
    wrapErrors((req, res) => getPurchases(req, res)),
  );
  api.post(
    '/',
    trackRequest,
    wrapErrors((req, res) => updatePurchase(req, res)),
  );
  api.put(
    '/',
    trackRequest,
    wrapErrors((req, res) => cancelPurchase(req, res)),
  );
  api.post(
    '/defer',
    trackRequest,
    wrapErrors((req, res) => deferPurchase(req, res)),
  );
  return api;
};
