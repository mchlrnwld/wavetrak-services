import { authenticateRequest, wrapErrors } from '@surfline/services-common';
import { Router } from 'express';
import trackRequest from '../middleware/trackRequest';
import { getCoupon, getCustomer } from '../../external/stripe';
import { plansForUser } from '../v1/subscriptions/helpers';
import fillStripeCustomerId from '../middleware/fillStripeCustomerId';
import setQueryParams from '../middleware/setQueryParams';

const pricingPlans = () => {
  const getPricingPlans = async (req, res) => {
    const {
      query: { brand, countryISO = 'US', couponcode = null, filter = 'all' },
    } = req;
    const { currency } = req.stripeCustomerId ? await getCustomer(req.stripeCustomerId) : {};
    const coupon = couponcode ? await getCoupon(couponcode) : null;
    let priceFilter = filter;
    if (filter === 'annual') priceFilter = 'year';
    if (filter === 'monthly') priceFilter = 'month';

    const type = filter === 'gift' ? 'one_time' : 'recurring';
    const prices = await plansForUser(coupon, countryISO, currency, type);

    const recurringPrices = prices.filter(
      price => !price.lookupKey?.toLowerCase().includes('gift'),
    );
    const giftPrices = prices.filter(price => price.lookupKey?.toLowerCase().includes('gift'));

    let filteredPrices;

    if (filter === 'gift') {
      filteredPrices = giftPrices
        .filter(price => {
          return price.lookupKey?.toLowerCase().includes(`${brand}_`);
        })
        .map(price => ({
          ...price,
          brand,
        }));
    } else {
      filteredPrices = recurringPrices
        .filter(price => {
          const lookupKey = price.lookupKey?.toLowerCase();
          if (filter !== 'all') {
            return lookupKey?.includes(`${brand}_`) && lookupKey?.includes(`${priceFilter}`);
          }
          return lookupKey?.includes(`${brand}_`);
        })
        .map(price => ({
          ...price,
          brand,
          // id: undefined, TODO: uncomment when Stripe Checkout is used for Promotions
        }));
    }

    return res.status(200).send({ plans: filteredPrices, prices: filteredPrices });
  };

  const api = Router();
  api.get(
    '/',
    trackRequest,
    authenticateRequest({ userIdRequired: false }),
    fillStripeCustomerId,
    wrapErrors((req, res) => getPricingPlans(req, res)),
  );
  api.get(
    '/gifts',
    trackRequest,
    authenticateRequest({ userIdRequired: false }),
    fillStripeCustomerId,
    setQueryParams({ filter: 'gift' }),
    wrapErrors((req, res) => getPricingPlans(req, res)),
  );
  return api;
};

export default pricingPlans;
