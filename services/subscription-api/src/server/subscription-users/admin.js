import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import * as bodyParser from 'body-parser';
import trackRequest from '../middleware/trackRequest';
import {
  deleteSubscriptionUserByUserId,
  getSubscriptionUserByUserId,
  getSubscriptionUsersByPrimaryUser,
  updateSubscriptionUsersByPrimaryUser,
  updateSubscriptionUsersByUser,
} from '../../models/subscription-users';
import {
  getSubscriptionPurchasesByUser,
  updateSubscriptionPurchasesByUser,
} from '../../models/subscription-purchases';
import {
  getActiveSubscriptionsByUserId,
  updateSubscriptionByUser,
} from '../../models/subscriptions';
import logger from '../../common/logger';
import { identifySubscription } from '../../tracking';

export default () => {
  const api = Router();
  const jsonBodyParser = bodyParser.json();
  const log = logger('subscription-service');
  /**
   * @description - Admin API for retrieving all SubscriptionUsers assosciated to a user
   * @returns {subscriptionUsers}  - An array of v2 subscriptionUser objects
   */
  const getSubscriptionUsers = async (req, res) => {
    try {
      const { userId } = req.params;
      const result = await getSubscriptionUserByUserId(userId);
      if (!result)
        return res
          .status(404)
          .send({ message: 'SubscriptionUsers does not exist for this userId' });
      const { primaryUser } = result;
      const subscriptionUsers = await getSubscriptionUsersByPrimaryUser(primaryUser);
      return res.send({ subscriptionUsers });
    } catch (error) {
      return res
        .status(500)
        .send({ message: 'There was an issue finding subscriptionUsers for this userId' });
    }
  };

  /**
   * @description - This Admin API is used for :
   *   1) Updates the primaryUser for all associated SubscriptionUsers documents to the newPrimaryUser
   *   2) Updates the user in Subscription and SubscriptionPurchase with the newPrimaryUser
   *   3) Re-identifies both the users in Segment
   * @param {Object} req.body - The request object
   * @param {string} req.body.oldPrimaryUser - required
   * @param {string} req.body.newPrimaryUser - required
   * @returns {object}  - Success Object on Success and Failure object on failure
   */
  const updateSubscriptionUsers = async (req, res) => {
    try {
      const { oldPrimaryUser, newPrimaryUser } = req.body;
      if (!oldPrimaryUser || !newPrimaryUser)
        return res.status(400).send({ message: 'Required fields are missing' });
      const subscriptionUsers = await getSubscriptionUsersByPrimaryUser(oldPrimaryUser);
      if (!subscriptionUsers.length)
        return res
          .status(404)
          .send({ message: 'SubscriptionUsers does not exist for this primaryUser' });

      await updateSubscriptionUsersByPrimaryUser(oldPrimaryUser, newPrimaryUser);
      await updateSubscriptionUsersByUser(oldPrimaryUser, {
        isPrimaryUser: false,
        isEntitled: false,
      });
      await updateSubscriptionUsersByUser(newPrimaryUser, {
        isPrimaryUser: true,
        isEntitled: true,
      });

      const subscriptionPurchase = await getSubscriptionPurchasesByUser(oldPrimaryUser);
      if (subscriptionPurchase) {
        await updateSubscriptionPurchasesByUser(oldPrimaryUser, {
          user: newPrimaryUser,
        });
      }

      const currentSubscription = (await getActiveSubscriptionsByUserId(oldPrimaryUser))[0];
      if (currentSubscription) {
        await updateSubscriptionByUser(oldPrimaryUser, {
          user: newPrimaryUser,
        });
        // Re Identify oldPrimaryUser with non-premium subscription traits
        identifySubscription({
          ...currentSubscription,
          entitlement: null,
          active: false,
          user: oldPrimaryUser,
        });
        // Re Identify newPrimaryUser with premium subscription traits
        identifySubscription({
          ...currentSubscription,
          user: newPrimaryUser,
        });
      }
      return res.send({ message: 'success' });
    } catch (error) {
      const message = error?.message || 'Something went wrong';
      log.error({
        model: 'updateSubscriptionUsers',
        message,
        error: JSON.stringify(error),
      });
      return res.status(500).send({ message });
    }
  };

  const deleteSubscriptionUser = async (req, res) => {
    const {
      body: { userId },
    } = req;
    try {
      const subUser = await getSubscriptionUserByUserId(userId);
      if (!subUser) {
        return res.status(200).send({ message: 'No subscription user found.' });
      }
      const { isPrimaryUser, active } = subUser;
      if (isPrimaryUser && active) {
        return res.status(400).send({ message: 'Cannot delete primary subscription user.' });
      }
      await deleteSubscriptionUserByUserId(userId);
      return res.status(200).send({ message: 'Successfully deleted subscription user.' });
    } catch (error) {
      log.error(error);
      return res
        .status(500)
        .send({ message: 'Error occurred attempting to delete subscription user.' });
    }
  };

  api.use('*', jsonBodyParser);
  api.get(
    '/:userId',
    trackRequest,
    wrapErrors((req, res) => getSubscriptionUsers(req, res)),
  );
  api.patch(
    '/',
    trackRequest,
    wrapErrors((req, res) => updateSubscriptionUsers(req, res)),
  );
  api.delete(
    '/',
    trackRequest,
    wrapErrors((req, res) => deleteSubscriptionUser(req, res)),
  );
  return api;
};
