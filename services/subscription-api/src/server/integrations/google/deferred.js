import { processGoogleNotification } from '../../../models/subscription-purchases/google';
import { getSubscriptionDataFromGooglePurchase } from '../../../utils/google';
import { updateGoogleSubscription } from '../../../models/subscriptions/google';

/**
 * (1) SUBSCRIPTION_DEFERRED
 * @desc A subscription's recurrence time has been extended.
 *
 * https://developer.android.com/google/play/billing/rtdn-reference
 *
 * Billing can be deferred by as little as one day and by as long as one year per API call. To
 * defer the billing even further, you can call the API again before the new billing date
 * arrives. During the deferral period, the user is subscribed to your content with full access
 * but is not charged. The subscription renewal date is updated to reflect the new date.
 *
 * {
 *   "kind": "androidpublisher#subscriptionPurchase",
 *   ...
 *   "expiryTimeMillis": updated_expiration_time,
 *   "autoRenewing": true,
 *   ...
 *   "paymentState": 0  # Payment pending
 * }
 *
 * @param {object} signature
 *
 */
const deferred = async signature => {
  const googlePurchase = await processGoogleNotification(signature);
  const subscriptionData = getSubscriptionDataFromGooglePurchase(googlePurchase);
  await updateGoogleSubscription(subscriptionData);
  return true;
};

export default deferred;
