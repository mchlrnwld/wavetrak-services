import { processGoogleNotification } from '../../../models/subscription-purchases/google';
import { getSubscriptionDataFromGooglePurchase } from '../../../utils/google';
import { updateGoogleSubscription } from '../../../models/subscriptions/google';

/**
 * (13) SUBSCRIPTION_EXPIRED
 * @desc An active google subscription expired.
 *
 * https://developer.android.com/google/play/billing/subs#expirations
 *
 * A SUBSCRIPTION_EXPIRED notification is also sent when a subscription
 * expires. When you receive this notification, your app should query the
 * Google Play Developer API to get the latest subscription state. The
 * subscription resource looks similar to the following:
 *
 * {
 *   "kind": "androidpublisher#subscriptionPurchase",
 *   ...
 *   "expiryTimeMillis": expiry_time_in_past,
 *   "autoRenewing": false,
 *   ...
 *   "paymentState": 1  # Payment received
 * }
 *
 * @param {object} signature
 *
 */
const expired = async signature => {
  const googlePurchase = await processGoogleNotification(signature);
  const subscriptionData = getSubscriptionDataFromGooglePurchase(googlePurchase);
  await updateGoogleSubscription(subscriptionData);
  return true;
};

export default expired;
