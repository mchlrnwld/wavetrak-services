import { processGoogleNotification } from '../../../models/subscription-purchases/google';
import { trackOrderCompleted } from '../../../tracking/orderCompleted';
/**
 * (4) SUBSCRIPTION_PURCHASED
 * @desc A new subscription has been purchased.
 *
 * https://developer.android.com/google/play/billing/subscriptions#new
 *
 *  {
      "kind": "androidpublisher#subscriptionPurchase",
      ...
      "expiryTimeMillis": next_renewal_date,
      "autoRenewing": true,
      ...
      "paymentState": 1  # Payment received
    }
 *
 * @param {object} signature
 *
 */
const purchased = async signature => {
  const googlePurchase = await processGoogleNotification(signature);
  const userId = googlePurchase.user.toString();
  trackOrderCompleted(googlePurchase, 'google', userId);

  return true;
};

export default purchased;
