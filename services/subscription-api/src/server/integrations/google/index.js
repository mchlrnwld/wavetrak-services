import { Router } from 'express';
import newrelic from 'newrelic';
import bodyparser from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import AWS from 'aws-sdk';
import logger from '../../../common/logger';
import renewed from './renewed';
import canceled from './canceled';
import expired from './expired';
import revoked from './revoked';
import onHold from './on-hold';
import recovered from './recovered';
import deferred from './deferred';
import purchased from './purchased';
import { getGooglePurchaseByPurchaseToken } from '../../../models/subscription-purchases/google';
import getPurchaseData from '../../../external/google';

/**
 * Hash containing key lookups matching a function handler
 * to a Google Developer notification type.
 */
export const mappings = {
  1: recovered,
  2: renewed,
  3: canceled,
  4: purchased, // purchased
  5: onHold, // on hold
  6: () => true, // in grace period
  7: () => true, // restarted
  8: () => true, // price change confirmed
  9: deferred,
  10: () => true, // paused
  11: () => true, // pause schedule changed
  12: revoked,
  13: expired,
};

export default () => {
  const api = Router();
  const log = logger('subscription-integration-google');

  const getNotificationSignature = (req, res, next) => {
    const {
      body: {
        message: { data },
      },
    } = req;
    const notificationJSON = Buffer.from(data, 'base64').toString('utf-8');
    const notification = JSON.parse(notificationJSON);

    // Google follows a different JSON contract for sending Test Notifications through the Play Console.
    const { testNotification } = notification;
    if (testNotification) res.status(200).send('OK');

    const {
      version,
      packageName,
      eventTimeMillis,
      subscriptionNotification: { notificationType, purchaseToken, subscriptionId: productId },
    } = notification;
    req.signature = {
      version,
      packageName,
      eventTimeMillis,
      notificationType,
      purchaseToken,
      productId,
    };
    return next();
  };

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/integrations/google/handler',
      request: {
        header: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  /**
   * Verifies the notification against the Google-Play Store.
   *
   * Note: TODO once Topic and Topic Subscription are published.
   *
   */
  const verifyNotification = async (req, res, next) => {
    const { signature } = req;
    if (!signature) return res.status(200).send('OK');

    const { purchaseToken } = signature;
    const googlePurchaseData = await getPurchaseData(signature);
    const { linkedPurchaseToken } = googlePurchaseData;
    const googleSubscriptionPurchase = await getGooglePurchaseByPurchaseToken(
      purchaseToken,
      linkedPurchaseToken,
    );
    if (!googleSubscriptionPurchase) {
      log.error({
        message: 'Google Notification verification failed',
        ...signature,
      });

      // TODO Determine how to reconcile unacknowledged Purchase Tokens.
      return res.status(200).send('OK');
    }
    return next();
  };

  /**
   * Firehose is a utility API to enable event streaming into AWS S3. This
   * stream should only be enabled for initial data collection, test setup,
   * validation of new events are needed, and API endpoint outages.
   *
   */
  const firehose = async (req, res) => {
    if (!process.env.ENABLE_GOOGLE_PLAY_FIREHOSE) {
      return res.status(200).send('OK');
    }
    const s3 = new AWS.S3({ apiVersion: '2006-03-01' });
    const { body, signature } = req;

    const { packageName, notificationType, eventTimeMillis } = signature;
    const environment = process.env.NODE_ENV === 'production' ? 'prod' : process.env.NODE_ENV;

    const data = {
      Key: `${packageName}-${notificationType}-${eventTimeMillis}`,
      Body: JSON.stringify(body),
      Bucket: `subscription-notifications-${environment}/google-play`,
      CacheControl: 'no-cache',
      ContentType: 'application/json',
    };

    return s3.putObject(data, err => {
      if (err) {
        const { message } = err;
        log.info({
          message: 'Failed to send notificiation data to S3',
          error: message,
        });
        return res.status(500).send({ message });
      }
      return res.status(200).send('OK');
    });
  };

  const eventHandler = async (req, res) => {
    const { signature } = req;
    const handler = mappings[signature.notificationType];

    if (!handler || !signature) {
      log.error({
        ...signature,
        message: 'No handler or event signature found.',
      });
      return res.status(400).send({ message: 'Notification handler not recognized.' });
    }

    newrelic.addCustomAttribute('googleNotificationType', signature.notificationType);

    try {
      const result = await handler(signature);

      if (result) {
        log.info({
          ...signature,
          message: 'Successfuly processed the Google Notification',
        });
        return res.status(200).send('OK');
      }
    } catch (error) {
      newrelic.noticeError(error);
      log.error({
        error: error.stack,
        ...signature,
        message: 'Problem in the the handler processing the Google Notification Event.',
      });
      return res.status(422).send('Could not process the Google Notification request.');
    }
    log.error({
      ...signature,
      message: 'Problem processing the Google Notification Event.',
    });
    return res.status(422).send('Could not process the Google request.');
  };

  api.use('*', bodyparser.json());
  api.post(
    '/firehose',
    trackRequest,
    getNotificationSignature,
    verifyNotification,
    async (req, res) => firehose(req, res),
  );
  api.post(
    '/handler',
    trackRequest,
    getNotificationSignature,
    verifyNotification,
    wrapErrors((req, res) => eventHandler(req, res)),
  );
  return api;
};
