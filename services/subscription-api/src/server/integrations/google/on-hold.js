import { processGoogleNotification } from '../../../models/subscription-purchases/google';
import { getSubscriptionDataFromGooglePurchase } from '../../../utils/google';
import { updateGoogleSubscription } from '../../../models/subscriptions/google';
/**
 * (5) SUBSCRIPTION_ON_HOLD
 * @desc A subscription has entered account hold (if enabled).
 *
 * https://developer.android.com/google/play/billing/subs#account-hold
 *
 * Account hold is a subscription state that begins when a user's form of payment fails and any
 * associated grace period has ended without payment resolution. When a subscription enters into
 * account hold, you should block access to your content or service. The account hold period
 * lasts for up to 30 days. During account hold, the expiryTimeMillis of the subscription resource
 * is set to a past timestamp, and paymentState is set to 0:
 *
 * {
 *   "kind": "androidpublisher#subscriptionPurchase",
 *   ...
 *   "expiryTimeMillis": timestamp_in_past,
 *   "autoRenewing": true,
 *   ...
 *   "paymentState": 0  # Payment pending
 * }
 *
 * @param {object} signature
 *
 */
const onHold = async signature => {
  const googlePurchase = await processGoogleNotification(signature);
  const subscriptionData = getSubscriptionDataFromGooglePurchase(googlePurchase);
  await updateGoogleSubscription(subscriptionData);
  return true;
};

export default onHold;
