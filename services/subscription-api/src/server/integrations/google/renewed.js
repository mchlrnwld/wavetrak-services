import { processGoogleNotification } from '../../../models/subscription-purchases/google';
import { getSubscriptionDataFromGooglePurchase } from '../../../utils/google';
import { updateGoogleSubscription } from '../../../models/subscriptions/google';

/**
 * (4) SUBSCRIPTION_RENEWED
 * @desc An active google subscription was renewed.
 *
 * https://developer.android.com/google/play/billing/subs#renewals
 *
 * A SUBSCRIPTION_RENEWED notification is also sent when a subscription renews.
 * Your app should make sure the user is still entitled to the subscription and
 * then update the subscription state with the new expiryTimeMillis provided in
 * the subscription resource returned from the Google Play Developer API. The
 * subscription resource looks similar to the following:
 *
 * {
 *   "kind": "androidpublisher#subscriptionPurchase",
 *   ...
 *   "expiryTimeMillis": updated_expiration_time,
 *   "autoRenewing": true,
 *   ...
 *   "paymentState": 1  # Payment received
 * }
 *
 * @param {object} signature
 *
 */
const renewed = async signature => {
  const googlePurchase = await processGoogleNotification(signature);
  const subscriptionData = getSubscriptionDataFromGooglePurchase(googlePurchase);
  await updateGoogleSubscription(subscriptionData);
  return true;
};

export default renewed;
