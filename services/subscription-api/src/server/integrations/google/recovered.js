import { processGoogleNotification } from '../../../models/subscription-purchases/google';
import { getSubscriptionDataFromGooglePurchase } from '../../../utils/google';
import { updateGoogleSubscription } from '../../../models/subscriptions/google';

/**
 * (1) SUBSCRIPTION_RECOVERED
 * @desc A subscription was recovered from account hold.
 *
 * https://developer.android.com/google/play/billing/subs#account-hold
 *
 * If your app synchronizes subscription state with a backend, your app
 * should listen for the SUBSCRIPTION_RECOVERED notification to be notified
 * when a subscription is recovered and the user should regain access. If
 * you query for a subscription after receiving this notification, the expiryTimeMillis
 * is set to a timestamp in the future, and paymentState is 1:
 *
 * {
 *   "kind": "androidpublisher#subscriptionPurchase",
 *   ...
 *   "expiryTimeMillis": timestamp_in_future,
 *   "autoRenewing": true,
 *   ...
 *   "paymentState": 1  # Payment recieved
 * }
 *
 * @param {object} signature
 *
 */
const recovered = async signature => {
  const googlePurchase = await processGoogleNotification(signature);
  const subscriptionData = getSubscriptionDataFromGooglePurchase(googlePurchase);
  await updateGoogleSubscription(subscriptionData);
  return true;
};

export default recovered;
