import { processGoogleNotification } from '../../../models/subscription-purchases/google';
import { getSubscriptionDataFromGooglePurchase } from '../../../utils/google';
import { updateGoogleSubscription } from '../../../models/subscriptions/google';

/**
 * (12) SUBSCRIPTION_REVOKED
 * @desc An active google subscription revoked.
 *
 * https://developer.android.com/google/play/billing/subs#revoke
 *
 * A subscription can be revoked from a user for a variety of reasons, including
 * your app revoking the subscription by using Purchases.subscriptions:revoke or
 * the purchase being charged back. In this situation, your app should revoke
 * entitlement from the user immediately. A revoked subscription is no longer
 * returned from BillingClient.queryPurchases(). A SUBSCRIPTION_REVOKED notification
 * is also sent when this occurs. When you receive this notification, the subscription
 * resource returned from the Google Play Developer API contains autoRenewing = false,
 * and expiryTimeMillis contains the date when the user should lose access to the
 * subscription. The subscription resource looks similar to the following:
 *
 * {
 *   "kind": "androidpublisher#subscriptionPurchase",
 *   ...
 *   "expiryTimeMillis": expiry_time_in_past,
 *   "autoRenewing": false,
 *   ...
 *   "paymentState": 1  # Payment received
 * }
 *
 * @param {object} signature
 *
 */
const revoked = async signature => {
  const googlePurchase = await processGoogleNotification(signature);
  const subscriptionData = getSubscriptionDataFromGooglePurchase(googlePurchase);
  await updateGoogleSubscription(subscriptionData);
  return true;
};

export default revoked;
