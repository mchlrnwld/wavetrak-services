import { processGoogleNotification } from '../../../models/subscription-purchases/google';
import { getSubscriptionDataFromGooglePurchase } from '../../../utils/google';
import { updateGoogleSubscription } from '../../../models/subscriptions/google';

/**
 * (3) SUBSCRIPTION_CANCELED
 * @desc An active google subscription was canceled.
 *
 * https://developer.android.com/google/play/billing/subs#cancel
 *
 * Cancelling a subscription triggers a SUBSCRIPTION_CANCELLED notification.
 * When you receive this notification, the subscription resource returned from
 * the Google Play Developer API contains autoRenewing = false, and the
 * expiryTimeMillis contains the date when the user should lose access to the
 * subscription. If expiryTimeMillis is in the past, then the user loses entitlement
 * immediately. Otherwise, the user should retain entitlement until it is expired.
 * The subscription resource looks similar to the following:
 *
 * {
 *   "kind": "androidpublisher#subscriptionPurchase",
 *   ...
 *   "expiryTimeMillis": expiry_time,
 *   "autoRenewing": false,
 *   ...
 *   "paymentState": 1  # Payment received
 *   "cancelReason": integer # Reason the subscription was cancelled: user, billing issue, etc.
 * }
 *
 * @param {object} signature
 *
 */
const canceled = async signature => {
  const googlePurchase = await processGoogleNotification(signature);
  const subscriptionData = getSubscriptionDataFromGooglePurchase(googlePurchase);
  await updateGoogleSubscription(subscriptionData);
  return true;
};

export default canceled;
