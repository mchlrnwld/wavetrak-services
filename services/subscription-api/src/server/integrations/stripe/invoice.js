import logger from '../../../common/logger';
import {
  getStripeSubscription,
  updateStripeSubscription,
} from '../../../models/subscriptions/stripe';
import {
  trackPaymentFailed,
  trackPaymentSucceeded,
  trackDaysToConvertFromTrial,
} from '../../../tracking';

const log = logger('subscription-integration-stripe');

/**
 * @description This hook may be useful if we need to compare things like
 * invoices paid vs invoices created.
 *
 * Occurs whenever a new invoice is created. If you are using webhooks, Stripe
 * will wait one hour after they have all succeeded to attempt to pay the
 * invoice; the only exception here is on the first invoice, which gets created
 * and paid immediately when you subscribe a customer to a plan. If your
 * webhooks do not all respond successfully, Stripe will continue* retrying
 * the webhooks every* hour and will not attempt to pay the invoice. After 3
 * days, Stripe will attempt to pay the invoice regardless of whether or not
 * your webhooks have succeeded. See how to respond to a webhook.
 *
 * Primairly used for listening to when users signup for subscriptions with
 * a discount code.
 *
 * `invoice.created fires whenever a new invoice is created.
 *
 * Since most coupons in Stripe are applied "ONCE", the discount
 * object is only applied to the first invoice created for that subscription.
 * NOTE: Coupons of this nature are not added to the Subscription discount object.
 *
 * @param {object} evt - https://stripe.com/docs/api/invoices/object
 *
 * @typedef {object} evt
 * @property {string} subscription - The id of the related Stripe Subscription
 * @property {string} customer - The id of the related Stripe Customer
 * @property {object|null} discount - The discount hash applied to the invoice
 *
 * @returns {boolean}
 */
export const created = async evt => {
  const {
    subscription: subscriptionId,
    customer: stripeCustomerId,
    lines: { data: invoiceItems },
  } = evt;

  // ignore invoice events for non-recurring payments
  const isSubscriptionInvoice = invoiceItems.some(({ type }) => type === 'subscription');
  if (!isSubscriptionInvoice) {
    return true;
  }

  const subscription = await getStripeSubscription(subscriptionId);
  if (!subscription) {
    log.warn({
      message: `Subscription (${subscriptionId}) not found.`,
      stripeCustomerId,
      subscriptionId,
      stripeEventType: 'invoice.created',
      webhookHandler: 'invoice:created',
    });
    return false;
  }
  return true;
};

/**
 * @description Primarily used to triggers sucbscription billing related segment events.
 *
 * `invoice.upcoming` Occurs X number of days before a subscription
 * is scheduled to create an invoice that is automatically charged—where
 * X is determined by your subscriptions settings.
 * Note: The received Invoice object will not have an invoice ID.
 *
 * @param {object} evt - https://stripe.com/docs/api/invoices/object
 *
 * @typedef {object} evt
 * @property {string} subscription - The id of the related Stripe Subscription
 * @property {string} customer - The id of the related Stripe Customer
 *
 * @returns {boolean}
 */
export const upcoming = async evt => {
  const {
    subscription: subscriptionId,
    customer: stripeCustomerId,
    lines: { data: invoiceItems },
  } = evt;

  // ignore invoice events for non-recurring payments
  const isSubscriptionInvoice = invoiceItems.some(({ type }) => type === 'subscription');
  if (!isSubscriptionInvoice) {
    return true;
  }

  const subscription = await getStripeSubscription(subscriptionId);
  if (!subscription) {
    log.warn({
      message: `Subscription (${subscriptionId}) not found.`,
      stripeCustomerId,
      subscriptionId,
      stripeEventType: 'invoice.upcoming',
      webhookHandler: 'invoice:upcoming',
    });
    return false;
  }

  const { trialing, planId, user, entitlement } = subscription;
  if (trialing) {
    trackDaysToConvertFromTrial({ user, daysToPremium: 7, platform: 'web', planId, entitlement });
  }

  return true;
};

/**
 * @description Updates `Subscriptions` if necessary.
 *
 * 'invoice.payment_failed' fires whenever an invoice payment fails.
 * This can occur due to a declined payment or because the customer has no active card.
 * A particular case of note is that if a customer with no active
 * card reaches the end of its free trial, an invoice.payment_failed notification will occur.
 *
 * @param {object} evt - https://stripe.com/docs/api/invoices/object
 *
 * @typedef {object} evt
 * @property {string} subscription - The id of the related Stripe Subscription
 * @property {string} customer - The id of the related Stripe Customer
 *
 * @returns {boolean}
 */
export const paymentFailed = async evt => {
  const {
    subscription: subscriptionId,
    customer: stripeCustomerId,
    lines: { data: invoiceItems },
  } = evt;

  // ignore invoice events for non-recurring payments
  const isSubscriptionInvoice = invoiceItems.some(({ type }) => type === 'subscription');
  if (!isSubscriptionInvoice) {
    return true;
  }
  const subscription = await getStripeSubscription(subscriptionId);
  if (!subscription) {
    log.warn({
      message: `Subscription (${subscriptionId}) not found.`,
      stripeCustomerId,
      subscriptionId,
      stripeEventType: 'invoice.payment_failed',
      webhookHandler: 'invoice:paymentFailed',
    });
    return false;
  }
  const { failedPaymentAttempts } = subscription;

  const updatedSubscription = await updateStripeSubscription({
    ...subscription,
    failedPaymentAttempts: failedPaymentAttempts + 1,
    expiring: true,
    paymentAlertType: 'failedPayment',
    paymentAlertStatus: 'open',
    alertPayment: true,
  });

  trackPaymentFailed(updatedSubscription);
  return true;
};

/**
 * @description Updates `Subscriptions` if necessary.
 *
 * Occurs whenever an invoice attempts to be paid, and the payment succeeds.
 *
 * Event Attributes used
 *  - subscription
 *  - customer
 *  - period.start
 *  - period.end
 *
 * @param {object} evt - https://stripe.com/docs/api/invoices/object
 *
 * @typedef {object} evt
 * @property {string} subscription - The id of the related Stripe Subscription
 * @property {string} customer - The id of the related Stripe Customer
 * @property {object} lines - The related invoice line item data (https://stripe.com/docs/api/invoices/line_item)
 *
 * @returns {boolean}
 */
export const paymentSucceeded = async evt => {
  const {
    subscription: subscriptionId,
    customer: stripeCustomerId,
    amount_paid: revenue,
    currency,
    lines: { data },
  } = evt;

  const invoiceItem = data.find(({ type }) => type === 'subscription');
  // // ignore invoice events for non-recurring payments
  if (!invoiceItem) {
    return true;
  }

  const {
    period: { end: nextEnd, start: nextStart },
    subscription: subscriptionForInvoice,
  } = invoiceItem;
  /**
   * Prevent failed webhook events for $0 invoices of expired subscriptions
   * For example: https://dashboard.stripe.com/events/evt_1HPDFLJ4F5N75cpX4Xj88VUr
   */
  if (!subscriptionForInvoice) {
    return true;
  }

  const subscription = await getStripeSubscription(subscriptionId);
  if (!subscription) {
    log.warn({
      message: `Subscription (${subscriptionId}) not found.`,
      stripeCustomerId,
      revenue,
      currency,
      subscriptionId,
      stripeEventType: 'invoice.payment_succeeded',
      webhookHandler: 'invoice:paymentSucceeded',
    });
    return false;
  }

  const { renewalCount, paymentAlertType } = subscription;

  const paymentAlertStatus = paymentAlertType === 'failedPayment' ? 'resolved' : null;

  const updatedSubscription = await updateStripeSubscription({
    ...subscription,
    renewalCount: renewalCount + 1,
    failedPaymentAttempts: 0,
    expiring: false,
    paymentAlertStatus,
    start: nextStart,
    end: nextEnd,
    alertPayment: false,
  });

  trackPaymentSucceeded({ ...updatedSubscription, revenue, currency });

  return true;
};
