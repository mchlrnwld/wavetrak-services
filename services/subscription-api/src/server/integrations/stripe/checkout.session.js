import logger from '../../../common/logger';
import { getStripeSubscription } from '../../../models/subscriptions/stripe';
import { trackCheckoutSessionCompleted, trackCheckoutSessionExpired } from '../../../tracking';

const log = logger('subscription-integration-stripe');

/**
 * @description This function handles the `checkout.session.completed` Stripe Event.
 *
 * This function is called when a Stripe checkout session is completed.
 * It will track the checkout session in Segment.
 *
 *
 * @param {object} evt - https://stripe.com/docs/api/subscriptions/object
 *
 * @typedef {object} evt
 * @property {string} subscription - The id of the related Stripe Subscription
 * @property {string} customer - The id of the related Stripe Customer
 *
 * @returns {boolean}
 */
export const completed = async evt => {
  const { subscription: subscriptionId, customer: stripeCustomerId } = evt;
  const subscription = await getStripeSubscription(subscriptionId);
  if (!subscription) {
    log.warn({
      message: `Subscription (${subscriptionId}) not found.`,
      stripeCustomerId,
      subscriptionId,
      stripeEventType: 'checkout.session.completed',
      webhookHandler: 'checkout.session.completed',
    });
    return false;
  }

  trackCheckoutSessionCompleted(subscription, evt);
  return true;
};

/**
 * @description This function handles the `checkout.session.expired` Stripe Event.
 *
 * This function is called when a Stripe checkout session gets expired.
 * It will track the checkout session in Segment.
 *
 *
 * @param {object} evt - https://stripe.com/docs/api/subscriptions/object
 *
 * @typedef {object} evt
 *
 * @returns {boolean}
 */
export const expired = evt => {
  trackCheckoutSessionExpired(evt);
  return true;
};
