import logger from '../../../common/logger';
import { updateStripeCustomer } from '../../../models/customers/stripe';

const log = logger('subscription-integration-stripe');

/**
 * @description This function handles the `customer.created` Stripe Event.
 *
 * When a new Stripe Customer is created, the respective Stripe identifier needs
 * to be added to the UserInfo document.
 *
 * @param {object} evt - https://stripe.com/docs/api/customers/object
 *
 * @typedef {object} evt
 * @property {string} id - The unique identifier for the Stripe Customer
 * @property {object} metadata - A hash containing the Mongo UserInfo document id.
 *
 * @returns {boolean}
 */
export const created = async evt => {
  const {
    id: stripeCustomerId,
    metadata: { userId },
  } = evt;
  try {
    await updateStripeCustomer(userId, stripeCustomerId);
    return true;
  } catch (error) {
    log.warn({
      message: `User ${userId} could not be updated`,
      stripeCustomerId,
      userId,
      stripeEventType: 'customer.created',
      webhookHandler: 'customer:created',
    });
    return false;
  }
};

/**
 * @description Placeholder for future needs
 */
export const deleted = async () => true;
