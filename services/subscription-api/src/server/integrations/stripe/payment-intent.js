import logger from '../../../common/logger';
import { createPurchasedRedeemCode, getRedeemCodeByParams } from '../../../models/gifts/redeem';
import { replaceHtml } from '../../../utils/replaceHtml';
import { getPrice } from '../../../external/stripe';

const log = logger('subscription-integration-stripe');
/**
 * @description This function handles the `payment_intent.succeeded` Stripe Event.
 *
 * This function's responsibility is to create Gift Redeem Codes
 * for customers who have purchases a Gift product through Checkout in Payment Mode
 *
 * @param {object} evt - https://stripe.com/docs/api/subscriptions/object
 *
 * @typedef {object} evt
 * @property {string} id - The unique identifier for the Payment Intent
 * @property {pbject} metadata - Key-value pairs for storing additional information about the payment
 * @property {integer} amount - A positive integer representing how much to charge in the smallest currency unit
 * @property {string} currency - Three-letter ISO currency code, in lowercase.
 * @property {string} invoice - ID of the invoice that created this PaymentIntent, if it exists.
 * @returns {boolean}
 */
export const succeeded = async evt => {
  const { id: paymentIntentId, metadata, amount, currency, invoice } = evt;
  if (invoice) return true;

  const { type } = metadata;
  if (type !== 'gift_purchase') return true;

  const {
    giftMessage: message,
    purchaserEmail,
    purchaserName,
    recipientEmail,
    recipientName,
    priceId,
    userId,
    brand,
  } = metadata;
  try {
    const {
      metadata: { description },
    } = await getPrice(priceId);

    await createPurchasedRedeemCode({
      amount,
      brand,
      currency,
      description,
      type: 'prices',
      message: replaceHtml(message),
      paymentIntentId,
      priceId,
      purchaserEmail,
      purchaserName: replaceHtml(purchaserName),
      recipientEmail,
      recipientName: replaceHtml(recipientName),
      userId,
    });

    return true;
  } catch (error) {
    log.error({
      error,
      message: 'Error handling payment processed for purchased gift code',
      paymentIntentId,
      priceId,
      stripeEventType: 'payment_intent.succeeded',
      webhookHandler: 'payment_intent.succeeded',
    });
    return false;
  }
};

export const paymentFailed = async evt => {
  const {
    id: paymentIntentId,
    metadata: { priceId, type },
    status,
  } = evt;
  if (type === 'gift_purchase') {
    const redeemCode = await getRedeemCodeByParams({ paymentIntentId });
    if (!redeemCode) {
      const {
        metadata: { description },
      } = await getPrice(priceId);
      log.error({
        message: 'Gift purchase payment has failed.',
        paymentIntentId,
        priceId,
        description,
        status,
        stripeEventType: 'payment_intent.payment_failed',
        webhookHandler: 'payment_intent.payment_failed',
      });
      return false;
    }
  }
  return true;
};
