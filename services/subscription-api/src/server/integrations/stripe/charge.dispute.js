import logger from '../../../common/logger';
import {
  getInvoiceFromCharge,
  cancelSubscription,
  removeSubscription,
} from '../../../external/stripe';
import {
  getStripeSubscription,
  updateStripeSubscription,
} from '../../../models/subscriptions/stripe';

const log = logger('subscription-integration-stripe');

/**
 * @description This function handles the `charge.dispute.created` Stripe Event.
 *
 * Updates the payment alert status of stripe or gift subscription type.
 *
 * @param {object} evt - https://stripe.com/docs/api/disputes/object
 *
 * @typedef {object} evt
 * @property {string} charge - The id of the disputed Stripe Charge
 * @property {string} reason - When a payment is disputed by a cardholder, their card issuer
 *  assigns one of the following categories to best describe the reason for it.
 *  https://stripe.com/docs/disputes/categories
 *
 * @returns {boolean}
 */
export const created = async evt => {
  const { charge, reason } = evt;
  const { subscription: subscriptionId, customer: stripeCustomerId } = await getInvoiceFromCharge(
    charge,
  );

  const subscription = await getStripeSubscription(subscriptionId);
  if (!subscription) {
    log.warn({
      message: `Subscription (${subscriptionId}) not found.`,
      stripeCustomerId,
      subscriptionId,
      stripeEventType: 'charge.dispute.created',
      webhookHandler: 'charge.dispute:created',
    });

    return false;
  }

  await updateStripeSubscription({
    ...subscription,
    paymentAlertType: 'dispute',
    paymentAlertStatus: 'open',
    paymentAlertReason: reason,
    alertPayment: true,
  });
  await cancelSubscription(subscriptionId);

  return true;
};

/**
 * @description This function handles the `charge.dispute.closed` Stripe Event.
 *
 * Updates the payment alert status of stripe or gift subscription type.  Cancels a subscription
 * if the dispute is lost.
 *
 * @param {object} evt - https://stripe.com/docs/api/disputes/object
 *
 * @typedef {object} evt
 * @property {string} charge - The id of the disputed Stripe Charge
 * @property {string} status - The current status of the dispute.
 *  https://stripe.com/docs/api/disputes/object#dispute_object-status
 *
 * @returns {boolean}
 */
export const closed = async evt => {
  const { charge, status } = evt;
  const { subscription: subscriptionId, customer: stripeCustomerId } = await getInvoiceFromCharge(
    charge,
  );

  const subscription = await getStripeSubscription(subscriptionId);

  if (!subscription) {
    log.warn({
      message: `Subscription (${subscriptionId}) not found.`,
      stripeCustomerId,
      subscriptionId,
      stripeEventType: 'charge.dispute.closed',
      webhookHandler: 'charge.dispute:closed',
    });

    return false;
  }

  if (status === 'lost') {
    await updateStripeSubscription({
      ...subscription,
      paymentAlertStatus: status,
      alertPayment: false,
    });

    try {
      await removeSubscription(subscriptionId);
    } catch (error) {
      // Subscription has already been cancelled by Stripe.
      if (error.statusCode === 404) return true;

      log.error('Error removing subscription from Stripe');
      return false;
    }
    return true;
  }

  if (status === 'won' || status === 'warning_closed') {
    await updateStripeSubscription({
      ...subscription,
      paymentAlertStatus: status,
      alertPayment: false,
    });
    return true;
  }

  return true;
};
