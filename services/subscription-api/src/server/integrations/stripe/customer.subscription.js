import logger from '../../../common/logger';
import * as stripe from '../../../external/stripe';
import {
  getStripeSubscription,
  updateStripeSubscription,
  createStripeSubscription,
} from '../../../models/subscriptions/stripe';
import { createStripeCustomer, getStripeCustomerId } from '../../../models/customers/stripe';
import { getActiveSubscriptionByUserIdAndEntitlement } from '../../../models/subscriptions';
import { incrementRedemptionCount } from '../../../external/promotion';
import { trackOrderCompleted } from '../../../tracking/orderCompleted';

const log = logger('subscription-integration-stripe');

/**
 * @description This function handles the `customer.subscription.create` Stripe Event.
 *
 * This function's responsibility is to create Subscription and StripeCustomer documents
 * for users who have created a subscription through Stripe Checkout
 *
 * @param {object} evt - https://stripe.com/docs/api/subscriptions/object
 *
 * @typedef {object} evt
 * @property {string} id - The unique identifier for the Stripe Subscription
 * @property {string} customer - The unique identifier for the Stripe Customer
 * @property {object} plan - Details of the plan the customer subscribed to, including the plan id
 *
 * @returns {boolean}
 */
export const created = async evt => {
  const {
    id: subscriptionId,
    customer: stripeCustomerId,
    items: {
      data: [
        {
          price: { lookup_key: lookupKey },
        },
      ],
    },
    metadata: { promotionId },
  } = evt;

  const {
    metadata: { userId },
  } = await stripe.getCustomer(stripeCustomerId);

  trackOrderCompleted(evt, 'stripe', userId);

  const brand = lookupKey.toLowerCase().substring(0, 2);
  const activeSubscription = await getActiveSubscriptionByUserIdAndEntitlement(userId, brand);

  if (activeSubscription) {
    if (activeSubscription.subscriptionId === subscriptionId) return true;
    log.error({
      message: 'Tried to create a subscription for a user with an active subscription',
      stripeCustomerId,
      subscriptionId,
      stripeEventType: 'customer.subscription.created',
      webhookHandler: 'customer.subscription:created',
    });
    return true;
  }

  try {
    const stripeCustomer = await getStripeCustomerId(userId);
    if (!stripeCustomer) {
      await createStripeCustomer(userId, stripeCustomerId);
    }
    await createStripeSubscription(userId, evt);
    if (promotionId) {
      await incrementRedemptionCount(promotionId, {
        userId,
        brand,
      });
    }
  } catch (error) {
    log.error({
      error,
      message: 'Error creating subscription documents for user',
      stripeCustomerId,
      subscriptionId,
      stripeEventType: 'customer.subscription.created',
      webhookHandler: 'customer.subscription:created',
    });
    return false;
  }
  return true;
};

/**
 * @description This function handles the `customer.subscription.updated` Stripe Event.
 *
 * This function updates a Subscription and Segment.io's user identity based on webhook events.
 *
 * Attribute we care about
 *  - canceled_at (a user cancelled)
 *  - status (a user has cancelled or resumed their plan)
 *  - plan.id (a user changed plans)
 *
 * @param {object} evt - https://stripe.com/docs/api/subscriptions/object
 * @param {object} previousData - An object with dynamic parameters that represent the properties that have that have changed state.
 *
 * @typedef {object} evt
 * @property {string} id - The unique identifier for the Stripe Subscription
 * @property {string} customer - The unique identifier for the Stripe Customer
 * @property {number} canceled_at - A date in the future at which the subscription will automatically get canceled
 * @property {object} plan - Hash describing the plan the customer is subscribed to. Only set if the subscription contains a single plan.
 * @property {number} current_period_start - Start of the current period that the subscription has been invoiced for.
 * @property {number} current_period_end - End of the current period that the subscription has been invoiced for. At the end of this period, a new invoice will be created.
 *
 * @returns {*}
 */
export const updated = async evt => {
  const {
    id: subscriptionId,
    customer: stripeCustomerId,
    canceled_at: canceled,
    current_period_start: start,
    current_period_end: end,
    status,
    items: {
      data: [
        {
          price: { id: planId },
        },
      ],
    },
  } = evt;
  const subscription = await getStripeSubscription(subscriptionId);

  if (!subscription) {
    log.warn({
      message: 'Tried to delete a subscription that does not exist',
      stripeCustomerId,
      subscriptionId,
      stripeEventType: 'customer.subscription.updated',
      webhookHandler: 'customer.subscription:updated',
    });
    return false;
  }

  const convertedFreeTrial = subscription.trialing && status === 'active';
  const changedPlan = subscription.planId !== planId;

  await updateStripeSubscription({
    ...subscription,
    canceled,
    expiring: !!canceled,
    planId,
    start,
    end,
    trialing: status === 'trialing',
    convertedFreeTrial,
    changedPlan,
  });

  return true;
};

/**
 * @description This function handles the `customer.subscription.deleted` Stripe Event.
 *
 * Occurs whenever a customer's subscription ends.
 *
 * @param {object} evt - https://stripe.com/docs/api/subscriptions/object
 *
 * @typedef {object} evt
 * @property {string} id - The unique identifier for the Stripe Subscription
 * @property {string} customer - The unique identifier for the Stripe Customer
 * @property {number} canceled_at - A date in the future at which the subscription will automatically get canceled
 *
 * @returns {boolean}
 */
export const deleted = async evt => {
  const { id: subscriptionId, customer: stripeCustomerId, canceled_at: expired, status } = evt;
  const subscription = await getStripeSubscription(subscriptionId);

  if (!subscription) {
    log.warn({
      message: 'Tried to delete a subscription that does not exist',
      stripeCustomerId,
      subscriptionId,
      stripeEventType: 'customer.subscription.deleted',
      webhookHandler: 'subscriptionDeleted',
    });
    return false;
  }
  const { paymentWarningType } = subscription;

  subscription.active = false;
  subscription.expired = expired || Math.round(new Date().getTime() / 1000);
  subscription.cancelledFreeTrial = subscription.trialing;
  subscription.trialing = status === 'trialing';

  if (paymentWarningType) subscription.paymentWarningStatus = 'unresolved';

  await updateStripeSubscription(subscription);
  await stripe.closeInvoiceBySubscription(stripeCustomerId, subscriptionId);

  return true;
};
