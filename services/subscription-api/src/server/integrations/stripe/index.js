import { Router } from 'express';
import newrelic from 'newrelic';
import bodyparser from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import logger from '../../../common/logger';
import * as stripe from '../../../external/stripe';
import * as stripeutils from '../../../external/stripeutils';

import * as chargeDispute from './charge.dispute';
import * as customer from './customer';
import * as subscription from './customer.subscription';
import * as invoice from './invoice';
import * as checkout from './checkout.session';
import * as paymentIntent from './payment-intent';

export const eventMappings = {
  'charge.dispute.created': chargeDispute.created,
  'charge.dispute.closed': chargeDispute.closed,
  'customer.created': customer.created,
  'customer.subscription.created': subscription.created,
  'customer.subscription.updated': subscription.updated,
  'customer.subscription.deleted': subscription.deleted,
  'invoice.created': invoice.created,
  'invoice.payment_succeeded': invoice.paymentSucceeded,
  'invoice.payment_failed': invoice.paymentFailed,
  'invoice.upcoming': invoice.upcoming,
  'checkout.session.completed': checkout.completed,
  'checkout.session.expired': checkout.expired,
  'payment_intent.succeeded': paymentIntent.succeeded,
  'payment_intent.payment_failed': paymentIntent.paymentFailed,
};

export default () => {
  const api = Router();
  const log = logger('subscription-integration-stripe');

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/integrations/stripe/handler',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  const getEventSignature = async evt => {
    const {
      type,
      id,
      data: {
        object: { id: referenceId },
      },
    } = evt;
    return {
      notificationType: type,
      eventId: id,
      referenceId,
    };
  };

  const verifyEvent = async (req, res, next) => {
    const { body } = req;
    const { id: eventId } = body;
    const signature = await getEventSignature(body);
    log.info({
      ...signature,
      message: 'Recieved Stripe Event',
    });

    if (process.env.VERIFY_WEBHOOKS === 'false') {
      newrelic.addCustomAttribute('verified', false);
      return next();
    }

    // Send event signature to New Relic
    const signatureKeys = Object.keys(signature);
    signatureKeys.map(key => newrelic.addCustomAttribute(key, signature[key]));

    try {
      const isEventAuthentic = await stripe.retrieveEvent(eventId);
      const isDuplicateEvent = false; // await stripeutils.requestDidRun(eventId);

      // Do not process the event if it does not exist in STripe.
      if (!isEventAuthentic) {
        log.warn({
          ...signature,
          message: 'This event could not be verified by Stripe',
        });
        newrelic.addCustomAttribute('verified', false);
        return res.status(401).send('There was a problem verifying the event.');
      }

      // Do not process the event if it has already been sent witin the past 5 minutes
      if (isDuplicateEvent) {
        log.warn({
          ...signature,
          message: 'This event has already been processed',
        });
        newrelic.addCustomAttribute('verified', true);
        return res.status(422).send({
          message: 'The event has already been processed',
        });
      }

      log.info({
        ...signature,
        message: 'Verified Stripe event',
      });
      newrelic.addCustomAttribute('verified', true);
      return next();
    } catch (err) {
      log.error({
        ...signature,
        errorMessage: err.toString(),
        err,
        message: 'There was a problem processing the event',
      });
      return res.status(500).send('There was a problem processing the event.');
    }
  };

  /**
   * @description The function is the universal route handler for the Stripe Event request.  It is
   * often referred to as the "Webhook" handler.
   *
   * @param {*} req
   * @param {*} res
   *
   * @typedef {*} req
   * @property {object} body
   *
   * @typedef {object} body - https://stripe.com/docs/api/events/object
   * @property {string} type - The description of the event (e.g., invouce.created or charge.disputed)
   * @property {string} id - The unique identifier for the Stripe Event
   * @property {object} data - The object containing the associated data for the event.  These values are
   *  determined by the `type` of event that is sent.
   *
   * @returns {*} res
   */
  const eventHandler = async (req, res) => {
    const { body } = req;
    const { type, id, data } = body;
    const signature = await getEventSignature(body);
    const handler = eventMappings[type];

    const signatureKeys = Object.keys(signature);
    signatureKeys.map(key => newrelic.addCustomAttribute(key, signature[key]));

    if (!handler) {
      log.error({
        ...signature,
        message: 'Problem processing a Stripe Event',
      });
      return res.status(422).send('Could not process the request, handler not found.');
    }

    try {
      const { object: attributes, previous_attributes: previousAttributes } = data;
      const result = await handler(attributes, previousAttributes);
      if (result) {
        await stripeutils.processRequest(id);
        log.info({
          ...signature,
          message: 'Successfully processed Stripe envent',
        });
        return res.status(200).send('OK');
      }
      log.error({
        ...signature,
        message: 'Problem processing a Stripe Event',
      });
      return res.status(422).send('Could not process the request');
    } catch (err) {
      newrelic.noticeError(err);
      log.error({
        ...signature,
        errorMessage: err.toString(),
        err,
        message: 'There was a fatal problem processing a Stripe Event',
      });
      return res.status(500).send('There was an error processing the request.');
    }
  };

  api.use('*', bodyparser.json());
  api.post('/firehose', async (req, res) => res.send('Ok'));
  api.post(
    '/handler',
    trackRequest,
    verifyEvent,
    wrapErrors((req, res) => eventHandler(req, res)),
  );

  return api;
};
