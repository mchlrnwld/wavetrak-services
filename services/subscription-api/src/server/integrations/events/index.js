import { Router } from 'express';
import bodyparser from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import logger from '../../../common/logger';
import triggerUpcomingSubscriptionRenewal from './upcomingSubscriptionRenewal';

export const eventMappings = {
  'Upcoming Subscription Renewal': triggerUpcomingSubscriptionRenewal,
};

export default () => {
  const api = Router();
  const log = logger('trigger-subscription-event');

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/integrations/subscriptions/event',
      request: {
        headers: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  /**
   * @description This function is a universal route handler for triggering Segment events.
   * @param {*} req
   * @param {*} res
   *
   * @typedef {*} req
   * @property {object} body
   *
   * @typedef {object} body
   * @property {string} eventName - The name of the event to be triggered
   *
   * @returns {*} res
   */
  const eventHandler = async (req, res) => {
    const {
      body: { eventName },
    } = req;

    if (!eventName) {
      log.error({
        message: 'Event name required in request body',
      });
      return res.status(400).send('Event name required in request body');
    }

    const handler = eventMappings[eventName];
    if (!handler) {
      log.error({
        eventName,
        message: 'Handler not found for event',
      });
      return res.status(400).send('Handler not found for event');
    }

    try {
      await handler(req.body);
      log.info({
        eventName,
        message: 'Successfully triggered event',
      });
      return res.status(200).send('Successfully triggered event');
    } catch (err) {
      log.error({
        errorMessage: err.toString(),
        err,
        message: 'An error occured while triggering the event',
      });
      return res.status(500).send('There was an error processing the request.');
    }
  };

  api.use('*', bodyparser.json());
  api.post(
    '/event',
    trackRequest,
    wrapErrors((req, res) => eventHandler(req, res)),
  );

  return api;
};
