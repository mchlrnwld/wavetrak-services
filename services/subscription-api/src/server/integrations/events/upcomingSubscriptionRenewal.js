import { trackUpcomingSubscriptionRenewal } from '../../../tracking/upcomingSubscriptionRenewal';
import { getUpcomingInvoice } from '../../../external/stripe';
import { getSubscriptionFromCustomParams } from '../../../models/subscriptions';
import { getPlanAmountAndInterval } from '../../../utils/offer';

const triggerUpcomingSubscriptionRenewal = async ({ subscriptionId }) => {
  if (!subscriptionId) {
    throw new Error('subscriptionId required in request body');
  }
  const subscription = await getSubscriptionFromCustomParams({
    subscriptionId,
    active: true,
    type: { $in: ['apple', 'google', 'stripe'] },
  });

  if (!subscription) {
    throw new Error('No active apple, google or stripe subscription found for this subscriptionId');
  }

  const { entitlement, planId } = subscription;

  let newInvoiceAmount;
  if (subscription.type === 'stripe') {
    const upcomingInvoice = await getUpcomingInvoice(subscription.stripeCustomerId, subscriptionId);
    const { lines } = upcomingInvoice;
    const {
      data: [
        {
          price: { unit_amount: amount },
        },
      ],
    } = lines;
    newInvoiceAmount = amount / 100;
  } else {
    const { amount } = getPlanAmountAndInterval(planId);
    newInvoiceAmount = amount / 100;
  }

  trackUpcomingSubscriptionRenewal(subscription, {
    entitlement,
    invoiceRenewalDate: subscription.end,
    newInvoiceAmount,
    platform: 'web',
  });
};

export default triggerUpcomingSubscriptionRenewal;
