import { getSubscriptionByItunesTransactionId } from '../../../models/subscriptions/itunes';
import { sortLatestReceiptInfo } from '../../../utils/itunes';
import { keysToCamel } from '../../../utils/toCamelCase';
import { trackExpiredSubscription } from '../../../tracking';

/**
 * iTunes Notification Handler for: [CANCEL]
 *
 * @description: [CANCEL] Indicates that the subscription was canceled either by Apple customer support
 * or by the App Store when the user upgraded their subscription. The [cancellation_date_ms] key
 * contains the date and time when the subscription was canceled or upgraded.
 *
 * @param {Object} notification The [notification] event object
 * @param {Object} notification.unified_receipt - An object that contains information about the most recent
 * in-app purchase transactions for the app
 *
 * @param {String} notification.unified_receipt.latest_receipt: iTunes receipt representing the event instance.
 * @param {Array} notification.unified_receipt.latest_receipt_info - An array that contains the latest 100 in-app
 * purchase transactions of the decoded value in latest_receipt. This array excludes transactions
 * for consumable products that your app has marked as finished. The contents of this array are
 * identical to those in responseBody.Latest_receipt_info in the verifyReceipt endpoint response
 * for receipt validation.
 *
 * @typedef {Object} latest_expired_receipt_info
 * @param {String} notification.unified_receipt.latest_receipt_info[].transaction_id - A unique identifier for a transaction such as a purchase,
 *  restore, or renewal. Also represents the subscriptionId value of the Mongo subscription.
 * @param {String} notification.unified_receipt.latest_receipt_info[].cancellation_reason - the reason for cancellation.
 *  This field is only present for refunded transactions.
 *   "1" - Customer canceled their transaction due to an actual or perceived issue within your app.
 *   "0" - Transaction was canceled for another reason (ie.the customer made the purchase
 *         accidentally).
 * @param {String} notification.unified_receipt.latest_receipt_info[].product_id - the subscription plan
 * @param {String} notification.unified_receipt.latest_receipt_info[].cancellation_date_ms - Specifies the date and time the App Store processed
 *  the refund for a subscription either because the customer requested a cancellation through
 *  Apple customer support or upgraded their subscription, in UNIX epoch time format, in
 *  milliseconds. Use this time format for processing dates. This field is only present for refunded transactions.
 *
 * @returns {Boolean} true - if the notifciation was process successfully
 * @throws {Boolean} false - if the notification cannot be processed.
 *
 */
const cancel = async notification => {
  const transformedNotificationPayload = keysToCamel(notification);
  const {
    unifiedReceipt: { latestReceipt, latestReceiptInfo },
  } = transformedNotificationPayload;

  const [{ transactionId, cancellationReason, cancellationDateMs }] = sortLatestReceiptInfo(
    latestReceiptInfo,
  );

  /**
   * If [cancellation_date_ms] is not present in the latest receipt object, this Indicates
   * a subscription upgrade to a new product sku.  Upgrades should be processed in the
   * `DID_CHANGE_RENEWAL_PREF` notification handler.  Therefore, the `CANCEL` handler should
   * only update subscriptions that are truely cancelled in nature.
   */
  if (!cancellationDateMs) return true;
  const subscription = await getSubscriptionByItunesTransactionId(transactionId);
  if (!subscription) {
    return false;
  }

  subscription.active = false;
  subscription.expired = cancellationDateMs / 1000;
  subscription.latestExpiredReceipt = latestReceipt;
  subscription.cancellationReason = cancellationReason;
  const expiredSubscription = await subscription.save();

  trackExpiredSubscription(expiredSubscription);

  return true;
};

export default cancel;
