import { Router } from 'express';
import newrelic from 'newrelic';
import bodyparser from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import AWS from 'aws-sdk';
import logger from '../../../common/logger';
import cancelHandler from './cancel';
import didChangeRenewalPrefHandler from './didChangeRenewalPref';
import didChangeRenewalStatusHandler from './didChangeRenewalStatus';
import initialBuyHandler from './initialBuy';
import interactiveRenewalHandler from './interactiveRenewal';
import didFailToRenew from './didFailToRenew';
import didRecover from './didRecover';
import didRenew from './didRenew';

/**
 * Hash containing key lookups matching function to proceess
 * iTunes Server-to-Server notification events
 */
export const mappings = {
  INITIAL_BUY: initialBuyHandler,
  CANCEL: cancelHandler,
  INTERACTIVE_RENEWAL: interactiveRenewalHandler,
  DID_CHANGE_RENEWAL_PREF: didChangeRenewalPrefHandler,
  DID_CHANGE_RENEWAL_STATUS: didChangeRenewalStatusHandler,
  DID_FAIL_TO_RENEW: didFailToRenew,
  DID_RECOVER: didRecover,
  DID_RENEW: didRenew,
};

export default () => {
  const api = Router();
  const log = logger('subscription-integration-itunes');

  const getNotificiationSignature = body => {
    /* eslint-disable camelcase */
    const { environment, notification_type: notificationType, unified_receipt } = body;
    const pendingRenewalInfo = unified_receipt?.pending_renewal_info;

    const [{ original_transaction_id: originalTransactionId }] = pendingRenewalInfo || [{}];
    /* eslint-enable camelcase */

    return {
      environment,
      notificationType,
      originalTransactionId,
    };
  };

  const trackRequest = (req, res, next) => {
    log.trace({
      action: '/integrations/itunes/handler',
      request: {
        header: req.headers,
        params: req.params,
        query: req.query,
        body: req.body,
      },
    });
    next();
  };

  /**
   * Verifies the notification against the iTunes store validation password per
   * envrironment.  If the password pass in the request body matches the password
   * stored in the APPLE_IAP_PASSWord environment variable the request is logges as
   * verified.
   *
   * Note: iTunes only supplies a Sandbox and PROD environment.
   *
   */
  const verifyNotification = async (req, res, next) => {
    const {
      body: { password },
    } = req;
    const notificationSignature = getNotificiationSignature(req.body);

    if (password !== process.env.APPLE_IAP_PASSWORD) {
      log.warn({
        ...notificationSignature,
        message: 'Forbidden iTunes noticiation request',
      });
      newrelic.addCustomAttribute('verified', false);
      return res.status(403).send({ message: 'This request is forbidden' });
    }
    newrelic.addCustomAttribute('verified', true);
    return next();
  };

  /**
   * Firehose is a utility API to enable event streaming into AWS S3. This
   * stream should only be enabled for initial data collection, test setup,
   * and validation of new events if needed.
   *
   */
  const firehose = async (req, res) => {
    if (!process.env.ENABLE_ITUNES_FIREHOSE) {
      return res.status(200).send('OK');
    }
    const { body } = req;
    const notificationSignature = getNotificiationSignature(body);

    const s3 = new AWS.S3({ apiVersion: '2006-03-01' });
    const timestamp = Date.now();
    const { notificationType } = notificationSignature;
    const environment = process.env.NODE_ENV === 'production' ? 'prod' : process.env.NODE_ENV;

    const data = {
      Key: `${notificationType}-${timestamp}`,
      Body: JSON.stringify(body),
      Bucket: `subscription-notifications-${environment}/itunes`,
      CacheControl: 'no-cache',
      ContentType: 'application/json',
    };

    return s3.putObject(data, err => {
      if (err) {
        const { message } = err;
        log.info({
          ...notificationSignature,
          message: 'Failed to send notificiation data to S3',
          error: message,
        });
        return res.status(500).send({ message });
      }
      return res.status(200).send('OK');
    });
  };

  /**
   * Primary entry point for processing iTunes Notifications requests.
   */
  const eventHandler = async (req, res) => {
    const handler = mappings[req.body.notification_type];
    const signature = getNotificiationSignature(req.body);

    if (!handler || !signature) {
      log.error({
        ...signature,
        message: 'No handler or event signature found.',
      });
      return res.status(400).send({ message: 'Notification handler not recognized.' });
    }

    newrelic.addCustomAttributes({
      itunesNotificationType: signature.notificationType,
      originalTransactionId: signature.originalTransactionId,
    });
    try {
      const result = await handler(req.body);

      if (result) {
        log.info({
          ...signature,
          message: 'Successfully processed iTunes notification',
        });
        return res.status(200).send('OK');
      }
    } catch (error) {
      const { message } = error;
      newrelic.noticeError(error, { message });
      log.error({
        ...signature,
        message: 'Problem in the handler processing the iTunes Notification Event.',
      });
      return res.status(422).send('Could not process the iTunes request.');
    }
    log.error({
      ...signature,
      message: 'Problem processing the iTunes Notification Event.',
    });
    return res.status(422).send('Could not process the iTunes request.');
  };

  api.use('*', bodyparser.json());
  api.post('/firehose', trackRequest, verifyNotification, async (req, res) => firehose(req, res));
  api.post(
    '/handler',
    trackRequest,
    verifyNotification,
    wrapErrors((req, res) => eventHandler(req, res)),
  );
  return api;
};
