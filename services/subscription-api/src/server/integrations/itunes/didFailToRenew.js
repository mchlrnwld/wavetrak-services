import { getSubscriptionByItunesTransactionId } from '../../../models/subscriptions/itunes';
import { trackPaymentFailed } from '../../../tracking';
import { sortLatestReceiptInfo } from '../../../utils/itunes';
import { keysToCamel } from '../../../utils/toCamelCase';

/**
 * iTunes Notification Handler for: [DID_FAIL_TO_RENEW]
 *
 * @description: [DID_FAIL_TO_RENEW] Indicates a subscription that failed to renew due to a billing issue.
 * Check [is_in_billing_retry_period] to know the current retry status of the subscription, and
 * [grace_period_expires_date] to know the new service expiration date if the subscription is in
 * a billing grace period.
 *
 * @param {Object} notification - the notification event object
 * @param {Object} notification.unified_receipt - An object that contains information about the most recent
 * in-app purchase transactions for the app
 *
 * @param {String} notification.unified_receipt.latest_receipt: iTunes receipt representing the event instance.
 * @param {Array} notification.unified_receipt.latest_receipt_info - An array that contains the latest 100 in-app
 * purchase transactions of the decoded value in latest_receipt. This array excludes transactions
 * for consumable products that your app has marked as finished. The contents of this array are
 * identical to those in responseBody.Latest_receipt_info in the verifyReceipt endpoint response
 * for receipt validation.
 *
 * @param {Array} notification.unified_receipt.pending_renewal_info - An array where each element contains
 * the pending renewal information for each auto-renewable subscription identified in product_id.
 * @param {String} notification.unified_receipt.pending_renewal_info[].is_in_billing_retry_period - This field
 * indicates whether Apple is attempting to renew an expired subscription automatically. If the customer’s subscription
 * failed to renew because the App Store was unable to complete the transaction, this value reflects whether the App
 * Store is still trying to renew the subscription. Two possible values inlcude:
 *  "1" - The App Store is attempting to renew the subscription.
 *  "0" - The App Store has stopped attempting to renew the subscription.
 * * @param {String} notification.unified_receipt.pending_renewal_info[].expiration_intent - The reason a subscription
 * expired. This field is only present for a receipt that contains an expired auto-renewable subscription.
 *
 * @param {String} notification.auto_renew_product_id - the subscription plan that the user's subscription
 *  renew with.
 * @param {String} notification.auto_renew_status - stringified boolean that represents if the subscription
 *  will renew.  This is different than the auto_renew_status field on the receipt (which constains
 *  "0" or "1")
 *
 * The [latest_receipt_info[]] object
 * @param {String} notification.unified_receipt.latest_receipt_info[].transaction_id - A unique identifier for a transaction such as a purchase,
 *  restore, or renewal.  Also represents the subscriptionId value of the Mongo subscription.
 * @param {String} notification.unified_receipt.latest_receipt_info[].original_transaction_id - The transaction identifier of the original purchase.
 * @param {String} notification.unified_receipt.latest_receipt_info[].product_id - the currect subscription plan.
 *
 * @returns {Boolean} true - if the notification is processed successfully
 * @throws {Boolean} false - if the notification cannot be processed.
 *
 */
const didFailToRenew = async notification => {
  const transformedNotificationPayload = keysToCamel(notification);
  const {
    unifiedReceipt: { latestReceipt, latestReceiptInfo, pendingRenewalInfo },
    autoRenewStatus,
  } = transformedNotificationPayload;

  const [{ transactionId, originalTransactionId }] = sortLatestReceiptInfo(latestReceiptInfo);

  const { expirationIntent, isInBillingRetryPeriod } = pendingRenewalInfo.find(
    ({ originalTransactionId: pendingOriginalTransactionId }) =>
      pendingOriginalTransactionId === originalTransactionId,
  );

  const subscription = await getSubscriptionByItunesTransactionId(transactionId);

  if (!subscription) {
    return false;
  }

  const isSubscriptionExpiring = autoRenewStatus === 'false' || !!expirationIntent;
  subscription.active = isInBillingRetryPeriod === '1';
  subscription.alertPayment = isInBillingRetryPeriod === '1';
  subscription.expiring = isSubscriptionExpiring;
  subscription.latestReceipt = latestReceipt;
  const updatedSubscription = await subscription.save();

  trackPaymentFailed(updatedSubscription);

  return true;
};

export default didFailToRenew;
