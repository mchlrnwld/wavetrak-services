import {
  getSubscriptionByItunesTransactionId,
  getSubscriptionByItunesOriginalTransactionId,
  updateItunesSubscription,
} from '../../../models/subscriptions/itunes';
import { keysToCamel } from '../../../utils/toCamelCase';
import {
  getSubscriptionDataFromItunesNotification,
  sortLatestReceiptInfo,
} from '../../../utils/itunes';

/**
 * iTunes Notification Handler for: [INTERACTIVE_RENEWAL]
 *
 * @description: [INTERACTIVE_RENEWAL] Indicates the customer renewed a subscription interactively, either
 * by using your app’s interface, or on the App Store in account settings. Make service available
 * immediately.
 *
 * @param {Object} notification - the notification event object
 * @param {Object} notification.unified_receipt - An object that contains information about the most recent
 * in-app purchase transactions for the app
 *
 * @param {String} notification.unified_receipt.latest_receipt: iTunes receipt representing the event instance.
 * @param {Array} notification.unified_receipt.latest_receipt_info - An array that contains the latest 100 in-app
 * purchase transactions of the decoded value in latest_receipt. This array excludes transactions
 * for consumable products that your app has marked as finished. The contents of this array are
 * identical to those in responseBody.Latest_receipt_info in the verifyReceipt endpoint response
 * for receipt validation.
 * @param {String} notification.auto_renewal_product_id - the subscription plan that the user's subscription
 *  renew with.
 * @param {String} notification.auto_renew_status - stringified boolean that represents if the subscription
 *  will renew.
 *
 * The [latest_receipt_info[]] object
 * @param {String} notification.unified_receipt.latest_receipt_info[].transaction_id - A unique identifier for a transaction such as a purchase,
 *  restore, or renewal.  Also represents the subscriptionId value of the Mongo subscription.
 * @param {String} notification.unified_receipt.latest_receipt_info[].original_transaction_id - The transaction identifier of the original purchase.
 * @param {String} notification.unified_receipt.latest_receipt_info[].product_id - the currect subscription plan.
 * @param {String} notification.unified_receipt.latest_receipt_info[].original_purchase_date_ms - The time in milliseconds of the
 *  original purchase.
 * @param {String} notification.unified_receipt.latest_receipt_info[].purchase_date_ms - In an auto-renewable subscription receipt, the purchase
 *  date is the date when the subscription was either purchased or renewed (with or without a
 *  lapse). For an automatic renewal that occurs on the expiration date of the current period, the
 *  purchase date is the start date of the next period, which is identical to the end date of the
 *  current period.
 * @param {String} notification.unified_receipt.latest_receipt_info[].expires_date - The time in milliseconds a subscription expires or renews.
 * @param {String} notification.unified_receipt.latest_receipt_info[].is_trial_period - An indicator of whether an auto-renewable subscription is
 * in the free trial period.
 *
 * @returns {Boolean} true - if the notification is processed successfully.
 * @throws {Boolean} false - if the notification cannot be processed.
 */
const interactiveRenewal = async notification => {
  const transformedNotificationPayload = keysToCamel(notification);
  const {
    unifiedReceipt: { latestReceipt, latestReceiptInfo },
  } = transformedNotificationPayload;

  const [{ transactionId, originalTransactionId }] = sortLatestReceiptInfo(latestReceiptInfo);

  const subscription = await getSubscriptionByItunesTransactionId(transactionId);

  if (!subscription) {
    const originalSubscription = await getSubscriptionByItunesOriginalTransactionId(
      originalTransactionId,
    );

    if (!originalSubscription) {
      return false;
    }

    const newSubscriptionData = getSubscriptionDataFromItunesNotification(
      transformedNotificationPayload,
    );

    const { user, baseReceipt } = originalSubscription;

    await updateItunesSubscription({ ...newSubscriptionData, user, baseReceipt });

    return true;
  }

  subscription.latestReceipt = latestReceipt;
  subscription.active = true;
  await subscription.save();

  return true;
};

export default interactiveRenewal;
