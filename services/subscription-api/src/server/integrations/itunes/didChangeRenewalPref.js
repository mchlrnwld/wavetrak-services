import { getSubscriptionByItunesTransactionId } from '../../../models/subscriptions/itunes';
import { trackChangedSubscriptionPlan } from '../../../tracking';
import { sortLatestReceiptInfo } from '../../../utils/itunes';
import { keysToCamel } from '../../../utils/toCamelCase';

/**
 * iTunes Notification Handler for: [DID_CHANGE_RENEWAL_PREF]
 *
 * @description: [DID_CHANGE_RENEWAL_PREF] Indicates the customer made a change in their subscription
 * plan that takes effect at the next renewal. The currently active plan is not affected.
 * Meaning, the current billing period will be completed before billing is initiated for the
 * on plan.
 *
 * @param {Object} notification - the notification event object
 * @param {Object} notification.unified_receipt - An object that contains information about the most recent
 * in-app purchase transactions for the app
 *
 * @param {String} notification.unified_receipt.latest_receipt: iTunes receipt representing the event instance.
 * @param {Array} notification.unified_receipt.latest_receipt_info - An array that contains the latest 100 in-app
 * purchase transactions of the decoded value in latest_receipt. This array excludes transactions
 * for consumable products that your app has marked as finished. The contents of this array are
 * identical to those in responseBody.Latest_receipt_info in the verifyReceipt endpoint response
 * for receipt validation.
 * @param {String} notification.auto_renew_product_id - the subscription plan that the user's subscription
 *  renew with.
 * @param {String} notification.auto_renew_status - stringified boolean that represents if the subscription
 * will renew. This is different than the auto_renew_status field on the receipt (which constains
 *  "0" or "1").
 *
 * The [latest_receipt_info[]] object
 * @param {object} notification.unified_receipt.latest_receipt_info[]
 * @param {String} notification.unified_receipt.latest_receipt_info[].transaction_id - A unique identifier for a transaction such as a purchase,
 *  restore, or renewal.  Also represents the subscriptionId value of the Mongo subscription.
 * @param {String} notification.unified_receipt.latest_receipt_info[].original_transaction_id - The transaction identifier of the original purchase.
 * @param {String} notification.unified_receipt.latest_receipt_info[].product_id - the currect subscription plan.
 *  will renew.
 *
 * @returns {Boolean} true - if the notification was processed successfully.
 * @throws {boolean} false - if the notification cannot be processed.
 *
 */
const didChangeRenewalPref = async notification => {
  const transformedNotificationPayload = keysToCamel(notification);
  const {
    unifiedReceipt: { latestReceipt, latestReceiptInfo },
    autoRenewStatus,
    autoRenewProductId,
  } = transformedNotificationPayload;

  const [{ transactionId, productId }] = sortLatestReceiptInfo(latestReceiptInfo);
  const subscription = await getSubscriptionByItunesTransactionId(transactionId);

  if (!subscription) {
    return false;
  }

  const isSubscriptionExpiring = autoRenewStatus === 'false';
  const hasChangingPlans = autoRenewProductId !== subscription.productId;

  subscription.expiring = isSubscriptionExpiring;
  subscription.autoRenewProductId = autoRenewProductId;
  subscription.latestReceipt = latestReceipt;
  subscription.planId = productId;
  const updatedSubscription = await subscription.save();

  if (hasChangingPlans) trackChangedSubscriptionPlan(updatedSubscription);

  return true;
};

export default didChangeRenewalPref;
