import { getSubscriptionByItunesTransactionId } from '../../../models/subscriptions/itunes';
import { trackCancelledFreeTrial, trackCancelledSubscription } from '../../../tracking';
import { sortLatestReceiptInfo } from '../../../utils/itunes';
import { keysToCamel } from '../../../utils/toCamelCase';
/**
 * iTunes Notification Handler for: [DID_CHANGE_RENEWAL_STATUS]
 *
 * @description: [DID_CHANGE_RENEWAL_STATUS] Indicates a change in the subscription renewal status.
 * Check the [auto_renew_status_change_date_ms] and the [auto_renew_status] in the JSON to know
 * the date and time when the status was last updated and the current renewal status.
 *
 * @param {Object} notification - the notification event object
 * @param {Object} notification.unified_receipt - An object that contains information about the most recent
 * in-app purchase transactions for the app
 *
 * @param {String} notification.unified_receipt.latest_receipt: iTunes receipt representing the event instance.
 * @param {Array} notification.unified_receipt.latest_receipt_info - An array that contains the latest 100 in-app
 * purchase transactions of the decoded value in latest_receipt. This array excludes transactions
 * for consumable products that your app has marked as finished. The contents of this array are
 * identical to those in responseBody.Latest_receipt_info in the verifyReceipt endpoint response
 * for receipt validation.
 * @param {String} notification.auto_renew_product_id - the subscription plan that the user's subscription
 *  renew with.
 * @param {String} notification.auto_renew_status - stringified boolean that represents if the subscription
 *  will renew.  This is different than the auto_renew_status field on the receipt (which constains
 *  "0" or "1")
 * @param {String} notification.auto_renew_status_change_date_ms - The time in milliseconds of when the
 *  subscription status was changed.
 *
 * The [latest_receipt_info[]] object
 * @param {String} notification.unified_receipt.latest_receipt_info[].transaction_id - A unique identifier for a transaction such as a purchase,
 *  restore, or renewal.  Also represents the subscriptionId value of the Mongo subscription.
 * @param {String} notification.unified_receipt.latest_receipt_info[].original_transaction_id - The transaction identifier of the original purchase.
 * @param {String} notification.unified_receipt.latest_receipt_info[].product_id - the currect subscription plan.
 *
 * @returns {Boolean} true - if the notification was processed successfully.
 * @throws {Boolean} false - if the notification cannot be processed.
 *
 */
const didChangeRenewalStatus = async notification => {
  const transformedNotificationPayload = keysToCamel(notification);
  const {
    unifiedReceipt: { latestReceipt, latestReceiptInfo },
    autoRenewStatus,
    autoRenewStatusChangeDateMs,
  } = transformedNotificationPayload;

  const [{ transactionId }] = sortLatestReceiptInfo(latestReceiptInfo);

  const subscription = await getSubscriptionByItunesTransactionId(transactionId);

  if (!subscription) {
    return false;
  }

  const isSubscriptionExpiring = autoRenewStatus === 'false';
  const isSubscriptionTrialing = subscription.trialing;

  subscription.expiring = isSubscriptionExpiring;
  subscription.autoRenewStatusChangeDate = autoRenewStatusChangeDateMs / 1000;
  subscription.latestReceipt = latestReceipt;
  const updatedSubscription = await subscription.save();

  if (isSubscriptionExpiring && isSubscriptionTrialing) {
    trackCancelledFreeTrial(updatedSubscription);
  }

  if (isSubscriptionExpiring && !isSubscriptionTrialing) {
    trackCancelledSubscription(updatedSubscription);
  }

  // TODO Do we want to fire an event if a subscription is reactivated from an expiring status?

  return true;
};

export default didChangeRenewalStatus;
