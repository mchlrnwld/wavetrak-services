import {
  getSubscriptionByItunesOriginalTransactionId,
  updateItunesSubscription,
} from '../../../models/subscriptions/itunes';
import { keysToCamel } from '../../../utils/toCamelCase';
import {
  getSubscriptionDataFromItunesNotification,
  sortLatestReceiptInfo,
} from '../../../utils/itunes';

/**
 * iTunes Notification Handler for: [DID_RECOVER]
 *
 * @description: [DID_RECOVER] Indicates a successful automatic renewal of an expired
 * subscription that failed to renew in the past. Check [expires_date] to determine the
 * next renewal date and time.
 *
 * @param {Object} notification - the notification event object
 * @param {Object} notification.unified_receipt - An object that contains information about the most recent
 * in-app purchase transactions for the app
 *
 * @param {String} notification.unified_receipt.latest_receipt: iTunes receipt representing the event instance.
 * @param {Array} notification.unified_receipt.latest_receipt_info - An array that contains the latest 100 in-app
 * purchase transactions of the decoded value in latest_receipt. This array excludes transactions
 * for consumable products that your app has marked as finished. The contents of this array are
 * identical to those in responseBody.Latest_receipt_info in the verifyReceipt endpoint response
 * for receipt validation.
 *
 * @property {String} notification.auto_renew_product_id - the subscription plan that the user's subscription
 *  renew with.
 * @property {String} notification.auto_renew_status - stringified boolean that represents if the subscription
 *  will renew.  This is different than the auto_renew_status field on the receipt (which constains
 *  "0" or "1")
 *
 * The [latest_receipt_info[]] object
 * @param {String} notification.unified_receipt.latest_receipt_info[].transaction_id - A unique identifier for a transaction such as a purchase,
 *  restore, or renewal.  Also represents the subscriptionId value of the Mongo subscription.
 * @param {String} notification.unified_receipt.latest_receipt_info[].original_transaction_id - The transaction identifier of the original purchase.
 * @param {String} notification.unified_receipt.latest_receipt_info[].product_id - the currect subscription plan.
 * @param {String} notification.unified_receipt.latest_receipt_info[].expires_date_ms - the currect subscription plan.
 *
 * @returns {Boolean} true - if the notification is processed successfully
 * @throws {Boolean} false - if the notification cannot be processed.
 *
 */
const didRecover = async notification => {
  const transformedNotificationPayload = keysToCamel(notification);
  const {
    unifiedReceipt: { latestReceiptInfo },
  } = transformedNotificationPayload;

  const [{ originalTransactionId }] = sortLatestReceiptInfo(latestReceiptInfo);
  const subscriptionData = getSubscriptionDataFromItunesNotification(
    transformedNotificationPayload,
  );
  const originalSubscription = await getSubscriptionByItunesOriginalTransactionId(
    originalTransactionId,
  );

  if (!originalSubscription) {
    return false;
  }

  const { user, baseReceipt } = originalSubscription;

  await updateItunesSubscription({ ...subscriptionData, user, baseReceipt, alertPayment: false });

  // TODO Do we want to fire an event if a subscription is reactivated from an expiring status?

  return true;
};

export default didRecover;
