import { getSubscriptionByItunesTransactionId } from '../../../models/subscriptions/itunes';
import { sortLatestReceiptInfo } from '../../../utils/itunes';
import { trackOrderCompleted } from '../../../tracking/orderCompleted';
/**
 * iTunes Notification Handler for: [INITIAL_BUY]
 *
 * @description: [INITIAL_BUY] Occurs at the initial purchase of the subscription.
 * Store the [unified_receipt.latest_receipt] on your server as a token to verify the user’s subscription status
 * at any time, by validating it with the App Store.
 *
 * @param {Object} notification - the notification event object
 * @param {Object} notification.unified_receipt - An object that contains information about the most recent
 * in-app purchase transactions for the app
 *
 * @param {String} notification.unified_receipt.latest_receipt: iTunes receipt representing the event instance.
 * @param {Array} notification.unified_receipt.latest_receipt_info - An array that contains the latest 100 in-app
 * purchase transactions of the decoded value in latest_receipt. This array excludes transactions
 * for consumable products that your app has marked as finished. The contents of this array are
 * identical to those in responseBody.Latest_receipt_info in the verifyReceipt endpoint response
 * for receipt validation.
 *
 * The latest_receipt_info[] object properties
 * @param {Object} notification.unified_receipt.latest_receipt_info[]
 * @param {String} notification.unified_receipt.latest_receipt_info[].transaction_id - A unique identifier for a transaction such as a purchase,
 *  restore, or renewal.  Also represents the subscriptionId value of the Mongo subscription.
 * @param {String} notification.unified_receipt.latest_receipt_info[].original_transaction_id - The transaction identifier of the original purchase.
 * @param {String} notification.unified_receipt.latest_receipt_info[].product_id - the currect subscription plan.
 * @param {String} notification.unified_receipt.latest_receipt_info[].original_purchase_date_ms - The time in milliseconds of the
 *  original purchase.
 * @param {String} notification.unified_receipt.latest_receipt_info[].purchase_date_ms - In an auto-renewable subscription receipt, the purchase
 *  date is the date when the subscription was either purchased or renewed (with or without a
 *  lapse). For an automatic renewal that occurs on the expiration date of the current period, the
 *  purchase date is the start date of the next period, which is identical to the end date of the
 *  current period.
 * @param {String} notification.unified_receipt.latest_receipt_info[].expires_date - The time in milliseconds a subscription expires or renews.
 * @param {String} notification.unified_receipt.latest_receipt_info[].is_trial_period - An indicator of whether an auto-renewable subscription is
 * in the free trial period.
 * @param {String} notification.unified_receipt.latest_receipt_info[].product_id - The subscription renewal planId.
 *
 * @returns {Boolean} true - if the notification was processed successfully
 * @throws {boolean} false - if the notification cannot be processed.
 *
 * @example responses can be found here: https://s3.console.aws.amazon.com/s3/buckets/subscription-notifications-prod/itunes/?region=us-west-1&tab=overview
 *
 */
const initialBuy = async notification => {
  const {
    unified_receipt: { latest_receipt: latestReceipt, latest_receipt_info: latestReceiptInfo },
  } = notification;

  const [{ transaction_id: transactionId }] = sortLatestReceiptInfo(latestReceiptInfo);

  const subscription = await getSubscriptionByItunesTransactionId(transactionId);
  if (!subscription) {
    /**
     * When a subscription does not exist for this notification
     * we should attempt to reconcile the following:
     *
     * 1. [WIP] Is there a corresponding {original_transaction_id} for an inactive
     *    (historical) suscription?  If so, trigger an event that instructs the
     *    corresponding user to "resend" their receipt from the application.
     */
    return true;
  }

  subscription.latestReceipt = latestReceipt;
  await subscription.save();

  const userId = subscription.user.toString();
  trackOrderCompleted(latestReceiptInfo[0], 'apple', userId);

  return true;
};

export default initialBuy;
