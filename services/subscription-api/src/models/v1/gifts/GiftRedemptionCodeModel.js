import mongoose from 'mongoose';

const GiftRedemptionCodeSchema = mongoose.Schema(
  {
    giftProduct: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'GiftProducts',
    },
    redeemCode: {
      type: String,
      required: [true, 'redeemCode is required'],
      trim: true,
    },
    chargeId: {
      type: String,
      trim: true,
    },
    purchaseDate: {
      type: Date,
      default: Date.now,
    },
    redeemDate: {
      type: Date,
      default: null,
    },
    redeemed: {
      type: Boolean,
      default: false,
    },
    purchaserEmail: {
      type: String,
      required: [true, 'purchaserEmail is required'],
      trim: true,
    },
    department: {
      type: String,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
  },
  { collection: 'GiftRedemptionCodes' },
);

GiftRedemptionCodeSchema.index({ redeemCode: 1 }, { unique: true });

const GiftRedemptionCodeModel = mongoose.model('GiftRedemptionCodes', GiftRedemptionCodeSchema);

export default GiftRedemptionCodeModel;
