import GiftProductsModel from './GiftProductsModel';

export const getGiftProducts = async (filterParams = {}) => {
  const giftProducts = await GiftProductsModel.find({ ...filterParams });
  return giftProducts;
};

export const createGiftProduct = giftParams =>
  new Promise(async (resolve, reject) => {
    const {
      brand,
      name,
      duration,
      price,
      isActive,
      imagePath,
      bestValue,
      description,
    } = giftParams;
    try {
      const newGiftProduct = new GiftProductsModel({
        brand,
        name,
        duration,
        price,
        isActive,
        imagePath,
        bestValue,
        description,
      });

      const giftProduct = await newGiftProduct.save();
      resolve(giftProduct);
    } catch (error) {
      reject(error);
    }
  });

export const updateGiftProduct = (id, giftParams) =>
  new Promise(async (resolve, reject) => {
    try {
      const updatedGiftProduct = await GiftProductsModel.findOneAndUpdate(
        { _id: id },
        { ...giftParams },
        { new: true },
      );
      resolve(updatedGiftProduct);
    } catch (error) {
      reject(error);
    }
  });

export const getGiftProduct = id =>
  new Promise(async (resolve, reject) => {
    try {
      const giftProduct = await GiftProductsModel.findOne({ _id: id });
      resolve(giftProduct);
    } catch (error) {
      reject(error);
    }
  });

export const getGiftProductByParams = params =>
  new Promise(async (resolve, reject) => {
    try {
      const giftProduct = await GiftProductsModel.findOne(params);
      resolve(giftProduct);
    } catch (error) {
      reject(error);
    }
  });
