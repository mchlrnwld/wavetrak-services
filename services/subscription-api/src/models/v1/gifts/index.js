import moment from 'moment';

export const generateGiftSubscription = giftRedemptionCode => {
  const {
    _id: id,
    redeemCode,
    giftProduct: { brand, duration },
  } = giftRedemptionCode;

  return {
    type: 'gift',
    subscriptionId: `gift_${redeemCode}`,
    periodStart: moment().toDate(),
    periodEnd: moment()
      .add(duration, 'months')
      .toDate(),
    entitlement: `${brand}_premium`,
    planId: `${brand}_gift`,
    createAt: moment().toDate(),
    onTrial: false,
    expiring: true,
    giftRedemptionCodes: [id],
  };
};

export const extendEndDate = (periodEnd, duration) =>
  moment(periodEnd)
    .add(duration, 'months')
    .toDate();
