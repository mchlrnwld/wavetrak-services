import moment from 'moment';
import GiftRedemptionCodeModel from './GiftRedemptionCodeModel';
import { updateCharge } from '../../../external/stripe';
import { trackGiftCodeRedeemed } from '../../../common/trackEvent';

export const createGiftRedemptionCode = async data => {
  try {
    const redemptioncode = new GiftRedemptionCodeModel(data);
    await redemptioncode.save();
    return redemptioncode;
  } catch (error) {
    throw new Error('Error creating gift redemption code');
  }
};

export const getGiftRedemptionCode = code =>
  GiftRedemptionCodeModel.findOne({ redeemCode: code }).populate('giftProduct');

export const updateGiftRedemptionCode = async details => {
  try {
    const { userId, id, creditApplied, subscription, code } = details;

    const giftRedemptionCode = await GiftRedemptionCodeModel.findOne({ _id: id });
    const redeemDate = moment().toDate();

    giftRedemptionCode.redeemed = true;
    giftRedemptionCode.user = userId;
    giftRedemptionCode.redeemDate = redeemDate;
    await giftRedemptionCode.save();

    const { chargeId } = giftRedemptionCode;
    if (chargeId) {
      await updateCharge(chargeId, {
        redeemedBy: userId.toString(),
        redeemDate,
        creditApplied,
      });
    }

    trackGiftCodeRedeemed({
      userId,
      subscription,
      code,
    });
  } catch (error) {
    throw new Error('Could not update GiftRedemptionCode');
  }
};
