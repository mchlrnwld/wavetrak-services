import mongoose, { Schema } from 'mongoose';

const options = {
  collection: process.env.MONGO_USER_ACCOUNTS_COLLECTION || 'UserAccounts',
  timestamps: true,
};

export const UserAccountSchema = mongoose.Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
    subscriptions: [Schema.Types.Mixed],
    archivedSubscriptions: [Schema.Types.Mixed],
    receipts: [Schema.Types.Mixed],
    entitlements: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Entitlement',
      },
    ],
    winbacks: [Schema.Types.Mixed],
    stripeCustomerId: String,
    premiumPending: [
      {
        type: Schema.Types.String,
      },
    ],
    paymentWarnings: [
      {
        subscriptionId: Schema.Types.String,
        cardId: Schema.Types.String,
        planId: Schema.Types.String,
        type: { type: Schema.Types.String },
        status: Schema.Types.String,
      },
    ],
  },
  options,
);

UserAccountSchema.index({ user: 1 });
UserAccountSchema.index({ stripeCustomerId: 1 }, { sparse: true });
UserAccountSchema.index(
  {
    'subscriptions.0.subscriptionId': 1,
  },
  { sparse: true },
);
UserAccountSchema.index(
  {
    'subscriptions.subscriptionId': 1.0,
  },
  { sparse: true },
);

export default mongoose.model('UserAccount', UserAccountSchema);
