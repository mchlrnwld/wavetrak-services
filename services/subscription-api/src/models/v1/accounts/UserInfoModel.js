import mongoose, { Schema } from 'mongoose';

const options = {
  collection: process.env.MONGO_USER_INFO_COLLECTION || 'UserInfo',
};

const UserInfoSchema = Schema(
  {
    firstName: { type: String },
    lastName: { type: String },
    email: { type: String },
  },
  options,
);

export default mongoose.model('UserInfo', UserInfoSchema);
