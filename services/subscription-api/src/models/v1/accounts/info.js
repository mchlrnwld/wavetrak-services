import UserInfoModel from './UserInfoModel';

export default userId => UserInfoModel.findById(userId);
