import _ from 'lodash';
import logger from '../../../common/logger';
import * as stripe from '../../../external/stripe';
import * as analytics from '../../../common/analytics';
import UserAccountModel from './UserAccountModel';

const log = logger('subscription-service');

/* Generates a new winback  */
export const generateWinback = (percentOff, accountId, couponRef) => {
  const winback = {
    code: `winback-${percentOff}-${accountId}-${Math.floor(Math.random() * 100) + 1}`,
    type: 'winback',
    ref: couponRef,
    percent_off: percentOff,
  };

  return winback;
};

/* Generates initial account winbacks */
export const generateAccountWinbacks = account => {
  const { _id: accountId } = account;
  account.winbacks = [
    /* eslint no-param-reassign: 0 */
    generateWinback(10, accountId, 'WB-10-OFF'),
    generateWinback(20, accountId, 'WB-20-OFF'),
    generateWinback(50, accountId, 'WB-50-OFF'),
  ];

  return account;
};

/**
 * Returns the winback object for a given winback code
 * @param userId - users account id
 * @param winbackCode - winback coupon code (string)
 * @returns {Promise} account winbacks object
 */
export const retrieveWinback = (userId, winbackCode) =>
  new Promise(async (resolve, reject) => {
    try {
      const account = await UserAccountModel.findOne({ user: userId });
      let winbackObj;
      account.winbacks.forEach(winback => {
        if (winback.code === winbackCode) {
          winbackObj = winback;
        }
      });
      return resolve(winbackObj);
    } catch (err) {
      return reject(err);
    }
  });

/**
 * Adds a winback to a users account
 * @param userId - users account id
 * @param winbackObj - winback object
 * @returns {Promise} account winbacks array
 */
export const addWinback = (userId, winbackObj) =>
  new Promise(async (resolve, reject) => {
    try {
      const account = await UserAccountModel.findOne({ user: userId });
      const { _id: accountId } = account;
      const winback = generateWinback(winbackObj.percent_off, accountId, winbackObj.ref);
      account.winbacks.push(winback);
      await account.save();
      const analyticsKey = `subscription.availablediscount.${winback.percent_off}off`;
      analytics.identifyAll({
        userId: userId.toString(),
        [analyticsKey]: winback.code,
      });
      return resolve(account.winbacks);
    } catch (err) {
      return reject(err);
    }
  });

/**
 * Removes a winback from a users account
 * @param userId - users account id
 * @param winbackCode - winback coupon code (string)
 * @returns {Promise} account winbacks array
 */
export const removeWinback = (userId, winbackCode) =>
  new Promise(async (resolve, reject) => {
    try {
      const account = await UserAccountModel.findOne({ user: userId });
      account.winbacks.forEach((winback, index) => {
        if (winback.code === winbackCode) {
          account.winbacks.splice(index, 1);
        }
      });
      await account.save();
      return resolve(account.winbacks);
    } catch (err) {
      return reject(err);
    }
  });

/**
 * Create a new user account in mongo and stripe.
 *
 * @param userId reference the account with a userId
 * @returns {Promise} the new UserAccount object
 */
export const createAccount = userId =>
  new Promise(async (resolve, reject) => {
    let account = new UserAccountModel({ user: userId });
    try {
      account = generateAccountWinbacks(account);
      await account.save();
      log.info({
        userId,
        message: `Account created for ${userId} in database`,
      });
      analytics.identifyAll({
        userId: userId.toString(),
        traits: {
          'subscription.availablediscount.10off': account.winbacks[0].code,
          'subscription.availablediscount.20off': account.winbacks[1].code,
          'subscription.availablediscount.50off': account.winbacks[2].code,
        },
      });
      return resolve(account);
    } catch (err) {
      log.err(err);
      return reject(err);
    }
  });

/**
 * Gets a user's account information.
 *
 * @param userId      UserInfo id
 * @returns {Promise} mongo UserAccount object
 */
export const getAccount = userId =>
  new Promise(async (resolve, reject) => {
    try {
      const account = await UserAccountModel.findOne({ user: userId });
      return resolve(account);
    } catch (err) {
      return reject(err);
    }
  });

/**
 * Get's a user's account information with User and
 *
 * @params userId  UserInfo id
 * @returns {Promise} mongo UserAccount object.
 */
export const getAccountVerbose = userId =>
  new Promise(async (resolve, reject) => {
    try {
      const account = await UserAccountModel.findOne({ user: userId }).populate('user');
      return resolve(account);
    } catch (err) {
      return reject(err);
    }
  });

/**
 * Finds a UserAccount from stripeCustomerId
 *
 * @param stripeCustomerId
 * @returns mongo UserAccount model
 */
export const getAccountFromStripe = stripeCustomerId =>
  UserAccountModel.findOne({ stripeCustomerId });

export const updateDefaultStripeSource = async (userId, stripeSource) => {
  const account = await getAccount(userId);
  const card = await stripe.getCard(account.stripeCustomerId, stripeSource);
  if (card) {
    await stripe.updateCustomer(account.stripeCustomerId, {
      default_source: stripeSource,
    });
  }
};

/**
 * Returns a list of UserInfo ids that all use In-App Purchased subscriptions
 *
 * Intended to be used by a scheduled job
 * in ../external/iapReceiptUpdateRunner to verify every IAP receipt
 *
 * @returns {Promise} Resolves as an array of UserInfo ids
 */
export const getIapUserInfoIds = () =>
  new Promise(async (resolve, reject) => {
    try {
      const accounts = await UserAccountModel.find(
        { 'receipts.0': { $exists: true } },
        { user: 1 },
      );
      return resolve(
        _.map(accounts, account => {
          if (account.user) {
            return account.user;
          }
          return undefined;
        }),
      );
    } catch (err) {
      return reject(err);
    }
  });

/**
 * Given a stripe subscription id, return the corresponding account.
 *
 * @param subscriptionId  stripe subscription id
 * @param customerId      stripe customer id
 * @returns mongo UserAccount model
 */
export const getAccountFromSubscription = async (subscriptionId, customerId) => {
  const result = await UserAccountModel.findOne({
    'subscriptions.subscriptionId': subscriptionId,
  });

  if (result && result.stripeCustomerId === customerId) {
    return result;
  }

  throw new Error(
    'A UserAccount with the provided customer and subscription id could not be found.',
  );
};

/**
 * Given a userAccount, payment warning and status, updates the userAccount with the new status
 *
 * @param userAccount  user account
 * @param item      payment warning to be updated
 * @param status    status to be updated
 */
export const updatePaymentWarning = (userAccount, item, status) => {
  const warning = item;
  userAccount.paymentWarnings.pull(warning);
  delete warning.status;
  const updatedWarning = {
    ...warning.toObject(),
    status,
  };
  userAccount.paymentWarnings.push(updatedWarning);
};

export const getv1SubscriptionFromUserIdAndBrand = async (userId, brand) => {
  const account = await UserAccountModel.findOne({ user: userId });
  if (!account) return null;
  const { stripeCustomerId } = account;
  const subscription = account.subscriptions.find(
    ({ entitlement }) => entitlement === `${brand}_premium`,
  );
  if (subscription) {
    subscription.stripeCustomerId = stripeCustomerId;
    subscription.user = userId;
    subscription.isV1 = true;
  }
  return subscription;
};

export const updateRedeemCodeForV1Subscription = async (userId, subscriptionId, redeemCode) => {
  const account = await UserAccountModel.findOne({ user: userId });
  const subscription = account.subscriptions.find(sub => sub.subscriptionId === subscriptionId);
  if (subscription.redeemCodes) {
    subscription.redeemCodes.push(redeemCode);
  } else {
    subscription.redeemCodes = [redeemCode];
  }
  account.markModified('subscriptions');
  await account.save();
  return subscription;
};
