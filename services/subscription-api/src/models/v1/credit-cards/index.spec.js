import chai, { expect } from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import creditCard, { validateCard } from './index';

chai.should();
chai.use(sinonChai);

describe('Models / CreditCard', () => {
  let getCreditCardStub;
  let addCreditCardStub;
  const userId = '1234';
  const fingerprint = 'xyz';
  const cardId = 'card_1Cq4oSLwFGcBvXawORUb4Sl2';

  beforeEach(() => {
    getCreditCardStub = sinon.stub();
    addCreditCardStub = sinon.stub();
    creditCard.__Rewire__('getCreditCard', getCreditCardStub); // eslint-disable-line no-underscore-dangle
    creditCard.__Rewire__('addCreditCard', addCreditCardStub); // eslint-disable-line no-underscore-dangle
  });

  afterEach(() => {
    getCreditCardStub.reset();
    addCreditCardStub.reset();
    creditCard.__ResetDependency__('getCreditCard'); // eslint-disable-line no-underscore-dangle
    creditCard.__ResetDependency__('addCreditCard'); // eslint-disable-line no-underscore-dangle
  });

  it('Validates and adds a card if it has not been used before', async () => {
    const brand = 'sl';
    const cardObject = {
      brands: ['sl'],
      userIds: ['1234'],
      fingerprint,
      cardIds: [cardId],
    };

    getCreditCardStub.resolves(null);
    addCreditCardStub.resolves(cardObject);

    const validate = await validateCard({
      userId,
      brand,
      fingerprint,
      cardId,
    });
    expect(validate).to.deep.equal(cardObject);
    expect(addCreditCardStub.calledOnce).to.equal(true);
  });

  it('Throws an error if a card has already been used for the given brand', async () => {
    const brand = 'sl';
    const cardObject = {
      brands: ['sl'],
      userIds: ['1234'],
      fingerprint,
      cardIds: [cardId],
    };

    getCreditCardStub.resolves(cardObject);
    try {
      await validateCard({
        userId,
        brand,
        fingerprint,
        cardId,
      });
    } catch (error) {
      expect(error.toJSON().code).to.equal(1001);
      expect(error.statusCode).to.equal(400);
      expect(error.name).to.equal('DuplicateCardError');
      expect(error.message).to.equal(
        'This card has already been used for a free trial on this brand.',
      );
    }
  });

  it('Adds the brand if the card has not been used for that brand', async () => {
    const brand = 'bw';

    const cardObject = {
      brands: ['sl'],
      userIds: ['1234'],
      fingerprint,
      cardIds: [cardId],
      markModified: () => null,
      save() {
        return this;
      },
    };

    getCreditCardStub.resolves(cardObject);

    const validate = await validateCard({
      userId,
      brand,
      fingerprint,
      cardId,
    });
    expect(validate.brands).to.deep.equal(['sl', 'bw']);
    expect(validate.userIds).to.deep.equal(['1234']);
  });

  it('Adds the brand and userId if the card has not been used for that brand and user Id', async () => {
    const brand = 'fs';
    const cardObject = {
      brands: ['sl'],
      userIds: ['1234'],
      fingerprint,
      cardIds: [cardId],
      markModified: () => null,
      save() {
        return this;
      },
    };

    getCreditCardStub.resolves(cardObject);

    const validate = await validateCard({
      userId: '12345',
      brand,
      fingerprint,
      cardId,
    });

    expect(validate.brands).to.deep.equal(['sl', 'fs']);
    expect(validate.userIds).to.deep.equal(['1234', '12345']);
  });

  it('Adds the cardId if a new cardId is used', async () => {
    const brand = 'fs';
    const newCardId = ' card_11111111';
    const cardObject = {
      brands: ['sl'],
      userIds: ['1234'],
      fingerprint,
      cardIds: [cardId],
      markModified: () => null,
      save() {
        return this;
      },
    };

    getCreditCardStub.resolves(cardObject);

    const validate = await validateCard({
      userId: '12345',
      brand,
      fingerprint,
      cardId: newCardId,
    });

    expect(validate.brands).to.deep.equal(['sl', 'fs']);
    expect(validate.userIds).to.deep.equal(['1234', '12345']);
    expect(validate.cardIds).to.deep.equal([cardId, newCardId]);
  });
});
