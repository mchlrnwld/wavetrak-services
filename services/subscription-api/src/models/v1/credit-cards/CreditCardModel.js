import mongoose, { Schema } from 'mongoose';

const options = {
  collection: process.env.MONGO_CREDIT_CARD_COLLECTION || 'CreditCard',
  timestamps: true,
};

export const CreditCardSchema = mongoose.Schema(
  {
    userIds: [
      {
        type: Schema.Types.ObjectId,
        ref: 'UserInfo',
      },
    ],
    fingerprint: { type: [String] },
    brands: [{ type: String, enum: ['sl', 'bw', 'fs'] }],
    cardIds: [{ type: String }],
  },
  options,
);

CreditCardSchema.index({ fingerprint: 1 });

export default mongoose.model('CreditCard', CreditCardSchema);
