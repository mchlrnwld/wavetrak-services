import CreditCardModel from './CreditCardModel';
import DuplicateCardError from '../../../errors/DuplicateCardError';

/**
 * Gets a credit card with a given fingerprint.
 *
 * @param fingerprint the credit card fingerprint.
 * @returns {Promise} the mongo `CreditCard` cursor object.
 */
export const getCreditCard = fingerprint => CreditCardModel.findOne({ fingerprint });

/**
 * Creates a new credit card in the credit card collection
 *
 * @param creditCard The credit card info object which contains the following:
 *  fingerprint, brand, lastFound, userId
 * @returns {CreditCardModel}
 */
export const addCreditCard = ({ userId, brand, fingerprint, cardId }) =>
  new CreditCardModel({
    fingerprint,
    brands: [brand],
    userIds: [userId],
    cardIds: [cardId],
  }).save();

/**
 * Updates a card to add a new user for the card
 *
 * @param {ObjectId} userId
 * @param {String} brand
 *
 * @returns {CreditCardModel}
 */
export const updateCard = async ({ userId, brand, fingerprint, cardId }) => {
  const creditCard = await getCreditCard(fingerprint);

  // Add the user ID if it isn't already present for the given card
  const hasUserId = creditCard.userIds.find(id => id.toString() === userId.toString());
  if (!hasUserId) {
    creditCard.userIds = [...creditCard.userIds, userId];
    creditCard.markModified('userIds');
  }

  // Add the card ID if it isn't already present for the given card
  const hasCardId = creditCard.cardIds.find(id => id.toString() === cardId.toString());
  if (!hasCardId) {
    creditCard.cardIds = [...creditCard.cardIds, cardId];
    creditCard.markModified('cardIds');
  }

  // Add the brand the card is being used for
  creditCard.brands = [...creditCard.brands, brand];
  creditCard.markModified('brands');

  return creditCard.save();
};

/**
 * Validates whether a card is eligible to be used for a free trial for the specific brand.
 *
 * If the card does not exist it will create it and then return true
 *
 * If the card does exist and is eligible it will update the card with the brand and user Id
 * and then return true
 *
 * @param {ObjectId} userId
 * @param {String} brand
 * @param {String} fingerprint
 *
 * @returns {CreditCardModel}
 */
export const validateCard = async ({ userId, brand, fingerprint, cardId }) => {
  const card = await getCreditCard(fingerprint);

  if (!card) {
    return addCreditCard({
      userId,
      brand,
      fingerprint,
      cardId,
    });
  }

  const hasBeenUsedForBrand = !!card.brands.find(usedBrand => usedBrand === brand);
  const shouldValidate = process.env.VALIDATE_CARDS !== 'OFF';
  if (hasBeenUsedForBrand) {
    if (shouldValidate) {
      const code = 1001;
      const statusCode = 400;
      throw new DuplicateCardError(code, statusCode);
    }
    return true;
  }

  return updateCard({
    userId,
    brand,
    fingerprint,
    cardId,
  });
};
