import { getAccount, createAccount } from '../accounts';

/**
 * @description Endpoint for indicating the IAP subscription request.
 * A premiumPending status is implied when a user initiates a purchase
 * request within a native application.  If any step in the native purchase
 * reuqest chain is interupted, if this indicator exists, the Apps will
 * prompt a user to retry (restore) their subscription purchase.
 * @param {string} userId User ID to fetch receipts for
 * @param {string} brand Brand the purchase was made for (Only needed for google)
 * @param {boolean} cancel Toggles the pendingPremium attr off
 */
const premiumPending = async ({ userId, brand, cancel }) => {
  try {
    if (!userId || !brand) {
      throw new Error('Cannot process request: Invalid parameters');
    }

    let account;
    account = await getAccount(userId);
    if (!account) {
      account = await createAccount(userId);
    }

    account.premiumPending.addToSet(`${brand}_premium`);
    if (cancel) {
      account.premiumPending.pull(`${brand}_premium`);
    }

    account.markModified('premiumPending');
    await account.save();
    return account;
  } catch (err) {
    const message = 'There was a problem intiating premium pending';
    throw new Error(message);
  }
};

export default premiumPending;
