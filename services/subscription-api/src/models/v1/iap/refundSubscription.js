import * as account from '../accounts';
import archiveSubscription from './archiveSubscription';

/**
 * Archives a subscription for a user in a "Refund" scenario
 *
 * Important Note:  This does not remove the receipt associated with the subscription.
 *                  If you don't refund users through the Play Store,
 *                  the subscription can re-appear during nightly jobs.
 *
 * @param userInfoId
 * @param subscriptionId
 * @param deviceType
 * @returns {Promise} UserAccount, modified
 */
const refundSubscription = async (userInfoId, subscriptionId, deviceType, log) => {
  const userAccount = await account.getAccount(userInfoId);
  if (!userAccount) {
    log.error(`Tried to refund missing user ${userInfoId}`);
    throw new Error('User not found');
  }

  const subscriptionMatches = userAccount.subscriptions.filter(
    sub => sub.subscriptionId === subscriptionId && sub.type === deviceType,
  );

  // Should only be one, but let's be safe here
  for (const subscription of subscriptionMatches) {
    await archiveSubscription(userAccount, subscription);
    await userAccount.save();
  }

  return userAccount;
};

export default refundSubscription;
