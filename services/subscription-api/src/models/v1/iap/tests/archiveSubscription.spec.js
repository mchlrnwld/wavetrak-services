import { expect } from 'chai';
import sinon from 'sinon';
import archiveSubscription from '../archiveSubscription';
import userAccountFixture from './fixtures/userAccount';
import iapSubscriptionFixture from './fixtures/iapSubscription';

describe('iap archiveSubscriptions', () => {
  let userAccount;

  let clock;
  const now = 1512115200; // 12/01/2017@00:00PST

  before(() => {
    clock = sinon.useFakeTimers(now * 1000);
  });

  after(() => {
    clock.restore();
  });

  beforeEach(() => {
    userAccount = userAccountFixture();
  });

  afterEach(() => {
    userAccount = userAccountFixture();
  });

  it('removes the subscriptions from the subscriptions array', async () => {
    await archiveSubscription(userAccount, iapSubscriptionFixture);
    expect(userAccount.subscriptions).to.not.include(iapSubscriptionFixture);
  });

  it('adds the subscription to the archivedSubscriptions array', async () => {
    await archiveSubscription(userAccount, iapSubscriptionFixture);
    expect(userAccount.archivedSubscriptions).to.deep.include({
      ...iapSubscriptionFixture,
      archivedDate: new Date(now * 1000),
    });
  });
});
