import { expect } from 'chai';
import moment from 'moment';
import sinon from 'sinon';
import * as utils from '../utils';

describe('iap utils', () => {
  let clock;
  const now = 1512115200; // 12/01/2017@00:00PST

  before(() => {
    clock = sinon.useFakeTimers(now * 1000);
  });

  after(() => {
    clock.restore();
  });

  const purchaseItem = {
    quantity: 1,
    productId: 'surfline.iosapp.1month.subscription.notrial',
    transactionId: '80000366075000',
    originalTransactionId: '80000310354662',
    purchaseDate: '2017-09-14 03:32:29 Etc/GMT',
    purchaseDateMs: 1505433602000,
    purchaseDatePst: '2017-09-13 20:32:29 America/Los_Angeles',
    originalPurchaseDate: '2017-03-14 03:32:31 Etc/GMT',
    originalPurchaseDateMs: 1489462351000,
    originalPurchaseDatePst: '2017-03-13 20:32:31 America/Los_Angeles',
    expiresDate: '2017-10-14 03:32:29 Etc/GMT',
    expiresDateMs: 1507951949000,
    expiresDatePst: '2017-10-13 20:32:29 America/Los_Angeles',
    webOrderLineItemId: 80000072371474,
    isTrialPeriod: 'false',
    bundleId: 'com.surfline.surfapp',
    expirationDate: 1539846000000,
  };

  const legacyPurchaseItem = {
    quantity: 1,
    productId: 'FS365APP',
    transactionId: '400000341241603',
    originalTransactionId: '400000341241603',
    purchaseDate: '2017-10-05 07:42:53 Etc/GMT',
    purchaseDateMs: 1509406029000,
    purchaseDatePst: '2017-10-05 00:42:53 America/Los_Angeles',
    originalPurchaseDate: '2017-10-05 07:42:53 Etc/GMT',
    originalPurchaseDateMs: 1507189373000,
    originalPurchaseDatePst: '2017-10-05 00:42:53 America/Los_Angeles',
    isTrialPeriod: 'false',
    bundleId: 'com.fishtrack.app',
    expirationDate: 0,
  };

  const purchaseData = [
    {
      type: 'apple',
      subscriptionId: '80000366075000',
      periodStart: new Date('Thu Sep 14 2017 16:00:02 GMT-0800 (PST)'),
      periodEnd: new Date('Wed Oct 18 2018 0:00:00 GMT-0700 (PDT)'),
      entitlement: 'sl_premium',
      planId: 'surfline.iosapp.1month.subscription.notrial',
      createdAt: new Date('Thu Sep 14 2017 16:00:02 GMT-0800 (PST)'),
      creationDate: new Date(now * 1000),
      expiring: false,
      failedPaymentAttempts: 0,
    },
  ];

  describe('getReceiptDate', () => {
    it('should return a moment object for a unix timestamp', () => {
      const receiptDate = 1507064194000;
      const date = utils.getReceiptDate(receiptDate);
      expect(date.toDate()).to.eql(moment(receiptDate).toDate());
    });

    it('should return a moment object for a string formatted timestamp', () => {
      const receiptDate = '1507064194000';
      const date = utils.getReceiptDate(receiptDate);
      expect(date.toDate()).to.eql(moment(parseInt(receiptDate, 10)).toDate());
    });

    it('should return a moment object if neither string nor number is passed', () => {
      const receiptDate = ['1507064194000'];
      const date = utils.getReceiptDate(receiptDate);
      expect(date.format('LLL')).to.eql(moment().format('LLL'));
    });
  });

  describe('calculatePurchaseExpiration', () => {
    it('should calculate expires date for autorenewing purchases', () => {
      const expirationDate = utils.calculatePurchaseExpiration(purchaseItem);
      expect(expirationDate).to.eql(moment(purchaseItem.expirationDate).toDate());
    });

    it('should calculate expires date for expired legacy one-time purchases', () => {
      const expirationDate = utils.calculatePurchaseExpiration(legacyPurchaseItem);
      expect(expirationDate).to.eql(
        moment(legacyPurchaseItem.purchaseDateMs)
          .add(365, 'days')
          .toDate(),
      );
    });
  });

  describe('isExpiring', () => {
    it('should not mark a purchase item as expiring', () => {
      const isExpiring = utils.isExpiring({ ...purchaseItem, expirationDate: 1539846000000 });
      expect(isExpiring).to.equal(false);
    });

    it('should mark a purchase item as expiring', () => {
      const newItem = { ...purchaseItem, expirationDate: 1506900429000 };
      const isExpiring = utils.isExpiring(newItem);
      expect(isExpiring).to.equal(true);
    });

    it('should not mark a legacy purchase item as expiring', () => {
      const isExpiring = utils.isExpiring(legacyPurchaseItem);
      expect(isExpiring).to.equal(false);
    });

    it('should mark a legacy purchase item as expiring', () => {
      const newItem = { ...legacyPurchaseItem, purchaseDateMs: 1475364429000 };
      const isExpiring = utils.isExpiring(newItem);
      expect(isExpiring).to.equal(true);
    });
  });

  describe('buildNewSubscription', () => {
    it('should build a subscription from a purchase item object', () => {
      const subscriptions = utils.buildNewSubscription(purchaseItem, 'apple');
      expect(subscriptions).to.eql(purchaseData[0]);
    });
  });

  describe('getSubsFromPurchaseData', () => {
    it('should build a subscriptions object from purchase Data', async () => {
      const mockLog = { error: () => {} };
      const subscription = await utils.getSubsFromPurchaseData('apple', [purchaseItem], mockLog);
      expect(subscription).to.be.an('object');
      expect(subscription.entitlement).to.eql(purchaseData[0].entitlement);
      expect(subscription.periodEnd).to.eql(purchaseData[0].periodEnd);
      expect(subscription.subscriptionId).to.eql(purchaseData[0].subscriptionId);
    });
  });
});
