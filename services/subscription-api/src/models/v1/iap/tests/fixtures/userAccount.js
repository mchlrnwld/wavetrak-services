export default () => ({
  _id: '59d3f86c65bd7c118c881b5e',
  updatedAt: '2017-10-05T21:37:11.953Z',
  createdAt: '2017-10-03T20:51:56.437Z',
  user: '59d3e823fd88312e35e87040',
  __v: 9,
  winbacks: [
    {
      code: 'winback-10-59d3f86c65bd7c118c881b5e-82',
      type: 'winback',
      ref: 'WB-10-OFF',
      percent_off: 10,
    },
    {
      code: 'winback-20-59d3f86c65bd7c118c881b5e-92',
      type: 'winback',
      ref: 'WB-20-OFF',
      percent_off: 20,
    },
    {
      code: 'winback-50-59d3f86c65bd7c118c881b5e-6',
      type: 'winback',
      ref: 'WB-50-OFF',
      percent_off: 50,
    },
  ],
  entitlements: ['574cc9c268387f052613a373', '582f8177b38c9bd9016574a3'],
  receipts: [
    {
      lastUpdated: '2017-10-05T21:37:11.945Z',
      purchases: [
        {
          expirationDate: 1507064194000,
          bundleId: 'com.surfline.surfapp',
          isTrialPeriod: 'false',
          webOrderLineItemId: 1000000036458445,
          expiresDatePst: '2017-10-03 13:56:34 America/Los_Angeles',
          expiresDateMs: 1507064194000,
          expiresDate: '2017-10-03 20:56:34 Etc/GMT',
          originalPurchaseDatePst: '2017-10-03 13:51:51 America/Los_Angeles',
          originalPurchaseDateMs: 1507063911000,
          originalPurchaseDate: '2017-10-03 20:51:51 Etc/GMT',
          purchaseDatePst: '2017-10-03 13:51:34 America/Los_Angeles',
          purchaseDateMs: 1507063894000,
          purchaseDate: '2017-10-03 20:51:34 Etc/GMT',
          originalTransactionId: '1000000340196961',
          transactionId: '1000000340196961',
          productId: 'surfline.iosapp.1month.subscription.notrial',
          quantity: 1,
        },
        {
          expirationDate: 1507064955000,
          bundleId: 'com.surfline.surfapp',
          isTrialPeriod: 'false',
          webOrderLineItemId: 1000000036458519,
          expiresDatePst: '2017-10-03 14:09:15 America/Los_Angeles',
          expiresDateMs: 1507064955000,
          expiresDate: '2017-10-03 21:09:15 Etc/GMT',
          originalPurchaseDatePst: '2017-10-03 13:51:51 America/Los_Angeles',
          originalPurchaseDateMs: 1507063911000,
          originalPurchaseDate: '2017-10-03 20:51:51 Etc/GMT',
          purchaseDatePst: '2017-10-03 14:04:15 America/Los_Angeles',
          purchaseDateMs: 1507064655000,
          purchaseDate: '2017-10-03 21:04:15 Etc/GMT',
          originalTransactionId: '1000000340196961',
          transactionId: '1000000340199002',
          productId: 'surfline.iosapp.1month.subscription.notrial',
          quantity: 1,
        },
        {
          expirationDate: 1507065698000,
          bundleId: 'com.surfline.surfapp',
          isTrialPeriod: 'false',
          webOrderLineItemId: 1000000036458615,
          expiresDatePst: '2017-10-03 14:21:38 America/Los_Angeles',
          expiresDateMs: 1507065698000,
          expiresDate: '2017-10-03 21:21:38 Etc/GMT',
          originalPurchaseDatePst: '2017-10-03 13:51:51 America/Los_Angeles',
          originalPurchaseDateMs: 1507063911000,
          originalPurchaseDate: '2017-10-03 20:51:51 Etc/GMT',
          purchaseDatePst: '2017-10-03 14:16:38 America/Los_Angeles',
          purchaseDateMs: 1507065398000,
          purchaseDate: '2017-10-03 21:16:38 Etc/GMT',
          originalTransactionId: '1000000340196961',
          transactionId: '1000000340201724',
          productId: 'surfline.iosapp.1month.subscription.notrial',
          quantity: 1,
        },
        {
          expirationDate: 1507070356000,
          bundleId: 'com.surfline.surfapp',
          isTrialPeriod: 'false',
          webOrderLineItemId: 1000000036458719,
          expiresDatePst: '2017-10-03 15:39:16 America/Los_Angeles',
          expiresDateMs: 1507070356000,
          expiresDate: '2017-10-03 22:39:16 Etc/GMT',
          originalPurchaseDatePst: '2017-10-03 13:51:51 America/Los_Angeles',
          originalPurchaseDateMs: 1507063911000,
          originalPurchaseDate: '2017-10-03 20:51:51 Etc/GMT',
          purchaseDatePst: '2017-10-03 15:34:16 America/Los_Angeles',
          purchaseDateMs: 1507070056000,
          purchaseDate: '2017-10-03 22:34:16 Etc/GMT',
          originalTransactionId: '1000000340196961',
          transactionId: '1000000340217446',
          productId: 'surfline.iosapp.1month.subscription.notrial',
          quantity: 1,
        },
      ],
      receiptData:
        'MIIUGwYJKoZIhvcNAQcCoIIUDDCCFAgCAQExCzAJBgUrDgMCGgUAMIIDvAYJKoZIhvcNAQcBoIIDrQSCA6kxggOlMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDwIBAQQDAgEAMAsCARACAQEEAwIBADALAgEZAgEBBAMCAQMwDAIBCgIBAQQEFgI0KzAMAgEOAgEBBAQCAgCMMA0CAQ0CAQEEBQIDAa2wMA0CARMCAQEEBQwDMS4wMA4CAQkCAQEEBgIEUDI0NzAWAgEDAgEBBA4MDDIwMTcwOTE4MTMxMzAYAgEEAgECBBC0PJbjMuGp1cEaJM1IQ3CuMBsCAQACAQEEEwwRUHJvZHVjdGlvblNhbmRib3gwHAIBBQIBAQQUWK1OVvReNJyJaSoNLUtzfubVEKEwHgIBAgIBAQQWDBRjb20uc3VyZmxpbmUuc3VyZmFwcDAeAgEMAgEBBBYWFDIwMTctMTAtMDNUMjA6NTE6NTZaMB4CARICAQEEFhYUMjAxMy0wOC0wMVQwNzowMDowMFowTQIBBwIBAQRFVlSHFV5vDIPThy2qh4M5cKPr6Ob3uvI+c0wBTaw8E9fPEo+GoGnJvRP+lXIx/ALulLqJkz7chJ5onzon95/ZZI80XkJ3MFYCAQYCAQEETsLgnJ/4lf54omnG0UREJqqS/UYlIqB/+4uB6B9DfA8+wf76IMTaGmBLSNfZTST+ZtyNH8bT0tVIf0yfIjnATmcV+ljyEvGyHAQYcLmPNjCCAYoCARECAQEEggGAMYIBfDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADASAgIGrwIBAQQJAgcDjX6m8s/NMBsCAganAgEBBBIMEDEwMDAwMDAzNDAxOTY5NjEwGwICBqkCAQEEEgwQMTAwMDAwMDM0MDE5Njk2MTAfAgIGqAIBAQQWFhQyMDE3LTEwLTAzVDIwOjUxOjM0WjAfAgIGqgIBAQQWFhQyMDE3LTEwLTAzVDIwOjUxOjUxWjAfAgIGrAIBAQQWFhQyMDE3LTEwLTAzVDIwOjU2OjM0WjA2AgIGpgIBAQQtDCtzdXJmbGluZS5pb3NhcHAuMW1vbnRoLnN1YnNjcmlwdGlvbi5ub3RyaWFsoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQBIdsQQjL6eItK5s0wdkmcLWMGmxMwb6LMH5VJfizhyHGk3wfGp1nXU/JhvHl9cHlzHTSckcY4vhoKX4d14Cg4iRHKe8lriFa+STvvFLsN3Wq0bxjhMEqIRXd6XqrlVMYlLTr2c8fFZmEO2gxqnvsLYwXiu9BrJchO1u3cAYllbvitARl22z1VVYAlatdSpMPTon0TwrwVgZMK+Vk12S94AhO3XLC6FVlJStBw23HVtIsfzfLOxdiCuC8jAjPH9y3kA/rLiI1L6g9C9NPow9QvZptXj83dgUU6juA/iQ4WeVhqDAYqL94f3/4zYfH0VYyFBx6fS/TdLInMzjJjyxifM',
      type: 'apple',
    },
  ],
  archivedSubscriptions: [
    {
      failedPaymentAttempts: 0,
      expiring: true,
      createdAt: '1970-01-01T00:00:02.017Z',
      applePlanId: 'surfline.iosapp.1month.subscription.notrial',
      entitlement: 'sl_premium',
      periodEnd: '2017-10-03T20:56:34.000Z',
      periodStart: '1970-01-01T00:00:02.017Z',
      subscriptionId: '1000000340196961',
      type: 'apple',
    },
  ],
  subscriptions: [
    {
      failedPaymentAttempts: 0,
      expiring: true,
      createdAt: '1970-01-01T00:00:02.017Z',
      applePlanId: 'surfline.iosapp.1month.subscription.notrial',
      entitlement: 'sl_premium',
      periodEnd: '2017-10-03T22:39:16.000Z',
      periodStart: '1970-01-01T00:00:02.017Z',
      subscriptionId:
        'bnjklbcnboceenjbfndcfbja.AO-J1OxBp8ddF3i6AWdzK4Dsi2MeSmOIB4jKCVhDl4rIBsrC-TtYwEQ1r2v1i6U5TCOxyGNqbV-KhvawuxaFfISH0TTvwmAMfXDWE9W8PKiRdx77GNMTArNcEl1awR1ZXlCv6OFsxPuYXHkryFa9trPybK6R2V75v5lb7cSq2q0Ycwy0GUVcH6g',
      type: 'google',
    },
  ],
  markModified: () => true,
  save: () => true,
});
