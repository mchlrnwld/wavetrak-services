/* import { expect } from 'chai';
import sinon from 'sinon';
import verifyUserAccountReceipt from '../verifyUserAccountReceipts';
import * as account from '../../accounts';
import logger from '../../../common/logger';
import userAccount from './fixtures/userAccount';

describe('verifyUserAccountReceipt', () => {
  let accountStub;
  let accountPromise;
  let loggerStub;
  beforeEach(() => {
    accountStub = sinon.stub(account, 'getAccount');
    accountPromise = accountStub;
    loggerStub = sinon.stub(logger, 'logger');
  });

  afterEach(() => {
    accountStub.restore();
    accountPromise.restore();
    loggerStub.restore();
  });

  it('should archived a subscription if the receipt purchase is expired', async () => {
    accountPromise.resolves(userAccount);
    const testUserAccount = userAccount;
    console.log(testUserAccount);
    expect(testUserAccount.subscriptions).to.have.lengthOf(1);
    expect(testUserAccount.archivedSubscriptions).to.have.lengthOf(1);

    const verifiedUserAccount = await verifyUserAccountReceipt(userAccount.user);
    expect(verifiedUserAccount.subscriptions).to.have.lengthOf(0);
    expect(verifiedUserAccount.archivedSubscriptions).to.have.lengthOf(2);
  });
});
 */
