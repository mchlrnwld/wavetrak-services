import * as iap from 'in-app-purchase';
import moment from 'moment';
// eslint-disable-next-line import/no-cycle
import archiveSubscription from './archiveSubscription';

export const deviceTypeMap = {
  apple: iap.APPLE,
  google: iap.GOOGLE,
};

export const subscriptionMap = {
  // the 'notrial' suffix is a legacy hack to address a problem in the
  // first release of IAP in iOS. Don't reuse. These will be deprecated at a later date.
  'surfline.iosapp.1month.subscription.notrial': 'sl_premium',
  'surfline.iosapp.1year.subscription.notrial': 'sl_premium',
  // for some reason this 'notrial' suffix was grandfathered into the first release of IAP
  // in Android. Again, DO NOT reuse. These will be deprecated at a later date as well.
  'surfline.android.1month.subscription.notrial': 'sl_premium',
  'surfline.android.1year.subscription.notrial': 'sl_premium',
  'surfline.android.monthly.subscription.notrial': 'sl_premium',
  'surfline.android.yearly.subscription.notrial': 'sl_premium',
  'surfline.android.annual.subscription.notrial': 'sl_premium',
  // auto renewing subscriptions
  'surfline.iosapp.1month.subscription': 'sl_premium',
  'surfline.iosapp.1year.subscription': 'sl_premium',
  'buoyweather.iosapp.1month.subscription': 'bw_premium',
  'buoyweather.iosapp.1year.subscription': 'bw_premium',
  'buoyweather.android.1month.subscription': 'bw_premium',
  'buoyweather.android.1year.subscription': 'bw_premium',
  'fishtrack.iosapp.1month.subscription': 'fs_premium',
  'fishtrack.iosapp.1year.subscription': 'fs_premium',
  'fs.iosapp.1month.subscription': 'fs_premium',
  'fs.iosapp.1year.subscription': 'fs_premium',
  'fishtrack.android.1month.subscription': 'fs_premium',
  'fishtrack.android.1year.subscription': 'fs_premium',
  'fishtrack.android.yearly.subscription.notrial': 'fs_premium',
  // legacy non-renewing subscriptions for backward compatibility
  FS30APP: 'fs_premium',
  FS365APP: 'fs_premium',
  BW30APP: 'bw_premium',
  BW365APP: 'bw_premium',
};

export const expDateMap = {
  FS30APP: 30,
  FS365APP: 365,
  BW30APP: 30,
  BW365APP: 365,
};

/**
 * Dates can be strings coming back from In-app-purchase library
 *
 * @param receiptDateFormat
 * @returns {Moment}
 */
export const getReceiptDate = receiptDate => {
  const isMillisecondString = /^\d+$/.test(receiptDate);

  if (typeof receiptDate === 'string' && isMillisecondString) {
    return moment(parseInt(receiptDate, 10));
  }
  if (typeof receiptDate === 'number') return moment(receiptDate);

  /*
    If the user has refunded, then apple gives us a cancellationDate as the
    expirationDate. It comes in the format of a date string like so:
        2018-04-11 22:19:57 Etc/GMT
    when moment parses this string we have to explicitly declare it is UTC
    since it is not a unix timestamp
  */
  if (!isMillisecondString) return moment.utc(receiptDate, 'YYYY-MM-DD HH:mm');

  return moment();
};

/**
 * Check for one-time inapp purchase types and calculates expiration date based on productId
 *
 * @param purchaseItem
 * @returns {object}
 */
export const calculatePurchaseExpiration = purchaseItem => {
  const expDateExtend = expDateMap[purchaseItem.productId];
  let expirationDate;
  if (expDateExtend) {
    expirationDate = getReceiptDate(purchaseItem.purchaseDateMs).add(expDateExtend, 'days');
  } else {
    /*
      The in-app-purchase library has a bug: https://github.com/voltrue2/in-app-purchase/issues/137
      which causes cancelling a subscription to overwrite the expiration date
      for this reason we have to use purchaseItem.expiresDate for apple
      The bug was fixed for google so purchaseItem.expirationDate is correct
    */
    expirationDate = getReceiptDate(purchaseItem.expirationDate);
  }

  return expirationDate.toDate();
};

/**
 *
 * @param purchaseItem
 * @returns {Boolean} Is the purchase item cancelled or expiring today?
 */
export const isExpiring = purchaseItem => calculatePurchaseExpiration(purchaseItem) <= moment();

/**
 * Build a new subscription object from a purchaseItem and deviceType
 *
 * @param purchaseItem
 * @param deviceType
 * @returns {object} {{
 *  type: *,
 *  subscriptionId: string,
 *  periodStart: (string|string),
 *  periodEnd: (string|string),
 *  entitlement: string,
 *  createdAt: Date,
 *  expiring: boolean,
 *  failedPaymentAttempts: number
 * }}
 */
export const buildNewSubscription = (purchaseItem, deviceType) => {
  const entitlement = subscriptionMap[purchaseItem.productId];
  if (!entitlement) {
    return null;
  }
  const purchaseDate =
    deviceType === 'apple' ? purchaseItem.purchaseDateMs : purchaseItem.purchaseDate;

  const purchaseData = {
    type: deviceType,
    subscriptionId: purchaseItem.transactionId,
    periodStart: getReceiptDate(purchaseDate).toDate(),
    periodEnd: calculatePurchaseExpiration(purchaseItem),
    entitlement,
    planId: purchaseItem.productId,
    createdAt: getReceiptDate(purchaseDate).toDate(),
    creationDate: new Date(),
    expiring: isExpiring(purchaseItem),
    failedPaymentAttempts: 0,
  };

  return purchaseData;
};

/**
 * Returns subscriptions from purchaseData
 *
 * @param deviceType
 * @param purchaseData
 * @returns {Array}
 */
export const getSubsFromPurchaseData = async (deviceType, purchaseData, log, newrelic) => {
  // Sort the subscriptions by their periodEnd date
  const purchases = [...purchaseData];
  // Use getReceiptDate to return a proper moment regardless of format
  const sortedPurchases = purchases.sort((purchase1, purchase2) =>
    getReceiptDate(purchase2.purchaseDate).diff(getReceiptDate(purchase1.purchaseDate)),
  );

  // Now create
  const purchase = sortedPurchases[0];
  const sub = buildNewSubscription(purchase, deviceType);

  if (sub) {
    return sub;
  }
  log.error({
    message: 'Malformed purchase data',
    purchase,
  });
  if (newrelic) newrelic.addCustomAttribute('iapSubscriptionMissing', true);
  throw new Error({ error: 'Error returned from getSubsFromPurchaseData' });
};

/**
 * Checks whether purchaseData has changed
 *
 * @param purchaseData1
 * @param purchaseData2
 * @returns {boolean}
 */
export const hasPurchaseDataChanged = (purchaseData1, purchaseData2) => {
  if (!purchaseData1 || !purchaseData2) {
    return true;
  }
  return purchaseData1.expirationDate !== purchaseData2.expirationDate;
};

/**
 * Parses the google receipt data to retrieve a brand for the receipt
 *
 * @param {object} receipt receipt object
 * @param {string} receipt.type 'apple' or 'google'
 * @param {array} receipt.purchaseData Array with data for purchases
 * @param {string} receipt.purchaseData.productId string with relevant brand info
 */
export const getBrand = receipt => {
  const hasPurchases =
    (receipt.purchaseData && receipt.purchaseData.length > 0) ||
    (receipt.purchases && receipt.purchases.length > 0);

  if (!hasPurchases) {
    throw new Error('No purchase data found');
  }

  const purchases = receipt.purchaseData || receipt.purchases;
  const brand = subscriptionMap[purchases[0].productId];

  if (brand && brand.indexOf('sl') > -1) return 'sl';
  if (brand && brand.indexOf('bw') > -1) return 'sl';
  if (brand && brand.indexOf('fs') > -1) return 'sl';
  return null;
};

/**
 * @description Helper function that returns a filter function used with .filter()
 * to match subscriptions based on plan ID
 *
 * @param {object} subscription
 * @param {string} subscription.planId - A plan ID
 * @param {string} subscription.applePlanId - DEPRECATED - some old subs have applePlanId
 *
 * @returns {function} filtering function
 */
export const filterByPlanId = subscription => sub => {
  /**
   * Since apple purchases have different subscription ID's
   * we rely on the applePlanId or planId to filter the subscriptions.
   * A user should theoretically only have one subscription for each product
   * so this will be a safe way to make sure we don't create extra subs.
   */
  const planId = subscriptionMap[sub.planId];
  const applePlanId = subscriptionMap[sub.applePlanId];
  const receiptPlanId = subscriptionMap[subscription.planId];

  return planId === receiptPlanId || applePlanId === receiptPlanId;
};

/**
 * @description This function takes the user account and a subscription object
 * created from the purchaseData for a given receipt on the user account. This subscription
 * object will represent the data for the most up to date purchase made on this receipt.
 * This function will take this subscription object and do any of the following:
 *  - Update the period start and end dates if the subscription is renewing
 *  - Archive and remove the subscription if it is outdated or expired
 *  - Create a new subscription if the receipt is new and the user doesn't have a subscription
 * @param {object} userAccount - userAccount document returned from mongo
 * @param {object} purchaseData - purchaseData for the receipt
 * @param {object} receiptSubscription - object created for the receipt being checked
 * @param {Array<string>} actionsToLog - object created for the receipt being checked
 */
export const syncReceiptSubscriptionToAccount = async (
  userAccount,
  purchaseData,
  receiptSubscription,
  actionsToLog = [],
) => {
  // Find the updated subscription in the list of subscriptions to check
  const userSubscriptions = userAccount.subscriptions.filter(filterByPlanId(receiptSubscription));

  const subscriptionExpiring =
    receiptSubscription.expiring || purchaseData.receipt.cancellation_date;

  const hasSubscription = userSubscriptions && userSubscriptions.length > 0;
  const shouldArchive = subscriptionExpiring && hasSubscription;

  // If the subscription is expiring or if the receipt is cancelled
  if (shouldArchive) {
    await archiveSubscription(userAccount, receiptSubscription, actionsToLog);
    // userAccount.markModified('archivedSubscriptions');
    actionsToLog.push(
      `Archived subs were modified. New length = ${userAccount.archivedSubscriptions.length}`,
    );
  } else if (hasSubscription) {
    /**
     * Since we are renewing/updating a currently active subscription we can
     * update the period start and end dates to reflect the new one.
     * We should also update the Plan ID in case the user switched from annual to monthly
     * or vice versa
     *
     * We can also assume that the is only one item in the 'renewingSubs'
     * array. If there are more it is bad data, and grabbing the first one
     * should work as expected
     */

    const isRenewing = userSubscriptions[0].periodEnd < receiptSubscription.periodEnd;
    if (isRenewing) {
      userSubscriptions[0].periodStart = receiptSubscription.periodStart;
      userSubscriptions[0].periodEnd = receiptSubscription.periodEnd;
      userSubscriptions[0].subscriptionId = receiptSubscription.subscriptionId;
      userSubscriptions[0].planId = receiptSubscription.planId;
      actionsToLog.push(`Updated period start to: ${receiptSubscription.periodStart}`);
      actionsToLog.push(`Updated period end to: ${receiptSubscription.periodEnd}`);
      userAccount.markModified('subscriptions');

      actionsToLog.push(
        `Renewing ${receiptSubscription.subscriptionId} for ${receiptSubscription.entitlement}`,
      );
    }
  } else if (!subscriptionExpiring && !hasSubscription) {
    // No user subscription found, adding a new subscription
    userAccount.subscriptions.push(receiptSubscription);

    actionsToLog.push(
      `${receiptSubscription.entitlement} subscription with id: ` +
        `${receiptSubscription.subscriptionId} added`,
    );
  }
};
