import _ from 'lodash';
// eslint-disable-next-line import/no-cycle
import { subscriptionMap } from './utils';

function findOlderArchivedSubs(userAccount, subscription, actionsToLog = [], self) {
  // This is a workaround using scope so that we can track if an array element was removed
  const thisSelf = self;
  return archivedSub => {
    const planId = subscription.applePlanId
      ? subscriptionMap[subscription.applePlanId]
      : subscriptionMap[subscription.planId];

    const archivedPlanId = archivedSub.applePlanId
      ? subscriptionMap[archivedSub.applePlanId]
      : subscriptionMap[archivedSub.planId];

    const archivedSubId = archivedSub.subscriptionId;
    const { subscriptionId } = subscription.subscriptionId;

    const isMatching = archivedSubId === subscriptionId || archivedPlanId === planId;
    const isNewer = archivedSub.periodEnd < subscription.periodEnd;
    const remove = isMatching && isNewer;
    if (remove) {
      thisSelf.removedArchivedSub = true;
      actionsToLog.push(
        'Removing older archived sub found.' +
          `Current archived sub end date: ${archivedSub.periodEnd}. ` +
          `Current sub end date: ${subscription.periodEnd}`,
      );
    }
    return remove;
  };
}

/**
 * Mutates UserAccount, archiving subscription
 *
 * Does not call userAccount.save() -> leaves up to caller to save()
 *
 * If we had a model abstraction on top of Mongoose, this would live on UserAccountModel
 *    ->  userAccountModel.archiveSubscription(subscription)
 *        userAccountModel.save()
 *
 * @param userAccount
 * @param subscription
 */
const archiveSubscription = async (userAccount, subscription, actionsToLog = []) => {
  if (!subscription || !userAccount) {
    throw new Error('Incorrectly attempting to remove subscription from userAccount');
  }

  /*
    Remove any archived subscriptions with the same Sub that are older
    so we don't store duplicates but rather overwrite them
  */
  const hasArchivedSubs = userAccount.archivedSubscriptions.length !== 0;
  const self = {};
  const matchFunction = findOlderArchivedSubs(userAccount, subscription, actionsToLog, self);
  _.remove(userAccount.archivedSubscriptions, matchFunction);

  if (self.removedArchivedSub || !hasArchivedSubs) {
    userAccount.archivedSubscriptions.push({
      ...subscription,
      archivedDate: new Date(),
    });
    actionsToLog.push(
      `Archived ${subscription.entitlement} subscription with id: ${subscription.subscriptionId}`,
    );
  }

  /**
   * Remove matching subscription that has become outdated
   */
  _.remove(userAccount.subscriptions, sub => {
    const planId = sub.applePlanId ? subscriptionMap[sub.applePlanId] : subscriptionMap[sub.planId];

    const subIdMatches = sub.subscriptionId === subscription.subscriptionId;
    const planIdMathes = planId === subscriptionMap[subscription.planId];

    const remove = subIdMatches || planIdMathes;
    if (remove) {
      userAccount.markModified('subscriptions');
      actionsToLog.push(`Removed ${subscription.entitlement} subscription`);
    }
    return remove;
  });

  return true;
};

export default archiveSubscription;
