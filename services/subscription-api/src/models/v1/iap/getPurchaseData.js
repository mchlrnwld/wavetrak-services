import * as iap from 'in-app-purchase';

/**
 * Validates receipt with Apple and returns Purchase data for a receipt
 *    -- expect call to return <500ms (talks to Apple)
 *
 * You pass in deviceType for future support of Google receipts.
 *
 * @param deviceType        'apple' || 'google'
 * @param receiptData       raw receipt data from device
 * @param brandForGoogle    'sl' || 'bw' || 'fs' || undefined (default)
 * @returns {Promise}       Promise that resolves as the extracted purchase data,
 *                          or rejects with error from iap
 */
const getPurchaseData = (deviceType, receiptData, brandForGoogle, log) =>
  new Promise(async (resolve, reject) => {
    let purchaseDataList;
    let iapResponse;
    try {
      log.trace(`IAP Verbose: ${process.env.IAP_VERBOSE} `);
      const iapConfig = {
        verbose: process.env.IAP_VERBOSE,
      };
      if (brandForGoogle && deviceType === 'google') {
        iapConfig.googleAccToken = process.env.PLAY_STORE_API_ACCESS_TOKEN;
        iapConfig.googleRefToken = process.env.PLAY_STORE_API_REFRESH_TOKEN;
        iapConfig.googleClientID = process.env.PLAY_STORE_API_CLIENT_ID;
        iapConfig.googleClientSecret = process.env.PLAY_STORE_API_CLIENT_SECRET;

        if (brandForGoogle === 'bw') {
          iapConfig.googlePublicKeyStrLive = process.env.BUOYWEATHER_GOOGLE_IAP_PUBLICKEY_LIVE;
        } else if (brandForGoogle === 'sl') {
          iapConfig.googlePublicKeyStrLive = process.env.SURFLINE_GOOGLE_IAP_PUBLICKEY_LIVE;
        } else if (brandForGoogle === 'fs') {
          iapConfig.googlePublicKeyStrLive = process.env.FISHTRACK_GOOGLE_IAP_PUBLICKEY_LIVE;
        }
      }

      iap.config(iapConfig);
      await iap.setup();

      iapResponse = await iap.validate(receiptData);

      if (iap.isValidated(iapResponse)) {
        purchaseDataList = iap.getPurchaseData(iapResponse);
        if (!purchaseDataList) {
          log.error({ message: 'Missing purchase data' });
          return reject(new Error('No purchase data found.'));
        }
      }
    } catch (error) {
      log.error({
        message: 'IAP:Error returned from getPurchaseData',
        stack: error,
      });
      reject(error);
    }
    return resolve({
      purchases: purchaseDataList,
      receipt: iapResponse.receipt || iapResponse,
    });
  });

export default getPurchaseData;
