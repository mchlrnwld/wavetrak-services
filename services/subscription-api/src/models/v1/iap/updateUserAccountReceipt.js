import newrelic from 'newrelic';
import logger from '../../../common/logger';
import * as account from '../accounts';
import getPurchaseData from './getPurchaseData';
import { getSubsFromPurchaseData, syncReceiptSubscriptionToAccount } from './utils';

const log = logger('iap-receipt-model');

/**
 * Adds or updates a receipt to a user and updates subscriptions accordingly
 *
 * @param deviceType
 * @param receiptData
 * @param userId    UserInfo id
 * @param brandName   'bw' || 'fs' || 'sl' || undefined (default)
 *
 * @returns UserAccount
 */
const updateUserAccountReceipt = (deviceType, receiptData, userId, brandName) =>
  new Promise(async (resolve, reject) => {
    // This is the only entitlement we are currently using
    const actionsToLog = [];
    let userAccount;
    try {
      // Retrieve the User Account so that we can modify it to the verified receipt
      userAccount = await account.getAccount(userId);
      if (!userAccount) {
        userAccount = await account.createAccount(userId);
      } else if (!userAccount.receipts) {
        userAccount.receipts = [];
      }

      // Query provider for purchase data from receipt
      const purchaseData = await getPurchaseData(deviceType, receiptData, brandName, log);

      // Grab the receipt that matches the current one we are targeting
      const deviceReceipt = userAccount.receipts.filter(receipt => {
        if (deviceType === 'apple') {
          return receipt.receiptData === receiptData;
        }
        return receipt.receiptData.signature === receiptData.signature;
      });

      if (deviceReceipt && deviceReceipt.length > 0) {
        // Using the purchaseData as the source of truth for purchases, update our records to match
        deviceReceipt[0].receiptData = receiptData;
        deviceReceipt[0].purchaseData = purchaseData.purchases;
        deviceReceipt[0].purchases = purchaseData.purchases;
        deviceReceipt[0].lastUpdated = new Date();
        userAccount.markModified('receipts');

        actionsToLog.push(`Receipt for ${brandName} on ${deviceType} modified`);
      } else {
        userAccount.receipts.push({
          type: deviceType,
          receiptData,
          purchaseData: purchaseData.purchases,
          purchases: purchaseData.purchases,
          lastUpdated: new Date(),
        });

        actionsToLog.push(`Receipt for ${brandName} on ${deviceType} added`);
      }

      /**
       * Create a subscription object from the most recent purchase in the purchase data.
       * We will pass this to the syncReceiptSubscriptionToAccount to parse the receipt
       * and update the users account to match their purchases
       */
      const receiptSubscription = await getSubsFromPurchaseData(
        deviceType,
        purchaseData.purchases,
        log,
        newrelic,
      );

      await syncReceiptSubscriptionToAccount(
        userAccount,
        purchaseData,
        receiptSubscription,
        actionsToLog,
      );

      /**
       * There's a weird issue with archived subscriptions where the array is not being
       * updated and saved properly. This allows us to change the reference to the array
       * completely so that it will force a save.
       */
      userAccount.archivedSubscriptions = [...userAccount.archivedSubscriptions];
      userAccount.subscriptions = [...userAccount.subscriptions];
      userAccount.markModified('archivedSubscriptions');
      userAccount.markModified('subscriptions');

      // Clear premiumPending status for active subscriptions.
      if (userAccount && userAccount.subscriptions.length > 0) {
        userAccount.subscriptions.map(({ entitlement }) => {
          if (
            userAccount.premiumPending &&
            userAccount.premiumPending.indexOf(entitlement) !== -1
          ) {
            userAccount.premiumPending.pull(entitlement);
            userAccount.markModified('premiumPending');
            return entitlement;
          }
          return entitlement;
        });
      }

      await userAccount.save();

      log.debug({
        message: 'Updating user account receipt',
        userId: userAccount.user.toString(),
        actions: actionsToLog,
      });
    } catch (error) {
      log.error({
        message: 'Error returned from updateUserAccountReceipt',
        errorMessage: error.message,
        userId: userAccount.user.toString(),
        actions: actionsToLog,
        stack: error.stack,
        receiptData,
      });
      reject(error);
    }
    resolve(userAccount);
  });

export default updateUserAccountReceipt;
