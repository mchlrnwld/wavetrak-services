import logger from '../../../common/logger';
import * as account from '../accounts';
import getPurchaseData from './getPurchaseData';
import { getSubsFromPurchaseData, getBrand, syncReceiptSubscriptionToAccount } from './utils';

const log = logger('iap-receipt-model');

/**
 * Verifies all of a user's receipts for cancellations and archive accordingly
 *
 * Run from a scheduled task to update ios iap receipts
 *
 * @param userId
 * @param commitChanges   Commit changes to UserAccount,
 *                        otherwise this function will run as an audit
 *
 * @returns UserAccount
 */
const verifyUserAccountReceipts = async (userId, commitChanges = false) => {
  let userAccountModified = false;
  const actionsToLog = [];
  const userAccount = await account.getAccount(userId);
  if (!userAccount) {
    log.error(`Tried to verify missing user ${userId}'s receipts`);
    throw new Error('User not found');
  } else if (!userAccount.receipts) {
    log.error(`Tried to verify user ${userId}'s missing receipts`);
    throw new Error('User receipts not found');
  }

  for (const receipt of userAccount.receipts) {
    const deviceType = receipt.type;
    const brandForGoogle = getBrand(receipt);
    const purchaseData = await getPurchaseData(
      deviceType,
      receipt.receiptData,
      brandForGoogle,
      log,
    );

    userAccountModified = true;
    receipt.purchases = purchaseData.purchases;
    receipt.purchaseData = purchaseData.purchases;
    receipt.lastUpdated = new Date();
    userAccount.markModified('receipts');
    actionsToLog.push('Purchase data updated');

    /**
     * Create a subscription object from the most recent purchase in the purchase data.
     * We will pass this to the syncReceiptSubscriptionToAccount to parse the receipt
     * and update the receipt subscrip
     */
    const receiptSubscription = await getSubsFromPurchaseData(
      deviceType,
      purchaseData.purchases,
      log,
    );

    await syncReceiptSubscriptionToAccount(
      userAccount,
      purchaseData,
      receiptSubscription,
      actionsToLog,
    );

    /**
     * There's a weird issue with archived subscriptions where the array is not being
     * updated and saved properly. This allows us to change the reference to the array
     * completely so that it will force a save.
     */
    userAccount.archivedSubscriptions = [...userAccount.archivedSubscriptions];
    userAccount.subscriptions = [...userAccount.subscriptions];
    userAccount.markModified('archivedSubscriptions');
    userAccount.markModified('subscriptions');
  }

  if (userAccountModified && commitChanges) {
    actionsToLog.push(`Saving changes to user: ${userAccount.user.toString()}`);
    await userAccount.save();
  }

  log.info({
    message: 'Batch Job: verifying user account receipt',
    userId: userAccount.user.toString(),
    mode: commitChanges ? 'write' : 'read-only',
    actions: actionsToLog,
  });

  return userAccount;
};

export default verifyUserAccountReceipts;
