import mongoose, { Schema } from 'mongoose';

const options = {
  collection: 'GiftProducts',
};

const ProductsSchema = Schema(
  {
    name: { type: String, required: [true, 'Name is Required'] },
    brand: { type: String, required: [true, 'Brand is Required'] },
    duration: { type: Number, required: [true, 'Duration is Required'] },
    price: { type: Number, required: [true, 'Price is Required'] },
    isActive: { type: Boolean, required: [true, 'isActive is Required'] },
    bestValue: { type: Boolean, required: [true, 'bestValue is Required'] },
    imagePath: { type: String, required: [true, 'imagePath is Required'] },
    description: { type: String },
  },
  options,
);

export default mongoose.model('Products', ProductsSchema);
