import { APIError } from '@surfline/services-common';
import ProductModel from './ProductModel';

export const getActiveProductsByBrand = async brand => {
  if (!brand) throw new APIError('Cannot find products without a valid brand');
  const products = await ProductModel.find({ isActive: true, brand });
  return products;
};

export const getProducts = () => ProductModel.find();

export const getProductById = async productId => {
  if (!productId) throw new APIError('Cannot find product without a valid product id');
  const product = await ProductModel.findOne({ _id: productId }).lean();
  return product;
};
