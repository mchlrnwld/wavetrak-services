import { APIError } from '@surfline/services-common';
import { RedeemCodeModel } from './RedeemCodeModel';
import { trackGeneratedGiftCode, trackPurchasedGift, trackRedeemedGiftCard } from '../../tracking';

/**
 * Generates a random alphanumeric string with billions of variations
 * to promote uniquness.
 *
 * @param length - {integer} Defines the character length of the code
 * @param chars - {string} Contains the characters used to generate the code
 *
 * @returns code - {string}
 */
export const generateCode = (length = 8, chars = '23456789ABCDEFGHJLMNPQRSTUVWXYZ') => {
  let result = '';
  for (let i = length; i > 0; i -= 1) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
};

export const createPurchasedRedeemCode = async data => {
  const { type, brand } = data;
  if (type !== 'prices' && type !== 'legacy') {
    throw new APIError('Invalid type when generating gift code');
  }
  const code = generateCode();

  const redeemCodeDoc = await RedeemCodeModel.create({
    ...data,
    redeemCode: code,
    giftProduct: data.productId ?? undefined,
  });

  trackPurchasedGift(
    {
      ...data,
      redeemCode: code,
      planId: `${brand}_gift`,
    },
    { brand },
  );

  return redeemCodeDoc;
};

export const createInternalRedeemCode = async (data = {}) => {
  const { department, purchaserEmail, productId, prefix, brand } = data;
  if (!productId) {
    throw new APIError('Cannot find product without a valid product id');
  }
  const code = generateCode();
  const redeemCodeDoc = await RedeemCodeModel.create({
    type: 'internal',
    giftProduct: productId,
    purchaserEmail,
    department,
    redeemCode: `${prefix}${code}`,
  });
  const redeemCode = redeemCodeDoc.toObject();
  trackGeneratedGiftCode({ ...redeemCode, brand });
  return redeemCode;
};

export const getRedeemCode = async code => {
  const formattedCode = code.toUpperCase().trim();
  return RedeemCodeModel.findOne({ redeemCode: formattedCode }).lean();
};

export const getRedeemCodeByParams = async params => {
  return RedeemCodeModel.findOne({ ...params });
};

export const updateRedeemCode = async (data, subscription) => {
  const { _id, redeemCode } = data;
  trackRedeemedGiftCard(subscription, redeemCode);
  return RedeemCodeModel.findOneAndUpdate({ _id }, { ...data }, { overwrite: true });
};
