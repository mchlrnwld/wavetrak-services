import mongoose from 'mongoose';

const options = {
  discriminatorKey: 'type',
  collection: 'GiftRedemptionCodes',
  timestamps: true,
};

const RedeemCodeSchema = new mongoose.Schema(
  {
    type: {
      type: String,
      required: [true, 'A redeem code must be of a specific type.'],
      enum: ['legacy', 'prices', 'internal'],
    },
    redeemCode: {
      type: String,
      required: [true, 'redeemCode is required'],
      trim: true,
    },
    purchaseDate: {
      type: Date,
      default: Date.now,
    },
    redeemDate: {
      type: Date,
      default: null,
    },
    redeemed: {
      type: Boolean,
      default: false,
    },
    purchaserEmail: {
      type: String,
      required: [true, 'purchaserEmail is required'],
      trim: true,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
  },
  options,
);

const LegacyRedeemCodeSchema = new mongoose.Schema({
  giftProduct: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'GiftProducts',
  },
  chargeId: {
    type: String,
    trim: true,
  },
});

const PricesRedeemCodeSchema = new mongoose.Schema({
  paymentIntentId: {
    type: String,
    trim: true,
    index: {
      unique: true,
      partialFilterExpression: { paymentIntentId: { $type: 'string' } },
    },
  },
  priceId: {
    type: String,
    trim: true,
    required: [true, 'a stripe price id is required'],
  },
  stripeCustomerId: {
    type: String,
    trim: true,
  },
  purchaserName: {
    type: String,
    trim: true,
  },
  recipientEmail: {
    type: String,
    trim: true,
  },
  recipientName: {
    type: String,
    trim: true,
  },
  giftMessage: {
    type: String,
  },
});

const InternalRedeemCodeSchema = new mongoose.Schema({
  giftProduct: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'GiftProducts',
  },
  department: {
    type: String,
  },
});

RedeemCodeSchema.index({ redeemCode: 1 }, { unique: true });

export const RedeemCodeModel = mongoose.model('RedeemCode', RedeemCodeSchema);
export const LegacyRedeemCodeModel = RedeemCodeModel.discriminator(
  'legacy',
  LegacyRedeemCodeSchema,
);
export const PricesRedeemCodeModel = RedeemCodeModel.discriminator(
  'prices',
  PricesRedeemCodeSchema,
);
export const InternalRedeemCodeModel = RedeemCodeModel.discriminator(
  'internal',
  InternalRedeemCodeSchema,
);
