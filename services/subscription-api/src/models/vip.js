import moment from 'moment';
import * as _ from 'lodash';
import * as account from './v1/accounts';

export const createVipSubscription = async (userId, entitlementName) => {
  const generatedSubId = (Math.random() * 1e32).toString(36);
  const userAccount = await account.getAccount(userId);
  const planId = `${entitlementName.substring(0, 2)}_vip_annual`;

  const subscription = {
    type: 'vip',
    subscriptionId: `vip_${generatedSubId}`,
    periodStart: moment().toDate(),
    periodEnd: moment()
      .add(1, 'y')
      .toDate(),
    entitlement: entitlementName,
    planId,
    createdAt: moment().toDate(),
    onTrial: false,
    expiring: false,
  };

  userAccount.subscriptions.push(subscription);
  userAccount.markModified('subscriptions');

  const updatedAccount = await userAccount.save();

  return updatedAccount;
};

export const removeVipSubscription = async (userId, subscriptionId) => {
  const userAccount = await account.getAccount(userId);

  const cancelledSubscription = _.remove(
    userAccount.subscriptions,
    sub => sub.subscriptionId === subscriptionId,
  );
  userAccount.markModified('subscriptions');

  // For tracking.  Will set `subscription.active` to false
  cancelledSubscription[0].expiring = true;

  userAccount.archivedSubscriptions.push(cancelledSubscription[0]);
  userAccount.markModified('archivedSubscriptions');

  const updatedAccount = await userAccount.save();

  return updatedAccount;
};
