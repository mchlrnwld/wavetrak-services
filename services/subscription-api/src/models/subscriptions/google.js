import { SubscriptionModel } from './SubscriptionModel';
import {
  deleteSubscriptionUsersBySubscriptionId,
  updateSubscriptionUsersBySubscriptionId,
} from '../subscription-users';
import logger from '../../common/logger';
import { googleProductMap } from '../../utils/google';
import { deletePendingSubscription } from './pending';
import {
  trackCancelledFreeTrial,
  trackCancelledSubscription,
  trackConvertedFreeTrial,
  trackExpiredSubscription,
  trackStartedFreeTrial,
  trackStartedSubscription,
  trackChangedSubscriptionPlan,
} from '../../tracking';

const log = logger('subscription-api-google');

export const getSubscriptionByGooglePackageNameAndUser = async (packageName, user, baseReceipt) => {
  const subscription = await SubscriptionModel.findOne({
    type: 'google',
    packageName,
    user,
    baseReceipt,
  });

  if (!subscription) {
    log.info(`No subscription found with packageName and user: ${packageName}:${user}`);
  }
  return subscription;
};

export const createGoogleSubscription = async data => {
  const { orderId, productId, user } = data;

  const entitlement = googleProductMap[productId];

  const subscription = await SubscriptionModel.create({
    type: 'google',
    subscriptionId: orderId,
    planId: productId,
    entitlement,
    ...data,
  });

  if (subscription.trialing) {
    trackStartedFreeTrial(subscription);
  } else {
    trackStartedSubscription(subscription);
  }

  const brand = entitlement.substring(0, 2);
  await deletePendingSubscription({ user, platform: 'google', brand });

  return subscription;
};

export const updateGoogleSubscription = async data => {
  const {
    packageName,
    user,
    orderId,
    productId,
    start,
    end,
    trialing,
    expiring,
    active,
    cancellationReason,
    baseReceipt,
    alertPayment,
  } = data;
  const subscription = await getSubscriptionByGooglePackageNameAndUser(
    packageName,
    user,
    baseReceipt,
  );

  const isAutoRenewing = subscription.end === start;
  const expiringHasChanged = subscription.expiring !== expiring;
  const isExpired = subscription.active && !active;
  const convertedFreeTrial = active && subscription.trialing && !trialing;
  const changedPlan = productId !== subscription.planId;
  const isExpiring = !subscription.expiring && expiring;

  subscription.subscriptionId = orderId;
  subscription.orderId = orderId;
  subscription.planId = productId;
  subscription.productId = productId;
  subscription.start = start;
  subscription.end = end;
  subscription.trialing = trialing;
  subscription.active = active;
  subscription.expiring = expiring;
  subscription.alertPayment = alertPayment;
  subscription.cancellationReason = cancellationReason;
  subscription.updatedAt = new Date();

  await subscription.save();

  if (isAutoRenewing || expiringHasChanged) {
    await updateSubscriptionUsersBySubscriptionId(subscription.subscriptionId, orderId);
  }

  if (changedPlan) {
    trackChangedSubscriptionPlan(subscription);
  }

  if (convertedFreeTrial) {
    trackConvertedFreeTrial(subscription);
  }

  if (active && isExpiring && !trialing) {
    trackCancelledSubscription(subscription);
  }

  if (active && isExpiring && trialing) {
    trackCancelledFreeTrial(subscription);
  }

  if (isExpired) {
    trackExpiredSubscription(subscription);
    await deleteSubscriptionUsersBySubscriptionId(orderId);
  }

  return subscription;
};

export const updateOrCreateGoogleSubscription = async data => {
  const { packageName, user, baseReceipt } = data;
  const subscription = await getSubscriptionByGooglePackageNameAndUser(
    packageName,
    user,
    baseReceipt,
  );

  if (!subscription) {
    const newSubscription = await createGoogleSubscription(data);
    return newSubscription;
  }
  const updatedSubscription = await updateGoogleSubscription(data);
  return updatedSubscription;
};
