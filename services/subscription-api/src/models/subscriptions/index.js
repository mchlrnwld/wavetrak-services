import { SubscriptionModel } from './SubscriptionModel';

export const getActiveSubscriptionsByUserId = async user => {
  return SubscriptionModel.find({ user, active: true }).lean();
};

export const getAllSubscriptionsByUserId = async user => {
  return SubscriptionModel.find({ user });
};

export const getActiveSubscriptionByUserIdAndEntitlement = async (user, brand) => {
  return SubscriptionModel.findOne({
    user,
    active: true,
    entitlement: new RegExp(`${brand}_`, 'i'),
  }).lean();
};

export const getSubscriptionFromCustomParams = async searchParams => {
  return SubscriptionModel.findOne(searchParams).lean();
};

export const deleteSubscriptions = async user => SubscriptionModel.deleteMany({ user });

export const updateSubscriptionByUser = async (user, data) =>
  SubscriptionModel.updateOne({ user }, data, { upsert: true });
