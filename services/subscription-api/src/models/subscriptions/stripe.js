import { APIError } from '@surfline/services-common';
import { SubscriptionModel, StripeSubscriptionModel } from './SubscriptionModel';

import {
  trackStartedSubscription,
  trackStartedFreeTrial,
  trackExpiredSubscription,
  trackCancelledSubscription,
  trackCancelledFreeTrial,
  trackConvertedFreeTrial,
  trackChangedSubscriptionPlan,
} from '../../tracking';

export const getStripeSubscription = async subscriptionId => {
  return SubscriptionModel.findOne({ type: 'stripe', subscriptionId }).lean();
};

export const getActiveStripeSubscription = async subscriptionId => {
  const activeSubscription = await SubscriptionModel.findOne({
    type: 'stripe',
    subscriptionId,
    active: true,
  }).lean();
  if (!activeSubscription) {
    throw new APIError('Cannot find active subscription with subscription id');
  }
  return activeSubscription;
};

export const getStripeSubscriptionsByUser = async user => {
  return SubscriptionModel.find({ type: 'stripe', user, active: true }).lean();
};

export const getActiveStripeSubscriptionByUser = async (user, product) => {
  const activeSubscription = await SubscriptionModel.findOne({
    type: 'stripe',
    active: true,
    user,
    entitlement: new RegExp(`${product}_`, 'i'),
  });
  if (!activeSubscription) {
    throw new APIError('Cannot find active subscription for user and product');
  }
  return activeSubscription;
};

export const getStripeSubscriptionByPromotionIdAndUser = async (user, promotionId) => {
  return StripeSubscriptionModel.findOne({ type: 'stripe', user, promotionId });
};

export const createStripeSubscription = async (user, data) => {
  const {
    id: subscriptionId,
    customer: stripeCustomerId,
    current_period_start: start,
    current_period_end: end,
    items: {
      data: [
        {
          price: {
            id: planId,
            metadata: { entitlement },
          },
        },
      ],
    },
    status,
    metadata: { promotionId, redeemCode },
  } = data;

  const stripeSubscriptionAttrs = {
    type: 'stripe',
    subscriptionId,
    user,
    planId,
    entitlement,
    start,
    end,
    active: true,
    trialing: status === 'trialing',
    stripeCustomerId,
  };

  const promotionAttrs = promotionId ? { promotionId, redeemCodes: [redeemCode] } : null;

  const newSubscriptionData = {
    ...stripeSubscriptionAttrs,
    ...promotionAttrs,
  };

  const subscription = await SubscriptionModel.create(newSubscriptionData);
  if (subscription.trialing) {
    trackStartedFreeTrial(subscription);
  } else {
    trackStartedSubscription(subscription);
  }

  return subscription;
};

export const updateStripeSubscription = async data => {
  const { subscriptionId } = data;
  const subscription = await SubscriptionModel.findOneAndUpdate({ subscriptionId }, data, {
    new: true,
    overwrite: true,
  }).lean();

  const { active, expiring, trialing } = subscription;
  const { cancelledFreeTrial, convertedFreeTrial, changedPlan } = data;
  if (convertedFreeTrial) {
    trackConvertedFreeTrial(subscription);
  }

  if (changedPlan) {
    trackChangedSubscriptionPlan(subscription);
  }

  if (active && expiring && !trialing) {
    trackCancelledSubscription(subscription);
  }

  if (!active && cancelledFreeTrial) {
    trackCancelledFreeTrial({ ...subscription, trialing: true });
  }

  if (!active) {
    trackExpiredSubscription(subscription);
  }

  return subscription;
};

export const deleteStripeSubscriptions = async user => SubscriptionModel.deleteMany({ user });
