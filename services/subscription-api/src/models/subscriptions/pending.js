import uuidV1 from 'uuid';
import { APIError } from '@surfline/services-common';
import { SubscriptionModel } from './SubscriptionModel';

export const getPendingSubscription = async ({ user, brand }) => {
  return SubscriptionModel.findOne({ user, planId: `${brand}_pending` }).lean();
};

export const createPendingSubscription = async data => {
  const { user, brand, platform } = data;
  const pendingDate = new Date() / 1000;
  const existingPendingSubscription = await getPendingSubscription({ user, brand });

  if (existingPendingSubscription) {
    const { platform: pendingPlatform } = existingPendingSubscription;
    throw new APIError(`There is an existing ${pendingPlatform} subscription pending.`);
  }
  const subscription = await SubscriptionModel.create({
    type: 'pending',
    subscriptionId: uuidV1(),
    active: true,
    planId: `${brand}_pending`,
    entitlement: `${brand}_pending`,
    platform,
    user,
    start: pendingDate,
    end: pendingDate,
  });
  return subscription;
};

export const deletePendingSubscription = async data => {
  const { user, brand, platform } = data;
  return SubscriptionModel.deleteOne({ user, planId: `${brand}_pending`, platform });
};
