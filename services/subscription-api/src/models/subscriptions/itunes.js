import { SubscriptionModel } from './SubscriptionModel';
import { deletePendingSubscription } from './pending';
import {
  deleteSubscriptionUsersBySubscriptionId,
  updateSubscriptionUsersBySubscriptionId,
} from '../subscription-users';
import { iTunesProductMap } from '../../utils/itunes';
import logger from '../../common/logger';
import {
  trackExpiredSubscription,
  trackStartedFreeTrial,
  trackStartedSubscription,
  trackCancelledFreeTrial,
  trackCancelledSubscription,
  trackConvertedFreeTrial,
  trackChangedSubscriptionPlan,
} from '../../tracking';

const log = logger('subscription-api-apple');

export const getSubscriptionBySubscriptionId = async subscriptionId => {
  const subscription = await SubscriptionModel.findOne({ subscriptionId });
  if (!subscription) {
    log.info(`No subscription found with subscriptionId: ${subscriptionId}`);
  }
  return subscription;
};

export const getSubscriptionByItunesTransactionId = async transactionId => {
  const subscription = await SubscriptionModel.findOne({ type: 'apple', transactionId });
  if (!subscription) {
    log.warn({
      transactionId,
      message: 'No subscription found.',
      function: 'getSubscriptionByItunesTransactionId',
    });
  }
  return subscription;
};

export const getSubscriptionByItunesOriginalTransactionId = async originalTransactionId => {
  const subscription = await SubscriptionModel.findOne({
    type: 'apple',
    originalTransactionId,
  });

  if (!subscription) {
    log.info(`No subscription found with originalTransactionId: ${originalTransactionId}`);
  }
  return subscription;
};

export const createItunesSubscription = async data => {
  const {
    originalTransactionId,
    transactionId,
    baseReceipt,
    user,
    latestReceipt,
    productId,
    start,
    end,
    trialing,
    expiring,
    active,
  } = data;

  const entitlement = iTunesProductMap[productId];

  const subscription = await SubscriptionModel.create({
    type: 'apple',
    subscriptionId: transactionId,
    user,
    active,
    planId: productId,
    entitlement,
    start,
    end,
    trialing,
    expiring,
    transactionId,
    originalTransactionId,
    baseReceipt,
    latestReceipt,
  });

  if (subscription.trialing) {
    trackStartedFreeTrial(subscription);
  } else {
    trackStartedSubscription(subscription);
  }

  const brand = entitlement.substring(0, 2);
  await deletePendingSubscription({ user, platform: 'apple', brand });

  return subscription;
};

export const updateItunesSubscription = async data => {
  const {
    originalTransactionId,
    transactionId,
    latestReceipt,
    productId,
    autoRenewProductId,
    start,
    end,
    user,
    trialing,
    expiring,
    expired,
    expirationIntent,
    cancellationReason,
    active,
    alertPayment,
  } = data;
  const subscription = await getSubscriptionByItunesOriginalTransactionId(originalTransactionId);
  const isAutoRenewing = subscription.end === start;
  const expiringHasChanged = subscription.expiring !== expiring;
  const isExpired = subscription.active && !active;
  const convertedFreeTrial = active && subscription.trialing && !trialing;
  const changedPlan = productId !== subscription.planId;
  const isExpiring = !subscription.expiring && expiring;

  subscription.transactionId = transactionId;
  subscription.subscriptionId = transactionId;
  subscription.latestReceipt = latestReceipt;
  subscription.planId = productId;
  subscription.autoRenewProductId = autoRenewProductId;
  subscription.start = start;
  subscription.end = end;
  subscription.trialing = trialing;
  subscription.expiring = expiring;
  subscription.expired = expired;
  subscription.expirationIntent = expirationIntent;
  subscription.cancellationReason = cancellationReason;
  subscription.active = active;
  subscription.alertPayment = !!alertPayment;

  await subscription.save();

  if (isAutoRenewing || expiringHasChanged) {
    await updateSubscriptionUsersBySubscriptionId(subscription.subscriptionId, transactionId);
  }

  if (changedPlan) {
    trackChangedSubscriptionPlan(subscription);
  }

  if (convertedFreeTrial) {
    trackConvertedFreeTrial(subscription);
  }

  if (active && isExpiring && !trialing) {
    trackCancelledSubscription(subscription);
  }

  if (active && isExpiring && trialing) {
    trackCancelledFreeTrial(subscription);
  }

  if (isExpired) {
    trackExpiredSubscription(subscription);
    await deleteSubscriptionUsersBySubscriptionId(transactionId);
  }

  const brand = subscription.entitlement.substring(0, 2);
  await deletePendingSubscription({ user, platform: 'apple', brand });

  return subscription;
};

export const updateOrCreateItunesSubscription = async data => {
  const { originalTransactionId } = data;
  const subscription = await getSubscriptionByItunesOriginalTransactionId(originalTransactionId);

  if (!subscription) {
    const newSubscription = await createItunesSubscription(data);
    return newSubscription;
  }
  const updatedSubscription = await updateItunesSubscription(data);
  return updatedSubscription;
};
