import { SubscriptionModel } from './SubscriptionModel';

export const createGiftSubscription = async (user, data) => {
  const { duration, brand, code, chargeId } = data;

  const start = Math.floor(new Date() / 1000);
  const durationInSeconds = parseInt(duration, 10) * 2628000;
  const end = start + durationInSeconds;

  const giftSubscriptionAttrs = {
    type: 'gift',
    subscriptionId: `gift_${code}`,
    user,
    active: true,
    planId: `${brand}_gift`,
    entitlement: `${brand}_premium`,
    start,
    end,
    trialing: false,
    expiring: true,
    redeemCodes: [code],
    chargeId,
  };

  return SubscriptionModel.create(giftSubscriptionAttrs);
};

export const updateGiftSubscription = async (subscription, data) => {
  const { subscriptionId, redeemCodes, end } = subscription;
  const { code, duration, chargeId } = data;

  const durationInSeconds = parseInt(duration, 10) * 2628000;
  const extendedEnd = end + durationInSeconds;

  const giftSubscriptionAttrs = {
    ...subscription,
    end: extendedEnd,
    redeemCodes: [...redeemCodes, code],
    chargeId,
  };

  return SubscriptionModel.findOneAndUpdate({ subscriptionId }, giftSubscriptionAttrs, {
    new: true,
    overwrite: true,
  });
};

export const expireGiftSubscription = async (subscriptionId, date) => {
  return SubscriptionModel.findOneAndUpdate(
    { subscriptionId, type: 'gift', end: { $lt: date }, active: true },
    { active: false, expired: new Date() / 1000 },
  );
};
