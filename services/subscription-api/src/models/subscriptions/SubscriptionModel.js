import mongoose, { Schema } from 'mongoose';

const options = {
  discriminatorKey: 'type',
  collection: 'Subscriptions',
  timestamps: true,
};

const SubscriptionSchema = new mongoose.Schema(
  {
    type: {
      type: Schema.Types.String,
      required: [true, 'A subscription must be of a specific type.'],
      enum: ['stripe', 'apple', 'google', 'gift'],
    },
    subscriptionId: {
      type: Schema.Types.String,
      required: [true, 'A subscription id must be defined.'],
      trim: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
    active: {
      type: Schema.Types.Boolean,
    },
    planId: {
      type: Schema.Types.String,
      required: [true, 'A subscription must include a plan id.'],
      trim: true,
    },
    entitlement: {
      type: Schema.Types.String,
    },
    start: {
      type: Schema.Types.Number,
      required: [true, 'A subscription start timestamp is required.'],
    },
    end: {
      type: Schema.Types.Number,
      required: [true, 'A subscription end timestamp is required.'],
    },
    expired: {
      type: Schema.Types.Number,
    },
    trialing: {
      type: Schema.Types.Boolean,
      required: [true, 'A subscription must indicate a free trial status.'],
      default: false,
    },
    expiring: {
      type: Schema.Types.Boolean,
      required: [true, 'A subscription must indicate a expiring status.'],
      default: false,
    },
    renewalCount: {
      type: Schema.Types.Number,
      default: 0,
    },
    alertPayment: {
      type: Schema.Types.Boolean,
      default: false,
    },
  },
  options,
);

const StripeSchema = new mongoose.Schema(
  {
    stripeCustomerId: {
      type: Schema.Types.String,
      required: [true, 'Stripe subscriptions must contain a customer id.'],
    },
    canceled: {
      type: Schema.Types.Number,
    },
    promotionId: {
      type: Schema.Types.ObjectId,
      ref: 'Promotions',
    },
    failedPaymentAttempts: {
      type: Schema.Types.Number,
      default: 0,
    },
    cardId: {
      type: Schema.Types.String,
    },
    paymentAlertType: {
      type: Schema.Types.String,
    },
    paymentAlertStatus: {
      type: Schema.Types.String,
    },
    paymentAlertReason: {
      type: Schema.Types.String,
    },
    redeemCodes: [Schema.Types.Mixed],
  },
  options,
);

const AppleSchema = new mongoose.Schema(
  {
    originalTransactionId: {
      type: Schema.Types.String,
      required: [true, 'Apple subscriptions must reference an original transaction'],
    },
    transactionId: {
      type: Schema.Types.String,
      required: [true, 'Apple subscription must reference the most recent transaction'],
    },
    baseReceipt: {
      type: Schema.Types.ObjectId,
      ref: 'SubscriptionPurchases',
      // required: [true, 'Apple subscription must have a corresponding receipt']
    },
    latestReceipt: {
      type: Schema.Types.String,
    },
    latestExpiredReceipt: {
      type: Schema.Types.String,
    },
    cancellationReason: {
      type: Schema.Types.String,
    },
    autoRenewStatusChangeDate: {
      type: Schema.Types.Number,
    },
    autoRenewProductId: {
      type: Schema.Types.String,
    },
    expirationIntent: {
      type: Schema.Types.String,
    },
  },
  options,
);

const GoogleSchema = new mongoose.Schema(
  {
    orderId: {
      type: Schema.Types.String,
      required: [true, 'Google subscriptions must include an orderId'],
    },
    packageName: {
      type: Schema.Types.String,
      required: [true, 'Google subscriptions must include a packageName'],
    },
    baseReceipt: {
      type: Schema.Types.ObjectId,
      ref: 'Receipts',
      required: [true, 'Google subscriptions must have a corresponding receipt'],
    },
    paymentState: {
      type: Schema.Types.Number,
    },
    cancellationReason: {
      type: Schema.Types.Number,
    },
    countryCode: {
      type: Schema.Types.String,
    },
  },
  options,
);

const GiftSchema = new mongoose.Schema(
  {
    redeemCodes: [Schema.Types.Mixed],
    chargeId: {
      type: String,
    },
    paymentAlertType: {
      type: Schema.Types.String,
    },
    paymentAlertStatus: {
      type: Schema.Types.String,
    },
    paymentAlertReason: {
      type: Schema.Types.String,
    },
  },
  options,
);

const PendingSchema = new mongoose.Schema({
  platform: {
    type: Schema.Types.String,
  },
});

SubscriptionSchema.index({ subscriptionId: 1 }, { unique: true });
SubscriptionSchema.index({ user: 1 });
GoogleSchema.index({
  packageName: 1,
  user: 1,
  baseReceipt: 1,
});
AppleSchema.index({ transactionId: 1 });
AppleSchema.index({ originalTransactionId: 1 });

export const SubscriptionModel = mongoose.model('Subscription', SubscriptionSchema);
export const StripeSubscriptionModel = SubscriptionModel.discriminator('stripe', StripeSchema);
export const AppleSubscriptionModel = SubscriptionModel.discriminator('apple', AppleSchema);
export const GoogleSubscriptionModel = SubscriptionModel.discriminator('google', GoogleSchema);
export const GiftSubscriptionModel = SubscriptionModel.discriminator('gift', GiftSchema);
export const PendingSubscriptionModel = SubscriptionModel.discriminator('pending', PendingSchema);
