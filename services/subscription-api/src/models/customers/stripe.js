import mongoose from 'mongoose';
import StripeCustomerModel from './StripeCustomerModel';

const getUserId = user => (typeof user === 'string' ? mongoose.Types.ObjectId(user) : user);

export const getStripeCustomerId = async user => {
  const res = await StripeCustomerModel.findOne({ user: getUserId(user) });
  return res ? res.stripeCustomerId : null;
};

export const getUserIdFromStripeCustomerId = async stripeCustomerId => {
  const res = await StripeCustomerModel.findOne({ stripeCustomerId });
  return res ? res.user : null;
};

export const createStripeCustomer = async (user, stripeCustomerId) =>
  StripeCustomerModel.create({ user: getUserId(user), stripeCustomerId });

export const updateStripeCustomer = async (user, stripeCustomerId) =>
  StripeCustomerModel.findOneAndUpdate(
    { user: getUserId(user) },
    { stripeCustomerId },
    { upsert: true },
  );

export const removeStripeCustomer = async user =>
  StripeCustomerModel.deleteOne({ user: getUserId(user) });
