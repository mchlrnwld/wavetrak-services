import mongoose, { Schema } from 'mongoose';

const options = {
  discriminatorKey: 'type',
  collection: 'SubscriptionPurchases',
  timestamps: true,
};

const SubscriptionPurchaseSchema = mongoose.Schema(
  {
    type: {
      type: String,
      required: [true, 'A purchase item must be of a specific type.'],
      enum: ['itunes', 'google-play', 'stripe'],
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
    expired: {
      type: Boolean,
    },
  },
  options,
);

/**
 * Apple Receipt Schema
 *
 * Documentation for Apple Receipts can be found within the following links
 * [Base] - https://developer.apple.com/documentation/storekit/in-app_purchase/validating_receipts_with_the_app_store
 * [ReceiptBody] - https://developer.apple.com/documentation/appstorereceipts/responsebody?changes=latest_minor
 * [latestReceiptInfo] - https://developer.apple.com/documentation/appstorereceipts/responsebody/latest_receipt_info?changes=latest_minor
 * [pendingRenewalInfo] - https://developer.apple.com/documentation/appstorereceipts/responsebody/pending_renewal_info?changes=latest_minor
 * [status] - https://developer.apple.com/documentation/appstorereceipts/status?changes=latest_minor
 *
 * Latest Receipt Info Properties
 *  - An array that contains all in-app purchase transactions
 * [isInTrialPeriod] - https://developer.apple.com/documentation/appstorereceipts/is_trial_period?changes=latest_minor
 *
 * Pending Renewal Status Properties
 *  - An array of elements that refers to auto-renewable subscription renewals
 *    that are open or failed in the past.
 * [autoRenewStatus] - https://developer.apple.com/documentation/appstorereceipts/auto_renew_status?changes=latest_minor
 * [expirationIntent] - https://developer.apple.com/documentation/appstorereceipts/expiration_intent?changes=latest_minor
 * [isInBillingRetryPeriod] - https://developer.apple.com/documentation/appstorereceipts/is_in_billing_retry_period?changes=latest_minor
 *
 */
const AppleReceiptSchema = mongoose.Schema(
  {
    environment: {
      type: String,
      required: [true, 'An apple receipt must contain a valid environment'],
      enum: ['Sandbox', 'Production'],
    },
    latestReceipt: {
      type: String,
      required: [true, 'An apple receipt must contain an encoded receipt string'],
    },
    status: {
      type: Number,
    },
    originalTransactionId: {
      type: String,
      require: [true, 'An apple receipt must conatin the id of the original transaction'],
    },
    pendingRenewalInfo: [
      {
        autoRenewStatus: {
          type: String,
          enum: ['0', '1'],
        },
        expirationIntent: {
          type: String,
          enum: ['1', '2', '3', '4', '5'],
        },
        originalTransactionId: {
          type: String,
        },
        productId: {
          type: String,
        },
        autoRenewProductId: {
          type: String,
        },
        isInBillingRetryPeriod: {
          type: String,
          enum: ['0', '1'],
        },
        gracePeriodExpiresDateMs: {
          type: String,
        },
        priceConsentStatus: {
          type: String,
          enum: ['0', '1'],
        },
      },
    ],
    latestReceiptInfo: [
      {
        productId: {
          type: String,
        },
        transactionId: {
          type: String,
        },
        originalTransactionId: {
          types: String,
        },
        isTrialPeriod: {
          type: String,
        },
        purchaseDateMs: {
          type: String,
        },
        expiresDateMs: {
          type: String,
        },
        originalPurchaseDateMs: {
          type: String,
        },
        cancellationDateMs: {
          type: String,
        },
        cancellationReason: {
          type: String,
        },
      },
    ],
  },
  options,
);

/**
 * Google Subscription Purchase Schema
 *
 * Documentation for Google Subscription Purchases can be found within the following links
 * [Subscription] - https://developers.google.com/android-publisher/api-ref/purchases/subscriptions
 * [Get] - https://developers.google.com/android-publisher/api-ref/purchases/subscriptions/get
 * [Authorization] - https://developers.google.com/android-publisher/authorization
 *
 */
const GoogleSubscriptionPurchaseSchema = mongoose.Schema(
  {
    signature: {
      type: String,
      required: [true, 'Google purchases must include a application signature'],
    },
    purchaseToken: {
      type: String,
      required: [true, 'Google purchases must include a purchase token.'],
    },
    packageName: {
      type: String,
      required: [true, 'Google purchases must include a package name.'],
    },
    productId: {
      type: String,
      required: [true, 'Google purchases must include a product id.'],
    },
    data: {
      orderId: {
        type: String,
        required: [true, 'Google purchases must include a order id.'],
      },
      autoRenewing: {
        type: Boolean,
        required: [true, 'Google purchases must provide an auto renewing status'],
      },
      expiryTimeMillis: {
        type: Number,
        required: [true, 'Google purchases must include an expiration date/time'],
      },
      startTimeMillis: {
        type: Number,
        required: [true, 'Google purchases must include an start date/time'],
      },
      cancelReason: {
        type: Number,
        enum: [0, 1, 2, 3],
      },
      cancelSurveyResult: {
        cancelSurveyReason: {
          type: Number,
          enum: [0, 1, 2, 3, 4],
        },
        userInputCancelReason: {
          type: String,
        },
      },
      countryCode: {
        type: String,
      },
      kind: {
        type: String,
      },
      linkedPurchaseToken: {
        type: String,
      },
      paymentState: {
        type: Number,
        enum: [0, 1, 2, 3],
      },
      priceAmountMicros: {
        type: Number,
      },
      promotionCode: {
        type: String,
      },
      userCancellationTimeMillis: {
        type: Number,
      },
    },
  },
  options,
);

SubscriptionPurchaseSchema.index({ user: 1 });
GoogleSubscriptionPurchaseSchema.index({ purchaseToken: 1 }, { sparse: true });
AppleReceiptSchema.index({ originalTransactionId: 1 }, { sparse: true });

export const SubscriptionPurchaseModel = mongoose.model(
  'SubscriptionPurchase',
  SubscriptionPurchaseSchema,
);
export const AppleReceiptModel = SubscriptionPurchaseModel.discriminator(
  'itunes',
  AppleReceiptSchema,
);
export const GooglePlaySubscriptionPurchaseModel = SubscriptionPurchaseModel.discriminator(
  'google-play',
  GoogleSubscriptionPurchaseSchema,
);
