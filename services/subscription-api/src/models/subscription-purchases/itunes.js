import { APIError } from '@surfline/services-common';
import getReceiptData from '../../external/itunes';
import { SubscriptionPurchaseModel } from './SubscriptionPurchaseModel';
import { nonRetryableStatusCodes } from '../../utils/itunes';

export const getApplePurchasesByUser = user =>
  SubscriptionPurchaseModel.find({ type: 'itunes', user });

export const getAppleReceiptByOriginalTransactionId = originalTransactionId =>
  SubscriptionPurchaseModel.findOne({ originalTransactionId });

export const getAppleReceiptByPurchaseId = purchaseId =>
  SubscriptionPurchaseModel.findOne({ type: 'itunes', _id: purchaseId });

export const createAppleReceipt = async ({ userId, originalTransactionId, iTunesReceiptData }) => {
  const iTunesReceipt = {
    type: 'itunes',
    user: userId,
    originalTransactionId,
    ...iTunesReceiptData,
  };
  const subscriptionPurchase = SubscriptionPurchaseModel.create(iTunesReceipt);
  return subscriptionPurchase;
};

export const updateAppleReceipt = async ({ originalTransactionId, iTunesReceiptData }) => {
  const appleReceipt = await getAppleReceiptByOriginalTransactionId(originalTransactionId);
  if (!appleReceipt) {
    throw new APIError('Cannot update unknown Apple Receipt.');
  }
  const { latestReceipt, latestReceiptInfo, pendingRenewalInfo } = iTunesReceiptData;
  appleReceipt.latestReceipt = latestReceipt;
  appleReceipt.latestReceiptInfo = latestReceiptInfo;
  appleReceipt.pendingRenewalInfo = pendingRenewalInfo;
  appleReceipt.active = true;

  await appleReceipt.save();
  return appleReceipt;
};

export const expireAppleReceiptByReceiptString = async (user, latestReceipt) => {
  const appleReceipt = await SubscriptionPurchaseModel.findOne({
    user,
    latestReceipt,
  });

  if (!appleReceipt) return null;

  appleReceipt.expired = true;
  await appleReceipt.save();
  return appleReceipt;
};

export const getAppleReceiptByReceiptString = (user, latestReceipt) => {
  return SubscriptionPurchaseModel.findOne({ user, latestReceipt });
};

export const processAppleReceipt = async (userId, receiptString) => {
  const iTunesReceiptData = await getReceiptData(receiptString);
  const { status, isRetryable } = iTunesReceiptData;
  const mustExpireReceipt = nonRetryableStatusCodes.includes(status) || isRetryable === false;

  if (mustExpireReceipt) {
    const expiredReceipt = await expireAppleReceiptByReceiptString(userId, receiptString);
    return expiredReceipt;
  }

  const {
    pendingRenewalInfo: [{ originalTransactionId }],
  } = iTunesReceiptData;
  const receiptDoc = await getAppleReceiptByOriginalTransactionId(originalTransactionId);
  if (!receiptDoc) {
    const newReceiptDoc = await createAppleReceipt({
      userId,
      originalTransactionId,
      iTunesReceiptData,
    });
    // TODO track receipt created
    return newReceiptDoc;
  }
  const updatedReceiptDoc = await updateAppleReceipt({
    originalTransactionId,
    iTunesReceiptData,
  });
  // TODO track receipt processed
  return updatedReceiptDoc;
};
