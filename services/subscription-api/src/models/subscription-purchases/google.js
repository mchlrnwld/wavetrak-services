import { APIError } from '@surfline/services-common';
import getPurchaseData, { postPurchaseData } from '../../external/google';
import { SubscriptionPurchaseModel } from './SubscriptionPurchaseModel';

export const getGooglePurchasesByUser = user =>
  SubscriptionPurchaseModel.find({ type: 'google-play', user });

export const getGooglePurchaseByPurchaseId = purchaseId =>
  SubscriptionPurchaseModel.findOne({ type: 'google-play', _id: purchaseId });

export const getGooglePurchaseByPurchaseToken = async (purchaseToken, linkedPurchaseToken) => {
  const existingPurchase =
    (await SubscriptionPurchaseModel.findOne({
      type: 'google-play',
      purchaseToken: linkedPurchaseToken,
    })) ||
    (await SubscriptionPurchaseModel.findOne({ type: 'google-play', purchaseToken })) ||
    (await SubscriptionPurchaseModel.findOne({
      type: 'google-play',
      'data.linkedPurchaseToken': linkedPurchaseToken,
      purchaseToken,
    }));
  return existingPurchase;
};

export const updateGoogleSubscriptionPurchase = async ({
  purchaseToken,
  productId,
  googlePurchaseData,
}) => {
  const { linkedPurchaseToken } = googlePurchaseData;
  const googleSubscriptionPurchase = await getGooglePurchaseByPurchaseToken(
    purchaseToken,
    linkedPurchaseToken,
  );

  googleSubscriptionPurchase.purchaseToken = purchaseToken;
  googleSubscriptionPurchase.productId = productId;
  googleSubscriptionPurchase.data = googlePurchaseData;

  await googleSubscriptionPurchase.save();
  return googleSubscriptionPurchase;
};

export const createGoogleSubscriptionPurchase = async ({
  userId,
  purchase,
  googlePurchaseData,
}) => {
  const {
    data: { productId, packageName, purchaseToken },
    signature,
  } = purchase;

  const googleSubscriptionPurchase = {
    type: 'google-play',
    user: userId,
    productId,
    packageName,
    signature,
    purchaseToken,
    data: { ...googlePurchaseData },
  };

  const subscription = await SubscriptionPurchaseModel.create(googleSubscriptionPurchase);
  return subscription;
};

export const expireGoogleSubscriptionPurchase = async ({ purchaseToken }) => {
  const purchase = await getGooglePurchaseByPurchaseToken(purchaseToken);
  purchase.expired = true;
  await purchase.save();
  return purchase;
};

/**
 * Google Play Billing tracks products and transactions using purchase tokens and order IDs.
 *
 * A purchase token is a string that represents a buyer's entitlement to a product on Google
 * Play. It indicates that a Google user has paid for a specific product, represented by a SKU.
 *
 * An order ID is a string that represents a financial transaction on Google Play. This string
 * is included in a receipt that is emailed to the buyer, and third-party developers use the
 * order ID to manage refunds in the Order Management section of the Google Play Console. Order
 * IDs are also used in sales and payout reports.
 *
 * For one-time products and rewarded products, every purchase creates a new token and a new
 * order ID.
 *
 * For subscriptions, an initial purchase creates a purchase token and an order ID. For each
 * continuous billing period, the purchase token stays the same and a new order ID is issued.
 * Upgrades, downgrades, and re-signups all create new purchase tokens and order IDs.
 *
 * If you are validating a subscription and the subscription is being upgraded, downgraded,
 * or the user has re-subscribed before the subscription has lapsed, check the
 * {linkedPurchaseToken} field. The {linkedPurchaseToken} field in a Purchases.subscriptions
 * resource contains the token of the previous, or “originating” purchase.
 *
 */
export const processGooglePurchase = async (userId, purchase) => {
  if (!purchase?.data?.purchaseToken) {
    throw new APIError('Cannot process subscription without a valid token');
  }
  const { data } = purchase;
  const { purchaseToken, productId } = data;
  const googlePurchaseData = await getPurchaseData(data);
  const { expired } = googlePurchaseData;
  if (expired) {
    const subscriptionPurchase = await expireGoogleSubscriptionPurchase(googlePurchaseData);
    return subscriptionPurchase;
  }
  const { linkedPurchaseToken } = googlePurchaseData;
  const googleSubscriptionPurchase = await getGooglePurchaseByPurchaseToken(
    purchaseToken,
    linkedPurchaseToken,
  );
  if (!googleSubscriptionPurchase) {
    const subscriptionPurchase = await createGoogleSubscriptionPurchase({
      userId,
      purchase,
      googlePurchaseData,
    });
    return subscriptionPurchase;
  }

  const subscriptionPurchase = await updateGoogleSubscriptionPurchase({
    purchaseToken,
    productId,
    googlePurchaseData,
  });
  return subscriptionPurchase;
};

/**
 * @description - Responsible for retrieving data from the Google Play API and
 * modifying a Subscription Purchase document accordingly.
 *
 * @param {object} signature
 * @param {string} signature.packageName - The Application or Brand affiliated with this purchase
 * @param {string} signature.purchaseToken - The token provided to the user’s device when the subscription
 * was purchased.
 * @param {string} signature.productId - The purchased subscription plan (for example,
 * ‘surfline.android.1month.subscription.notrial').
 *
 */
export const processGoogleNotification = async signature => {
  const { purchaseToken, productId } = signature;
  const googlePurchaseData = await getPurchaseData(signature);

  const { expired } = googlePurchaseData;
  if (expired) {
    const subscriptionPurchase = await expireGoogleSubscriptionPurchase(googlePurchaseData);
    return subscriptionPurchase;
  }

  const { linkedPurchaseToken } = googlePurchaseData;
  const googleSubscriptionPurchase = await getGooglePurchaseByPurchaseToken(
    purchaseToken,
    linkedPurchaseToken,
  );

  if (!googleSubscriptionPurchase) {
    throw new APIError('No Subscription Purchase associated with this token');
  }

  const subscriptionPurchase = await updateGoogleSubscriptionPurchase({
    purchaseToken,
    productId,
    googlePurchaseData,
  });
  return subscriptionPurchase;
};

/**
 * Google Play Billing allows programatic modification to each Subscription Purchase.
 *
 * These modifications include;
 *  - cancel: Cancels a user's subscription purchase. The subscription remains valid until its expiration time.
 *  - defer: Defers a user's subscription purchase until a specified future expiration time.
 *  - refund: Refunds a user's subscription purchase, but the subscription remains valid until its expiration time and it will continue to recur
 *  - revoke: Refunds and immediately revokes a user's subscription purchase. Access to the subscription will be terminated immediately and it will stop recurring.
 *
 *  Each modification can be executed by appending the `:<command` string to the end of the purchaseToken while making POST
 *  requests to the Google Purchases API.
 *
 *  Only the `defer` command requires request body parameters.  Otherwise, the body for these API requests should remain empty.
 *  Additionally, only the `defer` command will returns a response body.  Otherwise, successfuly request will return an empty response body.
 *
 */
export const modifyGooglePurchase = async (userId, purchase) => {
  if (!purchase?.data?.purchaseToken || !purchase?.data?.command) {
    throw new APIError('Cannot process subscription without a valid token or command');
  }
  const { data } = purchase;
  await postPurchaseData(data);

  const updatedPurchase = await processGooglePurchase(userId, purchase);
  return updatedPurchase;
};
