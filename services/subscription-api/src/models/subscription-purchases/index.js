import { SubscriptionPurchaseModel } from './SubscriptionPurchaseModel';

export const getSubscriptionPurchasesByUser = async user =>
  SubscriptionPurchaseModel.findOne({ user }).lean();

export const updateSubscriptionPurchasesByUser = async (user, data) =>
  SubscriptionPurchaseModel.updateOne({ user }, data, { upsert: true });
