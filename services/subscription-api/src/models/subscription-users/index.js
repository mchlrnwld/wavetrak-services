import SubscriptionUsersModel from './SubscriptionUsersModel';

export const getSubscriptionUserByUserId = async userId =>
  SubscriptionUsersModel.findOne({ user: userId });

export const getSubscriptionUsersBySubscriptionId = async subscriptionId => {
  return SubscriptionUsersModel.find({ subscriptionId });
};

export const deleteSubscriptionUsersBySubscriptionId = async subscriptionId => {
  return SubscriptionUsersModel.deleteMany({ subscriptionId });
};

export const deleteSubscriptionUserByUserId = async user => {
  return SubscriptionUsersModel.deleteOne({ user });
};

export const getSubscriptionUsersByPrimaryUser = async primaryUser =>
  SubscriptionUsersModel.find({ primaryUser }).populate('user', 'email');

export const updateSubscriptionUsersBySubscriptionId = async (
  oldSubscriptionId,
  newSubscriptionId,
) => {
  return SubscriptionUsersModel.updateMany(
    { subscriptionId: oldSubscriptionId },
    { subscriptionId: newSubscriptionId },
  );
};

export const updateSubscriptionUsersByPrimaryUser = async (oldPrimaryUser, newPrimaryUser) =>
  SubscriptionUsersModel.updateMany(
    { primaryUser: oldPrimaryUser },
    { primaryUser: newPrimaryUser },
  );

export const updateSubscriptionUsersByUser = async (user, data) =>
  SubscriptionUsersModel.updateOne({ user }, data, { upsert: true });

export const upsertSubscriptionUser = async (user, data) => {
  const { subscriptionId, user: primaryUser, type, entitlement, active } = data;
  return SubscriptionUsersModel.findOneAndUpdate(
    { user },
    {
      $set: {
        user,
        primaryUser,
        type,
        subscriptionId,
        entitlement,
        isPrimaryUser: user.toString() === primaryUser.toString(),
        isEntitled: user.toString() === primaryUser.toString(),
        active,
      },
    },
    { upsert: true },
  );
};
