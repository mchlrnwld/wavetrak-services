# Subscription API

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Overview](#overview)
- [Development](#development)
  - [Configuration](#configuration)
  - [Local Development](#local-development)
  - [Testing](#testing)
- [Integrations](#integrations)
  - [Stripe](#stripe)
  - [iTunes / Google Play](#itunes--google-play)
- [Authentication](#authentication)
- [API docs](#api-docs)
  - [V1](#v1)
  - [V2](#v2)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Overview
The Subscription-Service supports data storage of recurring billing information for Wavetrak's Surfline, Buoyweather, and Fishtrack consumer products.  The service is responsible for maintaining the data association between Wavetrak's Subscription APIs, the Billing APIs provided by Stripe, iTunes, and Google Play, and Wavetrak's Data Analytics APIs collectively.

## Development
### Configuration
The Subscription-Service's runtime configuration exists in the `.env-sample` file and lists the `ENV` variables necessary for development in all environments.  The Managed Files or the AWS Secrets Manager store the values for these variables per environment.

The `/infra` directory defines the AWS deployment and policy configuration for the Subscription-Service.

### Local Development
1. Configure the environment
	- Create your `.env` file.
	- Copy/Paste the environment variable names from the `.env-sample` file.
	- Define the environment variable values for your desired environment.

2. Install the Node dependencies
	- Set your Node Version to the [specified Node Engine](./package.json).
	- Run `npm install`.

3. Start the Application
	- Run `npm run startlocal`.
	- The Subscription-Service's default Express port is `8004`.

### Testing
The Subscription-Service leverages API integration testing against all of the RESTful APIs the service exposes.  The service's application code and integration tests are run in the same docker container and connect to the designated database and Stripe API environments to simulate as close to a live environment as possible.  A collection of stubbed JSON objects provides support for Stripe Events, iTunes/Google Notifications, and iTunes/Google Subscription requests.  Otherwise, most of the integration tests make requests directly to the designated Stripe test account (_Surfline Integration Tests_).  The service deletes the test data from each test case before the next test case can run to provide authenticity for each scenario.

**Running the tests**
1. The `.env.test` file defines the configuration credentials required to run the integration.
2. It would be best if you had a local instance of MongoDB installed. A Redis installation is not required.
3. Run `npm run test:integration:local`.

For more info, see [Subscription Backend Integration Tests](./integration-tests/README.md).

## Integrations
The Subscription-Service integrates with with [Stripe](https://stripe.com/docs), [iTunes](https://developer.apple.com/documentation/appstorereceipts), and [Google](https://developers.google.com/android-publisher/api-ref/purchases/subscriptions) for Subscription management.  Additionally, the integrations with Stripe support Subscription [Payment](https://stripe.com/docs/payments) (Credit Cards) and [Billing](https://stripe.com/docs/billing) (Invoices) management.

Integrations are documented in [`docs/v2/integrations`](./docs/v2/integrations).

**Webhooks**

Real-time Subscription notifications (webhooks) are managed with [Stripe's Events](https://stripe.com/docs/billing/webhooks) platform.  The V2 Subscription API supports real-time Subscription Server-to-Server [Subscription Notifications](https://developer.apple.com/documentation/storekit/in-app_purchase/subscriptions_and_offers/enabling_server-to-server_notifications) and [Real-Time Developer Notifications](https://developer.android.com/google/play/billing/realtime_developer_notifications) for iTunes and Google, respectively.

### Stripe
Stripe supports Wavetrak's web-based Subscription payments.  Additional components within the Subscription infrastructure are supported by Stripe as well.  These components include the following:

* Credit Card Tokenization
* Payment Disputes
* Direct Charges (Gift Cards)
* Products and Pricing Plans (Web only)
* Billing Retries
* Coupons and Discounts

### iTunes / Google Play
In-App-Purchase enabled Payments and Auto-Renewing Subscriptions are managed by the application's respective App Store.  These include Google's Play Store Console and Apple's iTunes Connect.

## Authentication

Many endpoints of the subscription service require authentication.

As an API user, either add a query param `?accesstoken=<ACCESS_TOKEN>` or header `X-Auth-AccessToken` to requests when using the public services. Our public service proxy will authenticate the request and pass the authenticated user id as header `X-Auth-UserId` for these services.

## API docs

Both V1 and V2 endpoints are documented in the [`docs/`](https://github.com/Surfline/wavetrak-services/tree/master/services/subscription-service/docs) directory.

### V1

- [V1 API documentation](./docs/v1/V1_API.md)

### V2
 - [Subscriptions](./docs/v2/subscriptions/Subscription_API.md)
 - [Subscription Purchases](./docs/v2/subscriptions/Subscription_Purchase_API.md)
 - [Gift Payments and Redemptions](./docs/v2/subscriptions/Gifts.md)
 - [Payment Alerts](./docs/v2/subscriptions/Payment_Alerts.md)
 - [Admin endpoints](./docs/v2/subscriptions/Admin.md)
 - [Stripe Credit Cards](./docs/v2/subscriptions/Stripe_Credit_Cards.md)
 - [Stripe Invoices](./docs/v2/subscriptions/Stripe_Invoices.md)
 - [Stripe Product Pricing Plans](./docs/v2/subscriptions/Stripe_Pricing_Plans.md)
