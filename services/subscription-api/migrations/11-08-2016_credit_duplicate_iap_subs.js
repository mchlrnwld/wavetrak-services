/*
    On November 4th, 2016 we released a buggy version of the iOS app.
    Premium users saw that they were not entitled and incorrectly bought an IAP subscription.
    Our solution is to credit their Stripe account and send them an email telling them to cancel their IAP subscription.

    Fill in that Stripe prod key and the mongo connection string!
    Usage: `node migrations/11-08-2016_credit_duplicate_iap_subs.js`
 */
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_CONNECTION_STRING);

const options = {
  collection: 'UserAccounts',
  timestamps: true,
};

const UserAccountSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'UserInfo'
  },
  subscriptions: [ mongoose.Schema.Types.Mixed ],
  stripeCustomerId: String
}, options);

const UserAccountModel = mongoose.model('UserAccount', UserAccountSchema);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.on('close', function() {
  console.log('Closing DB Connection and quitting...');
  process.exit(0);
});

var docNumber = 1;
function updateStripeCustomer(doc) {
  setTimeout(function() {
    if (!doc) {
      console.log('endOfCursor');
    } else {
      var stripeCustomerId = doc.stripeCustomerId;
      if (!stripeCustomerId) {
        console.error('No stripeCustomerId for: ' + stripeCustomerId);
      } else {
        var customerSubscriptions = doc.subscriptions;
        var stripePlan = customerSubscriptions[0].stripePlanId;
        var isYearly = stripePlan === 'sl_annual';
        var accountBalance = isYearly ? -81.50 : -25.98; // in dollars, will be converted into cents when updating
        stripe.customers.update(
          stripeCustomerId,
          {
            account_balance: accountBalance * 100 // cents -> dollars
            //account_balance: -10.0 * 100 // cents -> dollars
          },
          function (err, customer) {
            if (err) {
              console.error(err);
            } else {
              console.log('Updated: ' + stripeCustomerId + ' (' + (docNumber++) + ')');
            }
          }
        );
      }
    }
  }, Math.random() * (30000 - 500) + 500); // rate limited with some jitter
}
db.once('open', function() {

  const accountsToCreditCursor = UserAccountModel.aggregate([
    {
      $match: {
        //"stripeCustomerId": "cus_8ermvjfolJJI2H" // michael malchak's account for testing
        "subscriptions.1": { $exists: true },
        "subscriptions.0.type": "stripe",
        "subscriptions.0.periodEnd": { $gte : new Date("2016-11-03T00:00:00Z") },
        "subscriptions.0.expiring": false,
        "subscriptions.1.type": "apple",
        "subscriptions.entitlement": "sl_premium",
        "subscriptions.1.periodStart": { $regex: /^2016\-11.*/ }
      }
    },
    {
      $lookup: {
        from: "UserInfo",
        localField: "user",
        foreignField: "_id",
        as: "userInfo"
      }
    },
    {
      $unwind: '$userInfo'
    },
    {
      $project: {
        'subscriptions': 1,
        'userInfo.email': 1,
        'stripeCustomerId': 1
      }
    }
  ]).cursor({ batchSize: 500 }).exec();

  accountsToCreditCursor.each(function(error, doc) {
    updateStripeCustomer(doc);
  });
});

