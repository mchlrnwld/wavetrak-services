import ProgressBar from 'progress';
import csvWriter from 'csv-writer';
import path from 'path';
import { connectMongo } from '../../src/common/dbContext';
import UserAccountModel from '../../src/models/accounts/UserAccountModel';
import { updatePaymentWarning } from '../../src/models/accounts';
import { getCustomer } from '../../src/external/stripe';

const currPath = path.resolve(process.cwd());
const csvSuccessOutputFilePath = path.join(currPath,
  'migrations/paymentWarnings/output_customers_failed_payment_success.csv');
const csvFailureOutputFilePath = path.join(currPath,
    'migrations/paymentWarnings/output_customers_failed_payment_failure.csv');
const createCsvWriter = csvWriter.createObjectCsvWriter;
const csvOutputHeader = [
  { id: 'userId', title: 'userId' },
  { id: 'stripeCustomerId', title: 'stripeCustomerId' },
  { id: 'brand', title: 'Brand' },
];
const nodeCsvSuccessWriter = createCsvWriter({
  path: csvSuccessOutputFilePath,
  header: [
    ...csvOutputHeader,
        { id: 'cardId', title: 'cardId' }
  ]
});
const nodeCsvFailureWriter = createCsvWriter({
  path: csvFailureOutputFilePath,
  header: [
    ...csvOutputHeader,
        { id: 'error', title: 'Error' }
  ]
});

const shouldInsertFailedPaymentWarning = async (paymentWarnings, subscriptionId) =>
(paymentWarnings.find(item =>
  item.subscriptionId === subscriptionId &&
  item.type === 'failedPayment' &&
  item.status === 'open'));

const processUsers = async (brand) => {
  console.log('BEGIN:=========================================================BEGIN:');
  console.log(`Starting to insert failedPayment warnings for users in retry cycle for ${brand}`);

  const userAccounts = await UserAccountModel.find({
    subscriptions: {
      $elemMatch: {
        type: 'stripe',
        entitlement: `${brand}_premium`,
        expiring: true,
        failedPaymentAttempts: {
          $gt: 0
        },
      }
    }
  });
  const bar = new ProgressBar(':bar', { total: userAccounts.length });
  console.log(`Total Users to be processed = ${userAccounts.length}`);

  for (const user of userAccounts) {
    const { stripeCustomerId, user: userId, paymentWarnings } = user;
    const subscription = user.subscriptions.find(
      sub => (sub.entitlement === `${brand}_premium`)
    );
    if (subscription) {
      const { subscriptionId, stripePlanId } = subscription;
      const record = {
        userId,
        stripeCustomerId,
        brand,
      };
      try {
        const { default_source: chargedCard } = await getCustomer(stripeCustomerId);
        const warning = {
          subscriptionId,
          cardId: chargedCard,
          planId: stripePlanId,
          type: 'failedPayment',
          status: 'open'
        };
        const existingWarning = await shouldInsertFailedPaymentWarning(
          paymentWarnings,
          subscriptionId,
          );
        if (!existingWarning) {
          user.paymentWarnings.push({ ...warning });
          user.markModified('paymentWarnings');
          await user.save();
        }
        if (existingWarning && existingWarning.cardId !== chargedCard) {
          /*
            If a warning exists with a different card then mark the existing warning
            as unresolved and insert a new warning with the new card.
          */
          const statusToUpdate = 'unresolved';
          updatePaymentWarning(user, existingWarning, statusToUpdate);
          // insert warning
          user.paymentWarnings.push({ ...warning });
          user.markModified('paymentWarnings');
          await user.save();
        }
        const success = { ...record, cardId: chargedCard };
        await nodeCsvSuccessWriter.writeRecords([success]);
        bar.tick();
      } catch (error) {
        const failure = { ...record, error: error.message };
        await nodeCsvFailureWriter.writeRecords([failure]);
        bar.tick();
      }
    }
  }
  console.log('END:=========================================================END:');
};

const trigger = async () => {
  await connectMongo();
  await processUsers('sl');
  await processUsers('fs');
  await processUsers('bw');
};

trigger();
