import csvToJson from 'csvtojson';
import csvWriter from 'csv-writer';
import path from 'path';
import ProgressBar from 'progress';
import { getCustomer } from '../../src/external/stripe';
import { connectMongo } from '../../src/common/dbContext';
import UserAccountModel from '../../src/models/accounts/UserAccountModel';


const currPath = path.resolve(process.cwd());
const csvFilePath = path.join(currPath,
  'migrations/paymentWarnings/customers_failed_zip_check.csv');
const csvSuccessOutputFilePath = path.join(currPath,
    'migrations/paymentWarnings/output_customers_failed_zip_check_success.csv');
const csvFailureOutputFilePath = path.join(currPath,
      'migrations/paymentWarnings/output_customers_failed_zip_check_failure.csv');
const createCsvWriter = csvWriter.createObjectCsvWriter;
const csvOutputHeader = [
  { id: 'userId', title: 'userId' },
  { id: 'stripeCustomerId', title: 'stripeCustomerId' },
  { id: 'cardId', title: 'cardId' }
];
const nodeCsvSuccessWriter = createCsvWriter({
  path: csvSuccessOutputFilePath,
  header: [
    ...csvOutputHeader
  ]
});
const nodeCsvFailureWriter = createCsvWriter({
  path: csvFailureOutputFilePath,
  header: [
    ...csvOutputHeader,
    { id: 'error', title: 'Error' }
  ]
});

const shouldInsertInvalidZipWarning = async (paymentWarnings, cardId) => {
  const existingWarning = paymentWarnings.find(item =>
      item.cardId === cardId &&
      item.type === 'invalidZip' &&
      item.status === 'open');
  if (!existingWarning) return true;
  return false;
};

const processUsers = async () => {
  console.log('BEGIN:=========================================================BEGIN:');
  console.log('Starting to insert invalidZip warnings for users who failed Zip check');
  const bar = new ProgressBar(':bar', { total: 10593 });

  csvToJson().fromFile(csvFilePath)
  .subscribe(async(user) => {
    const { userId, stripeCustomerId, cardId } = user;
    const record = {
      userId,
      stripeCustomerId,
      cardId
    };
    try {
      const userAccount = await UserAccountModel.findOne({
        user: userId,
        stripeCustomerId
      });
      if (userAccount) {
        const { paymentWarnings } = userAccount;
        const {
          sources: { data },
          default_source: defaultSource
        } = await getCustomer(stripeCustomerId);
        const card = data.find(({ object, id }) => object === 'card' && id === defaultSource && cardId === defaultSource);
        if (card) {
          // card exists in Stripe for this customer as a default card.
          const shouldInsert = await shouldInsertInvalidZipWarning(paymentWarnings, cardId);
          if (shouldInsert) {
            const warning = {
              subscriptionId: null,
              cardId,
              planId: null,
              type: 'invalidZip',
              status: 'open'
            };
            userAccount.paymentWarnings.push({ ...warning });
            userAccount.markModified('paymentWarnings');
            await userAccount.save();
            bar.tick();
            await nodeCsvSuccessWriter.writeRecords([record]);
          }
        }
      }
    } catch (error) {
      bar.tick();
      const errorRecord = { ...record, error: error.message };
      await nodeCsvFailureWriter.writeRecords([errorRecord]);
    }
  });
  console.log('END:=========================================================END:');
};

const trigger = async () => {
  await connectMongo();
  await processUsers();
};

trigger();
