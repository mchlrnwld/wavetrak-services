import ProgressBar from 'progress';
import csvWriter from 'csv-writer';
import path from 'path';
import { connectMongo } from '../../src/common/dbContext';
import UserAccountModel from '../../src/models/accounts/UserAccountModel';
import { updatePaymentWarning } from '../../src/models/accounts';

const currPath = path.resolve(process.cwd());
const csvSuccessOutputFilePath = path.join(currPath,
  'migrations/paymentWarnings/output_active_subs_payment_warnings_success.csv');
const csvFailureOutputFilePath = path.join(currPath,
    'migrations/paymentWarnings/output_active_subs_payment_warnings_failure.csv');
const createCsvWriter = csvWriter.createObjectCsvWriter;
const csvOutputHeader = [
  { id: 'userId', title: 'userId' },
  { id: 'stripeCustomerId', title: 'stripeCustomerId' },
  { id: 'brand', title: 'Brand' },
];
const nodeCsvSuccessWriter = createCsvWriter({
  path: csvSuccessOutputFilePath,
  header: [
    ...csvOutputHeader,
  ],
  append: true,
});
const nodeCsvFailureWriter = createCsvWriter({
  path: csvFailureOutputFilePath,
  header: [
    ...csvOutputHeader,
        { id: 'error', title: 'Error' }
  ],
  append: true,
});

const processUsers = async (brand) => {
  console.log('BEGIN:=========================================================BEGIN:');
  const userAccounts = await UserAccountModel.find({
    paymentWarnings: {
      $elemMatch: {
        type: 'failedPayment',
        status: 'open',
        planId: {
          $regex: brand,
          $options: '/^/'
        }
      },
    },
  });
  const bar = new ProgressBar(':bar', { total: userAccounts.length });
  console.log(`Total Users to be processed = ${userAccounts.length}`);

  for (const user of userAccounts) {
    const { stripeCustomerId, user: userId, paymentWarnings } = user;
    const subscription = user.subscriptions.find(
      sub => (sub.entitlement === `${brand}_premium`)
    );
    if (subscription && !subscription.expiring && subscription.failedPaymentAttempts === 0) {
      const record = {
        userId,
        stripeCustomerId,
        brand,
      };
      try {
        if (paymentWarnings) {
          const existingWarnings = paymentWarnings.filter(({ type, status, planId }) =>
          type === 'failedPayment' && status === 'open' && planId.startsWith(brand));
          existingWarnings.forEach((warning) => {
            const statusToUpdate = 'resolved';
            updatePaymentWarning(user, warning, statusToUpdate);
          });
          if (existingWarnings.length) {
            user.markModified('paymentWarnings');
            await user.save();
            await nodeCsvSuccessWriter.writeRecords([record]);
            bar.tick();
          }
        }
      } catch (error) {
        const failure = { ...record, error: error.message };
        await nodeCsvFailureWriter.writeRecords([failure]);
        bar.tick();
      }
    }
  }
  console.log('END:=========================================================END:');
};

const trigger = async () => {
  await connectMongo();
  await processUsers('sl');
  await processUsers('fs');
  await processUsers('bw');
};

trigger();
