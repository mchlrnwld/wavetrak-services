import { connectMongo } from '../src/common/dbContext';
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
import UserAccountModel from '../src/models/accounts/UserAccountModel';

const buildSubscriptionObject = (subscription, stripePlanId) => ({
  type: 'stripe',
  subscriptionId: subscription.id,
  periodStart: new Date(subscription.current_period_start * 1000),
  periodEnd: new Date(subscription.current_period_end * 1000),
  entitlement: subscription.plan.metadata.entitlement || 'sl_default',
  stripePlanId,
  createdAt: new Date(),
  expiring: false,
  onTrial: (subscription.status === 'trialing'),
  failedPaymentAttempts: 0
});

const updateSubscription = async (subscriptionId, plan, opts, userAccount) => {
  const existingSubscription = userAccount.subscriptions.find(
    sub => sub.subscriptionId === subscriptionId && sub.stripePlanId);

  const response = await stripe.subscriptions.update(
      subscriptionId,
    {
      plan,
      ...opts
    },
    );
  const newSubscription = buildSubscriptionObject(response, plan);
  userAccount.subscriptions.pull(existingSubscription);
  userAccount.subscriptions.push(newSubscription);
  userAccount.markModified('archivedSubscriptions');
  userAccount.markModified('subscriptions');
  return await userAccount.save();
};

export default async function() {
  console.log('Starting to migrate non-expiring monthly users to new premium');
  await connectMongo();

  const failedUpdates = [];
  const successfulUpdates = [];
  const nonExpiringMonthlyUsers = await UserAccountModel.find({
    'subscriptions.stripePlanId': 'sl_monthly',
    'subscriptions.expiring': false,
  });

  for (const user of nonExpiringMonthlyUsers) {
    /* Get the user's subscription ID for their sl_monthly plan */
    const subscription = user.subscriptions.filter(
      sub => (sub.stripePlanId === 'sl_monthly') && (sub.expiring === false)
    )[0];
    const { subscriptionId, onTrial } = subscription;

    try {
      let trialEnd = 'now';
      if (onTrial) {
        const { trail_end: trialEndDate } = await stripe.subscriptions.retrieve(subscriptionId);
        trialEnd = trialEndDate;
      }

      /* Make the call to update them to v2 with proration FALSE */
      const updatedAccount = await updateSubscription(
        subscriptionId,
        'sl_monthly_v2',
        {
          prorate: false,
          trial_end: trialEnd
        },
        user
      );
      successfulUpdates.push(updatedAccount);
    } catch (error) {
      console.log(`Update failed for sub ID: ${subscriptionId}`);
      failedUpdates.push({ subscriptionId, error });
    }
  }

  console.log(`Successfully migrated ${successfulUpdates.length} users.`);
  console.log(`${failedUpdates.length} users were not migrated.`);
  console.log(JSON.stringify(failedUpdates));
}
