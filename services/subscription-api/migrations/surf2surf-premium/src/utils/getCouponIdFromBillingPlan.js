export default billingPlan => {
  return billingPlan.toLowerCase().includes('year') ? 's2s_welcome_annual' : 's2s_welcome_monthly';
};
