export default billingPlan => {
  return billingPlan.toLowerCase().includes('year') ? 'sl_annual_nzd' : 'sl_monthly_nzd';
};
