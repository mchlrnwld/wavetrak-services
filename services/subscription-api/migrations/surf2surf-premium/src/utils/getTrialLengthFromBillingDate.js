import { addMonths, differenceInDays } from 'date-fns';

export default billingDateInSeconds => {
  const billingDateInMilliseconds = billingDateInSeconds * 1000;
  const trialEndDate = addMonths(new Date(billingDateInMilliseconds), 2);
  const trialPeriodDays = differenceInDays(trialEndDate, new Date());
  return trialPeriodDays;
};
