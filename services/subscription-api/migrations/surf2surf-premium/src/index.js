import { initMongo, setupLogsene } from '@surfline/services-common';
import mongoose from 'mongoose';
import yargs from 'yargs';

setupLogsene(process.env.LOGSENE_KEY);

const { argv } = yargs.option('live').option('overwrite');

Promise.all([initMongo(mongoose, process.env.MONGO_CONNECTION_STRING)])
  .then(async () => {
    // your script here
    // await scriptName(argv.live, argv.active);
    process.exit(0);
  })
  .catch(err => {
    console.log(err);
    process.exit(1);
  });
