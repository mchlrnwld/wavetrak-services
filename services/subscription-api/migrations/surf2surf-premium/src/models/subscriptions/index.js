import { SubscriptionModel } from './SubscriptionModel';

export const getActiveSurflineSubscriptionByUserId = user =>
  SubscriptionModel.findOne({ user, entitlement: 'sl_premium', active: true });

export const getActiveStripeSubscriptionByUserId = user => {
  SubscriptionModel.findOne({ user, active: true, type: 'stripe' });
};
