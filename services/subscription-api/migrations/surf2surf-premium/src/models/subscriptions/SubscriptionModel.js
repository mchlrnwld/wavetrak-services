import mongoose, { Schema } from 'mongoose';

const options = {
  discriminatorKey: 'type',
  collection: 'Subscriptions',
  timestamps: true,
};

const SubscriptionSchema = new mongoose.Schema(
  {
    type: {
      type: Schema.Types.String,
      required: [true, 'A subscription must be of a specific type.'],
      enum: ['stripe', 'apple', 'google', 'gift'],
    },
    subscriptionId: {
      type: Schema.Types.String,
      required: [true, 'A subscription id must be defined.'],
      trim: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
    active: {
      type: Schema.Types.Boolean,
    },
    planId: {
      type: Schema.Types.String,
      required: [true, 'A subscription must include a plan id.'],
      trim: true,
    },
    entitlement: {
      type: Schema.Types.String,
    },
    start: {
      type: Schema.Types.Number,
      required: [true, 'A subscription start timestamp is required.'],
    },
    end: {
      type: Schema.Types.Number,
      required: [true, 'A subscription end timestamp is required.'],
    },
    expired: {
      type: Schema.Types.Number,
    },
    trialing: {
      type: Schema.Types.Boolean,
      required: [true, 'A subscription must indicate a free trial status.'],
      default: false,
    },
    expiring: {
      type: Schema.Types.Boolean,
      required: [true, 'A subscription must indicate a expiring status.'],
      default: false,
    },
    renewalCount: {
      type: Schema.Types.Number,
      default: 0,
    },
    alertPayment: {
      type: Schema.Types.Boolean,
      default: false,
    },
  },
  options,
);

const StripeSchema = new mongoose.Schema(
  {
    stripeCustomerId: {
      type: Schema.Types.String,
      required: [true, 'Stripe subscriptions must contain a customer id.'],
    },
    canceled: {
      type: Schema.Types.Number,
    },
    promotionId: {
      type: Schema.Types.ObjectId,
      ref: 'Promotions',
    },
    failedPaymentAttempts: {
      type: Schema.Types.Number,
      default: 0,
    },
    cardId: {
      type: Schema.Types.String,
    },
    paymentAlertType: {
      type: Schema.Types.String,
    },
    paymentAlertStatus: {
      type: Schema.Types.String,
    },
    paymentAlertReason: {
      type: Schema.Types.String,
    },
    redeemCodes: [Schema.Types.Mixed],
  },
  options,
);

SubscriptionSchema.index({ subscriptionId: 1 }, { unique: true });
SubscriptionSchema.index({ user: 1 });

export const SubscriptionModel = mongoose.model('Subscription', SubscriptionSchema);
export const StripeSubscriptionModel = SubscriptionModel.discriminator('stripe', StripeSchema);
