import mongoose, { Schema } from 'mongoose';

const options = {
  discriminatorKey: 'type',
  collection: 'Surf2SurfSubscriptions',
  timestamps: true,
};

const Surf2SurfSubscriptionSchema = new mongoose.Schema(
  {
    braintreeCustomerId: {
      type: Schema.Types.String,
      required: [true, 'An s2s braintree customer id must be defined.'],
      trim: true,
    },
    braintreeSubscriptionId: {
      type: Schema.Types.String,
      required: [true, 'An s2s braintree subscription id must be defined.'],
      trim: true,
    },
    braintreeTokenId: {
      type: Schema.Types.String,
      required: [true, 'An s2s braintree token id must be defined.'],
      trim: true,
    },
    s2sUserId: {
      type: Schema.Types.String,
      required: [true, 'An s2s user id must be defined.'],
      trim: true,
    },
    user: {
      type: Schema.Types.ObjectId,
    },
    billingPlan: {
      type: Schema.Types.String,
      required: [true, 'A billing plan must be defined'],
      trim: true,
    },
    billingDate: {
      type: Schema.Types.Number,
      required: [true, 'A subscription billingDate is required.'],
    },
    stripeCustomerId: {
      type: Schema.Types.String,
    },
    stripePriceId: {
      type: Schema.Types.String,
      required: [true, 'A stripe price id is required.'],
    },
    stripeCouponId: {
      type: Schema.Types.String,
      required: [true, 'A stripe coupon id is required.'],
    },
    stripeTrialLength: {
      type: Schema.Types.Number,
      required: [true, 'A trial length must be specified'],
    },
    firstName: {
      type: Schema.Types.String,
    },
    lastName: {
      type: Schema.Types.String,
    },
    email: {
      type: Schema.Types.String,
      required: [true, 'An email must be provided'],
    },
    hasExistingActiveSub: {
      type: Schema.Types.Boolean,
    },
    hasPreexistingStripeAccount: {
      type: Schema.Types.Boolean,
    },
    preexistingStripeCustomerId: {
      type: Schema.Types.String,
    },
    migrated: {
      type: Schema.Types.Boolean,
    },
    hasPassword: {
      type: Schema.Types.Boolean,
    },
    currency: {
      type: Schema.Types.String,
    },
  },
  options,
);

Surf2SurfSubscriptionSchema.index({ braintreeCustomerId: 1 }, { unique: true });
Surf2SurfSubscriptionSchema.index({ user: 1 }, { unique: true });
Surf2SurfSubscriptionSchema.index({ s2sUserId: 1 }, { unique: true });
Surf2SurfSubscriptionSchema.index({ stripeCustomerId: 1 });

export const Surf2SurfSubscriptionModel = mongoose.model(
  'Surf2SurfSubscription',
  Surf2SurfSubscriptionSchema,
);
