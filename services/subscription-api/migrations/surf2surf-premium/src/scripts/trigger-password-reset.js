import ProgressBar from 'progress';
import { Surf2SurfSubscriptionModel } from '../models/s2s-migration-docs/Surf2SurfSubscriptionModel';
import triggerPasswordReset from '../external/password-reset-api';

const executePasswordReset = async (liveRun = false, log = console) => {
  log.info(`Live Run: ${liveRun}`);
  log.info(`Starting --- Migration for Password Reset Triggers from Surf2Surf to Wavetrak`);

  let migratedCount = 0;
  const s2sMigrationCount = await Surf2SurfSubscriptionModel.countDocuments({});
  const bar = new ProgressBar(':percent :bar', s2sMigrationCount);

  await Surf2SurfSubscriptionModel.find({ hasPassword: false, migrated: true })
    .lean()
    .cursor()
    .eachAsync(async doc => {
      try {
        const { email } = doc;
        if (liveRun) {
          await triggerPasswordReset(email);
        }
        migratedCount += 1;
      } catch (error) {
        log.error(error);
        log.info(doc.email);
      }
      bar.tick();
    });

  log.info(`Finished --- Migration for Surf2Surf Password Reset`);
  log.info(`Triggered Users:${migratedCount}`);
};

export default executePasswordReset;
