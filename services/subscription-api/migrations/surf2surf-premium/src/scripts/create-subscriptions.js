import ProgressBar from 'progress';
import { Surf2SurfSubscriptionModel } from '../models/s2s-migration-docs/Surf2SurfSubscriptionModel';
import { createSubscription as createStripeSubscription } from '../external/stripe';

const createSubscriptions = async (liveRun = false, log = console) => {
  log.info(`Live Run: ${liveRun}`);
  log.info(`Starting --- Migrating Subscriptions from Surf2Surf to Wavetrak`);

  let migratedCount = 0;
  const s2sMigrationCount = await Surf2SurfSubscriptionModel.countDocuments({
    hasExistingActiveSub: false,
    hasPreexistingStripeAccount: false,
    migrated: { $ne: true },
    stripeCustomerId: { $exists: true },
  });
  const bar = new ProgressBar(':percent :bar', s2sMigrationCount);

  await Surf2SurfSubscriptionModel.find({
    hasExistingActiveSub: false,
    hasPreexistingStripeAccount: false,
    migrated: { $ne: true },
    stripeCustomerId: { $exists: true },
  })
    .cursor()
    .eachAsync(async doc => {
      try {
        const {
          stripeCustomerId,
          stripePriceId,
          user,
          stripeTrialLength,
          stripeCouponId,
          braintreeSubscriptionId,
        } = doc;
        if (liveRun) {
          await createStripeSubscription(stripeCustomerId, {
            priceId: stripePriceId,
            trialPeriodDays: stripeTrialLength,
            coupon: stripeCouponId,
            metadata: {
              user: user.toString(),
              braintreeSubscriptionId,
              migrated: true,
            },
          });
          // eslint-disable-next-line no-param-reassign
          doc.migrated = true;
          await doc.save();
        }
        migratedCount += 1;
      } catch (error) {
        log.info(doc.user);
        log.error(error);
      }
      bar.tick();
    });

  log.info(`Finished --- Migration for Surf2Surf Subscriptions`);
  log.info(`Migrated Users:${migratedCount}`);
};

export default createSubscriptions;
