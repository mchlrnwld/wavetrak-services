import { createObjectCsvWriter } from 'csv-writer';
import { Surf2SurfSubscriptionModel } from '../../models/s2s-migration-docs/Surf2SurfSubscriptionModel';
import { createCustomer } from '../../external/stripe';

const seedStripeCustomers = async (liveRun = false, log = console) => {
  log.info(`Live Run: ${liveRun}`);
  log.info(`Starting --- Seed Test Stripe Customers from Surf2Surf to Wavetrak`);

  const stripeCustomers = createObjectCsvWriter({
    path: `./csv/stripe-customers-test.csv`,
    header: [{ old_id: 'old_id', customerId: 'customerId' }],
    append: true,
  });

  await Surf2SurfSubscriptionModel.find({
    hasExistingActiveSub: false,
    hasPreexistingStripeAccount: false,
  })
    .lean()
    .cursor()
    .eachAsync(async doc => {
      const metadata = { old_id: doc.braintreeCustomerId };
      const { id } = await createCustomer({ metadata });
      console.log(doc.braintreeCustomerId, id);
      const record = { old_id: doc.braintreeCustomerId, customerId: id };
      await stripeCustomers.writeRecords([record]);
    });
};

export default seedStripeCustomers;
