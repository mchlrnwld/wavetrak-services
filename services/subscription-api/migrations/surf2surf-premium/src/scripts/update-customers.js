import csvtojson from 'csvtojson';
import ProgressBar from 'progress';
import { Surf2SurfSubscriptionModel } from '../models/s2s-migration-docs/Surf2SurfSubscriptionModel';
import { updateCustomer } from '../external/stripe';

const updateCustomers = async (liveRun = false, log = console) => {
  log.info(`Live Run: ${liveRun}`);
  log.info(`Starting --- Updating Stripe Customers from Surf2Surf`);

  const s2sMigrationCount = await Surf2SurfSubscriptionModel.countDocuments({});
  const bar = new ProgressBar(':percent :bar', s2sMigrationCount);

  return csvtojson()
    .fromFile('csv/stripe-customers-test.csv')
    .subscribe(async data => {
      const {
        old_customer_id: braintreeCustomerId,
        old_card_id: braintreeTokenId,
        customer_id: stripeCustomerId,
        card_id: stripeCardId,
      } = data;
      try {
        if (liveRun) {
          const doc = await Surf2SurfSubscriptionModel.findOneAndUpdate(
            { braintreeCustomerId, braintreeTokenId, hasPreexistingStripeAccount: false },
            { stripeCustomerId },
          ).lean();
          if (doc) {
            const { user, s2sUserId, firstName, lastName, email, braintreeSubscriptionId } = doc;
            const metadata = {
              userId: user.toString(),
              s2sUserId,
              braintreeSubscriptionId,
              braintreeTokenId,
              firstName,
              lastName,
              email,
            };
            await updateCustomer(stripeCustomerId, {
              email,
              name: `${firstName} ${lastName}`,
              default_source: stripeCardId,
              metadata,
            });
            bar.tick();
          }
        }
      } catch (error) {
        log.info(`Error caught updating customer ${stripeCustomerId}`);
        log.error(error);
      }
    });
};

export default updateCustomers;
