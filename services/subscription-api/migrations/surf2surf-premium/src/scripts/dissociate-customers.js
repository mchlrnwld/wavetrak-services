import ProgressBar from 'progress';
import { Surf2SurfSubscriptionModel } from '../models/s2s-migration-docs/Surf2SurfSubscriptionModel';
import { getActiveStripeSubscriptionByUserId } from '../models/subscriptions';
import dissociateStripeCustomer from '../external/admin-customers-api';

const dissociateStripeCustomer = async (liveRun = false, log = console) => {
  log.info(`Live Run: ${liveRun}`);
  log.info(`Starting --- Migration for Customer Dissociation from Stripe`);

  let migratedCount = 0;
  const s2sMigrationCount = await Surf2SurfSubscriptionModel.countDocuments({
    hasPreexistingStripeAccount: true,
    migrated: { $exists: false },
    dissociatedCustomer: { $exists: false },
  });
  const bar = new ProgressBar(':percent :bar', s2sMigrationCount);

  await Surf2SurfSubscriptionModel.find({
    hasPreexistingStripeAccount: true,
    migrated: { $exists: false },
    dissociatedCustomer: { $exists: false },
  })
    .cursor()
    .eachAsync(async doc => {
      try {
        const { user, preexistingStripeCustomerId } = doc;
        if (liveRun) {
          const existingSubscription = await getActiveStripeSubscriptionByUserId(user);
          if (!existingSubscription) {
            await dissociateStripeCustomer(preexistingStripeCustomerId)
            doc.dissociatedCustomer = true;
            await doc.save();
          }
        }
        migratedCount += 1;
      } catch (error) {
        log.error(error);
        log.info(doc.email);
      }
      bar.tick();
    });

  log.info(`Finished --- Migration for Surf2Surf Customer Dissociation`);
  log.info(`Triggered Users:${migratedCount}`);
};

export default dissociateStripeCustomer;
