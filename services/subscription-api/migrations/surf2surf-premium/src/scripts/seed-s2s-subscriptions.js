import csvtojson from 'csvtojson';
import { Surf2SurfSubscriptionModel } from '../models/s2s-migration-docs/Surf2SurfSubscriptionModel';
import StripeCustomerModel from '../models/customers/StripeCustomerModel';
import { getActiveSurflineSubscriptionByUserId } from '../models/subscriptions';
import {
  getPriceIdFromBillingPlan,
  getCouponIdFromBillingPlan,
  getTrialLengthFromBillingDate,
} from '../utils';

const createMigrationDocs = async (liveRun = false, log = console) => {
  log.info(`Live Run: ${liveRun}`);
  log.info(`Starting --- Seed Subscription Migration Docs from Surf2Surf to Wavetrak`);

  return csvtojson()
    .fromFile('csv/s2s-subscriptions.csv')
    .subscribe(async data => {
      try {
        const {
          braintreeCustomerId,
          braintreeSubscriptionId,
          paymentToken,
          systemId,
          _id,
          billingPlan,
          nextBillingDate,
          firstName,
          lastName,
          email,
        } = data;
        const stripePriceId = getPriceIdFromBillingPlan(billingPlan);
        const stripeCouponId = getCouponIdFromBillingPlan(billingPlan);
        const stripeTrialLength = getTrialLengthFromBillingDate(nextBillingDate);
        const hasActiveSubscription = await getActiveSurflineSubscriptionByUserId(_id);
        const stripeCustomer = await StripeCustomerModel.findOne({ user: _id }).lean();
        if (liveRun) {
          await Surf2SurfSubscriptionModel.create({
            braintreeCustomerId,
            braintreeSubscriptionId,
            braintreeTokenId: paymentToken,
            s2sUserId: systemId,
            user: _id,
            billingPlan,
            billingDate: nextBillingDate,
            stripePriceId,
            stripeCouponId,
            stripeTrialLength,
            firstName,
            lastName,
            email: email.toLowerCase().trim(),
            hasExistingActiveSub: !!hasActiveSubscription,
            hasPreexistingStripeAccount: !!stripeCustomer,
            preexistingStripeCustomerId: stripeCustomer ? stripeCustomer.stripeCustomerId : null,
          });
        }
      } catch (error) {
        log.error(error);
      }
    });
};

export default createMigrationDocs;
