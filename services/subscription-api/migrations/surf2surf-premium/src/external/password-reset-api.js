import axios from 'axios';

export default async email => {
  await axios.get(`${process.env.PASSWORD_RESET_API}?email=${email}&brand=sl`);
};
