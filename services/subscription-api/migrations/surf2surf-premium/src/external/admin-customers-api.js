import axios from 'axios';

export default async customerId => {
  await axios.delete(`${process.env.ADMIN_CUSTOMERS_API}`, { data: { customerId } });
};
