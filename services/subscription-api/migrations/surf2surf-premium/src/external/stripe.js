const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

export const getCustomer = async customerId => {
  return stripe.customers.retrieve(customerId, { expand: ['default_source'] });
};

export const createCustomer = async ({ metadata }) => {
  return stripe.customers.create({ metadata });
};

export const updateCustomer = async (customerId, data) => {
  // eslint-disable-next-line camelcase
  const { metadata, email, name, default_source } = data;
  return stripe.customers.update(customerId, {
    email,
    name,
    default_source,
    metadata,
  });
};

export const createSubscription = async (customerId, data) => {
  const { priceId, trialPeriodDays, coupon, metadata } = data;
  return stripe.subscriptions.create({
    customer: customerId,
    items: [{ price: priceId }],
    trial_period_days: trialPeriodDays,
    coupon,
    metadata,
  });
};
