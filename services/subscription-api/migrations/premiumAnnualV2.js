import ProgressBar from 'progress';
import { connectMongo } from '../src/common/dbContext';
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
import UserAccountModel from '../src/models/accounts/UserAccountModel';

const updateSubscription = async (subscriptionId, plan, opts) => {
  const { id, customer } = await stripe.subscriptions.update(
    subscriptionId,
    { plan, ...opts },
  );
  return { id, customer };
};

const migrate = async (oldPlanId, newPlanId) => {
  console.log(`Starting to migrate non-expiring ${oldPlanId} users to ${newPlanId}`);
  await connectMongo();

  const failedUpdates = [];
  const successfulUpdates = [];
  const nonExpiringAnnualUsers = await UserAccountModel.find({
    'subscriptions.stripePlanId': oldPlanId,
    'subscriptions.expiring': false,
  });
  const totalBarTicks = nonExpiringAnnualUsers.length;
  const bar = new ProgressBar(':percent :bar', {
    width: 50,
    total: totalBarTicks
  });

  for (const user of nonExpiringAnnualUsers) {
    /* Get the user's subscription ID for their sl_monthly plan */
    const subscription = user.subscriptions.filter(
      sub => (sub.stripePlanId === oldPlanId) && (sub.expiring === false)
    )[0];
    if (subscription) {
      const { subscriptionId, onTrial } = subscription;

      try {
        let trialEnd = 'now';
        if (onTrial) {
          const { trail_end: trialEndDate } = await stripe.subscriptions.retrieve(subscriptionId);
          trialEnd = trialEndDate;
        }

        /* Make the call to update them to v2 with proration FALSE */
        const updatedAccount = await updateSubscription(
          subscriptionId,
          newPlanId,
          {
            prorate: false,
            trial_end: trialEnd
          },
          user
        );
        successfulUpdates.push(updatedAccount);
        bar.tick();
      } catch (error) {
        console.log(`Update failed for sub ID: ${subscriptionId}`);
        bar.tick();
        failedUpdates.push({ subscriptionId, error });
      }
    }
  }

  console.log(`Successfully migrated ${successfulUpdates.length} users.`);
  console.log(`${failedUpdates.length} users were not migrated.`);
  console.log(JSON.stringify(failedUpdates));
};

migrate('bw_annual', 'bw_annual_v2');
