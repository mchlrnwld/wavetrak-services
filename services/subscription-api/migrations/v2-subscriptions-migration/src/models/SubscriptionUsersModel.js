import mongoose, { Schema } from 'mongoose';

const options = {
  collection: 'SubscriptionUsers',
  timestamps: true,
};

const SubscriptionUsersSchema = new mongoose.Schema(
  {
    subscriptionId: {
      type: Schema.Types.String,
      required: [true, 'A subscription id must be defined.'],
      trim: true,
    },
    type: {
      type: Schema.Types.String,
      required: [true, 'A subscription must be of a specific type.'],
      enum: ['stripe', 'apple', 'google', 'gift'],
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
    entitlement: {
      type: Schema.Types.String,
    },
    primaryUser: {
      type: Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
    isPrimaryUser: {
      type: Schema.Types.Boolean,
    },
    active: {
      type: Schema.Types.Boolean,
    },
  },
  options,
);

SubscriptionUsersSchema.index({ user: 1 });

export default mongoose.model('SubscriptionUser', SubscriptionUsersSchema);
