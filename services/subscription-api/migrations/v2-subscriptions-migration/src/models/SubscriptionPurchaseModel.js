import mongoose, { Schema } from 'mongoose';

const options = {
  discriminatorKey: 'type',
  collection: 'SubscriptionPurchases',
  timestamps: true,
};

const SubscriptionPurchaseSchema = mongoose.Schema(
  {
    type: {
      type: String,
      required: [true, 'A purchase item must be of a specific type.'],
      enum: ['itunes', 'google-play', 'stripe'],
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
    expired: {
      type: Boolean,
    },
  },
  options,
);

SubscriptionPurchaseSchema.index({ user: 1 });

export const SubscriptionPurchaseModel = mongoose.model(
  'SubscriptionPurchase',
  SubscriptionPurchaseSchema,
);
