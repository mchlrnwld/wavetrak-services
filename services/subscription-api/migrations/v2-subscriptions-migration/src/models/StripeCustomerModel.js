import mongoose, { Schema } from 'mongoose';

const StripeCustomerSchema = mongoose.Schema(
  {
    stripeCustomerId: {
      type: Schema.Types.String,
      required: [true, 'StripeCustomers must have a stripeCustomerId.'],
      unique: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'UserInfo',
      required: [true, 'StripeCustomers must have a user.'],
      unique: true,
    },
  },
  {
    collection: 'StripeCustomers',
    timestamps: true,
  },
);

export default mongoose.model('StripeCustomer', StripeCustomerSchema);
