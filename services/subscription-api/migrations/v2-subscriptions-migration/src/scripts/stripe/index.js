import ProgressBar from 'progress';
import UserAccountModel from '../../models/UserAccountModel';
import { SubscriptionModel, StripeSubscriptionModel } from '../../models/SubscriptionModel';
import StripeCustomerModel from '../../models/StripeCustomerModel';

const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

export const stripeProductMap = {
  sl: 'sl_premium',
  fs: 'fs_premium',
  bw: 'bw_premium',
};

export const getEntitlementFromStripePlanId = planId => {
  const deconstructedPlanId = planId.split('_');
  return stripeProductMap[deconstructedPlanId[0]];
};

const getSubscriptionFromStripe = async subscriptionId =>
  stripe.subscriptions.retrieve(subscriptionId);

const getPaymentAlertAttrs = (subscriptionId, paymentWarnings = [], expired) => {
  const paymentWarning = paymentWarnings.find(({ subscriptionId: id }) => id === subscriptionId);
  if (paymentWarning && paymentWarning.type === 'failedPayment') {
    return {
      paymentAlertType: 'failedPayment',
      paymentAlertStatus: !expired ? paymentWarning.status : 'unresolved',
    };
  }
  return {};
};

const createStripeSubscription = async ({ v1Sub, user, paymentWarnings, upsert }) => {
  const { subscriptionId: v1SubscriptionId, renewalCount, failedPaymentAttempts } = v1Sub;
  const data = await getSubscriptionFromStripe(v1SubscriptionId);
  const {
    id: subscriptionId,
    customer: stripeCustomerId,
    current_period_start: start,
    current_period_end: end,
    plan: { id: planId },
    cancel_at_period_end: expiring,
    ended_at: expired,
    canceled_at: canceled,
    metadata: { promotionId, redeemCode },
    status,
  } = data;

  const stripeSubscriptionAttrs = {
    type: 'stripe',
    subscriptionId,
    user,
    active: !expired,
    planId,
    entitlement: getEntitlementFromStripePlanId(planId),
    start,
    end,
    trialing: status === 'trialing',
    expiring,
    renewalCount,
    stripeCustomerId,
    failedPaymentAttempts,
    migratedFromV1: true,
  };
  const expiredAttr = expired ? { expired } : {};
  const canceledAttr = canceled ? { canceled } : {};
  const promotionAttrs = promotionId ? { promotionId, redeemCodes: [redeemCode] } : null;
  const paymentAlertAttrs = getPaymentAlertAttrs(subscriptionId, paymentWarnings, expired);

  const newSubscriptionData = {
    ...stripeSubscriptionAttrs,
    ...promotionAttrs,
    ...paymentAlertAttrs,
    ...expiredAttr,
    ...canceledAttr,
  };
  if (upsert) {
    return StripeSubscriptionModel.updateOne({ subscriptionId }, newSubscriptionData, { upsert });
  }
  return SubscriptionModel.create(newSubscriptionData);
};

const upsertStripeCustomer = (user, stripeCustomerId) =>
  StripeCustomerModel.updateOne({ stripeCustomerId }, { user, stripeCustomerId }, { upsert: true });

const migrateStripeSubscriptions = async (liveRun = false, active = false, log = console) => {
  let migratedCount = 0;
  log.info(`Live Run: ${liveRun}, Active: ${active}`);
  log.info(`Starting --- Migration for Stripe Subscriptions from UserAccounts to Subscriptions`);

  const query = active
    ? { 'subscriptions.type': 'stripe' }
    : { 'archivedSubscriptions.type': 'stripe' };
  const userAccountsCount = await UserAccountModel.countDocuments(query);

  log.info(`${userAccountsCount} accounts with subscriptions found.`);

  const bar = new ProgressBar(':percent :bar', userAccountsCount);

  await UserAccountModel.find(query)
    .lean()
    .cursor()
    .eachAsync(async doc => {
      const subscriptions = active
        ? doc.subscriptions.filter(({ type }) => type === 'stripe')
        : doc.archivedSubscriptions.filter(({ type }) => type === 'stripe');
      for (const subscription of subscriptions) {
        const previouslyMigrated = await SubscriptionModel.findOne({
          subscriptionId: subscription.subscriptionId,
          type: 'stripe',
        });
        const { user, paymentWarnings, stripeCustomerId } = doc;
        if (!previouslyMigrated && liveRun) {
          try {
            await createStripeSubscription({
              v1Sub: subscription,
              user,
              paymentWarnings,
              upsert: false,
            });
            await upsertStripeCustomer(user, stripeCustomerId);
            migratedCount += 1;
          } catch (error) {
            log.error(error.type, error?.raw?.message, subscription.subscriptionId);
          }
        } else if (previouslyMigrated && liveRun) {
          try {
            if (!previouslyMigrated.active) {
              await createStripeSubscription({
                v1Sub: subscription,
                user,
                paymentWarnings,
                upsert: true,
              });
              await upsertStripeCustomer(user, stripeCustomerId);
              migratedCount += 1;
            } else if (active) {
              log.info(
                'matching active subscription exists',
                previouslyMigrated.user,
                previouslyMigrated.subscriptionId,
              );
            }
          } catch (error) {
            log.error(error);
          }
        }
        bar.tick();
      }
    });

  log.info(`Finished --- Migration for Stripe Subscriptions`);
  log.info(`Migrated Users:${migratedCount}`);
};

export default migrateStripeSubscriptions;
