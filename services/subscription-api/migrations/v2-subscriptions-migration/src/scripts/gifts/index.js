import ProgressBar from 'progress';
import UserAccountModel from '../../models/UserAccountModel';
import { SubscriptionModel, GiftSubscriptionModel } from '../../models/SubscriptionModel';

const updateV2GiftSubscription = async (v1subscription, user, active) => {
  const {
    brand,
    entitlement,
    expiring,
    giftRedemptionCodes,
    onTrial,
    planId,
    periodEnd,
    periodStart,
    subscriptionId,
    type,
  } = v1subscription;
  await GiftSubscriptionModel.findOneAndUpdate(
    { subscriptionId },
    {
      active,
      brand,
      chargeId: null,
      end: periodEnd / 1000,
      expiring,
      planId,
      entitlement,
      redeemCodes: giftRedemptionCodes,
      start: periodStart / 1000,
      subscriptionId,
      trialing: onTrial,
      type,
      user,
    },
  );
};

const createV2GiftSubscription = async (v1subscription, user, active) => {
  const {
    brand,
    entitlement,
    expiring,
    giftRedemptionCodes,
    onTrial,
    planId,
    periodEnd,
    periodStart,
    subscriptionId,
    type,
  } = v1subscription;
  await GiftSubscriptionModel.create({
    active,
    brand,
    chargeId: null,
    end: Math.round(periodEnd / 1000),
    expiring,
    planId,
    entitlement,
    redeemCodes: giftRedemptionCodes,
    start: Math.round(periodStart / 1000),
    subscriptionId,
    trialing: onTrial,
    type,
    user,
  });
};

const migrateGiftSubscriptions = async (liveRun = false, overwrite = false, log = console) => {
  let migratedCount = 0;
  let existingCount = 0;

  log.info(`Live Run: ${liveRun}, Overwrite: ${overwrite}`);
  log.info(
    `Starting --- Migration for active Gift Subscriptions from UserAccounts to Subscriptions`,
  );
  const activeUserAccountsCount = await UserAccountModel.countDocuments({
    'subscriptions.type': 'gift',
  });
  log.info(`${activeUserAccountsCount} accounts with active subscriptions found.`);

  let bar = new ProgressBar(':percent :bar', activeUserAccountsCount);

  await UserAccountModel.find({ 'subscriptions.type': 'gift' })
    .cursor()
    .eachAsync(async doc => {
      for (const subscription of doc.subscriptions) {
        bar.tick();
        const previouslyMigrated = await SubscriptionModel.exists({
          subscriptionId: subscription.subscriptionId,
          type: 'gift',
        });
        if (previouslyMigrated) {
          if (overwrite && liveRun) {
            await updateV2GiftSubscription(subscription, doc.user, true);
          }
          existingCount += 1;
        } else if (subscription.type === 'gift') {
          if (liveRun) {
            await createV2GiftSubscription(subscription, doc.user, true);
          }
          migratedCount += 1;
        }
      }
    });

  log.info(`Finished --- Migration for active Gift Subscriptions`);
  log.info(`Migrated Users:${migratedCount}, Existing Users:${existingCount}`);

  log.info(
    `Starting --- Migration for expired Gift Subscriptions from UserAccounts to Subscriptions`,
  );
  const expiredUserAccountsCount = await UserAccountModel.countDocuments({
    'archivedSubscriptions.type': 'gift',
  });
  log.info(`${expiredUserAccountsCount} accounts with expired subscriptions found.`);

  bar = new ProgressBar(':percent :bar', expiredUserAccountsCount);
  existingCount = 0;
  migratedCount = 0;

  await UserAccountModel.find({ 'archivedSubscriptions.type': 'gift' })
    .cursor()
    .eachAsync(async doc => {
      for (const subscription of doc.archivedSubscriptions) {
        bar.tick();
        const previouslyMigrated = await SubscriptionModel.exists({
          subscriptionId: subscription.subscriptionId,
          type: 'gift',
        });
        if (previouslyMigrated) {
          if (overwrite && liveRun) {
            await updateV2GiftSubscription(subscription, doc.user, false);
          }
          existingCount += 1;
        } else if (subscription.type === 'gift') {
          if (liveRun) {
            await createV2GiftSubscription(subscription, doc.user, false);
          }
          migratedCount += 1;
        }
      }
    });

  log.info(`Finished --- Migration for expired Gift Subscriptions`);
  log.info(`Migrated Subscriptions: ${migratedCount}, Existing Subscriptions: ${existingCount}`);
};

export default migrateGiftSubscriptions;
