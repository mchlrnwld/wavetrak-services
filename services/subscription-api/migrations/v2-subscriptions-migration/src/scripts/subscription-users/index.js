import ProgressBar from 'progress';
import UserAccountModel from '../../models/UserAccountModel';
import { SubscriptionModel } from '../../models/SubscriptionModel';
import SubscriptionUsersModel from '../../models/SubscriptionUsersModel';

const createSubscriptionUser = async ({ subscription, userId }) => {
  const { subscriptionId, type, entitlement, user: primaryUser, active } = subscription;
  await SubscriptionUsersModel.create({
    subscriptionId,
    type,
    entitlement,
    user: userId,
    primaryUser,
    isPrimaryUser: userId.toString() === primaryUser.toString(),
    active,
  });
};

const aggregation = numOfDuplicates => [
  {
    $group: {
      _id: '$subscriptions.subscriptionId',
      userIds: { $push: '$user' },
      docCount: { $sum: 1 },
    },
  },
  {
    $match: {
      docCount: { $eq: numOfDuplicates },
    },
  },
];

const setupSubscriptionUsers = async (liveRun = false, log = console) => {
  let migratedCount = 0;

  log.info(`Live Run: ${liveRun}`);
  log.info(
    `Starting --- Migration for active Gift Subscriptions from UserAccounts to Subscriptions`,
  );

  const v1Duplicates = await UserAccountModel.aggregate(aggregation(2));
  const bar = new ProgressBar(':percent :bar', v1Duplicates.length);

  for (const doc of v1Duplicates) {
    const {
      _id: [subscriptionId],
      userIds,
    } = doc;
    const subscription = await SubscriptionModel.findOne({ subscriptionId });
    if (subscription) {
      for (const userId of userIds) {
        if (liveRun) {
          await createSubscriptionUser({ subscription, userId });
        }
      }
      migratedCount += 1;
    }
    bar.tick();
  }
  log.info(`Finished --- Migration for SubscriptionUsers`);
  log.info(`Migrated Users:${migratedCount}`);
};

export default setupSubscriptionUsers;
