import { resolve } from 'path';
import fs from 'fs';
import csvToJson from 'csvtojson';
import { write } from 'fast-csv';
import _remove from 'lodash.remove';
import UserAccountModel from '../../models/UserAccountModel';
import { SubscriptionModel } from '../../models/SubscriptionModel';
import { SubscriptionPurchaseModel } from '../../models/SubscriptionPurchaseModel';
import SubscriptionUsersModel from '../../models/SubscriptionUsersModel';
import {
  validateAppleReceipt,
  fetchAppleReceipt,
  validateGoogleReceipt,
  fetchGoogleReceipt,
} from '../../external';
import * as analytics from '../../analytics';

const cleanupAppleIAPSubs = async (log = console) => {
  try {
    // File path of the V1 V2 Comparison csv file
    const filePath = resolve(__dirname, 'v1v2_apple_users_test.csv');
    const errors = [];
    await csvToJson({
      trim: true,
    })
      .fromFile(filePath)
      .subscribe(
        async (data) => {
          const { user: userId } = data;
          const { receipts, subscriptions, user } = await UserAccountModel.findOne({
            user: userId,
          });
          console.log('%ccleanup.js line:24 userId', 'color: #007acc;', userId);
          const appleSubscriptionIds = subscriptions
            .filter(({ type }) => type === 'apple')
            .map(({ subscriptionId }) => {
              return subscriptionId;
            });

          for (const subscriptionId of appleSubscriptionIds) {
            const receipt = receipts.find(({ purchases }) => {
              const purchase = purchases.find(({ transactionId, originalTransactionId }) => {
                return subscriptionId === transactionId || subscriptionId === originalTransactionId;
              });
              if (purchase) return true;
            });
            if (receipt) {
              try {
                const { entitlement } = subscriptions.find(
                  ({ subscriptionId: subId }) => subId === subscriptionId,
                );
                // Verify once again if V2 Subscription exists or not
                const v2SubscriptionExists = await SubscriptionModel.exists({
                  user,
                  type: 'apple',
                  entitlement,
                });
                if (v2SubscriptionExists) return;

                // V2 Sub do not exist, proceed with creating V2 Subscription
                const { receiptData } = receipt;
                await validateAppleReceipt(userId, receiptData);
              } catch (error) {
                const errorMessage = error?.response?.data?.message;
                const errorFormatted = error.toJSON();
                const { message } = errorFormatted;
                let subUserInserted = false;
                if (errorMessage === 'This receipt is subscribed to a different account') {
                  const receiptData = error?.config?.data;
                  const formattedData = JSON.parse(receiptData);
                  const { purchase } = formattedData;
                  const { pending_renewal_info } = await fetchAppleReceipt(purchase);
                  const originalTransactionId = pending_renewal_info[0].original_transaction_id;
                  const subscriptionPurchaseResult = await SubscriptionPurchaseModel.findOne({
                    originalTransactionId,
                  });
                  if (subscriptionPurchaseResult) {
                    const primaryUser = subscriptionPurchaseResult.user;
                    const primarySubscription = await SubscriptionModel.find({
                      user: primaryUser,
                      active: true,
                    });
                    const {
                      subscriptionId: subId,
                      type,
                      entitlement,
                      active,
                    } = primarySubscription[0];
                    const record = {
                      subscriptionId: subId,
                      type,
                      entitlement,
                      user: userId,
                      primaryUser,
                      isPrimaryUser: userId.toString() === primaryUser.toString(),
                      active,
                    };
                    await SubscriptionUsersModel.create(record);
                    subUserInserted = true;
                  }
                }
                errors.push({
                  user,
                  subUserInserted,
                  error: message,
                  detail: errorMessage,
                });
              }
            }
          }
        },
        (error) => {
          log.error(error);
          process.exit(1);
        },
        () => {
          if (errors.length) {
            const errorFilePath = resolve(__dirname, 'v1v2_apple_users_error.csv');
            log.info(`Writing Error output to file`);
            const ws = fs.createWriteStream(errorFilePath, { flags: 'a' });
            write(errors, { headers: true }).pipe(ws);
          }
          log.info('Done');
          process.exit(0);
        },
      );
  } catch (err) {
    log.error('%cindex.js line:92 err', 'color: #007acc;', err);
  }
};

const cleanupGoogleIAPSubs = async (log = console) => {
  try {
    // File path of the V1 V2 Comparison csv file
    const filePath = resolve(__dirname, 'v1v2_google_users_test.csv');
    const errors = [];
    await csvToJson({
      trim: true,
    })
      .fromFile(filePath)
      .subscribe(
        async (data) => {
          const { user: userId } = data;
          const { receipts, subscriptions, user } = await UserAccountModel.findOne({
            user: userId,
          });
          console.log('%ccleanup.js line:24 userId', 'color: #007acc;', userId);
          const googleSubscriptionIds = subscriptions
            .filter(({ type }) => type === 'google')
            .map(({ subscriptionId }) => {
              return subscriptionId;
            });

          for (const subscriptionId of googleSubscriptionIds) {
            const receipt = receipts.find(({ purchases }) => {
              const purchase = purchases.find(({ transactionId }) => {
                return subscriptionId === transactionId;
              });
              if (purchase) return true;
            });
            if (receipt) {
              try {
                const { entitlement } = subscriptions.find(
                  ({ subscriptionId: subId }) => subId === subscriptionId,
                );
                // Verify once again if V2 Subscription exists or not
                const v2SubscriptionExists = await SubscriptionModel.exists({
                  user,
                  type: 'google',
                  entitlement,
                });
                if (v2SubscriptionExists) return;

                // V2 Sub do not exist, proceed with creating V2 Subscription
                const { receiptData } = receipt;
                await validateGoogleReceipt(userId, receiptData);
              } catch (error) {
                let subArchived = false;
                const errorMessage = error?.response?.data?.message;
                const errorFormatted = error.toJSON();
                const { message } = errorFormatted;
                // Something went wrong, handle it
                if (message === 'Request failed with status code 500') {
                  const receiptData = error?.config?.data;
                  const formattedData = JSON.parse(receiptData);
                  const { purchase } = formattedData;
                  // Get the receipt latest status from Google
                  const status = await fetchGoogleReceipt(purchase?.data);
                  // Check if it's expired
                  if (status === 'Request failed with status code 410') {
                    const currentSubscription = subscriptions.find(
                      ({ subscriptionId: subId }) => subId === subscriptionId,
                    );
                    // Check if periodEnd is in the past, then this subscription is inactive
                    if (new Date(currentSubscription?.periodEnd) < new Date()) {
                      const userAccount = await UserAccountModel.findOne({
                        user: userId,
                      });
                      // Remove the subscription
                      _remove(subscriptions, (elem) => elem.subscriptionId === subscriptionId);
                      // Archive the subscription
                      userAccount.archivedSubscriptions.push({
                        ...currentSubscription,
                        archivedDate: new Date(),
                      });
                      userAccount.subscriptions = subscriptions;
                      userAccount.markModified('subscriptions');
                      userAccount.markModified('archivedSubscriptions');
                      await userAccount.save();
                      const subscriptionTraits = {
                        'subscription.active': false,
                        'subscription.in_trial': false,
                        'subscription.in_grace_period': false,
                        'subscription.trial_eligible': false,
                        'subscription.plan_id': currentSubscription.planId,
                      };
                      analytics.identify({
                        userId: userId.toString(),
                        traits: subscriptionTraits,
                      });
                      subArchived = true;
                    }
                  }
                }

                errors.push({
                  user,
                  error: message,
                  detail: errorMessage,
                  subArchived,
                });
              }
            }
          }
        },
        (error) => {
          log.error(error);
          process.exit(1);
        },
        () => {
          if (errors.length) {
            const errorFilePath = resolve(__dirname, 'v1v2_google_users_error.csv');
            log.info(`Writing Error output to file`);
            const ws = fs.createWriteStream(errorFilePath, { flags: 'a' });
            write(errors, { headers: true }).pipe(ws);
          }
          log.info('Done');
          process.exit(0);
        },
      );
  } catch (err) {
    log.error('%cindex.js line:92 err', 'color: #007acc;', err);
  }
};

export default cleanupGoogleIAPSubs;
