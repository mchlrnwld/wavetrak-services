import { resolve } from 'path';
import fs from 'fs';
import { write } from 'fast-csv';
import csvToJson from 'csvtojson';
import ProgressBar from 'progress';
import UserAccountModel from '../../models/UserAccountModel';
import { SubscriptionModel } from '../../models/SubscriptionModel';
import SubscriptionUsersModel from '../../models/SubscriptionUsersModel';
import { fetchAppleReceipt, fetchGoogleReceipt } from '../../external';

const compareV1V2IAPSubscriptions = async (log = console) => {
  let affectedUserCount = 0;
  log.info('Starting --- Compare V1 and V2 subsriptions');
  try {
    // CSV file path to which the results are written
    const filePath = resolve(__dirname, 'v1v2_comparison_1.csv');

    // Fetch all IAP subscriptions
    const query = { 'subscriptions.type': { $in: ['apple', 'google'] } };
    const userAccountsCount = await UserAccountModel.countDocuments(query);
    log.info(`${userAccountsCount} accounts with subscriptions found.`);

    const bar = new ProgressBar(':percent :bar', userAccountsCount);

    const dataToWrite = [];

    // Get existing file data to verify if the user is already processed
    let fileData;
    if (fs.existsSync(filePath)) {
      fileData = await csvToJson({
        trim: true,
      }).fromFile(filePath);
    }

    const MAX_LIMIT = 100;
    for (let skip = 0; ; skip += MAX_LIMIT) {
      const userAccountDocs = await UserAccountModel.find(query)
        .limit(MAX_LIMIT)
        .skip(skip)
        .lean();
      // eslint-disable-next-line no-loop-func
      const promises = userAccountDocs.map(async doc => {
        const { user } = doc;
        // Check if user is already processed. If yes then exit immidietly
        const userAlreadyProcessed =
          fileData && fileData.some(elem => elem.user.toString() === user.toString());
        if (userAlreadyProcessed) return;

        const subscriptions = doc.subscriptions.filter(
          ({ type }) => type === 'apple' || type === 'google',
        );
        for (const subscription of subscriptions) {
          const v2SubscriptionExists = await SubscriptionModel.exists({
            user: doc.user,
            type: subscription.type,
          });
          const v2SubscriptionUserExists = await SubscriptionUsersModel.exists({
            user: doc.user,
            type: subscription.type,
          });
          if (!v2SubscriptionExists) {
            dataToWrite.push({
              user: doc.user,
              subscriptionId: subscription.subscriptionId,
              type: subscription.type,
              SubscriptionUser: !!v2SubscriptionUserExists,
            });
            affectedUserCount += 1;
          }
          bar.tick();
        }
      });
      await Promise.all(promises);
      log.info('%cindex.js line:74 skip', 'color: #007acc;', skip);
      if (skip >= userAccountsCount) break;
    }

    log.info(`Writing output to file`);
    const ws = fs.createWriteStream(filePath, { flags: 'a' });
    write(dataToWrite, { headers: true }).pipe(ws);

    log.info(`Finished --- Compare V1 and V2 subsriptions`);
    log.info(`Affected Users:${affectedUserCount}`);
  } catch (err) {
    log.error('%cindex.js line:11 err', 'color: #007acc;', err);
  }
};

const verifyAppleReceipts = async (log = console) => {
  try {
    // File path of the V1 V2 Comparison csv file
    const filePath = resolve(__dirname, 'v1v2_comparison_1.csv');
    const dataToWrite = [];
    const noReceipts = [];
    await csvToJson({
      trim: true,
    })
      .fromFile(filePath)
      .subscribe(
        async data => {
          const { user: userId, type: receiptType } = data;
          if (receiptType !== 'apple') return;
          const { receipts, subscriptions, user } = await UserAccountModel.findOne({
            user: userId,
          });
          const appleSubscriptionIds = subscriptions
            .filter(({ type }) => type === 'apple')
            .map(({ subscriptionId }) => {
              return subscriptionId;
            });
          for (const subscriptionId of appleSubscriptionIds) {
            const receipt = receipts.find(({ purchases }) => {
              const purchase = purchases.find(({ transactionId, originalTransactionId }) => {
                return subscriptionId === transactionId || subscriptionId === originalTransactionId;
              });
              if (purchase) return true;
            });
            if (!receipt) {
              noReceipts.push({
                user,
                type: 'apple',
                subscriptionId,
              });
            }
            if (receipt) {
              try {
                const { receiptData } = receipt;
                const iTunesReceiptData = await fetchAppleReceipt(receiptData);
                const { status } = iTunesReceiptData;
                dataToWrite.push({
                  user,
                  type: 'apple',
                  status,
                });
              } catch (error) {
                log.error(error);
              }
            }
          }
        },
        error => {
          log.error(error);
          process.exit(1);
        },
        () => {
          log.info(`Writing output to file`);
          const fileOutpuPath = resolve(__dirname, 'v1v2_receipts_verification_apple.csv');
          const ws = fs.createWriteStream(fileOutpuPath, { flags: 'a' });
          write(dataToWrite, { headers: true }).pipe(ws);

          const fileOutpuPath1 = resolve(__dirname, 'v1v2_no_receipts_apple.csv');
          const ws1 = fs.createWriteStream(fileOutpuPath1, { flags: 'a' });
          write(noReceipts, { headers: true }).pipe(ws1);

          log.info('Done');
          process.exit(0);
        },
      );
  } catch (err) {
    log.error('%cindex.js line:92 err', 'color: #007acc;', err);
  }
};

const verifyGoogleReceipts = async (log = console) => {
  try {
    // File path of the V1 V2 Comparison csv file
    const filePath = resolve(__dirname, 'v1v2_comparison_1.csv');
    const dataToWrite = [];
    await csvToJson({
      trim: true,
    })
      .fromFile(filePath)
      .subscribe(
        async data => {
          const { user: userId, type: receiptType } = data;
          if (receiptType !== 'google') return;
          const { receipts, subscriptions, user } = await UserAccountModel.findOne({
            user: userId,
          });
          const googleSubscriptionIds = subscriptions
            .filter(({ type }) => type === 'google')
            .map(({ subscriptionId }) => {
              return subscriptionId;
            });
          for (const subscriptionId of googleSubscriptionIds) {
            const receipt = receipts.find(({ purchases }) => {
              const purchase = purchases.find(({ transactionId }) => {
                return transactionId === subscriptionId;
              });
              if (purchase) return true;
            });
            if (receipt) {
              try {
                const { receiptData } = receipt;
                const status = await fetchGoogleReceipt(receiptData?.data);
                dataToWrite.push({
                  user,
                  type: 'google',
                  status,
                });
              } catch (error) {
                log.error(error);
              }
            }
          }
        },
        error => {
          log.error(error);
          process.exit(1);
        },
        () => {
          log.info(`Writing output to file`);
          const fileOutpuPath = resolve(__dirname, 'v1v2_receipts_verification_google.csv');
          const ws = fs.createWriteStream(fileOutpuPath, { flags: 'a' });
          write(dataToWrite, { headers: true }).pipe(ws);
          log.info('Done');
          process.exit(0);
        },
      );
  } catch (err) {
    log.error('%cindex.js line:92 err', 'color: #007acc;', err);
  }
};

// Supporting script/useful functions
// const verifyV2SubscriptionPurchase = async () => {
//   let affectedUserCount = 0;
//   // log.info('Starting --- Compare V1 and V2 subsriptions');
//   try {
//     // CSV file path to which the results are written
//     const filePath = resolve(__dirname, 'v1v2_comparison_1.csv');
//     // Get existing file data to verify if the user is already processed
//     let fileData;
//     if (fs.existsSync(filePath)) {
//       fileData = await csvToJson({
//         trim: true,
//       }).fromFile(filePath);
//     }
//     fileData.forEach(async record => {
//       const { user } = record;
//       const v2SubscriptionPurchaseModelExists = await SubscriptionPurchaseModel.exists({
//         user,
//       });
//       if (v2SubscriptionPurchaseModelExists) {
//         console.log('%cindex.js line:106 user', 'color: #007acc;', user);
//         affectedUserCount += 1;
//       }
//     });
//     console.log('%cindex.js line:110 affectedUserCount', 'color: #007acc;', affectedUserCount);
//   } catch (err) {
//     console.log('%cindex.js line:11 err', 'color: #007acc;', err);
//   }
// };

export default verifyAppleReceipts;
