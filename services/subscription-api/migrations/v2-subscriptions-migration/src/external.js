import axios from 'axios';

export const fetchAppleReceipt = async (receipt) => {
  const body = {
    'receipt-data': receipt,
    password: process.env.APPLE_IAP_PASSWORD,
  };
  const response = await axios({
    method: 'post',
    url: `${process.env.ITUNES_API}`,
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
    data: JSON.stringify(body),
  });

  return response.data;
};

const formatPurchaseApi = ({ packageName, productId, purchaseToken, command }) => {
  if (command) {
    return `https://www.googleapis.com/androidpublisher/v3/applications/${packageName}/purchases/subscriptions/${productId}/tokens/${purchaseToken}:${command}`;
  }
  return `https://www.googleapis.com/androidpublisher/v3/applications/${packageName}/purchases/subscriptions/${productId}/tokens/${purchaseToken}`;
};

// const getGoogleAccessToken = async () => {
//   const response = await axios({
//     method: 'POST',
//     url: 'https://accounts.google.com/o/oauth2/token',
//     headers: {
//       accept: 'application/json',
//       'content-type': 'application/json',
//     },
//     data: JSON.stringify({
//       grant_type: 'refresh_token',
//       client_id: process.env.PLAY_STORE_API_CLIENT_ID,
//       client_secret: process.env.PLAY_STORE_API_CLIENT_SECRET,
//       refresh_token: process.env.PLAY_STORE_API_REFRESH_TOKEN,
//     }),
//   });

//   return response.data;
// };

export const fetchGoogleReceipt = async ({ packageName, purchaseToken, productId }) => {
  const opts = {
    packageName,
    productId,
    purchaseToken,
    accessToken: process.env.PLAY_STORE_API_ACCESS_TOKEN,
  };
  try {
    const computedPurchaseApi = formatPurchaseApi(opts);
    await axios({
      method: 'GET',
      url: computedPurchaseApi,
      headers: {
        authorization: `Bearer ${opts.accessToken}`,
      },
    });
    return 'success';
  } catch (err) {
    const errorFormatted = err.toJSON();
    return errorFormatted.message;
  }
};

export const validateAppleReceipt = async (userId, purchase) => {
  const body = {
    purchase,
  };
  const response = await axios({
    method: 'post',
    url: `${process.env.SUBSCRIPTIONS_API}/itunes`,
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      'x-auth-userid': userId,
    },
    data: JSON.stringify(body),
  });

  return response.data;
};

export const validateGoogleReceipt = async (userId, purchase) => {
  const body = {
    purchase,
  };
  const response = await axios({
    method: 'post',
    url: `${process.env.SUBSCRIPTIONS_API}/google`,
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      'x-auth-userid': userId,
    },
    data: JSON.stringify(body),
  });

  return response.data;
};
