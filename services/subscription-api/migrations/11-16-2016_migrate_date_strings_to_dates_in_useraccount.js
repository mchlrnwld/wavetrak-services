/*
  In-app-purchase subscriptions were erroneously stored as date strings.
  This sweeps through the UserAccount collection in MongoDB and updates field types to dates.
 */
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_CONNECTION_STRING);

console.log("Starting UserAccount date field type migration");

const UserAccountSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'UserInfo'
  },
  archivedSubscriptions: [ mongoose.Schema.Types.Mixed ],
  subscriptions: [ mongoose.Schema.Types.Mixed ]
}, {
  collection: 'UserAccounts',
  timestamps: true,
});
const UserAccountModel = mongoose.model('UserAccount', UserAccountSchema);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {

  var mongoCollection = UserAccountModel;
  var dateStringFields = ["periodStart", "periodEnd", "createdAt"];
  var subscriptionArrayFields = ["subscriptions", "archivedSubscriptions"];
  mongoCollection.find({ $or: [ { "subscriptions.periodEnd": { $type: 2 } }, { "archivedSubscriptions.periodEnd": { $type: 2 } } ] }).cursor()
    .on('data', function(userAccount){

      // Loop through "subscriptions" & "archivedSubscriptions"
      // Adds update instructions for date fields to update with $set operator
      var $setInstructions = {};
      for (var subscriptionTypeIndex in subscriptionArrayFields) {
        var targetArrayName = subscriptionArrayFields[subscriptionTypeIndex];
        for (var targetArrayIndex in userAccount[targetArrayName]) {
          for (var d in dateStringFields) {
            var dateStringFieldName = dateStringFields[d];
            var targetField = targetArrayName + "." + targetArrayIndex + "." + dateStringFieldName;
            var targetFieldContent = userAccount[targetArrayName][targetArrayIndex][dateStringFieldName];
            if (typeof  targetFieldContent === "string") {

              // Add specific date field to instruction object for update's $set operator
              $setInstructions[targetField] = new Date(targetFieldContent);
            }
          }
        }
      }

      console.log(userAccount._id.toString() + ', ' + userAccount.user.toString());
      mongoCollection.update(
        { _id: userAccount._id },
        { $set: $setInstructions }
      );
    })
    .on('error', function(err){
      console.error(err);
    })
    .on('end', function(){
      console.log("Done migrating UserAccount date field type.");
    });
});
