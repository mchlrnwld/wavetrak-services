import ProgressBar from 'progress';
import { createObjectCsvWriter } from 'csv-writer';
import MigratedUserModel from '../models/MigratedUserModel';
import UserAccountModel from '../models/UserAccountModel';
import { createCustomer } from '../external/stripe';

const createStripeCustomer = async (doc) => {
  const { cwUserId, eWayToken, firstName, lastName, email, wtUserId } = doc;
  const { id: stripeCustomerId } = await createCustomer({
    email,
    name: `${firstName} ${lastName}`,
    description: `This customer has been migrated from Costalwatch Plus`,
    metadata: {
      cwUserId,
      name: `${firstName} ${lastName}`,
      firstName,
      lastName,
      eWayToken,
      userId: wtUserId.toString(),
    },
  });
  return stripeCustomerId;
};

export default async () => {
  const query = {
    stripeCustomerId: { $exists: false },
    hasActiveSubscription: false,
    hasActiveMarineStripeSubscription: false,
    eWayToken: { $ne: null },
  };
  try {
    const total = await MigratedUserModel.countDocuments(query);
    const bar = new ProgressBar(':percent :bar', {
      width: 50,
      total,
    });
    const cwToSTripeCustomerMap = createObjectCsvWriter({
      path: 'costalwatch_to_stripe_customer.csv',
      header: [
        { id: 'customerId', title: 'Stripe Customer ID' },
        { id: 'eWayToken', title: 'TokenCustomerId' },
        { id: 'cwUserId', title: 'Costalwatch User Id' },
      ],
    });

    await MigratedUserModel.find(query)
      .cursor()
      .eachAsync(async (doc) => {
        bar.tick();
        const { cwUserId, eWayToken, wtUserId } = doc;

        const userAccount =
          (await UserAccountModel.findOne({
            user: wtUserId,
            stripeCustomerId: { $exists: true },
          })) || {};

        const { stripeCustomerId: existingCustomerId, archivedSubscriptions } = userAccount;
        let customerId = existingCustomerId;

        if (!existingCustomerId) {
          const stripeCustomerId = await createStripeCustomer(doc);
          customerId = stripeCustomerId;
        }

        // dissociate inactive stripe customer and create a new customer
        if (existingCustomerId) {
          // Maintain a reference to the disassociated Stripe account for past subscriptions
          const archive = archivedSubscriptions || [];
          const updatedArchive = archive.map((subscription) => {
            if (subscription.type === 'stripe' && !subscription.stripeCustomerId) {
              return { ...subscription, stripeCustomerId: existingCustomerId };
            }
            return subscription;
          });

          const newStripeCustomerId = await createStripeCustomer(doc);
          userAccount.archivedSubscriptions = updatedArchive;
          userAccount.stripeCustomerId = newStripeCustomerId;

          userAccount.markModified('archivedSubscriptions');
          userAccount.markModified('stripeCustomerId');
          await userAccount.save();
          customerId = newStripeCustomerId;
        }

        // eslint-disable-next-line no-param-reassign
        doc.stripeCustomerId = customerId;
        await doc.save();

        const csvRow = [{ customerId, eWayToken, cwUserId }];
        await cwToSTripeCustomerMap.writeRecords(csvRow);
      });
  } catch (error) {
    throw new Error(error.message);
  }
};
