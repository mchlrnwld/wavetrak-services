import ProgressBar from 'progress';
import MigratedUserModel from '../models/MigratedUserModel';
import UserAccountModel from '../models/UserAccountModel';
import * as stripe from '../external/stripe';
import getExtendedTrialLength from '../utils/getExtendedTrialLength';
import getStripePlan from '../utils/getStripePlan';

/**
 * @description: Script to create Stripe Subscription for eligible CW Plus Memebers
 *
 * @step_1 - Cursor through MigratedUsers collection
 * @step_2 - Define the projected renewal date and trial length of each subscription.  Also
 *  determine the appropriate Plan.
 * @step_3 - Build the UserAccount document and included the stripeCustomerId and subscription details
 * @step_4 - Create a new trialing Subscription in stripe
 *
 */
export default async () => {
  const query = {
    hasActiveSubscription: false,
    hasActiveMarineStripeSubscription: false,
    stripeCustomerId: { $exists: true },
  };
  try {
    const total = await MigratedUserModel.countDocuments(query);
    const bar = new ProgressBar(':percent :bar', {
      width: 50,
      total,
    });

    /* Step 1. */
    await MigratedUserModel.find(query)
      .cursor()
      .eachAsync(async (doc) => {
        bar.tick();
        const { wtUserId, plan, renewalDate, stripeCustomerId, expiring } = doc;

        /* Step 2 */
        const trialLength = getExtendedTrialLength(renewalDate, 3);
        const planId = getStripePlan[plan];

        /* Step 3 */
        const account =
          (await UserAccountModel.findOne({ stripeCustomerId })) ||
          new UserAccountModel({ user: wtUserId });
        account.stripeCustomerId = stripeCustomerId;

        /* Step 4 */
        const subscription = await stripe.createSubscription({
          customer: stripeCustomerId,
          plan: planId,
          numTrialDays: trialLength,
          expiring: !!expiring,
          taxRateId: process.env.STRIPE_TAX_RATE_ID,
        });

        account.subscriptions.push(subscription);
        account.markModified('subscriptions');
        await account.save();
      });
  } catch (error) {
    throw new Error(error.message);
  }
};
