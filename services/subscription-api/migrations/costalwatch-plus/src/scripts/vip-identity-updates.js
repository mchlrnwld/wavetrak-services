import ProgressBar from 'progress';
import MigratedUserModel from '../models/MigratedUserModel';
import { updateSubscriptionIdentity } from '../external/segment';
import computeExpiresDate from '../utils/computeExpiresDate';

/**
 * @description Script to update a vip customer's Segment Identity trait
 *
 * @step_1 - Cursor through the MigratedUsers collection for VIP Members
 * @step_2 - Computed the updated expiration date
 * @step_3 - Call Segment HHTP API to update identity
 *
 */
export default async () => {
  const query = {
    isSlVip: true,
  };
  try {
    const total = await MigratedUserModel.countDocuments(query);
    const bar = new ProgressBar(':percent :bar', {
      width: 50,
      total,
    });
    await MigratedUserModel.find(query)
      .cursor()
      .eachAsync(async (doc) => {
        bar.tick();
        const { wtUserId, renewalDate } = doc;
        const expiresDate = computeExpiresDate(renewalDate);

        await updateSubscriptionIdentity({
          userId: wtUserId.toString(),
          subscriptionExpiration: expiresDate,
        });
      });
  } catch (error) {
    throw new Error(error.message);
  }
};
