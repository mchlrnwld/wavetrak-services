import Chance from 'chance';
import MigratedUserModel from '../models/MigratedUserModel';

const rChance = new Chance();

const getRandomCwUserId = () => {
  return Math.round(Math.random() * (1000 - 1) + 1);
};
const getRandomAnnualRenewalDate = () => {
  return Math.round(Math.random() * (1634955945 - 1603419945) + 1603419945);
};
const getRandomMonthlyRenewalDate = () => {
  return Math.round(Math.random() * (1606098345 - 1603419945) + 1603419945);
};

const getRandomAlphaNum = () => {
  return rChance.integer({ min: 1000, max: 10000 });
};

const getRandomFirstName = () => {
  return rChance.first({ nationality: 'en' });
};

const getRandomLastName = () => {
  return rChance.last({ nationality: 'en' });
};

export default async ({ amount, plan, expiring, emailSuffix, vip }) => {
  for (let i = 0; i < amount; i += 1) {
    const firstName = getRandomFirstName();
    const lastName = getRandomLastName();
    const renewalDate =
      plan === 'ANNUAL' ? getRandomAnnualRenewalDate() : getRandomMonthlyRenewalDate();

    await MigratedUserModel.create({
      cwUserId: getRandomCwUserId(),
      firstName,
      lastName,
      email: `${firstName.toLowerCase()}_${lastName.toLowerCase()}${emailSuffix}`,
      plan,
      expiring,
      eWayToken: vip ? null : getRandomAlphaNum(),
      renewalDate,
    });
  }
};
