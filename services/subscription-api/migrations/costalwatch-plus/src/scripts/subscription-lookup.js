import ProgressBar from 'progress';
import MigratedUserModel from '../models/MigratedUserModel';
import UserAccountModel from '../models/UserAccountModel';

/**
 * @description: Script to determine if a Migrated User has an Active Surfline Subscription
 *
 * @step1 - Cursor through the MigratedUsers collection
 * @step2 - For each MigratedUser document, perform a UserAccount lookup
 * @step3 - An exsiting issue with the Subscription infrastructure allows multiple
 *   UserAccount documents for a single Wavetrak user.  This issue has not been resolved
 *   at the time of this script creation.  If any UserAccount document has an active
 *   Surfline subscription, we can updated the `hasActiveSubscription` property on
 *   the MigratedUser Document.  This property is used to skip subscription creation
 *   in the subscription migration script.
 */

export default async () => {
  try {
    const total = await MigratedUserModel.countDocuments();
    const bar = new ProgressBar(':percent :bar', {
      width: 50,
      total,
    });

    /* Step 1 */
    await MigratedUserModel.find()
      .cursor()
      .eachAsync(async (doc) => {
        bar.tick();
        const { wtUserId } = doc;

        /* Step 2 */
        const accounts = await UserAccountModel.find({ user: wtUserId });

        /* Step 3 */
        if (accounts && accounts.length) {
          const isSubscribed = accounts
            .map(({ subscriptions }) => subscriptions)
            .flat()
            .some(({ entitlement }) => entitlement.includes('sl'));

          const isMarineSubscribed = accounts
            .map(({ subscriptions }) => subscriptions)
            .flat()
            .some(({ type, entitlement }) => {
              return (
                entitlement.includes('bw') || (entitlement.includes('fs') && type === 'stripe')
              );
            });

          if (isSubscribed) {
            // eslint-disable-next-line no-param-reassign
            doc.hasActiveSubscription = true;
          }
          if (isMarineSubscribed) {
            // eslint-disable-next-line no-param-reassign
            doc.hasActiveMarineStripeSubscription = true;
          }
        }

        await doc.save();
      });
  } catch (error) {
    throw new Error(error.message);
  }
};
