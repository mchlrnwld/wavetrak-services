import ProgressBar from 'progress';
import MigratedUserModel from '../models/MigratedUserModel';
import UserAccountModel from '../models/UserAccountModel';
import { requestGiftCode, redeemGiftCode } from '../external/gift';
import computeExpiresDate from '../utils/computeExpiresDate';

/**
 * @description Script to create Gift Subscription for VIP Coastalwatch Members
 *
 * @step_1 - Cursor through the MigratedUsers collection for VIP Members
 * @step_2 - Define the projected expiration date of each Gift Subscription
 * @step_3 - Call the admin Gift API to generate a gift code for each Members
 * @step_4 - Make a request to the Gift Redeem API to redeem the gift code
 * @step_5 - Modify the end date of the gift subscription in the UserAccount collection
 * @step_6 - Mark the MigratedUser as migrated for VIP
 *
 */
export default async () => {
  const query = {
    hasActiveSubscription: false,
    eWayToken: null,
  };
  try {
    const total = await MigratedUserModel.countDocuments(query);
    const bar = new ProgressBar(':percent :bar', {
      width: 50,
      total,
    });
    await MigratedUserModel.find(query)
      .cursor()
      .eachAsync(async (doc) => {
        bar.tick();
        const { wtUserId, renewalDate } = doc;
        const [giftCode] = await requestGiftCode();
        const {
          subscription: { subscriptionId: giftSubscriptionId },
        } = await redeemGiftCode(wtUserId, giftCode);

        // eslint-disable-next-line no-param-reassign
        doc.hasActiveSubscription = true;
        // eslint-disable-next-line no-param-reassign
        doc.isSlVip = true;

        await doc.save();
        const expiresDate = computeExpiresDate(renewalDate);
        const userAccount = await UserAccountModel.findOne({
          'subscriptions.subscriptionId': giftSubscriptionId,
        });
        const subscription = userAccount.subscriptions.find(
          ({ subscriptionId }) => subscriptionId === giftSubscriptionId,
        );
        subscription.periodEnd = expiresDate;
        userAccount.markModified('subscriptions');
        await userAccount.save();
      });
  } catch (error) {
    throw new Error(error.message);
  }
};
