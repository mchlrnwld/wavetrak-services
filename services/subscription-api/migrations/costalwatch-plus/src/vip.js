import { initMongo, setupLogsene, createLogger } from '@surfline/services-common';
import mongoose from 'mongoose';
import vipGiftMigration from './scripts/vip-gift-migration';
import vipIdentityUpdates from './scripts/vip-identity-updates';

setupLogsene(process.env.LOGSENE_KEY);
const log = createLogger('migrate-cw-vip-subscriptions');

Promise.all([initMongo(mongoose, process.env.MONGO_CONNECTION_STRING)])
  .then(async () => {
    await vipGiftMigration();
    await vipIdentityUpdates();
  })
  .catch((err) => {
    log.error({
      message: 'Fatal Error running vip migration script',
      errorMessage: err.message,
      stack: err.stack,
    });
    process.exit(1);
  });
