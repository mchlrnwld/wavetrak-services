import { initMongo, setupLogsene, createLogger } from '@surfline/services-common';
import mongoose from 'mongoose';
import subscriptionMigration from './scripts/subscription-migration';

setupLogsene(process.env.LOGSENE_KEY);
const log = createLogger('migrate-subscriptions-into-stripe');

Promise.all([initMongo(mongoose, process.env.MONGO_CONNECTION_STRING)])
  .then(async () => {
    await subscriptionMigration();
  })
  .catch((err) => {
    log.error({
      message: 'Fatal Error running subscription migration script',
      errorMessage: err.message,
      stack: err.stack,
    });
    process.exit(1);
  });
