import { initMongo, setupLogsene, createLogger } from '@surfline/services-common';
import mongoose from 'mongoose';
import subscriptionLookup from './scripts/subscription-lookup';

setupLogsene(process.env.LOGSENE_KEY);
const log = createLogger('lookup active subscriptions');

Promise.all([initMongo(mongoose, process.env.MONGO_CONNECTION_STRING)])
  .then(async () => {
    await subscriptionLookup();
  })
  .catch((err) => {
    log.error({
      message: 'Fatal Error running subscription lookup script',
      errorMessage: err.message,
      stack: err.stack,
    });
    process.exit(1);
  });
