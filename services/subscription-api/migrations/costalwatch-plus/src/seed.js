import { initMongo, setupLogsene, createLogger } from '@surfline/services-common';
import mongoose from 'mongoose';
import seed from './scripts/seedMigratedUsers';

setupLogsene(process.env.LOGSENE_KEY);
const log = createLogger('seed-migrated-users');

Promise.all([initMongo(mongoose, process.env.MONGO_CONNECTION_STRING)])
  .then(async () => {
    await seed({
      amount: 1,
      plan: 'ANNUAL',
      expiring: false,
      emailSuffix: '@malchak.33mail.com',
    });
    await seed({
      amount: 1,
      plan: 'ANNUAL',
      expiring: false,
      emailSuffix: '@malchak.33mail.com',
      vip: true,
    });
  })
  .catch((err) => {
    log.error({
      message: 'Error seeding MigratedUsers',
      errorMessage: err.message,
      stack: err.stack,
    });
    process.exit(1);
  });
