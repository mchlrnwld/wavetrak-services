import { initMongo, setupLogsene, createLogger } from '@surfline/services-common';
import mongoose from 'mongoose';
import createCustomers from './scripts/create-customers';

setupLogsene(process.env.LOGSENE_KEY);
const log = createLogger('create-cw-plus-stripe-customers');

Promise.all([initMongo(mongoose, process.env.MONGO_CONNECTION_STRING)])
  .then(async () => {
    await createCustomers();
  })
  .catch((err) => {
    log.error({
      message: 'Fatal Error running customer creation script',
      errorMessage: err.message,
      stack: err.stack,
    });
    process.exit(1);
  });
