import { addMonths, fromUnixTime, differenceInCalendarDays } from 'date-fns';

export default (unixTime, numTrialExtensionMonths) => {
  const originalRenewalDate = fromUnixTime(unixTime);
  const renewalDate = addMonths(originalRenewalDate, numTrialExtensionMonths);
  const trialLength = differenceInCalendarDays(renewalDate, Date.now());
  return trialLength < 90 ? 90 : trialLength;
};
