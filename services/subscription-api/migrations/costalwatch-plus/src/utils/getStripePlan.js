export default {
  MONTHLY: process.env.MONTHLY_PLAN,
  ANNUAL: process.env.ANNUAL_PLAN,
};
