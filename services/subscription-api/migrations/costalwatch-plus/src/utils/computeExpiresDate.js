import { addMonths, fromUnixTime, differenceInCalendarDays } from 'date-fns';

export default (renewalDate) => {
  const originalRenewalDate = fromUnixTime(renewalDate);
  const newRenewalDate = addMonths(originalRenewalDate, 3);
  const difference = differenceInCalendarDays(newRenewalDate, Date.now());
  if (difference < 90) {
    return addMonths(Date.now(), 3);
  }
  return newRenewalDate;
};
