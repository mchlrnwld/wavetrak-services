const stripe = require('stripe')(process.env.STRIPE_API_KEY);

/**
 * A function to transform the Stripe Subscription into the UserAccount Subscription Contract
 * */
const buildSubscriptionObject = (subscription, stripePlanId) => ({
  type: 'stripe',
  subscriptionId: subscription.id,
  periodStart: new Date(subscription.current_period_start * 1000),
  periodEnd: new Date(subscription.current_period_end * 1000),
  entitlement: subscription.plan.metadata.entitlement || 'sl_premium',
  stripePlanId,
  createdAt: new Date(),
  expiring: false,
  onTrial: subscription.status === 'trialing',
  failedPaymentAttempts: 0,
});

/**
 * Creates a stripe customer
 *
 * @param {Object} customer
 * @returns {Promise}
 */
export const createCustomer = (customer) =>
  new Promise((resolve, reject) => {
    stripe.customers.create(customer, (err, data) => {
      if (err) return reject(err);
      return resolve(data);
    });
  });

/**
 * Updates a stripe customer
 *
 * @param customerId
 * @param props
 * @returns {Promise}
 */
export const updateCustomer = (customerId, opts) =>
  new Promise((resolve, reject) => {
    stripe.customers.update(customerId, opts, (err, customer) => {
      if (err) return reject(err);
      return resolve(customer);
    });
  });

/**
 * Retrieves a user's stripe customer account given a stripe customer id.
 *
 * @param customerId stripe customer id.
 * @returns {Promise} stripe customer object.
 */
export const getCustomer = (customerId) =>
  new Promise((resolve, reject) => {
    stripe.customers.retrieve(customerId, (err, customer) => {
      if (err) return reject(err);
      return resolve(customer);
    });
  });

/**
 * Creates a subscription in Stripe
 * https://stripe.com/docs/api/subscriptions/create
 *
 * @typedef {Object} data
 * @property {String} customer
 * @property {String} plan
 * @property {Number} numTrialDays
 * @property {enum} payment_behavior
 *
 * The payment_behavior options attribute is required so the Subscription is not
 * created in an 'incomplete' state, which is the default.
 *
 * @returns {Promise} Resolves to a Stripe subscription object
 *
 */
export const createSubscription = ({ customer, plan, numTrialDays, expiring, taxRateId }) =>
  new Promise((resolve, reject) => {
    const opts = {
      trial_period_days: numTrialDays,
      payment_behavior: 'error_if_incomplete',
      cancel_at_period_end: expiring,
      default_tax_rates: taxRateId ? [taxRateId] : [],
      metadata: {
        migrated: 1,
      },
    };
    stripe.subscriptions.create({ customer, plan, ...opts }, (err, subscription) => {
      if (err) return reject(err);
      return resolve(buildSubscriptionObject(subscription, plan));
    });
  });

/**
 * Cancels a stripe subscription
 *
 * @param subscriptionId
 * @param options {object} | cancelAtPeriodEnd
 * @returns subscription
 */
export const cancelSubscription = (
  subscriptionId,
  { cancelAtPeriodEnd, metadata } = { cancelAtPeriodEnd: true, metadata: null },
) =>
  new Promise((resolve, reject) => {
    stripe.subscriptions.update(
      subscriptionId,
      { cancel_at_period_end: cancelAtPeriodEnd, metadata },
      (err, subscription) => {
        if (err) reject(err);
        resolve(subscription);
      },
    );
  });
