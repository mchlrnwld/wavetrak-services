import axios from 'axios';

export const requestGiftCode = async () => {
  const reqData = {
    department: 'Engineering',
    campaign: 'Coastalwatch x Surfline VIP Migration',
    prefix: 'CW',
    total: 1,
    brand: 'sl',
    sponsor: 'mmalchak@surfline.com',
    product: 0,
  };
  try {
    const { data } = await axios({
      method: 'POST',
      url: `${process.env.ADMIN_SERVICES_URL}/gifts/generate-codes`,
      headers: {
        Authorization: `Bearer ${process.env.ACCESS_TOKEN}`,
        'X-Auth-Employeeemail': 'mmalchak@surfline.com',
      },
      data: reqData,
    });
    return data;
  } catch (error) {
    console.log(error);
    throw new Error(error.message);
  }
};

export const redeemGiftCode = async (userId, giftCode) => {
  const { brand, redeemCode } = giftCode;
  try {
    const { data } = await axios({
      method: 'POST',
      url: `${process.env.SUBSCRIPTION_SERVICE_URL}/gifts/redeem`,
      headers: {
        'X-Auth-UserId': userId,
      },
      data: { brand, code: redeemCode },
    });
    return data;
  } catch (error) {
    console.log(error);
    throw new Error(error.message);
  }
};
