import axios from 'axios';

export const updateSubscriptionIdentity = async ({ userId, subscriptionExpiration }) => {
  const reqData = {
    userId,
    traits: {
      'subscription.expiration': new Date(subscriptionExpiration),
    },
  };
  try {
    const { data } = await axios({
      method: 'POST',
      url: 'https://api.segment.io/v1/identify',
      auth: {
        username: process.env.SEGMENT_API_KEY,
      },
      data: reqData,
    });
    return data;
  } catch (error) {
    throw new Error(error);
  }
};
