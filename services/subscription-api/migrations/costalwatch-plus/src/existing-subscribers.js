import { initMongo, setupLogsene, createLogger } from '@surfline/services-common';
import mongoose from 'mongoose';
import csvtojosn from 'csvtojson';
import { createObjectCsvWriter } from 'csv-writer';
import UserInfoModel from './models/UserInfoModel';
import UserAccountModel from './models/UserAccountModel';

setupLogsene(process.env.LOGSENE_KEY);
const log = createLogger('lookup-existing-subscriber');

const entToProduct = (ent) => {
  if (ent.includes('sl')) return 'Surfline';
  if (ent.includes('fs')) return 'Fishtrack';
  if (ent.includes('bw')) return 'Buoyweather';
  return null;
};

Promise.all([initMongo(mongoose, process.env.MONGO_CONNECTION_STRING)])
  .then(async () => {
    const cwExisting = createObjectCsvWriter({
      path: 'existing_cw_users.csv',
      header: [
        { id: 'email', title: 'Email' },
        { id: 'isSubscribed', title: 'Is Subscribed' },
        { id: 'platform', title: 'Platform' },
        { id: 'product', title: 'Product' },
      ],
    });
    csvtojosn()
      .fromFile('./cw_plus_emails.csv')
      .subscribe(async (data) => {
        const { email } = data;
        const user = await UserInfoModel.findOne({ email: email.toLowerCase() });
        if (user) {
          const accounts = await UserAccountModel.find({ user: user._id });
          if (accounts && accounts.length) {
            const subscription = accounts
              .map(({ subscriptions }) => subscriptions)
              .flat()
              .find(
                ({ entitlement }) =>
                  entitlement.includes('sl') ||
                  entitlement.includes('fs') ||
                  entitlement.includes('bw'),
              );
            if (subscription) {
              const csvRow = [
                {
                  email: user.email,
                  isSubscribed: 'yes',
                  platform: subscription.type,
                  product: entToProduct(subscription.entitlement),
                },
              ];
              await cwExisting.writeRecords(csvRow);
            }
          } else {
            const csvRow = [{ email: user.email, isSubscribed: 'no' }];
            await cwExisting.writeRecords(csvRow);
          }
        }
      });
  })
  .catch((err) => {
    log.error({
      message: 'Error executing exsiting subscriber lookup',
      errorMessage: err.message,
      stack: err.stack,
    });
    process.exit(1);
  });
