import mongoose, { Schema } from 'mongoose';

const options = {
  collection: 'MigratedUsers',
};

const MigratedUsersSchema = new mongoose.Schema(
  {
    cwUserId: {
      type: Schema.Types.Number,
    },
    wtUserId: {
      type: Schema.Types.ObjectId,
      ref: 'UserInfo',
    },
    firstName: {
      type: Schema.Types.String,
    },
    lastName: {
      type: Schema.Types.String,
    },
    email: {
      type: Schema.Types.String,
    },
    plan: {
      type: Schema.Types.String,
    },
    hasActiveSubscription: {
      type: Schema.Types.Boolean,
      default: false,
    },
    hasActiveMarineStripeSubscription: {
      type: Schema.Types.Boolean,
      default: false,
    },
    renewalDate: {
      type: Schema.Types.Number,
    },
    stripeCustomerId: {
      type: Schema.Types.String,
    },
    expiring: {
      type: Schema.Types.Number,
    },
    eWayToken: {
      type: Schema.Types.Number,
    },
    isSlVip: {
      type: Schema.Types.Boolean,
    },
  },
  options,
);

export default mongoose.model('MigratedUsers', MigratedUsersSchema);
