import { initMongo, setupLogsene, createLogger } from '@surfline/services-common';
import mongoose from 'mongoose';
import ProgressBar from 'progress';
import { argv } from 'yargs';
import StripeCustomerModel from './models/StripeCustomerModel';

const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

setupLogsene(process.env.LOGSENE_KEY);
const log = createLogger('sync-user-info-to-stripe');
const { live } = argv;
const isLive = live === 'true';

const updateCustomer = (customerId, props) =>
  new Promise((resolve, reject) => {
    stripe.customers.update(customerId, props, (err, customer) => {
      if (err) return reject(err);
      return resolve(customer);
    });
  });

const pipeline = [
  {
    $lookup: {
      from: 'UserInfo',
      localField: 'user',
      foreignField: '_id',
      as: 'userInfo',
    },
  },
  {
    $project: {
      stripeCustomerId: 1,
      userInfo: {
        email: 1,
        firstName: 1,
        lastName: 1,
      },
    },
  },
  {
    $unwind: '$userInfo',
  },
];

const syncUserInfo = async (liveRun = false) => {
  log.info(`Live Run: ${liveRun}`);
  log.info(`Starting --- Syncing user info to Stripe`);

  let syncedUserCount = 0;
  let errorCount = 0;
  const [{ total: totalUserCount }] = await StripeCustomerModel.aggregate([
    ...pipeline,
    {
      $count: 'total',
    },
  ]);

  const bar = new ProgressBar(':percent :bar :current/:total :eta', totalUserCount);

  await StripeCustomerModel.aggregate(pipeline)
    .cursor()
    .exec()
    .eachAsync(
      async doc => {
        try {
          const {
            stripeCustomerId,
            userInfo: { email, firstName, lastName },
          } = doc;
          const name = `${firstName} ${lastName}`;
          if (liveRun) {
            await updateCustomer(stripeCustomerId, {
              email,
              metadata: { name, firstName, lastName },
            });
            syncedUserCount += 1;
          }
        } catch (error) {
          errorCount += 1;
          log.info(doc.stripeCustomerId);
          log.error(error.message);
        }
        bar.tick();
      },
      { parallel: 5 },
    );

  log.info(`Finished --- Syncing user info to Stripe`);
  log.info(
    `Total Users: ${totalUserCount}, Synced Users: ${syncedUserCount}, Error Count: ${errorCount}`,
  );
};

Promise.all([initMongo(mongoose, process.env.MONGO_CONNECTION_STRING)])
  .then(async () => {
    await syncUserInfo(isLive);
    process.exit(0);
  })
  .catch(err => {
    log.error({
      message: 'Fatal Error running script',
      errorMessage: err.message,
      stack: err.stack,
    });
    process.exit(1);
  });
