docker-compose \
	-f ./integration-tests/docker-compose.test.yml \
	-f ./integration-tests/docker-compose.integration.yml \
	up \
	--build \
	--no-color \
	--force-recreate \
	--exit-code-from integration-tests
