# Subscription Service V1 API Docs

## Surfline Error Codes
To more easily differentiate certain errors related to subscription we are starting
to create some specific error codes that are unique to our codebase. These error
codes can be found on the error object with the `code` property. A list of error
codes can be found here:

| Code      | Description                                                                   |
| --------- | ----------------------------------------------------------------------------- |
|   1001    | Card has already been used for a free trial on this brand                     |


## Creating a new Surfline Paid Account and Subscription (Stripe)

_Subscription endpoints will be mounted at services.surfline.com/subscriptions._

Surfline uses Stripe for subscription management and recurring billing. Once a
registered user has upgraded to a paid plan they will have a `UserAccount`
document stored in Surfline's mongo database and a corresponding Stripe account.
After a user has been created with an active subscription all rebilling events
are triggered via Stripe and communicated back to Surfline via webhooks.

#### Validating cards when creating trials

To prevent users from resubscribing with a new account and gaming the free trial
system, we have started to collect the cards used in free trials inside a `CreditCard`
collection in Mongo. On the backend if the env variable `VALIDATE_CARDS` is not set to `OFF`
a user who signs up for a free trial will have their card checked against the `CreditCard`
collection to see if that card has been used for a free trial on the brand the user wants
to trial on.

#### Sample HTTP Request

    POST /subscriptions HTTP/1.1
    Host: example.surfline.com
    Content-Type: application/json
    X-Auth-userid: 56f9eaf946f566a11a5c6148

    {
      source: 'tok_17DILkJle83KVJVkGT9D00v8',
      plan: 'sl_monthly'
    }

**Response**

    {
      availablePlans: [
        {
          id: 'sl_premium_daily',
          amount: 25,
          entitlementKey: 'sl_premium',
          upgradable: false,
          description: 'Ad-free cams, expert forecasts, advanced forecast tools.',
          interval: 'day',
          intervalCount: 1,
          currency: 'usd',
          name: 'Surfline Premium Daily',
          trialPeriodDays: null
         }
       ],
      subscriptions: [
        {
          type: 'stripe',
          subscriptionId: 'sub_8QGNIbpyYYZPtP',
          periodStart: '2016-05-10T01:39:54.000Z',
          periodEnd: '2016-05-11T01:39:54.000Z',
          entitlement: 'sl_premium',
          createdAt: '2016-05-10T01:39:54.633Z',
          expiring: false,
          failedPaymentAttempts: 0,
          effectivePrice: 199
        }
      ],
      archivedSubscriptions: [],
      cards: [
        {
          brand: 'Visa',
          last4: '4242',
          id: 'card_189PqnJle83KVJVk20Sh0iwy'
        }
      ],
      defaultStripeSource: 'card_189PqnJle83KVJVk20Sh0iwy',
      stripeCustomerId: 'cus_8QGNVR4RS6ir69',
      updatedAt: '2016-05-10T02:12:42.368Z',
      createdAt: '2016-05-10T02:12:42.364Z'
    }

#### Parameters

| Parameter | Required | Description                                                                   |
| --------- | -------- | ----------------------------------------------------------------------------- |
| `source`  | yes      | stripe.js customer token. Contains enough card details to set a default card. |
| `plan`    | yes      | Plan key to attach to the user. This key                                      |

#### Error responses

```
{
code: 1001
errorHandler: "DuplicateCardError"
message: "This card has already been used for a free trial on this brand."
name: "DuplicateCardError"
notice: false
statusCode: 400
}
```

### Retreiving existing User Subscription Details

to be used for the account are and upgrade funnel. These
requests are authenticated through the services proxy. Subscription data will be
returned for the currently authenticated user as defined in `x-auth-userid`.

    GET /subscriptions HTTP/1.1
    Host: example.surfline.com
    x-auth-userid: 56f9eaf946f566a11a5c6164
    Content-Type: application/json

**Response**

    {
      availablePlans: [
        {
          id: 'sl_premium_daily',
          amount: 25,
          entitlementKey: 'sl_premium',
          interval: 'day',
          interval_count: 1,
          currency: 'usd',
          name: 'Surfline Premium Daily',
          trial_period_days: null
         }
       ],
      subscriptions: [
        {
          type: 'stripe',
          subscriptionId: 'sub_8QGNIbpyYYZPtP',
          periodStart: '2016-05-10T01:39:54.000Z',
          periodEnd: '2016-05-11T01:39:54.000Z',
          entitlement: 'sl_premium',
          createdAt: '2016-05-10T01:39:54.633Z',
          expiring: false,
          failedPaymentAttempts: 0
        }
      ],
      archivedSubscriptions: [],
      cards: [
        {
          brand: 'Visa',
          last4: '4242',
          id: 'card_189PqnJle83KVJVk20Sh0iwy'
        }
      ],
      defaultStripeSource: 'card_189PqnJle83KVJVk20Sh0iwy',
      stripeCustomerId: 'cus_8QGNVR4RS6ir69',
      updatedAt: '2016-05-10T02:12:42.368Z',
      createdAt: '2016-05-10T02:12:42.364Z'
    }

`availablePlans` will always be returned by the endpoint. Other attributes will
be returned as the related data exists in Stripe.

#### Parameters

| Parameter  | Required | Description                                                         |
| ---------- | -------- | ------------------------------------------------------------------- |
| `querySet` | no       | (_Default `sl`_) Returns a filtered set of plans defined in Stripe. |

### Retreiving Offered Plans for a Cancelling User
This endpoint will return all the Offered plans specific to each brand that a user can upgrade to.
These requests are authenticated and would require an accessToken to be available in the header. Also we need to pass in the `brand` in the url.

      GET /subscriptions/cancellation-offer?brand=sl HTTP/1.1
      Host: example.surfline.com
      x-auth-userid: 56f9eaf946f566a11a5c6164
      Content-Type: application/json

**Response**

    {
      offeredPlans: [
          {
              id: 'sl_annual',
              amount: 6999,
              entitlementKey: 'sl_premium',
              upgradable: '1',
              description: 'Billed once a year at ${amount}. Saving ${saving} per year.',
              interval: 'year',
              intervalCount: 1,
              currency: 'usd',
              name: 'Surfline Premium Annual',
              trialPeriodDays: 15
          }
      ],
      currentSubscriptionForBrand: {
              type: 'stripe',
              subscriptionId: 'sub_CZBdl77FYLV8hJ',
              periodStart: '2018-03-26T21:53:28.000Z',
              periodEnd: '2018-04-10T21:53:28.000Z',
              entitlement: 'sl_premium',
              stripePlanId: 'sl_monthly',
              createdAt: '2018-03-27T00:17:48.014Z',
              expiring: false,
              onTrial: true,
              failedPaymentAttempts: 0
          },
      user: '5ab96bc06d225f001143d729',
      stripeCustomerId: 'cus_CZBdjAWn7qC1RH'
  }

`offeredPlans` object will contain the available plans the user can upgrade to. `currentSubscriptionForBrand` object contains the current subscription details for the user.

### Updating an existing user's subscription

To update a user's subscription when switching plans. These requests are authenticated and would require an accessToken to be available in the header. This endpoint accepts the new `plan` name and the current user `subscription` Id. Returns the updated `userAccount` and the upcoming Invoice details.

    PUT /subscriptions HTTP/1.1
    Host: example.surfline.com
    x-auth-userid: 56f9eaf946f566a11a5c6164
    Content-Type: application/json

    {
      plan:'sl_monthly',
	    subscription:'sub_CZBdl77FYLV8hJ'
    }

**Response**

    HTTP 200 OK
```
{
  userAccount: {
    _id: "5ab96bd8a9b614177fb7336a",
    updatedAt: "2018-04-02T04:30:47.722Z",
    createdAt: "2018-03-26T21:53:28.992Z",
    user: "5ab96bc06d225f001143d729",
    __v: 72,
    stripeCustomerId: "cus_CZBdjAWn7qC1RH",
    winbacks: [
      {
        code: "winback-10-5ab96bd8a9b614177fb7336a-28",
        type: "winback",
        ref: "WB-10-OFF",
        percent_off: 10
      }
    ],
    receipts: [],
    archivedSubscriptions: [
      {
        failedPaymentAttempts: 0,
        onTrial: true,
        expiring: false,
        createdAt: "2018-03-26T21:53:28.990Z",
        stripePlanId: "sl_monthly",
        entitlement: "sl_premium",
        periodEnd: "2018-04-10T21:53:28.000Z",
        periodStart: "2018-03-26T21:53:28.000Z",
        subscriptionId: "sub_CZBdl77FYLV8hJ",
        type: "stripe"
      }
    ],
    subscriptions: [
      {
        failedPaymentAttempts: 0,
        onTrial: true,
        expiring: false,
        createdAt: "2018-04-02T04:30:47.668Z",
        stripePlanId: "sl_monthly",
        entitlement: "sl_premium",
        periodEnd: "2018-04-10T21:53:28.000Z",
        periodStart: "2018-03-30T22:09:18.000Z",
        subscriptionId: "sub_CZBdl77FYLV8hJ",
        type: "stripe"
      }
    ]
  },
  upcomingInvoice: {
    period_start: 1522447758,
    total: 1299,
  }
}
```


#### Parameters

| Parameter             | Required | Description                                                                  |
| --------------------- | -------- | ---------------------------------------------------------------------------- |
| `plan` | yes       | A valid plan name. |
| `subscription` | yes       | User's subscriptionId |

### Canceling a User's Subscription

Sending a `DELETE` request to the following endpoint will cancel a user's subscription.
If the subscription does not exists a `404` response code will be returned.
Canceling a user's account will not immediately go into effect until a corresponding
webhook is sent by Stripe. Stripe will first send a `customer.subscription.updated`
event. That event will result in a subscription being marked as `expiring`. After
the subscription's `periodEnd` has passed a subsequent `customer.subscription.deleted`
event will be sent. The event will move the subscription into `archivedSubscriptions`
and disentitle the `UserAccount`.

    DELETE /subscriptions/:subscriptionId HTTP/1.1

**Response**

    HTTP 200 OK

If the subscription does not exist

    HTTP 404 Not Found

## Gift Cards Workflow
Gift Cards enable access to premium features outside of the scope of the standard subscription model.

### Purchase Flow

To generate a gift code do a POST to `/gifts/purchase`.

| Parameter             | Required | Description                                                                  |
| --------------------- | -------- | ---------------------------------------------------------------------------- |
| `giftProductId` | yes       | A valid Gift Product Id |
| `source` | yes       | A valid stripe Token |
| `purchaserEmail` | yes       |
| `purchaserName` | yes       |
| `recipientEmail` | yes       |
| `recipientName` | yes       |
| `message` | yes       |

A stripe charge will be created against the source that is passed in. An alphanumeric redeem gift code is generated and
stored in `GiftRedemptionCode` collection. A segment track call `Purchased Gift Card` is triggered which will be sent the gift code to the recipient. The charge_id from the successful charge will be stored in the `GiftRedemptionCode` collection.

If no errors, then a successful response would look like:
```
{
    "message": "Successfully purchased <Gift Name>"
}
```

### Stripe Webhook Integrations (Billing Related)

Surfline uses Stripe's webhook to react to subscription and billing events.

All live events are routed through `/integrations/stripe/handler`.

The following events are responded to on behalf of a Surfline user's account.

* `invoice.payment_success`
* `invoice.payment_failed`
* `customer.subscription.deleted`
* `customer.subscription.updated`

### Rebill Success

A successful user account rebill is represented by a `invoice.payment_succeeded`
Stripe event.

When an `invoice.payment_succeeded` is received, the subscription service will update
`startDate` and `endDate` with the new period's start and end date.

The subscription system will also set `failedPaymentAttempts` to `0` and `expiring`
to `false`. The will recover the `UserAccount` from any `invoice.payment_failed`
events.

### Rebill Failure

After a payment has failed stripe will send `invoice.payment_failed`. The
subscription system will increment `failedPaymentsAttempts` by `1` and set
`expiring` to `true`.

After the specified payments attempts Stripe will send a `customer.subscription.deleted`
event. When the subscription receives that event the subscription will be moved from
`subscriptions` to `archivedSubscriptions`. Additionally the entitlement related
with the subscription will be removed from the `UserAccount` object.

### Managing a Customer's Cards

#### Delete a Card

    DELETE /subscriptions/cards/:cardId HTTP/1.1

\*_default cards may not be deleted._

#### Mark a Card as Default

Use the `PUT` verb on the `/subscriptions` and pass a valid `defaultStripeSource`.

#### Add a new Card

By default when a new Stripe card is created, the card is set as the default value
for `stripeDefaultSource`.

    POST /subscriptions/cards HTTP/1.1
    Host: example.surfline.com
    Content-Type: application/json
    X-Auth-userid: 56f9eaf946f566a11a5c6148

    { source: 'tok_189PqnJle83KVJVk20Sh0iwy' }

#### Update an existing Card

Use the `PUT` verb on `/subscriptions/cards/:cardId` and pass an `opts` object in the request body. The `opts` object accepts `exp_month` and `exp_year` properties with integer values.

    PUT /subscriptions/cards/:cardId HTTP/1.1
    Host: example.surfline.com
    Content-Type: application/json
    X-Auth-userid: 56f9eaf946f566a11a5c6148

    { opts: { exp_month: 12, exp_year: 22 } }

### Applying Discount Codes

Coupon codes are managed through stripe. Coupons can either be global or plan specific.
If the coupon is global, the customer will be able to use the coupon on any of the surfline plans.
If the coupon is plan specific then the customer will be able to use the coupon only on the plans
that are specific to the coupon. This is achieved by the use of a coupons metadata.

If no plan metadata is set within the coupon then we assume the coupon will be applied globally (to every plan).

If the metadata contains a plan key / values then the coupon will be specific to the list of values. An example can be seen below.

    {
      "plans": "sl_premium_daily,sl_monthly"
    }

**NOTE**: Plan values must be valid ids of existing plans.

### Applying Winbacks

Winbacks are managed within mongo and store a reference to a specific stripe coupon. When a user
is created we automatically create three winback codes specific to that user, which each contain a reference to an already existing stripe coupon. These are stored in the users account object.

When a user uses a winback code we search stripe for the coupon contained in the winbacks ref and use that coupon on the users subscription. If successful then the winback object is removed and a newly generated winback object is added to the account.

**NOTE**: Winbacks are global and are percent off only.

## Admin Endpoints

_Admin endpoints will be mounted at tools.surflineadmin.com/api._

Because these endpoints will be used for our admin tool kit, authentication will be handled before the request gets to the service and the service does not have to worry about authentication.

### Adding a UserAccount to an Existing User

#### Request

```
POST /api/subscriptions HTTP/1.1
Host: tools.surflineadmin.com
Content-Type: application/json

{
  "userId": "576c4a4bfd00c54237ceb023"
}
```

#### Response

```
{
  "availablePlans": [
    {
      "id": "2",
      "amount": 6999,
      "entitlementKey": "sl_premium",
      "upgradable": "1",
      "description": "Billed once a year at $69.99. Saving $85.89 per year.",
      "interval": "year",
      "intervalCount": 1,
      "currency": "usd",
      "name": "Surfline Premium Annual",
      "trialPeriodDays": 15
    },
    {
      "id": "1",
      "amount": 1299,
      "entitlementKey": "sl_premium",
      "upgradable": "1",
      "description": "Ad-free cams, expert forecasts, advanced forecast tools.",
      "interval": "month",
      "intervalCount": 1,
      "currency": "usd",
      "name": "Surfline Premium Monthly",
      "trialPeriodDays": 15
    }
  ],
  "subscriptions": [],
  "archivedSubscriptions": [],
  "updatedAt": "2016-09-09T16:56:44.557Z",
  "createdAt": "2016-09-09T16:56:44.557Z",
  "user": "576c4a4bfd00c54237ceb023"
}
```

#### Parameters

| Parameter | Required | Description                            |
| --------- | -------- | -------------------------------------- |
| `userId`  | yes      | The user's ID to add a UserAccount to. |

### Plan Entitlement Integrations

Plans are not considered valid unless they have `entitlement` and `features`
attributes set on metadata.

### Metadata Attribute Descriptions

| Key         | Value                                                                                                                                                                                                          |
| ----------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| entitlement | `Entitlement.name` lookup in mongo.                                                                                                                                                                            |
| features    | Inserts the union of `Entitlement.features` and `features` into the Surfline database. The features managed in Stripe should be comma separated, written in lowercase and snake_case forms. Eg. `vip_premium`. |
| query_set   | used by funnel frontend to group and query plans that a user can subscribe to.                                                                                                                                 |
| rank        | Used to sort available plans.                                                                                                                                                                                  |
| upgradable  | Only upgradable plans are presented to the user on the subscription page to change to.                                                                                                                         |
| description | Used on the subscription page to give extra information about an upgradable plan.                                                                                                                              |

**Note**: The current implementation does not support removing individual
features. The mongo `Entitlement` collection must be directly queried for a delete
operation.

## IAP Receipt Validation

Verify an in-app purchase receipt and entitle users with our `/iap/{platform}/verifyReceipt` endpoint.

POST to `/iap/ios/verifyReceipt` or `/iap/google/verifyReceipt`

JSON body format for iOS:

    {
       receipt: 'receipt...'
    }

JSON body format for Google:

    {
       receipt: {
         data: {PurchaseInfo json...}
         signature: 'receipt signature...'
       },
       brand: 'fs' || 'bw' || 'sl' (default)
    }

Apple receipts are tied to the Apple Store user, so a single iOS receipt can contain purchases across all brands. `/iap/ios/verifyReceipt` just needs the receipt in the body, no brand required. Brands are still required when verifying Google receipts.

If you call this service outside of the deployed .staging.surfline.com context, you'll need to pass `x-auth-userid` in the header.

Error responses:

    500: {message: 'Error encountered'}
         Possibilities: User retrieval or receipt parsing failed

    400: {message: 'Attach receipt as a "receipt" and "type" property in JSON body.'}

A nightly job runs in Jenkins to validate receipts attached to users.

## IAP Premium Pending

Notify the Subscription API of the authenticated user's intent to purchase.

`POST` to `/iap/ios/premium-pending` or `/iap/google/premium-pending`

JSON body format for iOS and Google:

    {
      brand: 'sl' // required string options (sl|bw|fs);
      cancel: false // optional boolean (true|false);
    }

These API are intended for asyncronous requests and their responses should not be recognized.

A successful request will modify a user's `entitlements` response from the `GET /entitlements` API

### Google IAP Subscription Refund

DELETE `/iap/google/:subscriptionId`

`:subscriptionId` will look gnarly, but toss it on the URL as a param: `localhost:8004/iap/google/edgcacfhmkpekcilnihgdjkb.AO-J1OxnZr_-c4xGioV-wbb9YI4w7gtRzY87CRLsa6CrHuP_nF97WNzHaBjbqCyZeYYf_sZByLD1DKxkMOFlpIsiOJnSeHxu5XIwa303DbJwFQ7Lo-sM6dgY4-4DCEqk61C9qgUx0GsLaOMZJF0zMC0mRS9K8Z2P3-uSDQpUv0qorTGt7xQC42s`

DELETE will return the updated UserAccount object as JSON.

## Debugging Webhooks

There is a general purpose catchall webhook reciever mounted at `/firehose` that
is used to log and debug all webhooks. The log level should be set to `trace`
in order to capture those events.

## Debugging Rebilling Events

* invoice web hooks that stripe send will fire error if the corresponding lookup
  failed from mongo

* 'A Webhook was received without a related UserAccount or entitlement' something
  went wrong trying to disentitle
