# Stripe

## Overview
Stripe manages and processes the most significant portion of the auto-renewing subscription and billing infrastructure across the Surfline, Buoyweather, and Fishtrack applications.  Stripe's API is robust and provides industry-leading features and extensibility.  However, the Subscription-Service only targets a specific portion of these features within their API.  These features include Customer, Products & Pricing Plans, Subscriptions, and Events.

### Our Accounts
Each Stripe environment (production, staging, and sandbox) has a designated Stripe account.  However, our production environment is the only account with "live" mode enabled.  All other accounts exist in "test" mode.

For access to any of these accounts, contact your manager or log in through SSO if it is enabled.

#### Account Names
- **Surfline Prod** - Top Secret
- **Surfline QA** - Hosts our Staging environment configuration and should closely resemble Production.
- **Surfline Sandbox** - Hosts our Sandbox envrionment and is great for local development and API testing.
  - *Disclaimer: If you get too creative in here expect things to blow up.  For a true sandbox experience I suggest creating your own dev account and using those credentials for local development.*
- **Surfline Integration Tests** - Controls the configuration for the Subscription-Service's suite of API integration tests.

## Customers ([docs](https://stripe.com/docs/api/customers))
> Stripe Customer objects allow you to perform recurring charges, and to track multiple charges, that are associated with the same customer. The API allows you to create, delete, and update your customers. You can retrieve individual customers as well as a list of all your customers.

The Subscription-Service uses the `stripeCustomerId` on the `UserInfo` and `Subscription` (if applicable) documents to associate the relationship between an application user and the corresponding Customer object in Stripe.  Conversely, the Subscription-Service adds application user information to the Customer Object's `metadata` property to form the reverse relationship.  The information includes the following:

#### Customer Metadata
| Property  | Description | Required? |
| ------------- | ------------- | ------------- |
| `name`  | Computed from the `firstName` and `lastName` values  | Yes  |
| `firstName`  | A User's first name value from the `UserInfo` doc  | Yes  |
| `lastName`  | A User's last name value from the `UserInfo` doc  | Yes  |
| `userId`  | A User's `_id` value from the `UserInfo` doc  | Yes  |

## Products & Pricing Plans ([docs](https://stripe.com/docs/api/service_products))
> Stripe Service Product objects describe items that your customers can subscribe to with a Subscription. An associated Plan determines the product pricing.  Stripe Plans define the base price, currency, and billing cycle for subscriptions.

The Subscription-Service API does not recognize Product objects.  Instead, the Service leverages the Stripe Plans API to pull all plans for all products and filters out the specific Plan per request by parsing the Plan's `id` and `metadata` properties.  The Plan's metadata includes the following:

#### Plan Metadata
| Property  | Description | Required? |
| ------------- | ------------- | ------------- |
| `entitlement`  | The access level granted by purchasing this Plan  | Yes  |
| `status`  | "active/inactive" determines if the plan is visible to the User | Yes  |
| `upgradable`  | 0 or 1 determines if a user can upgrade/downgrade to this Plan | Yes  |
| `query_set`  | Associates the Plan with the Wavetrak brand ("sl/bw/fs") | Yes  |
| `description`  | A string that describes the billing interval  | Yes  |

## Subscriptions ([docs](https://stripe.com/docs/api/subscriptions))
> Subscriptions allow you to charge a customer on a recurring basis.

The Subscription-Service leverages the Stripe Subscription object heavily.  The Stripe Subscription is SOT for creating a `Subscriptions` documents in Mongodb and bridges the realtionship between the Customer and the billing Plan.  The following Subscription object properties are used by the Subscription-Service:

#### Relevant Subscription Properties
| Property  | Description | Maps To Subscription |
| ------------- | ------------- | ------------- |
| `id`  | Unique identifier for the object  | `subscriptionId`  |
| `plan.id`  | The Stripe recurring billing Plan  | `planId`  |
| `current_period_start`  | Start of the current period that the subscription has been invoiced for | `start`  |
| `current_period_end`  | End of the current period that the subscription has been invoiced for. At the end of this period, a new invoice will be created | `end`  |
| `cancel_at_period_end`  | If the subscription has been canceled with the `at_period_end` flag set to `true`, `cancel_at_period_end` on the subscription will be `true`. You can use this attribute to determine whether a subscription that has a status of active is scheduled to be canceled at the end of the current period | `expiring`  |
| `trial_start`  | If the subscription has a trial, the beginning of that trial.  | `trialing`  |
| `trial_end`  | If the subscription has a trial, the end of that trial.  | `trialing`  |

## Events
> Events are our way of letting you know when something interesting happens in your account. When an interesting event occurs, we create a new Event object. For example, when a charge succeeds, we create a `charge.succeeded` event; and when an invoice payment attempt fails, we create an `invoice.payment_failed` event. Note that many API requests may cause multiple events to be created. For example, if you create a new subscription for a customer, you will receive both a `customer.subscription.created` event and a `charge.succeeded` event.

The Subscription-Service leverages Stripe's Events API to respond to lifecycle changes within the Core Stripe resources listed above; `Customers`, `Plans`, and `Subscriptions`.  The following Events are recognized by the Subscription-Service's Stripe Integrations Webhook Handler:

#### Relevant Stripe Events

| Event Name | Description | Use Case |
| ------------- | ------------- | ------------- |
| `charge.dispute.created`  | Occurs whenever a customer disputes a charge with their bank.  | The event intiates a Subscription payment alert and restricts access to the associated entitlement |
| `charge.dispute.closed`  | Occurs when a dispute is closed and the dispute status changes to lost, warning_closed, or won.  | If `won` access is reinstated.  If `lost` the Subscription is immediately expiring and deactivated and the payment warning is resolved |
| `customer.created`  | Occurs whenever a new customer is created. | This event initiates a request to the user-service to add the `stripeCustomerId` on the `UserInfo` document using the `metadata.userId` property|
| `customer.subscription.created`  | Occurs whenever a customer is signed up for a new plan  | Initiates a data request to our Analytics platform.  The actually database document is created from the Stripe Request callback response. |
| `customer.subscription.updated`  | Occurs whenever a subscription changes (e.g., switching from one plan to another, or changing the status from trial to active).  | As the description states, we use this event for status updates and cancellations. |
| `customer.subscription.deleted`  | Occurs whenever a customer's subscription ends.  | This event deactivates a subscription document and revokes access to the accosiated entitlement. |
| `invoice.created`  | Occurs whenever a new invoice is created.  | Primairly used for listening to when users signup for subscriptions with a discount code. |
| `invoice.upcoming`  | Occurs X number of days before a subscription is scheduled to create an invoice that is automatically charged—where X is determined by your subscriptions settings.  | Primarily used to triggers sucbscription billing related segment events. |
| `invoice.payment_succeeded`  | Occurs whenever an invoice payment attempt succeeds.  | This event triggers the updates to the `start` and `end` dates on the Subscription to represent transition into a new billing period. |
| `invoice.payment_failed`  | Occurs whenever an invoice payment attempt fails, due either to a declined payment or to the lack of a stored payment method.  | Triggers a payment alert and billing grace period.  The `failedPaymentAttempts` Subscription property represent the number of billing attempts on the open invoice. |

## Resources
 - https://stripe.com
 - https://stripe.com/docs/billing
 - https://stripe.com/docs/api
