# Integrations APIs
This documentation details the Subscription-Service API setup and contracts with the Stripe, iTunes, and Google-Play auto-renewing subscription integrations.

## Integrations Docs
 - [Stripe](https://github.com/Surfline/wavetrak-services/blob/master/services/subscription-service/docs/v2/integrations/Stripe.md)
 - [iTunes Connect](https://github.com/Surfline/wavetrak-services/blob/master/services/subscription-service/docs/v2/integrations/iTunes_Connect.md)
 - [Google Play](https://github.com/Surfline/wavetrak-services/blob/master/services/subscription-service/docs/v2/integrations/Google_Play.md)

## Purpose
The purpose of this documentation is to inform the engineer of how each integration's components align with the Subscription-Service's API contract.

### The Integrations API Namespace
External requests from our Subscription integrations are proxied to the subscription service at the following URL:
```
https://services.surfline.com/integrations/<stripe|itunes|google>
```

## Stripe Webhook API
Request sent to the Stripe integrations handler contains the Stripe [Event](https://stripe.com/docs/api/events/object) object in the request body.  The listener of these requests parses the Event and invokes the appropriate webhook handler to perform the necessary actions against the Subscription or Customer values stored by the Subscription-Service.

#### Example Event JSON Payload
```
// Some lines have been ommited for brevity
{
  "id": "evt_1GM2iSJ4F5N75cpXkZCKLaOF",
  "object": "event",
  "api_version": "2019-12-03",
  "created": 1584064416,
  "data": {
    "object": {
      "id": "in_1GM1mDJ4F5N75cpXPGDbosEK",
      "object": "invoice",
      "account_country": "US",
      "account_name": "Surfline/Wavetrak, Inc.",
      "amount_due": 9588,
      "amount_paid": 9588,
      "amount_remaining": 0,
      "charge": "ch_1GM2iQJ4F5N75cpXew0mDmc8",
      "collection_method": "charge_automatically",
      "created": 1584060805,
      "currency": "usd",
      "ending_balance": 0,
      "lines": {
        "object": "list",
        "data": [
          <A Stripe Invoice Object/>
        ],
        "has_more": false,
        "total_count": 1,
        "url": "/v1/invoices/in_1GM1mDJ4F5N75cpXPGDbosEK/lines"
      },
      "paid": true,
      "payment_intent": "pi_1GM2iPJ4F5N75cpXAzTVWGxS",
      "period_end": 1584060802,
      "period_start": 1552438402,
      "status": "paid",
      "status_transitions": {
        "finalized_at": 1584064413,
        "marked_uncollectible_at": null,
        "paid_at": 1584064415,
        "voided_at": null
      },
      "subscription": "sub_ABdZhyonZCSAQT",
      "subtotal": 9588,
      "total": 9588,
      "total_tax_amounts": [
      ],
      "webhooks_delivered_at": 1584060806
    }
  },
  "livemode": true,
  "pending_webhooks": 1,
  "request": {
    "id": null,
    "idempotency_key": null
  },
  "type": "invoice.payment_succeeded"
}
```


## iTunes Notifications API
iTunes server-to-server notifications also send a JSON object in the request body.  The iTunes notification handler performs in similar ways to the Stripe handler.  The handler parses the request to invoke the necessary Webhook listener.

#### Example Server-to-Server Notification JSON
```
{
    latest_receipt: <Base-64 encoded string>,
    latest_receipt_info: {
      original_purchase_date_pst: '2017-09-29 12:45:03 America/Los_Angeles',
      quantity: '1',
      subscription_group_identifier: '20337868',
      unique_vendor_identifier: '0F8E732F-8C13-4C23-BBF0-7A02E9498F36',
      original_purchase_date_ms: '1506714303000',
      expires_date_formatted: '2019-10-10 20:30:58 Etc/GMT',
      is_in_intro_offer_period: 'false',
      purchase_date_ms: '1570134658000',
      expires_date_formatted_pst: '2019-10-10 13:30:58 America/Los_Angeles',
      is_trial_period: 'false',
      item_id: '1138111154',
      unique_identifier: '00008027-000904660AE8002E',
      original_transaction_id: opts.originalTransactionId || opts.transactionId,
      expires_date: '1570739458000',
      app_item_id: '393782096',
      transaction_id: opts.transactionId,
      bvrs: '201909102218',
      web_order_line_item_id: '480000102442123',
      version_external_identifier: '832634305',
      bid: 'com.surfline.surfapp',
      product_id: opts.planId,
      purchase_date: '2019-10-03 20:30:58 Etc/GMT',
      purchase_date_pst: '2019-10-03 13:30:58 America/Los_Angeles',
      original_purchase_date: '2017-09-29 19:45:03 Etc/GMT'
    },
    environment: 'PROD',
    auto_renew_status: 'true',
    password: process.env.APPLE_IAP_PASSWORD,
    auto_renew_product_id: 'surfline.iosapp.1month.subscription.notrial',
    notification_type: 'INTERACTIVE_RENEWAL'
  };
```

## Google Push/Pull Notification API
Google's Subscription Notifications operate differently from iTunes and Stripe.  Google follows a Push/Pull pattern that informs the Subscription-Service of status changes of a specific subscription in the form of an base-64 econded string.  The JSON request body contains a decoded data is used to assume the changes the Service should expect to see when verifying the receipt against the Google Play store.  Each Webhook listener function within the Google integration verifies the purchase receipt against the Play store and parses the response payload accordingly.

#### Example Google Subscription Notification JSON
```
export default function() {
  return {
    message: {
      data: 'eyJ2ZXJzaW9uIjoiMS4wIiwicGFja2FnZU5hbWUiOiJjb20uc3VyZmxpbmUuYW5kcm9pZCIsImV2ZW50VGltZU1pbGxpcyI6IjE1NzM1OTA0Mzg1NjYiLCJ0ZXN0Tm90aWZpY2F0aW9uIjp7InZlcnNpb24iOiIxLjAifX0=',
      messageId: '835457810297778',
      message_id: '835457810297778',
      publishTime: '2019-11-12T20:27:18.698Z',
      publish_time: '2019-11-12T20:27:18.698Z'
    },
    subscription:
      'projects/surfline-android-1485812445501/subscriptions/sl-subscription-notifications'
  };
}

```