# Google Play

## Overview
The Google Play Store manages and proccesses the auto-renewing subscriptions and billing for Wavetrak's Surfline, Buoyweather, and Fishtrack Android Applications.

## Our Accounts
For the purposes and scope of this documentation, the Subscription-Service connects to the *Surfline/Wavetrak, Inc* Google Play Console

For access to this account, contact your manager or log in with your company Google credentials through the [Google Play Console](https://play.google.com/apps/publish/?account=8133598719665056434#AppListPlace).

## In-App Purchases
The subscription products for each application are found in the _Store Presence -> In-app products -> Subscription_ tab of the Console Dashboard.  The In-App Purchase **Id** corresponds to the Subscription document's `planId` attribute.  All other information about a user's Auto-Renewable Google Play Subscription is found in the App Store receipt.

### Play Store Receipts
The Subscription-Service leverages the Play Store's [GET Purchases](s://www.googleapis.com/androidpublisher/v3/applications/packageName/purchases/subscriptions/subscriptionId/tokens/token) API to validate app and in-app transaction receipts.  These receipts are created by Google when a user initiates a purchase within the Android application.  It's the Android applications responsibility to provide the Subscription-Service with the receipt after initial purchase.  Play Store receipts are persisted witin the Subscription-Purchase document.

#### Play Store Receipt Properties
The initial Play Store receipt recieved from the Android application contains limited data about the purchase.  The Subscription-Service sends this data back to the `GET Purchases` API with the following parameters:

| Property  | Description |
| ------------- | ------------- |
| `packageName`  | The package name of the application for which this subscription was purchased (for example, 'com.surfline.android').  |
| `subscriptionId`  | The purchased subscription ID (for example, 'surfline.android.1month.subscription.notrial'). |
| `token`  | The token provided to the user's device when the subscription was purchased.  |

#### Play Store Subscrition Purchase Properties
The `GET Purchases` API returns a `SubscriptionPurchase` resource that indicates the status of a user's subscription purchase.  The relevant properties of this JSON response include the following:

| Property  | Description | Maps to Subscription |
| ------------- | ------------- |  ------------- |
| `startTimeMillis`  | Time at which the subscription was granted, in milliseconds since the Epoch.  | `start` |
| `expiryTimeMillis`  | Time at which the subscription will expire, in milliseconds since the Epoch.  | `end` |
| `autoRenewing`  | Whether the subscription will automatically be renewed when it reaches its current expiry time.	  | `expiring` |
| `paymentState`  | The payment state of the subscription. Possible values are: **(0)**Payment pending, **(1)**Payment received, **(2)**Free trial, **3**Pending deferred upgrade/downgrade  | `trialing` |
| `orderId`  | The order id of the latest recurring order associated with the purchase of the subscription.  | `orderId` |
| `cancelReason`  | The reason why a subscription was canceled or is not auto-renewing. Possible values are: **(0)** - User canceled the subscription, **(1)** - Subscription was canceled by the system, for example because of a billing problem, **(2)** - Subscription was replaced with a new subscription, **(3)** Subscription was canceled by the developer. | `cancellationReason`
| `countryCode`  | 	[ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2#Current_codes) billing country/region code of the user at the time the subscription was granted.  | `countryCode` |


## Realtime Push/Pull Subscription Notifications
> Google Play Billing provides server push notifications that let you monitor state changes for Play-managed subscriptions and one-time purchases with pending transactions. By enabling real-time developer notifications, you'll receive a purchase token directly from Cloud Pub/Sub anytime there is an update to a pending transaction or an existing subscription.

> Real-time developer notifications do not provide full information about the state of the subscription, such as whether the user is currently entitled to access subscription content. When you receive the token, you should always use the purchase token to query the Google Play Developer API to get the full information and update your backend with the user's current entitlement status.

> Notification types might change in the future. You should be able to handle unrecognized notifications types, and you should always rely on the Google Play Developer API for critical business logic.

Each Subscription Notification recieved by the Subscription-Service's Google ingtegrations handler contains a single base64-encoded data field in the JSON body of the request.  The Subscription-Service is responsibile for decoding the data string which yeilds the following content:

| Property  | Description |
| ------------- | ------------- |
| `version`  | The version of this notification. Initially, this will be “1.0”. This version is distinct from other version fields  |
| `packageName`  | The package name of the application that this notification relates to (for example, com.surfline.android).  |
| `subscriptionNotification`  | If this field is present, then this notification relates to a subscription. It contains additional information related to the subscription. This field is mutually exclusive with testNotification and oneTimeProductNotification.  |

#### Subscription Notification Data
| Property  | Description |
| ------------- | ------------- |
| `version`  | The version of this notification. Initially, this will be “1.0”. This version is distinct from other version fields.  |
| `notificationType`  | The type of notification. It can have the following values described in the next table.  |
| `purchaseToken`  | The token provided to the user’s device when the subscription was purchased.  Use this to make requests back to the Play Store.  |
| `subscriptionId`  | The purchased subscription ID (for example, ‘surfline.android.1month.subscription.notrial’). |

The following list of Notification Types are handled by the iTunes Server Notifications Handlers.

| Value  | Description |
| ------------- | ------------- |
| `2` - _SUBSCRIPTION_RENEWED_  | An active subscription was renewed.  |
| `3` - _SUBSCRIPTION_CANCELED_  | A subscription was either voluntarily or involuntarily cancelled. For voluntary cancellation, sent when the user cancels.  |
| `12` - _SUBSCRIPTION_REVOKED_  | A subscription has been revoked from the user before the expiration time.  |
| `13`- _SUBSCRIPTION_EXPIRED_  | A subscription has expired.  |


## Resources
 - https://developers.google.com/android-publisher/api-ref/purchases/subscriptions
 - https://play.google.com/apps/publish/?account=8133598719665056434#AppListPlace
 - https://developer.android.com/google/play/billing/realtime_developer_notifications
