# iTunes Connect

## Overview
iTunes Connect manages and proccesses the auto-renewing subscriptions and billing for Wavetrak's Surfline, Buoyweather, and Fishtrack iOS Applications.

### Our Accounts
For the purposes and scope of this documentation, the Subscription-Service connects to the *Surfline/Wavetrak* App Store workspace.

For access to this account, contact your manager or log in with your company Apple Id through the [iTunes Connect](https://itunesconnect.apple.com) Developer console.

## In-App Purchases
The subscription products in the Connect dashboard's Features tab represent the in-app-purchase item for each iOS application, respectively.  The In-App Purchase **Product Id** corresponds to the Subscription document's `planId` attribute.  All other information about a user's Auto-Renewable iTunes Subscription is found in the App Store receipt.

### App Store Receipts
The Subscription-Service leverages the App Store's `verifyReceipt` API to validate app and in-app transaction receipts.  These receipts are created by Apple when a user initiates a purchase within the iOS application.  It's the iOS applications responsibility to provide the Subscription-Service with the receipt.  App Store receipts are persisted witin the Subscription-Purchase document.

> Submit a receipt to the App Store to receive a JSON response containing the app information and in-app purchase details in a number of fields that make up the receipt. Each field or combination of fields provides insight that you can use to deliver service and content to the user as you define.

> Receipts for auto-renewable subscriptions can grow over time since the renewal transactions stay in the receipt forever. To optimize performance, the App Store may truncate sandbox receipts to remove old transactions. When validating receipts for transactions made in the sandbox environment, consider creating new test accounts instead of reusing old accounts to test subscription purchases.

As stated, there are a number of fields that make up the receipt.  However, the Subscription-Services iTunes integration pay specific attention to the detail of the following items:

#### Relevant verifyReceipt Response Body Properties
| Property | Description |
| ------------- | ------------- |
| `latest_receipt`  |  The latest Base64 encoded app receipt. Only returned for receipts that contain auto-renewable subscriptions. |
| `latest_receipt_info`  | An array that contains all in-app purchase transactions. This excludes transactions for consumable products that have been marked as finished by your app. Only returned for receipts that contain auto-renewable subscriptions.  |
| `pending_renewal_info`  | In the JSON file, an array where each element contains the pending renewal information for each auto-renewable subscription identified by the product_id. Only returned for app receipts that contain auto-renewable subscriptions.  |
| `status`  | Either 0 if the receipt is valid, or a status code if there is an error. The status code reflects the status of the app receipt as a whole. See status for possible status codes and descriptions.  |
| `environment`  | The environment for which the receipt was generated. Possible values: _Sandbox_, _Production_  |
| `original_transaction_id`  | The transaction identifier of the original purchase.  |

#### Latest Receipt Info Properties
The Subscription-Service leverages the most recent purchase object in the `latest_receipt_info` array when creating or updating the status of an Apple Subscription.  The following properties are utilized for performing these actions:

| Property  | Description | Maps To Subscription |
| ------------- | ------------- | ------------- |
| `product_id`  | The unique identifier of the product purchased. You provide this value when creating the product in App Store Connect, and it corresponds to the productIdentifier property of the SKPayment object stored in the transaction's payment property.  | `planId`  |
| `transaction_id`  | A unique identifier for a transaction such as a purchase, restore, or renewal. See [transaction_id](https://developer.apple.com/documentation/appstorereceipts/transaction_id) for more information. | `subscriptionId`  |
| `purchase_date_ms`  | For consumable, non-consumable, and non-renewing subscription products, the time the App Store charged the user's account for a purchased or restored product, in the UNIX epoch time format, in milliseconds. For auto-renewable subscriptions, the time the App Store charged the user’s account for a subscription purchase or renewal after a lapse, in the UNIX epoch time format, in milliseconds. Use this time format for processing dates.  | `start` (_coverted to seconds_) |
| `expires_date_ms`  | The time a subscription expires or when it will renew, in UNIX epoch time format, in milliseconds. Use this time format for processing dates. See [expires_date_ms](https://developer.apple.com/documentation/appstorereceipts/expires_date_ms) for more information.  | `end` (_converted to seconds_)  |
| `cancellation_date_ms`  | The time Apple customer support canceled a transaction, or the time an auto-renewable subscription plan was upgraded, in UNIX epoch time format, in milliseconds. This field is only present for refunded transactions. Use this time format for processing dates. See [cancellation_date_ms](https://developer.apple.com/documentation/appstorereceipts/cancellation_date_ms) for more information.  | `expired`  |
| `is_trial_period`  | An indicator of whether a subscription is in the free trial period. See [is_trial_period](https://developer.apple.com/documentation/appstorereceipts/is_trial_period) for more information.  | `trialing`  |
| `cancellation_reason`  | The reason for a refunded transaction. When a customer cancels a transaction, the App Store gives them a refund and provides a value for this key. A value of “1” indicates that the customer canceled their transaction due to an actual or perceived issue within your app. A value of “0” indicates that the transaction was canceled for another reason; for example, if the customer made the purchase accidentally.  Possible values: 1, 0  | `cancellationReason`  |

#### Pending Renewal Info Properties
The `pending_renewal_info` is also leveraged by the Subscription service to determine the state of a user's subscription auto-renewal.  The following properties are used:

| Property  | Description | Maps to Subscription |
| ------------- | ------------- | ------------- |
| `expiration_intent`  | The reason a subscription expired. This field is only present for a receipt that contains an expired auto-renewable subscription.  | `expirationIntent`  |
| `auto_renew_product_id`  | The current renewal preference for the auto-renewable subscription. The value for this key corresponds to the productIdentifier property of the product that the customer’s subscription renews. This field is only present if the user downgrades or crossgrades to a subscription of a different duration for the subsequent subscription period.  | `autoRenewProdcutId`  |
| `auto_renew_status`  | The current renewal status for the auto-renewable subscription. See [auto_renew_status](https://developer.apple.com/documentation/appstorereceipts/auto_renew_status) for more information.  | `expiring`  |

## Server-to-Server Notifications
> Server-to-server notifications are a service for auto-renewable subscriptions. The App Store sends notifications to your server of real-time changes in a subscription's status. For information on all the fields contained in these server notifications, see [App Store Server Notifications](https://developer.apple.com/documentation/appstoreservernotifications).

> Using the server-to-server notification service is optional but recommended, especially if you offer subscription services across multiple platforms and you need to keep the subscription records updated. After you set up your server, you can start receiving notifications at any time by adding a server URL in App Store Connect.

> Use notifications along with receipt validation to validate a user's current subscription status and provide them with services or promotional offers based on that status.

#### Server to Server Notification Types
The following list of Notification Types are handled by the iTunes Server Notifications Handlers.


| Type  | Description |
| ------------- | ------------- |
| `INITIAL_BUY`  | Occurs at the initial purchase of the subscription.  |
| `CANCEL`  | Indicates that the subscription was canceled either by Apple customer support or by the App Store when the user upgraded their subscription. The `cancellation_date_ms` key contains the date and time when the subscription was canceled or upgraded.  |
| `RENEWAL`  | Indicates successful automatic renewal of an expired subscription that failed to renew in the past. Check expires_date to determine the next renewal date and time.  |
| `INTERACTIVE_RENEWAL`  | Indicates the customer renewed a subscription interactively, either by using your app’s interface, or on the App Store in account settings. Make service available immediately.  |
| `DID_CHANGE_RENEWAL_PREF`  | Indicates the customer made a change in their subscription plan that takes effect at the next renewal. The currently active plan is not affected. Meaning, the current billing period will be completed before billing is initiated for the on plan.  |
| `DID_CHANGE_RENEWAL_STATUS`  | Indicates a change in the subscription renewal status. Check the `auto_renew_status_change_date_ms` and the `auto_renew_status` in the JSON to know the date and time when the status was last updated and the current renewal status.  |

#### Server to Server Notification JSON
> The App Store sends notifications as JSON objects for a number of subscription events. These JSON events, documented in [responseBody](https://developer.apple.com/documentation/appstoreservernotifications/responsebody), contain fields and information you can use to react to user subscription states according to your business logic.

The Subscription Service parses the Notification JSON and transforms the relevant properties according to the iTunes Subscription document contract.  The properties include the following:

**NOTE: As of March/3/2020, these properties are scheduled for deprecation in favor of the [unified_receipt](https://developer.apple.com/documentation/appstoreservernotifications/unified_receipt) JOSN property.  This upgrade to the new contract will be made in a later PR.**

| Property  | Description |
| ------------- | ------------- |
| `latest_receipt`  | The latest Base64-encoded transaction receipt.  |
| `latest_receipt_info`  | The JSON representation of the value in `latest_receipt`. Note that this field is an array in the receipt but a single object in server-to-server notifications.  |
| `latest_expired_receipt`  | The latest Base64-encoded transaction receipt. This field appears in the notification instead of `latest_receipt` for expired transactions.|
| `latest_expired_receipt_info`  | The JSON representation of the value in `latest_expired_receipt`. This array appears in the notification instead of `latest_receipt_info` for expired transactions.    |
| `auto_renew_status`  | The current renewal status for an auto-renewable subscription product. Note that these values are different from those of the auto_renew_status in the receipt. Possible values: true, false  |
| `auto_renew_status_change_data_ms`  | The time at which the renewal status for an auto-renewable subscription was turned on or off, in UNIX epoch time format, in milliseconds. Use this time format for processing dates.  |
| `auto_renew_product_id`  | The product identifier of the auto-renewable subscription that the user's subscription renews.  |


The following properties are recognized from the `latest_receipt_info` and `latest_expried_receipt_info` when creating or updating the status of an Apple Subscription.

| First Header  | Second Header | Maps to Subscription |
| ------------- | ------------- | ------------- |
| `transaction_id`  | A unique identifier for a transaction such as a purchase, restore, or renewal. See [transaction_id](https://developer.apple.com/documentation/appstorereceipts/transaction_id) for more information. | `subscriptionId`  |
| `original_transaction_id`  | The transaction identifier of the original purchase.  | `originalTransactionId` |
| `purchase_date_ms`  | For consumable, non-consumable, and non-renewing subscription products, the time the App Store charged the user's account for a purchased or restored product, in the UNIX epoch time format, in milliseconds. For auto-renewable subscriptions, the time the App Store charged the user’s account for a subscription purchase or renewal after a lapse, in the UNIX epoch time format, in milliseconds. Use this time format for processing dates.  | `start` (_coverted to seconds_) |
| `expires_date_ms`  | The time a subscription expires or when it will renew, in UNIX epoch time format, in milliseconds. Use this time format for processing dates. See [expires_date_ms](https://developer.apple.com/documentation/appstorereceipts/expires_date_ms) for more information.  | `end` (_converted to seconds_)  |
| `is_trial_period`  | An indicator of whether a subscription is in the free trial period. See [is_trial_period](https://developer.apple.com/documentation/appstorereceipts/is_trial_period) for more information.  | `trialing`  |
| `cancellation_date_ms`  | The time Apple customer support canceled a transaction, or the time an auto-renewable subscription plan was upgraded, in UNIX epoch time format, in milliseconds. This field is only present for refunded transactions. Use this time format for processing dates. See [cancellation_date_ms](https://developer.apple.com/documentation/appstorereceipts/cancellation_date_ms) for more information.  | `expired`  |
| `cancellation_reason`  | The reason for a refunded transaction. When a customer cancels a transaction, the App Store gives them a refund and provides a value for this key. A value of “1” indicates that the customer canceled their transaction due to an actual or perceived issue within your app. A value of “0” indicates that the transaction was canceled for another reason; for example, if the customer made the purchase accidentally.  Possible values: 1, 0  | `cancellationReason`  |

## Resources
- https://developer.apple.com/documentation/appstorereceipts
- https://developer.apple.com/documentation/appstorereceipts/responsebody
- https://developer.apple.com/documentation/appstoreservernotifications
