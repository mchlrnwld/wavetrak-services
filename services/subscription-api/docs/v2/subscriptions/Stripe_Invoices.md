# Stripe Invoices API

## `POST invoices/pay`

This API is used to close or mark Stripe invoices as uncollectible when a subscription enters a canceled state after an Invoice has been drafted. This prevents a `past_due` Stripe Invoice state. Additionally, Invoice payments can be triggered immediately if a customer tries to resolve a payment warning on their own, e.g. through a manual credit card update.

The endpoint returns an array of invoices that were updated.

### Example Request
```bash
curl --location --request POST 'https://services.surfline.com/invoices/pay'
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
```

### Example Response
```json
[ "in_1JSr8SLwFGcBvXawx3TRPsal" ]
```