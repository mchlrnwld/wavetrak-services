
# Stripe Credit Cards API

The `/credit-cards` endpoints are used for managing Stripe Credit Card CRUD requests.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [`GET /credit-cards`](#get-credit-cards)
- [`POST /credit-cards`](#post-credit-cards)
- [`DELETE /credit-cards/:cardId`](#delete-credit-cardscardid)
- [`PUT /credit-cards/:cardId`](#put-credit-cardscardid)
- [`PUT /credit-cards/default/:cardId`](#put-credit-cardsdefaultcardid)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->
## `GET /credit-cards`
Returns a Stripe customer's credit card source objects, as well as the id of the default card on file.

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------ |
| `cards`  | An array of `card` objects | Array |
| `defaultSource`  | The id of the user's default source object | String |

### Example Request
```bash
curl --location --request POST 'https://services.surfline.com/credit-cards' \
--header 'x-auth-accesstoken: <VALID_TOKEN>'
```

### Example Response
```json
{
   "cards":[
      {
         "brand": "Visa",
         "last4": "4242",
         "id": "card_1IIjdGJle83KVJVk4Ba0YlV4",
         "exp_month": 12,
         "exp_year": 2025,
         "expMonth": 12,
         "expYear": 2025
      },
      {
         "brand": "Visa",
         "last4": "4242",
         "id": "card_1IIjW7Jle83KVJVkyJABoTIU",
         "exp_month": 12,
         "exp_year": 2023,
         "expMonth": 12,
         "expYear": 2023
      }
   ],
   "defaultSource": "card_1IIjdGJle83KVJVk4Ba0YlV4"
}
```

## `POST /credit-cards`
Adds a new Stripe Credit Card for a user.

**Note: by default when a new Stripe card is created, the card is set as the default source.**

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `source`  | The id of the user's default source object | String | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------ |
| `card`  | A `card` object representing the new card added by this request | Object |

### Example Request
```bash
curl --location --request POST 'https://services.surfline.com/credit-cards' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--data-raw '{
  "source": <STRIPE_SOURCE>
}'
```

### Example Response
```json
{
   "card": {
      "brand": "Visa",
      "last4": "4242",
      "id": "card_1JSk9wJle83KVJVk7TLfJbvh",
      "expMonth": 4,
      "expYear": 2024,
      "addressZip": "42424"
   }
}
```

## `DELETE /credit-cards/:cardId`
Deletes a Stripe Card for a user.

**Note: default cards may not be deleted.**

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `cardId`  | The id of the source object belonging to the Stripe Card | String | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------ |
| `message`  | A string indicating the successful deletion of the card | String |

### Example Request
```bash
curl --location --request DELETE 'https://services.surfline.com/credit-cards/<CARD_ID>' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--data-raw '{
  "source": <STRIPE_SOURCE>
}'
```

### Example Response
```json
{ "message": "The card was deleted" }
```


## `PUT /credit-cards/:cardId`
Updates an existing Stripe Credit Card using an `opts` object.
The opts object accepts `exp_month`, `exp_year` and `address_zip` properties.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `cardId`  | The id of the source object belonging to the Stripe Card | String | Yes |
| `id`  | The id of the source object belonging to the Stripe Card | String | Yes |
| `opts`  | An object representing the fields to update (`exp_month`, `exp_year` and `address_zip`) | Object | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------ |
| `card`  | A `card` object representing the updated card | Object |

### Example Request
```bash
curl --location --request PUT 'https://services.surfline.com/credit-cards/<CARD_ID>' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--data-raw '{
  "id": <STRIPE_SOURCE>,
  "opts": {
    "address_zip": "94122",
    "exp_month": 3,
    "exp_year": 2025
  }
}'
```

### Example Response
```json
{
   "card": {
      "brand": "Visa",
      "last4": "4242",
      "id": "card_1JSk9wJle83KVJVk7TLfJbvh",
      "expMonth": 4,
      "expYear": 2024,
      "addressZip": "42424"
   }
}
```

## `PUT /credit-cards/default/:cardId`
Sets an existing Stripe Credit Card as the default card for the user.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `cardId`  | The id of the source object to set as the default card | String | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------ |
| `message`  | A string indicating the successful update | String |

### Example Request
```bash
curl --location --request PUT 'https://services.surfline.com/credit-cards/default/<CARD_ID>' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
```

### Example Response
```json
{ "message": "Subscription updated" }
```