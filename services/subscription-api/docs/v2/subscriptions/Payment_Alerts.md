# Payment Alerts 
The Payment Alerts API supports credit card dunning and payment disputes 
specific to an active subscription with an outstanding invoice payment.

## `GET /payment/alerts`

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `product` | Represents the standardized abbreviation for each product.  Accepted values are: `"sl", "bw", "fs"`  | String | yes |

### Response Parameters
| Param  | Description |
| ------------- | ------------- |
| `message`  | A string describing the reason a payment failed |
| `alert`  | Indicates if the alert should notify the end user. |
| `type`  | Represents the subscription type; 'stripe', 'apple', 'google |

### Example Request
```bash
curl --location --request POST 'https://services.surfline.com/payment/alerts?product=<BRAND>' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
```

### Example Responses
```json
{
  "message": "No payment alerts found",
  "alert": false,
  "type": "stripe"
}
```

```json
{
  "message": "Your payment was disputed",
  "alert": true,
  "type": "stripe"
}
```