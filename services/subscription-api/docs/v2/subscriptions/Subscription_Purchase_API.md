# Subscription Purchase API

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Google Subscription Purchases](#google-subscription-purchases)
  - [`GET /admin/purchases/google`](#get-adminpurchasesgoogle)
  - [`POST /admin/purchases/google`](#post-adminpurchasesgoogle)
  - [`PUT /admin/purchases/google`](#put-adminpurchasesgoogle)
  - [`POST /admin/purchases/google/defer`](#post-adminpurchasesgoogledefer)
- [Apple Subscription Purchases](#apple-subscription-purchases)
  - [`GET /admin/purchases/itunes`](#get-adminpurchasesitunes)
  - [`POST /admin/purchases/itunes`](#post-adminpurchasesitunes)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Google Subscription Purchases

## `GET /admin/purchases/google` 
Admin API for retrieving the most recent google purchase for a single customer.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `userId`  | The id of the customer  | String  | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `purchases`  | An array of subscription-purchase objects | Array |

### Example Request
```bash
curl --location --request GET 'https://tools.surflineadmin.com/api/purchases/google?<USER_ID>' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>
```

### Example Response
```json
{
  "purchases":[
    {
      "data":{
        "kind":"androidpublisher#subscriptionPurchase",
        "startTimeMillis":1628724780967,
        "expiryTimeMillis":1631316780967,
        "autoRenewing":true,
        "priceAmountMicros":9990000,
        "countryCode":"US",
        "orderId":"GPA.3327-5391-7183-13013..0",
        "paymentState":1
      },
      "type":"google-play",
      "_id":"612824ac8c4100d3367c2659",
      "user":"612824ac8c4100d3367c264d",
      "productId":"surfline.android.1month.subscription.notrial",
      "packageName":"com.surfline.android",
      "signature":"ABCDEFGHIJKLMNOP123456789",
      "purchaseToken":"ABC123",
    }
  ]
}
```

## `POST /admin/purchases/google`
Admin API for updating a single google purchases against the google purchase API.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `purchaseId`  | The id of the purchase  | String  | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `purchase`  | The updated subscription purchase | Object |

### Example Request
```bash
curl --location --request POST 'https://tools.surflineadmin.com/api/purchases/google' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>' \
--data-raw '{
  "purchaseId": <PURCHASE_ID>
}'
```

### Example Response
```json
{
  "purchase":{
    "data":{
      "kind":"androidpublisher#subscriptionPurchase",
      "startTimeMillis":1628724847295,
      "expiryTimeMillis":1631316847295,
      "autoRenewing":true,
      "priceAmountMicros":9990000,
      "countryCode":"US",
      "orderId":"GPA.3327-5391-7183-13013..0",
      "paymentState":1
    },
    "type":"google-play",
    "_id":"612824ef9442f8d37f389052",
    "user":"612824ef9442f8d37f38904e",
    "productId":"surfline.android.1month.subscription.notrial",
    "packageName":"com.surfline.android",
    "signature":"ABCDEFGHIJKLMNOP123456789",
    "purchaseToken":"ABC123",
  }
}
```


## `PUT /admin/purchases/google`
Cancels, refunds, or revokes a user's subscription purchase.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `purchaseId`  | The id of the purchase  | String  | Yes |
| `command`  | The type of modification ("cancel", "refund" or "revoke") | String  | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `purchase`  | The updated subscription purchase | Object |

### Example Request
```bash
curl --location --request PUT 'https://tools.surflineadmin.com/api/purchases/google' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>' \
--data-raw '{
  "purchaseId": <PURCHASE_ID>,
  "command": <COMMAND>
}'
```

### Example Response
```json
{
  "purchase":{
    "data":{
      "kind":"androidpublisher#subscriptionPurchase",
      "startTimeMillis":1628724847295,
      "expiryTimeMillis":1631316847295,
      "autoRenewing":true,
      "priceAmountMicros":9990000,
      "countryCode":"US",
      "orderId":"GPA.3327-5391-7183-13013..0",
      "paymentState":1
    },
    "type":"google-play",
    "_id":"612824ef9442f8d37f389052",
    "user":"612824ef9442f8d37f38904e",
    "productId":"surfline.android.1month.subscription.notrial",
    "packageName":"com.surfline.android",
    "signature":"ABCDEFGHIJKLMNOP123456789",
    "purchaseToken":"ABC123",
  }
}
```

## `POST /admin/purchases/google/defer`
Defers a user's subscription purchase until a specified future expiration time.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `purchaseId`  | The id of the purchase  | String  | Yes |
| `deferMillis`  | The number of milliseconds to defer the renewal.  | Number  | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `purchase`  | The updated subscription purchase | Object |

### Example Request
```bash
curl --location --request POST 'https://tools.surflineadmin.com/api/purchases/google/defer' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>' \
--data-raw '{
  "purchaseId": <PURCHASE_ID>,
  "deferMillis": <NUM_MILLISECONDS>
}'>
```

### Example Response
```json
{
  "purchase":{
    "data":{
      "kind":"androidpublisher#subscriptionPurchase",
      "startTimeMillis":1628724847295,
      "expiryTimeMillis":1631316847295,
      "autoRenewing":true,
      "priceAmountMicros":9990000,
      "countryCode":"US",
      "orderId":"GPA.3327-5391-7183-13013..0",
      "paymentState":1
    },
    "type":"google-play",
    "_id":"612824ef9442f8d37f389052",
    "user":"612824ef9442f8d37f38904e",
    "productId":"surfline.android.1month.subscription.notrial",
    "packageName":"com.surfline.android",
    "signature":"ABCDEFGHIJKLMNOP123456789",
    "purchaseToken":"ABC123",
  }
}
```

# Apple Subscription Purchases

## `GET /admin/purchases/itunes` 
Retrieves all itunes purchases for a single customer.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `userId`  | The id of the customer  | String  | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `purchases`  | An array of subscription-purchase objects | Array |

### Example Request
```bash
curl --location --request GET 'https://tools.surflineadmin.com/api/purchases/itunes?<USER_ID>' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>
```
### Example Response
```json
{
  "purchases":[
    {
      "type":"itunes",
      "pendingRenewalInfo":[
        {
          "_id":"6128239a3a714cd1465677e2",
          "autoRenewProductId":"surfline.iosapp.1month.subscription.notrial",
          "originalTransactionId":"201910100000",
          "productId":"surfline.iosapp.1month.subscription.notrial",
          "autoRenewStatus":"1"
        }
      ],
      "latestReceiptInfo":[
        {
          "_id":"6128239a3a714cd1465677df",
          "productId":"surfline.iosapp.1month.subscription.notrial",
          "transactionId":"201910100000",
          "purchaseDateMs":"1622244506210",
          "originalPurchaseDateMs":"1506714303000",
          "expiresDateMs":"1624836506210",
          "isTrialPeriod":"false"
        },
        {
          "_id":"6128239a3a714cd1465677e0",
          "productId":"surfline.iosapp.1month.subscription.notrial",
          "transactionId":"2019101000001",
          "purchaseDateMs":"1624836506210",
          "originalPurchaseDateMs":"1506714303000",
          "expiresDateMs":"1627514906210",
          "isTrialPeriod":"false"
        },
        {
          "_id":"6128239a3a714cd1465677e1",
          "productId":"surfline.iosapp.1month.subscription.notrial",
          "transactionId":"2019101000002",
          "purchaseDateMs":"1627514906210",
          "originalPurchaseDateMs":"1506714303000",
          "expiresDateMs":"1630106906210",
          "isTrialPeriod":"false",
          "cancellationDateMs":null,
          "cancellationReason":null
        }
      ],
      "_id":"6128239a3a714cd1465677de",
      "user":"6128239a3a714cd1465677d6",
      "originalTransactionId":"201910100000",
      "status":0,
      "environment":"Production",
      "latestReceipt":"RECEIPT_DATA",
    }
  ]
}
```

## `POST /admin/purchases/itunes`
Updates a single itunes purchases against the itunes purchase API.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `purchaseId`  | The id of the purchase  | String  | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `purchase`  | The updated subscription purchase | Object |

### Example Request
```bash
curl --location --request POST 'https://tools.surflineadmin.com/api/purchases/itunes' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>' \
--data-raw '{
  "purchaseId": <PURCHASE_ID>
}'
```

### Example Response
```json
{
  "purchase":{
    "type":"itunes",
    "pendingRenewalInfo":[
      {
        "_id":"6128242c15e95fd20232fd21",
        "autoRenewProductId":"surfline.iosapp.1month.subscription.notrial",
        "originalTransactionId":"201910100000",
        "productId":"surfline.iosapp.1month.subscription.notrial",
        "autoRenewStatus":"1"
      }
    ],
    "latestReceiptInfo":[
      {
        "_id":"6128242c15e95fd20232fd1e",
        "productId":"surfline.iosapp.1month.subscription.notrial",
        "transactionId":"201910100000",
        "purchaseDateMs":"1622244652362",
        "originalPurchaseDateMs":"1506714303000",
        "expiresDateMs":"1624836652362",
        "isTrialPeriod":"false"
      },
      {
        "_id":"6128242c15e95fd20232fd1f",
        "productId":"surfline.iosapp.1month.subscription.notrial",
        "transactionId":"2019101000001",
        "purchaseDateMs":"1624836652362",
        "originalPurchaseDateMs":"1506714303000",
        "expiresDateMs":"1627515052362",
        "isTrialPeriod":"false"
      },
      {
        "_id":"6128242c15e95fd20232fd20",
        "productId":"surfline.iosapp.1month.subscription.notrial",
        "transactionId":"2019101000002",
        "purchaseDateMs":"1627515052362",
        "originalPurchaseDateMs":"1506714303000",
        "expiresDateMs":"1630107052362",
        "isTrialPeriod":"false",
        "cancellationDateMs":null,
        "cancellationReason":null
      }
    ],
    "_id":"6128242c15e95fd20232fd18",
    "user":"6128242c15e95fd20232fd10",
    "originalTransactionId":"201910100000",
    "status":0,
    "environment":"Production",
    "latestReceipt":"RECEIPT_DATA",
  }
}
```