# Gifts API

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [`GET /gift/payment`](#get-giftpayment)
- [`POST /gift/payment`](#post-giftpayment)
- [`GET /gift/redeem`](#get-giftredeem)
- [`POST /gift/redeem`](#post-giftredeem)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## `GET /gift/payment`
Retrieves a list of active Gift Products specific to the brand included in the request.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `product`  | The product brand (sl / bw / fs) | String  | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `products`  | The updated subscription purchase | Array |
| `email`  | The email value of a authorized user making the request if it exists | String |
| `card`  | The default credit card details if they exist | Object |

### Example Request
```bash
curl --location --request GET 'https://services.surfline.com/gift/payment?product=sl'
```

### Example Response
```json
{
  "products":[
    {
      "_id":"5bedb867068e15ed7696ed77",
      "brand":"sl",
      "name":"1-Year",
      "duration":12,
      "price":9588,
      "isActive":true,
      "imagePath":"https://creatives.cdn-surfline.com/giftcards/cards/sl/1year.png",
      "bestValue":true,
      "description":"1 Year of Surfline Premium"
    },
    {
      "_id":"5bedb8af068e15ed7696ed81",
      "brand":"sl",
      "name":"3-Months",
      "duration":3,
      "price":2997,
      "isActive":true,
      "imagePath":"https://creatives.cdn-surfline.com/giftcards/cards/sl/3months.png",
      "bestValue":false,
      "description":"3 Month Surfline Gift Plan"
    },
    {
      "_id":"5bedb8a3068e15ed7696ed7f",
      "brand":"sl",
      "name":"1-Month",
      "duration":1,
      "price":999,
      "isActive":true,
      "imagePath":"https://creatives.cdn-surfline.com/giftcards/cards/sl/1month.png",
      "bestValue":false,
      "description":"1 Month Surfline Premium"
    }
  ],
  "email":null,
  "card":null
}
```

## `POST /gift/payment`
Initiates a Stripe Charge for a Gift Product purchase.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `giftProductId`  | The document id representing the purchased product | String  | Yes |
| `source`  | The Stripe Card id or the Stripe Token key to charge. | String  | Yes |
| `purchaserEmail`  | A registered or anonymous user's email (the "from" email) | String  | Yes |
| `purchaserName`  | A registered or anonymous user's name (the "from" name) | String  | Yes |
| `recipientEmail`  | The email passed downstream to the email delivery service | String  | Yes |
| `recipientName`  | A name included in the Gift Code delivery email | String  | Yes |
| `message`  | A message included in the Gift Code delivery email | String  | Yes |


### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `message`  | Indicates successful processing of the Gift purchase | String |

### Example Request
```bash
curl --location --request POST 'https://services.surfline.com/gift/payment' \
--header 'Content-Type: application/json' \
--data-raw '{
    "giftProductId" : <GIFT_PRODUCT_ID>,
    "source": <SOURCE>,
    "purchaserEmail": <PURCHASER_EMAIL>,
    "purchaserName": <PURCHASER_NAME>,
    "recipientEmail": <RECIPIENT_EMAIL>,
    "recipientName": <RECIPIENT_NAME>,
    "message": <MESSAGE>,
}'
```

### Example response
```json
{ "message": "Successfully processed the 1-Year purchase." }
```

## `GET /gift/redeem`
Retrieves the active Gift Product details associated with a redeemable gift code.
If the authorized user has an active subscription, the subscription detail will also be returned in the request.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `code`  | The Gift code| String  | Yes |
| `brand`  | The product brand (sl / bw / fs)  | String  | Yes |


### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `giftProduct`  | The Gift Product document properties | Object |
| `subscription`  | The Subscription document properties | Object |

### Example Request
```bash
curl --location --request GET 'https://services.surfline.com/gift/redeem?code=<CODE>&brand=<BRAND>' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
```

### Example Response
```json
{
  "giftProduct":{
    "_id":"5ba96eb86659ef56051f88ca",
    "brand":"sl",
    "name":"1-Month",
    "duration":1,
    "price":999,
    "isActive":true,
    "imagePath":"https://creatives.cdn-surfline.com/giftcards/cards/sl/1month.png",
    "bestValue":false,
    "description":"1 Month of Surfline Premium",
    "__v":0
  },
  "subscription":null
}
```
## `POST /gift/redeem`
Processes the gift code redemption.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `code`  | The Gift code| String  | Yes |
| `brand`  | The product brand (sl / bw / fs)  | String  | Yes |


### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `accountBalance`  | The credit applied to a Stripe Customer account if a gift code is redeemed with an active Stripe Subscription. | Number |
| `subscription`  | The new or updated subscription | Object |

### Example request
```bash
curl --location --request POST 'https://services.sandbox.surfline.com/gift/redeem' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--data-raw '{
    "brand": <BRAND>,
    "code": <GIFT_CODE>
}'
```

### Example response
```json
{
  "subscription":{
    "redeemCodes":[
      "HRTS854W"
    ],
    "_id":"6126bdf55b9b56001427dac6",
    "type":"gift",
    "trialing":false,
    "expiring":true,
    "renewalCount":0,
    "alertPayment":false,
    "subscriptionId":"gift_HRTS854W",
    "user":"6011e649d5daec00122e48b2",
    "active":true,
    "planId":"sl_gift",
    "entitlement":"sl_premium",
    "start":1629928949,
    "end":1632556949,
    "chargeId":"ch_3JST2KCUgh9bySt61ek8zx2x",
    "createdAt":"2021-08-25T22:02:29.018Z",
    "updatedAt":"2021-08-25T22:02:29.018Z",
    "__v":0
  },
  "accountBalance":null
}
```
