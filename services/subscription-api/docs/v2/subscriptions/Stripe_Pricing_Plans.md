# Stripe Pricing Plans API

## `GET /pricing-plans`
Returns pricing plans specific to the brand and geo-targeted country location.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `brand`  | The product brand (sl / bw / fs)  | String  | Yes |
| `countrycode`  | The abbreviated string for each country (defaults to `"US"`) | String  | No |
| `couponcode`  | An optional Stripe Coupon ID | String  | No |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `plans`  | An array of Stripe Product Pricing Plans | Array |

### Example Request
```bash
curl --location --request GET 'https://services.surfline.com/pricing-plans?brand=sl&countrycode=us'
```

### Example Response
```json
{
  "plans":[
    {
      "id":"sl_annual_v2",
      "amount":9588,
      "product":"prod_D51bxaAfF9zdFu",
      "entitlementKey":"sl_premium",
      "upgradable":"1",
      "description":"Billed once a year at $95.88",
      "interval":"year",
      "intervalCount":1,
      "currency":"usd",
      "trialPeriodDays":15,
      "iso":"USD",
      "symbol":"$",
      "country":"US",
      "renewalPrice":9588
    },
    {
      "id":"sl_monthly_v2",
      "amount":999,
      "product":"prod_D51eEQm9SlAnch",
      "entitlementKey":"sl_premium",
      "upgradable":"1",
      "description":"Billed once a month at $9.99.",
      "interval":"month",
      "intervalCount":1,
      "currency":"usd",
      "trialPeriodDays":null,
      "iso":"USD",
      "symbol":"$",
      "country":"US",
      "renewalPrice":999
    }
  ]
}
```