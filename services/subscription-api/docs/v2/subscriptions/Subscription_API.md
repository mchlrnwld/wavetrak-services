
# Subscription API

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Stripe Subscriptions](#stripe-subscriptions)
  - [Overview](#overview)
  - [`GET /subscriptions/billing`](#get-subscriptionsbilling)
  - [`POST /subscriptions/billing`](#post-subscriptionsbilling)
  - [`PUT /subscriptions/billing`](#put-subscriptionsbilling)
  - [`DELETE /subscriptions/billing`](#delete-subscriptionsbilling)
- [Google IAP Subscriptions](#google-iap-subscriptions)
  - [Overview](#overview-1)
  - [`POST /subscriptions/google/premium-pending`](#post-subscriptionsgooglepremium-pending)
  - [`POST /subscriptions/google`](#post-subscriptionsgoogle)
- [Apple IAP Subscriptions](#apple-iap-subscriptions)
  - [Overview](#overview-2)
  - [`POST subscriptions/itunes/premium-pending`](#post-subscriptionsitunespremium-pending)
  - [`POST subscriptions/itunes`](#post-subscriptionsitunes)
- [Wavetrak Subscription Offers](#wavetrak-subscription-offers)
  - [Overview](#overview-3)
  - [`GET /subscriptions/retention-offer`](#get-subscriptionsretention-offer)
  - [`POST /subscriptions/retention-offer`](#post-subscriptionsretention-offer)
- [Wavetrak Promotional Subscriptions](#wavetrak-promotional-subscriptions)
  - [Overview](#overview-4)
  - [`GET /subscriptions/promotion`](#get-subscriptionspromotion)
  - [`POST /subscriptions/promotion`](#post-subscriptionspromotion)
  - [`GET /admin/subscriptions/wavetrak`](#get-adminsubscriptionswavetrak)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Stripe Subscriptions

## Overview
The Stripe Billing APIs accept requests from the Wavetrak Premium Subscription Web Funnels for Surfline, Buoyweather and Fishtrack. These Billing APIs create or modify the state of new and existing Subscriptions and maintain the integrity of data between Wavetrak's databases and Stripe's API.

## `GET /subscriptions/billing`

Retrieves an active subscription originated from the Wavetrak Upgrade Funnels of Buoyweather, Fishtrack, and Surfline.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `product`  | The product requesting the subscription (sl / bw / fs)  | String  | Yes  |

### Response Parameters

Returns a `subscription` object.

### Example Request
```bash
curl --location --request GET 'https://services.surfline.com/subscriptions/billing?product=<BRAND>' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
```

### Example Response
```json
{
  "failedPaymentAttempts":0,
  "redeemCodes":[],
  "type":"stripe",
  "trialing":false,
  "expiring":false,
  "renewalCount":0,
  "alertPayment":false,
  "_id":"61282672b5353ad5244d68f2",
  "subscriptionId":"test123",
  "entitlement":"sl_premium",
  "planId":"sl_monthly",
  "user":"61282671b5353ad5244d68ef",
  "start":1579201980,
  "end":1894821181,
  "active":true,
  "stripeCustomerId":"cus_K76OzeHGOXuS9M",
}
```

## `POST /subscriptions/billing`

Accepts requests to create trialing or direct-to-pay auto-renewing Subscriptions managed through Stripe. This API is also responsible for creating new customers and payment methods in Stripe. Auto-renewing Subscriptions created through this API must have an associated Customer and payment method in Stripe.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `planId`  | The stripe pricing plan applied to the auto-renewing subscription.  | String  | Yes  |
| `source`  | Tokenized credit card details.  | String  | Yes (if `cardId` is not passed)  |
| `cardId`  | The id of a credit card associated with an existing Stripe Customer.  | String  | Yes (if `source` is not passed)  |

### Response Parameters
| Param  | Description |
| ------------- | ------------- |
| `message`  | Indicates successful processing of the subscription |
| `subscriptionId`  | The id of the processed subscription |

### Example Request
```bash
curl --location --request POST 'https://services.surfline.com/subscriptions/billing' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--data-raw '{
  "planId": "sl_annual_v2",
  "source": "<STRIPE_SOURCE_TOKEN>"
}'
```

### Example Response
```json
{
  "message":"Successfully processed the Subscription",
  "subscriptionId":"sub_K76RRfbjk1OyTI"
}
```

## `PUT /subscriptions/billing`

Accepts requests for modifying the auto-renewal status of an active Subscription managed through Stripe.  These modifications include updating the Subscription's Stripe billing interval/pricing plan and enabling the auto-renewal of an expiring Subscription.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `subscriptionId`  | The id of the active Stripe Subscription object.  | String  | Yes  |
| `planId`  | The current plan or valid plan to apply to the auto-renewing subscription.  | String  | Yes  |
| `resume`  | A boolean indicating a user's desired to enable the auto-renewal of an expired subscription.  | Boolean  | No  |

### Response Parameters
| Param  | Description |
| ------------- | ------------- |
| `message`  | Indicates successful processing of the subscription |

### Example Request
```bash
curl --location --request PUT 'https://services.surfline.com/subscriptions/billing' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--data-raw '{
  "subscriptionId": "sub_GwP4Q7XHLKAEQe",
  "planId": "sl_annual_v2",
  "resume": true
}'
```

### Example Response
```json
{ "message":"Successfully processed the Subscription" }
```

## `DELETE /subscriptions/billing`
Accepts requests for canceling the auto-renewing billing status of an active Subscription managed through Stripe.  Requests to cancel a Subscription in a trialing state triggers an immediate deactivation of the Subscription.  All other requests to this endpoint deactivate a Subscription at the end of the current billing period.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `subscriptionId`  | The id of the active Stripe Subscription object.  | string  | true  |

### Response Parameters
| Param  | Description |
| ------------- | ------------- |
| `message`  | Indicates successful cancellation of the subscription |

### Example Request
```bash
curl --location --request DELETE 'https://services.surfline.com/subscriptions/billing' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--data-raw '{
  "subscriptionId": "<SUBSCRIPTION_ID>"
}'
```

### Example Response
```json
{ "message":"Successfully cancelled the Subscription" }
```

# Google IAP Subscriptions

## Overview
The Google IAP Subscription API accepts requests from the Wavetrak suite of Android applications.  Requests to this API should follow a successful initial payment transaction between the Google Play Store and the device running the application and should include the required purchase data in the request body.  The Google IAP Subscription API validates the purchases data against the Google Play Store's subscription APIs and applies the respective premium entitlements for the authenticated user.

## `POST /subscriptions/google/premium-pending`
The Premium Pending API attempts to reduce the data loss caused by failed or dropped requests between an Android Device and the Google IAP Subscription-Service API.  Send requests to the Premium Pending API  after the successful transaction between the device and the Google Play Store and before the device sends the request to the Google IAP Subscription API.  A user in a "premium pending" state is prompted to initiate the subscription request for their device and prevents them from oversubscribing (double-billing).

### Request Parameters
| Param  | Description | Type | Required |
| ------------- | ------------- | ------------- |  ------------- |
| `brand` | Represents the standardized abbreviation for each product.  Accepted values are: `"sl", "bw", "fs"`  | string | yes |
| `cancel`  | An optional parameter that circuit breaks the pending subscription state. | boolean | no |

### Response Parameters
| Param  | Description |
| ------------- | ------------- |
| `message`  | Indicates successful processing of the pending state |

### Example Request
```bash
curl --location --request POST 'https://services.surfline.com/subscriptions/google/premium-pending' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--data-raw '{
	"brand": "sl",
}'
```
### Example Response
```json
{ "message": "Successfully processed the premium pending status" }
```

## `POST /subscriptions/google`
The Google Subscriptions API creates, updates, or cancels subscriptions by validating the request data against the Play Store Subscription Purchase APIs.  Send requests to the Google Subscription API after the device has received the purchase receipt data from the Play Store transaction.  Include the purchase receipt data in the request body.

### Request Parameters
| Param | Description | Type | Required
| ------------- | ------------- | ------------- | ------------- |
| `purchase`  | The `data` and `signature` returned by the Google Play store after a successful purchase transaction is completed.  | JSON Object |  yes |

### Response Parameters
| Param  | Description |
| ------------- | ------------- |
| `message`  | Indicates successful processing of the subscription |

### Example Request
```bash
curl --location --request POST 'https://services.sandbox.surfline.com/subscription/google' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--data-raw '{
	"receipt": {
      "data": {
        "orderId": "GPA.3365-6119-6364-60363",
        "packageName": "com.surfline.android",
        "productId":"surfline.android.1month.subscription.notrial",
        "purchaseToken": <ENCODED_PURCHASE_TOKEN>
      },
      "signature": <ENCODED_SIGNATURE>
    }
}'
```

### Example Response
```json
{ "message": "Successfully processed the Subscription."}
```


# Apple IAP Subscriptions

## Overview
The iTunes IAP Subscription API accepts requests from the Wavetrak suite of iOS applications.  Requests to this API should follow a successful initial payment transaction between the App/iTunes Store and the device running the application and should include the required purchase data in the request body.  The iTunes IAP Subscription API validates the purchases data against the Apple iTunes Subscription APIs and applies the respective premium entitlements for the authenticated user.

## `POST subscriptions/itunes/premium-pending`
The Premium Pending API attempts to reduce the data loss caused by failed or dropped requests between an iOS Device and the iTunes IAP Subscription-Service API.  Send requests to the Premium Pending API  after the successful transaction between the device and the App/iTunes Store and before the device sends the request to the iTunes IAP Subscription API.  A user in a "premium pending" state is prompted to initiate the subscription request for their device and prevents them from oversubscribing (double-billing).

### Request Parameters
| Param  | Description | Type | Required |
| ------------- | ------------- | ------------- |  ------------- |
| `brand` | Represents the standardized abbreviation for each product.  Accepted values are: `"sl", "bw", "fs"`  | string | yes |
| `cancel`  | An optional parameter that circuit breaks the pending subscription state. | boolean | no |

### Response Parameters
| Param  | Description |
| ------------- | ------------- |
| `message`  | Indicates successful processing of the pending state |

### Example Request
```bash
curl --location --request POST 'https://services.surfline.com/subscriptions/itunes/premium-pending' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--data-raw '{
	"brand": "sl",
}'
```
### Example Response
```json
{ "message": "Successfully processed the premium pending status" }
```

## `POST subscriptions/itunes`
The iTunes Subscriptions API creates, updates, or cancels subscriptions by validating the request data against the Apple iTunes Subscription APIs.  Send requests to the iTunes Subscription API after the device has received the purchase receipt data from the App/iTunes Store transaction.  Include the purchase receipt data in the request body.

### Request Parameters
| Param | Description | Type | Required
| ------------- | ------------- | ------------- | ------------- |
| `purchase`  | The Base64 encoded purchase data returned by the App/iTunes Store after a successful purchase transaction is completed.  | string |  yes |

### Response Parameters
| Param  | Description |
| ------------- | ------------- |
| `message`  | Indicates successful processing of the subscription |

### Example Request
```bash
curl --location --request POST 'https://services.sandbox.surfline.com/subscription/itunes' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--data-raw '{
	"purchase": <BASE64_ENCODE_STRING>
}'
```

### Example Response
```json
{ "message": "Successfully processed the Subscription."}
```

# Wavetrak Subscription Offers

## Overview
The Wavetrak Subscription Offer APIs accept requests from the Subscription Offer Web Funnel for Surfline, Buoyweather, and Fishtrack.  Stripe manages the billing and auto-renewing subscription state for the Subscription Offers.  Thus, updates and cancellations to a Subscription created through the Offer APIs, are accepted by the Stripe Billing APIs at `/subscriptions/billing`.   The Subscription Offer APIs fetch proration and billing-related information about a customer subscription and create new or update existing Subscriptions.

## `GET /subscriptions/retention-offer`
Returns billing and savings details respective to the `promotionId` value passed in the request parameters.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `promotionId`  | The id of an active Wavetrak Offer(Promotion) object  | String  | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `currentAnnualCost`  | The cumulative annual cost of an active subscription's billing plan. | Number |
| `currentPlan`  | The name of the active subscription plan. | String |
| `offeredSavings`  | The project annual savings available if the offer is accepted. | Number |
| `offeredPlan`  | The plan applied to the subscription if the offer is accepted. | String |
| `renewalRate`  | The auto-renewing subscription cost after at the end of the offeredPlan's billing cycle. | Number |
| `totalCredits`  | The prorated amount remaining on the active billing cycle. | Number |
| `totalCost`  | The amount that will be billing immediately if the offer is accepted. | Number |

### Example Request
```bash
curl --location --request GET 'https://services.surfline.com/subscriptions/retention-offer?promotionId=5b843ccda47037000ec69b8b'
--header 'x-auth-accesstoken: <VALID_TOKEN>'
```
### Example Response
```json
{
  "currentAnnualCost": 11988,
  "currentPlan": "surfline.iosapp.1month.subscription",
  "offeredSavings": 4500,
  "offeredPlan": "sl_annual_v2",
  "renewalRate": 9588,
  "totalCredits": 0,
  "totalCost": 7588
}
```

## `POST /subscriptions/retention-offer`
Accepts requests to create or update auto-renewing Subscriptions managed through Stripe.  This API may also be responsible for creating new customers and payment methods in Stripe.  Auto-renewing Subscription created through this API must have an associated Customer and payment method in Stripe.  Additionally, this API validates the eligibility of each request against the Wavetrak Offer(Promotions) API and a user's eligibility for the specific Subscription Offer requested.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `promotionId`  | The id of an active Wavetrak Offer(Promotion) object  | String  | Yes |

### Response Parameters
| Param  | Description |
| ------------- | ------------- |
| `message`  | Indicates successful processing of the subscription |

### Example Request
```bash
curl --location --request POST 'https://services.surfline.com/subscriptions/retention-offer' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--data-raw '{
  "promotionId": "5b843ccda47037000ec69b8b"
}'
```

### Example Response
```json
{ "message":"Successfully processed the Subscription" }
```

# Wavetrak Promotional Subscriptions

## Overview
The Wavetrak Promotional Subscription APIs accept requests from the Promotional Web Funnels for Surfline, Buoyweather, Fishtrack.  Stripe manages the billing and auto-renewing subscription state for Promotional subscriptions.  Thus, updates and cancellations to a Promotional Subscription are accepted by the Stripe Billing APIs at `/subscriptions/billing.`  The Promotional Subscription APIs fetch existing or create new Promotional Subscriptions and maintain the integrity of data between Wavetrak's databases and Stripe's API.

## `GET /subscriptions/promotion`
Returns an active Promotional Subscription respective to the `promotionId` value passed in the request parameters.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `promotionId`  | The id of an active Wavetrak Promotion object  | String  | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `subscription`  | A promotional subscription object | Object |

### Example Request
```bash
curl --location --request GET 'https://services.surfline.com/subscriptions/promotion?promotionId=5b843ccda47037000ec69b8b'
--header 'x-auth-accesstoken: <VALID_TOKEN>'
```

### Example Response
```json
{
  "subscription": {
    "failedPaymentAttempts": 0,
    "redeemCodes": [],
    "type": "stripe",
    "trialing": false,
    "expiring": false,
    "renewalCount": 0,
    "alertPayment": false,
    "_id": "5e601cc4efd6034d0a74055e",
    "subscriptionId": "test123",
    "entitlement": "sl_premium",
    "planId": "sl_monthly",
    "user": "5e601cc1efd6034d0a74055a",
    "start": 1579201980,
    "end": 1894821181,
    "active": true,
    "stripeCustomerId": "cus_GqmHsFK8dBQ2gz",
    "promotionId": "5e601cc2efd6034d0a74055b",
    "createdAt": "2020-03-04T21:25:24.281Z",
    "updatedAt": "2020-03-04T21:25:24.281Z",
  }
}
```

## `POST /subscriptions/promotion`
Accepts requests to create trialing or direct-to-pay auto-renewing Promotional Subscriptions managed through Stripe.  This API is also responsible for creating new customers and payment methods in Stripe.  Auto-renewing Subscription created through this API must have an associated Customer and payment method in Stripe.  Additionally, this API validates the eligibility of each request against the Wavetrak Promotional Campaign API and a user's Subscription history for the brand associated with the Promotion.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `promotionId`  | The id of an active Wavetrak Promotion object  | String  | Yes |
| `promoCode`  | A unique and valid code associated with the Promotion  | String  | No |
| `planId`  | The stripe pricing plan applied to the auto-renewing subscription and associated promotion.  | String  | Yes  |
| `source`  | Tokenized credit card details.  | String  | Yes (if `cardId` is not passed)  |
| `cardId`  | The id of a credit card associated with an existing Stripe Customer.  | String  | Yes (if `source` is not passed)  |

### Response Parameters
| Param  | Description |
| ------------- | ------------- |
| `message`  | Indicates successful processing of the subscription |

### Example Request
```bash
curl --location --request POST 'https://services.surfline.com/subscriptions/promotion' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--data-raw '{
  "promotionId": "5b843ccda47037000ec69b8b"
  "planId": "sl_annual_v2",
  "source": "<STRIPE_SOURCE_TOKEN>",
  "promoCode": "ABC123"
}'
```

### Example Response
```json
{ "message":"Successfully processed the Subscription" }
```

## `GET /admin/subscriptions/wavetrak`
Returns subscriptions (across all brands) for a single customer.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `userId`  | The id of the customer  | String  | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------- |
| `subscriptions`  | An array of subscriptions | Array |

### Example Request
```bash
curl --location --request GET 'https://tools.surflineadmin.com/api/subscriptions/wavetrak?<USER_ID>' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>
```

### Example Response
```json
{
  "subscriptions":[
    {
      "failedPaymentAttempts":0,
      "redeemCodes":[],
      "type":"stripe",
      "trialing":false,
      "expiring":false,
      "renewalCount":0,
      "alertPayment":false,
      "_id":"6128214cd8f230cf6e3df409",
      "subscriptionId":"2ca412eb-ed01-4a3b-8138-f30bab64e1db",
      "entitlement":"bw_premium",
      "planId":"bw_monthly",
      "user":"6128214cd8f230cf6e3df407",
      "start":1579201980,
      "end":1894821181,
      "active":true,
      "stripeCustomerId":"cus_1234",
    },
    {
      "failedPaymentAttempts":0,
      "redeemCodes":[],
      "type":"stripe",
      "trialing":false,
      "expiring":false,
      "renewalCount":0,
      "alertPayment":false,
      "_id":"6128214cd8f230cf6e3df40a",
      "subscriptionId":"9acd4410-11a3-4d7c-8397-67a3cdf215ff",
      "entitlement":"sl_premium",
      "planId":"sl_monthly",
      "user":"6128214cd8f230cf6e3df407",
      "start":1579201980,
      "end":1894821181,
      "active":false,
      "stripeCustomerId":"cus_1234",
    }
  ]
}
```