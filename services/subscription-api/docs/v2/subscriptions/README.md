# Subscriptions APIs
This documentation details the consumable API contracts of the Subscription-Service.

## Table of Contents
 - [Subscriptions](./Subscription_API.md)
 - [Subscription Purchases](./Subscription_Purchase_API.md)
 - [Gift Payments and Redemptions](./Gifts.md)
 - [Payment Alerts](./Payment_Alerts.md)
 - [Admin endpoints](./Admin.md)
 - [Stripe Credit Cards](./Stripe_Credit_Cards.md)
 - [Stripe Invoices](./Stripe_Invoices.md)
 - [Stripe Product Pricing Plans](./Stripe_Pricing_Plans.md)