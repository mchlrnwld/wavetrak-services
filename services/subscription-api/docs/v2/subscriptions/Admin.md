# Admin endpoints

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [`GET /admin/subscriptions`](#get-adminsubscriptions)
- [`DELETE /admin/subscriptions/gifts`](#delete-adminsubscriptionsgifts)
- [`GET /admin/subscription-users/:userId`](#get-adminsubscription-usersuserid)
- [`PATCH /admin/subscription-users`](#patch-adminsubscription-users)
- [`POST /admin/coupons`](#post-admincoupons)
- [`GET /admin/coupons`](#get-admincoupons)
- [`DELETE /admin/coupons`](#delete-admincoupons)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## `GET /admin/subscriptions`
Retrieves all subscriptions for a single customer.

**NOTE: this endpoint is a duplicate of the `GET /admin/subscriptions/wavetrak` endpoint. The latter endpoint should be used instead of this one.**

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `userId`  | The id of the user | String | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------ |
| `subscriptions`  | An array of `subscription` objects | Array |

### Example Request
```bash
curl --location --request GET 'https://tools.surflineadmin.com/api/subscriptions?userId=<USER_ID>' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>' \
```

### Example Response
```json
{
  "subscriptions":[
    {
      "failedPaymentAttempts":0,
      "redeemCodes":[],
      "type":"stripe",
      "trialing":false,
      "expiring":false,
      "renewalCount":0,
      "alertPayment":false,
      "_id":"6128214cd8f230cf6e3df409",
      "subscriptionId":"2ca412eb-ed01-4a3b-8138-f30bab64e1db",
      "entitlement":"bw_premium",
      "planId":"bw_monthly",
      "user":"6128214cd8f230cf6e3df407",
      "start":1579201980,
      "end":1894821181,
      "active":true,
      "stripeCustomerId":"cus_1234",
    },
    {
      "failedPaymentAttempts":0,
      "redeemCodes":[],
      "type":"stripe",
      "trialing":false,
      "expiring":false,
      "renewalCount":0,
      "alertPayment":false,
      "_id":"6128214cd8f230cf6e3df40a",
      "subscriptionId":"9acd4410-11a3-4d7c-8397-67a3cdf215ff",
      "entitlement":"sl_premium",
      "planId":"sl_monthly",
      "user":"6128214cd8f230cf6e3df407",
      "start":1579201980,
      "end":1894821181,
      "active":false,
      "stripeCustomerId":"cus_1234",
    }
  ]
}
```

## `DELETE /admin/subscriptions/gifts`

This API is consumed by the `expire-gift-subscription` job in Airflow.  Requests to this API validate and deactivate the respective gift subscription if it has past its end date.  Additionally, this API triggers the downstream expiration events to Segment and include the respective subscription parameters.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `subscriptionId`  | The id of the Gift subscription to delete | String | Yes |
| `date`  | The timestamp for which any gift subscription should be expired if its `end` date is before this timestamp (Defaults to the current date.) | Number | No |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------ |
| `message`  | A string indicating the successful expiration of the gift subscription | String |

### Example Request
```bash
curl --location --request GET 'https://tools.surflineadmin.com/api/subscriptions?userId=<USER_ID>' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>' \
--data-raw '{
  "subscriptionId": ,
  "date": 
}'
```

### Example Response
```json
{ "message": "Successfully expired gift subscription" }
```

## `GET /admin/subscription-users/:userId`
Retrieves all SubscriptionUsers associated with a user

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `userId`  | The id of the user | String | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------ |
| `subscriptionUsers`  | An array of `SubscriptionUser` objects | Array |

### Example Request
```bash
curl --location --request GET 'https://tools.surflineadmin.com/api/subscription-users/<USER_ID>' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>' \
```

### Example Response
```json
{
  "subscriptionUsers":[
    {
      "_id":"6090b9f6ea496e24a6fa9535",
      "subscriptionId":"230000937945723",
      "type":"apple",
      "entitlement":"sl_premium",
      "user":{
        "_id":"5ad73a077c1a3a00138a446e",
        "email":"maringrohs@compuserve.com"
      },
      "primaryUser":"5ad73a077c1a3a00138a446e",
      "isPrimaryUser":true,
      "active":true,
    },
    {
      "_id":"6090b9f6ea496e24a6fa9536",
      "subscriptionId":"230000937945723",
      "type":"apple",
      "entitlement":"sl_premium",
      "user":{
        "_id":"572b480ea00bf0ce6d415976",
        "email":"martingrohs@compuserve.com"
      },
      "primaryUser":"5ad73a077c1a3a00138a446e",
      "isPrimaryUser":false,
      "active":true,
    }
  ]
}
```

## `PATCH /admin/subscription-users`
This endpoint is used for:
1) Updating the primaryUser for all associated SubscriptionUsers documents to the newPrimaryUser
2) Updating the user in Subscription and SubscriptionPurchase with the newPrimaryUser
3) Re-identifying both of the users in Segment

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `userId`  | The id of the user | String | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------ |
| `message`  | A string indicating the successul update | String |

### Example Request
```bash
curl --location --request PATCH 'https://tools.surflineadmin.com/api/subscription-users' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>' \
--data-raw '{
  "oldPrimaryUser": "5ad73a077c1a3a00138a446e",
  "newPrimaryUser": "572b480ea00bf0ce6d415976"
}'
```

### Example Response
```json
{ "message": "success" }
```

## `POST /admin/coupons`
Creates a Coupon object in Stripe.

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `name`  | The id/name of the Stripe coupon | String | Yes |
| `duration`  | Specifies how long the discount will be in effect | String | Yes |
| `percentOff`  | A positive float larger than 0, and smaller or equal to 100, that represents the discount the coupon will apply | Number | Yes |
| `maxRedemptions`  | Maximum number of times this coupon can be redeemed, in total, across all customers, before it is no longer valid | Number | Yes |
| `userId`  | The id of a Wavetrak user | String | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------ |
| `coupon`  | An object representing the newly created coupon | Object |

### Example Request
```bash
curl --location --request POST 'https://tools.surflineadmin.com/api/coupons' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>' \
--data-raw '{
  "name": <COUPON_NAME>,
  "duration": <DURATION>,
  "percentOff": <PERCENT_OFF>,
  "maxRedemptions": <MAX_REDEMPTIONS>
  "userId": <USER_ID>
}'
```

### Example Response
```json
{
  "coupon":{
    "id":"test",
    "object":"coupon",
    "amount_off":null,
    "created":1630019562,
    "currency":null,
    "duration":"once",
    "duration_in_months":null,
    "livemode":false,
    "max_redemptions":1,
    "metadata":{
      "userId":"61281fea1d1f9fcd22b0c1ab",
      "origin":"admin"
    },
    "name":"test",
    "percent_off":50,
    "redeem_by":null,
    "times_redeemed":0,
    "valid":true
  }
}
```

## `GET /admin/coupons`
Admin API for retrieving a Coupon object in Stripe

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `id`  | The id of the Stripe coupon | String | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------ |
| `coupon`  | An object representing the requested coupon | Object |

### Example Request
```bash
curl --location --request GET 'https://tools.surflineadmin.com/api/coupons?id=<COUPON_ID>' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>' \
```

### Example Response
```json
{
  "coupon":{
    "id":"test",
    "object":"coupon",
    "amount_off":null,
    "created":1630019562,
    "currency":null,
    "duration":"once",
    "duration_in_months":null,
    "livemode":false,
    "max_redemptions":1,
    "metadata":{
      "userId":"61281fea1d1f9fcd22b0c1ab",
      "origin":"admin"
    },
    "name":"test",
    "percent_off":50,
    "redeem_by":null,
    "times_redeemed":0,
    "valid":true
  }
}
```

## `DELETE /admin/coupons`
Admin API for deleting a Coupon object in Stripe

### Request Parameters
| Param | Description | Type | Required |
| ------------- | ------------- |  ------------- |  ------------- |
| `id`  | The id of the Stripe coupon to delete | String | Yes |

### Response Parameters
| Param  | Description | Type |
| ------------- | ------------- | ------------ |
| `message`  | A string indicating the successful deletion of the coupon | String |

### Example Request
```bash
curl --location --request DELETE 'https://tools.surflineadmin.com/api/coupons' \
--header 'Content-Type: application/json' \
--header 'x-auth-accesstoken: <VALID_TOKEN>' \
--header 'authorization: <BEARER_TOKEN>' \
--data-raw '{
  "id": <COUPON_ID>
}'
```

### Example Response
```json
{ "message": "Successfully deleted coupon: test_coupon" }
```