# Replace docker urls with localhost URL's
sed -i -e 's/\/\/mongo/\/\/127.0.0.1/g' ./.env.test
sed -i -e 's/redis/127.0.0.1/g' ./.env.test

# Start up mongo and redis containers
# Note: this command will fail if containers are already running, tests will continue
docker-compose -f ./integration-tests/docker-compose.test.yml up -d

# Run mocha integration tests
npm run test:integration:wip

# Replace localhost urls back to docker url's
sed -i -e 's/\/\/127.0.0.1/\/\/mongo/g' ./.env.test
sed -i -e 's/127.0.0.1/redis/g' ./.env.test

rm .env.test-e
