require('@babel/register')({
  /**
   * Allows stubbing of ES6 methods that are used within the same file for example:
   *
   * export const foo = () => true
   *
   * export const bar = () => foo()
   *
   * With rewire we are able to stub foo properly.
   * Docs: https://github.com/speedskater/babel-plugin-rewire
   */
  plugins: ['babel-plugin-rewire']
});

require('dotenv').config({ path: '.env.test' });

const logger = require('./src/common/logger');
const sinon = require('sinon');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

sinon.stub(logger, 'default').returns({
  trace: () => null,
  debug: () => null,
  info: () => null,
  error: () => null,
  warn: () => null
});
