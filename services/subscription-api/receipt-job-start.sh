#!/usr/bin/env bash

# receipt-job-start.sh
#   1) ENTRYPOINT for receipt job docker container

set -a && . ./.env && npm run tools:receipt_validation_job