provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/subscription-service/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "subscription_service" {
  source = "../../"

  company     = "sl"
  application = "subscription-service"
  environment = "prod"

  default_vpc      = "vpc-116fdb74"
  dns_name         = "subscription-service.prod.surfline.com"
  dns_zone_id      = "Z3LLOZIY0ZZQDE"
  ecs_cluster      = "sl-core-svc-prod"
  service_td_count = 2
  service_lb_rules = [
    {
      field = "host-header"
      value = "subscription-service.prod.surfline.com"
    },
  ]

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-2-prod/99e5fd75fdbf20fd/b668bfe144c94994"
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-2-prod/99e5fd75fdbf20fd"

  auto_scaling_enabled             = true
  auto_scaling_scale_by            = "schedule"
  auto_scaling_min_size            = 2
  auto_scaling_max_size            = 20
  auto_scaling_schedule_scale_up   = "cron(30 23 * * ? *)"
  auto_scaling_schedule_scale_down = "cron(0 3 * * ? *)"
}
