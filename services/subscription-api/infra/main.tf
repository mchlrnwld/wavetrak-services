module "subscription_service" {
  source = "git::ssh://git@github.com/Surfline/wavetrak-infrastructure.git//terraform/modules/aws/ecs/service"

  company      = var.company
  application  = var.application
  environment  = var.environment
  service_name = var.application

  service_lb_rules            = var.service_lb_rules
  service_lb_healthcheck_path = "/health"
  service_td_name             = "sl-core-svc-${var.environment}-${var.application}"
  service_td_container_name   = var.application
  service_td_count            = var.service_td_count
  service_port                = 8080
  service_alb_priority        = 414

  alb_listener      = var.alb_listener_arn
  iam_role_arn      = var.iam_role_arn
  dns_name          = var.dns_name
  dns_zone_id       = var.dns_zone_id
  load_balancer_arn = var.load_balancer_arn
  default_vpc       = var.default_vpc
  ecs_cluster       = var.ecs_cluster

  auto_scaling_enabled             = var.auto_scaling_enabled
  auto_scaling_scale_by            = var.auto_scaling_scale_by
  auto_scaling_min_size            = var.auto_scaling_min_size
  auto_scaling_max_size            = var.auto_scaling_max_size
  auto_scaling_schedule_scale_up   = var.auto_scaling_schedule_scale_up
  auto_scaling_schedule_scale_down = var.auto_scaling_schedule_scale_down
}

resource "aws_s3_bucket" "itunes_subscription_notifications" {
  bucket = "itunes-subscription-notifications-${var.environment}"
  acl    = "private"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  tags = {
    Name        = "${var.company}-${var.application}-itunes-subscription-notifications-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.application
  }
}

resource "aws_iam_policy" "itunes_subscription_notifications_policy" {
  name        = "${var.company}-${var.application}-s3_itunes_subscription_notifications_policy_${var.environment}"
  description = "policy to let subscription-service publish itunes data to s3"
  policy = templatefile("${path.module}/resources/s3-subscription-notification-policy.json", {
    resource_arn = "arn:aws:s3:::${aws_s3_bucket.itunes_subscription_notifications.bucket}"
  })
}

resource "aws_iam_role_policy_attachment" "attach_itunes_subscription_notifications_policy" {
  role       = "subscription_service_task_role_${var.environment}"
  policy_arn = aws_iam_policy.itunes_subscription_notifications_policy.arn
}

resource "aws_s3_bucket" "subscription_notifications" {
  bucket = "subscription-notifications-${var.environment}"
  acl    = "private"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  tags = {
    Name        = "${var.company}-${var.application}-subscription-notifications-${var.environment}"
    Application = var.application
    Company     = var.company
    Environment = var.environment
    Service     = var.application
  }
}

resource "aws_iam_policy" "subscription_notifications_policy" {
  name        = "${var.company}-${var.application}-s3_subscription_notifications_policy_${var.environment}"
  description = "policy to let subscription-service publish subscription notification data to s3"
  policy = templatefile("${path.module}/resources/s3-subscription-notification-policy.json", {
    resource_arn = "arn:aws:s3:::${aws_s3_bucket.subscription_notifications.bucket}"
  })
}

resource "aws_iam_role_policy_attachment" "attach_subscription_notifications_policy" {
  role       = "subscription_service_task_role_${var.environment}"
  policy_arn = aws_iam_policy.subscription_notifications_policy.arn
}
