#!/usr/bin/env bash

# receipt-job-run-task.sh
#   1) Runs task built from receipt-job-build-task.sh
#   2) Task output goes into Logsene & thrown errors would be reported on task completion
#
# Configure build specific environment variables in Jenkins job (CMD+F in this file for usages):
#   ENVIRONMENT='staging'
#   CLUSTER='sl-admin-tools-staging'

# Fixed environment variables
FAMILY=sl-jobs

# run task
$(aws --profile ${ENVIRONMENT} ecr get-login --no-include-email --region us-west-1)
TASK=$(aws ecs list-task-definitions --profile ${ENVIRONMENT} --family-prefix ${FAMILY} | jq -r '.taskDefinitionArns[-1]')

echo ${TASK}
echo ${CLUSTER}
echo ${ENVIRONMENT}

aws ecs run-task --task-definition ${TASK} --cluster ${CLUSTER} --profile ${ENVIRONMENT} --region us-west-1
