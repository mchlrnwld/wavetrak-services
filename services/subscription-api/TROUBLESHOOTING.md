# Troubleshooting the Subscription Service

## Generic Log Level Descriptions

| Level | Description |
|---| --------------- |
| DEBUG | Designates fine-grained informational events that are most useful to debug an application. |
| ERROR | Designates error events that might still allow the application to continue running. |
| INFO | Designates informational messages that highlight the progress of the application at coarse-grained level. |
| TRACE | Designates finer-grained informational events than the DEBUG. |
| WARN | Designates potentially harmful situations. |


## Subscription Endpoint errors

**Logger `subscriptions`**

All non `200` requests will also generate a corresponding log event. `4xx`
errors will usually result in a `WARN` level error message. `500` errors
will emit an `ERROR` level log.

## Stripe Webhook errors

**Logger: `subscriptions-integration-stripe`**

All errors returned by  Stripe Webhook integrations will contain the following
properties to help with debugging

* `eventId`
* `eventType`

These functions are defined in 

A successful webhook...

1. is received
2. is verified
3. is processed

An unhealthy webhook could...

* fail verification
* throw a fatal javascript error (from the backend handler)
* unsuccessfully process an event ("soft fail")
* not have an appropriate handler for the event


### Ensuring Stripe Webhook events are being correctly processed

The subscription backend uses redis to ensure that Stripe requests are
idempotent.

A webhook will be processed if

* The event signature is in not Redis.
* The event can be confirmed as legitimate by Stripe.

If the above invariants are false an event will fail
verification, cease to process and emit a log message at a `WARN` level.

To turn off event verification set the environment variable `VERIFY_WEBHOOK`
to `'false'`. This setting is useful for development environments where the
database may not be in sync with Stripe.

### `401` There was a problem processing the event.

_If `VERIFY_WEBHOOKS` is set to `'false'` this error won't fire._


## Common Errors and their Sources


    /subscription-service/src/external/stripe.js:104
            subscriptionId: subscription.id,
                                        ^

    TypeError: Cannot read property 'id' of null
        at stripe.js:91:27
        at [object Object]._onTimeout (/subscription-service/node_modules/stripe/lib/StripeResource.js:86:35)
        at Timer.listOnTimeout (timers.js:92:15)

**Reason:** There isn't a corresponding plan in Stripe.
