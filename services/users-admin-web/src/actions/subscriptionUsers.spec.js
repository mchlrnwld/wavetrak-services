import chai, { expect } from 'chai';
import sinon from 'sinon';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as api from '../common/api';
import {
  fetchSubscriptionUsers,
  updatePrimaryUser,
  FETCH_SUBSCRIPTION_USERS,
  FETCH_SUBSCRIPTION_USERS_SUCCESS,
  FETCH_SUBSCRIPTION_USERS_FAILURE,
  UPDATE_PRIMARY_USER,
  UPDATE_PRIMARY_USER_SUCCESS,
  UPDATE_PRIMARY_USER_FAILURE,
} from './subscriptionUsers';

chai.use(dirtyChai);
chai.use(sinonChai);

describe('Actions / SubscriptionUsers', () => {
  let mockStore;
  let getSubscriptionUsersStub;
  let patchSubscriptionUsersStub;

  before(() => {
    mockStore = configureMockStore([thunk]);
  });

  beforeEach(() => {
    getSubscriptionUsersStub = sinon.stub(api, 'getSubscriptionUsers');
    patchSubscriptionUsersStub = sinon.stub(api, 'patchSubscriptionUsers');
  });

  afterEach(() => {
    getSubscriptionUsersStub.restore();
    patchSubscriptionUsersStub.restore();
  });

  describe('fetchSubscriptionUsers', () => {
    it('gets subscription users', async () => {
      const store = mockStore({});
      expect(store.getActions()).to.be.empty();
      getSubscriptionUsersStub.resolves({
        subscriptionUsers: [
          {
            _id: '6090b9f6ea496e24a6fa9535',
            subscriptionId: '230000937945723',
            type: 'apple',
            entitlement: 'sl_premium',
            user: { _id: '5ad73a077c1a3a00138a446e', email: 'a@test.com' },
            primaryUser: '572b480ea00bf0ce6d415976',
            isPrimaryUser: false,
            active: true,
            createdAt: '2021-05-04T03:05:26.301Z',
            updatedAt: '2021-08-16T19:14:37.319Z',
          },
          {
            _id: '6090b9f6ea496e24a6fa9536',
            subscriptionId: '230000937945723',
            type: 'apple',
            entitlement: 'sl_premium',
            user: { _id: '572b480ea00bf0ce6d415976', email: 'b@test.com' },
            primaryUser: '572b480ea00bf0ce6d415976',
            isPrimaryUser: true,
            active: true,
            createdAt: '2021-05-04T03:05:26.344Z',
            updatedAt: '2021-08-16T19:14:37.324Z',
          },
        ],
      });
      const expectedActions = [
        { type: FETCH_SUBSCRIPTION_USERS },
        {
          type: FETCH_SUBSCRIPTION_USERS_SUCCESS,
          subscriptionUsers: [
            {
              _id: '6090b9f6ea496e24a6fa9535',
              subscriptionId: '230000937945723',
              type: 'apple',
              entitlement: 'sl_premium',
              user: { _id: '5ad73a077c1a3a00138a446e', email: 'a@test.com' },
              primaryUser: '572b480ea00bf0ce6d415976',
              isPrimaryUser: false,
              active: true,
              createdAt: '2021-05-04T03:05:26.301Z',
              updatedAt: '2021-08-16T19:14:37.319Z',
            },
            {
              _id: '6090b9f6ea496e24a6fa9536',
              subscriptionId: '230000937945723',
              type: 'apple',
              entitlement: 'sl_premium',
              user: { _id: '572b480ea00bf0ce6d415976', email: 'b@test.com' },
              primaryUser: '572b480ea00bf0ce6d415976',
              isPrimaryUser: true,
              active: true,
              createdAt: '2021-05-04T03:05:26.344Z',
              updatedAt: '2021-08-16T19:14:37.324Z',
            },
          ],
        },
      ];

      await store.dispatch(fetchSubscriptionUsers('1234'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(getSubscriptionUsersStub).to.have.been.calledOnce();
      expect(getSubscriptionUsersStub).to.have.been.calledWithExactly('1234');
    });

    it('errors on failure', async () => {
      const store = mockStore({});
      expect(store.getActions()).to.be.empty();
      getSubscriptionUsersStub.rejects('Error');
      const expectedActions = [
        { type: FETCH_SUBSCRIPTION_USERS },
        { type: FETCH_SUBSCRIPTION_USERS_FAILURE, error: 'Unable to fetch linked accounts' },
      ];

      await store.dispatch(fetchSubscriptionUsers('1234'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(getSubscriptionUsersStub).to.have.been.calledOnce();
      expect(getSubscriptionUsersStub).to.have.been.calledWithExactly('1234');
    });
  });

  describe('updateUserInfo', () => {
    it('updates the primary user', async () => {
      const store = mockStore({});
      expect(store.getActions()).to.be.empty();
      patchSubscriptionUsersStub.resolves({ message: 'success' });

      const expectedActions = [
        { type: UPDATE_PRIMARY_USER },
        { type: UPDATE_PRIMARY_USER_SUCCESS },
      ];

      await store.dispatch(updatePrimaryUser('1234', '5678'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(patchSubscriptionUsersStub).to.have.been.calledOnce();
      expect(patchSubscriptionUsersStub).to.have.been.calledWithExactly({
        oldPrimaryUser: '1234',
        newPrimaryUser: '5678',
      });
    });

    it('errors on failure', async () => {
      const store = mockStore({});
      expect(store.getActions()).to.be.empty();
      patchSubscriptionUsersStub.rejects('Error');

      const expectedActions = [
        { type: UPDATE_PRIMARY_USER },
        { type: UPDATE_PRIMARY_USER_FAILURE, error: 'Unable to update primary user' },
      ];

      await store.dispatch(updatePrimaryUser('1234', '5678'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(patchSubscriptionUsersStub).to.have.been.calledOnce();
      expect(patchSubscriptionUsersStub).to.have.been.calledWithExactly({
        oldPrimaryUser: '1234',
        newPrimaryUser: '5678',
      });
    });
  });
});
