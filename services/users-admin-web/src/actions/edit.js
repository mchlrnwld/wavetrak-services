import sortSubscriptions from '../utils/sortSubscriptions';
import * as api from '../common/api';

export const FETCH_USER_DETAILS = 'FETCH_USER_DETAILS';
export const FETCH_USER_DETAILS_SUCCESS = 'FETCH_USER_DETAILS_SUCCESS';
export const FETCH_USER_DETAILS_FAILURE = 'FETCH_USER_DETAILS_FAILURE';

export const CLEAR_USER_TOKENS = 'CLEAR_USER_TOKENS';
export const CLEAR_USER_TOKENS_SUCCESS = 'CLEAR_USER_TOKENS_SUCCESS';
export const CLEAR_USER_TOKENS_FAILURE = 'CLEAR_USER_TOKENS_FAILURE';

export const UPDATE_USER_INFO = 'UPDATE_USER_INFO';
export const UPDATE_USER_INFO_SUCCESS = 'UPDATE_USER_INFO_SUCCESS';
export const UPDATE_USER_INFO_FAILURE = 'UPDATE_USER_INFO_FAILURE';

export const RESET_PASSWORD = 'RESET_PASSWORD';
export const RESET_PASSWORD_SUCCESS = 'RESET_PASSWORD_SUCCESS';
export const RESET_PASSWORD_FAILURE = 'RESET_PASSWORD_FAILURE';

export const PASSWORD_COPIED = 'PASSWORD_COPIED';

export const PROCESS_GDPR_REQUEST = 'PROCESS_GDPR_REQUEST';
export const PROCESS_GDPR_REQUEST_SUCCESS = 'PROCESS_GDPR_REQUEST_SUCCESS';
export const PROCESS_GDPR_REQUEST_FAILURE = 'PROCESS_GDPR_REQUEST_FAILURE';

export const FETCH_CUSTOMER_DETAILS = 'FETCH_CUSTOMER_DETAILS';
export const FETCH_CUSTOMER_DETAILS_SUCCESS = 'FETCH_CUSTOMER_DETAILS_SUCCESS';
export const FETCH_CUSTOMER_DETAILS_FAILURE = 'FETCH_CUSTOMER_DETAILS_FAILURE';

export const DISSOCIATE_CUSTOMER = 'DISSOCIATE_CUSTOMER';
export const DISSOCIATE_CUSTOMER_SUCCESS = 'DISSOCIATE_CUSTOMER_SUCCESS';
export const DISSOCIATE_CUSTOMER_FAILURE = 'DISSOCIATE_CUSTOMER_FAILURE';

export const EMAIL_VERIFICATION = 'EMAIL_VERIFICATION';
export const EMAIL_VERIFICATION_SUCCESS = 'EMAIL_VERIFICATION_SUCCESS';
export const EMAIL_VERIFICATION_FAILURE = 'EMAIL_VERIFICATION_FAILURE';

export const fetchUserDetails = (userId) => async (dispatch) => {
  dispatch({ type: FETCH_USER_DETAILS });
  try {
    let userAccount = await api.fetchUserDetails(userId);

    if (!userAccount) {
      // No v1 Subscription found. Get the UserInfo details first.
      const { user: userInfo } = await api.findUserDetails(userId);
      /*
        Populate the userAccount with user info details. 
        We will set the subscription-* related fields to empty or null
        since we query V2 Subscription seperately
      */
      userAccount = {
        user: userInfo,
        subscriptions: [],
        archivedSubscriptions: [],
        stripeCustomerId: null,
        receipts: [],
      };
    }

    const { user, subscriptions, archivedSubscriptions, stripeCustomerId, receipts } = userAccount;
    const nonV2Subscriptions = subscriptions.filter(
      (sub) => sub.type !== 'apple' && sub.type !== 'google' && sub.type !== 'gift',
    );
    // Sort the past/present subscription data by date
    // and type for easier readabiliy on the front-end
    // Sort the past and present subscriptions by type and date if needed
    const sortedSubscriptions = sortSubscriptions(nonV2Subscriptions);
    const sortedPastSubscriptions = sortSubscriptions(archivedSubscriptions);
    dispatch({
      type: FETCH_USER_DETAILS_SUCCESS,
      user: {
        id: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        isEmailVerified: user.isEmailVerified,
        brand: user.brand,
        stripeCustomerId,
        subscriptions: sortedSubscriptions,
        archivedSubscriptions: sortedPastSubscriptions,
        receipts,
        googleInAppPurchases: subscriptions.filter(
          (subscription) => subscription.type === 'google',
        ),
        appleInAppPurchases: subscriptions.filter((subscription) => subscription.type === 'apple'),
      },
    });
  } catch (error) {
    dispatch({ type: FETCH_USER_DETAILS_FAILURE, error: 'User not found' });
  }
};

export const fetchCustomerDetails = (customerId) => async (dispatch) => {
  dispatch({ type: FETCH_CUSTOMER_DETAILS });
  try {
    const { customer } = await api.getCustomerInfo(customerId);
    return dispatch({ type: FETCH_CUSTOMER_DETAILS_SUCCESS, customer });
  } catch (error) {
    return dispatch({
      type: FETCH_CUSTOMER_DETAILS_FAILURE,
      error: 'Error fetching customer from Stripe.',
    });
  }
};

export const dissociateCustomer = (userId, customerId) => async (dispatch) => {
  dispatch({ type: DISSOCIATE_CUSTOMER });
  try {
    await api.dissociateCustomer(customerId);
    dispatch({ type: DISSOCIATE_CUSTOMER_SUCCESS });
    return dispatch(fetchUserDetails(userId));
  } catch (error) {
    return dispatch({
      type: DISSOCIATE_CUSTOMER_FAILURE,
      error: error.message,
    });
  }
};

export const clearUserTokens = () => async (dispatch, getState) => {
  dispatch({ type: CLEAR_USER_TOKENS });
  try {
    const state = getState();
    const userId = state.edit.user.id;
    const response = await api.clearUserTokens(userId);
    // Create and move to baseFetch.js
    if (response.status === 200) {
      dispatch({ type: CLEAR_USER_TOKENS_SUCCESS, message: 'Tokens cleared successfuly' });
    } else {
      dispatch({ type: CLEAR_USER_TOKENS_FAILURE, message: 'Unable to clear the tokens' });
    }
  } catch (error) {
    dispatch({ type: CLEAR_USER_TOKENS_FAILURE, message: 'Unable to clear the tokens' });
  }
};

export const updateUserInfo = (newEmail, newFirstName, newLastName) => async (
  dispatch,
  getState,
) => {
  dispatch({ type: UPDATE_USER_INFO });

  try {
    const state = getState();
    const userId = state.edit.user.id;

    if (newEmail) {
      // Make sure the email is not already in use
      const doesEmailExist = await api.validateEmail(newEmail);
      if (doesEmailExist.status === 200) {
        return dispatch({
          type: UPDATE_USER_INFO_FAILURE,
          error: 'Email already in use',
        });
      }
    }

    await api.updateUserInfo(userId, newEmail, newFirstName, newLastName);
    return dispatch({
      type: UPDATE_USER_INFO_SUCCESS,
      email: newEmail,
      firstName: newFirstName,
      lastName: newLastName,
    });
  } catch (error) {
    return dispatch({
      type: UPDATE_USER_INFO_FAILURE,
      error: "Unable to update user's info",
    });
  }
};

export const passwordReset = (userId, brand) => async (dispatch) => {
  dispatch({ type: RESET_PASSWORD });
  try {
    const response = await api.passwordReset(userId, brand);
    const { temporaryPassword: password } = response;
    dispatch({ type: RESET_PASSWORD_SUCCESS, password });
  } catch (error) {
    dispatch({ type: RESET_PASSWORD_FAILURE, message: 'Password Reset failed.' });
  }
};

export const copyPassword = () => async (dispatch) => dispatch({ type: PASSWORD_COPIED });

export const processGDPRRequest = (userId) => async (dispatch) => {
  dispatch({ type: PROCESS_GDPR_REQUEST });
  try {
    const { deleted, message } = await api.deleteUser(userId);

    if (deleted) {
      dispatch({ type: PROCESS_GDPR_REQUEST_SUCCESS, success: message });
      return;
    }
    dispatch({ type: PROCESS_GDPR_REQUEST_FAILURE, error: message });
  } catch (error) {
    dispatch({
      type: PROCESS_GDPR_REQUEST_FAILURE,
      error: `GDPR Request failed. Error: ${JSON.stringify(error.message)}`,
    });
  }
};

export const verifyEmail = (userId, email, brand) => async (dispatch) => {
  dispatch({ type: EMAIL_VERIFICATION });
  try {
    const response = await api.verifyUser(userId, email, brand);
    if (response.isEmailVerified) {
      dispatch({ type: EMAIL_VERIFICATION_SUCCESS, message: 'Email verified.' });
    } else {
      dispatch({
        type: EMAIL_VERIFICATION_FAILURE,
        message: 'Email verification failed.',
        error: response.statusText || 'An error ocurred',
      });
    }
  } catch (error) {
    dispatch({
      type: EMAIL_VERIFICATION_FAILURE,
      message: 'Email verification failed.',
      error: 'An error occurred',
    });
  }
};
