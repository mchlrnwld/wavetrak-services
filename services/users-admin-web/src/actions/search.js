import { findUsers } from '../common/api';
import history from '../history';

export const SEARCH = 'SEARCH';
export const SEARCH_SUCCESS = 'SEARCH_SUCCESS';
export const SEARCH_FAILURE = 'SEARCH_FAILURE';

export const searchUsers = (query, options = { link: null }) => async (dispatch) => {
  dispatch({ type: SEARCH });
  try {
    const response = await findUsers(query);
    dispatch({
      type: SEARCH_SUCCESS,
      results: response,
    });

    const baseUrl = `${window.location.pathname}?q=${query}`;
    const url = options.link ? `${baseUrl}&link=${options.link}` : baseUrl;

    history.push(url);
  } catch (error) {
    dispatch({ type: SEARCH_FAILURE, error: 'Search failed' });
  }
};
