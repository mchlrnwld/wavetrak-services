import * as api from '../common/api';

export const FETCH_SUBSCRIPTION_USERS = 'FETCH_SUBSCRIPTION_USERS';
export const FETCH_SUBSCRIPTION_USERS_SUCCESS = 'FETCH_SUBSCRIPTION_USERS_SUCCESS';
export const FETCH_SUBSCRIPTION_USERS_FAILURE = 'FETCH_SUBSCRIPTION_USERS_FAILURE';

export const UPDATE_PRIMARY_USER = 'UPDATE_SUBSCRIPTION_USERS';
export const UPDATE_PRIMARY_USER_SUCCESS = 'UPDATE_SUBSCRIPTION_USERS_SUCCESS';
export const UPDATE_PRIMARY_USER_FAILURE = 'UPDATE_SUBSCRIPTION_USERS_FAILURE';

export const fetchSubscriptionUsers = (userId) => async (dispatch) => {
  dispatch({ type: FETCH_SUBSCRIPTION_USERS });
  try {
    const { subscriptionUsers } = await api.getSubscriptionUsers(userId);
    dispatch({
      type: FETCH_SUBSCRIPTION_USERS_SUCCESS,
      subscriptionUsers,
    });
  } catch (error) {
    dispatch({ type: FETCH_SUBSCRIPTION_USERS_FAILURE, error: 'Unable to fetch linked accounts' });
  }
};

export const updatePrimaryUser = (oldPrimaryUser, newPrimaryUser) => async (dispatch) => {
  dispatch({ type: UPDATE_PRIMARY_USER });
  try {
    await api.patchSubscriptionUsers({ oldPrimaryUser, newPrimaryUser });
    dispatch({
      type: UPDATE_PRIMARY_USER_SUCCESS,
    });
  } catch (error) {
    dispatch({ type: UPDATE_PRIMARY_USER_FAILURE, error: 'Unable to update primary user' });
  }
};
