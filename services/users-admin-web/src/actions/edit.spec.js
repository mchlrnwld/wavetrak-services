import chai, { expect } from 'chai';
import sinon from 'sinon';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as api from '../common/api';
import {
  fetchUserDetails,
  clearUserTokens,
  updateUserInfo,
  verifyEmail,
  FETCH_USER_DETAILS,
  FETCH_USER_DETAILS_SUCCESS,
  FETCH_USER_DETAILS_FAILURE,
  CLEAR_USER_TOKENS,
  CLEAR_USER_TOKENS_SUCCESS,
  CLEAR_USER_TOKENS_FAILURE,
  UPDATE_USER_INFO,
  UPDATE_USER_INFO_SUCCESS,
  UPDATE_USER_INFO_FAILURE,
  EMAIL_VERIFICATION,
  EMAIL_VERIFICATION_SUCCESS,
  EMAIL_VERIFICATION_FAILURE,
} from './edit';
import {
  VERIFY_RECEIPT,
  VERIFY_RECEIPT_SUCCESS,
  verifyReceipt,
  VERIFY_RECEIPT_FAILURE,
  restorePurchases,
  RESTORE_PURCHASES,
  RESTORE_PURCHASES_SUCCESS,
  RESTORE_PURCHASES_FAILURE,
} from './editIap';

chai.use(dirtyChai);
chai.use(sinonChai);

describe('Actions / Edit', () => {
  let mockStore;
  let fetchUserDetailsStub;
  let updateUserInfoStub;
  let validateEmailStub;
  let verifyReceiptStub;
  let restorePurchasesStub;
  let verifyUserStub;
  let findUserDetailsStub;

  before(() => {
    mockStore = configureMockStore([thunk]);
  });

  beforeEach(() => {
    fetchUserDetailsStub = sinon.stub(api, 'fetchUserDetails');
    sinon.stub(api, 'clearUserTokens');
    updateUserInfoStub = sinon.stub(api, 'updateUserInfo');
    validateEmailStub = sinon.stub(api, 'validateEmail');
    verifyReceiptStub = sinon.stub(api, 'verifyReceipt');
    restorePurchasesStub = sinon.stub(api, 'restorePurchases');
    verifyUserStub = sinon.stub(api, 'verifyUser');
    findUserDetailsStub = sinon.stub(api, 'findUserDetails');
  });

  afterEach(() => {
    fetchUserDetailsStub.restore();
    api.clearUserTokens.restore();
    updateUserInfoStub.restore();
    validateEmailStub.restore();
    verifyReceiptStub.restore();
    restorePurchasesStub.restore();
    verifyUserStub.restore();
    findUserDetailsStub.restore();
  });

  describe('fetchUserDetails', () => {
    it('gets user details for store', async () => {
      const store = mockStore({});
      expect(store.getActions()).to.be.empty();
      fetchUserDetailsStub.resolves({
        name: 'Joe Smoe',
        user: {
          _id: 'some userid',
          firstName: 'Joe',
          lastName: 'Smoe',
          email: 'joesmoe@test.com',
          isEmailVerified: true,
          brand: 'sl',
        },
        subscriptions: [],
        receipts: [],
        archivedSubscriptions: [],
        stripeCustomerId: 'some id',
      });
      const expectedActions = [
        { type: FETCH_USER_DETAILS },
        {
          type: FETCH_USER_DETAILS_SUCCESS,
          user: {
            id: 'some userid',
            firstName: 'Joe',
            lastName: 'Smoe',
            email: 'joesmoe@test.com',
            isEmailVerified: true,
            brand: 'sl',
            googleInAppPurchases: [],
            appleInAppPurchases: [],
            archivedSubscriptions: [],
            subscriptions: [],
            receipts: [],
            stripeCustomerId: 'some id',
          },
        },
      ];

      await store.dispatch(fetchUserDetails('some userid'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(fetchUserDetailsStub).to.have.been.calledOnce();
      expect(fetchUserDetailsStub).to.have.been.calledWithExactly('some userid');
    });

    it('gets user info details for users having only V2 subscription from store', async () => {
      const store = mockStore({});
      expect(store.getActions()).to.be.empty();
      fetchUserDetailsStub.resolves(null);
      findUserDetailsStub.resolves({
        user: {
          _id: 'some userid',
          firstName: 'Joe',
          lastName: 'Smoe',
          email: 'joesmoe@test.com',
          isEmailVerified: true,
          brand: 'sl',
        },
      });
      const expectedActions = [
        { type: FETCH_USER_DETAILS },
        {
          type: FETCH_USER_DETAILS_SUCCESS,
          user: {
            id: 'some userid',
            firstName: 'Joe',
            lastName: 'Smoe',
            email: 'joesmoe@test.com',
            isEmailVerified: true,
            brand: 'sl',
            googleInAppPurchases: [],
            appleInAppPurchases: [],
            archivedSubscriptions: [],
            subscriptions: [],
            receipts: [],
            stripeCustomerId: null,
          },
        },
      ];

      await store.dispatch(fetchUserDetails('some userid'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(fetchUserDetailsStub).to.have.been.calledOnce();
      expect(fetchUserDetailsStub).to.have.been.calledWithExactly('some userid');
      expect(findUserDetailsStub).to.have.been.calledOnce();
      expect(findUserDetailsStub).to.have.been.calledWithExactly('some userid');
    });

    it('errors on failure', async () => {
      const store = mockStore({});
      expect(store.getActions()).to.be.empty();
      fetchUserDetailsStub.rejects('Internal error not to be exposed to user');
      const expectedActions = [
        { type: FETCH_USER_DETAILS },
        { type: FETCH_USER_DETAILS_FAILURE, error: 'User not found' },
      ];

      await store.dispatch(fetchUserDetails('some userid'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(fetchUserDetailsStub).to.have.been.calledOnce();
      expect(fetchUserDetailsStub).to.have.been.calledWithExactly('some userid');
    });
  });

  describe('clearUserTokens', () => {
    it('clears access_tokens for a user', async () => {
      const store = mockStore({
        edit: {
          user: { id: '59f164c72d787c00115ee1f0' },
        },
      });
      expect(store.getActions()).to.be.empty();
      const expectedActions = [
        { type: CLEAR_USER_TOKENS },
        {
          type: CLEAR_USER_TOKENS_SUCCESS,
          message: 'Tokens cleared successfuly',
        },
      ];
      api.clearUserTokens.returns({ status: 200 });
      await store.dispatch(clearUserTokens());
      expect(store.getActions()).to.deep.equal(expectedActions);
    });

    it('errors while clearing access_tokens for a user', async () => {
      const store = mockStore({
        edit: {
          user: { id: '59f164c72d787c00115ee1f0' },
        },
      });
      expect(store.getActions()).to.be.empty();
      const expectedActions = [
        { type: CLEAR_USER_TOKENS },
        {
          type: CLEAR_USER_TOKENS_FAILURE,
          message: 'Unable to clear the tokens',
        },
      ];
      api.clearUserTokens.returns({ status: 404 });
      await store.dispatch(clearUserTokens());
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });

  describe('updateUserInfo', () => {
    it("update a user's info, then reflect the change in the store", async () => {
      const store = mockStore({
        edit: {
          user: {
            email: 'some@email.com',
            firstName: 'name',
            lastName: 'lastname',
            id: 'someId',
          },
        },
      });
      expect(store.getActions()).to.be.empty();
      validateEmailStub.resolves({ status: 400 });
      updateUserInfoStub.resolves();
      const expectedActions = [
        { type: UPDATE_USER_INFO },
        {
          type: UPDATE_USER_INFO_SUCCESS,
          email: 'somenew@email.com',
          firstName: 'newName',
          lastName: 'newName2',
        },
      ];

      await store.dispatch(updateUserInfo('somenew@email.com', 'newName', 'newName2'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(updateUserInfoStub).to.have.been.calledOnce();
      expect(updateUserInfoStub).to.have.been.calledWithExactly(
        'someId',
        'somenew@email.com',
        'newName',
        'newName2',
      );
    });

    it('errors if the email already exists', async () => {
      const store = mockStore({
        edit: {
          user: {
            email: 'some@email.com',
            firstName: 'name',
            lastName: 'lastname',
            id: 'someId',
          },
        },
      });
      expect(store.getActions()).to.be.empty();
      validateEmailStub.resolves({ status: 200 });
      const expectedActions = [
        { type: UPDATE_USER_INFO },
        { type: UPDATE_USER_INFO_FAILURE, error: 'Email already in use' },
      ];

      await store.dispatch(updateUserInfo('some email'));
      expect(store.getActions()).to.deep.equal(expectedActions);
    });

    it('errors on failure', async () => {
      const store = mockStore({
        edit: {
          user: {
            email: 'some@email.com',
            firstName: 'name',
            lastName: 'lastname',
            id: 'someId',
          },
        },
      });
      expect(store.getActions()).to.be.empty();
      validateEmailStub.resolves({ status: 400 });
      updateUserInfoStub.rejects('Internal error not to be exposed to user');
      const expectedActions = [
        { type: UPDATE_USER_INFO },
        { type: UPDATE_USER_INFO_FAILURE, error: "Unable to update user's info" },
      ];

      await store.dispatch(updateUserInfo('some email', 'firstName', 'lastName'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(validateEmailStub).to.have.been.calledOnce();
      expect(validateEmailStub).to.have.been.calledWithExactly('some email');
      expect(updateUserInfoStub).to.have.been.calledOnce();
      expect(updateUserInfoStub).to.have.been.calledWithExactly(
        'someId',
        'some email',
        'firstName',
        'lastName',
      );
    });
  });

  describe('verifyReceipt', () => {
    it('pulls the receipt data and puts it in the store', async () => {
      const store = mockStore({
        edit: {
          user: {
            id: 'someuserId',
          },
          verifiedReceipts: {},
        },
      });
      expect(store.getActions()).to.be.empty();
      verifyReceiptStub.resolves({ data: 'some-receipt-data' });
      const expectedActions = [
        { type: VERIFY_RECEIPT, receiptNumber: 0 },
        {
          type: VERIFY_RECEIPT_SUCCESS,
          receiptNumber: 0,
          verifiedReceipt: {
            data: 'some-receipt-data',
          },
        },
      ];

      await store.dispatch(verifyReceipt(0, 'receiptData', 'sl', 'apple'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(verifyReceiptStub).to.have.been.calledOnce();
      expect(verifyReceiptStub).to.have.been.calledWithExactly(
        'someuserId',
        'receiptData',
        'sl',
        'apple',
      );
    });

    it('errors on failure', async () => {
      const store = mockStore({
        edit: {
          user: {
            id: 'someuserId',
          },
          verifiedReceipts: {},
        },
      });
      expect(store.getActions()).to.be.empty();
      verifyReceiptStub.rejects('Internal error not to be exposed to user');
      const expectedActions = [
        { type: VERIFY_RECEIPT, receiptNumber: 0 },
        { type: VERIFY_RECEIPT_FAILURE, error: 'Unable to verify receipt', receiptNumber: 0 },
      ];

      await store.dispatch(verifyReceipt(0, 'receiptData', 'sl', 'apple'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(verifyReceiptStub).to.have.been.calledOnce();
      expect(verifyReceiptStub).to.have.been.calledWithExactly(
        'someuserId',
        'receiptData',
        'sl',
        'apple',
      );
    });
  });

  describe('restorePurchases', () => {
    it("Restores a user's purchases, and updates the user object", async () => {
      const originalUserObject = {
        id: 'some userid',
        firstName: 'Joe',
        lastName: 'Smoe',
        email: 'joesmoe@test.com',
        stripeCustomerId: 'some id',
        googleInAppPurchases: [],
        appleInAppPurchases: [],
        subscriptions: [{ product: 'fs_premium', type: 'stripe' }],
        receipts: [{ receiptData: 'someReceipt1', productId: 'sl_premium' }],
      };
      const restoredUserObject = {
        receipts: [
          { receiptData: 'someReceipt1', productId: 'sl_premium' },
          { receiptData: 'someReceipt2', productId: 'fs_premium' },
        ],
        subscriptions: [
          { product: 'sl_premium', type: 'apple' },
          { product: 'fs_premium', type: 'stripe' },
        ],
        googleInAppPurchases: [],
        appleInAppPurchases: [{ product: 'sl_premium', type: 'apple' }],
      };
      const store = mockStore({
        edit: {
          user: originalUserObject,
        },
      });
      expect(store.getActions()).to.be.empty();
      restorePurchasesStub.resolves(restoredUserObject);
      const expectedActions = [
        { type: RESTORE_PURCHASES },
        {
          type: RESTORE_PURCHASES_SUCCESS,
          user: {
            ...originalUserObject,
            ...restoredUserObject,
          },
        },
      ];

      await store.dispatch(restorePurchases());
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(restorePurchasesStub).to.have.been.calledOnce();
      expect(restorePurchasesStub).to.have.been.calledWithExactly('some userid');
    });

    it('errors on failure', async () => {
      const store = mockStore({
        edit: {
          user: {
            id: 'someuserId',
          },
        },
      });
      expect(store.getActions()).to.be.empty();
      restorePurchasesStub.rejects('Internal error not to be exposed to user');
      const expectedActions = [
        { type: RESTORE_PURCHASES },
        { type: RESTORE_PURCHASES_FAILURE, error: 'Unable to restore purchases' },
      ];

      await store.dispatch(restorePurchases());
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(restorePurchasesStub).to.have.been.calledOnce();
      expect(restorePurchasesStub).to.have.been.calledWithExactly('someuserId');
    });
  });

  describe('verifyEmail', () => {
    it("verify a user's email, then reflect the change in the store", async () => {
      const store = mockStore({
        edit: {
          user: {
            isEmailVerified: false,
          },
        },
      });
      expect(store.getActions()).to.be.empty();
      verifyUserStub.resolves({ isEmailVerified: true });
      const expectedActions = [
        { type: EMAIL_VERIFICATION },
        {
          type: EMAIL_VERIFICATION_SUCCESS,
          message: 'Email verified.',
        },
      ];
      await store.dispatch(verifyEmail('someUserId', 'test@email.com', 'sl'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(verifyUserStub).to.have.been.calledOnce();
      expect(verifyUserStub).to.have.been.calledWithExactly('someUserId', 'test@email.com', 'sl');
    });

    it('errors on failure', async () => {
      const store = mockStore({
        edit: {
          user: {
            isEmailVerified: false,
          },
        },
      });

      expect(store.getActions()).to.be.empty();
      verifyUserStub.resolves({ status: 400, statusText: 'Bad Request' });
      const expectedActions = [
        { type: EMAIL_VERIFICATION },
        {
          type: EMAIL_VERIFICATION_FAILURE,
          message: 'Email verification failed.',
          error: 'Bad Request',
        },
      ];

      await store.dispatch(verifyEmail('someUserId', 'test@email.com', 'sl'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(verifyUserStub).to.have.been.calledOnce();
      expect(verifyUserStub).to.have.been.calledWithExactly('someUserId', 'test@email.com', 'sl');
    });
  });
});
