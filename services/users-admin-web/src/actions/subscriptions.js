import * as api from '../common/api';

export const FETCH_SUBSCRIPTIONS = 'FETCH_SUBSCRIPTIONS';
export const FETCH_SUBSCRIPTIONS_SUCCESS = 'FETCH_SUBSCRIPTIONS_SUCCESS';
export const FETCH_SUBSCRIPTIONS_FAILURE = 'FETCH_SUBSCRIPTIONS_FAILURE';

export const fetchV2Subscriptions = (userId) => async (dispatch) => {
  dispatch({ type: FETCH_SUBSCRIPTIONS });
  try {
    const { subscriptions, stripeCustomerId } = await api.fetchV2Subscriptions(userId);

    const v2subscriptions = subscriptions
      .filter((sub) => sub.active === true)
      .map((subscription) => ({
        ...subscription,
        // convert Unix epoch integer to Date object
        periodStart: new Date(subscription.start * 1000),
        periodEnd: new Date(subscription.end * 1000),
      }));

    dispatch({
      type: FETCH_SUBSCRIPTIONS_SUCCESS,
      v2subscriptions,
      v2googleInAppPurchases: v2subscriptions.filter(
        (subscription) => subscription.type === 'google',
      ),
      v2appleInAppPurchases: v2subscriptions.filter(
        (subscription) => subscription.type === 'apple',
      ),
      stripeCustomerId,
    });
  } catch (error) {
    dispatch({ type: FETCH_SUBSCRIPTIONS_FAILURE, error: 'Error fetching V2 subscriptions' });
  }
};
