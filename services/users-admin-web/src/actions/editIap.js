import { users } from '@surfline/admin-common';
import * as api from '../common/api';

export const REMOVE_IAP_SUBSCRIPTION = 'REMOVE_IAP_SUBSCRIPTION';
export const REMOVE_IAP_SUBSCRIPTION_SUCCESS = 'REMOVE_IAP_SUBSCRIPTION_SUCCESS';
export const REMOVE_IAP_SUBSCRIPTION_FAILURE = 'REMOVE_IAP_SUBSCRIPTION_FAILURE';

export const VERIFY_RECEIPT = 'VERIFY_RECEIPT';
export const VERIFY_RECEIPT_SUCCESS = 'VERIFY_RECEIPT_SUCCESS';
export const VERIFY_RECEIPT_FAILURE = 'VERIFY_RECEIPT_FAILURE';

export const RESTORE_PURCHASES = 'RESTORE_PURCHASES';
export const RESTORE_PURCHASES_SUCCESS = 'RESTORE_PURCHASES_SUCCESS';
export const RESTORE_PURCHASES_FAILURE = 'RESTORE_PURCHASES_FAILURE';

export const removeIapSubscription = (subscriptionId) => async (dispatch, getState) => {
  dispatch({ type: REMOVE_IAP_SUBSCRIPTION, subscriptionId });

  try {
    const userId = getState().edit.user.id;
    await users.deleteSubscription(userId, subscriptionId);
    dispatch({
      type: REMOVE_IAP_SUBSCRIPTION_SUCCESS,
      subscriptionId,
    });
  } catch (error) {
    dispatch({
      type: REMOVE_IAP_SUBSCRIPTION_FAILURE,
      subscriptionId,
      error: 'Unable to remove subscription',
    });
  }
};

export const verifyReceipt = (receiptNumber, receiptData, brand, deviceType) => async (
  dispatch,
  getState,
) => {
  dispatch({ type: VERIFY_RECEIPT, receiptNumber });

  try {
    const userId = getState().edit.user.id;
    const verifiedReceipt = await api.verifyReceipt(userId, receiptData, brand, deviceType);
    dispatch({
      type: VERIFY_RECEIPT_SUCCESS,
      receiptNumber,
      verifiedReceipt,
    });
  } catch (error) {
    dispatch({
      type: VERIFY_RECEIPT_FAILURE,
      error: 'Unable to verify receipt',
      receiptNumber,
    });
  }
};

export const restorePurchases = () => async (dispatch, getState) => {
  dispatch({ type: RESTORE_PURCHASES });

  try {
    const { user } = getState().edit;
    const userId = user.id;
    const { subscriptions, receipts } = await api.restorePurchases(userId);
    const updatedUser = {
      ...user,
      subscriptions,
      receipts,
      googleInAppPurchases: subscriptions.filter((subscription) => subscription.type === 'google'),
      appleInAppPurchases: subscriptions.filter((subscription) => subscription.type === 'apple'),
    };

    dispatch({
      type: RESTORE_PURCHASES_SUCCESS,
      user: updatedUser,
    });
  } catch (error) {
    dispatch({
      type: RESTORE_PURCHASES_FAILURE,
      error: 'Unable to restore purchases',
    });
  }
};
