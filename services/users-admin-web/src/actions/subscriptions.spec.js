import chai, { expect } from 'chai';
import sinon from 'sinon';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as api from '../common/api';
import {
  FETCH_SUBSCRIPTIONS,
  FETCH_SUBSCRIPTIONS_SUCCESS,
  FETCH_SUBSCRIPTIONS_FAILURE,
  fetchV2Subscriptions,
} from './subscriptions';

chai.use(dirtyChai);
chai.use(sinonChai);

describe('Actions / Subscriptions', () => {
  let mockStore;
  let fetchV2SubscriptionsStub;

  before(() => {
    mockStore = configureMockStore([thunk]);
  });

  beforeEach(() => {
    fetchV2SubscriptionsStub = sinon.stub(api, 'fetchV2Subscriptions');
  });

  afterEach(() => {
    fetchV2SubscriptionsStub.restore();
  });

  describe('fetchV2Subscriptions', () => {
    it('gets V2 subscriptions for a user', async () => {
      const store = mockStore({});

      const startEpoch = 0;
      const endEpoch = 1;
      const startDate = new Date(startEpoch * 1000);
      const endDate = new Date(endEpoch * 1000);

      expect(store.getActions()).to.be.empty();
      fetchV2SubscriptionsStub.resolves({
        subscriptions: [
          {
            type: 'apple',
            subscriptionId: '540000427013995',
            user: '5cf6f5ffeedf0d000f1dcfbd',
            active: true,
            entitlement: 'sl_premium',
            start: startEpoch,
            end: endEpoch,
          },
          {
            type: 'apple',
            subscriptionId: '540000427013996',
            user: '5cf6f5ffeedf0d000f1dcfbd',
            active: false,
            entitlement: 'sl_premium',
            start: startEpoch,
            end: endEpoch,
          },
        ],
        stripeCustomerId: 'cust_1234',
      });
      const expectedActions = [
        { type: FETCH_SUBSCRIPTIONS },
        {
          type: FETCH_SUBSCRIPTIONS_SUCCESS,
          v2subscriptions: [
            {
              type: 'apple',
              subscriptionId: '540000427013995',
              user: '5cf6f5ffeedf0d000f1dcfbd',
              active: true,
              entitlement: 'sl_premium',
              start: startEpoch,
              end: endEpoch,
              periodStart: startDate,
              periodEnd: endDate,
            },
          ],
          v2googleInAppPurchases: [],
          v2appleInAppPurchases: [
            {
              type: 'apple',
              subscriptionId: '540000427013995',
              user: '5cf6f5ffeedf0d000f1dcfbd',
              active: true,
              entitlement: 'sl_premium',
              start: startEpoch,
              end: endEpoch,
              periodStart: startDate,
              periodEnd: endDate,
            },
          ],
          stripeCustomerId: 'cust_1234',
        },
      ];

      await store.dispatch(fetchV2Subscriptions('1234'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(fetchV2SubscriptionsStub).to.have.been.calledOnce();
      expect(fetchV2SubscriptionsStub).to.have.been.calledWithExactly('1234');
    });

    it('errors on failure', async () => {
      const store = mockStore({});
      expect(store.getActions()).to.be.empty();
      fetchV2SubscriptionsStub.rejects('Error fetching V2 subscriptions');
      const expectedActions = [
        { type: FETCH_SUBSCRIPTIONS },
        { type: FETCH_SUBSCRIPTIONS_FAILURE, error: 'Error fetching V2 subscriptions' },
      ];

      await store.dispatch(fetchV2Subscriptions('1234'));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(fetchV2SubscriptionsStub).to.have.been.calledOnce();
      expect(fetchV2SubscriptionsStub).to.have.been.calledWithExactly('1234');
    });
  });
});
