import React from 'react';
import { render } from 'react-dom';
import { Route } from 'react-router';
import { Router } from 'react-router-dom';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { AppContainer } from '@surfline/admin-common';

import Users from './containers/Users';
import Edit from './containers/Edit';
import { withErrorBoundary } from './containers/ErrorBoundary';
import reducer from './reducer';
import history from './history';

import './styles/index.scss';

const enhancers = composeWithDevTools(applyMiddleware(thunk));
const store = createStore(reducer, enhancers);

const App = withErrorBoundary(() => (
  <AppContainer>
    <Route exact path="/users/" component={Users} />
    <Route exact path="/users/edit/:userId" component={Edit} />
  </AppContainer>
));

render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App} />
    </Router>
  </Provider>,
  document.getElementById('root'),
);
