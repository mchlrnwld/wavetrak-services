import PropTypes from 'prop-types';

export const meterPropTypes = PropTypes.shape({
  active: PropTypes.bool,
  meterLimit: PropTypes.number,
  meterRemaining: PropTypes.number,
  anonymousId: PropTypes.string,
  createdAt: PropTypes.string,
  updatedAt: PropTypes.string,
  countryCode: PropTypes.string,
  treatmentVariant: PropTypes.string,
  treatmentState: PropTypes.string,
});
