export default {
  // the 'notrial' suffix is a legacy hack to address a problem in the
  // first release of IAP in iOS. Don't reuse. These will be deprecated at a later date.
  'surfline.iosapp.1month.subscription.notrial': 'sl_premium',
  'surfline.iosapp.1year.subscription.notrial': 'sl_premium',
  // for some reason this 'notrial' suffix was grandfathered into the first release of IAP
  // in Android. Again, DO NOT reuse. These will be deprecated at a later date as well.
  'surfline.android.1month.subscription.notrial': 'sl_premium',
  'surfline.android.1year.subscription.notrial': 'sl_premium',
  // auto renewing subscriptions
  'surfline.iosapp.1month.subscription': 'sl_premium',
  'surfline.iosapp.1year.subscription': 'sl_premium',
  'buoyweather.iosapp.1month.subscription': 'bw_premium',
  'buoyweather.iosapp.1year.subscription': 'bw_premium',
  'buoyweather.android.1month.subscription': 'bw_premium',
  'buoyweather.android.1year.subscription': 'bw_premium',
  'fishtrack.iosapp.1month.subscription': 'fs_premium',
  'fishtrack.iosapp.1year.subscription': 'fs_premium',
  'fs.iosapp.1month.subscription': 'fs_premium',
  'fs.iosapp.1year.subscription': 'fs_premium',
  'fishtrack.android.1month.subscription': 'fs_premium',
  'fishtrack.android.1year.subscription': 'fs_premium',
  // legacy non-renewing subscriptions for backward compatibility
  FS30APP: 'fs_premium',
  FS365APP: 'fs_premium',
  BW30APP: 'bw_premium',
  BW365APP: 'bw_premium',
};
