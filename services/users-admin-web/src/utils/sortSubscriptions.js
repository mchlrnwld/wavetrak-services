// Sort the subscriptions by Date (desc) then by Type
export default (subscriptions) => {
  const dedupedSubs = subscriptions.filter(
    (sub, index, self) =>
      index === self.findIndex((currentSub) => currentSub.subscriptionId === sub.subscriptionId),
  );
  return dedupedSubs.sort((sub1, sub2) => new Date(sub2.periodStart) - new Date(sub1.periodStart));
};
