import fetch from 'isomorphic-fetch';
import { fetchWithAuth } from '@surfline/admin-common';
import config from '../config';

export const clearUserTokens = async (userId) => {
  const response = await fetch(`${config.servicesUrl}/auth/invalidate-token`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify({ id: userId }),
  });
  return response;
};

export const fetchUserDetails = (userId) => fetchWithAuth(`entitlements/${userId}/entitlements`);

export const findUserDetails = (userId) => fetchWithAuth(`user/?userId=${userId}`);

export const fetchV2Subscriptions = (userId) =>
  fetchWithAuth(`subscriptions/wavetrak/?userId=${userId}`);

export const createUserAccount = (userId) =>
  fetchWithAuth('subscriptions/', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify({ userId }),
  });

export const deleteSubscription = (userId, subscriptionId) =>
  fetchWithAuth(`subscriptions/user/${userId}/subscription/${subscriptionId}`, {
    method: 'DELETE',
    headers: {
      accept: 'application/json',
      credentials: 'same-origin',
    },
  });

export const verifyReceipt = (userId, receiptData, brand, deviceType) =>
  fetchWithAuth('subscriptions/iap/verify-receipt', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify({
      userId,
      receiptData,
      brand,
      deviceType,
    }),
  });

export const restorePurchases = (userId) =>
  fetchWithAuth(`subscriptions/iap/update-receipts/${userId}`, {
    method: 'PUT',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
  });

export const findUsers = (query) => fetchWithAuth(`users/?search=${query}`);

export const validateEmail = async (email) => {
  const response = await fetch(`${config.servicesUrl}/user/validateemail?email=${email}`, {
    method: 'GET',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
  });
  return response;
};

export const updateUserInfo = (_id, email, firstName, lastName) => {
  const body = {};

  // Only add the fields to the body if they aren't falsy
  if (email) body.email = email;
  if (firstName) body.firstName = firstName;
  if (lastName) body.lastName = lastName;

  return fetchWithAuth(`users/${_id}`, {
    method: 'PUT',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify(body),
  });
};

export const passwordReset = (userId, brand) => {
  const response = fetchWithAuth('password-reset/', {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify({ userId, brand }),
  });

  return response;
};

export const deleteUser = async (userId) =>
  fetchWithAuth(`users/${userId}`, {
    method: 'DELETE',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
  });

export const getCustomerInfo = (customerId) => fetchWithAuth(`customers/?${customerId}`);

export const dissociateCustomer = (customerId) =>
  fetchWithAuth('customers/', {
    method: 'DELETE',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify({ customerId }),
  });

/**
 * @typedef {object} Meter
 * @property {string} _id
 * @property {"sl_metered"} entitlement
 * @property {boolean} active
 * @property {string} anonymousId
 * @property {string} countryCode
 * @property {string} user
 * @property {string} treatmentName
 * @property {string} treatmentVariant
 * @property {string} treatmentState
 * @property {number} meterLimit
 * @property {number} meterRemaining
 * @property {string} createdAt
 * @property {string} updatedAt
 */

/**
 * @param {string} userId
 * @returns {Promise<Meter>}
 */
export const getMeter = async (userId) => {
  try {
    return await fetchWithAuth('meter/', {
      method: 'GET',
      headers: {
        'x-auth-userid': userId,
        accept: 'application/json',
        'content-type': 'application/json',
        credentials: 'same-origin',
      },
    });
  } catch (error) {
    if (error.status === 404) {
      return null;
    }
    throw error;
  }
};

/**
 * @param {string} meterId
 * @param {{ meterRemaining?: number }} body
 * @returns {Promise<{ success: boolean, message: string }>}
 */
export const patchMeter = (meterId, body) =>
  fetchWithAuth(`/meter/admin/${meterId}`, {
    method: 'PATCH',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'include',
    },
    body: JSON.stringify(body),
  });

export const verifyUser = async (userId, email, brand) =>
  fetchWithAuth(`user/verify-email/${userId}`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify({
      email,
      brand,
    }),
  });

export const getSubscriptionUsers = (userId) => fetchWithAuth(`subscription-users/${userId}`);

export const patchSubscriptionUsers = (body) =>
  fetchWithAuth(`subscription-users/`, {
    method: 'PATCH',
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      credentials: 'same-origin',
    },
    body: JSON.stringify(body),
  });
