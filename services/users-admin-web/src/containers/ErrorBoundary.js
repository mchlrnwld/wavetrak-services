/* eslint-disable react/no-multi-comp */
/* eslint-disable max-classes-per-file */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

const catchFunc = (error, errorInfo, ctx) => {
  // catch errors in any components below and re-render with error message
  ctx.setState({
    error,
    errorInfo,
  });

  /* eslint-disable-next-line no-console */
  console.log({ error, errorInfo });
};

const handleError = (ctx, componentName = null) => (
  // Error path
  <div className="section error-boundary">
    <h5 className="error-boundary__header">
      Something went wrong{componentName ? ` with the ${componentName} component.` : '.'}
    </h5>
  </div>
);
export class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    // eslint-disable-next-line react/no-unused-state
    this.state = { error: null, errorInfo: null };
  }

  componentDidCatch = (error, errorInfo) => catchFunc(error, errorInfo, this);

  render() {
    const { errorInfo } = this.state;
    const { children } = this.props;
    if (errorInfo) {
      return handleError(this);
    }
    return children;
  }
}

export const withErrorBoundary = (WrappedComponent, componentName) =>
  class extends Component {
    constructor(props) {
      super(props);
      // eslint-disable-next-line react/no-unused-state
      this.state = { error: null, errorInfo: null };
    }

    componentDidCatch = (error, errorInfo) => catchFunc(error, errorInfo, this);

    render() {
      const { errorInfo } = this.state;
      if (errorInfo) {
        return handleError(this, componentName);
      }
      return <WrappedComponent {...this.props} />;
    }
  };

ErrorBoundary.propTypes = {
  children: PropTypes.node,
};

ErrorBoundary.defaultProps = {
  children: null,
};
