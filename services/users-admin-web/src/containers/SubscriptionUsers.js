import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fetchSubscriptionUsers, updatePrimaryUser } from '../actions/subscriptionUsers';
import UpdatePrimaryUser from '../components/UpdatePrimaryUser';
import SubscriptionUsersTable from '../components/SubscriptionUsersTable';

const SubscriptionUsers = ({
  userId,
  doFetchSubscriptionUsers,
  doUpdatePrimaryUser,
  subscriptionUsers,
  updatePrimaryUserError,
}) => {
  useEffect(() => {
    doFetchSubscriptionUsers(userId);
  }, [userId, doFetchSubscriptionUsers]);

  return subscriptionUsers ? (
    <div className="sl-admin-subscription-users">
      <h5 className="header__label">Linked Accounts</h5>
      <p className="sub-table-title">Surfline Subscriptions</p>
      <SubscriptionUsersTable subscriptionUsers={subscriptionUsers} />
      <UpdatePrimaryUser
        subscriptionUsers={subscriptionUsers}
        updatePrimaryUser={doUpdatePrimaryUser}
        fetchSubscriptionUsers={doFetchSubscriptionUsers}
        updatePrimaryUserError={updatePrimaryUserError}
      />
    </div>
  ) : null;
};

SubscriptionUsers.propTypes = {
  userId: PropTypes.string.isRequired,
  doFetchSubscriptionUsers: PropTypes.func.isRequired,
  doUpdatePrimaryUser: PropTypes.func.isRequired,
  subscriptionUsers: PropTypes.arrayOf(
    PropTypes.shape({
      user: PropTypes.shape({
        _id: PropTypes.string,
        email: PropTypes.string,
      }),
      active: PropTypes.bool,
      type: PropTypes.string,
      isPrimaryUser: PropTypes.bool,
      subscriptionId: PropTypes.string,
    }),
  ).isRequired,
  updatePrimaryUserError: PropTypes.shape({}),
};

SubscriptionUsers.defaultProps = {
  updatePrimaryUserError: null,
};

export default connect(
  (state) => ({
    ...state.subscriptionUsers,
    updatePrimaryUserError: state.subscriptionUsers.error,
  }),
  (dispatch) => ({
    doFetchSubscriptionUsers: (userId) => dispatch(fetchSubscriptionUsers(userId)),
    doUpdatePrimaryUser: (oldPrimaryUser, newPrimaryUser) =>
      dispatch(updatePrimaryUser(oldPrimaryUser, newPrimaryUser)),
  }),
)(SubscriptionUsers);
