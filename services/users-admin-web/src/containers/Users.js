import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';

import { searchUsers } from '../actions/search';
import SearchInput from '../components/SearchInput';
import SearchResults from '../components/SearchResults';

const Users = ({ doSearch, results, loading, searched }) => {
  const location = useLocation();
  const params = new URLSearchParams(location.search);

  const query = {
    q: params.get('q'),
    version: params.get('version'),
    link: params.get('link'),
  };

  let link = '/users/edit/';
  if (query.q && query.link) {
    link = query.link;
  }

  useEffect(() => {
    let initialLink;
    if (query.q && query.link) {
      initialLink = query.link;
    }

    if (query.q) {
      doSearch(query.q, { link: initialLink });
    }
  }, [doSearch, query.q, query.link]);

  const transform = (resultArr, urlPath) =>
    resultArr.map((result) => ({
      firstName: result.firstName,
      lastName: result.lastName,
      email: result.email,
      link: `${urlPath}${result._id}`,
      key: result._id,
    }));

  return (
    <div className="user-search__container">
      <h4>Users</h4>
      <div className="search__container">
        <SearchInput
          onSubmit={(queryString) => doSearch(queryString)}
          defaultValue={query.q || ''}
          message="Use email"
          loading={loading}
        />
        {searched && results.length ? (
          <SearchResults results={transform(results, link) || []} />
        ) : null}
      </div>
      {searched && !results.length ? <div>No Results Found</div> : null}
    </div>
  );
};

Users.propTypes = {
  doSearch: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  searched: PropTypes.bool.isRequired,
  results: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      firstName: PropTypes.number,
      lastName: PropTypes.string,
      email: PropTypes.string,
    }),
  ),
};

Users.defaultProps = {
  results: [],
};

export default connect(
  (state) => ({ ...state.search }),
  (dispatch) => ({
    doSearch: (query, opts) => {
      dispatch(searchUsers(query, opts));
    },
  }),
)(Users);
