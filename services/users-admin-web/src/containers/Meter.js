import React from 'react';
import PropTypes from 'prop-types';
import useSWR from 'swr';

import { getMeter } from '../common/api';
import MeterInformation from '../components/Meter';
import UpdateMeter from '../components/UpdateMeter';
import SurfCheckTable from '../components/SurfCheckTable';

/**
 * @param {object} props
 * @param {string} props.userId
 * @returns {JSX.Element}
 */
const Meter = ({ userId }) => {
  const { data, isLoading, error } = useSWR(userId, getMeter, { errorRetryCount: 3 });

  return (
    <section className="section sl-admin-meter">
      <h5 className="sl-admin-meter__header">Rate Limiting</h5>
      <MeterInformation meter={data} isLoading={isLoading} error={error} />
      {data && (
        <div className="sl-admin-surfcheck">
          <SurfCheckTable meter={data} />
          <UpdateMeter meter={data} />
        </div>
      )}
    </section>
  );
};

Meter.propTypes = {
  userId: PropTypes.string.isRequired,
};

export default Meter;
