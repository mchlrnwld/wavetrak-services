import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Alert } from '@surfline/quiver-react';
import config from '../config';
import Header from '../components/Edit/Header';
import SubscriptionsTable from '../components/Edit/SubscriptionsTable';
import InAppPurchases from '../components/Edit/InAppPurchases';
import Receipts from '../components/Edit/Receipts';
import Actions from '../components/Edit/Actions';
import SubscriptionUsers from './SubscriptionUsers';
import Meter from './Meter';
import {
  fetchUserDetails,
  clearUserTokens,
  updateUserInfo,
  passwordReset,
  copyPassword,
  processGDPRRequest,
  dissociateCustomer,
  verifyEmail,
} from '../actions/edit';
import { removeIapSubscription, verifyReceipt, restorePurchases } from '../actions/editIap';
import { fetchV2Subscriptions } from '../actions/subscriptions';
import sortSubscriptions from '../utils/sortSubscriptions';

const EditUser = ({
  loading,
  error,
  user,
  clearTokens,
  doProcessGDPRRequest,
  updateUserError,
  updateSuccess,
  verifiedReceipts,
  clearing,
  updating,
  processing,
  restoring,
  restoreSuccess,
  gdprSuccessMessage,
  restoreError,
  message,
  doResetPassword,
  doCopyPassword,
  resetting,
  password,
  copied,
  doDissociateCustomer,
  dissociating,
  doUpdateUserInfo,
  doFetchUserDetails,
  doRemoveIapSubscription,
  doVerifyReceipt,
  doRestorePurchases,
  match,
  doVerifyEmail,
  verifyingEmail,
  emailVerificationError,
  doFetchV2Subscriptions,
  v2subscriptions,
  v2googleInAppPurchases,
  v2appleInAppPurchases,
  stripeCustomerId,
}) => {
  const { userId } = match.params;

  useEffect(() => {
    doFetchUserDetails(userId);
    doFetchV2Subscriptions(userId);
  }, [userId, doFetchUserDetails, doFetchV2Subscriptions]);

  const userLoaded = !loading && user;
  const customerId = stripeCustomerId || user?.stripeCustomerId;
  const stripeDashboardLink = customerId
    ? `${config.stripeCustomerDashboardUrl}/${customerId}`
    : '';

  const subscriptions = userLoaded
    ? sortSubscriptions(user.subscriptions.concat(v2subscriptions))
    : [];

  const googleInAppPurchases = userLoaded
    ? user.googleInAppPurchases.concat(v2googleInAppPurchases)
    : [];
  const appleInAppPurchases = userLoaded
    ? user.appleInAppPurchases.concat(v2appleInAppPurchases)
    : [];

  return (
    <div className="edit-page">
      <Header
        user={user}
        stripeDashboardLink={stripeDashboardLink}
        loading={loading}
        updateUserInfo={doUpdateUserInfo}
        updateUserError={updateUserError}
        updating={updating}
        updateSuccess={updateSuccess}
        verifyEmail={doVerifyEmail}
        verifyingEmail={verifyingEmail}
        emailVerificationError={emailVerificationError}
      />
      {error && <Alert type="error">{error}</Alert>}
      {gdprSuccessMessage && <Alert type="info">{gdprSuccessMessage}</Alert>}
      {userLoaded && (
        <div className="section user-info">
          <Actions
            user={user}
            clearTokens={clearTokens}
            clearing={clearing}
            message={message}
            doResetPassword={doResetPassword}
            doCopyPassword={doCopyPassword}
            password={password}
            resetting={resetting}
            copied={copied}
            doProcessGDPRRequest={doProcessGDPRRequest}
            processing={processing}
            subscriptions={subscriptions}
            doDissociateCustomer={doDissociateCustomer}
            dissociating={dissociating}
          />
          <SubscriptionsTable label="Current Subscriptions" subscriptionsData={subscriptions} />
          <InAppPurchases
            iapSubscriptions={googleInAppPurchases}
            type="google"
            removeIapSubscription={doRemoveIapSubscription}
          />
          <InAppPurchases
            iapSubscriptions={appleInAppPurchases}
            type="apple"
            removeIapSubscription={doRemoveIapSubscription}
          />
          <Receipts
            receipts={user.receipts}
            verifiedReceipts={verifiedReceipts}
            verifyReceipt={doVerifyReceipt}
            restorePurchases={doRestorePurchases}
            restoreSuccess={restoreSuccess}
            restoreError={restoreError}
            restoring={restoring}
          />
          <SubscriptionsTable
            label="Archived Subscriptions"
            subscriptionsData={user.archivedSubscriptions}
          />
          <Meter userId={userId} />
          <SubscriptionUsers userId={userId} />
        </div>
      )}
    </div>
  );
};

EditUser.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.string,
  user: PropTypes.shape(),
  doFetchUserDetails: PropTypes.func.isRequired,
  clearTokens: PropTypes.func.isRequired,
  doVerifyReceipt: PropTypes.func.isRequired,
  doRestorePurchases: PropTypes.func.isRequired,
  restoring: PropTypes.bool,
  restoreSuccess: PropTypes.bool,
  restoreError: PropTypes.string,
  verifiedReceipts: PropTypes.shape(),
  doUpdateUserInfo: PropTypes.func.isRequired,
  updateUserError: PropTypes.string,
  gdprSuccessMessage: PropTypes.string,
  updateSuccess: PropTypes.bool,
  clearing: PropTypes.bool,
  updating: PropTypes.bool,
  processing: PropTypes.bool,
  doRemoveIapSubscription: PropTypes.func.isRequired,
  message: PropTypes.string,
  match: PropTypes.shape({
    params: PropTypes.shape({
      userId: PropTypes.string,
    }).isRequired,
  }).isRequired,
  doResetPassword: PropTypes.func.isRequired,
  doCopyPassword: PropTypes.func.isRequired,
  resetting: PropTypes.bool,
  password: PropTypes.string,
  copied: PropTypes.bool,
  doProcessGDPRRequest: PropTypes.func.isRequired,
  doDissociateCustomer: PropTypes.func.isRequired,
  dissociating: PropTypes.bool,
  doVerifyEmail: PropTypes.func.isRequired,
  verifyingEmail: PropTypes.bool,
  emailVerificationError: PropTypes.bool,
  doFetchV2Subscriptions: PropTypes.func.isRequired,
  v2subscriptions: PropTypes.arrayOf(PropTypes.object),
  v2googleInAppPurchases: PropTypes.arrayOf(PropTypes.object),
  v2appleInAppPurchases: PropTypes.arrayOf(PropTypes.object),
  stripeCustomerId: PropTypes.string,
};

EditUser.defaultProps = {
  user: null,
  loading: false,
  error: null,
  restoring: null,
  restoreSuccess: null,
  restoreError: null,
  verifiedReceipts: null,
  updateUserError: '',
  gdprSuccessMessage: '',
  updateSuccess: null,
  clearing: null,
  updating: null,
  processing: null,
  message: '',
  resetting: null,
  password: null,
  copied: null,
  dissociating: null,
  verifyingEmail: null,
  emailVerificationError: null,
  v2subscriptions: [],
  v2googleInAppPurchases: [],
  v2appleInAppPurchases: [],
  stripeCustomerId: null,
};

export default connect(
  (state) => ({
    ...state.edit,
    ...state.subscriptions,
    loading: state.edit.loading || state.subscriptions.loading,
    error: state.edit.error || state.subscriptions.error,
  }),
  (dispatch) => ({
    doFetchUserDetails: (userId) => dispatch(fetchUserDetails(userId)),
    doRemoveIapSubscription: (subscriptionId) => dispatch(removeIapSubscription(subscriptionId)),
    clearTokens: (userId) => dispatch(clearUserTokens(userId)),
    doProcessGDPRRequest: (userId) => dispatch(processGDPRRequest(userId)),
    doVerifyReceipt: (receiptNumber, receiptData, brand, deviceType) =>
      dispatch(verifyReceipt(receiptNumber, receiptData, brand, deviceType)),
    doRestorePurchases: () => dispatch(restorePurchases()),
    doUpdateUserInfo: (email, firstName, lastName) =>
      dispatch(updateUserInfo(email, firstName, lastName)),
    doResetPassword: (userId, brand) => dispatch(passwordReset(userId, brand)),
    doCopyPassword: () => dispatch(copyPassword()),
    doDissociateCustomer: (userId, customerId) => dispatch(dissociateCustomer(userId, customerId)),
    doVerifyEmail: (userId, email, brand) => dispatch(verifyEmail(userId, email, brand)),
    doFetchV2Subscriptions: (userId) => dispatch(fetchV2Subscriptions(userId)),
  }),
)(EditUser);
