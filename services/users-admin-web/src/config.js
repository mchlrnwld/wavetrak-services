export default {
  development: {
    servicesUrl: 'https://services.sandbox.surfline.com',
    stripeCustomerDashboardUrl: 'https://dashboard.stripe.com/test/customers',
    stripeSubscriptionDashboardUrl: 'https://dashboard.stripe.com/test/subscriptions',
  },
  sandbox: {
    servicesUrl: 'https://services.sandbox.surfline.com',
    stripeCustomerDashboardUrl: 'https://dashboard.stripe.com/test/customers',
    stripeSubscriptionDashboardUrl: 'https://dashboard.stripe.com/test/subscriptions',
  },
  staging: {
    servicesUrl: 'https://services.staging.surfline.com',
    stripeCustomerDashboardUrl: 'https://dashboard.stripe.com/test/customers',
    stripeSubscriptionDashboardUrl: 'https://dashboard.stripe.com/test/subscriptions',
  },
  production: {
    servicesUrl: 'https://services.surfline.com',
    stripeCustomerDashboardUrl: 'https://dashboard.stripe.com/customers',
    stripeSubscriptionDashboardUrl: 'https://dashboard.stripe.com/subscriptions',
  },
}[process.env.NODE_ENV || 'development'];
