import React from 'react';
import PropTypes from 'prop-types';
import { format } from 'date-fns';
import { ErrorBox, Spinner } from '@surfline/quiver-react';
import { meterPropTypes } from '../propTypes/meter';

const Meter = ({ meter, isLoading, error }) => {
  if (isLoading) {
    return <Spinner />;
  }

  if (error) {
    return <ErrorBox type="meter" />;
  }

  if (!meter) {
    return <p className="sl-admin-meter__no-meter">User does not have a meter</p>;
  }

  return (
    <table>
      <thead>
        <tr>
          <td>Last Reset:</td>
          <td>Anonymous ID:</td>
          <td>Checks Remaining:</td>
          <td>Time Zone:</td>
          <td>Country Code:</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{meter.latestReset ? format(new Date(meter.latestReset), 'MM/DD/YY H:mm') : '-'}</td>
          <td>{meter.anonymousId}</td>
          <td>{`${meter.meterRemaining} out of ${meter.meterLimit}`}</td>
          <td>{`${meter.timezone}`}</td>
          <td>{meter.countryCode}</td>
        </tr>
      </tbody>
    </table>
  );
};

Meter.propTypes = {
  meter: meterPropTypes,
  isLoading: PropTypes.bool,
  error: PropTypes.string,
};

Meter.defaultProps = {
  meter: null,
  isLoading: false,
  error: null,
};

export default Meter;
