import React from 'react';
import PropTypes from 'prop-types';
import { color } from '@surfline/quiver-themes';
import { Table, TableBlock, TableRow } from './Table';

const SubscriptionUsersTable = ({ subscriptionUsers }) => {
  return (
    <Table backgroundColor={color('light-blue', 10)}>
      <TableRow backgroundColor={color('light-blue', 30)}>
        <TableBlock isHeader text="User ID" />
        <TableBlock isHeader text="Email" />
        <TableBlock isHeader text="Sub ID" />
        <TableBlock isHeader text="Primary User" />
        <TableBlock isHeader text="Active" />
        <TableBlock isHeader text="Type" />
      </TableRow>
      {subscriptionUsers.map((subscriptionUser, index) => {
        const { user, active, type, isPrimaryUser, subscriptionId } = subscriptionUser;
        return (
          <TableRow key={subscriptionUser._id} backgroundColor={index % 2 === 0 ? '#FFFFFF' : null}>
            <TableBlock text={user?._id} />
            <TableBlock text={user?.email} />
            <TableBlock text={subscriptionId} />
            <TableBlock text={isPrimaryUser ? 'X' : null} />
            <TableBlock text={active.toString()} />
            <TableBlock text={type} />
          </TableRow>
        );
      })}
    </Table>
  );
};

SubscriptionUsersTable.propTypes = {
  subscriptionUsers: PropTypes.arrayOf(
    PropTypes.shape({
      user: PropTypes.shape({
        _id: PropTypes.string,
        email: PropTypes.string,
      }),
      active: PropTypes.bool,
      type: PropTypes.string,
      isPrimaryUser: PropTypes.bool,
      subscriptionId: PropTypes.string,
    }),
  ).isRequired,
};

export default SubscriptionUsersTable;
