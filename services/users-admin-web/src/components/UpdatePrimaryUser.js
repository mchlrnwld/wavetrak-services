import React, { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import { Input, Button, ErrorBox } from '@surfline/quiver-react';

const UpdatePrimaryUser = ({
  subscriptionUsers,
  updatePrimaryUser,
  fetchSubscriptionUsers,
  updatePrimaryUserError,
}) => {
  const { register, errors, handleSubmit, reset, formState, setError, watch } = useForm({
    mode: 'all',
  });

  const watchAllFields = watch();

  const handleUpdatePrimaryUser = async (data) => {
    try {
      const oldPrimaryUser = subscriptionUsers
        ? subscriptionUsers.filter((subUser) => subUser.isPrimaryUser === true)[0]?.user?._id
        : null;
      const newPrimaryUser = data?.userId;
      await updatePrimaryUser(oldPrimaryUser, newPrimaryUser);
      await fetchSubscriptionUsers(oldPrimaryUser);
      reset({}, { isSubmitted: true });
    } catch (error) {
      reset(data, { isSubmitted: true });
      setError('submitError');
    }
  };

  useEffect(() => {
    if ((formState.isSubmitted || formState.isSubmitSuccessful) && formState.isDirty) {
      reset(watchAllFields);
    }
  }, [reset, watchAllFields, formState]);

  const validateUserId = (userId) =>
    subscriptionUsers?.some((subUser) => subUser?.user?._id === userId) ||
    'User not linked to current subscription';

  return (
    <div className="sl-admin-subscription-users__form">
      <form onSubmit={handleSubmit(handleUpdatePrimaryUser)}>
        <Input
          controlled
          type="text"
          input={{ name: 'userId' }}
          error={!!errors?.userId?.message}
          message={errors?.userId?.message}
          label="Update primary user"
          ref={register({
            required: 'User ID is required.',
            validate: validateUserId,
          })}
        />
        {updatePrimaryUserError && <ErrorBox message={updatePrimaryUserError} />}
        <Button
          style={{ width: '100%' }}
          loading={formState.isSubmitting}
          success={formState.isSubmitSuccessful && !updatePrimaryUserError}
          button={{ type: 'submit' }}
        >
          UPDATE PRIMARY USER
        </Button>
      </form>
    </div>
  );
};

UpdatePrimaryUser.propTypes = {
  fetchSubscriptionUsers: PropTypes.func.isRequired,
  updatePrimaryUser: PropTypes.func.isRequired,
  updatePrimaryUserError: PropTypes.shape({}),
  subscriptionUsers: PropTypes.arrayOf(
    PropTypes.shape({
      user: PropTypes.shape({
        _id: PropTypes.string,
        email: PropTypes.string,
      }),
      active: PropTypes.bool,
      type: PropTypes.string,
      isPrimaryUser: PropTypes.bool,
      subscriptionId: PropTypes.string,
    }),
  ).isRequired,
};

UpdatePrimaryUser.defaultProps = {
  updatePrimaryUserError: null,
};

export default UpdatePrimaryUser;
