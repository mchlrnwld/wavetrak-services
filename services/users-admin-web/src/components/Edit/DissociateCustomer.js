import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@surfline/quiver-react';

const DissociateStripeCustomer = ({
  doDissociateCustomer,
  customerId,
  userId,
  subscriptions,
  dissociating,
  message,
}) => {
  const confirmDissoication = () => {
    // eslint-disable-next-line no-alert
    const result = window.confirm('This action cannot be undone.  Would you like to proceed?');
    if (result) {
      return doDissociateCustomer(userId, customerId);
    }
    return null;
  };
  return (
    <div className="action__button__container">
      {subscriptions.length || !customerId ? (
        <button type="button" disabled className="quiver-button">
          Dissociate Stripe Customer
        </button>
      ) : (
        <Button loading={dissociating} onClick={() => confirmDissoication()}>
          Dissociate Stripe Customer
        </Button>
      )}
      {message && <h6 className="user-action__message">{message}</h6>}
    </div>
  );
};

DissociateStripeCustomer.propTypes = {
  doDissociateCustomer: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  customerId: PropTypes.string,
  subscriptions: PropTypes.arrayOf(PropTypes.shape({})),
  dissociating: PropTypes.bool,
  message: PropTypes.string,
};

DissociateStripeCustomer.defaultProps = {
  subscriptions: [],
  dissociating: false,
  message: null,
  customerId: null,
};

export default DissociateStripeCustomer;
