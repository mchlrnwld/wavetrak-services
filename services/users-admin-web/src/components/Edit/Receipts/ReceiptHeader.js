import React from 'react';
import PropTypes from 'prop-types';

const ReceiptHeader = ({ label, text }) => (
  <div className="receipt__header">
    <h6 className="receipt__header-label">{label}</h6>
    <p className="receipt__header-type">{text}</p>
  </div>
);

ReceiptHeader.propTypes = {
  label: PropTypes.string,
  text: PropTypes.string,
};

ReceiptHeader.defaultProps = {
  label: '',
  text: '',
};

export default ReceiptHeader;
