import React from 'react';
import PropTypes from 'prop-types';

import { Button } from '@surfline/quiver-react';
import ReceiptInfo from './ReceiptInfo';
import subscriptionMap from '../../../utils/subscriptionMap';

const Receipt = ({ receipt, receiptNumber, verifyReceipt, verifiedReceipt }) => {
  let { purchaseData } = receipt;
  if (!purchaseData) purchaseData = receipt.purchases;

  // Get the 3 latest purchases and reverse for the most recent one to be first
  const last3Purchases = purchaseData.slice(-3).reverse();

  // Set different states for renders
  const isVerifying = verifiedReceipt && verifiedReceipt.verifying;
  const isVerified = verifiedReceipt && verifiedReceipt.verificationSuccess;
  const verificationFailed = verifiedReceipt && verifiedReceipt.verificationSuccess === false;

  // Get the 3 latest verified purchases if the data has been pulled
  let last3VerifiedPurchases;
  if (isVerified) {
    last3VerifiedPurchases = verifiedReceipt.receipt.purchases.slice(-3).reverse();
  }

  // Brand is only needed for google
  let brand = null;
  const productId = subscriptionMap[last3Purchases[0].productId];
  if (productId) {
    if (productId.indexOf('sl') > -1) {
      brand = 'sl';
    } else if (productId.indexOf('bw') > -1) {
      brand = 'bw';
    } else if (productId.indexOf('fs') > -1) {
      brand = 'fs';
    }
  }

  const getVerifiedReceipt = () =>
    verifyReceipt(receiptNumber, receipt.receiptData, brand, receipt.type);

  const receiptInfoProps = {
    last3Purchases,
    type: receipt.type,
    secondHeader: { label: 'Last Updated', value: receipt.lastUpdated },
  };
  const btnProps = {
    loading: isVerifying,
    onClick: getVerifiedReceipt,
  };

  const showError = verificationFailed && !isVerifying;

  return (
    <div className="receipt-section">
      <ReceiptInfo {...receiptInfoProps} />
      <div className="receipt__validation">
        {isVerified ? (
          <ReceiptInfo
            type={verifiedReceipt.receipt.type}
            secondHeader={{ label: 'Created Date', value: verifiedReceipt.receipt.creationDate }}
            last3Purchases={last3VerifiedPurchases}
          />
        ) : (
          <div className="receipt__validation-btn">
            <Button {...btnProps}>Verify Receipt</Button>
            {showError && (
              <h6 className="receipt__validation-err-msg">{verifiedReceipt.verificationError}</h6>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

Receipt.propTypes = {
  receipt: PropTypes.shape({
    receiptData: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
    lastUpdated: PropTypes.string,
    purchaseData: PropTypes.arrayOf(PropTypes.object),
    purchases: PropTypes.arrayOf(PropTypes.object),
    type: PropTypes.string,
  }).isRequired,
  verifiedReceipt: PropTypes.shape({
    receipt: PropTypes.shape({
      type: PropTypes.string,
      creationDate: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      purchases: PropTypes.arrayOf(PropTypes.object),
    }),
    verificationError: PropTypes.string,
    verifying: PropTypes.bool,
    verificationSuccess: PropTypes.bool,
  }),
  verifyReceipt: PropTypes.func.isRequired,
  receiptNumber: PropTypes.number.isRequired,
};

Receipt.defaultProps = {
  verifiedReceipt: {},
};

export default Receipt;
