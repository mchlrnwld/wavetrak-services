/* eslint-disable react/no-array-index-key */

import React from 'react';
import PropTypes from 'prop-types';

import { Button } from '@surfline/quiver-react';
import Receipt from './Receipt';
import { withErrorBoundary } from '../../../containers/ErrorBoundary';

const Receipts = ({
  receipts,
  verifyReceipt,
  verifiedReceipts,
  restorePurchases,
  restoreSuccess,
  restoring,
  restoreError,
}) => {
  const btnProps = {
    loading: restoring,
    onClick: restorePurchases,
    success: restoreSuccess,
  };
  const restoreFailed = !restoring && restoreSuccess === false;
  const hasReceipts = receipts.length > 0;

  return (
    <div className="section receipts">
      <div className="receipts__header">
        <h5 className="header__label">Receipts</h5>
        {hasReceipts && <Button {...btnProps}>Restore Purchases</Button>}
      </div>
      {restoreFailed && (
        <h6 className="receipts__error">{restoreError || 'Could not restore receipts'}</h6>
      )}
      {hasReceipts ? (
        receipts.map((receipt, receiptNumber) => (
          <Receipt
            receiptNumber={receiptNumber}
            verifiedReceipt={verifiedReceipts[`receipt${receiptNumber}`]}
            key={`receipt${receiptNumber}`}
            receipt={receipt}
            verifyReceipt={verifyReceipt}
          />
        ))
      ) : (
        <p className="receipts__notfound">No receipts found</p>
      )}
    </div>
  );
};

Receipts.propTypes = {
  receipts: PropTypes.arrayOf(PropTypes.object),
  verifyReceipt: PropTypes.func.isRequired,
  restorePurchases: PropTypes.func.isRequired,
  verifiedReceipts: PropTypes.shape({
    receipt1: PropTypes.shape({}),
    receipt2: PropTypes.shape({}),
  }),
  restoring: PropTypes.bool,
  restoreSuccess: PropTypes.bool,
  restoreError: PropTypes.string,
};

Receipts.defaultProps = {
  receipts: [],
  verifiedReceipts: {},
  restoring: null,
  restoreSuccess: null,
  restoreError: null,
};

export default withErrorBoundary(Receipts, 'Receipts');
