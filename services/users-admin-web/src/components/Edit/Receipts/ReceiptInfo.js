import React from 'react';
import PropTypes from 'prop-types';

import { format } from 'date-fns';
import Purchase from '../Purchase';
import ReceiptHeader from './ReceiptHeader';

const ReceiptInfo = ({ type, secondHeader, last3Purchases }) => (
  <div className="receipt">
    <div className="receipt__headers">
      <ReceiptHeader label="Receipt Type:" text={type} />
      <ReceiptHeader
        label={`${secondHeader.label}:`}
        text={format(secondHeader.value, 'M/D/YY H:mm')}
      />
    </div>
    <div className="receipt__purchases">
      {last3Purchases.map((purchase) => (
        <Purchase key={purchase.transactionId || purchase.transaction_id} purchase={purchase} />
      ))}
    </div>
  </div>
);

ReceiptInfo.propTypes = {
  type: PropTypes.string.isRequired,
  last3Purchases: PropTypes.arrayOf(PropTypes.object).isRequired,
  secondHeader: PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  }).isRequired,
};

export default ReceiptInfo;
