import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@surfline/quiver-react';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import classnames from 'classnames';

const ResetPassword = ({ user, resetting, doResetPassword, password, copied, doCopyPassword }) => {
  const { id } = user;

  const copiedClass = classnames({
    result__password: true,
    'result__password--copied': !!copied,
    'result__password--not-copied': !copied,
  });

  return (
    <div className="action__button__container">
      <Button loading={resetting} onClick={() => doResetPassword(id, 'sl')}>
        Reset Password
      </Button>
      {password ? (
        <div className="user-action__result">
          <div className={copiedClass}>
            <CopyToClipboard text={password} onCopy={() => doCopyPassword()}>
              <span>{password}</span>
            </CopyToClipboard>
          </div>
          <div className="result__message">
            {copied ? 'copied!' : '* click to copy to clipboard'}
          </div>
        </div>
      ) : null}
    </div>
  );
};

ResetPassword.propTypes = {
  resetting: PropTypes.bool,
  doResetPassword: PropTypes.func.isRequired,
  password: PropTypes.string,
  doCopyPassword: PropTypes.func.isRequired,
  copied: PropTypes.bool,
  user: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
};

ResetPassword.defaultProps = {
  resetting: null,
  password: null,
  copied: null,
};

export default ResetPassword;
