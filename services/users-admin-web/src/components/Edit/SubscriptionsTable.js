import React from 'react';
import PropTypes from 'prop-types';

import { format } from 'date-fns';

import config from '../../config';
import { Table, TableBlock, TableRow } from '../Table';
import { withErrorBoundary } from '../../containers/ErrorBoundary';

const SubscriptionsTable = ({ subscriptionsData, label }) => {
  // Filter each specific type of subscription into seperate arrays
  const slSubscriptions = subscriptionsData.filter(
    ({ entitlement }) => entitlement.indexOf('sl') !== -1,
  );
  const bwSubscriptions = subscriptionsData.filter(
    ({ entitlement }) => entitlement.indexOf('bw') !== -1,
  );
  const fsSubscriptions = subscriptionsData.filter(
    ({ entitlement }) => entitlement.indexOf('fs') !== -1,
  );

  // Create an object to be iterated for creating the tables
  const subscriptions = {
    'Surfline Subscriptions': {
      bgColor: '#E6F3FF',
      headerBgColor: '#8CC6F9',
      data: slSubscriptions,
    },
    'Buoyweather Subscriptions': {
      data: bwSubscriptions,
      bgColor: '#D7F0FA',
      headerBgColor: '#6BC5EC',
    },
    'FishTrack Subscriptions': {
      data: fsSubscriptions,
      bgColor: '#D5F3F7',
      headerBgColor: '#9DE3EA',
    },
  };

  return (
    <div className="sub-table-container section">
      <h5 className="header__label">{label}</h5>
      {Object.keys(subscriptions).map((key) => (
        <div key={key}>
          <p className="sub-table-title">{key}</p>
          <Table backgroundColor={subscriptions[key].bgColor}>
            <TableRow backgroundColor={subscriptions[key].headerBgColor}>
              <TableBlock isHeader text="Type" />
              <TableBlock isHeader text="Period Start" />
              <TableBlock isHeader text="Period End" />
              <TableBlock isHeader text="Plan" />
              <TableBlock isHeader text="Sub ID" />
              <TableBlock isHeader text="Canceled" />
              <TableBlock isHeader text="Failed Attempts" />
            </TableRow>
            {subscriptions[key].data.map((subscription, index) => {
              const { applePlanId, stripePlanId, subscriptionId } = subscription;
              const planId = subscription.planId || applePlanId || stripePlanId;

              return (
                <TableRow
                  key={subscription.subscriptionId}
                  backgroundColor={index % 2 === 0 ? '#FFFFFF' : null}
                >
                  <TableBlock text={subscription.type} />
                  <TableBlock text={format(subscription.periodStart, 'M/D/YY H:mm')} />
                  <TableBlock text={format(subscription.periodEnd, 'M/D/YY H:mm')} />
                  <TableBlock text={planId} />
                  <TableBlock
                    text={subscriptionId}
                    link={
                      subscription.type === 'stripe'
                        ? `${config.stripeSubscriptionDashboardUrl}/${subscriptionId}`
                        : null
                    }
                  />
                  <TableBlock
                    text={subscription.expiring ? subscription.expiring.toString() : null}
                  />
                  <TableBlock text={subscription.failedPaymentAttempts || 0} />
                </TableRow>
              );
            })}
          </Table>
        </div>
      ))}
    </div>
  );
};

SubscriptionsTable.propTypes = {
  label: PropTypes.string,
  subscriptionsData: PropTypes.arrayOf(PropTypes.shape({})),
};

SubscriptionsTable.defaultProps = {
  label: '',
  subscriptionsData: null,
};

export default withErrorBoundary(SubscriptionsTable, 'Subscription Table');
