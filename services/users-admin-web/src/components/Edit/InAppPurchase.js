import React from 'react';
import PropTypes from 'prop-types';

import { TextButton } from '@surfline/admin-common';

class RemoveButton extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      removing: false,
    };
  }

  toggleRemove = () => {
    const { removing } = this.state;
    this.setState({
      removing: !removing,
    });
  };

  render() {
    const { remove } = this.props;
    const { removing } = this.state;

    if (removing) {
      return (
        <div className="flex-btn">
          <TextButton onClick={this.toggleRemove}>Cancel</TextButton>
          <TextButton onClick={remove} type="active">
            Delete
          </TextButton>
        </div>
      );
    }
    return (
      <div className="flex-btn">
        <TextButton loading={removing} onClick={this.toggleRemove}>
          Remove
        </TextButton>
      </div>
    );
  }
}

const InAppPurchase = ({ iapSubscription, remove }) => (
  <div className="in-app-purchase">
    <h6 className="iap-sub-id">{iapSubscription.applePlanId || iapSubscription.entitlement}</h6>
    {!iapSubscription.applePlanId && <RemoveButton remove={remove} />}
  </div>
);

InAppPurchase.propTypes = {
  iapSubscription: PropTypes.shape({
    entitlement: PropTypes.string,
    applePlanId: PropTypes.string,
  }).isRequired,
  remove: PropTypes.func.isRequired,
};

RemoveButton.propTypes = {
  remove: PropTypes.func.isRequired,
};

export default InAppPurchase;
