import React from 'react';
import PropTypes from 'prop-types';

import EditInfo from './EditInfo';

const Header = ({
  stripeDashboardLink,
  updateUserInfo,
  user,
  loading,
  children,
  updating,
  updateUserError,
  updateSuccess,
  verifyEmail,
  verifyingEmail,
  emailVerificationError,
}) => (
  <div className="section header-section">
    <div className="user-info">
      <h4 className="user-name">
        {loading ? 'Loading...' : user && `${user.firstName} ${user.lastName}`}
      </h4>
      {!loading && (
        <EditInfo
          user={user}
          updateUserInfo={updateUserInfo}
          updating={updating}
          updateUserError={updateUserError}
          updateSuccess={updateSuccess}
          verifyEmail={verifyEmail}
          verifyingEmail={verifyingEmail}
          emailVerificationError={emailVerificationError}
        />
      )}
      {stripeDashboardLink && (
        <a
          className="stripe-link"
          href={stripeDashboardLink}
          target="_blank"
          rel="noopener noreferrer"
        >
          Stripe Customer Dashboard
        </a>
      )}
    </div>
    {children}
  </div>
);

Header.propTypes = {
  stripeDashboardLink: PropTypes.string,
  updateUserInfo: PropTypes.func.isRequired,
  updateUserError: PropTypes.string,
  updating: PropTypes.bool,
  updateSuccess: PropTypes.bool,
  user: PropTypes.shape({ firstName: PropTypes.string, lastName: PropTypes.string }).isRequired,
  loading: PropTypes.bool,
  children: PropTypes.node,
  verifyEmail: PropTypes.func.isRequired,
  verifyingEmail: PropTypes.bool,
  emailVerificationError: PropTypes.string,
};

Header.defaultProps = {
  stripeDashboardLink: null,
  updateUserError: null,
  updating: null,
  updateSuccess: null,
  loading: null,
  children: null,
  verifyingEmail: null,
  emailVerificationError: null,
};

export default Header;
