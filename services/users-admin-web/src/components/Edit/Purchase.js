import React from 'react';
import PropTypes from 'prop-types';

import { format } from 'date-fns';

const Purchase = ({ purchase }) => {
  /**
   * The date formats for receipts are inconsistent :(
   * This means we have to perform some regex validation
   * and perform proper conversion so we don't get "Invalid Date"
   * when formatting.
   */

  let { purchaseDate, expirationDate } = purchase;
  purchaseDate = purchaseDate || purchase.purchase_date_ms;
  expirationDate = expirationDate || purchase.expires_date_ms;

  const purchaseDateIsNum = /^\d+$/.test(purchaseDate);
  const expirationDateIsNum = /^\d+$/.test(expirationDate);

  if (purchaseDateIsNum) {
    purchaseDate = parseInt(purchaseDate, 10);
  }
  if (expirationDateIsNum) {
    expirationDate = parseInt(expirationDate, 10);
  }

  return (
    <div className="receipt__purchase">
      <div className="receipt__purchase-info">
        <h6>Purchase Date:</h6>
        <p>{format(purchaseDate, 'M/D/YY H:mm')}</p>
      </div>
      <div className="receipt__purchase-info">
        <h6>Expiration Date:</h6>
        <p>{expirationDate ? format(expirationDate, 'M/D/YY H:mm') : 'Expired'}</p>
      </div>
      <div className="receipt__purchase-info">
        <h6>Product ID:</h6>
        <p>{purchase.productId || purchase.product_id}</p>
      </div>
    </div>
  );
};

Purchase.propTypes = {
  purchase: PropTypes.shape({
    product_id: PropTypes.string,
    purchase_date_ms: PropTypes.number,
    purchaseDate: PropTypes.string,
    productId: PropTypes.string,
    expires_date_ms: PropTypes.number,
    expirationDate: PropTypes.string,
  }).isRequired,
};

export default Purchase;
