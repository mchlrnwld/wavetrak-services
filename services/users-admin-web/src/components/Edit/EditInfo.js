import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from '@surfline/quiver-react';
import VerifiedLabel from './VerifiedLabel';

import emailValidation from '../../utils/validation';

/**
 * @description This stateful class component contains the logic for editing a user's email
 * from the admin tool. The state will toggle between editing and not editing the email.
 * Upon mounting the email will be displayed as text and if the user wants to edit it an input
 * component will be rendered.
 *
 * @param {object} props
 * @param {object} props.user
 */
class EditInfo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isEditing: false,
      editedEmail: null,
      editedFirstName: null,
      editedLastName: null,
      errorMessage: null,
      showErrorMessage: false,
    };
  }

  componentDidUpdate(prevProps) {
    const { showErrorMessage } = this.state;
    const { updateSuccess, updating } = this.props;
    const updateWasSuccessful =
      updateSuccess !== prevProps.updateSuccess && prevProps.updateSuccess === true;

    const shouldShowError =
      updating && prevProps.updateSuccess === false && showErrorMessage === false;

    if (updateWasSuccessful) {
      this.clearEditingState();
      this.toggleEditing();
    } else if (shouldShowError) {
      this.showErrorMessage();
    }
  }

  clearEditingState = () =>
    this.setState({ editedEmail: null, editedFirstName: null, editedLastName: null });

  showErrorMessage = () => this.setState({ showErrorMessage: true });

  onEmailChange = (evt) => {
    const { value } = evt.target;
    this.setState({ editedEmail: value, showErrorMessage: false, errorMessage: null });
  };

  onFirstNameChange = (evt) => {
    const { value } = evt.target;
    this.setState({ editedFirstName: value, showErrorMessage: false, errorMessage: null });
  };

  onLastNameChange = (evt) => {
    const { value } = evt.target;
    this.setState({ editedLastName: value, showErrorMessage: false, errorMessage: null });
  };

  toggleEditing = () => {
    const { isEditing } = this.state;
    this.setState({
      isEditing: !isEditing,
      editedEmail: null,
      editedFirstName: null,
      editedLastName: null,
      showErrorMessage: false,
    });
  };

  editInfo = () => {
    const { updateUserInfo } = this.props;
    const { editedEmail, editedFirstName, editedLastName } = this.state;
    const newEmail = editedEmail;
    const newFirstName = editedFirstName;
    const newLastName = editedLastName;

    if (newEmail) {
      // If there is no new email, then return early since it must be a mistake
      const isEmailValid = emailValidation(newEmail);
      if (!isEmailValid) {
        this.setState({
          showErrorMessage: true,
          errorMessage: 'Invalid Email.',
        });
        return;
      }
    }

    updateUserInfo(newEmail, newFirstName, newLastName);
  };

  render() {
    const {
      user,
      updateUserError,
      updating,
      updateSuccess,
      verifyEmail,
      verifyingEmail,
      emailVerificationError,
    } = this.props;

    const {
      isEditing,
      showErrorMessage,
      editedEmail,
      editedFirstName,
      editedLastName,
      errorMessage,
    } = this.state;

    const submitBtnProps = {
      button: {
        disabled: !editedEmail && !editedFirstName && !editedLastName,
      },
      loading: updating,
      onClick: this.editInfo,
    };

    const verifiedLabelProps = {
      user,
      verifyEmail,
      verifyingEmail,
      emailVerificationError,
    };

    // Don't render until we have a user
    if (!user) return null;

    return (
      <div className="edit-user-info">
        <div className="edit-info">
          {isEditing ? (
            <div className="user-info__form">
              <Input
                input={{ onChange: this.onEmailChange }}
                defaultValue={user.email}
                label="Email"
              />
              <Input
                input={{ onChange: this.onFirstNameChange }}
                defaultValue={user.firstName}
                label="First Name"
              />
              <Input
                input={{ onChange: this.onLastNameChange }}
                defaultValue={user.lastName}
                label="Last Name"
              />
            </div>
          ) : (
            <h5 className="user-info__email">
              {user.email}
              <VerifiedLabel {...verifiedLabelProps} />
            </h5>
          )}
          <div className="edit-info__action-buttons">
            {isEditing && <Button {...submitBtnProps}>Submit</Button>}
            <Button
              success={updateSuccess}
              button={{ disabled: updating }}
              onClick={this.toggleEditing}
            >
              {isEditing ? 'Cancel' : 'Edit Info'}
            </Button>
          </div>
        </div>
        <div className="error-block">
          {showErrorMessage && (
            <h6 className="error-block__message">{updateUserError || errorMessage}</h6>
          )}
        </div>
      </div>
    );
  }
}

EditInfo.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.string,
    email: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    isEmailVerified: PropTypes.bool,
  }).isRequired,
  updateUserInfo: PropTypes.func.isRequired,
  updating: PropTypes.bool,
  updateSuccess: PropTypes.bool,
  updateUserError: PropTypes.string,
  verifyEmail: PropTypes.func.isRequired,
  verifyingEmail: PropTypes.bool,
  emailVerificationError: PropTypes.string,
};

EditInfo.defaultProps = {
  updateUserError: null,
  updating: null,
  updateSuccess: null,
  verifyingEmail: null,
  emailVerificationError: null,
};

export default EditInfo;
