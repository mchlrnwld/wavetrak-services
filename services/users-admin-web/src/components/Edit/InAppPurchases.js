import React from 'react';
import PropTypes from 'prop-types';

import InAppPurchase from './InAppPurchase';

const InAppPurchases = ({ iapSubscriptions, removeIapSubscription, type }) => (
  <div className="section in-app-purchases">
    <h5 className="header__label">In-app Purchases: {type}</h5>
    {iapSubscriptions.length === 0
      ? `No ${type} subscriptions found`
      : iapSubscriptions.map((iapSub) => (
          <InAppPurchase
            iapSubscription={iapSub}
            key={iapSub.subscriptionId}
            remove={() => removeIapSubscription(iapSub.subscriptionId)}
          />
        ))}
  </div>
);

InAppPurchases.propTypes = {
  iapSubscriptions: PropTypes.arrayOf(
    PropTypes.shape({
      subscriptionId: PropTypes.string,
    }),
  ).isRequired,
  removeIapSubscription: PropTypes.func.isRequired,
  type: PropTypes.oneOf(['apple', 'google']).isRequired,
};

export default InAppPurchases;
