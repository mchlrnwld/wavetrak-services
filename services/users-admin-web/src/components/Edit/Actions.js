import React from 'react';
import PropTypes from 'prop-types';
import ClearUserTokens from './ClearUserTokens';
import ResetPassword from './ResetPassword';
import ProcessGDPRRequest from './ProcessGDPRRequest';
import DissociateCustomer from './DissociateCustomer';

const Actions = ({
  user,
  clearTokens,
  clearing,
  message,
  doResetPassword,
  doCopyPassword,
  password,
  resetting,
  copied,
  doProcessGDPRRequest,
  processing,
  subscriptions,
  doDissociateCustomer,
  dissociating,
}) => (
  <div>
    <h5 className="header__label">Actions</h5>
    <div className="actions_container">
      <div className="actions">
        <div className="action action--safe">
          <div className="action__description">
            <span>
              This action will invalidate all access tokens and log a user out of all authenticated
              sessions. This action is useful for troubleshooting persistent Authentication issues.
            </span>
          </div>
          <div className="action__button">
            <ClearUserTokens clearUserTokens={clearTokens} clearing={clearing} message={message} />
          </div>
        </div>
        <div className="action action--safe">
          <div className="action__description">
            <span>
              This action generates and sets a new, random password for the user. Clicking this
              button will display the new password when the request is successful.
            </span>
          </div>
          <div className="action__button">
            <ResetPassword
              doResetPassword={doResetPassword}
              doCopyPassword={doCopyPassword}
              password={password}
              resetting={resetting}
              user={user}
              copied={copied}
            />
          </div>
        </div>
      </div>
      <div className="actions">
        <div className="action action--destructive">
          <div className="action__description">
            <span>
              This action permanently deletes all data associated with this user. GDPR Requests for
              users with active subscriptions are unavailable for fulfillment. Therefore, the button
              will remain disabled for those instances.
            </span>
          </div>
          <div className="action__button">
            <ProcessGDPRRequest
              user={user}
              processing={processing}
              doProcessGDPRRequest={doProcessGDPRRequest}
              subscriptions={subscriptions}
            />
          </div>
        </div>
        <div className="action action--destructive">
          <div className="action__description">
            <span>
              Use this action when a user requests to change the currency used to bill their
              previous subscriptions. This action only applies to web-based subscriptions. A user
              will only be able to view plans respective to their location after this action
              succeeds.
            </span>
          </div>
          <div className="action__button">
            <DissociateCustomer
              subscriptions={subscriptions}
              customerId={user.stripeCustomerId}
              doDissociateCustomer={doDissociateCustomer}
              dissociating={dissociating}
              userId={user.id}
            />
          </div>
        </div>
      </div>
    </div>
  </div>
);

Actions.defaultProps = {
  clearing: null,
  processing: null,
  message: null,
  resetting: null,
  password: null,
  copied: null,
  subscriptions: null,
  dissociating: null,
};

Actions.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.string,
    stripeCustomerId: PropTypes.string,
  }).isRequired,
  clearTokens: PropTypes.func.isRequired,
  clearing: PropTypes.bool,
  processing: PropTypes.bool,
  message: PropTypes.string,
  doResetPassword: PropTypes.func.isRequired,
  doCopyPassword: PropTypes.func.isRequired,
  resetting: PropTypes.bool,
  password: PropTypes.string,
  copied: PropTypes.bool,
  doProcessGDPRRequest: PropTypes.func.isRequired,
  subscriptions: PropTypes.arrayOf(PropTypes.shape({})),
  doDissociateCustomer: PropTypes.func.isRequired,
  dissociating: PropTypes.bool,
};

export default Actions;
