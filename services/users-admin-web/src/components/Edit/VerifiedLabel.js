import React from 'react';
import PropTypes from 'prop-types';
import { CheckMark } from '@surfline/quiver-react';

const VerifiedLabel = ({ user, verifyEmail, verifyingEmail, emailVerificationError }) => {
  const attemptVerification = () => {
    verifyEmail(user.id, user.email, user.brand);
  };

  if (emailVerificationError) {
    return (
      <span className="user-info__verification-label user-info__verification-error">
        (Un-verified,
        <span>Unable to verify: {emailVerificationError.toString()}</span>
        <button type="button" className="user-info__verify-link" onClick={attemptVerification}>
          Try again
        </button>
        )
      </span>
    );
  }

  if (verifyingEmail) {
    return (
      <span className="user-info__verification-label user-info__verifying">
        (Un-verified, Verifying...)
      </span>
    );
  }

  if (!user.isEmailVerified) {
    return (
      <span className="user-info__verification-label user-info__unverified">
        (Un-verified,
        <button type="button" className="user-info__verify-link" onClick={attemptVerification}>
          Verify email
        </button>
        )
      </span>
    );
  }

  return (
    <span className="user-info__verification-label user-info__verified">
      Verified <CheckMark />
    </span>
  );
};

VerifiedLabel.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.string,
    email: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    isEmailVerified: PropTypes.bool,
    brand: PropTypes.string,
  }).isRequired,
  verifyEmail: PropTypes.func.isRequired,
  verifyingEmail: PropTypes.bool,
  emailVerificationError: PropTypes.string,
};

VerifiedLabel.defaultProps = {
  verifyingEmail: null,
  emailVerificationError: null,
};

export default VerifiedLabel;
