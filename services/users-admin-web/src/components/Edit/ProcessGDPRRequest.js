import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@surfline/quiver-react';

const ProcessGDPRRequest = ({ doProcessGDPRRequest, user, processing, subscriptions, message }) => {
  const { id } = user;
  const confirmGDPRRequest = () => {
    // eslint-disable-next-line no-alert
    const result = window.confirm(
      'The GDPR Request action cannot be undone.  Would you like to proceed?',
    );
    if (result) {
      return doProcessGDPRRequest(id);
    }
    return null;
  };
  return (
    <div className="action__button__container">
      {subscriptions.length ? (
        <button type="button" disabled className="quiver-button">
          Process GDPR Request
        </button>
      ) : (
        <Button loading={processing} onClick={() => confirmGDPRRequest()}>
          Process GDPR Request
        </Button>
      )}
      {message && <h6 className="user-action__message">{message}</h6>}
    </div>
  );
};

ProcessGDPRRequest.propTypes = {
  doProcessGDPRRequest: PropTypes.func.isRequired,
  user: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
  processing: PropTypes.bool,
  subscriptions: PropTypes.arrayOf(PropTypes.shape({})),
  message: PropTypes.string,
};

ProcessGDPRRequest.defaultProps = {
  subscriptions: [],
  message: null,
  processing: null,
};

export default ProcessGDPRRequest;
