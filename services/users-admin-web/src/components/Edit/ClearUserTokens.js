import React from 'react';
import PropTypes from 'prop-types';

import { Button } from '@surfline/quiver-react';

const ClearUserTokens = ({ clearing, clearUserTokens, message }) => (
  <div className="action__button__container">
    <Button loading={clearing} onClick={() => clearUserTokens()}>
      Clear Tokens
    </Button>
    {message && <h6 className="user-action__message">{message}</h6>}
  </div>
);

ClearUserTokens.propTypes = {
  clearing: PropTypes.bool,
  clearUserTokens: PropTypes.func.isRequired,
  message: PropTypes.string,
};

ClearUserTokens.defaultProps = {
  clearing: null,
  message: null,
};

export default ClearUserTokens;
