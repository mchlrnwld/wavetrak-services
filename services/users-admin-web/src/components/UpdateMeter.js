import React, { useCallback, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Input, Button, ErrorBox } from '@surfline/quiver-react';
import { mutate } from 'swr';

import { patchMeter } from '../common/api';
import { meterPropTypes } from '../propTypes/meter';

const UpdateMeter = ({ meter }) => {
  const { register, errors, handleSubmit, reset, formState, watch, setError } = useForm({
    mode: 'all',
  });

  const watchAllFields = watch();

  const updateMeter = useCallback(
    async (body) => {
      try {
        await patchMeter(meter?._id, body);

        // update form state with the body so that the form is not flagged as "dirty"
        reset({}, { isSubmitted: true });

        // refetch data after updating meter
        mutate(meter?.user);
      } catch (error) {
        // update form state so that error message persists till user changes input
        reset(body, { isSubmitted: true });
        setError('submitError', { message: 'Could not update meter.' });
      }
    },
    [meter, setError, reset],
  );

  // This effect will reset the form submission state (using the current form values)
  // once the user changes any of the inputs in the form again after submitting
  useEffect(() => {
    if ((formState.isSubmitted || formState.isSubmitSuccessful) && formState.isDirty) {
      reset(watchAllFields);
    }
  }, [reset, watchAllFields, formState]);

  return (
    <div className="sl-admin-meter__form">
      <form onSubmit={handleSubmit(updateMeter)}>
        <Input
          controlled
          type="number"
          input={{ name: 'meterRemaining' }}
          error={!!errors?.meterRemaining?.message}
          message={errors?.meterRemaining?.message}
          label="Set Meter Remaining"
          ref={register({
            required: false,
            setValueAs: (value) => (value ?? false ? parseInt(value, 10) : undefined),
            max: {
              value: meter.meterLimit || 3,
              message: 'Meter Remaining cannot be larger than `meterLimit` or 3.',
            },
            min: {
              value: -1,
              message: 'Meter Remaining cannot be below -1.',
            },
          })}
        />
        {errors?.submitError?.message && <ErrorBox message={errors.submitError.message} />}
        <Button
          style={{ width: '100%' }}
          loading={formState.isSubmitting}
          success={formState.isSubmitSuccessful && !errors?.submitError?.message}
          button={{ type: 'submit' }}
        >
          UPDATE METER
        </Button>
      </form>
    </div>
  );
};

UpdateMeter.propTypes = {
  meter: meterPropTypes.isRequired,
};

export default UpdateMeter;
