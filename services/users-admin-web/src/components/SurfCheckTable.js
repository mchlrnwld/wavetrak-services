import React from 'react';
import PropTypes from 'prop-types';
import { format } from 'date-fns';

const SurfCheckTable = ({ meter }) => {
  const { metersConsumed, meterRemaining } = meter;

  const renderUsedSurfChecks = () => {
    return metersConsumed?.map((timestamp, i) => {
      return (
        <tr>
          <td>{i + 1}</td>
          <td className="sl-admin-surfcheck__used">Used</td>
          <td>{format(new Date(timestamp), 'MM/DD/YY H:mm')}</td>
        </tr>
      );
    });
  };

  const renderUnusedSurfChecks = () => {
    const numUnusedSurfChecks = meterRemaining > -1 ? meterRemaining : 0;
    const offset = metersConsumed ? metersConsumed.length + 1 : 1;
    return [...Array(numUnusedSurfChecks)].map((_, i) => (
      <tr>
        <td>{offset + i}</td>
        <td>Unused</td>
        <td>-</td>
      </tr>
    ));
  };

  return (
    <div>
      <table>
        <thead>
          <tr>
            <td>Check #</td>
            <td>Status</td>
            <td>Started*</td>
          </tr>
        </thead>
        <tbody>
          {renderUsedSurfChecks()}
          {renderUnusedSurfChecks()}
        </tbody>
      </table>
      <div className="sl-admin-surfcheck__info">(* Displayed in your local timezone)</div>
    </div>
  );
};

SurfCheckTable.propTypes = {
  meter: PropTypes.isRequired,
};

export default SurfCheckTable;
