import React from 'react';
import PropTypes from 'prop-types';
import SearchResultRow from './SearchResultRow';

const SearchResults = ({ results }) => {
  const resultList = (arr) =>
    arr.map((elem) => {
      const { key, firstName, lastName, email, link } = elem;
      return (
        <SearchResultRow
          key={key}
          firstName={firstName}
          lastName={lastName}
          email={email}
          link={link}
        />
      );
    });

  const resultsTable = (
    <table className="search-results__table">
      <thead>
        <tr>
          <th>Email</th>
          <th>First name</th>
          <th>Last name</th>
        </tr>
      </thead>
      <tbody>{resultList(results)}</tbody>
    </table>
  );

  return resultsTable;
};

SearchResults.propTypes = {
  results: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string,
      firstName: PropTypes.number,
      lastName: PropTypes.string,
      email: PropTypes.string,
      link: PropTypes.string,
    }),
  ),
};

SearchResults.defaultProps = {
  results: [],
};

export default SearchResults;
