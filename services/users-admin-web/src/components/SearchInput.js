import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from '@surfline/quiver-react';

const SearchInput = ({ defaultValue, onSubmit, message, loading }) => {
  const [query, setQuery] = useState(defaultValue);

  const onChange = (event) => {
    setQuery(event.target.value);
  };

  const submitForm = (event) => {
    event.preventDefault();
    onSubmit(query);
  };

  const style = {
    Input: {
      flexGrow: 1,
      marginRight: '20px',
    },
  };

  return (
    <form onSubmit={submitForm} className="search-input" style={style}>
      <Input
        label="Search"
        type="text"
        defaultValue={defaultValue}
        message={message}
        input={{ onChange }}
        style={style.Input}
      />
      <Button loading={loading}>Search</Button>
    </form>
  );
};

SearchInput.propTypes = {
  defaultValue: PropTypes.string,
  message: PropTypes.string,
  onSubmit: PropTypes.func,
  loading: PropTypes.bool,
};

SearchInput.defaultProps = {
  defaultValue: '',
  message: '',
  onSubmit: () => {},
  loading: false,
};

export default SearchInput;
