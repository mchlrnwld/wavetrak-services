import React from 'react';
import PropTypes from 'prop-types';

const SearchResultRow = ({ firstName, lastName, email, link }) => {
  return (
    <tr className="search-results__row">
      <td className="search-results__col">
        <a href={link}>{email}</a>
      </td>
      <td className="search-results__col">
        <a href={link}>{firstName}</a>
      </td>
      <td className="search-results__col">
        <a href={link}>{lastName}</a>
      </td>
    </tr>
  );
};

SearchResultRow.propTypes = {
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  email: PropTypes.string,
  link: PropTypes.string,
};

SearchResultRow.defaultProps = {
  firstName: '',
  lastName: '',
  email: '',
  link: '',
};

export default SearchResultRow;
