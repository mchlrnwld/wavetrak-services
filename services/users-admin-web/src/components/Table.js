import React from 'react';
import PropTypes from 'prop-types';

export const Table = ({ children, backgroundColor }) => (
  <div className="flex-table" style={{ backgroundColor }}>
    {children}
  </div>
);

export const TableRow = ({ children, backgroundColor }) => (
  <div style={{ backgroundColor }} className="flex-table-row">
    {children}
  </div>
);

/**
 * @param {object} props
 * @param {React.CSSProperties} props.style
 * @param {string} props.link
 * @param {string} props.text
 * @param {boolean} props.isHeader
 * @returns {JSX.Element}
 */
export const TableBlock = ({ isHeader, text, link, style }) => (
  <div style={style} className={`table-${isHeader ? 'header' : 'block'} table-item`}>
    {link ? (
      <a className="link table-item-text" target="_blank" rel="noopener noreferrer" href={link}>
        {text}
      </a>
    ) : (
      <p className="table-item-text">{text}</p>
    )}
  </div>
);

TableBlock.propTypes = {
  isHeader: PropTypes.bool,
  text: PropTypes.node,
  link: PropTypes.string,
  style: PropTypes.shape(),
};

TableBlock.defaultProps = {
  isHeader: false,
  text: null,
  link: null,
  style: null,
};

Table.propTypes = {
  backgroundColor: PropTypes.string,
  children: PropTypes.node,
};

Table.defaultProps = {
  backgroundColor: null,
  children: null,
};

TableRow.propTypes = {
  children: PropTypes.node,
  backgroundColor: PropTypes.string,
};

TableRow.defaultProps = {
  children: null,
  backgroundColor: null,
};
