import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import reducer from './subscriptions';
import {
  FETCH_SUBSCRIPTIONS,
  FETCH_SUBSCRIPTIONS_SUCCESS,
  FETCH_SUBSCRIPTIONS_FAILURE,
} from '../actions/subscriptions';

describe('Reducers / Subscriptions', () => {
  const initialState = deepFreeze({
    loading: false,
    error: null,
    v2subscriptions: [],
    v2googleInAppPurchases: [],
    v2appleInAppPurchases: [],
    stripeCustomerId: null,
  });

  const v2subscriptions = [
    {
      type: 'apple',
      _id: '0000',
      subscriptionId: '0000',
      user: '5cf6f5ffeedf0d000f1dcfbd',
    },
  ];
  const v2googleInAppPurchases = [];
  const v2appleInAppPurchases = [
    {
      type: 'apple',
      _id: '0000',
      subscriptionId: '0000',
      user: '5cf6f5ffeedf0d000f1dcfbd',
    },
  ];

  it('defaults to initial state', () => {
    expect(reducer()).to.deep.equal(initialState);
  });

  it('handles fetch subscriptions', () => {
    const action = deepFreeze({ type: FETCH_SUBSCRIPTIONS });
    expect(reducer(initialState, action)).to.deep.equal({
      loading: true,
      error: null,
      v2subscriptions: [],
      v2googleInAppPurchases: [],
      v2appleInAppPurchases: [],
      stripeCustomerId: null,
    });
  });

  it('handles fetch subscriptions success', () => {
    const state = deepFreeze({
      loading: true,
      error: null,
      v2subscriptions: [],
      v2googleInAppPurchases: [],
      v2appleInAppPurchases: [],
      stripeCustomerId: null,
    });
    const action = deepFreeze({
      type: FETCH_SUBSCRIPTIONS_SUCCESS,
      v2subscriptions,
      v2googleInAppPurchases,
      v2appleInAppPurchases,
      stripeCustomerId: 'cust_1234',
    });
    expect(reducer(state, action)).to.deep.equal({
      loading: false,
      error: null,
      v2subscriptions,
      v2googleInAppPurchases,
      v2appleInAppPurchases,
      stripeCustomerId: `cust_1234`,
    });
  });

  it('handles fetch subscriptions failure', () => {
    const state = deepFreeze({
      loading: true,
      error: null,
      v2subscriptions: [],
      v2googleInAppPurchases: [],
      v2appleInAppPurchases: [],
      stripeCustomerId: null,
    });
    const action = deepFreeze({
      type: FETCH_SUBSCRIPTIONS_FAILURE,
      error: 'Error fetching subscriptions',
    });
    expect(reducer(state, action)).to.deep.equal({
      loading: false,
      error: 'Error fetching subscriptions',
      v2subscriptions: [],
      v2googleInAppPurchases: [],
      v2appleInAppPurchases: [],
      stripeCustomerId: null,
    });
  });
});
