import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import reducer from './subscriptionUsers';
import {
  FETCH_SUBSCRIPTION_USERS,
  FETCH_SUBSCRIPTION_USERS_SUCCESS,
  FETCH_SUBSCRIPTION_USERS_FAILURE,
  UPDATE_PRIMARY_USER,
  UPDATE_PRIMARY_USER_SUCCESS,
  UPDATE_PRIMARY_USER_FAILURE,
} from '../actions/subscriptionUsers';

describe('Reducers / SubscriptionUsers', () => {
  const initialState = {
    error: null,
    subscriptionUsers: null,
  };

  const subscriptionUsers = [
    {
      _id: '6090b9f6ea496e24a6fa9535',
      subscriptionId: '230000937945723',
      type: 'apple',
      entitlement: 'sl_premium',
      user: { _id: '5ad73a077c1a3a00138a446e', email: 'a@test.com' },
      primaryUser: '572b480ea00bf0ce6d415976',
      isPrimaryUser: false,
      active: true,
      createdAt: '2021-05-04T03:05:26.301Z',
      updatedAt: '2021-08-16T19:14:37.319Z',
    },
    {
      _id: '6090b9f6ea496e24a6fa9536',
      subscriptionId: '230000937945723',
      type: 'apple',
      entitlement: 'sl_premium',
      user: { _id: '572b480ea00bf0ce6d415976', email: 'b@test.com' },
      primaryUser: '572b480ea00bf0ce6d415976',
      isPrimaryUser: true,
      active: true,
      createdAt: '2021-05-04T03:05:26.344Z',
      updatedAt: '2021-08-16T19:14:37.324Z',
    },
  ];

  it('defaults to initial state', () => {
    expect(reducer()).to.deep.equal(initialState);
  });

  it('handles fetch subscription users', () => {
    const action = deepFreeze({ type: FETCH_SUBSCRIPTION_USERS });
    expect(reducer(initialState, action)).to.deep.equal(initialState);
  });

  it('handles fetch subscription users success', () => {
    const action = deepFreeze({ type: FETCH_SUBSCRIPTION_USERS_SUCCESS, subscriptionUsers });
    expect(reducer(initialState, action)).to.deep.equal({
      error: null,
      subscriptionUsers,
    });
  });

  it('handles fetch subscription users failure', () => {
    const action = deepFreeze({
      type: FETCH_SUBSCRIPTION_USERS_FAILURE,
      error: 'Could not fetch subscription users',
    });
    expect(reducer(initialState, action)).to.deep.equal({
      error: 'Could not fetch subscription users',
      subscriptionUsers: null,
    });
  });

  it('handles update primary user', () => {
    const action = deepFreeze({
      type: UPDATE_PRIMARY_USER,
    });
    expect(reducer(initialState, action)).to.deep.equal({
      error: null,
      subscriptionUsers: null,
    });
  });

  it('handles update primary user success, clearing previous error', () => {
    const state = {
      error: 'Could not update primary user',
    };
    const action = deepFreeze({
      type: UPDATE_PRIMARY_USER_SUCCESS,
    });
    expect(reducer(state, action)).to.deep.equal({
      error: null,
    });
  });

  it('handles update primary user failure', () => {
    const action = deepFreeze({
      type: UPDATE_PRIMARY_USER_FAILURE,
      error: 'Could not fetch subscription users',
    });
    expect(reducer(initialState, action)).to.deep.equal({
      error: 'Could not update primary user',
      subscriptionUsers: null,
    });
  });
});
