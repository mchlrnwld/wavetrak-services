import createReducerFromHandlers from './createReducerFromHandlers';

import {
  FETCH_SUBSCRIPTIONS,
  FETCH_SUBSCRIPTIONS_SUCCESS,
  FETCH_SUBSCRIPTIONS_FAILURE,
} from '../actions/subscriptions';

const initialState = {
  loading: false,
  error: null,
  v2subscriptions: [],
  v2googleInAppPurchases: [],
  v2appleInAppPurchases: [],
  stripeCustomerId: null,
};

const handlers = {};

handlers[FETCH_SUBSCRIPTIONS] = (state) => ({
  ...state,
  loading: true,
});

handlers[FETCH_SUBSCRIPTIONS_SUCCESS] = (
  state,
  { v2subscriptions, v2googleInAppPurchases, v2appleInAppPurchases, stripeCustomerId },
) => ({
  ...state,
  loading: false,
  v2subscriptions,
  v2googleInAppPurchases,
  v2appleInAppPurchases,
  stripeCustomerId,
});

handlers[FETCH_SUBSCRIPTIONS_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

export default createReducerFromHandlers(handlers, initialState);
