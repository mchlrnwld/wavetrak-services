/* eslint no-unneeded-ternary: ["error", { "defaultAssignment": true }] */
import createReducerFromHandlers from './createReducerFromHandlers';
import {
  FETCH_USER_DETAILS,
  FETCH_USER_DETAILS_SUCCESS,
  FETCH_USER_DETAILS_FAILURE,
  CLEAR_USER_TOKENS,
  CLEAR_USER_TOKENS_SUCCESS,
  CLEAR_USER_TOKENS_FAILURE,
  UPDATE_USER_INFO,
  UPDATE_USER_INFO_SUCCESS,
  UPDATE_USER_INFO_FAILURE,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILURE,
  PASSWORD_COPIED,
  PROCESS_GDPR_REQUEST,
  PROCESS_GDPR_REQUEST_SUCCESS,
  PROCESS_GDPR_REQUEST_FAILURE,
  FETCH_CUSTOMER_DETAILS,
  FETCH_CUSTOMER_DETAILS_SUCCESS,
  FETCH_CUSTOMER_DETAILS_FAILURE,
  DISSOCIATE_CUSTOMER,
  DISSOCIATE_CUSTOMER_SUCCESS,
  DISSOCIATE_CUSTOMER_FAILURE,
  EMAIL_VERIFICATION,
  EMAIL_VERIFICATION_SUCCESS,
  EMAIL_VERIFICATION_FAILURE,
} from '../actions/edit';
import {
  REMOVE_IAP_SUBSCRIPTION,
  REMOVE_IAP_SUBSCRIPTION_SUCCESS,
  REMOVE_IAP_SUBSCRIPTION_FAILURE,
  VERIFY_RECEIPT,
  VERIFY_RECEIPT_SUCCESS,
  VERIFY_RECEIPT_FAILURE,
  RESTORE_PURCHASES,
  RESTORE_PURCHASES_SUCCESS,
  RESTORE_PURCHASES_FAILURE,
} from '../actions/editIap';

const initialState = {
  loading: false,
  adding: false,
  error: null,
  user: null,
  verifiedReceipts: {},
  verifyingEmail: false,
  emailVerificationError: null,
};

const handlers = {};

handlers[FETCH_USER_DETAILS] = (state) => ({
  ...state,
  loading: true,
  error: null,
  user: null,
});

handlers[FETCH_USER_DETAILS_SUCCESS] = (state, { user }) => ({
  ...state,
  loading: false,
  error: null,
  user,
});

handlers[FETCH_USER_DETAILS_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

handlers[REMOVE_IAP_SUBSCRIPTION] = (state) => ({
  ...state,
  error: null,
  user: state.user && {
    ...state.user,
    // subscriptions: []
  },
});

handlers[REMOVE_IAP_SUBSCRIPTION_SUCCESS] = (state, { subscriptionId }) => {
  const { googleInAppPurchases } = state.user;

  const user = state.user && {
    ...state.user,
    googleInAppPurchases: googleInAppPurchases.filter(
      (sub) => sub.subscriptionId !== subscriptionId,
    ),
  };

  return {
    ...state,
    error: null,
    user,
  };
};

handlers[REMOVE_IAP_SUBSCRIPTION_FAILURE] = (state, { error }) => ({
  ...state,
  error,
});

handlers[CLEAR_USER_TOKENS] = (state) => ({
  ...state,
  clearing: true,
});

handlers[CLEAR_USER_TOKENS_SUCCESS] = (state, { message }) => ({
  ...state,
  message,
  clearing: false,
});

handlers[CLEAR_USER_TOKENS_FAILURE] = (state, { message }) => ({
  ...state,
  message,
  clearing: false,
});

handlers[UPDATE_USER_INFO] = (state) => ({
  ...state,
  updating: true,
});

handlers[UPDATE_USER_INFO_SUCCESS] = (state, { email, firstName, lastName }) => ({
  ...state,
  user: {
    ...state.user,
    email: email ? email : state.user.email,
    firstName: firstName ? firstName : state.user.firstName,
    lastName: lastName ? lastName : state.user.lastName,
  },
  updateSuccess: true,
  updating: false,
});

handlers[UPDATE_USER_INFO_FAILURE] = (state, { error }) => ({
  ...state,
  updateSuccess: false,
  updateUserError: error,
  updating: false,
});

handlers[VERIFY_RECEIPT] = (state, { receiptNumber }) => ({
  ...state,
  verifiedReceipts: {
    ...state.verifiedReceipts,
    [`receipt${receiptNumber}`]: {
      ...state.verifiedReceipts[`receipt${receiptNumber}`],
      verifying: true,
    },
  },
});

handlers[VERIFY_RECEIPT_SUCCESS] = (state, { verifiedReceipt, receiptNumber }) => ({
  ...state,
  verifiedReceipts: {
    ...state.verifiedReceipts,
    [`receipt${receiptNumber}`]: {
      ...state.verifiedReceipts[`receipt${receiptNumber}`],
      verificationSuccess: true,
      verifying: false,
      receipt: verifiedReceipt,
    },
  },
});

handlers[VERIFY_RECEIPT_FAILURE] = (state, { receiptNumber, error }) => ({
  ...state,
  verifiedReceipts: {
    ...state.verifiedReceipts,
    [`receipt${receiptNumber}`]: {
      ...state.verifiedReceipts[`receipt${receiptNumber}`],
      verificationSuccess: false,
      verificationError: error,
      verifying: false,
    },
  },
});

handlers[RESTORE_PURCHASES] = (state) => ({
  ...state,
  restoring: true,
});

handlers[RESTORE_PURCHASES_SUCCESS] = (state, { user }) => ({
  ...state,
  restoreSuccess: true,
  restoring: false,
  user: {
    ...state.user,
    ...user,
  },
});

handlers[RESTORE_PURCHASES_FAILURE] = (state, { error }) => ({
  ...state,
  restoreSuccess: false,
  restoreError: error,
  restoring: false,
});

handlers[RESET_PASSWORD] = (state) => ({
  ...state,
  resetting: true,
  copied: false,
});

handlers[RESET_PASSWORD_SUCCESS] = (state, { password }) => ({
  ...state,
  resetting: false,
  password,
  copied: false,
});

handlers[RESET_PASSWORD_FAILURE] = (state, { error }) => ({
  ...state,
  resetting: false,
  error,
  copied: false,
});

handlers[PASSWORD_COPIED] = (state) => ({
  ...state,
  copied: true,
});

handlers[PROCESS_GDPR_REQUEST] = (state) => ({
  ...state,
  processing: true,
});

handlers[PROCESS_GDPR_REQUEST_SUCCESS] = (state, { success }) => ({
  ...state,
  processing: false,
  error: null,
  gdprSuccessMessage: success,
});

handlers[PROCESS_GDPR_REQUEST_FAILURE] = (state, { error }) => ({
  ...state,
  processing: false,
  error,
  success: null,
});

handlers[FETCH_CUSTOMER_DETAILS] = (state) => ({
  ...state,
  loading: true,
  customer: null,
});

handlers[FETCH_CUSTOMER_DETAILS_SUCCESS] = (state, { customer }) => ({
  ...state,
  loading: false,
  error: null,
  customer,
});

handlers[FETCH_CUSTOMER_DETAILS_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  customer: null,
  error,
});

handlers[DISSOCIATE_CUSTOMER] = (state) => ({
  ...state,
  dissociating: true,
  success: false,
  error: false,
});

handlers[DISSOCIATE_CUSTOMER_SUCCESS] = (state) => ({
  ...state,
  dissociating: false,
  error: null,
  success: true,
});

handlers[DISSOCIATE_CUSTOMER_FAILURE] = (state, { error }) => ({
  ...state,
  dissociating: false,
  success: false,
  error,
});

handlers[EMAIL_VERIFICATION] = (state) => ({
  ...state,
  verifyingEmail: true,
  emailVerificationError: null,
});

handlers[EMAIL_VERIFICATION_SUCCESS] = (state) => ({
  ...state,
  verifyingEmail: false,
  emailVerificationError: null,
  user: {
    ...state.user,
    isEmailVerified: true,
  },
});

handlers[EMAIL_VERIFICATION_FAILURE] = (state, { error }) => ({
  ...state,
  verifyingEmail: false,
  emailVerificationError: error,
});

export default createReducerFromHandlers(handlers, initialState);
