import { combineReducers } from 'redux';
import edit from './edit';
import search from './search';
import subscriptions from './subscriptions';
import subscriptionUsers from './subscriptionUsers';

const reducer = combineReducers({
  edit,
  search,
  subscriptions,
  subscriptionUsers,
});

export default reducer;
