import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import reducer from './edit';
import {
  FETCH_USER_DETAILS,
  FETCH_USER_DETAILS_SUCCESS,
  FETCH_USER_DETAILS_FAILURE,
  UPDATE_USER_INFO,
  UPDATE_USER_INFO_SUCCESS,
  UPDATE_USER_INFO_FAILURE,
  EMAIL_VERIFICATION,
  EMAIL_VERIFICATION_SUCCESS,
  EMAIL_VERIFICATION_FAILURE,
} from '../actions/edit';
import { VERIFY_RECEIPT, VERIFY_RECEIPT_SUCCESS, VERIFY_RECEIPT_FAILURE } from '../actions/editIap';

describe('Reducers / Edit', () => {
  const user = deepFreeze({
    name: 'Awesome User',
    firstName: 'john',
    lastName: 'florence',
    email: 'someemail@email.com',
  });

  const initialState = {
    loading: false,
    adding: false,
    error: null,
    user: null,
    verifiedReceipts: {},
    verifyingEmail: false,
    emailVerificationError: null,
  };

  it('defaults to initial state', () => {
    expect(reducer()).to.deep.equal(initialState);
  });

  it('handles fetch user details', () => {
    const action = deepFreeze({ type: FETCH_USER_DETAILS });
    expect(reducer(initialState, action)).to.deep.equal({
      loading: true,
      adding: false,
      error: null,
      user: null,
      verifiedReceipts: {},
      verifyingEmail: false,
      emailVerificationError: null,
    });
  });

  it('handles fetch user details success', () => {
    const state = deepFreeze({
      loading: true,
      adding: false,
      error: null,
      user: null,
    });
    const action = deepFreeze({ type: FETCH_USER_DETAILS_SUCCESS, user });
    expect(reducer(state, action)).to.deep.equal({
      loading: false,
      adding: false,
      error: null,
      user,
    });
  });

  it('handles fetch user details failure', () => {
    const state = deepFreeze({
      loading: true,
      adding: false,
      error: null,
      user: null,
    });
    const action = deepFreeze({
      type: FETCH_USER_DETAILS_FAILURE,
      error: 'Something exploded!',
    });
    expect(reducer(state, action)).to.deep.equal({
      loading: false,
      adding: false,
      error: 'Something exploded!',
      user: null,
    });
  });

  it('handles update user info', () => {
    const action = deepFreeze({ type: UPDATE_USER_INFO });
    expect(reducer(initialState, action)).to.deep.equal({
      updating: true,
      loading: false,
      adding: false,
      error: null,
      user: null,
      verifiedReceipts: {},
      verifyingEmail: false,
      emailVerificationError: null,
    });
  });

  it('handles update user info success', () => {
    const state = deepFreeze({
      loading: false,
      adding: false,
      error: null,
      user: {
        email: 'bleh@test.com',
        firstName: 'jeremy',
        lastName: 'monson',
      },
    });
    const action = deepFreeze({
      type: UPDATE_USER_INFO_SUCCESS,
      email: 'someemail@email.com',
      lastName: 'moonsoon',
    });
    expect(reducer(state, action)).to.deep.equal({
      loading: false,
      adding: false,
      error: null,
      updateSuccess: true,
      updating: false,
      user: {
        email: 'someemail@email.com',
        firstName: 'jeremy',
        lastName: 'moonsoon',
      },
    });
  });

  it('handles update user info failure', () => {
    const state = deepFreeze({
      loading: false,
      adding: false,
      error: null,
      user: null,
    });
    const action = deepFreeze({
      type: UPDATE_USER_INFO_FAILURE,
      error: 'Jeremy broke something!',
    });
    expect(reducer(state, action)).to.deep.equal({
      loading: false,
      adding: false,
      error: null,
      user: null,
      updateSuccess: false,
      updateUserError: 'Jeremy broke something!',
      updating: false,
    });
  });

  it('handles verify receipt', () => {
    const action = deepFreeze({ type: VERIFY_RECEIPT, receiptNumber: 0 });
    expect(reducer(initialState, action)).to.deep.equal({
      loading: false,
      adding: false,
      error: null,
      user: null,
      verifiedReceipts: {
        receipt0: {
          verifying: true,
        },
      },
      verifyingEmail: false,
      emailVerificationError: null,
    });
  });

  it('handles verify receipt success', () => {
    const state = deepFreeze({
      loading: false,
      adding: false,
      error: null,
      user: null,
      verifiedReceipts: {
        receipt0: {
          verifying: true,
        },
      },
    });
    const action = deepFreeze({
      type: VERIFY_RECEIPT_SUCCESS,
      verifiedReceipt: {},
      receiptNumber: 0,
    });
    expect(reducer(state, action)).to.deep.equal({
      loading: false,
      adding: false,
      error: null,
      user: null,
      verifiedReceipts: {
        receipt0: {
          verifying: false,
          verificationSuccess: true,
          receipt: {},
        },
      },
    });
  });

  it('handles verify receipt failure', () => {
    const state = deepFreeze({
      loading: false,
      adding: false,
      error: null,
      user: null,
      verifiedReceipts: {
        receipt0: {
          verifying: true,
        },
      },
    });
    const action = deepFreeze({
      type: VERIFY_RECEIPT_FAILURE,
      error: 'Jeremy broke something!',
      receiptNumber: 0,
    });
    expect(reducer(state, action)).to.deep.equal({
      loading: false,
      adding: false,
      error: null,
      user: null,
      verifiedReceipts: {
        receipt0: {
          verificationSuccess: false,
          verificationError: 'Jeremy broke something!',
          verifying: false,
        },
      },
    });
  });

  it('handles verify email', () => {
    const action = deepFreeze({ type: EMAIL_VERIFICATION });
    expect(reducer(initialState, action)).to.deep.equal({
      loading: false,
      adding: false,
      error: null,
      user: null,
      verifiedReceipts: {},
      verifyingEmail: true,
      emailVerificationError: null,
    });
  });

  it('handles verify email success', () => {
    const state = deepFreeze({
      loading: false,
      adding: false,
      error: null,
      user,
      verifyingEmail: true,
      emailVerificationError: null,
    });
    const action = deepFreeze({
      type: EMAIL_VERIFICATION_SUCCESS,
    });
    expect(reducer(state, action)).to.deep.equal({
      loading: false,
      adding: false,
      error: null,
      user: { ...user, isEmailVerified: true },
      verifyingEmail: false,
      emailVerificationError: null,
    });
  });

  it('handles verify email failure', () => {
    const state = deepFreeze({
      loading: false,
      adding: false,
      error: null,
      user,
      verifyingEmail: true,
      emailVerificationError: null,
    });
    const action = deepFreeze({
      type: EMAIL_VERIFICATION_FAILURE,
      error: 'Bad Request',
      receiptNumber: 0,
    });
    expect(reducer(state, action)).to.deep.equal({
      loading: false,
      adding: false,
      error: null,
      user,
      verifyingEmail: false,
      emailVerificationError: 'Bad Request',
    });
  });
});
