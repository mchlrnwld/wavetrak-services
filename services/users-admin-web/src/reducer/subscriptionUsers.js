import createReducerFromHandlers from './createReducerFromHandlers';
import {
  FETCH_SUBSCRIPTION_USERS,
  FETCH_SUBSCRIPTION_USERS_SUCCESS,
  FETCH_SUBSCRIPTION_USERS_FAILURE,
  UPDATE_PRIMARY_USER,
  UPDATE_PRIMARY_USER_SUCCESS,
  UPDATE_PRIMARY_USER_FAILURE,
} from '../actions/subscriptionUsers';

const initialState = {
  error: null,
  subscriptionUsers: null,
};

const handlers = {};

handlers[FETCH_SUBSCRIPTION_USERS] = (state) => ({
  ...state,
});

handlers[FETCH_SUBSCRIPTION_USERS_SUCCESS] = (state, { subscriptionUsers }) => ({
  ...state,
  subscriptionUsers,
});

handlers[FETCH_SUBSCRIPTION_USERS_FAILURE] = (state) => ({
  ...state,
  error: 'Could not fetch subscription users',
});

handlers[UPDATE_PRIMARY_USER] = (state) => ({
  ...state,
});

handlers[UPDATE_PRIMARY_USER_SUCCESS] = (state) => ({
  ...state,
  error: null,
});

handlers[UPDATE_PRIMARY_USER_FAILURE] = (state) => ({
  ...state,
  error: 'Could not update primary user',
});

export default createReducerFromHandlers(handlers, initialState);
