# Surfline Admin User Manager

Node version:

```
nvm install v12.22.1
nvm use v12.22.1
```

To run the application locally:

```
npm install
npm start
```

Build:

```
npm run build
```

Development:

This module is using ESLint in cohesion with prettier/eslint-prettier to quickly and easily format files and fix any linting issues.
There is no need to worry about any of this formatting during development if you don't want to set it up. The formatting and linting will
be performed on the commit hook. If you do have your own prettier config set up locally, you also do not have to worry about having conflicting configurations. The commit hook will run eslint-prettier against the staged files using the `.prettierrc` config that is committed into the repo.
In cohesion with ESLint this will ensure that we have standardized formatting and linting for all javascript files.
