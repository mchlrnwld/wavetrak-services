const newRelicAppName = 'Surfline Admin - Users';
const isEnabled = process.env.NODE_ENV === 'production';

console.log(`NR ${isEnabled}`);
exports.config = {
  app_name: [newRelicAppName],
  agent_enabled: isEnabled,
  license_key: 'a26ab469b2d2b56766af51d1c8c436e736ba72bf',
  capture_params: true,
  logging: {
    level: 'info'
  },
  transaction_tracer: {
    enabled: true
  },
  rules: {
    ignore: ['^/health']
  }
};
