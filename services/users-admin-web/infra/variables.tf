variable "company" {
  default = "sl"
}

variable "application" {
  default = "admin-users-service"
}

variable "environment" {
}

variable "alb_listener_arn" {
}

variable "iam_role_arn" {
}

variable "load_balancer_arn" {
}

variable "default_vpc" {
}

variable "dns_zone" {
}

variable "container_name" {
  default = "users"
}

variable "service_lb_healthcheck_path" {
  default = "/users/health"
}

variable "service_lb_priority" {
  default = 220
}

variable "service_port" {
  default = 8080
}

variable "service_td_count" {
  default = 1
}

variable "task_deregistration_delay" {
  default = 60
}
