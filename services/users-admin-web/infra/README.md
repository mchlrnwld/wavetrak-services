# Admin Users Service

This module contains _only_ the **infrastructure code** for the `users-admin-web` formerly configured as part of the `surfline-admin-tools`.

The application code can be found at [wavetrak-services/services/users-admin-web].

## Naming Conventions

See [naming-conventions] for AWS resources.

## Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV={env} make plan
ENV={env} make apply
```

Available environments are:

- sandbox
- staging
- prod

[wavetrak-services/services/users-admin-web]: https://github.com/Surfline/wavetrak-services/tree/master/services/users-admin-web
[naming-conventions]: https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions
