docker build -t admin-tools/users .
docker tag admin-tools/users:latest \
  665294954271.dkr.ecr.us-west-1.amazonaws.com/admin-tools/users:latest

$(aws --profile staging ecr get-login --no-include-email --region us-west-1)

docker push \
  665294954271.dkr.ecr.us-west-1.amazonaws.com/admin-tools/users:latest

# cd ../../

# ecs-cli compose --file docker/proxy.yml --project-name admin-tools-proxy create

python ../../tools/cluster_manager.py
