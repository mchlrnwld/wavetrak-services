# Session Analyzer

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Build and Run](#build-and-run)
- [Tests](#tests)
- [Improving the Algorithm](#improving-the-algorithm)
- [Processing Data](#processing-data)
- [Running Validation](#running-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


Python microservice to analyze session data coming from the Surfline Apple Watch App

## Build and Run

```sh
$ cp .env.sample .env
$ make build
$ make run
```

## Tests

We have both integration and unit tests. We use the integration test runner to run all tests since they require the same dependencies.

To run the tests:

```sh
cp .env.sample .env.test
make test
```

To add to the unit tests either modify an existing test file like `lib.WaveClarrifier_test` or add a new unit test file so long as the file as `_test` at the end the unit test runner will pick it up.

We store various JSON payloads in `fixtures/` and use those to run the unit tests specified in `lib.WaveClassifier_test`.

## Improving the Algorithm

The main crux of the algorithm which users GPS data to determine when a user is surfing or paddling is within `lib.WaveClassified`. To improve the algorithm we need a set of raw session data and some ground truth on that data as to when a user was surfing or not. The workflow for improving the algorithm looks like this:

- Within the `research/process` folder is a series of scripts to download, store and process data to help validate our algorithm. The output of this is stored in `research/validate/data`. We store the raw data as part of this repo but don't store any validation results. See processing data below on details what to do here.
- `research/validate/data/` contains these files:
  - `[id]-[sl|rc]-positions.json` a JSON structure which matches the input for the `/analyze` endpoint. sl = came from our algorithm and rc = RipCurl.
  - `[id]-[sl|rc]-waves.json` JSON array with one object per detected wave, with start/end timestamps for those waves. These are considered the ground truths.
- `research/validate/validate.py` this is script that'll run the validation on one or more source files. See Running Validation below.
- `research/validate/viewer` contains a web app to view the validation results. See Running Validation below.

## Processing Data

To download and structure a given session for validation execute:

```bash
cp research/process/.env.sample research/process/.env
make build_process
make run_process SESSION_IDS=[csv of ids]
```

## Running Validation

The validation app contains both a validation runner and a viewer. Everything is run through the validation container, upon starting the container apache/php is started and the python env enabled.

```bash
cp .env.sample .env.validate
make build_validate
make run_validate

# Run should start the container and put you in it.
source activate session-analyzer

# You can either validate a specific source file.
python -m research.validate.validate --key 5d1648372f6f5e524c664d55-sl
# Or all
python -m research.validate.validate

# To view results open http://localhost:8080/
```
