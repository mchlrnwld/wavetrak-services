SHELL := /bin/bash

.PHONY: test-integration
test-integration:
	sh ../../scripts/integration-tests.sh

.PHONY: test
test:
	sh ../../scripts/integration-tests.sh

.PHONY: start
start:
	python app.py

.PHONY: build
build:
	docker build -t session-analyzer .

.PHONY: run
run:
	docker run -v $(PWD):/usr/src/app -p 5002:5002 --env-file=.env -it session-analyzer

.PHONY: enter
shell:
	docker run -v $(PWD):/usr/src/app -p 5002:5002 --entrypoint /bin/bash --env-file=.env -it session-analyzer

.PHONY: lint
lint:
	@pycodestyle --exclude=research/,infra/ .

.PHONY: build_process
build_process:
	docker build --no-cache -t session-analyzer-process -f research/Dockerfile.process .

.PHONY: run_process
run_process:
	docker run -v $(PWD)/research:/opt/app --env-file=research/process/.env --rm session-analyzer-process npm run start $(SESSION_IDS)

.PHONY: build_validate
build_validate:
	docker build -t session-analyzer .
	docker build --no-cache -t session-analyzer-validate -f research/Dockerfile.validate .

.PHONY: run_validate
run_validate:
	docker run -v $(PWD):/opt/app -p 8080:80 --env-file=.env.validate -it session-analyzer-validate /bin/bash
