#!/usr/bin/env bash
set -xe

source activate session-analyzer

python -m unittest integration-tests.main
python -m unittest discover -s lib -p '*_test.py'
