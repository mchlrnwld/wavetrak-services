# -*- coding: utf-8 -*-
import time
import logging
import os

logging.basicConfig(
  format='%(asctime)s %(levelname)-8s %(message)s',
  level='DEBUG',
  datefmt='%Y-%m-%d %H:%M:%S')

log = logging.getLogger(__name__)

def start():
  return time.time()

def checkpoint(name, start, silent=False):
  end = time.time()

  if not silent:
    log.info('%s %2.2fs (%3.2fms)' % (name, (end - start), (end - start) * 1000))

  return (end - start)
