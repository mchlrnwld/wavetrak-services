"""
R&D using Kalman filter to filter postions within WaveClassifier. This method should be developed within that class not here.
This is here so it's saved somewhere if we come back to it but not in the production code.
"""
    def __kalman_filter_positions(self):
        """
        Apply a naive Kalman filter to the position data assuming velocity is
        constant. Modifies self.positions in place.

        NB. This doesn't seem to fix position issues but creates new position
        problems.
        """
        points = [(x['longitude'], x['latitude']) for x in self.positions]
        measurements = np.asarray(points)

        initial_state_mean = [measurements[0, 0],
                              0,
                              measurements[0, 1],
                              0]

        transition_matrix = [[1, 1, 0, 0],
                             [0, 1, 0, 0],
                             [0, 0, 1, 1],
                             [0, 0, 0, 1]]

        observation_matrix = [[1, 0, 0, 0],
                              [0, 0, 1, 0]]

        kf1 = KalmanFilter(transition_matrices=transition_matrix,
                           observation_matrices=observation_matrix,
                           initial_state_mean=initial_state_mean)

        kf1 = kf1.em(measurements, n_iter=5)
        (smoothed_state_means,
         smoothed_state_covariances) = kf1.smooth(measurements)

        smoothed_positions = []
        for x, item in enumerate(self.positions):
            item['longitude'] = smoothed_state_means[x, 0]
            item['latitude'] = smoothed_state_means[x, 2]
            smoothed_positions.append(item)
        self.positions = smoothed_positions