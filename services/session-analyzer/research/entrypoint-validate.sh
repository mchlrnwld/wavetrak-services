#!/usr/bin/env bash
set -xe

apachectl start

exec "$@"
