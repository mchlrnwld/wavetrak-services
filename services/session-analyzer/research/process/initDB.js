import mongoose from 'mongoose';

mongoose.Promise = global.Promise;

export default () => {
  const connectionString = process.env.MONGO_CONNECTION_STRING_SESSIONS;
  const mongoDbConfig = {
    poolSize: 50,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  };
  return new Promise((resolve, reject) => {
    try {
      console.debug(`Mongo ConnectionString: ${connectionString} `);

      mongoose.connect(connectionString, mongoDbConfig);
      mongoose.connection.once('open', () => {
        console.info(`MongoDB connected on ${connectionString}`);
        resolve();
      });
      mongoose.connection.on('error', (error) => {
        console.error({
          action: 'MongoDB:ConnectionError',
          error,
        });
        reject(error);
      });
    } catch (error) {
      console.error({
        message: 'MongoDB:ConnectionError',
        stack: error,
      });
      reject(error);
    }
  });
};
