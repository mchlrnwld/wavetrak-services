/* eslint-disable no-await-in-loop */
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import AWS from 'aws-sdk';
import fs from 'fs';
import initDB from './initDB';

dotenv.config();

// Stub the schema so we can query with mongoose
const { Schema } = mongoose;
const SessionsSchema = new Schema({}, { strict: false });
const Sessions = mongoose.model('Sessions', SessionsSchema, 'sessions');

const getSessionEvents = async (sessionId) => {
  const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  });

  const s3Params = {
    Bucket: process.env.SESSIONS_EVENTS_BUCKET,
    Key: `${sessionId}.json`,
  };

  try {
    const response = await s3.getObject(s3Params).promise();
    return JSON.parse(response.Body);
  } catch (err) {
    throw new Error(`Could not retrieve session events for session ${sessionId} from S3`);
  }
};

const sessionIds = process.argv[2].split(',');

const retrieveSessions = async () => {
  await initDB();

  for (let i = 0; i < sessionIds.length; i += 1) {
    const sessionId = sessionIds[i];
    console.log(`processing session: ${sessionId}`);
    const session = await Sessions.findOne(
      { _id: mongoose.Types.ObjectId(sessionId) },
      { client: true },
    )
      .lean()
      .exec();
    const events = await getSessionEvents(sessionId);

    const waves = events
      .filter((event) => event.type === 'WAVE' && event.outcome === 'CAUGHT')
      .map((event) => ({
        start: new Date(event.startTimestamp).getTime() / 1000,
        end: new Date(event.endTimestamp).getTime() / 1000,
      }));

    const positions = events
      .map((event) =>
        event.positions.map((position) => ({
          timestamp: new Date(position.timestamp).getTime() / 1000,
          latitude: position.location.coordinates[1],
          longitude: position.location.coordinates[0],
        })),
      )
      .flat();

    const clientId = session.client?.toString();
    let clientName;
    switch (clientId) {
      case '5b764335b41cae367d6609cb':
        clientName = 'dp';
        break;
      case '5b0cc082b5acfa365f447b6a':
        clientName = 'rc';
        break;
      case '5c6f00c9f0b6cbda76bc6c40':
        clientName = 'sl';
        break;
      default:
        clientName = 'unknown';
      // do nothing
    }

    fs.writeFileSync(
      `../validate/data/${sessionId}-${clientName}-positions.json`,
      JSON.stringify({ positions }, null, 2),
    );
    fs.writeFileSync(
      `../validate/data/${sessionId}-${clientName}-waves.json`,
      JSON.stringify(waves, null, 2),
    );
    console.log(`Addded ${sessionId}`);
  }
};

retrieveSessions()
  .then(() => process.exit(0))
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });
