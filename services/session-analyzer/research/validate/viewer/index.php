<?php
  // Use results file as the "DB".
  $contents = file_get_contents('results/summary.json');
  if (!$contents) {
    throw new Exception('No results summary file found, run validation first.');
  }
  $results_summary = json_decode($contents, true);
  $current_key = isset($_GET['key']) ? $_GET['key'] : null;
  $current_result_index = !is_null($current_key) ? array_search($current_key, array_column($results_summary, 'key')) : false;
  if ($current_result_index !== false) {
    $current_result = $results_summary[$current_result_index];
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset='utf-8' />
  <title>Display a map</title>
  <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.0.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.0.0/mapbox-gl.css' rel='stylesheet' />
  <script src='//d3js.org/d3.v3.min.js' charset='utf-8'></script>
  <script src="https://code.highcharts.com/highcharts.src.js"></script>
  <style>
    body { margin:0; padding:0; }
    nav {
      height: 100vh;
    }
    .sidebar .nav-link {
      font-weight: 500;
      color: #333;
      font-size: 12px;
      padding: 5px 0;
    }
    .sidebar .nav-link.active {
      color: #007bff;
    }
    .map-container {
      height: 50vh;
    }
    .map {
      position:absolute;
      top:0;
      bottom:0;
      width:100%;
    }
    img {
      width: 100%;
    }
    #chart, .model-img {
      width: 100%;
      height: 50vh - 30px;
    }
  </style>
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <nav class="col-md-2 d-none d-md-block bg-light sidebar">
        <div class="sidebar-sticky">
          <ul class="nav flex-column">
            <li class="nav-item"><a class="nav-link" href="/">Summary</a></li>
            <?php
              foreach ($results_summary as $result) {
                echo('<li class="nav-item"><a class="nav-link'.($result['key'] == $current_key ? ' active' : '').'" href="?key='.$result['key'].'">'.$result['key'].'</a></li>');
              }
            ?>
          </ul>
        </div>
      </nav>
      <main class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <?php
          if (!is_null($current_key)) {
        ?>
          <div class="row">
            <div class="col-lg">
              <h4><?php echo($current_result['key']); ?> - Source: <?php echo($current_result['source_wave_count']); ?> SL: <?php echo($current_result['sl_wave_count']); ?></h4>
            </div>
          </div>
          <div class="row">
            <div class="col-lg">
                <div id="chart"></div>
            </div>
            <div class="col-lg">
              <img class="model-img" src="results/<?php echo($current_result['key']); ?>-model.png" />
            </div>
          </div>
          <div class="row">
            <div class="col-sm map-container">
              <div id='source_map' class="map"></div>
            </div>
            <div class="col-sm map-container">
              <div id='sl_map' class="map"></div>
            </div>
          </div>
        <?php
          } else {
        ?>
          <div class="row">
            <div class="col-lg">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Source Wave Count</th>
                    <th scope="col">SL Wave Count</th>
                    <th scope="col">Delta</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    foreach ($results_summary as $result) {
                      ?>
                        <tr>
                          <th scope="row"><a href="?key=<?php echo($result['key']); ?>"><?php echo($result['key']); ?></a></th>
                          <td><?php echo($result['source_wave_count']); ?></td>
                          <td><?php echo($result['sl_wave_count']); ?></td>
                          <td><?php echo(round($result['sl_wave_count'] / $result['source_wave_count'], 2)); ?></td>
                        </tr>
                      <?php
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        <?php
          }
        ?>
      </main>
    </div>
  </div>


  <script>
    function moveToMapPosition (master, clones) {
      var center = master.getCenter();
      var zoom = master.getZoom();
      var bearing = master.getBearing();
      var pitch = master.getPitch();
      clones.forEach(function (clone) {
        clone.jumpTo({
          center: center,
          zoom: zoom,
          bearing: bearing,
          pitch: pitch
        });
      });
    }
    // Sync movements of two maps.
    //
    // All interactions that result in movement end up firing
    // a "move" event. The trick here, though, is to
    // ensure that movements don't cycle from one map
    // to the other and back again, because such a cycle
    // - could cause an infinite loop
    // - prematurely halts prolonged movements like
    //   double-click zooming, box-zooming, and flying
    function syncMaps () {
      var maps;
      var argLen = arguments.length;
      if (argLen === 1) {
        maps = arguments[0];
      } else {
        maps = [];
        for (var i = 0; i < argLen; i++) {
          maps.push(arguments[i]);
        }
      }
      // Create all the movement functions, because if they're created every time
      // they wouldn't be the same and couldn't be removed.
      var fns = [];
      maps.forEach(function (map, index) {
        fns[index] = sync.bind(null, map, maps.filter(function (o, i) { return i !== index; }));
      });
      function on () {
        maps.forEach(function (map, index) {
          map.on('move', fns[index]);
        });
      }
      function off () {
        maps.forEach(function (map, index) {
          map.off('move', fns[index]);
        });
      }
      // When one map moves, we turn off the movement listeners
      // on all the maps, move it, then turn the listeners on again
      function sync (master, clones) {
        off();
        moveToMapPosition(master, clones);
        on();
      }
      on();
      return function(){  off(); fns = []; };
    }
    var graph = false;
    function renderGraph(data, moveMapMarker) {
      var plotLines = [];
      data.waves.forEach(function (wave) {
        plotLines.push({
          color: 'green', // Color value
          value: wave.start,
          width: 1
        });
        plotLines.push({
          color: 'red', // Color value
          value: wave.end,
          width: 1
        });
      });
      var chart = Highcharts.chart('chart', {
        chart: {
          zoomType: 'x'
        },
        yAxis: {
            title: {
                text: 'Speed (m/s)'
            },
            plotLines: [{
                value: data.threshold,
                color: 'green',
                width: 1,
                label: {
                    text: 'Speed Threshold '+(Math.round(data.threshold * 100) / 100)
                }
            }]
        },
        tooltip: {
          formatter: function () {
            var that = this;
            var point = data.positions.find((pos) => pos.timestamp == that.x);
            moveMapMarker(point);
            return (Math.round(this.y * 100) / 100)+' m/s <br>'+point.timestamp;
          }
        },
        xAxis: {
          title: {
            enabled: null
          },
          labels: {
            enabled: false
          },
          tickWidth: 0,
          plotLines: plotLines,
          min: data.positions[0].timestamp,
          max: data.positions[data.positions.length - 1].timestamp,
          crosshair: true
        },
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2010
            }
        },
        series: [{
          name: 'Speed',
          data: data.speeds,
          type: 'line',
          lineWidth: 1,
          states: {
            hover: {
              lineWidthPlus: 0
            }
          }
        }]
      });
      return chart;
    }

    var marker = {"geometry": {"type": "Point", "coordinates": []}, "type": "Feature", "properties": {}};

    function renderTracks(type, data, map) {
      // We use D3 to fetch the JSON here so that we can parse and use it separately
      // from GL JS's use in the added source. You can use any request method (library
      // or otherwise) that you want.

      // save full coordinate list for later
      data = data[type+'_geojson'];
      coordinates = data.features[0].geometry.coordinates

      // add it to the map
      map.addSource(type+'trace', { type: 'geojson', data: data });
      map.addLayer({
        "id": type+"_trace",
        "type": "line",
        "source": type+"trace",
        "paint": {
          "line-color": ['get', 'color'],
          "line-opacity": ['get', 'opacity'],
          "line-width": 5
        }
      });
      // setup the viewport
      map.jumpTo({ 'center': coordinates[0], 'zoom': 16 });
      map.setPitch(30);
      // Pass the first coordinates in the LineString to `lngLatBounds` &
      // wrap each coordinate pair in `extend` to include them in the bounds
      // result. A variation of this technique could be applied to zooming
      // to the bounds of multiple Points or Polygon geomteries - it just
      // requires wrapping all the coordinates with the extend method.
      var bounds = coordinates.reduce(function(bounds, coord) {
        return bounds.extend(coord);
      }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));
      map.fitBounds(bounds, {
        padding: 20
      });
    }
    // Draws a line 2km along the beach angle.
    function drawBeachAngle(map, beachAngle, coordinates) {
      var map_marker = new mapboxgl.Marker();
      map_marker.setLngLat(coordinates);
      map_marker.addTo(map);

      function getPointInDistanceOnBearing(lon1, lat1, brng, d) {
        // https://stackoverflow.com/questions/7222382/get-lat-long-given-current-point-distance-and-bearing
        var R = 6378.1;
        // Convert to radians.
        brng = brng * Math.PI / 180;
        lat1 = lat1 * Math.PI / 180;
        lon1 = lon1 * Math.PI / 180;
        var lat2 = Math.asin( Math.sin(lat1)*Math.cos(d/R) + Math.cos(lat1)*Math.sin(d/R)*Math.cos(brng));
        var lon2 = lon1 + Math.atan2(Math.sin(brng)*Math.sin(d/R)*Math.cos(lat1), Math.cos(d/R)-Math.sin(lat1)*Math.sin(lat2));
        lat2 = lat2 * (180 / Math.PI);
        lon2 = lon2 * (180 / Math.PI);
        // lon, lat
        return [lon2, lat2];
      }

      var start_point = getPointInDistanceOnBearing(coordinates[0], coordinates[1], beachAngle  - 90, 1);
      var end_point = getPointInDistanceOnBearing(coordinates[0], coordinates[1], beachAngle + 90, 1);

      map.addLayer({
        "id": "route",
        "type": "line",
        "source": {
          "type": "geojson",
          "data": {
            "type": "Feature",
            "properties": {},
            "geometry": {
              "type": "LineString",
              "coordinates": [
                start_point,
                end_point
              ]
            }
          }
        },
        "paint": {
          "line-color": "green",
          "line-width": 2
        }
      });

      function addWavePositionCourse(id, color, lon, lat, course) {
        var wave_end_point = getPointInDistanceOnBearing(lon, lat, course, 1);

        map.addLayer({
          "id": "wave_course_"+id,
          "type": "line",
          "source": {
            "type": "geojson",
            "data": {
              "type": "Feature",
              "properties": {},
              "geometry": {
                "type": "LineString",
                "coordinates": [
                  [lon, lat],
                  wave_end_point
                ]
              }
            }
          },
          "paint": {
            "line-color": color,
            "line-width": 2
          }
        });
      }

      //addWavePositionCourse(1, "green", -117.28273953456282,33.015143545255725,181.5783198216852);
    }

    d3.json('/results/<?php echo($current_key); ?>-graph.json', function(err, graph_data) {
      d3.json('/geojson.php?key=<?php echo($current_key); ?>', function(err, data) {
        if (err) throw err;

        mapboxgl.accessToken = 'pk.eyJ1Ijoic3VyZmxpbmUiLCJhIjoiY2poeDFnOGVqMDcxMzNrbWh6NmJ6cmR2dyJ9.FyL2SCk63viBSzZVY-AXjg';

        const first_source_point = data.source_geojson.features[0].geometry.coordinates[0]
        const newSourcePoint = graph_data.new_first_position;
        const newSpotCoordinates = graph_data.new_spot.coordinates
        const oldSpotCoordinates = graph_data.spot.coordinates
        const oldFirstPosition = graph_data.first_position;
        const newFirstPosition = graph_data.new_first_position;

        const map = new mapboxgl.Map({
          container: 'source_map', // container id
          style: 'mapbox://styles/mapbox/satellite-v9', // stylesheet location
          center: first_source_point, // starting position [lng, lat]
          zoom: 9 // starting zoom
        });

        map.on('load', function () {
          renderTracks('source', data, map);
        });

        const map_marker = new mapboxgl.Marker({color: 'green'});
        map_marker.setLngLat(first_position);
        map_marker.addTo(map);

        const oldSpotMarker = new mapboxgl.Marker({color: 'red'});
        oldSpotMarker.setLngLat(oldSpotCoordinates)
        oldSpotMarker.addTo(map)

        first_sl_point = data.sl_geojson.features[0].geometry.coordinates[0]
        var sl_map = new mapboxgl.Map({
          container: 'sl_map', // container id
          style: 'mapbox://styles/mapbox/satellite-v9', // stylesheet location
          center: first_sl_point, // starting position [lng, lat]
          zoom: 9 // starting zoom
        });

        sl_map.on('load', function () {
          renderTracks('sl', data, sl_map);

          // Draw beach angle on the algo map.
          if (graph_data.spot.beach_angle) {
            drawBeachAngle(sl_map, graph_data.spot.beach_angle, graph_data.spot.coordinates)
          }
        });

        const sl_marker = new mapboxgl.Marker({ color: 'black'});
        sl_marker.setLngLat(first_sl_point);
        sl_marker.addTo(sl_map);

        const newSpotMarker = new mapboxgl.Marker({ color: 'yellow'});
        newSpotMarker.setLngLat(newSpotCoordinates)
        newSpotMarker.addTo(sl_map)

        const newFirstPosMarker = new mapboxgl.Marker({ color: 'green'});
        newFirstPosMarker.setLngLat(newFirstPosition)
        newFirstPosMarker.addTo(sl_map)

        syncMaps(map, sl_map);

        const moveMapMarker = function (point) {

          var position = [point.longitude, point.latitude];
          map_marker.setLngLat(position);
          sl_marker.setLngLat(position);

          map.jumpTo({
            center: position
          });

          sl_map.jumpTo({
            center: position
          });
        }

        graph = renderGraph(graph_data, moveMapMarker);
      });
    });
  </script>

</body>
</html>
