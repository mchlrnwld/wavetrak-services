<?php
  if (!isset($_GET['key'])) {
    header('Content-type: application/json');
    echo(json_encode(array("error" => "Requires key param.")));
  }
  $key = $_GET['key'];
  $source_positions = @file_get_contents('../data/'.$key.'-positions.json');
  if (!$source_positions) {
    header('Content-type: application/json');
    echo(json_encode(array("error" => "No source files found for key: ".$key.".")));
  }
  $all_positions = json_decode($source_positions, true);
  $waves = json_decode(file_get_contents('../data/'.$key.'-waves.json'), true);
  $sl_data = @file_get_contents('../viewer/results/'.$key.'-sl-events.json');
  if ($sl_data) {
    $sl = json_decode($sl_data, true);
  }
  // ==== All positions.
  $coords = array();
  usort($all_positions['positions'], function($a, $b) {
    return $a['timestamp'] <=> $b['timestamp'];
  });
  foreach ($all_positions['positions'] as $p) {
    $coords[] = array(
      $p['longitude'],
      $p['latitude']
    );
  }
  // ==== Source wave features
  $wave_features = array();
  foreach ($waves as $w) {
    $tmp = array(
      "type" => "Feature",
      "properties" => array(
        "color" => "#F7455D",
        "opacity" => 0.8
      ),
      "geometry" => array(
        "type" => "LineString",
        "coordinates" => array()
      )
    );
    foreach ($all_positions['positions'] as $p) {
      if ($p['timestamp'] > $w['start'] && $p['timestamp'] < $w['end']) {
        $tmp['geometry']['coordinates'][] = array(
          $p['longitude'],
          $p['latitude']
        );
      }
    }
    $wave_features[] = $tmp;
  }
  // ==== SL track
  $sl_features = array();
  if (isset($sl)) {
    foreach ($sl as $event) {
      $tmp = array(
        "type" => "Feature",
        "properties" => array(
          "color" => $event['type'] == 'PADDLE' ? "#33C9EB" : "#F7455D",
          "opacity" => $event['type'] == 'PADDLE' ? 0.4 : 0.8
        ),
        "geometry" => array(
          "type" => "LineString",
          "coordinates" => array()
        )
      );
  
      foreach ($event['positions'] as $p) {
        $tmp['geometry']['coordinates'][] = array(
          $p['longitude'],
          $p['latitude']
        );
      }
  
      $sl_features[] = $tmp;
    }
  }
  $output = array(
    "source_geojson" => array(
      "type" => "FeatureCollection",
      "features" => array_values(array_merge(array(array(
        "type" => "Feature",
        "properties" => array(
          "color" => "#33C9EB",
          "opacity" => 0.4
        ),
        "geometry" => array(
          "type" => "LineString",
          "coordinates" => $coords
        )
      )), $wave_features))
    ),
    "sl_geojson" => array(
      "type" => "FeatureCollection",
      "features" => $sl_features
    ),
    "source_points" => array()
  );
  header('Content-type: application/json');
  echo(json_encode($output));
?>
