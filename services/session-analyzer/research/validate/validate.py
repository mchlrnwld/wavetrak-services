"""
Validate the wave classifiers output in comparisom to other commercial solutions
"""

import json
import os
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError
from lib.utils import flip_angle, get_transition_speed_model
from lib.WaveClassifier import WaveClassifier
import numpy as np
import math
import re
import argparse
from research.timer import start as timer_start, checkpoint as timer_checkpoint

import matplotlib.pyplot as plt

ROOT_DIR = os.path.dirname(os.path.realpath(__file__))
SOURCE_DIR = f'{ROOT_DIR}/data'
RESULTS_DIR = f'{ROOT_DIR}/viewer/results'

spots_api = f'http://10.12.66.108'


def compare_tagged_spots(
    original_spot_details, original_lat_lon, new_spot_details, new_lat_lon
):
    """
    Given two spot details and the lat lon that was used in the nearby
    spots api this function prints out some information about each spot
    """

    original_spot_name = original_spot_details["name"]
    new_spot_name = new_spot_details["name"]
    new_lat = new_lat_lon['latitude']
    new_lon = new_lat_lon['longitude']
    original_lat = original_lat_lon["latitude"]
    original_lon = original_lat_lon["longitude"]

    print(
        f'Original spot tagged at: {original_spot_name} using lat: {original_lat} and lon: {original_lon}'
    )
    print(
        f'New spot tagged at: {new_spot_name} using lat: {new_lat} and lon: {new_lon}'
    )

    return


def get_gps_centroid(positions):
    """
    Given an array of position with 'latitude' and 'longitude' properties
    this functions returns a centroid of the gps positions. First we convert
    each point to radians, then a cartesian coordinates. We take the sum of the
    cartesian coordinates, return an average, and then convert that back to
    degrees.
    """
    x = 0.0
    y = 0.0
    z = 0.0

    total_coords = len(positions)

    for pos in positions:
        longitude = pos['longitude']
        latitude = pos['latitude']
        longitude_radian = np.radians(longitude)
        latitude_radian = np.radians(latitude)

        x += math.cos(latitude_radian) * math.cos(longitude_radian)
        y += math.cos(latitude_radian) * math.sin(longitude_radian)
        z += math.sin(latitude_radian)

    avg_x = x / total_coords
    avg_y = y / total_coords
    avg_z = z / total_coords

    centralLongitude = math.atan2(avg_y, avg_x)
    centralSquareRoot = math.sqrt(avg_x * avg_x + avg_y * avg_y)
    centralLatitude = math.atan2(avg_z, centralSquareRoot)

    return {
        'latitude': np.rad2deg(centralLatitude),
        'longitude': np.rad2deg(centralLongitude),
    }


def get_spot_details(lat, lon):
    url = f'http://spots-api.prod.surfline.com/admin/spots/?filter=nearby&distance=3&coordinates={lon}&coordinates={lat}&limit=10&select=offshoreDirection,location,name'
    req = Request(url)

    try:
        response = urlopen(req, data=None, timeout=5)
    except HTTPError as e:
        print(f'spots_api server returned HTTP code: {e.code}.')
    except URLError as e:
        print('We failed to reach a server.')
        print(f'Reason: {e.reason}')
    else:
        content = response.read()
        data = json.loads(content.decode('utf-8'))

        if len(data) > 0 and '_id' in data[0]:
            return data[0]

    return False


def validate_data(key):
    with open(f'{SOURCE_DIR}/{key}-waves.json') as f:
        truth = json.load(f)
        rc_wave_starts = []
        rc_wave_ends = []
        for event in truth:
            rc_wave_starts.append(event['start'])
            rc_wave_ends.append(event['end'])

    with open(f'{SOURCE_DIR}/{key}-positions.json') as f:
        data = json.load(f)

        first_position = first_position = data['positions'][0]
        new_first_position = get_gps_centroid(data['positions'])
        new_longitude = new_first_position["longitude"]
        new_latitude = new_first_position["latitude"]
        old_longitude = first_position["longitude"]
        old_latitude = first_position["latitude"]

        print(f'Current start: lon = {old_longitude} lat = {old_latitude}')
        print(f'New start: lon = {new_longitude} lat = {new_latitude}')

        spot_details = get_spot_details(
            first_position['latitude'], first_position['longitude']
        )
        new_spot_details = get_spot_details(new_latitude, new_longitude)
        compare_tagged_spots(
            spot_details, first_position, new_spot_details, new_first_position
        )

        beach_angle = False
        spot_coordinates = None
        if spot_details:
            if (
                'offshoreDirection' in spot_details
                and spot_details['offshoreDirection'] is not None
            ):
                beach_angle = flip_angle(spot_details['offshoreDirection'])
            if (
                'location' in spot_details
                and 'coordinates' in spot_details['location']
            ):
                spot_coordinates = spot_details['location']['coordinates']

        if new_spot_details:
            if (
                'location' in new_spot_details
                and 'coordinates' in new_spot_details['location']
            ):
                new_spot_coordinates = new_spot_details['location'][
                    'coordinates'
                ]

        if beach_angle and spot_coordinates:
            print(
                f'Beach angle: {beach_angle} at {spot_coordinates[1]}, {spot_coordinates[0]}'
            )

        print(
            f'New spot coordinates: {new_spot_coordinates[1]}, {new_spot_coordinates[0]}'
        )

        start = timer_start()
        classifier = WaveClassifier(data["positions"], beach_angle)
        events = classifier.get_events()
        timer_checkpoint("Cacl events", start)

        # Save events to results dir.
        with open(f'{RESULTS_DIR}/{key}-sl-events.json', 'w') as outfile:
            json.dump(events, outfile)

        waves = []

        sl_wave_starts = []
        sl_wave_ends = []
        for event in events:
            if event['type'] == "WAVE":
                waves.append(
                    {
                        'start': event['positions'][0]['timestamp'],
                        'end': event['positions'][-1]['timestamp'],
                    }
                )
                sl_wave_starts.append(event['positions'][0]['timestamp'])
                sl_wave_ends.append(event['positions'][-1]['timestamp'])

        points = classifier.points

        speeds = [[x['timestamp'], x['speed']] for x in points]
        timestamps = [x['timestamp'] for x in points]
        courses = [x['course'] for x in points]
        window = np.array(
            [classifier.within_window(x) for x in courses]
        ).astype(int)

        graph_data = {
            'positions': classifier.points,
            'waves': waves,
            'speeds': speeds,
            'threshold': classifier.transition_speed,
            'spot': {
                'beach_angle': beach_angle,
                'coordinates': spot_coordinates,
            },
            'new_spot': {'coordinates': new_spot_coordinates},
            'first_position': {'lon': old_longitude, 'lat': old_latitude},
            'new_first_position': {'lon': new_longitude, 'lat': new_latitude},
        }

        with open(f'{RESULTS_DIR}/{key}-graph.json', 'w') as outfile:
            json.dump(graph_data, outfile)
            print('Written graph file.')

        plt.figure(figsize=(25, 6))
        plt.plot(timestamps, speeds, label="speed")
        plt.plot(
            [timestamps[0], timestamps[-1]],
            [classifier.transition_speed, classifier.transition_speed],
            label="Threshold ("
            + str(round(classifier.transition_speed, 2))
            + ")",
        )
        plt.scatter(
            rc_wave_starts,
            np.zeros(len(rc_wave_starts)) + 5,
            label="Source Wave Start",
        )
        plt.scatter(
            rc_wave_ends,
            np.zeros(len(rc_wave_ends)) + 5,
            label="Source Wave End",
        )
        plt.scatter(
            sl_wave_starts,
            np.zeros(len(sl_wave_starts)) + 4,
            label="New Algo Wave Start",
        )
        plt.scatter(
            sl_wave_ends,
            np.zeros(len(sl_wave_ends)) + 4,
            label="New Algo Wave End",
        )
        plt.scatter(timestamps, window, label="In Window", s=1)
        plt.legend()
        plt.savefig(f'{RESULTS_DIR}/{key}.png')

        # Graph model output for this session.
        model_results = get_transition_speed_model(
            classifier.points_within_threshold
        )

        if model_results:
            plt.figure(figsize=(12, 8))
            plt.plot(model_results['y'], label="Y")
            plt.plot(model_results['y_pred'], label="Y Pred")
            plt.plot(
                np.zeros([len(model_results['y'])])
                + model_results['inflection'],
                label="Inflection",
            )
            plt.legend()
            plt.savefig(f'{RESULTS_DIR}/{key}-model.png')

    # Metrics
    source_waves = len(truth)
    sl_waves = len([d for d in events if d["type"] == "WAVE"])
    print(f'Source wave count: {source_waves}, sl wave count: {sl_waves}')

    return {'source_wave_count': source_waves, 'sl_wave_count': sl_waves}


def get_source_data():
    keys = []

    # r=root, d=directories, f = files
    for r, d, f in os.walk(SOURCE_DIR):
        for file in f:
            m = re.search('([0-9a-zA-Z]+)-([a-z]+)-', file)
            if m:
                keys.append(f'{m.group(1)}-{m.group(2)}')

    # list/set makes the array unique.
    return list(set(keys))


def main():
    parser = argparse.ArgumentParser(description='Run validation.')
    parser.add_argument('--key', type=str, default=None, help='Key to load')

    args = parser.parse_args()

    if args.key is None:
        keys = get_source_data()
    else:
        keys = [args.key]

    results = []

    for key in keys:
        print(f'Validating: {key}')
        r = validate_data(key)
        r['key'] = key
        results.append(r)

    with open(f'{RESULTS_DIR}/summary.json', 'w') as outfile:
        json.dump(results, outfile)
        print('Written results file.')


if __name__ == '__main__':
    main()
