provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "session-analyzer/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "session_analyzer" {
  source = "../../"

  company     = "sl"
  application = "session-analyzer"
  environment = "staging"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-staging"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "session-analyzer.staging.surfline.com"
    },
  ]

  alb_listener_arn  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678/1b066cae0efbb0a4"
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678"

  dns_name    = "session-analyzer.staging.surfline.com"
  dns_zone_id = "Z3JHKQ8ELQG5UE"

  auto_scaling_enabled = false
}
