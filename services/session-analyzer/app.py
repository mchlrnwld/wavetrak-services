# NewRelic has to happen before other imports to properly instrument everything
import newrelic.agent
from lib import config  # noqa: E402
newrelic.agent.initialize('./newrelic.ini', config.ENV)
# DO NOT MOVE ABOVE IMPORTS

import logging  # noqa: E402
import os  # noqa: E402
from flask import Flask, request  # noqa: E402
from flask_restful import Resource, Api  # noqa: E402
from flask_jsonpify import jsonify  # noqa: E402
from lib.Session import Session as SessionHandler  # noqa: E402


# For logging at AWS.
root = logging.getLogger()
if root.handlers:
    for handler in root.handlers:
        root.removeHandler(handler)

logging.basicConfig(level=config.LOG_LEVEL)
logger = logging.getLogger("session-analyzer")

app = Flask(__name__)
api = Api(app)


class HealthCheck(Resource):
    def get(self):
        newrelic.agent.ignore_transaction(flag=True)
        return jsonify(status='OK')


class Session(Resource):
    def __init__(self):
        self.session_handler = SessionHandler()

    def post(self):
        response = self.session_handler.analyze(request)
        return response


api.add_resource(HealthCheck, '/health')
api.add_resource(Session, '/analyze')

if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    app.run(host='0.0.0.0', port=os.environ['EXPRESS_PORT'])
