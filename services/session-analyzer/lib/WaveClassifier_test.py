import json
import os
import unittest

from lib.WaveClassifier import WaveClassifier


class WaveClassifierTestCase(unittest.TestCase):
    def setUp(self):
        with open("./fixtures/data_test.json") as f:
            data = json.load(f)
            self.classifier = WaveClassifier(data["positions"], 22)

    def test_within_window(self):
        in_window = self.classifier.within_window(179.001440)
        self.assertEqual(False, in_window)
        in_window = self.classifier.within_window(293)
        self.assertEqual(True, in_window)

    def test_no_threshold(self):
        with open("./fixtures/data_test_broken.json") as f:
            data = json.load(f)
            classifier = WaveClassifier(data["positions"], 22)
            events = classifier.get_events()

            self.assertEqual(1, len(events))

    # This test ensures that a valid session that was previously synced
    # properly creates 11 events using the wave classifier
    def test_real_surf_session(self):
        with open("./fixtures/data_test_1.json") as f:
            data = json.load(f)
            classifier = WaveClassifier(data["positions"], 31)
            events = classifier.get_events()

            self.assertEqual(27, len(events))

    # This test ensures that a valid session that was previously synced
    # properly creates 13 events using the wave classifier
    def test_real_surf_session_2(self):
        with open("./fixtures/data_test_2.json") as f:
            data = json.load(f)
            classifier = WaveClassifier(data["positions"], 31)
            events = classifier.get_events()

            self.assertEqual(29, len(events))

    # When we have short sessions, sometimes PyEarth is not able to create
    # a basis for the model. In this case we want to ensure we fallthrough
    # and return one paddle event. This test ensures that we achieve this
    # result
    def test_short_session(self):
        with open("./fixtures/data_py_earth_error.json") as f:
            data = json.load(f)
            classifier = WaveClassifier(data["positions"], 31)
            events = classifier.get_events()

            self.assertEqual(1, len(events))

    # When we have a session with a first timestamp that is too far away
    # from the second position timestamp, we want to discard the first
    # position
    def test_bad_first_timestamp_session(self):
        with open("./fixtures/data_bad_first_timestamp.json") as f:
            data = json.load(f)
            classifier = WaveClassifier(data["positions"], 31)
            events = classifier.get_events()

            self.assertEqual(1, len(events))
            self.assertEqual(1, len(events[0]["positions"]))


if __name__ == "__main__":
    unittest.main()
