import unittest
import json

from lib.utils import get_distance, get_course


class UtilsTestCase(unittest.TestCase):
    def test_get_distance(self):
        distance = get_distance(50.317242, -4.083272, 50.317243, -4.083271)
        self.assertEqual(0.13196753769027317, distance)

    def test_get_course(self):
        course = get_course(33.01510642813438, -117.28337663803372,
                            33.01510263947027, -117.28333247567137)
        self.assertEqual(95.84157626659089, course)

        course = get_course(33.015138207534676, -117.28273970995946,
                            33.01513775505277, -117.28275215594915)
        self.assertEqual(267.5174097075126, course)
