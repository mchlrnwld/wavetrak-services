""" Utils methods module """

from functools import reduce
import json
import logging
import math
import requests
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError

import numpy as np
import pyearth

import lib.config as config

# For logging at AWS.
root = logging.getLogger()
if root.handlers:
    for handler in root.handlers:
        root.removeHandler(handler)

logging.basicConfig(level=config.LOG_LEVEL)
logger = logging.getLogger("session-analyzer")


def flip_angle(angle):
    angle = angle + 180

    return angle % 360


def get_distance(latitude_a, longitude_a, latitude_b, longitude_b):
    """
    Returns the distance between two geopoints in Meters

    Uses the haversine formula
    (https://en.wikipedia.org/wiki/Haversine_formula)
    """

    earth_radius = 6372.8

    lat1 = math.radians(latitude_a)
    lon1 = math.radians(longitude_a)
    lat2 = math.radians(latitude_b)
    lon2 = math.radians(longitude_b)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = (
        math.sin(dlat / 2) ** 2 +
        math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2) ** 2
    )
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    return earth_radius * c * 1000


def get_course(lat1, lon1, lat2, lon2):
    lat1 = math.radians(lat1)
    lat2 = math.radians(lat2)
    diffLong = math.radians(lon2 - lon1)

    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(lat1) * math.sin(lat2) - (
        math.sin(lat1) * math.cos(lat2) * math.cos(diffLong)
    )

    initial_bearing = math.atan2(x, y)

    # Now we have the initial bearing but math.atan2 return values
    # from -180° to + 180° which is not what we want for a compass bearing
    # The solution is to normalize the initial bearing as shown below
    initial_bearing = math.degrees(initial_bearing)
    return (initial_bearing + 360) % 360


def get_transition_speed_model(points_within_threshold):
    """
    Using the MARS model we want to find inflection points which we believe
    will denote a user riding a wave and use this inflection point speed
    as the threshold for wave events.
    Args:
    points_within_threshold -- List of points within high and low speed limits
    """
    x = np.arange(0, len(points_within_threshold), 1)
    x = np.reshape(x, [len(x), 1])
    y = sorted([x["speed"] for x in points_within_threshold])

    model = pyearth.Earth(max_terms=5)

    if len(x) > 0 and len(y) > 0:
        model.fit(x, y)

        if model.basis_ is not None and len(model.basis_) > 1:
            thing = str(model.basis_[1]).split("-")
            if len(thing) > 1:
                inflection = int(
                    str(model.basis_[1]).split("-")[1].split(")")[0]
                )
                y_pred = model.predict(x)

                return {
                    'model': model,
                    'y': y,
                    'y_pred': y_pred,
                    'inflection': y[inflection]
                }

    return False


def get_transition_speed(points_within_threshold):
    """
    Uses get_transition_speed_model and returns only the transition speed.

    Args:
        points_within_threshold -- List of points within high and
        low speed limits.
    """
    results = get_transition_speed_model(points_within_threshold)

    if results and 'inflection' in results:
        return results['inflection']

    return False


def get_beach_angle(lat, lon):
    try:
        params = {
            "filter": "nearby",
            "distance": 3,
            "coordinates": [lon, lat],
            "limit": 10,
            "select": "offshoreDirection",
        }
        host = f"{config.SPOTS_API}/admin/spots/"
        response = requests.get(host, params=params)
        # If the response was successful, no Exception will be raised
        response.raise_for_status()
    except HTTPError as http_err:
        logger.error(f"spots_api server returned HTTP code: {http_err.code}.")
    except URLError as e:
        logger.error("We failed to reach a server.")
        logger.error(f"Reason: {e.reason}")
    else:
        content = response.content
        data = json.loads(content.decode("utf-8"))

        if len(data) > 0 and "_id" in data[0]:
            spot_id = data[0]["_id"]
            has_offshore_direction = "offshoreDirection" in data[0]
            offshore_direction = None
            if has_offshore_direction:
                offshore_direction = data[0]["offshoreDirection"]
            if has_offshore_direction and offshore_direction is not None:
                return flip_angle(offshore_direction)

    return False
