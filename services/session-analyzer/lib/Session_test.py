import unittest
import json

from lib.Session import Session


class SessionTestCase(unittest.TestCase):
    def test_validate_session_data(self):
        """
          Validate we will approve and fail a min of 10s of data.
        """
        with open("./fixtures/data_test.json") as f:
            data = json.load(f)
            new_data = {
                'positions': [
                    data['positions'][0],
                    data['positions'][-1]
                ],
                'gpsCenter': {
                    "latitude": 50.317242,
                    "longitude": -4.083272
                }
            }

            fail_data = {
                'positions': [
                    data['positions'][0]
                ],
                'gpsCenter': {
                    "latitude": 50.317242,
                    "longitude": -4.083272
                }
            }

            session = Session()

            # _Session__* allows us to call the private method.
            self.assertEqual(
                True, session._Session__validate_session_data(data))
            self.assertEqual(
                True, session._Session__validate_session_data(new_data))
            self.assertEqual(
                False, session._Session__validate_session_data(fail_data))
