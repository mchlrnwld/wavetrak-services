"""
Session Endpoint Class

This class comes with the necessary methods to analyze a session sent from
the apple watch app in the form of a list of position dicts containing
a timestamp, a latitude, a longitude, and a property denoting the gps
center of the session

ex: {
    "positions": [
        {
            "timestamp": 1409404954.0,
            "latitude": 50.317242,
            "longitude": -4.083272
        }
    ],
    "gpsCenter": {
        "latitude": -80,
        "longitude": 100
    }
}
"""
import newrelic.agent
from flask_jsonpify import jsonify
from functools import reduce
import json
import logging
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError

from lib import config
from lib.utils import get_beach_angle
from lib.WaveClassifier import WaveClassifier

# For logging at AWS.
root = logging.getLogger()
if root.handlers:
    for handler in root.handlers:
        root.removeHandler(handler)

logging.basicConfig(level=config.LOG_LEVEL)
logger = logging.getLogger("session-analyzer")


class Session:
    def __validate_session_data(self, data):
        """
            Given session data of the form list of position dicts with
            timestamps, latitudes, and longitudes this fucntion ensures
            that the data meets the minimum threshold for analysis.

            Args:
            data -- the session data

            Returns: Boolean
        """
        first = False
        last = False
        if "positions" in data and len(data["positions"]) > 1:
            first = data["positions"][0]
            last = data["positions"][-1]

        if first and last:
            if (
                "timestamp" in first and "latitude" in first
                and "longitude" in first and "timestamp" in last
                and "latitude" in last and "longitude" in last
                and last["timestamp"] - first["timestamp"] > 10.0
               ):
                return True

        return False

    def build_session_object(self, data):
        """
            Given some session data, of the form mentioned above, this function
            runs the data through the wave classifier, and then uses the events
            returned to create a session object ready to be sent back to the
            sessions api.

            Args:
            data -- the session data

            Returns: dict
            {
                'startTimestamp': int (unix ms),
                'endTimestamp': int (unix ms),
                'visibility': 'PUBLIC'|'PRIVATE',
                'waveCount': int,
                'speedMax': float,
                'distancePaddled': float,
                'distanceWaves': float,
                'distanceLongestWave': float,
                'deviceType': string,
                'events': Dict[]
            }
        """
        # Determine the spot this session took place at, then get the beach
        # angle (we'll need that for the detection).
        gps_center = data["gpsCenter"]
        latitude = gps_center["latitude"]
        longitude = gps_center["longitude"]

        # Classification still works without a beach angle but it'll be
        # less accurate, we don't have every spot on earth so we'd rather
        # return data than not. We think the data returned will be reasonable
        # without beach angle.
        beach_angle = get_beach_angle(latitude, longitude)

        # Instantiate the wave classifier class for use in analysis
        classifier = WaveClassifier(data["positions"], beach_angle)

        events = classifier.get_events()

        only_waves = [d for d in events if d["type"] == "WAVE"]
        only_paddles = [d for d in events if d["type"] == "PADDLE"]

        # Reduce position properties to just what the API accepts.
        reduced_events = [
            dict(
                event,
                **{
                    "positions": [
                        {
                            "timestamp": p["timestamp"],
                            "latitude": p["latitude"],
                            "longitude": p["longitude"],
                            "horizontalAccuracy": p.get("horizontalAccuracy", None)  # noqa: E501
                        }
                        for p in event["positions"]
                    ]
                }
            )
            for event in events
        ]

        paddle_distances = [x["distance"] for x in only_paddles]
        wave_distances = [x["distance"] for x in only_waves]
        max_speeds = [x["speedMax"] for x in only_waves]

        session_object = {
            "startTimestamp": data["positions"][0]["timestamp"],
            "endTimestamp": data["positions"][-1]["timestamp"],
            "visibility": "PRIVATE",
            "waveCount": len(only_waves),
            "speedMax": max(max_speeds, default=0),
            "distancePaddled": sum(paddle_distances),
            "distanceWaves": sum(wave_distances),
            "distanceLongestWave": max(wave_distances, default=0),
            "deviceType": "Surfline Apple Watch",
            "events": reduced_events,
        }

        return session_object

    def analyze(self, request):
        """
            Given a request to the /analyze endpoint, this function
            validates that the session data meets the minimum threshold
            and then runs it through the classifier and creates the session
            object
        """
        data = request.get_json()

        if not data:
            error = jsonify(error=True, message="Unable to parse JSON.")
            error.status_code = 400
            newrelic.agent.record_exception(params=error)
            return error

        # === Log the data out for now for testing === #
        logger.info(json.dumps(data))

        if not self.__validate_session_data(data):
            error = jsonify(
                error=True,
                message=(
                    "Session JSON data doesn't include all required "
                    "attributes or there's less than 10s of data tracked."
                ),
            )
            error.status_code = 400
            newrelic.agent.record_exception(params=error)
            return error

        # === Make the session === #
        session = self.build_session_object(data)
        return jsonify(session)
