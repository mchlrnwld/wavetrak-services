""" Config value constants """
from os import environ

LOG_LEVEL = environ['SESSION_ANALYZER_LOG_LEVEL']
ENV = environ['APP_ENV']
SPOTS_API = environ['SPOTS_API']

# Local config
# HIGH/LOW are used to exlude out of bounds data when determining
# the transition speed. For example if a user acceidently tracks
# their car ride home we don't want that impacting the transition
# speed data (yes this doesn't remove slow moving cars).
HIGH_SPEED_LIMIT = 15.3
LOW_SPEED_LIMIT = 0.5

# Something could happen causing a very quick spike in speed, the
# speed must be sustained over this threshold before we'll consider
# the increased speed a wave.
MIN_WAVE_TIME = 2

# When a surfer does a cutback or alike they will slow down, this
# buffer allows X seconds of data under 60% of the wave threshold
# and outside the beach direction to occur before we consider
# this wave over.
ALLOWED_WAVE_END_SPEED_DIR_BUFFER = 6

# Similar to the above except we will allow this number of seconds
# to elapse if the user is still heading towards the beach since
# on some waves the surfer will slow right down whilst the wave reforms.
ALLOWED_WAVE_END_SPEED_BUFFER = 10

# If the user drops below the MIN_SURF_SPEED for more than this time
# we'll consider the wave ended, this is a more aggresive version of
# ALLOWED_BUFFER which uses the inflection speed and direction.
THRESHOLD_MIN_SURF_SPEED = 2

# If the speed drops below this m/s speed we consider the user paddling
# if they remain here for THRESHOLD_MIN_SURF_SPEED we end the wave.
MIN_SURF_SPEED = 1
