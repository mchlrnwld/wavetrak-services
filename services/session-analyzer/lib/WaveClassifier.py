"""
WaveClassifier Class

This class is used to analyze a user's positions for wave and paddle events.
Given an array of positions, this class creates a list of point and a list of
points within the speed threshold for a wave.

These two lists of points are then used to create the "events"
"""

import logging
import statistics
from operator import itemgetter

import numpy as np
import pandas as pd

from lib.utils import get_distance, get_course, get_transition_speed
from lib.config import LOW_SPEED_LIMIT, HIGH_SPEED_LIMIT,\
    ALLOWED_WAVE_END_SPEED_DIR_BUFFER, ALLOWED_WAVE_END_SPEED_BUFFER,\
    MIN_WAVE_TIME, LOG_LEVEL, THRESHOLD_MIN_SURF_SPEED,\
    MIN_SURF_SPEED

# For logging at AWS.
root = logging.getLogger()
if root.handlers:
    for handler in root.handlers:
        root.removeHandler(handler)

logger = logging.getLogger("session-analyzer")


class WaveClassifier:
    def __init__(self, positions, beach_angle):
        """
        Constructor

        Args:
            positions: list of position objects timestamp, latitude, longitude
        """
        self.beach_angle = beach_angle

        # It's quicker for us to create 2 arrays in this first loop, one with
        # all the points and one with just points within our speed thresholds.
        # The latter will be used for the transition speed model.
        self.points = []
        self.points_within_threshold = []

        # Ensure positions are unique based on the timestamp and are in the
        # correct order before setting.
        pos_list = list({v["timestamp"]: v for v in positions}.values())
        self.positions = sorted(pos_list, key=itemgetter("timestamp"))
        self.transition_speed = False
        self.__remove_bad_first_position()
        self.__fix_positions()
        self.__create_points()

    def __remove_bad_first_position(self):
        """
        In some cases (I suspect older apple watches) we receive a first
        position with a timestamp that is really far away from the second
        position's timestamp (sometimes up to multiple days). I suspect that
        the watch is giving us a cached time and location value for the first
        position.

        Since we interpolate the time between these two positions, when they
        are sufficiently far apart (hours/days) it ends up bloating the output
        session to be (potentially) megabytes in size.

        In order to avoid this sort of erroneous behavior, we'll purge the
        firstposition in the array if it is more than 15 minutes away from
        the second position

        Returns:
            void
        """
        first_pos = self.positions[0]
        second_pos = self.positions[1]
        fifteen_mins_in_seconds = 300
        diff = second_pos["timestamp"] - first_pos["timestamp"]
        if (diff > fifteen_mins_in_seconds):
            logger.debug("Removing first position")
            logger.debug(f"1st pos timestamp: {first_pos['timestamp']}. \
                2nd pos timestamp: {second_pos['timestamp']}")
            self.positions = self.positions[1:]

    def __get_positions_df(self):
        """
        Returns:
            self.positions as a dataframe with a date index
        """
        df = pd.DataFrame.from_records(self.positions)
        df['date'] = pd.to_datetime(df['timestamp'], unit='s')
        df['date'] = df['date'].dt.round("1S")
        df = df.set_index('date')

        return df

    def __fix_positions(self,
                        error_distance=20,
                        max_possible_speed=9,
                        walk=20):
        """
        We see two kinds of errors from bad GPS data:

        - Single abberant positions
        - Dropped signal, missing data then single poor positions when signal
        is re-aquired.

        Our solution is to identify large jumps in the position, to use the
        last good location prior as our starting point and then walk forward
        in time checking each row until we find one that's within a plausible
        distance of the last good point (using the max_possible_speed * time).
        Once we have a last good point we cut out everything in between.

        Once bad data is removed we then resample the data to
        1hz and use a linear interpolation to fill for the missing values. This
        doesn't stop the possibility of position jump caused by missing data,
        but does remove the problem of single aberant points both as isolated
        cases and on the resumption of signal.

        This fixes self.positions in place.

        Args:
            :param error_distance: The number of meters movement in a single
            update we consider likely an error
            :param max_possible_speed: The maximum plausible speed of travel
            for a surfer in the ocean (m/s)
            :param walk: The number of rows to walk forward to find the next
            good data point.

        Returns:
            Void
        """
        df = self.__get_positions_df()
        df['bad_data'] = np.NaN

        # Add shifted positions to this dataframe so we can apply a lamba
        # function to calculate distance on a row-wise basis
        df_shift = df.shift(1)
        df['prev_lat'] = df_shift['latitude']
        df['prev_lon'] = df_shift['longitude']
        # Fill the first rows new NaNs with the next value
        df = df.fillna(method='backfill')

        # Wrap the utils.get_distance function in a lambda function we'll
        # apply to each row.
        df['distance'] = df.apply(lambda row: get_distance(
                row['prev_lat'],
                row['prev_lon'],
                row['latitude'],
                row['longitude']
            ),
            axis=1
        )

        bad_points = df.index[df['distance'] > error_distance].tolist()

        for bad_point in bad_points:
            bad_point_loc = df.index.get_loc(bad_point)
            prior_row = df.iloc[bad_point_loc-1]
            rows = df.iloc[bad_point_loc:bad_point_loc + walk].iterrows()
            for i, r in rows:
                # Get the distance between the last good point and the point
                # being checked
                dist = get_distance(prior_row['latitude'],
                                    prior_row['longitude'],
                                    r['latitude'],
                                    r['longitude'])
                # Find the elapsed time between the last good point and the
                # point currently being checked and calculate a realistic
                # radius of travel in this time.
                time_delta = (i - bad_point) / np.timedelta64(1, 's')
                max_dist = max_possible_speed * time_delta
                # If we've found a reading that's within a plausible distance
                # of our last good reading make all rows between them and we'll
                # drop and linearly interpolate to fill this gap later
                if dist < max_dist:
                    # TODO: We should really cut at the row before 'i'
                    df.loc[bad_point:i, "bad_data"] = 1
                    break

        # Now we've looped we can cut out the bad data. We couldn't do this in
        # the loop above because sometimes we have bad data in sections that
        # are already cut from previous bad points
        df = df[df['bad_data'] != 1]

        # If we've still got bad points (perhaps the walk forward didn't go
        # far enough to find good data, we kill them here
        df['latitude'][df['distance'] > error_distance] = np.NaN
        df['longitude'][df['distance'] > error_distance] = np.NaN

        # Interpolate to 1hz and linearly fill
        df = df.resample('S').interpolate()

        self.__update_positions_from_df(df)

    def __update_positions_from_df(self, df):
        """
        Args:
            dataframe of positions in the format returned by get_positions_df

        Returns:
            The inverse of get_positions_df - updates the internal positions
            from a dataframe of positions
        """
        df = df[['latitude', 'longitude', 'timestamp']]
        self.positions = df.to_dict(orient='records')

    def __get_speed_and_course(self, prior_position, current_position):
        """
            Using two of the users positions (prior and current) creates
            a dict containing the users speed and their course which is then
            used for analyzing wave/paddle events

            Args:
                prior_position -- dict containing "latitude" and "longitude"
                current_position -- dict containing "latitude" and "longitude"

            Returns:
                dict with keys "speed" (float) and "course" (float)
        """
        distance = 0
        course = 0
        speed = 0

        if prior_position is not None:
            prior_timestamp = prior_position["timestamp"]
            current_timestamp = current_position["timestamp"]
            time_delta = current_timestamp - prior_timestamp

            distance = (
                get_distance(
                    prior_position["latitude"],
                    prior_position["longitude"],
                    current_position["latitude"],
                    current_position["longitude"],
                )
            )

            course = (
                get_course(
                    prior_position["latitude"],
                    prior_position["longitude"],
                    current_position["latitude"],
                    current_position["longitude"],
                )
            )

            speed = distance / time_delta

        return {"speed": speed, "course": course}

    def __create_points(self):
        """
            Private method used to do the initial iteration over the positions
            array in order to create the speed and course of the user using
            their current position and the previous position. From their it
            then creates the list of points and points within the threshold.

            Args:
            postions - array of position dicts with keys "timestamp",
            "latitude", "longitude".

            Returns:
            Void
        """
        prior_and_current_positions = [
            (self.positions[i - 1] if i > 0 else None, current_position)
            for i, current_position in enumerate(self.positions)
        ]

        speeds_and_courses = [
            self.__get_speed_and_course(prior, current)
            for (prior, current) in prior_and_current_positions
        ]

        self.points = [
            dict(position, **speeds_and_courses[i])
            for i, position in enumerate(self.positions)
        ]

        self.points_within_threshold = [
            point
            for point in self.points
            if LOW_SPEED_LIMIT < point["speed"] < HIGH_SPEED_LIMIT
        ]

    def __validate_wave_direction(self, course):
        """
            If a beach angle distance exists, we can validate that the
            direction the user is moving is towards the beach, rather than
            away from it, which will help us validate that this is actually
            a wave

            Args:
            course -- int

            Returns:
            Boolean
        """
        if self.beach_angle is not None:
            return self.within_window(course)
        else:
            # If we don't have a beach angle
            # we will consider any angle valid
            return True

    def angle_to_unit_circle_coords(self, angle):
        x = np.cos(np.deg2rad(angle))
        y = np.sin(np.deg2rad(angle))

        return np.array((x, y))

    def within_window(self, angle):
        """
        Converts the beach angle and given angle to unit circle coords
        and then uses the dot product to find out if the angles are within
        90 degrees of each other.

        Args:
            angle -- The angle of the direction that the user is surfing
        """
        angular_window = 90

        beach_angle_coords = self.angle_to_unit_circle_coords(self.beach_angle)
        angle_coords = self.angle_to_unit_circle_coords(angle)

        dot = beach_angle_coords.dot(angle_coords)
        angle_between_vectors = np.arccos(dot)

        return angle_between_vectors <= np.deg2rad(angular_window)

    def __calculate_stats(self, event):
        """
        Given an event we'll calculate the event distance, speeds
        and start/end timestamps.

        Args:
            event: event (dictionary) with a position
            key which is an array of position dictionaries.

        Return:
            Processed event.
        """
        event_with_stats = event.copy()

        first_position = event["positions"][0]
        last_position = event["positions"][-1]

        start_timestamp = first_position["timestamp"]
        end_timestamp = last_position["timestamp"]

        speeds = [x["speed"] for x in event["positions"]]
        ride_length = get_distance(
            first_position["latitude"],
            first_position["longitude"],
            last_position["latitude"],
            last_position["longitude"],
        )

        event_with_stats["distance"] = ride_length
        event_with_stats["speedMax"] = max(speeds)
        event_with_stats["speedAverage"] = statistics.mean(speeds)
        event_with_stats["startTimestamp"] = start_timestamp
        event_with_stats["endTimestamp"] = end_timestamp

        return event_with_stats

    def __filter_positions(self):
        """
        Walk through positions and determine which series of positions are
        waves and which are paddling using the transition_speed established
        from the Earth model. There is a far more efficient way of doing this
        whole method.
        Args:
            transiton_speed: Number
        Return:
            Array of events, each event has type PADDLE|WAVE and the series of
            positions for that event.
        """
        # Contains the output PADDLE and WAVE events.
        events = []

        # We will use this to incrementally build up an event before commiting
        # it to the main events array. Type is none since we add
        # that afterwards.
        tmp_event = {
            'type': None,
            'positions': []
        }

        # We retrospectivly transition from a PADDLE to a WAVE once the
        # MIN_WAVE_TIME has been met. When we do this we need to slice
        # out the positions that were added to tmp_event and put them
        # in the new wave event, this allows us to track the index in
        # the array we should slice out the data from.
        first_wave_threshold_index = None

        # Once we reach the threshold for one position we consider the track
        # to be in a wave. We might not add this as an actual wave unless
        # the MIN_WAVE_TIME (see config.py for explanation of that) is met
        # this allows us to track if we think we are in a wave.
        might_be_in_wave = False

        # We are actually in a wave that's been over the MIN_WAVE_TIME count.
        in_wave = False

        # When we're in a wave and then find the first position out of wave
        # we want to track that so we can determine as we iterate through
        # positions if we've been out of the wave for over
        # ALLOWED_WAVE_END_SPEED_DIR_BUFFER
        first_out_of_wave_index = None

        # If the user is below MIN_SURF_SPEED for THRESHOLD_MIN_SURF_SPEED
        # we end the wave, this is a more aggresive version of using the
        # inflection speed for ALLOWED_WAVE_END_SPEED_DIR_BUFFER.
        likely_ended_wave = False

        # Does the current position meet the required speed threshold for
        # started/still surfing?
        meets_speed_req = False

        logger.debug(f'Transition speed: {self.transition_speed}')

        for curr_index, row in enumerate(self.points):
            course = row["course"]
            timestamp = row["timestamp"]
            longitude = row["longitude"]
            latitude = row["latitude"]
            speed = row["speed"]

            # Is this point part of a surfed wave
            is_valid_direction = self.__validate_wave_direction(course)

            if self.transition_speed:
                if in_wave or might_be_in_wave:
                    # If the user is in a wave allow the user to continue
                    # within the wave so long as they're within 60% of the
                    # wave threshold
                    # this allows waves to extend should the user slow right
                    # down as a wave reforms.
                    threshold = self.transition_speed
                    if in_wave:
                        threshold = self.transition_speed * 0.6

                    meets_speed_req = speed >= threshold

                    if speed < MIN_SURF_SPEED and not is_valid_direction:
                        likely_ended_wave = True
                        logger.debug('probably paddling now override')
                # if speed isn't there but the direction is and it's only 1
                # unit out of place keey us in an might be wave state.
                else:
                    meets_speed_req = speed >= self.transition_speed

                # If we're in a wave and we've likely ended it override wave
                # theshold to false.
                if in_wave and likely_ended_wave:
                    meets_wave_threshold = False
                else:
                    meets_wave_threshold = meets_speed_req and is_valid_direction  # noqa: E501
            else:
                meets_wave_threshold = False

            # meets_wave_threshold checks both speed and direction if the
            # surfer slows right down but is still heading towards the beach
            # this will be false (which is correct) we wait for
            # ALLOWED_WAVE_END_SPEED_DIR_BUFFER (seconds) to elapse before we
            # consider the wave over however if the surfer is still heading
            # towards the beach we want use a higher threshold before we
            # consider this wave over since a surfer could still be surfing
            # and waiting for a wave to reform. This is all quite arbitary
            # when using speed and direction to evaluate whether a user
            # is surfing or not.
            allowed_not_meets_wave_buffer = ALLOWED_WAVE_END_SPEED_BUFFER
            if not is_valid_direction:
                allowed_not_meets_wave_buffer = ALLOWED_WAVE_END_SPEED_DIR_BUFFER  # noqa: E501

            # Always add the position to the tmp_event. We retroactively
            # categorize these positions if we transitons to/from WAVE/PADDLE.
            tmp_event["positions"].append(
                {
                    "timestamp": timestamp,
                    "latitude": latitude,
                    "longitude": longitude,
                    "speed": speed,
                }
            )

            logger.debug(f'{timestamp} {speed} {course} {is_valid_direction} \
                {meets_wave_threshold} {might_be_in_wave} {meets_speed_req} \
                {likely_ended_wave}')

            if (in_wave is False and not might_be_in_wave
               and meets_wave_threshold):
                # We didn't think we were in a wave because not enough
                # previous points were over the threshold for MIN_WAVE_TIME
                # We do now think we might be in a wave because the current
                # point is over the threshold.
                might_be_in_wave = True

                # Store the index of the last added position, we'll slice data
                # from here out if this is infact a wave. Remember we need
                # to go over the MIN_WAVE_TIME before we'll commit to this
                # being a wave.
                first_wave_threshold_index = len(tmp_event['positions']) - 1
                logger.debug('might be in wave')
            elif (
                    might_be_in_wave
                    and first_wave_threshold_index is not None
                    and timestamp - tmp_event['positions'][first_wave_threshold_index]['timestamp'] > MIN_WAVE_TIME  # noqa: E501
                 ):
                # We think we're in a wave and we've been in that wave
                # for > MIN_WAVE_TIME now we will actually transition
                # to a wave.
                # First slice out the preceding positions that were actually
                # the start of the new event. We take the point before the
                # first wave speed point since the wave threshold is a surfing
                # threshold and no one goes from not surfing to surfing
                # without any progressive increase in speed 1s is a guess
                # at that.
                fw_index = first_wave_threshold_index
                tmp_positions = tmp_event['positions'][fw_index - 1:]

                # Now commit the positions up to the fw_index in
                # tmp_event as a PADDLE event.
                events.append(self.__calculate_stats({
                    'type': 'PADDLE',
                    'positions': tmp_event['positions'][:fw_index]
                }))

                # Start the wave event with the prior positions that've
                # been above the threshold.
                tmp_event = {
                    'type': 'WAVE',
                    'positions': tmp_positions
                }

                # We are now in a wave.
                in_wave = True
                might_be_in_wave = False
                first_wave_threshold_index = None

                last_event = events[len(events) - 1]
                pos_len = len(last_event['positions'])
                last_position = last_event['positions'][pos_len - 1]
                first_position_ts = last_event['positions'][0]['timestamp']
                last_position_ts = last_position['timestamp']
                logger.debug(f'in a wave! commited paddle event \
                    {first_position_ts}:{last_position_ts}')
            elif (
                    in_wave and not meets_wave_threshold
                    and first_out_of_wave_index is not None
                    # Surfer has been out of the speed and direction threshold
                    # for over the buffer time = wave done.
                    and (
                        timestamp - tmp_event['positions'][first_out_of_wave_index]['timestamp'] > allowed_not_meets_wave_buffer  # noqa: E501
                        or
                        # Surfer has been below a hardcoded surf speed floor
                        # (MIN_SURF_SPEED) for over the allowed buffer time
                        # for this wave = wave done.
                        (
                            likely_ended_wave
                            and timestamp - tmp_event['positions'][first_out_of_wave_index]['timestamp'] > THRESHOLD_MIN_SURF_SPEED  # noqa: E501
                        )
                    )
                 ):
                # We are actively in a wave but the current position doesn't
                # signify we're still in said wave and we're over the
                # allowed_not_meets_wave_buffer of time not meeting
                # the wave threshold thus transition to a PADDLE.
                # First slice out the previous positions that were
                # actually paddle positions.
                fow_index = first_out_of_wave_index
                tmp_positions = tmp_event['positions'][fow_index:]

                # Now commit the positions up until this point as a wave
                # to the events list
                events.append(self.__calculate_stats({
                    'type': 'WAVE',
                    'positions': tmp_event['positions'][:fow_index]
                }))

                # Start the now paddle event.
                tmp_event = {
                    'type': 'PADDLE',
                    'positions': tmp_positions
                }

                # Set us out of a wave and clear the first out of wave index.
                in_wave = False
                first_out_of_wave_index = None
                likely_ended_wave = False

                last_event = events[len(events) - 1]
                pos_len = len(last_event['positions'])
                last_position = last_event['positions'][pos_len - 1]
                first_position_ts = last_event['positions'][0]['timestamp']
                last_position_ts = last_position['timestamp']
                logger.debug(f'no longer in a wave, switched to paddle, \
                    commited wave event \
                    {first_position_ts}:{last_position_ts}')
            elif (in_wave and not meets_wave_threshold
                  and first_out_of_wave_index is None):
                # If we get here we're in a wave and this is the first
                # position that might suggest we're no longer in a wave
                # track that so as we iterate forward we can determine
                # if we've been out of the wave for long enough.
                first_out_of_wave_index = len(tmp_event['positions']) - 1
                logger.debug('in a wave but we might be out of it, \
                    we will see')
            elif might_be_in_wave and not meets_wave_threshold:
                # We thought we were in a wave but we actually weren't
                # so reset that.
                might_be_in_wave = False
                likely_ended_wave = False
                first_wave_threshold_index = None
                logger.debug('wasn\'t actually a wave')
            elif (in_wave and meets_wave_threshold
                  and first_out_of_wave_index is not None):
                # If we're in a wave, the current position signifies we're
                # still in a wave and the first_out_of_wave_index is set we
                # thought for a moment we were out of the wave but
                # we actually weren't so reset that.
                first_out_of_wave_index = None
                logger.debug('actually no we are still in a wave')

        # Check to see if we were paddling the whole time.
        if len(events) == 0:
            events.append(self.__calculate_stats({
                'type': 'PADDLE',
                'positions': tmp_event['positions']
            }))
        else:
            # We only commit events after the fact, if we got here commit
            # to events the last positions not yet commited.
            events.append(self.__calculate_stats(tmp_event))

        return events

    def get_events(self):
        """
        Takes an array of position dictionaries with keys: timestamp (unix,
        ms as decimals), latitude (radians) and longitude (radians).
        Parses out when the user is paddling and surfing.

        Args:
            positions: Array of position dicts.

        Returns:
            Array of event dictionaries with the keys: startTimestamp,
            endTimestamp, type=PADDLE|WAVE and positions which is an array of
            coordinates and timestamps.
        """

        self.transition_speed = get_transition_speed(
            self.points_within_threshold
        )
        events = self.__filter_positions()

        return events
