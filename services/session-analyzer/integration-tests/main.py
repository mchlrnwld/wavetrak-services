import os
import unittest
import json
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError

hostname = os.environ['APP_HOSTNAME']
port = os.environ['APP_PORT']
sessions_host = f"http://{hostname}:{port}"


class IntegrationTests(unittest.TestCase):

    def test_healthcheck(self):
        req = Request(f'{sessions_host}/health', data=None, headers={
            'Content-Type': 'application/json'
        })

        response = False
        response = urlopen(req)
        content = response.read()
        data = json.loads(content.decode('utf-8'))

        self.assertEqual(200, response.status)
        self.assertEqual('OK', data['status'])


if __name__ == '__main__':
    unittest.main()
