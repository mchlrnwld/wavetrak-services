# Reports API Microservice

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Custom New Relic Instrumentation](#custom-new-relic-instrumentation)
- [Report API Endpoints](#report-api-endpoints)
- [Create reports](#create-reports)
- [Update reports](#update-reports)
- [Peek reports](#peek-reports)
- [Search Reports by Region](#search-reports-by-region)
    - [Query Parameters](#query-parameters)
- [Search Reports by Spot](#search-reports-by-spot)
    - [Query Parameters](#query-parameters-1)
    - [Sample Request](#sample-request)
    - [Sample Response](#sample-response)
  - [(For science use) Get reports by spotIds](#for-science-use-get-reports-by-spotids)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


Configure global environemnt
`nvm install v4.3.0`
`npm install -g eslint eslint-config-airbnb`

Configure .env appropriately (.env.sample provided)

Configuring the project
`npm install`

Testing the project
`npm run test`

Start logging docker in interactive mode (assuming docker is installed and daemon is running)
`cd ./services/logging-service && ./start-logstash.sh logstash-embedded.conf`

## Custom New Relic Instrumentation
You can add custom attributes with:
```
import * as newrelic from 'newrelic';
newrelic.default.addCustomAttribute('request', req);
```

## Report API Endpoints

Create reports
--------------
This operation can create multiple spot reports as specified in an array in the body.
The spot report body requires a valid ObjectId from the Spots Database which should
be tied to valid subregion (see spots-api for more information)

```
POST /admin/reports/  HTTP/1.1
```

```JavaScript
{
  "reports": [
      {
          "spot": "5823ad6d333e91c2c737b1a0",
          "condition": "3-4 ft – waist to chest",
          "occasionalHeight": "5",
          "rating": "very good",
          "note": "some note",
          "waterTempRange": "55-58",
          "regionalReport": "<html>Backing down but still plenty fun</html>"
      },
      {
          "spot": "4323ad6d333e91c2c737b1a1",
          "condition": "1-2 ft – knee to thigh",
          "rating": "poor-to-fair",
          "note": "some note",
          "waterTempRange": "60-62",
          "regionalReport": "<html>Great shape, but lacking power</html>"
      },
  ]
}
```

If the report saves successfully, we will get a 200 OK response, with the payload
looking like the following:

```
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache
```

```JavaScript
[
  {
    "__v": 0,
    "updatedAt": "2016-11-11T06:38:34.101Z",
    "created_at": "2016-11-11T06:38:34.101Z",
    "waterTempMax": 58,
    "waterTempMin": 55,
    "humanRelation": "waist to chest",
    "maxHeight": 4,
    "minHeight": 3,
    "spot": "5823ad6d333e91c2c737b1a0",
    "condition": "3-4 ft – waist to chest",
    "occasionalHeight": 5,
    "rating": "very good",
    "note": "some note",
    "waterTempRange": "55-58",
    "regionalReport": "<html>Backing down but still plenty fun</html>",
    "_id": "5825676a909068163db18d8d"
  },
  {
    "__v": 0,
    "updatedAt": "2016-11-11T06:38:34.101Z",
    "created_at": "2016-11-11T06:38:34.101Z",
    "waterTempMax": 62,
    "waterTempMin": 60,
    "humanRelation": "knee to thigh",
    "maxHeight": 2,
    "minHeight": 1,
    "spot": "4323ad6d333e91c2c737b1a1",
    "condition": "1-2 ft – knee to thigh",
    "rating": "poor-to-fair",
    "note": "some note",
    "waterTempRange": "55-58",
    "regionalReport": "<html>Great shape, but lacking power</html>",
    "_id": "2325676a909068163db18d8e"
  },
]
```

In the case of an error, the create operation will attempt to process all of the
spots in the array and at the end, present a list of reports that successfully saved
and a list of ones in error.  For example, assume that in our last example we had
a typo where the rating field is:

```
POST /admin/reports/  HTTP/1.1
```

```JavaScript
{
  "reports": [
      {
          "spot": "5823ad6d333e91c2c737b1a0",
          "condition": "3-4 ft – waist to chest",
          "occasionalHeight": "5",
          "rating": "very goo",
          "note": "some note",
          "waterTempRange": "55-58",
          "regionalReport": "<html>Backing down but still plenty fun</html>"
      },
      {
          "spot": "4323ad6d333e91c2c737b1a1",
          "condition": "1-2 ft – knee to thigh",
          "rating": "poor-to-fair",
          "note": "some note",
          "waterTempRange": "60-62",
          "regionalReport": "<html>Great shape, but lacking power</html>"
      },
  ]
}
```

We would get back the following:

```
HTTP/1.1 500 Internal Server Error
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache
```

```JavaScript
{
  "message": "Failed to save spot reports",
  "savedReports": [
    {
      "reportId": "58256a40909068163db18d90",
      "spot": "4323ad6d333e91c2c737b1a1"
    }
  ],
  "errors": [
    {
      "reason": "SpotReport validation failed",
      "spot": "5823ad6d333e91c2c737b1a0"
    }
  ]
}
```

Update reports
--------------
This operation is similar to the create, in that you can modify multiple spot
reports as specified in an array in the body. The spot report body requires a
valid ObjectId from the current reports database, as well as a valid ObjectId
from the Spots Database (again, which should be tied to valid subregion).

```
PUT /admin/reports/  HTTP/1.1
```

```JavaScript
{
  "reports": [
      {
          "_id": "5825676a909068163db18d8d",
          "spot": "5823ad6d333e91c2c737b1a0",
          "condition": "3-4 ft – waist to chest",
          "occasionalHeight": "5",
          "rating": "epic",
          "note": "woohoo!",
          "waterTempRange": "55-58",
          "regionalReport": "<html>Never mind, its going off!!!!</html>"
      },
      {
          "_id": "3825676a909068163db18d8e",
          "spot": "4323ad6d333e91c2c737b1a1",
          "condition": "1-2 ft – thigh",
          "rating": "poor-to-fair",
          "note": "hohum...",
          "waterTempRange": "60-62",
          "regionalReport": "<html>Scratch that, shape is poor</html>"
      },
  ]
}
```

In which a successful response would be:

```
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache
```

```JavaScript
[
  {
    "__v": 0,
    "updatedAt": "2016-11-11T06:38:34.101Z",
    "created_at": "2016-11-11T06:38:34.101Z",
    "waterTempMax": 58,
    "waterTempMin": 55,
    "humanRelation": "waist to chest",
    "maxHeight": 4,
    "minHeight": 3,
    "spot": "5823ad6d333e91c2c737b1a0",
    "condition": "3-4 ft – waist to chest",
    "occasionalHeight": 5,
    "rating": "epic",
    "note": "woohoo!",
    "waterTempRange": "55-58",
    "regionalReport": "<html>Never mind, its going off!!!!</html>",
    "_id": "5825676a909068163db18d8d"
  },
  {
    "__v": 0,
    "updatedAt": "2016-11-11T06:38:34.101Z",
    "created_at": "2016-11-11T06:38:34.101Z",
    "waterTempMax": 62,
    "waterTempMin": 60,
    "humanRelation": "knee to thigh",
    "maxHeight": 2,
    "minHeight": 1,
    "spot": "4323ad6d333e91c2c737b1a1",
    "condition": "1-2 ft – knee to thigh",
    "rating": "poor-to-fair",
    "note": "hohum...",
    "waterTempRange": "60-62",
    "regionalReport": "<html>Scratch that, shape is poor</html>"
    "_id": "3825676a909068163db18d8e"
  },
]
```

And in the case of an unsuccessful save:

```
PUT /admin/reports/  HTTP/1.1
```

```JavaScript
{
  "reports": [
      {
          "_id": "5825676a909068163db18d8d",
          "spot": "5823ad6d333e91c2c737b1a0",
          "condition": "3-4 ft – waist to chest",
          "occasionalHeight": "5",
          "rating": "epic",
          "note": "woohoo!",
          "waterTempRange": "55-58",
          "regionalReport": "<html>Never mind, its going off!!!!</html>"
      },
      {
          "_id": "3825676a909068163db18d8e",
          "spot": "4323ad6d333e91c2c737b1a1",
          "condition": "1-2 ft – knee to thigh",
          "rating": "poor-to-fair",
          "note": "hohum...",
          "waterTempRange": "60-62",
          "regionalReport": "<html>Scratch that, shape is poor</html>"
      },
  ]
}
```

We would get back the following:

```
HTTP/1.1 500
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache
```

```JavaScript
{
  "message": "Failed to update spot reports",
  "savedReports": [
    {
      "reportId": "5825676a909068163db18d8d",
      "spot": "5823ad6d333e91c2c737b1a0"
    }
  ],
  "errors": [
    {
      "reason": "Connection error",
      "spot": "4323ad6d333e91c2c737b1a1"
      reportId: "3825676a909068163db18d8e"
    }
  ]
}
```

Peek reports
------------

The peek reports operation grabs the latest report from each region.  You must
specify a valid SubregionId.

```
GET /admin/reports/peek/?regionId=[:regionId]
```

For example:

```
GET /admin/reports/peek/?regionId=5823b123333e91c2c737b1a4  HTTP/1.1
```

Which would have the response:

```
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache
```

```JavaScript
{
  "subregionId": "5823b123333e91c2c737b1a4",
  "subregionName": "North Orange County",
  "spotReports": [
    {
      "_id": "582561c922519f156f1bd18b",
      "spot": {
        "_id": "58126caff879804aa6edda4c",
        "name": "Huntington Beach, North Side"
      },
      "condition": "3-4 ft – waist to chest",
      "occasionalHeight": "5",
      "rating": "poor",
      "note": "some note",
      "waterTempRange": "55-58",
      "regionalReport": "<html>Crappy surf on tap</html>"
    },
    {
      "_id": "5825622122519f156f1bd18e",
      "spot": {
        "_id": "5823ad6d333e91c2c737b1a0",
        "name": "Newport Beach"
      },
      "condition": "3-4 ft – waist to chest",
      "occasionalHeight": "5",
      "rating": "fair-to-good",
      "note": "some note",
      "waterTempRange": "55-58",
      "regionalReport": "<html>Backing down but still plenty fun</html>"
    }
  ]
}
```

If an invalid region is specified, no spots are found, or no reports are found,
the operation will return a 404 error, respectively:

```
HTTP/1.1 404 Not Found
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache
```

```JavaScript
{
  "message": "Subregion not found"
}
```

```
HTTP/1.1 404 Not Found
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache
```

```JavaScript
{
  "message": "No spots found for subregion"
}


HTTP/1.1 404 Not Found
Content-Type: application/json;charset=UTF-8
Cache-Control: no-store
Pragma: no-cache

{
  "message": "Report(s) not found'"
}
```

## Search Reports by Region

Queries the report API for regional reports by `subregionId`.

#### Query Parameters
| Parameter | Required | Description                |
| ------------- | -------- | -------------------------- |
| `subregionId` | yes      | ObjectId for spot          |
| `limit`   | no       | Number to limit results to |


```
GET /admin/reports/regional?subregionId=58581a836630e24c44878fd6&amp;limit=1 HTTP/1.1

{
  "regionalReports": [
    {
      "_id": "58b0d1501c11990011e33c03",
      "updatedAt": "2017-02-25T00:35:28.247Z",
      "createdAt": "2017-02-25T00:35:28.247Z",
      "subregion": "58581a836630e24c44878fd6",
      "waterTemp": {
        "range": "56-58",
        "max": 58,
        "min": 56
      },
      "report": "<p><strong>Afternoon Report for North OC:</strong> Old W-WNW swell mix continues into the afternoon with mainly knee-waist+ waves at exposures and occasional shoulder high sets for top breaks.</p>",
      "legacySubregionId": 2143,
      "forecasterEmail": "aaron@surfline.com",
      "forecasterName": "Aaron Bernstein",
      "forecasterTitle": "Product Owner",
      "__v": 0,
      "stale": true
    }
  ]
}
```


Search Reports by Spot
----------------------

This endpoint returns reports for the spot sorted by created date descending.

#### Query Parameters
| Parameter | Required | Description                |
| --------- | -------- | -------------------------- |
| `spotId`  | yes      | ObjectId for spot          |
| `limit`   | no       | Number to limit results to |

#### Sample Request

```
GET /admin/reports/spot/?spotId=58191c3212c6a20012790683&limit=1 HTTP/1.1
```

#### Sample Response

```JavaScript
{
  "spotReports": [
    {
      "_id": "583224c04f1f680011a0aff6",
      "updatedAt": "2016-11-20T22:33:36.462Z",
      "createdAt": "2016-11-20T22:33:36.462Z",
      "waterTempMax": 58,
      "waterTempMin": 55,
      "humanRelation": "None",
      "maxHeight": 0,
      "minHeight": 0,
      "spot": "58191c3212c6a20012790683",
      "condition": 0,
      "occasionalHeight": 9,
      "rating": "FAIR_TO_GOOD",
      "note": "some note",
      "waterTempRange": "55-58",
      "regionalReport": "<html>Get out there, brah!</html>",
      "__v": 0,
      "stale": true
    }
  ]
}
```


### (For science use) Get reports by spotIds

Returns a set of reports for a given set of `spotIds` (You may pass a single
`spotId` to `spotIds`).

```
GET /admin/reports/?spotIds=5842041f4e65fad6a7708856,5842041f4e65fad6a7708bed
```

**Response**

```
{
  "reports": [
    {
      "_id": "587e9b1d5b46990011a1d8dd",
      "spot": {
        "_id": "5842041f4e65fad6a7708856",
        "legacyId": 4278,
        "name": "Manasquan Inlet",
        "forecastLocation": {
          "type": "Point",
          "coordinates": [
            -73.95965545654,
            40.111951234891
          ]
        }
      },
      "updatedAt": "2017-01-17T22:30:53.896Z",
      "createdAt": "2017-01-17T22:30:53.896Z",
      "maxHeight": 0,
      "minHeight": 0,
      "occasionalHeight": 0
    },
    {
      "_id": "587e9b1e5b46990011a1d8e0",
      "spot": {
        "_id": "5842041f4e65fad6a7708bed",
        "legacyId": 6319,
        "name": "Mantoloking",
        "forecastLocation": {
          "type": "Point",
          "coordinates": [
            -74.002,
            40.013
          ]
        }
      },
      "updatedAt": "2017-01-17T22:30:54.058Z",
      "createdAt": "2017-01-17T22:30:54.058Z",
      "maxHeight": 8,
      "minHeight": 5,
      "occasionalHeight": 9
    }
  ]
}
```

## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `reports-api` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnd5TnpBNU1UUTVOdyJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53eU56QTVNVFE1TnciLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the endpoints detailed in the [Report API Endpoints](#report-api-endpoints) section. 

**Ex:**

Test `GET` requests to the `/admin/reports` endpoint:

```
curl \
    -X GET \
    -i \
    http://reports-api.sandbox.surfline.com/admin/reports/spot?spotId=5842041f4e65fad6a770888a&limit=1
```
**Expected response:** The response should look something like:
```
{
  "spotReports": [
    {
      "_id": "58e2a9e94d61a10011d14708",
      "updatedAt": "2017-04-03T20:00:41.829Z",
      "createdAt": "2017-04-03T20:00:41.829Z",
      "humanRelation": "waist to head",
      "maxHeight": 5,
      "minHeight": 3,
      "spot": "5842041f4e65fad6a770888a",
      "condition": 7,
      "occasionalHeight": 6,
      "rating": "POOR_TO_FAIR",
      "note": "",
      "waterTemp": {
        "range": "58-60",
        "max": 60,
        "min": 58
      },
      "regionalReport": "<p><strong>Afternoon Report for South OC:</strong> Unfortunately, winds continue out of the Southerly direction, resulting in textured conditions. Our WNW swell tops out while small SSW swell blends in with mostly waist-head high waves, as standouts go overhead on sets. Overall, it's manageable with a few fun, lined up walls but lower expectations a notch due to these onshores.<br>\n<br>\n<strong>Short-Term Forecast for South OC:</strong> We have a 3.4' mid-high tide topping out at 5:20pm. Light+ SW-WSW wind continues into the afternoon, which may slack off in the evening - keep an eye on the cams. Cleaner conditions setting up for tomorrow morning...&nbsp;</p>",
      "forecasterEmail": "keaton@surfline.com",
      "forecasterName": "Keaton Browning",
      "forecasterTitle": "Forecaster",
      "__v": 0,
      "stale": true,
      "forecastLocation": {
        "type": "Point",
        "coordinates": [
          -117.588,
          33.381
        ]
      },
      "legacyId": 4740
    }
  ]
}
```