import chai from 'chai';
import dirtyChai from 'dirty-chai';
import pact from './pact';
import admin from './admin';

chai.use(dirtyChai);

describe('Reports API', () => {
  pact();
  admin();
});
