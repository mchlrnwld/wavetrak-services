import reports from './reports';

const admin = () => {
  describe('/admin', () => {
    reports();
  });
};

export default admin;
