import { expect } from 'chai';
import fetch from 'node-fetch';
import { ObjectID } from 'mongodb';
import connectMongo from '../helpers/connectMongo';
import cleanMongo from '../helpers/cleanMongo';
import config from '../config';

const reports = () => {
  describe('/reports', () => {
    let db;
    const spotId1 = new ObjectID('5842041f4e65fad6a77088ed');
    const spotId2 = new ObjectID('5842041f4e65fad6a7708827');
    const reportId1 = new ObjectID('5875387e4262a900136f35e1');
    const reportId2 = new ObjectID('58a60ee51796920011663edf');

    before(async () => {
      db = await connectMongo();
    });

    afterEach(() => cleanMongo(db));

    after(() => {
      db.close();
    });

    describe('POST', () => {
      it('creates list of spot reports', async () => {
        const response = await fetch(`${config.REPORTS_API}/admin/reports`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'X-Auth-EmployeeEmail': 'jwang@surfline.com',
            'X-Auth-EmployeeName': 'Jason Wang',
            'X-Auth-EmployeeTitle': 'Software Engineer',
          },
          body: JSON.stringify({
            reports: [
              {
                condition: 6,
                minHeight: 2,
                maxHeight: 3,
                humanRelation: 'thigh to waist',
                rating: 'FAIR_TO_GOOD',
                waterTemp: {
                  range: '75-80',
                  min: 75,
                  max: 80,
                },
                regionalReport: '<p>This is your regional report for 1/10</p>',
                spot: spotId1,
              },
              {
                condition: 12,
                minHeight: 3,
                maxHeight: 4,
                humanRelation: 'waist to chest',
                rating: 'POOR_TO_FAIR',
                waterTemp: {
                  range: '56-58',
                  min: 56,
                  max: 58,
                },
                regionalReport: '<p>This is your regional report for 1/10</p>',
                spot: spotId2,
              },
            ],
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body.reports).to.be.ok();
        expect(body.reports).to.have.length(2);
        expect(body.reports[0]._id).to.be.ok();
        expect(body.reports[1]._id).to.be.ok();

        const spotReports = await db.collection('SpotReports').find({
          _id: {
            $in: [
              new ObjectID(body.reports[0]._id),
              new ObjectID(body.reports[1]._id),
            ],
          },
        }).toArray();
        expect(spotReports).to.be.ok();
        expect(spotReports).to.have.length(2);
      });
    });

    describe('PUT', () => {
      before(async () => {
        await db.collection('SpotReports').insertMany([
          {
            _id: reportId1,
            condition: 6,
            minHeight: 2,
            maxHeight: 3,
            humanRelation: 'thigh to waist',
            rating: 'FAIR_TO_GOOD',
            waterTemp: {
              range: '75-80',
              min: 75,
              max: 80,
            },
            regionalReport: '<p>This is your regional report for 1/10</p>',
            spot: spotId1,
          },
          {
            _id: reportId2,
            condition: 12,
            minHeight: 3,
            maxHeight: 4,
            humanRelation: 'waist to chest',
            rating: 'POOR_TO_FAIR',
            waterTemp: {
              range: '56-58',
              min: 56,
              max: 58,
            },
            regionalReport: '<p>This is your regional report for 1/10</p>',
            spot: spotId2,
          },
        ]);
      });

      it('updates list of spot reports', async () => {
        const response = await fetch(`${config.REPORTS_API}/admin/reports`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            reports: [
              {
                _id: reportId1,
                regionalReport: '<p>This is your UPDATED regional report for 1/10</p>',
              },
              {
                _id: reportId2,
                regionalReport: '<p>This is your UPDATED regional report for 1/10</p>',
              },
            ],
          }),
        });
        expect(response.status).to.equal(200);
        const body = await response.json();
        expect(body).to.be.ok();
        expect(body.reports).to.be.ok();
        expect(body.reports).to.have.length(2);

        const spotReports = await db.collection('SpotReports').find({
          _id: {
            $in: [reportId1, reportId2],
          },
        }).toArray();
        expect(spotReports).to.be.ok();
        expect(spotReports).to.have.length(2);
        expect(spotReports[0]).to.be.ok();
        expect(spotReports[0].regionalReport).to.equal('<p>This is your UPDATED regional report for 1/10</p>');
        expect(spotReports[1]).to.be.ok();
        expect(spotReports[1].regionalReport).to.equal('<p>This is your UPDATED regional report for 1/10</p>');
      });
    });
  });
};

export default reports;
