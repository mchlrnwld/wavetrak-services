set -e
echo "Running Tests in $(pwd)"
npm run lint
npm test
echo "OK"
exit 0
