import mongoose from 'mongoose';
import { surfConditions, surfRatings } from './conditions';
import { parseWaterTempRange, validateWaterTemp, waterTempSchema } from './waterTemp';

const reportSchema = new mongoose.Schema(
  {
    createdAt: { type: Date },
    condition: {
      type: Number,
      required: [true, 'Condition is required'],
      enum: Object.keys(surfConditions).map((key) => Number.parseInt(key, 10)),
    },
    minHeight: { type: Number, required: [true, 'Min Height is required'] },
    maxHeight: { type: Number, required: [true, 'Max Height is required'] },
    plusHeight: { type: Boolean, default: false },
    humanRelation: { type: String, required: [true, 'Human Relation is required'] },
    occasionalHeight: { type: Number },
    rating: {
      type: String,
      required: [true, 'Rating is required'],
      enum: Object.keys(surfRatings),
    },
    waterTemp: {
      type: waterTempSchema,
      validate: validateWaterTemp,
      required: [true, 'Water Temp is required'],
      _id: false,
    },
    note: { type: String },
    regionalReport: { type: String, required: [true, 'Regional Report is required'] },
    spot: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Spot',
      required: [true, 'Spot is required'],
    },
    forecasterEmail: { type: String },
    forecasterName: { type: String },
    forecasterTitle: { type: String },
  },
  {
    collection: 'SpotReports',
    timestamps: true,
  },
);

reportSchema.index({ spot: 1, createdAt: -1 });
reportSchema.index({ spot: 1 });
reportSchema.index({ createdAt: -1 });

reportSchema.pre('validate', function parseConditions(next) {
  const conditions = surfConditions[this.condition];
  if (conditions) {
    this.minHeight = conditions.minHeight;
    this.maxHeight = conditions.maxHeight;
    this.humanRelation = conditions.humanRelation;
    this.plusHeight = conditions.plus;
  }
  return next();
});

reportSchema.pre('validate', parseWaterTempRange);

export default mongoose.model('SpotReport', reportSchema);
