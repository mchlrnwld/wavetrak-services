export function parseWaterTempRange(next) {
  if (this.waterTemp) {
    this.waterTemp.range = `${this.waterTemp.min}-${this.waterTemp.max}`;
  }
  return next();
}

export const validateWaterTemp = {
  validator: function validateWaterTemp(waterTemp) {
    return waterTemp.min <= waterTemp.max;
  },
  message: 'Water Temp Min cannot be greater than Water Temp Max',
};

export const waterTempSchema = {
  min: { type: Number, required: [true, 'Water Temp Min is required'] },
  max: { type: Number, required: [true, 'Water Temp Max is required'] },
  range: { type: String },
};
