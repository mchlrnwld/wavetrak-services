import mongoose, { Schema } from 'mongoose';

export const SpotModel = mongoose.model('Spot', new Schema({}, { collection: 'Spots' }));

// eslint-disable-next-line import/prefer-default-export
export const relatedVirtualSpots = (spotId) => {
  const id = mongoose.Types.ObjectId(spotId);
  return SpotModel.find(
    {
      $or: [
        {
          $and: [
            { 'humanReport.status': { $eq: 'AVERAGE' } },
            {
              $or: [
                { 'humanReport.average.spotOneId': { $eq: id } },
                { 'humanReport.average.spotTwoId': { $eq: id } },
              ],
            },
          ],
        },
        {
          $and: [
            { 'humanReport.status': { $eq: 'CLONE' } },
            { 'humanReport.clone.spotId': { $eq: id } },
          ],
        },
      ],
    },
    { _id: 1 },
  ).lean();
};
