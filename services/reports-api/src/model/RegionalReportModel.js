import mongoose from 'mongoose';
import { parseWaterTempRange, validateWaterTemp, waterTempSchema } from './waterTemp';

const reportSchema = new mongoose.Schema(
  {
    subregion: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Subregion',
      required: [true, 'Subregion is required'],
    },
    legacySubregionId: {
      type: Number,
      required: [true, 'Legacy Subregion ID is required'],
    },
    waterTemp: {
      type: waterTempSchema,
      validate: validateWaterTemp,
      required: [true, 'Water Temp is required'],
      _id: false,
    },
    report: { type: String, required: [true, 'Report is required'] },
    forecasterEmail: { type: String },
    forecasterName: { type: String },
    forecasterTitle: { type: String },
  },
  {
    collection: 'RegionalReports',
    timestamps: true,
  },
);

reportSchema.pre('validate', parseWaterTempRange);

reportSchema.index({ subregion: 1, createdAt: -1 });
reportSchema.index({ subregion: 1 });
reportSchema.index({ legacySubregionId: 1.0, updatedAt: -1.0 }, { unique: true, sparse: true });

const ReportModel = mongoose.model('RegionalReport', reportSchema);

export const createRegionalReport = (model) => new ReportModel(model);

export default ReportModel;
