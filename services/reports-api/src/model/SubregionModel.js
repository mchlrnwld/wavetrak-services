import mongoose from 'mongoose';

export const subregionSchema = new mongoose.Schema(
  {
    name: { type: String, required: [true, 'Name is required'] },
    spots: [
      {
        spot: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Spot',
          required: [true, 'Spot requires an ID'],
        },
        rank: { type: Number },
        humanReport: { type: Boolean },
      },
    ],
    imported: {
      subregionId: { type: Number },
    },
  },
  {
    collection: 'Subregions',
  },
);

export default mongoose.model('Subregion', subregionSchema);
