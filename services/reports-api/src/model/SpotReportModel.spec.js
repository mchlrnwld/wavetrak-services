import { expect } from 'chai';
import SpotReport from './SpotReportModel';

const validateShouldError = new Error('Validate expected to error');
validateShouldError.name = 'ValidateExpectedToError';

describe('SpotReport Model', () => {
  it('validates waterTemp.max is at least waterTemp.min', async () => {
    const validModel = new SpotReport({
      waterTemp: {
        min: 65,
        max: 65,
      },
    });
    const invalidModel = new SpotReport({
      waterTemp: {
        min: 65,
        max: 64,
      },
    });

    try {
      await validModel.validate();
      throw validateShouldError;
    } catch (err) {
      expect(err).to.be.ok();
      expect(err.errors.waterTemp).not.to.be.ok();
    }

    try {
      await invalidModel.validate();
      throw validateShouldError;
    } catch (err) {
      expect(err).to.be.ok();
      expect(err.errors.waterTemp).to.be.ok();
      expect(err.errors.waterTemp.name).to.equal('ValidatorError');
      expect(err.errors.waterTemp.message).to.equal(
        'Water Temp Min cannot be greater than Water Temp Max',
      );
    }
  });

  it('parses water temp before validate', async () => {
    const model = new SpotReport({
      waterTemp: {
        min: 65,
        max: 66,
      },
    });

    try {
      await model.validate();
    } catch (err) {
      expect(err).to.be.ok();
    }

    expect(model.waterTemp.range).to.equal('65-66');
  });
});
