import { expect } from 'chai';
import RegionalReport from './RegionalReportModel';

const validateShouldError = new Error('Validate expected to error');
validateShouldError.name = 'ValidateExpectedToError';

describe('RegionalReport Model', () => {
  it('validates required fields', async () => {
    const model = new RegionalReport();

    try {
      await model.validate();
      throw validateShouldError;
    } catch (err) {
      expect(err.name).to.equal('ValidationError');
      expect(err.errors).to.be.ok();
      expect(err.errors.report).to.be.ok();
      expect(err.errors.report.message).to.equal('Report is required');
      expect(err.errors.legacySubregionId).to.be.ok();
      expect(err.errors.legacySubregionId.message).to.equal('Legacy Subregion ID is required');
      expect(err.errors.subregion).to.be.ok();
      expect(err.errors.subregion.message).to.equal('Subregion is required');
      expect(err.errors.waterTemp).to.be.ok();
      expect(err.errors.waterTemp.message).to.equal('Water Temp is required');
    }
  });

  it('validates waterTemp.max is at least waterTemp.min', async () => {
    const validModel = new RegionalReport({
      waterTemp: {
        min: 65,
        max: 65,
      },
    });
    const invalidModel = new RegionalReport({
      waterTemp: {
        min: 65,
        max: 64,
      },
    });

    try {
      await validModel.validate();
      throw validateShouldError;
    } catch (err) {
      expect(err).to.be.ok();
      expect(err.errors.waterTemp).not.to.be.ok();
    }

    try {
      await invalidModel.validate();
      throw validateShouldError;
    } catch (err) {
      expect(err).to.be.ok();
      expect(err.errors.waterTemp).to.be.ok();
      expect(err.errors.waterTemp.name).to.equal('ValidatorError');
      expect(err.errors.waterTemp.message).to.equal(
        'Water Temp Min cannot be greater than Water Temp Max',
      );
    }
  });

  it('parses water temp before validate', async () => {
    const model = new RegionalReport({
      waterTemp: {
        min: 65,
        max: 66,
      },
    });

    try {
      await model.validate();
    } catch (err) {
      expect(err).to.be.ok();
    }

    expect(model.waterTemp.range).to.equal('65-66');
  });
});
