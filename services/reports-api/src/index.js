import 'newrelic';
import app from './server';
import logger from './common/logger';
import * as dbContext from './model/dbContext';
import { setupPactServers } from './pact/providers';

const log = logger('reports-api:server');

const start = async () => {
  if (process.env.NODE_ENV === 'test') {
    try {
      await setupPactServers();
    } catch (err) {
      log.error({
        message: "App Initialization failed. Can't setup Pact servers",
        stack: err,
      });
    }
  }

  try {
    await Promise.all([dbContext.initMongoDB()]);
    app();
  } catch (err) {
    log.error({
      message: "App Initialization failed. Can't connect to database",
      stack: err,
    });
  }
};

start();
