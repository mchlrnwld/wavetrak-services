import testconfig from './testconfig';

const defaultUrl = (port) => `http://localhost:${port}`;

export default {
  SPOTS_API: process.env.SPOTS_API || defaultUrl(testconfig.SPOTS_API_PORT),
  MONGO_CONNECTION_STRING_KBYG: process.env.MONGO_CONNECTION_STRING_KBYG,
};
