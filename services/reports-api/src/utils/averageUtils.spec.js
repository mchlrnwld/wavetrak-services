/* eslint-disable no-duplicate-imports, import/no-duplicates */
import { expect } from 'chai';
import { floorAvg, roundAvg, averageFields } from './averageUtils';

describe('validate averageUtils', () => {
  it('should average two values floored', async () => {
    const field1 = 3;
    const field2 = 4;
    const average = averageFields(field1, field2, floorAvg);
    expect(average).to.equal(3);
  });
  it('should average two values rounded', async () => {
    const field1 = 3;
    const field2 = 4;
    const average = averageFields(field1, field2, roundAvg);
    expect(average).to.equal(4);
  });
  it('should average with lookup', async () => {
    const conditionLookup = [5, 6];
    const field1 = 1;
    const field2 = 2;
    const average = averageFields(field1, field2, floorAvg, conditionLookup);
    expect(average).to.equal(6);
  });
});
