export const floorAvg = (field1, field2) => Math.floor((field1 + field2) / 2);
export const roundAvg = (field1, field2) => Math.round((field1 + field2) / 2);

export const averageFields = (field1, field2, avgMethod, lookupTable) => {
  let newField;
  if (field1 > 0 && field2 > 0) {
    newField = avgMethod(field1, field2);
  } else if (field1 > 0) {
    newField = field1;
  } else if (field2 > 0) {
    newField = field2;
  } else {
    newField = 0;
  }
  return lookupTable ? lookupTable[newField] : newField;
};
