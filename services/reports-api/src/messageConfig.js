const base = {
  region: 'us-west-1',
  api: '2010-03-31',
};

const envConfig = {
  development: {
    topic: 'arn:aws:sns:us-west-1:665294954271:spot_report_updated_sandbox',
  },
  sandbox: {
    topic: 'arn:aws:sns:us-west-1:665294954271:spot_report_updated_sandbox',
  },
  staging: {
    topic: 'arn:aws:sns:us-west-1:665294954271:spot_report_updated_staging',
  },
  production: {
    topic: 'arn:aws:sns:us-west-1:833713747344:spot_report_updated_prod',
  },
};

export default {
  ...base,
  ...envConfig[process.env.NODE_ENV || 'development'],
};
