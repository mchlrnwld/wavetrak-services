import { setupExpress } from '@surfline/services-common';
import logger from '../common/logger';
import admin from './admin';

const log = logger('reports-api');

const setupApp = () =>
  setupExpress({
    port: process.env.EXPRESS_PORT || 8089,
    log,
    name: 'reports-api',
    allowedMethods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
    handlers: [['/admin', admin(log)]],
  });

export default setupApp;
