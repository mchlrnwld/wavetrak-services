import RegionalReport, { createRegionalReport } from '../../../../model/RegionalReportModel';
import SubregionModel from '../../../../model/SubregionModel';
import getForecasterDetails from '../forecasterDetails';

export const trackReportsRequests = (log) => (req, res, next) => {
  log.trace({
    action: '/admin/reports/regional',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export const createReportHandler = async (req, res) => {
  const subregion = await SubregionModel.findById(req.body.subregion);

  if (!subregion) {
    return res.status(400).send({ message: 'Invalid subregion' });
  }

  const regionalReport = createRegionalReport({
    ...req.body,
    legacySubregionId: subregion.imported.subregionId,
    ...getForecasterDetails(req),
  });
  await regionalReport.save();
  return res.send(regionalReport);
};

export const updateReportHandler = async (req, res) => {
  const { waterTempMin, waterTempMax, report } = req.body;
  const regionalReport = await RegionalReport.findById(req.params.regionalReportId);

  if (!regionalReport) {
    return res.status(404).send({
      message: 'Regional report not found',
    });
  }

  regionalReport.waterTempMin = waterTempMin;
  regionalReport.waterTempMax = waterTempMax;
  regionalReport.report = report;

  await regionalReport.save();
  return res.send(regionalReport);
};
