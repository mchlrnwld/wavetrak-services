import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import { trackReportsRequests, updateReportHandler, createReportHandler } from './regional';

const reports = (log) => {
  const api = Router();

  api.get('/health', (_, res) =>
    res.send({
      status: 200,
      message: 'OK',
      version: process.env.APP_VERSION || 'unknown',
    }),
  );

  api.use('*', trackReportsRequests(log));
  api.put('/:regionalReportId', wrapErrors(updateReportHandler));
  api.post('/', wrapErrors(createReportHandler));

  return api;
};

export default reports;
