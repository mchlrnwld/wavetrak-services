import chai, { expect } from 'chai';
import { json } from 'body-parser';
import sinon from 'sinon';
import express from 'express';
import mongoose from 'mongoose';
import regionalReportsAPI from '.';
import Subregion from '../../../../model/SubregionModel';
import RegionalReport, * as regionalReportModel from '../../../../model/RegionalReportModel';

describe('/admin/reports/regional', () => {
  let log;
  let request;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(json());
    app.use(regionalReportsAPI(log));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    sinon.stub(Subregion, 'findById');
    sinon.stub(RegionalReport, 'findById');
    sinon.stub(regionalReportModel, 'createRegionalReport');
  });

  afterEach(() => {
    Subregion.findById.restore();
    RegionalReport.findById.restore();
    regionalReportModel.createRegionalReport.restore();
  });

  describe('POST /', () => {
    it('should create a new report for the subregion', async () => {
      const subregion = {
        _id: mongoose.Types.ObjectId(),
        name: 'North Orange County',
        imported: {
          subregionId: 1234,
        },
      };
      const expectedRegionalReport = {
        subregion: subregion._id,
        legacySubregionId: 1234,
        forecasterEmail: 'mwillis@surfline.com',
        forecasterName: 'Johnny Tsunami',
        forecasterTitle: 'Surfline Employee',
        report: 'Some report',
        waterTempMin: 65,
        waterTempMax: 70,
      };
      Subregion.findById.resolves(subregion);
      const save = sinon.stub().resolves();
      regionalReportModel.createRegionalReport.returns({ ...expectedRegionalReport, save });
      const res = await request.post('/').set('X-Auth-EmployeeEmail', 'mwillis@surfline.com').send({
        subregion: subregion._id,
        waterTempMin: 65,
        waterTempMax: 70,
        report: 'Some report',
      });
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        ...expectedRegionalReport,
        subregion: `${expectedRegionalReport.subregion}`,
        forecasterName: 'Johnny Tsunami',
        forecasterTitle: 'Surfline Employee',
      });
      expect(Subregion.findById).to.have.been.calledOnce();
      expect(Subregion.findById).to.have.been.calledWithExactly(`${subregion._id}`);
      expect(regionalReportModel.createRegionalReport).to.have.been.calledOnce();
      expect(regionalReportModel.createRegionalReport.firstCall.args).to.deep.equal([
        {
          ...expectedRegionalReport,
          subregion: `${expectedRegionalReport.subregion}`,
        },
      ]);
      expect(save).to.have.been.calledOnce();
    });

    it('should return 400 if the subregion is not found', async () => {
      const subregionId = mongoose.Types.ObjectId();
      Subregion.findById.resolves(null);

      try {
        const res = await request.post('/').send({
          subregion: `${subregionId}`,
        });
        expect(res).to.have.status(400);
      } catch (err) {
        expect(err.response).to.have.status(400);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Invalid subregion' });
        expect(Subregion.findById).to.have.been.calledOnce();
        expect(Subregion.findById).to.have.been.calledWithExactly(`${subregionId}`);
        expect(regionalReportModel.createRegionalReport).not.to.have.been.called();
      }
    });
  });

  describe('PUT /', () => {
    it('should update an existing report for the subregion', async () => {
      const regionalReportId = mongoose.Types.ObjectId();
      const subregionId = mongoose.Types.ObjectId();
      const save = sinon.stub().resolves();
      RegionalReport.findById.resolves({
        subregion: subregionId,
        legacySubregionId: 1234,
        waterTempMin: 60,
        waterTempMax: 65,
        report: 'Existing report notes',
        forecasterEmail: 'jwang@surfline.com',
        save,
      });
      const res = await request
        .put(`/${regionalReportId}`)
        .set('X-Auth-EmployeeEmail', 'mwillis@surfline.com')
        .send({
          subregion: mongoose.Types.ObjectId(), // Should not override existing
          legacySubregionId: 2345, // Should not override existing
          waterTempMin: 66,
          waterTempMax: 70,
          report: 'Updated report notes',
        });

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        subregion: `${subregionId}`,
        legacySubregionId: 1234,
        waterTempMin: 66,
        waterTempMax: 70,
        report: 'Updated report notes',
        forecasterEmail: 'jwang@surfline.com',
      });
      expect(RegionalReport.findById).to.have.been.calledOnce();
      expect(RegionalReport.findById).to.have.been.calledWithExactly(`${regionalReportId}`);
      expect(save).to.have.been.calledOnce();
    });

    it('should return 404 if the report does not exist', async () => {
      const regionalReportId = mongoose.Types.ObjectId();
      RegionalReport.findById.resolves(null);

      try {
        const res = await request.put(`/${regionalReportId}`).send();
        expect(res).to.have.status(404);
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Regional report not found' });
      }
    });
  });
});
