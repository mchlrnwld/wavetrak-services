/* eslint-disable no-duplicate-imports, import/no-duplicates */
import { expect } from 'chai';
import averageReports from './averageReports';

describe('validate averageReports', () => {
  it('should average two spots that have occasionalHeight', async () => {
    const spotId = '5842041f4e65fad6a77088ed';
    const spotReports = [
      {
        condition: 5,
        minHeight: 1,
        maxHeight: 2,
        occasionalHeight: 3,
        humanRelation: 'Knee to thigh',
        rating: 'NONE',
        waterTemp: {
          min: 55,
          max: 58,
          range: '55-58',
        },
        note: 'some note here',
        regionalReport: 'some html report',
        spot: spotId,
      },
      {
        condition: 5,
        minHeight: 2,
        maxHeight: 3,
        occasionalHeight: 4,
        humanRelation: 'Thigh to waist',
        rating: 'FAIR_TO_GOOD',
        waterTemp: {
          min: 55,
          max: 58,
          range: '55-58',
        },
        note: 'some note here',
        regionalReport: 'some html report',
        spot: spotId,
      },
    ];
    const average = averageReports(spotReports[0], spotReports[1]);
    expect(average.occasionalHeight).to.equal(3);
    expect(average.rating).to.equal('FAIR_TO_GOOD');
    expect(average.condition).to.equal(5);
    expect(average.waterTemp.range).to.equal('55-58');
    expect(average.waterTemp.max).to.equal(58);
    expect(average.waterTemp.min).to.equal(55);
  });
  it('should not average two spots with missing occasionalHeight', async () => {
    const spotId = '5842041f4e65fad6a77088ed';
    const spotReports = [
      {
        condition: 5,
        minHeight: 2,
        maxHeight: 3,
        humanRelation: 'Thigh to waist high',
        occasionalHeight: 0,
        rating: 'FAIR',
        waterTemp: {
          min: 55,
          max: 58,
          range: '55-58',
        },
        note: 'some note here',
        regionalReport: 'some html report',
        spot: spotId,
      },
      {
        condition: 5,
        minHeight: 2,
        maxHeight: 3,
        humanRelation: 'Thigh to waist high',
        occasionalHeight: 4,
        rating: 'FAIR_TO_GOOD',
        waterTemp: {
          min: 55,
          max: 58,
          range: '55-58',
        },
        note: 'some note here',
        regionalReport: 'some html report',
        spot: spotId,
      },
    ];
    const average = averageReports(spotReports[0], spotReports[1]);
    expect(average.occasionalHeight).to.equal(0);
    expect(average.rating).to.equal('FAIR');
    expect(average.condition).to.equal(5);
    expect(average.waterTemp.range).to.equal('55-58');
    expect(average.waterTemp.max).to.equal(58);
    expect(average.waterTemp.min).to.equal(55);
  });
  it('should return 0 for occasionalHeight if maxHeight equals occasionalHeight', async () => {
    const spotId = '5842041f4e65fad6a77088ed';
    const spotReports = [
      {
        condition: 5,
        minHeight: 2,
        maxHeight: 3,
        humanRelation: 'Thigh to waist high',
        occasionalHeight: 3,
        rating: 'FAIR',
        waterTemp: {
          min: 55,
          max: 58,
          range: '55-58',
        },
        note: 'some note here hey',
        regionalReport: 'some html report',
        spot: spotId,
      },
      {
        condition: 5,
        minHeight: 2,
        maxHeight: 3,
        humanRelation: 'Thigh to waist high',
        occasionalHeight: 4,
        rating: 'POOR_TO_FAIR',
        waterTemp: {
          min: 55,
          max: 58,
          range: '55-58',
        },
        note: 'some note here',
        regionalReport: 'some html report',
        spot: spotId,
      },
    ];
    const average = averageReports(spotReports[0], spotReports[1]);
    expect(average.occasionalHeight).to.equal(0);
    expect(average.rating).to.equal('POOR_TO_FAIR');
    expect(average.condition).to.equal(5);
    expect(average.waterTemp.range).to.equal('55-58');
    expect(average.waterTemp.max).to.equal(58);
    expect(average.waterTemp.min).to.equal(55);
  });
});
