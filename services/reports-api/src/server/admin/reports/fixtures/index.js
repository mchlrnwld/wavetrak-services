export const reportInput = {
  _id: '5822573e9aae0d00124a229f',
  condition: '6',
  occasionalHeight: '8',
  rating: 'POOR',
  spot: '584f0f0ba609b0c898cfd139',
  note: 'some note',
  waterTemp: {
    min: 48,
    max: 72,
  },
  regionalReport: '<html>Somthing to test</html>',
  forecasterEmail: 'forecasters@surfline.com',
  forecasterName: 'Johnny Tsunami',
  forecasterTitle: 'Surfline Employee',
};

export const updateReportInput = {
  _id: '5822573e9aae0d00124a229f',
  spot: '584f0f0ba609b0c898cfd139',
  condition: '4',
  occasionalHeight: '5',
  rating: 'FAIR_TO_GOOD',
  note: 'updated note',
  waterTemp: {
    min: 50,
    max: 70,
    range: '50-70',
  },
  regionalReport: '<html>updated regional report</html>',
};

export const reportOutput = {
  __v: 0,
  _id: '5822573e9aae0d00124a229f',
  condition: 6,
  occasionalHeight: 8,
  humanRelation: 'Waist to shoulder',
  maxHeight: 4,
  minHeight: 3,
  rating: 'POOR',
  spot: '584f0f0ba609b0c898cfd139',
  note: 'some note',
  waterTemp: {
    min: 48,
    max: 72,
    range: '48-72',
  },
  regionalReport: '<html>Somthing to test</html>',
  forecasterEmail: 'forecasters@surfline.com',
  forecasterName: 'Johnny Tsunami',
  forecasterTitle: 'Surfline Employee',
};

export const updatedReportOutput = {
  __v: 0,
  _id: '5822573e9aae0d00124a229f',
  condition: 4,
  occasionalHeight: 5,
  humanRelation: 'Thigh to stomach',
  maxHeight: 3,
  minHeight: 2,
  note: 'updated note',
  rating: 'FAIR_TO_GOOD',
  spot: '584f0f0ba609b0c898cfd139',
  waterTemp: {
    min: 50,
    max: 70,
    range: '50-70',
  },
  regionalReport: '<html>updated regional report</html>',
  forecasterEmail: 'forecasters@surfline.com',
  forecasterName: 'Johnny Tsunami',
  forecasterTitle: 'Surfline Employee',
};
