/* eslint-disable no-await-in-loop */
import { times } from 'lodash';
import mongoose from 'mongoose';
import { getSpot, getSpotsBySubregion } from '../../../common/spots';
import RegionalReport from '../../../model/RegionalReportModel';
import { relatedVirtualSpots } from '../../../model/SpotModel';
import SpotReport from '../../../model/SpotReportModel';
import Subregion from '../../../model/SubregionModel';
import averageReports from './averageReports';
import getForecasterDetails from './forecasterDetails';
import publishSpotTopic from './publishSpotTopic';

const thirtySixHours = 1000 * 60 * 60 * 36;
const isStale = (report) => new Date() - new Date(report.updatedAt) > thirtySixHours;

export const trackReportsRequests = (log) => (req, res, next) => {
  log.trace({
    action: '/admin/reports',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export const peekReportsHandler = async (req, res) => {
  const subregion = await Subregion.findOne({ _id: req.query.subregionId });
  if (!subregion) return res.status(404).send({ message: 'Subregion not found' });

  const body = await getSpotsBySubregion(subregion._id);
  const spots = body.filter((spot) => spot.humanReport && spot.humanReport.status === 'ON');

  const spotReports = await SpotReport.aggregate([
    {
      $match: { spot: { $in: spots.map((spot) => mongoose.Types.ObjectId(spot._id)) } },
    },
    {
      $sort: { createdAt: -1 },
    },
    {
      $group: { _id: '$spot', report: { $first: '$$ROOT' } },
    },
    {
      $lookup: { from: 'Spots', localField: 'report.spot', foreignField: '_id', as: 'spot' },
    },
    {
      $unwind: { path: '$spot' },
    },
    {
      $project: {
        _id: '$report._id',
        updatedAt: '$report.updatedAt',
        createdAt: '$report.createdAt',
        spot: { _id: '$spot._id', name: '$spot.name' },
        condition: '$report.condition',
        occasionalHeight: '$report.occasionalHeight',
        rating: '$report.rating',
        note: '$report.note',
        waterTemp: '$report.waterTemp',
        regionalReport: '$report.regionalReport',
      },
    },
  ]);

  const regionalReport = await RegionalReport.find({ subregion: subregion._id })
    .sort({ createdAt: -1 })
    .limit(1)
    .cursor()
    .next();

  return res.send({
    subregionId: subregion._id,
    subregionName: subregion.name,
    regionalReport: regionalReport || {
      subregion: subregion._id,
      legacySubregionId: subregion.legacySubregionId,
    },
    reports: spots.map(
      (spot) =>
        spotReports.find((report) => `${report.spot._id}` === `${spot._id}`) || {
          spot: { _id: spot._id, name: spot.name },
        },
    ),
  });
};

export const searchReportsHandler = async (req, res) => {
  const spots = req.query.spotIds.split(',');
  const spotReports = await SpotReport.aggregate([
    {
      $match: { spot: { $in: spots.map((spot) => new mongoose.Types.ObjectId(spot)) } },
    },
    {
      $sort: { createdAt: -1 },
    },
    {
      $group: { _id: '$spot', report: { $first: '$$ROOT' } },
    },
    {
      $lookup: { from: 'Spots', localField: 'report.spot', foreignField: '_id', as: 'spot' },
    },
    {
      $unwind: { path: '$spot' },
    },
    {
      $project: {
        _id: '$report._id',
        updatedAt: '$report.updatedAt',
        createdAt: '$report.createdAt',
        spot: {
          _id: '$spot._id',
          name: '$spot.name',
          legacyId: '$spot.legacyId',
          forecastLocation: '$spot.forecastLocation',
        },
        maxHeight: '$report.maxHeight',
        minHeight: '$report.minHeight',
        plusHeight: '$report.plusHeight',
        occasionalHeight: '$report.occasionalHeight',
      },
    },
  ]);

  return res.send({
    reports: spotReports,
  });
};

export const spotReportHandler = async (req, res) => {
  const { spotId } = req.query;
  const limit = Number(req.query.limit || 1);
  let spotReports = [];

  const spot = await getSpot(spotId);
  const { forecastLocation, legacyId } = spot;

  if (spot.humanReport.status === 'CLONE') {
    spotReports = await SpotReport.find({ spot: spot.humanReport.clone.spotId })
      .sort({ createdAt: -1 })
      .limit(limit)
      .lean();

    if (spotReports.length) {
      spotReports = spotReports.map((report) => ({
        ...report,
        spot: spotId,
        forecastLocation,
        legacyId,
      }));
    }
  } else if (spot.humanReport.status === 'AVERAGE') {
    const spotReportOne = await SpotReport.find({ spot: spot.humanReport.average.spotOneId })
      .sort({ createdAt: -1 })
      .limit(limit)
      .lean();
    const spotReportTwo = await SpotReport.find({ spot: spot.humanReport.average.spotTwoId })
      .sort({ createdAt: -1 })
      .limit(limit)
      .lean();

    const spotOneHasReport = spotReportOne.length > 0;
    const spotTwoHasReport = spotReportTwo.length > 0;

    if (spotOneHasReport && spotTwoHasReport) {
      spotReports = times(limit, (i) => ({
        ...averageReports(spotReportOne[i], spotReportTwo[i]),
        spot: spotId,
        forecastLocation,
        legacyId,
      }));
    } else if (spotOneHasReport) {
      spotReports = spotReportOne.map((report) => ({
        ...report,
        spot: spotId,
        forecastLocation,
        legacyId,
      }));
    } else if (spotTwoHasReport) {
      spotReports = spotReportTwo.map((report) => ({
        ...report,
        spot: spotId,
        forecastLocation,
        legacyId,
      }));
    }
  } else {
    spotReports = await SpotReport.find({ spot: { $eq: spotId } })
      .sort({ createdAt: -1 })
      .limit(limit)
      .lean();
  }

  // If no spot reports found, return the blank array
  return res.send({
    spotReports: spotReports.map((report) => ({
      ...report,
      stale: isStale(report),
      forecastLocation,
      legacyId,
    })),
  });
};

export const regionalReportHandler = async (req, res) => {
  const { subregionId } = req.query;
  const limit = Number(req.query.limit || 1);
  const regionalReports = await RegionalReport.find({ subregion: { $eq: subregionId } })
    .sort({ createdAt: -1 })
    .limit(limit)
    .lean();

  return res.send({
    regionalReports: regionalReports.map((report) => ({
      ...report,
      stale: isStale(report),
    })),
  });
};

export const createReportHandler = async (req, res) => {
  const savedReports = [];
  const errors = [];

  const { reports } = req.body;

  for (let i = 0; i < reports.length; i += 1) {
    const report = req.body.reports[i];
    try {
      const reportModel = new SpotReport({
        ...report,
        ...getForecasterDetails(req),
      });
      const result = await reportModel.save();
      const virtualSpots = await relatedVirtualSpots(reportModel.spot);
      await publishSpotTopic(reportModel.spot);
      await Promise.all(virtualSpots.map((spot) => publishSpotTopic(spot._id)));
      savedReports.push(result);
    } catch (err) {
      console.log(err);
      errors.push({ reason: err.message, spot: report.spot });
    }
  }

  if (errors.length > 0) {
    return res.status(500).send({
      message: 'Failed to create spot reports',
      savedReports: savedReports.map((report) => ({
        reportId: report._id,
        spot: report.spot,
      })),
      errors,
    });
  }

  return res.send({ reports: savedReports });
};

export const updateReportHandler = async (req, res) => {
  const { reports } = req.body;
  const savedReports = [];
  const errors = [];
  for (let i = 0; i < reports.length; i += 1) {
    const report = req.body.reports[i];
    try {
      const reportModel = await SpotReport.findById(report._id);
      if (!reportModel) throw new Error('Report does not exist');
      Object.assign(reportModel, report);
      // mark most important fields as modified, to ensure update
      reportModel.markModified('regionalReport');
      reportModel.markModified('humanRelation');
      reportModel.markModified('maxHeight');
      reportModel.markModified('minHeight');
      reportModel.markModified('occasionalHeight');
      reportModel.markModified('plusHeight');
      reportModel.markModified('condition');

      await reportModel.save();
      const virtualSpots = await relatedVirtualSpots(reportModel.spot);
      await publishSpotTopic(reportModel.spot);
      await Promise.all(virtualSpots.map((spot) => publishSpotTopic(spot._id)));
      savedReports.push(reportModel);
    } catch (err) {
      errors.push({
        reason: err.message,
        reportId: report._id,
        spot: report.spot,
      });
    }
  }
  if (errors.length > 0) {
    return res.status(500).send({
      message: 'Failed to update spot reports',
      savedReports: savedReports.map((report) => ({
        reportId: report._id,
        spot: report.spot,
      })),
      errors,
    });
  }
  return res.send({ reports: savedReports });
};
