import { surfConditions, surfRatings } from '../../../model/conditions';
import { floorAvg, roundAvg, averageFields } from '../../../utils/averageUtils';

const isMaxHeightEquivToOccHeight = (reportOne, reportTwo) =>
  reportOne.maxHeight === reportOne.occasionalHeight ||
  reportTwo.maxHeight === reportTwo.occasionalHeight;

const isMissingOccHeight = (reportOne, reportTwo) =>
  reportOne.occasionalHeight === 0 || reportTwo.occasionalHeight === 0;

const getOccasionalHeight = (reportOne, reportTwo) => {
  if (
    isMaxHeightEquivToOccHeight(reportOne, reportTwo) ||
    isMissingOccHeight(reportOne, reportTwo)
  ) {
    return 0;
  }
  return averageFields(reportOne.occasionalHeight, reportTwo.occasionalHeight, floorAvg);
};

const getCondition = (reportOne, reportTwo) => {
  const totalHeight = (condition) => condition.maxHeight + condition.minHeight;
  const averaged = averageFields(
    totalHeight(surfConditions[reportOne.condition]),
    totalHeight(surfConditions[reportTwo.condition]),
    (field1, field2) => Math.round((field1 + field2) / 2),
  );
  let conditionMatch;
  Object.keys(surfConditions).forEach((key) => {
    const conditionValueProximity = Math.abs(totalHeight(surfConditions[key]) - averaged);
    if (!conditionMatch || conditionValueProximity < conditionMatch.proximity) {
      conditionMatch = { proximity: conditionValueProximity, key };
    }
  });

  return parseInt(conditionMatch.key, 10);
};

const getRating = (reportOne, reportTwo) => {
  const surfRatingsArray = [];
  for (const key in surfRatings) { // eslint-disable-line
    surfRatingsArray.push(key);
  }
  const reportOneRating = surfRatingsArray.indexOf(reportOne.rating);
  const reportTwoRating = surfRatingsArray.indexOf(reportTwo.rating);
  return averageFields(reportOneRating, reportTwoRating, roundAvg, surfRatingsArray);
};

const getWaterTemp = (reportOne, reportTwo) => {
  const waterMax = floorAvg(reportOne.waterTemp.max, reportTwo.waterTemp.max);
  const waterMin = floorAvg(reportOne.waterTemp.min, reportTwo.waterTemp.min);
  return {
    range: `${waterMin}-${waterMax}`,
    max: waterMax,
    min: waterMin,
  };
};

const averageReports = (reportOne, reportTwo) => {
  const condition = getCondition(reportOne, reportTwo);
  return {
    ...reportOne,
    ...surfConditions[condition],
    occasionalHeight: getOccasionalHeight(reportOne, reportTwo),
    condition: getCondition(reportOne, reportTwo),
    rating: getRating(reportOne, reportTwo),
    waterTemp: getWaterTemp(reportOne, reportTwo),
  };
};

export default averageReports;
