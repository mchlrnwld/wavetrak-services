import AWS from 'aws-sdk';
import messageConfig from '../../../messageConfig';

const publishSpotTopic = (spotId) =>
  new Promise((resolve, reject) => {
    if (process.env.NODE_ENV === 'test') return resolve();

    AWS.config.update({
      region: messageConfig.region,
    });

    const sns = new AWS.SNS({ apiVersion: messageConfig.version });
    const topicArn = messageConfig.topic;
    const payload = { spotId: `${spotId}`, clearCF: true };
    const subject = 'Alert Cache Endpoint';
    const params = {
      TopicArn: topicArn,
      Message: JSON.stringify({
        default: JSON.stringify(payload),
      }),
      MessageStructure: 'json',
      Subject: subject,
    };

    return sns.publish(params, (err, data) => {
      if (err) return reject(err);
      return resolve(data);
    });
  });

export default publishSpotTopic;
