import chai, { expect } from 'chai';
import { json } from 'body-parser';
import sinon from 'sinon';
import express from 'express';
import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

import reportsAPI from '.';
import RegionalReport from '../../../model/RegionalReportModel';
import SpotReport from '../../../model/SpotReportModel';
import Subregion from '../../../model/SubregionModel';
import * as publishSpotTopic from './publishSpotTopic';
import * as spotsAPI from '../../../common/spots';
import * as SpotModel from '../../../model/SpotModel';
import { reportOutput, reportInput, updateReportInput, updatedReportOutput } from './fixtures';

/**
 * @param {string} spotId
 * @param {string} clonedSpotId
 */
const createClonedSpot = (spotId, clonedSpotId) =>
  SpotModel.SpotModel.collection.insertOne({
    _id: mongoose.Types.ObjectId(spotId),
    humanReport: {
      status: 'CLONE',
      clone: {
        spotId: mongoose.Types.ObjectId(clonedSpotId),
      },
    },
  });

/**
 * @param {string} spotId
 * @param {string} averagedSpotId
 */
const createAveragedSpot = (spotId, averagedSpotId) =>
  SpotModel.SpotModel.collection.insertOne({
    _id: mongoose.Types.ObjectId(spotId),
    humanReport: {
      status: 'AVERAGE',
      average: {
        spotOneId: mongoose.Types.ObjectId(averagedSpotId),
        spotTwoId: mongoose.Types.ObjectId(),
      },
    },
  });

const clonedSpotId1 = mongoose.Types.ObjectId();
const clonedSpotId2 = mongoose.Types.ObjectId();

const averagedSpotId1 = mongoose.Types.ObjectId();
const averagedSpotId2 = mongoose.Types.ObjectId();

describe('/admin/reports', () => {
  let clock;
  let log;
  /** @type {ChaiHttp.Agent} */
  let request;
  /** @type {MongoMemoryServer} */
  let mongoServer;

  const currentDate = 1629183600; // 2021-08-17T00:00:00 PDT

  before(async () => {
    mongoServer = await MongoMemoryServer.create();

    mongoose.connect(mongoServer.getUri(), { useNewUrlParser: true, useUnifiedTopology: true });

    log = { trace: sinon.spy() };
    const app = express();
    app.use(json());
    app.use(reportsAPI(log));
    request = chai.request(app).keepOpen();
  });

  beforeEach(() => {
    clock = sinon.useFakeTimers(currentDate * 1000);
    sinon.stub(publishSpotTopic, 'default');
  });

  afterEach(async () => {
    publishSpotTopic.default.restore();
    clock.restore();

    await SpotModel.SpotModel.deleteMany({});
    await SpotReport.deleteMany({});
    await RegionalReport.deleteMany({});
    await Subregion.deleteMany({});
  });

  after(async () => {
    request.close();
    mongoose.disconnect();
  });

  describe('GET /conditions', () => {
    it('should return a set of conditions suitable for forms', async () => {
      const res = await request.get('/conditions').send();
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.have.property('conditions');
      expect(res.body).to.have.property('occ');
      expect(res.body).to.have.property('ratings');
      expect(res.body.conditions).to.be.instanceOf(Array);
      expect(res.body.occ).to.be.instanceOf(Array);
      expect(res.body.ratings).to.be.instanceOf(Array);
    });
  });

  describe('GET /peek', () => {
    it('should return latest reports for a subregion', async () => {
      const subregion = {
        _id: mongoose.Types.ObjectId(),
        name: 'North Orange County',
      };
      const spots = [
        {
          _id: mongoose.Types.ObjectId(),
          name: 'HB Pier, Southside',
          humanReport: {
            status: 'ON',
          },
        },
        {
          _id: mongoose.Types.ObjectId(),
          name: 'HB Pier, Northside',
          humanReport: {
            status: 'OFF',
          },
        },
        {
          _id: mongoose.Types.ObjectId(),
          name: 'Anderson Street',
        },
        {
          _id: mongoose.Types.ObjectId(),
          name: 'Seal Beach',
          humanReport: {
            status: 'ON',
          },
        },
        {
          _id: mongoose.Types.ObjectId(),
          name: '40 St. Newport',
          humanReport: {
            status: 'ON',
          },
        },
      ];
      const regionalReport = {
        _id: `${mongoose.Types.ObjectId()}`,
        subregion: `${subregion._id}`,
        legacySubregionId: 1234,
        waterTemp: {
          min: 65,
          max: 70,
          range: '65-70',
        },
        report: 'Some forecaster report',
        forecasterEmail: 'kslater@surfline.com',
      };
      const spotReports = [
        {
          _id: mongoose.Types.ObjectId(),
          spot: { _id: spots[0]._id, name: 'HB Pier, Southside' },
          condition: 'FAIR_TO_GOOD',
        },
        {
          _id: mongoose.Types.ObjectId(),
          spot: { _id: spots[3]._id, name: 'Seal Beach' },
          condition: 'FAIR_TO_GOOD',
        },
      ];
      try {
        sinon.stub(Subregion, 'findOne').resolves(subregion);
        sinon.stub(SpotReport, 'aggregate').resolves(spotReports);
        sinon.stub(spotsAPI, 'getSpotsBySubregion').resolves(spots);
        const next = sinon.stub().resolves(regionalReport);
        const cursor = sinon.stub().returns({ next });
        const limit = sinon.stub().returns({ cursor });
        const sort = sinon.stub().returns({ limit });
        sinon.stub(RegionalReport, 'find').returns({ sort });
        const res = await request.get(`/peek/?subregionId=${subregion._id}`).send();

        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal({
          subregionId: `${subregion._id}`,
          subregionName: 'North Orange County',
          regionalReport,
          reports: [
            {
              _id: `${spotReports[0]._id}`,
              condition: 'FAIR_TO_GOOD',
              spot: {
                _id: `${spots[0]._id}`,
                name: 'HB Pier, Southside',
              },
            },
            {
              _id: `${spotReports[1]._id}`,
              condition: 'FAIR_TO_GOOD',
              spot: {
                _id: `${spots[3]._id}`,
                name: 'Seal Beach',
              },
            },
            {
              spot: {
                _id: `${spots[4]._id}`,
                name: '40 St. Newport',
              },
            },
          ],
        });
        expect(Subregion.findOne).to.have.been.calledOnce();
        expect(Subregion.findOne.firstCall.args).to.deep.equal([{ _id: `${subregion._id}` }]);
        expect(SpotReport.aggregate).to.have.been.calledOnce();
        expect(SpotReport.aggregate.firstCall.args[0][0]).to.deep.equal({
          $match: {
            spot: {
              $in: [spots[0]._id, spots[3]._id, spots[4]._id],
            },
          },
        });
        expect(RegionalReport.find).to.have.been.calledOnce();
        expect(RegionalReport.find.firstCall.args).to.deep.equal([
          {
            subregion: subregion._id,
          },
        ]);
      } finally {
        Subregion.findOne.restore();
        spotsAPI.getSpotsBySubregion.restore();
        SpotReport.aggregate.restore();
        RegionalReport.find.restore();
      }
    });

    it('should return 404 if subregion not found', async () => {
      try {
        const subregionId = new mongoose.Types.ObjectId();
        sinon.stub(Subregion, 'findOne').resolves(null);
        const response = await request.get(`/peek/?subregionId=${subregionId.toString()}`).send();
        expect(response).to.have.status(404);
      } catch (err) {
        expect(err.response).to.have.status(404);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal({ message: 'Subregion not found' });
      }
    });
  });

  describe('POST /', () => {
    it('should create a report', async () => {
      const reports = [reportInput];
      const res = await request.post('/').send({ reports });

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        reports: [
          {
            ...reportOutput,
            createdAt: '2021-08-17T07:00:00.000Z',
            updatedAt: '2021-08-17T07:00:00.000Z',
          },
        ],
      });

      expect(publishSpotTopic.default).to.have.been.calledOnceWithExactly(
        mongoose.Types.ObjectId(reportInput.spot),
      );

      const document = await SpotReport.findOne({ _id: reportInput._id }).lean();
      expect(document).to.exist();
      expect(document.condition).to.be.equal(reportOutput.condition);
      expect(document.occasionalHeight).to.be.equal(reportOutput.occasionalHeight);
    });

    it('should publish the spot topic for cloned spots', async () => {
      const reports = [reportInput];

      await createClonedSpot(clonedSpotId1, reportInput.spot);
      await createClonedSpot(clonedSpotId2, reportInput.spot);

      const res = await request.post('/').send({ reports });

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        reports: [
          {
            ...reportOutput,
            createdAt: '2021-08-17T07:00:00.000Z',
            updatedAt: '2021-08-17T07:00:00.000Z',
          },
        ],
      });

      const document = await SpotReport.findOne({ _id: reportInput._id }).lean();
      expect(document).to.exist();
      expect(document.condition).to.be.equal(reportOutput.condition);
      expect(document.occasionalHeight).to.be.equal(reportOutput.occasionalHeight);

      expect(publishSpotTopic.default).to.have.been.calledThrice();
      expect(publishSpotTopic.default.getCall(0)).to.have.been.calledWithExactly(
        mongoose.Types.ObjectId(reportInput.spot),
      );
      expect(publishSpotTopic.default.getCall(1)).to.have.been.calledWithExactly(clonedSpotId1);
      expect(publishSpotTopic.default.getCall(2)).to.have.been.calledWithExactly(clonedSpotId2);
    });

    it('should publish the spot topic for averaged spots', async () => {
      const reports = [reportInput];

      await createAveragedSpot(averagedSpotId1, reportInput.spot);
      await createAveragedSpot(averagedSpotId2, reportInput.spot);

      const res = await request.post('/').send({ reports });

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        reports: [
          {
            ...reportOutput,
            createdAt: '2021-08-17T07:00:00.000Z',
            updatedAt: '2021-08-17T07:00:00.000Z',
          },
        ],
      });

      const document = await SpotReport.findOne({ _id: reportInput._id }).lean();
      expect(document).to.exist();
      expect(document.condition).to.be.equal(reportOutput.condition);
      expect(document.occasionalHeight).to.be.equal(reportOutput.occasionalHeight);

      expect(publishSpotTopic.default).to.have.been.calledThrice();
      expect(publishSpotTopic.default.getCall(0)).to.have.been.calledWithExactly(
        mongoose.Types.ObjectId(reportInput.spot),
      );
      expect(publishSpotTopic.default.getCall(1)).to.have.been.calledWithExactly(averagedSpotId1);
      expect(publishSpotTopic.default.getCall(2)).to.have.been.calledWithExactly(averagedSpotId2);
    });
  });

  describe('PUT /', () => {
    const reportModel = {
      _id: '5822573e9aae0d00124a229f',
      condition: '6',
      occasionalHeight: '8',
      rating: 'POOR',
      spot: '584f0f0ba609b0c898cfd139',
      note: 'some note',
      waterTemp: {
        min: 48,
        max: 72,
      },
      regionalReport: '<html>Somthing to test</html>',
      forecasterEmail: 'forecasters@surfline.com',
      forecasterName: 'Johnny Tsunami',
      forecasterTitle: 'Surfline Employee',
    };

    it('should update report', async () => {
      try {
        const existingReportModel = {
          _id: reportModel._id,
          condition: '4',
          occasionalHeight: '5',
          rating: 'FAIR_TO_GOOD',
          note: 'existing note',
          waterTemp: {
            min: 50,
            max: 70,
            range: '50-70',
          },
          regionalReport: '<html>Existing regional report</html>',
          save: sinon.stub().resolves(),
          markModified: sinon.stub().resolves(),
        };
        const reports = Array(...Array(3)).map(() => reportModel);
        sinon.stub(SpotReport, 'findById').resolves(existingReportModel);
        sinon.stub(SpotModel, 'relatedVirtualSpots').resolves([]);
        const res = await request.put('/').send({ reports });
        expect(res).to.have.status(200);
        expect(res).to.be.json();
        expect(res.body).to.deep.equal({ reports });
        expect(SpotReport.findById).to.have.been.calledThrice();
        expect(SpotReport.findById.firstCall).to.have.been.calledWithExactly(reportModel._id);
        expect(SpotReport.findById.secondCall).to.have.been.calledWithExactly(reportModel._id);
        expect(SpotReport.findById.thirdCall).to.have.been.calledWithExactly(reportModel._id);
        expect(existingReportModel.save).to.have.been.calledThrice();
        expect(publishSpotTopic.default).to.have.been.calledThrice();
        expect(publishSpotTopic.default.firstCall).to.have.been.calledWithExactly(reportModel.spot);
        expect(publishSpotTopic.default.secondCall).to.have.been.calledWithExactly(
          reportModel.spot,
        );
        expect(publishSpotTopic.default.thirdCall).to.have.been.calledWithExactly(reportModel.spot);
      } finally {
        SpotModel.relatedVirtualSpots.restore();
        SpotReport.findById.restore();
      }
    });

    it('should publish the spot topic for cloned spots', async () => {
      await SpotReport.create(reportInput);
      const reports = [updateReportInput];

      await createClonedSpot(clonedSpotId1, updateReportInput.spot);
      await createClonedSpot(clonedSpotId2, updateReportInput.spot);

      const res = await request.put('/').send({ reports });

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        reports: [
          {
            ...updatedReportOutput,
            createdAt: '2021-08-17T07:00:00.000Z',
            updatedAt: '2021-08-17T07:00:00.000Z',
          },
        ],
      });

      const document = await SpotReport.findOne({ _id: updateReportInput._id }).lean();
      expect(document).to.exist();
      expect(document.condition).to.be.equal(updatedReportOutput.condition);
      expect(document.occasionalHeight).to.be.equal(updatedReportOutput.occasionalHeight);

      expect(publishSpotTopic.default).to.have.been.calledThrice();
      expect(publishSpotTopic.default.getCall(0)).to.have.been.calledWithExactly(
        mongoose.Types.ObjectId(updateReportInput.spot),
      );
      expect(publishSpotTopic.default.getCall(1)).to.have.been.calledWithExactly(clonedSpotId1);
      expect(publishSpotTopic.default.getCall(2)).to.have.been.calledWithExactly(clonedSpotId2);
    });

    it('should publish the spot topic for averaged spots', async () => {
      await SpotReport.create(reportInput);
      const reports = [updateReportInput];

      await createAveragedSpot(averagedSpotId1, updateReportInput.spot);
      await createAveragedSpot(averagedSpotId2, updateReportInput.spot);

      const res = await request.put('/').send({ reports });

      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        reports: [
          {
            ...updatedReportOutput,
            createdAt: '2021-08-17T07:00:00.000Z',
            updatedAt: '2021-08-17T07:00:00.000Z',
          },
        ],
      });

      const document = await SpotReport.findOne({ _id: updateReportInput._id }).lean();
      expect(document).to.exist();
      expect(document.condition).to.be.equal(updatedReportOutput.condition);
      expect(document.occasionalHeight).to.be.equal(updatedReportOutput.occasionalHeight);

      expect(publishSpotTopic.default).to.have.been.calledThrice();
      expect(publishSpotTopic.default.getCall(0)).to.have.been.calledWithExactly(
        mongoose.Types.ObjectId(updateReportInput.spot),
      );
      expect(publishSpotTopic.default.getCall(1)).to.have.been.calledWithExactly(averagedSpotId1);
      expect(publishSpotTopic.default.getCall(2)).to.have.been.calledWithExactly(averagedSpotId2);
    });

    it('should return 500 if report not found', async () => {
      const errModel = {
        message: 'Failed to update spot reports',
        savedReports: [],
        errors: [
          {
            reason: 'Report does not exist',
            reportId: '5822573e9aae0d00124a229f',
            spot: '584f0f0ba609b0c898cfd139',
          },
        ],
      };
      try {
        sinon.stub(SpotReport, 'findById').resolves(null);
        const res = await request.put('/').send({ reports: [reportModel] });
        expect(res).to.have.status(500);
      } catch (err) {
        expect(err.response).to.have.status(500);
        expect(err.response).to.be.json();
        expect(err.response.body).to.deep.equal(errModel);
      } finally {
        SpotReport.findById.restore();
      }
    });
  });

  describe('GET /spot', () => {
    beforeEach(() => {
      sinon.stub(SpotReport, 'find');
    });

    afterEach(() => {
      SpotReport.find.restore();
    });

    it('should return latest report for a spot', async () => {
      const spotId = '5842041f4e65fad6a77088ed';
      const spotReports = [
        {
          condition: 5,
          minHeight: 2,
          maxHeight: 3,
          occasionalHeight: 4,
          humanRelation: 'Thigh to waist',
          rating: 'FAIR_TO_GOOD',
          waterTemp: {
            min: 55,
            max: 58,
            range: '55-58',
          },
          note: 'some note here',
          regionalReport: 'some html report',
          spot: spotId,
        },
      ];
      const lean = sinon.stub().resolves(spotReports);
      const limit = sinon.stub().returns({ lean });
      const sort = sinon.stub().returns({ limit });
      SpotReport.find.returns({ sort });

      const res = await request.get(`/spot?spotId=${spotId}&limit=1`);
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        spotReports: spotReports.map((report) => ({
          ...report,
          forecastLocation: {
            coordinates: [-118.007, 33.656],
          },
          legacyId: 4874,
          stale: false,
        })),
      });
      expect(SpotReport.find).to.have.been.calledOnce();
      expect(SpotReport.find.firstCall.args).to.deep.equal([{ spot: { $eq: spotId } }]);
      expect(sort).to.have.been.calledOnce();
      expect(sort.firstCall.args).to.deep.equal([{ createdAt: -1 }]);
      expect(limit).to.have.been.calledOnce();
      expect(limit).to.have.been.calledWithExactly(1);
      expect(lean).to.have.been.calledOnce();
    });
  });

  describe('GET /regional', () => {
    beforeEach(() => {
      sinon.stub(RegionalReport, 'find');
    });

    afterEach(() => {
      RegionalReport.find.restore();
    });

    it('should return latest regional report for a spot', async () => {
      const subregionId = '5842041f4e65fad6a77088ab';
      const regionalReports = [
        {
          condition: 5,
          minHeight: 2,
          maxHeight: 3,
          occasionalHeight: 4,
          humanRelation: 'Thigh to waist',
          rating: 'FAIR_TO_GOOD',
          waterTemp: {
            min: 55,
            max: 58,
            range: '55-58',
          },
          note: 'some note here',
          regionalReport: 'some html report',
          subregion: subregionId,
        },
      ];

      const lean = sinon.stub().resolves(regionalReports);
      const limit = sinon.stub().returns({ lean });
      const sort = sinon.stub().returns({ limit });
      RegionalReport.find.returns({ sort });

      const res = await request.get(`/regional?subregionId=${subregionId}&limit=1`);
      expect(res).to.have.status(200);
      expect(res).to.be.json();
      expect(res.body).to.deep.equal({
        regionalReports: regionalReports.map((report) => ({ ...report, stale: false })),
      });
      expect(RegionalReport.find).to.have.been.calledOnce();
      expect(sort).to.have.been.calledOnce();
      expect(sort.firstCall.args).to.deep.equal([{ createdAt: -1 }]);
      expect(limit).to.have.been.calledOnce();
      expect(limit).to.have.been.calledWithExactly(1);
      expect(lean).to.have.been.calledOnce();
    });
  });
});
