const getForecasterDetails = (req) => ({
  forecasterEmail: req.get('X-Auth-EmployeeEmail') || 'forecasters@surfline.com',
  forecasterName: req.get('X-Auth-EmployeeName') || 'Johnny Tsunami',
  forecasterTitle: req.get('X-Auth-EmployeeTitle') || 'Surfline Employee',
});

export default getForecasterDetails;
