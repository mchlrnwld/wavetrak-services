import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import regional from './regional';
import getConditionOptions from './conditions';
import {
  trackReportsRequests,
  peekReportsHandler,
  spotReportHandler,
  updateReportHandler,
  createReportHandler,
  searchReportsHandler,
  regionalReportHandler,
} from './reports';

const reports = (log) => {
  const api = Router();

  api.get('/health', (_, res) =>
    res.send({
      status: 200,
      message: 'OK',
      version: process.env.APP_VERSION || 'unknown',
    }),
  );

  // Placed before trackReportsRequests so that we can use a more specific trackReportsRequests
  api.use('/regional', regional(log));

  api.use('*', trackReportsRequests(log));
  api.get('/peek', wrapErrors(peekReportsHandler));
  api.get('/spot', wrapErrors(spotReportHandler));
  api.get('/regional', wrapErrors(regionalReportHandler));
  api.get('/', wrapErrors(searchReportsHandler));
  api.put('/', wrapErrors(updateReportHandler));
  api.post('/', wrapErrors(createReportHandler));
  api.get('/conditions', wrapErrors(getConditionOptions));

  return api;
};

export default reports;
