import { Router } from 'express';
import reports from './reports';

const admin = (log) => {
  const api = Router();

  api.use('/reports', reports(log));

  return api;
};

export default admin;
