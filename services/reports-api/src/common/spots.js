import fetch from 'node-fetch';
import config from '../config';

export const getSpot = async (spotId) => {
  const url = `${config.SPOTS_API}/admin/spots/${spotId}`;
  const response = await fetch(url, {});
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};

export const getSpotsBySubregion = async (subregionId) => {
  const url = `${config.SPOTS_API}/admin/subregions/${subregionId}/spots`;
  const response = await fetch(url, {});
  const body = await response.json();

  if (response.status === 200) return body;
  throw body;
};
