import { Matchers } from 'pact';
import { objectId, humanReportStatus } from '../../matchers';

const { somethingLike } = Matchers;

export default {
  _id: objectId('5842041f4e65fad6a77088ed'),
  legacyId: somethingLike(4874),
  forecastLocation: {
    coordinates: somethingLike([-118.007, 33.656]),
  },
  humanReport: {
    status: humanReportStatus('ON'),
  },
};
