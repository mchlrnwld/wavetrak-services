import baseProvider from '../baseProvider';
import spot from './responses/spot';
import { applicationJSON } from '../matchers';
import testconfig from '../../../testconfig';

const spotInteraction = {
  state: 'spots exist',
  uponReceiving: 'a request for a spot',
  withRequest: {
    method: 'GET',
    path: '/admin/spots/5842041f4e65fad6a77088ed',
  },
  willRespondWith: {
    status: 200,
    headers: { 'Content-Type': applicationJSON },
    body: spot,
  },
};

const setupSpotsPactServer = async () => {
  const provider = baseProvider({
    provider: 'spots-api',
    port: testconfig.SPOTS_API_PORT,
  });

  await provider.setup();

  await Promise.all([spotInteraction].map((interaction) => provider.addInteraction(interaction)));

  return provider;
};

export default setupSpotsPactServer;
