import path from 'path';
import pact from 'pact';

const baseProvider = (options) =>
  pact({
    dir: path.resolve(process.cwd(), 'pacts'),
    spec: 2,
    consumer: 'reports-api',
    ...options,
  });

export default baseProvider;
