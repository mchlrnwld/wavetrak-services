import { Matchers } from 'pact';

const { term } = Matchers;

export const applicationJSON = term({
  matcher: 'application/json',
  generate: 'application/json',
});

export const objectId = (generate) =>
  term({
    matcher: '^[a-zA-Z0-9]{24}$',
    generate,
  });

export const humanReportStatus = (generate) =>
  term({
    matcher: '^(OFF|ON|AVERAGE|CLONE)$',
    generate,
  });
