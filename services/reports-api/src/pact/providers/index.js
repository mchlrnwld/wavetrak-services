import setupSpotsPactServer from './spots';

export const setupPactServers = () => Promise.all([setupSpotsPactServer()]);

export const teardownPactServers = (providers) =>
  Promise.all(
    providers.map(async ({ verify, finalize }) => {
      await verify();
      await finalize();
    }),
  );
