provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "services/reports-api/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = local.alb_listener_name
}

locals {
  dns_name          = "reports-api.prod.surfline.com"
  sns_policy        = "arn:aws:iam::833713747344:policy/sns_policy_spot_report_updated_prod"
  alb_listener_name = "sl-int-core-srvs-4-prod"
}

module "reports-api" {
  source = "../../"

  company     = "sl"
  application = "reports-api"
  environment = "prod"

  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  service_td_count = 6
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "path-pattern"
      value = "/admin/reports/*"
    },
  ]

  iam_role_arn           = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  load_balancer_arn      = data.aws_alb.main_internal.arn
  alb_listener_arn_http  = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-4-prod/689f354baf4e976c/d6a5a0222c6ea0c3"

  dns_name    = local.dns_name
  dns_zone_id = "Z3LLOZIY0ZZQDE"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 70
  auto_scaling_min_size     = 2
  auto_scaling_max_size     = 5000
}

resource "aws_route53_record" "reports_prod" {
  zone_id = "ZY7MYOQ65TY5X"
  name    = "reports-api.surfline.com"
  type    = "A"
  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = true
  }
}

resource "aws_iam_role_policy_attachment" "reports_api_sns_spot_report_updated" {
  role       = "reports_api_task_role_prod"
  policy_arn = local.sns_policy
}
