provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/reports-api/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = local.alb_listener_name
}

locals {
  dns_name          = "reports-api.staging.surfline.com"
  sns_policy        = "arn:aws:iam::665294954271:policy/sns_policy_spot_report_updated_staging"
  alb_listener_name = "sl-int-core-srvs-4-staging"
}

module "reports-api" {
  source = "../../"

  company     = "sl"
  application = "reports-api"
  environment = "staging"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-staging"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "path-pattern"
      value = "/admin/reports/*"
    },
  ]

  iam_role_arn           = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn      = data.aws_alb.main_internal.arn
  alb_listener_arn_http  = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-4-staging/26ee81426b4723db/a6bb3e305cea13f5"

  dns_name    = local.dns_name
  dns_zone_id = "Z3JHKQ8ELQG5UE"

  auto_scaling_enabled = false
}

resource "aws_iam_role_policy_attachment" "reports_api_sns_spot_report_updated" {
  role       = "reports_api_task_role_staging"
  policy_arn = local.sns_policy
}
