docker-compose \
	-f docker-compose.test.yml \
	-f docker-compose.integration.yml \
	up \
	--build \
	--no-color \
	--force-recreate \
	--exit-code-from integration-tests
