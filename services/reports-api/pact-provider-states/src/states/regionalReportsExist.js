import { ObjectID } from 'mongodb';

const regionalReportsExist = db => db.collection('RegionalReports').insertMany([
  {
    _id: new ObjectID('59d3c62ede536d0015a7874f'),
    updatedAt: new Date('2017-10-03T17:17:34.383+0000'),
    createdAt: new Date('2017-10-03T17:17:34.383+0000'),
    subregion: new ObjectID('58581a836630e24c44878fd6'),
    waterTemp: {
      range: '63-65',
      max: 65,
      min: 63,
    },
    report: '<p><strong>North Orange County Forecast</strong> effective Tuesday afternoon.<strong>Forecast Outlook:</strong><br>\n<strong>FRIDAY:</strong>&nbsp;Old&nbsp;S-SSE swell&nbsp;fades, as&nbsp;fresh/long period SSW swell builds in. Small NW windswell mixes in. Most breaks are in the thigh-waist-chest&nbsp;high range, while summertime standouts are shoulder+&nbsp;high on sets.&nbsp;<br>\n</p>\n<p><strong>WEATHER/WIND:</strong>&nbsp;Light/variable to light onshore winds early, trending onshore mid to late morning then rising to a light+ to locally moderate westerly sea-breeze in the afternoon. Overcast&nbsp;early, then clearing in afternoon hours.&nbsp;</p>',
    legacySubregionId: 2143,
    forecasterEmail: 'gmuthuraja@sirahu.com',
    forecasterName: 'Gowtham Muthuraja',
    forecasterTitle: 'Employee',
  },
]);

export default regionalReportsExist;
