import { ObjectID } from 'mongodb';

const spotReportsExist = db => db.collection('SpotReports').insertMany([
  {
    _id: new ObjectID('596cee75b438030011cbe48d'),
    updatedAt: new Date('2017-07-17T17:05:57.025+0000'),
    createdAt: new Date('2017-07-17T17:05:57.025+0000'),
    humanRelation: 'waist to shoulder',
    maxHeight: 4,
    minHeight: 3,
    spot: new ObjectID('5842041f4e65fad6a77088ed'),
    condition: 12,
    occasionalHeight: 4,
    rating: 'GOOD',
    note: '',
    waterTemp: {
      range: '70-75',
      max: 75,
      min: 70,
    },
    regionalReport: '<p><strong>Dawn Patrol Report for North OC:</strong> A fairly clean, peaky blend of rising NW windswell and overlapping SSW swells is on tap across the region this morning. Better exposed breaks are running thigh-waist-chest high, occasional peaks pushing shoulder high to a little larger for combo magnets from Newport through HB. Most of the region is a little drained as the tide approaches a -0.4\' low at 7:38am. HB Pier</p>\n<p><br>\n<strong>Short-Term Forecast for North OC:</strong> Light+ Southerly flow develops through the morning tide push, trending more SW\'erly and coming up a notch in the afternoon as the tide hits a 4.3\' high at 2:28pm. Peaky combo continues through the day, a window of improving conditions again before dark as onshore flow eases.&nbsp;</p>\n<p><br></p>',
    forecasterEmail: 'gmuthuraja@sirahu.com',
    forecasterName: 'Gowtham Muthuraja',
    forecasterTitle: 'Employee',
    plusHeight: true,
  },
]);

export default spotReportsExist;
