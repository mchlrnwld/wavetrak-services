import connectMongo from './helpers/connectMongo';
import server from './server';

connectMongo().then(server).catch((err) => {
  console.error(err);
  process.exit(-1);
});
