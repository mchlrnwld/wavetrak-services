import UnauthorizedError from './lib/Unauthorized';
import APIError from './lib/APIError';
import ResourceNotFound from './lib/ResourceNotFound';

export default {
  // Generic
  UnauthorizedError,

  // API
  APIError,
  ResourceNotFound,
};
