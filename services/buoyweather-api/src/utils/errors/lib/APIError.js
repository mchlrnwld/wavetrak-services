import BaseError from './BaseError';

class APIError extends BaseError {
  constructor(message, code = 400) {
    super();
    this.statusCode = code;
    this.message = message;
  }
}

export default APIError;
