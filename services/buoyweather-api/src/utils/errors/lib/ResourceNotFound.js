import BaseError from './BaseError';

class ResourceNotFound extends BaseError {
  constructor(resourceId) {
    super();
    this.statusCode = 404;
    this.message = `The resource ${resourceId} could not be found.`;
  }
}

export default ResourceNotFound;
