import BaseError from './BaseError';

class Unauthorized extends BaseError {
  constructor() {
    super();
    this.statusCode = 401;
    this.message = 'You are not authorized to view the resource';
  }
}

export default Unauthorized;
