
const round = (value, decimals) => {
  const rounded = Math.round(`${value}e${decimals}`);
  return Number(`${rounded}e-${decimals}`);
};

export default round;
