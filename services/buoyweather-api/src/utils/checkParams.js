import coordinates from '../utils/coordinates';

const checkParams = (params, favorites, checkForDuplicate = true) => {
  const issues = [];
  if (!params.point || params.point.length !== 2 || !params.name) {
    issues.push('One of the provided parameters is incorrect.');
  }

  if (params.name.length > 100) {
    issues.push('The name contains too many characters.');
  }

  if (checkForDuplicate) {
    // eslint-disable-next-line
    for (const favorite of favorites) {
      if (favorite.name.toLowerCase() === params.name.toLowerCase()) {
        issues.push('A favorite exists with the same name.');
      }
      if (coordinates.match(params.point, favorite.point) && params.product === favorite.product) {
        issues.push('This favorite already exists.');
      }
    }
  }

  return issues;
};

export default checkParams;
