export const getDayNumber = (dateString) => {
  const date = new Date(dateString);
  return date.getDate();
};

export const getMonthNumber = (dateString) => {
  const date = new Date(dateString);
  return date.getMonth() + 1;
};

export const getDayOfWeek = (dateString) => {
  const date = new Date(dateString);
  const weekday = [];
  weekday[0] = 'Sun';
  weekday[1] = 'Mon';
  weekday[2] = 'Tue';
  weekday[3] = 'Wed';
  weekday[4] = 'Thu';
  weekday[5] = 'Fri';
  weekday[6] = 'Sat';
  return weekday[date.getDay()];
};

export const chartDayFormat = dateString => `${getDayOfWeek(dateString)} ${getMonthNumber(dateString)}/${getDayNumber(dateString)}`;

export const isEmpty = value => value === null || value === undefined;
