const weatherCondition = (condition, night = false) => `${night ? 'NIGHT_' : ''}${condition}`;

export default weatherCondition;
