
export const maxArrayValue = (arr, elem) => {
  const elemArr = arr.map(obj => obj[elem]);
  return Math.max(...elemArr);
};

export const minArrayValue = (arr, elem) => {
  const elemArr = arr.map(obj => obj[elem]);
  return Math.min(...elemArr);
};

export const meanArrayValue = (arr, elem) => {
  const meanValue = arr
    .map(obj => obj[elem])
    .reduce((prev, curr) => prev + curr, 0) / arr.length;
  return meanValue;
};
