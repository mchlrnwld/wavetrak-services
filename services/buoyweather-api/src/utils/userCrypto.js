import crypto from 'crypto';
// import Logger from './logger';

const algorithm = 'aes-256-cbc';
const password = 'vmNbD8Nwxr%thyTkcA6Uo2U>wtTtbtcv';
const iv = '1234567812345678';

// let logger = new Logger();

/**
 * Encrypts a user's id using AES256 in CTR mode. In order to increase the
 * length of the encrypted key the current ISO-8601 date is appended to the
 * beginning of the encryption string followed by a `,`. Eg:
 *
 * `2016-02-07T02:07:56.271Z,12345`
 *
 * In this case `12345` would be decrypted as the user's id.
 *
 * @param id (number) the id number to encrypt.
 */
const encryptId = (id) => {
  const newId = `${new Date().toISOString()},${id.toString()}`;
  const cipher = crypto.createCipheriv(algorithm, password, iv);
  return cipher.update(newId, 'binary', 'base64') + cipher.final('base64');
};

/**
 * Decrypts an encrypted user id string. The algorithm used to parse out the
 * user's id can be reversed engineered from the `encryptId` function
 * documentation.
 *
 * @param id string the string
 * @return the decrypted userId or -2 if there was a problem decrypting the
 *   provided string.
 */
const decryptId = (encryptedId) => {
  const decipher = crypto.createDecipheriv(algorithm, password, iv);
  const decrypted = decipher.update(encryptedId, 'base64', 'binary')
    + decipher.final('binary');

  let userId = decrypted.split(',')[1];
  if (userId) {
    try {
      userId = parseInt(userId, 10);
      return userId;
    } catch (err) {
      // TODO noticeError logger.log('error', err);
    }
  }
  return -2;
};

export { encryptId, decryptId };
