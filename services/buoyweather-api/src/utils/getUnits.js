import {
  getUnitsByLocation,
  getLegacyUnits,
} from '../external/user';

const unitsMap = {
  e: {
    temperature: 'f',
    tide: 'ft',
    wave: {
      height: 'ft',
    },
    wind: 'kts',
    pressure: 'mb',
    visibility: 'miles',
    precipitation: {
      volume: 'in',
    },
  },
  m: {
    temperature: 'c',
    tide: 'm',
    wave: {
      height: 'm',
    },
    wind: 'kts',
    pressure: 'mb',
    visibility: 'km',
    precipitation: {
      volume: 'mm',
    },
  },
};

const stringMatch = (value, matchers) =>
  (typeof value === 'string' || value instanceof String) &&
  matchers.indexOf(value) > -1;

const getUnits = async (userId, geoCountryIso, units) => {
  if (units && stringMatch(units, ['m', 'e'])) {
    return unitsMap[units];
  }
  if (userId) {
    const legacyUnits = await getLegacyUnits(userId);
    if (legacyUnits) {
      return unitsMap[legacyUnits];
    }
  }

  try {
    // Get units based on location data passed from services proxy headers
    // Do not use wind from unitsByLocation, since it returns kph by default
    const unitsByLocation = await getUnitsByLocation(geoCountryIso);

    let defaultType = 'm';
    if (unitsByLocation.units.surfHeight === 'FT' || unitsByLocation.units.surfHeight === 'HI') {
      defaultType = 'e';
    }
    return {
      ...unitsMap[defaultType],
      temperature: unitsByLocation.units.temperature.toLowerCase(),
      tide: unitsByLocation.units.tideHeight.toLowerCase(),
      wave: {
        height: unitsByLocation.units.surfHeight.toLowerCase(),
      },
    };
  } catch (err) {
    return unitsMap.m;
  }
};

export default getUnits;
