export const isJsonResponse = response =>
  response
    && response.headers
    && response.headers.get('content-type').indexOf('json') > -1;

export const nonJsonErrorMsg = (response) => {
  let errorMessage = { message: response.statusText };
  if (response.status === 200) {
    errorMessage = { message: 'Invalid JSON' };
  }
  return JSON.stringify(errorMessage);
};
