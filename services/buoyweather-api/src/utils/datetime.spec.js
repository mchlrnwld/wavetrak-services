import { expect } from 'chai';
import {
  fromUnixTimestamp,
  toUnixTimestamp,
  midnightOfDate,
  solarData,
} from './datetime';

describe('utils / datetime', () => {
  describe('fromUnixTimestamp', () => {
    it('converts seconds from 01/01/1970 to UTC Date object', () => {
      expect(fromUnixTimestamp(1493771258)).to.deep.equal(new Date('2017-05-03T00:27:38Z'));
    });
  });

  describe('toUnixTimestamp', () => {
    it('converts UTC Date object to seconds from 01/01/1970', () => {
      expect(toUnixTimestamp(new Date('2017-05-03T00:27:38Z'))).to.equal(1493771258);
    });

    it('converts local Date object to seconds from 01/01/1970', () => {
      expect(toUnixTimestamp(new Date('2017-05-02T17:27:38-07:00'))).to.equal(1493771258);
    });
  });

  describe('midnightOfDate', () => {
    it('returns midnight of date local to UTC offset', () => {
      expect(midnightOfDate(-8, new Date('2017-05-03T00:27:38')))
        .to.deep.equal(new Date('2017-05-02T08:00:00Z'));
      expect(midnightOfDate(-7, new Date('2017-05-03T14:27:38')))
        .to.deep.equal(new Date('2017-05-03T07:00:00Z'));
      expect(midnightOfDate(-7, new Date('2017-05-02T18:00:00')))
        .to.deep.equal(new Date('2017-05-02T07:00:00Z'));
    });
  });

  describe('solarData', () => {
    it('returns sunrise and sunset times for a date local to a lat/lon', () => {
      expect(solarData(new Date('2017-05-03T00:27:38'), 33.654213041213, -118.0032588192))
        .to.deep.equal({
          dawn: parseInt(new Date('2017-05-02T12:36:39.019Z').getTime() / 1000, 10),
          sunrise: parseInt(new Date('2017-05-02T13:03:17.675Z').getTime() / 1000, 10),
          sunset: parseInt(new Date('2017-05-03T02:37:03.326Z').getTime() / 1000, 10),
          dusk: parseInt(new Date('2017-05-03T03:03:41.982Z').getTime() / 1000, 10),
        });
    });
  });
});
