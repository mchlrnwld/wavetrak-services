import moment from 'moment-timezone';

const utcOffsetFromTimezone = timezone => moment().tz(timezone).utcOffset() / 60;

export default utcOffsetFromTimezone;
