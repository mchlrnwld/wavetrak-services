import { round } from 'lodash';
import moment from 'moment-timezone';
import {
  timezoneData,
  getTimezoneRefByOffset,
} from './datetime';

const windSpeedType = {
  kts: 'knots',
  mph: 'miles per hour',
  kph: 'kilometres per hour',
};

const flagColorText = (forecast, units) => {
  const { wind, wave } = forecast;

  if (!wind) {
    return null;
  }

  let windSpeed = wind.speed.average;
  // Convert to mph from kph or kts
  if (units.wind !== 'mph') {
    windSpeed *= units.wind === 'kph' ? 0.621371 : 1.15078;
    windSpeed = round(windSpeed);
  }

  let warningColor = 'green';

  if (!wave) {
    if (windSpeed > 25) {
      warningColor = 'red';
    } else if (windSpeed > 16) {
      warningColor = 'yellow';
    }
  } else {
    const { period } = wave;
    let waveHeight = wave.height.mean;
    if (units.wave.height !== 'ft') {
      waveHeight *= 3.281;
      waveHeight = round(waveHeight);
    }

    if (windSpeed <= 16) {
      if (waveHeight > 6 && period <= 10) {
        warningColor = 'yellow';
      }
      if (waveHeight > 10 && period < 12) {
        warningColor = 'red';
      }
    }
    if (windSpeed > 16) {
      warningColor = 'yellow';
      if ((waveHeight > 10 && period < 12) || windSpeed > 25) {
        warningColor = 'red';
      }
    }
  }
  return warningColor;
};

const seasText = (forecast, unit) => {
  if (!forecast.wave) {
    return null;
  }
  const cardinal = forecast.wave.direction.cardinal;
  const period = forecast.wave.period;
  const waveHeight = unit === 'm' ? forecast.wave.height.mean : round(forecast.wave.height.mean);
  let unitText = 'feet';
  if (waveHeight === 1) {
    unitText = unit === 'm' ? 'meter' : 'foot';
  } else if (waveHeight !== 1 && unit === 'm') {
    unitText = 'meters';
  }
  return `${cardinal} ${waveHeight} ${unitText} at ${period} seconds.`;
};

const windText = (forecast, unit) => {
  if (!forecast.wind) {
    return null;
  }
  const cardinal = forecast.wind.direction.cardinal;
  const windMin = round(forecast.wind.speed.average);
  const windMax = round(forecast.wind.speed.peak);
  const unitText = windSpeedType[unit];

  return windMin === windMax
    ? `${cardinal} ${windMin} ${unitText}.`
    : `${cardinal} ${windMin} to ${windMax} ${unitText}.`;
};

const narrativeText = (forecast, units) => {
  let windForecast = '';
  if (forecast.wind && 'speed' in forecast.wind) {
    let windSpeed = forecast.wind.speed.average;
    // Convert to mph from kph or kts
    if (units.wind !== 'mph') {
      windSpeed *= units.wind === 'kph' ? 0.621371 : 1.15078;
    }
    const windCardinal = forecast.wind.direction.cardinal;
    windForecast = 'Light and variable winds with smooth seas.';
    if (windSpeed > 6 && windSpeed < 11) {
      windForecast = `Light ${windCardinal} winds with a slight chop.`;
    } else if (windSpeed >= 11 && windSpeed < 17) {
      windForecast = `Breezy ${windCardinal} winds with moderate choppy seas.`;
    } else if (windSpeed >= 17 && windSpeed < 22) {
      windForecast = `Moderate to strong ${windCardinal} winds with choppy seas.`;
    } else if (windSpeed >= 22 && windSpeed < 35) {
      windForecast = `Strong ${windCardinal} winds with very choppy seas. Small Craft Advisory.`;
    } else if (windSpeed >= 35 && windSpeed < 48) {
      windForecast = `Very strong ${windCardinal} winds with dangerous seas. Gale Warning.`;
    } else if (windSpeed >= 48 && windSpeed < 64) {
      windForecast = `Tropical storm force ${windCardinal} winds with dangerous seas. Storm Warning.`;
    } else if (windSpeed >= 64) {
      windForecast = `Hurricane force ${windCardinal} winds with dangerous seas. Hurricane Force Wind Warning.`;
    }
  }
  let swellType = '';
  let heightText = '';
  if (forecast.wave && 'height' in forecast.wave) {
    const period = forecast.wave.period;
    let waveHeight = forecast.wave.height.mean;
    if (units.wave.height !== 'ft') {
      waveHeight *= 3.281;
      waveHeight = round(waveHeight);
    }
    if (period > 0 && (waveHeight / period) >= 0.85 && waveHeight >= 10) {
      windForecast += ' Hazardous Seas Warning.';
    }

    swellType = 'very short period waves.';
    if (period >= 7 && period < 10) {
      swellType = 'short period waves.';
    } else if (period >= 10 && period < 15) {
      swellType = 'mid period waves.';
    } else if (period >= 15 && period < 20) {
      swellType = 'long period waves.';
    } else if (period >= 20) {
      swellType = 'very long period waves.';
    }

    heightText = 'Very small';
    if (waveHeight >= 3 && waveHeight < 5) {
      heightText = 'Small';
    } else if (waveHeight >= 5 && waveHeight < 10) {
      heightText = 'Moderate';
    } else if (waveHeight >= 10 && waveHeight < 15) {
      heightText = 'Large';
    } else if (waveHeight >= 15) {
      heightText = 'Very large';
    }
  }

  return `${windForecast} ${heightText} ${swellType}`;
};

const writtenFromTimeOfDay = (forecastData, units) => ({
  winds: (() => {
    try {
      const winds = windText(forecastData, units.wind);
      return winds;
    } catch (error) {
      return null;
    }
  })(),
  seas: (() => {
    try {
      const seas = seasText(forecastData, units.wave.height);
      return seas;
    } catch (error) {
      return null;
    }
  })(),
  narrative: (() => {
    try {
      const narrative = narrativeText(forecastData, units);
      return narrative;
    } catch (error) {
      return null;
    }
  })(),
  flagColor: (() => {
    try {
      const flagColor = flagColorText(forecastData, units);
      return flagColor;
    } catch (error) {
      return null;
    }
  })(),
});

const writtenForecast = (
  forecasts,
  units,
  lat,
  lon,
  timezoneArray,
  amHour = 9,
  pmHour = 15,
) => {
  let writtenData = {};
  const timestampData = {};
  const written = [];
  const datesTracked = [];
  forecasts.forEach((forecast) => {
    const { timestamp } = forecast;
    const { key, offset } = timezoneData(lat, lon, timestamp);
    const date = moment.tz(moment.unix(timestamp), key);
    const amOrPm = date.format('A');
    const dateEnum = date.format('MMDDYYYY');
    if (!timestampData[dateEnum]) {
      timestampData[dateEnum] = round(date.format('X'));
    }
    const hourOfDay = Number(date.format('H'));
    if ((hourOfDay >= amHour && amOrPm === 'AM') || (hourOfDay >= pmHour && amOrPm === 'PM')) {
      if (!writtenData[dateEnum]) {
        writtenData = {
          ...writtenData,
          [dateEnum]: {},
        };
      }
      if (!writtenData[dateEnum][amOrPm]) {
        writtenData[dateEnum][amOrPm] = { ...forecast };
      }
    }
    if (datesTracked.indexOf(dateEnum) === -1
      && writtenData[dateEnum]
      && writtenData[dateEnum].AM
      && writtenData[dateEnum].PM
    ) {
      datesTracked.push(dateEnum);
      const forecastData = writtenData[dateEnum];
      written.push({
        timestamp: timestampData[dateEnum],
        timezone: getTimezoneRefByOffset(timezoneArray, offset),
        AM: writtenFromTimeOfDay(forecastData.AM, units),
        PM: writtenFromTimeOfDay(forecastData.PM, units),
      });
    }
  });
  return written;
};

export {
  flagColorText,
  seasText,
  windText,
  narrativeText,
  writtenForecast,
};
