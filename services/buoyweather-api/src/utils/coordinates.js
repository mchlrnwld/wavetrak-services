const coordinates = {
  cardinal: (coords) => {
    const cardinalCoordinates = {};
    const deg = String.fromCharCode(176);
    const trimLatLng = this.trimToNearest(coords, 2);
    const lat = Math.abs(trimLatLng[0]);
    const lon = Math.abs(trimLatLng[1]);
    cardinalCoordinates.lat = (coords[0] >= 0) ? `${lat}${deg}N` : `${lat}${deg}S`;
    cardinalCoordinates.lon = (coords[1] >= 0) ? `${lon}${deg}E` : `${lon}${deg}W`;
    return cardinalCoordinates;
  },
  match: (coordinatesOne, coordinatesTwo) => {
    const marginError = 0.00011;
    const lng =
      Math.abs(
        Number(coordinatesOne[0]).toFixed(4) - Number(coordinatesTwo[0]).toFixed(4),
      ) < marginError;
    const lat =
      Math.abs(
        Number(coordinatesOne[1]).toFixed(4) - Number(coordinatesTwo[1]).toFixed(4),
      ) < marginError;
    return lng && lat;
  },
  trimToNearest: (coords, decimalPlace) => {
    const coordsReturn = coords.map((coord) => {
      let coordAdj = coord;
      if (coord % 1 !== 0 && !isNaN(coord)) {
        const point = coord.toString().split('.');
        coordAdj = Number(`${point[0]}.${point[1].substring(0, decimalPlace)}`);
      }
      return coordAdj;
    });
    return coordsReturn;
  },
};

export default coordinates;
