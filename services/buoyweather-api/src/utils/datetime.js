import moment from 'moment-timezone';
import suncalc from 'suncalc';
import tzlookup from 'tz-lookup';
import utcOffsetFromTimezone from '../utils/utcOffsetFromTimezone';
import round from '../utils/round';


const localOffset = (new Date()).getTimezoneOffset() / 60;

const getLocalDate = (timestamp, utcOffset) =>
  (timestamp * 1000) + ((utcOffset + localOffset) * 3600 * 1000);

const secondsPerDay = 60 * 60 * 24;
const secondsPerHour = 60 * 60;

const fromUnixTimestamp = timestamp => new Date(timestamp * 1000);

const toUnixTimestamp = date => Math.floor(date / 1000);

const midnightOfDate = (utcOffset, date) => {
  const localDate = toUnixTimestamp(date) + (utcOffset * secondsPerHour);
  const localMidnight = localDate - (localDate % secondsPerDay);
  const midnight = localMidnight - (utcOffset * secondsPerHour);
  return fromUnixTimestamp(midnight);
};

const addHoursToDate = (hours, date) => {
  const localDate = toUnixTimestamp(date) + (hours * secondsPerHour);
  return fromUnixTimestamp(localDate);
};

const legacyDate = (timestamp, offset, fmt) => {
  const dateFormat = fmt || 'dddd, M/D';
  return moment(timestamp * 1000).utcOffset(offset).format(dateFormat);
};

const timezoneData = (lat, lon, date, id, path) => {
  const key = tzlookup(lat, lon);
  const momentDate = date || new Date();
  const momentData = moment.tz(momentDate, key);
  const abbr = momentData.zoneAbbr();
  const offset = utcOffsetFromTimezone(key);
  return {
    key,
    abbr,
    offset,
    id,
    path,
  };
};

const getTimezoneArray = (lat, lon, start, end) => {
  const startEnd = [start, end];
  let lastOffset;
  const timeZoneArray = startEnd.map((date, idx) => {
    const path = 'associated.timezone';
    const tzDataThis = timezoneData(lat, lon, date, idx + 1, path);
    const tzData = lastOffset !== tzDataThis.offset ? tzDataThis : null;
    lastOffset = tzDataThis.offset;
    return tzData;
  })
    .filter(tzData => !!tzData);
  return timeZoneArray;
};

const getTimezoneRefByOffset = (timezoneArray, offset) => {
  const timezoneRef = timezoneArray.filter(tzData => tzData.offset === offset)
    .map((tzData) => {
      const tzRef = {
        id: tzData.id,
        path: 'associated.timezone',
      };
      return tzRef;
    });
  return timezoneRef.length ? timezoneRef[0] : {};
};

const offsetIsoMinutes = (offset) => {
  const offsetInt = parseInt(offset, 10);
  const decPart = offset % offsetInt;
  const defaultMins = '00';
  if (decPart > 0) {
    const isoMinsNum = 60 * decPart;
    const isoMins = isoMinsNum > 10 ?
      parseInt(isoMinsNum, 10).toString()
      : defaultMins;
    return isoMins;
  }
  return defaultMins;
};

const formatOffsetStringISO = (offset) => {
  const plusMinus = offset < 0 ? '-' : '+';
  const numVal = Math.abs(offset);
  const isoHours = parseInt(numVal, 10);
  const isoMins = offsetIsoMinutes(numVal);
  const offsetStr = numVal < 10 ? `${plusMinus}0${isoHours}:${isoMins}` : `${plusMinus}${isoHours}:${isoMins}`;
  return offsetStr;
};

const moonPhase = (phase) => {
  if ((phase >= 0 && phase <= 0.025) || phase >= 0.975) {
    return 'new moon';
  } else if (phase > 0.025 && phase <= 0.225) {
    return 'waxing crescent';
  } else if (phase > 0.225 && phase <= 0.275) {
    return 'first quarter';
  } else if (phase > 0.275 && phase <= 0.475) {
    return 'waxing gibbous';
  } else if (phase > 0.475 && phase <= 0.525) {
    return 'full moon';
  } else if (phase > 0.525 && phase <= 0.725) {
    return 'waning gibbous';
  } else if (phase > 0.725 && phase <= 0.775) {
    return 'last quarter';
  }

  return 'waning crescent';
};

const solarData = (date, lat, lon) => {
  const { dawn, sunrise, sunset, dusk } = suncalc.getTimes(date, lat, lon);
  return {
    dawn: moment(dawn).unix(),
    sunrise: moment(sunrise).unix(),
    sunset: moment(sunset).unix(),
    dusk: moment(dusk).unix(),
  };
};

const lunarData = (date, lat, lon) => {
  const { rise, set } = suncalc.getMoonTimes(date, lat, lon);
  const { phase, fraction } = suncalc.getMoonIllumination(date);

  return {
    rise: moment(rise).unix(),
    set: moment(set).unix(),
    phase: moonPhase(phase),
    illumination: round(fraction * 100, 2),
  };
};

const locationNighttime = (lat, lon) => {
  const currentDate = new Date();
  const times = suncalc.getTimes(currentDate, lat, lon);
  const currentTime = toUnixTimestamp(currentDate);
  const nauticalDawn = toUnixTimestamp(new Date(times.nauticalDawn));
  const nauticalDusk = toUnixTimestamp(new Date(times.nauticalDusk));
  return currentTime > nauticalDusk || currentTime < nauticalDawn;
};

/**
 * Returns midnight of the timezone in represented as UTC.
 *
 * @param {String} timezone the timezone to return of
 * @returns Moment a moment object
 */
const midnightOfTimezoneDatestamp = (timezone) => {
  const midnight = moment().tz(timezone).startOf('day').toDate();
  return moment(Date.UTC(
    midnight.getFullYear(),
    midnight.getMonth(),
    midnight.getDate(),
    0,
    0,
    0,
    0,
  ));
};

export {
  secondsPerDay,
  secondsPerHour,
  fromUnixTimestamp,
  toUnixTimestamp,
  midnightOfDate,
  midnightOfTimezoneDatestamp,
  timezoneData,
  getTimezoneArray,
  getTimezoneRefByOffset,
  solarData,
  lunarData,
  locationNighttime,
  addHoursToDate,
  formatOffsetStringISO,
  legacyDate,
  getLocalDate,
  localOffset,
};
