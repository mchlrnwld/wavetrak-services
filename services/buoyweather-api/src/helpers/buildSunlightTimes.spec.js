import { expect } from 'chai';
import { secondsPerDay, secondsPerHour } from '../utils/datetime';
import buildSunlightTimes from './buildSunlightTimes';

const buildSunlightTimesSpec = () => {
  describe('buildSunlightTimes', () => {
    const utcOffset = -7;
    const lat = 33.654213041213;
    const lon = -118.0032588192;

    it('can build sunrise and sunset date object for a single day', () => {
      const singleDayStart = 1493708400; // 05/02/17@00:00-PDT
      const singleDayEnd = 1493791200; // 05/02/17@11:00-PDT
      const singleDay = buildSunlightTimes({
        start: singleDayStart,
        end: singleDayEnd,
        utcOffset,
        lat,
        lon,
      });
      expect(singleDay).to.have.length(1);
      expect(singleDay[0].midnight).to.be.a.ok();
      expect(singleDay[0].dawn).to.be.a.ok();
      expect(singleDay[0].sunrise).to.be.a.ok();
      expect(singleDay[0].sunset).to.be.a.ok();
      expect(singleDay[0].dusk).to.be.a.ok();
      expect(singleDay[0].midnight % secondsPerDay).to.equal(-utcOffset * secondsPerHour);
      expect(singleDay[0].midnight).to.be.at.most(singleDayStart);
      expect(singleDay[0].midnight).to.be.at.most(singleDayEnd);
      expect(singleDay[0].midnight).to.be.closeTo(singleDayStart, secondsPerDay);
      expect(singleDay[0].midnight).to.be.closeTo(singleDayEnd, secondsPerDay);
    });

    it('can build sunrise and sunset date object for two days even when start and end are only an hour apart', () => {
      const twoDayStart = 1493791200; // 05/02/17@11:00-PDT
      const twoDayEnd = 1493794800; // 05/03/17@00:00-PDT
      const twoDay = buildSunlightTimes({
        start: twoDayStart,
        end: twoDayEnd,
        utcOffset,
        lat,
        lon,
      });
      expect(twoDay).to.have.length(2);
      twoDay.forEach((day) => {
        expect(day.midnight).to.be.a.ok();
        expect(day.dawn).to.be.a.ok();
        expect(day.sunrise).to.be.a.ok();
        expect(day.sunset).to.be.a.ok();
        expect(day.dusk).to.be.a.ok();
        expect(day.midnight % secondsPerDay).to.equal(-utcOffset * secondsPerHour);
      });
      expect(twoDay[0].midnight).to.be.at.most(twoDayStart);
      expect(twoDay[0].midnight).to.be.closeTo(twoDayStart, secondsPerDay);
      expect(twoDay[1].midnight).to.be.at.most(twoDayEnd);
      expect(twoDay[1].midnight).to.be.closeTo(twoDayEnd, secondsPerDay);
    });

    it('can build sunrise and sunset date objects for larger time spans', () => {
      const seventeenDayStart = 1493708400; // 05/02/17@00:00-PDT
      const seventeenDayEnd = 1495126740; // 05/18/17@11:59-PDT
      const seventeenDay = buildSunlightTimes({
        start: seventeenDayStart,
        end: seventeenDayEnd,
        utcOffset,
        lat,
        lon,
      });
      expect(seventeenDay).to.have.length(17);
      seventeenDay.forEach((day) => {
        expect(day.midnight).to.be.ok();
        expect(day.dawn).to.be.ok();
        expect(day.sunrise).to.be.ok();
        expect(day.sunset).to.be.ok();
        expect(day.dusk).to.be.ok();
        expect(day.midnight % secondsPerDay).to.equal(-utcOffset * secondsPerHour);
      });
      expect(seventeenDay[0].midnight).to.be.at.most(seventeenDayStart);
      expect(seventeenDay[0].midnight).to.be.closeTo(seventeenDayStart, secondsPerDay);
      expect(seventeenDay[16].midnight).to.be.at.most(seventeenDayEnd);
      expect(seventeenDay[16].midnight).to.be.closeTo(seventeenDayEnd, secondsPerDay);
    });
  });
};

export default buildSunlightTimesSpec;
