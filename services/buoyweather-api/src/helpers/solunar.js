import moment from 'moment';
import {
  solarData,
  lunarData,
  timezoneData,
  midnightOfDate,
  getTimezoneRefByOffset,
} from '../utils/datetime';

const getSolunar = async ({ lat, lon, days, timezoneArray }) => {
  const suncalcData = [];
  const { offset: utcOffset } = timezoneData(lat, lon, new Date());
  const utcDate = new Date();
  utcDate.getUTCDate();
  const currDate = midnightOfDate(utcOffset, utcDate);

  for (let i = 0; i < days; i += 1) {
    const { offset } = timezoneData(lat, lon, currDate);
    const timezone = getTimezoneRefByOffset(timezoneArray, offset);
    const datestamp = moment(currDate).unix();

    const solar = solarData(currDate, lat, lon);
    const lunar = lunarData(currDate, lat, lon);

    const solunarData = {
      datestamp,
      timezone,
      solar,
      lunar,
    };
    suncalcData.push(solunarData);
    currDate.setDate(currDate.getDate() + 1);
  }

  return suncalcData;
};

export default getSolunar;
