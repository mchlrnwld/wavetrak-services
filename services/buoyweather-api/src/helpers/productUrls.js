import config from '../config';

export const sanitizeFavoriteName = name => name.replace(/[^a-z0-9]+/gi, '+');

export const productsUrls = {
  marine_weather: (favorite) => {
    if ('name' in favorite) {
      return `/forecast/marine-weather/favorites/${sanitizeFavoriteName(favorite.name)}/@${favorite.point[1]},${favorite.point[0]}`;
    }
    return `/forecast/marine-weather/@${favorite.point[1]},${favorite.point[0]}`;
  },
  pointOfInterest: poi => `http://${config.WWW_HOST}/forecast/marine-weather/spot/${poi.title.replace(/[^a-z0-9]+/gi, '+')}/@${poi.location.coordinates[1]},${poi.location.coordinates[0]}`,

};

export const getProductUrl = (favorite) => {
  if (favorite.product in productsUrls) {
    return productsUrls[favorite.product](favorite);
  }
  return favorite.legacyUrl || '#';
};
