import config from '../config';
import coordinateUtils from '../utils/coordinates';
import createParamString from '../external/createParamString';

const legacyEndpoint = {
  url(fileName, coordinates, xParams, nearbyRegion) {
    const trimLatLon = coordinateUtils.trimToNearest(coordinates, 1);
    const latitude = trimLatLon[0];
    const longitude = trimLatLon[1];
    const defaultParams = {
      program: 'nww3BW1',
      grb: nearbyRegion.region_model_grb,
      region: nearbyRegion.region_short_code,
      latitude,
      longitude,
      reqLatitude: latitude,
      reqLongitude: longitude,
      zone: nearbyRegion.time_zone,
      units: nearbyRegion.units,
    };
    const params = Object.assign(defaultParams, xParams);
    const legacyUrl = `${config.BUOYWEATHER_API_V1}/${fileName}?${createParamString(params)}`;
    return legacyUrl;
  },
};

export default legacyEndpoint;
