import { getSwellForecast, getWeatherForecast } from '../external/scienceDataServiceForecast';

const getForecasts = async ({
  lat,
  lon,
  start,
  end,
  interval,
  units,
  waterways,
}) => {
  const [waveResponse] = !waterways ? await Promise.all([
    getSwellForecast({
      lat,
      lon,
      start,
      end,
      interval,
      units,
    }),
  ]) : [];

  const offshoreCoordinate = waveResponse ? {
    lat: waveResponse.associated.model.grid.lat,
    lon: waveResponse.associated.model.grid.lon,
  } : null;

  const waveForecastMap = waveResponse ?
    new Map(
      waveResponse.data.map(forecast => [forecast.timestamp, forecast]),
    ) : null;

  const [weatherResponse] = await Promise.all([
    getWeatherForecast({
      lat,
      lon,
      start,
      end,
      interval,
      units,
    }),
  ]);

  const weatherForecastMap = weatherResponse ?
    new Map(
      weatherResponse.data.map(forecast => [forecast.timestamp, forecast]),
    ) : null;

  return {
    offshoreCoordinate,
    waveForecastMap,
    weatherForecastMap,
  };
};

export default getForecasts;
