import { expect } from 'chai';
import sinon from 'sinon';
import allowableForecastRange from './allowableForecastRange';

const allowableForecastRangeSpec = () => {
  describe('allowableForecastRange', () => {
    let clock;

    const utcOffset = -8;
    const interval = 180;
    const premium = true;

    // Testing with a timestamp just past midnight to
    // see if we get midnight of the correct local day
    const current = 1486258500; // 02/05/2017@01:35
    const start = 1486195200; // 02/04/2017@08:00
    const end2Days = start + (2 * 24 * 60 * 60);
    const end16Days = start + (16 * 24 * 60 * 60);

    before(() => {
      clock = sinon.useFakeTimers(current * 1000);
    });

    after(() => {
      clock.restore();
    });

    it('should return a range of 2 days from the current hour at 3 hour steps for non-premium', async () => {
      const result = await allowableForecastRange(utcOffset, interval);
      expect(result).to.deep.equal({
        minStart: start,
        maxEnd: end2Days,
        totalTimeSteps: 16,
      });
    });

    it('should return a range of 16 days from now at 1 hour steps for premium', async () => {
      const result = await allowableForecastRange(utcOffset, interval, premium);
      expect(result).to.deep.equal({
        minStart: start,
        maxEnd: end16Days,
        totalTimeSteps: 128,
      });
    });

    it('should default to non-premium if premium not provided', async () => {
      const result = await allowableForecastRange(utcOffset, interval);
      expect(result).to.deep.equal({
        minStart: start,
        maxEnd: end2Days,
        totalTimeSteps: 16,
      });
    });

    it('should start from midnight of date local to the UTC offset', async () => {
      expect((await allowableForecastRange(-8, interval)).minStart).to.equal(start);
      expect((await allowableForecastRange(-5.5, interval)).minStart).to.equal(1486186200);
    });
  });
};

export default allowableForecastRangeSpec;
