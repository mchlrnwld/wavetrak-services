import { forOwn } from 'lodash';
import { legacyDate } from '../utils/datetime';
import config from '../config';

const iconBaseUrl = `${config.BW_ICON_CDN}/buoyweather/images/direction_icons/`;

export const forecastTransform = (forecast, legacyUserSettings) => {
  const associated = forecast.associated;
  const writtenForecast = 'written' in forecast.data && forecast.data.written ?
    forecast.data.written.map((written, index) => {
      const offset = associated.timezone.filter(zone =>
        zone.id === written.timezone.id)[0].offset;
      const title = legacyDate(written.timestamp, offset);
      return {
        id: index + 1,
        title,
        AM: written.AM || null,
        PM: written.PM || null,
      };
    }) : null;
  const units = {
    global: associated.units.wave.height === 'm' ? 'm' : 'e',
    wave: associated.units.wave.height,
    wind: associated.units.wind,
  };
  const forecastType = legacyUserSettings.type;
  const addTimeStep = (timeStep, localTimestamp, offset) => ({
    ...timeStep,
    timestamp: undefined,
    timezone: undefined,
    date: {
      local: legacyDate(localTimestamp, offset, 'MMMM DD, YYYY HH:00:00'),
      utc: legacyDate(localTimestamp, 0, 'MMMM DD, YYYY HH:00:00'),
    },
    wave: (() => (
      timeStep.wave ? {
        ...timeStep.wave,
        height: {
          peak: timeStep.wave.height.peak,
          average: timeStep.wave.height.mean,
        },
        direction: {
          ...timeStep.wave.direction,
          iconPath: `${iconBaseUrl}${timeStep.wave.direction.cardinal}.gif`,
        },
      } : null
    ))(),
    wind: (() => (
      timeStep.wind ? {
        speed: {
          ...timeStep.wind.speed,
          current: timeStep.wind.speed.average,
        },
        direction: {
          ...timeStep.wind.direction,
          iconPath: `${iconBaseUrl}${timeStep.wind.direction.cardinal}.gif`,
        },
      } : null
    ))(),
  });
  let maxWave = 0;
  let maxWind = 0;
  const days = [];
  let timeSteps = [];
  forecast.data.forecasts.forEach((timeStep, index) => {
    if (timeStep.weather) {
      // eslint-disable-next-line no-param-reassign
      delete timeStep.weather;
    }
    const windPeak = timeStep.wind ? timeStep.wind.speed.peak : 0;
    maxWind = windPeak > maxWind ? windPeak : maxWind;
    const wavePeak = timeStep.wave ? timeStep.wave.height.peak : 0;
    maxWave = wavePeak > maxWave ? wavePeak : maxWave;
    const offset = associated.timezone.filter(zone =>
      zone.id === timeStep.timezone.id)[0].offset;
    const date = legacyDate(timeStep.timestamp, offset, 'ddd M/D');
    const nextDate = forecast.data.forecasts.length > index + 1 ?
      legacyDate(forecast.data.forecasts[index + 1].timestamp, offset, 'ddd M/D')
      : null;
    if (date === nextDate) {
      timeSteps.push(addTimeStep(timeStep, timeStep.timestamp, offset));
    } else {
      timeSteps.push(addTimeStep(timeStep, timeStep.timestamp, offset));
      days.push({
        date,
        timeSteps,
      });
      timeSteps = [];
    }
  });
  const urls = {};
  forOwn(associated.urls, (value, key) => {
    urls[`${key}Url`] = value;
  });
  // API currently returns last day with one timestep. We only want full days.
  if (days[days.length - 1].timeSteps.length === 1) days.splice(-1, 1);
  const nearbyRegion = associated.region;
  const response = {
    writtenForecast,
    charts: {
      days,
      units,
      maxWave,
      maxWind,
    },
    ...urls,
    nearbyRegion,
    forecastType,
    badData: maxWave === 0 && maxWind === 0,
  };
  return response;
};

export default forecastTransform;
