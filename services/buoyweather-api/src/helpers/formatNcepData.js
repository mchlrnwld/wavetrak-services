import { toUnixTimestamp } from '../utils/datetime';
import round from '../utils/round';

export const getWaveData = (apiData) => {
  const {
    lat,
    lon,
    dateString,
    period,
    waveDirectionDegrees,
    waveDirection,
    heightPeak,
    heightAvg,
  } = apiData;

  const waveData = [...Array(dateString.length)].map((_, i) => {
    const timestampJS = new Date(`${dateString[i]} GMT+00:00`);
    const timestamp = toUnixTimestamp(timestampJS);
    const wavePeriod = period[i];
    const height = {
      mean: heightAvg[i],
      peak: heightPeak[i],
    };
    const direction = {
      azimuth: waveDirectionDegrees[i],
      cardinal: waveDirection[i],
    };

    return {
      timestamp,
      wave: {
        height,
        direction,
        period: round(wavePeriod, 0),
        offshoreCoordinate: {
          lat,
          lon,
        },
      },
    };
  });
  const waveForecastMap = waveData ?
    new Map(
      waveData.map(forecast => [forecast.timestamp, forecast]),
    ) : null;

  return waveForecastMap;
};

export const getWindData = (apiData) => {
  const {
    dateString,
    windSpeedAvg,
    windSpeedPeak,
    windDir,
  } = apiData;

  const windData = [...Array(dateString.length)].map((_, i) => {
    const timestampJS = new Date(`${dateString[i]} GMT+00:00`);
    const timestamp = toUnixTimestamp(timestampJS);
    const speed = windSpeedAvg[i];
    const direction = windDir[i];
    const gust = windSpeedPeak[i];

    return {
      timestamp,
      wind: {
        speed,
        gust,
        direction,
      },
    };
  });

  const windForecastMap = windData ?
    new Map(
      windData.map(forecast => [forecast.timestamp, forecast]),
    ) : null;

  return windForecastMap;
};
