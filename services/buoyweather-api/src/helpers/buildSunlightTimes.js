import {
  secondsPerDay,
  secondsPerHour,
  fromUnixTimestamp,
  toUnixTimestamp,
  midnightOfDate,
  solarData,
  timezoneData,
} from '../utils/datetime';

const twelveHours = 12 * secondsPerHour;

const buildSunlightTimes = ({ start, end, lat, lon }) => {
  const utcOffsetStart = timezoneData(lat, lon, start).offset;
  const utcOffsetEnd = timezoneData(lat, lon, end).offset;
  const startMidnight = toUnixTimestamp(midnightOfDate(utcOffsetStart, fromUnixTimestamp(start)));
  const endMidnight = toUnixTimestamp(midnightOfDate(utcOffsetEnd, fromUnixTimestamp(end)));
  const numberOfDays = Math.ceil((endMidnight - startMidnight) / secondsPerDay) + 1;
  const sunriseSunsetTimes = [...Array(numberOfDays)].map((_, i) => {
    const midnight = startMidnight + (i * secondsPerDay);

    // Use noon instead of midnight to ensure correct date
    const { dawn, sunrise, sunset, dusk } = solarData(
      fromUnixTimestamp(midnight + twelveHours),
      lat,
      lon,
    );
    return {
      midnight,
      dawn,
      sunrise,
      sunset,
      dusk,
    };
  });
  return sunriseSunsetTimes;
};

export default buildSunlightTimes;
