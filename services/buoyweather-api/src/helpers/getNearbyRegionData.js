import { getNearbyRegion } from '../server/cms/CmsContent';

export default async (lat, lon) => {
  const region = await getNearbyRegion(lon, lat) || null;
  if (region) {
    // Temp fix for IOS
    region.custom_fields = {};
    region.custom_fields.latitude = (isNaN(parseFloat(region.location.coordinates[1]))) ? '0.0' : `${region.location.coordinates[1]}`;
    region.custom_fields.longitude = (isNaN(parseFloat(region.location.coordinates[0]))) ? '0.0' : `${region.location.coordinates[0]}`;
    region.custom_fields.zoom = (isNaN(parseInt(region.zoom, 10))) ? '0' : `${region.zoom}`;
  }
  return region;
};
