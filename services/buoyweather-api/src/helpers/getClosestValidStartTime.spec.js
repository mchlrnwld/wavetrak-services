import { expect } from 'chai';
import getClosestValidStartTimestamp from './getClosestValidStartTimestamp';

const getClosestValidStartTimestampSpec = () => {
  describe('getClosestValidStartTimestamp', () => {
    const minStart = 1486177200; // 02/04/2017@03:00
    const interval = 3 * 60 * 60;

    it('should return minStart if requestedStart is not ok', () => {
      expect(getClosestValidStartTimestamp(minStart, interval)).to.equal(minStart);
    });

    it('should return minStart if requestedStart is smaller', () => {
      expect(getClosestValidStartTimestamp(minStart, interval, 0)).to.equal(minStart);
    });

    it('should return requestedStart if requestedStart is a valid interval step from minStart', () => {
      expect(getClosestValidStartTimestamp(minStart, interval, minStart + interval))
        .to.equal(minStart + interval);
    });

    it('should return closest previous interval step from minStart if requestedStart is not valid', () => {
      expect(getClosestValidStartTimestamp(minStart, interval, (minStart + (2 * interval)) + 1))
        .to.equal(minStart + (2 * interval));
      expect(getClosestValidStartTimestamp(minStart, interval, (minStart + (3 * interval)) - 1))
        .to.equal(minStart + (2 * interval));
    });
  });
};

export default getClosestValidStartTimestampSpec;
