const getClosestValidStartTimestamp = (minStart, intervalSeconds, requestedStart) => {
  if (!requestedStart || requestedStart < minStart) return minStart;
  return minStart + (intervalSeconds * Math.floor((requestedStart - minStart) / intervalSeconds));
};

export default getClosestValidStartTimestamp;
