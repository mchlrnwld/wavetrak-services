import { secondsPerDay, toUnixTimestamp, midnightOfDate } from '../utils/datetime';

const allowableForecastRange = async (utcOffset, interval, premium, legacy) => {
  const minStart = toUnixTimestamp(midnightOfDate(utcOffset, new Date()));

  // Seconds
  const baseDays = premium ? 16 : 2;
  const legacyDays = premium ? 7 : 2;
  const forecastDays = legacy ? legacyDays : baseDays;
  const stepsPerDay = 1440 / interval;
  const totalTimeSteps = stepsPerDay * forecastDays;

  const maxTimespan = (forecastDays * secondsPerDay);
  const maxEnd = minStart + maxTimespan;

  return { minStart, maxEnd, totalTimeSteps };
};

export default allowableForecastRange;
