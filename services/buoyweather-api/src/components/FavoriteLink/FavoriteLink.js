import React from 'react'; // eslint-disable-line no-unused-vars
import PropTypes from 'prop-types';
import { getProductUrl } from '../../helpers/productUrls';

const FavoriteLink = ({ favorite }) => <a href={getProductUrl(favorite)}>{favorite.name}</a>;

FavoriteLink.propTypes = {
  favorite: PropTypes.shape({}).isRequired,
};

export default FavoriteLink;
