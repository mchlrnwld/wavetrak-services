import React from 'react'; // eslint-disable-line no-unused-vars
import PropTypes from 'prop-types';
import FavoriteLink from '../FavoriteLink'; // eslint-disable-line no-unused-vars

const FavoritesList = ({ favorites }) => (
  <ul>
    {favorites.map(favorite => <li><FavoriteLink favorite={favorite} /></li>)}
  </ul>
);

FavoritesList.propTypes = {
  favorites: PropTypes.shape({}).isRequired,
};

export default FavoritesList;
