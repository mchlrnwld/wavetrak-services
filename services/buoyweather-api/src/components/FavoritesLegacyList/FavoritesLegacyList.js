import React from 'react'; // eslint-disable-line no-unused-vars
import PropTypes from 'prop-types';
import FavoriteLink from '../FavoriteLink'; // eslint-disable-line no-unused-vars

const FavoritesList = ({ favorites }) => (
  <div>
    {favorites.map(favorite => <FavoriteLink favorite={favorite} />)}
  </div>
);

FavoritesList.propTypes = {
  favorites: PropTypes.shape({}).isRequired,
};

export default FavoritesList;
