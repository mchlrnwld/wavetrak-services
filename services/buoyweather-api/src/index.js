import 'newrelic';
import app from './server';
import logger from './common/logger';
import * as dbContext from './model/dbContext';

const log = logger('buoyweather-api:server');

Promise.all([dbContext.initMongoDB()])
  .then(app)
  .catch((err) => {
    log.error({
      message: "App Initialization failed. Can't connect to database",
      stack: err,
    });
  });
