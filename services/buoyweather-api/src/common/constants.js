import productCDN from '@surfline/quiver-assets';

const IMPERIAL = 'e';
const METRIC = 'm';
const WEATHER_ICON_PATH = `${productCDN}/weathericons`;

export { IMPERIAL, METRIC, WEATHER_ICON_PATH };
