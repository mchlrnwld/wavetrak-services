import { setupExpress } from '@surfline/services-common';
import logger from '../common/logger';
import tides from './tides';
import solunar from './solunar';
import checkForecastParams from './middleware/checkForecastParams';
import entitlements from './middleware/entitlements';
import userUnits from './middleware/userUnits';
import userWindWaveModel from './middleware/userWindWaveModel';
import units from './units';
import forecasts from './forecasts';
import favorites from './favorites';
import cms from './cms';
import admin from './admin';
import timezone from './middleware/timezone';

const log = logger('buoyweather-api');

const setupApp = () =>
  setupExpress({
    log,
    urlEncoding: false,
    name: 'buoyweather-api',
    port: process.env.EXPRESS_PORT || 8080,
    handlers: [
      ['/tides', userUnits, entitlements, timezone, tides(log)],
      ['/solunar', entitlements, solunar(log)],
      ['/units', units(log)],
      [
        '/forecasts',
        userUnits,
        entitlements,
        userWindWaveModel,
        checkForecastParams,
        timezone,
        forecasts(),
      ],
      ['/favorites', favorites()],
      ['/cms', cms()],
      ['/admin', admin()],
    ],
  });

export default setupApp;
