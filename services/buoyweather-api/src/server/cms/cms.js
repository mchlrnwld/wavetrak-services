import { Router } from 'express';
import {
  getPageData,
  getMenuData,
  getRegionBySlug,
  getNearbyRegion,
  getNearbyPOI,
  getAllPOI,
  getAllPosts,
} from './CmsContent';

export default () => {
  const api = new Router();

  api.get('/', async (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.status(200).send({
      title: 'Buoyweather API',
      path: '/*',
    });
  });

  api.get('/pages/:page', async (req, res) => {
    const data = await getPageData(req.params.page);
    if (data) {
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send(data);
    } else {
      res.status(404).send('404 Not Found');
    }
  });

  api.get('/menus/:type', async (req, res) => {
    const data = await getMenuData(req.params.type);
    if (data) {
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send(data);
    } else {
      res.status(404).send('404 Not Found');
    }
  });

  api.get('/regions/:slug', async (req, res) => {
    if (req.params.slug === 'nearby-region') {
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send({
        slug_match: {
          name: '',
          slug: '',
          location: {
            type: 'Point',
            coordinates: [],
          },
          zoom: 6,
          legacy_mappings: {
            region_short_code: '',
            region_model_grb: '',
            time_zone: '',
            units: '',
          },
        },
      });
    }
    const regionData = await getRegionBySlug(req.params.slug);
    if (regionData) {
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send({
        slug_match: {
          name: regionData.title,
          slug: regionData.slug,
          location: regionData.location,
          zoom: regionData.zoom,
          legacy_mappings: {
            region_short_code: regionData.region_short_code,
            region_model_grb: regionData.region_model_grb,
            time_zone: regionData.time_zone,
            units: regionData.units,
          },
        },
      });
    } else {
      res.status(404).send('404 Not Found');
    }
  });

  api.get('/nearbyregion', async (req, res) => {
    const validCoordinates = [];
    if (Number(req.query.lat) <= 90 && Number(req.query.lat) >= -90) {
      validCoordinates.push(1);
    }
    if (Number(req.query.lon) <= 180 && Number(req.query.lon) >= -180) {
      validCoordinates.push(1);
    }

    if (validCoordinates.length === 2) {
      const data = await getNearbyRegion(req.query.lon, req.query.lat);
      if (data) {
        // Temp fix for IOS
        data.custom_fields = {};
        data.custom_fields.latitude = (isNaN(parseFloat(data.location.coordinates[1]))) ? '0.0' : `${data.location.coordinates[1]}`;
        data.custom_fields.longitude = (isNaN(parseFloat(data.location.coordinates[0]))) ? '0.0' : `${data.location.coordinates[0]}`;
        data.custom_fields.zoom = (isNaN(parseInt(data.zoom, 10))) ? '0' : `${data.zoom}`;
        res.setHeader('Content-Type', 'application/json');
        res.status(200).send(data);
      } else {
        res.status(404).send('404 Not Found');
      }
    } else {
      res.status(400).send('400 Bad Request');
    }
  });

  api.get('/poi', async (req, res) => {
    const validCoordinates = [];
    if (Number(req.query.lat) <= 90 && Number(req.query.lat) >= -90) {
      validCoordinates.push(1);
    }
    if (Number(req.query.lon) <= 180 && Number(req.query.lon) >= -180) {
      validCoordinates.push(1);
    }

    if (validCoordinates.length === 2) {
      const data = await getNearbyPOI(req.query.lon, req.query.lat);
      if (data) {
        res.setHeader('Content-Type', 'application/json');
        res.status(200).send({ pointOfInterest: data, forecastName: data.title });
      } else {
        res.status(404).send('404 Not Found');
      }
    } else {
      const allPOI = await getAllPOI();
      if (allPOI) {
        res.setHeader('Content-Type', 'application/json');
        res.status(200).send(allPOI);
      } else {
        res.status(404).send('404 Not Found');
      }
    }
  });

  api.get('/posts', async (req, res) => {
    const allPosts = await getAllPosts();
    if (allPosts) {
      res.setHeader('Content-Type', 'application/json');
      res.status(200).send(allPosts);
    } else {
      res.status(404).send('404 Not Found');
    }
  });

  return api;
};
