import Page from '../../model/cms/page';
import Menu from '../../model/cms/menu';
import Region from '../../model/cms/region';
import PointOfInterest from '../../model/cms/point_of_interest';
import Post from '../../model/cms/post';


export const getPageData = page => Page.findOne({ slug: page });

export const getMenuData = type => Menu.findOne({ id: type }).lean();

export const getNearbyRegion = (lon, lat) => Region.findOne({ location: { $near: { $geometry: { type: 'Point', coordinates: [lon, lat] } } } }).lean();

export const getRegionBySlug = slug => Region.findOne({ slug }).lean();

export const getNearbyPOI = (lon, lat) => PointOfInterest.findOne({ location: { $near: { $geometry: { type: 'Point', coordinates: [lon, lat] } } } }).lean();

export const getAllPOI = () => PointOfInterest.find({}).lean();

export const getAllPosts = () => Post.find({}).lean();
