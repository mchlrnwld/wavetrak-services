import { wrapErrors } from '@surfline/services-common';
import getUserSettings from '../../external/userSettings';

const userWaveModel = async (req, _, next) => {
  /* eslint-disable no-param-reassign */
  const { model } = req.query;
  const validModels = ['NCEP', 'LOLA', 'LOTUS'];
  let bwWindWaveModel = 'LOLA';

  if (model) {
    bwWindWaveModel = model;
  } else {
    const userSettings = await getUserSettings(req.authenticatedUserId);
    bwWindWaveModel = userSettings && userSettings.bwWindWaveModel ?
      userSettings.bwWindWaveModel : 'LOLA';
  }
  req.bwWindWaveModel = validModels.includes(bwWindWaveModel) ?
    bwWindWaveModel : 'LOLA';
  return next();
  /* eslint-enable no-param-reassign */
};

export default wrapErrors(userWaveModel);
