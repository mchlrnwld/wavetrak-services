import errors from '../../utils/errors';
import { decryptId } from '../../utils/userCrypto';

export default () => (req, res, next) => {
  const customAuth = req.headers['custom-authorization'];
  if (customAuth) {
    const userId = decryptId(customAuth);
    // eslint-disable-next-line
    req.userId = !isNaN(userId) ? userId : null;
  }
  if (!req.userId && !req.authenticatedUserId) {
    throw new errors.UnauthorizedError();
  }
  return next();
};
