import { timezoneData } from '../../utils/datetime';

const timezone = (req, res, next) => {
  /* eslint-disable no-param-reassign */
  req.timezone = timezoneData(req.query.lat, req.query.lon);
  return next();
  /* eslint-enable no-param-reassign */
};

export default timezone;
