import { wrapErrors } from '@surfline/services-common';
import getEntitlements from '../../external/entitlements';

const entitleRequest = async (req, _, next) => {
  /* eslint-disable no-param-reassign */
  req.user = {
    entitlements: [],
    premium: false,
    type: 'non-registered',
  };

  if (req.authenticatedUserId) {
    const userEntitlements = await getEntitlements(req.authenticatedUserId);
    const bwpremium = userEntitlements.entitlements.includes('bw_premium');
    const fspremiumapp = userEntitlements.entitlements.includes('fs_premium');
    const premium = bwpremium || fspremiumapp;
    let type = premium ? 'premium' : 'non-registered';

    if (type === 'non-registered') {
      type = userEntitlements.legacyEntitlements.filter(value =>
        value.company === 'bw' || value.company === 'fs',
      ).reduce((result, value) => {
        if (value.userType_alias.indexOf('premium') > -1) {
          if (value.isActive === true) {
            result = 'premium';
          } else {
            result = 'expired';
          }
        } else if (value.userType_alias === 'bw_basic') {
          result = 'registered';
        }
        return result;
      }, 'non-registered');
    }

    req.user = {
      id: req.authenticatedUserId,
      entitlements: userEntitlements.entitlements,
      premium,
      type,
    };
  }
  next();
  /* eslint-enable no-param-reassign */
};

export default wrapErrors(entitleRequest);
