import sinon from 'sinon';
import { expect } from 'chai';
import checkForecastParams from './checkForecastParams';

describe('middleware / checkForecastParams', () => {
  const next = sinon.spy();
  const send = sinon.spy();
  const res = { status: sinon.stub().returns({ send }) };

  afterEach(() => {
    next.reset();
    send.reset();
    res.status.reset();
  });

  it('should return 16 days for a premium user', () => {
    const req = { query: {}, user: { premium: true } };
    checkForecastParams(req, res, next);

    expect(req.forecast).to.deep.equal({ days: 16, offset: 0, interval: 180 });
    expect(next).to.have.been.calledOnce();
    expect(res.status).not.to.have.been.called();
  });

  it('should return 2 days for a registered user', () => {
    const req = { query: {}, user: { premium: false } };
    checkForecastParams(req, res, next);

    expect(req.forecast).to.deep.equal({ days: 2, offset: 0, interval: 180 });
    expect(next).to.have.been.calledOnce();
    expect(res.status).not.to.have.been.called();
  });

  it('should error if more than five days are requested for a non-premium user', () => {
    const req = { query: { days: 7 }, user: { premium: false } };
    checkForecastParams(req, res, next);

    expect(res.status).to.have.been.calledOnce();
    expect(res.status).to.have.been.calledWithExactly(400);
    expect(send).to.have.been.calledOnce();
    expect(send).to.have.been.calledWithExactly({ message: 'Parameters out of bounds' });
    expect(next).not.to.have.been.called();
  });

  it('should error if data past 16 days is requested', () => {
    const req = { query: { days: 16, offset: 4 }, user: { premium: true } };
    checkForecastParams(req, res, next);

    expect(res.status).to.have.been.calledOnce();
    expect(res.status).to.have.been.calledWithExactly(400);
    expect(send).to.have.been.calledOnce();
    expect(send).to.have.been.calledWithExactly({ message: 'Parameters out of bounds' });
    expect(next).not.to.have.been.called();
  });
});
