import { wrapErrors } from '@surfline/services-common';
import { getWaterways } from '../../external/waterways';

const waterwaysRequest = async (req, _, next) => {
  /* eslint-disable no-param-reassign */
  req.waterways = null;
  const lat = req.query.lat || null;
  const lon = req.query.lon || null;

  if (lat && lon) {
    try {
      const waterways = await getWaterways(lat, lon);
      if (waterways && waterways.waterways) {
        req.waterways = waterways.waterways;
        return next();
      }
    } catch (err) {
      return next();
    }
  }
  return next();
  /* eslint-enable no-param-reassign */
};

export default wrapErrors(waterwaysRequest);
