import { wrapErrors } from '@surfline/services-common';
import getUnits from '../../utils/getUnits';

const userUnits = async (req, _, next) => {
  /* eslint-disable no-param-reassign */
  const units = req.query.unit ? req.query.unit : req.query.units;
  req.units = await getUnits(req.authenticatedUserId, req.geoCountryIso, units);
  return next();
  /* eslint-enable no-param-reassign */
};

export default wrapErrors(userUnits);
