const checkForecastParams = (req, res, next) => {
  /* eslint-disable no-param-reassign */
  const { user: { premium }, query } = req;

  const legacy = !!req.legacy;
  const baseMaxDays = premium ? 16 : 2;
  const legacyMaxDays = premium ? 7 : 2;
  const maxDays = legacy ? legacyMaxDays : baseMaxDays;

  const days = query.days ? parseInt(query.days, 10) : maxDays;
  const interval = query.interval ? parseInt(query.interval, 10) : 180;
  // TODO: No longer need offset if we are fetching all data in one go
  const offset = query.offset ? parseInt(query.offset, 10) : 0;

  if (offset > maxDays || days > maxDays || offset + days > maxDays) {
    return res.status(400).send({ message: 'Parameters out of bounds' });
  }

  req.forecast = { days, offset, interval };
  return next();
  /* eslint-enable no-param-reassign */
};

export default checkForecastParams;
