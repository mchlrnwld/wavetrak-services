import { errorHandlerMiddleware } from '@surfline/services-common/dist/middlewares/errorHandlerMiddleware';
import authenticateRequest from '@surfline/services-common/dist/middlewares/authenticateRequest';
import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import * as entitlementsAPI from '../../external/entitlements';
import entitlements from './entitlements';

describe('middleware / entitlements', () => {
  let request;

  before(() => {
    const app = express();
    app.use(authenticateRequest(), entitlements, (req, res) => res.send(req.spot));
    app.use(errorHandlerMiddleware({ error: sinon.spy() }));
    request = chai.request(app);
  });

  it('returns 500 if get entitlements errors', async () => {
    sinon.stub(entitlementsAPI, 'default').throws();

    try {
      await request
        .get('/')
        .set('X-Auth-UserId', '56fae6ff19b74318a73a7875')
        .send();
    } catch (err) {
      expect(err).to.have.status(500);
      expect(err.response.body).to.deep.equal({ message: 'Error encountered' });
    } finally {
      entitlementsAPI.default.restore();
    }
  });
});
