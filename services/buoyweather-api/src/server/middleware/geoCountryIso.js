const geoCountryIso = (req, res, next) => {
  /* eslint-disable no-param-reassign */
  if (req.headers['x-geo-country-iso']) {
    req.geoCountryIso = req.headers['x-geo-country-iso'];
  }
  return next();
  /* eslint-enable no-param-reassign */
};

export default geoCountryIso;
