export default timestamps => ({
  associated: {
    model: {
      agency: 'NOAA',
      model: 'GFS',
      run: 2021032406,
      grid: {
        key: '0p25',
        lat: 50.25,
        lon: 355.75,
      },
    },
    timezones: [
      {
        refId: 0,
        name: 'Europe/London',
        abbr: 'GMT',
        offset: 0,
      },
    ],
    units: {
      windSpeed: 'knot',
      temperature: 'C',
      pressure: 'mb',
      visibility: 'm',
      precipitation: 'mm',
    },
  },
  data: timestamps.map(t => ({
    timestamp: t,
    timezoneRefId: 0,
    weather: {
      wind: {
        direction: 219.21747,
        speed: 22.13616,
        gust: 20.03686,
      },
      pressure: 1021.5529,
      temperature: 9.71787,
      visibility: 24134.889,
      dewpoint: 6.84543,
      humidity: 82.1,
      precipitation: {
        type: 'RAIN',
        volume: 0.0625,
      },
      conditions: 'LIGHT_SHOWERS',
    },
  })),
});
