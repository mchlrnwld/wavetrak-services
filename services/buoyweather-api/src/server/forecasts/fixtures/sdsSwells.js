export default timestamps => ({
  associated: {
    model: {
      agency: 'Wavetrak',
      model: 'Lotus-WW3',
      run: 2021032400,
      grid: {
        key: 'GLOB_30m',
        lat: 50.5,
        lon: 356,
      },
    },
    timezones: [
      {
        refId: 0,
        name: 'Europe/London',
        abbr: 'GMT',
        offset: 0,
      },
    ],
    units: {
      waveHeight: 'ft',
    },
  },
  data: timestamps.map(t => ({
    timestamp: t,
    timezoneRefId: 0,
    swells: {
      combined: {
        height: 2.4,
        direction: 236,
        period: 12,
      },
      components: [
        {
          height: 0,
          direction: 0,
          period: 0,
        },
        {
          height: 0,
          direction: 0,
          period: 0,
        },
        {
          height: 0,
          direction: 0,
          period: 0,
        },
        {
          height: 0,
          direction: 0,
          period: 0,
        },
        {
          height: 0,
          direction: 0,
          period: 0,
        },
        {
          height: 0,
          direction: 0,
          period: 0,
        },
      ],
    },
  })),
});
