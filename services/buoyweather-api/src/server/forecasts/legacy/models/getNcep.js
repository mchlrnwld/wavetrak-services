import Joi from 'joi';
import * as validations from '../../../../validations/legacy/forecast';
import {
  getDayNumber,
  chartDayFormat,
  isEmpty,
} from '../../../../utils/legacyFunctions';
import legacyEndpoint from '../../../../helpers/legacyEndpoint';
import getWrittenWindWave from '../../../../external/legacy/writtenWindWave';
import allowableForecastRange from '../../../../helpers/allowableForecastRange';
import getClosestValidStartTimestamp from '../../../../helpers/getClosestValidStartTimestamp';
import {
  timezoneData,
  getTimezoneArray,
} from '../../../../utils/datetime';
import getNearbyRegionData from '../../../../helpers/getNearbyRegionData';

const getNcep = async (req) => {
  const { user, units } = req;
  const { premium, type } = user;
  // support legacy when non-premium
  const lat = Number(req.query.lat);
  const lon = Number(req.query.lon);
  const legacy = !!req.legacy;
  const region = await getNearbyRegionData(lat, lon);
  const grb = region && region.region_model_grb ? region.region_model_grb : 'enp';

  /*
  Get region data here for legacy link unit values below
  */
  const legacyUnits = units.wave && units.wave.height === 'm' ? 'm' : 'e';
  // legacy uses hard coded interval of 360
  const interval = 360;
  const intervalSeconds = interval * 60;
  const utcOffset = timezoneData(lat, lon).offset;
  const {
    minStart,
    maxEnd,
  } = await allowableForecastRange(utcOffset, interval, premium, legacy);
  const requestedStart = parseInt(req.query.start, 10) || minStart;
  const requestedEnd = parseInt(req.query.end, 10) || maxEnd;
  const start = getClosestValidStartTimestamp(
    minStart,
    intervalSeconds,
    requestedStart,
  );
  const end = Math.min(requestedEnd, maxEnd);
  const tz = getTimezoneArray(lat, lon, start, end)[0].offset;
  const userType = type;
  const result = await getWrittenWindWave({ lat, lon, tz, units: legacyUnits, grb });
  const freeDays = 2;
  const maxTimeStepsAllowed = 4 * freeDays;
  let dates = result.modeloutput.date;
  dates = (userType === 'premium') ? dates : dates.splice(0, maxTimeStepsAllowed);

  const days = [];
  let day = {};
  day.date = '';
  day.timeSteps = [];
  let greatestWindSpeed = 0;
  let greatestWaveHeight = 0;

  // used for debugging bad data
  let badData = false;

  dates.forEach((currentLocalDate, index) => {
    const model = result.modeloutput;

    const checkWindSpeed = model.windSpeedPeak[index];
    greatestWindSpeed = (checkWindSpeed > greatestWindSpeed) ? checkWindSpeed : greatestWindSpeed;

    const checkWaveHeight = model.heightPeak[index];
    greatestWaveHeight = (checkWaveHeight > greatestWaveHeight) ?
      checkWaveHeight : greatestWaveHeight;

    let timeStep = {
      date: {
        local: currentLocalDate,
        utc: model.dateUTC[index],
      },
    };

    let validated = Joi.validate(timeStep, validations.timeStepControl);
    if (validated.error) {
      timeStep = validations.timeStepDefault;
      badData = true;
    }

    let waveTimeStep = {
      period: model.period[index],
      direction: {
        cardinal: model.heightDirText[index],
        iconPath: model.heightDirImg[index],
        azimuth: model.heightDir[index],
      },
      height: {
        peak: model.heightPeak[index],
        average: model.heightAvg[index],
      },
    };

    validated = Joi.validate(waveTimeStep, validations.waveStepControl);
    if (validated.error) {
      waveTimeStep = validations.waveStepDefault;
      badData = true;
    }

    let windTimeStep = {
      direction: {
        cardinal: model.windDirText[index],
        iconPath: model.windDirImg[index],
        azimuth: model.windDirectionDegrees[index],
      },
      speed: {
        current: model.windSpeed[index],
        average: model.windSpeedAvg[index],
        peak: model.windSpeedPeak[index],
      },
    };

    validated = Joi.validate(windTimeStep, validations.windStepControl);
    if (validated.error) {
      windTimeStep = validations.windStepDefault;
      badData = true;
    }
    timeStep.wave = (waveTimeStep.height.average < 0) ? null : waveTimeStep;
    timeStep.wind = (windTimeStep.speed.average < 0) ? null : windTimeStep;

    if (timeStep.wave === null || timeStep.wind === null) {
      badData = true;
    }

    let lastDate;
    if (index) {
      lastDate = dates[index - 1];
    }

    const lastDateDay = getDayNumber(lastDate);
    const currentDateDay = getDayNumber(currentLocalDate);

    function checkIfLast() {
      if (((dates.length - (index + 1)) === 0) && day.timeSteps.length !== 0) {
        day.date = chartDayFormat(currentLocalDate);
        days.push(day);
      }
    }

    if (currentDateDay === lastDateDay || index === 0) {
      day.timeSteps.push(timeStep);
      checkIfLast();
    } else {
      day.date = chartDayFormat(lastDate);
      days.push(day);
      day = {};
      day.date = '';
      day.timeSteps = [];
      day.timeSteps.push(timeStep);
      checkIfLast();
    }
  });

  let writtenForecastDays = result.modeloutput.forecastDays;

  // if wind AND wave data don't exist, don't show written forecast
  if (greatestWindSpeed === 0 && greatestWaveHeight === 0) {
    writtenForecastDays = null;
  } else {
    const writtenRegEx = /-\d+/; // any negative digits.
    writtenForecastDays = (userType === 'premium') ? writtenForecastDays : writtenForecastDays.splice(0, freeDays);

    writtenForecastDays.forEach((writtenDay) => {
      /* eslint-disable no-param-reassign */
      writtenDay.AM.winds = (writtenRegEx.test(writtenDay.AM.winds)) ? 'Unavailable' : writtenDay.AM.winds;
      writtenDay.AM.seas = (writtenRegEx.test(writtenDay.AM.seas)) ? 'Unavailable' : writtenDay.AM.seas;

      writtenDay.PM.winds = (writtenRegEx.test(writtenDay.PM.winds)) ? 'Unavailable' : writtenDay.PM.winds;
      writtenDay.PM.seas = (writtenRegEx.test(writtenDay.PM.seas)) ? 'Unavailable' : writtenDay.PM.seas;
      /* eslint-enable no-param-reassign */
    });
  }


  // investigate empty values that are crashing iOS.
  if (isEmpty(greatestWindSpeed)) {
    greatestWindSpeed = 0;
  }
  if (isEmpty(greatestWaveHeight)) {
    greatestWaveHeight = 0;
  }

  if (typeof result.modeloutput.units !== 'string') {
    result.modeloutput.units = 'm';
  }
  if (typeof result.modeloutput.waveUnits !== 'string') {
    result.modeloutput.waveUnits = 'm';
  }
  if (typeof result.modeloutput.windUnits !== 'string') {
    result.modeloutput.windUnits = 'kts';
  }
  // get legacy urls
  const printUrl = legacyEndpoint.url('wxnav6.jsp', [lat, lon], { print: true, units: legacyUnits }, region);
  // for opting out of new forcast page.
  const optOutUrl = legacyEndpoint.url('wxnav5-fcstLanding.jsp', [lat, lon], { units: legacyUnits }, region);
  const dailyEmailUrl = legacyEndpoint.url('wxnav5.jsp', [lat, lon], { program: 'dailyAddB', units: legacyUnits }, region);
  const dailyEmailAlertUrl = legacyEndpoint.url('wxnav5.jsp', [lat, lon], { program: 'alertAdd1B', units: legacyUnits }, region);
  const embedUrl = legacyEndpoint.url('widgetAdd.jsp', [lat, lon], { units: legacyUnits, lat, lon }, region);

  return {
    writtenForecast: writtenForecastDays,
    charts: {
      days,
      maxWind: greatestWindSpeed,
      maxWave: greatestWaveHeight,
      units: {
        global: result.modeloutput.units,
        wave: result.modeloutput.waveUnits,
        wind: result.modeloutput.windUnits,
      },
    },
    forecastType: userType,
    printUrl,
    optOutUrl,
    dailyEmailUrl,
    dailyEmailAlertUrl,
    embedUrl,
    nearbyRegion: region,
    badData,
    marineAPI: `http://api.buoyweather.com/v1/buoyweather/data?product=nww3&lat=${lat}&lon=${lon}&tz=${tz}&units=${legacyUnits}&grb=${grb}`,
  };
};

export default getNcep;
