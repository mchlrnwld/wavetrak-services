import getForecasts from '../../../../helpers/getForecasts';
import allowableForecastRange from '../../../../helpers/allowableForecastRange';
import getClosestValidStartTimestamp from '../../../../helpers/getClosestValidStartTimestamp';
import legacyEndpoint from '../../../../helpers/legacyEndpoint';
import { forecastTransform } from '../../../../helpers/legacyForecastTransform';
import round from '../../../../utils/round';
import cardinal from '../../../../utils/mapDegreesCardinal';
import getNearbyRegionData from '../../../../helpers/getNearbyRegionData';

import weatherCondition from '../../../../utils/weatherCondition';
import buildSunlightTimes from '../../../../helpers/buildSunlightTimes';
import {
  fromUnixTimestamp,
  toUnixTimestamp,
  midnightOfDate,
  timezoneData,
  getTimezoneArray,
  getTimezoneRefByOffset,
} from '../../../../utils/datetime';
import {
  writtenForecast,
} from '../../../../utils/writtenForecast';

const getLotus = async (req) => {
  const { user, units } = req;
  const { premium } = user;
  const lat = Number(req.query.lat);
  const lon = Number(req.query.lon);
  const legacy = !!req.legacy;
  const waterways = false;
  /*
  Get region data here for legacy link unit values below
  */
  const region = await getNearbyRegionData(lat, lon);

  // legacy uses hard coded interval of 360
  const interval = 360;
  const intervalSeconds = interval * 60;
  const utcOffset = timezoneData(lat, lon).offset;

  const {
    minStart,
    maxEnd,
    totalTimeSteps,
  } = await allowableForecastRange(utcOffset, interval, premium, legacy);

  const requestedStart = parseInt(req.query.start, 10) || minStart;
  const requestedEnd = parseInt(req.query.end, 10) || maxEnd;

  const start = getClosestValidStartTimestamp(
    minStart,
    intervalSeconds,
    requestedStart,
  );
  const end = Math.min(requestedEnd, maxEnd);
  const timezone = getTimezoneArray(lat, lon, start, end);

  const {
    offshoreCoordinate,
    waveForecastMap,
    weatherForecastMap,
  } = await getForecasts({
    lat,
    lon,
    start,
    end,
    interval: intervalSeconds,
    units,
    waterways,
  });

  const dawnDuskTimes = buildSunlightTimes({
    start,
    end,
    lat,
    lon,
  });

  const dawnDuskTimesMap = new Map(dawnDuskTimes.map(({
    midnight,
    dawn,
    dusk,
  }) => [
    midnight,
    { dawn, dusk },
  ]));

  const numberOfForecasts = Math.floor((end - start) / intervalSeconds);
  const numberOfForecastsReturn = numberOfForecasts <= totalTimeSteps ?
    numberOfForecasts : totalTimeSteps;
  const forecasts = [...Array(numberOfForecastsReturn)].map((_, i) => {
    const timestamp = start + (i * intervalSeconds);
    const waveForecast = waveForecastMap ? waveForecastMap.get(timestamp) : null;
    const weatherForecast = weatherForecastMap ? weatherForecastMap.get(timestamp) : null;
    const timezoneOffset = timezoneData(lat, lon, timestamp).offset;
    const midnight = midnightOfDate(timezoneOffset, fromUnixTimestamp(timestamp));
    const { dawn, dusk } = dawnDuskTimesMap.get(toUnixTimestamp(midnight));
    return {
      timestamp,
      timezone: getTimezoneRefByOffset(timezone, timezoneOffset),
      weather: weatherForecast ? {
        temperature: round(weatherForecast.weather.temperature, 0),
        condition: weatherCondition(
          weatherForecast.weather.conditions,
          timestamp < dawn || dusk <= timestamp,
        ),
        pressure: weatherForecast.weather.pressure,
        precipitation: {
          ...weatherForecast.weather.precipitation,
          volume: round(weatherForecast.weather.precipitation.volume, 1),
        },
      } : null,
      wind: weatherForecast ? {
        speed: {
          average: weatherForecast.weather.wind.speed,
          peak: weatherForecast.weather.wind.gust,
        },
        direction: {
          cardinal: cardinal(weatherForecast.weather.wind.direction),
          azimuth: weatherForecast.weather.wind.direction,
        },
      } : null,
      wave: waveForecast ? {
        period: waveForecast.swells.combined.period,
        height: {
          mean: waveForecast.swells.combined.height,
          peak: round(waveForecast.swells.combined.height * 1.3, 1),
        },
        direction: {
          cardinal: cardinal(waveForecast.swells.combined.direction),
          azimuth: waveForecast.swells.combined.direction,
        },
        offshoreCoordinate,
      } : null,
    };
  });

  // get interval of 180 for written forecasts for legacy
  // standard interval here is 360 for legacy, so needs to match site
  const intervalWritten = 180;
  const intervalSecondsWritten = 180 * 60;
  const {
    weatherForecastMap: weatherForecastMapWritten,
    waveForecastMap: waveForecastMapWritten,
  } = await getForecasts({
    lat,
    lon,
    start,
    end,
    interval: intervalSecondsWritten,
    units,
    waterways,
  });

  const {
    totalTimeSteps: totalTimeStepsWritten,
  } = await allowableForecastRange(utcOffset, intervalWritten, premium, legacy);

  const numberOfForecastsWritten = Math.floor((end - start) / intervalSecondsWritten);
  const numberOfForecastsWrittenReturn = numberOfForecastsWritten <= totalTimeStepsWritten ?
    numberOfForecastsWritten : totalTimeStepsWritten;

  const forecastsForWritten = [...Array(numberOfForecastsWrittenReturn)].map((_, i) => {
    const timestamp = start + (i * intervalSecondsWritten);
    const weatherForecastWritten = weatherForecastMapWritten ?
      weatherForecastMapWritten.get(timestamp) : null;
    const waveForecastWritten = waveForecastMapWritten ?
      waveForecastMapWritten.get(timestamp) : null;
    const timezoneOffset = timezoneData(lat, lon, timestamp).offset;
    return {
      timestamp,
      timezone: getTimezoneRefByOffset(timezone, timezoneOffset),
      wind: weatherForecastWritten ? {
        speed: {
          average: weatherForecastWritten.weather.wind.speed,
          peak: weatherForecastWritten.weather.wind.gust,
        },
        direction: {
          cardinal: cardinal(weatherForecastWritten.weather.wind.direction),
          azimuth: weatherForecastWritten.weather.wind.direction,
        },
      } : null,
      wave: waveForecastWritten ? {
        period: waveForecastWritten.swells.combined.period,
        height: {
          mean: waveForecastWritten.swells.combined.height,
          peak: round(waveForecastWritten.swells.combined.height * 1.3, 1),
        },
        direction: {
          cardinal: cardinal(waveForecastWritten.swells.combined.direction.mean),
          azimuth: waveForecastWritten.swells.combined.direction.mean,
        },
        offshoreCoordinate,
      } : null,
    };
  });

  const written = writtenForecast(forecastsForWritten, units, lat, lon, timezone);

  const legacyUnits = units.wave.height === 'm' ? 'm' : 'e';
  // get legacy urls
  const print = legacyEndpoint.url('wxnav6.jsp', [lat, lon], { print: true, units: legacyUnits }, region);
  // for opting out of new forcast page.
  const optOut = legacyEndpoint.url('wxnav5-fcstLanding.jsp', [lat, lon], { units: legacyUnits }, region);
  const dailyEmail = legacyEndpoint.url('wxnav5.jsp', [lat, lon], { program: 'dailyAddB', units: legacyUnits }, region);
  const dailyEmailAlert = legacyEndpoint.url('wxnav5.jsp', [lat, lon], { program: 'alertAdd1B', units: legacyUnits }, region);
  const embed = legacyEndpoint.url('widgetAdd.jsp', [lat, lon], { units: legacyUnits, lat, lon }, region);
  const urls = {
    print,
    optOut,
    dailyEmail,
    dailyEmailAlert,
    embed,
  };

  let response = {
    associated: {
      units,
      timezone,
      location: {
        lon,
        lat,
      },
      region,
      urls,
      waterways,
    },
    data: {
      written,
      forecasts,
    },
  };

  // for this legacy request, transform response to match legacy contract
  const legacyUserSettings = {
    type: premium ? 'premium' : 'non-registered',
  };
  response = forecastTransform(response, legacyUserSettings);
  return response;
};

export default getLotus;
