import getLotus from './models/getLotus';
import getNcep from './models/getNcep';

const getLegacyForecastHandler = async (req, res) => {
  if (req.bwWindWaveModel === 'LOLA') {
    return res.send(await getLotus(req));
  }
  return res.send(await getNcep(req));
};

export default getLegacyForecastHandler;
