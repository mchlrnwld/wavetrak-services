import allowableForecastRange from '../../../helpers/allowableForecastRange';
import getClosestValidStartTimestamp from '../../../helpers/getClosestValidStartTimestamp';
import legacyEndpoint from '../../../helpers/legacyEndpoint';
import round from '../../../utils/round';
import cardinal from '../../../utils/mapDegreesCardinal';
import getNearbyRegionData from '../../../helpers/getNearbyRegionData';
import weatherCondition from '../../../utils/weatherCondition';
import buildSunlightTimes from '../../../helpers/buildSunlightTimes';

import {
  fromUnixTimestamp,
  toUnixTimestamp,
  midnightOfDate,
  timezoneData,
  getTimezoneArray,
  getTimezoneRefByOffset,
} from '../../../utils/datetime';

import { writtenForecast } from '../../../utils/writtenForecast';

import {
  getSwellForecast,
  getWeatherForecast,
} from '../../../external/scienceDataServiceForecast';


const getLotus = async (req) => {
  const { type, user } = req;
  const { units, waterways } = req;
  const { premium } = user;
  const lat = Number(req.query.lat);
  const lon = Number(req.query.lon);

  const region = await getNearbyRegionData(lat, lon);

  const interval = req.query.interval ?
    Number(req.query.interval) : 360;

  const intervalSeconds = interval * 60;
  const utcOffset = timezoneData(lat, lon).offset;
  const {
    minStart,
    maxEnd,
  } = await allowableForecastRange(utcOffset, interval, premium, false);

  const requestedStart = parseInt(req.query.start, 10) || minStart;
  const requestedEnd = parseInt(req.query.end, 10) || maxEnd;

  const legacyUnits = units.wave && units.wave.height === 'm' ? 'm' : 'e';

  const start = getClosestValidStartTimestamp(
    requestedStart,
    intervalSeconds,
    requestedStart,
  );
  const end = Math.min(requestedEnd, maxEnd);
  const timezone = getTimezoneArray(lat, lon, start, end);

  const dawnDuskTimes = buildSunlightTimes({ start, end, lat, lon });
  const dawnDuskTimesMap = new Map(dawnDuskTimes.map(row => [row.midnight, row]));

  let params = ({ lat, lon, start, interval: intervalSeconds, units });
  if (!premium) {
    params = { ...params, end };
  }

  // Fetch Science Data Service forecasts
  const [weather, wave] = await Promise.all([
    getWeatherForecast(params),
    !waterways && getSwellForecast(params),
  ]);

  // Convert forecast data to Map type, index by forecast time for easy lookup below.
  const [weatherMap, waveMap] = [weather, wave].map(
    body => (body && body.data ? new Map(body.data.map(row => [row.timestamp, row]))
      : new Map()),
  );

  const numberOfForecasts = Math.floor((end - start) / intervalSeconds);

  const forecasts = [...Array(numberOfForecasts)].map((_, i) => {
    const timestamp = start + (i * intervalSeconds);
    const waveRow = waveMap.get(timestamp);
    const weatherRow = weatherMap.get(timestamp);
    const timezoneOffset = timezoneData(lat, lon, timestamp).offset;
    const midnight = midnightOfDate(timezoneOffset, fromUnixTimestamp(timestamp));
    const { dawn, dusk } = dawnDuskTimesMap.get(toUnixTimestamp(midnight));

    return {
      timestamp,
      timezone: getTimezoneRefByOffset(timezone, timezoneOffset),
      weather: weatherRow ? {
        temperature: round(weatherRow.weather.temperature, 0),
        condition: weatherCondition(
          weatherRow.weather.conditions,
          timestamp < dawn || dusk <= timestamp,
        ),
        pressure: round(weatherRow.weather.pressure, 0),
        precipitation: {
          ...weatherRow.weather.precipitation,
          volume: round(weatherRow.weather.precipitation.volume, 1),
        },
      } : null,
      wind: weatherRow ? {
        speed: {
          average: weatherRow.weather.wind.speed,
          peak: Math.max(weatherRow.weather.wind.gust, weatherRow.weather.wind.speed),
        },
        direction: {
          cardinal: cardinal(weatherRow.weather.wind.direction),
          azimuth: weatherRow.weather.wind.direction,
        },
      } : null,
      wave: waveRow ? {
        period: round(waveRow.swells.combined.period, 0),
        height: {
          mean: round(waveRow.swells.combined.height, 1),
          peak: round(waveRow.swells.combined.height * 1.3, 1), // Use 1.3 as per table at https://github.com/Surfline/wavetrak-services/blob/master/architecture/document/buoyweather-lotus-integration.md#6-hourly-forecast-properties
        },
        direction: {
          cardinal: cardinal(waveRow.swells.combined.direction),
          azimuth: waveRow.swells.combined.direction,
        },
        offshoreCoordinate: {
          lat: wave.associated.model.grid.lat,
          lon: wave.associated.model.grid.lon,
        },
      } : null,
    };
  });

  const urls = {
    print: legacyEndpoint.url('wxnav6.jsp', [lat, lon], { print: true, units: legacyUnits }, region),
    optOut: legacyEndpoint.url('wxnav5-fcstLanding.jsp', [lat, lon], { units: legacyUnits }, region),
    dailyEmail: legacyEndpoint.url('wxnav5.jsp', [lat, lon], { program: 'dailyAddB', units: legacyUnits }, region),
    dailyEmailAlert: legacyEndpoint.url('wxnav5.jsp', [lat, lon], { program: 'alertAdd1B', units: legacyUnits }, region),
    embed: legacyEndpoint.url('widgetAdd.jsp', [lat, lon], { units: legacyUnits, lat, lon }, region),
  };

  // Update units object with what we actually got from SDS (but match leagcy casing).
  if (weather && weather.associated) {
    units.temperature = weather.associated.units.temperature.toLowerCase();
    units.wind = weather.associated.units.windSpeed === 'knot' ? 'kts' : weather.associated.units.windSpeed;
    units.pressure = weather.associated.units.pressure;
    units.visibility = weather.associated.units.visibility;
    units.precipitation.volume = weather.associated.units.precipitation;
  }

  if (wave && wave.associated) {
    units.wave.height = wave.associated.units.waveHeight;
  }

  // Convert model run time from SDS to unix timestamp
  const runTimeString = wave.associated.model.run.toString();

  const year = runTimeString.slice(0, 4);
  const month = runTimeString.slice(4, 6);
  const day = runTimeString.slice(6, 8);
  const hour = runTimeString.slice(8);

  const runTime = new Date(`${year}-${month}-${day}T${hour}:00`).getTime() / 1000;

  return {
    associated: {
      runTime,
      units,
      timezone,
      location: {
        lon,
        lat,
      },
      region,
      urls,
      waterways,
    },
    data: {
      written: type === 'written' ? writtenForecast(forecasts, units, lat, lon, timezone, 5, 13) : undefined,
      forecasts: type !== 'written' ? forecasts : undefined,
    },
  };
};

export default getLotus;
