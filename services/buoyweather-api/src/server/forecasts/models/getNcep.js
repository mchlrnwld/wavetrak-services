import getForecasts from '../../../helpers/getForecasts';
import allowableForecastRange from '../../../helpers/allowableForecastRange';
import getClosestValidStartTimestamp from '../../../helpers/getClosestValidStartTimestamp';
import legacyEndpoint from '../../../helpers/legacyEndpoint';
import round from '../../../utils/round';
import cardinal from '../../../utils/mapDegreesCardinal';
import getNearbyRegionData from '../../../helpers/getNearbyRegionData';
import weatherCondition from '../../../utils/weatherCondition';
import buildSunlightTimes from '../../../helpers/buildSunlightTimes';
import {
  fromUnixTimestamp,
  toUnixTimestamp,
  midnightOfDate,
  timezoneData,
  getTimezoneArray,
  getTimezoneRefByOffset,
} from '../../../utils/datetime';
import {
  writtenForecast,
} from '../../../utils/writtenForecast';

// NCEP specific
import getWrittenWindWave from '../../../external/legacy/writtenWindWave';
import {
  getWaveData,
  getWindData,
} from '../../../helpers/formatNcepData';

const getNcep = async (req) => {
  const { type, user } = req;
  const { units, waterways } = req;
  const { premium } = user;
  const lat = Number(req.query.lat);
  const lon = Number(req.query.lon);

  const region = await getNearbyRegionData(lat, lon);

  const interval = req.query.interval ?
    Number(req.query.interval) : 360;

  const intervalSeconds = interval * 60;
  const utcOffset = timezoneData(lat, lon).offset;
  const {
    minStart,
    maxEnd,
    totalTimeSteps,
  } = await allowableForecastRange(utcOffset, interval, premium, true);
  const tz = getTimezoneArray(lat, lon, minStart, maxEnd)[0].offset;

  const legacyUnits = units.wave && units.wave.height === 'm' ? 'm' : 'e';
  const grb = region && region.region_model_grb ? region.region_model_grb : 'enp';
  const result = await getWrittenWindWave({ lat, lon, tz, units: legacyUnits, grb });
  const modelOutput = result.modeloutput;

  // set necessary legacy api data into an object for translation
  const legacyApiData = {
    lat,
    lon,
    dateString: modelOutput.dateString,
    windDirText: modelOutput.windDirText,
    windSpeed: modelOutput.windSpeed,
    windDir: modelOutput.windDir,
    windDirection: modelOutput.windDirection,
    windSpeedAvg: modelOutput.windSpeedAvg,
    windSpeedPeak: modelOutput.windSpeedPeak,
    period: modelOutput.period,
    heightDirText: modelOutput.heightDirText,
    waveDirectionDegrees: modelOutput.waveDirectionDegrees,
    waveDirection: modelOutput.waveDirection,
    heightPeak: modelOutput.heightPeak,
    heightAvg: modelOutput.heightAvg,
  };

  // get maps from legacy response
  const waveForecastMapNcep = !waterways ?
    getWaveData(legacyApiData) : null;
  let windForecastMapNcep = !waterways ?
    getWindData(legacyApiData) : null;

  // use base response to create start / end timestamps
  // TODO: Handle premium data, check waterways response options
  const firstDate = modelOutput.dateString.length ? modelOutput.dateString[0] : null;
  const firstDateJS = firstDate ? new Date(`${firstDate} GMT+00:00`) : null;
  const firstDateUnix = firstDateJS ? toUnixTimestamp(firstDateJS) : null;
  const reqStart = firstDateUnix || req.query.start || null;
  const lastDate = modelOutput.dateString.length ?
    modelOutput.dateString[modelOutput.dateString.length - 1] : null;
  const lastDateJS = lastDate ? new Date(`${lastDate} GMT+00:00`) : null;
  const lastDateUnix = lastDateJS ? toUnixTimestamp(lastDateJS) : null;
  const reqEnd = lastDateUnix || req.query.end || null;

  const requestedStart = parseInt(reqStart, 10) || minStart;
  const requestedEnd = parseInt(reqEnd, 10) || maxEnd;

  const start = getClosestValidStartTimestamp(
    requestedStart,
    intervalSeconds,
    requestedStart,
  );
  const end = Math.min(requestedEnd, maxEnd);
  const timezone = getTimezoneArray(lat, lon, start, end);

  const {
    windForecastMap,
    weatherForecastMap,
  } = await getForecasts({
    lat,
    lon,
    start,
    end,
    interval,
    units,
    waterways,
  });

  windForecastMapNcep = waveForecastMapNcep ? windForecastMapNcep : windForecastMap;

  const dawnDuskTimes = buildSunlightTimes({
    start,
    end,
    lat,
    lon,
  });

  const dawnDuskTimesMap = new Map(dawnDuskTimes.map(({
    midnight,
    dawn,
    dusk,
  }) => [
    midnight,
    { dawn, dusk },
  ]));

  let numberOfForecasts = 0;
  [...Array(totalTimeSteps)].forEach((_, tsIdx) => {
    const timestampCheck = start + (tsIdx * intervalSeconds);
    if (timestampCheck <= end) {
      numberOfForecasts = tsIdx + 1;
    }
  });
  const forecasts = [...Array(numberOfForecasts)].map((_, i) => {
    const timestamp = start + (i * intervalSeconds);
    const windForecast = windForecastMapNcep ? windForecastMapNcep.get(timestamp) : null;
    const waveForecast = waveForecastMapNcep ? waveForecastMapNcep.get(timestamp) : null;
    const weatherForecast = weatherForecastMap ? weatherForecastMap.get(timestamp) : null;
    const timezoneOffset = timezoneData(lat, lon, timestamp).offset;
    const midnight = midnightOfDate(timezoneOffset, fromUnixTimestamp(timestamp));
    const { dawn, dusk } = dawnDuskTimesMap.get(toUnixTimestamp(midnight));
    return {
      timestamp,
      timezone: getTimezoneRefByOffset(timezone, timezoneOffset),
      weather: weatherForecast ? {
        temperature: round(weatherForecast.weather.temperature, 0),
        condition: weatherCondition(
          weatherForecast.weather.skycondition.key,
          timestamp < dawn || dusk <= timestamp,
        ),
        pressure: weatherForecast.weather.pressure,
        precipitation: {
          ...weatherForecast.weather.precipitation,
          volume: round(weatherForecast.weather.precipitation.volume, 1),
        },
      } : null,
      wind: windForecast ? {
        speed: {
          average: windForecast.wind.speed,
          peak: windForecast.wind.gust,
        },
        direction: {
          cardinal: cardinal(windForecast.wind.direction),
          azimuth: windForecast.wind.direction,
        },
      } : null,
      wave: waveForecast ?
        { ...waveForecast.wave } : null,
    };
  });

  const written = writtenForecast(forecasts, units, lat, lon, timezone, 5, 13);

  // get legacy urls
  const print = legacyEndpoint.url('wxnav6.jsp', [lat, lon], { print: true, units: legacyUnits }, region);
  // for opting out of new forcast page.
  const optOut = legacyEndpoint.url('wxnav5-fcstLanding.jsp', [lat, lon], { units: legacyUnits }, region);
  const dailyEmail = legacyEndpoint.url('wxnav5.jsp', [lat, lon], { program: 'dailyAddB', units: legacyUnits }, region);
  const dailyEmailAlert = legacyEndpoint.url('wxnav5.jsp', [lat, lon], { program: 'alertAdd1B', units: legacyUnits }, region);
  const embed = legacyEndpoint.url('widgetAdd.jsp', [lat, lon], { units: legacyUnits, lat, lon }, region);
  const urls = {
    print,
    optOut,
    dailyEmail,
    dailyEmailAlert,
    embed,
  };

  const response = {
    associated: {
      units,
      timezone,
      location: {
        lon,
        lat,
      },
      region,
      urls,
      waterways,
    },
    data: {
      written: type === 'written' ? written : undefined,
      forecasts: type !== 'written' ? forecasts : undefined,
    },
  };

  return response;
};

export default getNcep;
