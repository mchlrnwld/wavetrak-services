import chai, { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import config from '../../config';
import createParamString from '../../external/createParamString';
import * as sdsAPI from '../../external/scienceDataServiceForecast';
import * as regionAPI from '../../helpers/getNearbyRegionData';
import userUnits from '../middleware/userUnits';
import entitlements from '../middleware/entitlements';
import waterways from '../middleware/waterways';
import userWindWaveModel from '../middleware/userWindWaveModel';
import checkForecastParams from '../middleware/checkForecastParams';
import timezone from '../middleware/timezone';
import getForecastsHandler from './getForecastsHandler';
import regionStub from './fixtures/region';
import swellsStub from './fixtures/sdsSwells';
import weatherStub from './fixtures/sdsWeather';

const getForecastsHandlerSpec = () => {
  describe('/', () => {
    let clock;
    let request;
    // const start = 1493622000; // A midnight time in UTC-7.
    const start = 1493708400; // A midnight time in UTC-7.
    const params = {
      lat: 28.815799886487298,
      lon: -115.99365234375,
      days: 1,
      interval: 360,
    };

    before(() => {
      const app = express();
      app.use(
        userUnits,
        entitlements,
        userWindWaveModel,
        checkForecastParams,
        timezone,
        waterways,
        getForecastsHandler,
      );
      request = chai.request(app);
      clock = sinon.useFakeTimers((start + 3600) * 1000); // 1AM local
      sinon.stub(regionAPI, 'default').returns(regionStub);
    });

    beforeEach(() => {
      sinon.stub(sdsAPI, 'getWeatherForecast');
      sinon.stub(sdsAPI, 'getSwellForecast');
    });

    afterEach(() => {
      sdsAPI.getWeatherForecast.restore();
      sdsAPI.getSwellForecast.restore();
    });

    after(() => {
      clock.restore();
      regionAPI.default.restore();
    });

    it('returns forecast data', async () => {
      sdsAPI.getWeatherForecast.returns(weatherStub([start, start + (6 * 3600)]));
      sdsAPI.getSwellForecast.returns(swellsStub([start, start + (6 * 3600)]));

      const url = `/?${createParamString({ model: 'LOTUS', ...params })}`;
      const res = await request.get(url).send();
      expect(res).to.have.status(200);
      expect(res.body).to.be.ok();
      expect(res.body.associated).to.be.ok();
      expect(res.body.associated.units).to.be.ok();
      expect(res.body.associated.units).to.be.deep.equal({
        temperature: 'c',
        tide: 'm',
        wave: { height: 'ft' },
        wind: 'kts',
        pressure: 'mb',
        visibility: 'm',
        precipitation: { volume: 'mm' },
      });
      expect(res.body.associated.timezone).to.be.an('array');
      expect(res.body.associated.timezone[0]).to.be.an('object').that.has.all.keys('id', 'path', 'abbr', 'key', 'offset');
      expect(res.body.associated.location).to.deep.equal({
        lat: 28.815799886487298,
        lon: -115.99365234375,
      });
      expect(res.body.associated.region).to.deep.equal(regionStub);
      expect(res.body.associated.urls).to.deep.equal({
        dailyEmail: `${config.BUOYWEATHER_API_V1}/wxnav5.jsp?program=dailyAddB&grb=enp&region=BA&latitude=28.8&longitude=-115.9&reqLatitude=28.8&reqLongitude=-115.9&zone=-7&units=m`,
        dailyEmailAlert: `${config.BUOYWEATHER_API_V1}/wxnav5.jsp?program=alertAdd1B&grb=enp&region=BA&latitude=28.8&longitude=-115.9&reqLatitude=28.8&reqLongitude=-115.9&zone=-7&units=m`,
        embed: `${config.BUOYWEATHER_API_V1}/widgetAdd.jsp?program=nww3BW1&grb=enp&region=BA&latitude=28.8&longitude=-115.9&reqLatitude=28.8&reqLongitude=-115.9&zone=-7&units=m&lat=28.815799886487298&lon=-115.99365234375`,
        optOut: `${config.BUOYWEATHER_API_V1}/wxnav5-fcstLanding.jsp?program=nww3BW1&grb=enp&region=BA&latitude=28.8&longitude=-115.9&reqLatitude=28.8&reqLongitude=-115.9&zone=-7&units=m`,
        print: `${config.BUOYWEATHER_API_V1}/wxnav6.jsp?program=nww3BW1&grb=enp&region=BA&latitude=28.8&longitude=-115.9&reqLatitude=28.8&reqLongitude=-115.9&zone=-7&units=m&print=true`,
      });
      expect(res.body.data).to.be.ok();

      // Forecast structure
      expect(res.body.data.forecasts).to.be.ok();
      expect(res.body.data.forecasts.length).to.equal(8);
      const row = res.body.data.forecasts[0];
      expect(row.timestamp).to.be.a('number');
      expect(row.weather.temperature).to.be.a('number');
      expect(row.weather.pressure).to.be.a('number');
      expect(row.weather.condition).to.be.a('string');
      expect(row.weather.precipitation.volume).to.be.a('number');
      expect(row.weather.precipitation.type).to.be.a('string');
      expect(row.wind.speed.average).to.be.a('number');
      expect(row.wind.speed.peak).to.be.a('number');
      expect(row.wind.speed.peak).to.be.at.least(row.wind.speed.average);
      expect(row.wind.direction.azimuth).to.be.a('number');
      expect(row.wind.direction.cardinal).to.be.a('string');

      expect(row.wave.height.mean).to.be.a('number');
      expect(row.wave.height.peak).to.be.a('number');
      expect(row.wave.direction.azimuth).to.be.a('number');
      expect(row.wave.direction.cardinal).to.be.a('string');
      expect(row.wave.period).to.be.a('number');
      expect(row.wave.offshoreCoordinate.lat).to.be.a('number');
      expect(row.wave.offshoreCoordinate.lon).to.be.a('number');


      // Expected calls in to the get forecast methods
      [
        sdsAPI.getWeatherForecast.firstCall.args[0],
        sdsAPI.getSwellForecast.firstCall.args[0],
      ].forEach((args) => {
        expect(args.lat).to.equal(28.815799886487298);
        expect(args.lon).to.equal(-115.99365234375);
        expect(args.start).to.equal(start);
        expect(args.end).to.equal(start + (2 * 60 * 60 * 24));
        expect(args.interval).to.equal(360 * 60);
      });
    });
  });
};

export default getForecastsHandlerSpec;
