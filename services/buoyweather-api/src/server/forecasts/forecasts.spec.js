import getForecastsHandlerSpec from './getForecastsHandler.spec';
import allowableForecastRangeSpec from '../../helpers/allowableForecastRange.spec';

describe('/forecasts', () => {
  getForecastsHandlerSpec();

  describe('helpers', () => {
    allowableForecastRangeSpec();
  });
});
