import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import waterways from '../middleware/waterways';
import getForecastsHandler from './getForecastsHandler';
import getLegacyForecastsHandler from './legacy/getLegacyForecastsHandler';

const forecasts = () => {
  const api = Router();
  api.get('/', waterways, wrapErrors(getForecastsHandler));
  api.get(
    '/legacy',
    waterways,
    (req, _, next) => {
      // eslint-disable-next-line no-param-reassign
      req.legacy = true;
      return next();
    },
    wrapErrors(getLegacyForecastsHandler),
  );
  api.get(
    '/written',
    (req, _, next) => {
      // eslint-disable-next-line no-param-reassign
      req.type = 'written';
      return next();
    },
    waterways,
    wrapErrors(getForecastsHandler),
  );
  return api;
};

export default forecasts;
