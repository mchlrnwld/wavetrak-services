import getLotus from './models/getLotus';
import getNcep from './models/getNcep';

const getForecastsHandler = async (req, res) => {
  if (req.bwWindWaveModel === 'NCEP') {
    return res.send(await getNcep(req));
  }
  return res.send(await getLotus(req));
};

export default getForecastsHandler;
