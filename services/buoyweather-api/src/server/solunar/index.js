import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import { trackSolunarRequests, getSolunarHandler } from './solunar';

const solunar = (log) => {
  const api = Router();

  api.use('*', trackSolunarRequests(log));
  api.get('/', wrapErrors(getSolunarHandler));

  return api;
};

export default solunar;
