import getSolunar from '../../helpers/solunar';
import {
  toUnixTimestamp,
  midnightOfDate,
  timezoneData,
  getTimezoneArray,
} from '../../utils/datetime';

export const trackSolunarRequests = log => (req, res, next) => {
  log.trace({
    action: '/buoyweather-api/solunar',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export const getSolunarHandler = async (req, res) => {
  const { user } = req;
  const { premium } = user;
  const lat = Number(req.query.lat);
  const lon = Number(req.query.lon);
  const days = premium ? 16 : 2;
  const utcOffset = timezoneData(lat, lon).offset;
  const start = toUnixTimestamp(midnightOfDate(utcOffset, new Date()));
  const end = start + (days * 60 * 60 * 24);
  const timezone = getTimezoneArray(lat, lon, start, end);

  if (isNaN(lat) || isNaN(lon)) {
    return res.status(400).send({
      message: 'valid lat/lon parameters are required',
    });
  }

  const data = {};
  data.solunar = await getSolunar({ lat, lon, days, timezoneArray: timezone });

  const associated = {
    timezone,
    location: {
      lat,
      lon,
    },
  };
  return res.send({ associated, data });
};
