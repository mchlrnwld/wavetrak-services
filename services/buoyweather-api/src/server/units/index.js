import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import userUnits from '../middleware/userUnits';
import getEntitlements from '../middleware/entitlements';
import { trackUnitsRequests, getUnitsHandler } from './units';

const units = (log) => {
  const api = Router();

  api.use('*', trackUnitsRequests(log));
  api.get('/', getEntitlements, userUnits, wrapErrors(getUnitsHandler));

  return api;
};

export default units;
