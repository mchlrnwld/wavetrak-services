export const trackUnitsRequests = log => (req, res, next) => {
  log.trace({
    action: '/buoyweather-api/units',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export const getUnitsHandler = async (req, res) => res.send(req.units);
