import errors from '../../utils/errors';

const saveFavoritesHandler = async (req, res) => {
  const favorites = req.favoritesModel;
  const favoritesForUpdate = req.body.favorites;
  // eslint-disable-next-line
  for (const favorite of favoritesForUpdate) {
    const theFavorite = favorites.favorites.id(favorite._id);
    if (theFavorite) {
      Object.assign(theFavorite, favorite);
    } else {
      throw new errors.APIError('A favorite trying to be saved does not exist.');
    }
  }
  await favorites.save();
  return res.status(200).send({ message: 'OK' });
};

export default saveFavoritesHandler;
