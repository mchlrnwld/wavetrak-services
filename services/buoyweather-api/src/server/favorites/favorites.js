import { Router } from 'express';
import bodyParser from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import customAuthenticateRequest from '../middleware/customAuthenticateRequest';
import { getLegacyUserId } from '../../external/user';

import findOrCreateFavoriteHandler from './findOrCreateFavoriteHandler';
import getFavoritesHandler from './getFavoritesHandler';
import createFavoriteHandler from './createFavoriteHandler';
import saveFavoritesHandler from './saveFavoritesHandler';
import deleteFavoritesHandler from './deleteFavoritesHandler';
import getFavoriteHandler from './getFavoriteHandler';
import saveFavoriteHandler from './saveFavoriteHandler';
import deleteFavoriteHandler from './deleteFavoriteHandler';
import errorHandler from './errorHandler';

export default () => {
  const api = Router();
  api.use(bodyParser.json());
  api.use(async (req, _, next) => {
    if (req.authenticatedUserId) {
      const userId = req.authenticatedUserId;
      const legacyUserId = await getLegacyUserId(userId);

      /* eslint-disable no-param-reassign */
      req.userId = (legacyUserId !== null) ? legacyUserId : userId;
    }
    next();
  });
  api.use(customAuthenticateRequest());
  api.all('*', findOrCreateFavoriteHandler);
  api.route('/')
    .get(wrapErrors((req, res) => getFavoritesHandler(req, res)))
    .post(wrapErrors((req, res) => createFavoriteHandler(req, res)))
    .put(wrapErrors((req, res) => saveFavoritesHandler(req, res)))
    .delete(wrapErrors((req, res) => deleteFavoritesHandler(req, res)));

  api.route('/:resource')
    .get(wrapErrors((req, res) => getFavoriteHandler(req, res)))
    .put(wrapErrors((req, res) => saveFavoriteHandler(req, res)))
    .delete(wrapErrors((req, res) => deleteFavoriteHandler(req, res)));
  api.use(errorHandler);
  return api;
};
