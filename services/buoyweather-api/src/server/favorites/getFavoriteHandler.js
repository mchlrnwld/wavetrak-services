import errors from '../../utils/errors';

const getFavoriteHandler = async (req, res) => {
  const favorites = req.favoritesModel;
  const favorite = await favorites.favorites.id(req.params.resource);
  if (favorite) {
    return res.status(200).send(favorite);
  }
  throw new errors.ResourceNotFound(req.params.resource);
};

export default getFavoriteHandler;
