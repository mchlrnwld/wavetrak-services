
import React from 'react'; // eslint-disable-line no-unused-vars
import ReactDOMServer from 'react-dom/server';
import coordinates from '../../utils/coordinates';
import FavoritesList from '../../components/FavoritesList'; // eslint-disable-line no-unused-vars
import FavoritesLegacyList from '../../components/FavoritesLegacyList'; // eslint-disable-line no-unused-vars

const getFavoritesHandler = async (req, res) => {
  const favorites = req.favoritesModel.orderedFavorites();
  let favoritesList = favorites.favorites.filter((favorite) => {
    if (favorite.legacyUrl && favorite.legacyUrl.match(/\/mobile\//)) {
      if (favorite.product === 'marine_weather') {
        favorite.legacyUrl = ''; // eslint-disable-line
        return favorite;
      }
    }
    return favorite;
  });

  if (req.query.tpl === 'links') {
    return res
      .status(200)
      .type('.html')
      .send(ReactDOMServer.renderToString(<FavoritesLegacyList favorites={favoritesList} />));
  } else if (req.query.tpl === 'linkslist') {
    return res
      .status(200)
      .type('.html')
      .send(ReactDOMServer.renderToString(<FavoritesList favorites={favoritesList} />));
  }

  const { lat, lon, product, query } = req.query;
  if (lat && lon && product && query) {
    if (query === 'nearest') {
      const coordinatesLonLat = [lon, lat];
      favoritesList = favoritesList.filter((favorite) => {
        if (coordinates.match(coordinatesLonLat, favorite.point)
          && favorite.product === product) {
          return favorite;
        }
        return null;
      });
    }
  }

  favorites.favorites = favoritesList;
  return res
    .status(200)
    .send(favorites);
};

export default getFavoritesHandler;
