import UserFavorites from '../../model/favorite';

const findOrCreateFavorite = async (req, res, next) => {
  const { userId } = req;
  let favorites = await UserFavorites.findOne({ userId });
  if (!favorites) {
    favorites = await UserFavorites
      .create({ userId, favorites: [] });
  }
  req.favoritesModel = favorites; // eslint-disable-line
  next();
};

export default findOrCreateFavorite;
