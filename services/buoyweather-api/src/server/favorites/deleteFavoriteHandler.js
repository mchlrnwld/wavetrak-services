const deleteFavoriteHandler = async (req, res) => {
  const { resource } = req.params;
  const favorite = req.favoritesModel;
  favorite.favorites.pull(resource);
  await favorite.save();
  return res.status(200).send({ message: 'OK' });
};

export default deleteFavoriteHandler;
