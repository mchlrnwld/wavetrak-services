import checkParams from '../../utils/checkParams';

const createFavoriteHandler = async (req, res) => {
  const conflicts = checkParams(req.body, req.favoritesModel.favorites);
  if (conflicts.length > 0) {
    return res.status(409).send({
      message: 'Favorite conflict',
      conflicts,
    });
  }
  const favorites = req.favoritesModel;
  favorites.favorites.push(req.body);
  const result = await favorites.save();
  const newFavorite = result && result.favorites && result.favorites.length > 0 ?
    result.favorites[result.favorites.length - 1] : req.body;
  return res
    .status(200)
    .send(newFavorite);
};

export default createFavoriteHandler;
