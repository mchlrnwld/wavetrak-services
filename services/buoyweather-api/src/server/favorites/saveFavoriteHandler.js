import errors from '../../utils/errors';

const saveFavoriteHandler = async (req, res) => {
  const favorites = req.favoritesModel;
  const { resource } = req.params;
  const favoriteBody = req.body;
  const favorite = favorites.favorites.id(resource);
  if (favorite) {
    Object.assign(favorite, favoriteBody);
  } else {
    throw new errors.ResourceNotFound(resource);
  }
  await favorites.save();
  return res.status(200).send({ message: 'OK' });
};

export default saveFavoriteHandler;
