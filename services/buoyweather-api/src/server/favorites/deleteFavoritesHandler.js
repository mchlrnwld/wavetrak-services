import errors from '../../utils/errors';

const deleteFavoritesHandler = async (req, res) => {
  const favorites = req.favoritesModel;
  const resources = req.body.favorites;

  // eslint-disable-next-line
  for (const resource of resources) {
    // TODO confirm that this will get converted to a $pullAll query
    const favorite = favorites.favorites.id(resource);
    if (favorite) {
      favorite.remove();
    } else {
      throw new errors.APIError('A resource was not found.');
    }
  }
  await favorites.save();
  return res.status(200).send({ message: 'OK' });
};

export default deleteFavoritesHandler;
