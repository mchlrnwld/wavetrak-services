const errorHandler = async (err, req, res) => {
  const statusCode = err.statusCode || 500;
  const resp = typeof err.toJSON === 'function' ? err.toJSON() :
    { message: 'Something went wrong.' };
  return res.status(statusCode).json(resp);
};

export default errorHandler;
