import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import deleteUserFavorites from './favorites';

const admin = () => {
  const api = Router();
  api.delete('/userfavorites/:userId', wrapErrors(deleteUserFavorites));
  return api;
};

export default admin;
