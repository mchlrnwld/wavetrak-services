import UserFavorites from '../../model/favorite';

/**
 * @description Admin endpoint for handling GDPR requests.
 * This endpoint deletes the favorited spots for a user from Favorites collection
 *
*/
const deleteUserFavorites = async (req, res) => {
  const { userId } = req.params;
  if (!userId) {
    return res.status(400).send({ message: 'You need to specify an id' });
  }
  try {
    await UserFavorites.deleteMany({ userId });
    return res.send({ message: 'Favorites deleted successfully' });
  } catch (error) {
    return res.status(500).send({
      message: 'Could not delete Buoyweather user favorites',
      error,
    });
  }
};

export default deleteUserFavorites;
