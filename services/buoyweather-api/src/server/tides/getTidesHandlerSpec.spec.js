import { expect } from 'chai';
import sinon from 'sinon';
import express from 'express';
import utcOffsetFromTimezone from '../../utils/utcOffsetFromTimezone';
import * as tidesApi from '../../external/tides';
import userUnits from '../middleware/userUnits';
import entitlements from '../middleware/entitlements';
import timezone from '../middleware/timezone';
import tides from './';

const getTidesHandlerSpec = () => {
  describe('/tides', () => {
    let clock;
    const start = 1493701200;
    const params = {
      lat: 6.991859181483692,
      lon: -79.4970703125,
    };

    before(() => {
      const app = express();
      app.use(userUnits, entitlements, timezone, tides());
      clock = sinon.useFakeTimers(start * 1000);
    });

    beforeEach(() => {
      sinon.stub(tidesApi, 'getTides');
    });

    afterEach(() => {
      tidesApi.getTides.restore();
    });

    after(() => {
      clock.restore();
    });

    it('returns tides forecast data', async () => {
      tidesApi.getTides.returns({
        associated: {
          timezone: {
            key: 'Etc/Greenwich',
            abbr: 'GMT',
            offset: 0,
          },
          location: {
            lat: 6.991859181483692,
            lon: -79.4970703125,
          },
          units: {
            temperature: 'c',
            tide: 'm',
            wave: {
              height: 'm',
            },
            wind: 'kts',
            pressure: 'mb',
            visibility: 'km',
            precipitation: {
              volume: 'mm',
            },
          },
        },
        data: {
          tides: [],
        },
      });

      const utcOffset = utcOffsetFromTimezone('Etc/Greenwich');
      const tidesResponse = tidesApi.getTides(params);
      expect(tidesResponse.associated.timezone.offset).to.equal(utcOffset);
      expect(tidesResponse.associated.timezone.key).to.equal('Etc/Greenwich');
      expect(tidesResponse.associated.location).to.deep.equal({
        lat: 6.991859181483692,
        lon: -79.4970703125,
      });
      expect(tidesResponse.data.tides).to.be.an('array');

      const getTidesForecastsArgs = tidesApi.getTides.firstCall.args[0];
      expect(getTidesForecastsArgs.lat).to.equal(6.991859181483692);
      expect(getTidesForecastsArgs.lon).to.equal(-79.4970703125);
    });
  });
};

export default getTidesHandlerSpec;
