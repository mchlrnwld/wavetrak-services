import { getTides } from '../../external/tides';
import {
  toUnixTimestamp,
  midnightOfDate,
  timezoneData,
  getTimezoneArray,
} from '../../utils/datetime';

export const trackTidesRequests = log => (req, res, next) => {
  log.trace({
    action: '/buoyweather-api/tides',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export const getTidesHandler = async (req, res) => {
  const { user } = req;
  const { premium } = user;
  const lat = Number(req.query.lat);
  const lon = Number(req.query.lon);
  const days = premium ? 16 : 2;
  if (isNaN(lat) || isNaN(lon)) {
    return res.status(400).send({
      message: 'valid lat/lon parameters are required',
    });
  }
  const utcOffset = timezoneData(lat, lon).offset;
  const start = toUnixTimestamp(midnightOfDate(utcOffset, new Date()));
  const end = start + (days * 60 * 60 * 24);
  const timezone = getTimezoneArray(lat, lon, start, end);

  const associated = {
    timezone,
    units: req.units,
    location: {
      lat,
      lon,
    },
  };

  const tidesData = await getTides({
    lat,
    lon,
    days,
    units: req.units,
    timezoneArray: timezone,
  });

  const data = {};
  data.tides = tidesData.filter(tideData => tideData !== null);
  return res.send({ associated, data });
};
