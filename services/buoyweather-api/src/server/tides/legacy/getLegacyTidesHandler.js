import { format } from 'date-fns';
import moment from 'moment';
import { getTides } from '../../../external/tides';
import {
  toUnixTimestamp,
  midnightOfDate,
  timezoneData,
  getTimezoneArray,
  getLocalDate,
} from '../../../utils/datetime';
import config from '../../../config';

const getLegacyTidesHandler = async (req, res) => {
  const { user } = req;
  const { type } = user;
  const lat = Number(req.query.lat);
  const lon = Number(req.query.lon);
  // const days = premium ? 16 : 2;
  const days = 7;
  if (isNaN(lat) || isNaN(lon)) {
    return res.status(400).send({
      message: 'valid lat/lon parameters are required',
    });
  }
  const utcOffset = timezoneData(lat, lon).offset;
  const start = toUnixTimestamp(midnightOfDate(utcOffset, new Date()));
  const end = start + (days * 60 * 60 * 24);
  const timezone = getTimezoneArray(lat, lon, start, end);

  const associated = {
    timezone,
    units: req.units,
    location: {
      lat,
      lon,
    },
  };

  const tidesData = await getTides({
    lat,
    lon,
    days,
    units: req.units,
    timezoneArray: timezone,
    legacy: true,
  });

  let data = {};
  data.tides = tidesData.filter(tideData => tideData !== null);
  let minMax = [0, 0];
  data = data.tides
    .map((tide, index) => {
      minMax[0] = tide.height < minMax[0] || minMax[0] === 0 ? tide.height : minMax[0];
      minMax[1] = tide.height > minMax[1] ? tide.height : minMax[1];
      const offset = associated.timezone.filter(zone =>
        zone.id === tide.timezone.id)[0].offset;
      const localDate = getLocalDate(tide.timestamp, offset);
      const utcDate = new moment(tide.timestamp * 1000).utc().format('YYYY-MM-DD HH:mm:ss');
      const utcString = `${utcDate} Z`;
      return {
        id: index,
        value: tide.height,
        type: tide.analysis,
        timestamp: tide.timestamp,
        monthDay: format(localDate, 'M/D'),
        formattedTime: format(localDate, 'h:mmA'),
        localTimeString: format(localDate, 'MMMM DD, YYYY HH:mm:ss'),
        utcString,
      };
    });
  const minMaxRegulatorValue = (associated.units.tide === 'm') ? 4 : 6;
  // Smallest height can't be greater than -4
  minMax[0] = (minMax[0] > -minMaxRegulatorValue) ? -minMaxRegulatorValue : minMax[0];
  // Greatest height can't be less than 4
  minMax[1] = (minMax[1] < minMaxRegulatorValue) ? minMaxRegulatorValue : minMax[1];
  const max = minMax[1];
  let tideScale = [];
  switch (true) {
    case (max === 6):
      tideScale = config.pages.forecast.marine_weather.tideScale.six;
      break;
    case (max === 8):
      tideScale = config.pages.forecast.marine_weather.tideScale.eight;
      break;
    case (max > 8 && max <= 12):
      minMax = [-12, 12];
      tideScale = config.pages.forecast.marine_weather.tideScale.twelve;
      break;
    case (max > 12 && max <= 16):
      minMax = [-16, 16];
      tideScale = config.pages.forecast.marine_weather.tideScale.sixteen;
      break;
    case (max > 16 && max <= 20):
      minMax = [-20, 20];
      tideScale = config.pages.forecast.marine_weather.tideScale.twenty;
      break;
    case (max > 20 && max <= 32):
      minMax = [-32, 32];
      tideScale = config.pages.forecast.marine_weather.tideScale.thirtytwo;
      break;
    default:
      tideScale = config.pages.forecast.marine_weather.tideScale.four;
      break;
  }
  return res.send({
    tides: {
      data,
      minMax,
      tideScale,
      units: associated.units.tide !== 'm' ? 'e' : associated.units.tide,
      tz: associated.timezone[0].offset,
      numberOfFreeDays: 2,
      userType: type,
    },
  });
};

export default getLegacyTidesHandler;
