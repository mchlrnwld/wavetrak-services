import { Router } from 'express';
import { wrapErrors } from '@surfline/services-common';
import { trackTidesRequests, getTidesHandler } from './tides';
import getLegacyTidesHandler from './legacy/getLegacyTidesHandler';

const tides = (log) => {
  const api = Router();

  api.use('*', trackTidesRequests(log));
  api.get('/', wrapErrors(getTidesHandler));
  api.get('/legacy', wrapErrors(getLegacyTidesHandler));

  return api;
};

export default tides;
