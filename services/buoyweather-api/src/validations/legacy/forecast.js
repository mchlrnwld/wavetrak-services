import dateformat from 'dateformat';
import Joi from 'joi';

const dateNow = () => {
  const now = new Date();
  return dateformat(now, 'mmmm dd, yyyy HH:mm:ss');
};

export const timeStepControl = Joi.object().keys({
  date: {
    // local: Joi.date().format('MMMM dd, yyyy hh:mm:ss'),
    // utc: Joi.date().format('MMMM dd, yyyy hh:mm:ss'),
    local: Joi.string().min(1),
    utc: Joi.string().min(1),
  },
});

export const windStepControl = Joi.object().keys({
  direction: {
    cardinal: Joi.string(),
    iconPath: Joi.string(),
    azimuth: Joi.number(),
  },
  speed: {
    current: Joi.number(),
    average: Joi.number(),
    peak: Joi.number(),
  },
});

export const waveStepControl = Joi.object().keys({
  period: Joi.number(),
  direction: {
    cardinal: Joi.string(),
    iconPath: Joi.string(),
    azimuth: Joi.number(),
  },
  height: {
    peak: Joi.number(),
    average: Joi.number(),
  },
});

export const timeStepDefault = {
  date: {
    local: dateNow(),
    utc: dateNow(),
  },
};

export const windStepDefault = {
  direction: {
    cardinal: '',
    iconPath: '',
    azimuth: 0.0,
  },
  speed: {
    current: 0.0,
    average: 0.0,
    peak: 0.0,
  },
};

export const waveStepDefault = {
  period: 0.0,
  direction: {
    cardinal: '',
    iconPath: '',
    azimuth: 0.0,
  },
  height: {
    peak: 0.0,
    average: 0.0,
  },
};
