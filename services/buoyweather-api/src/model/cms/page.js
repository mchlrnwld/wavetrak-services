import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const Page = new Schema({
  id: Number,
  type: String,
  slug: String,
  title: String,
  created_at: Date,
  updated_at: Date,
  content: {
    body: String,
  },
  seo: {
    focus_keywords: String,
    seo_title: String,
    meta_description: String,
  },
  posts: Array,
});

module.exports = mongoose.model('pages', Page);
