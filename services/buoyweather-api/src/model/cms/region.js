import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const Region = new Schema({
  id: Number,
  title: String,
  slug: String,
  location: {
    index: '2dsphere',
    type: [Number],
  },
  region_model_grb: String,
  region_short_code: String,
  time_zone: Number,
  units: String,
  zoom: Number,
  seo: {
    focus_keywords: String,
    seo_title: String,
    meta_description: String,
    facebook_title: String,
    facebook_description: String,
    facebook_image: String,
  },
});

module.exports = mongoose.model('bw_regions', Region);
