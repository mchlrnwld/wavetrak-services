import mongoose, { Schema } from 'mongoose';
import productKVP from './products';

const timeStamps = {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
};

const products = Object.keys(productKVP);

const Favorite = new Schema({
  name: String,
  point: {
    type: [Number],
    index: '2d',
  },
  product: { type: String, enum: products },
  rank: { type: Number, default: 0 },
  legacyUrl: String,
}, timeStamps);

const FavoritesSchema = new Schema({
  userId: Schema.Types.Mixed,
  favorites: [Favorite],
  updated_at: Date,
}, { usePushEach: true, timeStamps });

/**
 * Returns a user's favorites ordered by rank and secondarily on `created_at`.
 */
FavoritesSchema.methods.orderedFavorites = function () {
  this.favorites = this.favorites.sort((prev, next) => {
    if (prev.rank > next.rank) return 1;
    if (prev.rank < next.rank) return -1;
    if (prev.rank === next.rank) {
      if (prev.created_at > next.created_at) return -1;
      if (prev.created_at < next.created_at) return 1;
    }
    return 0;
  });

  return this;
};

const UserFavorites = mongoose.model('Favorite', FavoritesSchema);

export { Favorite };
export default UserFavorites;
