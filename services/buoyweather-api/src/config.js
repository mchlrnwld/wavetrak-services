const config = {
  WWW_HOST: process.env.WWW_HOST,
  MARINE_API: process.env.MARINE_API,
  USER_API: process.env.USER_SERVICE,
  GEO_TARGET_SERVICE: process.env.GEO_TARGET_SERVICE,
  ENTITLEMENTS_API: process.env.ENTITLEMENTS_SERVICE,
  SCIENCE_DATA_SERVICE: process.env.SCIENCE_DATA_SERVICE,
  MARINE_TIDES_API: process.env.MARINE_TIDES_API,
  BUOYWEATHER_API_V1: process.env.BUOYWEATHER_API_V1,
  LEGACY_API_HOST: process.env.LEGACY_API_HOST,
  BW_ICON_CDN: process.env.BW_ICON_CDN || 'http://i.cdn-surfline.com',
  pages: {
    forecast: {
      marine_weather: {
        paywall: {
          wind_wave_tide: {
            free_days: 2,
          },
        },
        tideScale: {
          four: [3, 2, 1, 0, -1, -2, -3],
          six: [4, 2, 0, -2, -4],
          eight: [6, 4, 2, 0, -2, -4, -6],
          twelve: [9, 6, 3, 0, -3, -6, -9],
          sixteen: [12, 8, 4, 0, -4, -8, -12],
          twenty: [15, 10, 5, 0, -5, -10, -15],
          thirtytwo: [24, 16, 8, 0, -8, -16, -24],
        },
      },
    },
  },
};

export default config;
