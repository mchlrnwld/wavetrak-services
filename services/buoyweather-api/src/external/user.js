import fetch from 'node-fetch';
import newrelic from 'newrelic';
import createParamString from './createParamString';
import config from '../config';

export const getLegacyUserId = async (id) => {
  const params = { id };
  const url = `${config.USER_API}/user?${createParamString(params)}`;
  let response;
  let body;

  try {
    response = await fetch(url);
    body = await response.json();

    if (response.status === 200) {
      const legacyUserId = body.usID ? body.usID : null;
      return legacyUserId;
    }
  } catch (error) {
    newrelic.noticeError(error, {
      message: `Request failed fetching legacy user ID for user with ID ${id}`,
      status: response.status,
      url,
      functionName: 'getLegacyUserId',
      filename: __filename,
    });
  }

  throw body;
};

export const getUnitsByLocation = async (geoCountryIso) => {
  const params = { geoCountryIso };
  const url = `${config.USER_API}/user/settings?${createParamString(params)}`;
  let response = await fetch(url);
  let body = await response.json();

  try {
    response = await fetch(url);
    body = await response.json();
    if (response.status === 200) return body;
  } catch (error) {
    newrelic.noticeError(error, {
      message: `Request failed fetching units for location: ${geoCountryIso}`,
      status: response.status,
      url,
      functionName: 'getUnitsByLocation',
      filename: __filename,
    });
  }

  throw body;
};

export const getLegacyUnits = async (userId) => {
  const id = await getLegacyUserId(userId);
  const params = { id };
  const url = `http://www.buoyweather.com/favoritesapi.jsp?${createParamString(params)}`;
  let response;
  let body;

  try {
    response = await fetch(url);
    body = await response.json();

    if (response.status === 200) {
      const legacyUnits = body.units ? body.units.trim() : null;
      return legacyUnits;
    }
  } catch (error) {
    newrelic.noticeError(error, {
      message: `Request failed fetching legacy units for user with ID: ${userId}`,
      status: response.status,
      url,
      functionName: 'getLegacyUnits',
      filename: __filename,
    });
  }

  throw body;
};
