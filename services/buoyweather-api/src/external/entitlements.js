import fetch from 'node-fetch';
import newrelic from 'newrelic';
import createParamString from './createParamString';
import config from '../config';

const getEntitlements = async (userId) => {
  const params = { objectId: userId };
  const url = `${config.ENTITLEMENTS_API}/entitlements?${createParamString(params)}`;
  let response;
  let body;

  try {
    response = await fetch(url);
    body = await response.json();

    if (response.status === 200) return body;
    if (response.status === 400) return { entitlements: [] };
  } catch (error) {
    newrelic.noticeError(error, {
      message: `Request failed fetching entitlements for user with id ${userId}`,
      status: response.status,
      url,
      functionName: 'getEntitlements',
      fileName: __filename,
    });
  }

  throw body;
};


export const isPremium = async (userId) => {
  if (!userId) return false;
  const entitlementsResponse = await getEntitlements(userId);
  const { entitlements } = entitlementsResponse;
  return entitlements.includes('bw_premium') || entitlements.includes('fs_premium');
};

export default getEntitlements;
