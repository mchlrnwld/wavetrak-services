import fetch from 'node-fetch';
import newrelic from 'newrelic';
import {
  midnightOfDate,
  timezoneData,
  getTimezoneRefByOffset,
  formatOffsetStringISO,
} from '../utils/datetime';
import createParamString from './createParamString';
import config from '../config';

const convertTideData = async (tideApiData, lat, lon, startDate, timezoneArray, legacy = false) => {
  const tideData = tideApiData.trim().split('\n');
  const tideDataConverted =
    tideData.length > 1 ? tideData.map((tidestring) => {
      const tideArr = tidestring.split(',');
      const timeFmtUTC = new Date(`${tideArr[0]}T${tideArr[1]}Z`);
      const timezoneOffset = timezoneData(lat, lon, timeFmtUTC).offset;
      const timeFmt = new Date(`${tideArr[0]}T${tideArr[1]}${formatOffsetStringISO(timezoneOffset)}`);
      // tides api does not accept hours, check if earlier elements are valid
      const validRange = legacy ? true : timeFmt.getTime() - startDate.getTime() >= 0;
      const timestamp = timeFmt.getTime() / 1000;

      const minutes = timeFmt.getMinutes();
      const height = Number(tideArr[2]);
      let analysis = 'NORMAL';
      const lowString = legacy ? 'Low' : 'LOW';
      const highString = legacy ? 'High' : 'HIGH';

      if (tideArr[4]) {
        analysis = tideArr[4] === 'Low' ? lowString : highString;
      }

      const criteria = legacy ? true : (minutes === 0 || analysis !== 'NORMAL');
      return validRange && criteria ? {
        height,
        timezone: getTimezoneRefByOffset(timezoneArray, timezoneOffset),
        analysis,
        timestamp,
      } : null;
    }) : [];

  return tideDataConverted;
};

// eslint-disable-next-line import/prefer-default-export
export const getTides = async ({ lat, lon, days, units, timezoneArray, legacy }) => {
  const utcDate = new Date();
  utcDate.getUTCDate();
  const { offset } = timezoneData(lat, lon, utcDate);
  const startDate = midnightOfDate(offset, utcDate);
  const startday = startDate.getDate();
  const startmonth = startDate.getMonth() + 1;
  const startyear = startDate.getFullYear();
  const params = {
    lat,
    lon,
    days,
    startday,
    startmonth,
    startyear,
    tz: offset,
    units: units.tide,
  };
  const url = `${config.MARINE_TIDES_API}/cgi-bin/lltide.pl?detail=1&mode=n&showsun=0&showtide=1&csv=1&${createParamString(params)}`;
  let tideArray;
  let response;

  try {
    response = await fetch(url);
    const tideApiData = await response.text();
    tideArray = await convertTideData(tideApiData, lat, lon, startDate, timezoneArray, legacy);

    if (response.status === 200) return tideArray;
  } catch (error) {
    newrelic.noticeError(error, {
      message: `Request failed fetching tides for coordinates: ${lat},${lon}`,
      status: response.status,
      url,
      lat,
      lon,
      days,
      startday,
      startmonth,
      startyear,
      tz: offset,
      units: units.tide,
      functionName: 'getTides',
      filename: __filename,
    });
  }

  throw tideArray;
};
