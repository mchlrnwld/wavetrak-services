import newrelic from 'newrelic';
import fetch from 'node-fetch';
import createParamString from './createParamString';
import config from '../config';
import { isJsonResponse, nonJsonErrorMsg } from '../utils/responseValidation';

const WEATHER_MODEL = {
  agency: 'NOAA',
  model: 'GFS',
  grid: '0p25',
};

const SWELL_MODEL = {
  agency: 'Wavetrak',
  model: 'Lotus-WW3',
};

const fetchAndCheckError = async (path, params) => {
  const url = `${config.SCIENCE_DATA_SERVICE}${path}?${createParamString(params)}`;
  let response;
  try {
    response = await fetch(url);
  } catch (e) {
    newrelic.noticeError(
      {
        message: `Science Data Service ${path} data fetch error "${e.mesage}" (${params.lat}/${params.lon})`,
        stack: e.stack,
      },
      { ...params, url },
    );

    return null;
  }
  const validResponse = isJsonResponse(response);
  const body = validResponse ? await response.json() : await response.text();
  if (validResponse && response.status === 200) {
    return body;
  }

  const errorStatus = response.status > 200 ? response.status : 500;
  const errorBody = validResponse ? JSON.stringify(body) : nonJsonErrorMsg(response);
  const errMsgAdd = !validResponse ? 'Invalid JSON' : '';
  const errMsg = `Science Data Service ${path} bad response "${errMsgAdd}" (${params.lat}/${params.lon})`;
  newrelic.noticeError(
    { message: errMsg, stack: errorBody, statusCode: errorStatus },
    { ...params, url },
  );
  return null;
};

/**
 * Fetch the weather forecast from the science data service for a lat/lon point
 *
 * @param {number} params                                Object of options
 * @param {number} params.lat                            Latitude
 * @param {number} params.lon                            Longitude
 * @param {number} params.start                          Unix timestmp (seconds) for forecast start
 * @param {number} params.end                            Unix timestmp (seconds) for forecast end
 * @param {number} params.interval                       Forecast step size (seconds)
 * @param {number} params.units.temperature             `C` or `F`.
 * @param {number} params.units.precipitation.volume    `mm` or `in`.
 * @param {number} params.units.pressure                `mb`
 * @return {Promise}                                     Weather forecast data, or `null` on error.
 */
export const getWeatherForecast = params => fetchAndCheckError('/forecasts/weather', {
  ...WEATHER_MODEL,
  ...params,
  units: JSON.stringify({
    windSpeed: 'knot',
    temperature: params.units.temperature.toUpperCase(),
    precipitation: params.units.precipitation.volume.toLowerCase(),
    pressure: params.units.pressure.toLowerCase(),
  }),
});


/**
 * Fetch the swell forecast from the science data service for a lat/lon point.
 *
 * @param {number} params                      Object of options
 * @param {number} params.lat                  Latitude
 * @param {number} params.lon                  Longitude
 * @param {number} params.start                Unix timestmp (seconds) for forecast start
 * @param {number} params.end                  Unix timestmp (seconds) for forecast end
 * @param {number} params.interval             Forecast step size (seconds)
 * @param {number} params.units.wave.height   `m` or `ft`.
 * @return {Promise}                           Swell forecast data, or `null` on error.
 */
export const getSwellForecast = (params) => {
  const unit = params.units.wave.height.toLowerCase();
  return fetchAndCheckError('/forecasts/swells', {
    ...SWELL_MODEL,
    ...params,
    units: JSON.stringify({
      waveHeight: unit === 'hi' ? 'ft' : unit,
    }),
  });
};
