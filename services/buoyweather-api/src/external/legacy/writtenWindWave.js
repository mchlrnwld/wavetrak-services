import newrelic from 'newrelic';
import fetch from 'node-fetch';
import createParamString from '../createParamString';
import config from '../../config';

const getWrittenWindWave = async (urlParams) => {
  const { lat, lon, tz, units, grb } = urlParams;
  const params = {
    product: 'nww3',
    lat,
    lon,
    tz,
    units,
    grb,
  };
  const url = `${config.MARINE_API}?${createParamString(params)}`;
  const response = await fetch(url);
  const body = await response.json();
  if (response.status === 200) return body;
  const errMsg = `getWrittenWindWave backend data fetch error (${lat}/${lon})`;
  const customParams = {
    ...params,
    url,
  };
  newrelic.noticeError(
    { message: errMsg, stack: JSON.stringify(body), statusCode: response.status },
    { ...customParams },
  );
  return {
    writtenForecast: null,
    charts: null,
    printUrl: null,
    optOutUrl: null,
  };
};

export default getWrittenWindWave;
