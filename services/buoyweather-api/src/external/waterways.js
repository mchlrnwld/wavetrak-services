import fetch from 'node-fetch';
import newrelic from 'newrelic';
import createParamString from './createParamString';
import config from '../config';

// eslint-disable-next-line import/prefer-default-export
export const getWaterways = async (lat, lon) => {
  const params = { lat, lon };
  const url = `${config.GEO_TARGET_SERVICE}/waterways?${createParamString(params)}`;
  let response;
  let body;

  try {
    response = await fetch(url);
    body = await response.json();

    if (response.status === 200) return body;
    if (response.status === 400) return null;
  } catch (error) {
    newrelic.noticeError(error, {
      message: `Request failed fetching waterways for coordinate: ${lat},${lon}`,
      status: response.status,
      url,
      functionName: 'getWaterways',
      filename: __filename,
    });
  }

  throw body;
};
