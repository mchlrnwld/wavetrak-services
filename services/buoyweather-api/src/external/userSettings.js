import fetch from 'node-fetch';
import newrelic from 'newrelic';
import config from '../config';

const getUserSettings = async (userId) => {
  const url = `${config.USER_API}/user/settings`;
  let response;
  let body;

  try {
    response = await fetch(url, {
      method: 'GET',
      headers: {
        'x-auth-userid': userId,
      },
    });
    body = await response.json();
    return body;
  } catch (error) {
    newrelic.noticeError(error, {
      message: `Request failed fetching user settings for user with ID: ${userId}`,
      status: response.status,
      url,
      functionName: 'getUserSettings',
      filename: __filename,
    });
  }

  throw body;
};

export default getUserSettings;
