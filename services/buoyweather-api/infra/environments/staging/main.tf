provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "buoyweather/buoyweather-api/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

module "buoyweather_api" {
  source = "../../"

  company     = "bw"
  environment = "staging"
  application = "buoyweather-api"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-staging"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678/1b066cae0efbb0a4"
  service_td_count = 3
  service_lb_rules = [
    {
      field = "host-header"
      value = "buoyweather-api.staging.buoyweather.com"
    },
  ]
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  dns_name          = "buoyweather-api.staging.buoyweather.com"
  dns_zone_id       = ""
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:loadbalancer/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678"

  auto_scaling_enabled = false
}
