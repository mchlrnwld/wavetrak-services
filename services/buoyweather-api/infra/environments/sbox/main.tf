provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "buoyweather/buoyweather-api/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}
