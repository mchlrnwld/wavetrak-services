provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-prod"
    key    = "buoyweather/buoyweather-api/prod/terraform.tfstate"
    region = "us-west-1"
  }
}

module "buoyweather_api" {
  source = "../../"

  company     = "bw"
  environment = "prod"
  application = "buoyweather-api"

  default_vpc      = "vpc-116fdb74"
  ecs_cluster      = "sl-core-svc-prod"
  alb_listener_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:listener/app/sl-int-core-srvs-1-prod/9fed27702f8f890f/05e6f65280962f71"
  service_td_count = 3
  service_lb_rules = [
    {
      field = "host-header"
      value = "buoyweather-api.prod.buoyweather.com"
    },
  ]
  iam_role_arn      = "arn:aws:iam::833713747344:role/sl-ecs-service-core-svc-prod"
  dns_name          = "buoyweather-api.prod.buoyweather.com"
  dns_zone_id       = ""
  load_balancer_arn = "arn:aws:elasticloadbalancing:us-west-1:833713747344:loadbalancer/app/sl-int-core-srvs-1-prod/9fed27702f8f890f"

  auto_scaling_enabled      = true
  auto_scaling_scale_by     = "alb_request_count"
  auto_scaling_target_value = 800
  auto_scaling_min_size     = 2
  auto_scaling_max_size     = 5000
  task_deregistration_delay = 60
}
