# buoyweather-api Infrastructure

## Summary

This provisions the new Buoyweather V2 api in ECS for both staging and production
(No current sandbox setup for Buoyweather, but can be added)

## Approach

Standard core service terraform setup, except for the addition of a couple items

The buoyweather.com domain is currently being managed from CloudFlare, so we do not use Route53 to resolve dns.
In `infra/modules/buoyweather-api/service.tf` there is an additional parameter set to false

```bash
provision_route_53          = "false"
```

This tells the Sturdy Terraform that manages the core services cluster updates to ignore setting up provisioning for Route 53 based on the dns_zone_id for each environment.

Also notice that the `service_td_name` is prefixed with `bw-` which differs from Surfline implementations.

In `infra/{staging|prod}/main.tf` the dns_zone_id value is blank, since there is not one setup to service either environment

```bash
dns_zone_id       = ""
```

(see Surfline/bw repo `/webapp` infra folder for similar approach)

## Required / Related Files

`services/buoyweather-api/ci.json`

Values in this file correspond to values set in the ENV/main.tf files in the `/infra/folder`

`docker/buoyweather-api.yml`

Values in this file correspond to values set in the ENV/main.tf files in the `/infra/folder`

## Naming Conventions

See https://wavetrak.atlassian.net/wiki/display/MGSVCS/AWS+Naming+Conventions.

## Terraform

_Uses terraform version `0.12`._

Terraform projects are broken up into modules and environments. `modules/` defines infrastructure to be deployed into each environment. Each individual environment will then implement it's respective module. See https://www.terraform.io/docs/modules/usage.html for more information.

### Using Terraform

The following commands are used to develop and deploy infrastructure changes.

```bash
ENV=sbox make plan
ENV=sbox make apply
```

Valid environments are `sbox`, `staging`, `prod`.
