const cleanMongo = async (db) => {
  const collections = await db.collections();
  await Promise.all(collections.map(collection => collection.remove({})));
};

export default cleanMongo;
