import { MongoClient } from 'mongodb';

const connectMongo = () => MongoClient.connect('mongodb://mongo:27017/buoyweather_cms_dev');

export default connectMongo;
