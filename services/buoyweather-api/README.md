# Buoyweather API Microservice

## Table of Contents
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Local development](#local-development)
- [Service Validation](#service-validation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Local development

Follow usual procedures:

```bash
cp .env.sample .env
```

Update the Mongo connection string in `.env` for your credentials for the `dev` mongo db. Add an `EXPRESS_PORT=..` if you need to use a port other than `8080`.

```bash
npm i
```

And after joining the `dev` VPN:

```bash
npm run startlocal
```

## Service Validation

To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `buoyweather-api` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbnc1TXpVME5URXdNZyJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53NU16VTBOVEV3TWciLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the following endpoint (substituting parameters as needed): 
 
```
curl \
	-X GET \
  http://api.buoyweather.com/v1/buoyweather/data?product=nww3&lat=39.87601941962116&lon=-73.5150146484375&tz=-5&units=e&grb=wna
```

**Expected response:** The response should look something like:

```
{
  "modeloutput": {
    "dateString": [
      "November 24, 2020 18:00:00",
      "November 25, 2020 00:00:00",
      "November 25, 2020 06:00:00",
      "November 25, 2020 12:00:00",
      "November 25, 2020 18:00:00",
      "November 26, 2020 00:00:00",
      "November 26, 2020 06:00:00",
      "November 26, 2020 12:00:00",
      "November 26, 2020 18:00:00",
      "November 27, 2020 00:00:00",
      "November 27, 2020 06:00:00",
 ...
``` 
