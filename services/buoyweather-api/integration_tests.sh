docker-compose \
	-f docker-compose.test.yml \
	-f docker-compose.integration.yml \
	up \
	--build \
	--force-recreate \
	--no-color \
	--exit-code-from integration-tests
