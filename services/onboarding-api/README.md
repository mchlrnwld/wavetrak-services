# Surfline User Onboarding Micro-service

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


    - [QuickStart](#quickstart)
  - [Development](#development)
    - [Onboarding-Service Directory](#onboarding-service-directory)
      - [/common](#common)
      - [/error](#error)
      - [/model](#model)
      - [/server](#server)
- [API](#api)
      - [Table of Contents](#table-of-contents)
  - [Onboarding](#onboarding)
    - [Authentication](#authentication)
        - [Headers](#headers)
    - [API Errors](#api-errors)
- [Service Validation](#service-validation)
  - [Prerequisite: Obtaining an X-Auth-UserID](#prerequisite-obtaining-an-x-auth-userid)
  - [Performing Validations](#performing-validations)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


### QuickStart
 1. `nvm use v6.9.2`
 2. `cat .env.sample >> .env && npm install && npm run startlocal`
 3. [localhost:8082/health](http://localhost:8082/health)

Skip to [API docs](#api)
## Development
### Onboarding-Service Directory
```
 - /src
   | - /common
   | - /error
   | - /external
   | - /model
   | - /server
```
#### /common
Files included in the common directory should be generic and reusable throughout our suite of microservices.
#### /error
All errors thrown by the onboarding service are defined within individual files named after their constructor type, and exported from `index.js`. **NOTE:** Try to make these errors as genaric as possible.  We may want to resuse this logic within other services.
#### /model
Models included most of the business logic for this service.  At the root, we define the parent Onboarding schema from which all other `kind`s inherit from.
#### /server
The onboarding service API is exposed under two routes; `/admin/onboarding` and `/onboarding`.  Both routes are agnostic to the `kind` of onboarding being requested.  Although `/admin` in mounted for internal user only, it requires the same authentication and called the same handlers as the publicly mounted `/onboarding` routes.  _These files shouldn't need updating to support addition types of onboarding_.


 - TBD - document `/src` directory, linting, and test runners.

# API
#### Table of Contents
 - [Onboarding](##onboarding)


## Onboarding
This service is organized around REST, and provides a flexible yet predictable API that supports multiple type of onboardingd items.
### Authentication
##### Headers
Each request made to the onboarding service is authenticated and requires a valid `X-Auth-AccessToken` or `X-Auth-UserId` to exist in the **request header**.  Unauthorized requests will respond with the following response.

```
// HTTP GET `/onboarding`
// Response statusCode: 401 Unauthorized
{
  "message": "You are unauthorized to view this resource"
}
```
### API Errors
| StatusCode| Message | Triggered by
| ----------|---------|-----------
| 401 | "You are unauthorized to view this resource"|Invalid request headers
| 400 | "Invalid Parameters"| Ommitting `spotId` or other invalid, missing, or malformed parmeters.
| 500 | "Internal Server Error" | We messed up...

# Service Validation

## Prerequisite: Obtaining an X-Auth-UserID

An `X-Auth-UserId` must be passed in the header to perform the validation `curl` requests. This value corresponds to the `_id` field in the Mongo `UserDB.UserInfo` collection. For information on how to obtain this, continue reading this section.

You should create a registered account in the respective test environment if you don't already have one. You can do that by navigating to the Surfline homepage in the appropriate environment, and clicking `Sign In` > `Sign Up`.

Once you have a registered account, you should query the `UserDB.UserInfo` collection for the email that you used to register. Find the Mongo document, and copy the `_id` value. This will be the `X-Auth-UserId` that you use to perform the following verifications.


## Performing Validations
To validate that the service is functioning as expected (for example, after a deployment), start with the following:

1. Check the `onboarding-service` health in [New Relic APM](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMub3ZlcnZpZXciLCJlbnRpdHlJZCI6Ik16VTJNalExZkVGUVRYeEJVRkJNU1VOQlZFbFBUbncwTWpFeU5UTXpOdyJ9&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJNelUyTWpRMWZFRlFUWHhCVUZCTVNVTkJWRWxQVG53ME1qRXlOVE16TnciLCJzZWxlY3RlZE5lcmRsZXQiOnsibmVyZGxldElkIjoiYXBtLW5lcmRsZXRzLm92ZXJ2aWV3In19&platform[accountId]=356245&platform[timeRange][duration]=1800000&platform[$isFallbackTimeRange]=true).
2. Perform `curl` requests against the endpoints detailed in the [API](#api) section. 

**Note:** An `X-Auth-UserId` must be passed in the header to perform the following curl request. This value corresponds to the `_id` document field in the Mongo `userInfo` collection. 

**Ex:**

Test `GET` requests against the `/onboarding` endpoint:

```
curl \
    -X GET \
    -i \
    -H 'x-auth-userid: 5cb8c278a9680b000ffc68be' \
    https://sandbox.surfline.com/onboarding/about-you
```
**Expected response:** You should receive a `200` response along with a response body:

```
HTTP/2 200
date: Tue, 24 Nov 2020 04:09:30 GMT
content-type: text/html; charset=utf-8
set-cookie: __cfduid=d157e111b9208d1599cc2c77ac88d45c51606190970; expires=Thu, 24-Dec-20 04:09:30 GMT; path=/; domain=.surfline.com; HttpOnly; SameSite=Lax
set-cookie: ajs_anonymous_id=; Domain=.surfline.com; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT
set-cookie: sl_reset_anonymous_id=true; Max-Age=31536000; Path=/; Expires=Wed, 24 Nov 2021 04:09:30 GMT
vary: Accept-Encoding
set-cookie: disable_opt_out=true
x-cache: Miss from cloudfront
via: 1.1 da42857c896088f1d50640bf030a2034.cloudfront.net (CloudFront)
x-amz-cf-pop: LAX50-C1
x-amz-cf-id: k2aU5YnrwQrY4FnCaTGd0sLwSD_-3-_A8YYzNVk34bx2qxs45EbZjw==
cf-cache-status: DYNAMIC
cf-request-id: 069a0a4dae0000eacb4537c000000001
expect-ct: max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"
server: cloudflare
cf-ray: 5f70465c4b59eacb-LAX



<!DOCTYPE html>
...................
```