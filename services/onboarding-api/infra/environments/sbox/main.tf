provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/onboarding-api/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-3-sandbox"
}

data "aws_alb_listener" "main_internal" {
  arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-3-sandbox/9ff25c35a340093b/66fd46bbe4d5aa9e"
}

module "onboarding" {
  source = "../../"

  company     = "sl"
  application = "onboarding-api"
  environment = "sandbox"

  default_vpc      = "vpc-981887fd"
  ecs_cluster      = "sl-core-svc-sandbox"
  service_td_count = 1
  service_lb_rules = [
    {
      field = "host-header"
      value = "onboarding-api.sandbox.surfline.com"
    },
    {
      field = "path-pattern"
      value = "/onboarding*"
    },
  ]

  alb_listener_arn  = data.aws_alb_listener.main_internal.arn
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = data.aws_alb.main_internal.arn

  dns_name    = "onboarding-api.sandbox.surfline.com"
  dns_zone_id = "Z3DM6R3JR1RYXV"

  auto_scaling_enabled = false
}
