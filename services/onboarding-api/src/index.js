import 'newrelic';
import app from './server';
import logger from './common/logger';

const log = logger('onboarding-service:server');

Promise.all([])
  .then(app)
  .catch((err) => {
    console.log(err);
    log.error({
      message: 'App Initialization failed. Can\'t connect to database',
      stack: err,
    });
  });
