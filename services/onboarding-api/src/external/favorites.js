import request from 'request';
import logger from '../common/logger';
import config from '../config';

const log = logger('onboarding-api-service');

export const getFavoriteSpots = token =>
  new Promise((resolve, reject) => {
    const query = { type: 'spots' };
    const options = {
      uri: `${config.publicProxyUrl}/favorites/`,
      headers: {
        'X-Auth-AccessToken': token,
      },
      qs: query,
      json: true,
      timeout: 8000,
    };
    request(options, (error, response, body) => {
      if (error) {
        log.error({
          message: `Error from external:getFavoriteSpots token ${token}`,
          stack: error.stack,
        });
        reject(error);
      } else if (response.statusCode === 400) {
        resolve({});
      } else {
        resolve(body);
      }
    });
  });

export default getFavoriteSpots;
