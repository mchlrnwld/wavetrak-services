import request from 'request';
import logger from '../common/logger';
import config from '../config';

const log = logger('onboarding-api-service');

export const getNearbySpots = (coordinates, limit, offset, camsOnly = false) =>
  new Promise((resolve, reject) => {
    const query = {
      filter: 'nearby',
      offset,
      limit,
      coordinates,
      select: 'name,thumbnail,cams',
      camsOnly,
    };
    const options = {
      uri: `${config.spotsAPI}/admin/spots/`,
      qs: query,
      json: true,
      timeout: 8000,
    };
    request(options, (error, response, body) => {
      if (error) {
        log.error({
          message: `Error from external:getNearbySpots ${coordinates}`,
          stack: error.stack,
        });
        reject(error);
      } else if (response.statusCode === 400) {
        resolve({});
      } else {
        resolve(body);
      }
    });
  });

export const searchSpots = (query, limit, offset, camsOnly = false) =>
  new Promise((resolve, reject) => {
    const qs = { q: query, querySize: limit, offset, camsOnly };
    const options = {
      uri: `${config.searchAPI}/spots`,
      qs,
      json: true,
      timeout: 8000,
    };
    request(options, (error, response, body) => {
      if (error) {
        log.error({
          message: `Error from external:searchSpots ${query}`,
          stack: error.stack,
        });
        reject(error);
      } else {
        resolve(body);
      }
    });
  });

export default getNearbySpots;
