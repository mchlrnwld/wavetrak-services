import request from 'request';
import logger from '../common/logger';
import config from '../config';

const log = logger('onboarding-api-service');

export const getLocation = ipAddress =>
  new Promise((resolve, reject) => {
    const query = { ipAddress };
    const options = {
      uri: `${config.geotargetAPI}/Region`,
      qs: query,
      json: true,
      timeout: 8000,
    };
    request(options, (error, response, body) => {
      if (error) {
        log.error({
          message: `Error from external:getLocation ${ipAddress}`,
          stack: error.stack,
        });
        reject(error);
      } else if (response.statusCode === 400) {
        resolve({});
      } else {
        resolve(body);
      }
    });
  });

export default getLocation;
