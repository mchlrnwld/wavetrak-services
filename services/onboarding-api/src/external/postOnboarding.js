/* eslint arrow-body-style: ["error", "as-needed"]*/
import request from 'request';
import mongoose from 'mongoose';
import logger from '../common/logger';
import config from '../config';

const log = logger('onboarding-api-service');

export const putOnboarded = (onboarded, token) =>
  new Promise((resolve, reject) => {
    const options = {
      headers: {
        'X-Auth-AccessToken': token,
      },
      uri: `${config.publicProxyUrl}/user/Settings`,
      method: 'PUT',
      form: { onboarded },
      json: true,
    };
    request(options, (error, response, body) => {
      if (error) {
        reject(error);
      } else if (response.statusCode === 200) {
        resolve(body);
      } else {
        reject(response);
      }
    });
  }
);

const postSpotData = (spotIds, userId, token) =>
  new Promise((resolve, reject) => {
    const options = {
      headers: {
        'X-Auth-AccessToken': token,
        'Content-Type': 'application/json',
      },
      uri: `${config.publicProxyUrl}/favorites/`,
      method: 'POST',
      body: { type: 'spots', spotIds },
      json: true,
    };

    request(options, (error, response, body) => {
      if (error) {
        log.error({
          message: 'Error from external:postOnboarding:postSpotData',
          stack: error.stack,
        });
        reject(error);
      } else if (response.statusCode === 200) {
        resolve(body);
      } else {
        reject(response);
      }
    });
  }
);

export const postAllSpotData = async (spotsAdded, authenticatedUserId, authenticatedToken) => {
  const userId = authenticatedUserId;
  const token = authenticatedToken;

  if (!spotsAdded.length) {
    return 0;
  }

  const spotIds = spotsAdded.filter(spotId => mongoose.Types.ObjectId.isValid(spotId));
  if (spotIds.length) {
    try {
      await postSpotData(spotIds, userId, token);
    } catch (err) {
      throw new Error('Error posting spots');
    }
  }
  return spotIds.length;
};

const deleteSpotData = (spotIds, userId, token) =>
  new Promise((resolve, reject) => {
    const options = {
      headers: {
        'X-Auth-AccessToken': token,
        'Content-Type': 'application/json',
      },
      uri: `${config.publicProxyUrl}/favorites/`,
      method: 'DELETE',
      body: { type: 'spots', spotIds },
      json: true,
    };

    request(options, (error, response, body) => {
      if (error) {
        log.error({
          message: 'Error from external:postOnboarding:deleteSpotData',
          stack: error.stack,
        });
        reject(error);
      } else if (response.statusCode === 200) {
        resolve(body);
      } else {
        reject(response);
      }
    });
  }
);

export const deleteAllSpotData = async (spotsRemoved, authenticatedUserId, authenticatedToken) => {
  const userId = authenticatedUserId;
  const token = authenticatedToken;

  if (!spotsRemoved.length) {
    return 0;
  }

  const spotIds = spotsRemoved.filter(spotId => mongoose.Types.ObjectId.isValid(spotId));

  if (spotIds.length) {
    try {
      await deleteSpotData(spotIds, userId, token);
    } catch (err) {
      throw new Error('Error deleting spots');
    }
  }
  return spotIds.length;
};
