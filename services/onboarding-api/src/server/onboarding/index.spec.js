import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import sinon from 'sinon';
import express from 'express';
import onboarding from './';
import * as GeoTargetService from '../../external/geotarget';
import * as Spots from '../../external/spots';
import * as PostOnboarding from '../../external/postOnboarding';

chai.use(chaiHttp);

describe('/onboarding/spots', () => {
  let log;
  let request;

  const location = {
    continent: {
      geoname_id: 6255149,
      name: 'North America',
    },
    country: {
      geoname_id: 3703430,
      name: 'Panama',
      iso_code: 'PA',
    },
    location: {
      latitude: 8.5667,
      longitude: -82.4167,
      time_zone: 'America/Panama',
    },
    subdivisions: [
      {
        geoname_id: 3712410,
        iso_code: '4',
        name: 'Provincia de Chiriqui',
      },
    ],
  };

  const spots = [{
    _id: '584204204e65fad6a770913e',
    name: 'Zancudo',
    cams: [],
    thumbnail: {
      300: 'https://spot-thumbnails.staging.surfline.com/spots/default/default_300.jpg',
      640: 'https://spot-thumbnails.staging.surfline.com/spots/default/default_640.jpg',
    },
  }];

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(onboarding(log));
    request = chai.request(app).keepOpen();
  });
  after(() => {
    request.close();
  });

  beforeEach(() => {
    sinon.stub(GeoTargetService, 'getLocation');
    sinon.stub(Spots, 'getNearbySpots');
  });

  afterEach(() => {
    GeoTargetService.getLocation.restore();
    Spots.getNearbySpots.restore();
  });

  it('should return spots near location', async () => {
    Spots.getNearbySpots.resolves(spots);
    GeoTargetService.getLocation.resolves(location);
    const res = await request.get('/spots').send();
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal({ spots });
    expect(Spots.getNearbySpots).to.have.been.calledOnce();
  });

  it('should return 400 for GET /spots when limit exceeds the max limit', (done) => {
    request.get('/spots')
    .query({ limit: 100000 })
    .end((_err, res) => {
      expect(res).to.have.status(400);
      expect(res.body).to.deep.equal({ message: 'Exceeded max limit' });
      done();
    });
  });
});

describe('/onboarding/post', () => {
  let log;
  let request;

  const onboardedResponse = { spotsAdded: 1, spotsRemoved: 1 };

  const onboardPostData = {
    spots: {
      added: [
        '5842041f4e65fad6a77087f3',
      ],
      removed: [
        '5842041f4e65fad6a77087f4',
      ],
    },
    authenticatedUserId: '123456',
  };

  const postSpotsResponse = 1;
  const deleteSpotsResponse = 1;

  before(() => {
    log = { trace: sinon.spy() };
    const app = express();
    app.use(onboarding(log));
    request = chai.request(app);
  });

  beforeEach(() => {
    sinon.stub(PostOnboarding, 'postAllSpotData');
    sinon.stub(PostOnboarding, 'deleteAllSpotData');
  });

  afterEach(() => {
    PostOnboarding.postAllSpotData.restore();
    PostOnboarding.deleteAllSpotData.restore();
  });

  it('should return onboarded success', async () => {
    PostOnboarding.postAllSpotData.resolves(postSpotsResponse);
    PostOnboarding.deleteAllSpotData.resolves(deleteSpotsResponse);
    const params = onboardPostData;
    const res = await request
    .post('/')
    .set('x-auth-accesstoken', '26701d3b97b0df2fd3e8966142af5a4e46771b55')
    .set('Content-Type', 'application/json')
    .set('x-auth-userid', '582e0255d7e44b1074114ba9')
    .send(params, () => {});
    expect(res).to.have.status(200);
    expect(res).to.be.json();
    expect(res.body).to.deep.equal(onboardedResponse);
    expect(PostOnboarding.postAllSpotData).to.have.been.calledOnce();
    expect(PostOnboarding.deleteAllSpotData).to.have.been.calledOnce();
  });
});
