import { getNearbySpots } from '../../../external/spots';
import { getLocation } from '../../../external/geotarget';

const getSpotsByLocation = async (ipAddress, limit, offset, camsOnly = false) => {
  const { location: { latitude, longitude } } = await getLocation(ipAddress);
  const spots = await getNearbySpots([longitude, latitude], limit, offset, camsOnly);
  return spots;
};

export default getSpotsByLocation;
