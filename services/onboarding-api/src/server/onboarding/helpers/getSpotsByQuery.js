import { searchSpots } from '../../../external/spots';

const getSpotsByQuery = async (query, limit, offset, camsOnly = false) => {
  const response = await searchSpots(query, limit, offset, camsOnly);
  const spots = response[0].hits.hits.map(({ _id, _source }) => ({
    _id,
    name: _source.name,
    thumbnail: {
      300: _source.thumbnail_300,
      640: _source.thumbnail_640,
    },
    cams: _source.cams,
  }));
  return spots;
};

export default getSpotsByQuery;
