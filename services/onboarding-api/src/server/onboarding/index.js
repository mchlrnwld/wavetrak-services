import { Router } from 'express';
import { json } from 'body-parser';
import { wrapErrors } from '@surfline/services-common';
import {
  trackOnboardingRequests,
  getOnboardingSpotsHandler,
  postOnboardingHandler,
} from './onboarding';

const onboarding = (log) => {
  const api = Router();
  api.get('/health', (req, res) => res.send({
    status: 200,
    message: 'OK',
    version: process.env.APP_VERSION || 'unknown',
  }));
  api.use('*', json(), trackOnboardingRequests(log));
  api.post('/', wrapErrors(postOnboardingHandler));
  api.get('/spots', wrapErrors(getOnboardingSpotsHandler));
  return api;
};

export default onboarding;
