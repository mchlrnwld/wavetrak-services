import { includes } from 'lodash';
import { getFavoriteSpots } from '../../external/favorites';
import { postAllSpotData, deleteAllSpotData } from '../../external/postOnboarding';
import { getSpotsByLocation, getSpotsByQuery } from './helpers';
import MAX_SPOT_LIMIT from '../../common/constants';

export const trackOnboardingRequests = log => (req, res, next) => {
  log.trace({
    action: '/onboarding',
    request: {
      headers: req.headers,
      params: req.params,
      query: req.query,
      body: req.body,
    },
  });
  return next();
};

export const getOnboardingSpotsHandler = async (req, res) => {
  const { ipAddress, query, limit = MAX_SPOT_LIMIT, offset, camsOnly } = req.query;
  if (limit && limit > MAX_SPOT_LIMIT) {
    return res.status(400).send({
      message: 'Exceeded max limit',
    });
  }

  const filterCamsOnly = camsOnly && camsOnly.toUpperCase() === 'TRUE';

  const spots = query
    ? await getSpotsByQuery(query, limit, offset, filterCamsOnly)
    : await getSpotsByLocation(ipAddress || req.ip, limit, offset, filterCamsOnly);

  const onboardingSpots = spots.map(spot => ({
    _id: spot._id,
    name: spot.name,

    // Onboarding spots only needs 2 smallest of the available thumbnails
    thumbnail: spot.thumbnail ? { 300: spot.thumbnail['300'], 640: spot.thumbnail['640'] } : null,
    cams: spot.cams && spot.cams[0] ? spot.cams : [],
  }));

  return res.send({
    spots: onboardingSpots,
  });
};

export const postOnboardingHandler = async (req, res) => {
  const { body, authenticatedToken } = req;
  const spots = body.spots || {};
  const { added, removed } = spots;

  // allow tests to bypass live user id value being passed
  // in actual requests bad user id values will fail api responses.
  const authenticatedUserId = req.authenticatedUserId || req.body.authenticatedUserId;

  // If not authenticated, return 401: Authentication Required
  if (!authenticatedUserId) {
    return res.status(401).send({
      message: 'Authentication Required',
    });
  }

  let spotsAdded = 0;
  // POST spots added during onboarding to /favorites api
  if (Array.isArray(added) && added.length > 0) {
    try {
      let addedSpots = added;

      // ensure no existing favorite spots are included in addedSpots array;
      const favoriteSpots = authenticatedToken ? await getFavoriteSpots(authenticatedToken) : null;

      if (favoriteSpots && favoriteSpots.favorites && favoriteSpots.favorites.length) {
        // Compare only the spotIds as this is what is being passed.
        const favoriteSpotIds = favoriteSpots.favorites.map(favoriteSpot => favoriteSpot.spotId);
        addedSpots = added.filter(favAdded => includes(favoriteSpotIds, favAdded) === false);
      }
      spotsAdded = await postAllSpotData(addedSpots, authenticatedUserId, authenticatedToken);
    } catch (err) {
      return res.status(400).send({
        message: 'Unable to add spot favorites',
      });
    }
  }

  let spotsRemoved = 0;
  // DELETE any existing favorites that have been deselected during onboarding
  if (Array.isArray(removed) && removed.length > 0) {
    try {
      spotsRemoved = await deleteAllSpotData(removed, authenticatedUserId, authenticatedToken);
    } catch (err) {
      return res.status(400).send({
        message: 'Unable to delete spot favorites',
      });
    }
  }

  return res.status(200).send({
    spotsAdded,
    spotsRemoved,
  });
};
