import { setupExpress } from '@surfline/services-common';
import onboarding from './onboarding';
import logger from '../common/logger';

const log = logger('onboarding-service');

const setupApp = () =>
  setupExpress({
    port: process.env.EXPRESS_PORT || 8080,
    name: 'onboarding-service',
    log,
    allowedMethods: ['GET', 'OPTIONS'],
    handlers: [['/onboarding', onboarding(log)]],
  });

export default setupApp;
