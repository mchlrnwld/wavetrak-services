export default {
  CONSOLE_LOG_LEVEL: process.env.CONSOLE_LOG_LEVEL,
  mongoConnectionString: process.env.MONGO_CONNECTION_STRING,
  geotargetAPI: process.env.GEOTARGET_API,
  LOGSENE_KEY: process.env.LOGSENE_KEY,
  LOGSENE_LEVEL: process.env.LOGSENE_LEVEL,
  searchAPI: process.env.SEARCH_API,
  spotsAPI: process.env.SPOTS_API,
  publicProxyUrl: process.env.PUBLIC_PROXY_URL,
};
