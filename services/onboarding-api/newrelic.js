let newRelicAppName = 'Surfline Services - Onboarding';
if (process.env.NODE_ENV !== 'production') {
  newRelicAppName += ` (${process.env.NODE_ENV})`;
}

console.log(`New Relic enabled: ${process.env.NEW_RELIC_ENABLED}`);
/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 * 
 * https://github.com/newrelic/node-newrelic/blob/master/lib/config/default.js
 */
exports.config = {
  /**
   * @env NEW_RELIC_APP_NAME
   */  
  app_name: [newRelicAppName]
};