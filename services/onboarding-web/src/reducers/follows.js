import createReducer from './createReducer';
import { SET_FOLLOWS } from '../actions/follows';

const initialState = {
  follows: null,
};

const handlers = {};

handlers[SET_FOLLOWS] = (state, { follows }) => ({
  ...state,
  follows,
});

export default createReducer(handlers, initialState);
