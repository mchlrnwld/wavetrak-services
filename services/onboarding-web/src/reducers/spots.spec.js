import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { FETCH_SPOTS, FETCH_SPOTS_SUCCESS, FETCH_SPOTS_FAILURE } from '../actions/spots';
import reducer from './spots';

describe('Reducers / spots', () => {
  const initialState = deepFreeze({
    loading: false,
    error: null,
    spots: [],
    query: null,
    end: false,
  });

  it('defaults to initial state', () => {
    expect(reducer()).to.deep.equal(initialState);
  });

  it('handles fetch spots', () => {
    const state = deepFreeze({
      loading: false,
      error: 'Some existing error',
      spots: ['spot one', 'spot two'],
      query: 'hb pier',
      end: false,
    });
    const action = deepFreeze({ type: FETCH_SPOTS });
    expect(reducer(state, action)).to.deep.equal({
      loading: true,
      error: null,
      spots: ['spot one', 'spot two'],
      query: 'hb pier',
      end: false,
    });
  });

  it('handles fetch spots success', () => {
    const state = deepFreeze({
      loading: true,
      error: null,
      spots: ['old spot'],
      query: 'hb pier',
      end: false,
    });
    const action = deepFreeze({
      type: FETCH_SPOTS_SUCCESS,
      spots: ['spot one', 'spot two'],
      end: true,
    });

    expect(reducer(state, action)).to.deep.equal({
      loading: false,
      error: null,
      spots: ['old spot', 'spot one', 'spot two'],
      query: 'hb pier',
      end: true,
    });
  });

  it('resets spots on fetch spots success with reset true', () => {
    const state = deepFreeze({
      loading: true,
      error: null,
      spots: ['old spot'],
      query: 'hb pier',
      end: false,
    });
    const action = deepFreeze({
      type: FETCH_SPOTS_SUCCESS,
      spots: ['spot one', 'spot two'],
      reset: true,
      end: true,
    });

    expect(reducer(state, action)).to.deep.equal({
      loading: false,
      error: null,
      spots: ['spot one', 'spot two'],
      query: 'hb pier',
      end: true,
    });
  });

  it('handles fetch spots failure', () => {
    const state = deepFreeze({
      loading: true,
      error: null,
      spots: [],
      query: 'hb pier',
      end: false,
    });
    const action = deepFreeze({
      type: FETCH_SPOTS_FAILURE,
      error: 'Fetch failed',
    });
    expect(reducer(state, action)).to.deep.equal({
      loading: false,
      error: 'Fetch failed',
      spots: [],
      query: 'hb pier',
      end: false,
    });
  });
});
