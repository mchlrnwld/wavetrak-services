import {
  FETCH_USER_DETAILS,
  FETCH_USER_DETAILS_SUCCESS,
  FETCH_USER_DETAILS_FAILURE,
  FETCH_USER_ENTITLEMENTS,
  FETCH_USER_ENTITLEMENTS_SUCCESS,
  FETCH_USER_ENTITLEMENTS_FAILURE,
  FETCH_USER_SETTINGS,
  FETCH_USER_SETTINGS_SUCCESS,
  FETCH_USER_SETTINGS_FAILURE,
  FETCH_USER_FAVORITES,
  FETCH_USER_FAVORITES_SUCCESS,
  FETCH_USER_FAVORITES_FAILURE,
} from '@surfline/web-common';
import createReducer from './createReducer';

const initialState = {
  details: null,
  settings: null,
  entitlements: [],
  favorites: [],
};

const handlers = {};

handlers[FETCH_USER_DETAILS] = (state) => ({
  ...state,
  loading: true,
});

handlers[FETCH_USER_DETAILS_SUCCESS] = (state, { details }) => ({
  ...state,
  loading: false,
  details: {
    firstName: details.firstName,
    lastName: details.lastName,
    email: details.email,
    _id: details._id,
  },
});

handlers[FETCH_USER_DETAILS_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

handlers[FETCH_USER_SETTINGS] = (state) => ({
  ...state,
  loading: true,
});

handlers[FETCH_USER_SETTINGS_SUCCESS] = (state, { settings }) => ({
  ...state,
  loading: false,
  settings,
});

handlers[FETCH_USER_SETTINGS_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

handlers[FETCH_USER_ENTITLEMENTS] = (state) => ({
  ...state,
  loading: true,
});

handlers[FETCH_USER_ENTITLEMENTS_SUCCESS] = (state, { entitlements }) => ({
  ...state,
  loading: false,
  entitlements,
});

handlers[FETCH_USER_ENTITLEMENTS_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

handlers[FETCH_USER_FAVORITES] = (state) => ({
  ...state,
  loading: true,
});

handlers[FETCH_USER_FAVORITES_SUCCESS] = (state, { favorites }) => ({
  ...state,
  loading: false,
  favorites,
});

handlers[FETCH_USER_FAVORITES_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

export default createReducer(handlers, initialState);
