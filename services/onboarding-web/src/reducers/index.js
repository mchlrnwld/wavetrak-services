import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import favorites from './favorites';
import follows from './follows';
import spots from './spots';
import user from './user';
import status from './status';
import onboarding from './onboarding';

// import map from './map';

function geo(state = {}, action = {}) {
  switch (action.type) {
    default:
      return state;
  }
}

export default (history) =>
  combineReducers({
    favorites,
    follows,
    spots,
    status,
    router: connectRouter(history),
    user,
    onboarding,
    geo,
    // map,
  });
