import { expect } from 'chai';
import { createMemoryHistory } from 'history';
import reducers from '.';

describe('reducers', () => {
  it('combines routing, onboarding', () => {
    const history = createMemoryHistory({
      initialEntries: ['/'],
    });
    const state = reducers(history)();
    expect(state.router).to.be.ok();
    expect(state.onboarding).to.be.ok();
  });
});
