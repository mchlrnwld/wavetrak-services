import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  FETCH_USER_DETAILS,
  FETCH_USER_DETAILS_SUCCESS,
  FETCH_USER_DETAILS_FAILURE,
  FETCH_USER_SETTINGS,
  FETCH_USER_SETTINGS_SUCCESS,
  FETCH_USER_SETTINGS_FAILURE,
  FETCH_USER_ENTITLEMENTS,
  FETCH_USER_ENTITLEMENTS_SUCCESS,
  FETCH_USER_ENTITLEMENTS_FAILURE,
} from '@surfline/web-common';
import user from './user';

describe('reducers / user', () => {
  const initialState = deepFreeze({
    details: null,
    settings: null,
    entitlements: [],
    favorites: [],
  });

  it('initializes with default values', () => {
    expect(user()).to.deep.equal(initialState);
  });

  it('sets loading on FETCH_USER_DETAILS', () => {
    const action = deepFreeze({
      type: FETCH_USER_DETAILS,
    });

    expect(user(initialState, action)).to.deep.equal({
      details: null,
      settings: null,
      entitlements: [],
      favorites: [],
      loading: true,
    });
  });

  it('sets loading and userDetails on FETCH_USER_DETAILS_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_USER_DETAILS_SUCCESS,
      details: {
        firstName: 'Lucy',
        lastName: 'Goosey',
        email: 'lucybear@gmail.com',
        _id: 'aaa',
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      details: action.details,
      settings: null,
      entitlements: [],
      favorites: [],
      loading: false,
    });
  });

  it('sets error and loading on FETCH_USER_DETAILS_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_USER_DETAILS_FAILURE,
      error: {
        message: 'An error occurred',
        status: 500,
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      details: null,
      settings: null,
      entitlements: [],
      favorites: [],
      loading: false,
      error: {
        message: 'An error occurred',
        status: 500,
      },
    });
  });

  it('sets loading on FETCH_USER_SETTINGS', () => {
    const action = deepFreeze({
      type: FETCH_USER_SETTINGS,
    });

    expect(user(initialState, action)).to.deep.equal({
      details: null,
      settings: null,
      entitlements: [],
      favorites: [],
      loading: true,
    });
  });

  it('sets loading and userSettings on FETCH_USER_SETTINGS_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_USER_SETTINGS_SUCCESS,
      settings: {
        _id: 'abc123',
        user: 'xyzpdq',
        preferredWaveChart: 'SURF',
        location: {
          coordinates: [0, 0],
          type: 'Point',
        },
        onboarded: [],
        units: {
          temperature: 'F',
          windSpeed: 'KTS',
          tideHeight: 'FT',
          swellHeight: 'FT',
          surfHeight: 'FT',
        },
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      details: null,
      settings: action.settings,
      entitlements: [],
      favorites: [],
      loading: false,
    });
  });

  it('sets error and loading on FETCH_USER_SETTINGS_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_USER_SETTINGS_FAILURE,
      error: {
        message: 'An error occurred',
        status: 500,
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      details: null,
      settings: null,
      entitlements: [],
      favorites: [],
      loading: false,
      error: {
        message: 'An error occurred',
        status: 500,
      },
    });
  });

  it('sets loading on FETCH_USER_ENTITLEMENTS', () => {
    const action = deepFreeze({
      type: FETCH_USER_ENTITLEMENTS,
    });

    expect(user(initialState, action)).to.deep.equal({
      details: null,
      settings: null,
      entitlements: [],
      favorites: [],
      loading: true,
    });
  });

  it('sets user entitlements and loading on FETCH_USER_ENTITLEMENTS_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_USER_ENTITLEMENTS_SUCCESS,
      entitlements: ['these', 'are', 'entitlements'],
    });

    expect(user(initialState, action)).to.deep.equal({
      details: null,
      settings: null,
      entitlements: action.entitlements,
      favorites: [],
      loading: false,
    });
  });

  it('sets error and loading on FETCH_USER_ENTITLEMENTS_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_USER_ENTITLEMENTS_FAILURE,
      error: {
        message: 'An error occurred',
        status: 500,
      },
    });

    expect(user(initialState, action)).to.deep.equal({
      details: null,
      settings: null,
      entitlements: [],
      favorites: [],
      loading: false,
      error: {
        message: 'An error occurred',
        status: 500,
      },
    });
  });
});
