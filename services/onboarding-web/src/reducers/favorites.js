import createReducer from './createReducer';
import {
  FETCH_FAVORITES,
  FETCH_FAVORITES_SUCCESS,
  FETCH_FAVORITES_FAILURE,
  SET_FAVORITED_SPOT,
  SET_FAVORITED_SPOT_SUCCESS,
  SET_FAVORITED_SPOT_FAILURE,
} from '../actions/favorites';

const initialState = {
  spots: [],
  loading: false,
  error: null,
};

const handlers = {};

handlers[FETCH_FAVORITES] = (state) => ({
  ...state,
  loading: true,
});

handlers[FETCH_FAVORITES_SUCCESS] = (state, { favorites }) => ({
  ...state,
  loading: false,
  spots: favorites.map((favorite) => favorite.spotId),
});

handlers[FETCH_FAVORITES_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

handlers[SET_FAVORITED_SPOT] = (state) => ({
  ...state,
  loading: true,
});

handlers[SET_FAVORITED_SPOT_SUCCESS] = (state, { spotId, isFavorite }) => {
  const spotIndex = state.spots.indexOf(spotId);
  return {
    ...state,
    loading: false,
    spots: isFavorite
      ? [...state.spots, spotId]
      : [...state.spots.slice(0, spotIndex), ...state.spots.slice(spotIndex + 1)],
  };
};

handlers[SET_FAVORITED_SPOT_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

export default createReducer(handlers, initialState);
