import createReducer from './createReducer';
import {
  GET_NEARBY_TAXONOMIES,
  GET_NEARBY_TAXONOMIES_SUCCESS,
  GET_NEARBY_TAXONOMIES_FAILURE,
  GET_TAXONOMY,
  GET_TAXONOMY_SUCCESS,
  GET_TAXONOMY_FAILURE,
  RESET_TAXONOMY_TYPE,
  GET_POPULAR_SPOTS,
  GET_POPULAR_SPOTS_SUCCESS,
  GET_POPULAR_SPOTS_FAILURE,
  GET_RECOMMENDED_SPOTS,
  GET_RECOMMENDED_SPOTS_SUCCESS,
  GET_RECOMMENDED_SPOTS_FAILURE,
  GET_SPOTS_WITHIN_CHOSEN_AREAS,
  GET_SPOTS_WITHIN_CHOSEN_AREAS_SUCCESS,
  GET_SPOTS_WITHIN_CHOSEN_AREAS_FAILURE,
  SET_ONBOARDING_BACKGROUND,
  REMOVE_ONBOARDING_BACKGROUND,
  FAVORITED_POPULAR_SPOT,
  FAVORITED_SEARCH_SPOT,
  FAVORITED_WITHIN_AREA_SPOT,
  FAVORITED_RECOMMENDED_SPOT,
  UNFAVORITED_POPULAR_SPOT,
  UNFAVORITED_RECOMMENDED_SPOT,
  UNFAVORITED_SEARCH_SPOT,
  UNFAVORITED_WITHIN_AREA_SPOT,
} from '../actions/onboarding';

const initialState = {
  background: '',
  favorites: {
    popular: 0,
    popularSpotsFavorited: [],
    withinAreas: 0,
    recommended: 0,
    search: 0,
  },
  popular: {
    loading: false,
    dataLoaded: false,
    spots: [],
    error: null,
    end: true,
  },
  withinAreas: {
    loading: false,
    dataLoaded: false,
    spots: [],
    error: null,
    end: true,
  },
  recommended: {
    loading: false,
    dataLoaded: false,
    abilityLevel: null,
    travelDistance: null,
    spots: [],
    error: null,
    end: false,
  },
  taxonomies: {
    loading: true,
    error: null,
    taxonomies: [],
    dataLoaded: false,
    earth: {
      loading: false,
      error: null,
      data: [],
      taxonomyId: null,
      dataLoaded: false,
    },
    continent: {
      loading: false,
      error: null,
      data: [],
      taxonomyId: null,
      dataLoaded: false,
    },
    country: {
      loading: false,
      error: null,
      data: [],
      taxonomyId: null,
      dataLoaded: false,
    },
    region: {
      loading: false,
      error: null,
      data: [],
      taxonomyId: null,
      dataLoaded: false,
    },
  },
};

const handlers = {};

handlers[SET_ONBOARDING_BACKGROUND] = (state, { backgroundUrl }) => ({
  ...state,
  background: backgroundUrl,
});

handlers[REMOVE_ONBOARDING_BACKGROUND] = (state) => ({
  ...state,
  background: '',
});

handlers[GET_NEARBY_TAXONOMIES] = (state) => ({
  ...state,
  taxonomies: {
    ...state.taxonomies,
    loading: true,
    dataLoaded: false,
    error: null,
    taxonomies: [],
  },
});

handlers[GET_NEARBY_TAXONOMIES_SUCCESS] = (state, { taxonomies }) => ({
  ...state,
  taxonomies: {
    ...state.taxonomies,
    loading: false,
    dataLoaded: true,
    taxonomies,
    error: null,
  },
});

handlers[GET_NEARBY_TAXONOMIES_FAILURE] = (state, { error }) => ({
  ...state,
  taxonomies: {
    ...state.taxonomies,
    loading: false,
    dataLoaded: false,
    error,
    taxonomies: [],
  },
});

handlers[GET_TAXONOMY] = (state, { taxonomyType }) => ({
  ...state,
  taxonomies: {
    ...state.taxonomies,
    [taxonomyType]: {
      ...state.taxonomies[taxonomyType],
      loading: true,
    },
  },
});

handlers[GET_TAXONOMY_SUCCESS] = (state, { taxonomyType, data, taxonomyId }) => ({
  ...state,
  taxonomies: {
    ...state.taxonomies,
    [taxonomyType]: {
      ...state.taxonomies[taxonomyType],
      loading: false,
      taxonomyId,
      data: data.map((item) => ({
        ...item,
        value: item._id,
        text: item.name,
      })),
      dataLoaded: true,
    },
  },
});

handlers[GET_TAXONOMY_FAILURE] = (state, { taxonomyType, error }) => ({
  ...state,
  taxonomies: {
    ...state.taxonomies,
    [taxonomyType]: {
      data: null,
      dataLoaded: false,
      loading: false,
      taxonomyId: null,
      error,
    },
  },
});

handlers[RESET_TAXONOMY_TYPE] = (state, { taxonomyType }) => ({
  ...state,
  taxonomies: {
    ...state.taxonomies,
    [taxonomyType]: {
      ...initialState.taxonomies[taxonomyType],
    },
  },
});

handlers[GET_POPULAR_SPOTS] = (state) => ({
  ...state,
  popular: {
    ...state.popular,
    loading: true,
    dataLoaded: false,
  },
});

handlers[GET_POPULAR_SPOTS_SUCCESS] = (state, { spots }) => ({
  ...state,
  popular: {
    ...state.popular,
    loading: false,
    dataLoaded: true,
    spots,
  },
});

handlers[GET_POPULAR_SPOTS_FAILURE] = (state, { error }) => ({
  ...state,
  popular: {
    ...state.popular,
    loading: false,
    dataLoaded: false,
    error,
    spots: [],
  },
});

handlers[GET_RECOMMENDED_SPOTS] = (state) => ({
  ...state,
  recommended: {
    ...state.recommended,
    loading: true,
    dataLoaded: false,
  },
});

handlers[GET_RECOMMENDED_SPOTS_SUCCESS] = (
  state,
  { spots, end, abilityLevel, travelDistance },
) => ({
  ...state,
  recommended: {
    ...state.recommended,
    loading: false,
    dataLoaded: true,
    spots: state.recommended.spots.concat(spots),
    end,
    abilityLevel,
    travelDistance,
  },
});

handlers[GET_RECOMMENDED_SPOTS_FAILURE] = (state, { error }) => ({
  ...state,
  recommended: {
    ...state.recommended,
    loading: false,
    dataLoaded: false,
    error,
    spots: [],
  },
});

handlers[GET_SPOTS_WITHIN_CHOSEN_AREAS] = (state) => ({
  ...state,
  withinAreas: {
    ...state.withinAreas,
    loading: true,
    dataLoaded: false,
  },
});

handlers[GET_SPOTS_WITHIN_CHOSEN_AREAS_SUCCESS] = (state, { spots, selectedTaxonomies }) => ({
  ...state,
  withinAreas: {
    ...state.withinAreas,
    loading: false,
    dataLoaded: true,
    spots,
    selectedTaxonomies,
  },
});

handlers[GET_SPOTS_WITHIN_CHOSEN_AREAS_FAILURE] = (state, { error }) => ({
  ...state,
  withinAreas: {
    ...state.withinAreas,
    loading: false,
    dataLoaded: false,
    error,
    spots: [],
  },
});

handlers[FAVORITED_POPULAR_SPOT] = (state, { spotName }) => ({
  ...state,
  favorites: {
    ...state.favorites,
    popular: state.favorites.popular + 1,
    popularSpotsFavorited: [...state.favorites.popularSpotsFavorited, spotName],
  },
});

handlers[UNFAVORITED_POPULAR_SPOT] = (state, { spotName }) => ({
  ...state,
  favorites: {
    ...state.favorites,
    popular: state.favorites.popular > 0 ? state.favorites.popular - 1 : 0,
    popularSpotsFavorited: state.favorites.popularSpotsFavorited.filter(
      (spot) => spot !== spotName,
    ),
  },
});

handlers[FAVORITED_RECOMMENDED_SPOT] = (state) => ({
  ...state,
  favorites: {
    ...state.favorites,
    recommended: state.favorites.recommended + 1,
  },
});

handlers[UNFAVORITED_RECOMMENDED_SPOT] = (state) => ({
  ...state,
  favorites: {
    ...state.favorites,
    recommended: state.favorites.recommended > 0 ? state.favorites.recommended - 1 : 0,
  },
});

handlers[FAVORITED_SEARCH_SPOT] = (state) => ({
  ...state,
  favorites: {
    ...state.favorites,
    search: state.favorites.search + 1,
  },
});

handlers[UNFAVORITED_SEARCH_SPOT] = (state) => ({
  ...state,
  favorites: {
    ...state.favorites,
    search: state.favorites.search > 0 ? state.favorites.search - 1 : 0,
  },
});

handlers[FAVORITED_WITHIN_AREA_SPOT] = (state) => ({
  ...state,
  favorites: {
    ...state.favorites,
    withinAreas: state.favorites.withinAreas + 1,
  },
});

handlers[UNFAVORITED_WITHIN_AREA_SPOT] = (state) => ({
  ...state,
  favorites: {
    ...state.favorites,
    withinAreas: state.favorites.withinAreas > 0 ? state.favorites.withinAreas - 1 : 0,
  },
});

export default createReducer(handlers, initialState);
