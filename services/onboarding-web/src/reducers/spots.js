import createReducer from './createReducer';
import {
  FETCH_SPOTS,
  FETCH_SPOTS_SUCCESS,
  FETCH_SPOTS_FAILURE,
  UPDATE_SEARCH_QUERY,
} from '../actions/spots';

const initialState = {
  loading: false,
  error: null,
  spots: [],
  query: null,
  end: false,
};

const handlers = {};

handlers[FETCH_SPOTS] = (state) => ({
  ...state,
  loading: true,
  error: null,
});

handlers[FETCH_SPOTS_SUCCESS] = (state, { spots, reset, end }) => ({
  ...state,
  loading: false,
  spots: reset ? spots : state.spots.concat(spots),
  end,
});

handlers[FETCH_SPOTS_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

handlers[UPDATE_SEARCH_QUERY] = (state, { query }) => ({
  ...state,
  query,
});

export default createReducer(handlers, initialState);
