import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { SET_FAVORITED_SPOT_SUCCESS } from '../actions/favorites';

import reducer from './favorites';

describe('Reducers / spots', () => {
  const initialState = {
    spots: [],
    loading: false,
    error: null,
  };

  it('defaults to initial state', () => {
    expect(reducer()).to.deep.equal(initialState);
  });

  it('sets a favorited spot', () => {
    const action = deepFreeze({
      type: SET_FAVORITED_SPOT_SUCCESS,
      spotId: 'randomSpotId',
      isFavorite: true,
    });
    expect(reducer(initialState, action)).to.deep.equal({
      spots: ['randomSpotId'],
      loading: false,
      error: null,
    });
  });

  it('removes a favorite spot', () => {
    const state = {
      ...initialState,
      spots: ['randomSpotId1', 'randomSpotId2', 'randomSpotId3'],
    };
    const action = deepFreeze({
      type: SET_FAVORITED_SPOT_SUCCESS,
      spotId: 'randomSpotId2',
      isFavorite: false,
    });
    expect(reducer(state, action)).to.deep.equal({
      spots: ['randomSpotId1', 'randomSpotId3'],
      loading: false,
      error: null,
    });
  });
});
