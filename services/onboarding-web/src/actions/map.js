// import canUseNavigator from '../common/canUseNavigator';
// import { getUserRegion, fetchLocationMapView } from '../common/api/map';
//
// export const FETCH_LOCATION_URL = 'FETCH_LOCATION_URL';
// export const FETCH_LOCATION_URL_SUCCESS = 'FETCH_LOCATION_URL_SUCCESS';
// export const FETCH_LOCATION_URL_FAILURE = 'FETCH_LOCATION_URL_FAILURE';
//
// const surflineLocation = {
//   // Default to Surfline HQ
//   center: {
//     lat: 33.654213041213,
//     lon: -118.0032588192,
//   },
//   zoom: 12,
// };
//
// const getUserLocationByIP = async () => {
//   const region = await getUserRegion();
//   let zoom = 12;
//
//   if (!region.city) zoom = 7;
//   if (!region.country) zoom = 5;
//
//   return {
//     center: {
//       lat: region.location.latitude,
//       lon: region.location.longitude,
//     },
//     zoom,
//   };
// };
//
// const getUserLocationByGPS = () => new Promise((resolve, reject) =>
//   navigator.geolocation.getCurrentPosition(position => resolve({
//     center: {
//       lat: position.coords.latitude,
//       lon: position.coords.longitude,
//     },
//     zoom: 12,
//   }), () => reject(new Error('Failed to fetch user location'))));
//
// const getUserLocation = async () => {
//   let location = surflineLocation;
//   if (canUseNavigator) {
//     try {
//       location = await getUserLocationByGPS();
//     } catch (err) {
//       location = await getUserLocationByIP();
//     }
//   } else {
//     location = await getUserLocationByIP();
//   }
//
//   return location;
// };
//
//
// export const fetchLocationUrl = findLocation => async (dispatch) => {
//   dispatch({
//     type: FETCH_LOCATION_URL,
//   });
//   try {
//     let userLocation;
//     if (findLocation) userLocation = await getUserLocation();
//     const response = await fetchLocationMapView(null, userLocation);
//     const { url } = response;
//     dispatch({
//       type: FETCH_LOCATION_URL_SUCCESS,
//       url: `/surf-reports-forecasts-cams${url}`,
//     });
//   } catch (error) {
//     dispatch({
//       type: FETCH_LOCATION_URL_FAILURE,
//       error,
//     });
//   }
// };
