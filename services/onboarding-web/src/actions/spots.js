import { getOnboardingSpotsInRange } from '../common/api/spots';

export const FETCH_SPOTS = 'FETCH_SPOTS';
export const FETCH_SPOTS_SUCCESS = 'FETCH_SPOTS_SUCCESS';
export const FETCH_SPOTS_FAILURE = 'FETCH_SPOTS_FAILURE';

export const UPDATE_SEARCH_QUERY = 'UPDATE_SEARCH_QUERY';

export const fetchSpots = (reset, camsOnly = false) => async (dispatch, getState) => {
  const pageSize = 12;
  dispatch({ type: FETCH_SPOTS, spots: [] });
  try {
    const state = getState();
    const { query } = state.spots;
    const offset = reset ? 0 : state.spots.spots.length;
    const { spots } = await getOnboardingSpotsInRange(query, pageSize, offset, camsOnly);

    // Do not dispatch FETCH_SPOTS_SUCCESS if the query changes
    // before the fetch returns to avoid race condition
    if (query === getState().spots.query) {
      dispatch({
        type: FETCH_SPOTS_SUCCESS,
        spots,
        reset,
        end: spots.length < pageSize,
      });
    }
  } catch (error) {
    dispatch({
      type: FETCH_SPOTS_FAILURE,
      error: 'Unable to load spots',
    });
  }
};

export const updateSearchQuery = (query, camsOnly) => async (dispatch) => {
  dispatch({
    type: UPDATE_SEARCH_QUERY,
    query,
  });
  await dispatch(fetchSpots(true, camsOnly));
};
