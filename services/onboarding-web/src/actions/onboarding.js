import config from '../config';
import { fetchNearbyTaxonomies, fetchTaxonomy } from '../common/api/taxonomies';
import { getBatchSpots, getRecommendedSpots, getPopularSpots } from '../common/api/spots';

export const SET_ONBOARDING_BACKGROUND = 'SET_ONBOARDING_BACKGROUND';
export const REMOVE_ONBOARDING_BACKGROUND = 'REMOVE_ONBOARDING_BACKGROUND';

export const setOnboardingBackground = (backgroundUrl) => (dispatch) =>
  dispatch({
    type: SET_ONBOARDING_BACKGROUND,
    backgroundUrl,
  });

export const removeOnboardingBackground = () => (dispatch) =>
  dispatch({
    type: REMOVE_ONBOARDING_BACKGROUND,
  });

export const GET_NEARBY_TAXONOMIES = 'GET_NEARBY_TAXONOMIES';
export const GET_NEARBY_TAXONOMIES_SUCCESS = 'GET_NEARBY_TAXONOMIES_SUCCESS';
export const GET_NEARBY_TAXONOMIES_FAILURE = 'GET_NEARBY_TAXONOMIES_FAILURE';

export const getNearbyTaxonomies = (geoLocation) => async (dispatch) => {
  dispatch({
    type: GET_NEARBY_TAXONOMIES,
  });

  try {
    const { latitude, longitude } = geoLocation;
    const nearbyTaxonomies = await fetchNearbyTaxonomies(
      latitude,
      longitude,
      config.numberOfNearbyTaxonomies,
    );
    const taxonomies = nearbyTaxonomies.data;
    const spotsWithinTaxonomies = await getBatchSpots(
      taxonomies.map(({ _id }) => _id),
      'name',
    );
    const taxonomiesWithSpots = taxonomies.map((taxonomy, index) => ({
      ...taxonomy,
      spots: spotsWithinTaxonomies.data[index].spots,
    }));
    dispatch({
      type: GET_NEARBY_TAXONOMIES_SUCCESS,
      taxonomies: taxonomiesWithSpots,
    });
  } catch (error) {
    dispatch({
      type: GET_NEARBY_TAXONOMIES_FAILURE,
      error: 'Oops, something went wrong.',
    });
  }
};

export const GET_TAXONOMY = 'GET_TAXONOMY';
export const GET_TAXONOMY_SUCCESS = 'GET_TAXONOMY_SUCCESS';
export const GET_TAXONOMY_FAILURE = 'GET_TAXONOMY_FAILURE';
export const RESET_TAXONOMY_TYPE = 'RESET_TAXONOMY_TYPE';

const CONTINENT = 'continent';
const COUNTRY = 'country';
const REGION = 'region';

const HIERARCHY = [CONTINENT, COUNTRY, REGION];

export const getTaxonomy = (taxonomyId, type) => async (dispatch) => {
  const taxonomyType = type.toLowerCase();

  if (taxonomyId) {
    dispatch({
      type: GET_TAXONOMY,
      taxonomyType,
    });
  }

  try {
    const currentTypeIndex = HIERARCHY.findIndex((item) => item === type);
    const typesToReset = HIERARCHY.slice(currentTypeIndex);
    typesToReset.forEach((typeToReset) =>
      dispatch({
        type: RESET_TAXONOMY_TYPE,
        taxonomyType: typeToReset,
      }),
    );

    if (taxonomyId) {
      const depth = 0;
      const data = await fetchTaxonomy(taxonomyId, depth);
      let contains = data.contains.filter((point) => point.type === 'geoname') || [];
      if (type === REGION) {
        const spotsWithinTaxonomies = await getBatchSpots(
          contains.map(({ _id }) => _id),
          'name',
        );
        contains = contains.map((taxonomy, index) => ({
          ...taxonomy,
          spots: spotsWithinTaxonomies.data[index].spots,
        }));
      }
      dispatch({
        type: GET_TAXONOMY_SUCCESS,
        taxonomyType,
        data: contains,
        taxonomyId,
      });
    }
  } catch (error) {
    dispatch({
      type: GET_TAXONOMY_FAILURE,
      taxonomyType,
      error: 'Oops, something went wrong.',
    });
  }
};

export const GET_RECOMMENDED_SPOTS = 'GET_RECOMMENDED_SPOTS';
export const GET_RECOMMENDED_SPOTS_SUCCESS = 'GET_RECOMMENDED_SPOTS_SUCCESS';
export const GET_RECOMMENDED_SPOTS_FAILURE = 'GET_RECOMMENDED_SPOTS_FAILURE';

/**
 * @description Action to fetch the recommended spots based on the user's selected ability
 * level, location, and distance they are willing to travel
 * @param {"BEGINNER"|"INTERMEDIATE"|"ADVANCED"} abilityLevel
 * @param {Object} geoLocation
 * @param {number} geoLocation.latitude
 * @param {number} geoLocation.longitude
 * @param {number} travelDistance
 */
export const fetchRecommendedSpots = (abilityLevel, geoLocation, travelDistance) => async (
  dispatch,
  getState,
) => {
  const limit = 12;
  dispatch({
    type: GET_RECOMMENDED_SPOTS,
  });

  try {
    const state = getState();
    const offset = state.onboarding.recommended.spots.length;

    const response = await getRecommendedSpots(
      abilityLevel,
      geoLocation.latitude,
      geoLocation.longitude,
      travelDistance,
      limit,
      offset,
    );

    const { spots } = response.data;

    return dispatch({
      type: GET_RECOMMENDED_SPOTS_SUCCESS,
      spots,
      end: spots.length < limit,
      abilityLevel,
      travelDistance,
    });
  } catch (e) {
    return dispatch({
      type: GET_RECOMMENDED_SPOTS_FAILURE,
      error: 'Oops, something went wrong.',
    });
  }
};

export const GET_POPULAR_SPOTS = 'GET_POPULAR_SPOTS';
export const GET_POPULAR_SPOTS_SUCCESS = 'GET_POPULAR_SPOTS_SUCCESS';
export const GET_POPULAR_SPOTS_FAILURE = 'GET_POPULAR_SPOTS_FAILURE';

export const fetchPopularSpots = () => async (dispatch) => {
  dispatch({
    type: GET_POPULAR_SPOTS,
  });

  try {
    const response = await getPopularSpots();
    const { spots } = response.data;

    return dispatch({
      type: GET_POPULAR_SPOTS_SUCCESS,
      spots,
    });
  } catch (e) {
    return dispatch({
      type: GET_POPULAR_SPOTS_FAILURE,
      error: 'Oops, something went wrong.',
    });
  }
};

export const GET_SPOTS_WITHIN_CHOSEN_AREAS = 'GET_SPOTS_WITHIN_CHOSEN_AREAS';
export const GET_SPOTS_WITHIN_CHOSEN_AREAS_SUCCESS = 'GET_SPOTS_WITHIN_CHOSEN_AREAS_SUCCESS';
export const GET_SPOTS_WITHIN_CHOSEN_AREAS_FAILURE = 'GET_SPOTS_WITHIN_CHOSEN_AREAS_FAILURE';

/**
 * @description Uses the users selected taxonomies (counties) to fetch spots within
 * the given areas
 *
 * @param {Array<String>} selectedTaxonomies Taxonomies that were fetched based on user location
 */
export const fetchSpotsWithinChosenAreas = (selectedTaxonomies) => async (dispatch) => {
  dispatch({
    type: GET_SPOTS_WITHIN_CHOSEN_AREAS,
  });

  try {
    const response = await getBatchSpots(selectedTaxonomies, 'thumbnail,cameras');
    const spots = response.data.reduce((acc, curr) => acc.concat(curr.spots), []);

    return dispatch({
      type: GET_SPOTS_WITHIN_CHOSEN_AREAS_SUCCESS,
      spots,
      selectedTaxonomies,
    });
  } catch (e) {
    return dispatch({
      type: GET_SPOTS_WITHIN_CHOSEN_AREAS_FAILURE,
      error: 'Oops, something went wrong.',
    });
  }
};

export const FAVORITED_POPULAR_SPOT = 'FAVORITED_POPULAR_SPOT';
export const FAVORITED_WITHIN_AREA_SPOT = 'FAVORITED_WITHIN_AREA_SPOT';
export const FAVORITED_RECOMMENDED_SPOT = 'FAVORITED_RECOMMENDED_SPOT';
export const FAVORITED_SEARCH_SPOT = 'FAVORITED_SEARCH_SPOT';

export const UNFAVORITED_POPULAR_SPOT = 'UNFAVORITED_POPULAR_SPOT';
export const UNFAVORITED_WITHIN_AREA_SPOT = 'UNFAVORITED_WITHIN_AREA_SPOT';
export const UNFAVORITED_RECOMMENDED_SPOT = 'UNFAVORITED_RECOMMENDED_SPOT';
export const UNFAVORITED_SEARCH_SPOT = 'UNFAVORITED_SEARCH_SPOT';

export const favoritedPopularSpot = (spotName) => (dispatch) =>
  dispatch({
    type: FAVORITED_POPULAR_SPOT,
    spotName,
  });

export const favoritedWithinAreaSpot = () => (dispatch) =>
  dispatch({
    type: FAVORITED_WITHIN_AREA_SPOT,
  });

export const favoritedRecommendedSpot = () => (dispatch) =>
  dispatch({
    type: FAVORITED_RECOMMENDED_SPOT,
  });

export const favoritedSearchSpot = () => (dispatch) =>
  dispatch({
    type: FAVORITED_SEARCH_SPOT,
  });

export const unfavoritedPopularSpot = (spotName) => (dispatch) =>
  dispatch({
    type: UNFAVORITED_POPULAR_SPOT,
    spotName,
  });

export const unfavoritedWithinAreaSpot = () => (dispatch) =>
  dispatch({
    type: UNFAVORITED_WITHIN_AREA_SPOT,
  });

export const unfavoritedRecommendedSpot = () => (dispatch) =>
  dispatch({
    type: UNFAVORITED_RECOMMENDED_SPOT,
  });

export const unfavoritedSearchSpot = () => (dispatch) =>
  dispatch({
    type: UNFAVORITED_SEARCH_SPOT,
  });
