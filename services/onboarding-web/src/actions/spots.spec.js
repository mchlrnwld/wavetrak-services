import { expect } from 'chai';
import sinon from 'sinon';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as spots from '../common/api/spots';
import { fetchSpots, FETCH_SPOTS, FETCH_SPOTS_SUCCESS, FETCH_SPOTS_FAILURE } from './spots';

describe('Actions / spots', () => {
  describe('fetchSpots', () => {
    let mockStore;

    before(() => {
      mockStore = configureStore([thunk]);
    });

    beforeEach(() => {
      sinon.stub(spots, 'getOnboardingSpotsInRange');
    });

    afterEach(() => {
      spots.getOnboardingSpotsInRange.restore();
    });

    it('gets spots for store', async () => {
      const store = mockStore({
        spots: {
          spots: [{ id: 'old camera id', name: 'Old Spot', favorited: false }],
          query: 'hb pier',
        },
      });
      expect(store.getActions()).to.be.empty();

      spots.getOnboardingSpotsInRange.resolves({
        spots: [
          { id: 'camera one id', name: 'Spot One', favorited: true },
          { id: 'camera two id', name: 'Spot Two', favorited: false },
        ],
      });

      const expectedActions = [
        {
          type: FETCH_SPOTS,
          spots: [],
        },
        {
          type: FETCH_SPOTS_SUCCESS,
          spots: [
            { id: 'camera one id', name: 'Spot One', favorited: true },
            { id: 'camera two id', name: 'Spot Two', favorited: false },
          ],
          reset: true,
          end: true,
        },
      ];
      await store.dispatch(fetchSpots(true));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(spots.getOnboardingSpotsInRange).to.have.been.calledOnce();
      expect(spots.getOnboardingSpotsInRange).to.have.been.calledWithExactly(
        'hb pier',
        12,
        0,
        false,
      );
    });

    it('paginates request if reset is false', async () => {
      const store = mockStore({
        spots: {
          spots: [{ id: 'old camera id', name: 'Old Spot', favorited: false }],
          query: 'hb pier',
        },
      });
      expect(store.getActions()).to.be.empty();

      spots.getOnboardingSpotsInRange.resolves({
        spots: [
          { id: 'camera one id', name: 'Spot One', favorited: true },
          { id: 'camera two id', name: 'Spot Two', favorited: false },
        ],
      });

      const expectedActions = [
        {
          type: FETCH_SPOTS,
          spots: [],
        },
        {
          type: FETCH_SPOTS_SUCCESS,
          spots: [
            { id: 'camera one id', name: 'Spot One', favorited: true },
            { id: 'camera two id', name: 'Spot Two', favorited: false },
          ],
          reset: false,
          end: true,
        },
      ];
      await store.dispatch(fetchSpots(false));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(spots.getOnboardingSpotsInRange).to.have.been.calledOnce();
      expect(spots.getOnboardingSpotsInRange).to.have.been.calledWithExactly(
        'hb pier',
        12,
        1,
        false,
      );
    });

    it('continues infinite scroll if returned spot length is not less than page size', async () => {
      const store = mockStore({
        spots: {
          spots: [{ id: 'old camera id', name: 'Old Spot', favorited: false }],
          query: 'hb pier',
        },
      });
      expect(store.getActions()).to.be.empty();

      const newSpots = [
        { id: 'camera one id', name: 'Spot One', favorited: true },
        { id: 'camera two id', name: 'Spot Two', favorited: false },
        { id: 'camera three id', name: 'Spot Three', favorited: false },
        { id: 'camera four id', name: 'Spot Four', favorited: false },
        { id: 'camera five id', name: 'Spot Five', favorited: false },
        { id: 'camera six id', name: 'Spot Six', favorited: false },
        { id: 'camera seven id', name: 'Spot Seven', favorited: false },
        { id: 'camera eight id', name: 'Spot Eight', favorited: false },
        { id: 'camera nine id', name: 'Spot Nine', favorited: false },
        { id: 'camera ten id', name: 'Spot Ten', favorited: false },
        { id: 'camera eleven id', name: 'Spot Eleven', favorited: false },
        { id: 'camera twelve id', name: 'Spot Twelve', favorited: false },
      ];

      spots.getOnboardingSpotsInRange.resolves({ spots: newSpots });

      const expectedActions = [
        {
          type: FETCH_SPOTS,
          spots: [],
        },
        {
          type: FETCH_SPOTS_SUCCESS,
          spots: newSpots,
          reset: false,
          end: false,
        },
      ];
      await store.dispatch(fetchSpots(false));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(spots.getOnboardingSpotsInRange).to.have.been.calledOnce();
      expect(spots.getOnboardingSpotsInRange).to.have.been.calledWithExactly(
        'hb pier',
        12,
        1,
        false,
      );
    });

    it('errors on failure', async () => {
      const store = mockStore({
        spots: {
          query: null,
          spots: [],
        },
      });
      expect(store.getActions()).to.be.empty();
      spots.getOnboardingSpotsInRange.rejects('Internal error not to be exposed to user');
      const expectedActions = [
        {
          type: FETCH_SPOTS,
          spots: [],
        },
        {
          type: FETCH_SPOTS_FAILURE,
          error: 'Unable to load spots',
        },
      ];

      await store.dispatch(fetchSpots(true));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(spots.getOnboardingSpotsInRange).to.have.been.calledOnce();
      expect(spots.getOnboardingSpotsInRange).to.have.been.calledWithExactly(null, 12, 0, false);
    });
  });
});
