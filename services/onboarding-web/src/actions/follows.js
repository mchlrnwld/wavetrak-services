export const SET_FOLLOWS = 'SET_FOLLOWS';

export const setFollows = (follows) => async (dispatch) => {
  dispatch({
    type: SET_FOLLOWS,
    follows,
  });
};
