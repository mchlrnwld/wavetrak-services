import * as favoritesApi from '../common/api/favorites';

export const FETCH_FAVORITES = 'FETCH_FAVORITES';
export const FETCH_FAVORITES_SUCCESS = 'FETCH_FAVORITES_SUCCESS';
export const FETCH_FAVORITES_FAILURE = 'FETCH_FAVORITES_FAILURE';

export const fetchFavorites = () => async (dispatch) => {
  dispatch({ type: FETCH_FAVORITES });
  try {
    const { favorites } = await favoritesApi.fetchFavorites();
    dispatch({
      type: FETCH_FAVORITES_SUCCESS,
      favorites,
    });
  } catch (error) {
    dispatch({
      type: FETCH_FAVORITES_FAILURE,
      error,
    });
  }
};

export const SET_FAVORITED_SPOT = 'SET_FAVORITED_SPOT';
export const SET_FAVORITED_SPOT_SUCCESS = 'SET_FAVORITED_SPOT_SUCCESS';
export const SET_FAVORITED_SPOT_FAILURE = 'SET_FAVORITED_SPOT_FAILURE';

export const toggleFavoriteSpot = (spotId, isFavorite) => async (dispatch) => {
  dispatch({ type: SET_FAVORITED_SPOT });
  try {
    if (isFavorite) {
      await favoritesApi.deleteFavorites([spotId]);
    } else {
      await favoritesApi.saveFavorites([spotId]);
    }
    dispatch({
      type: SET_FAVORITED_SPOT_SUCCESS,
      spotId,
      isFavorite: !isFavorite,
    });
  } catch (err) {
    dispatch({
      type: SET_FAVORITED_SPOT_FAILURE,
      error: err,
    });
  }
};
