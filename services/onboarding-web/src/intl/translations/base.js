export default {
  FavoriteSurfSpots: {
    header: {
      title: 'Add your favorites',
      message:
        'Quickly access the spots you care most about from anywhere on the site. Favorites also help us deliver personalized forecast and news content to your feed based on the regions you’re interested in. Be sure to add a location featuring a camera to activate your Homecam.',
      skip: {
        linkText: 'Skip for now',
        afterText: "I'll add my favorites later.",
      },
    },
    submit: {
      text: 'Next',
    },
  },
  ToggleSpotSearchType: {
    all: 'All Spots',
    cams: 'Spots with Cams',
  },
  AddFavoriteSurfSpots: {
    submit: {
      text: 'Done',
    },
  },
  FavoriteSpotsGrid: {
    altText: 'Loading...',
  },
};
