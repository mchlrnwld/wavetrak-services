import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _once from 'lodash/once';
import { connect } from 'react-redux';
import { Spinner, Alert } from '@surfline/quiver-react';

import { fetchSpots, updateSearchQuery } from '../../../actions/spots';
import { toggleFavoriteSpot } from '../../../actions/favorites';
import FavoriteSpotSearch from '../../../components/FavoriteSpotSearch';
import FavoriteSpotsGrid from '../../../components/FavoriteSpotsGrid';
import { spotsPropType, favoritesPropType, geoPropType } from '../../../propTypes';
import {
  fetchRecommendedSpots,
  favoritedRecommendedSpot,
  unfavoritedSearchSpot,
  favoritedSearchSpot,
  favoritedPopularSpot,
  unfavoritedPopularSpot,
  favoritedWithinAreaSpot,
  unfavoritedWithinAreaSpot,
  unfavoritedRecommendedSpot,
} from '../../../actions/onboarding';
import { TRAVEL_FORM_KEY, SKILL_LEVEL_FORM_KEY, FAVORITE_FORM_KEY } from '../Step/constants';
import Tabs from '../../../components/Tabs';

const RECOMMENDED = {
  value: 'recommended',
  label: 'Recommended',
  getter: 'recommended',
  noSpotsMessage: 'There are no recommended spots within your desired travel distance.',
};

const POPULAR = {
  value: 'popular',
  label: 'Popular',
  getter: 'popular',
};

const AREA = {
  value: 'area',
  label: 'Within your Areas',
  getter: 'withinAreas',
};

const SEARCH = {
  value: 'search',
  label: '',
  getter: 'spots',
};

const TABS = [RECOMMENDED, AREA, POPULAR];

class AddFavorites extends Component {
  constructor(props) {
    super(props);

    const recommendedSpots = props[RECOMMENDED.getter];
    const hasRecommendedSpots = recommendedSpots.dataLoaded && !!recommendedSpots.spots.length;
    const defaultTab = hasRecommendedSpots ? RECOMMENDED : AREA;
    this.state = {
      activeTab: defaultTab,
      isMounting: true,
    };
  }

  componentDidMount() {
    this.mountAfterTransitionCompletion();
  }

  /**
   * @description Favorite or unfavorite a spot when the spot card is clicked
   * @param {String} spotId
   * @param {Boolean} favorited True if the spot is already a favorite and should be unfavorited
   * @param {String} spotName
   *
   * @memberof AddFavorites
   */
  onClickFavorite = (spotId, favorited, spotName) => {
    const { activeTab } = this.state;
    const {
      updateFormState,
      onClickFavoriteSpot,
      doTrackFavoritedSearch,
      doTrackUnfavoritedSearch,
      doTrackFavoritedWithinAreas,
      doTrackUnfavoritedWithinAreas,
      doTrackFavoritedRecommended,
      doTrackUnfavoritedRecommended,
      doTrackFavoritedPopular,
      doTrackUnfavoritedPopular,
    } = this.props;
    const allowProceedOnSelect = _once(() => updateFormState(FAVORITE_FORM_KEY, true));

    onClickFavoriteSpot(spotId, favorited);
    allowProceedOnSelect();

    if (activeTab.value === SEARCH.value) {
      return favorited ? doTrackUnfavoritedSearch() : doTrackFavoritedSearch();
    }
    if (activeTab.value === POPULAR.value) {
      return favorited ? doTrackUnfavoritedPopular(spotName) : doTrackFavoritedPopular(spotName);
    }
    if (activeTab.value === AREA.value) {
      return favorited ? doTrackUnfavoritedWithinAreas() : doTrackFavoritedWithinAreas();
    }
    if (activeTab.value === RECOMMENDED.value) {
      return favorited ? doTrackUnfavoritedRecommended() : doTrackFavoritedRecommended();
    }
    return null;
  };

  setActiveTab = (activeTab) =>
    this.setState({
      activeTab,
    });

  /**
   * Depending on the active tab we want different functions to
   * fetch more data in the `InfiniteScroll` component
   *
   * @memberof AddFavorites
   */
  getFetchMoreSpots = () => {
    const { activeTab } = this.state;
    const { doFetchRecommended, geo, formState, doFetchSpots } = this.props;

    if (activeTab.value === RECOMMENDED.value) {
      const travelDistance = formState[TRAVEL_FORM_KEY].value;
      const abilityLevel = formState[SKILL_LEVEL_FORM_KEY].value;
      return () => doFetchRecommended(abilityLevel, geo.location, travelDistance);
    }
    if (activeTab.value === SEARCH.value) {
      return () => doFetchSpots(false);
    }
    return null;
  };

  /**
   * We want to wait for the transition to complete before fully mounting
   * this component because if not (depending on the scroll position) the
   * user may be place haflway down the page and not see the instructions
   *
   * @memberof AddFavorites
   */
  mountAfterTransitionCompletion = () =>
    setTimeout(
      () =>
        this.setState({
          isMounting: false,
        }),
      250,
    );

  /**
   * If the user begins to search or changes their search query
   * we want to ensure we activate the `SEARCH` tab
   *
   * @memberof AddFavorites
   */
  updateSearch = (query) => {
    const { doUpdateSearchQuery } = this.props;
    doUpdateSearchQuery(query);
    this.setActiveTab(SEARCH);
  };

  render() {
    const { activeTab, isMounting } = this.state;
    const { spots, favorites, [activeTab.getter]: tabData } = this.props;
    const isRecommendedTab = activeTab.value === RECOMMENDED.value;
    const shouldShowEmptyMessage = isRecommendedTab && tabData.dataLoaded && !tabData.spots.length;

    return (
      <div className="sl-onboarding-add-favorites">
        <FavoriteSpotSearch query={spots.query} onChangeQuery={this.updateSearch} />
        {isMounting ? (
          <Spinner />
        ) : (
          <>
            <Tabs
              setActiveTab={this.setActiveTab}
              activeTab={activeTab}
              className="sl-onboarding-add-favorites__tabs"
              tabs={TABS}
            />
            <div className="sl-onboarding-add-favorites__results">
              {!tabData.error ? (
                <FavoriteSpotsGrid
                  scrollableTarget="sl-onboarding-page-container"
                  onClickFavoriteSpot={this.onClickFavorite}
                  favorites={favorites.spots}
                  spots={tabData.spots}
                  fetchMoreSpots={this.getFetchMoreSpots()}
                  end={tabData.end}
                  emptyMessage={shouldShowEmptyMessage ? RECOMMENDED.noSpotsMessage : ''}
                />
              ) : (
                <Alert type="error">{tabData.error}</Alert>
              )}
            </div>
          </>
        )}
      </div>
    );
  }
}

AddFavorites.propTypes = {
  spots: spotsPropType.isRequired,
  favorites: favoritesPropType.isRequired,
  doUpdateSearchQuery: PropTypes.func.isRequired,
  onClickFavoriteSpot: PropTypes.func.isRequired,
  doFetchSpots: PropTypes.func.isRequired,
  doFetchRecommended: PropTypes.func.isRequired,
  updateFormState: PropTypes.func.isRequired,
  doTrackFavoritedSearch: PropTypes.func.isRequired,
  doTrackUnfavoritedSearch: PropTypes.func.isRequired,
  doTrackFavoritedWithinAreas: PropTypes.func.isRequired,
  doTrackUnfavoritedWithinAreas: PropTypes.func.isRequired,
  doTrackFavoritedRecommended: PropTypes.func.isRequired,
  doTrackUnfavoritedRecommended: PropTypes.func.isRequired,
  doTrackFavoritedPopular: PropTypes.func.isRequired,
  doTrackUnfavoritedPopular: PropTypes.func.isRequired,
  geo: geoPropType.isRequired,
  formState: PropTypes.shape({}).isRequired,
};

export default connect(null, (dispatch) => ({
  doFetchSpots: (reset) => dispatch(fetchSpots(reset, false)),
  doFetchRecommended: (abilityLevel, geoLocation, travelDistance) =>
    dispatch(fetchRecommendedSpots(abilityLevel, geoLocation, travelDistance)),
  onClickFavoriteSpot: (spotId, favorited) => dispatch(toggleFavoriteSpot(spotId, favorited)),
  doTrackFavoritedSearch: () => dispatch(favoritedSearchSpot()),
  doTrackUnfavoritedSearch: () => dispatch(unfavoritedSearchSpot()),
  doTrackFavoritedWithinAreas: () => dispatch(favoritedWithinAreaSpot()),
  doTrackUnfavoritedWithinAreas: () => dispatch(unfavoritedWithinAreaSpot()),
  doTrackFavoritedRecommended: () => dispatch(favoritedRecommendedSpot()),
  doTrackUnfavoritedRecommended: () => dispatch(unfavoritedRecommendedSpot()),
  doTrackFavoritedPopular: (spotName) => dispatch(favoritedPopularSpot(spotName)),
  doTrackUnfavoritedPopular: (spotName) => dispatch(unfavoritedPopularSpot(spotName)),
  doUpdateSearchQuery: (query) => dispatch(updateSearchQuery(query, false)),
}))(AddFavorites);
