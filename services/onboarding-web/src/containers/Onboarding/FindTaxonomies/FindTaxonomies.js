/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/interactive-supports-focus */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Chevron, Select, Alert } from '@surfline/quiver-react';
import classNames from 'classnames';
import { connect } from 'react-redux';

import { getTaxonomy } from '../../../actions/onboarding';
import Card from '../../../components/Card';
import RedirectError from '../../../components/RedirectError/RedirectError';

const CONTINENT = 'continent';
const COUNTRY = 'country';
const REGION = 'region';
const EARTH = 'earth';
const EARTH_TAXONOMY_ID = '58f7ed51dadb30820bb38782';

const DROPDOWNS = [
  {
    getter: EARTH,
    setter: CONTINENT,
  },
  {
    getter: CONTINENT,
    setter: COUNTRY,
  },
  {
    getter: COUNTRY,
    setter: REGION,
  },
];

class FindTaxonomies extends Component {
  constructor(props) {
    super(props);

    const {
      [CONTINENT]: { dataLoaded },
    } = this.props;
    this.state = {
      showTaxonomyFinder: dataLoaded,
    };
  }

  componentDidMount() {
    const { doFetchContinents, earth } = this.props;

    if (!earth.dataLoaded) {
      doFetchContinents();
    }
  }

  getTaxonomyFinderClass = (type, dataLoaded) =>
    classNames({
      'sl-taxonomy-finder-select': true,
      [`sl-taxonomy-finder-select--${type}`]: !!type,
      'sl-taxonomy-finder-select--disabled': !dataLoaded,
    });

  getRegionOptionsClass = (data) =>
    classNames({
      'sl-taxonomy-finder__options': true,
      'sl-taxonomy-finder__options--empty': !data.length,
    });

  toggleTaxonomyFinder = () => {
    const { showTaxonomyFinder } = this.state;
    this.setState({
      showTaxonomyFinder: !showTaxonomyFinder,
    });
  };

  render() {
    const { showTaxonomyFinder } = this.state;
    const {
      doFetchTaxonomy,
      region,
      updateFormState,
      formKey,
      removeValueFromForm,
      formState,
      isMulti,
      error,
    } = this.props;

    return (
      <div className="sl-taxonomy-finder-container">
        {error ? (
          <RedirectError redirectUrl="/surf-cams" />
        ) : (
          <div
            className="sl-taxonomy-finder_toggle"
            role="button"
            onClick={this.toggleTaxonomyFinder}
          >
            {"I don't see any areas of interest above "}
            <Chevron direction={showTaxonomyFinder ? 'down' : 'up'} />
          </div>
        )}
        {showTaxonomyFinder && (
          <>
            <div className="sl-taxonomy-finder">
              {DROPDOWNS.map(({ getter, setter }) => {
                const { [getter]: getterProps, [setter]: setterProps } = this.props;
                // Getter pulls options from one spot up in the hierarchy
                // Setter sets options one spot down in hierarchy
                const { data, dataLoaded } = getterProps;
                const { taxonomyId } = setterProps;
                return (
                  <div key={setter} className={this.getTaxonomyFinderClass(setter, dataLoaded)}>
                    <Select
                      id={setter}
                      select={{
                        disabled: !dataLoaded,
                        onChange: (event) => doFetchTaxonomy(event.target.value, setter),
                      }}
                      defaultValue={taxonomyId}
                      options={[
                        { value: '', text: `Select ${setter}` },
                        ...data.sort((item1, item2) => item1.text.localeCompare(item2.text)),
                      ]}
                    />
                  </div>
                );
              })}
            </div>
            {region.dataLoaded && (
              <div className={this.getRegionOptionsClass(region.data)}>
                {!region.data.length ? (
                  <Alert>There are no available subregions within the selected region.</Alert>
                ) : (
                  <span className="sl-taxonomy-finder__options_header">Results</span>
                )}
                {region.data.map((geoname) => {
                  const value = geoname._id;
                  const label = geoname.name;
                  const numSpots = geoname.spots ? geoname.spots.length : 0;
                  return (
                    <Card
                      key={value}
                      value={value}
                      label={label}
                      subtext={`${numSpots} ${numSpots > 1 ? 'spots' : 'spot'}`}
                      onClick={() => updateFormState(formKey, value)}
                      onUnselect={() => removeValueFromForm(formKey, value)}
                      isSelected={
                        isMulti && formState[formKey]
                          ? formState[formKey].value.indexOf(value) !== -1
                          : value === formState[formKey].value
                      }
                      formKey={formKey}
                    />
                  );
                })}
              </div>
            )}
          </>
        )}
      </div>
    );
  }
}

FindTaxonomies.propTypes = {
  doFetchContinents: PropTypes.func.isRequired,
  doFetchTaxonomy: PropTypes.func.isRequired,
  earth: PropTypes.shape({
    data: PropTypes.arrayOf(PropTypes.shape({})),
    dataLoaded: PropTypes.bool,
    loading: PropTypes.bool,
    taxonomyId: PropTypes.string,
    error: PropTypes.string,
  }).isRequired,
  region: PropTypes.shape({
    data: PropTypes.arrayOf(PropTypes.shape({})),
    dataLoaded: PropTypes.bool,
    loading: PropTypes.bool,
    error: PropTypes.string,
    taxonomyId: PropTypes.string,
  }).isRequired,
  updateFormState: PropTypes.func.isRequired,
  formKey: PropTypes.string.isRequired,
  removeValueFromForm: PropTypes.func.isRequired,
  formState: PropTypes.shape({}).isRequired,
  isMulti: PropTypes.bool.isRequired,
  error: PropTypes.string,
};

FindTaxonomies.defaultProps = {
  error: null,
};

export default connect(
  (state) => ({
    earth: state.onboarding.taxonomies.earth,
    continent: state.onboarding.taxonomies.continent,
    country: state.onboarding.taxonomies.country,
    region: state.onboarding.taxonomies.region,
  }),
  (dispatch) => ({
    doFetchContinents: () => dispatch(getTaxonomy(EARTH_TAXONOMY_ID, EARTH)),
    doFetchTaxonomy: (taxonomyId, type) => dispatch(getTaxonomy(taxonomyId, type)),
  }),
)(FindTaxonomies);
