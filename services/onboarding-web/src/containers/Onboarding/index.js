import React from 'react';
import Step from './Step';
import Steps from './Step/StepsConfig';

/**
 * This is just a fancy way to wrap the Step components so that we
 * can easily add them into the top level JSX in `Onboarding.js`
 * by mapping this export and creating a `Route`
 */
export default Steps.map((step, index) => ({
  path: step.path || `step-${index}`,
  step: step.step,
  form:
    step.content.questions &&
    step.content.questions.map((question) => ({
      required: question.required,
      isMulti: question.isMulti,
      formKey: question.formKey,
      value: question.isMulti ? [] : '',
      step: step.step,
    })),
  submitOnContinue: step.submitOnContinue,
  stepName: step.stepName,
  freeOnly: step.freeOnly,
  segmentProperties: step.segmentProperties,
  component: (routeProps, updateFormState, removeValueFromForm, formState) => (
    <Step
      {...routeProps}
      {...step}
      updateFormState={updateFormState}
      formState={formState}
      removeValueFromForm={removeValueFromForm}
    />
  ),
}));
