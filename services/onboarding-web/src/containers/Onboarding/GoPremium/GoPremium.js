import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PremiumBenefits from '../../../components/PremiumBenefits';
import { setOnboardingBackground, removeOnboardingBackground } from '../../../actions/onboarding';
import GoPremiumBackground from '../../../public/go-premium-background.jpg';

const GoPremium = ({ doSetBackground, doRemoveBackground }) => {
  useEffect(() => {
    // This effect will add the background image on mount, and remove it on unmount
    doSetBackground();
    return () => doRemoveBackground();
  }, [doRemoveBackground, doSetBackground]);

  return (
    <div className="sl-onboarding-go-premium">
      <PremiumBenefits />
    </div>
  );
};

GoPremium.propTypes = {
  doSetBackground: PropTypes.func.isRequired,
  doRemoveBackground: PropTypes.func.isRequired,
};

export default connect(null, (dispatch) => ({
  doSetBackground: () => dispatch(setOnboardingBackground(GoPremiumBackground)),
  doRemoveBackground: () => dispatch(removeOnboardingBackground()),
}))(GoPremium);
