import { expect } from 'chai';
import Steps from '.';
import StepsConfig from './Step/StepsConfig';

describe('selectors / Onboarding / Steps', () => {
  it('Should export steps as an Array with paths and component', () => {
    Steps.forEach((step) => {
      expect(step).to.have.property('path');
      expect(step).to.have.property('component');
      expect(step).to.have.property('step');
      expect(step).to.have.property('form');
    });
  });
  it('Should export the steps config in the proper format', () => {
    StepsConfig.forEach((step) => {
      expect(step).to.have.property('path');
      expect(step).to.have.property('content');
      expect(step).to.have.property('step');
      expect(step.content).to.haveOwnProperty('header');
      if (step.content.questions) {
        step.content.questions.forEach((question) => {
          expect(question).to.have.ownProperty('question');
          expect(question).to.have.ownProperty('getOptions');
          expect(question.getOptions).to.be.instanceOf(Function);
          expect(question).to.have.ownProperty('formKey');
        });
      }
    });
  });
});
