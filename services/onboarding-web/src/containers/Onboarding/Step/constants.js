import beginner from '../../../public/beginner.jpg';
import intermediate from '../../../public/intermediate.jpg';
import advanced from '../../../public/advanced.jpg';

export const FAVORITE_FORM_KEY = 'didFavorite';
export const SKILL_LEVEL_FORM_KEY = 'abilityLevel';
export const SKILL_LEVEL = 'What is your skill level?';
export const SKILL_LEVEL_OPTIONS = [
  { value: 'BEGINNER', label: 'Beginner', backgroundImage: beginner },
  {
    value: 'INTERMEDIATE',
    label: 'Intermediate',
    backgroundImage: intermediate,
  },
  { value: 'ADVANCED', label: 'Advanced', backgroundImage: advanced },
];

export const TAXONOMIES_FORM_KEY = 'taxonomies';
export const TRAVEL = 'How far do you normally travel to surf?';
export const TRAVEL_FORM_KEY = 'travelDistance';
export const TRAVEL_UNIT_BINS = {
  FT: ['0 - 10', '10 - 30', '30 - 90+'],
  M: ['0 - 15', '15 - 50', '50 - 150+'],
};
