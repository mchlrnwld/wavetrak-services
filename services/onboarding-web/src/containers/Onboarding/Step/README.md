### Step Config Instructions

Below is an example of a step config with all of the options being used

```javascript
  {
    path: '/onboarding/areas-of-interest',
    step: 2,
    mapStateToProps: state => ({
      taxonomies: state.onboarding.taxonomies,
      loading: state.onboarding.taxonomies.loading,
      geo: state.geo,
    }),
    fetches: [
      {
        fetch: (geoLocation, taxonomies) => {
          if (!taxonomies.taxonomies.length && !taxonomies.dataLoaded) {
            return getNearbyTaxonomies(geoLocation);
          }
          return null;
        },
        args: ['geo.location', 'taxonomies'],
      },
    ],
    content: {
      header: 'Choose some areas of interest',
      subheader:
        'We will curate forecasts, local content and lots of other cool things for all the different regions you select.',
      questions: [
        {
          question: null,
          required: true,
          getOptions: props =>
            props.taxonomies.taxonomies.map((taxonomy) => {
              const numSpots = taxonomy.spots ? taxonomy.spots.length : 0;
              return {
                id: taxonomy._id,
                geonameId: taxonomy.geonameId,
                label: taxonomy.name,
                value: taxonomy._id,
                subtext: `${numSpots} ${numSpots > 1 ? 'spots' : 'spot'}`,
              };
            }),
          isMulti: true,
          formKey: 'taxonomies',
          Child: FindTaxonomies,
        },
      ],
    },
  },
  ```

  Below is the description of each field along with whether or not it is required

  ```
    {
    path: Used to assign a path for the Route component (REQUIRED)
    step: Used to assign the step number for the given route (REQUIRED),
    mapStateToProps: Function used to connect the step and pick off any redux state needed,
    fetches: [
      {
        fetch [Function]: Function used to perform a given fetch on ComponentDidMount,
        args [Array<String>]: Used to pick off values from props and pass them as arguments,
      },
    ],
    content: {
      header: Header for the step,
      subheader: Subheader for the step,
      questions: [
        {
          question: String title for the question,
          required: Whether or not the field is required to proceed to next step,
          getOptions [Function(props) => Array<Object>]: function that receives props as an argument and returns an array of options,
          isMulti: whether or not the question can have multiple values selected,
          formKey: The string to be used as the key in the form object,
          Child: An optional child component that is rendered as a sibling to the question options,
        },
      ],
    },
  },