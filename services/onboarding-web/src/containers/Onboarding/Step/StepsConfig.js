import _get from 'lodash/get';
import _isEqual from 'lodash/isEqual';
import { trackEvent } from '@surfline/web-common';
import {
  getNearbyTaxonomies,
  fetchRecommendedSpots,
  fetchSpotsWithinChosenAreas,
  fetchPopularSpots,
} from '../../../actions/onboarding';
import FindTaxonomies from '../FindTaxonomies';
import { updateUserSettings } from '../../../common/api/user';
import {
  getRecommendedSpots,
  getSpotsWithinAreas,
  getPopularSpots,
  getTaxonomies,
  getTaxonomiesLoading,
  getTaxonomyError,
} from '../../../selectors/onboarding';
import AddFavorites from '../AddFavorites';
import { getGeo } from '../../../selectors/geo';
import { getUser } from '../../../selectors/user';
import GoPremium from '../GoPremium';
import oneToTen from '../../../public/1-10.jpg';
import tenToThirty from '../../../public/10-30.jpg';
import thirtyToNinety from '../../../public/30-90+.jpg';
import {
  TRAVEL_UNIT_BINS,
  SKILL_LEVEL,
  SKILL_LEVEL_OPTIONS,
  SKILL_LEVEL_FORM_KEY,
  TRAVEL,
  TRAVEL_FORM_KEY,
  TAXONOMIES_FORM_KEY,
  FAVORITE_FORM_KEY,
} from './constants';

const getTravelOptions = (props) => {
  const { geo, user } = props;
  const geoCountry = _get(geo, 'countryCode', null);
  const userUnits = _get(user, 'settings.swellHeight', null);
  const imperialMap = ['US', 'LR', 'MM', 'IE', 'GB'];
  const isImperial = imperialMap.indexOf(geoCountry) > -1;
  let units = isImperial ? 'FT' : 'M';
  if (userUnits) units = userUnits;
  const subtext = units === 'M' ? 'Kilometers' : 'Miles';
  return [
    { value: 10, label: TRAVEL_UNIT_BINS[units][0], subtext, backgroundImage: oneToTen },
    { value: 30, label: TRAVEL_UNIT_BINS[units][1], subtext, backgroundImage: tenToThirty },
    { value: 90, label: TRAVEL_UNIT_BINS[units][2], subtext, backgroundImage: thirtyToNinety },
  ];
};

export default [
  {
    path: '/onboarding/about-you',
    stepName: 'about-you',
    segmentName: 'Favorites Wizard',
    segmentProperties: {
      category: 'onboarding',
      group: 'favorites_wizard',
      screenName: 'about-you',
      pageName: 'Favorites Wizard Qestionnaire - Screen 1',
      platform: 'web',
    },
    step: 1,
    mapStateToProps: (state) => ({
      geo: getGeo(state),
      user: getUser(state),
    }),
    content: {
      header: 'Tell us about yourself',
      subheader:
        'By letting us know a bit about you, we can recommend spots that you might like to surf.',
      questions: [
        {
          required: true,
          question: SKILL_LEVEL,
          getOptions: () => SKILL_LEVEL_OPTIONS,
          formKey: SKILL_LEVEL_FORM_KEY,
        },
        {
          question: TRAVEL,
          required: true,
          getOptions: (props) => getTravelOptions(props),
          formKey: TRAVEL_FORM_KEY,
        },
      ],
    },
    submitOnContinue: (formState, props) => {
      const { dispatch, geo } = props;

      const skillLevel = formState[SKILL_LEVEL_FORM_KEY].value;
      const travelDistance = formState[TRAVEL_FORM_KEY].value;

      const settingsToUpdate = {
        [SKILL_LEVEL_FORM_KEY]: skillLevel,
        [TRAVEL_FORM_KEY]: travelDistance,
      };

      updateUserSettings(settingsToUpdate);
      return dispatch(fetchRecommendedSpots(skillLevel, geo.location, travelDistance));
    },
  },
  {
    path: '/onboarding/areas-of-interest',
    stepName: 'areas-of-interest',
    segmentName: 'Favorites Wizard',
    segmentProperties: {
      category: 'onboarding',
      screenName: 'areas-of-interest',
      group: 'favorites_wizard',
      pageName: 'Favorites Wizard Qestionnaire - Screen 2',
      platform: 'web',
    },
    step: 2,
    mapStateToProps: (state) => ({
      taxonomies: getTaxonomies(state),
      loading: getTaxonomiesLoading(state),
      geo: getGeo(state),
      error: getTaxonomyError(state),
    }),
    fetches: [
      {
        fetch: (geoLocation, taxonomies) => {
          if (!taxonomies.taxonomies.length && !taxonomies.dataLoaded) {
            return getNearbyTaxonomies(geoLocation);
          }
          return null;
        },
        args: ['geo.location', 'taxonomies'],
      },
    ],
    content: {
      header: 'Choose any regions of interest.',
      subheader: 'We will curate forecasts and local content for the regions you select',
      questions: [
        {
          question: null,
          required: true,
          getOptions: (props) =>
            props.taxonomies.taxonomies.map((taxonomy) => {
              const numSpots = taxonomy.spots ? taxonomy.spots.length : 0;
              return {
                id: taxonomy._id,
                geonameId: taxonomy.geonameId,
                label: taxonomy.name,
                value: taxonomy._id,
                subtext: `${numSpots} ${numSpots > 1 ? 'spots' : 'spot'}`,
              };
            }),
          isMulti: true,
          formKey: TAXONOMIES_FORM_KEY,
          Child: FindTaxonomies,
        },
      ],
    },
  },
  {
    path: '/onboarding/add-favorites',
    step: 3,
    stepName: 'add-favorites',
    segmentName: 'Favorites Wizard',
    segmentProperties: {
      category: 'onboarding',
      screenName: 'add-favorites',
      group: 'favorites_wizard',
      pageName: 'Favorites Wizard Spot Recommender',
      platform: 'web',
    },
    mapStateToProps: (state) => ({
      recommended: getRecommendedSpots(state),
      withinAreas: getSpotsWithinAreas(state),
      popular: getPopularSpots(state),
      geo: getGeo(state),
      favorites: state.favorites,
      spots: state.spots,
    }),
    fetches: [
      {
        fetch: (formState, geoLocation, recommended) => {
          if (!recommended.dataLoaded) {
            const abilityLevel = formState[SKILL_LEVEL_FORM_KEY].value;
            const travelDistance = formState[TRAVEL_FORM_KEY].value;
            return fetchRecommendedSpots(abilityLevel, geoLocation, travelDistance);
          }
          return null;
        },
        args: ['formState', 'geo.location', 'recommended'],
      },
      {
        fetch: (formState, withinAreas) => {
          const areasToQuery = formState[TAXONOMIES_FORM_KEY].value;
          const prevAreas = withinAreas.selectedTaxonomies;
          const areasChanged = !_isEqual(areasToQuery, prevAreas);

          if (!withinAreas.dataLoaded || areasChanged) {
            return fetchSpotsWithinChosenAreas(areasToQuery);
          }
          return null;
        },
        args: ['formState', 'withinAreas'],
      },
      {
        fetch: (popular) => {
          if (!popular.dataLoaded) {
            return fetchPopularSpots();
          }
          return null;
        },
        args: ['popular'],
      },
    ],
    content: {
      header:
        'Add favorites for quick access to cams and forecasts, and to receive storm updates for areas you care about.',
      questions: [
        {
          required: true,
          question: null,
          getOptions: () => [],
          formKey: FAVORITE_FORM_KEY,
        },
      ],
    },
    submitOnContinue: (formState, props) => {
      const { onboardingFavorites } = props;
      const { search, recommended, popular, withinAreas } = onboardingFavorites;
      const numberFavorites = search + recommended + popular + withinAreas;
      const properties = {
        platform: 'web',
        screenName: 'add-favorites',
        numberFavorites,
        numberSearch: search,
        numberRecommend: recommended,
        numberPopular: popular,
        numberRegion: withinAreas,
        spotsPopular: onboardingFavorites.popularSpotsFavorited,
      };
      return trackEvent('Selected Favorites', properties);
    },
    CustomComponent: AddFavorites,
  },
  {
    step: 4,
    path: '/onboarding/go-premium',
    stepName: 'go-premium',
    segmentName: 'Onboarding',
    segmentProperties: {
      screenName: 'go-premium',
      category: 'onboarding',
      group: 'onboarding',
      pageName: 'Onboarding Premium Funnel',
      platform: 'web',
    },
    content: {
      header: 'Surfline Premium',
      subheader: 'Score Better Waves, More Often',
    },
    CustomComponent: GoPremium,
    freeOnly: true,
  },
];
