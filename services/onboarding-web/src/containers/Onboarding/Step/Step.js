import React, { PureComponent } from 'react';
import { Spinner, Alert } from '@surfline/quiver-react';

import classNames from 'classnames';
import { connect } from 'react-redux';
import _get from 'lodash/get';
import PropTypes from 'prop-types';
import { trackNavigatedToPage } from '@surfline/web-common';
import OnboardingQuestion from '../../../components/OnboardingQuestion';
import Card from '../../../components/Card/Card';

/**
 * @description This component is a reusable component to render
 * any step in the entire onboarding. It should be as reusable and
 * extensible as possible. It will read in whatever content it is
 * given and generate a step in the onboarding
 * @param {function} updateFormState A function used to update the form state
 * in the top level container `Onboarding.js`
 * @param {object} formState The current formState values
 * @param {object} content The content object passed in from the config
 */
class Step extends PureComponent {
  componentDidMount() {
    const { fetches, dispatch, segmentProperties, segmentName } = this.props;
    // Perform any fetches for necessary data using the provided fetch and arguments in config
    if (fetches) {
      fetches.forEach(({ fetch, args = [] }) => {
        const dispatchFunc = fetch(...args.map((arg) => _get(this.props, arg)));
        // If the redux state or local state is already there we may not want to dispatch anything
        if (dispatchFunc) dispatch(dispatchFunc);
      });
    }

    trackNavigatedToPage(segmentName, segmentProperties);
  }

  cardContainerClass = (formKey) =>
    classNames({
      'sl-onboarding-card-container': true,
      [`sl-onboarding-card-container--${formKey}`]: true,
    });

  cardContainerErrorClass = (formKey) =>
    classNames({
      'sl-onboarding-card-container__error': true,
      [`sl-onboarding-card-container__error--${formKey}`]: true,
    });

  render() {
    const {
      updateFormState,
      removeValueFromForm,
      formState,
      content,
      loading,
      CustomComponent,
      error,
    } = this.props;
    const { questions, header, subheader } = content;

    return (
      <div className="sl-onboarding-step-container">
        <div className="sl-onboarding-step-header">{header}</div>
        {subheader && <div className="sl-onboarding-step-sub-header">{subheader}</div>}
        {questions &&
          questions.map(({ question, getOptions, formKey, isMulti, Child }) => (
            <div key={formKey} className="sl-onboarding-step-question-container">
              {question && <OnboardingQuestion question={question} />}
              {loading ? (
                <Spinner />
              ) : (
                <>
                  <div className={this.cardContainerClass(formKey)}>
                    {error && (
                      <div className={this.cardContainerErrorClass(formKey)}>
                        <Alert type="error">
                          {error}{' '}
                          <span className="sl-error-redirect">
                            Wait 10 seconds or click below to go back to Surfline.
                          </span>
                        </Alert>
                      </div>
                    )}
                    {getOptions(this.props).map(({ label, value, subtext, backgroundImage }) => (
                      <Card
                        key={value}
                        value={value}
                        label={label}
                        subtext={subtext}
                        onClick={() => updateFormState(formKey, value)}
                        onUnselect={() => removeValueFromForm(formKey, value)}
                        isSelected={
                          isMulti && formState[formKey]
                            ? formState[formKey].value.indexOf(value) !== -1
                            : value === formState[formKey].value
                        }
                        formKey={formKey}
                        backgroundImage={backgroundImage}
                      />
                    ))}
                  </div>
                  {Child && <Child {...this.props} formKey={formKey} isMulti={isMulti} />}
                </>
              )}
            </div>
          ))}
        {CustomComponent && <CustomComponent {...this.props} />}
      </div>
    );
  }
}

Step.propTypes = {
  content: PropTypes.shape({
    questions: PropTypes.arrayOf(
      PropTypes.shape({
        question: PropTypes.string,
        options: PropTypes.arrayOf(
          PropTypes.shape({
            value: PropTypes.string.isRequired,
            subtext: PropTypes.string,
          }),
        ),
        formKey: PropTypes.string,
      }),
    ),
    header: PropTypes.string,
    subheader: PropTypes.string,
  }).isRequired,
  fetches: PropTypes.arrayOf(
    PropTypes.shape({
      fetch: PropTypes.func.isRequired,
      args: PropTypes.arrayOf(PropTypes.string),
    }),
  ),
  loading: PropTypes.bool,
  updateFormState: PropTypes.func.isRequired,
  removeValueFromForm: PropTypes.func.isRequired,
  formState: PropTypes.shape({}).isRequired,
  CustomComponent: PropTypes.oneOfType([PropTypes.instanceOf(Object), PropTypes.node]),
  dispatch: PropTypes.func.isRequired,
  segmentProperties: PropTypes.shape({}),
  segmentName: PropTypes.string.isRequired,
  error: PropTypes.string,
};

Step.defaultProps = {
  fetches: null,
  loading: false,
  CustomComponent: null,
  error: null,
  segmentProperties: {},
};

export default connect((state, ownProps) => {
  const { mapStateToProps } = ownProps;
  if (!mapStateToProps) return {};
  return mapStateToProps(state);
})(Step);
