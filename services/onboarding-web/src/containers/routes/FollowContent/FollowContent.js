import React from 'react';
import { connect } from 'react-redux';

const FollowContent = () => <h1>Follow Content</h1>;

export default connect((state) => ({
  follows: state.favorites,
}))(FollowContent);
