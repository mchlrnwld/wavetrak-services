import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { trackNavigatedToPage } from '@surfline/web-common';
import FavoriteSpotsHeader from '../../../components/FavoriteSpotsHeader';
import FavoriteSpotSearch from '../../../components/FavoriteSpotSearch';
import ToggleSpotSearchType from '../../../components/ToggleSpotSearchType';
import FavoriteSpotsGrid from '../../../components/FavoriteSpotsGrid';
import FavoriteSpotsSubmit from '../../../components/FavoriteSpotsSubmit';
import { clearLocalStorage } from '../../../stores/localStorage';
import { toggleFavoriteSpot, fetchFavorites } from '../../../actions/favorites';
import { fetchSpots, updateSearchQuery } from '../../../actions/spots';
import { getPremiumStatus } from '../../../selectors/user';
import { getRedirectUrl } from '../../../selectors/router';
import { spotsPropType, favoritesPropType } from '../../../propTypes';
import config from '../../../config';
import en from '../../../intl/translations/en';

const basePath = '/setup/favorite-surf-spots';

class AddFavoriteSurfSpots extends Component {
  componentDidMount() {
    const {
      doFetchSpots,
      doFetchFavorites,
      isPremium,
      match: {
        params: { type },
      },
    } = this.props;

    const typeEnum = type && type.toUpperCase();
    if (typeEnum !== 'CAMS') {
      this.updatePath(basePath);
    }
    clearLocalStorage();
    trackNavigatedToPage('Add Favorites', {
      userType: isPremium ? 'sl_premium' : 'sl_basic',
      category: 'favorites',
      channel: 'favorites',
      subCategory: 'add favorites',
    });
    doFetchSpots(true);
    doFetchFavorites();
  }

  componentDidUpdate(prevProps) {
    const { match, doFetchSpots } = this.props;
    if (match.params.type !== prevProps.match.params.type) {
      doFetchSpots(true);
    }
  }

  handleSubmit = async () => {
    const { redirectUrl } = this.props;
    window.location.href = decodeURIComponent(redirectUrl || config.accountFavoritesUrl);
  };

  updatePath = (path) => {
    const {
      location: { search },
      history,
    } = this.props;
    history.push(`${path}${search}`);
  };

  toggleType = (newType) => {
    if (newType) {
      this.updatePath(`${basePath}/${newType.toLowerCase()}`);
    } else {
      this.updatePath(basePath);
    }
  };

  render() {
    const {
      match: {
        params: { type },
      },
      onClickFavoriteSpot,
      favorites,
      spots,
      doUpdateSearchQuery,
      doFetchSpots,
    } = this.props;

    return (
      <div className="sl-favorite-spots">
        <FavoriteSpotsHeader />
        <FavoriteSpotSearch query={spots.query} onChangeQuery={doUpdateSearchQuery} />
        <ToggleSpotSearchType type={type && type.toUpperCase()} onToggle={this.toggleType} />
        {spots && spots.spots ? (
          <FavoriteSpotsGrid
            onClickFavoriteSpot={onClickFavoriteSpot}
            favorites={favorites.spots}
            spots={spots.spots}
            fetchMoreSpots={() => doFetchSpots(false)}
            end={spots.end}
          />
        ) : null}
        <FavoriteSpotsSubmit
          error={favorites.error}
          loading={favorites.loading}
          disabled={favorites.loading}
          submitText={en.AddFavoriteSurfSpots.submit.text}
          onSubmit={this.handleSubmit}
        />
      </div>
    );
  }
}

AddFavoriteSurfSpots.propTypes = {
  history: PropTypes.shape({
    replace: PropTypes.func,
    push: PropTypes.func,
  }).isRequired,
  location: PropTypes.shape({
    search: PropTypes.string,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      type: PropTypes.string,
    }).isRequired,
  }).isRequired,
  isPremium: PropTypes.bool.isRequired,
  doFetchSpots: PropTypes.func.isRequired,
  doFetchFavorites: PropTypes.func.isRequired,
  spots: spotsPropType.isRequired,
  doUpdateSearchQuery: PropTypes.func.isRequired,
  favorites: favoritesPropType.isRequired,
  onClickFavoriteSpot: PropTypes.func.isRequired,
  redirectUrl: PropTypes.string,
};

AddFavoriteSurfSpots.defaultProps = {
  redirectUrl: config.accountFavoritesUrl,
};

export default withRouter(
  connect(
    (state) => ({
      user: state.user,
      isPremium: getPremiumStatus(state),
      favorites: state.favorites,
      spots: state.spots,
      redirectUrl: getRedirectUrl(state),
    }),
    (dispatch, ownProps) => {
      const { type } = ownProps.match.params;
      const camsOnly = type && type.toUpperCase() === 'CAMS';
      return {
        doFetchSpots: (reset) => dispatch(fetchSpots(reset, camsOnly)),
        doFetchFavorites: () => dispatch(fetchFavorites()),
        onClickFavoriteSpot: (spotId, favorited) => dispatch(toggleFavoriteSpot(spotId, favorited)),
        doUpdateSearchQuery: (query) => dispatch(updateSearchQuery(query, camsOnly)),
      };
    },
  )(AddFavoriteSurfSpots),
);
