import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { getPremiumStatus } from '../../selectors/user';

class OnboardingPageContainer extends Component {
  getClassName = () => {
    const { isPremium } = this.props;
    return classNames({
      'sl-onboarding-page-container': true,
      'sl-onboarding-page-container--is-premium': isPremium,
    });
  };

  render() {
    const { children, background } = this.props;

    const style = {
      backgroundImage: background ? `url(${background})` : '',
    };

    return (
      <div id="sl-onboarding-page-container" style={style} className={this.getClassName()}>
        {children}
      </div>
    );
  }
}

OnboardingPageContainer.propTypes = {
  children: PropTypes.node,
  isPremium: PropTypes.bool,
  background: PropTypes.string,
};

OnboardingPageContainer.defaultProps = {
  children: null,
  isPremium: false,
  background: '',
};

export default connect((state) => ({
  isPremium: getPremiumStatus(state),
  background: state.onboarding.background,
}))(OnboardingPageContainer);
