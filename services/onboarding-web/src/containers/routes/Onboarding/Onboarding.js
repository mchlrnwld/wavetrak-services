import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import _flatten from 'lodash/flatten';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { ContentContainer } from '@surfline/quiver-react';
import { Route, Switch, Redirect } from 'react-router';
import classNames from 'classnames';
import OnboardingSteps from '../../Onboarding';
import StepProgressBar from '../../../components/StepProgressBar';
import SurflineWaterdrop from '../../../components/Icons/SurflineWaterdrop';
import OnboardingFooter from '../../../components/OnboardingFooter/OnboardingFooter';

const LEFT = 'left';
const RIGHT = 'right';
const ENTER = 'enter';
const EXIT = 'exit';
const FORWARD = 'forward';
const BACKWARD = 'backward';
const ADD = 'add';
const REMOVE = 'remove';

const arrayToObject = (array) =>
  array.reduce((obj, item) => ({ ...obj, [item.formKey]: item }), {});

/**
 * Top level Onboarding V2 Component that holds all the form state and logic for
 * page transitions. Essentially the "brains" of the new onboarding experience
 *
 * @class Onboarding
 * @extends {PureComponent}
 */
class Onboarding extends PureComponent {
  /**
   * @description This lifecycle hook is KEY to the transitions between the different pages
   * in the onboarding. The original problem was that we could not update `history` and local
   * state at the same time. Updating the history would trigger an asynchronous state change.
   * As a result we had no way of ensuring consistency between the local state `state.step` and
   * `state.prevStep` and the pathname in `history`. This was causing `this.getTransitionClassName`
   * to have different state and props between two calls that are made in the transition.
   *
   * The fix was to rely on copying the react router `location` object to local state and use the
   * local state as the source of truth for the `<Switch>` and `<CSSTransition/>`. We then rely on
   * `getDerivedStateFromProps` to reconcile this state in between each prop update and render.
   *
   * NOTE: `getDerivedStateFromProps` is called right before render.
   * https://reactjs.org/docs/react-component.html#static-getderivedstatefromprops
   *
   * @static
   * @param {*} props incoming props
   * @param {*} state previous state
   * @returns
   * @memberof Onboarding
   */
  static getDerivedStateFromProps(props, state) {
    // React router doesn't initialize location state on first render
    const propsLocationState = props.location.state || state.location.state;
    const pathChanged = props.location.pathname !== state.location.pathname;
    const { step, prevStep } = propsLocationState;
    const stepChanged = step && step !== state.location.state.step;

    // Handle router changes through `history.push`
    if (stepChanged) {
      return {
        step,
        prevStep: state.location.state ? state.location.state.step : prevStep,
        location: props.location,
      };
    }
    // Handle back/forward button nav
    if (pathChanged) {
      const path = props.location.pathname;
      const { step: newStep } = OnboardingSteps.find((stp) => stp.path === path);
      return {
        step: newStep,
        prevStep: step,
        location: {
          ...props.location,
          state: {
            step: newStep,
            prevStep: step,
          },
        },
      };
    }
    return null;
  }

  constructor(props) {
    super(props);
    const { location, premium } = props;

    this.OnboardingSteps = premium
      ? OnboardingSteps.filter(({ freeOnly }) => !freeOnly)
      : OnboardingSteps;

    const currentStep = this.OnboardingSteps.find((stp) => stp.path === location.pathname);
    const step = currentStep ? currentStep.step : 1;
    const defaultForm = arrayToObject(
      _flatten(this.OnboardingSteps.map((stepItem) => stepItem.form)).filter(
        (stepItem) => stepItem && !!stepItem.formKey,
      ),
    );

    this.state = {
      prevStep: step,
      step,
      form: defaultForm,
      location: {
        ...props.location,
        state: {
          step,
          prevStep: step,
        },
      },
    };
  }

  componentDidMount() {
    const { doFetchFavorites } = this.props;

    doFetchFavorites();
  }

  getTransitionClassName = (action, type) => {
    const { prevStep, step } = this.state;
    const direction = prevStep <= step ? RIGHT : LEFT;
    return classNames({
      [`sl-route-transition-enter${type ? `-${type}` : ''}--left`]:
        action === ENTER && direction === LEFT,
      [`sl-route-transition-enter${type ? `-${type}` : ''}--right`]:
        action === ENTER && direction === RIGHT,
      [`sl-route-transition-exit${type ? `-${type}` : ''}--right`]:
        action === EXIT && direction === LEFT,
      [`sl-route-transition-exit${type ? `-${type}` : ''}--left`]:
        action === EXIT && direction === RIGHT,
    });
  };

  getOnboardingClassName = (step) =>
    classNames({
      'sl-onboarding': true,
      [`sl-onboarding--${step.stepName}`]: !!step.stepName,
    });

  changeStep = (direction = FORWARD) => {
    const { history } = this.props;
    const { step } = this.state;
    const newStep = direction === FORWARD ? step + 1 : step - 1;
    const nextStep = this.OnboardingSteps.find((stp) => stp.step === newStep);

    if (nextStep) {
      history.push({
        pathname: nextStep.path,
        state: {
          step: newStep,
          prevStep: step,
        },
      });
    }
  };

  submitStep = () => {
    const { step, form } = this.state;
    const { submitOnContinue } = this.OnboardingSteps.find((stp) => stp.step === step);
    if (submitOnContinue) {
      submitOnContinue(form, this.props);
    }
    this.changeStep(FORWARD);
  };

  /**
   * @description generic function to perform actions on the form state
   * @param {"ADD"|"REMOVE"} action What action to perform on the form state
   * @memberof Onboarding
   */
  editFormValues = (action) => (formKey, value) => {
    const { form } = this.state;
    const currentState = form[formKey];
    const currentValue = currentState.value;
    const { isMulti } = currentState;
    let nextValue;
    if (action === ADD) {
      if (isMulti) nextValue = currentValue.concat([value]);
      if (!isMulti) nextValue = value;
    }
    if (action === REMOVE) {
      if (isMulti) nextValue = currentValue.filter((val) => val !== value);
      if (!isMulti) nextValue = '';
    }

    return this.setState({
      form: {
        ...form,
        [formKey]: {
          ...currentState,
          value: nextValue,
        },
      },
    });
  };

  canProceedToNextStep = () => {
    const { form, step } = this.state;

    // If there is a required item that is empty, return false
    const canProceed = Object.keys(form).every((key) => {
      const item = form[key];
      if (item.required && item.step === step) {
        if ((item.isMulti && !item.value.length) || !item.value) {
          return false;
        }
      }
      return true;
    });

    return canProceed;
  };

  render() {
    const { step, form, location } = this.state;
    const { premium, history } = this.props;

    const classes = {
      enter: this.getTransitionClassName(ENTER),
      enterActive: this.getTransitionClassName(ENTER, 'active'),
      exit: this.getTransitionClassName(EXIT),
      exitActive: this.getTransitionClassName(EXIT, 'active'),
    };

    const currentStepObject = this.OnboardingSteps.find((stp) => stp.step === step);

    return (
      <div className={this.getOnboardingClassName(this.OnboardingSteps[step - 1])}>
        <div className="sl-logo">
          <SurflineWaterdrop />
        </div>
        <StepProgressBar
          step={step}
          numSteps={this.OnboardingSteps.length}
          steps={this.OnboardingSteps}
        />
        <ContentContainer>
          <div className="sl-onboarding-routes">
            <TransitionGroup>
              <CSSTransition key={location.pathname} classNames={classes} timeout={220}>
                <Switch location={location}>
                  <Route
                    path="/onboarding"
                    exact
                    render={() => <Redirect to={this.OnboardingSteps[0].path} />}
                  />
                  {this.OnboardingSteps.map(({ path, component }) => (
                    <Route
                      path={path}
                      key={path}
                      render={(routeProps) =>
                        component(
                          routeProps,
                          this.editFormValues(ADD),
                          this.editFormValues(REMOVE),
                          form,
                        )
                      }
                    />
                  ))}
                </Switch>
              </CSSTransition>
            </TransitionGroup>
          </div>
        </ContentContainer>
        <OnboardingFooter
          canProceed={this.canProceedToNextStep()}
          onContinue={() => this.submitStep()}
          step={step}
          numSteps={this.OnboardingSteps.length}
          premium={premium}
          onClickPrev={() => this.changeStep(BACKWARD)}
          segmentProperties={currentStepObject.segmentProperties}
          history={history}
        />
      </div>
    );
  }
}

Onboarding.propTypes = {
  location: PropTypes.shape({
    state: PropTypes.shape({}),
    key: PropTypes.string,
    pathname: PropTypes.string,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  premium: PropTypes.bool,
  doFetchFavorites: PropTypes.func.isRequired,
};

Onboarding.defaultProps = {
  premium: false,
};

export default Onboarding;
