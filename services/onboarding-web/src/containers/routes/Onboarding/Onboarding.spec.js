import { expect } from 'chai';
import sinon from 'sinon';
import { Button } from '@surfline/quiver-react';
import { Route } from 'react-router';
import { createMemoryHistory } from 'history';

import Onboarding from './Onboarding';
import OnboardingSteps from '../../Onboarding';
import OnboardingWithRouter from '.';
import Step from '../../Onboarding/Step';
import Card from '../../../components/Card';
import OnboardingFooter from '../../../components/OnboardingFooter';
import StepsConfig from '../../Onboarding/Step/StepsConfig';
import { mountWithReduxAndRouter } from '../../../utils/test-utils';
import * as userApi from '../../../common/api/user';
import * as onboardingActions from '../../../actions/onboarding';

describe('selectors / Onboarding', () => {
  const defaultState = {
    geo: {
      location: {},
    },
    user: {
      entitlements: [],
    },
    onboarding: {
      taxonomies: {
        loading: true,
        error: null,
        taxonomies: [],
        dataLoaded: false,
        earth: {
          loading: false,
          error: null,
          data: [],
          taxonomyId: null,
          dataLoaded: false,
        },
        continent: {
          loading: false,
          error: null,
          data: [],
          taxonomyId: null,
          dataLoaded: false,
        },
        country: {
          loading: false,
          error: null,
          data: [],
          taxonomyId: null,
          dataLoaded: false,
        },
        region: {
          loading: false,
          error: null,
          data: [],
          taxonomyId: null,
          dataLoaded: false,
        },
      },
    },
  };

  const defaultProps = {};

  before(() => {
    StepsConfig.forEach((stepConfig) => {
      if (stepConfig.fetches) {
        stepConfig.fetches.forEach((fetch) => {
          // eslint-disable-next-line
          fetch.fetch = sinon.spy();
        });
      }
    });
    sinon.stub(onboardingActions, 'fetchRecommendedSpots').returns({ type: 'stub' });
    sinon.stub(userApi, 'updateUserSettings').returns({});
  });

  it('Should render the proper step given the correct route', () => {
    const history = createMemoryHistory({
      initialEntries: [StepsConfig[0].path],
    });
    const { wrapper } = mountWithReduxAndRouter(OnboardingWithRouter, defaultProps, {
      initialState: defaultState,
      history,
    });
    const OnboardingInstance = wrapper.find(Onboarding);

    const RouteInstance = OnboardingInstance.find(Route);

    expect(RouteInstance).to.have.length(1);
    expect(RouteInstance.prop('path')).to.equal(`${StepsConfig[0].path}`);
  });

  it('Should render one step at a time', () => {
    const history = createMemoryHistory({
      initialEntries: [StepsConfig[0].path],
    });
    const { wrapper } = mountWithReduxAndRouter(OnboardingWithRouter, defaultProps, {
      initialState: defaultState,
      history,
    });
    const OnboardingInstance = wrapper.find(Onboarding);
    const StepInstances = OnboardingInstance.find(Step);
    expect(StepInstances).to.have.length(1);
  });

  it('Should update the state when selecting a card', () => {
    const history = createMemoryHistory({
      initialEntries: [StepsConfig[0].path],
    });
    const { wrapper } = mountWithReduxAndRouter(OnboardingWithRouter, defaultProps, {
      initialState: defaultState,
      history,
    });
    const OnboardingInstance = wrapper.find(Onboarding);
    const CardInstance = OnboardingInstance.find(Card);
    const formKey = CardInstance.at(0).prop('formKey');
    const value = CardInstance.at(0).prop('value');
    CardInstance.at(0).simulate('click');

    expect(OnboardingInstance.state('form')).to.have.ownProperty(formKey);
    expect(OnboardingInstance.state('form')[formKey].value).to.equal(value);
  });

  it('Clicking the continue button should increment the step if the required fields are filled out', () => {
    const history = createMemoryHistory({
      initialEntries: [StepsConfig[0].path],
    });
    const { wrapper } = mountWithReduxAndRouter(OnboardingWithRouter, defaultProps, {
      initialState: defaultState,
      history,
    });
    const OnboardingInstance = wrapper.find(Onboarding);
    const OnboardingFooterInstance = OnboardingInstance.find(OnboardingFooter);
    const ButtonInstance = OnboardingFooterInstance.find(Button);
    expect(OnboardingInstance.state('step')).to.be.equal(1);
    ButtonInstance.simulate('click');
    expect(OnboardingInstance.state('step')).to.be.equal(1);

    const CardInstance = OnboardingInstance.find(Card);
    CardInstance.at(0).simulate('click');
    CardInstance.last().simulate('click');
    ButtonInstance.simulate('click');
    expect(OnboardingInstance.state('step')).to.be.equal(2);
  });

  it('Should create the default form state based on the Onboarding Steps', () => {
    const history = createMemoryHistory({
      initialEntries: [StepsConfig[0].path],
    });
    const { wrapper } = mountWithReduxAndRouter(OnboardingWithRouter, defaultProps, {
      initialState: defaultState,
      history,
    });

    const OnboardingInstance = wrapper.find(Onboarding);
    OnboardingSteps.forEach((stepItem) => {
      if (stepItem.form) {
        stepItem.form.forEach((formItem) => {
          expect(OnboardingInstance.state('form')[formItem.formKey]).to.deep.equal({
            required: formItem.required,
            isMulti: formItem.isMulti,
            value: formItem.value,
            step: formItem.step,
            formKey: formItem.formKey,
          });
        });
      }
    });
  });

  it('Should handle route changes and update the state', () => {
    const history = createMemoryHistory({
      initialEntries: [StepsConfig[0].path],
    });
    const { wrapper } = mountWithReduxAndRouter(OnboardingWithRouter, defaultProps, {
      initialState: defaultState,
      history,
    });
    const OnboardingInstance = wrapper.find(Onboarding);
    history.push(StepsConfig[1].path);
    OnboardingInstance.update();
    expect(OnboardingInstance.state('step')).to.be.equal(2);
    expect(OnboardingInstance.state('prevStep')).to.be.equal(1);
    expect(OnboardingInstance.state('location').state.step).to.be.equal(2);
    expect(OnboardingInstance.state('location').state.prevStep).to.be.equal(1);
  });
});
