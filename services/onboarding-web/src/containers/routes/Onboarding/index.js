import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import Onboarding from './Onboarding';
import { getPremiumStatus } from '../../../selectors/user';
import { getOnboardingFavorites } from '../../../selectors/onboarding';
import { fetchFavorites } from '../../../actions/favorites';

export default withRouter(
  connect(
    (state) => ({
      geo: state.geo,
      premium: getPremiumStatus(state),
      onboardingFavorites: getOnboardingFavorites(state),
    }),
    (dispatch) => ({
      dispatch,
      doFetchFavorites: () => dispatch(fetchFavorites()),
    }),
  )(Onboarding),
);
