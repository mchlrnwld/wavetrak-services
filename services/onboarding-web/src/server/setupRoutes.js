import OnboardingPageContainer from '../containers/routes/OnboardingPageContainer';
import routesConfig from '../routes/config';

const setupRoutes = async () => [
  {
    component: OnboardingPageContainer,
    routes: await Promise.all(
      routesConfig.map(async ({ routeProps, loader }) => ({
        ...routeProps,
        component: (await loader()).default,
      })),
    ),
  },
];

export default setupRoutes;
