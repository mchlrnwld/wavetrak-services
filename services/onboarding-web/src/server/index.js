/* eslint-disable no-console */
import newrelic from 'newrelic';
import express from 'express';
import compression from 'compression';
import cookieParser from 'cookie-parser';
import Loadable from 'react-loadable';
import { getBundles } from 'react-loadable/webpack';
import { matchRoutes } from 'react-router-config';
import { hydrate, tokenHandler, renderFetch, getClientIp } from '@surfline/web-common';

import path from 'path';
import React from 'react';
import { Helmet } from 'react-helmet';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router';
import { createMemoryHistory } from 'history';
import { CookiesProvider } from 'react-cookie';
import cookiesMiddleware from 'universal-cookie-express';
import setupRoutes from './setupRoutes';
import { getStatusCode } from '../selectors/status';
import template from './template';
import Routes from '../routes/Routes';
import config from '../config';
import logger from '../common/logger';

import configureStore from '../stores';

const clientAssets = require(ASSETS_MANIFEST); // eslint-disable-line import/no-dynamic-require, no-undef, max-len
const app = express();

const serverPort = process.env.SERVER_PORT ? parseInt(process.env.SERVER_PORT, 10) : 8080;
const log = logger('web-onboarding');

function getBackplaneOptions(pathname) {
  if (pathname.indexOf('/onboarding') > -1) {
    return { 'renderOptions[hideNav]': true };
  }
  return {};
}

/**
 * SA-3062: reset anonymous IDs
 * @param {import('express').Request} req
 */
const resetAnonymousIdCookie = (req) => {
  const hasExistingResetCookie = req.cookies.sl_reset_anonymous_id_v2;

  if (!hasExistingResetCookie) {
    // Remove the cookie from the req object so it does not get sent to backplane
    delete req.cookies.ajs_anonymous_id;
  }
  return !hasExistingResetCookie;
};

const server = (loadableManifest, routes) => {
  app.disable('x-powered-by');
  app.use(compression());
  app.use(cookieParser());
  app.use(cookiesMiddleware());

  app.get('/health', (req, res) =>
    res.send({
      status: 200,
      message: 'OK',
      version: process.env.APP_VERSION || 'unknown',
    }),
  );

  app.use('/onboarding/static', express.static(path.join(process.cwd(), './build/public')));
  app.use('/favicon.ico', express.static(path.join(process.cwd(), './build/public/favicon.ico')));

  const wrapErrors = (fn) => (...args) => fn(...args).catch(args[2]);

  app.use(tokenHandler(`${config.servicesURL}/trusted`, log));

  app.use(
    wrapErrors(async (req, res) => {
      // SA-3062: reset anonymous IDs
      const shouldResetAnonymousIdCookie = resetAnonymousIdCookie(req);

      try {
        const backplaneOpts = getBackplaneOptions(req.path);
        const backplane = await renderFetch(process.env.BACKPLANE_HOST, getClientIp(req), {
          ...(req.userId ? { userId: req.userId } : null),
          ...(req.cookies.ajs_anonymous_id
            ? {
                anonymousId: JSON.parse(req.cookies.ajs_anonymous_id),
              }
            : null),
          bundle: 'margins',
          'renderOptions[mvpForced]': req.headers['x-surfline-mvp-forced'],
          vendor: 'v2',
          accessToken: req.cookies.access_token ?? null,
          ...backplaneOpts,
        });

        const { geo, favorites, settings } = backplane.data.api;
        const hasFavorites = favorites?.data?.favorites?.length > 0;
        const previouslyOnboarded = settings?.onboarded?.length > 0;
        const shouldSkipOnboarding = hasFavorites || previouslyOnboarded;

        if (req.path.includes('/onboarding') && shouldSkipOnboarding) {
          return res.redirect(config.homepageUrl);
        }

        const history = createMemoryHistory({
          initialEntries: [req.url],
        });
        const store = configureStore({ geo }, history);

        // Iterate all routes matching the URL and get the components and params to send to hydrate
        const matchedRoutes = matchRoutes(routes, req.path);
        const params = Object.assign({}, ...matchedRoutes.map(({ match }) => match.params));
        const components = matchedRoutes.map(({ route }) => route.component);

        await hydrate(store, { components, params }, req, backplane.data.api);
        const initialState = store.getState();

        const context = {};
        const modules = [];
        const root = renderToString(
          <CookiesProvider cookies={req.universalCookies}>
            <Provider store={store}>
              <StaticRouter location={req.url} context={context}>
                <Loadable.Capture report={(moduleName) => modules.push(moduleName)}>
                  <Routes />
                </Loadable.Capture>
              </StaticRouter>
            </Provider>
          </CookiesProvider>,
        );

        const loadableBundles = getBundles(loadableManifest, modules);

        // Handle the 404 route

        if (context.status === 404) {
          return res.status(404).send('Not Found');
        }
        // Handle redirect status
        if (context.url) {
          return res.redirect(302, `${context.url}`);
        }

        const head = Helmet.rewind();

        const hideWidget = req.path && req.path.indexOf('onboarding') > -1;
        return res.status(getStatusCode(initialState)).send(
          template({
            root,
            cssBundle: clientAssets['main.css'],
            jsBundle: clientAssets['main.js'],
            manifest: clientAssets['manifest.js'],
            initialState: JSON.stringify(initialState),
            backplane,
            head,
            config,
            loadableBundles,
            hideWidget,
            shouldResetAnonymousIdCookie,
          }),
        );
      } catch (error) {
        console.log(error);
        newrelic.noticeError(error);
        return res.status(500).send('Something went wrong');
      }
    }),
  );

  const errorHandlerMiddleware = (err, req, res) => {
    if (err) {
      newrelic.noticeError(err);
      console.log(err);
    }
    return res.status(500).send('Something went wrong');
  };

  app.use(errorHandlerMiddleware);

  return app;
};

Promise.all([
  // eslint-disable-next-line import/no-unresolved
  import('../../build/react-loadable'),
  setupRoutes(),
  Loadable.preloadAll(),
])
  .then(([loadableManifest, routes]) => {
    server(loadableManifest, routes).listen(serverPort, () => {
      console.log(`Express Server listening on ${serverPort}`);
    });
  })
  .catch((err) => {
    console.error(err);
    console.log('Exiting...');
    process.exit(-1);
  });
