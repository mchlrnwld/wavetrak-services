import newrelic from 'newrelic';
import {
  snippet as segment,
  googletagservices,
  resetAnonymousIdCookieSnippet,
} from '@surfline/web-common';

/* eslint-disable prefer-template, max-len */

export default ({
  cssBundle,
  jsBundle,
  root,
  initialState,
  head,
  config,
  backplane,
  loadableBundles,
  manifest,
  hideWidget = false,
  shouldResetAnonymousIdCookie,
}) => `

<!DOCTYPE html>
<html lang="en">

  <head>
    <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
    <meta charSet='utf-8' />
    <meta httpEquiv="Content-Language" content="en" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    ${shouldResetAnonymousIdCookie ? `${resetAnonymousIdCookieSnippet()}` : ''}
    ${newrelic.getBrowserTimingHeader()}

    ${segment(config.appKeys.segment)}
    ${googletagservices()}

    ${
      backplane.data.css
        ? `<link rel="stylesheet" type="text/css" href="${backplane.data.css}">`
        : ''
    }
    ${cssBundle ? '<link rel="stylesheet" type="text/css" href="' + cssBundle + '">' : ''}

    ${head.title.toString()}
    ${head.meta.toString()}

    <!-- React-Loadable requires loading the preloaded components early to pick up SSR on the client -->
    <script src="${manifest}"></script>
    ${loadableBundles.map((bundle) => `<script src="${bundle.publicPath}"></script>`).join('\n')}

    <meta property="fb:app_id" content="218714858155230"/>
    <meta property="fb:page_id" content="255110695512" />
    <link rel="publisher" href="https://plus.google.com/+Surfline" />
    <meta property="og:site_name" content="Surfline"/>
    <link href="https://wa.cdn-surfline.com/quiver/0.4.0/apple/surfline-apple-touch-icon.png" rel="apple-touch-icon">
    <meta name="apple-itunes-app" content="app-id=393782096,affiliate-data=at=1000lb9Z&ct=surfline-website-smart-banner&pt=261378">

  </head>

  <body style="margin: 0; padding: 0">
    <div class="quiver-page-container">
      <div id="backplane-header">${backplane.data.components.header}</div>
      <main class="quiver-page-container__content" role="main" id="root">${root}</main>
      <div id="backplane-footer" style="display: none">${backplane.data.components.footer}</div>
    </div>

    <script>window.__BACKPLANE_API__ = ${JSON.stringify(backplane.data.api)}</script>
    <script>window.__BACKPLANE_REDUX__ = ${JSON.stringify(backplane.data.redux)}</script>


    <script src="${backplane.associated.vendor.js}"></script>
    <script src="${backplane.data.js}"></script>

    <script>
      window.__DATA__ = ${initialState}
    </script>
    <script src="${jsBundle}"></script>

    ${
      !hideWidget
        ? `
    <!-- Start of surfline Zendesk Widget script -->
    <script>
      window.zESettings = {
        webWidget: {
          zIndex: 99
        }
      };
    </script>
    <script>
    /*<![CDATA[*/
      window.onload = function (e, t) {
        var n, o, d, i, s, a = [], r = document.createElement("iframe");
        window.zEmbed = function () {
          a.push(arguments)
        }, window.zE = window.zE || window.zEmbed, r.src = "javascript:false", r.title = "", r.role = "presentation", (r.frameElement || r).style.cssText = "display: none", d = document.getElementsByTagName("script"), d = d[d.length - 1], d.parentNode.insertBefore(r, d), i = r.contentWindow, s = i.document;
        try {
          o = s
        } catch (e) {
          n = document.domain, r.src = 'javascript:var d=document.open();d.domain="' + n + '";void(0);', o = s
        }
        o.open()._l = function () {
          var e = this.createElement("script");
          n && (this.domain = n), e.id = "js-iframe-async", e.src = "https://assets.zendesk.com/embeddable_framework/main.js", this.t = +new Date, this.zendeskHost = "surfline.zendesk.com", this.zEQueue = a, this.body.appendChild(e)
        }, o.write('<body onload="document._l();">'), o.close()
      };
    /*]]>*/
    </script>
    <!-- End of surfline Zendesk Widget script -->
    `
        : ''
    }
  </body>

</html>

`;
