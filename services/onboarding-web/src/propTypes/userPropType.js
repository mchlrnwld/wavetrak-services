import PropTypes from 'prop-types';

const userPropType = PropTypes.shape({
  _id: PropTypes.string,
  token: PropTypes.string,
  email: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
});

export default userPropType;
