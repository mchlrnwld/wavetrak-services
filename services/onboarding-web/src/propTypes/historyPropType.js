import PropTypes from 'prop-types';

const historyPropType = PropTypes.shape({
  push: PropTypes.func.isRequired,
});

export default historyPropType;
