import PropTypes from 'prop-types';

const errorsPropType = PropTypes.shape({
  favorites: PropTypes.shape(),
});

export default errorsPropType;
