import PropTypes from 'prop-types';

export default PropTypes.shape({
  _id: PropTypes.string,
  user: PropTypes.string,
  preferredWaveChart: PropTypes.string,
  onboarded: PropTypes.arrayOf(PropTypes.string),
  units: PropTypes.shape({
    temperature: PropTypes.string,
    windSpeed: PropTypes.string,
    tideHeight: PropTypes.string,
    swellHeight: PropTypes.string,
    surfHeight: PropTypes.string,
  }),
  location: PropTypes.shape({
    type: PropTypes.string,
    coordinates: PropTypes.arrayOf(PropTypes.number),
  }),
});
