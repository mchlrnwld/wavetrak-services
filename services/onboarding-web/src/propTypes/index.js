export favoritesPropType from './favoritesPropType';
export errorsPropType from './errorsPropType';
export spotPropType from './spotPropType';
export spotsPropType from './spotsPropType';
export userPropType from './userPropType';
export geoPropType from './geoPropType';
export devicePropType from './devicePropType';
export historyPropType from './historyPropType';
