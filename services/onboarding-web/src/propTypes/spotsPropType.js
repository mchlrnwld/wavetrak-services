import PropTypes from 'prop-types';

const spotsPropType = PropTypes.shape({
  loading: PropTypes.bool,
  error: PropTypes.string,
  query: PropTypes.string,
  end: PropTypes.bool,
});

export default spotsPropType;
