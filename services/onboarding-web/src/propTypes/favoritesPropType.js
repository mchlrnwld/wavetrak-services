import PropTypes from 'prop-types';

const favoritesPropType = PropTypes.shape({
  spots: PropTypes.arrayOf(PropTypes.string),
  loading: PropTypes.bool,
  error: PropTypes.string,
});

export default favoritesPropType;
