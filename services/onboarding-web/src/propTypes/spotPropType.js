import PropTypes from 'prop-types';

const spotPropType = PropTypes.shape({
  name: PropTypes.string,
  id: PropTypes.string,
  favorited: PropTypes.bool,
});

export default spotPropType;
