import PropTypes from 'prop-types';

const devicePropType = PropTypes.shape({
  mobile: PropTypes.bool,
  width: PropTypes.number,
});

export default devicePropType;
