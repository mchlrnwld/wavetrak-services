import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'connected-react-router';
import rootReducer from '../reducers';

export default function configureStore(initialState, history) {
  const enhancer = compose(applyMiddleware(thunk, routerMiddleware(history)));
  const store = createStore(rootReducer(history), initialState, enhancer);
  return store;
}
