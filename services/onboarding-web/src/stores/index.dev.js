import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'connected-react-router';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from '../reducers';

export default function configureStore(initialState, history) {
  const enhancer = composeWithDevTools(applyMiddleware(thunk, routerMiddleware(history)));
  const store = createStore(rootReducer(history), initialState, enhancer);

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      // eslint-disable-next-line global-require
      const nextRootReducer = require('../reducers').default;
      store.replaceReducer(nextRootReducer(history));
    });
  }

  return store;
}
