import React from 'react';
import Loadable from 'react-loadable';
import { hydrate, unmountComponentAtNode } from 'react-dom';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import { CookiesProvider } from 'react-cookie';
import '@surfline/quiver-themes/stylesheets/normalize.css';
import { hot } from 'react-hot-loader';

import configureStore from '../stores';
import '../styles/base.scss';

const root = document.querySelector('#root');
const preloadedState = window.__DATA__; // eslint-disable-line no-underscore-dangle
const history = createBrowserHistory();

// Create a history of your choosing (we're using a browser history in this case)
const store = configureStore(preloadedState, history);

const mount = () => {
  // eslint-disable-next-line global-require,import/newline-after-import
  const Routes = hot(module)(require('../routes/Routes').default);

  Loadable.preloadReady().then(() => {
    hydrate(
      <CookiesProvider>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <Routes />
          </ConnectedRouter>
        </Provider>
      </CookiesProvider>,
      root,
    );
  });
};

mount();

if (module.hot) {
  module.hot.accept('../routes/Routes', () => {
    unmountComponentAtNode(root);
    mount();
  });
}
