export default {
  geo: {
    location: {},
  },
  user: {
    entitlements: [],
  },
  onboarding: {
    taxonomies: {
      loading: true,
      error: null,
      taxonomies: [],
      dataLoaded: false,
      earth: {
        loading: false,
        error: null,
        data: [],
        taxonomyId: null,
        dataLoaded: false,
      },
      continent: {
        loading: false,
        error: null,
        data: [],
        taxonomyId: null,
        dataLoaded: false,
      },
      country: {
        loading: false,
        error: null,
        data: [],
        taxonomyId: null,
        dataLoaded: false,
      },
      region: {
        loading: false,
        error: null,
        data: [],
        taxonomyId: null,
        dataLoaded: false,
      },
    },
  },
};
