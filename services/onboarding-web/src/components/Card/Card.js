/* eslint-disable jsx-a11y/click-events-have-key-events */

import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Checkmark from '../Icons/Checkmark';

const className = (isSelected, formKey) =>
  classNames({
    'sl-onboarding-card': true,
    [`sl-onboarding-card--${formKey}`]: !!formKey,
    'sl-onboarding-card--selected': isSelected,
  });

const Card = ({ isSelected, label, subtext, formKey, onClick, onUnselect, backgroundImage }) => {
  const [isHovered, setHovered] = useState(false);
  const doHover = () => setHovered(true);
  const stopHover = () => setHovered(false);

  const showBackground = backgroundImage && (isSelected || isHovered);
  const clickEvent = !isSelected ? onClick : onUnselect;

  return (
    <div
      onMouseEnter={doHover}
      onMouseLeave={stopHover}
      className={className(isSelected, formKey)}
      style={{ backgroundImage: showBackground ? `url(${backgroundImage})` : '' }}
      onClick={clickEvent}
    >
      <div className="sl-onboarding-card__content">
        <h5 className="sl-onboarding-card__main-text">{label}</h5>
        {subtext && <h6 className="sl-onboarding-card__sub-text">{subtext}</h6>}
        {isSelected && <Checkmark />}
      </div>
    </div>
  );
};

Card.propTypes = {
  isSelected: PropTypes.bool,
  label: PropTypes.string.isRequired,
  subtext: PropTypes.string,
  formKey: PropTypes.string,
  backgroundImage: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  onUnselect: PropTypes.func.isRequired,
};

Card.defaultProps = {
  isSelected: false,
  subtext: null,
  formKey: null,
  backgroundImage: null,
};

export default Card;
