import React from 'react';
import PremiumTick from '../Icons/PremiumTick';
import CrossMark from '../Icons/CrossMark';

const benefitsArray = [
  'Daily Surf Reports',
  'Two-Day Forecasts',
  '500+ Surf Cams',
  'Long-Range Forecasts',
  'Ad-Free Streams',
  'Premium HD Cams',
  'Cam Rewinds',
  'Expert Analysis',
  'Exclusive Tools',
  'Proprietary Buoy Data',
];

const showFreeBenefit = (benefit) => {
  const freeBenefits = ['Daily Surf Reports', 'Two-Day Forecasts', '500+ Surf Cams'];
  return freeBenefits.includes(benefit) ? <PremiumTick /> : <CrossMark />;
};

const PremiumBenefits = () => (
  <div className="premium-benefits-creative">
    <div className="premium-benefits-creative__section__text">
      <div className="premium-benefits-creative__header" />
      {benefitsArray.map((benefit) => (
        <div key={benefit} className="premium-benefits-creative__text__div">
          {benefit}
        </div>
      ))}
    </div>
    <div className="premium-benefits-creative__section__free">
      <div className="premium-benefits-creative__header_text">FREE</div>
      {benefitsArray.map((benefit) => (
        <div key={benefit} className="premium-benefits-creative__free__div">
          {showFreeBenefit(benefit)}
        </div>
      ))}
    </div>
    <div className="premium-benefits-creative__section__premium">
      <div className="premium-benefits-creative__header_text">PREMIUM</div>
      {benefitsArray.map((benefit) => (
        <div key={benefit} className="premium-benefits-creative__premium__div">
          <PremiumTick />
        </div>
      ))}
    </div>
  </div>
);

export default PremiumBenefits;
