import React from 'react';
import en from '../../intl/translations/en';

const FavoriteSpotsHeader = () => (
  <div className="sl-favorite-spots-header">
    <h1>{en.FavoriteSurfSpots.header.title}</h1>
    <div className="sl-favorite-spots-header__message">{en.FavoriteSurfSpots.header.message}</div>
  </div>
);

export default FavoriteSpotsHeader;
