/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/interactive-supports-focus */
import React from 'react';
import { pickBy, identity } from 'lodash';
import { trackEvent, trackClickedSubscribeCTA } from '@surfline/web-common';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Button, Chevron } from '@surfline/quiver-react';

import classNames from 'classnames';
import historyPropType from '../../propTypes/historyPropType';
import withDevice from '../../common/withDevice';
import devicePropType from '../../propTypes/devicePropType';
import { getRecommendedSpots, getSpotsWithinAreas } from '../../selectors/onboarding';
import { getFavoriteSpots } from '../../selectors/favorites';

const btnClass = (canProceed) =>
  classNames({
    'sl-onboarding-continue-btn': true,
    'sl-onboarding-continue-btn--disabled': !canProceed,
  });

const footerClass = (isFinalStep) =>
  classNames({
    'sl-onboarding-footer-container': true,
    'sl-onboarding-footer-container--final-step': isFinalStep,
  });

const OnboardingFooter = ({
  onContinue,
  step,
  onClickPrev,
  canProceed,
  premium,
  numSteps,
  segmentProperties,
  history,
  device,
}) => {
  const isFinalStep = step === numSteps;
  const onboardingData = useSelector(getRecommendedSpots);
  const regionsSelected = useSelector(getSpotsWithinAreas);
  const spots = useSelector(getFavoriteSpots);

  const triggerCompletedOnboardingSegmentEvent = () => {
    const trackProperties = pickBy(
      {
        abilityLevel: onboardingData?.abilityLevel,
        travelDistance: onboardingData?.travelDistance,
        numberRegions: regionsSelected?.selectedTaxonomies?.length,
        numberFavorites: spots?.length,
        category: 'onboarding',
      },
      identity,
    ); // Remove all falsey values
    trackEvent('Completed Favorites Wizard Questionnaire', trackProperties);
  };

  const doContinue = () => (canProceed ? onContinue() : null);

  const doFinishOnboarding = () => {
    triggerCompletedOnboardingSegmentEvent();
    window.location.assign('/surf-cams');
  };

  const doSkipOnboarding = () => {
    trackEvent('Clicked Skip', {
      group: segmentProperties.group,
      pageName: segmentProperties.pageName,
      platform: segmentProperties.platform,
      category: segmentProperties.category,
      isMobileView: device.mobile,
    });
    if (premium) {
      return setTimeout(() => window.location.assign('/surf-cams'), 300);
    }
    return history.push('/onboarding/go-premium');
  };

  const doClickSubscribe = () => {
    triggerCompletedOnboardingSegmentEvent();
    trackClickedSubscribeCTA({
      pageName: segmentProperties.pageName,
      category: segmentProperties.category,
    });
    setTimeout(() => window.location.assign('/upgrade'), 300);
  };

  return (
    <div className={footerClass(isFinalStep)}>
      {step > 1 && (
        <div role="button" className="sl-onboarding-prev-step" onClick={onClickPrev}>
          <Chevron direction="left" /> Step
          {step - 1}
        </div>
      )}
      <div className="sl-onboarding-footer-button-container">
        <div className={btnClass(canProceed)}>
          <Button
            button={{ disabled: !canProceed }}
            onClick={isFinalStep ? doFinishOnboarding : doContinue}
          >
            {isFinalStep ? 'Done' : 'Continue'}
          </Button>
        </div>
        {!premium && isFinalStep && (
          <div className="sl-onboarding-footer-upgrade-button">
            <Button onClick={doClickSubscribe}>Start Free Trial</Button>
          </div>
        )}
      </div>
      {!isFinalStep && (
        <div role="button" className="sl-onboarding-skip-step" onClick={doSkipOnboarding}>
          Skip <Chevron direction="right" />
        </div>
      )}
    </div>
  );
};

OnboardingFooter.propTypes = {
  onContinue: PropTypes.func.isRequired,
  step: PropTypes.number.isRequired,
  onClickPrev: PropTypes.func.isRequired,
  canProceed: PropTypes.bool.isRequired,
  numSteps: PropTypes.number.isRequired,
  premium: PropTypes.bool.isRequired,
  segmentProperties: PropTypes.shape({
    group: PropTypes.string,
    pageName: PropTypes.string,
    platform: PropTypes.string,
    category: PropTypes.string,
  }).isRequired,
  history: historyPropType.isRequired,
  device: devicePropType.isRequired,
};

export default withDevice(OnboardingFooter);
