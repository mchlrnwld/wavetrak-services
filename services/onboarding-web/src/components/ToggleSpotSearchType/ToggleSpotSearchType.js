import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import en from '../../intl/translations/en';

const buttonClassName = (active) =>
  classNames({
    'sl-toggle-spot-search-type__button': true,
    'sl-toggle-spot-search-type__button--active': active,
  });

const ToggleSpotSearchType = ({ type, onToggle }) => (
  <div className="sl-toggle-spot-search-type">
    <button type="button" className={buttonClassName(type !== 'CAMS')} onClick={() => onToggle()}>
      {en.ToggleSpotSearchType.all}
    </button>
    <button
      type="button"
      className={buttonClassName(type === 'CAMS')}
      onClick={() => onToggle('CAMS')}
    >
      {en.ToggleSpotSearchType.cams}
    </button>
  </div>
);

ToggleSpotSearchType.propTypes = {
  type: PropTypes.string,
  onToggle: PropTypes.func.isRequired,
};

ToggleSpotSearchType.defaultProps = {
  type: null,
};

export default ToggleSpotSearchType;
