import PropTypes from 'prop-types';
import React from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { Spinner, Alert } from '@surfline/quiver-react';

import FavoriteSpot from '../FavoriteSpot';
import { spotPropType } from '../../propTypes';

const elementInfiniteLoad = (
  <div className="sl-favorite-spots-grid__loading">
    <Spinner />
  </div>
);

const FavoriteSpotsGrid = ({
  fetchMoreSpots,
  end,
  spots,
  favorites,
  onClickFavoriteSpot,
  scrollableTarget,
  emptyMessage,
}) => (
  <InfiniteScroll
    next={fetchMoreSpots}
    dataLength={spots.length}
    hasMore={!end}
    loader={elementInfiniteLoad}
    scrollableTarget={scrollableTarget}
    endMessage={' ' /* The blank space is necessary to hide the message */}
  >
    <div className="sl-favorite-spots-grid">
      {spots.map((spot) => (
        <FavoriteSpot
          key={spot._id}
          spot={spot}
          favorited={favorites.indexOf(spot._id) !== -1}
          onClick={onClickFavoriteSpot}
        />
      ))}
      {!spots.length && emptyMessage && (
        <div className="sl-favorite-spots-grid__empty-message">
          <Alert>{emptyMessage}</Alert>
        </div>
      )}
    </div>
  </InfiniteScroll>
);

FavoriteSpotsGrid.propTypes = {
  favorites: PropTypes.arrayOf(PropTypes.string),
  onClickFavoriteSpot: PropTypes.func.isRequired,
  spots: PropTypes.arrayOf(spotPropType).isRequired,
  fetchMoreSpots: PropTypes.func,
  end: PropTypes.bool,
  scrollableTarget: PropTypes.string,
  emptyMessage: PropTypes.string,
};

FavoriteSpotsGrid.defaultProps = {
  end: false,
  favorites: [],
  scrollableTarget: null,
  emptyMessage: null,
  fetchMoreSpots: () => null,
};

export default FavoriteSpotsGrid;
