import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const tabPropTypes = PropTypes.shape({
  value: PropTypes.string,
  label: PropTypes.string,
});

const Tabs = ({ setActiveTab, activeTab, className, tabs }) => {
  const getTabClassName = (tab) =>
    classNames({
      [`${className}_tab`]: true,
      [`${className}_tab--active`]: activeTab.value === tab.value,
    });

  return (
    <div className={className}>
      {tabs.map((tab) => (
        <button
          type="button"
          key={tab.value}
          className={getTabClassName(tab)}
          onClick={() => setActiveTab(tab)}
        >
          {tab.label}
        </button>
      ))}
    </div>
  );
};

Tabs.propTypes = {
  setActiveTab: PropTypes.func.isRequired,
  activeTab: tabPropTypes.isRequired,
  className: PropTypes.string.isRequired,
  tabs: PropTypes.arrayOf(tabPropTypes).isRequired,
};

export default Tabs;
