/* eslint-disable jsx-a11y/click-events-have-key-events */
import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { CamIcon, FavoritesIndicator } from '@surfline/quiver-react';
import { spotPropType } from '../../propTypes';

const className = (favorited) =>
  classNames({
    'sl-favorite-spot': true,
    'sl-favorite-spot--favorited': favorited,
  });

const FavoriteSpot = ({ favorited, spot, onClick }) => {
  const spotCardImage = (spot.thumbnail && spot.thumbnail['300']) || spot.thumbnail || null;
  const cameras = spot.cams || spot.cameras || [];
  const isMultiCam = cameras.length > 1;

  return (
    <div
      className={className(favorited)}
      onClick={() => onClick(spot._id, favorited, spot.name)}
      style={{
        backgroundImage: `url(${spotCardImage})`,
      }}
    >
      <div className="sl-favorite-spot__add">
        <FavoritesIndicator favorited={favorited} />
        <div className="sl-favorite-spot__name">
          {cameras.length ? <CamIcon isMultiCam={isMultiCam} withText /> : ''}
          {spot.name}
        </div>
      </div>
    </div>
  );
};

FavoriteSpot.propTypes = {
  favorited: PropTypes.bool.isRequired,
  spot: spotPropType.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default FavoriteSpot;
