import PropTypes from 'prop-types';
import React from 'react';
import { Alert, Button } from '@surfline/quiver-react';

const FavoriteSpotsSubmit = ({ error, loading, success, disabled, onSubmit, submitText }) => (
  <div className="sl-favorite-spots-submit">
    {error ? <Alert type="error">There was an error submitting your favorites</Alert> : null}
    <Button button={{ disabled }} loading={loading} success={success} onClick={onSubmit}>
      {submitText}
    </Button>
  </div>
);

FavoriteSpotsSubmit.propTypes = {
  error: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  success: PropTypes.bool,
  disabled: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
  submitText: PropTypes.string.isRequired,
};

FavoriteSpotsSubmit.defaultProps = {
  error: null,
  success: false,
  disabled: false,
};

export default FavoriteSpotsSubmit;
