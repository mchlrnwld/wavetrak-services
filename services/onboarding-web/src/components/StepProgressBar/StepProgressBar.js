import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const stepClassName = (index, step) =>
  classNames({
    'sl-onboarding-step': true,
    'sl-onboarding-step--highlighted': index + 1 <= step,
  });

/**
 * The component for the progress bar at the top of the onboarding experience
 * helping users see how far in they are.
 * @param {number} step - current step the user is on
 * @param {number} numSteps - total number of steps
 * @param {number} steps - The step objects from the config
 */
const StepProgressBar = ({ step, numSteps, steps }) => (
  <div className="sl-onboarding-step-progress-container">
    <h6 className="sl-onboarding-step-title">{`Step ${step} of ${numSteps}`}</h6>
    <div className="sl-onboarding-step-progress">
      {steps.map(({ path }, index) => (
        <div key={path} className={stepClassName(index, step)} />
      ))}
    </div>
  </div>
);

StepProgressBar.propTypes = {
  step: PropTypes.number.isRequired,
  steps: PropTypes.arrayOf(
    PropTypes.shape({
      path: PropTypes.string,
    }),
  ).isRequired,
  numSteps: PropTypes.number.isRequired,
};

export default StepProgressBar;
