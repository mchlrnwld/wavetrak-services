import React, { useEffect, useCallback } from 'react';
import { Button } from '@surfline/quiver-react';
import PropTypes from 'prop-types';

const RedirectError = ({ redirectUrl }) => {
  const doRedirect = useCallback(() => window.location.assign(redirectUrl), [redirectUrl]);

  useEffect(() => {
    const redirectTimeout = setTimeout(doRedirect, 5000);
    return () => {
      clearTimeout(redirectTimeout);
    };
  }, [doRedirect]);

  return (
    <div className="sl-redirect-error">
      <Button onClick="doRedirect">Return To Surfline</Button>
    </div>
  );
};

RedirectError.propTypes = {
  redirectUrl: PropTypes.string.isRequired,
};

export default RedirectError;
