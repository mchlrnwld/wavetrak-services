import React from 'react';

const Checkmark = () => (
  <svg
    width="16px"
    height="16px"
    viewBox="0 0 16 16"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    className="sl-onboarding-check"
  >
    <g id="Desktop" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="onboarding-desktop-step-1" transform="translate(-796.000000, -285.000000)">
        <g id="Group-3" transform="translate(796.000000, 285.000000)">
          <circle id="Oval-2" fill="#22D736" cx="8.00666063" cy="8.00666063" r="8.00666063" />
          <polygon
            id="checkmark"
            fill="#FEFEFE"
            points="11.3982317 5.33803324 7.11695479 9.61931014 5.28208064 7.784436 4.67055204 8.39625293 6.50542618 10.2308387 7.11695479 10.8423674 7.7284834 10.2308387 12.0097603 5.94956185"
          />
        </g>
      </g>
    </g>
  </svg>
);

export default Checkmark;
