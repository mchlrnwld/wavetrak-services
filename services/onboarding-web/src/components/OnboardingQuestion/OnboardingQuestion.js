import React from 'react';
import PropTypes from 'prop-types';

const OnboardingQuestion = ({ question, children }) => (
  <div className="sl-onboarding-question-container">
    <div className="sl-onboarding-question">{question}</div>
    {children && <div className="sl-onboarding-step-content">{children}</div>}
  </div>
);

OnboardingQuestion.propTypes = {
  question: PropTypes.string.isRequired,
  children: PropTypes.node,
};

OnboardingQuestion.defaultProps = {
  children: null,
};

export default OnboardingQuestion;
