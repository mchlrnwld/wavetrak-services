import PropTypes from 'prop-types';
import React from 'react';

const FavoriteSpotSearch = ({ query, onChangeQuery }) => (
  <input
    className="sl-favorite-spot-search"
    placeholder="Search for your favorite spots"
    value={query || ''}
    onChange={(event) => onChangeQuery(event.target.value)}
  />
);

FavoriteSpotSearch.propTypes = {
  query: PropTypes.string,
  onChangeQuery: PropTypes.func.isRequired,
};

FavoriteSpotSearch.defaultProps = {
  query: null,
};

export default FavoriteSpotSearch;
