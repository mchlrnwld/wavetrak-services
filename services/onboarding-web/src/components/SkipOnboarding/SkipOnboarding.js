/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/click-events-have-key-events */

import PropTypes from 'prop-types';
import React from 'react';

const SkipOnboarding = ({ children, onClick }) => (
  <a className="sl-skip-onboarding" onClick={onClick}>
    {children}
  </a>
);

SkipOnboarding.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default SkipOnboarding;
