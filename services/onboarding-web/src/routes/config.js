const FavoriteSurfSpotsRoute = {
  routeProps: {
    path: '/setup/favorite-surf-spots/:type?',
  },
  loader: () =>
    import(
      /* webpackChunkName: "FavoriteSurfSpots" */
      '../containers/routes/AddFavoriteSurfSpots/index.js'
    ),
  loading: () => null,
  modules: ['../containers/routes/AddFavoriteSurfSpots/index.js'],
  webpack: () => [require.resolveWeak('../containers/routes/AddFavoriteSurfSpots/index.js')],
};

const FollowContentRoute = {
  routeProps: {
    path: '/setup/follow-content',
  },
  loader: () =>
    import(
      /* webpackChunkName: "FollowContent" */
      '../containers/routes/FollowContent/index.js'
    ),
  loading: () => null,
  modules: ['../containers/routes/FollowContent/index.js'],
  webpack: () => [require.resolveWeak('../containers/routes/FollowContent/index.js')],
};

const OnboardingRoute = {
  routeProps: {
    path: '/onboarding',
    isOnboarding: true,
  },
  loader: () =>
    import(
      /* webpackChunkName: "Onboarding" */
      '../containers/routes/Onboarding/index.js'
    ),
  loading: () => null,
  modules: ['../containers/routes/Onboarding/index.js'],
  webpack: () => [require.resolveWeak('../containers/routes/Onboarding/index.js')],
};

const NotFoundRoute = {
  routeProps: {
    path: '*',
    status: 404,
  },
  loader: () =>
    import(
      /* webpackChunkName: "NotFound" */
      './NotFound.js'
    ),
  loading: () => null,
  modules: ['./NotFound.js'],
  webpack: () => [require.resolveWeak('./NotFound.js')],
};

export default [FavoriteSurfSpotsRoute, FollowContentRoute, OnboardingRoute, NotFoundRoute];
