import fetch from 'isomorphic-fetch';
import { Cookies } from 'react-cookie';
import { canUseDOM, doCookiesWarning } from '@surfline/web-common';
import config from '../config';

const checkStatus = (res) => {
  if (res.status >= 200 && res.status < 300) return res;
  const error = new Error(res.statusText);
  error.response = res;
  error.statusCode = res.status;
  throw error;
};

const parseJSON = (response) => response.json();

const doFetch = (path, options = {}) => {
  doCookiesWarning(path, options);
  const cookies = new Cookies(canUseDOM ? undefined : options.cookies);
  const at = cookies.get('access_token');
  if (at) {
    // todo remove
    path += `&accesstoken=${at}`; // eslint-disable-line no-param-reassign
  }

  return fetch(`${config.servicesURL}${path}`, {
    ...options,
  })
    .then(checkStatus)
    .then(parseJSON);
};

export const createParamString = (params) =>
  Object.keys(params)
    .filter((key) => params[key] !== undefined && params[key] !== null)
    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join('&');

export default doFetch;
