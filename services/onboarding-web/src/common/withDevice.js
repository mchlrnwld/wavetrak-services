import React from 'react';
import useDevice from '../hooks/useDevice';

const withDevice = (WrappedComponent) => (props) => {
  const device = useDevice();
  return <WrappedComponent device={device} {...props} />;
};

export default withDevice;
