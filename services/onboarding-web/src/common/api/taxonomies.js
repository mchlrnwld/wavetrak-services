import baseFetch from '../baseFetch';

// eslint-disable-next-line import/prefer-default-export
export const fetchNearbyTaxonomies = (lat, lon, count = 1) =>
  baseFetch(`/taxonomy/nearby?lat=${lat}&lon=${lon}&type=geoname&fcl=A&fcode=ADM2&count=${count}`);

export const fetchTaxonomy = (taxonomyId, depth = 0) =>
  baseFetch(`/taxonomy?type=taxonomy&id=${taxonomyId}&maxDepth=${depth}`);
