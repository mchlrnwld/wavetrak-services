import baseFetch from '../baseFetch';

export const fetchUserDetails = () => baseFetch('/user?');

export const fetchUserSettings = async (clientIp) => {
  const options = {};
  if (clientIp) options.headers = new Headers({ 'x-client-ip': clientIp });
  return baseFetch('/user/settings?', options);
};

export const updateUserSettings = (update) =>
  baseFetch('/user/settings?', {
    method: 'PUT',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(update),
  });

export const fetchUserEntitlements = () => baseFetch('/entitlements?');

export const fetchUserFavorites = async (clientIp) => {
  const options = {};
  if (clientIp) options.headers = new Headers({ 'x-client-ip': clientIp });
  return baseFetch('/kbyg/favorites?', options);
};
