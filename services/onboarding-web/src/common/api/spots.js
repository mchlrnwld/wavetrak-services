import baseFetch from '../baseFetch';
import { SKILL_LEVEL_OPTIONS } from '../../containers/Onboarding/Step/constants';

export const getOnboardingSpotsInRange = (query, limit, offset, camsOnly) =>
  baseFetch(
    `/onboarding/spots?query=${query || ''}&limit=${limit}&offset=${offset}&camsOnly=${camsOnly}`,
  );

export const getBatchSpots = (parentTaxonomyIds, select) =>
  baseFetch('/kbyg/spots/batch?', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      parentTaxonomyIds,
      select,
    }),
  });

export const getRecommendedSpots = (
  abilityLevel,
  lat,
  lon,
  distance = 10,
  limit = 12,
  offset = 0,
) => {
  // We need to pass all the ability levels below the selected one, including the one selected
  const abilityLevelIndex = SKILL_LEVEL_OPTIONS.findIndex(({ value }) => value === abilityLevel);
  const oneLevelDown = abilityLevelIndex !== 0 ? abilityLevelIndex - 1 : abilityLevelIndex;
  const abilityLevels = SKILL_LEVEL_OPTIONS.slice(oneLevelDown, abilityLevelIndex + 1)
    .map(({ value }) => value)
    .join(',');
  return baseFetch(
    `/kbyg/spots/nearbygeo?abilityLevels=${abilityLevels}&distance=${distance}&lat=${lat}&lon=${lon}&limit=${limit}&offset=${offset}s&select=cameras,thumbnail`,
  );
};

export const getPopularSpots = () => baseFetch('/kbyg/spots/popular?');
