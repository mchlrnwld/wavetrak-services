const canUseNavigator = !!(
  typeof navigator !== 'undefined' &&
  navigator.geolocation &&
  navigator.geolocation.getCurrentPosition
);

export default canUseNavigator;
