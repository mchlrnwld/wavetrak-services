import { useEffect, useState } from 'react';

export default function useDevice() {
  const [mobile, setMobile] = useState(true);
  const [width, setWidth] = useState(null);

  useEffect(() => {
    let resizeTimeout = null;

    const setDeviceWidth = (innerWidth) => {
      setMobile(innerWidth < 840);
      setWidth(innerWidth);
    };

    const resizeListener = (event) => {
      if (resizeTimeout) return;
      resizeTimeout = setTimeout(() => {
        resizeTimeout = null;
        setDeviceWidth(event.target.innerWidth);
      }, 66);
    };

    window.addEventListener('resize', resizeListener);
    setDeviceWidth(window.innerWidth);
    return () => {
      window.removeEventListener('resize', resizeListener);
    };
  }, []);

  return {
    mobile,
    width,
  };
}
