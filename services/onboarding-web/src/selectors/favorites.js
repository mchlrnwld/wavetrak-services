export const getFavoriteSpots = (state) => {
  const data = state.favorites;
  if (data) {
    return data.spots;
  }
  return null;
};
