import { parse } from 'query-string';

// eslint-disable-next-line import/prefer-default-export
export const getRedirectUrl = (state) => {
  const {
    location: { search },
  } = state.router;
  if (search) {
    const { redirectUrl } = parse(search);
    return redirectUrl;
  }
  return null;
};
