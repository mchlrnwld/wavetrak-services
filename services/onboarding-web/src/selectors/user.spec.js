import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import * as selectors from './user';
import stateFixture from './fixtures/state.json';

describe('selectors / user', () => {
  const state = deepFreeze(stateFixture);

  const nonPremiumUser = {
    entitlements: [
      'Single Surf Alert address',
      'Local photos',
      'Uploads',
      'Galleries',
      'Favorites',
      'Follow Photographers',
      'sl_basic',
    ],
    loading: false,
  };
  const nonPremiumState = deepFreeze({
    ...stateFixture,
    user: nonPremiumUser,
  });

  it('gets user details', () => {
    const { firstName, lastName, email } = selectors.getUserDetails(state);
    expect(firstName).to.deep.equal('Luigi');
    expect(lastName).to.deep.equal('Doge');
    expect(email).to.deep.equal('luigi@surfline.com');
  });

  it('gets user settings', () => {
    const { _id, user, onboarded, preferredWaveChart, units, location } = selectors.getUserSettings(
      state,
    );
    expect(_id).to.deep.equal('abc123');
    expect(user).to.deep.equal('xyzpdq');
    expect(onboarded).to.deep.equal([]);
    expect(preferredWaveChart).to.deep.equal('SURF');
    expect(location).to.have.property('coordinates');
    expect(units).to.have.property('surfHeight');
  });

  it('gets user premium entitlements from state', () => {
    const entitlements = selectors.getUserEntitlements(state);
    expect(entitlements[0]).to.deep.equal('sl_no_ads');
    expect(entitlements[1]).to.deep.equal('sl_premium');
  });

  it('gets user non-premium entitlements from local fixture', () => {
    const entitlements = selectors.getUserEntitlements(nonPremiumState);
    expect(entitlements[0]).to.deep.equal('Single Surf Alert address');
    expect(entitlements[1]).to.deep.equal('Local photos');
    expect(entitlements).to.not.include('sl_premium');
  });

  it('gets user sl_premium status', () => {
    const isPremium = selectors.getPremiumStatus(state);
    expect(isPremium).to.be.true();
  });

  it('gets user not sl_premium status', () => {
    const isPremium = selectors.getPremiumStatus(nonPremiumState);
    expect(isPremium).to.be.false();
  });
});
