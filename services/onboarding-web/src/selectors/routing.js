// eslint-disable-next-line import/prefer-default-export
export const getRedirectUrl = (state) => {
  const data = state.routing.locationBeforeTransitions;
  if (data) {
    return data.query.redirectUrl;
  }
  return null;
};
