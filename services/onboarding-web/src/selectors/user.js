export const getUser = (state) => {
  const data = state.user;
  if (data) {
    return data;
  }
  return null;
};

export const getUserDetails = (state) => {
  const data = state.user.details;
  if (data) {
    return data;
  }
  return null;
};

export const getUserSettings = (state) => {
  const data = state.user.settings;
  if (data) {
    return data;
  }
  return null;
};

export const getUserEntitlements = (state) => {
  const data = state.user.entitlements;
  if (data) {
    return data;
  }
  return null;
};

export const getUserFavorites = (state) => {
  const data = state.user.favorites;
  if (data) {
    return data;
  }
  return null;
};

export const getPremiumStatus = (state) => state.user.entitlements.includes('sl_premium');
