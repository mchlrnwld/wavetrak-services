export const getRecommendedSpots = (state) => {
  const data = state.onboarding;
  if (data) {
    return data.recommended;
  }
  return null;
};

export const getPopularSpots = (state) => {
  const data = state.onboarding;
  if (data) {
    return data.popular;
  }
  return null;
};

export const getSpotsWithinAreas = (state) => {
  const data = state.onboarding;
  if (data) {
    return data.withinAreas;
  }
  return null;
};

export const getTaxonomies = (state) => {
  const data = state.onboarding;
  if (data) {
    return data.taxonomies;
  }
  return null;
};

export const getEarthTaxonomy = (state) => {
  const data = getTaxonomies(state);
  if (data) {
    return data.earth;
  }
  return null;
};

export const getContinentTaxonomy = (state) => {
  const data = getTaxonomies(state);
  if (data) {
    return data.continent;
  }
  return null;
};

export const getCountryTaxonomy = (state) => {
  const data = getTaxonomies(state);
  if (data) {
    return data.country;
  }
  return null;
};

export const getRegionTaxonomy = (state) => {
  const data = getTaxonomies(state);
  if (data) {
    return data.region;
  }
  return null;
};

export const getTaxonomiesLoading = (state) => {
  const data = getTaxonomies(state);
  if (data) {
    return data.loading;
  }
  return null;
};

export const getTaxonomyError = (state) => {
  const data = getTaxonomies(state);
  if (data) {
    return data.error;
  }
  return null;
};

export const getEarthTaxonomyError = (state) => {
  const data = getEarthTaxonomy(state);
  if (data) {
    return data.error;
  }
  return null;
};

export const getContinentTaxonomyError = (state) => {
  const data = getContinentTaxonomy(state);
  if (data) {
    return data.error;
  }
  return null;
};

export const getCountryTaxonomyError = (state) => {
  const data = getCountryTaxonomy(state);
  if (data) {
    return data.error;
  }
  return null;
};

export const getRegionTaxonomyError = (state) => {
  const data = getRegionTaxonomy(state);
  if (data) {
    return data.error;
  }
  return null;
};

export const getTaxonomiesErrors = (state) => {
  const taxonomyError = getTaxonomyError(state);
  const earthTaxonomyError = getEarthTaxonomyError(state);
  const continentTaxonomyError = getContinentTaxonomyError(state);
  const countryTaxonomyError = getCountryTaxonomyError(state);
  const regionTaxonomyError = getRegionTaxonomyError(state);

  return (
    taxonomyError &&
    (earthTaxonomyError || continentTaxonomyError || countryTaxonomyError || regionTaxonomyError)
  );
};

export const getOnboardingFavorites = (state) => {
  const data = state.onboarding;
  if (data) {
    return data.favorites;
  }
  return {};
};
