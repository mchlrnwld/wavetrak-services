export const getGeo = (state) => {
  const data = state.geo;
  if (data) {
    return data;
  }
  return null;
};

export const getGeoLocation = (state) => {
  const data = state.geo;
  if (data) {
    return data.location;
  }
  return null;
};
