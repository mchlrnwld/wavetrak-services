import { merge } from 'lodash';

// eslint-disable-next-line import/no-dynamic-require
const envConfig = require(`./envs/${process.env.NODE_ENV}`);
const baseConfig = require('./base');

export default merge(baseConfig.default, envConfig.default);
