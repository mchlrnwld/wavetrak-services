export default {
  googleMapAPI: 'AIzaSyAbRbL0Y-QvLi5LF-LWgHKG7LZqFQPZaNw',
  cdnUrl: 'http://e.cdn-surfline.com/images/sl_landing/',
  createAccountPath: '/setup/create-account',
  homePath: '/',
  onboarding: {
    spots: {
      meta: {
        title: 'Favorite surf spots',
        description: '...',
        og: {
          title: 'Favorite surf spots',
          image: '/facebook-og-default.png',
          description: '...',
        },
      },
    },
  },
  numberOfNearbyTaxonomies: 8,
};
