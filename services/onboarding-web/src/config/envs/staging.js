export default {
  cdn: 'https://product-cdn.staging.surfline.com/onboarding/',
  servicesURL: 'https://services.staging.surfline.com',
  accountFavoritesUrl: 'https://staging.surfline.com/account/favorites',
  upstream: 'staging.surfline.com',
  homepageUrl: 'https://staging.surfline.com',
  appKeys: {
    segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
    newrelic: '',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || '3prmuinjgigdvi7m5haujp8jtq1un82rc64n',
  },
  onboarding: {
    spots: {
      meta: {
        canonical: 'https://staging.surfline.com/setup/favorite-surf-spots',
        og: {
          url: 'https://staging.surfline.com/setup/favorite-surf-spots',
        },
      },
    },
  },
};
