export default {
  cdn: 'https://wa.cdn-surfline.com/onboarding/',
  servicesURL: 'https://services.surfline.com',
  accountFavoritesUrl: 'https://www.surfline.com/account/favorites',
  upstream: 'www.surfline.com',
  homepageUrl: 'https://www.surfline.com',
  appKeys: {
    segment: 'Se6IGuzcvUhLx55hLHL0RiXkS6oUTz8L',
    newrelic: '',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'in20v2ebf6q1gh97ldcn1bp8ufdbr404e514',
  },
  onboarding: {
    spots: {
      meta: {
        canonical: 'https://www.surfline.com/setup/favorite-surf-spots',
        og: {
          url: 'https://www.surfline.com/setup/favorite-surf-spots',
        },
      },
    },
  },
};
