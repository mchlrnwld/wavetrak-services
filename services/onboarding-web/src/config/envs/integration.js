export default {
  cdn: 'https://product-cdn.integration.surfline.com/onboarding/',
  onboardingAPI: 'https://services.sandbox.surfline.com',
  servicesURL: 'https://services.sandbox.surfline.com',
  upstream: 'sandbox.surfline.com',
  homepageUrl: 'https://sandbox.surfline.com',
  accountFavoritesUrl: 'https://sandbox.surfline.com/account/favorites',
  appKeys: {
    segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
    newrelic: '',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
  },
  onboarding: {
    spots: {
      meta: {
        canonical: 'https://integration.surfline.com/setup/favorite-surf-spots',
        og: {
          url: 'https://integration.surfline.com/setup/favorite-surf-spots',
        },
      },
    },
  },
};
