export default {
  clientAssetPath: `//localhost:${process.env.CLIENT_PORT}/`,
  servicesURL: 'https://services.sandbox.surfline.com',
  upstream: 'sandbox.surfline.com',
  homepageUrl: 'https://sandbox.surfline.com',
  cdn: '/onboarding/static/',
  accountFavoritesUrl: 'https://sandbox.surfline.com/account/favorites',
  appKeys: {
    segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
    newrelic: '',
    splitio: process.env.SPLITIO_AUTHORIZATION_KEY || 'qgk2tspc9potna2ks72nb693ar5laood4s70',
  },
};
