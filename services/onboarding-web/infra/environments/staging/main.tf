provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-staging"
    key    = "services/onboarding/staging/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-1-staging"
}

data "aws_alb_listener" "main_internal" {
  arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-staging/fb54d1fb4dcc6678/1b066cae0efbb0a4"
}

locals {
  dns_name = "onboarding.staging.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "host-header"
      value = "internal-onboarding.staging.surfline.com"
    },
  ]
}

module "onboarding" {
  source = "../../"

  company     = "sl"
  application = "onboarding"
  environment = "staging"

  default_vpc = "vpc-981887fd"
  dns_name    = local.dns_name
  dns_zone_id = "Z3JHKQ8ELQG5UE"
  ecs_cluster = "sl-core-svc-staging"

  service_lb_rules = local.service_lb_rules
  service_td_count = 1

  alb_listener_arn  = data.aws_alb_listener.main_internal.arn
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-staging"
  load_balancer_arn = data.aws_alb.main_internal.arn

  auto_scaling_enabled = false
}

resource "aws_route53_record" "onboarding_internal" {
  zone_id = "Z3JHKQ8ELQG5UE"
  name    = "internal-onboarding.staging.surfline.com"
  type    = "A"

  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = true
  }
}
