provider "aws" {
  region = "us-west-1"
}

terraform {
  backend "s3" {
    bucket = "sl-tf-state-sbox"
    key    = "services/onboarding/sbox/terraform.tfstate"
    region = "us-west-1"
  }
}

data "aws_alb" "main_internal" {
  name = "sl-int-core-srvs-1-sandbox"
}

data "aws_alb_listener" "main_internal" {
  arn = "arn:aws:elasticloadbalancing:us-west-1:665294954271:listener/app/sl-int-core-srvs-1-sandbox/2b35d1b4c51a9c57/a0f3c0f67e6842bf"
}

locals {
  dns_name = "onboarding.sandbox.surfline.com"
  service_lb_rules = [
    {
      field = "host-header"
      value = local.dns_name
    },
    {
      field = "host-header"
      value = "internal-onboarding.sandbox.surfline.com"
    },
  ]
}

module "onboarding" {
  source = "../../"

  company     = "sl"
  application = "onboarding"
  environment = "sandbox"

  default_vpc = "vpc-981887fd"
  dns_name    = local.dns_name
  dns_zone_id = "Z3DM6R3JR1RYXV"
  ecs_cluster = "sl-core-svc-sandbox"

  service_lb_rules = local.service_lb_rules
  service_td_count = 1

  alb_listener_arn  = data.aws_alb_listener.main_internal.arn
  iam_role_arn      = "arn:aws:iam::665294954271:role/sl-ecs-service-core-svc-sandbox"
  load_balancer_arn = data.aws_alb.main_internal.arn

  auto_scaling_enabled = false
}

resource "aws_route53_record" "onboarding_internal" {
  zone_id = "Z3DM6R3JR1RYXV"
  name    = "internal-onboarding.sandbox.surfline.com"
  type    = "A"

  alias {
    name                   = data.aws_alb.main_internal.dns_name
    zone_id                = data.aws_alb.main_internal.zone_id
    evaluate_target_health = true
  }
}
