import path from 'path';
import webpack from 'webpack';
import autoprefixer from 'autoprefixer';
import { ReactLoadablePlugin } from 'react-loadable/webpack';
import ManifestPlugin from 'webpack-manifest-plugin';

import paths from '../paths';

const clientAssetPath = '//localhost:3001';
const { clientSrcPath, srcPath, buildPath, userRootPath } = paths;

export default () => ({
  mode: 'development',
  target: 'web',
  entry: [
    `webpack-hot-middleware/client?reload=true&path=${clientAssetPath}/__webpack_hmr`,
    path.resolve(clientSrcPath, 'index.js'),
  ],
  output: {
    filename: '[name].js',
    chunkFilename: '[name].js',
    path: buildPath,
    publicPath: 'http://localhost:3001/',
  },
  context: srcPath,
  devtool: 'inline-source-map',
  devServer: {
    hot: true,
    contentBase: buildPath,
    publicPath: 'http://localhost:3001',
  },
  optimization: {
    splitChunks: {
      name: 'manifest',
      minChunks: Infinity,
    },
  },
  resolve: {
    alias: {
      'react-dom': '@hot-loader/react-dom',
    },
  },
  module: {
    rules: [
      {
        test: /\.(jpg|jpeg|png|gif|eot|svg|ttf|woff|woff2|mp4)$/,
        loader: 'file-loader',
      },
      {
        test: /\.(js|jsx)$/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
          },
        },
        exclude: [/node_modules/, buildPath],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader?modules&sourceMap', 'resolve-url-loader'],
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader?sourceMap',
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [autoprefixer],
              },
            },
          },
          'resolve-url-loader',
          'sass-loader?sourceMap',
        ],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new ManifestPlugin({
      fileName: 'clientAssets.json',
      writeToFileEmit: true,
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.LoaderOptionsPlugin({
      options: {
        context: userRootPath,
        output: { path: buildPath },
      },
    }),
    new ReactLoadablePlugin({
      filename: path.resolve(buildPath, 'react-loadable.json'),
    }),
  ],
});
