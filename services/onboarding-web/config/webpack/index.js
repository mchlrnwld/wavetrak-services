import merge from 'webpack-merge';

import baseConfig from './base.config';
import devClientConfig from './dev.client.config';
import devServerConfig from './dev.server.config';
import prodClientConfig from './prod.client.config';
import prodServerConfig from './prod.server.config';

// const baseConfig = {
//   productionPublicPath: '/',
//   serverURL: url.parse('http://localhost:3000'),
//   clientURL: url.parse('http://localhost:3001'),
// };

export default (config, env = 'dev') => {
  let envClientConfig = devClientConfig;
  let envServerConfig = devServerConfig;

  // const clientOptions = {
  //   type: 'client',
  //   serverURL,
  //   clientURL,
  //   environment,
  //   publicPath: `${clientURL.href}`,
  //   publicDir: 'src/public',
  //   clientAssetsFile: 'publicAssets.json',
  // };


  if (env === 'prod') {
    envClientConfig = prodClientConfig;
    envServerConfig = prodServerConfig;
  }

  const clientConfig = merge.smart(baseConfig(), envClientConfig());
  const serverConfig = merge.smart(baseConfig(), envServerConfig());

  return {
    clientConfig,
    serverConfig,
  };
};
