const puppeteer = require('puppeteer');
const testWebpackConfig = require('./webpack/tests.config.js')({});

process.env.CHROME_BIN = puppeteer.executablePath();

module.exports = (config) => {
  config.set({
    basePath: '',
    frameworks: ['mocha'],
    files: [
      'http://www.googletagservices.com/tag/js/gpt.js',
      { pattern: './bundleTests.js', watched: false },
    ],
    exclude: [],
    customLaunchers: {
      ChromeCustom: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox'],
      },
    },
    crossOriginAttribute: false,
    preprocessors: {
      './bundleTests.js': ['webpack', 'sourcemap', 'sourcemap-writer'],
    },
    webpack: testWebpackConfig,
    reporters: ['coverage', 'mocha'],
    coverageReporter: {
      dir: '../coverage',
      reporters: [
        { type: 'text-summary' },
        { type: 'json' },
        { type: 'html' },
        { type: 'lcovonly' },
      ],
    },
    webpackMiddleware: {
      stats: 'errors-only',
    },
    plugins: [
      'karma-chrome-launcher',
      'karma-webpack',
      'karma-mocha',
      'karma-mocha-reporter',
      'karma-sourcemap-loader',
      'karma-sourcemap-writer',
      'karma-coverage',
    ],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['ChromeCustom'],
    singleRun: true,
    concurrency: Infinity,
    browserNoActivityTimeout: 30000,
  });
};
