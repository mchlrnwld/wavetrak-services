import once from 'lodash/fp/once';
import chokidar from 'chokidar';
import sh from 'shelljs';
import Express from 'express';
import nodemon from 'nodemon';
import devMiddleware from 'webpack-dev-middleware';
import hotMiddleware from 'webpack-hot-middleware';
import webpackCompiler from './webpackCompiler';
import config from '../config/webpack';

const port = 3001;

export default () => {
  process.on('SIGINT', () => {
    console.log('Exiting...');
    process.exit();
  });

  const compileServer = () => serverCompiler.run(() => null);

  const clientCompiler = webpackCompiler(config().clientConfig, (stats) => {
    if (stats.hasErrors()) return;
    compileServer();
  });

  sh.rm('-rf', './build');
  sh.mkdir('./build', './build/public');
  sh.cp('-r', './src/public/*', './build/public/');

  const watcher = chokidar.watch(['./src']);
  watcher.on('ready', () => {
    watcher
      .on('add', compileServer)
      .on('addDir', compileServer)
      .on('change', compileServer)
      .on('unlink', compileServer)
      .on('unlinkDir', compileServer);
  });

  const startClientServer = () => {
    const app = new Express();
    app.use(
      devMiddleware(clientCompiler, {
        headers: { 'Access-Control-Allow-Origin': '*' },
        hot: true,
        noInfo: true,
        stats: { colors: true },
      }),
    );
    app.use(hotMiddleware(clientCompiler));
    app.listen(port, () => {
      console.log(`Client server listening on ${port}`);
    });
  };

  const startServer = once(() => {
    nodemon({
      script: './build/server/main.js',
      watch: ['./src/server', './src/reducers', './src/actions'],
    })
      .on('start', () => {
        console.log('Development server started');
      })
      .on('restart', () => console.log('Restarted development server'))
      .on('quit', process.exit);
  });

  const serverCompiler = webpackCompiler(config().serverConfig, (stats) => {
    if (stats.hasErrors()) return;
    sh.cp('./config/newrelic.js', './build/server/');
    startServer();
  });

  startClientServer();
};
