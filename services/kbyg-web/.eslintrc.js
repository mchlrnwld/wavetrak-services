module.exports = {
  rules: {
    'prettier/prettier': 'error',
  },
  overrides: [
    {
      files: ['**/*.js', '**/*.jsx', '**/*.mjs'],
      extends: [
        '@surfline/eslint-config-web/base',
        '@surfline/eslint-config-web/react',
        '@surfline/eslint-config-web/jest',
        'next/core-web-vitals',
        'prettier',
      ],
      rules: {
        'import/extensions': [
          'error',
          'ignorePackages',
          {
            js: 'never',
            jsx: 'never',
            ts: 'never',
            tsx: 'never',
          },
        ],
        // TODO: remove once we have migrated off sinon and chai
        'jest/valid-expect': 'off',
        'no-restricted-exports': 'off',
      },
    },
    {
      parserOptions: {
        project: ['./tsconfig.json', './tsconfig.server.json'],
      },
      files: ['**/*.ts', '**/*.tsx'],
      extends: [
        '@surfline/eslint-config-web/jest',
        '@surfline/eslint-config-web/typescript',
        '@surfline/eslint-config-web/typescript-react',
        'next/core-web-vitals',
        'prettier',
      ],
      rules: {
        'react/require-default-props': 0,
        'react/jsx-props-no-spreading': 'off',
        // TODO: remove once we have migrated off sinon and chai
        'jest/valid-expect': 'off',
        'no-restricted-exports': 'off',
      },
    },
  ],
};
