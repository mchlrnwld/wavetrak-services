import '@surfline/web-common';

declare module '@surfline/web-common' {
  type Swell<T> = T & { height?: number; period?: number };
  // eslint-disable-next-line import/prefer-default-export
  export const getTop3Swells: <S>(swells: Swell<S>[]) => Swell<S>[];
  export const swellImpact: <S>(swell: Swell<S>) => number;
}
