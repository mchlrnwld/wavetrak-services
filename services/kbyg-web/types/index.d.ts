/// <reference types="@types/segment-analytics" />
/// <reference types="@types/googletag" />

import { BackplaneState } from '@surfline/web-common';
import type { KBYGAsyncReducers } from '../src/reducers';
import type { WavetrakStore } from '../src/stores';

declare global {
  interface Window {
    wavetrakNextApp?: string;
    wavetrakStore: WavetrakStore;
    wavetrakAsyncReducers?: KBYGAsyncReducers;
    __BACKPLANE_REDUX__: BackplaneState;
    wavetrakBackplaneMount?: () => void;
    analytics: SegmentAnalytics.AnalyticsJS;
    googletag: typeof googletag;
  }
}

declare module '*.module.scss' {
  const classes: { [key: string]: string };
  export default classes;
}
