# Surfline Web - KBYG

## Developing

```
npm run dev
```

## Bundling and Deploying

Building a non development distribution

```
npm build
```

Running the non development distribution

```
npm start
```

###

## Testing

Uses jest, chai, sinon, enzyme

```
npm test
```

## Creating new components

We have added `plop` to this service which is a simple way to generate component files (can be used
to other files but we need to create generators).

If you want to create a new component you can just run `npm run plop` and follow the prompts.

## Project Structure

```
  .next/
  public/
  types/
  generators/
  infra/
  src/
    actions/
    client/
    common/
    components/
    config/
    containers/
    intl/
    pages/
    reducers/
    styles/
```

## Configuration

### Environment Variables

| Key                | Description                                                  |
| ------------------ | ------------------------------------------------------------ |
| `NODE_ENV`         | `production` or `development` or `test`                      |
| `APP_ENV`          | `production` `sandbox` `staging` or `development`            |
| `NEWRELIC_ENABLED` | Valid options are `true` or `false`.                         |
| `BACKPLANE_HOST`   | Valid options are `http://web-backplane.{env}.surfline.com`. |

### Named Environment Configurations

Environment configurations are located in `src/config`. These variables are made
available to the client. Any sensitive keys should use an environment variable.

## Performance and Monitoring

Instruments...

- New Relic APM
- New Relic Browser

## Analytics

Segment is setup.

### Tracking Events in Segment

Use the `trackEvent` helper from `@surfline/web-common`

## Minimum Software Requirements

- Node v16
- NPM ^5
- React 17
- Redux 4
- Quiver
- Next.js ^12

## Minimum Browser Requirements

- Chrome latest
- FF latest
- Safari 8
- Edge
- Android 4.0.3 (Ice Cream Sandwich)
- Mobile Safari iOS 8
