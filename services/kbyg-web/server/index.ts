import 'dotenv/config';
import cookieParser from 'cookie-parser';
import { tokenHandler } from '@surfline/web-common';
import express, { ErrorRequestHandler, Request, Response } from 'express';
import next from 'next';

import logger from './logger';

type ENV = 'development' | 'sandbox' | 'staging' | 'production';
const appEnv: ENV = process.env.APP_ENV! as ENV;

const port = parseInt(process.env.PORT || '8080', 10);
const dev = process.env.NODE_ENV !== 'production';

const app = next({ dev });
const log = logger('web-kbyg');

const handle = app.getRequestHandler();

const servicesURL = {
  development: 'https://services.sandbox.surfline.com',
  sandbox: 'https://services.sandbox.surfline.com',
  staging: 'https://services.staging.surfline.com',
  production: 'https://services.surfline.com',
};

const errorHandler: ErrorRequestHandler = (err, _req, res) => {
  if (err) {
    log.error(err);
  }
  return res.status(500).send('Something went wrong');
};

const server = async () => {
  await app.prepare();
  const expressServer = express();

  expressServer.use(cookieParser());
  expressServer.use(tokenHandler(`${servicesURL[appEnv]}/trusted`, log));
  expressServer.get('/health', (_, res) =>
    res.status(200).json({ version: process.env.APP_VERSION || 'unknown', message: 'OK' }),
  );
  expressServer.all('*', (req: Request, res: Response) => handle(req, res));

  expressServer.use(errorHandler);

  expressServer.listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`);
  });
};

server().catch((err) => {
  console.error(err);
  process.exit(-1);
});
