import { useMemo } from 'react';
import { configureStore, ThunkAction, Action, ConfigureStoreOptions } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query/react';
import { createReducerInjecter, AsyncReducers, injectReducer } from '@surfline/web-common';
import getConfig from 'next/config';

import rootReducer, { KBYGAsyncReducers } from '../reducers';
import { buoysAPI } from '../common/api/buoys';
import createReducer from '../reducers/createReducer';

const { APP_ENV } = getConfig().publicRuntimeConfig;

export type WavetrakStore = ReturnType<typeof initStore>;
let store: WavetrakStore;

function initStore(initialState?: ConfigureStoreOptions['preloadedState']) {
  // We need to force cast this to the KBYGAsyncReducers since these are injected and managed by
  // Backplane and the types aren't available here.
  // This will and should change when we move backplane to a package, or to typescript
  const extraReducers = {
    backplane: createReducer({}, null),
  } as any as KBYGAsyncReducers;

  const initializedStore = configureStore({
    preloadedState: initialState,
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({ immutableCheck: { warnAfter: 100 } }).concat(...[buoysAPI.middleware]),
    // The initial backplane reducer is just for the server render, it doesn't need any
    // handlers. It just needs to be present to set the preloaded state from the server side
    reducer: rootReducer(extraReducers),
    devTools:
      APP_ENV !== 'production'
        ? {
            name: 'Surfline - KBYG',
            trace: true,
            traceLimit: 50,
          }
        : false,
  });

  createReducerInjecter(initializedStore, (asyncReducers) =>
    rootReducer(asyncReducers as KBYGAsyncReducers),
  );

  setupListeners(initializedStore.dispatch);

  return initializedStore;
}

export const getOrCreateStore = (preloadedState?: ConfigureStoreOptions['preloadedState']) => {
  // eslint-disable-next-line no-underscore-dangle
  const s = store ?? initStore(preloadedState);

  // For SSG and SSR always create a new store
  if (typeof window === 'undefined') return s;

  // Create the store once in the client
  if (!store) {
    store = s;
  }

  return s;
};

export function useStore(initialState: AppState) {
  const memoizedStore = useMemo(() => getOrCreateStore(initialState), [initialState]);
  return memoizedStore;
}

export type AppState = ReturnType<WavetrakStore['getState']>;
export type AppDispatch = ReturnType<typeof getOrCreateStore>['dispatch'];
export interface ReduxInitialProps {
  ssrReduxState?: AppState;
}

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  AppState,
  unknown,
  Action<string>
>;

export type { AsyncReducers, injectReducer };
