import { Camera } from './camera';
import { Units } from './units';

type AbilityLevels = 'BEGINNER' | 'INTERMEDIATE' | 'ADVANCED';
type Direction =
  | 'N'
  | 'NNE'
  | 'NE'
  | 'ENE'
  | 'E'
  | 'ESE'
  | 'SE'
  | 'SSE'
  | 'S'
  | 'SSW'
  | 'SW'
  | 'WSW'
  | 'W'
  | 'WNW'
  | 'NW'
  | 'NNW';
type Board = 'SHORTBOARD' | 'FUNBOARD' | 'LONGBOARD' | 'FISH' | 'GUN' | 'SUP';

type TideLevel = 'LOW' | 'HIGH' | 'NORMAL';
interface TidePoint {
  type: TideLevel;
  height: number;
  timestamp: number;
  utcOffset: number;
}

interface SwellPoint {
  height: number;
  direction: number;
  directionMin: number;
  period: number;
}

export interface SpotReport {
  associated: {
    localPhotosUrl: string;
    chartsUrl: string;
    beachesUrl: string;
    subregionUrl: string;
    href: string;
    units: Units;
    advertising: {
      spotId: string;
      subregionId: string;
      sst: string;
    };
    analytics: {
      spotId: string;
      subregionId: string;
    };
    weatherIconPath: string;
    utcOffset: number;
    abbrTimezone: string;
  };
  units: Units;
  spot: {
    name: string;
    breadcrumb: Array<{ name: string; href: string }>;
    lat: number;
    lon: number;
    cameras: Array<Camera>;
    subregion: {
      _id: string;
      forecastStatus: 'active' | 'inactive' | 'off';
    };
    abilityLevels: Array<AbilityLevels>;
    boardTypes: Array<Board>;
    metaDescription: string | null;
    titleTag: string | null;
    travelDetails: {
      abilityLevels?: {
        description: string;
        levels: Array<AbilityLevels>;
        summary: string;
      };
      best?: {
        season: {
          description: string;
          value: Array<'Spring' | 'Summer' | 'Autumn' | 'Winter'>;
        };
        tide: {
          description: string;
          value: Array<'Low' | 'Medium_Low' | 'Medium' | 'Medium_High' | 'High'>;
        };
        size: {
          description: string;
          value: string[];
        };
        windDirection: {
          description: string;
          value: Array<Direction>;
        };
        swellDirection: {
          description: string;
          value: Array<Direction>;
        };
      };
      bottom: {
        description: string;
        value: string[];
      };
      crowdFactor: {
        description: string;
        rating: number;
        summary: string;
      };
      localVibe: {
        description: string;
        rating: number;
        summary: string;
      };
      shoulderBurn: {
        description: string;
        rating: number;
        summary: string;
      };
      spotRating: {
        description: string;
        rating: number;
        summary: string;
      };
      waterQuality: {
        description: string;
        rating: number;
        summary: string;
      };
      travelId: string | null;
      access: string;
      breakType: string[];
      description: string;
      hazards: string;
      relatedArticleId: string;
      status: 'PUBLISHED' | 'DRAFT' | 'DELETED';
      boardTypes: Array<Board>;
    };
    legacyId: number;
    legacyRegionId: number;
  };
  forecast: {
    note: string | null;
    conditions: {
      human: boolean;
      value: string | null;
      expired: boolean;
    };
    wind: {
      speed: number;
      direction: number;
      directionType: string;
    };
    waveHeight: {
      human: boolean;
      min: number;
      max: number;
      occasional: number;
      humanRelation: string;
      plus: boolean;
    };
    tide: {
      previous: TidePoint;
      current: TidePoint;
      next: TidePoint;
    };
    waterTemp: {
      min: number;
      max: number;
    };
    wetsuit: {
      thickness: string;
      type: string;
    };
    weather: {
      temperature: number;
      condition: string;
    };
    swells: Array<SwellPoint>;
  };
  report: {
    timestamp: number;
    forecaster: {
      name: string;
      iconUrl: string;
      title: string;
    };
    body: string;
  } | null;
}
