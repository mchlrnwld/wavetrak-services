import { Units } from './units';

interface Thumbnail {
  url: string;
  sizes: string[];
}

interface Position {
  latitude: number;
  longitude: number;
  timestamp: number;
}

interface SessionsUser {
  firstName: string;
  lastName: string;
}

interface SessionsUnits extends Units {
  distance: 'FT' | 'M';
  speed: 'MPH' | 'KPH';
}

export interface SessionClip {
  associated: {
    units: SessionsUnits;
    utcOffset: number;
    timezoneAbbr: string;
  };
  spot: {
    id: string;
    name: string;
    location: {
      type: 'Point';
      coordinates: [number, number];
    };

    relivableRating?: number;
  };
  user: SessionsUser;
  wave: {
    startTimestamp: number;
    endTimestamp: number;
    distance: number;
    speedMax: number;
    clip: {
      clipUrl: string;
      thumbnail: Thumbnail;
    };
  };
}

export interface SessionWave {
  id: string;
  startTimestamp: number;
  endTimestamp: number;
  outcome: 'ATTEMPTED' | 'CAUGHT';
  distance: number;
  speedMax: number;
  speedAverage: number;
  clips: [
    {
      clipUrl: string;
      cameraId: string;
      startTimestamp: number;
      endTimestamp: number;
      thumbnail: Thumbnail;
      status: 'CLIP_AVAILABLE' | 'CLIP_PENDING' | 'CLIP_UNAVAILABLE';
      clipId: string;
    },
  ];
  positions: Position[];
  trackImage: Thumbnail;
  favorite: boolean;
}

export interface Session {
  associated: {
    timezone: [
      { abbr: string; id: number; key: string; offset: number; path: 'associated.timezone' },
    ];
    units: SessionsUnits;
  };
  id: string;
  clientName: string;
  name: string;
  spot: {
    id: string;
    name: string;
    location: {
      type: 'Point';
      coordinates: [number, number];
    };
    relivableRating?: number;
  };
  rating?: number;
  cams: Array<{
    id: string;
    alias: string;
    isPremium: boolean;
    stillUrl: string;
    title: string;
    visible: boolean;
  }>;
  clips: {
    available: number;
    total: number;
  };
  conditions: {
    waveHeight: {
      min: number;
      max: number;
      plus: boolean;
    };
    wind: {
      speed: number;
      directionCardinal: string;
    };
    conditions: {
      human: boolean;
      value: string;
    };
    tide: {
      direction: string;
      height: number;
    };
  };
  startTimestamp: number;
  endTimestamp: number;
  stats: {
    waveCount: number;
    speedMax: number;
    distancePaddled: number;
    distanceWaves: number;
    calories: number;
    wavesPerHour: number;
    longestWave: {
      distance: number;
      seconds: number;
    };
  };
  status: 'FAILED' | 'ENRICHED' | 'PENDING';
  thumbnail: Thumbnail;
  timezone: {
    id: number;
    path: 'associated.timezone';
  };
  waves: Array<SessionWave>;
  user: SessionsUser;
  createdAt: string;
  updatedAt: string;
}
