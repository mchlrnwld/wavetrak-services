export type Latitude = number;
export type Longitude = number;

export type Coordinates = [Longitude, Latitude];

export interface Location {
  coordinates: Coordinates;
  type: 'Point';
}

export interface Link {
  href: string;
  key: string;
}

export interface Geoname {
  fcode: string;
  lat: string;
  adminName1: string;
  fcodeName: string;
  countryName: string;
  fclName: string;
  name: string;
  countryCode: string;
  population: number;
  fcl: string;
  countryId: string;
  toponyName: string;
  geonameId: number;
  lon: string;
  adminCode1: number;
}

interface BaseTaxonomy {
  location: Location;
  _id: string;
  geonameId: number;
  type: string;
  liesIn: string[];
  geonames: Geoname;
  enumeratedPath: string;
  name: string;
  category: string;
  hasSpots: boolean;
  depth?: number;
  distance?: number;
  associated: { links: Link[] };
}

export interface WorldTaxonomy {
  _id: string;
  geonameId: number;
  name: string;
  slug: string;
  links: [{ key: string; href: string }];
  countries: [
    {
      _id: string;
      geonameId: number;
      name: string;
      slug: string;
      links: [{ key: string; href: string }];
      key: string;
      counties: [
        {
          _id: string;
          geonameId: number;
          name: string;
          slug: string;
          links: [{ key: string; href: string }];
        },
      ];
    },
  ];
}

export interface Taxonomy extends BaseTaxonomy {
  in?: BaseTaxonomy[] | null;
  contains?: BaseTaxonomy[];
}

export interface BreadCrumb {
  name: string;
  url: string;
  geonameId: number;
  id: string;
}

export interface BoundingBox {
  north: number;
  east: number;
  south: number;
  west: number;
}

export interface TravelContent {
  id: string;
  contentType: string;
  createdAt: number;
  updatedAt: number;
  content: { title: string; subtitle: string; summary: string; body: string };
  media: { large: string; medium: string; type: string; thumbnail: string; full: string };
}

export interface LocationView {
  loading: boolean;
  taxonomy: Partial<Taxonomy>;
  breadCrumbs: BreadCrumb[];
  boundingBox?: BoundingBox;
  siblings: Taxonomy[];
  url: string;
  travelContent?: TravelContent;
}
