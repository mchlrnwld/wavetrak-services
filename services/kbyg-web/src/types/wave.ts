import { Swell } from './swell';
import { Units } from './units';

export interface SurfHeight {
  min: number;
  max: number;
}

export interface WaveTimestep {
  timestamp: number;
  surf: SurfHeight;
  swells: Swell[];
}

export interface WaveData {
  error?: Error | string;
  loading: boolean;
  units: Units;
  utcOffset: number;
  location: {
    lat: number;
    lon: number;
  };
  forecastLocation: {
    lat: number;
    lon: number;
  };
  offshoreLocation: {
    lat: number;
    lon: number;
  };
  overallMaxSurfHeight: number;
  overallMaxSwellHeight: number;
  days: Array<Array<WaveTimestep>>;
}
