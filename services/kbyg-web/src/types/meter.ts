export interface Meter {
  meteringEnabled: boolean;
  showMeterRegwall: boolean;
  meterRemaining?: number;
  isMeteredPremium: boolean;
}
