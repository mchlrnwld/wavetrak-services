export interface SubregionOverviewData {
  _id: string;
  name: string;
  primarySpot: string;
  spots: Array<{
    _id: string;
    name: string;
    conditions: {
      human: boolean;
      value: string;
    };
    waveHeight: {
      human: boolean;
      min: number;
      max: number;
      occasional: number;
      humanRelation: string;
      plus: boolean;
    };
    lat: number;
    lon: number;
  }>;
  timestamp: number;
  forecastSummary: {
    forecastStatus: {
      status: string;
      inactiveMessage: string;
    };
    nextForecast: {
      timestamp: number;
      utcOffset: number;
    };
    forecaster: {
      name: string;
      title: string;
      email: string;
    };
    highlights: string[];
    breadcrumb: Array<{
      name: string;
      href: string;
    }>;
  };
}

export interface SubregionFeed {
  loading: boolean;
  error: boolean;
  articles: Array<{
    id: string;
    contentType: string;
    createdAt: number;
    updatedAt: number;
    promoted: string[];
    premium: boolean;
    content: {
      title: string;
      subtitle: string;
      body: string;
    };
    permalink: string;
    media: {
      type: string;
      feed1x: string;
      feed2x: string;
      promobox1x?: string;
      promobox2x?: string;
    };
    author: {
      iconUrl: string;
      name: string;
    };
    tags: Array<{
      name: string;
      url: string;
    }>;
  }>;
}

export interface NearbySubregion {
  _id: string;
  name: string;
  legacyId: number;
  offshoreSwellLocation: {
    type: string;
    coordinates: [number, number];
  };
}

export type NearbySubregions = Array<NearbySubregion>;
