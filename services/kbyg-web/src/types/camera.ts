export interface Camera {
  title: string;
  isPremium: boolean;
  isPrerecorded: boolean;
  lastPrerecordedClipStartTimeUTC: string;
  lastPrerecordedClipEndTimeUTC: string;
  status: {
    isDown: boolean;
    message: string;
    subMessage: string;
    altMessage: string;
  };
  supportsHighlights: boolean;
  supportsCrowds: boolean;
  streamUrl: string;
  stillUrl: string;
  rewindBaseUrl: string;
  alias: string;
  _id: string;
  control: string;
  nighttime: boolean;
  rewindClip: string;
}
