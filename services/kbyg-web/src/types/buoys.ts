import { EpochTimestamp, UtcOffset } from './time';
import { Units } from './units';
import { Wind } from './wind';

export type BuoyStatus = 'ONLINE' | 'OFFLINE';

export interface BuoySwell {
  height?: number;
  direction?: number;
  period?: number;
  spread?: number;
  impact?: number;
  index?: number;
}

export interface StationInfo {
  id: string;
  name?: string;
  sourceId?: string;
  source?: string;
  status: BuoyStatus;
  latitude: number;
  longitude: number;
  latestTimestamp?: EpochTimestamp;
  utcOffset?: UtcOffset;
}

export interface StationData {
  timestamp: EpochTimestamp;
  utcOffset: UtcOffset;
  height: number;
  period: number;
  direction: number;
  sst?: number;
  airTemperature?: number;
  waterTemperature?: number;
  dewPoint?: number;
  pressure?: number;
  elevation?: number;
  wind?: Wind;
  swells?: BuoySwell[];
}

export interface Station {
  station: StationInfo;
  data: StationData;
}

export interface NearbyStation {
  id: string;
  name?: string;
  latitude: number;
  longitude: number;
  status: BuoyStatus;
  sourceId: string;
  abbrTimezone: string;
  latestData: StationData;
}

// API Responses

export interface NearbyBuoysResponse {
  data: NearbyStation[];
  associated: { units: Units };
}

export interface BuoysBoundingBoxResponse {
  data: NearbyStation[];
  associated: { units: Units };
}

export interface BuoyDetailsResponse {
  associated: {
    abbrTimezone: string;
  };
  data: Station['station'];
}

export interface BuoyReportResponse {
  data: Array<Station['data']>;
  associated: {
    abbrTimezone: string;
    units: Units;
  };
}
