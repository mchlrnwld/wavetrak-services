import { Units } from './units';

export interface Wind {
  speed: number;
  direction: number;
  gust?: number;
}

export interface WindWithOptimalScore extends Wind {
  optimalScore: number;
}

export interface WindTimestep extends WindWithOptimalScore {
  timestamp: number;
  utcOffset: number;
}

/**
 * @typedef {object} Wind
 * @property {number} timestamp
 * @property {number} utcOffset
 * @property {number} direction
 * @property {number} speed
 * @property {number} gust
 * @property {number} optimalScore
 */

export interface WindData {
  error?: string | Error;
  loading: boolean;
  units: Units;
  days: Array<Array<WindTimestep>>;
}
