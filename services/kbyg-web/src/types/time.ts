export type EpochTimestamp = number;
export type UtcOffset = number;
