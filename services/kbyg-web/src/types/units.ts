import { FEET, METERS } from '@surfline/web-common';

export type SurfHeight = typeof METERS | typeof FEET;
export type Height = typeof METERS | typeof FEET;
export type Speed = 'KPH' | 'MPH' | 'KTS';
export type Temperature = 'C' | 'F';
export type DateFormat = 'MDY' | 'DMY';

export type Units = {
  surfHeight: SurfHeight;
  waveHeight?: SurfHeight;
  swellHeight: Height;
  tideHeight: Height;
  temperature: Temperature;
  windSpeed: Speed;
};
