export type SwellEvents = Array<SwellEvent>;

export interface SwellEvent {
  id: number;
  permalink: string;
  name: string;
  summary: string;
  thumbnailUrl: string;
  dfpKeyword: string;
  jwPlayerVideoId: string;
  active: boolean;
  newWindow: boolean | null;
  createdAt: string;
  updatedAt: string;
  basins: string[];
  taxonomies: {
    id: string;
    href: string;
    name: string;
    type: string;
    breadCrumbs: string[] | null;
  }[];
  titleTag: string;
  banner: string;
}
