export interface Swell {
  height: number;
  direction: number;
  directionMin?: number;
  period: number;
  optimalScore?: 0 | 1 | 2;
  color?: string;
  index: number;
}
