export interface BoundingBox {
  north: number;
  south: number;
  east: number;
  west: number;
}

export interface Location {
  center: { lat: number; lon: number };
  zoom: number;
  boundingBox?: BoundingBox;
}
