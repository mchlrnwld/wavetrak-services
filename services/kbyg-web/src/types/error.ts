export interface WavetrakError extends Error {
  statusCode: number;
}
