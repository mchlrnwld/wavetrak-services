import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import configureStore from 'redux-mock-store';
import {
  SET_ACTIVE_SPOT,
  CLEAR_ACTIVE_SPOT,
  SCROLL_TO_SPOT,
  SET_SCROLL_POSITION,
  setActiveSpot,
  clearActiveSpot,
  scrollToSpot,
  setCamListScrollPosition,
  setSpotListScrollPosition,
} from './animations';

describe('actions / animations', () => {
  let mockStore: ReturnType<typeof configureStore>;

  beforeAll(() => {
    mockStore = configureStore();
  });

  describe('setActiveSpot', () => {
    it('dispatches SET_ACTIVE_SPOT with spotId', () => {
      const spotId = '5842041f4e65fad6a77088ed';
      const store = mockStore({});
      store.dispatch(setActiveSpot(spotId));
      expect(store.getActions()).to.containSubset([
        {
          type: SET_ACTIVE_SPOT,
          payload: spotId,
        },
      ]);
    });
  });

  describe('clearActiveSpot', () => {
    it('dispatches CLEAR_ACTIVE_SPOT', () => {
      const store = mockStore({});
      store.dispatch(clearActiveSpot());
      expect(store.getActions()).to.containSubset([{ type: CLEAR_ACTIVE_SPOT }]);
    });
  });

  describe('scrollToSpot', () => {
    it('dispatches SCROLL_TO_SPOT with spotId', () => {
      const spotId = '5842041f4e65fad6a77088ed';
      const store = mockStore({});
      store.dispatch(scrollToSpot(spotId));
      expect(store.getActions()).to.containSubset([
        {
          type: SCROLL_TO_SPOT,
          payload: spotId,
        },
      ]);
    });
  });

  describe('setSpotListScrollPosition', () => {
    it('dispatches SET_SCROLL_POSITION for spotList component', () => {
      const scrollPosition = deepFreeze({
        top: 10,
        left: 15,
      });
      const store = mockStore({});
      store.dispatch(setSpotListScrollPosition(scrollPosition));
      expect(store.getActions()).to.containSubset([
        {
          type: SET_SCROLL_POSITION,
          payload: {
            component: 'spotList',
            scrollPosition,
          },
        },
      ]);
    });
  });

  describe('setCamListScrollPosition', () => {
    it('dispatches SET_SCROLL_POSITION for camList component', () => {
      const scrollPosition = deepFreeze({
        top: 10,
        left: 15,
      });
      const store = mockStore({});
      store.dispatch(setCamListScrollPosition(scrollPosition));
      expect(store.getActions()).to.containSubset([
        {
          type: SET_SCROLL_POSITION,
          payload: {
            component: 'camList',
            scrollPosition,
          },
        },
      ]);
    });
  });
});
