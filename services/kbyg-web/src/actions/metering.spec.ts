import { expect } from 'chai';
import { PERFORMED_SURF_CHECK_ACTION, performSurfCheckAction } from './metering';

describe('actions / metering', () => {
  it('should export PERFORMED_SURF_CHECK_ACTION', () => {
    expect(PERFORMED_SURF_CHECK_ACTION).to.be.equal('PERFORMED_SURF_CHECK_ACTION');
  });

  it('should return an action for PERFORMED_SURF_CHECK_ACTION', () => {
    const action = performSurfCheckAction('actionName');
    expect(action).to.deep.equal({ type: PERFORMED_SURF_CHECK_ACTION, action: 'actionName' });
  });
});
