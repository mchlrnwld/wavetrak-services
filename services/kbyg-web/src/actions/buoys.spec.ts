import { expect } from 'chai';
import sinon from 'sinon';
import { buoysAPI } from '../common/api/buoys';
import { serverSideFetchBuoyDetails } from './buoys';
import { redirectToNotFound } from './status';

describe('actions / buoys', () => {
  let initiateGetBuoyDetailsEndpointStub: sinon.SinonStub;

  beforeEach(() => {
    initiateGetBuoyDetailsEndpointStub = sinon.stub(buoysAPI.endpoints.getBuoyDetails, 'initiate');
  });

  afterEach(() => {
    initiateGetBuoyDetailsEndpointStub.restore();
  });

  describe('/ serverSideFetchBuoyDetails', () => {
    it('should dispatch the action on a successful fetch', async () => {
      const dispatch = sinon.stub().returnsArg(0);
      const actionResult = { type: 'success' };
      initiateGetBuoyDetailsEndpointStub.returns(actionResult);

      await serverSideFetchBuoyDetails('123')(dispatch);

      expect(initiateGetBuoyDetailsEndpointStub).to.have.been.calledOnceWithExactly('123');
      expect(dispatch).to.have.been.calledOnceWithExactly(actionResult);
    });

    it('should redirect to the not found page for 404 errors', async () => {
      const dispatch = sinon.stub().returnsArg(0);
      const actionResult = { error: { status: 404 } };
      initiateGetBuoyDetailsEndpointStub.returns(actionResult);

      await serverSideFetchBuoyDetails('123')(dispatch);

      expect(initiateGetBuoyDetailsEndpointStub).to.have.been.calledOnceWithExactly('123');
      expect(dispatch).to.have.been.calledTwice();
      expect(dispatch).to.have.been.calledWith(actionResult);
      expect(dispatch).to.have.been.calledWith(redirectToNotFound('Buoy not found'));
    });
  });
});
