import { expect } from 'chai';
import sinon from 'sinon';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as chartsApi from '../common/api/charts';
import {
  FETCH_SUBREGION_CHART_IMAGES,
  FETCH_SUBREGION_CHART_IMAGES_SUCCESS,
  FETCH_SUBREGION_CHART_IMAGES_FAILURE,
  SET_ACTIVE_CHART,
  fetchChartImages,
  setActiveChart,
} from './charts';

describe('actions / charts', () => {
  describe('fetchChartImages', () => {
    let mockStore;

    beforeAll(() => {
      mockStore = configureStore([thunk]);
    });

    beforeEach(() => {
      sinon.stub(chartsApi, 'fetchLegacyChartImages');
    });

    afterEach(() => {
      chartsApi.fetchLegacyChartImages.restore();
    });

    it('dispatches FETCH_SUBREGION_CHART_IMAGES with type', async () => {
      const store = mockStore({
        subregion: {
          overview: {
            associated: {
              advertising: {
                subregionId: '12345',
              },
            },
          },
        },
      });
      const images = [
        {
          url: 'http://slcharts01.cdn-surfline.com/charts/socal/norange/nearshore/norange_large_1.png',
          timestamp: 1529496000,
        },
        {
          url: 'http://slcharts01.cdn-surfline.com/charts/socal/norange/nearshore/norange_large_2.png',
          timestamp: 1529517600,
        },
      ];
      const data = { images };
      const timezone = 'PST';
      const associated = { timezone };
      chartsApi.fetchLegacyChartImages.resolves({ associated, data });
      const expectedActions = [
        {
          type: FETCH_SUBREGION_CHART_IMAGES,
        },
        {
          type: FETCH_SUBREGION_CHART_IMAGES_SUCCESS,
          images,
          timezone,
        },
      ];

      const type = 'localswell';
      await store.dispatch(fetchChartImages(type));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(chartsApi.fetchLegacyChartImages).to.have.been.calledOnce();
    });

    it('dispatches FETCH_SUBREGION_CHART_IMAGES_FAILURE on error', async () => {
      const store = mockStore({
        subregion: {
          overview: {
            associated: {
              advertising: {
                subregionId: '12345',
              },
            },
          },
        },
      });
      chartsApi.fetchLegacyChartImages.rejects({ message: 'Some error occurred', status: 500 });
      const expectedActions = [
        {
          type: FETCH_SUBREGION_CHART_IMAGES,
        },
        {
          type: FETCH_SUBREGION_CHART_IMAGES_FAILURE,
          error: 'Some error occurred',
        },
      ];
      const type = 'localswell';
      await store.dispatch(fetchChartImages(type));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(chartsApi.fetchLegacyChartImages).to.have.been.calledOnce();
    });
  });

  describe('setActiveChart', () => {
    let mockStore;

    beforeAll(() => {
      mockStore = configureStore([thunk]);
    });

    it('dispatches SET_ACTIVE_CHART with active', async () => {
      const store = mockStore({
        components: {
          charts: {
            types: {
              subregion: [{ type: 'localswell', name: 'Wave Height', slug: 'wave-height' }],
            },
          },
        },
      });

      const active = {
        type: 'localswell',
        name: 'Wave Height',
        category: 'subregion',
        slug: 'wave-height',
      };

      const expectedActions = {
        type: SET_ACTIVE_CHART,
        active,
      };

      await store.dispatch(setActiveChart('wave-height', '58581a836630e24c4487900b'));
      expect(store.getActions()).to.deep.include(expectedActions);
    });
  });
});
