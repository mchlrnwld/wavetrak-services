/**
 * Get the fixture for the cameras array with a certain number of cams
 * @param {number} numCams - The number of cameras desired in the array
 */
const getCamsFixture = (numCams) =>
  [...Array(numCams)].map((_, index) => ({
    _id: index,
    title: `cam-${index}`,
    streamUrl: 'https://streamUrl.com',
    stillUrl: 'https://stillUrl.com',
    rewindBaseUrl: 'https://rewinds.com/url',
    isPremium: false,
    status: {
      isDown: false,
      message: '',
    },
    control: 'https://controlUrl.com',
    nighttime: false,
    rewindClip: 'https://rewindClip.com/url',
  }));

export default getCamsFixture;
