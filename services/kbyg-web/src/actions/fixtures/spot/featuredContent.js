const cdnUrl = 'http://e.cdn-surfline.com/images/sl_landing/';

const featuredContent = {
  data: {
    promobox: [
      {
        id: 'eeh545hyser42g',
        url: 'http://staging.surfline.com/surf-news/mick-fannings-first-surf-back-at-j-bay/682',
        media: {
          mobile: `${cdnUrl}mick.jpg`,
          tablet: `${cdnUrl}mick.jpg`,
          desktop: `${cdnUrl}mick.jpg`,
          largeDesktop: `${cdnUrl}mick.jpg`,
        },
        align: 'right',
        title: 'Mick Fanning Will Return to Full-Time Competition',
        subtitle: "3x world champ announces end of sabbatical, says he's committed to 2017 season",
        categories: ['Feature'],
      },
      {
        id: '34grseht45a',
        url: 'http://staging.surfline.com/surf-news/mick-fannings-first-surf-back-at-j-bay/682',
        media: {
          mobile: `${cdnUrl}wsl.jpg`,
          tablet: `${cdnUrl}wsl.jpg`,
          desktop: `${cdnUrl}wsl.jpg`,
          largeDesktop: `${cdnUrl}wsl.jpg`,
        },
        align: 'left',
        title: "5 Guys who should win their first 'CT in 2017",
        subtitle: '',
        categories: ['Feature'],
      },
      {
        id: 'awrgawerwerawer',
        url: 'http://staging.surfline.com/surf-news/mick-fannings-first-surf-back-at-j-bay/682',
        media: {
          mobile: `${cdnUrl}alex-gray-1.jpg`,
          tablet: `${cdnUrl}alex-gray-1.jpg`,
          desktop: `${cdnUrl}alex-gray-1.jpg`,
          largeDesktop: `${cdnUrl}alex-gray-1.jpg`,
        },
        align: 'right',
        title: "Inside Alex Gray's North African Freight-Train Tube",
        subtitle: 'Another solid NW swell slams the rich port',
        categories: ['Swell Stories', 'Feature'],
      },
      {
        id: '43whergaetgw3g34',
        url: 'http://staging.surfline.com/surf-news/mick-fannings-first-surf-back-at-j-bay/682',
        media: {
          mobile: `${cdnUrl}namibia.jpg`,
          tablet: `${cdnUrl}namibia.jpg`,
          desktop: `${cdnUrl}namibia.jpg`,
          largeDesktop: `${cdnUrl}namibia.jpg`,
        },
        align: 'center',
        title: "Exclusive: Namibia's Greatest Hits",
        subtitle:
          'Starring Anthony Walsh, Koa + Alex Smith, Koa Rothman, Benji Brand -- and Skeleton Bay',
        categories: ['Swell Stories', 'Feature'],
      },
      {
        id: '33qh5q34gq34',
        url: 'http://staging.surfline.com/surf-news/mick-fannings-first-surf-back-at-j-bay/682',
        media: {
          mobile: `${cdnUrl}supertubos.jpg`,
          tablet: `${cdnUrl}supertubos.jpg`,
          desktop: `${cdnUrl}supertubos.jpg`,
          largeDesktop: `${cdnUrl}supertubos.jpg`,
        },
        align: 'right',
        title: 'Supersurfers at Supertubos',
        subtitle: 'Dane, Ando, Brendon & Bruce get drained in Peniche',
        categories: ['Swell Stories', 'Feature'],
      },
    ],
  },
};

export default featuredContent;
