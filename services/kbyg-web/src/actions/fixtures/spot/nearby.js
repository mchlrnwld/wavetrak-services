const nearby = {
  units: { windSpeed: 'KPH', waveHeight: 'M', tideHeight: 'M' },
  data: {
    spots: [
      {
        _id: '5842041f4e65fad6a7708832',
        lat: 33.202391008646,
        lon: -117.3947525024,
        name: 'Oceanside Harbor',
        cameras: [
          {
            title: 'Oceanside Harbor',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253362/82a9c5f1/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/oceansidecacam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/oceansidecacam/oceansidecacam',
            _id: '58acb0668816e20012bb43b2',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: true, value: 'FAIR' },
        wind: { speed: 9.93, direction: 302.07 },
        waveHeight: {
          human: true,
          min: 0.9,
          max: 1.5,
          occasional: 1.8,
          humanRelation: 'waist to head high',
        },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 0,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4238,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a7708835',
        lat: 33.192192245107016,
        lon: -117.38916277885437,
        name: 'Oceanside Pier NS',
        cameras: [
          {
            title: 'Oceanside Pier NS',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253451/825c582a/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/osidepiernscam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/osidepiernscam/osidepiernscam',
            _id: '58acb07d8816e20012bb43cf',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: false, value: null },
        wind: { speed: 9.86, direction: 303.98 },
        waveHeight: { human: false, min: 0.6, max: 0.9, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 1,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4241,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '584204204e65fad6a7709435',
        lat: 33.191,
        lon: -117.385,
        name: 'Oceanside Pier SS',
        cameras: [
          {
            title: 'Oceanside Pier SS',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253451/dda08c66/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/osidepiersscam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/osidepiersscam/osidepiersscam',
            _id: '58acb09e8816e20012bb43fb',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: false, value: null },
        wind: { speed: 9.85, direction: 303.98 },
        waveHeight: { human: false, min: 0.6, max: 0.9, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 2,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 68366,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a7708837',
        lat: 33.14948100706,
        lon: -117.3502922058,
        name: 'Tamarack',
        cameras: [
          {
            title: 'Tamarack',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253476/14c77b24/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/tamarackcam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/tamarackcam/tamarackcam',
            _id: '58acb0f38816e20012bb445f',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: false, value: null },
        wind: { speed: 10.01, direction: 310.12 },
        waveHeight: { human: false, min: 0.6, max: 0.9, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 3,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4242,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a77088a6',
        lat: 33.13110546470468,
        lon: -117.3378362073661,
        name: 'Terra Mar Point',
        cameras: [],
        conditions: { human: false, value: null },
        wind: { speed: 10.19, direction: 311.29 },
        waveHeight: { human: false, min: 0.7, max: 1, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 4,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4775,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a77088a5',
        lat: 33.08685865930869,
        lon: -117.3153204508531,
        name: 'Ponto',
        cameras: [
          {
            title: 'Ponto',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253451/501efa7c/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/pontocam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/pontocam/pontocam',
            _id: '58acb0a28816e20012bb43ff',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: true, value: 'FAIR' },
        wind: { speed: 10.2, direction: 323.01 },
        waveHeight: {
          human: true,
          min: 0.9,
          max: 1.5,
          occasional: 1.8,
          humanRelation: 'waist to head high',
        },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 5,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4773,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '584204214e65fad6a7709c79',
        lat: 33.08685865930869,
        lon: -117.3153204508531,
        name: 'Ponto at South Carlsbad State Beach',
        cameras: [
          {
            title: 'Ponto at South Carlsbad State Beach',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253451/21fb6475/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/pontostatecam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/pontostatecam/pontostatecam',
            _id: '58acb0ae8816e20012bb440c',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: true, value: 'FAIR' },
        wind: { speed: 10.19, direction: 323.01 },
        waveHeight: {
          human: true,
          min: 0.9,
          max: 1.5,
          occasional: null,
          humanRelation: 'waist to head high',
        },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 6,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 128083,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a770889f',
        lat: 33.07516210660336,
        lon: -117.311737671684,
        name: 'Grandview',
        cameras: [],
        conditions: { human: false, value: null },
        wind: { speed: 9.76, direction: 323.93 },
        waveHeight: { human: false, min: 1, max: 1.1, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 7,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4771,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a77088a0',
        lat: 33.06762965513461,
        lon: -117.3083294608364,
        name: 'Beacons',
        cameras: [
          {
            title: 'Beacons',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253176/7b3191d3/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/beaconscam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/beaconscam/beaconscam',
            _id: '58acb1138816e20012bb4481',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: false, value: null },
        wind: { speed: 9.27, direction: 324.02 },
        waveHeight: { human: false, min: 1, max: 1.1, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 8,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4772,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a77088a3',
        lat: 33.04694255008896,
        lon: -117.2996588219151,
        name: 'Moonlight Beach',
        cameras: [],
        conditions: { human: false, value: null },
        wind: { speed: 9.18, direction: 325.3 },
        waveHeight: { human: false, min: 1.2, max: 1.5, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 9,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4770,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a77088b7',
        lat: 33.04569760260221,
        lon: -117.2994012022731,
        name: 'D Street',
        cameras: [],
        conditions: { human: false, value: null },
        wind: { speed: 9.13, direction: 325.32 },
        waveHeight: { human: false, min: 1.2, max: 1.5, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 10,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4792,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a77088b4',
        lat: 33.0341859796287,
        lon: -117.2955191028633,
        name: "Swami's",
        cameras: [
          {
            title: "Swami's",
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253476/576566ea/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/swamiscam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/swamiscam/swamiscam',
            _id: '58acb0cf8816e20012bb442e',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: true, value: 'FAIR' },
        wind: { speed: 9.28, direction: 325.45 },
        waveHeight: {
          human: true,
          min: 0.9,
          max: 1.5,
          occasional: 1.8,
          humanRelation: 'waist to head high',
        },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 11,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4789,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a77088b5',
        lat: 33.02237397156142,
        lon: -117.28833317756653,
        name: 'Pipes at San Elijo State Beach',
        cameras: [
          {
            title: 'Pipes',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253451/ccc556eb/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/pipessandiegocam/latest_small.jpg',
            rewindBaseUrl:
              'http://live-cam-archive.cdn-surfline.com/pipessandiegocam/pipessandiegocam',
            _id: '58acb06f8816e20012bb43bf',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: false, value: null },
        wind: { speed: 9.13, direction: 325.39 },
        waveHeight: { human: false, min: 1, max: 1.2, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 12,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4790,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a77088b8',
        lat: 33.0195254541639,
        lon: -117.2855766242791,
        name: 'San Elijo State Beach',
        cameras: [
          {
            title: 'San Elijo',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253451/ea291122/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/sanelijocam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/sanelijocam/sanelijocam',
            _id: '58acb0c28816e20012bb441d',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: false, value: null },
        wind: { speed: 9.46, direction: 325.62 },
        waveHeight: { human: false, min: 1, max: 1.1, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 13,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4791,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '584204214e65fad6a7709d19',
        lat: 33.01475766452994,
        lon: -117.2842382170616,
        name: 'Cardiff Reef',
        cameras: [
          {
            title: 'Cardiff Reef',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253176/f91f6303/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/cardiffreefcam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/cardiffreefcam/cardiffreefcam',
            _id: '58acb12e8816e20012bb449d',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: true, value: 'FAIR' },
        wind: { speed: 9.44, direction: 325.66 },
        waveHeight: {
          human: true,
          min: 0.9,
          max: 1.5,
          occasional: 1.8,
          humanRelation: 'waist to head high',
        },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 14,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 139590,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a77088b1',
        lat: 33.01475766452994,
        lon: -117.2842382170616,
        name: 'Cardiff',
        cameras: [
          {
            title: 'Cardiff',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253176/eb9b9586/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/cardiffcam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/cardiffcam/cardiffcam',
            _id: '58acb10e8816e20012bb447a',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: true, value: 'FAIR' },
        wind: { speed: 9.44, direction: 325.65 },
        waveHeight: {
          human: true,
          min: 0.9,
          max: 1.5,
          occasional: null,
          humanRelation: 'waist to head high',
        },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 15,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4786,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a77088b3',
        lat: 33.00211185876003,
        lon: -117.281101957955,
        name: 'Seaside Reef',
        cameras: [
          {
            title: 'Seaside Reef',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253451/1b9177b8/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/seasidereefcam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/seasidereefcam/seasidereefcam',
            _id: '58acb0c28816e20012bb441e',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: false, value: null },
        wind: { speed: 9.49, direction: 326.51 },
        waveHeight: { human: false, min: 1, max: 1.3, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 16,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4787,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '584204214e65fad6a7709d1a',
        lat: 33.00211185876003,
        lon: -117.281101957955,
        name: "George's",
        cameras: [
          {
            title: "George's",
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253176/4e087fea/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/georgescam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/georgescam/georgescam',
            _id: '58acb1418816e20012bb44b2',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: false, value: null },
        wind: { speed: 9.46, direction: 326.53 },
        waveHeight: { human: false, min: 1, max: 1.3, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 17,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 139591,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a77088b0',
        lat: 32.97527261243354,
        lon: -117.2721061331305,
        name: 'Del Mar Rivermouth',
        cameras: [],
        conditions: { human: false, value: null },
        wind: { speed: 7.97, direction: 325.7 },
        waveHeight: { human: false, min: 0.9, max: 1.1, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 18,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4785,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a77088ae',
        lat: 32.96773495868229,
        lon: -117.2703491561836,
        name: 'Del Mar Beachbreak',
        cameras: [],
        conditions: { human: false, value: null },
        wind: { speed: 7.47, direction: 325.17 },
        waveHeight: { human: false, min: 1, max: 1.2, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 19,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4784,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '584204214e65fad6a7709d14',
        lat: 32.96773495868229,
        lon: -117.2703491561836,
        name: 'Del Mar 25th St.',
        cameras: [
          {
            title: 'Delmar 25th St.',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253176/c4e4ed53/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/delmar25thcam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/delmar25thcam/delmar25thcam',
            _id: '58acb1178816e20012bb4486',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: false, value: null },
        wind: { speed: 7.46, direction: 325.18 },
        waveHeight: { human: false, min: 1, max: 1.2, occasional: null },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 20,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 139340,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
      {
        _id: '5842041f4e65fad6a77088af',
        lat: 32.95916304183685,
        lon: -117.2696309974066,
        name: 'Del Mar',
        cameras: [
          {
            title: 'Del Mar',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253176/1783125f/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/delmarcam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/delmarcam/delmarcam',
            _id: '58acb1418816e20012bb44b4',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: true, value: 'FAIR' },
        wind: { speed: 7.46, direction: 326.24 },
        waveHeight: {
          human: true,
          min: 0.9,
          max: 1.5,
          occasional: null,
          humanRelation: 'waist to head high',
        },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 21,
        subregionId: '58581a836630e24c44878fd7',
        legacyId: 4783,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
    ],
  },
};

export default nearby;
