const buoys = {
  buoys: [
    {
      weather_conditions: {
        wind_gust: -1.94,
        water_temp: '71&deg; F',
        air_temp: '30&deg; F',
        wind_speed: -1.94,
        pressure: -1,
        wind_dir_card: 'N',
        wind_dir: -1,
      },
      _metadata: {
        dateCreated: 'July 21, 2017 15:36:14',
        cached: 'true',
        redisKey: 'cache:api:mobile:buoys:46222:buoys:units:e',
        hostname: 'prod-coldfusion-api-8',
      },
      title: 'SAN PEDRO (46222)',
      buoy_id: '46222',
      datestamp: 'July 21, 2017 14:00:00',
      swell: [
        '3ft @ 8s, SW 217&deg;',
        '1ft @ 16s, SSE 169&deg;',
        '1ft @ 12s, SSE 165&deg;',
        '1ft @ 4s, W 278&deg;',
      ],
      timestamp: 1500645600,
      timezone: '0',
      formatted_time_and_date: '7:00AM 07/21/2017',
      significant_wave_height: '3ft',
      cms_id: '129884',
      name: 'SAN PEDRO',
    },
    {
      weather_conditions: {
        wind_gust: -1.94,
        water_temp: '72&deg; F',
        air_temp: '30&deg; F',
        wind_speed: -1.94,
        pressure: -1,
        wind_dir_card: 'N',
        wind_dir: -1,
      },
      _metadata: {
        dateCreated: 'July 21, 2017 15:36:14',
        cached: 'true',
        redisKey: 'cache:api:mobile:buoys:46221:buoys:units:e',
        hostname: 'prod-coldfusion-api-8',
      },
      title: 'SANTA MONICA (46221)',
      buoy_id: '46221',
      datestamp: 'July 21, 2017 14:00:00',
      swell: ['3ft @ 7s, WSW 254&deg;', '2ft @ 14s, SSW 205&deg;', '2ft @ 9s, SSW 204&deg;'],
      timestamp: 1500645600,
      timezone: '0',
      formatted_time_and_date: '7:00AM 07/21/2017',
      significant_wave_height: '4ft',
      cms_id: '129885',
      name: 'SANTA MONICA',
    },
    {
      weather_conditions: {
        wind_gust: -1.94,
        water_temp: '75&deg; F',
        air_temp: '30&deg; F',
        wind_speed: -1.94,
        pressure: -1,
        wind_dir_card: 'N',
        wind_dir: -1,
      },
      _metadata: {
        dateCreated: 'July 21, 2017 15:36:14',
        cached: 'true',
        redisKey: 'cache:api:mobile:buoys:46242:buoys:units:e',
        hostname: 'prod-coldfusion-api-8',
      },
      title: 'CAMP PENDLETON (46242)',
      buoy_id: '46242',
      datestamp: 'July 21, 2017 14:00:00',
      swell: ['2ft @ 15s, SW 220&deg;', '2ft @ 9s, SSW 208&deg;', '1ft @ 5s, W 277&deg;'],
      timestamp: 1500645600,
      timezone: '0',
      formatted_time_and_date: '7:00AM 07/21/2017',
      significant_wave_height: '3ft',
      cms_id: '129876',
      name: 'CAMP PENDLETON',
    },
  ],
  _metadata: {
    dateCreated: 'July 21, 2017 15:36:14',
    cached: 'cache:api:mobile:nearby:4223:units:e:buoys:3:buoy',
    redisKey: 'cache:api:mobile:buoys:46242:buoys:units:e',
    hostname: 'prod-coldfusion-api-8',
  },
};

export default buoys;
