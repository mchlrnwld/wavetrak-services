const nearby = {
  associated: {
    units: {
      temperature: 'C',
      tideHeight: 'M',
      swellHeight: 'M',
      waveHeight: 'M',
      windSpeed: 'KPH',
    },
  },
  data: {
    subregions: [
      { _id: '58581a836630e24c44878fcb', name: 'North Shore Oahu' },
      { _id: '58581a836630e24c44878fcc', name: 'West Side Oahu' },
      { _id: '58581a836630e24c44878fcd', name: 'South Shore Oahu' },
      { _id: '58581a836630e24c44878fce', name: 'Windward Side Oahu' },
    ],
  },
};

export default nearby;
