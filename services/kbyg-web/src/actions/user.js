/* istanbul ignore file */

import {
  favoriteSpot,
  unfavoriteSpot,
  resendEmailVerification,
  setRecentlyVisited,
} from '@surfline/web-common';

/**  @param {string} spotId */
export const doFavoriteSpot = (spotId) => favoriteSpot({ spotId, useAccessTokenQueryParam: true });

/** @param {string} spotId */
export const doUnfavoriteSpot = (spotId) =>
  unfavoriteSpot({ spotId, useAccessTokenQueryParam: true });

export const doResendEmailVerification = () => resendEmailVerification();

/**
 * @param {'spots' | 'subregions'} type
 * @param {string} _id
 * @param {string} name
 */
export const doSetRecentlyVisited = (type, _id, name) => setRecentlyVisited({ type, _id, name });
