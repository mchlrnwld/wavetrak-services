import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {
  SET_PRIMARY_CAM,
  setPrimaryCam,
  INITIALIZE_MULTI_CAM,
  initializeMultiCam,
  setCamView,
  SET_CAM_VIEW,
} from './multiCam';
import createCamerasFixture from './fixtures/cameras';

describe('actions / multiCam', () => {
  let mockStore;

  beforeAll(() => {
    mockStore = configureStore([thunk]);
  });

  describe('setPrimaryCam', () => {
    it('Sets the primary cam properly', () => {
      const cameras = createCamerasFixture(3);
      const store = mockStore({
        intialCameras: cameras,
        cameras,
        primaryCam: cameras[0],
      });
      store.dispatch(setPrimaryCam(cameras[0]));
      store.dispatch(setPrimaryCam(cameras[2]));
      expect(store.getActions()).to.deep.equal([
        {
          type: SET_PRIMARY_CAM,
          primaryCam: cameras[0],
        },
        {
          type: SET_PRIMARY_CAM,
          primaryCam: cameras[2],
        },
      ]);
    });
  });

  describe('setCamView', () => {
    it('Sets the cam view properly', () => {
      const store = mockStore({
        activeView: 'SINGLE',
      });
      store.dispatch(setCamView('MULTI'));
      store.dispatch(setCamView('SINGLE'));
      expect(store.getActions()).to.deep.equal([
        {
          type: SET_CAM_VIEW,
          activeView: 'MULTI',
        },
        {
          type: SET_CAM_VIEW,
          activeView: 'SINGLE',
        },
      ]);
    });
  });

  describe('initializeMultiCam', () => {
    it('Initializes the multi cams properly', () => {
      const cameras = createCamerasFixture(3);
      const store = mockStore({
        intialCameras: null,
        cameras: null,
        primaryCam: null,
      });
      store.dispatch(initializeMultiCam(cameras));
      expect(store.getActions()).to.deep.equal([
        {
          type: INITIALIZE_MULTI_CAM,
          initialCameras: cameras,
          isMultiCam: true,
          numCams: 3,
          primaryCam: cameras[0],
        },
      ]);
    });
    it('Sets isMultiCam to false if numCams is not greater than one', () => {
      const cameras = createCamerasFixture(1);
      const store = mockStore({
        intialCameras: null,
        cameras: null,
        primaryCam: null,
      });
      store.dispatch(initializeMultiCam(cameras));
      expect(store.getActions()).to.deep.equal([
        {
          type: INITIALIZE_MULTI_CAM,
          initialCameras: cameras,
          isMultiCam: false,
          numCams: 1,
          primaryCam: cameras[0],
        },
      ]);
    });
  });
});
