import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as regionApi from '../common/api/region';
import { INTERNAL_SERVER_ERROR } from './status';
import {
  CLEAR_SUBREGION_OVERVIEW_DATA,
  FETCH_SUBREGION_OVERVIEW_DATA,
  FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS,
  FETCH_REGIONAL_ARTICLES,
  FETCH_REGIONAL_ARTICLES_SUCCESS,
  FETCH_REGIONAL_ARTICLES_FAILURE,
  clearOverviewData,
  fetchOverviewData,
  fetchRegionalArticles,
  FETCH_SUBREGION_OVERVIEW_DATA_FAILURE,
} from './subregion';
import sanitizeArticleHTML from '../utils/sanitizeArticleHTML';

describe('actions / subregion', () => {
  describe('clearOverviewData', () => {
    let mockStore;

    beforeAll(() => {
      mockStore = configureStore([thunk]);
    });

    it('dispatches CLEAR_SUBREGION_OVERVIEW_DATA', async () => {
      const store = mockStore({});
      await store.dispatch(clearOverviewData());
      const expectedActions = [
        {
          type: CLEAR_SUBREGION_OVERVIEW_DATA,
        },
      ];
      expect(store.getActions()).to.deep.equal(expectedActions);
    });
  });

  describe('fetchOverviewData', () => {
    let mockStore;

    beforeAll(() => {
      mockStore = configureStore([thunk]);
    });

    beforeEach(() => {
      sinon.stub(regionApi, 'fetchRegionalOverview');
      sinon.stub(regionApi, 'fetchRegionalArticles');
    });

    afterEach(() => {
      regionApi.fetchRegionalOverview.restore();
      regionApi.fetchRegionalArticles.restore();
    });

    it('dispatches FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS with subregionId', async () => {
      const store = mockStore({
        map: {
          location: {
            zoom: 12,
          },
        },
      });
      const overview = deepFreeze({
        associated: {
          advertising: {
            spotId: null,
            subregionId: '2144',
          },
          utcOffset: -8,
          units: {
            temperature: 'F',
            tideHeight: 'FT',
            waveHeight: 'FT',
            windSpeed: 'KTS',
          },
          chartsUrl: '/surf-charts/north-america/southern-california/north-san-diego/2144',
        },
        data: {
          _id: '58581a836630e24c44878fd7',
          name: 'North San Diego',
          primarySpot: '5842041f4e65fad6a77088b7',
          breadcrumb: [
            {
              name: 'United States',
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/6252001',
            },
            {
              name: 'San Diego',
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/san-diego/5391832',
            },
            {
              name: 'California',
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/5332921',
            },
          ],
          timestamp: 1512774170,
          forecastSummary: {
            forecastStatus: {
              status: 'inactive',
              inactiveMessage: 'sdfasdfasdfasds',
            },
            nextForecast: {
              timestamp: 1631818800,
              utcOffset: -7,
            },
            forecaster: {
              title: 'Development Employee',
              name: 'Super Admin',
              email: 'developer@surfline.com',
            },
            highlights: [],
          },
          spots: [],
        },
      });

      regionApi.fetchRegionalOverview.resolves(overview);

      const subregionId = '58581a836630e24c44878ff9';

      const expectedActions = [
        {
          type: FETCH_SUBREGION_OVERVIEW_DATA,
          subregionId,
        },
        {
          type: FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS,
          subregionId,
          overview,
        },
      ];

      await store.dispatch(fetchOverviewData(subregionId));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(regionApi.fetchRegionalOverview).to.have.been.calledOnce();
      expect(regionApi.fetchRegionalOverview).to.have.been.calledWithExactly(
        subregionId,
        undefined,
        undefined,
        undefined,
        false,
      );
    });

    it('dispatches INTERNAL_SERVER_ERROR on error for subregion page', async () => {
      const store = mockStore({});
      regionApi.fetchRegionalOverview.rejects({ message: 'Some error occurred', status: 500 });

      const subregionId = '58581a836630e24c44878ff8';
      const expectedActions = [
        {
          type: FETCH_SUBREGION_OVERVIEW_DATA,
          subregionId,
        },
        {
          type: FETCH_SUBREGION_OVERVIEW_DATA_FAILURE,
          subregionId,
        },
        {
          type: INTERNAL_SERVER_ERROR,
          message: 'The server could not return the Regional Forecast Overview',
        },
      ];
      await store.dispatch(fetchOverviewData(subregionId, undefined, undefined, true));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(regionApi.fetchRegionalOverview).to.have.been.calledOnce();
      expect(regionApi.fetchRegionalOverview).to.have.been.calledWithExactly(
        subregionId,
        undefined,
        undefined,
        undefined,
        false,
      );
    });

    it('should not dispatch INTERNAL_SERVER_ERROR on error for spot page', async () => {
      const store = mockStore({});
      regionApi.fetchRegionalOverview.rejects({ message: 'Some error occurred', status: 500 });

      const subregionId = '58581a836630e24c44878ff8';
      const expectedActions = [
        {
          type: FETCH_SUBREGION_OVERVIEW_DATA,
          subregionId,
        },
        {
          type: FETCH_SUBREGION_OVERVIEW_DATA_FAILURE,
          subregionId,
        },
      ];
      await store.dispatch(fetchOverviewData(subregionId, undefined, undefined, false));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(regionApi.fetchRegionalOverview).to.have.been.calledOnce();
      expect(regionApi.fetchRegionalOverview).to.have.been.calledWithExactly(
        subregionId,
        undefined,
        undefined,
        undefined,
        false,
      );
    });

    it('dispatches FETCH_REGIONAL_ARTICLES_SUCCESS', async () => {
      const store = mockStore({
        subregion: {
          feed: {
            loading: false,
            error: null,
            articles: [],
          },
        },
      });

      const articles = {
        data: {
          regional: [{ content: { body: '<div>article</div>' } }],
        },
      };
      regionApi.fetchRegionalArticles.resolves(articles);

      const subregionId = '58581a836630e24c44878ff9';

      const sanitizedArticles = articles.data.regional.map((article) => ({
        ...article,
        content: {
          ...article.content,
          body: sanitizeArticleHTML(article.content.body),
        },
      }));

      const expectedActions = [
        {
          type: FETCH_REGIONAL_ARTICLES,
        },
        {
          type: FETCH_REGIONAL_ARTICLES_SUCCESS,
          articles: sanitizedArticles,
        },
      ];

      await store.dispatch(fetchRegionalArticles(subregionId));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(regionApi.fetchRegionalArticles).to.have.been.calledOnce();
    });

    it('dispatches FETCH_REGIONAL_ARTICLES_FAILURE on error', async () => {
      const store = mockStore({
        subregion: {
          feed: {
            loading: false,
            error: null,
            articles: [],
          },
        },
      });
      regionApi.fetchRegionalArticles.rejects({ message: 'Some error occurred', status: 500 });

      const subregionId = '58581a836630e24c44878ff8';
      const expectedActions = [
        {
          type: FETCH_REGIONAL_ARTICLES,
        },
        {
          type: FETCH_REGIONAL_ARTICLES_FAILURE,
          error: 'Some error occurred',
        },
      ];

      await store.dispatch(fetchRegionalArticles(subregionId));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(regionApi.fetchRegionalArticles).to.have.been.calledOnce();
    });
  });
});
