import { nrNoticeError } from '@surfline/web-common';
import { buoysAPI } from '../common/api/buoys';
import { AppDispatch } from '../stores';
import { redirectToNotFound } from './status';

const {
  useGetNearbyBuoysQuery,
  useGetBuoysInBoundingBoxQuery,
  useGetBuoyDetailsQuery,
  useGetBuoyReportQuery,
  endpoints,
} = buoysAPI;

const serverSideFetchBuoyDetails = (id: string) => async (dispatch: AppDispatch) => {
  const { error } = (await dispatch(endpoints.getBuoyDetails.initiate(id))) as {
    error?: { status: number; name: string; message: string };
  };

  if (error) {
    if (error?.status === 404) {
      return dispatch(redirectToNotFound('Buoy not found'));
    }

    nrNoticeError(error, {});
  }
  return null;
};

export {
  useGetBuoyDetailsQuery,
  useGetBuoyReportQuery,
  useGetBuoysInBoundingBoxQuery,
  useGetNearbyBuoysQuery,
  serverSideFetchBuoyDetails,
};
