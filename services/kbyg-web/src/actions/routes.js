export const LEAVE_SPOT_ROUTE = 'LEAVE_SPOT_ROUTE';
export const LEAVE_GEONAME_MAP_ROUTE = 'LEAVE_GEONAME_MAP_ROUTE';

export const leaveSpotRoute = () => ({ type: LEAVE_SPOT_ROUTE });
export const leaveGeonameMapRoute = () => ({ type: LEAVE_GEONAME_MAP_ROUTE });
