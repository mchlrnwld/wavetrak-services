import { createAction } from '@reduxjs/toolkit';
import * as spotApi from '../common/api/spot';
import * as feedAPI from '../common/api/feed';
import getUrlBuoyTitle from '../utils/getUrlBuoyTitle';
import getFeedGeotarget from '../utils/getFeedGeotarget';
import { initializeMultiCam, setPrimaryCam } from './multiCam';
import { NOT_FOUND, INTERNAL_SERVER_ERROR } from './status';
import config from '../config';
import { getActiveMeter } from '../selectors/meter';

const SPOT_LOADING = 'SPOT_LOADING';
/** @type {import('@reduxjs/toolkit').ActionCreatorWithPayload<boolean, typeof SPOT_LOADING>} */
export const setSpotLoading = createAction(SPOT_LOADING);

/**
 * Accepts a spot report result and removes
 * travel details if its status is not PUBLISHED,
 * otherwise, returns the object whole.
 * @param {Object}
 */
const attachTravelDetails = ({ travelDetails, ...spotReport }) =>
  travelDetails.status === 'PUBLISHED'
    ? {
        ...spotReport,
        travelDetails,
      }
    : {
        ...spotReport,
      };

export const FETCH_SPOT_REPORT = 'FETCH_SPOT_REPORT';
export const FETCH_SPOT_REPORT_SUCCESS = 'FETCH_SPOT_REPORT_SUCCESS';

/**
 * @param {{ spotId: string, query: Record<string, string | string[] | undefined> }} args
 * @param {function} callback
 * @param {Record<string, string | string[]>} cookies
 * @param {string | undefined | null} [clientIp]
 */
export const fetchSpotReport =
  (
    { spotId, query = {} },
    callback = () => {},
    cookies = null,
    clientIp = null,
    sevenRatings = false,
  ) =>
  async (dispatch, getState) => {
    dispatch({ type: FETCH_SPOT_REPORT });
    let spotReport;
    try {
      const { camId, corrected } = query;
      const state = getState();
      const meter = getActiveMeter(state);
      spotReport = await spotApi.fetchReport(
        spotId,
        cookies,
        meter,
        corrected,
        clientIp,
        sevenRatings,
      );
      const { spot, report, forecast, associated } = spotReport;

      if (spotReport?.spot?.cameras?.length) {
        const initialCameras = spotReport.spot.cameras;
        dispatch(initializeMultiCam(initialCameras, query.camId));
        const primaryCam =
          initialCameras.length && camId && initialCameras.find((cam) => cam._id === camId);
        if (primaryCam) {
          dispatch(setPrimaryCam(primaryCam));
        }
      }

      dispatch({
        type: FETCH_SPOT_REPORT_SUCCESS,
        data: {
          spot: {
            ...attachTravelDetails(spot),
          },
          report,
          forecast,
        },
        associated,
        legacySpotId: associated.advertising.spotId,
        legacySubregionId: associated.advertising.subregionId,
      });
    } catch (err) {
      if (400 <= err.statusCode && err.statusCode < 500) {
        return dispatch({
          type: NOT_FOUND,
          message: 'The Spot Report Could not be found',
        });
      }
      return dispatch({
        type: INTERNAL_SERVER_ERROR,
        message: 'The server could not return the Spot Report',
      });
    }

    return callback(spotReport);
  };

export const FETCH_NEARBY_SPOTS = 'FETCH_NEARBY_SPOTS';
export const FETCH_NEARBY_SPOTS_SUCCESS = 'FETCH_NEARBY_SPOTS_SUCCESS';
export const FETCH_NEARBY_SPOTS_FAILURE = 'FETCH_NEARBY_SPOTS_FAILURE';

/**
 * @param {string} spotId
 * @param {Record<string, string | string[]>} cookies
 * @param {string | undefined | null} [clientIp]
 */
export const fetchNearbySpots =
  (spotId, cookies, clientIp = null, sevenRatings = false) =>
  async (dispatch, getState) => {
    dispatch({ type: FETCH_NEARBY_SPOTS });
    try {
      const state = getState();
      const meter = getActiveMeter(state);
      const nearbySpots = await spotApi.fetchNearbySpots(
        spotId,
        cookies,
        meter,
        clientIp,
        sevenRatings,
      );
      dispatch({
        type: FETCH_NEARBY_SPOTS_SUCCESS,
        nearbySpots,
      });
    } catch (err) {
      dispatch({
        type: FETCH_NEARBY_SPOTS_FAILURE,
        error: err.message,
      });
    }
  };

export const FETCH_SPOT_DETAILS = 'FETCH_SPOT_DETAILS';
export const FETCH_SPOT_DETAILS_SUCCESS = 'FETCH_SPOT_DETAILS_SUCCESS';
export const FETCH_SPOT_DETAILS_FAILURE = 'FETCH_SPOT_DETAILS_FAILURE';

export const fetchSpotDetails =
  (spotId, cookies, callback = () => {}) =>
  async (dispatch) => {
    dispatch({ type: FETCH_SPOT_DETAILS });
    let details;
    try {
      details = await spotApi.fetchSpotDetails(spotId, cookies);
      dispatch({
        type: FETCH_SPOT_DETAILS_SUCCESS,
        details,
      });
    } catch (err) {
      dispatch({
        type: FETCH_SPOT_DETAILS_FAILURE,
        error: err.message,
      });
    }

    return callback(details);
  };

export const FETCH_BUOYS = 'FETCH_BUOYS';
export const FETCH_BUOYS_SUCCESS = 'FETCH_BUOYS_SUCCESS';
export const FETCH_BUOYS_FAILURE = 'FETCH_BUOYS_FAILURE';
export const CLEAR_BUOYS = 'CLEAR_BUOYS';

export const fetchBuoys =
  (legacySpotId, legacySubregionId, { units, timezone }) =>
  async (dispatch) => {
    dispatch({ type: FETCH_BUOYS });
    try {
      const buoyResponse = await spotApi.fetchBuoys(legacySpotId, units);
      const buoyImagePaths = buoyResponse.buoys.map(({ buoy_id: buoyId, title }) => ({
        url: `http://www.surfline.com/buoy-report/${getUrlBuoyTitle(
          title,
        )}_${legacySubregionId}_${buoyId}/`,
        img: `${config.buoyPlotApi}?buoy_id=${buoyId}&bp_timezone=${timezone}&units=${units}`,
      }));
      dispatch({
        type: FETCH_BUOYS_SUCCESS,
        buoys: buoyImagePaths,
      });
    } catch (error) {
      dispatch({
        type: FETCH_BUOYS_FAILURE,
        error: error.message,
      });
    }
  };

export const clearBuoys = () => ({
  type: CLEAR_BUOYS,
});

export const FETCH_FEATURED_CONTENT = 'FETCH_FEATURED_CONTENT';
export const FETCH_FEATURED_CONTENT_SUCCESS = 'FETCH_FEATURED_CONTENT_SUCCESS';
export const FETCH_FEATURED_CONTENT_FAILURE = 'FETCH_FEATURED_CONTENT_FAILURE';

export const fetchFeaturedContent = (cookies) => async (dispatch, getState) => {
  dispatch({
    type: FETCH_FEATURED_CONTENT,
  });

  try {
    const limit = 7;
    const state = getState();
    const geotarget = getFeedGeotarget(state);
    const payload = await feedAPI.fetchPromoBoxSlides(cookies, geotarget, limit);
    dispatch({
      type: FETCH_FEATURED_CONTENT_SUCCESS,
      featuredContent: payload.data.promobox,
    });
  } catch (error) {
    dispatch({
      type: FETCH_FEATURED_CONTENT_FAILURE,
      error: error.message,
    });
  }
};
