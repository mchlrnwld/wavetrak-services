import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import sinon from 'sinon';

import {
  SET_MAPV2_LOCATION,
  setMapLocation,
  SET_MAPV2_LOCATION_LOADED,
  setMapLocationLoaded,
  SET_MAPV2_BOUNDING_BOX,
  setMapLoading,
  setMapError,
  setMapBoundingBox,
  SET_MAPV2_LOADING,
  SET_MAPV2_ERROR,
  SET_MAPV2_MESSAGE,
  setMapMessage,
  fetchLocationView,
  resetMapState,
  RESET_MAP_STATE,
  zoomIn,
  ZOOM_IN,
} from './mapV2';

import * as api from '../common/fetch';
import { getLocationViewFixture } from '../utils/fixtures/store';
import { NOT_FOUND } from './status';

describe('actions / mapV2', () => {
  let mockStore;

  /** @type {sinon.SinonStub} */
  let fetchLocationMapViewStub;

  beforeAll(() => {
    mockStore = configureStore([thunk]);
  });

  beforeEach(() => {
    fetchLocationMapViewStub = sinon.stub(api, 'fetchLocationMapView');
  });

  afterEach(() => {
    fetchLocationMapViewStub.restore();
  });

  describe('setMapLocation', () => {
    it('should send the set map location action', () => {
      const center = { lat: 10, lon: 10 };
      const zoom = 12;
      const store = mockStore({});

      store.dispatch(setMapLocation({ center, zoom }));

      expect(store.getActions()).to.containSubset([
        {
          type: SET_MAPV2_LOCATION,
          payload: {
            center,
            zoom,
          },
        },
      ]);
    });
  });

  describe('setMapLocationLoaded', () => {
    it('should send the set map location loaded action', () => {
      const store = mockStore({});

      store.dispatch(setMapLocationLoaded());

      expect(store.getActions()).to.containSubset([
        {
          type: SET_MAPV2_LOCATION_LOADED,
        },
      ]);
    });
  });

  describe('setMapBoundingBox', () => {
    it('should send the set map bounding box action', () => {
      const boundingBox = { north: 0, south: 0, east: 0, west: 0 };
      const store = mockStore({});

      store.dispatch(setMapBoundingBox(boundingBox));

      expect(store.getActions()).to.containSubset([
        {
          type: SET_MAPV2_BOUNDING_BOX,
          payload: boundingBox,
        },
      ]);
    });
  });

  describe('setMapError', () => {
    it('should send the set map error action', () => {
      const error = { message: 'test', status: 500 };
      const store = mockStore({});

      store.dispatch(setMapError(error));

      expect(store.getActions()).to.containSubset([
        {
          type: SET_MAPV2_ERROR,
          payload: error,
        },
      ]);
    });
  });

  describe('setMapLoading', () => {
    it('should send the set map loading action', () => {
      const store = mockStore({});

      store.dispatch(setMapLoading(true));

      expect(store.getActions()).to.containSubset([
        {
          type: SET_MAPV2_LOADING,
          payload: true,
        },
      ]);
    });
  });

  describe('setMapMessage', () => {
    it('should send the set map message action', () => {
      const store = mockStore({});

      store.dispatch(setMapMessage('message'));

      expect(store.getActions()).to.containSubset([
        {
          type: SET_MAPV2_MESSAGE,
          payload: 'message',
        },
      ]);
    });
  });

  describe('fetchLocationView', () => {
    it('should fetch a location view using a geonameId number', async () => {
      const store = mockStore({});

      const locationView = getLocationViewFixture();
      fetchLocationMapViewStub.resolves(locationView);
      await store.dispatch(fetchLocationView(1234));

      expect(fetchLocationMapViewStub).to.have.been.calledOnceWithExactly(1234);
      expect(store.getActions()).to.containSubset([
        {
          type: fetchLocationView.pending.toString(),
        },
        {
          type: fetchLocationView.fulfilled.toString(),
          payload: locationView,
        },
      ]);
    });

    it('should fetch a location view using a geonameId string', async () => {
      const store = mockStore({});

      const locationView = getLocationViewFixture();
      fetchLocationMapViewStub.resolves(locationView);
      await store.dispatch(fetchLocationView('1234'));

      expect(fetchLocationMapViewStub).to.have.been.calledOnceWithExactly('1234');

      expect(store.getActions()).to.containSubset([
        {
          type: fetchLocationView.pending.toString(),
        },
        {
          type: fetchLocationView.fulfilled.toString(),
          payload: locationView,
        },
      ]);
    });

    it('should error if no geoname is passed', async () => {
      const store = mockStore({});

      const locationView = getLocationViewFixture();
      fetchLocationMapViewStub.resolves(locationView);
      await store.dispatch(fetchLocationView());

      expect(fetchLocationMapViewStub).to.have.not.been.called();
      expect(store.getActions()).to.containSubset([
        {
          type: fetchLocationView.pending.toString(),
        },
        {
          type: fetchLocationView.rejected.toString(),
          error: {
            message: 'geonameId is not a number',
          },
        },
      ]);
    });

    it('should error if invalid geoname is passed', async () => {
      const store = mockStore({});

      await store.dispatch(fetchLocationView('asdf'));

      expect(fetchLocationMapViewStub).to.have.not.been.called();
      expect(store.getActions()).to.containSubset([
        {
          type: fetchLocationView.pending.toString(),
        },
        {
          type: fetchLocationView.rejected.toString(),
          error: {
            message: 'geonameId is not a number',
          },
        },
      ]);
    });

    it('should dispatch not found if the API returns a 400', async () => {
      const store = mockStore({});

      fetchLocationMapViewStub.rejects({ status: 400 });

      await store.dispatch(fetchLocationView(1234));

      expect(fetchLocationMapViewStub).to.have.been.calledOnceWithExactly(1234);
      expect(store.getActions()).to.containSubset([
        {
          type: fetchLocationView.pending.toString(),
        },
        {
          type: NOT_FOUND,
          message: 'The location could not be found',
        },
        {
          type: fetchLocationView.rejected.toString(),
          error: {
            message: 'The location could not be found',
          },
        },
      ]);
    });
  });

  describe('resetMapState', () => {
    it('should dispatch the reset map state action', () => {
      const store = mockStore({});

      store.dispatch(resetMapState());

      expect(store.getActions()).to.containSubset([
        {
          type: RESET_MAP_STATE,
        },
      ]);
    });
  });

  describe('zoomIn', () => {
    it('should dispatch the zoomIn action without a payload', () => {
      const store = mockStore({});

      store.dispatch(zoomIn());

      expect(store.getActions()).to.containSubset([
        {
          type: ZOOM_IN,
        },
      ]);
    });

    it('should dispatch the zoomIn action with a payload', () => {
      const store = mockStore({});

      store.dispatch(zoomIn({ lat: 1, lon: 1 }));

      expect(store.getActions()).to.containSubset([
        {
          type: ZOOM_IN,
          payload: { lat: 1, lon: 1 },
        },
      ]);
    });
  });
});
