import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { getWindow } from '@surfline/web-common';
import * as fetch from '../common/fetch';
import * as location from '../common/location';
import { mapPath } from '../utils/urls';
import {
  SET_MAP_LOCATION,
  setMapLocation,
  goToUsersLocation,
  goToUsersNearestSpot,
  zoomIntoLocation,
} from './map';

import { createMockRouter } from '../utils/test-utils';

describe('actions / map', () => {
  const win = getWindow();

  const getItemStub = sinon.stub();
  const setItemStub = sinon.stub();
  const sessionStorageMock = {
    getItem: getItemStub,
    setItem: setItemStub,
  };

  let mockStore;
  const replaceStub = sinon.stub();

  const surflineLocation = deepFreeze({
    lat: 33.654213041213,
    lon: -118.0032588192,
  });

  const { sessionStorage } = win;

  beforeAll(() => {
    mockStore = configureStore([thunk]);

    Object.defineProperty(win, 'sessionStorage', {
      value: sessionStorageMock,
    });
  });

  afterEach(() => {
    replaceStub.reset();
    getItemStub.reset();
    setItemStub.reset();
  });

  afterAll(() => {
    Object.defineProperty(win, 'sessionStorage', {
      value: sessionStorage,
    });
  });

  describe('setMapLocation', () => {
    it('replaces location in history if necessary and syncUrl true', () => {
      const center = surflineLocation;
      const zoom = 12;
      const pathname = `${mapPath(center, zoom)}`;
      const store = mockStore({});

      const router = createMockRouter({ asPath: '/map', replace: replaceStub });

      store.dispatch(setMapLocation({ center, zoom }, router, true));
      expect(replaceStub).to.have.been.calledOnce();
      expect(replaceStub).to.have.been.calledWithExactly(pathname);
      expect(setItemStub).not.to.have.been.called();

      const router2 = createMockRouter({ asPath: pathname, replace: replaceStub });
      replaceStub.reset();
      store.dispatch(setMapLocation({ center, zoom }, router2, true));
      expect(replaceStub).not.to.have.been.called();
      expect(setItemStub).not.to.have.been.called();
    });

    it('replaces location in session storage if storeLocationInSession true', () => {
      const center = surflineLocation;
      const zoom = 12;
      const store = mockStore({});

      const router = createMockRouter({ asPath: '/map', replace: replaceStub });

      store.dispatch(setMapLocation({ center, zoom }, router, false, true));
      expect(replaceStub).not.to.have.been.called();
      expect(setItemStub).to.have.been.calledOnce();
      expect(setItemStub).to.have.been.calledWithExactly(
        'slMapLocation',
        JSON.stringify({ center, zoom }),
      );
    });

    it('dispatches SET_MAP_LOCATION', () => {
      const center = surflineLocation;
      const zoom = 12;
      const store = mockStore({});
      const router = createMockRouter({ asPath: '/map', replace: replaceStub });

      store.dispatch(setMapLocation({ center, zoom }, router));
      expect(store.getActions()).to.deep.equal([
        {
          type: SET_MAP_LOCATION,
          center,
          zoom,
        },
      ]);
    });
  });

  describe('goToUsersLocation', () => {
    beforeEach(() => {
      sinon.stub(fetch, 'getUserRegion');
    });

    afterEach(() => {
      fetch.getUserRegion.restore();
    });

    it("dispatches setMapLocation with user's location as center", async () => {
      const userRegion = {
        location: {
          latitude: 33.593257,
          longitude: -117.880932,
        },
      };
      const store = mockStore({
        map: {
          location: { zoom: 12 },
        },
      });
      const pathname = `${mapPath(
        {
          lat: userRegion.location.latitude,
          lon: userRegion.location.longitude,
        },
        5,
      )}`;
      const router = createMockRouter({ asPath: '/map', replace: replaceStub });
      fetch.getUserRegion.resolves(userRegion);

      const center = {
        lat: userRegion.location.latitude,
        lon: userRegion.location.longitude,
      };
      await store.dispatch(goToUsersLocation(router, true, true));
      expect(store.getActions()).to.deep.equal([
        {
          type: SET_MAP_LOCATION,
          center,
          zoom: 5,
        },
      ]);
      expect(fetch.getUserRegion).to.have.been.calledOnce();
      expect(replaceStub).to.have.been.calledOnce();
      expect(replaceStub).to.have.been.calledWithExactly(pathname);
      expect(setItemStub).to.have.been.calledOnce();
      expect(setItemStub).to.have.been.calledWithExactly(
        'slMapLocation',
        JSON.stringify({ center, zoom: 5 }),
      );
    });

    it("defaults to Surfline location if unable to get user's location", async () => {
      const store = mockStore({
        map: {
          location: { zoom: 12 },
        },
      });
      const pathname = `${mapPath(surflineLocation, 12)}`;
      const router = createMockRouter({ asPath: '/map', replace: replaceStub });
      fetch.getUserRegion.rejects();

      await store.dispatch(goToUsersLocation(router, true, true));
      expect(store.getActions()).to.deep.equal([
        {
          type: SET_MAP_LOCATION,
          center: surflineLocation,
          zoom: 12,
        },
      ]);
      expect(fetch.getUserRegion).to.have.been.calledOnce();
      expect(replaceStub).to.have.been.calledOnce();
      expect(replaceStub).to.have.been.calledWithExactly(pathname);
      expect(setItemStub).to.have.been.calledOnce();
      expect(setItemStub).to.have.been.calledWithExactly(
        'slMapLocation',
        JSON.stringify({ center: surflineLocation, zoom: 12 }),
      );
    });
  });

  describe('goToUsersNearestSpot', () => {
    beforeEach(() => {
      sinon.stub(fetch, 'getUserRegion');
      sinon.stub(fetch, 'getNearestSpot');
      sinon.stub(location, 'getUserLocationByGPS');
      sinon.stub(location, 'getUserLocationByIP');
    });

    afterEach(() => {
      fetch.getUserRegion.restore();
      fetch.getNearestSpot.restore();
      location.getUserLocationByGPS.restore();
      location.getUserLocationByIP.restore();
    });

    it("dispatches setMapLocation with spot nearest user's location as center", async () => {
      const loc = {
        center: {
          latitude: 33.593257,
          longitude: -117.880932,
        },
        zoom: 12,
      };
      const nearestSpot = {
        spot: {
          lat: 33.5930302087,
          lon: -117.8819918632,
        },
      };
      const store = mockStore({
        map: {
          location: { zoom: 12 },
        },
      });
      const pathname = `${mapPath(nearestSpot.spot, 12)}`;

      const router = createMockRouter({ asPath: '/map', replace: replaceStub });
      location.getUserLocationByGPS.resolves(loc);
      location.getUserLocationByIP.resolves(loc);
      fetch.getNearestSpot.resolves(nearestSpot);

      const center = {
        lat: nearestSpot.spot.lat,
        lon: nearestSpot.spot.lon,
      };
      await store.dispatch(goToUsersNearestSpot(router, true, true));
      expect(store.getActions()).to.deep.equal([
        {
          type: SET_MAP_LOCATION,
          center,
          zoom: 12,
        },
        {
          type: SET_MAP_LOCATION,
          center,
          zoom: 12,
        },
      ]);
      expect(fetch.getNearestSpot).to.have.been.calledTwice();
      expect(fetch.getNearestSpot).to.have.been.calledWithExactly({ ...loc.center });
      expect(replaceStub).to.have.been.calledTwice();
      expect(replaceStub).to.have.been.calledWithExactly(pathname);
      expect(setItemStub).to.have.been.calledTwice();
      expect(setItemStub).to.have.been.calledWithExactly(
        'slMapLocation',
        JSON.stringify({ center, zoom: 12 }),
      );
    });

    it("defaults to Surfline location if unable to get user's location", async () => {
      const store = mockStore({
        map: {
          location: { zoom: 12 },
        },
      });
      const pathname = `${mapPath(surflineLocation, 12)}`;
      const router = createMockRouter({ asPath: '/map', replace: replaceStub });
      location.getUserLocationByGPS.rejects();
      location.getUserLocationByIP.rejects();

      await store.dispatch(goToUsersNearestSpot(router, true, true));
      expect(store.getActions()).to.deep.equal([
        {
          type: SET_MAP_LOCATION,
          center: surflineLocation,
          zoom: 12,
        },
        {
          type: SET_MAP_LOCATION,
          center: surflineLocation,
          zoom: 12,
        },
      ]);
      expect(fetch.getNearestSpot).not.to.have.been.called();
      expect(replaceStub).to.have.been.calledTwice();
      expect(replaceStub).to.have.been.calledWithExactly(pathname);
      expect(setItemStub).to.have.been.calledTwice();
      expect(setItemStub).to.have.been.calledWithExactly(
        'slMapLocation',
        JSON.stringify({ center: surflineLocation, zoom: 12 }),
      );
    });

    it('defaults to Surfline location if unable to get nearest spot', async () => {
      const loc = {
        center: {
          latitude: 33.593257,
          longitude: -117.880932,
        },
        zoom: 12,
      };
      const store = mockStore({
        map: {
          location: { zoom: 12 },
        },
      });
      const pathname = `${mapPath(surflineLocation, 12)}`;
      const router = createMockRouter({ asPath: '/map', replace: replaceStub });
      location.getUserLocationByGPS.resolves(loc);
      location.getUserLocationByIP.resolves(loc);
      fetch.getNearestSpot.rejects();

      await store.dispatch(goToUsersNearestSpot(router, true, true));
      expect(store.getActions()).to.deep.equal([
        {
          type: SET_MAP_LOCATION,
          center: surflineLocation,
          zoom: 12,
        },
        {
          type: SET_MAP_LOCATION,
          center: surflineLocation,
          zoom: 12,
        },
      ]);
      expect(fetch.getNearestSpot).to.have.been.calledTwice();
      expect(fetch.getNearestSpot).to.have.been.calledWithExactly({ ...loc.center });
      expect(replaceStub).to.have.been.calledTwice();
      expect(replaceStub).to.have.been.calledWithExactly(pathname);
      expect(setItemStub).to.have.been.calledTwice();
      expect(setItemStub).to.have.been.calledWithExactly(
        'slMapLocation',
        JSON.stringify({ center: surflineLocation, zoom: 12 }),
      );
    });
  });

  describe('zoomIntoLocation', () => {
    it('dispatches setMapLocation with zoom + 1 and new center', () => {
      const center = {
        lat: 33.593257,
        lon: -117.880932,
      };
      const store = mockStore({
        map: {
          location: {
            center: surflineLocation,
            zoom: 12,
          },
        },
      });
      const pathname = `${mapPath(center, 13)}`;
      const router = createMockRouter({ asPath: '/map', replace: replaceStub });

      store.dispatch(zoomIntoLocation(center, router, true, true));
      expect(store.getActions()).to.deep.equal([
        {
          type: SET_MAP_LOCATION,
          center,
          zoom: 13,
        },
      ]);
      expect(replaceStub).to.have.been.calledOnce();
      expect(replaceStub).to.have.been.calledWithExactly(pathname);

      expect(setItemStub).to.have.been.calledOnceWithExactly(
        'slMapLocation',
        JSON.stringify({ center, zoom: 13 }),
      );
    });
  });
});
