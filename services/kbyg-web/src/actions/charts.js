import _get from 'lodash/get';
import { getUserSettings, nrNoticeError } from '@surfline/web-common';
import * as chartsApi from '../common/api/charts';
import { formatChartTypes } from '../utils/basinCharts';
import { getActiveMeter } from '../selectors/meter';

export const FETCH_SUBREGION_CHART_IMAGES = 'FETCH_SUBREGION_CHART_IMAGES';
export const FETCH_SUBREGION_CHART_IMAGES_SUCCESS = 'FETCH_SUBREGION_CHART_IMAGES_SUCCESS';
export const FETCH_SUBREGION_CHART_IMAGES_FAILURE = 'FETCH_SUBREGION_CHART_IMAGES_FAILURE';

export const FETCH_SUBREGION_CHART_MENU = 'FETCH_SUBREGION_CHART_MENU';
export const FETCH_SUBREGION_CHART_MENU_SUCCESS = 'FETCH_SUBREGION_CHART_MENU_SUCCESS';
export const FETCH_SUBREGION_CHART_MENU_FAILURE = 'FETCH_SUBREGION_CHART_MENU_FAILURE';

export const SET_ACTIVE_CHART = 'SET_ACTIVE_CHART';

export const fetchChartImages = (type, nearshoreModelName) => async (dispatch, getState) => {
  dispatch({
    type: FETCH_SUBREGION_CHART_IMAGES,
  });
  try {
    const state = getState();
    const legacyId = _get(state, 'subregion.overview.associated.legacyId', null);
    const meter = getActiveMeter(state);
    const userSettings = getUserSettings(state);
    const tempUnit = userSettings?.units?.temperature;
    const {
      data: { images },
      associated: { timezone },
    } = await chartsApi.fetchLegacyChartImages(legacyId, type, nearshoreModelName, meter, tempUnit);
    dispatch({
      type: FETCH_SUBREGION_CHART_IMAGES_SUCCESS,
      images,
      timezone,
    });
  } catch (error) {
    dispatch({
      type: FETCH_SUBREGION_CHART_IMAGES_FAILURE,
      error: error.message,
    });
  }
};

export const fetchChartTypes = (subregionId, cookies) => async (dispatch, getState) => {
  dispatch({
    type: FETCH_SUBREGION_CHART_MENU,
  });
  try {
    const state = getState();
    const meter = getActiveMeter(state);
    const {
      data: { types },
      associated: { region },
    } = await chartsApi.fetchLegacyChartTypes(subregionId, cookies, meter);

    const formattedTypes = formatChartTypes(types);
    dispatch({
      type: FETCH_SUBREGION_CHART_MENU_SUCCESS,
      types: formattedTypes,
      region,
    });
  } catch (error) {
    if (error.status === 400) {
      nrNoticeError(error);
    }
    dispatch({
      type: FETCH_SUBREGION_CHART_MENU_FAILURE,
      error: error.message,
    });
  }
};

// find the chart using the slug as a lookup
const getActiveChartBySlug = (slug, types) => {
  let activeChart = {};
  if (types) {
    Object.keys(types).forEach((category) => {
      types[category].forEach((chart) => {
        if (chart.slug === slug) {
          activeChart = {
            ...chart,
            category,
          };
        }
      });
    });
  }
  return activeChart;
};

export const setActiveChart = (slug, subregionId, cookies) => async (dispatch, getState) => {
  const state = getState();
  let chartTypes = _get(state, 'components.charts.types', null);

  if (!chartTypes) {
    await dispatch(fetchChartTypes(subregionId, cookies));
    chartTypes = _get(getState(), 'components.charts.types', []);
  }

  const active = getActiveChartBySlug(slug, chartTypes);

  dispatch({
    type: SET_ACTIVE_CHART,
    active,
  });
};
