import type { Request } from 'express';
import { Dispatch } from 'redux';
import UAParser from 'ua-parser-js';

export const FETCH_OS_DATA = 'FETCH_OS_DATA';
export const FETCH_OS_DATA_SUCCESS = 'FETCH_OS_DATA_SUCCESS';
export const FETCH_OS_DATA_FAILURE = 'FETCH_OS_DATA_FAILURE';

export const fetchOSData = (headers: Request['headers']) => (dispatch: Dispatch) => {
  dispatch({
    type: FETCH_OS_DATA,
  });
  try {
    const userAgent = headers['user-agent'];
    const parser = new UAParser();
    const ua = userAgent;
    const osName = parser.setUA(ua || '').getOS().name;
    const osData = { osName };
    dispatch({
      type: FETCH_OS_DATA_SUCCESS,
      osData,
    });
  } catch (error) {
    const err = error as Error;
    dispatch({
      type: FETCH_OS_DATA_FAILURE,
      error: err.message,
    });
  }
};
