import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import {
  LEAVE_SPOT_ROUTE,
  LEAVE_GEONAME_MAP_ROUTE,
  leaveSpotRoute,
  leaveGeonameMapRoute,
} from './routes';

describe('actions / routes', () => {
  let mockStore;

  beforeAll(() => {
    mockStore = configureStore();
  });

  describe('leaveSpotRoute', () => {
    it('dispatches LEAVE_SPOT_ROUTE', () => {
      const store = mockStore({});
      store.dispatch(leaveSpotRoute());
      expect(store.getActions()).to.deep.equal([{ type: LEAVE_SPOT_ROUTE }]);
    });
    it('dispatches LEAVE_GEONAME_MAP_ROUTE', () => {
      const store = mockStore({});
      store.dispatch(leaveGeonameMapRoute());
      expect(store.getActions()).to.deep.equal([{ type: LEAVE_GEONAME_MAP_ROUTE }]);
    });
  });
});
