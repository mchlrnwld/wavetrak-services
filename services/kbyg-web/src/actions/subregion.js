import { createAction } from '@reduxjs/toolkit';
import * as regionApi from '../common/api/region';
import { NOT_FOUND, INTERNAL_SERVER_ERROR } from './status';
import sanitizeArticleHTML from '../utils/sanitizeArticleHTML';
import { getActiveMeter } from '../selectors/meter';

export const SET_SUBREGION_LOADING = 'SET_SUBREGION_LOADING';

/** @type {import('@reduxjs/toolkit').ActionCreatorWithOptionalPayload<boolean>} */
export const setSubregionLoading = createAction(SET_SUBREGION_LOADING);

export const CLEAR_SUBREGION_OVERVIEW_DATA = 'CLEAR_SUBREGION_OVERVIEW_DATA';
export const FETCH_SUBREGION_OVERVIEW_DATA = 'FETCH_SUBREGION_OVERVIEW_DATA';
export const FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS = 'FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS';
export const FETCH_SUBREGION_OVERVIEW_DATA_FAILURE = 'FETCH_SUBREGION_OVERVIEW_DATA_FAILURE';

export const FETCH_REGIONAL_ARTICLES = 'FETCH_REGIONAL_ARTICLES';
export const FETCH_REGIONAL_ARTICLES_SUCCESS = 'FETCH_REGIONAL_ARTICLES_SUCCESS';
export const FETCH_REGIONAL_ARTICLES_FAILURE = 'FETCH_REGIONAL_ARTICLES_FAILURE';

export const fetchOverviewData =
  (subregionId, cookies, clientIp, shouldFiveHundred = true, sevenRatings = false) =>
  async (dispatch, getState) => {
    dispatch({
      type: FETCH_SUBREGION_OVERVIEW_DATA,
      subregionId,
    });

    if (!subregionId) {
      dispatch({
        type: NOT_FOUND,
        message: 'The Regional Forecast Overview could not be found',
      });
    }

    try {
      const state = getState();
      const meter = getActiveMeter(state);

      const overview = await regionApi.fetchRegionalOverview(
        subregionId,
        cookies,
        meter,
        clientIp,
        sevenRatings,
      );

      dispatch({
        type: FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS,
        subregionId,
        overview,
      });
    } catch (err) {
      dispatch({
        type: FETCH_SUBREGION_OVERVIEW_DATA_FAILURE,
        subregionId,
      });
      if (err.statusCode === 400 || err.statusCode === 404) {
        dispatch({
          type: NOT_FOUND,
          message: 'The Regional Forecast Overview could not be found',
        });
      } else if (shouldFiveHundred) {
        dispatch({
          type: INTERNAL_SERVER_ERROR,
          message: 'The server could not return the Regional Forecast Overview',
        });
      }
    }
  };

export const clearOverviewData = () => ({
  type: CLEAR_SUBREGION_OVERVIEW_DATA,
});

export const fetchRegionalArticles = (subregionId, cookies) => async (dispatch) => {
  dispatch({
    type: FETCH_REGIONAL_ARTICLES,
  });

  try {
    const { data } = await regionApi.fetchRegionalArticles(subregionId, cookies);
    const sanitizedArticles = data.regional.map((article) => ({
      ...article,
      content: {
        ...article.content,
        body: sanitizeArticleHTML(article.content.body),
      },
    }));
    dispatch({
      type: FETCH_REGIONAL_ARTICLES_SUCCESS,
      articles: sanitizedArticles,
    });
  } catch (error) {
    dispatch({
      type: FETCH_REGIONAL_ARTICLES_FAILURE,
      error: error.message,
    });
  }
};

export const FETCH_NEARBY_SUBREGIONS = 'FETCH_NEARBY_SUBREGIONS';
export const FETCH_NEARBY_SUBREGIONS_SUCCESS = 'FETCH_NEARBY_SUBREGIONS_SUCCESS';
export const FETCH_NEARBY_SUBREGIONS_FAILURE = 'FETCH_NEARBY_SUBREGIONS_FAILURE';

export const fetchNearbySubregions =
  (subregionId, cookies, sevenRatings = false) =>
  async (dispatch, getState) => {
    dispatch({ type: FETCH_NEARBY_SUBREGIONS });
    try {
      const state = getState();
      const meter = getActiveMeter(state);
      const nearbySubregions = await regionApi.fetchNearbySubregions(
        subregionId,
        cookies,
        meter,
        sevenRatings,
      );
      dispatch({
        type: FETCH_NEARBY_SUBREGIONS_SUCCESS,
        nearbySubregions,
      });
    } catch (err) {
      dispatch({
        type: FETCH_NEARBY_SUBREGIONS_FAILURE,
        error: err.message,
      });
    }
  };
