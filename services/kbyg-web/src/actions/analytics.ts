import { createAction } from '@reduxjs/toolkit';

const ANALYTICS_READY = 'SPLIT_READY';

// eslint-disable-next-line import/prefer-default-export
export const analyticsReady = createAction(ANALYTICS_READY);
