import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as fetch from '../common/fetch';
import {
  FETCH_SPOT_MAP_DATA,
  FETCH_SPOT_MAP_DATA_SUCCESS,
  FETCH_SPOT_MAP_DATA_FAILURE,
  fetchSpotMapData,
} from './spotMapData';

describe('actions / spotMapData', () => {
  describe('fetchSpotMapData', () => {
    let mockStore;

    beforeAll(() => {
      mockStore = configureStore([thunk]);
    });

    beforeEach(() => {
      sinon.stub(fetch, 'getSpotMapData');
    });

    afterEach(() => {
      fetch.getSpotMapData.restore();
    });

    it('dispatches FETCH_SPOT_MAP_DATA_SUCCESS with spotMapData', async () => {
      const store = mockStore({
        map: {
          location: {
            zoom: 12,
          },
        },
      });
      const spotMapData = deepFreeze({
        units: {
          tideHeight: 'FT',
          waveHeight: 'FT',
          windSpeed: 'KT',
        },
        data: {
          spots: [
            {
              _id: '5842041f4e65fad6a77088ed',
              name: 'HB Pier, Southside',
              lat: 33.654213041213,
              lon: -118.0032588192,
              cameras: [],
            },
            {
              _id: '5842041f4e65fad6a7708827',
              name: 'HB Pier, Northside',
              lat: 33.656781041213,
              lon: -118.0064678192,
              cameras: [],
            },
          ],
          regionalForecast: {
            iconUrl: 'https://somecdn.surfline.com/region1.png',
            legacyRegionId: 2143,
            subregionId: '58581a836630e24c44878fd6',
          },
          subregions: [
            {
              _id: '58581a836630e24c44878fd6',
              subregion: {
                name: 'North Orange County',
                id: '58581a836630e24c44878fd6',
                forecasterEmail: 'developer@surfline.com',
              },
            },
          ],
        },
      });
      fetch.getSpotMapData.resolves(spotMapData);

      const bounds = {
        north: 33.707209069466316,
        south: 33.60118435784217,
        east: -117.86936294517659,
        west: -118.13715469322347,
      };
      const expectedActions = [
        {
          type: FETCH_SPOT_MAP_DATA,
        },
        {
          type: FETCH_SPOT_MAP_DATA_SUCCESS,
          regionalForecast: spotMapData.data.regionalForecast,
          subregions: spotMapData.data.subregions,
          spots: spotMapData.data.spots,
          units: spotMapData.units,
          zoom: 12,
          doCluster: true,
        },
      ];
      await store.dispatch(fetchSpotMapData(bounds, true));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(fetch.getSpotMapData).to.have.been.calledOnce();
      expect(fetch.getSpotMapData).to.have.been.calledWithExactly(bounds);
    });

    it('dispatches FETCH_SPOT_MAP_DATA_FAILURE on error', async () => {
      const store = mockStore({});
      fetch.getSpotMapData.rejects({ message: 'Some error occurred' });

      const bounds = {
        north: 33.707209069466316,
        south: 33.60118435784217,
        east: -117.86936294517659,
        west: -118.13715469322347,
      };
      const expectedActions = [
        {
          type: FETCH_SPOT_MAP_DATA,
        },
        {
          type: FETCH_SPOT_MAP_DATA_FAILURE,
          error: 'Some error occurred',
        },
      ];
      await store.dispatch(fetchSpotMapData(bounds));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(fetch.getSpotMapData).to.have.been.calledOnce();
      expect(fetch.getSpotMapData).to.have.been.calledWithExactly(bounds);
    });
  });
});
