import { fetchTaxononmySwellEvents } from '../common/api/kbyg';
import { getActiveMeter } from '../selectors/meter';
import type { AppDispatch, WavetrakStore } from '../stores';
import { SwellEvent } from '../types/swellEvents';

export const FETCH_SWELL_EVENTS = 'FETCH_SWELL_EVENTS';
export const FETCH_SWELL_EVENTS_SUCCESS = 'FETCH_SWELL_EVENTS_SUCCESS';
export const FETCH_SWELL_EVENTS_FAILURE = 'FETCH_SWELL_EVENTS_FAILURE';

export const fetchSwellEvents =
  (id: string, type: string) =>
  async (dispatch: AppDispatch, getState: WavetrakStore['getState']) => {
    dispatch({
      type: FETCH_SWELL_EVENTS,
    });

    try {
      const state = getState();
      const meter = getActiveMeter(state);
      const { data } = (await fetchTaxononmySwellEvents(id, type, meter)) as {
        data: SwellEvent[];
      };
      dispatch({
        type: FETCH_SWELL_EVENTS_SUCCESS,
        events: data,
      });
    } catch (error) {
      dispatch({
        type: FETCH_SWELL_EVENTS_FAILURE,
        error: (error as Error).message,
      });
    }
  };
