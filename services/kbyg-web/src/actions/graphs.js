import { nrNoticeError } from '@surfline/web-common';

import {
  fetchWaveForecast,
  fetchTideForecast,
  fetchWeatherForecast,
  fetchWindForecast,
  fetchRegionalConditionForecast,
  fetchRegionalWaveForecast,
  fetchAutomatedRatingForecast,
} from '../common/api/kbyg';
import { getActiveMeter } from '../selectors/meter';

const baseFetchGraphForecast =
  (action, success, failure, fetch) =>
  /**
   * @param {object} props
   * @param {string} props.id
   * @param {number} props.days
   * @param {number} props.intervalHours
   * @param {boolean} [props.maxHeights]
   * @param {number} [props.desired16DayInterval]
   * @param {boolean} [props.isGraphUpdates]
   * @param {Record<string | string[]>} [props.query={}]
   */
  ({
    id,
    days,
    intervalHours,
    maxHeights = false,
    desired16DayInterval = intervalHours,
    isGraphUpdates = false,
    query = {},
    sevenRatings = false,
  }) =>
  async (dispatch, getState) => {
    dispatch({ type: action });
    try {
      const state = getState();
      const meter = getActiveMeter(state);
      const { corrected, correctedRating, correctedWind, ratingsAlgorithm } = query;
      const response = await fetch({
        id,
        days,
        intervalHours,
        maxHeights,
        corrected,
        correctedRating,
        correctedWind,
        meter,
        ratingsAlgorithm,
        sevenRatings,
      });

      dispatch({
        type: success,
        intervalHours,
        desired16DayInterval,
        response,
        isGraphUpdates,
      });
    } catch (error) {
      if (error.statusCode === 400) {
        nrNoticeError(error);
      }

      dispatch({
        type: failure,
        error: error.message || 'something went wrong',
      });
    }
  };

export const FETCH_GRAPH_CONDITION_FORECAST = 'FETCH_GRAPH_CONDITION_FORECAST';
export const FETCH_GRAPH_CONDITION_FORECAST_SUCCESS = 'FETCH_GRAPH_CONDITION_FORECAST_SUCCESS';
export const FETCH_GRAPH_CONDITION_FORECAST_FAILURE = 'FETCH_GRAPH_CONDITION_FORECAST_FAILURE';

export const fetchGraphConditionForecast = baseFetchGraphForecast(
  FETCH_GRAPH_CONDITION_FORECAST,
  FETCH_GRAPH_CONDITION_FORECAST_SUCCESS,
  FETCH_GRAPH_CONDITION_FORECAST_FAILURE,
  fetchRegionalConditionForecast,
);

export const FETCH_GRAPH_WAVE_FORECAST = 'FETCH_GRAPH_WAVE_FORECAST';
export const FETCH_GRAPH_WAVE_FORECAST_SUCCESS = 'FETCH_GRAPH_WAVE_FORECAST_SUCCESS';
export const FETCH_GRAPH_WAVE_FORECAST_FAILURE = 'FETCH_GRAPH_WAVE_FORECAST_FAILURE';

export const fetchGraphWaveForecast = (type) =>
  baseFetchGraphForecast(
    FETCH_GRAPH_WAVE_FORECAST,
    FETCH_GRAPH_WAVE_FORECAST_SUCCESS,
    FETCH_GRAPH_WAVE_FORECAST_FAILURE,
    type === 'REGIONAL' ? fetchRegionalWaveForecast : fetchWaveForecast,
  );

export const FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS = 'FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS';
export const FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS_SUCCESS =
  'FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS_SUCCESS';
export const FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS_FAILURE =
  'FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS_FAILURE';

export const fetchGraphWaveForecastMaxHeights = (type) =>
  baseFetchGraphForecast(
    FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS,
    FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS_SUCCESS,
    FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS_FAILURE,
    type === 'REGIONAL' ? fetchRegionalWaveForecast : fetchWaveForecast,
  );

export const FETCH_GRAPH_TIDE_FORECAST = 'FETCH_GRAPH_TIDE_FORECAST';
export const FETCH_GRAPH_TIDE_FORECAST_SUCCESS = 'FETCH_GRAPH_TIDE_FORECAST_SUCCESS';
export const FETCH_GRAPH_TIDE_FORECAST_FAILURE = 'FETCH_GRAPH_TIDE_FORECAST_FAILURE';

export const fetchGraphTideForecast = baseFetchGraphForecast(
  FETCH_GRAPH_TIDE_FORECAST,
  FETCH_GRAPH_TIDE_FORECAST_SUCCESS,
  FETCH_GRAPH_TIDE_FORECAST_FAILURE,
  fetchTideForecast,
);

export const FETCH_GRAPH_WEATHER_FORECAST = 'FETCH_GRAPH_WEATHER_FORECAST';
export const FETCH_GRAPH_WEATHER_FORECAST_SUCCESS = 'FETCH_GRAPH_WEATHER_FORECAST_SUCCESS';
export const FETCH_GRAPH_WEATHER_FORECAST_FAILURE = 'FETCH_GRAPH_WEATHER_FORECAST_FAILURE';

export const fetchGraphWeatherForecast = baseFetchGraphForecast(
  FETCH_GRAPH_WEATHER_FORECAST,
  FETCH_GRAPH_WEATHER_FORECAST_SUCCESS,
  FETCH_GRAPH_WEATHER_FORECAST_FAILURE,
  fetchWeatherForecast,
);

export const FETCH_GRAPH_WIND_FORECAST = 'FETCH_GRAPH_WIND_FORECAST';
export const FETCH_GRAPH_WIND_FORECAST_SUCCESS = 'FETCH_GRAPH_WIND_FORECAST_SUCCESS';
export const FETCH_GRAPH_WIND_FORECAST_FAILURE = 'FETCH_GRAPH_WIND_FORECAST_FAILURE';

export const fetchGraphWindForecast = baseFetchGraphForecast(
  FETCH_GRAPH_WIND_FORECAST,
  FETCH_GRAPH_WIND_FORECAST_SUCCESS,
  FETCH_GRAPH_WIND_FORECAST_FAILURE,
  fetchWindForecast,
);

export const FETCH_GRAPH_AUTOMATED_RATING_FORECAST = 'FETCH_GRAPH_AUTOMATED_RATING_FORECAST';
export const FETCH_GRAPH_AUTOMATED_RATING_FORECAST_SUCCESS =
  'FETCH_GRAPH_AUTOMATED_RATING_FORECAST_SUCCESS';
export const FETCH_GRAPH_AUTOMATED_RATING_FORECAST_FAILURE =
  'FETCH_GRAPH_AUTOMATED_RATING_FORECAST_FAILURE';

export const fetchGraphAutomatedRatingForecast = baseFetchGraphForecast(
  FETCH_GRAPH_AUTOMATED_RATING_FORECAST,
  FETCH_GRAPH_AUTOMATED_RATING_FORECAST_SUCCESS,
  FETCH_GRAPH_AUTOMATED_RATING_FORECAST_FAILURE,
  fetchAutomatedRatingForecast,
);
