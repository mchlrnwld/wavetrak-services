import { isNumber } from 'lodash';
import {
  getMeterAnonymousId,
  getMeterRemaining,
  getUserDetails,
  getUserPremiumStatus,
  performAnonymousMeteringSurfCheck,
  setMeterRemaining,
} from '@surfline/web-common';
import { ActionCreatorWithPayload } from '@reduxjs/toolkit';
import { getActiveMeter } from '../selectors/meter';
import { AppDispatch, WavetrakStore } from '../stores';
import { performMeteringSurfCheck } from '../common/api/metering';

export const PERFORMED_SURF_CHECK_ACTION = 'PERFORMED_SURF_CHECK_ACTION';

/**
 * @description Can be used when a specific action that is not a route change can
 * trigger a surf check extension/consumption
 */
export const performSurfCheckAction = (actionName: string) => ({
  type: PERFORMED_SURF_CHECK_ACTION,
  action: actionName,
});

const performSurfCheck = async (dispatch: AppDispatch) => {
  const { meterRemaining } = await performMeteringSurfCheck();
  // TODO: fix the type for this in web-common
  dispatch((setMeterRemaining as any as ActionCreatorWithPayload<Number>)(meterRemaining));
};

export const performClientSidePageChangeSurfCheck = async (
  store: WavetrakStore,
  isServer = false,
) => {
  const state = store.getState();
  const { backplane } = state;

  // Without backplane metering state we can't do anything related to metering
  if (isServer || !backplane) {
    return null;
  }

  const meter = getActiveMeter(state);

  const isMeteredUser = !!meter;
  const anonymousId = getMeterAnonymousId(state);
  const isAnonymous = !getUserDetails(state);
  const isPremium = getUserPremiumStatus(state);
  const meterRemaining = getMeterRemaining(state);
  const hasRemainingSurfChecks = meterRemaining !== -1;

  // Page views are specific for Anonymous Sessions
  const hasRemainingPageViews = isNumber(meterRemaining) && meterRemaining >= 0;

  if (
    isPremium ||
    !isMeteredUser ||
    !hasRemainingSurfChecks ||
    (isAnonymous && !hasRemainingPageViews)
  ) {
    return null;
  }

  if (isAnonymous && hasRemainingPageViews) {
    await store.dispatch(performAnonymousMeteringSurfCheck({ anonymousId }));
    return null;
  }

  await performSurfCheck(store.dispatch);
  return null;
};
