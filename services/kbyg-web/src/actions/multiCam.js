export const SET_PRIMARY_CAM = 'SET_PRIMARY_CAM';
export const INITIALIZE_MULTI_CAM = 'INITIALIZE_MULTI_CAM';
export const SET_CAM_VIEW = 'SET_CAM_VIEW';

/**
 * @description Initializes the multi cam object in state for the spot when we load the spot report
 * @param {Array<Object>} initialCameras - The initial cameras array from fetching the spot
 * @param {string?} camIdQueryParam - The camId in the query param
 * report
 */
export const initializeMultiCam = (initialCameras) => (dispatch) => {
  const primaryCam = initialCameras.length ? initialCameras[0] : null;
  const numCams = initialCameras.length;
  const isMultiCam = numCams > 1;
  return dispatch({ type: INITIALIZE_MULTI_CAM, initialCameras, primaryCam, isMultiCam, numCams });
};

/**
 * @description Changes the primary cam that the user is viewing on the cam page
 * @param {Object} primaryCam - The desired primary cam object
 * @param {String} primaryCam._id - ID of the primary cam
 */
export const setPrimaryCam = (primaryCam) => ({ type: SET_PRIMARY_CAM, primaryCam });

/**
 * @description Sets the active cam view for the multi cam spot pages
 * @param {'SINGLE'|'MULTI'} activeView - The type of view, either single or multi
 */
export const setCamView = (activeView) => ({ type: SET_CAM_VIEW, activeView });
