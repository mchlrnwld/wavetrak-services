import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as spotApi from '../common/api/spot';
import * as feedAPI from '../common/api/feed';
import {
  FETCH_SPOT_REPORT,
  FETCH_SPOT_REPORT_SUCCESS,
  fetchSpotReport,
  FETCH_NEARBY_SPOTS,
  FETCH_NEARBY_SPOTS_SUCCESS,
  fetchNearbySpots,
  FETCH_BUOYS,
  FETCH_BUOYS_SUCCESS,
  fetchBuoys,
  FETCH_FEATURED_CONTENT,
  FETCH_FEATURED_CONTENT_SUCCESS,
  FETCH_FEATURED_CONTENT_FAILURE,
  fetchFeaturedContent,
} from './spot';

import { NOT_FOUND } from './status';

import reportFixture from './fixtures/spot/report';
import nearbyFixture from './fixtures/spot/nearby';
import buoyFixture from './fixtures/spot/buoys';
import promobox from './fixtures/spot/featuredContent';
import { INITIALIZE_MULTI_CAM } from './multiCam';

describe('actions / spot', () => {
  let mockStore;

  beforeAll(() => {
    mockStore = configureStore([thunk]);
  });

  beforeEach(() => {
    sinon.stub(spotApi, 'fetchReport');
    sinon.stub(spotApi, 'fetchNearbySpots');
    sinon.stub(spotApi, 'fetchBuoys');
  });

  afterEach(() => {
    spotApi.fetchReport.restore();
    spotApi.fetchNearbySpots.restore();
    spotApi.fetchBuoys.restore();
  });

  describe('fetchSpotReport', () => {
    it('dispatches FETCH_SPOT_REPORT_SUCCESS with spotId', async () => {
      const store = mockStore({});
      const spotReport = deepFreeze(reportFixture);
      spotApi.fetchReport.resolves(spotReport);

      const spotId = '5842041f4e65fad6a77088af';
      const spotWithoutTravelDetails = { ...spotReport.spot };
      delete spotWithoutTravelDetails.travelDetails;
      const expectedActions = [
        {
          type: FETCH_SPOT_REPORT,
        },
        {
          type: INITIALIZE_MULTI_CAM,
          initialCameras: spotReport.spot.cameras,
          numCams: 1,
          isMultiCam: false,
          primaryCam: spotReport.spot.cameras[0],
        },
        {
          type: FETCH_SPOT_REPORT_SUCCESS,
          data: {
            spot: spotWithoutTravelDetails,
            report: spotReport.report,
            forecast: spotReport.forecast,
          },
          associated: spotReport.associated,
          legacySpotId: spotReport.associated.advertising.spotId,
          legacySubregionId: spotReport.associated.advertising.subregionId,
        },
      ];
      await store.dispatch(
        fetchSpotReport({
          spotId,
          zoom: 11,
          sds: undefined,
          corrected: undefined,
        }),
      );
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(spotApi.fetchReport).to.have.been.calledOnce();
    });

    it('returns a 404 when no spotId is found', async () => {
      const store = mockStore({});
      const error = new Error();
      error.statusCode = 400;
      spotApi.fetchReport.rejects(error);

      const spotId = '5842041f4e65fad6a77088af';
      const expectedActions = [
        {
          type: FETCH_SPOT_REPORT,
        },
        {
          type: NOT_FOUND,
          message: 'The Spot Report Could not be found',
        },
      ];
      await store.dispatch(fetchSpotReport({ spotId, zoom: 11 }));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(spotApi.fetchReport).to.have.been.calledOnce();
    });
  });

  describe('fetchNearbySpots', () => {
    it('dispatches FETCH_NEARBY_SPOTS_SUCCESS with spotId', async () => {
      const store = mockStore({});
      const nearbySpots = deepFreeze(nearbyFixture);
      spotApi.fetchNearbySpots.resolves(nearbySpots);

      const spotId = '5842041f4e65fad6a77088af';
      const expectedActions = [
        {
          type: FETCH_NEARBY_SPOTS,
        },
        {
          type: FETCH_NEARBY_SPOTS_SUCCESS,
          nearbySpots,
        },
      ];
      await store.dispatch(fetchNearbySpots(spotId));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(spotApi.fetchNearbySpots).to.have.been.calledOnce();
    });
  });

  describe('fetchBuoys', () => {
    it('dispatches FETCH_BUOYS_SUCCESS with legacySpotId/legacySubregionId and properties', async () => {
      const store = mockStore({});

      const nearbyBuoys = deepFreeze(buoyFixture);
      spotApi.fetchBuoys.resolves(nearbyBuoys);

      const buoys = [
        {
          url: 'http://www.surfline.com/buoy-report/san-pedro_2415_46222/',
          img: 'https://tile.surfline.com/dashboard/buoyplot.cfm?buoy_id=46222&bp_timezone=-7&units=e',
        },
        {
          url: 'http://www.surfline.com/buoy-report/santa-monica_2415_46221/',
          img: 'https://tile.surfline.com/dashboard/buoyplot.cfm?buoy_id=46221&bp_timezone=-7&units=e',
        },
        {
          url: 'http://www.surfline.com/buoy-report/camp-pendleton_2415_46242/',
          img: 'https://tile.surfline.com/dashboard/buoyplot.cfm?buoy_id=46242&bp_timezone=-7&units=e',
        },
      ];
      const legacySpotId = 4222;
      const legacySubregionId = 2415;
      const properties = { units: 'e', timezone: -7 };

      const expectedActions = [
        {
          type: FETCH_BUOYS,
        },
        {
          type: FETCH_BUOYS_SUCCESS,
          buoys,
        },
      ];
      await store.dispatch(fetchBuoys(legacySpotId, legacySubregionId, properties));
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(spotApi.fetchBuoys).to.have.been.calledOnce();
    });
  });

  describe('fetchFeaturedContent', () => {
    beforeEach(() => {
      sinon.stub(feedAPI, 'fetchPromoBoxSlides');
    });

    afterEach(() => {
      feedAPI.fetchPromoBoxSlides.restore();
    });

    it('dispatches FETCH_FEATURED_CONTENT with fetchFeaturedContent', async () => {
      const store = mockStore({
        spot: {
          featuredContent: {
            articles: [],
          },
        },
      });
      feedAPI.fetchPromoBoxSlides.resolves(promobox);
      const expectedActions = [
        {
          type: FETCH_FEATURED_CONTENT,
        },
        {
          type: FETCH_FEATURED_CONTENT_SUCCESS,
          featuredContent: promobox.data.promobox,
        },
      ];
      await store.dispatch(fetchFeaturedContent());
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(feedAPI.fetchPromoBoxSlides).to.have.been.calledOnce();
    });

    it('dispatches FETCH_FEATURED_CONTENT_FAILURE on error', async () => {
      const store = mockStore({
        spot: {
          featuredContent: {
            articles: [],
          },
        },
      });
      feedAPI.fetchPromoBoxSlides.rejects({
        message: 'Some error occurred',
        status: 500,
      });

      const expectedActions = [
        {
          type: FETCH_FEATURED_CONTENT,
        },
        {
          type: FETCH_FEATURED_CONTENT_FAILURE,
          error: 'Some error occurred',
        },
      ];
      await store.dispatch(fetchFeaturedContent());
      expect(store.getActions()).to.deep.equal(expectedActions);
      expect(feedAPI.fetchPromoBoxSlides).to.have.been.calledOnce();
    });
  });
});
