import { createAction } from '@reduxjs/toolkit';

const SPLIT_READY = 'SPLIT_READY';

// eslint-disable-next-line import/prefer-default-export
export const splitReady = createAction(SPLIT_READY);
