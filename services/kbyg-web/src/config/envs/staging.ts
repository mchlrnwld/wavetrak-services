const staging = {
  cdnUrl: 'https://product-cdn.staging.surfline.com/',
  surflineHost: 'https://staging.surfline.com',
  servicesURL: 'https://services.staging.surfline.com',
  surflineEmbedURL: 'https://embed.staging.surfline.com',
  legacyApi: 'https://api.surfline.com/v1',
  robots: 'noindex,nofollow',
  htl: {
    scriptUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.js',
    cssUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.css',
    isTesting: 'yes',
  },
  appKeys: {
    segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
    fbAppId: process.env.FB_APP_ID || '564041023804928',
    logrocket: 'h57nhx/sl_sandbox',
  },
  clientId: '5bff1583ffda1e1eb8d35c4b',
  react: {
    src: 'https://unpkg.com/react@17.0.2/umd/react.production.min.js',
    integrity:
      'sha512-qlzIeUtTg7eBpmEaS12NZgxz52YYZVF5myj89mjJEesBd/oE9UPsYOX2QAXzvOAZYEvQohKdcY8zKE02ifXDmA==',
  },
  reactDOM: {
    src: 'https://unpkg.com/react-dom@17.0.2/umd/react-dom.production.min.js',
    integrity:
      'sha512-9jGNr5Piwe8nzLLYTk8QrEMPfjGU0px80GYzKZUxi7lmCfrBjtyCc1V5kkS5vxVwwIB7Qpzc7UxLiQxfAN30dw==',
  },
};

export default staging;
