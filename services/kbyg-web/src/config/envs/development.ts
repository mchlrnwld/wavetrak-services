const development = {
  cdnUrl: '/',
  surflineHost: 'https://sandbox.surfline.com',
  servicesURL: 'https://services.sandbox.surfline.com',
  surflineEmbedURL: 'https://embed.sandbox.surfline.com',
  legacyApi: 'https://api.surfline.com/v1',
  robots: 'noindex,nofollow',
  htl: {
    scriptUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.js',
    cssUrl: 'https://htlbid.com/stage/v3/surfline.com/htlbid.css',
    isTesting: 'yes',
  },
  appKeys: {
    segment: 'VQDMbHw65jkXg4go8KmBnDiXzeAz7GiO',
    fbAppId: process.env.FB_APP_ID || '564041023804928',
    logrocket: 'h57nhx/sl_sandbox',
  },
  clientId: '5bff1583ffda1e1eb8d35c4b',
  react: {
    src: 'https://unpkg.com/react@17.0.2/umd/react.development.js',
    integrity:
      'sha512-Vf2xGDzpqUOEIKO+X2rgTLWPY+65++WPwCHkX2nFMu9IcstumPsf/uKKRd5prX3wOu8Q0GBylRpsDB26R6ExOg==',
  },
  reactDOM: {
    src: 'https://unpkg.com/react-dom@17.0.2/umd/react-dom.development.js',
    integrity:
      'sha512-Wr9OKCTtq1anK0hq5bY3X/AvDI5EflDSAh0mE9gma+4hl+kXdTJPKZ3TwLMBcrgUeoY0s3dq9JjhCQc7vddtFg==',
  },
};

export default development;
