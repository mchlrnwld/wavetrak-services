const production = {
  cdnUrl: 'https://wa.cdn-surfline.com',
  surflineHost: 'https://www.surfline.com',
  servicesURL: 'https://services.surfline.com',
  surflineEmbedURL: 'https://embed.cdn-surfline.com',
  legacyApi: 'https://api.surfline.com/v1',
  htl: {
    scriptUrl: 'https://htlbid.com/v3/surfline.com/htlbid.js',
    cssUrl: 'https://htlbid.com/v3/surfline.com/htlbid.css',
    isTesting: 'no',
  },
  appKeys: {
    segment: 'Se6IGuzcvUhLx55hLHL0RiXkS6oUTz8L',
    fbAppId: process.env.FB_APP_ID || '218714858155230',
    logrocket: 'h57nhx/sl_prod',
  },
  clientId: '5c59e7c3f0b6cb1ad02baf66',
  react: {
    src: 'https://unpkg.com/react@17.0.2/umd/react.production.min.js',
    integrity:
      'sha512-qlzIeUtTg7eBpmEaS12NZgxz52YYZVF5myj89mjJEesBd/oE9UPsYOX2QAXzvOAZYEvQohKdcY8zKE02ifXDmA==',
  },
  reactDOM: {
    src: 'https://unpkg.com/react-dom@17.0.2/umd/react-dom.production.min.js',
    integrity:
      'sha512-9jGNr5Piwe8nzLLYTk8QrEMPfjGU0px80GYzKZUxi7lmCfrBjtyCc1V5kkS5vxVwwIB7Qpzc7UxLiQxfAN30dw==',
  },
};

export default production;
