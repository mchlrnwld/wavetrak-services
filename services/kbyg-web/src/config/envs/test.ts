const test = {
  cdnUrl: '/',
  surflineHost: 'http://localhost',
  servicesURL: 'http://localhost',
  surflineEmbedURL: 'http://localhost',
  legacyApi: 'http://localhost',
  robots: 'noindex,nofollow',
  clientId: 'test',
  htl: {
    isTesting: true,
  },
};

export default test;
