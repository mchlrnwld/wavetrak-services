/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
import getConfig from 'next/config';
import baseConfig from './base';

const env = process.env.APP_ENV || getConfig()?.publicRuntimeConfig?.APP_ENV;

interface EnvConfig {
  cdnUrl: string;
  surflineHost: string;
  servicesURL: string;
  surflineEmbedURL: string;
  legacyApi: string;
  robots: string;
  htl: {
    scriptUrl: string;
    cssUrl: string;
    isTesting: string;
  };
  appKeys: {
    segment: string;
    fbAppId: string;
    logrocket: string;
  };
  clientId: string;
  react: {
    src: string;
    integrity: string;
  };
  reactDOM: {
    src: string;
    integrity: string;
  };
}

const envConfig = require(`./envs/${env}`) as { default: EnvConfig };

const config = {
  ...baseConfig,
  ...(envConfig.default || {}),
};

export default config;
