import { accountPaths } from '@surfline/web-common';

const base = {
  facebookOGImage: '/facebook-og-default.png',
  mobileWidth: 512,
  tabletWidth: 769,
  wpAPI: 'https://www.surfline.com',
  tabletLargeWidth: 976,
  smallDesktopWidth: 992,
  rangeSelectorWidths: {
    oneDay: 976,
    fourDay: 1256,
    fiveDay: 1712,
  },
  hostForSEO: 'https://www.surfline.com',
  funnelUrl: accountPaths.funnelPath,
  accountSettingsUrl: '/account/edit-settings',
  createAccountUrl: '/create-account',
  buoyPlotApi: 'https://tile.surfline.com/dashboard/buoyplot.cfm',
  mapTileUrl: 'https://api.maptiler.com/maps/062c0d04-1842-4a45-8181-c5bec3bf2214',
  mapTileSatUrl:
    'https://api.maptiler.com/maps/hybrid/256/{z}/{x}/{y}@2x.jpg?key=qEC8tW26ziqOt0GVgs34',
  mapTileKey: '3tFgnOQBQixe61aigsBT',
  legacyAPI: 'https://api.surfline.com/v1',
  servicesURL: 'https://services.sandbox.surfline.com',
};

export default base;
