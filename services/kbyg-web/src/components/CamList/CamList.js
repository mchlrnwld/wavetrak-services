import PropTypes from 'prop-types';
import React, { Component } from 'react';
import CamListItem from '../CamListItem';
import CamListCTAItem from '../CamListCTAItem';
import ScrollControl from '../ScrollControl';
import { showPremiumCamCTA } from '../../utils/premiumCam';

class CamList extends Component {
  componentDidMount() {
    const { spotId } = this.props;
    this.centerListItem(spotId);
  }

  componentDidUpdate(prevProps) {
    const { spotId, scrollLeft } = this.props;
    if (prevProps.spotId !== spotId) {
      this.centerListItem(spotId);
    }
    this.el.scrollLeft = scrollLeft;
  }

  spotRefs = {};

  getOffSetCenter = (el, itemWidth = 176) => el.clientWidth / 2 - itemWidth / 2;

  centerListItem(spotId) {
    const { doSetCamListScrollPosition } = this.props;
    const offSetAtSpot = this.spotRefs[spotId] ? this.spotRefs[spotId].offsetLeft : null;
    const offSetCenter = this.getOffSetCenter(this.el);

    // 34 represends the width of the left scroll control div;
    const left = offSetAtSpot - offSetCenter - 34;
    doSetCamListScrollPosition({ top: 0, left });
  }

  scrollLeft() {
    const { doSetCamListScrollPosition } = this.props;
    const left = this.el.scrollLeft - this.el.clientWidth;
    doSetCamListScrollPosition({ top: 0, left });
  }

  scrollRight() {
    const { doSetCamListScrollPosition } = this.props;
    const left = this.el.scrollLeft + this.el.clientWidth;
    doSetCamListScrollPosition({ top: 0, left });
  }

  render() {
    const { spots, units, isPremium } = this.props;
    return (
      <div className="sl-cam-list">
        <ScrollControl size="tall" direction="left" clickHandler={() => this.scrollLeft()} />
        <div
          ref={(el) => {
            this.el = el;
          }}
          className="sl-cam-list__spots"
        >
          {spots.map((spot) => (
            <div
              className="sl-cam-list__spots__ref"
              key={spot._id}
              ref={(spotEl) => {
                this.spotRefs[spot._id] = spotEl;
              }}
            >
              {showPremiumCamCTA({ spot, isPremium }) ? (
                <CamListCTAItem spot={spot} units={units} />
              ) : (
                <CamListItem spot={spot} units={units} />
              )}
            </div>
          ))}
        </div>
        <ScrollControl size="tall" direction="right" clickHandler={() => this.scrollRight()} />
      </div>
    );
  }
}

CamList.propTypes = {
  spots: PropTypes.arrayOf(
    PropTypes.shape({
      conditions: PropTypes.shape({ value: PropTypes.string }),
      waveHeight: PropTypes.shape({
        min: PropTypes.number,
        max: PropTypes.number,
      }),
    }),
  ).isRequired,
  scrollLeft: PropTypes.number,
  spotId: PropTypes.string.isRequired,
  units: PropTypes.string.isRequired,
  doSetCamListScrollPosition: PropTypes.func.isRequired,
  isPremium: PropTypes.bool,
};

CamList.defaultProps = {
  scrollLeft: 0,
  isPremium: false,
};

export default CamList;
