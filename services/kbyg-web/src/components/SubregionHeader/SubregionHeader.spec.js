import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import deepFreeze from 'deep-freeze';
import SubregionHeader from './SubregionHeader';

describe('components / SubregionHeader', () => {
  const props = deepFreeze({
    subregion: {
      id: '58581a836630e24c44878fd6',
      name: 'North Orange County',
      forecastStatus: {
        status: 'active',
        inactiveMessage: '',
      },
    },
  });

  it('renders avatar, name, and see forecast link if map is false', () => {
    const wrapper = shallow(<SubregionHeader {...props} />);
    const region = wrapper.find('.subregionHeaderRegion');
    const seeForecast = wrapper.find('.subregionHeaderSeeForecast');
    expect(region).to.have.length(1);
    expect(region.text()).to.equal(`${props.subregion.name} Regional Forecast`);
    expect(seeForecast).to.have.length(1);

    const mapWrapper = shallow(<SubregionHeader {...props} map />);
    const mapRegion = mapWrapper.find('.subregionHeaderRegion');
    const mapSeeForecast = mapWrapper.find('.subregionHeaderSeeForecast');
    expect(mapRegion).to.have.length(1);
    expect(mapRegion.text()).to.equal('Regional Forecast');
    expect(mapSeeForecast).to.have.length(0);
  });
});
