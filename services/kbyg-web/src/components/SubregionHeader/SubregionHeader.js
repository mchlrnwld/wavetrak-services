import PropTypes from 'prop-types';
import React from 'react';
import SubregionHeaderLink from '../SubregionHeaderLink';
import styles from './SubregionHeader.module.scss';

const regionText = (map, name) => `${map ? '' : `${name} `}Regional Forecast`;

const SubregionHeader = ({ subregion, map }) => (
  <div>
    <SubregionHeaderLink
      subregion={subregion}
      map={map}
      classes={{ root: styles.subregionHeaderLink }}
    >
      <div className={styles.subregionHeaderRegion}>{regionText(map, subregion.name)}</div>
      {map ? null : <span className={styles.subregionHeaderSeeForecast}>See Forecast</span>}
    </SubregionHeaderLink>
  </div>
);

SubregionHeader.propTypes = {
  subregion: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    forecastStatus: PropTypes.shape({
      status: PropTypes.string,
      inactiveMessage: PropTypes.string,
    }),
  }).isRequired,
  map: PropTypes.bool,
};

SubregionHeader.defaultProps = {
  map: false,
};

export default SubregionHeader;
