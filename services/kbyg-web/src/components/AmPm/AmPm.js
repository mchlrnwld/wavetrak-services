import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import conditionClassModifier from '../../utils/conditionClassModifier';

const amPmClass = (rating) => {
  const classObject = {};
  classObject['sl-am-pm'] = true;
  classObject[`sl-am-pm--${conditionClassModifier(rating)}`] = !!rating;
  return classNames(classObject);
};

const AmPm = ({ ampm, rating }) => (
  <div className={amPmClass(rating)}>
    <div className="sl-am-pm__text">{ampm}</div>
  </div>
);

AmPm.propTypes = {
  ampm: PropTypes.string.isRequired,
  rating: PropTypes.string.isRequired,
};

export default AmPm;
