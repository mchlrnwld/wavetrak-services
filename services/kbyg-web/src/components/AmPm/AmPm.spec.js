import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import AmPm from './AmPm';

describe('components / AmPm', () => {
  it('displays am or pm text', () => {
    const wrapper = shallow(<AmPm ampm="am" rating="good" />);
    const textWrapper = wrapper.find('.sl-am-pm__text');
    expect(textWrapper.text()).to.equal('am');
  });

  it('should update class from rating', () => {
    const rating = 'GOOD';
    const wrapper = shallow(<AmPm rating={rating} ampm="am" />);
    const ampmWrapper = wrapper.find('.sl-am-pm--good');
    expect(ampmWrapper).to.have.length(1);
  });
});
