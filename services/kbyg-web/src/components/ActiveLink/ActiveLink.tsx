/* eslint-disable jsx-a11y/anchor-is-valid */
import { useRouter } from 'next/router';
import Link, { LinkProps } from 'next/link';
import React from 'react';

interface Props extends LinkProps {
  classes: { root: string; active: string };
}

const ActiveLink: React.FC<Props> = ({ children, classes, ...props }) => {
  const { asPath } = useRouter();

  // pages/index.js will be matched via props.href
  // pages/about.js will be matched via props.href
  // pages/[slug].js will be matched via props.as
  const className =
    asPath === props.href || asPath === props.as
      ? `${classes.root} ${classes.active}`.trim()
      : classes.root;

  return (
    <Link {...props}>
      <a className={className}>{children}</a>
    </Link>
  );
};

export default ActiveLink;
