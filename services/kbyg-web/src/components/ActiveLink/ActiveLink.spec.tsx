import { expect } from 'chai';

import { mountWithReduxAndRouter } from '../../utils/test-utils';
import ActiveLink from './ActiveLink';

describe('ActiveLink', () => {
  it('should render an active link when the URL matches', () => {
    const { wrapper } = mountWithReduxAndRouter(
      ActiveLink,
      {
        classes: { active: 'active', root: 'root' },
        href: '/',
      },
      { router: { pathname: '/', asPath: '/' } },
    );

    expect(wrapper.find('a').prop('className')).to.contain('root');
    expect(wrapper.find('a').prop('className')).to.contain('active');
  });

  it('should render an inactive link when the URL does not match', () => {
    const { wrapper } = mountWithReduxAndRouter(
      ActiveLink,
      {
        classes: { active: 'active', root: 'root' },
        href: '/',
      },
      { router: { pathname: '/[test]', asPath: '/test' } },
    );

    expect(wrapper.find('a').prop('className')).to.contain('root');
    expect(wrapper.find('a').prop('className')).to.not.contain('active');
  });
});
