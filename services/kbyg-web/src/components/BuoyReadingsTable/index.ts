import { withDevice } from '@surfline/quiver-react';
import BuoyReadingsTable from './BuoyReadingsTable';

export default withDevice(BuoyReadingsTable);
