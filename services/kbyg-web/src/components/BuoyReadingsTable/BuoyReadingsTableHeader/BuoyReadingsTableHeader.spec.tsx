import React from 'react';

import { expect } from 'chai';
import { mount } from 'enzyme';
import Skeleton from 'react-loading-skeleton';
import BuoyReadingsTableHeader from './BuoyReadingsTableHeader';

describe('components / BuoyReadingsTableHeader', () => {
  const props = {
    title: 'Buoy Data',
    divider: true,
    loading: false,
    error: false,
  };
  const propsWithLoading = {
    loading: true,
    error: false,
  };

  it('renders header', () => {
    const wrapper = mount(<BuoyReadingsTableHeader {...props} />);

    const header = wrapper.find('.readingsTableHeaderTitle');

    expect(header).to.have.length(1);
    expect(header.text()).to.contain('Buoy Data');
  });

  it('renders divider', () => {
    const wrapper = mount(<BuoyReadingsTableHeader {...props} />);

    const divider = wrapper.find('.readingsTableHeaderDivider');

    expect(divider).to.have.length(1);
  });

  it('renders loading skeleton', () => {
    const wrapper = mount(<BuoyReadingsTableHeader {...propsWithLoading} />);

    const LoadingSkeletonInstance = wrapper.find(Skeleton);

    expect(LoadingSkeletonInstance).to.have.length(1);
  });
});
