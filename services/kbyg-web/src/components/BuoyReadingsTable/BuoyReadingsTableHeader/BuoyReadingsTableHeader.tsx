import React from 'react';
import Skeleton from 'react-loading-skeleton';

import styles from './BuoyReadingsTableHeader.module.scss';

interface Props {
  title?: string;
  divider?: boolean;
  loading?: boolean;
}

const BuoyReadingsTableHeader: React.FC<Props> = ({
  title = 'Latest Buoy Data',
  divider,
  loading,
}) => (
  <div className={styles.readingsTableHeader}>
    <h1 className={styles.readingsTableHeaderTitle}>
      {loading ? <Skeleton width={250} /> : title}
    </h1>
    {divider && <hr className={styles.readingsTableHeaderDivider} />}
  </div>
);

export default BuoyReadingsTableHeader;
