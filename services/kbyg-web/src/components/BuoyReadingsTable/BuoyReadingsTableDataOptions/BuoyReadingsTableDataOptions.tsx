import React from 'react';
import classNames from 'classnames';
import Skeleton from 'react-loading-skeleton';
import { Button } from '@surfline/quiver-react';

import { ALL_SWELLS, OTHER_DATA } from '../constants';

import styles from './BuoyReadingsTableDataOptions.module.scss';

const getClass = () =>
  classNames({
    [styles.readingsTableDataOptions]: true,
  });

const getOptionClass = (selected: boolean) =>
  classNames({
    [styles.readingsTableDataOptionsButton]: true,
    [styles.readingsTableDataOptionsButtonSelected]: selected,
  });

interface Props {
  selected: string;
  setSelected: (selected: string) => void;
  loading?: boolean;
}

const BuoyReadingsTableDataOptions: React.FC<Props> = ({ selected, setSelected, loading }) => (
  <div className={getClass()}>
    <div className={getOptionClass(selected === ALL_SWELLS)}>
      {loading ? (
        <Skeleton className={styles.loadingSkeleton} />
      ) : (
        <Button onClick={() => setSelected(ALL_SWELLS)}>All Swells</Button>
      )}
    </div>
    <div className={getOptionClass(selected === OTHER_DATA)}>
      {loading ? (
        <Skeleton className={styles.loadingSkeleton} />
      ) : (
        <Button onClick={() => setSelected(OTHER_DATA)}>Other Data</Button>
      )}
    </div>
  </div>
);

export default BuoyReadingsTableDataOptions;
