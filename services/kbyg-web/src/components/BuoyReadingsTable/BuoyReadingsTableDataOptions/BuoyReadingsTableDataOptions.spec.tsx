import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import Skeleton from 'react-loading-skeleton';
import sinon from 'sinon';
import BuoyReadingsTableDataOptions from './BuoyReadingsTableDataOptions';

describe('components / BuoyReadingsTableDataOptions', () => {
  const setSelected = sinon.spy();
  const props = {
    loading: false,
    error: false,
    selected: 'all-swells',
    setSelected,
  };
  const propsWithLoading = {
    loading: true,
    error: false,
    selected: 'all-swells',
    setSelected,
  };

  it('renders buttons with labels', () => {
    const wrapper = mount(<BuoyReadingsTableDataOptions {...props} />);

    const button = wrapper.find('.readingsTableDataOptionsButton');
    const otherDataButton = wrapper
      .find('.readingsTableDataOptionsButton')
      .not('.readingsTableDataOptionsButtonSelected')
      .first()
      .find('button');

    expect(button).to.have.length(2);
    expect(otherDataButton).to.have.length(1);

    expect(button.first().text()).to.equal('All Swells');
    expect(otherDataButton.text()).to.equal('Other Data');
  });

  it('renders selected button', () => {
    const wrapper = mount(<BuoyReadingsTableDataOptions {...props} />);
    const button = wrapper.find('.readingsTableDataOptionsButtonSelected');

    expect(button).to.have.length(1);
    expect(button.text()).to.equal('All Swells');
  });
  it('renders loading skeleton', () => {
    const wrapper = mount(<BuoyReadingsTableDataOptions {...propsWithLoading} />);

    const LoadingSkeletonInstance = wrapper.find(Skeleton);

    expect(LoadingSkeletonInstance).to.have.length(2);
  });

  it('onClick function sets hook variable onClick', () => {
    const wrapper = mount(<BuoyReadingsTableDataOptions {...props} />);

    const otherDataButton = wrapper
      .find('.readingsTableDataOptionsButton')
      .not('.readingsTableDataOptionsButtonSelected')
      .first()
      .find('button');
    expect(otherDataButton).to.have.length(1);
    expect(otherDataButton.text()).to.equal('Other Data');

    otherDataButton.simulate('click');
    expect(setSelected).to.have.been.calledOnce();
  });
});
