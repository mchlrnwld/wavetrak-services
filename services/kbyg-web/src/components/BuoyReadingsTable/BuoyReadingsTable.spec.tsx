import { expect } from 'chai';

import BuoyReadingsTable, { Props } from './BuoyReadingsTable';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import { getBuoyReportFixture } from '../../common/api/fixtures/buoys';
import ForecastTable from '../ForecastTable/ForecastTable';
import BuoyReadingsTableDataOptions from './BuoyReadingsTableDataOptions';
import BuoyReadingsTableHeader from './BuoyReadingsTableHeader';
import { AppState } from '../../stores';

describe('components / BuoyReadingsTable', () => {
  const props: Props = {
    buoyReportData: getBuoyReportFixture.data,
    device: { width: 0, isMobile: false },
    loading: false,
    error: false,
    changeUnitsHref: '/',
  };

  const errorProps: Props = {
    buoyReportData: undefined,
    loading: false,
    error: true,
    device: { width: 0, isMobile: false },
    changeUnitsHref: '/',
  };

  const defaultState = {
    backplane: {
      user: {
        settings: {
          units: {
            surfHeight: 'FT',
            swellHeight: 'FT',
            tideHeight: 'FT',
            windSpeed: 'MPH',
            temperature: 'F',
          },
        },
      },
    },
  };

  it('should render 1 BuoyReadingsTableHeader components', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyReadingsTable, props, {
      initialState: defaultState as AppState,
    });
    const BuoyReadingsTableHeaderInstance = wrapper.find(BuoyReadingsTableHeader);
    expect(BuoyReadingsTableHeaderInstance).to.have.length(1);
    expect(BuoyReadingsTableHeaderInstance.prop('divider')).to.be.true();
  });

  it('should render 1 BuoyReadingsTableDataOptions components', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyReadingsTable, props, {
      initialState: defaultState as AppState,
    });
    const BuoyReadingsTableDataOptionsInstance = wrapper.find(BuoyReadingsTableDataOptions);
    expect(BuoyReadingsTableDataOptionsInstance).to.have.length(1);
    expect(BuoyReadingsTableDataOptionsInstance.prop('selected')).to.be.equal('all-swells');
  });

  it('should render 1 buoy reading metric components', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyReadingsTable, props, {
      initialState: defaultState as AppState,
    });
    const ForecastTableInstance = wrapper.find(ForecastTable);
    expect(ForecastTableInstance).to.have.length(1);
    expect(ForecastTableInstance.prop('isCustomTable')).to.be.true();
    expect(ForecastTableInstance.prop('customTableData')).to.be.equal(props.buoyReportData);
  });

  it('should collapse the readings table and data options if there is an error', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyReadingsTable, errorProps, {
      initialState: defaultState as AppState,
    });

    const options = wrapper.find(BuoyReadingsTableDataOptions);
    const table = wrapper.find(ForecastTable);

    expect(options).to.have.length(0);
    expect(table).to.have.length(0);
  });
});
