import React, { useCallback, useState } from 'react';
import classNames from 'classnames';
import Skeleton from 'react-loading-skeleton';
import { getUserSettings } from '@surfline/web-common';
import ForecastTable from '../ForecastTable/ForecastTable';
import BuoyReadingsTableDataOptions from './BuoyReadingsTableDataOptions';
import BuoyReadingsTableHeader from './BuoyReadingsTableHeader';
import ChangeUnits from '../ChangeUnits/ChangeUnits';
import { ALL_SWELLS, OTHER_DATA } from './constants';

import styles from './BuoyReadingsTable.module.scss';
import { formatBuoyReportTableData } from '../../utils/formatBuoyReportTableData';
import { useAppSelector } from '../../stores/hooks';

const getClass = (loading?: boolean, error?: boolean) =>
  classNames({
    [styles.buoyReadingsTable]: true,
    'sl-buoy-readings-table--loading': loading,
    'sl-buoy-readings-table--error': error,
  });

const buoySwellColumns = [{ display: 'Wave Height', type: 'significant-height' }];

const buoyMetricsColumns = [
  { display: 'Wind', type: 'wind', metricDataKey: 'wind' },
  { display: 'Sea Temp.', type: 'water-temperature', metricDataKey: 'waterTemperature' },
  { display: 'Air Temp.', type: 'air-temperature', metricDataKey: 'airTemperature' },
  { display: 'Pressure', type: 'pressure', metricDataKey: 'pressure' },
  { display: 'Dew Point', type: 'dew-point', metricDataKey: 'dewPoint' },
];

type BuoyReportData = ReturnType<typeof formatBuoyReportTableData>;

export interface Props {
  buoyReportData?: BuoyReportData;
  changeUnitsHref: string;
  device: { width: number; isMobile: boolean };
  loading?: boolean;
  error?: boolean;
}

const BuoyReadingsTable: React.FC<Props> = ({
  buoyReportData,
  changeUnitsHref,
  device,
  loading,
  error,
}) => {
  const [table, setTable] = useState({
    days: [...Array(3)].map(() => ({
      expanded: false,
      blurred: false,
    })),
  });

  const [selectedData, setSelectedData] = useState(ALL_SWELLS);

  const settings = useAppSelector(getUserSettings);
  const dateFormat = settings?.date?.format;

  const toggleDayExpanded = useCallback(
    (day: number) => {
      const days = [...table.days];
      days[day] = {
        ...days[day],
        expanded: !days[day].expanded,
      };
      setTable({
        ...table,
        days,
      });
    },
    [table],
  );

  const expandAllDays = useCallback(
    (expanded: boolean) => {
      setTable({
        ...table,
        days: table.days.map((day) => ({
          ...day,
          expanded: day.blurred || expanded,
        })),
      });
    },
    [table],
  );

  return (
    <section className={getClass()}>
      <BuoyReadingsTableHeader divider loading={loading} />
      {!error && (
        <>
          <BuoyReadingsTableDataOptions
            selected={selectedData}
            setSelected={setSelectedData}
            loading={loading}
          />
          {loading && <Skeleton className={styles.buoyReadingsTableLoadingSkeleton} />}
          {buoyReportData && !loading && (
            <ForecastTable
              customTableData={buoyReportData}
              isCustomTable
              customColumns={selectedData === ALL_SWELLS ? buoySwellColumns : buoyMetricsColumns}
              overrideSwellColumns={selectedData === OTHER_DATA}
              days={table.days}
              hideSurfAndWind
              utcOffset={buoyReportData.utcOffset}
              toggleDayExpanded={toggleDayExpanded}
              expandAllDays={expandAllDays}
              deviceWidth={device?.width}
              dateFormat={dateFormat}
            />
          )}
          {!loading && <ChangeUnits href={changeUnitsHref} />}
        </>
      )}
    </section>
  );
};

export default BuoyReadingsTable;
