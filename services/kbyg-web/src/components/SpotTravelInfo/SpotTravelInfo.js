import React from 'react';
import PropTypes from 'prop-types';
import travelDetailsPropTypes from '../../propTypes/travelDetails';
import SpotTravelArticle from '../SpotTravelArticle';
import TravelGuide from '../TravelGuide';

/**
 * @param {object} Props
 * @param {string} Props.spotName
 * @param {import('../../propTypes/travelDetails').TravelDetails} Props.travelDetails
 */
const SpotTravelInfo = ({ spotName, travelDetails }) => (
  <div className="sl-spot-travel-info">
    <section className="sl-spot-travel-info__content">
      {travelDetails.relatedArticleId && (
        <SpotTravelArticle spotName={spotName} articleId={travelDetails.relatedArticleId} />
      )}
      <section className="sl-spot-travel-info__content__guide">
        <TravelGuide spotName={spotName} travelDetails={travelDetails} />
      </section>
    </section>
  </div>
);

SpotTravelInfo.propTypes = {
  spotName: PropTypes.string.isRequired,
  travelDetails: travelDetailsPropTypes.isRequired,
};

export default SpotTravelInfo;
