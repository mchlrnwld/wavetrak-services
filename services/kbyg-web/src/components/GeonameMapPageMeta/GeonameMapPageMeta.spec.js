import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import MetaTags from '../MetaTags';
import GeonameMapPageMeta from './GeonameMapPageMeta';

describe('components / GeonameMapPageMeta', () => {
  it('renders MetaTags with appropriate props', () => {
    const name = 'Huntington Beach';
    const title = `${name} Surf Report & Forecast - Map of ${name} Surf Spots & Cams - Surfline`;
    const description =
      'The most accurate and trusted surf reports, forecasts, and coastal weather. Surfers from around the world choose Surfline for dependable and up to date surfing forecasts and high quality surf content, live surf cams, and features.';

    const wrapper = shallow(<GeonameMapPageMeta name={name} />);
    const metaTags = wrapper.find(MetaTags);

    expect(metaTags).to.have.length(1);
    expect(metaTags.prop('title')).to.equal(title);
    expect(metaTags.prop('description')).to.equal(description);
  });
});
