import PropTypes from 'prop-types';
import { pageStructuredData } from '@surfline/web-common';
import React from 'react';
import MetaTags from '../MetaTags';

const GeonameMapPageMeta = ({ name, urlPath }) => (
  <MetaTags
    title={`${name} Surf Report & Forecast - Map of ${name} Surf Spots & Cams - Surfline`}
    description="The most accurate and trusted surf reports, forecasts, and coastal weather. Surfers from around the world choose Surfline for dependable and up to date surfing forecasts and high quality surf content, live surf cams, and features."
    urlPath={urlPath}
  >
    <script type="application/ld+json">
      {pageStructuredData(
        {
          type: 'FAQPage',
          author: 'Surfline',
          authorType: 'Organization',
          isAccessibleForFree: false,
          cssSelector: '.sl-map-page',
        },
        {
          mainEntity: [
            {
              '@type': 'Question',
              name: `What's the best ${name} spot to surf right now?`,
              acceptedAnswer: {
                '@type': 'Answer',
                text: `Our ${name} map page sorts to the top, all of the most likely best spots to surf today based on forecast conditions and wave height. For spots with human-observed surf reports, Surfline forecasters update reports twice daily - once around sunrise, followed by an afternoon update typically between 12pm-2pm local time. As is typical with weather and surf, conditions can change quickly throughout the day. For example, the wave heights and conditions posted at sunrise may look different later in the morning due to changes in winds or tides.`,
              },
            },
            {
              '@type': 'Question',
              name: 'What is LOLA?',
              acceptedAnswer: {
                '@type': 'Answer',
                text: "LOLA is Surfline's global swell model that is continuously updated and corrected with real-time information. LOLA is a combination of many things - she's a computer model that crunches data, she gathers real-time data from offshore buoys, and she taps into orbiting satellites to measure hurricane-force winds and tremendous wave heights around the world. Over the years we have made a number of enhancements to LOLA. This includes a proprietary extended forecast algorithm, high-res wind charts, nearshore model enhancements, and revised LOLA charts.",
              },
            },
            {
              '@type': 'Question',
              name: 'Why are the winds different on spots forecast vs regional forecast?',
              acceptedAnswer: {
                '@type': 'Answer',
                text: `When using Surfline to plan your next ${name} surf session, you may notice that there's a difference in the wind conditions that are displayed on a spot forecast page and those that are displayed on a regional forecast page. This is because the wind data that is shown on a spot forecast page is generated for the exact location of the spot that you're viewing; whereas the wind data on a regional forecast page is generated for a single point within a region. The regional forecast is meant to serve as an overview for the entire region, but there may be some variance in conditions between the regional forecast location and the specific spot forecast location.`,
              },
            },
            {
              '@type': 'Question',
              name: 'What is Surfline Sessions?',
              acceptedAnswer: {
                '@type': 'Answer',
                text: 'Surfline Sessions helps you document your surfing experiences by tracking your rides through GPS and delivering shareable data and videos to your Surfline iOS app. Sessions employs our extensive camera network, Cam Rewind tool and forecast data in conjunction with third-party wearable devices to provide users with video clips, detailed ride metrics, surf and weather information directly to their iPhone while they surf. With Surfline Sessions, users have the ability to watch any wave they ride in front of a Surfline camera the moment they get out of the water. These rides can then be shared with friends and family, studied to improve future sessions, and saved for posterity.',
              },
            },
          ],
        },
      )}
    </script>
  </MetaTags>
);

GeonameMapPageMeta.propTypes = {
  name: PropTypes.string.isRequired,
  urlPath: PropTypes.string.isRequired,
};

export default GeonameMapPageMeta;
