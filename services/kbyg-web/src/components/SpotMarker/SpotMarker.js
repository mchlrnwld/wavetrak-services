/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/destructuring-assignment */

import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { SpotMarker as QuiverSpotMarker } from '@surfline/quiver-react';
import { SL_WEB_CONDITION_VARIATION } from '../../common/treatments';
import { useSplitTreatment } from '../../selectors/split';

const getClassName = (current) =>
  classNames({
    'sl-spot-marker': true,
    'sl-spot-marker--current': current,
  });

const SpotMarker = (props) => {
  const { treatment, loading } = useSplitTreatment(SL_WEB_CONDITION_VARIATION);

  return (
    <div
      className={getClassName(props.current)}
      onClick={props.onClick}
      onMouseEnter={props.onMouseEnter}
      onMouseLeave={props.onMouseLeave}
    >
      {!loading && <QuiverSpotMarker {...props} sevenRatings={treatment === 'on'} />}
    </div>
  );
};

SpotMarker.propTypes = {
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  onClick: PropTypes.func,
  current: PropTypes.bool,
};

SpotMarker.defaultProps = {
  onMouseEnter: () => null,
  onMouseLeave: () => null,
  onClick: () => null,
  current: false,
};

export default SpotMarker;
