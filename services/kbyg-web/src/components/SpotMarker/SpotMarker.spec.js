import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import { SpotMarker as QuiverSpotMarker } from '@surfline/quiver-react';
import SpotMarker from './SpotMarker';
import * as splitSelectors from '../../selectors/split';

describe('components / SpotMarker', () => {
  beforeEach(() => {
    sinon.stub(splitSelectors, 'useSplitTreatment').returns({ loading: false, treatment: null });
  });

  afterEach(() => {
    splitSelectors.useSplitTreatment.restore();
  });

  it('handles onClick', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(<SpotMarker onClick={onClick} />);
    wrapper.simulate('click');
    expect(onClick).to.have.been.calledOnce();
  });

  it('handles onMouseEnter', () => {
    const onMouseEnter = sinon.spy();
    const wrapper = shallow(<SpotMarker onMouseEnter={onMouseEnter} />);
    wrapper.simulate('mouseenter');
    expect(onMouseEnter).to.have.been.calledOnce();
  });

  it('handles onMouseLeave', () => {
    const onMouseLeave = sinon.spy();
    const wrapper = shallow(<SpotMarker onMouseLeave={onMouseLeave} />);
    wrapper.simulate('mouseleave');
    expect(onMouseLeave).to.have.been.calledOnce();
  });

  it('passes props into quiver SpotMarker', () => {
    const props = {
      prop1: 'prop1',
      prop2: 'prop2',
    };
    const wrapper = shallow(<SpotMarker {...props} />);
    const spotMarker = wrapper.find(QuiverSpotMarker);
    expect(spotMarker.prop('prop1')).to.deep.equal('prop1');
    expect(spotMarker.prop('prop2')).to.deep.equal('prop2');
  });
});
