import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ForecastName from './ForecastName';

describe('components / ForecastName', () => {
  it('should update class from modifier', () => {
    const modifier = 'subregion';
    const wrapper = shallow(<ForecastName modifier={modifier} />);
    const forecastNameWrapper = wrapper.find(`.forecastName--${modifier}`);
    expect(forecastNameWrapper).to.have.length(1);
  });
});
