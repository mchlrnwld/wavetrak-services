import PropTypes from 'prop-types';
import React from 'react';

const ForecastName = ({ name, modifier }) => (
  <div className={`forecastName--${modifier}`}>{name}</div>
);

ForecastName.propTypes = {
  name: PropTypes.string.isRequired,
  modifier: PropTypes.string,
};

ForecastName.defaultProps = {
  modifier: 'default',
};

export default ForecastName;
