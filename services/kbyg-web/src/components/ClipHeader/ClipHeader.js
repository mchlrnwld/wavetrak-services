import React from 'react';
import PropTypes from 'prop-types';
import { ShareArticle } from '@surfline/quiver-react';
import { trackEvent } from '@surfline/web-common';

import styles from './ClipHeader.module.scss';

const ClipHeader = ({ firstName, spotName, url }) => {
  const name = firstName || 'Surfline Sessions User';
  const clipTitle = `${name}'s wave from ${spotName}`;
  return (
    <div className={styles.clipHeader}>
      <h4 className={styles.clipTitle}>{clipTitle}</h4>
      <div className={styles.headerSocials}>
        <ShareArticle
          url={url}
          onClickLink={(shareChannel) =>
            trackEvent('Clicked Share Icon', {
              title: clipTitle,
              contentType: 'rewind',
              locationCategory: 'Sessions Wave',
              destinationUrl: url,
              mediaType: 'video',
              shareChannel,
            })
          }
        />
      </div>
    </div>
  );
};

ClipHeader.propTypes = {
  spotName: PropTypes.string.isRequired,
  firstName: PropTypes.string,
  url: PropTypes.string.isRequired,
};

ClipHeader.defaultProps = {
  firstName: null,
};

export default ClipHeader;
