import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { CTAProvider } from '@surfline/quiver-react';
import { getLocalDateForFutureDay } from '@surfline/web-common';
import unitsPropType from '../../propTypes/units';
import summaryDayPropType from '../../propTypes/summaryDay';
import surfPropType from '../../propTypes/surf';
import waveMaxHeightTimestepPropType from '../../propTypes/waveMaxHeightTimestep';
import ChartMobileDay from '../ChartMobileDay';
import ChartDesktopRange from '../ChartDesktopRange';
import ChartRangeSelectorCTA from '../ChartRangeSelectorCTA';
import SwellTrend from '../SwellTrend';

const rangeClassName = (active) =>
  classNames({
    'sl-chart-range-selector__range': true,
    'sl-chart-range-selector__range--active': active,
  });

class ChartRangeSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chartRangeSelectorRef: null,
    };
  }

  setChartRangeSelectorRef = (el) => {
    this.setState({
      chartRangeSelectorRef: el,
    });
  };

  getSubscribeCTAProperties = () => {
    const { type, spotId } = this.props;
    return {
      location: 'Mobile Range Selector',
      pageName: type === 'SPOT' ? 'Spot Forecast' : 'Regional Forecast',
      spotId,
    };
  };

  render() {
    const {
      mobile,
      ranges,
      selectedStartDay,
      daysEnabled,
      onSelectRange,
      waveDays,
      waveMaxHeightsDays,
      summaryDays,
      units,
      utcOffset,
      showConditions,
      isPremium,
      type,
      isTabbing,
      dateFormat,
      sevenRatings,
      splitReady,
    } = this.props;

    const { chartRangeSelectorRef } = this.state;

    return (
      <div className="sl-chart-range-selector" ref={this.setChartRangeSelectorRef}>
        {waveMaxHeightsDays && chartRangeSelectorRef && (
          <SwellTrend data={waveMaxHeightsDays} parentRef={chartRangeSelectorRef} height={80} />
        )}
        {ranges.map((range, index) => {
          const { start, end } = range;
          const localDate = getLocalDateForFutureDay(index, utcOffset);
          const computedWaveDay =
            start >= daysEnabled ? [{ surf: { min: 1, max: 2 } }] : waveDays && waveDays[index];
          const isActive = isTabbing
            ? start === selectedStartDay
            : start <= selectedStartDay && selectedStartDay <= end;
          return (
            <button
              type="button"
              key={start}
              className={rangeClassName(isActive)}
              onClick={() => onSelectRange(range)}
              disabled={start >= daysEnabled}
            >
              {mobile ? (
                <ChartMobileDay
                  localDate={localDate}
                  waveDay={computedWaveDay}
                  summaryDay={summaryDays && summaryDays[index]}
                  units={units}
                  showConditions={showConditions && splitReady}
                  obscured={start >= daysEnabled}
                  type={type}
                  dateFormat={dateFormat}
                  sevenRatings={sevenRatings}
                />
              ) : (
                <ChartDesktopRange start={start} end={end} />
              )}
            </button>
          );
        })}
        <div>
          {isPremium ? null : (
            <CTAProvider segmentProperties={this.getSubscribeCTAProperties()}>
              <ChartRangeSelectorCTA
                mobile={mobile}
                getSubscribeCTAProperties={this.getSubscribeCTAProperties}
              />
            </CTAProvider>
          )}
        </div>
      </div>
    );
  }
}

ChartRangeSelector.propTypes = {
  mobile: PropTypes.bool,
  ranges: PropTypes.arrayOf(
    PropTypes.shape({
      start: PropTypes.number.isRequired,
      end: PropTypes.number.isRequired,
    }),
  ).isRequired,
  selectedStartDay: PropTypes.number.isRequired,
  onSelectRange: PropTypes.func.isRequired,
  daysEnabled: PropTypes.number.isRequired,
  waveDays: PropTypes.arrayOf(PropTypes.arrayOf(surfPropType)),
  waveMaxHeightsDays: PropTypes.arrayOf(PropTypes.arrayOf(waveMaxHeightTimestepPropType)),
  summaryDays: PropTypes.arrayOf(summaryDayPropType),
  units: unitsPropType,
  utcOffset: PropTypes.number.isRequired,
  showConditions: PropTypes.bool,
  isPremium: PropTypes.bool,
  type: PropTypes.string.isRequired,
  spotId: PropTypes.string.isRequired,
  isTabbing: PropTypes.bool,
  dateFormat: PropTypes.oneOf(['MDY', 'DMY']),
  sevenRatings: PropTypes.bool,
  splitReady: PropTypes.bool,
};

ChartRangeSelector.defaultProps = {
  mobile: false,
  waveDays: null,
  waveMaxHeightsDays: null,
  summaryDays: null,
  units: null,
  showConditions: false,
  isPremium: false,
  isTabbing: false,
  dateFormat: 'MDY',
  sevenRatings: false,
  splitReady: false,
};

export default ChartRangeSelector;
