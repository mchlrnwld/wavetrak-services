import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import ChartDesktopRange from '../ChartDesktopRange';
import ChartMobileDay from '../ChartMobileDay';
import ChartRangeSelector from './ChartRangeSelector';

describe('components / ChartRangeSelector', () => {
  let clock;
  const now = 1512115200; // 12/01/2017@00:00PST
  const desktopRanges = [
    { start: 0, end: 4 },
    { start: 5, end: 9 },
    { start: 10, end: 14 },
    { start: 15, end: 16 },
  ];

  const mobileRanges = [
    { start: 0, end: 0 },
    { start: 1, end: 1 },
    { start: 2, end: 2 },
    { start: 3, end: 3 },
  ];
  const units = { waveHeight: 'FT' };
  const utcOffset = -8;

  beforeAll(() => {
    clock = sinon.useFakeTimers(now * 1000);
  });

  afterAll(() => {
    clock.restore();
  });

  it('marks selected range as active', () => {
    const wrapper = shallow(<ChartRangeSelector ranges={desktopRanges} selectedStartDay={6} />);
    const rangeList = wrapper.find('.sl-chart-range-selector__range');
    expect(rangeList).to.have.length(4);
    expect(rangeList.at(0).prop('className')).not.to.contain(
      'sl-chart-range-selector__range--active',
    );
    expect(rangeList.at(1).prop('className')).to.contain('sl-chart-range-selector__range--active');
    expect(rangeList.at(2).prop('className')).not.to.contain(
      'sl-chart-range-selector__range--active',
    );
    expect(rangeList.at(3).prop('className')).not.to.contain(
      'sl-chart-range-selector__range--active',
    );
  });

  it('handles range selection', () => {
    const handleSelectRange = sinon.spy();
    const wrapper = shallow(
      <ChartRangeSelector ranges={desktopRanges} onSelectRange={handleSelectRange} />,
    );
    const rangeList = wrapper.find('.sl-chart-range-selector__range');
    expect(rangeList).to.have.length(4);
    rangeList.at(1).simulate('click');
    expect(handleSelectRange).to.have.been.calledOnce();
    expect(handleSelectRange).to.have.been.calledWithExactly(desktopRanges[1]);
  });

  it('renders provided ranges for desktop', () => {
    const wrapper = shallow(<ChartRangeSelector ranges={desktopRanges} />);

    const mobileDayList = wrapper.find(ChartMobileDay);
    expect(mobileDayList).to.have.length(0);

    const desktopRangeList = wrapper.find(ChartDesktopRange);
    expect(desktopRangeList).to.have.length(4);
    expect(desktopRangeList.at(0).props()).to.deep.equal({ start: 0, end: 4 });
    expect(desktopRangeList.at(1).props()).to.deep.equal({ start: 5, end: 9 });
    expect(desktopRangeList.at(2).props()).to.deep.equal({ start: 10, end: 14 });
    expect(desktopRangeList.at(3).props()).to.deep.equal({ start: 15, end: 16 });
  });

  it('renders provided ranges for mobile with date showing after current week', () => {
    const waveDays = [1, 2, 3, 4];
    const summaryDays = [5, 6, 7, 8];
    const wrapper = shallow(
      <ChartRangeSelector
        mobile
        ranges={mobileRanges}
        waveDays={waveDays}
        summaryDays={summaryDays}
        units={units}
        utcOffset={utcOffset}
        type="SPOT"
        dateFormat="MDY"
        sevenRatings={false}
      />,
    );

    const desktopRangeList = wrapper.find(ChartDesktopRange);
    expect(desktopRangeList).to.have.length(0);

    const mobileDayList = wrapper.find(ChartMobileDay);
    expect(mobileDayList).to.have.length(4);
    expect(mobileDayList.at(0).props()).to.deep.equal({
      localDate: new Date('Fri Dec 01 2017 00:00:00'), // Friday
      waveDay: 1,
      summaryDay: 5,
      units,
      showConditions: false,
      obscured: false,
      type: 'SPOT',
      dateFormat: 'MDY',
      sevenRatings: false,
    });
    expect(mobileDayList.at(1).props()).to.deep.equal({
      localDate: new Date('Fri Dec 02 2017 00:00:00'), // Saturday
      waveDay: 2,
      summaryDay: 6,
      units,
      showConditions: false,
      obscured: false,
      type: 'SPOT',
      dateFormat: 'MDY',
      sevenRatings: false,
    });
    expect(mobileDayList.at(2).props()).to.deep.equal({
      localDate: new Date('Fri Dec 03 2017 00:00:00'), // Sunday
      waveDay: 3,
      summaryDay: 7,
      units,
      showConditions: false,
      obscured: false,
      type: 'SPOT',
      dateFormat: 'MDY',
      sevenRatings: false,
    });
    expect(mobileDayList.at(3).props()).to.deep.equal({
      localDate: new Date('Fri Dec 04 2017 00:00:00'), // Monday
      waveDay: 4,
      summaryDay: 8,
      units,
      showConditions: false,
      obscured: false,
      type: 'SPOT',
      dateFormat: 'MDY',
      sevenRatings: false,
    });
  });

  it('renders provided mobile ranges even when waveDays are not available', () => {
    const wrapper = shallow(
      <ChartRangeSelector mobile ranges={mobileRanges} units={units} utcOffset={utcOffset} />,
    );
    const mobileDayList = wrapper.find(ChartMobileDay);
    expect(mobileDayList).to.have.length(4);
    expect(mobileDayList.at(0).prop('waveDay')).to.be.null();
    expect(mobileDayList.at(1).prop('waveDay')).to.be.null();
    expect(mobileDayList.at(2).prop('waveDay')).to.be.null();
    expect(mobileDayList.at(3).prop('waveDay')).to.be.null();
  });
});
