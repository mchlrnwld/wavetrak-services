import React from 'react';
import { format } from 'date-fns';
import { color } from '@surfline/quiver-themes';
import classNames from 'classnames';

import Star from '../Icons/Star';
import conditionClassModifier from '../../utils/conditionClassModifier';
import styles from './SessionDetailsBanner.module.scss';

interface Props {
  timezoneAbbr?: string;
  sessionName?: string;
  sessionRating?: number;
  sessionStartTime?: Date | number;
  sessionSpot?: {
    name: string;
  };
  sessionConditions?: {
    conditions: { value: string };
  };
}

const conditionClassName = (condition?: string) =>
  classNames({
    [styles.detailsBannerSubHeaderSessionSpot]: true,
    [styles[`detailsBannerSubHeaderSessionSpot--${conditionClassModifier(condition || '')}`]]: true,
  });

const SessionDetailsBanner: React.FC<Props> = ({
  sessionName = "User's Session",
  sessionRating = 0,
  sessionConditions = {},
  sessionSpot = {},
  sessionStartTime,
  timezoneAbbr,
}) => (
  <div className={styles.detailsBannerContainer}>
    <div className={styles.detailsBannerHeader}>
      <h6 className={styles.detailsBannerHeaderName}>{sessionName}</h6>
      <div>
        {[...Array(sessionRating)].map((_, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <Star key={index} fill={color('white')} />
        ))}
      </div>
    </div>
    <div className={styles.detailsBannerSubHeader}>
      <p className={styles.detailsBannerSubHeaderSessionDate}>
        {sessionStartTime
          ? `${format(sessionStartTime, "MMM, d '-' h':'mmaaaaa'm'")}${
              timezoneAbbr ? ` ${timezoneAbbr}` : ''
            }`
          : 'Unknown start time'}
      </p>
      <p className={conditionClassName(sessionConditions?.conditions?.value)}>{sessionSpot.name}</p>
    </div>
  </div>
);

export default SessionDetailsBanner;
