import { expect } from 'chai';
import { SpotListItem } from '@surfline/quiver-react';
import sinon from 'sinon';
import GeonameMapHeader from '../GeonameMapHeader';
import SubregionHeader from '../SubregionHeader';
import SpotListMessageHeader from '../SpotListMessageHeader';
import SpotListSummary from '../SpotListSummary';
import SpotList from './SpotList';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import * as splitSelectors from '../../selectors/split';

describe('components / SpotList', () => {
  const subregions = {
    subregions: [
      {
        _id: '58581a836630e24c44878fd6',
        subregion: {
          name: 'North Orange County',
        },
      },
      {
        _id: '58581a836630e24c4487900a',
        subregion: {
          name: 'South Orange County',
        },
      },
    ],
    doCluster: false,
  };

  const spots = [
    {
      _id: '5842041f4e65fad6a77088f3',
      name: 'Corona del Mar',
      subregionId: '58581a836630e24c44878fd6',
      cameras: [],
      wind: {},
      conditions: {},
      waveHeight: {},
    },
    {
      _id: '5842041f4e65fad6a7708f21',
      name: 'Crystal Cove',
      subregionId: '58581a836630e24c44878fd6',
      cameras: [],
      wind: {},
      conditions: {},
      waveHeight: {},
    },
    {
      _id: '5842041f4e65fad6a77088e3',
      name: 'Rockpile',
      subregionId: '58581a836630e24c4487900a',
      cameras: [],
      wind: {},
      conditions: {},
      waveHeight: {},
    },
  ];

  const units = {
    waveHeight: 'FT',
    windSpeed: 'KTS',
    temperature: 'F',
  };

  const props = {
    spots,
    subregions,
    units,
    animations: {
      activeSpotId: '5842041f4e65fad6a7708f21',
    },
    entitlements: [],
  };

  beforeEach(() => {
    sinon.stub(splitSelectors, 'useSplitTreatment').returns({ loading: false, treatment: null });
  });

  afterEach(() => {
    splitSelectors.useSplitTreatment.restore();
  });

  it('renders list items', () => {
    const { wrapper } = mountWithReduxAndRouter(SpotList, props);

    const listItemsContainer = wrapper.find('.sl-spot-list__spots');
    expect(listItemsContainer).to.have.length(1);

    const listItems = listItemsContainer.children();
    expect(listItems).to.have.length(5);

    const subregionHeader1 = listItems.at(0);
    expect(subregionHeader1.type()).to.equal(SubregionHeader);
    expect(subregionHeader1.prop('subregion')).to.equal(subregions.subregions[0].subregion);

    const spot1 = listItems.at(1).find(SpotListItem);
    expect(spot1).to.have.length(1);
    expect(spot1.prop('spot')).to.equal(spots[0]);
    expect(spot1.prop('units')).to.equal(units);

    const spot2 = listItems.at(2).find(SpotListItem);
    expect(spot2).to.have.length(1);
    expect(spot2.prop('spot')).to.equal(spots[1]);
    expect(spot2.prop('units')).to.equal(units);

    const subregionHeader2 = listItems.at(3);
    expect(subregionHeader2.type()).to.equal(SubregionHeader);
    expect(subregionHeader2.prop('subregion')).to.equal(subregions.subregions[1].subregion);

    const spot3 = listItems.at(4).find(SpotListItem);
    expect(spot3).to.have.length(1);
    expect(spot3.prop('spot')).to.equal(spots[2]);
    expect(spot3.prop('units')).to.equal(units);
  });

  it('renders a geoname map header and spot list summary if locationview is provided', () => {
    const { wrapper: defaultWrapper } = mountWithReduxAndRouter(SpotList, props);
    expect(defaultWrapper.find(GeonameMapHeader)).to.have.length(0);
    expect(defaultWrapper.find(SpotListSummary)).to.have.length(0);

    const locationView = {
      breadCrumbs: [
        {
          name: 'United States',
          url: '/united-states/6252001',
          geonameId: 6252001,
          id: '58f7ed51dadb30820bb3879c',
        },
      ],
      taxonomy: {
        name: '',
      },
    };
    const { wrapper: locationViewWrapper } = mountWithReduxAndRouter(SpotList, {
      ...props,
      locationView,
    });

    const geonameMapHeader = locationViewWrapper.find(GeonameMapHeader);
    expect(geonameMapHeader).to.have.length(1);
    expect(geonameMapHeader.prop('locationView')).to.equal(locationView);

    const spotListSummary = locationViewWrapper.find(SpotListSummary);
    expect(spotListSummary).to.have.length(1);
    expect(spotListSummary.prop('locationView')).to.equal(locationView);
  });

  it('renders message header if message is provided', () => {
    const { wrapper: defaultWrapper } = mountWithReduxAndRouter(SpotList, props);
    expect(defaultWrapper.find(SpotListMessageHeader)).to.have.length(0);

    const message = {};
    const { wrapper: messageWrapper } = mountWithReduxAndRouter(SpotList, { ...props, message });
    const spotListMessageHeader = messageWrapper.find(SpotListMessageHeader);
    expect(spotListMessageHeader.prop('message')).to.equal(message);
    expect(spotListMessageHeader.prop('hasSpots')).to.be.true();
  });

  it('renders subregion headers if doCluster is false', () => {
    const { wrapper: defaultWrapper } = mountWithReduxAndRouter(SpotList, props);
    expect(defaultWrapper.find(SubregionHeader)).to.have.length(2);
  });

  it('does not render subregion header if doCluster is true', () => {
    const clusterProps = {
      ...props,
      subregions: {
        subregions: [],
        doCluster: false,
      },
    };
    const { wrapper: defaultWrapper } = mountWithReduxAndRouter(SpotList, clusterProps);
    expect(defaultWrapper.find(SubregionHeader)).to.have.length(0);
  });
});
