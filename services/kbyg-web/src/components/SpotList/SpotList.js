import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classnames from 'classnames';
import { SurflineFooter, SpotListItem } from '@surfline/quiver-react';
import spotsPropType from '../../propTypes/spot';
import unitsPropType from '../../propTypes/units';
import animationsPropType from '../../propTypes/animations';
import locationViewPropShape from '../../propTypes/locationView';
import subregionsPropType from '../../propTypes/subregions';
import GeonameMapHeader from '../GeonameMapHeader';
import SpotListSummary from '../SpotListSummary';
import SubregionHeader from '../SubregionHeader';
import CamListLink from '../CamListLink';
import SpotListMessageHeader from '../SpotListMessageHeader';
import withConditionVariationV2 from '../../common/withConditionVariationV2';
import { showPremiumCamCTA } from '../../utils/premiumCam';

const SpotListItemWithConditionVariation = withConditionVariationV2(SpotListItem);

class SpotList extends Component {
  constructor(props) {
    super(props);

    this.spotRefs = {};
  }

  componentDidUpdate(prevProps) {
    const { scrollTop, scrollLeft, animations, doSetScrollPosition } = this.props;
    if (animations.spotListScrollAnchor !== prevProps.animations.spotListScrollAnchor) {
      const foundItem = this.spotRefs[animations.spotListScrollAnchor];
      const top = foundItem?.offsetTop;
      const left = foundItem?.offsetLeft;
      doSetScrollPosition({ top, left });
    }

    if (scrollTop !== prevProps.scrollTop) {
      this.el.scrollTop = scrollTop - 10;
    } else if (scrollLeft !== prevProps.scrollLeft) {
      const offSetCenter = this.getOffSetCenter(this.el);
      this.el.scrollLeft = scrollLeft - offSetCenter;
    }
  }

  getOffSetCenter = (el, itemWidth = 176) => el.clientWidth / 2 - itemWidth / 2;

  getSpotListSpotsClasses = () => {
    const { spots } = this.props;
    return classnames({
      'sl-spot-list__spots': true,
      'sl-spot-list__spots--no-spots': spots.length < 1,
    });
  };

  renderListItems = () => {
    const {
      locationView,
      spots,
      subregions,
      units,
      animations,
      doSetActiveSpot,
      doClearActiveSpot,
      isPremium,
    } = this.props;
    let nextSubregion;
    const summarySubregions = [];
    const spotListItemsToRender = [];
    const subregionMap = new Map(
      subregions.subregions.map((subregion) => [subregion._id, subregion.subregion]),
    );

    spots.forEach((spot) => {
      if (nextSubregion !== spot.subregionId) {
        const subregion = subregionMap.get(spot.subregionId);
        if (subregion) {
          if (summarySubregions.indexOf(subregion) === -1) summarySubregions.push(subregion);
          if (!subregions.doCluster) {
            nextSubregion = spot.subregionId;
            spotListItemsToRender.push(
              <SubregionHeader subregion={subregion} key={spot.subregionId} />,
            );
          }
        }
      }

      spotListItemsToRender.push(
        <div
          className="sl-spot-list__ref"
          key={spot._id}
          ref={(spotEl) => {
            this.spotRefs[spot._id] = spotEl;
          }}
        >
          <CamListLink
            spot={spot}
            active={animations.activeSpotId === spot._id}
            location="map page"
            onMouseEnter={() => doSetActiveSpot(spot._id)}
            onMouseLeave={doClearActiveSpot}
          >
            <SpotListItemWithConditionVariation
              spot={spot}
              units={units}
              showPremiumCamCTA={showPremiumCamCTA({
                spot,
                isPremium,
              })}
            />
          </CamListLink>
        </div>,
      );
    });

    if (locationView) {
      spotListItemsToRender.push(
        <SpotListSummary
          locationView={locationView}
          key="spotlist-summary"
          subregions={summarySubregions}
        />,
      );
    }

    return spotListItemsToRender;
  };

  render() {
    const { locationView, message, spots, entitlements } = this.props;
    return (
      <div
        ref={(el) => {
          this.el = el;
        }}
        className="sl-spot-list"
      >
        {locationView ? <GeonameMapHeader locationView={locationView} /> : null}
        {message ? <SpotListMessageHeader message={message} hasSpots={!!spots.length} /> : null}
        <div className={this.getSpotListSpotsClasses()}>{this.renderListItems()}</div>
        <SurflineFooter small entitlements={entitlements} />
      </div>
    );
  }
}

SpotList.propTypes = {
  locationView: locationViewPropShape,
  spots: PropTypes.arrayOf(spotsPropType).isRequired,
  subregions: PropTypes.shape({
    subregions: PropTypes.arrayOf(subregionsPropType),
    doCluster: PropTypes.bool,
  }).isRequired,
  units: unitsPropType.isRequired,
  scrollTop: PropTypes.number,
  scrollLeft: PropTypes.number,
  animations: animationsPropType.isRequired,
  doSetActiveSpot: PropTypes.func.isRequired,
  doClearActiveSpot: PropTypes.func.isRequired,
  doSetScrollPosition: PropTypes.func.isRequired,
  message: PropTypes.shape({
    text: PropTypes.string,
    optionalText: PropTypes.string,
  }),
  entitlements: PropTypes.arrayOf(PropTypes.string).isRequired,
  isPremium: PropTypes.bool,
};

SpotList.defaultProps = {
  scrollTop: 0,
  scrollLeft: 0,
  message: null,
  locationView: null,
  isPremium: false,
};

export default SpotList;
