import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import MapButton from '../MapButton';
import LocationArrowIcon from '../LocationArrowIcon';
import MapLocationButton from './MapLocationButton';

describe('components / MapLocationButton', () => {
  it('renders MapButton with LocationArrowIcon', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(<MapLocationButton onClick={onClick} />);

    const mapButton = wrapper.find(MapButton);
    expect(mapButton).to.have.length(1);

    mapButton.simulate('click');
    expect(onClick).to.have.been.calledOnce();

    const locationArrowIcon = mapButton.find(LocationArrowIcon);
    expect(locationArrowIcon).to.have.length(1);
  });
});
