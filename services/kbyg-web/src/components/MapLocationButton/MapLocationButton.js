import PropTypes from 'prop-types';
import React from 'react';
import MapButton from '../MapButton';
import LocationArrowIcon from '../LocationArrowIcon';

const MapLocationButton = ({ onClick }) => (
  <div className="sl-map-location-button">
    <MapButton onClick={onClick}>
      <LocationArrowIcon />
    </MapButton>
  </div>
);

MapLocationButton.propTypes = {
  onClick: PropTypes.func,
};

MapLocationButton.defaultProps = {
  onClick: () => null,
};

export default MapLocationButton;
