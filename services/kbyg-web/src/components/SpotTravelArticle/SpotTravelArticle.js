import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Spinner } from '@surfline/quiver-react';
import { fetchEditorialArticleById } from '../../common/api/editorial';

const getBreadcrumbs = ({ series, categories }) =>
  [series[0], categories[0]].filter((item) => item);

const SpotTravelArticle = ({ articleId }) => {
  const [article, setArticle] = useState();

  useEffect(() => {
    const fetchArticle = async () => {
      try {
        const response = await fetchEditorialArticleById(articleId);

        if (response.id === +articleId) {
          setArticle(response);
        }
      } catch (error) {
        setArticle(undefined);
      }
    };
    fetchArticle();
  }, [articleId]);

  return (
    <article
      className={`
        sl-spot-travel-article
        ${!article ? 'sl-spot-travel-article--loading' : ''}
      `}
    >
      {!article ? (
        <Spinner />
      ) : (
        <>
          <figure className="sl-spot-travel-article__thumbnail">
            <div
              className="sl-spot-travel-article__thumbnail__img"
              alt={article.content.title}
              style={{
                backgroundImage: `url(${article.media.feed1x})`,
              }}
            />
          </figure>
          <div className="sl-spot-travel-article__details">
            <ul className="sl-spot-travel-article__details__breadcrumbs">
              {getBreadcrumbs(article).map(({ name, slug, url }) => (
                <li key={slug} className="sl-spot-travel-article__details__breadcrumb">
                  <a
                    className="sl-spot-travel-article__details__breadcrumb__link"
                    href={url}
                    title={name}
                  >
                    {name}
                  </a>
                </li>
              ))}
            </ul>
            <h3 className="sl-spot-travel-article__details__title">
              <a href={article.permalink} title={article.content.title}>
                {article.content.title}
              </a>
            </h3>
            <p className="sl-spot-travel-article__details__subtitle">{article.content.subtitle}</p>
            <a className="sl-spot-travel-article__details__link" href={article.permalink}>
              Read Article
            </a>
          </div>
        </>
      )}
    </article>
  );
};

SpotTravelArticle.propTypes = {
  articleId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
};

export default SpotTravelArticle;
