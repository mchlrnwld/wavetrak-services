import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import { ForecasterProfile } from '@surfline/quiver-react';
import HighlightList from '../HighlightList';
import RegionalForecast from './RegionalForecast';
import { overviewDataOff, overviewDataActive } from './fixtures/overviewData';

describe('components / RegionalForecast', () => {
  let mockStore;

  beforeAll(() => {
    mockStore = configureStore();
  });

  it('does not render ForecasterProfile or HighlightList if status is off', () => {
    const store = mockStore(overviewDataOff);
    const { subregion } = store.getState();
    const subregionAdConfig = {
      adTargets: [['subregionid', '12345']],
      userEntitlements: [],
      subregionId: '12345',
      isUser: false,
    };
    const wrapper = shallow(
      <RegionalForecast
        overviewData={subregion.overview.data}
        forecastUnits={subregion.overview.associated.units}
        subregionAdConfig={subregionAdConfig}
      />,
    );
    expect(wrapper.find(ForecasterProfile)).to.have.length(0);
    expect(wrapper.find(HighlightList)).to.have.length(0);
  });

  it('renders a Region Forecast Update if status is active', () => {
    const store = mockStore(overviewDataActive);
    const { subregion } = store.getState();
    const subregionAdConfig = {
      adTargets: [['subregionid', '12345']],
      userEntitlements: [],
      subregionId: '12345',
      isUser: false,
    };
    const wrapper = shallow(
      <RegionalForecast
        overviewData={subregion.overview.data}
        subregionAdConfig={subregionAdConfig}
        isPremium
      />,
    );
    expect(wrapper.find('RegionForecastUpdate')).to.have.length(1);
  });
});
