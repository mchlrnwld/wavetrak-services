export const overviewDataOff = {
  subregion: {
    overview: {
      associated: {
        advertising: {
          spotId: null,
          subregionId: '2144',
        },
        utcOffset: -8,
        units: {
          temperature: 'F',
          tideHeight: 'FT',
          waveHeight: 'FT',
          windSpeed: 'KTS',
        },
        chartsUrl: '/surf-charts/north-america/southern-california/north-san-diego/2144',
      },
      data: {
        _id: '58581a836630e24c44878fd7',
        name: 'North San Diego',
        primarySpot: '5842041f4e65fad6a77088b7',
        breadcrumb: [
          {
            name: 'United States',
            href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/6252001',
          },
          {
            name: 'San Diego',
            href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/san-diego/5391832',
          },
          {
            name: 'California',
            href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/5332921',
          },
        ],
        timestamp: 1512774170,
        forecastSummary: {
          forecastStatus: {
            status: 'inactive',
            inactiveMessage: 'sdfasdfasdfasds',
          },
          nextForecast: {
            timestamp: 1631818800,
            utcOffset: -7,
          },
          forecaster: {
            title: 'Development Employee',
            name: 'Super Admin',
            email: 'developer@surfline.com',
          },
          highlights: [],
        },
        spots: [],
      },
    },
  },
};

export const overviewDataActive = {
  subregion: {
    overview: {
      associated: {
        advertising: {
          spotId: null,
          subregionId: '2144',
        },
        utcOffset: -8,
        units: {
          temperature: 'F',
          tideHeight: 'FT',
          waveHeight: 'FT',
          windSpeed: 'KTS',
        },
        chartsUrl: '/surf-charts/north-america/southern-california/north-san-diego/2144',
      },
      data: {
        _id: '58581a836630e24c44878fd7',
        name: 'North San Diego',
        primarySpot: '5842041f4e65fad6a77088b7',
        breadcrumb: [
          {
            name: 'United States',
            href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/6252001',
          },
          {
            name: 'San Diego',
            href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/san-diego/5391832',
          },
          {
            name: 'California',
            href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/5332921',
          },
        ],
        timestamp: 1512774170,
        forecastSummary: {
          forecastStatus: {
            status: 'active',
            inactiveMessage: 'sdfasdfasdfasds',
          },
          nextForecastTimestamp: 1513098000,
          forecaster: {
            title: 'Development Employee',
            name: 'Super Admin',
            email: 'developer@surfline.com',
          },
          highlights: ['highlight 1'],
          hasHighlights: true,
        },
        spots: [],
      },
    },
  },
};
