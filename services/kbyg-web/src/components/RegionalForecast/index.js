import RegionalForecast from './RegionalForecast';
import withMetering from '../../common/withMetering';

export default withMetering(RegionalForecast);
