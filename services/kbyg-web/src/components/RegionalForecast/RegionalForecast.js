import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames';
import { ContentContainer } from '@surfline/quiver-react';
import { postedDate } from '@surfline/web-common';
import RegionForecastUpdate from '../RegionForecastUpdate';
import InactiveMessage from '../InactiveMessage';
import ForecastDataVisualizer from '../../containers/ForecastDataVisualizer';
import MobileForecastCTA from '../MobileForecastCTA/MobileForecastCTA';
import devicePropTypes from '../../propTypes/device';
import summaryPropType from '../../propTypes/summary';
import userSettingsPropTypes from '../../propTypes/userSettings';
import overviewDataPropShape from '../../propTypes/subregionOverviewData';
import meterPropType from '../../propTypes/meter';
import treatmentPropType from '../../propTypes/treatment';
import { LOTUS } from '../../common/constants';

const className = (highlights) =>
  classNames({
    'sl-regional-forecast': true,
    'sl-regional-forecast--no-highlights': highlights.length === 0,
  });

class RegionalForecast extends Component {
  getCurrentCondition = (summary) => {
    if (!summary?.days) {
      return null;
    }
    const conditionDay = summary?.days[0];
    const pm = new Date().getHours() > 12;
    const currentCondition = conditionDay[pm ? 'pm' : 'am'].rating;
    return currentCondition;
  };

  getSegmentProperties = () => {
    const {
      overviewData: { _id: subregionId },
    } = this.props;

    return {
      location: 'mobile forecast go premium',
      pageName: 'Regional Forecast',
      subregionId,
    };
  };

  render() {
    const {
      isPremium,
      primarySpotId,
      overviewData: { _id: subregionId, name: subregionName, forecastSummary, spots, timestamp },
      utcOffset,
      abbrTimezone,
      device,
      summary,
      meter,
      userSettings,
      userDetails,
      conditionsTreatment,
    } = this.props;
    const showMeterRegwall = meter?.showMeterRegwall;
    const primarySpot = spots.find((spot) => spot._id === primarySpotId);

    let dateFormat = 'MDY';
    if (userSettings && userSettings.date && userSettings.date.format)
      dateFormat = userSettings.date.format;
    const regionForecastUpdateAvailable =
      forecastSummary.forecastStatus.status === 'active' &&
      (forecastSummary.hasHighlights || forecastSummary.hasBets);
    const isAnonymous = !userDetails;
    const showPaywall =
      meter?.meteringEnabled && (isAnonymous || (userDetails && !(meter?.meterRemaining > -1)));
    return (
      <div className={className(forecastSummary.highlights)}>
        {regionForecastUpdateAvailable ? (
          <ContentContainer>
            <RegionForecastUpdate
              dateFormat={dateFormat}
              forecastSummary={forecastSummary}
              subregionId={subregionId}
              subregionName={subregionName}
              timestamp={timestamp}
              condition={this.getCurrentCondition(summary)}
              utcOffset={utcOffset}
              abbrTimezone={abbrTimezone}
              isPremium={isPremium}
              clickEventProps={{
                category: 'regionForecast',
                linkLocation: 'regional forecast',
              }}
              showPaywall={showPaywall}
              isAnonymous={isAnonymous}
            />
          </ContentContainer>
        ) : null}
        {forecastSummary.forecastStatus.status === 'inactive' &&
        forecastSummary.forecastStatus.inactiveMessage ? (
          <ContentContainer>
            <InactiveMessage message={forecastSummary.forecastStatus.inactiveMessage} />
          </ContentContainer>
        ) : null}
        <ContentContainer>
          {!isPremium ? (
            <div className="sl-regional-forecast__mobile-cta">
              <MobileForecastCTA segmentProperties={this.getSegmentProperties} />
            </div>
          ) : null}
          {primarySpotId && subregionId && !showMeterRegwall ? (
            <div className="sl-regional-forecast__charts">
              <ForecastDataVisualizer
                type="REGIONAL"
                category="regional forecast"
                pageName="Regional Forecast"
                spotId={primarySpotId}
                subregionId={subregionId}
                spotName={primarySpot ? primarySpot.name : null}
                utcOffset={utcOffset}
                lastUpdate={timestamp ? postedDate(timestamp, utcOffset, abbrTimezone) : null}
                extended
                canToggleDataView
                device={device}
                dateFormat={dateFormat}
                model={LOTUS}
                showForecastGraphsDaySummariesCTA={showPaywall}
                isAnonymous={isAnonymous}
                sevenRatings={conditionsTreatment?.treatment === 'on'}
              />
            </div>
          ) : null}
        </ContentContainer>
      </div>
    );
  }
}

RegionalForecast.propTypes = {
  primarySpotId: PropTypes.string,
  overviewData: overviewDataPropShape.isRequired,
  isPremium: PropTypes.bool,
  utcOffset: PropTypes.number.isRequired,
  abbrTimezone: PropTypes.string.isRequired,
  device: devicePropTypes.isRequired,
  summary: summaryPropType.isRequired,
  meter: meterPropType,
  userSettings: userSettingsPropTypes,
  conditionsTreatment: treatmentPropType,
};

RegionalForecast.defaultProps = {
  primarySpotId: null,
  isPremium: false,
  meter: null,
  userSettings: null,
  conditionsTreatment: null,
};

export default RegionalForecast;
