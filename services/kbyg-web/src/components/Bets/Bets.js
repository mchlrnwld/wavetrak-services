import PropTypes from 'prop-types';
import React from 'react';
import BestBetLabel from '../Icons/BestBetLabel';
import WorstBetLabel from '../Icons/WorstBetLabel';

const Bets = ({ bestBets, bets, hasBets, hasBestBets }) => (
  <div className="sl-bets">
    {hasBets && bets && !hasBestBets && (
      <>
        <p className="sl-bets__header">Next 24 Hrs</p>
        {bets.best ? (
          <div className="sl-bets__bet sl-bets__bet--best">
            <BestBetLabel />
            {bets.best}
          </div>
        ) : null}
        {bets.worst ? (
          <div className="sl-bets__bet sl-bets__bet--worst">
            <WorstBetLabel />
            {bets.worst}
          </div>
        ) : null}
      </>
    )}
    {hasBestBets && (
      <>
        <p className="sl-bets__header">
          <BestBetLabel />
        </p>
        <div className="sl-bets__bet">
          <ul>
            {bestBets.map((bet, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <li key={`bet-${index}`}>{bet}</li>
            ))}
          </ul>
        </div>
      </>
    )}
  </div>
);

Bets.propTypes = {
  bestBets: PropTypes.arrayOf(PropTypes.string),
  bets: PropTypes.shape({
    best: PropTypes.string,
    worst: PropTypes.string,
  }),
  hasBets: PropTypes.bool,
  hasBestBets: PropTypes.bool,
};

Bets.defaultProps = {
  bestBets: [],
  bets: {
    best: null,
    worst: null,
  },
  hasBets: false,
  hasBestBets: false,
};

export default Bets;
