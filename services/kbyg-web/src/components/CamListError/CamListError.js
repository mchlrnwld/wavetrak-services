import React from 'react';
import en from '../../intl/translations/en';
import { camsAndForecastsPaths } from '../../utils/urls/camsAndForecastsPath';
import WavetrakLink from '../WavetrakLink';

const CamListError = () => (
  <div className="sl-cam-list__error">
    <div className="sl-cam-list__error--title">{en.SpotCamera.CamList.nearbyError.title}</div>
    <div className="sl-cam-list__error--message">
      {en.SpotCamera.CamList.nearbyError.message}
      {en.SpotCamera.CamList.nearbyError.link.beforeText}
      <WavetrakLink href={`${camsAndForecastsPaths.base}-${camsAndForecastsPaths.map}`}>
        {en.SpotCamera.CamList.nearbyError.link.linkText}
      </WavetrakLink>
      {en.SpotCamera.CamList.nearbyError.link.afterText}
    </div>
  </div>
);

export default CamListError;
