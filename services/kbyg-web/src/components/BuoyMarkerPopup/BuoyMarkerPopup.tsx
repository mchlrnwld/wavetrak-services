import React from 'react';
import classNames from 'classnames';
import { Chevron, Popup } from '@surfline/quiver-react';

import WavetrakLink from '../WavetrakLink';
import { getBuoyReportPath } from '../../utils/urls';
import { NearbyStation } from '../../types/buoys';

import styles from './BuoyMarkerPopup.module.scss';

interface Props {
  buoy: NearbyStation;
}

const subtitleClass = (status: NearbyStation['status']) =>
  classNames({
    [styles.buoyMarkerPopupContentSubtitle]: true,
    [styles.buoyMarkerPopupContentSubtitleOffline]: status === 'OFFLINE',
  });

const BuoyMarkerPopup: React.FC<Props> = ({ buoy }) => (
  <Popup>
    <section className={styles.buoyMarkerPopupContent}>
      <div className={styles.buoyMarkerPopupContentTitleContainer}>
        <WavetrakLink
          href={getBuoyReportPath({
            name: buoy.name,
            id: buoy.id,
            sourceID: buoy.sourceId,
          })}
          className={styles.buoyMarkerPopupContentLink}
        >
          <h1 className={styles.buoyMarkerPopupContentTitle}>{buoy.name}</h1>
          <Chevron direction="right" />
        </WavetrakLink>
      </div>
      <span className={subtitleClass(buoy.status)}>
        {buoy.status === 'ONLINE' ? (
          buoy.sourceId
        ) : (
          <>
            Status:
            <span className={styles.buoyMarkerPopupContentOfflineSubtitle}>{' Offline'}</span>
          </>
        )}
      </span>
    </section>
  </Popup>
);

export default React.memo(BuoyMarkerPopup);
