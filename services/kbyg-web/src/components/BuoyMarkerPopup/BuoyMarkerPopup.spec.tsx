import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import BuoyMarkerPopup from './BuoyMarkerPopup';

import { getNearbyBuoysFixture } from '../../common/api/fixtures/buoys';
import WavetrakLink from '../WavetrakLink';
import { getBuoyReportPath } from '../../utils/urls';

describe('components / BuoyMarkerPopup', () => {
  it('should display an ONLINE buoy', () => {
    const [buoy] = getNearbyBuoysFixture.data;
    const wrapper = shallow(<BuoyMarkerPopup buoy={buoy} />);
    const title = wrapper.find('.buoyMarkerPopupContentTitle');
    const subtitle = wrapper.find('.buoyMarkerPopupContentSubtitle');

    expect(title.text()).to.be.equal(buoy.name);

    expect(subtitle.text()).to.equal(buoy.sourceId);
  });

  it('should render a link to the buoy page', () => {
    const [buoy] = getNearbyBuoysFixture.data;
    const wrapper = shallow(<BuoyMarkerPopup buoy={buoy} />);
    const link = wrapper.find(WavetrakLink);

    expect(link.prop('href')).to.be.equal(
      getBuoyReportPath({ name: buoy.name, id: buoy.id, sourceID: buoy.sourceId }),
    );
  });

  it('should display an OFFLINE buoy', () => {
    const [buoy] = getNearbyBuoysFixture.data;
    const wrapper = shallow(<BuoyMarkerPopup buoy={{ ...buoy, status: 'OFFLINE' }} />);
    const title = wrapper.find('.buoyMarkerPopupContentTitle');
    const subtitle = wrapper.find('.buoyMarkerPopupContentSubtitleOffline');

    expect(title.text()).to.be.equal(buoy.name);

    expect(subtitle.text()).to.equal('Status: Offline');
  });
});
