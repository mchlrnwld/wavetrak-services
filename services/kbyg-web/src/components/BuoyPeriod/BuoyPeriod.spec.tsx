import React from 'react';

import { expect } from 'chai';
import { mount } from 'enzyme';

import BuoyPeriod from './BuoyPeriod';

describe('components / BuoyPeriod', () => {
  it('should render the buoy period', () => {
    const wrapper = mount(<BuoyPeriod period={10} />);
    const period = wrapper.find('.sl-buoy-period');

    expect(period.text()).to.equal('10s');
  });

  it('should render the a dash if the period is missing', () => {
    const wrapper = mount(<BuoyPeriod />);
    const period = wrapper.find('.sl-buoy-period');

    expect(period.text()).to.equal('- s');
  });
});
