import React from 'react';
import { isNumber } from 'lodash';

interface Props {
  period?: number;
}

const BuoyPeriod: React.FC<Props> = ({ period }) => (
  <span className="sl-buoy-period">{isNumber(period) ? period : '- '}s</span>
);

export default BuoyPeriod;
