import React from 'react';
import { Pane, ErrorBoundary } from '@surfline/quiver-react';

import BuoyMapMarker from '../BuoyMapMarker';
import BuoyClusterMarker from '../BuoyClusterMarker';
import useMap from '../../hooks/map/useMap';
import { NearbyStation } from '../../types/buoys';

import type { Cluster } from '../../utils/cluster/cluster';

import styles from './BuoyMapMarkers.module.scss';

interface Props {
  clusters?: Cluster<{ count: number }>[];
  buoys?: NearbyStation[];
}

const BuoyMapMarkers: React.FC<Props> = ({ buoys = [], clusters }) => {
  const [map] = useMap();

  return (
    <Pane
      name="sl-buoy-map-markers"
      className={styles.slBuoyMapMarkers}
      usingPortal
      map={map}
      style={{}}
    >
      {clusters && clusters?.length > 0
        ? clusters.map((cluster) => (
            <ErrorBoundary
              key={`${cluster.geometry.coordinates[0]}-${cluster.geometry.coordinates[1]}`}
              render={() => null}
            >
              <BuoyClusterMarker cluster={cluster} />
            </ErrorBoundary>
          ))
        : buoys.map((buoy) => (
            // Don't render anything if the marker throws
            <ErrorBoundary key={buoy.id} render={() => null}>
              <BuoyMapMarker buoy={buoy} />
            </ErrorBoundary>
          ))}
    </Pane>
  );
};

export default BuoyMapMarkers;
