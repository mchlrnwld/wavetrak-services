import React from 'react';

import { act } from 'react-dom/test-utils';
import { expect } from 'chai';
import { mount } from 'enzyme';
import sinon, { SinonStub } from 'sinon';
import { Map, Pane } from '@surfline/quiver-react';
import type { Map as LMap } from 'leaflet';

import { NearbyStation } from '../../types/buoys';
import * as useMap from '../../hooks/map/useMap';
import { getNearbyBuoysFixture } from '../../common/api/fixtures/buoys';
import { wait, mountWithReduxAndRouter } from '../../utils/test-utils';

import BuoyMapMarkers from './BuoyMapMarkers';
import BuoyMapMarker from '../BuoyMapMarker';
import BuoyClusterMarker from '../BuoyClusterMarker';
import { Cluster } from '../../utils/cluster/cluster';

const nearbyBuoysWithOffline = [
  getNearbyBuoysFixture.data[0],
  {
    ...getNearbyBuoysFixture.data[1],
    status: 'OFFLINE',
  },
];

const clusterFixture: Cluster<{ count: number }>[] = [
  { geometry: { type: 'Point', coordinates: [1, 2] }, type: 'Feature', properties: { count: 2 } },
  { geometry: { type: 'Point', coordinates: [3, 4] }, type: 'Feature', properties: { count: 10 } },
];

describe('components / BuoyMapMarkers', () => {
  let useMapStub: SinonStub;

  let map: LMap | undefined;

  const Comp = ({
    buoys,
    clusters,
  }: {
    clusters?: Cluster<{ count: number }>[];
    buoys?: NearbyStation[];
  }) => (
    <Map
      center={{ lat: 0, lon: 0 }}
      mapProps={{
        whenCreated: (m) => {
          useMapStub.returns([m]);
          map = m;
        },
      }}
    >
      <BuoyMapMarkers clusters={clusters} buoys={buoys} />
    </Map>
  );

  beforeEach(() => {
    useMapStub = sinon.stub(useMap, 'default').returns([undefined, () => {}]);
  });

  afterEach(() => {
    useMapStub.restore();
  });

  it('should render buoy markers inside a Pane', async () => {
    const wrapper = mount(<Comp buoys={nearbyBuoysWithOffline} />);
    try {
      await act(async () => {
        // Wait for the map to setup
        await wait(150);
        wrapper.setProps({ buoys: [...nearbyBuoysWithOffline] });
        wrapper.update();
      });

      expect(map).to.exist("The map instance must exist or else the markers can't render");

      const pane = wrapper.find(Pane);

      expect(pane).to.have.length(1);
      expect(pane.prop('name')).to.exist();
    } finally {
      await act(async () => {
        await wait(150);
        wrapper.unmount();
      });
    }
  });

  it('should pass the map instance to the Pane', async () => {
    const wrapper = mount(<Comp buoys={nearbyBuoysWithOffline} />);
    try {
      await act(async () => {
        // Wait for the map to setup
        await wait(150);
        wrapper.setProps({ buoys: [...nearbyBuoysWithOffline] });
        wrapper.update();
      });

      expect(map).to.exist("The map instance must exist or else the markers can't render");

      const pane = wrapper.find(Pane);

      expect(pane.prop('map')).to.deep.equal(map);
    } finally {
      await act(async () => {
        await wait(150);
        wrapper.unmount();
      });
    }
  });

  it('should render valid buoy markers', async () => {
    const [buoy1, buoy2] = nearbyBuoysWithOffline;
    const wrapper = mount(<Comp buoys={nearbyBuoysWithOffline} />);
    try {
      await act(async () => {
        // Wait for the map to setup
        // Enzyme is kind of weird and we need two renders to get everything to render
        await wait(150);
        wrapper.setProps({ clusters: [] });
        wrapper.update();
      });

      expect(map).to.exist("The map instance must exist or else the markers can't render");

      act(() => {
        wrapper.setProps({ clusters: undefined });
        wrapper.update();
      });

      const markers = wrapper.find(BuoyMapMarker);

      expect(markers).to.have.length(2);

      expect(markers.at(0).prop('buoy')).to.deep.equal(buoy1);
      expect(markers.at(1).prop('buoy')).to.deep.equal(buoy2);
    } finally {
      await act(async () => {
        await wait(150);
        wrapper.unmount();
      });
    }
  });

  // TODO: re-enable once we migrate to react-testing-library
  it.skip('should render others markers even if one errors', async () => {
    const wrapper = mount(<Comp buoys={nearbyBuoysWithOffline} />);
    try {
      await act(async () => {
        // Wait for the map to setup
        await wait(150);
        wrapper.setProps({ clusters: [] });
        wrapper.update();
      });

      expect(map).to.exist("The map instance must exist or else the markers can't render");

      act(() => {
        wrapper.setProps({ clusters: undefined });
        wrapper.update();
      });
      const markers = wrapper.find(BuoyMapMarker);

      expect(markers).to.have.length(2);

      act(() => {
        wrapper.setProps({ clusters: [] });
        wrapper.update();
        markers.at(0).simulateError('some error in the marker');
        wrapper.setProps({ clusters: undefined });
        wrapper.update();
      });

      const markers2 = wrapper.find(BuoyMapMarker);

      expect(markers2).to.have.length(1);
    } finally {
      await act(async () => {
        await wait(150);
        wrapper.unmount();
      });
    }
  });

  it('should render cluster markers if clusters exist', async () => {
    const { wrapper } = mountWithReduxAndRouter(Comp, { clusters: clusterFixture });
    await act(async () => {
      // Wait for the map to setup
      await wait(150);
      wrapper.setProps({ buoys: [] });
      wrapper.update();
    });
    try {
      expect(map).to.exist("The map instance must exist or else the markers can't render");

      act(() => {
        wrapper.setProps({ buoys: [...nearbyBuoysWithOffline] });
        wrapper.update();
      });

      const clusterMarkers = wrapper.find(BuoyClusterMarker);

      expect(clusterMarkers).to.have.length(2);
    } finally {
      await act(async () => {
        await wait(150);
        wrapper.unmount();
      });
    }
  });

  it('should render other cluster markers even if one errors', async () => {
    const { wrapper } = mountWithReduxAndRouter(Comp, { clusters: clusterFixture });

    await act(async () => {
      await wait(150);
      // Wait for the map to setup
      wrapper.setProps({ buoys: [] });
      wrapper.update();
    });

    try {
      expect(map).to.exist("The map instance must exist or else the markers can't render");

      act(() => {
        wrapper.setProps({ buoys: [...nearbyBuoysWithOffline] });
        wrapper.update();
      });

      const clusterMarkers = wrapper.find(BuoyClusterMarker);

      expect(clusterMarkers).to.have.length(2);

      act(() => {
        clusterMarkers.at(0).simulateError('some error in the marker');
        wrapper.setProps({ buoys: undefined });
        wrapper.update();
      });

      const markers2 = wrapper.find(BuoyClusterMarker);

      expect(markers2).to.have.length(1);
    } finally {
      await act(async () => {
        await wait(150);
        wrapper.unmount();
      });
    }
  });
});
