import React from 'react';
import BuoyMapMarkers from './BuoyMapMarkers';

BuoyMapMarkers.displayName = 'BuoyMapMarkers';

export default React.memo(BuoyMapMarkers);
