import React from 'react';
import { getLocalDate, pageStructuredData, placeStructuredData } from '@surfline/web-common';
import { getBuoyReportPath } from '../../utils/urls';
import MetaTags from '../MetaTags';
import config from '../../config';

const title = (name: string | undefined, sourceID: string) =>
  `${
    name ? `${name} ` : ''
  }Buoy (${sourceID}) Swell Height, Peak Period & Wave Direction Report - Surfline`;

const description = (name: string | undefined, sourceID: string) =>
  `Current ${
    name ? `${name} ` : ''
  }Buoy (${sourceID}) swell report with the latest in wave height, peak period and mean wave direction data. Along with weather reporting that includes wind, water temp and air temp at Surfline.`;

const urlPath = (name: string | undefined, id: string, sourceID: string) =>
  getBuoyReportPath({ name, id, sourceID });

interface Props {
  name?: string;
  id: string;
  sourceID?: string;
  latitude?: number;
  longitude?: number;
  latestTimestamp?: number;
  utcOffset?: number;
}

const BuoysPageMeta: React.FC<Props> = ({
  name,
  id,
  sourceID = '',
  latitude = 0,
  longitude = 0,
  latestTimestamp,
  utcOffset = 0,
}) => {
  const pageTitle = title(name, sourceID);
  const metaDescription = description(name, sourceID);

  const url = `${config.surflineHost}${urlPath(name, id, sourceID)}`;

  // Midnight of current day
  const currentDay = new Date();
  const startOfCurrentDayUTC = new Date(currentDay.setUTCHours(0, 0, 0, 0));

  const reportDate = latestTimestamp
    ? getLocalDate(latestTimestamp, utcOffset)
    : startOfCurrentDayUTC;

  return (
    <MetaTags title={pageTitle} description={metaDescription} urlPath={urlPath(name, id, sourceID)}>
      <script type="application/ld+json">
        {pageStructuredData({
          url,
          headline: pageTitle,
          description: metaDescription,
          dateModified: reportDate,
          publisher: 'Surfline',
        })}
      </script>
      <script type="application/ld+json">
        {placeStructuredData(name || '', latitude, longitude)}
      </script>
    </MetaTags>
  );
};

export default BuoysPageMeta;
