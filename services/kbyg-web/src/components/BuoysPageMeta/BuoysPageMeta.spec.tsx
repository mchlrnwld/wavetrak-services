import React from 'react';

import { expect } from 'chai';
import { shallow } from 'enzyme';
import { slugify } from '@surfline/web-common';
import MetaTags from '../MetaTags';
import BuoysPageMeta from './BuoysPageMeta';

describe('components / BuoysPageMeta', () => {
  const buoyName = 'WESTERN  HAWAII - 205 NM SW of Honolulu, HI';
  const sourceID = '51003';
  const id = '5e076428-ca15-11eb-8cf8-0242015eddd9';
  const latitude = 19.196;
  const longitude = -160.639;

  it('renders MetaTags with appropriate props', () => {
    const title = `${buoyName} Buoy (${sourceID}) Swell Height, Peak Period & Wave Direction Report - Surfline`;
    const description = `Current ${buoyName} Buoy (${sourceID}) swell report with the latest in wave height, peak period and mean wave direction data. Along with weather reporting that includes wind, water temp and air temp at Surfline.`;
    const urlPath = `/buoy-report/${slugify(buoyName)}-buoy-${sourceID}/${id}`;
    const latestTimestamp = 1627332600;

    const wrapper = shallow(
      <BuoysPageMeta
        name={buoyName}
        id={id}
        sourceID={sourceID}
        latitude={latitude}
        longitude={longitude}
        latestTimestamp={latestTimestamp}
      />,
    );
    const metaTags = wrapper.find(MetaTags);

    expect(metaTags).to.have.length(1);
    expect(metaTags.prop('title')).to.equal(title);
    expect(metaTags.prop('description')).to.equal(description);
    expect(metaTags.prop('urlPath')).to.equal(urlPath);
  });
});
