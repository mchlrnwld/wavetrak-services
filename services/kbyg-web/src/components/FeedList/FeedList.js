import PropTypes from 'prop-types';
import React from 'react';
import FeedArticle from '../FeedArticle';
import articlePropType from '../../propTypes/article';
import subregionAdConfigPropType from '../../propTypes/subregionAdConfig';

const FeedList = ({
  articles,
  isPremium,
  subregionAdConfig,
  subregionName,
  subregionId,
  showFullArticle,
}) => {
  const getAdIdentifier = (index) => {
    if (index === 2) return 'forecastFeedMobile';
    if (index === 10) return 'forecastFeedMobileBottom';
    return null;
  };

  return (
    <div className="sl-feed-list">
      {articles.map((article, index) => (
        <FeedArticle
          key={`feedArticle_${article.id}`}
          article={article}
          adIdentifier={getAdIdentifier(index)}
          subregionAdConfig={subregionAdConfig}
          isPremium={isPremium}
          subregionName={subregionName}
          subregionId={subregionId}
          showFullArticle={showFullArticle}
        />
      ))}
    </div>
  );
};

FeedList.propTypes = {
  articles: PropTypes.arrayOf(articlePropType).isRequired,
  isPremium: PropTypes.bool.isRequired,
  subregionAdConfig: subregionAdConfigPropType.isRequired,
  subregionName: PropTypes.string.isRequired,
  subregionId: PropTypes.string.isRequired,
  showFullArticle: PropTypes.bool.isRequired,
};

export default FeedList;
