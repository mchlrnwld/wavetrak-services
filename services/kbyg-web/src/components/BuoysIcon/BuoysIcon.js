import React from 'react';
import { color } from '@surfline/quiver-themes';

const BuoysIcon = () => (
  <svg className="sl-buoys-icon" width="26" height="26" viewBox="0 0 26 26">
    <circle cx="13" cy="13" r="13" fill="#96A9B2" />
    <circle fill={color('white')} cx="13" cy="6" r="2" />
    <path
      d="M 8 9.006
         C 8 7.898 8.887 7 10 7
         h 6
         c 1.105 0 2 0.897 2 2.006
         V 21
         H 8
         V 9.006
         z
         m 2 0.996
         V 21
         h 6
         V 10.002
         C 16 9.456 15.556 9 15.01 9
         h -4.02
         c -0.539 0 -0.99 0.449 -0.99 1.002
         z"
      fill={color('white')}
    />
    <path fill={color('white')} d="M 10 13 h 6 v 2 h -6 z" />
    <rect fill={color('white')} x="6" y="19" width="14" height="2" rx="0.5" />
  </svg>
);

export default BuoysIcon;
