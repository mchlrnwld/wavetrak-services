import PropTypes from 'prop-types';
import React from 'react';
import { Chevron } from '@surfline/quiver-react';

const TIME_TYPES = ['16day', 'hourly'];

const TIME_TYPE_LABEL = {
  '16day': '16-Day',
  hourly: 'Hourly',
};

const ForecastTimeViewDropdown = ({ timeView, onClick }) => (
  <div className="sl-forecast-time-view-dropdown">
    <div className="sl-forecast-time-view-dropdown__title">
      {TIME_TYPE_LABEL[timeView]}
      <Chevron direction="down" />
    </div>
    <div className="sl-forecast-time-view-dropdown__options">
      {TIME_TYPES.map((timeType) => (
        <button
          type="button"
          key={timeType}
          className="sl-forecast-time-view-dropdown__options__option"
          onClick={
            timeView === timeType ? null : () => onClick(timeType, TIME_TYPE_LABEL[timeType])
          }
        >
          <div className="sl-forecast-time-view-dropdown__options__option__details">
            <span className="sl-forecast-time-view-dropdown__options__option__details__name">
              {TIME_TYPE_LABEL[timeType]}
            </span>
          </div>
        </button>
      ))}
    </div>
  </div>
);

ForecastTimeViewDropdown.propTypes = {
  timeView: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

ForecastTimeViewDropdown.defaultProps = {};

export default ForecastTimeViewDropdown;
