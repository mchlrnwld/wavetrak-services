import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ForecastTimeViewDropdown from './ForecastTimeViewDropdown';

describe('components / ForecastTimeViewDropdown', () => {
  it('has options', () => {
    const wrapper = shallow(<ForecastTimeViewDropdown timeView="hourly" />);
    const stateOptionsWrapper = wrapper.find('.sl-forecast-time-view-dropdown__options__option');
    expect(stateOptionsWrapper).to.have.length(2);
  });
});
