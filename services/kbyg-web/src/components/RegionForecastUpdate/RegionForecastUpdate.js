import PropTypes from 'prop-types';
import React, { useRef, useMemo } from 'react';
import { slugify } from '@surfline/web-common';
import {
  ForecasterProfile,
  PremiumAnalysisLink,
  TrackableLink,
  RegionalForecastUpdateCTA,
} from '@surfline/quiver-react';
import Bets from '../Bets';
import ForecastHighlights from '../ForecastHighlights';
import config from '../../config';
import en from '../../intl/translations/en';
import premiumAnalysisClickEventPropType from '../../propTypes/premiumAnalysisClickEvent';
import { postedDate, nextUpdateDate } from '../../utils/formatReportDate';
import { CLICKED_SUBSCRIBE_CTA } from '../../common/constants';

const RegionForecastUpdate = ({
  forecastSummary: { forecaster, hasHighlights, highlights, nextForecast, bestBets, hasBets, bets },
  subregionId,
  subregionName,
  timestamp,
  utcOffset,
  abbrTimezone,
  condition,
  clickEventProps,
  isPremium,
  dateFormat,
  showPaywall,
  isAnonymous,
}) => {
  const linkRef = useRef();
  const eventProperties = useMemo(
    () => ({
      location: 'go premium - forecast analysis',
      subregionId,
      pageName: 'Regional Forecast',
      category: 'regional forecast',
    }),
    [subregionId],
  );
  const hasBestBets = bestBets && bestBets.length > 0;
  return (
    <div className="sl-region-forecast-update">
      {forecaster ? (
        <ForecasterProfile
          forecasterName={forecaster.name}
          forecasterTitle={forecaster.title}
          url={forecaster.iconUrl}
          lastUpdate={postedDate(timestamp, utcOffset, abbrTimezone, false, dateFormat)}
          nextUpdate={nextUpdateDate(
            nextForecast.timestamp,
            nextForecast.utcOffset,
            abbrTimezone,
            dateFormat,
          )}
          condition={condition || 'POOR'}
          expired={!condition}
          baseUrl={config.surflineHost}
        />
      ) : null}
      {(() => {
        if (showPaywall)
          return (
            <RegionalForecastUpdateCTA
              isAnonymous={isAnonymous}
              segmentProperties={eventProperties}
            />
          );
        if (isPremium)
          return (
            <div className="sl-region-forecast-update__body">
              {hasBestBets || hasBets || hasHighlights ? (
                <div className="sl-region-forecast-update__divider" />
              ) : null}
              {hasBestBets || hasBets ? (
                <Bets bestBets={bestBets} hasBestBets={hasBestBets} bets={bets} hasBets={hasBets} />
              ) : null}
              {hasHighlights ? <ForecastHighlights highlights={highlights} /> : null}
              <PremiumAnalysisLink
                link={en.RegionalForecast.pageLevelLinkText.premiumAnalysis.path(
                  slugify(subregionName),
                  subregionId,
                )}
                clickEventProps={clickEventProps}
              />
            </div>
          );
        return (
          <div className="sl-region-forecast-update__cta">
            <div className="sl-region-forecast-update__cta__text">
              {en.premiumCTA.RegionForecastUpdateCTA.defaultText}
            </div>
            <TrackableLink
              ref={linkRef}
              eventName={CLICKED_SUBSCRIBE_CTA}
              eventProperties={eventProperties}
            >
              <a
                ref={linkRef}
                className="sl-region-forecast-update__cta__go-premium"
                href={config.funnelUrl}
              >
                {en.premiumCTA.RegionForecastUpdateCTA.linkText}
              </a>
            </TrackableLink>
          </div>
        );
      })()}
    </div>
  );
};

RegionForecastUpdate.propTypes = {
  forecastSummary: PropTypes.shape({
    forecaster: PropTypes.shape({
      name: PropTypes.string,
      title: PropTypes.string,
      email: PropTypes.string,
      iconUrl: PropTypes.string,
    }),
    hasHighlights: PropTypes.bool.isRequired,
    highlights: PropTypes.arrayOf(PropTypes.string),
    bestBets: PropTypes.arrayOf(PropTypes.string),
    nextForecast: PropTypes.shape({
      timestamp: PropTypes.number,
      utcOffset: PropTypes.number,
    }),
    hasBets: PropTypes.bool.isRequired,
    bets: PropTypes.shape({
      best: PropTypes.string,
      worst: PropTypes.string,
    }).isRequired,
  }).isRequired,
  subregionId: PropTypes.string.isRequired,
  subregionName: PropTypes.string.isRequired,
  timestamp: PropTypes.number.isRequired,
  condition: PropTypes.string,
  isPremium: PropTypes.bool.isRequired,
  utcOffset: PropTypes.number.isRequired,
  abbrTimezone: PropTypes.string.isRequired,
  clickEventProps: premiumAnalysisClickEventPropType.isRequired,
  dateFormat: PropTypes.oneOf(['MDY', 'DMY']),
};

RegionForecastUpdate.defaultProps = {
  dateFormat: 'MDY',
  condition: '',
};

export default RegionForecastUpdate;
