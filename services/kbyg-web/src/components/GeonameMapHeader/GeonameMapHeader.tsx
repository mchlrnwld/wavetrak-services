import React from 'react';
import classNames from 'classnames';

import Breadcrumb from '../Breadcrumb';
import { mapPaths } from '../../utils/urls/mapPaths';
import { LocationView } from '../../types/locationView';

interface Props {
  isBuoys?: boolean;
  locationView: LocationView;
  classes?: { root?: string; content?: string };
}

const getClass = (rootClass?: string) => classNames('sl-geoname-map-header', rootClass);
const getContentClass = (contentClass?: string) =>
  classNames('sl-geoname-map-header__header', contentClass);

const GeonameMapHeader: React.FC<Props> = ({ locationView, isBuoys, classes = {} }) => {
  const name = locationView?.taxonomy?.name || '';
  const description = isBuoys ? 'Buoys' : 'Surf Reports & Cams';
  const baseUrl = isBuoys ? mapPaths.geonameBuoysBase : mapPaths.geonameBase;

  return (
    <div className={getClass(classes.root)}>
      <Breadcrumb breadcrumbs={locationView.breadCrumbs} baseUrl={baseUrl} />
      <h1 className={getContentClass(classes.content)}>
        {name} {description}
      </h1>
    </div>
  );
};

export default GeonameMapHeader;
