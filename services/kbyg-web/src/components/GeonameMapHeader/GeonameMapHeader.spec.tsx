import { expect } from 'chai';

import GeonameMapHeader from './GeonameMapHeader';
import Breadcrumb from '../Breadcrumb';

import stateFixture from '../../selectors/fixtures/state';
import { mapPaths } from '../../utils/urls/mapPaths';
import { mountWithReduxAndRouter } from '../../utils/test-utils';

describe('components / GeonameMapHeader', () => {
  const { locationView } = stateFixture.mapV2;

  it('should render the surf reports geoname map header', () => {
    const { wrapper } = mountWithReduxAndRouter(GeonameMapHeader, { locationView });

    const breadcrumb = wrapper.find(Breadcrumb);
    const header = wrapper.find('.sl-geoname-map-header__header');

    expect(breadcrumb.prop('breadcrumbs')).to.deep.equal(locationView.breadCrumbs);
    expect(breadcrumb.prop('baseUrl')).to.deep.equal(mapPaths.geonameBase);

    expect(header.text()).to.be.equal('Santa Cruz Surf Reports & Cams');
  });

  it('should render the buoys geoname map header', () => {
    const { wrapper } = mountWithReduxAndRouter(GeonameMapHeader, { locationView, isBuoys: true });

    const breadcrumb = wrapper.find(Breadcrumb);
    const header = wrapper.find('.sl-geoname-map-header__header');

    expect(breadcrumb.prop('breadcrumbs')).to.deep.equal(locationView.breadCrumbs);
    expect(breadcrumb.prop('baseUrl')).to.deep.equal(mapPaths.geonameBuoysBase);

    expect(header.text()).to.be.equal('Santa Cruz Buoys');
  });
});
