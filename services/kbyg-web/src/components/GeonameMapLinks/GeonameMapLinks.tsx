import React from 'react';
import classNames from 'classnames';

import MapLink from '../MapLink';
import { getBuoyGeonameMapPath, getGeonameMapPath } from '../../utils/urls/mapPaths';
import useBreadcrumbsAndGeonameId from '../../hooks/useBreadcrumbsAndGeonameId';

import styles from './GeonameMapLinks.module.scss';

const getClass = () => classNames('sl-geoname-map-links', styles.geonameMapLinks);

const GeonameMapLinks: React.FC = () => {
  const { geonameId, breadcrumbs } = useBreadcrumbsAndGeonameId();

  const breadcrumbsUrl = breadcrumbs.join('/');

  return (
    <nav className={getClass()}>
      <MapLink href={getGeonameMapPath({ breadcrumbs: breadcrumbsUrl, geonameId })}>
        Surf Spots
      </MapLink>
      <MapLink href={getBuoyGeonameMapPath({ breadcrumbs: breadcrumbsUrl, geonameId })}>
        Wave Buoys
      </MapLink>
    </nav>
  );
};

export default GeonameMapLinks;
