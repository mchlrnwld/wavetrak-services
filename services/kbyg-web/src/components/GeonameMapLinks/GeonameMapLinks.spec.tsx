import { expect } from 'chai';
import { mountWithReduxAndRouter } from '../../utils/test-utils';

import GeonameMapLinks from './GeonameMapLinks';

describe('components / GeonameMapLinks', () => {
  const router = {
    pathname: '/surf-reports-forecasts-cams/buoys/[...geonames]',
    asPath: '/surf-reports-forecasts-cams/buoys/united-states/california/santa-cruz/1234',
    query: { geonames: ['united-states', 'california', 'santa-cruz', '1234'] },
  };

  // TODO: Enable this test once we migrate the old map pages to map v2
  it.skip('should be active for a geoname map path', () => {
    const { wrapper } = mountWithReduxAndRouter(GeonameMapLinks, {}, { router });

    const activeLink = wrapper.find('a.slMapLinkActive');

    expect(activeLink).to.have.length(1);
    expect(activeLink.text()).to.be.equal('Surf Spots');
  });

  it('should be active for a buoy geoname map path', () => {
    const { wrapper } = mountWithReduxAndRouter(GeonameMapLinks, {}, { router });

    const activeLink = wrapper.find('a.slMapLinkActive');

    expect(activeLink).to.have.length(1);
    expect(activeLink.text()).to.be.equal('Wave Buoys');
  });
});
