import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { format } from 'date-fns';

const swellTrendSliderDayClassName = (obscured) =>
  classNames({
    'sl-swell-trend-slider-day': true,
    'sl-swell-trend-slider-day--darkened': obscured,
  });

const SwellTrendSliderDay = ({ localDate, obscured, count, dateFormat }) => {
  const displayDate = dateFormat === 'MDY' ? 'M/d' : 'd/M';
  return (
    <div className={swellTrendSliderDayClassName(obscured)}>
      <div className="sl-swell-trend-slider-day__day">
        {format(localDate, count < 7 ? 'iii' : displayDate)}
      </div>
    </div>
  );
};

SwellTrendSliderDay.propTypes = {
  localDate: PropTypes.instanceOf(Date).isRequired,
  obscured: PropTypes.bool,
  count: PropTypes.number.isRequired,
  dateFormat: PropTypes.oneOf(['MDY', 'DMY']),
};

SwellTrendSliderDay.defaultProps = {
  obscured: false,
  dateFormat: 'MDY',
};

export default SwellTrendSliderDay;
