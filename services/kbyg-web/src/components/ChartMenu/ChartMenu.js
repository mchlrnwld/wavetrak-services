/* eslint-disable jsx-a11y/click-events-have-key-events */

import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { CloseIcon, Scrollbar, TrackableLink } from '@surfline/quiver-react';
import { slugify } from '@surfline/web-common';
import classNames from 'classnames';
import ChevronBox from '../ChevronBox';
import { chartSegmentPropsPropTypes, chartsPropTypes } from '../../propTypes/charts';
import getChartSubtitle from '../../utils/getChartSubtitle';
import WavetrakLink from '../WavetrakLink';

const getChartMenuClasses = (mobileMenuOpen) =>
  classNames({
    'sl-chart-menu': true,
    'sl-chart-menu--open': mobileMenuOpen,
  });

const getChartListItemClasses = (active) =>
  classNames({
    'sl-chart-menu__list__item': true,
    'sl-chart-menu__list__item--active': active,
  });

const getChartMenuBlockClasses = (active) =>
  classNames({
    'sl-chart-menu__block': true,
    'sl-chart-menu__block--active': active,
  });

const getChartUrl = (chart, category, subregionId, subregionName, regionName) => {
  const locationName = category === 'region' ? slugify(regionName) : slugify(subregionName);
  return `/surf-charts/${chart.slug}/${locationName}/${subregionId}`;
};

class ChartMenu extends Component {
  constructor(props) {
    super(props);
    this.chartCategories = Object.keys(props.charts.types);
    this.state = {
      openChartsBlockIndex: this.chartCategories.indexOf(props.charts.active.category) || 0,
    };
    this.chartsLinkRef = React.createRef();
  }

  getChartsLinkProperties = () => {
    const { chartSegmentProps } = this.props;
    return {
      linkUrl: '/surf-charts/',
      linkName: 'All Charts',
      linkLocation: 'Chart Menu',
      category: chartSegmentProps.category,
      pageName: chartSegmentProps.pageName,
    };
  };

  listItems = () => {
    const { charts, subregionName, subregionId, onMenuClick, mobileMenuVisible } = this.props;
    return (
      <div className="sl-chart-menu-container">
        {this.chartCategories.map((category, index) => {
          const { openChartsBlockIndex } = this.state;
          const active = !mobileMenuVisible && openChartsBlockIndex === index;
          return charts.types[category].length ? (
            <div key={category} className={getChartMenuBlockClasses(active)}>
              <button
                type="button"
                onClick={() => this.setState({ openChartsBlockIndex: index })}
                className="sl-chart-menu__block__title"
              >
                {mobileMenuVisible ? null : <ChevronBox direction={active ? 'down' : 'right'} />}
                <h5>{getChartSubtitle(category, charts.region, subregionName)}</h5>
              </button>
              {openChartsBlockIndex === index || mobileMenuVisible ? (
                <ul className="sl-chart-menu__list">
                  {charts.types[category].map((chart) => {
                    const chartUrl = getChartUrl(
                      chart,
                      category,
                      subregionId,
                      subregionName,
                      charts.region,
                    );
                    return (
                      <li
                        key={`${chart.type} ${chart.name}`}
                        className={getChartListItemClasses(chart.slug === charts.active.slug)}
                      >
                        <WavetrakLink
                          href={chartUrl}
                          onClick={() => onMenuClick(chart.slug, subregionId)}
                        >
                          {chart.displayName ? chart.displayName : chart.name}
                        </WavetrakLink>
                      </li>
                    );
                  })}
                </ul>
              ) : null}
            </div>
          ) : null;
        })}
        <div className="sl-chart-menu__link">
          <TrackableLink
            ref={this.chartsLinkRef}
            eventName="Clicked Link"
            eventProperties={this.getChartsLinkProperties}
          >
            <a ref={this.chartsLinkRef} href="/surf-charts/">
              All Charts
            </a>
          </TrackableLink>
        </div>
      </div>
    );
  };

  scrollableMenu = () => {
    const { chartMenuHeight } = this.props;
    return chartMenuHeight > 0 ? (
      <Scrollbar
        speed={25}
        style={{
          height: chartMenuHeight,
        }}
      >
        {this.listItems()}
      </Scrollbar>
    ) : null;
  };

  render() {
    const { mobileMenuOpen, toggleMobileMenu, mobileMenuVisible } = this.props;
    return (
      <div
        className={getChartMenuClasses(mobileMenuOpen)}
        ref={(el) => {
          // eslint-disable-next-line react/no-unused-class-component-methods
          this.chartMenuRef = el;
        }}
      >
        <div className="sl-chart-menu__title">
          <span>Charts</span>
          <div onClick={toggleMobileMenu}>
            <CloseIcon />
          </div>
        </div>
        {mobileMenuVisible ? this.listItems() : this.scrollableMenu()}
      </div>
    );
  }
}

ChartMenu.propTypes = {
  charts: chartsPropTypes.isRequired,
  mobileMenuOpen: PropTypes.bool.isRequired,
  subregionName: PropTypes.string.isRequired,
  subregionId: PropTypes.string.isRequired,
  onMenuClick: PropTypes.func.isRequired,
  toggleMobileMenu: PropTypes.func.isRequired,
  chartSegmentProps: chartSegmentPropsPropTypes.isRequired,
  chartMenuHeight: PropTypes.number.isRequired,
  mobileMenuVisible: PropTypes.bool.isRequired,
};

export default ChartMenu;
