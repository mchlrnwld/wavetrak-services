import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import swellHeightClassesFromUnits from '../../common/swellHeightClassesFromUnits';

const feetClassName = (heightClass) =>
  classNames({
    'sl-swell-height-legend__class': true,
    [`sl-swell-height-legend__class--${heightClass}-ft`]: true,
    'sl-swell-height-legend__class--single': heightClass < 12,
    'sl-swell-height-legend__class--double': heightClass >= 12 && heightClass < 23,
    'sl-swell-height-legend__class--triple': heightClass >= 23,
  });

const metersClassName = (heightClass) =>
  classNames({
    'sl-swell-height-legend__class': true,
    [`sl-swell-height-legend__class--${heightClass.toString().replace('.', '_')}-m`]: true,
    'sl-swell-height-legend__class--single': heightClass < 4,
    'sl-swell-height-legend__class--double': heightClass >= 4 && heightClass < 7,
    'sl-swell-height-legend__class--triple': heightClass >= 7,
  });

const feetClassLabels = new Set([2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 23, 26, 29, 32, 35]);

const metersClassLabels = new Set([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]);

const heightClassFunctions = (units) => {
  switch (units) {
    case 'M':
      return {
        heightClassName: metersClassName,
        heightClassLabels: metersClassLabels,
      };
    case 'FT':
    default:
      return {
        heightClassName: feetClassName,
        heightClassLabels: feetClassLabels,
      };
  }
};

const SwellHeightLegend = ({ units }) => {
  const { heightClassName, heightClassLabels } = heightClassFunctions(units);
  return (
    <div className="sl-swell-height-legend">
      {swellHeightClassesFromUnits(units).map((heightClass) => (
        <div key={heightClass} className={heightClassName(heightClass)}>
          {heightClassLabels.has(heightClass) ? (
            <div className="sl-swell-height-legend__label">{heightClass}</div>
          ) : null}
        </div>
      ))}
    </div>
  );
};

SwellHeightLegend.propTypes = {
  units: PropTypes.oneOf(['FT', 'M']).isRequired,
};

export default SwellHeightLegend;
