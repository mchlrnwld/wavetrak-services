import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import deepFreeze from 'deep-freeze';
import SubregionHeaderLink from './SubregionHeaderLink';
import WavetrakLink from '../WavetrakLink';

describe('components / SubregionHeaderLink', () => {
  const props = deepFreeze({
    subregion: {
      id: '58581a836630e24c44878fd6',
      name: 'North Orange County',
      forecastStatus: {
        status: 'active',
        inactiveMessage: '',
      },
    },
  });

  it('renders a subregion header link to the subregion forecast page', () => {
    const wrapper = shallow(<SubregionHeaderLink {...props} />);
    const wrapperLink = wrapper.find(WavetrakLink);
    expect(wrapperLink.prop('href')).to.equal(
      `/surf-forecasts/north-orange-county/${props.subregion.id}`,
    );
  });
});
