import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { slugify, kbygPaths } from '@surfline/web-common';

import WavetrakLink from '../WavetrakLink';

import styles from './SubregionHeaderLink.module.scss';

const { subregionForecastPath } = kbygPaths;

const getClassName = (map, rootClass) =>
  classNames({
    [styles.subregionHeader]: true,
    [styles.subregionHeaderMap]: map,
    [rootClass]: !!rootClass,
  });

const SubregionHeaderLink = ({ children, subregion, map, classes }) => {
  const { name, id } = subregion;
  return (
    <WavetrakLink
      className={getClassName(map, classes.root)}
      href={subregionForecastPath(slugify(name), id)}
    >
      {children}
    </WavetrakLink>
  );
};

SubregionHeaderLink.propTypes = {
  classes: PropTypes.shape({
    root: PropTypes.string,
  }),
  children: PropTypes.node,
  subregion: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    forecastStatus: PropTypes.shape({
      status: PropTypes.string,
      inactiveMessage: PropTypes.string,
    }),
  }).isRequired,
  map: PropTypes.bool,
};

SubregionHeaderLink.defaultProps = {
  children: null,
  map: false,
  classes: {},
};

export default SubregionHeaderLink;
