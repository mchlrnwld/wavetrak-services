import React from 'react';

const LongArrowIcon = () => (
  <svg width="64px" height="30px" viewBox="0 0 64 30">
    <g id="Desktop" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="charts-desktop-paywall-active" transform="translate(-640.000000, -845.000000)">
        <rect x="0" y="0" width="1440" height="1896" />
        <g
          id="Paywall"
          transform="translate(431.000000, 415.000000)"
          stroke="#FFFFFF"
          strokeWidth="2"
        >
          <g id="Group-4" transform="translate(67.000000, 289.000000)">
            <g id="Group-2" transform="translate(142.000000, 141.000000)">
              <polyline
                id="Rectangle-8"
                transform="translate(51.000000, 15.000000) rotate(-45.000000) translate(-51.000000, -15.000000) "
                points="61 5 59.2185557 23.2185557 41 25"
              />
              <path d="M62,15 L0,15" id="Path-4" />
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default LongArrowIcon;
