import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { TrackableLink } from '@surfline/quiver-react';
import en from '../../intl/translations/en';
import config from '../../config';
import { CLICKED_SUBSCRIBE_CTA } from '../../common/constants';

const SpotReportCamHeaderCTA = ({ eventProperties }) => {
  const linkRef = useRef();
  return (
    <div className="sl-spot-report-cam-header-cta">
      <span className="sl-spot-report-cam-header-cta__text">
        {en.premiumCTA.SpotReportCamHeaderCTA.text}
      </span>
      <TrackableLink
        ref={linkRef}
        eventName={CLICKED_SUBSCRIBE_CTA}
        eventProperties={eventProperties}
      >
        <a ref={linkRef} href={config.funnelUrl} className="sl-spot-report-cam-header-cta__link">
          {en.premiumCTA.SpotReportCamHeaderCTA.linkText}
        </a>
      </TrackableLink>
    </div>
  );
};

SpotReportCamHeaderCTA.propTypes = {
  eventProperties: PropTypes.oneOfType([PropTypes.func, PropTypes.shape()]).isRequired,
};

export default SpotReportCamHeaderCTA;
