import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import HighlightList from './HighlightList';

describe('components / HighlightList', () => {
  it('should render a li for each highlight', () => {
    const overviewData = {
      forecastSummary: {
        highlights: ['Highlight one', 'Highlight two', 'Highlight three'],
        forecaster: {
          name: 'Surfline Admin',
        },
      },
      name: 'North Orange County',
      timestamp: 1572565372,
    };
    const wrapper = shallow(<HighlightList overviewData={overviewData} utcOffset={-8} />);
    expect(wrapper.find('li')).to.have.length(3);
  });
});
