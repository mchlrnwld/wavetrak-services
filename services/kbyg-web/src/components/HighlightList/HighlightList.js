/* eslint-disable react/no-array-index-key */
import PropTypes from 'prop-types';

import React from 'react';
import { format } from 'date-fns';
import { slugify, getLocalDate } from '@surfline/web-common';
import { PremiumAnalysisLink } from '@surfline/quiver-react';
import premiumAnalysisClickEventPropType from '../../propTypes/premiumAnalysisClickEvent';
import overviewDataPropShape from '../../propTypes/subregionOverviewData';
import en from '../../intl/translations/en';

const getFirstName = (name) => {
  const nameArr = name.split(' ');
  return nameArr[0];
};

const getTimeOfDay = (timestamp, utcOffset) => {
  const lastUpdatedTime = format(getLocalDate(timestamp, utcOffset), "h aaaaa'm'");
  let timeOfDay = 'morning';
  if (lastUpdatedTime.indexOf('pm') > -1) {
    const timeOfDayInt = parseInt(lastUpdatedTime, 10);
    timeOfDay =
      timeOfDayInt === 12 || (timeOfDayInt >= 1 && timeOfDayInt <= 4) ? 'afternoon' : 'evening';
  }
  return timeOfDay;
};

const HighlightList = ({ overviewData, utcOffset, clickEventProps }) => (
  <div className="sl-highlight-list">
    <div className="sl-highlight-list__message">
      {`Hello, this is ${getFirstName(overviewData.forecastSummary.forecaster.name)}
      with your ${overviewData.name} forecast effective
      ${format(getLocalDate(overviewData.timestamp, utcOffset), 'iiii')}
      ${getTimeOfDay(overviewData.timestamp, utcOffset)}.`}
    </div>
    <ul className="sl-highlight-list__list">
      {overviewData.forecastSummary.highlights.map(
        (highlight, key) =>
          highlight && (
            <li key={key} className="sl-highlight-list__list__item">
              <span className="sl-highlight-list__list__item__text">{highlight}</span>
            </li>
          ),
      )}
    </ul>
    <PremiumAnalysisLink
      link={en.RegionalForecast.pageLevelLinkText.premiumAnalysis.path(
        slugify(overviewData.name),
        overviewData._id,
      )}
      clickEventProps={clickEventProps}
    />
  </div>
);

HighlightList.propTypes = {
  overviewData: overviewDataPropShape.isRequired,
  utcOffset: PropTypes.number.isRequired,
  clickEventProps: premiumAnalysisClickEventPropType.isRequired,
};

export default HighlightList;
