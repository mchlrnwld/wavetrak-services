import React from 'react';
import { SpotDescriptor } from '@surfline/quiver-react';
import { travelDetailsPropType } from '../../propTypes/travelDetails';
import TravelGuideRatingAbilityLevel from '../TravelGuideRatingAbilityLevel';

const TravelGuideRatings = ({
  abilityLevels,
  localVibe,
  crowdFactor,
  spotRating,
  shoulderBurn,
  waterQuality,
}) => (
  <>
    <TravelGuideRatingAbilityLevel abilityLevels={abilityLevels} />
    <SpotDescriptor
      kind="Local Vibe"
      value={localVibe.summary}
      scale={localVibe.rating}
      options={['Welcoming', 'Intimidating']}
      description={localVibe.description}
    />
    <SpotDescriptor
      kind="Crowd Factor"
      value={crowdFactor.summary}
      scale={crowdFactor.rating}
      options={['Mellow', 'Heavy']}
      description={crowdFactor.description}
    />
    <SpotDescriptor
      kind="Spot Rating"
      value={spotRating.summary}
      scale={spotRating.rating}
      options={['Poor', 'Perfect']}
      description={spotRating.description}
    />
    <SpotDescriptor
      kind="Shoulder Burn"
      value={shoulderBurn.summary}
      scale={shoulderBurn.rating}
      options={['Light', 'Exhausting']}
      description={shoulderBurn.description}
    />
    <SpotDescriptor
      kind="Water Quality"
      value={waterQuality.summary}
      scale={waterQuality.rating}
      options={['Clean', 'Dirty']}
      description={waterQuality.description}
    />
  </>
);

TravelGuideRatings.propTypes = travelDetailsPropType;

export default TravelGuideRatings;
