import React, { useMemo, useRef, useState, useCallback } from 'react';
import { v1 as uuid } from 'uuid';

import BuoyCard from '../BuoyCard';
import BuoyCarouselArrows from '../BuoyCarouselArrows';

import BuoyCardPlaceholders from '../BuoyCardPlaceholders';
import { NearbyStation } from '../../types/buoys';

import styles from './BuoyCarouselCards.module.scss';

const DEFAULT_NUM_PLACEHOLDERS = 12;

interface Props {
  loading?: boolean;
  stations?: NearbyStation[];
  numPlaceholders?: number;
  trackLocation?: string;
}

const BuoyCarouselCards: React.FC<Props> = ({
  loading,
  stations,
  trackLocation,
  numPlaceholders = DEFAULT_NUM_PLACEHOLDERS,
}) => {
  const containerRef = useRef<HTMLUListElement>(null);
  const [leftDisabled, setLeftDisabled] = useState(true);
  const [rightDisabled, setRightDisabled] = useState(false);

  const placeholders = useMemo(
    () =>
      [...Array(numPlaceholders)].map(() => ({
        id: uuid(),
        isPlaceholder: true,
      })),
    [numPlaceholders],
  );

  const scrollCallback = useCallback(
    /** @param {UIEvent} event */
    (event) => {
      const carouselRef = event.target;
      const disableLeft = carouselRef.scrollLeft === 0;
      const disableRight =
        carouselRef.scrollWidth - carouselRef.scrollLeft <= carouselRef.clientWidth;

      setLeftDisabled(disableLeft);
      setRightDisabled(disableRight);
    },
    [],
  );

  const stationsToRender = loading ? placeholders : stations;

  return (
    <div className={styles.buoyCarouselCardsContainer}>
      <BuoyCarouselArrows
        carouselRef={containerRef.current}
        leftDisabled={leftDisabled}
        rightDisabled={rightDisabled}
      />
      {loading ? (
        <BuoyCardPlaceholders
          classes={{ root: styles.placeholders, placeholder: styles.placeholder }}
          numPlaceholders={8}
          loading={loading}
          largeOnly
        />
      ) : (
        <ul ref={containerRef} className={styles.buoyCarouselCards} onScroll={scrollCallback}>
          {stationsToRender?.map((station) => (
            <li key={station.id} className={styles.buoyCarouselCard}>
              <BuoyCard
                withTitles
                station={station as NearbyStation}
                largeOnly
                trackLocation={trackLocation}
              />
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default BuoyCarouselCards;
