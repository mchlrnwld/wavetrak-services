import React, { UIEvent } from 'react';

import { expect } from 'chai';
import { mount } from 'enzyme';

import { act } from 'react-dom/test-utils';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import BuoyCarouselCards from './BuoyCarouselCards';
import BuoyCard from '../BuoyCard';
import { getNearbyBuoysFixture } from '../../common/api/fixtures/buoys';
import BuoyCarouselArrows from '../BuoyCarouselArrows';
import BuoyCardPlaceholders from '../BuoyCardPlaceholders';

describe('components / BuoyCarouselCards', () => {
  it('should render the placeholders when loading', () => {
    const wrapper = mount(<BuoyCarouselCards loading />);

    const placeholders = wrapper.find(BuoyCardPlaceholders);

    expect(placeholders.prop('numPlaceholders')).to.be.equal(8);
  });

  it('should render the Buoy Cards when data is defined', () => {
    const stations = getNearbyBuoysFixture.data;
    const { wrapper } = mountWithReduxAndRouter(BuoyCarouselCards, { stations });

    const cards = wrapper.find(BuoyCard);

    expect(cards).to.have.length(stations.length);

    expect(cards.at(0).prop('station')).to.deep.equal(stations[0]);
    expect(cards.at(0).prop('largeOnly')).to.be.true();
    expect(cards.at(0).prop('withTitles')).to.be.true();
  });

  it('should set leftDisabled if the list is scolled to the beginning', () => {
    const stations = getNearbyBuoysFixture.data;
    const { wrapper } = mountWithReduxAndRouter(BuoyCarouselCards, { stations });

    const list = wrapper.find('.buoyCarouselCards');

    expect(list).to.have.length(1);

    const onScroll = list.prop('onScroll')!;

    act(() => {
      onScroll({
        target: { scrollLeft: 0, scrollWidth: 20, clientWidth: 2.5 },
      } as any as UIEvent<HTMLUListElement>);

      wrapper.setProps({ update: true });
      wrapper.update();
    });

    const arrows = wrapper.find(BuoyCarouselArrows);
    expect(arrows.prop('leftDisabled')).to.be.true();
  });

  it('should set rightDisabled if the list is scolled to the end', () => {
    const stations = getNearbyBuoysFixture.data;
    const { wrapper } = mountWithReduxAndRouter(BuoyCarouselCards, { stations });

    const list = wrapper.find('.buoyCarouselCards');

    expect(list).to.have.length(1);

    act(() => {
      list.prop('onScroll')!({
        target: { scrollLeft: 20, scrollWidth: 20, clientWidth: 2.5 },
      } as any as UIEvent<HTMLUListElement>);

      wrapper.setProps({ update: true });
      wrapper.update();
    });

    const arrows = wrapper.find(BuoyCarouselArrows);
    expect(arrows.prop('rightDisabled')).to.be.true();
  });
});
