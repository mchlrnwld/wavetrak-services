import PropTypes from 'prop-types';
import React from 'react';
import { pageStructuredData, videoStructuredData } from '@surfline/web-common';
import { spotReportPath } from '../../utils/urls';
import MetaTags from '../MetaTags';
import config from '../../config';

const title = (spotName, cameras, multiCam) => {
  if (multiCam) {
    return `${spotName} Surf Report, Live Surf Cams & 16-Day Surf Forecast - Surfline`;
  }
  if (cameras && cameras[0]) {
    return `${spotName} Surf Report, Live Surf Cam & 16-Day Surf Forecast - Surfline`;
  }
  return `${spotName} Surf Report & 16-Day Surf Forecast - Surfline`;
};

const description = (spotName, cameras, multiCam, report) => {
  const hasCamera = cameras && cameras[0];
  if (multiCam && report) {
    return `Get today's most accurate ${spotName} surf report with multiple live HD surf cams for current swell, wind, and wave conditions. With full written report (updated twice daily) and 16-day surf forecasts, so you can know before you go.`;
  }
  if (multiCam && !report) {
    return `Get today's most accurate ${spotName} surf report with multiple live HD surf cams for current swell, wind, and wave conditions. With proprietary LOTUS report on swell, wind and waves and 16-day surf forecasts, so you can know before you go.`;
  }
  if (hasCamera && report) {
    return `Get today's most accurate ${spotName} surf report with live HD surf cam for current swell, wind and wave conditions. With full written report (updated twice daily) and 16-day surf forecasts, so you can know before you go.`;
  }
  if (!hasCamera && report) {
    return `Get today's most accurate ${spotName} surf report. With full written report (updated twice daily) on swell, wind and waves and 16-day surf forecasts, so you can know before you go.`;
  }
  return `Get today's most accurate ${spotName} surf report. With proprietary LOTUS report on swell, wind and waves and 16-day surf forecasts, so you can know before you go.`;
};

const urlPath = (spotId, spotName) => spotReportPath({ _id: spotId, name: spotName });
const image = (cameras) => cameras?.[0]?.control;

const SpotPageMeta = ({
  spotName,
  spotId,
  cameras,
  multiCam,
  report,
  metaDescription: customDescription,
  titleTag,
}) => {
  const pageTitle = titleTag || title(spotName, cameras, multiCam);
  const metaDescription = customDescription || description(spotName, cameras, multiCam, report);
  const metaImage = image(cameras);
  const camDescription = `Watch the latest ${spotName} surf conditions at Surfline.com`;
  const url = `${config.surflineHost}${urlPath(spotId, spotName)}`;

  // Midnight of current day
  const currentDay = new Date();
  const startOfCurrentDayUTC = new Date(currentDay.setUTCHours(0, 0, 0, 0));
  const endOfCurrentDayUTC = new Date(currentDay.setUTCHours(24, 0, 0, 0));

  const fallbackDate = startOfCurrentDayUTC;

  const reportDate = report?.timestamp ? new Date(report.timestamp) : fallbackDate;

  const camStructuredDataName = `${spotName} Surf Cam`;

  return (
    <MetaTags
      title={pageTitle}
      description={metaDescription}
      urlPath={urlPath(spotId, spotName)}
      image={metaImage}
    >
      <script type="application/ld+json">
        {pageStructuredData({
          url,
          headline: pageTitle,
          image: metaImage,
          description: metaDescription,
          author: report?.forecaster?.name || 'Surfline Forecast Team',
          cssSelector: '.sl-spot-module',
          dateModified: reportDate,
        })}
      </script>
      <script type="application/ld+json">
        {videoStructuredData(
          camStructuredDataName,
          spotName,
          camDescription,
          metaImage,
          startOfCurrentDayUTC,
          startOfCurrentDayUTC,
          endOfCurrentDayUTC,
          `${config.surflineHost}${urlPath(spotId, spotName)}`,
        )}
      </script>
    </MetaTags>
  );
};

SpotPageMeta.propTypes = {
  spotId: PropTypes.string.isRequired,
  spotName: PropTypes.string.isRequired,
  report: PropTypes.shape({
    timestamp: PropTypes.number,
    forecaster: PropTypes.shape({
      name: PropTypes.string,
    }),
  }),
  multiCam: PropTypes.bool,
  cameras: PropTypes.arrayOf(
    PropTypes.shape({
      stillUrl: PropTypes.string,
    }),
  ),
  metaDescription: PropTypes.string,
  titleTag: PropTypes.string,
};

SpotPageMeta.defaultProps = {
  cameras: [],
  report: false,
  multiCam: false,
  metaDescription: null,
  titleTag: null,
};

export default SpotPageMeta;
