import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import MetaTags from '../MetaTags';
import SpotPageMeta from './SpotPageMeta';

describe('components / SpotPageMeta', () => {
  const spotName = 'HB Pier, Southside';
  const spotId = '5842041f4e65fad6a77088ed';

  it('renders MetaTags with appropriate props', () => {
    const cameras = [
      { control: 'https://camstills.cdn-surfline.com/hbpiersscam/latest_small.jpg' },
    ];

    const title = `${spotName} Surf Report, Live Surf Cams & 16-Day Surf Forecast - Surfline`;
    const description = `Get today's most accurate ${spotName} surf report with multiple live HD surf cams for current swell, wind, and wave conditions. With full written report (updated twice daily) and 16-day surf forecasts, so you can know before you go.`;
    const urlPath = `/surf-report/hb-pier-southside/${spotId}`;
    const image = cameras[0].control;

    const wrapper = shallow(
      <SpotPageMeta spotName={spotName} report multiCam spotId={spotId} cameras={cameras} />,
    );
    const metaTags = wrapper.find(MetaTags);

    expect(metaTags).to.have.length(1);
    expect(metaTags.prop('title')).to.equal(title);
    expect(metaTags.prop('description')).to.equal(description);
    expect(metaTags.prop('image')).to.equal(image);
    expect(metaTags.prop('urlPath')).to.equal(urlPath);
  });

  it('renders MetaTags with appropriate title for a spot w/o cams', () => {
    const cameras = [];
    const title = `${spotName} Surf Report & 16-Day Surf Forecast - Surfline`;

    const wrapper = shallow(<SpotPageMeta spotName={spotName} spotId={spotId} cameras={cameras} />);
    const metaTags = wrapper.find(MetaTags);

    expect(metaTags).to.have.length(1);
    expect(metaTags.prop('title')).to.equal(title);
  });
});
