import React, { Component } from 'react';
import { Cookies } from 'react-cookie';
import { EmailVerificationModal, userPropType } from '@surfline/quiver-react';
import { getWindow } from '@surfline/web-common';
import { resendEmailVerification } from '../../common/api/emailVerification';
import CTAHackChecker from '../../utils/CTAHackChecker';

class EmailVerification extends Component {
  componentDidMount() {
    const win = getWindow();
    this.trackModalOpen();
    win.document.body.style.position = 'fixed';
    win.document.body.style.width = '100%';
    const ctaHackConfig = {
      targetClasses: ['quiver-modal-container', 'ReactModal__Body--open'],
      position: 'fixed',
    };

    this.ctaHackChecker = new CTAHackChecker(ctaHackConfig);
    this.ctaHackChecker.observe();
    this.ctaHackChecker.startDOMCheck();
    win.addEventListener('resize', this.resizeListener);
  }

  componentWillUnmount() {
    const win = getWindow();
    this.ctaHackChecker.disconnect();
    this.ctaHackChecker.stopDOMCheck();
    win.removeEventListener('resize', this.resizeListener);
  }

  resizeListener = () => {
    this.ctaHackChecker.disconnect();
    this.ctaHackChecker.stopDOMCheck();
    if (this.resizeTimeout) return;

    this.resizeTimeout = setTimeout(() => {
      this.resizeTimeout = null;
      this.ctaHackChecker.observe();
      this.ctaHackChecker.startDOMCheck();
    }, 3000);
  };

  trackModalOpen = () => {
    const win = getWindow();
    if (win?.analytics) {
      win.analytics.track('Showed Modal', {
        modalCategory: 'email verification modal',
      });
    }
  };

  trackButtonClicked = () => {
    const win = getWindow();
    if (win?.analytics) {
      win.analytics.track('Clicked Request Email Verification', {
        location: 'email verification modal',
      });
    }
  };

  triggerEmail = () => {
    const cookies = new Cookies();
    const accessToken = cookies.get('access_token');
    resendEmailVerification(accessToken);
    this.trackButtonClicked();
  };

  render() {
    const { userDetails } = this.props;
    return <EmailVerificationModal user={userDetails} doTriggerEmail={this.triggerEmail} />;
  }
}

EmailVerification.propTypes = {
  userDetails: userPropType.isRequired,
};

export default EmailVerification;
