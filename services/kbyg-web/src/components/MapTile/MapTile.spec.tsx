import React from 'react';

import { expect } from 'chai';
import { mount } from 'enzyme';
import { color } from '@surfline/quiver-themes';
import { CircleIcon } from '@surfline/quiver-react';
import Skeleton from 'react-loading-skeleton';
import MapTile, { Props } from './MapTile';

describe('components / MapTile', () => {
  const lat = 34.02587527588963;
  const lon = -118.7554939066114;

  const props: Props = {
    lat,
    lon,
    loading: false,
    status: 'ONLINE',
  };

  const propsOffline: Props = {
    lat,
    lon,
    loading: false,
    status: 'OFFLINE',
  };

  const propsWithLoading: Props = {
    loading: true,
  };

  const propsLatMissing: Props = {
    lon,
  };

  const propsLonMissing: Props = {
    lat,
  };

  it('renders 1 CircleIcon component on top of background image', () => {
    const wrapper = mount(<MapTile {...props} />);

    const CircleIconInstance = wrapper.find(CircleIcon);
    expect(CircleIconInstance).to.have.length(1);
    expect(CircleIconInstance.prop('fill')).to.be.equal(color('light-blue', 90));
  });

  it('renders 1 CircleIcon component with an offline fill', () => {
    const wrapper = mount(<MapTile {...propsOffline} />);

    const CircleIconInstance = wrapper.find(CircleIcon);
    expect(CircleIconInstance).to.have.length(1);
    expect(CircleIconInstance.prop('fill')).to.be.equal(color('bright-red', 30));
  });

  it('renders loading skeleton', () => {
    const wrapper = mount(<MapTile {...propsWithLoading} />);

    const LoadingSkeletonInstance = wrapper.find(Skeleton);
    const loadingDiv = wrapper.find('.mapTile');
    expect(LoadingSkeletonInstance).to.have.length(1);
    expect(loadingDiv).to.have.length(1);
  });

  it('renders error div if lat is missing', () => {
    const wrapper = mount(<MapTile {...propsLatMissing} />);

    const errorDiv = wrapper.find('.mapTileError');

    expect(errorDiv).to.have.length(1);
  });

  it('renders error div if lon is missing', () => {
    const wrapper = mount(<MapTile {...propsLonMissing} />);

    const errorDiv = wrapper.find('.mapTileError');

    expect(errorDiv).to.have.length(1);
  });
});
