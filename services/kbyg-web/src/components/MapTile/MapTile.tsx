import React from 'react';
import { getMapTileUrl } from '@surfline/web-common';
import Skeleton from 'react-loading-skeleton';
import { CircleIcon } from '@surfline/quiver-react';
import { color } from '@surfline/quiver-themes';
import classNames from 'classnames';
import {
  BUOY_CURRENT_READINGS_MAP_WIDTH,
  BUOY_CURRENT_READINGS_MAP_HEIGHT,
} from '../../common/constants';

import styles from './MapTile.module.scss';
import { BuoyStatus } from '../../types/buoys';

const MAP_TILE_ZOOM = 7;

const getMapTileClass = (rootClass?: string, error?: boolean) =>
  classNames(
    {
      [styles.mapTile]: true,
      [styles.mapTileError]: error,
    },
    rootClass,
  );

export interface Props {
  lat?: number;
  lon?: number;
  loading?: boolean;
  status?: BuoyStatus;
  classes?: { root?: string };
}

const MapTile: React.FC<Props> = ({ lat, lon, loading, status, classes = {} }) => {
  const mapUrl = getMapTileUrl(
    BUOY_CURRENT_READINGS_MAP_WIDTH,
    BUOY_CURRENT_READINGS_MAP_HEIGHT,
    lat,
    lon,
    MAP_TILE_ZOOM,
  );
  const size = { width: BUOY_CURRENT_READINGS_MAP_WIDTH, height: BUOY_CURRENT_READINGS_MAP_HEIGHT };

  if (loading) {
    return (
      <div className={getMapTileClass(classes.root)} style={size}>
        <Skeleton
          width={BUOY_CURRENT_READINGS_MAP_WIDTH}
          height={BUOY_CURRENT_READINGS_MAP_HEIGHT}
        />
      </div>
    );
  }
  if (!lat || !lon) {
    return <div className={getMapTileClass(classes.root, true)} style={size} />;
  }

  return (
    <div
      className={getMapTileClass(classes.root)}
      style={{
        ...size,
        backgroundImage: `url("${mapUrl}")`,
      }}
    >
      <CircleIcon
        border="white"
        size={15}
        borderWidth={2}
        fill={status === 'ONLINE' ? color('light-blue', 90) : color('bright-red', 30)}
      />
    </div>
  );
};

export default MapTile;
