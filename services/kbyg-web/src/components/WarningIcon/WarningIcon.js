import React from 'react';

const WarningIcon = () => (
  <svg className="sl-warning-icon" width="18" height="18" viewBox="0 0 18 18">
    <g stroke="none" fill="none">
      <circle cx="9" cy="9" r="8.5" />
      <rect x="8" y="3" width="2" height="8" rx="1" />
      <rect x="8" y="13" width="2" height="2" rx="1" />
    </g>
  </svg>
);

export default WarningIcon;
