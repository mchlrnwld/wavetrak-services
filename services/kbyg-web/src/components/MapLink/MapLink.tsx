import React from 'react';

import ActiveLink from '../ActiveLink';

import styles from './MapLink.module.scss';

interface Props {
  href: string;
}

const MapLink: React.FC<Props> = ({ children, href }) => (
  <ActiveLink classes={{ root: styles.slMapLink, active: styles.slMapLinkActive }} href={href}>
    {children}
  </ActiveLink>
);

export default MapLink;
