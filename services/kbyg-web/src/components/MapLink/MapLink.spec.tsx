import { expect } from 'chai';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import ActiveLink from '../ActiveLink';

import MapLink from './MapLink';

describe('components / MapLink', () => {
  it('should render a nav link', () => {
    const { wrapper } = mountWithReduxAndRouter(MapLink, { children: 'test', href: '/test' });

    const activeLink = wrapper.find(ActiveLink);

    expect(activeLink).to.have.length(1);

    expect(activeLink.text()).to.be.equal('test');
    expect(activeLink.prop('href')).to.be.equal('/test');
    expect(activeLink.prop('classes')).to.deep.equal({
      root: 'slMapLink',
      active: 'slMapLinkActive',
    });
  });
});
