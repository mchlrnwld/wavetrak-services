import React, { useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import { slugify } from '@surfline/web-common';
import { useEvent } from 'react-use';

import styles from './CamAngleSelector.module.scss';
import WavetrakLink from '../WavetrakLink';

const CamAngleSelector = ({ angles, setVisible, spotName }) => {
  const ref = useRef(null);

  const outsideClickHandler = useCallback(
    (event) => {
      if (ref.current && !ref.current.contains(event.target)) {
        setVisible(false);
        event.preventDefault();
      }
    },
    [setVisible],
  );

  useEvent('mousedown', outsideClickHandler);
  useEvent('touchend', outsideClickHandler);

  const anglebuttons = angles.map(({ stillUrl, title, _id }) => (
    <WavetrakLink
      key={_id}
      className={styles.camAngleOption}
      href={`/surf-cams/${slugify(spotName)}/${_id}`}
      title={title}
    >
      <div className={styles.camAngleThumbnail} style={{ backgroundImage: `url('${stillUrl}')` }} />
      <h4 className={styles.camAngleName}>{title.split(' - ')[1] || title}</h4>
    </WavetrakLink>
  ));
  return (
    <div className={styles.camAngleContainer}>
      <div ref={ref} className={styles.camAngle}>
        {anglebuttons}
      </div>
    </div>
  );
};

CamAngleSelector.propTypes = {
  angles: PropTypes.arrayOf(
    PropTypes.shape({
      stillUrl: PropTypes.string,
      title: PropTypes.string,
      _id: PropTypes.string,
    }),
  ).isRequired,
  setVisible: PropTypes.func.isRequired,
  spotName: PropTypes.string.isRequired,
};

export default CamAngleSelector;
