import React from 'react';

import { expect } from 'chai';
import sinon, { SinonStub } from 'sinon';
import { mount } from 'enzyme';
import { CircleMarker, Map } from '@surfline/quiver-react';
import { color } from '@surfline/quiver-themes';

import { Map as TMap, PopupEvent } from 'leaflet';
import { act } from 'react-dom/test-utils';
import * as useMap from '../../hooks/map/useMap';
import { getNearbyBuoysFixture } from '../../common/api/fixtures/buoys';

import BuoyMapMarker from './BuoyMapMarker';
import * as useActiveBuoy from '../../hooks/buoys/useActiveBuoy';
import { NearbyStation } from '../../types/buoys';
import { wait } from '../../utils/test-utils';

const [buoyFixture] = getNearbyBuoysFixture.data;

describe('components / BuoyMapMarker', () => {
  let useMapStub: SinonStub;
  let useActiveBuoyStub: SinonStub;

  const setActiveBuoyIdStub = sinon.stub();

  let map: TMap;

  const Comp = ({ buoy }: { buoy: NearbyStation }) => (
    <Map
      center={{ lat: 0, lon: 0 }}
      mapProps={{
        whenCreated: (m) => {
          useMapStub.returns([m]);
          map = m;
        },
      }}
    >
      <BuoyMapMarker buoy={buoy} />
    </Map>
  );

  beforeEach(() => {
    useMapStub = sinon.stub(useMap, 'default').returns([undefined, () => {}]);
    useActiveBuoyStub = sinon.stub(useActiveBuoy, 'default').returns(['', setActiveBuoyIdStub]);
  });

  afterEach(() => {
    setActiveBuoyIdStub.reset();
    useMapStub.restore();
    useActiveBuoyStub.restore();
  });

  it('should render an online buoy with the proper activeFill', async () => {
    const wrapper = mount(<Comp buoy={{ ...buoyFixture, status: 'ONLINE' }} />);
    try {
      // Wait for the map to setup
      await act(async () => {
        await wait(150);
        wrapper.setProps({ update: 1 });
        wrapper.update();
      });

      expect(map).to.exist("The map instance must exist or else the markers can't render");

      const marker = wrapper.find(CircleMarker);

      expect(marker).to.have.length(1);

      expect(marker.prop('activeFill')).to.be.equal(color('light-blue', 90));
    } finally {
      act(() => {
        wrapper.unmount();
      });
    }
  });

  it('should render an offline buoy with the proper activeFill', async () => {
    const wrapper = mount(<Comp buoy={{ ...buoyFixture, status: 'OFFLINE' }} />);
    try {
      // Wait for the map to setup
      await act(async () => {
        await wait(150);
        wrapper.setProps({ update: 1 });
        wrapper.update();
      });

      expect(map).to.exist("The map instance must exist or else the markers can't render");

      const marker = wrapper.find(CircleMarker);

      expect(marker).to.have.length(1);

      expect(marker.prop('activeFill')).to.be.equal(color('bright-red', 30));
    } finally {
      act(() => {
        wrapper.unmount();
      });
    }
  });

  it('should handle activating the buoy on popup open', async () => {
    const wrapper = mount(<Comp buoy={buoyFixture} />);
    try {
      // Wait for the map to setup
      await act(async () => {
        await wait(150);
        wrapper.setProps({ update: 1 });
        wrapper.update();
      });

      expect(map).to.exist("The map instance must exist or else the markers can't render");

      const marker = wrapper.find(CircleMarker);

      expect(marker).to.have.length(1);

      marker.prop('eventHandlers').popupopen!({} as PopupEvent);

      expect(setActiveBuoyIdStub).to.have.been.calledOnceWithExactly(buoyFixture.id);
    } finally {
      act(() => {
        wrapper.unmount();
      });
    }
  });

  it('should handle de-activating the buoy on popup close', async () => {
    useActiveBuoyStub.returns([buoyFixture.id, setActiveBuoyIdStub]);
    const wrapper = mount(<Comp buoy={buoyFixture} />);
    try {
      // Wait for the map to setup
      await act(async () => {
        await wait(150);
        wrapper.setProps({ update: 1 });
        wrapper.update();
      });

      expect(map).to.exist("The map instance must exist or else the markers can't render");

      const marker = wrapper.find(CircleMarker);

      expect(marker).to.have.length(1);

      marker.prop('eventHandlers').popupclose!({} as PopupEvent);

      expect(setActiveBuoyIdStub).to.have.been.calledOnceWithExactly();
    } finally {
      act(() => {
        wrapper.unmount();
      });
    }
  });
});
