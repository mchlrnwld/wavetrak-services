import React, { useMemo, useRef, useCallback, memo, useEffect } from 'react';
import classNames from 'classnames';
import { color } from '@surfline/quiver-themes';
import { CircleMarker } from '@surfline/quiver-react';
import { Marker } from 'leaflet';

import BuoyMarkerPopup from '../BuoyMarkerPopup';

import useActiveBuoy from '../../hooks/buoys/useActiveBuoy';
import { NearbyStation } from '../../types/buoys';

import styles from './BuoyMapMarker.module.scss';

const className = (active: boolean) =>
  classNames(styles.buoyMapMarker, {
    [styles.buoyMapMarkerActive]: active,
  });

interface Props {
  buoy: NearbyStation;
}

const BuoyMapMarker: React.FC<Props> = ({ buoy }) => {
  const ref = useRef<Marker>();
  const { latitude, longitude, status, id } = buoy;
  const position = useMemo(() => ({ lat: latitude, lon: longitude }), [latitude, longitude]);
  const [activeBuoyId, setActiveBuoyId] = useActiveBuoy();
  const active = activeBuoyId === buoy.id;

  // Effect that toggles the popup open whenever the marker becomes `active`
  useEffect(() => {
    if (active) {
      ref.current?.openPopup();
    } else {
      ref.current?.closePopup();
    }
  }, [active]);

  const onPopupOpen = useCallback(() => setActiveBuoyId(id), [setActiveBuoyId, id]);

  const eventHandlers = useMemo(
    () => ({
      popupopen: onPopupOpen,
      popupclose: () => {
        setActiveBuoyId();
      },
    }),
    [onPopupOpen, setActiveBuoyId],
  );

  const activeFill = status === 'ONLINE' ? color('light-blue', 90) : color('bright-red', 30);

  return (
    <CircleMarker
      inactiveFill={color('gray')}
      activeFill={activeFill}
      active={active}
      size={20}
      border={color('white')}
      borderWidth={2}
      // @ts-ignore
      position={position}
      eventHandlers={eventHandlers}
      // @ts-ignore - Types on the quiver component are incorrect
      ref={ref}
      className={className(active)}
      // To ensure that popups render above markers
      zIndexOffset={-500}
    >
      <BuoyMarkerPopup buoy={buoy} />
    </CircleMarker>
  );
};

export default memo(BuoyMapMarker);
