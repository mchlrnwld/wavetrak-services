import React, { memo, forwardRef } from 'react';
import PropTypes from 'prop-types';
import { useWindowSize } from 'react-use';
import { areEqual, FixedSizeGrid as Grid } from 'react-window';
import AutoSizer from 'react-virtualized-auto-sizer';
import { shallowEqual } from 'react-redux';

import config from '../../config';
import BuoyCard from '../BuoyCard';
import { NearbyStationPropType } from '../../propTypes/buoys';

export const DESKTOP_HORIZ_GUTTER_SIZE = 20;
export const MOBILE_HORIZ_GUTTER_SIZE = 8;
export const DESKTOP_VERT_GUTTER_SIZE = 20;
export const MOBILE_VERT_GUTTER_SIZE = 5;

// WARNING: If you change any of these you will need to also change the scss.
// We aren't using javascript because we want to ensure the server-side render
// is able to render the cards in the correct width (SSR doesn't have access to
// device width in JS)
const SMALL_BUOY_CARD_HEIGHT = 130;
const SMALL_BUOY_CARD_WIDTH = 203;
const LARGE_BUOY_CARD_HEIGHT = 184;
const LARGE_BUOY_CARD_WIDTH = 300;

/**
 * @typedef {import('../../propTypes/buoys').NearbyStation} NearbyStation
 */

/**
 * @typedef {object} GridInnerElementProps
 * @property {React.CSSProperties} style
 */

const useGutterSizes = () => {
  const { width } = useWindowSize();

  const isDesktop = width >= config.tabletLargeWidth;

  const horizontalGutterSize = isDesktop ? DESKTOP_HORIZ_GUTTER_SIZE : MOBILE_HORIZ_GUTTER_SIZE;
  const verticalGutterSize = isDesktop ? DESKTOP_VERT_GUTTER_SIZE : MOBILE_VERT_GUTTER_SIZE;

  return { horizontalGutterSize, verticalGutterSize };
};

/**
 * @type {React.MemoExoticComponent<React.ForwardRefExoticComponent<GridInnerElementProps>>}
 */
export const GridInnerElement = memo(
  forwardRef(({ style, ...rest }, ref) => {
    const { verticalGutterSize, horizontalGutterSize } = useGutterSizes();
    return (
      <ul
        ref={ref}
        style={{
          ...style,
          listStyleType: 'none',
          paddingLeft: horizontalGutterSize,
          paddingTop: verticalGutterSize,
        }}
        {...rest}
      />
    );
  }),
  areEqual,
);

GridInnerElement.displayName = 'GridInnerElement';

GridInnerElement.propTypes = {
  style: PropTypes.shape({
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }).isRequired,
};

/**
 * @typedef {object} ItemProps
 * @property {NearbyStation[]} data
 * @property {number} columnIndex
 * @property {number} rowIndex
 * @property {React.CSSProperties} style
 * /

/**
 * @type {React.MemoExoticComponent<React.FunctionComponent<ItemProps>>}
 */
export const GridBuoyItem = memo(({ columnIndex, rowIndex, style, data }) => {
  const index = rowIndex * 2 + columnIndex;
  const buoy = data[index];

  const { horizontalGutterSize, verticalGutterSize } = useGutterSizes();

  if (!buoy) {
    return null;
  }

  return (
    <li
      className="sl-buoy-virtual-list-item"
      style={{
        ...style,
        top: style.top + verticalGutterSize,
        left: style.left + horizontalGutterSize,
        width: style.width - horizontalGutterSize,
        height: style.height - verticalGutterSize,
      }}
    >
      <BuoyCard key={buoy.id} station={buoy} withActiveState trackLocation="Map" />
    </li>
  );
}, areEqual);

GridBuoyItem.displayName = 'GridBuoyItem';

GridBuoyItem.propTypes = {
  columnIndex: PropTypes.number.isRequired,
  rowIndex: PropTypes.number.isRequired,
  style: PropTypes.shape({
    top: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    left: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }).isRequired,
  data: PropTypes.arrayOf(PropTypes.shape(NearbyStationPropType)).isRequired,
};

/**
 * @typedef {object} Props
 * @property {NearbyStation[]} buoys
 * @property {boolean} isVerticalView
 */

/**
 * @type {React.MemoExoticComponent<React.FunctionComponent<Props>>}
 */
export const BuoyVirtualList = memo(
  ({ isVerticalView, buoys }) => (
    <AutoSizer className="sl-buoy-virtual-list-autosizer">
      {({ height, width }) => {
        const desktopColumnCount = Math.floor(width / LARGE_BUOY_CARD_WIDTH);

        const rowHeight = isVerticalView
          ? LARGE_BUOY_CARD_HEIGHT + DESKTOP_VERT_GUTTER_SIZE
          : SMALL_BUOY_CARD_HEIGHT + MOBILE_VERT_GUTTER_SIZE;
        const columnWidth = isVerticalView
          ? LARGE_BUOY_CARD_WIDTH + DESKTOP_HORIZ_GUTTER_SIZE
          : SMALL_BUOY_CARD_WIDTH + MOBILE_HORIZ_GUTTER_SIZE;

        const columnCount = isVerticalView ? desktopColumnCount : buoys.length;
        const rowCount = isVerticalView ? Math.round(buoys.length / 2) : 1;

        return (
          <Grid
            className="sl-buoy-virtual-list-grid"
            height={height}
            width={width}
            columnWidth={columnWidth}
            columnCount={columnCount}
            rowHeight={rowHeight}
            rowCount={rowCount}
            style={{ overflow: isVerticalView ? 'hidden auto' : 'auto hidden' }}
            itemData={buoys}
            innerElementType={GridInnerElement}
          >
            {GridBuoyItem}
          </Grid>
        );
      }}
    </AutoSizer>
  ),
  shallowEqual,
);

BuoyVirtualList.displayName = 'BuoyVirtualList';

BuoyVirtualList.propTypes = {
  isVerticalView: PropTypes.bool.isRequired,
  buoys: PropTypes.arrayOf(PropTypes.shape(NearbyStationPropType)).isRequired,
};
