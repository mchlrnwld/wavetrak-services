import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { useWindowSize } from 'react-use';

import config from '../../config';
import { BuoyVirtualList } from './GridInnerElement';
import { NearbyStationPropType } from '../../propTypes/buoys';

import styles from './MapBuoyCards.module.scss';

/**
 * @typedef {import('../../propTypes/buoys').NearbyStation} NearbyStation
 */

/**
 * @typedef {object} Props
 * @property {NearbyStation[]} [buoys]
 * @property {NearbyStation["id"]} [activeBuoyId]
 * @property {React.Dispatch<React.SetStateAction<NearbyStation['id']>>} setActiveBuoyId
 */

/** @type {React.FunctionComponent<Props>} */
const MapBuoyCards = ({ buoys }) => {
  const { width: deviceWidth } = useWindowSize();
  const isVerticalView = deviceWidth > config.tabletLargeWidth;

  return (
    <ul
      className={styles.mapBuoyCards}
      aria-label="List of buoys station within the map boundaries"
    >
      <BuoyVirtualList isVerticalView={isVerticalView} buoys={buoys} />
    </ul>
  );
};

MapBuoyCards.propTypes = {
  buoys: PropTypes.arrayOf(
    PropTypes.shape({
      ...NearbyStationPropType,
    }),
  ),
};

MapBuoyCards.defaultProps = {
  buoys: [],
};

export default memo(MapBuoyCards);
