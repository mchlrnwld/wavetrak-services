import { expect } from 'chai';

import MapBuoyCards from './MapBuoyCards';

import { getNearbyBuoysFixture } from '../../common/api/fixtures/buoys';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import { BuoyVirtualList } from './GridInnerElement';

const buoysProps = {
  buoys: getNearbyBuoysFixture.data,
  loading: false,
  error: false,
  noBuoysFound: false,
  setActiveBuoyId: () => {},
};

describe('components / MapBuoyCards', () => {
  it('should use an aria-label for accessibility purposes', () => {
    const { wrapper } = mountWithReduxAndRouter(MapBuoyCards, buoysProps);

    const ul = wrapper.find('ul');
    expect(ul.prop('aria-label')).to.exist();
  });

  it('should render the virtual list', () => {
    const { wrapper } = mountWithReduxAndRouter(MapBuoyCards, {
      isVerticalView: true,
      buoys: getNearbyBuoysFixture.data,
    });

    const list = wrapper.find(BuoyVirtualList);
    expect(list).to.have.length(1);
  });
});
