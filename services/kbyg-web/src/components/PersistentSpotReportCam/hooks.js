import { useCallback, useEffect, useState, useMemo } from 'react';
import { throttle as _throttle } from 'lodash';
import { getWindow } from '@surfline/web-common';
import { updatePlayerAdDimensions } from './utils';

/**
 * @param {import('./PersistentSpotReportCam').PersistentSpotReportCamProps["camPlayerRef"]} camPlayerRef
 * @param {import('./PersistentSpotReportCam').PersistentSpotReportCamProps["camRefHeight"]} camRefHeight
 * @param {import('./PersistentSpotReportCam').PersistentSpotReportCamProps["setCamRefHeight"]} setCamRefHeight
 */
export const useManageCamRefHeight = (camPlayerRef, camRefHeight, setCamRefHeight) => {
  const manageCamRefHeight = useCallback(
    (isResize) => {
      if (camPlayerRef.current) {
        const { height } = camPlayerRef.current.getBoundingClientRect();
        if (!camRefHeight || (isResize && camRefHeight !== height)) {
          setCamRefHeight(height);
        }
      }
    },
    [camPlayerRef, camRefHeight, setCamRefHeight],
  );

  return { manageCamRefHeight };
};

/**
 * @param {ReturnType<typeof useManageCamRefHeight>['manageCamRefHeight']} manageCamRefHeight
 * @param {ReturnType<typeof usePersistentCamPlayerAdDimensions>['setPersistentCamPlayerAdDimensions']} setCamPlayerAdDimensions
 */
export const useScrollListener = (manageCamRefHeight, setCamPlayerAdDimensions) => {
  // eslint-disable-next-line react/sort-comp
  const scrollListen = useMemo(
    () =>
      _throttle(
        () => {
          manageCamRefHeight();
          setCamPlayerAdDimensions();
        },
        350,
        { trailing: true },
      ),
    [manageCamRefHeight, setCamPlayerAdDimensions],
  );

  useEffect(() => {
    const win = getWindow();
    win?.addEventListener('scroll', scrollListen);

    return () => {
      win?.removeEventListener('scroll', scrollListen);
    };
  }, [scrollListen]);
};

/**
 * @param {ReturnType<typeof useManageCamRefHeight>['manageCamRefHeight']} manageCamRefHeight
 * @param {ReturnType<typeof usePersistentCamPlayerAdDimensions>['setPersistentCamPlayerAdDimensions']} setCamPlayerAdDimensions
 */
export const useResizeListener = (manageCamRefHeight, setCamPlayerAdDimensions) => {
  const resizeListener = useMemo(
    () =>
      _throttle(
        () => {
          const isResize = true;
          manageCamRefHeight(isResize);
          setCamPlayerAdDimensions();
        },
        350,
        { trailing: true },
      ),
    [manageCamRefHeight, setCamPlayerAdDimensions],
  );

  useEffect(() => {
    const win = getWindow();
    win?.addEventListener('resize', resizeListener);

    return () => {
      win?.removeEventListener('resize', resizeListener);
    };
  }, [resizeListener]);
};

/**
 * @deprecated If we move forward with rate limiting, we have no need for pre-rolls and can remove this
 * @param {import('./PersistentSpotReportCam').PersistentSpotReportCamProps["camPlayerRef"]} camPlayerRef
 * @param {boolean} stickyCamIsOpen
 * @param {boolean} stickyCamIsActive
 * @param {import('./PersistentSpotReportCam').PersistentSpotReportCamProps["playerId"]} playerId
 */
export const usePersistentCamPlayerAdDimensions = (
  camPlayerRef,
  stickyCamIsOpen,
  stickyCamIsActive,
  playerId,
) => {
  const win = getWindow();
  const [camPlayerAd, setCamPlayerAd] = useState({ height: 0, width: 0 });

  const setPersistentCamPlayerAdDimensions = useCallback(() => {
    if (!camPlayerRef?.current) return;

    const { height, width } = camPlayerRef.current.getBoundingClientRect();
    const isDimensionsDiff = camPlayerAd.width !== width && camPlayerAd.height !== height;

    if (win?.document && playerId && isDimensionsDiff) {
      updatePlayerAdDimensions(playerId, height, width);

      setCamPlayerAd({ height, width });
    }
    // Fixes an issue where the persistent cam styles were breaking on Safari.
    // For some reason the inline style changes causes the CSS to break, so
    // overriding them specifically on the persistent cam last allows Safari to repaint
    // properly
    // https://wavetrak.atlassian.net/browse/KBYG-2561
    const persistentCam = win?.document.querySelector('.sl-persistent-spot-report-cam');
    if (persistentCam?.style) {
      persistentCam.style.position = stickyCamIsActive && stickyCamIsOpen ? 'relative' : '';
    }
  }, [
    camPlayerAd.width,
    camPlayerAd.height,
    camPlayerRef,
    playerId,
    stickyCamIsActive,
    stickyCamIsOpen,
    win?.document,
  ]);

  return { setPersistentCamPlayerAdDimensions };
};
