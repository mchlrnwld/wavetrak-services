import { canUseDOM, getWindow } from '@surfline/web-common';

/**
 * @param {string} playerId
 * @param {number} height
 * @param {number} width
 */
export const updatePlayerAdDimensions = (playerId, height, width) => {
  if (!canUseDOM) {
    return;
  }
  const win = getWindow();
  const playerAd = win.document.getElementById(`${playerId}_ad`);

  if (!playerAd || !playerAd.firstElementChild) {
    return;
  }

  playerAd.firstElementChild.style.height = `${height}px`;
  playerAd.firstElementChild.style.width = `${width}px`;

  const video = playerAd.querySelector('video');
  if (video) {
    video.style.height = `${height}px`;
    video.style.width = `${width}px`;
  }

  const iframe = playerAd.querySelector('iframe');
  if (iframe) {
    iframe.style.height = `${height}px`;
    iframe.style.width = `${width}px`;
  }

  const topLevelDivs = playerAd.querySelectorAll(':scope > div');
  if (topLevelDivs.length > 1) {
    topLevelDivs.forEach((div) => {
      // eslint-disable-next-line no-param-reassign
      div.style.height = `${height}px`;
      // eslint-disable-next-line no-param-reassign
      div.style.width = `${width}px`;
    });
  }
};
