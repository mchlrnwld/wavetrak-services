import React, { useCallback, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { ClosedMediaPlayer, PersistentMediaPlayer } from '@surfline/quiver-react';
import { trackEvent } from '@surfline/web-common';
import SpotCamOverlay from '../SpotCamOverlay';
import spotReportDataPropType from '../../propTypes/spot';
import userDetailsPropTypes from '../../propTypes/userDetails';
import userSettingsPropTypes from '../../propTypes/userSettings';
import { readStickyCamCookie, setStickyCamCookie } from '../../utils/spotCamCookie';
import getSubscriptionEntitlement from '../../utils/getSubscriptionEntitlement';
import {
  useScrollListener,
  useResizeListener,
  usePersistentCamPlayerAdDimensions,
  useManageCamRefHeight,
} from './hooks';

/**
 * @param {boolean} stickyCamIsActive
 * @param {boolean} stickyCamIsOpen
 */
const getCamPlayerClasses = (stickyCamIsActive, stickyCamIsOpen) =>
  classNames({
    'sl-persistent-spot-report-cam': true,
    'sl-persistent-spot-report-cam--closed': stickyCamIsActive && !stickyCamIsOpen,
  });

/**
 * @typedef {Object} PersistentSpotReportCamProps
 * @property {boolean} isPremium,
 * @property {import('../SpotReportCam/SpotReportCam').SpotReportCamProps["reportData"]} reportData,
 * @property {import('../SpotReportCam/SpotReportCam').SpotReportCamProps["spot"]} spot,
 * @property {import('@surfline/web-common').UserState["details"]} userDetails,
 * @property {import('@surfline/web-common').UserState["settings"]} userSettings,
 * @property {(stickyCamIsActive: boolean) => JSX.Element} renderCamPlayer,
 * @property {React.MutableRefObject<HTMLElement>} camRef,
 * @property {string} playerId,
 * @property {?number} camRefHeight,
 * @property {(camRefHeight: number) => void} setCamRefHeight,
 *
 * @param {PersistentSpotReportCamProps}
 * @returns {JSX.Element}
 */
const PersistentSpotReportCam = ({
  camRef,
  camRefHeight,
  isPremium,
  playerId,
  spot,
  userDetails,
  userSettings,
  renderCamPlayer,
  reportData,
  setCamRefHeight,
}) => {
  /** @type {React.MutableRefObject<HTMLDivElement>} */
  const camPlayerRef = useRef();

  const [stickyCamIsActive, setStickyCamIsActive] = useState(false);
  const [stickyCamIsOpen, setStickyCamIsOpen] = useState(readStickyCamCookie());

  const { setPersistentCamPlayerAdDimensions } = usePersistentCamPlayerAdDimensions(
    camPlayerRef,
    stickyCamIsOpen,
    stickyCamIsActive,
    playerId,
  );

  const { manageCamRefHeight } = useManageCamRefHeight(camPlayerRef, camRefHeight, setCamRefHeight);
  useScrollListener(manageCamRefHeight, setPersistentCamPlayerAdDimensions);
  useResizeListener(manageCamRefHeight, setPersistentCamPlayerAdDimensions);

  const toggleStickyCamOpen = useCallback(() => {
    const subscriptionEntitlement = getSubscriptionEntitlement(isPremium, userDetails);
    trackEvent(`${stickyCamIsOpen ? 'Closed' : 'Opened'} Cam`, { subscriptionEntitlement });

    setStickyCamCookie(!stickyCamIsOpen);
    setStickyCamIsOpen(!stickyCamIsOpen);
  }, [isPremium, setStickyCamIsOpen, stickyCamIsOpen, userDetails]);

  return (
    <div className={getCamPlayerClasses(stickyCamIsActive, stickyCamIsOpen)}>
      <PersistentMediaPlayer
        containerRef={camRef?.current}
        stickyToggleCallback={(isSticky) => setStickyCamIsActive(isSticky)}
      >
        {(stickyCamIsActive && stickyCamIsOpen) || !stickyCamIsActive ? (
          <div className="sl-persistent-spot-report-cam__overlay">
            <div ref={camPlayerRef}>
              {stickyCamIsActive && (
                <SpotCamOverlay
                  isPremium={isPremium}
                  onClickCloseHandler={toggleStickyCamOpen}
                  reportData={reportData}
                  spot={spot}
                  stickyCamIsOpen={stickyCamIsOpen}
                  userSettings={userSettings}
                />
              )}
              {renderCamPlayer(stickyCamIsActive)}
            </div>
          </div>
        ) : null}
        {stickyCamIsActive && !stickyCamIsOpen ? (
          <ClosedMediaPlayer onClickHandler={toggleStickyCamOpen} />
        ) : null}
      </PersistentMediaPlayer>
    </div>
  );
};

PersistentSpotReportCam.defaultProps = {
  spot: null,
  isPremium: false,
  userDetails: null,
  userSettings: null,
  reportData: null,
  camRefHeight: null,
};

PersistentSpotReportCam.propTypes = {
  playerId: PropTypes.string.isRequired,
  camRefHeight: PropTypes.number,
  setCamRefHeight: PropTypes.func.isRequired,
  camRef: PropTypes.shape({
    current: PropTypes.shape(),
  }).isRequired,
  renderCamPlayer: PropTypes.func.isRequired,
  isPremium: PropTypes.bool,
  spot: PropTypes.shape({
    name: PropTypes.string,
    subregion: PropTypes.shape({
      _id: PropTypes.string,
      forecastStatus: PropTypes.string,
    }),
  }),
  userDetails: userDetailsPropTypes,
  userSettings: userSettingsPropTypes,
  reportData: spotReportDataPropType,
};

export default PersistentSpotReportCam;
