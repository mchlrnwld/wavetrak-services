import React from 'react';

import Skeleton from 'react-loading-skeleton';
import { mount } from 'enzyme';
import { expect } from 'chai';

import BuoyCardPlaceholder from './BuoyCardPlaceholder';

describe('components / BuoyCardPlaceholder', () => {
  it('should not render a skeleton if not loading', () => {
    const wrapper = mount(<BuoyCardPlaceholder />);

    expect(wrapper.find('.placeholder')).to.have.length(1);
    expect(wrapper.find(Skeleton)).to.have.length(0);
  });

  it('should render the loading skeleton when loading', () => {
    const wrapper = mount(<BuoyCardPlaceholder loading />);

    expect(wrapper.find('.placeholderLoading')).to.have.length(1);
    expect(wrapper.find(Skeleton)).to.have.length(1);
  });
});
