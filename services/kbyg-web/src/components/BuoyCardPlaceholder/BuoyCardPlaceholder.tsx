import React from 'react';
import classNames from 'classnames';
import Skeleton from 'react-loading-skeleton';
import PropTypes from 'prop-types';

import styles from './BuoyCardPlaceholder.module.scss';

const getClass = (loading?: boolean, largeOnly?: boolean, rootClass?: string) =>
  classNames(
    'sl-buoy-card-placeholder',
    {
      [styles.placeholder]: true,
      [styles.placeholderLoading]: loading,
      [styles.placeholderLargeOnly]: largeOnly,
    },
    rootClass,
  );

interface Props {
  loading?: boolean;
  largeOnly?: boolean;
  classes?: { root?: string };
}

const BuoyCardPlaceholder: React.FC<Props> = ({ loading, largeOnly, classes = {} }) => (
  <div className={getClass(loading, largeOnly, classes.root)}>
    {loading && <Skeleton className={styles.skeleton} />}
  </div>
);

BuoyCardPlaceholder.propTypes = {
  loading: PropTypes.bool.isRequired,
  largeOnly: PropTypes.bool,
};

BuoyCardPlaceholder.defaultProps = {
  largeOnly: false,
};

export default BuoyCardPlaceholder;
