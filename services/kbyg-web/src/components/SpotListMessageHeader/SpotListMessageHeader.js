import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

const messageClass = (hasSpots) =>
  classNames({
    'sl-spot-list-message-header': true,
    'sl-spot-list-message-header--no-spots': !hasSpots,
  });

const SpotListMessageHeader = ({ message, hasSpots }) => (
  <div className={messageClass(hasSpots)}>
    <div className="sl-spot-list-message-header__text">
      {message.text}
      <span className="sl-spot-list-message-header__text--hidden">{`, ${message.optionalText}`}</span>
      .
    </div>
  </div>
);

SpotListMessageHeader.propTypes = {
  message: PropTypes.shape({
    text: PropTypes.string,
    optionalText: PropTypes.string,
  }).isRequired,
  hasSpots: PropTypes.bool.isRequired,
};

export default SpotListMessageHeader;
