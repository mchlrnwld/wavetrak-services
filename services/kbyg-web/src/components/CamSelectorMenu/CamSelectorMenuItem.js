/* eslint-disable jsx-a11y/click-events-have-key-events */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import DualCamIcon from '../Icons/DualCam';
import TripleCamIcon from '../Icons/TripleCam';
import QuadCamIcon from '../Icons/QuadCam';
import SingleCamIcon from '../Icons/SingleCam';
import CameraPropTypes from '../../propTypes/camera';

const SINGLE = 'SINGLE';
const MULTI = 'MULTI';

class CamSelectorMenuItem extends Component {
  getCamOptionIcon = () => {
    const { menuType, numCams, isPrimary } = this.props;

    if (menuType === MULTI) {
      if (numCams === 2) {
        return <DualCamIcon isPrimary={isPrimary} />;
      }
      if (numCams === 3) {
        return <TripleCamIcon isPrimary={isPrimary} />;
      }
      if (numCams >= 4) {
        return <QuadCamIcon isPrimary={isPrimary} />;
      }
    }

    return <SingleCamIcon isPrimary={isPrimary} />;
  };

  camOptionIconClassName = () => {
    const { isPrimary } = this.props;
    return classNames({
      'sl-cam-selector-menu__camera-icon': true,
      'sl-cam-selector-menu__camera-icon--primary': isPrimary,
    });
  };

  render() {
    const { onClick, cam } = this.props;

    const camName = cam.title.split('- ')[1] || cam.title;

    return (
      <div onClick={() => onClick(cam)} key={cam._id} className="sl-cam-selector-menu__camera-item">
        <span className="sl-cam-selector-menu__camera-name">{camName}</span>
        <div className={this.camOptionIconClassName()}>{this.getCamOptionIcon()}</div>
      </div>
    );
  }
}

CamSelectorMenuItem.propTypes = {
  onClick: PropTypes.func.isRequired,
  cam: CameraPropTypes.isRequired,
  menuType: PropTypes.oneOf([SINGLE, MULTI]).isRequired,
  numCams: PropTypes.number.isRequired,
  isPrimary: PropTypes.bool,
};

CamSelectorMenuItem.defaultProps = {
  isPrimary: false,
};

export default CamSelectorMenuItem;
