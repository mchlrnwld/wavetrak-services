/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { CloseIcon, CamViewSelector } from '@surfline/quiver-react';
import { getWindow, trackEvent } from '@surfline/web-common';
import CamSelectorMenuItem from './CamSelectorMenuItem';
import CameraPropTypes from '../../propTypes/camera';
import devicePropType from '../../propTypes/device';
import config from '../../config';
import getSubscriptionEntitlement from '../../utils/getSubscriptionEntitlement';
import userDetailsPropTypes from '../../propTypes/userDetails';

const { tabletLargeWidth: TABLET_LARGE_WIDTH } = config;
const SINGLE = 'SINGLE';
const MULTI = 'MULTI';

class CamSelectorMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menuOpen: false,
      menuType: SINGLE,
      isHovering: false,
    };
  }

  componentDidMount() {
    this.setBodyStyle();
  }

  componentDidUpdate(prevProps, prevState) {
    const { menuOpen } = this.state;
    if (prevState.menuOpen !== menuOpen) {
      this.setBodyStyle();
    }
  }

  onCamSelect = (camera) => {
    const { spotId, subregionId, isPremium, device, cameras, userDetails, doSetCamView } =
      this.props;
    const { menuType } = this.state;
    const isDesktop = device.width >= TABLET_LARGE_WIDTH;
    const subscriptionEntitlement = getSubscriptionEntitlement(isPremium, userDetails);

    this.setState({
      menuOpen: false,
    });

    doSetCamView(menuType, camera);

    trackEvent('Toggled Cam View', {
      category: 'cams & reports',
      subregionId,
      spotId,
      camID: camera._id,
      camName: camera.title,
      isMobileView: !isDesktop,
      view: menuType,
      cam_number: cameras.length,
      subscriptionEntitlement,
    });
    return true;
  };

  onMouseEnterMenu = (type) => {
    const { device } = this.props;
    const isDesktop = device.width >= TABLET_LARGE_WIDTH;
    // Hover actions are only used for Desktop
    if (isDesktop) {
      this.setState({
        menuOpen: true,
        menuType: type,
        isHovering: true,
      });
    }
  };

  onMouseLeaveMenu = () => {
    const { device } = this.props;
    const isDesktop = device.width >= TABLET_LARGE_WIDTH;
    if (isDesktop) {
      this.setState({
        menuOpen: false,
        isHovering: false,
      });
    }
  };

  onClickSelector = (type) => {
    const {
      spotId,
      subregionId,
      device,
      doSetCamView,
      cameras,
      isPremium,
      userDetails,
      primaryCam,
    } = this.props;
    const isDesktop = device.width >= TABLET_LARGE_WIDTH;
    const subscriptionEntitlement = getSubscriptionEntitlement(isPremium, userDetails);

    // Desktop and mobile have different interactions for the cam view selectors
    if (!isDesktop) {
      return this.setState({
        menuOpen: true,
        menuType: type,
      });
    }

    trackEvent('Toggled Cam View', {
      category: 'cams & reports',
      subregionId,
      spotId,
      camID: primaryCam._id,
      camName: primaryCam.title,
      view: type,
      isMobileView: !isDesktop,
      cam_number: cameras.length,
      subscriptionEntitlement,
    });

    this.setState({
      menuOpen: false,
    });
    return doSetCamView(type, primaryCam);
  };

  setBodyStyle = () => {
    const win = getWindow();
    if (win) {
      const { menuOpen } = this.state;
      const { device } = this.props;
      const isDesktop = device.width >= TABLET_LARGE_WIDTH;
      if (menuOpen && !isDesktop) {
        win.document.body.style.position = 'fixed';
      } else {
        win.document.body.style.position = '';
      }
    }
  };

  closeMenu = () => {
    this.setState({
      menuOpen: false,
      isHovering: false,
    });
  };

  menuClassName = () => {
    const { menuOpen, menuType } = this.state;
    return classNames({
      'sl-cam-selector-menu__contents': true,
      'sl-cam-selector-menu__contents--open': menuOpen,
      'sl-cam-selector-menu__contents--closed': !menuOpen,
      'sl-cam-selector-menu__contents--single': menuType === SINGLE,
      'sl-cam-selector-menu__contents--multi': menuType === MULTI,
    });
  };

  render() {
    const { isPremium, initialCameras, cameras, primaryCam, numCams, activeView } = this.props;
    const { menuType, isHovering } = this.state;
    const menuTypeString = menuType === 'SINGLE' ? 'Single' : 'Multi';

    return (
      <div className="sl-cam-selector-menu">
        <div className="sl-cam-selector-menu__selectors">
          <CamViewSelector
            onMouseEnterMenu={this.onMouseEnterMenu}
            onClick={this.onClickSelector}
            isPremium={isPremium}
            active={activeView}
            cameras={cameras}
            onMouseLeaveMenu={this.onMouseLeaveMenu}
            isHovering={isHovering}
            menuType={menuType}
          />
        </div>
        <div
          className={this.menuClassName()}
          onMouseEnter={() => this.onMouseEnterMenu(menuType)}
          onMouseLeave={this.onMouseLeaveMenu}
        >
          <div className="sl-cam-selector-menu__triangle" />
          <div onClick={this.closeMenu} className="sl-cam-selector-menu__header">
            <span className="sl-flex-placeholder" />
            <span className="sl-cam-selector-menu__header__title">{`${menuTypeString}-Cam View`}</span>
            <CloseIcon />
          </div>
          <div className="sl-cam-selector-menu__cameras">
            {initialCameras.map((cam) => {
              const isPrimary = cam._id === primaryCam._id;
              return (
                <CamSelectorMenuItem
                  key={cam._id}
                  primaryCam={primaryCam}
                  cam={cam}
                  isPrimary={isPrimary}
                  menuType={menuType}
                  onClick={this.onCamSelect}
                  numCams={numCams}
                />
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

CamSelectorMenu.propTypes = {
  isPremium: PropTypes.bool,
  cameras: PropTypes.arrayOf(CameraPropTypes),
  initialCameras: PropTypes.arrayOf(CameraPropTypes),
  activeView: PropTypes.oneOf([SINGLE, MULTI]).isRequired,
  primaryCam: CameraPropTypes.isRequired,
  numCams: PropTypes.number.isRequired,
  spotId: PropTypes.string.isRequired,
  subregionId: PropTypes.string.isRequired,
  doSetCamView: PropTypes.func.isRequired,
  device: devicePropType.isRequired,
  userDetails: userDetailsPropTypes,
};

CamSelectorMenu.defaultProps = {
  isPremium: false,
  cameras: [],
  initialCameras: [],
  userDetails: null,
};

export default CamSelectorMenu;
