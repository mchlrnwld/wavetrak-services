import { withDevice } from '@surfline/quiver-react';
import CamSelectorMenu from './CamSelectorMenu';

export default withDevice(CamSelectorMenu);
