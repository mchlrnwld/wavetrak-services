import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { mount } from 'enzyme';
import { CloseIcon } from '@surfline/quiver-react';
import CamSelectorMenu from './CamSelectorMenu';
import CamSelectorMenuItem from './CamSelectorMenuItem';

const cameras = [
  {
    _id: '58a377daea714bf7668c000f',
    title: 'SOCAL - HB Pier Southside Overview',
    streamUrl: 'https://cams.cdn-surfline.com/cdn-wc/wc-hbpierssov/playlist.m3u8',
    stillUrl: 'https://camstills.cdn-surfline.com/wc-hbpierssov/latest_small.jpg',
    rewindBaseUrl: 'https://camrewinds.cdn-surfline.com/wc-hbpierssov/wc-hbpierssov',
    isPremium: false,
    status: {
      isDown: false,
      message: '',
    },
    control: 'https://camstills.cdn-surfline.com/wc-hbpierssov/latest_small.jpg',
    nighttime: false,
    rewindClip:
      'https://camrewinds.cdn-surfline.com/wc-hbpierssov/wc-hbpierssov.1300.2018-12-02.mp4',
  },
  {
    _id: '5978cb2518fd6daf0da062ae',
    title: 'SOCAL - HB Pier Southside Closeup',
    streamUrl: 'https://cams.cdn-surfline.com/cdn-wc/wc-hbpiersclose/playlist.m3u8',
    stillUrl: 'https://camstills.cdn-surfline.com/wc-hbpiersclose/latest_small.jpg',
    rewindBaseUrl: 'https://camrewinds.cdn-surfline.com/wc-hbpiersclose/wc-hbpiersclose',
    isPremium: false,
    status: {
      isDown: false,
      message: '',
    },
    control: 'https://camstills.cdn-surfline.com/wc-hbpiersclose/latest_small.jpg',
    nighttime: false,
    rewindClip:
      'https://camrewinds.cdn-surfline.com/wc-hbpiersclose/wc-hbpiersclose.1300.2018-12-02.mp4',
  },
];

describe('components / CamSelectorMenu', () => {
  const doSetPrimaryCam = sinon.stub();
  const doSetCamView = sinon.stub();

  const defaultProps = {
    isPremium: true,
    cameras,
    initialCameras: cameras,
    doSetPrimaryCam,
    doSetCamView,
    primaryCam: cameras[0],
    numCams: 2,
    device: {
      mobile: false,
      width: 1256,
    },
  };

  afterEach(() => {
    doSetPrimaryCam.reset();
    doSetCamView.reset();
  });

  it('Renders the CamSelectorMenu', () => {
    const wrapper = mount(<CamSelectorMenu {...defaultProps} />);
    const CamSelectorMenuInstance = wrapper.find(CamSelectorMenu);
    expect(CamSelectorMenuInstance).to.have.length(1);
  });

  it('Should show paywall for non premium users', () => {
    const props = {
      ...defaultProps,
      isPremium: false,
      device: {
        mobile: false,
        width: 1200,
      },
    };
    const wrapper = mount(<CamSelectorMenu {...props} />);

    const CamSelectorMenuItemInstance = wrapper.find('.quiver-cam-view-selector__option--multi');
    CamSelectorMenuItemInstance.simulate('mouseEnter');
    const CamViewSelectorPaywallInstance = wrapper.find(
      '.quiver-cam-view-selector__option--premium-paywall',
    );
    expect(CamViewSelectorPaywallInstance).to.have.length(1);
  });

  it('Should change the primaryCam when clicking a cam selector menu item', () => {
    const wrapper = mount(<CamSelectorMenu {...defaultProps} />);
    const CamSelectorMenuItemInstance = wrapper.find(CamSelectorMenuItem);
    CamSelectorMenuItemInstance.at(1).simulate('click');
    expect(doSetCamView).to.have.been.calledOnceWithExactly('SINGLE', cameras[1]);
  });

  it('Should open the fullscreen menu when clicking a cam view selector on mobile', () => {
    const props = {
      ...defaultProps,
      device: {
        mobile: true,
        width: 400,
      },
    };
    const mobile = mount(<CamSelectorMenu {...props} />);
    const CamViewSelectorMultiInstance = mobile.find('.quiver-cam-view-selector__option--multi');
    CamViewSelectorMultiInstance.simulate('click');
    expect(mobile.state('menuOpen')).to.equal(true);
    expect(mobile.state('menuType')).to.equal('MULTI');
  });

  it('Should change the active view and primary when a user clicks a cam in the menu on mobile', () => {
    const props = {
      ...defaultProps,
      device: {
        mobile: true,
        width: 400,
      },
    };
    const mobile = mount(<CamSelectorMenu {...props} />);

    // Open the menu
    const CamViewSelectorMultiInstance = mobile.find('.quiver-cam-view-selector__option--multi');
    CamViewSelectorMultiInstance.simulate('click');
    expect(mobile.state('menuOpen')).to.equal(true);
    expect(mobile.state('menuType')).to.equal('MULTI');

    const CamSelectorMenuItemInstance = mobile.find(CamSelectorMenuItem);
    CamSelectorMenuItemInstance.at(1).simulate('click');
    expect(mobile.state('menuOpen')).to.equal(false);
    expect(mobile.state('menuType')).to.equal('MULTI');
  });

  it('Should not change the active view if the user closes the mobile menu', () => {
    const props = {
      ...defaultProps,
      device: {
        mobile: true,
        width: 400,
      },
    };
    const mobile = mount(<CamSelectorMenu {...props} />);

    // Open the menu
    const CamViewSelectorMultiInstance = mobile.find('.quiver-cam-view-selector__option--multi');
    CamViewSelectorMultiInstance.simulate('click');
    expect(mobile.state('menuOpen')).to.equal(true);
    expect(mobile.state('menuType')).to.equal('MULTI');

    // Close the menu
    const CloseIconInstance = mobile.find(CloseIcon);
    CloseIconInstance.simulate('click');
    expect(mobile.state('menuOpen')).to.equal(false);
    expect(mobile.state('menuType')).to.equal('MULTI');
  });
});
