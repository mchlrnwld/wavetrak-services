import { expect } from 'chai';
import sinon from 'sinon';
import { CamPlayerV2 } from '@surfline/quiver-react';
import { shallowWithReduxAndRouter } from '../../utils/test-utils';
import * as useSpotIsFavorite from '../../hooks/useSpotIsFavorite';
import SessionClipPlayer from './SessionClipPlayer';

describe('components / SessionClipPlayer', () => {
  let useSpotIsFavoriteStub;

  beforeEach(() => {
    useSpotIsFavoriteStub = sinon.stub(useSpotIsFavorite, 'useSpotIsFavorite');
  });

  afterEach(() => {
    useSpotIsFavoriteStub.restore();
  });

  const SESSION = { spot: { id: '123', name: 'jeremys desk' } };
  const CLIP = {
    clipId: 'clipId',
    clipUrl: 'clipUrl',
    thumbnail: { url: '{size}', sizes: [null, null, '1920x1080'] },
  };
  const CLIP_NO_STILL = {
    clipId: 'clipId',
    clipUrl: 'clipUrl',
    thumbnail: {},
  };

  const EMPTY_CLIP = {};

  const CAMERA = {
    id: 'camId',
    title: 'tilte',
  };

  it('should render the CamPlayer', () => {
    useSpotIsFavoriteStub.returns(true);
    const { wrapper } = shallowWithReduxAndRouter(SessionClipPlayer, {
      session: SESSION,
      camera: CAMERA,
      clip: CLIP,
    });

    expect(useSpotIsFavoriteStub).to.have.been.calledOnceWithExactly(SESSION.spot.id);

    expect(wrapper.find(CamPlayerV2)).to.have.length(1);
    expect(wrapper.find(CamPlayerV2).props()).to.deep.equal({
      playerId: CLIP.clipId,
      camera: {
        ...CAMERA,
        _id: CAMERA.id,
        streamUrl: CLIP.clipUrl,
        stillUrl: '1920x1080',
        status: {
          isDown: false,
          message: "We're sorry we are still working on generating this clip.",
          subMessage: 'Clip is not available yet.',
          altMessage: 'Clip is not available yet.',
        },
      },
      spotName: SESSION.spot.name,
      spotId: SESSION.spot.id,
      isClip: true,
      location: 'Sessions Clip Player',
      isFavorite: true,
    });
  });

  it('should handle a clip with no still', () => {
    useSpotIsFavoriteStub.returns(false);
    const { wrapper } = shallowWithReduxAndRouter(SessionClipPlayer, {
      session: SESSION,
      camera: CAMERA,
      clip: CLIP_NO_STILL,
    });

    expect(useSpotIsFavoriteStub).to.have.been.calledOnceWithExactly(SESSION.spot.id);

    expect(wrapper.find(CamPlayerV2)).to.have.length(1);

    expect(wrapper.find(CamPlayerV2).props()).to.deep.equal({
      playerId: CLIP.clipId,
      camera: {
        ...CAMERA,
        _id: CAMERA.id,
        streamUrl: CLIP.clipUrl,
        stillUrl: undefined,
        status: {
          isDown: false,
          message: "We're sorry we are still working on generating this clip.",
          subMessage: 'Clip is not available yet.',
          altMessage: 'Clip is not available yet.',
        },
      },
      spotName: SESSION.spot.name,
      spotId: SESSION.spot.id,
      isClip: true,
      location: 'Sessions Clip Player',
      isFavorite: false,
    });
  });

  it('should handle an empty clip', () => {
    useSpotIsFavoriteStub.returns(false);
    const { wrapper } = shallowWithReduxAndRouter(SessionClipPlayer, {
      session: SESSION,
      camera: CAMERA,
      clip: EMPTY_CLIP,
    });

    expect(useSpotIsFavoriteStub).to.have.been.calledOnceWithExactly(SESSION.spot.id);

    expect(wrapper.find(CamPlayerV2)).to.have.length(1);

    expect(wrapper.find(CamPlayerV2).props()).to.deep.equal({
      playerId: 'sl-clip-player',
      camera: {
        ...CAMERA,
        _id: CAMERA.id,
        stillUrl: undefined,
        streamUrl: undefined,
        status: {
          isDown: true,
          message: "We're sorry we are still working on generating this clip.",
          subMessage: 'Clip is not available yet.',
          altMessage: 'Clip is not available yet.',
        },
      },
      spotName: SESSION.spot.name,
      spotId: SESSION.spot.id,
      isClip: true,
      location: 'Sessions Clip Player',
      isFavorite: false,
    });
  });

  it('should handle no clip', () => {
    useSpotIsFavoriteStub.returns(false);
    const { wrapper } = shallowWithReduxAndRouter(SessionClipPlayer, {
      session: SESSION,
      camera: CAMERA,
    });

    expect(useSpotIsFavoriteStub).to.have.been.calledOnceWithExactly(SESSION.spot.id);

    expect(wrapper.find(CamPlayerV2)).to.have.length(0);
  });
});
