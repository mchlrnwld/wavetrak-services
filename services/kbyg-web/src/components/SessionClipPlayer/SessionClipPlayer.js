import React from 'react';
import PropTypes from 'prop-types';
import { CamPlayerV2 } from '@surfline/quiver-react';

import { useSpotIsFavorite } from '../../hooks/useSpotIsFavorite';
import { sessionPropType } from '../../propTypes/session';

import styles from './SessionClipPlayer.module.scss';

const SessionClipPlayer = ({ session, camera, clip }) => {
  const { spot } = session;
  const createCameraObject = () => ({
    ...camera,
    _id: camera.id,
    streamUrl: clip.clipUrl,
    stillUrl: clip.thumbnail?.url?.replace('{size}', clip.thumbnail.sizes[2]),
    status: {
      isDown: !clip.clipUrl,
      message: "We're sorry we are still working on generating this clip.",
      subMessage: 'Clip is not available yet.',
      altMessage: 'Clip is not available yet.',
    },
  });

  const isFavorite = useSpotIsFavorite(spot.id);

  return (
    <div className={styles.clipPlayer}>
      {clip && (
        <CamPlayerV2
          playerId={clip?.clipId || 'sl-clip-player'}
          key={clip?.clipId || 'sl-clip-player'}
          camera={createCameraObject()}
          spotName={spot.name}
          spotId={spot.id}
          isClip
          location="Sessions Clip Player"
          isFavorite={isFavorite}
        />
      )}
    </div>
  );
};

SessionClipPlayer.propTypes = {
  session: sessionPropType.isRequired,
  clip: PropTypes.shape({
    clipId: PropTypes.string,
    clipUrl: PropTypes.string,
    thumbnail: PropTypes.shape({
      url: PropTypes.string,
      sizes: PropTypes.arrayOf(PropTypes.string),
    }),
  }),
  camera: PropTypes.shape({
    title: PropTypes.string,
    alias: PropTypes.string,
    stillUrl: PropTypes.string,
    isPremium: PropTypes.bool,
    visible: PropTypes.bool,
    id: PropTypes.string,
  }),
};

SessionClipPlayer.defaultProps = {
  clip: null,
  camera: {},
};

export default SessionClipPlayer;
