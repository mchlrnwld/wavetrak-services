import React from 'react';
import PropTypes from 'prop-types';
import { mapDegreesCardinal } from '@surfline/web-common';
import { isNumber } from 'lodash';

/**
 * @typedef {object} Props
 * @property {number} [direction]
 */

/** @type {React.FunctionComponent<Props>} */
const BuoyDirection = ({ direction }) => {
  const directionString = isNumber(direction)
    ? `${mapDegreesCardinal(direction)} ${direction}º`
    : '- º';

  return <span className="sl-buoy-direction">{directionString}</span>;
};

BuoyDirection.propTypes = {
  direction: PropTypes.number,
};

BuoyDirection.defaultProps = {
  direction: null,
};

export default BuoyDirection;
