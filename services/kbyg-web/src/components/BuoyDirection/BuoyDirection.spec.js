import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';

import BuoyDirection from './BuoyDirection';

describe('components / BuoyDirection', () => {
  it('renders S if direction is 182', () => {
    const wrapper = mount(<BuoyDirection direction={182} />);
    const direction = wrapper.find('.sl-buoy-direction');
    expect(direction.text()).to.equal('S 182º');
  });

  it('renders dash if direction is null', () => {
    const wrapper = mount(<BuoyDirection />);
    const direction = wrapper.find('.sl-buoy-direction');
    expect(direction.text()).to.equal('- º');
  });
});
