import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { area, curveCardinal } from 'd3-shape';
import { scaleTime, scaleLinear } from 'd3-scale';
import { select } from 'd3-selection';
import { extent } from 'd3-array';
import { getWindow } from '@surfline/web-common';

class SwellTrend extends Component {
  componentDidMount() {
    const win = getWindow();
    this.renderGraph();
    win.addEventListener('resize', this.resizeListener);
  }

  componentDidUpdate(props) {
    const { data } = this.props;
    if (props.data !== data) this.renderGraph();
  }

  componentWillUnmount() {
    const win = getWindow();
    win.removeEventListener('resize', this.resizeListener);
  }

  resizeListener = () => {
    if (this.resizeTimeout) return;
    this.resizeTimeout = setTimeout(() => {
      this.resizeTimeout = null;
      this.renderGraph();
    }, 66);
  };

  renderGraph = () => {
    const { data, parentRef, width, height } = this.props;

    const chartWidth = width || parentRef.scrollWidth;
    const chartHeight = height || parentRef.offsetHeight;
    let max = 0;
    let { min } = data[0][0].surf;
    const swellData = data
      .map((day) =>
        day
          .map((dataPoint, dayNumber) => {
            if (dataPoint.surf.max > max) {
              max = dataPoint.surf.max;
            }
            if (dataPoint.surf.min < min) {
              min = dataPoint.surf.min;
            }
            if (dayNumber % 2 !== 0) return null;
            return {
              dateTime: new Date(dataPoint.timestamp * 1000),
              surfMax: dataPoint.surf.max,
            };
          })
          .filter((dataPoint) => !!dataPoint),
      )
      .reduce((acc, arr) => [...acc, ...arr], []);

    /**
     * Provide a bit of extra room top so that the date isn't covering the graph
     */
    const maxYDomain = max;
    const minYDomain = min > 1 ? min - 1 : 0;

    select('.sl-swell-trend-graph__area').remove();

    // Get the full width of the chart selector from the parent ref
    const chartSelectorWidth = chartWidth;
    // Define the x-axis
    const x = scaleTime()
      .rangeRound([0, chartSelectorWidth])
      .domain(extent(swellData, (d) => d.dateTime));

    /**
     * Define the y-axis for Area path and line
     */
    const yScalePadding = 0.4 * chartHeight;
    const y = scaleLinear()
      .rangeRound([chartHeight, yScalePadding])
      .domain([minYDomain, maxYDomain]);

    // Define the root swell trend SVG.
    const canvas = select('.sl-swell-trend-mobile .sl-swell-trend-graph__canvas');
    const svg = canvas
      .append('svg')
      .attr('class', 'sl-swell-trend-graph__area')
      .attr('width', chartSelectorWidth);

    // Define the area below the line.
    const swellArea = area()
      .x((d) => x(d.dateTime))
      .y0(y(minYDomain))
      .y1((d) => y(d.surfMax))
      .curve(curveCardinal);

    // Append the Area under the line.
    svg
      .append('path')
      .attr('class', 'sl-swell-trend-graph__area__fill')
      .attr('d', swellArea(swellData));
  };

  render() {
    return (
      <div className="sl-swell-trend-mobile">
        <div className="sl-swell-trend-graph__canvas" />
      </div>
    );
  }
}

SwellTrend.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.arrayOf(
      PropTypes.shape({
        timestamp: PropTypes.number,
        surf: PropTypes.shape({
          max: PropTypes.number,
          min: PropTypes.number,
        }),
      }),
    ),
  ).isRequired,
  parentRef: PropTypes.shape({
    scrollWidth: PropTypes.number,
    offsetHeight: PropTypes.number,
  }).isRequired,
  width: PropTypes.number,
  height: PropTypes.number,
};

SwellTrend.defaultProps = {
  width: null,
  height: null,
};

export default SwellTrend;
