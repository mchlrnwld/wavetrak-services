import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import SubHeader from './SubHeader';

const eventProperties = { test: true };

describe('components / SubHeader', () => {
  it('should have text', () => {
    const wrapper = shallow(<SubHeader eventProperties={eventProperties} />);
    const text = wrapper.find('.sl-sub-header__text');
    expect(text).to.have.length(1);
  });

  it('should have a link', () => {
    const wrapper = shallow(<SubHeader eventProperties={eventProperties} />);
    const link = wrapper.find('.sl-sub-header__link');
    expect(link).to.have.length(1);
  });
});
