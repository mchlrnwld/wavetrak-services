import PropTypes from 'prop-types';
import React, { useRef } from 'react';
import { TrackableLink } from '@surfline/quiver-react';
import LockIcon from '../LockIcon';
import en from '../../intl/translations/en';
import config from '../../config';
import { CLICKED_SUBSCRIBE_CTA } from '../../common/constants';

const SubHeader = React.memo(({ eventProperties }) => {
  const linkRef = useRef();
  return (
    <div className="sl-sub-header">
      <div className="sl-sub-header__icon">
        <LockIcon />
      </div>
      <div className="sl-sub-header__text">
        {en.premiumCTA.SubHeader.textBefore}
        <span className="sl-sub-header__text--hidden">{en.premiumCTA.SubHeader.text}</span>
        {en.premiumCTA.SubHeader.textAfter}
      </div>
      <TrackableLink
        ref={linkRef}
        eventName={CLICKED_SUBSCRIBE_CTA}
        eventProperties={eventProperties}
      >
        <a ref={linkRef} className="sl-sub-header__link" href={config.funnelUrl}>
          {en.premiumCTA.SubHeader.linkText}
        </a>
      </TrackableLink>
    </div>
  );
});

SubHeader.displayName = 'SubHeader';

SubHeader.propTypes = {
  eventProperties: PropTypes.oneOfType([PropTypes.shape(), PropTypes.func]).isRequired,
};

export default SubHeader;
