import React, { useMemo, memo } from 'react';
import classNames from 'classnames';

import BuoyCardPlaceholder from '../BuoyCardPlaceholder';

import styles from './BuoyCardPlaceholders.module.scss';

const getClass = (loading?: boolean, error?: boolean, noBuoysFound?: boolean, rootClass?: string) =>
  classNames(
    'sl-buoy-card-placeholders',
    {
      [styles.placeholders]: true,
      [styles.placeholdersLoading]: loading,
      [styles.placeholdersError]: error,
      [styles.placeholdersNoBuoys]: noBuoysFound,
    },
    rootClass,
  );

interface Props {
  numPlaceholders?: number;
  largeOnly?: boolean;
  loading?: boolean;
  noBuoysFound?: boolean;
  error?: Error | boolean | string;
  classes?: {
    root?: string;
    placeholder?: string;
  };
}

const BuoyCardPlaceholders: React.FC<Props> = ({
  numPlaceholders = 12,
  largeOnly,
  loading,
  error,
  noBuoysFound,
  classes = {},
}) => {
  const placeholders = useMemo(
    () => [...Array(numPlaceholders)].map((_, index) => ({ id: index, isPlaceholder: true })),
    [numPlaceholders],
  );

  return (
    <ul className={getClass(loading, !!error, noBuoysFound, classes.root)}>
      {placeholders.map((placeholder) => (
        <li key={placeholder.id}>
          <BuoyCardPlaceholder
            classes={{ root: classes.placeholder }}
            largeOnly={largeOnly}
            loading={loading}
          />
        </li>
      ))}
    </ul>
  );
};

export default memo(BuoyCardPlaceholders);
