import React from 'react';

import { mount } from 'enzyme';
import { expect } from 'chai';

import BuoyCardPlaceholders from './BuoyCardPlaceholders';
import BuoyCardPlaceholder from '../BuoyCardPlaceholder';

describe('components / BuoyCardPlaceholders', () => {
  it('should render a given number of placeholders', () => {
    const wrapper = mount(<BuoyCardPlaceholders numPlaceholders={2} />);

    expect(wrapper.find(BuoyCardPlaceholder)).to.have.length(2);
  });
});
