import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classnames from 'classnames';
import { format } from 'date-fns';
import { getLocalDate } from '@surfline/web-common';
import { chartSegmentPropsPropTypes, chartsPropTypes } from '../../propTypes/charts';
import scrubberStatePropTypes from '../../propTypes/scrubberstate';
import ChartScrubberPaywall from '../ChartScrubberPaywall';
import ChartPlayerControls from '../ChartPlayerControls';
import overviewDataPropTypes from '../../propTypes/subregionOverviewData';

const getActiveScrubberItemClasses = (index, activeIndex, paywalled) =>
  classnames({
    'sl-chart-scrubber__list__item': true,
    'sl-chart-scrubber__list__item--active': index === activeIndex,
    'sl-chart-scrubber__list__item--paywalled': paywalled,
  });

class ChartScrubber extends Component {
  chartScrubberTimestepRef = [];

  render() {
    const {
      activeIndex,
      charts,
      isPlaying,
      jumpToIndex,
      utcOffset,
      paywall,
      timestepsAvailable,
      scrubberState,
      toggleIsPlaying,
      overviewData,
      displayChartScrubberDayBorder,
      chartSegmentProps,
    } = this.props;
    return (
      <div className="sl-chart-scrubber">
        {charts.images.images.length ? (
          <div>
            {charts.active.type !== 'nearshoresst' && charts.active.type !== 'regionalsst' ? (
              <ul className="sl-chart-scrubber__list">
                {charts.images.images.map((image, index) => {
                  const paywalled = paywall && index >= timestepsAvailable;
                  return (
                    <li
                      key={`${image.timestamp || index}-desktop`}
                      className={getActiveScrubberItemClasses(index, activeIndex, paywalled)}
                      style={{
                        borderLeft: `1px solid ${
                          displayChartScrubberDayBorder(charts.images.images, index, utcOffset) &&
                          !paywalled
                            ? 'white'
                            : null
                        }`,
                      }}
                      onMouseEnter={() => jumpToIndex(index, null, paywalled)}
                      ref={(el) => {
                        this.chartScrubberTimestepRef[index] = el;
                      }}
                    />
                  );
                })}
                {paywall && scrubberState.paywallWidth ? (
                  <ChartScrubberPaywall
                    className="sl-chart-scrubber__list__paywall"
                    scrubberState={scrubberState}
                    jumpToIndex={jumpToIndex}
                    timestepsAvailable={timestepsAvailable}
                    overviewData={overviewData}
                    chartSegmentProps={chartSegmentProps}
                  />
                ) : null}
              </ul>
            ) : null}
            <div className="sl-chart-scrubber__date">
              {charts.active.type !== 'nearshore' && charts.images.images[0].timestamp ? (
                <div>
                  {format(
                    getLocalDate(charts.images.images[activeIndex].timestamp, utcOffset),
                    'iii MMM do | h:mma ',
                  )}
                  {charts.images.timezone}
                </div>
              ) : null}
              {charts.active.type !== 'nearshoresst' && charts.active.type !== 'regionalsst' ? (
                <ChartPlayerControls
                  className="sl-chart-scrubber__controls"
                  overviewData={overviewData}
                  jumpToIndex={jumpToIndex}
                  toggleIsPlaying={toggleIsPlaying}
                  isPlaying={isPlaying}
                  activeIndex={activeIndex}
                  chartSegmentProps={chartSegmentProps}
                />
              ) : null}
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

ChartScrubber.propTypes = {
  scrubberState: scrubberStatePropTypes.isRequired,
  activeIndex: PropTypes.number.isRequired,
  charts: chartsPropTypes.isRequired,
  jumpToIndex: PropTypes.func.isRequired,
  isPlaying: PropTypes.bool.isRequired,
  utcOffset: PropTypes.number,
  paywall: PropTypes.bool.isRequired,
  timestepsAvailable: PropTypes.number.isRequired,
  toggleIsPlaying: PropTypes.func.isRequired,
  overviewData: overviewDataPropTypes.isRequired,
  displayChartScrubberDayBorder: PropTypes.func.isRequired,
  chartSegmentProps: chartSegmentPropsPropTypes.isRequired,
};

ChartScrubber.defaultProps = {
  utcOffset: null,
};

export default ChartScrubber;
