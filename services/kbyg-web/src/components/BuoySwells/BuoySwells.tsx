import React, { useMemo } from 'react';
import classNames from 'classnames';

import BuoySwell from '../BuoySwell';

import type { BuoySwell as IBuoySwell } from '../../types/buoys';
import { SurfHeight } from '../../types/units';

import styles from './BuoySwells.module.scss';

const getClass = (offline: boolean, largeOnly: boolean) =>
  classNames({
    [styles.buoySwells]: true,
    [styles.buoySwellsLargeOnly]: largeOnly,
    [styles.buoySwellsOffline]: offline,
  });

interface Props {
  swells?: IBuoySwell[];
  units: SurfHeight;
  largeOnly?: boolean;
  offline?: boolean;
}

const swellImpact = (swell: IBuoySwell) =>
  swell.period && swell.height ? swell.height * (0.1 * swell.period) : 0;

export const getTop3Swells = (swells: IBuoySwell[]) =>
  [...swells]
    .map((swell) => ({ ...swell, impact: swellImpact(swell) }))
    .sort((a, b) => a.impact - b.impact)
    .reverse()
    .slice(0, 3);

const BuoySwells: React.FC<Props> = ({
  units,
  largeOnly = false,
  offline = false,
  swells = [],
}) => {
  const top3Swells = useMemo(() => getTop3Swells(swells), [swells]);

  if (offline && !largeOnly) {
    return null;
  }

  return (
    <ul className={getClass(offline, largeOnly)}>
      {largeOnly && <h2 className={styles.buoySwellsTitle}>Individual Swells</h2>}
      {!offline
        ? top3Swells.map((swell) => (
            <li
              className={styles.buoySwellsItem}
              key={`${swell.height}-${swell.period}-${swell.direction}`}
            >
              <BuoySwell
                height={swell.height}
                period={swell.period}
                direction={swell.direction}
                units={units}
              />
            </li>
          ))
        : ['No Data', '-', '-'].map((text, i) => (
            // Disable the key lint error since it's a static array
            // eslint-disable-next-line react/no-array-index-key
            <li className={styles.buoySwellsOfflineItem} key={`${text}-${i}`}>
              <p>{text}</p>
            </li>
          ))}
    </ul>
  );
};

export default BuoySwells;
