import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';

import BuoySwells from './BuoySwells';
import BuoySwell from '../BuoySwell/BuoySwell';

const swells = [
  {
    height: 1.2,
    period: 20,
    direction: 120,
  },
  {
    height: 100,
    period: undefined,
    direction: 360,
  },
  { height: 0.1, period: 0.1, direction: 0.1 },
  { height: 10, period: 12, direction: 150 },
  { height: 0.1, period: 0.1, direction: 0.1 },
  { height: undefined, period: undefined, direction: undefined },
];

describe('components / BuoySwells', () => {
  it('should render a list of the top 3 swells', () => {
    const wrapper = mount(<BuoySwells swells={swells} units="FT" />);

    const swellNodes = wrapper.find(BuoySwell);

    expect(swellNodes).to.have.length(3);
    expect(swellNodes.at(0).props()).to.deep.equal({ ...swells[3], units: 'FT' });
    expect(swellNodes.at(1).props()).to.deep.equal({ ...swells[0], units: 'FT' });
    expect(swellNodes.at(2).props()).to.deep.equal({ ...swells[2], units: 'FT' });
  });

  it('should be an empty render for offline and not largeOnly', () => {
    const wrapper = mount(<BuoySwells offline units="FT" />);

    expect(wrapper.isEmptyRender()).to.be.true();
  });

  it('should render empty swell entries for offline and largeOnly', () => {
    const wrapper = mount(<BuoySwells offline largeOnly units="FT" />);

    expect(wrapper.find('.buoySwellsOffline')).to.have.length(1);
    const noDataSwells = wrapper.find('.buoySwellsOfflineItem');
    expect(noDataSwells).to.have.length(3);
    expect(noDataSwells.at(0).text()).to.be.equal('No Data');
    expect(noDataSwells.at(1).text()).to.be.equal('-');
    expect(noDataSwells.at(2).text()).to.be.equal('-');
  });
});
