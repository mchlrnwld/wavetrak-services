import PropTypes from 'prop-types';
import React, { useRef, useMemo } from 'react';
import classNames from 'classnames';
import { FavoritesIndicator, TrackableLink } from '@surfline/quiver-react';
import resolveBreadcrumbs from '../../utils/urls/breadcrumbResolver';
import Breadcrumb from '../Breadcrumb';
import ForecastHeaderMenu from '../ForecastHeaderMenu';
import breadcrumbsPropTypes from '../../propTypes/breadcrumbs';
import userSettingsPropTypes from '../../propTypes/userSettings';
import spotPropTypes from '../../propTypes/spot';
import linkPropTypes, { pageLevelLinkPropTypes } from '../../propTypes/link';
import nearbySubregionsPropTypes from '../../propTypes/nearbySubregions';

const getClassName = (active) =>
  classNames({
    'sl-forecast-header__nav__page-level__link': true,
    'sl-forecast-header__nav__page-level__link--active': active,
  });

const isMultiPageSingleTabActive = (itemPath, locationPath, isMultiPage) => {
  if (!locationPath || !itemPath) return false;
  const { 1: rootPath } = locationPath.split('/');
  return isMultiPage && itemPath.indexOf(rootPath) !== -1 && locationPath.indexOf(rootPath) !== -1;
};

const PageLevelLink = React.memo(({ link, pathName, isMultiPage, spotPage }) => {
  const linkRef = useRef();
  const eventProperties = useMemo(
    () => ({
      linkUrl: link.path,
      linkName: link.text,
      linkLocation: spotPage ? 'spot forecast navigation' : 'regional forecast navigation',
      category: spotPage ? 'spot forecast' : 'regional forecast',
    }),
    [link.path, link.text, spotPage],
  );
  return (
    <TrackableLink ref={linkRef} eventName="Clicked Link" eventProperties={eventProperties}>
      <a
        ref={linkRef}
        href={link.path}
        key={link.text}
        className={getClassName(
          link.path === pathName || isMultiPageSingleTabActive(link.path, pathName, isMultiPage),
        )}
      >
        <div className="sl-forecast-header__nav__page-level__link__text">{link.text}</div>
      </a>
    </TrackableLink>
  );
});

PageLevelLink.displayName = 'PageLevelLink';

const ForecastHeader = ({
  breadcrumb,
  pageTitle,
  pageLevelLinks,
  pathName,
  externalLinks1,
  externalLinks2,
  nearbySpots,
  showFavorite,
  spotPage,
  userSettings,
  handleFavoriteClick,
  favorited,
  isMultiPage,
  nearbySubregions,
}) => (
  <div className="sl-forecast-header">
    <div className="sl-forecast-header__breadcrumb">
      <Breadcrumb breadcrumbs={resolveBreadcrumbs(breadcrumb)} />
    </div>
    <div className="sl-forecast-header__main">
      <h1 className="sl-forecast-header__main__title">{pageTitle}</h1>
      {showFavorite ? (
        <div className="sl-forecast-header__main__favorite">
          <button
            type="button"
            onClick={() => handleFavoriteClick && handleFavoriteClick(favorited)}
          >
            <FavoritesIndicator favorited={favorited} showText />
          </button>
        </div>
      ) : null}
    </div>
    <div className="sl-forecast-header__nav">
      <div className="sl-forecast-header__nav__page-level">
        {pageLevelLinks.map((link) => (
          <PageLevelLink
            key={link?.path}
            isMultiPage={isMultiPage}
            link={link}
            pathName={pathName}
            spotPage={spotPage}
          />
        ))}
      </div>
      <div className="sl-forecast-header__nav__divider" />
      <ForecastHeaderMenu
        userSettings={userSettings}
        externalLinks1={externalLinks1}
        externalLinks2={externalLinks2}
        nearbySpots={nearbySpots}
        nearbySubregions={nearbySubregions}
        spotPage={spotPage}
        pathName={pathName}
      />
    </div>
  </div>
);

ForecastHeader.propTypes = {
  breadcrumb: PropTypes.arrayOf(breadcrumbsPropTypes).isRequired,
  pageTitle: PropTypes.string.isRequired,
  showFavorite: PropTypes.bool,
  spotPage: PropTypes.bool,
  pageLevelLinks: PropTypes.arrayOf(PropTypes.shape(pageLevelLinkPropTypes)).isRequired,
  externalLinks1: PropTypes.arrayOf(PropTypes.shape(linkPropTypes)),
  externalLinks2: PropTypes.arrayOf(PropTypes.shape(linkPropTypes)),
  nearbySpots: PropTypes.arrayOf(spotPropTypes),
  userSettings: userSettingsPropTypes,
  handleFavoriteClick: PropTypes.func,
  favorited: PropTypes.bool,
  pathName: PropTypes.string,
  isMultiPage: PropTypes.bool,
  nearbySubregions: nearbySubregionsPropTypes,
};

PageLevelLink.propTypes = {
  link: PropTypes.shape(linkPropTypes).isRequired,
  isMultiPage: PropTypes.bool,
  pathName: PropTypes.string,
  spotPage: PropTypes.bool,
};

ForecastHeader.defaultProps = {
  userSettings: null,
  nearbySpots: null,
  nearbySubregions: null,
  favorited: false,
  showFavorite: false,
  spotPage: false,
  externalLinks1: [],
  externalLinks2: [],
  handleFavoriteClick: null,
  pathName: null,
  isMultiPage: false,
};

PageLevelLink.defaultProps = {
  isMultiPage: false,
  spotPage: false,
  pathName: null,
};

export default ForecastHeader;
