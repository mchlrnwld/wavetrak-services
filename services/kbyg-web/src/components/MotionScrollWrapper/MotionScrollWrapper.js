import PropTypes from 'prop-types';
import React from 'react';
import { Motion, spring } from 'react-motion';
import scrollPositionPropType from '../../propTypes/animations/scrollPosition';

const MotionScrollWrapper = ({ children, scrollPosition, disable, onRest }) => {
  const { top, left } = scrollPosition;
  const style = {
    motionScrollTop: spring(top, { stiffness: 400, damping: 50 }),
    motionScrollLeft: spring(left, { stiffness: 400, damping: 50 }),
  };
  return (
    <div className="sl-motion-scroll-wrapper">
      <Motion style={style} onRest={onRest}>
        {(interpolatedStypes) =>
          disable
            ? children
            : React.cloneElement(children, {
                scrollTop: interpolatedStypes.motionScrollTop,
                scrollLeft: interpolatedStypes.motionScrollLeft,
                forceScrolling: true,
              })
        }
      </Motion>
    </div>
  );
};

MotionScrollWrapper.propTypes = {
  scrollPosition: scrollPositionPropType.isRequired,
  children: PropTypes.node.isRequired,
  disable: PropTypes.bool,
  onRest: PropTypes.func,
};

MotionScrollWrapper.defaultProps = {
  disable: false,
  onRest: () => null,
};

export default MotionScrollWrapper;
