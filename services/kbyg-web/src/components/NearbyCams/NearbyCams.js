import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserSettings } from '@surfline/web-common';

import CamList from '../CamList';
import MotionScrollWrapper from '../MotionScrollWrapper';

import { getCamListScrollPosition } from '../../selectors/animations';
import { getSpotMultiCam, getNearbySpots, getReverseSortBeachView } from '../../selectors/spot';

import { setCamListScrollPosition } from '../../actions/animations';

import scrollPositionPropType from '../../propTypes/animations/scrollPosition';
import userSettingsPropTypes from '../../propTypes/userSettings';

const NearbyCams = ({
  nearbySpots,
  camListScrollPosition,
  reverseSortBeachView,
  doSetCamListScrollPosition,
  userSettings,
  isPremium,
  spotId,
}) => (
  <div className="sl-spot-report-cam__nearby-cams">
    {nearbySpots ? (
      <MotionScrollWrapper scrollPosition={camListScrollPosition}>
        <CamList
          spots={reverseSortBeachView ? [...nearbySpots].reverse() : nearbySpots}
          spotId={spotId}
          doSetCamListScrollPosition={doSetCamListScrollPosition}
          units={userSettings.units.surfHeight}
          isPremium={isPremium}
        />
      </MotionScrollWrapper>
    ) : null}
  </div>
);

NearbyCams.defaultProps = {
  isPremium: false,
  nearbySpots: null,
  reverseSortBeachView: null,
  userSettings: null,
};

NearbyCams.propTypes = {
  isPremium: PropTypes.bool,
  userSettings: userSettingsPropTypes,
  spotId: PropTypes.string.isRequired,
  nearbySpots: PropTypes.arrayOf(PropTypes.shape()),
  reverseSortBeachView: PropTypes.bool,
  doSetCamListScrollPosition: PropTypes.func.isRequired,
  camListScrollPosition: scrollPositionPropType.isRequired,
};

export default connect(
  (state) => ({
    multiCam: getSpotMultiCam(state),
    nearbySpots: getNearbySpots(state),
    nearbySpotsError: !!state.spot.nearby.error,
    reverseSortBeachView: getReverseSortBeachView(state),
    userSettings: getUserSettings(state),
    camListScrollPosition: getCamListScrollPosition(state),
  }),
  (dispatch) => ({
    doSetCamListScrollPosition: (scrollPosition) =>
      dispatch(setCamListScrollPosition(scrollPosition)),
  }),
)(NearbyCams);
