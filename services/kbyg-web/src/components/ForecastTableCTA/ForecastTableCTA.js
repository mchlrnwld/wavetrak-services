import PropTypes from 'prop-types';
import React from 'react';
import ForecastCTAFeatures from '../ForecastCTAFeatures';
import ForecastCTAHeader from '../ForecastCTAHeader';
import ForecastCTAActionLink from '../ForecastCTAActionLink';
import ForecastCTAText from '../ForecastCTAText';
import en from '../../intl/translations/en';

const ForecastTableCTA = ({ category, pageName, spotId, subregionId }) => {
  const copy = en.premiumCTA.ForecastVisualizerCTA;
  return (
    <div className="sl-forecast-table-cta">
      <ForecastCTAHeader copy={copy.header} />
      <ForecastCTAText copy={copy.mainText} />
      <ForecastCTAActionLink
        category={category}
        pageName={pageName}
        spotId={spotId}
        subregionId={subregionId}
        location="table - bottom"
      />
      <ForecastCTAFeatures copy={copy.features} />
    </div>
  );
};

ForecastTableCTA.propTypes = {
  category: PropTypes.string.isRequired,
  pageName: PropTypes.string.isRequired,
  spotId: PropTypes.string.isRequired,
  subregionId: PropTypes.string,
};

ForecastTableCTA.defaultProps = {
  subregionId: null,
};

export default ForecastTableCTA;
