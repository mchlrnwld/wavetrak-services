import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ForecastHighlights from './ForecastHighlights';

describe('components / ForecastHighlights', () => {
  it('should render a li for each highlight', () => {
    const highlights = ['Highlight one', 'Highlight two', 'Highlight three'];
    const wrapper = shallow(<ForecastHighlights highlights={highlights} />);
    expect(wrapper.find('li')).to.have.length(3);
  });
});
