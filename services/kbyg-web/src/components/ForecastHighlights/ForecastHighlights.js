import PropTypes from 'prop-types';
import React from 'react';

const ForecastHighlights = ({ highlights }) => (
  <div className="sl-forecast-highlights">
    <p className="sl-forecast-highlights__header">Forecast Highlights</p>
    <ul className="sl-forecast-highlights__group">
      {highlights.map((highlight) => (
        <li key={highlight}>{highlight}</li>
      ))}
    </ul>
  </div>
);

ForecastHighlights.propTypes = {
  highlights: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default ForecastHighlights;
