import React from 'react';

// TODO: Do we want to implement this using a font?
const LocationArrow = () => (
  <svg className="sl-location-arrow-icon" viewBox="0 0 15 15" width="15" height="15">
    <polygon points="15,0 10,15 7,7 0,5" />
  </svg>
);

export default LocationArrow;
