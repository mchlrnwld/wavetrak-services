import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { getWindow } from '@surfline/web-common';
import ForecastCTAFeatures from '../ForecastCTAFeatures';
import ForecastCTAHeader from '../ForecastCTAHeader';
import ForecastCTAActionLink from '../ForecastCTAActionLink';
import ForecastCTAText from '../ForecastCTAText';
import CTAHackChecker from '../../utils/CTAHackChecker';
import en from '../../intl/translations/en';

class ForecastGraphsCTA extends PureComponent {
  componentDidMount() {
    const win = getWindow();
    const ctaHackConfig = {
      targetClasses: ['sl-forecast-graphs-cta', 'sl-chart-range-selector-cta'],
      backgroundTransMatch: '.925',
      hideContainerClass: 'sl-forecast-graphs',
      blurPxMatch: '6px',
    };
    this.ctaHackChecker = new CTAHackChecker(ctaHackConfig);
    this.ctaHackChecker.observe();
    this.ctaHackChecker.startDOMCheck();
    win.addEventListener('resize', this.resizeListener);
  }

  componentWillUnmount() {
    const win = getWindow();
    this.ctaHackChecker.disconnect();
    this.ctaHackChecker.stopDOMCheck();
    win.removeEventListener('resize', this.resizeListener);
  }

  resizeListener = () => {
    this.ctaHackChecker.disconnect();
    this.ctaHackChecker.stopDOMCheck();
    if (this.resizeTimeout) return;

    this.resizeTimeout = setTimeout(() => {
      this.resizeTimeout = null;
      this.ctaHackChecker.observe();
      this.ctaHackChecker.startDOMCheck();
    }, 3000);
  };

  render() {
    const { category, pageName, spotId, subregionId } = this.props;
    const copy = en.premiumCTA.ForecastVisualizerCTA;
    return (
      <div className="sl-forecast-graphs-cta">
        <ForecastCTAHeader copy={copy.header} />
        <ForecastCTAActionLink
          category={category}
          pageName={pageName}
          spotId={spotId}
          subregionId={subregionId}
          location="chart - top"
        />
        <ForecastCTAText copy={copy.mainText} />
        <ForecastCTAFeatures copy={copy.features} />
        <div className="sl-forecast-graphs-cta__bottom-container">
          <ForecastCTAActionLink
            category={category}
            pageName={pageName}
            spotId={spotId}
            subregionId={subregionId}
            location="chart - bottom"
            size="large"
          />
          <a href="/premium-benefits" className="sl-forecast-graphs-cta__learn-more-link">
            {copy.learnMoreText}
          </a>
        </div>
      </div>
    );
  }
}

ForecastGraphsCTA.propTypes = {
  category: PropTypes.string.isRequired,
  pageName: PropTypes.string.isRequired,
  spotId: PropTypes.string.isRequired,
  subregionId: PropTypes.string,
};

ForecastGraphsCTA.defaultProps = {
  subregionId: null,
};

export default ForecastGraphsCTA;
