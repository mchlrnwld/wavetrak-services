import React from 'react';
import PropTypes from 'prop-types';
import { upperCaseFirstLetter } from '../../utils/string';

const TravelGuideNotes = ({ hazards, boardTypes, access, bottom, bestSeason }) => {
  const notes = [
    {
      title: 'Hazards',
      description: hazards,
    },
    {
      title: 'Bring Your',
      description: boardTypes
        .map((board) => (board === 'SUP' ? board : upperCaseFirstLetter(board)))
        .join(', '),
    },
    {
      title: 'Access',
      description: access,
    },
    {
      title: 'Bottom',
      description: bottom,
    },
    {
      title: 'Best Season',
      description: bestSeason,
    },
  ];

  return notes.map(({ title, description }) => (
    <div key={title} className="sl-travel-guide__notes__item">
      <h3 className="sl-travel-guide__notes__item__title">{title}</h3>
      <p className="sl-travel-guide__notes__item__description">{description}</p>
    </div>
  ));
};

TravelGuideNotes.propTypes = {
  hazards: PropTypes.string.isRequired,
  boardTypes: PropTypes.arrayOf(PropTypes.string).isRequired,
  access: PropTypes.string.isRequired,
  bottom: PropTypes.string.isRequired,
  bestSeason: PropTypes.string.isRequired,
};

export default TravelGuideNotes;
