import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ForecastModelToggle from './ForecastModelToggle';
import { LOTUS } from '../../common/constants';

describe('components / ForecastModelToggle', () => {
  it('has only 1 active view state', () => {
    const wrapper = shallow(<ForecastModelToggle model={LOTUS} />);
    const inactiveLotus = wrapper.find('.sl-forecast-model-toggle__container__lotus');
    const activeLola = wrapper.find('.sl-forecast-model-toggle__container__lotus--active');
    expect(inactiveLotus).to.have.length(1);
    expect(activeLola).to.have.length(1);
  });
});
