/* eslint-disable jsx-a11y/click-events-have-key-events */
import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { LOLA, LOTUS } from '../../common/constants';

const toggleCircleClassName = (isLotus) =>
  classNames({
    'sl-forecast-model-toggle__container__toggle__bar__circle': true,
    'sl-forecast-model-toggle__container__toggle__bar__circle--lotus--on': isLotus,
  });

const toggleBarClassName = (isLotus) =>
  classNames({
    'sl-forecast-model-toggle__container__toggle__bar': true,
    'sl-forecast-model-toggle__container__toggle__bar--lotus--on': isLotus,
  });

const lotusClassNames = (isActive) =>
  classNames({
    'sl-forecast-model-toggle__container__lotus': true,
    'sl-forecast-model-toggle__container__lotus--active': isActive,
  });

const ForecastModelToggle = ({ model, onClick }) => {
  const isLotusOff = model === LOLA;

  return (
    <div className="sl-forecast-model-toggle">
      <h3>16-Day Forecast</h3>
      <div className="sl-forecast-model-toggle__container">
        <div
          className="sl-forecast-model-toggle__container__toggle"
          onClick={() => onClick(model === LOTUS ? LOLA : LOTUS)}
        >
          <div className={toggleBarClassName(!isLotusOff)}>
            <div className={toggleCircleClassName(!isLotusOff)} />
          </div>
        </div>
        <div className={lotusClassNames(model === LOTUS)}>
          LOTUS <i>BETA</i>
        </div>
      </div>
    </div>
  );
};

ForecastModelToggle.propTypes = {
  model: PropTypes.oneOf([LOLA, LOTUS]).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default ForecastModelToggle;
