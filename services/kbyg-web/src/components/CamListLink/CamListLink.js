import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { slugify, kbygPaths, trackEvent } from '@surfline/web-common';
import spotProptype from '../../propTypes/spot';
import WavetrakLink from '../WavetrakLink';

const { spotReportPath } = kbygPaths;

const getClassName = (active) =>
  classNames({
    'sl-cam-list-link': true,
    'sl-cam-list-link--active': active,
  });

const CamListLink = ({ spot, children, location, onMouseEnter, onMouseLeave, active }) => (
  <WavetrakLink
    href={spotReportPath(slugify(spot.name), spot._id)}
    className={getClassName(active)}
    onClick={() =>
      trackEvent('Clicked Nearby Spot', {
        spotId: spot._id,
        spotName: spot.name,
        clickedLocation: location,
      })
    }
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
    title={`${spot.name} Surf Report & Forecast`}
  >
    {children}
  </WavetrakLink>
);

CamListLink.defaultProps = {
  onMouseEnter: null,
  onMouseLeave: null,
  active: false,
};

CamListLink.propTypes = {
  spot: spotProptype.isRequired,
  children: PropTypes.node.isRequired,
  location: PropTypes.string.isRequired,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  active: PropTypes.bool,
};

export default CamListLink;
