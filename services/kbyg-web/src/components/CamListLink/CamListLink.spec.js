import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import CamListLink from './CamListLink';

describe('components / CamListLink', () => {
  const defaultSpot = {
    name: '',
    conditions: {},
    waveHeight: {},
    tide: {},
    wind: {},
    cameras: [],
  };
  it('sets active class if spot is active', () => {
    const inactiveWrapper = shallow(<CamListLink spot={defaultSpot} location="map" />);
    const activeWrapper = shallow(<CamListLink active spot={defaultSpot} location="map" />);
    expect(inactiveWrapper.prop('className')).not.to.contain('sl-cam-list-link--active');
    expect(activeWrapper.prop('className')).to.contain('sl-cam-list-link--active');
  });
});
