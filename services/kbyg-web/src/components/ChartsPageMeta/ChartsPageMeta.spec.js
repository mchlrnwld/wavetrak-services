import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import MetaTags from '../MetaTags';
import ChartsPageMeta from './ChartsPageMeta';

describe('components / ChartsPageMeta', () => {
  it('renders MetaTags with appropriate props', () => {
    const title = 'Northern California Wind Forecast - High Resolution NAM Chart - Surfline';
    const description =
      "High resolution Northern California wind chart showing HD wind speed and direction over time. These high resolution (HD) Northern California wind forecasts are from NOAA's NAM (North American Mesoscale Model). The NAM model often performs better than global models (i.e. the GFS) with small scale phenomena such as the sea breeze circulation and winds that interact with terrain.";
    const urlPath =
      'https://staging.surfline.com/surf-charts/regional-wind-hi-res/sf-san-mateo-county/58581a836630e24c44879010';

    const wrapper = shallow(
      <ChartsPageMeta type="hireswind" name="Northern California" urlPath={urlPath} />,
    );
    const metaTags = wrapper.find(MetaTags);

    expect(metaTags).to.have.length(1);
    expect(metaTags.prop('title')).to.equal(title);
    expect(metaTags.prop('description')).to.equal(description);
    expect(metaTags.prop('urlPath')).to.equal(urlPath);
  });
});
