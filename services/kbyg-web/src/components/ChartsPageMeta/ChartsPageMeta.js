import PropTypes from 'prop-types';
import React from 'react';
import MetaTags from '../MetaTags';

const META_DATA_TEMPLATE = (name) => ({
  localswell: {
    title: `${name} Wave Height Forecast Chart - Surfline`,
    description: `Check the latest ${name} wave height charts showing wave height and direction as it changes over time. Get the most accurate and trusted ${name} surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  localperiod: {
    title: `${name} Dominant Wave Period Chart - Surfline`,
    description: `Local dominant wave period chart for ${name} showing wave period intervals in seconds ranging from >10s to >20s, as they change over time. Get the most accurate and trusted ${name} surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  localwind: {
    title: `${name} Wind - Global Forecast System (GFS) Model - Surfline`,
    description: `Local GFS wind chart for ${name} showing wind speed and direction as it changes over the next few days. Get the most accurate and trusted ${name} surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  nearshoresst: {
    title: `${name} Water Temp & Sea Surface Temperature Chart (SST) - Surfline`,
    description: `Sea surface temperature (SST) chart for ${name} showing easy to read degrees and changes in water temperature. Get the most accurate and trusted ${name} surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  localhireswind: {
    title: `${name} Wind Forecast - High Resolution NAM Chart - Surfline`,
    description: `High resolution ${name} wind chart showing HD wind speed and direction over time. These high resolution (HD) ${name} wind forecasts are from NOAA's NAM (North American Mesoscale Model). The NAM model often performs better than global models (i.e. the GFS) with small scale phenomena such as the sea breeze circulation and winds that interact with terrain.`,
  },
  nearshore: {
    title: `${name} Nearshore Wave Forecast Model - Surfline`,
    description: `Nearshore wave charts for ${name} show upcoming swells forming at the shore. Check the ${name} nearshore chart before you surf this week, so you can know before you go.`,
  },
  regionalswell: {
    title: `${name} Wave Height Forecast Chart - Surfline`,
    description: `Check the latest ${name} wave height charts showing wave height and direction as it changes over time. Get the most accurate and trusted ${name} surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  regionalperiod: {
    title: `${name} Dominant Wave Period Chart - Surfline`,
    description: `Dominant wave period chart for ${name} showing wave period intervals in seconds ranging from >10s to >20s, as they change over time. Get the most accurate and trusted ${name} surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  hireswind: {
    title: `${name} Wind Forecast - High Resolution NAM Chart - Surfline`,
    description: `High resolution ${name} wind chart showing HD wind speed and direction over time. These high resolution (HD) ${name} wind forecasts are from NOAA's NAM (North American Mesoscale Model). The NAM model often performs better than global models (i.e. the GFS) with small scale phenomena such as the sea breeze circulation and winds that interact with terrain.`,
  },
  regionalwind: {
    title: `${name} Wind - Global Forecast System (GFS) Model - Surfline`,
    description: `Local GFS wind chart for ${name} showing wind speed and direction as it changes over the next few days. Get the most accurate and trusted ${name} surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  regionalsst: {
    title: `${name} Water Temp & Sea Surface Temperature Chart (SST) - Surfline`,
    description: `Sea surface temperature (SST) chart for ${name} showing easy to read degrees and changes in water temperature. Get the most accurate and trusted ${name} surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  globalwave: {
    title: 'Global Wave Height Forecast Chart - Surfline',
    description:
      'Check the latest global wave height charts showing wave height and direction as it changes over time. Get the most accurate and trusted surf forecasts, issued by our team of expert meteorologists at Surfline.',
  },
  globalperiod: {
    title: 'Global Dominant Wave Period Chart - Surfline',
    description:
      'Global Dominant wave period chart showing wave period intervals in seconds ranging from <10s to 20s, as they change over time. Get the most accurate and trusted global surf forecasts, issued by our team of expert meteorologists at Surfline.',
  },
  globalwind: {
    title: 'Global Wind - Global Forecast System (GFS) Model - Surfline',
    description:
      'Global GFS wind chart showing wind speed and direction as it changes over the next few days. Get the most accurate and trusted global surf forecasts, issued by our team of expert meteorologists at Surfline.',
  },
  globaleband10: {
    title: 'Global Dominant Wave Period Chart >10s - Surfline',
    description:
      'Global Dominant wave period chart showing wave period intervals in seconds ranging from 10s to 20s, as they change over time. Get the most accurate and trusted global surf forecasts, issued by our team of expert meteorologists at Surfline.',
  },
  globaleband13: {
    title: 'Global Dominant Wave Period Chart >13s - Surfline',
    description:
      'Global Dominant wave period chart showing wave period intervals in seconds ranging from 13s to 20s, as they change over time. Get the most accurate and trusted global surf forecasts, issued by our team of expert meteorologists at Surfline.',
  },
  globaleband16: {
    title: 'Global Dominant Wave Period Chart >16s - Surfline',
    description:
      'Global Dominant wave period chart showing wave period intervals in seconds ranging from 16s to 20s, as they change over time. Get the most accurate and trusted global surf forecasts, issued by our team of expert meteorologists at Surfline.',
  },
  globaleband20: {
    title: 'Global Dominant Wave Period Chart >20s - Surfline',
    description:
      'Global Dominant wave period chart showing wave period intervals >20s, as they change over time. Get the most accurate and trusted global surf forecasts, issued by our team of expert meteorologists at Surfline.',
  },
  basinwave: {
    title: `${name} Wave Height Forecast Chart - Surfline`,
    description: `Check the latest ${name} wave height charts showing wave height and direction as it changes over time. Get the most accurate and trusted surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  basinwind: {
    title: `${name} Wind - Global Forecast System (GFS) Model - Surfline`,
    description: `${name} GFS wind chart showing wind speed and direction as it changes over the next few days. Get the most accurate and trusted ${name} surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  basinperiod: {
    title: `${name} Dominant Wave Period Chart - Surfline`,
    description: `${name} dominant wave period chart showing wave period intervals in seconds ranging from 10s to >20s, as they change over time. Get the most accurate and trusted ${name} surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  basinswell10: {
    title: `${name} Dominant Wave Period Chart >10s - Surfline`,
    description: `${name} dominant wave period chart showing wave period intervals in seconds ranging from 10s to 20s, as they change over time. Get the most accurate and trusted surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  basinswell13: {
    title: `${name} Dominant Wave Period Chart >13s - Surfline`,
    description: `${name} dominant wave period chart showing wave period intervals in seconds ranging from 13s to 20s, as they change over time. Get the most accurate and trusted surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  basinswell16: {
    title: `${name} Dominant Wave Period Chart >16s - Surfline`,
    description: `${name} dominant wave period chart showing wave period intervals in seconds ranging from 16s to 20s, as they change over time. Get the most accurate and trusted surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
  basinswell20: {
    title: `${name} Dominant Wave Period Chart >20s - Surfline`,
    description: `${name} dominant wave period chart showing wave period intervals >20s, as they change over time. Get the most accurate and trusted surf forecasts, issued by our team of expert meteorologists at Surfline.`,
  },
});

const ChartsPageMeta = ({ type, name, urlPath }) => (
  <MetaTags
    title={META_DATA_TEMPLATE(name)[type].title}
    description={META_DATA_TEMPLATE(name)[type].description}
    urlPath={urlPath}
  />
);

ChartsPageMeta.propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  urlPath: PropTypes.string.isRequired,
};

export default ChartsPageMeta;
