import PropTypes from 'prop-types';
import React from 'react';
import breadcrumbProps from '../../propTypes/breadcrumbs';
import WavetrakLink from '../WavetrakLink';

const Breadcrumb = ({ baseUrl, breadcrumbs }) => (
  <ul className="sl-breadcrumbs">
    {breadcrumbs.map((breadcrumb) => (
      <li key={breadcrumb.name}>
        <WavetrakLink href={baseUrl ? `${baseUrl}${breadcrumb.url}` : breadcrumb.href}>
          {breadcrumb.name}
        </WavetrakLink>
      </li>
    ))}
  </ul>
);

Breadcrumb.propTypes = {
  breadcrumbs: PropTypes.arrayOf(breadcrumbProps).isRequired,
  baseUrl: PropTypes.string,
};

Breadcrumb.defaultProps = {
  baseUrl: '',
};

export default Breadcrumb;
