import { expect } from 'chai';
import Breadcrumb from './Breadcrumb';
import { mountWithReduxAndRouter } from '../../utils/test-utils';

describe('components / Breadcrumb', () => {
  const breadcrumbs = [
    {
      name: 'United States',
      href: '/surf-reports-forecasts-cams/us/6252001',
    },
    {
      name: 'California',
      href: '/surf-reports-forecasts-cams/us/ca/5332921',
    },
    {
      name: 'Orange County',
      href: '/surf-reports-forecasts-cams/us/ca/orange-county/5379524',
    },
    {
      name: 'Seal Beach',
      href: '/surf-reports-forecasts-cams/us/ca/orange-county/seal-beach/5394086',
    },
  ];

  it('renders bread crumbs', () => {
    const { wrapper: crumbs } = mountWithReduxAndRouter(Breadcrumb, { breadcrumbs });
    const ul = crumbs.find('ul');
    expect(ul.find('li').length).to.equal(4);

    const [crumb] = breadcrumbs;
    const linkName = crumb.name;
    const linkUrl = crumb.href;

    expect(crumbs.find('a')).to.have.length(4);
    expect(crumbs.find('a').first().text()).to.have.string(linkName);
    expect(crumbs.find('a').first().instance().href).to.contain(linkUrl);
  });
});
