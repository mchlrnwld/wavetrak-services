/* eslint-disable react/no-danger */
import PropTypes from 'prop-types';
import React from 'react';
import _get from 'lodash/get';
import locationViewPropShape from '../../propTypes/locationView';
import subregionsPropType from '../../propTypes/subregions';
import SpotListSiblings from '../SpotListSiblings';

const SpotListSummary = ({ locationView, subregions }) => {
  const getTravelLink = () =>
    locationView.taxonomy.associated.links.map((link) => {
      if (link.key === 'travel') {
        return (
          <div key={locationView.taxonomy.name} className="sl-spot-list-summary__beaches">
            <a href={link.href}>{`Surfing in ${locationView.taxonomy.name} Guide`}</a>
          </div>
        );
      }
      return null;
    });

  return (
    <div className="sl-spot-list-summary">
      {_get(locationView, 'travelContent.content.summary', null) ? (
        <div>
          <h3>{locationView.taxonomy.name} Surfing Insights</h3>
          <div className="sl-spot-list-summary__content-summary">
            <div dangerouslySetInnerHTML={{ __html: locationView.travelContent.content.summary }} />
          </div>
        </div>
      ) : null}
      {locationView.siblings && locationView.siblings.length
        ? ['subregion', 'map', 'travel'].map((type) => (
            <SpotListSiblings
              key={type}
              locationView={locationView}
              subregions={subregions}
              type={type}
            />
          ))
        : null}
      {_get(locationView, 'taxonomy.associated.links', null) ? getTravelLink() : null}
    </div>
  );
};

SpotListSummary.propTypes = {
  locationView: locationViewPropShape.isRequired,
  subregions: PropTypes.arrayOf(subregionsPropType),
};

SpotListSummary.defaultProps = {
  subregions: [],
};

export default SpotListSummary;
