import React from 'react';
import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { shallow } from 'enzyme';
import SpotListSummary from './SpotListSummary';
import { mountWithReduxAndRouter } from '../../utils/test-utils';

describe('components / SpotListSummary', () => {
  it('renders travel summary if content exists', () => {
    const props = deepFreeze({
      locationView: {
        taxonomy: {
          name: 'Huntington Beach',
        },
        travelContent: {
          content: {
            summary: 'Huntington Beach travel content summary',
          },
        },
      },
    });
    const wrapper = shallow(<SpotListSummary {...props} />);
    const summary = wrapper.find('.sl-spot-list-summary__content-summary');
    const h3 = wrapper.find('h3');

    expect(summary).to.have.length(1);
    expect(h3).to.have.length(1);
  });

  it('does not render travel summary if no content exists', () => {
    const props = deepFreeze({
      locationView: {
        taxonomy: {
          name: 'Huntington Beach',
        },
      },
    });
    const wrapper = shallow(<SpotListSummary {...props} />);
    const summary = wrapper.find('.sl-spot-list-summary__content-summary');
    const h3 = wrapper.find('h3');

    expect(summary).to.have.length(0);
    expect(h3).to.have.length(0);
  });

  it('renders nearby siblings', () => {
    const props = deepFreeze({
      locationView: {
        taxonomy: {
          name: 'Huntington Beach',
        },
        travelContent: {
          content: {
            summary: 'Huntington Beach travel content summary',
          },
        },
        siblings: [
          {
            _id: '1',
            name: 'Newport Beach',
            enumeratedPath:
              'Earth, North America, United States, California, Orange County, Newport Beach',
            associated: {
              links: [
                {
                  key: 'www',
                  href: '/one',
                },
                {
                  key: 'travel',
                  href: '/one',
                },
              ],
            },
          },
          {
            _id: '2',
            name: 'Laguna Beach',
            enumeratedPath:
              'Earth, North America, United States, California, Orange County, Laguna Beach',
            associated: {
              links: [
                {
                  key: 'www',
                  href: '/two',
                },
                {
                  key: 'travel',
                  href: '/two',
                },
              ],
            },
          },
          {
            _id: '3',
            name: 'Seal Beach',
            enumeratedPath:
              'Earth, North America, United States, California, Orange County, Seal Beach',
            associated: {
              links: [
                {
                  key: 'www',
                  href: '/three',
                },
                {
                  key: 'travel',
                  href: '/three',
                },
              ],
            },
          },
        ],
      },
    });

    const { wrapper } = mountWithReduxAndRouter(SpotListSummary, props);
    const siblingLinks = wrapper.find('a');

    expect(siblingLinks).to.have.length(6);
  });
});
