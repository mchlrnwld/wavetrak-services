import React from 'react';

const ChartIcon = () => (
  <svg width="52px" height="52px" viewBox="0 0 52 52">
    <defs>
      <rect id="path-1" x="0" y="0" width="44" height="44" />
    </defs>
    <g id="Desktop" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Group-23" transform="translate(2.000000, 2.000000)">
        <path
          d="M0,0 L0,48 L48,48 L48,0 L0,0 Z M-2,-2 L50,-2 L50,50 L-2,50 L-2,-2 Z"
          id="Rectangle-11-Copy"
          fill="#FFFFFF"
          fillRule="nonzero"
        />
        <g
          id="Lines-Copy-2"
          transform="translate(24.000000, 24.000000) scale(1, -1) translate(-24.000000, -24.000000) translate(2.000000, 2.000000)"
        >
          <mask id="mask-2" fill="white">
            <use xlinkHref="#path-1" />
          </mask>
          <g id="Mask" opacity="0" />
          <g id="Page-1" mask="url(#mask-2)" stroke="#22D737" strokeWidth="2">
            <g transform="translate(-710.000000, 5.000000)">
              <path
                d="M1172,352 C354.773905,330.789388 886.299356,-28.4325077 709.58353,1.80588095 C511.34984,35.7265286 517.359548,178.916622 37,107.078626"
                id="Stroke-179"
              />
              <path
                d="M1165,371 C331.135338,344.371467 880.037592,-23.1810908 701.863459,7.85914108 C514.063207,40.576682 493.052855,203.692174 26,119.293486"
                id="Stroke-185"
              />
              <path
                d="M1159,395 C303.926181,361.175781 875.338411,-17.2281135 695.445447,14.9380891 C521.768043,45.9925891 464.762559,235.541494 16,134.481934"
                id="Stroke-193"
              />
              <path
                d="M1153,410 C287.327815,372.533244 869.979206,-11.8288822 689.235949,20.9849695 C522.610478,51.2358894 447.624386,254.261835 8,144.739075"
                id="Stroke-197"
              />
              <path
                d="M1147,425 C270.733975,383.990854 864.583262,-5.51759218 683.026607,28.0482709 C523.420873,57.272662 430.491396,273.383472 0,155.687799"
                id="Stroke-201-Copy"
              />
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default ChartIcon;
