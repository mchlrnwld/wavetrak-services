import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';

import sinon, { SinonStub } from 'sinon';

import * as useDeviceSize from './useDeviceSize';
import BuoyCarouselArrows from './BuoyCarouselArrows';

describe('components / BuoyCarouselArrows', () => {
  let useDeviceSizeStub: SinonStub<[], { isDesktop: boolean }>;

  const scrollToStub = sinon.stub();

  const carouselRef = {
    scrollTo: scrollToStub,
    scrollLeft: 10,
    scrollWidth: 20,
    clientWidth: 2.5,
  };

  beforeEach(() => {
    useDeviceSizeStub = sinon
      .stub(useDeviceSize, 'default')
      .returns({ isDesktop: false, width: 0, height: 0, isMobile: true, isTablet: false });
  });

  afterEach(() => {
    useDeviceSizeStub.restore();
    scrollToStub.reset();
  });

  it('should not render anything if the carouselRef is not passed', () => {
    const wrapper = mount(<BuoyCarouselArrows />);

    expect(wrapper).to.be.empty();
  });

  it('should not render anything if disabled', () => {
    const wrapper = mount(<BuoyCarouselArrows disable />);

    expect(wrapper).to.be.empty();
  });

  it('should not render anything for a device smaller than tablet large', () => {
    useDeviceSizeStub.returns({ isDesktop: false });

    const wrapper = mount(<BuoyCarouselArrows disable />);

    expect(wrapper).to.be.empty();
  });

  it('should handle clicking the scroll buttons', () => {
    useDeviceSizeStub.returns({ isDesktop: true });

    const wrapper = mount(
      <BuoyCarouselArrows carouselRef={carouselRef as any as HTMLUListElement} />,
    );

    const left = wrapper.find('.buoyCarouselArrowLeft');
    const right = wrapper.find('.buoyCarouselArrowRight');

    expect(left).to.have.length(1);
    expect(right).to.have.length(1);

    right.simulate('click');
    expect(scrollToStub).to.have.been.calledOnceWithExactly({
      left: 12.5,
      behavior: 'smooth',
    });
    scrollToStub.reset();

    left.simulate('click');
    expect(scrollToStub).to.have.been.calledOnceWithExactly({
      left: 7.5,
      behavior: 'smooth',
    });
  });
});
