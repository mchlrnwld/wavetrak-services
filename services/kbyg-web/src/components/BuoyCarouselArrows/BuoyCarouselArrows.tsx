import React, { useCallback } from 'react';
import classNames from 'classnames';
import { Chevron } from '@surfline/quiver-react';

import useDeviceSize from './useDeviceSize';

import styles from './BuoyCarouselArrows.module.scss';

const arrowClass = (direction: 'left' | 'right', disabled?: boolean) =>
  classNames({
    [styles.buoyCarouselArrow]: true,
    [styles.buoyCarouselArrowLeft]: direction === 'left',
    [styles.buoyCarouselArrowRight]: direction === 'right',
    [styles.buoyCarouselArrowDisabled]: !!disabled,
  });

interface Props {
  carouselRef?: HTMLUListElement | null;
  disable?: boolean;
  leftDisabled?: boolean;
  rightDisabled?: boolean;
}

const BuoyCarouselArrows: React.FC<Props> = ({
  carouselRef,
  disable,
  leftDisabled,
  rightDisabled,
}) => {
  const device = useDeviceSize();

  const showScrollArrows = !disable && device.isDesktop;
  const isScrollable = carouselRef && carouselRef?.scrollWidth > carouselRef?.clientWidth;

  const scrollCarousel = useCallback(
    (direction: 'left' | 'right') => {
      const left =
        direction === 'left'
          ? carouselRef!.scrollLeft - carouselRef!.clientWidth
          : carouselRef!.scrollLeft + carouselRef!.clientWidth;

      carouselRef!.scrollTo({
        left,
        behavior: 'smooth',
      });
    },
    [carouselRef],
  );

  if (!carouselRef || !showScrollArrows || !isScrollable) return null;

  const directions: ['left', 'right'] = ['left', 'right'];
  return (
    <>
      {directions.map((direction: 'left' | 'right') => {
        const disabled = direction === 'left' ? leftDisabled : rightDisabled;
        return (
          <button
            key={direction}
            type="button"
            disabled={disabled}
            className={arrowClass(direction, disabled)}
            onClick={() => scrollCarousel(direction)}
          >
            <Chevron direction={direction} />
          </button>
        );
      })}
    </>
  );
};

export default BuoyCarouselArrows;
