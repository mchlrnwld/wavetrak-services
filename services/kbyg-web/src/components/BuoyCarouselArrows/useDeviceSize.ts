import { useDeviceSize } from '@surfline/quiver-react';

export default useDeviceSize;
