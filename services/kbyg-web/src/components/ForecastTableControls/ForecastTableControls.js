import PropTypes from 'prop-types';
import React from 'react';

const ForecastTableControls = ({ expandAllDays }) => (
  <div className="sl-forecast-table-controls">
    <button
      type="button"
      className="sl-forecast-table-controls__expand-collapse"
      onClick={() => expandAllDays(true)}
    >
      Expand all
    </button>
    &nbsp;/&nbsp;
    <button
      type="button"
      className="sl-forecast-table-controls__expand-collapse"
      onClick={() => expandAllDays(false)}
    >
      Collapse all
    </button>
  </div>
);

ForecastTableControls.propTypes = {
  expandAllDays: PropTypes.func.isRequired,
};

export default ForecastTableControls;
