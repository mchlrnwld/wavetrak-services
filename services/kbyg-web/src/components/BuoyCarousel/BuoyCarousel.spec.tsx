import { expect } from 'chai';
import { ErrorBoundary, ErrorBox } from '@surfline/quiver-react';

import BuoyCarousel from './BuoyCarousel';
import BuoyCarouselCards from '../BuoyCarouselCards';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import { getNearbyBuoysFixture } from '../../common/api/fixtures/buoys';

describe('components / BuoyCarousel', () => {
  it('should render the error box if there is an error', () => {
    const error = new Error('oops');
    const { wrapper } = mountWithReduxAndRouter(BuoyCarousel, {
      error,
      loading: false,
    });

    const boundary = wrapper.find(ErrorBoundary);
    expect(boundary).to.have.length(1);
    expect(boundary.prop('properties')).to.deep.equal({ location: 'BuoyCarousel' });
    expect(boundary.prop('error')).to.deep.equal(error);

    expect(wrapper.find(ErrorBox).props()).to.deep.equal({
      message: 'Oh buoy something went wrong',
      type: 'nearby buoys',
      classes: {},
    });
  });

  it('should render buoy carousel cards', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyCarousel, {
      error: false,
      loading: true,
      stations: getNearbyBuoysFixture.data,
    });

    const cards = wrapper.find(BuoyCarouselCards);
    expect(cards).to.have.length(1);
  });

  it('should catch error in child components', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyCarousel, {
      loading: false,
      stations: [],
    });

    const error = new Error('something broke');
    wrapper.find(BuoyCarouselCards).simulateError(error);

    expect(wrapper.find(ErrorBox).text()).to.be.equal('Error loading nearby buoys');
  });
});
