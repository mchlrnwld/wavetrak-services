import React from 'react';
import { ErrorBox, ErrorBoundary } from '@surfline/quiver-react';

import BuoyCarouselCards from '../BuoyCarouselCards';
import { NearbyStation } from '../../types/buoys';

import styles from './BuoyCarousel.module.scss';

const errorBoundaryProperties = {
  location: 'BuoyCarousel',
};

interface Props {
  loading?: boolean;
  stations?: Array<NearbyStation>;
  error?: Error | string | boolean;
  trackLocation?: string;
}

const BuoyCarousel: React.FC<Props> = ({ loading, stations, error, trackLocation }) => (
  <section className={styles.buoyCarousel}>
    <ErrorBoundary
      properties={errorBoundaryProperties}
      error={error}
      render={() => (
        <ErrorBox message={error ? 'Oh buoy something went wrong' : ''} type="nearby buoys" />
      )}
    >
      <BuoyCarouselCards loading={loading} stations={stations} trackLocation={trackLocation} />
    </ErrorBoundary>
  </section>
);

export default BuoyCarousel;
