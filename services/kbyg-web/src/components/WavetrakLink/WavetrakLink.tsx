import React from 'react';
import Link from 'next/link';

interface Props extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
  href: string;
  onClick?: () => void;
  children: React.ReactNode;
}

/**
 * @description This is our custom implementation of a NextJS Link
 */
const WavetrakLink: React.FC<Props> = ({ href, children, ...extraProps }) => (
  <Link href={href}>
    <a {...extraProps}>{children}</a>
  </Link>
);

export default WavetrakLink;
