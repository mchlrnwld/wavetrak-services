import PropTypes from 'prop-types';
import React from 'react';

const ChartDesktopRange = ({ start, end }) => (
  <div className="sl-chart-desktop-range">
    {start < end ? `${start + 1}-${end + 1}` : `${start + 1}`}
  </div>
);

ChartDesktopRange.propTypes = {
  start: PropTypes.number.isRequired,
  end: PropTypes.number.isRequired,
};

export default ChartDesktopRange;
