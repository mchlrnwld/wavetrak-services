import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ChartDesktopRange from './ChartDesktopRange';

describe('components / ChartDesktopRange', () => {
  it('renders start and end range', () => {
    const wrapper = shallow(<ChartDesktopRange start={5} end={9} />);
    expect(wrapper.text()).to.equal('6-10');
  });

  it('renders only start when start and end are the same', () => {
    const wrapper = shallow(<ChartDesktopRange start={5} end={5} />);
    expect(wrapper.text()).to.equal('6');
  });
});
