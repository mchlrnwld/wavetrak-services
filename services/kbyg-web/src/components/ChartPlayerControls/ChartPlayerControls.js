import PropTypes from 'prop-types';
import React from 'react';
import { trackEvent } from '@surfline/web-common';
import overviewDataPropTypes from '../../propTypes/subregionOverviewData';
import PlayIcon from '../Icons/Play';
import PauseIcon from '../Icons/Pause';
import RewindIcon from '../Icons/Rewind';
import { chartSegmentPropsPropTypes } from '../../propTypes/charts';

const ChartPlayerControls = ({
  overviewData,
  jumpToIndex,
  toggleIsPlaying,
  isPlaying,
  activeIndex,
  className,
  chartSegmentProps,
}) => {
  const Forward = RewindIcon;
  const Back = RewindIcon;
  const { _id, primarySpot } = overviewData;
  const { category, pageName } = chartSegmentProps;
  const clickedProperties = {
    subregionId: _id,
    spotId: primarySpot,
    category,
    pageName,
  };
  const onClickControl = (buttonAction, callback) => {
    trackEvent('Clicked Scrubber Player', {
      buttonAction,
      ...clickedProperties,
    });
    return callback;
  };
  return (
    <div className={className}>
      <Back onClick={() => onClickControl('back', jumpToIndex(activeIndex - 1, true))} />
      {isPlaying ? (
        <PauseIcon onClick={() => onClickControl('pause', toggleIsPlaying())} />
      ) : (
        <PlayIcon onClick={() => onClickControl('play', toggleIsPlaying())} />
      )}
      <Forward
        forward
        onClick={() => onClickControl('forward', jumpToIndex(activeIndex + 1, true))}
      />
    </div>
  );
};

ChartPlayerControls.propTypes = {
  overviewData: overviewDataPropTypes.isRequired,
  jumpToIndex: PropTypes.func.isRequired,
  toggleIsPlaying: PropTypes.func.isRequired,
  isPlaying: PropTypes.bool.isRequired,
  activeIndex: PropTypes.number.isRequired,
  className: PropTypes.string.isRequired,
  chartSegmentProps: chartSegmentPropsPropTypes.isRequired,
};

export default ChartPlayerControls;
