import PropTypes from 'prop-types';
import React from 'react';
import { format } from 'date-fns';
import { getLocalDate } from '@surfline/web-common';
import { chartSegmentPropsPropTypes, chartsPropTypes } from '../../propTypes/charts';
import DownPointer from './DownPointer';
import scrubberStatePropTypes from '../../propTypes/scrubberstate';
import ChartPlayerControls from '../ChartPlayerControls';
import overviewDataPropTypes from '../../propTypes/subregionOverviewData';
import ChartScrubberMobileControls from './ChartScrubberMobileControls';

const ChartScrubberMobile = React.forwardRef(
  (
    {
      activeIndex,
      charts,
      isPlaying,
      jumpToIndex,
      toggleIsPlaying,
      utcOffset,
      stepped,
      paywall,
      timestepsAvailable,
      scrubberState,
      overviewData,
      displayChartScrubberDayBorder,
      chartSegmentProps,
    },
    ref,
  ) => {
    const timestepWidth = 26;
    const chartImages = charts.images.images;

    return (
      <div className="sl-chart-scrubber-mobile">
        {chartImages.length ? (
          <div>
            <div
              className="sl-chart-scrubber-mobile__date"
              style={{
                paddingLeft: timestepWidth / 2,
              }}
            >
              {charts.active.type !== 'nearshore' && chartImages[0].timestamp ? (
                <div>
                  {format(
                    getLocalDate(chartImages[activeIndex].timestamp, utcOffset),
                    'iii MMM do | h:mma ',
                  )}
                  {charts.images.timezone}
                </div>
              ) : null}
              {charts.active.type !== 'nearshoresst' && charts.active.type !== 'regionalsst' ? (
                <div>
                  <ChartPlayerControls
                    className="sl-chart-scrubber-mobile__controls"
                    overviewData={overviewData}
                    jumpToIndex={jumpToIndex}
                    toggleIsPlaying={toggleIsPlaying}
                    isPlaying={isPlaying}
                    activeIndex={activeIndex}
                    chartSegmentProps={chartSegmentProps}
                  />
                  <DownPointer className="sl-chart-scrubber-mobile__down-pointer" />
                </div>
              ) : null}
            </div>
            {charts.active.type !== 'nearshoresst' && charts.active.type !== 'regionalsst' ? (
              <div className="sl-chart-scrubber-mobile__scrubber-controls">
                <ChartScrubberMobileControls
                  className="sl-chart-scrubber-mobile__scrubber-controls__drag"
                  jumpToIndex={jumpToIndex}
                  timestepWidth={timestepWidth}
                  activeIndex={activeIndex}
                  isPlaying={isPlaying}
                  toggleIsPlaying={toggleIsPlaying}
                  stepped={stepped}
                  charts={charts}
                  ref={ref}
                  paywall={paywall}
                  timestepsAvailable={timestepsAvailable}
                  scrubberState={scrubberState}
                  utcOffset={utcOffset}
                  displayChartScrubberDayBorder={displayChartScrubberDayBorder}
                  overviewData={overviewData}
                  chartSegmentProps={chartSegmentProps}
                />
              </div>
            ) : null}
          </div>
        ) : null}
      </div>
    );
  },
);

ChartScrubberMobile.displayName = 'ChartScrubberMobile';

ChartScrubberMobile.propTypes = {
  scrubberState: scrubberStatePropTypes.isRequired,
  activeIndex: PropTypes.number.isRequired,
  charts: chartsPropTypes.isRequired,
  jumpToIndex: PropTypes.func.isRequired,
  isPlaying: PropTypes.bool.isRequired,
  toggleIsPlaying: PropTypes.func.isRequired,
  utcOffset: PropTypes.number,
  stepped: PropTypes.bool,
  paywall: PropTypes.bool.isRequired,
  timestepsAvailable: PropTypes.number.isRequired,
  overviewData: overviewDataPropTypes.isRequired,
  displayChartScrubberDayBorder: PropTypes.func.isRequired,
  chartSegmentProps: chartSegmentPropsPropTypes.isRequired,
};

ChartScrubberMobile.defaultProps = {
  utcOffset: null,
  stepped: null,
};

export default ChartScrubberMobile;
