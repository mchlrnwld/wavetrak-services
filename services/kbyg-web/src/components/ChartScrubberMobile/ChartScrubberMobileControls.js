/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classnames from 'classnames';
import { getWindow } from '@surfline/web-common';
import { chartSegmentPropsPropTypes, chartsPropTypes } from '../../propTypes/charts';
import ChartScrubberPaywall from '../ChartScrubberPaywall';
import scrubberStatePropTypes from '../../propTypes/scrubberstate';
import overviewDataPropTypes from '../../propTypes/subregionOverviewData';

class ChartScrubberMobileControls extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dragging: false,
    };
  }

  componentDidMount() {
    const win = getWindow();
    win.addEventListener('resize', this.checkScrubberDimensions);
  }

  componentWillUnmount() {
    const win = getWindow();
    if (this.animate) clearTimeout(this.animate);
    win.removeEventListener('resize', this.checkScrubberDimensions);
  }

  chartScrubberTimestepRef = [];

  endHandle() {
    const { dragging } = this.state;
    if (dragging) {
      this.setState({ dragging: false });
    }
  }

  startHandle(e) {
    const { dragging } = this.state;
    const { toggleIsPlaying, isPlaying } = this.props;
    if (!dragging) {
      if (isPlaying) toggleIsPlaying();
      this.setState({ dragging: true });
      this.lastClientX = e.touches ? e.touches[0].clientX : e.clientX;
    }
  }

  moveHandle(e) {
    const { dragging } = this.state;
    if (dragging) {
      this.containerRef.scrollLeft -=
        -this.lastClientX + (this.lastClientX = e.touches ? e.touches[0].clientX : e.clientX);
    }
  }

  scrollHandle() {
    const { isPlaying, jumpToIndex } = this.props;
    const { dragging } = this.state;

    // Check if we are dragging so we don't fire a duplicate event for arrow clicks
    if (this.containerRef && !isPlaying && dragging) {
      const { timestepWidth } = this.props;
      const activeIndex = Math.round(this.containerRef.scrollLeft / timestepWidth, 10);
      jumpToIndex(activeIndex, null);
    }
  }

  scrollToIndex() {
    const { timestepWidth, activeIndex } = this.props;
    const scrollTo = activeIndex * timestepWidth;
    this.containerRef.scrollLeft = scrollTo;
  }

  bindEndHandle() {
    return this.endHandle.bind(this);
  }

  bindMoveHandle() {
    return this.moveHandle.bind(this);
  }

  bindStartHandle() {
    return this.startHandle.bind(this);
  }

  bindScrollHandle() {
    return this.scrollHandle.bind(this);
  }

  render() {
    const {
      className,
      isPlaying,
      stepped,
      timestepWidth,
      charts,
      activeIndex,
      paywall,
      timestepsAvailable,
      scrubberState,
      utcOffset,
      displayChartScrubberDayBorder,
      overviewData,
      chartSegmentProps,
    } = this.props;
    if (isPlaying || stepped) this.scrollToIndex();
    const chartImages = charts.images.images;
    const getActiveScrubberItemClasses = (index, currentIndex, paywalled) =>
      classnames({
        'sl-chart-scrubber-mobile__scrubber-controls__drag__list__item': true,
        'sl-chart-scrubber-mobile__scrubber-controls__drag__list__item--active':
          index === currentIndex,
        'sl-chart-scrubber-mobile__scrubber-controls__drag__list__item--paywalled': paywalled,
      });
    return (
      <div
        className={className}
        onMouseUp={this.bindEndHandle(this)}
        onMouseMove={this.bindMoveHandle(this)}
        onTouchEnd={this.bindEndHandle(this)}
        onTouchMove={this.bindMoveHandle(this)}
        onScroll={this.bindScrollHandle(this)}
        ref={(el) => {
          this.containerRef = el;
        }}
      >
        <ul
          className="sl-chart-scrubber-mobile__scrubber-controls__drag__list"
          style={{
            paddingRight: chartImages.length * timestepWidth - timestepWidth * 2,
            paddingLeft: timestepWidth,
          }}
          onMouseUp={this.bindEndHandle(this)}
          onMouseDown={this.bindStartHandle(this)}
          onTouchEnd={this.bindEndHandle(this)}
          onTouchStart={this.bindStartHandle(this)}
        >
          {chartImages.map((image, index) => {
            const paywalled = paywall && index >= timestepsAvailable;
            return (
              <li
                key={`${image.timestamp || index}-mobile`}
                className={getActiveScrubberItemClasses(index, activeIndex, paywalled)}
                ref={(el) => {
                  this.chartScrubberTimestepRef[index] = el;
                }}
                style={{
                  borderLeft: `1px solid ${
                    displayChartScrubberDayBorder(charts.images.images, index, utcOffset) &&
                    !paywalled
                      ? 'white'
                      : null
                  }`,
                }}
              />
            );
          })}
          {paywall && scrubberState.paywallWidth ? (
            <ChartScrubberPaywall
              className="sl-chart-scrubber-mobile__scrubber-controls__drag__list__paywall"
              scrubberState={scrubberState}
              timestepsAvailable={timestepsAvailable}
              overviewData={overviewData}
              chartSegmentProps={chartSegmentProps}
            />
          ) : null}
        </ul>
      </div>
    );
  }
}

ChartScrubberMobileControls.propTypes = {
  scrubberState: scrubberStatePropTypes.isRequired,
  activeIndex: PropTypes.number.isRequired,
  className: PropTypes.string,
  jumpToIndex: PropTypes.func.isRequired,
  timestepWidth: PropTypes.number.isRequired,
  isPlaying: PropTypes.bool.isRequired,
  toggleIsPlaying: PropTypes.func.isRequired,
  stepped: PropTypes.bool,
  charts: chartsPropTypes.isRequired,
  paywall: PropTypes.bool.isRequired,
  timestepsAvailable: PropTypes.number.isRequired,
  utcOffset: PropTypes.number,
  displayChartScrubberDayBorder: PropTypes.func.isRequired,
  overviewData: overviewDataPropTypes.isRequired,
  chartSegmentProps: chartSegmentPropsPropTypes.isRequired,
};

ChartScrubberMobileControls.defaultProps = {
  className: null,
  stepped: false,
  utcOffset: null,
};

export default ChartScrubberMobileControls;
