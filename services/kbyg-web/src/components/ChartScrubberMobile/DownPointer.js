import PropTypes from 'prop-types';
import React from 'react';

const DownPointer = ({ className }) => (
  <svg width="24px" height="15px" viewBox="0 0 24 15" className={className}>
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <polygon
        fill="#0058B0"
        transform="translate(12.000000, 6.500000) scale(1, -1) translate(-12.000000, -6.500000) "
        points="12 0 23 13 1 13"
      />
      <polyline stroke="#FFFFFF" points="0 -0.5 12.0211182 13.557251 24.026001 -0.5" />
    </g>
  </svg>
);

DownPointer.propTypes = {
  className: PropTypes.string.isRequired,
};

export default DownPointer;
