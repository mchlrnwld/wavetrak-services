import React from 'react';

import styles from './MapHeader.module.scss';

interface Props {
  isBuoys?: boolean;
}

const MapHeader: React.FC<Props> = ({ isBuoys }) => (
  <section className={styles.mapHeader}>
    <p className={styles.mapHeaderText}>{`Displaying all ${
      isBuoys ? 'Buoys' : 'Spots'
    } in your map view, move the map to see more`}</p>
  </section>
);

export default MapHeader;
