import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';

import MapHeader from './MapHeader';

describe('components / MapHeader', () => {
  it('should render the buoys map header', () => {
    const wrapper = mount(<MapHeader isBuoys />);

    const text = wrapper.find('.mapHeaderText');

    expect(text).to.have.length(1);

    expect(text.text()).to.be.equal(
      'Displaying all Buoys in your map view, move the map to see more',
    );
  });

  it('should render the spot map header', () => {
    const wrapper = mount(<MapHeader />);

    const text = wrapper.find('.mapHeaderText');

    expect(text).to.have.length(1);

    expect(text.text()).to.be.equal(
      'Displaying all Spots in your map view, move the map to see more',
    );
  });
});
