import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@surfline/quiver-react';

import { getWindow } from '@surfline/web-common';
import HDCams from '../Icons/HDCams';

const MultiCamPaywall = ({ href, onClickPaywall }) => {
  const onClickGoPremium = () => {
    const win = getWindow();
    onClickPaywall();
    win.location.assign(href);
  };

  return (
    <div className="sl-multi-cam-paywall">
      <div className="sl-multi-cam-paywall__content">
        <h5 className="sl-multi-cam-paywall__title">Multi-Cam is a Premium Only Feature</h5>
        <div className="sl-multi-cam-paywall__subtitle-container">
          <HDCams isFilled />
          <span className="sl-multi-cam-paywall__subtitle">
            View multiple camera angles at the same time.
          </span>
        </div>
        <Button onClick={onClickGoPremium}>Start Free Trial</Button>
        <a className="sl-multi-cam-paywall__learn-more" href="/premium-benefits">
          Learn more about Surfline Premium
        </a>
      </div>
    </div>
  );
};

MultiCamPaywall.propTypes = {
  href: PropTypes.string.isRequired,
  onClickPaywall: PropTypes.func,
};

MultiCamPaywall.defaultProps = {
  onClickPaywall: null,
};

export default MultiCamPaywall;
