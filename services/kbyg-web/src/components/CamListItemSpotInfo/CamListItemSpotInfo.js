import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { SurfHeight } from '@surfline/quiver-react';
import spotProptype from '../../propTypes/spot';
import conditionClassModifier from '../../utils/conditionClassModifier';
import { SL_WEB_CONDITION_VARIATION } from '../../common/treatments';
import { useSplitTreatment } from '../../selectors/split';

const className = (conditions, expired, sevenRatings = false) => {
  const classObject = {};
  classObject['sl-cam-list-item-spot-info__conditions'] = true;
  classObject[
    `sl-cam-list-item-spot-info__conditions${sevenRatings ? '-v2' : ''}--${conditionClassModifier(
      conditions,
      sevenRatings,
    )}`
  ] = true;
  classObject['sl-cam-list-item-spot-info__conditions--expired'] = expired;
  return classNames(classObject);
};

const CamListItemSpotInfo = ({ spot, units }) => {
  const { treatment, loading } = useSplitTreatment(SL_WEB_CONDITION_VARIATION);

  return (
    <div>
      <div className="sl-cam-list-item-spot-info__name">
        {!loading ? (
          <span
            className={className(
              spot.conditions.value,
              spot.conditions.expired && spot.conditions.human,
              treatment === 'on',
            )}
          />
        ) : (
          <span className="sl-cam-list-item-spot-info__conditions" />
        )}
        {spot.name}
      </div>
      <SurfHeight min={spot.waveHeight.min} max={spot.waveHeight.max} units={units} />
    </div>
  );
};

CamListItemSpotInfo.propTypes = {
  spot: spotProptype.isRequired,
  units: PropTypes.string.isRequired,
};

export default CamListItemSpotInfo;
