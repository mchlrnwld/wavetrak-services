import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ForecastDataViewToggle from './ForecastDataViewToggle';

describe('components / ForecastDataViewToggle', () => {
  it('has only 1 active view state', () => {
    const wrapper = shallow(<ForecastDataViewToggle dataView="GRAPH" />);
    const stateOptionsWrapper = wrapper.find('.sl-forecast-data-view-toggle__button');
    const activeStateWrapper = wrapper.find('.sl-forecast-data-view-toggle__button--active');
    expect(stateOptionsWrapper).to.have.length(2);
    expect(activeStateWrapper).to.have.length(1);
  });
});
