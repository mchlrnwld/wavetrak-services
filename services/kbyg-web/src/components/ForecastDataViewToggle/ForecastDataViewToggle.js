import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import GraphIcon from '../Icons/GraphIcon';
import ListIcon from '../Icons/ListIcon';

const VIEW_TYPES = ['GRAPH', 'TABLE'];

const VIEW_TYPE_ICON = {
  GRAPH: <GraphIcon />,
  TABLE: <ListIcon />,
};

const ForecastDataViewToggle = ({ dataView, onClick }) => {
  const buttonClassName = (isActive) =>
    classNames({
      'sl-forecast-data-view-toggle__button': true,
      'sl-forecast-data-view-toggle__button--active': isActive,
    });

  return (
    <div className="sl-forecast-data-view-toggle">
      {VIEW_TYPES.map((viewType) => (
        <button
          type="button"
          key={viewType}
          className={buttonClassName(dataView === viewType)}
          onClick={() => onClick(viewType)}
        >
          {VIEW_TYPE_ICON[viewType]}
        </button>
      ))}
    </div>
  );
};

ForecastDataViewToggle.propTypes = {
  dataView: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default ForecastDataViewToggle;
