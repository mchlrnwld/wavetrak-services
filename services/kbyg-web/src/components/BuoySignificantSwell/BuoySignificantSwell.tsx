import React from 'react';
import classNames from 'classnames';

import BuoyOfflineWrapper from '../BuoyOfflineWrapper';
import BuoyHeight from '../BuoyHeight';
import BuoyPeriod from '../BuoyPeriod';
import BuoyDirection from '../BuoyDirection';

import styles from './BuoySignificantSwell.module.scss';
import { SurfHeight } from '../../types/units';

const getClass = (largeOnly?: boolean) =>
  classNames({
    [styles.buoySigSwell]: true,
    [styles.buoySigSwellLargeOnly]: largeOnly,
  });

interface Props {
  offline?: boolean;
  height?: number;
  period?: number;
  direction?: number;
  units: SurfHeight;
  largeOnly?: boolean;
  withTitle?: boolean;
}

const BuoySignificantSwell: React.FC<Props> = ({
  offline = false,
  height,
  period,
  direction,
  units,
  largeOnly,
  withTitle,
}) => (
  <section className={getClass(largeOnly)}>
    {withTitle && <h2 className={styles.buoySigSwellTitle}>Wave Height</h2>}
    <BuoyOfflineWrapper
      offline={offline}
      className={styles.buoyOfflineWrapperHeight}
      fallback={
        <>
          -.-
          <span className={styles.fallbackUnits}>{units.toLowerCase()}</span>
        </>
      }
    >
      <BuoyHeight
        height={height}
        units={units}
        classes={{
          root: styles.buoyHeight,
          height: styles.buoyHeightData,
          units: styles.buoyHeightUnits,
        }}
      />
    </BuoyOfflineWrapper>
    <div className={styles.buoySigSwellData}>
      <BuoyOfflineWrapper
        offline={offline}
        className={styles.buoyOfflineWrapperSwell}
        fallback="No Data Found"
      >
        <BuoyPeriod period={period} />
        {', '}
        <BuoyDirection direction={direction} />
      </BuoyOfflineWrapper>
    </div>
  </section>
);

export default BuoySignificantSwell;
