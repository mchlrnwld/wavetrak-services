import React from 'react';

import { expect } from 'chai';
import { mount } from 'enzyme';

import BuoySignificantSwell from './BuoySignificantSwell';
import BuoyOfflineWrapper from '../BuoyOfflineWrapper';
import BuoyHeight from '../BuoyHeight';
import BuoyPeriod from '../BuoyPeriod';
import BuoyDirection from '../BuoyDirection';

describe('components / BuoySignificantSwell', () => {
  it('should render offline text when the buoy is offline', () => {
    const wrapper = mount(<BuoySignificantSwell offline units="FT" />);
    const section = wrapper.find('.buoySigSwell');
    const offlineWrappers = wrapper.find(BuoyOfflineWrapper);

    expect(section).to.have.length(1);
    expect(offlineWrappers).to.have.length(2);
    expect(offlineWrappers.at(0).text()).to.be.equal('-.-ft');
    expect(offlineWrappers.at(1).text()).to.be.equal('No Data Found');
  });

  it('should render the data when the buoy is online', () => {
    const wrapper = mount(<BuoySignificantSwell height={1} period={1} direction={1} units="FT" />);

    const base = wrapper.find('.buoySigSwell');
    const height = wrapper.find(BuoyHeight);
    const period = wrapper.find(BuoyPeriod);
    const direction = wrapper.find(BuoyDirection);

    expect(base).to.have.length(1);

    expect(height).to.have.length(1);
    expect(period).to.have.length(1);
    expect(direction).to.have.length(1);

    expect(height.props()).to.deep.equal({
      height: 1,
      units: 'FT',
      classes: { height: 'buoyHeightData', root: 'buoyHeight', units: 'buoyHeightUnits' },
    });
    expect(period.props()).to.deep.equal({ period: 1 });
    expect(direction.props()).to.deep.equal({ direction: 1 });
  });

  it('should render the title when `withTitle` is true', () => {
    const wrapper = mount(
      <BuoySignificantSwell withTitle height={1} period={1} direction={1} units="FT" />,
    );
    const title = wrapper.find('.buoySigSwellTitle');

    expect(title).to.have.length(1);
    expect(title.text()).to.be.equal('Wave Height');
  });
});
