import PropTypes from 'prop-types';
import React from 'react';
import { Chevron } from '@surfline/quiver-react';

const ChevronBox = ({ direction }) => (
  <div className="sl-chevron-box">
    <Chevron direction={direction} />
  </div>
);

ChevronBox.propTypes = {
  direction: PropTypes.string,
};

ChevronBox.defaultProps = {
  direction: 'down',
};

export default ChevronBox;
