import PropTypes from 'prop-types';
import React, { useRef } from 'react';
import { TrackableLink } from '@surfline/quiver-react';
import en from '../../intl/translations/en';
import { CLICKED_SUBSCRIBE_CTA } from '../../common/constants';

const ChartRangeSelectorCTA = ({ href, getSubscribeCTAProperties }) => {
  const ctaLinkRef = useRef();

  return (
    <div className="sl-chart-range-selector-cta">
      <div className="sl-chart-range-selector-cta__text">
        {en.premiumCTA.ChartRangeSelectorCTA.mainText}
      </div>
      <div className="sl-chart-range-selector-cta__action">
        <TrackableLink
          ref={ctaLinkRef}
          eventName={CLICKED_SUBSCRIBE_CTA}
          eventProperties={getSubscribeCTAProperties}
        >
          <a ref={ctaLinkRef} href={href} className="sl-chart-range-selector-cta__action__link">
            {en.premiumCTA.ChartRangeSelectorCTA.linkText}
          </a>
        </TrackableLink>
      </div>
    </div>
  );
};

ChartRangeSelectorCTA.defaultProps = {
  href: null,
};

ChartRangeSelectorCTA.propTypes = {
  href: PropTypes.string,
  getSubscribeCTAProperties: PropTypes.func.isRequired,
};

export default ChartRangeSelectorCTA;
