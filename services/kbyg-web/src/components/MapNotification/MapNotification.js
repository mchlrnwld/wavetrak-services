import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { AnimatePresence, motion } from 'framer-motion';

import useNotification from './useNotification';
import styles from './MapNotification.module.scss';

/**
 * @param {object} params
 * @param {boolean?} params.isError
 * @param {boolean?} params.isLoading
 * @param {boolean?} params.isMessage
 */
const getClassName = ({ isError, isLoading, isMessage }) =>
  classNames({
    [styles.slMapNotification]: true,
    [styles.slMapNotificationError]: isError,
    [styles.slMapNotificationLoading]: isLoading,
    [styles.slMapNotificationWithMessage]: isMessage,
  });

const MapNotification = ({ hide }) => {
  const { message, isError, isLoading, isMessage } = useNotification();

  if (hide) {
    return null;
  }

  return (
    <AnimatePresence>
      {message && (
        <motion.dialog
          initial={{ opacity: 0, y: -10, x: '-50%', scale: 0.3 }}
          animate={{ opacity: 1, y: 0, scale: 1 }}
          exit={{ opacity: 0, scale: 0.5, transition: { duration: 0.15, ease: 'easeInOut' } }}
          className={getClassName({ isError, isLoading, isMessage })}
        >
          <p className={styles.slMapNotificationMessage}>{message}</p>
        </motion.dialog>
      )}
    </AnimatePresence>
  );
};

MapNotification.propTypes = {
  hide: PropTypes.bool,
};

MapNotification.defaultProps = {
  hide: false,
};

export default MapNotification;
