import { expect } from 'chai';

import MapNotification from '.';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import state from '../../selectors/fixtures/state';

describe('components / MapNotification', () => {
  it('should render nothing if hide is true', () => {
    const { wrapper } = mountWithReduxAndRouter(
      MapNotification,
      { hide: true },
      {
        initialState: {
          mapV2: {
            ...state.mapV2,
            loading: true,
          },
        },
      },
    );

    expect(wrapper.isEmptyRender()).to.be.true();
  });

  it('should not render a notification if there is no message, loading, or error', () => {
    const { wrapper } = mountWithReduxAndRouter(
      MapNotification,
      {},
      {
        initialState: {
          mapV2: state.mapV2,
        },
      },
    );

    expect(wrapper.find('.slMapNotificationMessage')).to.have.length(0);
  });

  it('should render a loading message as the top priority', () => {
    const { wrapper } = mountWithReduxAndRouter(
      MapNotification,
      {},
      {
        initialState: {
          mapV2: {
            ...state.mapV2,
            loading: true,
            error: { message: 'test' },
            message: 'Some message',
          },
        },
      },
    );

    const dialog = wrapper.find('dialog.slMapNotificationLoading');
    const message = wrapper.find('.slMapNotificationMessage');

    expect(dialog).to.have.length(1);
    expect(message).to.have.length(1);
    expect(message.text()).to.be.equal('Loading Buoys...');
  });

  it('should render an error message as the second priority', () => {
    const { wrapper } = mountWithReduxAndRouter(
      MapNotification,
      {},
      {
        initialState: {
          mapV2: {
            ...state.mapV2,
            loading: false,
            error: { message: 'test' },
            message: 'Some message',
          },
        },
      },
    );

    const dialog = wrapper.find('dialog.slMapNotificationError');
    const message = wrapper.find('.slMapNotificationMessage');

    expect(dialog).to.have.length(1);
    expect(message).to.have.length(1);
    expect(message.text()).to.be.equal('Oh buoy! No data found');
  });

  it('should render a message as the last priority', () => {
    const { wrapper } = mountWithReduxAndRouter(
      MapNotification,
      {},
      {
        initialState: {
          mapV2: {
            ...state.mapV2,
            loading: false,
            error: null,
            message: 'Some message',
          },
        },
      },
    );

    const dialog = wrapper.find('dialog.slMapNotificationWithMessage');
    const message = wrapper.find('.slMapNotificationMessage');

    expect(dialog).to.have.length(1);
    expect(message).to.have.length(1);
    expect(message.text()).to.be.equal('Some message');
  });
});
