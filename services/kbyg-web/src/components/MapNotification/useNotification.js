import { useSelector } from 'react-redux';

import { getMapMessage, getMapError, getMapLoading } from '../../selectors/mapV2';

const useNotification = () => {
  const message = useSelector(getMapMessage);
  const error = useSelector(getMapError);
  const loading = useSelector(getMapLoading);

  if (loading) {
    return { isLoading: true, message: 'Loading Buoys...' };
  }

  if (error) {
    return { isError: true, message: 'Oh buoy! No data found' };
  }

  if (message) {
    return { isMessage: true, message };
  }

  return {};
};

export default useNotification;
