import { expect } from 'chai';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import {
  getBuoyMapPath,
  getBuoyMapPathWithLocation,
  getSpotMapPath,
  getSpotMapPathWithLocation,
} from '../../utils/urls/mapPaths';
import MapLinks from './MapLinks';
import MapLink from '../MapLink';

describe('components / MapLinks', () => {
  const router = {
    pathname: '/surf-reports-forecasts-cams-map/buoys',
    asPath: '/surf-reports-forecasts-cams-map/buoys',
  };

  const routerWithLocation = {
    pathname: '/surf-reports-forecasts-cams-map/buoys/[location]',
    asPath: '/surf-reports-forecasts-cams-map/buoys/@10,10,12z',
    query: { location: '@10,10,12z' },
  };
  it('should render links without locations if no location exists', () => {
    const { wrapper } = mountWithReduxAndRouter(MapLinks, {}, { router });

    const links = wrapper.find(MapLink);

    expect(links).to.have.length(2);

    expect(links.at(0).prop('href')).to.be.equal(getSpotMapPath());
    expect(links.at(1).prop('href')).to.be.equal(getBuoyMapPath());
  });

  it('should render links with a location if one exists', () => {
    const location = '@10,10,12z';
    const { wrapper } = mountWithReduxAndRouter(MapLinks, {}, { router: routerWithLocation });

    const navLinks = wrapper.find(MapLink);

    expect(navLinks).to.have.length(2);

    expect(navLinks.at(0).prop('href')).to.be.equal(getSpotMapPathWithLocation(location));
    expect(navLinks.at(1).prop('href')).to.be.equal(getBuoyMapPathWithLocation(location));
  });

  // TODO: Enable this test once we migrate the old map pages to map v2
  it.skip('should be active for a surf map path', () => {
    const { wrapper } = mountWithReduxAndRouter(MapLinks, {}, { router });

    const activeLink = wrapper.find('a.slMapLinkActive');

    expect(activeLink).to.have.length(1);
    expect(activeLink.text()).to.be.equal('Surf Spots');
  });

  // TODO: Enable this test once we migrate the old map pages to map v2
  it.skip('should be active for a surf map path with a location', () => {
    const { wrapper } = mountWithReduxAndRouter(MapLinks, {}, { router: routerWithLocation });

    const activeLink = wrapper.find('a.slMapLinkActive');

    expect(activeLink).to.have.length(1);
    expect(activeLink.text()).to.be.equal('Surf Spots');
  });

  it('should be active for a buoy map path', () => {
    const { wrapper } = mountWithReduxAndRouter(MapLinks, {}, { router });

    const activeLink = wrapper.find('a.slMapLinkActive');

    expect(activeLink).to.have.length(1);
    expect(activeLink.text()).to.be.equal('Wave Buoys');
  });

  it('should be active for a buoy map path with a location', () => {
    const { wrapper } = mountWithReduxAndRouter(MapLinks, {}, { router: routerWithLocation });

    const activeLink = wrapper.find('a.slMapLinkActive');

    expect(activeLink).to.have.length(1);
    expect(activeLink.text()).to.be.equal('Wave Buoys');
  });
});
