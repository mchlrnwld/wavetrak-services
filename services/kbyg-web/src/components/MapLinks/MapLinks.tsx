import React, { useCallback } from 'react';
import { useRouter } from 'next/router';
import classNames from 'classnames';

import MapLink from '../MapLink';
import {
  getSpotMapPath,
  getSpotMapPathWithLocation,
  getBuoyMapPath,
  getBuoyMapPathWithLocation,
} from '../../utils/urls/mapPaths';

import styles from './MapLinks.module.scss';

const getClass = () => classNames('sl-map-links', styles.slMapLinks);

const MapLinks: React.FC = () => {
  const router = useRouter();
  const { location } = router.query as { location: string };

  const getSpotLink = useCallback(() => {
    if (location) {
      return getSpotMapPathWithLocation(location);
    }
    return getSpotMapPath();
  }, [location]);

  const getBuoyLink = useCallback(() => {
    if (location) {
      return getBuoyMapPathWithLocation(location);
    }
    return getBuoyMapPath();
  }, [location]);

  return (
    <nav className={getClass()}>
      <MapLink href={getSpotLink()}>Surf Spots</MapLink>
      <MapLink href={getBuoyLink()}>Wave Buoys</MapLink>
    </nav>
  );
};

MapLinks.propTypes = {};

export default MapLinks;
