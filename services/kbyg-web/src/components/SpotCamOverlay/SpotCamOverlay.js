/* eslint-disable jsx-a11y/click-events-have-key-events */

import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { Favorite, CloseMediaPlayerIcon } from '@surfline/quiver-react';
import spotDetailsPropType from '../../propTypes/spotDetails';
import spotReportDataPropType from '../../propTypes/spot';
import userSettingsPropTypes from '../../propTypes/userSettings';

const getSpotCamOverlayClasses = (isPremium) =>
  classNames({
    'sl-spot-cam-overlay': true,
    'sl-spot-cam-overlay--premium': isPremium,
  });

const SpotCamOverlay = ({
  isPremium,
  onClickCloseHandler,
  reportData,
  spot,
  stickyCamIsOpen,
  userSettings,
}) => (
  <div className={getSpotCamOverlayClasses(isPremium)}>
    <div className="sl-spot-cam-overlay__close" onClick={onClickCloseHandler}>
      <CloseMediaPlayerIcon />
    </div>
    {stickyCamIsOpen && isPremium ? (
      <div className="sl-spot-cam-overlay__spot">
        <Favorite
          disableLink
          favorite={{ ...spot, ...reportData }}
          onClickLink={() => {}}
          settings={userSettings}
        />
      </div>
    ) : null}
  </div>
);

SpotCamOverlay.propTypes = {
  isPremium: PropTypes.bool,
  onClickCloseHandler: PropTypes.func,
  reportData: spotReportDataPropType,
  spot: spotDetailsPropType,
  stickyCamIsOpen: PropTypes.bool,
  userSettings: userSettingsPropTypes,
};

SpotCamOverlay.defaultProps = {
  isPremium: false,
  onClickCloseHandler: null,
  reportData: null,
  spot: null,
  stickyCamIsOpen: false,
  userSettings: null,
};

export default SpotCamOverlay;
