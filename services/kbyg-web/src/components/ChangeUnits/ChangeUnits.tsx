import React from 'react';

const ChangeUnits: React.FC<{ href: string }> = ({ href }) => (
  <a href={href} className="sl-change-units">
    Change units
  </a>
);

export default ChangeUnits;
