import PropTypes from 'prop-types';
import React from 'react';

const ForecastCTAText = ({ copy }) => <div className="sl-forecast-cta-text">{copy}</div>;

ForecastCTAText.propTypes = {
  copy: PropTypes.string,
};

ForecastCTAText.defaultProps = {
  copy: null,
};

export default ForecastCTAText;
