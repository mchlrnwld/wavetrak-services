import React from 'react';
import PropTypes from 'prop-types';
import { createAccessibleOnClick } from '@surfline/quiver-react';
import { format } from 'date-fns';
import classnames from 'classnames';
import { getLocalDate } from '@surfline/web-common';

import { isNumber } from 'lodash';
import conditionClassModifier from '../../utils/conditionClassModifier';
import { sessionPropType, wavePropType } from '../../propTypes/session';
import styles from './SessionDetails.module.scss';
import Star from '../Icons/Star';
import { getSessionTime, formatSurfHeight } from './utils';

const InfoBox = ({ title, data, unitsKey, units, showUnits, round }) => {
  const dataString = round ? Math.round(data) : data;
  return (
    <div className={styles.sessionInfoBox}>
      <h6 className={styles.sessionInfoBoxTitle}>{title}</h6>
      <p className={styles.sessionInfoBoxData}>
        {dataString || 'N/A'}
        {showUnits && dataString ? units[unitsKey] : ''}
      </p>
    </div>
  );
};

const conditionClassName = (condition) =>
  classnames({
    [styles.sessionDetailsConditionsTitle]: true,
    [styles[`sessionDetailsConditionsTitle--${conditionClassModifier(condition)}`]]: true,
  });

const clipUrlClassName = (clipUrl) =>
  classnames({
    [styles.sessionDetailsClipDownload]: true,
    [styles.sessionDetailsClipDownloadNoClip]: !clipUrl,
  });

const SessionDetails = ({
  session,
  setActiveClipIndex,
  activeWave,
  activeClipIndex,
  utcOffset,
  timezoneAbbr,
}) => {
  const sessionConditionsValue = session?.conditions?.conditions?.value;
  const { units } = session.associated;
  const sessionWaveHeights = session?.conditions?.waveHeight;
  const sessionWind = session?.conditions?.wind;
  const sessionTide = session?.conditions?.tide;
  const sessionMinHeight = sessionWaveHeights?.min;
  const sessionMaxHeight = sessionWaveHeights?.max;
  const numClips = activeWave?.clips?.length;
  const clipUrl = activeWave?.clips?.[activeClipIndex]?.clipUrl;
  const isFirstClip = activeClipIndex === 0;
  const isLastClip = activeClipIndex === numClips - 1;
  const hasMultipleClips = numClips > 1;
  const goToPrevClip = () => {
    if (!isFirstClip) {
      setActiveClipIndex((curr) => curr - 1);
    }
  };

  const goToNextClip = () => {
    if (!isLastClip) {
      setActiveClipIndex((curr) => curr + 1);
    }
  };

  const timezoneString = timezoneAbbr ? ` ${timezoneAbbr}` : '';

  const localStartDate = getLocalDate(session.startTimestamp / 1000, utcOffset);
  const localEndDate = getLocalDate(session.endTimestamp / 1000, utcOffset);
  return (
    <div className={styles.sessionDetailsContainer}>
      <div className={styles.sessionDetailsHeader}>
        <h1 className={styles.sessionDetailsTitle}>
          {session.name} at {session.spot?.name}
        </h1>
        <a className={clipUrlClassName(clipUrl)} href={clipUrl} download>
          Download Clip
        </a>
        <button
          disabled={!!isFirstClip || !hasMultipleClips}
          {...createAccessibleOnClick(goToPrevClip, 'button')}
          className={styles.sessionDetailsChangeClip}
          type="button"
        >
          {'< Previous Angle'}
        </button>
        <button
          disabled={!!isLastClip || !hasMultipleClips}
          {...createAccessibleOnClick(goToNextClip, 'button')}
          className={styles.sessionDetailsChangeClip}
          type="button"
        >
          {'Next Angle >'}
        </button>
      </div>
      <div className={styles.sessionDetailsInfo}>
        <p className={styles.sessionDetailsInfoTimestamp}>
          {`${format(localStartDate, 'EEE, MMM d, yyyy')} | ${format(
            localStartDate,
            "hh:mmaaaaa'm'",
          )}-${format(localEndDate, "hh:mmaaaaa'm'")}${timezoneString}, ${getSessionTime(
            session.startTimestamp,
            session.endTimestamp,
          )}`}
        </p>
        {session.rating && (
          <p className={styles.sessionDetailsInfoRating}>
            {[...Array(session.rating)].map((_, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <Star key={index} />
            ))}
          </p>
        )}
      </div>
      <div className={styles.sessionDetails}>
        <div className={styles.sessionDetailsStats}>
          <h6 className={styles.sessionDetailsStatsTitle}>Session Stats</h6>
          <div className={styles.sessionDetailsStatsContainer}>
            <InfoBox title="Waves" data={session?.stats.waveCount} />
            <InfoBox
              title="Top Speed"
              data={session.stats?.speedMax}
              round
              showUnits
              unitsKey="speed"
              units={units}
            />
            <InfoBox
              title="Longest Wave"
              data={session.stats?.longestWave?.distance || 0}
              round
              showUnits
              units={units}
              unitsKey="distance"
            />
          </div>
        </div>
        <div className={styles.sessionDetailsConditions}>
          <h6 className={conditionClassName(sessionConditionsValue)}>
            {conditionClassModifier(sessionConditionsValue)}
          </h6>
          <div className={styles.sessionDetailsConditionsContainer}>
            <InfoBox
              title="Surf Height"
              data={formatSurfHeight(
                sessionMinHeight,
                sessionMaxHeight,
                units.surfHeight,
                sessionWaveHeights?.plus,
              )}
            />
            <InfoBox
              title="Wind"
              data={sessionWind?.speed}
              round
              showUnits
              units={units}
              unitsKey="windSpeed"
            />
            <InfoBox
              title="Tide"
              data={isNumber(sessionTide?.height) && Math.round(sessionTide.height * 100) / 100}
              showUnits
              units={units}
              unitsKey="tideHeight"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

SessionDetails.propTypes = {
  session: sessionPropType,
  activeWave: wavePropType,
  setActiveClipIndex: PropTypes.func.isRequired,
  activeClipIndex: PropTypes.number,
  utcOffset: PropTypes.number.isRequired,
  timezoneAbbr: PropTypes.string,
};

SessionDetails.defaultProps = {
  session: {},
  activeWave: {},
  activeClipIndex: 0,
  timezoneAbbr: null,
};

InfoBox.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]),
  units: PropTypes.shape(),
  unitsKey: PropTypes.string,
  showUnits: PropTypes.bool,
  round: PropTypes.bool,
};

InfoBox.defaultProps = {
  data: '',
  showUnits: false,
  units: {},
  unitsKey: '',
  round: false,
};

export default React.memo(SessionDetails);
