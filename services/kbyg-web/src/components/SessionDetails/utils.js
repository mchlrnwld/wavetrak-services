import { differenceInHours, differenceInMinutes } from 'date-fns';

export const getSessionTime = (start, end) => {
  const hourDiff = differenceInHours(end, start);
  const minutes = differenceInMinutes(end, start);
  const minutesWithoutHours = minutes % 60;

  if (hourDiff > 0) {
    return `${hourDiff}h ${minutesWithoutHours}m`;
  }

  return `${minutesWithoutHours}m`;
};

export const formatSurfHeight = (minHeight, maxHeight, units, plus = false) => {
  const useOnlyMaxHeight = minHeight === maxHeight;
  const minHeightString = !useOnlyMaxHeight ? `${minHeight}-` : '';

  return `${minHeightString}${maxHeight}${units}${plus ? '+' : ''}`;
};
