import PropTypes from 'prop-types';
import React, { useMemo, useRef } from 'react';
import { accountPaths } from '@surfline/web-common';
import { TrackableLink } from '@surfline/quiver-react';
import classNames from 'classnames';

import en from '../../intl/translations/en';
import { CLICKED_SUBSCRIBE_CTA } from '../../common/constants';

const getCTALinkClassName = (size) =>
  classNames({
    'sl-forecast-cta-action-link': true,
    [`sl-forecast-cta-action-link--${size}`]: true,
  });

const ForecastCTAActionLink = ({ category, pageName, spotId, subregionId, location, size }) => {
  const linkRef = useRef();
  const segmentProperties = useMemo(
    () => ({
      category,
      pageName,
      spotId,
      subregionId,
      location,
      size,
    }),
    [category, pageName, spotId, location, subregionId, size],
  );

  return (
    <div className={getCTALinkClassName(size)}>
      <TrackableLink
        ref={linkRef}
        eventName={CLICKED_SUBSCRIBE_CTA}
        eventProperties={segmentProperties}
      >
        <a ref={linkRef} href={accountPaths.funnelPath}>
          {en.premiumCTA.ForecastVisualizerCTA.linkText}
        </a>
      </TrackableLink>
    </div>
  );
};

ForecastCTAActionLink.propTypes = {
  category: PropTypes.string.isRequired,
  pageName: PropTypes.string.isRequired,
  spotId: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  subregionId: PropTypes.string,
  size: PropTypes.string,
};

ForecastCTAActionLink.defaultProps = {
  subregionId: null,
  size: 'small',
};

export default ForecastCTAActionLink;
