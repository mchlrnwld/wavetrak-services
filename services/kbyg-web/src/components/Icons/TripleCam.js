import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const svgClassName = (isPrimary) =>
  classNames({
    'sl-triple-cam-icon': true,
    'sl-triple-cam-icon--primary': isPrimary,
  });

const TripleCam = ({ isPrimary }) => (
  <svg
    className={svgClassName(isPrimary)}
    width="28px"
    height="13px"
    viewBox="0 0 28 13"
    version="1.1"
    opacity={isPrimary ? 1 : 0.4}
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="Multiple-Cams" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="mobile-stadium-select" transform="translate(-132.000000, -334.000000)">
        <g id="Group-12-Copy" transform="translate(132.000000, 334.000000)">
          <polygon
            className="sl-primary-rect"
            id="Rectangle-25"
            fill={isPrimary ? '#0058B0' : '#96A9B2'}
            fillRule="nonzero"
            points="0 0 18 0 18 13 0 13"
          />
          {isPrimary && (
            <path
              d="M7.16382872,9.84253539 C7.15176747,9.86072487 7.13782174,9.8778837 7.12201162,9.89369382 C6.987407,10.0282984 6.77435805,10.0334867 6.63317866,9.89230733 L3.9997207,7.25884937 C3.86435034,7.123479 3.86766618,6.90068443 3.99833421,6.77001641 C4.13293883,6.63541179 4.34598778,6.63022352 4.48716717,6.77140291 L6.87361772,9.15785346 L12.9270922,3.10437901 C13.0601241,2.97134711 13.2810229,2.97655866 13.411691,3.10722668 C13.5462956,3.24183131 13.5491056,3.45725853 13.4145386,3.59182547 L7.16382872,9.84253539 Z"
              id="Combined-Shape"
              fill="#22D736"
            />
          )}
          <polygon
            id="Rectangle-25-Copy-5"
            fill="#96A9B2"
            fillRule="nonzero"
            points="19 0 28 0 28 6 19 6"
          />
          <polygon
            id="Rectangle-25-Copy-6"
            fill="#96A9B2"
            fillRule="nonzero"
            points="19 7 28 7 28 13 19 13"
          />
        </g>
      </g>
    </g>
  </svg>
);

TripleCam.propTypes = {
  isPrimary: PropTypes.bool,
};

TripleCam.defaultProps = {
  isPrimary: false,
};

export default TripleCam;
