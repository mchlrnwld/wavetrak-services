import React from 'react';

const Buoy = () => (
  <svg
    width="20px"
    height="34px"
    viewBox="0 0 20 34"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
  >
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-537.000000, -2589.000000)" strokeWidth="1.2">
        <g transform="translate(537.000000, 2589.000000)">
          <g>
            <g transform="translate(7.283578, 0.487418)" stroke="#22D737">
              <polyline
                strokeLinecap="round"
                points="2.88711532 8.65791048 10.4767191 5.1944139 2.88711532 2.30816675"
              />
              <path d="M2.19715134,12.0125825 L2.19715134,0.191585503" />
            </g>
            <path
              d="M0.932752562,27.3142857 C2.58031215,30.5302531 5.85243397,32.6176692 9.5,32.6176692 C13.147566,32.6176692 16.4196878,30.5302531 18.0672474,27.3142857 L0.932752562,27.3142857 Z"
              stroke="#FFFFFF"
            />
            <path
              d="M4.75,16.3825725 L4.75,16.3825725 C4.75,14.0410424 6.64818524,12.1428571 8.98971539,12.1428571 L10.0828628,12.1428571 C12.3843476,12.1428571 14.2500696,14.0085792 14.2500696,16.310064 C14.2500696,16.3342349 14.2498593,16.3584053 14.2494388,16.3825725 L4.75,16.3825725 Z"
              stroke="#FFFFFF"
              strokeLinecap="round"
            />
            <path
              d="M6.3867596,16.3857143 L3.17631843,27.3285714 L15.8236816,27.3285714 L12.6132404,16.3857143 L6.3867596,16.3857143 Z"
              stroke="#FFFFFF"
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default Buoy;
