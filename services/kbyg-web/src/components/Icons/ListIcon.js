import React from 'react';

const ListIcon = () => (
  <svg width="22" height="17" viewBox="0 0 22 17">
    <g fill="none" fillRule="evenodd" transform="translate(1 1)">
      <path d="M5 1h16v1H5zM5 7h16v1H5zM5 13h16v1H5z" />
      <circle cx="1.5" cy="1.5" r="1.5" />
      <circle cx="1.5" cy="7.5" r="1.5" />
      <circle cx="1.5" cy="13.5" r="1.5" />
    </g>
  </svg>
);

export default ListIcon;
