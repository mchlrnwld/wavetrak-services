import React from 'react';

const GraphIcon = () => (
  <svg width="24" height="20" viewBox="0 0 24 20">
    <g fill="none" fillRule="evenodd">
      <circle cx="9" cy="6" r="2.5" />
      <circle cx="3" cy="17" r="2.5" />
      <circle cx="15" cy="14" r="2.5" />
      <path d="M4 15l4-7" />
      <circle cx="21" cy="3" r="2.5" />
      <path d="M16 12l4-7M13 12l-3-4" />
    </g>
  </svg>
);

export default GraphIcon;
