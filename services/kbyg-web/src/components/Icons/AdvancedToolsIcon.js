import React from 'react';

const AdvancedToolsIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="25" height="23" viewBox="0 0 25 23">
    <g fill="none" fillRule="evenodd" stroke="#FFF">
      <path
        fill="#FFF"
        d="M12.787 4.42C8.766 2.125 3.917.708 3.55 1.074c-.35.35.946 4.795 3.062 8.7A43.095 43.095 0 0 1 9.094 7.09c.98-.968 2.287-1.872 3.693-2.671zm6.152 5.374c1.757 2.064 3.57 4.512 5.441 7.343l-4.275.491-.492 4.276c-2.614-1.717-4.897-3.387-6.848-5.01a46.4 46.4 0 0 0 3.215-2.917c1.09-1.093 2.094-2.592 2.959-4.183z"
      />
      <path d="M21.997 1.075c.493.493-2.25 9.124-6.017 12.902-2.511 2.519-5.86 5.161-10.046 7.928l-.492-4.276-4.275-.492C3.93 12.93 6.572 9.581 9.094 7.091c3.783-3.735 12.41-6.51 12.903-6.016z" />
      <path strokeLinecap="square" d="M5.087 17.235l8-7.123" />
    </g>
  </svg>
);

export default AdvancedToolsIcon;
