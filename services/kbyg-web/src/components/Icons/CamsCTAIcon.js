import React from 'react';

const CamsCTAIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="34" height="22" viewBox="0 0 34 22">
    <g fill="none" fillRule="evenodd" stroke="#FFF">
      <path d="M.5.5h33v21H.5z" />
      <path
        fill="#FFF"
        d="M1.941 21C3.494 13.103 14.791.01 26.671 6.722c-9.737 5.73-1.144 14.11 7.27 14.278h-32zM13.93 7.887l-.072 7.758 6.266-3.937-6.194-3.821z"
      />
    </g>
  </svg>
);

export default CamsCTAIcon;
