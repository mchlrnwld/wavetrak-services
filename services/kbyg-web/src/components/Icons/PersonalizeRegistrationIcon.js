import React from 'react';

const PersonalizeRegistrationIcon = () => (
  <svg width="40px" height="32px" viewBox="0 0 40 32">
    <g id="Create-Account-Modal" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="desktop-modal" transform="translate(-551.000000, -375.000000)" fillRule="nonzero">
        <g id="Group-2" transform="translate(468.000000, 145.000000)">
          <g id="homepage-icon" transform="translate(83.000000, 230.000000)">
            <rect
              id="Rectangle"
              stroke="#96A9B2"
              strokeWidth="1.5"
              x="5.75"
              y="4.75"
              width="16.5"
              height="8.5"
              rx="1"
            />
            <rect id="Rectangle" fill="#22D736" x="27" y="4" width="8" height="24" rx="1" />
            <rect
              id="Rectangle"
              stroke="#96A9B2"
              strokeWidth="1.5"
              x="0.75"
              y="0.75"
              width="38.5"
              height="30.5"
              rx="2"
            />
            <rect id="Rectangle" fill="#96A9B2" x="5" y="17" width="18" height="1.5" rx="0.75" />
            <rect
              id="Rectangle-Copy-4"
              fill="#96A9B2"
              x="5"
              y="21.5"
              width="18"
              height="1.5"
              rx="0.75"
            />
            <rect
              id="Rectangle-Copy-5"
              fill="#96A9B2"
              x="5"
              y="26"
              width="18"
              height="1.5"
              rx="0.75"
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default PersonalizeRegistrationIcon;
