import PropTypes from 'prop-types';
import React from 'react';

const PauseIcon = ({ onClick }) => (
  <svg viewBox="0 0 14 16" onClick={onClick} className="sl-pause-icon">
    <g>
      <rect x="0" y="0" width="5" height="16" />
      <rect x="9" y="0" width="5" height="16" />
    </g>
  </svg>
);

PauseIcon.propTypes = {
  onClick: PropTypes.func,
};

PauseIcon.defaultProps = {
  onClick: null,
};

export default PauseIcon;
