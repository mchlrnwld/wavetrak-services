import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const svgClassName = (isPrimary) =>
  classNames({
    'sl-single-cam-icon': true,
    'sl-single-cam-icon--primary': isPrimary,
  });

const SingleCam = ({ isPrimary }) => (
  <svg
    className={svgClassName(isPrimary)}
    width="16px"
    height="12px"
    viewBox="0 0 16 12"
    version="1.1"
    opacity={isPrimary ? 1 : 0.5}
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="Multiple-Cams" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g
        id="stadium-2-navigation"
        transform="translate(-242.000000, -331.000000)"
        fillRule="nonzero"
      >
        <g id="Group-2" transform="translate(110.000000, 279.000000)">
          <g id="Group-3-Copy" transform="translate(132.000000, 52.000000)">
            <rect
              id="Rectangle-6"
              stroke="#96A9B2"
              fill="#FFFFFF"
              x="0.5"
              y="0.5"
              width="12"
              height="11"
              rx="2"
            />
            <circle
              className="sl-single-cam-icon_red-dot"
              xmlns="http://www.w3.org/2000/svg"
              id="circle-6"
              stroke="none"
              fill="#FF0000"
              cx="6.5"
              cy="6"
              r="2.5"
            />
            <polygon id="Path-2" fill="#96A9B2" points="12 5 16 2 16 10 12 7" />
          </g>
        </g>
      </g>
    </g>
  </svg>
);

SingleCam.propTypes = {
  isPrimary: PropTypes.bool,
};

SingleCam.defaultProps = {
  isPrimary: false,
};

export default SingleCam;
