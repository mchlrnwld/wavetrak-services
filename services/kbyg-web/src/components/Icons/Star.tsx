import React from 'react';
import { color } from '@surfline/quiver-themes';

interface Props {
  fill?: string;
}

const Star: React.FC<Props> = ({ fill = color('dark-blue', 10) }) => (
  <svg className="sl-star-icon" width="16px" height="16px" viewBox="0 0 20 20" version="1.1">
    <g transform="translate(-2, -1)" fill={fill}>
      <path d="M18.4414138,21.6115643 L16.7234856,14.211456 L22.4573746,9.23296839 L14.8994885,8.58320757 L11.9480559,1.60642142 L8.99662326,8.58320757 L1.43873723,9.23296839 L7.17262615,14.211456 L5.45469804,21.6115643 L11.9480559,17.6878483 L18.4414138,21.6115643 Z" />
    </g>
  </svg>
);

export default Star;
