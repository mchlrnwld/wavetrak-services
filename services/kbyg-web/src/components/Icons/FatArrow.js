import React from 'react';

const FatArrow = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="64"
    height="30"
    viewBox="0 0 64 30"
    className="sl-fat-arrow-icon"
  >
    <g fill="none" fillRule="evenodd" stroke="#FFF" strokeWidth="2">
      <path d="M51 .858L62.623 15 51 29.142M62 15H0" />
    </g>
  </svg>
);

export default FatArrow;
