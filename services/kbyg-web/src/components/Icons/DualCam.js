import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const svgClassName = (isPrimary) =>
  classNames({
    'sl-dual-cam-icon': true,
    'sl-dual-cam-icon--primary': isPrimary,
  });

const DualCam = ({ isPrimary }) => (
  <svg
    className={svgClassName(isPrimary)}
    width="29px"
    height="11px"
    opacity={isPrimary ? 1 : 0.4}
    viewBox="0 0 29 11"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="Multiple-Cams" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="mobile-stadium-select" transform="translate(-190.000000, -335.000000)">
        <g id="Group-2" transform="translate(190.000000, 335.000000)">
          <polygon
            className="sl-primary-rect"
            id="Rectangle-25"
            fill={isPrimary ? '#0058B0' : '#96A9B2'}
            fillRule="nonzero"
            points="0 0 14 0 14 11 0 11"
          />
          {isPrimary && (
            <path
              d="M5.56456697,8.84253539 C5.55250572,8.86072487 5.53855999,8.8778837 5.52274987,8.89369382 C5.38814525,9.02829845 5.1750963,9.03348672 5.03391691,8.89230733 L2.40045895,6.25884937 C2.26508859,6.123479 2.26840444,5.90068443 2.39907246,5.77001641 C2.53367708,5.63541179 2.74672603,5.63022352 2.88790542,5.77140291 L5.27435597,8.15785346 L11.3278304,2.10437901 C11.4608623,1.97134711 11.6817612,1.97655866 11.8124292,2.10722668 C11.9470338,2.24183131 11.9498438,2.45725853 11.8152769,2.59182547 L5.56456697,8.84253539 Z"
              id="Combined-Shape"
              fill="#22D736"
            />
          )}
          <polygon
            id="Rectangle-25"
            fill="#96A9B2"
            fillRule="nonzero"
            points="15 0 29 0 29 11 15 11"
          />
        </g>
      </g>
    </g>
  </svg>
);

DualCam.propTypes = {
  isPrimary: PropTypes.bool,
};

DualCam.defaultProps = {
  isPrimary: false,
};
export default DualCam;
