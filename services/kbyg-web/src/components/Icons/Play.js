import PropTypes from 'prop-types';
import React from 'react';

const PlayIcon = ({ onClick }) => (
  <svg viewBox="0 0 10 16" className="sl-play-icon" onClick={onClick}>
    <g>
      <g transform="translate(-1249.000000, -390.000000)">
        <g transform="translate(431.000000, 382.000000)">
          <g transform="translate(741.000000, 0.000000)">
            <polygon
              transform="translate(82.000000, 16.000000) scale(-1, 1) translate(-82.000000, -16.000000)"
              points="87 8 77 16 87 24"
            />
          </g>
        </g>
      </g>
    </g>
  </svg>
);

PlayIcon.propTypes = {
  onClick: PropTypes.func,
};

PlayIcon.defaultProps = {
  onClick: null,
};

export default PlayIcon;
