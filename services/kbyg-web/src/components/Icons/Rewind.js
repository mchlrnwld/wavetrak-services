import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';

const getRewindIconClasses = (forward) =>
  classnames({
    'sl-rewind-icon': true,
    'sl-rewind-icon--forward': !!forward,
  });

const RewindIcon = ({ forward, onClick }) => (
  <svg viewBox="0 0 16 13" className={getRewindIconClasses(forward)} onClick={onClick}>
    <g>
      <g transform="translate(-1196.000000, -392.000000)">
        <g transform="translate(431.000000, 382.000000)">
          <g transform="translate(741.000000, 0.000000)">
            <path d="M40,10 L32,16.5 L40,23 L40,10 Z M24,16.5 L32,23 L32,16.5 L32,10 L24,16.5 Z" />
          </g>
        </g>
      </g>
    </g>
  </svg>
);

RewindIcon.propTypes = {
  forward: PropTypes.bool,
  onClick: PropTypes.func,
};

RewindIcon.defaultProps = {
  forward: false,
  onClick: null,
};

export default RewindIcon;
