import React from 'react';

const LongRangeForecastIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" width="34" height="24" viewBox="0 0 34 24">
    <g fill="none" fillRule="evenodd" transform="translate(0 1)">
      <circle cx="4" cy="10" r="2" fill="#FFF" stroke="#FFF" />
      <circle cx="17" cy="2" r="2" fill="#FFF" stroke="#FFF" />
      <path stroke="#FFF" strokeLinecap="square" d="M8.5 7.5l4-3" />
      <circle cx="30" cy="7" r="2" fill="#FFF" stroke="#FFF" />
      <path fill="#FFF" d="M0 14h8v9H0zM13 7h8v16h-8zM26 10h8v13h-8z" />
      <path stroke="#FFF" strokeLinecap="square" d="M21 3l5.42 2.136" />
    </g>
  </svg>
);

export default LongRangeForecastIcon;
