import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const svgClassName = (isPrimary) =>
  classNames({
    'sl-quad-cam-icon': true,
    'sl-quad-cam-icon--primary': isPrimary,
  });

const QuadCam = ({ isPrimary }) => (
  <svg
    className={svgClassName(isPrimary)}
    width="21px"
    height="13px"
    viewBox="0 0 21 13"
    version="1.1"
    opacity={isPrimary ? 1 : 0.4}
    xmlns="http://www.w3.org/2000/svg"
  >
    <g id="Multiple-Cams" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g
        id="mobile-stadium-select"
        transform="translate(-165.000000, -334.000000)"
        fillRule="nonzero"
      >
        <g id="Group-12-Copy-2" transform="translate(165.000000, 334.000000)">
          <polygon
            className="sl-primary-rect"
            id="Rectangle-25"
            fill={isPrimary ? '#0058B0' : '#96A9B2'}
            points="0 0 10 0 10 6 0 6"
          />
          <polygon id="Rectangle-25-Copy-5" fill="#96A9B2" points="11 0 21 0 21 6 11 6" />
          <polygon id="Rectangle-25-Copy-6" fill="#96A9B2" points="11 7 21 7 21 13 11 13" />
          <polygon id="Rectangle-25-Copy-7" fill="#96A9B2" points="0 7 10 7 10 13 0 13" />
        </g>
      </g>
    </g>
  </svg>
);

QuadCam.propTypes = {
  isPrimary: PropTypes.bool,
};

QuadCam.defaultProps = {
  isPrimary: false,
};

export default QuadCam;
