import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import MetaTags from '../MetaTags';
import RegionalForecastMeta from './RegionalForecastMeta';

describe('components / RegionalForecastMeta', () => {
  it('passes forecast data values to MetaTags', () => {
    const forecastData = {
      entitlements: [],
      reports: [],
      subregionId: '58581a836630e24c44878fd6',
      subregionName: 'North Orange County',
      subregionSpots: [],
      timestamp: 1486139669,
      forecastStatus: {
        status: 'active',
        inactiveMessage: 'Inactive status message for North Orange County.',
      },
      forecaster: {
        name: 'Ben Holtzclaw',
        title: 'FishTrack Product Owner',
        iconUrl: 'https://www.gravatar.com/avatar/412e8d86271169376b0e5b20423f253c?d=mm',
      },
      highlights: [
        'Modest blend of new WNW swell-mix',
        'Better breaks are knee-waist high',
        'Top spots hit shoulder-high',
      ],
      nextForecast: {
        timestamp: 1631818800,
        utcOffset: -7,
      },
      mapBounds: {
        north: 33.740402112054994,
        south: 33.56239,
        east: -117.83335,
        west: -118.1111921691,
      },
    };
    const title = 'North Orange County Surf Forecast: 16-Day Surf Report & Live Cams - Surfline';
    const description =
      'The most accurate and trusted North Orange County surf reports, forecasts, and coastal weather. Surfers from around the world choose Surfline for daily forecasts and timely news.';
    const urlPath = '/surf-forecasts/north-orange-county/58581a836630e24c44878fd6';
    const wrapper = shallow(
      <RegionalForecastMeta
        subregionId={forecastData.subregionId}
        subregionName={forecastData.subregionName}
      />,
    );
    const metaTagsWrapper = wrapper.find(MetaTags);
    expect(metaTagsWrapper.prop('title')).to.equal(title);
    expect(metaTagsWrapper.prop('description')).to.equal(description);
    expect(metaTagsWrapper.prop('urlPath')).to.equal(urlPath);
  });
});
