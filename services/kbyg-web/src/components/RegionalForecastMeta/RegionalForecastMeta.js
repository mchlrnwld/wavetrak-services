import PropTypes from 'prop-types';
import React from 'react';
import { pageStructuredData } from '@surfline/web-common';
import { subregionForecastPath } from '../../utils/urls';
import MetaTags from '../MetaTags';

const title = (subregionName) =>
  `${subregionName} Surf Forecast: 16-Day Surf Report & Live Cams - Surfline`;
const description = (subregionName) =>
  `The most accurate and trusted ${subregionName} surf reports, forecasts, and coastal weather. Surfers from around the world choose Surfline for daily forecasts and timely news.`;
const urlPath = (subregionId, subregionName) => subregionForecastPath(subregionId, subregionName);

const RegionalForecastMeta = ({ subregionId, subregionName }) => (
  <MetaTags
    title={title(subregionName)}
    description={description(subregionName)}
    urlPath={urlPath(subregionId, subregionName)}
  >
    <script type="application/ld+json">
      {pageStructuredData(
        {
          type: 'FAQPage',
          author: 'Surfline',
          authorType: 'Organization',
        },
        {
          mainEntity: [
            {
              '@type': 'Question',
              name: `What's the Best Bet for ${subregionName} surf?`,
              acceptedAnswer: {
                '@type': 'Answer',
                text: `Surfline Premium subscriptions provide for access to curated ${subregionName} forecast condition reports and proprietary surf, wind and buoy data. Plan your next trip or session with Surfline's expert forecast guidance.`,
              },
            },
            {
              '@type': 'Question',
              name: 'What is LOLA?',
              acceptedAnswer: {
                '@type': 'Answer',
                text: "LOLA is Surfline's global swell model that is continuously updated and corrected with real-time information. LOLA is a combination of many things - she's a computer model that crunches data, she gathers real-time data from offshore buoys, and she taps into orbiting satellites to measure hurricane-force winds and tremendous wave heights around the world. Over the years we have made a number of enhancements to LOLA. This includes a proprietary extended forecast algorithm, high-res wind charts, nearshore model enhancements, and revised LOLA charts.",
              },
            },
            {
              '@type': 'Question',
              name: "How can I better understand Surfline's forecast products?",
              acceptedAnswer: {
                '@type': 'Answer',
                text: 'Surfline offers numerous forecast products so you can \'Know Before You Go\'. The forecasters have created tutorials in order to help you get familiar with our products and learn how they are used. Check out <a href="https://support.surfline.com/hc/en-us/articles/115000015051-How-can-I-better-understand-Surfline-s-forecast-products-">these links for more information</a>.',
              },
            },
          ],
        },
      )}
    </script>
  </MetaTags>
);

RegionalForecastMeta.propTypes = {
  subregionId: PropTypes.string.isRequired,
  subregionName: PropTypes.string.isRequired,
};

export default RegionalForecastMeta;
