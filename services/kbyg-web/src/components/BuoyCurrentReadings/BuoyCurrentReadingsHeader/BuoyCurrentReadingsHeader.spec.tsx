import React from 'react';

import { expect } from 'chai';
import { mount } from 'enzyme';
import Skeleton from 'react-loading-skeleton';
import { ErrorBox } from '@surfline/quiver-react';
import BuoyCurrentReadingsHeader, { Props } from './BuoyCurrentReadingsHeader';

describe('components / BuoyCurrentReadingsHeader', () => {
  const props: Props = {
    error: false,
    loading: false,
    dateString: 'Tuesday, June 6 at 9pm',
    abbrTimezone: 'PST',
    status: 'ONLINE',
  };
  const propsWithLoading: Props = {
    error: false,
    loading: true,
    dateString: 'Tuesday, June 6 at 9pm',
    abbrTimezone: 'PST',
  };
  const propsWithError: Props = {
    error: true,
    loading: false,
    dateString: 'Tuesday, June 6 at 9pm',
    abbrTimezone: 'PST',
  };
  const propsWithOffline: Props = {
    error: true,
    loading: false,
    dateString: 'Tuesday, June 6 at 9pm',
    abbrTimezone: 'PST',
    status: 'OFFLINE',
  };

  it('renders header', () => {
    const wrapper = mount(<BuoyCurrentReadingsHeader {...props} />);

    const header = wrapper.find('.buoyReadingsHeaderTitle');

    expect(header).to.have.length(1);
    expect(header.text()).to.contain('Latest Reading');
  });

  it('renders offline header', () => {
    const wrapper = mount(<BuoyCurrentReadingsHeader {...propsWithOffline} />);

    const header = wrapper.find('.buoyReadingsHeaderOffline');

    expect(header).to.have.length(1);
  });

  it('renders subheader', () => {
    const wrapper = mount(<BuoyCurrentReadingsHeader {...props} />);

    const subHeader = wrapper.find('.buoyReadingsHeaderSubTitle');

    expect(subHeader.first().text()).to.equal(
      `Last updated ${props.dateString} ${props.abbrTimezone}`,
    );
  });

  it('renders loading skeleton', () => {
    const wrapper = mount(<BuoyCurrentReadingsHeader {...propsWithLoading} />);

    const LoadingSkeletonInstance = wrapper.find(Skeleton);
    // 4 is number of loading skeleton lines (mobile and up)
    expect(LoadingSkeletonInstance).to.have.length(2);
  });

  it('renders error header and errorbox', () => {
    const wrapper = mount(<BuoyCurrentReadingsHeader {...propsWithError} />);

    const errorHeader = wrapper.find('.buoyReadingsHeaderErrorSubTitle');
    const ErrorBoxInstance = wrapper.find(ErrorBox);

    expect(errorHeader).to.have.length(1);
    expect(ErrorBoxInstance).to.have.length(1);
  });
});
