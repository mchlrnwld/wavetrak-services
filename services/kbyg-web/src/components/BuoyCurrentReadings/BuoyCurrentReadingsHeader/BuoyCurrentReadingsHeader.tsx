import React from 'react';
import classNames from 'classnames';
import Skeleton from 'react-loading-skeleton';
import { ErrorBox } from '@surfline/quiver-react';

import { BuoyStatus } from '../../../types/buoys';

import styles from './BuoyCurrentReadingsHeader.module.scss';

export interface Props {
  error?: boolean;
  loading?: boolean;
  dateString?: string;
  abbrTimezone?: string;
  status?: BuoyStatus;
}

const getClass = (error?: boolean, loading?: boolean, status?: BuoyStatus) =>
  classNames({
    [styles.buoyReadingsHeader]: true,
    [styles.buoyReadingsHeaderLoading]: loading,
    [styles.buoyReadingsHeaderError]: error,
    [styles.buoyReadingsHeaderOffline]: status === 'OFFLINE',
  });

const BuoyCurrentReadingsHeader: React.FC<Props> = ({
  error,
  loading,
  dateString,
  abbrTimezone,
  status,
}) => {
  const lastUpdated = dateString ? `Last updated ${dateString} ${abbrTimezone}` : '';

  return (
    <div className={getClass(error, loading, status)}>
      <h1 className={styles.buoyReadingsHeaderTitle}>
        {loading ? <Skeleton width={150} /> : 'Latest Reading'}
      </h1>
      <p className={styles.buoyReadingsHeaderSubTitle}>
        {loading ? <Skeleton width={200} /> : lastUpdated}
      </p>
      {error && (
        <div className={styles.buoyReadingsHeaderErrorSubTitle}>
          <ErrorBox message="Oh Buoy! No Data Found." />
        </div>
      )}
    </div>
  );
};

export default BuoyCurrentReadingsHeader;
