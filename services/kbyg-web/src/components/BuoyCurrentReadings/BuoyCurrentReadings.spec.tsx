import { expect } from 'chai';

import BuoyCurrentReadings, { Props } from './BuoyCurrentReadings';
import BuoyReadingMetric from '../BuoyReadingMetric';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import BuoyReadingSwells from '../BuoyReadingSwells';
import BuoyReadingWind from '../BuoyReadingWind';
import MapTile from '../MapTile/MapTile';
import { MDY } from '../../common/constants';
import BuoyCurrentReadingsHeader from './BuoyCurrentReadingsHeader';
import { AppState } from '../../stores';

describe('components / BuoyCurrentReadings', () => {
  const buoyCurrentReadingsProps: Props = {
    timestamp: 1622608918,
    period: 15,
    height: 3.4,
    airTemperature: 80,
    waterTemperature: 68,
    swells: [
      {
        period: 1.03,
        height: 5.114,
        direction: 180,
      },
      {
        period: 15,
        height: 5.114,
        direction: 20,
      },
      {
        period: 20,
        height: 1.567,
        direction: 215,
      },
    ],
    wind: {
      speed: 7,
      direction: 359,
      gust: 10,
    },
    loading: false,
  };

  const noSwellsProps = {
    timestamp: 1622608918,
    period: 15,
    height: 3.4,
    direction: 250,
    airTemperature: 80,
    waterTemperature: 68,
    swells: [],
    wind: {
      speed: 7,
      direction: 359,
      gust: 10,
    },
    loading: false,
  };

  const defaultState = {
    backplane: {
      user: {
        settings: {
          units: {
            surfHeight: 'FT',
            swellHeight: 'FT',
            tideHeight: 'FT',
            windSpeed: 'MPH',
            temperature: 'F',
          },
          date: {
            format: MDY,
          },
        },
      },
    },
  };

  const defaultStateDMY = {
    backplane: {
      user: {
        settings: {
          units: {
            surfHeight: 'FT',
            swellHeight: 'FT',
            tideHeight: 'FT',
            windSpeed: 'MPH',
            temperature: 'F',
          },
          date: {
            format: 'DMY',
          },
        },
      },
    },
  };

  it('renders 6 buoy reading metric components', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyCurrentReadings, buoyCurrentReadingsProps, {
      initialState: defaultState as AppState,
    });
    const BuoyReadingMetricInstance = wrapper.find(BuoyReadingMetric);
    expect(BuoyReadingMetricInstance).to.have.length(6);
  });

  it('renders 1 buoy reading swell component', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyCurrentReadings, buoyCurrentReadingsProps, {
      initialState: defaultState as AppState,
    });
    const BuoyReadingSwellsInstance = wrapper.find(BuoyReadingSwells);
    expect(BuoyReadingSwellsInstance).to.have.length(1);
  });

  it('renders 1 buoy reading wind component', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyCurrentReadings, buoyCurrentReadingsProps, {
      initialState: defaultState as AppState,
    });
    const BuoyReadingWindInstance = wrapper.find(BuoyReadingWind);
    expect(BuoyReadingWindInstance).to.have.length(1);
  });

  it('renders 1 map tile component', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyCurrentReadings, buoyCurrentReadingsProps, {
      initialState: defaultState as AppState,
    });
    const MapTileInstance = wrapper.find(MapTile);
    expect(MapTileInstance).to.have.length(1);
  });

  it('should format the date with MDY', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyCurrentReadings, buoyCurrentReadingsProps, {
      initialState: defaultState as AppState,
    });
    const ReadingsHeader = wrapper.find(BuoyCurrentReadingsHeader);
    expect(ReadingsHeader).to.have.length(1);

    expect(ReadingsHeader.prop('dateString')).to.be.equal('Wednesday, June 2 at 04:41am');
  });

  it('should format the date with DMY', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyCurrentReadings, buoyCurrentReadingsProps, {
      initialState: defaultStateDMY as AppState,
    });
    const ReadingsHeader = wrapper.find(BuoyCurrentReadingsHeader);
    expect(ReadingsHeader).to.have.length(1);

    expect(ReadingsHeader.prop('dateString')).to.be.equal('Wednesday, 2 June at 04:41am');
  });

  it('should render even with no swells', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyCurrentReadings, noSwellsProps, {
      initialState: defaultStateDMY as AppState,
    });
    const swells = wrapper.find(BuoyReadingSwells);
    expect(swells).to.have.length(1);
    expect(swells.prop('swells')).to.deep.equal([]);
  });
});
