import React, { useMemo } from 'react';
import { SerializedError } from '@reduxjs/toolkit';
import { getLocalDate, getUserSettings } from '@surfline/web-common';
import { format } from 'date-fns';
import BuoyReadingMetric from '../BuoyReadingMetric/BuoyReadingMetric';
import BuoyCurrentReadingsHeader from './BuoyCurrentReadingsHeader';
import BuoyReadingSwells from '../BuoyReadingSwells';
import BuoyReadingWind from '../BuoyReadingWind';

import MapTile from '../MapTile/MapTile';
import { MDY } from '../../common/constants';

import { BuoyStatus, StationData } from '../../types/buoys';
import { Units } from '../../types/units';

import styles from './BuoyCurrentReadings.module.scss';
import { useAppSelector } from '../../stores/hooks';

const formatTemp = (unit: string) => `º${unit}`;

export interface Props {
  swells?: StationData['swells'];
  wind?: StationData['wind'];
  airTemperature?: StationData['airTemperature'];
  waterTemperature?: StationData['waterTemperature'];
  height?: StationData['height'];
  period?: StationData['period'];
  timestamp?: StationData['timestamp'];
  pressure?: StationData['pressure'];
  dewPoint?: StationData['dewPoint'];
  utcOffset?: StationData['utcOffset'];
  loading?: boolean;
  lat?: number;
  lon?: number;
  error?: SerializedError | Error;
  abbrTimezone?: string;
  status?: BuoyStatus;
}

const BuoyCurrentReadings: React.FC<Props> = ({
  swells = [],
  wind,
  airTemperature,
  waterTemperature,
  height,
  period,
  timestamp,
  pressure,
  dewPoint,
  loading,
  lat,
  lon,
  error,
  abbrTimezone,
  utcOffset = 0,
  status,
}) => {
  const settings = useAppSelector(getUserSettings);
  const { units, date } = settings || {};

  const swellsWithIndex = useMemo(() => {
    if (swells.length) {
      return swells.map((swell, i) => ({ ...swell, index: i }));
    }
    return swells;
  }, [swells]);

  const dateFormat =
    date?.format === MDY ? "EEEE, MMMM d 'at' hh:mmaaa" : "EEEE, d MMMM 'at' hh:mmaaa";
  const dateString = timestamp ? format(getLocalDate(timestamp, utcOffset), dateFormat) : '';
  const formattedTemp = formatTemp(units?.temperature || 'F');
  const incompleteReading = !height && !period;

  return (
    <section className={styles.buoyCurrentReadings}>
      <BuoyCurrentReadingsHeader
        error={!!error}
        loading={loading}
        status={status}
        dateString={dateString}
        abbrTimezone={abbrTimezone}
      />
      <div className={styles.buoyCurrentReadingsDividerContainer}>
        <hr className={styles.buoyCurrentReadingsDivider} />
      </div>
      <BuoyReadingMetric
        classes={{ root: styles.metricWaveHeight }}
        measurement={height}
        units={units?.swellHeight}
        title="Wave Height"
        altTitle="Wave Height"
        metric="wave-height"
        loading={loading}
        incompleteReading={incompleteReading}
      />
      <BuoyReadingMetric
        classes={{ root: styles.metricWavePeriod }}
        measurement={period}
        units="s"
        title="Wave Period"
        altTitle="Wave Period"
        metric="wave-period"
        loading={loading}
        incompleteReading={incompleteReading}
      />
      <BuoyReadingMetric
        classes={{ root: styles.metricWaterTemp }}
        measurement={waterTemperature}
        units={formattedTemp}
        title="Water Temperature"
        metric="water-temp"
        loading={loading}
        incompleteReading={incompleteReading}
      />
      <BuoyReadingMetric
        classes={{ root: styles.metricAirTemp }}
        measurement={airTemperature}
        units={formattedTemp}
        title="Air Temperature"
        metric="air-temp"
        loading={loading}
        incompleteReading={incompleteReading}
      />
      <BuoyReadingSwells
        classes={{ root: styles.metricSwells }}
        swells={swellsWithIndex}
        units={units as Units}
        loading={loading}
      />
      <BuoyReadingWind
        classes={{ root: styles.metricWind }}
        wind={wind}
        units={units as Units}
        loading={loading}
        error={!!error}
      />
      <BuoyReadingMetric
        classes={{ root: styles.metricPressure }}
        measurement={pressure}
        units="mb"
        title="Sea Level Pressure"
        metric="pressure"
        loading={loading}
        incompleteReading={incompleteReading}
      />
      <BuoyReadingMetric
        classes={{ root: styles.metricDewPoint }}
        measurement={dewPoint}
        units={formattedTemp}
        title="Dew Point"
        metric="dew-point"
        loading={loading}
        incompleteReading={incompleteReading}
      />
      <MapTile
        classes={{ root: styles.mapTile }}
        lat={lat}
        lon={lon}
        loading={loading}
        status={status}
      />
    </section>
  );
};

export default BuoyCurrentReadings;
