import React, { Dispatch, SetStateAction } from 'react';
import classnames from 'classnames';
import { getLocalDate } from '@surfline/web-common';
import { createAccessibleOnClick } from '@surfline/quiver-react';
import { format, differenceInSeconds, differenceInMinutes } from 'date-fns';

import styles from './SessionWaves.module.scss';
import { SessionWave } from '../../types/sessions';

const waveClass = (wave: SessionWave, activeWave?: SessionWave) =>
  classnames({
    [styles.wave]: true,
    [styles.waveActive]: wave?.id === activeWave?.id,
  });

const calculateWaveTime = (start: Date, end: Date) => {
  const timeMins = differenceInMinutes(end, start);
  const timeSeconds = differenceInSeconds(end, start) % 60;

  return `${timeMins}:${timeSeconds >= 10 ? timeSeconds : `0${timeSeconds}`}`;
};

interface Props {
  waves: SessionWave[];
  units: {
    distance: 'M' | 'FT';
    speed: 'MPH' | 'KPH';
  };
  activeWave?: SessionWave;
  setActiveWave: Dispatch<SetStateAction<SessionWave | undefined>>;
  setActiveClipIndex: (index: number) => void;
  utcOffset: number;
}

const SessionWaves: React.FC<Props> = ({
  waves,
  units,
  setActiveWave,
  activeWave,
  setActiveClipIndex,
  utcOffset,
}) => (
  <div>
    {waves.map((wave, index) => {
      const waveStart = getLocalDate(wave.startTimestamp / 1000, utcOffset);
      const waveEnd = getLocalDate(wave.endTimestamp / 1000, utcOffset);
      return (
        <button
          key={wave.id}
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...createAccessibleOnClick(() => {
            setActiveWave(wave);
            setActiveClipIndex(0);
          }, 'button')}
          className={waveClass(wave, activeWave)}
          type="button"
        >
          <div>
            <h6 className={styles.waveHeaderTitle}>
              Wave
              {index + 1}
            </h6>
          </div>
          <div className={styles.waveDetails}>
            <p className={styles.waveDetailsItem}>{format(waveStart, "h':'mmaaaaa'm'")}</p>
            <p className={styles.waveDetailsItem}>{calculateWaveTime(waveStart, waveEnd)}</p>
            <p className={styles.waveDetailsItem}>
              {Math.round(wave.distance)}
              {units.distance?.toLowerCase()}
            </p>
            <p className={styles.waveDetailsItem}>
              {Math.round(wave.speedMax)}
              {units.speed?.toLowerCase()}
            </p>
          </div>
        </button>
      );
    })}
  </div>
);

export default SessionWaves;
