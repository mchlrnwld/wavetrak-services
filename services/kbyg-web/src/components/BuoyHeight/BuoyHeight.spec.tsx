import React from 'react';

import { expect } from 'chai';
import { mount } from 'enzyme';

import BuoyHeight from './BuoyHeight';

const classes = {
  root: 'root',
  height: 'height',
  units: 'units',
};

describe('components / BuoyHeight', () => {
  it('should render a height with units', () => {
    const wrapper = mount(<BuoyHeight classes={classes} height={5.5} units="FT" />);
    const height = wrapper.find('.height');
    const units = wrapper.find('.units');

    expect(height.text()).to.equal('5.5');
    expect(units.text()).to.equal('ft');
  });

  it('should render a dash if the height is 0', () => {
    const wrapper = mount(<BuoyHeight classes={classes} height={0} units="FT" />);
    const height = wrapper.find('.height');
    const units = wrapper.find('.units');

    expect(height.text()).to.equal('- ');
    expect(units.text()).to.equal('ft');
  });

  it('should render a dash if the height is null', () => {
    const wrapper = mount(<BuoyHeight classes={classes} units="FT" />);
    const height = wrapper.find('.height');
    const units = wrapper.find('.units');

    expect(height.text()).to.equal('- ');
    expect(units.text()).to.equal('ft');
  });
});
