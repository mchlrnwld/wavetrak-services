import React from 'react';
import { isNumber } from 'lodash';
import { SurfHeight } from '../../types/units';

interface Props {
  height?: number;
  units?: SurfHeight;
  classes?: {
    root?: string;
    height?: string;
    units?: string;
  };
}

const BuoyHeight: React.FC<Props> = ({ height = 0, units, classes = {} }) => (
  <div className={classes.root}>
    <span className={classes.height}>{isNumber(height) && height !== 0 ? height : '- '}</span>
    <span className={classes.units}>{units?.toLowerCase()}</span>
  </div>
);

export default BuoyHeight;
