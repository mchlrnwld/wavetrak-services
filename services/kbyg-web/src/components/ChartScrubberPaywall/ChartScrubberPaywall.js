import PropTypes from 'prop-types';
import React, { useRef, useMemo } from 'react';
import { TrackableLink } from '@surfline/quiver-react';
import scrubberStatePropTypes from '../../propTypes/scrubberstate';
import config from '../../config';
import overviewDataPropTypes from '../../propTypes/subregionOverviewData';
import { chartSegmentPropsPropTypes } from '../../propTypes/charts';
import { CLICKED_SUBSCRIBE_CTA } from '../../common/constants';

const ChartScrubberPaywall = ({
  scrubberState,
  className,
  jumpToIndex,
  timestepsAvailable,
  overviewData,
  chartSegmentProps,
}) => {
  const subscribeLinkRef = useRef();
  const subscribeEventProperties = useMemo(
    () => ({
      location: 'chart - scrubber',
      subregionId: overviewData._id,
      category: chartSegmentProps.category,
      pageName: chartSegmentProps.pageName,
    }),
    [overviewData._id, chartSegmentProps.category, chartSegmentProps.pageName],
  );

  return (
    <div
      className={className}
      style={{
        width: scrubberState.paywallWidth,
        left: scrubberState.paywallLeft,
      }}
    >
      <div
        className={`${className}__content`}
        onMouseEnter={() => jumpToIndex(timestepsAvailable, false)}
      >
        <p>
          Score great waves with long-range charts
          <TrackableLink
            ref={subscribeLinkRef}
            eventName={CLICKED_SUBSCRIBE_CTA}
            eventProperties={subscribeEventProperties}
          >
            <a ref={subscribeLinkRef} href={config.funnelUrl}>
              Start Free Trial
            </a>
          </TrackableLink>
        </p>
      </div>
    </div>
  );
};

ChartScrubberPaywall.propTypes = {
  scrubberState: scrubberStatePropTypes.isRequired,
  className: PropTypes.string.isRequired,
  timestepsAvailable: PropTypes.number,
  jumpToIndex: PropTypes.func,
  overviewData: overviewDataPropTypes.isRequired,
  chartSegmentProps: chartSegmentPropsPropTypes.isRequired,
};

ChartScrubberPaywall.defaultProps = {
  timestepsAvailable: null,
  jumpToIndex: () => null,
};

export default ChartScrubberPaywall;
