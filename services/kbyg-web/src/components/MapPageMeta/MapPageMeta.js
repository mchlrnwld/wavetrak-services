import React from 'react';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import { pageStructuredData, placeStructuredData } from '@surfline/web-common';
import MetaTags from '../MetaTags';
import {
  getBuoyMapPathWithLocationObject,
  getSpotMapPath,
  getSpotMapPathWithLocation,
} from '../../utils/urls/mapPaths';
import config from '../../config';
import locationPropType from '../../propTypes/map/location';

/**
 * @param {import('../../propTypes/map/location').Location} location
 */
const urlPath = (location) => getBuoyMapPathWithLocationObject(location);

const MapPageMeta = ({ buoyMap = false, location }) => {
  const router = useRouter();
  // Regular Map Page:
  if (!buoyMap) {
    const loc = router.query.location;
    return (
      <MetaTags
        title="Local surf reports, swell forecasts, and surf cams - Surfline"
        description="The most accurate and trusted surf reports, forecasts, and coastal weather. Surfers from around the world choose Surfline for dependable and up to date surfing forecasts and high quality surf content, live surf cams, and features."
        urlPath={loc ? getSpotMapPathWithLocation(loc) : getSpotMapPath()}
      />
    );
  }

  // Buoy Map Page:
  const pageTitle =
    'Global Map of Wave Buoys with Swell Height, Peak Period & Wave Direction - Surfline';
  const metaDescription =
    'The most accurate and trusted surf reports, forecasts, and coastal weather. Surfers from around the world choose Surfline for buoy swell reports with the latest in wave height, peak period and mean wave direction.';

  const url = `${config.surflineHost}${urlPath(location)}`;

  // Midnight of current day
  const currentDay = new Date();
  const startOfCurrentDayUTC = new Date(currentDay.setUTCHours(0, 0, 0, 0));

  return (
    <MetaTags title={pageTitle} description={metaDescription} urlPath={urlPath(location)}>
      <script type="application/ld+json">
        {pageStructuredData({
          url,
          headline: pageTitle,
          description: metaDescription,
          dateModified: startOfCurrentDayUTC,
          publisher: 'Surfline',
        })}
      </script>
      <script type="application/ld+json">
        {placeStructuredData('Wave Buoys', location?.center?.lat, location?.center?.lon)}
      </script>
    </MetaTags>
  );
};

MapPageMeta.propTypes = {
  buoyMap: PropTypes.bool,
  location: locationPropType,
};

MapPageMeta.defaultProps = {
  buoyMap: false,
  location: null,
};

export default MapPageMeta;
