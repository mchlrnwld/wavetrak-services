import { expect } from 'chai';
import MetaTags from '../MetaTags';
import MapPageMeta from './MapPageMeta';
import { mountWithReduxAndRouter } from '../../utils/test-utils';

jest.mock('../MetaTags', () => {
  const Page = () => <div className="page">Page</div>;
  return Page;
});

describe('components / MapPageMeta', () => {
  afterAll(() => {
    jest.unmock('../MetaTags');
  });
  it('renders MetaTags with appropriate props', () => {
    const title = 'Local surf reports, swell forecasts, and surf cams - Surfline';
    const description =
      'The most accurate and trusted surf reports, forecasts, and coastal weather. Surfers from around the world choose Surfline for dependable and up to date surfing forecasts and high quality surf content, live surf cams, and features.';
    const urlPath = '/surf-reports-forecasts-cams-map';

    const { wrapper } = mountWithReduxAndRouter(MapPageMeta);
    const metaTags = wrapper.find(MetaTags);

    expect(metaTags).to.have.length(1);
    expect(metaTags.prop('title')).to.equal(title);
    expect(metaTags.prop('description')).to.equal(description);
    expect(metaTags.prop('urlPath')).to.equal(urlPath);
  });

  it('renders Buoy Map Page MetaTags with appropriate props', () => {
    const title =
      'Global Map of Wave Buoys with Swell Height, Peak Period & Wave Direction - Surfline';
    const description =
      'The most accurate and trusted surf reports, forecasts, and coastal weather. Surfers from around the world choose Surfline for buoy swell reports with the latest in wave height, peak period and mean wave direction.';
    const location = {
      center: { lat: 34.08906131584996, lon: -119.33898925781251 },
      zoom: 8,
    };
    const urlPath = `/surf-reports-forecasts-cams-map/buoys/@${location.center.lat},${location.center.lon},${location.zoom}z`;
    const { wrapper } = mountWithReduxAndRouter(MapPageMeta, { buoyMap: true, location });
    const metaTags = wrapper.find(MetaTags);

    expect(metaTags).to.have.length(1);
    expect(metaTags.prop('title')).to.equal(title);
    expect(metaTags.prop('description')).to.equal(description);
    expect(metaTags.prop('urlPath')).to.equal(urlPath);
  });
});
