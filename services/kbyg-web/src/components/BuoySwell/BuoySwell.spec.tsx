import React from 'react';

import { expect } from 'chai';
import { mount } from 'enzyme';

import BuoySwell from './BuoySwell';
import BuoyHeight from '../BuoyHeight';
import BuoyPeriod from '../BuoyPeriod';
import BuoyDirection from '../BuoyDirection';

describe('components / BuoySwell', () => {
  it('should render a buoy swell', () => {
    const wrapper = mount(<BuoySwell height={5.56} period={10} direction={200} units="FT" />);

    const height = wrapper.find(BuoyHeight);
    const period = wrapper.find(BuoyPeriod);
    const direction = wrapper.find(BuoyDirection);

    expect(height).to.have.length(1);
    expect(period).to.have.length(1);
    expect(direction).to.have.length(1);

    expect(height.props()).to.deep.equal({ height: 5.56, units: 'FT' });
    expect(period.props()).to.deep.equal({ period: 10 });
    expect(direction.props()).to.deep.equal({ direction: 200 });
  });
});
