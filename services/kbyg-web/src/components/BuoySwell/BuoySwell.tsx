import React from 'react';

import BuoyHeight from '../BuoyHeight';
import BuoyPeriod from '../BuoyPeriod';
import BuoyDirection from '../BuoyDirection';
import { SurfHeight } from '../../types/units';

import styles from './BuoySwell.module.scss';

interface Props {
  height?: number;
  period?: number;
  direction?: number;
  units: SurfHeight;
}

const BuoySwell: React.FC<Props> = ({ height, period, direction, units }) => (
  <div className={styles.buoySwell}>
    <BuoyHeight height={height} units={units} />
    {', '}
    <BuoyPeriod period={period} />
    {', '}
    <BuoyDirection direction={direction} />
  </div>
);

export default BuoySwell;
