import React, { useEffect, useRef, useState } from 'react';
import { format, getTime } from 'date-fns';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import smoothscroll from 'smoothscroll-polyfill';
import { createAccessibleOnClick } from '@surfline/quiver-react';
import { canUseDOM, getLocalDate, getWindow, trackEvent } from '@surfline/web-common';
import clipPropType from '../../propTypes/clip';
import styles from './CameraClips.module.scss';

// smooth scrolling ensures click doesn't fire on a random element when the scroll finishes
// (as sometimes a touch is fired after the click and that seems to fire
// wherever the click was on screen rather than on dom)
// this is a polyfill so that it's supported on safari
if (canUseDOM) {
  smoothscroll.polyfill();
}

const clipClass = (clip, activeClip) =>
  classnames({
    [styles.cameraClip]: true,
    [styles.cameraClipActive]: clip?.startDate === activeClip?.startDate,
  });

const CameraClips = ({
  clips,
  setActiveClip,
  activeClip,
  utcOffset,
  isPremium,
  segmentProperties,
  daysAgo,
}) => {
  const daytimeClipRef = useRef();
  const [firstClip, setFirstClip] = useState(clips[0]);

  useEffect(() => {
    if (clips.length > 0 && daysAgo === 0) setActiveClip(clips[0]);

    if (daysAgo > 0) {
      daytimeClipRef.current?.scrollIntoView({
        behavior: 'smooth',
        block: 'nearest',
      });

      setActiveClip(firstClip);
    }
  }, [daysAgo, clips, firstClip, setActiveClip]);

  return (
    <div className={styles.cameraClips}>
      {clips.length > 0 || !isPremium ? (
        clips.map((clip, index) => {
          const clipStart = getLocalDate(getTime(new Date(clip.startDate)) / 1000, +utcOffset);
          const prevClipStart =
            index !== 0 &&
            getLocalDate(getTime(new Date(clips[index - 1].startDate) / 1000), +utcOffset);
          const clipStartString = format(clipStart, 'h a');
          const firstClipOfHour =
            !prevClipStart || format(prevClipStart, 'h a') !== clipStartString || index === 0;
          const isDefaultClip = firstClipOfHour && clipStartString.toLowerCase() === '8 am';

          if (isDefaultClip && daysAgo > 0 && firstClip?.startDate !== clip.startDate) {
            setFirstClip(clip);
          }
          return (
            <>
              {firstClipOfHour ? (
                <div className={styles.cameraClipsHeading}>{clipStartString.toLowerCase()}</div>
              ) : null}
              <button
                {...createAccessibleOnClick(() => {
                  const win = getWindow();
                  setActiveClip(clip);
                  // On tablet and mobile screen widths scroll back to the player when a new clip is selected
                  if (canUseDOM && win?.innerWidth < 977)
                    win?.scrollTo({
                      top: 100,
                      behavior: 'smooth',
                    });
                  trackEvent('Clicked Clip Selector', {
                    ...segmentProperties,
                    button: 'clip thumb',
                  });
                  return null;
                }, 'button')}
                key={clip.startDate}
                className={clipClass(clip, activeClip)}
                type="button"
                style={{ backgroundImage: `url('${clip.thumbSmallUrl}')` }}
                ref={isDefaultClip ? daytimeClipRef : null}
              >
                <h4 className={styles.cameraClipTime}>
                  {format(clipStart, 'h:mm a').toLowerCase()}
                </h4>
              </button>
            </>
          );
        })
      ) : (
        <div className={styles.cameraClipsNoClips}>
          Unfortunately there are no clips available for this day/cam yet, please select another day
          or try a different cam
        </div>
      )}
    </div>
  );
};

CameraClips.propTypes = {
  clips: PropTypes.arrayOf(clipPropType).isRequired,
  setActiveClip: PropTypes.func.isRequired,
  activeClip: clipPropType,
  utcOffset: PropTypes.number.isRequired,
  isPremium: PropTypes.bool.isRequired,
  segmentProperties: PropTypes.shape({
    pageName: PropTypes.string.isRequired,
    spotId: PropTypes.string.isRequired,
    spotName: PropTypes.string.isRequired,
    category: PropTypes.string.isRequired,
    camID: PropTypes.string.isRequired,
    camName: PropTypes.string.isRequired,
    premiumCam: PropTypes.bool.isRequired,
    daysAgo: PropTypes.number.isRequired,
    platform: PropTypes.string.isRequired,
  }).isRequired,
  daysAgo: PropTypes.number.isRequired,
};

CameraClips.defaultProps = {
  activeClip: {},
};

export default CameraClips;
