import React from 'react';

import { expect } from 'chai';
import { mount } from 'enzyme';
import { WindStats } from '@surfline/quiver-react';
import Skeleton from 'react-loading-skeleton';
import BuoyReadingWind from './BuoyReadingWind';
import BuoyReadingMetric from '../BuoyReadingMetric/BuoyReadingMetric';
import { Units } from '../../types/units';

describe('components / BuoyReadingWind', () => {
  const props = {
    wind: {
      speed: 7,
      direction: 359,
      gust: 10,
    },
    units: {
      windSpeed: 'MPH',
    } as Units,
    loading: false,
    error: false,
  };

  const propsWithNoWind = {
    wind: undefined,
    units: { windSpeed: 'MPH' } as Units,
    loading: false,
    error: false,
  };

  const propsWithLoading = {
    wind: undefined,
    units: { windSpeed: 'MPH' } as Units,
    loading: true,
  };

  const propsWithError = {
    error: true,
  };

  it('renders outer individual swells div', () => {
    const wrapper = mount(<BuoyReadingWind {...props} />);

    const outer = wrapper.find('.buoyReadingWind');

    expect(outer).to.have.length(1);
  });

  it('renders 1 WindStats component', () => {
    const wrapper = mount(<BuoyReadingWind {...props} />);

    const WindStatsInstance = wrapper.find(WindStats);
    expect(WindStatsInstance).to.have.length(1);
  });

  it('renders loading skeleton', () => {
    const wrapper = mount(<BuoyReadingWind {...propsWithLoading} />);

    const LoadingSkeletonInstance = wrapper.find(Skeleton);
    expect(LoadingSkeletonInstance).to.have.length(1);
  });

  it('renders no data on error', () => {
    const wrapper = mount(<BuoyReadingWind {...propsWithError} />);

    const BuoyReadingMetricInstance = wrapper.find(BuoyReadingMetric);
    expect(BuoyReadingMetricInstance).to.have.length(1);
    expect(BuoyReadingMetricInstance.find('span').text()).to.equal('No Data');
  });

  it('renders no data on null wind reading', () => {
    const wrapper = mount(<BuoyReadingWind {...propsWithNoWind} />);

    const BuoyReadingMetricInstance = wrapper.find(BuoyReadingMetric);
    expect(BuoyReadingMetricInstance).to.have.length(1);
    expect(BuoyReadingMetricInstance.find('span').text()).to.equal('No Data');
  });
});
