import { WindStats } from '@surfline/quiver-react';
import classNames from 'classnames';
import React from 'react';
import Skeleton from 'react-loading-skeleton';
import { Units } from '../../types/units';
import { Wind } from '../../types/wind';
import BuoyReadingMetric from '../BuoyReadingMetric';

import styles from './BuoyReadingWind.module.scss';

interface Props {
  wind?: Wind;
  units?: Units;
  loading?: boolean;
  error?: boolean;
  classes?: { root?: string };
}

const getClass = (rootClass?: string) => classNames(rootClass, styles.buoyReadingWind);

const BuoyReadingWind: React.FC<Props> = ({ wind, units, loading, error, classes = {} }) =>
  error || (!wind && !loading) ? (
    <BuoyReadingMetric
      classes={{ root: classes.root }}
      units=""
      title="Wind"
      metric="wind"
      loading={loading}
    />
  ) : (
    <div className={getClass(classes.root)}>
      <div className={styles.buoyReadingWindStats}>
        {!loading ? <WindStats wind={wind!} units={units!} /> : <Skeleton count={3} width={150} />}
      </div>
    </div>
  );

export default BuoyReadingWind;
