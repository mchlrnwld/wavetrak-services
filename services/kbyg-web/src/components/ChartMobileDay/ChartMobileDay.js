import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { format } from 'date-fns';
import { roundSurfHeight } from '@surfline/web-common';
import conditionClassModifier from '../../utils/conditionClassModifier';
import unitsPropType from '../../propTypes/units';
import summaryDayPropType from '../../propTypes/summaryDay';
import surfPropType from '../../propTypes/surf';

const formatRoundedSurfHeightString = (min, max, waveHeightUnit) => {
  if (max > 0.5 && max <= 1 && (waveHeightUnit === 'FT' || waveHeightUnit === 'HI'))
    return `0.5-1ft`;
  if (max > 0 && max <= 0.5 && (waveHeightUnit === 'FT' || waveHeightUnit === 'HI'))
    return `0-0.5ft`;
  const roundedMin = roundSurfHeight(min, waveHeightUnit);
  const roundedMax = roundSurfHeight(max, waveHeightUnit);
  return `${roundedMin}-${roundedMax}${waveHeightUnit === 'HI' ? 'FT' : waveHeightUnit}`;
};

const maxSpotSurfHeight = (waveDay, units) => {
  const max = Math.max(...waveDay.map(({ surf }) => surf.max));
  const wave = waveDay.find(({ surf }) => surf.max === max);
  return formatRoundedSurfHeightString(wave.surf.min, wave.surf.max, units.waveHeight);
};

const maxRegionalSurfHeight = (summaryDay, units) => {
  const { am, pm } = summaryDay;
  const selectedSummary = am.maxHeight < pm.maxHeight ? pm : am;

  const summaryMin = selectedSummary.minHeight;
  const summaryMax = selectedSummary.maxHeight;

  return formatRoundedSurfHeightString(summaryMin, summaryMax, units.waveHeight);
};

const conditionClassName = (condition, sevenRatings = false) =>
  classNames({
    'sl-chart-mobile-day__condition': true,
    [`sl-chart-mobile-day__condition${sevenRatings ? '-v2' : ''}--${conditionClassModifier(
      condition,
      sevenRatings,
    )}`]: !!condition,
  });

const chartMobileDayClassName = (obscured) =>
  classNames({
    'sl-chart-mobile-day': true,
    'sl-chart-mobile-day--blurred': obscured,
  });

const ChartMobileDay = ({
  localDate,
  waveDay,
  summaryDay,
  units,
  showConditions,
  obscured,
  type,
  dateFormat,
  sevenRatings,
}) => (
  <div className={chartMobileDayClassName(obscured)}>
    <div className="sl-chart-mobile-day__conditions">
      <div
        className={
          showConditions
            ? conditionClassName(summaryDay && summaryDay.am && summaryDay.am.rating, sevenRatings)
            : null
        }
      />
      <div
        className={
          showConditions
            ? conditionClassName(summaryDay && summaryDay.pm && summaryDay.pm.rating, sevenRatings)
            : null
        }
      />
    </div>
    <div className="sl-chart-mobile-day__day">
      {format(localDate, dateFormat === 'MDY' ? 'iii M/d' : 'iii d/M')}
    </div>
    {summaryDay && units && type === 'REGIONAL' ? (
      <div className="sl-chart-mobile-day__surf-height">
        {maxRegionalSurfHeight(summaryDay, units)}
      </div>
    ) : null}
    {waveDay && units && type === 'SPOT' ? (
      <div className="sl-chart-mobile-day__surf-height">{maxSpotSurfHeight(waveDay, units)}</div>
    ) : null}
  </div>
);

ChartMobileDay.propTypes = {
  localDate: PropTypes.instanceOf(Date).isRequired,
  waveDay: PropTypes.arrayOf(surfPropType),
  summaryDay: summaryDayPropType,
  units: unitsPropType,
  showConditions: PropTypes.bool,
  obscured: PropTypes.bool,
  type: PropTypes.string.isRequired,
  dateFormat: PropTypes.oneOf(['MDY', 'DMY']),
  sevenRatings: PropTypes.bool,
};

ChartMobileDay.defaultProps = {
  waveDay: null,
  summaryDay: null,
  units: null,
  showConditions: false,
  obscured: false,
  dateFormat: 'MDY',
  sevenRatings: false,
};

export default ChartMobileDay;
