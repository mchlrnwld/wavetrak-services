import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ChartMobileDay from './ChartMobileDay';

describe('components / ChartMobileDay', () => {
  const localDate = new Date('Fri Dec 01 2017 00:00:00').getTime();

  it('renders weekday, month and day', () => {
    const wrapper = shallow(<ChartMobileDay localDate={localDate} />);
    const day = wrapper.find('.sl-chart-mobile-day__day');
    expect(day).to.have.length(1);
    expect(day.text()).to.equal('Fri 12/1');
  });

  it('renders max surf heights if type spot and wave day provided', () => {
    const units = { waveHeight: 'FT' };
    const waveDay = [
      { surf: { min: 2, max: 3 } },
      { surf: { min: 1, max: 2 } },
      { surf: { min: 1, max: 4 } },
      { surf: { min: 0, max: 1 } },
    ];
    const wrapper = shallow(
      <ChartMobileDay localDate={localDate} waveDay={waveDay} units={units} type="SPOT" />,
    );
    const surfHeight = wrapper.find('.sl-chart-mobile-day__surf-height');
    expect(surfHeight).to.have.length(1);
    expect(surfHeight.text()).to.equal('1-4FT');
  });

  it('renders max surf heights if type regional and summary day provided', () => {
    const units = { waveHeight: 'FT' };
    const summaryDay = {
      am: { minHeight: 2, maxHeight: 3 },
      pm: { minHeight: 3, maxHeight: 5 },
    };
    const wrapper = shallow(
      <ChartMobileDay
        localDate={localDate}
        summaryDay={summaryDay}
        units={units}
        type="REGIONAL"
      />,
    );
    const surfHeight = wrapper.find('.sl-chart-mobile-day__surf-height');
    expect(surfHeight).to.have.length(1);
    expect(surfHeight.text()).to.equal('3-5FT');
  });

  it('renders AM/PM conditions if summaryDay provided and showConditions is true', () => {
    const noSummaryWrapper = shallow(<ChartMobileDay localDate={localDate} showConditions />);
    const noSummaryConditions = noSummaryWrapper.find('.sl-chart-mobile-day__condition');
    expect(noSummaryConditions).to.have.length(2);
    expect(noSummaryConditions.at(0).prop('className')).to.equal('sl-chart-mobile-day__condition');
    expect(noSummaryConditions.at(1).prop('className')).to.equal('sl-chart-mobile-day__condition');

    const summaryDay = {
      am: { rating: 'FAIR_TO_GOOD' },
      pm: { rating: 'GOOD' },
    };
    const summaryWrapper = shallow(
      <ChartMobileDay localDate={localDate} summaryDay={summaryDay} showConditions />,
    );
    const summaryConditions = summaryWrapper.find('.sl-chart-mobile-day__condition');
    expect(summaryConditions).to.have.length(2);
    expect(summaryConditions.at(0).prop('className')).to.equal(
      'sl-chart-mobile-day__condition sl-chart-mobile-day__condition--fair-to-good',
    );
    expect(summaryConditions.at(1).prop('className')).to.equal(
      'sl-chart-mobile-day__condition sl-chart-mobile-day__condition--good',
    );

    const hideConditionsWrapper = shallow(
      <ChartMobileDay localDate={localDate} summaryDay={summaryDay} />,
    );
    const hideConditionsConditions = hideConditionsWrapper.find('.sl-chart-mobile-day__condition');
    expect(hideConditionsConditions).to.have.length(0);
  });
});
