import React from 'react';
import { SpotDescriptor } from '@surfline/quiver-react';
import { upperCaseFirstLetter } from '../../utils/string';
import { abilityLevelsPropType } from '../../propTypes/travelDetails';

const AbilityLevels = ({ abilityLevels: { description, levels } }) => {
  // This array needs to be in sync with the graph for ability levels
  const allLevels = ['BEGINNER', 'INTERMEDIATE', 'ADVANCED'];
  const value = allLevels.every((val) => levels.includes(val))
    ? 'All Abilities'
    : levels.map((level) => upperCaseFirstLetter(level.toLowerCase())).join(' - ');

  const options = allLevels.map((level) => ({
    name: upperCaseFirstLetter(level.slice(0, 3)),
    value: levels.includes(level),
  }));

  return (
    <SpotDescriptor
      kind="Ability Level"
      value={value}
      options={options}
      description={description}
    />
  );
};

AbilityLevels.propTypes = abilityLevelsPropType;

export default AbilityLevels;
