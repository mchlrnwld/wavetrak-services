import React, { useMemo } from 'react';
import Skeleton from 'react-loading-skeleton';
import { isNumber } from 'lodash';
import { trackEvent } from '@surfline/web-common';

import { getBuoyMapPath, getBuoyMapPathWithLocationObject } from '../../utils/urls/mapPaths';

import styles from './NearbyBuoysHeader.module.scss';
import WavetrakLink from '../WavetrakLink';

export const DEFAULT_MAP_ZOOM = 8;

interface Props {
  loading?: boolean;
  latitude?: number;
  longitude?: number;
  trackLocation?: string;
  spotId?: string;
  spotName?: string;
}

const NearbyBuoysHeader: React.FC<Props> = ({
  loading,
  latitude,
  longitude,
  trackLocation,
  spotId,
  spotName,
}) => {
  const linkText = 'View Map';
  const linkLocation = useMemo(() => {
    if (isNumber(latitude) && isNumber(longitude)) {
      return getBuoyMapPathWithLocationObject({
        center: { lat: latitude, lon: longitude },
        zoom: DEFAULT_MAP_ZOOM,
      });
    }
    return getBuoyMapPath();
  }, [longitude, latitude]);

  const trackClickedViewMap = () => {
    const trackingProps = {
      category: 'buoys',
      location: trackLocation,
      linkName: linkText,
      linkUrl: linkLocation,
      pageName: trackLocation,
      spotId,
      spotName,
    };

    trackEvent('Clicked Link', trackingProps);
  };

  return (
    <header className={styles.nearbyBuoysHeader}>
      <h1 className={styles.nearbyBuoysTitle}>Nearby Buoys</h1>
      {loading ? (
        <Skeleton width={70} />
      ) : (
        <WavetrakLink href={linkLocation} className={styles.mapLink} onClick={trackClickedViewMap}>
          {linkText}
        </WavetrakLink>
      )}
    </header>
  );
};

export default NearbyBuoysHeader;
