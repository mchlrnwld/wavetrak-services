import React, { useMemo } from 'react';

import { useGetNearbyBuoysQuery } from '../../actions/buoys';

import NearbyBuoysHeader from './NearbyBuoysHeader';
import BuoyCarousel from '../BuoyCarousel';

import styles from './NearbyBuoys.module.scss';

interface Props {
  buoyId?: string;
  loading?: boolean;
  error?: boolean | string | Error;
  location?: {
    latitude: number;
    longitude: number;
  };
  trackLocation?: string;
  spotId?: string;
  spotName?: string;
}

const NearbyBuoys: React.FC<Props> = ({
  buoyId,
  loading,
  location,
  error,
  trackLocation,
  spotId,
  spotName,
}) => {
  const skip = !!(loading || error || !location);

  const {
    data: nearbyBuoys,
    error: nearbyError,
    isFetching: nearbyFetching,
    isUninitialized: nearbyUninitialized,
  } = useGetNearbyBuoysQuery(location!, { skip });

  const nearbyBuoysWithoutCurrentBuoy = useMemo(() => {
    if (nearbyBuoys?.data) {
      return nearbyBuoys.data.filter((nearbyBuoy) => nearbyBuoy.id !== buoyId);
    }
    return null;
  }, [nearbyBuoys, buoyId]);

  const nearbyLoading = nearbyFetching || nearbyUninitialized;

  if (nearbyBuoysWithoutCurrentBuoy?.length === 0) {
    return null;
  }

  return (
    <section className={styles.nearbyBuoys}>
      <NearbyBuoysHeader
        loading={loading}
        latitude={location?.latitude}
        longitude={location?.longitude}
        trackLocation={trackLocation}
        spotId={spotId}
        spotName={spotName}
      />
      <BuoyCarousel
        loading={nearbyLoading}
        error={error || (nearbyError as Error)}
        stations={nearbyBuoysWithoutCurrentBuoy || []}
        trackLocation={trackLocation}
      />
    </section>
  );
};

export default NearbyBuoys;
