import sinon, { SinonStub } from 'sinon';
import { expect } from 'chai';

import NearbyBuoys from './NearbyBuoys';
import NearbyBuoysHeader from './NearbyBuoysHeader';
import BuoyCarousel from '../BuoyCarousel';

import * as buoysActions from '../../actions/buoys';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import { getNearbyBuoysFixture } from '../../common/api/fixtures/buoys';

describe('components / NearbyBuoys', () => {
  let useGetBuoyNearbyQueryStub: SinonStub;

  beforeEach(() => {
    useGetBuoyNearbyQueryStub = sinon
      .stub(buoysActions, 'useGetNearbyBuoysQuery')
      .returns({ isUninitialized: true, refetch: () => {} });
  });

  afterEach(() => {
    useGetBuoyNearbyQueryStub.restore();
  });

  it('should skip calling the nearby endpoint if the details endpoint is loading', () => {
    mountWithReduxAndRouter(NearbyBuoys, { loading: true });

    expect(useGetBuoyNearbyQueryStub).to.have.been.calledOnceWithExactly(undefined, { skip: true });
  });

  it('should send the loading prop to the header', () => {
    const { wrapper } = mountWithReduxAndRouter(NearbyBuoys, { loading: true });

    expect(wrapper.find(NearbyBuoysHeader).prop('loading')).to.be.true();
  });

  it('should skip calling the nearby endpoint if the details endpoint errored', () => {
    mountWithReduxAndRouter(NearbyBuoys, { error: true });

    expect(useGetBuoyNearbyQueryStub).to.have.been.calledOnceWithExactly(undefined, { skip: true });
  });

  it('should pass the details error to the buoy carousel', () => {
    const { wrapper } = mountWithReduxAndRouter(NearbyBuoys, { error: true });

    expect(wrapper.find(BuoyCarousel).prop('error')).to.be.true();
  });

  it('should skip calling the nearby endpoint if the location is missing', () => {
    mountWithReduxAndRouter(NearbyBuoys, {});

    expect(useGetBuoyNearbyQueryStub).to.have.been.calledOnceWithExactly(undefined, { skip: true });
  });

  it('should call the nearby endpoint', () => {
    useGetBuoyNearbyQueryStub.returns({ data: getNearbyBuoysFixture });
    const location = { latitude: 1, longitude: 1 };
    mountWithReduxAndRouter(NearbyBuoys, { location });

    expect(useGetBuoyNearbyQueryStub).to.have.been.calledOnceWithExactly(location, { skip: false });
  });

  it('should filter the current buoy from the nearby endpoint', () => {
    useGetBuoyNearbyQueryStub.returns({ data: getNearbyBuoysFixture });
    const location = { latitude: 1, longitude: 1 };
    const { wrapper } = mountWithReduxAndRouter(NearbyBuoys, {
      location,
      buoyId: getNearbyBuoysFixture.data[0].id,
    });

    const carousel = wrapper.find(BuoyCarousel);

    expect(carousel.prop('stations')).to.deep.equal(getNearbyBuoysFixture.data.slice(1));
  });

  it('should hide the module if no buoys nearby', () => {
    useGetBuoyNearbyQueryStub.returns({ data: { data: [], associated: {} } });
    const location = { latitude: 1, longitude: 1 };
    const { wrapper } = mountWithReduxAndRouter(NearbyBuoys, {
      location,
      buoyId: getNearbyBuoysFixture.data[0].id,
    });

    expect(wrapper).to.be.empty();
  });
});
