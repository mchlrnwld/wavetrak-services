import { expect } from 'chai';
import Skeleton from 'react-loading-skeleton';

import NearbyBuoysHeader, { DEFAULT_MAP_ZOOM } from './NearbyBuoysHeader';

import { mountWithReduxAndRouter } from '../../utils/test-utils';
import { getBuoyMapPath, getBuoyMapPathWithLocationObject } from '../../utils/urls/mapPaths';
import WavetrakLink from '../WavetrakLink';

describe('components / NearbyBuoys / NearbyBuoysHeader', () => {
  it('should show a skeleton in place of the link until the buoy details have loaded', () => {
    const { wrapper } = mountWithReduxAndRouter(NearbyBuoysHeader, {
      loading: true,
    });

    const mapLinkLoading = wrapper.find(Skeleton);
    expect(mapLinkLoading).to.have.length(1);
    expect(mapLinkLoading.prop('width')).to.be.equal(70);
  });

  it('should render a link to the map view with the given coordinates', () => {
    const { wrapper } = mountWithReduxAndRouter(NearbyBuoysHeader, {
      latitude: 1,
      longitude: 2,
    });

    const mapLink = wrapper.find(WavetrakLink);
    expect(mapLink).to.have.length(1);
    expect(mapLink.prop('href')).to.be.equal(
      getBuoyMapPathWithLocationObject({
        center: { lat: 1, lon: 2 },
        zoom: DEFAULT_MAP_ZOOM,
      }),
    );
  });

  it('should link to the base buoy map if no coordinates are provided', () => {
    const { wrapper } = mountWithReduxAndRouter(NearbyBuoysHeader, {});

    const mapLink = wrapper.find(WavetrakLink);
    expect(mapLink).to.have.length(1);
    expect(mapLink.prop('href')).to.be.equal(getBuoyMapPath());
  });
});
