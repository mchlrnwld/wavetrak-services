import React from 'react';
import { mount } from 'enzyme';
import { expect } from 'chai';
import Skeleton from 'react-loading-skeleton';

import MapPlaceholder from './MapPlaceholder';

describe('containers / routes / MapV2 / MapPlaceholder', () => {
  it('should render nothing if show is false', () => {
    const wrapper = mount(<MapPlaceholder />);

    expect(wrapper.isEmptyRender()).to.be.true();
  });
  it('should render the skeleton loading if show is true', () => {
    const wrapper = mount(<MapPlaceholder show />);

    expect(wrapper.find(Skeleton)).to.have.length(1);
  });
});
