import React from 'react';

import { expect } from 'chai';
import { shallow } from 'enzyme';
import deepFreeze from 'deep-freeze';
import Skeleton from 'react-loading-skeleton';
import BuoysHeader from './BuoysHeader';

describe('components / BuoysHeader', () => {
  const loadingProps = deepFreeze({
    loading: true,
  });

  const props = deepFreeze({
    id: '12345',
    name: 'Monterey Bay',
  });

  const errorProps = deepFreeze({
    error: true,
  });

  it('should render a loading skeleton', () => {
    const wrapper = shallow(<BuoysHeader {...loadingProps} />);

    const skeleton = wrapper.find(Skeleton);
    expect(skeleton).to.have.length(1);
  });

  it('should render the buoy header with a name and id', () => {
    const wrapper = shallow(<BuoysHeader {...props} />);

    const title = wrapper.find('.sl-buoys-header__main__title');
    expect(title.text()).to.be.equal(`${props.name} Buoy (${props.id}) Report`);
  });

  it('should use a backup name if there is an error', () => {
    const wrapper = shallow(<BuoysHeader {...errorProps} />);

    const title = wrapper.find('.sl-buoys-header__main__title');
    expect(title.text()).to.be.equal(`Buoy Report`);
  });
});
