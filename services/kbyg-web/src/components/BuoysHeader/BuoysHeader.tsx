import { SerializedError } from '@reduxjs/toolkit';
import { ContentContainer } from '@surfline/quiver-react';
import React from 'react';
import Skeleton from 'react-loading-skeleton';

interface Props {
  name?: string;
  id?: string;
  loading?: boolean;
  error?: SerializedError | Error | boolean;
}

const BuoysHeader: React.FC<Props> = ({ name, id, loading, error }) => {
  const title = error ? 'Buoy Report' : `${name} Buoy ${id ? `(${id})` : ''} Report`;
  return (
    <header className="sl-buoys-header">
      <ContentContainer>
        <div className="sl-buoys-header__main">
          <h1 className="sl-buoys-header__main__title">
            {loading ? <Skeleton width={350} /> : title}
          </h1>
        </div>
      </ContentContainer>
    </header>
  );
};

export default BuoysHeader;
