import React from 'react';

import { expect } from 'chai';
import { mount } from 'enzyme';

import BuoyOfflineWrapper from './BuoyOfflineWrapper';

describe('components / BuoyOfflineWrapper', () => {
  it('should render the children if the buoy is online', () => {
    const wrapper = mount(
      <BuoyOfflineWrapper fallback="-.-">
        <div className="child" />
      </BuoyOfflineWrapper>,
    );

    const child = wrapper.find('.child');
    const fallback = wrapper.find('.offlineWrapper');
    expect(child).to.have.length(1);
    expect(fallback).to.have.length(0);
  });

  it('should render the offline if the buoy is offline', () => {
    const wrapper = mount(
      <BuoyOfflineWrapper offline fallback="-.-">
        <div className="child" />
      </BuoyOfflineWrapper>,
    );
    const child = wrapper.find('.child');
    const fallback = wrapper.find('.offlineWrapper');
    expect(child).to.have.length(0);
    expect(fallback).to.have.length(1);
    expect(fallback.text()).to.be.equal('-.-');
  });
});
