import React from 'react';
import classNames from 'classnames';

import styles from './BuoyOfflineWrapper.module.scss';

const getClass = (className: Props['className']) => classNames(styles.offlineWrapper, className);

interface Props {
  offline?: boolean;
  fallback: React.ReactNode;
  className?: string;
}

const BuoyOfflineWrapper: React.FC<Props> = ({
  offline = false,
  fallback,
  children,
  className = '',
}) => {
  if (!offline) return children as React.ReactElement;

  return <span className={getClass(className)}>{fallback}</span>;
};

export default BuoyOfflineWrapper;
