import PropTypes from 'prop-types';
import React from 'react';
import { Alert as QuiverAlert } from '@surfline/quiver-react';
import WarningIcon from '../WarningIcon';

const InactiveMessage = ({ message }) => (
  <div className="sl-inactive-message">
    <QuiverAlert>
      <div className="sl-inactive-message__title">
        <WarningIcon />
        <h5>Forecast Inactive</h5>
      </div>
      <p>{message}</p>
    </QuiverAlert>
  </div>
);

InactiveMessage.propTypes = {
  message: PropTypes.string.isRequired,
};

export default InactiveMessage;
