import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import InactiveMessage from './InactiveMessage';

describe('components / InactiveMessage', () => {
  it('should render a message', () => {
    const message = 'Test inactive message.';
    const wrapper = shallow(<InactiveMessage message={message} />);
    expect(wrapper.find('p').text()).to.equal('Test inactive message.');
  });
});
