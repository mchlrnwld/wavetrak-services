import React from 'react';
import { shallow } from 'enzyme';
import Head from 'next/head';
import MetaTags from './MetaTags';
import config from '../../config';

describe('components / MetaTags', () => {
  const title = 'HB Pier, Southside Surf Report, Cam, and Forecast - Surfline';
  const description =
    'HB Pier, Southside surf report updated daily with live HD cam stream. Watch the live HB Pier, Southside HD surf cam now so you can make the call before you go surfing today.';
  const urlPath = '/surf-report/hb-pier-southside/5842041f4e65fad6a77088ed/forecast';
  const image = 'https://camstills.cdn-surfline.com/hbpiersscam/latest_small.jpg';
  const url = `${config.surflineHost}${urlPath}`;
  const seoUrl = `${config.hostForSEO}${urlPath}`;

  it('renders Head with appropriate props', () => {
    const wrapper = shallow(
      <MetaTags title={title} description={description} image={image} urlPath={urlPath} />,
    );

    const head = wrapper.find(Head);

    expect(head).toHaveLength(1);

    const meta = wrapper.find('meta').map((m) => m.props());

    expect(meta).toHaveLength(10);
    expect(meta!.find((m) => m.name === 'robots')).toStrictEqual({
      name: 'robots',
      content: config.robots,
      property: undefined,
    });
    expect(meta!.find((m) => m.name === 'description')).toStrictEqual({
      name: 'description',
      content: description,
      property: undefined,
    });
    expect(meta!.find((m) => m.property === 'og:title')).toStrictEqual({
      property: 'og:title',
      content: title,
      name: undefined,
    });
    expect(meta!.find((m) => m.property === 'og:url')).toStrictEqual({
      property: 'og:url',
      content: url,
      name: undefined,
    });
    expect(meta.find((m) => m.property === 'og:image')).toStrictEqual({
      property: 'og:image',
      content: image,
      name: undefined,
    });
    expect(meta.find((m) => m.property === 'og:description')).toStrictEqual({
      property: 'og:description',
      content: description,
      name: undefined,
    });
    expect(meta.find((m) => m.property === 'twitter:title')).toStrictEqual({
      property: 'twitter:title',
      content: title,
      name: undefined,
    });
    expect(meta.find((m) => m.property === 'twitter:url')).toStrictEqual({
      property: 'twitter:url',
      content: url,
      name: undefined,
    });
    expect(meta.find((m) => m.property === 'twitter:image')).toStrictEqual({
      property: 'twitter:image',
      content: image,
      name: undefined,
    });
    expect(meta.find((m) => m.property === 'twitter:description')).toStrictEqual({
      property: 'twitter:description',
      content: description,
      name: undefined,
    });

    const link = head.find('link');
    expect(link).toHaveLength(1);
    expect(link.props()).toStrictEqual({
      title: undefined,
      rel: 'canonical',
      href: seoUrl,
    });
  });

  it('renders Head with appropriate link props', () => {
    const wrapper = shallow(
      <MetaTags
        title={title}
        description={description}
        image={image}
        urlPath={urlPath}
        parentUrl={urlPath}
        parentTitle={title}
      />,
    );
    const helmet = wrapper.find(Head);
    expect(helmet).toHaveLength(1);

    const link = wrapper.find('link').map((l) => l.props());

    expect(link).toHaveLength(2);

    expect(link.find((l) => l.rel === 'canonical')).toStrictEqual({
      rel: 'canonical',
      href: seoUrl,
      title: undefined,
    });
    expect(link.find((l) => l.rel === 'start')).toStrictEqual({
      rel: 'start',
      href: seoUrl,
      title,
    });
  });

  it('defaults image to Facebook OG image', () => {
    const wrapper = shallow(<MetaTags title={title} description={description} urlPath={urlPath} />);
    const head = wrapper.find(Head);

    expect(head).toHaveLength(1);

    const meta = wrapper.find('meta').map((m) => m.props());

    expect(meta.find((m) => m.property === 'og:image')).toStrictEqual({
      property: 'og:image',
      content: '/facebook-og-default.png',
      name: undefined,
    });
    expect(meta.find((m) => m.property === 'twitter:image')).toStrictEqual({
      property: 'twitter:image',
      content: '/facebook-og-default.png',
      name: undefined,
    });
  });
});
