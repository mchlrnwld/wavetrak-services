import Head from 'next/head';
import React from 'react';

import config from '../../config';

interface Props {
  title: string;
  description: string;
  urlPath: string;
  image?: string;
  parentUrl?: string;
  parentTitle?: string;
}

interface Link {
  rel: string;
  href: string;
  title?: string;
}

const MetaTags: React.FC<Props> = ({
  title,
  description,
  urlPath,
  image = config.facebookOGImage,
  parentUrl,
  parentTitle,
  children,
}) => {
  const seoUrl = `${config.hostForSEO}${urlPath}`;
  const url = `${config.surflineHost}${urlPath}`;
  const link: Link[] = [
    {
      rel: 'canonical',
      href: seoUrl,
    },
  ];
  if (parentUrl && parentTitle) {
    link.push({
      rel: 'start',
      href: `${config.hostForSEO}${parentUrl}`,
      title: parentTitle,
    });
  }

  const meta = [
    {
      name: 'description',
      content: description,
    },
    {
      property: 'og:title',
      content: title,
    },
    {
      property: 'og:url',
      content: url,
    },
    {
      property: 'og:image',
      content: image,
    },
    {
      property: 'og:description',
      content: description,
    },
    {
      property: 'twitter:title',
      content: title,
    },
    {
      property: 'twitter:url',
      content: url,
    },
    {
      property: 'twitter:image',
      content: image,
    },
    {
      property: 'twitter:description',
      content: description,
    },
  ];

  const robotsMeta = config.robots
    ? {
        name: 'robots',
        content: config.robots,
      }
    : null;

  if (robotsMeta) {
    meta.push(robotsMeta);
  }

  return (
    <Head>
      <title>{title}</title>
      {meta.map((m) => (
        <meta property={m.property} content={m.content} key={m.property || m.name} name={m.name} />
      ))}
      {link.map((l) => (
        <link rel={l.rel} href={l.href} key={l.href} title={l.title} />
      ))}
      {children}
    </Head>
  );
};

export default MetaTags;
