import React from 'react';
import { color } from '@surfline/quiver-themes';

const ChartsIcon = () => (
  <svg className="sl-charts-icon" width="26" height="26" viewBox="0 0 26 26">
    <circle cx="13" cy="13" r="13" fill="#96A9B2" />
    <rect fill={color('white')} x="5" y="14" width="4" height="5" rx="0.5" />
    <rect fill={color('white')} x="11" y="11" width="4" height="8" rx="0.5" />
    <rect fill={color('white')} x="17" y="7" width="4" height="12" rx="0.5" />
  </svg>
);

export default ChartsIcon;
