import React from 'react';
import { getLocalDate } from '@surfline/web-common';
import format from 'date-fns/format';
import differenceInSeconds from 'date-fns/differenceInSeconds';
import { sessionPropType } from '../../propTypes/session';

import styles from './ClipFooter.module.scss';

const ClipFooter = ({ session }) => {
  const {
    wave,
    user,
    associated: { units },
  } = session;
  const { firstName, lastName } = user;
  const { utcOffset, timezoneAbbr } = session.associated;
  const { startTimestamp, endTimestamp } = wave;
  const surfUnits = units.surfHeight;
  const speedUnits = units.speed;

  const waveDate = getLocalDate(startTimestamp / 1000, utcOffset);
  const waveStartTime = new Date(startTimestamp);
  const waveEndTime = new Date(endTimestamp);
  const rideSeconds = differenceInSeconds(waveEndTime, waveStartTime);
  const rideTimeMinutes = Math.round(rideSeconds >= 60 ? rideSeconds / 60 : 0);
  const rideTimeSeconds = Math.round(rideTimeMinutes >= 1 ? rideSeconds % 60 : rideSeconds);

  return (
    <div className={styles.clipFooter}>
      <div className={styles.clipFooterLeft}>
        <h6 className={styles.clipFooterUserName}>
          {firstName ? `${firstName} ${lastName}` : 'Surfline Sessions User'}
        </h6>
        <p className={styles.clipFooterWaveTime}>
          {format(waveDate, `E, MMM d, yyyy - h:mmaaaaa'm' '${timezoneAbbr}'`)}
        </p>
      </div>
      <div className={styles.clipFooterRight}>
        <div className={styles.clipFooterRightItem}>
          <h6 className={styles.clipFooterRightTitle}>
            {`${rideTimeMinutes >= 10 ? rideTimeMinutes : `0${rideTimeMinutes}`}:${
              rideTimeSeconds >= 10 ? rideTimeSeconds : `0${rideTimeSeconds}`
            }`}
          </h6>
          <p className={styles.clipFooterRightLabel}>Ride Time</p>
        </div>
        <div className={styles.clipFooterRightItem}>
          <h6 className={styles.clipFooterRightTitle}>
            {Math.round(wave.distance)}
            {surfUnits?.toLowerCase()}
          </h6>
          <p className={styles.clipFooterRightLabel}>Ride Length</p>
        </div>
        <div className={styles.clipFooterRightItem}>
          <h6 className={styles.clipFooterRightTitle}>
            {Math.round(wave.speedMax)}
            {speedUnits?.toLowerCase()}
          </h6>
          <p className={styles.clipFooterRightLabel}>Max Speed</p>
        </div>
      </div>
    </div>
  );
};

ClipFooter.propTypes = {
  session: sessionPropType.isRequired,
};

export default ClipFooter;
