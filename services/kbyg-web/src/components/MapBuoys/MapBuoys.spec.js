import React from 'react';
import { act } from 'react-dom/test-utils';
import { expect } from 'chai';
import sinon from 'sinon';
import { Map } from '@surfline/quiver-react';

import MapBuoys from './MapBuoys';
import MapBuoyCards from '../MapBuoyCards';
import BuoyCardPlaceholders from '../BuoyCardPlaceholders';
import BuoyMapMarkers from '../BuoyMapMarkers';

import * as useMap from '../../hooks/map/useMap';
import * as useBuoysBoundingBoxQuery from '../../hooks/map/useBuoysBoundingBoxQuery';
import { getNearbyBuoysFixture } from '../../common/api/fixtures/buoys';
import { mountWithReduxAndRouter, wait } from '../../utils/test-utils';
import * as useActiveBuoy from '../../hooks/buoys/useActiveBuoy';

describe('containers / MapBuoys', () => {
  const setActiveBuoyIdStub = sinon.stub();
  /** @type {sinon.SinonStub} */
  let useMapStub;

  /** @type {sinon.SinonStub} */
  let useActiveBuoyStub;

  /** @type {sinon.SinonStub} */
  let useBuoysBoundingBoxQueryStub;

  let map;

  const Comp = () => (
    <>
      <Map
        center={{ lat: 0, lon: 0 }}
        mapProps={{
          // eslint-disable-next-line
          whenCreated: (m) => (map = m),
        }}
      />
      <MapBuoys />
    </>
  );

  beforeEach(() => {
    useMapStub = sinon.stub(useMap, 'default').returns([]);
    useActiveBuoyStub = sinon.stub(useActiveBuoy, 'default').returns([null, setActiveBuoyIdStub]);
    useBuoysBoundingBoxQueryStub = sinon
      .stub(useBuoysBoundingBoxQuery, 'default')
      .returns({ buoys: getNearbyBuoysFixture.data, hasBuoys: true });
  });

  afterEach(() => {
    setActiveBuoyIdStub.reset();
    useMapStub.restore();
    useBuoysBoundingBoxQueryStub.restore();
    useActiveBuoyStub.restore();
  });

  it('should render the map buoys cards', async () => {
    const { wrapper } = mountWithReduxAndRouter(Comp);
    try {
      const cards = wrapper.find(MapBuoyCards);
      expect(cards).to.have.length(1);

      const markers = wrapper.find(BuoyMapMarkers);
      expect(markers).to.have.length(0);

      await act(async () => {
        await wait(500);

        useMapStub.returns([map]);
        wrapper.setProps({ update: 1 });
        wrapper.update();
      });

      const markers2 = wrapper.find(BuoyMapMarkers);
      expect(markers2).to.have.length(1);
    } finally {
      act(() => {
        wrapper.unmount();
      });
    }
  });

  it('should render the map buoy card placeholders', async () => {
    useBuoysBoundingBoxQueryStub.returns({
      buoys: null,
      hasBuoys: false,
      loading: true,
    });
    const { wrapper } = mountWithReduxAndRouter(Comp);
    try {
      const cards = wrapper.find(BuoyCardPlaceholders);
      expect(cards).to.have.length(1);
    } finally {
      act(() => {
        wrapper.unmount();
      });
    }
  });
});
