import React from 'react';
import { createPortal } from 'react-dom';

import useMap from '../../hooks/map/useMap';
import useBuoysBoundingBoxQuery from '../../hooks/map/useBuoysBoundingBoxQuery';

import MapBuoyCards from '../MapBuoyCards';
import BuoyMapMarkers from '../BuoyMapMarkers';
import BuoyCardPlaceholders from '../BuoyCardPlaceholders';
import useClustering from '../../hooks/map/useClustering';

const MapBuoys = () => {
  const [map] = useMap();
  const mapContainer = map?.getContainer();

  const { buoys, loading, error, hasBuoys, noBuoysFound } = useBuoysBoundingBoxQuery();
  const clusters = useClustering(hasBuoys && buoys);

  return (
    <>
      {hasBuoys ? (
        <MapBuoyCards buoys={buoys} />
      ) : (
        <BuoyCardPlaceholders
          numPlaceholders={12}
          loading={loading}
          error={error}
          noBuoysFound={noBuoysFound}
        />
      )}
      {mapContainer &&
        createPortal(<BuoyMapMarkers clusters={clusters} buoys={buoys} />, mapContainer)}
    </>
  );
};

MapBuoys.propTypes = {};

export default MapBuoys;
