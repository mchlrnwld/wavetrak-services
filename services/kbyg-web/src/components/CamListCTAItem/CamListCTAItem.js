import PropTypes from 'prop-types';
import React from 'react';
import { CamIcon, PremiumCamIconSmall } from '@surfline/quiver-react';
import CamListLink from '../CamListLink';
import CamListItemSpotInfo from '../CamListItemSpotInfo';
import spotProptype from '../../propTypes/spot';

const CamListCTAItem = ({ spot, units }) => {
  const stillImageUrl = spot.cameras[0] ? spot.cameras[0].stillUrl : null;
  const backgroundImage = `url(${stillImageUrl})`;
  const isMultiCam = spot.cameras && spot.cameras.length > 1;

  return (
    <CamListLink spot={spot} location="camera player">
      <div className="sl-cam-list-cta-item">
        <div className="sl-cam-list-cta-item__still" style={{ backgroundImage }} />
        <div className="sl-cam-list-cta-item__content">
          <div className="sl-cam-list-cta-item__indicator">
            <PremiumCamIconSmall />
            {isMultiCam && <CamIcon isMultiCam withText />}
          </div>
          <div className="sl-cam-list-cta-item__spot-details">
            <CamListItemSpotInfo spot={spot} units={units} />
          </div>
        </div>
      </div>
    </CamListLink>
  );
};

CamListCTAItem.defaultProps = {};

CamListCTAItem.propTypes = {
  spot: spotProptype.isRequired,
  units: PropTypes.string.isRequired,
};

export default CamListCTAItem;
