/* eslint-disable jsx-a11y/click-events-have-key-events */

import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { Chevron } from '@surfline/quiver-react';

const getClassNames = (size) =>
  classNames({
    'sl-scroll-control': true,
    'sl-scroll-control--tall': size === 'tall',
    'sl-scroll-control--extra-tall': size === 'extraTall',
  });

const ScrollControl = ({ direction, size, clickHandler }) => (
  <div className={getClassNames(size)} onClick={clickHandler}>
    <Chevron direction={direction} />
  </div>
);

ScrollControl.propTypes = {
  direction: PropTypes.string.isRequired,
  clickHandler: PropTypes.func,
  size: PropTypes.string,
};

ScrollControl.defaultProps = {
  clickHandler: null,
  size: null,
};

export default ScrollControl;
