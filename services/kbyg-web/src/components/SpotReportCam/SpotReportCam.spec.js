import React from 'react';
import { expect } from 'chai';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import { mount } from 'enzyme';
import { CamPlayer } from '@surfline/quiver-react';
import SpotReportCam from './SpotReportCam';

describe('components / SpotReportCam', () => {
  let mockStore;
  const defaultState = {
    animations: {
      scrollPositions: {
        camList: {
          top: 0,
          left: 0,
        },
      },
    },
    backplane: {
      user: {
        entitlements: [],
        settings: {
          units: {
            surfHeight: 'M',
          },
        },
      },
    },
    spot: {
      report: {
        associated: {
          advertising: {
            spotId: '4875',
            subregionId: '2143',
          },
        },
      },
      nearby: {
        data: {
          spots: [],
        },
      },
    },
  };
  const defaultProps = {
    spot: {
      name: 'Santa Ana River Jeties',
      cameras: [
        {
          _id: '5834a0223421b20545c4b581',
          title: 'Santa Ana River Jetties',
          streamUrl:
            'https://cams.cdn-surfline.com/wsc-west/wc-riverjettiescam.stream/playlist.m3u8',
          stillUrl: 'https://camstills.cdn-surfline.com/riverjettiescam/latest_small_pixelated.png',
          rewindBaseUrl: 'https://camrewinds.cdn-surfline.com/riverjettiescam/riverjettiescam',
          status: {
            isDown: false,
            message: '',
          },
          control: 'https://camstills.cdn-surfline.com/riverjettiescam/latest_small.jpg',
          nighttime: false,
          rewindClip:
            'https://camrewinds.cdn-surfline.com/riverjettiescam/riverjettiescam.1300.2017-12-12.mp4',
        },
      ],
    },
    camera: {
      _id: '5834a0223421b20545c4b581',
      title: 'Santa Ana River Jetties',
      streamUrl: 'https://cams.cdn-surfline.com/wsc-west/wc-riverjettiescam.stream/playlist.m3u8',
      stillUrl: 'https://camstills.cdn-surfline.com/riverjettiescam/latest_small_pixelated.png',
      rewindBaseUrl: 'https://camrewinds.cdn-surfline.com/riverjettiescam/riverjettiescam',
      status: {
        isDown: false,
        message: '',
      },
      control: 'https://camstills.cdn-surfline.com/riverjettiescam/latest_small.jpg',
      nighttime: false,
      rewindClip:
        'https://camrewinds.cdn-surfline.com/riverjettiescam/riverjettiescam.1300.2017-12-12.mp4',
    },
    spotId: '123',
    legacySpotId: '11233',
  };

  beforeAll(() => {
    mockStore = configureStore([ReduxThunk]);
  });

  it('renders CamPlayer for a Spot with a cam', () => {
    const store = mockStore(defaultState);
    const wrapper = mount(
      <Provider store={store}>
        <SpotReportCam {...defaultProps} />
      </Provider>,
    );
    const CamPlayerInstance = wrapper.find(CamPlayer);
    expect(CamPlayerInstance).to.have.length(1);
  });
});
