import PropTypes from 'prop-types';
import React, { useCallback, useMemo, useState, useRef } from 'react';
import { connect } from 'react-redux';
import {
  CamPlayer,
  GoogleDFP,
  PremiumCamCTA,
  CTAProvider,
  CamPlayerV2,
  CamCTA,
} from '@surfline/quiver-react';
import { trackClickedSubscribeCTA, getUserSettings } from '@surfline/web-common';
import config from '../../config';
import {
  getAdvertisingIds,
  getNearbySpots,
  getReverseSortBeachView,
  getSpotReportAssociated,
} from '../../selectors/spot';
import { getCamListScrollPosition } from '../../selectors/animations';
import spotReportDataPropType from '../../propTypes/spot';
import userDetailsPropTypes from '../../propTypes/userDetails';
import userSettingsPropTypes from '../../propTypes/userSettings';
import meterPropType from '../../propTypes/meter';
import cameraPropType from '../../propTypes/camera';
import loadAdConfig from '../../utils/adConfig';
import getUTCDate from '../../utils/getUTCDate';
import getCamStillImage from '../../utils/getCamStillImage';

import PersistentSpotReportCam from '../PersistentSpotReportCam/PersistentSpotReportCam';
import { ALT, MSG, SUB } from '../../common/constants';
import { getIsSurfCheckEligible } from '../../selectors/meter';
import withMetering from '../../common/withMetering';
import { useSpotIsFavorite } from '../../hooks/useSpotIsFavorite';

import styles from './SpotReportCam.module.scss';

const convertUtcOffsetToMilliseconds = (offset) => offset * 60 * 60 * 1000;

/**
 * @typedef {Object} SpotReportCamProps
 * @property {Object} props
 * @property {boolean} adblock
 * @property {{accept: string, decline: string, message: string, headline: string}} adFreePopupText
 * @property {{spotId: string, subregionId: string}} advertisingIds
 * @property {import('../../propTypes/camera').Camera} camera
 * @property {boolean} isMultiCam
 * @property {boolean} isPremium
 * @property {boolean} isPrimaryCam
 * @property {string} legacySpotId
 * @property {ALT | SUB | MSG} messageType
 * @property {import('@surfline/web-common').MeterState} meter
 * @property {string} playerId
 * @property {{ utcOffset: number }} reportAssociated
 * @property {import('../../propTypes/spot').SpotReportData} reportData
 * @property {{ name: string, subregion: { _id: string, forecastStatus: string, }}} spot
 * @property {string} spotId
 * @property {boolean} timeoutMultiCams
 * @property {import('@surfline/web-common').UserState['settings']} userSettings
 * @property {import('@surfline/web-common').UserState['details']} userDetails
 *
 * @param {SpotReportCamProps} props
 * @returns {JSX.Element}
 */
const SpotReportCam = ({
  adblock,
  adFreePopupText,
  advertisingIds,
  camera,
  isMultiCam,
  isPremium,
  isPrimaryCam,
  legacySpotId,
  messageType,
  meter,
  playerId,
  reportAssociated,
  reportData,
  spot,
  spotId,
  timeoutMultiCams,
  userSettings,
  userDetails,
  isSurfCheckEligible,
}) => {
  const camRef = useRef(null);
  const isFavorite = useSpotIsFavorite(spotId);

  /** @type {[number, (camRefHeight: React.Dispatch<React.SetStateAction<number>>) => void)]} */
  const [camRefHeight, setCamRefHeight] = useState();

  const isMetered = !isPremium && meter?.meteringEnabled;

  const defaultSegmentProperties = useMemo(
    () => ({
      pageName: 'Spot Report',
      spotId,
      spotName: spot?.name,
    }),
    [spotId, spot.name],
  );

  const getCamHostAd = useCallback(
    // Important: Don't use the 'isHTL' prop here. It causes the Ad Id to be the same for all Cam Host ads.
    // On spots that have multiple cameras, this causes Google ads to throw an error and prevents the ads from loading.
    // The AdOps team confirmed this ad doesn't go through HTL (ad service provider) anyways,
    // and by ommitting this, a uuid is appended to the ad id to make it unique.
    () => (
      <div className="sl-spot-report-page__camera__host-ad">
        <GoogleDFP
          adConfig={{
            ...loadAdConfig('camHostAd', [
              ['spotid', advertisingIds.spotId],
              ['camId', camera._id],
              ['className', 'kbyg-cam'],
            ]),
          }}
          isTesting={config.htl.isTesting}
        />
      </div>
    ),
    [advertisingIds?.spotId, camera?._id],
  );

  const renderCamPlayer = useCallback(
    (isPersistentCam) => {
      const prerecordedTimeRange = camera.isPrerecorded &&
        camera.lastPrerecordedClipStartTimeUTC &&
        camera.lastPrerecordedClipEndTimeUTC && {
          start:
            getUTCDate(new Date(camera.lastPrerecordedClipStartTimeUTC)).getTime() +
            convertUtcOffsetToMilliseconds(reportAssociated.utcOffset),
          end:
            getUTCDate(new Date(camera.lastPrerecordedClipEndTimeUTC)).getTime() +
            convertUtcOffsetToMilliseconds(reportAssociated.utcOffset),
        };

      const camPlayerBaseProps = {
        prerecordedTimeRange,
        playerId: playerId || 'sl-spot-report-camera',
        aspectRatio: '16:9',
        camera,
        legacySpotId,
        spotName: spot.name,
        spotId,
        isPremium,
        camHostAd: getCamHostAd(),
        isMultiCam,
        isPrimaryCam,
        timeoutMultiCams,
        messageType,
        showOverlays: !isPersistentCam,
        location: 'Spot Report Cam',
        isFavorite,
      };

      if (isMetered) {
        return <CamPlayerV2 {...camPlayerBaseProps} autoplay="enabled" />;
      }

      const midRollSegmentProperties = {
        location: 'mid roll',
        ...defaultSegmentProperties,
      };

      const adBlockSegmentProperties = {
        location: 'cam player ad blocker',
        ...defaultSegmentProperties,
      };

      return (
        <CamPlayer
          {...camPlayerBaseProps}
          adFreePopupText={adFreePopupText}
          cssModules={styles}
          funnelUrl={config.funnelUrl}
          adblock={adblock && !isPremium}
          advertisingIds={advertisingIds}
          onClickAdBlock={() => trackClickedSubscribeCTA(adBlockSegmentProperties)}
          onClickUpSell={() => trackClickedSubscribeCTA(midRollSegmentProperties)}
        />
      );
    },
    [
      adFreePopupText,
      adblock,
      advertisingIds,
      camera,
      defaultSegmentProperties,
      getCamHostAd,
      isMultiCam,
      isPremium,
      isPrimaryCam,
      legacySpotId,
      messageType,
      isMetered,
      playerId,
      reportAssociated.utcOffset,
      spot.name,
      spotId,
      timeoutMultiCams,
      isFavorite,
    ],
  );

  const premiumCamCTASegmentProperties = {
    location: 'premium only cam - spot',
    category: 'cams & reports',
    ...defaultSegmentProperties,
  };

  const camStillUrl = getCamStillImage(camera);
  const isPremiumCam = camera.isPremium;
  const showPremiumCamPaywall = !isMetered && isPremiumCam && !isPremium;

  if (showPremiumCamPaywall) {
    return (
      <CTAProvider segmentProperties={premiumCamCTASegmentProperties}>
        <PremiumCamCTA spotName={spot.name} camStillUrl={camStillUrl} />
      </CTAProvider>
    );
  }
  const isAnonymous = !userDetails;
  if (meter?.meteringEnabled && (isAnonymous || (userDetails && !isSurfCheckEligible))) {
    const backgroundImage = `url(${camStillUrl})`;
    const segmentProperties = {
      location: 'Cam Player',
      ...defaultSegmentProperties,
    };
    return (
      <CamCTA
        backgroundImage={backgroundImage}
        isAnonymous={isAnonymous}
        segmentProperties={segmentProperties}
      />
    );
  }

  return (
    <div className="sl-spot-report-page__camera-container" ref={camRef}>
      <div className="sl-spot-report-page__camera" style={{ height: camRefHeight }}>
        {isPrimaryCam || !isMultiCam ? (
          <PersistentSpotReportCam
            camRef={camRef}
            isPremium={isPremium}
            spot={spot}
            userDetails={userDetails}
            userSettings={userSettings}
            reportData={reportData}
            renderCamPlayer={renderCamPlayer}
            playerId={playerId}
            camRefHeight={camRefHeight}
            setCamRefHeight={setCamRefHeight}
          />
        ) : (
          renderCamPlayer()
        )}
      </div>
    </div>
  );
};

SpotReportCam.defaultProps = {
  spot: null,
  isPremium: false,
  adblock: false,
  userDetails: null,
  userSettings: null,
  reportData: null,
  playerId: 'sl-spot-report-camera',
  isMultiCam: false,
  isPrimaryCam: false,
  timeoutMultiCams: () => null,
  messageType: 'MSG',
  adFreePopupText: null,
  meter: null,
};

SpotReportCam.propTypes = {
  adblock: PropTypes.bool,
  advertisingIds: PropTypes.shape({
    spotId: PropTypes.string,
    subregionId: PropTypes.string,
  }).isRequired,
  adFreePopupText: PropTypes.shape({
    accept: PropTypes.string,
    decline: PropTypes.string,
    message: PropTypes.string,
    headline: PropTypes.string,
  }),
  meter: meterPropType,
  isPremium: PropTypes.bool,
  spot: PropTypes.shape({
    name: PropTypes.string,
    subregion: PropTypes.shape({
      _id: PropTypes.string,
      forecastStatus: PropTypes.string,
    }),
  }),
  camera: cameraPropType.isRequired,
  spotId: PropTypes.string.isRequired,
  legacySpotId: PropTypes.string.isRequired,
  isMultiCam: PropTypes.bool,
  userDetails: userDetailsPropTypes,
  userSettings: userSettingsPropTypes,
  reportData: spotReportDataPropType,
  playerId: PropTypes.string,
  isPrimaryCam: PropTypes.bool,
  timeoutMultiCams: PropTypes.func,
  messageType: PropTypes.oneOf([ALT, SUB, MSG]),
  reportAssociated: PropTypes.shape({
    utcOffset: PropTypes.number,
  }).isRequired,
  isSurfCheckEligible: PropTypes.bool.isRequired,
};

export default connect((state) => ({
  advertisingIds: getAdvertisingIds(state),
  nearbySpots: getNearbySpots(state),
  nearbySpotsError: !!state.spot.nearby.error,
  reverseSortBeachView: getReverseSortBeachView(state),
  userSettings: getUserSettings(state),
  camListScrollPosition: getCamListScrollPosition(state),
  adblock: state.adblock,
  reportAssociated: getSpotReportAssociated(state),
  isSurfCheckEligible: getIsSurfCheckEligible(state),
}))(withMetering(SpotReportCam));
