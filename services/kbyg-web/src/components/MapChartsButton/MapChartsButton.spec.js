import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import MapButton from '../MapButton';
import ChartsIcon from '../ChartsIcon';
import MapChartsButton from './MapChartsButton';

describe('components / MapChartsButton', () => {
  it('renders MapButton with ChartsIcon and Charts text', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(<MapChartsButton onClick={onClick} />);

    const mapButton = wrapper.find(MapButton);
    expect(mapButton).to.have.length(1);

    mapButton.simulate('click');
    expect(onClick).to.have.been.calledOnce();

    const chartsIcon = mapButton.find(ChartsIcon);
    expect(chartsIcon).to.have.length(1);

    const text = mapButton.find('.sl-map-charts-button__text');
    expect(text).to.have.length(1);
    expect(text.text()).to.equal('Charts');
  });
});
