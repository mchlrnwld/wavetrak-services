import PropTypes from 'prop-types';
import React from 'react';
import MapButton from '../MapButton';
import ChartsIcon from '../ChartsIcon';

const MapChartsButton = ({ onClick }) => (
  <div className="sl-map-charts-button">
    <MapButton onClick={onClick}>
      <ChartsIcon />
      <div className="sl-map-charts-button__text">Charts</div>
    </MapButton>
  </div>
);

MapChartsButton.propTypes = {
  onClick: PropTypes.func,
};

MapChartsButton.defaultProps = {
  onClick: () => null,
};

export default MapChartsButton;
