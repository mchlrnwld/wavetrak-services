import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classnames from 'classnames';
import {
  Chevron,
  ChartLegend,
  withWindow,
  createAccessibleOnClick,
  TrackableLink,
} from '@surfline/quiver-react';
import { format } from 'date-fns';
import { getLocalDate, getWindow, trackClickedSubscribeCTA } from '@surfline/web-common';

import Image from 'next/image';
import ChartScrubber from '../ChartScrubber';
import ChartScrubberMobile from '../ChartScrubberMobile';
import { chartsPropTypes, chartSegmentPropsPropTypes } from '../../propTypes/charts';
import ChartIcon from '../ChartIcon';
import LongArrowIcon from '../LongArrowIcon';
import config from '../../config';
import overviewDataPropTypes from '../../propTypes/subregionOverviewData';
import userSettingsPropType from '../../propTypes/userSettings';
import en from '../../intl/translations/en';
import getChartSubtitle from '../../utils/getChartSubtitle';
import { CLICKED_SUBSCRIBE_CTA } from '../../common/constants';

const getChartImageClasses = (index, activeIndex) =>
  classnames({
    'sl-chart-player__list__item': true,
    'sl-chart-player__list__item--active': index === activeIndex,
  });

class ChartPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
      isPlaying: false,
      stepped: false,
      paywall: false,
      scrubber: {
        mobile: {
          timestepWidth: null,
          paywallWidth: null,
        },
        desktop: {
          timestepWidth: null,
          paywallWidth: null,
        },
      },
    };
    const {
      charts: {
        images: { images },
      },
    } = this.props;
    this.images = images;
    this.imagesAvailable = images.filter((image) => image.url);
    this.timestepsAvailable = this.imagesAvailable.length;
    this.subscribeLinkRef = React.createRef();
    this.chartScrubberMobileControlsRef = React.createRef();
  }

  componentDidMount() {
    const win = getWindow();
    if (win) win.addEventListener('resize', this.checkDimensions);
    this.checkDimensions();
  }

  componentDidUpdate(prevProps, prevState) {
    const { charts } = this.props;
    const { activeIndex: prevActiveIndex } = prevState;
    const { activeIndex, isPlaying } = this.state;
    if (charts.images !== prevProps.charts.images) {
      this.setActiveIndex(0);
    }
    if (isPlaying !== prevState.isPlaying || (isPlaying && activeIndex !== prevActiveIndex)) {
      if (isPlaying) this.animateChart(activeIndex + 1);
      else clearTimeout(this.animate);
    }
  }

  componentWillUnmount() {
    const win = getWindow();
    if (this.animate) clearTimeout(this.animate);
    if (win) win.removeEventListener('resize', this.checkDimensions);
  }

  setActiveIndex = (index) => {
    const { isPremium } = this.props;
    const { paywall, isPlaying, stepped } = this.state;

    const activeIndex = index;
    const numImagesAvailable = this.imagesAvailable.length;
    const isEnd = index >= numImagesAvailable;
    const isStart = index < 0;
    const isScrubbing = !(isPlaying || stepped);
    if (!isPremium) {
      if (isEnd && (!paywall || isScrubbing)) {
        return this.setState({ activeIndex: index, paywall: true });
      }
      if (isEnd) {
        return this.setState({ activeIndex: 0, paywall: false });
      }
      if (isStart) {
        return this.setState({ activeIndex: numImagesAvailable, paywall: true });
      }
    }
    if (isEnd) {
      return this.setState({ activeIndex: 0, paywall: false });
    }
    if (isStart) {
      return this.setState({ activeIndex: numImagesAvailable - 1, paywall: false });
    }
    return this.setState({ activeIndex, paywall: false });
  };

  checkDimensions = () => {
    if (!this.resizeTimeout) {
      this.resizeTimeout = setTimeout(() => {
        this.resizeTimeout = null;
        this.checkScrubberPaywallWidth();
      }, 100);
    }
    return null;
  };

  checkScrubberPaywallWidth = () => {
    if (this.chartScrubberMobileControlsRef && this.chartScrubberDesktopRef) {
      const mobileTimesteps = this.chartScrubberMobileControlsRef.current?.chartScrubberTimestepRef;
      const desktopTimesteps = this.chartScrubberDesktopRef?.chartScrubberTimestepRef;
      if (mobileTimesteps?.[0] && desktopTimesteps?.[0]) {
        const mobileTimestep = mobileTimesteps[0].getBoundingClientRect();
        const mobileTimestepWidth = mobileTimestep.width;
        const mobilePaywallWidth =
          mobileTimestepWidth * (mobileTimesteps.length - this.timestepsAvailable);
        const desktopTimestep = desktopTimesteps[0].getBoundingClientRect();
        const desktopTimestepWidth = desktopTimestep.width;
        const desktopPaywallWidth =
          desktopTimestepWidth * (desktopTimesteps.length - this.timestepsAvailable);
        this.setState({
          scrubber: {
            mobile: {
              timestepWidth: mobileTimestepWidth,
              paywallWidth: mobilePaywallWidth,
              paywallLeft: mobileTimestepWidth * this.timestepsAvailable + mobileTimestepWidth,
            },
            desktop: {
              timestepWidth: desktopTimestepWidth,
              paywallWidth: desktopPaywallWidth,
              paywallLeft: desktopTimestepWidth * this.timestepsAvailable,
            },
          },
        });
      }
    }
  };

  animateChart = (index) => {
    const { isPremium } = this.props;
    const isPaywall = !isPremium && index > this.imagesAvailable.length;

    if (isPaywall) {
      this.animate = setTimeout(() => this.setActiveIndex(index), 3000);
    } else {
      this.animate = setTimeout(() => this.setActiveIndex(index), 1000);
    }
  };

  jumpToIndex = (index, step = false) => {
    this.setState({ isPlaying: false, stepped: step }, () => this.setActiveIndex(index));
  };

  goPremium = () => {
    const win = getWindow();
    const { overviewData, chartSegmentProps } = this.props;
    const { category, pageName } = chartSegmentProps;
    trackClickedSubscribeCTA({
      location: 'chart - image',
      subregion: overviewData._id,
      category,
      pageName,
    });
    if (win) win.location.href = config.funnelUrl;
  };

  toggleIsPlaying = () => {
    const { activeIndex, isPlaying } = this.state;
    let newIndex = activeIndex;

    // Handle edge case where non-premium user clicks play while on paywall
    if (!isPlaying && activeIndex === this.imagesAvailable.length) {
      newIndex = 0;
    }

    this.setState({ isPlaying: !isPlaying, paywall: false, activeIndex: newIndex });
  };

  resizeTimeout;

  renderChartLegend = (chartType, hasLegend) => {
    const { userSettings } = this.props;
    let userUnits = {};
    if (userSettings?.units) {
      const { units } = userSettings;
      const swellHeight = units?.swellHeight.toLowerCase();
      userUnits = {
        ...units,
        swellHeight,
      };
    }
    let type;
    if (chartType === 'localswell' || chartType === 'regionalswell' || chartType === 'nearshore') {
      type = 'swell';
    } else if (chartType === 'localperiod' || chartType === 'regionalperiod') {
      type = 'period';
    } else if (chartType === 'localwind' || chartType === 'regionalwind') {
      type = 'wind';
    } else if (chartType === 'hireswind' || !Number.isNaN(parseFloat(chartType))) {
      type = 'hireswind';
    } else if (hasLegend) {
      if (chartType.indexOf('wave') > -1 || chartType.indexOf('band') > -1) {
        type = 'swell';
      } else if (chartType.indexOf('wind') > -1) {
        type = 'wind';
      } else if (chartType.indexOf('period') > -1) {
        type = 'period';
      } else {
        type = null;
      }
    } else {
      type = null;
    }

    return type ? (
      <div className="sl-chart-player__legend">
        <ChartLegend type={type} userUnits={userUnits} />
      </div>
    ) : null;
  };

  getSubscribeCTAEventProperties = () => {
    const { overviewData, chartSegmentProps } = this.props;
    return {
      location: 'mobile chart go premium',
      subregionId: overviewData._id,
      category: chartSegmentProps.category,
      pageName: chartSegmentProps.pageName,
    };
  };

  render() {
    const {
      charts,
      toggleMobileMenu,
      utcOffset,
      overviewData,
      chartSegmentProps,
      isPremium,
      subregionName,
    } = this.props;
    const { activeIndex, isPlaying, stepped, scrubber, paywall } = this.state;
    const showPaywall = this.imagesAvailable !== this.images;
    const displayChartScrubberDayBorder = (chartImages, index, stepUtcOffset) => {
      const currentStepDay = format(
        getLocalDate(chartImages[index].timestamp, stepUtcOffset),
        'iii',
      );
      const lastStepDay =
        index > 0
          ? format(getLocalDate(chartImages[index - 1].timestamp, stepUtcOffset), 'iii')
          : currentStepDay;
      return lastStepDay !== currentStepDay;
    };

    return (
      <div className="sl-chart-player">
        <div className="sl-chart-player__container">
          <div
            className="sl-chart-player__title"
            {...createAccessibleOnClick(toggleMobileMenu, 'button')}
          >
            <p className="sl-chart-player__title__subtitle">
              {getChartSubtitle(charts.active.category, charts.region, subregionName)}
            </p>
            <div className="sl-chart-player__title__name">
              <h3>{charts.active.displayName ? charts.active.displayName : charts.active.name}</h3>
              <Chevron direction="down" />
            </div>
          </div>
          {this.imagesAvailable.length > 0 ? (
            <ChartScrubber
              activeIndex={activeIndex}
              charts={charts}
              isPlaying={isPlaying}
              jumpToIndex={this.jumpToIndex}
              toggleIsPlaying={this.toggleIsPlaying}
              utcOffset={utcOffset}
              paywall={showPaywall}
              timestepsAvailable={this.timestepsAvailable}
              ref={(el) => {
                this.chartScrubberDesktopRef = el;
              }}
              scrubberState={scrubber.desktop}
              overviewData={overviewData}
              displayChartScrubberDayBorder={displayChartScrubberDayBorder}
              chartSegmentProps={chartSegmentProps}
            />
          ) : null}
          {this.renderChartLegend(charts.active.type, charts.active.hasLegend)}
          {paywall || this.imagesAvailable.length === 0 ? (
            <div className="sl-chart-player__paywall">
              <div className="sl-chart-player__paywall__content">
                <div className="sl-chart-player__paywall__content__left">
                  <div className="sl-chart-player__paywall__content__left__chart-icon">
                    <ChartIcon />
                  </div>
                  <h4 className="sl-chart-player__paywall__content__left__callout-text">
                    Score great waves with long-range charts
                  </h4>
                  <div
                    className="sl-chart-player__paywall__content__left__go-premium"
                    {...createAccessibleOnClick(this.goPremium, 'button')}
                  >
                    <div className="sl-chart-player__paywall__content__left__go-premium__text">
                      Start Free Trial
                    </div>
                    <div className="sl-chart-player__paywall__content__left__go-premium__arrow">
                      <LongArrowIcon />
                    </div>
                  </div>
                </div>
                <div className="sl-chart-player__paywall__content__right">
                  <div className="sl-chart-player__paywall__content__right__learn-more">
                    <a href="/premium-benefits">Learn more about Surfline Premium</a>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <ul className="sl-chart-player__list">
              {this.imagesAvailable.map((image, index) => (
                <li
                  key={`${image.url}-${image.timestamp}`}
                  className={getChartImageClasses(index, activeIndex)}
                >
                  <Image
                    src={image.url}
                    alt={charts.active?.type || 'Chart Image'}
                    layout="fill"
                    priority
                    unoptimized
                  />
                </li>
              ))}
            </ul>
          )}
          {this.imagesAvailable.length > 0 ? (
            <ChartScrubberMobile
              activeIndex={activeIndex}
              charts={charts}
              isPlaying={isPlaying}
              jumpToIndex={this.jumpToIndex}
              toggleIsPlaying={this.toggleIsPlaying}
              utcOffset={utcOffset}
              stepped={stepped}
              paywall={showPaywall}
              timestepsAvailable={this.timestepsAvailable}
              ref={this.chartScrubberMobileControlsRef}
              scrubberState={scrubber.mobile}
              overviewData={overviewData}
              displayChartScrubberDayBorder={displayChartScrubberDayBorder}
              chartSegmentProps={chartSegmentProps}
            />
          ) : null}
        </div>
        {!isPremium && (
          <div className="sl-chart-player__mobile-cta">
            <p className="sl-chart-player__mobile-cta_copy">
              {en.ChartPlayer.description}
              {' - '}
              <TrackableLink
                ref={this.subscribeLinkRef}
                eventName={CLICKED_SUBSCRIBE_CTA}
                eventProperties={this.getSubscribeCTAEventProperties}
              >
                <a
                  ref={this.subscribeLinkRef}
                  href="/upgrade"
                  className="sl-chart-player__mobile-cta_copy--link"
                >
                  {en.ChartPlayer.link}
                </a>
              </TrackableLink>
            </p>
          </div>
        )}
      </div>
    );
  }
}

ChartPlayer.propTypes = {
  charts: chartsPropTypes.isRequired,
  toggleMobileMenu: PropTypes.func.isRequired,
  utcOffset: PropTypes.number,
  overviewData: overviewDataPropTypes.isRequired,
  chartSegmentProps: chartSegmentPropsPropTypes.isRequired,
  isPremium: PropTypes.bool.isRequired,
  subregionName: PropTypes.string.isRequired,
  userSettings: userSettingsPropType,
};

ChartPlayer.defaultProps = {
  utcOffset: null,
  userSettings: null,
};

export default withWindow(ChartPlayer);
