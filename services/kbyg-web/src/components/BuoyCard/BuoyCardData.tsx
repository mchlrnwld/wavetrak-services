import React from 'react';
import classNames from 'classnames';
import { getUserSettings } from '@surfline/web-common';

import BuoySwells from '../BuoySwells';
import BuoySignificantSwell from '../BuoySignificantSwell';
import { StationData } from '../../types/buoys';
import { Height } from '../../types/units';

import styles from './BuoyCardData.module.scss';
import { useAppSelector } from '../../stores/hooks';

const className = (largeOnly?: boolean) =>
  classNames({
    [styles.buoyCardData]: true,
    [styles.buoyCardDataLarge]: largeOnly,
  });

interface Props {
  latestData: StationData;
  offline?: boolean;
  withTitles?: boolean;
  largeOnly?: boolean;
}

const BuoyCardData: React.FC<Props> = ({ latestData, offline, largeOnly, withTitles }) => {
  const settings = useAppSelector(getUserSettings);
  const units = settings?.units?.swellHeight as Height;

  return (
    <div className={className(largeOnly)}>
      <BuoySignificantSwell
        offline={offline}
        units={units}
        height={latestData?.height}
        period={latestData?.period}
        direction={latestData?.direction}
        largeOnly={largeOnly}
        withTitle={withTitles}
      />
      <BuoySwells
        largeOnly={largeOnly}
        offline={offline}
        swells={latestData?.swells}
        units={units}
      />
    </div>
  );
};

export default BuoyCardData;
