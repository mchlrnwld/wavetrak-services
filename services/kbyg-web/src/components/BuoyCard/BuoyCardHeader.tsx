import React from 'react';
import classNames from 'classnames';
import { format } from 'date-fns';
import { getUserSettings } from '@surfline/web-common';

import { DMY } from '../../common/constants';
import { StationData } from '../../types/buoys';
import { useAppSelector } from '../../stores/hooks';

import styles from './BuoyCardHeader.module.scss';

interface Props {
  latestTimestamp: StationData['timestamp'];
  name: string;
  offline?: boolean;
  active?: boolean;
}

const className = (offline: boolean, active: boolean) =>
  classNames({
    [styles.buoyCardHeader]: true,
    [styles.buoyCardHeaderOffline]: offline,
    [styles.buoyCardHeaderActive]: active,
  });

const BuoyCardHeader: React.FC<Props> = ({
  name,
  latestTimestamp,
  offline = false,
  active = false,
}) => {
  const settings = useAppSelector(getUserSettings);

  const dateFormat =
    settings?.date?.format === DMY ? "h':'mmaaa dd',' MMM yyyy" : "h':'mmaaa MMM dd',' yyyy";

  return (
    <div className={className(offline, active)}>
      <div className={styles.buoyCardHeaderActiveNotch} />
      <h1 className={styles.buoyCardHeaderName}>{name}</h1>
      {latestTimestamp && (
        <p className={styles.buoyCardHeaderTimestamp}>
          {format(new Date(latestTimestamp * 1000), dateFormat)}
        </p>
      )}
    </div>
  );
};

export default BuoyCardHeader;
