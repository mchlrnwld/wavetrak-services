import React, { useCallback, memo } from 'react';
import classNames from 'classnames';
import { motion } from 'framer-motion';
import { trackEvent } from '@surfline/web-common';

import useActiveBuoy from '../../hooks/buoys/useActiveBuoy';
import BuoyCardHeader from './BuoyCardHeader';
import BuoyCardData from './BuoyCardData';
import WavetrakLink from '../WavetrakLink';

import { OFFLINE } from '../../common/constants';
import { getBuoyReportPath } from '../../utils/urls';
import type { NearbyStation } from '../../types/buoys';

import styles from './BuoyCard.module.scss';

interface Props {
  station: NearbyStation;
  largeOnly?: boolean;
  withTitles?: boolean;
  withActiveState?: boolean;
  trackLocation?: string;
}

const buoyCardClassName = (
  status: NearbyStation['status'],
  active?: boolean,
  largeOnly?: boolean,
) =>
  classNames({
    'sl-buoy-card': true,
    [styles.buoyCard]: true,
    [styles.buoyCardOffline]: status === OFFLINE,
    [styles.buoyCardActive]: active,
    [styles.buoyCardLargeOnly]: largeOnly,
  });

const BuoyCard: React.FC<Props> = ({
  station,
  largeOnly,
  withTitles,
  withActiveState,
  trackLocation,
}) => {
  const [activeBuoyId, setActiveBuoyId] = useActiveBuoy();
  const isOffline = station.status === OFFLINE;
  const { id } = station;

  const makeActive = useCallback(() => {
    if (withActiveState) {
      setActiveBuoyId(id);
    }
  }, [setActiveBuoyId, id, withActiveState]);

  const makeInactive = useCallback(() => {
    if (withActiveState) {
      setActiveBuoyId();
    }
  }, [setActiveBuoyId, withActiveState]);

  const viewBuoyData = () => {
    trackEvent('Clicked Nearby Buoy', {
      category: 'buoys',
      location: trackLocation,
      buoyId: station?.sourceId,
      buoyName: station?.name,
    });
  };

  const active = withActiveState && activeBuoyId === id;

  return (
    <motion.div
      animate={{ scale: active ? 1.05 : 1 }}
      transition={{ duration: 0.1, bounce: 0.5 }}
      onHoverStart={makeActive}
      onHoverEnd={makeInactive}
      onTapStart={makeActive}
      className={buoyCardClassName(station.status, active, largeOnly)}
    >
      <BuoyCardHeader
        offline={isOffline}
        name={station.name || station.id}
        latestTimestamp={station.latestData?.timestamp}
        active={active}
      />
      <BuoyCardData
        withTitles={withTitles}
        latestData={station.latestData}
        offline={isOffline}
        largeOnly={largeOnly}
      />
      <WavetrakLink
        href={getBuoyReportPath({
          name: station.name,
          id: station.id,
          sourceID: station.sourceId,
        })}
        className={styles.buoyCardLink}
        onClick={viewBuoyData}
      >
        View Buoy Data
      </WavetrakLink>
    </motion.div>
  );
};

export default memo(BuoyCard);
