import { expect } from 'chai';
import Link from 'next/link';
import { EventInfo, motion } from 'framer-motion';
import { slugify } from '@surfline/web-common';
import sinon, { SinonStub } from 'sinon';

import { mountWithReduxAndRouter } from '../../utils/test-utils';

import BuoyCard from './BuoyCard';
import BuoyCardHeader from './BuoyCardHeader';
import BuoyCardData from './BuoyCardData';
import { getBackplaneSettingsFixture } from '../../utils/fixtures/store';
import * as useActiveBuoy from '../../hooks/buoys/useActiveBuoy';

import { getNearbyBuoysFixture } from '../../common/api/fixtures/buoys';
import { AppState } from '../../stores';

const dataProps = { station: getNearbyBuoysFixture.data[0] };

const stationNoNameProps = {
  station: { ...getNearbyBuoysFixture.data[0], name: undefined },
};

const reduxState = {
  backplane: { user: { settings: getBackplaneSettingsFixture() } },
} as any as AppState;

describe('components / BuoyCard', () => {
  let useActiveBuoyStub: SinonStub;

  const setActiveBuoyIdStub = sinon.stub();

  beforeEach(() => {
    useActiveBuoyStub = sinon.stub(useActiveBuoy, 'default').returns(['', setActiveBuoyIdStub]);
  });

  afterEach(() => {
    setActiveBuoyIdStub.reset();
    useActiveBuoyStub.restore();
  });

  it('should render a buoy card', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyCard, dataProps, { initialState: reduxState });

    expect(wrapper.find(BuoyCardHeader)).to.have.length(1);
    expect(wrapper.find(BuoyCardHeader).prop('offline')).to.be.false();
    expect(wrapper.find(BuoyCardHeader).prop('name')).to.be.equal(dataProps.station.name);
    expect(wrapper.find(BuoyCardHeader).prop('latestTimestamp')).to.be.equal(
      dataProps.station.latestData.timestamp,
    );
    expect(wrapper.find(BuoyCardData)).to.have.length(1);
    expect(wrapper.find(BuoyCardData).prop('offline')).to.be.false();
    expect(wrapper.find(BuoyCardData).prop('latestData')).to.be.equal(dataProps.station.latestData);

    expect(wrapper.find(Link)).to.have.length(1);
    expect(wrapper.find(Link).prop('href')).to.be.equal(
      `/buoy-report/${slugify(dataProps.station.name)}-buoy-${dataProps.station.sourceId}/${
        dataProps.station.id
      }`,
    );
  });

  it('should render an active buoy card', () => {
    useActiveBuoyStub.returns([dataProps.station.id, setActiveBuoyIdStub]);

    const { wrapper } = mountWithReduxAndRouter(
      BuoyCard,
      { ...dataProps, withActiveState: true },
      { initialState: reduxState },
    );

    expect(wrapper.find('div.buoyCardActive')).to.have.length(1);
  });

  it('should call setActiveBuoyId on hover if withActiveState', () => {
    const { wrapper } = mountWithReduxAndRouter(
      BuoyCard,
      { ...dataProps, withActiveState: true },
      { initialState: reduxState },
    );

    const motionWrapper = wrapper.find(motion.div);

    motionWrapper.prop('onHoverStart')!({} as any, {} as EventInfo);

    expect(setActiveBuoyIdStub).to.have.been.calledOnceWithExactly(dataProps.station.id);
    setActiveBuoyIdStub.reset();

    motionWrapper.prop('onHoverEnd')!({} as any, {} as EventInfo);

    expect(setActiveBuoyIdStub).to.have.been.calledOnceWithExactly();
  });

  it('should not call setActiveBuoyId on hover if withActiveState is false', () => {
    const { wrapper } = mountWithReduxAndRouter(
      BuoyCard,
      { ...dataProps },
      { initialState: reduxState },
    );

    const motionWrapper = wrapper.find(motion.div);

    motionWrapper.prop('onHoverStart')!({} as any, {} as EventInfo);

    expect(setActiveBuoyIdStub).to.not.have.been.called();
    setActiveBuoyIdStub.reset();

    motionWrapper.prop('onHoverEnd')!({} as any, {} as EventInfo);

    expect(setActiveBuoyIdStub).to.not.have.been.called();
  });

  it('should not set the active animation if `withActiveState` is false', () => {
    const { wrapper } = mountWithReduxAndRouter(
      BuoyCard,
      { ...dataProps, withActiveState: false },
      { initialState: reduxState },
    );

    expect(wrapper.find(motion.div).prop('animate')).to.deep.equal({ scale: 1 });
  });

  it('should call setActiveBuoyId on tap', () => {
    const { wrapper } = mountWithReduxAndRouter(
      BuoyCard,
      { ...dataProps, withActiveState: true },
      {
        initialState: reduxState,
      },
    );

    const motionWrapper = wrapper.find(motion.div);

    motionWrapper.prop('onTapStart')!({} as any, {} as EventInfo);

    expect(setActiveBuoyIdStub).to.have.been.calledOnceWithExactly(dataProps.station.id);
  });

  it('should use the buoy ID as the name if a buoy name is missing', () => {
    const { wrapper } = mountWithReduxAndRouter(
      BuoyCard,
      { ...stationNoNameProps },
      { initialState: reduxState },
    );

    expect(wrapper.find(BuoyCardHeader)).to.have.length(1);
    expect(wrapper.find(BuoyCardHeader).prop('name')).to.be.equal(stationNoNameProps.station.id);
    expect(wrapper.find(Link)).to.have.length(1);
    expect(wrapper.find(Link).prop('href')).to.be.equal(
      `/buoy-report/buoy-${stationNoNameProps.station.sourceId}/${stationNoNameProps.station.id}`,
    );
  });
});
