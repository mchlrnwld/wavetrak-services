import { expect } from 'chai';
import { METERS } from '@surfline/web-common';

import { mountWithReduxAndRouter } from '../../utils/test-utils';
import { getBackplaneSettingsFixture } from '../../utils/fixtures/store';
import BuoyCardData from './BuoyCardData';
import BuoySignificantSwell from '../BuoySignificantSwell/BuoySignificantSwell';
import BuoySwells from '../BuoySwells';
import { AppState } from '../../stores';

const latestData = {
  height: 1.56,
  period: 18,
  direction: 220,
  swells: [
    { height: 0.567, direction: 100, period: 12 },
    { height: 2.1, direction: 10, period: 18 },
    { height: 1 },
  ],
  timestamp: 0,
  utcOffset: 0,
};

const reduxState = {
  backplane: { user: { settings: getBackplaneSettingsFixture({ swellHeight: METERS }) } },
} as any as AppState;

const defaultProps = { latestData, offline: false };

describe('components / BuoyCard / BuoyCardData', () => {
  it('should render the buoy data', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyCardData, defaultProps, {
      initialState: reduxState,
    });

    const significantSwell = wrapper.find(BuoySignificantSwell);
    expect(significantSwell).to.have.length(1);

    const swells = wrapper.find(BuoySwells);
    expect(swells).to.have.length(1);
  });

  it('should handle offline buoys', () => {
    const { wrapper } = mountWithReduxAndRouter(
      BuoyCardData,
      { ...defaultProps, offline: true },
      {
        initialState: reduxState,
      },
    );

    const significantSwell = wrapper.find(BuoySignificantSwell);
    expect(significantSwell).to.have.length(1);
    expect(significantSwell.prop('offline')).to.be.true();

    const swells = wrapper.find(BuoySwells);
    expect(swells).to.have.length(1);
    expect(swells.prop('offline')).to.be.true();
  });

  it('should support the largeOnly prop', () => {
    const { wrapper } = mountWithReduxAndRouter(
      BuoyCardData,
      { ...defaultProps, largeOnly: true },
      {
        initialState: reduxState,
      },
    );

    const largeOnly = wrapper.find('.buoyCardDataLarge');
    expect(largeOnly).to.have.length(1);
  });
});
