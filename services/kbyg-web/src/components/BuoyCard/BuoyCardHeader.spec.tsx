import { expect } from 'chai';

import { mountWithReduxAndRouter } from '../../utils/test-utils';

import BuoyCardHeader from './BuoyCardHeader';
import { getBackplaneSettingsFixture } from '../../utils/fixtures/store';
import { DMY } from '../../common/constants';
import { AppState } from '../../stores';

const defaultProps = {
  name: 'Some Buoy Name',
  // Fri Jan 01 2021 00:00:00 GMT-0800 (Pacific Standard Time)
  latestTimestamp: 1609488000,
  offline: false,
  active: false,
};

describe('components / BuoyCard / BuoyCardHeader', () => {
  it('should render the name and latest data timestamp', () => {
    const { wrapper } = mountWithReduxAndRouter(
      BuoyCardHeader,
      { ...defaultProps, offline: true },
      {
        initialState: {
          backplane: { user: { settings: getBackplaneSettingsFixture() } },
        } as any as AppState,
      },
    );

    const name = wrapper.find('.buoyCardHeaderName');
    const latestTimestamp = wrapper.find('.buoyCardHeaderTimestamp');

    expect(name).to.have.length(1);
    expect(name.text()).to.be.equal(defaultProps.name);

    expect(latestTimestamp).to.have.length(1);
    expect(latestTimestamp.text()).to.be.equal('8:00am Jan 01, 2021');
  });

  it('should render the latestTimestamp based on date format', () => {
    const { wrapper } = mountWithReduxAndRouter(
      BuoyCardHeader,
      { ...defaultProps, offline: true },
      {
        initialState: {
          backplane: { user: { settings: getBackplaneSettingsFixture({ dateFormat: DMY }) } },
        } as any as AppState,
      },
    );

    const latestTimestamp = wrapper.find('.buoyCardHeaderTimestamp');

    expect(latestTimestamp).to.have.length(1);
    expect(latestTimestamp.text()).to.be.equal('8:00am 01, Jan 2021');
  });

  it('should render the active class', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyCardHeader, {
      ...defaultProps,
      active: true,
    });

    expect(wrapper.find('.buoyCardHeaderActive')).to.have.length(1);
  });

  it('should render the active notch', () => {
    const { wrapper } = mountWithReduxAndRouter(BuoyCardHeader, { ...defaultProps });

    expect(wrapper.find('.buoyCardHeaderActiveNotch')).to.have.length(1);
  });
});
