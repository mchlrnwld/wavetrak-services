import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

const TIME_TYPES = ['16day', 'hourly'];

const TIME_TYPE_LABEL = {
  '16day': '16-Day',
  hourly: 'Hourly',
};

const ForecastTimeViewToggle = ({ timeView, onClick }) => {
  const buttonClassName = (isActive) =>
    classNames({
      'sl-forecast-time-view-toggle__button': true,
      'sl-forecast-time-view-toggle__button--active': isActive,
    });

  return (
    <div className="sl-forecast-time-view-toggle">
      {TIME_TYPES.map((timeType) => (
        <button
          type="button"
          key={timeType}
          className={buttonClassName(timeView === timeType)}
          onClick={
            timeView === timeType ? null : () => onClick(timeType, TIME_TYPE_LABEL[timeType])
          }
        >
          {TIME_TYPE_LABEL[timeType]}
        </button>
      ))}
    </div>
  );
};

ForecastTimeViewToggle.propTypes = {
  timeView: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

ForecastTimeViewToggle.defaultProps = {};

export default ForecastTimeViewToggle;
