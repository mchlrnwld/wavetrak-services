import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import ForecastTimeViewToggle from './ForecastTimeViewToggle';

describe('components / ForecastTimeViewToggle', () => {
  it('has only 1 active view state', () => {
    const wrapper = shallow(<ForecastTimeViewToggle timeView="hourly" />);
    const stateOptionsWrapper = wrapper.find('.sl-forecast-time-view-toggle__button');
    const activeStateWrapper = wrapper.find('.sl-forecast-time-view-toggle__button--active');
    expect(stateOptionsWrapper).to.have.length(2);
    expect(activeStateWrapper).to.have.length(1);
  });
});
