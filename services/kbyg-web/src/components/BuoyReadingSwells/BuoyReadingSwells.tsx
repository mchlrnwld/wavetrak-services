import React from 'react';
import classNames from 'classnames';
import { SwellStats } from '@surfline/quiver-react';
import Skeleton from 'react-loading-skeleton';

import { BuoySwell } from '../../types/buoys';
import { Units } from '../../types/units';

import styles from './BuoyReadingSwells.module.scss';

interface Props {
  swells?: BuoySwell[];
  units?: Units;
  loading?: boolean;
  classes?: { root?: string };
}

const getClass = (rootClass?: string) =>
  classNames(
    {
      [styles.buoyReadingSwells]: true,
    },
    rootClass,
  );

const BuoyReadingSwells: React.FC<Props> = ({ swells = [], units, loading, classes = {} }) => (
  <div className={getClass(classes.root)}>
    {!loading ? (
      <SwellStats
        swells={swells}
        units={units}
        title="Individual Swells"
        isEmptySwell={!swells?.length}
      />
    ) : (
      <Skeleton count={4} width={150} />
    )}
  </div>
);

export default BuoyReadingSwells;
