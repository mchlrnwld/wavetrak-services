import React from 'react';

import { expect } from 'chai';
import { mount } from 'enzyme';
import { SwellStats } from '@surfline/quiver-react';
import Skeleton from 'react-loading-skeleton';
import BuoyReadingSwells from './BuoyReadingSwells';
import { Units } from '../../types/units';

describe('components / BuoyReadingSwells', () => {
  const props = {
    swells: [
      {
        period: 1.03,
        height: 5.114,
        direction: 180,
        index: 0,
      },
      {
        period: 15,
        height: 5.114,
        direction: 20,
        index: 1,
      },
      {
        period: 20,
        height: 1.567,
        direction: 215,
        index: 2,
      },
    ],
    units: { swellHeight: 'FT' } as Units,
    loading: false,
  };

  const propsWithLoading = {
    swells: [],
    units: { swellHeight: 'FT' } as Units,
    loading: true,
  };

  const propsWithError = {
    loading: false,
  };

  it('renders outer individual swells div', () => {
    const wrapper = mount(<BuoyReadingSwells {...props} />);

    const outer = wrapper.find('.buoyReadingSwells');

    expect(outer).to.have.length(1);
  });

  it('renders 1 SwellStats component', () => {
    const wrapper = mount(<BuoyReadingSwells {...props} />);

    const SwellStatsInstance = wrapper.find(SwellStats);
    expect(SwellStatsInstance).to.have.length(1);
  });

  it('renders loading skeleton', () => {
    const wrapper = mount(<BuoyReadingSwells {...propsWithLoading} />);

    const LoadingSkeletonInstance = wrapper.find(Skeleton);
    expect(LoadingSkeletonInstance).to.have.length(1);
  });

  it('renders individual swells as empty swells component on error', () => {
    const wrapper = mount(<BuoyReadingSwells {...propsWithError} />);

    const emptySwells = wrapper.find('.quiver-condition-stats__empty-swells');
    expect(emptySwells).to.have.length(1);
  });
});
