import PropTypes from 'prop-types';
import React from 'react';
import { Chevron, createAccessibleOnClick } from '@surfline/quiver-react';
import { slugify, kbygPaths } from '@surfline/web-common';
import nearbySubregionsPropType from '../../propTypes/nearbySubregions';
import en from '../../intl/translations/en';
import subregionForecastChartsPath from '../../utils/urls/subregionForecastChartsPath';
import WavetrakLink from '../WavetrakLink';

const { subregionForecastPath, subregionPremiumAnalysisPath } = kbygPaths;

const linkTo = (subregionSlug, subregionId, subregionName, pathName) => {
  if (pathName.indexOf('surf-charts') !== -1) {
    return `${subregionForecastChartsPath(subregionId, 'wave-height', subregionName)}`;
  }
  if (pathName.indexOf('premium-analysis') !== -1) {
    return subregionPremiumAnalysisPath(subregionSlug, subregionId);
  }
  return subregionForecastPath(subregionSlug, subregionId);
};

const SubregionLink = ({ subregion, pathName, onClickLink }) => (
  <WavetrakLink
    title={`${subregion.name} ${en.RegionalForecast.header.titleSuffix}`}
    className="sl-forecast-header-menu__nearby-subregion"
    href={linkTo(slugify(subregion.name), subregion._id, subregion.name, pathName)}
    onClick={() => {
      onClickLink(subregion);
    }}
  >
    <span className="sl-forecast-header-menu__nearby-subregion__details__name">
      {subregion.name}
    </span>
  </WavetrakLink>
);

const NearbySubregions = ({
  className,
  title,
  nearbySubregions,
  chevronDirection,
  pathName,
  onClickLink,
  onClick,
}) => (
  <div className={className}>
    <div
      className="sl-forecast-header-menu__dropdown__title"
      {...createAccessibleOnClick((event) => {
        event.preventDefault();
        onClick();
      })}
    >
      {title} <Chevron direction={chevronDirection} />
    </div>
    <div className="sl-forecast-header-menu__nearby-subregions">
      {nearbySubregions.map((subregion) => (
        <SubregionLink
          key={subregion._id}
          subregion={subregion}
          pathName={pathName}
          onClickLink={onClickLink}
        />
      ))}
    </div>
  </div>
);

SubregionLink.propTypes = {
  subregion: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    legacyId: PropTypes.number,
    offshoreSwellLocation: PropTypes.shape({
      type: PropTypes.string,
      coordinates: PropTypes.arrayOf(PropTypes.number),
    }),
  }).isRequired,
  pathName: PropTypes.string,
  onClickLink: PropTypes.func.isRequired,
};

NearbySubregions.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  nearbySubregions: nearbySubregionsPropType.isRequired,
  chevronDirection: PropTypes.oneOf(['down', 'up']).isRequired,
  pathName: PropTypes.string,
  onClickLink: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
};

SubregionLink.defaultProps = {
  pathName: null,
};

NearbySubregions.defaultProps = {
  className: null,
  pathName: null,
};

export default NearbySubregions;
