/* eslint-disable jsx-a11y/click-events-have-key-events */

import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';
import { CamIcon, Chevron, SurfHeight, createAccessibleOnClick } from '@surfline/quiver-react';
import { slugify } from '@surfline/web-common';
import { useRouter } from 'next/router';
import unitsPropType from '../../propTypes/units';
import spotPropType from '../../propTypes/spot';
import conditionClassModifier from '../../utils/conditionClassModifier';
import en from '../../intl/translations/en';
import WavetrakLink from '../WavetrakLink';
import { SL_WEB_CONDITION_VARIATION } from '../../common/treatments';
import { useSplitTreatment } from '../../selectors/split';

const getConditionsClassName = (conditions, expired, sevenRatings = false) =>
  classNames({
    'sl-forecast-header-menu__nearby-spot__details__conditions': true,
    [`sl-forecast-header-menu__nearby-spot__details__conditions${
      sevenRatings ? '-v2' : ''
    }--${conditionClassModifier(conditions, sevenRatings)}`]: true,
    'sl-forecast-header-menu__nearby-spot__details__conditions--expired': expired,
  });

const NearbySpots = ({
  className,
  title,
  units,
  nearbySpots,
  chevronDirection,
  pathGenerator,
  onClickLink,
  onClick,
}) => {
  const router = useRouter();
  const { spotId } = router.query;
  const { treatment, loading } = useSplitTreatment(SL_WEB_CONDITION_VARIATION);
  return (
    <div className={className}>
      <div
        className="sl-forecast-header-menu__dropdown__title"
        {...createAccessibleOnClick((event) => {
          event.preventDefault();
          onClick();
        })}
      >
        {title} <Chevron direction={chevronDirection} />
      </div>
      <div className="sl-forecast-header-menu__nearby-spots">
        {nearbySpots.map((spot) => {
          const isMultiCam = spot.cameras && spot.cameras.length > 1;
          const isCurrentSpot = spotId === spot._id;

          if (isCurrentSpot) return null;
          return (
            <WavetrakLink
              key={spot._id}
              title={`${spot.name} ${en.SpotPage.header.titleSuffix}`}
              className="sl-forecast-header-menu__nearby-spot"
              href={pathGenerator(slugify(spot.name), spot._id)}
              onClick={() => onClickLink(spot)}
            >
              {spot.conditions ? (
                <div className="sl-forecast-header-menu__nearby-spot__details">
                  {!loading ? (
                    <span
                      className={getConditionsClassName(
                        spot.conditions.value,
                        spot.conditions.expired && spot.conditions.human,
                        treatment === 'on',
                      )}
                    />
                  ) : (
                    <span className="sl-forecast-header-menu__nearby-spot__details__conditions" />
                  )}
                  <span className="sl-forecast-header-menu__nearby-spot__details__name">
                    {spot.name}
                  </span>
                </div>
              ) : (
                <span className="sl-forecast-header-menu__nearby-spot__details__name">
                  {spot.name}
                </span>
              )}
              {spot.cameras && spot.cameras.length ? (
                <span className="sl-forecast-header-menu__nearby-spot__icon">
                  <CamIcon isMultiCam={isMultiCam} withText />
                </span>
              ) : null}
              {spot.waveHeight ? (
                <SurfHeight
                  min={spot.waveHeight.min}
                  max={spot.waveHeight.max}
                  units={units.surfHeight}
                />
              ) : null}
            </WavetrakLink>
          );
        })}
      </div>
    </div>
  );
};

NearbySpots.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  units: unitsPropType.isRequired,
  nearbySpots: PropTypes.arrayOf(spotPropType).isRequired,
  chevronDirection: PropTypes.oneOf(['down', 'up']).isRequired,
  pathGenerator: PropTypes.func.isRequired,
  onClickLink: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired,
};

NearbySpots.defaultProps = {
  className: null,
};

export default NearbySpots;
