import { expect } from 'chai';
import sinon from 'sinon';
import ForecastHeaderMenu from './ForecastHeaderMenu';
import { getBackplaneSettingsFixture } from '../../utils/fixtures/store';
import { mountWithReduxAndRouter } from '../../utils/test-utils';
import nearbySpotsFixture from '../../actions/fixtures/spot/nearby';
import nearbyRegionsFixture from '../../actions/fixtures/region/nearby';
import * as splitSelectors from '../../selectors/split';

const reduxState = { backplane: { user: { settings: getBackplaneSettingsFixture() } } };

describe('components / ForecastHeaderMenu', () => {
  beforeEach(() => {
    sinon.stub(splitSelectors, 'useSplitTreatment').returns({ loading: false, treatment: null });
  });

  afterEach(() => {
    splitSelectors.useSplitTreatment.restore();
  });

  it('renders with NearbySpots component', () => {
    const { wrapper } = mountWithReduxAndRouter(
      ForecastHeaderMenu,
      { nearbySpots: nearbySpotsFixture.data.spots, userSettings: getBackplaneSettingsFixture() },
      { initialState: reduxState },
    );
    const nearbySpotWrapper = wrapper.find('a.sl-forecast-header-menu__nearby-spot');
    expect(nearbySpotWrapper).to.have.length(22);

    const nearbySpotsLabelWrapper = wrapper.find('.sl-forecast-header-menu__nearby-spots');
    expect(nearbySpotsLabelWrapper).to.have.length(1);
  });

  it('renders without the NearbySpots component', () => {
    const { wrapper } = mountWithReduxAndRouter(
      ForecastHeaderMenu,
      { nearbySpots: null, userSettings: getBackplaneSettingsFixture() },
      { initialState: reduxState },
    );
    const nearbySpotsWrapper = wrapper.find('.sl-forecast-header-menu__nearby-spots');
    expect(nearbySpotsWrapper).to.have.length(0);
  });

  it('renders with the NearbyRegions component', () => {
    const { wrapper } = mountWithReduxAndRouter(
      ForecastHeaderMenu,
      {
        nearbySubregions: nearbyRegionsFixture.data.subregions,
        pathName: 'surf-forecasts',
        userSettings: getBackplaneSettingsFixture(),
      },
      { initialState: reduxState },
    );
    const nearbyRegionsLabelWrapper = wrapper.find('.sl-forecast-header-menu__nearby-subregions');
    expect(nearbyRegionsLabelWrapper).to.have.length(1);

    const nearbyRegionsWrapper = wrapper.find('a.sl-forecast-header-menu__nearby-subregion');
    expect(nearbyRegionsWrapper).to.have.length(4);
  });

  it('renders without the NearbyRegions component', () => {
    const { wrapper } = mountWithReduxAndRouter(
      ForecastHeaderMenu,
      {
        nearbySubregions: null,
        pathName: 'surf-forecasts',
        userSettings: getBackplaneSettingsFixture(),
      },
      { initialState: reduxState },
    );
    const nearbyRegionsLabelWrapper = wrapper.find('.sl-forecast-header-menu__nearby-subregions');
    expect(nearbyRegionsLabelWrapper).to.have.length(0);
  });

  it('renders the External Links', () => {
    const externalLinks = [
      {
        newWindow: false,
        path: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/buoys/canada/nova-scotia/halifax/6324729',
        text: 'Buoys',
      },
    ];
    const { wrapper } = mountWithReduxAndRouter(
      ForecastHeaderMenu,
      {
        externalLinks1: externalLinks,
        userSettings: getBackplaneSettingsFixture(),
      },
      { initialState: reduxState },
    );
    const externalLinksWrapper = wrapper.find('.sl-forecast-header-menu__external-link');
    expect(externalLinksWrapper).to.have.length(1);
  });
});
