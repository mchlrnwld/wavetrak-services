/* eslint-disable jsx-a11y/click-events-have-key-events */

import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames';
import { getWindow, kbygPaths, trackEvent } from '@surfline/web-common';
import { Chevron, CloseIcon } from '@surfline/quiver-react';
import userSettingsPropTypes from '../../propTypes/userSettings';
import spotPropTypes from '../../propTypes/spot';
import nearbySubregionsPropType from '../../propTypes/nearbySubregions';
import linkPropTypes from '../../propTypes/link';
import en from '../../intl/translations/en';
import ExternalLinks from './ExternalLinks';
import NearbySpots from './NearbySpots';
import NearbySubregions from './NearbySubregions';

const { spotReportPath } = kbygPaths;
class ForecastHeaderMenu extends Component {
  constructor() {
    super();
    this.state = {
      mobileMenuOpen: false,
      camsReportsCollapsed: true,
      nearbySubregionsCollapsed: true,
    };
  }

  async componentDidMount() {
    const win = getWindow();
    win.addEventListener('resize', this.resizeListener);
  }

  componentWillUnmount() {
    const win = getWindow();
    win.removeEventListener('resize', this.resizeListener);
    win.document.body.style.position = 'relative';
  }

  onClickExternalLink = (item) => {
    const { spotPage } = this.props;

    this.toggleMobileMenu();

    if (item.segmentProperties) return item.segmentProperties;

    return {
      linkUrl: item.path,
      linkName: item.text,
      linkLocation: spotPage ? 'spot forecast navigation' : 'regional forecast navigation',
      category: spotPage ? 'spot forecast' : 'regional forecast',
    };
  };

  updateScrollLock = () => {
    const { mobileMenuOpen } = this.state;
    const win = getWindow();
    if (mobileMenuOpen && win?.innerWidth > 768) {
      this.toggleMobileMenu();
    }
  };

  resizeListener = () => {
    if (this.resizeTimeout) return;
    this.resizeTimeout = setTimeout(() => {
      this.resizeTimeout = null;
      this.updateScrollLock();
    }, 66);
  };

  toggleMobileMenu = () => {
    const { mobileMenuOpen } = this.state;
    const win = getWindow();
    if (!mobileMenuOpen && win.innerWidth < 769) {
      win.document.body.style.position = 'fixed';
    } else {
      win.document.body.style.position = 'relative';
    }
    this.setState({
      mobileMenuOpen: !mobileMenuOpen,
    });
  };

  mobileMenuClassName = () => {
    const { mobileMenuOpen } = this.state;
    return classNames({
      'sl-forecast-header-menu__contents': true,
      'sl-forecast-header-menu__contents--open': mobileMenuOpen,
    });
  };

  collapsibleDropdownClassName = (open) =>
    classNames({
      'sl-forecast-header-menu__dropdown': true,
      'sl-forecast-header-menu__dropdown--open': open,
    });

  render() {
    const { mobileMenuOpen, camsReportsCollapsed, nearbySubregionsCollapsed } = this.state;
    const {
      externalLinks1,
      externalLinks2,
      nearbySpots,
      nearbySubregions,
      userSettings,
      spotPage,
      pathName,
    } = this.props;

    return (
      <div className="sl-forecast-header-menu">
        <div onClick={this.toggleMobileMenu} className="sl-forecast-header-menu__title">
          {en.ForecastHeader.menuHeader} <Chevron direction="down" />
        </div>
        <div className={this.mobileMenuClassName()}>
          <div onClick={this.toggleMobileMenu} className="sl-forecast-header-menu__header">
            <span className="sl-flex-placeholder" />
            <span className="sl-forecast-header-menu__header__title">
              {en.ForecastHeader.menuHeader}
            </span>
            <CloseIcon />
          </div>
          <ExternalLinks links={externalLinks1} onClickLink={this.onClickExternalLink} />
          {nearbySpots ? (
            <NearbySpots
              className={this.collapsibleDropdownClassName(!camsReportsCollapsed)}
              title={en.ForecastHeader.nearbySpotsTitle}
              units={userSettings?.units}
              nearbySpots={nearbySpots}
              chevronDirection={!mobileMenuOpen || camsReportsCollapsed ? 'down' : 'up'}
              pathGenerator={spotReportPath}
              onClickLink={(spot) => {
                if (mobileMenuOpen) this.toggleMobileMenu();
                trackEvent('Clicked Nearby Spot', {
                  spotId: spot._id,
                  spotName: spot.name,
                  clickedLocation: spotPage ? 'spot forecast header' : 'regional forecast header',
                });
                return true;
              }}
              onClick={() => {
                this.setState({ camsReportsCollapsed: !camsReportsCollapsed });
              }}
            />
          ) : null}
          {nearbySubregions && nearbySubregions.length > 1 ? (
            <NearbySubregions
              className={this.collapsibleDropdownClassName(!nearbySubregionsCollapsed)}
              title={en.ForecastHeader.nearbySubregionsTitle}
              units={userSettings?.units}
              nearbySubregions={nearbySubregions}
              chevronDirection={!mobileMenuOpen || nearbySubregionsCollapsed ? 'down' : 'up'}
              pathName={pathName}
              onClickLink={(subregion) => {
                if (mobileMenuOpen) this.toggleMobileMenu();
                trackEvent('Clicked Nearby Subregion', {
                  subregionId: subregion._id,
                  subregionName: subregion.name,
                  clickedLocation: spotPage ? 'spot forecast header' : 'regional forecast header',
                });
              }}
              onClick={() => {
                this.setState({ nearbySubregionsCollapsed: !nearbySubregionsCollapsed });
              }}
            />
          ) : null}
          {externalLinks2.length ? (
            <div className="sl-forecast-header-menu__secondary-links">
              <ExternalLinks links={externalLinks2} onClickLink={this.onClickExternalLink} />
            </div>
          ) : null}
          {externalLinks2.length ? (
            <div className="sl-forecast-header-menu__dropdown sl-forecast-header-menu__dropdown--secondary">
              <div className="sl-forecast-header-menu__dropdown__title">
                {en.ForecastHeader.menuHeader} <Chevron direction="down" />
              </div>
              <ExternalLinks links={externalLinks2} onClickLink={this.onClickExternalLink} />
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

ForecastHeaderMenu.defaultProps = {
  nearbySpots: null,
  nearbySubregions: null,
  spotPage: false,
  externalLinks1: [],
  externalLinks2: [],
  pathName: null,
};

ForecastHeaderMenu.propTypes = {
  nearbySpots: PropTypes.arrayOf(spotPropTypes),
  nearbySubregions: nearbySubregionsPropType,
  externalLinks1: PropTypes.arrayOf(PropTypes.shape(linkPropTypes)),
  externalLinks2: PropTypes.arrayOf(PropTypes.shape(linkPropTypes)),
  userSettings: userSettingsPropTypes.isRequired,
  spotPage: PropTypes.bool,
  pathName: PropTypes.string,
};

export default ForecastHeaderMenu;
