import PropTypes from 'prop-types';
import React, { useRef, useCallback } from 'react';
import { TrackableLink } from '@surfline/quiver-react';
import linkPropTypes from '../../propTypes/link';

const ExternalLink = React.memo(({ item, onClickLink }) => {
  const linkRef = useRef();

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const eventPropertiesCallback = useCallback(() => onClickLink(item), []);

  return (
    <TrackableLink
      ref={linkRef}
      eventName={item.eventName || 'Clicked Link'}
      eventProperties={eventPropertiesCallback}
    >
      <a
        ref={linkRef}
        key={item.text}
        className="sl-forecast-header-menu__external-link"
        href={item.path}
        target={item.newWindow ? '_blank' : ''}
        rel="noreferrer"
      >
        {item.text}
      </a>
    </TrackableLink>
  );
});

ExternalLink.displayName = 'ExternalLink';

const ExternalLinks = ({ links, onClickLink }) => (
  <div className="sl-forecast-header-menu__external-links">
    {links.map((item) => (
      <ExternalLink key={item?.path} item={item} onClickLink={onClickLink} />
    ))}
  </div>
);

ExternalLink.propTypes = {
  item: PropTypes.shape(linkPropTypes).isRequired,
  onClickLink: PropTypes.func.isRequired,
};

ExternalLinks.propTypes = {
  links: PropTypes.arrayOf(PropTypes.shape(linkPropTypes)).isRequired,
  onClickLink: PropTypes.func.isRequired,
};

export default ExternalLinks;
