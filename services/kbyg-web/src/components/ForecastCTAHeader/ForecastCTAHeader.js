import PropTypes from 'prop-types';
import React from 'react';

const ForecastCTAHeader = ({ copy }) => {
  const HeaderIcon = copy.icon;
  return (
    <div className="sl-forecast-cta-header">
      {HeaderIcon ? (
        <div className="sl-forecast-cta-header__icon">
          <HeaderIcon />
        </div>
      ) : null}
      <div className="sl-forecast-cta-header__title">{copy.title}</div>
    </div>
  );
};

ForecastCTAHeader.propTypes = {
  copy: PropTypes.shape().isRequired,
};

export default ForecastCTAHeader;
