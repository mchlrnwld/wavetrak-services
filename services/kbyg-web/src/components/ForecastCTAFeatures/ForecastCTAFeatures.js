import PropTypes from 'prop-types';
import React from 'react';

const ForecastCTAFeatures = ({ copy }) => {
  const CamCTAIcon = copy.cams.icon;
  const ForecastsCTAIcon = copy.forecasts.icon;
  const ExpertCTAIcon = copy.expert.icon;

  return (
    <div className="sl-forecast-cta-features">
      <div className="sl-forecast-cta-features__feature">
        <div className="sl-forecast-cta-features__icon">
          <CamCTAIcon />
        </div>
        <div className="sl-forecast-cta-features__text">
          <div className="sl-forecast-cta-features__title">{copy.cams.title}</div>
          <div className="sl-forecast-cta-features__description">{copy.cams.description}</div>
        </div>
      </div>
      <div className="sl-forecast-cta-features__feature">
        <div className="sl-forecast-cta-features__icon">
          <ForecastsCTAIcon />
        </div>
        <div className="sl-forecast-cta-features__text">
          <div className="sl-forecast-cta-features__title">{copy.forecasts.title}</div>
          <div className="sl-forecast-cta-features__description">{copy.forecasts.description}</div>
        </div>
      </div>
      <div className="sl-forecast-cta-features__feature">
        <div className="sl-forecast-cta-features__icon">
          <ExpertCTAIcon />
        </div>
        <div className="sl-forecast-cta-features__text">
          <div className="sl-forecast-cta-features__title">{copy.expert.title}</div>
          <div className="sl-forecast-cta-features__description">{copy.expert.description}</div>
        </div>
      </div>
    </div>
  );
};

ForecastCTAFeatures.propTypes = {
  copy: PropTypes.shape({
    cams: PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string,
      icon: PropTypes.node,
    }),
    forecasts: PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string,
      icon: PropTypes.node,
    }),
    expert: PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string,
      icon: PropTypes.node,
    }),
  }).isRequired,
};

export default ForecastCTAFeatures;
