import React from 'react';
import classNames from 'classnames';
import Skeleton from 'react-loading-skeleton';

import styles from './BuoyReadingMetric.module.scss';

const getBuoyReadingsClass = (metric: string, loading?: boolean, rootClass?: string) =>
  classNames(
    {
      'sl-buoy-reading-metric': true,
      [`sl-buoy-reading-metric--${metric}`]: metric,
      [`sl-buoy-reading-metric--loading`]: loading,
    },
    rootClass,
  );

// Set 'No Data' font to red if height & period are both missing
const getBuoyStatsClass = (noData: boolean, incompleteReading?: boolean, statsClass?: string) =>
  classNames(
    {
      [styles.metricStats]: true,
      [styles.metricStatsNoData]: noData,
      [styles.metricStatsNoDataImportant]: noData && incompleteReading,
    },
    statsClass,
  );

interface Props {
  title: string;
  altTitle?: string;
  measurement?: string | number;
  units?: string;
  metric: string;
  loading?: boolean;
  incompleteReading?: boolean;
  classes?: {
    root?: string;
    stats?: string;
  };
}

const BuoyReadingMetric: React.FC<Props> = ({
  classes = {},
  title,
  altTitle,
  measurement,
  units,
  metric,
  loading,
  incompleteReading,
}) => (
  <section className={getBuoyReadingsClass(metric, loading, classes.root)}>
    {/* alt title for mobile - regular title for tablet and up */}
    <div className="sl-buoy-current-readings__title-container">
      {loading ? (
        <Skeleton width={150} />
      ) : (
        <>
          <h4 className={styles.metricAltTitle}>{altTitle || title}</h4>
          <h4 className={styles.metricTitle}>{title}</h4>
        </>
      )}
    </div>
    <div className={styles.metricStats}>
      {loading ? (
        <Skeleton width={150} />
      ) : (
        <span className={getBuoyStatsClass(!measurement, incompleteReading, classes.stats)}>
          {measurement ? (
            <>
              {measurement}
              {units && <sup className={styles.statsSuperScript}>{units}</sup>}
            </>
          ) : (
            'No Data'
          )}
        </span>
      )}
    </div>
  </section>
);

export default BuoyReadingMetric;
