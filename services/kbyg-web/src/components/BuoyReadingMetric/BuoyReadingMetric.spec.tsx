import React from 'react';

import { expect } from 'chai';
import { mount } from 'enzyme';
import Skeleton from 'react-loading-skeleton';
import BuoyReadingMetric from './BuoyReadingMetric';

describe('components / BuoyReadingMetric', () => {
  const props = {
    title: 'Wave Height',
    altTitle: 'Wave Height',
    measurement: 3,
    units: 'FT',
    metric: 'wave-height',
  };

  const propsWithMissingMeasurement = {
    title: 'Wave Height',
    units: 'FT',
    metric: 'wave-height',
  };

  const propsWithLoading = {
    loading: true,
    title: 'Wave Height',
    units: 'FT',
    metric: 'wave-height',
  };

  it('renders outer metric div', () => {
    const wrapper = mount(<BuoyReadingMetric {...props} />);

    const outer = wrapper.find(`.sl-buoy-reading-metric--${props.metric}`);

    expect(outer).to.have.length(1);
  });

  it('renders buoy metric title', () => {
    const wrapper = mount(<BuoyReadingMetric {...props} />);

    const title = wrapper.find('.metricTitle');

    expect(title.text()).to.equal(props.title);
  });

  it('renders buoy metric measurement', () => {
    const wrapper = mount(<BuoyReadingMetric {...props} />);

    const measurement = wrapper.find('.metricStats');

    expect(measurement.first().text()).to.contain(props.measurement);
  });

  it('renders buoy metric measurement unit', () => {
    const wrapper = mount(<BuoyReadingMetric {...props} />);

    const units = wrapper.find('.statsSuperScript');

    expect(units.text()).to.equal(props.units);
  });

  it('does not render No Data when measurement is present', () => {
    const wrapper = mount(<BuoyReadingMetric {...props} />);

    const noData = wrapper.find('.metricStatsNoData');

    expect(noData).to.have.length(0);
  });

  it('renders No Data when measurement is missing', () => {
    const wrapper = mount(<BuoyReadingMetric {...propsWithMissingMeasurement} />);

    const noData = wrapper.find('.metricStatsNoData');

    expect(noData).to.have.length(1);
  });

  it('renders loading skeletons', () => {
    const wrapper = mount(<BuoyReadingMetric {...propsWithLoading} />);

    const LoadingSkeletonInstance = wrapper.find(Skeleton);
    // 18 is number of loading skeleton lines (mobile and up)
    expect(LoadingSkeletonInstance).to.have.length(2);
  });
});
