import React, { useRef, useMemo } from 'react';
import PropTypes from 'prop-types';
import LinesEllipsis from 'react-lines-ellipsis';
import { Scrollbar, withDevice, TrackableLink } from '@surfline/quiver-react';

import Image from 'next/image';
import featuredContentPropType from '../../propTypes/featuredContent';
import devicePropType from '../../propTypes/device';
import articlePropType from '../../propTypes/article';

const Article = ({ spotId, spotName, article, width, feedLocale }) => {
  const ref = useRef();
  const eventProperties = useMemo(
    () => ({
      title: article.content.title,
      contentType: article.contentType,
      contentId: article.id,
      tags: article.tags,
      destinationUrl: article.permalink,
      spotName,
      spotId,
      feedLocale,
    }),
    [
      spotId,
      spotName,
      article.permalink,
      article.tags,
      article.id,
      article.contentType,
      article.content.title,
      feedLocale,
    ],
  );
  return (
    <TrackableLink
      ref={ref}
      eventName="Clicked Spot Page Feed Link"
      eventProperties={eventProperties}
    >
      <a
        ref={ref}
        key={article.id}
        href={article.permalink}
        className="sl-featured-content__articles__article"
      >
        <Image
          loader={({ src }) => src}
          src={article.media.feed1x || article.media.promobox1x}
          alt={article.content.title}
          layout="fixed"
          width={80}
          height={45}
          loading="lazy"
          style={{ marginLeft: '16px' }}
        />
        <div className="sl-featured-content__articles__article__info">
          <span className="sl-featured-content__articles__article__info__content-type">
            {article.tags[0].name}
          </span>
          <h4>
            {width ? (
              <LinesEllipsis
                className="sl-featured-content__articles__article__info__title"
                text={article.content.title}
                maxLine={2}
                ellipsis="..."
                basedOn="words"
                winWidth={width}
                style={{
                  width: width > 975 ? 188 : width - 160,
                  fontSize: 14,
                  fontWeight: 500,
                  whiteSpace: 'pre-wrap',
                }}
              />
            ) : null}
          </h4>
        </div>
      </a>
    </TrackableLink>
  );
};

const FeaturedContentArticles = ({ articles, device, spotName, spotId, feedLocale, style }) => (
  <div style={{ ...style }}>
    <h3>Featured Content</h3>
    <div id="sl-featured-content__articles" className="sl-featured-content__articles">
      {articles.map((article) => (
        <Article
          key={article.id}
          spotId={spotId}
          spotName={spotName}
          article={article}
          width={device.width}
          feedLocale={feedLocale}
        />
      ))}
    </div>
  </div>
);

const FeaturedContent = ({
  featuredContent: { articles },
  device,
  spotName,
  spotId,
  feedLocale,
}) => (
  <div className="sl-featured-content">
    {!device.mobile ? (
      <Scrollbar speed={18} style={{ height: '100%', padding: '0 18px' }}>
        <FeaturedContentArticles
          articles={articles}
          spotName={spotName}
          spotId={spotId}
          feedLocale={feedLocale}
          device={device}
        />
      </Scrollbar>
    ) : (
      <FeaturedContentArticles
        style={{ height: '100%', padding: '0 18px' }}
        articles={articles.slice(0, 4)}
        spotName={spotName}
        spotId={spotId}
        feedLocale={feedLocale}
        device={device}
      />
    )}
  </div>
);

Article.propTypes = {
  article: articlePropType.isRequired,
  feedLocale: PropTypes.string.isRequired,
  width: PropTypes.number,
  spotName: PropTypes.string.isRequired,
  spotId: PropTypes.string.isRequired,
};

Article.defaultProps = {
  width: 0,
};

FeaturedContentArticles.propTypes = {
  articles: PropTypes.arrayOf(articlePropType).isRequired,
  device: devicePropType.isRequired,
  feedLocale: PropTypes.string.isRequired,
  spotId: PropTypes.string.isRequired,
  spotName: PropTypes.string.isRequired,
  style: PropTypes.shape(),
};

FeaturedContentArticles.defaultProps = {
  style: {},
};

FeaturedContent.propTypes = {
  featuredContent: featuredContentPropType.isRequired,
  feedLocale: PropTypes.string.isRequired,
  device: devicePropType.isRequired,
  spotName: PropTypes.string.isRequired,
  spotId: PropTypes.string.isRequired,
};

export default withDevice(FeaturedContent);
