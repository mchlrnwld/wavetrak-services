import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import MapButton from './MapButton';

describe('components / MapButton', () => {
  it('handles onClick', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(<MapButton onClick={onClick} />);
    wrapper.simulate('click');
    expect(onClick).to.have.been.calledOnce();
  });

  it('renders children', () => {
    const wrapper = shallow(<MapButton>Button</MapButton>);
    expect(wrapper.children().text()).to.equal('Button');
  });
});
