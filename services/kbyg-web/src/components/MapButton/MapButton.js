/* eslint-disable jsx-a11y/click-events-have-key-events */

import PropTypes from 'prop-types';
import React from 'react';

const MapButton = ({ children, onClick }) => (
  <div className="sl-map-button" onClick={onClick}>
    {children}
  </div>
);

MapButton.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
};

MapButton.defaultProps = {
  children: null,
  onClick: () => null,
};

export default MapButton;
