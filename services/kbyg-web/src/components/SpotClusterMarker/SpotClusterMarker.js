import PropTypes from 'prop-types';
import React from 'react';
import { color, conditionColorMap, conditionColorMapV2 } from '@surfline/quiver-themes';
import conditionMapTo7Ratings from '../../utils/conditionMapTo7Ratings';
import { SL_WEB_CONDITION_VARIATION } from '../../common/treatments';
import { useSplitTreatment } from '../../selectors/split';

import styles from './SpotClusterMarker.module.scss';

const gradientOffsetX = 15;
const gradientOffsetY = 27 - Math.sqrt(24 * 24 - gradientOffsetX * gradientOffsetX);

const topGradientStartPoint = `${27 - gradientOffsetX},${gradientOffsetY}`;
const topGradientEndPoint = `${27 + gradientOffsetX},${gradientOffsetY}`;
const bottomGradientStartPoint = `${27 - gradientOffsetX},${54 - gradientOffsetY}`;
const bottomGradientEndPoint = `${27 + gradientOffsetX},${54 - gradientOffsetY}`;

const topGradientPath = `M${topGradientStartPoint} A24,24,0,0,1,${topGradientEndPoint}`;
const bottomGradientPath = `M${bottomGradientStartPoint} A24,24,0,0,0,${bottomGradientEndPoint}`;
const leftArcPath = `M${topGradientStartPoint} A24,24,0,0,0,${bottomGradientStartPoint}`;
const rightArcPath = `M${topGradientEndPoint} A24,24,0,0,1,${bottomGradientEndPoint}`;

const getColorFromColorMap = (colorCondition, colorMap) =>
  colorMap.has(colorCondition) ? colorMap.get(colorCondition).default : color('white');

const SpotClusterMarker = ({ spotCount, worstCondition, bestCondition, onClick }) => {
  const { treatment, loading } = useSplitTreatment(SL_WEB_CONDITION_VARIATION);
  const sevenRatings = treatment === 'on';
  const worstColor = getColorFromColorMap(
    sevenRatings ? conditionMapTo7Ratings(worstCondition) : worstCondition,
    sevenRatings ? conditionColorMapV2 : conditionColorMap,
  );
  const bestColor = getColorFromColorMap(
    sevenRatings ? conditionMapTo7Ratings(bestCondition) : bestCondition,
    sevenRatings ? conditionColorMapV2 : conditionColorMap,
  );

  // This is necessary to prevent the linearGradient defs from conflicting with each other
  const conditionsGradientId = `ConditionsGradient:${worstCondition}:${bestCondition}`;
  const conditionsGradientStroke = `url(#${conditionsGradientId})`;

  return loading ? null : (
    <svg className={styles.spotClusterMarker} width="54px" height="54px" onClick={onClick}>
      <defs>
        <linearGradient id={conditionsGradientId}>
          <stop offset="0%" stopColor={worstColor} />
          <stop offset="100%" stopColor={bestColor} />
        </linearGradient>
      </defs>
      <g>
        <circle cx="27" cy="27" r="24" />
        <path d={leftArcPath} stroke={worstColor} />
        <path d={topGradientPath} stroke={conditionsGradientStroke} />
        <path d={rightArcPath} stroke={bestColor} />
        <path d={bottomGradientPath} stroke={conditionsGradientStroke} />
      </g>
      <text x="27" y="25">
        {spotCount}
      </text>
    </svg>
  );
};

SpotClusterMarker.propTypes = {
  worstCondition: PropTypes.string,
  bestCondition: PropTypes.string,
  spotCount: PropTypes.number.isRequired,
  onClick: PropTypes.func,
};

SpotClusterMarker.defaultProps = {
  worstCondition: null,
  bestCondition: null,
  onClick: () => null,
};

export default SpotClusterMarker;
