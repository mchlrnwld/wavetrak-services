import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import { color, conditionColorMap } from '@surfline/quiver-themes';
import SpotClusterMarker from './SpotClusterMarker';
import * as splitSelectors from '../../selectors/split';

describe('components / SpotClusterMarker', () => {
  beforeEach(() => {
    sinon.stub(splitSelectors, 'useSplitTreatment').returns({ loading: false, treatment: null });
  });

  afterEach(() => {
    splitSelectors.useSplitTreatment.restore();
  });

  it('handles onClick', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(<SpotClusterMarker onClick={onClick} />);
    wrapper.simulate('click');
    expect(onClick).to.have.been.calledOnce();
  });

  it('displays spotCount', () => {
    const wrapper = shallow(<SpotClusterMarker spotCount={15} />);
    const textWrapper = wrapper.find('text');
    expect(textWrapper.text()).to.equal('15');
  });

  it('gradients stroke color of circle path from worstCondition to bestCondition', () => {
    const worstCondition = 'FAIR_TO_GOOD';
    const bestCondition = 'VERY_GOOD';
    const worstColor = conditionColorMap.get(worstCondition).default;
    const bestColor = conditionColorMap.get(bestCondition).default;
    const conditionsGradientId = `ConditionsGradient:${worstCondition}:${bestCondition}`;
    const wrapper = shallow(
      <SpotClusterMarker worstCondition={worstCondition} bestCondition={bestCondition} />,
    );
    const linearGradient = wrapper.find('linearGradient');
    const linearGradientStops = linearGradient.find('stop');
    const paths = wrapper.find('path');
    expect(linearGradient.prop('id')).to.equal(conditionsGradientId);
    expect(linearGradientStops.first().prop('stopColor')).to.equal(worstColor);
    expect(linearGradientStops.last().prop('stopColor')).to.equal(bestColor);
    expect(paths.at(0).prop('stroke')).to.equal(worstColor);
    expect(paths.at(1).prop('stroke')).to.equal(`url(#${conditionsGradientId})`);
    expect(paths.at(2).prop('stroke')).to.equal(bestColor);
    expect(paths.at(3).prop('stroke')).to.equal(`url(#${conditionsGradientId})`);
  });

  it('sets circle path color to white if no conditions provided', () => {
    const white = color('white');
    const conditionsGradientId = 'ConditionsGradient:null:null';
    const wrapper = shallow(<SpotClusterMarker />);
    const linearGradient = wrapper.find('linearGradient');
    const linearGradientStops = linearGradient.find('stop');
    const paths = wrapper.find('path');
    expect(linearGradient.prop('id')).to.equal(conditionsGradientId);
    expect(linearGradientStops.first().prop('stopColor')).to.equal(white);
    expect(linearGradientStops.last().prop('stopColor')).to.equal(white);
    expect(paths.at(0).prop('stroke')).to.equal(white);
    expect(paths.at(1).prop('stroke')).to.equal(`url(#${conditionsGradientId})`);
    expect(paths.at(2).prop('stroke')).to.equal(white);
    expect(paths.at(3).prop('stroke')).to.equal(`url(#${conditionsGradientId})`);
  });
});
