import PropTypes from 'prop-types';
import React, { useRef } from 'react';
import { accountPaths } from '@surfline/web-common';
import { TrackableLink } from '@surfline/quiver-react';
import en from '../../intl/translations/en';
import { CLICKED_SUBSCRIBE_CTA } from '../../common/constants';

const MobileForecastCTA = ({ segmentProperties }) => {
  const linkRef = useRef();
  return (
    <div className="sl-mobile-forecast-cta">
      <span className="sl-mobile-forecast-cta__text">{en.premiumCTA.MobileForecastCTA.text}</span>
      <TrackableLink
        ref={linkRef}
        eventName={CLICKED_SUBSCRIBE_CTA}
        eventProperties={segmentProperties}
      >
        <a ref={linkRef} href={accountPaths.funnelPath} className="sl-mobile-forecast-cta__link">
          {en.premiumCTA.MobileForecastCTA.linkText}
        </a>
      </TrackableLink>
    </div>
  );
};

MobileForecastCTA.propTypes = {
  segmentProperties: PropTypes.oneOfType([PropTypes.shape(), PropTypes.func]).isRequired,
};

export default MobileForecastCTA;
