import PropTypes from 'prop-types';
import React from 'react';
import { CamIcon, PlayMarker } from '@surfline/quiver-react';
import CamListLink from '../CamListLink';
import CamListItemSpotInfo from '../CamListItemSpotInfo';
import spotProptype from '../../propTypes/spot';

const getBackgroundImg = (spot) => (spot.cameras[0] ? spot.cameras[0].stillUrl : spot.thumbnail);

const CamListItem = ({ spot, units }) => {
  const isMultiCam = spot.cameras && spot.cameras.length > 1;
  return (
    <CamListLink spot={spot} location="camera player">
      <div
        className="sl-cam-list-item"
        style={{ backgroundImage: `url('${getBackgroundImg(spot)}')` }}
      >
        <div className="sl-cam-list-item__top-container">
          {spot.cameras[0] ? <PlayMarker /> : null}
          {isMultiCam && <CamIcon isMultiCam withText />}
        </div>
        <div className="sl-cam-list-item__details">
          <CamListItemSpotInfo spot={spot} units={units} />
        </div>
      </div>
    </CamListLink>
  );
};

CamListItem.defaultProps = {};

CamListItem.propTypes = {
  spot: spotProptype.isRequired,
  units: PropTypes.string.isRequired,
};

export default CamListItem;
