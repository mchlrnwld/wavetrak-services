import PropTypes from 'prop-types';
import React from 'react';
import { pageStructuredData } from '@surfline/web-common';
import { format } from 'date-fns';
import { premiumAnalysisLinkMap } from '../../utils/urls';
import MetaTags from '../MetaTags';
import config from '../../config';

const title = (subregionName, timestamp) =>
  `Premium ${subregionName} Weather Forecast for Storms, Swell & Surf in ${format(
    timestamp ? new Date(timestamp * 1000) : new Date(),
    'MMMM yyyy',
  )} - Surfline`;

const description = (subregionName) =>
  `The most accurate and trusted premium ${subregionName} surf reports, forecasts, and coastal weather. Surfers from around the world choose Surfline for daily forecasts and timely news`.replace(
    '  ',
    ' ',
  );

const PremiumAnalysisMeta = ({ subregionId, subregionName, timestamp, headline, author }) => (
  <MetaTags
    title={title(subregionName, timestamp)}
    description={description(subregionName)}
    urlPath={premiumAnalysisLinkMap.all.path(subregionId, subregionName)}
    parentUrl={premiumAnalysisLinkMap.all.path(subregionId, subregionName)}
    parentTitle={title(subregionName, timestamp)}
  >
    <script type="application/ld+json">
      {pageStructuredData({
        url: `${config.surflineHost}${premiumAnalysisLinkMap.all.path(subregionId, subregionName)}`,
        headline: headline || title(subregionName, timestamp),
        dateModified: '10/1/2019',
        author: author || 'Surfline Forecast Team',
        description: description(subregionName),
        cssSelector: '.sl-premium-analysis',
      })}
    </script>
  </MetaTags>
);

PremiumAnalysisMeta.propTypes = {
  subregionId: PropTypes.string.isRequired,
  subregionName: PropTypes.string.isRequired,
  timestamp: PropTypes.number.isRequired,
  headline: PropTypes.string,
  author: PropTypes.string,
};

PremiumAnalysisMeta.defaultProps = {
  headline: null,
  author: null,
};

export default PremiumAnalysisMeta;
