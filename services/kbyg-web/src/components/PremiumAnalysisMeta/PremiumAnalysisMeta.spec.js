import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import MetaTags from '../MetaTags';
import PremiumAnalysisMeta from './PremiumAnalysisMeta';

describe('components / PremiumAnalysisMeta', () => {
  /** @type {sinon.SinonFakeTimers} */
  let clock;
  const now = 1633824000; // Sun Oct 10 2021 00:00:00 GMT+0000

  beforeAll(() => {
    clock = sinon.useFakeTimers(now * 1000);
  });

  afterAll(() => {
    clock.restore();
  });

  it('passes correct data values to MetaTags', () => {
    const title =
      'Premium North Orange County Weather Forecast for Storms, Swell & Surf in May 2018 - Surfline';
    const description =
      'The most accurate and trusted premium North Orange County surf reports, forecasts, and coastal weather. Surfers from around the world choose Surfline for daily forecasts and timely news';
    const urlPath = '/surf-forecasts/north-orange-county/58581a836630e24c44878fd6/premium-analysis';
    const wrapper = shallow(
      <PremiumAnalysisMeta
        subregionName="North Orange County"
        subregionId="58581a836630e24c44878fd6"
        tabPath="all"
        timestamp={1525470133}
      />,
    );
    const metaTagsWrapper = wrapper.find(MetaTags);
    expect(metaTagsWrapper.prop('title')).to.equal(title);
    expect(metaTagsWrapper.prop('description')).to.equal(description);
    expect(metaTagsWrapper.prop('urlPath')).to.equal(urlPath);
  });

  it('defaults to the current date for the title if no timestamp exists', () => {
    const title =
      'Premium North Orange County Weather Forecast for Storms, Swell & Surf in October 2021 - Surfline';
    const wrapper = shallow(
      <PremiumAnalysisMeta
        subregionName="North Orange County"
        subregionId="58581a836630e24c44878fd6"
        tabPath="all"
      />,
    );
    const metaTagsWrapper = wrapper.find(MetaTags);
    expect(metaTagsWrapper.prop('title')).to.equal(title);
  });
});
