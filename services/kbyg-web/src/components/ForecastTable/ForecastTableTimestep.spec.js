import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import ForecastTableTimestep from './ForecastTableTimestep';
import { getCustomMetrics, getTableSwells } from './utils';

describe('components / ForecastTable / ForecastTableTimestep', () => {
  const props = {
    units: {},
    wave: {
      swells: [],
      surf: {},
    },
    wind: {},
  };

  it('can be tabbed to, clicked, or enter keyed', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(<ForecastTableTimestep {...props} onClick={onClick} />);
    expect(onClick).not.to.have.been.called();
    wrapper.simulate('click');
    expect(onClick).to.have.been.calledOnce();
    wrapper.simulate('keypress', { charCode: 13 });
    expect(onClick).to.have.been.calledTwice();
    wrapper.simulate('keypress', {});
    expect(onClick).to.have.been.calledTwice();
  });

  describe('utils / getTableSwells', () => {
    it('returns list of 6 objects from null input', () => {
      const response = getTableSwells(null, true, null);
      expect(response).to.have.length(6);
      response.forEach((swell) => {
        expect(swell.index).to.be.lessThanOrEqual(6);
        expect(swell.index).to.be.greaterThanOrEqual(0);
      });
    });

    it('returns list of 6 objects sorted by swell impact', () => {
      // using swell impact expression `height * (.1 * period)`
      const sortedBySwellImpact = [
        // 3 * (.1 * 20) = 6
        { height: 3, period: 20 },
        // 2 * (.1 * 25) = 5
        { height: 2, period: 25 },
        // 4 * (.1 * 10) = 4
        { height: 4, period: 10 },
        // 1 * (.1 * 10) = 1
        { height: 1, period: 10 },
      ];
      const response = getTableSwells(
        {
          swells: [
            { height: 1, period: 10 },
            { height: 3, period: 20 },
            { height: 4, period: 10 },
            { height: 2, period: 25 },
          ],
        },
        false,
        null,
      );
      expect(response).to.have.length(4);
      expect(response).to.eql(sortedBySwellImpact);
    });
  });

  describe('utils / getCustomMetrics', () => {
    it('returns custom metrics on override swell columns', () => {
      const response = getCustomMetrics(true, { metrics: { test: 'value' } });

      expect(response).to.be.deep.equal({ test: 'value' });
    });
    it('returns null on false override swell columns option', () => {
      const response = getCustomMetrics(false, { metrics: { test: 'value' } });

      expect(response).to.be.null();
    });
  });
});
