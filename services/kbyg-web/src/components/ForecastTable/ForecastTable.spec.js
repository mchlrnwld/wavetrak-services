import React from 'react';
import { expect } from 'chai';
import { shallow, mount } from 'enzyme';
import { OptimalScoreLegend } from '@surfline/quiver-react';
import { getBuoyReportFixture } from '../../common/api/fixtures/buoys';
import SwellHeightLegend from '../SwellHeightLegend';
import ForecastTableCTA from '../ForecastTableCTA';
import ForecastTable from './ForecastTable';
import { MDY, DMY } from '../../common/constants';

describe('components / ForecastTable', () => {
  const wave = {
    units: { waveHeight: 'FT', swellHeight: 'FT' },
    utcOffset: -8,
    days: [...Array(5)].map((_, day) =>
      [...Array(8)].map((_, timestep) => ({
        timestamp: 1523268000 + day * 60 * 60 * 24 + 3 * timestep * 60 * 60,
        utcOffset: 0,
        surf: { min: 3, max: 4 },
        swells: [
          { height: 1, index: 1 },
          { height: 2, index: 2 },
          { height: 3, index: 3 },
          { height: 4, index: 4 },
          { height: 5, index: 5 },
          { height: 6, index: 6 },
        ],
      })),
    ),
  };
  const wind = {
    units: { windSpeed: 'MPH' },
    utcOffset: -8,
    days: [...Array(5)].map((_, day) =>
      [...Array(8)].map((_, timestep) => ({
        timestamp: 1523268000 + day * 60 * 60 * 24 + 3 * timestep * 60 * 60,
        speed: 5,
        direction: 90,
        utcOffset: 0,
      })),
    ),
  };

  const days = [
    {
      blurred: false,
      expanded: false,
    },
    {
      blurred: false,
      expanded: true,
    },
    {
      blurred: false,
      expanded: false,
    },
    {
      blurred: true,
      expanded: true,
    },
  ];

  const props = {
    wave,
    wind,
    isPremium: false,
    utcOffset: 0,
    days,
  };

  it('renders correct column headers', () => {
    const defaultWrapper = shallow(<ForecastTable {...props} />);
    expect(defaultWrapper.find('.sl-forecast-table__cell--header')).to.have.length(9);

    const hideSurfAndWindWrapper = shallow(<ForecastTable {...props} hideSurfAndWind />);
    expect(hideSurfAndWindWrapper.find('.sl-forecast-table__cell--header')).to.have.length(7);

    const bothWrapper = shallow(<ForecastTable {...props} hideSurfAndWind />);
    expect(bothWrapper.find('.sl-forecast-table__cell--header')).to.have.length(7);
  });

  it('renders an error if wave or wind errors', () => {
    const defaultWrapper = shallow(<ForecastTable {...props} />);
    expect(defaultWrapper.find('.sl-forecast-table__error')).to.have.length(0);

    const waveErrorWrapper = shallow(<ForecastTable {...props} wave={{ error: 'some error' }} />);
    expect(waveErrorWrapper.find('.sl-forecast-table__error')).to.have.length(1);

    const windErrorWrapper = shallow(<ForecastTable {...props} wind={{ error: 'some error' }} />);
    expect(windErrorWrapper.find('.sl-forecast-table__error')).to.have.length(1);
  });

  it('renders rows for each day', () => {
    const wrapper = mount(<ForecastTable {...props} />);
    const dayRows = wrapper.find('tbody');
    expect(dayRows).to.have.length(days.length);
    dayRows.forEach((dayRow, i) => {
      const rows = dayRow.find('tr');
      expect(rows).to.have.length(days[i].expanded ? 9 : 1);
    });
  });

  it('should format the date with MDY', () => {
    const wrapper = mount(
      <ForecastTable
        days={getBuoyReportFixture.data.days}
        isCustomTable
        customTableData={getBuoyReportFixture.data}
        dateFormat={MDY}
        hideSurfAndWind
      />,
    );

    const dayRows = wrapper.find('.sl-forecast-table__day th');
    expect(dayRows.at(0).text()).to.equal('Friday 6/25');
  });

  it('should format the date with DMY', () => {
    const wrapper = mount(
      <ForecastTable
        days={getBuoyReportFixture.data.days}
        isCustomTable
        customTableData={getBuoyReportFixture.data}
        dateFormat={DMY}
        hideSurfAndWind
      />,
    );

    const dayRows = wrapper.find('.sl-forecast-table__day th');
    expect(dayRows.at(0).text()).to.equal('Friday 25/6');
  });

  it('can render CTA', () => {
    const defaultWrapper = shallow(<ForecastTable {...props} />);
    expect(defaultWrapper.find(ForecastTableCTA)).to.have.length(0);

    const showCTAWrapper = shallow(<ForecastTable {...props} showCTA />);
    expect(showCTAWrapper.find(ForecastTableCTA)).to.have.length(1);
  });

  it('renders a legend depending on highlight type', () => {
    const optimalWrapper = shallow(<ForecastTable {...props} highlightType="OPTIMAL" />);
    expect(optimalWrapper.find(OptimalScoreLegend)).to.have.length(1);

    const swellHeightWrapper = shallow(<ForecastTable {...props} highlightType="SWELL_HEIGHT" />);
    expect(swellHeightWrapper.find(SwellHeightLegend)).to.have.length(1);
  });
});
