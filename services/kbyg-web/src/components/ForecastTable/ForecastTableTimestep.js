import PropTypes from 'prop-types';
import React from 'react';
import { round } from 'lodash';
import classNames from 'classnames';
import {
  mapDegreesCardinal as cardinal,
  roundSurfHeight,
  getWaveUnits,
} from '@surfline/web-common';
import { WindArrow, WindStats } from '@surfline/quiver-react';
import swellHeightClassesFromUnits from '../../common/swellHeightClassesFromUnits';
import unitsPropType from '../../propTypes/units';
import waveTimestepPropType from '../../propTypes/waveTimestep';
import windTimestepPropType from '../../propTypes/windTimestep';
import buoyReportTimestepPropType from '../../propTypes/buoyReportTimestep';
import highlightTypePropType from '../../propTypes/forecastTableHighlightType';
import ChevronBox from '../ChevronBox';
import { getCustomMetrics, getTableSwells } from './utils';
import ForecastTableHeight from './ForecastTableHeight';

const timestepClassName = (isDay, expanded) =>
  classNames({
    'sl-forecast-table__timestep': true,
    'sl-forecast-table__timestep--day': isDay,
    'sl-forecast-table__timestep--time': !isDay,
    'sl-forecast-table__timestep--expanded': expanded,
  });

const swellHeightClass = (height, swellHeightClasses) =>
  swellHeightClasses.find((max) => height < max) ||
  swellHeightClasses[swellHeightClasses.length - 1];

const roundWithMappingCheck = (min, max, waveHeightUnit) => {
  if (max > 0.5 && max <= 1 && (waveHeightUnit === 'FT' || waveHeightUnit === 'HI'))
    return {
      min: 0.5,
      max: 1,
      unit: 'ft',
    };
  if (max > 0 && max <= 0.5 && (waveHeightUnit === 'FT' || waveHeightUnit === 'HI'))
    return {
      min: 0,
      max: 0.5,
      unit: 'ft',
    };

  return {
    min: roundSurfHeight(min, waveHeightUnit),
    max: roundSurfHeight(max, waveHeightUnit),
    unit: waveHeightUnit,
  };
};

const timestepCellClassName = ({
  highlightType,
  score,
  swellHeight,
  units,
  swellHeightClasses,
  isTimeLabel,
}) => {
  const className = {
    'sl-forecast-table__cell': true,
    'sl-forecast-table__cell--time-label': isTimeLabel,
  };

  switch (highlightType) {
    case 'OPTIMAL':
      className['sl-forecast-table__cell--optimal'] = score === 2;
      className['sl-forecast-table__cell--good'] = score === 1;
      break;
    case 'SWELL_HEIGHT': {
      const height = `${swellHeightClass(swellHeight, swellHeightClasses)
        .toString()
        .replace('.', '_')}-${units.swellHeight.toLowerCase()}`;
      className[`sl-forecast-table__cell--swell-${height}`] = true;
      break;
    }
    default:
      break;
  }
  return classNames(className);
};

const customTimestepCellClassName = (wind) =>
  classNames({
    'sl-forecast-table__cell': true,
    'sl-forecast-table__cell--wind-stats': wind,
  });

const headerColSpan = (
  isExpandedDay,
  hideSurfAndWind,
  customColumnsLength,
  overrideSwellColumns,
) => {
  if (!isExpandedDay) return 0;
  if (overrideSwellColumns && customColumnsLength) {
    return customColumnsLength + 1;
  }
  return 7 + 2 * !hideSurfAndWind + customColumnsLength;
};

const getUnits = (units, _metric = '') => {
  const metric = _metric.toLowerCase();
  if (metric.includes('temp') || metric.includes('dew')) {
    return `º${units.temperature}`;
  }
  if (metric.includes('pressure')) {
    return 'mb';
  }
  return '';
};

const ForecastTableTimestep = ({
  units,
  label,
  wave,
  wind,
  customTableData,
  isCustomTable,
  customColumns,
  overrideSwellColumns,
  isDay,
  expanded,
  hideSurfAndWind,
  onClick,
  highlightType,
}) => {
  const swellScore = !isCustomTable
    ? Math.max(...wave.swells.map((swell) => swell.optimalScore))
    : null;
  const combinedScore = swellScore && wind.optimalScore === 2 && swellScore === 2 ? 2 : 0;
  const swells = getTableSwells(wave, isCustomTable, customTableData);
  const customMetrics = getCustomMetrics(overrideSwellColumns, customTableData);
  const isIndividualTimeReading = !(isDay && expanded);
  const showCustomAlternateSwellMetric =
    isIndividualTimeReading && isCustomTable && customColumns && !overrideSwellColumns;

  const swellHeightClasses = swellHeightClassesFromUnits(units.swellHeight);
  const surfValues =
    !isCustomTable && roundWithMappingCheck(wave.surf.min, wave.surf.max, units.waveHeight);

  return (
    <tr
      className={timestepClassName(isDay, expanded)}
      onClick={onClick}
      onKeyPress={({ charCode }) => {
        if (charCode === 13) onClick();
      }}
      tabIndex={onClick ? 0 : null}
    >
      <th
        className={timestepCellClassName({
          highlightType: highlightType !== 'SWELL_HEIGHT' ? highlightType : null,
          score: combinedScore,
          isTimeLabel: !isDay,
        })}
        scope="row"
        colSpan={headerColSpan(
          isDay && expanded,
          hideSurfAndWind,
          customColumns?.length || 0,
          overrideSwellColumns,
        )}
      >
        {isDay ? <ChevronBox direction={expanded ? 'up' : 'down'} /> : null}
        {label}
      </th>
      {isIndividualTimeReading && !hideSurfAndWind && (
        <ForecastTableHeight
          minHeight={surfValues.min}
          maxHeight={surfValues.max}
          waveHeightUnits={getWaveUnits(units.waveHeight)}
        />
      )}
      {showCustomAlternateSwellMetric && (
        <ForecastTableHeight
          waveHeight={customTableData.height}
          waveHeightUnits={getWaveUnits(units.waveHeight)}
        />
      )}
      {isIndividualTimeReading && !hideSurfAndWind ? (
        <td className={timestepCellClassName({ highlightType, score: wind.optimalScore })}>
          {round(wind.speed)}
          <span className="sl-forecast-table__units">{units.windSpeed}</span>
          &nbsp;
          <span className="sl-forecast-table__direction">
            {cardinal(wind.direction)}({round(wind.direction)}
            º)
          </span>
        </td>
      ) : null}
      {isIndividualTimeReading &&
        !overrideSwellColumns &&
        swells.map((swell) => (
          <td
            key={swell.index}
            className={timestepCellClassName({
              highlightType,
              score: swell.optimalScore || null,
              swellHeight: swell.height,
              units,
              swellHeightClasses,
            })}
          >
            {round(swell.height, 1) > 0 ? (
              <div>
                {round(swell.height, 1)}
                <span className="sl-forecast-table__units">{`${units.swellHeight} `}</span>
                {swell.period}
                s&nbsp;
                <span className="sl-forecast-table__direction">
                  {cardinal(swell.direction)}({round(swell.direction)}
                  º)
                </span>
              </div>
            ) : (
              '-'
            )}
          </td>
        ))}
      {isIndividualTimeReading &&
        overrideSwellColumns &&
        customColumns.map(({ metricDataKey }) => (
          <td key={metricDataKey} className={customTimestepCellClassName(metricDataKey === 'wind')}>
            {customMetrics[metricDataKey] && metricDataKey !== 'wind' && (
              <>
                {customMetrics[metricDataKey]}
                <span className="sl-forecast-table__units">{getUnits(units, metricDataKey)}</span>
              </>
            )}
            {customMetrics[metricDataKey] && metricDataKey === 'wind' && (
              <div className="sl-forecast-table__custom-wind">
                <WindArrow
                  degrees={customMetrics[metricDataKey].direction}
                  speed={customMetrics[metricDataKey].speed}
                />
                <div className="sl-forecast-table__wind-reading-text">
                  <WindStats wind={customMetrics[metricDataKey]} units={units} />
                </div>
              </div>
            )}
            {!customMetrics[metricDataKey] && '-'}
          </td>
        ))}
    </tr>
  );
};

ForecastTableTimestep.propTypes = {
  units: unitsPropType.isRequired,
  label: PropTypes.string.isRequired,
  wave: waveTimestepPropType,
  wind: windTimestepPropType,
  customTableData: buoyReportTimestepPropType,
  isCustomTable: PropTypes.bool,
  customColumns: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string,
      display: PropTypes.string,
      metricDataKey: PropTypes.string,
    }),
  ),
  overrideSwellColumns: PropTypes.bool,
  isDay: PropTypes.bool,
  expanded: PropTypes.bool,
  hideSurfAndWind: PropTypes.bool,
  onClick: PropTypes.func,
  highlightType: highlightTypePropType,
};

ForecastTableTimestep.defaultProps = {
  isDay: false,
  expanded: false,
  hideSurfAndWind: false,
  onClick: null,
  wind: null,
  wave: null,
  customTableData: {},
  isCustomTable: false,
  customColumns: [],
  overrideSwellColumns: false,
  highlightType: null,
};

export default ForecastTableTimestep;
