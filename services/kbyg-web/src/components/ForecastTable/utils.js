import { swellImpact } from '@surfline/web-common';

export const sortSwellsByImpact = (swells) =>
  [...swells].sort((prevSwell, nextSwell) => swellImpact(nextSwell) - swellImpact(prevSwell));

export function getTableSwells(wave, isCustomTable, customTableData) {
  if (isCustomTable) {
    // If we use the customTableData we want to desctructure to avoid modifying the reference with `.push`
    // If the customTableData comes for @redux/toolkit api hooks it is also frozen due to the caching  and
    // internal logic, so we need to spread it to be able to modify it
    const customSwells = customTableData?.swells ? [...customTableData.swells] : [];

    if (customSwells.length < 6) {
      for (let i = customSwells.length; i < 6; i += 1) {
        customSwells.push({ height: 0, index: i });
      }
    }

    return customSwells;
  }
  return sortSwellsByImpact(wave.swells);
}

export function getCustomMetrics(overrideSwellColumns, customTableData) {
  if (overrideSwellColumns) {
    return customTableData.metrics;
  }

  return null;
}

export function isCustomMetric(columns, type) {
  const customColumnsList = columns.map((column) => column.type);

  if (customColumnsList.includes(type)) {
    return true;
  }
  return false;
}

export function getColumns({ hideSurfAndWind, customColumns, overrideSwellColumns }) {
  const columns = [{ display: customColumns ? '' : 'Day', type: 'day' }];

  if (!hideSurfAndWind) {
    columns.push.apply(columns, [
      { display: 'Surf', type: 'surf' },
      { display: 'Wind', type: 'wind' },
    ]);
  }

  if (customColumns) {
    columns.push.apply(columns, [...customColumns]);

    if (overrideSwellColumns) {
      return columns;
    }
  }

  columns.push.apply(columns, [
    { display: 'Swell 1', type: 'swell' },
    { display: 'Swell 2', type: 'swell' },
    { display: 'Swell 3', type: 'swell' },
    { display: 'Swell 4', type: 'swell' },
    { display: 'Swell 5', type: 'swell' },
    { display: 'Swell 6', type: 'swell' },
  ]);

  return columns;
}
