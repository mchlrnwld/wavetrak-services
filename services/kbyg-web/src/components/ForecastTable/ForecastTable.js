import PropTypes from 'prop-types';
import React from 'react';
import { format } from 'date-fns/fp';
import classNames from 'classnames';
import Sticky from 'react-stickynode';
import { ErrorBox, OptimalScoreLegend } from '@surfline/quiver-react';
import { getLocalDate, getLocalDateForFutureDay } from '@surfline/web-common';
import ForecastTableControls from '../ForecastTableControls';
import ForecastTableCTA from '../ForecastTableCTA';
import SwellHeightLegend from '../SwellHeightLegend';
import waveChartPropType from '../../propTypes/waveChart';
import windChartPropType from '../../propTypes/windChart';
import buoyReportPropType from '../../propTypes/buoyReport';
import highlightTypePropType from '../../propTypes/forecastTableHighlightType';
import ForecastTableDay from './ForecastTableDay';
import config from '../../config';
import { getColumns, isCustomMetric } from './utils';

const className = (hideSurfAndWind, isCustomTable, overrideSwellColumns) =>
  classNames({
    'sl-forecast-table': true,
    'sl-forecast-table--hide-surf-and-wind': hideSurfAndWind && !isCustomTable,
    'sl-forecast-table--custom': isCustomTable,
    'sl-forecast-table--custom-columns': overrideSwellColumns,
  });

const tableHeaderClassName = (type, isCustomTable, customColumns) =>
  classNames({
    'sl-forecast-table__cell': true,
    'sl-forecast-table__cell--header': true,
    'sl-forecast-table__cell--wind-stats': type === 'wind' && isCustomTable,
    [`sl-forecast-table__cell--${type}`]: !(isCustomTable && type === 'wind'),
    [`sl-forecast-table__cell--custom-metric`]: isCustomMetric(customColumns, type),
  });

/**
 * @template D
 * @typedef {object} CustomTableData
 * @property {import('../../propTypes/units')} units
 * @property {number} utcOffset
 * @property {number} [showMinutes]
 * @property {Array<Array<D & { timestamp: number, utcOffset: number}>>} days
 */

/**
 * @typedef {Object} Props
 * @property {CustomTableData} [customTableData]
 * @property {boolean} [isCustomTable]
 * @property {Array<{display: string, type: string, metricDataKey?: string}>} [customColumns]
 * @property {boolean} [overrideSwellColumns]
 * @property {Array<{ expanded: boolean, blurred: boolean }>} days
 * @property {boolean} [hideSurfAndWind]
 * @property {number} utcOffset
 * @property {(day: number) => void} toggleDayExpanded
 * @property {(expanded: boolean) => void} expandAllDays
 * @property {number} [deviceWidth]
 * @property {string} [dateFormat]
 */

/** @type {React.FC<Props>} */
const ForecastTable = ({
  days,
  wave,
  wind,
  customTableData,
  isCustomTable,
  customColumns,
  overrideSwellColumns,
  utcOffset,
  showCTA,
  category,
  pageName,
  spotId,
  subregionId,
  toggleDayExpanded,
  expandAllDays,
  hideSurfAndWind,
  highlightType,
  deviceWidth,
  dateFormat,
}) => {
  const columns = getColumns({
    hideSurfAndWind,
    customColumns,
    overrideSwellColumns,
  });
  const mobileView = deviceWidth < config.tabletLargeWidth;
  const dayDateFormat = mobileView ? 'iii' : 'iiii';
  const error = wave?.error || wind?.error;

  const formatString = dateFormat === 'MDY' ? 'M/d' : 'd/M';
  const getDayString = format(`${dayDateFormat} ${formatString}`);

  if (wind && wave && (wave.loading || wind.loading)) return null;
  if (isCustomTable && !customTableData) return null;

  return (
    <div className={className(hideSurfAndWind, isCustomTable, overrideSwellColumns)}>
      <div className="sl-forecast-table__subheader">
        <ForecastTableControls expandAllDays={expandAllDays} />
        {highlightType === 'OPTIMAL' ? <OptimalScoreLegend /> : null}
        {highlightType === 'SWELL_HEIGHT' ? (
          <SwellHeightLegend units={wave.units.swellHeight} />
        ) : null}
      </div>
      <div className="sl-forecast-table__container">
        <div className="sl-forecast-table__table-wrapper">
          <table className="sl-forecast-table__table">
            <thead>
              <tr>
                {columns.map(({ display, type }) => (
                  <th
                    key={display}
                    className={tableHeaderClassName(type, isCustomTable, customColumns)}
                    scope="col"
                  >
                    {mobileView ? display : <Sticky>{display}</Sticky>}
                  </th>
                ))}
              </tr>
            </thead>
            {error ? (
              <tbody className="sl-forecast-table__error">
                <tr>
                  <th colSpan={columns.length}>
                    <ErrorBox type="data" />
                  </th>
                </tr>
              </tbody>
            ) : (
              days.map(({ blurred, expanded }, i) => {
                const day = getDayString(
                  isCustomTable
                    ? getLocalDate(
                        customTableData.days[i][0].timestamp,
                        customTableData.days[i][0].utcOffset,
                      )
                    : getLocalDateForFutureDay(i, utcOffset),
                );
                return (
                  <ForecastTableDay
                    key={day}
                    units={
                      !isCustomTable
                        ? {
                            swellHeight: wave.units.swellHeight,
                            waveHeight: wave.units.waveHeight,
                            windSpeed: wind.units.windSpeed,
                          }
                        : customTableData.units
                    }
                    day={day}
                    isCustomTable={isCustomTable}
                    customTableDataDay={customTableData?.days[i]}
                    customColumns={customColumns}
                    overrideSwellColumns={overrideSwellColumns}
                    waveDay={wave?.days[i]}
                    windDay={wind?.days[i]}
                    blurred={blurred}
                    expanded={expanded}
                    hideSurfAndWind={hideSurfAndWind}
                    onClickDay={() => toggleDayExpanded(i)}
                    highlightType={highlightType}
                    showMinutes={customTableData?.showMinutes}
                  />
                );
              })
            )}
          </table>
        </div>
      </div>
      {showCTA ? (
        <ForecastTableCTA
          category={category}
          pageName={pageName}
          spotId={spotId}
          subregionId={subregionId}
        />
      ) : null}
    </div>
  );
};

ForecastTable.propTypes = {
  days: PropTypes.arrayOf(
    PropTypes.shape({
      blurred: PropTypes.bool,
      expanded: PropTypes.bool,
    }),
  ).isRequired,
  wave: waveChartPropType,
  wind: windChartPropType,
  customTableData: buoyReportPropType,
  isCustomTable: PropTypes.bool,
  customColumns: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string,
      display: PropTypes.string,
      metricDataKey: PropTypes.string,
    }),
  ),
  overrideSwellColumns: PropTypes.bool,
  utcOffset: PropTypes.number.isRequired,
  category: PropTypes.string,
  pageName: PropTypes.string,
  spotId: PropTypes.string,
  subregionId: PropTypes.string,
  showCTA: PropTypes.bool,
  toggleDayExpanded: PropTypes.func.isRequired,
  expandAllDays: PropTypes.func.isRequired,
  hideSurfAndWind: PropTypes.bool,
  highlightType: highlightTypePropType,
  deviceWidth: PropTypes.number.isRequired,
  dateFormat: PropTypes.oneOf(['MDY', 'DMY']),
};

ForecastTable.defaultProps = {
  subregionId: null,
  showCTA: false,
  hideSurfAndWind: false,
  dateFormat: 'MDY',
  wave: null,
  wind: null,
  customTableData: null,
  isCustomTable: false,
  customColumns: [],
  overrideSwellColumns: false,
  category: '',
  pageName: '',
  spotId: '',
  highlightType: null,
};

export default ForecastTable;
