import React from 'react';
import { FEET, METERS, getWaveUnits } from '@surfline/web-common';
import PropTypes from 'prop-types';

/**
 * @type {React.FunctionComponent<Props>}
 * @typedef {object} Props
 * @param {number} [Props.waveHeight]
 * @param {number} [Props.minHeight]
 * @param {number} [Props.maxHeight]
 * @param {FEET | METERS} waveHeightUnits
 */
const ForecastTableHeight = ({ waveHeight, minHeight, maxHeight, waveHeightUnits }) => {
  const height = waveHeight || `${minHeight}-${maxHeight}`;

  return (
    <td className="sl-forecast-table__cell">
      {height}
      <span className="sl-forecast-table__units">{getWaveUnits(waveHeightUnits)}</span>
    </td>
  );
};

ForecastTableHeight.propTypes = {
  waveHeight: PropTypes.number,
  minHeight: PropTypes.number,
  maxHeight: PropTypes.number,
  waveHeightUnits: PropTypes.oneOf([FEET, METERS]).isRequired,
};

ForecastTableHeight.defaultProps = {
  waveHeight: null,
  minHeight: null,
  maxHeight: null,
};

export default ForecastTableHeight;
