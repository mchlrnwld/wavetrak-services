import React from 'react';
import { expect } from 'chai';
import { mount } from 'enzyme';
import { FEET, METERS } from '@surfline/web-common';
import ForecastTableHeight from './ForecastTableHeight';

describe('components / ForecastTableHeight', () => {
  const props = {
    waveHeight: 3.1,
    waveHeightUnits: FEET,
  };

  const propsWithMinAndMax = {
    minHeight: 1,
    maxHeight: 3,
    waveHeightUnits: METERS,
  };

  it('renders rounded wave height', () => {
    const wrapper = mount(
      <table>
        <tbody>
          <tr>
            <ForecastTableHeight {...props} />
          </tr>
        </tbody>
      </table>,
    );

    const waveHeight = wrapper.find('.sl-forecast-table__cell');
    const waveHeightUnits = wrapper.find('.sl-forecast-table__units');

    expect(waveHeight.text()).to.contain('3');
    expect(waveHeightUnits.text()).to.contain('FT');
  });

  it('renders min and max wave height', () => {
    const wrapper = mount(
      <table>
        <tbody>
          <tr>
            <ForecastTableHeight {...propsWithMinAndMax} />
          </tr>
        </tbody>
      </table>,
    );

    const waveHeight = wrapper.find('.sl-forecast-table__cell');
    const waveHeightUnits = wrapper.find('.sl-forecast-table__units');

    expect(waveHeight.text()).to.contain('1-3');
    expect(waveHeightUnits.text()).to.contain('M');
  });
});
