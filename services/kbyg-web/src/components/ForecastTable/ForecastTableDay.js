import PropTypes from 'prop-types';
import React, { useMemo } from 'react';
import classNames from 'classnames';
import { getLocalDate } from '@surfline/web-common';
import { format } from 'date-fns/fp';
import unitsPropType from '../../propTypes/units';
import waveTimestepPropType from '../../propTypes/waveTimestep';
import windTimestepPropType from '../../propTypes/windTimestep';
import highlightTypePropType from '../../propTypes/forecastTableHighlightType';
import buoyReportTimestepPropType from '../../propTypes/buoyReportTimestep';
import ForecastTableTimestep from './ForecastTableTimestep';

const dayClassName = (blurred) =>
  classNames({
    'sl-forecast-table__day': true,
    'sl-forecast-table__day--blurred': blurred,
  });

/**
 * @param {object} Props
 * @param {Partial<import('../../types/units').Units>} Props.units
 * @param {string} Props.day
 * @param {import('../../propTypes/wave').Wave} Props.waveDay
 * @param {import('../../propTypes/windTimestep').Wind[]} Props.windDay
 * @param {import('../../propTypes/buoyReportTimestep').BuoyReport[]} Props.customTableDataDay
 * @param {boolean} Props.isCustomTable
 * @param {Array<Object>} Props.customColumns
 * @param {boolean} Props.overrideSwellColumns
 * @param {boolean} Props.blurred
 * @param {boolean} Props.expanded
 * @param {boolean} Props.hideSurfAndWind
 * @param {boolean} Props.filterTop3Swells
 * @param {() => void} Props.onClickDay
 * @param {"OPTIMAL" | "SWELL_HEIGHT"} Props.highlightType
 */
const ForecastTableDay = ({
  units,
  day,
  waveDay,
  windDay,
  customTableDataDay,
  isCustomTable,
  customColumns,
  overrideSwellColumns,
  blurred,
  expanded,
  hideSurfAndWind,
  onClickDay,
  highlightType,
  showMinutes,
}) => {
  const formatTimeLabel = useMemo(() => format(showMinutes ? 'h:mmaaa' : 'haaa'), [showMinutes]);
  return (
    <tbody className={dayClassName(blurred)}>
      <ForecastTableTimestep
        key={day}
        units={units}
        label={day}
        isCustomTable={isCustomTable}
        customTableData={
          customTableDataDay ? customTableDataDay[Math.floor(customTableDataDay.length / 2)] : null
        }
        customColumns={customColumns}
        overrideSwellColumns={overrideSwellColumns}
        wave={waveDay ? waveDay[waveDay.length / 2] : null}
        wind={windDay ? windDay[windDay.length / 2] : null}
        onClick={onClickDay}
        isDay
        expanded={expanded}
        hideSurfAndWind={hideSurfAndWind}
        highlightType={highlightType}
      />
      {expanded && !isCustomTable
        ? [...Array(Math.max(waveDay.length, windDay.length))].map((_, timestep) => {
            // We have to duplicate the surf/swell data here manually since we only fetch 6 hour intervals
            const wave = waveDay[Math.floor(timestep / 2)];
            const wind = windDay[timestep];

            const label = formatTimeLabel(getLocalDate(wind.timestamp, wind.utcOffset));
            return (
              <ForecastTableTimestep
                key={label}
                units={units}
                label={label}
                wave={wave}
                wind={wind}
                hideSurfAndWind={hideSurfAndWind}
                highlightType={highlightType}
              />
            );
          })
        : null}
      {expanded && isCustomTable
        ? [...Array(customTableDataDay.length)].map((_, timestep) => {
            const customTableData = customTableDataDay[timestep];
            const customTableDataTime = customTableDataDay[timestep];
            const label = formatTimeLabel(
              getLocalDate(customTableDataTime.timestamp, customTableDataTime.utcOffset),
            );

            return (
              <ForecastTableTimestep
                key={label}
                units={units}
                label={label}
                customTableData={customTableData}
                isCustomTable
                customColumns={customColumns}
                overrideSwellColumns={overrideSwellColumns}
                hideSurfAndWind={hideSurfAndWind}
                highlightType={highlightType}
              />
            );
          })
        : null}
    </tbody>
  );
};

ForecastTableDay.propTypes = {
  units: unitsPropType.isRequired,
  day: PropTypes.string.isRequired,
  waveDay: PropTypes.arrayOf(waveTimestepPropType),
  windDay: PropTypes.arrayOf(windTimestepPropType),
  customTableDataDay: PropTypes.arrayOf(buoyReportTimestepPropType),
  isCustomTable: PropTypes.bool,
  customColumns: PropTypes.arrayOf(
    PropTypes.shape({
      type: PropTypes.string,
      display: PropTypes.string,
      metricDataKey: PropTypes.string,
    }),
  ),
  overrideSwellColumns: PropTypes.bool,
  blurred: PropTypes.bool,
  expanded: PropTypes.bool,
  hideSurfAndWind: PropTypes.bool,
  onClickDay: PropTypes.func,
  highlightType: highlightTypePropType,
  showMinutes: PropTypes.bool,
};

ForecastTableDay.defaultProps = {
  blurred: false,
  expanded: false,
  hideSurfAndWind: false,
  waveDay: null,
  windDay: null,
  customTableDataDay: null,
  isCustomTable: false,
  customColumns: [],
  overrideSwellColumns: false,
  onClickDay: null,
  highlightType: null,
  showMinutes: false,
};

export default ForecastTableDay;
