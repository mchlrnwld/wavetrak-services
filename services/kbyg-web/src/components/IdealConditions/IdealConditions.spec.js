import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import IdealConditions from './IdealConditions';

describe('components / IdealConditions', () => {
  const idealSpotConditions = {
    season: { description: 'January-December', value: ['Spring', 'Summer', 'Autumn', 'Winter'] },
    size: { description: 'waist high to a bit overhead', value: ['1-6'] },
    swellDirection: { description: 'WNW, W, SW, SSW; most South swells will miss', value: [] },
    tide: { description: 'low to medium', value: ['Low', 'Medium_Low', 'Medium'] },
    windDirection: { description: 'E, NE', value: [] },
  };

  it('renders ideal conditions', () => {
    const wrapper = shallow(
      <IdealConditions name="Swamis" idealConditions={idealSpotConditions} />,
    );

    // Title
    expect(wrapper.find('.sl-ideal-conditions h3').text()).to.equal('Ideal Swamis Surf Conditions');

    // Correct count
    expect(wrapper.find('.sl-ideal-conditions__condition').length).to.equal(4);

    const conditions = wrapper.find('.sl-ideal-conditions__condition__description p');

    // Swell Direction
    expect(conditions.at(0).text()).to.equal('WNW, W, SW, SSW; most South swells will miss');

    // Wind
    expect(conditions.at(1).text()).to.equal('E, NE');

    // Surf Height
    expect(conditions.at(2).text()).to.equal('waist high to a bit overhead');

    // Tide
    expect(conditions.at(3).text()).to.equal('low to medium');
  });
});
