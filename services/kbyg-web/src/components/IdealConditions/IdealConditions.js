import PropTypes from 'prop-types';
import React from 'react';
import { WindIcon, TideIcon } from '@surfline/quiver-react';
import SurfIcon from '../SurfIcon';
import SwellIcon from '../SwellIcon';
import spotIdealConditionsPropShape from '../../propTypes/spotIdealConditions';

const idealConditionTypes = [
  {
    title: 'Swell Direction',
    type: 'swellDirection',
    image: <SwellIcon />,
  },
  {
    title: 'Wind',
    type: 'windDirection',
    image: <WindIcon />,
  },
  {
    title: 'Surf Height',
    type: 'size',
    image: <SurfIcon />,
  },
  {
    title: 'Tide',
    type: 'tide',
    image: <TideIcon />,
  },
];

const IdealConditions = ({ name, idealConditions }) => (
  <div className="sl-ideal-conditions">
    <h3>Ideal {name} Surf Conditions</h3>
    {idealConditionTypes.map((condition) => (
      <div key={condition.type} className="sl-ideal-conditions__condition">
        {condition.image}
        <div className="sl-ideal-conditions__condition__description">
          <h5>{condition.title}</h5>
          <p>
            {idealConditions[condition.type]
              ? idealConditions[condition.type].description
              : 'Not available for this spot'}
          </p>
        </div>
      </div>
    ))}
  </div>
);

IdealConditions.defaultProps = {
  idealConditions: {
    loading: false,
  },
};

IdealConditions.propTypes = {
  name: PropTypes.string.isRequired,
  idealConditions: spotIdealConditionsPropShape,
};

export default IdealConditions;
