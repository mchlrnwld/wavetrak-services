import PropTypes from 'prop-types';
import React from 'react';
import { SpotDetails, SpotListCTAThumbnail, SpotThumbnail } from '@surfline/quiver-react';
import { slugify, kbygPaths } from '@surfline/web-common';
import Skeleton from 'react-loading-skeleton';

import spotPropType from '../../propTypes/spot';
import unitsPropType from '../../propTypes/units';
import WavetrakLink from '../WavetrakLink';

import { SL_WEB_CONDITION_VARIATION } from '../../common/treatments';
import { useSplitTreatment } from '../../selectors/split';

const { spotReportPath } = kbygPaths;
const getBackgroundImg = (spot) => (spot.cameras[0] ? spot.cameras[0].stillUrl : null);

const SpotMarkerTooltip = ({ spot, units, position, showPremiumCamCTA }) => {
  const renderSpotThumbnail = showPremiumCamCTA ? (
    <SpotListCTAThumbnail spot={spot} isTooltip />
  ) : (
    <SpotThumbnail
      windDirection={spot.wind.direction}
      windSpeed={spot.wind.speed}
      units={units}
      hasCamera={spot.cameras.length > 0}
    />
  );

  const { treatment, loading } = useSplitTreatment(SL_WEB_CONDITION_VARIATION);
  return (
    <WavetrakLink
      className={`sl-spot-marker-tooltip sl-spot-marker-tooltip--${position.vertical}${position.horizontal}`}
      href={spotReportPath(slugify(spot.name), spot._id)}
      style={{ backgroundImage: `url('${getBackgroundImg(spot)}')` }}
      title={`${spot.name} Surf Report & Forecast`}
    >
      {spot.cameras[0] ? renderSpotThumbnail : null}
      {loading ? (
        <Skeleton height={90} />
      ) : (
        <SpotDetails spot={spot} units={units} sevenRatings={treatment === 'on'} />
      )}
    </WavetrakLink>
  );
};

SpotMarkerTooltip.propTypes = {
  spot: spotPropType.isRequired,
  units: unitsPropType.isRequired,
  position: PropTypes.shape({
    vertical: PropTypes.string,
    horizontal: PropTypes.string,
  }),
  showPremiumCamCTA: PropTypes.bool,
};

SpotMarkerTooltip.defaultProps = {
  position: {
    vertical: 'top',
    horizontal: 'center',
  },
  showPremiumCamCTA: false,
};

export default SpotMarkerTooltip;
