import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { SpotDetails, SpotThumbnail } from '@surfline/quiver-react';
import sinon from 'sinon';
import SpotMarkerTooltip from './SpotMarkerTooltip';
import * as splitSelectors from '../../selectors/split';

describe('components / SpotMarkerTooltip', () => {
  const spot = {
    _id: '5842041f4e65fad6a7708827',
    name: 'HB Pier, Northside',
    lat: 33.656781041213,
    lon: -118.0064678192,
    waveHeight: {
      min: 2,
      max: 3,
    },
    wind: {
      speed: 1,
      direction: 0,
    },
    conditions: {
      human: true,
      value: 'VERY_GOOD',
    },
    cameras: [
      {
        _id: '5842041f4e65fad6a7708827',
        stillUrl: 'someFakeStillUrl',
      },
    ],
  };
  const units = {
    tideHeight: 'M',
    windSpeed: 'KPH',
    waveHeight: 'M',
  };

  beforeEach(() => {
    sinon.stub(splitSelectors, 'useSplitTreatment').returns({ loading: false, treatment: null });
  });

  afterEach(() => {
    splitSelectors.useSplitTreatment.restore();
  });

  it('adds modified classname based on position props', () => {
    const wrapper = shallow(
      <SpotMarkerTooltip
        units={units}
        spot={spot}
        position={{ vertical: 'top', horizontal: 'left' }}
      />,
    );
    expect(wrapper.prop('className')).to.contain('sl-spot-marker-tooltip--topleft');
    const defaultPosition = shallow(<SpotMarkerTooltip units={units} spot={spot} />);
    expect(defaultPosition.prop('className')).to.contain('sl-spot-marker-tooltip--topcenter');
  });

  it('renders and passes props into children', () => {
    const wrapper = shallow(
      <SpotMarkerTooltip
        units={units}
        spot={spot}
        position={{ vertical: 'top', horizontal: 'left' }}
      />,
    );
    const spotThumbnail = wrapper.find(SpotThumbnail);
    const spotDetails = wrapper.find(SpotDetails);

    expect(spotThumbnail).to.have.length(1);
    expect(spotThumbnail.prop('windDirection')).to.deep.equal(spot.wind.direction);
    expect(spotThumbnail.prop('windSpeed')).to.deep.equal(spot.wind.speed);
    expect(spotThumbnail.prop('units')).to.deep.equal(units);
    expect(spotThumbnail.prop('hasCamera')).to.be.true();

    expect(spotDetails).to.have.length(1);
    expect(spotDetails.prop('spot')).to.deep.equal(spot);
    expect(spotDetails.prop('units')).to.deep.equal(units);
  });

  it('renders only spotDetails if the spot has no camera', () => {
    const noCamSpot = {
      ...spot,
      cameras: [],
    };
    const wrapper = shallow(
      <SpotMarkerTooltip
        units={units}
        spot={noCamSpot}
        position={{ vertical: 'top', horizontal: 'left' }}
      />,
    );
    const spotThumbnail = wrapper.find(SpotThumbnail);
    const spotDetails = wrapper.find(SpotDetails);

    expect(spotThumbnail).to.have.length(0);
    expect(spotDetails).to.have.length(1);
    expect(spotDetails.prop('spot')).to.deep.equal(noCamSpot);
    expect(spotDetails.prop('units')).to.deep.equal(units);
  });
});
