import PropTypes from 'prop-types';
import React from 'react';
import MapButton from '../MapButton';
import BuoysIcon from '../BuoysIcon';

const MapBuoysButton = ({ onClick }) => (
  <div className="sl-map-buoys-button">
    <MapButton onClick={onClick}>
      <BuoysIcon />
      <div className="sl-map-buoys-button__text">Buoys</div>
    </MapButton>
  </div>
);

MapBuoysButton.propTypes = {
  onClick: PropTypes.func,
};

MapBuoysButton.defaultProps = {
  onClick: () => null,
};

export default MapBuoysButton;
