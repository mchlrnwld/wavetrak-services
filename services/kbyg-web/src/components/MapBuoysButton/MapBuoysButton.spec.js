import React from 'react';
import { expect } from 'chai';
import sinon from 'sinon';
import { shallow } from 'enzyme';
import MapButton from '../MapButton';
import BuoysIcon from '../BuoysIcon';
import MapBuoysButton from './MapBuoysButton';

describe('components / MapBuoysButton', () => {
  it('renders MapButton with BuoysIcon and Buoys text', () => {
    const onClick = sinon.spy();
    const wrapper = shallow(<MapBuoysButton onClick={onClick} />);

    const mapButton = wrapper.find(MapButton);
    expect(mapButton).to.have.length(1);

    mapButton.simulate('click');
    expect(onClick).to.have.been.calledOnce();

    const buoysIcon = mapButton.find(BuoysIcon);
    expect(buoysIcon).to.have.length(1);

    const text = mapButton.find('.sl-map-buoys-button__text');
    expect(text).to.have.length(1);
    expect(text.text()).to.equal('Buoys');
  });
});
