import React from 'react';
import { expect } from 'chai';

import sinon, { SinonStub } from 'sinon';
import { mount } from 'enzyme';
import { CustomMarker, Map } from '@surfline/quiver-react';
import type { Map as TMap, LeafletMouseEvent } from 'leaflet';

import { act } from 'react-dom/test-utils';
import BuoyClusterMarker from './BuoyClusterMarker';

import * as mapActions from '../../actions/mapV2';
import { mountWithReduxAndRouter, wait } from '../../utils/test-utils';
import { Cluster } from '../../utils/cluster/cluster';

describe('components / BuoyClusterMarker', () => {
  let zoomInStub: SinonStub;

  let map: TMap;

  const cluster: Cluster<{ count: number }> = {
    geometry: { type: 'Point', coordinates: [1, 2] },
    properties: { count: 10 },
    type: 'Feature',
  };

  const Comp = () => (
    <Map
      center={{ lat: 0, lon: 0 }}
      mapProps={{
        // eslint-disable-next-line
        whenCreated: (m) => (map = m),
      }}
    >
      <BuoyClusterMarker cluster={cluster} />
    </Map>
  );

  beforeEach(() => {
    zoomInStub = sinon.stub(mapActions, 'zoomIn').returns({ type: 'ZOOM_IN', payload: undefined });
  });

  afterEach(() => {
    zoomInStub.restore();
  });

  it('should render the cluster marker into the map', async () => {
    const { wrapper } = mountWithReduxAndRouter(Comp);
    try {
      // Wait for the map to setup
      await act(async () => {
        await wait(150);
        wrapper.setProps({ update: 1 });
        wrapper.update();
      });

      expect(map).to.exist("The map instance must exist or else the markers can't render");

      const marker = wrapper.find(CustomMarker);
      expect(marker).to.have.length(1);
      expect(marker.prop('position')).to.deep.equal({ lon: 1, lat: 2 });

      const Icon = marker.prop('icon');
      expect(mount(Icon as React.ReactElement<any>).text()).to.equal(`${cluster.properties.count}`);
    } finally {
      act(() => {
        wrapper.unmount();
      });
    }
  });

  it('should zoom into the marker position when clicked', async () => {
    const center = { lon: 1, lat: 2 };
    const { wrapper } = mountWithReduxAndRouter(Comp);
    try {
      // Wait for the map to setup
      await act(async () => {
        await wait(150);
        wrapper.setProps({ update: 1 });
        wrapper.update();
      });

      expect(map).to.exist("The map instance must exist or else the markers can't render");

      const marker = wrapper.find(CustomMarker);
      expect(marker).to.have.length(1);
      marker.prop('eventHandlers').click!({} as LeafletMouseEvent);

      expect(zoomInStub).to.have.been.calledOnceWithExactly(center);
    } finally {
      act(() => {
        wrapper.unmount();
      });
    }
  });
});
