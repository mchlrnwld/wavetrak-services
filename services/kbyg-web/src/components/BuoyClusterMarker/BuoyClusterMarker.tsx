import React, { useCallback, useMemo } from 'react';
import { CustomMarker } from '@surfline/quiver-react';

import type { Cluster } from '../../utils/cluster/cluster';
import { zoomIn } from '../../actions/mapV2';
import { useAppDispatch } from '../../stores/hooks';

import styles from './BuoyClusterMarker.module.scss';

interface Props {
  cluster: Cluster<{ count: number }>;
}

const BuoyClusterMarker: React.FC<Props> = ({ cluster }) => {
  const dispatch = useAppDispatch();
  const doZoomIn = useCallback((center) => dispatch(zoomIn(center)), [dispatch]);

  const position = useMemo(
    () => ({ lat: cluster.geometry.coordinates[1], lon: cluster.geometry.coordinates[0] }),
    [cluster.geometry.coordinates],
  );

  const marker = useMemo(
    () => (
      <svg width="54px" height="54px">
        <g>
          <circle cx="27" cy="27" r="24" />
        </g>
        <text x="27" y="28.5">
          {cluster.properties.count}
        </text>
      </svg>
    ),
    [cluster.properties.count],
  );

  return (
    <CustomMarker
      // Incorrect types in quiver-react
      // @ts-ignore
      position={position}
      icon={marker}
      className={styles.buoyClusterMarker}
      iconSize={[54, 54]}
      eventHandlers={{ click: () => doZoomIn(position) }}
    />
  );
};

export default BuoyClusterMarker;
