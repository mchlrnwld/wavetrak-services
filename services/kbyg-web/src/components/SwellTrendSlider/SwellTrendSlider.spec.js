import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { mount } from 'enzyme';
import SwellTrendSlider from './SwellTrendSlider';
import daysAndRanges from '../../containers/ForecastDataVisualizer/daysAndRanges';

describe('components / SwellTrendSlider', () => {
  const onSelectRangeSpy = sinon.spy();

  it('renders the 16 day swell trend slider', () => {
    const props = {
      daysAndRanges,
      daysEnabled: 16,
      deviceWidth: 1237,
      isHourly: false,
      isPremium: true,
      isSlider: false,
      isTabbing: false,
      onSelectRange: onSelectRangeSpy,
      selectedStartDay: 0,
      spotId: '5842041f4e65fad6a77088ed',
      type: 'SPOT',
      utcOffset: -8,
    };

    const { days, ranges, daysPerRange } = daysAndRanges(
      props.isPremium,
      true,
      props.deviceWidth,
      props.selectedStartDay,
      props.isTabbing,
      props.isSlider,
      false,
      props.selectedStartDay,
      props.isHourly,
    );
    const wrapper = mount(
      <SwellTrendSlider {...props} ranges={ranges} days={days} daysPerRange={daysPerRange} />,
    );

    const SwellSliderDays = wrapper.find('.sl-swell-trend-slider__shield-area__day-range__range');
    expect(SwellSliderDays).to.have.length(16);
    expect(SwellSliderDays.findWhere((day) => day.prop('disabled') === true)).to.have.length(
      daysPerRange,
    );
  });

  it('renders the 16 day swell trend slider with a paywall', () => {
    const props = {
      daysAndRanges,
      daysEnabled: 2,
      deviceWidth: 1237,
      isHourly: false,
      isPremium: false,
      isSlider: false,
      isTabbing: false,
      onSelectRange: onSelectRangeSpy,
      selectedStartDay: 0,
      spotId: '5842041f4e65fad6a77088ed',
      type: 'SPOT',
      utcOffset: -8,
    };

    const { days, ranges, daysPerRange } = daysAndRanges(
      props.isPremium,
      true,
      props.deviceWidth,
      props.selectedStartDay,
      props.isTabbing,
      props.isSlider,
      false,
      props.selectedStartDay,
      props.isHourly,
    );

    const wrapper = mount(
      <SwellTrendSlider {...props} ranges={ranges} days={days} daysPerRange={daysPerRange} />,
    );

    const Tooltip = wrapper.find('TooltipProvider');
    Tooltip.setState({ tooltip: { xPercentage: 1 } });

    const SliderTooltip = wrapper.find('.sl-swell-trend-slider__tooltip');

    expect(SliderTooltip).to.have.length(1);
    expect(SliderTooltip.text()).to.be.equal('Premium members get the detailed 16-day forecast.');

    const SwellSliderDays = wrapper.find('.sl-swell-trend-slider__shield-area__day-range__range');
    expect(SwellSliderDays).to.have.length(16);

    expect(SwellSliderDays.findWhere((day) => day.prop('disabled') === false)).to.have.length(0);
  });

  it('renders the hourly view swell trend slider', () => {
    const props = {
      daysAndRanges,
      daysEnabled: 5,
      deviceWidth: 1237,
      isHourly: true,
      isPremium: true,
      isSlider: false,
      isTabbing: false,
      onSelectRange: onSelectRangeSpy,
      selectedStartDay: 0,
      spotId: '5842041f4e65fad6a77088ed',
      type: 'SPOT',
      utcOffset: -8,
    };

    const { days, ranges, daysPerRange } = daysAndRanges(
      props.isPremium,
      true,
      props.deviceWidth,
      props.selectedStartDay,
      props.isTabbing,
      props.isSlider,
      false,
      props.selectedStartDay,
      props.isHourly,
    );

    const wrapper = mount(
      <SwellTrendSlider {...props} ranges={ranges} days={days} daysPerRange={daysPerRange} />,
    );

    const Tooltip = wrapper.find('TooltipProvider');
    Tooltip.setState({ tooltip: { xPercentage: 1 } });

    const SliderTooltip = wrapper.find('.sl-swell-trend-slider__tooltip');
    expect(SliderTooltip).to.have.length(1);
    expect(SliderTooltip.text()).to.be.equal(
      'The 1-day hourly forecast is only available up to 5 days.',
    );

    const SwellSliderDays = wrapper.find('.sl-swell-trend-slider__shield-area__day-range__range');
    expect(SwellSliderDays).to.have.length(16);

    expect(SwellSliderDays.findWhere((day) => day.prop('disabled') === false)).to.have.length(4);
  });

  it('renders the hourly view swell trend slider with a paywall', () => {
    const props = {
      daysAndRanges,
      daysEnabled: 2,
      deviceWidth: 1237,
      isHourly: true,
      isPremium: false,
      isSlider: false,
      isTabbing: false,
      onSelectRange: onSelectRangeSpy,
      selectedStartDay: 0,
      spotId: '5842041f4e65fad6a77088ed',
      type: 'SPOT',
      utcOffset: -8,
    };

    const { days, ranges, daysPerRange } = daysAndRanges(
      props.isPremium,
      true,
      props.deviceWidth,
      props.selectedStartDay,
      props.isTabbing,
      props.isSlider,
      false,
      props.selectedStartDay,
      props.isHourly,
    );

    const wrapper = mount(
      <SwellTrendSlider {...props} ranges={ranges} days={days} daysPerRange={daysPerRange} />,
    );

    const Tooltip = wrapper.find('TooltipProvider');
    Tooltip.setState({ tooltip: { xPercentage: 1 } });

    const SliderTooltip = wrapper.find('.sl-swell-trend-slider__tooltip');
    expect(SliderTooltip).to.have.length(1);
    expect(SliderTooltip.text()).to.be.equal('Premium Members get five days of hourly wind.');

    const SwellSliderDays = wrapper.find('.sl-swell-trend-slider__shield-area__day-range__range');
    expect(SwellSliderDays).to.have.length(16);

    expect(SwellSliderDays.findWhere((day) => day.prop('disabled') === false)).to.have.length(1);
  });
});
