import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Draggable from 'react-draggable';
import { TooltipProvider } from '@surfline/quiver-react';
import { getLocalDateForFutureDay } from '@surfline/web-common';
import waveMaxHeightTimestepPropType from '../../propTypes/waveMaxHeightTimestep';
import SwellTrendSliderDay from '../SwellTrendSliderDay';
import SwellTrend from '../SwellTrend';
import en from '../../intl/translations/en';

const DAY_WIDTH = 42.5;

const rangeClassName = (active, hover, isDragging, isOverShield, isPremium, isHourly) =>
  classNames({
    'sl-swell-trend-slider__shield-area__day-range__range': true,
    'sl-swell-trend-slider__shield-area__day-range__range--active':
      (isPremium || isHourly) && active,
    'sl-swell-trend-slider__shield-area__day-range__range--hover':
      (isPremium || isHourly) && !isDragging && !isOverShield && hover,
  });

class SwellTrendSlider extends Component {
  constructor(props) {
    super(props);
    const { days } = this.props;
    this.state = {
      chartRangeSelectorRef: null,
      hoverDays: [],
      controlledPosition: {
        x: days[0] * DAY_WIDTH,
        y: 0,
      },
      isDragging: false,
      isOverShield: false,
    };
  }

  componentDidUpdate(prevProps) {
    const { days, daysPerRange, daysEnabled, selectedStartDay } = this.props;
    if (days[0] !== prevProps.days[0] || selectedStartDay !== prevProps.selectedStartDay) {
      const shieldStart = days[0] * DAY_WIDTH;
      const shieldEnd = shieldStart / DAY_WIDTH + daysPerRange;
      const x = shieldEnd >= daysEnabled ? (daysEnabled - daysPerRange) * DAY_WIDTH : shieldStart;
      // eslint-disable-next-line
      this.setState({
        controlledPosition: {
          x,
          y: 0,
        },
      });
    }
  }

  onClickRange = (innerRange, isSlider, sliderRangeStart) => {
    const { onSelectRange, daysPerRange, days, daysEnabled } = this.props;
    const activeRangeMax = days.splice(-1);
    const x =
      daysPerRange + innerRange.start > daysEnabled || activeRangeMax < innerRange.end
        ? (innerRange.start - daysPerRange + 1) * DAY_WIDTH
        : innerRange.start * DAY_WIDTH;
    this.setState({
      controlledPosition: {
        x,
        y: 0,
      },
    });
    onSelectRange(innerRange, isSlider, sliderRangeStart, true);
  };

  onMouseOnShield = () => {
    this.setState({ isOverShield: true });
  };

  onMouseOffShield = () => {
    this.setState({ isOverShield: false });
  };

  setChartRangeSelectorRef = (el) => {
    this.setState({
      chartRangeSelectorRef: el,
    });
  };

  handleStart = () => {
    this.setState({ isDragging: true });
  };

  handleDrag = (e, ui) => {
    const { controlledPosition } = this.state;
    const { x, y } = controlledPosition;
    this.setState({
      controlledPosition: {
        x: x + ui.deltaX,
        y: y + ui.deltaY,
      },
    });
  };

  handleStop = (sliderRangeStart) => {
    const { controlledPosition } = this.state;
    const { x } = controlledPosition;
    const { onSelectRange, daysPerRange } = this.props;
    const shieldStart = Math.round(x / DAY_WIDTH);
    const shieldEnd = shieldStart + daysPerRange - 1;
    const range = {
      start: shieldStart,
      end: shieldEnd,
    };
    onSelectRange(range, true, sliderRangeStart);
    this.setState({ isDragging: false });
  };

  hoverDays = (start) => {
    const { daysAndRanges, isPremium, deviceWidth, selectedStartDay, isHourly } = this.props;
    const daysAndRangesObj = daysAndRanges(
      isPremium,
      true,
      deviceWidth,
      start,
      false,
      true,
      true,
      selectedStartDay,
      isHourly,
    );
    this.setState({
      hoverDays: daysAndRangesObj.days,
    });
  };

  resetHoverDays = () => {
    this.setState({
      hoverDays: [],
    });
  };

  sliderRangeSelector = () => {
    const {
      ranges,
      selectedStartDay,
      daysEnabled,
      waveMaxHeightsDays,
      utcOffset,
      isPremium,
      isTabbing,
      isSlider,
      daysPerRange,
      days,
      isHourly,
      dateFormat,
    } = this.props;

    const { controlledPosition, isDragging, isOverShield, chartRangeSelectorRef } = this.state;
    const draggableBounds = {
      left: 0,
      right: (daysEnabled - daysPerRange) * DAY_WIDTH,
      top: 0,
      bottom: 0,
    };
    return (
      <div className="sl-swell-trend-slider__shield-area">
        <div
          className="sl-swell-trend-slider__shield-area__day-range"
          ref={this.setChartRangeSelectorRef}
        >
          {waveMaxHeightsDays && chartRangeSelectorRef && (
            <SwellTrend
              data={waveMaxHeightsDays}
              parentRef={chartRangeSelectorRef}
              width={680}
              height={DAY_WIDTH}
            />
          )}
          <div>
            {ranges.map((range) => {
              const sliderRanges = [...Array(Math.ceil(range.end + 1 - range.start))].map(
                (_, i) => ({
                  start: range.start + i,
                  end: range.start + i + daysPerRange,
                }),
              );
              return sliderRanges.map((innerRange) => {
                const { hoverDays } = this.state;
                const { start, end } = innerRange;
                const localDate = getLocalDateForFutureDay(start, utcOffset);
                let isActive = false;
                if (isTabbing || isSlider) {
                  isActive = days.indexOf(start) !== -1;
                } else {
                  isActive =
                    (start <= selectedStartDay && selectedStartDay <= end) ||
                    start < selectedStartDay + daysPerRange;
                }

                const maxHourlyHoverDay = isPremium ? 5 : 2;
                const isHover = isHourly
                  ? start < maxHourlyHoverDay && hoverDays.indexOf(start) !== -1
                  : hoverDays.indexOf(start) !== -1;

                const allowMouseOver = !isActive && !isDragging && (isPremium || isHourly);
                const allowMouseOut = !isActive && (isPremium || isHourly);
                return (
                  <button
                    key={start}
                    className={rangeClassName(
                      isActive,
                      isHover,
                      isDragging,
                      isOverShield,
                      isPremium,
                      isHourly,
                    )}
                    onClick={() =>
                      !isActive &&
                      this.onClickRange(
                        {
                          start: hoverDays[0],
                          end: hoverDays.splice(-1)[0],
                        },
                        true,
                        hoverDays[0],
                      )
                    }
                    type="button"
                    onMouseOver={() => allowMouseOver && this.hoverDays(start)}
                    onFocus={() => allowMouseOver && this.hoverDays(start)}
                    onMouseOut={() => allowMouseOut && this.resetHoverDays()}
                    onBlur={() => allowMouseOut && this.resetHoverDays()}
                    disabled={start >= daysEnabled || isActive}
                  >
                    <SwellTrendSliderDay
                      localDate={localDate}
                      count={start}
                      obscured={start >= daysEnabled}
                      dateFormat={dateFormat}
                    />
                  </button>
                );
              });
            })}
          </div>
        </div>
        {(isPremium || isHourly) && (
          <Draggable
            axis="x"
            handle=".sl-swell-trend-slider__shield-area__day-range__shield"
            bounds={draggableBounds}
            position={controlledPosition}
            grid={[DAY_WIDTH, DAY_WIDTH]}
            onStart={this.handleStart}
            onDrag={this.handleDrag}
            onStop={() => this.handleStop(controlledPosition.x / DAY_WIDTH)}
          >
            <div
              className="sl-swell-trend-slider__shield-area__day-range__shield"
              style={{
                width: DAY_WIDTH * daysPerRange,
              }}
              onMouseOver={this.onMouseOnShield}
              onFocus={this.onMouseOnShield}
              onMouseOut={this.onMouseOffShield}
              onBlur={this.onMouseOffShield}
            >
              <div
                className="sl-swell-trend-slider__shield-area__day-range__shield__grab-bar"
                style={{
                  width: DAY_WIDTH * daysPerRange + 2,
                }}
              />
            </div>
          </Draggable>
        )}
      </div>
    );
  };

  tooltipPaywall = () => (
    <TooltipProvider renderTooltip={this.renderTooltip}>
      {this.sliderRangeSelector()}
    </TooltipProvider>
  );

  renderTooltip = (xPercentage) => {
    const { isHourly, isPremium } = this.props;

    const percentageCutOff = isHourly && isPremium ? 0.29 : 0.12;
    const hourlyTooltipVersion = isPremium ? 'hourlyPremiumTooltip' : 'hourlyFreeTooltip';
    return (
      xPercentage > percentageCutOff && (
        <div className="sl-swell-trend-slider__tooltip">
          {isHourly
            ? en.swellTrendSlider[hourlyTooltipVersion]
            : en.swellTrendSlider.tooltipPaywall}
        </div>
      )
    );
  };

  render() {
    const { isPremium, isHourly } = this.props;

    return (
      <div className="sl-swell-trend-slider">
        {!isPremium || isHourly ? this.tooltipPaywall() : this.sliderRangeSelector()}
      </div>
    );
  }
}

SwellTrendSlider.propTypes = {
  ranges: PropTypes.arrayOf(
    PropTypes.shape({
      start: PropTypes.number.isRequired,
      end: PropTypes.number.isRequired,
    }),
  ).isRequired,
  selectedStartDay: PropTypes.number.isRequired,
  onSelectRange: PropTypes.func.isRequired,
  daysEnabled: PropTypes.number.isRequired,
  daysPerRange: PropTypes.number.isRequired,
  waveMaxHeightsDays: PropTypes.arrayOf(PropTypes.arrayOf(waveMaxHeightTimestepPropType)),
  utcOffset: PropTypes.number.isRequired,
  isPremium: PropTypes.bool,
  isTabbing: PropTypes.bool,
  isSlider: PropTypes.bool.isRequired,
  days: PropTypes.arrayOf(PropTypes.number).isRequired,
  daysAndRanges: PropTypes.func.isRequired,
  deviceWidth: PropTypes.number.isRequired,
  isHourly: PropTypes.bool,
  dateFormat: PropTypes.oneOf(['MDY', 'DMY']),
};

SwellTrendSlider.defaultProps = {
  waveMaxHeightsDays: null,
  isPremium: false,
  isTabbing: false,
  isHourly: false,
  dateFormat: 'MDY',
};

export default SwellTrendSlider;
