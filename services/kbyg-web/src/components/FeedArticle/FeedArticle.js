import PropTypes from 'prop-types';
import React from 'react';
import { formatDistanceStrict } from 'date-fns';
import { EditorialArticleCard, ForecastArticleCard, GoogleDFP } from '@surfline/quiver-react';
import { canUseDOM, trackEvent, trackClickedSubscribeCTA, getWindow } from '@surfline/web-common';
import articlePropType from '../../propTypes/article';
import subregionAdConfigPropType from '../../propTypes/subregionAdConfig';
import loadAdConfig from '../../utils/adConfig';
import config from '../../config';

const onClickPaywall = (article, subregionId, subregionName) => {
  const win = getWindow();
  trackClickedSubscribeCTA({
    location: 'premium analysis',
    contentId: article.id.toString(),
    subregionId,
    subregionName,
  });

  if (canUseDOM) win.location.assign(`${config.surflineHost}${config.funnelUrl}`);
};

const FeedArticle = ({
  article,
  isPremium,
  subregionAdConfig,
  adIdentifier,
  showFullArticle,
  subregionName,
  subregionId,
}) => (
  <div className="sl-feed-article">
    {adIdentifier ? (
      <GoogleDFP
        key={`googleDFP_${article.id}`}
        adConfig={loadAdConfig(
          adIdentifier,
          subregionAdConfig.adTargets,
          subregionAdConfig.userEntitlements,
          subregionAdConfig.isUser,
        )}
        isHtl
        isTesting={config.htl.isTesting}
      />
    ) : null}
    {article.contentType === 'EDITORIAL' ? (
      <EditorialArticleCard
        author={article.author.name}
        externalLink={article.externalLink}
        externalSource={article.externalSource}
        newWindow={article.newWindow}
        publishedDateTime={formatDistanceStrict(article.updatedAt * 1000, new Date())}
        premium={article.premium}
        title={article.content.title}
        subtitle={article.content.subtitle}
        permalink={article.permalink}
        media={article.media}
        tags={article.tags}
        showPaywall={false} // Editoral articles should never show paywalls.
        funnelUrl={config.funnelUrl}
        onClickContent={() =>
          trackEvent('Clicked Regional Feed Link', {
            category: 'Regional Feed',
            location: 'premium analysis',
            destinationUrl: article.permalink,
            mediaType: article.media && article.media.type ? article.media.type : null,
            contentId: article.id,
          })
        }
        onClickPaywall={() => onClickPaywall(article, subregionId, subregionName)}
        onClickTag={(tag) =>
          trackEvent('Clicked Regional Feed Link', {
            category: 'Regional Feed',
            location: 'premium analysis',
            destinationUrl: tag.url,
            mediaType: article.media && article.media.type ? article.media.type : null,
            contentId: article.id,
          })
        }
      />
    ) : (
      <ForecastArticleCard
        publishedDateTime={formatDistanceStrict(article.updatedAt * 1000, new Date())}
        premium={article.premium}
        expert={article.promoted ? article.promoted.includes('REGIONAL') : false}
        title={article.content.title}
        body={article.content.body}
        author={article.author ? article.author.name : null}
        id={article.id}
        tags={article.tags}
        showPaywall={!isPremium && article.premium}
        isPremium={isPremium}
        funnelUrl={config.funnelUrl}
        showFullArticle={showFullArticle}
        onClickPaywall={() => onClickPaywall(article, subregionId, subregionName)}
        onClickReadMore={() =>
          trackEvent('Clicked Read More', {
            location: 'premium analysis',
            contentId: article.id,
            title: article.content.title,
            premiumArticle: article.premium,
            subregionId,
            subregionName,
          })
        }
        onClickShareArticle={(shareChannel) =>
          trackEvent('Clicked Share Icon', {
            title: article.content.title,
            contentId: `${article.id}`,
            tags: article.tags.map((tag) => tag.name).join(','),
            locationCategory: 'Premium Analysis',
            destinationUrl: article.permalink,
            mediaType: article.media ? article.media.type : null,
            shareChannel,
          })
        }
        segmentCategory="Regional Feed"
        fbAppId={config.appKeys.fbAppId}
        url={article.permalink}
        showShare={isPremium}
      />
    )}
  </div>
);

FeedArticle.propTypes = {
  article: articlePropType.isRequired,
  adIdentifier: PropTypes.string,
  isPremium: PropTypes.bool.isRequired,
  subregionAdConfig: subregionAdConfigPropType.isRequired,
  showFullArticle: PropTypes.bool.isRequired,
  subregionName: PropTypes.string.isRequired,
  subregionId: PropTypes.string.isRequired,
};

FeedArticle.defaultProps = {
  adIdentifier: null,
};

export default FeedArticle;
