/* eslint-disable react/no-danger */
import React from 'react';
import PropTypes from 'prop-types';
import TravelGuideRatings from '../TravelGuideRatings';
import TravelGuideNotes from '../TravelGuideNotes';
import travelDetailsPropTypes from '../../propTypes/travelDetails';

const TravelGuide = ({ spotName, travelDetails }) => {
  const {
    description: spotDescription,
    abilityLevels,
    localVibe,
    crowdFactor,
    spotRating,
    shoulderBurn,
    waterQuality,
    hazards,
    boardTypes,
    access,
    bottom,
    best,
  } = travelDetails;

  return (
    <div className="sl-travel-guide">
      <h3 className="sl-travel-guide__title">{spotName} Surf Guide</h3>
      <div className="sl-travel-guide__content">
        <section className="sl-travel-guide__content__overview">
          <p
            className="sl-travel-guide__overview__description"
            dangerouslySetInnerHTML={{
              __html: spotDescription,
            }}
          />
          <div className="sl-travel-guide__overview__ratings">
            <TravelGuideRatings
              abilityLevels={abilityLevels}
              localVibe={localVibe}
              crowdFactor={crowdFactor}
              spotRating={spotRating}
              shoulderBurn={shoulderBurn}
              waterQuality={waterQuality}
            />
          </div>
        </section>
        <aside className="sl-travel-guide__notes">
          <TravelGuideNotes
            hazards={hazards}
            boardTypes={boardTypes}
            access={access}
            bottom={bottom.description}
            bestSeason={best.season.description}
          />
        </aside>
      </div>
    </div>
  );
};

TravelGuide.propTypes = {
  spotName: PropTypes.string.isRequired,
  travelDetails: travelDetailsPropTypes.isRequired,
};

export default TravelGuide;
