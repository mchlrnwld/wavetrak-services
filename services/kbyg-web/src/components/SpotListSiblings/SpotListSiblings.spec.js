import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import deepFreeze from 'deep-freeze';
import SpotListSiblings from './SpotListSiblings';
import WavetrakLink from '../WavetrakLink';

describe('components / SpotListSiblings', () => {
  it('renders nearby sibling links when type map', () => {
    const props = deepFreeze({
      type: 'map',
      locationView: {
        taxonomy: {
          name: 'Huntington Beach',
        },
        travelContent: {
          content: {
            summary: 'Huntington Beach travel content summary',
          },
        },
        siblings: [
          {
            _id: '1',
            name: 'Newport Beach',
            enumeratedPath:
              'Earth, North America, United States, California, Orange County, Newport Beach',
            associated: {
              links: [
                {
                  key: 'www',
                  href: '/one',
                },
                {
                  key: 'travel',
                  href: '/one',
                },
              ],
            },
          },
          {
            _id: '2',
            name: 'Laguna Beach',
            enumeratedPath:
              'Earth, North America, United States, California, Orange County, Laguna Beach',
            associated: {
              links: [
                {
                  key: 'www',
                  href: '/two',
                },
                {
                  key: 'travel',
                  href: '/two',
                },
              ],
            },
          },
          {
            _id: '3',
            name: 'Seal Beach',
            enumeratedPath:
              'Earth, North America, United States, California, Orange County, Seal Beach',
            associated: {
              links: [
                {
                  key: 'www',
                  href: '/three',
                },
                {
                  key: 'travel',
                  href: '/three',
                },
              ],
            },
          },
        ],
      },
    });

    const wrapper = shallow(<SpotListSiblings {...props} />);
    const siblingLinks = wrapper.find(WavetrakLink);

    expect(siblingLinks).to.have.length(3);
  });

  it('renders nearby sibling anchors when type travel', () => {
    const props = deepFreeze({
      type: 'travel',
      locationView: {
        taxonomy: {
          name: 'Huntington Beach',
        },
        travelContent: {
          content: {
            summary: 'Huntington Beach travel content summary',
          },
        },
        siblings: [
          {
            _id: '1',
            name: 'Newport Beach',
            enumeratedPath:
              'Earth, North America, United States, California, Orange County, Newport Beach',
            associated: {
              links: [
                {
                  key: 'www',
                  href: '/one',
                },
                {
                  key: 'travel',
                  href: '/one',
                },
              ],
            },
          },
          {
            _id: '2',
            name: 'Laguna Beach',
            enumeratedPath:
              'Earth, North America, United States, California, Orange County, Laguna Beach',
            associated: {
              links: [
                {
                  key: 'www',
                  href: '/two',
                },
                {
                  key: 'travel',
                  href: '/two',
                },
              ],
            },
          },
          {
            _id: '3',
            name: 'Seal Beach',
            enumeratedPath:
              'Earth, North America, United States, California, Orange County, Seal Beach',
            associated: {
              links: [
                {
                  key: 'www',
                  href: '/three',
                },
                {
                  key: 'travel',
                  href: '/three',
                },
              ],
            },
          },
        ],
      },
    });

    const wrapper = shallow(<SpotListSiblings {...props} />);
    const siblingLinks = wrapper.find('a');

    expect(siblingLinks).to.have.length(3);
  });

  it('renders nearby subregion links when type subregion', () => {
    const props = deepFreeze({
      type: 'subregion',
      subregions: [
        {
          id: '12345',
          name: 'North Orange County',
          forecastStatus: {
            status: 'off',
          },
        },
        {
          id: '12346',
          name: 'South Orange County',
          forecastStatus: {
            status: 'active',
          },
        },
        {
          id: '12347',
          name: 'South Los Angeles',
          forecastStatus: {
            status: 'inactive',
          },
        },
      ],
    });

    const wrapper = shallow(<SpotListSiblings {...props} />);
    const siblingLinks = wrapper.find(WavetrakLink);

    expect(siblingLinks).to.have.length(3);
  });
});
