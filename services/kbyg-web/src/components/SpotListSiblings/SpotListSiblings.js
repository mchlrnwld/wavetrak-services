/* eslint-disable jsx-a11y/click-events-have-key-events */

import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classnames from 'classnames';
import { slugify } from '@surfline/web-common';
import locationViewPropShape from '../../propTypes/locationView';
import subregionsPropType from '../../propTypes/subregions';
import { subregionForecastPath } from '../../utils/urls';
import { camsAndForecastsPaths } from '../../utils/urls/camsAndForecastsPath';
import WavetrakLink from '../WavetrakLink';

class SpotListSiblings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemCount: 5,
      showMore: false,
    };
  }

  componentDidUpdate(prevProps) {
    const { locationView } = this.props;
    if (prevProps.locationView.siblings !== locationView.siblings) {
      this.resetSiblingState();
    }
  }

  typeMap = {
    subregion: {
      suffix: 'Surf Forecast',
      title: 'Top Regional Surf Forecasts',
      linkType: 'subregion',
    },
    map: {
      suffix: 'Surf Reports',
      title: 'Nearby Reports',
      linkType: 'www',
    },
    travel: {
      suffix: 'Beaches',
      title: 'Nearby Beaches',
      linkType: 'travel',
    },
  };

  getSiblingClasses = (length) => {
    const { showMore } = this.state;
    return classnames({
      'sl-spot-list-siblings__more': true,
      'sl-spot-list-siblings__more--hidden': length <= 5 || showMore,
    });
  };

  getMapLink = (sibling) => {
    const paths = sibling.enumeratedPath.split(',');
    const geoPaths = paths
      .slice(3)
      .map((slug) => slugify(slug))
      .join('/');
    return `${camsAndForecastsPaths.base}/${geoPaths}/${sibling.geonameId}`;
  };

  getChildLinks = (length, child, type, index, link) => {
    const { itemCount } = this.state;
    const linkText = `${child.name} ${this.typeMap[type].suffix}`;
    const linkSeparator = index !== itemCount - 1 && index !== length - 1 ? ',' : '';
    const text = `${linkText}${linkSeparator}`;

    return type === 'subregion'
      ? this.getSubregionLinks(child, text)
      : this.getSiblingLinks(child, type, text, link);
  };

  getSiblingLinks = (sibling, type, text, link) =>
    this.typeMap[type].linkType === 'travel' ? (
      <a href={link.href}>{text}</a>
    ) : (
      <WavetrakLink href={this.getMapLink(sibling)}>{text}</WavetrakLink>
    );

  getSubregionLinks = (subregion, text) => {
    const { name, id } = subregion;
    return <WavetrakLink href={`${subregionForecastPath(id, name)}`}>{text}</WavetrakLink>;
  };

  resetSiblingState = () => {
    this.setState({
      itemCount: 5,
      showMore: false,
    });
  };

  viewMoreClickHandler = () => {
    this.setState({
      itemCount: 10,
      showMore: true,
    });
  };

  resolveChildLinks = (id, length, child, type, index, link) => (
    <span key={id}>{this.getChildLinks(length, child, type, index, link)}</span>
  );

  renderSiblings = (iterator, sibling, type, index) => {
    if (type === 'subregion') {
      return this.resolveChildLinks(sibling.id, iterator.length, sibling, type, index);
    }
    return sibling.associated.links.map((link) => {
      if (link.key === this.typeMap[type].linkType) {
        return this.resolveChildLinks(sibling._id, iterator.length, sibling, type, index, link);
      }
      return null;
    });
  };

  render() {
    const { locationView, subregions, type } = this.props;
    const { itemCount } = this.state;
    const iterator = type === 'subregion' ? subregions : locationView.siblings;
    return (
      <div className="sl-spot-list-siblings">
        <div className="sl-spot-list-siblings__type">
          {iterator.length > 0 ? <h5>{`${this.typeMap[type].title}`}</h5> : null}
          {iterator
            .slice(0, itemCount)
            .map((sibling, index) => this.renderSiblings(iterator, sibling, type, index))}
          <span
            className={this.getSiblingClasses(iterator.length)}
            onClick={() => this.viewMoreClickHandler()}
          >
            <span>More</span>
          </span>
        </div>
      </div>
    );
  }
}

SpotListSiblings.propTypes = {
  locationView: locationViewPropShape.isRequired,
  type: PropTypes.string.isRequired,
  subregions: PropTypes.arrayOf(subregionsPropType).isRequired,
};

export default SpotListSiblings;
