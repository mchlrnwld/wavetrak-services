import type { NextPageContext } from 'next';
import { expect } from '@jest/globals';
import { mount } from 'enzyme';
import * as webCommon from '@surfline/web-common';
import ChartsPage from '../pages/surf-charts/[chartSlug]/[subregionSlug]/[subregionId]';

import { setActiveChart, fetchChartTypes } from '../actions/charts';
import * as subregionActions from '../actions/subregion';
import * as swellEventsActions from '../actions/swellEvents';
import * as subregionSelectors from '../selectors/subregion';
import * as meteringActions from '../actions/metering';

jest.mock('../containers/Charts', () => {
  const Charts = () => <div className="charts">Charts Page</div>;
  return Charts;
});

jest.mock('@surfline/web-common');
jest.mock('../actions/charts');
jest.mock('../actions/subregion');
jest.mock('../actions/swellEvents');
jest.mock('../actions/metering');
jest.mock('../selectors/subregion');

const performClientSidePageChangeSurfCheck =
  meteringActions.performClientSidePageChangeSurfCheck as jest.MockedFunction<
    typeof meteringActions.performClientSidePageChangeSurfCheck
  >;
const getClientIp = webCommon.getClientIp as jest.MockedFunction<typeof webCommon.getClientIp>;
const setActiveChartMock = setActiveChart as jest.MockedFunction<typeof setActiveChart>;
const fetchChartTypesMock = fetchChartTypes as jest.MockedFunction<typeof fetchChartTypes>;
const fetchNearbySubregions = subregionActions.fetchNearbySubregions as jest.MockedFunction<
  typeof subregionActions.fetchNearbySubregions
>;
const fetchRegionalArticles = subregionActions.fetchRegionalArticles as jest.MockedFunction<
  typeof subregionActions.fetchRegionalArticles
>;
const fetchOverviewData = subregionActions.fetchOverviewData as jest.MockedFunction<
  typeof subregionActions.fetchOverviewData
>;
const fetchSwellEvents = swellEventsActions.fetchSwellEvents as jest.MockedFunction<
  typeof swellEventsActions.fetchSwellEvents
>;

const getSubregionId = subregionSelectors.getSubregionId as jest.MockedFunction<
  typeof subregionSelectors.getSubregionId
>;

describe('ChartsPage', () => {
  beforeEach(() => {
    fetchNearbySubregions.mockReturnValue(async () => {});
    fetchRegionalArticles.mockReturnValue(async () => {});
    fetchOverviewData.mockReturnValue(async () => {});
    fetchChartTypesMock.mockReturnValue(async () => {});
    fetchSwellEvents.mockReturnValue(async () => {});
    setActiveChartMock.mockReturnValue(async () => {});
  });
  afterEach(() => {
    performClientSidePageChangeSurfCheck.mockClear();
    getClientIp.mockClear();
    setActiveChartMock.mockClear();
    fetchChartTypesMock.mockClear();
    fetchNearbySubregions.mockClear();
    fetchRegionalArticles.mockClear();
    fetchOverviewData.mockClear();
    fetchSwellEvents.mockClear();
    getSubregionId.mockClear();
  });

  afterAll(() => {
    jest.unmock('../actions/charts');
    jest.unmock('../actions/subregion');
    jest.unmock('../actions/swellEvents');
    jest.unmock('../actions/metering');
    jest.unmock('../containers/Charts');
    jest.unmock('@surfline/web-common');
  });

  it('should render the Charts Page', () => {
    const wrapper = mount(<ChartsPage />);

    const Chart = wrapper.find('.charts');

    expect(Chart).toHaveLength(1);
  });

  it('should have a getInitialProps function', async () => {
    expect(ChartsPage.getInitialProps).toBeInstanceOf(Function);
  });

  it('should return the server redux state for server requests', async () => {
    getClientIp.mockReturnValueOnce('1.2.3.4');
    getSubregionId.mockReturnValueOnce('');

    const result = await ChartsPage.getInitialProps!({
      req: { cookies: { token: '123' } },
      query: { subregionId: '123', chartSlug: 'wave-height' },
    } as unknown as NextPageContext);

    const cookies = { token: '123' };

    expect(result.ssrReduxState).toBeInstanceOf(Object);
    expect(result.isChartsPage).toBeTruthy();
    expect(fetchOverviewData).toHaveBeenNthCalledWith(1, '123', cookies, '1.2.3.4', true, false);
    expect(fetchNearbySubregions).toHaveBeenNthCalledWith(1, '123', cookies, false);
    expect(fetchRegionalArticles).toHaveBeenNthCalledWith(1, '123', cookies);
    expect(fetchChartTypesMock).toHaveBeenNthCalledWith(1, '123', cookies);
    expect(fetchSwellEvents).toHaveBeenNthCalledWith(1, '123', 'subregion');
    expect(setActiveChartMock).toHaveBeenNthCalledWith(1, 'wave-height', '123', cookies);
  });

  it('should not return the redux state for client-side requests', async () => {
    getClientIp.mockReturnValueOnce(undefined);
    getSubregionId.mockReturnValueOnce('345');
    const result = await ChartsPage.getInitialProps!({
      query: { subregionId: '123', chartSlug: 'wave-height' },
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeUndefined();
    expect(result.isChartsPage).toBeTruthy();
    expect(fetchOverviewData).toHaveBeenNthCalledWith(1, '123', {}, undefined, true, false);
    expect(fetchNearbySubregions).toHaveBeenNthCalledWith(1, '123', {}, false);
    expect(fetchRegionalArticles).toHaveBeenNthCalledWith(1, '123', {});
    expect(fetchChartTypesMock).toHaveBeenNthCalledWith(1, '123', {});
    expect(fetchSwellEvents).toHaveBeenNthCalledWith(1, '123', 'subregion');
    expect(setActiveChartMock).toHaveBeenNthCalledWith(1, 'wave-height', '123', {});
  });

  it('should skip fetching subregion data if it has not changed', async () => {
    getClientIp.mockReturnValueOnce(undefined);
    getSubregionId.mockReturnValueOnce('123');
    const result = await ChartsPage.getInitialProps!({
      query: { subregionId: '123', chartSlug: 'wave-height' },
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeUndefined();
    expect(result.isChartsPage).toBeTruthy();
    expect(fetchOverviewData).not.toHaveBeenCalled();
    expect(fetchNearbySubregions).not.toHaveBeenCalled();
    expect(fetchRegionalArticles).not.toHaveBeenCalled();
    expect(fetchChartTypesMock).not.toHaveBeenCalled();
    expect(fetchSwellEvents).not.toHaveBeenCalled();
    expect(setActiveChartMock).toHaveBeenNthCalledWith(1, 'wave-height', '123', {});
  });
});
