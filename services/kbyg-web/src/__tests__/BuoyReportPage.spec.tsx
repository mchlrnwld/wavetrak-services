import type { NextPageContext } from 'next';
import { expect } from '@jest/globals';
import { mount } from 'enzyme';
import BuoyPage from '../pages/buoy-report/[buoySlug]/[buoyId]';
import { serverSideFetchBuoyDetails } from '../actions/buoys';

jest.mock('../containers/Buoy', () => {
  const Buoy = () => <div className="buoy">Buoy Page</div>;
  return Buoy;
});

const fetchBuoyDetailsMock = serverSideFetchBuoyDetails as jest.MockedFunction<
  typeof serverSideFetchBuoyDetails
>;

jest.mock('../actions/buoys');

describe('BuoyReportPage', () => {
  afterEach(() => {
    fetchBuoyDetailsMock.mockClear();
  });
  afterAll(() => {
    jest.unmock('../actions/buoys');
    jest.unmock('../containers/Buoy');
  });

  it('Should render the Buoy Report Page', () => {
    const wrapper = mount(<BuoyPage />);

    const buoy = wrapper.find('.buoy');

    expect(buoy).toHaveLength(1);
  });

  it('should have a getInitialProps function', async () => {
    expect(BuoyPage.getInitialProps).toBeInstanceOf(Function);
  });

  it('should return the server redux state for server requests', async () => {
    fetchBuoyDetailsMock.mockReturnValueOnce(async () => ({
      type: 'SERVER_SIDE_FETCH_BUOY_DETAILS',
      message: '',
    }));
    const result = await BuoyPage.getInitialProps!({
      req: {},
      query: { buoyId: '123' },
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeInstanceOf(Object);
    expect(fetchBuoyDetailsMock).toHaveBeenNthCalledWith(1, '123');
  });

  it('should not return the redux state for client-side requests', async () => {
    const result = await BuoyPage.getInitialProps!({
      query: { buoyId: '123' },
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeUndefined();
    expect(fetchBuoyDetailsMock).not.toHaveBeenCalled();
  });
});
