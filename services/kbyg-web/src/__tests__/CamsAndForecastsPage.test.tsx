import type { NextPageContext } from 'next';
import { expect } from '@jest/globals';
import { mount } from 'enzyme';

import CamsAndForecastsPage from '../pages/surf-reports-forecasts-cams';

import * as worldTaxonomyActions from '../actions/worldTaxonomy';
import * as meteringActions from '../actions/metering';

jest.mock('../containers/Map', () => {
  const Page = () => <div className="page">Page</div>;
  return Page;
});

jest.mock('../actions/worldTaxonomy');
jest.mock('../actions/metering');
jest.mock('../containers/CamsAndForecasts', () => {
  const Page = () => <div className="page">Page</div>;
  return Page;
});

const fetchWorldTaxonomy = worldTaxonomyActions.fetchWorldTaxonomy as jest.MockedFunction<
  typeof worldTaxonomyActions.fetchWorldTaxonomy
>;
const performClientSidePageChangeSurfCheck =
  meteringActions.performClientSidePageChangeSurfCheck as jest.MockedFunction<
    typeof meteringActions.performClientSidePageChangeSurfCheck
  >;

describe('CamsAndForecastsPage', () => {
  afterAll(() => {
    jest.unmock('../containers/CamsAndForecasts');
    jest.unmock('../actions/worldTaxonomy');
    jest.unmock('../actions/metering');
  });

  beforeEach(() => {
    fetchWorldTaxonomy.mockReturnValue(async () => {});
  });

  afterEach(() => {
    fetchWorldTaxonomy.mockClear();
    performClientSidePageChangeSurfCheck.mockClear();
  });

  it('should render the Buoy Map Page', () => {
    const wrapper = mount(<CamsAndForecastsPage />);
    const page = wrapper.find('.page');

    expect(page).toHaveLength(1);
  });

  it('should have a getInitialProps function', async () => {
    expect(CamsAndForecastsPage.getInitialProps).toBeInstanceOf(Function);
  });

  it('should return the server redux state for server requests', async () => {
    const result = await CamsAndForecastsPage.getInitialProps!({
      req: {},
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeInstanceOf(Object);
    expect(performClientSidePageChangeSurfCheck).toHaveBeenCalledTimes(1);
  });

  it('should not return the redux state for client-side requests', async () => {
    const result = await CamsAndForecastsPage.getInitialProps!({} as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeUndefined();
    expect(performClientSidePageChangeSurfCheck).toHaveBeenCalledTimes(1);
  });
});
