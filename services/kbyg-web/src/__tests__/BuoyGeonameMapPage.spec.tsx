import type { NextPageContext } from 'next';
import { expect } from '@jest/globals';

import { AsyncThunkAction } from '@reduxjs/toolkit';
import BuoyGeonameMapPage from '../pages/surf-reports-forecasts-cams/buoys/[...geonames]';

import * as mapActions from '../actions/mapV2';

import { mountWithReduxAndRouter } from '../utils/test-utils';

jest.mock('../containers/BuoyGeonameMap', () => {
  const Page = () => <div className="page">Page</div>;
  return Page;
});

jest.mock('../actions/mapV2');

const fetchLocationView = mapActions.fetchLocationView as jest.MockedFunction<
  typeof mapActions.fetchLocationView
>;

describe('BuoyGeonameMapPage', () => {
  beforeEach(() => {
    fetchLocationView.mockReturnValue((async () => ({
      type: 'FETCH_LOCATION_VIEW_SUCCESS',
      meta: {},
      payload: {},
    })) as unknown as AsyncThunkAction<any, any, any>);
  });

  afterEach(() => {
    fetchLocationView.mockClear();
  });

  afterAll(() => {
    jest.unmock('../actions/mapV2');
    jest.unmock('../containers/BuoyGeonameMap');
  });

  const geonames = ['united-states', 'santa-cruz', '123'];

  it('should render the Geoname Map Page', () => {
    const { wrapper } = mountWithReduxAndRouter(
      BuoyGeonameMapPage,
      {},
      { router: { query: { geonames } } },
    );

    const page = wrapper.find('.page');

    expect(page).toHaveLength(1);
  });

  it('should have a getInitialProps function', async () => {
    expect(BuoyGeonameMapPage.getInitialProps).toBeInstanceOf(Function);
  });

  it('should return the server redux state for server requests', async () => {
    const result = await BuoyGeonameMapPage.getInitialProps!({
      req: {},
      query: { geonames },
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeInstanceOf(Object);
    expect(result.isMapV2Page).toBe(true);
    expect(fetchLocationView).toHaveBeenNthCalledWith(1, '123');
  });

  it('should not return the redux state for client-side requests', async () => {
    const result = await BuoyGeonameMapPage.getInitialProps!({
      query: { geonames },
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeUndefined();
    expect(result.isMapV2Page).toBe(true);

    expect(fetchLocationView).toHaveBeenNthCalledWith(1, '123');
  });
});
