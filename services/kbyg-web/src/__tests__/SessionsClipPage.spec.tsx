import type { NextPageContext } from 'next';
import { expect } from 'chai';
import { mount } from 'enzyme';
import SessionsClipPage from '../pages/sessions/clips/[clipId]';
import { getOrCreateStore } from '../stores';
import { fetchSessionsClip } from '../common/api/sessions';
import type { SessionClip } from '../types/sessions';

jest.mock('../containers/SessionsClip', () => {
  const Sessions = () => <div className="sessions">Sessions Clip</div>;
  return Sessions;
});

const fetchSessionsClipMock = fetchSessionsClip as jest.MockedFunction<typeof fetchSessionsClip>;
jest.mock('../common/api/sessions');

describe('pages / SessionsClip', () => {
  afterEach(() => {
    fetchSessionsClipMock.mockReset();
  });

  afterAll(() => {
    jest.unmock('../containers/SessionsClip');
    jest.unmock('../common/api/sessions');
  });

  const clip = { _id: '123' } as unknown as SessionClip;

  it('Should render the Sessions Page in an Error Boundary', () => {
    const wrapper = mount(<SessionsClipPage />);

    const eb = wrapper.find('ErrorBoundary');
    const sessions = wrapper.find('.sessions');

    expect(eb).to.have.length(1);
    expect(sessions).to.have.length(1);
  });

  it('should have a getInitialProps function', async () => {
    expect(SessionsClipPage.getInitialProps).to.be.a('function');
  });

  it('should handle server side requests', async () => {
    fetchSessionsClipMock.mockResolvedValue(clip);
    const result = await SessionsClipPage.getInitialProps!({
      req: {},
      query: { clipId: '123' },
      headers: { cookie: '' },
    } as any as NextPageContext);

    expect(result.ssrReduxState).to.be.an('object');
    expect(result.sessionClip).to.deep.equal(clip);
  });

  it('should handle server side requests - clip not found error', async () => {
    fetchSessionsClipMock.mockRejectedValue({ statusCode: 404 });
    const result = await SessionsClipPage.getInitialProps!({
      req: {},
      query: { clipId: '123' },
      headers: { cookie: '' },
    } as any as NextPageContext);

    expect(result.ssrReduxState).to.be.an('object');
    expect(result.ssrReduxState?.status.code).to.be.equal(404);
    expect(result.sessionClip).to.not.exist();
  });

  it('should not return the redux state for client-side requests', async () => {
    fetchSessionsClipMock.mockResolvedValue(clip);
    const result = await SessionsClipPage.getInitialProps!({
      query: { clipId: '123' },
    } as any as NextPageContext);

    expect(result.ssrReduxState).to.not.exist();
    expect(result.sessionClip).to.deep.equal(clip);
  });

  it('should handle client side requests - clip not found error', async () => {
    fetchSessionsClipMock.mockRejectedValue({ statusCode: 500 });
    const result = await SessionsClipPage.getInitialProps!({
      query: { clipId: '123' },
      headers: { cookie: '' },
    } as any as NextPageContext);

    const store = getOrCreateStore();
    expect(result.sessionClip).to.not.exist();
    expect(store.getState().status.code).to.be.equal(500);
  });
});
