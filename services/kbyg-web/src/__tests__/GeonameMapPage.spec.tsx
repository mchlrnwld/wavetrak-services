import type { NextPageContext } from 'next';
import { expect } from '@jest/globals';
import { act } from 'react-dom/test-utils';
import * as common from '@surfline/web-common';

import GeonameMapPage from '../pages/surf-reports-forecasts-cams/[...geonames]';

import * as mapActions from '../actions/map';
import * as routesActions from '../actions/routes';
import * as meteringActions from '../actions/metering';
import * as meteringHooks from '../common/hooks/useMetering';

import { mountWithReduxAndRouter } from '../utils/test-utils';

jest.mock('../containers/GeonameMap', () => {
  const Page = () => <div className="page">Page</div>;
  return Page;
});

jest.mock('@surfline/web-common');
jest.mock('../actions/routes');
jest.mock('../actions/map');
jest.mock('../actions/metering');
jest.mock('../common/hooks/useMetering');

const trackNavigatedToPage = common.trackNavigatedToPage as jest.MockedFunction<
  typeof common.trackNavigatedToPage
>;

const getWindow = common.getWindow as jest.MockedFunction<typeof common.getWindow>;

const performClientSidePageChangeSurfCheck =
  meteringActions.performClientSidePageChangeSurfCheck as jest.MockedFunction<
    typeof meteringActions.performClientSidePageChangeSurfCheck
  >;

const fetchLocationView = mapActions.fetchLocationView as jest.MockedFunction<
  typeof mapActions.fetchLocationView
>;

const leaveGeonameMapRoute = routesActions.leaveGeonameMapRoute as jest.MockedFunction<
  typeof routesActions.leaveGeonameMapRoute
>;

const useMetering = meteringHooks.default as jest.MockedFunction<typeof meteringHooks.default>;

const meter = {
  showMeterRegwall: false,
  meterRemaining: undefined,
  isMeteredPremium: false,
  meteringEnabled: false,
};

describe('GeonameMapPage', () => {
  beforeEach(() => {
    fetchLocationView.mockReturnValue(async () => {});
    useMetering.mockReturnValue(meter);
    leaveGeonameMapRoute.mockReturnValue({ type: 'LEAVE_GEONAME_MAP_ROUTE' });
    getWindow.mockReturnValue(window);
  });

  afterEach(() => {
    trackNavigatedToPage.mockClear();
    performClientSidePageChangeSurfCheck.mockClear();
    fetchLocationView.mockClear();
    useMetering.mockClear();
    leaveGeonameMapRoute.mockClear();
  });

  afterAll(() => {
    jest.unmock('@surfline/web-common');
    jest.unmock('../actions/map');
    jest.unmock('../actions/metering');
    jest.unmock('../actions/routes');
    jest.unmock('../common/hooks/useMetering');
    jest.unmock('../containers/GeonameMap');
  });

  const geonames = ['united-states', 'santa-cruz', '123'];

  it('should render the Geoname Map Page', () => {
    const { wrapper } = mountWithReduxAndRouter(
      GeonameMapPage,
      {},
      { router: { query: { geonames } } },
    );

    const page = wrapper.find('.page');

    expect(page).toHaveLength(1);
    expect(window.document.body.classList).toContain('sl-map-page');
    expect(window.document.body.classList).toContain('sl-hide-ze-widget');
    expect(trackNavigatedToPage).toHaveBeenNthCalledWith(1, 'Map', {
      meteringEnabled: meter.meteringEnabled,
      category: 'cams & forecasts',
      channel: 'cams & forecasts',
    });

    act(() => {
      wrapper.unmount();
    });

    expect(window.document.body.classList).not.toContain('sl-map-page');
    expect(window.document.body.classList).not.toContain('sl-hide-ze-widget');
    expect(leaveGeonameMapRoute).toHaveBeenCalledTimes(1);
  });

  it('should have a getInitialProps function', async () => {
    expect(GeonameMapPage.getInitialProps).toBeInstanceOf(Function);
  });

  it('should return the server redux state for server requests', async () => {
    const result = await GeonameMapPage.getInitialProps!({
      req: {},
      query: { geonames },
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeInstanceOf(Object);
    expect(performClientSidePageChangeSurfCheck).toHaveBeenCalledTimes(1);
    expect(fetchLocationView).toHaveBeenNthCalledWith(1, '123');
  });

  it('should not return the redux state for client-side requests', async () => {
    const result = await GeonameMapPage.getInitialProps!({
      query: { geonames },
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeUndefined();

    expect(performClientSidePageChangeSurfCheck).toHaveBeenCalledTimes(1);
    expect(fetchLocationView).toHaveBeenNthCalledWith(1, '123');
  });
});
