import type { NextPageContext } from 'next';
import { expect } from '@jest/globals';
import { mount } from 'enzyme';

import BuoyMapPage from '../pages/surf-reports-forecasts-cams-map/buoys';
import BuoyMapPageWithLocation from '../pages/surf-reports-forecasts-cams-map/buoys/[location]';

jest.mock('../containers/BuoyMap', () => {
  const Page = () => <div className="page">Page</div>;
  return Page;
});

describe('Buoy Map Pages', () => {
  afterAll(() => {
    jest.unmock('../containers/BuoyMap');
  });

  describe('BuoyMapPage', () => {
    it('should render the Buoy Map Page', () => {
      const wrapper = mount(<BuoyMapPage />);
      const page = wrapper.find('.page');

      expect(page).toHaveLength(1);
    });

    it('should have a getInitialProps function', async () => {
      expect(BuoyMapPage.getInitialProps).toBeInstanceOf(Function);
    });

    it('should return the server redux state for server requests', async () => {
      const result = await BuoyMapPage.getInitialProps!({
        req: {},
      } as unknown as NextPageContext);

      expect(result.ssrReduxState).toBeInstanceOf(Object);
      expect(result.isMapV2Page).toBe(true);
    });

    it('should not return the redux state for client-side requests', async () => {
      const result = await BuoyMapPage.getInitialProps!({} as unknown as NextPageContext);

      expect(result.ssrReduxState).toBeUndefined();
      expect(result.isMapV2Page).toBe(true);
    });
  });

  describe('BuoyMapPageWithLocation', () => {
    it('should render the Buoy Map Page', () => {
      const wrapper = mount(<BuoyMapPageWithLocation />);
      const page = wrapper.find('.page');

      expect(page).toHaveLength(1);
    });

    it('should have a getInitialProps function', async () => {
      expect(BuoyMapPageWithLocation.getInitialProps).toBeInstanceOf(Function);
    });

    it('should return the server redux state for server requests', async () => {
      const result = await BuoyMapPageWithLocation.getInitialProps!({
        req: {},
      } as unknown as NextPageContext);

      expect(result.ssrReduxState).toBeInstanceOf(Object);
      expect(result.isMapV2Page).toBe(true);
    });

    it('should not return the redux state for client-side requests', async () => {
      const result = await BuoyMapPageWithLocation.getInitialProps!(
        {} as unknown as NextPageContext,
      );

      expect(result.ssrReduxState).toBeUndefined();
      expect(result.isMapV2Page).toBe(true);
    });
  });
});
