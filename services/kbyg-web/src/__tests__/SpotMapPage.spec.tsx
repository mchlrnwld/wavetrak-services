import type { NextPageContext } from 'next';
import { expect } from '@jest/globals';
import { act } from 'react-dom/test-utils';
import * as common from '@surfline/web-common';

import SpotMapPage from '../pages/surf-reports-forecasts-cams-map';
import SpotMapPageWithLocation from '../pages/surf-reports-forecasts-cams-map/[location]';

import * as mapActions from '../actions/map';
import * as meteringActions from '../actions/metering';
import * as meteringHooks from '../common/hooks/useMetering';

import { mountWithReduxAndRouter } from '../utils/test-utils';

jest.mock('../containers/Map', () => {
  const Page = () => <div className="page">Page</div>;
  return Page;
});

jest.mock('../components/PageLoading', () => {
  const Page = () => <div className="loading">Loading</div>;
  return Page;
});

jest.mock('../components/MapPageMeta', () => {
  const Page = () => <div className="meta">meta</div>;
  return Page;
});

jest.mock('@surfline/web-common');

jest.mock('../actions/map');
jest.mock('../actions/metering');
jest.mock('../common/hooks/useMetering');

const performClientSidePageChangeSurfCheck =
  meteringActions.performClientSidePageChangeSurfCheck as jest.MockedFunction<
    typeof meteringActions.performClientSidePageChangeSurfCheck
  >;

const setMapLocation = mapActions.setMapLocation as jest.MockedFunction<
  typeof mapActions.setMapLocation
>;

const goToUsersNearestSpot = mapActions.goToUsersNearestSpot as jest.MockedFunction<
  typeof mapActions.goToUsersNearestSpot
>;

const trackNavigatedToPage = common.trackNavigatedToPage as jest.MockedFunction<
  typeof common.trackNavigatedToPage
>;

const getWindow = common.getWindow as jest.MockedFunction<typeof common.getWindow>;

const useMetering = meteringHooks.default as jest.MockedFunction<typeof meteringHooks.default>;

describe('Spot Map Pages', () => {
  const meter = {
    showMeterRegwall: false,
    meterRemaining: undefined,
    isMeteredPremium: false,
    meteringEnabled: false,
  };

  const getItemStub = jest.fn();
  const setItemStub = jest.fn();
  const sessionStorageMock = {
    getItem: getItemStub,
    setItem: setItemStub,
  };

  const { sessionStorage } = window;

  beforeAll(() => {
    Object.defineProperty(window, 'sessionStorage', {
      value: sessionStorageMock,
    });
  });

  beforeEach(() => {
    getWindow.mockReturnValue(window);
    setMapLocation.mockReturnValue(async () => {});
    goToUsersNearestSpot.mockReturnValue(async () => {});
    getWindow.mockReturnValue(window);
    useMetering.mockReturnValue(meter);
  });

  afterEach(() => {
    performClientSidePageChangeSurfCheck.mockClear();
    getItemStub.mockReset();
    setItemStub.mockClear();
    setMapLocation.mockClear();
    goToUsersNearestSpot.mockClear();
  });

  afterAll(() => {
    jest.unmock('@surfline/web-common');
    jest.unmock('../actions/map');
    jest.unmock('../actions/metering');
    jest.unmock('../containers/Map');
    jest.unmock('../components/PageLoading');
    jest.unmock('../components/MapPageMeta');

    Object.defineProperty(window, 'sessionStorage', {
      value: sessionStorage,
    });
  });

  const location = { center: { lat: 1, lon: 2 }, zoom: 3 };

  describe('SpotMapPage', () => {
    it('should render the loading until it sets map location', () => {
      const { wrapper } = mountWithReduxAndRouter(SpotMapPage);

      const page = wrapper.find('.loading');
      const meta = wrapper.find('.meta');

      expect(page).toHaveLength(1);
      expect(meta).toHaveLength(1);
      expect(window.document.body.classList).toContain('sl-map-page');
      expect(window.document.body.classList).toContain('sl-hide-ze-widget');

      act(() => {
        wrapper.unmount();
      });

      expect(window.document.body.classList).not.toContain('sl-map-page');
      expect(window.document.body.classList).not.toContain('sl-hide-ze-widget');
    });

    it('should set the map location if a session location exists', () => {
      getItemStub.mockReturnValue(JSON.stringify(location));
      const { router } = mountWithReduxAndRouter(SpotMapPage);
      expect(setMapLocation).toHaveBeenNthCalledWith(1, location, router, true);
      expect(goToUsersNearestSpot).not.toHaveBeenCalled();
    });

    it('should go to the users nearest spot if no session location exists', () => {
      const { router } = mountWithReduxAndRouter(SpotMapPage);

      expect(setMapLocation).not.toHaveBeenCalled();
      expect(goToUsersNearestSpot).toHaveBeenNthCalledWith(1, router, true);
    });

    it('should have a getInitialProps function', async () => {
      expect(SpotMapPage.getInitialProps).toBeInstanceOf(Function);
    });

    it('should return the server redux state for server requests', async () => {
      const result = await SpotMapPage.getInitialProps!({
        req: {},
      } as unknown as NextPageContext);

      expect(result.ssrReduxState).toBeInstanceOf(Object);
      expect(performClientSidePageChangeSurfCheck).toHaveBeenCalledTimes(1);
    });

    it('should not return the redux state for client-side requests', async () => {
      const result = await SpotMapPage.getInitialProps!({} as unknown as NextPageContext);

      expect(result.ssrReduxState).toBeUndefined();

      expect(performClientSidePageChangeSurfCheck).toHaveBeenCalledTimes(1);
    });
  });

  describe('SpotMapPageWitLocation', () => {
    it('should render the Spot Map Page with location', () => {
      const { wrapper, router } = mountWithReduxAndRouter(
        SpotMapPageWithLocation,
        {},
        { router: { query: { location: '1,2,3' } } },
      );

      const page = wrapper.find('.page');

      expect(page).toHaveLength(1);

      expect(window.document.body.classList).toContain('sl-map-page');
      expect(window.document.body.classList).toContain('sl-hide-ze-widget');

      expect(trackNavigatedToPage).toHaveBeenNthCalledWith(1, 'Map', {
        meteringEnabled: meter.meteringEnabled,
        category: 'cams & forecasts',
        channel: 'cams & forecasts',
      });

      expect(setMapLocation).toHaveBeenNthCalledWith(1, location, router, true);

      act(() => {
        wrapper.unmount();
      });

      expect(window.document.body.classList).not.toContain('sl-map-page');
      expect(window.document.body.classList).not.toContain('sl-hide-ze-widget');
    });

    it('should use a session location', () => {
      const sessionLocation = { center: { lat: 3, lon: 4 }, zoom: 5 };
      getItemStub.mockReturnValue(JSON.stringify(sessionLocation));

      const { router } = mountWithReduxAndRouter(SpotMapPageWithLocation);

      expect(setMapLocation).toHaveBeenNthCalledWith(1, sessionLocation, router, true);
    });

    it('should have a getInitialProps function', async () => {
      expect(SpotMapPageWithLocation.getInitialProps).toBeInstanceOf(Function);
    });

    it('should return the server redux state for server requests', async () => {
      const result = await SpotMapPageWithLocation.getInitialProps!({
        req: {},
      } as unknown as NextPageContext);

      expect(result.ssrReduxState).toBeInstanceOf(Object);
      expect(performClientSidePageChangeSurfCheck).toHaveBeenCalledTimes(1);
    });

    it('should not return the redux state for client-side requests', async () => {
      const result = await SpotMapPageWithLocation.getInitialProps!(
        {} as unknown as NextPageContext,
      );

      expect(result.ssrReduxState).toBeUndefined();

      expect(performClientSidePageChangeSurfCheck).toHaveBeenCalledTimes(1);
    });
  });
});
