import type { NextPageContext } from 'next';
import { expect } from '@jest/globals';
import { mount } from 'enzyme';
import * as webCommon from '@surfline/web-common';

import PremiumAnalysisPage from '../pages/surf-forecasts/[subregionSlug]/[subregionId]/premium-analysis';

import * as subregionActions from '../actions/subregion';
import * as swellEventsActions from '../actions/swellEvents';
import * as meteringActions from '../actions/metering';

jest.mock('../containers/PremiumAnalysis', () => {
  const PremiumAnalysis = () => <div className="premium-analysis">Premium Analysis Page</div>;
  return PremiumAnalysis;
});

jest.mock('@surfline/web-common');
jest.mock('../actions/subregion');
jest.mock('../actions/swellEvents');
jest.mock('../actions/metering');

const performClientSidePageChangeSurfCheck =
  meteringActions.performClientSidePageChangeSurfCheck as jest.MockedFunction<
    typeof meteringActions.performClientSidePageChangeSurfCheck
  >;
const getClientIp = webCommon.getClientIp as jest.MockedFunction<typeof webCommon.getClientIp>;

const fetchNearbySubregions = subregionActions.fetchNearbySubregions as jest.MockedFunction<
  typeof subregionActions.fetchNearbySubregions
>;
const fetchOverviewData = subregionActions.fetchOverviewData as jest.MockedFunction<
  typeof subregionActions.fetchOverviewData
>;
const fetchRegionalArticles = subregionActions.fetchRegionalArticles as jest.MockedFunction<
  typeof subregionActions.fetchRegionalArticles
>;
const fetchSwellEvents = swellEventsActions.fetchSwellEvents as jest.MockedFunction<
  typeof swellEventsActions.fetchSwellEvents
>;
const setSubregionLoading = subregionActions.setSubregionLoading as jest.MockedFunction<
  typeof subregionActions.setSubregionLoading
>;

describe('PremiumAnalyisPage', () => {
  beforeEach(() => {
    fetchNearbySubregions.mockReturnValue(async () => {});
    fetchOverviewData.mockReturnValue(async () => {});
    fetchRegionalArticles.mockReturnValue(async () => {});
    fetchSwellEvents.mockReturnValue(async () => {});
    setSubregionLoading.mockReturnValue({ type: 'SET_SUBREGION_LOADING', payload: true });
  });
  afterEach(() => {
    performClientSidePageChangeSurfCheck.mockClear();
    getClientIp.mockClear();

    fetchNearbySubregions.mockClear();
    fetchOverviewData.mockClear();
    fetchSwellEvents.mockClear();
    fetchRegionalArticles.mockClear();
  });

  afterAll(() => {
    jest.unmock('../actions/subregion');
    jest.unmock('../actions/swellEvents');
    jest.unmock('../actions/metering');
    jest.unmock('../containers/SubregionForecast');
  });

  it('should render the Premium Analysis Page', () => {
    const wrapper = mount(<PremiumAnalysisPage />);

    const subregion = wrapper.find('.premium-analysis');

    expect(subregion).toHaveLength(1);
  });

  it('should have a getInitialProps function', async () => {
    expect(PremiumAnalysisPage.getInitialProps).toBeInstanceOf(Function);
  });

  it('should return the server redux state for server requests', async () => {
    getClientIp.mockReturnValueOnce('1.2.3.4');

    const result = await PremiumAnalysisPage.getInitialProps!({
      req: { cookies: { token: '123' } },
      query: { subregionId: '123' },
    } as unknown as NextPageContext);

    const cookies = { token: '123' };

    expect(result.ssrReduxState).toBeInstanceOf(Object);
    expect(result.isPremiumAnalysisPage).toBeTruthy();

    expect(fetchOverviewData).toHaveBeenNthCalledWith(1, '123', cookies, '1.2.3.4', true, false);
    expect(fetchNearbySubregions).toHaveBeenNthCalledWith(1, '123', cookies, false);
    expect(fetchRegionalArticles).toHaveBeenNthCalledWith(1, '123', cookies);
    expect(fetchSwellEvents).toHaveBeenNthCalledWith(1, '123', 'subregion');
    expect(setSubregionLoading).toHaveBeenNthCalledWith(1, true);
  });

  it('should not return the redux state for client-side requests', async () => {
    getClientIp.mockReturnValueOnce(undefined);

    const result = await PremiumAnalysisPage.getInitialProps!({
      query: { subregionId: '123' },
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeUndefined();
    expect(result.isPremiumAnalysisPage).toBeTruthy();
    expect(fetchOverviewData).toHaveBeenNthCalledWith(1, '123', {}, undefined, true, false);
    expect(fetchNearbySubregions).toHaveBeenNthCalledWith(1, '123', {}, false);
    expect(fetchSwellEvents).toHaveBeenNthCalledWith(1, '123', 'subregion');
    expect(fetchRegionalArticles).toHaveBeenNthCalledWith(1, '123', {});
    expect(setSubregionLoading).toHaveBeenNthCalledWith(1, true);
  });
});
