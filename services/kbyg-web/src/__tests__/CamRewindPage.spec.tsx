import type { NextPageContext } from 'next';
import { expect } from '@jest/globals';
import { mount } from 'enzyme';
import CamRewindPage from '../pages/surf-cams/[spotName]/[camId]';
import { performClientSidePageChangeSurfCheck } from '../actions/metering';

jest.mock('../containers/CamRewind', () => {
  const CamRewind = () => <div className="cam-rewind">Cam Rewind Page</div>;
  return CamRewind;
});

const performClientSidePageChangeSurfCheckMock =
  performClientSidePageChangeSurfCheck as jest.MockedFunction<
    typeof performClientSidePageChangeSurfCheck
  >;

jest.mock('../actions/metering');

describe('CamRewindPage', () => {
  afterEach(() => {
    performClientSidePageChangeSurfCheckMock.mockClear();
  });
  afterAll(() => {
    jest.unmock('../actions/metering');
    jest.unmock('../containers/CamRewind');
  });

  it('Should render the Cam Rewind Page', () => {
    const wrapper = mount(<CamRewindPage />);

    const camrewind = wrapper.find('.cam-rewind');

    expect(camrewind).toHaveLength(1);
  });

  it('should have a getInitialProps function', async () => {
    expect(CamRewindPage.getInitialProps).toBeInstanceOf(Function);
  });

  it('should return the server redux state for server requests', async () => {
    const result = await CamRewindPage.getInitialProps!({
      req: {},
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeInstanceOf(Object);
    expect(performClientSidePageChangeSurfCheckMock).toHaveBeenCalledTimes(1);
  });

  it('should not return the redux state for client-side requests', async () => {
    const result = await CamRewindPage.getInitialProps!({} as NextPageContext);

    expect(result.ssrReduxState).toBeUndefined();
    expect(performClientSidePageChangeSurfCheckMock).toHaveBeenCalledTimes(1);
  });
});
