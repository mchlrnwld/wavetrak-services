import type { NextPageContext } from 'next';
import { expect } from '@jest/globals';
import { mount } from 'enzyme';
import * as webCommon from '@surfline/web-common';

import SubregionForecastPage from '../pages/surf-forecasts/[subregionSlug]/[subregionId]';

import * as subregionActions from '../actions/subregion';
import * as swellEventsActions from '../actions/swellEvents';
import * as meteringActions from '../actions/metering';

jest.mock('../containers/SubregionForecast', () => {
  const SubregionForecast = () => <div className="subregion">Subregion Forecast Page</div>;
  return SubregionForecast;
});

jest.mock('@surfline/web-common');
jest.mock('../actions/subregion');
jest.mock('../actions/swellEvents');
jest.mock('../actions/metering');

const performClientSidePageChangeSurfCheck =
  meteringActions.performClientSidePageChangeSurfCheck as jest.MockedFunction<
    typeof meteringActions.performClientSidePageChangeSurfCheck
  >;
const getClientIp = webCommon.getClientIp as jest.MockedFunction<typeof webCommon.getClientIp>;

const fetchNearbySubregions = subregionActions.fetchNearbySubregions as jest.MockedFunction<
  typeof subregionActions.fetchNearbySubregions
>;
const fetchOverviewData = subregionActions.fetchOverviewData as jest.MockedFunction<
  typeof subregionActions.fetchOverviewData
>;
const fetchRegionalArticles = subregionActions.fetchRegionalArticles as jest.MockedFunction<
  typeof subregionActions.fetchRegionalArticles
>;
const fetchSwellEvents = swellEventsActions.fetchSwellEvents as jest.MockedFunction<
  typeof swellEventsActions.fetchSwellEvents
>;
const setSubregionLoading = subregionActions.setSubregionLoading as jest.MockedFunction<
  typeof subregionActions.setSubregionLoading
>;

describe('SubregionForecastPage', () => {
  beforeEach(() => {
    fetchNearbySubregions.mockReturnValue(async () => {});
    fetchOverviewData.mockReturnValue(async () => {});
    fetchRegionalArticles.mockReturnValue(async () => {});
    fetchSwellEvents.mockReturnValue(async () => {});
    setSubregionLoading.mockReturnValue({ type: 'SET_SUBREGION_LOADING', payload: true });
  });

  afterEach(() => {
    performClientSidePageChangeSurfCheck.mockClear();
    getClientIp.mockClear();

    fetchNearbySubregions.mockClear();
    fetchOverviewData.mockClear();
    fetchSwellEvents.mockClear();
    fetchRegionalArticles.mockClear();
  });

  afterAll(() => {
    jest.unmock('../actions/subregion');
    jest.unmock('../actions/swellEvents');
    jest.unmock('../actions/metering');
    jest.unmock('../containers/SubregionForecast');
    jest.unmock('@surfline/web-common');
  });

  it('should render the Subregion Forecast Page', () => {
    const wrapper = mount(<SubregionForecastPage />);

    const subregion = wrapper.find('.subregion');

    expect(subregion).toHaveLength(1);
  });

  it('should have a getInitialProps function', async () => {
    expect(SubregionForecastPage.getInitialProps).toBeInstanceOf(Function);
  });

  it('should return the server redux state for server requests', async () => {
    getClientIp.mockReturnValueOnce('1.2.3.4');

    const result = await SubregionForecastPage.getInitialProps!({
      req: { cookies: { token: '123' } },
      query: { subregionId: '123' },
    } as unknown as NextPageContext);

    const cookies = { token: '123' };

    expect(result.ssrReduxState).toBeInstanceOf(Object);
    expect(result.isSubregionForecastPage).toBeTruthy();

    expect(fetchOverviewData).toHaveBeenNthCalledWith(1, '123', cookies, '1.2.3.4', true, false);
    expect(fetchNearbySubregions).toHaveBeenNthCalledWith(1, '123', cookies, false);
    expect(fetchRegionalArticles).toHaveBeenNthCalledWith(1, '123', cookies);
    expect(fetchSwellEvents).toHaveBeenNthCalledWith(1, '123', 'subregion');
    expect(setSubregionLoading).toHaveBeenNthCalledWith(1, true);
  });

  it('should not return the redux state for client-side requests', async () => {
    getClientIp.mockReturnValueOnce(undefined);

    const result = await SubregionForecastPage.getInitialProps!({
      query: { subregionId: '123' },
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeUndefined();
    expect(result.isSubregionForecastPage).toBeTruthy();
    expect(fetchOverviewData).toHaveBeenNthCalledWith(1, '123', {}, undefined, true, false);
    expect(fetchNearbySubregions).toHaveBeenNthCalledWith(1, '123', {}, false);
    expect(fetchSwellEvents).toHaveBeenNthCalledWith(1, '123', 'subregion');
    expect(fetchRegionalArticles).toHaveBeenNthCalledWith(1, '123', {});
    expect(setSubregionLoading).toHaveBeenNthCalledWith(1, true);
  });
});
