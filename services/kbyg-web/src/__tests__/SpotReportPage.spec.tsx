import type { NextPageContext } from 'next';
import { expect } from '@jest/globals';
import { act } from 'react-dom/test-utils';
import * as webCommon from '@surfline/web-common';

import SpotReportPage from '../pages/surf-report/[spotSlug]/[spotId]';

import * as subregionActions from '../actions/subregion';
import * as routesActions from '../actions/routes';
import * as spotActions from '../actions/spot';
import * as meteringActions from '../actions/metering';
import * as deviceActions from '../actions/deviceData';
import { mountWithReduxAndRouter } from '../utils/test-utils';

jest.mock('../containers/SpotPage', () => {
  const SpotReport = () => <div className="spot">Spot Report Page</div>;
  return SpotReport;
});

jest.mock('@surfline/web-common');
jest.mock('../actions/subregion');
jest.mock('../actions/spot');
jest.mock('../actions/routes');
jest.mock('../actions/metering');
jest.mock('../actions/deviceData');

const performClientSidePageChangeSurfCheck =
  meteringActions.performClientSidePageChangeSurfCheck as jest.MockedFunction<
    typeof meteringActions.performClientSidePageChangeSurfCheck
  >;
const getClientIp = webCommon.getClientIp as jest.MockedFunction<typeof webCommon.getClientIp>;

const leaveSpotRoute = routesActions.leaveSpotRoute as jest.MockedFunction<
  typeof routesActions.leaveSpotRoute
>;

const fetchOverviewData = subregionActions.fetchOverviewData as jest.MockedFunction<
  typeof subregionActions.fetchOverviewData
>;
const fetchRegionalArticles = subregionActions.fetchRegionalArticles as jest.MockedFunction<
  typeof subregionActions.fetchRegionalArticles
>;

const fetchFeaturedContent = spotActions.fetchFeaturedContent as jest.MockedFunction<
  typeof spotActions.fetchFeaturedContent
>;
const fetchNearbySpots = spotActions.fetchNearbySpots as jest.MockedFunction<
  typeof spotActions.fetchNearbySpots
>;
const fetchSpotDetails = spotActions.fetchSpotDetails as jest.MockedFunction<
  typeof spotActions.fetchSpotDetails
>;
const fetchSpotReport = spotActions.fetchSpotReport as jest.MockedFunction<
  typeof spotActions.fetchSpotReport
>;
const setSpotLoading = spotActions.setSpotLoading as jest.MockedFunction<
  typeof spotActions.setSpotLoading
>;

const fetchOsData = deviceActions.fetchOSData as jest.MockedFunction<
  typeof deviceActions.fetchOSData
>;

describe('SpotReportPage', () => {
  const spotReport = { spot: { subregion: { _id: '123' } } };
  beforeEach(() => {
    leaveSpotRoute.mockReturnValue({ type: 'leaveSpotRoute' });
    fetchOverviewData.mockReturnValue(async () => {});
    fetchRegionalArticles.mockReturnValue(async () => {});
    fetchFeaturedContent.mockReturnValue(async () => {});
    fetchNearbySpots.mockReturnValue(async () => {});
    fetchSpotDetails.mockReturnValue(async () => {});
    fetchSpotReport.mockImplementation((_, cb) => async () => {
      cb?.(spotReport);
    });
    setSpotLoading.mockImplementation((loading: boolean) => ({
      type: 'SPOT_LOADING',
      payload: loading,
    }));
    fetchOsData.mockReturnValue(async () => {});
  });

  afterEach(() => {
    performClientSidePageChangeSurfCheck.mockClear();
    getClientIp.mockClear();

    leaveSpotRoute.mockClear();
    fetchOverviewData.mockClear();
    fetchRegionalArticles.mockClear();
    fetchFeaturedContent.mockClear();
    fetchNearbySpots.mockClear();
    fetchSpotDetails.mockClear();
    fetchSpotReport.mockClear();
    setSpotLoading.mockClear();
    fetchOsData.mockClear();
  });

  afterAll(() => {
    jest.unmock('@surfline/web-common');
    jest.unmock('../actions/subregion');
    jest.unmock('../actions/routes');
    jest.unmock('../actions/spot');
    jest.unmock('../actions/metering');
    jest.unmock('../actions/deviceData');
    jest.unmock('../containers/SpotPage');
  });

  it('should render the Spot Report Page', () => {
    const { wrapper } = mountWithReduxAndRouter(
      SpotReportPage,
      {},
      { router: { query: { spotId: '123' } } },
    );

    const spot = wrapper.find('.spot');

    expect(spot).toHaveLength(1);

    expect(setSpotLoading).toHaveBeenNthCalledWith(1, false);

    act(() => {
      wrapper.unmount();
    });
    expect(leaveSpotRoute).toHaveBeenCalledTimes(1);
  });

  it('should have a getInitialProps function', async () => {
    expect(SpotReportPage.getInitialProps).toBeInstanceOf(Function);
  });

  it('should return the server redux state for server requests', async () => {
    getClientIp.mockReturnValueOnce('1.2.3.4');

    const cookies = { token: '345' };
    const headers = { 'x-header': 'true' };
    const result = await SpotReportPage.getInitialProps!({
      req: { cookies, headers },
      query: { spotId: '123' },
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeInstanceOf(Object);

    expect(setSpotLoading).toHaveBeenNthCalledWith(1, true);
    expect(fetchSpotDetails).toHaveBeenNthCalledWith(1, '123', cookies);
    expect(fetchNearbySpots).toHaveBeenNthCalledWith(1, '123', cookies, '1.2.3.4', false);
    expect(fetchFeaturedContent).toHaveBeenNthCalledWith(1, cookies);
    expect(fetchSpotReport).toHaveBeenNthCalledWith(
      1,
      { spotId: '123', query: { spotId: '123' } },
      fetchSpotReport.mock.calls[0][1],
      cookies,
      '1.2.3.4',
      false,
    );
    expect(fetchOverviewData).toHaveBeenNthCalledWith(1, '123', cookies, '1.2.3.4', false, false);
    expect(fetchRegionalArticles).toHaveBeenNthCalledWith(1, '123', cookies);

    expect(fetchOsData).toHaveBeenNthCalledWith(1, headers);
  });

  it('should not return the redux state for client-side requests', async () => {
    getClientIp.mockReturnValueOnce(undefined);
    const cookies = {};

    const result = await SpotReportPage.getInitialProps!({
      query: { spotId: '123' },
    } as unknown as NextPageContext);

    expect(result.ssrReduxState).toBeUndefined();

    expect(setSpotLoading).toHaveBeenNthCalledWith(1, true);
    expect(fetchSpotDetails).toHaveBeenNthCalledWith(1, '123', cookies);
    expect(fetchNearbySpots).toHaveBeenNthCalledWith(1, '123', cookies, undefined, false);
    expect(fetchFeaturedContent).toHaveBeenNthCalledWith(1, cookies);
    expect(fetchSpotReport).toHaveBeenNthCalledWith(
      1,
      { spotId: '123', query: { spotId: '123' } },
      fetchSpotReport.mock.calls[0][1],
      cookies,
      undefined,
      false,
    );
    expect(fetchOverviewData).toHaveBeenNthCalledWith(1, '123', cookies, undefined, false, false);
    expect(fetchRegionalArticles).toHaveBeenNthCalledWith(1, '123', cookies);

    expect(fetchOsData).not.toHaveBeenCalled();
  });
});
