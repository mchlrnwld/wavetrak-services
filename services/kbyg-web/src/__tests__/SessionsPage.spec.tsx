import type { NextPageContext } from 'next';
import { expect } from 'chai';
import { mount } from 'enzyme';
import SessionsPage from '../pages/sessions/[sessionId]';

jest.mock('../containers/Sessions', () => {
  const Sessions = () => <div className="sessions">Sessions</div>;
  return Sessions;
});

describe('pages / Sessions', () => {
  afterAll(() => {
    jest.unmock('../containers/Sessions');
  });

  it('Should render the Sessions Page in an Error Boundary', () => {
    const wrapper = mount(<SessionsPage />);

    const eb = wrapper.find('ErrorBoundary');
    const sessions = wrapper.find('.sessions');

    expect(eb).to.have.length(1);
    expect(sessions).to.have.length(1);
  });

  it('should have a getInitialProps function', async () => {
    expect(SessionsPage.getInitialProps).to.be.a('function');
  });

  it('should return the server redux state for server requests', async () => {
    const result = await SessionsPage.getInitialProps!({ req: {} } as NextPageContext);

    expect(result.ssrReduxState).to.be.an('object');
  });

  it('should not return the redux state for client-side requests', async () => {
    const result = await SessionsPage.getInitialProps!({} as NextPageContext);

    expect(result.ssrReduxState).to.not.exist();
  });
});
