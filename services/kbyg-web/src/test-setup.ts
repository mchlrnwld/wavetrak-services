/* eslint-disable import/no-extraneous-dependencies */
import chai from 'chai';
import dirtyChai from 'dirty-chai';
import sinonChai from 'sinon-chai';

import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import sinon from 'sinon';
import chaiSubset from 'chai-subset';

Enzyme.configure({ adapter: new Adapter() });

chai.use(dirtyChai);
chai.use(sinonChai);
chai.use(chaiSubset);

window.analytics = {
  page: sinon.stub(),
  track: sinon.stub(),
  trackLink: sinon.stub(),
} as any as SegmentAnalytics.AnalyticsJS;

window.googletag = {
  cmd: {
    push: sinon.spy(),
  },
  destroySlots: sinon.spy(),
} as any as typeof googletag;

jest.mock('next/config', () => () => ({
  publicRuntimeConfig: {
    APP_ENV: 'test',
  },
}));
