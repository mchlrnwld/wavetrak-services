import { getClientIp, getTreatment } from '@surfline/web-common';
import { NextPage } from 'next';
import React from 'react';

import {
  fetchNearbySubregions,
  fetchOverviewData,
  fetchRegionalArticles,
  setSubregionLoading,
} from '../../../../actions/subregion';
import SubregionForecast from '../../../../containers/SubregionForecast';
import { getOrCreateStore, ReduxInitialProps } from '../../../../stores';
import { performClientSidePageChangeSurfCheck } from '../../../../actions/metering';
import { fetchSwellEvents } from '../../../../actions/swellEvents';
import { WavetrakPageContext } from '../../../../types/wavetrak';
import { SL_WEB_CONDITION_VARIATION } from '../../../../common/treatments';

const SubregionForecastPage: NextPage<
  {},
  ReduxInitialProps & { isSubregionForecastPage: true }
> = () => <SubregionForecast />;

SubregionForecastPage.getInitialProps = async (context: WavetrakPageContext) => {
  const store = getOrCreateStore();
  const { dispatch } = store;
  const { req, query } = context;

  const subregionId = query.subregionId as string;

  const cookies = req?.cookies || {};

  const clientIp = getClientIp(context.req);

  const isServer = !!req;
  const colorTreatment = getTreatment(SL_WEB_CONDITION_VARIATION);
  const sevenRatings = colorTreatment === 'on';

  await performClientSidePageChangeSurfCheck(store, isServer);

  await dispatch(setSubregionLoading(true));

  await Promise.all([
    dispatch(fetchOverviewData(subregionId, cookies, clientIp, true, sevenRatings)),
    dispatch(fetchNearbySubregions(subregionId, cookies, sevenRatings)),
    dispatch(fetchRegionalArticles(subregionId, cookies)),
    dispatch(fetchSwellEvents(subregionId, 'subregion')),
  ]);

  if (isServer) {
    dispatch(setSubregionLoading(false));

    return {
      ssrReduxState: store.getState(),
      isSubregionForecastPage: true,
    };
  }

  return {
    isSubregionForecastPage: true,
  };
};

export default SubregionForecastPage;
