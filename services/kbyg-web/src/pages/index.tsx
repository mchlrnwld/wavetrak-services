/* istanbul ignore file */
/* eslint-disable @next/next/no-html-link-for-pages */
import React from 'react';
import { NextPage } from 'next';
import { getOrCreateStore } from '../stores';
import WavetrakLink from '../components/WavetrakLink';

const Base: NextPage = () => (
  <div
    style={{
      display: 'flex',
      flexFlow: 'column',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
    <WavetrakLink style={{ margin: '16px' }} href="/surf-reports-forecasts-cams">
      Cams and Reports
    </WavetrakLink>
    <WavetrakLink href="/rl-test">Rate Limit Test Page</WavetrakLink>
  </div>
);

Base.getInitialProps = async (ctx) => {
  const store = getOrCreateStore();
  const isServer = !!ctx.req;

  if (isServer) {
    return {
      ssrReduxState: store.getState(),
    };
  }
  return {};
};

export default Base;
