/* istanbul ignore file */
import React from 'react';
import { NextPage } from 'next';
import { getOrCreateStore } from '../stores';
import RateLimitTestPage from '../containers/RateLimitTestPage';
import { performClientSidePageChangeSurfCheck } from '../actions/metering';

const RlTestPage: NextPage = () => <RateLimitTestPage />;

RlTestPage.getInitialProps = async (ctx) => {
  const store = getOrCreateStore();

  const isServer = !!ctx.req;

  await performClientSidePageChangeSurfCheck(store, isServer);

  if (isServer) {
    return {
      ssrReduxState: store.getState(),
    };
  }
  return {};
};

export default RlTestPage;
