import React from 'react';
import { NextPage } from 'next';

import CamsAndForecasts from '../containers/CamsAndForecasts';
import { getOrCreateStore, ReduxInitialProps } from '../stores';
import { fetchWorldTaxonomy } from '../actions/worldTaxonomy';
import { performClientSidePageChangeSurfCheck } from '../actions/metering';
import { WavetrakPageContext } from '../types/wavetrak';

const CamsAndForecastsPage: NextPage<{}, ReduxInitialProps> = () => <CamsAndForecasts />;

CamsAndForecastsPage.getInitialProps = async (context: WavetrakPageContext) => {
  const store = getOrCreateStore();

  const { req } = context;
  const cookies = req?.cookies || {};

  const isServer = !!req;

  await performClientSidePageChangeSurfCheck(store, isServer);
  await store.dispatch(fetchWorldTaxonomy(cookies));

  if (isServer) {
    return {
      ssrReduxState: store.getState(),
    };
  }

  return {};
};

export default CamsAndForecastsPage;
