/* istanbul ignore file */
/* eslint-disable react/no-danger */
import newrelic from 'newrelic';
import Document, { Head, Main, NextScript, Html } from 'next/document';
import classNames from 'classnames';
import {
  renderFetch,
  getClientIp,
  googletagservices,
  consentManagerConfig,
} from '@surfline/web-common';

import NewRelicBrowser from '../components/NewRelicBrowser';
import computeShowMeterRegWall from '../utils/computeShowMeterRegWall';
import config from '../config';
import { Backplane, getBackplaneRenderOpts } from '../utils/backplane';
import { JW_PLAYER_URL, LEAFLET_URL } from '../common/constants';
import segment from '../utils/segment';
import { WavetrakContext } from '../types/wavetrak';

const getBodyClass = (isPremium: boolean) =>
  classNames({
    'htl-usertype-premium': isPremium,
  });

const getPageContainerClass = (native: boolean) =>
  classNames({
    'quiver-page-container__content': true,
    'quiver-page-container__content--native': native,
  });

interface DocProps {
  backplane: Backplane;
  showRegWall: boolean;
  showFooter: boolean;
}

class MyDocument extends Document<DocProps> {
  static async getInitialProps(ctx: WavetrakContext) {
    const { req, err } = ctx;

    if (err) {
      newrelic.noticeError(err);
    }

    const { cookies } = req;

    const path = ctx.asPath!;

    const backplaneOpts = getBackplaneRenderOpts(path!, ctx.query);

    const backplane: Backplane = await renderFetch(
      process.env.BACKPLANE_HOST!,
      getClientIp(req) || '',
      {
        ...(req.userId ? { userId: req.userId } : null),
        ...(cookies.ajs_anonymous_id
          ? { anonymousId: JSON.parse(cookies.ajs_anonymous_id) }
          : null),
        accessToken: req.cookies.access_token ?? null,
        bundle: 'margins',
        apiVersion: 'v2',
        ...backplaneOpts,
      },
    );

    const showRegWall = computeShowMeterRegWall(path);
    const showFooter = !(path.indexOf('/sessions') > -1 && path.indexOf('/sessions/clips') === -1);

    const originalRenderPage = ctx.renderPage;

    ctx.renderPage = () =>
      originalRenderPage({
        // eslint-disable-next-line react/display-name
        enhanceApp: (App: any) => (props) =>
          (
            // eslint-disable-next-line react/jsx-props-no-spreading
            <App {...props} backplaneServerReduxState={backplane.data.redux} />
          ),
        enhanceComponent: (Component) => Component,
      });

    const initialProps = await Document.getInitialProps(ctx);

    return { ...initialProps, backplane, showRegWall, showFooter };
  }

  render() {
    const { backplane, showRegWall, showFooter } = this.props;

    const native = backplane.data.api.renderOptions.hideNav === 'true';
    return (
      <Html lang="en" data-sl-sha={process.env.APP_VERSION}>
        <Head>
          <script dangerouslySetInnerHTML={{ __html: `window.wavetrakNextApp = 'kbyg'` }} />
          <script
            dangerouslySetInnerHTML={{
              __html: `
                // create the consent-manager
                var cm = document.createElement('div');
                cm.setAttribute('id', 'consent-manager');
                document.documentElement.appendChild(cm);
          `,
            }}
          />
          <script dangerouslySetInnerHTML={{ __html: segment() }} />
          <script
            dangerouslySetInnerHTML={{
              __html: googletagservices(false),
            }}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: consentManagerConfig(config.appKeys.segment, false),
            }}
          />
          {/* eslint-disable-next-line @next/next/no-sync-scripts */}
          <script
            src="https://unpkg.com/@segment/consent-manager@5.0.0/standalone/consent-manager.js"
            crossOrigin="anonymous"
            integrity="sha384-Faksh8FKgpBzGdQLDwKe5L9/6YZmofEhEQRwRhM/rUvcJ2fFTEnF6Y3SCjLMTne/"
          />
          <link rel="preconnect" href={config.cdnUrl} />
          <link rel="preconnect" href={config.servicesURL} />
          <link rel="preconnect" href="https://www.gravatar.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link rel="preconnect" href="https://surfline.zendesk.com" />
          <link rel="preconnect" href="https://unpkg.com" />
          <link rel="preconnect" href="https://htlbid.com" />
          <link rel="stylesheet" type="text/css" href={config.htl.cssUrl} />
          {/* eslint-disable-next-line @next/next/no-sync-scripts */}
          <script src={config.htl.scriptUrl} />
          <script
            dangerouslySetInnerHTML={{
              __html: `
                window.htlbid = window.htlbid || {cmd: []};
              `,
            }}
          />
          <link
            rel="preload"
            as="script"
            crossOrigin="anonymous"
            href={config.react.src}
            integrity={config.react.integrity}
          />
          <link
            rel="preload"
            as="script"
            crossOrigin="anonymous"
            href={config.reactDOM.src}
            integrity={config.reactDOM.integrity}
          />
          <link rel="preload" as="script" href={JW_PLAYER_URL} />
          <link rel="preload" as="script" href={backplane.data.js} />
          {
            // in development CSS is not inlined
            backplane.data.css && (
              <>
                <link rel="preload" as="style" type="text/css" href={backplane.data.css} />
                <link rel="stylesheet" type="text/css" href={backplane.data.css} />
              </>
            )
          }
          <link
            rel="preload"
            as="style"
            type="text/css"
            href={LEAFLET_URL}
            integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
            crossOrigin="anonymous"
          />
          <link
            rel="stylesheet"
            href={LEAFLET_URL}
            integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
            crossOrigin="anonymous"
          />
          <meta property="fb:app_id" content="218714858155230" />
          <meta property="fb:page_id" content="255110695512" />
          <meta property="og:site_name" content="Surfline" />
          <link
            href="https://wa.cdn-surfline.com/quiver/0.4.0/apple/surfline-apple-touch-icon.png"
            rel="apple-touch-icon"
          />
          <meta
            name="apple-itunes-app"
            content="app-id=393782096,affiliate-data=at=1000lb9Z&ct=surfline-website-smart-banner&pt=261378"
          />
        </Head>
        <body
          className={getBodyClass(
            !!backplane?.data?.redux?.backplane?.user?.entitlements.includes('sl_premium'),
          )}
        >
          <NewRelicBrowser />
          <div className="quiver-page-container">
            <div
              id="backplane-header"
              dangerouslySetInnerHTML={{ __html: backplane.data.components.header }}
            />
            <main className={getPageContainerClass(native)}>
              <Main />
            </main>
            <div
              id="backplane-footer"
              style={{ display: showFooter ? 'block' : 'none' }}
              dangerouslySetInnerHTML={{ __html: backplane.data.components.footer }}
            />
            {showRegWall && (
              <div
                id="backplane-regwall"
                dangerouslySetInnerHTML={{ __html: backplane.data.components.regwall }}
              />
            )}
          </div>
          {/* eslint-disable-next-line @next/next/no-sync-scripts */}
          <script src={JW_PLAYER_URL} />
          {/* eslint-disable-next-line @next/next/no-sync-scripts */}
          <script
            crossOrigin="anonymous"
            src={config.react.src}
            integrity={config.react.integrity}
          />
          {/* eslint-disable-next-line @next/next/no-sync-scripts */}
          <script
            crossOrigin="anonymous"
            src={config.reactDOM.src}
            integrity={config.reactDOM.integrity}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
                window.vendor = {}
                window.vendor.React = window.React;
                window.vendor.ReactDOM = window.ReactDOM;
                window.__BACKPLANE_API__ = ${JSON.stringify(backplane.data.api)};
                window.__BACKPLANE_REDUX__ = ${JSON.stringify(backplane.data.redux)};
              `,
            }}
          />
          <NextScript />
          <script defer src={backplane.data.js} />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
