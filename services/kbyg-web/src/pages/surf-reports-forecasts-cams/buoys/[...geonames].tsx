import React from 'react';
import { NextPage } from 'next';
import { getOrCreateStore, ReduxInitialProps } from '../../../stores';

import BuoyGeonameMap from '../../../containers/BuoyGeonameMap';
import { fetchLocationView } from '../../../actions/mapV2';

const BuoyGeonameMapPage: NextPage<{}, ReduxInitialProps & { isMapV2Page: true }> = () => (
  <BuoyGeonameMap />
);

BuoyGeonameMapPage.getInitialProps = async (context) => {
  const store = getOrCreateStore();
  const { geonames } = context.query as { geonames: string[] };

  // The geoname ID should always be last in the URL.
  const geonameId = geonames[geonames.length - 1];

  await store.dispatch(fetchLocationView(geonameId));

  const isServer = !!context.req;

  if (isServer) {
    return {
      ssrReduxState: store.getState(),
      isMapV2Page: true,
    };
  }

  return {
    isMapV2Page: true,
  };
};

export default BuoyGeonameMapPage;
