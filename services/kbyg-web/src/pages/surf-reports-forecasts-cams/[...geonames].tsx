import React from 'react';
import { NextPage } from 'next';
import { getWindow, trackNavigatedToPage } from '@surfline/web-common';
import { useMount, useUnmount } from 'react-use';
import { getOrCreateStore, ReduxInitialProps } from '../../stores';
import { useAppDispatch } from '../../stores/hooks';

import GeonameMap from '../../containers/GeonameMap';
import { fetchLocationView } from '../../actions/map';
import useMetering from '../../common/hooks/useMetering';
import { leaveGeonameMapRoute } from '../../actions/routes';
import { performClientSidePageChangeSurfCheck } from '../../actions/metering';

const GeonameMapPage: NextPage<{}, ReduxInitialProps> = () => {
  const dispatch = useAppDispatch();
  const meter = useMetering();

  // When the map page mounts with the location in the URL, we want to sync it to the store and
  // session storage.
  useMount(() => {
    const win = getWindow();
    if (win) {
      win.document.body.classList.add('sl-hide-ze-widget', 'sl-map-page');

      trackNavigatedToPage('Map', {
        meteringEnabled: meter.meteringEnabled,
        category: 'cams & forecasts',
        channel: 'cams & forecasts',
      });
    }
  });

  useUnmount(() => {
    const win = getWindow();
    win.document.body.classList.remove('sl-hide-ze-widget', 'sl-map-page');
    dispatch(leaveGeonameMapRoute());
  });

  return <GeonameMap />;
};

GeonameMapPage.getInitialProps = async (context) => {
  const store = getOrCreateStore();
  const { geonames } = context.query as { geonames: string[] };

  // The geoname ID should always be last in the URL.
  const geonameId = geonames[geonames.length - 1];

  const isServer = !!context.req;

  await performClientSidePageChangeSurfCheck(store, isServer);

  await store.dispatch(fetchLocationView(geonameId));

  if (isServer) {
    return {
      ssrReduxState: store.getState(),
    };
  }

  return {};
};

export default GeonameMapPage;
