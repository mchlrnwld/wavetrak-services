/* istanbul ignore file */
import type { NextApiRequest, NextApiResponse } from 'next';

type Response = {
  version: string;
  message: 'OK';
};

const handler = (_req: NextApiRequest, res: NextApiResponse<Response>) => {
  res.status(200).json({ version: process.env.APP_VERSION || 'unknown', message: 'OK' });
};

export default handler;
