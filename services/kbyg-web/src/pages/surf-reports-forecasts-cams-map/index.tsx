import React from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { getWindow } from '@surfline/web-common';
import { useMount, useUnmount } from 'react-use';
import { getOrCreateStore, ReduxInitialProps } from '../../stores';
import { useAppDispatch } from '../../stores/hooks';

import { goToUsersNearestSpot, setMapLocation } from '../../actions/map';
import { performClientSidePageChangeSurfCheck } from '../../actions/metering';
import PageLoading from '../../components/PageLoading';
import MapPageMeta from '../../components/MapPageMeta';

const SYNC_URL = true;

const SpotMapPage: NextPage<{}, ReduxInitialProps> = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();

  // When the map page is mounted under this route, we want to redirect it to the map page with the
  // location in the url. This logic will fire on mount, and will redirect the user to the router with
  // the location in the url.
  useMount(() => {
    const win = getWindow();
    if (win) {
      win.document.body.classList.add('sl-hide-ze-widget', 'sl-map-page');

      const storageItem = win.sessionStorage.getItem('slMapLocation');
      const sessionLocation = storageItem && JSON.parse(storageItem);

      if (sessionLocation) {
        dispatch(setMapLocation(sessionLocation, router, SYNC_URL));
      } else {
        dispatch(goToUsersNearestSpot(router, SYNC_URL));
      }
    }
  });

  useUnmount(() => {
    const win = getWindow();
    win.document.body.classList.remove('sl-hide-ze-widget', 'sl-map-page');
  });

  return (
    <>
      <MapPageMeta />
      <PageLoading />
    </>
  );
};

SpotMapPage.getInitialProps = async (context) => {
  const store = getOrCreateStore();

  const isServer = !!context.req;

  await performClientSidePageChangeSurfCheck(store, isServer);

  if (isServer) {
    return {
      ssrReduxState: store.getState(),
    };
  }

  return {};
};

export default SpotMapPage;
