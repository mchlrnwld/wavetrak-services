import React from 'react';
import { NextPage } from 'next';
import { useRouter } from 'next/router';
import { getWindow, trackNavigatedToPage } from '@surfline/web-common';
import { useMount, useUnmount } from 'react-use';
import { getOrCreateStore, ReduxInitialProps } from '../../stores';
import { useAppDispatch } from '../../stores/hooks';

import Map from '../../containers/Map';
import { setMapLocation } from '../../actions/map';
import useMetering from '../../common/hooks/useMetering';
import { performClientSidePageChangeSurfCheck } from '../../actions/metering';

const SYNC_URL = true;

const SpotMapPageWithLocation: NextPage<{}, ReduxInitialProps> = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const meter = useMetering();

  // When the map page mounts with the location in the URL, we want to sync it to the store and
  // session storage.
  useMount(() => {
    const win = getWindow();
    if (win) {
      win.document.body.classList.add('sl-hide-ze-widget', 'sl-map-page');

      trackNavigatedToPage('Map', {
        meteringEnabled: meter.meteringEnabled,
        category: 'cams & forecasts',
        channel: 'cams & forecasts',
      });

      const { location } = router.query as { location: string };

      const storageItem = win.sessionStorage.getItem('slMapLocation');
      const sessionLocation = storageItem && JSON.parse(storageItem);
      const pathLocation = location?.split(',');
      const loc = pathLocation
        ? {
            center: {
              lat: Number(pathLocation[0]),
              lon: Number(pathLocation[1]),
            },
            zoom: pathLocation[2] && Number(pathLocation[2].replace('z', '')),
          }
        : sessionLocation;

      dispatch(setMapLocation(loc, router, SYNC_URL));
    }
  });

  useUnmount(() => {
    const win = getWindow();
    win.document.body.classList.remove('sl-hide-ze-widget', 'sl-map-page');
  });

  return <Map />;
};

SpotMapPageWithLocation.getInitialProps = async (context) => {
  const store = getOrCreateStore();

  const isServer = !!context.req;

  await performClientSidePageChangeSurfCheck(store, isServer);

  if (isServer) {
    return {
      ssrReduxState: store.getState(),
    };
  }

  return {};
};

export default SpotMapPageWithLocation;
