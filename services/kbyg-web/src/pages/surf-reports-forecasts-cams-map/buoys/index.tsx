import React from 'react';
import { NextPage } from 'next';
import { getOrCreateStore, ReduxInitialProps } from '../../../stores';

import BuoyMap from '../../../containers/BuoyMap';

const BuoyMapPage: NextPage<{}, ReduxInitialProps & { isMapV2Page: true }> = () => <BuoyMap />;

BuoyMapPage.getInitialProps = async (context) => {
  const isServer = !!context.req;

  if (isServer) {
    const store = getOrCreateStore();
    return {
      ssrReduxState: store.getState(),
      isMapV2Page: true,
    };
  }

  return {
    isMapV2Page: true,
  };
};

export default BuoyMapPage;
