import React from 'react';
import { NextPage } from 'next';
import { getOrCreateStore, ReduxInitialProps } from '../../../stores';

import BuoyMap from '../../../containers/BuoyMap';

const BuoyMapPageWithLocation: NextPage<{}, ReduxInitialProps & { isMapV2Page: true }> = () => (
  <BuoyMap />
);

BuoyMapPageWithLocation.getInitialProps = async (context) => {
  const isServer = !!context.req;

  if (isServer) {
    const store = getOrCreateStore();
    return {
      ssrReduxState: store.getState(),
      isMapV2Page: true,
    };
  }

  return {
    isMapV2Page: true,
  };
};

export default BuoyMapPageWithLocation;
