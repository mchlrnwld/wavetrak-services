import React, { useEffect } from 'react';
import { NextPage } from 'next';
import { getClientIp, getTreatment } from '@surfline/web-common';
import { useUnmount } from 'react-use';
import { useRouter } from 'next/router';
import { SL_WEB_CONDITION_VARIATION } from '../../../common/treatments';
import { getOrCreateStore, ReduxInitialProps } from '../../../stores';
import SpotPage from '../../../containers/SpotPage';

import {
  fetchFeaturedContent,
  fetchNearbySpots,
  fetchSpotDetails,
  fetchSpotReport,
  setSpotLoading,
} from '../../../actions/spot';
import { useAppDispatch } from '../../../stores/hooks';
import { fetchOSData } from '../../../actions/deviceData';
import { fetchOverviewData, fetchRegionalArticles } from '../../../actions/subregion';
import { performClientSidePageChangeSurfCheck } from '../../../actions/metering';
import { leaveSpotRoute } from '../../../actions/routes';
import { SpotReport } from '../../../types/spot';
import { WavetrakPageContext } from '../../../types/wavetrak';

const SpotReportPage: NextPage<{}, ReduxInitialProps> = () => {
  const dispatch = useAppDispatch();

  const router = useRouter();
  const { spotId } = router.query;

  useEffect(() => {
    dispatch(setSpotLoading(false));
  }, [spotId, dispatch]);

  useEffect(
    () => {
      const doSetSpotLoading = (_: string, { shallow }: { shallow: boolean }) => {
        if (!shallow) {
          dispatch(setSpotLoading(true));
        }
      };
      router.events.on('routeChangeStart', doSetSpotLoading);
      return () => {
        router.events.off('routeChangeStart', doSetSpotLoading);
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useUnmount(() => {
    dispatch(leaveSpotRoute());
  });

  return <SpotPage />;
};

SpotReportPage.getInitialProps = async (context: WavetrakPageContext) => {
  const store = getOrCreateStore();
  const { req, query } = context;
  const isServer = !!req;
  const { dispatch } = store;
  const spotId = query.spotId as string;

  const cookies = req?.cookies || {};

  const clientIp = getClientIp(context.req);

  const colorTreatment = getTreatment(SL_WEB_CONDITION_VARIATION);
  const sevenRatings = colorTreatment === 'on';

  await performClientSidePageChangeSurfCheck(store, isServer);

  await dispatch(setSpotLoading(true));
  await dispatch(fetchSpotDetails(spotId, cookies));

  await Promise.all([
    dispatch(fetchNearbySpots(spotId, cookies, clientIp, sevenRatings)),
    dispatch(fetchFeaturedContent(cookies)),
    dispatch(
      fetchSpotReport(
        { spotId, query: context.query },
        (spotReport: SpotReport) =>
          Promise.all([
            dispatch(fetchRegionalArticles(spotReport.spot.subregion._id, cookies)),
            dispatch(
              fetchOverviewData(
                spotReport.spot.subregion._id,
                cookies,
                clientIp,
                false,
                sevenRatings,
              ),
            ),
          ]),
        cookies,
        clientIp,
        sevenRatings,
      ),
    ),
  ]);

  if (isServer) {
    await store.dispatch(fetchOSData(context.req!.headers));
    dispatch(setSpotLoading(false));
    return {
      ssrReduxState: store.getState(),
    };
  }

  return {};
};

export default SpotReportPage;
