/* istanbul ignore file */
/* eslint-disable react/jsx-props-no-spreading */
import { useRouter } from 'next/router';
import { useRef, useEffect, useState } from 'react';
import Head from 'next/head';
import type { AppProps } from 'next/app';
import { Provider } from 'react-redux';
import { getWindow, BackplaneState } from '@surfline/web-common';

import { useRafLoop } from 'react-use';
import { useStore } from '../stores';
import setNewRelicErrorHandler from '../utils/setNewRelicErrorHandler';
import { instantiateClientSideBackplane } from '../utils/backplane';
import KBYGPageContainer from '../containers/KBYGPageContainer';
import SubregionForecastContainer from '../containers/SubregionForecastContainer';
import MapV2 from '../containers/MapV2';
import { splitReady } from '../actions/split';
import { analyticsReady } from '../actions/analytics';
import Scripts from '../components/Scripts';

import '@surfline/quiver-themes/stylesheets/normalize.css';
import '../styles/base.scss';
import 'react-loading-skeleton/dist/skeleton.css';

interface PageProps {
  ssrReduxState: any;
  isSubregionForecastPage?: boolean;
  isPremiumAnalysisPage?: boolean;
  isChartsPage?: boolean;
  isMapV2Page?: boolean;
}

export interface WavetrakAppProps extends AppProps<PageProps> {
  pageProps: PageProps;
  backplaneServerReduxState: BackplaneState;
}

setNewRelicErrorHandler();

const MyApp = ({ Component, pageProps, backplaneServerReduxState }: WavetrakAppProps) => {
  const [backplaneReady, setBackplaneReady] = useState(false);
  const {
    isMapV2Page,
    isSubregionForecastPage,
    isPremiumAnalysisPage,
    isChartsPage,
    ssrReduxState,
  } = pageProps;

  const win = getWindow();

  const state = useRef({
    ...ssrReduxState,
    // eslint-disable-next-line no-underscore-dangle
    backplane: win?.__BACKPLANE_REDUX__?.backplane || backplaneServerReduxState?.backplane,
  });

  const store = useStore(state.current);
  const router = useRouter();

  // We want to keep lookng for the backplane script to have properly executed, at which point we can
  // proceed with hydrating. We can't reliably know when it will finish since order of execution is
  // not consistent
  const [loopStop] = useRafLoop(() => {
    if (win?.wavetrakBackplaneMount) {
      setBackplaneReady(true);
    }
  }, !!win);

  useEffect(
    () => {
      if (backplaneReady) {
        loopStop();
        instantiateClientSideBackplane(
          store,
          router,
          () => store.dispatch(splitReady()),
          () => store.dispatch(analyticsReady()),
        );
      }
    },
    // We only care about when backplane is ready here and this only needs to be called once.
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [backplaneReady],
  );

  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"
        />
        <meta charSet="utf-8" />
        <meta httpEquiv="Content-Language" content="en" />
      </Head>
      <Scripts />
      <Provider store={store}>
        <KBYGPageContainer>
          {(() => {
            if (isMapV2Page) {
              return (
                <MapV2>
                  <Component {...pageProps} />
                </MapV2>
              );
            }
            if (isSubregionForecastPage || isPremiumAnalysisPage || isChartsPage) {
              return (
                <SubregionForecastContainer>
                  <Component {...pageProps} />
                </SubregionForecastContainer>
              );
            }

            return <Component {...pageProps} />;
          })()}
        </KBYGPageContainer>
      </Provider>
    </>
  );
};

export default MyApp;
