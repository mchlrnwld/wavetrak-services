import React from 'react';
import { NextPage } from 'next';
import { getOrCreateStore, ReduxInitialProps } from '../../../stores';
import CamRewind from '../../../containers/CamRewind';
import { performClientSidePageChangeSurfCheck } from '../../../actions/metering';

const CamRewindPage: NextPage<{}, ReduxInitialProps> = () => <CamRewind />;

CamRewindPage.getInitialProps = async (context) => {
  const store = getOrCreateStore();

  const isServer = !!context.req;
  await performClientSidePageChangeSurfCheck(store, isServer);

  if (isServer) {
    return {
      ssrReduxState: store.getState(),
    };
  }

  return {};
};

export default CamRewindPage;
