import React from 'react';
import { NextPage } from 'next';
import { ErrorBoundary } from '@surfline/quiver-react';

import Sessions from '../../containers/Sessions';
import { getOrCreateStore, ReduxInitialProps } from '../../stores';

const SessionsPage: NextPage<{}, ReduxInitialProps> = () => (
  <ErrorBoundary>
    <Sessions />
  </ErrorBoundary>
);

SessionsPage.getInitialProps = async (context) => {
  const isServer = !!context.req;

  if (isServer) {
    const store = getOrCreateStore();
    return {
      ssrReduxState: store.getState(),
    };
  }

  return {};
};

export default SessionsPage;
