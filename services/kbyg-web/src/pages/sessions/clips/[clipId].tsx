import React from 'react';
import { NextPage } from 'next';
import { ErrorBoundary } from '@surfline/quiver-react';

import { getClientIp } from '@surfline/web-common';
import { getOrCreateStore, ReduxInitialProps } from '../../../stores';
import SessionsClip from '../../../containers/SessionsClip';
import { fetchSessionsClip } from '../../../common/api/sessions';
import { INTERNAL_SERVER_ERROR, NOT_FOUND } from '../../../actions/status';
import { SessionClip } from '../../../types/sessions';
import { WavetrakPageContext } from '../../../types/wavetrak';

interface Props {
  sessionClip?: SessionClip;
}

const SessionsClipPage: NextPage<Props, ReduxInitialProps & { sessionClip?: SessionClip }> = ({
  sessionClip,
}) => (
  <ErrorBoundary>
    <SessionsClip sessionClip={sessionClip!} />
  </ErrorBoundary>
);

SessionsClipPage.getInitialProps = async (context: WavetrakPageContext) => {
  const store = getOrCreateStore();
  const { clipId } = context.query as { clipId: string };

  const { req } = context;
  const cookies = req?.cookies || {};

  const isServer = !!context.req;

  try {
    const sessionClip = await fetchSessionsClip(clipId, cookies, getClientIp(context.req));

    if (isServer) {
      return {
        ssrReduxState: store.getState(),
        sessionClip,
      };
    }
    return { sessionClip };
  } catch (err) {
    const error = err as { statusCode: number };
    if (error.statusCode === 404) {
      store.dispatch({
        type: NOT_FOUND,
        message: 'The clip was not found.',
      });
    } else {
      store.dispatch({
        type: INTERNAL_SERVER_ERROR,
        message: 'The server could not return the Session Clip.',
      });
    }
  }

  if (isServer) {
    return {
      ssrReduxState: store.getState(),
    };
  }
  return {};
};

export default SessionsClipPage;
