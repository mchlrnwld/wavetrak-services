import React from 'react';
import { NextPage } from 'next';
import { getOrCreateStore, ReduxInitialProps } from '../../../stores';
import Buoy from '../../../containers/Buoy';
import { serverSideFetchBuoyDetails } from '../../../actions/buoys';

const BuoyPage: NextPage<{}, ReduxInitialProps> = () => <Buoy />;

BuoyPage.getInitialProps = async (context) => {
  const { buoyId } = context.query;
  const isServer = !!context.req;

  if (isServer) {
    const store = getOrCreateStore();
    await store.dispatch(serverSideFetchBuoyDetails(buoyId as string));
    return {
      ssrReduxState: store.getState(),
    };
  }

  return {};
};

export default BuoyPage;
