import { getClientIp, getTreatment } from '@surfline/web-common';
import { NextPage } from 'next';
import React from 'react';

import Charts from '../../../../containers/Charts';

import { setActiveChart, fetchChartTypes } from '../../../../actions/charts';
import { performClientSidePageChangeSurfCheck } from '../../../../actions/metering';
import {
  fetchNearbySubregions,
  fetchOverviewData,
  fetchRegionalArticles,
} from '../../../../actions/subregion';
import { fetchSwellEvents } from '../../../../actions/swellEvents';
import { getSubregionId } from '../../../../selectors/subregion';
import { getOrCreateStore, ReduxInitialProps } from '../../../../stores';
import { WavetrakPageContext } from '../../../../types/wavetrak';
import { SL_WEB_CONDITION_VARIATION } from '../../../../common/treatments';

const ChartsPage: NextPage<{}, ReduxInitialProps & { isChartsPage: true }> = () => <Charts />;

ChartsPage.getInitialProps = async (context: WavetrakPageContext) => {
  const store = getOrCreateStore();
  const { dispatch } = store;
  const { req, query } = context;

  const subregionId = query.subregionId as string;
  const chartSlug = query.chartSlug as string;

  const cookies = req?.cookies || {};

  const clientIp = getClientIp(req);

  const isServer = !!req;

  const colorTreatment = getTreatment(SL_WEB_CONDITION_VARIATION);
  const sevenRatings = colorTreatment === 'on';
  await performClientSidePageChangeSurfCheck(store, isServer);

  const subregionChanged = getSubregionId(store.getState()) !== subregionId;
  await Promise.all(
    [
      // If this is a client-side page change and the subregion hasn't changed, we can skip fetching
      // subregion level data
      subregionChanged &&
        dispatch(fetchOverviewData(subregionId, cookies, clientIp, true, sevenRatings)),
      subregionChanged && dispatch(fetchNearbySubregions(subregionId, cookies, sevenRatings)),
      subregionChanged && dispatch(fetchRegionalArticles(subregionId, cookies)),
      subregionChanged && dispatch(fetchChartTypes(subregionId, cookies)),
      subregionChanged && dispatch(fetchSwellEvents(subregionId, 'subregion')),
      dispatch(setActiveChart(chartSlug, subregionId, cookies)),
    ].filter(Boolean),
  );

  if (isServer) {
    return {
      isChartsPage: true,
      ssrReduxState: store.getState(),
    };
  }

  return {
    isChartsPage: true,
  };
};

export default ChartsPage;
