import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { SET_MAP_LOCATION } from '../../actions/map';
import location from './location';

describe('reducers / map / location', () => {
  const initialState = deepFreeze({
    center: {},
    zoom: 12,
  });

  it('initializes with empty center and zoom level 12', () => {
    expect(location()).to.deep.equal(initialState);
  });

  it('updates center, zoom, and bounds, on SET_MAP_LOCATION', () => {
    const action = deepFreeze({
      type: SET_MAP_LOCATION,
      center: {
        lat: 33.654213041213,
        lon: -118.0032588192,
      },
      zoom: 11,
    });
    expect(location(initialState, action)).to.deep.equal({
      center: action.center,
      zoom: action.zoom,
    });
  });
});
