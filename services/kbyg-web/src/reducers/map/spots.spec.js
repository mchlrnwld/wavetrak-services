import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { FETCH_SPOT_MAP_DATA_SUCCESS } from '../../actions/spotMapData';
import spots from './spots';
import { rankToCondition } from '../../utils/conditionRank';

describe('reducers / spots', () => {
  const initialState = deepFreeze([]);

  it('initializes with an empty array', () => {
    expect(spots()).to.deep.equal(initialState);
  });

  it('updates spots with sorted spots if doCluster false on FETCH_SPOT_MAP_DATA_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_SPOT_MAP_DATA_SUCCESS,
      spots: [
        {
          _id: '5842041f4e65fad6a77088ed',
          name: 'HB Pier, Southside',
          rank: [1, 1],
        },
        {
          _id: '5842041f4e65fad6a7708927',
          name: 'Cabrillo Beach',
          rank: [0, 2],
        },
        {
          _id: '5842041f4e65fad6a7708827',
          name: 'HB Pier, Northside',
          rank: [1, 0],
        },
      ],
      doCluster: false,
    });
    expect(spots(initialState, action)).to.deep.equal([
      action.spots[1],
      action.spots[2],
      action.spots[0],
    ]);
  });

  it('returns the top 75 spots if doCluster true on FETCH_SPOT_MAP_DATA_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_SPOT_MAP_DATA_SUCCESS,
      spots: [...Array(100)].map((_, i) => ({
        _id: `spot${i}`,
        conditions: { value: rankToCondition(i % 10) },
        waveHeight: { max: i },
      })),
      doCluster: true,
    });
    const result = spots(initialState, action);
    expect(result).to.have.length(75);
    expect(result[0].conditions.value).to.equal('EPIC');
  });

  it('sorts spots by condition then wave height if doCluster true on FETCH_SPOT_MAP_DATA_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_SPOT_MAP_DATA_SUCCESS,
      spots: [...Array(10)].map((_, i) => ({
        _id: `spot${i}`,
        conditions: { value: rankToCondition(i % 2) },
        waveHeight: { max: i },
      })),
      doCluster: true,
    });
    const result = spots(initialState, action);
    expect(result[0].conditions.value).to.equal('VERY_POOR');
    expect(result[1].conditions.value).to.equal('VERY_POOR');
    expect(result[2].conditions.value).to.equal('VERY_POOR');
    expect(result[3].conditions.value).to.equal('VERY_POOR');
    expect(result[4].conditions.value).to.equal('VERY_POOR');
    expect(result[5].conditions.value).to.equal('FLAT');
    expect(result[6].conditions.value).to.equal('FLAT');
    expect(result[7].conditions.value).to.equal('FLAT');
    expect(result[8].conditions.value).to.equal('FLAT');
    expect(result[9].conditions.value).to.equal('FLAT');
    expect(result[0].waveHeight.max).to.be.above(result[1].waveHeight.max);
    expect(result[1].waveHeight.max).to.be.above(result[2].waveHeight.max);
    expect(result[2].waveHeight.max).to.be.above(result[3].waveHeight.max);
    expect(result[3].waveHeight.max).to.be.above(result[4].waveHeight.max);
    expect(result[5].waveHeight.max).to.be.above(result[6].waveHeight.max);
    expect(result[6].waveHeight.max).to.be.above(result[7].waveHeight.max);
    expect(result[7].waveHeight.max).to.be.above(result[8].waveHeight.max);
    expect(result[8].waveHeight.max).to.be.above(result[9].waveHeight.max);
  });
});
