import { expect } from 'chai';
import sinon from 'sinon';
import deepFreeze from 'deep-freeze';
import { FETCH_SPOT_MAP_DATA_SUCCESS } from '../../actions/spotMapData';
import * as cluster from '../../utils/cluster/cluster';
import clusters, { properties } from './clusters';

describe('reducers / map / clusters', () => {
  const initialState = deepFreeze([]);

  beforeEach(() => {
    sinon.stub(cluster, 'default');
  });

  afterEach(() => {
    cluster.default.restore();
  });

  it('initializes with an empty array', () => {
    expect(clusters()).to.deep.equal(initialState);
    expect(cluster.default).not.to.have.been.called();
  });

  it('reinitializes if doCluster false on FETCH_SPOT_MAP_DATA_SUCCESS', () => {
    const state = deepFreeze([
      {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [-118.0192012569068, 33.66704400178788],
        },
        properties: { count: 4 },
      },
      {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [-118.08982, 33.728733],
        },
        properties: { count: 3 },
      },
    ]);
    const action = deepFreeze({
      type: FETCH_SPOT_MAP_DATA_SUCCESS,
      zoom: 12,
      spots: [
        {
          _id: '5842041f4e65fad6a77088ed',
          lat: 33.654213041213,
          lon: -118.0032588192,
          name: 'HB Pier, Southside',
        },
        {
          _id: '5842041f4e65fad6a7708827',
          lat: 33.656781041213,
          lon: -118.0064678192,
          name: 'HB Pier, Northside',
        },
      ],
      doCluster: false,
    });
    expect(clusters(state, action)).to.deep.equal(initialState);
    expect(cluster.default).not.to.have.been.called();
  });

  it('sets clusters from spots doCluster true on FETCH_SPOT_MAP_DATA_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_SPOT_MAP_DATA_SUCCESS,
      zoom: 11,
      spots: [
        {
          _id: '5842041f4e65fad6a77088ed',
          lat: 33.654213041213,
          lon: -118.0032588192,
          name: 'HB Pier, Southside',
        },
        {
          _id: '5842041f4e65fad6a7708827',
          lat: 33.656781041213,
          lon: -118.0064678192,
          name: 'HB Pier, Northside',
        },
      ],
      doCluster: true,
    });
    const expectedState = [
      {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [-118.0048633182, 33.655497041213],
        },
        properties: { count: 2 },
      },
    ];
    cluster.default.returns(expectedState);

    expect(clusters(initialState, action)).to.deep.equal(expectedState);
    expect(cluster.default).to.have.been.calledOnce();
    expect(cluster.default).to.have.been.calledWithExactly(
      [
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [action.spots[0].lon, action.spots[0].lat],
          },
          properties: action.spots[0],
        },
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [action.spots[1].lon, action.spots[1].lat],
          },
          properties: action.spots[1],
        },
      ],
      11,
      {
        radius: 100,
        properties,
      },
    );
  });

  describe('properties', () => {
    it('builds spotCount, worstCondition, and bestCondition from points', () => {
      const points = [
        {
          properties: {
            conditions: { value: 'VERY_GOOD' },
          },
        },
        {
          properties: {
            conditions: { value: 'FAIR' },
          },
        },
        {
          properties: {
            conditions: { value: null },
          },
        },
        {
          properties: {
            conditions: { value: 'FAIR_TO_GOOD' },
          },
        },
      ];
      expect(properties(points)).to.deep.equal({
        spotCount: 4,
        worstCondition: 'FAIR',
        bestCondition: 'VERY_GOOD',
      });
    });
  });
});
