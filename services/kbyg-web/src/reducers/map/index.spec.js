import { expect } from 'chai';
import map from '.';

describe('reducers / map', () => {
  it('combines bounds, clusters, and location', () => {
    const mapState = map();
    expect(mapState.clusters).to.be.ok();
    expect(mapState.location).to.be.ok();
    expect(mapState.spots).to.be.ok();
    expect(mapState.status).to.be.ok();
  });
});
