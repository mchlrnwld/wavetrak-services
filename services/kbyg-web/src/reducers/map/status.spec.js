import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  FETCH_SPOT_MAP_DATA,
  FETCH_SPOT_MAP_DATA_SUCCESS,
  FETCH_SPOT_MAP_DATA_FAILURE,
} from '../../actions/spotMapData';
import status from './status';

describe('reducers / status', () => {
  const initialState = deepFreeze({
    loading: true,
    error: null,
  });

  it('initializes with loading false and error null', () => {
    expect(status()).to.deep.equal(initialState);
  });

  it('sets loading true and error null on FETCH_SPOT_MAP_DATA', () => {
    const action = deepFreeze({ type: FETCH_SPOT_MAP_DATA });
    expect(status(initialState, action)).to.deep.equal({
      loading: true,
      error: null,
    });
  });

  it('sets loading false and error null on FETCH_SPOT_MAP_DATA_SUCCESS', () => {
    const state = deepFreeze({
      loading: true,
      error: 'Some error message',
    });
    const action = deepFreeze({ type: FETCH_SPOT_MAP_DATA_SUCCESS });
    expect(status(state, action)).to.deep.equal({
      loading: false,
      error: null,
    });
  });

  it('sets loading false and error on FETCH_SPOT_MAP_DATA_FAILURE', () => {
    const state = deepFreeze({
      loading: true,
      error: null,
    });
    const action = deepFreeze({
      type: FETCH_SPOT_MAP_DATA_FAILURE,
      error: 'Some error message',
    });
    expect(status(state, action)).to.deep.equal({
      loading: false,
      error: action.error,
    });
  });
});
