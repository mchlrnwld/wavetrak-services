import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { FETCH_SPOT_MAP_DATA_SUCCESS } from '../actions/spotMapData';
import units from './units';

describe('reducers / units', () => {
  const initialState = {
    tideHeight: null,
    waveHeight: null,
    windSpeed: null,
  };

  it('initializes tideHeight, waveHeight, windSpeed null', () => {
    expect(units()).to.deep.equal(initialState);
  });

  it('sets tideHeight, waveHeight, and windSpeed on FETCH_SPOT_MAP_DATA_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_SPOT_MAP_DATA_SUCCESS,
      units: {
        tideHeight: 'FT',
        waveHeight: 'FT',
        windSpeed: 'KT',
      },
    });
    expect(units(initialState, action)).to.deep.equal(action.units);
  });
});
