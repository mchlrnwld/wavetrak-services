import { createReducer } from '@reduxjs/toolkit';
import { FETCH_WORLD_TAXONOMY_SUCCESS } from '../actions/worldTaxonomy';
import { WorldTaxonomy } from '../types/locationView';

type WorldTaxonomyState = WorldTaxonomy[];
export const initialState: WorldTaxonomyState = [];

export default createReducer(initialState, (builder) => {
  builder.addCase<
    typeof FETCH_WORLD_TAXONOMY_SUCCESS,
    { taxonomy: WorldTaxonomy[]; type: typeof FETCH_WORLD_TAXONOMY_SUCCESS }
  >(FETCH_WORLD_TAXONOMY_SUCCESS, (_, { taxonomy }) => taxonomy);
});
