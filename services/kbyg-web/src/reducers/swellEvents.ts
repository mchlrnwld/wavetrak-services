import { createReducer } from '@reduxjs/toolkit';
import {
  FETCH_SWELL_EVENTS,
  FETCH_SWELL_EVENTS_SUCCESS,
  FETCH_SWELL_EVENTS_FAILURE,
} from '../actions/swellEvents';
import { SwellEvent } from '../types/swellEvents';

interface SwellEventsState {
  events: SwellEvent[];
  loading: boolean;
  error: string | null;
  success: boolean;
}

export const initialState: SwellEventsState = {
  error: null,
  events: [],
  loading: false,
  success: false,
};

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(FETCH_SWELL_EVENTS, () => ({
    ...initialState,
    loading: true,
  }));
  builder.addCase<
    typeof FETCH_SWELL_EVENTS_SUCCESS,
    { events: SwellEvent[]; type: typeof FETCH_SWELL_EVENTS_SUCCESS }
  >(FETCH_SWELL_EVENTS_SUCCESS, (state, { events }) => ({
    ...state,
    events,
    loading: false,
    success: true,
  }));

  builder.addCase<
    typeof FETCH_SWELL_EVENTS_FAILURE,
    { error: string; type: typeof FETCH_SWELL_EVENTS_FAILURE }
  >(FETCH_SWELL_EVENTS_FAILURE, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  }));
});

export default reducer;
