import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  setMapBoundingBox,
  setMapError,
  setMapLoading,
  setMapLocation,
  setMapLocationLoaded,
  setMapMessage,
  fetchLocationView,
  resetMapState,
  zoomIn,
} from '../actions/mapV2';
import mapV2, { initialState as reducerInitialState } from './mapV2';
import stateFixture from '../selectors/fixtures/state';

describe('reducers / mapV2', () => {
  const initialState = deepFreeze(reducerInitialState);

  it('initializes with empty center and zoom level 12', () => {
    expect(mapV2(initialState, { type: 'test' })).to.deep.equal(initialState);
  });

  it('updates center, zoom, and bounds, on SET_MAP_LOCATION', () => {
    const action = deepFreeze(setMapLocation({ center: { lat: 1, lon: 1 }, zoom: 2 }));
    expect(mapV2(initialState, action)).to.containSubset({
      location: {
        center: action.payload.center,
        zoom: action.payload.zoom,
      },
      locationLoaded: true,
    });
  });

  it('should set locationLoaded on SET_MAP_LOCATION_LOADED', () => {
    const action = deepFreeze(setMapLocationLoaded());
    expect(mapV2(initialState, action)).to.containSubset({
      locationLoaded: true,
    });
  });

  it('updates bounding box on SET_MAP_BOUNDING_BOX', () => {
    const action = deepFreeze(setMapBoundingBox({ north: 1, south: -1, west: -1, east: 1 }));
    expect(mapV2(initialState, action)).to.containSubset({
      boundingBox: {
        north: 1,
        south: -1,
        west: -1,
        east: 1,
      },
    });
  });

  it('updates error on SET_MAP_ERROR', () => {
    const action = deepFreeze(setMapError({ message: 'test', status: 500 }));
    expect(mapV2(initialState, action)).to.containSubset({
      error: {
        message: 'test',
        status: 500,
      },
    });
  });

  it('clear error on SET_MAP_ERROR with no payload', () => {
    const action = deepFreeze(setMapError());
    expect(mapV2(initialState, action)).to.containSubset({
      error: null,
    });
  });

  it('should set loading on SET_MAP_LOADING', () => {
    const action = deepFreeze(setMapLoading(true));
    expect(mapV2(initialState, action)).to.containSubset({
      loading: true,
    });
  });

  it('should set the message on SET_MAP_MESSAGE', () => {
    const action = deepFreeze(setMapMessage('test message'));
    expect(mapV2(initialState, action)).to.containSubset({
      message: 'test message',
    });
  });

  it('should set a locationView loading state', () => {
    const action = deepFreeze({ type: fetchLocationView.pending.toString() });
    expect(mapV2(initialState, action)).to.containSubset({
      locationView: {
        loading: true,
      },
    });
  });

  it('should set a locationView state', () => {
    const action = deepFreeze({
      type: fetchLocationView.fulfilled.toString(),
      payload: stateFixture.mapV2.locationView,
    });
    expect(mapV2(initialState, action)).to.containSubset({
      locationView: stateFixture.mapV2.locationView,
    });
  });

  it('should set a locationView error state', () => {
    const action = deepFreeze({
      type: fetchLocationView.rejected.toString(),
      error: { message: 'something went wrong' },
    });
    expect(mapV2(initialState, action)).to.containSubset({
      locationView: {
        loading: false,
        error: { message: 'something went wrong' },
      },
    });
  });

  it('should reset the map state', () => {
    const action = deepFreeze(resetMapState());
    expect(
      mapV2({ ...initialState, locationView: { someLocationView: true } }, action),
    ).to.deep.equal(initialState);
  });

  it('should zoom the map on the current location', () => {
    const action = deepFreeze(zoomIn());
    expect(mapV2(initialState, action).location.zoom).to.deep.equal(13);
  });

  it('should zoom the map onto the location', () => {
    const action = deepFreeze(zoomIn({ lat: 20, lon: 30 }));
    expect(mapV2(initialState, action).location).to.deep.equal({
      ...initialState.location,
      center: { lat: 20, lon: 30 },
      zoom: 13,
    });
  });
});
