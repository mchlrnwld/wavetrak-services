import { createReducer } from '@reduxjs/toolkit';
import { splitReady } from '../actions/split';

type SplitState = {
  ready: boolean;
};
export const initialState: SplitState = {
  ready: false,
};

const reducer = createReducer(initialState, (builder) =>
  builder.addCase(splitReady, () => ({ ready: true })),
);

export default reducer;
