import createReducer from '../createReducer';
import {
  FETCH_SPOT_DETAILS,
  FETCH_SPOT_DETAILS_SUCCESS,
  FETCH_SPOT_DETAILS_FAILURE,
} from '../../actions/spot';
import { LEAVE_SPOT_ROUTE } from '../../actions/routes';

export const initialState = {};

const handlers = {};

handlers[FETCH_SPOT_DETAILS] = (state) => ({
  ...state,
  data: null,
  loading: true,
});

handlers[FETCH_SPOT_DETAILS_SUCCESS] = (state, { details }) => ({
  ...state,
  data: details.spot,
  loading: false,
});

handlers[FETCH_SPOT_DETAILS_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

handlers[LEAVE_SPOT_ROUTE] = () => initialState;

export default createReducer(handlers, initialState);
