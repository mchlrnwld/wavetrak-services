import { expect } from 'chai';
import spot from '.';

describe('reducers / spot', () => {
  it('combines spot page reducers', () => {
    const state = spot({}, { type: 'INIT' });
    expect(state.report).to.be.ok();
    expect(state.nearby).to.be.ok();
  });
});
