import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { FETCH_BUOYS_SUCCESS } from '../../actions/spot';
import buoys from './buoys';

describe('reducers / spot / buoys', () => {
  const buoysImagesPaths = [
    {
      url: 'http://www.surfline.com/buoy-report/san-pedro_2415_46222/',
      img: 'https://tile.surfline.com/dashboard/buoyplot.cfm?buoy_id=46222&bp_timezone=-7&units=e',
    },
    {
      url: 'http://www.surfline.com/buoy-report/santa-monica_2415_46221/',
      img: 'https://tile.surfline.com/dashboard/buoyplot.cfm?buoy_id=46221&bp_timezone=-7&units=e',
    },
    {
      url: 'http://www.surfline.com/buoy-report/camp-pendleton_2415_46242/',
      img: 'https://tile.surfline.com/dashboard/buoyplot.cfm?buoy_id=46242&bp_timezone=-7&units=e',
    },
  ];
  const initialState = {
    loading: false,
    data: null,
    error: null,
  };

  it('sets buoys data on FETCH_BUOYS_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_BUOYS_SUCCESS,
      buoys: buoysImagesPaths,
    });
    const state = buoys(initialState, action);
    expect(state.data).to.equal(buoysImagesPaths);
    expect(state.loading).to.equal(false);
    expect(state.error).to.equal(null);
  });
});
