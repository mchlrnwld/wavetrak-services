import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { FETCH_NEARBY_SPOTS_SUCCESS } from '../../actions/spot';
import { LEAVE_SPOT_ROUTE } from '../../actions/routes';
import nearby from './nearby';

const nearbyFixture = {
  associated: {
    units: { windSpeed: 'KPH', waveHeight: 'M', tideHeight: 'M' },
    reverseSortBeachView: true,
  },
  data: {
    spots: [
      {
        _id: '584204204e65fad6a7709994',
        lat: 32.927,
        lon: -117.265,
        name: 'Torrey Pines State Beach',
        cameras: [
          {
            title: 'Torrey Pines State Beach',
            streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253476/d9f7ca26/playlist.m3u8',
            stillUrl: 'http://camstills.cdn-surfline.com/torreypinescam/latest_small.jpg',
            rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/torreypinescam/torreypinescam',
            _id: '58acb0e48816e20012bb4447',
            status: { isDown: false, message: '' },
          },
        ],
        conditions: { human: true, value: 'FAIR' },
        wind: { speed: 6.2, direction: 323.96 },
        waveHeight: {
          human: true,
          min: 1.2,
          max: 1.5,
          occasional: 1.8,
          humanRelation: 'shoulder to head high',
        },
        tide: {
          previous: {
            type: 'LOW',
            height: 0,
            timestamp: 1487710022,
            utcOffset: -8,
          },
          next: {
            type: 'HIGH',
            height: 1,
            timestamp: 1487733721,
            utcOffset: -8,
          },
        },
        rank: 0,
        subregionId: '58581a836630e24c4487900d',
        legacyId: 107951,
        thumbnail: 'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
      },
    ],
  },
};

describe('reducers / spot / nearby', () => {
  const initialState = {};

  it('sets forecast data on FETCH_NEARBY_SPOTS_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_NEARBY_SPOTS_SUCCESS,
      nearbySpots: nearbyFixture,
    });

    expect(nearby(initialState, action)).to.deep.equal({
      loading: false,
      data: nearbyFixture.data,
      units: nearbyFixture.associated.units,
      reverseSortBeachView: nearbyFixture.associated.reverseSortBeachView,
    });
  });

  it('resets on LEAVE_SPOT_ROUTE', () => {
    const state = deepFreeze({
      loading: true,
      units: {},
    });
    const action = deepFreeze({ type: LEAVE_SPOT_ROUTE });
    expect(nearby(state, action)).to.deep.equal(initialState);
  });
});
