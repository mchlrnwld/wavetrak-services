import { combineReducers } from 'redux';
import { createReducer } from '@reduxjs/toolkit';

import report from './report';
import nearby from './nearby';
import buoys from './buoys';
import multiCam from './multiCam';
import featuredContent from './featuredContent';
import details from './details';
import { setSpotLoading } from '../../actions/spot';

export default combineReducers({
  report,
  nearby,
  buoys,
  featuredContent,
  multiCam,
  details,
  loading: createReducer(false, (builder) => {
    builder.addCase(setSpotLoading, (state, action) => action.payload);
  }),
});
