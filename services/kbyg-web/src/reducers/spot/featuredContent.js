import createReducer from '../createReducer';

import { FETCH_FEATURED_CONTENT_SUCCESS } from '../../actions/spot';

export const initialState = {
  articles: [],
};

const handlers = {};

handlers[FETCH_FEATURED_CONTENT_SUCCESS] = (state, { featuredContent }) => ({
  ...state,
  articles: featuredContent,
});

export default createReducer(handlers, initialState);
