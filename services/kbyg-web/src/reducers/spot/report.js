import createReducer from '../createReducer';
import { FETCH_SPOT_REPORT, FETCH_SPOT_REPORT_SUCCESS } from '../../actions/spot';
import { LEAVE_SPOT_ROUTE } from '../../actions/routes';

export const initialState = {};

const handlers = {};

handlers[FETCH_SPOT_REPORT] = (state) => ({
  ...state,
  loading: true,
});

handlers[FETCH_SPOT_REPORT_SUCCESS] = (
  state,
  { data, associated, legacySpotId, legacySubregionId },
) => ({
  ...state,
  data: {
    ...data,
    forecast: {
      ...data.forecast,
      swells: data.forecast.swells.map((swell, index) => ({ ...swell, index })),
    },
  },
  associated,
  legacySpotId,
  legacySubregionId,
  loading: false,
});

handlers[LEAVE_SPOT_ROUTE] = () => initialState;

export default createReducer(handlers, initialState);
