import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { SET_PRIMARY_CAM, INITIALIZE_MULTI_CAM, SET_CAM_VIEW } from '../../actions/multiCam';
import multiCam from './multiCam';
import createCamerasFixture from '../fixtures/cameras';

describe('reducers / spot / multiCam', () => {
  const camerasFixture = createCamerasFixture(3);

  const initialState = {};

  it('Initializes the multiCam and primary cam', () => {
    const action = deepFreeze({
      type: INITIALIZE_MULTI_CAM,
      isMultiCam: true,
      numCams: 3,
      initialCameras: camerasFixture,
      primaryCam: camerasFixture[0],
    });
    const state = multiCam(initialState, action);

    expect(state).to.deep.equal({
      primaryCam: camerasFixture[0],
      cameras: camerasFixture,
      isMultiCam: true,
      numCams: 3,
      initialCameras: camerasFixture,
    });
  });

  it('Should be able to change to mutli view', () => {
    const action = deepFreeze({
      type: SET_CAM_VIEW,
      activeView: 'MULTI',
    });
    const state = multiCam({ activeView: 'SINGLE' }, action);

    expect(state).to.deep.equal({
      activeView: 'MULTI',
    });
  });

  it('Should be able to change to single view', () => {
    const action = deepFreeze({
      type: SET_CAM_VIEW,
      activeView: 'SINGLE',
    });
    const state = multiCam({ activeView: 'MULTI' }, action);

    expect(state).to.deep.equal({
      activeView: 'SINGLE',
    });
  });

  it('Changes to the chosen primary and resorts the cameras array based off the initial cameras array', () => {
    const action = deepFreeze({
      type: INITIALIZE_MULTI_CAM,
      initialCameras: camerasFixture,
      isMultiCam: true,
      numCams: 3,
      primaryCam: camerasFixture[0],
    });
    const state = multiCam(initialState, action);

    expect(state).to.deep.equal({
      primaryCam: camerasFixture[0],
      cameras: camerasFixture,
      isMultiCam: true,
      numCams: 3,
      initialCameras: camerasFixture,
    });

    const action2 = deepFreeze({
      type: SET_PRIMARY_CAM,
      primaryCam: camerasFixture[2],
    });
    const state2 = multiCam(state, action2);

    const expectedCamerasArray = [camerasFixture[2], camerasFixture[1], camerasFixture[0]];
    expect(state2).to.deep.equal({
      primaryCam: camerasFixture[2],
      isMultiCam: true,
      numCams: 3,
      cameras: expectedCamerasArray,
      initialCameras: camerasFixture,
    });
  });
});
