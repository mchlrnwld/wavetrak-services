import createReducer from '../createReducer';
import { SET_PRIMARY_CAM, INITIALIZE_MULTI_CAM, SET_CAM_VIEW } from '../../actions/multiCam';

export const initialState = {
  primaryCam: null,
  cameras: null,
  initialCameras: null,
  numCams: null,
  isMultiCam: false,
  activeView: 'SINGLE',
};

const handlers = {};

handlers[INITIALIZE_MULTI_CAM] = (state, { initialCameras, primaryCam, isMultiCam, numCams }) => ({
  ...state,
  initialCameras,
  primaryCam,
  isMultiCam,
  numCams,
  cameras: initialCameras,
});

handlers[SET_PRIMARY_CAM] = (state, { primaryCam }) => {
  const { cameras } = state;
  const oldPrimaryIndex = cameras.findIndex((cam) => cam._id === primaryCam._id);
  const filteredCameras = cameras.filter((cam) => cam._id !== primaryCam._id);
  const newCameras = [
    primaryCam,
    ...filteredCameras.slice(1, oldPrimaryIndex),
    state.primaryCam,
    ...filteredCameras.slice(oldPrimaryIndex),
  ];

  return {
    ...state,
    primaryCam,
    cameras: newCameras,
  };
};

handlers[SET_CAM_VIEW] = (state, { activeView }) => ({
  ...state,
  activeView,
});

export default createReducer(handlers, initialState);
