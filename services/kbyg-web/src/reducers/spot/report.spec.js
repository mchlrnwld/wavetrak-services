import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { FETCH_SPOT_REPORT_SUCCESS } from '../../actions/spot';
import { LEAVE_SPOT_ROUTE } from '../../actions/routes';
import report from './report';
import reportFixture from '../fixtures/report';

describe('reducers / spot / report', () => {
  const initialState = {};

  it('sets forecast data on FETCH_SPOT_REPORT_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_SPOT_REPORT_SUCCESS,
      data: reportFixture.data,
      associated: reportFixture.associated,
      legacySpotId: reportFixture.associated.advertising.spotId,
      legacySubregionId: reportFixture.associated.advertising.subregionId,
    });

    expect(report(initialState, action)).to.deep.equal({
      loading: false,
      data: {
        ...reportFixture.data,
        forecast: {
          ...reportFixture.data.forecast,
          swells: [{ index: 0 }],
        },
      },
      associated: reportFixture.associated,
      legacySpotId: reportFixture.associated.advertising.spotId,
      legacySubregionId: reportFixture.associated.advertising.subregionId,
    });
  });

  it('resets on LEAVE_SPOT_ROUTE', () => {
    const state = deepFreeze({
      loading: false,
      data: [],
    });
    const action = deepFreeze({
      type: LEAVE_SPOT_ROUTE,
    });
    expect(report(state, action)).to.deep.equal(initialState);
  });
});
