import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  setScrollPosition,
  clearActiveSpot,
  scrollToSpot,
  setActiveSpot,
} from '../actions/animations';
import animations from './animations';

describe('reducers / animations', () => {
  const initialState = deepFreeze({
    scrollPositions: {
      spotList: {
        top: 0,
        left: 0,
      },
      camList: {
        top: 0,
        left: 0,
      },
    },
  });

  it('sets activeSpotId on SET_ACTIVE_SPOT', () => {
    const spotId = '5842041f4e65fad6a77088ed';
    expect(animations(initialState, setActiveSpot(spotId)).activeSpotId).to.equal(spotId);
  });

  it('sets activeSpotId null on CLEAR_ACTIVE_SPOT', () => {
    const state = {
      ...initialState,
      activeSpotId: '5842041f4e65fad6a77088ed',
    };
    expect(animations(state, clearActiveSpot()).activeSpotId).to.be.undefined();
  });

  it('sets spotListScrollAnchor and activeSpotId on SCROLL_TO_SPOT', () => {
    const spotId = '5842041f4e65fad6a77088ed';
    expect(animations(initialState, scrollToSpot(spotId))).to.deep.equal({
      activeSpotId: spotId,
      spotListScrollAnchor: spotId,
      scrollPositions: initialState.scrollPositions,
    });
  });

  it('sets scrollPosition for spotList on SET_SCROLL_POSITION', () => {
    const action = setScrollPosition('spotList', {
      top: 15,
      left: 10,
    });
    expect(animations(initialState, action)).to.deep.equal({
      ...initialState,
      scrollPositions: {
        ...initialState.scrollPositions,
        spotList: {
          top: 15,
          left: 10,
        },
      },
    });
  });

  it('sets scrollPosition for camList on SET_SCROLL_POSITION', () => {
    const action = setScrollPosition('camList', {
      top: 15,
      left: 10,
    });
    expect(animations(initialState, action)).to.deep.equal({
      ...initialState,
      scrollPositions: {
        ...initialState.scrollPositions,
        camList: {
          top: 15,
          left: 10,
        },
      },
    });
  });

  it('sets scrollPosition for daySelectorList on SET_SCROLL_POSITION', () => {
    const action = setScrollPosition('daySelectorList', {
      top: 15,
      left: 10,
    });
    expect(animations(initialState, action)).to.deep.equal({
      ...initialState,
      scrollPositions: {
        ...initialState.scrollPositions,
        daySelectorList: {
          top: 15,
          left: 10,
        },
      },
    });
  });

  it('sets scrollPosition for chartList on SET_SCROLL_POSITION', () => {
    const action = setScrollPosition('chartList', {
      top: 15,
      left: 10,
    });
    expect(animations(initialState, action)).to.deep.equal({
      ...initialState,
      scrollPositions: {
        ...initialState.scrollPositions,
        chartList: {
          top: 15,
          left: 10,
        },
      },
    });
  });
});
