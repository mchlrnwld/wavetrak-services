/* eslint-disable no-param-reassign */
import { combineReducers, Reducer, CombinedState } from 'redux';
import { AsyncReducers, BackplaneState } from '@surfline/web-common';
import status from './status';
import map from './map';
import mapV2 from './mapV2';
import spot from './spot';
import units from './units';
import animations from './animations';
import subregion from './subregion';
import worldTaxonomy from './worldTaxonomy';
import components from './components';
import swellEvents from './swellEvents';
import deviceData from './deviceData';
import buoys from './buoys';
import analytics from './analytics';
import split from './split';

export type KBYGAsyncReducers = AsyncReducers & {
  backplane: Reducer<CombinedState<BackplaneState['backplane']>>;
};

const rootReducer = (asyncReducers: KBYGAsyncReducers) =>
  combineReducers({
    map,
    mapV2,
    units,
    status,
    animations,
    spot,
    subregion,
    worldTaxonomy,
    components,
    swellEvents,
    deviceData,
    analytics,
    split,
    [buoys.path]: buoys.reducer,
    ...asyncReducers,
  });

export default rootReducer;
