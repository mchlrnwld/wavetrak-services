import { buoysAPI } from '../common/api/buoys';

const buoysReducer = { path: buoysAPI.reducerPath, reducer: buoysAPI.reducer };

export default buoysReducer;
