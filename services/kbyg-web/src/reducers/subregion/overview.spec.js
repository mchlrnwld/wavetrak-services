import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  CLEAR_SUBREGION_OVERVIEW_DATA,
  FETCH_SUBREGION_OVERVIEW_DATA,
  FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS,
  FETCH_SUBREGION_OVERVIEW_DATA_FAILURE,
} from '../../actions/subregion';
import overview from './overview';

describe('reducers / subregion / overview', () => {
  const initialState = deepFreeze({
    subregionId: '',
    associated: {
      chartsUrl: null,
      units: {
        waveHeight: null,
      },
    },
    overview: {
      data: null,
    },
    loading: false,
    fetched: false,
  });

  it('initializes with default values', () => {
    expect(overview()).to.deep.equal(initialState);
  });

  it('clears data on CLEAR_SUBREGION_OVERVIEW_DATA', () => {
    const action = deepFreeze({
      type: CLEAR_SUBREGION_OVERVIEW_DATA,
    });
    expect(overview(initialState, action)).to.deep.equal({
      ...initialState,
    });
  });

  it('sets subregionId and loading on FETCH_SUBREGION_OVERVIEW_DATA', () => {
    const action = deepFreeze({
      type: FETCH_SUBREGION_OVERVIEW_DATA,
      subregionId: '5842041f4e65fad6a77088ed',
    });
    expect(overview(initialState, action)).to.deep.equal({
      subregionId: action.subregionId,
      associated: {
        chartsUrl: null,
        units: {
          waveHeight: null,
        },
      },
      overview: {
        data: null,
      },
      loading: true,
      fetched: true,
    });
  });

  it('sets overview and loading on FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS,
      overview: {
        associated: {
          chartsUrl: null,
        },
        data: {},
        units: {
          waveHeight: 'FT',
        },
      },
    });
    expect(overview(initialState, action)).to.deep.equal({
      subregionId: '',
      associated: {
        chartsUrl: null,
      },
      overview: {
        associated: {
          chartsUrl: null,
        },
        data: {},
        units: {
          waveHeight: 'FT',
        },
      },
      loading: false,
      fetched: true,
    });
  });

  it('sets loading on FETCH_SUBREGION_OVERVIEW_DATA_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_SUBREGION_OVERVIEW_DATA_FAILURE,
      subregionId: '',
    });
    expect(overview(initialState, action)).to.deep.equal({
      subregionId: action.subregionId,
      loading: false,
      fetched: true,
      associated: {
        chartsUrl: null,
        units: {
          waveHeight: null,
        },
      },
      overview: {
        data: null,
      },
    });
  });
});
