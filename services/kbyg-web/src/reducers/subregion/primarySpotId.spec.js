import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS } from '../../actions/subregion';
import primarySpotId from './primarySpotId';

describe('reducers / subregion / primarySpotId', () => {
  const initialState = null;

  it('sets primarySpotId data on FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS,
      overview: { data: { primarySpot: '5842041f4e65fad6a7708827' } },
    });
    expect(primarySpotId(initialState, action)).to.equal('5842041f4e65fad6a7708827');
  });
});
