import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  FETCH_REGIONAL_ARTICLES,
  FETCH_REGIONAL_ARTICLES_SUCCESS,
  FETCH_REGIONAL_ARTICLES_FAILURE,
} from '../../actions/subregion';
import feed from './feed';

describe('reducers / subregion / feed', () => {
  const initialState = deepFreeze({
    loading: false,
    error: null,
    articles: [],
  });

  it('initializes with default values', () => {
    expect(feed()).to.deep.equal(initialState);
  });

  it('sets loading on FETCH_REGIONAL_ARTICLES', () => {
    const action = deepFreeze({
      type: FETCH_REGIONAL_ARTICLES,
    });
    expect(feed(initialState, action)).to.deep.equal({
      loading: true,
      error: null,
      articles: [],
    });
  });

  it('sets articles, loading on FETCH_REGIONAL_ARTICLES_SUCCESS', () => {
    const articles = ['article'];
    const action = deepFreeze({
      type: FETCH_REGIONAL_ARTICLES_SUCCESS,
      articles,
    });

    expect(feed(initialState, action)).to.deep.equal({
      loading: false,
      error: null,
      articles: ['article'],
    });
  });

  it('sets error and loading on FETCH_REGIONAL_ARTICLES_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_REGIONAL_ARTICLES_FAILURE,
      error: {
        message: 'An error occurred',
        status: 500,
      },
    });
    expect(feed(initialState, action)).to.deep.equal({
      loading: false,
      error: {
        message: 'An error occurred',
        status: 500,
      },
      articles: [],
    });
  });
});
