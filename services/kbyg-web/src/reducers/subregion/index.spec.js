import { expect } from 'chai';
import subregion from '.';

describe('reducers / subregion', () => {
  it('combines subregion reducers', () => {
    const subregionState = subregion({}, { type: '@@INIT' });
    expect(subregionState.overview).to.be.ok();
    expect(subregionState.feed).to.be.ok();
  });
});
