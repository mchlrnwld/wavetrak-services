import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { FETCH_NEARBY_SUBREGIONS_SUCCESS } from '../../actions/subregion';
import nearby from './nearby';

const nearbyFixture = {
  associated: {
    units: { windSpeed: 'KPH', waveHeight: 'M', tideHeight: 'M' },
  },
  data: {
    subregions: [
      {
        _id: '58581a836630e24c4487901e',
        name: 'North Costa Rica',
        legacyId: 2979,
        offshoreSwellLocation: {
          type: 'Point',
          coordinates: [-86, 10],
        },
      },
      {
        _id: '58581a836630e24c44878fea',
        name: 'Central Costa Rica',
        legacyId: 2164,
        offshoreSwellLocation: {
          type: 'Point',
          coordinates: [-84.75, 9.5],
        },
      },
    ],
  },
};

describe('reducers / subregion / nearby', () => {
  const initialState = {};

  it('sets forecast data on FETCH_NEARBY_SUBREGIONS_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_NEARBY_SUBREGIONS_SUCCESS,
      nearbySubregions: nearbyFixture,
    });

    expect(nearby(initialState, action)).to.deep.equal({
      loading: false,
      data: nearbyFixture.data,
      units: nearbyFixture.associated.units,
    });
  });
});
