import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import { FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS } from '../../actions/subregion';
import subregionId from './subregionId';

describe('reducers / subregion / subregionId', () => {
  const initialState = null;

  it('sets subregionId data on FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_SUBREGION_OVERVIEW_DATA_SUCCESS,
      subregionId: '58581a836630e24c44878fd6',
    });
    expect(subregionId(initialState, action)).to.equal('58581a836630e24c44878fd6');
  });
});
