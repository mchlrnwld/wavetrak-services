import createReducer from './createReducer';
import { NOT_FOUND, INTERNAL_SERVER_ERROR } from '../actions/status';

export const initialState = { code: 200 };
const handlers = {};

handlers[NOT_FOUND] = (state) => ({
  ...state,
  code: 404,
});

handlers[INTERNAL_SERVER_ERROR] = (state, { message }) => ({
  ...state,
  code: 500,
  message,
});

export default createReducer(handlers, initialState);
