import { combineReducers } from 'redux';
import charts from './charts';
import graphs from './graphs';

export default combineReducers({
  charts,
  graphs,
});
