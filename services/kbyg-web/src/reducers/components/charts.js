import createReducer from '../createReducer';
import {
  FETCH_SUBREGION_CHART_IMAGES,
  FETCH_SUBREGION_CHART_IMAGES_SUCCESS,
  FETCH_SUBREGION_CHART_IMAGES_FAILURE,
  FETCH_SUBREGION_CHART_MENU,
  FETCH_SUBREGION_CHART_MENU_SUCCESS,
  FETCH_SUBREGION_CHART_MENU_FAILURE,
  SET_ACTIVE_CHART,
} from '../../actions/charts';

export const initialState = {
  loading: false,
  error: null,
  active: {
    type: 'localswell',
    name: 'Wave Height',
    category: 'subregion',
    slug: 'wave-height',
  },
  types: null,
  region: null,
  nearshoreModelName: null,
  images: {
    error: null,
    loading: false,
    images: [],
    timezone: null,
  },
};

const handlers = {};

handlers[FETCH_SUBREGION_CHART_IMAGES] = (state) => ({
  ...state,
  images: {
    ...initialState.images,
    loading: true,
  },
});

handlers[FETCH_SUBREGION_CHART_IMAGES_SUCCESS] = (state, { images, timezone }) => ({
  ...state,
  images: {
    ...initialState.images,
    images,
    timezone,
  },
});

handlers[FETCH_SUBREGION_CHART_IMAGES_FAILURE] = (state, { error }) => ({
  ...state,
  images: {
    ...initialState.images,
    error,
  },
});

handlers[FETCH_SUBREGION_CHART_MENU] = (state) => ({
  ...state,
  loading: true,
});

handlers[FETCH_SUBREGION_CHART_MENU_SUCCESS] = (state, { types, region }) => ({
  ...state,
  region,
  types,
  loading: false,
});

handlers[FETCH_SUBREGION_CHART_MENU_FAILURE] = (state, { error }) => ({
  ...state,
  error,
  loading: false,
});

handlers[SET_ACTIVE_CHART] = (state, { active }) => ({
  ...state,
  active,
});

export default createReducer(handlers, initialState);
