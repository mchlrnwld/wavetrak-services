import createReducer from '../../createReducer';
import {
  FETCH_GRAPH_WEATHER_FORECAST,
  FETCH_GRAPH_WEATHER_FORECAST_FAILURE,
  FETCH_GRAPH_WEATHER_FORECAST_SUCCESS,
} from '../../../actions/graphs';

export const initialState = {
  utcOffset: null,
  days: null,
  loading: false,
};

const handlers = {};

handlers[FETCH_GRAPH_WEATHER_FORECAST] = () => ({
  ...initialState,
  loading: true,
});

handlers[FETCH_GRAPH_WEATHER_FORECAST_SUCCESS] = (state, { response }) => ({
  utcOffset: response.associated.utcOffset,
  days: response.data.sunlightTimes,
  loading: false,
});

handlers[FETCH_GRAPH_WEATHER_FORECAST_FAILURE] = (state, { error }) => ({
  ...state,
  loading: false,
  error,
});

export default createReducer(handlers, initialState);
