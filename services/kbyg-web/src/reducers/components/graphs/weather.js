import { transformGraphDataToDays as transformDataToDays } from '@surfline/web-common';
import createReducer from '../../createReducer';
import {
  FETCH_GRAPH_WEATHER_FORECAST,
  FETCH_GRAPH_WEATHER_FORECAST_SUCCESS,
  FETCH_GRAPH_WEATHER_FORECAST_FAILURE,
} from '../../../actions/graphs';
import createFiveDayHourlyData from './helpers/createFiveDayHourlyData';

export const initialState = {
  error: null,
  loading: true,
  weatherIconPath: null,
  units: null,
  days: null,
};

const handlers = {};

handlers[FETCH_GRAPH_WEATHER_FORECAST] = () => initialState;

handlers[FETCH_GRAPH_WEATHER_FORECAST_SUCCESS] = (state, { response, desired16DayInterval }) => ({
  ...state,
  loading: false,
  weatherIconPath: response.associated.weatherIconPath,
  units: response.associated.units,
  days: transformDataToDays(
    response.data.weather.filter((_, index) => index % desired16DayInterval === 0),
    desired16DayInterval,
    (pt) => pt,
    true,
  ),
  hourly: createFiveDayHourlyData(response.data.weather),
});

handlers[FETCH_GRAPH_WEATHER_FORECAST_FAILURE] = (state, { error }) => ({
  ...state,
  error,
  loading: false,
});

export default createReducer(handlers, initialState);
