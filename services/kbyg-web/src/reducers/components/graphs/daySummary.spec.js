import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import daySummaryFixture from './fixtures/daySummary';
import {
  FETCH_GRAPH_CONDITION_FORECAST,
  FETCH_GRAPH_CONDITION_FORECAST_SUCCESS,
  FETCH_GRAPH_CONDITION_FORECAST_FAILURE,
} from '../../../actions/graphs';
import daySummary from './daySummary';

describe('reducers / components / chart / daySummary', () => {
  const initialState = deepFreeze({
    error: null,
    loading: true,
    units: null,
    utcOffset: null,
    days: null,
  });

  it('resets on FETCH_GRAPH_CONDITION_FORECAST', () => {
    const action = deepFreeze({ type: FETCH_GRAPH_CONDITION_FORECAST });
    const state = deepFreeze({
      error: 'Some error',
      loading: false,
      units: {},
      days: [],
    });
    expect(daySummary(state, action)).to.deep.equal(initialState);
  });

  it('updates on FETCH_GRAPH_CONDITION_FORECAST_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_GRAPH_CONDITION_FORECAST_SUCCESS,
      response: daySummaryFixture,
    });
    expect(daySummary(initialState, action)).to.deep.equal({
      error: null,
      loading: false,
      units: action.response.associated.units,
      utcOffset: action.response.associated.utcOffset,
      days: action.response.data.conditions,
    });
  });

  it('errors on FETCH_GRAPH_CONDITION_FORECAST_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_GRAPH_CONDITION_FORECAST_FAILURE,
      error: 'Some error',
    });
    expect(daySummary(initialState, action)).to.deep.equal({
      ...initialState,
      error: action.error,
      loading: false,
    });
  });
});
