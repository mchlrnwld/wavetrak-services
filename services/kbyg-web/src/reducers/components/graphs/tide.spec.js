import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  FETCH_GRAPH_TIDE_FORECAST,
  FETCH_GRAPH_TIDE_FORECAST_SUCCESS,
  FETCH_GRAPH_TIDE_FORECAST_FAILURE,
} from '../../../actions/graphs';
import tide from './tide';
import tideFixture from './fixtures/tide';

describe('reducers / components / chart / tide', () => {
  const initialState = deepFreeze({
    error: null,
    loading: true,
    units: null,
    days: null,
  });

  it('resets on FETCH_GRAPH_TIDE_FORECAST', () => {
    const action = deepFreeze({ type: FETCH_GRAPH_TIDE_FORECAST });
    const state = deepFreeze({
      error: null,
      loading: true,
      units: null,
      days: null,
    });
    expect(tide(state, action)).to.deep.equal(initialState);
  });

  it('updates on FETCH_GRAPH_TIDE_FORECAST_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_GRAPH_TIDE_FORECAST_SUCCESS,
      response: tideFixture,
    });
    expect(tide(initialState, action)).to.deep.equal({
      error: null,
      loading: false,
      units: action.response.associated.units,
      utcOffset: action.response.associated.utcOffset,
      tideLocation: action.response.associated.tideLocation,
      days: [action.response.data.tides],
    });
  });

  it('errors on FETCH_GRAPH_TIDE_FORECAST_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_GRAPH_TIDE_FORECAST_FAILURE,
      error: 'Some error',
    });
    expect(tide(initialState, action)).to.deep.equal({
      ...initialState,
      error: action.error,
      loading: false,
    });
  });
});
