import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import sinon from 'sinon';
import {
  FETCH_GRAPH_AUTOMATED_RATING_FORECAST,
  FETCH_GRAPH_AUTOMATED_RATING_FORECAST_FAILURE,
  FETCH_GRAPH_AUTOMATED_RATING_FORECAST_SUCCESS,
} from '../../../actions/graphs';
import automatedRatings from './automatedRatings';
import automatedRatingsResponse from '../../../common/api/fixtures/kbyg/automatedRatings';

describe('reducers / components / chart / automatedRatings', () => {
  const initialState = deepFreeze({
    error: null,
    loading: true,
    location: null,
    days: null,
    hourly: null,
  });

  /** @type {sinon.SinonFakeTimers} */
  let clock;
  const now = 1633824000; // Sun Oct 10 2021 00:00:00 GMT+0000

  beforeAll(() => {
    clock = sinon.useFakeTimers(now * 1000);
  });

  afterAll(() => {
    clock.restore();
  });

  it('resets on FETCH_GRAPH_AUTOMATED_RATING_FORECAST', () => {
    const automatedRatingsFixture = automatedRatingsResponse();
    const action = deepFreeze({ type: FETCH_GRAPH_AUTOMATED_RATING_FORECAST });

    const state = deepFreeze({
      error: null,
      loading: true,
      location: null,
      days: [[...automatedRatingsFixture.data.rating].filter((_, i) => i % 3 === 0)],
      hourly: [[...automatedRatingsFixture.data.rating]],
    });

    expect(automatedRatings(state, action)).to.deep.equal(initialState);
  });

  it('updates on FETCH_GRAPH_AUTOMATED_RATING_FORECAST_SUCCESS', () => {
    const response = automatedRatingsResponse(2, 24);

    const action = deepFreeze({
      type: FETCH_GRAPH_AUTOMATED_RATING_FORECAST_SUCCESS,
      desired16DayInterval: 24,
      response,
    });

    const expected = {
      error: null,
      loading: false,
      location: action.response.associated.location,
      // TODO: We'll likely need to change this test a bit once we update the `transformDataToDays`
      // function to properly use `groupBy` logic for sorting timestamps into days
      days: [
        [
          {
            timestamp: 1633824000,
            utcOffset: 0,
            rating: 'POOR',
          },
        ],
      ],
      hourly: [
        [
          {
            timestamp: 1633824000,
            utcOffset: 0,
            rating: 'POOR',
          },
        ],
        [
          {
            timestamp: 1633910400,
            utcOffset: 0,
            rating: 'POOR',
          },
        ],
      ],
    };

    expect(automatedRatings(initialState, action)).to.deep.equal(expected);
  });

  it('updates on FETCH_GRAPH_AUTOMATED_RATING_FORECAST_SUCCESS - DST shift forward', () => {
    const automatedRatingsWithDSTForward = automatedRatingsResponse(2, 1, true, 'forward');
    const action = deepFreeze({
      type: FETCH_GRAPH_AUTOMATED_RATING_FORECAST_SUCCESS,
      desired16DayInterval: 3,
      response: automatedRatingsWithDSTForward,
    });

    const state = automatedRatings(initialState, action);
    expect(state.days[0]).to.have.length(8);
    expect(state.days[1]).to.have.length(8);

    expect(state.hourly[0]).to.have.length(23);
    expect(state.hourly[1]).to.have.length(24);
  });

  it('updates on FETCH_GRAPH_AUTOMATED_RATING_FORECAST_SUCCESS - DST shift backward', () => {
    const automatedRatingsWithDSTBackward = automatedRatingsResponse(2, 1, true, 'backward');
    const action = deepFreeze({
      type: FETCH_GRAPH_AUTOMATED_RATING_FORECAST_SUCCESS,
      desired16DayInterval: 3,
      response: automatedRatingsWithDSTBackward,
    });

    const state = automatedRatings(initialState, action);
    expect(state.days[0]).to.have.length(8);
    expect(state.days[1]).to.have.length(8);

    expect(state.hourly[0]).to.have.length(25);
    expect(state.hourly[1]).to.have.length(24);
  });

  it('errors on FETCH_GRAPH_AUTOMATED_RATING_FORECAST_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_GRAPH_AUTOMATED_RATING_FORECAST_FAILURE,
      error: 'Some error',
    });
    expect(automatedRatings(initialState, action)).to.deep.equal({
      ...initialState,
      error: action.error,
      loading: false,
    });
  });
});
