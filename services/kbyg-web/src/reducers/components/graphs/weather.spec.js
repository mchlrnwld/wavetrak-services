import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import weatherFixture from './fixtures/weather';
import {
  FETCH_GRAPH_WEATHER_FORECAST,
  FETCH_GRAPH_WEATHER_FORECAST_SUCCESS,
  FETCH_GRAPH_WEATHER_FORECAST_FAILURE,
} from '../../../actions/graphs';
import weather from './weather';
import createFiveDayHourlyData from './helpers/createFiveDayHourlyData';

describe('reducers / components / chart / weather', () => {
  const initialState = deepFreeze({
    error: null,
    loading: true,
    weatherIconPath: null,
    units: null,
    days: null,
  });

  it('resets on FETCH_GRAPH_WEATHER_FORECAST', () => {
    const action = deepFreeze({ type: FETCH_GRAPH_WEATHER_FORECAST });
    const state = deepFreeze({
      error: 'Some error',
      loading: false,
      weatherIconPath: 'https://weathericonpath.surfline.com',
      units: {},
      days: [],
    });
    expect(weather(state, action)).to.deep.equal(initialState);
  });

  it('updates on FETCH_GRAPH_WEATHER_FORECAST_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_GRAPH_WEATHER_FORECAST_SUCCESS,
      intervalHours: 3,
      desired16DayInterval: 3,
      response: {
        associated: {
          units: {},
          weatherIconPath: 'https://weathericonpath.surfline.com',
        },
        data: weatherFixture,
      },
    });
    expect(weather(initialState, action)).to.deep.equal({
      error: null,
      loading: false,
      weatherIconPath: action.response.associated.weatherIconPath,
      units: action.response.associated.units,
      days: [action.response.data.weather.filter((_, index) => index % 3 === 0)],
      hourly: createFiveDayHourlyData(action.response.data.weather),
    });
  });

  it('errors on FETCH_GRAPH_WEATHER_FORECAST_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_GRAPH_WEATHER_FORECAST_FAILURE,
      error: 'Some error',
    });
    expect(weather(initialState, action)).to.deep.equal({
      ...initialState,
      error: action.error,
      loading: false,
    });
  });
});
