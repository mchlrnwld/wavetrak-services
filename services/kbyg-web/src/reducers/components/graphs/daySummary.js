import createReducer from '../../createReducer';
import {
  FETCH_GRAPH_CONDITION_FORECAST,
  FETCH_GRAPH_CONDITION_FORECAST_SUCCESS,
  FETCH_GRAPH_CONDITION_FORECAST_FAILURE,
} from '../../../actions/graphs';

export const initialState = {
  error: null,
  loading: true,
  units: null,
  utcOffset: null,
  days: null,
};

const handlers = {};

handlers[FETCH_GRAPH_CONDITION_FORECAST] = () => initialState;

handlers[FETCH_GRAPH_CONDITION_FORECAST_SUCCESS] = (state, { response }) => ({
  ...state,
  loading: false,
  units: response.associated.units,
  utcOffset: response.associated.utcOffset,
  days: response.data.conditions,
});

handlers[FETCH_GRAPH_CONDITION_FORECAST_FAILURE] = (state, { error }) => ({
  ...state,
  error,
  loading: false,
});

export default createReducer(handlers, initialState);
