import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import windFixture from './fixtures/wind';
import {
  FETCH_GRAPH_WIND_FORECAST,
  FETCH_GRAPH_WIND_FORECAST_SUCCESS,
  FETCH_GRAPH_WIND_FORECAST_FAILURE,
} from '../../../actions/graphs';
import wind from './wind';
import createFiveDayHourlyWind from './helpers/createFiveDayHourlyData';

describe('reducers / components / chart / wind', () => {
  const initialState = deepFreeze({
    error: null,
    loading: true,
    units: null,
    utcOffset: null,
    location: null,
    overallMaxSpeed: null,
    days: null,
  });

  it('resets on FETCH_GRAPH_WIND_FORECAST', () => {
    const action = deepFreeze({ type: FETCH_GRAPH_WIND_FORECAST });
    const state = deepFreeze({
      error: 'Some error',
      loading: false,
      units: {},
      utcOffset: -8,
      location: {
        lat: 0,
        lon: 1,
      },
      days: [],
    });
    expect(wind(state, action)).to.deep.equal(initialState);
  });

  it('updates on FETCH_GRAPH_WIND_FORECAST_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_GRAPH_WIND_FORECAST_SUCCESS,
      intervalHours: 3,
      desired16DayInterval: 3,
      response: windFixture,
    });
    expect(wind(initialState, action)).to.deep.equal({
      error: null,
      loading: false,
      units: action.response.associated.units,
      utcOffset: action.response.associated.utcOffset,
      location: action.response.associated.location,
      overallMaxSpeed: 12,
      days: [action.response.data.wind.filter((_, index) => index % 3 === 0)],
      hourly: createFiveDayHourlyWind(action.response.data.wind),
    });
  });

  it('errors on FETCH_GRAPH_WIND_FORECAST_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_GRAPH_WIND_FORECAST_FAILURE,
      error: 'Some error',
    });
    expect(wind(initialState, action)).to.deep.equal({
      ...initialState,
      error: action.error,
      loading: false,
    });
  });
});
