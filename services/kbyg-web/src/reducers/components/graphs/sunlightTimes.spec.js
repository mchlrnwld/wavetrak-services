import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import weatherFixture from './fixtures/weather';
import {
  FETCH_GRAPH_WEATHER_FORECAST,
  FETCH_GRAPH_WEATHER_FORECAST_FAILURE,
  FETCH_GRAPH_WEATHER_FORECAST_SUCCESS,
} from '../../../actions/graphs';
import sunlightTimes from './sunlightTimes';

describe('reducers / components / chart / sunlightTimes', () => {
  const initialState = deepFreeze({
    utcOffset: null,
    days: null,
    loading: false,
  });

  it('resets on FETCH_GRAPH_WEATHER_FORECAST', () => {
    const action = deepFreeze({ type: FETCH_GRAPH_WEATHER_FORECAST });
    const state = deepFreeze([]);
    expect(sunlightTimes(state, action)).to.deep.equal({
      ...initialState,
      loading: true,
    });
  });

  it('updates on FETCH_GRAPH_WEATHER_FORECAST_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_GRAPH_WEATHER_FORECAST_SUCCESS,
      response: {
        associated: {
          utcOffset: 0,
        },
        data: weatherFixture,
      },
    });
    expect(sunlightTimes(initialState, action)).to.deep.equal({
      utcOffset: action.response.associated.utcOffset,
      days: action.response.data.sunlightTimes,
      loading: false,
    });
  });

  it('updates on FETCH_GRAPH_WEATHER_FORECAST_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_GRAPH_WEATHER_FORECAST_FAILURE,
      error: 'some error',
    });
    expect(sunlightTimes(initialState, action)).to.deep.equal({
      utcOffset: null,
      days: null,
      loading: false,
      error: 'some error',
    });
  });
});
