import { expect } from 'chai';
import createFiveHourlyData from './createFiveDayHourlyData';

describe('helpers / createFiveHourlyData', () => {
  it('should create an array for each day within the timestamp array', () => {
    const data = [
      // Fri Jan 01 2021 00:00:00 GMT+0000
      { timestamp: 1609459200, utcOffset: 0 },
      // Sat Jan 02 2021 00:00:00 GMT+0000
      { timestamp: 1609545600, utcOffset: 0 },
    ];

    expect(createFiveHourlyData(data)).to.deep.equal([
      [{ timestamp: 1609459200, utcOffset: 0 }],
      [{ timestamp: 1609545600, utcOffset: 0 }],
    ]);
  });

  it('should apply the custom map function to each element', () => {
    const data = [
      // Fri Jan 01 2021 00:00:00 GMT+0000
      { timestamp: 1609459200, utcOffset: 0 },
      // Sat Jan 02 2021 00:00:00 GMT+0000
      { timestamp: 1609545600, utcOffset: 0 },
    ];

    expect(createFiveHourlyData(data, (p) => ({ ...p, test: true }))).to.deep.equal([
      [{ timestamp: 1609459200, utcOffset: 0, test: true }],
      [{ timestamp: 1609545600, utcOffset: 0, test: true }],
    ]);
  });

  it('should handle DST shifting back at midnight', () => {
    const data = [
      // Fri Jan 01 2021 00:00:00 GMT+0000
      { timestamp: 1609459200, utcOffset: 0 },
      // Sat Jan 02 2021 00:00:00 GMT+0000
      { timestamp: 1609545600, utcOffset: -1 },
      // Sat Jan 02 2021 01:00:00 GMT+0000
      { timestamp: 1609549200, utcOffset: -1 },
    ];

    expect(createFiveHourlyData(data)).to.deep.equal([
      [
        { timestamp: 1609459200, utcOffset: 0 },
        { timestamp: 1609545600, utcOffset: -1 },
      ],
      [{ timestamp: 1609549200, utcOffset: -1 }],
    ]);
  });

  it('should handle DST shifting forward at midnight', () => {
    const data = [
      // Fri Jan 01 2021 00:00:00 GMT+0000
      { timestamp: 1609459200, utcOffset: 0 },
      // Sat Jan 02 2021 00:00:00 GMT+0000
      { timestamp: 1609545600, utcOffset: 1 },
      // Sat Jan 02 2021 01:00:00 GMT+0000
      { timestamp: 1609549200, utcOffset: 1 },
    ];

    expect(createFiveHourlyData(data)).to.deep.equal([
      [{ timestamp: 1609459200, utcOffset: 0 }],
      [
        { timestamp: 1609545600, utcOffset: 1 },
        { timestamp: 1609549200, utcOffset: 1 },
      ],
    ]);
  });

  it('should throw an error if timestamps are missing', () => {
    const data = [{ utcOffset: 0 }];

    let error;
    try {
      createFiveHourlyData(data);
    } catch (err) {
      error = err;
    }

    expect(error).to.exist();
    expect(error.message).to.be.equal('Data points must have a `timestamp` and a `utcOffset`');
  });

  it('should throw an error if utcOffsets are missing', () => {
    const data = [{ timestamp: 0 }];

    let error;
    try {
      createFiveHourlyData(data);
    } catch (err) {
      error = err;
    }

    expect(error).to.exist();
    expect(error.message).to.be.equal('Data points must have a `timestamp` and a `utcOffset`');
  });
});
