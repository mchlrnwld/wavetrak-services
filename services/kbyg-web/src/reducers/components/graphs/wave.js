import { transformGraphDataToDays as transformDataToDays } from '@surfline/web-common';
import createReducer from '../../createReducer';
import {
  FETCH_GRAPH_WAVE_FORECAST,
  FETCH_GRAPH_WAVE_FORECAST_SUCCESS,
  FETCH_GRAPH_WAVE_FORECAST_FAILURE,
} from '../../../actions/graphs';
import createFiveDayHourlyWave from './helpers/createFiveDayHourlyData';

export const initialState = {
  error: null,
  loading: true,
  units: null,
  utcOffset: null,
  location: null,
  forecastLocation: null,
  offshoreLocation: null,
  overallMaxSurfHeight: null,
  overallMaxSwellHeight: null,
  days: null,
};

const handlers = {};

handlers[FETCH_GRAPH_WAVE_FORECAST] = () => initialState;

const maxSwell = (swells) => Math.max(...swells.map((swell) => swell.height));

handlers[FETCH_GRAPH_WAVE_FORECAST_SUCCESS] = (
  state,
  { response, desired16DayInterval, isGraphUpdates },
) => {
  const mapFunc = ({ timestamp, utcOffset, surf, swells }) => ({
    timestamp,
    utcOffset,
    surf,
    swells: swells.map((swell, index) => ({
      ...swell,
      index,
    })),
  });
  return {
    ...state,
    loading: false,
    units: response.associated.units,
    utcOffset: response.associated.utcOffset,
    location: response.associated.location,
    forecastLocation: response.associated.forecastLocation,
    offshoreLocation: response.associated.offshoreLocation,
    overallMaxSurfHeight: Math.max(...response.data.wave.map(({ surf }) => surf.max)),
    overallMaxSwellHeight: Math.max(...response.data.wave.map(({ swells }) => maxSwell(swells))),
    days: transformDataToDays(
      isGraphUpdates
        ? response.data.wave.filter((_, index) => index % desired16DayInterval === 0)
        : response.data.wave,
      desired16DayInterval,
      mapFunc,
    ),
    hourly: createFiveDayHourlyWave(response.data.wave, mapFunc),
  };
};

handlers[FETCH_GRAPH_WAVE_FORECAST_FAILURE] = (state, { error }) => ({
  ...state,
  error,
  loading: false,
});

export default createReducer(handlers, initialState);
