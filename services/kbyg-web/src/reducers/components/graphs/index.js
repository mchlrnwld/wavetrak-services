import { combineReducers } from 'redux';
import sunlightTimes from './sunlightTimes';
import daySummary from './daySummary';
import wave from './wave';
import waveMaxHeights from './waveMaxHeights';
import tide from './tide';
import weather from './weather';
import wind from './wind';
import automatedRatings from './automatedRatings';

export default combineReducers({
  sunlightTimes,
  daySummary,
  wave,
  waveMaxHeights,
  tide,
  weather,
  wind,
  automatedRatings,
});
