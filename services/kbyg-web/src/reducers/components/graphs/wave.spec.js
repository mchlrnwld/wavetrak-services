import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  FETCH_GRAPH_WAVE_FORECAST,
  FETCH_GRAPH_WAVE_FORECAST_SUCCESS,
  FETCH_GRAPH_WAVE_FORECAST_FAILURE,
} from '../../../actions/graphs';
import wave from './wave';
import createFiveDayHourlyWave from './helpers/createFiveDayHourlyData';

describe('reducers / components / chart / wave', () => {
  const initialState = deepFreeze({
    error: null,
    loading: true,
    units: null,
    utcOffset: null,
    location: null,
    forecastLocation: null,
    offshoreLocation: null,
    overallMaxSurfHeight: null,
    overallMaxSwellHeight: null,
    days: null,
  });

  it('resets on FETCH_GRAPH_WAVE_FORECAST', () => {
    const action = deepFreeze({ type: FETCH_GRAPH_WAVE_FORECAST });
    const state = deepFreeze({
      error: 'Some error',
      loading: false,
      units: {},
      utcOffset: -8,
      location: {
        lat: 0,
        lon: 1,
      },
      forecastLocation: {
        lat: 2,
        lon: 3,
      },
      offshoreLocation: {
        lat: 9,
        lon: -9,
      },
      surf: {
        overallMaxHeight: 4,
        days: [],
      },
      swell: {
        overallMaxHeight: 4,
        days: [],
      },
    });
    expect(wave(state, action)).to.deep.equal(initialState);
  });

  it('updates on FETCH_GRAPH_WAVE_FORECAST_SUCCESS', () => {
    const action = deepFreeze({
      type: FETCH_GRAPH_WAVE_FORECAST_SUCCESS,
      intervalHours: 3,
      desired16DayInterval: 3,
      isGraphUpdates: false,
      response: {
        associated: {
          units: {},
          utcOffset: -8,
          location: {
            lat: 0,
            lon: 1,
          },
          forecastLocation: {
            lat: 2,
            lon: 3,
          },
          offshoreLocation: {
            lat: 9,
            lon: -9,
          },
        },
        data: {
          wave: [...Array(8)].map(() => ({
            timestamp: 1510696028,
            utcOffset: -8,
            surf: { min: 3, max: 4 },
            swells: [{ height: 1 }, { height: 3 }, { height: 2 }],
          })),
        },
      },
    });
    const expected = {
      error: null,
      loading: false,
      units: action.response.associated.units,
      utcOffset: action.response.associated.utcOffset,
      location: action.response.associated.location,
      forecastLocation: action.response.associated.forecastLocation,
      offshoreLocation: action.response.associated.offshoreLocation,
      overallMaxSurfHeight: 4,
      overallMaxSwellHeight: 3,
      days: [
        [...Array(8)].map(() => ({
          timestamp: 1510696028,
          utcOffset: -8,
          surf: {
            min: 3,
            max: 4,
          },
          swells: [
            { height: 1, index: 0 },
            { height: 3, index: 1 },
            { height: 2, index: 2 },
          ],
        })),
      ],
      hourly: createFiveDayHourlyWave(
        action.response.data.wave,
        ({ timestamp, utcOffset, surf, swells }) => ({
          timestamp,
          utcOffset,
          surf,
          swells: swells.map((swell, index) => ({
            ...swell,
            index,
          })),
        }),
      ),
    };

    expect(wave(initialState, action)).to.deep.equal(expected);
  });

  it('errors on FETCH_GRAPH_WAVE_FORECAST_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_GRAPH_WAVE_FORECAST_FAILURE,
      error: 'Some error',
    });
    expect(wave(initialState, action)).to.deep.equal({
      ...initialState,
      error: action.error,
      loading: false,
    });
  });
});
