import { transformGraphDataToDays as transformDataToDays } from '@surfline/web-common';
import createReducer from '../../createReducer';
import {
  FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS,
  FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS_SUCCESS,
  FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS_FAILURE,
} from '../../../actions/graphs';

export const initialState = {
  error: null,
  loading: true,
  units: null,
  utcOffset: null,
  location: null,
  forecastLocation: null,
  offshoreLocation: null,
  overallMaxSurfHeight: null,
  overallMaxSwellHeight: null,
  days: null,
};

const handlers = {};

handlers[FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS] = () => initialState;

handlers[FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS_SUCCESS] = (state, { intervalHours, response }) => ({
  ...state,
  loading: false,
  units: response.associated.units,
  utcOffset: response.associated.utcOffset,
  location: response.associated.location,
  forecastLocation: response.associated.forecastLocation,
  offshoreLocation: response.associated.offshoreLocation,
  overallMaxSurfHeight: Math.max(...response.data.wave.map(({ surf }) => surf.max)),
  days: transformDataToDays(
    response.data.wave,
    intervalHours,
    ({ timestamp, utcOffset, surf }) => ({
      timestamp,
      utcOffset,
      surf,
    }),
  ),
});

handlers[FETCH_GRAPH_WAVE_FORECAST_MAX_HEIGHTS_FAILURE] = (state, { error }) => ({
  ...state,
  error,
  loading: false,
});

export default createReducer(handlers, initialState);
