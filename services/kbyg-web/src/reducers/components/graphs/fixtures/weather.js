const weather = {
  sunlightTimes: [
    {
      midnight: 0,
      nauticalDawn: 1,
      sunrise: 2,
      sunset: 3,
      nauticalDusk: 4,
    },
  ],
  weather: [
    { timestamp: 0, utcOffset: 0, temperature: 55, condition: 'CLEAR_NO_RAIN' },
    { timestamp: 1, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 2, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 3, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 4, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 5, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 6, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 7, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 8, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 9, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 10, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 11, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 12, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 13, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 14, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 15, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 16, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 17, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 18, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 19, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 20, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 21, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 22, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
    { timestamp: 23, utcOffset: 0, temperature: 55, direction: 'CLEAR_NO_RAIN' },
  ],
};

export default weather;
