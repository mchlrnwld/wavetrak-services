const wind = {
  associated: {
    units: { windSpeed: 'MPH' },
    utcOffset: -8,
    location: {
      lat: 0,
      lon: 1,
    },
    intervalHours: 3,
  },
  data: {
    wind: [
      { timestamp: 0, utcOffset: 0, speed: 1, direction: 90 },
      { timestamp: 1, utcOffset: 0, speed: 3, direction: 90 },
      { timestamp: 2, utcOffset: 0, speed: 5, direction: 90 },
      { timestamp: 3, utcOffset: 0, speed: 8, direction: 90 },
      { timestamp: 4, utcOffset: 0, speed: 9, direction: 90 },
      { timestamp: 5, utcOffset: 0, speed: 12, direction: 75 },
      { timestamp: 6, utcOffset: 0, speed: 8, direction: 90 },
      { timestamp: 7, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 8, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 9, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 10, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 11, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 12, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 13, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 14, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 15, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 16, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 17, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 18, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 19, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 20, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 21, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 22, utcOffset: 0, speed: 4, direction: 90 },
      { timestamp: 23, utcOffset: 0, speed: 4, direction: 90 },
    ],
  },
};

export default wind;
