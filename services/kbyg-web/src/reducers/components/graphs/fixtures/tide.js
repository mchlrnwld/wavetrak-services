const tide = {
  associated: {
    units: {},
    intervalHours: 3,
    utcOffset: -8,
    tideLocation: {
      min: -3.31,
      max: 8.76,
    },
  },
  data: {
    tides: [
      {
        timestamp: 1511161200,
        type: 'NORMAL',
        height: 3.64,
      },
      {
        timestamp: 1511164800,
        type: 'LOW',
        height: 3.25,
      },
      {
        timestamp: 1511168400,
        type: 'NORMAL',
        height: 2.72,
      },
    ],
  },
};

export default tide;
