const report = {
  condition: 1,
  occasionalHeight: 5,
  rating: 'GOOD',
  humanRelation: 'Waist to shoulder high',
  maxHeight: 6,
  minHeight: 3,
};

const daySummary = {
  associated: {
    units: { windSpeed: 'MPH' },
    utcOffset: -10,
  },
  data: {
    conditions: [
      { timestamp: 0, forecaster: {}, am: report, pm: report, observation: 'observation' },
      { timestamp: 1, forecaster: {}, am: report, pm: report, observation: 'observation' },
      { timestamp: 2, forecaster: {}, am: report, pm: report, observation: 'observation' },
      { timestamp: 3, am: report, pm: report, observation: 'observation' },
      { timestamp: 4, am: report, pm: report, observation: 'observation' },
      { timestamp: 5, am: report, pm: report, observation: 'observation' },
    ],
  },
};

export default daySummary;
