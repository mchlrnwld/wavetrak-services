import { transformGraphDataToDays as transformDataToDays } from '@surfline/web-common';

import createReducer from '../../createReducer';
import {
  FETCH_GRAPH_AUTOMATED_RATING_FORECAST,
  FETCH_GRAPH_AUTOMATED_RATING_FORECAST_SUCCESS,
  FETCH_GRAPH_AUTOMATED_RATING_FORECAST_FAILURE,
} from '../../../actions/graphs';
import createFiveDayHourlyWave from './helpers/createFiveDayHourlyData';

/**
 * @typedef {import('../../../propTypes/automatedRatings').AutomatedRating} AutomatedRating
 */

/**
 * @typedef {Object} GraphAutomatedRatingState
 * @property {{ lat: number, lon: number }} [location]
 * @property {string} [error]
 * @property {boolean} loading
 * @property {AutomatedRating[][]} [days]
 * @property {AutomatedRating[][]} [hourly]
 */

/**
 * @type {GraphAutomatedRatingState}
 */
export const initialState = {
  error: null,
  loading: true,
  location: null,
  days: null,
  hourly: null,
};

const handlers = {};

handlers[FETCH_GRAPH_AUTOMATED_RATING_FORECAST] = () => initialState;

handlers[FETCH_GRAPH_AUTOMATED_RATING_FORECAST_SUCCESS] = (
  state,
  { response, desired16DayInterval },
) => {
  /** @type {{ rating: AutomatedRating[] }}  */
  const { rating } = response.data;

  /**
   * @param {AutomatedRating} p
   */
  const mapFunc = (p) => ({
    ...p,
    rating: p.rating.key,
  });

  return {
    ...state,
    loading: false,
    location: response.associated.location,
    days: transformDataToDays(
      rating.filter((_, index) => index % desired16DayInterval === 0),
      desired16DayInterval,
      mapFunc,
    ),
    hourly: createFiveDayHourlyWave(rating, mapFunc),
  };
};

handlers[FETCH_GRAPH_AUTOMATED_RATING_FORECAST_FAILURE] = (state, { error }) => ({
  ...state,
  error,
  loading: false,
});

export default createReducer(handlers, initialState);
