import { transformGraphDataToDays as transformDataToDays } from '@surfline/web-common';
import createReducer from '../../createReducer';
import {
  FETCH_GRAPH_WIND_FORECAST,
  FETCH_GRAPH_WIND_FORECAST_SUCCESS,
  FETCH_GRAPH_WIND_FORECAST_FAILURE,
} from '../../../actions/graphs';
import createFiveDayHourlyWind from './helpers/createFiveDayHourlyData';

export const initialState = {
  error: null,
  loading: true,
  units: null,
  utcOffset: null,
  location: null,
  overallMaxSpeed: null,
  days: null,
};

const handlers = {};

handlers[FETCH_GRAPH_WIND_FORECAST] = () => initialState;

handlers[FETCH_GRAPH_WIND_FORECAST_SUCCESS] = (state, { response, desired16DayInterval }) => ({
  ...state,
  loading: false,
  units: response.associated.units,
  utcOffset: response.associated.utcOffset,
  location: response.associated.location,
  overallMaxSpeed: Math.max(...response.data.wind.map(({ speed }) => speed)),
  days: transformDataToDays(
    response.data.wind.filter((_, index) => index % desired16DayInterval === 0),
    desired16DayInterval,
  ),
  hourly: createFiveDayHourlyWind(response.data.wind),
});

handlers[FETCH_GRAPH_WIND_FORECAST_FAILURE] = (state, { error }) => ({
  ...state,
  error,
  loading: false,
});

export default createReducer(handlers, initialState);
