import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import {
  FETCH_SUBREGION_CHART_IMAGES,
  FETCH_SUBREGION_CHART_IMAGES_SUCCESS,
  FETCH_SUBREGION_CHART_IMAGES_FAILURE,
  SET_ACTIVE_CHART,
} from '../../actions/charts';
import charts from './charts';

describe('reducers / subregion / charts', () => {
  const initialState = deepFreeze({
    loading: false,
    error: null,
    active: {
      type: 'localswell',
      name: 'Wave Height',
      category: 'subregion',
      slug: 'wave-height',
    },
    types: null,
    region: null,
    nearshoreModelName: null,
    images: {
      error: null,
      loading: false,
      images: [],
      timezone: null,
    },
  });

  it('initializes with default values', () => {
    expect(charts()).to.deep.equal(initialState);
  });

  it('sets loading on FETCH_SUBREGION_CHART_IMAGES', () => {
    const action = deepFreeze({
      type: FETCH_SUBREGION_CHART_IMAGES,
    });
    expect(charts(initialState, action)).to.deep.equal({
      ...initialState,
      images: {
        ...initialState.images,
        loading: true,
      },
    });
  });

  it('sets images, loading on FETCH_SUBREGION_CHART_IMAGES_SUCCESS', () => {
    const images = [
      {
        url: 'http://slcharts01.cdn-surfline.com/charts/socal/norange/nearshore/norange_large_1.png',
        timestamp: 1529496000,
      },
      {
        url: 'http://slcharts01.cdn-surfline.com/charts/socal/norange/nearshore/norange_large_2.png',
        timestamp: 1529517600,
      },
    ];
    const timezone = 'PST';
    const action = deepFreeze({
      type: FETCH_SUBREGION_CHART_IMAGES_SUCCESS,
      images,
      timezone,
    });
    expect(charts(initialState, action)).to.deep.equal({
      ...initialState,
      images: {
        ...initialState.images,
        images,
        timezone,
      },
    });
  });

  it('sets error and loading on FETCH_SUBREGION_CHART_IMAGES_FAILURE', () => {
    const action = deepFreeze({
      type: FETCH_SUBREGION_CHART_IMAGES_FAILURE,
      error: {
        message: 'An error occurred',
        status: 500,
      },
    });
    expect(charts(initialState, action)).to.deep.equal({
      ...initialState,
      images: {
        ...initialState.images,
        error: {
          message: 'An error occurred',
          status: 500,
        },
      },
    });
  });

  it('sets active on SET_ACTIVE_CHART', () => {
    const active = {
      type: 'regionalSwell',
      name: 'Wave Height',
      category: 'region',
    };
    const action = deepFreeze({
      type: SET_ACTIVE_CHART,
      active,
    });
    expect(charts(initialState, action)).to.deep.equal({
      ...initialState,
      active,
    });
  });
});
