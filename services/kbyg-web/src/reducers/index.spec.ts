import { expect } from 'chai';
import { CombinedState, Reducer } from 'redux';
import { BackplaneState } from '@surfline/web-common';
import reducers from '.';

describe('reducers', () => {
  it('combines map, spots, status, units, router, subregion, and animations', () => {
    const state = reducers({
      backplane: ((s) => ({
        ...s,
      })) as Reducer<CombinedState<BackplaneState['backplane']>>,
    })(undefined, { type: 'type' });

    expect(state.map).to.be.ok();
    expect(state.mapV2).to.be.ok();
    expect(state.units).to.be.ok();
    expect(state.status).to.be.ok();
    expect(state.animations).to.be.ok();
    expect(state.spot).to.be.ok();
    expect(state.subregion).to.be.ok();
    expect(state.worldTaxonomy).to.be.ok();
    expect(state.components).to.be.ok();
    expect(state.swellEvents).to.be.ok();
    expect(state.deviceData).to.be.ok();
    expect(state.analytics).to.be.ok();
    expect(state.split).to.be.ok();
    expect(state.buoysAPI).to.be.ok();
    expect(state.backplane).to.be.ok();
  });
});
