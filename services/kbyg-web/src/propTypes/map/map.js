import PropTypes from 'prop-types';
import spotPropType from '../spot';
import locationPropType from './location';

export default PropTypes.shape({
  location: locationPropType,
  spots: PropTypes.arrayOf(spotPropType),
  clusters: PropTypes.arrayOf(
    PropTypes.shape({
      geometry: PropTypes.shape({
        coordinates: PropTypes.arrayOf(PropTypes.number),
      }),
      properties: PropTypes.shape({
        spotCount: PropTypes.number,
        worstCondition: PropTypes.string,
        bestCondition: PropTypes.string,
      }),
    }),
  ),
});
