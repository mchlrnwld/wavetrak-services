import PropTypes from 'prop-types';

export const NO_SYNC = 'NoSync';
export const SYNC_ONLY_ON_MAP_INTERACTIONS = 'SyncOnlyOnMapInteractions';
export const ALWAYS_SYNC = 'AlwaysSync';

export const mapUrlSyncPropType = PropTypes.oneOf([
  NO_SYNC,
  SYNC_ONLY_ON_MAP_INTERACTIONS,
  ALWAYS_SYNC,
]);
