import PropTypes from 'prop-types';

/**
 * @typedef {Object} BoundingBox
 * @property {number} north
 * @property {number} south
 * @property {number} east
 * @property {number} west
 */

/**
 * @typedef {Object} Location
 * @property {{ lat: number, lon: number }} center
 * @property {number} zoom
 * @property {BoundingBox} [boundingBox]
 */

export default PropTypes.shape({
  boundingBox: PropTypes.shape({
    north: PropTypes.number.isRequired,
    south: PropTypes.number.isRequired,
    east: PropTypes.number.isRequired,
    west: PropTypes.number.isRequired,
  }),
  center: PropTypes.shape({
    lat: PropTypes.number,
    lon: PropTypes.number,
  }),
  zoom: PropTypes.number,
});
