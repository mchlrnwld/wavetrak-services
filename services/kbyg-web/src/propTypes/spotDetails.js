import PropTypes from 'prop-types';

export default PropTypes.shape({
  subregion: PropTypes.shape({
    _id: PropTypes.string,
  }),
});
