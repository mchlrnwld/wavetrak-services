import PropTypes from 'prop-types';

export default PropTypes.shape({
  height: PropTypes.number.isRequired,
  direction: PropTypes.number.isRequired,
  directionMin: PropTypes.number,
  period: PropTypes.number.isRequired,
  pixels: PropTypes.number,
  color: PropTypes.string,
  index: PropTypes.number,
  optimalScore: PropTypes.oneOf([0, 1, 2]),
});

/**
 * @typedef {object} Swell
 * @property {number} height
 * @property {number} direction
 * @property {number} directionMin
 * @property {number} period
 * @property {number} [pixels]
 * @property {number} index
 * @property {0 | 1 | 2} [optimalScore]
 * @property {string} [color]
 */
