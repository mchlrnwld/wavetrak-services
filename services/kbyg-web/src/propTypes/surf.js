import PropTypes from 'prop-types';

export default PropTypes.shape({
  min: PropTypes.number,
  max: PropTypes.number,
});

/**
 * @typedef {object} Surf
 * @property {number} min
 * @property {number} max
 * @property {number} optimalScore
 */
