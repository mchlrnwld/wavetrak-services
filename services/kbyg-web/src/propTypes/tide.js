import PropTypes from 'prop-types';

export default PropTypes.shape({
  height: PropTypes.number,
  timestamp: PropTypes.number,
  type: PropTypes.string,
});
