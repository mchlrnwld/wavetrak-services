import PropTypes from 'prop-types';

export default PropTypes.shape({
  utcOffset: PropTypes.number,
  days: PropTypes.arrayOf(
    PropTypes.shape({
      dawn: PropTypes.number.isRequired,
      sunrise: PropTypes.number.isRequired,
      sunset: PropTypes.number.isRequired,
      dusk: PropTypes.number.isRequired,
    }),
  ),
});
