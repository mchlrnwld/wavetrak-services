import PropTypes from 'prop-types';

/**
  * @typedef {import('@surfline/web-common')['FEET']} Feet
  * @typedef {import('@surfline/web-common')['METERS']} Meters
  *

/**
  * @typedef {object} Units
  * @property {'F' | 'C'} temperature
  * @property {Feet | Meters} tideHeight
  * @property {Feet | Meters} swellHeight
  * @property {Feet | Meters} waveHeight
  * @property {'MPH' | 'KPH'} windSpeed
  * @property {'MPH' | 'KPH'} speed
  * @property {Feet | Meters} speed
  */

export default PropTypes.shape({
  temperature: PropTypes.string,
  tideHeight: PropTypes.string,
  swellHeight: PropTypes.string,
  waveHeight: PropTypes.string,
  windSpeed: PropTypes.string,
  speed: PropTypes.string,
  distance: PropTypes.string,
});
