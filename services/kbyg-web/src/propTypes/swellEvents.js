import PropTypes from 'prop-types';

export default PropTypes.arrayOf(
  PropTypes.shape({
    id: PropTypes.number,
    permalink: PropTypes.string,
    name: PropTypes.string,
    summary: PropTypes.string,
    thumbnailUrl: PropTypes.string,
    dfpKeyword: PropTypes.string,
    jwPlayerVideoId: PropTypes.string,
    active: PropTypes.bool,
  }),
);
