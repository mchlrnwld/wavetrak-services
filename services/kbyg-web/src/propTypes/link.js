import PropTypes from 'prop-types';

export const pageLevelLinkPropTypes = {
  text: PropTypes.string,
  title: PropTypes.string,
  path: PropTypes.string,
  newWindow: PropTypes.bool,
};

const linkPropType = {
  ...pageLevelLinkPropTypes,
  eventName: PropTypes.string,
  segmentProperties: PropTypes.shape({}),
};

export default linkPropType;
