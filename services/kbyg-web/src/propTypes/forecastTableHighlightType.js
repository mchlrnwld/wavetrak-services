import PropTypes from 'prop-types';

export default PropTypes.oneOf(['OPTIMAL', 'SWELL_HEIGHT']);
