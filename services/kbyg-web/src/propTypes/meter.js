import PropTypes from 'prop-types';

/**
 * @typedef {Object} MeterProp
 * @property {boolean} meteringEnabled
 * @property {boolean} showMeterRegwall
 * @property {number} meterRemaining
 * @property {boolean} isMeteredPremium
 */

const meterPropTypes = PropTypes.shape({
  meteringEnabled: PropTypes.bool,
  showMeterRegwall: PropTypes.bool,
  meterRemaining: PropTypes.number,
  isMeteredPremium: PropTypes.bool,
});

export default meterPropTypes;
