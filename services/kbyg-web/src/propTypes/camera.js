import PropTypes from 'prop-types';

/**
 * @typedef {Object} Camera
 * @property {string} _id
 * @property {string} title
 * @property {string} streamUrl
 * @property {string} alias
 * @property {string} stillUrl
 * @property {string} rewindBaseUrl
 * @property {{ isDown: boolean, message: string, altMessage: string }} status
 * @property {string} lastPrerecordedClipStartTimeUTC
 * @property {string} lastPrerecordedClipEndTimeUTC
 * @property {string} isPrerecorded
 */

export default PropTypes.shape({
  _id: PropTypes.string,
  title: PropTypes.string,
  streamUrl: PropTypes.string,
  alias: PropTypes.string,
  stillUrl: PropTypes.string,
  rewindBaseUrl: PropTypes.string,
  status: PropTypes.shape({
    isDown: PropTypes.bool,
    message: PropTypes.string,
    altMessage: PropTypes.string,
  }),
  lastPrerecordedClipStartTimeUTC: PropTypes.string,
  lastPrerecordedClipEndTimeUTC: PropTypes.string,
  isPrerecorded: PropTypes.bool,
});
