import PropTypes from 'prop-types';

export default PropTypes.shape({
  activeSpotId: PropTypes.string,
});
