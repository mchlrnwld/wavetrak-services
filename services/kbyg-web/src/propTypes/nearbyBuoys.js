import PropTypes from 'prop-types';

export default PropTypes.shape({
  loading: PropTypes.bool,
  error: PropTypes.bool,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      url: PropTypes.string,
      img: PropTypes.string,
    }),
  ),
});
