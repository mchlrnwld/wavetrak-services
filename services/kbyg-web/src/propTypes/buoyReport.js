import PropTypes from 'prop-types';
import unitsPropType from './units';
import buoyReportTimestepPropType from './buoyReportTimestep';

export default PropTypes.shape({
  error: PropTypes.bool,
  loading: PropTypes.bool,
  units: unitsPropType,
  utcOffset: PropTypes.number,
  location: PropTypes.shape({
    lat: PropTypes.number,
    lon: PropTypes.number,
  }),
  offshoreLocation: PropTypes.shape({
    lat: PropTypes.number,
    lon: PropTypes.number,
  }),
  days: PropTypes.arrayOf(PropTypes.arrayOf(buoyReportTimestepPropType)),
});

/**
 * @typedef {object} BuoyReportData
 * @property {boolean} error
 * @property {boolean} loading
 * @property {import('./units')} units
 * @property {number} utcOffset
 * @property {{ lat: number, lon: number }} location
 * @property {{ lat: number, lon: number }} offshoreLocation
 * @property {Array<Array<import('./buoyReportTimestep').BuoyReport>>} days
 */
