import PropTypes from 'prop-types';
import metricsPropType from './metrics';
import swellPropType from './swell';

export default PropTypes.shape({
  timestamp: PropTypes.number,
  utcOffset: PropTypes.number,
  period: PropTypes.number,
  height: PropTypes.number,
  direction: PropTypes.number,
  metrics: metricsPropType,
  swells: PropTypes.arrayOf(swellPropType),
});

/**
 * @typedef {object} BuoyReport
 * @property {number} timestamp
 * @property {number} utcOffset
 * @property {number} period
 * @property {number} height
 * @property {number} direction
 * @property {import('./metrics').Metrics} metrics
 * @property {import('./buoys').BuoySwells[]} swells
 */
