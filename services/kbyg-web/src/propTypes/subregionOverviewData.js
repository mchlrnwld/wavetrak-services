import PropTypes from 'prop-types';

export default PropTypes.shape({
  _id: PropTypes.string,
  name: PropTypes.string,
  primarySpot: PropTypes.string,
  spots: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string,
      name: PropTypes.string,
      conditions: PropTypes.shape({
        human: PropTypes.bool,
        value: PropTypes.string,
      }),
      waveHeight: PropTypes.shape({
        human: PropTypes.bool,
        min: PropTypes.number,
        max: PropTypes.number,
        occasional: PropTypes.number,
        humanRelation: PropTypes.string,
        plus: PropTypes.bool,
      }),
      lat: PropTypes.number,
      lon: PropTypes.number,
    }),
  ),
  timestamp: PropTypes.number,
  forecastSummary: PropTypes.shape({
    forecastStatus: PropTypes.shape({
      status: PropTypes.string,
      inactiveMessage: PropTypes.string,
    }),
    nextForecast: PropTypes.shape({
      timestamp: PropTypes.number,
      utcOffset: PropTypes.number,
    }),
    forecaster: PropTypes.shape({
      name: PropTypes.string,
      title: PropTypes.string,
      email: PropTypes.string,
    }),
    highlights: PropTypes.arrayOf(PropTypes.string),
    breadcrumb: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        href: PropTypes.string,
      }),
    ),
  }),
});
