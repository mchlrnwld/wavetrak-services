import PropTypes from 'prop-types';

export default PropTypes.shape({
  osName: PropTypes.string,
});
