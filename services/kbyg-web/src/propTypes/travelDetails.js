import PropTypes from 'prop-types';

export const abilityLevelsPropType = {
  description: PropTypes.string,
  levels: PropTypes.arrayOf(PropTypes.string),
};

export const abilityLevelsPropTypes = PropTypes.shape(abilityLevelsPropType);

const travelDetailsProps = PropTypes.shape({
  description: PropTypes.string,
  rating: PropTypes.number,
  summary: PropTypes.string,
});

export const travelDetailsPropType = {
  status: PropTypes.string,
  abilityLevels: abilityLevelsPropTypes.isRequired,
  localVibe: travelDetailsProps.isRequired,
  crowdFactor: travelDetailsProps.isRequired,
  spotRating: travelDetailsProps.isRequired,
  shoulderBurn: travelDetailsProps.isRequired,
  waterQuality: travelDetailsProps.isRequired,
};

const travelDetailsPropTypes = PropTypes.shape(travelDetailsPropType);

export default travelDetailsPropTypes;

/**
  * @typedef {"SHORTBOARD" | "FUNBOARD" | "LONGBOARD" | "FISH" | "GUN" | "SUP"} BoardTypes

/**
 * @typedef {object} AbilityLevels
 * @property {string} description
 * @property {("BEGINNER" | "INTERMEDIATE" | "ADVANCED")[]} levels
 * @property {string} summary
 */

/**
 * @typedef {object} Item
 * @property {string} description
 * @property {string[]} value
 */

/**
 * @typedef {object} RatedItem
 * @property {string} description
 * @property {number} rating
 * @property {string} summary
 */

/**
 * @typedef {object} Best
 * @property {Item} season
 * @property {Item} size
 * @property {Item} swellDirection
 * @property {Item} tide
 * @property {Item} windDirection
 */

/**
 * @typedef {object} TravelDetails
 * @property {"PUBLISHED" | "DELETED" | "DRAFT"} status
 * @property {AbilityLevels} abilityLevels
 * @property {string} access
 * @property {Best} best
 * @property {BoardTypes[]} boardTypes
 * @property {Item} bottom
 * @property {string[]} breakType
 * @property {RatedItem} crowdFactor
 * @property {string} description
 * @property {string} hazards
 * @property {RatedItem} localVibe
 * @property {string} relatedArticleId
 * @property {RatedItem} shoulderBurn
 * @property {RatedItem} spotRating
 * @property {string} travelId
 * @property {RatedItem} waterQuality
 */
