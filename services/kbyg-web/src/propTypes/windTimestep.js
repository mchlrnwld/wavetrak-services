import PropTypes from 'prop-types';

export default PropTypes.shape({
  timestamp: PropTypes.number,
  utcOffset: PropTypes.number,
  direction: PropTypes.number,
  speed: PropTypes.number,
  gust: PropTypes.number,
});

/**
 * @typedef {object} Wind
 * @property {number} timestamp
 * @property {number} utcOffset
 * @property {number} direction
 * @property {number} speed
 * @property {number} gust
 * @property {number} optimalScore
 */
