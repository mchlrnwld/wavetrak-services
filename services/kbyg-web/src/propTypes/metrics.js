import PropTypes from 'prop-types';
import { WindPropType } from './wind';

export default PropTypes.shape({
  wind: WindPropType,
  airTemperature: PropTypes.number,
  waterTemperature: PropTypes.number,
  pressure: PropTypes.number,
  dewPoint: PropTypes.number,
});

/**
 * @typedef {object} Metrics
 * @property {import('./wind').Wind} wind
 * @property {number} airTemperature
 * @property {number} waterTemperature
 * @property {number} pressure
 * @property {number} dewPoint
 */
