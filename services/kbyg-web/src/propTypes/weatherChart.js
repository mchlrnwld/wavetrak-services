import PropTypes from 'prop-types';
import unitsPropType from './units';
import weatherPropType from './weather';

export default PropTypes.shape({
  error: PropTypes.shape(),
  loading: PropTypes.bool,
  units: unitsPropType,
  days: PropTypes.arrayOf(PropTypes.arrayOf(weatherPropType)),
});
