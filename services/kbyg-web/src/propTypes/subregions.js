import PropTypes from 'prop-types';

export default PropTypes.shape({
  _id: PropTypes.string,
  subregion: PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    forecasterEmail: PropTypes.string,
  }),
});
