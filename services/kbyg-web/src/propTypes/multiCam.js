import PropTypes from 'prop-types';
import CameraPropTypes from './camera';

export default PropTypes.shape({
  isMultiCam: PropTypes.bool,
  numCams: PropTypes.number,
  cameras: PropTypes.arrayOf(CameraPropTypes),
  initialCameras: PropTypes.arrayOf(CameraPropTypes),
  primaryCam: CameraPropTypes,
  activeView: PropTypes.oneOf(['SINGLE', 'MULTI']),
});
