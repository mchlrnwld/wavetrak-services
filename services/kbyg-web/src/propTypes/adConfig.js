import PropTypes from 'prop-types';

export default PropTypes.shape({
  adUnit: PropTypes.string.isRequired,
  adSizes: PropTypes.arrayOf(PropTypes.string).isRequired,
  adSizeDesktop: PropTypes.arrayOf(PropTypes.string),
  adSizeMobile: PropTypes.arrayOf(PropTypes.string),
  adSizeMappings: PropTypes.arrayOf(PropTypes.string),
  adViewType: PropTypes.string.isRequired,
  adTargets: PropTypes.arrayOf(PropTypes.string).isRequired,
});
