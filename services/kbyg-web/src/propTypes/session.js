import PropTypes from 'prop-types';

// eslint-disable-next-line import/prefer-default-export
export const sessionPropType = PropTypes.shape({
  spot: PropTypes.shape({
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
  }),
  user: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }),
  camera: PropTypes.shape({
    title: PropTypes.string,
    alias: PropTypes.string,
    stillUrl: PropTypes.string,
    isPremium: PropTypes.bool,
    visible: PropTypes.bool,
    id: PropTypes.string,
  }),
});

export const wavePropType = PropTypes.shape({
  clips: PropTypes.arrayOf(
    PropTypes.shape({
      clipUrl: PropTypes.string,
    }),
  ),
  distance: PropTypes.number,
  speedMax: PropTypes.number,
  startTimestamp: PropTypes.number,
  endTimestamp: PropTypes.number,
});
