import PropTypes from 'prop-types';

export default PropTypes.arrayOf(
  PropTypes.shape({
    _id: PropTypes.string,
    name: PropTypes.string,
    legacyId: PropTypes.number,
    offshoreSwellLocation: PropTypes.shape({
      type: PropTypes.string,
      coordinates: PropTypes.arrayOf(PropTypes.number),
    }),
  }),
);
