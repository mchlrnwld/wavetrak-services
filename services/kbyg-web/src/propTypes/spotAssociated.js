import PropTypes from 'prop-types';

export default PropTypes.shape({
  localPhotosUrl: PropTypes.string,
  chartsUrl: PropTypes.string.isRequired,
  beachesUrl: PropTypes.string.isRequired,
});
