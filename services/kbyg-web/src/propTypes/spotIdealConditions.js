import PropTypes from 'prop-types';

const idealPropShape = PropTypes.shape({
  description: PropTypes.string,
  values: PropTypes.arrayOf(PropTypes.string),
});

export default PropTypes.shape({
  season: idealPropShape,
  size: idealPropShape,
  swellDirection: idealPropShape,
  tide: idealPropShape,
  windDirection: idealPropShape,
});
