import PropTypes from 'prop-types';

export default PropTypes.shape({
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  thumbLargeUrl: PropTypes.string,
  alias: PropTypes.string,
  recordingUrl: PropTypes.string,
  thumbSmallUrl: PropTypes.string,
  cameraId: PropTypes.string,
});
