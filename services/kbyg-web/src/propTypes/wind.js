import PropTypes from 'prop-types';

/**
 * @typedef {object} Wind
 * @property {number} speed
 * @property {number} direction
 * @property {number} [gust]
 */

export const WindPropType = PropTypes.shape({
  speed: PropTypes.number.isRequired,
  direction: PropTypes.number.isRequired,
  gust: PropTypes.number,
});
