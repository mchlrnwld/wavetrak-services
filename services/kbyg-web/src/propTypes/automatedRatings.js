import PropTypes from 'prop-types';

/**
 * @typedef {object} AutomatedRating
 * @property {number} timestamp
 * @property {0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9} rating
 * @property {number} utcOffset
 */

const automatedRatingPropType = PropTypes.shape({
  timestamp: PropTypes.number,
  utcOffset: PropTypes.number,
  rating: PropTypes.shape({
    key: PropTypes.oneOf([
      'VERY_POOR',
      'POOR',
      'POOR_TO_FAIR',
      'FAIR',
      'FAIR_TO_GOOD',
      'GOOD',
      'EPIC',
    ]),
  }),
});

export const automatedRatingsPropType = PropTypes.shape({
  error: PropTypes.string,
  loading: PropTypes.bool,
  location: PropTypes.shape({
    lat: PropTypes.number,
    lon: PropTypes.number,
  }),
  days: PropTypes.arrayOf(PropTypes.arrayOf(automatedRatingPropType)),
  hourly: PropTypes.arrayOf(PropTypes.arrayOf(automatedRatingPropType)),
});

export default automatedRatingPropType;
