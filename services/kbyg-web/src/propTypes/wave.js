import PropTypes from 'prop-types';
import swellPropType from './swell';
import surfPropType from './surf';

export default PropTypes.shape({
  timestamp: PropTypes.number,
  surf: surfPropType,
  swells: PropTypes.arrayOf(swellPropType),
  utcOffset: PropTypes.number,
});

/**
 * @typedef {object} Wave
 * @property {number} timestamp
 * @property {number} utcOffset
 * @property {import('./surf').Surf} surf
 * @property {import('./swell').Swell[]} swells
 */
