import charts from './charts';
import chartType from './chartType';
import chartSegmentProps from './chartSegmentProps';

export const chartsPropTypes = charts;
export const chartTypePropTypes = chartType;
export const chartSegmentPropsPropTypes = chartSegmentProps;
