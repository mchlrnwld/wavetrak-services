import PropTypes from 'prop-types';

export default PropTypes.shape({
  pageName: PropTypes.string,
  category: PropTypes.string,
});
