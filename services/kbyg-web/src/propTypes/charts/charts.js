import PropTypes from 'prop-types';
import chartTypePropType from './chartType';

export default PropTypes.shape({
  loading: PropTypes.bool,
  error: PropTypes.string,
  active: PropTypes.shape({
    type: PropTypes.string,
    name: PropTypes.string,
    category: PropTypes.string,
  }),
  types: PropTypes.shape({
    nearshore: chartTypePropType,
    region: chartTypePropType,
    subregion: chartTypePropType,
  }),
  nearshoreModelName: PropTypes.string,
  region: PropTypes.string,
  images: PropTypes.shape({
    loading: PropTypes.bool,
    error: PropTypes.string,
    baseUrl: PropTypes.string,
    end: PropTypes.number,
    increment: PropTypes.number,
    localtime: PropTypes.instanceOf(Date),
    numSteps: PropTypes.number,
  }),
});
