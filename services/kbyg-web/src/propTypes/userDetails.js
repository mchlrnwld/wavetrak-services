import PropTypes from 'prop-types';

export default PropTypes.shape({
  _id: PropTypes.string,
  email: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
});
