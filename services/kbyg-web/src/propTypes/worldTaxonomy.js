import PropTypes from 'prop-types';

const linkPropType = PropTypes.shape({
  key: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
});

const taxonomyPropTypes = {
  _id: PropTypes.string.isRequired,
  geonameId: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  links: PropTypes.arrayOf(linkPropType),
};

export default PropTypes.shape({
  ...taxonomyPropTypes,
  countries: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      countries: PropTypes.arrayOf(
        PropTypes.shape({
          ...taxonomyPropTypes,
        }),
      ).isRequired,
    }),
  ).isRequired,
});
