import PropTypes from 'prop-types';

/**
 * @typedef {Object} Spot
 * @property {string} _id
 * @property {string} name
 */

/**
 * @typedef {Object} SpotReportData
 * @property {string} _id
 * @property {string} name
 * @property {number} lat
 * @property {number} lon
 * @property {{ min: number, max: number, plus?: boolean }} waveHeight
 * @property {{ value: string }} conditions
 */

const spotReportDataPropTypes = PropTypes.shape({
  _id: PropTypes.string,
  name: PropTypes.string,
  lat: PropTypes.number,
  lon: PropTypes.number,
  waveHeight: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number,
    plus: PropTypes.bool,
  }),
  conditions: PropTypes.shape({ value: PropTypes.string }),
});

export default spotReportDataPropTypes;
