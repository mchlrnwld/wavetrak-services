import PropTypes from 'prop-types';
import unitsPropType from './units';
import waveMaxHeightTimestepPropType from './waveMaxHeightTimestep';

export default PropTypes.shape({
  error: PropTypes.shape(),
  loading: PropTypes.bool,
  units: unitsPropType,
  utcOffset: PropTypes.number,
  location: PropTypes.shape({
    lat: PropTypes.number,
    lon: PropTypes.number,
  }),
  forecastLocation: PropTypes.shape({
    lat: PropTypes.number,
    lon: PropTypes.number,
  }),
  offshoreLocation: PropTypes.shape({
    lat: PropTypes.number,
    lon: PropTypes.number,
  }),
  overallMaxSurfHeight: PropTypes.number,
  overallMaxSwellHeight: PropTypes.number,
  days: PropTypes.arrayOf(PropTypes.arrayOf(waveMaxHeightTimestepPropType)),
});
