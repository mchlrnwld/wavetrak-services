import PropTypes from 'prop-types';

export default PropTypes.shape({
  timestamp: PropTypes.number,
  surf: PropTypes.shape({
    min: PropTypes.number,
    max: PropTypes.number,
  }),
});
