import PropTypes from 'prop-types';
import { OFFLINE, ONLINE } from '../common/constants';
import { WindPropType } from './wind';

/**
 * @typedef {number} EpochTimestamp
 */

/**
 * @typedef {object} BuoySwell
 * @property {number} [height]
 * @property {number} [direction]
 * @property {number} [period]
 * @property {number} [spread]
 * @property {number} [impact]
 */

/**
 * @typedef {Array<BuoySwell>} BuoySwells
 */

/**
 * @typedef {object} StationData
 * @property {EpochTimestamp} timestamp
 * @property {number} utcOffset
 * @property {number} height
 * @property {number} period
 * @property {number} direction
 * @property {number} [sst]
 * @property {number} [airTemperature]
 * @property {number} [waterTemperature]
 * @property {number} [dewPoint]
 * @property {number} [pressure]
 * @property {number} [elevation]
 * @property {import('./wind').Wind} [wind]
 * @property {BuoySwells} [swells]
 */

/**
 * @typedef {object} StationInfo
 * @property {string} Station.id
 * @property {string} [Station.name]
 * @property {string} [Station.sourceId]
 * @property {string} [Station.source]
 * @property {ONLINE | OFFLINE} Station.status
 * @property {number} Station.latitude
 * @property {number} Station.longitude
 * @property {EpochTimestamp} [Station.latestTimestamp]
 * @property {number} [Station.utcOffset]
 */

/**
 * @typedef {object} Station
 * @property {StationInfo} Station.station
 * @property {StationData} Station.data
 */

/**
 * @typedef {object} NearbyStation
 * @property {StationInfo['id']} id
 * @property {StationInfo['name']} [name]
 * @property {StationInfo['latitude']} latitude
 * @property {StationInfo['longitude']} longitude
 * @property {StationInfo['status']} status
 * @property {StationInfo['sourceId']} sourceId
 * @property {string} abbrTimezone
 * @property {StationData} latestData
 */

const statusPropType = PropTypes.oneOf([OFFLINE, ONLINE]).isRequired;

export const BuoySwellPropType = PropTypes.shape({
  height: PropTypes.number,
  period: PropTypes.number,
  direction: PropTypes.number,
  spread: PropTypes.number,
  impact: PropTypes.number,
});

export const StationDataPropType = PropTypes.shape({
  timestamp: PropTypes.number.isRequired,
  utcOffset: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  period: PropTypes.number.isRequired,
  direction: PropTypes.number.isRequired,
  sst: PropTypes.number,
  airTemperature: PropTypes.number,
  dewPoint: PropTypes.number,
  pressure: PropTypes.number,
  wind: WindPropType,
  elevation: PropTypes.number,
  swells: PropTypes.arrayOf(BuoySwellPropType),
});

export const StationInfoPropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  name: PropTypes.string,
  status: statusPropType,
  latitude: PropTypes.number.isRequired,
  longitude: PropTypes.number.isRequired,
  latestTimestamp: PropTypes.number,
  utcOffset: PropTypes.number,
});

export const StationPropType = PropTypes.shape({
  station: StationInfoPropType.isRequired,
  data: StationDataPropType.isRequired,
});

export const NearbyStationPropType = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string,
  status: statusPropType,
  latitude: PropTypes.number.isRequired,
  longitude: PropTypes.number.isRequired,
  abbrTimezone: PropTypes.string.isRequired,
  latestData: StationDataPropType.isRequired,
};
