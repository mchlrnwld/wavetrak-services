import PropTypes from 'prop-types';

export default PropTypes.shape({
  paywallWidth: PropTypes.number,
  timestepWidth: PropTypes.number,
  paywallLeft: PropTypes.number,
});
