import PropTypes from 'prop-types';
import unitsPropType from './units';

export default PropTypes.shape({
  error: PropTypes.shape(),
  loading: PropTypes.bool,
  units: unitsPropType,
  days: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.shape)),
});
