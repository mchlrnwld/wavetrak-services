import PropTypes from 'prop-types';

export default PropTypes.shape({
  treatment: PropTypes.string,
  loading: PropTypes.bool,
});
