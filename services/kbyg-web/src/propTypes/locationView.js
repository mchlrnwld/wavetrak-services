import PropTypes from 'prop-types';

/**
 * @typedef {number} Latitude
 *
 * @typedef {number} Longitude
 *
 * @typedef {[Longitude, Latitude]} Coordinates
 *
 * @typedef {Object} Location
 * @property {"Point"} type
 * @property {Coordinates} coordinates
 *
 * @typedef {Object} Link
 * @property {string} key
 * @property {string} href
 *
 * @typedef {Object} Geoname
 * @property {string} fcode
 * @property {string} lat
 * @property {string} adminName1
 * @property {string} fcodeName
 * @property {string} countryName
 * @property {string} fclName
 * @property {string} name
 * @property {string} countryCode
 * @property {number} population
 * @property {string} fcl
 * @property {string} countryId
 * @property {string} toponyName
 * @property {number} geonameId
 * @property {string} lon
 * @property {number} adminCode1
 *
 * @typedef {Object} Taxonomy
 * @property {Location} location
 * @property {string} _id
 * @property {string} [spot]
 * @property {number} [geonameId]
 * @property {string} type
 * @property {string[]} liesIn
 * @property {Geoname} [geonames]
 * @property {string} [enumeratedPath]
 * @property {string} name
 * @property {string} category
 * @property {boolean} hasSpots
 * @property {?number} [depth]
 * @property {?number} [distance]
 * @property {{ links: Link[]}} associated
 * @property {?Taxonomy[]} [in]
 * @property {?Taxonomy[]} [contains]
 *
 * @typedef {Object} BreadCrumb
 * @property {string} name
 * @property {string} url
 * @property {number} geonameId
 * @property {string} id
 *
 * @typedef {Object} BoundingBox
 * @property {number} north
 * @property {number} south
 * @property {number} east
 * @property {number} west
 *
 * @typedef {Object} TravelContent
 * @property {string} id
 * @property {string} contentType
 * @property {number} createdAt
 * @property {number} updatedAt
 * @property {{ title: string, subtitle: string, summary: string, body: string }} content
 * @property {{ large: string, medium: string, type: string, thumbnail: string, full: string }} media
 *
 * @typedef {Object} LocationView
 * @property {boolean} loading
 * @property {Partial<Taxonomy>} taxonomy
 * @property {BreadCrumb[]} breadCrumbs
 * @property {BoundingBox} [boundingBox]
 * @property {Taxonomy[]} siblings
 * @property {string} url
 * @property {TravelContent} [travelContent]
 */

export default PropTypes.shape({
  loading: PropTypes.bool,
  url: PropTypes.string,
  taxonomy: PropTypes.shape({
    location: PropTypes.shape({
      coordinates: PropTypes.arrayOf(PropTypes.number),
    }),
    name: PropTypes.string,
  }),
  boundingBox: PropTypes.shape({
    north: PropTypes.number,
    south: PropTypes.number,
    east: PropTypes.number,
    west: PropTypes.number,
  }),
  travelContent: PropTypes.shape({
    id: PropTypes.string,
    contentType: PropTypes.string,
    createdAt: PropTypes.number,
    updatedAt: PropTypes.number,
    content: PropTypes.shape({
      title: PropTypes.string,
      subtitle: PropTypes.string,
      summary: PropTypes.string,
      body: PropTypes.string,
    }),
    media: PropTypes.shape({
      large: PropTypes.string,
      medium: PropTypes.string,
      type: PropTypes.string,
      thumbnail: PropTypes.string,
      full: PropTypes.string,
    }),
  }),
});
