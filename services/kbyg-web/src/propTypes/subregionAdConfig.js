import PropTypes from 'prop-types';

export default PropTypes.shape({
  adTarget: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
  userEntitlements: PropTypes.arrayOf(PropTypes.string).isRequired,
  isUser: PropTypes.bool.isRequired,
});
