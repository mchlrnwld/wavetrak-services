import PropTypes from 'prop-types';
import summaryDayPropType from './summaryDay';
import unitsPropType from './units';

export default PropTypes.shape({
  days: PropTypes.arrayOf(summaryDayPropType),
  utcOffset: PropTypes.number,
  units: unitsPropType,
});
