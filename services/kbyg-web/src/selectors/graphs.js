import { createSelector } from 'reselect';

/**
 * @typedef {import('../stores').AppState} State
 */

const mobileOrDesktopData = (days, mobile, extended) => {
  if (!days) return days;
  if (mobile && !extended) {
    return days.map((data) => data.filter((_, index) => index % 3 === 0));
  }
  return days;
};

export const getChartSunlightTimes = (state) => state.components.graphs.sunlightTimes;

export const getChartDaySummary = (state) => state.components.graphs.daySummary;

export const getChartWave = createSelector(
  (state) => state.components.graphs.wave,
  (wave, mobile, extended) => ({
    ...wave,
    days: mobileOrDesktopData(wave.days, mobile, extended),
  }),
);

export const getWaveMaxHeights = createSelector(
  (state) => state.components.graphs.waveMaxHeights,
  (wave, mobile, extended) => ({
    ...wave,
    days: mobileOrDesktopData(wave.days, mobile, extended),
  }),
);

export const getChartTide = (state) => state.components.graphs.tide;

export const getChartWeather = createSelector(
  (state) => state.components.graphs.weather,
  (_, props) => props,
  (weather, { device: { mobile }, extended }) => ({
    ...weather,
    days: mobileOrDesktopData(weather.days, mobile, extended),
  }),
);

export const getChartWind = createSelector(
  (state) => state.components.graphs.wind,
  (_, props) => props,
  (wind, { device: { mobile }, extended }) => ({
    ...wind,
    days: mobileOrDesktopData(wind.days, mobile, extended),
  }),
);

/**
 * @param {State} state
 */
export const getAutomatedRatings = (state) => state.components.graphs.automatedRatings;
