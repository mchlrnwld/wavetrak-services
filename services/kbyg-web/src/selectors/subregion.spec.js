import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import * as selectors from './subregion';
import stateFixture from './fixtures/state';

describe('selectors / subregion', () => {
  const state = deepFreeze(stateFixture);

  it('gets a subregion overview fetch state', () => {
    const fetched = selectors.getSubregionOverviewFetched(state);
    expect(fetched).to.equal(false);
  });
});
