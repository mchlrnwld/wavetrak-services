import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import * as selectors from './spot';
import stateFixture from './fixtures/state';

describe('selectors / spot', () => {
  const state = deepFreeze(stateFixture);

  it('gets a spot report', () => {
    const report = selectors.getSpotReport(state);
    expect(report).to.have.property('forecaster');
    expect(report).to.have.property('body');
  });

  it("gets a spot's report data", () => {
    const forecast = selectors.getSpotReportData(state);
    expect(forecast).to.have.property('conditions');
  });

  it('gets spot details', () => {
    const details = selectors.getSpotDetails(state);
    expect(details).to.have.property('name');
    expect(details).to.have.property('lat');
    expect(details).to.have.property('lon');
  });

  it('gets nearby spots', () => {
    const nearby = selectors.getNearbySpots(state);
    expect(nearby).to.have.length.above(2);
  });
});
