/* eslint-disable import/prefer-default-export */
import { AppState } from '../stores';

export const getStatusCode = (state: AppState) => state.status.code;
