/* eslint-disable import/prefer-default-export */
export const getOSData = (state) => {
  const data = state.deviceData.osData;
  if (data) {
    return data;
  }
  return null;
};
