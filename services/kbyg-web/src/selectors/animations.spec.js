import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import stateFixture from './fixtures/state';
import * as selectors from './animations';

describe('selectors / animations', () => {
  const state = deepFreeze(stateFixture);

  it('gets spot list scroll position', () => {
    const scrollPosition = selectors.getSpotListScrollPosition(state);
    expect(scrollPosition).to.equal(state.animations.scrollPositions.spotList);
  });

  it('gets cam list scroll position', () => {
    const scrollPosition = selectors.getCamListScrollPosition(state);
    expect(scrollPosition).to.equal(state.animations.scrollPositions.camList);
  });
});
