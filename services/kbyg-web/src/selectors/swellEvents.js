const getSwellEvents = (state) => state.swellEvents.events || [];

export default getSwellEvents;
