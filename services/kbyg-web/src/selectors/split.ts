import { getTreatment } from '@surfline/web-common';
import { useAppSelector } from '../stores/hooks';

import { AppState } from '../stores';

export const getSplitReady = (state: AppState) => state.split?.ready;

export const useSplitTreatment = (treatmentName: string) => {
  const splitReady = useAppSelector(getSplitReady);
  const treatment = getTreatment(treatmentName);
  return { loading: !splitReady, treatment };
};
