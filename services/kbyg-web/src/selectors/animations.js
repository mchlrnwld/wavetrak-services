const getScrollPosition = (component) => (state) => state.animations.scrollPositions[component];

export const getSpotListScrollPosition = getScrollPosition('spotList');
export const getCamListScrollPosition = getScrollPosition('camList');
