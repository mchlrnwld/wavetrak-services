export const getPrimarySpotId = (state) => state.subregion.primarySpotId;
export const getSubregionFeed = (state) => state.subregion.feed;
export const getSubregionId = (state) => state.subregion.subregionId;

export const getSubregionAssociated = (state) => {
  const overview = state?.subregion?.overview;
  if (overview) {
    return overview.associated;
  }
  return null;
};

export const getSubregionOverviewLoading = (state) => state.subregion.overview.loading;

export const getSubregionOverviewData = (state) => {
  const overview = state?.subregion?.overview?.overview;
  if (overview) {
    return overview.data;
  }
  return null;
};

export const getNearbySubregions = (state) => {
  const data = state.subregion && state.subregion.nearby ? state.subregion.nearby.data : null;
  if (data) {
    return data.subregions;
  }
  return null;
};

export const getSubregionOverviewFetched = (state) => state.subregion.overview.fetched;

/**
 * @param {import('../stores').AppState} state
 */
export const getSubregionLoading = (state) => state.subregion.loading;
