import { initialState as worldTaxonomyInitialState } from '../../reducers/worldTaxonomy';
import { initialState as swellEventsInitialState } from '../../reducers/swellEvents';
import { initialState as deviceDataInitialState } from '../../reducers/deviceData';
import { initialState as analyticsInitialState } from '../../reducers/analytics';
import { initialState as splitInitialState } from '../../reducers/split';
import { initialState as chartsComponentsInitialState } from '../../reducers/components/charts';
import { initialState as sunlightTimesInitialState } from '../../reducers/components/graphs/sunlightTimes';
import { initialState as daySummaryInitialState } from '../../reducers/components/graphs/daySummary';
import { initialState as waveInitialState } from '../../reducers/components/graphs/wave';
import { initialState as waveMaxHeightsInitialState } from '../../reducers/components/graphs/waveMaxHeights';
import { initialState as tideInitialState } from '../../reducers/components/graphs/tide';
import { initialState as weatherInitialState } from '../../reducers/components/graphs/weather';
import { initialState as windInitialState } from '../../reducers/components/graphs/wind';
import { initialState as automatedRatingsInitialState } from '../../reducers/components/graphs/automatedRatings';
import { initialState as featuredContentInitialState } from '../../reducers/spot/featuredContent';
import { initialState as multiCamInitialState } from '../../reducers/spot/multiCam';
import { initialState as buoysInitialState } from '../../reducers/spot/buoys';
import { buoysAPI } from '../../common/api/buoys';
import { AppState } from '../../stores';

const state: AppState = {
  worldTaxonomy: worldTaxonomyInitialState,
  components: {
    charts: chartsComponentsInitialState,
    graphs: {
      sunlightTimes: sunlightTimesInitialState,
      daySummary: daySummaryInitialState,
      wave: waveInitialState,
      waveMaxHeights: waveMaxHeightsInitialState,
      tide: tideInitialState,
      weather: weatherInitialState,
      wind: windInitialState,
      automatedRatings: automatedRatingsInitialState,
    },
  },
  swellEvents: swellEventsInitialState,
  deviceData: deviceDataInitialState,
  analytics: analyticsInitialState,
  split: splitInitialState,
  buoysAPI: buoysAPI.reducer(undefined, { type: 'INIT' }),
  map: {
    rerenderMap: { doRerender: false },
    clusters: [],
    location: {
      center: {},
      zoom: 12,
    },
    spots: [],
    subregions: {
      doCluster: true,
      subregions: [],
    },
    regionalForecast: {},
    status: {
      loading: false,
      error: null,
    },
    locationView: {
      loading: false,
      taxonomy: {
        location: {
          type: 'Point',
          coordinates: [0, 0],
        },
      },
      breadCrumbs: [],
      siblings: [],
      url: 'url',
    },
  },
  mapV2: {
    location: {
      center: { lat: 0, lon: 0 },
      zoom: 12,
    },
    locationLoaded: true,
    boundingBox: {
      north: 1,
      south: 1,
      east: 1,
      west: 1,
    },
    loading: false,
    message: '',
    locationView: {
      loading: false,
      taxonomy: {
        _id: '58f7ed54dadb30820bb38b79',
        geonameId: 5393052,
        type: 'geoname',
        liesIn: ['58f7ed54dadb30820bb38b6a'],
        geonames: {
          fcode: 'PPLA2',
          lat: '36.97412',
          adminName1: 'California',
          fcodeName: 'seat of a second-order administrative division',
          countryName: 'United States',
          fclName: 'city, village,...',
          name: 'Santa Cruz',
          countryCode: 'US',
          population: 64220,
          fcl: 'P',
          countryId: '6252001',
          toponyName: 'Santa Cruz',
          geonameId: 5393052,
          lon: '-122.0308',
          adminCode1: 1,
        },
        location: {
          coordinates: [-122.0308, 36.97412],
          type: 'Point',
        },
        enumeratedPath:
          ',Earth,North America,United States,California,Santa Cruz County,Santa Cruz',
        name: 'Santa Cruz',
        category: 'geonames',
        hasSpots: true,
        associated: {
          links: [
            {
              key: 'taxonomy',
              href: 'http://spots-api.sandbox.surfline.com/taxonomy?id=5393052&type=geoname',
            },
            {
              key: 'www',
              href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/santa-cruz-county/santa-cruz/5393052',
            },
            {
              key: 'travel',
              href: 'https://sandbox.surfline.com/travel/united-states/california/santa-cruz-county/santa-cruz-surfing-and-beaches/5393052',
            },
          ],
        },
        in: [
          {
            _id: '58f7ed54dadb30820bb38b6a',
            geonameId: 5393068,
            type: 'geoname',
            liesIn: ['58f7ed51dadb30820bb387a6'],
            geonames: {
              fcode: 'ADM2',
              lat: '37.02161',
              adminName1: 'California',
              fcodeName: 'second-order administrative division',
              countryName: 'United States',
              fclName: 'country, state, region,...',
              name: 'Santa Cruz County',
              countryCode: 'US',
              population: 262382,
              fcl: 'A',
              countryId: '6252001',
              toponyName: 'Santa Cruz County',
              geonameId: 5393068,
              lon: '-122.00979',
              adminCode1: 1,
            },
            location: {
              coordinates: [-122.00979, 37.02161],
              type: 'Point',
            },
            enumeratedPath: ',Earth,North America,United States,California,Santa Cruz County',
            name: 'Santa Cruz County',
            category: 'geonames',
            hasSpots: true,
            depth: 1,
            associated: {
              links: [
                {
                  key: 'taxonomy',
                  href: 'http://spots-api.sandbox.surfline.com/taxonomy?id=5393068&type=geoname',
                },
                {
                  key: 'www',
                  href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/santa-cruz-county/5393068',
                },
                {
                  key: 'travel',
                  href: 'https://sandbox.surfline.com/travel/united-states/california/santa-cruz-county-surfing-and-beaches/5393068',
                },
              ],
            },
          },
        ],
        contains: [
          {
            geonameId: 5393052,
            enumeratedPath:
              ',Earth,North America,United States,California,Santa Cruz County,Santa Cruz',
            geonames: {
              fcode: 'PPLA2',
              lat: '36.97412',
              adminName1: 'California',
              fcodeName: 'seat of a second-order administrative division',
              countryName: 'United States',
              fclName: 'city, village,...',
              name: 'Santa Cruz',
              countryCode: 'US',
              population: 64220,
              fcl: 'P',
              countryId: '6252001',
              toponyName: 'Santa Cruz',
              geonameId: 5393052,
              lon: '-122.0308',
              adminCode1: 1,
            },
            _id: '58f80ab7dadb30820bd14cbc',
            type: 'spot',
            liesIn: ['58f7ed54dadb30820bb38b79', '58f7ed54dadb30820bb38b94'],
            location: {
              coordinates: [-122.02321529388428, 36.960814372533086],
              type: 'Point',
            },
            name: 'Cowells Overview',
            category: 'surfline',
            hasSpots: false,
            depth: 0,
            associated: {
              links: [
                {
                  key: 'taxonomy',
                  href: 'http://spots-api.sandbox.surfline.com/taxonomy?id=584204214e65fad6a7709d20&type=spot',
                },
                {
                  key: 'api',
                  href: 'http://spots-api.sandbox.surfline.com/admin/spots/584204214e65fad6a7709d20',
                },
                {
                  key: 'www',
                  href: 'https://sandbox.surfline.com/surf-report/cowells-overview/584204214e65fad6a7709d20',
                },
              ],
            },
          },
        ],
      },
      breadCrumbs: [
        {
          name: 'United States',
          url: '/united-states/6252001',
          geonameId: 6252001,
          id: '58f7ed51dadb30820bb3879c',
        },
        {
          name: 'California',
          url: '/united-states/california/5332921',
          geonameId: 5332921,
          id: '58f7ed51dadb30820bb387a6',
        },
        {
          name: 'Santa Cruz County',
          url: '/united-states/california/santa-cruz-county/5393068',
          geonameId: 5393068,
          id: '58f7ed54dadb30820bb38b6a',
        },
        {
          name: 'Santa Cruz',
          url: '/united-states/california/santa-cruz-county/santa-cruz/5393052',
          geonameId: 5393052,
          id: '58f7ed54dadb30820bb38b79',
        },
      ],
      boundingBox: {
        north: 36.96632852130351,
        south: 36.94785295955728,
        east: -121.9628,
        west: -122.1264825495626,
      },
      siblings: [
        {
          _id: '58f7edd1dadb30820bb4144b',
          geonameId: 5330222,
          type: 'geoname',
          liesIn: ['58f7ed54dadb30820bb38b6a'],
          geonames: {
            fcode: 'PPL',
            lat: '37.12606',
            adminName1: 'California',
            fcodeName: 'populated place',
            countryName: 'United States',
            fclName: 'city, village,...',
            name: 'Boulder Creek',
            countryCode: 'US',
            population: 4923,
            fcl: 'P',
            countryId: '6252001',
            toponyName: 'Boulder Creek',
            geonameId: 5330222,
            lon: '-122.12219',
            adminCode1: 1,
          },
          location: {
            coordinates: [-122.12219, 37.12606],
            type: 'Point',
          },
          enumeratedPath:
            ',Earth,North America,United States,California,Santa Cruz County,Boulder Creek',
          name: 'Boulder Creek',
          category: 'geonames',
          hasSpots: true,
          depth: 0,
          associated: {
            links: [
              {
                key: 'taxonomy',
                href: 'http://spots-api.sandbox.surfline.com/taxonomy?id=5330222&type=geoname',
              },
              {
                key: 'www',
                href: 'https://sandbox.surfline.com/surf-reports-forecasts-cams/united-states/california/santa-cruz-county/boulder-creek/5330222',
              },
              {
                key: 'travel',
                href: 'https://sandbox.surfline.com/travel/united-states/california/santa-cruz-county/boulder-creek-surfing-and-beaches/5330222',
              },
            ],
          },
          distance: 18.739940240955768,
        },
      ],
      url: '/united-states/california/santa-cruz-county/santa-cruz/5393052',
    },
  },
  units: {
    tideHeight: null,
    waveHeight: null,
    windSpeed: null,
  },
  status: {
    code: 200,
  },
  animations: {
    scrollPositions: {
      spotList: {
        top: 10,
        left: 15,
      },
      camList: {
        top: 20,
        left: 25,
      },
    },
  },
  spot: {
    loading: false,
    buoys: buoysInitialState,
    featuredContent: featuredContentInitialState,
    multiCam: multiCamInitialState,
    details: {
      data: {
        name: 'Windansea',
      },
      loading: false,
    },
    report: {
      loading: false,
      associated: {
        units: {
          windSpeed: 'KPH',
          waveHeight: 'M',
          tideHeight: 'M',
        },
      },
      data: {
        spot: {
          name: 'Windansea',
          breadCrumbs: [],
          lat: 32.829151647032,
          lon: -117.2822713851,
          cameras: [
            {
              title: 'Windansea',
              streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253476/d5adaa4c/playlist.m3u8',
              stillUrl: 'http://camstills.cdn-surfline.com/windanseacam/latest_small.jpg',
              rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/windanseacam/windanseacam',
              _id: '58acb10d8816e20012bb4478',
              status: {
                isDown: false,
                message: '',
              },
            },
          ],
        },
        forecast: {
          conditions: {
            human: true,
            value: 'FAIR',
          },
          wind: {
            speed: 7.34,
            direction: 334.26,
          },
          waveHeight: {
            human: true,
            min: 1.2,
            max: 1.8,
            occasional: 2.1,
            humanRelation: 'shoulder high to 1 ft overhead',
          },
          tide: {
            previous: {
              type: 'LOW',
              height: 0,
              timestamp: 1487710022,
              utcOffset: -8,
            },
            next: {
              type: 'HIGH',
              height: 1,
              timestamp: 1487733721,
              utcOffset: -8,
            },
          },
        },
        report: {
          timestamp: 1486935689,
          forecaster: {
            name: 'Keaton Browning',
            iconUrl: 'https://www.gravatar.com/avatar/a7628271b83f93b6361514e747566ee4?d=mm',
            title: 'Forecaster',
          },
          body: "<p><strong>Afternoon Report for South SD: </strong>Winds have switched out of the WNW this afternoon, putting good amount of bump/chop on the water. Old Westerly swell mix fades for mainly 3-5' jumbled waves at top spots. Overall, pretty dismal out there due to this onshore flow. Dropping tide.&nbsp;</p>\n<p><br></p>\n<p><strong>Short-Term Forecast for South SD:</strong> Tide bottoms out at a -.6' low at 4:28pm, draining things late afternoon.&nbsp;Winds continue out of the WNW-NW direction, topping out at moderate+ levels. Expect smaller surf by the evening with the combination of drained tide.</p>\n<p><br></p>\n<p><strong>Note:</strong> County health officials advise against ocean water contact for up to 72 hours following a period of significant rainfall due to elevated bacteria levels - especially near drain pipes, harbors and river mouths.</p>",
        },
      },
    },
    nearby: {
      loading: false,
      data: {
        spots: [
          {
            _id: '584204204e65fad6a7709994',
            lat: 32.927,
            lon: -117.265,
            name: 'Torrey Pines State Beach',
            cameras: [
              {
                title: 'Torrey Pines State Beach',
                streamUrl:
                  'https://wowzaprod3-i.akamaihd.net/hls/live/253476/d9f7ca26/playlist.m3u8',
                stillUrl: 'http://camstills.cdn-surfline.com/torreypinescam/latest_small.jpg',
                rewindBaseUrl:
                  'http://live-cam-archive.cdn-surfline.com/torreypinescam/torreypinescam',
                _id: '58acb0e48816e20012bb4447',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
            conditions: {
              human: true,
              value: 'FAIR',
            },
            wind: {
              speed: 6.2,
              direction: 323.96,
            },
            waveHeight: {
              human: true,
              min: 1.2,
              max: 1.5,
              occasional: 1.8,
              humanRelation: 'shoulder to head high',
            },
            tide: {
              previous: {
                type: 'LOW',
                height: 0,
                timestamp: 1487710022,
                utcOffset: -8,
              },
              next: {
                type: 'HIGH',
                height: 1,
                timestamp: 1487733721,
                utcOffset: -8,
              },
            },
            rank: 0,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 107951,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a770883b',
            lat: 32.877231,
            lon: -117.253031,
            name: 'Blacks',
            cameras: [],
            conditions: {
              human: true,
              value: 'FAIR',
            },
            wind: {
              speed: 5.02,
              direction: 330.5,
            },
            waveHeight: {
              human: true,
              min: 1.2,
              max: 1.8,
              occasional: 2.4,
              humanRelation: 'shoulder high to 1 ft overhead',
            },
            tide: {
              previous: {
                type: 'LOW',
                height: 0,
                timestamp: 1487710022,
                utcOffset: -8,
              },
              next: {
                type: 'HIGH',
                height: 1,
                timestamp: 1487733721,
                utcOffset: -8,
              },
            },
            rank: 1,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4245,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a7708839',
            lat: 32.867062083147026,
            lon: -117.25773453712463,
            name: 'Scripps',
            cameras: [
              {
                title: 'Scripps',
                streamUrl:
                  'https://wowzaprod3-i.akamaihd.net/hls/live/253451/b9ac605e/playlist.m3u8',
                stillUrl: 'http://camstills.cdn-surfline.com/scrippscam/latest_small.jpg',
                rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/scrippscam/scrippscam',
                _id: '58acb0ea8816e20012bb444f',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
            conditions: {
              human: true,
              value: 'FAIR',
            },
            wind: {
              speed: 4.62,
              direction: 330.06,
            },
            waveHeight: {
              human: true,
              min: 0.9,
              max: 1.5,
              occasional: 1.8,
              humanRelation: 'waist to head high',
            },
            tide: {
              previous: {
                type: 'LOW',
                height: 0,
                timestamp: 1487710022,
                utcOffset: -8,
              },
              next: {
                type: 'HIGH',
                height: 1,
                timestamp: 1487733721,
                utcOffset: -8,
              },
            },
            rank: 2,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4246,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a77088cc',
            lat: 32.863,
            lon: -117.257,
            name: 'La Jolla Shores',
            cameras: [
              {
                title: 'La Jolla Shores',
                streamUrl:
                  'https://wowzaprod3-i.akamaihd.net/hls/live/253362/486d86c1/playlist.m3u8',
                stillUrl: 'http://camstills.cdn-surfline.com/lajollashorescam/latest_small.jpg',
                rewindBaseUrl:
                  'http://live-cam-archive.cdn-surfline.com/lajollashorescam/lajollashorescam',
                _id: '58acb06b8816e20012bb43ba',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
            conditions: {
              human: true,
              value: 'FAIR',
            },
            wind: {
              speed: 4.82,
              direction: 334.31,
            },
            waveHeight: {
              human: true,
              min: 0.9,
              max: 1.5,
              occasional: 1.5,
              humanRelation: 'waist to head high',
            },
            tide: {
              previous: {
                type: 'LOW',
                height: 0,
                timestamp: 1487710022,
                utcOffset: -8,
              },
              next: {
                type: 'HIGH',
                height: 1,
                timestamp: 1487733721,
                utcOffset: -8,
              },
            },
            rank: 3,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4812,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a770883d',
            lat: 32.838278,
            lon: -117.283963,
            name: 'Horseshoe',
            cameras: [],
            conditions: {
              human: false,
              value: null,
            },
            wind: {
              speed: 7.33,
              direction: 334.33,
            },
            waveHeight: {
              human: false,
              min: 1.2,
              max: 1.4,
              occasional: null,
            },
            tide: {
              previous: {
                type: 'LOW',
                height: 0,
                timestamp: 1487710022,
                utcOffset: -8,
              },
              next: {
                type: 'HIGH',
                height: 1,
                timestamp: 1487733721,
                utcOffset: -8,
              },
            },
            rank: 4,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4247,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a770883c',
            lat: 32.829151647032,
            lon: -117.2822713851,
            name: 'Windansea',
            cameras: [
              {
                title: 'Windansea',
                streamUrl:
                  'https://wowzaprod3-i.akamaihd.net/hls/live/253476/d5adaa4c/playlist.m3u8',
                stillUrl: 'http://camstills.cdn-surfline.com/windanseacam/latest_small.jpg',
                rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/windanseacam/windanseacam',
                _id: '58acb10d8816e20012bb4478',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
            conditions: {
              human: true,
              value: 'FAIR',
            },
            wind: {
              speed: 7.34,
              direction: 334.26,
            },
            waveHeight: {
              human: true,
              min: 1.2,
              max: 1.8,
              occasional: 2.1,
              humanRelation: 'shoulder high to 1 ft overhead',
            },
            tide: {
              previous: {
                type: 'LOW',
                height: 0,
                timestamp: 1487710022,
                utcOffset: -8,
              },
              next: {
                type: 'HIGH',
                height: 1,
                timestamp: 1487733721,
                utcOffset: -8,
              },
            },
            rank: 5,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4248,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a770883a',
            lat: 32.813094,
            lon: -117.273807,
            name: 'Birdrock',
            cameras: [],
            conditions: {
              human: true,
              value: 'FAIR',
            },
            wind: {
              speed: 8.32,
              direction: 333.59,
            },
            waveHeight: {
              human: true,
              min: 1.2,
              max: 1.8,
              occasional: 2.4,
              humanRelation: 'shoulder high to 1 ft overhead',
            },
            tide: {
              previous: {
                type: 'LOW',
                height: 0,
                timestamp: 1487710022,
                utcOffset: -8,
              },
              next: {
                type: 'HIGH',
                height: 1,
                timestamp: 1487733721,
                utcOffset: -8,
              },
            },
            rank: 6,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4249,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a77088c2',
            lat: 32.80662846492798,
            lon: -117.2688764333725,
            name: 'PB Point',
            cameras: [],
            conditions: {
              human: false,
              value: null,
            },
            wind: {
              speed: 8.57,
              direction: 330.62,
            },
            waveHeight: {
              human: false,
              min: 1.4,
              max: 1.6,
              occasional: null,
            },
            tide: {
              previous: {
                type: 'LOW',
                height: 0,
                timestamp: 1487710022,
                utcOffset: -8,
              },
              next: {
                type: 'HIGH',
                height: 1,
                timestamp: 1487733721,
                utcOffset: -8,
              },
            },
            rank: 7,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4803,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a77088c4',
            lat: 32.8060476681448,
            lon: -117.2647886555987,
            name: "Old Man's/Tourmaline",
            cameras: [
              {
                title: "Old Man's/Tourmaline",
                streamUrl:
                  'https://wowzaprod3-i.akamaihd.net/hls/live/253476/c0056f5e/playlist.m3u8',
                stillUrl: 'http://camstills.cdn-surfline.com/tourmalinecam/latest_small.jpg',
                rewindBaseUrl:
                  'http://live-cam-archive.cdn-surfline.com/tourmalinecam/tourmalinecam',
                _id: '58acb07d8816e20012bb43ce',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
            conditions: {
              human: true,
              value: 'FAIR',
            },
            wind: {
              speed: 8.56,
              direction: 327.48,
            },
            waveHeight: {
              human: true,
              min: 0.9,
              max: 1.5,
              occasional: null,
              humanRelation: 'waist to head high',
            },
            tide: {
              previous: {
                type: 'LOW',
                height: 0,
                timestamp: 1487710022,
                utcOffset: -8,
              },
              next: {
                type: 'HIGH',
                height: 1,
                timestamp: 1487733721,
                utcOffset: -8,
              },
            },
            rank: 8,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4804,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a7708841',
            lat: 32.796726556979,
            lon: -117.2599124908,
            name: 'Pacific Beach',
            cameras: [
              {
                title: 'Pacific Beach',
                streamUrl:
                  'https://wowzaprod3-i.akamaihd.net/hls/live/253451/3c0b6b79/playlist.m3u8',
                stillUrl: 'http://camstills.cdn-surfline.com/pacificbeachcam/latest_small.jpg',
                rewindBaseUrl:
                  'http://live-cam-archive.cdn-surfline.com/pacificbeachcam/pacificbeachcam',
                _id: '58acb09c8816e20012bb43f6',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
            conditions: {
              human: true,
              value: 'FAIR',
            },
            wind: {
              speed: 8.92,
              direction: 326.83,
            },
            waveHeight: {
              human: true,
              min: 1.2,
              max: 1.5,
              occasional: 1.8,
              humanRelation: 'shoulder to head high',
            },
            tide: {
              previous: {
                type: 'LOW',
                height: 0,
                timestamp: 1487710022,
                utcOffset: -8,
              },
              next: {
                type: 'HIGH',
                height: 1,
                timestamp: 1487733721,
                utcOffset: -8,
              },
            },
            rank: 9,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4250,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a7708842',
            lat: 32.776636,
            lon: -117.255139,
            name: 'Mission Beach',
            cameras: [
              {
                title: 'Mission Beach',
                streamUrl:
                  'https://wowzaprod3-i.akamaihd.net/hls/live/253362/cca8fb14/playlist.m3u8',
                stillUrl: 'http://camstills.cdn-surfline.com/missionbeachcam/latest_small.jpg',
                rewindBaseUrl:
                  'http://live-cam-archive.cdn-surfline.com/missionbeachcam/missionbeachcam',
                _id: '58acb0908816e20012bb43e9',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
            conditions: {
              human: true,
              value: 'FAIR',
            },
            wind: {
              speed: 9.32,
              direction: 324.55,
            },
            waveHeight: {
              human: true,
              min: 1.2,
              max: 1.5,
              occasional: 1.8,
              humanRelation: 'shoulder to head high',
            },
            tide: {
              previous: {
                type: 'LOW',
                height: 0,
                timestamp: 1487710022,
                utcOffset: -8,
              },
              next: {
                type: 'HIGH',
                height: 1,
                timestamp: 1487733721,
                utcOffset: -8,
              },
            },
            rank: 10,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4252,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a770883f',
            lat: 32.748931,
            lon: -117.254273,
            name: 'Ocean Beach, SD',
            cameras: [
              {
                title: 'Ocean Beach, SD',
                streamUrl:
                  'https://wowzaprod3-i.akamaihd.net/hls/live/253362/7965c37e/playlist.m3u8',
                stillUrl: 'http://camstills.cdn-surfline.com/oceanbeachsdcam/latest_small.jpg',
                rewindBaseUrl:
                  'http://live-cam-archive.cdn-surfline.com/oceanbeachsdcam/oceanbeachsdcam',
                _id: '58acb0738816e20012bb43c6',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
            conditions: {
              human: true,
              value: 'FAIR',
            },
            wind: {
              speed: 8.89,
              direction: 326.06,
            },
            waveHeight: {
              human: true,
              min: 1.2,
              max: 1.5,
              occasional: 2.1,
              humanRelation: 'shoulder to head high',
            },
            tide: null,
            rank: 11,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4253,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a7708840',
            lat: 32.73693,
            lon: -117.257176,
            name: 'Sunset Cliffs',
            cameras: [
              {
                title: 'Sunset Cliffs',
                streamUrl:
                  'https://wowzaprod3-i.akamaihd.net/hls/live/253476/b88a152a/playlist.m3u8',
                stillUrl: 'http://camstills.cdn-surfline.com/sunsetcliffscam/latest_small.jpg',
                rewindBaseUrl:
                  'http://live-cam-archive.cdn-surfline.com/sunsetcliffscam/sunsetcliffscam',
                _id: '58acb0dc8816e20012bb443d',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
            conditions: {
              human: true,
              value: 'POOR_TO_FAIR',
            },
            wind: {
              speed: 9.11,
              direction: 327.36,
            },
            waveHeight: {
              human: true,
              min: 1.2,
              max: 1.5,
              occasional: 2.1,
              humanRelation: 'shoulder to head high',
            },
            tide: null,
            rank: 12,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4254,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a77088ce',
            lat: 32.68213970271471,
            lon: -117.1870663079863,
            name: 'Coronado Beach',
            cameras: [],
            conditions: {
              human: false,
              value: null,
            },
            wind: {
              speed: 10.83,
              direction: 313.17,
            },
            waveHeight: {
              human: false,
              min: 1.2,
              max: 1.4,
              occasional: null,
            },
            tide: {
              previous: {
                type: 'LOW',
                height: 0,
                timestamp: 1487710771,
                utcOffset: -8,
              },
              next: {
                type: 'HIGH',
                height: 1.1,
                timestamp: 1487734201,
                utcOffset: -8,
              },
            },
            rank: 13,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4816,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a7708847',
            lat: 32.5816434988244,
            lon: -117.13321566581726,
            name: 'Imperial Pier NS',
            cameras: [
              {
                title: 'Imperial Pier NS',
                streamUrl:
                  'https://wowzaprod3-i.akamaihd.net/hls/live/253362/566941cf/playlist.m3u8',
                stillUrl: 'http://camstills.cdn-surfline.com/imperialpierncam/latest_small.jpg',
                rewindBaseUrl:
                  'http://live-cam-archive.cdn-surfline.com/imperialpierncam/imperialpierncam',
                _id: '58acb1458816e20012bb44ba',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
            conditions: {
              human: true,
              value: 'FAIR',
            },
            wind: {
              speed: 12.48,
              direction: 308.96,
            },
            waveHeight: {
              human: true,
              min: 1.2,
              max: 1.5,
              occasional: 1.8,
              humanRelation: 'shoulder to head high',
            },
            tide: null,
            rank: 14,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4255,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a7708844',
            lat: 32.577195519016,
            lon: -117.1347498893,
            name: 'Imperial Pier SS',
            cameras: [
              {
                title: 'Imperial Pier SS',
                streamUrl:
                  'https://wowzaprod3-i.akamaihd.net/hls/live/253362/566941cf/playlist.m3u8',
                stillUrl: 'http://camstills.cdn-surfline.com/imperialpierscam/latest_small.jpg',
                rewindBaseUrl:
                  'http://live-cam-archive.cdn-surfline.com/imperialpierscam/imperialpierscam',
                _id: '58acb1458816e20012bb44bb',
                status: {
                  isDown: false,
                  message: '',
                },
              },
            ],
            conditions: {
              human: true,
              value: 'FAIR',
            },
            wind: {
              speed: 12.31,
              direction: 310.67,
            },
            waveHeight: {
              human: true,
              min: 1.2,
              max: 1.5,
              occasional: 1.8,
              humanRelation: 'shoulder to head high',
            },
            tide: null,
            rank: 15,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4256,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
          {
            _id: '5842041f4e65fad6a77088cb',
            lat: 32.557056711256,
            lon: -117.1327445020322,
            name: 'Tijuana Slough',
            cameras: [],
            conditions: {
              human: false,
              value: null,
            },
            wind: {
              speed: 12.05,
              direction: 311.68,
            },
            waveHeight: {
              human: false,
              min: 1.6,
              max: 1.9,
              occasional: null,
            },
            tide: null,
            rank: 16,
            subregionId: '58581a836630e24c4487900d',
            legacyId: 4814,
            thumbnail:
              'https://s3-us-west-1.amazonaws.com/sl-tmp-kbyg-cdn/nocam/nocam-img%402x.jpg',
          },
        ],
      },
      units: {
        windSpeed: 'KPH',
        waveHeight: 'M',
        tideHeight: 'M',
      },
    },
  },
  subregion: {
    loading: false,
    subregionId: null,
    overview: {
      subregionId: '',
      associated: {
        chartsUrl: null,
        units: {
          waveHeight: null,
        },
      },
      overview: {
        data: null,
      },
      loading: false,
      fetched: false,
    },
    nearby: {},
    feed: {
      loading: false,
      error: null,
      articles: [],
    },
    primarySpotId: null,
  },
  backplane: {
    meter: null,
    state: { meterLoaded: true, shouldRecomputeMeterModal: false },
    errors: [],
    user: {
      details: {
        firstName: 'Luigi',
        lastName: 'Doge',
        email: 'luigi@surfline.com',
      },
      promotionEntitlements: [],
      warnings: [],
      emailSent: false,
      countryCode: 'US',
      ip: '123',
      location: {
        latitude: 90,
        longitude: 123,
        time_zone: 'America/Los_Angeles',
      },
      favorites: [
        {
          _id: '5842041f4e65fad6a77088e4',
          name: 'South Side/13th Street',
          cameras: [
            {
              title: 'South Side/13th Street',
              streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253451/91961185/playlist.m3u8',
              stillUrl:
                'https://camstills.cdn-surfline.com/sealbeachsscam/latest_small_pixelated.png',
              rewindBaseUrl:
                'http://live-cam-archive.cdn-surfline.com/sealbeachsscam/sealbeachsscam',
              _id: '58dad5a4d572cb0012fdb4b1',
              status: {
                isDown: false,
                message: '',
              },
            },
          ],
          conditions: {
            human: false,
            value: null,
          },
          wind: {
            speed: 11.81,
            direction: 342.58,
          },
          waveHeight: {
            human: false,
            min: 0,
            max: 1,
            occasional: null,
            humanRelation: null,
          },
          tide: {
            previous: {
              type: 'HIGH',
              height: 5,
              timestamp: 1490721448,
              utcOffset: -7,
            },
            next: {
              type: 'LOW',
              height: 0,
              timestamp: 1490743709,
              utcOffset: -7,
            },
          },
          thumbnail:
            'https://spot-thumbnails.cdn-surfline.com/spots/5842041f4e65fad6a77088e4/5842041f4e65fad6a77088e4_1500.jpg',
          rank: 0,
        },
      ],
      entitlements: [
        'sl_no_ads',
        'sl_premium',
        'Single Surf Alert address',
        'Local photos',
        'Uploads',
        'Galleries',
        'Favorites',
        'Follow Photographers',
      ],
      settings: {
        units: {
          surfHeight: 'FT',
          swellHeight: 'M',
          tideHeight: 'M',
          windSpeed: 'KTS',
          temperature: 'C',
        },
        feed: {
          localized: 'US',
        },
        date: {
          format: 'MDY',
        },
        navigation: {
          spotsTaxonomyLocation: '58f7ed54dadb30820bb38b6a',
          forecastsTaxonomyLocation: '58f7ed87dadb30820bb3c50d',
        },
        recentlyVisited: {
          subregions: [
            {
              _id: '58581a836630e24c44878fcb',
              name: 'North Shore Oahu',
            },
            {
              _id: '58581a836630e24c44879011',
              name: 'Santa Cruz',
            },
            {
              _id: '5b60a5639c0292001aa51919',
              name: 'U.S. Wave Pool',
            },
            {
              _id: '58581a836630e24c44879053',
              name: 'Central Chile',
            },
            {
              _id: '58581a836630e24c44878fe5',
              name: 'Cabo',
            },
            {
              _id: '5d3a1a9d72d34700014f5e73',
              name: 'S OC - The Point/Old Mans',
            },
            {
              _id: '5d39e96972d34700014f5e6b',
              name: 'N OC - HB Pier',
            },
            {
              _id: '58581a836630e24c4487915c',
              name: 'Marseille',
            },
            {
              _id: '58581a836630e24c44879081',
              name: 'Hawaii Hilo',
            },
            {
              _id: '58581a836630e24c44878ff5',
              name: 'Northern NSW',
            },
          ],
          spots: [
            {
              _id: '5842041f4e65fad6a7708806',
              name: 'Steamer Lane',
            },
            {
              _id: '5842041f4e65fad6a7708827',
              name: 'HB Pier, Northside',
            },
            {
              _id: '5842041f4e65fad6a7708890',
              name: 'Mokuleia',
            },
            {
              _id: '5842041f4e65fad6a7708bb5',
              name: 'Mole Point',
            },
            {
              _id: '5842041f4e65fad6a7708980',
              name: 'Waddell Creek',
            },
            {
              _id: '5842041f4e65fad6a7708805',
              name: 'Steamer Lane',
            },
            {
              _id: '5842041f4e65fad6a7708982',
              name: 'Scott Creek',
            },
            {
              _id: '5842041f4e65fad6a7708981',
              name: 'Scott Creek',
            },
            {
              _id: '5977abb3b38c2300127471ec',
              name: 'HB Pier Southside',
            },
            {
              _id: '5842041f4e65fad6a7708987',
              name: "Mitchell's Cove",
            },
          ],
        },
        feedFilters: {
          subregions: '',
        },
        onboarded: ['NEW_SURFLINE_ONBOARDING_INTRO', 'FAVORITE_SPOTS'],
        boardTypes: [],
        travelDistance: 90,
        bwWindWaveModel: 'LOLA',
        preferredWaveChart: 'SURF',
        homeSpot: null,
        homeForecast: null,
        isSwellChartCollapsed: true,
        receivePromotions: false,
        _id: '5aac31f7ffd66500117e5b63',
        user: '5aac31f7ffd66500117e5b62',
        abilityLevel: 'ADVANCED',
      },
    },
  },
};

export default state;
