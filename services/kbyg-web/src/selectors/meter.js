import { getMeterRemaining, getPromotionEntitlements, getMeter } from '@surfline/web-common';
import { shallowEqual } from 'react-redux';
import { useAppSelector } from '../stores/hooks';

/**
 * @typedef {import('@surfline/web-common').State} State
 */

export const usePromotionEntitlements = () =>
  useAppSelector(getPromotionEntitlements, shallowEqual);

/**
 * @param {{ backplane: { meter: import('@surfline/web-common').MeterState }}} state
 */
export const getIsSurfCheckEligible = (state) => {
  const meterRemaining = getMeterRemaining(state);

  return meterRemaining > -1;
};

/**
 * @returns {boolean}
 */
export const useIsSurfCheckEligible = () => {
  const meterRemaining = useAppSelector(getMeterRemaining);

  return meterRemaining > -1;
};

/**
 * @param {State} state
 * @returns {boolean}
 */
export const getMeteringEnabled = (state) => state.backplane?.state?.meteringEnabled;

export const getActiveMeter = (state) => {
  if (getMeteringEnabled(state)) {
    return getMeter(state);
  }
  return undefined;
};
