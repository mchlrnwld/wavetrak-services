import { expect } from 'chai';
import deepFreeze from 'deep-freeze';
import * as selectors from './mapV2';
import stateFixture from './fixtures/state';

describe('selectors / mapV2', () => {
  const state = deepFreeze(stateFixture);

  it('should get a map location', () => {
    const location = selectors.getMapLocation(state);
    expect(location).to.equal(stateFixture.mapV2.location);
  });
  it('should get a map location loaded', () => {
    const locationLoaded = selectors.getMapLocationLoaded(state);
    expect(locationLoaded).to.equal(stateFixture.mapV2.locationLoaded);
  });
  it('should get a map bounding box', () => {
    const s = selectors.getMapBoundingBox(state);
    expect(s).to.equal(stateFixture.mapV2.boundingBox);
  });
  it('should get a map error', () => {
    const error = selectors.getMapError(state);
    expect(error).to.equal(stateFixture.mapV2.error);
  });
  it('should get a map loading', () => {
    const loading = selectors.getMapLoading(state);
    expect(loading).to.equal(stateFixture.mapV2.loading);
  });
  it('should get a map message', () => {
    const message = selectors.getMapMessage(state);
    expect(message).to.equal(stateFixture.mapV2.message);
  });

  it('should get a location view', () => {
    const message = selectors.getLocationView(state);
    expect(message).to.equal(stateFixture.mapV2.locationView);
  });

  it('should get a location view bounding box', () => {
    const message = selectors.getLocationViewBoundingBox(state);
    expect(message).to.equal(stateFixture.mapV2.locationView.boundingBox);
  });

  it('should get a location view geonameId', () => {
    const message = selectors.getLocationViewGeonameId(state);
    expect(message).to.equal(stateFixture.mapV2.locationView.taxonomy.geonameId);
  });

  it('should get the map zoom', () => {
    const message = selectors.getMapZoom(state);
    expect(message).to.equal(stateFixture.mapV2.location.zoom);
  });
});
