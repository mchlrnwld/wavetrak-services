import React from 'react';
import PropTypes from 'prop-types';
import { Spinner, useTreatment } from '@surfline/quiver-react';

import { SL_WEB_CONDITION_VARIATION } from './treatments';
import { useAppSelector } from '../stores/hooks';

const withConditionVariation = (WrappedComponent) => {
  const WrappedWithConditionVariation = (props) => {
    const splitReady = useAppSelector((state) => state.split.ready);
    const { treatment, loading } = useTreatment(SL_WEB_CONDITION_VARIATION, { splitReady });

    if (loading) {
      return (
        <div className="sl-condition-variant-wrapper">
          <Spinner />
        </div>
      );
    }

    return <WrappedComponent {...props} conditionsV2={treatment === 'on'} />;
  };
  WrappedWithConditionVariation.propTypes = {
    search: PropTypes.string,
  };
  WrappedWithConditionVariation.defaultProps = {
    search: null,
  };
  return WrappedWithConditionVariation;
};

export default withConditionVariation;
