import { expect } from 'chai';
import sinon from 'sinon';
import * as fetch from '../baseFetch';
import { fetchCamRewind } from './cameras';

describe('cameras api fetch', () => {
  beforeEach(() => {
    sinon.stub(fetch, 'default');
  });
  afterEach(() => {
    fetch.default.restore();
  });
  it('should fetch from the cameras api', async () => {
    fetch.default.resolves([]);
    const data = await fetchCamRewind('test_alius', 1602975600, true);
    expect(data).to.eql([]);
    expect(fetch.default).to.have.been.calledWith(
      '/cameras/recording/test_alius?startDate=1602975600000&endDate=1603062000000',
    );
    return true;
  });
});
