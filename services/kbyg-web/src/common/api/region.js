import baseFetch, { createParamString } from '../baseFetch';
import { getMeterParamsForRequest } from '../../utils/meteringHelper';
import addClientIpHeader from '../../utils/addClientIpHeader';

export const fetchRegionalOverview = (
  subregionId,
  cookies,
  meter,
  clientIp,
  sevenRatings = false,
) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  const clientIpHeader = addClientIpHeader(clientIp);
  return baseFetch(
    `/kbyg/regions/overview?${createParamString({ subregionId, meterRemaining, sevenRatings })}`,
    {
      cookies,
      headers: {
        ...meterHeaders,
        ...clientIpHeader,
      },
    },
  );
};

export const fetchNearbySubregions = (subregionId, cookies, meter, sevenRatings = false) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  return baseFetch(
    `/kbyg/regions/nearby?${createParamString({ subregionId, meterRemaining, sevenRatings })}`,
    {
      cookies,
      headers: {
        ...meterHeaders,
      },
    },
  );
};

export const fetchRegionalArticles = async (subregionId, cookies) =>
  baseFetch(`/feed/regional?${createParamString({ subregionId })}`, { cookies });
