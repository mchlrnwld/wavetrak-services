import baseFetch from '../baseFetch';

// eslint-disable-next-line import/prefer-default-export
export const resendEmailVerification = (accessToken) =>
  baseFetch(`/user/resend-email-verification?accesstoken=${accessToken}`, {
    method: 'POST',
    body: JSON.stringify({ brand: 'sl' }),
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
    },
  });
