import fetch from 'isomorphic-fetch';
import config from '../../config';
import { secondsPerDay } from '../../utils/midnightOfDate';
import baseFetch, { createParamString } from '../baseFetch';
import { getMeterParamsForRequest } from '../../utils/meteringHelper';

export const fetchCamEmbedDetails = async (cameraId) => {
  const response = await fetch(`${config.surflineEmbedURL}/cam-details/${cameraId}.json`);
  return response.json();
};

// Converts seconds to milliseconds
const toMilliseconds = (seconds) => seconds * 1000;

export const fetchCamRewind = async (camName, date, isPremium, activeMeter) => {
  // Allows use with useSVR so that it can be called before date is calculated
  // (where date is dependent on another request)
  if (!date || !camName) return null;
  if (!isPremium) return [];
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(activeMeter);
  const startDate = toMilliseconds(date);
  const endDate = toMilliseconds(+date + secondsPerDay);
  try {
    return baseFetch(
      `/cameras/recording/${camName}?${createParamString({ startDate, endDate, meterRemaining })}`,
      {
        headers: {
          ...meterHeaders,
          'X-Auth-ClientId': `Basic ${Buffer.from(`${config.clientId}`).toString('base64')}`,
        },
      },
    );
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(error);
    return [];
  }
};

export const fetchSpotsForCam = async (cameraId) => baseFetch(`/cameras/rewind/id/${cameraId}?`);
