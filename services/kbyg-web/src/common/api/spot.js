import 'isomorphic-fetch';
import baseFetch, { createParamString } from '../baseFetch';
import config from '../../config';
import { getMeterParamsForRequest } from '../../utils/meteringHelper';
import addClientIpHeader from '../../utils/addClientIpHeader';

export const fetchReport = (
  spotId,
  cookies,
  meter,
  corrected = false,
  clientIp = null,
  sevenRatings = false,
) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  const clientIpHeader = addClientIpHeader(clientIp);
  return baseFetch(
    `/kbyg/spots/reports?${createParamString({ spotId, meterRemaining, corrected, sevenRatings })}`,
    {
      cookies,
      headers: {
        ...meterHeaders,
        ...clientIpHeader,
      },
    },
  );
};

export const fetchNearbySpots = (spotId, cookies, meter, clientIp = null, sevenRatings = false) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  const clientIpHeader = addClientIpHeader(clientIp);
  return baseFetch(
    `/kbyg/spots/nearby?${createParamString({ spotId, meterRemaining, sevenRatings })}`,
    {
      cookies,
      headers: {
        ...meterHeaders,
        ...clientIpHeader,
      },
    },
  );
};

export const fetchSpotDetails = (spotId, cookies) =>
  baseFetch(`/kbyg/spots/details?spotId=${spotId}`, { cookies });

export const fetchBuoys = async (legacySpotId, units) => {
  const url = `${config.legacyApi}/mobile/nearby/${legacySpotId}?resources=buoy&unit=${units}&buoys=6`;
  const response = await fetch(url);
  const body = await response.json();
  if (response.status > 200) throw body;
  return body;
};
