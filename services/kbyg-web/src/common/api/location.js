import baseFetch from '../baseFetch';

// eslint-disable-next-line import/prefer-default-export
export const fetchWorldTaxonomy = (cookies) =>
  baseFetch('/location/view/worldtaxonomy?', { cookies });
