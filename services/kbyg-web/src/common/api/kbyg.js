import baseFetch, { createParamString } from '../baseFetch';
import { getMeterParamsForRequest } from '../../utils/meteringHelper';

export const fetchWaveForecast = ({
  id,
  days,
  intervalHours,
  maxHeights = false,
  corrected,
  meter,
}) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  return baseFetch(
    `/kbyg/spots/forecasts/wave?${createParamString({
      spotId: id,
      days,
      intervalHours,
      maxHeights,
      meterRemaining,
      corrected,
    })}`,
    {
      headers: {
        ...meterHeaders,
      },
    },
  );
};

export const fetchTideForecast = ({ id, days, meter }) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  return baseFetch(
    `/kbyg/spots/forecasts/tides?${createParamString({
      spotId: id,
      days,
      meterRemaining,
    })}`,
    {
      headers: {
        ...meterHeaders,
      },
    },
  );
};

export const fetchWeatherForecast = ({ id, days, intervalHours, corrected, meter }) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  return baseFetch(
    `/kbyg/spots/forecasts/weather?${createParamString({
      spotId: id,
      days,
      intervalHours,
      meterRemaining,
      corrected,
    })}`,
    {
      headers: {
        ...meterHeaders,
      },
    },
  );
};

export const fetchWindForecast = ({ id, days, intervalHours, correctedWind = false, meter }) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  return baseFetch(
    `/kbyg/spots/forecasts/wind?${createParamString({
      spotId: id,
      days,
      intervalHours,
      meterRemaining,
      corrected: correctedWind,
    })}`,
    {
      headers: {
        ...meterHeaders,
      },
    },
  );
};

export const fetchAutomatedRatingForecast = ({
  id,
  days,
  intervalHours,
  ratingsAlgorithm,
  meter,
  corrected,
  correctedRating,
  correctedWind = false,
}) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  return baseFetch(
    `/kbyg/spots/forecasts/rating?${createParamString({
      spotId: id,
      days,
      intervalHours,
      meterRemaining,
      ratingsAlgorithm,
      corrected: correctedRating,
      correctedSurf: corrected,
      correctedWind,
    })}`,
    {
      headers: {
        ...meterHeaders,
      },
    },
  );
};

export const fetchRegionalConditionForecast = ({
  id,
  days,
  corrected,
  meter,
  sevenRatings = false,
}) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  return baseFetch(
    `/kbyg/regions/forecasts/conditions?${createParamString({
      subregionId: id,
      days,
      meterRemaining,
      corrected,
      sevenRatings,
    })}`,
    {
      headers: {
        ...meterHeaders,
      },
    },
  );
};

export const fetchRegionalWaveForecast = ({
  id,
  days,
  intervalHours,
  maxHeights = false,
  corrected,
  meter,
}) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  return baseFetch(
    `/kbyg/regions/forecasts/wave?${createParamString({
      subregionId: id,
      days,
      intervalHours,
      maxHeights,
      meterRemaining,
      corrected,
    })}`,
    {
      headers: {
        ...meterHeaders,
      },
    },
  );
};

export const fetchTaxononmySwellEvents = (id, type, meter) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  return baseFetch(`/kbyg/events/taxonomy?${createParamString({ id, type, meterRemaining })}`, {
    headers: {
      ...meterHeaders,
    },
  });
};
