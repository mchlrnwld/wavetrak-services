import baseFetch from '../baseFetch';
import addClientIpHeader from '../../utils/addClientIpHeader';
import type { SessionClip, Session } from '../../types/sessions';

export const fetchSessionsClip = (
  clipId: string,
  cookies: Record<string, string>,
  clientIp?: string | null,
): Promise<SessionClip> =>
  baseFetch(`/sessions/clips/${clipId}?`, {
    cookies,
    headers: {
      ...addClientIpHeader(clientIp),
    },
  });

export const fetchSession = (
  sessionId: string,
  cookies: { [key: string]: string },
): Promise<Session> => baseFetch(`/sessions/${sessionId}/details?`, { cookies });
