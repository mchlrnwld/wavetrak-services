/* istanbul ignore file */
import baseFetch from '../baseFetch';

/**
 * @description
 * Performs a metering service surf-check call in order to
 * either consume a new surf check or extend the current surf check.
 * @param {Record<string, string | string[]>} [cookies] - For SSR requests performed by a static fetch
 * we have to manually pass cookies in order to have the access_token
 */
export const performMeteringSurfCheck = (cookies) =>
  baseFetch('/meter/surf-check?', {
    method: 'PATCH',
    cookies,
  });
