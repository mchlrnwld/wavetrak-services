import 'isomorphic-fetch';
import baseFetch, { createParamString } from '../baseFetch';
import { getMeterParamsForRequest } from '../../utils/meteringHelper';

export const fetchLegacyChartImages = (legacyId, type, nearshoreModelName, meter, tempUnit) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  return baseFetch(
    `/charts/legacy/images?${createParamString({
      legacyId,
      type,
      nearshoreModelName,
      meterRemaining,
      tempUnit,
    })}`,
    {
      headers: {
        ...meterHeaders,
      },
    },
  );
};

export const fetchLegacyChartTypes = (subregionId, cookies, meter) => {
  const { meterHeaders, meterRemaining } = getMeterParamsForRequest(meter);
  return baseFetch(`/charts/legacy/types?${createParamString({ subregionId, meterRemaining })}`, {
    cookies,
    headers: {
      ...meterHeaders,
    },
  });
};
