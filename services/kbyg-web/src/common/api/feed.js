import baseFetch, { createParamString } from '../baseFetch';

// eslint-disable-next-line import/prefer-default-export
export const fetchPromoBoxSlides = async (cookies, geotarget, limit = 7) =>
  baseFetch(`/feed/promobox?${createParamString({ limit, geotarget })}`, { cookies });
