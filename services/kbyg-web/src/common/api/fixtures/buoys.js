import { formatBuoyReportTableData } from '../../../utils/formatBuoyReportTableData';

/** @type {import('../buoys').NearbyBuoysResponse} */
export const getNearbyBuoysFixture = {
  associated: {
    units: {
      temperature: 'F',
      tideHeight: 'FT',
      swellHeight: 'FT',
      waveHeight: 'FT',
      windSpeed: 'KTS',
      model: 'LOTUS',
    },
  },
  data: [
    {
      id: '6a05b758-ca15-11eb-9a8d-0242015eddd9',
      sourceId: '46047',
      name: 'TANNER BANK - 121 NM West of San Diego, CA',
      latitude: 32.388,
      longitude: -119.525,
      status: 'ONLINE',
      abbrTimezone: 'PDT',
      latestData: {
        timestamp: 1626731400,
        height: 4.59318,
        period: 10,
        direction: 283,
        swells: [
          {
            height: 4.10051,
            period: 10,
            direction: 290,
          },
          {
            height: 1.7213,
            period: 14.70588,
            direction: 165,
          },
          {
            height: 0.35214,
            period: 2.94118,
            direction: 300,
          },
          {
            height: 0,
            period: null,
            direction: null,
          },
          {
            height: 0,
            period: null,
            direction: null,
          },
          {
            height: 0,
            period: null,
            direction: null,
          },
        ],
        utcOffset: -7,
      },
    },
    {
      id: '4aadee92-ca15-11eb-bf89-0242015eddd9',
      sourceId: '067',
      name: 'San Nicolas Island, CA (067)',
      latitude: 33.219,
      longitude: -119.872,
      status: 'ONLINE',
      abbrTimezone: 'PDT',
      latestData: {
        timestamp: 1626731760,
        height: 5.24934,
        period: 10,
        direction: 300,
        swells: [
          {
            height: 4.56519,
            period: 9.90099,
            direction: 310,
          },
          {
            height: 2.13731,
            period: 14.28571,
            direction: 175,
          },
          {
            height: 1.42037,
            period: 5.55556,
            direction: 290,
          },
          {
            height: 0.55683,
            period: 2.5641,
            direction: 290,
          },
          {
            height: 0,
            period: null,
            direction: null,
          },
          {
            height: 0,
            period: null,
            direction: null,
          },
        ],
        utcOffset: -7,
      },
    },
  ],
};

/** @type {import('../buoys').BoundingBoxResponse} */
export const getBuoysInBoundingBoxFixture = {
  associated: {
    units: {
      temperature: 'F',
      tideHeight: 'FT',
      swellHeight: 'FT',
      waveHeight: 'FT',
      windSpeed: 'KTS',
      model: 'LOTUS',
    },
  },
  data: [
    {
      id: '1b3c155c-cecc-11eb-bbec-024238d3b313',
      name: 'Santa Monica Basin - 33NM WSW of Santa Monica, CA',
      sourceId: '46025',
      latitude: 33.758,
      longitude: -119.044,
      status: 'ONLINE',
      abbrTimezone: 'PDT',
      latestData: {
        timestamp: 1627415400,
        height: 2.62467,
        period: 14,
        direction: 196,
        swells: [
          {
            height: 2.28931,
            period: 13.69863,
            direction: 190,
          },
          {
            height: 1.16071,
            period: 7.69231,
            direction: 240,
          },
          {
            height: 0,
            period: null,
            direction: null,
          },
          {
            height: 0,
            period: null,
            direction: null,
          },
          {
            height: 0,
            period: null,
            direction: null,
          },
          {
            height: 0,
            period: null,
            direction: null,
          },
        ],
        utcOffset: -7,
      },
    },
    {
      id: 'c795cab8-cecc-11eb-84e7-024238d3b313',
      name: 'Harvest, CA (071)',
      sourceId: '46218',
      latitude: 34.452,
      longitude: -120.78,
      status: 'ONLINE',
      abbrTimezone: 'PDT',
      latestData: {
        timestamp: 1627415760,
        height: 4.59318,
        period: 13,
        direction: 192,
        swells: [
          {
            height: 3.38851,
            period: 8.33333,
            direction: 320,
          },
          {
            height: 2.88992,
            period: 13.33333,
            direction: 190,
          },
          {
            height: 0.55751,
            period: 4.16667,
            direction: 305,
          },
          {
            height: 0,
            period: null,
            direction: null,
          },
          {
            height: 0,
            period: null,
            direction: null,
          },
          {
            height: 0,
            period: null,
            direction: null,
          },
        ],
        utcOffset: -7,
      },
    },
  ],
};

export const buoyReportDataFixture = {
  associated: {
    abbrTimezone: 'PDT',
    units: {
      temperature: 'F',
      tideHeight: 'FT',
      swellHeight: 'FT',
      waveHeight: 'FT',
      surfHeight: 'FT',
      windSpeed: 'KTS',
    },
  },
  data: [
    {
      timestamp: 1627422600,
      height: 4.3,
      period: 13,
      direction: 179,
      waterTemperature: 32,
      airTemperature: 58,
      dewPoint: 56,
      pressure: 1017.13,
      wind: {
        gust: 8,
        speed: 6,
        direction: 290,
      },
      swells: [
        {
          height: 3.2,
          period: 8,
          direction: 320,
        },
        {
          height: 0,
          period: 13,
          direction: 175,
        },
        {
          height: 1.5,
          period: null,
          direction: 280,
        },
        {
          height: 3.2,
          period: 8,
          direction: null,
        },
        {
          height: 0,
          period: null,
          direction: null,
        },
        {
          height: 0,
          period: null,
          direction: null,
        },
      ],
      utcOffset: -7,
    },
  ],
};

function generateGetBuoyReportFixture(numDays) {
  const getBuoyReportFixture = {
    associated: {
      abbrTimezone: 'UTC',
      units: {
        temperature: 'C',
        tideHeight: 'FT',
        swellHeight: 'FT',
        waveHeight: 'FT',
        windSpeed: 'MPH',
        model: 'LOTUS',
      },
    },
    data: [],
  };

  for (let i = 0; i < numDays * 24; i += 1) {
    const swells = [];
    for (let j = 0; j < 6; j += 1) {
      swells.push({
        height: Math.floor(Math.random() * 26 + 1),
        period: Math.floor(Math.random() * 23 + 3),
        direction: Math.floor(Math.random() * 360),
        directionMin: Math.floor(Math.random() * 360),
        optimalScore: 2,
      });
    }

    getBuoyReportFixture.data.push({
      timestamp: 1624431600 + 3600 * i,
      utcOffset: -7,
      period: Math.floor(Math.random() * 23 + 3),
      height: Math.floor(Math.random() * 26 + 1),
      direction: Math.floor(Math.random() * 360),
      airTemperature: Math.floor(Math.random() * 101),
      waterTemperature: Math.floor(Math.random() * 101),
      wind: {
        speed: Math.floor(Math.random() * 26 + 1),
        direction: Math.floor(Math.random() * 360),
        gust: Math.floor(Math.random() * 26 + 1),
      },
      pressure: Math.floor(Math.random() * 501 + 1000),
      dewPoint: Math.floor(Math.random() * 51),
      swells,
    });
  }

  return {
    data: formatBuoyReportTableData(getBuoyReportFixture),
    associated: getBuoyReportFixture.associated,
  };
}

export const getBuoyReportFixture = generateGetBuoyReportFixture(3);

export const getBuoyReportSinglePointFixture = {
  data: formatBuoyReportTableData(buoyReportDataFixture),
  associated: buoyReportDataFixture.associated,
};

/** @type {import('../buoys').BuoyDetailsResponse} */
export const getBuoyDetailsFixture = {
  associated: {
    abbrTimezone: 'PDT',
  },
  data: {
    id: '98417d08-ca15-11eb-bedd-0242015eddd9',
    source: 'NDBC',
    sourceId: '46012',
    name: 'HALF MOON BAY - 24NM SSW of San Francisco, CA',
    status: 'ONLINE',
    latitude: 37.356,
    longitude: -122.881,
    latestTimestamp: 1626817800,
    utcOffset: -7,
  },
};
