/* istanbul ignore file */
const report = {
  units: {
    waveHeight: 'FT',
    tideHeight: 'FT',
    windSpeed: 'KT',
  },
  breadCrumbs: ['North America', 'Southern California'],
  spot: {
    name: 'HB Pier, Southside',
    lat: 33.654213041213,
    lon: -118.0032588192,
    cameras: [
      {
        title: 'Huntington Beach Overview',
        streamUrl: 'https://wowzaprod3-i.akamaihd.net/hls/live/253176/22d9084e/playlist.m3u8',
        stillUrl: 'http://camstills.cdn-surfline.com/hbpiernsovcam/latest_small.jpg',
        rewindBaseUrl: 'http://live-cam-archive.cdn-surfline.com/hbpiernsovcam/hbpiernsovcam',
        _id: '589bb121843b99001225ea7b',
      },
    ],
    abilityLevels: ['ADVANCED'],
    boardTypes: ['SHORTBOARD'],
  },
  forecast: {
    conditions: {
      value: 'FAIR_TO_GOOD',
    },
    waveHeight: { min: 2, max: 3, humanRelation: 'Ankle to knee high' },
    tide: {
      height: 5.6,
      type: 'LOW',
      timestamp: 1485364825,
    },
    wind: {
      speed: 4,
      direction: 280,
    },
  },
  report: {
    timestamp: 1485461383,
    forecaster: {
      name: 'Chris Borg',
      iconUrl: 'https://www.gravatar.com/avatar/4eb0adfe0a8e5b2bae1a573ef16ec27f?d=mm',
    },
    report:
      '<strong>Forecast Outlook:</strong><br><strong>TODAY:</strong>&nbsp;New longer period WNW swell&nbsp;peaks.&nbsp;Better breaks are in the waist-head&nbsp;high range&nbsp;in the morning&nbsp;as standouts see some overhead high sets. Significantly smaller in Newport area.&nbsp;<strong>Note</strong>&nbsp;- SLOW EARLY - &nbsp;many&nbsp;breaks&nbsp;will improve through the later part&nbsp;of the morning&nbsp;as the tide drops.<br><strong></strong><br><strong>WEATHER/WIND:</strong>&nbsp;Moderate to breezy&nbsp;offshore flow in the morning hours, light onshore Westerly flow&nbsp;developing for the afternoon in North HB. However, spots in South HB through Newport potentially see offshore flow all day.',
  },
};

export default report;
