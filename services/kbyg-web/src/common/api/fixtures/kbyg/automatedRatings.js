const HOUR_IN_SECONDS = 3600;

/**
 * @typedef {object} Rating
 * @property {number} timestamp
 * @property {0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9} rating
 * @property {number} utcOffset
 */

/**
 * @typedef {object} RatingsResponse
 * @property {object} associated
 * @property {object} associated.location
 * @property {number} associated.location.lon
 * @property {number} associated.location.lat
 * @property {object} data
 * @property {Array<Rating>} data.rating
 */

/**
 * @param {number} [numDays]
 * @param {number} [intervalHours]
 * @param {boolean} [withDSTShift]
 * @param {'forward' | 'backward'} [dstShiftDirection]
 * @returns {RatingsResponse}
 */
const automatedRatingsResponse = (
  numDays = 1,
  intervalHours = 24,
  withDSTShift = false,
  dstShiftDirection = 'forward',
) => {
  const date = new Date();
  const ts = Math.floor(+date / 1000);

  const dstAmount = dstShiftDirection === 'forward' ? -1 : 1;
  const dstShift = withDSTShift ? dstAmount : 0;
  const numDataPoints = numDays * (24 / intervalHours) + dstShift;

  const ratings = [...new Array(numDataPoints)].map((_, i) => ({
    // Derive the timestamp
    timestamp: ts + i * HOUR_IN_SECONDS * intervalHours,
    // We'll do the DST shift at the third data point
    utcOffset: i >= 2 && withDSTShift ? 0 - dstAmount : 0,
    // Deterministic value of rating so that we can target test to it more easily
    rating: {
      key: 'POOR',
      value: 1.1,
    },
  }));

  return {
    associated: {
      location: {
        lon: -122.02522,
        lat: 36.95188851423011,
      },
    },
    data: {
      rating: ratings,
    },
  };
};

export default automatedRatingsResponse;
