import baseFetch, { createParamString } from '../baseFetch';

export const fetchEditorialArticleById = (id, preview, cookies) => {
  const opts = {};
  let url = `/wp-json/sl/v1/posts?${createParamString({ id })}`;
  if (preview) {
    opts.credentials = 'include';
    url += `&${createParamString({ preview: true, uuid: Date.now() })}`;
  }
  return baseFetch(url, opts, { cookies });
};
