const swellHeightClassesFromUnits = (units) => {
  switch (units) {
    case 'M':
      return [0.33, 0.67, 1, 1.33, 1.67, 2, 2.33, 2.67, 3, 3.5, 4, 4.5, 5, 5.5, 6, 7, 8, 9, 10, 11];
    case 'FT':
    default:
      return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 18, 20, 23, 26, 29, 32, 35];
  }
};
export default swellHeightClassesFromUnits;
