import { expect } from '@jest/globals';
import { getUserPremiumStatus, getUserDetails, getMeter } from '@surfline/web-common';
import { useSelector } from 'react-redux';
import { getMeteringEnabled, useIsSurfCheckEligible } from '../../selectors/meter';

import useMetering from './useMetering';

jest.mock('react-redux');
jest.mock('@surfline/web-common');
jest.mock('../../selectors/meter');

describe('Buoy Map Pages', () => {
  useSelector.mockImplementation((selector) => selector());

  afterAll(() => {
    jest.unmock('react-redux');
    jest.unmock('@surfline/web-common');
  });

  it('should return a default state for premium users', async () => {
    getUserPremiumStatus.mockReturnValue(true);
    getMeteringEnabled.mockReturnValue(true);
    const result = useMetering();

    expect(result).toEqual({
      showMeterRegwall: false,
      meterRemaining: undefined,
      isMeteredPremium: false,
      meteringEnabled: false,
    });
  });

  it('should return a default state when metering not enabled', async () => {
    getUserPremiumStatus.mockReturnValue(false);
    getMeteringEnabled.mockReturnValue(false);
    const result = useMetering();

    expect(result).toEqual({
      showMeterRegwall: false,
      meterRemaining: undefined,
      isMeteredPremium: false,
      meteringEnabled: false,
    });
  });

  it('should return a default state when there is not treatment state', async () => {
    getMeteringEnabled.mockReturnValue(true);
    getMeter.mockReturnValue({ treatmentState: undefined });

    const result = useMetering();

    expect(result).toEqual({
      showMeterRegwall: false,
      meterRemaining: undefined,
      isMeteredPremium: false,
      meteringEnabled: true,
    });
  });

  it('should be disabled for anonymous users with treatmentState off', async () => {
    getMeteringEnabled.mockReturnValue(true);
    getMeter.mockReturnValue({ treatmentState: 'off' });

    const result = useMetering();

    expect(result).toEqual({
      showMeterRegwall: false,
      meterRemaining: undefined,
      isMeteredPremium: false,
      meteringEnabled: true,
    });
  });

  it('should be disabled for anonymous user that is surf check eligible', async () => {
    getMeteringEnabled.mockReturnValue(true);
    useIsSurfCheckEligible.mockReturnValue(true);
    getMeter.mockReturnValue({ treatmentState: 'on' });

    const result = useMetering();

    expect(result).toEqual({
      showMeterRegwall: false,
      meterRemaining: undefined,
      isMeteredPremium: true,
      meteringEnabled: true,
    });
  });

  it('should be enabled for anonymous user that is not surf check eligible', async () => {
    getMeteringEnabled.mockReturnValue(true);
    useIsSurfCheckEligible.mockReturnValue(false);
    getMeter.mockReturnValue({ treatmentState: 'on' });

    const result = useMetering();
    expect(result).toEqual({
      showMeterRegwall: false,
      meterRemaining: undefined,
      isMeteredPremium: false,
      meteringEnabled: true,
    });
  });

  it('should be disabled for registered user with treatmentState off', async () => {
    getMeteringEnabled.mockReturnValue(true);
    getUserDetails.mockReturnValue({ _id: '123' });
    getMeter.mockReturnValue({ treatmentState: 'off' });

    const result = useMetering();

    expect(result).toEqual({
      showMeterRegwall: false,
      meterRemaining: undefined,
      isMeteredPremium: false,
      meteringEnabled: true,
    });
  });

  it('should be disabled for a registered user that is surf check eligible', async () => {
    getMeteringEnabled.mockReturnValue(true);
    getUserDetails.mockReturnValue({ _id: '123' });
    getMeter.mockReturnValue({ treatmentState: 'on' });
    useIsSurfCheckEligible.mockReturnValue(true);

    const result = useMetering();

    expect(result).toEqual({
      showMeterRegwall: false,
      meterRemaining: undefined,
      isMeteredPremium: true,
      meteringEnabled: true,
    });
  });

  it('should be enabled for a registered user that is not surf check eligible', async () => {
    getMeteringEnabled.mockReturnValue(true);
    getUserDetails.mockReturnValue({ _id: '123' });
    getMeter.mockReturnValue({ treatmentState: 'on' });
    useIsSurfCheckEligible.mockReturnValue(false);

    const result = useMetering();

    expect(result).toEqual({
      showMeterRegwall: false,
      meterRemaining: undefined,
      isMeteredPremium: false,
      meteringEnabled: true,
    });
  });
});
