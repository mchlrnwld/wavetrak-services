import { useSelector, shallowEqual } from 'react-redux';
import { getUserPremiumStatus, getUserDetails, getMeter } from '@surfline/web-common';
import { getMeteringEnabled, useIsSurfCheckEligible } from '../../selectors/meter';

/**
 * React Hook for Metering Modals. This is a centralized location for all Meter related logic.
 * Depending on the meter state of a user this hook computes whether or not to show the Meter Modal.
 * The hook also determines which Modal to be rendered based on the treatment variant the user is in.
 *
 * @return Object meterState
 */
const useMetering = (component) => {
  const isMeteringEnabled = useSelector(getMeteringEnabled);
  const meter = useSelector(getMeter, shallowEqual);
  const isSurfCheckEligible = useIsSurfCheckEligible();
  const userDetails = useSelector(getUserDetails, shallowEqual);
  const isAnonymous = !userDetails;
  const isPremium = useSelector(getUserPremiumStatus);

  const defaultMeterState = {
    showMeterRegwall: false,
    meterRemaining: undefined,
    isMeteredPremium: false,
    meteringEnabled: !isPremium && isMeteringEnabled,
  };
  // If the user is Premium then don't show the Modals and go through any of this rate limit logic
  if (isPremium || !isMeteringEnabled) {
    return defaultMeterState;
  }

  const { meterRemaining, meterLimit, treatmentState } = meter || {};
  const meterState = {
    ...defaultMeterState,
    meterRemaining,
    meterLimit,
  };
  // If there is no meter then don't show the Modals
  if (!treatmentState) {
    return defaultMeterState;
  }

  // Anonymous Sessions
  if (isAnonymous) {
    // If treatment is OFF then don't show the Modals
    if (treatmentState === 'off') {
      return {
        ...meterState,
        showMeterRegwall: false,
        isMeteredPremium: false,
      };
    }
    if (component === 'ForecastDataVisualizer') {
      return {
        ...meterState,
        showMeterRegwall: !isSurfCheckEligible,
        isMeteredPremium: true,
      };
    }
    if (component === 'BuoyReport') {
      return {
        ...meterState,
        showMeterRegwall: !isSurfCheckEligible,
        isMeteredPremium: isSurfCheckEligible,
      };
    }
    return {
      ...meterState,
      showMeterRegwall: false,
      isMeteredPremium: isSurfCheckEligible,
    };
  }

  // Registered Users
  if (!isPremium) {
    // If treatment is OFF then don't show the Modals
    if (treatmentState === 'off') {
      return {
        ...meterState,
        showMeterRegwall: false,
        isMeteredPremium: false,
      };
    }
    if (component === 'ForecastDataVisualizer') {
      return {
        ...meterState,
        showMeterRegwall: !isSurfCheckEligible,
        isMeteredPremium: true,
      };
    }
    if (component === 'BuoyReport') {
      return {
        ...meterState,
        showMeterRegwall: !isSurfCheckEligible,
        isMeteredPremium: isSurfCheckEligible,
      };
    }
    return {
      ...meterState,
      showMeterRegwall: false,
      isMeteredPremium: isSurfCheckEligible,
    };
  }
  return defaultMeterState;
};

export default useMetering;
