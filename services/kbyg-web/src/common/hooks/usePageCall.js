import { useEffect } from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { trackNavigatedToPage } from '@surfline/web-common';
import { getMeteringEnabled } from '../../selectors/meter';

/**
 * @description Sends a page call event to segment.
 * @param {String} pageName
 * @param {Object} properties A memoized version of the properties object
 * **do not define inline or else the page call will fire every re-render**
 */
export function usePageCall(pageName, properties, rendered = true) {
  const meteringEnabled = useSelector(getMeteringEnabled, shallowEqual);
  useEffect(() => {
    if (rendered) {
      trackNavigatedToPage(pageName, {
        ...properties,
        meteringEnabled,
      });
    }
  }, [pageName, properties, meteringEnabled, rendered]);
}
