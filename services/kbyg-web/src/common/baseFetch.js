import fetch from 'isomorphic-fetch';
import { Cookies } from 'react-cookie';
import { canUseDOM, doCookiesWarning, nrNoticeError } from '@surfline/web-common';
import config from '../config';

const checkStatus = (res) => {
  if (res.status >= 200 && res.status < 300) {
    return res;
  }

  const error = new Error(res.statusText);
  error.response = res;
  error.statusCode = res.status;

  if (error.status >= 500) {
    nrNoticeError(error);
  }

  throw error;
};

/**
 * @param {Response} response
 */
const parseJSON = (response) => response.json();

/**
 * @param {object} [serverSideCookies]
 * @return {string?}
 */
export const getAccessToken = (serverSideCookies) => {
  const cookies = new Cookies(canUseDOM ? undefined : serverSideCookies);
  return cookies.get('access_token');
};

/**
 * @param {boolean?} [wpAPI]
 * @return {string}
 */
export const getBaseURL = (wpAPI) => {
  const baseUrl = wpAPI ? config.wpAPI : config.servicesURL;

  return baseUrl;
};

export const doFetch = (path, options = {}) => {
  doCookiesWarning(path, options);
  const cookies = new Cookies(canUseDOM ? undefined : options.cookies);
  const wpApi = path.indexOf('wp-json') > -1;
  const at = cookies.get('access_token');
  if (at && !wpApi) {
    path += `&accesstoken=${at}`; // eslint-disable-line no-param-reassign
  }
  const baseUrl = wpApi ? config.wpAPI : config.servicesURL;
  return fetch(`${baseUrl}${path}`, {
    ...options,
  })
    .then(checkStatus)
    .then(parseJSON);
};

export const createParamString = (params) =>
  Object.keys(params)
    .filter((key) => params[key] !== undefined && params[key] !== null)
    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join('&');

export const createDecodedParamString = (params) =>
  Object.keys(params)
    .filter((key) => params[key] !== undefined && params[key] !== null)
    .map((key) => `${key}=${params[key]}`)
    .join('&');

export default doFetch;
