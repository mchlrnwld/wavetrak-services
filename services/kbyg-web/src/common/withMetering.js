import React from 'react';
import PropTypes from 'prop-types';
import useMetering from './hooks/useMetering';

/**
 * @description This is a HOC for passing in the meter state to class components.
 * The idea is to get a component as an input, and return that same component with some additional props.
 * This way we can use useMetering hook with class components.
 * @param {JSX.Element} Component
 * @returns {React.FC} Component
 */

const withMetering = (WrappedComponent) => {
  const WrappedWithMetering = (props) => {
    const { componentName } = props;
    const meter = useMetering(componentName);

    const { isPremium } = props;

    if (isPremium) {
      return <WrappedComponent {...props} meter={meter} />;
    }

    const { isMeteredPremium } = meter;

    return <WrappedComponent {...props} meter={meter} isPremium={isMeteredPremium} />;
  };

  WrappedWithMetering.propTypes = {
    isPremium: PropTypes.bool,
  };

  WrappedWithMetering.defaultProps = {
    isPremium: false,
  };

  /**
   * If the Wrapped Component is a top level route and has a `fetch` property
   * we need to assign it here so SSR can pick it up
   */
  if (WrappedComponent.fetch) {
    WrappedWithMetering.fetch = WrappedComponent.fetch;
  }

  return WrappedWithMetering;
};

export default withMetering;
