import React from 'react';
import PropTypes from 'prop-types';
import Skeleton from 'react-loading-skeleton';
import { useSplitTreatment } from '../selectors/split';
import { SL_WEB_CONDITION_VARIATION } from './treatments';

const withConditionVariationV2 = (WrappedComponent) => {
  const WrappedWithConditionVariation = (props) => {
    const { treatment, loading } = useSplitTreatment(SL_WEB_CONDITION_VARIATION);
    if (loading) {
      return (
        <div className="sl-condition-variant-wrapper">
          <Skeleton />
        </div>
      );
    }

    return <WrappedComponent {...props} sevenRatings={treatment === 'on'} />;
  };
  WrappedWithConditionVariation.propTypes = {
    search: PropTypes.string,
  };
  WrappedWithConditionVariation.defaultProps = {
    search: null,
  };
  return WrappedWithConditionVariation;
};

export default withConditionVariationV2;
