export const CLICKED_SUBSCRIBE_CTA = 'Clicked Subscribe CTA';
export const SURF_CHECK_TREATMENT_VARIANT = 'surf_check';
export const ACCOUNT_BENEFITS_TREATMENT_VARIANT = 'account_benefits';

/*
cam error message type options
SUB - Top message + secondary message below
MSG - Top message only
ALT - Single word message for small cam displays
*/
export const SUB = 'SUB';
export const MSG = 'MSG';
export const ALT = 'ALT';

export const LOLA = 'LOLA';
export const LOTUS = 'LOTUS';

export const ONLINE = 'ONLINE';
export const OFFLINE = 'OFFLINE';

export const BUOY_CURRENT_READINGS_MAP_WIDTH = 294;
export const BUOY_CURRENT_READINGS_MAP_HEIGHT = 214;
export const DMY = 'DMY';
export const MDY = 'MDY';

export const BRAZE_CONTENT_CARDS = {
  SPOT_PAGE: 'Braze Content Card - Spot',
};

export const JW_PLAYER_URL = 'https://wa.cdn-surfline.com/quiver/0.12.5/scripts/jwplayer.js';
export const LEAFLET_URL = 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css';
